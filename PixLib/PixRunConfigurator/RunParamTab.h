#ifndef _RunParamTab_h_
#define _RunParamTab_h_

#include <q3groupbox.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
#include <qsplitter.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <qcombobox.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "CommonDataTabView.h"
#include "View.h"
#include "BrowserStateManager.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"

namespace PixRunConfigurator
{

class CommonDataTabView;

class RunParamTab : public View
{

	Q_OBJECT
	
	public:
	
	RunParamTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig);
	~RunParamTab();
		
	void resetView();
	
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void connectAll();
	void disconnectAll();	
	
	void showObjectDetails();
	
	CommonDataTabView* parent;
	
	std::string runConf;
	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
		
	QLabel* internalGen;
	QLabel* consecutiveLVL1;
	QLabel* latency;
	QLabel* nPixInject;
	QLabel* digitalInject;
	QLabel* cInjHi;
	QLabel* vcal;
	QLabel* injectMode;
	QLabel* mccCalDelay;
	QLabel* rodCalDelay;
	QLabel* mccCalWidth;
	QLabel* rodBcidOffset;
	QLabel* injType;
	QLabel* injStep;
	QLabel* injStartPoint;
	QLabel* rawDataMode;
	QLabel* startPreAmpKilled;
	QLabel* dataTakingSpeed;

	QSpinBox* internalGen_edit;
	QSpinBox* consecutiveLVL1_edit;
	QSpinBox* latency_edit;
	QSpinBox* nPixInject_edit;
	QPushButton* digitalInject_edit;//bool
	QPushButton* cInjHi_edit;//bool
	QSpinBox* vcal_edit;
	QPushButton* injectMode_edit;//bool
	QSpinBox* mccCalDelay_edit;
	QSpinBox* rodCalDelay_edit;
	QSpinBox* mccCalWidth_edit;
	QSpinBox* rodBcidOffset_edit;
	QComboBox* injType_edit;//list
	QSpinBox* injStep_edit;
	QSpinBox* injStartPoint_edit;
	QPushButton* rawDataMode_edit;//bool
	QPushButton* startPreAmpKilled_edit;//bool
	QSpinBox* dataTakingSpeed_edit;
	
   	std::map<std::string,int> defaultValues;
   	std::map<std::string,int> newValues;
	
	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	
	Config* cf;
				
	private slots:
	
	void internalGenChanges(int);
	void consecutiveLVL1Changes(int);
	void latencyChanges(int);
	void nPixInjectChanges(int);
 	void digitalInjectChanges();
 	void cInjHiChanges();
	void vcalChanges(int);
 	void injectModeChanges();
	void mccCalDelayChanges(int);
	void rodCalDelayChanges(int);
	void mccCalWidthChanges(int);
	void rodBcidOffsetChanges(int);
// 	void injTypeChanges(int);
	void injStepChanges(int);
	void injStartPointChanges(int);
	void rawDataModeChanges();
	void startPreAmpKilledChanges();
	void dataTakingSpeedChanges(int);	
	void injTypeChanges(int);
		
	signals:
	
};

};

#endif

