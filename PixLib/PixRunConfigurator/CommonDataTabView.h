#ifndef _CommonDataTabView_h_
#define _CommonDataTabView_h_

#include <q3frame.h>
#include <qwidget.h>
#include <qtabwidget.h>
#include <qlayout.h>
#include <q3frame.h>
#include <qpushbutton.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <Q3GridLayout>

#include "RunParamTab.h"
#include "DbConnection.h"
#include "Browser.h"
#include "BrowserStateManager.h"
#include "ReadoutEnableTab.h"
#include "RodMonitoringTab.h"
#include "EventSimulationTab.h"
#include "TriggerTab.h"

namespace PixRunConfigurator
{

class Browser;
class RunParamTab;
class ReadoutEnableTab;
class EventSimulationTab;
class RodMonitoringTab;
class TriggerTab;
class BrowserStateManager;

class CommonDataTabView : public QTabWidget
{
	Q_OBJECT

	public:

	CommonDataTabView(QWidget* parent,Browser* root,DbConnection* dbConn,BrowserStateManager* stateManager);
	~CommonDataTabView();

	Browser* getParent() {return this->root;}
	BrowserStateManager* getBrowserStateManager() {return this->stateManager;}
	
	public slots:
		
	void receiveStatus(StatusAction);

	private:

	Q3Frame* runParamFrame;
	Q3GridLayout* checkLayout;
	Browser* root;
	QPushButton* writeConfigButton;
	
	RunParamTab* runParamTab;
	ReadoutEnableTab* readoutEnableTab;
	EventSimulationTab* eventSimulationTab;
	RodMonitoringTab* rodMonitoringTab;
	TriggerTab* triggerTab;

	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	
	PixRunConfig* runConfig;

	Q3VBoxLayout* runParamLayout;

	QPushButton* saveButton;

	BrowserStateManager* stateManager;
	
	void connectAll();
	
	private slots:
	void writeConfig();
	void timerRunDone();
	void timerTrigDone();
    
}; 


};

#endif

