#include "BrowserHandler.h"
#include <ipc/partition.h>
#include <string>

using namespace PixRunConfigurator;

BrowserHandler::BrowserHandler(int argnum,char* args[]) : QApplication(argnum,args)
{
  std::string partitionName;
  for(int i = 1 ; i < argnum ; ++i) {
    if( std::string(args[i]) == "-p") {
      if( i+1 <= argnum ) partitionName = std::string(args[i+1]);
      else {
        std::cerr << "Error: you didn't specify the partition name." << std::endl;
        this->exit(1);
      }
    }
  }

  // We need the partition list in case the argument provided doesn't match an active partition
  std::list<IPCPartition> partList;
  IPCPartition::getPartitions(partList);
	std::list<IPCPartition>::iterator it;

  if(partitionName!="") {
    for (it=partList.begin();it!=partList.end();++it)
      if(it->name() == partitionName) { break; } // The partition provided is active
  } else {
    for (it=partList.begin();it!=partList.end();++it)
      if(it->name().compare(0,3, "Pix")==0) // Find a partition name starting with Pix
        if(partitionName=="") partitionName = it->name();
        else { partitionName = ""; break; } // We have more than one choice ; let the user pick!
  }

  if(partitionName != "") std::cout << "Partition name: " << partitionName << std::endl;
  browser=new Browser();
  browser->partitionName = QString(partitionName.c_str());
  start();
}

void BrowserHandler::start()
{
	browser->init();
	connect(browser,SIGNAL(exitBrowserSignal(bool)),this,SLOT(closeApp(bool)));
	connect(browser,SIGNAL(exitBrowserTagSignal(std::string)),this,SLOT(getTagWindow(std::string)));
}

void BrowserHandler::closeApp(bool b)
{
	if (b)
		this->exit(0);
	else
	{
		disconnect(browser,SIGNAL(exitBrowserSignal(bool)),this,SLOT(closeApp(bool)));
		browser->hide();
		start();
	}
}

void BrowserHandler::getTagWindow(std::string partitionname)
{
 	disconnect(browser,SIGNAL(exitBrowserTagSignal(std::string)),this,SLOT(getTagWindow(std::string)));
 	browser->hide();
 	browser->initTag(partitionname);
 	connect(browser,SIGNAL(exitBrowserSignal(bool)),this,SLOT(closeApp(bool)));
 	connect(browser,SIGNAL(exitBrowserTagSignal(std::string)),this,SLOT(getTagWindow(std::string)));
}
