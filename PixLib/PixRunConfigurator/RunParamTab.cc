#include "RunParamTab.h"

#include <qfont.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3HBoxLayout>
#include <Q3GridLayout>
#include <QLabel>
#include <Q3VBoxLayout>

#include <iostream>

using namespace PixRunConfigurator;

RunParamTab::RunParamTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig)
			  : View(stateManager,parent,QString("obj_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=runConfig;
	
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);
	
	detailLayout=new Q3GridLayout(revLayout,19,7);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(2,5);
	detailLayout->setColSpacing(3,10);
	detailLayout->setColSpacing(5,5);
	detailLayout->setColSpacing(6,20);
	detailLayout->setColSpacing(2,20);
	detailLayout->setRowSpacing(0,50);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(6,5);
	detailLayout->setRowSpacing(8,5);
	detailLayout->setRowSpacing(10,5);
	detailLayout->setRowSpacing(12,5);
	detailLayout->setRowSpacing(14,5);
	detailLayout->setRowSpacing(16,5);
	detailLayout->setRowSpacing(18,50);
	
	internalGen=new QLabel("Internal fragments generation:",this);
	detailLayout->addWidget(internalGen,1,1);	
	consecutiveLVL1=new QLabel("ConsecutiveLVL1:",this);
	detailLayout->addWidget(consecutiveLVL1,3,1);	
	latency=new QLabel("Latency:",this);
	detailLayout->addWidget(latency,5,1);	
	nPixInject=new QLabel("Number of strobed pixels per module:",this);
	detailLayout->addWidget(nPixInject,7,1);
	digitalInject=new QLabel("Digital injection:",this);
	detailLayout->addWidget(digitalInject,9,1);
 	cInjHi=new QLabel("Use High injection cap:",this);
 	detailLayout->addWidget(cInjHi,11,1);
 	vcal=new QLabel("VCAL value:",this);
 	detailLayout->addWidget(vcal,13,1);
	injectMode=new QLabel("Enable the MCC:",this);
	detailLayout->addWidget(injectMode,15,1);
	mccCalDelay=new QLabel("MCC cal Delay:",this);
	detailLayout->addWidget(mccCalDelay,17,1);

	internalGen_edit=new QSpinBox(this);
	detailLayout->addWidget(internalGen_edit,1,2);
	internalGen_edit->setMaximumWidth(80);
	internalGen_edit->setMaxValue(999);
	consecutiveLVL1_edit=new QSpinBox(this);
	detailLayout->addWidget(consecutiveLVL1_edit,3,2);
	consecutiveLVL1_edit->setMaximumWidth(80);
	consecutiveLVL1_edit->setMaxValue(999);
	latency_edit=new QSpinBox(this);
	detailLayout->addWidget(latency_edit,5,2);
	latency_edit->setMaximumWidth(80);
	latency_edit->setMaxValue(999);
	nPixInject_edit=new QSpinBox(this);
	detailLayout->addWidget(nPixInject_edit,7,2);
	nPixInject_edit->setMaximumWidth(80);	
	nPixInject_edit->setMaxValue(999);
	digitalInject_edit=new QPushButton(this);
	detailLayout->addWidget(digitalInject_edit,9,2);
	digitalInject_edit->setMaximumWidth(80);
	cInjHi_edit=new QPushButton(this);
	detailLayout->addWidget(cInjHi_edit,11,2);
	cInjHi_edit->setMaximumWidth(80);
	vcal_edit=new QSpinBox(this);
	detailLayout->addWidget(vcal_edit,13,2);
	vcal_edit->setMaximumWidth(80);
	vcal_edit->setMaxValue(999);
	injectMode_edit=new QPushButton(this);
	detailLayout->addWidget(injectMode_edit,15,2);
	injectMode_edit->setMaximumWidth(80);
	mccCalDelay_edit=new QSpinBox(this);
	detailLayout->addWidget(mccCalDelay_edit,17,2);
	mccCalDelay_edit->setMaximumWidth(80);	
	mccCalDelay_edit->setMaxValue(999);
	
	rodCalDelay=new QLabel("ROD cal Delay:",this);
	detailLayout->addWidget(rodCalDelay,1,4);
	mccCalWidth=new QLabel("MCC CAL pulse length:",this);
	detailLayout->addWidget(mccCalWidth,3,4);
	rodBcidOffset=new QLabel("ROD Bcid offset:",this);
	detailLayout->addWidget(rodBcidOffset,5,4);
	injType=new QLabel("Injection Strategy:",this);
	detailLayout->addWidget(injType,7,4);
	injStep=new QLabel("Number of step in regular injection:",this);
	detailLayout->addWidget(injStep,9,4);
	injStartPoint=new QLabel("Starting point of regular injection:",this);
	detailLayout->addWidget(injStartPoint,11,4);
	rawDataMode=new QLabel("Enables RAW data mode during data taking:",this);
	detailLayout->addWidget(rawDataMode,13,4);
	startPreAmpKilled=new QLabel("Pre-Amp killed at run start:",this);
	detailLayout->addWidget(startPreAmpKilled,15,4);
	dataTakingSpeed=new QLabel("Data taking speed:",this);
	detailLayout->addWidget(dataTakingSpeed,17,4);	

	rodCalDelay_edit=new QSpinBox(this);
	detailLayout->addWidget(rodCalDelay_edit,1,5);
	rodCalDelay_edit->setMaximumWidth(80);
	rodCalDelay_edit->setMaxValue(999);
	mccCalWidth_edit=new QSpinBox(this);
	detailLayout->addWidget(mccCalWidth_edit,3,5);
	mccCalWidth_edit->setMaximumWidth(80);
	mccCalWidth_edit->setMaxValue(999);
	rodBcidOffset_edit=new QSpinBox(this);
	detailLayout->addWidget(rodBcidOffset_edit,5,5);
	rodBcidOffset_edit->setMaximumWidth(80);
	rodBcidOffset_edit->setMaxValue(999);
	injType_edit=new QComboBox(this);
	detailLayout->addWidget(injType_edit,7,5);
 	injType_edit->setMaximumWidth(80);	
	injStep_edit=new QSpinBox(this);
	detailLayout->addWidget(injStep_edit,9,5);
	injStep_edit->setMaximumWidth(80);
	injStep_edit->setMaxValue(999);
	injStartPoint_edit=new QSpinBox(this);
	detailLayout->addWidget(injStartPoint_edit,11,5);
	injStartPoint_edit->setMaximumWidth(80);
	injStartPoint_edit->setMaxValue(999);
	rawDataMode_edit=new QPushButton(this);
	detailLayout->addWidget(rawDataMode_edit,13,5);
	rawDataMode_edit->setMaximumWidth(80);
	startPreAmpKilled_edit=new QPushButton(this);
	detailLayout->addWidget(startPreAmpKilled_edit,15,5);
	startPreAmpKilled_edit->setMaximumWidth(80);
	dataTakingSpeed_edit=new QSpinBox(this);
	detailLayout->addWidget(dataTakingSpeed_edit,17,5);
	dataTakingSpeed_edit->setMaximumWidth(80);
	dataTakingSpeed_edit->setMaxValue(999);
	
	setEnabled(true);
}

RunParamTab::~RunParamTab()
{
	disconnectAll();
	
	delete layout;
	delete revLayout;
	delete detailLayout;
}

void RunParamTab::connectAll()
{
 	connect(internalGen_edit,SIGNAL(valueChanged(int)),this,SLOT(internalGenChanges(int)));
 	connect(consecutiveLVL1_edit,SIGNAL(valueChanged(int)),this,SLOT(consecutiveLVL1Changes(int)));
 	connect(latency_edit,SIGNAL(valueChanged(int)),this,SLOT(latencyChanges(int)));  
 	connect(nPixInject_edit,SIGNAL(valueChanged(int)),this,SLOT(nPixInjectChanges(int)));  
 	connect(vcal_edit,SIGNAL(valueChanged(int)),this,SLOT(vcalChanges(int)));  
 	connect(mccCalDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(mccCalDelayChanges(int)));  
 	connect(rodCalDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(rodCalDelayChanges(int)));  
 	connect(mccCalWidth_edit,SIGNAL(valueChanged(int)),this,SLOT(mccCalWidthChanges(int)));  
 	connect(rodBcidOffset_edit,SIGNAL(valueChanged(int)),this,SLOT(rodBcidOffsetChanges(int)));  
 	connect(injStep_edit,SIGNAL(valueChanged(int)),this,SLOT(injStepChanges(int)));  
 	connect(injStartPoint_edit,SIGNAL(valueChanged(int)),this,SLOT(injStartPointChanges(int)));  
 	connect(dataTakingSpeed_edit,SIGNAL(valueChanged(int)),this,SLOT(dataTakingSpeedChanges(int)));  
	connect(digitalInject_edit,SIGNAL(clicked()),this,SLOT(digitalInjectChanges()));
	connect(cInjHi_edit,SIGNAL(clicked()),this,SLOT(cInjHiChanges()));
	connect(injectMode_edit,SIGNAL(clicked()),this,SLOT(injectModeChanges()));
	connect(rawDataMode_edit,SIGNAL(clicked()),this,SLOT(rawDataModeChanges()));
	connect(startPreAmpKilled_edit,SIGNAL(clicked()),this,SLOT(startPreAmpKilledChanges()));
	connect(injType_edit,SIGNAL(activated(int)),this,SLOT(injTypeChanges(int)));
}

void RunParamTab::injTypeChanges(int index)
{
	if(injType_edit->currentItem()!=(int)(runConfig->injType()))
 	      (int&)(runConfig->injType())=injType_edit->currentItem();
       newValues.erase("Injection Strategy");	      
       newValues.insert( pair<std::string,int>("Injection Strategy",(int)(runConfig->injType())));	      
}

void RunParamTab::internalGenChanges(int value)
{
	runConfig->internalGen()=value;
        newValues.erase("Internal fragments generation");	      
        newValues.insert( pair<std::string,int>("Internal fragments generation",runConfig->internalGen()) );
}

void RunParamTab::consecutiveLVL1Changes(int value)
{
	runConfig->consecutiveLVL1()=value;
        newValues.erase("ConsecutiveLVL1");	      
	newValues.insert( pair<std::string,int>("ConsecutiveLVL1",runConfig->consecutiveLVL1()) );
}
	
void RunParamTab::latencyChanges(int value)
{
	runConfig->latency()=value;
        newValues.erase("Latency");	      
	newValues.insert( pair<std::string,int>("Latency",runConfig->latency()) );
}

void RunParamTab::nPixInjectChanges(int value)
{
	runConfig->nPixInject()=value;
        newValues.erase("Number of strobed pixels per module");	      
	newValues.insert( pair<std::string,int>("Number of strobed pixels per module",runConfig->nPixInject()) );
}

void RunParamTab::vcalChanges(int value)
{
	runConfig->vcal()=value;
        newValues.erase("VCAL value");	      
	newValues.insert( pair<std::string,int>("VCAL value",runConfig->vcal()) );
}

void RunParamTab::mccCalDelayChanges(int value)
{
	runConfig->mccCalDelay()=value;
        newValues.erase("MCC cal Delay");	      
	newValues.insert( pair<std::string,int>("MCC cal Delay",runConfig->mccCalDelay()) );
}

void RunParamTab::rodCalDelayChanges(int value)
{
	runConfig->rodCalDelay()=value;
        newValues.erase("ROD cal Delay");	      
	newValues.insert( pair<std::string,int>("ROD cal Delay",runConfig->rodCalDelay()) );
}

void RunParamTab::mccCalWidthChanges(int value)
{
	runConfig->mccCalWidth()=value;
        newValues.erase("MCC cal pulse length");	      
	newValues.insert( pair<std::string,int>("MCC cal pulse length",runConfig->mccCalWidth()) );
}

void RunParamTab::rodBcidOffsetChanges(int value)
{
	runConfig->rodBcidOffset()=value;
        newValues.erase("ROD Bcid offset");	      
	newValues.insert( pair<std::string,int>("ROD Bcid offset",runConfig->rodBcidOffset()) );
}

void RunParamTab::injStepChanges(int value)
{
	runConfig->injStep()=value;
        newValues.erase("Number of step in regular injection");	      
	newValues.insert( pair<std::string,int>("Number of step in regular injection",runConfig->injStep()) );
}

void RunParamTab::injStartPointChanges(int value)
{
	runConfig->injStartPoint()=value;
        newValues.erase("Starting point of regular injection");	      
	newValues.insert( pair<std::string,int>("Starting point of regular injection",runConfig->injStartPoint()) );
}

void RunParamTab::dataTakingSpeedChanges(int value)
{
	runConfig->dataTakingSpeed()=value;
        newValues.erase("Data taking speed");	      
	newValues.insert( pair<std::string,int>("Data taking speed",runConfig->dataTakingSpeed()) );	      
}

void RunParamTab::digitalInjectChanges()
{
        newValues.erase("Digital injection");	      
	if(digitalInject_edit->text()=="On")
	{
	  digitalInject_edit->setText("Off");
	  runConfig->digitalInject()=false;	  
	  newValues.insert( pair<std::string,int>("Digital injection",0) );	      
	}
	else  
	{
	  digitalInject_edit->setText("On");
	  runConfig->digitalInject()=true;	  
	  newValues.insert( pair<std::string,int>("Digital injection",1) );	      
	}
}

void RunParamTab::cInjHiChanges()
{
        newValues.erase("High injection cap");	      
	if(cInjHi_edit->text()=="On")
	{
	  cInjHi_edit->setText("Off");
	  runConfig->cInjHi()=false;	  
	  newValues.insert( pair<std::string,int>("High injection cap",0) );	      
	}
	else  
	{
	  cInjHi_edit->setText("On");
	  runConfig->cInjHi()=true;	  
	  newValues.insert( pair<std::string,int>("High injection cap",1) );	      
	}
}

void RunParamTab::injectModeChanges()
{
        newValues.erase("MCC");	      
	if(injectMode_edit->text()=="On")
	{
	  injectMode_edit->setText("Off");
	  runConfig->injectMode()=false;	  
	  newValues.insert( pair<std::string,int>("MCC",0) );	      
	}
	else  
	{
	  injectMode_edit->setText("On");
	  runConfig->injectMode()=true;	  
	  newValues.insert( pair<std::string,int>("MCC",1) );	      
	}
}

void RunParamTab::rawDataModeChanges()
{
        newValues.erase("RAW data mode during data taking");	      
	if(rawDataMode_edit->text()=="On")
	{
	  rawDataMode_edit->setText("Off");
	  runConfig->rawDataMode()=false;	  
	  newValues.insert( pair<std::string,int>("RAW data mode during data taking",0) );	      
	}
	else  
	{
	  rawDataMode_edit->setText("On");
	  runConfig->rawDataMode()=true;	  
	  newValues.insert( pair<std::string,int>("RAW data mode during data taking",1) );	      
	}
}

void RunParamTab::startPreAmpKilledChanges()
{
        newValues.erase("Pre-Amp killed at run start");	      
	if(startPreAmpKilled_edit->text()=="On")
	{
	  startPreAmpKilled_edit->setText("Off");
	  runConfig->startPreAmpKilled()=false;	  
	  newValues.insert( pair<std::string,int>("Pre-Amp killed at run start",0) );	      
	}
	else  
	{
	  startPreAmpKilled_edit->setText("On");
	  runConfig->startPreAmpKilled()=true;	  
	  newValues.insert( pair<std::string,int>("Pre-Amp killed at run start",1) );	      
	}

}

void RunParamTab::disconnectAll()
{
 	disconnect(internalGen_edit,SIGNAL(valueChanged(int)),this,SLOT(internalGenChanges(int)));
 	disconnect(consecutiveLVL1_edit,SIGNAL(valueChanged(int)),this,SLOT(consecutiveLVL1Changes(int)));
 	disconnect(latency_edit,SIGNAL(valueChanged(int)),this,SLOT(latencyChanges(int)));  
 	disconnect(nPixInject_edit,SIGNAL(valueChanged(int)),this,SLOT(nPixInjectChanges(int)));  
 	disconnect(vcal_edit,SIGNAL(valueChanged(int)),this,SLOT(vcalChanges(int)));  
 	disconnect(mccCalDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(mccCalDelayChanges(int)));  
 	disconnect(rodCalDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(rodCalDelayChanges(int)));  
 	disconnect(mccCalWidth_edit,SIGNAL(valueChanged(int)),this,SLOT(mccCalWidthChanges(int)));  
 	disconnect(rodBcidOffset_edit,SIGNAL(valueChanged(int)),this,SLOT(rodBcidOffsetChanges(int)));  
 	disconnect(injStep_edit,SIGNAL(valueChanged(int)),this,SLOT(injStepChanges(int)));  
 	disconnect(injStartPoint_edit,SIGNAL(valueChanged(int)),this,SLOT(injStartPointChanges(int)));  
 	disconnect(dataTakingSpeed_edit,SIGNAL(valueChanged(int)),this,SLOT(dataTakingSpeedChanges(int))); 
	disconnect(digitalInject_edit,SIGNAL(clicked()),this,SLOT(digitalInjectChanges()));
	disconnect(cInjHi_edit,SIGNAL(clicked()),this,SLOT(cInjHiChanges()));
	disconnect(injectMode_edit,SIGNAL(clicked()),this,SLOT(injectModeChanges()));
	disconnect(rawDataMode_edit,SIGNAL(clicked()),this,SLOT(rawDataModeChanges()));
	disconnect(startPreAmpKilled_edit,SIGNAL(clicked()),this,SLOT(startPreAmpKilledChanges()));	
	disconnect(injType_edit,SIGNAL(activated(int)),this,SLOT(injTypeChanges(int)));	
}

void RunParamTab::receiveStatus(PixRunConfigurator::StatusAction action)
{	
 	if(action==PixRunConfigurator::editRun)
	{
		showObjectDetails();
	}
	
 	if(action==PixRunConfigurator::writeRun)
	{
		stateManager->setRunParamChanges(defaultValues,newValues);
		showObjectDetails();
	}
}

void RunParamTab::showObjectDetails()
{
 	std::string object=stateManager->getCurrentObject();
	runConf=stateManager->getCurrentRunConf();
	runConfig->selectSubConf(object);
	if(runConfig->subConfExists(object))
	{
	      cf = new Config(runConfig->config());
	      internalGen_edit->setValue(runConfig->internalGen());
	      consecutiveLVL1_edit->setValue(runConfig->consecutiveLVL1());
	      latency_edit->setValue(runConfig->latency());
	      nPixInject_edit->setValue(runConfig->nPixInject());
	      vcal_edit->setValue(runConfig->vcal());
	      mccCalDelay_edit->setValue(runConfig->mccCalDelay());
	      rodCalDelay_edit->setValue(runConfig->rodCalDelay());
	      mccCalWidth_edit->setValue(runConfig->mccCalWidth());
	      rodBcidOffset_edit->setValue(runConfig->rodBcidOffset());
	      injStep_edit->setValue(runConfig->injStep());
	      injStartPoint_edit->setValue(runConfig->injStartPoint());
	      dataTakingSpeed_edit->setValue(runConfig->dataTakingSpeed());

	      defaultValues.clear();
	      newValues.clear();

	     defaultValues.insert( pair<std::string,int>("Internal fragments generation",runConfig->internalGen()) );
	     defaultValues.insert( pair<std::string,int>("ConsecutiveLVL1",runConfig->consecutiveLVL1()) );
	     defaultValues.insert( pair<std::string,int>("Latency",runConfig->latency()) );
	     defaultValues.insert( pair<std::string,int>("Number of strobed pixels per module",runConfig->nPixInject()) );
	     defaultValues.insert( pair<std::string,int>("VCAL value",runConfig->vcal()) );
	     defaultValues.insert( pair<std::string,int>("MCC cal Delay",runConfig->mccCalDelay()) );
	     defaultValues.insert( pair<std::string,int>("ROD cal Delay",runConfig->rodCalDelay()) );
	     defaultValues.insert( pair<std::string,int>("MCC cal pulse length",runConfig->mccCalWidth()) );
	     defaultValues.insert( pair<std::string,int>("ROD Bcid offset",runConfig->rodBcidOffset()) );
	     defaultValues.insert( pair<std::string,int>("Number of step in regular injection",runConfig->injStep()) );
	     defaultValues.insert( pair<std::string,int>("Starting point of regular injection",runConfig->injStartPoint()) );
	     defaultValues.insert( pair<std::string,int>("Data taking speed",runConfig->dataTakingSpeed()) );	      
	      
	      if(runConfig->digitalInject())
	      {
		  digitalInject_edit->setText("On");
		  defaultValues.insert( pair<std::string,int>("Digital injection",1) );	      
	      }
	      else
	      {
		  digitalInject_edit->setText("Off");
		  defaultValues.insert( pair<std::string,int>("Digital injection",0) );	      
	      }
	      if(runConfig->cInjHi())
	      {
		  cInjHi_edit->setText("On");
		  defaultValues.insert( pair<std::string,int>("High injection cap",1) );	      
	      }
	      else
	      {
		  cInjHi_edit->setText("Off");
		  defaultValues.insert( pair<std::string,int>("High injection cap",0) );	      
	      }
	      if(runConfig->injectMode())
	      {
		  injectMode_edit->setText("On");
		  defaultValues.insert( pair<std::string,int>("MCC",1) );	      
	      }
	      else
	      {
		  injectMode_edit->setText("Off");
		  defaultValues.insert( pair<std::string,int>("MCC",0) );	      
	      }
	      if(runConfig->rawDataMode())
	      {
		  rawDataMode_edit->setText("On");
		  defaultValues.insert( pair<std::string,int>("RAW data mode during data taking",1) );	      
	      }
	      else
	      {
		  rawDataMode_edit->setText("Off");
		  defaultValues.insert( pair<std::string,int>("RAW data mode during data taking",0) );	      
	      }
	      if(runConfig->startPreAmpKilled())
	      {
		  startPreAmpKilled_edit->setText("On");
		  defaultValues.insert( pair<std::string,int>("Pre-Amp killed at run start",1) );	      
	      }
	      else
	      {
		  startPreAmpKilled_edit->setText("Off");
		  defaultValues.insert( pair<std::string,int>("Pre-Amp killed at run start",0) );	      
	      }

	
	      injType_edit->insertItem("NOINJ");
	      injType_edit->insertItem("RANDOM");
	      injType_edit->insertItem("ROW");
	      injType_edit->insertItem("COLUMN");
	      injType_edit->setCurrentItem((int)(runConfig->injType()));
	      defaultValues.insert( pair<std::string,int>("Injection Strategy",(int)(runConfig->injType())));	      
	}	


		
    	connectAll();
}