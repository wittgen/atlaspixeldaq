#include "Browser.h"
#include "Exception.h"

#include <iostream>

#include <ipc/core.h>
#include <ipc/partition.h>

#include <qlabel.h>
#include <qpushbutton.h>
#include <qpixmap.h>
#include <qmessagebox.h>
#include <q3progressbar.h>
#include <qcursor.h>
#include <qapplication.h>
#include <QDesktopWidget>
//Added by qt3to4:
#include <QCloseEvent>
#include <Q3GridLayout>
#include <Q3PopupMenu>

using namespace PixRunConfigurator;

Browser::Browser()
{
        ipcChooser=nullptr;
	startTimer=new QTimer(this);
	timer=new QTimer(this);
}

Browser::~Browser()
{
	delete ipcChooser;
	delete tagChooser;
	delete connView;
	delete commDataTabView;
	delete dbConn;
}

void Browser::init()
{	
  if( partitionName != "" ) {
    startBrowser(std::string(partitionName.toLatin1().data()));
  } else {
    startPartitionChooser();
  }
	
	layout=new Q3GridLayout(this,2,1);
	layout->setRowSpacing(0,20);
	layout->setRowSpacing(1,500);
	layout->setRowStretch(1,20);
	layout->setMargin(5);	
}

void Browser::startPartitionChooser() {
        if(!ipcChooser) ipcChooser=new PartitionChooser(this);
	
	connect(ipcChooser,SIGNAL(okClicked(std::string)),this,SLOT(startBrowser(std::string)));
	connect(ipcChooser,SIGNAL(cancelClicked()),this,SLOT(close()));
}
void Browser::startBrowser(std::string partitionName)
{	
        this->partitionName=QString(partitionName.c_str());
	this->dbConn=new DbConnection(partitionName,std::string("PixelDbServer"),std::string("PixelRunParams"),std::string("RunParams"));
        dbConn->connect();
	dbConn->loadDbContent();

	if(ipcChooser) {
	  disconnect(ipcChooser,SIGNAL(okClicked(std::string)),this,SLOT(startBrowser(std::string)));
	  disconnect(ipcChooser,SIGNAL(cancelClicked()),this,SLOT(close()));
	  ipcChooser->hide();
	}
	
	startTagWindow();
}

void Browser::initTag(std::string partition)
{	
	layout=new Q3GridLayout(this,2,1);
	layout->setRowSpacing(0,20);
	layout->setRowSpacing(1,500);
	layout->setRowStretch(1,20);
	layout->setMargin(5);	

        this->partitionName=QString(partition.c_str());
	this->dbConn=new DbConnection(partition,std::string("PixelDbServer"),std::string("PixelRunParams"),std::string("RunParams"));
        dbConn->connect();
	dbConn->loadDbContent();

	startTagWindow();	
}

void Browser::startTagWindow()
{
		std::vector<std::string> tmp_idList;
		const std::vector<std::string>& domainList=dbConn->getDomainList();
	
		for (unsigned int i=0;i<domainList.size();i++)
		{
		   if(domainList.at(i).substr(0,14)=="Configuration-")
		     tmp_idList.push_back(domainList.at(i).substr(14,domainList.at(i).length()));
 		   if(domainList.at(i).substr(0,13)=="Connectivity-") 	
 		     tmp_idList.push_back(domainList.at(i).substr(13,domainList.at(i).length()));
		}

		std::vector<std::string> idList;
		std::vector<std::string> tmp_idList2=tmp_idList;
		for (unsigned int i=0;i<tmp_idList.size();i++)
		{
		      for (unsigned int j=0;j<tmp_idList2.size();j++)
		      {
			  if(tmp_idList2.at(j)==tmp_idList.at(i) && j>i)
			      idList.push_back(tmp_idList2.at(j));
		      }		  
		}
	
 	tagChooser=new TagChooser(this,dbConn,idList);
	
	connect(tagChooser,SIGNAL(okClicked(std::string,std::string,std::string)),this,SLOT(startBrowser2(std::string,std::string,std::string)));
	connect(tagChooser,SIGNAL(cancelClicked()),this,SLOT(close()));			
}

void Browser::startBrowser2(std::string id,std::string conn,std::string cfg)
{	
	this->id=id;
	this->conn=conn;
	this->cfg=cfg;
		
        disconnect(tagChooser,SIGNAL(okClicked(std::string,std::string,std::string)),this,SLOT(startBrowser2(std::string,std::string,std::string)));
	disconnect(tagChooser,SIGNAL(cancelClicked()),this,SLOT(close()));
	
	tagChooser->hide();
	
	dbConn->connectPixConn(id,conn,cfg);
	
	delete tagChooser;

	createLayout();
	show();	
	update();  
}	
void Browser::createLayout()
{
	hSplitter=new QSplitter(Qt::Horizontal,this);
	layout->addWidget(hSplitter,1,0);
 	vSplitter=new QSplitter(Qt::Vertical,hSplitter);
	Q3GroupBox* connGroup=new Q3GroupBox(2,Qt::Vertical,vSplitter,"conn_group");
	Q3GroupBox* listGroup=new Q3GroupBox(2,Qt::Vertical,vSplitter,"obj_list_group");
	listGroup->setTitle("Selected object");
	
	stateManager=new BrowserStateManager(this,dbConn);

	stateManager->setCurrentId(id);
	stateManager->setCurrentConn(conn);
	stateManager->setCurrentCfg(cfg);
	
 	connView=new ConnectivityView(connGroup,this,dbConn,stateManager);	
   	objInfo=new ObjectInfo(listGroup,this,dbConn,stateManager);

	
 	commDataTabView=new CommonDataTabView(hSplitter,this,dbConn,stateManager);
	
	createMenus();
	
	setCaption("PixRunConfigurator - "+partitionName+" - "+QString((id+"/"+conn+"/"+cfg).c_str()));
	setIcon(QPixmap("icon_main.gif"));
	
	QSize ds=QApplication::desktop()->size();
	move((ds.width()-1024*1.5)/2,(ds.height()-768*1.5)/2);
	resize(1024*1.5,768*1.5);

}

void Browser::createMenus()
{
	menuBar=new QMenuBar(this);
	
	fileMenu=new Q3PopupMenu(this);
	fileMenu->insertItem("Change partition...",this,SLOT(changePartition()));
	fileMenu->insertItem("Change tags...",this,SLOT(changeTags()));
	fileMenu->insertSeparator();
	fileMenu->insertItem("Exit",this,SLOT(close()));
	menuBar->insertItem("File",fileMenu);
	
	helpMenu=new Q3PopupMenu(this);
	helpMenu->insertItem("About...",this,SLOT(showAboutWindow()));
	menuBar->insertItem("Help",helpMenu);
	
	layout->addWidget(menuBar,0,0);
}



void Browser::changePartition()
{
	if(QMessageBox::question(this,
		"Change partition",
		"Do you really want to change the partition?\nThe current browser instance will be closed!",
		"Ok",
		"Cancel")==0)
	{
	  this->partitionName="";
		closeCheckTimer=new QTimer(this);
		closeCheckTimer->start(200, true);
		connect(closeCheckTimer,SIGNAL(timeout()),this,SLOT(changeBrowser()));
	}
}

void Browser::changeTags()
{
	if(QMessageBox::question(this,
		"Change tags",
		"Do you really want to change tags?\nThe current browser instance will be closed!",
		"Ok",
		"Cancel")==0)
	{
		closeCheckTimer=new QTimer(this);
		closeCheckTimer->start(200, true);
		connect(closeCheckTimer,SIGNAL(timeout()),this,SLOT(changeTagBrowser()));
	}
}

// SLOTS


void Browser::closeIPCChooser()
{
	ipcChooser->hide();
	delete ipcChooser;
}

void Browser::closeEvent(QCloseEvent*)
{
	close();
}

void Browser::close()
{
	closeCheckTimer=new QTimer(this);
	closeCheckTimer->start(200);
	connect(closeCheckTimer,SIGNAL(timeout()),this,SLOT(exitBrowser()));	
}

void Browser::showAboutWindow()
{
	about=new AboutWindow(this);
	connect(about,SIGNAL(windowClosed()),this,SLOT(deleteAboutWindow()));
}

void Browser::deleteAboutWindow()
{
	about->hide();
	disconnect(about,SIGNAL(windowClosed()),this,SLOT(deleteAboutWindow()));
	delete about;
}

void Browser::closeBrowser(bool reallyExit)
{
		emit exitBrowserSignal(reallyExit);
}

void Browser::exitBrowser()
{
	closeBrowser(true);
}

void Browser::changeBrowser()
{
	closeBrowser(false);
}

void Browser::changeTagBrowser()
{
  emit exitBrowserTagSignal(std::string(partitionName.toLatin1().data()));
}
