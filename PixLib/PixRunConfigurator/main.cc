#include "BrowserHandler.h"
#include "Browser.h"

#include <qthread.h>
 
#include <iostream>

#include <ipc/core.h>

using namespace PixRunConfigurator;

int main(int argnum,char** args)
{
	IPCCore::init(argnum,args);
	BrowserHandler* app=new BrowserHandler(argnum,args);
	//	app->setStyle("platinum");
	return app->exec();
}
