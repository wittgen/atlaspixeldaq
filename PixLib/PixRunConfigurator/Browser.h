#ifndef _Browser_h_
#define _Browser_h_

#include <qlayout.h>
#include <q3groupbox.h>
#include <q3listbox.h>
#include <qmenubar.h>
#include <q3popupmenu.h>
#include <q3frame.h>
#include <qsplitter.h>
#include <qtimer.h>
#include <q3groupbox.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>
#include <QCloseEvent>

#include "ConnectivityView.h"
#include "CommonDataTabView.h"
#include "ObjectInfo.h"
#include "DbConnection.h"
#include "PartitionChooser.h"
#include "TagChooser.h"
#include "AboutWindow.h"

#include "BrowserStateManager.h"

namespace PixRunConfigurator
{

class ConnectivityView;
class CommonDataTabView;
class ObjectInfo;
class TagChooser;

class Browser : public QWidget
{
	Q_OBJECT
	
	public:
	
	Browser();
	~Browser();
	
	void init();
	void initTag(std::string partition);
	
	ConnectivityView* getConnectivityView() {return this->connView;}
	CommonDataTabView* getCommonDataTabView() {return this->commDataTabView;}
	
	BrowserStateManager* getBrowserStateManager() {return this->stateManager;}
		
	void closeBrowser(bool reallyExit);	
	void startTagWindow();
	
	QString partitionName;

	protected:
	
	void closeEvent(QCloseEvent*);	
	
	private:
	
	PartitionChooser* ipcChooser;
	TagChooser* tagChooser;
	
	Q3GridLayout* layout;
	Q3GridLayout* layout2;
	Q3VBoxLayout* leftVLayout;
	Q3VBoxLayout* rightVLayout;
	Q3HBoxLayout* mainLayout;
	
	Q3VBoxLayout* connLayout;
	
	Q3Frame* detailFrame;
	
	QSplitter* vSplitter;
	QSplitter* hSplitter;
	
	Q3GroupBox* connBox;
	Q3GroupBox* confBox;
	
	Q3ListBox* partitionList;
	
	QMenuBar* menuBar;
	Q3PopupMenu *fileMenu,*helpMenu;
	
	QTimer* closeCheckTimer;
	QTimer* startTimer;
	QTimer* timer;
	
	ConnectivityView* connView;
	ObjectInfo* objInfo;
	CommonDataTabView* commDataTabView;
	DbConnection* dbConn;
	AboutWindow* about;
	
	BrowserStateManager* stateManager;
	
	std::string id;
	std::string conn;
	std::string cfg;	
		
	void createMenus();
	void createLayout();
	
	signals:
	
	void exitBrowserSignal(bool);
	void exitBrowserTagSignal(std::string);	
	
	private slots:	

	void startBrowser2(std::string id,std::string conn,std::string cfg);
	void closeIPCChooser();
	void changePartition();
	void changeTags();
	void startBrowser(std::string partitionName);
	void close();
	void showAboutWindow();
	void deleteAboutWindow();
	void exitBrowser();
	void changeBrowser();	
	void changeTagBrowser();	
	void startPartitionChooser();	
	
};


};

#endif

