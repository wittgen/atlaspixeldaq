#include "TagChooser.h"

#include <ipc/partition.h>

#include <qlayout.h>
#include <qlabel.h>
//Added by qt3to4:
#include <QPixmap>
#include <Q3GridLayout>
#include <Q3VBoxLayout>
#include <QKeyEvent>
#include <QCloseEvent>

#include <iostream>

using namespace PixRunConfigurator;

TagChooser::TagChooser(QWidget* parent,DbConnection* dbConn,std::vector<std::string>& domainList) : QWidget(parent,"ipc_Chooser",Qt::WType_Dialog | Qt::WShowModal)
{	
	this->domainList=domainList;
	this->dbConn=dbConn;
	makeWindow();
}

void TagChooser::makeWindow()
{
	setCaption("Choose tags");
	Q3VBoxLayout* layout=new Q3VBoxLayout(this);

	Q3GridLayout* grid1=new Q3GridLayout(layout,9,5);
	grid1->addColSpacing(0,10);
	grid1->addColSpacing(2,20);
	grid1->addColSpacing(4,20);
	grid1->addRowSpacing(0,20);
	grid1->addRowSpacing(2,10);
	grid1->addRowSpacing(4,10);
	grid1->addRowSpacing(6,10);
	grid1->addRowSpacing(8,20);	

	QLabel* l2=new QLabel("Please select tags:",this);	
	grid1->addWidget(l2,1,1);
	QLabel* idl=new QLabel("Id ",this);	
	grid1->addWidget(idl,3,1);
	QLabel* connl=new QLabel("Connectivity",this);	
	grid1->addWidget(connl,5,1);
	QLabel* cfgl=new QLabel("Configuration ",this);	
	grid1->addWidget(cfgl,7,1);	
	
	
	Q3GridLayout* grid2=new Q3GridLayout(layout,3,5);
	grid2->addColSpacing(0,10);
	grid2->addColSpacing(2,20);
	grid2->addColSpacing(4,10);
	grid2->addRowSpacing(0,10);
	grid2->addRowSpacing(2,10);
	
	okButton=new QPushButton("Ok",this);
	grid2->addWidget(okButton,1,1);
	cancelButton=new QPushButton("Cancel",this);
	grid2->addWidget(cancelButton,1,3);

	okButton->setEnabled(false);
	
	idList=new QComboBox(this);
	connList=new QComboBox(this);
	cfgList=new QComboBox(this);
	
	grid1->addWidget(idList,3,3);
	idList->setMinimumWidth(200);
	grid1->addWidget(connList,5,3);
	connList->setMinimumWidth(200);
	grid1->addWidget(cfgList,7,3);
	cfgList->setMinimumWidth(200);

	for (unsigned int i=0;i<domainList.size();i++)
	  idList->insertItem(domainList[i].c_str());
	idList->setCurrentItem(-1);
	idList->setEnabled(true);
	
	connect(cancelButton,SIGNAL(clicked()),this,SLOT(cancel()));
   	connect(idList,SIGNAL(activated(const QString&)),this,SLOT(setConnectList(const QString&)));
	
	setIcon(QPixmap("icon_main.gif"));
	
	setMinimumSize(QSize(450,250));
	setMaximumSize(QSize(450,250));
	
	setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
		
	show();
}

void TagChooser::setConnectList(const QString&)
{	
	part=idList->currentText();
	std::vector<std::string> connectList=dbConn->getTagList(std::string(("Connectivity-"+part).toLatin1().data()));
	connList->clear();
	for (unsigned int i=0;i<connectList.size();i++)
	  connList->insertItem(connectList[i].c_str());
	connList->setCurrentItem(-1);
	connList->setEnabled(true);	      
   	connect(connList,SIGNAL(activated(const QString&)),this,SLOT(setCfgList(const QString&)));	
}

void TagChooser::setCfgList(const QString&)
{	
	QString conn=connList->currentText();
	cfgList->clear();
	std::vector<std::string> confList=dbConn->getTagList(std::string(("Configuration-"+part).toLatin1().data()));
	for (unsigned int i=0;i<confList.size();i++)
	  cfgList->insertItem(confList[i].c_str());
	cfgList->setCurrentItem(-1);
	cfgList->setEnabled(true);	      
   	connect(cfgList,SIGNAL(activated(const QString&)),this,SLOT(enableButton()));	
}

void TagChooser::enableButton()
{
	 okButton->setEnabled(true);
	connect(okButton,SIGNAL(clicked()),this,SLOT(ok()));
}

void TagChooser::ok()
{
  emit okClicked(std::string(idList->currentText().toLatin1().data()),
		 std::string(connList->currentText().toLatin1().data()),std::string(cfgList->currentText().toLatin1().data()));
}

void TagChooser::cancel()
{
	emit cancelClicked();
}

void TagChooser::closeEvent(QCloseEvent*)
{
	emit cancelClicked();
}

void TagChooser::keyPressEvent(QKeyEvent* ke)
{
	if (ke->key()==Qt::Key_Return)
		ok();
	if (ke->key()==Qt::Key_Escape)
		cancel();
}

