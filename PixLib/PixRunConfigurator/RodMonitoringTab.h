#ifndef _RodMonitoringTab_h_
#define _RodMonitoringTab_h_

#include <q3groupbox.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
#include <qsplitter.h>
#include <qlineedit.h>
#include <qpushbutton.h>
#include <qspinbox.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <Q3GridLayout>
#include <Q3HBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "CommonDataTabView.h"
#include "View.h"
#include "BrowserStateManager.h"

namespace PixRunConfigurator
{

class CommonDataTabView;

class RodMonitoringTab : public View
{

	Q_OBJECT
	
	public:
	
	RodMonitoringTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig);
	~RodMonitoringTab();
			
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void connectAll();
	void disconnectAll();	
	
	void showObjectDetails();
	
	CommonDataTabView* parent;
	
	std::string runConf;

	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
	
	Q3ListView* revView;
	
	QLabel* AutoDisable;
	QLabel* RodMonPrescale;
	QLabel* OccupancyRate;
	QLabel* BusyRate;
	QLabel* StatusRate;
	QLabel* BuffRate;

	QPushButton* AutoDisable_edit;
	QSpinBox* RodMonPrescale_edit;
	QSpinBox* OccupancyRate_edit;
	QSpinBox* BusyRate_edit;
	QSpinBox* StatusRate_edit;
	QSpinBox* BuffRate_edit;
	
	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	Config* cf;

   	std::map<std::string,int> defaultValues;
   	std::map<std::string,int> newValues;
	
	private slots:
	  
	void autoDisableChanges();
	void rodMonPrescaleChanges(int);
	void occupancyRateChanges(int);
	void busyRateChanges(int);
	void statusRateChanges(int);
	void buffRateChanges(int);	  
		
	signals:
};

};

#endif

