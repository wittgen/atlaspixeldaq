#ifndef _AboutWindow_h_
#define _AboutWindow_h_

#include <q3textbrowser.h>
#include <qmime.h>
//Added by qt3to4:
#include <QCloseEvent>

namespace PixRunConfigurator
{

class AboutWindow : public QWidget
{
	Q_OBJECT
	
	public:
	
	AboutWindow(QWidget* parent);
	~AboutWindow();
	
	private:
	
	Q3TextBrowser* content;
	Q3MimeSourceFactory* mime;
	
	void makeWindow();
	
	protected:
	
	void closeEvent(QCloseEvent*);
	
	signals:
	
	void windowClosed();
};

}

#endif


