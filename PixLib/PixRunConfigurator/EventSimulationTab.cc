#include "EventSimulationTab.h"

#include <qfont.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>
#include <QLabel>

#include <iostream>

using namespace PixRunConfigurator;

EventSimulationTab::EventSimulationTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig)
			  : View(stateManager,parent,QString("readout_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=runConfig;
	
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);
	
	detailLayout=new Q3GridLayout(revLayout,19,4);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(2,5);
	detailLayout->setColSpacing(3,20);
	detailLayout->setRowSpacing(0,50);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(6,5);
	detailLayout->setRowSpacing(8,5);
	detailLayout->setRowSpacing(10,5);
	detailLayout->setRowSpacing(12,5);
	detailLayout->setRowSpacing(14,5);
	detailLayout->setRowSpacing(16,5);
	detailLayout->setRowSpacing(18,50);
	
	SimulationEnabled=new QLabel("Enables/disables the writing of the register:",this);
	detailLayout->addWidget(SimulationEnabled,1,1);	
	SimulatorMode=new QLabel("Single/multiple simulator mode:",this);
	detailLayout->addWidget(SimulatorMode,3,1);	
	LVL1Accept=new QLabel("4-bit Number of LVL1 accept:",this);
	detailLayout->addWidget(LVL1Accept,5,1);	
	SkippedLVL1=new QLabel("4-bit Number of skipped LVL1:",this);
	detailLayout->addWidget(SkippedLVL1,7,1);
	BitFlipMask=new QLabel("3-bit Bit-flip header:",this);
	detailLayout->addWidget(BitFlipMask,9,1);
 	MCCFlags=new QLabel("8-bit MCC flags:",this);
 	detailLayout->addWidget(MCCFlags,11,1);
 	FEFlags=new QLabel("8-bit FE flags:",this);
 	detailLayout->addWidget(FEFlags,13,1);
	FormattersEnaMask=new QLabel("8-bit enable/disable generation in formatters:",this);
	detailLayout->addWidget(FormattersEnaMask,15,1);
	ModuleOccupancy=new QLabel("8-bit occupancy, from 0 to 255 hits per module:",this);
	detailLayout->addWidget(ModuleOccupancy,17,1);

	SimulationEnabled_edit=new QPushButton(this);
	detailLayout->addWidget(SimulationEnabled_edit,1,2);
	SimulationEnabled_edit->setMaximumWidth(80);
	SimulatorMode_edit=new QPushButton(this);
	detailLayout->addWidget(SimulatorMode_edit,3,2);
	SimulatorMode_edit->setMaximumWidth(80);
	LVL1Accept_edit=new QSpinBox(this);
	detailLayout->addWidget(LVL1Accept_edit,5,2);
	LVL1Accept_edit->setMaxValue(999);
	LVL1Accept_edit->setMaximumWidth(80);
	SkippedLVL1_edit=new QSpinBox(this);
	detailLayout->addWidget(SkippedLVL1_edit,7,2);
	SkippedLVL1_edit->setMaximumWidth(80);	
	SkippedLVL1_edit->setMaxValue(999);
	BitFlipMask_edit=new QSpinBox(this);
	detailLayout->addWidget(BitFlipMask_edit,9,2);
	BitFlipMask_edit->setMaximumWidth(80);
	BitFlipMask_edit->setMaxValue(999);
	MCCFlags_edit=new QSpinBox(this);
	detailLayout->addWidget(MCCFlags_edit,11,2);
	MCCFlags_edit->setMaximumWidth(80);
	MCCFlags_edit->setMaxValue(999);
	FEFlags_edit=new QSpinBox(this);
	detailLayout->addWidget(FEFlags_edit,13,2);
	FEFlags_edit->setMaximumWidth(80);
	FEFlags_edit->setMaxValue(999);
	FormattersEnaMask_edit=new QSpinBox(this);
	detailLayout->addWidget(FormattersEnaMask_edit,15,2);
	FormattersEnaMask_edit->setMaximumWidth(80);
	FormattersEnaMask_edit->setMaxValue(999);
	ModuleOccupancy_edit=new QSpinBox(this);
	detailLayout->addWidget(ModuleOccupancy_edit,17,2);
	ModuleOccupancy_edit->setMaximumWidth(80);	
	ModuleOccupancy_edit->setMaxValue(999);
	
	setEnabled(true);
}

EventSimulationTab::~EventSimulationTab()
{

}

void EventSimulationTab::connectAll()
{
 	connect(LVL1Accept_edit,SIGNAL(valueChanged(int)),this,SLOT(LVL1AcceptChanges(int)));  
 	connect(SkippedLVL1_edit,SIGNAL(valueChanged(int)),this,SLOT(SkippedLVL1Changes(int)));  
 	connect(BitFlipMask_edit,SIGNAL(valueChanged(int)),this,SLOT(BitFlipMaskChanges(int)));  
 	connect(MCCFlags_edit,SIGNAL(valueChanged(int)),this,SLOT(MCCFlagsChanges(int)));  
 	connect(FEFlags_edit,SIGNAL(valueChanged(int)),this,SLOT(FEFlagsChanges(int)));  
 	connect(FormattersEnaMask_edit,SIGNAL(valueChanged(int)),this,SLOT(FormattersEnaMaskChanges(int)));  
 	connect(FormattersEnaMask_edit,SIGNAL(valueChanged(int)),this,SLOT(FormattersEnaMaskChanges(int)));  
	connect(ModuleOccupancy_edit,SIGNAL(valueChanged(int)),this,SLOT(ModuleOccupancyChanges(int)));
	connect(SimulationEnabled_edit,SIGNAL(clicked()),this,SLOT(SimulationEnabledChanges()));
	connect(SimulatorMode_edit,SIGNAL(clicked()),this,SLOT(SimulatorModeChanges()));
}

void EventSimulationTab::LVL1AcceptChanges(int value)
{
	runConfig->simLVL1Accept()=value;
        newValues.erase("4-bit Number of LVL1 accept");	      
	newValues.insert( pair<std::string,int>("4-bit Number of LVL1 accept",value) );	      
}	

void EventSimulationTab::SkippedLVL1Changes(int value)
{
	runConfig->simSkippedLVL1()=value;
        newValues.erase("4-bit Number of skipped LVL1");	      
	newValues.insert( pair<std::string,int>("4-bit Number of skipped LVL1",value) );	      
}

void EventSimulationTab::BitFlipMaskChanges(int value)
{
	runConfig->simBitFlip()=value;
        newValues.erase("3-bit Bit-flip header");	      
	newValues.insert( pair<std::string,int>("3-bit Bit-flip header",value) );	      
}

void EventSimulationTab::MCCFlagsChanges(int value)
{
	runConfig->simMCCFlags()=value;
        newValues.erase("8-bit MCC flags");	      
	newValues.insert( pair<std::string,int>("8-bit MCC flags",value) );	      
}

void EventSimulationTab::FEFlagsChanges(int value)
{
	runConfig->simFEFlags()=value;
        newValues.erase("8-bit FE flags");	      
	newValues.insert( pair<std::string,int>("8-bit FE flags",value) );	      
}

void EventSimulationTab::FormattersEnaMaskChanges(int value)
{
	runConfig->simFormattersMask()=value;
        newValues.erase("8-bit enable/disable generation in formatters");	      
	newValues.insert( pair<std::string,int>("8-bit enable/disable generation in formatters",value) );	      
}

void EventSimulationTab::ModuleOccupancyChanges(int value)
{
	runConfig->simModOccupancy()=value;
        newValues.erase("8-bit occupancy");	      
	newValues.insert( pair<std::string,int>("8-bit occupancy",runConfig->simModOccupancy()) );	      
}

void EventSimulationTab::SimulationEnabledChanges()
{
        newValues.erase("Writing of the register");	      
	if(SimulationEnabled_edit->text()=="On")
	{
	  SimulationEnabled_edit->setText("Off");
	  runConfig->simActive()=false;	  
          newValues.insert( pair<std::string,int>("Writing of the register",0) );	      
	}
	else  
	{
	  SimulationEnabled_edit->setText("On");
	  runConfig->simActive()=true;	  
          newValues.insert( pair<std::string,int>("Writing of the register",1) );	      
	}
}

void EventSimulationTab::SimulatorModeChanges()
{
        newValues.erase("Single/multipe simulator mode");	      
	if(SimulatorMode_edit->text()=="On")
	{
	 SimulatorMode_edit->setText("Off");
	  runConfig->simMode()=false;	  
          newValues.insert( pair<std::string,int>("Single/multiple simulator mode",0) );	      
	}
	else  
	{
	  SimulatorMode_edit->setText("On");
	  runConfig->simMode()=true;	  
          newValues.insert( pair<std::string,int>("Single/multiple simulator mode",1) );	      
	}
}

void EventSimulationTab::disconnectAll()
{
	disconnect(LVL1Accept_edit,SIGNAL(valueChanged(int)),this,SLOT(LVL1AcceptChanges(int)));  
 	disconnect(SkippedLVL1_edit,SIGNAL(valueChanged(int)),this,SLOT(SkippedLVL1Changes(int)));  
 	disconnect(BitFlipMask_edit,SIGNAL(valueChanged(int)),this,SLOT(BitFlipMaskChanges(int)));  
 	disconnect(MCCFlags_edit,SIGNAL(valueChanged(int)),this,SLOT(MCCFlagsChanges(int)));  
 	disconnect(FEFlags_edit,SIGNAL(valueChanged(int)),this,SLOT(FEFlagsChanges(int)));  
 	disconnect(FormattersEnaMask_edit,SIGNAL(valueChanged(int)),this,SLOT(FormattersEnaMaskChanges(int)));  
 	disconnect(FormattersEnaMask_edit,SIGNAL(valueChanged(int)),this,SLOT(FormattersEnaMaskChanges(int)));  
	disconnect(ModuleOccupancy_edit,SIGNAL(clicked()),this,SLOT(ModuleOccupancyChanges()));
	disconnect(SimulatorMode_edit,SIGNAL(clicked()),this,SLOT(SimulatorModeChanges()));
}

void EventSimulationTab::receiveStatus(PixRunConfigurator::StatusAction action)
{
 	if(action==PixRunConfigurator::editRun)
	{
		showObjectDetails();
	}
 	if(action==PixRunConfigurator::writeRun)
	{
		stateManager->setEventSimulationChanges(defaultValues,newValues);
		showObjectDetails();
	}	
}

void EventSimulationTab::showObjectDetails()
{

 	std::string object=stateManager->getCurrentObject();
	runConf=stateManager->getCurrentRunConf();
	runConfig->selectSubConf(object);
	if(runConfig->subConfExists(object)){
	cf = new Config(runConfig->config());
	LVL1Accept_edit->setValue(runConfig->simLVL1Accept());
	SkippedLVL1_edit->setValue(runConfig->simSkippedLVL1());
	BitFlipMask_edit->setValue(runConfig->simBitFlip());
	MCCFlags_edit->setValue(runConfig->simMCCFlags());
	FEFlags_edit->setValue(runConfig->simFEFlags());
	FormattersEnaMask_edit->setValue(runConfig->simFormattersMask());
	ModuleOccupancy_edit->setValue(runConfig->simModOccupancy());

        defaultValues.clear();
	newValues.clear();
	
	defaultValues.insert( pair<std::string,int>("4-bit Number of LVL1 accept",runConfig->simLVL1Accept()) );	      
	defaultValues.insert( pair<std::string,int>("4-bit Number of skipped LVL1",runConfig->simSkippedLVL1()) );	      
	defaultValues.insert( pair<std::string,int>("3-bit Bit-flip header",runConfig->simBitFlip()) );	      
	defaultValues.insert( pair<std::string,int>("8-bit MCC flags",runConfig->simMCCFlags()) );	      
	defaultValues.insert( pair<std::string,int>("8-bit FE flags",runConfig->simFEFlags()) );	      
	defaultValues.insert( pair<std::string,int>("8-bit enable/disable generation in formatters",runConfig->simFormattersMask()) );	      
	defaultValues.insert( pair<std::string,int>("8-bit occupancy",runConfig->simModOccupancy()) );	      
	
	if(runConfig->simActive())
	{
	  SimulationEnabled_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Writing of the register",1) );	      
	}
	else
	{
	  SimulationEnabled_edit->setText("Off");	
          defaultValues.insert( pair<std::string,int>("Writing of the register",0) );	      
	}
	if(runConfig->simMode())
	{
	  SimulatorMode_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Single/multiple simulator mode",1) );	      
	}
	else
	{
	  SimulatorMode_edit->setText("Off");
          defaultValues.insert( pair<std::string,int>("Single/multiple simulator mode",0) );	      
	}
	}	

    	connectAll();
	
}
