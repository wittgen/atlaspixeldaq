#include "ObjectInfo.h"

#include <qfont.h>
#include <qcursor.h>
#include <qmessagebox.h>
#include <qtimer.h>
//Added by qt3to4:
#include <QPixmap>
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>
#include <QLabel>

#include <iostream>

using namespace PixRunConfigurator;

ObjectInfo::ObjectInfo(QWidget* parent,Browser* root,DbConnection* dbConn,BrowserStateManager* stateManager)
			  : View(stateManager,parent,QString("obj_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=dbConn->getPixRunConfig();
 	this->disableConfig=dbConn->getPixDisable();

	disableConfig->put(conn);
	
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);

	detailLayout=new Q3GridLayout(revLayout,10,6);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(1,40);
	detailLayout->setColSpacing(2,20);
	detailLayout->setColSpacing(3,40);
	detailLayout->setColSpacing(4,5);
	detailLayout->setColSpacing(5,20);
	detailLayout->setRowSpacing(0,40);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(3,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(5,60);
	detailLayout->setRowSpacing(7,10);
	
	selObjLbl=new QLabel("  N/A  ",this);
	objTypeLbl=new QLabel("  ---  ",this);
	domainLbl=new QLabel("  ---  ",this);
	statusIcon=new QLabel("",this);

	
	changeStatus= new QPushButton("Change Status",this);
	detailLayout->addMultiCellWidget(changeStatus,6,6,3,4);	
	
	editObject= new QPushButton("Edit RunConfig",this);
	detailLayout->addWidget(editObject,8,1);

	createObject= new QPushButton("Create RunConfig",this);
	detailLayout->addMultiCellWidget(createObject,8,8,3,4);	
	
	changeStatus->setMinimumWidth(140);
	editObject->setMinimumWidth(140);
	createObject->setMinimumWidth(140);
	
	createView();	
	
	setEnabled(false);
}

ObjectInfo::~ObjectInfo()
{	
	delete layout;
	delete revLayout;
	delete detailLayout;
	
	delete selObjLbl;
	delete objTypeLbl;
	delete domainLbl;
}

void ObjectInfo::receiveStatus(PixRunConfigurator::StatusAction action)
{
 	if(action==PixRunConfigurator::statusSet)
	{
		setCursor(Qt::BusyCursor);
		resetView();
		setEnabled(true);

		showObjectDetails();
	}		
}

void ObjectInfo::resetView()
{
	selObjLbl->setText("N/A");
	objTypeLbl->setText("---");
	domainLbl->setText("---");
			
	setEnabled(false);

	disconnect(changeStatus,SIGNAL(clicked()),this,SLOT(setStatus()));
	disconnect(editObject,SIGNAL(clicked()),this,SLOT(setEdit()));
	disconnect(createObject,SIGNAL(clicked()),this,SLOT(addRunConf()));
}



void ObjectInfo::createView()
{
	selObjLbl->setFont(QFont("Arial",18,QFont::Bold));
	detailLayout->addMultiCellWidget(selObjLbl,1,1,1,3);
		
	QLabel* l1=new QLabel("Object type:",this);
	detailLayout->addWidget(l1,3,1);
	detailLayout->addWidget(objTypeLbl,3,3,Qt::AlignRight);
	
	QLabel* l2=new QLabel("Status:",this);
	detailLayout->addWidget(l2,4,1);
	detailLayout->addWidget(domainLbl,4,3,Qt::AlignRight);	
	detailLayout->addWidget(statusIcon,4,4);		
}
void ObjectInfo::showObjectDetails()
{
 	domain=stateManager->getCurrentDomain();
	tag=stateManager->getCurrentTag();
	type=stateManager->getCurrentType();
 	object=stateManager->getCurrentObject();

	statusIcon->clear();
 	selObjLbl->clear();
	objTypeLbl->clear();
	domainLbl->clear();
	
 	selObjLbl->setText(object.c_str());
	objTypeLbl->setText(type.c_str());
	
	if(disableConfig->isDisable(object))
	{
	  changeStatus->setText("Enable object");
	  statusIcon->setPixmap(QPixmap("red.png"));
	  status="disabled";
	}
	else
	{
	  changeStatus->setText("Disable object");		
	  statusIcon->setPixmap(QPixmap("green.png"));
	  status="enabled";
	}

	domainLbl->setText(status.c_str());


      if (runConfig->subConfExists(object) && (type=="Partition" || type=="RodCrate" || type=="RodBoc")) {
	changeStatus->setEnabled(true);
	createObject->setText("Delete RunConfig");
	editObject->setEnabled(true);
        editObject->setText("Edit RunConfig");
      }
      else if (type=="Tim"){
	changeStatus->setEnabled(false);
	createObject->setEnabled(false);
	editObject->setEnabled(true);	
        editObject->setText("Edit TimConfig");
      }
      else if ((!runConfig->subConfExists(object) && (type=="Partition" || type=="RodCrate" || type=="RodBoc"))){
	changeStatus->setEnabled(true);
	createObject->setText("Create RunConfig");
	createObject->setEnabled(true);
	editObject->setEnabled(false);
       }
      else if (type=="Module" || type=="Pp0"){
	changeStatus->setEnabled(true);
	createObject->setEnabled(false);
	editObject->setEnabled(false);
       }       
       
        confExist=(runConfig->subConfExists(object));
	setCursor(Qt::ArrowCursor);

	if((!disableConfig->isDisable(object) && domain=="disabled"))
	{
	  (QMessageBox::information(this, ("Object status",object+" is enabled, but inactive!\n[A parent object is probably disabled]").c_str(), "Ok")==0);	  
    	}	
	
	connect(changeStatus,SIGNAL(clicked()),this,SLOT(setStatus()));
	connect(editObject,SIGNAL(clicked()),this,SLOT(setEdit()));
	connect(createObject,SIGNAL(clicked()),this,SLOT(addRunConf())); 
}


void ObjectInfo::setStatus()
{
	changeStatus->setEnabled(false);
	setCursor(Qt::BusyCursor);

	if(disableConfig->isDisable(object))
	{ 
	  disableConfig->enable(object);
	  if(!disableConfig->isDisable(object))
	  {  
		changeStatus->setText("Disable object");
		status="enabled";
		domainLbl->setText(status.c_str());
		statusIcon->setPixmap(QPixmap("green.png"));
	  }
	  else
	  {
 	  }  
	}
	else
	{
	  disableConfig->disable(object);
	  if(disableConfig->isDisable(object))
	  {
		changeStatus->setText("Enable object");
		status="disabled";	  
		domainLbl->setText(status.c_str());
		statusIcon->setPixmap(QPixmap("red.png"));
	  }
	  else
	  {
	    
	  }
	}
	disableConfig->put(conn); 
	conn->writeDisable(disableConfig, disConf);
 
 	PixRunConfigurator::StatusAction action;
	action=PixRunConfigurator::statusChanged;
  	stateManager->receiveAction(action,"status changed");	
        QTimer *timer = new QTimer(this);
        connect(timer , SIGNAL(timeout()), this, SLOT(timerDone()) );
        timer->start( 1000, TRUE );
}  

void ObjectInfo::setEdit()
{
        editObject->setEnabled(false);
        if (runConfig->subConfExists(object) && (type=="Partition" || type=="RodCrate" || type=="RodBoc"))
	{
	      PixRunConfigurator::StatusAction action;
	      action=PixRunConfigurator::editRun;
	      stateManager->receiveAction(action,"edit runConfig");	
	}

        if (type=="Tim")
	{
	      PixRunConfigurator::StatusAction action;
	      action=PixRunConfigurator::editTim;
	      stateManager->receiveAction(action,"edit timConfig");	
	} 
}  

void ObjectInfo::addRunConf()
{
	createObject->setEnabled(false);
        editObject->setEnabled(false);
 	PixRunConfigurator::StatusAction action;
	setCursor(Qt::BusyCursor);
	if(confExist)
	{
	    createObject->setText("Create RunConfig");
	    runConfig->removeSubConf(object);
	    conn->writeRunConfig(runConfig, "Global");
	    action=PixRunConfigurator::deleteRun;
	    stateManager->receiveAction(action,"status changed");
	}
	else
 	{
	    createObject->setText("Delete RunConfig");
 	    runConfig->addSubConf(object);	    
	    conn->writeRunConfig(runConfig, "Global");
	    action=PixRunConfigurator::addRun;
	    stateManager->receiveAction(action,"status changed");
 	}

        confExist=(runConfig->subConfExists(object));

	action=PixRunConfigurator::statusChanged;
  	stateManager->receiveAction(action,"status changed");

	QTimer *timer = new QTimer(this);
        connect(timer , SIGNAL(timeout()), this, SLOT(timerDone()) );
        timer->start( 1000, TRUE );
}


void ObjectInfo::timerDone()
{
	    changeStatus->setEnabled(true);
	    createObject->setEnabled(true);

	    if(confExist)
	          editObject->setEnabled(true);
	    else
	          editObject->setEnabled(false);
	    
	    setCursor(Qt::ArrowCursor);
} 
