#ifndef _DbConnection_h_
#define _DbConnection_h_

#include <ipc/core.h>

#include <qstring.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbDomain.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixController/PixScan.h"

#include <oks/kernel.h>
#include <oks/object.h>
#include <oks/class.h>
#include <oks/index.h>
#include <oks/attribute.h>
#include <oks/relationship.h>

#include <map>
#include <string>

#include "Object.h"

using namespace PixLib;

class IPCPartition;

namespace PixRunConfigurator
{

struct ModifiedObject
{
	std::string domain,tag,type,name;
	unsigned int revision;
};

class DbConnection
{

	public:
	
	DbConnection(std::string partition,std::string dbServer,std::string isServer,std::string isServer4Disable);
	~DbConnection();
	
	void connect();
	void connectPixConn();
	void connectPixConn(std::string idTag,std::string connTag,std::string cfgTag);
	
	void loadDbContent();
	
	void EnableRem();
	std::vector<std::string> getIdTagList();
	
	const std::vector<std::string>& getDomainList();
	const std::vector<std::string>& getTagList(std::string domName);
	const std::vector<std::string>& getObjectList(std::string domainName,std::string tag);
	const std::vector<std::string>& getObjectList(std::string domainName,std::string tag,std::string type);
	const std::vector<std::pair<std::string,unsigned int> >& getTypes(std::string domain,std::string tag);
	const std::string getPendingTag(std::string domain,std::string tag,std::string type,std::string obj,unsigned int revId);
	
	std::vector<std::string>* getLoadList();
	std::vector<std::string>* getSaveList();
	unsigned int getLoadListLen();
	
	void addLoadList(Object* obj,unsigned int revision);
	
	bool writeLocalFile(std::string fileName);
	
	PixDbServerInterface* getDbServer() {return this->dbServer;}
	PixConnectivity* getPixConnectivity() {return this->pixConn;} 
	PixConnectivity* getdefinedPixConnectivity() {return this->conn;} 
 	PixDisable* getPixDisable() {return this->disableConfig;}
 	PixRunConfig* getPixRunConfig() {return this->runConfig;}
	
	private:
	
	std::string partitionName;
	std::string dbServerName;
	std::string isServerName;
	std::string isServerName4Disable;
	QString object;

 	std::vector<std::string>* part_map;
   	std::multimap<std::string, std::string>* rodcrate_map;
  	std::multimap<std::string, std::string>* tim_map;
 	std::multimap<std::string, std::string>* rodboc_map;
	std::multimap<std::string, std::string>* pp0_map;
	std::multimap<std::string, std::string>* module_map;
	
 	std::vector<std::string>* rodcrate_vector;
  	std::vector<std::string>* tim_vector;
 	std::vector<std::string>* rodboc_vector;
	std::vector<std::string>* pp0_vector;
	std::vector<std::string>* module_vector;

	IPCPartition* partition;
	PixConnectivity* pixConn;
	PixConnectivity* conn;
	PixDbServerInterface* dbServer;
	PixISManager* isManager;
	PixISManager* isManager4Disable;
	PixRunConfig* runConfig;
	PixDisable* disableConfig;	
	
	std::vector<std::string>* domainList;
	std::map<std::string,std::vector<std::string> >* tagList;
	std::vector<std::string>* objList;
	std::vector<std::string>* objTypeList;
	std::vector<std::pair<std::string,std::string> >* objInfoList;
	std::map<std::string,vector<std::string> >* tags;
	std::vector<unsigned int>* revList;
	std::vector<std::pair<std::string,unsigned int> >* typeList;
	std::vector<ModifiedObject>* modObjects;
	
	
};

}

#endif

