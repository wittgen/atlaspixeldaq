#include "View.h"
//Added by qt3to4:
#include <Q3Frame>

using namespace PixRunConfigurator;

View::View(BrowserStateManager* stateManager,QWidget* parent,QString name) : Q3Frame(parent,name) 
{
	this->stateManager=stateManager;
	bind();
}	

void View::bind()
{
	connect(this->stateManager,SIGNAL(trigger(StatusAction)),
		this,SLOT(receiveStatus(StatusAction)));
}

void View::unbind()
{
	disconnect(this->stateManager,SIGNAL(trigger(StatusAction)),
		this,SLOT(receiveStatus(StatusAction)));
}
