#ifndef _EventSimulationTab_h_
#define _EventSimulationTab_h_

#include <q3groupbox.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
#include <qsplitter.h>
#include <qlineedit.h>
#include <qspinbox.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "CommonDataTabView.h"
#include "View.h"
#include "BrowserStateManager.h"

namespace PixRunConfigurator
{

class CommonDataTabView;

class EventSimulationTab : public View
{

	Q_OBJECT
	
	public:
	
	EventSimulationTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig);
	~EventSimulationTab();
		
	void resetView();
	
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void connectAll();
	void disconnectAll();	

	void showObjectDetails();

	std::string runConf;	
	
	CommonDataTabView* parent;
	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
	
	Q3ListView* revView;
	
	QLabel* SimulationEnabled;
	QLabel* SimulatorMode;
	QLabel* LVL1Accept;
	QLabel* SkippedLVL1;
	QLabel* BitFlipMask;
	QLabel* MCCFlags;
	QLabel* FEFlags;
	QLabel* FormattersEnaMask;
	QLabel* ModuleOccupancy;

	QPushButton* SimulationEnabled_edit;
	QPushButton* SimulatorMode_edit;
	QSpinBox* LVL1Accept_edit;
	QSpinBox* SkippedLVL1_edit;
	QSpinBox* BitFlipMask_edit;
	QSpinBox* MCCFlags_edit;
	QSpinBox* FEFlags_edit;
	QSpinBox* FormattersEnaMask_edit;
	QSpinBox* ModuleOccupancy_edit;

	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	
	Config* cf;

   	std::map<std::string,int> defaultValues;
   	std::map<std::string,int> newValues;	
	
	private slots:
	 
	void LVL1AcceptChanges(int);  
 	void SkippedLVL1Changes(int);  
 	void BitFlipMaskChanges(int);  
 	void MCCFlagsChanges(int);  
 	void FEFlagsChanges(int);  
 	void FormattersEnaMaskChanges(int);  
	void ModuleOccupancyChanges(int);
	void SimulationEnabledChanges();
	void SimulatorModeChanges();	  
		
	signals:
};

};

#endif

