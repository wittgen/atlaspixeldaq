#include "CommonDataTabView.h"

#include <iostream>
#include <qcursor.h>
#include <qmessagebox.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3VBoxLayout>
#include <Q3Frame>

using namespace PixRunConfigurator;

CommonDataTabView::CommonDataTabView(QWidget* parent,Browser* root,DbConnection* dbConn,BrowserStateManager* stateManager) : QTabWidget(parent,"common_data_tab_view")
{
	this->dbConn=dbConn;
	this->root=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->stateManager=stateManager;
	this->runConfig=dbConn->getPixRunConfig();
	
 	runParamFrame=new Q3Frame(this,"runparam_frame");
 	runParamLayout=new Q3VBoxLayout(runParamFrame);
 	runParamLayout->setMargin(5);
 	
	runParamTab=new RunParamTab(runParamFrame,this,dbConn,stateManager,runConfig);
	runParamLayout->addWidget(runParamTab);
	

	eventSimulationTab=new EventSimulationTab(this,this,dbConn,stateManager,runConfig);
	readoutEnableTab=new ReadoutEnableTab(this,this,dbConn,stateManager,runConfig);
	rodMonitoringTab=new RodMonitoringTab(this,this,dbConn,stateManager,runConfig);
	triggerTab=new TriggerTab(this,this,dbConn,stateManager,runConfig);
	
	addTab(runParamFrame,"RunParams");
  	addTab(readoutEnableTab,"ModGroups");
  	addTab(eventSimulationTab,"EventSimulation");
  	addTab(rodMonitoringTab,"RodMonitoring");
   	addTab(triggerTab,"Trigger");	

	setTabEnabled(runParamFrame,false);
	setTabEnabled(readoutEnableTab,false);
	setTabEnabled(eventSimulationTab,false);
	setTabEnabled(rodMonitoringTab,false);
	setTabEnabled(triggerTab,false);
	this->setCurrentPage(0);

	checkLayout=new Q3GridLayout(this,3,3);
	checkLayout->setColSpacing(0,50);
	checkLayout->setRowSpacing(0,900);
	checkLayout->setRowSpacing(2,10);
	checkLayout->setColSpacing(2,50);	
	
	writeConfigButton=new QPushButton(this);
	checkLayout->addWidget(writeConfigButton,1,1);	
	writeConfigButton->setText("Save Changes");
	writeConfigButton->setEnabled(false);

	disconnect(writeConfigButton,SIGNAL(clicked()),this,SLOT(writeConfig()));
	
	setMaximumWidth(975);

	connectAll();
}

CommonDataTabView::~CommonDataTabView()
{
}

void CommonDataTabView::writeConfig()
{
	if(stateManager->getCurrentType()=="Tim")
	{
	PixRunConfigurator::StatusAction action;	
 	action=PixRunConfigurator::writeTim;
   	stateManager->receiveAction(action,"timconfig written");

        QTimer *timer = new QTimer(this);
        connect(timer , SIGNAL(timeout()), this, SLOT(timerTrigDone()) );
        timer->start( 500, TRUE );	
	}
	else
	{
	conn->writeRunConfig(runConfig, "Global");
	
 	PixRunConfigurator::StatusAction action;	
 	action=PixRunConfigurator::writeRun;
   	stateManager->receiveAction(action,"runconfig written");	

        QTimer *timer = new QTimer(this);
        connect(timer , SIGNAL(timeout()), this, SLOT(timerRunDone()) );
        timer->start( 500, TRUE );	
	}
}

void CommonDataTabView::receiveStatus(PixRunConfigurator::StatusAction action)
{
        setCursor(Qt::BusyCursor);

 	if(action==PixRunConfigurator::statusSet || action==PixRunConfigurator::deleteRun)
	{
	disconnect(writeConfigButton,SIGNAL(clicked()),this,SLOT(writeConfig()));
	setTabEnabled(runParamFrame,false);
	setTabEnabled(readoutEnableTab,false);
	setTabEnabled(eventSimulationTab,false);
	setTabEnabled(rodMonitoringTab,false);
	setTabEnabled(triggerTab,false);
	writeConfigButton->setEnabled(false);
	this->setCurrentPage(0);
	}  
	if(action==PixRunConfigurator::editRun)
	{
	connect(writeConfigButton,SIGNAL(clicked()),this,SLOT(writeConfig()));
	setTabEnabled(runParamFrame,true);
	setTabEnabled(readoutEnableTab,true);
	setTabEnabled(eventSimulationTab,true);
	setTabEnabled(rodMonitoringTab,true);
	writeConfigButton->setEnabled(true);
	}

 	if(action==PixRunConfigurator::editTim)
	{
	connect(writeConfigButton,SIGNAL(clicked()),this,SLOT(writeConfig()));
	setTabEnabled(triggerTab,true);
	writeConfigButton->setEnabled(true);
	this->setCurrentPage(4);
	}		

	setCursor(Qt::ArrowCursor);
}

void CommonDataTabView::connectAll()
{
	connect(stateManager,SIGNAL(trigger(StatusAction)),this,SLOT(receiveStatus(StatusAction)));
}  

void CommonDataTabView::timerRunDone()
{
  QString mess = stateManager->getConfigChanges().c_str();
  (QMessageBox::information(this, "Changes in RunConfig", mess, "Ok")==0);	
  PixRunConfigurator::StatusAction action;	
  action=PixRunConfigurator::statusChanged;
  stateManager->receiveAction(action,"status changed");     
}

void CommonDataTabView::timerTrigDone()
{
  QString mess = stateManager->getTimChanges().c_str();
  (QMessageBox::information(this, "Changes in TimConfig", mess, "Ok")==0);	  
  PixRunConfigurator::StatusAction action;	
  action=PixRunConfigurator::statusChanged;
  stateManager->receiveAction(action,"status changed");   
}
