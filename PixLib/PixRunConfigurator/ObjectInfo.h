#ifndef _ObjectInfo_h_
#define _ObjectInfo_h_

#include <q3groupbox.h>
#include <qpushbutton.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <Q3GridLayout>
#include <Q3HBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "Browser.h"
#include "View.h"
#include "BrowserStateManager.h"

#include "PixConnectivity/PixDisable.h"

namespace PixRunConfigurator
{

class Browser;

class ObjectInfo : public View
{

	Q_OBJECT
	
	public:
	
	ObjectInfo(QWidget* parent,Browser* root,DbConnection* dbConn,BrowserStateManager* stateManager);
	~ObjectInfo();
	
	void resetView();
	
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void showObjectDetails();
	
	
	Browser* parent;
	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
	
	QLabel* selObjLbl;
	QLabel* objTypeLbl;
	QLabel* domainLbl;
	QLabel* statusIcon;
	
	QPushButton* changeStatus;
	QPushButton* editObject;
	QPushButton* createObject;
		
	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixDisable* disableConfig;
	PixRunConfig* runConfig;

	std::string domain;
	std::string status;
	std::string tag;
	std::string type;
 	std::string object;
	std::string disConf;
	bool confExist;
		
	void createView();
	void disconnectAll(){};
	
	private slots:

	void setStatus();
	void setEdit();
	void addRunConf();
	void connectAll(){};
	void timerDone();
	
	signals:
	
};

};

#endif

