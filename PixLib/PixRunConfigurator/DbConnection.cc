#include "DbConnection.h"
#include "Exception.h"

#include <iostream>


using namespace PixRunConfigurator;

DbConnection::DbConnection(std::string partition,std::string dbServer,std::string isServer,std::string isServer4Disable) : partitionName(partition),dbServerName(dbServer),isServerName(isServer),isServerName4Disable(isServer4Disable)
{
	domainList=new std::vector<std::string>;
	tagList=new std::map<std::string,vector<std::string> >;
	objList=new std::vector<std::string>;
	objInfoList=new std::vector<std::pair<std::string,std::string> >;
	objList=new std::vector<std::string>;
	typeList=new std::vector<std::pair<std::string,unsigned int> >;
	revList=new std::vector<unsigned int>;
	modObjects=new std::vector<ModifiedObject>;
 	part_map=new std::vector<std::string>;
   	rodcrate_map=new std::multimap<std::string, std::string>;
   	tim_map=new std::multimap<std::string, std::string>;
   	rodboc_map=new std::multimap<std::string, std::string>;
   	pp0_map=new std::multimap<std::string, std::string>;
   	module_map=new std::multimap<std::string, std::string>;
}

DbConnection::~DbConnection()
{
	delete domainList;
	delete tags;
	delete partition;
	delete dbServer;
	delete isManager;
	delete pixConn;
}

void DbConnection::loadDbContent()
{
    domainList->clear();
    tagList->clear();    
    objInfoList->clear();
    
    dbServer->setReadEnableRem(true);    
    dbServer->listDomainRem(*domainList);
    
    for (unsigned int i=0;i<domainList->size();i++)
    {
    	std::vector<std::string> tmpList;
    	dbServer->listTagsRem(domainList->at(i),tmpList);
    	tagList->insert(std::make_pair(domainList->at(i),tmpList));
    }
}

void DbConnection::EnableRem()
{
	dbServer->setReadEnableRem(true);
}

const std::vector<std::string>& DbConnection::getDomainList()
{
	return *domainList;
}

const std::vector<std::string>& DbConnection::getTagList(std::string domName)
{
	std::map<std::string,vector<std::string> >::iterator it=tagList->find(domName);
	return it->second;
}

const std::vector<std::string>& DbConnection::getObjectList(std::string domainName,std::string tag)
{
	objList->clear();
	dbServer->listObjectsRem(domainName,tag,*objList);
	return *objList;
}

const std::vector<std::string>& DbConnection::getObjectList(std::string domainName,std::string tag,std::string type)
{
	objList->clear();
	dbServer->listObjectsRem(domainName,tag,type,*objList);
	return *objList;
}


const std::vector<std::pair<std::string,unsigned int> >& DbConnection::getTypes(std::string domainName,std::string tag)
{
	typeList->clear();
	std::vector<std::string> tmpStrList;
	std::vector<unsigned int> tmpCount;
	dbServer->listTypes(domainName,tag,tmpStrList,tmpCount);
	for (unsigned int i=0;i<tmpStrList.size();i++)
	{
		std::pair<std::string,unsigned int> tmpPair;
		tmpPair.first=tmpStrList[i];
		tmpPair.second=tmpCount[i];
		typeList->push_back(tmpPair);
	}
	return *typeList;
}

std::vector<std::string>* DbConnection::getLoadList()
{
	std::vector<std::string>* ret=new std::vector<std::string>;
	dbServer->listLoadList(*ret);
	return ret;
}

std::vector<std::string>* DbConnection::getSaveList()
{
	std::vector<std::string>* ret=new std::vector<std::string>;
	dbServer->listSaveList(*ret);
	return ret;
}

void DbConnection::addLoadList(Object* obj,unsigned int revision)
{
	PixDbDomain::obj_id objId;
	objId.name=obj->getName();
	objId.tag=obj->getTag();
	objId.domain=obj->getDomain();
	objId.type=obj->getType();
	objId.rev=revision;
	dbServer->addLoadList(objId);
}

unsigned int DbConnection::getLoadListLen()
{
	return dbServer->getLoadListLen();
}

bool DbConnection::writeLocalFile(std::string fileName)
{
	return dbServer->writeInFile(fileName);
}

std::vector<std::string> DbConnection::getIdTagList()
{
	return pixConn->listConnTagsOI();
}


void DbConnection::connect() 
{
	try 
	{
		partition = new IPCPartition(partitionName.c_str());
	}
	catch(...)
	{
		throw Exception("Error while connecting to IPC partition!",Exception::Fatal);
	}
	bool ready=false;
	int nTry=0;
	
	do
	{
		sleep(1);
		dbServer=new PixDbServerInterface(partition,dbServerName.c_str(),PixDbServerInterface::CLIENT, ready);
	
    		if(!ready)
    			delete dbServer;
    		else
    			break;
		nTry++;
	} 
	while(nTry<5);  
	if(!ready)
		throw (Exception("Error while connecting to DbServer!",Exception::Fatal));

	try
	{
		isManager = new PixISManager(partition, isServerName.c_str());
	}  
	catch(PixISManagerExc e)
	{ 
		throw (Exception(std::string("Error from PixISManager!"),Exception::Fatal));
	}
	catch(...)
	{ 
		throw (Exception(std::string("Error while creating ISManager fro IsServer ")+isServerName,Exception::Fatal));

	}
	

	try
	{
		isManager4Disable = new PixISManager(partition, isServerName4Disable.c_str());
	}  
	catch(PixISManagerExc e)
	{ 
		throw (Exception(std::string("Error from PixISManager4Disable!"),Exception::Fatal));
	}
	catch(...)
	{ 
		throw (Exception(std::string("Error while creating ISManager fro IsServer ")+isServerName4Disable,Exception::Fatal));

	}	
}

void DbConnection::connectPixConn()
{
	try
	{
		pixConn = new PixConnectivity("Base","Base","Base","Base","Base");
	}
	catch (...)
	{
		throw (Exception(std::string("Error while connecting to PixConnectivity!"),Exception::Fatal));
	}
}

void DbConnection::connectPixConn(std::string idTag,std::string connTag,std::string cfgTag)
{
 	string cfgModTag = cfgTag;
  	try
	{
	      conn = new PixConnectivity(dbServer, connTag, connTag, connTag, cfgTag, idTag, cfgModTag);
	}
	catch (...)
	{
		throw (Exception(std::string("Error while connecting to PixConnectivity!"),Exception::Fatal));
	}

	try
	{
		conn->loadConn();
	}
	catch (...)
	{
		throw (Exception(std::string("Error while reading to PixConnectivity!"),Exception::Fatal));
	}	
	
	std::vector<std::string> tagCfglist;
	dbServer->listTagsRem("Configuration-"+idTag,tagCfglist);
	bool foundCfg=false;
	bool foundCfgMod=false;
	for (unsigned int i=0; i<tagCfglist.size(); i++)
	{
		if (tagCfglist[i]==cfgTag) foundCfg=true;
		if (tagCfglist[i]==cfgModTag) foundCfgMod=true;
	}
	if (!foundCfg)
	{
		throw (Exception(std::string("Chosen cfgTag not found in the DbServer!"),Exception::Fatal));
	}
	if (!foundCfgMod)
	{
		throw (Exception(std::string("Chosen cfgModTag not found in the DbServer!"),Exception::Fatal));
	}	

	std::string runConf = "Global";  
	runConfig = new PixRunConfig(runConf);
	conn->setCfgTag(cfgTag);  
	try
	{
	  conn->readRunConfig(runConfig, runConf);
	}
	catch (...)
	{
		std::cout << "PixRunConfig " << runConf << " probably does not exist in DbServer!!!" << std::endl;
		if (runConf=="Global")
		{
		  std::cout << "Creating PixRunConfig: Global" << std::endl;
		  conn->writeRunConfig(runConfig, runConf);
		}
		else exit(-1);
	}

	std::string disConf = "GlobalDis";
	disableConfig = new PixDisable(disConf);
	try
	{
	  conn->readDisable(disableConfig, disConf);
	}
	catch (...)
	{
	      std::cout << "PixDisable " << disConf << " probably does not exist in DbServer!!!" << std::endl;
	      if (disConf=="GlobalDis")
	      {
		std::cout << "Creating PixDisable: GlobalDis" << std::endl;
		conn->writeDisable(disableConfig, disConf);
	      }
	      else exit(-1);
	}
	disableConfig->put(conn);
}