#ifndef _BrowserHandler_h_
#define _BrowserHandler_h_

#include "Browser.h"

#include <qapplication.h>
#include <qstylefactory.h>

namespace PixRunConfigurator
{

class BrowserHandler : public QApplication
{
	Q_OBJECT
	
	public:
	
	BrowserHandler(int argnum,char* args[]);
	
	private:
	
	Browser* browser;
	
	private slots:
	
	void start();
	void closeApp(bool exit);
	void getTagWindow(std::string partitionname);

};

};

#endif

