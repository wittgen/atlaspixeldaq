#include "TriggerTab.h"

#include <qfont.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3HBoxLayout>
#include <Q3GridLayout>
#include <QLabel>
#include <Q3VBoxLayout>

#include <iostream>

using namespace PixRunConfigurator;

TriggerTab::TriggerTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig)
			  : View(stateManager,parent,QString("readout_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=runConfig;
	this->dbServer=dbConn->getDbServer();
	  
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);
	
	detailLayout=new Q3GridLayout(revLayout,19,7);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(2,5);
	detailLayout->setColSpacing(3,10);
	detailLayout->setColSpacing(5,5);
	detailLayout->setColSpacing(6,20);
	detailLayout->setColSpacing(2,20);
	detailLayout->setRowSpacing(0,50);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(6,5);
	detailLayout->setRowSpacing(8,5);
	detailLayout->setRowSpacing(10,5);
	detailLayout->setRowSpacing(12,5);
	detailLayout->setRowSpacing(14,5);
	detailLayout->setRowSpacing(16,5);
	detailLayout->setRowSpacing(18,50);
	
	timTriggerMode=new QLabel("Trigger generation mode:",this);
	detailLayout->addWidget(timTriggerMode,1,1);	
	triggerFrequency=new QLabel("Trigger frequency in kHz:",this);
	detailLayout->addWidget(triggerFrequency,3,1);	
	ECRFrequency=new QLabel("ECR frequency in mHz:",this);
	detailLayout->addWidget(ECRFrequency,5,1);	
	triggerRand2Frequency=new QLabel("Mean randomizer2 trigger frequency in Hz:",this);
	detailLayout->addWidget(triggerRand2Frequency,7,1);
	triggerDelay=new QLabel("Internal trigger delay in BC:",this);
	detailLayout->addWidget(triggerDelay,9,1);
 	ttcrxCoarseDelay=new QLabel("Coarse delay in TTCrx in BC:",this);
 	detailLayout->addWidget(ttcrxCoarseDelay,11,1);
 	ttcrxFineDelay=new QLabel("Fine delay in TTCrx in ns(*10):",this);
 	detailLayout->addWidget(ttcrxFineDelay,13,1);
 	triggerType=new QLabel("Trigger type:",this);
 	detailLayout->addWidget(triggerType,15,1);
	
	
	timTriggerMode_edit=new QComboBox(this);
	detailLayout->addWidget(timTriggerMode_edit,1,2);
	timTriggerMode_edit->setMaximumWidth(250);
	triggerFrequency_edit=new QSpinBox(this);
	detailLayout->addWidget(triggerFrequency_edit,3,2);
	triggerFrequency_edit->setMaximumWidth(80);
	triggerFrequency_edit->setMinValue(1);
	triggerFrequency_edit->setMaxValue(40000);
	ECRFrequency_edit=new QSpinBox(this);
	detailLayout->addWidget(ECRFrequency_edit,5,2);
	ECRFrequency_edit->setMaximumWidth(80);
	ECRFrequency_edit->setMaxValue(60000);
	ECRFrequency_edit->setMinValue(5);
	ECRFrequency_edit->setLineStep(5);
	triggerRand2Frequency_edit=new QSpinBox(this);
	detailLayout->addWidget(triggerRand2Frequency_edit,7,2);
	triggerRand2Frequency_edit->setMaximumWidth(80);	
	triggerRand2Frequency_edit->setMaxValue(150000);
	triggerDelay_edit=new QSpinBox(this);
	detailLayout->addWidget(triggerDelay_edit,9,2);
	triggerDelay_edit->setMaximumWidth(80);
	triggerDelay_edit->setMaxValue(255);
	ttcrxCoarseDelay_edit=new QSpinBox(this);
	detailLayout->addWidget(ttcrxCoarseDelay_edit,11,2);
	ttcrxCoarseDelay_edit->setMaximumWidth(80);
	ttcrxCoarseDelay_edit->setMaxValue(15);
	ttcrxFineDelay_edit=new QSpinBox(this);
	detailLayout->addWidget(ttcrxFineDelay_edit,13,2);
	ttcrxFineDelay_edit->setMaximumWidth(80);
	ttcrxFineDelay_edit->setMaxValue(249);
	triggerType_edit=new QSpinBox(this);
	detailLayout->addWidget(triggerType_edit,15,2);
	triggerType_edit->setMaximumWidth(80);
	triggerType_edit->setMaxValue(4905);

 	BCID_offset=new QLabel("BCID offset in BC:",this);
 	detailLayout->addWidget(BCID_offset,1,4);	
	RodBusyMask=new QLabel("RodBusy mask:",this);
	detailLayout->addWidget(RodBusyMask,3,4);	
/*	Seq_config_file=new QLabel("Sequencer configuration file path:",this);
	detailLayout->addWidget(Seq_config_file,5,4);	*/
	Seq_triggering_mode=new QLabel("Sequencer triggering mode:",this);
	detailLayout->addWidget(Seq_triggering_mode,5,4);	
	timCalInjMode=new QLabel("TIM CAL_INJ mode:",this);
	detailLayout->addWidget(timCalInjMode,7,4);
	CalTrigDelayActiveTIM=new QLabel("Delay between CAL and TRIG in active TIM:",this);
	detailLayout->addWidget(CalTrigDelayActiveTIM,9,4);
 	TrigDelayPassiveTIM=new QLabel("Trigger delay in passive TIM:",this);
 	detailLayout->addWidget(TrigDelayPassiveTIM,11,4);
 	NumOfSubsTriggersPassiveTIM=new QLabel("Number of subsequent triggers in passive TIM:",this);
 	detailLayout->addWidget(NumOfSubsTriggersPassiveTIM,13,4);
	
	BCID_offset_edit=new QSpinBox(this);
	detailLayout->addWidget(BCID_offset_edit,1,5);
	BCID_offset_edit->setMaximumWidth(80);
	BCID_offset_edit->setMaxValue(4905);
	RodBusyMask_edit=new QSpinBox(this);
	detailLayout->addWidget(RodBusyMask_edit,3,5);
	RodBusyMask_edit->setMaximumWidth(80);
	RodBusyMask_edit->setMaxValue(999);
// 	Seq_config_file_edit=new QLineEdit(this);
// 	detailLayout->addWidget(Seq_config_file_edit,5,5);
// 	Seq_config_file_edit->setMaximumWidth(100);
	Seq_triggering_mode_edit=new QComboBox(this);
	detailLayout->addWidget(Seq_triggering_mode_edit,5,5);
	Seq_triggering_mode_edit->setMaximumWidth(250);
	timCalInjMode_edit=new QComboBox(this);
	detailLayout->addWidget(timCalInjMode_edit,7,5);
	timCalInjMode_edit->setMaximumWidth(250);	
	CalTrigDelayActiveTIM_edit=new QSpinBox(this);
	detailLayout->addWidget(CalTrigDelayActiveTIM_edit,9,5);
	CalTrigDelayActiveTIM_edit->setMaximumWidth(80);
	CalTrigDelayActiveTIM_edit->setMaxValue(999);
	TrigDelayPassiveTIM_edit=new QSpinBox(this);
	detailLayout->addWidget(TrigDelayPassiveTIM_edit,11,5);
	TrigDelayPassiveTIM_edit->setMaximumWidth(80);
	TrigDelayPassiveTIM_edit->setMaxValue(999);
	NumOfSubsTriggersPassiveTIM_edit=new QSpinBox(this);
	detailLayout->addWidget(NumOfSubsTriggersPassiveTIM_edit,13,5);
	NumOfSubsTriggersPassiveTIM_edit->setMaximumWidth(80);
	NumOfSubsTriggersPassiveTIM_edit->setMaxValue(999);
	
	setEnabled(true);
	timTriggerMode_edit->setEnabled(false);
	Seq_triggering_mode_edit->setEnabled(false);
	timCalInjMode_edit->setEnabled(false);
}

TriggerTab::~TriggerTab()
{
 	disconnectAll();
	delete layout;	
	delete revLayout;
	delete detailLayout;
}

void TriggerTab::connectAll()
{
 	connect(triggerDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerDelayChanges(int)));
	connect(ttcrxCoarseDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(ttcrxCoarseDelayChanges(int)));
	connect(triggerType_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerTypeChanges(int)));
	connect(BCID_offset_edit,SIGNAL(valueChanged(int)),this,SLOT(BCID_offsetChanges(int)));
	connect(RodBusyMask_edit,SIGNAL(valueChanged(int)),this,SLOT(RodBusyMaskChanges(int)));
	connect(CalTrigDelayActiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(CalTrigDelayActiveTIMChanges(int)));
	connect(TrigDelayPassiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(TrigDelayPassiveTIMChanges(int)));
	connect(NumOfSubsTriggersPassiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(NumOfSubsTriggersPassiveTIMChanges(int)));
 	connect(triggerFrequency_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerFrequencyChanges(int)));
	connect(ECRFrequency_edit,SIGNAL(valueChanged(int)),this,SLOT(ECRFrequencyChanges(int)));
	connect(triggerRand2Frequency_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerRand2FrequencyChanges(int)));	
	connect(ttcrxFineDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(ttcrxFineDelayChanges(int)));	
/*	connect(timTriggerMode_edit,SIGNAL(activated(int)),this,SLOT(timTriggerModeChanges(int)));
	connect(Seq_triggering_mode_edit,SIGNAL(activated(int)),this,SLOT(Seq_triggering_modeChanges(int)));
	connect(timCalInjMode_edit,SIGNAL(activated(int)),this,SLOT(timCalInjModeChanges(int)));*/	
}

void TriggerTab::disconnectAll()
{
	disconnect(triggerDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerDelayChanges(int)));
	disconnect(ttcrxCoarseDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(ttcrxCoarseDelayChanges(int)));
	disconnect(triggerType_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerTypeChanges(int)));
	disconnect(BCID_offset_edit,SIGNAL(valueChanged(int)),this,SLOT(BCID_offsetChanges(int)));
	disconnect(RodBusyMask_edit,SIGNAL(valueChanged(int)),this,SLOT(RodBusyMaskChanges(int)));
	disconnect(CalTrigDelayActiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(CalTrigDelayActiveTIMChanges(int)));
	disconnect(TrigDelayPassiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(TrigDelayPassiveTIMChanges(int)));
	disconnect(NumOfSubsTriggersPassiveTIM_edit,SIGNAL(valueChanged(int)),this,SLOT(NumOfSubsTriggersPassiveTIMChanges(int)));
	disconnect(triggerFrequency_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerFrequencyChanges(int)));
	disconnect(ECRFrequency_edit,SIGNAL(valueChanged(int)),this,SLOT(ECRFrequencyChanges(int)));
	disconnect(triggerRand2Frequency_edit,SIGNAL(valueChanged(int)),this,SLOT(triggerRand2FrequencyChanges(int)));
	disconnect(ttcrxFineDelay_edit,SIGNAL(valueChanged(int)),this,SLOT(ttcrxFineDelayChanges(int)));
/*	disconnect(timTriggerMode_edit,SIGNAL(activated(int)),this,SLOT(timTriggerModeChanges(int)));
	disconnect(Seq_triggering_mode_edit,SIGNAL(activated(int)),this,SLOT(Seq_triggering_modeChanges(int)));
	disconnect(timCalInjMode_edit,SIGNAL(activated(int)),this,SLOT(timCalInjModeChanges(int)));*/	
}

// void TriggerTab::timCalInjModeChanges(int value)
// {
// 	timC->setTriggerFreq((float)value/1000);
// // 
// /*	if(injType_edit->currentItem()!=(int)(runConfig->injType()))
//  	      (int&)(runConfig->injType())=injType_edit->currentItem();
// 	conn->writeRunConfig(runConfig, "Global");*/	
// }	
// 
// void TriggerTab::Seq_triggering_modeChanges(int value)
// {
// 	timC->setTriggerFreq((float)value/1000);
// // 
// /*	if(injType_edit->currentItem()!=(int)(runConfig->injType()))
//  	      (int&)(runConfig->injType())=injType_edit->currentItem();
// 	conn->writeRunConfig(runConfig, "Global");*/	
// }
// 
// void TriggerTab::timCalInjModeChanges(int value)
// {
// 	timC->setTriggerFreq((float)value/1000);
// // 
// /*	if(injType_edit->currentItem()!=(int)(runConfig->injType()))
//  	      (int&)(runConfig->injType())=injType_edit->currentItem();
// 	conn->writeRunConfig(runConfig, "Global");*/	
// }

void TriggerTab::triggerFrequencyChanges(int value)
{
	timC->setTriggerFreq(value);
	newValues.erase("Trigger frequency in kHz");
	newValues.insert( pair<std::string,int>("Trigger frequency in kHz",timC->triggerFreq()) );
}

void TriggerTab::ECRFrequencyChanges(int value)
{
	timC->setECRFreq((float)value/1000.);
	newValues.erase("ECR frequency in mHz");
	newValues.insert( pair<std::string,int>("ECR frequency in mHz",timC->ECRFreq()*1000) );
}

void TriggerTab::triggerRand2FrequencyChanges(int value)
{
	timC->setRandom2Freq((float)value/1000);
	newValues.erase("Mean randomizer2 trigger frequency in Hz");
	newValues.insert( pair<std::string,int>("Mean randomizer2 trigger frequency in Hz",timC->random2Freq()*1000.) );
}

void TriggerTab::ttcrxFineDelayChanges(int value)
{
	timC->ttcrxFineDelay()=(float)value/10;
	newValues.erase("Fine delay in TTCrx in ns(*10)");
	newValues.insert( pair<std::string,int>("Fine delay in TTCrx in ns(*10)",timC->ttcrxFineDelay()*10.) );
}

void TriggerTab::triggerDelayChanges(int value)
{
	timC->setTriggerDelay(value);
	newValues.erase("Internal trigger delay in BC");
	newValues.insert( pair<std::string,int>("Internal trigger delay in BC",timC->triggerDelay()) );
}

void TriggerTab::ttcrxCoarseDelayChanges(int value)
{
	timC->ttcrxCoarseDelay()=value;
	newValues.erase("Coarse delay in TTCrx in BC");
	newValues.insert( pair<std::string,int>("Coarse delay in TTCrx in BC",timC->ttcrxCoarseDelay()) );
}

void TriggerTab::triggerTypeChanges(int value)
{
	timC->triggerType()=value;
	newValues.erase("Trigger type");
	newValues.insert( pair<std::string,int>("Trigger type",timC->triggerType()) );
}

void TriggerTab::BCID_offsetChanges(int value)
{
	timC->bcidOffset()=value;
	newValues.erase("BCID offset in BC");
	newValues.insert( pair<std::string,int>("BCID offset in BC",timC->bcidOffset()) );
}

void TriggerTab::RodBusyMaskChanges(int value)
{
	timC->busyMask()=value;
	newValues.erase("RodBusy mask");
	newValues.insert( pair<std::string,int>("RodBusy mask",timC->busyMask()) );
}

void TriggerTab::CalTrigDelayActiveTIMChanges(int value)
{
	timC->calinjCalTrigDelay()=value;
	newValues.erase("Delay between CAL and TRIG in active TIM");
	newValues.insert( pair<std::string,int>("Delay between CAL and TRIG in active TIM",timC->calinjCalTrigDelay()) );
}

void TriggerTab::TrigDelayPassiveTIMChanges(int value)
{
	timC->calinjTrigDelay()=value;
	newValues.erase("Trigger delay in passive TIM");
	newValues.insert( pair<std::string,int>("Trigger delay in passive TIM",timC->calinjTrigDelay()) );
}

void TriggerTab::NumOfSubsTriggersPassiveTIMChanges(int value)
{
	timC->calTrigNumber()=value;
	newValues.erase("Number of subsequent triggers in passive TIM");
	newValues.insert( pair<std::string,int>("Number of subsequent triggers in passive TIM",timC->calTrigNumber()) );
}

void TriggerTab::receiveStatus(PixRunConfigurator::StatusAction action)
{
 	if(action==PixRunConfigurator::editTim)
	{
		showObjectDetails();
	}		
 	if(action==PixRunConfigurator::writeTim)
	{
		stateManager->setTimChanges(defaultValues,newValues);
		writeConfig();
	}
}

void TriggerTab::writeConfig()
{
 	timC->writeConfig(dbServer, id, cfg, "_Tmp");
}

void TriggerTab::showObjectDetails()
{
	id.clear();
	cfg.clear();
	id="Configuration-";
	id+=stateManager->getCurrentId();  
	cfg=stateManager->getCurrentCfg();  
  	std::string object=stateManager->getCurrentObject();
	timC = new TimPixTrigController(object);
	timC->readConfig(dbServer, id, cfg);
 	Config* cf = timC->config();
	triggerDelay_edit->setValue(timC->triggerDelay());
	ttcrxCoarseDelay_edit->setValue(timC->ttcrxCoarseDelay());
	triggerType_edit->setValue(timC->triggerType());
	BCID_offset_edit->setValue(timC->bcidOffset());
	RodBusyMask_edit->setValue(timC->busyMask());
	CalTrigDelayActiveTIM_edit->setValue(timC->calinjCalTrigDelay());
	TrigDelayPassiveTIM_edit->setValue(timC->calinjTrigDelay());
	NumOfSubsTriggersPassiveTIM_edit->setValue(timC->calTrigNumber());
	triggerFrequency_edit->setValue((int)(timC->triggerFreq()));
	ECRFrequency_edit->setValue((int)(timC->ECRFreq()*1000));
	triggerRand2Frequency_edit->setValue((int)(timC->random2Freq()*1000.));
	ttcrxFineDelay_edit->setValue((int)(timC->ttcrxFineDelay()*10.));

	defaultValues.clear();
	newValues.clear();
	
	
	defaultValues.insert( pair<std::string,int>("Trigger frequency in kHz",timC->triggerFreq()) );
	defaultValues.insert( pair<std::string,int>("ECR frequency in mHz",timC->ECRFreq()*1000) );
	defaultValues.insert( pair<std::string,int>("Mean randomizer2 trigger frequency in Hz",timC->random2Freq()*1000.) );
	defaultValues.insert( pair<std::string,int>("Internal trigger delay in BC",timC->triggerDelay()) );
	defaultValues.insert( pair<std::string,int>("Coarse delay in TTCrx in BC",timC->ttcrxCoarseDelay()) );
	defaultValues.insert( pair<std::string,int>("Fine delay in TTCrx in ns(*10)",timC->ttcrxFineDelay()*10.) );
	defaultValues.insert( pair<std::string,int>("Trigger type",timC->triggerType()) );
	defaultValues.insert( pair<std::string,int>("BCID offset in BC",timC->bcidOffset()) );
	defaultValues.insert( pair<std::string,int>("RodBusy mask",timC->busyMask()) );
	defaultValues.insert( pair<std::string,int>("Delay between CAL and TRIG in active TIM",timC->calinjCalTrigDelay()) );
	defaultValues.insert( pair<std::string,int>("Trigger delay in passive TIM",timC->calinjTrigDelay()) );
	defaultValues.insert( pair<std::string,int>("Number of subsequent triggers in passive TIM",timC->calTrigNumber()) );
	
	timTriggerMode_edit->insertItem("INTERNAL");
	timTriggerMode_edit->insertItem("EXTERNAL_NIM");
	timTriggerMode_edit->insertItem("EXTERNAL_LTP");
	timTriggerMode_edit->insertItem("INT_RANDOM");
	timTriggerMode_edit->insertItem("SEQUENCER");
	timTriggerMode_edit->insertItem("CAL_INJECT");

	Seq_triggering_mode_edit->insertItem("INTTRIGSEQ");
	Seq_triggering_mode_edit->insertItem("EXTTRIGSEQ");
	timCalInjMode_edit->insertItem("ACTIVE");
	timCalInjMode_edit->insertItem("PASSIVE");
	
	connectAll();	
}