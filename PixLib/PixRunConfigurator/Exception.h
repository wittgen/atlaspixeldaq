#ifndef _Exception_h_
#define _Exception_h_

namespace PixRunConfigurator
{



class Exception
{

	public:
	
	enum ErrorType {Fatal=0,Error=1};
	
	Exception(std::string msg,ErrorType type) : msg(msg), type(type) {}
	
	ErrorType errorType() {return type;}
	std::string message() {return msg;}
	
	private:
	
	std::string msg;
	ErrorType type;
};

}

#endif

