#include "BrowserStateManager.h"

using namespace PixRunConfigurator;

BrowserStateManager::BrowserStateManager(QWidget* parent,DbConnection* dbConn) : QObject(parent,"browser_state_manager")
{
	this->object="";
	this->status="";
	this->type="";
	this->statusSet=false;
	this->objectSet=false;
	this->structureSet=false;
	this->dbConn=dbConn;
}

void BrowserStateManager::setCurrentObject(std::string object)
{
	this->object=object;	
	this->statusSet=true;
}

void BrowserStateManager::setCurrentStatus(std::string status)
{
	this->status=status;	
	this->statusSet=true;
}

void BrowserStateManager::receiveAction(StatusAction action,const std::string source)
{
	emit trigger(action);
}


void BrowserStateManager::setCurrentTag(std::string tag)
{
	this->tag=tag;	
	
}

bool  BrowserStateManager::hasStatus()
{
  bool stat = statusSet;
  statusSet = false;
  return stat;
}

void BrowserStateManager::setRunParamChanges(std::map<std::string,int> defaultValues_run,std::map<std::string,int> newValues_run)
{
	this->defaultValues_run=defaultValues_run;	
	this->newValues_run=newValues_run;
	mess_run="";
	
	map<std::string, int>::const_iterator itr;
	for(itr = newValues_run.begin(); itr != newValues_run.end(); ++itr)
	{
	      std::stringstream out;
	      out << defaultValues_run[(*itr).first];	  
	      std::stringstream out2;
	      out2 << (*itr).second;
	      mess_run+=(*itr).first;
	      mess_run+=" changed from ";
	      mess_run+=out.str();
	      mess_run+=" to ";
	      mess_run+=out2.str();
	      mess_run+=" \n";  
	}
}

void BrowserStateManager::setEventSimulationChanges(std::map<std::string,int> defaultValues_sim,std::map<std::string,int> newValues_sim)
{
	this->defaultValues_sim=defaultValues_sim;	
	this->newValues_sim=newValues_sim;
	mess_sim="";
	
	map<std::string, int>::const_iterator itr;
	for(itr = newValues_sim.begin(); itr != newValues_sim.end(); ++itr)
	{
	      std::stringstream out;
	      out << defaultValues_sim[(*itr).first];	  
	      std::stringstream out2;
	      out2 << (*itr).second;
	      mess_sim+=(*itr).first;
	      mess_sim+=" changed from ";
	      mess_sim+=out.str();
	      mess_sim+=" to ";
	      mess_sim+=out2.str();
	      mess_sim+=" \n";  
	}
}

void BrowserStateManager::setReadoutEnableChanges(std::map<std::string,int> defaultValues_readout,std::map<std::string,int> newValues_readout)
{
	this->defaultValues_readout=defaultValues_readout;	
	this->newValues_readout=newValues_readout;
	mess_readout="";
	
	map<std::string, int>::const_iterator itr;
	for(itr = newValues_readout.begin(); itr != newValues_readout.end(); ++itr)
	{
	      std::stringstream out;
	      out << defaultValues_readout[(*itr).first];	  
	      std::stringstream out2;
	      out2 << (*itr).second;
	      mess_readout+=(*itr).first;
	      mess_readout+=" changed from ";
	      mess_readout+=out.str();
	      mess_readout+=" to ";
	      mess_readout+=out2.str();
	      mess_readout+=" \n";  
	}
}

void BrowserStateManager::setRodMonitoringChanges(std::map<std::string,int> defaultValues_rod,std::map<std::string,int> newValues_rod)
{
	this->defaultValues_rod=defaultValues_rod;	
	this->newValues_rod=newValues_rod;
	mess_rod="";
	
	map<std::string, int>::const_iterator itr;
	for(itr = newValues_rod.begin(); itr != newValues_rod.end(); ++itr)
	{
	      std::stringstream out;
	      out << defaultValues_rod[(*itr).first];	  
	      std::stringstream out2;
	      out2 << (*itr).second;
	      mess_rod+=(*itr).first;
	      mess_rod+=" changed from ";
	      mess_rod+=out.str();
	      mess_rod+=" to ";
	      mess_rod+=out2.str();
	      mess_rod+=" \n";  
	}
}

void BrowserStateManager::setTimChanges(std::map<std::string,int> defaultValues_trig,std::map<std::string,int> newValues_trig)
{
	this->defaultValues_trig=defaultValues_trig;	
	this->newValues_trig=newValues_trig;
	mess_trig="";
	
	map<std::string, int>::const_iterator itr;
	for(itr = newValues_trig.begin(); itr != newValues_trig.end(); ++itr)
	{
	      std::stringstream out;
	      out << defaultValues_trig[(*itr).first];	  
	      std::stringstream out2;
	      out2 << (*itr).second;
	      mess_trig+=(*itr).first;
	      mess_trig+=" changed from ";
	      mess_trig+=out.str();
	      mess_trig+=" to ";
	      mess_trig+=out2.str();
	      mess_trig+=" \n";  
	}
}

std::string BrowserStateManager::getConfigChanges()
{
	std::string mess;
	mess+=mess_run;
	mess+=mess_readout;
	mess+=mess_sim;
	mess+=mess_rod;
	
	return mess;
}

std::string BrowserStateManager::getTimChanges()
{	
	return mess_trig;
}