#include "ConnectivityView.h"

#include <string>
#include <qstring.h>
#include <qpixmap.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3VBoxLayout>

#include <iostream>

using namespace PixRunConfigurator;

ConnectivityView::ConnectivityView(QWidget* parent,Browser* root, DbConnection* dbConn,BrowserStateManager* stateManager)
				  : View(stateManager,parent,QString("domain_tree"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=dbConn->getPixRunConfig();
	
	layout=new Q3VBoxLayout(this);
	tabs=new QTabWidget(this,"tabs");
	layout->addWidget(tabs);
	
	tree=new Q3ListView(this);
	tree->setRootIsDecorated(true);
 	tree->setSelectionMode(Q3ListView::Single);

	tree->addColumn("Name");
	tree->addColumn("Type");
	tree->addColumn("Status");
	tree->addColumn("RunConf");
	tree->addColumn("");
	tree->setColumnWidthMode(4, Q3ListView::Manual);	
	tree->hideColumn(4);
	tree->setColumnAlignment(2,Qt::AlignHCenter);
	tree->setColumnAlignment(3,Qt::AlignHCenter);

  	tree_part=new Q3ListView(this);
	tree_part->addColumn("Name");
	tree_part->addColumn("Status");
	tree_part->addColumn("RunConf");
	tree_part->addColumn("");
	tree_part->setColumnWidthMode(3, Q3ListView::Manual);	
	tree_part->hideColumn(3);
	tree_part->setSelectionMode(Q3ListView::Single);
	tree_part->setColumnAlignment(1,Qt::AlignHCenter);
	tree_part->setColumnAlignment(2,Qt::AlignHCenter);
	
	tree_crate=new Q3ListView(this);
	tree_crate->addColumn("Name");
	tree_crate->addColumn("Status");
	tree_crate->addColumn("RunConf");
	tree_crate->addColumn("");
	tree_crate->setColumnWidthMode(3, Q3ListView::Manual);	
	tree_crate->hideColumn(3);
	tree_crate->setSelectionMode(Q3ListView::Single);
	tree_crate->setColumnAlignment(1,Qt::AlignHCenter);
	tree_crate->setColumnAlignment(2,Qt::AlignHCenter);
	
	tree_tim=new Q3ListView(this);
	tree_tim->addColumn("Name");
	tree_tim->addColumn("Status");
	tree_tim->addColumn("");
	tree_tim->setColumnWidthMode(2, Q3ListView::Manual);	
	tree_tim->hideColumn(2);
	tree_tim->setSelectionMode(Q3ListView::Single);
	tree_tim->setColumnAlignment(1,Qt::AlignHCenter);
	
	tree_boc=new Q3ListView(this);
	tree_boc->addColumn("Name");
	tree_boc->addColumn("Status");
	tree_boc->addColumn("RunConf");
	tree_boc->addColumn("");
	tree_boc->setColumnWidthMode(3, Q3ListView::Manual);	
	tree_boc->hideColumn(3);
	tree_boc->setSelectionMode(Q3ListView::Single);
	tree_boc->setColumnAlignment(1,Qt::AlignHCenter);
	tree_boc->setColumnAlignment(2,Qt::AlignHCenter);
	
	tree_pp=new Q3ListView(this);
	tree_pp->addColumn("Name");
	tree_pp->addColumn("Status");
	tree_pp->addColumn("");
	tree_pp->setColumnWidthMode(2, Q3ListView::Manual);	
	tree_pp->hideColumn(2);
	tree_pp->setSelectionMode(Q3ListView::Single);
	tree_pp->setColumnAlignment(1,Qt::AlignHCenter);

	tree_mod=new Q3ListView(this);
	tree_mod->addColumn("Name");
	tree_mod->addColumn("Status");
	tree_mod->addColumn("");
	tree_mod->setColumnWidthMode(2, Q3ListView::Manual);	
	tree_mod->hideColumn(2);
	tree_mod->setSelectionMode(Q3ListView::Single);
	tree_mod->setColumnAlignment(1,Qt::AlignHCenter);
	
 	tabs->addTab(tree,"TreeView");	
  	tabs->addTab(tree_part,"Partition");	
 	tabs->addTab(tree_crate,"RodCrate");	
 	tabs->addTab(tree_tim,"Tim");	
 	tabs->addTab(tree_boc,"RodBoc");	
 	tabs->addTab(tree_pp,"Pp0");	
 	tabs->addTab(tree_mod,"Module");	
	tabs->show();
	tabs->setEnabled(true);	

	selectButton=new QPushButton("Select",this);
	layout->addWidget(selectButton,Qt::AlignHCenter);
	selectButton->setMaximumWidth(200);
	
	
	setMinimumWidth(400);
	setMinimumHeight(500);
	
	this->dbConn=dbConn;
		
	fillTree();
}

void ConnectivityView::connectSender(Q3ListViewItem*)
{	
	if(tree==QObject::sender())
	{
	    object=tree->selectedItem()->text(0);
	    type=tree->selectedItem()->text(1);
            status=tree->selectedItem()->text(4);
	}    
	if(tree_part==QObject::sender())
	{  
	    object=tree_part->selectedItem()->text(0);
	    type=QString("Partition");
	    status=tree_part->selectedItem()->text(3);
	} 
	if(tree_crate==QObject::sender())
	{  
	    object=tree_crate->selectedItem()->text(0);
	    type=QString("RodCrate");
	    status=tree_crate->selectedItem()->text(3);
	} 
	if(tree_tim==QObject::sender())
	{  
	    object=tree_tim->selectedItem()->text(0);
	    type=QString("Tim");
	    status=tree_tim->selectedItem()->text(3);
	} 
	if(tree_boc==QObject::sender())
	{  
	    object=tree_boc->selectedItem()->text(0);
	    type=QString("RodBoc");
	    status=tree_boc->selectedItem()->text(3);
	} 
	if(tree_pp==QObject::sender())
	{  
	    object=tree_pp->selectedItem()->text(0);
	    type=QString("Pp0");
	    status=tree_pp->selectedItem()->text(3);
	} 
	if(tree_mod==QObject::sender())
	{  
	    object=tree_mod->selectedItem()->text(0);
	    type=QString("Module");
	    status=tree_mod->selectedItem()->text(3);
	}
	connect(selectButton,SIGNAL(clicked()),this,SLOT(setCurrentTag()));
}

void ConnectivityView::connectAll()
{
	connect(tree,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_part,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_crate,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_tim,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_boc,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_pp,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	connect(tree_mod,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
}

void ConnectivityView::disconnectAll()
{
	disconnect(tree,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_part,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_crate,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_tim,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_boc,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_pp,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
	disconnect(tree_mod,SIGNAL(selectionChanged(Q3ListViewItem*)),this,SLOT(connectSender(Q3ListViewItem*)));
}

void ConnectivityView::receiveStatus(PixRunConfigurator::StatusAction action)
{
	disconnectAll();
	
 	if(action==PixRunConfigurator::statusChanged || action==PixRunConfigurator::editRun || action==PixRunConfigurator::editTim || action==PixRunConfigurator::writeTim || action==PixRunConfigurator::writeTim)
	  fillTree();
}



ConnectivityView::~ConnectivityView()
{
	delete tree;
}

void ConnectivityView::fillTree()
{
	disconnectAll();
	
	tree->clear();
	tree_part->clear();
	tree_crate->clear();
	tree_tim->clear();
	tree_boc->clear();
	tree_pp->clear();
	tree_mod->clear();
	
	std::map<std::string, PartitionConnectivity*>::iterator itp;
	for (itp=conn->parts.begin(); itp!=conn->parts.end(); itp++)
	{
		PartitionConnectivity *xp = itp->second; 
		string pname = xp->name();
		bool ep = xp->active();
		Q3ListViewItem* tmp=new Q3ListViewItem(tree);
 		Q3ListViewItem* tmp_part=new Q3ListViewItem(tree_part);
		tmp->setText(0,QString(pname.c_str()));
		tmp->setText(1,QString("Partition"));
		if(ep)
		{  
		    tmp->setText(4,QString("enabled"));
		    tmp->setPixmap(2,QPixmap("green.png"));
		    tmp_part->setPixmap(1,QPixmap("green.png"));
		    tmp_part->setText(3,QString("enabled"));
		}    
		else
		{  
		    tmp->setText(4,QString("disabled"));
		    tmp->setPixmap(2,QPixmap("red.png"));
		    tmp_part->setPixmap(1,QPixmap("red.png"));
		    tmp_part->setText(3,QString("disabled"));
		}  
		if (runConfig->subConfExists(pname))
		{
		    tmp->setPixmap(3,QPixmap("checkmark.png"));
		    tmp_part->setPixmap(2,QPixmap("checkmark.png"));
		}
		tmp->setSelectable(true);
		tmp->setOpen(true);
		tmp_part->setText(0,QString(pname.c_str()));
		tmp_part->setSelectable(true);
		std::map<std::string, RodCrateConnectivity*>::iterator itc;
		for (itc=itp->second->rodCrates().begin(); itc!=itp->second->rodCrates().end(); itc++)
		{
		    RodCrateConnectivity *xc = itc->second; 
		    string cname = xc->name();
		    bool ec = xc->active() && xc->enableReadout;
			Q3ListViewItem* tmp2=new Q3ListViewItem(tmp);
			Q3ListViewItem* tmp_crate=new Q3ListViewItem(tree_crate);
			tmp2->setText(0,QString(cname.c_str()));
			tmp2->setText(1,QString("RodCrate"));
			tmp2->setOpen(true);
			tmp2->setSelectable(true);	
			tmp_crate->setText(0,QString(cname.c_str()));
			tmp_crate->setSelectable(true);
			if(ec)
			{  
			    tmp2->setPixmap(2,QPixmap("green.png"));
			    tmp2->setText(4,QString("enabled"));
			    tmp_crate->setPixmap(1,QPixmap("green.png"));
			    tmp_crate->setText(3,QString("enabled"));
			}         
			else
			{  
			    tmp2->setPixmap(2,QPixmap("red.png"));
			    tmp2->setText(4,QString("disabled"));
			    tmp_crate->setPixmap(1,QPixmap("red.png"));
			    tmp_crate->setText(3,QString("disabled"));
			} 
			if (runConfig->subConfExists(cname))
			{
			    tmp2->setPixmap(3,QPixmap("checkmark.png"));
			    tmp_crate->setPixmap(2,QPixmap("checkmark.png"));
			}
			if (itc->second->tim() != NULL)
			{
			    TimConnectivity *xt = itc->second->tim();
			    string rname = xt->name();
			    bool et = xt->active() && xt->enableReadout;
				Q3ListViewItem* tmp3=new Q3ListViewItem(tmp2);
				Q3ListViewItem* tmp_tim=new Q3ListViewItem(tree_tim);
				tmp3->setText(0,QString(rname.c_str()));
				tmp3->setText(1,QString("Tim"));
				tmp3->setSelectable(true);			
				tmp_tim->setText(0,QString(rname.c_str()));
				tmp_tim->setSelectable(true);
				if(et)
				{  
				    tmp3->setPixmap(2,QPixmap("green.png"));
				    tmp3->setText(4,QString("enabled"));
				    tmp_tim->setPixmap(1,QPixmap("green.png"));
				    tmp_tim->setText(3,QString("enabled"));
				}         
				else
				{  
				    tmp3->setPixmap(2,QPixmap("red.png"));
				    tmp3->setText(4,QString("disabled"));
				    tmp_tim->setPixmap(1,QPixmap("red.png"));
				    tmp_tim->setText(3,QString("disabled"));
				}
			}
			std::map<int, RodBocConnectivity*>::iterator itr;
			for (itr=itc->second->rodBocs().begin(); itr!=itc->second->rodBocs().end(); itr++)
			{
			    RodBocConnectivity *xr = itr->second; 
			    string rname = xr->name();
			    bool er = xr->active() && xr->enableReadout;
				Q3ListViewItem* tmp3=new Q3ListViewItem(tmp2);
				Q3ListViewItem* tmp_boc=new Q3ListViewItem(tree_boc);
				tmp3->setText(0,QString(rname.c_str()));
				tmp3->setText(1,QString("RodBoc"));
				tmp3->setOpen(true);
				tmp3->setSelectable(true);
				tmp_boc->setText(0,QString(rname.c_str()));
				tmp_boc->setSelectable(true);
				if(er)
				{  
				    tmp3->setPixmap(2,QPixmap("green.png"));
				    tmp3->setText(4,QString("enabled"));
				    tmp_boc->setPixmap(1,QPixmap("green.png"));
				    tmp_boc->setText(3,QString("enabled"));
				}         
				else
				{  
				    tmp3->setPixmap(2,QPixmap("red.png"));
				    tmp3->setText(4,QString("disabled"));
				    tmp_boc->setPixmap(1,QPixmap("red.png"));
				    tmp_boc->setText(3,QString("disabled"));
				}
				if (runConfig->subConfExists(rname))
				{
				    tmp3->setPixmap(3,QPixmap("checkmark.png"));
				    tmp_boc->setPixmap(2,QPixmap("checkmark.png"));
				}
				for (unsigned int ito=0; ito<4; ito++)
				{
				    if (xr->obs(ito) != NULL)
				    {
					Pp0Connectivity *xo = xr->obs(ito)->pp0(); 
					if (xo != NULL)
					{
					    string oname = xo->name();
					    bool eo = xo->active();
					    Q3ListViewItem* tmp4=new Q3ListViewItem(tmp3);
					    Q3ListViewItem* tmp_pp=new Q3ListViewItem(tree_pp);
					    tmp4->setText(0,QString(oname.c_str()));
					    tmp4->setText(1,QString("Pp0"));
					    tmp4->setSelectable(true);
					    tmp_pp->setText(0,QString(oname.c_str()));
					    tmp_pp->setSelectable(true);
					    if(eo)
					    {  
						tmp4->setPixmap(2,QPixmap("green.png"));
						tmp4->setText(4,QString("enabled"));
						tmp_pp->setPixmap(1,QPixmap("green.png"));
						tmp_pp->setText(3,QString("enabled"));
					    }         
					    else
					    {  
						tmp4->setPixmap(2,QPixmap("red.png"));
						tmp4->setText(4,QString("disabled"));
						tmp_pp->setPixmap(1,QPixmap("red.png"));
						tmp_pp->setText(3,QString("disabled"));
					    }					    
					    for (unsigned int itm=1; itm<=8; itm++)
					    {
						ModuleConnectivity *xm = xo->modules(itm); 
						if (xm != NULL)
						{
						    string mname = xm->name();
						    bool em = xm->active() && xm->enableReadout;
						    // 
						    Q3ListViewItem* tmp5=new Q3ListViewItem(tmp4);
						    Q3ListViewItem* tmp_mod=new Q3ListViewItem(tree_mod);
						    tmp5->setText(0,QString(mname.c_str()));
						    tmp5->setText(1,QString("Module"));
						    tmp5->setSelectable(true);	
						    tmp_mod->setText(0,QString(mname.c_str()));
						    tmp_mod->setSelectable(true);	
						    if(em)
						    { 
							tmp5->setPixmap(2,QPixmap("green.png"));
							tmp5->setText(4,QString("enabled"));
							tmp_mod->setPixmap(1,QPixmap("green.png"));
							tmp_mod->setText(3,QString("enabled"));
						    }	
						    else
						    {
							tmp5->setPixmap(2,QPixmap("red.png"));
							tmp5->setText(4,QString("disabled"));
							tmp_mod->setPixmap(1,QPixmap("red.png"));
							tmp_mod->setText(3,QString("disabled"));
						    }
						}    
					    }
					}
				    }     
				}				
			}			
 	        }	
 	}   
  connectAll();
}

void ConnectivityView::setCurrentTag()
{
	disconnect(selectButton,SIGNAL(clicked()),this,SLOT(setCurrentTag()));
	disconnectAll();
	
 	PixRunConfigurator::StatusAction action;

 	stateManager->setCurrentDomain(std::string(status.toLatin1().data()));
 	stateManager->setCurrentStatus(std::string(status.toLatin1().data()));
 	stateManager->setCurrentType(std::string(type.toLatin1().data()));
 	stateManager->setCurrentObject(std::string(object.toLatin1().data()));
	
 	action=PixRunConfigurator::statusSet;
   	stateManager->receiveAction(action,"connectivity view");
 	
	connectAll();
 }





