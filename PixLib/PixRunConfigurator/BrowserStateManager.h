#ifndef _BrowserStateManager_h_
#define _BrowserStateManager_h_

#include <qobject.h>
#include <qwidget.h>

#include "Object.h"
#include "DbConnection.h"

namespace PixRunConfigurator
{

enum StatusAction {none=0x0,statusSet=0x1,statusChanged=0x2,editRun=0x4,editTim=0x8,viewUpdate=0x100,
			domainChanged=0x20,tagChanged=0x40,writeTim=0x80,writeRun=0x10,addRun=0x200,deleteRun=0x400};

class BrowserStateManager : public QObject
{

	Q_OBJECT

	public:
	
	BrowserStateManager(QWidget* parent,DbConnection* dbConn);
	
	void setCurrentDomain(std::string domain) {this->domain=domain;}
	void setCurrentStatus(std::string status);
	void setCurrentTag(std::string tag);
	void setCurrentObject(std::string object);
	void setCurrentType(std::string type) {this->type=type;}
	void setRunParamChanges(std::map<std::string,int> defaultValues_run,std::map<std::string,int> newValues_run);	
	void setEventSimulationChanges(std::map<std::string,int> defaultValues_sim,std::map<std::string,int> newValues_sim);	
	void setReadoutEnableChanges(std::map<std::string,int> defaultValues_readout,std::map<std::string,int> newValues_readout);	
	void setRodMonitoringChanges(std::map<std::string,int> defaultValues_rod,std::map<std::string,int> newValues_rod);
	void setTimChanges(std::map<std::string,int> defaultValues_rod,std::map<std::string,int> newValues_rod);
	std::string getConfigChanges();
	std::string getTimChanges();
	void receiveAction(StatusAction,const std::string);

	void setCurrentId(std::string id){this->id=id;}
	void setCurrentConn(std::string conn){this->conn=conn;}
	void setCurrentCfg(std::string cfg){this->cfg=cfg;}
	std::string getCurrentId(){return id;}
	std::string getCurrentConn(){return conn;}
	std::string getCurrentCfg(){return cfg;}	
	
	std::string getCurrentDomain() {return domain;}
	std::string getCurrentStatus() {return status;}
	std::string getCurrentTag() {return tag;}
	std::string getCurrentRunConf() {return "Global";}
	std::string getCurrentType() {return type;}
	std::string getCurrentObject() {return object;}
	bool hasObject() {return objectSet;}
	bool hasStatus();
	bool hasTagStructure() {return structureSet;}
	
	private:
	
	std::string domain,tag,type,object,status,id,conn,cfg;
	DbConnection* dbConn;
 	bool objectSet,structureSet,statusSet;
	int maxSteps,currentStep;
   	std::map<std::string,int> defaultValues_run;
   	std::map<std::string,int> newValues_run;
   	std::map<std::string,int> defaultValues_sim;
   	std::map<std::string,int> newValues_sim;
   	std::map<std::string,int> defaultValues_readout;
   	std::map<std::string,int> newValues_readout;
   	std::map<std::string,int> defaultValues_rod;
   	std::map<std::string,int> newValues_rod;
   	std::map<std::string,int> defaultValues_trig;
   	std::map<std::string,int> newValues_trig;
	std::string mess_run;
	std::string mess_sim;
	std::string mess_readout;
	std::string mess_rod;
	std::string mess_trig;
	
	signals:
	
	void trigger(StatusAction);
	

};
// 
}

#endif

