#include "ReadoutEnableTab.h"

#include <qfont.h>
#include <qcursor.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>
#include <QLabel>

#include <iostream>

using namespace PixRunConfigurator;

ReadoutEnableTab::ReadoutEnableTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig)
			  : View(stateManager,parent,QString("readout_struct"))
{
	this->parent=root;
 	this->conn=dbConn->getdefinedPixConnectivity();
	this->runConfig=runConfig;
	
	layout=new Q3HBoxLayout(this);	
	
	revLayout=new Q3VBoxLayout(layout);
	
	detailLayout=new Q3GridLayout(revLayout,19,4);
	detailLayout->setColSpacing(0,20);
	detailLayout->setColSpacing(2,5);
	detailLayout->setColSpacing(3,20);
	detailLayout->setRowSpacing(0,50);
	detailLayout->setRowSpacing(2,5);
	detailLayout->setRowSpacing(4,5);
	detailLayout->setRowSpacing(6,5);
	detailLayout->setRowSpacing(8,5);
	detailLayout->setRowSpacing(10,5);
	detailLayout->setRowSpacing(12,5);
	detailLayout->setRowSpacing(14,5);
	detailLayout->setRowSpacing(16,5);
	detailLayout->setRowSpacing(18,50);
	
	readoutEnable_0=new QLabel("Readout enable group 0:",this);
	detailLayout->addWidget(readoutEnable_0,5,1);	
	readoutEnable_1=new QLabel("Readout enable group 1:",this);
	detailLayout->addWidget(readoutEnable_1,7,1);	
	readoutEnable_2=new QLabel("Readout enable group 2:",this);
	detailLayout->addWidget(readoutEnable_2,9,1);	
	readoutEnable_3=new QLabel("Readout enable group 3:",this);
	detailLayout->addWidget(readoutEnable_3,11,1);

	readoutEnable_0_edit=new QPushButton(this);
	detailLayout->addWidget(readoutEnable_0_edit,5,2);
	readoutEnable_0_edit->setMaximumWidth(80);
	readoutEnable_1_edit=new QPushButton(this);
	detailLayout->addWidget(readoutEnable_1_edit,7,2);
	readoutEnable_1_edit->setMaximumWidth(80);
	readoutEnable_2_edit=new QPushButton(this);
	detailLayout->addWidget(readoutEnable_2_edit,9,2);
	readoutEnable_2_edit->setMaximumWidth(80);
	readoutEnable_3_edit=new QPushButton(this);
	detailLayout->addWidget(readoutEnable_3_edit,11,2);
	readoutEnable_3_edit->setMaximumWidth(80);

	
	setEnabled(true);
}

ReadoutEnableTab::~ReadoutEnableTab()
{
 	disconnectAll();

}

void ReadoutEnableTab::connectAll()
{
	connect(readoutEnable_0_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_0Changes()));
	connect(readoutEnable_1_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_1Changes()));
	connect(readoutEnable_2_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_2Changes()));
	connect(readoutEnable_3_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_3Changes()));
}

void ReadoutEnableTab::disconnectAll()
{
	disconnect(readoutEnable_0_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_0Changes()));
	disconnect(readoutEnable_1_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_1Changes()));
	disconnect(readoutEnable_2_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_2Changes()));
	disconnect(readoutEnable_3_edit,SIGNAL(clicked()),this,SLOT(readoutEnable_3Changes()));
}

void ReadoutEnableTab::readoutEnable_0Changes()
{
        newValues.erase("Readout enable group 0");	      
	if(readoutEnable_0_edit->text()=="On")
	{
	  readoutEnable_0_edit->setText("Off");
	  runConfig->disableReadout(0);	  
          newValues.insert( pair<std::string,int>("Readout enable group 0",0) );	      
	}
	else  
	{
	  readoutEnable_0_edit->setText("On");
	  runConfig->enableReadout(0);	  
          newValues.insert( pair<std::string,int>("Readout enable group 0",1) );	      
	}
}

void ReadoutEnableTab::readoutEnable_1Changes()
{
        newValues.erase("Readout enable group 1");	      
	if(readoutEnable_1_edit->text()=="On")
	{
	  readoutEnable_1_edit->setText("Off");
	  runConfig->disableReadout(1);	  
          newValues.insert( pair<std::string,int>("Readout enable group 1",0) );	      
	}
	else  
	{
	  readoutEnable_1_edit->setText("On");
	  runConfig->enableReadout(1);	  
          newValues.insert( pair<std::string,int>("Readout enable group 1",1) );	      
	}
}

void ReadoutEnableTab::readoutEnable_2Changes()
{
        newValues.erase("Readout enable group 2");	      
	if(readoutEnable_2_edit->text()=="On")
	{
	  readoutEnable_2_edit->setText("Off");
	  runConfig->disableReadout(2);	  
          newValues.insert( pair<std::string,int>("Readout enable group 2",0) );	      
	}
	else  
	{
	  readoutEnable_2_edit->setText("On");
	  runConfig->enableReadout(2);	  
          newValues.insert( pair<std::string,int>("Readout enable group 2",1) );	      
	}
}

void ReadoutEnableTab::readoutEnable_3Changes()
{
        newValues.erase("Readout enable group 3");	      
	if(readoutEnable_3_edit->text()=="On")
	{
	  readoutEnable_3_edit->setText("Off");
	  runConfig->disableReadout(3);	  
          newValues.insert( pair<std::string,int>("Readout enable group 3",0) );	      
	}
	else  
	{
	  readoutEnable_3_edit->setText("On");
	  runConfig->enableReadout(3);	  
          newValues.insert( pair<std::string,int>("Readout enable group 3",1) );	      
	}
}

void ReadoutEnableTab::receiveStatus(PixRunConfigurator::StatusAction action)
{
 	if(action==PixRunConfigurator::editRun)
	{
		showObjectDetails();
	}
 	if(action==PixRunConfigurator::writeRun)
	{
		stateManager->setReadoutEnableChanges(defaultValues,newValues);
		showObjectDetails();
	}	
}

void ReadoutEnableTab::showObjectDetails()
{
 	std::string object=stateManager->getCurrentObject();
	runConf=stateManager->getCurrentRunConf();
	runConfig->selectSubConf(object);
	if(runConfig->subConfExists(object)){
	cf = new Config(runConfig->config());	

	defaultValues.clear();
	newValues.clear();
		
	if(runConfig->readoutEnabled(0))
	{
	  readoutEnable_0_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Readout enable group 0",1) );	      
	}
	else
	{
	  readoutEnable_0_edit->setText("Off");	
          defaultValues.insert( pair<std::string,int>("Readout enable group 0",0) );	      
	}
	if(runConfig->readoutEnabled(1))
	{
	  readoutEnable_1_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Readout enable group 1",1) );	      
	}
	else
	{
	  readoutEnable_1_edit->setText("Off");	
          defaultValues.insert( pair<std::string,int>("Readout enable group 1",0) );	      
	}
	if(runConfig->readoutEnabled(2))
	{
	  readoutEnable_2_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Readout enable group 2",1) );	      
	}
	else
	{
	  readoutEnable_2_edit->setText("Off");	
          defaultValues.insert( pair<std::string,int>("Readout enable group 2",0) );	      
	}
	if(runConfig->readoutEnabled(3))
	{
	  readoutEnable_3_edit->setText("On");
          defaultValues.insert( pair<std::string,int>("Readout enable group 3",1) );	      
	}
	else
	{
	  readoutEnable_3_edit->setText("Off");		
          defaultValues.insert( pair<std::string,int>("Readout enable group 3",0) );	      
	}
	
	}	

	connectAll();  
}