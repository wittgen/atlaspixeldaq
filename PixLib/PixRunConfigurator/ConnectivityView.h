#ifndef _ConnectivityView_h_
#define _ConnectivityView_h_

#include <map>

#include <qlayout.h>
#include <q3listview.h>
#include <qpushbutton.h>
#include <qtabwidget.h>
#include <qlabel.h>
#include <qtimer.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <Q3GridLayout>

#include "View.h"
#include "DbConnection.h"
#include "Browser.h"
#include "BrowserStateManager.h"

#include <ipc/core.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbDomain.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixController/PixScan.h"

#include <oks/kernel.h>
#include <oks/object.h>
#include <oks/class.h>
#include <oks/index.h>
#include <oks/attribute.h>
#include <oks/relationship.h>

#include <map>
#include <string>

#include "Object.h"

namespace PixRunConfigurator
{

class Browser;

class ConnectivityView : public View
{
	Q_OBJECT
	
	public:
	
	ConnectivityView(QWidget* parent,Browser* root,DbConnection* dbConn,BrowserStateManager* stateManager);
	~ConnectivityView();	
	
	private:
	
	Q3ListView* tree;
	Q3ListView* tree_crate;
	Q3ListView* tree_tim;
 	Q3ListView* tree_part;
 	Q3ListView* tree_boc;
 	Q3ListView* tree_pp;
 	Q3ListView* tree_mod;
	
	Q3VBoxLayout* layout;
	Q3GridLayout* grid1;
	QTabWidget* tabs;
	QPushButton* selectButton;
	
	QString object;
	QString type;
 	QString status;
	
	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	
	Browser* parent;
			
	void disconnectAll();	
	void connectAll();
	
	public slots:
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private slots:

	void fillTree();
	void connectSender(Q3ListViewItem*);
	void setCurrentTag();
};

};

#endif

