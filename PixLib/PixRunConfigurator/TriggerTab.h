#ifndef _TriggerTab_h_
#define _TriggerTab_h_

#include <q3groupbox.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
#include <qsplitter.h>
#include <qlineedit.h>
#include <qspinbox.h>
#include <q3listbox.h>
//Added by qt3to4:
#include <Q3GridLayout>
#include <Q3HBoxLayout>
#include <Q3VBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "CommonDataTabView.h"
#include "View.h"
#include "BrowserStateManager.h"

#include "PixTrigController/TimPixTrigController.h"

namespace PixRunConfigurator
{

class CommonDataTabView;

class TriggerTab : public View
{

	Q_OBJECT
	
	public:
	
	TriggerTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig);
	~TriggerTab();
			
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void connectAll();
	void disconnectAll();	
	
	void showObjectDetails();
	void writeConfig();
	
	CommonDataTabView* parent;
	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
	
	Q3ListView* revView;
	
	QLabel* timTriggerMode;
	QLabel* triggerFrequency;
	QLabel* ECRFrequency;
	QLabel* triggerRand2Frequency;
	QLabel* triggerDelay;
	QLabel* ttcrxCoarseDelay;
	QLabel* ttcrxFineDelay;
	QLabel* triggerType;
	QLabel* BCID_offset;
	QLabel* RodBusyMask;
	QLabel* Seq_config_file;
	QLabel* Seq_triggering_mode;
	QLabel* timCalInjMode;
	QLabel* CalTrigDelayActiveTIM;
	QLabel* TrigDelayPassiveTIM;
	QLabel* NumOfSubsTriggersPassiveTIM;
	
	QComboBox* timTriggerMode_edit;
	QSpinBox* triggerFrequency_edit;
	QSpinBox* ECRFrequency_edit;
	QSpinBox* triggerRand2Frequency_edit;
	QSpinBox* triggerDelay_edit;
	QSpinBox* ttcrxCoarseDelay_edit;
	QSpinBox* ttcrxFineDelay_edit;
	QSpinBox* triggerType_edit;
	QSpinBox* BCID_offset_edit;
	QSpinBox* RodBusyMask_edit;
	QLineEdit* Seq_config_file_edit;
	QComboBox* Seq_triggering_mode_edit;
	QComboBox* timCalInjMode_edit;
	QSpinBox* CalTrigDelayActiveTIM_edit;
	QSpinBox* TrigDelayPassiveTIM_edit;
	QSpinBox* NumOfSubsTriggersPassiveTIM_edit;
	
	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	TimPixTrigController* timC;
	PixDbServerInterface* dbServer;
	
   	std::map<std::string,int> defaultValues;
   	std::map<std::string,int> newValues;
	std::string id;
	std::string cfg;
	
	private slots:
	void triggerDelayChanges(int);
	void ttcrxCoarseDelayChanges(int);
	void triggerTypeChanges(int);
	void BCID_offsetChanges(int);
	void RodBusyMaskChanges(int);
	void CalTrigDelayActiveTIMChanges(int);
	void TrigDelayPassiveTIMChanges(int);
	void NumOfSubsTriggersPassiveTIMChanges(int);
	void triggerFrequencyChanges(int);
	void ECRFrequencyChanges(int);
	void triggerRand2FrequencyChanges(int);
	void ttcrxFineDelayChanges(int);
	
	signals:
};

};
#endif

