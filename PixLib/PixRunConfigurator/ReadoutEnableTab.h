#ifndef _ReadoutEnableTab_h_
#define _ReadoutEnableTab_h_

#include <q3groupbox.h>
#include <q3table.h>
#include <qlayout.h>
#include <qlabel.h>
#include <q3listview.h>
#include <qwidget.h>
#include <qsplitter.h>
#include <qlineedit.h>
//Added by qt3to4:
#include <Q3VBoxLayout>
#include <Q3GridLayout>
#include <Q3HBoxLayout>

#include "Object.h"
#include "DbConnection.h"
#include "CommonDataTabView.h"
#include "View.h"
#include "BrowserStateManager.h"

namespace PixRunConfigurator
{

class CommonDataTabView;

class ReadoutEnableTab : public View
{

	Q_OBJECT
	
	public:
	
	ReadoutEnableTab(QWidget* parent,CommonDataTabView* root,DbConnection* dbConn,BrowserStateManager* stateManager, PixRunConfig* runConfig);
	~ReadoutEnableTab();
		
	void resetView();
	
	public slots:
	
	
	void receiveStatus(PixRunConfigurator::StatusAction);
	
	private:
	
	void connectAll();
	void disconnectAll();	
		
	void showObjectDetails();
	
	std::string runConf;

	
	CommonDataTabView* parent;
	
	Q3HBoxLayout* layout;
	Q3VBoxLayout* revLayout;
	Q3GridLayout* detailLayout;
	
	Q3ListView* revView;
	
	QLabel* readoutEnable_0;
	QLabel* readoutEnable_1;
	QLabel* readoutEnable_2;
	QLabel* readoutEnable_3;

	QPushButton* readoutEnable_0_edit;
	QPushButton* readoutEnable_1_edit;
	QPushButton* readoutEnable_2_edit;
	QPushButton* readoutEnable_3_edit;

	DbConnection* dbConn;
  	PixLib::PixConnectivity* conn;
	PixRunConfig* runConfig;
	Config* cf;

   	std::map<std::string,int> defaultValues;
   	std::map<std::string,int> newValues;	
	
	private slots:
	
	void readoutEnable_0Changes();  
	void readoutEnable_1Changes();  
	void readoutEnable_2Changes();  
	void readoutEnable_3Changes();  
		
	signals:
};

};

#endif

