/////////////////////////////////////////////////////////////////////
// PixDcs.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 03/10/06  Version 1.0 (CS,PR)
//           Initial release
// 22/11/06  Version 1.1 (CS,NG)
//           Extended set of DCS and FSM commands
//
// 03/10/06  Version 1.2 (AO)
//           Clean-up done; Added enum able to decide what params/actions to do.
//
// 10/01/08 Version 2.0 (NG)
//          Final clean up for the Connectivity Test and first calibration in the PIT


//using namespace std;

#include <time.h>
#include <unistd.h>
#include <sstream>
#include <ipc/partition.h>
#include <is/infodictionary.h>
#include <ddc/DdcData.hxx>
#include <ddc/DdcRequest.hxx>
#include <ddc/DdcCommander.hxx>

#include "PixDcs/PixDcs.h"

using namespace PixLib;

PixDcs::PixDcs(IPCPartition *ipcPartition, std::string isServer, std::string rodName, std::string requestServer) :
  m_isServer(isServer), m_requestServer(requestServer), m_rodName(rodName), m_ipcPartition(ipcPartition) {
  
  // Create IS dictionary
  m_isDictionary = new ISInfoDictionary(*m_ipcPartition);

  // Create DDC commander and request sender
  m_ddcRequest = new DdcRequest(*m_ipcPartition, m_requestServer);
  m_ddcCommander = new DdcCommander(*m_ipcPartition);

}



PixDcs::~PixDcs() 
{
  if(m_isDictionary)  delete m_isDictionary;
  if(m_ddcRequest)    delete m_ddcRequest;
  if(m_ddcCommander)  delete m_ddcCommander;
}



bool PixDcs::initRead(std::string name)
{
  std::string daqRequest = name + " => " + m_isServer;
  std::string errorMsg;
  bool status = m_ddcRequest->ddcMoreImportOnChange(daqRequest, errorMsg);
  if(!status) {
  sleep(1);
  return false;
  }
  else {
    m_subscriptionMem.push_back(name);
    sleep(1);
    //std::cout << "Initialize sybscription for " << name << std::endl;
    return true;
  }
}



bool PixDcs::endRead(std::string name)  
{
  std::string daqRequest = name + " => " + m_isServer;
  std::string errorMsg;
  bool status = m_ddcRequest->ddcCancelImport(daqRequest, errorMsg);

  if(!status) return false;
  else return true;
}



void PixDcs::read(std::string connDbName, PixDcsDefs::DataPointType dataPointType, float& value) 
{
  std::string name = convertName(connDbName, dataPointType);
  bool status = true;
  std::stringstream info, infoErr;
  std::string applName = "PixDcs::read";

  if (m_subscriptionMem.size() == 0) {
    status = initRead(name);
    if (!status) {
      infoErr << name << " failed the subscription! ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERSUBSCRIPTION", infoErr.str()));
    } else {
      info << name << " *** was succesfully subscribed ";
      ers::log(PixLib::pix::daq (ERS_HERE, applName, info.str()));
    }
  } else if (m_subscriptionMem.size() > 0) {
    for(unsigned int i=0; i<m_subscriptionMem.size(); i++) {
      if (name == m_subscriptionMem[i]) break;
      else if (name != m_subscriptionMem[i] && i==m_subscriptionMem.size()-1) {
	status = initRead(name);
	if (!status) {
	  infoErr << name << " failed the subscription! ";
          ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
	  throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERSUBSCRIPTION", infoErr.str()));
	  break;
	} else {
	  info << name << " --- was succesfully subscribed ";
	  //std::cout << info.str() << std::endl;
	  ers::log(PixLib::pix::daq (ERS_HERE, applName, info.str()));
	}
      }
    }
  }
  if (!status) return;

  info.str(std::string());
  infoErr.str(std::string());

  std::vector<std::pair<int, OWLTime> > tags;
  std::vector<std::pair<int, OWLTime> >::iterator timeStampIterator;
  // int case
  if(dataPointType==PixDcsDefs::HVON) {
    DcsSingleData<int> isInt(0, *m_ipcPartition, m_isServer+"."+name);
    try {
      m_isDictionary->getValue(m_isServer+"."+name, isInt);
      m_isDictionary->getTags(m_isServer+"."+name, tags);
      for(timeStampIterator=tags.begin(); timeStampIterator!=tags.end(); timeStampIterator++) {
	info << "PixDcs::updateDataPoint " << name << " on " << m_rodName 
	     << " is "  << (float)isInt.getValue() << " with time stamp: "
	     << (*timeStampIterator).second << " ";
      }
      ers::info(PixLib::pix::daq (ERS_HERE, applName, info.str()));
    } catch (const daq::is::InvalidName&) {
      infoErr << "PixDcs::updateDataPoint - IS server, invalid Name - "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERNAME", infoErr.str()));
    } catch (const daq::is::RepositoryNotFound&) {
      infoErr << "PixDcs::updateDataPoint - IS server, repository not found "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERREPOSITORY", infoErr.str()));
    } catch (const daq::is::InfoNotFound&) {
      infoErr << "PixDcs::updateDataPoint - IS server, Information not found "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "ISINFORMATION", infoErr.str()));
    } catch (const daq::is::InfoNotCompatible&) {
      infoErr << "PixDcs::updateDataPoint - IS server, Information not compatible "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISINFOCOMPATIBLE", infoErr.str()));
    } catch(...) {
      value = -666.;
      infoErr << "PixDcs::updateDataPoint - Unknown exception was thrown - I force " << name << " on " << m_rodName << " to "  
	      << value << " in IS, but IS was not updated in time! ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "UNKNOWN", infoErr.str()));
    }
    value = (float)isInt.getValue();

    // float case
  } else {
    DcsSingleData<float> isFloat(0, *m_ipcPartition, m_isServer+"."+name);
    try { 
      m_isDictionary->getValue(m_isServer+"."+name, isFloat);
      m_isDictionary->getTags(m_isServer+"."+name, tags);
      for(timeStampIterator=tags.begin(); timeStampIterator!=tags.end(); timeStampIterator++) {
	info << "PixDcs::updateDataPoint " << name << " on " << m_rodName 
	     << " is "  << (float)isFloat.getValue() << " with time stamp: "
	     << (*timeStampIterator).second.c_str() << " ";
      }
      ers::info(PixLib::pix::daq (ERS_HERE, applName, info.str()));
    } catch (const daq::is::InvalidName&) {
      infoErr << "PixDcs::updateDataPoint - IS server, invalid Name "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERNAME", infoErr.str()));      
    } catch (const daq::is::RepositoryNotFound&) {
      infoErr << "PixDcs::updateDataPoint - IS server, repository not found "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERREPOSITORY", infoErr.str()));
    } catch (const daq::is::InfoNotFound&) {
      infoErr << "PixDcs::updateDataPoint - IS server, Information not found "<< name <<  " on " << m_rodName << " ";
      ers::warning(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::WARNING, "ISINFORMATION", infoErr.str()));
    } catch (const daq::is::InfoNotCompatible&) {
      infoErr << "PixDcs::updateDataPoint - IS server, Information not compatible "<< name <<  " on " << m_rodName << " ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISINFOCOMPATIBLE", infoErr.str()));    
    } catch(...){
      value = -666.;
      infoErr << "PixDcs::updateDataPoint - Unknown exception was thrown - I force " << name << " on " << m_rodName << " to "  
	      << value << " in IS, but IS was not updated in time! ";
      ers::warning(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "UNKNOWN", infoErr.str()));
    }
    value = isFloat.getValue();
  }
}


// Used in CT
bool PixDcs::readDataPoint(std::string connDbName, PixDcsDefs::DataPointType dataPointType, float& value) 
{
  std::string name = convertName(connDbName, dataPointType);
 
  // Subscribe the variable to DIM via DDC
  std::string errInfo, daqRequest = name + " => " + m_isServer;

  bool status = m_ddcRequest->ddcReadDcsData(daqRequest, errInfo);
  if (!status) {
    std::string msg = "Could not send DDC request for "+name+": "+errInfo;
    ers::error(PixLib::pix::daq (ERS_HERE, "ddc::ddcReadDcsData", msg));
  }
  sleep(1); // Wait for DIM subscription to arrive in IS. To be fixed in DDC (03/06/2008)
  std::vector<std::pair<int, OWLTime> > tags;
  std::vector<std::pair<int, OWLTime> >::iterator it;
  // Read the variable from IS
  std::string applName = "is::getValue";
  std::stringstream info, infoErr;

  // int case
  if(dataPointType==PixDcsDefs::HVON) {
    DcsSingleData<int> isInt(0, *m_ipcPartition, m_isServer+"."+name);
    try {
      m_isDictionary->getValue(m_isServer+"."+name, isInt);
      info << "PixDcs::readDataPoint " << name << " is "  << (float)isInt.getValue() << " ";
      std::cout << info.str() << std::endl;
    } catch (const daq::is::InvalidName&) {
      infoErr << "PixDcs::readDataPoint - IS server, invalid Name ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERNAME", infoErr.str()));
      return false;
    } catch (const daq::is::RepositoryNotFound&) {
      infoErr << "PixDcs::readDataPoint - IS server, repository not found ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERREPOSITORY", infoErr.str()));
      return false;
    } catch (const daq::is::InfoNotFound&) {
      infoErr << "PixDcs::readDataPoint - IS server, Information not found ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "ISINFORMATION", infoErr.str()));
      return false;
    } catch (const daq::is::InfoNotCompatible&) {
      infoErr << "PixDcs::readDataPoint - IS server, Information not compatible ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "ISINFOCOMPATIBLE", infoErr.str()));
      return false;
    } catch(...) {
      value = -666.;
      infoErr << "PixDcs::readDataPoint - Unknown exception was thrown - I force " << name << " to "  
	      << value << " in IS, but IS was not updated in time! ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "UNKNOWN", infoErr.str()));
      return false;
    }
    value = (float)isInt.getValue(); 
    for(it=tags.begin(); it!=tags.end(); it++) {
      std::cerr << (*it).second.c_str() << std::endl;  
    }
    // float case
  } else {
    DcsSingleData<float> isFloat(0, *m_ipcPartition, m_isServer+"."+name);
    try { 
      m_isDictionary->getValue(m_isServer+"."+name, isFloat);
      info << "PixDcs::readDataPoint " << name << " is "  << isFloat.getValue() << " ";
      std::cout << info.str() << std::endl;
    } catch (const daq::is::InvalidName&) {
      infoErr << "PixDcs::readDataPoint - IS server, invalid Name ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERNAME", infoErr.str()));   
      return false;
    } catch (const daq::is::RepositoryNotFound&) {
      infoErr << "PixDcs::readDataPoint - IS server, repository not found ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::FATAL, "ISSERVERREPOSITORY", infoErr.str()));
      return false;
    } catch (const daq::is::InfoNotFound&) {
      infoErr << "PixDcs::readDataPoint - IS server, Information not found ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "ISINFORMATION", infoErr.str()));
      return false;
    } catch (const daq::is::InfoNotCompatible&) {
      infoErr << "PixDcs::readDataPoint - IS server, Information not compatible ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      throw(PixDcsExc(PixDcsExc::ERROR, "ISINFOCOMPATIBLE", infoErr.str()));     
      return false;    
    } catch(...){
      value = -666.;
      infoErr << "PixDcs::readDataPoint - Unknown exception was thrown - I force " << name << " to "  
	      << value << " in IS, but IS was not updated in time! ";
      ers::error(PixLib::pix::daq (ERS_HERE, applName, infoErr.str()));
      return false;
    }
    value = isFloat.getValue();
    for(it=tags.begin(); it!=tags.end(); it++) {
      std::cerr << (*it).second.c_str() << std::endl;  
    }
  }  
  return true;
}



// DDC command sending method
int PixDcs::writeDataPoint(std::string connDbName, float value, PixDcsDefs::DDCCommands type, unsigned int timeout) 
{
  int status;
  std::string parameter, name;

  std::stringstream commandParams; 
  commandParams<<value;
  switch(type) {
  case PixDcsDefs::SET_HV: name = "SET_HV"; break;
  case PixDcsDefs::SET_VISET: name = "SET_VISet"; break;
  case PixDcsDefs::SET_VPIN: name = "SET_VPin"; break;
  case PixDcsDefs::SET_VVDC: name = "SET_VVDC"; break;
  case PixDcsDefs::SET_VDDD: name = "SET_VDDD"; break;
  case PixDcsDefs::SET_VDDA: name = "SET_VDDA"; break;
  case PixDcsDefs::TEST_DDC: name = "TEST"; break;
  default:
    { 
      PixDcsDefs::DDCCommandsResponse reply = PixDcsDefs::ERR_NAME;
      std::stringstream info;
      info << "PixDcs::writedataPoint - " << type <<  " is an obsolete command, not implemented anymore, nothing will be done";
      std::cout << info.str() << std::endl;
      ers::warning(PixLib::pix::daq (ERS_HERE,"PixDcs::writedataPoint", info.str()));
      return reply;
    }
  }
  std::string dataPointName = "DDCcommand_"+m_rodName;
  parameter = name+" "+commandParams.str()+" "+connDbName;
  std::cout << "PixDcs::writeDataPoint: Go to execCommand("<<dataPointName<<", "
	    <<parameter<<", "<<timeout<<");"<< std::endl;
  status=execCommand(dataPointName, parameter, timeout);
  return status;
}



// FSM command sending method
int PixDcs::sendCommand(PixDcsDefs::FSMCommands command, std::string connDbName, unsigned int timeout) 
{
  int status; 
  std::string parameter, name;

  switch(command) {
  case PixDcsDefs::SWITCH_ON: name = "SWITCH_ON"; break;
  case PixDcsDefs::SWITCH_OFF: name = "SWITCH_OFF"; break;
  case PixDcsDefs::ENABLE: name = "ENABLE"; break;
  case PixDcsDefs::DISABLE: name = "DISABLE"; break;
  case PixDcsDefs::STARTUP: name = "STARTUP"; break; 
  case PixDcsDefs::SWITCH_ON_OPTO: name = "SWITCH_ON_OPTO"; break;
  case PixDcsDefs::SWITCH_ON_LV: name = "SWITCH_ON_LV"; break;
  case PixDcsDefs::SWITCH_ON_HV: name = "SWITCH_ON_HV"; break;
  case PixDcsDefs::SWITCH_OFF_HV: name = "SWITCH_OFF_HV"; break;
  case PixDcsDefs::BACK_TO_OPTO_ON: name = "BACK_TO_OPTO_ON"; break; 
  case PixDcsDefs::BACK_TO_STARTED: name = "BACK_TO_STARTED"; break; 
  case PixDcsDefs::RESET: name = "RESET"; break;   
  case PixDcsDefs::OPTO_RESET: name = "OPTO_RESET"; break;  
  case PixDcsDefs::RELOAD: name = "RELOAD"; break;   
  case PixDcsDefs::SWITCH_OFF_VVDC: name = "SWITCH_OFF_VVDC"; break;
  case PixDcsDefs::TEST_FSM: name = "TEST"; break;
  default: 
    {
      PixDcsDefs::DDCCommandsResponse reply = PixDcsDefs::ERR_NAME;
      std::stringstream info;
      info << "PixDcs::sendCommand - " << command <<  " is an obsolete command, not implemented anymore, nothing will be done";
      std::cout << info.str() << std::endl;
      ers::warning(PixLib::pix::daq (ERS_HERE, "PixDcs::sendCommand", info.str()));
      return reply;
    }
  }
  parameter = name + " " + connDbName;
  std::string dataPointName = "FSMcommand_"+m_rodName;
  std::cout << "PixDcs::sendCommand: Go to execCommand(" << dataPointName << ", "
	    << parameter << ", " << timeout << ");"<< std::endl;
  status=execCommand(dataPointName, parameter, timeout);
  return status;
}



// New DDC execution command - with parallel commands supported
int PixDcs::execCommand(std::string dataPointName, std::string parameter, unsigned int timeout, bool wait) 
{


  int status;
  std::stringstream info;
  PixDcsDefs::DDCCommandsResponse reply = PixDcsDefs::UNKNOWN_ERR;

  std::cout << "PixDcs::execCommand -  executing ... execCommand(" << dataPointName << ", "
	    << parameter << ", " << timeout << "); on "<< m_rodName << std::endl;

  status = m_ddcCommander->ddcExecCommand("Ctrl_PixelDDC", dataPointName, parameter, timeout, wait);
  std::cout << "PixDcs::execCommand - I have executed DdcCommander::execCommand(" << dataPointName << ", "
	    << parameter << ", " << timeout << "); on "<< m_rodName << std::endl;

  if(status==COMM_ERR_OK) {
    reply = PixDcsDefs::OK;
  } else if (status==COMM_ERR_UNDEF) {
    reply = PixDcsDefs::ERR_UNDEF;
    info << "PixDcs::execCommand - " << parameter << " - " << dataPointName << " - " << m_rodName << " is in undefined status ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_NAME) {
    reply = PixDcsDefs::ERR_NAME;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<" is an invalid command name ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_DCSDP) {
    reply = PixDcsDefs::ERR_DCSDP;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<" is an invalid dcs datapoint definition ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_TIMEOUT) {
    reply = PixDcsDefs::ERR_TIMEOUT;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<" timeout expired ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_CONNECT) {
    reply = PixDcsDefs::ERR_CONNECT;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<"  SCADA communication fault (no connection) ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_CONNBROKEN) {
    reply = PixDcsDefs::ERR_CONNBROKEN;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<"  SCADA communication fault (connection broken) ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_CONFIG) {
    reply = PixDcsDefs::ERR_CONFIG;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName <<" invalid configuration ";
    std::cout << info.str() << std::endl;    
  } else if (status==COMM_ERR_NTDESC) {
    reply = PixDcsDefs::ERR_NTDESC;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName << " invalid definition of an nt-command ";
    std::cout << info.str() << std::endl;
  } else if (status==COMM_ERR_BUSY) {
    reply = PixDcsDefs::ERR_BUSY;
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName << " previous command has not ended ";
    std::cout << info.str() << std::endl;
  } else if (status>0){
    info << "PixDcs::execCommand - " << parameter <<  " - " << dataPointName << " - " << m_rodName << " Pixel DCS error ... wrong state? wrong object name? ";
    ers::warning(PixLib::pix::daq (ERS_HERE, "ddc:execCommand", info.str()));
    reply = PixDcsDefs::ERR_DCS;
  }
  return reply;
}



std::string PixDcs::convertName(std::string connDbName, PixDcsDefs::DataPointType dataPointType)
{
  std::string name = connDbName + "_";
  
  switch(dataPointType) {
  case PixDcsDefs::HVON: name += "HV.ON"; break;
    //case HV_MON: name += "HV.Vmeas"; break;
    //case IHV_MON: name += "HV.Imeas"; break;
  case PixDcsDefs::HV_MON: name += "HV.Vmon"; break;
  case PixDcsDefs::IHV_MON: name += "HV.Imon"; break;
  case PixDcsDefs::VDDD: name += "VDD1.Vset"; break;
  case PixDcsDefs::VDDD_MON: name += "VDD1.Vmon"; break;
  case PixDcsDefs::WIENERD_MON: name += "VDD2.Vmon"; break;
  case PixDcsDefs::VDDA: name += "VDDA1.Vset"; break;
  case PixDcsDefs::VDDA_MON: name += "VDDA1.Vmon"; break;
  case PixDcsDefs::WIENERA_MON: name += "VDDA2.Vmon"; break;
  case PixDcsDefs::IDDD_MON: name += "VDD1.Imon"; break;
  case PixDcsDefs::IDDA_MON: name += "VDDA1.Imon"; break;
  case PixDcsDefs::TMODULE: name += "TModule"; break;
  case PixDcsDefs::TOPTO: name += "TOpto"; break;
  case PixDcsDefs::VISET: name += "VISET.Vset"; break;
  case PixDcsDefs::VISET_MON: name += "VISET.Vmon"; break;
  case PixDcsDefs::IVISET_MON: name += "VISET.Imon"; break;
  case PixDcsDefs::VPIN: name += "VPIN.Vset"; break;
  case PixDcsDefs::VPIN_MON: name += "VPIN.Vmon"; break;
  case PixDcsDefs::IPIN_MON: name += "VPIN.Imon"; break;
  case PixDcsDefs::VVDC: name += "VVDC1.Vset"; break;
  case PixDcsDefs::VVDC_MON: name += "VVDC1.Vmon"; break;
  case PixDcsDefs::IVDC_MON: name += "VVDC1.Imon"; break;
  default: return NULL;
  }
  return name;
}
