// 29 October, 2008
// Since enum cannot be forward declared and I don't want to include PixDcs.h in PixModuleGroup.h
// I need to write an include file to define all the enum.

#ifndef _PIXLIB_PIXDCSDEFS
#define _PIXLIB_PIXDCSDEFS

namespace PixDcsDefs {

    enum DDCCommands{ALL_DDC, SET_HV, SET_VISET, SET_VPIN, SET_VVDC, SET_VDDD, SET_VDDA, TEST_DDC};
    enum Access{ALL_ACCESS, DCS_READONLY, DCS_READWRITE, DAQ_READWRITE};
    enum Type{ALL_TYPE, BOOL, CHAR, INT, UNSIGNED, FLOAT, STRING, DOUBLE};
    enum DataPointType{ALLTYPE, HVON, HV_MON, IHV_MON, VDDD, VDDD_MON, WIENERD_MON, VDDA, VDDA_MON, 
		       WIENERA_MON, IDDD_MON, IDDA_MON, TMODULE, TOPTO, VISET, VISET_MON, IVISET_MON, 
		       VPIN, VPIN_MON, IPIN_MON, VVDC, VVDC_MON, IVDC_MON};
    
    enum FSMCommands {ALL_FSM, SWITCH_ON, SWITCH_OFF, ENABLE, DISABLE, STARTUP, SWITCH_ON_OPTO, SWITCH_ON_LV, 
		      SWITCH_ON_HV, SWITCH_OFF_HV, BACK_TO_OPTO_ON, BACK_TO_STARTED, RESET, OPTO_RESET, 
		      RELOAD, SWITCH_OFF_VVDC, TEST_FSM,
		      // from here: obsolete commands, still in the enum to let old code compile
		      // if used an error message is published and no command is sent
		      SWITCH_ON_VDDD, SWITCH_ON_VDDA, SWITCH_OFF_VDDD, SWITCH_OFF_VDDA, SWITCH_OFF_OPTO, RECOVER,  
		      GOTO_QUIET, BACK_TO_STANDBY, GOTO_STANDBY};
    enum StatusType{STATE, STATUS};
    enum DDCCommandsResponse{OK, ERR_UNDEF, ERR_NAME, ERR_DCSDP, ERR_TIMEOUT, ERR_CONNECT, 
			     ERR_CONNBROKEN, ERR_CONFIG, ERR_NTDESC, ERR_BUSY, ERR_DCS, UNKNOWN_ERR};

}

#endif
