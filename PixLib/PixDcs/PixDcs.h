/////////////////////////////////////////////////////////////////////
// PixDcs.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 03/10/06  Version 1.0 (CS,PR)
//           Initial release
//
// 03/10/06  Version 1.2 (AO)
//           Clean-up done; Added enum able to decide what params/actions to do.
//

//! Pixel DAQ-to-DCS interface class

#ifndef _PIXLIB_PIXDCS
#define _PIXLIB_PIXDCS

#include <iostream>
#include <set>
#include <is/type.h>

#include "BaseException.h"
#include "PixUtilities/PixMessages.h"
#include "PixDcs/PixDcsDefs.h"

class IPCPartition;
class ISInfoDictionary;
class DdcRequest;
class DdcCommander;

#define  REQUESTSERVER "RunCtrl"


namespace PixLib {
  
  //! PixDcs Exception class; an object of this type is thrown in case of a Dcs communication or setting error
  class PixDcsExc : public SctPixelRod::BaseException{
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    PixDcsExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
      virtual ~PixDcsExc() {};

      //! Dump the error
      virtual void dump(std::ostream &out) {
	out << "Pixel Controller " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
      }
      std::string dumpLevel() {
	switch (m_errorLevel) {
	case INFO : 
	  return "INFO";
	case WARNING :
	  return "WARNING";
	case ERROR :
	  return "ERROR";
	case FATAL :
	  return "FATAL";
	default :
	  return "UNKNOWN";
	}
      }
      //! m_errorLevel accessor
      ErrorLevel getErrorLevel() const  { return m_errorLevel; };
      std::string getDescr()const  { return m_descr; };
      std::string getId() const  { return m_id; };
  private:
      ErrorLevel m_errorLevel;
      std::string m_id;
      std::string m_descr;
  };

 class PixDcs {
    

 public:
   
   // Constructor
   PixDcs(IPCPartition* ipcPartition, std::string isServer, std::string rodName, 
	  std::string requestServer=REQUESTSERVER);
   
   // Destructor
   ~PixDcs();
   
   IPCPartition &ipcPartition() { return *m_ipcPartition; };
   
   // read 
   void read(std::string connDbName, PixDcsDefs::DataPointType DPType, float& value);
   bool readDataPoint(std::string connDbName, PixDcsDefs::DataPointType DPType, float& value);

   // SEND COMMAND
   int writeDataPoint(std::string connDbName, float value, PixDcsDefs::DDCCommands type, unsigned int timeout = 60);
   int sendCommand(PixDcsDefs::FSMCommands command, std::string connDbName, unsigned int timeout = 120);
   

 private:
   int execCommand(std::string command, std::string parameter, 
		   unsigned int timeout=60, bool wait=true); // (wait=true means parallel execution)
   std::string convertName(std::string connDbName, PixDcsDefs::DataPointType dataPointType);
   bool initRead(std::string name);
   bool endRead(std::string name);

 private:
   std::string m_isServer;
   std::string m_requestServer;
   std::string m_rodName;
   IPCPartition* m_ipcPartition;
   ISInfoDictionary* m_isDictionary;
   DdcRequest* m_ddcRequest;
   DdcCommander* m_ddcCommander;
   std::vector<std::string> m_subscriptionMem;
      
 };
 
}

#endif

