#include <unistd.h>
#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <oh/OHRawProvider.h>


#include "Histo/Histo.h"
#include "PixController/PixScan.h"
#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixHistoServer/PixNameServerIDL.hh"
#include "PixHistoServer/PixNameServer.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;

int main(int argc, char **argv) {

  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: ListScanInHistoServer [partition name] " << std::endl;
    return 1;
  }

  std::string ipcPartitionName = argv[1];
  std::string serverName = "nameServer";
  std::string isServerName = std::string(OHSERVERBASE)+"PixNameServerInternal";
  std::string program = "ListScansInHistoServer";
  std::string providerName = std::string(PROVIDERBASE)+program;
  std::string fol = "/";
  std::vector<std::string> folder;
  
  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  std::stringstream info;

  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(ipcPartition, isServerName, 
							       serverName, providerName);
    std::cout<<std::endl;
    folder  = hInt->lsFolder(fol);
    if (folder.size() != 0) {
      std::cout << "The Histogram server contains: "<< std::endl;
      for(unsigned int i =0; i<folder.size(); i++){
	std::cout<< folder[i] <<std::endl;
      }
      std::cout<<std::endl;
    } else {
      std::cout<< "NO FOLDERS HAVE BEEN FOUND" <<std::endl;
    }
  } catch (PixHistoServerExc e) {
    if (e.getId() == "NOISSERVER") {      
      info << "ListScansInHistoServer - no IS Server "<< isServerName << " has been found. ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, program, info.str()));
    } else if (e.getId() == "NOPARTITION") {
      info << "ListScansInHistoServer - no IPC partition "<< ipcPartitionName << " has been found. ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, program, info.str()));
    } else if (e.getId() == "NONAMESERVER") {
      info << "ListScansInHistoServer - no name server  "<< serverName << " is runnnig ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, program, info.str()));
    } else if (e.getId() == "NOLS") {
      info << "ListScansInHistoServer - The name server is empty ";
      ers::warning(PixLib::pix::daq (ERS_HERE, program, info.str()));
    }   
  } catch (...) {
    std::cout << program << ":  Unknown Exception was thrown" << std::endl;
  }
  return 0;
}

