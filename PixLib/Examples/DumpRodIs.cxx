/*********************************************************/
/* Get STDDUMP from IS and dump to file                  */
/*                                                       */
/*   Jens Dopke, 20160611, for Debugging                 */
/*       jens.dopke@stfc.ac.uk                           */
/*********************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <set>
#include <string>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixUtilities/PixISManager.h"

#include "RodCrate/registeraddress.h"
//#include "sysParams.h"

using namespace std;
using namespace PixLib;

#include <ctype.h>

#define N_FORMATTERS  8
#define ROD_BUSY_STATUS           (4 + RRIF_STATUS_0)

int main(int argc, char *argv[]) {

  bool verbose = true;
  std::string ipcPartitionName;
  std::string isServerName = "RunParams";
  std::string rodName;
  std::string crateName;
  std::string filename;

  // Check arguments
  if(argc<4) {
    std::cout << "usage: ./DumpRodIs CrateName RodName Filename" << std::endl;
    exit(0);
  } else {
    char *part;
    part = getenv("PIX_PART_INFR_NAME");
    if (part != NULL) {
      ipcPartitionName = part;
    } else {
      ipcPartitionName = "PixelInfr";
    }
    rodName = argv[2];
    crateName = argv[1];
    filename = argv[3];
  }

  // Start IPCCore
  IPCCore::init(argc, argv);
  
  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;

  // CREATE ISManager
  PixISManager* m_isManager;
  m_isManager = new PixISManager(ipcPartitionName,isServerName);
 


  // start address of FPGA sections of ROD
  long unsigned int dspStart[4]    = {0x400000,0x402000, 0x402400, 0x404400 };

  /* by default the formatter dump contains ALL rod registers */
  /* load saved register dump into buffer for decoding        */ 
#define registerlength 0x2000
  unsigned int * buffer = new unsigned int[registerlength];
  int time;
  std::vector<unsigned int> v = m_isManager->read< std::vector<unsigned int> >(crateName+"/"+rodName+"/STDDUMP", time);
  for (unsigned int iv=0; iv<v.size(); iv++) {
    if (iv < 0x2000) buffer[iv] = v[iv];
  }

  ofstream outFile(filename.c_str(), ios::binary);
    
  // Save file 
  outFile.write((char *)buffer, 0x8000);

  outFile.close();

  return 0;  
}


