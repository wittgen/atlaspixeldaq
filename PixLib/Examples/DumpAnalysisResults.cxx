/**
 * @file DumpAnalysisResults.cxx
 * @brief Print calibration analysis results
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2015/03/28
 */

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <set>
#include <cmath>
#include <ctime>
#include <iomanip>
#include <exception>

#include "PixelCoralClientUtils/CoralClient.hh"

coral::ISessionProxy* connect(std::string dbConnString, coral::AccessMode dbAccessMode)
{
  coral::Context* context = &coral::Context::instance();
  coral::IHandle<coral::IConnectionService> handle = context->query<coral::IConnectionService>();
  if (!handle.isValid()) {
    context->loadComponent("CORAL/Services/ConnectionService");
    handle = context->query<coral::IConnectionService>();
  }
  if (!handle.isValid()) {
    throw std::runtime_error("Could not locate connection service");
  }
  return handle->connect(dbConnString,dbAccessMode);
}

int main(int argc, char** argv)
{
  std::string analysisNumber;

  for (int i=1; i<argc-1; i+=2) {
    std::string option(argv[i]);
    std::string argument(argv[i+1]);
    if (option.compare("-a") == 0) analysisNumber = argument;
  }

  if (analysisNumber.empty()) {
    std::cout << "usage: ./DumpAnalysisResults -a <analysisNumber>" << std::endl;
    std::cout << "e.g. ./DumpAnalysisResults -a 16062" << std::endl;
    return 1;
  }

  CAN::SerialNumber_t analysisID = std::stoi(analysisNumber);
  std::cout << "\nINFO ==>> Fetching results for analysis: " << analysisID << std::endl;

  std::string dbConnString(getenv("CAN_CALIB_DB"));
  coral::AccessMode dbAccessMode = coral::ReadOnly;
  std::string dbTableName("CALIB_ANAL");
  coral::ISessionProxy* dbSession = connect(dbConnString, dbAccessMode);
  dbSession->transaction().start(true);

  try {
    const coral::ITableDescription& dbTable = dbSession->nominalSchema().tableHandle(dbTableName).description();
    coral::IQuery* dbQuery = dbSession->nominalSchema().tableHandle(dbTableName).newQuery();

    for (int icol = 0; icol < dbTable.numberOfColumns(); icol++) {
      const coral::IColumn& column = dbTable.columnDescription(icol);
      std::string columnName(column.name());
      if (column.name() == "DATE") columnName = "\""+column.name()+"\"";
      dbQuery->addToOutputList(columnName);
      //std::cout << dbTableName << "  " << columnName << "  " << column.type() << std::endl;
    }

    dbQuery->defineOutputType("ANAL_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    dbQuery->defineOutputType("\"DATE\"",coral::AttributeSpecification::typeNameForType<time_t>());
    std::string dbQueryCond = dbTableName+".ANAL_ID = :analid";
    coral::AttributeList dbQueryCondData;
    dbQueryCondData.extend<CAN::SerialNumber_t>("analid");
    dbQueryCondData["analid"].data<CAN::SerialNumber_t>() = analysisID;
    dbQuery->setCondition(dbQueryCond, dbQueryCondData);
    coral::ICursor& dbCursor = dbQuery->execute();

    while (dbCursor.next()) {
      const coral::AttributeList& row = dbCursor.currentRow();
      CAN::SerialNumber_t id = row[0].data<CAN::SerialNumber_t>();
      struct tm* tm = localtime(&(row[1].data<time_t>()));
      char date[20]; strftime(date, sizeof(date), "%Y-%m-%d %H:%M", tm);
      long long fk = row[2].data<long long>();
      std::cout << id << "  " << date << "  " << fk << std::endl;

      std::set<std::string> tableSet = dbSession->nominalSchema().listTables();
      std::set<std::string>::const_iterator tableItrI = tableSet.begin();
      std::set<std::string>::const_iterator tableItrE = tableSet.end();
      for (; tableItrI != tableItrE; tableItrI++) {
	std::string tableName(*tableItrI);
	if (tableName.find(dbTableName+"_") == std::string::npos) continue;
	coral::IQuery* query = dbSession->nominalSchema().tableHandle(tableName).newQuery();
	const coral::ITableDescription& table = dbSession->nominalSchema().tableHandle(tableName).description();
	std::vector<std::string> columnNameVec;
	std::vector<std::string> columnTypeVec;
	for (int icol = 0; icol < table.numberOfColumns(); icol++) {
	  const coral::IColumn& column = table.columnDescription(icol);
	  std::string columnName(column.name());
	  query->addToOutputList(columnName);
	  columnNameVec.push_back(column.name());
	  columnTypeVec.push_back(column.type());
	  //std::cout << tableName << "  " << columnName << "  " << column.type() << std::endl;
	}

	std::string queryCond = tableName+".FK = :analfk";
	coral::AttributeList queryCondData;
	queryCondData.extend<long long>("analfk");
	queryCondData["analfk"].data<long long>() = fk;
	query->setCondition(queryCond, queryCondData);
	coral::ICursor& cursor = query->execute();

	while (cursor.next()) {
	  const coral::AttributeList& rowi = cursor.currentRow();
	  std::cout << columnNameVec[0] << ": " << rowi[0].data<long long>() << "  "
		    << columnNameVec[1] << ": " << rowi[1].data<std::string>() << "  "
		    << columnNameVec[2] << ": " << rowi[2].data<std::string>() << "  ";
	  for (int i = 3; i < table.numberOfColumns(); i++) {
	    if (columnTypeVec[i] == "float") std::cout << columnNameVec[i] << ": " << rowi[i].data<float>() << "  ";
	    else if (columnTypeVec[i] == "int") std::cout << columnNameVec[i] << ": " << rowi[i].data<int>() << "  ";
	    else if (columnTypeVec[i] == "bool") std::cout << columnNameVec[i] << ": " << rowi[i].data<bool>() << "  ";
	    else if (columnTypeVec[i] == "long long") std::cout << columnNameVec[i] << ": " << rowi[i].data<long long>();
	  }
	  std::cout << std::endl;
	}
	delete query;
      }
    }
    delete dbQuery;
  }
  catch (std::exception& exc) {
    std::cerr << "exception caught: " << exc.what() << std::endl;
  }

  dbSession->transaction().commit();
  delete dbSession;
  std::cout << "\nINFO ==>> End of execution\n" << std::endl;

  return 0;
}
