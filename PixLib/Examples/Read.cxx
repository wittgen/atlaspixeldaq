// April 2009
// 
// Stress test of DCS, to check the subscription in DIM using DDC-DT

// Variable types:
// D1A_B01_S1_TOpto          
// D1A_B01_S1_VISET.Vmon
// D1A_B01_S1_VISET.Imon
// D1A_B01_S1_VPIN.Imon
// D1A_B01_S1_VPIN.Vmon
// D1A_B01_S1_VVDC1.Imon
// D1A_B01_S1_VVDC1.Vmon


#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <ipc/partition.h>
#include <ipc/core.h>
#include <is/infodictionary.h>
#include <is/infodocument.h>
#include <is/info.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <ddc/DdcData.hxx>
#include <ddc/DdcRequest.hxx>

#include "PixDcs/PixDcs.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;


int main(int argc, char **argv) {

  if(argc<2) {
    std::cout << "USAGE: Read [In file name] " << std::endl;
    return 1;
  }

  std::string fileNameIn=argv[1];
  std::string ipcPartitionName="PixelInfr";
  std::string isServerName="RunParams";
  std::vector<std::string> collection;
  //  std::string rodName = "ROD_B1_S10";

  std::ifstream filein(fileNameIn.c_str());
  if(!filein) std::cout << " I can't read "<< fileNameIn << std::endl;
  while(!filein.eof()) {
    std::string word;
    filein >> word;
    std::cout << word << std::endl;
    collection.push_back(word+" => "+isServerName);
  }

  // START IPC
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  
  std::string errorMsg;
  std::string requestServer = "RunCtrl";
  DdcRequest *ddcRequest = new DdcRequest(*ipcPartition, requestServer);

//   for (int i=0; i!=collection.size(); ++i) {
//     std::string daqRequest = collection[i]; // + " => " + isServerName;
//     bool status = ddcRequest->ddcMoreImportOnChange(daqRequest,errorMsg);
//     if(!status) {
//       msg->publishMessage(PixMessages::ERROR, "Read.cxx", "ddcMoreImportOnChange=false");
//     }
//     else {
//       msg->publishMessage(PixMessages::SUCCESS, "Read.cxx", "ddcMoreImportOnChange=true");
//     }
//   }


  bool status = ddcRequest->ddcMoreImportOnChange(collection,errorMsg);
  if(!status) {
    sleep(1);
    ers::error(PixLib::pix::daq (ERS_HERE, "Read.cxx", "ddcMoreImportOnChange=false"));
  }
  else {
    //  msg->printMessage(PixMessages::SUCCESS, "Read.cxx", "ddcMoreImportOnChange=true");
  }
  
  //   sleep(3);
  //   bool status2 = ddcRequest->ddcCancelImport(collection, errorMsg);
  //   if(!status2) {
  //     sleep(1);
  //     msg->publishMessage(PixMessages::ERROR, "Read.cxx", "ddcMoreImportOnChange=false");
  //   }
  //   else {
  //     msg->publishMessage(PixMessages::SUCCESS, "Read.cxx", "ddcMoreImportOnChange=true");
  //   }


  filein.close(); 
  return 0;
}
