#!/bin/sh
#
export cmd="${PIX_LIB}/Examples/CreateCLDis --connTag PIT-ALL-V30 --cfgTag PIT --idTag PIXEL --pendTag Test"

$cmd --clName cooling_loop3  --disName CL3
$cmd --clName cooling_loop5  --disName CL5
$cmd --clName cooling_loop6  --disName CL6
$cmd --clName cooling_loop17 --disName CL17
$cmd --clName cooling_loop4  --disName CL4
$cmd --clName cooling_loop7  --disName CL7
$cmd --clName cooling_loop16 --disName CL16
$cmd --clName cooling_loop18 --disName CL18

$cmd --clName cooling_loop3  \
     --clName cooling_loop5  \
     --clName cooling_loop6  \
     --clName cooling_loop17 \
     --clName cooling_loop4  \
     --clName cooling_loop7  \
     --clName cooling_loop16 \
     --clName cooling_loop18 --disName Q1A

$cmd --clName cooling_loop8  --disName CL8
$cmd --clName cooling_loop9  --disName CL9
$cmd --clName cooling_loop11 --disName CL11
$cmd --clName cooling_loop14 --disName CL14
$cmd --clName cooling_loop19 --disName CL19
$cmd --clName cooling_loop21 --disName CL21
$cmd --clName cooling_loop10 --disName CL10
$cmd --clName cooling_loop12 --disName CL12
$cmd --clName cooling_loop13 --disName CL13
$cmd --clName cooling_loop15 --disName CL15
$cmd --clName cooling_loop20 --disName CL20

$cmd --clName cooling_loop8  \
     --clName cooling_loop9  \
     --clName cooling_loop11 \
     --clName cooling_loop14 \
     --clName cooling_loop19 \
     --clName cooling_loop21 \
     --clName cooling_loop10 \
     --clName cooling_loop12 \
     --clName cooling_loop13 \
     --clName cooling_loop15 \
     --clName cooling_loop20 --disName Q1C

$cmd --clName cooling_loop3  \
     --clName cooling_loop5  \
     --clName cooling_loop6  \
     --clName cooling_loop17 \
     --clName cooling_loop4  \
     --clName cooling_loop7  \
     --clName cooling_loop16 \
     --clName cooling_loop18 \
     --clName cooling_loop8  \
     --clName cooling_loop9  \
     --clName cooling_loop11 \
     --clName cooling_loop14 \
     --clName cooling_loop19 \
     --clName cooling_loop21 \
     --clName cooling_loop10 \
     --clName cooling_loop12 \
     --clName cooling_loop13 \
     --clName cooling_loop15 \
     --clName cooling_loop20 --disName Q1

$cmd --clName cooling_loop24 --disName CL24
$cmd --clName cooling_loop27 --disName CL27
$cmd --clName cooling_loop28 --disName CL28
$cmd --clName cooling_loop39 --disName CL39
$cmd --clName cooling_loop25 --disName CL25
$cmd --clName cooling_loop26 --disName CL26
$cmd --clName cooling_loop29 --disName CL29
$cmd --clName cooling_loop38 --disName CL38
$cmd --clName cooling_loop40 --disName CL40

$cmd --clName cooling_loop24 \
     --clName cooling_loop27 \
     --clName cooling_loop28 \
     --clName cooling_loop39 \
     --clName cooling_loop25 \
     --clName cooling_loop26 \
     --clName cooling_loop29 \
     --clName cooling_loop38 \
     --clName cooling_loop40 --disName Q2A

$cmd --clName cooling_loop30 --disName CL30
$cmd --clName cooling_loop32 --disName CL32
$cmd --clName cooling_loop35 --disName CL35
$cmd --clName cooling_loop36 --disName CL36
$cmd --clName cooling_loop41 --disName CL41
$cmd --clName cooling_loop43 --disName CL43
$cmd --clName cooling_loop31 --disName CL31
$cmd --clName cooling_loop33 --disName CL33
$cmd --clName cooling_loop34 --disName CL34
$cmd --clName cooling_loop37 --disName CL37
$cmd --clName cooling_loop42 --disName CL42

$cmd --clName cooling_loop30 \
     --clName cooling_loop32 \
     --clName cooling_loop35 \
     --clName cooling_loop36 \
     --clName cooling_loop41 \
     --clName cooling_loop43 \
     --clName cooling_loop31 \
     --clName cooling_loop33 \
     --clName cooling_loop34 \
     --clName cooling_loop37 \
     --clName cooling_loop42 --disName Q2C

$cmd --clName cooling_loop24 \
     --clName cooling_loop27 \
     --clName cooling_loop28 \
     --clName cooling_loop39 \
     --clName cooling_loop25 \
     --clName cooling_loop26 \
     --clName cooling_loop29 \
     --clName cooling_loop38 \
     --clName cooling_loop40 \
     --clName cooling_loop30 \
     --clName cooling_loop32 \
     --clName cooling_loop35 \
     --clName cooling_loop36 \
     --clName cooling_loop41 \
     --clName cooling_loop43 \
     --clName cooling_loop31 \
     --clName cooling_loop33 \
     --clName cooling_loop34 \
     --clName cooling_loop37 \
     --clName cooling_loop42 --disName Q2

$cmd --clName cooling_loop45 --disName CL45
$cmd --clName cooling_loop52 --disName CL52
$cmd --clName cooling_loop54 --disName CL54
$cmd --clName cooling_loop55 --disName CL55
$cmd --clName cooling_loop44 --disName CL44
$cmd --clName cooling_loop46 --disName CL46
$cmd --clName cooling_loop53 --disName CL53
$cmd --clName cooling_loop56 --disName CL56
$cmd --clName cooling_loop57 --disName CL57

$cmd --clName cooling_loop45 \
     --clName cooling_loop52 \
     --clName cooling_loop54 \
     --clName cooling_loop55 \
     --clName cooling_loop44 \
     --clName cooling_loop46 \
     --clName cooling_loop53 \
     --clName cooling_loop56 \
     --clName cooling_loop57 --disName Q3A

$cmd --clName cooling_loop47 --disName CL47
$cmd --clName cooling_loop49 --disName CL49
$cmd --clName cooling_loop58 --disName CL58
$cmd --clName cooling_loop60 --disName CL60
$cmd --clName cooling_loop61 --disName CL61
$cmd --clName cooling_loop63 --disName CL63
$cmd --clName cooling_loop48 --disName CL48
$cmd --clName cooling_loop59 --disName CL59
$cmd --clName cooling_loop62 --disName CL62
$cmd --clName cooling_loop64 --disName CL64
$cmd --clName cooling_loop65 --disName CL65

$cmd --clName cooling_loop47 \
     --clName cooling_loop49 \
     --clName cooling_loop58 \
     --clName cooling_loop60 \
     --clName cooling_loop61 \
     --clName cooling_loop63 \
     --clName cooling_loop48 \
     --clName cooling_loop59 \
     --clName cooling_loop62 \
     --clName cooling_loop64 \
     --clName cooling_loop65 --disName Q3C

$cmd --clName cooling_loop45 \
     --clName cooling_loop52 \
     --clName cooling_loop54 \
     --clName cooling_loop55 \
     --clName cooling_loop44 \
     --clName cooling_loop46 \
     --clName cooling_loop53 \
     --clName cooling_loop56 \
     --clName cooling_loop57 \
     --clName cooling_loop47 \
     --clName cooling_loop49 \
     --clName cooling_loop58 \
     --clName cooling_loop60 \
     --clName cooling_loop61 \
     --clName cooling_loop63 \
     --clName cooling_loop48 \
     --clName cooling_loop59 \
     --clName cooling_loop62 \
     --clName cooling_loop64 \
     --clName cooling_loop65 --disName Q3

$cmd --clName cooling_loop67 --disName CL67
$cmd --clName cooling_loop74 --disName CL74
$cmd --clName cooling_loop75 --disName CL75
$cmd --clName cooling_loop77 --disName CL77
$cmd --clName cooling_loop66 --disName CL66
$cmd --clName cooling_loop68 --disName CL68
$cmd --clName cooling_loop76 --disName CL76
$cmd --clName cooling_loop78 --disName CL78
$cmd --clName cooling_loop79 --disName CL79

$cmd --clName cooling_loop67 \
     --clName cooling_loop74 \
     --clName cooling_loop75 \
     --clName cooling_loop77 \
     --clName cooling_loop66 \
     --clName cooling_loop68 \
     --clName cooling_loop76 \
     --clName cooling_loop78 \
     --clName cooling_loop79 --disName Q4A

$cmd --clName cooling_loop69 --disName CL69
$cmd --clName cooling_loop71 --disName CL71
$cmd --clName cooling_loop80 --disName CL80
$cmd --clName cooling_loop82 --disName CL82
$cmd --clName cooling_loop83 --disName CL83
$cmd --clName cooling_loop86 --disName CL86
$cmd --clName cooling_loop70 --disName CL70
$cmd --clName cooling_loop81 --disName CL81
$cmd --clName cooling_loop84 --disName CL84
$cmd --clName cooling_loop85 --disName CL85
$cmd --clName cooling_loop87 --disName CL87
$cmd --clName cooling_loop88 --disName CL88

$cmd --clName cooling_loop69 \
     --clName cooling_loop71 \
     --clName cooling_loop80 \
     --clName cooling_loop82 \
     --clName cooling_loop83 \
     --clName cooling_loop86 \
     --clName cooling_loop70 \
     --clName cooling_loop81 \
     --clName cooling_loop84 \
     --clName cooling_loop85 \
     --clName cooling_loop87 \
     --clName cooling_loop88 --disName Q4C

$cmd --clName cooling_loop67 \
     --clName cooling_loop74 \
     --clName cooling_loop75 \
     --clName cooling_loop77 \
     --clName cooling_loop66 \
     --clName cooling_loop68 \
     --clName cooling_loop76 \
     --clName cooling_loop78 \
     --clName cooling_loop79 \
     --clName cooling_loop69 \
     --clName cooling_loop71 \
     --clName cooling_loop80 \
     --clName cooling_loop82 \
     --clName cooling_loop83 \
     --clName cooling_loop86 \
     --clName cooling_loop70 \
     --clName cooling_loop81 \
     --clName cooling_loop84 \
     --clName cooling_loop85 \
     --clName cooling_loop87 \
     --clName cooling_loop88 --disName Q4

