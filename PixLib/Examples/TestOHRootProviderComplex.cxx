// Test the OHRootProvider performnce

#include <unistd.h>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <memory>

#include "PixHistoServer/my_rootReceiver.h"

#include <oh/OHRootHistogram.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>
#include <oh/OHStream.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoT.h>
#include <is/serveriterator.h>

#include <TClass.h> 
#include <TROOT.h>
#include <TApplication.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TBenchmark.h>


int main(int argc, char **argv) 
{
  if(argc<3) {
    std::cout << "USAGE: TestOHRootProviderComplex [partition name] [root file name]" << std::endl;
    return 1;
  }
  std::string ipcPartitionName = argv[1];
  std::string fileName = argv[2];
  std::string isServerName = "pixel_histo_server_crate0";

  std::string tempo = "Writing Time";
  std::string retrieving = "Retrieving Time";
  TBenchmark *t = new TBenchmark();

  my_rootReceiver pixelReceiver;

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  TFile* outFile = new TFile(fileName.c_str(), "RECREATE");
  //outFile->SetCompressionLevel(0);
  std::string mainFold = "SCAN";
  TDirectory* newDir = gDirectory->mkdir(mainFold.c_str());
  newDir = gDirectory->GetDirectory("SCAN");

  for (int nf=0; nf<1000; nf++) {
    newDir->cd();
    std::stringstream secondF;
    secondF << "ROD" << nf;
    TDirectory* secondDir = gDirectory->mkdir(secondF.str().c_str());
    secondDir->cd();
    std::string thirdF = "OCCUP";
    TDirectory* thirdDir = gDirectory->mkdir(thirdF.c_str());
    thirdDir->cd();
    std::string fourthF = "A";
    TDirectory* fourthDir = gDirectory->mkdir(fourthF.c_str());
    fourthDir->cd();
    std::string fifthF = "B";
    TDirectory* fifthDir = gDirectory->mkdir(fifthF.c_str());
    fifthDir->cd();
    std::string sixthF = "C";
    TDirectory* sixthDir = gDirectory->mkdir(sixthF.c_str());
  }

  t->Start(retrieving.c_str());
  for (int nh=0; nh<1000; nh++) {
    //    newDir->cd();
    std::stringstream secondF;
    secondF << "ROD" << nh;
    gDirectory->cd((fileName+":/SCAN/"+secondF.str()+"/OCCUP/A/B/C").c_str());
    std::string histogramWildCard = "/S1/"+secondF.str()+".*";

    // iterator
//     OHHistogramIterator ith(*ipcPartition, isServerName, ".*", histogramWildCard);
//     while(ith++) {
//       std::string histoName = ith.name();
//       try {
// 	ith.retrieve(pixelReceiver);
	
//     // OHRootHistoTest
//     OHHistogramIterator ith(*ipcPartition, isServerName, ".*", histogramWildCard);
//     while(ith++) {
//       std::string histoName = ith.name();
//       try {
// 	OHRootHistogram his=OHRootReceiver::getRootHistogram(*ipcPartition, isServerName, "pixel_provider_pro", histoName, -1);
// 	TH1* copy = his.histogram.release();
// 	copy->SetDirectory(gDirectory);


    // stream
    OHHistogramStream ith(*ipcPartition, isServerName, ".*", histogramWildCard);
    while(!ith.eof()) {
      try {
	ith.retrieve(pixelReceiver);
	

      } catch (daq::oh::ObjectNotFound) {
	std::cout << "Object not found in OH " << std::endl;
      } catch (daq::oh::InvalidObject) {
	std::cout << " Invalid Obj " << std::endl;
      } catch (...) {
	std::string mess = "Unknown exception thrown ";
      }
    }
    
  }
  t->Show(retrieving.c_str());
  
  t->Start(tempo.c_str());
  outFile->Write(); 
  outFile->Close();
  t->Show(tempo.c_str());
  std::cout << "written to disk" << std::endl;
  
  return 0;
}


