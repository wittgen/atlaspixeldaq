#include "PixUtilities/PixStoplessRecovery.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <thread>

void help(){
  std::cout << "Please provide proper args:AutoRecover ROD_NAME [--part InfrPartition]" << std::endl;
  std::cout << "Examples:"<<std::endl;
  std::cout << "AutoRecover ROD_C1_S17"<<std::endl;
  std::cout << "AutoRecover ROD_C1_S17,ROD_C3_S11,ROD_C3_S15"<<std::endl;
  std::cout << "You can provide many RODs separated with commas"<<std::endl;
}

int main(int argc, char** argv) {

  std::string ipcPartitionName = "PixelInfr";
  std::string ctrlPixelName = "Pixel";
  std::string rod = "";

  int ipp = 1;
  while (ipp < argc) {
    std::string par = argv[ipp];
    if (par == "--part" && ipp+1 < argc) {
      ipcPartitionName = argv[ipp+1];
      ipp = ipp+2;
    } else if (par == "--help" ||  par == "-h") {
      help();
      return EXIT_FAILURE;
    } else {
	rod = par;
	ipp = ipp+1;
      }
    }

  if(rod==""){
    help();
    return EXIT_FAILURE;
  }

  // Start IPCCore    
  IPCCore::init(argc, argv); 

  std::map  <std::string, std::string> itemMap;
  
  std::vector<std::thread> workers;
    
    if (!PixLib::PixStoplessRecovery::getParStoplessRecovery(ipcPartitionName, rod ,itemMap ) ){
      help();
      return EXIT_FAILURE;
    }

    for(const auto & it : itemMap){
      
      if(it.first != it.second){
        std::cout<<"Please provide ROD name"<<std::endl;
        continue;
      }
      std::cout<<"Trying to RECOVER "<<it.first<<std::endl;
      
      workers.emplace_back(std::thread( PixLib::PixStoplessRecovery::autoRecover,ipcPartitionName,it.first, ctrlPixelName) );
  }

  for(auto & w : workers)w.join();

return EXIT_SUCCESS;

}


