/**
 * @file PixelCalibration.cxx
 * @brief Pixel ToT-charge calibration parametrisation
 * @author Tayfun Ince <tayfun.ince@cern.ch>
 * @date 2015/01/26
 */

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <iomanip>
#include "TROOT.h"
#include "TApplication.h"
#include "TFile.h"
#include "TDirectoryFile.h"
#include "TString.h"
#include "TObjString.h"
#include "TKey.h"
#include "TList.h"
#include "TH1F.h"
#include "TH2F.h"
#include "TGraphErrors.h"
#include "TF1.h"

namespace {
  const int nchip = 16;
  const int ncol = 18;
  const int nrow = 160;
  const int ncharge = 6;
  const float chargeArr[ncharge] = {5000, 10000, 15000, 20000, 25000, 30000};
  const	float chargeErrArr[ncharge] = {0, 0, 0, 0, 0, 0};
}
  
int getChipId(int col, int row)
{
  if (row <= nrow) return (col-1)/ncol;
  else return nchip-1-((col-1)/ncol);
}

std::string getPixelType(int col, int row)
{
  int chipCol;
  int chipRow;
  if (row <= nrow) {
    chipCol = (col-1)%ncol;
    chipRow = row-1;
  }
  else {
    chipCol = ncol-1-((col-1)%ncol);
    chipRow = 2*nrow-1-row-1;
  }
  if ((chipCol > 0) && (chipCol < ncol-1)) {
    if (chipRow >= 152) return std::string("ganged");
    else return std::string("normal");
  }
  else {
    if (chipRow >= 152) return std::string("ganged");
    else return std::string("long");
  }
}

double funcTot(double* x, double* par)
{
 double ret = 9.9e10;
 double num = x[0]+par[1];
 double denom = x[0]+par[2];
 if (denom != 0.0) ret = par[0]*(num/denom);
 return ret;
}

double funcDisp(double* x, double* par)
{  
  double ret = 9.9e10;
  ret = par[0]+par[1]*x[0];
  return ret;
}

int main(int argc, char** argv)
{
  std::string thrFileName;
  std::string intFileName;
  std::string totFileName;
  for (int i=1; i<argc-1; i+=2) {
    std::string option(argv[i]);
    std::string argument(argv[i+1]);
    if (option.compare("--thr") == 0) thrFileName = argument;
    if (option.compare("--int") == 0) intFileName = argument;
    if (option.compare("--tot") == 0) totFileName = argument;
  }

  if (thrFileName.empty() || intFileName.empty() || totFileName.empty()) {
    std::cout << "usage: ./PixelCalibration "
	      << "--thr <thresholdScan> --int <intimeThresholdScan> --tot <totCalibScan>"
	      << std::endl;
    return 1;
  }

  std::map<std::string,std::map<std::string,std::map<std::string, float> > > pcdMap;

  // threshold and sigma
  {
    TFile thrFile(thrFileName.c_str(),"READ");
    TString chi2HistName = "SCURVE_CHI2";
    TString thrHistName = "SCURVE_MEAN";
    TString sigHistName = "SCURVE_SIGMA";
    TDirectoryFile* scanDir = (TDirectoryFile*)((TKey*)thrFile.GetListOfKeys()->First())->ReadObj();
    TList* rodKeyList = (TList*)scanDir->GetListOfKeys();
    TIter rodItr(rodKeyList);
    TKey* rodKey;
    while ((rodKey=(TKey*)rodItr())) {
      TString rodName(rodKey->GetName());
      TDirectoryFile* rodDir = (TDirectoryFile*)rodKey->ReadObj();
      TList* modKeyList = (TList*)rodDir->GetListOfKeys();
      TIter modItr(modKeyList);
      TKey* modKey;
      while ((modKey=(TKey*)modItr())) {
	TString modName(modKey->GetName());
	std::string modStr(modKey->GetName());
	if (modName.Contains("DSP")) continue;
	std::map<TString,TH1F*> thrHistNormMap;
	std::map<TString,TH1F*> sigHistNormMap;
	std::map<TString,TH1F*> thrHistLongMap;
	std::map<TString,TH1F*> sigHistLongMap;
	std::map<TString,TH1F*> thrHistGangMap;
	std::map<TString,TH1F*> sigHistGangMap;
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  thrHistNormMap[chipName] = new TH1F("thrNorm"+chipName, "", 500, 0, 10000);
	  sigHistNormMap[chipName] = new TH1F("sigNorm"+chipName, "", 500, 0, 1000);
	  thrHistLongMap[chipName] = new TH1F("thrLong"+chipName, "", 500, 0, 10000);
	  sigHistLongMap[chipName] = new TH1F("sigLong"+chipName, "", 500, 0, 1000);
	  thrHistGangMap[chipName] = new TH1F("thrGang"+chipName, "", 500, 0, 10000);
	  sigHistGangMap[chipName] = new TH1F("sigGang"+chipName, "", 500, 0, 1000);
	}
	TString chi2HistDirPath = modName + "/" + chi2HistName + "/A0/B0";
	TDirectoryFile* chi2HistDir = (TDirectoryFile*)rodDir->Get(chi2HistDirPath);
	TH2F* h2dChi2 = (TH2F*)((TKey*)chi2HistDir->GetListOfKeys()->First())->ReadObj();
	TString thrHistDirPath = modName + "/" + thrHistName + "/A0/B0";
	TDirectoryFile* thrHistDir = (TDirectoryFile*)rodDir->Get(thrHistDirPath);
	TH2F* h2dThr = (TH2F*)((TKey*)thrHistDir->GetListOfKeys()->First())->ReadObj();
	TString sigHistDirPath = modName + "/" + sigHistName + "/A0/B0";
	TDirectoryFile* sigHistDir = (TDirectoryFile*)rodDir->Get(sigHistDirPath);
	TH2F* h2dSig = (TH2F*)((TKey*)sigHistDir->GetListOfKeys()->First())->ReadObj();
	for (int col = 1; col <= ncol*nchip/2; col++) {
	  for (int row = 1; row <= nrow*2; row++) {
	    float chi2 = h2dChi2->GetBinContent(col,row);
	    float thr = h2dThr->GetBinContent(col,row);
	    float sig = h2dSig->GetBinContent(col,row);
	    if (thr <= 0 || thr > 10000 || sig <= 0 || sig > 1000 || chi2 <= 0 || chi2 > 10.0) continue;
	    TString chipName = "I";
	    chipName += getChipId(col, row);
	    std::string pixelType = getPixelType(col, row);
	    if (pixelType.compare("normal") == 0) {
	      thrHistNormMap[chipName]->Fill(thr);
	      sigHistNormMap[chipName]->Fill(sig);
	    }
	    else if (pixelType.compare("long") == 0) {
	      thrHistLongMap[chipName]->Fill(thr);
	      sigHistLongMap[chipName]->Fill(sig);
	    }
	    else if (pixelType.compare("ganged") == 0) {
	      thrHistGangMap[chipName]->Fill(thr);
	      sigHistGangMap[chipName]->Fill(sig);
	    }
	  }
	}
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  std::string chipStr(chipName.Data());
	  pcdMap[modStr][chipStr]["ThrNorm"] = thrHistNormMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["ThrRmsNorm"] = thrHistNormMap[chipName]->GetRMS();
	  pcdMap[modStr][chipStr]["ThrSigNorm"] = sigHistNormMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["ThrLong"] = thrHistLongMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["ThrRmsLong"] = thrHistLongMap[chipName]->GetRMS();
	  pcdMap[modStr][chipStr]["ThrSigLong"] = sigHistLongMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["ThrGang"] = thrHistGangMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["ThrRmsGang"] = thrHistGangMap[chipName]->GetRMS();
	  pcdMap[modStr][chipStr]["ThrSigGang"] = sigHistGangMap[chipName]->GetMean();
	  delete thrHistNormMap[chipName];
	  delete sigHistNormMap[chipName];
	  delete thrHistLongMap[chipName];
	  delete sigHistLongMap[chipName];
	  delete thrHistGangMap[chipName];
	  delete sigHistGangMap[chipName];
	}
      }
    }
  }

  // intime threshold
  {
    TFile intFile(intFileName.c_str(),"READ");
    TString chi2HistName = "SCURVE_CHI2";
    TString thrHistName = "SCURVE_MEAN";
    TString sigHistName = "SCURVE_SIGMA";
    TDirectoryFile* scanDir = (TDirectoryFile*)((TKey*)intFile.GetListOfKeys()->First())->ReadObj();
    TList* rodKeyList = (TList*)scanDir->GetListOfKeys();
    TIter rodItr(rodKeyList);
    TKey* rodKey;
    while ((rodKey=(TKey*)rodItr())) {
      TString rodName(rodKey->GetName());
      TDirectoryFile* rodDir = (TDirectoryFile*)rodKey->ReadObj();
      TList* modKeyList = (TList*)rodDir->GetListOfKeys();
      TIter modItr(modKeyList);
      TKey* modKey;
      while ((modKey=(TKey*)modItr())) {
	TString modName(modKey->GetName());
	std::string modStr(modKey->GetName());
	if (modName.Contains("DSP")) continue;
	std::map<TString,TH1F*> thrHistNormMap;
	std::map<TString,TH1F*> sigHistNormMap;
	std::map<TString,TH1F*> thrHistLongMap;
	std::map<TString,TH1F*> sigHistLongMap;
	std::map<TString,TH1F*> thrHistGangMap;
	std::map<TString,TH1F*> sigHistGangMap;
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  thrHistNormMap[chipName] = new TH1F("thrNorm"+chipName, "", 500, 0, 10000);
	  thrHistLongMap[chipName] = new TH1F("thrLong"+chipName, "", 500, 0, 10000);
	  thrHistGangMap[chipName] = new TH1F("thrGang"+chipName, "", 500, 0, 10000);
	}
	TString chi2HistDirPath = modName + "/" + chi2HistName + "/A0/B0";
	TDirectoryFile* chi2HistDir = (TDirectoryFile*)rodDir->Get(chi2HistDirPath);
	TH2F* h2dChi2 = (TH2F*)((TKey*)chi2HistDir->GetListOfKeys()->First())->ReadObj();
	TString thrHistDirPath = modName + "/" + thrHistName + "/A0/B0";
	TDirectoryFile* thrHistDir = (TDirectoryFile*)rodDir->Get(thrHistDirPath);
	TH2F* h2dThr = (TH2F*)((TKey*)thrHistDir->GetListOfKeys()->First())->ReadObj();
	TString sigHistDirPath = modName + "/" + sigHistName + "/A0/B0";
	TDirectoryFile* sigHistDir = (TDirectoryFile*)rodDir->Get(sigHistDirPath);
	TH2F* h2dSig = (TH2F*)((TKey*)sigHistDir->GetListOfKeys()->First())->ReadObj();
	for (int col = 1; col <= ncol*nchip/2; col++) {
	  for (int row = 1; row <= nrow*2; row++) {
	    float chi2 = h2dChi2->GetBinContent(col,row);
	    float thr = h2dThr->GetBinContent(col,row);
	    float sig = h2dSig->GetBinContent(col,row);
	    if (thr <= 0 || thr > 10000 || sig <= 0 || sig > 1000 || chi2 <= 0 || chi2 > 10.0) continue;
	    TString chipName = "I";
	    chipName += getChipId(col, row);
	    std::string pixelType = getPixelType(col, row);
	    if (pixelType.compare("normal") == 0) {
	      thrHistNormMap[chipName]->Fill(thr);
	    }
	    else if (pixelType.compare("long") == 0) {
	      thrHistLongMap[chipName]->Fill(thr);
	    }
	    else if (pixelType.compare("ganged") == 0) {
	      thrHistGangMap[chipName]->Fill(thr);
	    }
	  }
	}
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  std::string chipStr(chipName.Data());
	  pcdMap[modStr][chipStr]["IntNorm"] = thrHistNormMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["IntLong"] = thrHistLongMap[chipName]->GetMean();
	  pcdMap[modStr][chipStr]["IntGang"] = thrHistGangMap[chipName]->GetMean();
	  delete thrHistNormMap[chipName];
	  delete thrHistLongMap[chipName];
	  delete thrHistGangMap[chipName];
	}
      }
    }
  }
  
  // ToT calibration
  {
    TFile totFile(totFileName.c_str(),"READ");
    TString totHistName = "TOT_MEAN";
    TString totSigHistName = "TOT_SIGMA";
    TDirectoryFile* scanDir = (TDirectoryFile*)((TKey*)totFile.GetListOfKeys()->First())->ReadObj();
    TList* rodKeyList = (TList*)scanDir->GetListOfKeys();
    TIter rodItr(rodKeyList);
    TKey* rodKey;
    while ((rodKey=(TKey*)rodItr())) {
      TString rodName(rodKey->GetName());
      TDirectoryFile* rodDir = (TDirectoryFile*)rodKey->ReadObj();
      TList* modKeyList = (TList*)rodDir->GetListOfKeys();
      TIter modItr(modKeyList);
      TKey* modKey;
      while ((modKey=(TKey*)modItr())) {
	TString modName(modKey->GetName());
	std::string modStr(modKey->GetName());
	if (modName.Contains("DSP")) continue;
	if (pcdMap.find(modStr) == pcdMap.end()) continue;
	float totNormLongArr[nchip][ncharge];
	float totErrNormLongArr[nchip][ncharge];
	float totSigNormLongArr[nchip][ncharge];
	float totSigErrNormLongArr[nchip][ncharge];
	float totGangArr[nchip][ncharge];
	float totErrGangArr[nchip][ncharge];
	float totSigGangArr[nchip][ncharge];
	float totSigErrGangArr[nchip][ncharge];
	for (int i = 0; i < ncharge; i++) {
	  TString totHistDirPath = modName + "/" + totHistName + "/A0/B0/C";
	  totHistDirPath += i;
	  TDirectoryFile* totHistDir = (TDirectoryFile*)rodDir->Get(totHistDirPath);
	  TH2F* h2dTot = (TH2F*)((TKey*)totHistDir->GetListOfKeys()->First())->ReadObj();
	  TString totSigHistDirPath = modName + "/" + totSigHistName + "/A0/B0/C";
	  totSigHistDirPath += i;
	  TDirectoryFile* totSigHistDir = (TDirectoryFile*)rodDir->Get(totSigHistDirPath);
	  TH2F* h2dTotSig = (TH2F*)((TKey*)totSigHistDir->GetListOfKeys()->First())->ReadObj();
	  std::map<TString,TH1F*> totHistNormLongMap;
	  std::map<TString,TH1F*> totSigHistNormLongMap;
	  std::map<TString,TH1F*> totHistGangMap;
	  std::map<TString,TH1F*> totSigHistGangMap;
	  for (int chip = 0; chip < nchip; chip++) {
	    TString chipName = "I";
	    chipName += chip;
	    totHistNormLongMap[chipName] = new TH1F("totNormLong"+chipName, "", 1000, 0, 250);
	    totSigHistNormLongMap[chipName] = new TH1F("totSigNormLong"+chipName, "", 200, 0, 2);
	    totHistGangMap[chipName] = new TH1F("totGang"+chipName, "", 1000, 0, 250);
	    totSigHistGangMap[chipName] = new TH1F("totSigGang"+chipName, "", 100, 0, 2);
	  }
	  for (int col = 1; col <= ncol*nchip/2; col++) {
	    for (int row = 1; row <= nrow*2; row++) {
	      float tot = h2dTot->GetBinContent(col,row);
	      float totSig = h2dTotSig->GetBinContent(col,row);
	      if (tot <= 0 || tot > 250 || totSig <= 0 || totSig > 2) continue;
	      TString chipName = "I";
	      chipName += getChipId(col, row);
	      std::string pixelType = getPixelType(col, row);
	      if (pixelType.compare("normal") == 0 || pixelType.compare("long") == 0) {
		totHistNormLongMap[chipName]->Fill(tot);
		totSigHistNormLongMap[chipName]->Fill(totSig);
	      }
	      else if (pixelType.compare("ganged") == 0) {
		totHistGangMap[chipName]->Fill(tot);
		totSigHistGangMap[chipName]->Fill(totSig);
	      }
	    }
	  }
	  for (int chip = 0; chip < nchip; chip++) {
	    TString chipName = "I";
	    chipName += chip;
	    totNormLongArr[chip][i] = totHistNormLongMap[chipName]->GetMean();
	    totErrNormLongArr[chip][i] = totHistNormLongMap[chipName]->GetMeanError();
	    totSigNormLongArr[chip][i] = sqrt(pow(totSigHistNormLongMap[chipName]->GetMean(),2)+
					      pow(totHistNormLongMap[chipName]->GetRMS(),2));
	    totSigErrNormLongArr[chip][i] = sqrt(pow(totSigHistNormLongMap[chipName]->GetMeanError(),2)+
						 pow(totHistNormLongMap[chipName]->GetRMSError(),2));
	    totGangArr[chip][i] = totHistGangMap[chipName]->GetMean();
	    totErrGangArr[chip][i] = totHistGangMap[chipName]->GetMeanError();
	    totSigGangArr[chip][i] = sqrt(pow(totSigHistGangMap[chipName]->GetMean(),2)+
					  pow(totHistGangMap[chipName]->GetRMS(),2));
	    totSigErrGangArr[chip][i] = sqrt(pow(totSigHistGangMap[chipName]->GetMeanError(),2)+
					     pow(totHistGangMap[chipName]->GetRMSError(),2));
	    delete totHistNormLongMap[chipName];
	    delete totSigHistNormLongMap[chipName];
	    delete totHistGangMap[chipName];
	    delete totSigHistGangMap[chipName];
	  }
	}
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  std::string chipStr(chipName.Data());
	  TGraphErrors grTotNormLong(ncharge,
				     chargeArr, totNormLongArr[chip],
				     chargeErrArr, totErrNormLongArr[chip]);
	  TGraphErrors grTotSigNormLong(ncharge,
					totNormLongArr[chip], totSigNormLongArr[chip],
					totErrNormLongArr[chip], totSigErrNormLongArr[chip]);
	  TF1 f1TotNormLong("f1TotNormLong", funcTot,
			    chargeArr[0], chargeArr[ncharge-1], 3);
	  TF1 f1DispNormLong("f1DispNormLong", funcDisp,
			     totNormLongArr[chip][0], totNormLongArr[chip][ncharge-1], 2);
	  grTotNormLong.Fit(&f1TotNormLong,"MRQ");
	  grTotSigNormLong.Fit(&f1DispNormLong,"MRQ");
	  float parANormLong = f1TotNormLong.GetParameter(0);
	  float parENormLong = f1TotNormLong.GetParameter(1);
	  float parCNormLong = f1TotNormLong.GetParameter(2);
	  float parP0NormLong = f1DispNormLong.GetParameter(0);
	  float parP1NormLong = f1DispNormLong.GetParameter(1);
	  pcdMap[modStr][chipStr]["ParANormLong"] = parANormLong;
	  pcdMap[modStr][chipStr]["ParENormLong"] = parENormLong;
	  pcdMap[modStr][chipStr]["ParCNormLong"] = parCNormLong;
	  pcdMap[modStr][chipStr]["ParP0NormLong"] = parP0NormLong;
	  pcdMap[modStr][chipStr]["ParP1NormLong"] = parP1NormLong;
	  TGraphErrors grTotGang(ncharge,
				 chargeArr, totGangArr[chip],
				 chargeErrArr, totErrGangArr[chip]);
	  TGraphErrors grTotSigGang(ncharge,
				    totGangArr[chip], totSigGangArr[chip],
				    totErrGangArr[chip], totSigErrGangArr[chip]);
	  TF1 f1TotGang("f1TotGang", funcTot,
			chargeArr[0], chargeArr[ncharge-1], 3);
	  TF1 f1DispGang("f1DispGang", funcDisp,
			 totGangArr[chip][0], totGangArr[chip][ncharge-1], 2);
	  grTotGang.Fit(&f1TotGang,"MRQ");
	  grTotSigGang.Fit(&f1DispGang,"MRQ");
	  float parAGang = f1TotGang.GetParameter(0);
	  float parEGang = f1TotGang.GetParameter(1);
	  float parCGang = f1TotGang.GetParameter(2);
	  float parP0Gang = f1DispGang.GetParameter(0);
	  float parP1Gang = f1DispGang.GetParameter(1);
	  pcdMap[modStr][chipStr]["ParAGang"] = parAGang;
	  pcdMap[modStr][chipStr]["ParEGang"] = parEGang;
	  pcdMap[modStr][chipStr]["ParCGang"] = parCGang;
	  pcdMap[modStr][chipStr]["ParP0Gang"] = parP0Gang;
	  pcdMap[modStr][chipStr]["ParP1Gang"] = parP1Gang;
	}
	std::cout << modStr << std::endl;
	for (int chip = 0; chip < nchip; chip++) {
	  TString chipName = "I";
	  chipName += chip;
	  std::string chipStr(chipName.Data());
	  std::cout << chipName << " "
		    << int(pcdMap[modStr][chipStr]["ThrNorm"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrRmsNorm"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrSigNorm"]) << " "
		    << int(pcdMap[modStr][chipStr]["IntNorm"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrLong"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrRmsLong"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrSigLong"]) << " "
		    << int(pcdMap[modStr][chipStr]["IntLong"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrGang"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrRmsGang"]) << " "
		    << int(pcdMap[modStr][chipStr]["ThrSigGang"]) << " "
		    << int(pcdMap[modStr][chipStr]["IntGang"]) << " "
		    << pcdMap[modStr][chipStr]["ParANormLong"] << " "
		    << pcdMap[modStr][chipStr]["ParENormLong"] << " "
		    << pcdMap[modStr][chipStr]["ParCNormLong"] << " "
		    << pcdMap[modStr][chipStr]["ParAGang"] << " "
		    << pcdMap[modStr][chipStr]["ParEGang"] << " "
		    << pcdMap[modStr][chipStr]["ParCGang"] << " "
		    << pcdMap[modStr][chipStr]["ParP0NormLong"] << " "
		    << pcdMap[modStr][chipStr]["ParP1NormLong"]
		    << std::endl;
	}
      }
    }
  }
  
  return 0;
}
