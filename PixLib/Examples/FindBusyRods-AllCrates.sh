#! /bin/bash

outfile=/tmp/ExecuteTimStatus_temp.out
resultsfile=/tmp/ExecuteTimStatus_results.out

if [ -e $resultsfile ] 
then 
rm $resultsfile
fi
echo "==============================================================">$resultsfile
for i in l-1 l-2 l-3 l-4 d-1 d-2 b-1 b-2 b-3
do

if [ -e $outfile ]
then
rm $outfile
fi
crate=`echo $i | sed 's/-//g' |tr '[:lower:]' '[:upper:]'`
echo 
echo "Crate: $crate" 


ssh sbc-pix-rcc-$i 'source /det/pix/scripts/Default-pixel.sh; $ROD_DAQ/RodUtils/FindBusyRods'

done
