std::vector<RodCrateConnectivity *> vc;
std::vector<RodBocConnectivity *> vr;
std::vector<ModuleConnectivity *> vm;
std::vector<TimConnectivity *> vt;

SctPixelRod::VmeInterface* m_vme;

std::map<std::string, TimPixTrigController *> tim;
//std::map<std::string, RodPixController *> rod;
std::map<std::string, PixModuleGroup *> modGrp;
std::map<std::string, PixRunConfig *> rod;
std::map<std::string, std::string> RodsModsCorrespondanceMap;

int crSel = -1;
int timSel = -1;
int rodSel = -1;
int modSel = -1;

std::string dbname;
DbRecord *rootRec;
PixConnectivity* m_conn;
