#include "Bits/Bits.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixController/PixRunConfig.h"
#include "PixModule/PixModule.h"
#include "PixUtilities/PixISManager.h"

#include <oks/kernel.h>
#include <oks/relationship.h>

#define GETNAME get_name
#ifdef TDAQ010900
#define GETNAME GetName
#endif

using namespace std;

std::string pSel = "";
std::string cSel = "";
std::string rSel = "";
std::string oSel = "";
std::string mSel = "";
std::string rcdFolder = "PIT";

PixConnectivity* gConn;
PixDisable* disableConfig = NULL;
PixDbServerInterface* DbServer = NULL;
IPCPartition* ipcPartition = NULL;
PixRunConfig* runConfig = NULL;
PixISManager* isManager = NULL; 
PixISManager* isManager4Disable = NULL; 

std::string IsServerName     = "PixelRunParams";
std::string IsServerName4Disable     = "RunParams";
std::string serverName       = "PixelDbServer";
std::string ipcPartitionName =     "PixelInfr";
std::string dataTakingPartitionNamePit = "PixelDD";
std::string dataTakingPartitionNameSr1 = "PixelDD";

std::string runConf = "Global";
std::string disConf = "GlobalDis";
std::string connTag   =   "PIT-CONN";
std::string  cfgTag   =   "PIT_BOC";
std::string cfgModTag =   "PIT_MOD";
std::string idTag     = "PIXEL16";
std::string pendTag = "_Tmp"; 
bool disModif;
bool runModif;
bool showHelp = true;
bool setTags = false;
bool appDis = false;
std::string addDisConf = "GlobalDis";
bool addDis = false;

enum mainMode { NORMAL, HELP  };
enum mainMode mode;

bool active = false;

int cnt = 0;
std::map<std::string, std::string> pv, cv, rv, ov, mv;

void justifyXmlDisables(PixConnectivity* connArg) {

  std::map<string, std::string>  partNames;
  partNames["PIXEL_B"] = "PixelRunControlBLayer";
  partNames["PIXEL_D"] = "PixelRunControlDisk";
  partNames["PIXEL_L"] = "PixelRunControlBarrel";

  std::map<std::string, std::string> toBeDisabledRods;
  std::map<std::string, std::string> toBeDisabledPartitions;

  //--------- READ CONNECTIVITY-------------------------------

  std::map<std::string, RodBocConnectivity*>::iterator irb;
  for (irb=connArg->rods.begin(); irb!=connArg->rods.end(); ++irb) {
    if (irb->second->active() == false) {
      toBeDisabledRods[irb->first] = "TODIS";
      cout << "To be Disabled ROD: " << irb->first << endl;
    }
  }

  std::map<std::string, PartitionConnectivity*>::iterator ip;
  for (ip=connArg->parts.begin(); ip!=connArg->parts.end(); ++ip) {
    if (ip->second->active() == false) {
      if (partNames.find(ip->first) != partNames.end()) {
	toBeDisabledPartitions[partNames[ip->first]] = "TODIS";
	cout << "To be Disabled partition : " << ip->first << endl;
      }
    }
  }

  //--------- READ XML File ------------------------------------------
  std::string mainXmlFile;
  mainXmlFile = getenv("TDAQ_DB_DATA");  
  
  std::cout << "Main XML file is: " << mainXmlFile << std::endl;

  OksKernel kernel;
  OksFile * fp = kernel.load_data(mainXmlFile.c_str());
  if (fp == 0) {
    std::cerr << "ERROR: Can`t load database file " << mainXmlFile << " ,exiting ...\n";
    return;
  } else {
    std::cout << "Reading information from " << mainXmlFile << std::endl;
  }

  OksClass *mainPartition = kernel.find_class("Partition"); 
  
  OksObject *mainPartitionObject = mainPartition->get_object(dataTakingPartitionNamePit);
  
  if (mainPartitionObject) {
    //look what is already disabled in partition
    OksData *d(mainPartitionObject->GetRelationshipValue("Disabled"));
    if (d->type == OksData::list_type) { 
      OksData::List * d_list = d->data.LIST;
      for(OksData::List::iterator i2 = d_list->begin(); i2 != d_list->end(); ++i2) {
	OksData *d2 = *i2;
	std::cout << "Disabled " << d2->data.OBJECT->GetClass()->GETNAME() << " - " << d2->data.OBJECT->GetId() << std::endl;
	if (d2->data.OBJECT->GetClass()->GETNAME() == "PixelROD") { 
	  std::string rodname = d2->data.OBJECT->GetId();
	  if (toBeDisabledRods.find(rodname) == toBeDisabledRods.end()) {
	    toBeDisabledRods[rodname] = "DISABLED";
	  } else {
	    toBeDisabledRods[rodname] = "OK";
	  }
	}
	if (d2->data.OBJECT->GetClass()->GETNAME() == "Segment") { 
	  std::string partname = d2->data.OBJECT->GetId();
	  bool found = false;
	  std::map<std::string, std::string>::iterator ipn;
	  for (ipn=partNames.begin(); ipn!=partNames.end(); ipn++) {
	    if (ipn->second == partname) found = true;
	  }
          if (found) {
	    if (toBeDisabledRods.find(partname) == toBeDisabledRods.end()) {
	      toBeDisabledPartitions[partname] = "DISABLED";
	    } else {
	      toBeDisabledPartitions[partname] = "OK";
	    }
	  }
	}
      }
    }

    std::map<std::string, std::string>::iterator it;
    for (it=toBeDisabledRods.begin(); it!=toBeDisabledRods.end(); it++) {
      if (it->second == "TODIS") {
	mainPartitionObject->AddRelationshipValue("Disabled", "PixelROD", it->first);
      } else if (it->second == "DISABLED") {
	mainPartitionObject->RemoveRelationshipValue("Disabled", "PixelROD", it->first);
      }
    }
    for (it=toBeDisabledPartitions.begin(); it!=toBeDisabledPartitions.end(); it++) {
      if (it->second == "TODIS") {
	mainPartitionObject->AddRelationshipValue("Disabled", "Segment", it->first);
      } else if (it->second == "DISABLED") {
	mainPartitionObject->RemoveRelationshipValue("Disabled", "Segment", it->first);
      }
    }

    // SAVE OKS FILE
    try {
      kernel.save_data(fp, true);
    }
    catch (...) {
      std::cerr << "cannot save oks data file: "<< mainXmlFile << std::endl;
      return;
    }
  } else {
    std::cout << "Could not find object main partition object!" << std::endl;
  }
}


std::string wrCmd(string p1, string p2, string p3, string p4, string p5, string p6="", string p7="", string p8="") {
  std::string cmd;
  if (p1 == "CMD") {
    cmd = "--> ";
    if (p2 != "") {
      pv[p2] = "CMD";
      cmd = cmd + p2;
    }
    if (p3 != "") {
      pv[p3] = "CMD";
      cmd = cmd + "," + p3;
    }
    if (p4 != "") {
      pv[p4] = "CMD";
      cmd = cmd + "," + p4;
    }
    if (p5 != "") {
      pv[p5] = "CMD";
      cmd = cmd + "," + p5;
    }
    if (p6 != "") {
      pv[p6] = "CMD";
      cmd = cmd + "," + p6;
    }
    if (p7 != "") {
      pv[p7] = "CMD";
      cmd = cmd + "," + p7;
    }
    if (p8 != "") {
      pv[p8] = "CMD";
      cmd = cmd + "," + p8;
    }
  } else {
    ostringstream os, os1;
    os << setw(3);
    os << cnt;
    os1 << cnt++;
    cmd = os.str();
    pv[os1.str()] = p1;
    cv[os1.str()] = p2;
    rv[os1.str()] = p3;
    ov[os1.str()] = p4;
    mv[os1.str()] = p5;
  }
  return cmd;
}

void prompt() {
  std::string name, pname, cname, rname, oname, mname;
  std::string cmd;
  std::string comm;
  std::string line;

  cnt = 0;
  pv.clear();
  cv.clear();
  rv.clear();
  ov.clear();
  mv.clear();

  name = "Global";
  comm = "   Ena (Ena) RunConf";
  bool eg = true;
  std::cout << std::endl << std::endl;
  
  if (pSel=="" && cSel=="" && rSel=="" && oSel=="" && mSel=="") {
    cmd = wrCmd("CMD", "r", "", "", "");
  } else {
    cmd = wrCmd("", "", "", "", "");
  }
  cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
  /*
  std::vector<std::string> listConf=runConfig->listSubConf();
  for( int i=0; i<listConf.size(); i++) {
    std::cout << "-subName " << listConf[i] << std::endl;
  }
  */
  std::map<std::string, PartitionConnectivity*>::iterator itp;
  for (itp=gConn->parts.begin(); itp!=gConn->parts.end(); itp++) {
    PartitionConnectivity *xp = itp->second; 
    pname = xp->name(); name = "  "+pname;
    bool ep = xp->active();
    if (ep) comm = "   Ena"; else comm = "   Dis";
    ep = ep && eg;
    if (ep) comm += " (Ena)"; else comm += " (Dis)";
    if (runConfig->subConfExists(pname)) comm += " RunConf";
    if (pSel==pname && cSel=="" && rSel=="" && oSel=="" && mSel=="") {
      if (runConfig->subConfExists(pname)) {
	cmd = wrCmd("CMD", "e", "d", "r", "x");
      } else {
	cmd = wrCmd("CMD", "e", "d", "n", "");
      }
    } else {
      cmd = wrCmd(pname, "", "", "", "");
    }
    cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
    if (pSel==pname) {
      std::map<std::string, RodCrateConnectivity*>::iterator itc;
      for (itc=itp->second->rodCrates().begin(); itc!=itp->second->rodCrates().end(); itc++) {
	RodCrateConnectivity *xc = itc->second; 
	cname = xc->name(); name = "    "+cname;
	bool ec = xc->active() && xc->enableReadout;
	if (ec) comm = "   Ena"; else comm = "   Dis";
	ec = ec && ep;
	if (ec) comm += " (Ena)"; else comm += " (Dis)";
	if (runConfig->subConfExists(cname)) comm += " RunConf";
	if (pSel==pname && cSel==cname && rSel=="" && oSel=="" && mSel=="") {
	  if (runConfig->subConfExists(cname)) {
	    cmd = wrCmd("CMD", "e", "d", "r", "x");
	  } else {
	    cmd = wrCmd("CMD", "e", "d", "n", "");
	  }
	} else { 
	  cmd = wrCmd(pname, cname, "", "", "");
	}
	cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
	if (pSel==pname && cSel==cname) {
          if (itc->second->tim() != NULL) {
	    TimConnectivity *xt = itc->second->tim();
	    rname = xt->name(); name = "      "+rname;
	    bool et = xt->active() && xt->enableReadout;
	    if (et) comm = "   Ena"; else comm = "   Dis";
	    et = et && ec;
	    if (et) comm += " (Ena)"; else comm += " (Dis)";
	    if (pSel==pname && cSel==cname && rSel==rname && oSel=="" && mSel=="") {
	      cmd = wrCmd("CMD", "e", "d", "t", "");
	    } else { 
	      cmd = wrCmd(pname, cname, rname, "", "");
	    }
	    cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
	  }
	  std::map<int, RodBocConnectivity*>::iterator itr;
	  for (itr=itc->second->rodBocs().begin(); itr!=itc->second->rodBocs().end(); itr++) {
	    RodBocConnectivity *xr = itr->second; 
	    rname = xr->name(); name = "      "+rname;
	    bool er = xr->active() && xr->enableReadout;
	    if (er) comm = "   Ena"; else comm = "   Dis";
	    er = er && ec;
	    if (er) comm += " (Ena)"; else comm += " (Dis)";
	    if (runConfig->subConfExists(rname)) comm += " RunConf";
	    if (pSel==pname && cSel==cname && rSel==rname && oSel=="" && mSel=="") {
	      if (runConfig->subConfExists(rname)) {
		cmd = wrCmd("CMD", "e", "d", "r", "x");
	      } else {
		cmd = wrCmd("CMD", "e", "d", "n", "");
	      }
	    } else { 
	      cmd = wrCmd(pname, cname, rname, "", "");
	    }
	    cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
	    if (pSel==pname && cSel==cname && rSel==rname) {
	      for (unsigned int ito=0; ito<4; ito++) {
		if (xr->obs(ito) != NULL) {
		  if (xr->obs(ito)->pp0() != NULL) {
		    Pp0Connectivity *xo = xr->obs(ito)->pp0(); 
		    oname = xo->name(); name = "        "+oname;
		    bool eo = xo->active();
		    if (eo) comm = "   Ena"; else comm = "   Dis";
		    eo = eo && er;
		    if (eo) comm += " (Ena)"; else comm += " (Dis)";
		    if (pSel==pname && cSel==cname && rSel==rname && oSel==oname && mSel=="") {
		      cmd = wrCmd("CMD", "e", "d", "", "");
		    } else { 
		      cmd = wrCmd(pname, cname, rname, oname, "");
		    }
		    cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
		    if (pSel==pname && cSel==cname && rSel==rname && oSel==oname) {
		      for (unsigned int itm=1; itm<=8; itm++) {
			if (xo->modules(itm) != NULL) {
			  ModuleConnectivity *xm = xo->modules(itm); 
			  mname = xm->name(); name = "          "+mname;
			  bool em = xm->active() && xm->enableReadout;
			  if (em) comm = "   Ena"; else comm = "   Dis";
			  em = em && eo;
			  if (em) comm += " (Ena)"; else comm += " (Dis)";
			  if (pSel==pname && cSel==cname && rSel==rname && oSel==oname && mSel==mname) {
			    cmd = wrCmd("CMD", "e", "d", "", "");
			  } else { 
			    cmd = wrCmd(pname, cname, rname, oname, mname);
			  }
			  cout << setw(15) << left << cmd << setw(28) << left << name << comm << endl;
			}
		      }
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

void editTimConfig() {
  std::string nam = "";
  if (mSel == "" && rSel != "" && cSel != "" && pSel != "") {
    nam = rSel;
  }
  if (nam != "") {
    TimPixTrigController* timC = new TimPixTrigController(nam);
    timC->readConfig(DbServer, "Configuration-"+idTag, cfgTag);
    Config *cf = timC->config();
    bool mod = cf->edit();
    if (mod) {
      cout << "TimPixTrigController Config has been modified; do you want to save it? (y/n) ";
      std::string ans;
      cin >> ans;
      cin.ignore(100,'\n');
      char yn = tolower(ans[0]);
      if (yn == 'y') {
	timC->writeConfig(DbServer, "Configuration-"+idTag, cfgTag, pendTag);
      }
    }
  }
}

void editRodConfig() {
}

void editBocConfig() {
}

void editRunConfig() {
  std::string nam;
  if (mSel != "") {
    nam = mSel;
  } else if (rSel != "") {
    nam = rSel;
  } else if (cSel != "") {
    nam = cSel;
  } else if (pSel != "") {
    nam = pSel;
  }
  PixRunConfig *rc = runConfig;
  rc->selectSubConf(pSel, cSel, rSel);
  Config *cf = &rc->config();
  bool mod = cf->edit();
  if (mod) runModif = true;  
}

void enable(bool en) {
  disModif = true;
  std::string nam;
  if (mSel != "") {
    nam = mSel;
  } else if (oSel != "") {
    nam = oSel;
  } else if (rSel != "") {
    nam = rSel;
  } else if (cSel != "") {
    nam = cSel;
  } else if (pSel != "") {
    nam = pSel;
  }
  if (en) {
    disableConfig->enable(nam);
  } else {
    disableConfig->disable(nam);
  }
  disableConfig->put(gConn);
}

void runSubConf(bool ena) {
  std::string nam1;
  if (mSel != "") {
    nam1 = mSel;
  } else if (rSel != "") {
    nam1 = rSel;
  } else if (cSel != "") {
    nam1 = cSel;
  } else if (pSel != "") {
    nam1 = pSel;
  } 
  if (ena) {
    if (!runConfig->subConfExists(nam1)) {
      runConfig->addSubConf(nam1);
      runConfig->selectSubConf("?");
      Config &cf = runConfig->config();
      runConfig->selectSubConf(nam1);
      Config &cf1 = runConfig->config();
      std::string nam2 = cf1.name();
      cf1 = cf;
      cf1.name(nam2);
      runModif = true;
    }
  } else {
    if (runConfig->subConfExists(nam1)) {
      runConfig->removeSubConf(nam1);
      runModif = true;
    }
  }
}

void getInput() {
  bool ok;
  std::string inp;
  
  if (showHelp) {
    cout << endl;
    cout << "Enter: " << endl;
    cout << "  - a number" << endl;
    cout << "  - e to enable selected object" << endl;
    cout << "  - d to disable selected object" << endl;
    cout << "  - c to edit the configuration of selected object" << endl;
    cout << "  - r to edit the run configuration for selected object" << endl;
    cout << "  - R to edit the ROD configuration for selected object" << endl;
    cout << "  - b to edit the BOC configuration for selected object" << endl;
    cout << "  - t to edit the TIM configuration for selected object" << endl;
    cout << "  - n to create a new run configuration for selected object" << endl;
    cout << "  - x to remove the run configuration for selected object" << endl;
    cout << "  - h to toggle the display of this help" << endl;
    cout << "  - quit to exit" << endl;
  }
  do {
    std::cout << ">>> ";
    std::cin >> inp;
    for (unsigned int i=0; i<inp.size(); i++) inp[i] = tolower(inp[i]);
    if (inp == "quit") {
      active = false;
      return;
    } else if (inp == "h") {
      showHelp = !showHelp;
      return;
    }
    if (pv.find(inp) == pv.end()) {
      ok = false;
      cout << "Invalid input\n" << endl;
    } else {
      ok = true;
    }
  } while(!ok);
  std::map<std::string, std::string>::iterator it;
  it = pv.find(inp);
  if (it->second == "CMD") {
    if (inp == "e") {
      enable(true);
    } else if (inp == "d") {
      enable(false);
    } else if (inp == "c") {
    } else if (inp == "r") {
      editRunConfig();
    } else if (inp == "n") {
      runSubConf(true);
    } else if (inp == "x") {
      runSubConf(false);
    } else if (inp == "t") {
      editTimConfig();
    } else if (inp == "b") {
      editBocConfig();
    } else if (inp == "R") {
      editRodConfig();
    }
  } else {
    pSel = it->second;
    it = cv.find(inp);
    cSel = it->second;  
    it = rv.find(inp);
    rSel = it->second;  
    it = ov.find(inp);
    oSel = it->second;  
    it = mv.find(inp);
    mSel = it->second;  
  }
}

void getParams(int argc, char **argv) {
  int ip = 1;
  bool found=false;
  mode = NORMAL;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  serverName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--isServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  IsServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No IsServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--idTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  idTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No idTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--connTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  connTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No connTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--cfgTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No cfgTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--cfgModTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgModTag = argv[ip+1];
	  found=true;
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No cfgmodTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--pendTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pendTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No pendingTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--runConf") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  runConf = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No PixRunConfig name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--disConf") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  disConf = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No PixRunConfig name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--addDis") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  addDisConf = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No addDisConf name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--setTags") {
	setTags = true;
	ip++; 
      } else if (std::string(argv[ip]) == "--appDis") {
	appDis = true;
	ip++; 
      } else {
	std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
	mode = HELP;
	break;
      }
    }
  }
  if (!found) cfgModTag=cfgTag;
}

void help() {
  cout << endl;
  cout << "This program gives an access to data taking configuration parameters," << endl;
  cout << endl; 
  std::cout << "Usage: RunConfigEditor2 --help\n"
	    << "            // prints this message\n"
	    <<std::endl;
  cout << "Optional switches:" << endl;
  cout << "  --dbPart <part>      IPC Partition (def. PixelInfr)" << endl;
  cout << "  --dbServ <dbServer>  DbServer name (def. PixelDbServer)" << endl;
  cout << "  --isServ <dbServer>  IsServer name (def. PixelRunParams)" << endl;
  cout << endl;
  cout << "  --idTag     <id tag>     id DB tag           (def. " << idTag << ")" << endl;
  cout << "  --connTag   <conn tag>   Connectivity DB tag (def. " << connTag << ")" << endl;
  cout << "  --cfgTag    <cfg tag>    Config DB tag       (def. " << cfgTag << ")" << endl;
  cout << "  --cfgModTag <cfg ModTag> Config Mod DB tag   (def. " << cfgModTag << ")" << endl;
  cout << "  --pendTag   <pend tag>   Pending tag to use for new objects (def. _Tmp)" << endl;
  cout << endl;
  cout << "  --runConf <run config>     PixRunConfig name (def. Global)" << endl;
  cout << "  --disConf <disable config> PixDisable name   (def. GlobalDis)" << endl;
  cout << "  --addDisConf <disable config> PixDisable name   (def. GlobalDis)" << endl;
  cout << endl;
  cout << "  --setTags Set default tags values in IS" << endl;
  cout << "  --appDis Append specified PixDisable based on cuts on IS variables" << endl;
  cout << endl;
  exit(0);
}

int main(int argc, char** argv) {
  getParams(argc, argv);
  if(mode == HELP) {
    help();
  }

  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  std::cout << "Connecting to partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "RunConfigEditor: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }

 
  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(const PixISManagerExc &e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return -1;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return -1;
  }
  
  try {
    isManager4Disable = new PixISManager(ipcPartition, IsServerName4Disable);
  }  
  catch(const PixISManagerExc &e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return -1;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName4Disable << std::endl; 
    return -1;
  }

  char *venv;
  venv = getenv("PIX_PART_INFR_NAME");
  if (venv != NULL) ipcPartitionName = venv;
  venv = getenv("PIX_PART_DD_NAME");
  if (venv != NULL) {
    dataTakingPartitionNamePit = venv;
    dataTakingPartitionNameSr1 = venv;
  }
  venv = getenv("PIXRCD_FOLDER");
  if (venv != NULL) rcdFolder = venv;
  std::cout <<  dataTakingPartitionNamePit << " / " << dataTakingPartitionNameSr1 <<  " / " << rcdFolder << std::endl;
  

  bool ready=false;
  int nTry=0;
  if (!setTags) {
    if(ipcPartition != NULL) {
      std::cout << "Trying to connect to DbServer with name " << serverName << std::endl;
      do {
	sleep(1);
	DbServer = new PixDbServerInterface(ipcPartition, serverName, PixDbServerInterface::CLIENT, ready);
	if(!ready) delete DbServer;
	else break;
	std::cout << " ..... attempt number: " << nTry << std::endl;
	nTry++;
      } while(nTry<20);
      if(!ready) {
	std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
	exit(-1);
      } else {
	std::cout << "Successfully connected to DbServer with name " << serverName << std::endl;
      }
      sleep(2);
      DbServer->ready();
    } else {
      cout << "No dbserver is created: no IPC partition were correctly handled!!!" << endl;
    }
    
    cout << "Initializing PixConnectivity db" << endl;
    try {
      gConn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag, cfgModTag);
    }  
    catch(const PixConnectivityExc &e) { 
      std::cout <<"PixConnectivity: "<<e.getDescr()<<std::endl; 
      return -1;
    }
    catch(...) { 
      std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating PixConnectivity"<<std::endl; 
      return -1;
    }
    try {
      gConn->loadConn();
    }
    catch (...) {
      std::cout <<"Error reading PixConnectivity; tag " << connTag << std::endl; 
      return -1;
    }

    std::vector<std::string> tagCfglist;
    DbServer->listTagsRem("Configuration-"+idTag,tagCfglist);
    bool foundCfg=false;
    bool foundCfgMod=false;
    for (unsigned int i=0; i<tagCfglist.size(); i++) {
      if (tagCfglist[i]==cfgTag) foundCfg=true;
      if (tagCfglist[i]==cfgModTag) foundCfgMod=true;
    }
    if (!foundCfg) {
      std::cout << std::endl << "FATAL: cfgTag " << cfgTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    if (!foundCfgMod) {
      std::cout << std::endl << "FATAL: cfgModTag " << cfgModTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }

    // Look into DbServer for PixRunConfig objects
    //PixConnectivity *xconn = new PixConnectivity("Base", "Base", "Base", "Base", idTag);
    //xconn->setCfgTag(cfgTag);
    //xconn->setTagPen("RunConfigEditor");
    //runConfig = new PixRunConfig(runConf);
    //xconn->readRunConfig(runConfig, runConf);
    //runConfig->addSubConf("PIXEL_D");
    //xconn->writeRunConfig(runConfig, runConf);
    //xconn->readRunConfig(runConfig, runConf);
    runConfig = new PixRunConfig(runConf);
    gConn->setCfgTag(cfgTag);  
    try {
      gConn->readRunConfig(runConfig, runConf);
    } catch (...) {
      std::cout << "PixRunConfig " << runConf << " probably does not exist in DbServer!!!" << std::endl;
      if (runConf=="Global") {
	std::cout << "Creating PixRunConfig: Global" << std::endl;
	gConn->writeRunConfig(runConfig, runConf);
      } else exit(-1);
    }
    // Look into DbServer for PixDisable objects
    disableConfig = new PixDisable(disConf);
    try {
      gConn->readDisable(disableConfig, disConf);
    } catch (...) {
      std::cout << "PixDisable " << disConf << " probably does not exist in DbServer!!!" << std::endl;
      if (disConf=="GlobalDis") {
	std::cout << "Creating PixDisable: GlobalDis" << std::endl;
	gConn->writeDisable(disableConfig, disConf);
      } else exit(-1);
    }
    disableConfig->put(gConn);
  
    if (appDis) {
      // Read from IS variables with certain suffix
      // if value of a virable > 0 save it in a vector (now done in two steps, later add method to PixISMan)
      // go over current disable and append it
      std::vector<pair<std::string, std::string> > appendDisableModNames;
      std::cout << std::endl << "Reading from IS" << std::endl << std::endl;
      try {
	std::cout << "Timeout errors first" << std::endl;
	std::vector<std::string> listedTimeoutErrors = isManager4Disable->listVariables("timeout_errors");
	//std::cout << "Timeout vars found: "<< listedTimeoutErrors.size() << std::endl;
	for (unsigned int i=0; i<listedTimeoutErrors.size(); i++) {
	  if ((listedTimeoutErrors[i].find("_M")!= std::string::npos) && (isManager4Disable->read<int>(listedTimeoutErrors[i]) >0 )){
	    std::string::size_type conn_start = listedTimeoutErrors[i].find("/");
	    std::string::size_type conn_end = listedTimeoutErrors[i].rfind("/");
	    if ((conn_start != std::string::npos) && (conn_end != std::string::npos) && (conn_start!=conn_end)) {
	      pair <std::string, std::string > modConnName = make_pair (listedTimeoutErrors[i].substr(0,conn_start), listedTimeoutErrors[i].substr(conn_start+1,conn_end-1-conn_start));
	      std::cout << "Timeout Disable candidate: "<< modConnName.second<< std::endl;
	      if (modConnName.second.find("_M")!= std::string::npos) appendDisableModNames.push_back(modConnName);
	    }
	  }
	}
	std::cout << "LVL1ID errors ..." << std::endl;
	std::vector<std::string> listedLVL1IDErrors = isManager4Disable->listVariables("LVL1ID_errors");
	//std::cout << "LVL1ID vars found: "<< listedLVL1IDErrors.size() << std::endl;
	for (unsigned int i=0; i<listedLVL1IDErrors.size(); i++) {
	  if ((listedLVL1IDErrors[i].find("_M")!= std::string::npos) && (isManager4Disable->read<int>(listedLVL1IDErrors[i]) >0 )){
	    std::string::size_type conn_start = listedLVL1IDErrors[i].find("/");
	    std::string::size_type conn_end = listedLVL1IDErrors[i].rfind("/");
	    if ((conn_start != std::string::npos) && (conn_end != std::string::npos) && (conn_start!=conn_end)) {
	      pair <std::string, std::string > modConnName = make_pair(listedLVL1IDErrors[i].substr(0,conn_start), listedLVL1IDErrors[i].substr(conn_start+1,conn_end-1-conn_start));
	      std::cout << "LVL1ID Disable candidate: "<< modConnName.second<< std::endl;
	      if (modConnName.second.find("_M")!= std::string::npos) {
		unsigned int idx = 0;
		for (idx=0; idx<appendDisableModNames.size(); idx++) if (appendDisableModNames[idx].second == modConnName.second) break;
		if (idx == appendDisableModNames.size()) { appendDisableModNames.push_back(modConnName);}
	      }
	    }
	  }
	}
	std::cout << "preamble errors ..." << std::endl;
	std::vector<std::string> listedpreambleErrors = isManager4Disable->listVariables("preamble_errors");
	//std::cout << "preamble vars found: "<< listedpreambleErrors.size() << std::endl;
	for (unsigned int i=0; i<listedpreambleErrors.size(); i++) {
	  if ((listedpreambleErrors[i].find("_M")!= std::string::npos) && (isManager4Disable->read<int>(listedpreambleErrors[i]) >0 )){
	    std::string::size_type conn_start = listedpreambleErrors[i].find("/");
	    std::string::size_type conn_end = listedpreambleErrors[i].rfind("/");
	    if ((conn_start != std::string::npos) && (conn_end != std::string::npos) && (conn_start!=conn_end)) {
	      pair <std::string, std::string > modConnName = make_pair(listedpreambleErrors[i].substr(0,conn_start),listedpreambleErrors[i].substr(conn_start+1,conn_end-conn_start-1));
	      std::cout << "preamble Disable candidate: "<< modConnName.second<< std::endl;
	      if (modConnName.second.find("_M")!= std::string::npos) {
		unsigned int idx = 0;
		for (idx=0; idx<appendDisableModNames.size(); idx++) if (appendDisableModNames[idx].second == modConnName.second) break;
		if (idx == appendDisableModNames.size()) { appendDisableModNames.push_back(modConnName);}
	      }
	    }
	  }
	}
      } catch(...) {
	std::cout << "Problem in reading information from IS Server" << std::endl;
      }
      std::cout << "Suggested modules to disable: " << std::endl;
      for (unsigned int i=0; i<appendDisableModNames.size(); i++) {
	std::cout << appendDisableModNames[i].first<<" "<< appendDisableModNames[i].second << std::endl;
	disableConfig->disable(appendDisableModNames[i].second);
      }
      disableConfig->put(gConn);
      disModif = true;
    } else if (addDis) {

    } else {
  
    // Main loop
      active = true;
      while(active) {
	prompt();
	getInput();
      }
    
    }

    // Save PixDisable if needed
    if (disModif) {
      cout << "PixDisable has been modified; do you want to save it? (y/n) ";
      std::string ans;
      cin >> ans;
      cin.ignore(100,'\n');
      char yn = tolower(ans[0]);
      if (yn == 'y') {
	cout << "PixDisable name (Ret = " << disConf << ") ";
	ans = ""; std::getline(cin, ans);
	if (ans != "") disConf = ans; 
        //conn->cfgFromDb();
	gConn->setTagPen(pendTag);
	gConn->writeDisable(disableConfig, disConf);
      }
    }
   
    // Save PixRunConfig if needed
    if (runModif) {
      cout << "PixRunConfig has been modified; do you want to save it? (y/n) ";
      std::string ans;
      cin >> ans;
      cin.ignore(100,'\n');
      char yn = tolower(ans[0]);
      if (yn == 'y') {
	cout << "PixRunConfig name (Ret = " << runConf << ") ";
	ans = ""; std::getline(cin, ans);
	if (ans != "") runConf = ans;
	//PixConnectivity *xconn = new PixConnectivity("Base", "Base", "Base", "Base", idTag);
	//xconn->setCfgTag(cfgTag);
	gConn->setTagPen(pendTag);
	gConn->writeRunConfig(runConfig, runConf);
      }
    }
    //if (rcdFolder == "PIT") justifyXmlDisables(conn);
    //if (rcdFolder == "SR1") justifyXmlDisables(conn);
  }
  
  if (setTags) {
    // Publish tags in IS
    std::cout << std::endl << "Wrinting tag in IS" << std::endl << std::endl;
    try {
      //   isManager->publish("PixDisable" ,disConf);  ?????????????????
      isManager->publish("DataTakingConfig-IdTag", idTag); 
      isManager->publish("DataTakingConfig-ConnTag", connTag); 
      isManager->publish("DataTakingConfig-CfgTag", cfgTag); 
      isManager->publish("DataTakingConfig-CfgRev", ""); 
      isManager->publish("DataTakingConfig-ModCfgTag", cfgModTag); 
      isManager->publish("DataTakingConfig-ModCfgRev", "");  
      isManager->publish("DataTakingConfig-Disable", disConf); 
      isManager->publish("DataTakingConfig-DisableRev", ""); 
      isManager->publish("DataTakingConfig-RunConfig", runConf); 
      isManager->publish("DataTakingConfig-RunConfigRev", ""); 
    } catch(...) {
      std::cout << "Problem in writing taf information in Is Server" << std::endl;
    }
  }
}
