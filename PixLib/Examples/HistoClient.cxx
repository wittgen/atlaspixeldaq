#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>


#include <ipc/alarm.h>
#include <ipc/core.h>

#include "Histo/Histo.h"
#include "PixHistoServer/PixHistoServerInterface.h"

#include "TCanvas.h"
#include "TApplication.h"
#include <TGraph.h>

using namespace PixLib;

std::string part = "nico";
std::string nameServer = "nameServer";
std::string folder = "/SCAN/ROD/MOD/A/B/C/";
std::string histogram = "histogram";
int dimension = 2;

enum mainMode{HELP, GO};
enum mainMode mode;



void getParams(int argc, char **argv) {
  int ip = 1;
  mode = GO;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--partitionName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  part = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--nameServerName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  nameServer = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--histoDim") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dimension = atoi(argv[ip+1]);
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--folderName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  folder = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--histoName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  histogram = argv[ip+1];
	  ip += 2; 
	}
      } else {
	std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
	mode = HELP;
	break;
      }     
    } else {
      ip++;
    }
  }
}



int main(int argc, char **argv) {
  getParams(argc, argv);

  if (mode == HELP) {
    std::cout << std::endl;
    std::cout << "USAGE: HistoClient --partitionName <partition name> <def. PixelInfr>" << std::endl
	      << "--nameServerName <nameServer name> <def. nameServer>" << std::endl
	      << "--histoDim <histogram dimension (1 or 2)> <def. 2>" << std::endl
	      << "--folderName </path/> <def. /SCAN/ROD/MOD/A/B/C/>" << std::endl
	      << "--histoName <histogramName> <def. histogram>" << std::endl;
    std::cout << "CHECK TO HAVE THE IS SERVER pixel_histo_server_crate0 RUNNING! "<< std::endl;
    exit(-1);
  }
  
//   std::string ipcPartitionName = argv[1];
//   std::string serverName = argv[2];
//   std::string histoName;
//   std::string folderName;
//   int dim;

//   std::istringstream is(argv[3]);
//   is >> dim;
  
  // Start IPCCore
  IPCCore::init(argc, argv);
  
  // Create IPCPartition
  std::cout << "Getting partition " << part << std::endl;
  IPCPartition *m_ipcPartition = new IPCPartition(part.c_str());
  std::cout<< "I start IPC" << std::endl;

//   std::cout << "Write the name of the histogram that you want to save!" << std::endl;
//   std::cout <<  "Folder: (ex: /A/B/C/) ";
//   std::cin >> folderName;
//   std::cout<<std::endl;
//   std::cout <<  "File (ex: histogram Name ): ";
//   std::cout<<std::endl;
//   std::cin >> histoName;

  std::cout << "Check: " <<  part << " " << nameServer << " " << folder << " " <<  histogram << std::endl;

  Histo *his;  
  if (dimension==2) {
    his = new Histo(histogram, "test-2D-HistoClient", 144, 0.5, 144.5,
		    320, 0.5, 320.5);
    for (int i=0; i<144; i++) {
      for (int j=0; j<320; j++) {
	his->set(i, j, (rand()%10000));
      }
    }
    std::cout << "The histogram is done " << std::endl;
  } else if (dimension==1) {
    his = new Histo(histogram, "test-1D-HistoClient", 1744, 0.5, 1744.5);
    for (int i=0; i<1744; i++){
      his->set(i, (rand()%10000));
    }
    std::cout << "The histogram "<< histogram << " is done " << std::endl;
  } else {
    std::cout << "Histogram dimension not supported " <<std::endl;
    return 0;
  }
  
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, "pixel_histo_server_crate0", 
							       nameServer, std::string(PROVIDERBASE)+"histo_client");
    
    /*    PixScan *scn = new PixScan(); // test of method used in PixScanTask - 14March08 - test passed
    PixModuleGroup *grp = new PixModuleGroup("test-grp");
    scn->addHisto(*his, PixScan::OCCUPANCY, 0,0,0,0);
    scn->writeHisto(hInt, grp, folderName);
    */

    hInt->sendHisto(folder, *his); 

  } catch (PixHistoServerExc e) {
    if (e.getId() == "NONAMESERVER") {
      std::cout << "problems in the constructor" << std::endl;
    return 0;
    }
    else if (e.getId() == "PUBLISHFAILED") {
      std::cout << "problems in publishing in OH" << std::endl;
      return 0;
    }
    
    /*TApplication theApp("App", 0, 0);
  TCanvas* canvas = new TCanvas();
  TGraph *h1d;
  int tsiz = his->nBin(0);
  double x1d[tsiz], v1d[tsiz];
  for (int ii=0; ii<tsiz; ii++) {
    x1d[ii] = ii;
    v1d[ii] = 0;
    }
  h1d = new TGraph(his->nBin(0), x1d, v1d);
  canvas->cd();
  h1d->Draw();
  canvas -> Update( );
    */
  } catch (...) {
    std::cout << "HistoClient. Unknown exception was thrown. PixHistoServerInterface ERROR." <<std::endl;
    return 0;
  }
  return 0;
}

