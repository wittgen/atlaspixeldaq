#!/bin/sh

TDAQ_PATH="/data/tdaqfiles/TurboDAQ_files/"
OLDDB_PATH="/daq/db/cfg-oldDB/"
NEWDB_PATH="/daq/db/cfg/"
CONFIG_NAME="test.cfg"
CONFIG_NAME2="STAVE.cfg"
CONFIG_NAME3="FLEX.cfg"

ERROR1="NO"

echo "################################"
echo "# CreateConfigCondDbCreate 0.1 #"
echo "# Daniel Dobos                 #"
echo "################################"

if [ $# -lt 2 ]; then
  echo "# ERROR: Usage: './CreateConfigCondDbCreate.sh <connection-tag> <config-tag> [options]'"
  echo "# ERROR: Example: './CreateConfigCondDbCreate.sh C34_ISP_v29 C34_ISP_v29'"
  echo "# ERROR: Exit"
  exit 1
else
  tag_connection=$1
  tag_config=$2
  option=$3
fi

#get modules
echo "# INFO: Searching modules for connection tag: '${tag_connection}':"
sh_modules=`./ConnDbCreate --read --connTag ${tag_connection}  --verbose | grep "Module" | awk '{print $3}' | paste -s -d" "`
numb_modules=`echo ${sh_modules} | wc -w | awk '{print $1}'`
echo "# INFO: Found: '${numb_modules}' modules"
if [ ${option} == "-m" ]; then
  rm -f /tmp/cccdbc.1
  ./ConnDbCreate --dumpcf --cfgTag $tag_config --connTag $tag_connection > /tmp/cccdbc.1
fi

#loop over modules
for MODULE in ${sh_modules}
do
  MODULE_PATH=`echo "${TDAQ_PATH}${MODULE}/"`
  if ! [ -d ${MODULE_PATH} ]; then
    echo "# ERROR: Module path: '${MODULE_PATH}' not found"
    ERROR1="YES"
  else
    CONFIGS_PATH=`echo "${MODULE_PATH}configs/"`
    if ! [ -d ${CONFIGS_PATH} ]; then
      echo "# ERROR: Module configs path: '${CONFIGS_PATH}' not found"
      ERROR1="YES"
    else
      TDACS_PATH=`echo "${MODULE_PATH}tdacs"`
      if ! [ -d ${TDACS_PATH} ]; then
        echo "# ERROR: Module tdacs path: '${TDACS_PATH}' not found"
        ERROR1="YES"
      else
        FDACS_PATH=`echo "${MODULE_PATH}fdacs"`
        if ! [ -d ${FDACS_PATH} ]; then
          echo "# ERROR: Module fdacs path: '${FDACS_PATH}' not found"
          ERROR1="YES"
        else
          CONFIG_FILE=`echo "${CONFIGS_PATH}${CONFIG_NAME}"`
          MODULE_WO_M=`echo ${MODULE} | sed s/M//g`
          NEW_TURBODAQ_CONFIG=`echo "${CONFIGS_PATH}${MODULE_WO_M}.cfg"`
          if ! [ -f ${CONFIG_FILE} ]; then
            echo "# ERROR: Module config file: '${CONFIG_FILE}' not found trying other name"
            CONFIG_FILE=`echo "${CONFIGS_PATH}${CONFIG_NAME2}"`
          fi
          if ! [ -f ${CONFIG_FILE} ]; then
            echo "# ERROR: Module config file: '${CONFIG_FILE}' not found trying other name"
            CONFIG_FILE=`echo "${CONFIGS_PATH}${CONFIG_NAME3}"`
          fi
          if ! [ -f ${CONFIG_FILE} ]; then
            echo "# ERROR: Module config file: '${CONFIG_FILE}' not found"
            ERROR1="YES"
          else
            if [ ${option} != "-m" ]; then
              echo "# INFO: Module config file '${CONFIG_FILE}' found"
              echo "# INFO: Copying: '${CONFIG_FILE}' to '${NEW_TURBODAQ_CONFIG}'"
              cp ${CONFIG_FILE} ${NEW_TURBODAQ_CONFIG}
              ./ModuleGroup_Wizard -n BaseConf.cfg
              NEW_CONFIG_FILE=`echo "${MODULE_PATH}${MODULE}-${tag_config}.cfg.root"`
              NEWDB_CONFIG_FILE=`echo "${MODULE_PATH}${MODULE}-${tag_config}.root"`
              echo "# INFO: Moveing: 'BaseConf.cfg.root' to '${NEW_CONFIG_FILE}'"
              mv BaseConf.cfg.root ${NEW_CONFIG_FILE}
              echo "# Executing: './ModuleGroup_Wizard -a ${NEW_CONFIG_FILE} \"${NEW_CONFIG_FILE}:/Test_0/application;1\" Grp0_0 ${NEW_TURBODAQ_CONFIG}'"
              ./ModuleGroup_Wizard -a ${NEW_CONFIG_FILE} "${NEW_CONFIG_FILE}:/Test_0/application;1" Grp0_0 ${NEW_TURBODAQ_CONFIG}
              echo "# Module RootDB: '${NEW_CONFIG_FILE}' created:"
              ls -lh ${NEW_CONFIG_FILE}
#              echo "# Executing: './ConvModConfig -c ${NEW_CONFIG_FILE} -o ${NEWDB_CONFIG_FILE}'"
#              ./ConvModConfig -c ${NEW_CONFIG_FILE} -o ${NEWDB_CONFIG_FILE}
#              echo "# New Module RootDB '${NEWDB_CONFIG_FILE}' created:"
#              ls -lh ${NEW_DBCONFIG_FILE}
              if [ ! -d ${OLDDB_PATH}${MODULE} ]; then
                echo "# INFO: Creating old DB config path: '${OLDDB_PATH}${MODULE}'"
                mkdir ${OLDDB_PATH}${MODULE}
              fi
              echo "# INFO: Moveing: '${NEW_CONFIG_FILE}' to '${OLDDB_PATH}${MODULE}'"            
              mv ${NEW_CONFIG_FILE} ${OLDDB_PATH}${MODULE}
            fi
            if [ ${option} != "-m" ]; then
              echo "# INFO: Executing: './ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root':"
              ./ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root
            else
              if [ ! -d ${NEWDB_PATH}${MODULE} ]; then
                echo "# INFO: Executing: './ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root':"
                ./ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root
              else
                cfgfile=`grep ${MODULE} /tmp/cccdbc.1 | awk '{ aa=$3 } END { print aa }'`
                echo "==${cfgfile}=="
                if [ "${cfgfile}" == "" ]; then
                  cfgfile="xxxxx"
                fi
                if ! [ -f $cfgfile ]; then
                  echo "# INFO: Executing: './ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root':"
                  ./ConnDbCreate --createModConfig --connTag ${tag_connection} --cfgTag ${tag_config} --turbodaqCfg ${OLDDB_PATH}${MODULE}/${MODULE}-${tag_config}.cfg.root
                else
                  ls -la $cfgfile 
                  echo "# INFO: New DB allready exists: '${NEWDB_PATH}${MODULE}'"
                fi
              fi
            fi
          fi
        fi
      fi
    fi
  fi
done
