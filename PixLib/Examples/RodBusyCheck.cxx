#include "PixDbInterface/PixDbInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>

using namespace std;

std::string IsServerName     = "RunParams";
std::string ipcPartitionName = "PixelInfr";

namespace PixLib {
  template<> inline int PixISManager::read<int>(const char* name, int &time)
  {
     ISInfoInt value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     time = value.time().c_time();
     return value.getValue();
  }
}

IPCPartition* ipcPartition = NULL;
PixISManager* isManager = NULL; 
 
std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

int main(int argc, char** argv) {
  int ipp = 1;
  while (ipp < argc) {
    std::string par = argv[ipp];
    if (par == "--part" && ipp+1 < argc) {
      ipcPartitionName = argv[ipp+1];
      ipp = ipp+2;
   }
  }

  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "RunConfigEditor: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }
 
  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(PixISManagerExc e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return -1;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return -1;
  }
  std::cout << std::endl;
 
  std::vector<std::string> varList = isManager->listVariables("*BUSY");
  for (unsigned int iv = 0; iv<varList.size(); iv++) {
    if (varList[iv].substr(0,4) == "ROD_") {
      int tim;
      int mods = isManager->read<int>(varList[iv].c_str(), tim);
      if (time(NULL)-tim > 60) {
        char t_s[100];
        time_t t1 = tim;
        tm t_m;
        localtime_r(&t1, &t_m);
        strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
        std::cout << varList[iv] << " BUSY info out of date (" << t_s << ")" << std::endl;
      } else {
        if (mods > 0) { 
          std::cout << varList[iv] << " BUSY=" << mods << " " << std::endl;
        }
      }
    }
  }
}
