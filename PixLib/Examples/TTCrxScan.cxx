#include "PixDbInterface/PixDbInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>

using namespace std;

IPCPartition* ipcPartition = NULL;
PixISManager* isManager = NULL; 

//publish start, s
std::string IsServerName     = "RunParams";
std::string ipcPartitionName = getenv("PIX_PART_INFR_NAME"); //"PixelInfr_ibragimo";

std::string mode = "START";
std::string dtPartName = "ATLAS";
float startVal, stopVal, stepSize;
unsigned int stepDur;

int main(int argc, char** argv) {
  int ip = 1;
  bool iblOnlyFlag = false;
  mode = "HELP";
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = "HELP";
	break;
      } else if (std::string(argv[ip]) == "--dtPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dtPartName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "No DT partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--mode") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  mode = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "Mode not specified. " << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--startVal") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  startVal=atof(argv[ip+1]);
	  std::cout << "Timing scan start value = " << startVal << std::endl;
	  ip++; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "No start value specified. " << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--stopVal") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  stopVal=atof(argv[ip+1]);
	  std::cout << "Timing scan start value = " << stopVal << std::endl;
	  ip++; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "No stop value specified. " << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--stepSize") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  stepSize=atof(argv[ip+1]);
	  std::cout << "Timing scan start value = " << stepSize << std::endl;
	  ip++; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "No step size specified. " << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--stepDur") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  stepDur=atoi(argv[ip+1]);
	  std::cout << "Timing step duration = " << stepDur << std::endl;
	  ip++; 
	} else {
	  mode = "HELP";
	  std::cout << std::endl << "No step duration specified. " << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--onlyIBL") {
	iblOnlyFlag = true;
	ip++;
      }  else {
	std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
	mode = "HELP";
	break;
      }
    }
  }

  cout << "MODE: "<< mode << endl; 
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "RunConfigEditor: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }

  std::vector<std::string> timVarList;
  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
    //get names of active TIMs
    if ( !iblOnlyFlag ) { // this is default
      timVarList = isManager->listVariables("*TimOperationMode");
    } else {
      timVarList.push_back( "TIM_I1" );
    }
    for (unsigned int idxVar = 0; idxVar<timVarList.size(); idxVar++) {
      timVarList[idxVar] = timVarList[idxVar].substr(0,6); //"TIM_L1", etc.
      std::cout << timVarList[idxVar] << std::endl;
    }
  }  
  catch(PixISManagerExc e) { 
    std::cout << "Exception caught in TTCrxScan.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return -1;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in TTCrxScan.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return -1;
  }
 
  std::cout << std::endl;
  if (mode == "HELP") {
    std::cout << std::endl;
    std::cout << "Usage: TTCrxScan --dtPart <DT partition> --mode <mode> --startVal <start Value> --stopVal <stop Value> --stepSize <step Size> --stepDur <step Duration>\n"
	      << std::endl << std::endl;
  } else if (mode == "ACTIVE") {
    std::cout << "Publishing timing scan variables to partition " << ipcPartitionName << " server " << IsServerName << std::endl;
    isManager->publish("TIMING_SCAN_DTPART", dtPartName);
    isManager->publish("TIMING_SCAN_STARTVALUE", startVal);
    isManager->publish("TIMING_SCAN_STOPVALUE", stopVal);
    isManager->publish("TIMING_SCAN_STEPSIZE", stepSize);
    isManager->publish("TIMING_SCAN_STEPDUR", stepDur);
    for (unsigned int idxVar = 0; idxVar<timVarList.size(); idxVar++) isManager->publish(timVarList[idxVar]+"/TIMING_SCAN_STATUS", mode);
      
  } else if (mode == "ABORT") {
    for (unsigned int idxVar = 0; idxVar<timVarList.size(); idxVar++) isManager->publish(timVarList[idxVar]+"/TIMING_SCAN_STATUS", mode); 
  }
}
