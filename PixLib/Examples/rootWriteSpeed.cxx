#include "RConfig.h"
#include <string>
#include <vector>
#include "TChain.h"
#include "TFile.h"
#include "THashList.h"
#include "TH1.h"
#include "TH2.h"
#include "TKey.h"
#include "TObjString.h"
#include "Riostream.h"
#include "TClass.h"
#include "TSystem.h"
#include <stdlib.h>
#include <sstream>
#include <unistd.h>
#include <sys/time.h>
#include <pthread.h>

unsigned int nrod = 25;
unsigned int dR = 2, dC = 1;
bool createDirBefore = true;
int comprLevel = 1;

TFile *Target;

float vref[144][320];

std::string scanName = "S000000001";
std::string rodName = "ROD_";
std::string modName = "MOD_";
std::string typName = "TYPE_";
std::string aName = "A_";
std::string bName = "B_";

#define MAX_THREADS 4
pthread_t threads[MAX_THREADS];
unsigned int threadData[MAX_THREADS] = { 0, 1, 2, 3 };
pthread_mutex_t threadLock;
pthread_rwlock_t masterLock;
unsigned int nThreadsCompleted=0;

std::vector<bool> rodTb;

void printTime(struct timeval t2, struct timeval t1, struct timeval t0) {
  float dt1 = t2.tv_sec - t1.tv_sec;
  dt1 += (t2.tv_usec - t1.tv_usec)*1e-6;
  float dt2 = t2.tv_sec - t0.tv_sec;
  dt2 += (t2.tv_usec - t0.tv_usec)*1e-6;
  std::cout << "DT = " << dt1 << " " << dt2 << std::endl;
}

void createDir() {

  TDirectory *scanDir;
  TDirectory *rodDir;
  TDirectory *modDir;
  TDirectory *typDir;
  TDirectory *aDir;
  TDirectory *bDir;


  Target->cd();
  scanDir = Target->mkdir(scanName.c_str(), "");
  //scanDir->ResetBit(kMustCleanup);
  for (unsigned int ir=0; ir<nrod; ir++) {
    std::cout << ir << std::endl;
    std::ostringstream rss;
    rss << ir;
    scanDir->cd();
    rodDir = scanDir->mkdir((rodName+rss.str()).c_str(), "");
    //rodDir->ResetBit(kMustCleanup);
    for (unsigned int im=0; im<13; im++) {
      std::ostringstream mss;
      mss << im;
      rodDir->cd();
      modDir = rodDir->mkdir((modName+mss.str()).c_str(), "");
      //modDir->ResetBit(kMustCleanup);
      for (unsigned int it=0; it<3; it++) {
	std::ostringstream tss;
	tss << it;
	modDir->cd();
	typDir = modDir->mkdir((typName+tss.str()).c_str(), "");
	//typDir->ResetBit(kMustCleanup);
	for (unsigned int ia=0; ia<1; ia++) {
	  std::ostringstream ass;
	  ass << ia;
	  typDir->cd();
	  aDir = typDir->mkdir((aName+ass.str()).c_str(), "");
	  //aDir->ResetBit(kMustCleanup);
	  for (unsigned int ib=0; ib<18; ib++) {
	    std::ostringstream bss;
	    bss << ib;
	    aDir->cd();
	    bDir = aDir->mkdir((bName+bss.str()).c_str(), "");
	    //bDir->ResetBit(kMustCleanup);
	    bDir->cd();
	    if (!createDirBefore) {
	      // Crete the histogram
	      std::string tit = "Test_"+rss.str()+"_"+mss.str()+"_"+tss.str()+"_"+ass.str()+"_"+bss.str();
	      TH2F *h = new TH2F(tit.c_str(), tit.c_str(), 144,0.,144.,320,0.,320.);
	      h->ResetBit(kMustCleanup);
	      for (unsigned int i=0; i<144; i+=dR) {
		for (unsigned int j=0; j<320; j+=dC) {
		  if (j != i+ir) {
		    h->SetBinContent(i, j, vref[i][j]);
		  }
		}
	      }
	      h->Write(tit.c_str(), TObject::kSingleKey);
	      delete h;
	    }
	  }
	}	
      }
    }
  }
}

void writeHisto(unsigned int rStart) {
  for (unsigned int ir=rStart; ir<rStart+1; ir++) {
    std::cout << ir << std::endl;
    std::ostringstream rss;
    rss << ir;
    std::string path = "/"+scanName+"/"+rodName+rss.str()+"/";
    for (unsigned int im=0; im<13; im++) {
      std::ostringstream mss;
      mss << im;
      std::string mpath = path + modName+mss.str()+"/";
      for (unsigned int it=0; it<3; it++) {
	std::ostringstream tss;
	tss << it;
	std::string tpath = mpath + typName+tss.str()+"/";
	for (unsigned int ia=0; ia<1; ia++) {
	  std::ostringstream ass;
	  ass << ia;
	  std::string apath = tpath + aName+ass.str()+"/";
	  for (unsigned int ib=0; ib<18; ib++) {
	    std::ostringstream bss;
	    bss << ib;
	    std::string bpath = apath + bName+bss.str();
	    pthread_mutex_lock(&threadLock);
	    Target->Cd(bpath.c_str());
	    // Crete the histogram
	    std::string tit = "Test_"+rss.str()+"_"+mss.str()+"_"+tss.str()+"_"+ass.str()+"_"+bss.str();
	    TH2F *h = new TH2F(tit.c_str(), tit.c_str(), 144,0.,144.,320,0.,320.);
	    h->ResetBit(kMustCleanup);
	    pthread_mutex_unlock(&threadLock);
	    for (unsigned int i=0; i<144; i+=dR) {
	      for (unsigned int j=0; j<320; j+=dC) {
		if (j != i+ir) {
		  h->SetBinContent(i, j, vref[i][j]);
		}
	      }
	    }
	    pthread_mutex_lock(&threadLock);
            h->Write(tit.c_str(), TObject::kSingleKey);
	    pthread_mutex_unlock(&threadLock);
	    delete h;
	  }
	}	
      }
    }
  }
}

void *runTest (void *threadid) {
  // Get the master read lock
  pthread_rwlock_rdlock(&masterLock);

  bool loop = true;
  while (loop) {
    // Exedcute one ROD
    unsigned int rodN; 
    pthread_mutex_lock(&threadLock);
    loop = false;
    for (unsigned int ir=0; ir<rodTb.size(); ir++) {
      if (!rodTb[ir]) {
	rodN = ir;
	loop = true;
	rodTb[ir] = true;
	break;
      }
    }
    pthread_mutex_unlock(&threadLock);
    if (loop) {
      writeHisto(rodN);
    }
  }

  // Release the master lock
  pthread_mutex_lock(&threadLock);
  nThreadsCompleted++;
  pthread_mutex_unlock(&threadLock);
  pthread_rwlock_unlock(&masterLock);
  // Terminate the thread
  pthread_exit(NULL);

}

int main( int argc, char **argv ) {

  struct timeval t0,t1,t2,t3; 

  // Fill reference histogram
  for (unsigned int i=0; i<320; i++) {
    for (unsigned int j=0; j<144; j++) {
      float rnd = 1000.0*random()/RAND_MAX;
      vref[j][i] = rnd;
    }
  }

  // Create the root file
  gettimeofday(&t0,NULL); 
  std::string fName = argv[1];
  std::cout << "Target file: " << fName << std::endl;
  Target = TFile::Open(fName.c_str(), ("CREATE") );
  if (!Target || Target->IsZombie()) {
    std::cerr << "Error opening target file (does " << fName << " exist?)." << std::endl;
    exit(1);
  }  
  //Target->ResetBit(kMustCleanup);
  Target->SetCompressionLevel(comprLevel);

  // Create directory structure
  createDir();
 
  // Init locks
  pthread_mutex_init(&threadLock,0);
  pthread_rwlock_init(&masterLock,0);

  // Write histograms
  if (createDirBefore) {
    unsigned int it;
    for (it=0; it<nrod; it++) {
      rodTb.push_back(false);
    }
    for (it=0; it<1; it++) {
      pthread_create(&threads[it], NULL, runTest, (void *) threadData[it]);
    }
    // Wait for completion
    usleep(5000);
    pthread_rwlock_wrlock(&masterLock);
    std::cout << "# of completed threads = " << nThreadsCompleted << std::endl;
  }
  gettimeofday(&t1,NULL); 
  printTime(t1,t0,t0);

  Target->Write();
  gettimeofday(&t2,NULL); 
  printTime(t2,t1,t0);
  Target->Close();
  delete Target;
  gettimeofday(&t3,NULL);
  printTime(t3,t2,t0);
  return 0;
}
