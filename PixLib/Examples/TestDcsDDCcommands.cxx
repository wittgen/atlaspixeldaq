// 23-january-2008
// Test of the new commander method: execCommand.

#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <ipc/partition.h>
#include <ipc/core.h>


#include "PixDcs/PixDcs.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

PixDcs *dcs;

void readDDC(std::string pp0Name, float& value)
{
  PixDcsDefs::DataPointType ipin = PixDcsDefs::IPIN_MON;
  int tryToRead=3;
  if (dcs == NULL) return;
  do {
    try {
      dcs->read(pp0Name, ipin, value);
      tryToRead = 0;
    } catch (PixDcsExc e) {
      if (e.getId() == "ISINFORMATION") {
	sleep(7); // 4.11.2008: Pixel ELMB readout takes 7s
	tryToRead--;
	if (tryToRead == 0) {
	  std::string message;
	  message = "Information not published";
	  std::cout << message << std::endl;
	}
      }
      else if (e.getId() == "ISSERVERREPOSITORY"
	       || e.getId() == "ISSERVERNAME"
	       || e.getId() == "ISINFOCOMPATIBLE") {
	std::string message = "IS Server cannot be used";
	std::cout << message << std::endl;
        exit(-1);
      }
    } catch (...) {
      std::string message;
      message = "Unknown exception was thrown";
      std::cout << message << std::endl;
      exit(-1);
    }
  } while(tryToRead>0);

}

int main(int argc, char **argv) {
  if(argc<5) {
    std::cout << "USAGE: TestDcsDDCcommands [DDC command ... see list below] [set value] [ROD_NAME] [Connectivity Obj Name]" << std::endl;
    std::cout << "-set_hv -set_viset -set_vpin -set_vvdc -set_vddd -set_vdda -test" <<std::endl;
    return 1;
  }

  std::string ipcPartitionName="PixelInfr";
  std::string isServerName="DDC";
  std::stringstream stringvalue;
  stringvalue << argv[2];
  float value;
  stringvalue >> value;
  std::string rodName = argv[3];
  std::string obj = argv[4];

  PixDcsDefs::DDCCommands command;
  int status;

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  
  PixMessages *msg = new PixMessages();
  msg->pushPartition(ipcPartition->name(), ipcPartition);
  msg->pushStream("cout", std::cout);

  // Get PixDcs Interface
  dcs = new PixDcs(ipcPartition, isServerName, rodName, msg);

  try {
    std::string c=argv[1];
    if (c == "-set_hv") {
      command = PixDcsDefs::SET_HV;
    } else if (c == "-set_viset") {  
      command = PixDcsDefs::SET_VISET;
    } else if (c == "-set_vpin") {  
      command = PixDcsDefs::SET_VPIN;
    } else if (c == "-set_vvdc") {  
      command = PixDcsDefs::SET_VVDC;
    } else if (c == "-set_vddd") {  
      command = PixDcsDefs::SET_VDDD;
    } else if (c == "-set_vdda") { 
      command = PixDcsDefs::SET_VDDA;
    } else if (c == "-test") {  
      command = PixDcsDefs::TEST_DDC;
    } else if (c == "-read") {
      float val;
      readDDC(obj, val);
      std::cout << obj << " - " << value << " = " << val << std::endl;
      return 0;     
    }
    status = dcs->writeDataPoint(obj, value, command, 20);
    std::cout << "FEEDBACK IS: "; 
    if (status==PixDcsDefs::OK) {
      std::cout<< " OK "  << std::endl;
    } else if (status==PixDcsDefs::ERR_TIMEOUT) {
      std::cout<< " timeout "  << std::endl;
    } else {
      std::cout<< " status: " << status  << std::endl;
    }
  } catch(...) {
    std::cout << "Unknown exception was thrown" << std::endl;
  }
  
  delete dcs;
  return 0;
}

