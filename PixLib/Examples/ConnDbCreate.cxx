#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixDbCoralDB/PixDbCoralDB.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "RCCVmeInterface.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixBoc/PixBoc.h"
#include "ConfigRootIO/ConfigStoreFactory.h"
#include "ConfigRootIO/ConfigStore.h"

#include <sys/stat.h>
#include <dirent.h>

enum mainMode { RECREATE, CREATEROD, CREATEMOD, CREAD, DUMP, WRITE, DUMPPL, WRITEPL, DUMPCF, WRITECF, LISTTAGS, TODBS, HELP  };
enum mainMode gMode;

std::string idTag = "CT";
std::string gConnTag = "";
std::string gCfgTag = "";
std::string destTag = "";
std::string fileName = "";
std::string inFileName = "";
std::string objName = "";
std::string gFileToLoadFrom = "";
bool gVerbose = false;
bool newTag = false;
bool full = false;
bool fromFiles = false;
bool dbServ  = false;
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
std::string moduleName = "";
unsigned int cfgRev = 1164000000;

PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;
PixConnectivity *conn;
SctPixelRod::VmeInterface *vme = NULL;    

void moduleType(std::string type, PixModule::MCCflavour &mccFlv, PixModule::FEflavour &feFlv, int &nFe){
  mccFlv = PixModule::PM_NO_MCC;
  feFlv   = PixModule::PM_NO_FE;
  nFe=0;
  // determine FE type from MODULE_*-string in connectivity
  if(type=="MODULE"){
    mccFlv = PixModule::PM_MCC_I2;
    feFlv  = PixModule::PM_FE_I2;
    nFe = 16;
  } else if(type.substr(type.size()-3,3)=="I4A"){
    mccFlv = PixModule::PM_NO_MCC;
    feFlv  = PixModule::PM_FE_I4A;
    std::stringstream snFe;
    snFe << type.substr(7,type.size()-10);
    snFe >> nFe;
  } else if(type.substr(type.size()-3,3)=="I4B"){
    mccFlv = PixModule::PM_NO_MCC;
    feFlv  = PixModule::PM_FE_I4B;
    std::stringstream snFe;
    snFe << type.substr(7,type.size()-10);
    snFe >> nFe;
  }
}
void tt() {
  struct timeval t;
  gettimeofday(&t, NULL);
  std::cout << t.tv_sec << "." << t.tv_usec << std::endl;
}

std::string getTime() {
  char t_s[100];
  time_t t;
  tm t_m;
  t = time(NULL);
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

bool fileExists(std::string name) {
  struct stat buf;
  int ret = stat(name.c_str(), &buf);
  if (ret < 0) return 0;
  if (S_ISDIR(buf.st_mode) != 0) return false;
  return true;
}

bool dirExists(std::string name) {
  struct stat buf;
  int ret = stat(name.c_str(), &buf);
  if (ret < 0) return 0;
  if (S_ISDIR(buf.st_mode) != 0) return true;
  return false;
}

void listDir(std::string path, std::vector<std::string>&out) {
  DIR *dp;
  struct dirent *ep;

  dp = opendir(path.c_str());
  if (dp != NULL) {
    ep = readdir(dp);
    while (ep) {
      std::string name = path+"/"+ep->d_name;
      out.push_back(name);
      ep = readdir(dp);
    }
    closedir(dp);
  }
}



PixDbInterface *openDb(std::string dbname, std::string modeArg) {
  PixDbInterface *cDb=nullptr;
  std::string::size_type dotpos = dbname.find_last_of('.');
  if(dotpos != std::string::npos) {
    if( dbname.substr(dotpos)==".root" ) {
      cDb = new RootDb(dbname, modeArg);
    }
    else if( dbname.substr(dotpos)==".crl" ) {
      if (modeArg == "UPDATE") {
	cDb = new PixDbCoralDB("sqlite_file:"+dbname, PixDbCoralDB::UPDATE, AutoCommit(false), "ROOT");
      } else if (modeArg == "RECREATE") {
	cDb = new PixDbCoralDB("sqlite_file:"+dbname, PixDbCoralDB::RECREATE, AutoCommit(false), "ROOT");
      }
    }
    else {
      std::cout<<"Unrecognized DB file name extension:"<<dbname.substr(dotpos)<<std::endl;
      exit(-1);
    }
  }
  else {
    std::cout<<"Empty DB file name extension is not supported."<<std::endl;
    exit(-1);
  }
  return cDb;
}

std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

bool setBool(std::string type, std::string name, std::string value, std::string xname, bool &b) {
  if (type == "BOOL" && name == xname) {
    b = true;
    if (value == "0") b = false;
    return true;
  }
  return false;
}

bool setString(std::string type, std::string name, std::string value, std::string xname, std::string &b) {
  if (type == "STRING" && name == xname) {
    b = value;
    return true;
  }
  return false;
}

bool setInt(std::string type, std::string name, std::string value, std::string xname, int &b) {
  if (type == "INT" && name == xname) {
    if (value.substr(0,2) == "0x") {
      std::istringstream is(value.substr(2));
      is >> std::hex >> b;
    } else {
      std::istringstream is(value);
      is >> b;
    }
    return true;
  }
  return false;
}

bool setFloat(std::string type, std::string name, std::string value, std::string xname, float &b) {
  if (type == "FLOAT" && name == xname) {
    std::istringstream is(value);
    is >> b;
    return true;
  }
  return false;
}

void writeConfig(std::string cfgname, std::string tagCF, std::string tagConn) {
  // Load the connectivity db
  conn->setConnTag(tagConn, tagConn, tagConn, idTag);
  conn->loadConn();

  // Set cfg catalog tag
  try {
    conn->createCfgTag(tagCF);
  }
  catch (PixConnectivityExc &e) {
    if (e.getId() == "CANTCREATETAGS") {
      conn->setCfgTag(tagCF);
    } else {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATETAGS", "Cannot create configuration DB tags"));
    }
  }

  std::string name;
  std::ifstream cfg(cfgname.c_str());
  std::string line; 
  std::vector<unsigned int> vit;
  std::vector<std::string> vnam;
  std::vector<std::string> vel;
  while (!cfg.eof()) {
    std::getline(cfg, line);
    if (line != "" && line[0] != '#') {
      std::string el0 = element(line, 0, '\t');
      std::string el1 = element(line, 1, '\t');
      std::string el2 = element(line, 2, '\t');
      std::string el3 = element(line, 3, '\t');
      if (el0 == "->") {
	std::istringstream is(el1);
	unsigned int it;
	is >> it;
	//conn->addCfg(name, el2, it);
        vit.push_back(it);
        vnam.push_back(name);
        vel.push_back(el2); 
	std::cout << name << " - " << el2 << std::endl;
      } else {
	name = el0;
	if (el1 == "RUNCONF_CFG") {
	  name = "RUNCONF:"+name;
	} else if (el1 == "DISABLE_CFG") {
	  name = "DISABLE:"+name;
	} else if (el1 == "SCAN_CFG") {
	  name = "SCAN:"+name;
	}
      }
    }
  }
  conn->addCfg(vnam, vel, vit);
}

void writeConnectivityPL(std::string cfgname, std::string tag1, std::string tag2, bool xfull) {
  // Load the connectivity db
  conn->setConnTag(tag1, tag1, tag1, idTag);
  if (tag2 != "") conn->createConnTag(tag2, tag2, tag2, idTag);
  conn->loadConn();
  if (!xfull) conn->connTransStart();

  std::ifstream cfg(cfgname.c_str());
  std::string line; 
  while (!cfg.eof()) {
    std::getline(cfg, line);
    if (line != "" && line[0] != '#') {
      std::string el0 = element(line, 0, '\t');
      std::string el1 = element(line, 1, '\t');
      std::string el2 = element(line, 2, '\t');
      std::string el3 = element(line, 3, '\t');
      std::string el4 = element(line, 4, '\t');
      if (el1 == "PARTITION") {
        if (conn->parts.find(el0) == conn->parts.end()) {
	  conn->parts[el0] = new PartitionConnectivity(el0);
	}
	setString(el2, el3, el4, "TDAQ_daqPartName", conn->parts[el0]->daqPartName());
	setString(el2, el3, el4, "TDAQ_ddcPartName", conn->parts[el0]->ddcPartName());
	setString(el2, el3, el4, "TDAQ_calibISServName", conn->parts[el0]->calibISServName());
	setString(el2, el3, el4, "TDAQ_ddcISServName", conn->parts[el0]->ddcISServName());
        if (!xfull) ((GenericConnectivityObject*)(conn->parts[el0]))->writeConfig(0);
      } else if (el1 == "RODCRATE") { 
        if (conn->crates.find(el0) == conn->crates.end()) {
	  conn->crates[el0] = new RodCrateConnectivity(el0);
	}
	setString(el2, el3, el4, "TDAQ_histoServName", conn->crates[el0]->histoServName());
	setBool(el2, el3, el4, "Enable_readout", conn->crates[el0]->enableReadout);
        if (!xfull) ((GenericConnectivityObject*)(conn->crates[el0]))->writeConfig(0);
      } else if (el1 == "SBC") {
        if (conn->sbcs.find(el0) == conn->sbcs.end()) {
	  conn->sbcs[el0] = new SbcConnectivity(el0);
	}
	setString(el2, el3, el4, "Network_ipName", conn->sbcs[el0]->ipName());
	setString(el2, el3, el4, "Network_ipAddr", conn->sbcs[el0]->ipAddr());
	setString(el2, el3, el4, "Network_hwAddr", conn->sbcs[el0]->hwAddr());
	setString(el2, el3, el4, "Network_ipName1", conn->sbcs[el0]->ipName1());
	setString(el2, el3, el4, "Network_ipAddr1", conn->sbcs[el0]->ipAddr1());
	setString(el2, el3, el4, "Network_hwAddr1", conn->sbcs[el0]->hwAddr1());
        if (!xfull) ((GenericConnectivityObject*)(conn->sbcs[el0]))->writeConfig(0);
     }  else if (el1 == "TIM") {
        if (conn->tims.find(el0) == conn->tims.end()) {
	  conn->tims[el0] = new TimConnectivity(el0);
	}
	setString(el2, el3, el4, "Config_timConfig", conn->tims[el0]->timConfig());
	setBool(el2, el3, el4, "Enable_readout", conn->tims[el0]->enableReadout);
        if (!xfull) ((GenericConnectivityObject*)(conn->tims[el0]))->writeConfig(0);
      } else if (el1 == "RODBOC") {
        if (conn->rods.find(el0) == conn->rods.end()) {
	  conn->rods[el0] = new RodBocConnectivity(el0);
	}
	setString(el2, el3, el4, "Config_rodConfig", conn->rods[el0]->rodConfig());
	setString(el2, el3, el4, "Config_bocConfig", conn->rods[el0]->bocConfig());
	setBool(el2, el3, el4, "Enable_readout", conn->rods[el0]->enableReadout);
	if (!xfull)  ((GenericConnectivityObject*)(conn->rods[el0]))->writeConfig(0);
      } else if (el1 == "OPTOBOARD") {
        if (conn->obs.find(el0) == conn->obs.end()) {
	  conn->obs[el0] = new OBConnectivity(el0);
	}
	setFloat(el2, el3, el4, "Config_vIset", conn->obs[el0]->vIset());
	setFloat(el2, el3, el4, "Config_vPin", conn->obs[el0]->vPin());
        if (!xfull) ((GenericConnectivityObject*)(conn->obs[el0]))->writeConfig(0);
      } else if (el1 == "PP0") {
        if (conn->pp0s.find(el0) == conn->pp0s.end()) {
	  conn->pp0s[el0] = new Pp0Connectivity(el0);
	}
	setString(el2, el3, el4, "Links_CL", conn->pp0s[el0]->coolingLoop());
        //if (!xfull) ((GenericConnectivityObject*)(conn->pp0s[el0]))->writeConfig(0);
      } else if (el1.substr(0,6) == "MODULE") {
        if (conn->mods.find(el0) == conn->mods.end()) {
	  PixModule::MCCflavour mccFlv;
	  PixModule::FEflavour feFlv;
	  int nFe;
	  moduleType(el1, mccFlv, feFlv, nFe);
	  conn->mods[el0] = new ModuleConnectivity(el0, mccFlv, feFlv, nFe);
	}
	setBool(el2, el3, el4, "Enable_readout", conn->mods[el0]->enableReadout);
        setInt(el2, el3, el4, "Identifier_ModuleId", conn->mods[el0]->modId);
	setInt(el2, el3, el4, "Identifier_GroupId", conn->mods[el0]->groupId);
	setString(el2, el3, el4, "Config_modConfig", conn->mods[el0]->modConfig());
        if (!xfull) ((GenericConnectivityObject*)(conn->mods[el0]))->writeConfig(0);
      } else if (el1 == "OBLINKMAP") {
        if (conn->obmaps.find(el0) == conn->obmaps.end()) {
	  conn->obmaps[el0] = new OBLinkMap(el0);
	}
	int i;
	for (i=0; i<8; i++) {
	  std::ostringstream o;
	  o << "_" << i;
	  setInt(el2, el3, el4, "Maps_dciFiber"+o.str(), conn->obmaps[el0]->dciFiber[i]);
	  setInt(el2, el3, el4, "Maps_dtoFiber"+o.str(), conn->obmaps[el0]->dtoFiber[i]);
	  setInt(el2, el3, el4, "Maps_dto2Fiber"+o.str(), conn->obmaps[el0]->dto2Fiber[i]);
	}
	if (!xfull) ((GenericConnectivityObject*)(conn->obmaps[el0]))->writeConfig(0);
     } else if (el1 == "RODBOCLINKMAP") {
        if (conn->bocmaps.find(el0) == conn->bocmaps.end()) {
	  conn->bocmaps[el0] = new RodBocLinkMap(el0);
	}
	int i,j;
	for (i=0; i<4; i++) {
	  for (j=0; j<8; j++) {
	    std::ostringstream o;
	    o << "_" << i << "_" << j;
	    setInt(el2, el3, el4, "Maps_RodTx"+o.str(), conn->bocmaps[el0]->rodTx[i][j]);
	    setInt(el2, el3, el4, "Maps_RodRx40"+o.str(), conn->bocmaps[el0]->rodRx40[i][j]);
	    setInt(el2, el3, el4, "Maps_RodRx80a"+o.str(), conn->bocmaps[el0]->rodRx80a[i][j]);
	    setInt(el2, el3, el4, "Maps_RodRx80b"+o.str(), conn->bocmaps[el0]->rodRx80b[i][j]);
	  }
	}        
        if (!xfull) ((GenericConnectivityObject*)(conn->bocmaps[el0]))->writeConfig(0);
      }
    }
  }
  
  if (xfull) {
    std::map<std::string, ModuleConnectivity*>::iterator im;
    for (im=conn->mods.begin(); im != conn->mods.end(); im++) {
      ModuleConnectivity *m = im->second;
      Pp0Connectivity *pp0 = m->pp0();
      if (pp0->ob() != NULL) {
	m->groupId = pp0->ob()->bocRxSlot(0);
      } else {
	if (m->groupId < 0) m->groupId = 0;
      }
      m->modId = m->groupId*7 + (m->pp0Slot()-1)%7;
      m->groupId = m->modId%4;
    }
  }

  if (xfull) conn->saveConn();
  else       conn->connTransCommit();
  conn->loadConn();
}

void writeConnectivity(std::string cfgname, std::string tag, bool newtag) {
  conn->clearConn();
  std::ifstream cfg(cfgname.c_str());
  std::string line; 
  while (!cfg.eof()) {
    std::getline(cfg, line);
    if (line != "" && line[0] != '#') {
      std::cout << line << std::endl;
      std::string el0 = element(line, 0, '\t');
      std::string el1 = element(line, 1, '\t');
      std::string el2 = element(line, 2, '\t');
      std::string el3 = element(line, 3, '\t');
      std::string el4 = element(line, 4, '\t');
      std::string el5 = element(line, 5, '\t');
      std::string el6 = element(line, 6, '\t');
      if (el1 == "CONTAINER") {
	if (el6 == "") {
	  if (el4 == "PARTITION") {
            if (conn->parts.find(el3) == conn->parts.end()) {
	      conn->parts[el3] = new PartitionConnectivity(el3);
	    }
	  } else if (el4 == "PP0") {
            if (conn->pp0s.find(el3) == conn->pp0s.end()) {
	      conn->pp0s[el3] = new Pp0Connectivity(el3);
	    }
	  } else if (el4 == "OBLINKMAP") {
            if (conn->obmaps.find(el3) == conn->obmaps.end()) {
	      conn->obmaps[el3] = new OBLinkMap(el3);
	    }
	  } else if (el4 == "RODBOCLINKMAP") {
            if (conn->bocmaps.find(el3) == conn->bocmaps.end()) {
	      conn->bocmaps[el3] = new RodBocLinkMap(el3);
	    }
	  }
	}
      } else if (el1 == "PARTITION") {
        if (conn->parts.find(el0) == conn->parts.end()) {
	  conn->parts[el0] = new PartitionConnectivity(el0);
	}
	if (el6 == "") {
	  if (el4 == "RODCRATE") {
            if (conn->crates.find(el3) == conn->crates.end()) {
	      conn->crates[el3] = new RodCrateConnectivity(el3);
	    }
	    conn->parts[el0]->addCrate(el3, conn->crates[el3]);
	  }
	}
      } else if (el1 == "RODCRATE") { 
        if (conn->crates.find(el0) == conn->crates.end()) {
	  conn->crates[el0] = new RodCrateConnectivity(el0);
	}
	if (el6 == "") {
	  if (el4 == "RODBOC") {
	    if (conn->rods.find(el3) == conn->rods.end()) {
	      conn->rods[el3] = new RodBocConnectivity(el3);
	    }
	    std::size_t pos = el2.find("SLOT_");
	    if (pos != std::string::npos) {
	      std::istringstream snum(el2.substr(pos+5));
	      int islot;
	      snum >> islot;
	      conn->crates[el0]->addRodBoc(islot, conn->rods[el3]);
	    }
	  } else if (el4 == "SBC") {
	    if (conn->sbcs.find(el3) == conn->sbcs.end()) {
	      conn->sbcs[el3] = new SbcConnectivity(el3);
	    }
	    std::size_t pos = el2.find("SLOT_");
	    if (pos != std::string::npos) {
	      std::istringstream snum(el2.substr(pos+5));
	      int islot;
	      snum >> islot;
	      conn->crates[el0]->addSbc(islot, conn->sbcs[el3]);
	    }
	  } else if (el4 == "TIM") {
	    if (conn->tims.find(el3) == conn->tims.end()) {
	      conn->tims[el3] = new TimConnectivity(el3);
	    }
	    std::size_t pos = el2.find("SLOT_");
	    if (pos != std::string::npos) {
	      std::istringstream snum(el2.substr(pos+5));
	      int islot;
	      snum >> islot;
	      conn->crates[el0]->addTim(islot, conn->tims[el3]);
	    }
	  }
	}
      } else if (el1 == "SBC") {
        if (conn->sbcs.find(el0) == conn->sbcs.end()) {
	  conn->sbcs[el0] = new SbcConnectivity(el0);
	}
      } else if (el1 == "TIM") {
        if (conn->tims.find(el0) == conn->tims.end()) {
	  conn->tims[el0] = new TimConnectivity(el0);
	}
      } else if (el1 == "RODBOC") {
        if (conn->rods.find(el0) == conn->rods.end()) {
	  conn->rods[el0] = new RodBocConnectivity(el0);
	}
	if (el6 == "") {
	  if (el4 == "OPTOBOARD") {
            if (conn->obs.find(el3) == conn->obs.end()) {
	      conn->obs[el3] = new OBConnectivity(el3);
	    }
	    int ix[3] = { -1, -1, -1 };
            if (el2 == "A") el2 = "AA";
            if (el2 == "B") el2 = "BB";
            if (el2 == "C") el2 = "CC";
            if (el2 == "D") el2 = "DD";
	    for (unsigned int i=0; i<el2.size(); i++) {
	      if (i<3) {
		if (el2[i] == 'A') ix[i] = 0;
		if (el2[i] == 'B') ix[i] = 1;
		if (el2[i] == 'C') ix[i] = 2;
		if (el2[i] == 'D') ix[i] = 3;
	      }
	    }
	    if (ix[0] >= 0 && ix[1] >=  0) {
	      conn->rods[el0]->addOB(ix[0], ix[1], ix[2], conn->obs[el3]);
	    }
	  } else if (el4 == "RODBOCLINKMAP") {
	    if (conn->bocmaps.find(el3) == conn->bocmaps.end()) {
	      conn->bocmaps[el3] = new RodBocLinkMap(el3);
	    }
	    conn->rods[el0]->linkMap(conn->bocmaps[el3]);
	  } else if (el4 == "CTRLTYPE") {
	    conn->rods[el0]->setRodBocType(el2);
	    conn->rods[el0]->setRodBocInfo(el3);
	    el5 = el5.substr(0, el5.length()-1); // seems like \n is also read...
	    if(el5!="UP") conn->rods[el0]->setFitFarmInstance(el5);
	  }
	}
      } else if (el1 == "OPTOBOARD") {
        if (conn->obs.find(el0) == conn->obs.end()) {
	  conn->obs[el0] = new OBConnectivity(el0);
	}
	if (el6 == "") {
	  if (el4 == "PP0") {
	    if (conn->pp0s.find(el3) == conn->pp0s.end()) {
	      conn->pp0s[el3] = new Pp0Connectivity(el3);
	    }
	    conn->obs[el0]->addPp0(conn->pp0s[el3]);
	  } else if (el4 == "OBLINKMAP") {
	    if (conn->obmaps.find(el3) == conn->obmaps.end()) {
	      conn->obmaps[el3] = new OBLinkMap(el3);
	    }
	    conn->obs[el0]->linkMap(conn->obmaps[el3]);
	  }
	}
      } else if (el1 == "PP0") {
        if (conn->pp0s.find(el0) == conn->pp0s.end()) {
	  conn->pp0s[el0] = new Pp0Connectivity(el0);
	}
	if (el6 == "") {
	  if (el4.substr(0,6) == "MODULE") {
            if (conn->mods.find(el3) == conn->mods.end()) {
	      PixModule::MCCflavour mccFlv;
	      PixModule::FEflavour feFlv;
	      int nFe;
	      moduleType(el4, mccFlv, feFlv, nFe);
	      conn->mods[el3] = new ModuleConnectivity(el3, mccFlv, feFlv, nFe);
	    }
            if (el2[0] == 'M') el2 = el2.substr(1);
	    std::istringstream snum(el2);
	    int islot = -1;
	    snum >> islot;
	    if (islot >= 0) {
	      conn->pp0s[el0]->addModule(islot, conn->mods[el3]);
	    }
	  }
	}
      } else if (el1.substr(0,6) == "MODULE") {
        if (conn->mods.find(el0) == conn->mods.end()) {
	  PixModule::MCCflavour mccFlv;
	  PixModule::FEflavour feFlv;
	  int nFe;
	  moduleType(el1, mccFlv, feFlv, nFe);
	  conn->mods[el0] = new ModuleConnectivity(el0, mccFlv, feFlv, nFe);
	}
      } else if (el1 == "OBLINKMAP") {
        if (conn->obmaps.find(el0) == conn->obmaps.end()) {
	  conn->obmaps[el0] = new OBLinkMap(el0);
	}
      } else if (el1 == "RODBOCLINKMAP") {
        if (conn->bocmaps.find(el0) == conn->bocmaps.end()) {
	  conn->bocmaps[el0] = new RodBocLinkMap(el0);
	}
      }
    }
  }
  if (newtag) conn->createConnTag(tag, tag, tag, idTag);
  conn->setConnTag(tag, tag, tag, idTag);
  conn->saveConn();
}

void openDbServer(int argc, char** argv) {   
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void copyToDbs(std::string tag, int argc, char** argv) {
  // Load the connectiivty
  conn->clearConn();
  conn->setConnTag(tag, tag, tag, idTag);
  conn->loadConn();
  // Open DB servwer connection
  if (DbServer == NULL) openDbServer(argc, argv);
  if (DbServer != NULL) {
    // Dump to DB server
    conn->dumpInDb(DbServer);
  } else {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
}

void dumpConnectivity(std::string tag, std::string ctag, int dump, bool verboseArg) {

  // Load the connectiivty
  conn->clearConn();
  conn->setConnTag(tag, tag, tag, idTag);
  // Set the connectivity tag
  if (ctag != "") {
    conn->setCfgTag(ctag);
  }
  // Load the connectivity DB
  conn->loadConn();

  // Dump the tree structure
  if (dump>0) {
    if (dump == 1) {
      std::cout << "ROOT\tROOT\tPARTITIONS\tPARTITIONS\tCONTAINER\tUP" << std::endl; 
      std::cout << "ROOT\tROOT\tPP0S\tPP0S\tCONTAINER\tUP" << std::endl; 
      std::cout << "ROOT\tROOT\tOBLINKMAPS\tOBLINKMAPS\tCONTAINER\tUP" << std::endl; 
      std::cout << "ROOT\tROOT\tRODBOCLINKMAPS\tRODBOCLINKMAPS\tCONTAINER\tUP" << std::endl; 
    }
    std::map<std::string, PartitionConnectivity*>::iterator i1;
    for (i1=conn->parts.begin(); i1!=conn->parts.end(); ++i1) {
      PartitionConnectivity *p = (*i1).second;
      if (dump==1) {
	std::cout << "PARTITIONS\tCONTAINER\t" << p->name() << "\t";
	std::cout << p->name() << "\tPARTITION\tUP" << std::endl; 
      }
    }
    std::map<std::string, Pp0Connectivity*>::iterator i2;
    for (i2=conn->pp0s.begin(); i2!=conn->pp0s.end(); ++i2) {
      Pp0Connectivity *p = (*i2).second;
      if (dump==1) {
	std::cout << "PP0S\tCONTAINER\t" << p->name() << "\t";
	std::cout << p->name() << "\tPP0\tUP" << std::endl; 
      }
    }
    std::map<std::string, OBLinkMap*>::iterator i3;
    for (i3=conn->obmaps.begin(); i3!=conn->obmaps.end(); ++i3) {
      OBLinkMap *p = (*i3).second;
      if (dump==1) {
	std::cout << "OBLINKMAPS\tCONTAINER\t" << p->name() << "\t";
	std::cout << p->name() << "\tOBLINKMAP\tUP" << std::endl; 
      } else if (dump==2) {
	int i;
	for (i=0; i<8; i++) {
	  std::ostringstream o;
	  o << "_" << i;
	  std::cout << p->name() << "\tOBLINKMAP\tINT\tMaps_dciFiber" << o.str() << "\t" << p->dciFiber[i] << std::endl;
	  std::cout << p->name() << "\tOBLINKMAP\tINT\tMaps_dtoFiber" << o.str() << "\t" << p->dtoFiber[i] << std::endl;
	  std::cout << p->name() << "\tOBLINKMAP\tINT\tMaps_dto2Fiber" << o.str() << "\t" << p->dto2Fiber[i] << std::endl;
	}
      }
    }
    std::map<std::string, RodBocLinkMap*>::iterator i4;
    for (i4=conn->bocmaps.begin(); i4!=conn->bocmaps.end(); ++i4) {
      RodBocLinkMap *p = (*i4).second;
      if (dump==1) {
	std::cout << "RODBOCLINKMAPS\tCONTAINER\t" << p->name() << "\t";
	std::cout << p->name() << "\tRODBOCLINKMAP\tUP" << std::endl; 
      } else if (dump==2) {
	int i,j;
	for (i=0; i<4; i++) {
	  for (j=0; j<8; j++) {
	    std::ostringstream o;
	    o << "_" << i << "_" << j;
            std::cout << p->name() << "\tRODBOCLINKMAP\tINT\tMaps_RodTx" << o.str() << "\t";
            if (p->rodTx[i][j] >= 0) std::cout << "0x" << std::hex;
	    std::cout << p->rodTx[i][j] << std::dec << std::endl;
	  }
	}
	for (i=0; i<4; i++) {
	  for (j=0; j<8; j++) {
	    std::ostringstream o;
	    o << "_" << i << "_" << j;
            std::cout << p->name() << "\tRODBOCLINKMAP\tINT\tMaps_RodRx40" << o.str() << "\t";
            if (p->rodRx40[i][j] >= 0) std::cout << "0x" << std::hex;
	    std::cout << p->rodRx40[i][j] << std::dec << std::endl;
	  }
	}
	for (i=0; i<4; i++) {
	  for (j=0; j<8; j++) {
	    std::ostringstream o;
	    o << "_" << i << "_" << j;
            std::cout << p->name() << "\tRODBOCLINKMAP\tINT\tMaps_RodRx80a" << o.str() << "\t";
            if (p->rodRx80a[i][j] >= 0) std::cout << "0x" << std::hex;
	    std::cout << p->rodRx80a[i][j] << std::dec << std::endl;
	  }
	}
	for (i=0; i<4; i++) {
	  for (j=0; j<8; j++) {
	    std::ostringstream o;
	    o << "_" << i << "_" << j;
            std::cout << p->name() << "\tRODBOCLINKMAP\tINT\tMaps_RodRx80b" << o.str() << "\t";
            if (p->rodRx80b[i][j] >= 0) std::cout << "0x" << std::hex;
	    std::cout << p->rodRx80b[i][j] << std::dec << std::endl;
	  }
	}
      }
    }
  }
  std::map<std::string, PartitionConnectivity*>::iterator i;
  for (i=conn->parts.begin(); i!=conn->parts.end(); ++i) {
    bool partEna = true;
    bool crateEna = true;
    bool rodEna = true;
    if (dump==0) {
      std::cout << "Partition " << i->second->name();
      std::cout << std::endl;
    }
    if (dump==2) {
      std::cout << i->second->name() << "\tPARTITION\tSTRING\tTDAQ_daqPartName\t" << i->second->daqPartName() << std::endl;
      std::cout << i->second->name() << "\tPARTITION\tSTRING\tTDAQ_ddcPartName\t" << i->second->ddcPartName() << std::endl;
      std::cout << i->second->name() << "\tPARTITION\tSTRING\tTDAQ_calibISServName\t" << i->second->calibISServName() << std::endl;
      std::cout << i->second->name() << "\tPARTITION\tSTRING\tTDAQ_ddcISServName\t" << i->second->ddcISServName() << std::endl;
    }
    PartitionConnectivity *p = (*i).second;
    std::map<std::string, RodCrateConnectivity*>::iterator j;
    for (j=p->rodCrates().begin(); j!=p->rodCrates().end(); ++j) {
      RodCrateConnectivity *c = (*j).second;
      if (dump==0) {
	std::cout << "  Crate " << c->name();
	crateEna = true;
	if (c->enableReadout) {
	  std::cout << " ENA";
	} else {
	  std::cout << " DIS";
	}
	if (partEna && c->enableReadout) {
	  std::cout << " (ENA)";
	} else {
	  std::cout << " (DIS)";
	  crateEna = false;
	}
	std::cout << std::endl;
      }
      if (dump==1) {
	std::cout << p->name() << "\tPARTITION\t" << (*j).first;
	std::cout << "\t" << c->name() << "\tRODCRATE\tUP" << std::endl;
      } else if (dump==2) {
	std::cout << c->name() << "\tRODCRATE\tSTRING\tTDAQ_histoServName\t" << c->histoServName() << std::endl;
	std::cout << c->name() << "\tRODCRATE\tBOOL\tEnable_readout\t" << c->enableReadout << std::endl;
      }
      std::map<int, RodBocConnectivity*>::iterator k;
      if (c->tim() != NULL) {
	if (dump==0) {
          std::cout << "    TIM " << c->tim()->name() << std::endl;
	  if (ctag != "") {
	    std::cout << "      Config " << conn->getCfgName(c->tim()->name()) << std::endl;
	  }
	}
	if (dump==1) {
	  std::cout << c->name() << "\tRODCRATE\tSLOT_" << c->tim()->vmeSlot();
	  std::cout << "\t" << c->tim()->name() << "\tTIM\tUP" << std::endl; 
	} else if (dump==2) {
	  std::cout << c->tim()->name() << "\tTIM\tSTRING\tConfig_timConfig\t" << c->tim()->timConfig() << std::endl;
	  std::cout << c->tim()->name() << "\tTIM\tBOOL\tEnable_readout\t" << c->tim()->enableReadout << std::endl;
	}
      }
      if (c->sbc() != NULL) {
	if (dump==0) std::cout << "    SBC " << c->sbc()->name() << std::endl;
	if (dump==1) {
	  std::cout << c->name() << "\tRODCRATE\tSLOT_" << c->sbc()->vmeSlot();
	  std::cout << "\t" << c->sbc()->name() << "\tSBC\tUP" << std::endl; 
	} else if (dump==2) {
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_ipName\t" << c->sbc()->ipName() << std::endl;
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_ipAddr\t" << c->sbc()->ipAddr() << std::endl;
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_hwAddr\t" << c->sbc()->hwAddr() << std::endl;
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_ipName1\t" << c->sbc()->ipName1() << std::endl;
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_ipAddr1\t" << c->sbc()->ipAddr1() << std::endl;
	  std::cout << c->sbc()->name() << "\tSBC\tSTRING\tNetwork_hwAddr1\t" << c->sbc()->hwAddr1() << std::endl;
	}
      }
      for (k=c->rodBocs().begin(); k!=c->rodBocs().end(); ++k) {
	RodBocConnectivity *r = (*k).second;
	if (dump==0) {
	  std::cout << "    ROD " << k->second->name();
	  std::cout << " RodId " << k->second->offlineId();          
	  rodEna = true;
	  if (k->second->enableReadout) {
	    std::cout << " ENA";
	  } else {
	    std::cout << " DIS";
	  }
	  if (partEna && crateEna && k->second->enableReadout) {
	    std::cout << " (ENA)";
	  } else {
	    std::cout << " (DIS)";
	    rodEna = false;
	  }
	  std::cout << std::endl;
	  if (r->rol() != NULL) {
	    std::cout << "      ROL " << r->rol()->name();
	    if (r->rol()->robin() != NULL) {
	      std::cout << " - ROBIN " << r->rol()->robin()->name();
	      if (r->rol()->robin()->ros() != NULL) {
		std::cout << " - ROS " << r->rol()->robin()->ros()->name();
	      }
	    }
	    std::cout << std::endl;
	  }
	  if (ctag != "") {
	    std::cout << "      Config " << conn->getCfgName(((*k).second)->name()) << std::endl;
	  }
	}
	if (dump==1) {
	  std::cout << c->name() << "\tRODCRATE\tSLOT_" << r->vmeSlot();
	  std::cout << "\t" << r->name() << "\tRODBOC\tUP" << std::endl; 
	} else if (dump==2) {
	  std::cout << r->name() << "\tRODBOC\tSTRING\tConfig_rodConfig\t" << r->rodConfig() << std::endl;
	  std::cout << r->name() << "\tRODBOC\tSTRING\tConfig_bocConfig\t" << r->bocConfig() << std::endl;
	  std::cout << r->name() << "\tRODBOC\tBOOL\tEnable_readout\t" << r->enableReadout << std::endl;
	}
        if (r->linkMap() != NULL && dump==1) {
	  std::cout << r->name() << "\tRODBOC\tLINK_MAP";
	  std::cout << "\t" << r->linkMap()->name() << "\tRODBOCLINKMAP\t" << r->name() << std::endl; 
	}
	int l;
	char lnam[4] = { 'A', 'B', 'C', 'D' };
	for (l=0; l<4; l++) {
	  if (r->obs(l) != NULL) {
	    std::string slotName;
	    slotName = lnam[r->obs(l)->bocTxSlot()];
	    slotName += lnam[r->obs(l)->bocRxSlot(0)];
	    if (r->obs(l)->bocRxSlot(1)>=0) slotName += lnam[r->obs(l)->bocRxSlot(1)];
	    if (slotName == "AA") slotName = "A";
	    if (slotName == "BB") slotName = "B";
	    if (slotName == "CC") slotName = "C";
	    if (slotName == "DD") slotName = "D";
	    OBConnectivity *ob = r->obs(l);
	    if (dump==0) {
	      std::cout << "      OB " << ob->name();
	      std::cout << " Rx0=" << ob->bocRxSlot(0); 
	      std::cout << " Rx1=" << ob->bocRxSlot(1); 
	      std::cout << " Tx=" << ob->bocTxSlot();
	    } else if (dump==1) {
	      std::cout << r->name() << "\tRODBOC\t" << slotName;
	      std::cout << "\t" << ob->name() << "\tOPTOBOARD\tUP" << std::endl; 
	    } else if (dump==2) {
	      //std::cout << ob->name() << "\tOPTOBOARD\tSTRING\tCables_txRibbonId\t" << ob->txRibbonId << std::endl;
	      //std::cout << ob->name() << "\tOPTOBOARD\tSTRING\tCables_rxRibbonId_0\t" << ob->rxRibbonId[0] << std::endl;
	      //std::cout << ob->name() << "\tOPTOBOARD\tSTRING\tCables_rxRibbonId_1\t" << ob->rxRibbonId[1] << std::endl;
	      std::cout << ob->name() << "\tOPTOBOARD\tFLOAT\tConfig_vIset\t" << ob->vIset() << std::endl;
	      std::cout << ob->name() << "\tOPTOBOARD\tFLOAT\tConfig_vPin\t" << ob->vPin() << std::endl;
	    }
            if (ob->linkMap() != NULL && dump==0) {
	      std::cout << " LinkMapName " << ob->linkMap()->name();
	    }	
	    if (dump==0) std::cout << std::endl;
	    if (ob->linkMap() != NULL && dump==1) {
	      std::cout << ob->name() << "\tOPTOBOARD\tLINK_MAP";
	      std::cout << "\t" << ob->linkMap()->name() << "\tOBLINKMAP\tUP" << std::endl; 
	    }
	    if (ob->pp0() != NULL) {
	      Pp0Connectivity *pp = ob->pp0();
	      if (dump==0) {
		std::cout << "        PP0 " << pp->name();
		if (pp->coolingLoop() != "") {
		  std::cout << " (Cooling Loop: " << pp->coolingLoop() << ") ";
		}
		std::cout << std::endl;
	      }
	      if (dump==1) {
		std::cout << ob->name() << "\tOPTOBOARD\tPP0";
		std::cout << "\t" << pp->name() << "\tPP0\tOB";
		std::cout << std::endl; 
	      }
	      if (dump==2) {
		std::cout << pp->name() << "\tOPTOBOARD\tSTRING\tLinks_CL\t" << pp->coolingLoop() << std::endl;
	      }
	      int m;
	      for (m=1; m<=8; m++) {
		if (pp->modules(m) != NULL) {
		  ModuleConnectivity *mod = pp->modules(m);
		  if (dump==0) {
		    std::cout << "          Module " << mod->name() << " " << mod->prodId();
		    std::cout << " Slot " << mod->pp0Slot();
		    std::cout << " OfflineId " << mod->offlineId();
		    if (mod->enableReadout) {
		      std::cout << " ENA";
		    } else {
		      std::cout << " DIS";
		    }
		    if (partEna && crateEna && rodEna && mod->enableReadout) {
		      std::cout << " (ENA)";
		    } else {
		      std::cout << " (DIS)";
		    }
		    std::cout << std::endl;
                    if (verboseArg) {
		      std::cout << "            InLink " << mod->inLink() << std::endl;
		      PixModuleGroup::MccBandwidth speed = PixModuleGroup::SINGLE_40;
		      std::cout << "            OutLinks 40 " << std::hex; 
		      std::cout << std::setw(2) << setfill('0') << mod->outLink1A(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink1B(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink2A(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink2B(speed) << "/";
		      std::cout << std::dec << std::endl;
		      speed = PixModuleGroup::SINGLE_80;
		      std::cout << "            OutLinks 80 " << std::hex;
		      std::cout << std::setw(2) << setfill('0') << mod->outLink1A(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink1B(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink2A(speed) << "/";
		      std::cout << std::setw(2) << setfill('0') << mod->outLink2B(speed) << "/";
		      std::cout << std::dec << std::endl; 
		      std::cout << "            Rx0: Rx = " << ((mod->bocLinkRx(0) == -1) ? -1 : mod->bocLinkRx(0)/10) << " Rx input = " << mod->bocLinkRx(0)%10 << std::endl; 
		      std::cout << "            Rx1: Rx = " << ((mod->bocLinkRx(1) == -1) ? -1 : mod->bocLinkRx(1)/10) << " Rx input = " << mod->bocLinkRx(1)%10 << std::endl; 
		      std::cout << "            Tx:  Tx = " << ((mod->bocLinkTx() == -1) ? -1 : mod->bocLinkTx()/10) << " Tx output = " << mod->bocLinkTx()%10 << std::endl; 
		      //std::cout << "[KJP] " << mod->offlineId() << std::endl;
          std::string linkMapName = mod->pp0()->ob()->linkMap()->name();
          //std::cout << "[KJP] " << mod->pp0()->ob()->rodBoc()->linkMap()->name() << std::endl;
          //std::cout << "[KJP] ";
          //for(int i = 0 ; i < 4 ; ++i) {
          //  std::cout << "[KJP] ";
          //  for(int j = 0 ; j < 8 ; ++j)
          //    std::cout << mod->pp0()->ob()->rodBoc()->linkMap()->rodRx40[i][j] << "\t";
          //  std::cout << std::endl;
          //}
          //for (auto n : mod->pp0()->ob()->rodBoc()->linkMap()->rodRx40) std::cout << "\t" << n;
          //std::cout << std::endl;
          //auto linkMapRx40 = mod->pp0()->ob()->rodBoc()->linkMap()->rodRx40;
          //auto linkMapTx   = mod->pp0()->ob()->rodBoc()->linkMap()->rodTx;
          // Currently the Tx maps are incorrect
          //uint16_t txCh = linkMapTx[mod->bocLinkTx()%10][mod->bocLinkTx()/10];
          //uint16_t rxCh = linkMapRx40[mod->bocLinkRx(1)%10][mod->bocLinkRx(1)/10];
		      if( ( mod->name()[0] == 'L' && ( mod->name()[1] == '0' || mod->name()[1] == '1' || mod->name()[1] == '2' ) ) || mod->name()[0] == 'D' ) {
		        uint16_t rxCh = 0;
		        uint16_t txCh = 0;
		        uint16_t bocOutputLink = mod->useAltLink ? mod->bocLinkRx(1) : mod->bocLinkRx(0);
		        bocOutputLink = mod->bocLinkRx(1);
		        uint16_t bocInputLink = mod->bocLinkTx();
		        switch(bocOutputLink/10) {
		          case 0: rxCh = 31 - (bocOutputLink%10) ; break;
		          case 1: rxCh = 23 - (bocOutputLink%10) ; break;
		          case 2: rxCh =  7 - (bocOutputLink%10) ; break;
		          case 3: rxCh = 15 - (bocOutputLink%10) ; break;
		        }

		        switch(bocInputLink/10) {
		          case 0: txCh = 23 - (bocInputLink%10) ; break;
		          case 1: txCh = 31 - (bocInputLink%10) ; break;
		          case 2: txCh = 15 - (bocInputLink%10) ; break;
		          case 3: txCh =  7 - (bocInputLink%10) ; break;
		        }
		        if( mod->name()[0] == 'L' && ( mod->name()[1] == '1' || mod->name()[1] == '2' ) ) {
		          std::cout << "            L12ROD mapping (Tx/Rx1/Rx2): ";
		          std::cout << txCh << "/" << rxCh << "/" << std::endl;
            }
		        if( (mod->name()[1] == '0' || mod->name()[0] == 'D') ) { // B-Layer or Disk
              uint32_t optoPlugin1 = mod->bocRxLink1()/10;
              uint32_t optoChannel1 = mod->bocRxLink1()%10;
              //uint32_t optoPlugin2 = mod->bocRxLink2()/10;
              //uint32_t optoChannel2 = mod->bocRxLink2()%10;
              //std::cout << "[KJP] " << mod->outLink1A() << "\t" << optoPlugin1 << "/" << optoChannel1 << "\t" << optoPlugin2 << "/" << optoChannel2 << std::endl;
              if( linkMapName == "B_TYPE" ) {
                switch(optoPlugin1) {
                  case 0: rxCh = 30 - optoChannel1/2 ; break; // Plugin A
                  case 1: rxCh = 27 - optoChannel1/2 ; break; // Plugin B
                  case 2: rxCh = 11 - optoChannel1/2 ; break; // Plugin C
                  case 3: rxCh = 14 - optoChannel1/2 ; break; // Plugin D
                }
              } else { // Disk
                switch(optoPlugin1) {
                  case 0: rxCh = 31 - optoChannel1 ; break; // Plugin A
                  case 1: rxCh = 23 - optoChannel1 ; break; // Plugin B
                  case 2: rxCh =  7 - optoChannel1 ; break; // Plugin C
                  case 3: rxCh = 15 - optoChannel1 ; break; // Plugin D
                }
              }
              // rxCh = linkMapRx40[optoPlugin1][optoChannel1];
              std::cout << "            BLDROD mapping (Tx/Rx1/Rx2): ";
		          std::cout << txCh << "/" << rxCh << "/" << std::endl;
		          //std::cout << conn->bocmaps[el0]->rodRx40[i][j] << std::endl;
		        }
		      }
		    }
		    if (ctag != "") {
		      std::cout << "            Config " << conn->getCfgName(mod->name()) << std::endl;
		    }
		  } else if (dump==1) {
		    std::cout << pp->name() << "\tPP0\t" << m;
		    std::cout << "\t" << mod->name() << "\tMODULE\tUP" << std::endl;
		  } else if (dump==2) {
		    //std::cout << mod->name() << "\tMODULE\tSTRING\tIdentifier_ProdId\t" << mod->prodId() << std::endl;
		    //std::cout << mod->name() << "\tMODULE\tSTRING\tCables_t0CableId\t" << mod->t0CableId << std::endl;
		    std::cout << mod->name() << "\tMODULE\tBOOL\tEnable_readout\t" << mod->enableReadout << std::endl;
		    std::cout << mod->name() << "\tMODULE\tINT\tIdentifier_ModuleId\t" << mod->modId << std::endl;
		    std::cout << mod->name() << "\tMODULE\tINT\tIdentifier_GroupId\t" << mod->groupId << std::endl;
		    std::cout << mod->name() << "\tMODULE\tSTRING\tConfig_modConfig\t" << mod->modConfig() << std::endl;
                    std::cout << mod->name() << "\tMODULE\tSTRING\tConfig_useAltLink\t" << mod->useAltLink << std::endl;
		  } 
		}
	      }
	    }
	  } 
	}
      }
    }
  }
}

void listCfgFiles(std::string name, std::string name1, std::string type) { 
  std::string path = std::string(getenv("PIX_CFG_PREF")) + "/" + name;
  if (dirExists(path)) {
    std::vector<std::string> files;
    listDir(path, files);
    bool first = true;
    for (unsigned int i=0; i<files.size(); i++) {
      if (element(files[i], 1, '.') == "root") {
	if (first) { std::cout << name1 << "\t" << type << std::endl; first = false; }
	std::size_t pos = files[i].find_last_of("_");
	if (pos != std::string::npos) {
	  std::string sdate = files[i].substr(pos+1);
	  sdate = element(sdate, 0, '.');
	  sdate = sdate.substr(0, sdate.size()-3);
	  int date = 0;
	  std::istringstream is(sdate);
	  is >> date;
	  std::cout << "->\t" << date << "\t" << files[i] << "\t(" << getTime(date) << ")" << std::endl;
	}
      }
    }
  }
}

void  dumpCfgFromFile(std::string connTagArg) {
  try {
    conn->setConnTag(connTagArg, connTagArg, connTagArg, idTag);
    conn->loadConn();
  }
  catch (PixConnectivityExc &e) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "TAGNOTFOUND", "CONN tag " + connTagArg + " does not exists"));
  }
  // Loop over RODBOCs
  {
    std::map<std::string, RodBocConnectivity*>::iterator i1;
    for (i1=conn->rods.begin(); i1!=conn->rods.end(); i1++) {
      listCfgFiles(i1->second->name(), i1->second->name(), "RODBOC_CFG");
    }
  }
  // Loop over TIMs
  {
    std::map<std::string, TimConnectivity*>::iterator i1;
    for (i1=conn->tims.begin(); i1!=conn->tims.end(); i1++) {
      listCfgFiles(i1->second->name(), i1->second->name(), "TIM_CFG");
    }
  }
  // Loop ober Modules
  {
    std::map<std::string, ModuleConnectivity*>::iterator i1;
    for (i1=conn->mods.begin(); i1!=conn->mods.end(); i1++) {
      listCfgFiles(i1->second->prodId(), i1->second->name(), "MODULE_CFG");
    }
  }
}

void  dumpConfig(std::string cfgTagArg, std::string connTagArg) {
  try {
    conn->setConnTag(connTagArg, connTagArg, connTagArg, idTag);
    // Set cfg catalog tag
    try {
      conn->setCfgTag(cfgTagArg);
    }
    catch (PixConnectivityExc &e) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "TAGNOTFOUND", "CFG tag " + cfgTagArg + " does not exists"));
    }
    conn->loadConn();
  }
  catch (PixConnectivityExc &e) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "TAGNOTFOUND", "CONN tag " + connTagArg + " does not exists"));
  }

  std::map<std::string, std::string> a;
  a = conn->listCfgObj("");
  std::map<std::string, std::string>::iterator it;
  for (it = a.begin(); it != a.end(); it++) {
    std::string name = it->first;
    if (it->second == "MODULE_CFG") {
      std::map<std::string, ModuleConnectivity*>::iterator im;
      for (im=conn->mods.begin(); im != conn->mods.end(); im++) {
	if (im->second->prodId() == name) {
	  name = im->second->name();
	  break;
	}
      }
    }
    std::cout << name << "\t" << it->second << std::endl;
    std::map<std::string, std::string> b;
    b = conn->listCfgRevRep(it->first);
    std::map<std::string, std::string>::iterator it1;
    for (it1 = b.begin(); it1 != b.end(); it1++) {
      std::string stim = it1->first;
      std::istringstream is(stim.substr(0,stim.size()-3));
      unsigned int utim;
      is >> utim;
      std::cout << "->\t" << stim.substr(0,stim.size()-3) << "\t" << it1->second << "\t(" << getTime(utim) << ") - " << stim.substr(stim.size()-3) << std::endl;
    }
  }
}

void  WriteModGroupConfig(std::string conntag, std::string cfgtag, std::string name) {
  // Load the connectivity db
  conn->setConnTag(conntag, conntag, conntag, idTag);
  conn->loadConn();

  conn->setCfgTag(cfgtag);
  std::map<std::string, TimConnectivity*>::iterator i;
  for (i=conn->tims.begin(); i!=conn->tims.end(); ++i) {
    std::string ltim = ((*i).second)->name();
    if (name == "" || name == ltim) {
      std::cout << "Tim " << ltim << std::endl;
      TimPixTrigController *timC = new TimPixTrigController(ltim+"_CFG");
      //unsigned int it = time(NULL);
      DbRecord *r = conn->addCfg(ltim, cfgRev);
      if(!r) { std::cerr << "ERROR: null pointer at " << __LINE__ << std::endl; exit(1); }
      r->getDb()->transactionStart();   
      timC->saveConfig(r);
      r->getDb()->transactionCommit();   
      delete timC;
    }
  }

  for (auto const & rod_map: conn->rods) {
    auto const & rod = rod_map.second;
    std::string lrod = rod->name();
    if (name == "" || name == lrod) {
      CtrlType ctrlTypeID = UNKNOWN;
      bool haveBoc = false;
      std::string ctrlType = rod->rodBocType();
      if(ctrlType=="CPPRODBOC" || ctrlType == "CPPROD") {
	ctrlTypeID = CPPROD; 
	if(ctrlType=="CPPRODBOC") haveBoc = true;
      } else if(ctrlType=="L12RODBOC" || ctrlType == "L12ROD") {
	ctrlTypeID = L12ROD; 
	if(ctrlType=="L12RODBOC") haveBoc = true;
      } else if(ctrlType=="DUMMY") {
	ctrlTypeID = DUMMY; 
      } else {
        throw std::runtime_error("The specified controller: "+ctrlType+" for ROD "+lrod+" is not supported.");
      }
      auto mg = PixModuleGroup(lrod, ctrlTypeID, haveBoc);
      if (mg.getPixController() == nullptr) {
        std::cout << "Could not get PixController for ROD "+lrod+", this results in the ROD being skipped!\n";
	continue;
      }
      Config& rodcfg = mg.getPixController()->config();
      if(ctrlTypeID==CPPROD || ctrlTypeID==L12ROD) {
	std::string iblRodIP = "";
	if(rod->rodBocInfo()!="VME") iblRodIP = rod->rodBocInfo();
	std::cout << "ROD IP SET TO: " << iblRodIP << std::endl;
	((ConfString&)rodcfg["general"]["IPaddress"]).m_value = iblRodIP;
      } 
      // auto-turn on used BOC Tx links (laser to 0xa0)
      if(haveBoc){
	std::array<int,8> inl { 9, 8, 7, 6, 5, 4, 3, 2 };
	for(int k=0;k<4;k++){
	  if(rod->obs(k)!=0 && rod->obs(k)->pp0()!=0){
	    for(int l=1;l<=8;l++){
	      if(rod->obs(k)->pp0()->modules(l)!=0){
		mg.getPixBoc()->getTx(k)->setTxLaserCurrent(inl[rod->obs(k)->pp0()->modules(l)->pp0Slot()-1]-2, 0xa0);
	      }
	    }
	  }
	}
      }
      DbRecord *r = conn->addCfg(lrod, cfgRev);
      r->getDb()->transactionStart();   
      mg.saveConfig(r);
      r->getDb()->transactionCommit();   
    }
  }
}


void moduleConfigWriteNoConnLoading(std::string conntag, std::string cfgtag, std::string modname_in, std::string infile);

void moduleConfigWriteMulti(std::string conntag, std::string cfgtag, std::string fileToLoadFromArg) {
  // Load the connectivity db
  conn->setConnTag(conntag, conntag, conntag, idTag);
  conn->loadConn();

	std::string modname_in; std::string infile;

	fstream in_file(fileToLoadFromArg.c_str());
	while(in_file.good()) {
		in_file >> modname_in >> infile;
		if( modname_in == "" || infile == "" ) continue;
		std::cout << "Loading " << modname_in <<  " from " << infile  << std::endl;
		moduleConfigWriteNoConnLoading(conntag,cfgtag,modname_in,infile);
		modname_in = ""; infile = "";
	}
}

void moduleConfigWrite(std::string conntag, std::string cfgtag, std::string modname_in, std::string infile) {
  // Load the connectivity db
  conn->setConnTag(conntag, conntag, conntag, idTag);
  conn->loadConn();

	moduleConfigWriteNoConnLoading(conntag,cfgtag,modname_in,infile);
}

void moduleConfigWriteNoConnLoading(std::string conntag, std::string cfgtag, std::string modname_in, std::string infile) {
  // set flavours
  PixModule::MCCflavour mccFlv = PixModule::PM_MCC_I2;
  PixModule::FEflavour  feFlv  = PixModule::PM_FE_I2;
  int nFe = 16;
  
  unsigned int cel = 0;
  std::string mods = modname_in;
  while (element(mods, cel, ',') != "") {
    std::string modname = element(mods, cel, ',');
    std::cout << "Current name of module :" << modname << std::endl;
    cel++; 
    // Look for this module in the connectivity DB
    std::string prodname = "";
    ModuleConnectivity *modc = 0;
    std::map<std::string, ModuleConnectivity*>::iterator im;
    for (im=conn->mods.begin(); im!=conn->mods.end(); ++im) {
      if (im->second->name() == modname){
	prodname = im->second->prodId();
	modc = im->second;
	break;
      }
    }
    if (prodname != "" || modname == "ALL") {
      bool newBE = true;
      bool modOK = false;
      DbRecord *rootRec = NULL;
      DbRecord *r = NULL;
      if(infile!=""){
	// Read the module config file
	// Try new format
	if(modc!=0) modc->getModPars(mccFlv, feFlv, nFe);
	PixModule *mod=new PixModule("Base", "Base", "Def", mccFlv, feFlv, nFe);
	try {
	  std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
	  store->read(mod->config(), infile, "Def", 0);
	  modOK = true;
	  newBE = true;
	} catch(...) {
	  newBE=false;
	  modOK = false;
	}
	delete mod;
	// Try old format
	if (!modOK) {
	  PixDbInterface *rdb = NULL;
	  try {
	    PixDbInterfaceFactory fact(infile, AutoCommit(false)); 
	    rdb= fact.openDB_readonly(PixDbCompoundTag("Base", "Base", "Base", "Base"));
	  }
	  catch(...) {
	    std::cout << "Unable to open root file " << infile << std::endl;
	    return;
	  }
	  rootRec = rdb->readRootRecord(); 	
	  r = NULL;
	  for (dbRecordIterator it = rootRec->recordBegin(); it != rootRec->recordEnd(); ++it) {
	    if ((*it)->getClassName() == "PixModule") {
	      r = *it;
	      break;
	    }
	  }
	  if (r != NULL) {
	    newBE = false;
	    modOK = true;
	  }
	}
      } else
	modOK = true;
      if (modOK) {
	// Set cfg catalog tag
	try {
	  conn->createCfgTag(cfgtag);
	}
	catch (PixConnectivityExc &e) {
	  if (e.getId() == "CANTCREATETAGS") {
	    conn->setCfgTag(cfgtag);
	  } else {
	    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTCREATETAGS", "Cannot create configuration DB tags"));
	  }
	}
	if (modname != "ALL") {
	  PixModule *mod2;
	  // Create a PixModule object
	  if (newBE) {
	    mod2=new PixModule("Base", "Base", prodname, mccFlv, feFlv,nFe);
	    std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
	    if(infile!="") store->read(mod2->config(), infile, "Def", 0);
	    else           mod2->setModuleGeomConnName(modname);
	  } else {
	    mod2 = new PixModule(r, (PixModuleGroup*)NULL, prodname);
	    mod2->setModuleGeomConnName(modname);
	  }
	  // Save module config
	  mod2->saveConfig(conn, cfgRev, true);
	  //mod2->config().dump(std::cout, "**");
	  delete mod2;
	} else {
	  PixModule *mod2;
	  for (im=conn->mods.begin(); im!=conn->mods.end(); ++im) {
	    // Create a PixModule object
	    if (newBE) {
	      im->second->getModPars(mccFlv, feFlv, nFe);
	      mod2=new PixModule("Base", "Base", ((*im).second)->prodId(), mccFlv, feFlv,nFe);
	      std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
	      if(infile!="") store->read(mod2->config(), infile, "Def", 0);
	    } else {
	      mod2 = new PixModule(r, (PixModuleGroup*)NULL, ((*im).second)->prodId());
	    }
	    mod2->setModuleGeomConnName(im->second->name());
	    // Save module config
	    mod2->saveConfig(conn, cfgRev, true);
	    delete mod2;
	  }
	}
      } else {
	std::cout << "Unable to create/read default config " << std::endl;
      }
    } else {
      std::cout << "Module " << modname << " not found in connectivity DB" << std::endl;
    }
  }
}

void listTags() {
  unsigned int i;
  std::vector<std::string> v;

  std::cout << "Connection Tags : " << std::endl;
  std::cout << "================" << std::endl;
  v = conn->listConnTagsC();
  for (i=0; i<v.size(); i++) std::cout << v[i] << std::endl;
  std::cout << std::endl;
  std::cout << "Alias Tags : " << std::endl;
  std::cout << "============" << std::endl;
  v = conn->listConnTagsA();
  for (i=0; i<v.size(); i++) std::cout << v[i] << std::endl;
  std::cout << std::endl;
  std::cout << "Data Tags : " << std::endl;
  std::cout << "===========" << std::endl;
  v = conn->listConnTagsP();
  for (i=0; i<v.size(); i++) std::cout << v[i] << std::endl;
  std::cout << std::endl;
  std::cout << "Cfg Tags : " << std::endl;
  std::cout << "==========" << std::endl;
  v = conn->listCfgTags();
  for (i=0; i<v.size(); i++) std::cout << v[i] << std::endl;
}

void getParams(int argc, char **argv) {
  int ip = 1;
  gMode = HELP;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	gMode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--idTag") {
	if (ip+1 < argc) {
	  idTag = argv[ip+1];
	  ip += 2;
	} 
      } else if (std::string(argv[ip]) == "--cfgTag") {
	if (ip+1 < argc) {
	  gCfgTag = argv[ip+1];
	  ip += 2;
	} 
      } else if (std::string(argv[ip]) == "--connTag") {
	if (ip+1 < argc) {
	  gConnTag = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--destTag") {
	if (ip+1 < argc) {
	  destTag = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--recreate") {
	gMode = RECREATE; 
	if (ip+1 < argc) {
	  fileName = argv[ip+1];
	  ip += 2;
	} 
      } else if (std::string(argv[ip]) == "--createRodConfig") {
	gMode = CREATEROD; ip++; 
	if (ip < argc) {
	  if (std::string(argv[ip]).substr(0,2) != "--") {
	    objName = argv[ip];
	    ip++; 
	  }
	}      
      } else if (std::string(argv[ip]) == "--createModConfig") {
	gMode = CREATEMOD; ip++; 
	if (ip < argc) {
	  if (std::string(argv[ip]).substr(0,2) != "--") {
	    objName = argv[ip];
	    ip++;
	  } 
	}      
      } else if (std::string(argv[ip]) == "--moduleCfg") {
	if (ip+1 < argc) {
	  inFileName = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--cfgRev") {
	if (ip+1 < argc) {
	  std::istringstream is(argv[ip+1]);
          is >> cfgRev;
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--moduleName") {
	if (ip+1 < argc) {
	  moduleName = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--fileToLoadFrom") {
	if (ip+1 < argc) {
	  gFileToLoadFrom = argv[ip+1];
	  ip += 2;
	}
      } else if (std::string(argv[ip]) == "--read") {
	gMode = CREAD; ip++; 
      } else if (std::string(argv[ip]) == "--dump") {
	gMode = DUMP; ip++; 
      } else if (std::string(argv[ip]) == "--write") {
	if (ip+1 < argc) {
	  gMode = WRITE;
	  fileName = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--dumppl") {
	gMode = DUMPPL; ip++; 
      } else if (std::string(argv[ip]) == "--writepl") {
	if (ip+1 < argc) {
	  gMode = WRITEPL;
	  fileName = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--dumpcf") {
	gMode = DUMPCF; ip++; 
      } else if (std::string(argv[ip]) == "--writecf") {
	if (ip+1 < argc) {
	  gMode = WRITECF;
	  fileName = argv[ip+1];
	  ip++; 
	}
	ip++;
      } else if (std::string(argv[ip]) == "--listTags") {
	gMode = LISTTAGS; ip++; 
      } else if (std::string(argv[ip]) == "--verbose") {
	gVerbose = true; ip++;  
      } else if (std::string(argv[ip]) == "--newTag") {
	newTag = true; ip++; 
      } else if (std::string(argv[ip]) == "--full") {
	full = true; ip++; 
      } else if (std::string(argv[ip]) == "--fromFiles") {
	fromFiles = true; ip++; 
      } else if (std::string(argv[ip]) == "--todbs") {
	if (ip+1 < argc) {
	  gMode = TODBS;
	  gConnTag = argv[ip+1];
	  ip++; 
	}
	ip++;
      } else if (std::string(argv[ip]) == "--dbServ") {
	dbServ = true; ip++; 
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc) {
	  dbsPart = argv[ip+1];
	  ip++; 
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--dbName") {
	if (ip+1 < argc) {
	  dbsName = argv[ip+1];
	  ip++; 
	}
        ip++; 
      } else {
	ip++;
      }
    } else {
      ip++;
    }
  }
}

int main(int argc, char** argv) {
  std::string dbx = "";
  try{
//     try {
//       vme = new SctPixelRod::RCCVmeInterface();
//     }
//     catch (SctPixelRod::VmeException &v) {
//       std::cerr << "Error during VmeInterface initialization" << std::endl;
//     }
    getParams(argc, argv);

    if (gMode == HELP) {
      std::cout << "Usage: ConnDbCreate --help\n"
                << "            // prints this message\n"
                << "       ConnDbCreate --write <connectivity file>  --connTag <connectivity tag> [ --newTag ]\n"
		<< "            // reads <config file> and updates/generates a connectivity tag.\n"
		<< "       ConnDbCreate --writepl <payload_file> --connTag <connectivity tag> [--destTag <destination conn tag> --full] \n"
		<< "            // loads payload data in source_tag and save as destination_tag\n"
		<< "       ConnDbCreate --writecf <config file>  --connTag <connectivity tag> --cfgTag <config tag> \n"
		<< "            // loads configuration index in <config tag> using <connectivity tag>\n"
		<< "       ConnDbCreate --createRodConfig [name] --connTag <connectivity tag>  --cfgTag <config tag> \n"
		<< "            // generates default ROD, BOC and TIM cfg for all objects in conn DB\n"
		<< "       ConnDbCreate --createModConfig --connTag <connectivity tag>  --cfgTag <config tag> --moduleCfg <default RootDb config file> --moduleName <mod>\n"
		<< "            // creates a default config for module <mod> based on a template RootDb file\n"
		<< "       ConnDbCreate --createModConfig --connTag <connectivity tag>  --cfgTag <config tag> --fileToLoadFrom <text file>\n"
		<< "            // creates a config for a module (name is 1st column of <text file>) based on a template RootDb file (2nd column of <text file>)\n"
		<< "       ConnDbCreate --read --connTag <connectivity db tag> [-cfgTag <config tag> --verbose]\n"
		<< "       ConnDbCreate --dump --connTag <connectivity db tag> \n"
		<< "       ConnDbCreate --dumppl --connTag <connectivity db tag> \n"
		<< "            // dumps <connectivity tag>. If --dump or --dumppl is specified the output in spreadsheet format \n"          
		<< "       ConnDbCreate --dumpcf --cfgTag <config tag> --connTag <connectivity tag> [--fromFiles]\n"
		<< "            // dumps <configuration db tag> using <connectivity tag>\n"          
		<< "       ConnDbCreate --listTags \n"
		<< "            // lists tags\n"
		<< "       ConnDbCreate --todbs <connectivity db tag>\n"
		<< "            // copy the specified tag to the db server\n"
                << "\n"
	        << "     Optional switches:\n"
	        << "        --idTag <id tag>\n"
	        << "        --dbServ               Use dbServer instead of direct Oracle connection\n"
	        << "        --dbPart <partition>  Specify partition name (def. PixelInfr\n"
	        << "        --dbName <name>       Specify dbServer name (def. PixelDbServer)\n"
		<<std::endl;
      exit(-1);
    } else if (gMode == RECREATE) {
      std::cout << "=============== Recreate mode" << std::endl;
      PixDbInterfaceFactory factory(fileName, AutoCommit(false));
      PixDbInterface* rDb = factory.openDB_recreate(PixDbCompoundTag(idTag, "Base", "Base", "Base"), "ROOT");
      delete rDb;
    } else {
      if (dbServ) {
	// Open DB servwer connection
	if (DbServer == NULL) openDbServer(argc, argv);
	conn = new PixConnectivity(DbServer, "Base", "Base", "Base", "Base", idTag);
	//conn->cfgFromDb();
      } else {
	conn = new PixConnectivity("Base", "Base", "Base", "Base", idTag);
      }
      if (gMode == LISTTAGS) {
	listTags();
      } else if (gMode == WRITE) {
	std::cout << "=============== Write mode" << std::endl;
        if (fileName != "" && gConnTag != "") {
	  writeConnectivity(fileName, gConnTag, newTag);
	} else {
	  std::cout << "ConnDbCreate: --connTag switch or file name missing" << std::endl;
	}
      } else if (gMode == WRITEPL) {
	std::cout << "=============== Write PL mode" << std::endl;
        if (fileName != "" && gConnTag != "") {
	  writeConnectivityPL(fileName, gConnTag, destTag, full);
	} else {
	  std::cout << "ConnDbCreate: --connTag switch or file name missing" << std::endl;
	}
      } else if (gMode == WRITECF) {
	std::cout << "=============== Write CF mode" << std::endl;
        if (fileName != "" && gConnTag != "") {
	  writeConfig(fileName, gCfgTag, gConnTag);
	} else {
	  std::cout << "ConnDbCreate: --connTag switch or --cfgTag switch or file name missing" << std::endl;
	}
      } else if (gMode == CREATEROD) {
	std::cout << "=============== PixModuleGroup Config Write mode" << std::endl;
        if (gConnTag != "" && gCfgTag != "") {
	  WriteModGroupConfig(gConnTag, gCfgTag, objName);
	} else {
	  std::cout << "ConnDbCreate: --connTag switch or --cfgTag switch missing" << std::endl;
	}
      } else if (gMode == CREATEMOD) {
	std::cout << "=============== PixModule Config Write mode" << std::endl;
				if (gConnTag != "" && gCfgTag != "" && gFileToLoadFrom != "") {
					moduleConfigWriteMulti(gConnTag, gCfgTag, gFileToLoadFrom);
				}
				else if (gConnTag != "" && gCfgTag != "" && moduleName != "") {
	  moduleConfigWrite(gConnTag, gCfgTag, moduleName, inFileName);
	} else {
	  std::cout << "ConnDbCreate: --connTag, --cfgTag or --moduleName switch missing" << std::endl;
	}
      } else if (gMode == DUMPCF) {
	std::cout << "=============== Dump Config mode" << std::endl;
        if (fromFiles) {
	  if (gConnTag != "") {
	    dumpCfgFromFile(gConnTag);
	  } else {
	    std::cout << "ConnDbCreate: --connTag switch missing" << std::endl;
	  }
	} else {
	  if (gConnTag != "" && gConnTag != "") {
	    dumpConfig(gCfgTag, gConnTag);
	  } else {
	    std::cout << "ConnDbCreate: --connTag switch or --cfgTag switch missing" << std::endl;
	  }
	}
      } else if (gMode == CREAD || gMode == DUMP || gMode == DUMPPL) {
	std::cerr << "=============== Read mode" << std::endl;
	int dump = 0;
	if (gMode == DUMP) dump = 1;
	if (gMode == DUMPPL) dump = 2;
        if (gConnTag == "") {
	  std::cout << "ConnDbCreate: --connTag switch missing" << std::endl;
	} else if (gMode == 2 && gConnTag == "") {
	  std::cout << "ConnDbCreate: --cfgTag switch missing" << std::endl;
	} else {
	  dumpConnectivity(gConnTag, gCfgTag, dump, gVerbose);
	}
      } else if (gMode == TODBS) {
	std::cout << "=============== Todbs mode" << std::endl;
        if (gConnTag != "") {
	  copyToDbs(gConnTag, argc, argv);
	} else {
	  std::cout << "ConnDbCreate: invalif Connectivity Tag specified" << std::endl;
	}
      }
    delete conn;
    }
  } 
  catch(PixDBException& e) {
    std::cerr<<"Got PixDBException: ";
    e.what(std::cerr);
    std::cerr<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
  return 0;
}
