#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>


#include <ipc/alarm.h>
#include <ipc/core.h>

#include "Histo/Histo.h"
#include "PixHistoServer/PixHistoServerInterface.h"


using namespace PixLib;

int main(int argc, char **argv) 
{

  std::string ipcPartitionName = "PixelInfr";
  std::string serverName = "nameServer";
  
  std::cout << "CHECK TO HAVE THE IS SERVER pixel_histo_server_HistoClient and pixel_histo_server_helper RUNNING! "<< std::endl;

  // Check arguments
  if(argc<2) {
    std::cout << std::endl
	      << "USAGE: HistoRootFileReDump (root file name) [partition name] [name server name]" 
	      << std::endl 
	      << "e.g.:  HistoRootFileReDump ThresholdScan.root "
	      << std::endl  
	      << "Default parameters:" 
	      << std::endl 
	      << " partition name = " << ipcPartitionName 
	      << std::endl
	      << " name server name = " << serverName
	      << std::endl;
    return 1;
  } 
  if (argc>=3) ipcPartitionName = argv[2];
  if (argc>=4) serverName = argv[3];
  if (argc>5) {
    std::cout << "ERROR too many input parameters." << std::endl;
    return -1;
  }
  
  std::string rootFileName = argv[1];

  std::cout << "*** HistoRootFileReDump ***" << std::endl;
  std::cout << "You are opening: " << rootFileName
	    << std::endl
	    << " in the partition " << ipcPartitionName 
	    << std::endl
	    << " in the name server " << serverName
	    << std::endl;

  struct timeval t0; 
  struct timeval t1;
  int time; 

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  
  OHRootHistogram his;
  time=gettimeofday(&t0,NULL);
  std::string provName = "helper";
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, 
							       std::string(OHSERVERBASE)+provName, 
							       serverName, std::string(PROVIDERBASE)+provName);
    hInt->reCreateOnLineRootFile(rootFileName);
  } catch (PixHistoServerExc e) {
    if (e.getId() =="IPCTIMEOUT" || e.getId() =="NOIPC" ||e.getId() =="NAMESERVER"|| e.getId() =="NAMESERVERPROBLEM") std::cout << e.getDescr() << std::endl;
  } catch (...) {
    std::cout << "HistoRootFileReDump. Unknown exception thrown "<<std::endl;
    return 0;
  }

  time=gettimeofday(&t1,NULL); 
  std::cout << "The Tool spent: " 
	    << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) 
	    << " s for putting in the histogram server the root file " 
	    << rootFileName << std::endl;

  return 0;
}

