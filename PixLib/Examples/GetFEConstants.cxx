///////
//
//   Tags need to be loaded into DbServer???
// check if tags are there with  DumpDbServer = PixLib/Examples/DbServer_monitor --dump'
// use dbserverloader in case not there?
//   PixLib/Examples/DbServer_Loader 
//
////////

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
//#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <sys/time.h>
#include <string>

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include "PixDbServer/PixDbServerInterface.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFe.h"


#include "Config/ConfMask.h"
#include "Config/ConfObj.h"

#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "Config/ConfMask.h"

#include <stdlib.h>



PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;

std::string idTag = "PIXEL";
std::string domainName = "Configuration-"+idTag;
std::string cfgModTag = "PIT_MOD";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
unsigned int revision = 0;

bool help = false;
std::string fname = "PitVcalChargeConversion.root";

struct ModStats
{
  float caplo[16];
  float caphi[16];
  float v0[16];
  float v1[16];
  float v2[16];
  float v3[16];
};

using namespace PixLib;

void openDbServer(int argc, char** argv) {   
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help=true;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsPart = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsName = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--domain") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  domainName = argv[ip+1];
	  ip +=2;
	} else {
	  std::cout << std::endl << "No Domain name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--tag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgModTag = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No Tag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--rev") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
	  revision = atoi(argv[ip+1]);
	  ip += 2; 
	} else  {
	  std::cout << std::endl << "No revision inserted" << std::endl << std::endl;
	  break;
	}
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
}


int main(int argc, char** argv) {

  using std::cout;
  using std::endl;
  using std::string;
  std::string dbx = "";

  try {
    // Open DB servwer connection
    
    getParams(argc, argv);
    if (help) {
      std::cout << "Usage: CheckGDACs\n"
		<< "     Optional switches:\n"
		<< "        --dbPart <partition>             Specify partition name         def. " << dbsPart  << std::endl
		<< "        --dbServ <name>                  Specify dbServer name          def. " << dbsName << std::endl
		<< "        --domain <domain name>           Specify domain name            def. " << domainName << std::endl
		<< "        --tag    <config module tag>     Specify config module tag name def. " << cfgModTag << std::endl
		<< "        --rev    <revision>              Specify object revision        def. " << revision << std::endl
		<< std::endl;
    }

    char * outdir = getenv("PIXSCAN_STORAGE_PATH");
    std::stringstream filename;
    filename << outdir << "/" << fname;
    //    filename << "/home/muellerk/Data/" << fname;
    std::cout << "will write to file " << filename.str() << std::endl;
//     std::cout << "ok? (type 7 if not) ";
//     int stop(0); 
//     std::cin >> stop;
//     if (stop==7) exit(1);
    TFile* root_file = new TFile(filename.str().c_str(), "RECREATE"); 
    
    TTree * tree = new TTree("tree", "Many Calib Numbers");
    ModStats * stats = new ModStats;
  

    // Open DB server connection
    if (DbServer == NULL) openDbServer(argc, argv);
    
    
    std::vector<std::string> modlist;
    DbServer->listObjectsRem(domainName,cfgModTag,"PixModule",modlist);
    std::vector<std::string>::iterator modit;
    for (modit = modlist.begin(); modit != modlist.end(); modit++) {

      PixModule *mod = new PixModule(DbServer,(PixModuleGroup*)NULL,domainName,cfgModTag,*modit);
      //      std::cout << mod->connName() << std::endl;
      tree->Branch((mod->connName()).c_str(), &stats->caplo, "caplo[16]/F:caphi[16]:v0[16]:v1[16]:v2[16]:v3[16]");

      for (std::vector<PixFe*>::iterator fe = mod->feBegin(); fe != mod->feEnd(); fe++){
 	char fe_name[50];
 	sprintf(fe_name,"PixFe_%i",(*fe)->number());
	//	std::cout << "\t" << fe_name << std::endl;
	
	int gdac = (*fe)->readGlobRegister("GLOBAL_DAC");

	PixLib::Config& fe_config = const_cast<PixLib::Config &>((*fe)->config());

	std::string cnamelo = "CInjLo";
	std::string cnamehi = "CInjHi";
	// should not used the casts below, but rather the aux functions from ConfObjUtil.h
	float cInjlo = ((PixLib::ConfFloat&)fe_config["Misc"][cnamelo]).value();
	float cInjhi = ((PixLib::ConfFloat&)fe_config["Misc"][cnamehi]).value();
	float vcal_a = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient3"]).value();
	float vcal_b = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient2"]).value();
	float vcal_c = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient1"]).value();
	float vcal_d = ((PixLib::ConfFloat&)fe_config["Misc"]["VcalGradient0"]).value();
	
	stats->caplo[(*fe)->number()] = cInjlo;
	stats->caphi[(*fe)->number()] = cInjhi;
	stats->v0[(*fe)->number()] = vcal_d;
	stats->v1[(*fe)->number()] = vcal_c;
	stats->v2[(*fe)->number()] = vcal_b;
	stats->v3[(*fe)->number()] = vcal_a;

// 	std::cout << "cInjlo " << cInjlo << std::endl;
// 	std::cout << "cInjhi " << cInjhi << std::endl;	      
// 	std::cout << "vcal_a " << vcal_a << std::endl;
// 	std::cout << "vcal_b " << vcal_b << std::endl;
// 	std::cout << "vcal_c " << vcal_c << std::endl;
// 	std::cout << "vcal_d " << vcal_d << std::endl;
	
	// 	int NvcalSteps = 201;
	// 	for (int step=0; step!=NvcalSteps; ++step)
	// 	  {
	// 	    float v = (float)step; 
	// 	    // convert vcal to charge
	// 	    float q = (((vcal_a*v + vcal_b)*v + vcal_c)*v + vcal_d)*cInjlo/0.160218;
	// 	    chargeArray[fe].push_back(q);
	// 	  }
      }
      
      tree->Fill();
      
    }
    
    tree->Print();
    root_file->Write();
    root_file->Close();
    
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
  e.dump(std::cerr);
  std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
  
  return 0;
}
