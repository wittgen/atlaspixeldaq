#include <vector>
#include <string>
#include <iostream>
#include <sstream>
#include <fstream>
#include <iterator>
#include <iomanip>
#include <stdexcept>
#include <cstdlib>
#include <cassert>
#include <sys/time.h>

#include "PixDbCoralDB/PixDbCoralDB.h"
#include "PixDbInterface/PixDBException.h"
#include "PixDbInterface/PixDbCompoundTag.h"
#include "Histo/Histo.h"
#include "RootDb/RootDb.h"

#include "PixDbInterfaceFactory/PixDbInterfaceFactory.h"

#ifndef _GNU_SOURCE
#define _GNU_SOURCE /* for getopt_long() */
#endif
#include <getopt.h>

using namespace std;
using namespace PixLib;

//================================================================
class TestTimer {
  string m_name;
  struct timeval m_t1;
  struct timeval m_t2;
public:
  // Starts the timer
  TestTimer(const string& name);
  
  void stop();
  const string& name() const { return m_name; }
  double elapsed() const;
};

TestTimer::TestTimer(const string& name) : m_name(name) {
  gettimeofday(&m_t1, 0);
}

void TestTimer::stop() {
  gettimeofday(&m_t2, 0);
}

double TestTimer::elapsed() const {
  return m_t2.tv_sec - m_t1.tv_sec + 1.e-6 * (m_t2.tv_usec - m_t1.tv_usec);
}

std::ostream& operator<<(std::ostream& os, const TestTimer& t) {
  return os<<"TestTimer(name = \""<<t.name()<<"\", elapsed = "<<t.elapsed()<<" )";
}

//================================================================
class ID_Generator {
  string m_prefix;
  unsigned m_current;
  unsigned m_mindigits;
public: 
  ID_Generator(const string& prefix, unsigned offset=0, unsigned mindigits=4) 
    : m_prefix(prefix), m_current(offset), m_mindigits(mindigits) 
  {}
  string nextID();
};

string ID_Generator::nextID() {
  ostringstream os;
  os<<m_prefix<<setw(m_mindigits)<<setfill('0')<<m_current++;
  return os.str();
}

//================================================================
class RandomTree {
public:
  typedef vector<string> DecNameContainer;

  // Keep only one link of each pair of PixDbInterface's "bidirectional" links in the container.
  typedef vector<pair<pair<DbRecord*,string>, pair<DbRecord*, string> > > SoftLinkContainer; // ((srcObj,srcSlotName), (dstObj, dstSlotName))
  typedef set<pair<string,string> > KnownLinkIdSlotContainer; // (obj,slotname), both src and dst

private:
  PixDbInterface *m_db;
  ID_Generator m_idgen;
  ID_Generator m_slotgen;
  DbRecord *m_root;
  DecNameContainer m_allDecNames;

  SoftLinkContainer m_allSoftLinks;
public:
  RandomTree(PixDbInterface* db);
  DbRecord *drawRandomNode() const;
  DbRecord *addRandomNode();
  SoftLinkContainer::reference drawRandomLink();

  dbRecordIterator addRandomLink();
  dbRecordIterator updateRandomLink();
  void deleteRandomLink();

  DecNameContainer::const_iterator decNamesBegin() const { return m_allDecNames.begin(); }
  DecNameContainer::const_iterator decNamesEnd() const { return m_allDecNames.end(); }

  SoftLinkContainer::const_iterator softLinksBegin() const { return m_allSoftLinks.begin(); }
  SoftLinkContainer::const_iterator softLinksEnd() const { return m_allSoftLinks.end(); }

private:
  void addSubTreeInfo(DbRecord *rec, KnownLinkIdSlotContainer *knownLinks); 
};

//----------------------------------------------------------------
void RandomTree::addSubTreeInfo(DbRecord *rec, KnownLinkIdSlotContainer *knownLinks) {
  m_allDecNames.push_back(rec->getDecName());
  for(dbRecordIterator i=rec->recordBegin(); i!=rec->recordEnd(); i++) {
    if( ! i.isLink() ) {
      addSubTreeInfo(*i, knownLinks);
    }
    else {
      // the if() is to skip the backlink if the forward link is already in m_allSoftLinks.
      // What is forward and what is back not defined at the level of PixDbInterface
      // so it is "random" here. (But it is defined e.g. in PixDbCoralDB.)
      if( knownLinks->insert(make_pair(i.srcObject()->getName(), i.srcSlotName() )).second && 
	  knownLinks->insert(make_pair(i.dstObject()->getName(), i.dstSlotName() )).second ) {
	m_allSoftLinks.push_back(make_pair(make_pair(i.srcObject(), i.srcSlotName()), make_pair(i.dstObject(), i.dstSlotName())));
      }
    }
  }
}

//----------------------------------------------------------------
RandomTree::RandomTree(PixDbInterface* db) 
  : m_db(db), m_idgen("rec"), m_slotgen("slt"),
    m_root(db->readRootRecord())
{
  KnownLinkIdSlotContainer knownLinks;
  addSubTreeInfo(m_root, &knownLinks);
}

//----------------------------------------------------------------
DbRecord* RandomTree::drawRandomNode() const {
  unsigned r = rand() % m_allDecNames.size();
  return m_db->DbFindRecordByName(m_allDecNames[r]);
}

//----------------------------------------------------------------
RandomTree::SoftLinkContainer::reference RandomTree::drawRandomLink() {
  unsigned r = rand() % m_allSoftLinks.size();
  return m_allSoftLinks[r];
}

//----------------------------------------------------------------
DbRecord* RandomTree::addRandomNode() {
  DbRecord *parent = drawRandomNode();
  DbRecord *added = parent->addRecord("TestRecord", m_idgen.nextID() );
  m_allDecNames.push_back(added->getDecName());
  return added;
}

//----------------------------------------------------------------
dbRecordIterator RandomTree::addRandomLink() {
  DbRecord *src = drawRandomNode();
  DbRecord *dst = drawRandomNode(); // don't care if dst==src
  string srcSlotName = m_slotgen.nextID();
  string dstSlotName = m_slotgen.nextID();
  m_allSoftLinks.push_back(make_pair(make_pair(src, srcSlotName), make_pair(dst, dstSlotName)));
  return src->linkRecord(dst, srcSlotName, dstSlotName);
}

//----------------------------------------------------------------
dbRecordIterator RandomTree::updateRandomLink() {
  DbRecord *newDstObj = drawRandomNode();
  string newDstSlot = m_slotgen.nextID();

  SoftLinkContainer::reference it = drawRandomLink();

  unsigned updateForwardLink = rand() %2;

  DbRecord *parent = updateForwardLink? it.first.first : it.second.first;
  string srcSlotName = updateForwardLink? it.first.second : it.second.second;

  // Keep the list of all soft links current
  it = make_pair(make_pair(parent, srcSlotName), make_pair(newDstObj, newDstSlot));
  
  return parent->updateLink(newDstObj, srcSlotName, newDstSlot);
}

//----------------------------------------------------------------
void RandomTree::deleteRandomLink() {
  // Not truly "random".  It's inefficient to delete an entry from the
  // middle of m_allSoftLinks vector, so we'll delete the last entry.
  SoftLinkContainer::reference it = *(m_allSoftLinks.rbegin());

  unsigned deleteForwardLink = rand() %2; // 
  DbRecord *parent = deleteForwardLink? it.first.first : it.second.first;
  std::string slot = deleteForwardLink? it.first.second : it.second.second;

  // Keep the list of all soft links current
  m_allSoftLinks.pop_back();

  parent->eraseRecord( parent->findRecord(slot) );
}


//================================================================
namespace {

  // Would be nice to have something like this in PixLib/Histo
  std::string toString(const PixLib::Histo& h) {
    std::ostringstream os;
    os<<"Histo(name=\""<<h.name()<<"\", title=\""<<h.title()<<"\", nDim="<<h.nDim()<<", ";
    switch(h.nDim()) {
    case 0: case 1: {
      os<<"nbins="<<h.nBin(0)<<", ";
      os<<"min="<<h.min(0)<<", ";
      os<<"max="<<h.max(0)<<", ";
      os<<"bins=(";
      for(int i=0; i<h.nBin(0); i++) {
	os<<h(i)<<", ";
      }
      break;
    }
    case 2: {
      os<<"nbin1="<<h.nBin(0)<<", ";
      os<<"min1="<<h.min(0)<<", ";
      os<<"max1="<<h.max(0)<<", ";
      os<<"nbin2="<<h.nBin(1)<<", ";
      os<<"min2="<<h.min(1)<<", ";
      os<<"max2="<<h.max(1)<<", ";
      os<<"bins=(";
      for(int i=0; i<h.nBin(0); i++) {
	os<<"(";
	for(int j=0; j<h.nBin(1); j++) {
	  os<<h(i,j)<<", ";
	}
	os<<"), ";
      }
      break;
    }
    default: 
      ostringstream err;
      err<<"toString(const Histo&): unknown nDim="<<h.nDim();
      throw runtime_error(err.str());
    }
    
    os<<"))";
    return os.str();
  }
}

//================================================================
// Assembles all types listed in PixDbInterface
struct TestData {
  // with bool can test handling of all possible cases :)
  bool bt; 
  bool bf; 

  std::vector<bool> vb;
  int i;
  std::vector<int> vi;
  unsigned int u;
  float f;
  std::vector<float> vf;
  double d;
  std::vector<double> vd;

  std::string s;
  std::string es; // empty string

  // Default ("0-dim") histo, 1D and 2D
  PixLib::Histo h0;
  PixLib::Histo h1;
  PixLib::Histo h2;
};

//================================================================
std::ostream& operator<<(std::ostream& os, const TestData& x) {
  using namespace std;
  os<<setw(80)<<setfill('=')<<"=\n";
  os<<"TestData:\n";

  os<<"bt = "<<x.bt<<"\n";
  os<<"bf = "<<x.bf<<"\n";
  
  os<<"vb = (";
  std::copy(x.vb.begin(), x.vb.end(), std::ostream_iterator<bool>(os, ", "));
  os<<")\n";

  os<<"i = "<<x.i<<"\n";

  os<<"vi = (";
  std::copy(x.vi.begin(), x.vi.end(), std::ostream_iterator<int>(os, ", "));
  os<<")\n";

  os<<"u = "<<x.u<<"\n";

  os<<"f = "<<x.f<<"\n";

  os<<"vf = (";
  std::copy(x.vf.begin(), x.vf.end(), std::ostream_iterator<float>(os, ", "));
  os<<")\n";
  
  os<<"d = "<<x.d<<"\n";

  os<<"vd = (";
  std::copy(x.vd.begin(), x.vd.end(), std::ostream_iterator<double>(os, ", "));
  os<<")\n";
  
  os<<"s = "<<x.s<<"\n";

  os<<"es = "<<x.es<<"\n";

  os<<"h0 = "<<toString(x.h0)<<"\n";
  os<<"h1 = "<<toString(x.h1)<<"\n";
  os<<"h2 = "<<toString(x.h2)<<"\n";

  os<<setw(80)<<setfill('=')<<"=\n";

  return os;
}

//================================================================
// fill randomly
void fillData(TestData *out) {
  //----------------
  out->bt = true;
  out->bf = false;

  //----------------
  unsigned nbools = 5 + rand()%30;
  out->vb.reserve(nbools);
  for(unsigned i=0; i<nbools; i++) {
    out->vb.push_back( double(std::rand())/RAND_MAX > 0.5);
  }

  //----------------
  out->i =  -rand();

  //----------------
  unsigned nints = 5 + rand()%30;
  out->vi.reserve(nints);
  for(unsigned i=0; i<nints; i++) {
    out->vi.push_back(std::rand() - RAND_MAX/2); // test negatives as well
  }

  //----------------
  out->u = rand();

  //----------------
  out->f = rand() - RAND_MAX/2;

  //----------------
  unsigned nfloats = 5 + rand()%30;
  out->vf.reserve(nints);
  for(unsigned i=0; i<nfloats; i++) {
    out->vf.push_back(std::rand() - RAND_MAX/2); // test negatives as well
  }

  //----------------
  out->d = rand() - RAND_MAX/2;

  //----------------
  unsigned ndoubles = 5 + rand()%30;
  out->vd.reserve(nints);
  for(unsigned i=0; i<ndoubles; i++) {
    out->vd.push_back(std::rand() - RAND_MAX/2); // test negatives as well
  }

  //----------------
  out->s = "00Test string123";

  //----------------
  out->es = "";

  //----------------
  // RootDb does not like 0-dim histograms:  
  //out->h0 = PixLib::Histo();
  out->h0 = PixLib::Histo("histo0", "Test 1D histogram", 15, -3, 3);
  
  //----------------
  {
    PixLib::Histo h1("histo1", "Test 1D histogram", 15, -3, 3);
    for(int i=0; i<h1.nBin(0); i++) {
      h1.set(i, double(rand()));
    }
    out->h1 = h1;
  }
  //----------------
  {
    PixLib::Histo h2("histo2", "Test 2D histogram", 4, -2., 2., 5, -1., 1.);
    for(int i=0; i<h2.nBin(0); i++) {
      for(int j=0; j<h2.nBin(1); j++) {
	h2.set(i, j, double(rand()));
      }
    }
    out->h2 = h2;
  }
}

//================================================================
void createTestFields(DbRecord* rec) {
  rec->addField("bt");
  rec->addField("bf");
  rec->addField("vb");
  rec->addField("i");
  rec->addField("vi");
  rec->addField("u");
  rec->addField("f");
  rec->addField("vf");
  rec->addField("d");
  rec->addField("vd");
  rec->addField("s");
  rec->addField("es");
  rec->addField("h0");
  rec->addField("h1");
  rec->addField("h2");
}

//================================================================
void processTestFields(DbRecord* rec, PixDb::DbAccessType mode, TestData& d) {
  PixDbInterface *db = rec->getDb();
  dbFieldIterator fi;
  
  fi = rec->findField("bt");
  db->DbProcess(fi, mode, d.bt);
  
  fi = rec->findField("bf");
  db->DbProcess(fi, mode, d.bf);
  
  fi = rec->findField("vb");
  db->DbProcess(fi, mode, d.vb);
  
  fi = rec->findField("i");
  db->DbProcess(fi, mode, d.i);

  fi = rec->findField("vi");
  db->DbProcess(fi, mode, d.vi);
  
  fi = rec->findField("u");
  db->DbProcess(fi, mode, d.u);
  
  fi = rec->findField("f");
  db->DbProcess(fi, mode, d.f);
  
  fi = rec->findField("vf");
  db->DbProcess(fi, mode, d.vf);
  
  fi = rec->findField("d");
  db->DbProcess(fi, mode, d.d);
  
  fi = rec->findField("vd");
  db->DbProcess(fi, mode, d.vd);
  
  fi = rec->findField("s");
  db->DbProcess(fi, mode, d.s);
  
  fi = rec->findField("es");
  db->DbProcess(fi, mode, d.es);
  
  fi = rec->findField("h0");
  db->DbProcess(fi, mode, d.h0);
  
  fi = rec->findField("h1");
  db->DbProcess(fi, mode, d.h1);
  
  fi = rec->findField("h2");
  db->DbProcess(fi, mode, d.h2);
}

//================================================================
class TestPixDb_Config {
  string m_dbConnectionString;
  string m_outfile_prefix;

  unsigned m_numRandomRecords;
  unsigned m_numRandomLinks;

  // int instead of bool for getopt_long() compatibility
  int m_manualCommitMode;
  int m_doFields; 
  int m_doRandomTree; 
  int m_debug;
public:
  TestPixDb_Config(int argc, char *const argv[]);
  const string& dbConnectionString() const { return m_dbConnectionString; }
  const string& outfile_prefix() const { return m_outfile_prefix; }
  std::string usage() const;
  
  unsigned numRandomRecords() const { return m_numRandomRecords; }
  unsigned numRandomLinks() const { return m_numRandomLinks; }

  AutoCommit autoCommitMode() const { return AutoCommit(!m_manualCommitMode); }

  //----------------
  // Switches for different groups of tests

  bool doTestFields() const { return m_doFields; }
  bool doRandomTree() const { return m_doRandomTree; }

  // 
  bool debug() const { return m_debug; }
};

std::string  TestPixDb_Config::usage() const {
  std::ostringstream os;
  os<<endl<<endl<<"Usage: TestPixDbCoralDB [options] dbconnection  outfile_prefix"<<endl
    <<"outfile_prefix string will be used to construct file names for output files"<<endl<<endl

    <<"Options:"<<endl
    <<"\t--autocommit \t\tUse autocommit mode instead of default manual commit mode."<<endl
    <<"\t--no-fields \t\tDon't attempt to test DbFields."<<endl
    <<"\t--no-random-tree \tDon't create a `large` random tree. Also disables timing test of bool fields."<<endl
    <<"\t--num-random-records \tThe number of nodes to in the random tree, also the number of bool fields for the timing test."<<endl
    <<"\t--num-random-links \tThe number of symlinks to put in the random tree. Independend of the number of tree nodes."<<endl<<endl
    
    <<"Example:\n\t rm test.db; ./TestPixDbCoralDB sqlite_file:test.db coraldbTest"<<endl<<endl
    ;

  return os.str();
}
  
TestPixDb_Config::TestPixDb_Config(int argc, char *const argv[]) 
  : m_numRandomRecords(100),
    m_numRandomLinks(100),
    m_manualCommitMode(1),
    m_doFields(1), 
    m_doRandomTree(1),
    m_debug(0)
{
  // Process options
  while(1) {
    int option_index = 0;
    enum {TRUE = 1, OPT_NUMRECORD=900, OPT_NUMLINKS};
    static struct option long_options[] = {
      {"debug", 0, &m_debug, TRUE},
      {"autocommit", 0, &m_manualCommitMode, 0},
      {"no-fields", 0, &m_doFields, 0},
      {"no-random-tree", 0, &m_doRandomTree, 0},
      {"num-random-records", 1, 0, OPT_NUMRECORD},
      {"num-random-links", 1, 0, OPT_NUMLINKS},
      {0, 0, 0, 0}
    };
    
    int status = getopt_long(argc, argv, "+"/*No short opts, don't permute argv*/, long_options, &option_index);
    if(status == -1) {
      break;
    }
    switch(status) {
    case 0: case TRUE: // long option
      // cerr<<"Got option: "<<long_options[option_index].name;
      // if(optarg) {
      // 	cerr<<" with arg "<< optarg;
      // }
      // cerr<<endl;
      break;

    case OPT_NUMRECORD: {
      istringstream is(optarg);
      if(! (is>>m_numRandomRecords)) {
	cerr<<"Argument to --num-random-records is not a valid unsigned int: "<<optarg<<endl;
	throw runtime_error(usage());
      }
      break;
    }

    case OPT_NUMLINKS: { 
      istringstream is(optarg);
      if(! (is>>m_numRandomLinks)) {
	cerr<<"Argument to --num-random-links is not a valid unsigned int: "<<optarg<<endl;
	throw runtime_error(usage());
      }
      break;
    }

    case ':': // missing arg
    case '?': // unrecognized or ambiguous option
      throw runtime_error(usage());
      break;
    default:
      throw runtime_error("getopt_long() returned unrecognized code");
    }
  }

  if(argc - optind !=2) {
    cerr<<"Wrong number of non-options args"<<endl;
    throw runtime_error(usage());
  }
  
  m_dbConnectionString = argv[optind];
  m_outfile_prefix = argv[optind+1];

  //test: cerr<<"TestPixDb_Config(): connection="<<m_dbConnectionString<<", prefix="<<m_outfile_prefix
  //test:     <<endl<<"debug()="<<debug()
  //test:     <<endl<<"autoCommitMode()="<<autoCommitMode()
  //test:     <<endl<<"m_doFields="<<m_doFields
  //test:     <<endl<<"m_doRandomTree="<<m_doRandomTree
  //test:     <<endl<<"m_numRandomRecords="<<m_numRandomRecords
  //test:     <<endl<<"m_numRandomLinks="<<m_numRandomLinks
  //test:     <<endl;
  //test: 
  //test: exit(0);
}

std::ostream& operator<<(std::ostream& os, const TestPixDb_Config& opt) {
  return os<<"config: dbConnectionString="<<opt.dbConnectionString()<<endl
	   <<"config: outfile_prefix="<<opt.outfile_prefix()<<endl    
	   <<"config: numRandomRecords="<<opt.numRandomRecords()<<endl    
	   <<"config: numRandomLinks="<<opt.numRandomLinks()<<endl    
	   <<"config: autoCommitMode="<<opt.autoCommitMode()<<endl    
	   <<"config: doTestFields="<<opt.doTestFields()<<endl    
	   <<"config: doRandomTree="<<opt.doRandomTree()<<endl    
    ;
}

//================================================================
int main(int argc, char *const argv[]) {
  using namespace std;
  using namespace PixLib;
  int exitcode = 1;

  try {
    srand(1);
    cerr<<"After srand(1): next random number is "<<rand()<<endl;
    std::vector<TestTimer> timers;

    TestPixDb_Config opt(argc, argv);
    PixDbInterfaceFactory dbFactory(opt.dbConnectionString(), opt.autoCommitMode());
    
    TestData dataSet1, dataSet2;
    fillData(&dataSet1);
    fillData(&dataSet2);

    ostringstream os_orig1, os_orig2;
    os_orig1<<dataSet1;
    os_orig2<<dataSet2;

    //----------------------------------------------------------------
    string decname_b1L1, decname_b1L2, decname_b1L3, decname_b2L1, decname_b2L2, decname_b2L3;
    string decname_b1L1_field, decname_b1L2_field, decname_b1L3_field;
    if(1) { // The scope for one instance of DB
      cerr<<"Creating a new database"<<endl;
      timers.push_back(TestTimer("DB Creation"));
      PixDbInterface *db = dbFactory.openDB_recreate(PixDbCompoundTag("IDTAG", "CTAG", "DTAG", "ATAG"), "ROOTRECORD" );
      DbRecord *root = db->readRootRecord();
      timers.rbegin()->stop();

      timers.push_back(TestTimer("Transaction start"));
      db->transactionStart();
      timers.rbegin()->stop();
      
      cerr<<"Testing addRecord()"<<endl;

      DbRecord* b1L1 = root->addRecord("basicTest", "b1L1");
      decname_b1L1 = b1L1->getDecName();
      DbRecord* b1L2 = b1L1->addRecord("basicTest", "b1L2");
      decname_b1L2 = b1L2->getDecName();
      DbRecord* b1L3 = b1L2->addRecord("basicTest", "b1L3");
      decname_b1L3 = b1L3->getDecName();

      DbRecord* b2L1 = root->addRecord("basicTest", "b2L1");
      decname_b2L1 = b2L1->getDecName();
      DbRecord* b2L2 = b2L1->addRecord("basicTest", "b2L2");
      decname_b2L2 = b2L2->getDecName();
      DbRecord* b2L3 = b2L2->addRecord("basicTest", "b2L3");
      decname_b2L3 = b2L3->getDecName();

      cerr<<"Testing linkRecord()"<<endl;
      dbRecordIterator linkA = b1L1->linkRecord(b2L2, "linkA");
      dbRecordIterator linkB = b1L2->linkRecord(b2L2, "linkB");
      dbRecordIterator linkC = b1L3->linkRecord(b2L2, "linkC");

      dbRecordIterator linkU1 = b1L1->linkRecord(b2L2, "linkU1", "linkU1back");

      if(opt.doTestFields()) {
	cerr<<"Testing addField()"<<endl;
	createTestFields(b1L1);
	processTestFields(b1L1, DBCOMMIT, dataSet1);
	assert(b1L1->fieldBegin() != b1L1->fieldEnd());

	decname_b1L1_field = (*b1L1->fieldBegin())->getDecName();

	cerr<<"Testing eraseField() on freshly committed fields. Erasing fields:"<<endl;
	for(dbFieldIterator fi=b1L1->fieldBegin(); fi != b1L1->fieldEnd();) {
	  cerr<<(*fi)->getName()<<" ";
	  b1L1->eraseField(fi++);
	}
	cerr<<endl;
	assert(b1L1->fieldBegin() == b1L1->fieldEnd());
	
	cerr<<"Adding more fields"<<endl;
	createTestFields(b1L2);
	processTestFields(b1L2, DBCOMMIT, dataSet1);
	createTestFields(b1L3);
	processTestFields(b1L3, DBCOMMIT, dataSet1);
	
	decname_b1L2_field = (*b1L2->fieldBegin())->getDecName();
	decname_b1L3_field = (*b1L3->fieldBegin())->getDecName();
      
	cerr<<"Testing that DbFindFieldByName() gives expected results"<<endl;
	assert(!db->DbFindFieldByName(decname_b1L1_field));
	assert(db->DbFindFieldByName(decname_b1L2_field));
	assert(db->DbFindFieldByName(decname_b1L3_field));
	cerr<<"PASSED"<<endl;
      }

      //----------------------------------------------------------------
      { // Test tag copying
	db->copyAllForObjectDictionaryTag("BACKUPTAG");
      }
      
      timers.push_back(TestTimer("Transaction commit"));
      db->transactionCommit();
      timers.rbegin()->stop();

      cerr<<"Closing DB"<<endl;
      delete db;
    }

    //----------------------------------------------------------------
    if(opt.doTestFields()) {
      // continue with basic tests of DbFields
      timers.push_back(TestTimer("DB re-opening"));
      std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
      db->transactionStart();
      timers.rbegin()->stop();
      
      cerr<<"Locating records after DB re-opening"<<endl;
      DbRecord *b1L2 = db->DbFindRecordByName(decname_b1L2);
      assert(b1L2);

      cerr<<"Deleting fields after DB re-opening. Erasing fields:"<<endl;
      for(dbFieldIterator fi=b1L2->fieldBegin(); fi != b1L2->fieldEnd();) {
	cerr<<(*fi)->getName()<<" ";
	b1L2->eraseField(fi++);
      }
      cerr<<endl;
      assert(b1L2->fieldBegin() == b1L2->fieldEnd());

      cerr<<"Testing that DbFindFieldByName() gives expected results"<<endl;
      assert(!db->DbFindFieldByName(decname_b1L1_field));
      assert(!db->DbFindFieldByName(decname_b1L2_field));
      assert(db->DbFindFieldByName(decname_b1L3_field));

      timers.push_back(TestTimer("Transaction commit"));
      db->transactionCommit();
      timers.rbegin()->stop();

      cerr<<"PASSED"<<endl;
    }

    //----------------------------------------------------------------
    if(opt.doTestFields()) {
      // continue with basic tests
      timers.push_back(TestTimer("DB re-opening"));
      std::unique_ptr<PixDbInterface> db(dbFactory.openDB_readonly());
      timers.rbegin()->stop();

      cerr<<"Locating records after DB re-opening"<<endl;
      DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
      assert(b1L1);
      DbRecord *b1L2 = db->DbFindRecordByName(decname_b1L2);
      assert(b1L2);
      DbRecord *b1L3 = db->DbFindRecordByName(decname_b1L3);
      assert(b1L3);

      // make sure previously deleted fields are gone
      if(opt.doTestFields()) {
	cerr<<"Checking that field presence/abscence matches the expected state."<<endl;
	assert(b1L1->fieldBegin() == b1L1->fieldEnd());
	assert(b1L2->fieldBegin() == b1L2->fieldEnd());
	assert(b1L3->fieldBegin() != b1L3->fieldEnd());
      }
    }
    //----------------------------------------------------------------
    if(1) {
      // continue with basic tests
      timers.push_back(TestTimer("DB re-opening"));
      std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
      timers.rbegin()->stop();

      DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
      assert(b1L1);
      DbRecord *b1L2 = db->DbFindRecordByName(decname_b1L2);
      assert(b1L2);
      DbRecord *b1L3 = db->DbFindRecordByName(decname_b1L3);
      assert(b1L3);

      DbRecord *b2L3 = db->DbFindRecordByName(decname_b2L3);
      assert(b2L3);

      cerr<<"Testing symlink removal"<<endl;
      dbRecordIterator it_A = b1L1->findRecord("linkA");
      assert(it_A != b1L1->recordEnd());
      assert(it_A.isLink());
      DbRecord* b2L2_before = *it_A;

      db->transactionStart();
      b1L1->eraseRecord(it_A);
      
      // The symlink must be gone
      assert(b1L1->findRecord("linkA") == b1L1->recordEnd());

      // But the pointed-to record should still be there
      DbRecord *b2L2_after = db->DbFindRecordByName(decname_b2L2);
      assert(b2L2_before == b2L2_after);
      // and should be dereferencable
      assert(b2L2_after->getDecName() == decname_b2L2);


      //----------------
      cerr<<"Testing symlink updating"<<endl;
      dbRecordIterator it_U1_before = b1L1->findRecord("linkU1");
      assert(it_U1_before != b1L1->recordEnd());
      DbRecord *linkU1_dstObj_before = *it_U1_before;
      assert(linkU1_dstObj_before == b2L2_before);

      b1L1->updateLink(b2L3, "linkU1", "linkU1updated_back");

      dbRecordIterator it_U1_after = b1L1->findRecord("linkU1");
      assert(it_U1_after != b1L1->recordEnd());
      DbRecord *linkU1_dstObj_after = *it_U1_after;
      assert(linkU1_dstObj_after == b2L3);


      //----------------

      // cerr<<"Getting more verbose"<<endl;
      // dynamic_cast<PixDbCoralDB*>(db.get())->setMsgLevel(seal::Msg::Debug);

      cerr<<"Testing hard link removal"<<endl;
      dbRecordIterator it_L2 = b1L1->findRecord("b1L2");
      assert(it_L2 != b1L1->recordEnd());
      assert(!it_L2.isLink());
      
      cerr<<"Erasing hard link "<<it_L2<<"\n"<< (**it_L2)<<endl;

      b1L1->eraseRecord(it_L2);
      
      // The subtree must be gone
      assert(b1L1->findRecord("b1L2") == b1L1->recordEnd());
      assert(! db->DbFindRecordByName(decname_b1L2) );
      assert(! db->DbFindRecordByName(decname_b1L3) );

      if(opt.doTestFields()) {
	// With its subfields
	assert(!db->DbFindFieldByName(decname_b1L2_field));
	assert(!db->DbFindFieldByName(decname_b1L3_field));
      }

      timers.push_back(TestTimer("Transaction commit"));
      db->transactionCommit();
      timers.rbegin()->stop();

      cerr<<"PASSED"<<endl;
    }

    //----------------------------------------------------------------
    // Check that erased links and records are gone, 
    if(1) {
      cerr<<"Checking that the state of records and links is as expected after DB re-opening. "<<endl;
      timers.push_back(TestTimer("DB re-opening"));
      std::unique_ptr<PixDbInterface> db(dbFactory.openDB_readonly());
      timers.rbegin()->stop();

      DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
      assert(b1L1);
      DbRecord *b1L2 = db->DbFindRecordByName(decname_b1L2);
      assert(!b1L2);
      DbRecord *b1L3 = db->DbFindRecordByName(decname_b1L3);
      assert(!b1L3);
      
      DbRecord *b2L3 = db->DbFindRecordByName(decname_b2L3);
      assert(b2L3);
      
      dbRecordIterator it_L2 = b1L1->findRecord("b1L2");
      assert(it_L2 == b1L1->recordEnd());

      dbRecordIterator it_linkA = b1L1->findRecord("linkA");
      assert(it_linkA == b1L1->recordEnd());

      // a record pointed to by an erased softlink should still be around
      DbRecord *b2L2 = db->DbFindRecordByName(decname_b2L2);
      assert(b2L2);
      assert(b2L2->getDecName() == decname_b2L2);

      //----------------
      // Test that the symlink we updated above *is* updated after DB reopening.
      dbRecordIterator it_U1_after = b1L1->findRecord("linkU1");
      assert(it_U1_after != b1L1->recordEnd());
      DbRecord *linkU1_dstObj_after = *it_U1_after;
      assert(linkU1_dstObj_after == b2L3);
    }
    
    //----------------------------------------------------------------
    // Check that there is no data corruption in DbFields
    // cached reads:
    if(opt.doTestFields()) {

      string testname("Field storage test");

      if(1) { // DB instance for the initial commit
	timers.push_back(TestTimer("DB re-opening"));
	std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
	timers.rbegin()->stop();

	db->transactionStart();

	DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
	assert(b1L1);
	
	cerr<<testname<<": creating new fields"<<endl;
	createTestFields(b1L1);
	
	cerr<<testname<<": committing data"<<endl;
	processTestFields(b1L1, DBCOMMIT, dataSet1);
	
	cerr<<testname<<": reading fields back (cached)"<<endl;
	TestData fromMem;
	processTestFields(b1L1, DBREAD, fromMem);
	
	ostringstream os_fromMem;
	os_fromMem<<fromMem;
	
	cerr<<testname<<": comparing cached read back to the original: ";
	if(os_fromMem.str() == os_orig1.str()) {
	  cerr<<"PASSED"<<endl;
	}
	else {
	  string filename_orig1(opt.outfile_prefix() + "_dataSet1_orig.txt");
	  ofstream of_orig1(filename_orig1.c_str());
	  of_orig1<<dataSet1;
	  
	  string filename_fromMem(opt.outfile_prefix() + "_dataSet1_cachedRead.txt");
	  ofstream of_fromMem(filename_fromMem.c_str());
	  of_fromMem<<fromMem;
	  
	  cerr<<"*** FAILED.  Compare files "<<filename_orig1<<" and "<<filename_fromMem <<endl;
	}
	
	timers.push_back(TestTimer("Transaction commit"));
	db->transactionCommit();
	timers.rbegin()->stop();
      }

      //----------------
      // real DB reads:
      if(1) {
	timers.push_back(TestTimer("DB re-opening"));
	std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
	timers.rbegin()->stop();
	
	db->transactionStart();

	DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
	assert(b1L1);
	
	cerr<<testname<<": reading fields back after DB re-opening"<<endl;
	TestData fromDB;
	
	processTestFields(b1L1, DBREAD, fromDB);

	ostringstream os_fromDB;
	os_fromDB<<fromDB;
	
	cerr<<testname<<": comparing read back from DB to the original: ";
	if(os_fromDB.str() == os_orig1.str()) {
	  cerr<<"PASSED"<<endl;
	}
	else {
	  string filename_orig1(opt.outfile_prefix() + "_dataSet1_orig.txt");
	  ofstream of_orig1(filename_orig1.c_str());
	  of_orig1<<dataSet1;
	
	  string filename_fromDB(opt.outfile_prefix() + "_dataSet1_DbRead.txt");
	  ofstream of_fromDB(filename_fromDB.c_str());
	  of_fromDB<<fromDB;

	  cerr<<"*** FAILED.  Compare files "<<filename_orig1<<" and "<<filename_fromDB <<endl;
	}
	//----------------
	
	cerr<<testname<<": updating stored values"<<endl;
	processTestFields(b1L1, DBCOMMIT, dataSet2);
	cerr<<testname<<": reading fields back (cached) after the update"<<endl;
	TestData fromMem;
	processTestFields(b1L1, DBREAD, fromMem);
	
	ostringstream os_fromMem;
	os_fromMem<<fromMem;
      
	cerr<<testname<<": comparing cached read back after update to the original: ";
	if(os_fromMem.str() == os_orig2.str()) {
	  cerr<<"PASSED"<<endl;
	}
	else {
	  string filename_orig2(opt.outfile_prefix() + "_dataSet2_orig.txt");
	  ofstream of_orig2(filename_orig2.c_str());
	  of_orig2<<dataSet2;
	  
	  string filename_fromMem(opt.outfile_prefix() + "_dataSet2_cachedRead.txt");
	  ofstream of_fromMem(filename_fromMem.c_str());
	  of_fromMem<<fromMem;
	  
	  cerr<<"*** FAILED.  Compare files "<<filename_orig2<<" and "<<filename_fromMem <<endl;
	}
	
	timers.push_back(TestTimer("Transaction commit"));
	db->transactionCommit();
	timers.rbegin()->stop();
      }
      
      //----------------
      // real DB reads after an update
      if(1) {
	timers.push_back(TestTimer("DB re-opening"));
	std::unique_ptr<PixDbInterface> db(dbFactory.openDB_readonly());
	timers.rbegin()->stop();
	
	DbRecord *b1L1 = db->DbFindRecordByName(decname_b1L1);
	assert(b1L1);
	
	cerr<<testname<<": reading updated fields after DB re-opening"<<endl;
	TestData fromDB;

	processTestFields(b1L1, DBREAD, fromDB);
	
	ostringstream os_fromDB;
	os_fromDB<<fromDB;
	
	cerr<<testname<<": comparing read back to the original: ";
	if(os_fromDB.str() == os_orig2.str()) {
	  cerr<<"PASSED"<<endl;
	}
	else {
	  string filename_orig2(opt.outfile_prefix() + "_dataSet2_orig.txt");
	  ofstream of_orig2(filename_orig2.c_str());
	  of_orig2<<dataSet2;
	  
	  string filename_fromDB(opt.outfile_prefix() + "_dataSet2_DbRead.txt");
	  ofstream of_fromDB(filename_fromDB.c_str());
	  of_fromDB<<fromDB;

	  cerr<<"*** FAILED. Compare files "<<filename_orig2<<" and "<<filename_fromDB <<endl;
	}
      }
    }

    //----------------------------------------------------------------
    if(opt.doRandomTree()) {
      // Test DbRecord operations
      timers.push_back(TestTimer("DB re-opening"));
      std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
      timers.rbegin()->stop();

      db->transactionStart();

      cerr<<"Creating random tree"<<endl;
      RandomTree tree(db.get());
    
      cerr<<"Adding random subrecords"<<endl;
      timers.push_back(TestTimer("Adding random subrecords"));
      for(unsigned i=0; i<opt.numRandomRecords(); i++) {
	tree.addRandomNode();
      }
      timers.rbegin()->stop();

      cerr<<"Adding random links"<<endl;
      timers.push_back(TestTimer("Adding random links"));
      for(unsigned i=0; i<opt.numRandomLinks(); i++) {
	tree.addRandomLink();
      }
      timers.rbegin()->stop();

      cerr<<"Updating random links"<<endl;
      timers.push_back(TestTimer("Updating random links"));
      for(unsigned i=0; i<opt.numRandomLinks(); i++) {
	tree.updateRandomLink();
      }
      timers.rbegin()->stop();

      cerr<<"Deleting random links"<<endl;
      timers.push_back(TestTimer("Deleting random links (1/10 of soft links)"));
      for(unsigned i=0; i<opt.numRandomLinks()/10; i++) {
	tree.deleteRandomLink();
      }
      timers.rbegin()->stop();

      timers.push_back(TestTimer("Transaction commit (random tree)"));
      db->transactionCommit();
      timers.rbegin()->stop();
    }

    // ----------------------------------------------------------------
    if(opt.doRandomTree()) { // the scope for re-opened DB

      if(1) {
	cerr<<"Re-opening the database"<<endl;
	timers.push_back(TestTimer("DB re-opening"));
	std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
	timers.rbegin()->stop();
	
	db->transactionStart();

	cerr<<"Traversing DbRecord tree"<<endl;
	timers.push_back(TestTimer("Traversing DbRecord tree"));
	RandomTree tree(db.get());
	timers.rbegin()->stop();
	
	cerr<<"Looking up DbRecords by DecName"<<endl;
	timers.push_back(TestTimer("Lookup by DecName"));
	for(RandomTree::DecNameContainer::const_iterator i= tree.decNamesBegin(); i!= tree.decNamesEnd(); i++) {
	  DbRecord *rec = db->DbFindRecordByName(*i);
	  assert(rec);
	}
	timers.rbegin()->stop();
	
	if(opt.doTestFields()) {
	  cerr<<"Creating boolean fields"<<endl;
	  timers.push_back(TestTimer("Creating boolean fields"));
	  for(RandomTree::DecNameContainer::const_iterator i= tree.decNamesBegin(); i!= tree.decNamesEnd(); i++) {
	    DbRecord *rec = db->DbFindRecordByName(*i);
	    dbFieldIterator fi = rec->addField("enable");
	    bool b(rand()%2);
	    db->DbProcess(fi, DBCOMMIT, b);
	  }
	  timers.rbegin()->stop();
	  
	  cerr<<"Toggling enable fields (mem)"<<endl;
	  timers.push_back(TestTimer("Toggling boolean fields (mem)"));
	  for(RandomTree::DecNameContainer::const_iterator i= tree.decNamesBegin(); i!= tree.decNamesEnd(); i++) {
	    DbRecord *rec = db->DbFindRecordByName(*i);
	    dbFieldIterator fi = rec->findField("enable");
	    bool b(rand()%2);
	    db->DbProcess(fi, DBCOMMIT, b);
	  }
	  timers.rbegin()->stop();
	}

	timers.push_back(TestTimer("Transaction commit (field toggle)"));
	db->transactionCommit();
	timers.rbegin()->stop();
      }

      // ----------------------------------------------------------------
      if(opt.doTestFields()) { // the scope for re-opened DB
	cerr<<"Re-opening the database"<<endl;
        timers.push_back(TestTimer("DB re-opening"));
	std::unique_ptr<PixDbInterface> db(dbFactory.openDB_update());
        timers.rbegin()->stop();

	db->transactionStart();

	//----------------------------------------------------------------
	// Test switching to a new tag
	cerr<<"copying tags and switching to the new ones"<<endl;
	timers.push_back(TestTimer("copyConnectivityTag"));
	db->copyConnectivityTagAndSwitch("NEWCTAG");
	timers.rbegin()->stop();
	
	timers.push_back(TestTimer("copyDataTag"));
	db->copyDataTagAndSwitch("NEWDTAG");
	timers.rbegin()->stop();
	
	timers.push_back(TestTimer("copyAliasTag"));
	db->copyAliasTagAndSwitch("NEWATAG");
        timers.rbegin()->stop();

	//----------------------------------------------------------------
	RandomTree tree(db.get());
      
	cerr<<"Toggling boolean fields (DB)"<<endl;
	timers.push_back(TestTimer("Toggling boolean fields (DB)"));
	for(RandomTree::DecNameContainer::const_iterator i= tree.decNamesBegin(); i!= tree.decNamesEnd(); i++) {
	  DbRecord *rec = db->DbFindRecordByName(*i);
	  dbFieldIterator fi = rec->findField("enable");
	  bool b(rand()%2);
	  db->DbProcess(fi, DBCOMMIT, b);
	}
	timers.rbegin()->stop();
      
	cerr<<"Toggling boolean fields (mem2)"<<endl;
	timers.push_back(TestTimer("Toggling boolean fields (mem2)"));
	for(RandomTree::DecNameContainer::const_iterator i= tree.decNamesBegin(); i!= tree.decNamesEnd(); i++) {
	  DbRecord *rec = db->DbFindRecordByName(*i);
	  dbFieldIterator fi = rec->findField("enable");
	  bool b(rand()%2);
	  //cerr<<b<<" ";
	  db->DbProcess(fi, DBCOMMIT, b);
	}
	timers.rbegin()->stop();
	cerr<<endl;

	timers.push_back(TestTimer("Transaction commit"));
	db->transactionCommit();
	timers.rbegin()->stop();
      }

    } // if(opt.doRandomTree())

    //----------------------------------------------------------------
    // 
    if(1) {
      string filename(opt.outfile_prefix() + "_timingSummary.txt");
      ofstream os(filename.c_str());
      os<<opt<<endl;

      for(std::vector<TestTimer>::const_iterator i=timers.begin(); i!=timers.end(); i++) {
	os<<*i<<endl;
      }
      cerr<<"Timing statistics saved to file "<<filename<<endl;
    }

    // Everything is done, did not fall to an exception handler below
    exitcode = 0;
  }

  //----------------------------------------------------------------
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
  }
  catch(PixLib::PixDBException& e) {
    e.what(std::cerr);
    std::cerr<<endl;
  }
  catch(...) {
    std::cerr<<"Got a non-std exception"<<std::endl;
  }

  cerr<<"At the end: next random number is "<<rand()<<endl;
  return exitcode;
}
