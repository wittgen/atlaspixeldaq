#!/bin/bash
echo "Following RODs are used on idividual Partitions"
ipc_ls --partitions > temp.txt
LN=$(echo "$(cat temp.txt | wc -l)-2" | bc) 
for Partition in $(tail -n$LN temp.txt); do
   echo "Used on Partition with name" $Partition
##   ./UsedActions $Partition | grep SINGLE
   ~/daq/Applications/PixLib/Examples/UsedActions $Partition | grep SINGLE | awk 'BEGIN{FS="/"} {print $2}'
done

