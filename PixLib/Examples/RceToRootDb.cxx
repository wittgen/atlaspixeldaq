#include "iblModuleConfigStructures.h"
#include "moduleConfigStructures.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixModule/PixModule.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "rcecalib/server/FEI4AConfigFile.hh"
#include "rcecalib/server/FEI4BConfigFile.hh"
#include <iostream>
#include "PixFe/PixFeI4A.h"
#include "PixFe/PixFeI4.h"
#include <exception>
#include <stdlib.h>
#include <sstream>

int usage(const char** argv) {
    printf("Usage: %s rceConfA rootDb\n", argv[0]);
    printf("Usage: %s rceConfA rceConfB rootDb\n", argv[0]);
    return 1;
}

int main(int argc, const char* argv[]){
  int flavour;
  ipc::PixelFEI4AConfig  configa[2];
  ipc::PixelFEI4BConfig  configb[2];

  try{
    // argument 1 ist the file name
    if(argc<3) return usage(argv);
    string rceName[2];
    rceName[0] = argv[1];
    rceName[1] = "";
    string rdbName (argv[2]);
    int nFE=1;
    if(argc==4) {
      rceName[1] = rdbName;
      rdbName = argv[3];
      nFE=2;
    }
    // create Db file and fill with module and FE records
    FEI4AConfigFile confa;
    FEI4BConfigFile confb;
    for(int iFE=0; iFE<nFE; iFE++){
      try {
	confa.readModuleConfig(&configa[iFE],rceName[iFE]);
	flavour=1;
      } catch (...){
	try {
	  confb.readModuleConfig(&configb[iFE],rceName[iFE]);
	  flavour=2;
	} catch (...){
	  printf("failed to read RCE config no. %d\n", iFE);
	  exit(-1);
	}
      }
    }
    std::cout << "flavour is "<< flavour << std::endl; 
    PixDbInterface* myDb = new RootDb(rdbName, "NEW");
    DbRecord* myRoot = myDb->readRootRecord();
    DbRecord* modRec = myDb->makeRecord("PixModule", "Def");
    dbRecordIterator findRec = myRoot->pushRecord(modRec);
    modRec = *(myDb->DbProcess(findRec,PixDb::DBREAD));
    for(int iFE=0; iFE<nFE; iFE++){
      std::stringstream a;
      a << iFE;
      DbRecord* feRec = myDb->makeRecord("PixFe", "PixFe_"+a.str());
      findRec = modRec->pushRecord(feRec);
      feRec = *(myDb->DbProcess(findRec,PixDb::DBREAD));
      // add class description to make sure an FE-I4 is created
      std::string classname="ClassInfo_ClassName";
      DbField* clField = myDb->makeField(classname);
      feRec->pushField(clField);      
      dbFieldIterator findField = feRec->findField(clField->getName());
      myDb->DbProcess(findField, PixDb::DBREAD);
      string clcont(flavour==1?"PixFeI4A":"PixFeI4B");
      myDb->DbProcess(findField,PixDb::DBCOMMIT,clcont);
      DbRecord* regRec = myDb->makeRecord("GlobalRegister", "GlobalRegister_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
      regRec = myDb->makeRecord("PixelRegister", "PixelRegister_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
      regRec = myDb->makeRecord("Trim", "Trim_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
    }



    // create a PixModule from Db and save FE default setting back to it
    PixModule *pm = new PixModule(modRec, 0, flavour==1?"Def":"Def");
    // set correct MCC, FE flavour
    bool enable[80][336];
    bool largeCap[80][336];
    bool smallCap[80][336];
    bool hitbus[80][336];
    unsigned short  fdac[80][336];
    unsigned short tdac[80][336]; 
    for(int iFE=0; iFE<nFE; iFE++){
      PixFe *fe=pm->pixFE(iFE);
      Config &feconfig=pm->pixFE(iFE)->config();
      if(flavour==1) {
	for(int x=0;x<80;x++) for(int y=0;y<336;y++) {     
	  enable[x][y]=(configa[iFE].FEMasks[x][y]&1)==1;
	  largeCap[x][y]=(configa[iFE].FEMasks[x][y]&2)==2 ;
	  smallCap[x][y]=(configa[iFE].FEMasks[x][y]&4)==4 ; 
	  hitbus[x][y]=(configa[iFE].FEMasks[x][y]&8)==8 ; 
	  fdac[x][y]=configa[iFE].FETrims.dacFeedbackTrim[x][y];
	  tdac[x][y]=configa[iFE].FETrims.dacThresholdTrim[x][y];
	}
	
	((ConfList&)pm->config()["general"]["MCC_Flavour"]).setValue(PixModule::PM_NO_MCC);
	((ConfList&)pm->config()["general"]["FE_Flavour"]).setValue(flavour==1?PixModule::PM_FE_I4A:PixModule::PM_FE_I4B);

	((ConfInt&)feconfig["Misc"]["Address"]).setValue(configa[iFE].FECommand.address);
	((ConfFloat&)feconfig["Misc"]["CInjLo"]).m_value=(float)configa[iFE].FECalib.cinjLo;
	((ConfFloat&)feconfig["Misc"]["CInjMed"]).m_value=(float)configa[iFE].FECalib.cinjHi;
	((ConfFloat&)feconfig["Misc"]["CInjHi"]).m_value=(float)configa[iFE].FECalib.cinjHi+(float)configa[iFE].FECalib.cinjLo-0.1;
	((ConfFloat&)feconfig["Misc"]["VcalGradient0"]).m_value=(float)configa[iFE].FECalib.vcalCoeff[0] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient1"]).m_value=(float)configa[iFE].FECalib.vcalCoeff[1] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient2"]).m_value=(float)configa[iFE].FECalib.vcalCoeff[2] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient3"]).m_value=(float)configa[iFE].FECalib.vcalCoeff[3] * 1000; // Converting V to mV -- LJ -- Karolos 
	printf("%d %d\n",((ConfMask<bool>&)feconfig["PixelRegister"]["CAP0"]).ncol(),((ConfMask<bool>&)feconfig["PixelRegister"]["CAP0"]).nrow());
	
	for(int x=0;x<80;x++) for(int y=0;y<336;y++) {    
	  //	printf("%u %u %u\n",x,y,fdac[x][y]);
	  fe->readPixRegister("CAP0").set(x,y,smallCap[x][y]);
	  fe->readPixRegister("CAP1").set(x,y,largeCap[x][y]);
	  fe->readPixRegister("ENABLE").set(x,y,enable[x][y]);
	  fe->readPixRegister("ILEAK").set(x,y,hitbus[x][y]);
	  
	  fe->readTrim("TDAC").set(x,y,tdac[x][y]);
	  fe->readTrim("FDAC").set(x,y,fdac[x][y]);
	  
	}	
	
	
	printf("%d %d\n",fe->readPixRegister("ILEAK").ncol(),fe->readPixRegister("ILEAK").nrow());
      }
      if(flavour==2) {
	for(int x=0;x<80;x++) for(int y=0;y<336;y++) {     
	  enable[x][y]=(configb[iFE].FEMasks[x][y]&1)==1;
	  largeCap[x][y]=(configb[iFE].FEMasks[x][y]&2)==2 ;
	  smallCap[x][y]=(configb[iFE].FEMasks[x][y]&4)==4 ; 
	  hitbus[x][y]=(configb[iFE].FEMasks[x][y]&8)==8 ; 
	  fdac[x][y]=configb[iFE].FETrims.dacFeedbackTrim[x][y];
	  tdac[x][y]=configb[iFE].FETrims.dacThresholdTrim[x][y];
	}
	((ConfList&)pm->config()["general"]["MCC_Flavour"]).setValue(PixModule::PM_NO_MCC);
	((ConfList&)pm->config()["general"]["FE_Flavour"]).setValue(flavour==1?PixModule::PM_FE_I4A:PixModule::PM_FE_I4B);
	//      ((ConfList&)pm->config()["Misc"][""]).setValue();
	((ConfInt&)feconfig["Misc"]["Address"]).setValue(configb[iFE].FECommand.address);
	((ConfFloat&)feconfig["Misc"]["CInjLo"]).m_value=(float)configb[iFE].FECalib.cinjLo;
	((ConfFloat&)feconfig["Misc"]["CInjMed"]).m_value=(float)configb[iFE].FECalib.cinjHi;
	((ConfFloat&)feconfig["Misc"]["CInjHi"]).m_value=(float)configb[iFE].FECalib.cinjHi+(float)configb[iFE].FECalib.cinjLo-0.1;
	((ConfFloat&)feconfig["Misc"]["VcalGradient0"]).m_value=(float)configb[iFE].FECalib.vcalCoeff[0] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient1"]).m_value=(float)configb[iFE].FECalib.vcalCoeff[1] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient2"]).m_value=(float)configb[iFE].FECalib.vcalCoeff[2] * 1000; // Converting V to mV -- LJ -- Karolos
	((ConfFloat&)feconfig["Misc"]["VcalGradient3"]).m_value=(float)configb[iFE].FECalib.vcalCoeff[3] * 1000; // Converting V to mV -- LJ -- Karolos 
	/*   ((ConfList&)feconfig["Misc"]["CInjLo"]).setValue(configb[iFE].FECalib.cinjLo);
	     ((ConfList&)feconfig["Misc"]["CInjHi"]).setValue(configb[iFE].FECalib.cinjHi);
	     ((ConfList&)feconfig["Misc"]["VcalGradient0"]).setValue(configb[iFE].FECalib.vcalCoeff[0]);
	     ((ConfList&)feconfig["Misc"]["VcalGradient1"]).setValue(configb[iFE].FECalib.vcalCoeff[1]);
	     ((ConfList&)feconfig["Misc"]["VcalGradient2"]).setValue(configb[iFE].FECalib.vcalCoeff[2]);
	     ((ConfList&)feconfig["Misc"]["VcalGradient3"]).setValue(configb[iFE].FECalib.vcalCoeff[3]); */
	for(int x=0;x<80;x++) for(int y=0;y<336;y++) {    
	  fe->readPixRegister("CAP0").set(x,y,smallCap[x][y]);
	  fe->readPixRegister("CAP1").set(x,y,largeCap[x][y]);
	  fe->readPixRegister("ENABLE").set(x,y,enable[x][y]);
	  fe->readPixRegister("ILEAK").set(x,y,hitbus[x][y]);	
	  fe->readTrim("TDAC").set(x,y,tdac[x][y]);
	  fe->readTrim("FDAC").set(x,y,fdac[x][y]);
	  
	}
	
      }
      if(flavour==1) {
	ipc::PixelFEI4AGlobal* cfg=&configa[iFE].FEGlobal;
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbn"]).setValue(cfg->Amp2Vbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbp"]).setValue(cfg->Amp2Vbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2VbpFol"]).setValue(cfg->Amp2VbpFol);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbpf"]).setValue(cfg->Amp2Vbpf);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["BonnDac"]).setValue(cfg->BonnDac);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CLK0"]).setValue(cfg->CLK0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CLK1"]).setValue(cfg->CLK1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CMDcnt"]).setValue(cfg->CMDcnt);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CalEn"]).setValue(cfg->CalEn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Chip_SN"]).setValue(cfg->Chip_SN);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Clk2OutCnfg"]).setValue(cfg->Clk2OutCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Colpr_Addr"]).setValue(cfg->Colpr_Addr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Colpr_Mode"]).setValue(cfg->Colpr_Mode);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Conf_AddrEnable"]).setValue(cfg->Conf_AddrEnable);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DIGHITIN_Sel"]).setValue(cfg->DIGHITIN_Sel);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DINJ_Override"]).setValue(cfg->DINJ_Override);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisVbn"]).setValue(cfg->DisVbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisVbn_CPPM"]).setValue(cfg->DisVbn_CPPM);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg0"]).setValue(cfg->DisableColumnCnfg0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg1"]).setValue(cfg->DisableColumnCnfg1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg2"]).setValue(cfg->DisableColumnCnfg2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN160M"]).setValue(cfg->EN160M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN320M"]).setValue(cfg->EN320M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN40M"]).setValue(cfg->EN40M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN80M"]).setValue(cfg->EN80M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN_PLL"]).setValue(cfg->EN_PLL);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EfuseCref"]).setValue(cfg->EfuseCref);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EfuseVref"]).setValue(cfg->EfuseVref);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Efuse_sense"]).setValue(cfg->Efuse_sense);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EmptyRecord"]).setValue(cfg->EmptyRecord);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ErrMask0"]).setValue(cfg->ErrMask0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ErrMask1"]).setValue(cfg->ErrMask1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ExtAnaCalSW"]).setValue(cfg->ExtAnaCalSW);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ExtDigCalSW"]).setValue(cfg->ExtDigCalSW);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["FdacVbn"]).setValue(cfg->FdacVbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["GateHitOr"]).setValue(cfg->GateHitOr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["HITLD_In"]).setValue(cfg->HITLD_In);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["HitDiscCnfg"]).setValue(cfg->HitDiscCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvEn"]).setValue(cfg->LVDSDrvEn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvIref"]).setValue(cfg->LVDSDrvIref);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet06"]).setValue(cfg->LVDSDrvSet06);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet12"]).setValue(cfg->LVDSDrvSet12);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet30"]).setValue(cfg->LVDSDrvSet30);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvVos"]).setValue(cfg->LVDSDrvVos);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Latch_en"]).setValue(cfg->Latch_en);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PllIbias"]).setValue(cfg->PllIbias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PllIcp"]).setValue(cfg->PllIcp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDAC"]).setValue(cfg->PlsrDAC);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDacBias"]).setValue(cfg->PlsrDacBias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDelay"]).setValue(cfg->PlsrDelay);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrIdacRamp"]).setValue(cfg->PlsrIdacRamp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrPwr"]).setValue(cfg->PlsrPwr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrRiseUpTau"]).setValue(cfg->PlsrRiseUpTau);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrVgOPamp"]).setValue(cfg->PlsrVgOPamp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbnFol"]).setValue(cfg->PrmpVbnFol);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbnLcc"]).setValue(cfg->PrmpVbnLcc);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbp"]).setValue(cfg->PrmpVbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpLeft"]).setValue(cfg->PrmpVbpLeft);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpRight"]).setValue(cfg->PrmpVbpRight);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpTop"]).setValue(cfg->PrmpVbpTop);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpf"]).setValue(cfg->PrmpVbpf);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PxStrobes"]).setValue(cfg->PxStrobes);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ReadErrorReq"]).setValue(cfg->ReadErrorReq);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ReadSkipped"]).setValue(cfg->ReadSkipped);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg13Spare"]).setValue(cfg->Reg13Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg17Spare"]).setValue(cfg->Reg17Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg18Spare"]).setValue(cfg->Reg18Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg19Spare"]).setValue(cfg->Reg19Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg21Spare"]).setValue(cfg->Reg21Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg22Spare1"]).setValue(cfg->Reg22Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg22Spare2"]).setValue(cfg->Reg22Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg27Spare"]).setValue(cfg->Reg27Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbn"]).setValue(cfg->Amp2Vbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg28Spare"]).setValue(cfg->Reg28Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg29Spare1"]).setValue(cfg->Reg29Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg29Spare2"]).setValue(cfg->Reg29Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg2Spare"]).setValue(cfg->Reg2Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg31Spare"]).setValue(cfg->Reg31Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["S0"]).setValue(cfg->S0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["S1"]).setValue(cfg->S1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB0"]).setValue(cfg->SELB0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB1"]).setValue(cfg->SELB1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB2"]).setValue(cfg->SELB2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SR_Clock"]).setValue(cfg->SR_Clock);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SR_clr"]).setValue(cfg->SR_clr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["StopModeCnfg"]).setValue(cfg->StopModeCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Stop_Clk"]).setValue(cfg->Stop_Clk);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TdacVbp"]).setValue(cfg->TdacVbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TempSensBias"]).setValue(cfg->TempSensBias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TrigCnt"]).setValue(cfg->TrigCnt);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TrigLat"]).setValue(cfg->TrigLat);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Vthin"]).setValue(cfg->Vthin);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Vthin_AltCoarse"]).setValue(cfg->Vthin_AltCoarse);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Vthin_AltFine"]).setValue(cfg->Vthin_AltFine);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["no8b10b"]).setValue(cfg->no8b10b);      
      }
      if(flavour==2) {
	ipc::PixelFEI4BGlobal* cfg=&configb[iFE].FEGlobal;
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbn"]).setValue(cfg->Amp2Vbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbp"]).setValue(cfg->Amp2Vbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2VbpFol"]).setValue(cfg->Amp2VbpFol);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Amp2Vbpf"]).setValue(cfg->Amp2Vbpf);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["BufVgOpAmp"]).setValue(cfg->BufVgOpAmp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CLK0"]).setValue(cfg->CLK0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CLK1"]).setValue(cfg->CLK1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CMDcnt"]).setValue(cfg->CMDcnt);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["CalEn"]).setValue(cfg->CalEn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Chip_SN"]).setValue(cfg->Chip_SN);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Clk2OutCnfg"]).setValue(cfg->Clk2OutCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Colpr_Addr"]).setValue(cfg->Colpr_Addr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Colpr_Mode"]).setValue(cfg->Colpr_Mode);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Conf_AddrEnable"]).setValue(cfg->Conf_AddrEnable);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DIGHITIN_Sel"]).setValue(cfg->DIGHITIN_Sel);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DINJ_Override"]).setValue(cfg->DINJ_Override);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisVbn"]).setValue(cfg->DisVbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg0"]).setValue(cfg->DisableColumnCnfg0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg1"]).setValue(cfg->DisableColumnCnfg1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["DisableColumnCnfg2"]).setValue(cfg->DisableColumnCnfg2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN160M"]).setValue(cfg->EN160M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN320M"]).setValue(cfg->EN320M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN40M"]).setValue(cfg->EN40M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN80M"]).setValue(cfg->EN80M);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EN_PLL"]).setValue(cfg->EN_PLL);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Efuse_sense"]).setValue(cfg->Efuse_sense);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["EmptyRecord"]).setValue(cfg->EmptyRecord);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ErrMask0"]).setValue(cfg->ErrMask0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ErrMask1"]).setValue(cfg->ErrMask1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Eventlimit"]).setValue(cfg->Eventlimit);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ExtAnaCalSW"]).setValue(cfg->ExtAnaCalSW);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ExtDigCalSW"]).setValue(cfg->ExtDigCalSW);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["FdacVbn"]).setValue(cfg->FdacVbn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["GADCOpAmp"]).setValue(cfg->GADCOpAmp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["GADCSel"]).setValue(cfg->GADCSel);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["GADC_Enable"]).setValue(cfg->GADC_Enable);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["GateHitOr"]).setValue(cfg->GateHitOr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["HITLD_In"]).setValue(cfg->HITLD_In);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["HitDiscCnfg"]).setValue(cfg->HitDiscCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["IleakRange"]).setValue(cfg->IleakRange);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvEn"]).setValue(cfg->LVDSDrvEn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvIref"]).setValue(cfg->LVDSDrvIref);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet06"]).setValue(cfg->LVDSDrvSet06);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet12"]).setValue(cfg->LVDSDrvSet12);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvSet30"]).setValue(cfg->LVDSDrvSet30);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["LVDSDrvVos"]).setValue(cfg->LVDSDrvVos);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Latch_en"]).setValue(cfg->Latch_en);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PllIbias"]).setValue(cfg->PllIbias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PllIcp"]).setValue(cfg->PllIcp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDAC"]).setValue(cfg->PlsrDAC);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDacBias"]).setValue(cfg->PlsrDacBias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrDelay"]).setValue(cfg->PlsrDelay);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrIdacRamp"]).setValue(cfg->PlsrIdacRamp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrPwr"]).setValue(cfg->PlsrPwr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrRiseUpTau"]).setValue(cfg->PlsrRiseUpTau);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PlsrVgOPamp"]).setValue(cfg->PlsrVgOPamp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbnFol"]).setValue(cfg->PrmpVbnFol);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbnLcc"]).setValue(cfg->PrmpVbnLcc);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbp"]).setValue(cfg->PrmpVbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpLeft"]).setValue(cfg->PrmpVbpLeft);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpMsnEn"]).setValue(cfg->PrmpVbpMsnEn);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpRight"]).setValue(cfg->PrmpVbpRight);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PrmpVbpf"]).setValue(cfg->PrmpVbpf);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["PxStrobes"]).setValue(cfg->PxStrobes);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ReadErrorReq"]).setValue(cfg->ReadErrorReq);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg13Spare"]).setValue(cfg->Reg13Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg17Spare"]).setValue(cfg->Reg17Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg1Spare"]).setValue(cfg->Reg1Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg21Spare"]).setValue(cfg->Reg21Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg22Spare1"]).setValue(cfg->Reg22Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg22Spare2"]).setValue(cfg->Reg22Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg27Spare1"]).setValue(cfg->Reg27Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg27Spare2"]).setValue(cfg->Reg27Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg28Spare"]).setValue(cfg->Reg28Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg29Spare1"]).setValue(cfg->Reg29Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg29Spare2"]).setValue(cfg->Reg29Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg2Spare"]).setValue(cfg->Reg2Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg30Spare"]).setValue(cfg->Reg30Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg31Spare"]).setValue(cfg->Reg31Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg34Spare1"]).setValue(cfg->Reg34Spare1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg34Spare2"]).setValue(cfg->Reg34Spare2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg6Spare"]).setValue(cfg->Reg6Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Reg9Spare"]).setValue(cfg->Reg9Spare);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["S0"]).setValue(cfg->S0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["S1"]).setValue(cfg->S1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB0"]).setValue(cfg->SELB0);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB1"]).setValue(cfg->SELB1);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SELB2"]).setValue(cfg->SELB2);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SR_Clock"]).setValue(cfg->SR_Clock);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SR_clr"]).setValue(cfg->SR_clr);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["ShiftReadBack"]).setValue(cfg->ShiftReadBack);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["SmallHitErase"]).setValue(cfg->SmallHitErase);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["StopModeCnfg"]).setValue(cfg->StopModeCnfg);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Stop_Clk"]).setValue(cfg->Stop_Clk);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TdacVbp"]).setValue(cfg->TdacVbp);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TempSensBias"]).setValue(cfg->TempSensBias);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TempSensDiodeSel"]).setValue(cfg->TempSensDiodeSel);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TempSensDisable"]).setValue(cfg->TempSensDisable);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TrigCnt"]).setValue(cfg->TrigCnt);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["TrigLat"]).setValue(cfg->TrigLat);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["VrefAnTune"]).setValue(cfg->VrefAnTune);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["VrefDigTune"]).setValue(cfg->VrefDigTune);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Vthin_AltCoarse"]).setValue(cfg->Vthin_AltCoarse);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["Vthin_AltFine"]).setValue(cfg->Vthin_AltFine);
	((ConfInt&)feconfig.subConfig("GlobalRegister_0/GlobalRegister")["GlobalRegister"]["no8b10b"]).setValue(cfg->no8b10b);
      }
    }

    pm->saveConfig(modRec);
    delete myDb;

  }catch(...){
    cout << "Error!" << endl;
    return -1;
  }
  return 0;
}
