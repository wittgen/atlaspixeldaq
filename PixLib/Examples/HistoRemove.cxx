#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

int main(int argc, char **argv) {
  
  
  // Check arguments
  if(argc<3) {
    std::cout << "Tool to remove folders and their content (files and subfolders) from the Histogram Server \n" 
	      << "USAGE: HistoRemove [partition name] [Scan/Analysis to delete (e.g.: /A00023/)]" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string rootName = argv[2];
  std::string oh = std::string(OHSERVERBASE)+"PixNameServerInternal";
  std::string serverName = "nameServer";
  std::string providerName = std::string(PROVIDERBASE)+"prov-remove";
  std::string program = "HistoRemove";
  long cmdExec;

  std::cout << "I want to delete the scan/analysis "<< rootName
	    << " from the cache (OH + nameServer)" << std::endl;

  struct timeval t0; 
  struct timeval t1;
  int time; 

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  std::stringstream info;

  time=gettimeofday(&t0,NULL); 
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, oh, serverName, providerName);
    std::cout << "Start deletion from the cache of the histograms contained in "<< rootName << " " << std::endl;
    cmdExec = hInt->removeTree(rootName);
    if (cmdExec == 0) {
      info << "The deletion of  "<< rootName << " has been succesfully executed ";
      ers::log(PixLib::pix::daq (ERS_HERE, program, info.str()));
    } else if (cmdExec == 1) {
      info << "The main folder does not exist ";
      ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
      return 0;
    } else if (cmdExec == 2) {
      info << "A subfoler does not exist ";
      ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
      return 0;
    } else if (cmdExec == 3) {
      info << "No histograms have been found in the histogram server ";
      ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
      return 0;
    } else if (cmdExec == 5) {
      info << "Unknown Exception was thrown ";
      ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
    }
  } catch (PixHistoServerExc e) {
    if (e.getId() == "IPCTIMEOUT") {      
      info << "IPC timeout ";
      ers::warning(PixLib::pix::daq (ERS_HERE, program, info.str()));
    } else if (e.getId() == "REMOVETREE" || e.getId() == "EMPTYSUPERLS" || e.getId() == "NOFOLDER") {
      ers::error(PixLib::pix::daq (ERS_HERE, program, e.getDescr()));
    }
    return 0;
  } catch (...) {
    info << "Unknown Exception was thrown ";
    ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
    return 0;
  }
  
  time=gettimeofday(&t1,NULL);
  std::cout << "HistoRemove tool spent: " 
	    << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) 
	    << " s for cleaning up the histogram server memory. " 
	    << std::endl;
  
  return 0;
}
