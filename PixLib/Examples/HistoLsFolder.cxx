#include <unistd.h>
#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixHistoServer/PixHistoServerInterface.h"


using namespace PixLib;


int main(int argc, char **argv) {
  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: HistoLsFolder [partition name] " << std::endl;
    return 1;
  }
  std::string ipcPartitionName = argv[1];
  std::string serverName = "nameServer";
  
  std::string fol;
  std::cout << "'LS' of the folder: ";
  std::cin >> fol;
  std::vector<std::string> f, folder, file, tot;
  
  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, std::string(OHSERVERBASE)+"crate0", 
							       serverName, std::string(PROVIDERBASE)+"histo_ls_folder");
    std::cout<<std::endl;
    std::cout << "1. LsFolder" << std::endl;
    folder  = hInt->lsFolder(fol);
    std::cout << "Inside folder: "<< fol << ". you find: "<< std::endl;
    if (folder.size() == 0) std::cout << " Nothing! " << std::endl;
    else {
      for(unsigned int i =0; i<folder.size(); i++){
 	std::cout<< folder[i] <<std::endl;
      }
    }
  
    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout << "2. LsFile" << std::endl;
    file  = hInt->lsFile(fol);
    std::cout << "Inside folder: "<< fol << ". you find: "<< std::endl;
    if (file.size() == 0) std::cout << " Nothing! " << std::endl;
    else {
      for(unsigned int j =0; j<file.size(); j++){
 	std::cout<< file[j] <<std::endl;
      }
    }
  
    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout << "3. Ls" << std::endl;
    f  = hInt->ls(fol);
    std::cout << "Inside folder: "<< fol << ". you find: "<< std::endl;
    if (f.size() == 0) std::cout << " Nothing! " << std::endl;
    else {
      for(unsigned int k =0; k<f.size(); k++){
 	std::cout<< f[k] <<std::endl;
      }
    }
  
    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout << "4. Super Ls - default false - (tot)" << std::endl;
    tot  = hInt->superLs(fol);
    std::cout << "Inside folder: "<< fol << ". you find: "<< std::endl;
    if (tot.size() == 0) std::cout << " Nothing! " << std::endl;
    else {
      for(unsigned int l=0; l<tot.size(); l++){
 	std::cout << tot[l] <<std::endl;
      }
    }

    std::cout<<std::endl;
    std::cout<<std::endl;
    std::cout << "5. Super Ls - true - (histo)" << std::endl;
    tot  = hInt->superLs(fol, true);
    std::cout << "Inside folder: "<< fol << ". you find: "<< std::endl;
    if (tot.size() == 0) std::cout << " Nothing! " << std::endl;
    else {
      for(unsigned int l=0; l<tot.size(); l++){
	std::cout << tot[l] <<std::endl;
      }
    }
    
  } catch (PixHistoServerExc e) {
    if (e.getId()=="NOIPC" || e.getId()== "NAMESERVER" || e.getId()=="NAMESERVERPROBLEM"
	|| e.getId()=="NOHOME")  {
      std::cout << e.getDescr() << std::endl;
      return 0; 
    } else if ( e.getId()== "SUPERLS" || e.getId()=="EMPTYSUPERLS" 
		|| e.getId()== "NOFOLDER" || e.getId()== "IPCTIMEOUT" 
		|| e.getId()== "LSFILE"|| e.getId()== "LSFOLDER") {
      std::cout << e.getDescr() << std::endl; 
    }
  } catch (...) {
    std::cout << "HistoLsFolder: Unknown Exception was thrown. " <<std::endl;
    return 0;
  }
  return 0;
}

