#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"

#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

#include <oks/kernel.h>
#include <oks/object.h>
#include <oks/class.h>
#include <oks/index.h>
#include <oks/attribute.h>
#include <oks/relationship.h>

using namespace PixLib;

enum mainMode { NORMAL, HELP  };
enum mainMode mode;

enum content { ALL, MOD, BOC};
enum content tagCont;

std::string dbServerName     = "PixelDbServer";
std::string ipcPartitionName = "PixelInfr";
std::string IsServerName     = "PixelRunParams";
IPCPartition *ipcPartition = NULL;
PixDbServerInterface *dbServer = NULL;
PixISManager* isManager = NULL; 

std::string rcdFolder = "PIT";

PixConnectivity* conn;
PixDisable* disableConfig = NULL;

std::string dataTakingPartitionNamePit = "PixelDD";
std::string dataTakingPartitionNameSr1 = "PixelDD";

void getParams(int argc, char **argv) {
  int ip = 1;
  mode = NORMAL;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--isServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  IsServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No IsServer name inserted" << std::endl << std::endl;
	  break;
	}
      } 
    }
  }
}

void connect() {
  //  Create IPCPartition constructors
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  do {
    sleep(1);
    dbServer=new  PixDbServerInterface(ipcPartition,dbServerName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete dbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << dbServerName << std::endl;
    exit(-1);
  }

  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(PixISManagerExc& e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return;
  }
}

std::string idTag = ""; 
std::string connTag = "";
std::string cfgTag = "";
std::string cfgMTag = "";
std::string disName = "";
std::string runName = "";
std::string wsDisName = "";
  
void readFromIs() {
  sleep(1);
  try { 
    idTag =  isManager->read<string>("DataTakingConfig-IdTag");
    connTag =  isManager->read<string>("DataTakingConfig-ConnTag");
    cfgTag =  isManager->read<string>("DataTakingConfig-CfgTag");
    cfgMTag =  isManager->read<string>("DataTakingConfig-ModCfgTag");
  }
  catch (...) {
    std::cout << "Cannot read tags from IS" << std::endl;
  }

  try { 
    disName =  isManager->read<string>("DataTakingConfig-Disable");
  }
  catch (...) {
    std::cout << "Cannot read PixDisable Name from IS" << std::endl;
  }

  try { 
    runName = isManager->read<string>("DataTakingConfig-RunConfig");
  }
  catch (...) {
    std::cout << "Cannot read PixRunConfig Name from IS" << std::endl;
  }

  try { 
    wsDisName = isManager->read<string>("DataTakingConfig-WsDisable");
  }
  catch (...) {
    std::cout << "Cannot read WS PixDisable Name from IS" << std::endl;
  }
}

void help() {
    cout << endl;
    cout << "This program gives copies the default tags used for data-taking to the environment " << endl;
    cout << endl; 
    cout << "Usage: GetDefTags\n" << std::endl;
    cout << "Optional switches:" << endl;
    cout << "  --help               prints this message" << endl; 
    cout << "  --dbPart <part>      IPC Partition  (def. PixelInfr)     " << endl;
    cout << "  --dbServ <dbServer>  DB server name (def. PixelDbServer) " << endl;
    cout << "  --isServ <IsServer>  IS server name (def. PixelRunParams)" << endl;
    cout << endl;
    exit(0);
}

int main(int argc, char **argv) {

  getParams(argc, argv);
  if(mode == HELP) {
    help();
  } else {
    IPCCore::init(argc, argv);
    connect();
    readFromIs();    
    if (idTag != "") std::cout << "PIX_ID_DEFTAG=" << idTag << " "; 
    if (connTag != "") std::cout << "PIX_CONN_DEFTAG=" << connTag << " "; 
    if (cfgTag != "") std::cout << "PIX_CFG_DEFTAG=" << cfgTag << " " ; 
    if (cfgMTag != "") std::cout << "PIX_MODCFG_DEFTAG=" << cfgMTag << " " ; 
    if (disName != "") std::cout << "PIX_DIS_DEFTAG=" << disName << " "; 
    if (runName != "") std::cout << "PIX_RUN_DEFTAG=" << runName << endl; 
  }
}
