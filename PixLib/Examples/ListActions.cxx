#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixUtilities/PixMessages.h"

#include "PixActions/PixActionsMulti.h"
#include "PixActions/PixActionsSingleIBL.h"
#include "PixBroker/PixBrokerMultiCrate.h"


using namespace PixLib;
using namespace SctPixelRod;

std::string cfgFileName;

int main(int argc, char **argv) {

  // GET COMMAND LINE

  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: ListActions [partition name]" << std::endl;
    return 1;
  }

  std::vector<std::string> freeActions, freeTimActions;  
  std::string ipcPartitionName = argv[1];

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  // CREATE BROKER
  PixBrokerMultiCrate* m_broker;
  m_broker = new PixBrokerMultiCrate("CRATE_ALL", m_ipcPartition);
  std::cout<< " " <<std::endl;
  std::set<std::string> brokersList = m_broker->listSubBrokers();
  std::set<std::string>::iterator broker, brokerEnd=brokersList.end();
  std::set<std::string> actionsList1 = m_broker->listActions(false);
  std::set<std::string> actionsList2 = m_broker->listActions(false);
  std::set<std::string> timActionsList1 = m_broker->listActions(false);
  std::set<std::string> timActionsList2 = m_broker->listActions(false);
  std::set<std::string>::iterator actions1, actions2, timActions1, timActions2;

  actionsList1 = m_broker->listActions(true, PixActions::SINGLE_ROD);
  actionsList2 = m_broker->listActions(false, PixActions::SINGLE_ROD);
  std::cout << "ON PARTITION "<< ipcPartitionName <<" THE FREE ACTIONS SINGLE_ROD ARE: " << std::endl;
  std::cout<< " " <<std::endl;
  int i = 0;
  for(actions1=actionsList1.begin(); actions1!=actionsList1.end(); actions1++) {
    for(actions2=actionsList2.begin(); actions2!=actionsList2.end(); actions2++) {
      if ((*actions1)==(*actions2) ) {
	i++;
	std::cout << i << ". " << (*actions2) << std::endl;
	freeActions.push_back((*actions1));
      }
    }
  }
  if (freeActions.size()==0) {
    std::cout << "ALL ACTIONS SINGLE_ROD ARE ALLOCATED! " << std::endl;
  }
  std::cout<< " " <<std::endl;

  timActionsList1 = m_broker->listActions(true, PixActions::SINGLE_TIM);
  timActionsList2 = m_broker->listActions(false, PixActions::SINGLE_TIM);
  std::cout << "ON PARTITION "<< ipcPartitionName <<" THE FREE ACTIONS SINGLE_TIM ARE: " << std::endl;
  std::cout<< " " <<std::endl;
  int j = 0;
  for(timActions1=timActionsList1.begin(); timActions1!=timActionsList1.end(); timActions1++) {
    for(timActions2=timActionsList2.begin(); timActions2!=timActionsList2.end(); timActions2++) {
      if ((*timActions1)==(*timActions2) ) {
	j++;
	std::cout << j << ". " << (*timActions2) << std::endl;
	freeTimActions.push_back((*timActions1));
      }
    }
  }
  if (freeTimActions.size()==0) {
    std::cout << "ALL ACTIONS SINGLE_TIM ARE ALLOCATED! " << std::endl;
  }
  std::cout<< " " <<std::endl;

}
