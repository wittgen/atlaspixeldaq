#! /bin/sh


${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.77 ROD_C0_S15 L2_B99_S1_C7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C0_S20 L2_B99_S1_A6 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.85 ROD_C0_S15 L2_B99_S2_A7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C0_S20 L2_B99_S2_C6 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C1_S14 D1A_B05_S1 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.78 ROD_C1_S14 D1A_B05_S2 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.85 ROD_C1_S12 L0_B12_S1_C7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C1_S17 L0_B12_S1_A6 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.81 ROD_C1_S5  L0_B12_S2_A7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C0_S11 L0_B12_S2_C6 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C1_S21 L2_B27_S1_C7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C1_S21 L2_B27_S1_A6 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.85 ROD_C1_S21 L2_B27_S2_A7 
${HOME}/daq/Applications/PixLib/Examples/TestDcsDDCcommands -set_viset 0.80 ROD_C1_S21 L2_B27_S2_C6 
