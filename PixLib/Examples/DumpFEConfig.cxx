//////////////////////////////////////////////////////////////////////////
//
//GetVrefDigValFromCfg, read the argument varible from the config and write it into a file
//
// Author: JuanAn xx/03/2016 
//
//
//////////////////////////////////////////////////////////////////////////


#include "RootDb/RootDb.h"

#include "TKey.h"
#include "TTree.h"
#include "TBranch.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TString.h"
#include "TDirectory.h"

#include <iostream>
#include <fstream>

TTree *t;

float getValFromBranch(TString name, TString title){

TBranch *br;
br=t->GetBranch(name.Data());


if(title.Contains("/I")){
int val;
br->SetAddress(&val);
br->GetEntry(0);
return val;
}

if(title.Contains("/F")){
float val;
br->SetAddress(&val);
br->GetEntry(0);
return val;
}

if(title.Contains("/B")){
bool val;
br->SetAddress(&val);
br->GetEntry(0);
return val;
}

return -1;
}

//This function return the last alphabetical file in the path in fileName argument
float getFileName(char *fullPath, std::string &fileName){

TSystemDirectory dir(fullPath, fullPath);

int counter=0;
float lastTS=0;

        TList *files = dir.GetListOfFiles();
         	if (files) {
      		TSystemFile *file;
      		TIter next(files);
      		
      		while ((file=(TSystemFile*)next())) {
      		  std::string fName = file->GetName();
		  if(fName.size() <23)continue;
		  std::string newName = fName.substr(8,15);
		  float timeStamp = atof(newName.c_str());
		  if(lastTS < timeStamp){
		   fileName=file->GetName();
		   lastTS=timeStamp;
		  }
		  counter++;  
		}
		
		}

std::cout<<"Nfiles "<<counter<<std::endl;

return 0;

}

void printHelp(){
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
std::cout<<"This program generate a text file with the parameter name from the current configuration"<<"\n"<<"the user has to provide an index txt file with the module name LI_SXX_X_XX_XX and the production name e.g. F100204601706"<<"\n"<<" and the path where the config files are stored e.g. /daq/db/cfg/ in SR1"<<"\n"<<std::endl;
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;

std::cout<<"Please enter all parameters---> Method: DumpFEConfig --i indexFile --p pathToCfg --o outputFile "<<std::endl;

exit(0);
}

int main(int argc, const char** argv) {

//Default args
std::string findex =" ";
std::string fout =" ";
std::string path =" ";

        if(argc>=2)
        {
                for(int i = 1; i < argc; i++)
                        if( *argv[i] == '-')
                        {
                                argv[i]++;
                                if( *argv[i] == '-') argv[i]++;
                                {
                                        switch ( *argv[i] )
                                        {
                                                case 'i' : findex=argv[i+1]; break;
                                                case 'p' : path=argv[i+1]; break;
                                                case 'o' : fout=argv[i+1]; break;
                                                case 'h' : printHelp( ); break;
                                                default : printHelp( ); return 0;
                                        }
                                }
                        }
        }

        if(fout==" "||findex==" "||fout==" "){
        printHelp();
        }

//Open index file
std::ifstream fIndex(findex.c_str());

if(!fIndex.is_open()){
  std::cout<<"File "<<findex<<" can not be open!!!"<<std::endl;
  return 0;
  }

int f0,f1,f2;

//Open output file
std::ofstream fOut(fout.c_str());

std::string moduleID,name,fileName;

const char *nameChar;

char target[256],fullPath[256],fullName[256],letter;

   //Bucle over the file entries
   while((fIndex>>moduleID>>name)){
   
   nameChar = name.c_str();
   //Scan first digits of the module name
   sscanf(nameChar,"%c%02d%02d%02d%*02d%*02d%*02d",&letter,&f0,&f1,&f2);
   
   sprintf(target,"%c%02d%02d%02d",letter,f0,f1,f2);
	
   std::cout<<"Target name "<<target<<std::endl;
   
   //Get the full path
   auto ret = snprintf(fullPath,sizeof(fullPath),"%s/%s/",path.c_str(),target);
   if(ret < 0) {
     std::cout << "Could not get proper full path. Terminating.\n";
     abort();	
   }
   std::cout<<fullPath<<std::endl;
   
   //Get the last file in the path (alphabetical order)
   getFileName(fullPath,fileName);
   
   //Full name of the file path/name
   ret = snprintf(fullName,sizeof(fullPath),"%s/%s",fullPath,fileName.c_str());
   if(ret < 0) {
     std::cout << "Could not get proper full path. Terminating.\n";
     abort();	
   }

   std::cout<<fullName<<std::endl;
   TFile *f = TFile::Open(fullName);
   std::string fName = fullName;
   fName = fName.substr(fName.find_last_of("/") + 1, fName.size() - fName.find_last_of("/") - 1);
   fName = fName.substr(0, fName.find("_00"));
   TDirectory* cfgDir = static_cast<TDirectory*>( f->GetDirectory(fName.c_str()) );
   if(!cfgDir) cfgDir = static_cast<TDirectory*>( f->GetDirectory("Def") );
   if(!cfgDir) std::cout << "Error: cannot find directory" << std::endl;
   else {
    // Look for a TTree. If you find one, it contains a module confiugration
    
      TIter next( cfgDir->GetListOfKeys() );
      TKey *key;
      while( (key=static_cast<TKey*>( next() )) ){
        if( key->ReadObj()->InheritsFrom("TTree") ) {
          t = (TTree*) key->ReadObj();
          TIter tNext (t->GetListOfBranches());
          TKey *tKey;
	  fOut<<fName<<"\t"<<moduleID<<"\n";
              while( (tKey=static_cast<TKey*>( tNext() )) ) {
                 
                 TString tS(tKey->GetName());
              	 TString tT(tKey->GetTitle());
              	 //std::cout<<tT<<std::endl;
              	if(tS.Contains("PixMcc_0.Registers") || tS.Contains("PixFe_")){
                
		float val = getValFromBranch(tS,tT);
		fOut<<tS<<"\t"<<val<<"\n";
	        }
	    }
         }
      }
    }
fOut<<"\n";

f->Close();
   
}

fOut.close();
fIndex.close();

}


