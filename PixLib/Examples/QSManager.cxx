/* ==============================================================
     Adapted from AutoDisOn.cxx - Feb. 14, 2012
===============================================================*/

#include "PixDbInterface/PixDbInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <map>
#include <sys/time.h>

using namespace std;

IPCPartition* ipcPartition = NULL;
PixISManager* isManager = NULL; 

std::string IsServerName     = "RunParams";
std::string ipcPartitionName = "PixelInfr"; //"PixelInfr" or "PixelInfr_user"
std::string rodName = "";
std::string action = "";
std::string item = "";

int main(int argc, char** argv) {



  int ipp = 1;
  while (ipp < argc) {
    std::string par = argv[ipp];
    if (par == "--part" && ipp+1 < argc) {
      ipcPartitionName = argv[ipp+1];
      ipp = ipp+2;
			std::cout << "Partition: " << ipcPartitionName << std::endl;
    } else if (par == "--rodName" && ipp+1 < argc) {
      rodName = argv[ipp+1];
      ipp = ipp+2;
			std::cout << "ROD: " << rodName << std::endl;
    } else if (par == "--action" && ipp+1 < argc) {
      action = argv[ipp+1];
      ipp = ipp+2;
    } else if (par == "--item" && ipp+1 < argc) {
      item = argv[ipp+1];
      ipp = ipp+2;
    }
  }

  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "QSManager: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }

  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(PixISManagerExc &e) {
    std::cout << "Exception caught in QSManager: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return -1;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in QSManager while creating ISManager for IsServer " << IsServerName << std::endl; 
    return -1;
  }

  std::cout << std::endl;
  if (rodName=="" && action!="B1S18"){
    cout<<"Enter a ROD (or layer for sync) name: ";
    getline(cin, rodName);
    cout<<endl;
  }

   
  string isString = "";
  if (action==""){
		while(1) {
    int option = -1;
    cout<<"Choose option: "<<endl;
    cout<<"1) Change QS mode"<<endl;
    cout<<"2) Issue QS user action"<<endl;
    cout<<"3) Change QS parameters"<<endl;
    cout<<"4) Enable/disable Module auto-sync"<<endl;
    cout<<"5) Change Module auto-sync parameters"<<endl;
    cout<<"6) Enable/disable ROD auto-sync parameters"<<endl;
    cout<<"7) Change ROD auto-sync parameters"<<endl<<endl;
    cout<<"8) Set History length"<<endl<<endl;
    cout<<"9) Quit"<<endl<<endl;
    cout<<">>>";
    cin>>option;
    std::cout<<"------------------------------------------------"<<std::endl;
    
    unsigned int it;
    string mode[7] = {"QS_OFF","QS_IDLE","QS_MON","QS_RST","QS_RST_DIS","QS_RST_CFG_ON","QS_RST_CFG_OFF"};
    string user[7] = {"QS_SINGLE_RST", "QS_SINGLE_CFG","QS_SINGLE_ENA","QS_SINGLE_DIS","QS_SINGLE_RST_ALL","QS_SINGLE_EXPERT_CFG_ON","QS_SINGLE_EXPERT_CFG_OFF"};
    string qsPar[5] = {"N_FC50_MAX","N_BU_MAX","N_TO_MAX","N_RST_MAX","N_CFG_MAX"};
    string syncModPar[4] = {"MODsyncErrThresh", "MODsyncISReadDelay", "MODsyncMaxTrials", "MODsyncMinIdle"};
    string syncRodPar[4] = {"RODsyncErrThresh", "RODsyncISReadDelay", "RODsyncMaxTrials", "RODsyncMinIdle"};
    string en = "";
	  
    switch(option){
    case 1: // change qs mode
      for (int i=0;i<7;i++) cout<<i<<") "<<mode[i]<<endl;
      cout<<">>>";
      cin>>it;
      isString = rodName+"/"+mode[it] +"/QuickStatusAction";
      isManager->publish(isString.c_str(), "");     
      cout<<"Published IS var: "<<isString<<endl;
      break;
      
    case 2: //issue qs user action
      for (int i=0;i<7;i++) cout<<i<<") "<<user[i]<<endl;
      cout<<">>>";
      cin>>it;
      std::cout<<"------------------------------------------------"<<std::endl;
      if (it<4 && item==""){
	cout<<"Enter module name: "<<endl;
	cin>>item;
      }
      isString = rodName+"/"+user[it]+"/QuickStatusAction";
      isManager->publish(isString.c_str(), item);     
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
      break;
      
    case 3: //change qs parameters
      for (int i=0;i<5;i++) cout<<i<<") "<<qsPar[i]<<endl;
      cout<<">>>";
      cin>>it;
      std::cout<<"------------------------------------------------"<<std::endl;
      cout<<"Enter new value: "<<endl;
      cin>>item;
      isString = rodName+"/"+qsPar[it] +"/QuickStatusAction";
      isManager->publish(isString.c_str(), item);     
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
      break;
      
    case 4: //ena/dis mod sync
      cout<<"Enter new value(ON/OFF): "<<endl;
      cin>>en;
      if (en=="ON" || en=="OFF"){
	isString = (rodName == "") ? "DSPMon_MODsyncEna" : rodName +"/DSPMon_MODsyncEna";
	isManager->publish(isString.c_str(), en);
	cout<<"Published IS var: "<<isString<<" with value "<<en<<endl;     
      }
      break;
      
    case 5: //change mod sync parameters
      for (int i=0;i<4;i++) cout<<i<<") "<<syncModPar[i]<<endl;
      cout<<">>>";
      cin>>it;
      isString = (rodName == "") ? "DSPMon_"+syncModPar[it] : rodName+"/DSPMon_"+syncModPar[it];
      std::cout<<"------------------------------------------------"<<std::endl;
      cout<<"Enter new value: "<<endl;
      if (isString =="DSPMon_MODsyncErrThresh") cout<<"(enter in multiples of 100)";
      else if (isString =="DSPMon_MODsyncISReadDelay") cout<<"(enter in multiples of 5 sec)";
      else if (isString =="DSPMon_MODsyncMinIdle") cout<<"(enter in multiples of DSPMon_MODsyncMinIdle, see IS-PixelInfr-RunParams)";
      cin>>it;
      isManager->publish(isString.c_str(), it);     
      cout<<"Published IS var: "<<isString<<" with value "<<it<<endl;
      break;
      
    case 6: //ena/dis rod sync
      cout<<"Enter new value(ON/OFF): "<<endl;
      cin>>en;
      if (en=="ON" || en=="OFF"){
	isString = rodName +"/DSPMon_RODsyncEna";
	isManager->publish(isString.c_str(), en);
	cout<<"Published IS var: "<<isString<<" with value "<<en<<endl;     
      }
      break;
      
    case 7: //change rod sync parameters
      for (int i=0;i<4;i++) cout<<i<<") "<<syncRodPar[i]<<endl;
      cout<<">>>";
      cin>>it;
      isString = rodName+"/DSPMon_"+syncRodPar[it];
      std::cout<<"------------------------------------------------"<<std::endl;
      cout<<"Enter new value: "<<endl;
      if (isString =="DSPMon_RODsyncErrThresh") cout<<"(enter in multiples of 100)";
      else if (isString =="DSPMon_RODsyncISReadDelay") cout<<"(enter in multiples of 5 sec)";
      else if (isString =="DSPMon_RODsyncMinIdle") cout<<"(enter in multiples of DSPMon_MODsyncMinIdle, see IS-PixelInfr-RunParams)";
      cin>>it;
      isManager->publish(isString.c_str(), it);     
      cout<<"Published IS var: "<<isString<<" with value "<<it<<endl;
      break;
      
    case 8: //change histLen (and re-enable)
      std::cout<<"Enter busy history length"<<std::endl;
      std::cin>>it;
      if (it > 0xffff) {
        cerr<<"Invalid entry."<<endl;
        return 0;
      }
      it = 0xffff | (it << 16);
      char str[1024];
      sprintf(str,"%i",it);
      item = str;
      isString = rodName+"/N_RST_MAX/QuickStatusAction";
      isManager->publish(isString.c_str(), item);       
      break;

    case 9: //exit
			exit(1);
			break;
    default:
      cout<<"Invalid entry"<<endl;
    }
   	} 
  } else if (action=="B1S18"){

    /***  menu for ROD_B1_S18  ***/

    rodName = "ROD_B1_S18";
    
    std::cout<<"Available actions"<<std::endl;
    std::cout<<"----------------------------------------------------------------------"<<std::endl;
    std::cout<<"ena - Enable all links that had enabled state at QS initialize"<<std::endl;
    std::cout<<"dis - Disable all links that had enabled state at QS initialize"<<std::endl;
    std::cout<<"cfg - Config all mods"<<std::endl;
    std::cout<<"----------------------------------------------------------------------"<<std::endl;
    std::cout<<"rtl - Set a new Readout Timeout limit."<<std::endl;
    std::cout<<"----------------------------------------------------------------------"<<std::endl;
    std::cout<<"hlen - Set history busy length"<<std::endl;
    std::cout<<"arr - Auto ROD re-enable  (TOGGLE)"<<std::endl;
    std::cout<<"trigPause - Trigger pause duration at link re-enable (arr)"<<std::endl;
    std::cout<<"----------------------------------------------------------------------"<<std::endl;
    std::cout<<">>> ";
    std::cin>>item;

    string mods[7] = {"L0_B10_S1_C7_M0","L0_B10_S1_C7_M1C","L0_B10_S1_C7_M2C","L0_B10_S1_C7_M3C","L0_B10_S1_C7_M4C","L0_B10_S1_C7_M5C","L0_B10_S1_C7_M6C"};
    unsigned int intItem = 0;
    char str[1024];
    if (item == "arr"){
      isString = rodName+"/QS_TEST_SYNCH/QuickStatusAction";
      isManager->publish(isString.c_str(), "0"); 

    } else if (item=="rtl"){
      std::cout<<"Enter value in microseconds: "<<std::endl;
      std::cin>>intItem;
      if (intItem > 0xffff) {
        cerr<<"Invalid entry."<<endl;
        return 0;
      }
      intItem = 0xffff | (intItem<<16);
      sprintf(str,"%i",intItem);
      item = str;
      isString = rodName+"/N_CFG_MAX/QuickStatusAction";
      isManager->publish(isString.c_str(), item);       
    
    } else if (item=="hlen"){
      std::cout<<"Enter busy history length"<<std::endl;
      std::cin>>intItem;
      if (intItem > 0xffff) {
        cerr<<"Invalid entry."<<endl;
        return 0;
      }
      intItem = 0xffff | (intItem<<16);
      sprintf(str,"%i",intItem);
      item = str;
      isString = rodName+"/N_RST_MAX/QuickStatusAction";
      isManager->publish(isString.c_str(), item);       
    
    } else if (item=="trigPause"){
      std::cout<<"Enter trigger pause at link re-enable (ms)"<<std::endl;
      std::cin>>intItem;
      if (intItem > 10){
        cerr<<"Entries bigger than 10 not allowed."<<endl;
        return 0;
      }
      intItem = 0xffff | (intItem<<16);
      sprintf(str,"%i",intItem);
      item = str;
      isString = rodName+"/N_FC50_MAX/QuickStatusAction";
      isManager->publish(isString.c_str(), item);       
    
    } else if (item=="ena"){
      for (int i=0;i<7;i++){
        isString = rodName+"/QS_SINGLE_ENA/QuickStatusAction";
        item = mods[i];
        isManager->publish(isString.c_str(), item); 
        std::cout<<"Enabled module "<<mods[i]<<std::endl;
        sleep(15);
      }
    
    } else if (item=="dis"){
      for (int i=0;i<7;i++){
        isString = rodName+"/QS_SINGLE_DIS/QuickStatusAction";
        item = mods[i];
        isManager->publish(isString.c_str(), item); 
        std::cout<<"Disabled module "<<mods[i]<<std::endl;
        sleep(15);
      }
    
    } else if (item=="cfg"){
      for (int i=0;i<7;i++){
        isString = rodName+"/QS_SINGLE_CFG/QuickStatusAction";
        item = mods[i];
        isManager->publish(isString.c_str(), item); 
        std::cout<<"Configured module "<<mods[i]<<std::endl;
        sleep(15);
      }
    } 

  } else { 
    
    /***   maintain compatibility with old way of entering ***/
    
    if (action == "QS_OFF" || action == "QS_MON"|| action == "QS_IDLE" || action == "QS_RST" ||
	action == "QS_RST_CFG_OFF" || action == "QS_RST_CFG_ON" || action == "QS_RST_DIS" ||
	action == "QS_SINGLE_RST" || action == "QS_SINGLE_CFG_ON"|| action == "QS_SINGLE_CFG_OFF" ||
	action == "QS_SINGLE_DIS"|| action == "QS_SINGLE_ENA" || action =="N_FC50_MAX" || action =="N_BU_MAX" ||
	action == "N_TO_MAX" || action =="N_RST_MAX" || action =="N_CFG_MAX" || action == "QS_TEST" ||
	action == "QS_SINGLE_CFG" || action == "QS_SINGLE_RST_ALL" || action == "QS_SINGLE_EXPERT_CFG_ON" || action == "QS_SINGLE_EXPERT_CFG_OFF" ) {
      isString = rodName+"/"+action +"/QuickStatusAction";
      std::string confirm;

      if (  action == "QS_SINGLE_EXPERT_CFG_ON" || action == "QS_SINGLE_EXPERT_CFG_OFF") {
        std::cout << "You are going to change the status of the preApms: " + action+ " , do you want to continue? [Yes/No] ";
        std::cin >> confirm;
        if ( confirm != "Yes" ){
          std::cout<< "Abort action, break!" << std::endl;
          return 0;
        }
      }
 
      isManager->publish(isString.c_str(), item);
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
    } else if(action == "RODsyncErrThresh" ||action == "RODsyncISReadDelay" ||action == "RODsyncMaxTrials" ||action == "RODsyncMinIdle"){
      isString = rodName+"/DSPMon_"+action;
      unsigned int itemInt = atol(item.c_str());
      isManager->publish(isString.c_str(), itemInt);
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
    } else if(action == "MODsyncErrThresh" ||action == "MODsyncISReadDelay" ||action == "MODsyncMaxTrials" ||action == "MODsyncMinIdle"){
      isString = rodName+"/DSPMon_"+action;
      unsigned int itemInt = atol(item.c_str());
      isManager->publish(isString.c_str(), itemInt);
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
    } else if (action=="RODsyncEna" || action=="MODsyncEna"){
      isString = rodName+"/DSPMon_"+action;
      isManager->publish(isString.c_str(), item);      
      cout<<"Published IS var: "<<isString<<" with value "<<item<<endl;
    } else {
      cout<<"Invalid entry."<<endl;
    }
  }

  return 0;
}
