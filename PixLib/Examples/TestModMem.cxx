#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixDbCoralDB/PixDbCoralDB.h"
#include "Config/Config.h"
#include "Examples/ConfigEx.h"
#include "RCCVmeInterface.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeData.h"
#include "PixFe/PixFeStructures.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI3Config.h"
#include "PixFe/PixFeI2.h"
#include "PixFe/PixFeI2Config.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Config/ConfMask.h"

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

#include <vector>

ConfMask<bool> cmb;
ConfMask<unsigned short int> cmus;

void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  cmb.dumpCounters();
  cmus.dumpCounters();
  sleep(del);
}

int main(int argc, char **argv) {

  int rep = 64;
  if (argc > 1) {
    std::string srep(argv[2]);
    std::istringstream is(srep);
    is >> rep;
  }

  mem(0);

  RootDb *rDb = new RootDb("testFe.cfg.root", "RECREATE");
  rDb->transactionStart();
  DbRecord* rootRec = rDb->readRootRecord();     
  DbRecord *mRec = rootRec->addRecord("ConfMask", "ConfMask");
  ConfMask<unsigned short int> *ms = new ConfMask<unsigned short int>(18, 160, 32, 16);
  for (int ic=0; ic<18; ic++) {
    for (int ir=0; ir<160; ir++) {
      ms->set(ic, ir, ic);
    }
  }
  ConfMatrix *mt = new ConfMatrix("Test", *ms, 16, "Test", true);
  mt->write(mRec);
  rDb->transactionCommit();
  delete rDb;
  delete mt;
  delete ms;

  std::cout << "Config file Created " << std::endl;
  mem(0);

  for (int rep=0; rep<2; rep++) {
    std::vector< ConfMatrix *> vmt;
    std::vector< ConfMask<unsigned short int>* > vmk;
    for (int i=0; i<150; i++) {
      for (int j=0; j<rep; j++) {
	vmk.push_back(new ConfMask<unsigned short int>(18, 160, 32, 16));
	vmt.push_back(new ConfMatrix("Test", *(vmk[vmk.size()-1]), 16, "Test", true));
        if (argc > 1) {
	  if (strncmp(argv[1],"read", 4) == 0) {
	    rDb = new RootDb("testFe.cfg.root", "READ");
	    DbRecord* root = rDb->readRootRecord();     
	    for (dbRecordIterator it1 = root->recordBegin(); it1 != root->recordEnd(); it1++) {
	      if ((*it1)->getClassName() == "ConfMask") { 
		vmt[vmt.size()-1]->read(*it1);
		break;
	      }
	    }
	    delete rDb;
	  }
	}
      }
      if (i%50 ==0) std::cout << "++ " << i << std::endl;
    }
    
    std::cout << "Created " << vmt.size() << " ConfMatrix" << std::endl;
    mem(0);
    
    for (int i=0; i<150; i++) {
      for (int j=0; j<rep; j++) {
	delete vmt[i*rep+j];
	delete vmk[i*rep+j];
      }
      if (i%50==0) std::cout << "++ " << i << std::endl;
    }
    
    std::cout << "Deleted All ConfMatrix" << std::endl;
    mem(0);
  }

}
