//////////////////////////////////////////////////////////////////////////
//
//GetVrefDigValFromCfg, read the current VrefDigVal module config and write it into a file
//
// Author: JuanAn xx/03/2016 
//
//TODO: make more generic, by passing the parameter via argument
//
//////////////////////////////////////////////////////////////////////////


#include "RootDb/RootDb.h"

#include "TKey.h"
#include "TTree.h"
#include "TBranch.h"
#include "TSystem.h"
#include "TSystemDirectory.h"
#include "TSystemFile.h"
#include "TString.h"
#include "TDirectory.h"

#include <iostream>

//This function return the last alphabetical file in the path in fileName argument
void getFileName(char *fullPath, std::string &fileName){

TSystemDirectory dir(fullPath, fullPath);
        TList *files = dir.GetListOfFiles();
         	if (files) {
      		TSystemFile *file;
      		TIter next(files);
      		
      		while ((file=(TSystemFile*)next())) {
      		  fileName=file->GetName();
		  }
		
		}

}

//This function gives the DAC VrefDigTune value which is stored in filename (root file) via args, extracted form dumpRootDbObj.cxx
int DumpRootDbObj(const char* fileName, int &DAC_FE0, int &DAC_FE1) {
	std::cout << "File name: " << fileName << std::endl;

	
	TFile *f = TFile::Open(fileName);
	TBranch *branch0;
	TBranch *branch1;
	std::string fName = fileName;
	fName = fName.substr(fName.find_last_of("/") + 1, fName.size() - fName.find_last_of("/") - 1);
	fName = fName.substr(0, fName.find("_00"));
	TDirectory* cfgDir = static_cast<TDirectory*>( f->GetDirectory(fName.c_str()) );
	if(!cfgDir) cfgDir = static_cast<TDirectory*>( f->GetDirectory("Def") );
	if(!cfgDir) std::cout << "Error: cannot find directory" << std::endl;
	else {
    // Look for a TTree. If you find one, it contains a module confiugration
    TTree *t;
    TIter next( cfgDir->GetListOfKeys() );
    TKey *key;
    while( (key=static_cast<TKey*>( next() )) ) {
      if( key->ReadObj()->InheritsFrom("TTree") ) {
        t = (TTree*) key->ReadObj();
        t->Scan("PixFe_0.GlobalRegister_0.GlobalRegister.VrefDigTune:PixFe_1.GlobalRegister_0.GlobalRegister.VrefDigTune");
        branch0=t->GetBranch("PixFe_0.GlobalRegister_0.GlobalRegister.VrefDigTune");
        branch0->SetAddress(&DAC_FE0);
        branch1=t->GetBranch("PixFe_1.GlobalRegister_0.GlobalRegister.VrefDigTune");
        branch1->SetAddress(&DAC_FE1);
        t->GetEntry(0);
        
        cout<<DAC_FE0<<"  "<<DAC_FE1<<endl;
      }
    }
    //DumpRootDbInDir(cfgDir);
  }
  //DumpRootDbInDir(f);
	f->Close();

return 0;
}


void printHelp(){
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
std::cout<<"This program generate a text file with the DAC from the current configuration"<<"\n"<<"the user has to provide an index txt file with the module name LI_SXX_X_XX_XX and the production name e.g. F100204601706"<<"\n"<<" and the path where the config files are stored e.g. /daq/db/cfg/ in SR1"<<"\n"<<std::endl;
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;

std::cout<<"Please enter all parameters---> Method: GetVrefDigDACFromCfg --i indexFile --p pathToCfg --o outputFile "<<std::endl;

exit(0);
}

int main(int argc, const char** argv) {

//Default args
std::string findex =" ";
std::string fout =" ";
std::string path =" ";
        if(argc>=2)
        {
                for(int i = 1; i < argc; i++)
                        if( *argv[i] == '-')
                        {
                                argv[i]++;
                                if( *argv[i] == '-') argv[i]++;
                                {
                                        switch ( *argv[i] )
                                        {
                                                case 'i' : findex=argv[i+1]; break;
                                                case 'p' : path=argv[i+1]; break;
                                                case 'o' : fout=argv[i+1]; break;
                                                case 'h' : printHelp( ); break;
                                                default : printHelp( ); return 0;
                                        }
                                }
                        }
        }

        if(fout==" "||findex==" "||fout==" "){
        printHelp();
        
        }

//Open index file
std::ifstream fIndex(findex.c_str());

if(!fIndex.is_open()){
  std::cout<<"File "<<findex<<" can not be open!!!"<<std::endl;
  //To be save the module name is changed to no val 
  return 0;
  }

int DAC_FE0,DAC_FE1;

int f0,f1,f2;

//Open output file
std::ofstream fOut(fout.c_str());

std::string moduleID,name,fileName;

const char *nameChar;

char target[256],fullPath[256],fullName[256],letter;

   //Bucle over the file entries
   while((fIndex>>moduleID>>name)){
   
   nameChar = name.c_str();
   //Scan first digits of the module name
   sscanf(nameChar,"%c%02d%02d%02d%02*d%02*d%02*d",&letter,&f0,&f1,&f2);
   
   sprintf(target,"%c%02d%02d%02d",letter,f0,f1,f2);
	
   std::cout<<"Target name "<<target<<std::endl;
   
   //Get the full path
   sprintf(fullPath,"%s/%s/",path.c_str(),target);
	
   std::cout<<fullPath<<std::endl;
   
   //Get the last alphabetical file in the path
   getFileName(fullPath,fileName);
   
   //Construct the full name of the file path/name
   sprintf(fullName,"%s/%s",fullPath,fileName.c_str());
    
   std::cout<<fullName<<std::endl;
   
   //Get VrefDigTune from the file
   DumpRootDbObj(fullName,DAC_FE0,DAC_FE1);
   
   //Write file
   fOut<<name<<"\t"<<moduleID<<"\t"<<DAC_FE0<<"\t"<<DAC_FE1<<"\n";
   
   }

fOut.close();
fIndex.close();

}


