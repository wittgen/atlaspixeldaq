#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>

#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixController/PixRunConfig.h"

using namespace std;

IPCPartition* ipcPartition = NULL;
PixDbServerInterface *DbServer;

std::string ipcPartitionName =     "PixelInfr";
std::string serverName = "PixelDbServer";
bool restore = false;
bool clean = false;
bool clear = false;

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

void getParams(int argc, char **argv) {
  int ip = 1;
  bool help = false;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help = true;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  help = true;
	  break;
	}
      } else if (std::string(argv[ip]) == "--clean") {
	clean = true; ip++; 
      } else if (std::string(argv[ip]) == "--clear") {
	clear = true; ip++;
      } else if (std::string(argv[ip]) == "--restore") {
	restore = true; ip++; 
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
  if (help) {
    std::cout << "\nDumpLoadList: dumps and manupulates the DbServer Load List \n\n";
    std::cout << "              --dbPart <name> select partition name \n";
    std::cout << "              --restore       put items in the processing list back to the load list \n";
    std::cout << "              --clean         clean the processing list \n\n";
    std::cout << "              --clear         clear the load list \n\n";
    exit(0);
  }
}

int main(int argc, char** argv) {
  getParams(argc, argv);
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  std::cout << "Connecting to partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "RunConfigEditor: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }
 
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << serverName << "  ..... " << std::endl;
  do {
    sleep(1);
    DbServer=new  PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << serverName << std::endl;

  std::vector<std::string> domlist;
  DbServer->ready();
  DbServer->listDomainRem(domlist);
  for (unsigned int id=0; id<domlist.size(); id++) {
    std::cout << "Domain " << domlist[id] << std::endl;
    std::vector<std::string> taglist;
    DbServer->listTagsRem(domlist[id],taglist);
    for(unsigned int it=0; it<taglist.size(); it++) {
      std::cout << "  Tag " << taglist[it] << std::endl;
      std::vector<std::string> types;
      std::vector<unsigned int> count;
      DbServer->listTypes(domlist[id], taglist[it], types, count);
      for (unsigned int it1=0; it1<types.size(); it1++) {
	std::cout << "    " << types[it1] << "  " << count[it1] << std::endl;
      }
      DbServer->listTypes(domlist[id], "?"+taglist[it], types, count);
      for (unsigned int it2=0; it2<types.size(); it2++) {
	std::cout << "    " << types[it2] << "  " << count[it2] << std::endl;
      }
    }
  }

  std::vector<std::string> list;
  DbServer->listLoadList(list);
  for (unsigned int i=0; i<list.size(); i++) {
    std::cout << list[i] << std::endl;
    if (restore || clean || clear) {
      if (clear || (list[i].substr(list[i].size()-7,7) == " (PROC)")) {
        if (clean) {
          DbServer->cleanLoadList(list[i].substr(0,list[i].size()-7));
          std::cout << list[i].substr(0,list[i].size()-7) << " cleaned" << std::endl;
        } else if (clear && (list[i].substr(list[i].size()-7,7) != " (PROC)")) {
          DbServer->cleanLoadList(list[i]);
          std::cout << list[i] << " removed from the load list" << std::endl;
        } else if (restore) {
          DbServer->restoreLoadList(list[i].substr(0,list[i].size()-7));
          std::cout << list[i].substr(0,list[i].size()-7) << " restored" << std::endl;
        }
      }
    }	
  }
  std::cout << std::endl;
  unsigned int llen = DbServer->getLoadListLen();
  std::cout << "Load list Lenght = " << llen << std::endl;

}
