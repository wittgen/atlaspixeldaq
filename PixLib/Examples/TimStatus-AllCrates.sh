#! /bin/bash

outfile=/tmp/ExecuteTimStatus_temp1.out
resultsfile=/tmp/ExecuteTimStatus_results1.out

if [ -e $resultsfile ] 
then 
rm $resultsfile
fi
echo "==============================================================">$resultsfile
for i in l-1 l-2 l-3 l-4 d-1 d-2 b-1 b-2 b-3
do

if [ -e $outfile ]
then
rm $outfile
fi

#ssh sbc-pix-rcc-$i '~/ExecuteTimStatus.sh' >> $outfile
ssh sbc-pix-rcc-$i 'source /det/pix/scripts/Default-pixel.sh; $PIX_LIB/Examples/TimStatus' >>$outfile
crate=`echo $i | sed 's/-//g' |tr '[:lower:]' '[:upper:]'`
echo " " >>$resultsfile
echo "Crate: $crate" >>$resultsfile
head -18 $outfile |tail -6 >>$resultsfile
echo " " >>$resultsfile

done
cat $resultsfile
