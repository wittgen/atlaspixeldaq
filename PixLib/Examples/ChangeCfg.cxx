#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI4.h"
#include "PixMcc/PixMcc.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "Config/ConfMask.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixBoc/PixBoc.h"
#include "PixBoc/IblPixBoc.h"

#include <TROOT.h>
#include <TH1.h>
#include "TH2.h"
#include <TKey.h>
#include <TFile.h>
#include "TCanvas.h"
#include "TApplication.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <regex>

using namespace PixLib;

struct timeval ti;
struct timeval tf;
int timeElapsed;


PixDbServerInterface *DbServer = nullptr;
IPCPartition *ipcPartition = nullptr;
PixConnectivity *conn = nullptr; 

std::string partitionName = "all partitions";
std::string     crateName = "all crates";
std::string       rodName = "all rods";
std::string       pp0Name = "all pp0s";
std::string        clName = "all cls";
std::string    moduleName = "all modules";
int feI=-1;

std::string inputRootFileName;
std::string outputRootFileName;
TFile* inputRootFile = nullptr;
TFile* outputRootFile = nullptr;

std::string txdelayFileName;
std::string rxPhasesFileName;

std::string idTag     = "PIXEL16";
std::string connTag   = "";
std::string cfgTag    = "";
std::string cfgModTag = "";
std::string pendTag = "_Tmp";
std::string domain  = "Configuration-"+idTag;
std::string prefix  = "";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
std::string dacName = "";
int dacStep = -9999;
int dacValue = -9999;
float t0Value = -9999.0;
int timDel = -1;
float VISet = -1;
bool isFei3Reg = false;
bool isFei4Reg = false;

//Variables added to implement fromFile Added by JuanAn
struct moduleFromFile{
  std::string moduleID;
  std::map<int,int> FE_DAC;
};

std::vector <moduleFromFile> moduleInfo;
moduleFromFile currentModule;

bool isFile = false;
bool noIbl = false;

enum mainMode { ALLBUTONEFE, ALLFE, GETALLFEOFF, GLOBDACSET, GLOBDACSHIFT, PIXDACSET, PIXREGSET, PIXDACSHIFT, HELP, ACTIVEI32, LASERON, LASEROFF, SETMSR, HITBUSOFF, ENABLE, DISABLE, SETSAFECFG, NOISE_MASK, TX_DELAY, TIM_DELAY, T0SET,SETPIXDAC_FROMHISTO,DUMPPIXDAC_TOHISTO,VISET,SET_RX_PHASES,GET_RX_PHASES,SET_TX_DELAYS_FROM_BOC};

enum saveMode { SAVE, NOSAVE};
enum verboseMode { VERBOSE, CLEAN};
mainMode mode;
saveMode smode = NOSAVE;
verboseMode vmode = VERBOSE;

bool isFei3PixelReg(std::string_view const sv) {
  try{
      static_cast<void>(Fei3::pixreg_name_lookup(sv));
      return true;
  } catch (std::range_error &) {
      return false;
  }
}

bool isFei4PixelReg(std::string_view const sv) {
  try{
      static_cast<void>(Fei4::pixreg_name_lookup(sv));
      return true;
  } catch (std::range_error &) {
      return false;
  }
}

bool isFei3GlobalReg(std::string_view const sv) {
  try{
      static_cast<void>(Fei3::globreg_name_lookup(sv));
      return true;
  } catch (std::range_error &) {
      return false;
  }
}

bool isFei4GlobalReg(std::string_view const sv) {
  try{
      static_cast<void>(Fei4::globreg_name_lookup(sv));
      return true;
  } catch (std::range_error &) {
    //do nothing
  }
   try{
      static_cast<void>(Fei4::mergedGlobreg_name_lookup(sv));
      return true;
  } catch (std::range_error &) {
      return false;
  }
}

bool checkFEFlavour(bool isFei3) {
      if(isFei3 && !isFei3Reg) {
	      std::cout << "--- Trying to set register " << dacName << " for a Fei3 ROD/module which is only a Fei4 register. Skipping.\n";
	      return false; 
      }
      if(!isFei3 && !isFei4Reg) {
	      std::cout << "--- Trying to set register " << dacName << " for a Fei4 ROD/module which is only a Fei3 register. Skipping.\n";
              return false;
      }
      return true;
}

bool checkIfPixelReg(std::string_view const sv){
  isFei3Reg = isFei3PixelReg(sv); 
  isFei4Reg = isFei4PixelReg(sv);
  return isFei3Reg || isFei4Reg;
}

bool checkIfGlobalReg(std::string_view const sv){
  isFei3Reg = isFei3GlobalReg(sv); 
  isFei4Reg = isFei4GlobalReg(sv);
  return isFei3Reg || isFei4Reg;
}

void printTagInfo(){
  std::cout << "Working on idTag \"" << idTag << "\" with connTag/cfgTag/cfgModTag " << connTag << "/" << cfgTag << "/" << cfgModTag << '\n'; 
}

void printModInfo(ModuleConnectivity* modConn){
 std::cout << "- Working on module: " << modConn->name() << " prodId: " << modConn->prodId() << '\n';
}

std::tuple<EnumFEflavour::FEflavour, int, std::unique_ptr<PixModule>> setupPixModule(ModuleConnectivity* modConn) {
  EnumMCCflavour::MCCflavour mccFlv;
  EnumFEflavour::FEflavour feFlv;
  int nFe;
  modConn->getModPars(mccFlv, feFlv, nFe);
  auto mod = std::make_unique<PixModule>(DbServer, (PixModuleGroup*)nullptr, domain, cfgModTag, modConn->prodId(), mccFlv, feFlv, nFe);
  return std::make_tuple(feFlv, nFe, std::move(mod));
}

void save(PixModule* mod) {
    if(pendTag.empty())
      mod->writeConfig(DbServer, domain, cfgModTag);
    else 
      mod->writeConfig(DbServer, domain, cfgModTag, pendTag);
}

void save(PixModuleGroup& modGrp) {
    if(pendTag.empty())
      modGrp.writeConfigAll(DbServer, domain, cfgTag);
    else
      modGrp.writeConfigAll(DbServer, domain, cfgTag, pendTag);
}

inline std::string strtolower(std::string data) {
  std::transform(data.begin(), data.end(), data.begin(), ::tolower);
  return data;
}

TH2* getHisto(std::string modName) {
  TH2* histo = nullptr;
  auto histoDir = (TDirectory*)inputRootFile->GetDirectory(modName.c_str());
  if (histoDir) histo = (TH2*)((TKey*)histoDir->GetListOfKeys()->First())->ReadObj();
  if (!histo) std::cout << "Cannot find histogram for module " << modName << std::endl;
  return histo;
}


//Added by JuanAn read the fileName and store it in vectors
bool getFromFile(std::string const & fileName){
  std::ifstream file(fileName.c_str());
  if(!file.is_open()){
    std::cout<<"File "<<fileName<<" can not be opened!"<<std::endl;
    //To be save the module name is changed to no val 
    moduleName ="";
    return false;
  }
  
  std::string modName,line;
  int fe,value;
  bool update = false;
  
   while(getline(file, line)){
    std::stringstream ss;
    ss <<line;
    if(!(ss>>modName>>fe>>value))continue;
    moduleFromFile readedModule;
    readedModule.moduleID = modName;
    readedModule.FE_DAC[fe]=value;
    update=false;
    	//Update module info
    	for(unsigned int i=0;i<moduleInfo.size();i++){
  	  if(modName==moduleInfo[i].moduleID){
  	  moduleInfo[i].FE_DAC[fe]=value;
    	  update=true;
    	  }
    	}
    	if(!update)moduleInfo.push_back(readedModule);
    if(fe<0||fe>1){std::cout<<"FrontEnd number cannot be different than 0 or 1. File formal <module> <frontEnd> <value>"<<std::endl;moduleName ="";return false;}
    }
  file.close();
  return true;
}

//Added by JuanAn set the values from the file stored in the vector
bool setVarFromFile(std::string modID){

	for(unsigned int i=0;i<moduleInfo.size();i++){
	
	  if(moduleInfo[i].moduleID==modID){
	  moduleName=modID;
	  currentModule = moduleInfo[i];
	  
	  if(vmode == VERBOSE)
	  std::cout<<moduleName<<" changed fromFile "<<std::endl;
	  	  
	  return true;
	  }
	
	
	}

//To be save the module name is changed to no val
moduleName ="";

return false;
}

void openDbServer(int argc, char** argv) {   
  IPCCore::init(argc, argv);                                                       
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

bool getParams(int argc, char **argv) {
  int ip = 1;
  bool found=false;
  mode = HELP;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsPart = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--idTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  idTag = argv[ip+1];
	  domain = "Configuration-";
	  domain += idTag;
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No idTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--connTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  connTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No connTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--cfgTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No cfgTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--cfgModTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgModTag = argv[ip+1];
	  ip += 2; 
	  found = true;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No cfgmodTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--pendTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pendTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No pendingTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--save") {
	smode = SAVE; ip++; 
      } else if (std::string(argv[ip]) == "--clean") {
	vmode = CLEAN; ip++; 
      } else if (std::string(argv[ip]) == "--partitionName") {
	if (ip+1 < argc  && std::string(argv[ip+1]).substr(0,2) != "--") {
	  partitionName = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partitionName name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--crateName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  crateName = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No crateName name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      }
      else if (std::string(argv[ip]) == "--rodName") {
	if (ip+1 < argc &&  std::string(argv[ip+1]).substr(0,2) != "--") {
	  rodName = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No rodName name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--pp0Name") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pp0Name = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No pp0Name name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--clName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  clName = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No clName name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--moduleName") {
	if (ip+1 < argc &&  std::string(argv[ip+1]).substr(0,2) != "--") {
	  moduleName = argv[ip+1];
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No moduleName name inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--feNumber") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  feI=atoi(argv[ip+1]);
	  std::cout << "Fe Number= " << feI << std::endl;
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No feNumber inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--setFeMaskOff") {
	mode = ALLBUTONEFE; ip++; 
      } else if (std::string(argv[ip]) == "--setFeMaskOn") {
	mode = ALLFE; ip++; 
      } else if (std::string(argv[ip]) == "--getFeMaskOff") {
	mode = GETALLFEOFF; ip++;
      } else if (std::string(argv[ip]) == "--increaseGlobDac") {
	mode = GLOBDACSHIFT;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfGlobalReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacStep = atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC step inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--decreaseGlobDac") {
	mode = GLOBDACSHIFT;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfGlobalReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacStep = -atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC step inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--setGlobDac") {
	mode = GLOBDACSET;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfGlobalReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
          return false;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacValue = atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC value inserted" << std::endl << std::endl;
	  break;
	}
	ip++;	
      } else if (std::string(argv[ip]) == "--increasePixDac") {
	mode = PIXDACSHIFT;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfPixelReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacStep = atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC value inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--decreasePixDac") {
	mode = PIXDACSHIFT;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfPixelReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacStep = -atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC value inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--setPixDac") {
	mode = PIXDACSET;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfPixelReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacValue = atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC value inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--setPixReg") {
	mode = PIXREGSET;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  if(!checkIfPixelReg(dacName)) {
            std::cout << "DAC " << dacName << " is not a valid register!\nPlease rerun with proper register\n"; 
            return false;
	  }
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacValue = atoi(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No DAC value inserted" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--hitbusOff") {
	mode = HITBUSOFF;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  inputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "RootFile name is: " << inputRootFileName << std::endl; 
	ip++;
      }  else if (std::string(argv[ip]) == "--enaPix") {
	mode = ENABLE;
	ip++;
      }  else if (std::string(argv[ip]) == "--disPix") {
	mode = DISABLE;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  inputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "RootFile name is: " << inputRootFileName << std::endl; 
	ip++;
      }  else if (std::string(argv[ip]) == "--setSafeCfg") {
	mode = SETSAFECFG;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  inputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "RootFile name is: " << inputRootFileName << std::endl; 
	ip++;
      } else if (std::string(argv[ip]) == "--appMask") {
	mode = NOISE_MASK;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  inputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "RootFile name is: " << inputRootFileName << std::endl; 
	ip++;
      } else if (std::string(argv[ip]) == "--dumpPixDacToHisto") {
	mode = DUMPPIXDAC_TOHISTO;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  outputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout<<"fileName="<<outputRootFileName<<std::endl;
	
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  ip ++;
	} 
	std::cout<<"dacName="<<dacName<<std::endl;
	ip++;
      } else if (std::string(argv[ip]) == "--setPixDacFromHisto") {
	mode = SETPIXDAC_FROMHISTO;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  inputRootFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout<<"fileName="<<inputRootFileName<<std::endl;
		
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  ip ++;
	} 
	std::cout<<"dacName="<<dacName<<std::endl;
	ip++;
	
      } else if (std::string(argv[ip]) == "--setTxDelay") {
	mode = TX_DELAY;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  txdelayFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "Tx delay file name is: " << txdelayFileName << std::endl; 
	ip++;
      } else if (std::string(argv[ip]) == "--setTxDelaysFromBOC") {
	mode = SET_TX_DELAYS_FROM_BOC;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  txdelayFileName = argv[ip+1];
	  ip ++;
	}
	std::cout << "Tx delay file name is: " << txdelayFileName << std::endl; 
	ip++;
      } else if (std::string(argv[ip]) == "--setVISet") {
	mode = VISET;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  VISet=atof(argv[ip+1]);
	  std::cout << "VISet = " << VISet << std::endl;
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No VISET specified" << std::endl << std::endl;
	  break;
	}
	ip++;
      }else if (std::string(argv[ip]) == "--setRxPhases") {
	mode = SET_RX_PHASES;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  rxPhasesFileName = argv[ip+1];
	  ip ++;
	} 
	std::cout << "Rx Phases file name is: " << rxPhasesFileName << std::endl; 
	ip++;
      } else if (std::string(argv[ip]) == "--getRxPhases") {
	mode = GET_RX_PHASES;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  rxPhasesFileName = argv[ip+1];
	  ip ++;
	}
	ip++;
      }
      else if (std::string(argv[ip]) == "--setTimDelay") {
	mode = TIM_DELAY;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  timDel=atoi(argv[ip+1]);
	  std::cout << "TIM Delay = " << timDel << std::endl;
	  ip++; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No TIM delay specified" << std::endl << std::endl;
	  break;
	}
	ip++;
      } else if (std::string(argv[ip]) == "--setT0") {
	mode = T0SET;
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dacName = argv[ip+1];
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No T0 parameter name inserted" << std::endl << std::endl;
	  break;
	}
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  t0Value = atof(argv[ip+1]);
	  ip++;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No T0 parameter value inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } 
         //Added by JuanAn added fromFile argument
      else if (std::string(argv[ip]) == "--fromFile") {
	std::string fileName;
	
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  fileName = argv[ip+1];
	  std::cout << "fileName = " << fileName << std::endl;
	  ip++; 
	  
	  isFile=getFromFile(fileName);
	    if(!isFile){
	    mode = HELP;
	    std::cout << std::endl << "File "<<fileName<<" is no properly set" << std::endl << std::endl;
	    break;
	   }
	 }
	  	  
	 else {
	  mode = HELP;
	  std::cout << std::endl << "No fileName has been written" << std::endl << std::endl;
	  break;
	}
	ip++;
      }
      else if (strtolower(argv[ip]) == "--noibl") {	  
        std::cout << "Skipping IBL ..." << std::endl;
        noIbl = true;
        ip++; // Todo: move to outer loop
      }
       else {
	std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
	mode = HELP;
	break;
      }     
    }
  }
  if (!found) cfgModTag=cfgTag;
  return true;
}

void setFeMaskOn(ModuleConnectivity* modConn) {
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool changed = false;
  int actFe = -1;
  if (feI >=0 && feI<nFe){
    actFe=feI;
  }
  for(int i=0;i<nFe;i++){
    if (actFe == -1 || i == actFe) {
      Config &cfg = mod->pixFE(i)->config();
      if(((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value == false) {
	changed = true;
	((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value = true;
      }
      if(((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value == false) {
	changed = true;
	((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value = true;
      }
      if(((ConfBool&)cfg["Misc"]["DacsEnable"]).m_value == false) {
	changed = true;
	((ConfBool&)cfg["Misc"]["DacsEnable"]).m_value = true;
      }
    }
  }
  if(vmode == VERBOSE){
    if(!isFile) std::cout << " Setting for module: " << modConn->name() << " prodId: " << modConn->prodId() << " requested frontend enabled" << std::endl; 
    if(changed == true) std::cout << "configuration changed" << std::endl; else std::cout << "WARNING: module was already configured as desired" << std::endl;
  }
  if(smode == SAVE && changed) save(mod.get());
}

void getFeMaskOff(ModuleConnectivity* modConn) {
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  for(int i=0;i<nFe;i++){
    Config &cfg = mod->pixFE(i)->config();
    if(((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value == false) {
      std::cout << "Module:" << modConn->name() << "  FE:" << i << " is disabled" << std::endl;
    }
  }
}

void setFeMaskOff(ModuleConnectivity* modConn) {
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool changed = false;
  int actFe=-1;
  if (feI >=0 && feI<nFe){
    actFe=feI;
  }
  for(int i=0;i<nFe;i++){
    if (actFe == -1 || i == actFe) {
      Config &cfg = mod->pixFE(i)->config();
      if(((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value == true) {
	changed = true;
	((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value = false;
      }
      if(((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value == true) {
	  changed = true;
	  ((ConfBool&)cfg["Misc"]["ScanEnable"]).m_value = false;
      }
      if(((ConfBool&)cfg["Misc"]["DacsEnable"]).m_value == true) {
	changed = true;
	((ConfBool&)cfg["Misc"]["DacsEnable"]).m_value = false;
      }
    }
  }
  if(vmode == VERBOSE){
    std::cout << " Setting for module: " << modConn->name() << " prodId: " << modConn->prodId() << " requested frontend disabled" << std::endl; 
    if(changed) std::cout << "Configuration changed\n";
    else std::cout << "WARNING: module was already configured as desired\n";
  }
  if(smode == SAVE && changed) save(mod.get());
}

void globDacSet(ModuleConnectivity* modConn) {

  std::string mydac = dacName;
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  int prevDAC = -1;
  for(int i=0;i<nFe;i++){
  if(isFile){
      auto it = currentModule.FE_DAC.find(i);
    	if (it == currentModule.FE_DAC.end())continue;

      dacValue = currentModule.FE_DAC[i];

      feI=i;
      }
    if (feI == -1 || feI == i){
      Config &cfg = mod->pixFE(i)->config();
      Config *subcfg=0;
      for(unsigned int j=0; j<cfg.subConfigSize(); j++){
	if(cfg.subConfig(j).name()=="GlobalRegister_0"){
	  subcfg = &(cfg.subConfig(j));
	  break;
	}	  
      }
      if(subcfg==0){
	std::cerr << "FE " << i << " had no globreg config" << std::endl;
      } else{
	Config &globreg = *subcfg;
	if (globreg["GlobalRegister"][mydac].name() != "__TrashConfObj__"){
	  prevDAC = (int)((ConfInt&)globreg["GlobalRegister"][mydac]).getValue();
	  *((int *)((ConfInt&)globreg["GlobalRegister"][mydac]).m_value) = (int)dacValue;
	  if(vmode == VERBOSE)
            std::cout<< "Setting " << mydac << " for " << mod->moduleName() <<", FE "<<i<<" to be changed from "<<prevDAC<<" to "<<dacValue<<std::endl;
	}
	else {
	  std::cerr << "DAC " << mydac << " not in global register" << std::endl; 
	}
      }
    }
  }
  if(vmode == VERBOSE){
    if(!isFile) std::cout << " Setting for module: " << modConn->name()<< " FE "<<feI  << " prodId: " << modConn->prodId() << " " << mydac <<" from "<< prevDAC<< " to " << dacValue << std::endl;
  }
  if(smode == SAVE) save(mod.get());
}

void t0Set(ModuleConnectivity* modConn) {
  if (dacName != "CAL_Delay" && dacName != "LV1_Contiguous" && dacName != "CAL_Range" && dacName.find("DELAY_") == std::string::npos && dacName.find("WBITD_") == std::string::npos && dacName != "WRECD") {
    std::cout << "No T0 parameter specified, stopping" << std::endl;
    return;
  }
  std::string mydac = dacName;

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
   if (dacName.find("CAL") != std::string::npos) {
    if (mod->pixMCC()->config()["Registers"][mydac.c_str()].name()!= "__TrashConfObj__")
      *((int *)((ConfInt&)mod->pixMCC()->config()["Registers"][mydac.c_str()]).m_value) = (int)t0Value;
    else std::cerr << "Register " << mydac << " is not in MCC configuration." << std::endl;
  } else if (dacName.find("LV1") != std::string::npos) {
    if (mod->pixMCC()->config()["Registers"][mydac.c_str()].name()!= "__TrashConfObj__")
      *((int *)((ConfInt&)mod->pixMCC()->config()["Registers"][mydac.c_str()]).m_value) = (int)t0Value;
    else std::cerr << "Register " << mydac << " is not in MCC configuration." << std::endl;
  } else if (dacName.find("DELAY_") != std::string::npos) {
    if (mod->pixMCC()->config()["Strobe"][mydac.c_str()].name()!= "__TrashConfObj__")
      ((ConfFloat&)mod->pixMCC()->config()["Strobe"][mydac.c_str()]).m_value = (float)t0Value;
    else std::cerr << "Strobe " << mydac << " is not in MCC configuration." << std::endl;
  }
  else if (dacName.find("WBITD") != std::string::npos || dacName.find("WRECD") != std::string::npos) {
    if (mod->pixMCC()->config()["Registers"][mydac.c_str()].name()!= "__TrashConfObj__")
      *((int *)((ConfInt&)mod->pixMCC()->config()["Registers"][mydac.c_str()]).m_value) = (int)t0Value;
    else std::cerr << "Register " << mydac << " is not in MCC configuration." << std::endl;
  }
 
  if(vmode == VERBOSE){
    std::cout << " Setting for module: " << modConn->name() << " prodId: " << modConn->prodId() << " " << mydac << " to " << t0Value << std::endl;
  }
  if(smode == SAVE) save(mod.get());
}


void globDacShift(ModuleConnectivity* modConn) {
 if(vmode == VERBOSE) {
     printModInfo(modConn);
     std::cout << "-- Attempting to shift " << dacName << " by " << dacStep << '\n';
  }
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  for(int i=0;i<nFe;i++){
  if(isFile){
      auto it = currentModule.FE_DAC.find(i);
    	if (it == currentModule.FE_DAC.end())continue;
      
      dacValue = currentModule.FE_DAC[i];
      feI=i;
      }
    if (feI == -1 || i == feI){
      Config &cfg = mod->pixFE(i)->config();
      Config *subcfg=0;
      for(unsigned int j=0; j<cfg.subConfigSize(); j++){
	if(cfg.subConfig(j).name()=="GlobalRegister_0"){
	  subcfg = &(cfg.subConfig(j));
	  break;
	}	  
      }
      if(subcfg==0){
	std::cerr << "FE " << i << " had no globreg config" << std::endl;
      } else{
	Config &globreg = *subcfg;
	if (globreg["GlobalRegister"][dacName].name() != "__TrashConfObj__"){
	  std::cout<<"--- Original Global register:"<<((ConfInt&)globreg["GlobalRegister"][dacName]).getValue()<<std::endl;
	  *((int *)((ConfInt&)globreg["GlobalRegister"][dacName]).m_value) = ((ConfInt&)globreg["GlobalRegister"][dacName]).getValue() + (int)dacStep;
	  
	  if (dacName == "GLOBAL_DAC" && ((ConfInt&)globreg["GlobalRegister"][dacName]).getValue() < 10) {
	    std::cout << "--- GLOBAL_DAC limited to 10 for module " << modConn->name() << " FE " << i << std::endl;
	    *((int *)((ConfInt&)globreg["GlobalRegister"][dacName]).m_value) = 10;
	  }
	  if (dacName == "GLOBAL_DAC" && ((ConfInt&)globreg["GlobalRegister"][dacName]).getValue() > 31) {
	    std::cout << "--- GLOBAL_DAC limited to 31 for module " << modConn->name() << " FE " << i << std::endl;
	    *((int *)((ConfInt&)globreg["GlobalRegister"][dacName]).m_value) = 31;
	  }
	}
	else {
	  std::cerr << "--- DAC " << dacName << " not in global register" << std::endl; 
	}
      }
    }
  }
  if(smode == SAVE) save(mod.get());
}

void activeI32(ModuleConnectivity* modConn) {
  bool changed = false;
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  if(vmode == VERBOSE){
    std::cout << " Module: " << modConn->name() << " prodId: " << modConn->prodId() << " has Active flag" << (mod->config())["general"]["Active"].valueInt() << std::endl; 
  }
  if((mod->config())["general"]["Active"].valueInt() == 0) {
    changed = true; 
    (mod->config())["general"]["Active"].valueInt() = 1;
    std::cout << " Module: " << modConn->name() << " prodId: " << modConn->prodId() << " has now Active flag" << (mod->config())["general"]["Active"].valueInt() << std::endl;
  }  
  if(smode == SAVE && changed) save(mod.get());
}

void pixRegSet(ModuleConnectivity* modConn) { 
  if (vmode==VERBOSE) {
	  printModInfo(modConn);
          std::cout << "-- Attempting to set " << dacName << " to " << dacValue << std::endl; 
  }
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  if(!checkFEFlavour(isFei3)) return;
  for (auto fe: *mod){
    if (feI == -1 || fe->number() == feI){
      ConfMask<bool> &pixReg =  isFei3 ? static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::pixreg_name_lookup(dacName)):
                                         static_cast<PixFeI4*>(fe)->readPixRegister(Fei4::pixreg_name_lookup(dacName));
      pixReg.setAll(dacValue);
    }
  }
  if(smode == SAVE) save(mod.get());
}

void pixDacSet(ModuleConnectivity* modConn) {
  if(vmode==VERBOSE) {
	  printModInfo(modConn);
          std::cout << "-- Attempting to set " << dacName << " to " << dacValue << std::endl; 
  }
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  if(!checkFEFlavour(isFei3)) return;
  for(auto fe: *mod){
    if (feI == -1 || fe->number() == feI){
      ConfMask<unsigned short int> &tdacs =  isFei3 ? static_cast<PixFeI3*>(fe)->readTrim(Fei3::pixreg_name_lookup(dacName)):
	                                              static_cast<PixFeI4*>(fe)->readTrim(Fei4::pixreg_name_lookup(dacName));
      if(dacValue > tdacs.maxVal()) {
	      std::cout << "--- Requested to write " << dacValue << " for register " << dacName 
		        << " which is larger then the max. value of " << tdacs.maxVal() << ". Skipping.\n";
	      return;
      }
      tdacs.setAll(dacValue);
    }
  }
  if(smode == SAVE) save(mod.get());
}

//TEMPORARY AND TESTING VERSION -> Not to be used at production level.
void dumpPixDacToHisto(ModuleConnectivity* modConn) {

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);

  //Prepare the limits of the histogram axes depending on the module type: IBL/oldPixel - to be fixed for DBM..
  int const nCols = mod->geometry().nCol();
  int const nRows = mod->geometry().nRow();
    
  TH2F* histo = new TH2F((modConn->name()).c_str(),(modConn->name()).c_str(),nCols,0,nCols,nRows,0,nRows);
  std::cout<<"Created histo "<<histo<<std::endl;
  histo->SetDirectory(0); //necessary? I don't know if I want it memory staged
  std::cout<<"SetDirectory(0)"<<std::endl;
  
  //Declare the conf mask of all the front ends of this module.
  std::vector<ConfMask<unsigned short int>*> allMap_trim {static_cast<std::size_t>(nFe), nullptr};
  std::vector<ConfMask<bool>*>               allMap_bool {static_cast<std::size_t>(nFe), nullptr};
  //dumpMode: if 0 dump the ENABLE or PREAMP, if 1 dump the dac (TDAC/FDAC)
  unsigned int dumpMode = 0;
  
  if (dacName.find("TDAC")!=std::string::npos || dacName.find("FDAC")!=std::string::npos)
    dumpMode=1;
  else if (dacName.find("ENABLE")!=std::string::npos || dacName.find("PREAMP")!=std::string::npos)
    dumpMode=0;
  else 
    {
      std::cout<<"Not found a proper dumpMode: please select TDAC/FDAC/ENABLE/PREAMP"<<std::endl;
      return;
    }
  //And Get the actual Values
  if(!checkFEFlavour(isFei3)) return;
  for (auto fe: *mod){
    if (dumpMode==1)
      allMap_trim[fe->number()] = isFei3 ? &(static_cast<PixFeI3*>(fe)->readTrim(Fei3::pixreg_name_lookup(dacName))):
	                                   &(static_cast<PixFeI4*>(fe)->readTrim(Fei4::pixreg_name_lookup(dacName))); 
    else //dumpMode==0
      allMap_bool[fe->number()] = isFei3 ? &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::pixreg_name_lookup(dacName))):
	                                   &(static_cast<PixFeI4*>(fe)->readPixRegister(Fei4::pixreg_name_lookup(dacName)));
    std::cout<<"get fe number " << fe->number()<<std::endl;
  }
  
  //Set the histograms entries according to the values of the configuration.
  if (histo) {
    for (int i=1; i<=histo->GetNbinsX(); i++) {
      for (int j=1; j<=histo->GetNbinsY(); j++) {
	int feN = mod->geometry().coord(i-1,j-1).chip();
	int col = mod->geometry().coord(i-1,j-1).col();
	int row = mod->geometry().coord(i-1,j-1).row();
	if (dumpMode==1) 
	  {
	    std::cout<<"Setting bin "<<i<<","<<j<<" to "<<allMap_trim[feN]->get(col,row)<< " which is saved in FE="<<feN<<" col/row="<<col<<"/"<<row<<std::endl;
	    histo->SetBinContent(i,j,allMap_trim[feN]->get(col,row));
	  }
	else {
	    std::cout<<"Setting bin "<<i<<","<<j<<" to "<<allMap_bool[feN]->get(col,row)<< " which is saved in FE="<<feN<<" col/row="<<col<<"/"<<row<<std::endl;
	    histo->SetBinContent(i,j,(int)allMap_bool[feN]->get(col,row));
	}
	  
      }//i loop
    }//j loop
    
  }// histo exists
  
  outputRootFile->cd();
  TDirectory* dir = outputRootFile->mkdir((modConn->name()).c_str());
  dir->cd();
  histo->Write();
}

void setPixDacFromHisto(ModuleConnectivity* modConn) {
  TH2* histo = nullptr;
  if (inputRootFile) {
    if (vmode == VERBOSE) 
      std::cout<<"Module:" << modConn->prodId() << " ( " <<modConn->name() << ")" << std::endl;
    histo = getHisto(modConn->name());
    std::cout<<histo<<std::endl;
  }
  else
    return;

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  std::vector<ConfMask<unsigned short int>*> allMap {static_cast<std::size_t>(nFe), nullptr};
  
  //Get the TDAC Values
  if(!checkFEFlavour(isFei3)) return;
  for (auto fe: *mod){
    allMap[fe->number()] = isFei3 ? &(static_cast<PixFeI3*>(fe)->readTrim(Fei3::pixreg_name_lookup(dacName))):
	                            &(static_cast<PixFeI4*>(fe)->readTrim(Fei4::pixreg_name_lookup(dacName))); 
  }
  
  //Set the TDAC values according to the histogram passed
  if (histo) {
    for (int i=1;i<=histo->GetNbinsX();i++)
      for (int j=1; j<=histo->GetNbinsY();j++)
	//For security reasons let's not set TDAC below 1
	if ((dacName=="TDAC" && histo->GetBinContent(i,j)>0) || (dacName=="FDAC") ){
	  int feN = mod->geometry().coord(i-1,j-1).chip();
	  int col = mod->geometry().coord(i-1,j-1).col();
	  int row = mod->geometry().coord(i-1,j-1).row();
	  if (vmode == VERBOSE) {
	    std::cout<<"found pixel to retune:" << i-1 << "  " << j-1<< "--->";
	    std::cout<<".. corresponds to: " <<feN <<"  "<<col<< " " << row <<std::endl; 
	  }//verbose
	  std::cout<<"col,row,FE,"<<dacName<<":"<<col<<","<<row<<","<<feN<<","<<allMap[feN]->get(col,row);
	  std::cout<<"---> new "<<dacName<<":"<<histo->GetBinContent(i,j)<<std::endl;
	  //Change the values
	  allMap[feN]->set(col,row,histo->GetBinContent(i,j));
	}
  }
  if(vmode == VERBOSE){
    std::cout << " Setting for module: " << modConn->name() << " prodId: " << modConn->prodId() << " " << dacName <<std::endl;
  }
  if(smode == SAVE) save(mod.get());
}

void pixDacShift(ModuleConnectivity* modConn) {
  if(vmode == VERBOSE) {
     printModInfo(modConn);
     std::cout << "-- Attempting to shift " << dacName << " by " << dacStep << '\n';
  }
  
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  if(!checkFEFlavour(isFei3)) return;
  for (auto fe: *mod){
    if (feI == -1 || fe->number() == feI){
      ConfMask<unsigned short int> &tdacs = isFei3 ? static_cast<PixFeI3*>(fe)->readTrim(Fei3::pixreg_name_lookup(dacName)):
	                                             static_cast<PixFeI4*>(fe)->readTrim(Fei4::pixreg_name_lookup(dacName));
      const auto maxValue = tdacs.maxVal();
      for (int col=0; col<tdacs.ncol(); col++) {
	for (int row=0; row<tdacs.nrow(); row++) {

	  const int old_value = tdacs.get(col,row);
	  int new_value = old_value + dacStep;

	  if(new_value > maxValue) {
	    std::cout << "--- " << dacName << " for pixel " << col << "," << row << " too large after increase. Would be " << new_value << ", setting to max value of " << maxValue << '\n';
	    new_value = maxValue;
	  } else if (new_value < 0) {
	    std::cout << "--- " << dacName << " for pixel " << col << "," << row << " too small after decrease. Would be " << new_value << ", setting to 0\n";
	    new_value = 0;
	  }

	  tdacs.set(col, row, new_value);
	  if (dacName == "TDAC" && tdacs.get(col,row) < 10) {
	    std::cout << "--- TDAC limited to 10 for module " << modConn->name() << " FE " << fe->number() << " pixel " << col << "," << row << std::endl;
	    tdacs.set(col, row, 10);
	  }
	  if (dacName == "TDAC" && tdacs.get(col,row) > 127) {
	    std::cout << "--- TDAC limited to 127 for module " << modConn->name() << " FE " << fe->number() << " pixel " << col << "," << row << std::endl;
	    tdacs.set(col, row, 127);
	  }
	}
      }
    }
  }  
  if(smode == SAVE) save(mod.get());
}

void setHitBusBit(ModuleConnectivity* modConn, bool enable) {
  if(vmode == VERBOSE){
    std::cout << "Module: " << modConn->prodId() << "  ( " << modConn->name() << " )" << std::endl;
    //    std::cout << " Working on conn tag: " << connTag << ",  cfgTag: " << cfgModTag << std::endl; 
  }
  
  if( modConn->name().find("LI_S") !=std::string::npos)enable = true;//ILEAK set to 1 for IBL means hitbus set to 0!
  if( modConn->name().find("LI_S15") !=std::string::npos)enable = false;//Default value for DBM
  
  TH2* histo = nullptr;
  if (inputRootFile) histo = getHisto(modConn->name());

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  std::vector<ConfMask<bool>*> ro_enable {static_cast<std::size_t>(nFe), nullptr};
  for (auto fe: *mod){
    if((modConn->name().find("LI_S") !=std::string::npos)) {
	    ro_enable [fe->number()] = &(static_cast<PixFeI4*>(fe)->readPixRegister(Fei4::PixReg::HitBus));
    } else {
	    ro_enable [fe->number()] = &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::PixReg::HitBus));
    }

    if (!inputRootFile) {
    if (enable) {
      if (feI!=-1) {
	if (fe->number()==feI) {
	  ro_enable[fe->number()]->enableAll();
	  std::cout << "Enabling all hitbus pixels in FE " << feI << std::endl;
	}
      } else{
	  ro_enable[fe->number()]->enableAll(); 
	  std::cout << "Enabling all hitbus pixels" << std::endl;
      }
    } else {
      if (feI!=-1) {
	if (fe->number()==feI) {
	  ro_enable[fe->number()]->disableAll();
	  std::cout << "Disabling all hitbus pixels in FE " << feI << std::endl;
	}
      } else {
	ro_enable[fe->number()]->disableAll(); 
	std::cout << "Disabling all hitbus pixels" << std::endl;
      }
    }
    } else {
    if (histo && ((modConn->name().find("LI_S15") !=std::string::npos) || (modConn->name().find("LI_S20") !=std::string::npos)) ) {
      int nPixSet =0;
      for (int i=1; i<=histo->GetNbinsX(); i++) {
	for (int j=1; j<=histo->GetNbinsY(); j++) {
	  if (histo->GetBinContent(i,j)!=0) {
	    int feN = mod->geometry().coord(i-1,j-1).chip();
	    int col = mod->geometry().coord(i-1,j-1).col();
	    int row = mod->geometry().coord(i-1,j-1).row();
	    //if (vmode == VERBOSE) {
	      //std::cout << "found hitbus pixel to mask: " << i-1 << "  " << j-1 << " --> ";
	      //std::cout << "... corresponds to: " << feN << "  "  << col << "  " << row << std::endl;
	    //}
	    nPixSet++;
	    ro_enable[feN]->enable(col,row);
	  }
	}
      }
     std::cout<<"Disabling HIT OR in "<<nPixSet<<std::endl;
    }
  }
  }
  if(smode == SAVE) save(mod.get());
}


void setReadOut(ModuleConnectivity* modConn, bool enable) {
  if (vmode == VERBOSE) std::cout << "Module: " << modConn->prodId() << "  ( " << modConn->name() << " )" << std::endl;

  TH2* histo = nullptr;
  if (inputRootFile) histo = getHisto(modConn->name());

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  std::vector<ConfMask<bool>*> allMap {static_cast<std::size_t>(nFe), nullptr};

  for(auto fe: *mod){
    allMap[fe->number()] = isFei3 ? &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::PixReg::Enable)):
	                            &(static_cast<PixFeI4*>(fe)->readPixRegister(Fei4::PixReg::Enable));

    if (!inputRootFile) {
      if (enable) {
	if (feI!=-1) {
	  if (fe->number()==feI) {
	    allMap[fe->number()]->enableAll();
	    if (vmode == VERBOSE) std::cout << "Enabling all pixels in FE " << feI << std::endl;
	  }
	} else{
	  allMap[fe->number()]->enableAll(); 
	  if (vmode == VERBOSE) std::cout << "Enabling all pixels" << std::endl;
	}
      } else {
	if (feI!=-1) {
	  if (fe->number()==feI) {
	    allMap[fe->number()]->disableAll();
	    if (vmode == VERBOSE) std::cout << "Disabling all pixels in FE " << feI << std::endl;
	  }
	} else {
	  allMap[fe->number()]->disableAll(); 
	  if (vmode == VERBOSE) std::cout << "Disabling all pixels" << std::endl;
	}
      }
    }
  }
  if (inputRootFile) {
    if (histo) {
      for (int i=1; i<=histo->GetNbinsX(); i++) {
	for (int j=1; j<=histo->GetNbinsY(); j++) {
	  if (histo->GetBinContent(i,j)!=0) {
	    int feN = mod->geometry().coord(i-1,j-1).chip();
	    int col = mod->geometry().coord(i-1,j-1).col();
	    int row = mod->geometry().coord(i-1,j-1).row();
	    if (vmode == VERBOSE) {
	      std::cout << "found pixel to mask: " << i-1 << "  " << j-1 << " --> ";
	      std::cout << "... corresponds to: " << feN << "  "  << col << "  " << row << std::endl;
	    }
	    allMap[feN]->disable(col,row);
	  }
	}
      }
    }
  }
  if(smode == SAVE) save(mod.get());
}

void setSafeCfg(ModuleConnectivity* modConn) {
  if (vmode == VERBOSE) std::cout << "Module: " << modConn->prodId() << "  ( " << modConn->name() << " )" << std::endl;

  bool isIBLDBM = false;
  bool isIBLOutermostA = false;
  bool isIBLOutermostC = false;
  if (modConn->name().substr(0,2).compare("LI") == 0) {
    isIBLDBM = true;
    if (modConn->name().substr(3,3).compare("S15") != 0) {
      if (modConn->name().substr(modConn->name().length()-2,2).compare("A8") == 0) isIBLOutermostA = true;
      if (modConn->name().substr(modConn->name().length()-2,2).compare("C8") == 0) isIBLOutermostC = true;
    }
  }
  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  std::vector<ConfMask<bool>*> allMap {static_cast<std::size_t>(nFe), nullptr};
  if (isIBLDBM) {
    // Setting threshold to maximum value
    std::string const vthinCoarse("Vthin_AltCoarse");
    constexpr int vthinCoarseValue = 255;
    std::string const vthinFine("Vthin_AltFine");
    constexpr int vthinFineValue = 255;
    // Turning second stage amp off
    std::string const amp2Vbn("Amp2Vbn");
    constexpr int amp2VbnValue = 0;
    // enable one double column of the outermost 3D FEs
    // and do not set maximum threshold for them so that
    // they can act as beam monitor in this low occupancy mode
    std::string const disableColumnCnfg0("DisableColumnCnfg0");
    std::string const disableColumnCnfg1("DisableColumnCnfg1");
    std::string const disableColumnCnfg2("DisableColumnCnfg2");
    constexpr int disableColumnCnfgValue01 = 0xFFFF;
    constexpr int disableColumnCnfgValue2 = 0xFF;
    constexpr int disableColumnCnfgValueEnableDC0 = 0xFFFE;
    constexpr int disableColumnCnfgValueEnableDC39 = 0x7F;
    for (int i=0;i<nFe;i++) {
    //Added by JuanAn, if isFile check the currentModule info
      if(isFile){
      auto it = currentModule.FE_DAC.find(i);
    	if (it == currentModule.FE_DAC.end())continue;
      
      dacValue = currentModule.FE_DAC[i];
      feI=i;
      }
      if (feI == -1 || feI == i) {
	Config &cfg = mod->pixFE(i)->config();
	Config *subcfg=0;
	for (unsigned int j=0; j<cfg.subConfigSize(); j++) {
	  if (cfg.subConfig(j).name()=="GlobalRegister_0") {
	    subcfg = &(cfg.subConfig(j));
	    break;
	  }	  
	}
	if (subcfg==0) {
	  std::cerr << "FE " << i << " had no globreg config" << std::endl;
	} else {
	  Config &globreg = *subcfg;
	  if ((isIBLOutermostA && i==1) || (isIBLOutermostC && i==0)) {
	    if (globreg["GlobalRegister"][disableColumnCnfg0].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][disableColumnCnfg0]).m_value) = (isIBLOutermostC) ? disableColumnCnfgValueEnableDC0 : disableColumnCnfgValue01;
	    }
	    else {
	      std::cerr << "DAC " << disableColumnCnfg0 << " not in global register" << std::endl; 
	    }
	    if (globreg["GlobalRegister"][disableColumnCnfg1].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][disableColumnCnfg1]).m_value) = disableColumnCnfgValue01;
	    }
	    else {
	      std::cerr << "DAC " << disableColumnCnfg1 << " not in global register" << std::endl; 
	    }
	    if (globreg["GlobalRegister"][disableColumnCnfg2].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][disableColumnCnfg2]).m_value) = (isIBLOutermostA) ? disableColumnCnfgValueEnableDC39 : disableColumnCnfgValue2;
	    }
	    else {
	      std::cerr << "DAC " << disableColumnCnfg2 << " not in global register" << std::endl; 
	    }
	  }
	  else {
	    if (globreg["GlobalRegister"][vthinCoarse].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][vthinCoarse]).m_value) = vthinCoarseValue;
	    }
	    else {
	      std::cerr << "DAC " << vthinCoarse << " not in global register" << std::endl; 
	    }
	    if (globreg["GlobalRegister"][vthinFine].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][vthinFine]).m_value) = vthinFineValue;
	    }
	    else {
	      std::cerr << "DAC " << vthinFine << " not in global register" << std::endl; 
	    }
	    if (globreg["GlobalRegister"][amp2Vbn].name() != "__TrashConfObj__") {
	      *((int *)((ConfInt&)globreg["GlobalRegister"][amp2Vbn]).m_value) = amp2VbnValue;
	    }
	    else {
	      std::cerr << "DAC " << amp2Vbn << " not in global register" << std::endl; 
	    }
	  }
	}
      }
    }
  }
  else {
  TH2* histo = 0;
  if (inputRootFile) histo = getHisto(modConn->name());
  
    if(!inputRootFile){
    
    for (auto fe: *mod){
      allMap[fe->number()] = &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::PixReg::Preamp));
        if (feI!=-1) {
	  if (fe->number()==feI) {
	    allMap[fe->number()]->disableAll();
	    if (vmode == VERBOSE) std::cout << "Disabling all preamps in FE " << feI << std::endl;
	  }
        } else {
	  allMap[fe->number()]->disableAll(); 
	  if (vmode == VERBOSE) std::cout << "Disabling all preamps" << std::endl;
        }
    }
    
    } else {
    int nPixSB =0;
    int nPixReady = 0;
    if(histo){
    for (auto fe: *mod)
      allMap[fe->number()] = &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::PixReg::Preamp));
        for (int i=1; i<=histo->GetNbinsX(); i++) {
	  for (int j=1; j<=histo->GetNbinsY(); j++) {
	     int feN = mod->geometry().coord(i-1,j-1).chip();
	     int col = mod->geometry().coord(i-1,j-1).col();
	     int row = mod->geometry().coord(i-1,j-1).row();
	     /*if (vmode == VERBOSE && histo->GetBinContent(i,j)==1) {
	        std::cout << "found pixel to mask: " << i-1 << "  " << j-1 << " --> ";
	        std::cout << "... corresponds to: " << feN << "  "  << col << "  " << row << std::endl;
	      }*/
	      if (histo->GetBinContent(i,j)==0){
		allMap[feN]->disable(col,row);
	        nPixSB++;
		}
	      else {
		allMap[feN]->enable(col,row);
		nPixReady++;
	      }
	  }
        }
	std::cout<<"NPixel in stand-by "<<nPixSB<<std::endl;
	std::cout<<"NPixel in READY "<<nPixReady<<std::endl;
      }
    }
  }
  if(smode == SAVE) save(mod.get());
}



void appMask(ModuleConnectivity* modConn, bool enable) {
  TH2* histo = 0;
  if (inputRootFile) {
    if (vmode == VERBOSE) std::cout << "Module: " << modConn->prodId() << "  ( " << modConn->name() << " )" << std::endl;
    histo = getHisto(modConn->name()); 
  }

  auto [feFlv, nFe, mod] = setupPixModule(modConn);
  bool isFei3 = (feFlv == EnumFEflavour::PM_FE_I2);
  std::vector<ConfMask<bool>*> allMap {static_cast<std::size_t>(nFe), nullptr};

  for(auto fe: *mod){
    allMap[fe->number()] = isFei3 ? &(static_cast<PixFeI3*>(fe)->readPixRegister(Fei3::PixReg::Enable)):
	                            &(static_cast<PixFeI4*>(fe)->readPixRegister(Fei4::PixReg::Enable));
    allMap[fe->number()]->enableAll();
  }
  if (histo) {
    for (int i=1; i<=histo->GetNbinsX(); i++) {
      for (int j=1; j<=histo->GetNbinsY(); j++) {
	if (histo->GetBinContent(i,j)!=0) {
	  int feN = mod->geometry().coord(i-1,j-1).chip();
	  int col = mod->geometry().coord(i-1,j-1).col();
	  int row = mod->geometry().coord(i-1,j-1).row();
	  if (vmode == VERBOSE) {
	    std::cout << "found pixel to mask: " << i-1 << "  " << j-1 << " --> ";
	    std::cout << "... corresponds to: " << feN << "  "  << col << "  " << row << std::endl;
	  }
	  allMap[feN]->disable(col,row);
	}
      }
    }
  }
  if(smode == SAVE) save(mod.get());
}

void setTimDelay(std::string nam, int timDelArg) {
  if (timDelArg < 0 || timDelArg >255) {
    std::cout << "Invalid TIM delay specified, stopping" << std::endl;
    return;
  }

  TimPixTrigController* timC = new TimPixTrigController(nam);
  timC->readConfig(DbServer, domain, cfgTag);
  timC->setTriggerDelay(timDelArg);
  if(smode == SAVE) {
    if(pendTag == "") timC->writeConfig(DbServer, domain, cfgTag);
    else timC->writeConfig(DbServer, domain, cfgTag, pendTag);
  }
  if(vmode == VERBOSE) std::cout << " TIM " << nam << " trigger delay has been changed to " << timDelArg << std::endl;
}


PixLib::CtrlType getCtrlType(std::string ctrlType){
  std::map<std::string, PixLib::CtrlType> ctrltypeMap;
  ctrltypeMap["DUMMY"]  = DUMMY;
  ctrltypeMap["ROD"]    = ROD;
  ctrltypeMap["IBLROD"]   = IBLROD;
  ctrltypeMap["CPPROD"]   = CPPROD;
  ctrltypeMap["L12ROD"]   = L12ROD;
  return ctrltypeMap[ctrlType];
}

void setTxDelay(RodBocConnectivity* rod) {
  static bool readFile = true;
  static std::map<std::string, std::pair<int,int>> delays;
  //static std::map<std::string, unsigned int> delo;
  if (!txdelayFileName.empty()) {
    if (readFile) {
      std::ifstream txDel = std::ifstream(txdelayFileName);
      while (!txDel.eof()) {
	std::string name;
	int coarseDel, fineDel;
	txDel >> name >> coarseDel >> fineDel;
	if (!txDel.eof()) {
	  delays[name] = std::make_pair(coarseDel, fineDel);
	}
      }
      readFile = false;
    }
    std::cout << "Rod: " << rod->name() << std::endl;
    auto tmpModGrp = PixModuleGroup(rod->name(), getCtrlType(rod->rodBocType()));
    bool result= tmpModGrp.readConfig(DbServer,domain,cfgTag);
    if (!result) {
      std::cout << "Problem in getting PixModuleGroup " << rod->name() << " from DbServer " << std::endl;
    } else {
      PixBoc *tmpBoc = tmpModGrp.getPixBoc();
      bool modif = false;
      int obslot;
      for (obslot = 0; obslot < 4; obslot++){
	// tmp for the absence of cooling loops
	OBConnectivity* obConn = NULL;
	obConn = rod->obs(obslot);
	if (obConn != NULL){
	  if (obConn->pp0() != NULL) {
	    int moduleslot;
	    for (moduleslot = 1; moduleslot < 9; moduleslot++) {
	      ModuleConnectivity* modConn = NULL;
	      modConn = obConn->pp0()->modules(moduleslot);
	      if (modConn != NULL) {
		std::map<std::string, std::pair<int,int>>::iterator it = delays.find(modConn->name());
		if (it != delays.end()) {
		  int plugin = modConn->bocLinkTx()/10;
		  int ch = modConn->bocLinkTx()%10;
		  if(modConn->name().find("LI")!=std::string::npos && plugin>1) ch = 7-ch;//IBL A side !!!???
		  std::cout << " Plugin " << plugin << " Ch " << ch << std::endl;
		  std::pair <int, int> del = it->second; 
		  tmpBoc->getTx(plugin)->setTxCoarseDelay(ch, del.first);
		  tmpBoc->getTx(plugin)->setTxFineDelay(ch, del.second);
		  std::cout << "  Module: " << modConn->name() << " setting delay to: Coarse "<<del.first<<" Fine " << del.second<<std::endl;
		  modif = true;
                  //delays[modConn->name()] = 1000;
		} else {
		  std::cout << "  Module: " << modConn->name() << " NOT FOUND in TX delay file" << std::endl;
		}
	      }
	    }
	  }
	}
      }
      if(smode == SAVE && modif) {
	std::cout << "Saving configuration for ModGroup " << rod->name() << std::endl; 
        save(tmpModGrp);	
      }
    }
    //std::map<std::string, unsigned int>::iterator it;
    //for (it=delays.begin(); it!=delays.end(); it++) {
    //  if (it->second != 1000) {
    //    std::cout << it->first << "\t" << it->second << std::endl;
    //  }
    //}
  }
}

std::string getRodNameFromIP(std::string ip){

std::string rodN,slot;
int slotN;
    if(ip.find("10.145.89.") !=string::npos){rodN="ROD_I1_S";slot = ip.substr (11,2);slotN = atoi(slot.c_str());}
    else if(ip.find("10.145.97.") !=string::npos){rodN="ROD_L1_S";slot = ip.substr (11,2);slotN = atoi(slot.c_str());}
    else if(ip.find("10.145.98.") !=string::npos){rodN="ROD_L2_S";slot = ip.substr (11,2);slotN = atoi(slot.c_str());}
    else if(ip.find("10.145.99.") !=string::npos){rodN="ROD_L3_S";slot = ip.substr (11,2);slotN = atoi(slot.c_str());}
    else if(ip.find("10.145.100.") !=string::npos){rodN="ROD_L4_S";slot = ip.substr (12,2);slotN = atoi(slot.c_str());}
    else if(ip.find("10.145.104.") !=string::npos){
    	slot = ip.substr (12,2);
    	slotN = atoi(slot.c_str());
    		if(slotN>21){rodN="ROD_B3_S";slotN -=30;}
    		else rodN="ROD_B2_S";
    }
    else if(ip.find("10.145.105.") !=string::npos){
    	slot = ip.substr (12,2);slotN = atoi(slot.c_str());
    	if(slotN>21){rodN="ROD_D2_S";slotN -=30;}
    	else rodN="ROD_D1_S";
    } else if(ip.find("192.168.") !=string::npos){
    	std::string crate=ip.substr (8,2); int crateN = atoi(crate.c_str()); crateN -=10;
    	slot = ip.substr (12,2);slotN = atoi(slot.c_str());
    	rodN="ROD_C"+std::to_string(crateN) +"_S";
    } else {std::cout<<"Unknown crate"<<std::endl; return " ";}

    rodN += std::to_string(slotN);
return rodN;
}

int getBocInputLinkFromTxCh(int txCh){
int fibreNr = 2 + txCh%8 + 12 * (txCh/8);
int bocInputLink =0;

switch(fibreNr/12) {
case 0: bocInputLink = 39 - fibreNr;break;
case 1: bocInputLink = 41 - fibreNr;break;
case 2: bocInputLink = 33 - fibreNr;break;
case 3: bocInputLink = 55 - fibreNr;break;
}
//std::cout<<"TxCh "<<txCh<<" Fibre "<<fibreNr<<" BocInputLink "<<bocInputLink<< std::endl;

return bocInputLink;

}

void setTxDelayFromBOC(RodBocConnectivity* rod) {

  std::cout << "Rod: " << rod->name() << std::endl;
  
    auto tmpModGrp = PixModuleGroup(rod->name(), getCtrlType(rod->rodBocType()));
    bool result = tmpModGrp.readConfig(DbServer,domain,cfgTag);
    if (!result) {
      std::cout << "Problem in getting PixModuleGroup " << rod->name() << " from DbServer " << std::endl;
      return;
    }
    PixBoc *tmpBoc = tmpModGrp.getPixBoc();
    bool modif = false;
  
  if (txdelayFileName.empty()){std::cout<<"No Tx delays file specified"<<std::endl; return;}
  std::ifstream txDel = std::ifstream(txdelayFileName);
      while (!txDel.eof()) {
	std::string ip;
	int coarseDel, fineDel,txCh;
	txDel >> ip >> txCh >> coarseDel >> fineDel;
	if (!txDel.eof()) {
	  std::string rodN = getRodNameFromIP(ip);
	  //std::cout<<ip<<" "<<getRodNameFromIP(ip)<<std::endl;
	  if(rodN != rod->name())continue;
	  int ch, plugin;
	  
	  
	  int bocInputLink = getBocInputLinkFromTxCh(txCh);
	  plugin = bocInputLink/10;
	  ch = bocInputLink%10;
	  
	  tmpBoc->getTx(plugin)->setTxCoarseDelay(ch, coarseDel);
	  tmpBoc->getTx(plugin)->setTxFineDelay(ch, fineDel);
	  std::cout << "  Rod: " <<rodN <<" txCh "<<txCh<<" Plugin "<<plugin<<" Ch "<< ch <<" setting delay to: Coarse "<<coarseDel<<" Fine " << fineDel<<std::endl;
	  modif = true;
	  
	  }
      }
      if(smode == SAVE && modif) {
	std::cout << "Saving configuration for ModGroup " << rod->name() << std::endl; 
        save(tmpModGrp);	
      }
}

void setVISet(RodBocConnectivity* rod) {

    std::cout << "Rod: " << rod->name() << std::endl;
    auto tmpModGrp= PixModuleGroup(rod->name(), getCtrlType(rod->rodBocType()));
    bool result = tmpModGrp.readConfig(DbServer,domain,cfgTag);
    if (!result) {
      std::cout << "Problem in getting PixModuleGroup " << rod->name()<< " from DbServer " << std::endl;
    } else {
      PixBoc *tmpBoc = tmpModGrp.getPixBoc();
      if(tmpBoc==NULL){std::cout << "Problem in getting PixBOC " << rod->name() << " from DbServer " << std::endl;return;}
      bool modif = false;
      int obslot;
      for (obslot = 0; obslot < 4; obslot++){
	// tmp for the absence of cooling loops
	OBConnectivity* obConn = NULL;
	obConn = rod->obs(obslot);
	if (obConn != NULL){
	  if (obConn->pp0() != NULL)
	  if(pp0Name == "all pp0s" || pp0Name == obConn->pp0()->name()){
	    //std::cout<<"Working on PP0 "<<obConn->pp0()->name()<<std::endl;
	    int moduleslot;
	    for (moduleslot = 1; moduleslot < 9; moduleslot++) {
	      ModuleConnectivity* modConn = NULL;
	      modConn = obConn->pp0()->modules(moduleslot);
	      std::cout << "  PP0: " << obConn->pp0()->name() << " setting VISET " << VISet;
	      if (modConn != NULL) {
		if( modConn->bocLinkRx(1)==-1)break;
		int plugin = modConn->bocLinkRx(1)/10;
		std::cout << " Plugin " << plugin;
		tmpBoc->getRx(plugin)->setViset(VISet);
		  if( modConn->bocLinkRx(2)!=-1){
		  plugin = modConn->bocLinkRx(2)/10;
		  std::cout << " Plugin " << plugin;
		  tmpBoc->getRx(plugin)->setViset(VISet);
		  }
	      	std::cout<<std::endl;
	      	modif = true;
	      }
	    }
	  }
	}
      }
      if(smode == SAVE && modif) {
	std::cout << "Saving configuration for ModGroup " << rod->name() << std::endl; 
        save(tmpModGrp);	
      }
    }
}

void setRxPhases(RodBocConnectivity* rod) {
  std::string line,rodN;
  bool modif = false;
  int rxCh=-1,phase=-1;

  std::cout<<__PRETTY_FUNCTION__ << "Rod: " << rod->name() << std::endl;
  auto tmpModGrp = PixModuleGroup(rod->name(), getCtrlType(rod->rodBocType()));
  bool result = tmpModGrp.readConfig(DbServer,domain,cfgTag);
    if (!result) {
      std::cout << "Problem in getting PixModuleGroup " << rod->name()<< " from DbServer " << std::endl;
      return;
    } 
      PixBoc *tmpBoc = tmpModGrp.getPixBoc();

   if(tmpBoc==NULL){std::cout << "Problem in getting PixBOC " << rod->name() << " from DbServer " << std::endl;return;}
  
  if ( rxPhasesFileName == ""){std::cout << "No file name specified " << std::endl;return;}
  ifstream inFile (rxPhasesFileName.c_str());//File generated from ReadCdrPhases!!!
  if (!inFile){std::cout << "Cannot open "<<rxPhasesFileName << std::endl;return;}
  
    while(getline(inFile, line)){
      /*std::size_t found=line.find("BOC");
      //std::cout<<line<<std::endl;
      if(found==std::string::npos)continue;
      std::string subs = line.substr(found);
      std::regex regEx("BOC: ([^ ]*), rxCh=([^ ]*), cdrPhase=([^ ]*)");
      std::smatch match;
      bool success = boost::std_match(subs, match, regEx);
      if (!success){std::cout<<"Regex didn't match"<<std::endl;continue;}
      std::string ip(match[1].first, match[1].second);
      std::string sRxCh(match[2].first, match[2].second);
      std::string sPhase(match[3].first, match[3].second);

      rxCh = atoi(sRxCh.c_str());
      phase = atoi(sPhase.c_str());

      //std::cout<<"IP "<<ip<<" rxCh "<<rxCh<<" Phase "<<phase<<std::endl;
      std::string slot ="";
      if(ip.find("10.145.87.") != std::string::npos ){slot = ip.substr (11,2);rodN="ROD_I1_S";}//IBL/DBM
      else if (ip.find("192.168.13.") != std::string::npos ){slot = ip.substr (12,2);rodN="ROD_C3_S";}//SR1
      else{std::cout<<"IP "<<ip<<" now known "<<std::endl; continue;}
      int slotN = atoi(slot.c_str());
      rodN += std::to_string(slotN);*/
      //std::cout<<"RodName "<<rodN<<std::endl;
      std::stringstream ss;
      ss<<line;
      if(!(ss>>rodN>>rxCh>>phase))continue;
      //std::cout<<" "<<rodN<<" "<<rxCh<<" "<<phase<<std::endl;
      if(rxCh<0||rxCh>31)continue;
      
      if(rod->name() != rodN)continue;

      //std::cout<<rodN<<" "<<rod->name()<<std::endl;
      int dto = IblPixBoc::getDtoFromRxCh(rxCh);
      tmpBoc->getRx(dto/10)->setRxPhase(dto%10, phase);
      modif = true;
      std::cout<<rodN<<" RxCh "<<rxCh << " Plugin " << dto/10 << " DTO " << dto <<" Phase "<<phase<< " Readback "<<(int)tmpBoc->getRx(dto/10)->getRxPhase(dto%10) <<std::endl;

      }
      inFile.close();
      if(smode == SAVE && modif) {
	std::cout << "Saving configuration for ModGroup " << rod->name() << std::endl; 
        save(tmpModGrp);
      }
}

void getRxPhases(RodBocConnectivity* rod) {
  auto tmpModGrp= PixModuleGroup(rod->name(), getCtrlType(rod->rodBocType()));
  bool result=tmpModGrp.readConfig(DbServer,domain,cfgTag);
  if (!result) {
    std::cout << "Problem in getting PixModuleGroup " << rod->name()<< " from DbServer " << std::endl;
    return;
  } 
  PixBoc *tmpBoc = tmpModGrp.getPixBoc();
  if(!tmpBoc){
    std::cout << "Problem in getting PixBOC " << rodName << " from DbServer " << std::endl;
    return;
  }
  for(int plugin =0;plugin<4;plugin++)
    for(int ch=0;ch<8;ch++)std::cout<<" rxCh "<<IblPixBoc::getRxChFromDto(plugin*10+ch)<<" BocOutputLink "<<plugin*10+ch<<" phase "<<(int)tmpBoc->getRx(plugin)->getRxPhase(ch)<<std::endl;  
}

int main(int argc, char** argv) {
  std::string dbx = "";
  try {
    if(!getParams(argc, argv)) {
	    return -1;
    }
    if (mode == HELP) {
      std::cout << std::endl;
      std::cout << "\n\nUsage: ChangeCfg --setFeMaskOff --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set all fe to disable status (optional --feNumber disables only one FE)\n" 
		<< "Usage: ChangeCfg --setFeMaskOn  --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set all fe to enable status (optional --feNumber disables only one FE)\n" 
		<< "Usage: ChangeCfg --getFeMaskOff --connTag <conn tag> --cfgTag <cfg tag> --cfgModTag\n"
		<< "                                 // get list of disabled FEs\n"
		<< "Usage: ChangeCfg --increaseGlobDac <DAC> <step> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // increase specified global DAC by step\n"
		<< "Usage: ChangeCfg --decreaseGlobDac <DAC> <step> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // decrease specified global DAC by step\n"
		<< "Usage: ChangeCfg --setGlobDac <DAC> <value> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set specified global DAC to given value (optional --feNumber only acts on one FE)\n"
		<< "Usage: ChangeCfg --increasePixDac <DAC> <step> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // increase specified pixel DAC by step\n"
		<< "Usage: ChangeCfg --decreasePixDac <DAC> <step> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // decrease specified pixel DAC by step\n"
		<< "Usage: ChangeCfg --setPixDac <DAC> <value> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set specified pixel DAC to given value (optional --feNumber only acts on one FE)\n"
		<< "Usage: ChangeCfg --setPixDacFromHisto <FILENAME> <DAC>  --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set specified pixel DAC to value stored in histograms in <FILENAME>\n"
		<< "Usage: ChangeCfg --setPixReg <REG> <value> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set specified pixel REG to given value (optional --feNumber only acts on one FE)\n"
		<< "                                 // Possible registers: CAP0, CAP1, ILEAK (ENABLE also supported, but use enaPix instead)\n "
		<< "                                 // switch on  laser for selected BOC \n"
		<< "Usage: ChangeCfg --enaPix --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // enable readout for all pixels  \n"
		<< "Usage: ChangeCfg --disPix (rootFileName optional) --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // disable readout for all pixels \n"
		<< "Usage: ChangeCfg --setSafeCfg --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // disable preamps for pixel, and switch off 2nd stage amplifier and set maximum GDAC for IBL\n"
		<< "Usage: ChangeCfg --appMask rootFileName --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // apply the mask written in the rootFileName \n"
		<< "Usage: ChangeCfg --setTxDelay delayFileName --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // apply the TX delay from delayFileName \n"
		<< "Usage: ChangeCfg --setTimDelay <value> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set internal TIM trigger delay (0..255) \n"
		<< "Usage: ChangeCfg --setT0 <CAL_Delay, CAL_Range or DELAY_X> <value> --connTag <conn tag> --cfgTag <cfg tag>\n"
		<< "                                 // set specified MCC register to given value \n"
		<< "\n"
		<< "Optional switches (some are MANDATORY so read carefully): \n"
		<< "IMPORTANT!! --cfgModTag <cfgModTag> Specify Module cfg Tag if different from cfgTag \n"  
		<< " \n"
		<< "            --dbPart <partition> IPC Part name (def. PixelInfr)\n"
	        << "            --dbName <name>      DbServer name (def. PixelDbServer)\n"
		<< "            --idTag  <idtag>     Db idTag [from it also the domain] (def. PIXEL16)\n"
		<< "            --pendTag <name>     pending tag for DbServer (default _Tmp) \n"
		<< " \n"
     	        << "            --clean              less output to screen (default is verbose mode)\n"
	        << "            --save               save changes in DbServer with selected pendTag (def. NoSave mode [does not do anything])\n"
		<< " \n"
		<< "            --partitionName <name>  Specify a partition explicitly (def. all)\n"
	        << "            --crateName <name>      Specify a crate explicitly     (def. all)\n"
	        << "            --rodName <name>        Specify a rod explicitly       (def. all)\n"
	        << "            --pp0Name <name>        Specify a pp0 explicitly       (def. all)\n"
	        << "            --clName <name>         Specify a cool loop explicitly (def. all)\n"
	        << "            --moduleName <name>     Specify a module explicitly    (def. all)\n"
		<< "            --feNumber <number>     Specify a frontEnd number      (def. all)\n"
		<< "\n\n"; 
      return -1;
    }
    timeElapsed=gettimeofday(&ti,NULL);
    // open noise mask input root file
    if (inputRootFileName != "") {
      inputRootFile = new TFile(inputRootFileName.c_str(),"READ");
      if (inputRootFile->IsZombie()) {
	std::cout << " Cannot open inputRootFile:  " << inputRootFileName << std::endl;
	exit(-1);
      }
    }

    //open output root file for storing the dump
    if (!outputRootFileName.empty()) {
      outputRootFile = new TFile(outputRootFileName.c_str(),"RECREATE");
      if (!outputRootFile || outputRootFile->IsZombie()) {
	std::cout << " Cannot open output ROOT file:  " << outputRootFileName << std::endl;
	return -1;
      }
    }

    // Open DB server connection
    if (!DbServer) openDbServer(argc, argv);
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag,idTag);
    conn->loadConn();

    std::vector<std::string> tagCfglist;
    DbServer->listTagsRem("Configuration-"+idTag,tagCfglist);
    bool foundCfg=false;
    bool foundCfgMod=false;
    for (unsigned int i=0; i<tagCfglist.size(); i++) {
      if (tagCfglist[i]==cfgTag) foundCfg=true;
      if (tagCfglist[i]==cfgModTag) foundCfgMod=true;
    }
    if (!foundCfg) {
      std::cout << std::endl << "FATAL: cfgTag " << cfgTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    if (!foundCfgMod) {
      std::cout << std::endl << "FATAL: cfgModTag " << cfgModTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    
    if(vmode == VERBOSE) printTagInfo();

    // loop over partitions
    for (auto& [partitionKey, partition]: conn->parts) {
      if(noIbl == true && partition->name() == "PIXEL_I") continue;
      if(partition->name() == partitionName || partitionName == "all partitions"){
	if(vmode == VERBOSE){
	  std::cout << "[Working on partition: " << partition->name() << "]\n";
	}
	for (auto& [rodcrateKey, rodcrate]: partition->rodCrates()){
	  if(rodcrate->name() == crateName || crateName == "all crates"){	
	    if(vmode == VERBOSE){
	      std::cout << "[Working on crate: " << rodcrate->name() << "]\n"; 
	    }
	    if (mode == TIM_DELAY) {
	      TimConnectivity* timConn = rodcrate->tim();
	      if (timConn) setTimDelay(rodcrate->tim()->name(), timDel);
	      continue;
	    } 
	    for (auto & [rodbocKey, rodboc]: rodcrate->rodBocs()){
	      if(rodboc->name() == rodName || rodName == "all rods"){	
		if(vmode == VERBOSE){
		  std::cout << "[Working on RodBoc: " << rodboc->name() << "]\n";
		}
		if (mode == TX_DELAY){
		  setTxDelay(rodboc); 		      
		} else if (mode == SET_TX_DELAYS_FROM_BOC){
		  setTxDelayFromBOC(rodboc); 		      
		} else if (mode == SET_RX_PHASES){
		  setRxPhases(rodboc); 		      
		} else if (mode == GET_RX_PHASES){
		  getRxPhases(rodboc); 		      
		} else if (mode == VISET){
		  setVISet(rodboc); 		      
		}
		else {
		  int obslot;
		  for (obslot = 0; obslot < 4; obslot++){
		    // tmp for the absence of cooling loops
		    OBConnectivity *obConn = rodboc->obs(obslot);
		    if(obConn){
		      if(obConn->pp0()->name() == pp0Name || pp0Name == "all pp0s"){
			if(obConn->pp0()->coolingLoop() == clName || clName == "all cls"){		    
			  if(vmode == VERBOSE){
			    std::cout << "[Working on pp0: " << pp0Name << " on cooling loop: " << obConn->pp0()->coolingLoop() << "]\n";
			  }
			    for(int moduleslot = 1; moduleslot < 9; moduleslot++){
			      ModuleConnectivity* modConn = obConn->pp0()->modules(moduleslot);
			      if(modConn){
			      //Added by JuanAn check the variables from the file, if not continue;
			       if(isFile){
			         if(mode==PIXDACSET||mode==GLOBDACSET){
			           if(!setVarFromFile(modConn->name()))continue;
			         }
			         else {
			           std::cout << "Wrong mode " << mode << " to use the readFile option\n";
			           break;
			         }
			       }
				if(modConn->name() == moduleName || moduleName == "all modules"){
				  if (mode == ALLBUTONEFE){
				    setFeMaskOff(modConn);
				  }
				  if (mode == ALLFE){
				    setFeMaskOn(modConn);
				  }
				  if (mode == GETALLFEOFF){
				    getFeMaskOff(modConn);
				  }
				  if (mode == HITBUSOFF){
				    setHitBusBit(modConn, false);
				  } 
				  if (mode == ENABLE){
				    setReadOut(modConn, true);
				  } 
				  if (mode == DISABLE){
				    setReadOut(modConn, false);
				  }
				  if (mode == SETSAFECFG){
				    setSafeCfg(modConn);
				  } 
				  if (mode == NOISE_MASK){
				    appMask(modConn, false);
				  } 		      
				  if (mode == SETPIXDAC_FROMHISTO) {
				    setPixDacFromHisto(modConn);
				  }
				  if (mode == DUMPPIXDAC_TOHISTO) {
				    dumpPixDacToHisto(modConn);
				  }
				  if (mode == GLOBDACSHIFT){
				    globDacShift(modConn);
				  }
				  if (mode == GLOBDACSET){
				    globDacSet(modConn);
				  }
				  if (mode == T0SET){
				    t0Set(modConn);
				  }
				  if (mode == PIXDACSHIFT){
				    if(!(dacName == "TDAC" || dacName == "FDAC")) {
					    std::cout << "--increasePixDac and --decreasePixDac can only in/decrease TDAC and FDAC\n";
					    return -1;
				    }
      				     pixDacShift(modConn);
				  }
				  if (mode == PIXDACSET){
				    if(!(dacName == "TDAC" || dacName == "FDAC")) {
					    std::cout << "To set " << dacName << " use --setPixReg instead of --setPixDac\n";
					    return -1;
				    }
				    pixDacSet(modConn);
				  }	
				  if (mode == PIXREGSET) {
				    if(dacName == "TDAC" || dacName == "FDAC") {
					    std::cout << "To set " << dacName << " use --setPixDac instead of --setPixReg\n";
					    return -1;
				    }
				    if ( dacValue > 1) {
					    std::cout << "Register " << dacName << " can only be set to 0/1\n";
				    }
				    pixRegSet(modConn);
				  }
				  if (mode == ACTIVEI32){
				    activeI32(modConn);
				  }	
				}
			      }	
			    }	
			}
		      }
		    }
		  }   		
		}
	      }	    
	    }
	  }
	}
      }    
    }
    // close noise mask input root file
    if (inputRootFile) {
      inputRootFile->Close();
      delete inputRootFile;
    }
    timeElapsed=gettimeofday(&tf,NULL);  
    std::cout << std::endl << std::endl <<  "Time for this operation is:  " <<tf.tv_sec - ti.tv_sec+ 1e-6*(tf.tv_usec-ti.tv_usec) << " s " << std::endl << std::endl;
  }
  catch(PixDBException& e) {
    std::cerr<<"Got PixDBException: ";
    e.what(std::cerr);
    std::cerr<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
  //Close the outputFile
  if(outputRootFile){outputRootFile->Close();}
  return 0;
}
