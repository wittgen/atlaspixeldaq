//////////////////////////////////////////////////////////////////////////
//
//GetVrefDigDACFromRootFile, write in a file the VrefDigVal for a given vSet
//from the TGraphs in a root file that contains the LV digital cal from production
//
// Author: JuanAn xx/03/2016 
//
//////////////////////////////////////////////////////////////////////////


#include "TFile.h"
#include "TKey.h"
#include "TTree.h"
#include "TGraph.h"
#include "TF1.h"
#include "TString.h"

#include <iostream>
#include <fstream>

//This function returns the DAC corresponding with the interpolated value of the TGraph for a given vSet
int getDAC(TGraph *gr,double vSet){

TGraph *evalGraph = new TGraph(gr->GetN(),gr->GetY(),gr->GetX());

double DAC=evalGraph->Eval(vSet);

//DAC should be between 0 and 255
if(DAC>255)return 255;
if(DAC<0)return 0;

return (int)floor(DAC+0.5);

}

//This function returns the DAC corresponding with the fitting of the TGraph for a given vSet
int getDACFitting(TGraph *gr,double vSet){

TGraph *evalGraph = new TGraph(gr->GetN(),gr->GetY(),gr->GetX());

TF1 f1("f1","[0]+[1]*x",0,255);

evalGraph->Fit("f1","Q");

double DAC= f1.Eval(vSet);

//DAC should be between 0 and 255
if(DAC>255)return 255;
if(DAC<0)return 0;

return (int)floor(DAC+0.5);

}

void printHelp(){
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
std::cout<<"This program generate a text file with the DAC value for a given VrefDigTune voltaje"<<"\n"<<"the user has to provide a root file with the TGraphs of the different modules, named by module name LI_SXX_X_XX_XX"<<"\n"<<std::endl;
std::cout<<"------------------------------------------------------------------------------------"<<std::endl;
std::cout<<"Please enter all parameters---> Method: GetVrefDigDACFromRootFile --f fileName --v vSet --o outputFile "<<std::endl;

exit(0);
}

int main(int argc, const char** argv) {

//default args
double vSet =-1;
std::string fout =" ";
std::string fileName =" ";

        if(argc>=2)
        {
                for(int i = 1; i < argc; i++)
                        if( *argv[i] == '-')
                        {
                                argv[i]++;
                                if( *argv[i] == '-') argv[i]++;
                                {
                                        switch ( *argv[i] )
                                        {
                                                case 'v' : vSet= atof(argv[i+1]); break;
                                                case 'f' : fileName=argv[i+1]; break;
                                                case 'o' : fout=argv[i+1]; break;
                                                case 'h' : printHelp( ); break;
                                                default : printHelp( ); return 0;
                                        }
                                }
                        }
        }

        if(fout==" "||fileName==" "||vSet==-1){
        printHelp();
        }

//Open output file
std::ofstream fOut(fout.c_str());

//Open root file
TFile *fIn= TFile::Open(fileName.c_str());
TIter next(fIn->GetListOfKeys());
TKey *key;

TGraph *gr;

int FENumber=-1;
   
   //Bucle over TFile elements
   while ((key=(TKey*)next())) {
   
   if(strcmp(key->GetClassName(),"TGraph")!=0)continue;
   
   //Gets the TGraph
   gr = (TGraph *)fIn->Get(key->GetName());
   
   //DAC is obtained by a fitting of the TGraph
   int DAC=getDACFitting(gr,vSet);
   
   //Get the module name
   TString moduleName(key->GetName());
   
   //Get FE number
   TString FE(moduleName(moduleName.Sizeof()-2,moduleName.Sizeof()));
   
   //Get module short name
   TString name(moduleName(0,moduleName.Sizeof()-5));
   
   std::cout<<moduleName.Data()<<"\t"<<name.Data()<<"\t"<<FE.Data()<<"\t"<<DAC<<std::endl;
   
   //Write in output file
   fOut<<name.Data()<<"\t"<<FE.Data()<<"\t"<<DAC<<"\n";
   
   }

fOut.close();
fIn->Close();

}


