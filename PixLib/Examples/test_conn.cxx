#include "PixConnectivity/PixConnectivity.h"
#include <string>
int main(int argc,char *argv[]) {
  std::string connTag(argv[2]);
  std::string idTag(argv[1]);
  PixLib::PixConnectivity conn(connTag, connTag, connTag, "Base", idTag, "Base");
  conn.loadConn();
 std::map<std::string,std::string> m=conn.listCfgObj("MODULE");
 for(const auto&it:m) {
   std::cout << it.first << " " << it.second << std::endl;
 }
 
 return 0;
}
