#include <iostream>
#include <sstream>
#include <iomanip>
#include <stdlib.h>
#include <unistd.h>

#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <pmg/pmg_initSync.h>

#include <sys/time.h>
#include <sys/types.h>
#include <sys/wait.h>

#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TGResourcePool.h>
#include <TGListBox.h>
#include <TGListTree.h>
#include <TGFSContainer.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGIcon.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGCanvas.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <TColor.h>
#include <TStyle.h>
#include <TGTextView.h>
#include <TGMenu.h>
#include <TGFileDialog.h>

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixConfigDb.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixMessages.h"

#include "RootDb/RootDb.h"


using namespace PixLib;

enum mainMode { NORMAL, HELP  };
enum mainMode mode;

enum connAction { LOAD, NOLOAD };
enum connAction aConn;

enum cfgAction { ALL, MOD, BOC , NONE};
enum cfgAction aCfg;

struct timeval t0;
struct timeval t1;  
struct timeval t2;
struct timeval t3;
struct timeval t4;

time_t myT;

int argc_All;
char** argv_All;

std::string dbServerName     = "PixelDbServer";
std::string ipcPartitionName = "PixelInfr";

std::string idTag = "Base";
std::string connTag = "Base";
std::string cfgTag = "Base";
std::string cfgModTag = "Base";
int errcount=0;
int forkcount=0;

PixConnectivity *conn;
PixConfigDb *conf;  
IPCPartition *ipcPartition;
PixDbServerInterface *DbServer;

void getParams(int argc, char **argv) {
  int ip = 1;
  mode = NORMAL;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } 
    }
  }
}

void help() {
    cout << endl;
    cout << "This program gives the possibility to load tag in DbServer " << endl;
    cout << endl; 
    std::cout << "Usage: DbServer_Loader  --help\n"
	      << "            // prints this message\n"
	      <<std::endl;
    cout << "Optional switches:" << endl;
    cout << "  --dbPart <part>      IPC Partition  (def. PixelInfr)" << endl;
    cout << "  --dbServ <dbServer>  DB server name (def. PixelDbServer)" << endl;
    cout << endl;
    exit(0);
}

void mem(int del) {
  pid_t pid = getpid();
  std::ostringstream os;
  os << "/proc/" << pid << "/status";
  std::cout << "Mem Info (" << os.str() << "):" << std::endl;
  std::ifstream st(os.str().c_str());
  while (!st.eof()) {
    std::string line;
    std::getline(st, line);
    if (line.substr(0,2) == "Vm") {
      std::cout << "  " << line << std::endl;
    }
  }
  sleep(del);
}

std::string getTime() {
  char t_s[100];
  time_t t;
  tm t_m;
  t = time(NULL);
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d-%H%M%S",&t_m);
  std::string date = t_s;
  return date;
}

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;                                                                                                                   
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

unsigned int getRev(std::string filename) {
  // std::cout << " filename is " << filename << std::endl;
  std::string::size_type pos = filename.find_last_of("_");
  if (pos != std::string::npos) {
    std::string sdate = filename.substr(pos+1);
    sdate = element(sdate, 0, '.');
    sdate = sdate.substr(0, sdate.size()-3);
    unsigned int date = 0;
    std::istringstream is(sdate);
    is >> date;
    std::cout << "->\t" << date << "\t" << filename << "\t(" << getTime(date) << ")" << std::endl;
    return date;
  } else {
    return 0;
  }
}

void connectToIpc(IPCPartition* &ipcPartition, std::string ipcPartitionName, PixDbServerInterface* &DbServer, std::string serverName) {
  //  Create IPCPartition constructors
  std::cout << std::endl << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "DbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << serverName << "  ..... " << std::endl;
  do {
    sleep(1);
    DbServer=new  PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << serverName << std::endl;
}

class HistoFrame : public TGMainFrame {

private:
  TGTab               *fTab, *fTab1;
  TGCompositeFrame    *tf;
  TGCompositeFrame    *line1, *line2, *line3, *line4, *line5, *line6, *ExF;
  TGLayoutHints       *step1;
  TGLabel             *TitleLabel, *idTagLabel,  *connTagLabel,  *cfgTagLabel,  *ModcfgTagLabel;
  TGLabel             *domCFLabel, *tagCFLabel, *tagModCFLabel, *readLabel;

  TGButton            *loadB, *pubB, *IsB, *ExitB;
  TGComboBox          *idTagCombo, *connTagCombo, *cfgTagCombo, *ModcfgTagCombo;

  TGLayoutHints        *fL1, *fL2, *fL5;

  TGCompositeFrame    *fFrameOcc, *fFrameOccTx;

  void loadIdTag();
  void loadTag(std::string type);

  std::vector<std::string> IdTagList;
  std::vector<std::string> ConnTagList;
  std::vector<std::string> CfgTagList;
  PixConnectivity* cin;
  std::map< std::string, PixConnectivity *> dbCfg;
  PixConnectivity *dbsCfg;

  bool ContainsModules(PixDbServerInterface* &dbServer, std::string dom, std::string tag);
  bool ContainsBoc(PixDbServerInterface* &dbServer, std::string dom, std::string tag);

  void CheckTags();
  void LoadInDbServer();
  bool checkConnectivity();
  bool checkCfg(std::string, std::string);
  //bool loadDisable(std::string, PixDbDomain::obj_id, PixConnectivity*, std::string);
  //bool loadRunConf(std::string, PixDbDomain::obj_id, PixConnectivity*, std::string);
  //bool loadTim(std::string, PixDbDomain::obj_id, PixConnectivity*, std::string);
  //bool loadModGroup(std::string, PixDbDomain::obj_id, PixConnectivity*, std::string);
  //bool loadModule(std::string, PixDbDomain::obj_id, PixConnectivity*, std::string);

public:
  HistoFrame(const TGWindow *p, UInt_t w, UInt_t h);
  virtual ~HistoFrame();
  
  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
};


HistoFrame::HistoFrame(const TGWindow *p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h) {
  std::cout << "Creating main window..." << std::endl;
  
  fL5 = new TGLayoutHints(kLHintsBottom | kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 1);

  // Exit button frame
  ExF = new TGHorizontalFrame(this, 60, 20, kFixedWidth);
  ExitB = new TGTextButton(ExF, "&Exit", 1);
  ExitB->Associate(this);
  fL1 = new TGLayoutHints(kLHintsLeft | kLHintsExpandX, 2, 2, 2, 2);
  ExF->AddFrame(ExitB, fL1);
  fL2 = new TGLayoutHints(kLHintsBottom | kLHintsRight, 2, 2, 5, 1);
  ExF->Resize(80, ExitB->GetDefaultHeight());
  AddFrame(ExF, fL2);

  fTab = new TGTab(this, 300, 300); 
  TGCompositeFrame *tf = fTab->AddTab("Tag Selection From Connectivity");


  line6 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  line5 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  line4 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  line3 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  line2 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  line1 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  step1 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 5, 5);

  TitleLabel = new TGLabel(line1, "Choose connectivity and configuration tag");
  line1->AddFrame(TitleLabel, step1);

  idTagLabel = new TGLabel(line2, "idTag");
  line2->AddFrame(idTagLabel, step1);
  idTagCombo = new TGComboBox(line2, 10);
  idTagCombo->Resize(100,20);
  idTagCombo->Associate(this);
  line2->AddFrame(idTagCombo, step1);

  connTagLabel = new TGLabel(line3, "connTag");
  line3->AddFrame(connTagLabel, step1);
  connTagCombo = new TGComboBox(line3, 11);
  connTagCombo->Resize(250,20);
  connTagCombo->Associate(this);
  line3->AddFrame(connTagCombo, step1);

  cfgTagLabel = new TGLabel(line4, "   cfgTag");
  line4->AddFrame(cfgTagLabel, step1);
  cfgTagCombo = new TGComboBox(line4, 12);
  cfgTagCombo->Resize(250,20);
  cfgTagCombo->Associate(this);
  line4->AddFrame(cfgTagCombo, step1);
  
  ModcfgTagLabel = new TGLabel(line5, "cfgModTag");
  line5->AddFrame(ModcfgTagLabel, step1);
  ModcfgTagCombo = new TGComboBox(line5, 13);
  ModcfgTagCombo->Resize(250,20);
  ModcfgTagCombo->Associate(this);
  line5->AddFrame(ModcfgTagCombo, step1);
  
  // load button
  loadB = new TGTextButton(line6, "Load Tag in DbServer", 2);
  loadB->Associate(this);
  line6->AddFrame(loadB, step1);

  tf->AddFrame(line1, step1);
  tf->AddFrame(line2, step1);
  tf->AddFrame(line3, step1);
  tf->AddFrame(line4, step1);
  tf->AddFrame(line5, step1);
  tf->AddFrame(line6, step1);
   
  AddFrame(fTab, fL5);

  loadIdTag();

  MapSubwindows();
  SetWindowName("Tag Loader for Pixel Database Server");
  SetWindowName( std::string("Tag Loader for dbServer -- " + ipcPartitionName).c_str() );
  Resize();   // resize to default size
  MapWindow();
 }

void HistoFrame::loadIdTag() {
  connTagCombo->RemoveEntries(0, 999999);
  connTagCombo->AddEntry("--", 999999);
  cfgTagCombo->RemoveEntries(0, 999999);
  cfgTagCombo->AddEntry("--", 999999);
  ModcfgTagCombo->RemoveEntries(0, 999999);
  ModcfgTagCombo->AddEntry("--", 999999);
  idTagCombo->RemoveEntries(0, 999999);
  idTagCombo->AddEntry("--", 999999);
  IdTagList.clear();

  conn = new PixConnectivity("Base", "Base", "Base", "Base", idTag);
  conf = new PixConfigDb();  
  conf->setCfgTag("Base", "Base");
  conf->openDbServer(DbServer);
  conf->cfgFromDb();

  std::vector<std::string> idlist;
  idlist = conf->listObjTags();
  int count=0;
  for (std::vector<std::string>::size_type i=0; i<idlist.size(); i++) {
    std::cout << "  -> IdTag :   " <<  idlist[i] << std::endl;
    //if (idlist[i].find("CFG")==std::string::npos) {
      idTagCombo->AddEntry((idlist[i]).c_str(), count);
      IdTagList.push_back(idlist[i]);
      count++;
    //}
  }
  for (int i=0; i<6; i++) {
    connTagCombo->AddEntry("", i);
    cfgTagCombo->AddEntry("", i);
    ModcfgTagCombo->AddEntry("", i);
  }
}


void HistoFrame::loadTag(std::string type) {

  int sel= idTagCombo->GetSelected();
  if (sel>-1 && sel <1000) {
    idTag= IdTagList[idTagCombo->GetSelected()];
    std::cout << "IdTag is: " << idTag << std::endl;
    if (type=="Connectivity") {
      conn->setConnTag("Base", "Base", "Base", idTag);
      connTagCombo->RemoveEntries(0, 999999);
      connTagCombo->AddEntry("--", 999999);
      ConnTagList.clear();
      ConnTagList=conn->listConnTagsC();
      for (unsigned int i=0; i<ConnTagList.size(); i++) {
	std::cout << "  -> tag   " << ConnTagList[i] << std::endl;
	connTagCombo->AddEntry((ConnTagList[i]).c_str(), i); 
      }
    }
    if (type=="Configuration") {
      conf->setCfgTag("Base", idTag);
      cfgTagCombo->RemoveEntries(0, 999999);
      cfgTagCombo->AddEntry("--", 999999);
      CfgTagList.clear();
      CfgTagList=conf->listCfgTags(idTag);
      for (unsigned int i=0; i<CfgTagList.size(); i++) {
	std::cout << "  -> tag   " << CfgTagList[i] << std::endl;
	cfgTagCombo->AddEntry((CfgTagList[i]).c_str(), i); 
      }
    }
    if (type=="Configuration-Mod") {
      conf->setCfgTag("Base", idTag);
      ModcfgTagCombo->RemoveEntries(0, 999999);
      ModcfgTagCombo->AddEntry("--", 999999);
      CfgTagList.clear();
      CfgTagList=conf->listCfgTags(idTag);
      for (unsigned int i=0; i<CfgTagList.size(); i++) {
	std::cout << "  -> tag   " << CfgTagList[i] << std::endl;
        ModcfgTagCombo->AddEntry((CfgTagList[i]).c_str(), i); 
      }
    }
  }
}

Bool_t HistoFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t) {
  // Process messages coming from widgets associated with the dialog.
  switch (GET_MSG(msg)) {
  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {
    case kCM_COMBOBOX:
      switch(parm1) {
      case 10:
	loadTag("Connectivity");
	loadTag("Configuration");
	loadTag("Configuration-Mod");
	break;
      case 11:
	break;
      }
      break;
    case kCM_BUTTON:
      switch(parm1) {
      case 1:
	CloseWindow();
	break;
      case 2:
	CheckTags();
	break;
      }
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
  return kTRUE;
}



HistoFrame::~HistoFrame(){
  Cleanup();
}

void HistoFrame::CloseWindow() {
   // Called when window is closed via the window manager.
   gApplication->Terminate(0);
}

void HistoFrame::CheckTags() {
  std::cout << "Checking Tag value before sending command ....." << std::endl;
  int sel=idTagCombo->GetSelected();
  if (sel>-1 && sel<1000) {
    idTag=IdTagList[sel];
    std::cout << "IdTag: " << idTag << std::endl;
  } else {
    std::cout << "IdTag Not Chosen " << std::endl;
    return;
  }
  sel=connTagCombo->GetSelected();
  std::cout << "Selection is: " << sel << std::endl;
  if (sel>-1 && sel<1000) {
    connTag=ConnTagList[sel];
    std::cout << "Connectivity Tag: " << connTag << std::endl;
  } else {
    std::cout << "Connectivity Tag Not Chosen " << std::endl;
    connTag = "Default";
  }
  sel=cfgTagCombo->GetSelected();
  if (sel>-1 && sel<1000) {
    cfgTag=CfgTagList[sel];
    std::cout << "Configuration Tag: " << cfgTag << std::endl;
  } else {
    std::cout << "Configuration Tag Not Chosen " << std::endl;
    cfgTag = "Base";
  }
  sel=ModcfgTagCombo->GetSelected();
  if (sel>-1 && sel<1000) {
    cfgModTag=CfgTagList[sel];             
    std::cout << "Module Configuration Tag: " << cfgModTag << std::endl;
  } else {
    std::cout << "Configuration Tag Not Chosen, setting equal to Configuration Tag " << std::endl;
    cfgModTag = "Base";
  }
  LoadInDbServer();
}

bool HistoFrame::ContainsModules(PixDbServerInterface* &dbServer, std::string dom, std::string tag) {
  std::vector<std::string> modlist;
  dbServer->listObjectsRem(dom,tag,"PixModule",modlist);
  if (modlist.size()==0) return false;
  else return true;
}

bool HistoFrame::ContainsBoc(PixDbServerInterface* &dbServer, std::string dom, std::string tag) {
  std::vector<std::string> list;
  dbServer->listObjectsRem(dom,tag,"PixModuleGroup",list);
  if (list.size()==0) return false;
  else return true;
}

void HistoFrame::LoadInDbServer() {
  std::cout << "LoadInDbServer called..." << std::endl;

  DbServer->setReadEnableRem(true);

  if (!checkConnectivity()) return;

  if (!checkCfg(cfgTag, "ROD-OTH")) return;

  if (!checkCfg(cfgModTag, "MOD")) return;

}

bool HistoFrame::checkConnectivity() {
  bool load = false;
  std::vector<std::string> domlist;
  DbServer->listDomainRem(domlist);
  for (unsigned int id=0; id<domlist.size(); id++) {
    if (domlist[id] == "Connectivity-"+idTag) {
      std::vector<std::string> taglist;
      DbServer->listTagsRem(domlist[id],taglist);
      if (connTag == "Default") {
	if (taglist.size() > 0) {
	  connTag = taglist[0];
	  ers::warning(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Loading connectivity tag "+connTag+" from DbServer"));
	  cin = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag);
	  cin->loadConn();
	  std::cout << "Nmods = " << cin->mods.size() << std::endl;
	} else {
	  ers::error(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "No connectivity tag selected"));
	  return false;
	}
      } else {
	for (unsigned int it=0; it<taglist.size(); it++) {
	  if (taglist[it] == connTag) {
	    load = true;
	  }
	}
	if (load) {
	  ers::warning(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Loading connectivity tag "+connTag+" from DbServer"));
	  cin = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag);
	  cin->loadConn();
	  std::cout << "Nmods = " << cin->mods.size() << std::endl;
	} else {
	  ers::warning(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Uploading connectivity tag "+connTag+" to DbServer"));
	  cin = new PixConnectivity(connTag, connTag, connTag, cfgTag, idTag);
	  cin->loadConn();
	  std::cout << "Nmods = " << cin->mods.size() << std::endl;
	  cin->dumpInDb(DbServer);
	} 
      }
    }
  }
  return true;
}

bool HistoFrame::checkCfg(std::string tag, std::string cfgType) {
  bool update = false;
  std::string modName;

  if (tag == "Base") return true;

  conf->cfgFromDbServ();
  std::vector<std::string> domlist = conf->listCfgTags(idTag);
  for (std::string it : domlist) {
    if (it == tag) update = true;
  }

  std::vector<std::string> dbsObj;
  if (update) {
    dbsObj = conf->listObjects(tag, idTag);
    ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Tag "+tag+" found in DbServer; check if updates are needed"));
  } else {
    ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Tag "+tag+" not found in DbServer; loading all"));
  }

  conf->cfgFromDb();
  std::vector<std::string> dbObj = conf->listObjects(tag, idTag);

  for (std::string obj : dbObj) {
    bool use = false;
    std::string objType = conf->getObjectType(obj, tag, idTag);
    if (objType.substr(objType.size()-4,4) == "_CFG") { 
      if (cfgType == "MOD" && objType == "MODULE_CFG") use = true;
      if (cfgType == "ROD" && objType == "RODBOC_CFG") use = true;
      if (cfgType == "ROD-OTH" && objType != "MODULE_CFG") use = true;
      if (cfgType == "OTH" && (objType != "MODULE_CFG" && objType != "RODBOC_CFG")) use = true;
      if (obj.substr(0,7) == "Disable" && objType == "DISABLE_CFG") use = false;
    }
    if (use) {
      std::vector<unsigned int> dbRev, dbsRev;
      dbRev = conf->listRevisions(obj, tag, idTag);
      unsigned int lrev=0;
      for (unsigned int rev : dbRev) {
	if (rev > lrev) lrev = rev;
      }
      std::ostringstream os;
      os << lrev;
      std::string srev = os.str();
      bool load = true;
      if (update) {
	if (std::find(dbsObj.begin(), dbsObj.end(), obj) != dbsObj.end()) {
          conf->cfgFromDbServ();
	  dbsRev = conf->listRevisions(obj, tag, idTag);
	  if (std::find(dbsRev.begin(), dbsRev.end(), lrev) != dbsRev.end()) {
	    ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Object "+obj+" "+objType+" rev "+srev+" found in DbServer"));
	    load = false;
	  } else {
	    ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main",  "Object "+obj+" "+objType+" rev "+srev+" not found in DbServer; force loading"));
	  }
	} else {
	  ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main",  "Object "+obj+" "+objType+" not found in DbServer; force loading"));
	}
      } else {
	ers::info(PixLib::pix::daq (ERS_HERE, "DBS_Loader_Main", "Loading object "+obj+" "+objType));
      }
      if (load) {
	PixDbDomain::obj_id oid;
	oid.domain = "Configuration-"+idTag;
	oid.tag = tag; 
	oid.rev =lrev;
	oid.type = objType;
        oid.name = obj;
	DbServer->addLoadList(oid);
      }
    }
  }

  // Print load list
  DbServer->setExpertMode(true);
  unsigned int llen=DbServer->getLoadListLen();
  std::cout << "List len = " << llen << std::endl;

  return true;
}


int main(int argc, char** argv) {

  argc_All=argc;
  argv_All=argv;

  getParams(argc, argv);
  
  if(mode == HELP) {
    help();
  } else {
    IPCCore::init(argc_All, argv_All);
    connectToIpc(ipcPartition, ipcPartitionName, DbServer, dbServerName); 
    TApplication *app = new TApplication("app",NULL,NULL);
    HistoFrame mainWindow(gClient->GetRoot(), 400, 350);
    app->Run(true);
  }
}
