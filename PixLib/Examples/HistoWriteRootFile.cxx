#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixHistoServer/PixHistoServerInterface.h"


using namespace PixLib;

int main(int argc, char **argv) {
  // Check arguments
  if(argc<5) {
    std::cout << "USAGE: HistoWriteRootFile [partition name] [foldersToSave] [rootFileName] [moreFiles]\n" 
	      << "moreFiles can be 1(= it will write more files, but it is faster) or 0(=1 huge file, but slower)" 
	      << std::endl;
    std::cout << "Pay attention! Eg: foldersToSave is /scan0/rod-c0-s11/" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string serverName = "nameServer";
  std::string foldersToSave = argv[2];
  std::string rootFileName = argv[3];
  std::stringstream stringMoreFiles;
  stringMoreFiles << argv[4];
  bool moreFiles;
  stringMoreFiles >> moreFiles;

  bool success=false;
  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  OHRootHistogram his;
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, 
							       std::string(OHSERVERBASE)+"helper", 
							       serverName, std::string(PROVIDERBASE)+"histo_client_receiver");
    success = hInt->writeRootHistoServer(foldersToSave, rootFileName, moreFiles);
  } catch (PixHistoServerExc e) {
    if (e.getId()=="NOIPC" || e.getId()== "NAMESERVER" || e.getId()=="NAMESERVER") {
      std::cout << e.getDescr() << std::endl; 
    }
    return 0;
  } catch (...) {
    std::cout << "HistoWriteRootFile. PixHistoServerInterface Unknown exception was thrown " << std::endl;
    return 0;
  }
  if (success) std::cout << "*** The ROOT FILE " << rootFileName << " has been processed. " << std::endl;
  else std::cout << foldersToSave << " does not exist OR the writer did not manage to put it in queue " << std::endl;
  return 0;
}
