#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <sys/time.h>
#include <string>

#include <TFile.h>
#include <TH1.h>
#include <TH2F.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"


#include "iblModuleConfigStructures.h"
#include "moduleConfigStructures.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFe.h"
#include "Config/Config.h"
#include "Config/ConfMask.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"

PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;

std::string idTag = "PIXEL";
//std::string idTag = "SR1";
std::string domainName = "Configuration-"+idTag;
std::string cfgModTag = "PIT_MOD";
//std::string cfgModTag = "TOOTHPIX-ORIG";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
unsigned int revision = 0;
bool help=false;

std::string savePath = "~";

using namespace PixLib;

void openDbServer(int argc, char** argv) {
  // Start IPCCore
  IPCCore::init(argc, argv);
  // Create IPCPartition constructors
  try {
    ipcPartition = new IPCPartition(dbsPart);
  }
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
        delete DbServer;
      } else {
        break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help=true;
        break;
      } else if (std::string(argv[ip]) == "--savePath") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          savePath = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No save path inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--dbPart") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          dbsPart = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--dbServ") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          dbsName = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--domain") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          domainName = argv[ip+1];
          ip +=2;
        } else {
          std::cout << std::endl << "No Domain name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--tag") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          cfgModTag = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No Tag name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--rev") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
          revision = atoi(argv[ip+1]);
          ip += 2;
        } else  {
          std::cout << std::endl << "No revision inserted" << std::endl << std::endl;
          break;
        }
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
}


int main(int argc, char** argv) {
  std::string dbx = "";
  try {

    getParams(argc, argv);
    if (help){
      std::cout << "Usage: CountTDACs\n"
		<< "     Optional switches:\n"
		<< "        --savePath <path>                Specify path                   def. " << savePath << std::endl
		<< "        --dbPart <partition>             Specify partition name         def. " << dbsPart << std::endl
		<< "        --dbServ <name>                  Specify dbServer name          def. " << dbsName << std::endl
		<< "        --domain <domain name>           Specify domain name            def. " << domainName << std::endl
		<< "        --tag    <config module tag>     Specify config module tag name def. " << cfgModTag << std::endl
		<< "        --rev    <revision>              Specify object revision  NOT TESTED!!!  def. last)\n"
		<< std::endl;
      return 0;
    }
    // Open DB server connection
    if (DbServer == NULL) openDbServer(argc, argv);

    int count_mod = 0;
  
    std::string filename;
    if(revision!=0){
      std::stringstream rev;
      rev << revision;
      filename = "Tdac_" + cfgModTag + "_" + rev.str() + ".root";
    }
    else {
      filename = savePath + "/Tdac_" + cfgModTag + ".root";
    }
    TFile *rootfile = new TFile(filename.c_str(),"RECREATE");

    // create summary histograms TDACs, GDACs, TDAC average per FE, TDAC vs GDAC
    std::string  histoname;
    histoname = "Tdac_" + cfgModTag;
    TH1F *h_tdac = new TH1F(histoname.c_str(), histoname.c_str(), 128, 0., 128.);
    histoname = "Gdac_" + cfgModTag;
    TH1F *h_gdac = new TH1F(histoname.c_str(), histoname.c_str(), 32, 0., 32.);
    histoname = "TdacAvgFE_" + cfgModTag;
    TH1F *h_tdac_avg_fe = new TH1F(histoname.c_str(), histoname.c_str(), 128, 0., 128.);
    histoname = "GDAC_vs_TDAC_" + cfgModTag;
    TH2F *h_tdac_vs_gdac = new TH2F(histoname.c_str(), histoname.c_str(), 128, 0., 128., 32, 0., 32.);
    histoname = "avgTDAC_vs_GDAC" + cfgModTag;
    TH2F *h_avgtdac_vs_gdac = new TH2F(histoname.c_str(), histoname.c_str(), 32, 0., 32., 128., 0., 128.);

    std::vector<std::string> modlist;
    DbServer->listObjectsRem(domainName,cfgModTag,"PixModule",modlist);
    std::vector<std::string>::iterator modit;

    for (modit = modlist.begin(); modit != modlist.end(); modit++) { 
      count_mod++;
      PixModule *mod = new PixModule(DbServer,(PixModuleGroup*)NULL,domainName,cfgModTag,*modit);

      //read special revision if not newest
      // 
      // NOT tested!
      if (revision!=0){
	if (!(mod->readConfig(DbServer, domainName, cfgModTag, revision))){
	  std::cout << "ERROR could not read Configuration for module\n";
	}
      }

      int count127 = 0;
      int countDis = 0;
      for (std::vector<PixFe*>::iterator fe = mod->feBegin(); fe != mod->feEnd(); fe++){
	Config &cfg = (*fe)->config();
	bool feEnabled = ((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value;
        ConfMask<unsigned short int> &tdac_mask = (*fe)->readTrim("TDAC");
	int gdac = (*fe)->readGlobRegister("GLOBAL_DAC");
	ConfMask<bool> &ro_enable = (*fe)->readPixRegister("ENABLE");
	h_gdac->Fill(gdac);    

	double tdac_avg_fe = 0;
	for (int col=0; col<tdac_mask.ncol(); col++) {
          for (int row=0; row<tdac_mask.nrow(); row++) {
            int tdac = tdac_mask.get(col, row);
	    h_tdac->Fill(tdac);
	    h_tdac_vs_gdac->Fill(tdac, gdac);
	    tdac_avg_fe+=tdac;
	    if (tdac >=127 && feEnabled) {
	      count127++;
	    }
	    if (!(ro_enable.get(col, row)) && feEnabled) {
	      countDis++;
	    }
          }
	}
	h_tdac_avg_fe->Fill(tdac_avg_fe/2880.);
	h_avgtdac_vs_gdac->Fill(gdac, tdac_avg_fe/2880.);
      }
      if (count127 > 0 || countDis > 0) {
	std::cout << mod->connName() << " -- " << count127 << " -- " << countDis << std::endl;
      }
    }
    
    std::cout<<"Number of modules found in Configuration: " << count_mod << std::endl; 
    rootfile->Write();
    rootfile->Close();
    
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }

  return 0;
}
