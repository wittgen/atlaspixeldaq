// Simple Test for testing DDC_DT reading temperature

#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <owl/time.h>
#include <ipc/partition.h>
#include <ipc/core.h>
#include <is/infodictionary.h>
#include <is/infodocument.h>
#include <is/info.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <ddc/DdcData.hxx>

#include "PixDcs/PixDcs.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;


int main(int argc, char **argv) {

  // GET COMMAND LINE

  // Check arguments
  if(argc<2) {
    std::cout << "USAGE: ReadOne connDbName" << std::endl;
    return 1;
  }

  std::string ipcPartitionName="PixelInfr";
  std::string isServerName="DDC";
  std::string rodName = "ROD_B1_S10";

  std::string connDbName = argv[1];
 
  // Start IPCCore
  IPCCore::init(argc, argv);
  // Create IPCPartition constructors
  std::cout << "Starting partition " << ipcPartitionName << std::endl << std::endl;
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  PixMessages* msg = new PixMessages();
  msg->pushPartition(ipcPartition->name(), ipcPartition);
  msg->pushStream("cout", std::cout);
  
  // GET INTERFACES
  PixDcs* dcs = new PixDcs(ipcPartition, isServerName, rodName, msg);

  // Read a single parametere from IS
  float val;
  PixDcsDefs::DataPointType dpt = PixDcsDefs::TOPTO;
  std::cout << " *** phase1 *** " << std::endl;
  std::cout << "Test the subscribtion-unsubscription method " << std::endl;
  bool r0 = dcs->readDataPoint(connDbName, dpt, val);
  if (r0) {
    std::cout << "Data Point Type number " << dpt << " = " << val << std::endl;
  } else {
    std::cout << "ReadOne.cxx - ERROR -  " << dpt << std::endl;
  }
  // test of is 4-aut-08
  ISInfoDictionary* m_isDictionary = new ISInfoDictionary(*ipcPartition);
  std::string name = connDbName + "_"+"TOpto";
  DcsSingleData<float> isFloat(0, *ipcPartition, "DDC."+name);
  m_isDictionary->getValue("DDC."+name, isFloat);
  std::stringstream info;
  info << "PixDcs::readDataPoint - OK! " << name << " is "  << isFloat.getValue();
  std::vector<std::pair<int, OWLTime> > tags;
  std::vector<std::pair<int, OWLTime> >::iterator it;
  m_isDictionary->getTags("DDC."+name, tags);
  for(it=tags.begin(); it!=tags.end(); it++) {
    std::cout << (*it).second.c_str() << std::endl;  
  }
  std::cout << " *** end phase1 *** " << std::endl;
  sleep(3);

  std::cout << "Test tags method" << std::endl;
  for(int i=0; i<4; i++) {
    sleep(5);
    for(it=tags.begin(); it!=tags.end(); it++) {
      std::cout << (*it).second.c_str() << std::endl;  
    }
  }

  std::cout << " *** phase2 *** " << std::endl;
  std::cout << "Test the subscribtion method PixDcs:read(connDbName, dpt, val)" << std::endl;
  try {
    dcs->read(connDbName, dpt, val);
  } catch (PixDcsExc e) {
    std::cout << "Exception caught "+e.getDescr() << std::endl;
  }
  std::cout << " *** end phase2 *** " << std::endl;
  
  
  return 0;
}

