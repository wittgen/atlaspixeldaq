#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixDbCoralDB/PixDbCoralDB.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "PixController/RodPixController.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "RCCVmeInterface.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/RosConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixBoc/PixBoc.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RodCrate/registeraddress.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <set>
#include <string>
#include <ipc/partition.h>
//#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

using namespace std;
using namespace PixLib;

#include <ctype.h>

#define N_FORMATTERS  8

enum mainMode { NORM, HELP  };
enum mainMode mode;

bool dbServ = true;
std::string dbsPart = "PixelInfr_moretti";
std::string dbsName = "PixelDbServer";
std::string idTag = "PIXEL";
std::string connTag = "PIT-ALL-V35";
std::string cfgTag = "PIT-20C";
std::string rodName = "";
std::string crateName = "";
std::string isServerName = "RunParams";

PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;
PixConnectivity *conn;

void openDbServer(int argc, char** argv) {   
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  char *part;
  part = getenv("PIX_PART_INFR_NAME");
  if (part != NULL) {
    dbsPart = part;
  }
  mode = HELP;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--idTag") {
	if (ip+1 < argc) {
	  idTag = argv[ip+1];
	  ip += 2;
	} 
      } else if (std::string(argv[ip]) == "--cfgTag") {
	if (ip+1 < argc) {
	  cfgTag = argv[ip+1];
	  ip += 2;
	} 
      } else if (std::string(argv[ip]) == "--connTag") {
	if (ip+1 < argc) {
	  connTag = argv[ip+1];
	  ip += 2; 
	}
      } else if (std::string(argv[ip]) == "--rod") {
	if (ip+1 < argc) {
	  rodName = argv[ip+1];
	  mode = NORM;
	  ip++; 
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc) {
	  dbsPart = argv[ip+1];
	  ip++; 
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--dbName") {
	if (ip+1 < argc) {
	  dbsName = argv[ip+1];
	  ip++; 
	}
        ip++; 
      } else {
	ip++;
      }
    } else {
      ip++;
    }
  }
}

int main(int argc, char *argv[]) {

  std::map<unsigned int, std::string> links;

  getParams(argc, argv);
  if (mode == HELP) {
    std::cout << "usage: ./ModLinkStat --rod RodName [--idTag idtag --connTag conntag --dbPart part]" << std::endl;
    exit(0);
  }
  try {
    // Open DB servwer connection
    if (DbServer == NULL) openDbServer(argc, argv);
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag);
    if (!dbServ) conn->cfgFromDb();
    conn->loadConn();

    bool nextRod = true;
    std::string nextRodName = "";
    std::map<std::string, RodBocConnectivity*>::iterator it = conn->rods.begin();
    if (it->second == NULL) nextRod = false;

    while (nextRod) {
      if (rodName == "ALL") {
	nextRodName = it->second->name();
	it++;
	if (it == conn->rods.end()) {
	  nextRod = false;
	} else if (it->second == NULL) {
	  nextRod = false;
	}
      } else {
	nextRodName = rodName;
	nextRod = false;
      }
      // Look for ROD and CRATE
      std::map<std::string, RodBocConnectivity *>::iterator it;
      it = conn->rods.find(nextRodName);
      if (it == conn->rods.end()) {
	std::cout << "ROD " << nextRodName << " not found in connDB (tag : " << connTag << ")" << std::endl;
	exit(-1);
      } else {
	std::cout << "ROD " << nextRodName << std::endl;
      }
      RodBocConnectivity *rod  = it->second;
      RodCrateConnectivity *crate = rod->crate();
      if (crate != NULL) crateName = crate->name();
      
      // Get module names
      for (int i=0; i<4; i++) {
	if (rod->obs(i) != NULL) {
	  if (rod->obs(i)->pp0() != NULL) {
	    for (int j=1; j<=8; j++) {
	      if (rod->obs(i)->pp0()->modules(j) != NULL) {
		ModuleConnectivity *mod = rod->obs(i)->pp0()->modules(j);
		if (mod->outLink1A() >= 0) links[mod->outLink1A()] = mod->name();
		if (mod->outLink1B() >= 0) links[mod->outLink1B()] = mod->name()+"-2A";
		if (mod->outLink2A() >= 0) links[mod->outLink2A()] = mod->name()+"-1B";
		if (mod->outLink2B() >= 0) links[mod->outLink2B()] = mod->name()+"-2B";
	      }
	    }
	  }
	}
      }
      
      // CREATE ISManager and read dump
      PixISManager* m_isManager;
      m_isManager = new PixISManager(dbsPart, isServerName);
      int time;
      std::vector<unsigned int> v = m_isManager->read< std::vector<unsigned int> >(crateName+"/"+nextRodName+"/STDDUMP", time);
      
      // start address of FPGA sections of ROD
      unsigned int errBase = 11152;
      unsigned int errLen = 194;
      unsigned int linkErrLen = 66;
      long unsigned int dspStart[4]    = {0x400000,0x402000, 0x402400, 0x404400 };
      string modebits[4]               = {"play", "mask", "skip", "drop"};
      string fmtfifostat[16]           = {"unknown", "reset", "idle" , "fetch modebits", 
					  "wait for MB", "wait for token", "read out link", "wait for data",
					  "dump event", "next link", "timeout", "overflow", 
					  "backpressure", "pass token", "other", "others"};
     
      std::map<unsigned int, std::string>::iterator il;
      std::cout << std::endl;
      for (il=links.begin(); il!=links.end(); il++) {
	int fmt = il->first/16;
	int lnk = il->first%16;
	std::cout << "Mod "  << setw(20) << std::left << il->second << " " << fmt << ":" << lnk;
	std::cout << " FMT " << fmtfifostat[(v[(FMT_MODEBIT_STAT(fmt)-dspStart[0])/4] >> 28 ) & 0xF];
	if ((v[(EFB_FORMATTER_STAT-dspStart[0])/4] & (1 << fmt))) std::cout << " STUCK";
	std::cout << endl;
	if(v[(FMT_LINK_EN(fmt)-dspStart[0])/4] & (1<<lnk)){
	  std::cout << "                           ENABLED ";
	  if (v[(FMT_TIMEOUT_ERR(fmt)-dspStart[0])/4] & (1<<lnk)) std::cout << " TIMEOUT ";
	  if (v[(FMT_DATA_OVERFLOW_ERR(fmt)-dspStart[0])/4] & (1<<lnk))  std::cout << " OVERFLOW ";
	  if (v[(FMT_HEADER_TRAILER_ERR(fmt)-dspStart[0])/4] & (1<<lnk)) std::cout << " HEADER/TRAILERLIMIT ";
	  if (v[(FMT_ROD_BUSY_ERR(fmt)-dspStart[0])/4] & (1<<lnk)) std::cout << " BUSY ";
	  std::cout << " MB " << modebits[((v[(FMT_MODEBIT_STAT(fmt)-dspStart[0])/4] >> (2*lnk) ) & 0x3)]; 
	  std::cout << " 0x" << v[(FMT_LINK_OCC_CNT(fmt,lnk)-dspStart[0])/4] << " words pending";
	} else {
	  std::cout << "                           DISABLED ";
	}
	std::cout << std::endl;
	if (v.size() == errBase + errLen) {
	  std::cout << "                           Scan error counters per DSP : ";
	  std::cout << " " << v[errBase + linkErrLen + fmt*16 + lnk];
	  if (v.size() == errBase + 2*errLen) std::cout << " " << v[errBase + errLen + linkErrLen + fmt*16 + lnk];
	  if (v.size() == errBase + 3*errLen) std::cout << " " << v[errBase + 2*errLen + linkErrLen + fmt*16 + lnk];
	  if (v.size() == errBase + 4*errLen) std::cout << " " << v[errBase + 3*errLen + linkErrLen + fmt*16 + lnk];
	  std::cout << std::endl;
	}
      }
      
      if (v.size() == errBase + errLen) {
	std::cout << std::endl;
	std::cout << "Event counters per DSP : ";
	std::cout << " " << v[errBase + 2];
	if (v.size() == errBase + 2*errLen) std::cout << " " << v[errBase + errLen + 2];
	if (v.size() == errBase + 3*errLen) std::cout << " " << v[errBase + 2*errLen + 2];
	if (v.size() == errBase + 4*errLen) std::cout << " " << v[errBase + 3*errLen + 2];
	std::cout << std::endl;
	std::cout << "Events with errors per DSP : ";
	std::cout << " " << v[errBase + 3];
	if (v.size() == errBase + 2*errLen) std::cout << " " << v[errBase + errLen + 3];
	if (v.size() == errBase + 3*errLen) std::cout << " " << v[errBase + 2*errLen + 3];
	if (v.size() == errBase + 4*errLen) std::cout << " " << v[errBase + 3*errLen + 3];
	std::cout << std::endl;
	std::cout << "Total number of errors per DSP : ";
	std::cout << " " << v[errBase + 1];
	if (v.size() == errBase + 2*errLen) std::cout << " " << v[errBase + errLen + 1];
	if (v.size() == errBase + 3*errLen) std::cout << " " << v[errBase + 2*errLen + 1];
	if (v.size() == errBase + 4*errLen) std::cout << " " << v[errBase + 3*errLen + 1];
	std::cout << std::endl;
      }
      std::cout << std::endl;
      char t_s[100];
      tm t_m;
      time_t t1;
      if (v.size() == errBase + errLen) {
	t1 = v[errBase];
	localtime_r(&t1, &t_m);
	strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
	std::cout << std::endl << "Last scan error update : " << t_s << std::endl;
      }
      t1 = time;
      localtime_r(&t1, &t_m);
      strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
      std::cout << std::endl << "Last update : " << t_s << std::endl << std::endl;
    } 
  }
  catch(PixDBException& e) {
    std::cerr<<"Got PixDBException: ";
    e.what(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    return -1;
  }
  return 0;
}


