#include <unistd.h>
#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixUtilities/PixISManager.h"


using namespace PixLib;


int main(int argc, char **argv) {
  std::string ipcPartitionName;
  std::string isServerName;
  // GET COMMAND LINE

  // Check arguments
  if(argc<3) {
    //std::cout << "USAGE: TestISManager [partition name] [IS server name]" << std::endl;
    //return 1;
    std::cout << "Start of TestISManager with default partition and server names" << std::endl;
    ipcPartitionName = "MyPartition";
    isServerName = "MyServer";
  }
  else {
  ipcPartitionName = argv[1];

  isServerName = argv[2];
  }

  // START IPC

  // Start IPCCore
  IPCCore::init(argc, argv);
  
  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  //IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  

  // CREATE ISManager
  PixISManager* m_isManager;
  m_isManager = new PixISManager(ipcPartitionName,isServerName);
 
  // Publish some ints and floats, update them read them, etc...

  char ch;
 
  char* str = 0;
  int choice=0;
  float float0;
  float float1;
  float float2;
  float float3;
  float float4;
  int int0;
  int int1;
  int int2;
  int int3;
  int int4;
  std::string string0;
  std::string string1;
  std::string string2;
  std::string string3;
  std::string string4;
  do {
    std::cout << "Publishing and Updating of Variables in IS Manager: " << std::endl;
    std::cout << "     0 - quit " << std::endl;
    std::cout << "     1 - Short" << std::endl;
    std::cout << "     2 - Int" << std::endl;
    std::cout << "     3 - Float" << std::endl;
    std::cout << "     4 - Double" << std::endl;	
    std::cout << "     5 - String" << std::endl;
    std::cout << "     6 - five Floats" << std::endl;
    std::cout << "     7 - five Ints" << std::endl;
    std::cout << "     8 - five Strings" << std::endl;
    std::cout << "     9 - five Doubles" << std::endl;
    std::cin >> choice;
    switch (choice)
      {
      case 1:
	break;
      case 2:
	break;
      case 3:
	break;
      case 4:
	break;
      case 5:
	break;
      case 6: 
	std::cout << "=======================Test of publishing, updating and deleting floats========================" << std::endl;
	
	std::cout << "PUBLISHING LIST OF FLOATS TO THE IS SERVER" << std::endl;
	m_isManager->publish("DeviceVoltage0",(float)2.03454);
	m_isManager->publish("DeviceVoltage1",(float)2.13454);
	m_isManager->publish("DeviceVoltage2",(float)2.23454);
	m_isManager->publish("DeviceVoltage3",(float)2.33454);
	m_isManager->publish("DeviceVoltage4",(float)2.43454);
	
	std::cout << "READING PUBLISHED FLOATS FROM THE IS SERVER" << std::endl;
	float0 = m_isManager->read<float>("DeviceVoltage0");
	float1 = m_isManager->read<float>("DeviceVoltage1");
	float2 = m_isManager->read<float>("DeviceVoltage2");
	float3 = m_isManager->read<float>("DeviceVoltage3");
	float4 = m_isManager->read<float>("DeviceVoltage4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::DeviceVoltage0: " << float0 << std::endl;
	std::cout << "TestISManager::DeviceVoltage1: " << float1 << std::endl;
	std::cout << "TestISManager::DeviceVoltage2: " << float2 << std::endl;
	std::cout << "TestISManager::DeviceVoltage3: " << float3 << std::endl;
	std::cout << "TestISManager::DeviceVoltage4: " << float4 << std::endl;
	
	while(true) {ch = getchar();if(ch == 'q') break;}
	std::cout << "UPDATING FLOATS IN THE IS SERVER" << std::endl;
	
	m_isManager->publish("DeviceVoltage0", (float) 0.0);
	m_isManager->publish("DeviceVoltage1", (float) 1.1);
	m_isManager->publish("DeviceVoltage2", (float) 2.2);
	m_isManager->publish("DeviceVoltage3", (float) 3.3);
	m_isManager->publish("DeviceVoltage4", (float) 2.4);
	
	std::cout << "READING UPDATED FLOATS FROM THE IS SERVER" << std::endl;
	
	float0 = m_isManager->read<float>("DeviceVoltage0");
	float1 = m_isManager->read<float>("DeviceVoltage1");
	float2 = m_isManager->read<float>("DeviceVoltage2");
	float3 = m_isManager->read<float>("DeviceVoltage3");
	float4 = m_isManager->read<float>("DeviceVoltage4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::DeviceVoltage0: " << float0 << std::endl;
	std::cout << "TestISManager::DeviceVoltage1: " << float1 << std::endl;
	std::cout << "TestISManager::DeviceVoltage2: " << float2 << std::endl;
	std::cout << "TestISManager::DeviceVoltage3: " << float3 << std::endl;
	std::cout << "TestISManager::DeviceVoltage4: " << float4 << std::endl;
	
	while(true) {ch = getchar(); if(ch == 'q') break; }

	std::cout << "UPDATING FLOATS IN THE IS SERVER AGAIN" << std::endl;
  
	m_isManager->publish("DeviceVoltage0", (float) 0.0);
	m_isManager->publish("DeviceVoltage1", (float) 1.1);
	m_isManager->publish("DeviceVoltage2", (float) 2.2);
	m_isManager->publish("DeviceVoltage3", (float) 3.3);
	m_isManager->publish("DeviceVoltage4", (float) 4.4);
	
	std::cout << "READING UPDATED FLOATS FROM THE IS SERVER" << std::endl;
	
	float0 = m_isManager->read<float>("DeviceVoltage0");
	float1 = m_isManager->read<float>("DeviceVoltage1");
	float2 = m_isManager->read<float>("DeviceVoltage2");
	float3 = m_isManager->read<float>("DeviceVoltage3");
	float4 = m_isManager->read<float>("DeviceVoltage4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::DeviceVoltage0: " << float0 << std::endl;
	std::cout << "TestISManager::DeviceVoltage1: " << float1 << std::endl;
	std::cout << "TestISManager::DeviceVoltage2: " << float2 << std::endl;
	std::cout << "TestISManager::DeviceVoltage3: " << float3 << std::endl;
	std::cout << "TestISManager::DeviceVoltage4: " << float4 << std::endl;
	
	while(true) {ch = getchar(); if(ch == 'q') break; }	//no need slow enough!
	
	m_isManager->removeVariable("DeviceVoltage0");
	m_isManager->removeVariable("DeviceVoltage1");
	m_isManager->removeVariable("DeviceVoltage2");
	m_isManager->removeVariable("DeviceVoltage3");
	m_isManager->removeVariable("DeviceVoltage4");
	
	std::cout << "================End of test of publishing floats=================" << std::endl;
	break;
      case 7:
	std::cout << "=======================Test of publishing, updating and removing INTEGERS========================" << std::endl;

	std::cout << "PUBLISHING LIST OF INTEGERS TO THE IS SERVER" << std::endl;
	m_isManager->publish("GroupID0",5);
	m_isManager->publish("GroupID1",4);
	m_isManager->publish("GroupID2",3);
	m_isManager->publish("GroupID3",2);
	m_isManager->publish("GroupID4",1);
	
	std::cout << "READING PUBLISHED INTEGERS FROM THE IS SERVER" << std::endl;
	int0 = m_isManager->read<int>("GroupID0");
	int1 = m_isManager->read<int>("GroupID1");
	int2 = m_isManager->read<int>("GroupID2");
	int3 = m_isManager->read<int>("GroupID3");
	int4 = m_isManager->read<int>("GroupID4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::GroupID0: " << int0 << std::endl;
	std::cout << "TestISManager::GroupID1: " << int1 << std::endl;
	std::cout << "TestISManager::GroupID2: " << int2 << std::endl;
	std::cout << "TestISManager::GroupID3: " << int3 << std::endl;
	std::cout << "TestISManager::GroupID4: " << int4 << std::endl;
	
	while(true) {ch = getchar();if(ch == 'q') break;}
	std::cout << "UPDATING INTEGERS IN THE IS SERVER" << std::endl;
	
	m_isManager->publish("GroupID0", 1);
	m_isManager->publish("GroupID1", 2);
	m_isManager->publish("GroupID2", 3);
	m_isManager->publish("GroupID3", 4);
	m_isManager->publish("GroupID4", 7);
	
	std::cout << "READING UPDATED INTEGERS FROM THE IS SERVER" << std::endl;
	
	int0 = m_isManager->read<int>("GroupID0");
	int1 = m_isManager->read<int>("GroupID1");
	int2 = m_isManager->read<int>("GroupID2");
	int3 = m_isManager->read<int>("GroupID3");
	int4 = m_isManager->read<int>("GroupID4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::GroupID0: " << int0 << std::endl;
	std::cout << "TestISManager::GroupID1: " << int1 << std::endl;
	std::cout << "TestISManager::GroupID2: " << int2 << std::endl;
	std::cout << "TestISManager::GroupID3: " << int3 << std::endl;
	std::cout << "TestISManager::GroupID4: " << int4 << std::endl;

	while(true) {ch = getchar(); if(ch == 'q') break; }

	std::cout << "UPDATING INTEGERS IN THE IS SERVER AGAIN" << std::endl;
	
	m_isManager->publish("GroupID0", (int)1);
	m_isManager->publish("GroupID1", (int)2);
	m_isManager->publish("GroupID2", (int)3);
	m_isManager->publish("GroupID3", (int)4);
	m_isManager->publish("GroupID4", (int)5);
	
	std::cout << "READING UPDATED INTEGERS FROM THE IS SERVER" << std::endl;

	int0 = m_isManager->read<int>("GroupID0");
	int1 = m_isManager->read<int>("GroupID1");
	int2 = m_isManager->read<int>("GroupID2");
	int3 = m_isManager->read<int>("GroupID3");
	int4 = m_isManager->read<int>("GroupID4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::GroupID0: " << int0 << std::endl;
	std::cout << "TestISManager::GroupID1: " << int1 << std::endl;
	std::cout << "TestISManager::GroupID2: " << int2 << std::endl;
	std::cout << "TestISManager::GroupID3: " << int3 << std::endl;
	std::cout << "TestISManager::GroupID4: " << int4 << std::endl;
	
	while(true) {ch = getchar(); if(ch == 'q') break; }
	
	m_isManager->removeVariable("GroupID0");
	m_isManager->removeVariable("GroupID1");
	m_isManager->removeVariable("GroupID2");
	m_isManager->removeVariable("GroupID3");
	m_isManager->removeVariable("GroupID4");
  
	std::cout << "================End of test of publishing INTEGERS=================" << std::endl;
	break;
      case 8:
	std::cout << "=======================Test of publishing, updating and removing STRINGS========================" << std::endl;

	std::cout << "PUBLISHING LIST OF STRINGS TO THE IS SERVER" << std::endl;
	m_isManager->publish("Status0","Started");
	m_isManager->publish("Status1","Idle");
	m_isManager->publish("Status2","Idle");
	m_isManager->publish("Status3","Started");
	m_isManager->publish("Status4","Started");
	
	std::cout << "READING PUBLISHED STRINGS FROM THE IS SERVER" << std::endl;
	string0 = m_isManager->read<std::string>("Status0");
	string1 = m_isManager->read<std::string>("Status1");
	string2 = m_isManager->read<std::string>("Status2");
	string3 = m_isManager->read<std::string>("Status3");
	string4 = m_isManager->read<std::string>("Status4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::Status0: " << string0 << std::endl;
	std::cout << "TestISManager::Status1: " << string1 << std::endl;
	std::cout << "TestISManager::Status2: " << string2 << std::endl;
	std::cout << "TestISManager::Status3: " << string3 << std::endl;
	std::cout << "TestISManager::Status4: " << string4 << std::endl;
	
	while(true) {ch = getchar();if(ch == 'q') break;}
	std::cout << "UPDATING STRINGS IN THE IS SERVER" << std::endl;
	
	m_isManager->publish("Status0", "Running");
	m_isManager->publish("Status1", "Idle");
	m_isManager->publish("Status2", "Idle");
	m_isManager->publish("Status3", "Running");
	m_isManager->publish("Status4", "Running");
	
	std::cout << "READING UPDATED STRINGS FROM THE IS SERVER" << std::endl;
	string0 = m_isManager->read<std::string>("Status0");
	string1 = m_isManager->read<std::string>("Status1");
	string2 = m_isManager->read<std::string>("Status2");
	string3 = m_isManager->read<std::string>("Status3");
	string4 = m_isManager->read<std::string>("Status4");
	
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::Status0: " << string0 << std::endl;
	std::cout << "TestISManager::Status1: " << string1 << std::endl;
	std::cout << "TestISManager::Status2: " << string2 << std::endl;
	std::cout << "TestISManager::Status3: " << string3 << std::endl;
	std::cout << "TestISManager::Status4: " << string4 << std::endl;

	while(true) {ch = getchar(); if(ch == 'q') break; }

	std::cout << "UPDATING STRINGS IN THE IS SERVER AGAIN" << std::endl;
	
	m_isManager->publish("Status0", "Finished");
	m_isManager->publish("Status1", "Idle");
	m_isManager->publish("Status2", "Idle");
	m_isManager->publish("Status3", "Hanging");
	m_isManager->publish("Status4", "Finished");
	
	std::cout << "READING UPDATED STRINGS FROM THE IS SERVER" << std::endl;

	string0 = m_isManager->read<std::string>("Status0");
	string1 = m_isManager->read<std::string>("Status1");
	string2 = m_isManager->read<std::string>("Status2");
	string3 = m_isManager->read<std::string>("Status3");
	string4 = m_isManager->read<std::string>("Status4");
	
	std::cout << "READ VALUES:" << std::endl;
	std::cout << "TestISManager::Status0: " << string0 << std::endl;
	std::cout << "TestISManager::Status1: " << string1 << std::endl;
	std::cout << "TestISManager::Status2: " << string2 << std::endl;
	std::cout << "TestISManager::Status3: " << string3 << std::endl;
	std::cout << "TestISManager::Status4: " << string4 << std::endl;

	while(true) {ch = getchar(); if(ch == 'q') break; }
	
	m_isManager->removeVariable("Status0");
	m_isManager->removeVariable("Status1");
	m_isManager->removeVariable("Status2");
	m_isManager->removeVariable("Status3");
	m_isManager->removeVariable("Status4");
  
	std::cout << "================End of test of publishing STRINGS=================" << std::endl;
	break;
      case 9:
	break;
      default:
	std::cout << "Please correct your choice ..." << std::endl;
	break;
      }
  }
  while (choice);
  delete m_isManager;
  return 0;
}
