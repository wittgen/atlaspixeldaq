

#include <iostream>
#include <Config/Config.h>
#include <Config/ConfMask.h>
#include <ConfigRootIO/MaskWrapper.h>
#include <ConfigRootIO/ConfigStoreFactory.h>
#include <ConfigRootIO/ConfigStore.h>
#include <PixModule/PixModule.h>
#include <filesystem>
#include <variant.hpp>

using namespace std;
using namespace PixLib;

class PixVariant {
public:
    PixVariant()=default;
    void read(const std::string &name,variant32 &v) {
        PixModule::MCCflavour mccFlv = PixModule::PM_NO_MCC;
        PixModule::FEflavour  feFlv  = PixModule::PM_FE_I4B;
        int nFe = 1;
        PixModule mod("Base", "Base", "Def", mccFlv, feFlv, nFe);
        std::string filename=std::string(name);
        std::unique_ptr<RootConf::IConfigStore> store( RootConf::ConfigStoreFactory::configStore());
        PixLib::Config &config=mod.config();
        std::string modname=std::filesystem::path(filename).filename();
        std::size_t  pos=modname.find("_");
        if (pos!=std::string::npos) modname.resize(pos);
        config.m_confName=modname+"_0";
        try {
            store->read(mod.config(), filename, "Def", 0);
        } catch(RootConf::RevisionNotFound &e) {
            std::cout << e << std::endl;
        }
        process(config,v);
    }
private:
    void process(PixLib::ConfObj &c,const std::string &gn,variant32 &v) {
        std::string n=c.name();
        std::size_t  pos = n.find(gn);
        if (pos!=std::string::npos) n.erase(0,gn.size());
        switch(c.type()) {
            case  PixLib::ConfObj::INT: {
                PixLib::ConfInt &i=(PixLib::ConfInt &) c;
                switch(c.subtype()) {
                    case PixLib::ConfObj::S32:
                    {
                        int32_t val=i.valueInt();
                        v[n]=val;
                        break;
                    }
                    case PixLib::ConfObj::U32:
                    {
                        uint32_t val=i.valueUInt();
                        v[n]=val;
                        break;
                    }
                    case PixLib::ConfObj::S16:
                    {
                        int16_t val=i.valueInt();
                        v[n]=val;
                        break;
                    }
                    case PixLib::ConfObj::U16:
                    {
                        uint16_t val=i.valueUInt();
                        v[n]=val;
                        break;
                    }
                    case PixLib::ConfObj::S8:
                    {
                        int8_t val=i.valueInt();
                        v[n]=val;
                        break;
                    }
                    case PixLib::ConfObj::U8:
                    {
                        uint8_t val=i.valueUInt();
                        v[n]=val;
                        break;
                    }
                    default: break;
                }
                break;
            }
            case PixLib::ConfObj::FLOAT:
            {
                PixLib::ConfFloat &f=(PixLib::ConfFloat &) c;
                float  val=f.valueFloat();
                v[n]=val;
                break;
            }
            case PixLib::ConfObj::VECTOR:
                switch(c.subtype()) {
                    case PixLib::ConfObj::V_INT:
                        std::cout << "V_INT\n";
                        break;
                    case PixLib::ConfObj::V_UINT:
                        std::cout << "V_UINT\n";
                        break;
                    case PixLib::ConfObj::V_FLOAT:
                        std::cout << "V_FlOAT\n";
                        break;
                    default: break;
                }
                break;
            case PixLib::ConfObj::STRVECT:
                break;
            case PixLib::ConfObj::MATRIX: {
                PixLib::ConfMatrix &m=(PixLib::ConfMatrix &) c;
                variant32 w;
                switch(c.subtype()) {
                    case PixLib::ConfObj::M_U16: {
                        PixLib::ConfMask<uint16_t> val=m.valueU16();
                        w["__mask__"]["data"]=val.getMask();
                        w["__mask__"]["ncol"]=val.ncol();
                        w["__mask__"]["nrow"]=val.nrow();
                        w["__mask__"]["type"]="ushort";
                        break;
                    }
                    case PixLib::ConfObj::M_U1: {
                        PixLib::ConfMask<bool> val=m.valueU1();
                        w["__mask__"]["data"]=val.getMask();
                        w["__mask__"]["ncol"]=val.ncol();
                        w["__mask__"]["nrow"]=val.nrow();
                        w["__mask__"]["type"]="bool";
                        break;
                    }
                    default: break;
                }
                v[n]=w;

                break;
            }
            case PixLib::ConfObj::LIST: {
                variant32 w;
                PixLib::ConfList &l=(PixLib::ConfList &) c;
                int32_t val=l.valueInt();
                w["__list__"]["data"]=val;
                v[n]=w;
                break;
            }
            case PixLib::ConfObj::BOOL: {
                PixLib::ConfBool &b=(PixLib::ConfBool &) c;
                bool &val=b.valueBool();
                v[n]=val;
                break;
            }
            case PixLib::ConfObj::STRING:
            {
                PixLib::ConfString &s=(PixLib::ConfString &) c;
                const std::string &val=s.valueString();
                v[n]=val;
                break;
            }
            case PixLib::ConfObj::LINK:
                break;
            case PixLib::ConfObj::ALIAS:
                break;
            case PixLib::ConfObj::VOID:
                v[n]=nullptr;
                break;
        }
    }


    void process(const  PixLib::ConfGroup &config,variant32 &v) {
        const std::string name=config.name()+"_";
        for(auto  const &c:config) {
            process(*c,name,v);
        }
    }

    void process(const  PixLib::Config &config,variant32 &v) {
        for(unsigned i=0;i<config.size();i++) {
            variant32 w;
            process(config[i],w);
            v[config[i].name()]=w;
        }
        for(unsigned i=0;i<config.subConfigSize();i++) {
            variant32 w;
            process(config.subConfig(i),w);
            v[config.subConfig(i).name()]=w;
        }
    }
};


int main(int argc, const char** argv) {

  variant32 v;
 // PixModule::MCCflavour mccFlv = PixModule::PM_MCC_I2;
 // PixModule::FEflavour  feFlv  = PixModule::PM_FE_I2;
  PixVariant pv;
  pv.read(argv[1],v);
  v.dump();


  return 0;
	
}
