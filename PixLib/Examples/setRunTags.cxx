//#include <ipc/server.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include <PixUtilities/PixISManager.h>

using namespace PixLib;

int main(int argc, char** argv) {
  // Start IPCCore    
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition;
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition("Partition_PixelInfrastructure");
  } catch(...) {
    std::cout << "ERROR!! problems while connecting to the partition!" << std::endl;
    exit(-1);
  }
  
  PixISManager *ism = new PixISManager(ipcPartition);
  if (argc == 4) {
    ism->publish("RUN_ID_TAG", argv[1]);
    ism->publish("RUN_CONN_TAG", argv[2]);
    ism->publish("RUN_CFG_TAG", argv[3]);
  } else {
    std::cout << "Usage: setRunTags idTag connTag cfgTag" << std::endl;
  }
}
