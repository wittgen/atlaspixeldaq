#include "PixUtilities/PixISManager.h"

#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TGResourcePool.h>
#include <TGListBox.h>
#include <TGListTree.h>
#include <TGFSContainer.h>
#include <TGClient.h>
#include <TGFrame.h>
#include <TGIcon.h>
#include <TGLabel.h>
#include <TGButton.h>
#include <TGTextEntry.h>
#include <TGNumberEntry.h>
#include <TGMsgBox.h>
#include <TGMenu.h>
#include <TGCanvas.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TRootEmbeddedCanvas.h>
#include <TCanvas.h>
#include <TH1.h>
#include <TH2.h>
#include <TGraph.h>
#include <TColor.h>
#include <TStyle.h>
#include <TGTextView.h>
#include <TGMenu.h>
#include <TGFileDialog.h>
#include <TGProgressBar.h>

using namespace PixLib;

using namespace std;

enum mainMode { NORMAL, HELP  };
enum mainMode mode;

std::string ipcPartitionName = "PixelInfr";
std::string IsServerName     = "PixelRunParams";
IPCPartition *ipcPartition = NULL;
PixISManager* isManager = NULL; 

void connect() {
  //  Create IPCPartition constructors
  std::cout << std::endl << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }

  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(PixISManagerExc e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return;
  }
  std::cout << "Succesfully connected to IsManager " << IsServerName << std::endl;
}



class HistoFrame : public TGMainFrame {

private:
  const TGWindow      *fMain;
  TGTab               *fTab;
  TGLayoutHints       *step1, *step2, *step3, *step4, *fL1, *fL2, *fL5;
 
  //Variable for SR1
  TGCompositeFrame    *tf;
  TGCompositeFrame    *botS;
  TGButton            *ExS, *StartS;
  TGVerticalFrame     *vframe3, *vframe4;
  TGLabel             *sbc3Label, *sbc4Label;
  TGHProgressBar      *fHP_3, *fHP_4;
 
  //Variable for PIT
  TGCompositeFrame    *tf1;
  TGButton            *StartP;
  TGVerticalFrame     *vframeB, *vframeL, *vframeD;
  TGVerticalFrame     *vframeB1,  *vframeB2, *vframeB3, *vframeD1,  *vframeD2, *vframeL1,  *vframeL2, *vframeL3, *vframeL4;
  TGLabel             *sbcB1Label, *sbcB2Label, *sbcB3Label, *sbcD1Label, *sbcD2Label,*sbcL1Label, *sbcL2Label, *sbcL3Label, *sbcL4Label;
  TGHProgressBar      *fHP_B1, *fHP_B2, *fHP_B3, *fHP_D1, *fHP_D2,*fHP_L1, *fHP_L2, *fHP_L3, *fHP_L4;

  //my methods
  void showMemSR1();
  void showMemPIT();
  void showVar(std::string valName, TGHProgressBar *Bar);

public:
  HistoFrame(const TGWindow *p, UInt_t w, UInt_t h);
  virtual ~HistoFrame();
  
  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
};

HistoFrame::HistoFrame(const TGWindow *p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h) {
  std::cout << "Creating main window..." << std::endl;
  fMain = p;
  
  fTab = new TGTab(this, 500, 500); 
 
  step1 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,5, 5, 5);
  step2 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,5, 5, 5);
  //  step2 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,35, 5, 35);
  step3 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 250, 5);
  step4 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 400, 5, 5,5);
  fL1 = new TGLayoutHints(kLHintsLeft | kLHintsExpandX, 2, 2, 2, 2);
  fL2 = new TGLayoutHints(kLHintsBottom | kLHintsRight, 2, 2, 5, 1);
  fL5 = new TGLayoutHints(kLHintsBottom | kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 1);
  
  TGCompositeFrame *tf1 = fTab->AddTab("SbcMemory - PIT");
  TGCompositeFrame *tf = fTab->AddTab("SbcMemory - SR1");

  // Button frame
  botS = new TGHorizontalFrame(this, 300, 20);
  ExS = new TGTextButton(botS, "&Exit", 1);
  ExS->Associate(this);
  botS->AddFrame(ExS, fL1);
  StartS = new TGTextButton(botS, "&SR1", 3);
  StartS->Associate(this);
  botS->AddFrame(StartS, fL1);
  StartP = new TGTextButton(botS, "&PIT", 4);
  StartP->Associate(this);
  botS->AddFrame(StartP, fL1);
  botS->Resize(300, ExS->GetDefaultHeight());
  tf->AddFrame(botS, fL2);
  
  //SR1 graphics
  // vertical frame with three horizontal progressive bars
  vframe3 = new TGVerticalFrame(tf, 10, 10);
  sbc3Label = new TGLabel(vframe3, "pixrcc03");
  vframe3->AddFrame(sbc3Label, step1);
  fHP_3 = new TGHProgressBar(vframe3,TGProgressBar::kStandard,300);
  fHP_3->ShowPosition();
  fHP_3->SetMax(1e6);
  fHP_3->SetMin(0);
  vframe3->AddFrame(fHP_3,step1);

  vframe4 = new TGVerticalFrame(tf, 10, 10);
  sbc4Label = new TGLabel(vframe4, "pixrcc04");
  vframe4->AddFrame(sbc4Label, step1);
  fHP_4 = new TGHProgressBar(vframe4,TGProgressBar::kStandard,300);
  fHP_4->ShowPosition();
  fHP_4->SetMax(1e6);
  fHP_4->SetMin(0);
  vframe4->AddFrame(fHP_4,step1);
  
  vframe3->Resize();
  vframe4->Resize();

  tf->AddFrame(vframe3, step2);
  tf->AddFrame(vframe4, step2);
  

  //PIT graphics
  // bAl = new TGCompositeFrame(tf1, 10, 10,  kHorizontalFrame);
  
  vframeB = new TGVerticalFrame(tf1, 10, 10);
  //sbc frame
  vframeB1 = new TGVerticalFrame(tf1, 10, 10);
  sbcB1Label = new TGLabel(vframeB1, "sbc-pix-rcc-b-1");
  vframeB1->AddFrame(sbcB1Label, step1);
  fHP_B1 = new TGHProgressBar(vframeB1,TGProgressBar::kStandard,300);
  fHP_B1->ShowPosition();
  fHP_B1->SetMax(1e6);
  fHP_B1->SetMin(0);
  vframeB1->AddFrame(fHP_B1,step1);
  vframe3->Resize();
  vframeB->AddFrame(vframeB1, step2);

  vframeB2 = new TGVerticalFrame(tf1, 10, 10);
  sbcB2Label = new TGLabel(vframeB2, "sbc-pix-rcc-b-2");
  vframeB2->AddFrame(sbcB2Label, step1);
  fHP_B2 = new TGHProgressBar(vframeB2,TGProgressBar::kStandard,300);
  fHP_B2->ShowPosition();
  fHP_B2->SetMax(1e6);
  fHP_B2->SetMin(0);
  vframeB2->AddFrame(fHP_B2,step1);
  vframe3->Resize();
  vframeB->AddFrame(vframeB2, step2);

  vframeB3 = new TGVerticalFrame(tf1, 10, 10);
  sbcB3Label = new TGLabel(vframeB3, "sbc-pix-rcc-b-3");
  vframeB3->AddFrame(sbcB3Label, step1);
  fHP_B3 = new TGHProgressBar(vframeB3,TGProgressBar::kStandard,300);
  fHP_B3->ShowPosition();
  fHP_B3->SetMax(1e6);
  fHP_B3->SetMin(0);
  vframeB3->AddFrame(fHP_B3,step1);
  vframe3->Resize();
  vframeB->AddFrame(vframeB3, step2);

  vframeB->Resize();
  tf1->AddFrame(vframeB, step1);
  
  
  vframeD = new TGVerticalFrame(tf1, 10, 10);
  //sbc frame
  vframeD1 = new TGVerticalFrame(tf1, 10, 10);
  sbcD1Label = new TGLabel(vframeD1, "sbc-pix-rcc-d-1");
  vframeD1->AddFrame(sbcD1Label, step1);
  fHP_D1 = new TGHProgressBar(vframeD1,TGProgressBar::kStandard,300);
  fHP_D1->ShowPosition();
  fHP_D1->SetMax(1e6);
  fHP_D1->SetMin(0);
  vframeD1->AddFrame(fHP_D1,step1);
  vframe3->Resize();
  vframeD->AddFrame(vframeD1, step3);

  vframeD2 = new TGVerticalFrame(tf1, 10, 10);
  sbcD2Label = new TGLabel(vframeD2, "sbc-pix-rcc-d-2");
  vframeD2->AddFrame(sbcD2Label, step1);
  fHP_D2 = new TGHProgressBar(vframeD2,TGProgressBar::kStandard,300);
  fHP_D2->ShowPosition();
  fHP_D2->SetMax(1e6);
  fHP_D2->SetMin(0);
  vframeD2->AddFrame(fHP_D2,step1);
  vframe3->Resize();
  vframeD->AddFrame(vframeD2, step2);
  vframeD->Resize();
  tf1->AddFrame(vframeD, step1);

  
  vframeL = new TGVerticalFrame(tf1, 10, 10);
  //sbc frame
  vframeL1 = new TGVerticalFrame(tf1, 10, 10);
  sbcL1Label = new TGLabel(vframeL1, "sbc-pix-rcc-l-1");
  vframeL1->AddFrame(sbcL1Label, step1);
  fHP_L1 = new TGHProgressBar(vframeL1,TGProgressBar::kStandard,300);
  fHP_L1->ShowPosition();
  fHP_L1->SetMax(1e6);
  fHP_L1->SetMin(0);
  vframeL1->AddFrame(fHP_L1,step1);
  vframe3->Resize();
  vframeL->AddFrame(vframeL1, step4);

  vframeL2 = new TGVerticalFrame(tf1, 10, 10);
  sbcL2Label = new TGLabel(vframeL2, "sbc-pix-rcc-l-2");
  vframeL2->AddFrame(sbcL2Label, step1);
  fHP_L2 = new TGHProgressBar(vframeL2,TGProgressBar::kStandard,300);
  fHP_L2->ShowPosition();
  fHP_L2->SetMax(1e6);
  fHP_L2->SetMin(0);
  vframeL2->AddFrame(fHP_L2,step1);
  vframe3->Resize();
  vframeL->AddFrame(vframeL2, step4);

  vframeL3 = new TGVerticalFrame(tf1, 10, 10);
  sbcL3Label = new TGLabel(vframeL3, "sbc-pix-rcc-l-3");
  vframeL3->AddFrame(sbcL3Label, step1);
  fHP_L3 = new TGHProgressBar(vframeL3,TGProgressBar::kStandard,300);
  fHP_L3->ShowPosition();
  fHP_L3->SetMax(1e6);
  fHP_L3->SetMin(0);
  vframeL3->AddFrame(fHP_L3,step1);
  vframe3->Resize();
  vframeL->AddFrame(vframeL3, step4);

  vframeL4 = new TGVerticalFrame(tf1, 10, 10);
  sbcL4Label = new TGLabel(vframeL4, "sbc-pix-rcc-l-4");
  vframeL4->AddFrame(sbcL4Label, step1);
  fHP_L4 = new TGHProgressBar(vframeL4,TGProgressBar::kStandard,300);
  fHP_L4->ShowPosition();
  fHP_L4->SetMax(1e6);
  fHP_L4->SetMin(0);
  vframeL4->AddFrame(fHP_L4,step1);
  vframe4->Resize();
  vframeL->AddFrame(vframeL4, step4);

  vframeL->Resize();
  tf1->AddFrame(vframeL, step1);

  //general
  AddFrame(fTab, fL5);

  MapSubwindows();
  SetWindowName("Sbc Memor information");
  Resize();   // resize to default size
  MapWindow();
}

void HistoFrame::showVar(std::string valName, TGHProgressBar *Bar) {
  unsigned int val=0;
  if (isManager!=0) {
    try {
      val = isManager->read<int>(valName);
    } catch (...) {
      std::cout << "Problem in reading parameter " << valName << " from IS" << std::endl;
    }
  }
  if (val<6e5) Bar->SetBarColor("green");
  else if (val>=6e5 && val<8.5e5) Bar->SetBarColor("yellow");
  else Bar->SetBarColor("red");
  Bar->SetPosition(val);
}
  
void HistoFrame::showMemSR1() {
  int i=0;
  std::cout << "Called ShowMemSR1()" << std::endl;
  do {
    i++;
    showVar("ROD_CRATE_0/memory",fHP_4);
    showVar("ROD_CRATE_1/memory",fHP_3);
    sleep(1);
  } while (i<10);
}

void HistoFrame::showMemPIT() {
  int i=0;
  do {
    i++;
    showVar("ROD_CRATE_B1/memory",fHP_B1);
    showVar("ROD_CRATE_B2/memory",fHP_B2);
    showVar("ROD_CRATE_B3/memory",fHP_B3);
    showVar("ROD_CRATE_D1/memory",fHP_D1);
    showVar("ROD_CRATE_D2/memory",fHP_D2);
    showVar("ROD_CRATE_L1/memory",fHP_L1);
    showVar("ROD_CRATE_L2/memory",fHP_L2);
    showVar("ROD_CRATE_L3/memory",fHP_L3);
    showVar("ROD_CRATE_L4/memory",fHP_L4);
    sleep(i);
  } while (i<10);
}

HistoFrame::~HistoFrame(){
  Cleanup();
}


void HistoFrame::CloseWindow() {
  // Called when window is closed via the window manager.
  gApplication->Terminate(0);
}

Bool_t HistoFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t) {
  switch (GET_MSG(msg)) {
  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {
    case kCM_COMBOBOX:
      switch(parm1) {
      case 10:
	break;
      }
      break;
    case kCM_BUTTON:
      switch(parm1) {
      case 1:
	std::cout << "Case 1" << std::endl;
	CloseWindow();
	break;
      case 2:
	std::cout << "Case 2" << std::endl;
	CloseWindow();
	break;
      case 3:
	std::cout << "Case 3" << std::endl;
        showMemSR1();
	break;
      case 4:
	std::cout << "Case 4" << std::endl;
	showMemPIT();
	break;
      }
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
  return kTRUE;
}

void getParams(int argc, char **argv) {
  int ip = 1;
  mode = NORMAL;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--isServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  IsServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No IsServer name inserted" << std::endl << std::endl;
	  break;
	}
      } 
    }
  }
}

void help() {
  std::cout << std::endl;
  cout << "This program shows memory content of sbc" << endl;
  cout << endl; 
  std::cout << "Usage: SbcMem --help\n"
	    << "            // prints this message\n"
	    <<std::endl;
  cout << "Optional switches:" << endl;
  cout << "  --dbPart <part>      IPC Partition  (def. PixelInfr)     " << endl;
  cout << endl;
  exit(0);
}

int main(int argc, char **argv) {
  
  getParams(argc, argv);
  if(mode == HELP) {
    help();
  } else {
    IPCCore::init(argc, argv);
    connect();
    
    TApplication *app = new TApplication("app",NULL,NULL);
    HistoFrame mainWindow(gClient->GetRoot(), 400, 350);
    app->Run(true);
  }
}
