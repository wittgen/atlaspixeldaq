#include "TApplication.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixController/PixScan.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixFe/PixFe.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "PixConnectivity/Pp0Connectivity.h"

#include <iostream>
#include <exception>
#include <stdlib.h>
#include <sstream>
#include <ctime>

#include <vector>
#include "TTree.h"
#include "TObjString.h"

using namespace PixLib;
using namespace std;

class TimeExtractor {
public:
  TimeExtractor(string outfile) {
    name_ROD = new vector<string>;
    scanTime_ROD = new vector<double>;
    confTime_ROD = new vector<double>;
    histoWritingTime_ROD = new vector<double>;
    totalTime_ROD = new vector<double>;
    histoDownloadTime_ROD = new vector<int>;
    endLoopActionTime_ROD = new vector<int>;
    num_modules = new vector<int>;
    num_pp0 = new vector<int>;
    num_modSDSP = new vector< vector<int> >;
    num_FEmod = new vector< vector<int> >;
    scanSList = new vector< time_t >;
    scanEList = new vector< time_t >;
    InitTree(outfile);
    maskSteps = events = 0;
    mccMode = -1;
    for (int i = 0; i < 3; i++){
      loopSteps[i] = -1;
      loopVars[i] = -1;
    }
    fileName = new TObjString;
    scanName = new TObjString;
    timeScanStart = new TObjString;
    cFileName = 0;
    cScanName = 0;
    scanTime_tot = 0;
    numRODs = 0;
  };
  ~TimeExtractor() {};
  
private:
  TFile * file;
  TTree * resultsTree;

  //timing results
  Float_t scanTime_tot;
  //Float_t histoWritingTime_tot;
  //Float_t configWritingTime_tot;

  //vars per ROD
  Int_t numRODs;
  vector<string> *name_ROD;
  vector<double> *scanTime_ROD;
  vector<double> *confTime_ROD;
  vector<double> *histoWritingTime_ROD;
  vector<double> *totalTime_ROD;
  vector<int> *histoDownloadTime_ROD;
  vector<int> *endLoopActionTime_ROD;


  //input parameters
  TObjString *fileName; 
  TObjString *scanName;
  TObjString *timeScanStart;
  const char *cScanName, *cFileName;

  //specific scan parameters
  int maskSteps, loopSteps[3], loopVars[3], mccMode, events;

  //configuration parameters
  vector<int> *num_modules;
  vector<int> *num_pp0;
  vector< vector<int> > *num_modSDSP;
  vector< vector<int> > *num_FEmod;
  vector< time_t > *scanSList;
  vector< time_t > *scanEList;


public:
  
  void TimeDiffAll() {
    time_t minSTime = 0; time_t maxETime = 0;
    vector< time_t >::iterator timeit;
    //Find min ScanStartTime
    for (timeit=scanSList->begin(); timeit != scanSList->end(); timeit++)
    {
      if (minSTime == 0)
      {
        minSTime = *timeit;
        continue;
      }
      if (minSTime > *timeit)
        minSTime = *timeit;
    }
    //Find max ScanEndTime
    for (timeit=scanEList->begin(); timeit != scanEList->end(); timeit++)
    {
      if (maxETime < *timeit)
        maxETime = *timeit;
    }
    scanTime_tot = difftime(maxETime, minSTime);
    cout << "Scanning time: " << scanTime_tot << "s" << endl;
    scanSList->clear();
    scanEList->clear();
  }

  void TimeDiffRod(DbRecord* ScanRecord){
    string scanS, scanE, writeS, writeE, confE;
    int hDownloadT = -2;
    int loopActionT = -2;
    
    PixScan *pstmp = new PixScan();
    pstmp->readInfo(ScanRecord);
    Config &scanInfo = pstmp->info();
    if (scanInfo["Time"].name()!="__TrashConfGroup__"){
      if (scanInfo["Time"]["ScanStart"].name()!="__TrashConfObj__"){
	scanS = ((ConfString&) scanInfo["Time"]["ScanStart"]).value();
      timeScanStart->SetString(scanS.c_str());
      }
      if (scanInfo["Time"]["ScanEnd"].name()!="__TrashConfObj__"){
	scanE = ((ConfString&) scanInfo["Time"]["ScanEnd"]).value();
      }
      if (scanInfo["Time"]["ScanHwriteStart"].name()!="__TrashConfObj__"){
	writeS = ((ConfString&) scanInfo["Time"]["ScanHwriteStart"]).value();
      }
      if (scanInfo["Time"]["ScanHwriteEnd"].name()!="__TrashConfObj__"){
	writeE = ((ConfString&) scanInfo["Time"]["ScanHwriteEnd"]).value();
      }
// cout << "just before read attempt" << endl;
      if (scanInfo["Time"]["ScanConfigEnd"].name()!="__TrashConfObj__"){
  confE = ((ConfString&) scanInfo["Time"]["ScanConfigEnd"]).value();
      }
// cout << "just after read attempt. Value: " << confE << endl;
      if (scanInfo["Time"]["HistoDownloadTime"].name()!="__TrashConfObj__"){
	//cout << "jump in loop :" <<  scanInfo["Time"]["HistoDownloadTime"].name() <<endl;
		hDownloadT = ((ConfInt&) scanInfo["Time"]["HistoDownloadTime"]).value();
     	 //	cout << "just retrieved: hDownloadT: " << hDownloadT << endl;
	      }
      if (scanInfo["Time"]["EndLoopActionTime"].name()!="__TrashConfObj__"){
       // cout << "jump in loop :" <<  scanInfo["Time"]["EndLoopActionTime"].name() <<endl;	
        loopActionT = (((ConfInt&) scanInfo["Time"]["EndLoopActionTime"]).value());
      //	cout << "just retrieved: loopActionT: " << loopActionT << endl;
        }
    }

    struct tm tmtmp[5];
    time_t t[5] = {0,0,0,0,0};
    if (scanS != "") {
      strptime(scanS.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[0]);
      t[0] = mktime(&tmtmp[0]);
      // ARGH !!!! Why is this hack needed ? If not there time is wrong by one hour ...
      strptime(scanS.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[0]);
      t[0] = mktime(&tmtmp[0]);
      // end of hack
      scanSList->push_back( t[0] );

      char timeBuff[20];
      tm *buffTime = localtime(&t[0]);
//Is wrong by 2 hours, cheating here:
      buffTime->tm_hour += 2;

      strftime(timeBuff, 20, "%Y-%m-%d %H:%M:%S", buffTime);
      
      timeScanStart->SetString(timeBuff);
    if (confE != "") {
      strptime(confE.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[5]);
      t[5] = mktime(&tmtmp[5]);
            
      confTime_ROD->push_back( difftime(t[5],t[0]) );
      cout << "Configuration time: " << difftime(t[5],t[0]) << " s" << endl;
          }
          else {
      cout << "Configuration time not available (missing end time)" << endl;
      confTime_ROD->push_back( -1 );
          }
    
      if (scanE != "") {
	 strptime(scanE.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[1]);
	 t[1] = mktime(&tmtmp[1]);
              
	 scanTime_ROD->push_back( difftime(t[1],t[0]) );
	 cout << "Scanning time: " << difftime(t[1],t[0]) << " s" << endl;
            }
            else {
	 cout << "Scanning time not available (missing end time)" << endl;
	 scanTime_ROD->push_back( -1 );
            }
 
   }
    else {
      cout << "Scanning, config and total time not available (missing start time)" << endl;
      scanTime_ROD->push_back( -1 );
      confTime_ROD->push_back( -1 );
    }

    if (writeE != "") {
      strptime(writeE.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[3]);
      t[3] = mktime(&tmtmp[3]);
      scanEList->push_back( t[3] );
      if (writeS != "") {
	strptime(writeS.c_str(),"%Y-%m-%d %H:%M:%S",&tmtmp[2]);
	t[2] = mktime(&tmtmp[2]);
	histoWritingTime_ROD->push_back( difftime(t[3],t[2]) );
	cout << "Histo writing time: " << difftime(t[3],t[2]) << " s" << endl;
      }
      else {
	cout << "Histo writing time not available (missing start time)" << endl;
	histoWritingTime_ROD->push_back( -1 );
      }
      if (scanS != "") {
	totalTime_ROD->push_back( difftime(t[3],t[0]) );
	cout << "Scan+histo time: " << difftime(t[3],t[0]) << " s" << endl;
      }
      else {
	totalTime_ROD->push_back( -1 );
      }
    }
    else{
      cout << "Histo writing and scan+histo time not available (missing end time)" << endl;
      histoWritingTime_ROD->push_back( -1 );
      totalTime_ROD->push_back( -1 );
    }

    if (hDownloadT > -1)
    {
	cout << "Total histo download time: " << hDownloadT << " usec" << endl;
    }
    else if (hDownloadT == -1)
	  {
		cout  << "Total histo download time was not set during scan" << endl;
	  }
    else
	cout << "Scan contained no Information on histo download time" << endl;

    if (loopActionT > -1)
    {
	cout << "Total end loop action time: " << loopActionT << " usec" << endl;
    }
    else if (loopActionT == -1)
	  {
		cout  << "Total end loop action time was not set during scan" << endl;
	  }
    else
	cout << "Scan contained no Information on end loop action time" << endl;

    histoDownloadTime_ROD->push_back(hDownloadT);
    endLoopActionTime_ROD->push_back(loopActionT);
    int maskSteps, loopSteps[3], FEmask, events;
    PixScan::ScanParam  loopVar[3];
    PixScan::MccBandwidth mccMode;

    for (int h = 0; h < 3; h++) {
      loopSteps[h] = -1;
    }

    cout << "********************" << endl;
    cout << "Scan parameters" << endl;
    cout << "********************" << endl;

    pstmp->readConfig(ScanRecord);
    Config &scanCfg = pstmp->config();
    maskSteps = pstmp->getMaskStageSteps();
    cout << "Mask steps: " << maskSteps << endl;
    mccMode = pstmp->getMccBandwidth();
    events = ((ConfInt&)scanCfg["general"]["repetitions"]).value();
    cout << "Events: " << events << endl;
    // Not an FE by FE scan: need number of active FE in module config
    // Do we have reliable information there?
    //  FEmask = pstmp->getFEbyFEMask();
    //  cout << "FE mask: " << hex << FEmask << dec << endl;

    for (int i = 0; i < 3; i++){
      if (pstmp->getLoopActive(i)){
//  cout << "*****Loop " << i << " is active." << endl;
	loopVar[i] = pstmp->getLoopParam(i);
	loopSteps[i] = pstmp->getLoopVarNSteps(i);
	char tmpstr[50];
	sprintf(tmpstr,"paramLoop_%d",i);
	for (map<string, int>::iterator it = ((ConfList &)scanCfg["loops"][tmpstr]).symbols().begin(); it != ((ConfList &)scanCfg["loops"][tmpstr]).symbols().end(); it++){
	  if ((*it).second==loopVar[i]){
	    cout << "Loop " << i << ": " << (*it).first << " in " << loopSteps[i] << " steps" << endl;
	    break;
	  }
	}
      }
    }
    SetMaskSteps(maskSteps);
    SetLoopSteps(loopSteps);
    SetLoopVars(loopVar);
    SetMccMode(mccMode);
    SetEvents(events);
    cout << "********************" << endl;

    delete pstmp;
    pstmp = 0;
  }

/*  void TimeDiffTot(DbRecord* ScanRecord){
    string scanS, scanE, cfgE, writeE;

    dbFieldIterator it;
    it = ScanRecord->findField("TimeStamp");
    if (it != ScanRecord->fieldEnd()){
      ScanRecord->getDb()->DbProcess(it, PixDb::DBREAD, scanS);
    }
    it = ScanRecord->findField("TimeStampEnd");
    if (it != ScanRecord->fieldEnd()){
      ScanRecord->getDb()->DbProcess(it, PixDb::DBREAD, scanE);
    }
    it = ScanRecord->findField("TimeStampHistogramWritingEnd");
    if (it != ScanRecord->fieldEnd()){
      ScanRecord->getDb()->DbProcess(it, PixDb::DBREAD, writeE);
    }
    it = ScanRecord->findField("TimeStampConfigWritingEnd");
    if (it != ScanRecord->fieldEnd()){
      ScanRecord->getDb()->DbProcess(it, PixDb::DBREAD, cfgE);
    }
    
    struct tm tmtmp[4];
    time_t t[4] = {0,0,0,0};

    if (scanS != "") {
      strptime(scanS.c_str(),"Scan commenced at %H:%M:%S on %m-%d-%Y",&tmtmp[0]);
      t[0] = mktime(&tmtmp[0]);
      if (scanE != "") {
	strptime(scanE.c_str(),"Scan terminated at %H:%M:%S on %m-%d-%Y",&tmtmp[1]);
	t[1] = mktime(&tmtmp[1]);
	scanTime_tot = difftime(t[1],t[0]);
	cout << "Scanning time: " << scanTime_tot << " s" << endl;
      }
      else {
	cout << "Scanning time not available (missing end time)" << endl;
	scanTime_tot = -1;
      }
    }
    else {
      cout << "Scanning, scah+histo and total time not available (missing start time)" << endl;
      scanTime_tot = -1;
    }
    
    if (writeE != "") {
      strptime(writeE.c_str(),"%H:%M:%S on %m-%d-%Y",&tmtmp[2]);
      t[2] = mktime(&tmtmp[2]);
      if (scanE != "") {
	histoWritingTime_tot = difftime(t[2],t[1]);
	cout << "Histo writing time: " << histoWritingTime_tot << " s" << endl;
      }
      else {
	cout << "Histo writing time not available (missing scan end time)" << endl;
	histoWritingTime_tot = -1;
      }
      if (scanS != "") {
	cout << "Scan+histo time: " << difftime(t[2],t[0]) << " s" << endl;
      }
    }
    else{
      cout << "Histo writing and scan+histo time not available (missing histo end time)" << endl;
      histoWritingTime_tot = -1;
    }

    if (cfgE != "") {
      strptime(cfgE.c_str(),"%H:%M:%S on %m-%d-%Y",&tmtmp[3]);
      t[3] = mktime(&tmtmp[3]);
      if (writeE != "") {
	configWritingTime_tot = difftime(t[3],t[1]); //cla t3-t2 ??
	cout << "Cfg writing time: " << configWritingTime_tot << " s" << endl;
      }
      else {
	cout << "Cfg writing time not available (missing histo end time)" << endl;
	configWritingTime_tot = -1;
      }
      if (scanS != "") {
	cout << "Total time: " << difftime(t[3],t[0]) << " s" << endl;
      }
    }
    else{
      cout << "Cfg writing and total time not available (missing end time)" << endl;
      configWritingTime_tot = -1;
    }
  }
*/
  
  void FillTree()
  {
    /*cout << fileName << endl << scanName << endl;
    cFileName = fileName.Data();
    cScanName = scanName.Data();
    cout << cFileName << endl << cScanName << endl;
    */resultsTree->Fill();
  }

  void AddRODName(string name) { name_ROD->push_back(name);};
  void SetNumMod(Int_t nMod) { num_modules->push_back(nMod);};
  void SetNumPp0(Int_t nPp0) { num_pp0->push_back(nPp0);};
  void SetModsInSDSP(vector<int> nm) { num_modSDSP->push_back(nm);}
  void SetFEsInMod(vector<int> nm) { num_FEmod->push_back(nm);}
  void SetNumRods(Int_t nRod) { numRODs = nRod;};
  void SetScan(string scan) { scanName->SetString(scan.c_str()); };
  void SetFile(string file) { fileName->SetString(file.c_str()); };
  void SetMaskSteps(Int_t ms) { maskSteps = ms;};
  void SetLoopSteps(Int_t ls[3]) { for (Int_t i = 0; i < 3; i++) { loopSteps[i] = ls[i]; }};
  void SetLoopVars(PixScan::ScanParam lv[3]) { for (Int_t i = 0; i < 3; i++) { loopVars[i] = (int)lv[i]; }};
  void SetMccMode(PixScan::MccBandwidth mb) { mccMode = (int)mb; };
  void SetEvents(Int_t e) { events = e; };

  void Finalize() {
    if (file->IsOpen())
      file->Close();
  }

  void Count(DbRecord *ScanRecord, int *num_Pp0, int *num_Modules, int num_modDSP[4], vector<int> *nFEmod){
    map<string, int> module_pp0s;
    int pp0s[4];
    int FEcount = 0;
    for (int i = 0; i < 4; i++)
    	pp0s[i] = 0;
    
    string modname;
    vector<string> modnames;
    map<string, ModuleConnectivity*>::iterator it;
    // Need to use connectivity info. ModuleGroups in scancfg are SDSP groups
    PixScan *pstmp = new PixScan();
    PixConnectivity *conn = new PixConnectivity();
    pstmp->readHisto(ScanRecord, conn, false);
//loop over all modules
    pstmp->readConfig(ScanRecord);
    PixModuleGroup *mGrp = pstmp->getTmpGrp();
    int mask = 0; int slave_done = 0; int mod_buff = 0;

    for (int slave = 0; slave < 4; slave++){
      if (pstmp->getReadoutEnabled(slave)){
	mask = pstmp->getModuleMask(slave);
//         cout << hex << mask << dec << endl;
	for (int im=0; im<32; im++) {
	  if (mask & (0x1<<im)) {
	    modname = pstmp->getModNames(im);
// 		cout << slave << " " << im << ": " << modname << endl;
            if (modname != "==") {
	      it = conn->mods.find(modname);
//               cout << (*it).second->pp0()->name()<< endl;
	      modnames.push_back(modname);
// 	      cout << "Im vektor: " << modname << endl;
              module_pp0s.insert(make_pair((*it).second->pp0()->name(),1));
	      mod_buff++;
              //pp0s[(*it).second->pp0Slot()]++;
            }
	  }
	}
      }
//cout << endl << endl;
//    num_modDSP[slave] = pp0s[0] + pp0s[1] + pp0s[2] + pp0s[3] - slave_done;
//    slave_done = pp0s[0] + pp0s[1] + pp0s[2] + pp0s[3];
    num_modDSP[slave] = mod_buff - slave_done;
    slave_done = mod_buff;

    }
    int num_pp0s = 0;

    map<string,int>::iterator pp0_iter;
    for( pp0_iter = module_pp0s.begin(); pp0_iter != module_pp0s.end(); ++pp0_iter ) {
    num_pp0s++;
  }

    //for (int i = 0; i < 4; i++)
      //if (pp0s[i] != 0)
        //num_pp0s++;
	
    (*num_Pp0) = num_pp0s;
    (*num_Modules) = mod_buff;
    
    for (std::vector<PixModule*>::iterator mit = mGrp->modBegin(); mit != mGrp->modEnd(); mit++)
    {
	    FEcount = 0;
	    string aktName= (*mit)->connName();
	    if ( find(modnames.begin(), modnames.end(), aktName) != modnames.end() )
	    {	
		    //cout << (*mit)->connName() << endl;
		    for (int k = 0; k < 16; k++)
		    {
			    Config &FEConf = (*mit)->pixFE(k)->config();
			    if (FEConf["Misc"].name()!="__TrashConfGroup__"){
				    if (FEConf["Misc"]["ScanEnable"].name()!="__TrashConfObj__"){
					    bool FEenable = ((ConfBool&) FEConf["Misc"]["ScanEnable"]).value();
					    if (FEenable) 
					    {
						    FEcount++;
					    }
				    }
			    }
		    }
//		    cout << "Module " << aktName << ": " << FEcount << " FEs enabled" << endl;
		    nFEmod->push_back(FEcount);
	    }
	    
    }
    return;
  }

  
/* Old Counting Methods Replaced by Count-method
  int CountPp0s(DbRecord *ScanRecord){
    int pp0s[4];
    for (int i = 0; i < 4; i++)
    	pp0s[i] = 0;
    string modname;
    map<string, ModuleConnectivity*>::iterator it;
    // Need to use connectivity info. ModuleGroups in scancfg are SDSP groups
    PixScan *pstmp = new PixScan();
    PixConnectivity *conn = new PixConnectivity;
    pstmp->readHisto(ScanRecord, conn, false);
//loop over all modules
    pstmp->readConfig(ScanRecord);
    int mask = 0;
    for (int slave = 0; slave < 4; slave++){
      if (pstmp->getReadoutEnabled(slave)){
	mask |= pstmp->getModuleMask(slave);
	for (int im=0; im<32; im++) {
	  if (mask & (0x1<<im)) {
	    modname = pstmp->getModNames(im);
//		cout << im << ": " << modname << endl;
            if (modname != "==") {
	      it = conn->mods.find(modname);
//              cout << (*it).second->groupId<< endl;
		pp0s[(*it).second->groupId]++;
            }
	  }
	}
      }
    }
    int num_pp0s = 0;
    for (int i = 0; i < 4; i++)
      if (pp0s[i] != 0)
        num_pp0s++;

    return num_pp0s;
  }

  int CountModsInSDSP(DbRecord *ScanRecord, int slave) {
    PixScan *pstmp = new PixScan();
    pstmp->readConfig(ScanRecord);
    int mask = 0, nMod = 0;
    if (slave < 4){
      if (pstmp->getReadoutEnabled(slave)){
	mask |= pstmp->getModuleMask(slave);
	for (int im=0; im<32; im++) {
	  if (mask & (0x1<<im)) {
	    if (pstmp->getModNames(im) != "==") {
	      nMod++;
	    }
	  }
	}
      }
    }
    return nMod;
  }

  int CountMods(DbRecord *ScanRecord) {
    int nMod = 0;
    for (int slave = 0; slave < 4; slave++){
      nMod += CountModsInSDSP(ScanRecord, slave);
    }
    return nMod;
  }
*/

  int CheckEmptyHistos(DbRecord *dbr) {
    //need a good way to identify possbly aborted scans
    // ignored for the moment
    return 0;
  }

  void clear() {
    scanTime_ROD->clear();
    confTime_ROD->clear();
    name_ROD->clear();
    histoWritingTime_ROD->clear();
    totalTime_ROD->clear();
    histoDownloadTime_ROD->clear();
    endLoopActionTime_ROD->clear();
    num_modules->clear();
    num_pp0->clear();
    for (vector< vector<int> >::iterator it = num_modSDSP->begin(); it != num_modSDSP->end(); it++) {
      (*it).clear();
    }
    num_modSDSP->clear();
    for (vector< vector<int> >::iterator it = num_FEmod->begin(); it != num_FEmod->end(); it++) {
      (*it).clear();
    }
    num_FEmod->clear();
    maskSteps = events = 0;
    mccMode = -1;
    for (int i = 0; i < 3; i++){
      loopSteps[i] = -1;
      loopVars[i] = -1;
    }
    fileName->SetString("");
    scanName->SetString("");
    //scanTime_tot = histoWritingTime_tot = configWritingTime_tot;
    scanTime_tot = 0;
    numRODs = 0;
  }

  void SaveTree()
  {
    file->cd();
    resultsTree->Write();
  }

private: //private functions
  void StartNewTree()
  {
    resultsTree = new TTree("timingTree","timingTree");
    ///
    resultsTree->Branch("scanTime_tot",&scanTime_tot,"scanTime_tot/F");
    ///
    resultsTree->Branch("numRODs",&numRODs,"numRODs/I");
    resultsTree->Branch("name_ROD","vector<string>",&name_ROD);
    resultsTree->Branch("scanTime_ROD","vector<double>",&scanTime_ROD);
    resultsTree->Branch("confTime_ROD","vector<double>",&confTime_ROD);
    resultsTree->Branch("histoWritingTime_ROD","vector<double>",&histoWritingTime_ROD);
    resultsTree->Branch("totalTime_ROD","vector<double>",&totalTime_ROD);
    resultsTree->Branch("histoDownloadTime_ROD","vector<int>",&histoDownloadTime_ROD);
    resultsTree->Branch("endLoopActionTime_ROD","vector<int>",&endLoopActionTime_ROD);
    ////
    resultsTree->Branch("fileName","TObjString", &fileName);
    resultsTree->Branch("scanName","TObjString", &scanName);
    resultsTree->Branch("timeScanStart","TObjString", &timeScanStart);

    ///
    resultsTree->Branch("maskSteps",&maskSteps,"maskSteps/I");
    resultsTree->Branch("loopSteps[3]",loopSteps,"loopSteps[3]/I");
    resultsTree->Branch("loopVars[3]",loopVars,"loopVars[3]/I");
    resultsTree->Branch("mccMode",&mccMode,"mccMode/I");
    resultsTree->Branch("events",&events,"events/I");
    ///
    resultsTree->Branch("num_modules","vector<int>",&num_modules);
    resultsTree->Branch("num_pp0","vector<int>",&num_pp0);
    resultsTree->Branch("num_modSDSP","vector<vector<int> >",&num_modSDSP);
    resultsTree->Branch("num_FEmod","vector<vector<int> >",&num_FEmod);
    ///
    return;
  }
  void InitTree(string outfile)
  {
    file = new TFile(outfile.c_str(),"UPDATE");
    resultsTree = (TTree*)file->Get("timingTree");
    if (!resultsTree) {
      StartNewTree();
    }
    else
    {
    resultsTree->GetBranch("scanTime_tot")->SetAddress(&scanTime_tot);
    ///
    resultsTree->GetBranch("numRODs")->SetAddress(&numRODs);
    resultsTree->GetBranch("name_ROD")->SetAddress(&name_ROD);
    resultsTree->GetBranch("scanTime_ROD")->SetAddress(&scanTime_ROD);
    resultsTree->GetBranch("confTime_ROD")->SetAddress(&confTime_ROD);
    resultsTree->GetBranch("histoWritingTime_ROD")->SetAddress(&histoWritingTime_ROD);
    resultsTree->GetBranch("totalTime_ROD")->SetAddress(&totalTime_ROD);
    resultsTree->GetBranch("histoDownloadTime_ROD")->SetAddress(&histoDownloadTime_ROD);
    resultsTree->GetBranch("endLoopActionTime_ROD")->SetAddress(&endLoopActionTime_ROD);
    ////
    resultsTree->GetBranch("fileName")->SetAddress(&fileName);
    resultsTree->GetBranch("scanName")->SetAddress(&scanName);
    resultsTree->GetBranch("timeScanStart")->SetAddress(&timeScanStart);

    ///
    resultsTree->GetBranch("maskSteps")->SetAddress(&maskSteps);
    resultsTree->GetBranch("loopSteps[3]")->SetAddress(loopSteps);
    resultsTree->GetBranch("loopVars[3]")->SetAddress(loopVars);
    resultsTree->GetBranch("mccMode")->SetAddress(&mccMode);
    resultsTree->GetBranch("events")->SetAddress(&events);
    ///
    resultsTree->GetBranch("num_modules")->SetAddress(&num_modules);
    resultsTree->GetBranch("num_pp0")->SetAddress(&num_pp0);
    resultsTree->GetBranch("num_modSDSP")->SetAddress(&num_modSDSP);
    resultsTree->GetBranch("num_FEmod")->SetAddress(&num_FEmod);


/*    	resultsTree->GetBranch("timeScanStart")->SetAddress(&timeScanStart);
      resultsTree->GetBranch("fileName")->SetAddress(&fileName);
      resultsTree->GetBranch("scanName")->SetAddress(&scanName);
      resultsTree->GetBranch("name_ROD")->SetAddress(&name_ROD);
      resultsTree->GetBranch("scanTime_ROD")->SetAddress(&scanTime_ROD);
	    resultsTree->GetBranch("confTime_ROD")->SetAddress(&confTime_ROD);
      resultsTree->GetBranch("histoWritingTime_ROD")->SetAddress(&histoWritingTime_ROD);
	    resultsTree->GetBranch("totalTime_ROD")->SetAddress(&totalTime_ROD);
	    resultsTree->GetBranch("histoDownloadTime_ROD")->SetAddress(&histoDownloadTime_ROD);
	    resultsTree->GetBranch("endLoopActionTime_ROD")->SetAddress(&endLoopActionTime_ROD);
	    resultsTree->GetBranch("num_modules")->SetAddress(&num_modules);
	    resultsTree->GetBranch("num_pp0")->SetAddress(&num_pp0);
	    resultsTree->GetBranch("num_modSDSP")->SetAddress(&num_modSDSP);
    	resultsTree->GetBranch("num_FEmod")->SetAddress(&num_FEmod);*/
    }
    return;
  }
};

int main(int argc, char* argv[]){
  //cla
  TimeExtractor * te;

  TApplication app("MyApp",0,0); // this is to allow pop up of graphical windows in root -> see root documentation

  if (argc>1){
    int start_args = 1;
    bool options = true;
    string scan_type = "", outfile = "";
    while (start_args < argc && options){
      options = false;
      int i = start_args;
      if (!strcmp(argv[i],"-s")){
	scan_type = argv[++i];
	i++;
	options = true;
      }
      if (!strcmp(argv[i],"-o")){
	outfile = argv[++i];
	i++;
	options = true;
      }
      start_args = i;
    }
    cout << outfile << endl;
    if (outfile != ""){
      te = new TimeExtractor(outfile);
    }
    else {
      te = new TimeExtractor("TimingTreeFile.root");
    }
    for (int i=start_args; i<argc; i++){
      cout << "!!!!!!!!!!!!!!!NEW FILE!!!!!!!!!!!!!!!" << endl;
      cout << argv[i] << endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" << endl;
      DbRecord* topRec;
      try {
	RootDb* myDb;
	string myDbName = argv[i];
	te->SetFile(myDbName);
	myDb = new RootDb(myDbName, "READ");
	topRec = myDb->readRootRecord();
      }
      catch(...){
	cout << "Exception in reading file " << argv[i] << endl;
	continue;
      }      
      for (dbRecordIterator it = topRec->recordBegin(); it != topRec->recordEnd(); it++){
	if ((*it)->getClassName() == "PixScanResult"){
	  if (scan_type == "" || strstr((*it)->getName().c_str(),scan_type.c_str())){
	    cout << "####################" << endl;
	    cout << (*it)->getName() << endl;
	    te->SetScan((*it)->getName());
	    cout << "####################" << endl;
/*	    cout << "--------------------" << endl;
	    cout << "All RODs" << endl;
	    cout << "--------------------" << endl;
	    te->TimeDiffTot(*it);
*/	    int nRods = 0;
	    for (dbRecordIterator iit = (*it)->recordBegin(); iit != (*it)->recordEnd(); iit++){
	      if ((*iit)->getClassName() == "PixModuleGroup"){
		cout << "--------------------" << endl;
		cout << (*iit)->getName() << endl; 
		cout << "--------------------" << endl;
		nRods++;
		te->AddRODName((*iit)->getName());
    for (dbRecordIterator iiit = (*iit)->recordBegin(); iiit != (*iit)->recordEnd(); iiit++){
		  int nMod = 0, nMod1 = 0, nPp0 = 0, nEmpty = 0;
		  vector<int> nmSDSP;
		  vector<int> nFEmod;
		  if ((*iiit)->getClassName() == "PixScanData"){
		    
/*Old Readout Calls
		    nPp0 = te->CountPp0s(*iiit);
		    nMod = te->CountMods(*iiit);
		    for (int is = 0; is < 4; is++){
		      nmSDSP.push_back(te->CountModsInSDSP(*iiit,is));
		    }
*/		    
		    int num_modDSP[4]; 
		    int n = 0;
		    te->Count(*iiit, &nPp0, &nMod, num_modDSP, &nFEmod);
		    for (int is = 0; is < 4; is++){
		      nmSDSP.push_back(num_modDSP[is]);
		    }
		     for (vector<int>::iterator it = nFEmod.begin(); it != nFEmod.end(); it++)
		      {
                        n+=*it;
//			cout << "Module " << n << ": " << (*it) << " FEs enabled" << endl;
		      }
		    nEmpty = te->CheckEmptyHistos(*iiit);
		    if (!nEmpty){
		      te->TimeDiffRod(*iiit);
		      for (dbRecordIterator iiiit = (*iiit)->recordBegin(); iiiit != (*iiit)->recordEnd(); iiiit++){
			if ((*iiiit)->getClassName() == "PixScan_Histo") {
			  for (dbRecordIterator iiiiit = (*iiiit)->recordBegin(); iiiiit != (*iiiit)->recordEnd(); iiiiit++){
			    if ((*iiiiit)->getClassName() == "PixScanHisto") {
			      nMod1++;
			    }
			  }
			}
			cout << "--------------------" << endl;
			cout << n << " FEs on " << nMod << " module(s) scanned (test " << nMod1 << ") in " << nPp0 << " PP0(s)" << endl;
			cout << "--------------------" << endl;
			te->SetNumMod(nMod);
			te->SetNumPp0(nPp0);
			te->SetModsInSDSP(nmSDSP);
			te->SetFEsInMod(nFEmod);
			break;
		      }
		    }
		  }
		}
	      }
	    }
	    te->SetNumRods(nRods);
    
	    
      cout << "--------------------" << endl;
      cout << "All RODs" << endl;
      cout << "--------------------" << endl;
      te->TimeDiffAll();
      cout << "--------------------" << endl << endl;

      cout << "####################" << endl << endl;	  
    
      te->FillTree();
      te->clear();
      

     }
	}
      }
    }
    te->SaveTree();
  }
  else {
    cout << "Usage: ./CheckScanTiming [options] <filenames>\n"
	      << "           // dumps timing info from files to screen\n"
	      << "Options: -s <scantype>\n"
	      << "           // analyse only scans of given type\n"
              << "         -o <outfile>\n"
	      << "           // write roottuple to file <outfile>"
              << "           // default outfile name is TimingTreeFile.root"        
              << std::endl;
  }
  te->Finalize();
  return 0;
}

