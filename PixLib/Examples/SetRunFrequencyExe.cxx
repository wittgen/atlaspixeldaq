// Written by Hendrik Czirr, University of Siegen 23.04.08
// Program is changing run trigger frequency by modifying
// .dat file for the LTP Pattern Generator

#include <iostream>
#include <string>
#include <vector>
#include <sstream> 
#include <fstream>
#include <stdlib.h>

using namespace std;

void usage(){

   cout << "SetRunFrequency [-h/--help] pattern_gen_file [rate(Hz)]\n\n";
}
	
int main(int argc, char **argv) {
  

//  string triggerFileName = getenv("TDAQ_DB_DATA");
//  triggerFileName.erase(triggerFileName.find_last_of("/"),triggerFileName.length());
//  triggerFileName.erase(triggerFileName.find_last_of("/"),triggerFileName.length());
//  triggerFileName+="/segments/configs/pg_Trigger.dat";
  if(argc != 2){ usage(); exit(1); }
  string par = argv[1];
  if( par == "-h" || par == "--help") { usage(); exit(1); } 

  string triggerFileName = argv[1];


  ifstream Trigger;;
  Trigger.open(triggerFileName.c_str());
  if (!Trigger.is_open())
    {
      cout << endl;
      cout << endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< endl;
      cout << "------------------------------------------------"<<endl;
      cout << "ERROR opening pattern generator file "<< triggerFileName << " for reading, exiting..."<< endl;
      cout << "------------------------------------------------"<<endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<endl;
      cout<<endl;
      cout<<endl;
      exit(1);
    }
  ostringstream zeros; 
  
  vector<string> data;
  double freq; 
  double old_freq;
  
  int num_zeros;
  int length_of_line;
//  int old_number_of_zeros =-1;
  int in_old_z;
  int confirm;
    
  string s;
  string check; // Checks the line
  string check_2; // Checks data line
  string old_z;
  
  bool zeroes_found = false; //to ingnore any other line with zeroes
  bool triggerline_found = false;
  bool unknownline_found = false;
  bool modified = false;

  cout << "########################################## " << endl;
  cout << " " << endl;
  cout << "Create .dat file for LTP pattern generator" << endl;
  cout << " " << endl;
  cout << "########################################## " << endl;
  while(!Trigger.eof()){
    
    getline(Trigger,s);
    check = s.substr(0,1);
    
    
    if (check == "#" ){ //ignore these lines, but preserve them in the output for easy reading
      data.push_back(s);
    } else if (check ==" "){
      check_2 = s.substr(0,21);
      
      
      if (check_2 == "  0000 0000 0000 0100" && !triggerline_found){
	triggerline_found = true;
	data.push_back(s);
      } else if (check_2 =="  0000 0000 0000 0000" && !zeroes_found){
	zeroes_found = true;
	
	if (argc == 2){
	  length_of_line = s.size();
//	  old_number_of_zeros = length_of_line - 22;
	  old_z = s.substr(22,length_of_line);
	  in_old_z = atoi(old_z.c_str());
	  
	  
	  old_freq = 1/(in_old_z * 25e-9);
	  cout << ">> The current frequency is: " << old_freq << "Hz"<< endl;
	  cout << ">> Change frequency (1=yes / 9=no)?" << endl;
	  cout << ">> ";
	  cin >> confirm;
	  cout << "  " << endl;
	  if (confirm != 9 && confirm !=1){
	    cout << ">> Please type 1=yes / 9=no: " << endl;
	    cin >> confirm;
	    if (confirm != 9 && confirm !=1){
	      cout << "----------------------------------------" << endl;
	      cout << " Default setting, no change of frequency " << endl;
	      cout << "----------------------------------------" << endl;
	      confirm = 20;
	      data.push_back(s);
	    }	
	  }
	  if (confirm == 9){
	    data.push_back(s);
	    cout << "  " << endl;
	    cout << "----------------------------------------" << endl;
	    cout << "INFO No change in frequency made" << endl;
	    cout << "----------------------------------------" << endl;
	    cout << "  " << endl;
	  }
	  if (confirm == 1){
	    // Input Frequency---------------------------------
	    
	    cout << ">> Frequency in Hz (min 41Hz): ";
	    cin >> freq;
	    
	    //Calculating Frequency
	    while(freq<41){
	      cout << ">> Frequency too low, please choose Frequ. >=41 Hz" << endl;
	      cout << ">> Frequency in Hz (min 41Hz): ";
	      cin >> freq;
	      if(freq<41){
		cout << ">> Frequency still too low default setting (41Hz) will be used" << endl;
		freq =41;
	      }
	    }
	    modified = true;
	    num_zeros = (int) (1.0/(25e-9 * freq)+0.5);
	    
	    //cout << "Number of zeros: " << num_zeros << endl;
	    
	    zeros << num_zeros;
	    // -------------------------------------------------
	    s = "  0000 0000 0000 0000 ";
	  
	    s.append(zeros.str());
	    data.push_back(s);
	   
	  }
	}
	else if (argc == 3) {
	  int freqHz;
	  freqHz = atoi(argv[1]);
	  if (freqHz < 41 || freqHz>150000) cout << "Impossible to set this frequency" << endl;
	  else {
	    num_zeros = (int) (1.0/(25e-9 * freqHz)+0.5);
	    zeros << num_zeros;
	    s = "  0000 0000 0000 0000 ";
	    s.append(zeros.str());
	    data.push_back(s);
	    modified = true;
	  }
	}
	
      } else unknownline_found = true;		
    } else unknownline_found = true;	
  }
  Trigger.close();
  if (!modified && !unknownline_found && triggerline_found && zeroes_found) exit(1);
  // writing into the same file

 

  ofstream new_Trigger;
  new_Trigger.open(triggerFileName.c_str());
  if (!new_Trigger.is_open())
    {
      cout<< endl;
      cout << endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< endl;
      cout << "------------------------------------------------"<<endl;
      cout << "ERROR opening pattern generator file " << triggerFileName << " for writing, exiting..." << endl;
      cout << "------------------------------------------------"<<endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<endl;
      cout<<endl;
      cout<<endl;
      exit(1);
    }

  if (!zeroes_found || !triggerline_found || unknownline_found) {
    cout << endl;
    cout << endl;
    cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< endl;
    cout << "------------------------------------------------"<<endl;
    cout << "WARNING pattern generator file corrupted. Creating a default file with 1000 Hz frequency. Please re-run the tool again to setup correct frequency"<<endl;
    cout << "------------------------------------------------"<<endl;
    cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<endl;
    cout<<endl;
    cout<<endl;
    new_Trigger <<"# data file for LTP pattern generator" << endl;
    new_Trigger <<"#" << endl;
    new_Trigger <<"#                     M" << endl;
    new_Trigger <<"#		      u" << endl;
    new_Trigger <<"#		      l" << endl;
    new_Trigger <<"#		      t" << endl;
    new_Trigger <<"#		      i" << endl;
    new_Trigger <<"#		      p" << endl;
    new_Trigger <<"#		      l" << endl;
    new_Trigger <<"#  ICC C     TT     O i" << endl;
    new_Trigger <<"# Bnaa aBBB BTT     r c" << endl;
    new_Trigger <<"# uhll lGGG GYYT TTLb i" << endl;
    new_Trigger <<"# siRR Rooo oPPT TT1i t" << endl;
    new_Trigger <<"# yb21 0321 0103 21At y" << endl;
    new_Trigger <<"#" << endl;
    new_Trigger <<"#" << endl;
    new_Trigger <<"  0000 0000 0000 0000 40000" << endl;
    new_Trigger <<"  0000 0000 0000 0100 1" << endl;
    new_Trigger <<"#";

 } else {
  for(unsigned int i=0; i<data.size();i++){
    //cout << data[i] << endl;
    if (i==(data.size()-1)) new_Trigger << data[i];
    else new_Trigger << data[i] << endl;
  }
 }
  new_Trigger.close();
  cout << "  " << endl;
  cout << "----------------------------------------" << endl;
  cout << "INFO File " << triggerFileName << " successfully written..."<< endl;
  cout << "----------------------------------------" << endl;
  cout << "  " << endl;
 
  //READING BACK
 
  cout << "  " << endl;
  cout << "----------------------------------------" << endl;
  cout << "INFO reading back the file " << triggerFileName << " ."<< endl;
 
  ifstream tf;
  tf.open(triggerFileName.c_str());
  if (!tf.is_open())
    {
      cout << endl;
      cout << endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"<< endl;
      cout << "------------------------------------------------"<<endl;
      cout << "ERROR opening pattern generator file "<< triggerFileName << " for reading, exiting..."<< endl;
      cout << "------------------------------------------------"<<endl;
      cout << "!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!" <<endl;
      cout<<endl;
      cout<<endl;
      exit(1);
    }
  
  zeroes_found = false;
  triggerline_found = false;
  unknownline_found = false;
  modified = false;

  while(!tf.eof()){
    getline(tf,s);
    check = s.substr(0,1);
    if (check == "#" ){ 
    } 
    else if (check ==" "){
      check_2 = s.substr(0,21);
      if (check_2 == "  0000 0000 0000 0100" && !triggerline_found){
	triggerline_found = true;
      } else if (check_2 =="  0000 0000 0000 0000" && !zeroes_found){
	zeroes_found = true;
	length_of_line = s.size();
//	old_number_of_zeros = length_of_line - 22;
	old_z = s.substr(22,length_of_line);
	in_old_z = atoi(old_z.c_str());
	old_freq = 1/(in_old_z * 25e-9);
	cout << "INFO the frequency is: " << old_freq << " Hz";
      } else unknownline_found = true;		
    }
    else unknownline_found = true;
  }
  if (zeroes_found && triggerline_found && !unknownline_found) cout << ", file is not corrupted"<< endl;
  else {
    cout << endl;
    cout << "WARNING file is corrupted, re-run the tool!"<< endl;
  }
  cout << "----------------------------------------" << endl;
  cout << "  " << endl;	
  tf.close();
}
