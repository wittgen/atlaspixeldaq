#include "PixUtilities/PixStoplessRecovery.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>
#include <thread>


using namespace PixLib;


void help(){
  std::cout<<"Please provide proper args: AutoDisOn MODE ITEM --part partitionName"<<std::endl;
  std::cout<<"Examples:"<<std::endl;
  std::cout<<"AutoDisOn DISABLE ROD_C1_S7 //Disable ROD_C1_S7"<<std::endl;
  std::cout<<"AutoDisOn DISABLE ROD_C1_S7,ROD_C3_S11 //Disable ROD_C1_S7 and ROD_C3_S11"<<std::endl;
  std::cout<<"AutoDisOn ENABLE LX_BXX_XX_MX,ROD_C3_S11 //Enable ROD_C3_S11 and module LX_BXX_XX_MX"<<std::endl;
  std::cout<<"AutoDisOn ON ROD_C1_S7 //SET AutoDisableEnable to ON for ROD_C1_S7"<<std::endl;
  std::cout<<"AutoDisOn OFF ROD_C1_S17,ROD_C3_S11 //SET AutoDisableEnable to OFF for ROD_C1_S17 and ROD_C3_S11"<<std::endl;
  std::cout<<"AutoDisOn DUMP ROD_C1_S7 //DUMP AutoDisable counters on ROD_C1_S7"<<std::endl;
  std::cout<<"AutoDisOn CLEAN ROD_C1_S7 //CLEAN AutoDisable counters on ROD_C1_S7"<<std::endl;
  std::cout<<"Supported modes: ENABLE; DISABLE; ON; OFF; CLEAN; DUMP"<<std::endl;
  std::cout<<"You can provide many RODs, SLINKs, PP0s and modules separated with commas"<<std::endl;
}

int main(int argc, char** argv) {

 std::string ipcPartitionName = "PixelInfr";
 std::string eon = "";
 std::string mod = "";

  int ipp = 1;
  while (ipp < argc) {
    std::string par = argv[ipp];
    if (par == "--part" && ipp+1 < argc) {
      ipcPartitionName = argv[ipp+1];
      ipp = ipp+2;
    } else if (par == "--help" ||  par == "-h") {
      help();
      return EXIT_FAILURE;
    } else {
      if (mod == "def") {
	mod = par;
      } else {
	eon = par;
	mod = "def";
      }
      ipp = ipp+1;
    }
  }

 if(eon.empty() || mod.empty()){
   help();
   return EXIT_FAILURE;
 }

  // Start IPCCore    
  IPCCore::init(argc, argv); 

  std::vector<std::thread> workers;

  std::map <std::string, std::string> itemMap;
    if (!PixStoplessRecovery::getParStoplessRecovery(ipcPartitionName, mod ,itemMap ) ){
      help();
      return EXIT_FAILURE;
    }

  for(auto & [rodName, item]: itemMap){
    std::cout<<"Trying to "<<eon<<" "<<item<<" for ROD "<<rodName<<std::endl;
    workers.emplace_back(std::thread(PixLib::PixStoplessRecovery::autoDisable,ipcPartitionName,eon,rodName,item));
  }

  for(auto & w : workers)w.join();

  return EXIT_SUCCESS;

}


