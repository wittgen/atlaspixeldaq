#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;

int main(int argc, char **argv) {

  if(argc<4) {
    std::cout << "USAGE: HistoNumber [partition name] [serverName] [folderToInspect] " << std::endl;
    return 1;
  }
  std::string ipcPartitionName = argv[1];
  std::string serverName = argv[2];
  std::string isServerName = std::string(OHSERVERBASE)+"PixNameServerInternal";
  std::string program = "HistoNumber";
  std::string providerName = std::string(PROVIDERBASE)+program;
  std::string fol = argv[3];
  long num;
  std::vector<std::string> countHisto;
  
  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  std::stringstream info;

  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(ipcPartition, isServerName, 
							       serverName, providerName);
    num = hInt->numberHisto(fol);
    std::cout<< " The number of histograms in the folder " << fol 
	     << " is " << num << std::endl;
    
    countHisto = hInt->superLs(fol, true);
    std::cout<< " The number of histograms in the folder " << fol 
	     << " and in *ALL* the subfolders is " << countHisto.size() << std::endl;
    
  } catch (PixHistoServerExc e) {
    if ( e.getId() == "NOIPC" || e.getId() == "NAMESERVER" || e.getId() == "NAMESERVERPROBLEM" 
	 || e.getId() == "NUMBERHISTO" || e.getId() == "IPCTIMEOUT"|| e.getId() == "NOFOLDER"
	 || e.getId() == "NOHOME" || e.getId() == "SUPERLS" || e.getId() == "EMPTYSUPERLS") {
      std::cout << e.getDescr() << std::endl;
      return 0;
    } 
  } catch (...) {
    std::cout << program << ":  Unknown Exception was thrown" << std::endl;
    return 0;
  }
  return 0;
}
