#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "Histo/Histo.h"
#include "PixController/PixScan.h"
#include "PixHistoServer/PixHistoServerInterface.h"

#include "TCanvas.h"
#include "TApplication.h"

using namespace PixLib;

int main(int argc, char **argv) {

  bool status = true;
  
  // GET COMMAND LINE
  
  // Check arguments
  if(argc<6) {
    std::cout << "USAGE: GoetzFunctions [folder path without A/B/C/] [a] [b] [c] [d]" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = "nico"; //"Partition_PixelInfrastructureNG";
  std::string serverName = "nameServer";
  std::string ohServerName = std::string(OHSERVERBASE)+"crate0";
  std::string providerName = std::string(PROVIDERBASE)+"pro";
  std::string name = argv[1];
  std::string iniName = argv[1];

  std::stringstream stringa, stringb, stringc, stringd;
  stringa << argv[2];
  stringb << argv[3];
  stringc << argv[4];
  stringd << argv[5];
  int a, b, c, d;
  stringa >> a;
  stringb >> b;
  stringc >> c;
  stringd >> d;

  // Start IPCCore
  IPCCore::init(argc, argv);
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  OHRootHistogram his;
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, ohServerName, serverName, providerName);

    std::cout << "scanHistoExist: "<< name << std::endl;
    status = hInt->scanHistoExist(name, a, b, c, d);
    if (status) std::cout << "YES!" << std::endl;
    else std::cout << "NO!" << std::endl;
    std::cout << std::endl;

    std::cout << "scanHistoName - Which is the NAME of the Histo in OH?" << std::endl;
    std::string ohName;
    ohName = hInt->scanHistoName(iniName, a, b, c, d);
    std::cout << "It is: "<< ohName << std::endl;
    std::cout << std::endl;

    std::cout << "scanHistoBack - May I download it ?" << std::endl;
    bool test = hInt->scanHistoBack(iniName, a, b, c, d, his);
    if (test) std::cout << "YES " << std::endl;
    else std::cout << "NO!" << std::endl;
  } catch (...) {
    std::cout << "GoetzFunctions. PixHistoServerInterface ERROR" <<std::endl;
  }
  return 0;
}
