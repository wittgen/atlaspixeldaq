#include "RootDb/RootDb.h"

#include "TKey.h"
#include "TTree.h"
#include "TSystem.h"

#include <signal.h>

void DumpRootDbInDir(TDirectory *cfgDir, int nObj);

void DumpRootDbObj(const char* fileName, int nObj) {
	std::cout << "File name: " << fileName << std::endl;

	gSystem->Load("PixLib");
	
	TFile *f = TFile::Open(fileName);

	std::string fName = fileName;
	fName = fName.substr(fName.find_last_of("/") + 1, fName.size() - fName.find_last_of("/") - 1);
	fName = fName.substr(0, fName.find("_00"));
	TDirectory* cfgDir = static_cast<TDirectory*>( f->GetDirectory(fName.c_str()) );
	if(!cfgDir) cfgDir = static_cast<TDirectory*>( f->GetDirectory("Def") );
	if(!cfgDir) std::cout << "Error: cannot find directory" << std::endl;
	else DumpRootDbInDir(cfgDir, nObj);
	DumpRootDbInDir(gDirectory, nObj);
	f->Close();
}

void PrintTree(TTree* t) {
  //t->Print();
  TIter next( t->GetListOfBranches() );
  TBranch *br;
	while( (br = static_cast<TBranch*>( next() )) ) {
    std::string brName = br->GetName();
    std::string brTitle = br->GetTitle();
    if(brTitle.find("/I") != std::string::npos) {
      int v; br->SetAddress(&v); br->GetEntry(0);
      std::cout << brName << "\t" << v << std::endl;
    } else if(brTitle.find("/I") != std::string::npos) {
      float v; br->SetAddress(&v); br->GetEntry(0);
      std::cout << brName << "\t" << v << std::endl;
    }
    /*
    std::cout << brName << "\t";
    std::string leafName = brName;
    size_t pos;
    if( (pos = brName.find_last_of('.')) != std::string::npos )
      leafName = brName.substr(pos+1);
    std::cout << leafName << std::endl;
    TLeaf* l = br->GetLeaf(leafName.c_str());
    br->GetEntry(1);
    if( l ) std::cout << l->GetValue() << std::endl;
    else std::cout << "NULLPTR" << std::endl;
    br->Print();
    */
  }
}

void DumpRootDbInDir(TDirectory *cfgDir, int nObj) {
	TIter next( cfgDir->GetListOfKeys() );
	TKey* key;
  static int itCnt = 0;
	while( (key = static_cast<TKey*>( next() )) ) {
    if(itCnt++ >= nObj) return;
    key->Print();
		RootDbField* cfgField  = dynamic_cast<RootDbField*>( cfgDir->Get(key->GetName()) );
		if(cfgField) cfgField->Dump();
    //TTree* treeField = dynamic_cast<TTree*>( key->ReadObj() );
    //if(treeField) PrintTree(treeField);
    if( key->ReadObj()->InheritsFrom("TDirectory") )
      DumpRootDbInDir( (TDirectory*) key->ReadObj(), nObj );
    else if( key->ReadObj()->InheritsFrom("TTree") ) {
      TTree* t = (TTree*) key->ReadObj();
      PrintTree( t );
    }
	}

}

void handle_sigpipe(int i) {
  exit(i);
}

int main(int argc, const char** argv) {
  signal(SIGPIPE, handle_sigpipe);
  const char *fName=nullptr;
  int nObj = std::numeric_limits<int>::max();
	if(argc>=2) fName = argv[1];
  if(argc>=3) nObj = std::atoi(argv[2]);
  DumpRootDbObj(fName, nObj);
	return 0;
}
