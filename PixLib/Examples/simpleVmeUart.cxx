#include "RCCVmeInterface.h"
#include "VmeUtils.hh"
#include "VmeUartReader.hh"
#include "PpcVmeInterface.h"
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>

#include <sys/ipc.h> 
#include <sys/shm.h>
#include <signal.h>
#include <chrono>
#include <atomic>
#include <csignal>
#include <thread>
#include<filesystem>

std::atomic_bool stop = false;

void signal_handler(int signum) {
    stop = true;
}

void help(){

  std::cout<<"This program reads VMEUart in a single slot"<<std::endl;
  std::cout<<"Usage singleVmeUart -s slotNumber "<<std::endl;
  std::cout<<std::endl;
  std::cout<<"Options:"<<std::endl;
  std::cout<<"-s slotNumber"<<std::endl;
  std::cout<<"Optional:"<<std::endl;
  std::cout<<"-l logDir"<<std::endl;
  std::cout<<"-c crateName"<<std::endl;
  std::cout<<"-u uartSourceId (0->master, 1->slave1, 2->slave2)"<<std::endl;

  std::cout<<std::endl;

  exit(0);
}

int main(int argc, const char** argv) {

	int slotNr = 0;
	std::string logDir = std::filesystem::current_path();
	std::string crateName ="XX";
	int uartSourceId=0;

	if(argc>=2)
	{
		for(int i = 1; i < argc; i++)
			if( *argv[i] == '-')
			{
				argv[i]++;
					switch ( *argv[i] )
					{
						case 's' : slotNr = atoi(argv [i+1]); break;
						case 'l' : logDir = argv [i+1]; break;
						case 'c' : crateName = argv [i+1]; break;
						case 'u' : uartSourceId= atoi(argv [i+1]);break;
						default : help();
					}
			}
	}

  if(slotNr <5 && slotNr>21){
    std::cout<<"Please, provide a valid slot number [5-21]"<<std::endl;
    help();
  }

    // Install the signal handler
  std::signal(SIGINT   , signal_handler);
  std::signal(SIGQUIT  , signal_handler);
  std::signal(SIGILL   , signal_handler);
  std::signal(SIGTRAP  , signal_handler);
  std::signal(SIGABRT  , signal_handler);
  std::signal(SIGIOT   , signal_handler);
  std::signal(SIGBUS   , signal_handler);
  std::signal(SIGFPE   , signal_handler);
  std::signal(SIGKILL  , signal_handler);
  std::signal(SIGSEGV  , signal_handler);
  std::signal(SIGPIPE  , signal_handler);
  std::signal(SIGTERM  , signal_handler);
  std::signal(SIGSTKFLT, signal_handler);
  std::signal(SIGSYS   , signal_handler);

  VmeUtils::checkPrograms("simpleVmeUart");
  VmeUtils::VmeUartControl( slotNr , true);//lock VME UART

  std::string rodName = "ROD_"+crateName+ "_S"+std::to_string(slotNr);

  VmeUartReader *vmeReader = new VmeUartReader(slotNr,rodName,logDir);
  vmeReader->init();

  bool skip =false;

  std::cout<<"Log file: "<<vmeReader->m_logFileName<<std::endl;
  std::cout<<"simpleVmeUart is running, please press CTRL-C to stop it."<<std::endl;

  std::ifstream logFile (vmeReader->m_logFileName);
  std::string line;

  while(!stop){
  if(!vmeReader->readVmeUart(uartSourceId, skip))break;
   if(skip){
   std::cout<<"Too many non-ascii characters, exiting..."<<std::endl;
   break;
   }
  logFile.clear();
  while(getline(logFile,line))std::cout<<line<<std::endl;
  std::this_thread::sleep_for( std::chrono::milliseconds(500) );
  }

  logFile.close();
  VmeUtils::VmeUartControl( slotNr , false);//unlock VME UART
  std::cout<<"Log file: "<<vmeReader->m_logFileName<<std::endl;
  return 0;

}


