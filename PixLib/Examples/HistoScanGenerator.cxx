#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "Histo/Histo.h"
#include "PixHistoServer/PixHistoServerInterface.h"


using namespace PixLib;

int main(int argc, char **argv) {
  // Check arguments
  if(argc<7) {
    std::cout << "USAGE: HistoScanGenerator [partition name] [server name] [disk(=D)/barrel(=B)/blayer(=BL)] [total # of histos] [scan name (without /)] provName" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string serverName = argv[2];
  std::string suffixOH = argv[3];
  std::string OhServer;

  std::stringstream stringNumberHistos;
  stringNumberHistos << argv[4];
  int numberHistos;
  stringNumberHistos >> numberHistos;

  std::string scanName = argv[5];
  std::string provName = argv[6];

  std::string mainFolder = "/" + scanName + "/";

  struct timeval t0; 
  struct timeval t1;
  int time; 

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  if(suffixOH=="D") OhServer="disk"; 
  else if(suffixOH=="B") OhServer="barrel"; 
  else if(suffixOH=="BL") OhServer="blayer";
  else {
    std::cout << "Wrong option: disk(=D)/barrel(=B)/blayer(=BL)" <<std::endl;
    return 1;
  }

  time=gettimeofday(&t0,NULL); 
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, 
							       std::string(OHSERVERBASE)+OhServer, 
							       serverName, 
							       std::string(PROVIDERBASE)+provName);

    int rodCounter = 0;
    for(int histogram=0; histogram<numberHistos; histogram++) {
      Histo *his;
      std::stringstream convertInt;
      convertInt << histogram;
      std::string histoName = "occup" + convertInt.str(); 

      his = new Histo(histoName, "LoadTest", 144, 0.5, 144.5,
 		      320, 0.5, 320.5);
      for (int i=0; i<144; i++) {
 	for (int j=0; j<320; j++) {
 	  his->set(i, j, (rand()%10000));
 	}
      }
      std::stringstream folder;
      folder << mainFolder << "ROD" << rodCounter << "/OCCUPANCY/A/B/C/"; 
      //folder << mainFolder << "ROD"; 
      //      if (rodCounter==10) rodCounter=0; // to test just 10 folders
      //else rodCounter++;
      
      rodCounter++; // to test performance in case just one histo per folder
      
      hInt->sendHisto(folder.str(), *his);
      delete his;
    }
     
  } catch (PixHistoServerExc e) {
    if (e.getId()=="IPCTIMEOUT" || e.getId()== "NONAMESERVER" || e.getId()=="NOIPC"
	|| e.getId()=="NAMESERVER" || e.getId()=="NAMESERVERPROBLEM")  {
      std::cout << e.getDescr() << std::endl; 
    } else if (e.getId() == "PUBLISHFAILED") {
      std::cout << "problems in publishing in OH" << std::endl;
    }
    return 0;
  } catch (...) {
    std::cout << "HistoScanGenerator. PixHistoServerInterface Unknown exception was thrown " << std::endl;
    return 0;  
  }
  
  time=gettimeofday(&t1,NULL);
  std::cout << "HistoScanGenerator tool spent: " 
	    << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) 
	    << " s for writing the histograms. " 
	    << std::endl;

  
  return 0;
}
