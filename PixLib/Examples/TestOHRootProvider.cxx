// Test the OHRootProvider performnce

#include <unistd.h>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <memory>

#include "PixHistoServer/my_rootReceiver.h"

#include <oh/OHRootHistogram.h>
#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>

#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoT.h>
#include <is/serveriterator.h>

#include <TClass.h> 
#include <TROOT.h>
#include <TApplication.h>
#include <TH1.h>
#include <TH2.h>
#include <TFile.h>
#include <TBenchmark.h>


int main(int argc, char **argv) 
{
  if(argc<3) {
    std::cout << "USAGE: TestOHRootProvider [partition name] [root file name]" << std::endl;
    return 1;
  }
  std::string ipcPartitionName = argv[1];
  std::string fileName = argv[2];
  std::string isServerName = "pixel_histo_server_crate0";

  std::string tempo = "Writing Time";
  std::string retrieving = "Retrieving Time";
  TBenchmark *t = new TBenchmark();

  my_rootReceiver pixelReceiver;

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  TFile* outFile = new TFile(fileName.c_str(), "RECREATE");
  //outFile->SetCompressionLevel(0);
  std::string mainFold = "SCAN";
  TDirectory* newDir = gDirectory->mkdir(mainFold.c_str());
  t->Start(retrieving.c_str());
  std::string histogramWildCard = "/S1/.*";
  OHHistogramIterator ith(*ipcPartition, isServerName, ".*", histogramWildCard); 
  while(ith++) {
    std::string histoName = ith.name();
    //std::cout << histoName << std::endl;
    try {
       ith.retrieve(pixelReceiver);
       //OHRootHistogram his=OHRootReceiver::getRootHistogram(*ipcPartition, isServerName, "pixel_provider_pro", histoName, -1);
       //TH1* copy = his.histogram.release();
       //copy->SetDirectory(newDir);
    } catch (daq::oh::ObjectNotFound) {
      std::cout << "Object not found in OH " << std::endl;
    } catch (daq::oh::InvalidObject) {
      std::cout << " Invalid Obj " << std::endl;
    } catch (...) {
      std::string mess = "Unknown exception thrown ";
    }
  }
  t->Show(retrieving.c_str());
  
  t->Start(tempo.c_str());
  outFile->Write(); 
  outFile->Close();
  t->Show(tempo.c_str());
  std::cout << "written to disk" << std::endl;

  return 0;
}

