#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <ipc/partition.h>
#include <ipc/core.h>

#include "PixDcs/PixDcs.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;



int main(int argc, char **argv) {

  // GET COMMAND LINE

  // Check arguments
  if(argc<4) {
    std::cout << "USAGE: TestFSMcommands [- FSM commands ...see list below] [Connectivity Object Name] [ROD Name]" << std::endl;
    std::cout << "FSM commads available" << std::endl;
    std::cout << "-on: switch on (module: OFF->ON)" << std::endl;
    std::cout << "-off: switch off (module: ON->OFF or PP0: STARTED->OFF)" << std::endl;
    std::cout << "-enable: enable (module DISABLED->ENABLED)" <<std::endl;
    std::cout << "-disable: disable (module ENABLED->DISABLED)" <<std::endl;
    std::cout << "-startup: startup procedure with module check (PP0: OFF->STARTED)" << std::endl;    
    std::cout << "-opto-on: switch on opto (PP0: STARTED->OPTO_ON) " << std::endl;
    std::cout << "-lv-on: switch on LV (PP0: OPTO_ON->STANDBY)" << std::endl;
    std::cout << "-hv-on: switch on HV (PP0: STANDBY->ON)" << std::endl;
    std::cout << "-hv-off: switch off HV (PP0: ON->STANDBY)" << std::endl;
    std::cout << "-lv-off: switch off LV (PP0: STANDBY->OPTO_ON)" << std::endl;
    std::cout << "-opto-off: switch on opto (PP0: OPTO_ON->STARTED) " << std::endl;
    std::cout << "-reset: reset (PP0 from any state but OFF and STARTED)" << std::endl;
    std::cout << "-reset-opto: reset optoboard (PP0 from any state but OFF and STARTED)" << std::endl;
    std::cout << "-reload: reload (PP0 from any state but OFF)" << std::endl;
    std::cout << "-vvdc-off: switch off VVDC (PP0)" << std::endl;
    return 1;
  }

  std::cout << std::endl;
  std::cout << "This is an easy test to check if the FSM-commands are correctly sent via DDC" <<std::endl;
  std::cout << std::endl; 

  std::string ipcPartitionName="PixelInfr";
  std::string isServerName="RunParams";
  std::string obj = argv[2];
  std::string rodName = argv[3];
  PixDcsDefs::FSMCommands command;
  int status;

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  PixMessages *msg = new PixMessages();
  msg->pushPartition(ipcPartition->name(), ipcPartition);
  msg->pushStream("cout", std::cout);

  // Get PixDcs Interface
  PixDcs* dcs = new PixDcs(ipcPartition, isServerName, rodName, msg); 

  try {
    std::string c=argv[1];
    if (c == "-on") {
      command = PixDcsDefs::SWITCH_ON;
    } else if (c == "-off") {
      command = PixDcsDefs::SWITCH_OFF;
    } else if (c == "-enable") {
      command = PixDcsDefs::ENABLE;
    } else if (c == "-disable") {
      command = PixDcsDefs::DISABLE;
    } else if (c == "-startup") {
      command = PixDcsDefs::STARTUP;
    } else if (c == "-opto-on") {
      command = PixDcsDefs::SWITCH_ON_OPTO;
    } else if (c == "-lv-on") {
      command = PixDcsDefs::SWITCH_ON_LV;
    } else if (c == "-hv-on") {
      command = PixDcsDefs::SWITCH_ON_HV;
    } else if (c == "-hv-off") {
      command = PixDcsDefs::SWITCH_OFF_HV;
    } else if (c == "-lv-off") {
      command = PixDcsDefs::BACK_TO_OPTO_ON;
    } else if (c == "-opto-off") {
      command = PixDcsDefs::BACK_TO_STARTED;
    } else if (c == "-reset") {
      command = PixDcsDefs::RESET;
    } else if (c == "-reset-opto") {
      command = PixDcsDefs::OPTO_RESET;
    } else if (c == "-reload") {
      command = PixDcsDefs::RELOAD;
    } else if (c == "-vvdc-off") {
      command = PixDcsDefs::SWITCH_OFF_VVDC;
    }
    status = dcs->sendCommand(command, obj);
    std::cout << "FEEDBACK IS: " << status << std::endl;
    if (status == PixDcsDefs::UNKNOWN_ERR || status == PixDcsDefs::ERR_DCS) {
      std::cout << "DDC INTERNAL ERROR" << std::endl;
    } else if (status>0) {
      std::cout << "WRONG NAME OR COMMUNICATION ERROR" << std::endl;
    }
  } catch (...) {
    std::cout << "Exception was thrown" << std::endl;
  }

  std::cout << std::endl;   
  // DELETE
  delete dcs;
  
  return 0;
}

