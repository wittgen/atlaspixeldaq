#include "iblModuleConfigStructures.h"
#include "moduleConfigStructures.h"
#include "PixDbInterface/PixDbInterface.h"
#include "RootDb/RootDb.h"
#include "PixModule/PixModule.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"

#include <iostream>
#include <exception>
#include <stdlib.h>
#include <sstream>
int main(int argc, char* argv[]){
  
  try{
    // argument 1 ist the file name
    if(argc<2){
      std::cout << "USAGE: GenPixFeI4 [RootDb output file name] <-nFE n> <-FEI4B>" << std::endl;
      return -1;
    }
    string argv1(argv[1]); // Db file name
    // argument 2 is optional and specifies the no. of FEs
    int nFE = 1, offset = 0;
    if(argc>3 && string(argv[2])=="-nFE"){
      std::stringstream a;
      a << argv[3];
      a >> nFE;
      offset = 2;
    }
    // argument 2 or 4 is optional and indicates we want FE-I4B, not I4A
    bool i4a=true;
    if(argc>(2+offset) && string(argv[2+offset])=="-FEI4B") i4a=false;
    // create Db file and fill with module and FE records
    PixDbInterface* myDb = new RootDb(argv1, "NEW");
    DbRecord* myRoot = myDb->readRootRecord();
    DbRecord* modRec = myDb->makeRecord("PixModule", "M990001");
    dbRecordIterator findRec = myRoot->pushRecord(modRec);
    modRec = *(myDb->DbProcess(findRec,PixDb::DBREAD));
    for(int iFE=0; iFE<nFE; iFE++){
      std::stringstream a;
      a << iFE;
      DbRecord* feRec = myDb->makeRecord("PixFe", "PixFe_"+a.str());
      findRec = modRec->pushRecord(feRec);
      feRec = *(myDb->DbProcess(findRec,PixDb::DBREAD));
      // add class description to make sure an FE-I4 is created
      std::string classname="ClassInfo_ClassName";
      DbField* clField = myDb->makeField(classname);
      feRec->pushField(clField);      
      dbFieldIterator findField = feRec->findField(clField->getName());
      myDb->DbProcess(findField, PixDb::DBREAD);
      string clcont(i4a?"PixFeI4A":"PixFeI4B");
      myDb->DbProcess(findField,PixDb::DBCOMMIT,clcont);
      DbRecord* regRec = myDb->makeRecord("GlobalRegister", "GlobalRegister_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
      regRec = myDb->makeRecord("PixelRegister", "PixelRegister_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
      regRec = myDb->makeRecord("Trim", "Trim_0");
      findRec = feRec->pushRecord(regRec);
      myDb->DbProcess(findRec,PixDb::DBREAD);
    }

    // create a PixModule from Db and save FE default setting back to it
    PixModule *pm = new PixModule(modRec, 0, "M990001");
    // set correct MCC, FE flavour
    ((ConfList&)pm->config()["general"]["MCC_Flavour"]).setValue(PixModule::PM_NO_MCC);
    ((ConfList&)pm->config()["general"]["FE_Flavour"]).setValue(i4a?PixModule::PM_FE_I4A:PixModule::PM_FE_I4B);
    pm->saveConfig(modRec);
    delete myDb;

  }catch(...){
    cout << "Error!" << endl;
    return -1;
  }
  return 0;
}
