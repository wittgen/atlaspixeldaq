#include "PixModuleGroup/PixModuleGroup.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeData.h"
#include "PixFe/PixFeStructures.h"
#include "PixFe/PixFeI3.h"
#include "PixFe/PixFeI2.h"
#include "PixMcc/PixMcc.h"
#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "Config/ConfMask.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixBoc/PixBoc.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixBoc/PixBoc.h"

#include <TROOT.h>
#include <TH1.h>
#include "TH2.h"
#include <TKey.h>
#include <TFile.h>
#include "TCanvas.h"
#include "TApplication.h"

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <dirent.h>
#include <unistd.h>
#include <wait.h>
#include <regex>

struct timeval ti;
struct timeval tf;
int timeElapsed;


PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;
PixConnectivity *conn; 

std::string partitionName = "all partitions";
std::string     crateName = "all crates";
std::string       rodName = "all rods";
std::string       pp0Name = "all pp0s";
std::string        clName = "all cls";
std::string    moduleName = "all modules";

std::string idTag     = "PIXEL18";
std::string connTag   = "";
std::string cfgTag    = "";
std::string cfgModTag = "";
std::string destCfgModTag = "";
std::string pendTag = "_Tmp";
std::string domain  = "Configuration-"+idTag;
std::string prefix  = "";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
std::string dacName = "";

bool noIbl = false;
bool save = false;

void openDbServer(int argc, char** argv) {   
  IPCCore::init(argc, argv);                                                       
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}



void help(){
      std::cout << std::endl;
      std::cout << "Usage CopyModuleConfig --idTag <idTag> --connTag <conTag> --cfgTag <cfgTag> --cfgModTag <cfgModTag> --destTag <destTag> \n"  
		<< "            --dbPart <partition> IPC Part name (def. PixelInfr)\n"
	        << "            --dbName <name>      DbServer name (def. PixelDbServer)\n"
		<< "            --idTag  <idtag>     Db idTag [from it also the domain] (def. PIXEL18)\n"
		<< "            --cfgTag <name>      Configuration TAG \n"
		<< "            --cfgModTag <name>   Module Configuration TAG (default _Tmp) \n"
		<< "            --destTag <name>     destination Module Configuration TAG (should exist) \n"
		<< "            --pendTag <name>     pending tag for DbServer (default _Tmp) \n"
		<< " \n"
	        << "            --save               save changes in DbServer with selected pendTag (def. NoSave mode [does not do anything])\n"
		<< " \n"
		<< "            --partitionName <name>  Specify a partition explicitly (def. all)\n"
	        << "            --crateName <name>      Specify a crate explicitly     (def. all)\n"
	        << "            --rodName <name>        Specify a rod explicitly       (def. all)\n"
	        << "            --pp0Name <name>        Specify a pp0 explicitly       (def. all)\n"
	        << "            --moduleName <name>     Specify a module explicitly    (def. all)\n"
	    	<< "            --noIBL                 exlude IBL partition (PIXEL_I)\n"
	        << "            --3DIBL                 Only IBL 3D sensors\n"
		<< std::endl << std::endl;
      exit(-1);

}

void copyModuleTag(ModuleConnectivity* modConn) {

  EnumMCCflavour::MCCflavour mccFlv;
  EnumFEflavour::FEflavour feFlv;
  int nFe;
  modConn->getModPars(mccFlv, feFlv, nFe);
  PixModule *mod = new PixModule(DbServer,(PixModuleGroup*)NULL,domain,cfgModTag, modConn->prodId(),mccFlv,feFlv,nFe);

 if(save){
    if(pendTag == "")
      mod->writeConfig(DbServer, domain, destCfgModTag);
    else 
      mod->writeConfig(DbServer, domain, destCfgModTag, pendTag);
  }
}

bool is3DIBL(std::string modName){

std::regex regEx("LI_S([0-9]{1,2})_[AC]_M4_[AC][7-8]");
std::smatch match;
return std::regex_match(modName, match, regEx);

}

int main(int argc, char** argv) {

int ip = 1;
  bool found=false;
  bool only3DIBL = false;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help();
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsPart = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsName = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--idTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  idTag = argv[ip+1];
	  domain = "Configuration-";
	  domain += idTag;
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No idTag name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--connTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  connTag = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No connTag name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--cfgTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgTag = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No cfgTag name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--cfgModTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgModTag = argv[ip+1];
	  ip += 2; 
	  found = true;
	} else {
	  std::cout << std::endl << "No cfgmodTag name inserted" << std::endl << std::endl;
	  help();
	}
      }else if (std::string(argv[ip]) == "--destTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  destCfgModTag = argv[ip+1];
	  ip += 2; 
	  found = true;
	} else {
	  std::cout << std::endl << "No destTag name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--pendTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pendTag = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No pendingTag name inserted" << std::endl << std::endl;
	  help();
	}
      } else if (std::string(argv[ip]) == "--save") {
	save = true;
	ip+=2; 
      } else if (std::string(argv[ip]) == "--3DIBL") {
	only3DIBL = true;
	ip+=2;
      } else if (std::string(argv[ip]) == "--noIBL") {
	noIbl = true;
	ip+=2;
      } else if (std::string(argv[ip]) == "--partitionName") {
	if (ip+1 < argc  && std::string(argv[ip+1]).substr(0,2) != "--") {
	  partitionName = argv[ip+1];
	  ip++; 
	} else {
	  std::cout << std::endl << "No partitionName name inserted" << std::endl << std::endl;
	  help();
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--crateName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  crateName = argv[ip+1];
	  ip++; 
	} else {
	  std::cout << std::endl << "No crateName name inserted" << std::endl << std::endl;
	  help();
	}
        ip++; 
      }
      else if (std::string(argv[ip]) == "--rodName") {
	if (ip+1 < argc &&  std::string(argv[ip+1]).substr(0,2) != "--") {
	  rodName = argv[ip+1];
	  ip++; 
	} else {
	  std::cout << std::endl << "No rodName name inserted" << std::endl << std::endl;
	  help();
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--pp0Name") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pp0Name = argv[ip+1];
	  ip++; 
	} else {
	  std::cout << std::endl << "No pp0Name name inserted" << std::endl << std::endl;
	  help();
	}
        ip++; 
      }  else if (std::string(argv[ip]) == "--moduleName") {
	if (ip+1 < argc &&  std::string(argv[ip+1]).substr(0,2) != "--") {
	  moduleName = argv[ip+1];
	  ip++; 
	} else {
	  std::cout << std::endl << "No moduleName name inserted" << std::endl << std::endl;
	  help();
	}
        ip++; 
      } else {
	std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
	help();
      }
    }
  }

  timeElapsed=gettimeofday(&ti,NULL);  

  try{
    // Open DB server connection
    if (DbServer == NULL) openDbServer(argc, argv);
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag,idTag);
    conn->loadConn();

    std::vector<std::string> tagCfglist;
    DbServer->listTagsRem("Configuration-"+idTag,tagCfglist);
    bool foundCfg=false;
    bool foundCfgMod=false;
    bool foundDestCfgModTag = false;
    for (unsigned int i=0; i<tagCfglist.size(); i++) {
      if (tagCfglist[i]==cfgTag) foundCfg=true;
      if (tagCfglist[i]==cfgModTag) foundCfgMod=true;
      if (tagCfglist[i]==destCfgModTag) foundDestCfgModTag=true;
    }
    if (!foundCfg) {
      std::cout << std::endl << "FATAL: cfgTag " << cfgTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    if (!foundCfgMod) {
      std::cout << std::endl << "FATAL: cfgModTag " << cfgModTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    if (!foundDestCfgModTag) {
      std::cout << std::endl << "FATAL: destCfgModTag " << destCfgModTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    
      std::cout << " Working on conn tag: " << connTag << ",  cfgTag: " << cfgTag << ", cfgModTag: " << cfgModTag <<", destCfgModTag: " << destCfgModTag <<std::endl; 
      std::cout << " Running on Partition: " << partitionName << ",  crate: " << crateName << ", ROD: " << rodName <<", PP0: " << pp0Name<< " Module "<< moduleName <<std::endl; 

    // loop over partitions
    std::map< std::string, PartitionConnectivity* >::iterator partit;
    for (partit=conn->parts.begin(); partit!=conn->parts.end(); ++partit) {
      if(noIbl == true && partit->second->name() == "PIXEL_I") continue;
      if(partit->second->name() == partitionName || partitionName == "all partitions"){
	  std::cout << " Working on partition: " << partit->second->name() << std::endl; 
	std::map<std::string, RodCrateConnectivity*>::iterator rodcrateit;
	for (rodcrateit = partit->second->rodCrates().begin(); rodcrateit != partit->second->rodCrates().end(); ++rodcrateit){
	  if(rodcrateit->second->name() == crateName || crateName == "all crates"){	
	      std::cout << " Working on crate: " << rodcrateit->second->name() << std::endl; 

	    std::map<int, RodBocConnectivity*>::iterator rodbocit;
	    for (rodbocit = rodcrateit->second->rodBocs().begin(); rodbocit != rodcrateit->second->rodBocs().end(); ++rodbocit){
	      if(rodbocit->second->name() == rodName || rodName == "all rods"){	
		  std::cout << " Working on RodBoc: " << rodbocit->second->name() << std::endl; 
		  int obslot;
		  for (obslot = 0; obslot < 4; obslot++){
		    // tmp for the absence of cooling loops
		    OBConnectivity* obConn = NULL;
		    obConn = rodbocit->second->obs(obslot);
		    if(obConn == NULL)continue;
		      if(obConn->pp0()->name() == pp0Name || pp0Name == "all pp0s"){
			    int moduleslot;
			    for(moduleslot = 1; moduleslot < 9; moduleslot++){
			      ModuleConnectivity * modConn = NULL;
			      modConn = obConn->pp0()->modules(moduleslot);
			      if(modConn == NULL)continue;
				if(modConn->name() == moduleName || moduleName == "all modules"){	  
				    if(only3DIBL && !is3DIBL(modConn->name()))continue;
				    
				    std::cout << " Working on module: " << modConn->name() << std::endl;
				    copyModuleTag(modConn);
				}
			    }
			  }
		      }
		}
	      }	    
	    }
	  }
	}
    }

    timeElapsed=gettimeofday(&tf,NULL);  
    std::cout << std::endl << std::endl <<  "Time for this operation is:  " <<tf.tv_sec - ti.tv_sec+ 1e-6*(tf.tv_usec-ti.tv_usec) << " s " << std::endl << std::endl;
    std::cout << std::endl << std::endl;
  }
  catch(PixDBException& e) {
    std::cerr<<"Got PixDBException: ";
    e.what(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    return -1;
  }
  return 0;
}


