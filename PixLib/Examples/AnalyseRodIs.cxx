/*********************************************************/
/* Decode StdDump file of RodRegisters for Diagnostics   */
/*                                                       */
/*  checks enabled formatter links and dumps status      */
/*  checks XOFF status and backpressure                  */
/*  checks FIFO status                                   */
/*                                                       */
/* uses:                                                 */
/* FMT_LINK_EN(i)		                         */
/* FMT_TIMEOUT_ERR(i)		                         */
/* FMT_DATA_OVERFLOW_ERR(i)	                         */
/* FMT_HEADER_TRAILER_ERR(i)	                         */
/* FMT_ROD_BUSY_ERR(i)		                         */
/* FMT_MODEBIT_STAT(i)		                         */
/* FMT_LINK_OCC_CNT(i,j)	                         */
/* FMT_PXL_BANDWIDTH(i)		                         */
/* RRIF_STATUS_1		                         */
/* RRIF_CMND_1                                           */
/* EVENT_HEADER_DATA                                     */
/* EV_FIFO_DATA1	                                 */
/* EV_FIFO_DATA2                                         */
/* RTR_CMND_STAT                                         */
/* EFB_RUNTIME_STAT_REG                                  */
/* EVT_MEM_FLAGS                                         */
/*                                                       */
/*                                                       */
/*     Andreas Korn LBNL (Andreas.Korn(AT)cern.ch) 12/06 */
/*                                                       */
/*********************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <set>
#include <string>
#include <ipc/partition.h>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixUtilities/PixISManager.h"

#include "RodCrate/registeraddress.h"
//#include "sysParams.h"

using namespace std;
using namespace PixLib;

#include <ctype.h>

#define N_FORMATTERS  8
#define ROD_BUSY_STATUS           (4 + RRIF_STATUS_0)

int main(int argc, char *argv[]) {

  bool verbose = true;
  std::string ipcPartitionName;
  std::string isServerName = "RunParams";
  std::string rodName;
  std::string crateName;

  // Check arguments
  if(argc<3) {
    std::cout << "usage: ./AnalyseRodIs CrateName RodName" << std::endl;
    exit(0);
  } else {
    char *part;
    part = getenv("PIX_PART_INFR_NAME");
    if (part != NULL) {
      ipcPartitionName = part;
    } else {
      ipcPartitionName = "PixelInfr";
    }
    rodName = argv[2];
    crateName = argv[1];
  }

  // Start IPCCore
  IPCCore::init(argc, argv);
  
  // Create IPCPartition
  std::cout << "Getting partition " << ipcPartitionName << std::endl;

  // CREATE ISManager
  PixISManager* m_isManager;
  m_isManager = new PixISManager(ipcPartitionName,isServerName);
 


  // start address of FPGA sections of ROD
  long unsigned int dspStart[4]    = {0x400000,0x402000, 0x402400, 0x404400 };

  /*********************************************************************************8*/
  string modebits[4]               = {"play", "mask", "skip", "drop"};
  string fmtfifostat[16]           = {"unknown", "reset", "idle" , "fetch modebits", 
				      "wait for MB", "wait for token", "read out link", "wait for data",
				      "dump event", "next link", "timeout", "overflow", 
				      "backpressure", "pass token", "other", "others"};

  string rrif_status_bit[32]      = {" TIM CLOCK OK",
				     " BOC CLOCK OK",
				     " BOC busy",
				     " Inmem Done",
				     " CAL test ready",
				     " FE Trigger FIFO empty",
				     " FE Trigger FIFO full",
 				     " Formatter bank A: EFB stop output",
				     " Formatter bank A: modebit FIFO full",
 				     " Formatter bank B: EFB stop output",
				     " Formatter bank B: modebit FIFO full",
				     " Formatter bank A: Header/Trailer limit",
				     " Formatter bank B: Header/Trailer limit",
				     " Formatter bank A: Busy limit",
				     " Formatter bank B: Busy limit",
				     " EFB dyn mask FIFO empty",
				     " EFB dyn mask FIFO full",
				     " EFB Event ID empty ERROR",
				     " Event memory A FIFO empty",
				     " Event memory A FIFO full",
				     " Event memory B FIFO empty",
				     " Event memory B FIFO full",
				     "",
				     "",
				     "",
				     "",
				     "",
				     "",
				     "",
				     " FE Occ counters ALL ZERO",
				     " Command Mask READY"};

  string rrif_cmnd_bit[32]        = {" FE command output enabled",
				     " TIM is input source",
				     " New mask READY",
				     " FE occ Counters decrement on trailer",
				     " ?",
				     " FE Command Pulse Counter ENABLED",
				     " FE Command Pulse Counter LOAD",
 				     " ?",
				     " TIM EVENT ID Trigger decoder ENABLED",
 				     " ?",
				     " FMT mode bits Encoder ENABLED",
				     " ?",
				     " EFB dyn mask Encoder  ENABLED",
				     " ?",
				     " ?",
				     " ?",
				     " Test Bench ENABLED",
				     " Test Bench RUN",
				     " Trigger command decoder ENABLED",
				     " Configuration read back ENABLED",
				     " Command mask LOAD",
				     " STATIC BCID",
				     " STATIC L1ID",
				     " corrective mask READY",
				     " Inhibit Inmem Playpack",
				     " ?",
				     " ?",
				     " ?",
				     " ?",
				     " internal scan engine START",
				     " internal scan engine CLEAR",
				     ""};

  string control_signal_mux[16]  = {" DISABLE FE inputs",
				    " LOAD/READ FIFO's with DSP",
				    " FE inputs to TestBench",
				    " DISABLE FE inputs",
				    " NORMAL DATA PATH",
				    " CONFIGURATION READBACK MODE",
				    " CALIBRATION TEST MODE",
				    " CAPTURE data in INMEMS",
				    "",
				    "",
				    "",
				    "",
				    "",
				    "",
				    "",
				    ""};

  string rtr_cmnd_stat_bit[16]   = {" DUMP ATLAS event",
				    " DUMP TIM event",
				    " DUMP ROD event",
				    " S-Link write INHIBIT",
				    " S-Link RESET",
				    " S-Link TEST",
				    " Backpressure to EFB ENABLED",
				    " S-Link down MASKED",
				    " S-Link XOFF",
				    " S-Link DOWN",
				    " BACKPRESSURE to EFB",
				    " 40MHz DLL LOCKED",
				    " 80MHz DLL LOCKED",
				    " S-Link lateched XOFF",
				    " S-Link little Endian",
				    ""};

  string efb_runtime_stat_bit[16]   = {" Event FIFO A almost FULL",
				       " Error summary FIFO A almost FULL",
				       " Event ID Engine A EMPTY",
				       " Engine A Pause Formatters",
				       " Event FIFO B almost FULL",
				       " Error summary FIFO B almost FULL",
				       " Event ID Engine B EMPTY",
				       " Engine B Pause Formatters",
				       " Router HALT output",
				       " Event ID Data FIFO almost FULL",
				       " ROL test block ENABLED",
				       " Synch Event ERROR",
				       " ",
				       " ",
				       " ",
				       " EFB DLL LOCKED" };

  string evt_mem_flags_bit[24]   = {" FIFO A EMPTY",
				    " FIFO A almost EMPTY",
				    " FIFO A almost FULL",
				    " FIFO A FULL",
				    " FIFO B EMPTY",
				    " FIFO B almost EMPTY",
				    " FIFO B almost FULL",
				    " FIFO B FULL",
				    " Header/Trailer FIFO EMPTY",
				    " Header/Trailer FIFO FULL",
				    " Event ID FIFO A EMPTY",
				    " Event ID FIFO A almost FULL",
				    " Event ID FIFO B EMPTY",
				    " Event ID FIFO B almost FULL",
				    " Header Event ID FIFO EMPTY",
				    " Header Event ID FIFO almost FULL",
				    " Header Event ID FIFO FULL",
				    " Event Data FIFO FULL",
				    " EVENT ID FIFO A FULL",
				    " EVENT ID FIFO B FULL",
				    " Word count FIFO A FULL",
				    " Word count FIFO B FULL",
				    " Error Summary FIFO A FULL",
				    " Error Summary FIFO B FULL"};

  string trap_bits[8] = {"ATLAS Event", 
			 "TIM Event", 
			 "ROD Event" , 
			 "using error format", 
			 "using S-Link format", 
			 "All Events", 
			 "using more than one Event per Frame", 
			 "Error Events" };


  string rod_busy_status_bits[24] = {"Main ROD Busy (latched)",
			      "FMT EFB Bank A Busy (latched)",
			      "FMT EFB Bank B Busy (latched)",
			      "EFB Event Header FIFO Busy (latched)",
			      "Pause ROL Triggers (latched)",
			      "Pause FMT Bank A from EFB (latched)",
			      "Pause FMT Bank B from EFB (latched)",
			      "Dsp pause triggers (latched)",
			      "Sweeper Event (latched)",
			      "FMT Bank A Header Trailer Limit (latched)",
			      "FMT Bank B Header Trailer Limit (latched)",
			      "?",
			      "Main ROD Busy (not-latched)",
			      "FMT EFB Bank A Busy (not-latched)",
			      "FMT EFB Bank B Busy (not-latched)",
			      "EFB Event Header FIFO Busy (not-latched)",
			      "Pause ROL Triggers (not-latched)",
			      "Pause FMT Bank A from EFB (not-latched)",
			      "Pause FMT Bank B from EFB (not-latched)",
			      "Dsp pause triggers (not-latched)",
			      "Sweeper Event (not-latched)",
			      "FMT Bank A Header Trailer Limit (not-latched)",
			      "FMT Bank B Header Trailer Limit (not-latched)",
			      "?"};


  /* by default the formatter dump contains ALL rod registers */
  /* load saved register dump into buffer for decoding        */ 
#define registerlength 0x2000
  unsigned int * buffer = new unsigned int[registerlength];
  int time;
  std::vector<unsigned int> v = m_isManager->read< std::vector<unsigned int> >(crateName+"/"+rodName+"/STDDUMP", time);
  for (std::size_t iv=0; iv<v.size(); iv++) {
    if (iv < 0x2000) buffer[iv] = v[iv];
  }

  /* loop over enabled Formatters Links and dump status   */
  for(int i = 0; i<N_FORMATTERS;i++){
    //    printf(" Enabled Links:  ");
    printf(" FMT %d: status %s", i, fmtfifostat[(buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4] >> 28 ) & 0xF].c_str()); 
    if((buffer[(EFB_FORMATTER_STAT-dspStart[0])/4] & (1 << i)))printf(" STUCK");
    printf("\n");
    for(int j = 0; j<4;j++){
      if(buffer[(FMT_LINK_EN(i)-dspStart[0])/4] & (1<<j)){
	printf("  ENABLED %x:%x ", i, j);
      } else {
	printf(" DISABLED %x:%x ", i, j);
      } 
	if(buffer[(FMT_TIMEOUT_ERR(i)-dspStart[0])/4] & (1<<j))       printf(" TIMEOUT ");
	if(buffer[(FMT_DATA_OVERFLOW_ERR(i)-dspStart[0])/4] & (1<<j)) printf(" OVERFLOW ");
	if(buffer[(FMT_HEADER_TRAILER_ERR(i)-dspStart[0])/4] & (1<<j))printf(" HEADER/TRAILERLIMIT ");
	if(buffer[(FMT_ROD_BUSY_ERR(i)-dspStart[0])/4] & (1<<j))      printf(" BUSY ");
	printf(" MB %s", modebits[((buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4] >> (2*j) ) & 0x3)].c_str()); 
	printf(" 0x%lx words pending ", buffer[(FMT_LINK_OCC_CNT(i,j)-dspStart[0])/4]);
	printf("\n");
      //}
    }
    if(verbose){
      printf(" FMT_LINK_EN  FMT %d: 0x%x 0x%lx\n", i, FMT_LINK_EN(i), buffer[(FMT_LINK_EN(i)-dspStart[0])/4]);
      printf("FOR KERSTIN !!! FMT_EXP_MODE_EN  FMT %d: 0x%x 0x%lx\n", i, FMT_EXP_MODE_EN(i), buffer[(FMT_EXP_MODE_EN(i)-dspStart[0])/4]); // Added by Jens to read out ECR reset bit
      printf(" Link mapping FMT %d: 0x%x 0x%lx\n", i, FMT_PXL_BANDWIDTH(i), buffer[(FMT_PXL_BANDWIDTH(i)-dspStart[0])/4]);
      printf(" FMT_MODEBIT_STAT %d: 0x%x 0x%lx\n", i, FMT_MODEBIT_STAT(i), buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4]);
      printf(" FMT_HEADER_TRAILER_LIMIT %d: 0x%x 0x%lx\n", i, FMT_HEADER_TRAILER_LIMIT(i), buffer[(FMT_HEADER_TRAILER_LIMIT(i)-dspStart[0])/4]);
      printf(" FMT_ROD_BUSY_LIMIT %d: 0x%x 0x%lx\n", i, FMT_ROD_BUSY_LIMIT(i), buffer[(FMT_ROD_BUSY_LIMIT(i)-dspStart[0])/4]);
      printf(" FMT_READOUT_TIMEOUT %d: 0x%x 0x%lx\n", i, FMT_READOUT_TIMEOUT(i), buffer[(FMT_READOUT_TIMEOUT(i)-dspStart[0])/4]);
    }
  }
  printf("============================================================\n\n");


  /* Dump Status bits  */
  printf(" RUN Number 0x%x 0x%lx\n", 0x402208, buffer[(0x402208-dspStart[0])/4]);
  printf(" ECR preset 0x%x 0x%lx\n", 0x404448, buffer[(0x404448-dspStart[0])/4]);
  printf(" ECR Count 0x%x 0x%lx\n", 0x40444c, buffer[(0x40444c-dspStart[0])/4]);
  printf(" RTR_CMD_STAT 0x%x 0x%lx\n", 0x402500, buffer[(0x402500-dspStart[0])/4]);
  printf(" RRIF_STATUS_0 0x%x 0x%lx\n", RRIF_STATUS_0, buffer[(RRIF_STATUS_0-dspStart[0])/4]);
  printf(" RRIF_STATUS_1 0x%x 0x%lx\n", RRIF_STATUS_1, buffer[(RRIF_STATUS_1-dspStart[0])/4]);
  printf("   RRIF_STATUS_1 FE Command Pulse counters: 0x%lx\n", (buffer[(RRIF_STATUS_1-dspStart[0])/4]>>29)& 0xFF);
  for(int i=0;i<22;i++){
    if((buffer[(RRIF_STATUS_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_status_bit[i].c_str());
  }
  for(int i=30;i<32;i++){
    if((buffer[(RRIF_STATUS_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_status_bit[i].c_str());
  }
  /* Dump BuSY Status bits  */
  printf(" ROD_BUSY_STATUS 0x%x 0x%lx\n", ROD_BUSY_STATUS, buffer[(ROD_BUSY_STATUS-dspStart[0])/4]);
  for(int i=0;i<24;i++){
    if((buffer[(ROD_BUSY_STATUS-dspStart[0])/4]) & (1<<i))printf("   %s\n",rod_busy_status_bits[i].c_str());
  }


  /* Dump Command (Rod Configuration) bits  */
  printf(" RRIF_CMND_1 0x%x 0x%lx\n", RRIF_CMND_1, buffer[(RRIF_CMND_1-dspStart[0])/4]);

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & 0x1){
    printf("    FE Command Output ENABLED\n");
  }else{
    printf("    FE Command Output DISABLED\n");
  }

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & 0x2){
    printf("    Command input source is TIM\n");
  }else{
    printf("    Command input source is DSP\n");
  }
  for(int i=2;i<24;i++){
    if((buffer[(RRIF_CMND_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_cmnd_bit[i].c_str());
  }

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & (1<<24)){
    printf("    Inmem playback INHIBIT\n");
  }else{
    printf("    PLAYBACK Inmems\n");
  }

   printf("   %s\n", control_signal_mux[(buffer[(RRIF_CMND_1-dspStart[0])/4] >>25) & 0xF].c_str());


   /* Dump used event ID FIFO data */
  printf("=========================================================================\n\n");
  printf(" last EVENT used in EFB  : L1ID: 0x%04lx  BCID: 0x%012lx\n", 
	 (buffer[(EVENT_HEADER_DATA-dspStart[0])/4] >> 12) & 0xF,
	 (buffer[(EVENT_HEADER_DATA-dspStart[0])/4] ) & 0xFFF);
  printf(" last EVENT used in EFB 0: L1ID: 0x%04lx  BCID: 0x%012lx ROD: 0x%04lx\n", 
	 (buffer[(EV_FIFO_DATA1-dspStart[0])/4] >> 8) & 0xF,
	 (buffer[(EV_FIFO_DATA1-dspStart[0])/4] ) & 0xFF,
	 (buffer[(EV_FIFO_DATA1-dspStart[0])/4] >> 12) & 0xF);
  printf(" last EVENT used in EFB 1: L1ID: 0x%04lx  BCID: 0x%012lx ROD: 0x%04lx\n\n", 
	 (buffer[(EV_FIFO_DATA2-dspStart[0])/4] >> 8) & 0xF,
	 (buffer[(EV_FIFO_DATA2-dspStart[0])/4] ) & 0xFF,
	 (buffer[(EV_FIFO_DATA2-dspStart[0])/4] >> 12) & 0xF);

  printf("=========================================================================\n\n");

   /* Dump Router and S-Link status*/
  printf(" RTR_CMND_STAT %x  %lx\n", RTR_CMND_STAT ,buffer[(RTR_CMND_STAT-dspStart[0])/4]);
  for(int i=0;i<16;i++){
    if((buffer[(RTR_CMND_STAT-dspStart[0])/4]) & (1<<i))printf("   %s\n",rtr_cmnd_stat_bit[i].c_str());
  }

  printf("=========================================================================\n\n");

  printf(" EFB_RUNTIME_STAT_REG %x  %lx\n", EFB_RUNTIME_STAT_REG ,buffer[(EFB_RUNTIME_STAT_REG-dspStart[0])/4]);
  for(int i=0;i<16;i++){
    if((buffer[(EFB_RUNTIME_STAT_REG-dspStart[0])/4]) & (1<<i))printf("   %s\n",efb_runtime_stat_bit[i].c_str());
  }

  printf("=========================================================================\n\n");

  printf(" EVT_MEM_FLAGS %x  %lx\n", EVT_MEM_FLAGS ,buffer[(EVT_MEM_FLAGS-dspStart[0])/4]);
  for(int i=0;i<24;i++){
    if((buffer[(EVT_MEM_FLAGS-dspStart[0])/4]) & (1<<i))printf("   %s\n",evt_mem_flags_bit[i].c_str());
  }

  printf("=========================================================================\n\n");
  printf(" EFB Large Frag %x : %lx\n", 0x402214 ,buffer[(0x402214-dspStart[0])/4]); // Line added by JD for reading Large event threshold
  printf("============================================================\n\n");


  for(int i=0;i<4;i++){
    printf(" ==Slave%d:============  \n", i);

    printf(" RTR_TRAP_CMND 0   0x%lx\n",  buffer[(RTR_TRAP_CMND_0(i)-dspStart[0])/4]);
    printf(" RTR_TRAP_MATCH 0  0x%lx\n",  buffer[(RTR_TRAP_MATCH_0(i)-dspStart[0])/4]);
    printf(" RTR_TRAP_MOD 0    0x%lx\n",  buffer[(RTR_TRAP_MOD_0(i)-dspStart[0])/4]);
    printf(" RTR_TRAP_CMND 1   0x%lx\n",  buffer[(RTR_TRAP_CMND_1(i)-dspStart[0])/4]);
    printf(" RTR_TRAP_MATCH 1  0x%lx\n",  buffer[(RTR_TRAP_MATCH_1(i)-dspStart[0])/4]);
    printf(" RTR_TRAP_MOD 1    0x%lx\n",  buffer[(RTR_TRAP_MOD_1(i)-dspStart[0])/4]);

    printf(" RTR_TRAP_FIFO_WRD_COUNT 0x%lx\n",  buffer[(RTR_TRAP_FIFO_WRD_CNT(i)-dspStart[0])/4]);

    printf(" RTR_TRAP_CMND Trap 0  trapping on ");
    for(int j=0;j<8;j++){
      if(buffer[(RTR_TRAP_CMND_0(i)-dspStart[0])/4] & (1<< j)) printf(trap_bits[j].c_str());
    }
    printf("\n");
    printf(" RTR_TRAP_CMND Trap 0  matching %ld remainder %ld, prescale %ld\n",  
	   buffer[(RTR_TRAP_MATCH_0(i)-dspStart[0])/4] & 0xFF,
	   ((buffer[(RTR_TRAP_MOD_0(i)-dspStart[0])/4]) >> 8) & 0xFF,
	   ((buffer[(RTR_TRAP_MOD_0(i)-dspStart[0])/4])) & 0xFF
           );
    printf(" RTR_TRAP_CMND Trap 1  trapping on ");
    for(int j=0;j<8;j++){
      if(buffer[(RTR_TRAP_CMND_1(i)-dspStart[0])/4] & (1<< j)) printf(trap_bits[j].c_str());
    }
    printf("\n");
    printf(" RTR_TRAP_CMND Trap 1  matching %ld remainder %ld, prescale %ld\n",  
	   buffer[(RTR_TRAP_MATCH_1(i)-dspStart[0])/4] & 0xFF,
	   ((buffer[(RTR_TRAP_MOD_1(i)-dspStart[0])/4]) >> 8) & 0xFF,
	   ((buffer[(RTR_TRAP_MOD_1(i)-dspStart[0])/4])) & 0xFF
           );
    printf("\n");

  }   
  printf("============================================================\n\n");
  printf("=========================================================================\n\n");

  char t_s[100];
  time_t t1 = time;
  tm t_m;
  localtime_r(&t1, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);

  std::cout << "Last update : " << t_s << std::endl;
  return 0;  
}


