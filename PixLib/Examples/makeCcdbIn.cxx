#include <set>
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>
#include <map>

std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

int main(int argc, char **argv) {

  // GET COMMAND LINE

  // Check arguments
  if (argc != 3) {
    std::cout << "makeCddbIn obj-dict connections" << std::endl;
    exit(0);
  }

  std::string objDict = argv[1];
  std::string conn = argv[2];
  std::string line; 

  std::map<std::string, std::string> obj;

  std::ifstream of(objDict.c_str());
  while (!of.eof()) {
    std::getline(of, line);
    if (line != "" && line[0] != '#') {
      std::string el0 = element(line, 0, '\t');
      std::string el1 = element(line, 1, '\t');
      obj[el0] = el1;
    }
  }

  std::ifstream cf(conn.c_str());
  while (!cf.eof()) {
    std::getline(cf, line);
    if (line != "" && line[0] != '#') {
      //std::string el0 = element(line, 0, '\t');
      //std::string el1 = element(line, 1, '\t');
      //std::string el2 = element(line, 2, '\t');
      //std::string el3 = element(line, 3, '\t');
      std::string el0 = element(line, 0, ' ');
      std::string el1 = element(line, 2, ' ');
      std::string el2 = element(line, 4, ' ');
      std::string el3 = element(line, 6, ' ');
      std::cout << el0 << "\t" << obj[el0] << "\t" << el1 << "\t";
      std::cout << el2 << "\t" << obj[el2] << "\t" << el3 << std::endl;
    } else {
      std::cout << line << std::endl;
    }
  }

  return 0;
}





