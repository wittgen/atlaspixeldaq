#include <iostream>
#include <unistd.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/UserExceptions.h"
#include "PixUtilities/PixMessages.h"

#include "PixDbInterface/PixDbInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"

#include <sstream>
#include <iomanip>
#include <sys/time.h>


int main(int argc, char **argv) {

  std::string header;
  std::string message;
  std::string ipcPartitionName;

  IPCCore::init(argc, argv);

if ( argc < 3 ) {
    std::cout<<"Usage: publishMessage <Partition> <header> <Message>\n";
    exit(0);
  } else {
    ipcPartitionName = argv[1];
    header = argv[2];
    for (int i=3; i<argc; i++) {message += argv[i]; message +=" ";} 
  }

  try {
    ers::error(PixLib::pix::daq (ERS_HERE, header, message));
    }
    catch(...){
    std::cout<<"Error publishing message "<<message<<" for "<<header<<std::endl;
    }



}
