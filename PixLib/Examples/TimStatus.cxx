#include <iostream>
#include "RodCrate/TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {
  VmeInterface *vme;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  /*
  try {
   
  } catch (...) {
    throw TimException("Exception caught in TimModule creation", 0, 0 );
  }
  */
  try{
    uint16_t regstatus, timID, timfirm, timSN;
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    timID = tim->fetchTimID();
    timSN = (timID & 0xFF);
    timfirm = (timID >> 8);
    //if (timSN >= 0x20) timfirm += 0xF00; // FPGA
    hex(cout);
    cout << "General TIM information (all dec unless specified):" << endl;
    cout << "TIM serial number (hex): " << hex <<timSN << endl;
    cout << "TIM firmware (hex): " << hex << timfirm << endl;
    dec(cout);
    //==============================================================
    if (!tim->regFetch(TIM_REG_STATUS)& 0x2000) cout << "TIM is not OK (clk not running)!";
    else {
      cout << "Running clock source: ";
      if (timfirm>12) {                          // to be precise following is valid for TIM3
	regstatus=tim->regFetch(TIM_REG_STATUS3);      
	if ((regstatus >>7) & 0x1) cout << "TTC" << endl;
	if ((regstatus >>8) & 0x1) cout << "External" << endl;
	if ((regstatus >>9) & 0x1) cout << "Internal" << endl;
	if (!((regstatus >>10) & 0x1)) cout << "Error: Clock PLL not stable after switching clocks" << endl;
      }
      else {
	regstatus=tim->regFetch(TIM_REG_STATUS);
	if ((tim->regFetch(TIM_REG_RUN_ENABLE) & 0x1) && (regstatus & 0x0100)) cout << "TTC" << endl; // TTC clk enabled && present
	else if ((tim->regFetch(TIM_REG_ENABLES)>>8) & 0x1) cout << "External" << endl;
	else if (regstatus & 0x0200) cout << "Internal" << endl;
      }
    
    }
    //==============================================================
    cout << "Trigger source: ";
    regstatus=tim->regFetch(TIM_REG_RUN_ENABLE);
    if ((regstatus >>1) & 0x1) cout << "TTC" << endl;
    else {
      if ((timfirm > 13) && (tim->regFetch(TIM_REG_ENABLES3) & 0x0800)) cout << "TTC delayed" <<endl;
      else {  //source is TIM
	regstatus=tim->regFetch(TIM_REG_ENABLES);
	if ((regstatus >>9) & 0x1) cout << "External" << endl; // just enables the input
	else {
	  if ((regstatus >>1) & 0x1) { //old internal trigger generator
	    int index;
	    int regfreq = (tim->regFetch(TIM_REG_FREQUENCY) & 0x1f);
	    for (index=0; index<TIM_FREQ_SIZE;index++) if (TIM_TRIG_FREQUENCY[index][0]== regfreq) break;
	    cout << "Old Internal with frequency: " << TIM_TRIG_FREQUENCY[index][1] <<" Hz" <<endl; 
	  }
	  else {
	    regstatus=tim->regFetch(TIM_REG_ENABLES3);
	    if ((timfirm >= 19) && (regstatus & 0x0080)) { //TimDefine.h is a problem!
	      unsigned int regperiod = tim->regFetch(TIM_REG_TROSC2_PERL) + (tim->regFetch(TIM_REG_TROSC2_PERH)<<16);
	      float regfreq = 40000000.0/regperiod;
	      cout << "New Internal trigger oscillator with frequency: " << regfreq <<" Hz "<<endl;
	    }
	    else if ((timfirm > 12) && (regstatus & 0x0040)) cout << "Internal improved randomizer" << endl;
	    else cout << "FP button" << endl;
	  }
	}
      }
      cout << "Trigger delay value (bcos): " << dec << tim->regFetch(TIM_REG_DELAY) << endl;
    }
    regstatus=tim->regFetch(TIM_REG_BUSY_STAT3);
    if ((regstatus >>15) & 0x1) cout << "FFTV DISABLE LINK INSERTED!" << endl;
    regstatus=tim->regFetch(TIM_REG_DEBUG_CTL);
    if (regstatus & 0x1000) cout << "FFTV DISABLED IN SOFTWARE!" << endl;
    else cout << "FFTV NOT DISABLED IN SOFTWARE!" << endl;
    //==============================================================
    
   
    cout << "====================== RodBusy status =================== " << endl;
    regstatus=tim->regFetch(TIM_REG_RB_MASK);
    cout << "ROD Slot#:  5  6  7  8  9 10 11 12 14 15 16 17 18 19 20 21" << endl;
    cout << "OR'ed       ";
    for (int i=0;i<16;i++) {
      cout << (regstatus & 0x1) << "  ";
      regstatus>>=1;
    }
    cout << endl;
    regstatus=tim->regFetch(TIM_REG_RB_STAT);
    cout << "BUSY        ";
    for (int i=0;i<16;i++) {
      cout << (regstatus & 0x1) << "  ";
      regstatus>>=1;
    }
    cout << endl;
    //==============================================================
    regstatus=tim->regFetch(TIM_REG_STATUS);
    if (regstatus & 0x0080) {
      cout << "ROD busy output On" << endl;
      cout << "VME ROD BUSY: ";
      regstatus=tim->regFetch(TIM_REG_COMMAND);
      if (regstatus & 0x0100) cout << " forced" << endl;
      else {
	cout << " not forced";
	regstatus=tim->regFetch(TIM_REG_DEBUG_CTL);
	if ((regstatus & 0x0200) ==0 ) cout << ", SA making RodBusy" << endl;
	else cout << endl;
      }
    }
    else cout << "ROD busy output Off" << endl;
    if ((regstatus & 0x0004) && (!(tim->regFetch(TIM_REG_RUN_ENABLE) && 0x0002))) cout << "TIM stopping triggers internally" << endl;
    if (regstatus & 0x0008) cout << "TIM is internally busy" << endl;
    cout << "==============================================================" << endl;
    cout << "External inputs used (enabled): " << endl;
    regstatus=tim->regFetch(TIM_REG_ENABLES);
    if ((regstatus >>8) & 0x1) cout << "Clock ";
    if ((regstatus >>9) & 0x1)  cout << "L1A Trigger ";
    if ((regstatus >>10) & 0x1)  cout << "ECReset ";
    if ((regstatus >>11) & 0x1)  cout << "BCReset ";
    if ((regstatus >>12) & 0x1)  cout << "Cal strobe ";
    if ((regstatus >>13) & 0x1)  cout << "FEReset ";
    if ((regstatus >>14) & 0x1)  cout << "Sequencer Go ";
    if ((regstatus >>15) & 0x1)  cout << "Ext Busy ";
    cout << endl;	   
    //==============================================================	 
    cout << "TTC inputs used (enabled): " << endl;
    regstatus=tim->regFetch(TIM_REG_RUN_ENABLE);
    if (regstatus & 0x1) cout << "Clock ";
    if ((regstatus >>1) & 0x1)  cout << "L1A Trigger ";
    if ((regstatus >>2) & 0x1)  cout << "ECReset ";
    if ((regstatus >>3) & 0x1)  cout << "BCReset ";
    if ((regstatus >>4) & 0x1)  cout << "Cal strobe ";
    if ((regstatus >>5) & 0x1)  cout << "FEReset ";
    if ((regstatus >>6) & 0x1)  cout << "Spare ";
    cout << endl;
    cout << "Serial outputs enabled: " << endl;
    if ((regstatus >>9) & 0x1)  cout << "L1A&BCID";
    if ((regstatus >>10) & 0x1)  cout << " TType";
    cout << endl;
    //==============================================================
    if (tim->regFetch(TIM_REG_STATUS) & 0x0020) cout << "Sequencer is running" << endl;
    //==============================================================    
    uint32_t tim_l1id = tim->fetchL1ID();
    cout << "L1ID counter value 0x" << hex << tim_l1id << dec << " (" << tim_l1id << ")" << endl;
    regstatus = tim->regFetch( TIM_REG_L1IDH );
    cout << "ECR counter value: " << ((regstatus & 0xff00) >> 8) << endl;
    regstatus = tim->regFetch( TIM_REG_TTID );
    cout << "TTC trigger type: " << (regstatus & 0x00ff) << " TIM TT: "<< ((regstatus & 0xff00) >> 8) << endl;
    regstatus = tim->regFetch( TIM_REG_BCID );
    cout << "BCID value: " << (regstatus & 0x0fff) << " offset: "<< ((regstatus & 0xf000) >> 12) << endl;
    cout << endl;
    /*  
	case 1:
	  regstatus=tim->regFetch(TIM_REG_BUSY_EN3);
	  cout << "Enable RodBusy into Busy: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable External-RodBusy into Busy: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable VME-RodBusy into Busy: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable External-Busy into Busy: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable VME-Busy into Busy: ";
	  regstatus = YesNo(regstatus);
	  regstatus >>=3;
	  cout << "Enable RodBusy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable External-RodBusy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable VME-RodBusy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable External-Busy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable VME-Busy into Busyout (set by Reset): ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable Burst-Busy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable Test-Busy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  cout << "Enable Deadtime-Busy into Busyout: ";
	  regstatus = YesNo(regstatus);
	  break;
	  case 2:
	  regstatus=tim->regFetch(TIM_REG_BUSY_STAT3);
	  cout << "RodBusy: OR of all RodBusy backplane inputs (post masking): ";
	  regstatus = YesNo(regstatus);
	  cout << "XRB: External RodBusy (post-enable): ";
	  regstatus = YesNo(regstatus);
	  cout << "VRB: VME-RodBusy: ";
	  regstatus = YesNo(regstatus);
	  cout << "XB: Front Panel Busy Input (post-enable): ";
	  regstatus = YesNo(regstatus);
	  cout << "VB: VME-Busy: ";
	  regstatus = YesNo(regstatus);
	  cout << "BSTB: Burst Ready - awaiting Burst-Go/Burst Done: ";
	  regstatus = YesNo(regstatus);
	  cout << "TSTB: Test-Busy Triggered: ";
	  regstatus = YesNo(regstatus);
	  cout << "DTB: Stand-Alone Signal Dead-Time: ";
	  regstatus = YesNo(regstatus);
	  cout << "Timeout after clock-switch/reset to ensure downstream stable: ";
	  regstatus = YesNo(regstatus);
	  cout << "TIM stopping triggers internally: ";
	  regstatus = YesNo(regstatus);
	  cout << "Front-panel RodBusy Output: ";
	  regstatus = YesNo(regstatus);
	  cout << "Front-panel Busy Output: ";
	  regstatus = YesNo(regstatus);
	  regstatus >>=1; 
	  cout << "Front-panel Masked ExtBusy Output: ";
	  regstatus = YesNo(regstatus);
	  cout << "Fixed Frequency Trigger Veto On: ";
	  regstatus = YesNo(regstatus);
	  cout << "FFTV Disable Link Inserted: ";
	  regstatus = YesNo(regstatus);
	  break;
	  }
    */
    delete tim;
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  try {
    delete vme;
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  return 0;
}

uint16_t YesNo(uint16_t regstatus)
{
  if (regstatus & 0x0001) cout << "Yes"<< endl;
      else cout << "No"<< endl;
      return regstatus>>1; 
}
