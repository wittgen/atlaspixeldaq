#include <iostream>
#include <unistd.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/UserExceptions.h"

int main(int argc, char **argv) {

  std::string appl;
  std::string mode;

  IPCCore::init(argc, argv);

  if ( argc != 3 ) {
    std::cout<<"Usage: sendPreAmpCtrlExe <ON/OFF> <applName>\n";
    exit(0);
  } else {
    mode = argv[1];
    appl = argv[2];
  }

  if (mode == "ON") {
    daq::rc::PixelUp issueHW(ERS_HERE, appl.c_str());
    ers::warning(issueHW);
  } else if (mode == "OFF") {
    daq::rc::PixelDown issueHW(ERS_HERE, appl.c_str());
    ers::warning(issueHW);
  }
}
