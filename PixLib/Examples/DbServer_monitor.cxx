#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <sys/time.h>
#include <string>

#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include "PixDbServer/PixDbServerInterface.h"


PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;

enum mainMode { DUMP, DUMPOBJECT, DUMPTYPE, CLONETAG, RENAMEPTAG, REMOVETAG, PURGETAG, HELP  };
enum mainMode mode;

enum dumpMode { SIMPLE, OBJ, VERBOSE };
enum dumpMode dmode;

std::string idTag = "PIXEL";
std::string domainName = "all domain in the dbserver";
std::string pendTag = "_Tmp";
std::string newPendTag = "new_Tmp";
std::string cfgTag = "defTag";
std::string cfgModTag = "defTag";
std::string destTag = "newTag";
std::string prefix = "";
std::string filter = "";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
std::string objType = "";
std::string objName = "";
unsigned int revision = 0;
struct timeval ti;
struct timeval tf;
int timeElapsed;


using namespace PixLib;

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

void cloneTagInDbServer(){
  timeElapsed=gettimeofday(&ti,NULL);
  if(cfgTag == "defTag"){
    std::cout << "Error in clone tag: no source tag specified!" << std::endl;
    return;
  }
  DbServer->cloneTag(domainName, cfgTag, destTag, pendTag);

  timeElapsed=gettimeofday(&tf,NULL);  
  std::cout << std::endl << std::endl <<  "Time for this operation is:  " <<tf.tv_sec - ti.tv_sec+ 1e-6*(tf.tv_usec-ti.tv_usec) << " s " << std::endl << std::endl;
  std::cout << std::endl << std::endl;
}

void renamePendTag(){
  DbServer->renamePendingTag(domainName, pendTag, newPendTag, cfgTag, objType);
}

void removeTag(){
  DbServer->removeTag(domainName, cfgTag);
}

void purgeTag(){
  DbServer->purgeTag(domainName, cfgTag);
}

std::string Content(std::string dom, std::string tag) {
  std::string cont="NULL";
  bool mod=false, boc=false;
  std::vector<std::string> modlist;
  DbServer->listObjectsRem(dom,tag,"PixModule",modlist);
  if (modlist.size()!=0) mod=true;
  modlist.clear();
  DbServer->listObjectsRem(dom,tag,"PixModuleGroup",modlist);
  if (modlist.size()!=0) boc=true;
  
  if (mod && boc) cont="ALL";
  if (mod && !boc) cont="MOD";
  if (!mod && boc) cont="BOC";
  return cont;
}

void dumpDbServer(){
  timeElapsed=gettimeofday(&ti,NULL);
 
  std::vector<std::string> domlist;
  DbServer->listDomainRem(domlist);
  std::vector<std::string>::iterator domit;
  std::cout << std::endl << "- DbServer has " << domlist.size() << " domains" << std::endl;
  for(domit=domlist.begin();domit!=domlist.end(); domit++) {
    if (domainName == "all domain in the dbserver" || (*domit) == domainName) {
      if (mode == DUMP) {
	std::vector<std::string> taglist;
	DbServer->listTagsRem((*domit),taglist);
	std::vector<std::string>::iterator tagit;
	std::cout << std::endl << "  -> domain   " << (*domit) << " has " << taglist.size() << " tags" << std::endl;
	for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	  std::vector<std::string> objlist;
	  DbServer->listObjectsRem((*domit),(*tagit),objlist);
	  std::vector<std::string>::iterator objit;
	  std::cout << std::endl <<  "     -> tag   " << (*tagit) << " (content: " << Content((*domit), (*tagit)) << " )  has " << objlist.size() << " objects" << std::endl;
	  for(objit=objlist.begin();objit!=objlist.end();objit++) {
	    if (dmode == OBJ) std::cout << "       -> object   " << (*objit) << std::endl;
	    if (dmode == VERBOSE) {
	      std::vector<unsigned int> revlist;
	      DbServer->listRevisionsRem((*domit),(*tagit),(*objit),revlist);
	      std::vector<unsigned int>::iterator revit;
	      std::cout << std::endl <<  "       -> object   " << (*objit) << " has " << revlist.size() << " revisions" << std::endl;
	      for(revit=revlist.begin();revit!=revlist.end();revit++) {
		std::string pen;
		DbServer->getPending(pen,(*domit),(*tagit),(*objit),(*revit));
		std::cout << "          -> revision   " << (*revit) << " with pending " << pen << "  ( " << getTime((*revit)) << " )" <<std::endl;
	      }
	    }
	  }
	}
      } 
      
      if (mode == DUMPOBJECT) {
	PixDbDomain::rev_content cont;
	bool result;
	if(revision!=0) result=DbServer->getObject(cont, (*domit), cfgTag, objName, revision);
	else result=DbServer->getObject(cont, (*domit), cfgTag, objName);

	if (result) {
	  std::string resultPendTag;
	  std::vector<unsigned int> revlist;
	  DbServer->listRevisionsRem((*domit),cfgTag, objName, revlist);
	  std::vector<unsigned int>::iterator revit;
	  std::cout << std::endl <<  "       -> object   " << objName << " has " << revlist.size() << " revisions" << std::endl;
	  for(revit=revlist.begin();revit!=revlist.end();revit++) {
	    DbServer->getPending(resultPendTag, (*domit), cfgTag, objName, (*revit));
	    std::cout << "          -> revision   " << (*revit) << " pendingTag " << resultPendTag << "  ( " << getTime((*revit)) << " ) " << std::endl;
	  }
	  std::cout << std::endl;
	  std::cout << " Content for object " << objName << " in domain " << (*domit) << " Tag " << cfgTag << " PendingTag " << resultPendTag;
	  if (revision!=0) std::cout << "  revision: " << revision <<"  ( " << getTime((*revit)) << " ) " << std::endl;
	  else std::cout << "  revision: MOST RECENT" << std::endl;
	  DbServer->printRevContent(cont);
	} else {
	  std::cout << std::endl << "Object " << objName << " not found in domain " << (*domit) << " Tag " << cfgTag << std::endl;
	}
      }
    
      if (mode == DUMPTYPE) {
	std::vector<std::string> taglist;
	DbServer->listTagsRem((*domit),taglist);
	std::vector<std::string>::iterator tagit;
	std::cout << std::endl << "  -> domain   " << (*domit) << " has " << taglist.size() << " tags" << std::endl;
	if (cfgTag=="defTag") {
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    std::vector<std::string> objlist;
	    DbServer->listObjectsRem((*domit),(*tagit),objType,objlist);
	    std::vector<std::string>::iterator objit;
	    std::cout << " tag " << (*tagit) << " has " << objlist.size() << " " << objType  <<std::endl;
	    for(objit=objlist.begin();objit!=objlist.end();objit++) {
	      if (dmode == SIMPLE)  std::cout << "       -> object   " << (*objit) << std::endl;
	      if (dmode == VERBOSE) {
		std::vector<unsigned int> revlist;
		DbServer->listRevisionsRem((*domit),(*tagit),(*objit),revlist);
		std::vector<unsigned int>::iterator revit;
		std::cout << std::endl <<  "       -> object   " << (*objit) << " has " << revlist.size() << " revisions" << std::endl;
		for(revit=revlist.begin();revit!=revlist.end();revit++) {
		  std::string pen;
		  DbServer->getPending(pen,(*domit),(*tagit),(*objit),(*revit));
		  std::cout << "          -> revision   " << (*revit) << " with pending " << pen << "  ( " << getTime((*revit)) << " ) " << std::endl;
		}
	      }
	    } 
	  }
	} else {
	  bool found=false;
	  for(tagit=taglist.begin();tagit!=taglist.end();tagit++) {
	    if ( cfgTag == (*tagit)) {
	      std::vector<std::string> objlist;
	      DbServer->listObjectsRem((*domit),(*tagit),objType,objlist);
	      std::vector<std::string>::iterator objit;
	      std::cout << " tag " << (*tagit) << " has " << objlist.size() << " " << objType  <<std::endl;
	      for(objit=objlist.begin();objit!=objlist.end();objit++) { 
		if (dmode == SIMPLE) std::cout << "       -> object   " << (*objit) << std::endl;
		if (dmode == VERBOSE) {
		  std::vector<unsigned int> revlist;
		  DbServer->listRevisionsRem((*domit),(*tagit),(*objit),revlist);
		  std::vector<unsigned int>::iterator revit;
		  std::cout << std::endl <<  "       -> object   " << (*objit) << " has " << revlist.size() << " revisions" << std::endl;
		  for(revit=revlist.begin();revit!=revlist.end();revit++) {
		    std::string pen;
		    DbServer->getPending(pen,(*domit),(*tagit),(*objit),(*revit));
		    std::cout << "          -> revision   " << (*revit) << " with pending " << pen << "  ( " << getTime((*revit)) << " ) " << std::endl;
		  }
		}
	      }
	      found=true;
	    }
	  }
	  if (!found)
	    std::cout << "Tag " << cfgTag << " not present in domain " << (*domit) << std::endl;
	}
      }
    }
  }
  
  timeElapsed=gettimeofday(&tf,NULL);
  
  std::cout << std::endl << std::endl <<  "Time for this operation is:  " <<tf.tv_sec - ti.tv_sec+ 1e-6*(tf.tv_usec-ti.tv_usec) << " s " << std::endl << std::endl;
  std::cout << std::endl << std::endl;
  
}

void dumpMem() {
  std::ostringstream o;
  o << "cat /proc/" << getpid() << "/status | grep VmSize";
  std::string cmd = o.str();
  system(cmd.c_str());
}

void openDbServer(int argc, char** argv) {   
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  mode = HELP;
  dmode = SIMPLE;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsPart = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--domain") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  domainName = argv[ip+1];
	  ip +=2;
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No Domain name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--tag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No Tag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--destTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  destTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No destTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--pendTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  pendTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No pendingTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--newPendTag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  newPendTag = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No New pendingTag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--rev") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
	  revision = atoi(argv[ip+1]);
	  std::cout << "Global revision is: " << revision << std::endl;
	  ip++; 
	} else  {
	  mode = HELP;
	  std::cout << std::endl << "No revision inserted" << std::endl << std::endl;
	  break;
	}
        ip++; 
      } else if (std::string(argv[ip]) == "--objName") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  objName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No object name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--objType") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  objType = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No object Type inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dump") {
	mode = DUMP; ip++; 
      } else if (std::string(argv[ip]) == "--objMode") {
	dmode = OBJ; ip++; 
      } else if (std::string(argv[ip]) == "--verboseMode") {
	dmode = VERBOSE; ip++; 
      } else if (std::string(argv[ip]) == "--dumpObj") {
	mode = DUMPOBJECT ; ip++; 
      } else if (std::string(argv[ip]) == "--dumpType") {
	mode = DUMPTYPE ; ip++; 
      } else if (std::string(argv[ip]) == "--cloneTag") {
	mode = CLONETAG ; ip++; 
      } else if (std::string(argv[ip]) == "--renamePendTag") {
	mode = RENAMEPTAG ; ip++; 
      } else if (std::string(argv[ip]) == "--purgeTag") {
	mode = PURGETAG ; ip++; 
      } else if (std::string(argv[ip]) == "--removeTag") {
	mode = REMOVETAG ; ip++; 
      } else if (std::string(argv[ip]) == "--save") {
	std::cout << "Ignoring --save option (this is not ChangeCfg)" << std::endl;
	ip++;
      } else {
	mode = HELP;ip++;
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      mode = HELP;
      break;
    }
  }
}


int main(int argc, char** argv) {
  std::string dbx = "";
  try {
     // Open DB servwer connection
    
    getParams(argc, argv);

    if (mode == HELP) {
      std::cout << std::endl
                << "Usage: DbServer_monitor --dump [--verboseMode || --objMode]\n"
		<< "            // Dump object numbers [opt.: obj+rev, obj]\n\n" 
		<< "Usage: DbServer_monitor --dumpObj --objName <objName> --tag <tag>\n"
		<< "            // Display last revision of <objName> in the <tag>\n\n" 
		<< "Usage: DbServer_monitor --dumpType --objType <type> --tag <tag>\n"
		<< "            // List object of <type> in the <tag>\n\n" 
		<< "Usage: DbServer_monitor --cloneTag --domain <domain> --tag <source> --destTag <destination> --pendTag <tag> \n"
		<< "            // Create a new tag <destination> in the domain <domain> from <source> with pendingTag <tag>.\n"
		<< "            // If no --destTag is specified, a tag named CLONE-<source> is created/used.\n"
		<< "            // If no --pendTag is specified, a pendingtag _Tmp is used.\n"
	        << "            // Parameter --domain is MANDATORY\n\n"
		<< "Usage: DbServer_monitor --renamePendTag --domain <domain> --pendTag <tag> --newPendTag <tag_new> --tag <source> \n"
		<< "            // Rename pending tag <tag> to <tag_new> in cfg tag <source>.\n"
	        << "            // Parameter --domain is MANDATORY\n"
		<< "            // To use to permanently save a tag\n\n"
		<< "Usage: DbServer_monitor --removeTag --domain <domain> --tag <tag>\n"
		<< "            // Remove tag <tag> from <domain>.\n"
	        << "            // Parameter --domain is MANDATORY.\n \n"
		<< "Usage: DbServer_monitor --purgeTag --domain <domain> --tag <tag>\n"
		<< "            // Remove old revisions from tag <tag> in <domain>.\n"
	        << "            // Parameter --domain is MANDATORY.\n \n"
	        << "*** Optional switches: ***\n"
	        << "        --dbPart <partition>   Specify partition name (def. PixelInfr)\n"
	        << "        --dbServ <name>        Specify dbServer name  (def. PixelDbServer)\n"
                << "        --domain <domain name> Specify domain name    (def. all domain in the DbServer)\n"
		<< "        --rev   <revision>     Specify object revision (def. last)\n"
		<< std::endl;
      exit(-1);
    } 

    // Open DB server connection
    if (DbServer == NULL) openDbServer(argc, argv);
    
    if (mode == CLONETAG){
      cloneTagInDbServer();
    } else if (mode == RENAMEPTAG) {
      renamePendTag();
    } else if (mode == REMOVETAG) {
      removeTag();
    } else if (mode == PURGETAG) {
      purgeTag();
    } else {
      dumpDbServer();
    }
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }

  return 0;
}
