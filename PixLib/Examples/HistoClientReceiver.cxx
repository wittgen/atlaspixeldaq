#include <unistd.h>
#include <set>
//#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

//#include <ipc/partition.h>
//#include <ipc/server.h>
//#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>

//#include <oh/OHRawProvider.h>


//#include "Histo/Histo.h"
//#include "PixController/PixScan.h"
#include "PixHistoServer/PixHistoServerInterface.h"

#include "TCanvas.h"
#include "TApplication.h"

using namespace PixLib;

int main(int argc, char **argv) {

  std::cout << "CHECK TO HAVE THE IS SERVER pixel_histo_server_HistoClient RUNNING! "<< std::endl;
  // Check arguments
  if(argc<4) {
    std::cout << "USAGE: HistoClient [partition name] [server name] [histo name]" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string serverName = argv[2];
  std::string histoName = argv[3];

  // Start IPCCore
  IPCCore::init(argc, argv);
  std::cout << "Getting partition " << ipcPartitionName << std::endl;
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  std::cout<< "I start IPC" << std::endl;

  OHRootHistogram his;
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, std::string(OHSERVERBASE)+"HistoClient", 
							       serverName, std::string(PROVIDERBASE)+"histo_client_receiver");
    //    TApplication theApp("App", 0, 0);
 //    TCanvas* canvas = new TCanvas();
//     his.histogram -> Draw();
//     canvas -> Update( );
//    std::cout <<"***END PHASE 3 ***" << std::endl;

    OHRootHistogram his;
    bool status = hInt->receiveHisto(histoName, his);
    if (status) {
      std::cout << "I have downloaded the histogram: " << histoName << std::endl;
      TApplication theApp("App", 0, 0);
      TCanvas* canvas = new TCanvas();
      his.histogram -> Draw();
      canvas -> Update( );
      sleep(5);
    } else {
      std::cout << "I have NOT downloaded the histogram: " << histoName 
		<< " PROBLEM! "<< std::endl;
      return 0;
    }
  } catch (...) {
    std::cout << "HistoClientReceiver. PixHistoServerInterface ERROR" << std::endl;
    return 0;
  }
  
  std::cout << "********** HISTO CLIENT RECEIVER DONE ********** " << std::endl;
  return 0;
}
