///////
//
//   Tags need to be loaded into DbServer???
// check if tags are there with  DumpDbServer = PixLib/Examples/DbServer_monitor --dump'
// use dbserverloader in case not there?
//   PixLib/Examples/DbServer_Loader 
//
////////

#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <sys/time.h>
#include <string>

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>

#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include "PixDbServer/PixDbServerInterface.h"
#include "PixModuleGroup/PixModuleGroup.h"
#include "PixBoc/PixBoc.h"


#include "Config/ConfMask.h"
#include "Config/ConfObj.h"

#include "Config/Config.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"
#include "Config/ConfMask.h"

#include <stdlib.h>
using namespace std;

PixDbServerInterface *DbServer = 0;
IPCPartition *ipcPartition = 0;
PixConnectivity *conn = 0;


std::string idTag = "PIXEL16";
std::string connTag   = "PIT-CONN";
std::string domainName = "Configuration-"+idTag;
std::string cfgModTag = "PIT_MOD";
std::string cfgTag = "PIT_BOC";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
unsigned int revision = 0;
unsigned int lower = 5;
unsigned int upper = 27;
bool help = false;
struct ModData
{
  int delay;
  int coarse;
  int msr;
  int inh;
  int laser;
  int slot;
  int channel;
  char ModuleName[28];
};

using namespace PixLib;

void openDbServer(int argc, char** argv) {   
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(dbsPart);
  } 
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != 0) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
	delete DbServer;
      } else {
	break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help = true;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsPart = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbsName = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--domain") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  domainName = argv[ip+1];
	  ip +=2;
	} else {
	  std::cout << std::endl << "No Domain name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--tag") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  cfgTag = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No Tag name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--rev") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
	  revision = atoi(argv[ip+1]);
	  ip += 2; 
	} else  {
	  std::cout << std::endl << "No revision inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--low") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
	  lower = atoi(argv[ip+1]);
	  ip += 2; 
	} else  {
	  std::cout << std::endl << "No lower limit inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--up") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
	  upper = atoi(argv[ip+1]);
	  ip += 2; 
	} else  {
	  std::cout << std::endl << "No upper limit inserted" << std::endl << std::endl;
	  break;
	}
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
}


int main(int argc, char** argv) {

  getParams(argc, argv);
  if (help) {
    std::cout << "Usage: CheckGDACs\n"
	      << "     Optional switches:\n"
	      << "        --dbPart <partition>             Specify partition name         def. " << dbsPart  << std::endl
	      << "        --dbServ <name>                  Specify dbServer name          def. " << dbsName << std::endl
	      << "        --domain <domain name>           Specify domain name            def. " << domainName << std::endl
	      << "        --tag    <config module tag>     Specify config module tag name def. " << cfgTag << std::endl
	      << "        --rev    <revision>              Specify object revision        def. " << revision << std::endl
	      << std::endl;
    exit(0);
  }

  char * outdir = getenv("HOME");
  if (outdir)
    std::cout << "Value of PATH is: " << outdir << endl;
  else {
    cout << "Storage path variable not defined (PIXSCAN_STORAG_PATH)" << endl;
    exit(1);
  }
  
  std::stringstream filename1, filename2;
  filename1 << outdir << "/BPMFineDelaysHistos.root";
  filename2 << outdir << "/BPMFineDelaysNtuple.root";
  std::cout << "will write to files " << filename1.str()<< " and "<< filename2.str() << std::endl;
  std::cout << "ok? (type 7 if not) ";
  int stop(0); 
  std::cin >> stop;
  if (stop==7) exit(1);
  
  TFile* root_file = new TFile(filename1.str().c_str(), "RECREATE"); 
  TFile* rfileData = new TFile(filename2.str().c_str(), "RECREATE"); 
  TTree* tree = new TTree("tree", "TX fine delays");
  ModData * stats = new ModData;

  std::map <std::string, int> dels;
  std::map <std::string, int> coars;
  std::map <std::string, int> msrs;
  std::map <std::string, int> inhs;
  std::map <std::string, int> lasers;
  std::map <std::string, int>::iterator it;

  tree->Branch("TXdelays", &stats->delay, "delay/I:coarse/I:msr/I:inh/I:laser/I:slot/I:channel/I:ModuleName[27]/C");
  
  int zerodelays=0;
  int delay;    
  root_file->cd();
  std::string histoname; 
  histoname = "BPMFineDelays_" + cfgTag; 
  TH1F* h_finedelays = new TH1F(histoname.c_str(), histoname.c_str(), 128, 0., 127.);  
  histoname = "Non_Zero_BPMFineDelays_" + cfgTag; 
  TH1F* h_nonzerodelays = new TH1F(histoname.c_str(), histoname.c_str(), 128, 0., 127.);  
  histoname = "RxDataDelays_" + cfgTag; 
  TH1F* h_RxDataDelays = new TH1F(histoname.c_str(), histoname.c_str(), 32, 0., 31.);  
  histoname = "RxThresholds_" + cfgTag; 
  TH1F* h_RxThresholds = new TH1F(histoname.c_str(), histoname.c_str(), 256, 0., 255.);    


  std::string dbx = "";
  try {
    // Open DB server connection
    if ( !DbServer ) openDbServer(argc, argv);
  
    conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag,idTag);
    conn->loadConn();

    std::vector<std::string> tagCfglist;
    DbServer->listTagsRem("Configuration-"+idTag,tagCfglist);
    bool foundCfg    = false;
    bool foundCfgMod = false;
    for (unsigned int i=0; i<tagCfglist.size(); i++) {
      if (tagCfglist[i]==cfgTag)    foundCfg    = true;
      if (tagCfglist[i]==cfgModTag) foundCfgMod = true;
    }

    if ( !foundCfg ) {
      std::cout << std::endl << "FATAL: cfgTag " << cfgTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }
    if ( !foundCfgMod ) {
      std::cout << std::endl << "FATAL: cfgModTag " << cfgModTag << " not found in the DbServer!!!" << std::endl << std::endl;
      exit(-1);
    }

    // loop over partitions
    std::map< std::string, PartitionConnectivity* >::iterator partit;
    for (partit=conn->parts.begin(); partit!=conn->parts.end(); ++partit) {
      std::map<std::string, RodCrateConnectivity*>::iterator rodcrateit;
      for (rodcrateit = partit->second->rodCrates().begin(); rodcrateit != partit->second->rodCrates().end(); ++rodcrateit){
	std::map<int, RodBocConnectivity*>::iterator rodbocit;
	for (rodbocit = rodcrateit->second->rodBocs().begin(); rodbocit != rodcrateit->second->rodBocs().end(); ++rodbocit){
	  PixModuleGroup *m_grp = new PixModuleGroup(rodbocit->second->name());
	  bool result=m_grp->readConfig(DbServer,domainName,cfgTag);
	  if (!result) {
	    std::cout << "Problem in getting PixModuleGroup " << rodbocit->second->name() << " from DbServer " << std::endl;
	  }
	  else {
	    PixBoc *m_boc = m_grp->getPixBoc();
	    for(int i=0; i<4; i++) {
	      OBConnectivity* obConn = 0;
	      obConn = rodbocit->second->obs(i);
	      if (obConn != 0){
		if (obConn->pp0() != 0) {
		  PixBoc::PixTx *plugintx = m_boc->getTx(i);
		  PixBoc::PixRx *pluginrx = m_boc->getRx(i);
		  for (int j=0; j<9; j++){
		    ModuleConnectivity* modConn = 0;
		    modConn = obConn->pp0()->modules(j);
		    if (modConn != 0) {
		      h_finedelays->Fill(delay = plugintx->getTxFineDelay(j));
		      std::string temp = rodbocit->second->name() + "/" + modConn->name();
		      strncpy(stats->ModuleName,temp.c_str(),27);
		      stats->ModuleName[27]='\0';
		      int slot = modConn->bocLinkTx()/10;
		      stats->slot = slot;
		      int ch = modConn->bocLinkTx()%10;
		      stats->channel = ch;
		      stats->delay = m_boc->getTx(slot)->getTxFineDelay(ch);
		      stats->coarse = m_boc->getTx(slot)->getTxCoarseDelay(ch);
		      stats->msr = m_boc->getTx(slot)->getTxMarkSpace(ch);
		      stats->inh = m_boc->getTx(slot)->getTxStreamInhibit(ch);
		      stats->laser = m_boc->getTx(slot)->getTxLaserCurrent(ch);
		      dels[modConn->name()]=stats->delay;
		      coars[modConn->name()]=stats->coarse;
		      msrs[modConn->name()]=stats->msr;
		      inhs[modConn->name()]=stats->inh;
		      lasers[modConn->name()]=stats->laser;
		      tree->Fill();
		      h_RxDataDelays->Fill(pluginrx->getRxDataDelay(j, false));
		      h_RxThresholds->Fill(pluginrx->getRxThreshold(j, false));
		      if (delay) h_nonzerodelays->Fill(delay);
		      else zerodelays++;
		    }
		  }
		}
	      }
	    }
	  }
	}
      }
    }
    
    std::cout << "Total Zero Finedelays: " << zerodelays << std::endl;
    rfileData->cd();
    tree->Write();
    rfileData->Close();
    root_file->cd();
    h_finedelays->Write();
    h_RxDataDelays->Write();
    h_RxThresholds->Write();
    h_nonzerodelays->Write();
    root_file->Close();
    
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
  e.dump(std::cerr);
  std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }
 
  ofstream outDataFile;
  outDataFile.open("dumpDels.txt", ofstream::out);
  for (it=dels.begin(); it != dels.end(); it++ ) {
    outDataFile << (*it).first << "\t" <<  dels[(*it).first]<< "\t" << coars[(*it).first] << "\n";
  }
  outDataFile.close();

  ofstream outMSRFile;
  outMSRFile.open("MSRs.txt", ofstream::out);
  for (it=msrs.begin(); it != msrs.end(); it++ ) outMSRFile << (*it).first << "\t" <<  msrs[(*it).first]<< "\n";
  outMSRFile.close();

  ofstream outLaserFile;
  outLaserFile.open("Lasers.txt", ofstream::out);
  for (it=lasers.begin(); it != lasers.end(); it++ ) outLaserFile << (*it).first << "\t" <<  lasers[(*it).first]<< "\n";
  outLaserFile.close();
  return 0;
}
