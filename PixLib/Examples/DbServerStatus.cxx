#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>

#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixController/PixRunConfig.h"

using namespace std;

IPCPartition* ipcPartition = NULL;
PixDbServerInterface *DbServer;

std::string ipcPartitionName =     "PixelInfr";
std::string serverName = "PixelDbServer";

std::string getTime(int i) {
  char t_s[100];
  time_t t;
  tm t_m;
  t = i;
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

void getParams(int argc, char **argv) {
  int ip = 1;
  bool help = false;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help = true;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  help = true;
	  break;
	}
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
  if (help) {
    std::cout << "\nDbServerStatus: prints a report of the DbServer memory usage \n\n";
    std::cout << "              --dbPart <name> select partition name \n";
    exit(0);
  }
}

int main(int argc, char** argv) {
  getParams(argc, argv);
  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  std::cout << "Connecting to partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "RunConfigEditor: ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = NULL;
    return -1;
  }
 
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << serverName << "  ..... " << std::endl;
  do {
    sleep(1);
    DbServer=new  PixDbServerInterface(ipcPartition,serverName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete DbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << serverName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << serverName << std::endl;

  std::vector<std::string> tags;
  std::vector<unsigned int> nobj;
  std::vector<unsigned int> nrev;
  std::vector<unsigned int> size;
  unsigned int narev = 0;
  unsigned int revsize = 0;

  DbServer->memoryReport(tags, nobj, nrev, size, narev, revsize);

  unsigned int maxsize=0;
  for (unsigned int i=0; i<tags.size(); i++) {
    if (tags[i].size() > maxsize) maxsize = tags[i].size(); 
  }
  if (maxsize < 32) maxsize = 32;
  std::string sepLine;
  for (unsigned int i=0; i<maxsize+41; i++) sepLine+="=";
  std::cout << std::endl;
  std::cout << sepLine << std::endl;
  std::cout << " Tag" << setw(maxsize+36) << "# objects   # of revs   Size (KB)" << std::endl;
  std::cout << sepLine << std::endl;
  for (unsigned int i=0; i<tags.size(); i++) {
    std::cout << " " << tags[i] << setw(maxsize+3-tags[i].size()) << " ";
    std::cout << std::setw(12) << nobj[i];
    std::cout << std::setw(12) << nrev[i];
    std::cout << std::setw(12) << size[i]/1024 << std::endl;
  }
  std::cout << sepLine << std::endl;
  std::cout << " Total number of revisions ";
  std::cout << setw(12) << narev;
  std::cout << "      Total size (KB) ";
  std::cout << setw(12) << revsize/1024 << std::endl;
  std::cout << sepLine << std::endl << std::endl;
}
