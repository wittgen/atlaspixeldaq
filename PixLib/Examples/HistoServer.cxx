#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unistd.h>

//#include <ipc/server.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <owl/semaphore.h>

#include "PixHistoServer/PixNameServer.h"

OWLSemaphore semaphore;

using namespace PixLib;

int main(int argc, char **argv) {

   if(argc<3) {
     std::cout << "USAGE: HistoServer [partition name] [nameServer name]" << std::endl;
     return 1;
   }

   std::string ipcPartitionName = argv[1];
   std::string nameServer = argv[2];

   // Start IPCCore
   IPCCore::init(argc, argv);
   std::cout << "Starting partition " << ipcPartitionName << std::endl;
   IPCPartition *ipcPartition = new IPCPartition(ipcPartitionName.c_str());
   PixNameServer nServer(ipcPartition, nameServer);

   std::cout << "IPC started. NameServer started." << " " << std::endl;

   // START SERVER
   semaphore.wait();


   return 0;



}


