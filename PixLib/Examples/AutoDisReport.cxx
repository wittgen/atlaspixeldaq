#include "PixDbInterface/PixDbInterface.h"
#include "PixUtilities/PixISManager.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include <iostream>
#include <sstream>
#include <iomanip>
#include <sys/time.h>

using namespace std;


std::string IsServerName     = "RunParams";
std::string IsServerNameDT   = "RunParams";
std::string ipcPartitionName = "PixelInfr";
std::string autoDisPartName =  "ATLAS";
std::string eon = "YES";
std::string mod = "";

std::string element(std::string str, int num, char sep) {
  unsigned int ip;
  int start = -1,end = -1;
  int nel = -1;
  bool spc = true;
  bool quote = false;
                                                                                                                                        
  for (ip=0;ip<str.size();ip++) {
    if (str[ip] == '"') quote = !quote;
    if (str[ip] == sep || (sep == 0 && 
       (str[ip] == 9 || str[ip]==10 || str[ip] == 32))) {
      if (!spc && !quote) {
        spc = true;
        if (nel==num) end = ip-1;
      }
    } else {
      if (spc) {
        spc = false;
        nel++;
        if (nel==num) start = ip;
      }
    }
  }
  if (!spc && nel==num) end = str.size()-1;
                                                                                                                                        
  if (start >= 0) {
    std::string c;
    std::string s = str.substr(start,end-start+1);
    for (ip=0;ip<s.size();ip++) {
      if (s[ip] != '"') c += s[ip];
    }
    return c;
  } else {
    return "";
  }
}

int main(int argc, char** argv) {
  int ipp = 1;
  while (ipp < argc) {
    std::string par = argv[ipp];
    if (par == "--part" && ipp+1 < argc) {
      ipcPartitionName = argv[ipp+1];
      ipp = ipp+2;
    } else if (par == "--autoDisPart" && ipp+1 < argc) {
      autoDisPartName = argv[ipp+1];
      ipp = ipp+2;
    }
  }

  IPCPartition* ipcPartition = nullptr;
  IPCPartition* ipcDTPartition = nullptr;
  PixISManager* isManager = nullptr;
  PixISManager* isManagerDT = nullptr; 

  // Start IPCCore    
  IPCCore::init(argc, argv); 
  // Create IPCPartition constructors                                                              
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcPartition = nullptr;
    return EXIT_FAILURE;
  }
  try {
    ipcDTPartition = new IPCPartition(autoDisPartName);
  } catch(...) {
    std::cout << "ERROR!! problems while searching for the partition, cannot continue" << std::endl;
    ipcDTPartition = nullptr;
    return EXIT_FAILURE;;
  }

  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(PixISManagerExc const & e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return EXIT_FAILURE;;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerName << std::endl; 
    return EXIT_FAILURE;;
  }
  try {
    isManagerDT = new PixISManager(ipcDTPartition, IsServerNameDT);
  }  
  catch(PixISManagerExc const & e) { 
    std::cout << "Exception caught in RunConfigEditor.cxx: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return EXIT_FAILURE;;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating ISManager for IsServer " << IsServerNameDT << std::endl; 
    return EXIT_FAILURE;;
  }
  std::cout << std::endl;

  std::vector<std::string> varListAutoDis = isManagerDT->listVariables("*AUTO_DISABLE_ENA");

  if (varListAutoDis.empty()){
    std::cout << "Transition CONFIGURE not yet executed; please try again later\n" << std::endl;
    return -1;
  }

  int totDis = 0;
  std::vector<std::string> varList = isManager->listVariables("*AUTO_DISABLED_MODULES");
  for (unsigned int iv = 0; iv<varList.size(); iv++) {
    std::string mods = isManager->read<std::string>(varList[iv].c_str());
    if (mods != "") { 
      int nmods = 0;
      std::string sm, smods = "";
      sm=element(mods,nmods,'/');
      while (sm != "") {
	nmods++;
	smods += (sm+" ");
	sm=element(mods,nmods,'/');
      }
      totDis += nmods;
      std::cout << element(varList[iv],1,'/') << " = " << smods << std::endl;
    }
  }
  int totDisRods = 0;
  std::vector<std::string> varListRod = isManager->listVariables("*AUTO_DISABLED_ROD");
  for (unsigned int ivr = 0; ivr<varListRod.size(); ivr++) {
    std::string rod = isManager->read<std::string>(varListRod[ivr].c_str());
    if (rod != "") { 
      totDisRods++;
      std::cout << element(varListRod[ivr],1,'/') << " = " << rod << std::endl;
    }
  }
  std::cout << std::endl << "Total number of auto-disabled modules " << totDis << std::endl;  
  std::cout << std::endl << "Total number of auto-disabled rods " << totDisRods << std::endl;  
  // LASER
  std::cout << std::endl << "QuickStatus-disabled modules" << std::endl;
  int totDisQS = 0;
  std::vector<std::string> varListQS = isManager->listVariables("*QS_DISABLED_MODULES");
  for (unsigned int ivqs = 0; ivqs<varListQS.size(); ivqs++) {
    std::string mods = isManager->read<std::string>(varListQS[ivqs].c_str());
    if (mods != "") { 
      int nmods = 0;
      std::string sm, smods = "";
      sm=element(mods,nmods,'/');
      while (sm != "") {
	nmods++;
	smods += (sm+" ");
	sm=element(mods,nmods,'/');
      }
      totDisQS += nmods;
      std::cout << element(varList[ivqs],1,'/') << " = " << smods << std::endl;
    }
  }

  std::cout << std::endl << "Total number of QuickStatus-disabled modules " << totDisQS << std::endl;
  
  std::cout << std::endl << "AUTO_DISABLE_ENA is set to :" << std::endl;
  for (const auto &it : varListAutoDis) {
    std::cout<<" "<<it<<" "<<isManagerDT->read<std::string>(it.c_str())<<std::endl;
  }
  std::cout << std::endl << "DSPMon_trick1 is set to  ";
  try {
    std::cout << isManager->read<std::string>("DSPMon_trick1");
  }
  catch (...) {
    std::cout << "(unset)";
  }
  std::cout << std::endl << std::endl;  
  std::vector<std::string> varListTrk1 = isManager->listVariables("*Trick1Count");
  for (unsigned int ivt = 0; ivt<varListTrk1.size(); ivt++) {
    unsigned int t1c = isManager->read<unsigned int>(varListTrk1[ivt].c_str());
    std::cout << varListTrk1[ivt] << " = " << t1c << std::endl;
  }
}
