#include <iostream>
#include <unistd.h>
#include "ers/ers.h"
#include "ipc/core.h"
#include "RunControl/Common/UserExceptions.h"

int main(int argc, char **argv) {

  std::string appl;
  std::string rod;
  std::string mode;

  IPCCore::init(argc, argv);

  if ( argc != 4 ) {
    std::cout<<"Usage: sendDisableMessage <rodName> <applName> <ROD or MOD>\n";
    exit(0);
  } else {
    rod = argv[1];
    appl = argv[2];
    mode = argv[3]; 
  }

  if (mode == "MOD") {
    if (rod[0] == '+') {
      std::string mod = rod.substr(1);
      daq::rc::ModulesEnabled issueHW(ERS_HERE, mod.c_str(), false);
      ers::warning(issueHW);
    } else {
      daq::rc::ModulesDisabled issueHW(ERS_HERE, rod.c_str(), false);
      ers::warning(issueHW);
    }
  } else {
    if (rod[0] == '*') {
      std::string mod = rod.substr(1);
      daq::rc::HardwareRecovered issueHW(ERS_HERE, mod.c_str(), appl.c_str());
      ers::warning(issueHW);
    } else if (rod[0] == '+') {
      std::string mod = rod.substr(1);
      daq::rc::ReadyForHardwareRecovery issueHW(ERS_HERE, mod.c_str(), appl.c_str());
      ers::warning(issueHW);
    } else {
      daq::rc::HardwareError issueHW(ERS_HERE, rod.c_str(), appl.c_str());
      ers::warning(issueHW);
    }
  }
}
