#include <unistd.h>
#include <set>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <ipc/alarm.h>
#include <ipc/core.h>

#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

int main(int argc, char **argv) {
  
  
  // Check arguments
  if(argc<3) {
    std::cout << "USAGE: HistoRemoveHistogram [partition name] [Histogram name]" << std::endl;
    return 1;
  }
  
  std::string ipcPartitionName = argv[1];
  std::string hName = argv[2];
  std::string oh = std::string(OHSERVERBASE)+"crate0";
  std::string serverName = "nameServer";
  std::string providerName = std::string(PROVIDERBASE)+"prov-remove";
  std::string program = "HistoRemove";

  std::cout << "I want to delete the histogram "<< hName
	    << " from the cache (OH + nameServer)" << std::endl;

  struct timeval t0; 
  struct timeval t1;
  int time; 

  // Start IPCCore
  IPCCore::init(argc, argv);
  IPCPartition *m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());

  std::stringstream info;

  bool status;
  time=gettimeofday(&t0,NULL); 
  try {
    PixHistoServerInterface *hInt= new PixHistoServerInterface(m_ipcPartition, oh, serverName, providerName);
    std::cout << "Start deletion from the cache of the histogram "<< hName << " " << std::endl;
    status = hInt->removeHisto(hName);
  } catch (PixHistoServerExc e) {
    if (e.getId() == "IPCTIMEOUT") {      
      info << "IPC timeout ";
      ers::warning(PixLib::pix::daq (ERS_HERE, program, info.str()));
    }
    return 0;
  } catch (...) {
    info << "Unknown Exception was thrown ";
    ers::error(PixLib::pix::daq (ERS_HERE, program, info.str()));
    return 0;
  }
  
  time=gettimeofday(&t1,NULL);
  std::cout << "HistoRemoveHistogram tool spent: " 
	    << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) 
	    << " s for cleaning up the histogram server memory. " 
	    << std::endl;
  
  return 0;
}
