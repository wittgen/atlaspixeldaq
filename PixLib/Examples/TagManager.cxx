#include "PixDbInterface/PixDbInterface.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixUtilities/PixISManager.h"

#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"

#include <TROOT.h>
#include <TApplication.h>
#include <TVirtualX.h>
#include <TGResourcePool.h>
#include <TGListBox.h>
#include <TGListTree.h>
#include <TGFSContainer.h>
#include <TGLabel.h>
#include <TGMsgBox.h>
#include <TGComboBox.h>
#include <TGTab.h>
#include <TH1.h>
#include <TGraph.h>
#include <TStyle.h>
#include <TGTextView.h>

#include <oks/kernel.h>
#include <oks/relationship.h>

#define GETNAME get_name
#ifdef TDAQ010900
#define GETNAME GetName
#endif

using namespace PixLib;

enum mainMode { NORMAL, HELP  };
enum mainMode mode;

enum content { ALL, MOD, BOC};
enum content tagCont;

std::string dbServerName     = "PixelDbServer";
std::string ipcPartitionName = "PixelInfr";
std::string IsServerName     = "PixelRunParams";
IPCPartition *ipcPartition = NULL;
PixDbServerInterface *dbServer = NULL;
PixISManager* isManager = NULL; 
PixISManager* isManagerDDC = NULL; 

std::string rcdFolder = "PIT";

PixConnectivity* conn;
PixDisable* disableConfig = NULL;

std::string dataTakingPartitionNamePit = "PIX-DAQSlice";
std::string dataTakingPartitionNameSr1 = "PIX-DAQSlice";
std::string dataTakingPartitionNamePit_IBL = "IBL-DAQSlice";
std::string dataTakingPartitionNameSr1_IBL = "IBL-DAQSlice";

OksKernel kernel;

OksObject* getMainPartitionObjectFromFile(std::string& mainXmlFileArg, std::string& dataTakingPartitionName) {

  OksFile * fp = kernel.load_data(mainXmlFileArg.c_str());
  if (fp == 0) {
    std::cerr << "ERROR: Can't load database file " << mainXmlFileArg << std::endl;
  } else {
    std::cout << "Reading information from " << mainXmlFileArg << std::endl;
  }

  OksClass *mainPartition = kernel.find_class("Partition");

  OksObject *mainPartitionObject = mainPartition->get_object(dataTakingPartitionName);
	if(!mainPartitionObject) {
    kernel.close_all_data(); // Unload the file if the partition is not found
    std::string mainXmlFileName = mainXmlFileArg.substr(0, mainXmlFileArg.find_last_of('/')) + "/" + dataTakingPartitionName + ".data.xml";
		std::cout << "Trying file " << mainXmlFileName << std::endl;
    fp = kernel.load_data(mainXmlFileName.c_str());
    mainPartitionObject = mainPartition->get_object(dataTakingPartitionName);
  }
  return mainPartitionObject;
}

void justifyXmlDisables(PixConnectivity* connArg, std::string& dataTakingPartitionName) {

  std::map<string, std::string>  partNames;
  partNames["PIXEL_B"] = "PixelRunControlBLayer";
  partNames["PIXEL_D"] = "PixelRunControlDisk";
  partNames["PIXEL_L"] = "PixelRunControlBarrel";
  partNames["PIXEL_I"] = "PixelRunControlIBL";

  std::map<std::string, std::string> toBeDisabledRods;
  std::map<std::string, std::string> toBeDisabledPartitions;

  //--------- READ CONNECTIVITY-------------------------------

  std::map<std::string, RodBocConnectivity*>::iterator irb;
  for (irb=connArg->rods.begin(); irb!=connArg->rods.end(); ++irb) {
    if (irb->second->active() == false) {
      toBeDisabledRods[irb->first] = "TODIS";
      cout << "To be Disabled ROD: " << irb->first << endl;
    }
  }

  std::map<std::string, PartitionConnectivity*>::iterator ip;
  for (ip=connArg->parts.begin(); ip!=connArg->parts.end(); ++ip) {
    if (ip->second->active() == false) {
      if (partNames.find(ip->first) != partNames.end()) {
	toBeDisabledPartitions[partNames[ip->first]] = "TODIS";
	cout << "To be Disabled partition : " << ip->first << endl;
      }
    }
  }

  //--------- READ XML File ------------------------------------------
  std::string mainXmlFile;
  mainXmlFile = getenv("TDAQ_DB_DATA");  

  OksObject *mainPartitionObject = getMainPartitionObjectFromFile(mainXmlFile, dataTakingPartitionName);
  OksFile * fp = kernel.get_active_data();
  if (fp == 0) {
    std::cerr << "ERROR: Can`t load database file " << mainXmlFile << " ,exiting ...\n";
    return;
  } else {
    std::cout << "Reading information from " << mainXmlFile << std::endl;
  }
  mainXmlFile = fp->get_full_file_name();
  std::cout << "Main XML file is: " << mainXmlFile << std::endl;

  if (mainPartitionObject) {
    //look what is already disabled in partition
    OksData *d(mainPartitionObject->GetRelationshipValue("Disabled"));
    if (d->type == OksData::list_type) { 
      OksData::List * d_list = d->data.LIST;
      for(OksData::List::iterator i2 = d_list->begin(); i2 != d_list->end(); ++i2) {
	OksData *d2 = *i2;
	std::cout << "Disabled " << d2->data.OBJECT->GetClass()->GETNAME() << " - " << d2->data.OBJECT->GetId() << std::endl;
	if (d2->data.OBJECT->GetClass()->GETNAME() == "PixelROD") { 
	  std::string rodname = d2->data.OBJECT->GetId();
	  if (toBeDisabledRods.find(rodname) == toBeDisabledRods.end()) {
	    toBeDisabledRods[rodname] = "DISABLED";
	  } else {
	    toBeDisabledRods[rodname] = "OK";
	  }
	}
	if (d2->data.OBJECT->GetClass()->GETNAME() == "Segment") { 
	  std::string partname = d2->data.OBJECT->GetId();
	  bool found = false;
	  std::map<std::string, std::string>::iterator ipn;
	  for (ipn=partNames.begin(); ipn!=partNames.end(); ipn++) {
	    if (ipn->second == partname) found = true;
	  }
          if (found) {
	    if (toBeDisabledRods.find(partname) == toBeDisabledRods.end()) {
	      toBeDisabledPartitions[partname] = "DISABLED";
	    } else {
	      toBeDisabledPartitions[partname] = "OK";
	    }
	  }
	}
      }
    }

    std::map<std::string, std::string>::iterator it;
    for (it=toBeDisabledRods.begin(); it!=toBeDisabledRods.end(); it++) {
      if (it->second == "TODIS") {
	mainPartitionObject->AddRelationshipValue("Disabled", "PixelROD", it->first);
      } else if (it->second == "DISABLED") {
	mainPartitionObject->RemoveRelationshipValue("Disabled", "PixelROD", it->first);
      }
    }
    for (it=toBeDisabledPartitions.begin(); it!=toBeDisabledPartitions.end(); it++) {
      if (it->second == "TODIS") {
	mainPartitionObject->AddRelationshipValue("Disabled", "Segment", it->first);
      } else if (it->second == "DISABLED") {
	mainPartitionObject->RemoveRelationshipValue("Disabled", "Segment", it->first);
      }
    }

    // SAVE OKS FILE
    try {
      kernel.save_data(fp, true);
    }
    catch (...) {
      std::cerr << "cannot save oks data file: "<< mainXmlFile << std::endl;
      return;
    }
  } else {
    std::cout << "Could not find object main partition object!" << std::endl;
  }
}


void connect() {
  //  Create IPCPartition constructors
  std::cout << std::endl << "Starting partition " << ipcPartitionName << std::endl;
  try {
    ipcPartition = new IPCPartition(ipcPartitionName);
  } catch(...) {
    std::cout << "PixDbServer: ERROR!! problems while connecting to the partition!" << std::endl;
  }
  bool ready=false;
  int nTry=0;
  std::cout << "Trying to connect to DbServer with name " << dbServerName << "  ..... " << std::endl;
  do {
    sleep(1);
    dbServer=new  PixDbServerInterface(ipcPartition,dbServerName,PixDbServerInterface::CLIENT, ready);
    if(!ready) delete dbServer;
    else break;
    std::cout << " ..... attempt number: " << nTry << std::endl;
    nTry++;
  } while(nTry<10);  
  if(!ready) {
    std::cout << "Impossible to connect to DbServer " << dbServerName << std::endl;
    exit(-1);
  }
  std::cout << "Succesfully connected to DbServer " << dbServerName << std::endl;

  try {
    isManager = new PixISManager(ipcPartition, IsServerName);
  }  
  catch(const PixISManagerExc &e) { 
    std::cout << "Exception caught in TagManager: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in TagManager while creating ISManager for IsServer " << IsServerName << std::endl; 
    return;
  }
  std::cout << "Succesfully connected to IsManager " << IsServerName << std::endl;

  try {
    isManagerDDC = new PixISManager(ipcPartition, "DDC");
  }  
  catch(const PixISManagerExc &e) { 
    std::cout << "Exception caught in TagManager: " << e.getErrorLevel() << "-" << e.getISmanName() << "-" << e.getExcName() << std::endl; 
    return;
  }
  catch(...) { 
    std::cout <<"Unknown exception caught in TagManager while creating ISManager for IsServer DDC" << std::endl; 
    return;
  }
  std::cout << "Succesfully connected to IsManager DDC" << std::endl;
}



class HistoFrame : public TGMainFrame {

private:
  const TGWindow      *fMain;
  TGTab               *fTab, *fTab1;
  //TGCompositeFrame    *tfmain;
  TGCompositeFrame    /* *line1, *line2, *line3,*/ *line4, *line5, *line6, *line7, *line8, *line9, *line10, *line11, *ExF;
  TGLayoutHints       *step1, *step2, *step3, *step4;
  TGLabel             *TitleLabel,/* *idTagLabel,  *connTagLabel,  *cfgTagLabel, *TitleLabel2, *cLabel,*/ *domLabel, *tagLabel,/* *IsLabel,*/  *idTagIsLabel,  *connTagIsLabel,  *cfgTagIsLabel,  *revIsLabel;
  TGLabel             *domCFLabel, *tagCFLabel, *tagModCFLabel, *readLabel, *isDisLab, *isRunLab, *isWsDisLab, *isSBOLab;
  TGLabel             *disLabel, *runLabel, *wsDisLabel;
  TGCheckButton       *estat, *sbo;

  TGButton            /**loadB,*/ *pubB, *IsB, *ExitB;
  TGComboBox          /**idTagCombo, *connTagCombo, *cfgTagCombo, *cCombo,*/ *domCombo, *tagCombo, *domCFCombo, *tagCFCombo, *tagModCFCombo, *disCombo, *runCombo, *wsDisCombo;

  TGCompositeFrame    *tf2;
  TGCompositeFrame    *tf2_line1, *tf2_line2, *tf2_line3;
  TGCheckButton       /**tf2_veto,*/ *tf2_actionsRestart, *tf2_actionsRestartLocalDT, *tf2_atlasVeto, *tf2_disableResetViset;
  TGButton            *tf2_pubB;


  TGLayoutHints        *fL1, *fL2, *fL3, *fL4, *fL5;

  TGTextView          /**fOccT,*/ *idTagT,  *connTagT,  *cfgTagT,  *revT, *runT, *disT, *wsDisT, *wsSBOT;
  //TGCompositeFrame    *fFrameOcc, *fFrameOccTx;

  //my methods
  void loadDomain(std::string type);
  std::vector<std::string> domConnlist;
  std::vector<std::string> domCfglist;
  void loadTag(std::string type);
  std::vector<std::string> tagConnlist;
  std::vector<std::string> tagCfglist;
  std::vector<std::string> tagCfgModlist;
  void loadDis();
  void loadRun();

  content checkContent(std::string dom, std::string tag);
  
  std::vector<std::string> runlist;
  std::vector<std::string> dislist;
  void publishInIs();
  void readFromIs();

  void selectText();
  void selectCanvas();

  enum { kCloseWindow, kPublishToIs, kReadFromIs };

public:
  HistoFrame(const TGWindow *p, UInt_t w, UInt_t h);
  virtual ~HistoFrame();
  
  virtual void CloseWindow();
  virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2);
};

HistoFrame::HistoFrame(const TGWindow *p, UInt_t w, UInt_t h)
  : TGMainFrame(p, w, h) {
  std::cout << "Creating main window..." << std::endl;
  fMain = p;
  
  fTab = new TGTab(this, 300, 300); 
  TGCompositeFrame *tf = fTab->AddTab("Tag Manager");

  step1 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,5, 5, 5);
  step2 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5,35, 5, 5);
  step3 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 5, 5, 25, 5);
  step4 = new TGLayoutHints(kLHintsTop | kLHintsLeft, 85, 5, 5,5);

  // Exit button frame
  ExF = new TGHorizontalFrame(this, 60, 20, kFixedWidth);
  ExitB = new TGTextButton(ExF, "&Exit", kCloseWindow);
  ExitB->Associate(this);
  fL1 = new TGLayoutHints(kLHintsLeft | kLHintsExpandX, 2, 2, 2, 2);
  ExF->AddFrame(ExitB, fL1);
  fL2 = new TGLayoutHints(kLHintsBottom | kLHintsRight, 2, 2, 5, 1);
  ExF->Resize(80, ExitB->GetDefaultHeight());
  AddFrame(ExF, fL2);

  line4 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  TitleLabel = new TGLabel(line4, "Choose DbServer Tags: ");
  line4->AddFrame(TitleLabel, step1);
  
  line5 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  domLabel = new TGLabel(line5, "Connectivity Domain: ");
  line5->AddFrame(domLabel, step1);
  domCombo = new TGComboBox(line5, 10);
  domCombo->Resize(200,20);
  domCombo->Associate(this);
  line5->AddFrame(domCombo, step1);
  tagLabel = new TGLabel(line5, "Connectivity Tag");
  line5->AddFrame(tagLabel, step1);
  tagCombo = new TGComboBox(line5, 11);
  tagCombo->Resize(250,20);
  tagCombo->Associate(this);
  line5->AddFrame(tagCombo, step1);
  
  
  line6 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  domCFLabel = new TGLabel(line6, "Configuration Domain:");
  line6->AddFrame(domCFLabel, step1);
  domCFCombo = new TGComboBox(line6, 12);
  domCFCombo->Resize(200,20);
  domCFCombo->Associate(this);
  line6->AddFrame(domCFCombo, step1);
  tagCFLabel = new TGLabel(line6, "Configuration Tag");
  line6->AddFrame(tagCFLabel, step1);
  tagCFCombo = new TGComboBox(line6, 13);
  tagCFCombo->Resize(200,20);
  tagCFCombo->Associate(this);
  line6->AddFrame(tagCFCombo, step1);
  tagModCFLabel = new TGLabel(line6, "Module Configuration Tag");
  line6->AddFrame(tagModCFLabel, step1);
  tagModCFCombo = new TGComboBox(line6, 14);
  tagModCFCombo->Resize(400,20);
  tagModCFCombo->Associate(this);
  line6->AddFrame(tagModCFCombo, step1);

  line11 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  disLabel = new TGLabel(line11, "PixDisable:");
  line11->AddFrame(disLabel, step1);
  disCombo = new TGComboBox(line11, 15);
  disCombo->Resize(200,20);
  disCombo->Associate(this);
  line11->AddFrame(disCombo, step1);
  runLabel = new TGLabel(line11, "PixRunConfig");
  line11->AddFrame(runLabel, step1);
  runCombo = new TGComboBox(line11, 16);
  runCombo->Resize(200,20);
  runCombo->Associate(this);
  line11->AddFrame(runCombo, step1);
  wsDisLabel = new TGLabel(line11, "Warm Start PixDisable:");
  line11->AddFrame(wsDisLabel, step1);
  wsDisCombo = new TGComboBox(line11, 15);
  wsDisCombo->Resize(200,20);
  wsDisCombo->Associate(this);
  line11->AddFrame(wsDisCombo, step1);

  // Is publish button
  line7 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  pubB = new TGTextButton(line7, "Publish Information in IS", kPublishToIs);
  pubB->Associate(this);
  // Update xml check
  estat = new TGCheckButton(line7,"Update XML files",1);
  estat->SetState(kButtonUp);
  // Stable Beams Override check
  sbo = new TGCheckButton(line7,"Override Stable Beams Flag",1);
  sbo->SetState(kButtonUp);
  line7->AddFrame(pubB, step1);
  line7->AddFrame(estat, step1);
  line7->AddFrame(sbo, step1);

  line8 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  readLabel = new TGLabel(line8, "Tag in IS");
  line8->AddFrame(readLabel, step1);
  // Is reading button
  IsB = new TGTextButton(line8, "Read Variables", kReadFromIs);
  IsB->Associate(this);
  line8->AddFrame(IsB, step4);

  fL3 = new TGLayoutHints(kLHintsTop | kLHintsRight, 5, 5, 5, 5);
  fL4 = new TGLayoutHints(kLHintsTop | kLHintsLeft | kLHintsExpandX | kLHintsExpandY, 5, 5, 5, 5);
  fL5 = new TGLayoutHints(kLHintsBottom | kLHintsExpandX | kLHintsExpandY, 2, 2, 5, 1);

  line9 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  idTagIsLabel = new TGLabel(line9, "idTag");
  line9->AddFrame(idTagIsLabel, step1);
  idTagT = new TGTextView(line9, 200, 40);
  line9->AddFrame(idTagT, step2);
  connTagIsLabel = new TGLabel(line9, "connTag");
  line9->AddFrame(connTagIsLabel, step2);
  connTagT = new TGTextView(line9, 200,40);
  line9->AddFrame(connTagT, step2);
  cfgTagIsLabel = new TGLabel(line9, "cfgTag");
  line9->AddFrame(cfgTagIsLabel, step2);
  cfgTagT = new TGTextView(line9, 200, 40);
  line9->AddFrame(cfgTagT, step2);
  revIsLabel = new TGLabel(line9, "cfgModTag");
  line9->AddFrame(revIsLabel, step2);
  revT = new TGTextView(line9, 400, 40);
  line9->AddFrame(revT, step2);

  line10 = new TGCompositeFrame(tf, 60, 20, kHorizontalFrame);
  isDisLab = new TGLabel(line10, "PixDisable");
  line10->AddFrame(isDisLab, step1);
  disT = new TGTextView(line10, 200, 40);
  line10->AddFrame(disT, step2);
  isRunLab = new TGLabel(line10, "PixRunConfig");
  line10->AddFrame(isRunLab, step1);
  runT = new TGTextView(line10, 200, 40);
  line10->AddFrame(runT, step2);
  isWsDisLab = new TGLabel(line10, "Warm Start PixDisable");
  line10->AddFrame(isWsDisLab, step2);
  wsDisT = new TGTextView(line10, 150, 40);
  line10->AddFrame(wsDisT, step2);
  isSBOLab = new TGLabel(line10, "Stable Beams Override");
  line10->AddFrame(isSBOLab, step2);
  wsSBOT = new TGTextView(line10, 200, 40);
  line10->AddFrame(wsSBOT, step2);

  tf->AddFrame(line4, step1);
  tf->AddFrame(line5, step1);
  tf->AddFrame(line6, step1);
  tf->AddFrame(line11, step1);
  tf->AddFrame(line7, step1);
  tf->AddFrame(line8, step3);
  tf->AddFrame(line9, step1);
  tf->AddFrame(line10, step1);
  
  AddFrame(fTab, fL5);

  tf2 = fTab->AddTab("Pixel Flags");

  tf2_line1 = new TGCompositeFrame(tf2, 20, 20, kHorizontalFrame);

  tf2_atlasVeto = new TGCheckButton(tf2_line1,"VETO Pixel/IBL Inclusion in ATLAS",1);
  tf2_atlasVeto->SetState(kButtonUp);
  tf2_line1->AddFrame(tf2_atlasVeto, step1);
  tf2->AddFrame(tf2_line1, step1);

  tf2_disableResetViset = new TGCheckButton(tf2_line1,"DISABLE ResetViset",1);
  tf2_disableResetViset->SetState(kButtonUp);
  tf2_line1->AddFrame(tf2_disableResetViset, step1);
  tf2->AddFrame(tf2_line1, step1);

  tf2_line2 = new TGCompositeFrame(tf2, 40, 20, kHorizontalFrame);

  tf2_actionsRestart = new TGCheckButton(tf2_line2,"Restart PixActions Enabled",1);
  tf2_actionsRestart->SetState(kButtonUp);
  tf2_line2->AddFrame(tf2_actionsRestart, step1);
  tf2->AddFrame(tf2_line2, step1);

  tf2_actionsRestartLocalDT = new TGCheckButton(tf2_line2,"Restart PixActions Enabled in Local Data-Taking",1);
  tf2_actionsRestartLocalDT->SetState(kButtonUp);
  tf2_line2->AddFrame(tf2_actionsRestartLocalDT, step1);
  tf2->AddFrame(tf2_line2, step1);

  // Is publish button
  tf2_line3 = new TGCompositeFrame(tf2, 60, 20, kHorizontalFrame);
  tf2_pubB = new TGTextButton(tf2_line3, "Publish Information in IS", kPublishToIs);
  tf2_pubB->Associate(this);

  loadDomain("Configuration");
  loadDomain("Connectivity");

  MapSubwindows();
  SetWindowName("TagManager for Pixel Database Server");
  Resize();   // resize to default size
  MapWindow();

  // Reading from IS upon opening the GUI
  readFromIs();
 }

content HistoFrame::checkContent(std::string dom, std::string tag) {
  std::vector<std::string> modlist;
  dbServer->listObjectsRem(dom,tag,"PixModule",modlist);
  std::vector<std::string> boclist;
  dbServer->listObjectsRem(dom,tag,"PixModuleGroup",boclist);
  if (modlist.size()==0 && boclist.size()!=0) return BOC;
  if (modlist.size()!=0 && boclist.size()==0) return MOD;
  if (modlist.size()!=0 && boclist.size()!=0) return ALL;
  return ALL;
}

void HistoFrame::loadDomain(std::string type) {
  if (type=="Configuration") {
    wsDisCombo->RemoveEntries(0,999999);
    wsDisCombo->AddEntry("--",999999);
    disCombo->RemoveEntries(0,999999);
    disCombo->AddEntry("--",999999);
    runCombo->RemoveEntries(0,999999);
    runCombo->AddEntry("--",999999);
    tagCFCombo->RemoveEntries(0, 999999);
    tagCFCombo->AddEntry("--", 999999);
    tagModCFCombo->RemoveEntries(0, 999999);
    tagModCFCombo->AddEntry("--", 999999);
    domCFCombo->RemoveEntries(0, 999999);
    domCFCombo->AddEntry("--", 999999);
    std::vector<std::string> domlist;
    dbServer->listDomainRem(domlist);
    int count=0;
    for (unsigned int i=0; i<domlist.size(); i++) {
      std::cout << "  -> domain   " << domlist[i] << std::endl;
      if (domlist[i].find(type)!=std::string::npos) {
        std::vector<std::string> tmpTagList;
        dbServer->listTagsRem(domlist[i],tmpTagList);
        if(!tmpTagList.size()) {
          std::cerr << "WARNING: Skipping empty domain " << domlist[i] << std::endl;
          continue;
        }
	domCFCombo->AddEntry((domlist[i]).c_str(), count);
	domCfglist.push_back(domlist[i]);
	count++;
      }
    }
    for (int i=0; i<6; i++) {
      tagCFCombo->AddEntry("", i);
      tagModCFCombo->AddEntry("", i);
      wsDisCombo->AddEntry("",i);
      disCombo->AddEntry("",i);
      runCombo->AddEntry("",i);
    }
  } 
  if (type=="Connectivity") {
    tagCombo->RemoveEntries(0, 999999);
    tagCombo->AddEntry("--", 999999);
    domCombo->RemoveEntries(0, 999999);
    domCombo->AddEntry("--", 999999);
    std::vector<std::string> domlist;
    dbServer->listDomainRem(domlist);
    int count=0;
    for (unsigned int i=0; i<domlist.size(); i++) {
      std::cout << "  -> domain   " << domlist[i] << std::endl;
      if (domlist[i].find(type)!=std::string::npos) {
        std::vector<std::string> tmpTagList;
        dbServer->listTagsRem(domlist[i],tmpTagList);
        if(!tmpTagList.size()) {
          std::cerr << "WARNING: Skipping empty domain " << domlist[i] << std::endl;
          continue;
        }
	domCombo->AddEntry((domlist[i]).c_str(), count);
	domConnlist.push_back(domlist[i]);
	count++;
      }
    }
    for (int i=0; i<6; i++) {
      tagCombo->AddEntry("", i);
    }
  }
}

void HistoFrame::loadTag(std::string type) {
  if (type=="Connectivity") {
    tagCombo->RemoveEntries(0, 999999);
    tagCombo->AddEntry("--", 999999);
    std::string dom;
    int sel=domCombo->GetSelected();
    std::cout << "Selection is: " << sel << std::endl;
    if (sel>-1 && sel <10) {
      dom = domConnlist[domCombo->GetSelected()];
      dbServer->listTagsRem(dom,tagConnlist);
      for (unsigned int i=0; i<tagConnlist.size(); i++) {
	std::cout << "  -> tag   " << tagConnlist[i] << std::endl;
	tagCombo->AddEntry((tagConnlist[i]).c_str(), i); 
      }
    }
  }
  if (type=="Configuration") {
    wsDisCombo->RemoveEntries(0,999999);
    wsDisCombo->AddEntry("--",999999);
    disCombo->RemoveEntries(0,999999);
    disCombo->AddEntry("--",999999);
    runCombo->RemoveEntries(0,999999);
    runCombo->AddEntry("--",999999);
    tagCFCombo->RemoveEntries(0, 999999);
    tagCFCombo->AddEntry("--", 999999);
    std::string dom;
    int sel=domCombo->GetSelected();
    std::cout << "Selection is: " << sel << std::endl;
    if (sel>-1 && sel <10) {
      dom = domCfglist[domCFCombo->GetSelected()];
      std::vector<std::string> taglist;
      dbServer->listTagsRem(dom,taglist);
      int count=0;
      for (unsigned int i=0; i<taglist.size(); i++) {
	std::cout << "  -> tag   " << taglist[i] << std::endl;
	content myC = checkContent(dom,taglist[i]);
	if (myC==ALL) {
	  tagCFCombo->AddEntry((taglist[i]).c_str(), count); 
	  tagCfglist.push_back(taglist[i]);
	  count++;
	} else if (myC==BOC) {
	  std::cout << "Tag " << taglist[i] << " does not contain Module config.  WARNING!!!!!" << std::endl;
	  tagCFCombo->AddEntry((taglist[i]).c_str(), count); 
	  tagCfglist.push_back(taglist[i]);
	  count++;
	} else {
	  std::cout << "Tag " << taglist[i] << " does not contain BOC config" << std::endl;
	}  
      }
    }
    for (int i=0; i<6; i++) {
      wsDisCombo->AddEntry("",i);
      disCombo->AddEntry("",i);
      runCombo->AddEntry("",i);
    }
    
  }
  if (type=="Configuration-Mod") {
    tagModCFCombo->RemoveEntries(0, 999999);
    tagModCFCombo->AddEntry("--", 999999);
    std::string dom;
    int sel=domCombo->GetSelected();
    std::cout << "Selection is: " << sel << std::endl;
    if (sel>-1 && sel <10) {
      dom = domCfglist[domCFCombo->GetSelected()];
      std::vector<std::string> taglist;
      dbServer->listTagsRem(dom,taglist);
      int count=0;
      for (unsigned int i=0; i<taglist.size(); i++) {
	std::cout << "  -> tag   " << taglist[i] << std::endl;
	content myC = checkContent(dom,taglist[i]);
	if (myC==ALL || myC==MOD) {
	  tagModCFCombo->AddEntry((taglist[i]).c_str(), count);
	  count++;
	  tagCfgModlist.push_back(taglist[i]);
	}
      }
    }
  }
}

void HistoFrame::loadDis() {
  wsDisCombo->RemoveEntries(0,999999);
  wsDisCombo->AddEntry("--",999999);
  disCombo->RemoveEntries(0,999999);
  disCombo->AddEntry("--",999999);
  std::string dom,tag;
  int sel=domCombo->GetSelected();
  int sel1=tagCFCombo->GetSelected();
  std::cout << "Selection is: " << sel << "   " << sel1 <<  std::endl;
  if (sel >= (int)domCfglist.size()) {
    std::cerr << "ERROR: sel (=" << sel << ") not available in domCfglist." << std::endl;
    return;
  }
  if (sel1 >= (int)tagCfglist.size()) {
    std::cerr << "ERROR: sel1 (=" << sel1 << ") not available in tagCfglist." << std::endl;
    return;
  }
  if (sel>-1 && sel <100) {
    dom = domCfglist[sel];
    if (sel1>-1 && sel1<100) {
      tag = tagCfglist[sel1];
      std::vector<std::string> tempDisList;
      dbServer->listObjectsRem(dom,tag,"PixDisable",tempDisList);
      int count=0;
      for (unsigned int i=0; i<tempDisList.size(); i++) {
	std::cout << "  -> disable  " << tempDisList[i] << std::endl;
	if (tempDisList[i].find("S0000")==std::string::npos) {
	  disCombo->AddEntry((tempDisList[i]).c_str(), count);
	  wsDisCombo->AddEntry((tempDisList[i]).c_str(), count);
	  count++; 
	  dislist.push_back(tempDisList[i]);
	}
      }
    }
  } 
}

void HistoFrame::loadRun() {
  runCombo->RemoveEntries(0,999999);
  runCombo->AddEntry("--",999999);
  std::string dom,tag;
  int sel=domCombo->GetSelected();
  int sel1=tagCFCombo->GetSelected();
  std::cout << "Selection is: " << sel << "   " << sel1 <<  std::endl;
  if (sel >= (int)domCfglist.size()) {
    std::cerr << "ERROR: sel (=" << sel << ") not available in domCfglist." << std::endl;
    return;
  }
  if (sel1 >= (int)tagCfglist.size()) {
    std::cerr << "ERROR: sel1 (=" << sel1 << ") not available in tagCfglist." << std::endl;
    return;
  }
  if (sel>-1 && sel <100) {
    dom = domCfglist[sel];
    if (sel1>-1 && sel1<100) {
      tag = tagCfglist[sel1];
      std::vector<std::string> tempRunList;
      dbServer->listObjectsRem(dom,tag,"PixRunConfig",tempRunList);
      int count=0;
      for (unsigned int i=0; i<tempRunList.size(); i++) {
	std::cout << "  -> runConfig   " << tempRunList[i] << std::endl;
	if (tempRunList[i].find("|")==std::string::npos) {
	  runCombo->AddEntry((tempRunList[i]).c_str(), count);
	  count++; 
	  runlist.push_back(tempRunList[i]);
	}
      }
    }
  } 
}



void HistoFrame::publishInIs() {
  std::cout << "Checking Tag value before pubblication ....." << std::endl;
  std::cout << "Button content is " << estat->IsOn() << std::endl;
  std::string IdTag;
  std::string ConnTag;
  std::string CfgTag;
  std::string ModCfgTag;
  std::string Disable="";
  std::string wsDisable="";
  std::string RunConfig="";
  int sel=tagCombo->GetSelected();
  std::cout << "Selection is: " << sel << std::endl;
  if (sel>-1 && sel<100) {
    ConnTag=tagConnlist[sel];
    std::cout << "Connectivity Tag: " << ConnTag << std::endl;
  } else {
    std::cout << "Connectivity Tag Not Chosen " << std::endl;
    return;
  }
  sel=tagCFCombo->GetSelected();
  if (sel>-1 && sel<100) {
    CfgTag=tagCfglist[sel];
    std::cout << "Configuration Tag: " << CfgTag << std::endl;
  } else {
    std::cout << "Configuration Tag Not Chosen " << std::endl;
    return;
  }
  sel=tagModCFCombo->GetSelected();
  if (sel>-1 && sel<100) {
    ModCfgTag=tagCfgModlist[sel];
    std::cout << "Module Configuration Tag: " << ModCfgTag << std::endl;
  } else {
    std::cout << "Module Configuration Tag Not Chosen, setting equal to Configuration Tag " << std::endl;
    ModCfgTag=CfgTag;
    if (checkContent("Configuration-"+IdTag,ModCfgTag)==BOC) {
      std::cout << "Configuration Tag does not contain Module configuration" << std::endl;
      return;
    }
  }
  sel=domCFCombo->GetSelected();
  if (sel>-1 && sel<100) {
    IdTag=domCfglist[sel].substr(14,20);
    std::cout << "ID Tag: " << IdTag << std::endl;
  } else {
    std::cout << "Configuration domain not chosen" << std::endl;
    return;
  }
  sel=disCombo->GetSelected();
  if (sel>-1 && sel<1000) {
    Disable=dislist[sel];
    std::cout << "PixDisable: " << Disable << std::endl;
  } else {
    std::cout << "PixDisable not chosen" << std::endl;
  }
  sel=runCombo->GetSelected();
  if (sel>-1 && sel<100) {
    RunConfig=runlist[sel];
    std::cout << "PixRunConfig: " << RunConfig << std::endl;
  } else {
    std::cout << "PixRunConfig not chosen" << std::endl;
  }
  sel=wsDisCombo->GetSelected();
  if (sel>-1 && sel<1000) {
    wsDisable=dislist[sel];
    std::cout << "WS PixDisable: " << Disable << std::endl;
  } else {
    std::cout << "WS PixDisable not chosen" << std::endl;
  }
  std::cout << std::endl << "Wrinting tag in IS" << std::endl << std::endl;
  try {
    isManager->publish("DataTakingConfig-IdTag", IdTag); 
    isManager->publish("DataTakingConfig-ConnTag", ConnTag); 
    isManager->publish("DataTakingConfig-CfgTag", CfgTag); 
    isManager->publish("DataTakingConfig-ModCfgTag", ModCfgTag);  
    if (Disable!="") isManager->publish("DataTakingConfig-Disable", Disable); 
    if (RunConfig!="") isManager->publish("DataTakingConfig-RunConfig", RunConfig);  
    if (wsDisable == "") {
      isManager->publish("DataTakingConfig-WsDisable", "NONE");
    } else {
      isManager->publish("DataTakingConfig-WsDisable", wsDisable);
    } 
    if (sbo->IsOn()) {
      int override = 1; 
      isManagerDDC->publish("StableBeamsOverride", override);
    } else {
      int override = 0; 
      isManagerDDC->publish("StableBeamsOverride", override);
    }
  } catch(...) {
    std::cout << "Problem in writing tags information in Is Server" << std::endl;
  }
  if (estat->IsOn()) {
    std::cout << "Writing XML files" << std::endl;
    char *venv;
    venv = getenv("PIX_PART_INFR_NAME");
    if (venv != NULL) ipcPartitionName = venv;
    venv = getenv("PIX_PART_DD_NAME");
    if (venv != NULL) {
      dataTakingPartitionNamePit = venv;
      dataTakingPartitionNameSr1 = venv; 
    }
    venv = getenv("IBL_PART_DD_NAME");
    if (venv != NULL) {
      dataTakingPartitionNamePit_IBL = venv;
      dataTakingPartitionNameSr1_IBL = venv;
    }

    venv = getenv("PIXRCD_FOLDER");
    if (venv != NULL) rcdFolder = venv;
    std::cout <<  dataTakingPartitionNamePit << " / " << dataTakingPartitionNameSr1 <<  " / " << rcdFolder << std::endl;
    
    try {
      conn = new PixConnectivity(dbServer, ConnTag, ConnTag, ConnTag, CfgTag, IdTag, ModCfgTag);
    }  
    catch(const PixConnectivityExc &e) { 
      std::cout <<"PixConnectivity: "<<e.getDescr()<<std::endl; 
      exit(-1);
    }
    catch(...) { 
      std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating PixConnectivity"<<std::endl; 
      exit(-1);
    }
    try {
      conn->loadConn();
    }
    catch (...) {
      std::cout <<"Error reading PixConnectivity; tag " << ConnTag << std::endl; 
      exit(-1);
    }
    if (Disable=="") Disable="GlobalDis";
    PixDisable* disConfig = conn->getDisable(Disable);
    if (disConfig!=NULL) disConfig->put(conn);
    else std::cout << "Problem in reading Pixdisble " << Disable << " from DbServer" << std::endl;
    if (rcdFolder == "PIT") justifyXmlDisables(conn, dataTakingPartitionNamePit);
    if (rcdFolder == "SR1") justifyXmlDisables(conn, dataTakingPartitionNameSr1);
  }
}

void HistoFrame::readFromIs() {
  sleep(1);
  bool success=true;
  std::string idTag = ""; 
  std::string connTag = "";
  std::string cfgTag = "";
  std::string cfgMTag = "";
  std::string disName = "";
  std::string runName = "";
  std::string wsDisName = "";
  int SBOval= 0;
  bool restartActions = false;
  bool restartActionsInLocalDT = false;
  
  try { 
    idTag =  isManager->read<string>("DataTakingConfig-IdTag");
    connTag =  isManager->read<string>("DataTakingConfig-ConnTag");
    cfgTag =  isManager->read<string>("DataTakingConfig-CfgTag");
    cfgMTag =  isManager->read<string>("DataTakingConfig-ModCfgTag");
    std::cout << "Tags in Is are: "+idTag+" / "+connTag+" / "+cfgTag+" / "+cfgMTag << std::endl;
  }
  catch (...) {
    std::cout << "Cannot read tags from IS" << std::endl;
    success=false;
  }

  if(success) {
    idTagT->Clear();
    idTagT->AddLineFast(idTag.c_str());
    idTagT->Update();
    connTagT->Clear();
    connTagT->AddLine(connTag.c_str());
    connTagT->Update();
    cfgTagT->Clear();
    cfgTagT->AddLine(cfgTag.c_str());
    cfgTagT->Update();
    revT->Clear();
    revT->AddLine(cfgMTag.c_str());
    revT->Update();
    std::string connDom(std::string("Connectivity-")+idTag);
    domCombo->Select(std::find(domConnlist.begin(),domConnlist.end(),connDom.c_str())-domConnlist.begin());
    std::string cfgDom(std::string("Configuration-")+idTag);
    domCFCombo->Select(std::find(domCfglist.begin(),domCfglist.end(),cfgDom.c_str())-domCfglist.begin());
    loadTag("Connectivity");
    loadTag("Configuration");
    loadTag("Configuration-Mod");
    loadDis();
    loadRun();
    tagCombo->Select(std::find(tagConnlist.begin(),tagConnlist.end(),connTag.c_str())-tagConnlist.begin());
    tagCFCombo->Select(std::find(tagCfglist.begin(),tagCfglist.end(),cfgTag.c_str())-tagCfglist.begin());
    tagModCFCombo->Select(std::find(tagCfgModlist.begin(),tagCfgModlist.end(),cfgMTag.c_str())-tagCfgModlist.begin());
   }

  try { 
    disName =  isManager->read<string>("DataTakingConfig-Disable");
  }
  catch (...) {
    std::cout << "Cannot read PixDisable Name from IS" << std::endl;
    success=false;
  }
  if(success) {
    disT->Clear();
    disT->AddLine(disName.c_str());
    disT->Update();
    loadDis();
    disCombo->Select(std::find(dislist.begin(),dislist.end(),disName.c_str())-dislist.begin());
  }

  try { 
    runName = isManager->read<string>("DataTakingConfig-RunConfig");
  }
  catch (...) {
    std::cout << "Cannot read PixRunConfig Name from IS" << std::endl;
    success=false;
  }
  if(success) {
    runT->Clear();
    runT->AddLine(runName.c_str());
    runT->Update();
    loadRun();
    runCombo->Select(std::find(runlist.begin(),runlist.end(),runName.c_str())-runlist.begin());
  }

  try { 
    wsDisName = isManager->read<string>("DataTakingConfig-WsDisable");
  }
  catch (...) {
    std::cout << "Cannot read WS PixDisable Name from IS" << std::endl;
    success=false;
  }
  if(success) {
    wsDisT->Clear();
    wsDisT->AddLine(wsDisName.c_str());
    wsDisT->Update();   
    wsDisCombo->Select(std::find(dislist.begin(),dislist.end(),wsDisName.c_str())-dislist.begin());
  }

  try { 
    SBOval = isManagerDDC->read<int>("StableBeamsOverride");
  }
  catch (...) {
    SBOval = 0;
  }
  std::ostringstream os;
  os << SBOval;
  wsSBOT->Clear();
  wsSBOT->AddLine(os.str().c_str());
  wsSBOT->Update();
  if (SBOval == 1) sbo->SetState(kButtonDown);
  else sbo->SetState(kButtonUp);

  try {
    restartActions = isManager->read<bool>("PIX_RestartActionsEnabled");
    restartActionsInLocalDT = isManager->read<bool>("PIX_RestartActionsInLocalDataTakingEnabled");
  }
  catch (...) {
    restartActions = false;
    restartActionsInLocalDT = false;
  }

  tf2_actionsRestart->SetState( restartActions ? kButtonDown : kButtonUp);
  tf2_actionsRestartLocalDT->SetState( restartActionsInLocalDT ? kButtonDown : kButtonUp);


}

HistoFrame::~HistoFrame(){
  Cleanup();
}

void HistoFrame::selectCanvas() { 
  fTab1->SetTab("Histogram");
}

void HistoFrame::selectText() {
  fTab1->SetTab("Text");
}

void HistoFrame::CloseWindow() {
   // Called when window is closed via the window manager.
   gApplication->Terminate(0);
}

Bool_t HistoFrame::ProcessMessage(Long_t msg, Long_t parm1, Long_t) {
  switch (GET_MSG(msg)) {
  case kC_COMMAND:
    switch (GET_SUBMSG(msg)) {
    case kCM_COMBOBOX:
      switch(parm1) {
      case 10:
	loadTag("Connectivity");
	break;
      case 12:
	loadTag("Configuration");
	loadTag("Configuration-Mod");
	break;
      case 13:
	loadRun();
	loadDis();
	break;
      }
      break;
    case kCM_BUTTON:
      switch(parm1) {
      case kCloseWindow:
	CloseWindow();
	break;
      case kPublishToIs:
	publishInIs();
	readFromIs();
	break;
      case kReadFromIs:
	readFromIs();
	break;
	break;
      }
      break;
    default:
      break;
    }
    break;
  default:
    break;
  }
  return kTRUE;
}

void getParams(int argc, char **argv) {
  int ip = 1;
  mode = NORMAL;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	mode = HELP;
	break;
      } else if (std::string(argv[ip]) == "--dbPart") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  ipcPartitionName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--dbServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  dbServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
	  break;
	}
      } else if (std::string(argv[ip]) == "--isServ") {
	if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
	  IsServerName = argv[ip+1];
	  ip += 2; 
	} else {
	  mode = HELP;
	  std::cout << std::endl << "No IsServer name inserted" << std::endl << std::endl;
	  break;
	}
      } 
    }
  }
}

void help() {
    cout << endl;
    cout << "This program gives the possibility to load tag in DbServer and set the reference tag for DataTaking " << endl;
    cout << endl; 
    std::cout << "Usage: TagManager --help\n"
	      << "            // prints this message\n"
	      <<std::endl;
    cout << "Optional switches:" << endl;
    cout << "  --dbPart <part>      IPC Partition  (def. PixelInfr)     " << endl;
    cout << "  --dbServ <dbServer>  DB server name (def. PixelDbServer) " << endl;
    cout << "  --isServ <IsServer>  IS server name (def. PixelRunParams)" << endl;
    cout << endl;
    exit(0);
}

int main(int argc, char **argv) {

  getParams(argc, argv);
  if(mode == HELP) {
    help();
  } else {
    IPCCore::init(argc, argv);
    connect();
   
    TApplication *app = new TApplication("app",NULL,NULL);
    HistoFrame mainWindow(gClient->GetRoot(), 400, 350);
    app->Run(true);
  }
}
