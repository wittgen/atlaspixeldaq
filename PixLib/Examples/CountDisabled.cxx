#include <string>
#include <vector>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <map>
#include <ipc/object.h>
#include <ipc/alarm.h>
#include <ipc/core.h>
#include <pmg/pmg_initSync.h>
#include <sys/time.h>
#include <string>

#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/PixDisable.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "RootDb/RootDb.h"
#include "RCCVmeInterface.h"

#include "PixDbServer/PixDbServerInterface.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFe.h"
#include "Config/Config.h"
#include "Config/ConfMask.h"
#include "Config/ConfGroup.h"
#include "Config/ConfObj.h"

PixDbServerInterface *DbServer = NULL;
IPCPartition *ipcPartition = NULL;
PixDisable* disableConfig = NULL;
PixConnectivity* conn = NULL;

std::string idTag = "PIXEL";
//std::string idTag = "SR1";
std::string domainName = "Configuration-"+idTag;
std::string cfgModTag = "PIT_MOD";
//std::string cfgModTag = "TOOTHPIX-ORIG";
std::string dbsPart = "PixelInfr";
std::string dbsName = "PixelDbServer";
std::string cfgTag = "";
std::string connTag = "";
std::string disName = "";
unsigned int revision = 0;
bool help=false;


using namespace PixLib;

void openDbServer(int argc, char** argv) {
  // Start IPCCore
  IPCCore::init(argc, argv);
  // Create IPCPartition constructors
  try {
    ipcPartition = new IPCPartition(dbsPart);
  }
  catch(...) {
    throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETPART", "Cannot connect to partition "+dbsPart));
  }
  bool ready=false;
  int nTry=0;
  if (ipcPartition != NULL) {
    do {
      DbServer = new PixDbServerInterface(ipcPartition, dbsName, PixDbServerInterface::CLIENT, ready);
      if (!ready) {
        delete DbServer;
      } else {
        break;
      }
      nTry++;
      sleep(1);
    } while (nTry<20);
    if (!ready) {
      throw(PixConnectivityExc(PixConnectivityExc::ERROR, "CANTGETDBS", "Cannot connect to DB server "+dbsPart));
    } else {
      std::cout << "Successfully connected to DB Server " << dbsName << std::endl;
    }
    sleep(2);
  }
  DbServer->ready();
}

void getParams(int argc, char **argv) {
  int ip = 1;
  while (ip < argc) {
    if (std::string(argv[ip]).substr(0,2) == "--") {
      if (std::string(argv[ip]) == "--help") {
	help=true;
        break;
      } else if (std::string(argv[ip]) == "--dbPart") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          dbsPart = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No partition name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--dbServ") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          dbsName = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No dbServer name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--idTag") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          idTag = argv[ip+1];
          ip +=2;
        } else {
          std::cout << std::endl << "No ID tag name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--modTag") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          cfgModTag = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No module config tag name inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--rev") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--" ) {
          revision = atoi(argv[ip+1]);
          ip += 2;
        } else  {
          std::cout << std::endl << "No revision inserted" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--connTag") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          connTag = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No connectivity tag name inserted for disable object" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--disTag") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          cfgTag = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No config tag name inserted for disable object" << std::endl << std::endl;
          break;
        }
      } else if (std::string(argv[ip]) == "--dis") {
        if (ip+1 < argc && std::string(argv[ip+1]).substr(0,2) != "--") {
          disName = argv[ip+1];
          ip += 2;
        } else {
          std::cout << std::endl << "No disable name inserted" << std::endl << std::endl;
          break;
        }
      }
    } else {
      std::cout << std::endl << "COMMAND: '" << std::string(argv[ip]) << "' unknown!!!" << std::endl << std::endl;
      break;
    }
  }
}


int main(int argc, char** argv) {
  std::string dbx = "";
  try {

    getParams(argc, argv);
    if (help){
      std::cout << "Usage: CountTDACs\n"
		<< "     Optional switches:\n"
		<< "        --dbPart  <partition>             Specify partition name                         def. " << dbsPart << std::endl
		<< "        --dbServ  <name>                  Specify dbServer name                          def. " << dbsName << std::endl
		<< "        --idTag   <ID tag>                Specify ID tag name                            def. " << idTag << std::endl
		<< "        --modTag  <config module tag>     Specify config module tag name                 def. " << cfgModTag << std::endl
		<< "        --rev     <revision>              Specify object revision  NOT TESTED!!!         def. last\n"
	        << "        --connTag <connectivity tag>      Specify connectivity tag name for disable      def. none\n"
		<< "        --disTag  <config tag>            Specify config tag name for disable            def. none\n"
                << "        --dis     <name>                  Specify disable name for modules to be ignored def. none" << std::endl 
		<< std::endl;
      return 0;
    }
    // Open DB server connection
    if (DbServer == NULL) openDbServer(argc, argv);

    domainName = "Configuration-"+idTag;

    int countMod = 0;
    int totalFEs = 0;
    int totalCPs = 0;
    int totalDis = 0;
    int total127 = 0;

    std::string filename;
    std::vector<std::string> modlist;
    DbServer->listObjectsRem(domainName,cfgModTag,"PixModule",modlist);
    std::vector<std::string>::iterator modit;

    if (disName!="" && cfgTag!=""&& connTag!="") {
      try {
	conn = new PixConnectivity(DbServer, connTag, connTag, connTag, cfgTag, idTag);
      }  
      catch(PixConnectivityExc e) { 
	std::cout <<"PixConnectivity: "<<e.getDescr()<<std::endl; 
	return -1;
      }
      catch(...) { 
	std::cout <<"Unknown exception caught in RunConfigEditor.cxx while creating PixConnectivity"<<std::endl; 
	return -1;
      }
      try {
	conn->loadConn();
      }
      catch (...) {
	std::cout <<"Error reading PixConnectivity; tag " << connTag << std::endl; 
	return -1;
      }
      disableConfig = new PixDisable(disName);
      disableConfig->readConfig(DbServer,domainName,cfgTag);
      disableConfig->put(conn);
    }

    std::map<std::string, ModuleConnectivity*> modConnMap = conn->mods;

    for (modit = modlist.begin(); modit != modlist.end(); modit++) { 
      PixModule *mod = new PixModule(DbServer,(PixModuleGroup*)NULL,domainName,cfgModTag,*modit);
      ModuleConnectivity *modConn = modConnMap[mod->connName()];
      if (!(modConn->active()) || !(modConn->enableReadout)){
	continue;
      }

      countMod++;

      //read special revision if not newest
      // 
      // NOT tested!
      if (revision!=0){
	if (!(mod->readConfig(DbServer, domainName, cfgModTag, revision))){
	  std::cout << "ERROR could not read Configuration for module\n";
	}
      }

      int countFEs = 0;
      int countCPs = 0;
      int count127 = 0;
      int countDis = 0;
      std::vector<int> FEs;
      std::map<int,std::vector<int> > CPs;
      for (std::vector<PixFe*>::iterator fe = mod->feBegin(); fe != mod->feEnd(); fe++){
	Config &cfg = (*fe)->config();
	bool feEnabled = ((ConfBool&)cfg["Misc"]["ConfigEnable"]).m_value;
        ConfMask<unsigned short int> &tdac_mask = (*fe)->readTrim("TDAC");
	ConfMask<bool> &ro_enable = (*fe)->readPixRegister("ENABLE");

	if (!feEnabled) {
	  countFEs++;
	  FEs.push_back((*fe)->number());
	  continue;
	}

	Config *subcfg=0;
	for(unsigned int j=0; j<cfg.subConfigSize(); j++){
	  if(cfg.subConfig(j).name()=="GlobalRegister_0"){
	    subcfg = &(cfg.subConfig(j));
	    break;
	  }
	}

	std::vector<int> CPsInFe;
	for (int i = 0; i < 9; i++) {
	  std::stringstream var;
	  var << "ENABLE_CP" << i;
	  Config &globreg = *subcfg;
	  int cpEnabled = *((int *)((ConfInt&)globreg["GlobalRegister"][var.str()]).m_value);
	  if (cpEnabled == 0) {
	    countCPs++;
	    CPsInFe.push_back(i);
	  }
	}

	if (CPsInFe.size() > 0) {
	  CPs.insert(std::make_pair((*fe)->number(),CPsInFe));
	}

	for (int col=0; col<tdac_mask.ncol(); col++) {
	  if (CPsInFe.size() > 0) {
	    bool skip = false;
	    for (std::vector<int>::iterator it = CPsInFe.begin(); it != CPsInFe.end(); it++) {
	      if ((col/2) == (*it)) {
		skip = true;
	      }
	    }
	    if (skip) {
	      continue;
	    }
	  }
          for (int row=0; row<tdac_mask.nrow(); row++) {
            int tdac = tdac_mask.get(col, row);
	    if (tdac >=127) {
	      count127++;
	    }
	    if (!(ro_enable.get(col, row))) {
	      countDis++;
	    }
          }
	}
      }
      std::cout << std::endl << mod->connName() << ":" << std::endl;
      if (countFEs > 0) {
	std::cout << "* Disabled FEs: " << countFEs << " ( ";
	for (std::vector<int>::iterator it = FEs.begin(); it != FEs.end(); it++) { 
	  std::cout << (*it) << " ";
	}
	std::cout << ")" << std::endl;
      }
      if (countCPs > 0) {
	std::cout << "* Disabled CPs: " << countCPs << std::endl;
	for (std::map<int,std::vector<int> >::iterator it = CPs.begin(); it != CPs.end(); it++) {
	  std::cout << "** FE: " << (*it).first << " ( ";
	  for (std::vector<int>::iterator it1 = (*it).second.begin(); it1 != (*it).second.end(); it1++) {
	    std::cout << (*it1) << " ";
	  }
	  std::cout << ")" << std::endl;
	}
      }
      if (countDis > 0) {
	std::cout << "* Disabled pixels: " << countDis << std::endl;
      }
      if (count127 > 0) {
	std::cout << "* Untuned pixels: " << count127 << std::endl;
      }
      FEs.clear();
      CPs.clear();
      totalFEs += countFEs;
      totalCPs += countCPs;
      totalDis += countDis;
      total127 += count127;
    }
    std::cout << std::endl << "Number of enabled modules: " << countMod << std::endl;  
    std::cout << "Total number of disabled FEs: " << totalFEs << std::endl;
    std::cout << "Total number of disabled CPS: " << totalCPs << std::endl;
    std::cout << "Total number of disabled pixels: " << totalDis << std::endl;
    std::cout << "Total number of untunable pixels: " << total127 << std::endl;
  }
  catch(PixConnectivityExc& e) {
    std::cerr<<"Got PixConnectivityExc: ";
    e.dump(std::cerr);
    std::cerr<<std::endl;
    return -1;
  }
  catch(std::exception& e) {
    std::cerr<<"Got std::exception: "<<e.what()<<std::endl;
    if (dbx != "") std::remove(dbx.c_str());
    return -1;
  }

  return 0;
}
