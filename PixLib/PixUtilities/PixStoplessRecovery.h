
#include "PixUtilities/PixMessages.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixRcCommander.h"

#include "PixDbServer/PixDbServerInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"

#ifndef PIX_STOPLESS_RECOVERY
#define PIX_STOPLESS_RECOVERY


namespace PixLib {

  namespace PixStoplessRecovery {

    bool autoDisable(const std::string &partitionName, const std::string &rodName, const std::string &mode, const std::string &item);
    bool autoRecover(const std::string &partitionName, const std::string &rodName ,std::string applName = "Pixel");
    std::unique_ptr <PixISManager> getIsManager(const std::string &partitionName);
    bool allocated(PixISManager* const isManagerPtr, const std::string &isName, std::string &autoDisPartName);
    bool waitAutoDis(PixISManager* const isManager, const std::string &rodName, const std::string &action);
    bool getParStoplessRecovery(const std::string &partition, const std::string &item ,std::map  <std::string, std::string> &disableMap );
    bool getIsString(const std::string &partition, const std::string &rodName, std::string &parName);
    std::unique_ptr<PixConnectivity> getConnectivity(const std::string &partition );
  }

} //namespace

#endif //PIX_STOPLESS_RECOVERY

