/////////////////////////////////////////////////////////////////////
// PixISManager.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 02/04/07  Version 1.0 (DL,II)
//           Full version
//
// Note: Names of IS variables are passed to methods in a "bare" form, i.e. w/o specifying
// a server name followed by dot. Full name will be constructed inside a method.  

#ifndef _PIXLIB_ISMANAGER
#define _PIXLIB_ISMANAGER

#include <string>
#include <set>

#include <ipc/partition.h>
#include <ipc/object.h>
#include <is/infodictionary.h>
#include <is/inforeceiver.h>
#include <is/infoT.h>
#include <is/infoiterator.h>

#include "BaseException.h"
#include <ipc/alarm.h>
#include <rc/RunParams.h>
#include <TRP/LuminosityInfo.h>
#include <ddc/DdcData.hxx>
#include <TTCInfo/LumiBlock.h>

static inline bool timeout_callback(void* param)
{ 
  std::pair<bool*,bool*>* input = (std::pair<bool*,bool*>*) param;
  (*(input->first)) = true;
  (*(input->second)) = true; 
  return false ;
}

template<class T> class ISInfoT;

namespace PixLib {

class PixISManagerExc : public SctPixelRod::BaseException{
public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  PixISManagerExc(ErrorLevel el, std::string name) : BaseException(name), m_errorLevel(el), m_name(name) {}; 
  virtual ~PixISManagerExc() {};

  //! Dump the error
  virtual void dump(std::ostream &out) {
    out << "PixISManager " << m_name << " -- Level : " << dumpLevel(); 
  }
  std::string dumpLevel() {
    switch (m_errorLevel) {
    case INFO : 
      return "INFO";
    case WARNING :
      return "WARNING";
    case ERROR :
      return "ERROR";
    case FATAL :
      return "FATAL";
    default :
      return "UNKNOWN";
    }
  }
  ErrorLevel getErrorLevel() const { return m_errorLevel; };
  std::string getISmanName() const { return m_name; };
  std::string getExcName() const { return m_excName; };
 private:
  ErrorLevel m_errorLevel;
  std::string m_name;
  std::string m_excName;
};

 struct ManagerCallback;
  
 class PixISManager {

 public:

   enum WaitAction{ BiggerThan = 0, LessThan = 1, EqualTo = 2, Change = 3 };
   
   // Constructor
   PixISManager(IPCPartition* ipcPartition, std::string isServerName = "RunParams");
   PixISManager(std::string ipcPartitionName = "Partition_PixelDDC", std::string isServerName = "RunParams");
   
   // Destructor
   virtual ~PixISManager();

   virtual std::string name() { return m_ipcPartitionName; };
   
   virtual void publish(const char* name, unsigned short value);
   virtual void publish(const char* name, short value);
   virtual void publish(const char* name, long value);
   virtual void publish(const char* name, unsigned long value);
   virtual void publish(const char* name, int value);
   virtual void publish(const char* name, unsigned int value);
   virtual void publish(const char* name, float value);
   virtual void publish(const char* name, double value);
   virtual void publish(const char* name, const char* value){publish(name,std::string(value));}
   virtual void publish(const char* name, std::string value);
   virtual void publish(const char* name, std::vector<unsigned int>value);
   
   virtual void publish(std::string name, short value){publish(name.c_str(),value);}
   virtual void publish(std::string name, unsigned short value){publish(name.c_str(),value);}
   virtual void publish(std::string name, long value){publish(name.c_str(),value);}
   virtual void publish(std::string name, unsigned long value){publish(name.c_str(),value);}
   virtual void publish(std::string name, int value){publish(name.c_str(),value);}
   virtual void publish(std::string name, unsigned int value){publish(name.c_str(),value);}
   virtual void publish(std::string name, float value){publish(name.c_str(),value);}
   virtual void publish(std::string name, double value){publish(name.c_str(),value);}
   virtual void publish(std::string name, const char* value){publish(name.c_str(),value);}
   virtual void publish(std::string name, std::string value){publish(name.c_str(),value);}
   virtual void publish(std::string name, std::vector<unsigned int> value){publish(name.c_str(),value);}
     
   template<typename T> T read(const char* name);
   template<typename T> T readDcs(const char* name);
   template<typename T> T read(const char* name, int &time);
   template<typename T> T read(std::string name){return read<T>(name.c_str());}
   template<typename T> T read(std::string name, int& time ){return read<T>(name.c_str(), time);}
   
   virtual void removeVariable(const char* name);                                      //remove a single variable
   virtual void removeVariable(std::string name){removeVariable(name.c_str());}
   virtual void removeVariables(const char* suffix);                                   //combination of listVariables and removeVariable methods  
   virtual void removeVariables(std::string suffix){removeVariables(suffix.c_str());}

   bool exists(const char* name);
   bool exists(std::string name){return exists(name.c_str());}
   
   virtual std::vector<std::string> listVariables(const char* name);
   virtual std::vector<std::string> listVariables(std::string name){return listVariables(name.c_str());}

   void subscribe(const char* name){subscribe(std::string(name));}
   void subscribe(std::string name);
   void subscribe(std::vector<std::string> names);

   void unsubscribe(const char* name){unsubscribe(std::string(name));}
   void unsubscribe(std::string name);
   void unsubscribe(std::vector<std::string> names);
   void unsubscribeAll();
   
   std::string getPartitionName() { return m_ipcPartitionName;}

   void setWaitAction(PixISManager::WaitAction action);

   template<typename T> bool waitOnSubscribed(T value, double timeout = -1);
   
 protected:
   
   // SW interfaces
   IPCPartition* m_ipcPartition;
   ISInfoDictionary* m_infoDictionary;
   ISInfoReceiver* m_rec;
   std::string m_isServerName;
   std::string m_ipcPartitionName;
   ManagerCallback* m_callback;
 };

 struct ManagerCallback{
   
   ManagerCallback(ISInfoReceiver* rec);
   
   void init();
   
   void arrayCallback(ISCallbackInfo* isc);

   void setWaitValue(unsigned short value);
   void setWaitValue(short value);
   void setWaitValue(unsigned long value);
   void setWaitValue(long value);
   void setWaitValue(unsigned int value);
   void setWaitValue(int value);
   void setWaitValue(float value);
   void setWaitValue(double value);
   void setWaitValue(const char* value);
   void setWaitValue(std::string value);
   
   ISInfoReceiver* m_recCallback;
   int             m_type;
   bool            m_deleted;
   bool            m_stop;
   int             m_callbackCalls;
   PixISManager::WaitAction m_action;

   ISInfo* m_value;
   std::vector<std::string> m_subscribedVars;
   std::vector<int>            m_completed;
   
 };


 template<typename T> inline bool PixISManager::waitOnSubscribed(T value, 
								 double timeout) {

   bool existing = true;
   int length = m_callback->m_subscribedVars.size();
   for(unsigned int i = 0; i<length; i++){
     if (!exists(m_callback->m_subscribedVars[i]))
       existing = false;
   }

   if (!existing) {
     if (!existing) throw PixISManagerExc(PixISManagerExc::ERROR, "INVALID OPERATION::SUBCRIBE TO NON-EXISTING VARIABLE");
     return true;
   }
   
   //--------------- if wait is needed setting timeout thread --------------------------//
   
   bool terminated = false;
   std::pair<bool*,bool*> input;
   input.first = &m_callback->m_stop;
   input.second = &terminated;
   IPCAlarm* alarm = 0;
   if(timeout > 0) alarm = new IPCAlarm(timeout, &timeout_callback, (void *) &input);
   
   m_callback->setWaitValue(value);

   while (m_callback->m_stop == false) sleep(1);
   m_callback->init();

   if(alarm) delete alarm;
   if (m_callback->m_deleted) throw PixISManagerExc(PixISManagerExc::ERROR, "INVALID OPERATION::SUBCRIBED VARIABLE DELETED");
   
   return (!terminated);
 }
 
 /***************************************************************/
 /***************************************************************/


 template<typename T> inline T PixISManager::read(const char* name){
   std::cout << "Read type not supported by IS" << std::endl;
 }
 
 template<> inline bool PixISManager::read<bool>(const char* name)
   {
     ISInfoBool value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
      m_infoDictionary->getValue(nameStr.c_str(),value);
      return value.getValue();
    }

 template<> inline short PixISManager::read<short>(const char* name)
   {
     ISInfoShort value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
      m_infoDictionary->getValue(nameStr.c_str(),value);
      return value.getValue();
    }
 
 template<> inline unsigned short PixISManager::read<unsigned short>(const char* name)
   {
     ISInfoUnsignedShort value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 
 template<> inline int PixISManager::read<int>(const char* name)
   {
     ISInfoInt value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }

 template<> inline int PixISManager::readDcs<int>(const char* name)
   {
     DcsSingleData<int> isInt(0, *m_ipcPartition, m_isServerName+"."+name);
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),isInt);
     return isInt.getValue();
   }
 
 template<> inline unsigned int PixISManager::read<unsigned int>(const char* name)
   {
     ISInfoUnsignedInt value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 
 template<> inline long PixISManager::read<long>(const char* name)
   {
     ISInfoLong value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 
 template<> inline unsigned long PixISManager::read<unsigned long>(const char* name)
   {
     ISInfoUnsignedLong value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 
 template<> inline float PixISManager::read<float>(const char* name)
   {
     ISInfoFloat value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return (float) value.getValue();
   }
 
 template<> inline std::string PixISManager::readDcs<std::string>(const char* name) 
   {
     DcsSingleData<std::string> isString(0,*m_ipcPartition,m_isServerName+"."+name);
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),isString);
     return isString.getValue();
   }
     

 template<> inline float PixISManager::readDcs<float>(const char* name)
   {
     DcsSingleData<float> isFloat(0, *m_ipcPartition, m_isServerName+"."+name);
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),isFloat);
     return isFloat.getValue();
   }
 
 template<> inline double PixISManager::read<double>(const char* name)
   {
     ISInfoDouble value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 
 template<> inline std::string PixISManager::read<std::string>(const char* name)
   {
     ISInfoString value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 template<> inline const char* PixISManager::read<const char*>(const char* name)
   {
     ISInfoString value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return (value.getValue()).c_str();
   }
 template<> inline std::vector<unsigned int> PixISManager::read<std::vector<unsigned int> >(const char* name)
   {
     ISInfoT< std::vector<unsigned int> > value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value.getValue();
   }
 template<> inline std::vector<unsigned int> PixISManager::read<std::vector<unsigned int> >(const char* name, int &time)
   {
     ISInfoT< std::vector<unsigned int> > value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     time = value.time().c_time();
     return value.getValue();
   }
   
 template<> inline int PixISManager::read<int>(const char* name, int &time)
  {
     ISInfoInt value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     time = value.time().c_time();
     return value.getValue();
  }

 template<> inline RunParams PixISManager::read<RunParams>(const char* name)
   {
     RunParams value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value;
   }
 template<> inline LuminosityInfo PixISManager::read<LuminosityInfo>(const char* name)
   {
     LuminosityInfo value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value;
   }
 template<> inline LumiBlock PixISManager::read<LumiBlock>(const char* name)
   {
     LumiBlock value;
     std::string nameStr = m_isServerName + std::string(".") + std::string(name);
     m_infoDictionary->getValue(nameStr.c_str(),value);
     return value;
   }
}

#endif // _PIXLIB_ISMANAGER

