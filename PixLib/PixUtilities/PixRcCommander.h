

#include "RunControl/Common/CommandSender.h"
#include "RunControl/Common/Exceptions.h"
#include "RunControl/Common/RunControlBasicCommand.h"
#include "RunControl/Common/RunControlCommands.h"
#include "RunControl/FSM/FSMCommands.h"
#include "ers/ers.h"

#ifndef PIX_RC_COMMANDER
#define PIX_RC_COMMANDER

namespace PixLib {

class PixRcCommander{

 public:
 static void sendRcUserCommand(std::string partitionName, std::string applName,std::string cmd, std::vector<std::string> cmdArgs){
   try {
        daq::rc::CommandSender snd(partitionName, "CmdSender");
        std::unique_ptr<daq::rc::RunControlBasicCommand> rcCmd;
        const daq::rc::UserCmd usrCmd(cmd, cmdArgs);
        rcCmd.reset(new daq::rc::UserBroadcastCmd(usrCmd));
        snd.sendCommand(applName, *(rcCmd->clone()));
      } catch(ers::Issue& ex) {
        ers::error(ex);
      }

 }

};


} //namespace


#endif //PIX_RC_COMMANDER

