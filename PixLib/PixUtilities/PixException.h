/////////////////////////////////////////////////////////////////////
// PixException.h
//
/////////////////////////////////////////////////////////////////////
//
// 25/02/11  Initial release (PM)
//


#ifndef _PIXLIB_EXCEPTION
#define _PIXLIB_EXCEPTION

#include <exception>
#include <string>

class PixException : public std::exception {
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    PixException(std::string text) : m_text(text), m_errorLevel(ERROR), m_type("GENERIC") {};
    PixException(ErrorLevel level, std::string text) : m_text(text), m_errorLevel(level), m_type("GENERIC") {};
    PixException(ErrorLevel level, std::string type, std::string text) : m_text(text), m_errorLevel(level), m_type(type) {};
    virtual ~PixException() throw() {};
    virtual const char* what() const throw () { return m_text.c_str(); };
    virtual const std::string dump() const throw () { return dumpLevel() + " - " + m_type + " - " + m_text; };
    virtual ErrorLevel getErrorLevel() { return m_errorLevel; };
    virtual std::string getType() { return m_type; }; 
  private:
    const std::string dumpLevel() const {
      switch (m_errorLevel) {
      case INFO : 
	return "INFO";
      case WARNING :
	return "WARNING";
      case ERROR :
	return "ERROR";
      case FATAL :
	return "FATAL";
      default :
	return "UNKNOWN";
      }
    };
    std::string m_text;
    ErrorLevel m_errorLevel;
    std::string m_type;
};

#endif // _PIXLIB_EXCEPTION
