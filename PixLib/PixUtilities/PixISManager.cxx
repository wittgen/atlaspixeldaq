/////////////////////////////////////////////////////////////////////
// PixISManager.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 02/04/07  Version 1.0 (DLM,II)
//           Full version
// 

//#include <is/internal/typetraits.h>
#include "PixUtilities/PixISManager.h"

using namespace PixLib;


    // Constructor
PixISManager::PixISManager(IPCPartition *ipcPartition, std::string isServerName):
  m_ipcPartition(ipcPartition), m_isServerName(isServerName), m_ipcPartitionName(ipcPartition->name())
{
  //Create the ISInfoDictionary with the given partition
  m_infoDictionary = new ISInfoDictionary(*m_ipcPartition);
  m_rec = new ISInfoReceiver(*m_ipcPartition);
  m_callback = new ManagerCallback(m_rec);
}

PixISManager::PixISManager(std::string ipcPartitionName, std::string isServerName):
  m_isServerName(isServerName),m_ipcPartitionName(ipcPartitionName)
{
  
  //IPCCore should be started elsewhere
  m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  //Create the ISInfoDictionary with the given partition
  m_infoDictionary = new ISInfoDictionary(*m_ipcPartition);
  m_rec = new ISInfoReceiver(*m_ipcPartition);
  m_callback = new ManagerCallback(m_rec);
  
}
  
    // Destructor
PixISManager::~PixISManager()
{
  //Destroy the ISInfoDictionary
  delete m_infoDictionary;
  delete m_callback;
  delete m_rec;
}

void PixISManager::publish(const char* name, short value)
{

  ISInfoShort val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str()))
    m_infoDictionary->update(nameStr.c_str(),val);
  else
    m_infoDictionary->insert(nameStr.c_str(),val);
}   
      
    

void PixISManager::publish(const char* name, unsigned short value)
{

  ISInfoUnsignedShort val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

void PixISManager::publish(const char* name, long value)
{

  ISInfoLong val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

void PixISManager::publish(const char* name, unsigned long value)
{

  ISInfoUnsignedLong val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

void PixISManager::publish(const char* name, int value)
{

  ISInfoInt val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

void PixISManager::publish(const char* name, unsigned int value)
{

  ISInfoUnsignedInt val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }

}

void PixISManager::publish(const char* name, float value)
{

  ISInfoFloat val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }

}

void PixISManager::publish(const char* name, double value)
{

  ISInfoDouble val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }

}

void PixISManager::publish(const char* name, std::string value)
{
  ISInfoString val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

void PixISManager::publish(const char* name, std::vector<unsigned int>value)
{
  ISInfoT< std::vector<unsigned int> > val(value);
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  //Try first to update, if non-existing, publish for the first time
  if(m_infoDictionary->contains(nameStr.c_str())){
    m_infoDictionary->update(nameStr.c_str(),val);
  }else{
    m_infoDictionary->insert(nameStr.c_str(),val);
  }
}

bool PixISManager::exists(const char* name)
{
  //Try first to check, delete only if existing
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  return m_infoDictionary->contains(nameStr.c_str());
}

void PixISManager::removeVariable(const char* name)
{
  //Try first to check, delete only if existing
  std::string nameStr = m_isServerName + std::string(".") + std::string(name);
  if (m_infoDictionary->contains(nameStr.c_str())) m_infoDictionary->remove(nameStr.c_str());
}

void PixISManager::removeVariables(const char* suffix)
{
  std::string nameStr;
  std::vector<std::string> listedVar = listVariables(suffix);
  for (unsigned int i=0; i<listedVar.size(); i++) {
    nameStr = m_isServerName + std::string(".") + listedVar[i];
    if (m_infoDictionary->contains(nameStr.c_str())) m_infoDictionary->remove(nameStr.c_str());
  }
}

std::vector<std::string> PixISManager::listVariables(const char* name)
{
  IPCPartition p = IPCPartition(m_ipcPartitionName);

  // In the old format <name> could appear only at the end of variable name
  // Now it can appear at any place
  // 4 use cases *name -1 , name* -2  or *name* -3, name == *name -1 to keep old interface working
  // notice that * is not treated strictly, which implies that there is no need for a character to be before or after <name>

  // ---------------- ANALYSE USE CASE and REMOVE STARS--------------------------
  std::string sname = std::string(name);
  std::string cleanname;
  int index=0;
  size_t firstStarPos = sname.find("*");
  //std::cout << "firstStarPos " << firstStarPos << std::endl;
  size_t lastStarPos = sname.rfind("*");
  //std::cout << "lastStarPos " << lastStarPos << std::endl;
  if (firstStarPos != std::string::npos) { // there is a star at all
    if (firstStarPos != lastStarPos) { // more than one star
      //check that one is at the beginning and other in the end 
      if ((firstStarPos == 0)&&(lastStarPos+1 == sname.length())&&(sname.length()>2)) { index = 3; cleanname = sname.substr(1,sname.length()-2);}   
    }
    else if (firstStarPos == 0) { index = 1; cleanname = sname.substr(1,sname.length()-1); }// use old interface 
    else if (lastStarPos+1 == sname.length()) { index = 2; cleanname = sname.substr(0,sname.length()-1); }
  } 
  else { index = 1; cleanname = sname; }
 
  //std::cout << "Cleanname " << cleanname << std::endl;
  //std::cout << "Index " << index << std::endl;
  // ---------------- PERFORM SEARCH  --------------------------
	
  std::vector<std::string> result;
  if (index == 1)
    {
      std::string varName = ".*" + cleanname;
      ISInfoIterator ii(p, m_isServerName, ISCriteria(varName));
      while( ii() ){
	std::string iiName = std::string(ii.name());
	std::string bareName = iiName.substr(iiName.find(".")+1); //getting their bare names
	result.push_back(std::string(bareName));
      }
    }
  else if (index == 2) {
    ISInfoIterator ii(p, m_isServerName,ISCriteria(std::string(".*")));     //Loop over all variables on that server
    while( ii() ){
      std::string iiName = std::string(ii.name());
      std::string bareName = iiName.substr(iiName.find(".")+1); //getting their bare names
      if ((bareName.find(cleanname)!= std::string::npos) && (bareName.find(cleanname) == 0)) result.push_back(bareName);
    }
  }
  else if (index == 3) { 
    ISInfoIterator ii(p, m_isServerName,ISCriteria(std::string(".*"))); //Loop over all variables on that server
    while( ii() ){
      std::string iiName = std::string(ii.name());
      std::string bareName = iiName.substr(iiName.find(".")+1); //getting their bare names
      if (bareName.find(cleanname)!= std::string::npos) result.push_back(bareName);
    }
  }
  return result;
}

void PixISManager::subscribe(std::string name)
{
  std::string translatedName = m_isServerName + std::string(".")+name;
  m_callback->m_subscribedVars.push_back(translatedName);
  m_callback->m_completed.push_back(0);
  m_rec->subscribe(translatedName.c_str(), &ManagerCallback::arrayCallback, m_callback);
}

void PixISManager::subscribe(std::vector<std::string> names)
{
  std::vector<std::string>::iterator n, nEnd = names.end();
  for(n=names.begin(); n != nEnd; n++){
    subscribe(*n);
  }
}

void PixISManager::unsubscribe(std::string name)
{
  std::string translatedName = m_isServerName + std::string(".")+name;
  std::vector<std::string>::iterator n, nEnd = m_callback->m_subscribedVars.end();
  std::vector<int>::iterator c ;//  , cEnd = m_callback->m_completed.end();
  for(n=m_callback->m_subscribedVars.begin(); n != nEnd; n++ , c++){
    if((*n) == translatedName){
      m_callback->m_subscribedVars.erase(n);
      m_callback->m_completed.erase(c);
    }
    m_rec->unsubscribe(translatedName.c_str());
  }  
}

void PixISManager::unsubscribe(std::vector<std::string> names)
{
  std::vector<std::string>::iterator n, nEnd = names.end();
  for(n=names.begin(); n != nEnd; n++){
    unsubscribe(*n);
  }
}

void PixISManager::unsubscribeAll()
{
  m_callback->m_subscribedVars.clear();
  m_callback->m_completed.clear();
}

void PixISManager::setWaitAction(PixISManager::WaitAction action)
{
  m_callback->m_action = action;
}

void ManagerCallback::init()
{
  m_deleted = false;
  m_stop = false;
  m_callbackCalls = 0;
  if(m_value) delete m_value;
  m_value = 0;
  for(unsigned int i = 0; i<m_subscribedVars.size(); i++) m_completed[i] = 0;
  m_action = PixISManager::Change;
}

ManagerCallback::ManagerCallback(ISInfoReceiver* rec) 
{
  m_recCallback = rec;
  m_value = 0;
  init();
}

void ManagerCallback::arrayCallback(ISCallbackInfo* isc)
{

  m_callbackCalls++;
  bool stop = false;

  //One of the vectors was initialized, check which one.
  const char* name = isc->name();
  unsigned int index = 0;
  //Not safe against user evilness
 
  if (isc->reason() == ISInfo::Deleted) { m_deleted=true; m_stop = true; return; }
 
  if(m_subscribedVars.size() != 0){
   
    for(index = 0; index < m_subscribedVars.size(); index++){
      if(m_subscribedVars[index] == name) break;
    }
    if(m_action == PixISManager::Change) m_completed[index] = 1;
    
    //need to check the type
    if(m_type == 1){
      ISInfoShort* isi = new ISInfoShort;
      isc->value(*isi);
      ISInfoShort* value = dynamic_cast<ISInfoShort*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 2){
      ISInfoUnsignedShort* isi = new ISInfoUnsignedShort;
      isc->value(*isi);
      ISInfoUnsignedShort* value = dynamic_cast<ISInfoUnsignedShort*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 3){
      ISInfoLong* isi = new ISInfoLong;
      isc->value(*isi);
      ISInfoLong* value = dynamic_cast<ISInfoLong*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 4){
      ISInfoUnsignedLong* isi = new ISInfoUnsignedLong;
      isc->value(*isi);
      ISInfoUnsignedLong* value = dynamic_cast<ISInfoUnsignedLong*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 5){
      ISInfoInt* isi = new ISInfoInt;
      isc->value(*isi);
      ISInfoInt* value = dynamic_cast<ISInfoInt*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 6){
      ISInfoUnsignedInt* isi = new ISInfoUnsignedInt;
      isc->value(*isi);
      ISInfoUnsignedInt* value = dynamic_cast<ISInfoUnsignedInt*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 7){
      ISInfoFloat* isi = new ISInfoFloat;
      isc->value(*isi);
      ISInfoFloat* value = dynamic_cast<ISInfoFloat*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 8){
      ISInfoDouble* isi = new ISInfoDouble;
      isc->value(*isi);
      ISInfoDouble* value = dynamic_cast<ISInfoDouble*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }else if(m_type == 9){
      ISInfoString* isi = new ISInfoString;
      isc->value(*isi);
      ISInfoString* value = dynamic_cast<ISInfoString*>(m_value);
      if(m_action == PixISManager::EqualTo){
	if(value->getValue() == isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::BiggerThan){
	if(value->getValue() > isi->getValue())
	  m_completed[index] = 1;
      }else if(m_action == PixISManager::LessThan){
	if(value->getValue() < isi->getValue())
	  m_completed[index] = 1;
      }
    }

    for(index = 0; index < m_completed.size(); index++){
      if(m_completed[index] == 0) break;
    }
    if(index == m_completed.size()) stop = true;    
  }
  if(stop) m_stop = true;
  
}


void ManagerCallback::setWaitValue(unsigned short value)
{
  m_type = 2;
  m_value = new ISInfoUnsignedShort(value);
}
void ManagerCallback::setWaitValue(short value)
{
  m_type = 1;
  m_value = new ISInfoShort(value);
}
void ManagerCallback::setWaitValue(unsigned long value)
{
  m_type = 4;
  m_value = new ISInfoUnsignedLong(value);

}
void ManagerCallback::setWaitValue(long value)
{
  m_type = 3;
  m_value = new ISInfoLong(value);

}
void ManagerCallback::setWaitValue(unsigned int value)
{
  m_type = 6;
  m_value = new ISInfoUnsignedInt(value);
}
void ManagerCallback::setWaitValue(int value)
{
  m_type = 5;
  m_value = new ISInfoInt(value);
}
void ManagerCallback::setWaitValue(float value)
{
  m_type = 7;
  m_value = new ISInfoFloat(value);
}
void ManagerCallback::setWaitValue(double value)
{
  m_type = 8;
  m_value = new ISInfoDouble(value);
}
void ManagerCallback::setWaitValue(const char* value)
{
  setWaitValue(std::string(value));
}
void ManagerCallback::setWaitValue(std::string value)
{
  m_type = 9;
  m_value = new ISInfoString(value);

}
