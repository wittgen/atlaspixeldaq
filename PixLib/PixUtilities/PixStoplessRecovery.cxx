

#include "PixUtilities/PixStoplessRecovery.h"
#include "csignal"

 using namespace PixLib;

 static std::atomic_bool abortSR=false;

 static const std::string isServerName = "RunParams";
 
 static const int waitTimeout = 60;//Timeout in seconds

 void signal_handler(int signum){
   PIX_WARNING("PixStoplessRecovery signal handler "<<signum);
   abortSR=true;
 }

 bool PixStoplessRecovery::autoDisable(const std::string &partitionName, const std::string &mode, const std::string &rodName, const std::string &item){

  std::unique_ptr<PixISManager> isMan =getIsManager(partitionName);
    if(!isMan ){
      PIX_ERROR(rodName<<" Cannot get ISManager from,"<< partitionName <<" cannot continue");
      return false;
    }

  std::string isName;

    if (!getIsString(partitionName, rodName, isName)){
      PIX_ERROR(rodName<<" Cannot get ISString name from ,"<< partitionName <<" cannot continue");
      return false;
    }

   std::string autoDisPartName="";

     if(!allocated(isMan.get(),isName,autoDisPartName)){
       PIX_ERROR(rodName<<" cannot verify that the ROD is allocated ");
       return false;
     }

  std::unique_ptr<PixISManager> isManagerDT=getIsManager(autoDisPartName);
    if(!isManagerDT){
      PIX_ERROR(rodName<<" Cannot get ISManager from,"<< autoDisPartName <<" cannot continue");
      return false;
    }

  std::string oldDis;
  std::string autoDisStr = isName +"AUTO_DISABLE_ENA";

   try {
     if(isManagerDT->exists(autoDisStr.c_str()) ){
       oldDis = isManagerDT->read<std::string>(autoDisStr.c_str());
     } else{
       PIX_ERROR(rodName<<" Cannot get AUTO_DISABLE_ENA from isManagerDT");
       return false;
     }
   } catch (...) {
     PIX_ERROR(rodName<<" Transition CONFIGURE not yet completed, please try later");
     return false;	
   }

   if (mode == "ENABLE" || mode == "DISABLE" || mode == "CLEAN" || mode == "DUMP" )
     if( !( oldDis == "ON" || oldDis == "OFF") ){
	PIX_ERROR(rodName<<" Another AutoDisable command for **"<< oldDis <<"** has been issued, the program will not continue!!!");
	return false;
     }

   if (mode == "ENABLE" || mode == "DISABLE" || mode == "CLEAN" || mode == "DUMP"){

     if( item == "" ){
       PIX_ERROR(rodName<<" No item to Enable/Disable has been providing");
       return false;
     }

     abortSR=false;
     // Install the signal handler
     //std::atexit(exit_handler);
     std::signal(SIGINT   , signal_handler);
     std::signal(SIGQUIT  , signal_handler);
     std::signal(SIGILL   , signal_handler);
     std::signal(SIGTRAP  , signal_handler);
     std::signal(SIGABRT  , signal_handler);
     std::signal(SIGIOT   , signal_handler);
     std::signal(SIGBUS   , signal_handler);
     std::signal(SIGFPE   , signal_handler);
     std::signal(SIGKILL  , signal_handler);
     std::signal(SIGSEGV  , signal_handler);
     std::signal(SIGPIPE  , signal_handler);
     std::signal(SIGTERM  , signal_handler);
     std::signal(SIGSTKFLT, signal_handler);
     std::signal(SIGSYS   , signal_handler);

      if(abortSR){ isManagerDT->publish(autoDisStr.c_str(), oldDis); return false;}

      std::string autoDisDone= isName +"AUTO_DIS_DONE";

      isMan->removeVariable(autoDisDone.c_str());

      PIX_INFO(rodName<<" Set AUTO_DISABLE_ENA to " << mode << " " << item);
      if(mode == "CLEAN" || mode == "DUMP")  isManagerDT->publish(autoDisStr.c_str(), mode);
      else isManagerDT->publish(autoDisStr.c_str(), mode+" "+item);

      std::this_thread::sleep_for( std::chrono::seconds(5));

      std::string status="IDLE";
      bool ok=false;
      int wait=0;
        do {
          if(isMan->exists(autoDisDone.c_str())){
            status = isMan->read<std::string>(autoDisDone.c_str());
          }

          if (status == "DONE") ok = true;

           if (!ok) {
	     std::this_thread::sleep_for( std::chrono::seconds(1));
	     wait++;
	   }
            if(abortSR){
              isManagerDT->publish(autoDisStr.c_str(), oldDis);
              return false;
            }
        } while(!ok && wait<waitTimeout);

        if (wait >= waitTimeout) {
          PIX_ERROR(rodName<<" No reply from action servers, set AUTO_DISABLE_ENA to " <<oldDis );
          isManagerDT->publish(autoDisStr.c_str(), oldDis);
          return false;
        } else {
          PIX_INFO(rodName<<" Operation completed, set AUTO_DISABLE_ENA to " << oldDis);
        }
        isManagerDT->publish(autoDisStr.c_str(), oldDis);
        return true;
    } else if (mode == "ON" || mode == "OFF") {
          PIX_INFO(rodName<<" Set AUTO_DISABLE_ENA to " << mode );
          isManagerDT->publish(autoDisStr.c_str(), mode); 
          return true;
    }

   return false;
 }

 bool PixStoplessRecovery::autoRecover(const std::string &partitionName, const std::string &rodName ,std::string applName){

   std::unique_ptr<PixISManager> isManager = getIsManager(partitionName);
    if(!isManager ){
      PIX_ERROR(rodName<<" Cannot get ISManager from,"<< partitionName <<" cannot continue");
      return false;
   }

   std::string isName;

    if (!getIsString(partitionName, rodName, isName)){
      PIX_ERROR(rodName<<" Cannot get ISString name from ,"<< partitionName <<" cannot continue");
      return false;
    }

   std::string autoRecoverStr = isName+"AUTO_RECOVER_COMMAND";

   if(isManager->exists(autoRecoverStr.c_str())){
     std::string autoRecoverCmd = isManager->read<std::string>(autoRecoverStr.c_str() );
	if(autoRecoverCmd != ""){
	  PIX_ERROR(rodName<<" Another AutoRecover command for "<< autoRecoverCmd <<" has been issued, the program will not continue!!!");
	}
   }

   std::string autoDisPartName;
   if(!allocated(isManager.get(),isName,autoDisPartName)){
     PIX_ERROR(rodName<<" cannot verify that the ROD is allocated in "<<autoDisPartName);
     return false;
   }

   abortSR=false;
   // Install the signal handler
   std::signal(SIGINT   , signal_handler);
   std::signal(SIGQUIT  , signal_handler);
   std::signal(SIGILL   , signal_handler);
   std::signal(SIGTRAP  , signal_handler);
   std::signal(SIGABRT  , signal_handler);
   std::signal(SIGIOT   , signal_handler);
   std::signal(SIGBUS   , signal_handler);
   std::signal(SIGFPE   , signal_handler);
   std::signal(SIGKILL  , signal_handler);
   std::signal(SIGSEGV  , signal_handler);
   std::signal(SIGPIPE  , signal_handler);
   std::signal(SIGTERM  , signal_handler);
   std::signal(SIGSTKFLT, signal_handler);
   std::signal(SIGSYS   , signal_handler);

   isManager->publish(autoRecoverStr.c_str(),rodName.c_str());

   //DISABLE ROD
   if(!autoDisable(partitionName ,"DISABLE", rodName, rodName)){
     PIX_ERROR(rodName <<" cannot be disabled!");
     isManager->publish(autoRecoverStr.c_str(),"");
     return false;
   }

   if(abortSR){isManager->publish(autoRecoverStr.c_str(),""); return false;}

   if(!waitAutoDis(isManager.get(),isName,"DISABLE")){
     PIX_ERROR(rodName <<" cannot verify that has been disabled!");
     isManager->publish(autoRecoverStr.c_str(),"");
     return false;
   }

   std::this_thread::sleep_for( std::chrono::seconds(5));

   if(abortSR){isManager->publish(autoRecoverStr.c_str(),""); return false;}

   PIX_INFO(rodName<<" DISABLED!!!");

   std::string recoStat=isName+"RECOVERY";

    isManager->removeVariable(recoStat.c_str());

    //RECONFIGURE ROD
    std::vector<std::string> cmdArgs{rodName};
    PixLib::PixRcCommander::sendRcUserCommand(autoDisPartName,applName,"RECOVER", cmdArgs);

    if(abortSR){isManager->publish(autoRecoverStr.c_str(),""); return false;}

      std::string status="IDLE";
      bool ok=false;
      int wait=0;
        do {
          if(isManager->exists(recoStat.c_str())){
            status = isManager->read<std::string>(recoStat.c_str());
          }

          if (status == "DONE") ok = true;
          
          //std::cout<<"Status "<<status<<std::endl;

           if (!ok) {
	     std::this_thread::sleep_for( std::chrono::seconds(1));
	     wait++;
	   }
            if(abortSR){
              isManager->publish(autoRecoverStr.c_str(),"");
              return false;
            }
        } while(!ok && wait<waitTimeout);

      if (wait >= waitTimeout) {
       PIX_ERROR(rodName<<" No reply from action server ROD cannot be recovered!!!");
       isManager->publish(autoRecoverStr.c_str(),"");
       return false;
      }

    //Removing IS recover flag
    isManager->removeVariable(recoStat.c_str());

    if(abortSR){isManager->publish(autoRecoverStr.c_str(),""); return false;}

    PIX_INFO("ROD "<<rodName <<" RECONFIGURED!!!");

    std::this_thread::sleep_for( std::chrono::seconds(5));

      if(!autoDisable(partitionName ,"ENABLE", rodName,rodName)){
       PIX_ERROR(rodName <<" cannot be enabled!");
       isManager->publish(autoRecoverStr.c_str(),"");
       return false;
     }

     if(abortSR){isManager->publish(autoRecoverStr.c_str(),""); return false;}

     if(!waitAutoDis(isManager.get(),isName,"ENABLE")){
       PIX_ERROR(rodName <<" cannot verify that the ROD has been enabled!");
       isManager->publish(autoRecoverStr.c_str(),"");
       return false;
     }

   isManager->publish(autoRecoverStr.c_str(),"");

   PIX_INFO(rodName<<" ENABLED!!!");

  return true;

  }

  bool PixStoplessRecovery::allocated(PixISManager* const isManagerPtr, const std::string &isName, std::string &autoDisPartName){
    std::string str = isName+"ALLOCATING-PARTITION";
    if( isManagerPtr->exists(str.c_str()))
      autoDisPartName = isManagerPtr->read<std::string>(str.c_str() );

    PIX_INFO(isName<<" "<<autoDisPartName);
    
      if(autoDisPartName.find("PixelInfr") != std::string::npos || autoDisPartName=="NONE" || autoDisPartName.empty())
        return false;

    return true;

  }

 bool PixStoplessRecovery::waitAutoDis(PixISManager* const isManager, const std::string &isName, const std::string &action){
    std::string str= isName+"DAQEnabled";

    int status =-1;
    bool ok=false;
    int wait=0;	
      do {
        if(isManager->exists(str.c_str())){
          status = isManager->read<int>(str.c_str());
        }

         if(action == "ENABLE"  && status ==0) ok = true;
         if(action == "DISABLE" && status > 0) ok = true;

         if(wait%10 ==0 )PIX_INFO("Waiting for "<<str<<" "<<action<<" "<<status);

           if (!ok) {
	     std::this_thread::sleep_for( std::chrono::seconds(1));
	     wait++;
	   }
            if(abortSR){
              return false;
            }
        } while(!ok && wait<waitTimeout);
      if(wait>=waitTimeout){
        PIX_ERROR("Cannot verify that the ROD has been "<<action<<" the program will not continue!!!");
        return false;
      }

    return true;

  }

 std::unique_ptr<PixISManager> PixStoplessRecovery::getIsManager(const std::string &partitionName){


    std::unique_ptr <IPCPartition> ipcPartitionPtr; 
    std::unique_ptr<PixISManager> isManagerPtr;
    // Accessing the TDAQ partition
    try {
      ipcPartitionPtr = std::make_unique<IPCPartition>(partitionName);
    } catch(...) {
      PIX_ERROR("Couldn't connect to the partition " + partitionName);
      return isManagerPtr;
    }

    try {
      isManagerPtr = std::make_unique<PixISManager>(ipcPartitionPtr.get(), isServerName);
    } catch(...) {
      PIX_ERROR("Couldn't create ISManager "<<isServerName);
    }

  return isManagerPtr;

  }

  bool PixStoplessRecovery::getParStoplessRecovery(const std::string &partition, const std::string &item ,std::map  <std::string, std::string> &disableMap ){
  
  std::unique_ptr<PixConnectivity> pixConnectivityPtr = getConnectivity(partition);

  if(!pixConnectivityPtr) return false;

  std::istringstream ss(item);
  std::string toDis;
  std::string rodName;


    while(std::getline(ss, toDis, ',')) {//check separate with commas, this happens when several modules, RODs are disabled
     bool found=false;
      for(auto & rod : pixConnectivityPtr->rods) {
        if (!rod.second)continue;
        if( !((rod.second)->enableReadout && (rod.second)->active()) )continue;
        rodName = (rod.second)->name();
         if(toDis == rodName ){//ITEM is a ROD
           disableMap[rodName]+=rodName+",";
           found=true;
           break;
         } else if(toDis == rodName+"_0" || toDis == rodName+"_1" || toDis == rodName+"_2" || toDis == rodName+"_3"){//SLINK
           disableMap[rodName]+=toDis+",";
           found=true;
           break;
         } else {
           for (int ob=0; ob<4; ob++) {
	     if (! ((rod.second)->obs(ob)) )continue;
	     if (! ((rod.second)->obs(ob)->pp0()))continue;
	     std::string pp0Name = (rod.second)->obs(ob)->pp0()->name();
	     bool isPP0 = (pp0Name == toDis);
	     for (int m=0;m<=8;m++) {
	       PixLib::ModuleConnectivity *module = (rod.second)->obs(ob)->pp0()->modules(m);
	       if(!module )continue;
	       std::string moduleName =  module->name();
	       if( !(module->enableReadout && module->active()) )continue;
	         if (isPP0) {
	           disableMap[rodName]+=moduleName+",";
	           found=true;
	         } else if (toDis == moduleName) {
                   disableMap[rodName]+=moduleName+",";
                   found=true;
                   break;
	         }
	     }
	     if(found)break;
           }
         }
        if(found)break;
      }
      if(!found)PIX_WARNING(toDis<<" not found in connectivity DB");
    }

    if(disableMap.empty()){
      PIX_ERROR("Disable map is empty");
      return false; 
    }

    for(auto & it : disableMap){//Remove last comma
      if(!it.second.empty())it.second.pop_back();
    }

  return true;

  }

  bool PixStoplessRecovery::getIsString(const std::string &partition, const std::string &rodName, std::string &parName){

  std::unique_ptr<PixConnectivity> pixConnectivityPtr = getConnectivity(partition);
  if(!pixConnectivityPtr) return false;

  bool found = false;

    for(const auto & rod : pixConnectivityPtr->rods) {
        if (!rod.second)continue;
         if( (rod.second)->name() == rodName ){
           parName = (rod.second)->crate()->name() +"/"+rodName+"/";
           found=true;
           break;
         }
    }

  return found;

  }

  std::unique_ptr<PixConnectivity> PixStoplessRecovery::getConnectivity(const std::string &partition){

  std::unique_ptr <IPCPartition> ipcPartitionPtr;
  std::unique_ptr<PixConnectivity> pixConnectivityPtr;
  // Accessing the TDAQ partition
  try {
    ipcPartitionPtr = std::make_unique<IPCPartition>(partition);
  } catch(...) {
    PIX_ERROR("Couldn't connect to the partition " + partition);
    return pixConnectivityPtr;
  }

  // Interfacing to IS
  std::unique_ptr<PixISManager> isManagerPtr;

  try {
    isManagerPtr = std::make_unique<PixISManager>(ipcPartitionPtr.get(), "PixelRunParams");
  } catch(...) {
    PIX_ERROR("Couldn't create ISManager PixelRunParams");
    return pixConnectivityPtr;
  }

  // Now we are reading the connectiviyt, configuration and ID tags first from the environment, and then from IS
  std::string connTag, cfgTag, idTag,modCfgTag;

  if(getenv("CONNECTIVITY_TAG_C")) connTag = getenv("CONNECTIVITY_TAG_C");
  try {
    if(isManagerPtr->exists("DataTakingConfig-ConnTag"))
    connTag = isManagerPtr->read<std::string>("DataTakingConfig-ConnTag");
  } catch(...) {
    PIX_WARNING("Cannot read DataTakingConfig-ConnTag from IS");
    return pixConnectivityPtr;
  }

  if(getenv("CONNECTIVITY_TAG_CF")) cfgTag = getenv("CONNECTIVITY_TAG_CF");
  try {
    if(isManagerPtr->exists("DataTakingConfig-CfgTag"))
    cfgTag = isManagerPtr->read<std::string>("DataTakingConfig-CfgTag");
  } catch(...) {
    PIX_WARNING( "Cannot read DataTakingConfig-CfgTag from IS");
    return pixConnectivityPtr;
  }

  if(getenv("ID_TAG")) idTag = getenv("ID_TAG");
  try {
    if(isManagerPtr->exists("DataTakingConfig-IdTag"))
    idTag = isManagerPtr->read<std::string>("DataTakingConfig-IdTag");
  } catch(...) {
    PIX_WARNING("Cannot read DataTakingConfig-IdTag from IS");
    return pixConnectivityPtr;
  }

  if(getenv("CONNECTIVITY_TAG_MOD_CF")) modCfgTag = getenv("CONNECTIVITY_TAG_MOD_CF");
  try {
    if(isManagerPtr->exists("DataTakingConfig-ModCfgTag"))
    modCfgTag = isManagerPtr->read<std::string>("DataTakingConfig-ModCfgTag");
  } catch(...) {
    PIX_WARNING("Cannot read DataTakingConfig-ModCfgTag from IS");
    return pixConnectivityPtr;
  }

  PIX_INFO("Continuing with the following tags: " + idTag << ", " << connTag + ", " << cfgTag << ", "<<modCfgTag);

  bool ready=false;
  int nTry=0;
  std::string dbsrv_name = "PixelDbServer";//harcoded
  std::string msg = "Trying to connect to DbServer with name "+dbsrv_name ;
  std::unique_ptr<PixDbServerInterface> DbServerPtr;
    do {
      DbServerPtr = std::make_unique<PixDbServerInterface>(ipcPartitionPtr.get(), dbsrv_name, PixDbServerInterface::CLIENT, ready);
      if(ready)break;
      std::stringstream info;
      info << " ..... attempt number: " << nTry;
      msg = info.str();
      nTry++;
      std::this_thread::sleep_for( std::chrono::seconds(1));
    } while(nTry<20);

    if(!ready) {
        PIX_ERROR (" Impossible to connect to DbServer " << dbsrv_name);
        return pixConnectivityPtr;
    } else {
        std::cout<<"Successfully connected to DbServer with name " << dbsrv_name<<std::endl;
    }

  // Creating and loading the connectivity
  pixConnectivityPtr = std::make_unique<PixConnectivity>(DbServerPtr.get(), connTag, connTag, connTag, cfgTag, idTag, modCfgTag );

  try {
    pixConnectivityPtr->loadConn();
  } catch (const PixLib::PixConnectivityExc &e){
    PIX_ERROR("Exception caught at loadConn():" << e.getDescr());
  }
  catch (...){
    PIX_ERROR("Exception caught at loadConn()");
  }

  PIX_INFO("Connectivity tag " << connTag << " loaded successfully");

  return pixConnectivityPtr;

  }


