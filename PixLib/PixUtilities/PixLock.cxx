/////////////////////////////////////////////////////////////////////
// PixLock.cxx
//
/////////////////////////////////////////////////////////////////////
//
// 26/05/15  Definition of static objects (PM)
//


#include <PixUtilities/PixException.h>
#include <PixUtilities/PixLock.h>

using namespace PixLib;

std::map<std::string, PixMutex*> PixMutex::m_objects = std::map<std::string, PixMutex*>();
std::map<std::string, PixMutexRW*> PixMutexRW::m_objects = std::map<std::string, PixMutexRW*>();
std::map<std::string, PixCondMutex*> PixCondMutex::m_objects = std::map<std::string, PixCondMutex*>();

