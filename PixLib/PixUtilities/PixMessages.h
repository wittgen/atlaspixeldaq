/////////////////////////////////////////////////////////////////////
// PixMessages.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 28/10/07  Version 1.0 (GAO)
//           First PixMRS implementation for a basic MRS sender
// 12/03/14  Version 2.0 (JGK)
//           kick out MRS and use existing ERS code only

//! Pixel ERS interface class

#ifndef _PIXLIB_PIXMESSAGES
#define _PIXLIB_PIXMESSAGES

#include <iostream>
#include <set>
//#include <is/type.h>
#include <map>


#include "BaseException.h"
#include <ers/InputStream.h>
#include <ers/ers.h>


// Forward declarations
class IPCPartition;
class DFMutex;
//class MRSStream;

namespace PixLib {

ERS_DECLARE_ISSUE( 	pix,
			pixissue,
			ERS_EMPTY,
			ERS_EMPTY
                 )


ERS_DECLARE_ISSUE_BASE( 	pix,
				daq,
                                pixissue,
				header << " - " << message,
				ERS_EMPTY,
				((std::string)header )
				((std::string)message )
		      )




//! PixMessages Exception class; an object of this type is thrown in case of an ERS communication or setting error
class PixMessagesExc : public SctPixelRod::BaseException{
public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  //! Constructor
  PixMessagesExc(ErrorLevel el, std::string name) : BaseException(name), m_errorLevel(el), m_name(name) {}; 
  //! Destructor
  virtual ~PixMessagesExc() {};

  //! Dump the error
  virtual void dump(std::ostream &out) {
    out << "Pixel ERS interface: " << m_name << " -- Level : " << dumpLevel(); 
  }
  std::string dumpLevel() {
    switch (m_errorLevel) {
    case INFO : 
      return "INFO";
    case WARNING :
      return "WARNING";
    case ERROR :
      return "ERROR";
    case FATAL :
      return "FATAL";
    default :
      return "UNKNOWN";
    }
  }
  //! m_errorLevel accessor
  ErrorLevel getErrorLevel() { return m_errorLevel; };
  //! m_name accessor
  std::string getName() { return m_name; };
private:
  ErrorLevel m_errorLevel;
  std::string m_name;
};

class PixMessages {
   
public:
   
  enum MESSAGE_TYPE  { 
     SUCCESS, INFORMATION, DIAGNOSTIC, WARNING, ERROR, FATAL
  };
    
  enum PUBLICATION_STATUS {
     MRS_OPERATION_SUCCESS = 0,
     MRS_NO_SERVER = 231,
     MRS_REPORT_ERROR = 232,
     MRS_MORE_THAN_ONE_MESSAGE_NAME = 233,
     MRS_MESSAGE_WITHOUT_IDENTIFIER = 234
   };
   
public:
   
   // Constructor
   PixMessages();
   // Destructor
   ~PixMessages();
   
   void pushPartition(std::string partitionName, IPCPartition* part);
   void pushPartition(std::map<std::string, IPCPartition*> vpart);
   void pushStream(std::string streamName, std::ostream &ostr);
   void publishMessage(PixMessages::MESSAGE_TYPE type, std::string header, std::string message);
   void publishMessage(PixMessages::MESSAGE_TYPE type, std::string partitionName, std::string header, std::string message);
   void printMessage(PixMessages::MESSAGE_TYPE type, std::string header, std::string message);
   void printMessage(PixMessages::MESSAGE_TYPE type, std::string partitionName, std::string header, std::string message);

 private:
   //   void publish(PixMessages::MESSAGE_TYPE type, MRSStream* str, std::string header, std::string message);
   void spublish(PixMessages::MESSAGE_TYPE type, std::ostream& str, std::string header, std::string message);
   void epublish(PixMessages::MESSAGE_TYPE type, std::string header, std::string message);
   std::string getState(std::string partitionName);
   
   // Software infrastructure
   //   std::map<std::string, MRSStream*> mout;
   std::map<std::string, std::ostream*> sout;
   bool ersout;
   std::string m_partName;
   DFMutex* m_mutex;
 };
 
}


#ifndef PIX_INFO
#define PIX_INFO(message)						\
  {									\
    ERS_REPORT_IMPL(ers::info,PixLib::pix::daq::pixissue,message, ); \
  }
#endif

#ifndef PIX_LOG
#define PIX_LOG(message)						\
  {									\
    ERS_REPORT_IMPL(ers::log,PixLib::pix::daq::pixissue,message, ); \
  }
#endif

#ifndef PIX_DEBUG
#define PIX_DEBUG(level,message)					\
  {									\
      ERS_REPORT_IMPL(ers::debug,PixLib::pix::daq::pixissue,message,level); \
  }
#endif

#ifndef PIX_WARNING
#define PIX_WARNING(message)					\
  {									\
    ERS_REPORT_IMPL(ers::warning,PixLib::pix::daq::pixissue,message, ); \
  }
#endif

#ifndef PIX_ERROR
#define PIX_ERROR(message)					\
  {									\
    ERS_REPORT_IMPL(ers::error,PixLib::pix::daq::pixissue,message, );	\
  }
#endif

#ifndef PIX_FATAL
#define PIX_FATAL(message)					\
  {									\
    ERS_REPORT_IMPL(ers::fatal,PixLib::pix::daq::pixissue,message, );	\
  }
#endif

#endif // _PIXLIB_PIXMESSAGES

