/////////////////////////////////////////////////////////////////////
// PixLock.h
//
/////////////////////////////////////////////////////////////////////
//
// 18/02/11  Initial release (PM)
//


#ifndef _PIXLIB_LOCK
#define _PIXLIB_LOCK

#include <PixUtilities/PixException.h>

#include <time.h>
#include <sys/time.h>
#include <pthread.h>
#include <map>
#include <asm-generic/errno.h>


namespace PixLib {

  class PixMutex {
  public:
    class NotLockedByThread : public PixException {
    public:
      NotLockedByThread() : PixException("The mutex is unlocked or is locked by another thread") {}
    };

    class AlreadyLockedByThread : public PixException {
    public:
      AlreadyLockedByThread() : PixException("The mutex is already locked by this thread") {}
    };

    void incrUseCount() {
      m_useCount++;
    }

    static PixMutex* create(std::string name) {
      if (m_objects.find(name) == m_objects.end()) {
	m_objects[name] = new PixMutex(name);
      } else {
	m_objects[name]->incrUseCount();
      }
      return m_objects[name];
    }

    void destroy() {
      m_useCount--;
      if (m_useCount == 0) {
	m_objects.erase(m_name);
	delete this;
      }
    }
    
    PixMutex(std::string name) {
      m_name = name;
      m_useCount = 1;
      pthread_mutexattr_init(&m_attr);
      pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_ERRORCHECK);
      pthread_mutex_init(&m_mutex, &m_attr);
    }

    ~PixMutex() {
      pthread_mutex_destroy(&m_mutex);
    }

    void lock() {
      int ret = pthread_mutex_lock(&m_mutex);
      if (ret == EDEADLK) throw(PixMutex::AlreadyLockedByThread());
    }

    void unlock() {
      int ret = pthread_mutex_unlock(&m_mutex);
      if (ret == EPERM) throw(PixMutex::NotLockedByThread());
    }

  private:
    pthread_mutexattr_t m_attr;
    pthread_mutex_t m_mutex;
    std::string m_name;
    static std::map<std::string, PixMutex* > m_objects; 
    unsigned int m_useCount; 
  };

  // std::map<std::string, PixMutex*> PixMutex::m_objects = std::map<std::string, PixMutex*>();

  class PixLock {
  public:
    PixLock(PixMutex *lock) {
      m_holdLock = true;
      m_lock = lock;
      try{
	m_lock->lock();
	//std::cout << "Mutex locked" << std::endl;
      }
      catch (...) {
	m_holdLock = false;
      }
    }

    ~PixLock() {
      if (m_holdLock) {
	m_lock->unlock();
	//std::cout << "Mutex unlocked" << std::endl;
      }
    }

  private:
    bool m_holdLock;
    PixMutex *m_lock;
  };

  class PixMutexRW {
  public:
    static PixMutexRW* create(std::string name) {
      if (m_objects.find(name) == m_objects.end()) {
	m_objects[name] = new PixMutexRW(name);
      } else {
	m_objects[name]->incrUseCount();
      }
      return m_objects[name];
    }

    void destroy() {
      m_useCount--;
      if (m_useCount == 0) {
	m_objects.erase(m_name);
	delete this;
      }
    }
    
    PixMutexRW(std::string name) {
      m_name = name;
      m_useCount = 1;
      pthread_rwlock_init(&m_mutex,0);
    }

    ~PixMutexRW() {
      pthread_rwlock_destroy(&m_mutex);
    }

    void read_lock() {
      int ret = pthread_rwlock_rdlock(&m_mutex);
      if (ret == EDEADLK) throw(PixMutex::AlreadyLockedByThread());
    }

    void write_lock() {
      int ret = pthread_rwlock_wrlock(&m_mutex);
      if (ret == EDEADLK) throw(PixMutex::AlreadyLockedByThread());
    }

    void unlock() {
      int ret = pthread_rwlock_unlock(&m_mutex);
      if (ret == EPERM) throw(PixMutex::NotLockedByThread());
    }

    void incrUseCount() {
      m_useCount++;
    }

  private:
    pthread_rwlock_t m_mutex;
    std::string m_name;
    static std::map<std::string, PixMutexRW* > m_objects; 
    unsigned int m_useCount; 
  };
  //  std::map<std::string, PixMutexRW*> PixMutexRW::m_objects = std::map<std::string, PixMutexRW*>();

  class PixCondMutex {
  public:

    static PixCondMutex* create(std::string name) {
      if (m_objects.find(name) == m_objects.end()) {
	m_objects[name] = new PixCondMutex(name);
      } else {
	m_objects[name]->incrUseCount();
      }
      return m_objects[name];
    }

    void destroy() {
      m_useCount--;
      if (m_useCount == 0) {
	m_objects.erase(m_name);
	delete this;
      }
    }
    
    PixCondMutex(std::string name) {
      m_name = name;
      m_useCount = 1;
      pthread_mutexattr_init(&m_attr);
      pthread_mutexattr_settype(&m_attr, PTHREAD_MUTEX_ERRORCHECK);
      pthread_mutex_init(&m_mutex, &m_attr);
      pthread_cond_init(&m_cond,0);
    }

    ~PixCondMutex() {
      pthread_mutex_destroy(&m_mutex);
      pthread_cond_destroy(&m_cond);
      pthread_mutexattr_destroy(&m_attr);
    }

    void lock() {
      int ret = pthread_mutex_lock(&m_mutex);
      if (ret == EDEADLK) throw(PixMutex::AlreadyLockedByThread());
    }

    void unlock() {
      int ret = pthread_mutex_unlock(&m_mutex);
      if (ret == EPERM) throw(PixMutex::NotLockedByThread());
    }

    void wait() {
      int ret = pthread_mutex_lock(&m_mutex);
      pthread_cond_wait(&m_cond, &m_mutex);
      if (ret != EDEADLK) pthread_mutex_unlock(&m_mutex);
    }

    bool wait(unsigned int timeout) {
      int ret = pthread_mutex_trylock(&m_mutex);
      struct timespec time;
      struct timeval tval;
      gettimeofday(&tval, NULL);
      time.tv_sec = tval.tv_sec+timeout;
      time.tv_nsec = tval.tv_usec*1000;
      int wret = pthread_cond_timedwait(&m_cond, &m_mutex, &time);
      if (ret != EBUSY && ret != EDEADLK) pthread_mutex_unlock(&m_mutex);
      if (wret == ETIMEDOUT) return false;
      return true;      
    }

    void signal() {
      int ret = pthread_mutex_lock(&m_mutex);
      pthread_cond_signal(&m_cond);
      if (ret != EDEADLK) pthread_mutex_unlock(&m_mutex);
    }

    void broadcast() {
      int ret = pthread_mutex_lock(&m_mutex);
      pthread_cond_broadcast(&m_cond);
      if (ret != EDEADLK) pthread_mutex_unlock(&m_mutex);
    }

    void incrUseCount() {
      m_useCount++;
    }

  private:
    pthread_mutexattr_t m_attr;
    pthread_mutex_t m_mutex;
    pthread_cond_t m_cond;
    std::string m_name;
    static std::map<std::string, PixCondMutex* > m_objects; 
    unsigned int m_useCount; 
  };
  //  std::map<std::string, PixCondMutex*> PixCondMutex::m_objects = std::map<std::string, PixCondMutex*>();
}

#endif // _PIXLIB_LOCK
