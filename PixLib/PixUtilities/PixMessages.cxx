/////////////////////////////////////////////////////////////////////
// PixMessages.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 28/10/07  Version 1.0 (GAO)
//           First PixMessages implementation for a basic MRS sender
// 12/03/14  Version 2.0 (JGK)
//           kick out MRS and use existing ERS code only
//

/*
This class can be used like a message redirector to cout, cerr, MRS server.
It provides six different message levels (SUCCESS, INFO, DIAGNOSTIC, WARNING, ERROR, FATAL)
which can be selected in order to publish informations on IS or even for cout or cerr messages.
A standard constructor is provided. With the created object is possible to 
send messages to cout and cerr or any other ostream and/or to MRS. 
To do so, an IPCPartition needs to be passed to the class (pushPartition).
The interface accepts one or more partitions and their corresponding names.
This permits to publish messages by selecting the partition where we want to publish (using 
'publishMessage' method with 3 params).
Moreover it's possible to publish everywhere without regarding to the names passed (using 
'publishMessage' method with 4 params).
*/


#include <stdlib.h>
#include <sstream>
#include <ipc/partition.h>
#include <DFThreads/DFMutex.h>
#include <cmdl/cmdargs.h>
//#include <mrs/message.h>
#include <PixUtilities/PixMessages.h>

using namespace std;
using namespace PixLib;


PixMessages::PixMessages() {
  // Create mutex
  m_mutex = DFMutex::Create((char *)"PixMessages_Mutex"); // TODO remove DFMUtex
  ersout = false;
  char* pn = NULL;
  pn = getenv("TDAQ_PARTITION");
  if (pn != NULL) {
    m_partName = pn;
  } else {
    m_partName = "?";
  }
}

PixMessages::~PixMessages() {
  // Delete mutex
  m_mutex->destroy();

  //Delete MRSStream
//   for(std::map<std::string, MRSStream*>::iterator it = mout.begin(); it != mout.end(); it++) {
//     delete (*it).second;
//   }
}

void PixMessages::pushPartition(std::string partitionName, IPCPartition* part) {
  m_mutex->lock();
  if (partitionName == m_partName) {
    ersout = true;
  } else {
    //    MRSStream *mrs =  new MRSStream(*part);
    //    mout.insert( std::make_pair(partitionName, mrs) );
  }
  m_mutex->unlock();
}

void PixMessages::pushPartition(std::map<std::string, IPCPartition*> vpart) {
  m_mutex->lock();
  for(std::map<std::string, IPCPartition*>::iterator it = vpart.begin(); it != vpart.end(); it++) {
    if (it->first == m_partName) {
      ersout = true;
    } else {
      //      mout.insert(std::make_pair( (*it).first ,new MRSStream(*((*it).second) )  ) );
    }
  }
  m_mutex->unlock();
}

void PixMessages::pushStream(std::string streamName, std::ostream &ostr) {
  m_mutex->lock();
  sout.insert(std::make_pair( streamName, &ostr ) );
  m_mutex->unlock();
}

void PixMessages::publishMessage(PixMessages::MESSAGE_TYPE type, std::string header, std::string message) {
//   if(mout.size() + sout.size() == 0) {
//     std::string errorMsg =  "PixMessages::publishMessage: No stream or  MRS server can be found. Cannot publish.";  
//     throw PixMessagesExc(PixMessagesExc::ERROR, errorMsg);
//   }

  m_mutex->lock();
  for(std::map<std::string, std::ostream *>::iterator it = sout.begin(); it != sout.end(); it++) {
    spublish(type, *(it->second), header, message);
  }
//   for(std::map<std::string, MRSStream*>::iterator it = mout.begin(); it != mout.end(); it++) {
//     publish(type, it->second, header, message);
//     std::string state = getState(it->first);
//     if(state != "SUCCESS") {
//       std::string errorMsg = "Error during publication of the message; STATUS = " + state;  
//       m_mutex->unlock();
//       throw( PixMessagesExc (PixMessagesExc::WARNING, errorMsg) );
//     }  
//   }
//  if (ersout) 
  epublish(type, header, message);
  m_mutex->unlock();
}

void PixMessages::publishMessage(PixMessages::MESSAGE_TYPE type, std::string partitionName, std::string header, std::string message) {
//   if(mout.size() + sout.size() == 0) {
//     std::string errorMsg =  "PixMessages::publishMessage: No stream or  MRS server can be found. Cannot publish.";  
//     throw PixMessagesExc(PixMessagesExc::ERROR, errorMsg);
//   }

  m_mutex->lock();
  for(std::map<std::string, std::ostream *>::iterator it = sout.begin(); it != sout.end(); it++) {
    if (it->first == partitionName) {
      spublish(type, *(it->second), header, message);
    }
  }
//   for(std::map<std::string, MRSStream*>::iterator it = mout.begin(); it != mout.end(); it++) {
//     if (it->first == partitionName) {    
//       publish(type, it->second, header, message);
//       std::string state = getState(partitionName);
//       if(state != "SUCCESS") {
// 	std::string errorMsg =  "Error during publication of the message; STATUS = " + state;  
// 	m_mutex->unlock();
// 	throw( PixMessagesExc(PixMessagesExc::WARNING, errorMsg) );
//       }
//     }
//   }
//  if (ersout) 
  epublish(type, header, message);
  m_mutex->unlock();
}

void PixMessages::printMessage(PixMessages::MESSAGE_TYPE type, std::string header, std::string message) {
//   if(mout.size() + sout.size() == 0) {
//     std::string errorMsg =  "PixMessages::publishMessage: No stream or  MRS server can be found. Cannot publish.";  
//     throw PixMessagesExc(PixMessagesExc::ERROR, errorMsg);
//   }

  m_mutex->lock();
  for(std::map<std::string, std::ostream *>::iterator it = sout.begin(); it != sout.end(); it++) {
    spublish(type, *(it->second), header, message);
  }
  m_mutex->unlock();
}

void PixMessages::printMessage(PixMessages::MESSAGE_TYPE type, std::string partitionName, std::string header, std::string message) {
//   if(mout.size() + sout.size() == 0) {
//     std::string errorMsg =  "PixMessages::publishMessage: No stream or  MRS server can be found. Cannot publish.";  
//     throw PixMessagesExc(PixMessagesExc::ERROR, errorMsg);
//   }

  m_mutex->lock();
  for(std::map<std::string, std::ostream *>::iterator it = sout.begin(); it != sout.end(); it++) {
    if (it->first == partitionName) {
      spublish(type, *(it->second), header, message);
    }
  }
  m_mutex->unlock();
}

void PixMessages::spublish(PixMessages::MESSAGE_TYPE type, std::ostream &str, std::string header, std::string message) {

  std::string myMessage = "UNKNOWN";
  switch(type) {
  case SUCCESS:
    myMessage = "SUCCESS";  
    break;  
  case INFORMATION:
    myMessage = "INFORMATION";  
    break;
  case DIAGNOSTIC:  
    myMessage = "DIAGNOSTIC; ";  
    break;  
  case WARNING:
    myMessage = "WARNING";  
    break;
  case ERROR:
    myMessage = "ERROR";  
    break;  
  case FATAL:
    myMessage = "FATAL";  
    break;
  }

  char t_s[100];
  time_t t;
  tm t_m;
  t = time(NULL);
  localtime_r(&t, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;

  str << date << " : " << myMessage << " - " << header << " - " << message << std::endl;
}

// void PixMessages::publish(PixMessages::MESSAGE_TYPE type, MRSStream* str, std::string header, std::string message) {  
//   severity SEVERITY;
//   switch(type) {
//   case SUCCESS: 
//     SEVERITY = MRS_SUCCESS;
//     break;   
//   case INFORMATION: 
//     SEVERITY = MRS_INFORMATION;
//     break;
//   case DIAGNOSTIC: 
//     SEVERITY = MRS_DIAGNOSTIC;
//     break;
//   case WARNING: 
//     SEVERITY = MRS_WARNING;
//     break;
//   case ERROR: 
//     SEVERITY = MRS_ERROR;
//     break;
//   case FATAL:
//     SEVERITY = MRS_FATAL;
//     break;
//   default:
//     SEVERITY = MRS_ERROR;
//     message += " (Severtity unknown)";
//   }
//   *str << header << SEVERITY << MRS_TEXT(message) << ENDM;
// }


void PixMessages::epublish(PixMessages::MESSAGE_TYPE type, std::string header, std::string message) {  
  PixLib::pix::daq issue(ERS_HERE, header, message);
  switch(type) {
  case SUCCESS: 
    ers::log(issue);
    break;   
  case INFORMATION: 
    ers::info(issue);
    break;
  case DIAGNOSTIC: 
    ers::debug(issue);
    break;
  case WARNING: 
    ers::warning(issue);
    break;
  case ERROR: 
    ers::error(issue);
    break;
  case FATAL:
    ers::fatal(issue);
    break;
  default:
    ers::error(issue);
  }
}

std::string PixMessages::getState(std::string partName) {
  //int state = 0;
  std::string output = "MAIN ERROR";
//   for(std::map<std::string, MRSStream*>::iterator it = mout.begin(); it != mout.end(); it++) {
//    if((*it).first == partName) {  
//       state =  ((*it).second)->getState();
      
//       switch(state){
//       case MRS_OPERATION_SUCCESS:
// 	output = "SUCCESS";
// 	break;
//       case MRS_NO_SERVER:
// 	output = "MRS_NO_SERVER";
// 	break;
//       case MRS_REPORT_ERROR:
// 	output = "MRS_REPORT_ERROR";
// 	break;
//       case MRS_MORE_THAN_ONE_MESSAGE_NAME:
// 	output = "MRS_MORE_THAN_ONE_MESSAGE_NAME";
// 	break;
//       case MRS_MESSAGE_WITHOUT_IDENTIFIER:
// 	output = "MRS_MESSAGE_WITHOUT_IDENTIFIER";
// 	break;
//       default: 
// 	return "UNKNOWN";
//       }
//     }
//   }
  return output;
}
