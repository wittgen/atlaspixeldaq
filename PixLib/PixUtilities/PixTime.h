
#ifndef _PIXLIB_PIXTIME
#define _PIXLIB_PIXTIME

#include <string>

namespace PixLib {

unsigned int Time();
std::string TimeToStr(unsigned int t);

}

#endif // _PIXLIB_PIXTIME
