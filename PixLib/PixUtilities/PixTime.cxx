
#include "PixTime.h"

#include <sys/time.h>

using namespace PixLib;

unsigned int PixLib::Time() {
  struct timeval t;
  gettimeofday(&t, NULL);
  return t.tv_sec;
}

std::string PixLib::TimeToStr(unsigned int t) {
  char t_s[100];
  tm t_m;
  time_t tt = (time_t)t;
  gmtime_r(&tt, &t_m);
  strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
  std::string date = t_s;
  return date;
}

