//////////////////////////////////////////////////////////////////////
// PixRootFileInMem.h
/////////////////////////////////////////////////////////////////////
//
// 7/10/09  Version 1.0 (NG)
//           
//

// Class dedicated to read a root file and put it in the histogram server

#ifndef _PIXLIB_PIXROOTFILEINMEM
#define _PIXLIB_PIXROOTFILEINMEM

#include <ipc/partition.h>
#include <ipc/object.h>

#include "BaseException.h"

#include <vector>
#include <list>
#include <map>
#include <string>

#include "PixRootFileInMemIDL.hh"


class IPCPartition;

namespace PixLib {


  class PixHistoServerInterface;

  // Class to handle PixRootFileInMem Exceptions missing
  
  class PixRootFileInMem : public IPCNamedObject<POA_ipc::PixRootFileInMem,ipc::multi_thread> {
    
  public:
    PixRootFileInMem(IPCPartition *ipcPartition, std::string ipcReaderName, std::string serverName);
    
    ~PixRootFileInMem();

    void shutdown() {}; 

    // Receive the file name to read
    bool ipc_reCreateOnLineRootFile(const char* rootFile);


  private:
    // Methods to dump a root histo in OH
    void processDirectory(TObject* object);
    bool processRootHistogram(TH2* histo);

    
    // Variables
    IPCPartition *m_ipcPartition;
    PixHistoServerInterface *m_hInt;
    std::string m_serverName;
  };

}

#endif
