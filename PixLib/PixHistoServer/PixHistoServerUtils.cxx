//////////////////////////////////////////////////////////////////////
// PixHistoServerUtils.cxx
/////////////////////////////////////////////////////////////////////
//
// 7/10/09  Version 1.0 (NG)
//           
//

#include <iostream>
#include <fstream>
#include <sstream>

#include "PixHistoServer/PixHistoServerUtils.h"

using namespace PixLib;



bool PixLib::compare(std::string firstPath, std::string secondPath, std::string &diffPath, std::string &equalPath)
{

  std::string subFirst = partString(firstPath, 0);
  std::string subSecond = partString(secondPath, 0);
  std::string oldFirst = firstPath;
  std::string oldSecond = secondPath;
  if (subFirst != subSecond) return false; 
  while ( (subFirst==subSecond) && (!subFirst.empty()) && (!subSecond.empty()) ) {
    oldFirst = oldFirst.substr(subFirst.length()+1);
    oldSecond = oldSecond.substr(subSecond.length()+1);
    if (!oldFirst.empty()) subFirst = partString(oldFirst, 0);
    if (!oldSecond.empty()) subSecond = partString(oldSecond, 0);
  }
  if (!subFirst.empty() && !subSecond.empty()) {
    equalPath = firstPath.substr(0, firstPath.length()-oldFirst.length());
    diffPath = oldSecond;
  } 
  return true;
  
}



std::string PixLib::partString (std::string initial, int start)
{
  std::string outPath;
  if (initial.find("/") != std::string::npos) {
    outPath = initial.substr(start, initial.find("/"));
  } else {
    outPath = "";
  } 
  return outPath;
}



std::string PixLib::parsingFolders (std::string initial) 
{
  std::string folders;
  int b = initial.rfind ("/");
  folders = initial.substr(0, b+1);
  return folders;
}
