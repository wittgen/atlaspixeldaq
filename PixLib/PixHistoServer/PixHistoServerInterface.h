//////////////////////////////////////////////////////////////////////
// PixHistoServerInterface.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 19/09/07  Version 1.0 (NG)
//           
// 5/10/09   Last Modification (NG)



#ifndef _PIXLIB_PIXHISTOSERVERINTERFACE
#define _PIXLIB_PIXHISTOSERVERINTERFACE

#include "BaseException.h"

//#include <ipc/server.h>
#include <owl/mutexrw.h>

#include "oh/OHRawProvider.h"
#include "oh/OHRootProvider.h" 
#include "oh/OHRootHistogram.h"
#include "oh/OHCommandListener.h" 

#include "PixHistoServer/PixNameServer.h"
#include "PixHistoServer/PixHistoServerUtils.h"

#include <TROOT.h>
#include <TApplication.h>
#include <TClass.h> 
#include <TH2.h>
#include <TFile.h>


class IPCPartition;

#define PROVIDERBASE "pixel_provider_" 
#define OHSERVERBASE "pixel_histo_server_" 

namespace PixLib {

  class MyCommandListener : public OHCommandListener {
  public:
    void command ( const std::string & name, const std::string & cmd ) {
      std::cout << " Command " << cmd << " received for the " << name << " histogram" << std::endl;
    }
    
    void command ( const std::string & cmd ){
      std::cout << " Command " << cmd << " received for all histograms" << std::endl;
      if ( cmd == "exit" ){
	//ipcServer.stop();
      }
    }
  };

  // Class to handle PixHistoServer Exceptions  
  class PixHistoServerExc : public SctPixelRod::BaseException { 
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    
    PixHistoServerExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
    virtual ~PixHistoServerExc() {};
    
    virtual void dump(std::ostream &out) {
      out << "Pixel Histo Server Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
    };
    std::string dumpLevel() {
      switch (m_errorLevel) {
      case INFO : 
	return "INFO";
      case WARNING :
	return "WARNING";
    case ERROR :
      return "ERROR";
      case FATAL :
	return "FATAL";
      default :
	return "UNKNOWN";
      }
    };
    ErrorLevel getErrorLevel() const  { return m_errorLevel; }; 
    std::string getId() const  { return m_id; };
    std::string getDescr() const  { return m_descr; };
  private:
    ErrorLevel m_errorLevel;
    std::string m_id;
    std::string m_descr;
  };

  class Histo;
  class PixScanHisto;
  class PixISManager;

  class PixHistoServerInterface {
  public:
    // constructor
    PixHistoServerInterface (IPCPartition* ipcPartition, std::string ohServerName, 
			     std::string serverName, std::string providerName);
    // destructor 
    ~PixHistoServerInterface ();     

    // check
    bool isValid();

    // save in the histogram server (OH+nameServer) the histogram 
    bool sendHisto(const std::string &name, Histo& his);
    bool sendHisto(const std::string &name, TH1& his);
    bool sendHisto(const std::string &name, TH2* his);
 
    bool receiveHisto(const std::string name, OHRootHistogram &his);

    std::vector<std::string> ls(std::string name);
    std::vector<std::string> lsFolder(std::string name);
    std::vector<std::string> lsFile(std::string name);
    std::vector<std::string> superLs(std::string name, bool scanHisto=false);

    long numberHisto(std::string name);

    bool removeHisto(std::string name);
    long removeTree(std::string rootName);

    // Write a Root File -> Interface with PixWriter
    bool writeRootHistoServer(std::string inPath, std::string outPath, bool multiFile); 

    // Put a root file in the histogram server -> Interface with PixReader
    bool reCreateOnLineRootFile (const std::string rootFilename);

    // Specialized Methods for PixScanHisto
    bool scanHistoExist(std::string &name, int a, int b, int c, int d);
    bool scanHistoBack(std::string name, int a, int b, int c, int d, OHRootHistogram &his);
    std::string scanHistoName(std::string name, int a, int b, int c, int d);
    

  protected:  
    OWLMutexRW m_mutex;  
    

  private:
    IPCPartition* m_ipcPartition;
    std::string m_ohServerName;
    std::string m_serverName;
    std::string m_providerName;
    PixISManager *m_is; 
    MyCommandListener m_lst;
    OHRawProvider<OHBins> * m_rawProvider;
    OHRootProvider * m_rootProvider;
    ipc::PixNameServer_var m_pixServer;

    // Use private variables   
    PixISManager &getIsManager();
    OHRawProvider<OHBins> &getRawProvider();
    OHRootProvider &getRootProvider(const std::string rootProviderName);
  
    // Build up the histogram name, the same of the folder structure 
    std::string histoName(const std::string &name, Histo& his);                   

    // Send the histogram in IS
    bool putHisto(std::string name, Histo& his);
    bool putHisto(std::string name, TH1& his);
    bool putRootHisto(std::string name, TH2* histo); 
    // Send the name to the name server
    bool putFolder(std::string const & name);  

    //Get the histograms from the histo server
    bool getHisto(std::string name, OHRootHistogram &his);            

    // Methods to check the naming convention
    bool nullName(std::string const & name);
    bool noSlash(std::string const & name);
    bool firstSlashMissing(std::string const & name);
    bool lastSlashMissing(std::string const & name);
  };
}

#endif 





