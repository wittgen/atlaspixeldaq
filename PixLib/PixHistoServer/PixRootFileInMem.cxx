//////////////////////////////////////////////////////////////////////
// PixRootFileInMem.cxx
/////////////////////////////////////////////////////////////////////
//
// 7/10/09  Version 1.0 (NG)

#include <exception>
#include <fstream>
#include <sstream>


#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixHistoServer/PixRootFileInMem.h"
#include "Histo/Histo.h"
#include "PixUtilities/PixMessages.h"

#include <TRegexp.h>

using namespace PixLib;

  

PixRootFileInMem::PixRootFileInMem(IPCPartition* ipcPartition, std::string ipcReaderName, std::string serverName) :
  IPCNamedObject<POA_ipc::PixRootFileInMem,ipc::multi_thread>(*ipcPartition, ipcReaderName.c_str()),
  m_serverName(serverName)
{
  m_ipcPartition = new IPCPartition(*ipcPartition);
  try {
    publish();
  } catch (const daq::ipc::InvalidPartition&) {
    std::cout << "InvalidPartition " << std::endl;
  } catch (const daq::ipc::InvalidObjectName&) {
    std::cout << "InvalidObjectName " << std::endl;
  }
  
  std::string provName = "helper";
  try {
    m_hInt = new PixHistoServerInterface(m_ipcPartition, OHSERVERBASE+provName, m_serverName, PROVIDERBASE+provName);
  } catch (const PixHistoServerExc &e) {
    if (e.getId()=="NONAMESERVER" || e.getId()=="NOIPC" || e.getId()=="NAMESERVER" 
	|| e.getId()=="NAMESERVERPROBLEM" || e.getId()=="NONAMESERVER") {
      std::cout << e.getDescr() << std::endl; 
    }
  } catch (...) {
   std::cout << "PixHistoServerInterface constructor thrown an unknown exception " << std::endl; 
  }
}


PixRootFileInMem::~PixRootFileInMem() 
{
  if (m_hInt) delete m_hInt;
  if (m_ipcPartition != 0) delete m_ipcPartition;
  // Withdraw from IPC partition
  //withdraw();
  IPCNamedObject<POA_ipc::PixRootFileInMem,ipc::multi_thread>::_destroy();
}


bool PixRootFileInMem::ipc_reCreateOnLineRootFile(const char* rootFileName)
{

 TFile* dumpFile = TFile::Open(rootFileName);
 dumpFile->ReadAll("dirs*");
 TList* list = dumpFile->GetList();
 TIter next(list);
 TObject* obj;
 while ((obj = next())) {
   TString className = obj->IsA()->GetName();
   std::cout << " PixRootFileInMem::ipc_reCreateOnLineRootFile - Processing first object in file: " 
	     << obj << " class " << className << std::endl;
   if (className == "TDirectory" || className == "TDirectoryFile") {
     processDirectory(obj);
   } else {
     ers::fatal(PixLib::pix::daq (ERS_HERE, "reCreateOnLineRootFileLocal", "No directory to start with "));
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NODIR", "No directory to start with ");
   }
 }
 return true;
}


void PixRootFileInMem::processDirectory(TObject* object)
{
  // TDirectory* dir = (TDirectory*)(object);
  TDirectory* dir = dynamic_cast<TDirectory*>(object);
  dir->ReadAll();
  TIter next(dir->GetList());
  TObject* obj;
  while ((obj = next())) {
    TString className = obj->IsA()->GetName();
    //std::cout << "Processing object in file: " << obj << " class " << className << std::endl;
    if (className == "TDirectory" || className == "TDirectoryFile" ) {
      //std::cout << "Found dir: " << obj << " class " << className << " name " << obj->GetName() << std::endl;
      processDirectory(obj);
    }
    else if (className=="TH2F" || className=="TH2D" || className=="TH1F" || className=="TH1D") {
      //std::cout << "Found histogram: " << obj << " class " << className << " name " << obj->GetName() << std::endl;
      processRootHistogram(dynamic_cast<TH2*>(obj));
    }
    else {
      ers::fatal(PixLib::pix::daq (ERS_HERE, "processDirectory", "No TDirectory or TH2 "));
      throw PixHistoServerExc(PixHistoServerExc::FATAL, "NODIRTH2", "No TDirectory or TH2 ");
    }
  }
}



bool PixRootFileInMem::processRootHistogram(TH2* histo)
{
  std::string basicName = histo->GetName();
  int startName = basicName.find_first_of("/");
  const std::string realName = basicName.substr(startName, basicName.size());

  bool check = m_hInt->sendHisto(realName, histo);
  return check;
  
}


