//////////////////////////////////////////////////////////////////////
// PixRootFileWriter.h
// version 0.0
/////////////////////////////////////////////////////////////////////
//
// 13/07/09  Version 0.0 (NG)
//           
// 5/10/09   Last Modification (NG)

// Class dedicated to write root file, using the histogram server

#ifndef _PIXLIB_PIXROOTFILEWRITER
#define _PIXLIB_PIXROOTFILEWRITER

#include <ipc/partition.h>
#include <ipc/object.h>

#include "BaseException.h"

#include <vector>
#include <list>
#include <map>
#include <string>

#include "PixRootFileWriterIDL.hh"
#include "PixHistoServer/my_rootReceiver.h"


class IPCPartition;

namespace PixLib {
  
  // Class to handle PixRootFileWriter Exceptions  
  class PixRootFileWriterExc : public SctPixelRod::BaseException { 
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    
    PixRootFileWriterExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
      virtual ~PixRootFileWriterExc() {};
      
      virtual void dump(std::ostream &out) {
	out << "PixRootFileWriter Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
      };
      std::string dumpLevel() {
	switch (m_errorLevel) {
	case INFO : 
	  return "INFO";
	case WARNING :
	  return "WARNING";
	case ERROR :
	  return "ERROR";
	case FATAL :
	  return "FATAL";
	default :
	  return "UNKNOWN";
	}
      };
      ErrorLevel getErrorLevel() const { return m_errorLevel; }; 
      std::string getId() const  { return m_id; };
      std::string getDescr() const  { return m_descr; };
  private:
      ErrorLevel m_errorLevel;
      std::string m_id;
      std::string m_descr;
  };
  
  
  class PixHistoServerInterface;

  
  class PixRootFileWriter : public IPCNamedObject<POA_ipc::PixRootFileWriter,ipc::multi_thread> {
    
  public:
    PixRootFileWriter(IPCPartition *ipcPartition, std::string ipcWriterName, std::string serverName);
    
    ~PixRootFileWriter();

    // Receive the histogram(s) to store in a root file
    bool ipc_writeRootHisto(const char* inPath, const char* outPath, bool multiFile);

    void shutdown() {}; 

    PixISManager* isManager() { return m_isMan;};

    void processData(std::string folderToProcess, std::string rootFile, bool multiFile);
    long int countHisto(std::string inPath, std::string outPath);

  protected:
    std::multimap<std::string, std::string> m_storage;
    std::vector<std::string> m_storageOrder;
   

  private:
    // method called by processData.
    // method which actually writes the file
    // multiFile = true -> more files will be written
    void saveRootHisto(std::string inPath, std::string outPath, bool multiFile); 

    bool createDirRootStruct(std::string inPath); 
    void fillDirRootStruct(std::string outputName, std::string inPath, std::vector<std::string> &trueSubFolder); 
    std::string writeInTheRightPlace(std::string outputPath, std::string& fileName);
    void copyRootFile(std::string outputPath, std::string fileName);
    void publishWritingStatus(std::string fileName);
    void mergeFiles(const TList* list, TDirectory* targetDir, TDirectory* sourceDir);

    // Variables
    IPCPartition *m_ipcPartition;
    PixHistoServerInterface *m_hInt;
    std::string m_ipcWriterName;
    std::string m_serverName;
    PixISManager* m_isMan;
    int m_writerStatus;
    long int m_memStatusCounter;
  };

}

#endif
