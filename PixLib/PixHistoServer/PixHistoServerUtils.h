//////////////////////////////////////////////////////////////////////
// PixHistoServerUtils.h
/////////////////////////////////////////////////////////////////////
//
// 7/10/09  Version 1.0 (NG)
//           
//

// Functions to handle strings

#ifndef _PIXLIB_PIXHISTOSERVERUTILS
#define _PIXLIB_PIXHISTOSERVERUTILS

#include "BaseException.h"

#include <vector>
#include <list>
#include <map>
#include <string>


namespace PixLib {

  bool compare(std::string firstPath, std::string secondPath, 
	       std::string &diffPath, std::string &equalPath);

  std::string partString(std::string initial, int start);

  std::string parsingFolders(std::string initial); 
}

#endif
