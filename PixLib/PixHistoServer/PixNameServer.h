//////////////////////////////////////////////////////////////////////
// PixNameServer.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 24/09/07  Version 1.0 (NG)
//           
// 5/10/09   Last Modification (NG)



#ifndef _PIXLIB_PIXNAMESERVER
#define _PIXLIB_PIXNAMESERVER

#include <ipc/partition.h>
#include <ipc/object.h>
#include <owl/mutexrw.h>

#include "BaseException.h"

#include <vector>
#include <list>
#include <map>
#include <string>

#include "PixNameServerIDL.hh"

#ifdef CORBALONGLONG
#define MY_LONG_INT int32_t
#else
#define MY_LONG_INT CORBA::Long
#endif

class IPCPartition;

namespace PixLib {

  class PixHistoServerInterface;

  // Class to handle PixNameServer Exceptions
  
  class PixNameServerExc : public SctPixelRod::BaseException { 
  public:
    enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
    
    PixNameServerExc(ErrorLevel el, std::string id, std::string descr) : BaseException(id), m_errorLevel(el), m_id(id), m_descr(descr) {}; 
    virtual ~PixNameServerExc() {};
    
    virtual void dump(std::ostream &out) {
      out << "Pixel Name Server Exception: " << dumpLevel() << ":" << m_id << " - "<< m_descr << std::endl; 
    };
    std::string dumpLevel() {
      switch (m_errorLevel) {
      case INFO : 
	return "INFO";
      case WARNING :
	return "WARNING";
      case ERROR :
	return "ERROR";
      case FATAL :
	return "FATAL";
      default :
	return "UNKNOWN";
      }
    };
    ErrorLevel getErrorLevel() const  { return m_errorLevel; }; 
    std::string getId() const { return m_id; };
    std::string getDescr() const { return m_descr; } ;
  private:
    ErrorLevel m_errorLevel;
    std::string m_id;
    std::string m_descr;
  };


  class PixNameServer : public IPCNamedObject<POA_ipc::PixNameServer,ipc::multi_thread> {

  public:
    // Constructor of the main folder, /
    PixNameServer(IPCPartition *ipcPartition, std::string nameServer);
    // Destructor
    ~PixNameServer();

    void shutdown() {};

    // Put an obj in the server
    MY_LONG_INT ipc_putName(const char* name);
    
    // Get an obj from the server
    bool ipc_getName(const char* name);

    // Open a folder and return what it is inside (ls)
    MY_LONG_INT ipc_lsFolder(const char* name, ipc::ipc_string_vect_out result);
    MY_LONG_INT ipc_lsFile(const char* name, ipc::ipc_string_vect_out result);
    MY_LONG_INT ipc_superLs(const char* name, ipc::ipc_string_vect_out result, bool scanHisto);
    MY_LONG_INT ipc_numberHisto(const char* name);

    // Remove obj
    bool ipc_removeHisto(const char* name);
    MY_LONG_INT  ipc_removeTree(const char* rootName);

  protected:
     struct mem {
      std::map<std::string, mem> folder;
      std::map<std::string, std::string> file;
    };
    mem m_home; 

    OWLMutexRW m_mutex;
  
  private:
    void fillStruct(mem &MyStruct, std::string name, std::string path);
    bool readStruct(mem MyStruct, std::string path);
    std::vector<std::string> openFolder(mem &MyStruct, std::string name, 
					std::string path, bool type); // type=true: look for folders
    std::vector<std::string> superLs(const char* name, std::string start, 
				     bool scanHisto); // scanHisto=true: look just for files 
    bool removeHisto(mem &MyStruct, std::string path);
    bool removeTree(mem &MyStruct, std::string path);

    IPCPartition *m_ipcPartition;
    std::string m_nameServer;
    // bool m_remove;
  };

}

#endif
