//////////////////////////////////////////////////////////////////////
// PixHistoServerInterface.cxx
/////////////////////////////////////////////////////////////////////
//
// 19/09/07  Version 1.0 (NG)
//           
// 5/10/09   Interface with PixReader (NG)
//           Separate PixHistoServerUtils (NG)


#include <unistd.h>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>

#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixHistoServer/PixRootFileWriter.h"
#include "PixHistoServer/PixRootFileInMem.h"
#include "Histo/Histo.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"

#include <oh/OHIterator.h>
#include <oh/OHProviderIterator.h>

#include <is/serveriterator.h>

using namespace PixLib;



PixHistoServerInterface::PixHistoServerInterface(IPCPartition* ipcPartition, std::string ohServerName, std::string serverName, std::string providerName) :
  m_ipcPartition(ipcPartition), m_ohServerName(ohServerName), m_serverName(serverName), 
  m_providerName(providerName), m_is(NULL), m_rawProvider(NULL), m_rootProvider(NULL) 
{
  try {
    m_pixServer = m_ipcPartition->lookup<ipc::PixNameServer>(m_serverName);
  } catch (const daq::ipc::InvalidPartition&) {
    // The partion ipcPartition does not exist
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NOIPC", "IPC server is unavailable ");
  } catch (const daq::ipc::InvalidObjectName&) {
    // The serverName is an empty string
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVER", "The name of the nameServer is null ");
  } catch (const daq::ipc::ObjectNotFound&) {
    // The name server serverName does not exist
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVERPROBLEM", 
			    "nameServer does not exist or is not registered with the partition server.  ");
  }

  getIsManager();
  
}


PixHistoServerInterface::~PixHistoServerInterface() 
{
  if (m_is) delete m_is;
  if (m_rawProvider) delete m_rawProvider;
  if (m_rootProvider) delete m_rootProvider;
}


bool PixHistoServerInterface::isValid()  
{
  bool valid;
  valid = m_ipcPartition->isValid();
  if (valid) {
    valid = m_ipcPartition->isObjectValid<is::repository>(m_ohServerName);
    if (valid) {
      valid = m_ipcPartition->isObjectValid<ipc::PixNameServer>(m_serverName);
      if (!valid) std::cerr << "Provider: " << m_providerName << " --- m_serverName seems invalid, name is: " << m_serverName << std::endl;
    } else {
      std::cerr << "Provider: " << m_providerName << " --- m_ohServerName seems invalid, name is: " << m_ohServerName << std::endl;
    }
  } else {
    std::cerr << "Provider: " << m_providerName << " --- m_ipcPartition is not a valid partition, name is: " << std::endl;
  }
  return valid;
}

PixISManager& PixHistoServerInterface::getIsManager()
{
  m_is = new PixISManager(m_ipcPartition->name(), m_ohServerName);
  return *m_is;
}


OHRawProvider<OHBins>& PixHistoServerInterface::getRawProvider()
{
  if (!m_rawProvider) {
    try {
      m_rawProvider = new OHRawProvider<OHBins>(*m_ipcPartition, m_ohServerName, m_providerName, &m_lst);
    } catch (const daq::oh::Exception &e) {
//       std::string info = "rawProv";
//       std::string mess = "m_providerName already in use, I create a new one! ";
//       m_msg->publishMessage(PixMessages::WARNING, info, mess);
      struct timeval t0;
      int time = gettimeofday(&t0,NULL);
      std::stringstream newProviderName;
      newProviderName << m_providerName << time;
      m_rawProvider = new OHRawProvider<OHBins>(*m_ipcPartition, m_ohServerName, newProviderName.str(), &m_lst);
    } 
    catch (...) {
      std::string info = "rawProv";
      std::string mess = "Unknown Exception was thrown in the provider constructor ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));
    }
  } else {
//     std::string info = "rawProv";
//     std::string mess = "You have already m_rawProvider! ";
//     m_msg->publishMessage(PixMessages::INFORMATION, info, mess);
  }
  return *m_rawProvider;
}


OHRootProvider& PixHistoServerInterface::getRootProvider(const std::string rootProviderName)
{
  if (!m_rootProvider)
  try {
    m_rootProvider = new OHRootProvider(*m_ipcPartition, m_ohServerName, rootProviderName, &m_lst);
  } catch (const daq::oh::RepositoryNotFound&) {
    std::cout << "Repository not found!" << std::endl;
  } catch (const daq::oh::ProviderAlreadyExist&) {
//     std::string info = "rootProv";
//     std::string mess = "rootProvider already exist, I create a new one! ";
//     m_msg->publishMessage(PixMessages::WARNING, info, mess);
    struct timeval t0;
    gettimeofday(&t0,NULL);
    std::stringstream newRootProviderName;
    newRootProviderName << rootProviderName << t0.tv_sec;
    std::cout << newRootProviderName.str() << std::endl;
    m_rootProvider = new OHRootProvider(*m_ipcPartition, m_ohServerName, newRootProviderName.str(), &m_lst);
  } catch (...) {
    std::string info = "rootProv";
    std::string mess = "Unknown Exception was thrown! ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
  }
  return *m_rootProvider;
}




bool PixHistoServerInterface::sendHisto(const std::string &name, 
					Histo& his) 
{
  std::string f;
  bool check;
  bool exist;

  std::string xname,yname;
  yname = name;// + '/';
  for (size_t ic=0; ic<yname.size(); ic++) {
    if (yname[ic] == '/' && ic > 0) {
      if (yname[ic-1] != '/') {
	xname += yname[ic];
      }
    } else {
      xname += yname[ic];
    }
  }

  f = histoName(xname, his);
  check = putHisto(f, his);
  if (check)  {
    exist = putFolder(f);
  } else  {
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in OHServer failed. ");
  }
  if (!exist){ 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in Name Server failed. ");
  } else {
    return true;
  }
}


bool PixHistoServerInterface::sendHisto(const std::string &name, 
					TH1& his) 
{
  bool check;
  bool exist;

  std::string xname;
  for (size_t ic=0; ic<name.size(); ic++) {
    if (name[ic] == '/' && ic > 0) {
      if (name[ic-1] != '/') {
	xname += name[ic];
      }
    } else {
      xname += name[ic];
    }
  }

  check = putHisto(xname, his);
  if (check)  {
    exist = putFolder(xname);
  } else  {
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in OHServer failed. ");
  }
  if (!exist){ 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in OHServer failed. ");
    return false;
  } else {
    return true;
  }
}


bool PixHistoServerInterface::sendHisto(const std::string &name, 
					TH2* his) 
{
  bool check;
  bool exist;

  std::string xname;
  for (size_t ic=0; ic<name.size(); ic++) {
    if (name[ic] == '/' && ic > 0) {
      if (name[ic-1] != '/') {
	xname += name[ic];
      }
    } else {
      xname += name[ic];
    }
  }

  check = putRootHisto(xname, his);
  if (check)  {
    exist = putFolder(xname);
  } else  {
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in OHServer failed. ");
  }
  if (!exist){ 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "PUBLISHFAILED", "Publishing in OHServer failed. ");
    return false;
  } else {
    return true;
  }
}



bool PixHistoServerInterface::receiveHisto(std::string name, OHRootHistogram &his) 
{
  bool status, check;
  try {
    status = m_pixServer->ipc_getName(name.c_str());
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_getname", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  }
  if (status) {
    std::string info = "getName";
    std::string mess = "The histogram "+name+" is in the name server ";
    check=getHisto(name, his);
  } else {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOHISTOGRAM", "The histogram "+name+" was not found on name Server");
  }
  return status&check;
}


std::string PixHistoServerInterface::histoName(const std::string &name, Histo& his) 
{
  // The final name of the histogram is the name passed by the user 
  // + the Histo name
  std::string finalName = name+his.name();
  return finalName;
}


std::vector<std::string> PixHistoServerInterface::ls(std::string name) 
{
  std::string info = "ls";
  std::vector<std::string> folder, file, tot;
  try {
    folder = lsFolder(name);
    if (folder.size()!=0) {
      for(size_t i =0; i<folder.size(); i++){
	tot.push_back(folder[i]);
      }
    } else folder.clear();
    file = lsFile(name);
    if (file.size()!=0) {
      for(size_t j =0; j<file.size(); j++){
	tot.push_back(file[j]);
      }
    } else file.clear();
    if (folder.size()==0 && file.size()==0) {
      std::string mess = "The folder "+name+ " is empty ";
      ers::info(PixLib::pix::daq (ERS_HERE, info, mess));
      tot.clear();
    }
  } catch(const PixHistoServerExc &e){ throw; }
  return tot;
}


std::vector<std::string> PixHistoServerInterface::lsFolder(std::string name) 
{
  CORBA::Long test=0;
  std::vector<std::string> folder;
  ipc::ipc_string_vect_var resultFolder;
  std::string res;
  std::string info = "lsFolder";

  if (nullName(name) || noSlash(name) || firstSlashMissing(name) || lastSlashMissing(name)) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "LSFOLDER", "The folder "+name+" is an invalid name");


  try {
    test = m_pixServer->ipc_lsFolder(name.c_str(), resultFolder);
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, info, "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch(const ipc::no_folder&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOFOLDER", "folder not found ");
  } catch(const ipc::no_home&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOHOME", "home not found ");
  }
  if (test==1) {
    size_t L = resultFolder->length(); 
    for (size_t i=0; i<L; i++) {
      res = resultFolder[i].str;
      folder.push_back(res);
    }
  } else if (test==0) {
    std::string mess = "No folder(s) inside "+name+ " ";
    ers::info(PixLib::pix::daq (ERS_HERE, info, mess));
    folder.clear();
  } 
  return folder;
}


std::vector<std::string> PixHistoServerInterface::lsFile(std::string name) 
{
  CORBA::Long test=0;
  std::vector<std::string> file;
  ipc::ipc_string_vect_var resultFile;
  std::string res;
  std::string info = "lsFile";

  if (nullName(name) || noSlash(name) || firstSlashMissing(name) || lastSlashMissing(name)) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "LSFILE", "The folder "+name+" is an invalid name");

  try {
    test = m_pixServer->ipc_lsFile(name.c_str(), resultFile);
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_lsFile", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch(const ipc::no_folder&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOFOLDER", "folder not found ");
  } catch(const ipc::no_home&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOHOME", "home not found ");
  }
  if (test==1) {
    size_t L = resultFile->length(); 
    for (size_t i=0; i<L; i++) {
      res = resultFile[i].str;
      file.push_back(res);
    }
  } else if (test==0){
    std::string mess = "No file(s) inside "+name+ " ";
    ers::info(PixLib::pix::daq (ERS_HERE, info, mess));
    file.clear();
  }
  return file;
}


std::vector<std::string> PixHistoServerInterface::superLs(std::string name, bool scanHisto) 
{
  std::string info = "superLs";
  CORBA::Long test=0;
  std::vector<std::string> results;
  ipc::ipc_string_vect_var resultsTot;
  std::string res;

  if (nullName(name) || noSlash(name) || firstSlashMissing(name) || lastSlashMissing(name)) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "SUPERLS", "The folder "+name+" is an invalid name");

  try {
    test = m_pixServer->ipc_superLs(name.c_str(), resultsTot, scanHisto);
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_superLs", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch(const ipc::no_folder&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOFOLDER", "folder not found ");
  } catch(const ipc::no_home&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOHOME", "home not found ");
  }
  if (test==1) {
    size_t L = resultsTot->length(); 
    for (size_t i=0; i<L; i++) {
      res = resultsTot[i].str;
      results.push_back(res);
    }
  } else if (test==0){
    std::string mess = "The folder "+name+ " is empty ";
    ers::info(PixLib::pix::daq (ERS_HERE, info, mess));
    results.clear();
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "EMPTYSUPERLS", "The folder "+name+" is empty ");
  }
  return results;
}

 

bool PixHistoServerInterface::putHisto(std::string name, Histo& his) {
  int nbin0 = his.nBin(0);
  std::string title = his.title();
  float* array = new float[nbin0]; // float, to reduce the used mem
  float* err = 0; 
  OHAxis xaxis("label", nbin0, 0, 1); 

  bool check=false;
  std::string info = "putHisto";

  // Create Annotations
  std::vector<std::pair<std::string,std::string> > annotations;
  annotations.push_back( std::make_pair("PixHistoServerInterface::createArray", "RawProvider publish") );

  if (his.nDim() == 1)  {
    for (int i=0; i<nbin0; i++) {
      array[i]=his(i);
    }
    // Publish in OH
    if (!m_rawProvider) getRawProvider();
    if(m_rawProvider) {
      try {
	m_rawProvider->publish(name, title, xaxis, array, err, false, -1, annotations);
	check = true;
      } catch (const daq::oh::RepositoryNotFound&) {
	std::string mess = "The repository is not found ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (const daq::oh::ObjectTypeMismatch&) {
	std::string mess = "Arguments invalid ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (...) {
	std::string mess = "Unknown exception thrown ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      }
    } else {
      std::string mess = "No raw provider created ";
      ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));
      check = false;
    }
  } else if (his.nDim() == 2) {
    int nbin1 = his.nBin(1);
    float* array1 = new float[nbin1*nbin0]; 
    float* err1 = 0; 
    // Extract array
    for (int i=0; i<nbin0; i++) {
      for (int j=0; j<nbin1; j++) {
	array1[j*nbin0+i]=his(i, j);
      }
    }
    OHAxis yaxis("labely", nbin1, 0, 1); 
    if (!m_rawProvider) getRawProvider();
    if(m_rawProvider) {
      try {
	m_rawProvider->publish(name, title, xaxis, yaxis, array1, err1, false, -1, annotations);
	check = true;
      } catch (const daq::oh::RepositoryNotFound&) {
	std::string mess = "The repository is not found ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (const daq::oh::ObjectTypeMismatch&) {
	std::string mess = "Arguments invalid ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (...) {
	std::string mess = "Unknown exception thrown ";
	ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));
      }
    }
    delete[] array1;
  } else {
    check = false;
    std::string mess = "The histogram has dimension different from 1 and 2 - NOT SUPPORTED YET ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NORIGHTHISTODIM", mess);
  }
  delete[] array;
  return check;  
}



bool PixHistoServerInterface::putHisto(std::string name, TH1& his) 
{
  bool check;
  std::string info = "rootProv";
  std::string rootHistoFromAnalysis = "RootHistoFromAnalysis";
  std::string rootAnalysisProviderName = PROVIDERBASE+rootHistoFromAnalysis;

  if (!m_rootProvider) getRootProvider(rootAnalysisProviderName);
  // Publish in OH
  if(m_rootProvider) {
    try {
      m_rootProvider->publish(his, name);
    } catch (const daq::oh::RepositoryNotFound&) {
      std::string mess = "The repository is not found ";
      ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
    } catch (const daq::oh::ObjectTypeMismatch&) {
      std::string mess = "Arguments invalid ";
      ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
    } catch (...) {
      std::string mess = "Unknown exception thrown ";
      ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
    }
    check = true;
  } else {
    std::string mess = "No root provider was created ";
    ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));
    check = false;
  }
  return check;
}

bool PixHistoServerInterface::putRootHisto(std::string name, TH2* histo) 
 {
   bool putInOH=false;
   std::string info = "rootProv";
   std::string ReloadOH = "ReloadOH";
   std::string rootProviderName = PROVIDERBASE+ReloadOH;
 
   if(!m_rootProvider) getRootProvider(rootProviderName);
   if (m_rootProvider) {
     try {
       m_rootProvider->publish(*(TH1*)histo, name);
       putInOH=true;
     } catch (const daq::oh::RepositoryNotFound&) {
       std::string mess = "The repository is not found ";
       ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
     } catch (const daq::oh::ObjectTypeMismatch&) {
       std::string mess = "Arguments invalid ";
       ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
     } catch (...) {
       std::string mess = "Unknown exception thrown ";
       ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
     }
   } else {
     std::string mess = "No root provider was created ";
     ers::fatal(PixLib::pix::daq (ERS_HERE, info, mess));
     putInOH=false;
   } 
   return putInOH;
 }

    
     
bool PixHistoServerInterface::putFolder(std::string const & name) {
  CORBA::Long cmdExec;
  std::string info = "putFolder";

  try {
    cmdExec = m_pixServer->ipc_putName(name.c_str());
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_putName", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  }

  if (cmdExec == 0) {
    return true;
  } else if (cmdExec == 1) {
    std::string mess = "Invalid name ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
  } else if (cmdExec == 2) {
    std::string mess = "Empty folder ";
    ers::warning(PixLib::pix::daq (ERS_HERE, info, mess));
  }
  return false;
}



bool PixHistoServerInterface::getHisto(std::string name, OHRootHistogram &his) 
{
  std::string info = "getRootHisto";
  ISServerIterator isIter(*m_ipcPartition);
  std::string isServerName;
  while(isIter++) {
    isServerName = isIter.name();
    // Check if the name contains the define OHSERVERBASE "pixel_histo_server_"
    // Loop over all the providers
    if (isServerName.substr(0, 19) == std::string(OHSERVERBASE)) {
      std::string provBase = ".*";
      OHProviderIterator provIter(*m_ipcPartition, isServerName, provBase);
      std::string providerName;
      while (provIter++) {
 	providerName=provIter.name();
	try {
	  his=OHRootReceiver::getRootHistogram(*m_ipcPartition, isServerName, providerName, name, -1);
	  std::string mess = "The histogram has been found in the provider "+ providerName + " ";
 	  //	  m_msg->publishMessage(PixMessages::SUCCESS, info, mess);
 	  return true;
 	} catch (const daq::oh::ObjectNotFound&) {
 // 	  std::string mess = "The object is not in the provider "+ providerName + " ... try in the next one! ";
 // 	  m_msg->publishMessage(PixMessages::WARNING, info, mess);
 	} catch (const daq::oh::InvalidObject&) {
 	  std::string mess = "Invalid Object ";
 	  ers::warning(PixLib::pix::daq (ERS_HERE, info, mess));
 	} catch (...) {
 	  std::string mess = "Unknown exception thrown ";
 	  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
 	}
      }
    }
  }
  return false;
}


// The histogram name is intended as the complete one, 
// starting from the main folder (e.g. /S0000/ROD0/test.root)
bool PixHistoServerInterface::removeHisto(std::string name) 
{
  std::string info = "removeHisto";

  // Check conventions 
  if (nullName(name) || noSlash(name) || firstSlashMissing(name) ) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "REMOVEHISTO", "The folder "+name+" is an invalid name");
  if (name.find("/") == std::string::npos || name.length() <= 1) {
    std::string message = "ERROR: Histogram " + name + " is an invalid name ";
    message += "/ missing? Please, correct the name OR Do you want to delete all the histograms (i.e. you are passing /)? This is the wrong method! "; 
    ers::error(PixLib::pix::daq (ERS_HERE, info, message));
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "REMOVEHISTO", message);
  }
  // check if the name exists
  if (m_pixServer->ipc_getName(name.c_str())) {
    // delte it from IS
    ISServerIterator isIt(*m_ipcPartition, std::string(OHSERVERBASE)+".*");
    while ( isIt() ) {
      OHIterator ohIt(*m_ipcPartition, isIt.name(), ".*", name);
      while (ohIt++) {
	try {
	  // JGK TO DO: temporary, need to check for proper replacement
	  //ohIt.remove();
	} catch (const daq::is::RepositoryNotFound&) {
	  std::string mess = "The repository is not found ";
	  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
	} catch (const daq::is::InvalidIterator&) {
	  std::string mess = "Invalid Iterator ";
	  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
	} catch (...) {
	  std::string mess = "Unknown exception thrown ";
	  ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
	}
      }
    }
    if (m_pixServer->ipc_removeHisto(name.c_str()) ) {
      return true;
    } else {
      std::cout << "ERROR: Histogram "<< name <<" NOT deleted in NameServer" << std::endl; 
      throw PixHistoServerExc(PixHistoServerExc::ERROR, "REMOVEHISTO", "Histogram not deleted in the name server ");
    }
  } else {
    std::string mess = "Histogram "+ name +" does not exist ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "REMOVEHISTO", "The folder "+name+" is an invalid name");
  }
}


long PixHistoServerInterface::removeTree(std::string rootName) 
{
  std::vector<std::string> groupFile;
  std::string nameForOh = rootName.substr(1,rootName.length()-1);
  CORBA::Long cmdExec;
  std::string info = "removeTree";

  // Check conventions 
  // null name or no / or first slash missing
  if (nullName(rootName) || noSlash(rootName) || firstSlashMissing(rootName) || lastSlashMissing(rootName)) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "REMOVETREE", "The folder "+rootName+" is an invalid name");
    
  try {
    groupFile = superLs(rootName, true);
  } catch (const PixHistoServerExc &e) {
    if (e.getId() == "EMPTYSUPERLS") {
      cmdExec = 3;     
      std::string mess = "No histograms or subfolders found in " + rootName;
      ers::warning(PixLib::pix::daq (ERS_HERE, info, mess));
      return long(cmdExec);
    } else if (e.getId()=="NOFOLDER" || e.getId()=="NOHOME") {throw;}
  }
  try {
    cmdExec = m_pixServer->ipc_removeTree(rootName.c_str());
  } catch (const CORBA::TIMEOUT&) {
   ers::warning(PixLib::pix::daq (ERS_HERE,"ipc_removeTree", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  }
  if (cmdExec == 0) {
    ISServerIterator isIt(*m_ipcPartition, std::string(OHSERVERBASE)+".*");
    while ( isIt() ) {
      try {
	OHIterator ohIt(*m_ipcPartition, isIt.name(), ".*", rootName+".*");
	// JGK TO DO: temporary, need to check for proper replacement
	//ohIt.removeAll();
      } catch (const daq::is::RepositoryNotFound&) {
	std::string mess = "The repository is not found ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (const daq::is::InvalidIterator&) {
	std::string mess = "Invalid Iterator ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      } catch (...) {
	std::string mess = "Unknown exception thrown ";
	ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      }
    } 
  }
  return long(cmdExec);  
}


long PixHistoServerInterface::numberHisto(std::string name) 
{
  CORBA::Long tot=0;

  if (nullName(name) || noSlash(name) || firstSlashMissing(name) || lastSlashMissing(name)) 
    throw PixHistoServerExc(PixHistoServerExc::ERROR, "NUMBERHISTO", "The folder "+name+" is an invalid name");

  try {
  tot = m_pixServer->ipc_numberHisto(name.c_str()); 
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE,"ipc_numberHisto", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch(const ipc::no_folder&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOFOLDER", "folder not found ");
  } catch(const ipc::no_home&) {
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "NOHOME", "home not found ");
  }
  return long (tot);
}


// name is the folder path without A/B/C
// 0 = A0; 1 = A1; -1=skipped folder
bool PixHistoServerInterface::scanHistoExist(std::string &name, int a, int b, int c, int d) 
{
  bool status=false;
  std::stringstream finalName;
  std::vector<std::string> res;
  std::string pass;

  std::cout << a << b << c << d<< std::endl;
  if (a != -1) {
    finalName << name << "A" << a << "/";
    if (b != -1) {
      finalName << "B" << b <<"/";
      if (c != -1) {
	finalName << "C" << c <<"/";
      }
    }
    name = finalName.str();
  }
  try {
    res = lsFile(name);
  } catch(const PixHistoServerExc &e) {throw;}
  std::stringstream stringd;
  stringd << d;
  for (size_t i=0; i<res.size(); i++) {
    if (res[i].compare(0,stringd.str().size(), stringd.str())==0 && (res[i].size()==stringd.str().size() || res[i][stringd.str().size()]==':')){
      finalName << res[i];
      pass = finalName.str();
      try {
	status = m_pixServer->ipc_getName(pass.c_str());
      } catch (const CORBA::TIMEOUT&) {
	ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_getName", "IPC timeout exception caught! "));
	throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
      }
    }
  }
  name = pass;
  //std::cout << "COMPLETE NAME IS: "<< name <<std::endl;
  return status;
}



bool PixHistoServerInterface::scanHistoBack(std::string name, int a, int b, int c, int d, OHRootHistogram &his) 
{
  bool status = false; 
  std::string ohName;
  try {
    ohName = scanHistoName(name,a,b,c,d);
    status = receiveHisto(ohName, his);
  } catch(const PixHistoServerExc &e) {throw;}
  return status; 
}


std::string PixHistoServerInterface::scanHistoName(std::string name, int a, int b, int c, int d) 
{
  bool status=false;

  status = scanHistoExist(name,a,b,c,d);
  if (status) std::cout << "PixHistoServerInterface::scanHistoName Success, with name = " << name << std::endl;
  return name;
}



bool PixHistoServerInterface::writeRootHistoServer(std::string inPath, std::string outPath, bool multiFile) 
{
  bool status=false;
  std::string info = "writeRootHistoServer";

  // Does the inPath exist?
  std::vector<std::string> checkFolderValidity;
  //std::vector<std::string>::iterator findInPath;
  std::string inPathToCheck = inPath.substr(1);
  try {
    checkFolderValidity = superLs("/");
  } catch(const PixHistoServerExc &e) {throw;}
  catch(...) {
    std::string message = "writeRootHistoServer - Unknown Exception throw ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, message));
  }
  for(size_t searchInPath=0; searchInPath<checkFolderValidity.size(); searchInPath++) {
    if (checkFolderValidity[searchInPath] == inPathToCheck) break;
    else if (searchInPath == checkFolderValidity.size()-1) {
      std::string message = inPath + " is not a valid path! ";
      ers::error(PixLib::pix::daq (ERS_HERE, info, message));
      return false; 
    }
  }
//   std::vector<std::string> countHisto;

//   try {
//     countHisto = superLs(inPath,true);
//   } catch(PixHistoServerExc e) {throw;}
//   catch(...) {
//     std::string message = "writeRootHistoServer - Unknown Exception throw ";
//     m_msg->publishMessage(PixMessages::ERROR, info, message);
//   }

//   std::stringstream histoNumberInfo;
//   histoNumberInfo << "In the file " <<  outPath 
// 		  << " there will be " << countHisto.size() << " histograms";
//   m_msg->publishMessage(PixMessages::INFORMATION, info, histoNumberInfo.str());

  try {
    ipc::PixRootFileWriter_var writer = m_ipcPartition->lookup<ipc::PixRootFileWriter>( "writer" );
    status = writer->ipc_writeRootHisto(inPath.c_str(), outPath.c_str(), multiFile);
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "ipc_writeRootHisto", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch (const daq::ipc::InvalidPartition&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NOIPC", "IPC server is unavailable ");
  } catch (const daq::ipc::InvalidObjectName&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVER", "The name of the nameServer is not valid ");
  } catch (const daq::ipc::ObjectNotFound&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVERPROBLEM", 
			    "nameServer does not exist or is not registered with the partition server.  ");
  }

  return status;
  
}



bool PixHistoServerInterface::reCreateOnLineRootFile(const std::string rootFileName)
{
  bool status;
  try {
    ipc::PixRootFileInMem_var reader = m_ipcPartition->lookup<ipc::PixRootFileInMem>( "reader" );
    status = reader->ipc_reCreateOnLineRootFile(rootFileName.c_str());
  } catch (const CORBA::TIMEOUT&) {
    ers::warning(PixLib::pix::daq (ERS_HERE, "reCreateOnLineRootFile", "IPC timeout exception caught! "));
    throw PixHistoServerExc(PixHistoServerExc::WARNING, "IPCTIMEOUT", "IPC timeout exception caught! ");
  } catch (const daq::ipc::InvalidPartition&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NOIPC", "IPC server is unavailable ");
  } catch (const daq::ipc::InvalidObjectName&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVER", "The name of the nameServer is not valid ");
  } catch (const daq::ipc::ObjectNotFound&) {
    throw PixHistoServerExc(PixHistoServerExc::FATAL, "NAMESERVERPROBLEM", 
			    "nameServer does not exist or is not registered with the partition server.  ");
  }
  return status;
} 



bool PixHistoServerInterface::nullName(std::string const & name)
{
  if (name.length() == 0) return true;
  else return false;
}


bool PixHistoServerInterface::noSlash(std::string const & name)
{
  if ((name.find("/")==std::string::npos 
       && name.find("/", name.find("/")+1)==std::string::npos)) return true;
  else return false;
}


bool PixHistoServerInterface::firstSlashMissing(std::string const & name)
{
  if (name.find("/") != 0) return true;
  else return false;
}


bool PixHistoServerInterface::lastSlashMissing(std::string const & name)
{
  if (name.length() > 2) { 
    if (name.find("/", name.length()-1) ==  std::string::npos) {
      return true;
    } else {
      return false;
    }
  } else if (name == "/") {
    return false;
  }
  return false;
}
