//////////////////////////////////////////////////////////////////////
// PixRootFileWriter.cxx
/////////////////////////////////////////////////////////////////////
//
// 14/07/09  Version 1.0 (NG)
//           
// 5/10/09   Last Modification (NG)


#include <unistd.h>
#include <exception>
#include <fstream>
#include <sstream>

#include <boost/thread/thread.hpp>

#include "PixHistoServer/PixHistoServerInterface.h"

#include "PixHistoServer/PixRootFileWriter.h"
#include "PixUtilities/PixISManager.h"
#include "PixUtilities/PixMessages.h"

#include <oh/OHIterator.h>

#include <is/serveriterator.h>

#include <TRegexp.h>

using namespace PixLib;

  
boost::mutex m_storageMutex; 
boost::mutex m_writerMutex;

  
void saveFile(PixRootFileWriter *my_writer, std::string folderToProcess, 
	      std::string rootFile, bool multiFile)
{

  std::cout << " Start the new thread "
	    << boost::this_thread::get_id() << " " << std::endl;

  my_writer->processData(folderToProcess, rootFile, multiFile);

  std::cout << " Stop thread "
	    << boost::this_thread::get_id() << " " << std::endl;
  
}



PixRootFileWriter::PixRootFileWriter(IPCPartition* ipcPartition, std::string ipcWriterName, std::string serverName) :
  IPCNamedObject<POA_ipc::PixRootFileWriter,ipc::multi_thread>(*ipcPartition, ipcWriterName.c_str()),
  m_serverName(serverName), m_isMan(NULL) 
{
  m_ipcPartition = new IPCPartition(*ipcPartition);
  try {
    publish();
  } catch (const daq::ipc::InvalidPartition&) {
    std::cout << "InvalidPartition " << std::endl;
    throw PixRootFileWriterExc(PixRootFileWriterExc::FATAL, "NOPARTITION", "The partition is not running ");
  } catch (const daq::ipc::InvalidObjectName&) {
    throw PixRootFileWriterExc(PixRootFileWriterExc::FATAL, "INVALIDOBJNAME", "The partition has an invalid name ");
    std::cout << "InvalidObjectName " << std::endl;
  }
  
  std::string provName = "PixRootFileWriter";
  try {
    m_hInt = new PixHistoServerInterface(m_ipcPartition, OHSERVERBASE+provName, 
					 m_serverName, PROVIDERBASE+provName);
  } catch (const PixHistoServerExc &e) {
    if (e.getId()=="NOIPC" || e.getId()=="NAMESERVER" || e.getId()=="NAMESERVERPROBLEM") {
      throw PixRootFileWriterExc(PixRootFileWriterExc::FATAL, e.getId(), e.getDescr()); 
    }
  } catch (...) {
    std::cout << "PixHistoServerInterface constructor thrown an unknown exception " << std::endl; 
  }
  if (ipcPartition->name() != ""){
    m_isMan = new PixISManager(ipcPartition->name(), "PixRootFileWriting");
  }
  m_writerStatus = -1;
  m_memStatusCounter = 0;
}


PixRootFileWriter::~PixRootFileWriter() 
{
  if (m_hInt) delete m_hInt;
  if (m_isMan) delete m_isMan;
  //  delete m_writerMutex;  // ??should I delete it?
  if (m_ipcPartition != 0) delete m_ipcPartition;
  // Withdraw from IPC partition
  //withdraw();
  IPCNamedObject<POA_ipc::PixRootFileWriter,ipc::multi_thread>::_destroy();
}



bool PixRootFileWriter::ipc_writeRootHisto(const char* inPath, const char* outPath, bool multiFile)
{
  // Update map m_storage
  // Update vector for queuing order
  // publish in IS the status of inPath
  // start the thread 
  
  boost::mutex::scoped_lock lock(m_storageMutex); 

  // std::cout << "Beginning of PixRootFileWriter::ipc_writeRootHisto" << std::endl;
  std::string resIn = inPath;
  std::string resOut = outPath;
  
//   std::cout << "The parameters passed via IPC are: "
//  	    << "resIn: " << inPath 
//  	    << "resOut: " << outPath 
// 	    << std::endl; 
  
  std::multimap<std::string,std::string>::iterator storageIterator;  
  std::pair<std::string, std::string> newEntry(resIn, resOut);
 // std::vector<std::string>::iterator orderMaker;
  
  // If the map and the vector mismatch
  if (m_storage.size() != m_storageOrder.size()){
    std::cout << "m_storage problem, throw an exception!" << std::endl;
    std::cout << "m_storage= " << m_storage.size() 
	      << "m_storageOrder.size()= " << m_storageOrder.size() 
	      << " throw an exception! " << std::endl;
    return false;
    // I want to add it at the end of the vector m_storageOrder
  } else {
    m_storage.insert(newEntry);
    m_storageOrder.push_back(resIn);
  }
  std::cout << "m_storage contains:\n";
  for (storageIterator=m_storage.begin(); storageIterator != m_storage.end(); 
       storageIterator++ ) {
    std::cout << (*storageIterator).first << " => " 
	      << (*storageIterator).second << std::endl;
  }
  std::cout << "and the order of execution is:\n";
  for (size_t i=0; i!=m_storageOrder.size(); i++) {
    std::cout << i << ": " 
	      << m_storageOrder[i] << std::endl;
  }
  
  if(m_isMan != 0){
    for (size_t publishPos=0; publishPos!=m_storageOrder.size(); publishPos++) {
      if (m_storageOrder[publishPos] == resIn) {
      	std::stringstream message;
	message << "IN THE QUEUE @ POS. " << publishPos << " ";
	m_isMan->publish(resIn.c_str(), message.str());
	break;
      } else if (publishPos == m_storageOrder.size()-1){
	m_isMan->publish(resIn.c_str(), "NOTHING ABOUT THIS");
	// I would throw an exception
	return false;
      }
    }
  } else {
    std::cout << "IS problem, I cannot publish! " << std::endl;
  }
  
  boost::thread thr(boost::bind(&saveFile,this,resIn,resOut,multiFile));
  
  return true;
}




void PixRootFileWriter::processData(std::string folderToProcess, std::string rootFile, bool multiFile) 
{
  // publish writer status inside the writerMutex, 
  // otherwise Writer-Status in IS is not sync
  // saveRootFile
  {
    boost::mutex::scoped_lock lock(m_writerMutex);
    std::string message = "PROCESSING " + rootFile + " ";
    if(m_isMan != 0) m_isMan->publish("Writer-Status", message);
    ers::info(PixLib::pix::daq (ERS_HERE, "PixRootFileWriter", message));
    saveRootHisto(folderToProcess, rootFile, multiFile);
    if(m_isMan != 0) m_isMan->publish("Writer-Status", "NOTHING TO DO");
  }
  
  // Update IS
  boost::mutex::scoped_lock lock(m_storageMutex);
  if(m_isMan != 0)  m_isMan->publish(folderToProcess, "PROCESSED");
}




void PixRootFileWriter::saveRootHisto(std::string inPath, std::string outPath, bool multiFile) 
{
  std::string realFileName = outPath;
  std::string newFileName;
  std::string mainFileName, logFileName;
  m_writerStatus = 0; // started
  publishWritingStatus(realFileName);
  m_memStatusCounter = 0;

  struct timeval t0; 
  struct timeval t1;
  //  struct timeval t2;
  //int time;

  std::string info = "saveRootHisto";
  std::string fileName;
  std::vector<std::string> trueSubFolders;

  // real limit 10000 
  long int maxHistoPerFile = 10000; 

  // count how many histograms have to be written
  long int totHisto = countHisto(inPath, outPath);

  // if the file will be more than 1, rename the first one
  if(multiFile && totHisto>maxHistoPerFile) {
    outPath = outPath.substr(0,outPath.length()-5) + "_0";
  }

  // Write the root file in the right location  
  std::string outputName = writeInTheRightPlace(outPath, fileName);

  // handle strings for the next files
  std::string mainRootFile = outputName.substr(0, outputName.length()-6);
  int fileCounter = 0;

  // Open the root file
  TFile *outFile = new TFile(outputName.c_str(), "UPDATE");
    if(outFile->IsZombie() ){
      PIX_ERROR("Cannot open "<<outputName<<" results cannot be saved");
      return;
    }
  //outFile->SetCompressionLevel(0);
  std::string mess = "Start writing "+realFileName;
  ers::info(PixLib::pix::daq (ERS_HERE, info, mess));

  // create the first directory
  createDirRootStruct(inPath);
  // craete the sub durs and fill the vector trueSubFolders
  fillDirRootStruct(outputName, inPath, trueSubFolders);
  // created dirs
  m_writerStatus = 1;
  publishWritingStatus(realFileName);
  
  // write histograms  
  my_rootReceiver pixelReceiver;
  std::string fileNameToUse = outputName;
  if (m_writerStatus != 2) {
    m_writerStatus = 2; // pushing histos
    publishWritingStatus(realFileName);
  }

  //time=
          (void)gettimeofday(&t0,NULL);
  std::string histogramWildCard = inPath+".*";
  ISServerIterator isIter(*m_ipcPartition);
  std::string isServerName;
  while(isIter++) {
    isServerName = isIter.name();
    if (isServerName.substr(0, 19) == std::string(OHSERVERBASE)) {
      OHHistogramIterator ith(*m_ipcPartition, isServerName, ".*", histogramWildCard);
      while(ith++) {
	//std::cout << "th.name() " << ith.name() << std::endl;
	int firstSlash = (ith.name()).find_first_of("/");
	int lastSlash  = (ith.name()).find_last_of("/");
	std::string positionFolder = (ith.name()).substr(firstSlash, lastSlash);
	try {
	  gDirectory->cd((fileNameToUse+":"+positionFolder).c_str());
	  if (multiFile) {
	    ith.retrieve(pixelReceiver);
	    //std::cout << " Retrieve " << ith.name() << std::endl;
	    m_memStatusCounter++;
	  } else {
	    std::string provName = ith.provider();
	    std::string histoName = ith.name();
	    OHRootHistogram his=OHRootReceiver::getRootHistogram(*m_ipcPartition, isServerName, provName, histoName, -1);
            his.histogram->ResetBit(kMustCleanup);
	    his.histogram->Write(); 
	  }
	} catch (const daq::oh::ObjectNotFound&) {
	  //std::string mess = "Object not found in OH ";
	  std::cout << "Object not found in OH " << std::endl;
	} catch (const daq::oh::InvalidObject&) {
	  //std::string mess = "Invalid Object ";
	  std::cout << " Invalid Obj " << std::endl;
	} catch (...) {
	  //std::string mess = "Unknown exception thrown ";
	}
	if( multiFile && 
	    m_memStatusCounter%maxHistoPerFile == 0 && m_memStatusCounter !=totHisto) { 
	  outFile->Write();
	  std::stringstream histoNumberInfo;
	  histoNumberInfo << "In the file " << realFileName << " " 
			  << m_memStatusCounter << " histograms have been already written " ;
	  ers::info(PixLib::pix::daq (ERS_HERE, info, histoNumberInfo.str()));
	  outFile->Close();
	  
	  delete outFile;
	  fileCounter++;
	  std::stringstream nextRootFile;
	  nextRootFile << mainRootFile << fileCounter;
	  newFileName = nextRootFile.str() + ".root";
	  
	  outFile = new TFile(newFileName.c_str(),"UPDATE");
	    if(outFile->IsZombie() ){
              PIX_ERROR("Cannot open "<<outputName<<" results cannot be saved");
              return;
            }
	  //outFile->SetCompressionLevel(0);
	  createDirRootStruct(inPath);
	  //trueSubFolders.clear();
	  std::vector<std::string> trueSubFoldersBis;
	  fillDirRootStruct(newFileName, inPath, trueSubFoldersBis);
	  nextRootFile.str(std::string());
	  fileNameToUse = newFileName;
	}
      } // end while(ith++)
    }
  } 
  
  //  time=gettimeofday(&t2,NULL);   
  if(multiFile) outFile->Write();
  outFile->Close();
  //time=
          (void)gettimeofday(&t1,NULL);
  
  m_writerStatus = 3; // Done
  publishWritingStatus(realFileName);
  
//   std::cout << "The writer spent: " 
// 	    << (t2.tv_sec - t0.tv_sec + 1e-6*(t2.tv_usec-t0.tv_usec) ) 
// 	    << " s for retrieving the histograms from IS " << std::endl;

  std::cout << "The writer spent: " 
	    << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) 
	    << " s for executing saveRootHisto(" << inPath 
	    << ", "<< realFileName << ", "<< multiFile << ") " << std::endl;
  
  std::stringstream message;
  message << "Writing of " << realFileName << " completed in " 
	  << (t1.tv_sec - t0.tv_sec + 1e-6*(t1.tv_usec-t0.tv_usec) ) << " s. ";
  ers::log(PixLib::pix::daq (ERS_HERE, info, message.str()));
  
  if (multiFile && totHisto>maxHistoPerFile) {
    
    std::vector<std::string> listOfSources;
    for (int i=0; i<fileCounter+1; i++) {
      std::stringstream subRootFile;
      subRootFile << mainRootFile << i << ".root ";
      listOfSources.push_back(subRootFile.str());
      //      subRootFile.str().clear();
    }
    std::string rootName = realFileName.substr(0, realFileName.length()-5);
    //    std::cout << "rootName: " << rootName << std::endl;
    //logFileName = rootName + ".out";
    // logFileName location is missing!
    mainFileName = writeInTheRightPlace(rootName, realFileName);
    TFile *FileTarget = TFile::Open(  mainFileName.c_str(), "RECREATE" );
    TDirectory* targetDir = gDirectory;
    TFile *FileSource=nullptr;
      for(size_t i=0; i<listOfSources.size(); i++) {
        FileSource = TFile::Open(listOfSources[i].c_str()); 
        TDirectory* sourceDir = gDirectory;
        FileSource->ReadAll();
        TList* list = FileSource->GetList();
        mergeFiles(list, targetDir, sourceDir);
        std::cout << "SourceFile" <<i<< ": "<<listOfSources[i]<<std::endl;
        FileSource->Close();
      }
    FileTarget->Close();

    copyRootFile(mainFileName,  realFileName+".root");
  } else {
  // just copy the file in the location to be transferred to castor
  copyRootFile(outputName, fileName);
  }

  m_writerStatus = -1;

  /*
  std::cout << " ***** " << std::endl;
  std::cout << "inPath: " << inPath << std::endl;
  std::cout << "outPath: " << outPath << std::endl;
  std::cout << "realFileName: " << realFileName << std::endl;
  std::cout << "outputName: " << outputName << std::endl;
  std::cout << "fileNameToUse: " << fileNameToUse << std::endl;
  std::cout << "fileName: " << fileName << std::endl;
  std::cout << "newFileName: " << newFileName << std::endl;
  std::cout << "logFileName: " << logFileName << std::endl;
  std::cout << "mainRootFile: " << mainRootFile << std::endl;
  std::cout << "mainFileName: " << mainFileName << std::endl;
  */
  
}


void PixRootFileWriter::mergeFiles(const TList* list, TDirectory* targetDir, TDirectory* sourceDir){
  TIter next(list);
  TObject* obj=nullptr;
  while ((obj = next())) {
    TString className = obj->IsA()->GetName();
//     std::cout << "Processing " << obj->GetName() 
// 	      << " class " << className << std::endl;
    if (className == "TDirectory" || className == "TDirectoryFile") {
      //     gDirectory->pwd();
      if (!targetDir->mkdir(obj->GetName())) {
	//	std::cout << "newDir is null" << std::endl;
	if ( !targetDir->cd(obj->GetName()) ) {
	  PIX_WARNING("Error! Folder " << obj->GetName() << " can't be opened!");
	}
      } else {
	//	std::cout << "Created the folder" << std::endl;
	targetDir->cd(obj->GetName());
      }
      TDirectory* newSourceDir =  sourceDir->GetDirectory(obj->GetName());
      newSourceDir->ReadAll();
      TList *nextList = newSourceDir->GetList();
      mergeFiles(nextList, gDirectory, newSourceDir);
//       targetDir->pwd();
//       sourceDir->pwd();
//       gDirectory->pwd();
    } else if (className=="TH2F" || className=="TH2D" 
	       || className=="TH1F" || className=="TH1D") {
      obj->Write();
      delete obj;
    }
  }
}

bool PixRootFileWriter::createDirRootStruct(std::string inPath)
{
  size_t pos=inPath.find("/", 1);
  inPath = inPath.substr(1); // cut away leading slash
  pos=inPath.find("/"); // sub-folder to create
  
  while ( pos != std::string::npos ) {
    std::string folder = inPath.substr(0, pos);
    TDirectory* newDir = gDirectory->mkdir(folder.c_str());
    if (!newDir) {
      if ( !gDirectory->cd(folder.c_str()) ) {
	std::cout << "Error! Folder " << folder << " can't be opened!" << std::endl;
	return false;
      }
    } else {
      newDir->cd();
    }
    if (pos+1 > inPath.length()-1) return true; 

    inPath = inPath.substr(pos+1);
    pos=inPath.find("/");
  }
  return true;
}



void PixRootFileWriter::publishWritingStatus(std::string fileName) 
{
  if(m_isMan != 0){
    switch (m_writerStatus)
      {
      case 0: 
	m_isMan->publish(fileName.c_str(), "STARTED");
	break;
      case 1: 
	m_isMan->publish(fileName.c_str(), "CREATED DIRECTORY STRUCTURE");
	break;
      case 2: 
	m_isMan->publish(fileName.c_str(), "PUSHING HISTO(S)");
	break;
      case 3: 
	m_isMan->publish(fileName.c_str(), "DONE");
	break;
      }
  }
  
}



void PixRootFileWriter::fillDirRootStruct(std::string outputName, std::string inPath, std::vector<std::string> &trueSubFolders)
{

  std::vector<std::string> groupFile;
  std::vector<std::string> groupSubFolders;
  
  groupFile = m_hInt->superLs(inPath, true);
  
  std::string firstPath;
  std::string secondPath;
  gDirectory->cd((outputName+":"+inPath).c_str());
  for (unsigned j=0; j<groupFile.size(); j++) {
    std::string folders=parsingFolders(groupFile[j]);
    groupSubFolders.push_back(folders);
  }
  // create the first one
  createDirRootStruct("/"+groupSubFolders[0]);
  gDirectory->cd((outputName+":"+inPath).c_str());
  trueSubFolders.push_back(groupSubFolders[0]); 

  // create the others
  size_t i=0;
  while (i<groupSubFolders.size()-1) {
    firstPath = groupSubFolders[i];
    secondPath = groupSubFolders[i+1];
    if ( firstPath == secondPath ) {
      i++;
    } else {
      trueSubFolders.push_back(secondPath);
      std::string diffPath="";
      std::string equalPath="";
      if (compare (firstPath, secondPath, diffPath, equalPath)) {
	gDirectory->cd((outputName+":"+inPath+equalPath).c_str());
	createDirRootStruct("/"+diffPath);
      } else {
	gDirectory->cd((outputName+":"+inPath).c_str());
	createDirRootStruct("/"+secondPath);
      }
      i++;
    }
  }

}



std::string PixRootFileWriter::writeInTheRightPlace(std::string outPath, std::string& fileName)
{
  std::string outputPath = "";
  std::string outputName;
  char *ope = getenv("PIXSCAN_STORAGE_PATH_EXP");
  if (ope != NULL) {
    outputPath = ope;
    outputPath += "/";
  } else {
    char *op = getenv("PIXSCAN_STORAGE_PATH");
    if (op != NULL) {
      outputPath = op;
      outputPath += "/";
    }
  }
  size_t slp = outPath.find_last_of("/");
  if (slp != std::string::npos) {
    fileName = outPath.substr(slp+1);
    if (outputPath == "") {
      outputPath = outPath.substr(0,slp+1);
    }
  } else {
    fileName = outPath;
  }
  outputName = outputPath + fileName;
  std::string root = outputName.substr(outputName.length()-5);
  if (root != ".root") {
    outputName = outputName + ".root";
  }
  return outputName;
}



void PixRootFileWriter::copyRootFile(std::string outputName, std::string fileName)
{
  std::string exportPath = "";
  std::string exportPathE = "";
  std::string info = "copyRootFile";

  char *epe = getenv("PIXSCAN_STORAGE_PATH_EXP");
  if (epe != NULL) {
    exportPathE = epe;
    exportPathE += "/";
  }
  if (exportPathE != "") {
    char *ep = getenv("PIXSCAN_STORAGE_PATH");
    if (ep != NULL) {
      exportPath = ep;
      exportPath += "/";
    }
    if (exportPath != "") {

      // Old PIT file server 
      //std::string cmd = "/bin/cp "+outputName+" "+exportPath+fileName+".writing";
      //int b1 = system(cmd.c_str());
      //cmd = "/bin/mv "+exportPath+fileName+".writing "+exportPath+fileName;
      //int b2 = system(cmd.c_str());
      // New PIT file server 
      //std::string cmd = "/bin/mv "+outputName+" "+exportPath+fileName;
      //int b1 = system(cmd.c_str());
      //int b2 = 0;
      if (std::rename(outputName.c_str(), std::string(exportPath+fileName).c_str()) ) { // && b2==0) {
        std::string mess = "Successfully copied root file "+fileName+" to "+exportPath;
        ers::info(PixLib::pix::daq (ERS_HERE,"saveRootFile", mess));
      } else {
        std::string mess = "Error copying root file "+fileName+" to "+exportPath;
        ers::error(PixLib::pix::daq (ERS_HERE, info, mess));
      }
    }
  }
}



long int  PixRootFileWriter::countHisto(std::string inPath, std::string outPath)
{
  std::vector<std::string> countHisto;
  std::string info = "countHisto";
  try {
    countHisto = m_hInt->superLs(inPath,true);
  } catch(const PixHistoServerExc &e) {throw;}
  catch(...) {
    std::string message = "countHisto - Unknown Exception throw ";
    ers::error(PixLib::pix::daq (ERS_HERE, info, message));
  }
   std::stringstream histoNumberInfo;
   histoNumberInfo << "In the file " <<  outPath 
 		  << " there will be " << countHisto.size() << " histograms";
   ers::info(PixLib::pix::daq (ERS_HERE, info,histoNumberInfo.str()));
   return countHisto.size();
}
