//////////////////////////////////////////////////////////////////////
// PixNameServer.cxx
/////////////////////////////////////////////////////////////////////
//
// 19/09/07  Version 1.0 (NG)
//           
// 5/10/09   Last Modification (NG)


#include <unistd.h>
#include <exception>
#include <iostream>
#include <fstream>
#include <sstream>

#include "PixHistoServer/PixNameServer.h"


using namespace PixLib;


PixNameServer::PixNameServer(IPCPartition *ipcPartition, std::string nameServer) :
  IPCNamedObject<POA_ipc::PixNameServer,ipc::multi_thread>(*ipcPartition, nameServer.c_str()), 
  m_nameServer(nameServer) {
  m_ipcPartition = new IPCPartition(*ipcPartition);
  try {
    publish();
  } catch (const daq::ipc::InvalidPartition&) {
    std::cout << "InvalidPartition " << std::endl;
    throw PixNameServerExc(PixNameServerExc::FATAL, "NOPARTITION", "The partition is not running ");
  } catch (const daq::ipc::InvalidObjectName&) {
    std::cout << "InvalidObjectName " << std::endl;
    throw PixNameServerExc(PixNameServerExc::FATAL, "INVALIDOBJNAME", "The partition has an invalid name ");
  }
  
  std::string provName = "helper";
}


PixNameServer::~PixNameServer() 
{
  if (m_ipcPartition != 0) delete m_ipcPartition;
  // Withdraw from IPC partition
  //withdraw();
  IPCNamedObject<POA_ipc::PixNameServer,ipc::multi_thread>::_destroy();
}


MY_LONG_INT PixNameServer::ipc_putName(const char* name)
{
  try {
    fillStruct(m_home, name, name);
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "FILLSTRUCT1") {
      std::cout << e.getDescr() << std::endl;
      return 1;
    } else if (e.getId() == "FILLSTRUCT2") {
      std::cout << e.getDescr() << std::endl;
      return 2;
    } 
  }
  return 0;
}


bool PixNameServer::ipc_getName(const char* name) 
{
  return readStruct(m_home, name);
}


MY_LONG_INT PixNameServer::ipc_numberHisto(const char* name) {
  size_t ll=0;
  std::string path = name;
  size_t a = path.find("/");
  size_t check = path.length();
  
  if (check==0) {
    std::cout << "Invalid name" << std::endl;
  } else if (a==std::string::npos) {
    std::cout << "It's a file, not a folder!" << std::endl;
    throw PixNameServerExc(PixNameServerExc::WARNING, "NONUMBERHISTO", "The folder "+path+" is not a folder, but a file");
  } else if (a==0) {
    std::cout << "Open the folder " << name << std::endl;
    try {
      ll = (openFolder(m_home,path,path, false)).size();
    } catch (const PixNameServerExc &e) {
      if (e.getId() == "NOFOLDER") {throw;}
      if (e.getId() == "NOHOME") {throw;}
    } catch(...) {
      std::cout << "ipc_numberHisto - Unknown Exception was thrown! " << std::endl;
    }
  }  
  return ll;
  
}


MY_LONG_INT PixNameServer::ipc_lsFile(const char* name, ipc::ipc_string_vect_out resultFile) 
{
  MY_LONG_INT testFile=0;
  size_t ll;
  std::string path = name;

  try {
    // Open the folder
    std::vector<std::string> searchFile(openFolder(m_home,path,path, false));
    ll = searchFile.size();
    resultFile = new ipc::ipc_string_vect(ll);
    resultFile->length(ll);
    if (ll!=0) {
      for (size_t i=0; i<ll; i++) {
	resultFile[i].str = CORBA::string_dup(searchFile[i].c_str()); 
      }
      testFile=1;
    } else testFile=0;
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "NOHOME") {
      throw ipc::no_home();
    } else if (e.getId() == "NOFOLDER") {
      std::cout << e.getDescr() << std::endl; // 6 Oct 09 - to be deleted 
      throw ipc::no_folder();
    }
  }
  return testFile;
}


MY_LONG_INT PixNameServer::ipc_lsFolder(const char* name, ipc::ipc_string_vect_out resultFolder) 
{
  MY_LONG_INT testFolder=0;
  size_t L=0;
  std::string path = name;
  
  try {
    std::vector<std::string> searchFolder(openFolder(m_home, path, path, true));
    L = searchFolder.size();
    resultFolder = new ipc::ipc_string_vect(L);
    resultFolder->length(L);
    if (L!=0) {
      for (size_t i=0; i<L; i++) {
	resultFolder[i].str = CORBA::string_dup(searchFolder[i].c_str()); 
      }
      testFolder=1;
    } else testFolder=0;
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "NOHOME") {
      throw ipc::no_home();
    } else if (e.getId() == "NOFOLDER") {
      std::cout << e.getDescr() << std::endl; // 6 Oct 09 - to be deleted 
      throw ipc::no_folder();
    }
  }
  return testFolder;
}


std::vector<std::string> PixNameServer::superLs(const char* name, std::string start, bool scanHisto) 
{
  std::string path = name;
  size_t L = start.length();
  std::vector<std::string> pass;
  std::vector<std::string> results;
  
  try {
    std::vector<std::string> searchFile(openFolder(m_home,path,path, false));
    for(size_t i=0; i<searchFile.size(); i++) {
      std::string p = path+searchFile[i];
      std::string final = p.substr(L,p.length());
      results.push_back(final);
    }
    std::vector<std::string> searchFolder(openFolder(m_home,path,path, true));
    for(size_t j=0; j<searchFolder.size(); j++) {
      pass.push_back(path+searchFolder[j]);
      std::string f = pass[j].substr(L,pass[j].length());
      if (!scanHisto) results.push_back(f);
    }
    for(size_t k=0; k<pass.size(); k++) {
      std::vector<std::string> newResults = superLs(pass[k].c_str(), start, scanHisto);
      for (size_t j=0; j<newResults.size(); j++) {
	results.push_back(newResults[j]);
      }
    }
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "NOFOLDER") {throw;}
    if (e.getId() == "NOHOME") {throw;}
  } catch(...) {
    std::cout << "superLs - Unknown Exception was thrown! " << std::endl;
  }
  return results;
}


MY_LONG_INT PixNameServer::ipc_superLs(const char* name, ipc::ipc_string_vect_out ipcResults, bool scanHisto) 
{
  MY_LONG_INT test=0;
  std::string start = name;
  
  try {
    std::vector<std::string> results(superLs(name, start, scanHisto));
    size_t resultsSize = results.size();
    ipcResults = new ipc::ipc_string_vect(resultsSize);
    ipcResults->length(resultsSize);
    if (resultsSize!=0) {
      for (size_t i=0; i<resultsSize; i++) {
	ipcResults[i].str = CORBA::string_dup(results[i].c_str());
      }
      test=1;
      // Nothing inside name 
    } else test=0;
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "NOHOME") {
      throw ipc::no_home();
    } else if (e.getId() == "NOFOLDER") {
      std::cout << e.getDescr() << std::endl; // 6 Oct 09 - to be deleted 
      throw ipc::no_folder();
    }
  }
  return test;
}



std::vector<std::string> PixNameServer::openFolder(mem &MyStruct, std::string name, std::string path, bool type) 
{
  size_t resultsSize = name.length();
  std::map<std::string, mem>::iterator res;
  std::map<std::string, std::string>::iterator resFile;
  size_t inia = name.find("/");
  size_t inib = name.find("/", inia+1);
  size_t a = path.find("/");
  
  std::vector<std::string> searchFile;
  std::vector<std::string> searchFolder;
  
  // if you are looking in the main folder
  if (name == "/") {
    std::map<std::string, mem>::iterator it;
    {
      OWLMutexRLock rLock(m_mutex);
      it = m_home.folder.find(name);
    }
    if(it != m_home.folder.end()) {
      if(type) {
	for (res=(*it).second.folder.begin(); res!=(*it).second.folder.end(); ++res) {
	  searchFolder.push_back((*res).first);
	}
      } else {
	for (resFile=(*it).second.file.begin(); resFile!=(*it).second.file.end(); ++resFile) {
	  searchFile.push_back((*resFile).first);
	}
      }
    } else {
      if (searchFile.size()!=0) searchFile.clear();
      if (searchFolder.size()!=0) searchFolder.clear();
      throw PixNameServerExc(PixNameServerExc::ERROR, "NOHOME", "m_home doesn't exist ");
    }
    // if you have a more complex name
  } else if (path != "/") {
    // first turn
    if(a==0) {
      std::map<std::string, mem>::iterator it;
      {
	OWLMutexRLock rLock(m_mutex);
	it = m_home.folder.find("/");
      }
      if(it != m_home.folder.end()) {
	std::string ss = name.substr(inia+1,inib);
	std::map<std::string, mem>::iterator inside;
	{
	  OWLMutexRLock rLock(m_mutex);
	  inside = (*it).second.folder.find(ss);
	}
	if (inside != (*it).second.folder.end()) {
	  if (inib != resultsSize-1) {
	    std::string ps=name.substr(inib+1,resultsSize-inib+1);
	    try {
	      std::vector<std::string> retList = openFolder((*inside).second, name, ps, type);
	      if (type) searchFolder = retList;
	      else searchFile = retList;
	    } catch (const PixNameServerExc &e) {
	      if (e.getId() == "NOFOLDER") { throw; }
	      if (e.getId() == "NOHOME") { throw; }
	    } catch (...) { throw; }
	  } else {
	    if(type) {
	      for (res=(*inside).second.folder.begin(); res!=(*inside).second.folder.end(); ++res) {
		searchFolder.push_back((*res).first);
	      }
	    } else {
	      for (resFile=(*inside).second.file.begin(); resFile!=(*inside).second.file.end(); ++resFile) {
		searchFile.push_back((*resFile).first);
	      }
	    }
	  }
	} else {
 	  if (searchFile.size()!=0) searchFile.clear();
 	  if (searchFolder.size()!=0) searchFolder.clear();
	  throw PixNameServerExc(PixNameServerExc::ERROR, "NOFOLDER", "The folder "+ss+" doesn't exist ");
	}
      } else {
 	if (searchFile.size()!=0) searchFile.clear();
	if (searchFolder.size()!=0) searchFolder.clear();
 	throw PixNameServerExc(PixNameServerExc::ERROR, "NOHOME", "The folder / doesn't exist in the server.");
      }
      // further runs
    } else {
      size_t ll = path.length();
      size_t b = path.find("/", a+1);
      std::string s = path.substr(0, a+1);
      std::map<std::string, mem>::iterator itt;
      {
	OWLMutexRLock rLock(m_mutex);
	itt = MyStruct.folder.find(s);
      }
      if (itt != MyStruct.folder.end()) {
	if (b==std::string::npos) {
	  if (type) {
	    for (res=(*itt).second.folder.begin(); res!=(*itt).second.folder.end(); ++res) {
	      searchFolder.push_back((*res).first);
	    }
	  } else {
	    for (resFile=(*itt).second.file.begin(); resFile!=(*itt).second.file.end(); ++resFile) {
	      searchFile.push_back((*resFile).first);
	    }
	  }
	} else {
	  std::string ns=path.substr(a+1,ll-a+1);
	  try {
	    std::vector<std::string> retList = openFolder((*itt).second, name, ns, type);
	    if (type) searchFolder = retList;
	    else searchFile = retList;
	  } catch (const PixNameServerExc &e) {
	    if (e.getId() == "NOFOLDER") {throw;}
	    if (e.getId() == "NOHOME") {throw;}
	  } catch (...) {throw;}
	}
      } else {
 	if (searchFile.size()!=0) searchFile.clear();
	if (searchFolder.size()!=0) searchFolder.clear();
	throw PixNameServerExc(PixNameServerExc::ERROR, "NOFOLDER", "The folder "+s+" doesn't exist ");
      }
    }
  }
  return type ? searchFolder : searchFile;
}



void PixNameServer::fillStruct(mem &MyStruct, std::string name, std::string path) 
{
  int nameLength = name.length();
  size_t inia = name.find("/");
  size_t a = path.find("/");
  std::string ps;
  std::string nf;

  // If the name is valid
  if (inia !=std::string::npos) {
    // I have to check if it's the first time that you publish smething in the server
    // If yes, you have to save the home == /
    if (a==0) {
      std::string start = "/";
      std::map<std::string, mem>::iterator it;
      {
	OWLMutexRLock rLock(m_mutex);
	it = MyStruct.folder.find(start);
      }
      if (it == MyStruct.folder.end()) {
	mem newFolder;
	std::pair<std::string, mem>myInit(start, newFolder);
	{
	  OWLMutexWLock wLock(m_mutex);
	  MyStruct.folder.insert(myInit);
	}
	ps = name.substr(inia+1, nameLength-inia+1);
	{
	  OWLMutexRLock rLock(m_mutex);
	  it = MyStruct.folder.find(start);
	}
	fillStruct((*it).second, name, ps);
      } else {
	ps = name.substr(inia+1, nameLength-inia+1);
	{
	  OWLMutexRLock rLock(m_mutex);
	  it = MyStruct.folder.find(start);
	}
	fillStruct((*it).second, name, ps);
      }
    } else if (a!=0 && a!=std::string::npos) {
      std::string sf = path.substr(0,a+1);
      std::map<std::string, mem>::iterator itt;
      {
	OWLMutexRLock rLock(m_mutex);
	itt = MyStruct.folder.find(sf);
      }
      // If This Sub Folder doesn't exist I create it
      if (itt == MyStruct.folder.end()) {
	mem newSubFolder;
	std::pair<std::string, mem>myInit(sf, newSubFolder);
	{
	  OWLMutexWLock wLock(m_mutex);
	  MyStruct.folder.insert(myInit);
	}
	nf = path.substr(a+1, path.length()-a+1);
	{
	  OWLMutexRLock rLock(m_mutex);
	  itt = MyStruct.folder.find(sf);
	}
	fillStruct((*itt).second, name, nf);
	// If this Sub Folder exists, I open it
      } else {
	nf = path.substr(a+1, path.length()-a+1); 
	{
	  OWLMutexRLock rLock(m_mutex);
	  itt = MyStruct.folder.find(sf);
	}
	fillStruct((*itt).second, name, nf);
      }
      // If you have a file
    } else if (a==std::string::npos) {
      std::map<std::string, std::string>::iterator ittt;
      {
	OWLMutexRLock rLock(m_mutex);
	ittt = MyStruct.file.find(path);
      }
      // If this File doesn't exist I create it, otherwise I go on...
      if (ittt == MyStruct.file.end()) {
	std::pair<std::string, std::string> myFile(path, path);
	{
	  OWLMutexWLock wLock(m_mutex);
	  MyStruct.file.insert(myFile);
	}
      }
    } else {
      throw PixNameServerExc(PixNameServerExc::ERROR, "FILLSTRUCT2", "The folder does not contain any files ");
    }
  } else {
    throw PixNameServerExc(PixNameServerExc::ERROR, "FILLSTRUCT1", "The name of the histogram is not valid ");
  }
}



bool PixNameServer::readStruct(mem MyStruct, std::string path) 
{
  bool status=false;
  size_t a = path.find("/");
  size_t b = path.find("/", a+1);
  size_t nameLength = path.length();

  // if you have a main folder
  if (a==0 && b!=nameLength-1) {
    std::string start = path.substr(a, a+1);
    std::map<std::string, mem>::iterator res;
    {
      OWLMutexRLock rLock(m_mutex);
      res = MyStruct.folder.find(start);
    }
    if (res!=MyStruct.folder.end()) {
      std::string next = path.substr(a+1, nameLength);
      status=readStruct((*res).second, next);
    } else return false;
    
    // if you have just a file
  } else if (a==std::string::npos && b==std::string::npos) {
    std::map<std::string, std::string>::iterator r;
    {
      OWLMutexRLock rLock(m_mutex);
      r = MyStruct.file.find(path);
    }
    if (r!=MyStruct.file.end()) {
      status = true;
    } else status = false;
    
    // if you are looking in a SubFolder
  } else if ( a!=0 )  {
    std::string p = path.substr(0, a+1);
    std::map<std::string, mem>::iterator lf;
    {
      OWLMutexRLock rLock(m_mutex);
      lf = MyStruct.folder.find(p);
    }
    if (lf!=MyStruct.folder.end()) {
      std::string name = path.substr(a+1,nameLength);
      status=readStruct((*lf).second,name);
    } else status = false;
  }
  return status; 
}



bool PixNameServer::ipc_removeHisto(const char* name) 
{
  // Convention: name has to be the whole path, starting from /
  return removeHisto(m_home, name);
}



bool PixNameServer::removeHisto(mem &MyStruct, std::string path) 
{
  bool status=true;
  size_t a = path.find("/");
  size_t b = path.find("/", a+1);
  size_t nameLength = path.length();

  // main folder
  if (a==0 && b!=nameLength-1) {
    std::string start = path.substr(a, a+1);
    std::map<std::string, mem>::iterator objIterator;
    {
      OWLMutexRLock rLock(m_mutex);
      objIterator = MyStruct.folder.find(start);
    }
    if (objIterator!=MyStruct.folder.end()) {
      std::string withoutFirstFolder = path.substr(a+1, nameLength);
      status=removeHisto((*objIterator).second, withoutFirstFolder);
    } else {
      return false;
    }

    // file in the main folder
  } else if (a==std::string::npos && b==std::string::npos) {
    std::map<std::string, std::string>::iterator fileIt;
    std::string fileName = path.substr(a+1, nameLength);
    {
      OWLMutexRLock rLock(m_mutex);
      fileIt = MyStruct.file.find(fileName);
    }
    if (fileIt!=MyStruct.file.end()) {
      {
	OWLMutexWLock wLock(m_mutex);
	MyStruct.file.erase(fileIt);
      }
      status = true;
    } else status = false;

    // subfolders 
  } else if ( a!=0 )  {
    std::string reducedPath = path.substr(0, a+1);
    std::map<std::string, mem>::iterator subFoldIt;
    {
      OWLMutexRLock rLock(m_mutex);
      subFoldIt = MyStruct.folder.find(reducedPath);
    }
    if (subFoldIt!=MyStruct.folder.end()) {
      // if you have parsed the whole folder struct
      if (b == std::string::npos) { 
	std::string fileName = path.substr(a+1, nameLength);
	std::map<std::string, std::string>::iterator fileIt;
	{
	  OWLMutexRLock rLock(m_mutex);
	  fileIt = (*subFoldIt).second.file.find(fileName);
	}
	if (fileIt!=(*subFoldIt).second.file.end())
	  {
	    OWLMutexWLock wLock(m_mutex);
	    (*subFoldIt).second.file.erase(fileIt);
	  }
	status = true;
	// you have still to open the structure
      } else { 
	std::string nextPathName = path.substr(a+1,nameLength);
	status=removeHisto((*subFoldIt).second, nextPathName);
      }
    }
  }
  return status; 
}



MY_LONG_INT PixNameServer::ipc_removeTree(const char* rootName)
{
  //  std::cout << "Beginning of PixNameServer::ipc_removeTree" << std::endl;
  bool test=false;
  try {
    test = removeTree(m_home, rootName);
  } catch (const PixNameServerExc &e) {
    if (e.getId() == "MAINFOLDER") {      
      std::cout << e.getDescr() << std::endl;
      return 1;
    } else if (e.getId() == "SUBFOLDER") {
      std::cout << e.getDescr() << std::endl;
      return 2;
    }
  } catch (...) {
    std::cout << "Unknown exception was thrown in PixNameServer::ipc_removeTree" << std::endl;
    return 5;
  }
  return test ? 0 : 1;
}



bool PixNameServer::removeTree(mem &MyStruct, std::string rootName) 
{
  bool status=true;
  size_t a = rootName.find("/");
  size_t nameLength = rootName.length();

  // Remove all - rootName = /
  if (a==0 && nameLength==1) {
    std::map<std::string, mem>::iterator searchRootName;
    {
      OWLMutexRLock rLock(m_mutex);
      searchRootName = MyStruct.folder.find(rootName);
    } 
    if (searchRootName!=MyStruct.folder.end()) {
      {
	OWLMutexWLock wLock(m_mutex);
	MyStruct.folder.clear();
	MyStruct.file.clear();
      }
      status = true;
    } else {
      throw PixNameServerExc(PixNameServerExc::ERROR, "MAINFOLDER", "The main folder, home, is not existing ");
    }

    // if you have a folder structure starting from home, i.e. rootName starts with /
  } else if (a==0 && nameLength!=1) {
    std::string start = rootName.substr(a, a+1);
    std::map<std::string, mem>::iterator res;
    {
      OWLMutexRLock rLock(m_mutex);
      res = MyStruct.folder.find(start);
    }
    if (res!=MyStruct.folder.end()) {
      std::string withoutFirstFolder = rootName.substr(a+1, nameLength);
      status=removeTree((*res).second, withoutFirstFolder);
    } else {
      throw PixNameServerExc(PixNameServerExc::ERROR, "MAINFOLDER", "The main folder, home, is not existing ");
    }

    // subfolders
  } else if (a!=0) {
    std::string next = rootName.substr(0, a+1);
    std::map<std::string, mem>::iterator it;
    {
      OWLMutexRLock rLock(m_mutex);
      it = MyStruct.folder.find(next);
    }
    if (it!=MyStruct.folder.end()) {
      // if you have parsed the whole name
      if (a+1 == nameLength) { 
	{
	  OWLMutexWLock wLock(m_mutex);
	  (*it).second.folder.clear();
	  (*it).second.file.clear();
	  MyStruct.folder.erase(it);
	}
	status = true;
	// you have still to open the structure
      } else { 
	std::string nextName = rootName.substr(a+1,nameLength);
	status=removeTree((*it).second, nextName);
      }
    } else {
      throw PixNameServerExc(PixNameServerExc::ERROR, "SUBFOLDER", (*it).first + " not found ");
    }
  }
   return status; 
}



