/////////////////////////////////////////////////////////////////////
// TimPixTrigController.cxx
/////////////////////////////////////////////////////////////////////
// 29/08/07  Revision 1.7 (II)
//           update to match new trigger oscillator in firmware 19
//           allow internal triggering of sequencer
//           include BCID Offset
// 23/11/06  Revision 1.6 (II)
//           Enable output parallel bus of TTCrx chip in TTC mode
//           at initialisation to accept TType
//           disable automatic setting RodBusy in SA-mode
// 27/10/06  Revision 1.4 (II)
//           disable FFTVeto at the moment of TIM initialisation
//           implementation of timTriggerMode (TTC & VME, similar to that of TimModule)
//           timIOmode expansion to accept external CAL 
//           integration of IntTrigger & ECR frequency settings from TimModule
//           singleCal method 
//
// 16/07/04  Version 1.0 (GG RB)
//           Initial release
//

//! Implementation of PixTriggerController in raw mode 

#include <iostream>
#include <fstream>
#include "Config/Config.h"
#include "RCCVmeInterface.h"
//#include "RodCrate/TimModule.h"
//#include "RodCrate/TimDefine.h"
#include "PixTrigController/TimPixTrigController.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"

#include "PixUtilities/PixISManager.h" //II

using namespace PixLib;
using namespace SctPixelRod;

//   SctPixelRod::TimModule *m_tim;     //! Pointer to TimModule
//   int m_timSlot;                     //! Tim Slot
//   std::string m_mode;                //! Tim Operating Mode
//   int m_triggerFrequency;            //! internal trigger frequency
//   Config *m_conf;                    //! Tim Configuration structure


TimPixTrigController::TimPixTrigController(std::string name) : 
  m_dbRecord(NULL), m_mode("instantiated"), m_triggerFrequency(TIM_MASK_TRIG_100_KHZ), m_triggerDelay(0), m_clockDelay(0), 
  m_conf(NULL) 
 {
   m_tim = NULL;
   m_name=name;
   initConfig();   
   m_is = NULL;
   m_isDT = NULL;
 }

TimPixTrigController::TimPixTrigController(DbRecord *dbRecord, std::string name) : 
   m_dbRecord(dbRecord), m_mode("instantiated"), m_triggerFrequency(TIM_MASK_TRIG_100_KHZ), m_triggerDelay(0), m_clockDelay(0), 
  m_conf(NULL)
 {
   try {
     createVmeInterface();

     const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
     m_timSlot = 13;
     const uint32_t  mapSize =    0x10000;  // VME window size

     m_isDT = NULL;

     if (m_vme != NULL) {
       m_tim = new TimModule( baseAddr, mapSize, *m_vme );  // Create tim
     } else {
       m_tim = NULL;
       m_is = NULL;
     }
     if (m_vme == NULL || m_tim == NULL) std::cout << "TIM module object with name " << name << " could not be created"  << std::endl;
     m_name=name;
     initConfig();
     loadConfig(dbRecord);
     if (m_vme != NULL) {
       std::string monIsServerName = "RunParams";               // should be TIM Configuration Parameter
       std::string partitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
       m_is = new PixISManager(partitionName, monIsServerName); 
       std::cout << "TimPixTrigController::" << m_name << "IS manager will publish TIM status values in partition: " <<  partitionName << " using IS server: " << monIsServerName << std::endl;
     }
   } catch (...) {
     throw TimException("Exception caught in TimPixTrigController constructor", 0, 0 );
   }
}     //! Constructor


TimPixTrigController::TimPixTrigController(PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name) : 
   m_dbRecord(NULL), m_mode("instantiated"), m_triggerFrequency(TIM_MASK_TRIG_100_KHZ), m_triggerDelay(0), m_clockDelay(0), 
  m_conf(NULL)
 {
   try {
     createVmeInterface();

     const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
     m_timSlot = 13;
     const uint32_t  mapSize =    0x10000;  // VME window size
     
     m_isDT = NULL;
   
     if (m_vme != NULL) {
       m_tim = new TimModule( baseAddr, mapSize, *m_vme );  // Create tim
     } else {
       m_tim = NULL;
       m_is = NULL;
     }
     if (m_vme == NULL || m_tim == NULL) std::cout << "TIM module object with name " << name << " could not be created"  << std::endl;
     m_name=name;
     initConfig();
     m_name=name;
     if (readConfig(dbServer, dom, tag)) { 
       std::cout << "Successfully created TimPixTrigController" << std::endl;
     } else { 
       m_name=name;
       std::cout << "Problem in reading config for TimPixTrigController " << name << std::endl;
     }
     if (m_vme != NULL) {
       std::string monIsServerName = "RunParams";  
       std::string partitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
       if (partitionName == "") {
	 std::cout << "TDAQ_PARTITION_INFRASTRUCTURE is not set" << std::endl;
       } else {
	 m_is = new PixISManager(partitionName, monIsServerName);
	 std::cout << "TimPixTrigController::" << m_name << " IS manager will publish TIM status values in partition: " <<  partitionName << " using IS server: " <<monIsServerName << std::endl;
       }
     }
   } catch (...) {
     throw TimException("Exception caught in TimPixTrigController constructor", 0, 0 );
  }
}     //! Constructor




TimPixTrigController::~TimPixTrigController(){
  delete m_tim;
  if (m_is !=NULL) delete m_is;
  if (m_isDT !=NULL) delete m_isDT;
}                  //! Destructor

void TimPixTrigController::createVmeInterface() {
  // Create VME interface
  m_vme = NULL;
  try {
    m_vme = SctPixelRod::RCCVmeInterface::makeRCCVmeInterface();
  }
  catch(SctPixelRod::VmeException &v) {
    std::stringstream ss;
    ss <<"TimPixController::VMEException"<<" ErrorClass = "<< v.getErrorClass() <<" ErrorCode = " <<v.getErrorCode();
    ers::error(PixLib::pix::daq (ERS_HERE, "TimPixController::VMEException",ss.str()));
  }
  catch(SctPixelRod::BaseException & exc) {
    ers::error(PixLib::pix::daq (ERS_HERE,"TimPixController::BaseException","Unknown failure during VME Interface setup"));
  }
  catch (...) {
     ers::error(PixLib::pix::daq (ERS_HERE,"TimPixController::Exception","Unknown failure during VME Interface setup"));
  }
}

//! DataBase interaction
void TimPixTrigController::loadConfig(DbRecord *r){
  //Read the config object
  m_dbRecord = r;
  m_conf->read(m_dbRecord);
  m_conf->name(m_name);
  m_conf->type("TimPixTrigController");
  std::cout << "**************** TIM RECORD DEC NAME: " << m_dbRecord->getDecName() << std::endl;
}

void TimPixTrigController::saveConfig(DbRecord *r){
  // Write the config object
  m_dbRecord = r;
  m_conf->write(m_dbRecord);
}

void TimPixTrigController::loadConfig(PixConnectivity *conn, unsigned int rev) {  //! load configuration from the database
  if (conn != NULL) {
    DbRecord *r;
    if (rev != 0) {
      r = conn->getCfg(m_name, rev);
    } else {
      r = conn->getCfg(m_name);
    }
    loadConfig(r);
  }
}

void TimPixTrigController::saveConfig(PixConnectivity *conn, unsigned int rev) {  //! save configuration to the db
  if (conn != NULL) {
    DbRecord *r = conn->addCfg(m_name, rev);
    saveConfig(r);
    conn->cleanCfgCache();
  }
}


//! Accessors
Config * TimPixTrigController::config() {return m_conf;}
std::string& TimPixTrigController::mode(){ return m_mode; }
int& TimPixTrigController::slot(){ return m_timSlot;}
SctPixelRod::TimModule& TimPixTrigController::timModule(){return *m_tim;}
float& TimPixTrigController::triggerFreq(){ return m_triggerFrequency;}
int& TimPixTrigController::triggerDelay(){return m_triggerDelay;}
int& TimPixTrigController::ttcrxCoarseDelay(){return m_ttcrxCoarseDelay;}
float& TimPixTrigController::ttcrxFineDelay(){return m_ttcrxFineDelay;}
int& TimPixTrigController::bcidOffset(){return m_bcidOffset;}
float& TimPixTrigController::ECRFreq() {return m_ECRFrequency;}
float& TimPixTrigController::random2Freq() {return m_triggerRand2Frequency;}
int& TimPixTrigController::triggerType() {return m_triggerType;}
int&  TimPixTrigController::calinjCalTrigDelay() {return m_calinj_caltrig_delay;}
int&  TimPixTrigController::calinjTrigDelay() {return m_calinj_trig_delay;}
int&  TimPixTrigController::calTrigNumber() {return m_calinj_trig_number;}
unsigned int &TimPixTrigController::busyMask() {return m_busyMask;}

#if 0
bool TimPixTrigController::getModeFFTV() { return m_FFTV_disable; }
int TimPixTrigController::getMatchThresholdFFTV() { return m_FFTV_match_threshold; }
int TimPixTrigController::getMatchThresholdFFTV_B() { return m_FFTV_B_match_threshold; }
void TimPixTrigController::setModeFFTV(bool mode) { m_FFTV_disable = mode; }
void TimPixTrigController::setMatchThresholdFFTV(int match_threshold) { m_FFTV_match_threshold = match_threshold; }
void TimPixTrigController::setMatchThresholdFFTV_B(int match_threshold) { m_FFTV_B_match_threshold = match_threshold; }
#endif
//! Configuration
void TimPixTrigController::initConfig() {

  string  defaultSeqConfFile="~/configs/sequin.cfg";

  // Values updated according to https://atlpix01.cern.ch/elog/Detector/5079

  float defaultFineDelay; int defaultCoarseDelay;
  if (m_name == "TIM_B1"){ defaultFineDelay = 23.2; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_B2"){ defaultFineDelay = 21.4; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_B3"){ defaultFineDelay = 17.0; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_D1"){ defaultFineDelay = 10.6; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_D2"){ defaultFineDelay = 8.8; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_L1"){ defaultFineDelay = 24.0; defaultCoarseDelay = 1; }
  else if (m_name == "TIM_L2"){ defaultFineDelay = 23.0; defaultCoarseDelay = 1; }
  else if (m_name == "TIM_L3"){ defaultFineDelay = 3.2; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_L4"){ defaultFineDelay = 0.8; defaultCoarseDelay = 2; }
  else if (m_name == "TIM_I1"){ defaultFineDelay = 0.0; defaultCoarseDelay = 10; }
  else { defaultFineDelay = 0; defaultCoarseDelay = 0; }

  m_conf = new Config(m_name, "TimPixTrigController");
  Config &conf = *m_conf;
  
  std::map<std::string, int> timtrmMap;
  timtrmMap["INTERNAL"] = INTERNAL;
  timtrmMap["EXTERNAL_NIM"] = EXTERNAL_NIM;
  timtrmMap["EXTERNAL_LTP"] = EXTERNAL_LTP;
  timtrmMap["INT_RANDOM"] = INT_RANDOM;
  timtrmMap["SEQUENCER"] = SEQUENCER;
  timtrmMap["CAL_INJECT"] = CAL_INJECT;
  
  std::map<std::string, int> timCalTrigModeMap;
  timCalTrigModeMap["ACTIVE"] = ACTIVE;
  timCalTrigModeMap["PASSIVE"] = PASSIVE;

  std::map<std::string, int> timSeqTriggeringModeMap;
  timSeqTriggeringModeMap["INTTRIGSEQ"] = INTTRIGSEQ;
  timSeqTriggeringModeMap["EXTTRIGSEQ"] = EXTTRIGSEQ;

  conf.addGroup("trigger");
  conf["trigger"].addList("timTriggerMode", m_triggerGenMode, INTERNAL, timtrmMap, 
			  "Trigger generation mode", true);
  conf["trigger"].addFloat("triggerFrequency", m_triggerFrequency, 1, "Trigger frequency in kHz", true);
  conf["trigger"].addFloat("ECRFrequency", m_ECRFrequency, 0.1, "ECR frequency in Hz", true);
  conf["trigger"].addFloat("triggerRand2Frequency", m_triggerRand2Frequency, 0.01, "Mean randomizer2 trigger frequency in kHz", true);
  conf["trigger"].addInt("triggerDelay", m_triggerDelay, 140,
			 "Internal trigger delay in BC [0-255]", true);
  conf["trigger"].addInt("ttcrxCoarseDelay", m_ttcrxCoarseDelay, defaultCoarseDelay,
			 "Coarse delay in TTCrx in BC [0-15]", true);
  conf["trigger"].addFloat("ttcrxFineDelay", m_ttcrxFineDelay, defaultFineDelay,
			 "Fine delay in TTCrx in ns [0.0-24.9]", true);
  conf["trigger"].addInt("triggerType", m_triggerType, 10, "Trigger type", true);
  conf["trigger"].addInt("BCID offset", m_bcidOffset, 13,
			 "BCID offset in BC", true);
  conf["trigger"].addInt("RodBusyMask", m_busyMask, 0u, "RodBusy mask", true);
  
  conf["trigger"].addString("Seq_config_file", m_sequencerConfFile, defaultSeqConfFile, "Sequencer configuration file path", true);
  conf["trigger"].addList("Seq_triggering_mode", m_seqtriggering_mode, INTTRIGSEQ, timSeqTriggeringModeMap,
			  "Sequencer triggering mode", true);
  conf["trigger"].addList("timCalInjMode", m_calinj_injectmode, ACTIVE, timCalTrigModeMap,
			  "TIM CAL_INJ mode", true);
  conf["trigger"].addInt("CalTrigDelayActiveTIM",m_calinj_caltrig_delay , 129,
			 "Delay between CAL and TRIG in active TIM, CAL_INJ mode", true);
  conf["trigger"].addInt("TrigDelayPassiveTIM",m_calinj_trig_delay , 0,
			 "Trigger delay in passive TIM, CAL_INJ mode", true);
  conf["trigger"].addInt("NumOfSubsTriggersPassiveTIM",m_calinj_trig_number , 0,
			 "Number of subsequent triggers in passive TIM, CAL_INJ mode", true);
  conf["trigger"].addBool("DisableFFTVmode",m_FFTV_disable, 1,
                         "Disable FFTV mode", true);
  conf["trigger"].addInt("FFTVmatchThreshold",m_FFTV_match_threshold, 10,
			  "Threshold for FFTV-T matching in periods [2-10]", true);
  conf["trigger"].addInt("FFTVmatchTolerance",m_FFTV_match_tolerance, 400,
			 "Tolerance for FFTV-T matching in BC [65535-32]", true);
  conf["trigger"].addInt("FFTVemergencyThreshold",m_FFTV_emergency_threshold, 10,
			  "Threshold for FFTV-T emergency action in number of FFTV [2-255]", true);
  conf["trigger"].addInt("FFTVemergencyCounterLeakRate",m_FFTV_emergency_counter_leakrate, 60,
			  "Leak rate of FFTV-T emergency counter in seconds [1-65535]", true);
  conf["trigger"].addInt("FFTV_B_matchThreshold",m_FFTV_B_match_threshold, 10,
			 "Threshold for FFTV-B matching in periods [0-31]", true);
  conf["trigger"].addBool("FFTV_B_decrementalmode",m_FFTV_B_decremental_mode, 0,
			  "Set FFTV-B decremental mode to -2", true);
  conf["trigger"].addBool("EnableFPDebugOutputs",m_enableFPDebugOutputs, 0,
			  "Enable FP debug outputs TRIO/TRO to monitor TTC clocks or strobes", true);
  conf["trigger"].addBool("FPDebugOutputsMode",m_FPDebugOutputs_mode, 0,
			  "Mode of FP debug output signals TRIO/TRO [false-clocks true-strobes]", true);
  //conf["trigger"].addString("TIMName", m_name, "TimPixTrigController", "TimObject name in Db", true);
  conf.reset();

  m_fftv_B_BusyDuration_Hist = 0;
  m_fftvBusyDuration_Hist = 0;
  m_time = time(NULL);
}

void TimPixTrigController::init(){
  if (m_tim!=NULL) {
    m_tim->reset();
    std::hex(std::cout);
    std::cout << "serial " << m_tim->getSerialNumber()          << std::endl;     
    std::cout << "TIM ID " << m_tim->regFetch( TIM_REG_TIM_ID ) << std::endl;
    m_fw = m_tim->getFirmware();
    std::cout << "TIM FW 0x" << std::hex <<m_fw << std::dec << std::endl;
    m_tim->loadBitClear(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_CSBDISABLE); //turn clock-switching BUSY back on, which is off by reset
    m_tim->loadBitSet(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_SARBDISABLE); //disable SA-mode setting RodBusy
    if (m_is!=NULL) {
      std::cout << "TimPixTrigController::init() : deleting IS variables..." << std::endl;
      try
	{
	  m_is->removeVariable(m_name + "/TimOkChanges");
	  m_is->removeVariable(m_name + "/QpllLockChanges");
	  m_is->removeVariable(m_name + "/L1IDCounterValue");
	  m_is->removeVariable(m_name + "/ECRCounterValue");
	  m_is->removeVariable(m_name + "/TriggerType");
	  m_is->removeVariable(m_name + "/RODBusyMask");
	  m_is->removeVariable(m_name + "/RODBusyState");
	  m_is->removeVariable(m_name + "/RodBusyDuration");
	  m_is->removeVariable(m_name + "/TimOperationMode");
	  m_is->removeVariable(m_name + "/TimBcidOffset");
	  m_is->removeVariable(m_name + "/TimTriggerDelay");
	  m_is->removeVariable(m_name + "/TTCrxFineDelay");
	  m_is->removeVariable(m_name + "/TTCrxCoarseDelay");
	  m_is->removeVariable(m_name + "/TIMING_SCAN_STATUS");
	  m_is->removeVariable(m_name + "/TIM_SCAN_STATUS");
	  m_is->removeVariable(m_name + "/FFTVCounterValue");
	  m_is->removeVariable(m_name + "/FFTVBusyFraction");
	  m_is->removeVariable(m_name + "/FFTVmatchThreshold");
	  m_is->removeVariable(m_name + "/FFTVmatchTolerance");
	  m_is->removeVariable(m_name + "/FFTVemergencyThreshold");
	  m_is->removeVariable(m_name + "/FFTVemergencyState");
	  m_is->removeVariable(m_name + "/FFTVemergencyCounter");
	  m_is->removeVariable(m_name + "/FFTVemergencyCounterLeakRate");
	  m_is->removeVariable(m_name + "/FFTV_B_matchThreshold");
	  m_is->removeVariable(m_name + "/FFTV_B_decrementalmode");
	  m_is->removeVariable(m_name + "/FFTV_B_BusyFraction");
	  m_is->removeVariable(m_name + "/FFTV_B_CounterValue");
	  m_is->removeVariable(m_name + "/FFTV_B_BusyPerc");
          m_is->removeVariable(m_name + "/FFTVBusyPerc");
	}
      catch(PixISManagerExc& exc){
	 ers::error(PixLib::pix::daq (ERS_HERE,"TimPixController::PixISManager","Exception caught on removing variables from IS." + exc.getISmanName()));
      }
    }
  }
}

//Setting in hardware methods
void TimPixTrigController::setTriggerFreq(const float freq){
  if (m_tim!=NULL) {
    if (m_fw<19) {
      if(freq < 0.05 || freq > 600) {
	std::cout << "TimPixTrigController::setTriggerFreq : invalid trigger frequency " << freq << ". Values allowed 0.05-600kHz" << std::endl;
	return;
      }
      m_tim->loadFrequencyTrig(freq);
    }
    else {
      if(freq < 0.0000001 || freq > 40000) {  //0.0000001 <-> 4294967295 bco period
	std::cout << "TimPixTrigController::setTriggerFreq : invalid trigger frequency " << freq << ". Values allowed 0.0000001-40000kHz" << std::endl;
	return;
      }
      uint32_t clkPeriodBCO;
      clkPeriodBCO = (uint32_t)(40000.0/freq);  //clkPeriodBCO = 400000; - 0.1kHz
      m_tim->regLoad(TIM_REG_TROSC2_PERL, clkPeriodBCO & 0x0000ffff);
      m_tim->regLoad(TIM_REG_TROSC2_PERH, (clkPeriodBCO & 0xffff0000) >> 16);
    } 
  }
  m_triggerFrequency = freq;
}

void TimPixTrigController::setECRFreq(const float freq){
  if(freq < 0.005 || freq > 60) {
     ers::error(PixLib::pix::daq (ERS_HERE,"TimPixTrigController::setECRFreq","Invalid ECR frequency "+std::to_string(freq) +". Values allowed 0.005-60Hz"));
    return;
  }
  if(m_tim!=NULL) m_tim->loadFrequencyFECR(freq);
  m_ECRFrequency = freq;
}

void TimPixTrigController::setRandom2Freq(const float freq){
  
  if(freq < 0.001 || freq > 150) {
     ers::error(PixLib::pix::daq (ERS_HERE,"TimPixTrigController::setRandom2Freq","Invalid Randomizer2 mean frequency " +std::to_string(freq) +". Values allowed 0.001-150KHz"));
    return;
  }
  if(m_tim!=NULL) {
    int freqHz = int (freq * 1000.); // Hz
    int rand2_pattern = TIM_RAND2_FREQ_150KHZ; // default
    int chooser = 0;
    
    for (int i = 0; i < TIM_RANDOM2_FREQ_SIZE; i++) { // will be TIM_RANDOM2_FREQ_SIZE, different in firmware 14
      if (TIM_RANDOM2_FREQ[i][1] <= freqHz &&
	  TIM_RANDOM2_FREQ[i][1] > chooser) {
	chooser = TIM_RANDOM2_FREQ[i][1];
	rand2_pattern = TIM_RANDOM2_FREQ[i][0];
      }
    }
    m_tim->regLoad(TIM_REG_CONTROL, rand2_pattern << 4);
  }
  m_triggerRand2Frequency = freq;
}

void TimPixTrigController::setTtcRxDelay(float delay_ns){
  if(delay_ns < 0 || delay_ns > 399.9) {
    ers::error(PixLib::pix::daq (ERS_HERE,"TimPixTrigControllersetTtcRxDelay","Invalid delay " + std::to_string(delay_ns) +". Values allowed 0 - 399.9"));
    return;
  }
  //m_ttcrxCoarseDelay = (int)(delay_ns/25);
  //m_ttcrxFineDelay = fmod(delay_ns,25);
  if(m_tim!=NULL) {
    int fineDelay = (int)(fmod(delay_ns,25)*9.619+0.5);
    int coarseDelay = (int)(delay_ns/25);
    int n, m;
    if (coarseDelay>=0) {
      coarseDelay = ((coarseDelay & 0xf) << 4) + (coarseDelay & 0xf); //same delay for both delay groups
      m_tim->ttcRxWrite(0x2,coarseDelay & 0xff);
      std::cout << m_name << ": coarse delay in TTCrx chip set to "<< (int)(delay_ns/25) << std::endl;
    }
    if (fineDelay>=0) {
      n = fineDelay % 15;
      m = ((fineDelay/15)-n+14) % 16;
      m_tim->ttcRxWrite(0x0, ((n <<4) + m) & 0xff);
      std::cout << m_name << ": fine delay in TTCrx chip set to "<< fmod(delay_ns,25) << std::endl;
    }
  }
}

float TimPixTrigController::readTtcRxDelay(){
  if(m_tim!=NULL) {
    int regval,coarseDelay, n, m;
    float fineDelay;
    regval = m_tim->ttcRxRead(0x2);
    coarseDelay = (regval >>4) & 0xf;
    if ((coarseDelay & 0xf) != (regval & 0xf)) std::cout << m_name << ": coarse delay for delay groups in TTCrx chip are not equal: "<< coarseDelay<< " " << (regval & 0xf) << std::endl;
    regval = m_tim->ttcRxRead(0x0);
    n = (regval >> 4) & 0xf;
    m = regval & 0xf;
    fineDelay = ((m*15 + n*16 + 30)%240)*0.104;
    return ((float)coarseDelay*25 + fineDelay);
  }
  else return 0;
}


void TimPixTrigController::setTriggerDelay(int ckSteps){
  if(ckSteps < 0 || ckSteps > 255) {
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Invalid ckSteps "+std::to_string( ckSteps) + ". Values allowed 0-255"));
    return;
  }
  if(m_tim!=NULL)  {
    m_tim->loadByteLo( TIM_REG_DELAY, ckSteps);
  }
  m_triggerDelay = ckSteps;
}

void TimPixTrigController::setBcidOffset(int bcidOffset){
  if(bcidOffset < 0 || bcidOffset > 4095) {
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"invalid bcidOffset " + std::to_string( bcidOffset) +". Values allowed 0-4095"));
    return;
  }
  if(m_tim!=NULL)  {
    if (bcidOffset<16) m_tim->regLoad( TIM_REG_BCID, bcidOffset << 12 );
    else m_tim->regLoad(TIM_REG_BCID_OFFSET,(bcidOffset & 0xfff));
  }
  m_bcidOffset = bcidOffset;
}


void TimPixTrigController::setTriggerType(int ttype){
  if(ttype < 0 || ttype > 4095) {
     ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Invalid ttype "+std::to_string(ttype)+ ". Values allowed 0-4095"));
    return;
  }
  if(m_tim!=NULL)  m_tim->regLoad( TIM_REG_TTID, ttype );
  m_triggerType = ttype;
}


void TimPixTrigController::startRandom2Triggers(){
  m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_RANDOM2);
}

void TimPixTrigController::stopRandom2Triggers(){
  m_tim->loadBitClear(TIM_REG_ENABLES3, TIM_BIT_EN_RANDOM2);
}

void TimPixTrigController::setSaClkDelay(int halfnsMult){
  if(halfnsMult < 0 || halfnsMult > 63) {
     ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Invalid halfnsMult " + std::to_string(halfnsMult)+". Values allowed 0-63"));
    return;
  }
  m_tim->loadByteHi( TIM_REG_DELAY, halfnsMult);
}

void TimPixTrigController::setVmeTimBusy(){
  m_tim->loadBitSet(TIM_REG_BUSY_EN3, TIM_BIT_BUSY_EN3_VME_BUSY4SA_L1ASTOP | TIM_BIT_BUSY_EN3_VME_BUSY2TB_FP);     // , has to be released at StartTrigger level and (0x1000) see it as TB signal on FP
  m_tim->loadBitSet(TIM_REG_COMMAND, TIM_VBUSY);   // force TIM to stop internal triggers, sets TB LED 
}

void TimPixTrigController::releaseVmeTimBusy(){
  m_tim->loadBitClear(TIM_REG_COMMAND, TIM_VBUSY); // release forced TIMBusy
}

void TimPixTrigController::setVmeRodBusy(){
  m_tim->loadBitSet(TIM_REG_COMMAND, TIM_VROD_BUSY); 
}

void TimPixTrigController::releaseVmeRodBusy(){
  m_tim->loadBitClear(TIM_REG_COMMAND, TIM_VROD_BUSY);
}

void TimPixTrigController::enableRodBusyStopSATriggers(){
  m_tim->loadBitSet( TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY );  // additionally it will be seen as TB on FP
}

int TimPixTrigController::loadSequencerFromDatafile(){
  uint16_t pattern;
  int mult;
  char t_buf[100];
  char a1[5];
  char a2[5];
  int count = 0;
  int index;
  uint16_t buffer[ TIM_SEQ_SIZE ];

  for (unsigned int i = 0; i < TIM_SEQ_SIZE; i++) buffer[i] = 0; //clear sequencer memory before
  std::ifstream seqfile((&m_sequencerConfFile)->c_str(), ios::in);
    if (!seqfile.is_open()) {
      return -1;
      
    }
    else {
      std::cout << "Loading file " << (&m_sequencerConfFile)->c_str() << " into memory" << std::endl;
      index=0;
      while(!seqfile.eof()){
	seqfile.getline(t_buf,100);
	// ignore lines with #
	if (t_buf[0]!='#' && strlen(t_buf)>10) {
	  sscanf(t_buf,"%4s %4s %d",&a1[0],&a2[0],&mult);			   
	  pattern = uint16_t(char2int(a1)*0x10 + char2int(a2)*0x01);
	  cout << "Pattern: " << a1 << " " << a2 << "=" << pattern << " Multiplicity: " << mult <<  endl;
	  count+=mult;
	  while (mult--) buffer[index++]=pattern;
	}
      }
      m_tim->regLoad( TIM_REG_SEQ_END, index - 1 );
      m_tim->seqLoad( index, buffer );
    }
    seqfile.close();
    std::cout << "Read " << count << " patterns" << std::endl;
    return count; 
}

int TimPixTrigController::loadSequencerFromDatabase(){  // for CAL_INJECT mode
  int size = 0;
  uint16_t buffer[ TIM_SEQ_SIZE ];

  for (unsigned int i = 0; i < TIM_SEQ_SIZE; i++) buffer[i] = 0; //clear sequencer memory before
  switch (m_calinj_injectmode) {
    case ACTIVE:
      buffer[0]=TIM_CAL;
      buffer[m_calinj_caltrig_delay]=TIM_L1A;                      //L1A multiplicity supposed to be setup in MCC!
      buffer[m_calinj_caltrig_delay+m_calinj_trig_delay]=TIM_L1A;  //after-pulse readout now possible with the same TIM, since delay is zero by default
      size = m_calinj_caltrig_delay+m_calinj_trig_delay+1;
      std::cout << "TIM in the active CAL_INJECT mode" << std::endl;
      std::cout << "Delay between CAL and L1A: " << m_calinj_caltrig_delay <<std::endl;
      break;
    case PASSIVE:
      for (int i=0;i<m_calinj_trig_number;i++) buffer[m_calinj_caltrig_delay+m_calinj_trig_delay+i*2]= TIM_L1A;
      size = m_calinj_caltrig_delay+m_calinj_trig_delay+m_calinj_trig_number*2 +1;
      std::cout << "TIM in the passive CAL_INJECT mode" << std::endl;
      std::cout << "Delay after triggering in the master mode: " << m_calinj_trig_delay << std::endl;
      std::cout << "Number of subsequent triggers: " << m_calinj_trig_number << std::endl;
      break;
  default:
     ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Error: wrong mode selected!"));
    // ACTIVE?
    break;
  }
  if (size>0x4000) return -1;
  m_tim->regLoad( TIM_REG_SEQ_END, size-1);	
  m_tim->seqLoad( size, buffer );
  return size; 
}

void TimPixTrigController::configureTriggerMode(){        // at configure
  std::string timOperationMode;
  float readTTCrxDelay;
  init();
  if (m_FFTV_disable) m_tim->loadBitSet(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_FVDISABLE); // bypass repetitive trigger veto
  else {
    m_tim->loadByteLo(TIM_REG_FV_MATCH, m_FFTV_match_threshold & 0x000f);
    m_tim->regLoad(TIM_REG_FV_DELTAT, m_FFTV_match_tolerance & 0xffff);
    m_tim->loadByteHi(TIM_REG_FV_MATCH, m_FFTV_emergency_threshold & 0xff);
    m_tim->regLoad( TIM_REG_FV_EMERG_TO, m_FFTV_emergency_counter_leakrate & 0xffff);
    m_tim->loadBitClear(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_FVDISABLE);
    m_tim->regLoad( TIM_REG_FVB_MATCH, m_FFTV_B_match_threshold & 0x001f);
    if (m_FFTV_B_decremental_mode) m_tim->loadBitSet(TIM_REG_CONTROL,TIM_BIT_CTL_FVB_DECR_MODE);
    else m_tim->loadBitClear(TIM_REG_CONTROL,TIM_BIT_CTL_FVB_DECR_MODE);
    //m_tim->loadBitSet(TIM_REG_DEBUG_CTL,0x0800); //enable IBL mode //<=--- up to v2c only!!
  }
  switch(m_triggerGenMode){
  case INTERNAL:
    m_tim->setupVME(); //enable serial output streams ensure stand-alone mode / no signals
    setVmeTimBusy();
    setTriggerDelay(m_triggerDelay);
    setTriggerFreq(m_triggerFrequency); 
    setECRFreq(m_ECRFrequency);
    setTriggerType(m_triggerType); 
    enableRodBusyStopSATriggers();
    if (m_fw<19) setIOMode(INT_TRIGGER); //enable internal triggers
    else m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_OSC2);
    timOperationMode = "INTERNAL";
    break;
  case EXTERNAL_NIM:
    m_tim->setupVME(); //enable serial output streams ensure stand-alone mode / no signals
    setVmeTimBusy();
    //m_tim->setIOMode(EXT_CLOCK); //
    setIOMode(EXT_TRIGGER);
    setIOMode(EXT_ECR);
    setIOMode(EXT_BCR);
    setIOMode(EXT_FER);
    setIOMode(EXT_CAL);
    setTriggerDelay(m_triggerDelay);
    setTriggerType(m_triggerType); 
    enableRodBusyStopSATriggers();
    timOperationMode = "EXTERNAL_NIM";
    break;
  case INT_RANDOM:
    m_tim->setupVME(); //enable serial output streams ensure stand-alone mode / no signals
    setVmeTimBusy();
    setTriggerType(m_triggerType); 
    setRandom2Freq(m_triggerRand2Frequency);
    enableRodBusyStopSATriggers();
    startRandom2Triggers();
    timOperationMode = "INT_RANDOM";
    break;
  case CAL_INJECT:
    if (m_fw<0x3e) { 
      // presumes synchronised TIMs at prepareForRun for EXTTRIGSEQ mode
      m_tim->setupVME(); //enable serial output streams ensure stand-alone mode / no signals
      if (m_seqtriggering_mode == EXTTRIGSEQ) m_tim->regLoad( TIM_REG_RUN_ENABLE, TIM_BIT_EN_ID | TIM_BIT_EN_TYPE | TIM_BIT_EN_TTC_CLK | TIM_BIT_EN_TTC_BCR);   // ENABLE from TTC only CLK & BCR
      do {} while (!(m_tim->regFetch( TIM_REG_STATUS3) & TIM_BIT_STATUS3_PLLSTABLE ));// wait till clk is stable
      setTriggerType(m_triggerType);
      setVmeTimBusy(); // will ihnibit external seq triggering
      
      if (loadSequencerFromDatabase()<0)
	{ std::cout << "Out of sequencer memory..." << endl; break;}
      
      if (m_seqtriggering_mode == EXTTRIGSEQ) m_tim->loadBitSet(TIM_REG_ENABLES, TIM_BIT_EN_EXT_SEQ); //enable ext sequencer trigger input
      if (m_seqtriggering_mode == INTTRIGSEQ) {                                                       //setting up int sequencer triggering
	setTriggerFreq(m_triggerFrequency);
	enableRodBusyStopSATriggers();
	if (m_fw<19) m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_TRIG ); //start internal sequencer triggering
	else m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_OSC2);
      }
      m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TRIG_SEQ); // enable triggering of sequencer from internal trigger sources

      m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_RESET ); 
      m_tim->regLoad( TIM_REG_SEQ_CTL, 0 );
      m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_BCR | TIM_BIT_SEQ_CAL | TIM_BIT_SEQ_TRIG ); //enable only CAL,BCR & L1A, GO not set since EXT-Trigg, enable cyclic operat  
      timOperationMode = "CAL_INJECT";
    } else {
      std::cout << "FATAL: cannot setup CAL_INJECT mode for this FW: sequencer circuitry removed from v3e." << endl;
    }
    break;
  case SEQUENCER:
    if (m_fw<0x3e) { 
      m_tim->setupVME(); //enable serial output streams ensure stand-alone mode / no signals
      m_tim->regLoad( TIM_REG_RUN_ENABLE, TIM_BIT_EN_ID | TIM_BIT_EN_TYPE | TIM_BIT_EN_TTC_CLK | TIM_BIT_EN_TTC_BCR);   // ENABLE from TTC only CLK & BCR
      do {} while (!(m_tim->regFetch( TIM_REG_STATUS3) & TIM_BIT_STATUS3_PLLSTABLE ));// wait till clk is stable
      setTriggerType(m_triggerType);
      setVmeTimBusy(); // will ihnibit external seq triggering
      
      if (loadSequencerFromDatafile()<0) { std::cout << "Not possible to open the sequencer file..." << endl; break;}
      
      if (m_seqtriggering_mode == EXTTRIGSEQ) m_tim->loadBitSet(TIM_REG_ENABLES, TIM_BIT_EN_EXT_SEQ); //enable ext sequencer trigger input
      if (m_seqtriggering_mode == INTTRIGSEQ) {                                                       //setting up int sequencer triggering
	setTriggerFreq(m_triggerFrequency);
	enableRodBusyStopSATriggers();
	if (m_fw<19) m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_TRIG ); //start internal sequencer triggering
	else m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_OSC2);
      }
      
      m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TRIG_SEQ); // enable triggering of sequencer from internal trigger sources
   
      m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_RESET );
      m_tim->regLoad( TIM_REG_SEQ_CTL, 0 );
      m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_EN_ALL); //enable all SEQ outputs
      timOperationMode = "SEQUENCER";
    } else {
      ers::fatal(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Cannot setup SEQUENCER mode for this FW: sequencer circuitry removed from v3e."));
    }
    break;
  case EXTERNAL_LTP:
    setVmeRodBusy(); //set vme RODBusy to stop triggers at LTP level (not TTCvi!!!) 
    //
    // the TIM's 0x54 and 0x55 require special clock for proper operation.
    //
    if ( m_tim->getSerialNumber()==0x54 ||  m_tim->getSerialNumber()==0x55) {
      m_tim->loadBitSet(TIM_REG_TTC_SELECT,0x1<<8);
      sleep(2);     
    }

    //--------------------- enable TTCrx output bus to TIM--------------
    m_tim->ttcRxWrite(0x3,0xb3);
    //    3 - command register of TTCrx                                          
    //    b3 - enable External bus for Dout and DoutStr
    //---------------------                    --------------------------
       
    m_tim->setupTTC(); //enable TTC signals and output streams, ensure no external/internal SA signals
    //the following code is necessary to prevent TIM internal Busy to stop delayed TTC triggers 
    m_tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY); //Busy from RODs will not stop TIM internal triggers 
    
    setVmeRodBusy(); //set vme RODBusy to stop triggers at LTP level (not TTCvi!!!)

    //---- Here we taking over QPLL control and reset into into the right operation mode (0) - not to be confused with the "mode" bit, which stays "1"! 
    
    m_tim->regLoad(TIM_REG_QPLL_CTL,0xa0);  
    m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_QPLLCONTROL);
    m_tim->loadBitClear(TIM_REG_QPLL_CTL,0x0020);
    usleep(10);
    m_tim->loadBitSet(TIM_REG_QPLL_CTL,0x0020);
   
    if (m_fw<0x26) { 
      // mode with delay>0 is not TTC mode since L1A are sent trough SA trigger chain
      // m_tim->loadBitClear(TIM_REG_COMMAND, TIM_BIT_EN_TTC);  //disable running mode
      m_tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_L1A); //to disable double pulse on trigger output
      //m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TTC_TTYPE2SA);    //to enable TTYPE in SA mode
      m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TTC_TRIG_DELAY); //enable TTC trigger delay
      m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TTC_BCR2SA); //Enable TTCrx BCR into SA Mode
      m_tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_TTC_BCR_DELAY); //Enable SA BCR to be delayed by trigger delay value
    }
   
    setTriggerDelay(m_triggerDelay);
    setBcidOffset(m_bcidOffset);

    //-------------set up TTCrx delay registers after all other configurations  ------------------------
      
    setTtcRxDelay(m_ttcrxCoarseDelay*25+m_ttcrxFineDelay);
    readTTCrxDelay = readTtcRxDelay();
    std::cout << m_name <<" Readback TTCrx delays, coarse delay: " << (int)(readTTCrxDelay/25) <<" fine delay: "<< (double)(fmod(readTTCrxDelay,25)) << std::endl;
    //-----------------fine delay part finished---------------

    timOperationMode = "EXTERNAL_LTP";
    break;
  default:
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__, "Wrong mode selected!"));
    // or VME mode
    timOperationMode = "UNDEFINED";
    break;
  }
  // common for all modes
  
  m_tim->regLoad( TIM_REG_ROD_MASK, 0x0); // disable all RODs
  m_tim->loadBitSet(TIM_REG_ROD_MASK, m_busyMask); //set only allowed RODs
  //publish operation mode & fixed cfg parameters in IS
  if (m_is !=NULL) {
    try
      {
	m_is->publish(m_name+"/TimOperationMode", timOperationMode);
	m_is->publish(m_name + "/TimTriggerDelay", m_triggerDelay);
	m_is->publish(m_name+"/TTCrxFineDelay",  (double)(fmod(readTTCrxDelay,25)));
	m_is->publish(m_name+"/TTCrxCoarseDelay", (int)(readTTCrxDelay/25));
	m_is->publish(m_name + "/FFTVmatchThreshold", m_FFTV_match_threshold);
	m_is->publish(m_name + "/FFTVmatchTolerance", m_FFTV_match_tolerance);
	m_is->publish(m_name + "/FFTVemergencyThreshold", m_FFTV_emergency_threshold);
	m_is->publish(m_name + "/FFTVemergencyCounterLeakRate", m_FFTV_emergency_counter_leakrate);
	m_is->publish(m_name + "/FFTV_B_matchThreshold", m_FFTV_B_match_threshold);
	m_is->publish(m_name + "/FFTV_B_decrementalmode", m_FFTV_B_decremental_mode);
      }
    catch(PixISManagerExc& exc){
       ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"PixISManager Exception caught on publishing in IS"+exc.getISmanName()));
    }
  }
}

void TimPixTrigController::prepareForRunTriggerMode(){        // at prepareForRun to change parameters
  switch(m_triggerGenMode){
  case INTERNAL:
    setTriggerDelay(m_triggerDelay);
    setTriggerFreq(m_triggerFrequency); 
    setECRFreq(m_ECRFrequency);
    setTriggerType(m_triggerType); 
    break;
  case EXTERNAL_NIM:
    setTriggerDelay(m_triggerDelay);
    setTriggerType(m_triggerType); 
    break;
  case INT_RANDOM:
    setTriggerType(m_triggerType); 
    setRandom2Freq(m_triggerRand2Frequency);
    break;
  case CAL_INJECT:
    setTriggerType(m_triggerType);
    if (m_seqtriggering_mode == INTTRIGSEQ) setTriggerFreq(m_triggerFrequency); 
    if (loadSequencerFromDatabase()<0)
      { std::cout << "Out of sequencer memory..." << endl; break;}
    m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_RESET );
    m_tim->regLoad( TIM_REG_SEQ_CTL, 0 );
    m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_BCR | TIM_BIT_SEQ_CAL | TIM_BIT_SEQ_TRIG ); //enable only CAL,BCR & L1A to TIM output
    break;
  case SEQUENCER:
    setTriggerType(m_triggerType);
    if (m_seqtriggering_mode == INTTRIGSEQ) setTriggerFreq(m_triggerFrequency); 
    if (loadSequencerFromDatafile()<0) { std::cout << "Not possible to open the sequencer file..." << endl; break;}
    m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_RESET ); // no harm, but what it does?
    m_tim->regLoad( TIM_REG_SEQ_CTL, 0 );
    m_tim->regLoad( TIM_REG_SEQ_CTL, TIM_BIT_SEQ_EN_ALL); //enable all SEQ outputs
    break;
  case EXTERNAL_LTP:
    setTriggerDelay(m_triggerDelay);
    setBcidOffset(m_bcidOffset);
    break;
  default:
     ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Error: wrong mode selected!"));
    // or VME mode
    break;
  }
  // common for all modes
  m_scanMode = INIT;
  m_tim->regLoad( TIM_REG_ROD_MASK, 0x0); // disable all RODs
  m_tim->loadBitSet(TIM_REG_ROD_MASK, m_busyMask); //set only allowed RODs
  //publishing of TIM BCID offset and Trigger delay in IS
  if (m_is !=NULL) {
    try
      {
	m_is->publish(m_name + "/TimBcidOffset", m_bcidOffset);
	m_is->publish(m_name + "/TimTriggerDelay", m_triggerDelay);
      }
    catch(PixISManagerExc& exc){
      ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"PixISManager Exception caught on publishing in IS"+ exc.getISmanName()));
    }
  }
}


void TimPixTrigController::startTriggerMode(){
  clearBusyCountersAndLatches();        
  switch(m_triggerGenMode){
  case INTERNAL:
    releaseVmeTimBusy();
    break;
  case EXTERNAL_NIM:
    releaseVmeTimBusy();
    break;
  case INT_RANDOM:
    releaseVmeTimBusy();
    break;
  case CAL_INJECT:
    releaseVmeTimBusy(); // enables Ext Seq triggering
    break;
  case SEQUENCER:
    releaseVmeTimBusy(); // enables Ext Seq triggering
    break;
  case EXTERNAL_LTP:
    setFPDebugOutputs(); // can spy on TTC clock or strobes
    releaseVmeRodBusy(); // release forced RODBusy
    break;
  default:
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Wrong mode selected!"));
    // or VME mode
    break;
  }
}

void TimPixTrigController::stopTriggerMode(){        
switch(m_triggerGenMode){
  case INTERNAL:
    setVmeTimBusy();
    break;
  case EXTERNAL_NIM:
    setVmeTimBusy();
    break;
  case INT_RANDOM:
    setVmeTimBusy();
    break;
  case CAL_INJECT:
    setVmeTimBusy();
    break;
  case SEQUENCER:
   // TIMBusy setting doesn't stop sequencer triggers, but will stop sequencer triggering itself
    setVmeTimBusy();
    break;
  case EXTERNAL_LTP:
    setVmeRodBusy();
    break;
  default:
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Wrong mode selected!"));
    // or VME mode
    break;
  }
}

int TimPixTrigController::publishTimInfo(){
  double rodBusyDuration=0.;
  double fftvBusyFraction=0.;
  double fftv_B_BusyFraction=0.;
  double fftvBusyPerc=0.;
  double fftv_B_BusyPerc=0.;
  std::string rodBusyMaskInIs;
  std::string rodBusyStateInIs;
  int FFTVemergencyState(0);   
  uint16_t rodBusyMask, rodBusyState;
  if(m_tim!=NULL) {
    int l1idCounterValue = m_tim->fetchL1ID();
    if (l1idCounterValue == 0xffffff) l1idCounterValue = -1; 
    rodBusyMask=m_tim->regFetch(TIM_REG_RB_MASK);
    int ecrCounterValue = (m_tim->regFetch(TIM_REG_L1IDH) & 0xff00) >> 8;
    int triggerType = (m_tim->regFetch(TIM_REG_TTID) & 0x00ff);

    bool timOkChanged = (m_tim->regFetch(TIM_REG_STATUS_LCH) & 0x2000) >> 13;
    bool qpllLockChanged = (m_tim->regFetch(TIM_REG_STAT3_LCH) & 0x0008) >> 3;
    int numOfTimOkChanges = 0;
    int numOfQpllLockChanges = 0;
    if (timOkChanged) {
      char t_s[100];
      time_t t;
      tm t_m;
      t = time(NULL);
      localtime_r(&t, &t_m);
      strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
      std::cout<< t_s<<" " << m_name << " TIMOK changed. Register content before cleaning: 0x"<< std::hex<< m_tim->regFetch(TIM_REG_STATUS_LCH)<<std::dec<<std::endl; 
      m_tim->regLoad(TIM_REG_STATUS_LCH,0x0); //clear the latch register
      if (m_is !=NULL) {
	try {
	  if (m_is->exists(m_name + "/TimOkChanges")) numOfTimOkChanges = m_is->read<int>(m_name + "/TimOkChanges")+1;
	  else numOfTimOkChanges=1;
	} catch (PixISManagerExc& exc){
	  std::cout <<"PixISManager Exception caught on reading from IS, trying to publish first"<< exc.getISmanName() << std::endl;
	}
      }
    }
    if (qpllLockChanged) {
      char t_s[100];
      time_t t;
      tm t_m;
      t = time(NULL);
      localtime_r(&t, &t_m);
      strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
      std::cout << t_s <<" "<< m_name << " QPLL lock changed. Register content before cleaning: 0x"<< std::hex<< m_tim->regFetch(TIM_REG_STAT3_LCH)<<std::dec<<std::endl; 
      m_tim->regLoad(TIM_REG_STAT3_LCH,0x0); //clear the latch register
      if (m_is !=NULL) {
	try {
	  if (m_is->exists(m_name + "/QpllLockChanges")) numOfQpllLockChanges = m_is->read<int>(m_name + "/QpllLockChanges")+1;
	  else numOfQpllLockChanges=1;
	} catch (PixISManagerExc& exc){
	  ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"PixISManager Exception caught on reading from IS, trying to publish first"+ exc.getISmanName()));
	}
      }
    }

    // checks BUSY duration during polling interval 
    unsigned long long int rodBusyBC = m_tim->regFetch(TIM_REG_BCOUNTL);
    rodBusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_BCOUNTH))<<16;
    rodBusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_BCOUNTX))<<32;
    rodBusyDuration = 25e-9 * rodBusyBC;

    if (!m_FFTV_disable) {
      unsigned long long int fftvBusyBC = m_tim->regFetch(TIM_REG_FV_COUNTL);
      fftvBusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_FV_COUNTH))<<16;
      fftvBusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_FV_COUNTX))<<32;
      double fftvBusyDuration = 25e-9 * fftvBusyBC;

      unsigned long long int fftv_B_BusyBC = m_tim->regFetch(TIM_REG_FVB_COUNTL);
      fftv_B_BusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_FVB_COUNTH))<<16;
      fftv_B_BusyBC |=((unsigned long long int)m_tim->regFetch(TIM_REG_FVB_COUNTX))<<32;
      double fftv_B_BusyDuration = 25e-9 * fftv_B_BusyBC;

      if (rodBusyDuration!=0.0) {
	fftvBusyFraction = fftvBusyDuration/rodBusyDuration;
	fftv_B_BusyFraction = fftv_B_BusyDuration/rodBusyDuration;
      } else {
	fftvBusyFraction = 0.0;
	fftv_B_BusyFraction = 0.0;
      }

      time_t tnow = time(NULL);
      double elapsed_seconds = difftime(tnow,m_time);
      if (elapsed_seconds <= 0){
      fftvBusyPerc = 0;
      fftv_B_BusyPerc = 0;
      }else {
      fftvBusyPerc = (fftvBusyDuration - m_fftvBusyDuration_Hist)*100./elapsed_seconds;
      fftv_B_BusyPerc = (fftv_B_BusyDuration - m_fftv_B_BusyDuration_Hist)*100./elapsed_seconds;
      }
      if( fftvBusyPerc > 10)PIX_ERROR("FFTV EMERGENCY ACTION: Trigger has to be released by Pixel Run Coordinator after trigger rate is reduced to a reasonable value. Call Pixel RunCo and trigger on-call. Inform Shift Leader");
      if( fftv_B_BusyPerc > 1)PIX_ERROR("FFTV_B VETO is "<< fftv_B_BusyPerc <<"%, might be due to wrong trigger keys or high trigger rate, call Pixel on call");
    //std::cout<<"JP Debug: FFTV busy "<<(fftvBusyDuration - m_fftvBusyDuration_Hist)<<" s; FFTV_B busy "<<(fftv_B_BusyDuration - m_fftv_B_BusyDuration_Hist)<<"s, during last "<<elapsed_seconds<<" seconds"<<std::endl;
    m_fftv_B_BusyDuration_Hist = fftv_B_BusyDuration;
    m_fftvBusyDuration_Hist = fftvBusyDuration;
    m_time = time(NULL);
    }

    std::ostringstream rbMaskTemp;
    rbMaskTemp << "OR'ed       ";
    for (int i=0;i<16;i++) {
      rbMaskTemp << (rodBusyMask & 0x1);
      rbMaskTemp << "  ";
      rodBusyMask>>=1;
    }
    rodBusyMaskInIs = rbMaskTemp.str();

    rodBusyState=m_tim->regFetch(TIM_REG_RB_STAT);
    std::ostringstream rbStateTemp;
    rbStateTemp << "BUSY        ";
    for (int i=0;i<16;i++) {
      rbStateTemp << (rodBusyState & 0x1);
      rbStateTemp<<"  ";
      rodBusyState>>=1;
    }
    rodBusyStateInIs = rbStateTemp.str();
    
    if (m_is !=NULL) {
      try
	{
	  m_is->publish(m_name + "/L1IDCounterValue", l1idCounterValue);
	  m_is->publish(m_name + "/ECRCounterValue", ecrCounterValue);
	  m_is->publish(m_name + "/TriggerType", triggerType);
	  m_is->publish(m_name + "/RODBusyMask", rodBusyMaskInIs);    
	  m_is->publish(m_name + "/RODBusyState", rodBusyStateInIs);
	  m_is->publish(m_name + "/RodBusyDuration", rodBusyDuration);
	  if (!m_FFTV_disable) {
	    unsigned int nFV   = (m_tim->regFetch( TIM_REG_FV_IDH )<<16) + m_tim->regFetch( TIM_REG_FV_IDL );
	    unsigned int nFVem = m_tim->regFetch( TIM_REG_FV_EMERG_CNT );
	    unsigned int nFVB  = (m_tim->regFetch( TIM_REG_FVB_IDH )<<16) + m_tim->regFetch( TIM_REG_FVB_IDL );
	    FFTVemergencyState = (int)(((m_tim->regFetch(TIM_REG_STATUS3))>>13)&0x1);
	    m_is->publish(m_name + "/FFTVBusyFraction", fftvBusyFraction);
	    m_is->publish(m_name + "/FFTVCounterValue", nFV);
	    m_is->publish(m_name + "/FFTVemergencyCounter", nFVem);
	    m_is->publish(m_name + "/FFTV_B_BusyFraction", fftv_B_BusyFraction);
            m_is->publish(m_name + "/FFTV_B_CounterValue", nFVB);
	    m_is->publish(m_name + "/FFTVBusyPerc", fftvBusyPerc);
	    m_is->publish(m_name + "/FFTV_B_BusyPerc", fftv_B_BusyPerc);
	    int fftvEmergencyStateInIS(-1);
	    if (m_is->exists(m_name + "/FFTVemergencyState")) fftvEmergencyStateInIS = m_is->read<int>(m_name + "/FFTVemergencyState");
	    if (FFTVemergencyState!=fftvEmergencyStateInIS) m_is->publish(m_name + "/FFTVemergencyState", FFTVemergencyState);
	  }
	  if (numOfTimOkChanges>0) m_is->publish(m_name + "/TimOkChanges", numOfTimOkChanges);
	  if (numOfQpllLockChanges>0) m_is->publish(m_name + "/QpllLockChanges", numOfQpllLockChanges);
	  
	  std::string timingScanStatus = "INACTIVE"; //ACTIVE, INACTIVE, ABORT common to all TIMs
	  //each TIM performs scans separately, writing in IS current delay, find 
	  if (m_is->exists(m_name + "/TIMING_SCAN_STATUS")) timingScanStatus = m_is->read<std::string>(m_name + "/TIMING_SCAN_STATUS");
	  if (timingScanStatus.find("INACTIVE")== std::string::npos) { 
	    if (timingScanStatus.find("ABORT")!= std::string::npos) {
	      if (m_scanStopVal!= -400.0) {
		m_scanStopVal = -400.0;
		m_scanMode = SETDELAY;
	      }
	    }

	    if (m_scanMode == INIT) {
	      bool connectToATLAS = true;
	      std::string partitionName =  m_is->read<std::string>("TIMING_SCAN_DTPART");
	      std::string monIsServerName = "RunParams"; 
	      try {
		m_isDT = new PixISManager(partitionName, monIsServerName);
	      } catch(...) {
		ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__," (init timing scan) ERROR!! problems creating IS server for "+ partitionName +" partition, will not read info from this partition."));
		m_isDT = NULL;
		m_is->publish(m_name + "/TIMING_SCAN_STATUS", "INACTIVE");
		connectToATLAS = false;	 
	      }
	      if (connectToATLAS) {
		std::cout << "TimPixTrigController::probe (init timing scan)" << m_name << " IS manager will read LumiBlock from partition: " <<  partitionName << " using IS server: " <<monIsServerName << std::endl;
	      }
	    }
	    bool complete = true;
	    //read LB
	    LumiBlock lbInfo;
	    if (m_isDT !=NULL) {
	      try
		{
		  if (m_isDT->exists("LumiBlock")) {
		    lbInfo = m_isDT->read<LumiBlock>("LumiBlock");
		  }
		  else complete = false; 
		}
	      catch(PixISManagerExc& exc){
		ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__, "PixISManager Exception caught on publishing in IS"+ exc.getISmanName()));
		complete =false;
	      }
	    }
	  
	    if (m_scanMode == INIT) {
	      float startVal = 0.0;
	      //read delays
	      if (m_is->exists("TIMING_SCAN_STARTVALUE")) startVal = m_is->read<float>("TIMING_SCAN_STARTVALUE");
	      else complete = false;
	      if (m_is->exists("TIMING_SCAN_STEPSIZE")) m_scanStepSize = m_is->read<float>("TIMING_SCAN_STEPSIZE");
	      else complete = false;
	      if (m_is->exists("TIMING_SCAN_STOPVALUE")) m_scanStopVal = m_is->read<float>("TIMING_SCAN_STOPVALUE");
	      else complete = false;
	      if (m_is->exists("TIMING_SCAN_STEPDUR")) m_scanStepDur = m_is->read<unsigned int>("TIMING_SCAN_STEPDUR");
	      else complete = false; 
	      //Check if what is neeed is read
	      if (complete == false) {
		std::cout << "Timing scan parameters could not be found in IS, stopping ..." << std::endl;
		m_is->publish(m_name + "/TIMING_SCAN_STATUS", "INACTIVE");
	      } else {
		m_scanMode = SETDELAY;
		m_ttcrxCurrentDelay = m_ttcrxCoarseDelay*25+m_ttcrxFineDelay+startVal; 
	      }
	    } else if (m_scanMode == SETDELAY) {
	      if (m_ttcrxCurrentDelay > m_ttcrxCoarseDelay*25+m_ttcrxFineDelay+m_scanStopVal) {
		m_ttcrxCurrentDelay = m_ttcrxCoarseDelay*25+m_ttcrxFineDelay; 
		m_scanMode = FINISHED;
		m_targetLumi = lbInfo.LumiBlockNumber + 2; //assume that all TIMs finishing time < 4 min 
		m_is->publish(m_name + "/TIM_SCAN_STATUS", "FINISHED");
	      } else {
		m_scanMode = RUNNING;
		//calculate target LB
		m_targetLumi = lbInfo.LumiBlockNumber + m_scanStepDur; 
		m_is->publish(m_name + "/TIM_SCAN_STATUS", "RUNNING");
	      }
	      setVmeRodBusy(); //force busy
	      setTtcRxDelay(m_ttcrxCurrentDelay); //change && set delay
	      float readTTCrxDelay = readTtcRxDelay();
	      std::cout << m_name <<" Readback TTCrx delays, coarse delay: " << (int)(readTTCrxDelay/25) <<" fine delay: "<< (double)(fmod(readTTCrxDelay,25)) << std::endl;
	      m_is->publish(m_name+"/TTCrxFineDelay",  (double)(fmod(readTTCrxDelay,25)));
	      m_is->publish(m_name+"/TTCrxCoarseDelay", (int)(readTTCrxDelay/25));
	      releaseVmeRodBusy();
	      std::cout << m_name <<" TTCRX delay scan, delay set to " << m_ttcrxCurrentDelay <<" current LBN: "<< lbInfo.LumiBlockNumber<< " will wait for LBN: "<< m_targetLumi << std::endl;
	    } else if (m_scanMode == RUNNING) {
	      if (lbInfo.LumiBlockNumber >= m_targetLumi) {
		m_ttcrxCurrentDelay+=m_scanStepSize;
		m_scanMode = SETDELAY;
	      }
	    } else if (m_scanMode == FINISHED) {
	      if (lbInfo.LumiBlockNumber >= m_targetLumi) { 
		m_is->publish(m_name + "/TIMING_SCAN_STATUS", "INACTIVE");
		m_scanMode = INIT;
	      }
	    }
	  }
	}
      catch(PixISManagerExc& exc){
	ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"PixISManager Exception caught on publishing in IS" + exc.getISmanName()));
      }
    }
  }
  // Trigger Veto stuff ?
  // clean up IS
  return FFTVemergencyState;
}

void TimPixTrigController::setIOMode(enum timIOMode mode){
  switch(mode){
  case INT_TRIGGER:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_TRIG );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_TRIG );
    break;
  case EXT_TRIGGER:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_TRIG );
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_TRIG );
    break;
  case INT_CLOCK:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_CLK );
    break;
  case EXT_CLOCK:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_CLK );
    break;
  case INT_ECR:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_ECR );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_ECR );
    break;
  case EXT_ECR:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_ECR );
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_ECR );
    break;
  case NO_ECR:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_ECR );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_ECR );
    break;
  case INT_BCR:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_BCR );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BCR );
    break;
  case EXT_BCR:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_BCR );
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BCR );
    break;
  case NO_BCR:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_BCR );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BCR );
    break;
  case INT_FER:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_FER );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_FER );
    break;
  case EXT_FER:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_FER );
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_FER );
    break;
  case NO_FER:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_FER );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_FER );
    break;
  case INT_BUSY:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_BUSY );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BUSY );
    break;
  case EXT_BUSY:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_BUSY );
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BUSY );
    break;
  case ROD_BUSY_SET:
    m_tim->loadBitSet( TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY); // enables busy's from RODs stop triggers in TIM
    break;
  case ROD_BUSY_CLEAR:
    m_tim->loadBitClear( TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY);
    break;
  case NO_BUSY:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_INT_BUSY );
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_BUSY );
    break;
  case RANDOM:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_RANDOM );
    break;
  case NO_RANDOM:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_RANDOM );
    break;
  case WINDOW:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_WINDOW );
    break;
  case NO_WINDOW:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_WINDOW );
    break;
  case EXT_CAL:
    m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_EXT_CAL );
    break;
  case INT_CAL:
    m_tim->loadBitClear( TIM_REG_ENABLES, TIM_BIT_EN_EXT_CAL );
    break;  
  default:
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Wrong IOMode selected!" ));
    break;
  }
  
}

//! Commands
void TimPixTrigController::intTrigStart(){
  m_tim->loadBitSet( TIM_REG_ENABLES, TIM_BIT_EN_INT_TRIG );
}

void TimPixTrigController::intTrigStop(){
  m_tim->intTrigStop();
}

void TimPixTrigController::enableTTCRunMode(){
  m_tim->loadBitSet( TIM_REG_COMMAND, TIM_BIT_EN_TTC );
}

void TimPixTrigController::disableTTCRunMode(){
  m_tim->loadBitClear( TIM_REG_COMMAND, TIM_BIT_EN_TTC ); 
}

void TimPixTrigController::singleTrigger(){
  m_tim->issueCommand(TIM_VTRG);
}

void TimPixTrigController::singleECR(){
  m_tim->issueCommand(TIM_VECR);
}

void TimPixTrigController::singleBCR(){
  m_tim->issueCommand(TIM_VBCR);
}

void TimPixTrigController::singleFER(){
  m_tim->issueCommand(TIM_VFER);
}

void TimPixTrigController::singleCAL(int delay){
  setTriggerDelay(delay);
  m_tim->issueCommand(TIM_VCAL);
}

int TimPixTrigController::pow(int a, int k) {
  int ret = 1;
  for (int i = 0; i < k; ++i) {
    ret *= a;
  }
  return(ret);
}

int TimPixTrigController::char2int(char* a) {
  int r=0;
  for (int k = 0; k<4; k++) {
    if (a[k]=='1') r+=pow(2,3-k);
    else if (a[k]=='0');
    else { r = -1; break;}
  }
  return r;
}


bool  TimPixTrigController::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  unsigned int tmprev=revision;
  std::cout << "revision passed to write is " << tmprev << std::endl;
  // if(m_conf->write(DbServer, domainName, tag, m_name,"TimPixTrigController",ptag, tmprev)) old
  if(m_conf->write(DbServer, domainName, tag, m_name,m_conf->type(),ptag, tmprev))
    return true;
  else {
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__, "ERROR in writing config on DBserver"));
    return false;
  }
}

bool TimPixTrigController::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  if(m_conf->read(DbServer, domainName, tag, m_name, revision))
    return true;
  else {
    ers::error(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__, "ERROR in reading config on DBserver"));
    return false;
  } 
}
#if 0
void TimPixTrigController::setTemp() {
  m_conf->penTag("Temp");
}
#endif

void TimPixTrigController::dumpCfgRegisters(){
  uint16_t regstatus;

  std::cout << "DUMPING CONFIGURATION SETTINGS FOR THE TIM: " << m_name << std::endl;
  //==> DELAYS
  std::cout << "==> Trigger delay value (BCs): " << dec << m_tim->regFetch(TIM_REG_DELAY) << std::endl;
  float readTTCrxDelay = readTtcRxDelay();
  std::cout << "==> TTCrx coarse delay: " << (int)(readTTCrxDelay/25) << std::endl;
  std::cout << "==> TTCrx fine delay: "<< (double)(fmod(readTTCrxDelay,25)) << std::endl;

  //==> BCID OFFSET
  regstatus = m_tim->regFetch( TIM_REG_BCID );
  std::cout << "==> BCID offset: "<< ((regstatus & 0xf000) >> 12) << std::endl;

  //==> ROD BUSY MASK
  std::cout << "==> ====================== RodBusy status =================== " << std::endl;
  regstatus=m_tim->regFetch(TIM_REG_RB_MASK);
  std::cout << "==> ROD Slot#:  5  6  7  8  9 10 11 12 14 15 16 17 18 19 20 21" << std::endl;
  std::cout << "==> OR'ed       ";
  for (int i=0;i<16;i++) {
    std::cout << (regstatus & 0x1) << "  ";
    regstatus>>=1;
  }
  std::cout << std::endl;

  bool fftv_enabled(true);
  if ((m_tim->regFetch(TIM_REG_DEBUG_CTL) & TIM_BIT_DEBUGCTL_FVDISABLE) && 
      ((m_tim->regFetch(TIM_REG_BUSY_STAT3)>>15) & 0x1))
    fftv_enabled=false;

  if (fftv_enabled) {
    bool fftvT_sct(true);
    if ((m_tim->regFetch(TIM_REG_STATUS3)>>15) & 0x1) fftvT_sct = false; //IBL mode
    if (m_fw>0x26) {
      std::cout << std::endl;
      regstatus=m_tim->regFetch(TIM_REG_FV_MATCH);
      std::cout << "==> Threshold for FFTV-T action: "<< (regstatus & 0x000f) << std::endl;
      if (!fftvT_sct) {
	std::cout << "==> Threshold for FFTV-T match tolerance: "<< (m_tim->regFetch(TIM_REG_FV_DELTAT) & 0xffff) << std::endl;
	std::cout << "==> Threshold for FFTV-T emergency: "<< ((regstatus>>8) & 0x00ff) << std::endl;
	std::cout << "==> Leak rate of FFTV-T emergency counter: "<< (m_tim->regFetch(TIM_REG_FV_EMERG_TO) & 0xffff) << std::endl;
      }
    }
    if (m_fw>0x40) {
      std::cout << std::endl;
      std::cout << "==> Threshold for FFTV-B action: "<< (m_tim->regFetch(TIM_REG_FVB_MATCH) & 0x001f)<< std::endl;
      regstatus=m_tim->regFetch(TIM_REG_CONTROL);
      cout << "==> FFTV-B snapshot mode: " << ((regstatus>>1) & 0x1)<< " option (+1,-2): " << (regstatus & 0x1) << endl;
    }
  }
}

void TimPixTrigController::dumpCounters(){
  uint16_t regstatus;

  // TTC counters
  std::cout << "==> L1ID counter value: " << m_tim->fetchL1ID() << std::endl;
  regstatus = m_tim->regFetch( TIM_REG_L1IDH );
  std::cout << "==> ECR counter value: " << ((regstatus & 0xff00) >> 8) << std::endl;
  regstatus = m_tim->regFetch( TIM_REG_TTID );
  std::cout << "==> TTC trigger type: " << (regstatus & 0x00ff) << " TIM TT: "<< ((regstatus & 0xff00) >> 8) << std::endl;
  regstatus = m_tim->regFetch( TIM_REG_BCID );
  std::cout << "==> BCID value: " << (regstatus & 0x0fff) << " offset: "<< ((regstatus & 0xf000) >> 12) << std::endl;
  
  // ROD BUSY counter
  unsigned long long int den = m_tim->regFetch(TIM_REG_BCOUNTL);
  den |= ((unsigned long long int)m_tim->regFetch(TIM_REG_BCOUNTH))<<16;
  den |= ((unsigned long long int)m_tim->regFetch(TIM_REG_BCOUNTX))<<32;
  std::cout << "==> BUSY (->ATLAS) in BCs: 0x"<< hex << den << dec << std::endl;
  std::cout << "==> BUSY (->ATLAS) in s: " << setprecision(3) << (double)den * 25.0e-9 << std::endl;

  bool fftv_enabled(true);
  if ((m_tim->regFetch(TIM_REG_DEBUG_CTL) & TIM_BIT_DEBUGCTL_FVDISABLE) && 
      ((m_tim->regFetch(TIM_REG_BUSY_STAT3)>>15) & 0x1))
    fftv_enabled=false;

  if (fftv_enabled) {
    unsigned long long int num;
    
    // FFTV-T counters
    if (m_fw>0x26) {
      std::cout << std::endl;
      std::cout << "==> FFTV-T(rigger) monitoring: "<< std::endl;
      regstatus = (m_tim->regFetch( TIM_REG_FV_IDH )<<16) + m_tim->regFetch( TIM_REG_FV_IDL );
      std::cout << "==> # of FFTV-Ts: " << regstatus << std::endl;
      num = m_tim->regFetch(TIM_REG_FV_COUNTL);
      num |= ((unsigned long long int)m_tim->regFetch(TIM_REG_FV_COUNTH))<<16;
      num |= ((unsigned long long int)m_tim->regFetch(TIM_REG_FV_COUNTX))<<32;
      std::cout << "==> FFTV-T BUSY in BCs: 0x"<< hex << num << dec << std::endl;
      std::cout << "==> FFTV-T BUSY in s: " << setprecision(3) << (double)num * 25.0e-9 << std::endl;
      if (den!=0) std::cout << "==> FFTV-T fraction of BUSY: "<< (double)num/(double)den<< std::endl;
      if (m_fw>=0x50) std::cout << "==> FFTV-T Emergency counts: "<< m_tim->regFetch(TIM_REG_FV_EMERG_CNT) << std::endl;
    }
    
    // FFTV-B counters
    if (m_fw>0x40) { 
      std::cout << std::endl;
      std::cout << "==> FFTV-B(unch) monitoring: "<< std::endl;
      std::cout << "==> Threshold for FFTV-B action: "<< m_tim->regFetch(TIM_REG_FVB_MATCH)<< std::endl;
      regstatus = (m_tim->regFetch( TIM_REG_FVB_IDH )<<16) + m_tim->regFetch( TIM_REG_FVB_IDL );
      std::cout << "==> # of FFTV-Bs: " << regstatus << std::endl;
      num = m_tim->regFetch(TIM_REG_FVB_COUNTL);
      num |= ((unsigned long long int)m_tim->regFetch(TIM_REG_FVB_COUNTH))<<16;
      num |= ((unsigned long long int)m_tim->regFetch(TIM_REG_FVB_COUNTX))<<32;
      std::cout << "==> FFTV-B BUSY in BCs: 0x"<< hex << num << dec << std::endl;
      std::cout << "==> FFTV-B BUSY in s: " << setprecision(3) << (double)num * 25.0e-9 << std::endl;    
      if (den!=0) std::cout << "==> FFTV-B fraction of BUSY: "<< (double)num/(double)den<< std::endl;
    }
    regstatus = (m_tim->regFetch( TIM_REG_FV_TCOUNTH )<<16) + m_tim->regFetch( TIM_REG_FV_TCOUNTL );
    std::cout << std::endl;
    std::cout << "==> # of FFTVeto'ed triggers (by all FFTVs): " << regstatus << std::endl;
    std::cout << std::endl;
  }
}

void TimPixTrigController::clearBusyCountersAndLatches(){
  m_tim->regLoad(TIM_REG_STATUS_LCH,0x0); //clear STATUS latch (see reg 0x0c for bits) register
  m_tim->regLoad(TIM_REG_STAT3_LCH,0x0); //clear STATUS3 latch (see reg 0x4a for bits) register
  if (!m_FFTV_disable) {
    // clear BUSY registers due to FFTVetoes -> removed to allow for measurement of total dead time/run
    //
    m_tim->regLoad(TIM_REG_FV_COUNTL,0x0); //clear the FFTV-T busy duration register
    m_tim->regLoad(TIM_REG_FVB_COUNTL,0x0); //clear the FFTV-B busy duration register
  }
  // clear RodBUSY register -> removed to allow for measurement of total dead time/run
  //
  m_tim->regLoad(TIM_REG_BCOUNTL,0x0); //clear the busy duration register
}

void TimPixTrigController::setFFTVTEmergencyCounterLeakrate(){
  if (!m_FFTV_disable && m_fw>=0x50) {
    m_tim->regLoad( TIM_REG_FV_EMERG_TO, m_FFTV_emergency_counter_leakrate & 0xffff);
  } else {
    ers::warning(PixLib::pix::daq (ERS_HERE,__PRETTY_FUNCTION__,"Trying to set FFTVT counter leak rate either in too old FW or for TIM with FFTV features disabled."));
  }
}

void TimPixTrigController::setFPDebugOutputs(){
  if (m_fw>=0x43) {
    if (m_enableFPDebugOutputs) {
      m_tim->loadBitSet(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUG_TRIO);
      setFPDebugOutputsMode();
    } else m_tim->loadBitClear(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUG_TRIO); 
  }
}

void TimPixTrigController::setFPDebugOutputsMode(){
  // from  4d: TRO: TTCCLK/EvCntLStr,   TRIO: L1IDnonconseq, BO: TTCCLKDet/EvCntHStr
  //       4a: TRO: L1Accept/EvCntLStr, TRIO: L1IDnonconseq, BO: BCntRes/EvCntHStr
  //       48: TRO: TTCCLK/EvCntLStr,   TRIO: L1IDnonconseq, BO: TTCCLKDet/EvCntHStr
  //       46: TRO: TTCCLK/EvCntLStr,   TRIO: L1IDnonconseq, BO: EvCntHStr 
 if (m_fw>=0x43) {
   if (m_FPDebugOutputs_mode) m_tim->loadBitSet(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUGMODE_TRO); //strobes 
   else m_tim->loadBitClear(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUGMODE_TRO); //clocks
  }
}

bool TimPixTrigController::status(){

  uint32_t timID = m_tim->fetchTimID();

  if (timID == 0 || timID == 0xFFFF) return false;

  return true;

}


