/////////////////////////////////////////////////////////////////////
// TimPixTrigController.h
/////////////////////////////////////////////////////////////////////
// 29/08/07  Revision 1.7 (II)
//           update to match new trigger oscillator in firmware 19
//           allow internal triggering of sequencer
//           include BCID Offset
// 27/10/06  Revision 1.4 (II)
//           implementation of timTriggerMode (TTC & VME, similar to that of TimModule)
//           timIOmode expansion to accept external CAL 
//           integration of IntTrigger & ECR frequency settings from TimModule
//           singleCal method
//
// 16/07/04  Version 1.0 (GG RB)
//           Initial release
//

//! Class for the Pixel Tim

#ifndef _PIXLIB_PIXTIMRAWCONTROLLER
#define _PIXLIB_PIXTIMRAWCONTROLLER

#include "BaseException.h"
#include "PixTrigController/PixTrigController.h"
#include "RodCrate/TimDefine.h"
#include "RodCrate/TimModule.h"
#include "PixUtilities/PixMessages.h"


namespace PixLib {

  class Bits;
  class Config;
  class PixDbInterface;
  class PixConnectivity;
  class PixDbServerInterface;
  class PixISManager;

class TimPixTrigController : public PixLib::PixTrigController {
public:

  TimPixTrigController(std::string name);
  TimPixTrigController(DbRecord *dbRecord, std::string name);     //! Constructor
  TimPixTrigController(PixDbServerInterface *dbServer, std::string dom, std::string tag, std::string name);     //! Constructor

  
 virtual ~TimPixTrigController();                  //! Destructor
  
  enum TimTriggerMode: int { INTERNAL, EXTERNAL_NIM, EXTERNAL_LTP, INT_RANDOM, SEQUENCER, CAL_INJECT};
  enum CalInjTrigMode: int {ACTIVE, PASSIVE};
  enum TimSeqTriggeringMode: int {INTTRIGSEQ, EXTTRIGSEQ};
  enum TimingScanMode: int {INIT, SETDELAY, RUNNING, FINISHED};
  //! DataBase interaction
  virtual void loadConfig(DbRecord *r);
  virtual void saveConfig(DbRecord *r);
  virtual void loadConfig(PixConnectivity *conn, unsigned int rev = 0);
  virtual void saveConfig(PixConnectivity *conn, unsigned int rev = 0);
  virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Tim_Tmp", unsigned int revision=0xffffffff);
  virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);

  virtual void createVmeInterface();       
  virtual void initConfig();
  virtual Config *config();

  //! Accessors
  virtual std::string getName() {return m_name;};
  virtual std::string& mode();
  virtual int& slot();
  virtual SctPixelRod::TimModule& timModule();
  virtual unsigned int& busyMask();
  virtual float& triggerFreq();
  virtual float& ECRFreq();
  virtual float& random2Freq();
  virtual int& triggerDelay();
  virtual int& ttcrxCoarseDelay();
  virtual float& ttcrxFineDelay();
  virtual int& bcidOffset();
  virtual int& triggerType();
  virtual int& calinjCalTrigDelay();
  virtual int& calinjTrigDelay();
  virtual int& calTrigNumber();

  //! Setting Configuration into Hardware and into mem
  virtual void setTriggerFreq(const float freq);   // in KHz
  virtual void setECRFreq(const float freq);       // in Hz
  virtual void setRandom2Freq(const float freq);   // in KHz
  virtual void setTriggerDelay(int ckSteps);       // in BCOS
  virtual void setBcidOffset(int bcidOffset);      // in BCOS
  virtual void setTriggerType(int ttype);
  virtual void setSaClkDelay(int halfnsMult);

  // Setters and getters for FFTV
#if 0
  virtual bool getModeFFTV();
  virtual int getMatchThresholdFFTV();
  virtual int getMatchThresholdFFTV_B();
  virtual void setModeFFTV(bool);
  virtual void setMatchThresholdFFTV(int);
  virtual void setMatchThresholdFFTV_B(int);
#endif
  virtual void setTtcRxDelay(float delay_ns);
  virtual float readTtcRxDelay();
  virtual void setVmeTimBusy();
  virtual void setVmeRodBusy();
  virtual void releaseVmeTimBusy();
  virtual void releaseVmeRodBusy();
  virtual void enableRodBusyStopSATriggers();
  virtual void startRandom2Triggers();
  virtual void stopRandom2Triggers();
  virtual int loadSequencerFromDatafile();
  virtual int loadSequencerFromDatabase();
  virtual void setFFTVTEmergencyCounterLeakrate();
  virtual void setFPDebugOutputs();
  virtual void setFPDebugOutputsMode();
  
  virtual void configureTriggerMode();
  virtual void prepareForRunTriggerMode();
  virtual void startTriggerMode();
  virtual void stopTriggerMode();

  virtual int  publishTimInfo();  //if -1 emergency has happened
  virtual void dumpCfgRegisters();
  virtual void dumpCounters();
  virtual void clearBusyCountersAndLatches();
  virtual void setIOMode(enum timIOMode);
  
  //! Commands
  virtual void init();             // Set TIM in initial state, equivalent of pushing RESET button on TIM FP
  virtual void intTrigStart();
  virtual void intTrigStop();
  virtual void enableTTCRunMode();
  virtual void disableTTCRunMode();
  virtual void singleTrigger();
  virtual void singleECR();
  virtual void singleBCR();
  virtual void singleFER();
  virtual void singleCAL(int delay);   //in BCOS
  virtual bool status();

  //! Other
  int pow(int a, int k);
  int char2int(char* a);
  void setTemp();

 private:  
  DbRecord *m_dbRecord;
  std::string m_name;
  SctPixelRod::TimModule *m_tim;     //! Pointer to TimModule
  SctPixelRod::VmeInterface *m_vme;  //! VME interface
  int m_timSlot;                     //! Tim Slot
  std::string m_mode;                //! Tim Operating Mode
  float m_triggerFrequency;          //! internal trigger frequency in KHz 
  int m_triggerDelay;                //! internal trigger delay (ck counts) 
  int m_clockDelay;                  //! internal clock delay (0.5 ns counts)
  int m_ttcrxCoarseDelay;            //! ttcrx clock coarse delay (25 ns counts) used in DT
  float m_ttcrxFineDelay;            //! ttcrx clock fine delay (0.1 ns counts) used in DT

  float m_ttcrxCurrentDelay;         //! total ttcrx clock delay in ns (coarse + fine) used for timing scan
  float m_scanStepSize;
  float m_scanStopVal;
  unsigned int m_scanStepDur;
  unsigned int m_targetLumi;
  TimingScanMode m_scanMode;

  Config *m_conf;                    //! Tim Configuration structure
  float m_ECRFrequency;
  float m_triggerRand2Frequency;
  int m_triggerType;
  int m_bcidOffset;
  TimTriggerMode m_triggerGenMode;
  CalInjTrigMode m_calinj_injectmode;
  int m_calinj_caltrig_delay;
  int m_calinj_trig_delay;    
  int m_calinj_trig_number;   
  bool m_FFTV_disable;
  int m_FFTV_match_threshold;
  int m_FFTV_match_tolerance;
  int m_FFTV_emergency_threshold;
  int m_FFTV_emergency_counter_leakrate;
  int m_FFTV_B_match_threshold;
  bool m_FFTV_B_decremental_mode;
  bool m_enableFPDebugOutputs;
  bool m_FPDebugOutputs_mode;
  unsigned int m_fw;
  double m_fftv_B_BusyDuration_Hist;
  double m_fftvBusyDuration_Hist;
  time_t m_time;//To measure time between 2 readings

  unsigned int  m_busyMask;          //! ROD busy mask
  std::string  m_sequencerConfFile;
  TimSeqTriggeringMode m_seqtriggering_mode;

  PixISManager* m_is;
  PixISManager* m_isDT;
};

}

#endif
