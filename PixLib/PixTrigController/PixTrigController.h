/////////////////////////////////////////////////////////////////////
// PixTrigController.h
/////////////////////////////////////////////////////////////////////
// 27/10/06  Revision 1.4 (II)
//           disable FFTVeto at the moment of TIM initialisation
//           implementation of timTriggerMode (TTC & VME, similar to that of TimModule)
//           timIOmode expansion to accept external CAL 
//           integration of IntTrigger & ECR frequency settings from TimModule
//           singleCal method
// 16/07/04  Version 1.0 (GG RB)
//           Initial release
//

//! Class for the Pixel Tim

#ifndef _PIXLIB_PIXTRIGCONTROLLER
#define _PIXLIB_PIXTRIGCONTROLLER

#include "BaseException.h"
//#include "PixController/PixScanConfig.h"
#include "Config/Config.h"

#include <queue>
#include <string>


namespace SctPixelRod {
  class TimModule;
  class VmeInterface;
}

namespace PixLib {

class Bits;
class PixRunConfig;
class PixDbServerInterface;

//! PixTrigController Exception class; an object of this type is thrown in case of a TIM error
class PixTrigControllerExc : public SctPixelRod::BaseException{
public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  //! Constructor
  PixTrigControllerExc(ErrorLevel el, std::string excName, std::string name) : BaseException(name), m_errorLevel(el), m_name(name), m_excName(excName) {}; 
  //! Destructor
  virtual ~PixTrigControllerExc() {};

  //! Dump the error
  void dump(std::ostream &out) {
    out << "PixTriggerController " << m_name << " -- Level : " << dumpLevel() << " named: " << m_excName; 
  }
  std::string dumpLevel() {
    switch (m_errorLevel) {
    case INFO : 
      return "INFO";
    case WARNING :
      return "WARNING";
    case ERROR :
      return "ERROR";
    case FATAL :
      return "FATAL";
    default :
      return "UNKNOWN";
    }
  }
  //! m_errorLevel accessor
  ErrorLevel getErrorLevel() { return m_errorLevel; };
  //! m_name accessor
  std::string getCtrlName() { return m_name; };
  //! m_excName accessor
  std::string getExcName() { return m_excName; };
private:
  ErrorLevel m_errorLevel;
  std::string m_name;
  std::string m_excName;
};

class PixTrigController {
public:
    enum timIOMode{INT_TRIGGER, EXT_TRIGGER, INT_CLOCK, EXT_CLOCK, INT_ECR, EXT_ECR, NO_ECR, INT_BCR, EXT_BCR, NO_BCR, RANDOM, NO_RANDOM, INT_FER, EXT_FER, NO_FER, WINDOW, NO_WINDOW, INT_BUSY, EXT_BUSY, ROD_BUSY_SET, ROD_BUSY_CLEAR, NO_BUSY, EXT_CAL, INT_CAL};  
  virtual ~PixTrigController() {}                 //! Destructor

  //! DataBase interaction
  virtual void loadConfig(DbRecord *r) = 0;
  virtual void saveConfig(DbRecord *r) = 0;
  virtual void initConfig() = 0;
  virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Tim_Tmp", unsigned int revision=0xffffffff) = 0;
  virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff) = 0;
  
  virtual Config *config() = 0;// { return m_conf; };

  //! Accessors
  virtual std::string& mode() = 0;
  virtual int& slot() = 0;
  virtual SctPixelRod::TimModule& timModule() = 0;
  virtual unsigned int &busyMask() = 0;
  virtual float& triggerFreq() = 0;
  virtual float& ECRFreq() = 0;
  virtual float& random2Freq() = 0;
  virtual int& triggerDelay() = 0;
  virtual int& ttcrxCoarseDelay() = 0;
  virtual float& ttcrxFineDelay() = 0;
  virtual int& bcidOffset() = 0;
  virtual int& triggerType() = 0;
  virtual int& calinjCalTrigDelay() = 0;
  virtual int& calinjTrigDelay() = 0;
  virtual int& calTrigNumber() = 0;

  //! Setting Configuration into Hardware
  virtual void setTtcRxDelay(float delay_ns)    = 0;
  virtual float readTtcRxDelay()    = 0;
  virtual void setTriggerFreq(const float freq) = 0;
  virtual void setECRFreq(const float freq)     = 0;
  virtual void setRandom2Freq(const float freq) = 0;
  virtual void setTriggerDelay(int ckSteps)     = 0;
  virtual void setTriggerType(int ttype)        = 0;
  virtual void setSaClkDelay(int halfnsMult)    = 0;
  
  virtual void setVmeTimBusy() = 0;
  virtual void setVmeRodBusy() = 0;
  virtual void releaseVmeTimBusy() = 0;
  virtual void releaseVmeRodBusy() = 0;
  virtual void enableRodBusyStopSATriggers() = 0;
  virtual void startRandom2Triggers() = 0;
  virtual void stopRandom2Triggers() = 0;
  virtual int loadSequencerFromDatafile() = 0;
  virtual int loadSequencerFromDatabase() = 0;
  virtual void setFFTVTEmergencyCounterLeakrate() = 0;
  virtual void setFPDebugOutputs() = 0;
  virtual void setFPDebugOutputsMode() = 0;

  virtual void configureTriggerMode() = 0;
  virtual void prepareForRunTriggerMode() = 0;
  virtual void startTriggerMode() = 0;
  virtual void stopTriggerMode() =0;

  virtual int publishTimInfo() =0;
  virtual void dumpCfgRegisters() =0;
  virtual void dumpCounters() =0;
  virtual void clearBusyCountersAndLatches() =0;

  virtual void setIOMode(enum timIOMode) = 0;
  
  //! Commands
  virtual void init() = 0;
  virtual void intTrigStart() = 0;
  virtual void intTrigStop() = 0;
  virtual void enableTTCRunMode() = 0;
  virtual void disableTTCRunMode() = 0;
  virtual void singleTrigger() = 0;
  virtual void singleECR() = 0;
  virtual void singleBCR() = 0;
  virtual void singleFER() = 0;
  virtual void singleCAL(int delay) = 0;
  virtual bool status() =0;
};

}

#endif
