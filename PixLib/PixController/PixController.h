/////////////////////////////////////////////////////////////////////
// PixController.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 08/04/03  Version 1.0 (PM)
//           Initial release
//

//! Abstract class for the generic pixel module controller

#ifndef _PIXLIB_PIXCONTROLLER
#define _PIXLIB_PIXCONTROLLER

#include <vector>
#include <string>

#include "PixModuleGroup/PixModuleGroup.h"
#include "BaseException.h"
#include "PixController/PixRunConfig.h"
#include "PixScanBase.h"

namespace SctPixelRod {
  class BocCard;
}

namespace PixLib {

class Bits;
class Histo;
class Config;
class PixDbServerInterface;
class PixModule;

//! Pix Controller Exception class; an object of this type is thrown in case of a controller error
class PixControllerExc : public SctPixelRod::BaseException{

public:
  enum ErrorLevel{INFO, WARNING, ERROR, FATAL};
  //! Constructor
  PixControllerExc(ErrorLevel el, std::string name) : BaseException(name), m_errorLevel(el), m_name(name) {};
  //! Destructor
  virtual ~PixControllerExc() {};

  //! Dump the error
  virtual void dump(std::ostream& out) {
    out << "Pixel Controller " << m_name << " -- Level : " << dumpLevel();
  }
  std::string dumpLevel() {
    switch (m_errorLevel) {
    case INFO:
      return "INFO";
    case WARNING:
      return "WARNING";
    case ERROR:
      return "ERROR";
    case FATAL:
      return "FATAL";
    default:
      return "UNKNOWN";
    }
  }
  //! m_errorLevel accessor
  ErrorLevel getErrorLevel() { return m_errorLevel; };
  //! m_name accessor
  std::string getCtrlName() { return m_name; };
private:
  ErrorLevel m_errorLevel;
  std::string m_name;
};

class PixScan;
class DbRecord;

class PixController :public EnumHistogramType, public EnumMccBandwidth {

public:
  //! Factory methods for creating the different PixControllers, given a type string and possibly a database (connection)
  //  Note that the factory does not take ownership, but ownership has to be handled by the invoking class
  static PixController *make(PixModuleGroup &grp, DbRecord *dbrec, std::string const& type);
  static PixController *make(PixModuleGroup &grp, PixDbServerInterface *dbServer, std::string const& dom, std::string const& tag, std::string const& type);
  static PixController *make(PixModuleGroup &grp, std::string const& type);

  //! Constructors
  PixController(PixModuleGroup &modGrp, DbRecord *dbRec):
                m_modGroup(modGrp), m_dbRecord(dbRec) {
    m_name = m_modGroup.getRodName();
  }
  PixController(PixModuleGroup &modGrp):
                m_modGroup(modGrp), m_dbRecord(nullptr) {
    m_name = m_modGroup.getRodName();
  }
  //TBi TODO: check if this is actually used somewhere, it seems like half the functionality is not properly implemented
  PixController(PixModuleGroup &modGrp, PixDbServerInterface* /*dbServer*/, std::string const & /*tag*/):
                m_modGroup(modGrp), m_dbRecord(nullptr) {
    m_name = m_modGroup.getRodName();
  }

  virtual ~PixController();                                           //! Destructor
  virtual void initHW() = 0;                                          //! Hardware (re)init
  virtual std::string getipAddress(){return "";}                      //! get the ipAddress of the ROD
  virtual void loadConfig(DbRecord *rec);                             //! Read config from DB
  virtual void saveConfig(DbRecord *rec);                             //! Write config to DB

  //TBi: This function is deprecated, but still 'used' in the MCCI1 - check how to fix this and replace
  virtual void sendCommand(Bits commands, int moduleMask) {};                                    //! Send command no out

  virtual void writeModuleConfig(PixModule& mod, bool forceWrite = false) = 0;                    //! Write module configuration
  virtual void sendModuleConfig(unsigned int moduleMask, unsigned int bitMask=0x3fff) = 0;        //! Send module configuration
  virtual void sendModuleConfigWithPreampOff(unsigned int moduleMask, unsigned int bitMask) = 0;

  //! read back the latches. This is vaid only for this controller
  virtual void readBackFEI4Latches(unsigned int moduleMask) {;};
  virtual void ActivateECRReconfiguration(uint32_t moduleMask, bool activate, uint32_t params = 0)=0;
  virtual uint32_t getRxMaskFromModuleMask(uint32_t moduleMask){ return 0x0;};
  //virtual void setCalibrationMode() = 0;
  virtual void setConfigurationMode() = 0;
  virtual void enableLink(int link)=0;
  virtual void writeScanConfig(PixScan &scn) = 0;                                                               //! Write scan parameters
  virtual void startScan() = 0;                                                                                 //! Start a scan

  //! Read an histogram
  virtual void getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector<std::vector<Histo*>>& his) = 0;
  //! Read an histogram, with information about which loop you're in, for Fei4
  virtual void getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector<std::vector<Histo*>>& his, int loopStep);

  virtual bool moduleActive(int nmod) = 0;                    //! true if a module is active during scan or datataking
  virtual void startRun(PixRunConfig* run) = 0;                   //! Start a run
  virtual void stopRun(PixHistoServerInterface *hsi) = 0; //! Terminates a run
  virtual int runStatus() = 0;                                //! Check the status of the run
  virtual int nTrigger() = 0;                                 //! Returns the number of trigger processed so far
  virtual int getLoop0Parameter() = 0;                                 //! Returns the number of trigger processed so far
  virtual int getLoop1Parameter() = 0;                                 //! Returns the number of trigger processed so far
  virtual int getLoop2Parameter() = 0;                                 //! Returns the number of trigger processed so far
  virtual int getMaskStage() = 0;                                 //! Returns the number of trigger processed so far

  //add missing member function for full abstraction
  virtual void readModuleConfigIdentifier(int moduleId, char* idStr, char* tag, uint32_t* revision, uint32_t* crc32chksm) =0 ;
  virtual void stopHistogramming(int slave)=0;
  virtual  unsigned int &fmtLinkMap(int fmt)=0;
  virtual void disableLink(int link)=0;
  virtual void disableSLink( );
  virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Rod_Tmp", unsigned int revision=0xffffffff)=0;
  virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff)=0; 
  virtual void prepareBocScanDsp(PixScan *scn)=0;
  virtual void prepareMonLeakScan(PixScan *scn)=0;
  virtual void stopScan()=0;
  virtual void abortScan(); // abort scan does a harder cleanup than stopScan, implemented for IBL
  virtual uint32_t getRodSerialNumber()=0;
  virtual void writeRunConfig(PixRunConfig* run)=0;
  virtual void configureEventGenerator(PixRunConfig* run, bool active)=0;
  virtual void setRunNumber(unsigned int runN)=0;
  virtual void setEcrCount(unsigned int ecrC)=0;
  virtual unsigned int getEcrCount()=0;
  //virtual int ctrlID()=0; //former rodSlot
  virtual void initBoc()=0;
  virtual void resetBoc()=0;
  virtual unsigned int readCmdStat()=0;
  virtual int moduleOutLink(int nmod)=0;
  virtual void resetModules(uint32_t moduleMask, uint32_t syncW){ };
  virtual void dumpRodBusyStatus( ){ };
  virtual uint32_t getRunMask(){return 0x0;};
  //inline void setCtrlVerbose(bool yn = true) {m_ctrlVerbose = yn;};             //! Call this function to set verbosity in the controller

  // Accessors
  PixModuleGroup &getModGroup() { return m_modGroup; };       //! Parent module group accessor
  PixModuleGroup const & getModGroup() const { return m_modGroup; };       //! Parent module group accessor
  std::string getCtrlName() { return m_name; };               //! Group name accessor
  Config &config() { return *m_conf; };                       //! Configuration object accessor

protected:
  Config *m_conf;                  //! Configuration object
  PixModuleGroup &m_modGroup;      //! Pointer to the module group using this controller
  bool m_ctrlVerbose;              //! Control verbosity in the controller

private:
  virtual void configInit() = 0;   //! Init configuration structure

  std::string m_name;              //! Name of the controller
  DbRecord *m_dbRecord;            //! DbRecord
};

}

#endif
