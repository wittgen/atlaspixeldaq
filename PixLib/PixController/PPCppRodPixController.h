//////////////////////////////////////////////////////////////////
// PPCppRodPixController.h
/////////////////////////////////////////////////////////////////////
//
// Author: K. Potamianos <karolos.potamianos@cern.ch>
// Date: 2013-X-10
// Author: Russell Smith, L Jeanty <laura.jeanty@cern.ch>
// Date: 2014-X-02/03
// Description: This class interfaces with the PPCpp code (C++) running
// 		on the PPC (IBL ROD). It uses the command pattern.
// Based on: IBLRodPixController
//

//! Class for the IBL ROD

#ifndef _PIXLIB_PPCPPRODPIXCONTROLLER
#define _PIXLIB_PPCPPRODPIXCONTROLLER

#include "PixController.h"
#include "PixUtilities/PixMessages.h"
#include "RodCrate/AbstrRodModule.h"
#include "PixModule/PixModule.h"
#include "RodCrate/RodModule.h"
#include "RodCrate/IblRodModule.h"

#include "DataTakingConfig.h"
#include "IblRodScanStatus.h"
#include "iblSlaveCmds.h"
/* cannot really include registeraddress.h clashes with BocAddrresses.h */

#include "RodCrate/registeraddress.h"
#include "GetFEConfigHash.h"

/* ugly hack */
#undef BOC_RESET
#undef BOC_STATUS
#undef BOC_MODULE_TYPE
#undef BOC_MANUFACTURER
#undef BOC_SERIAL_NUMBER

class SendModuleConfig;

namespace PixLib {
  class PixDbServerInterface;
  class PixRunConfig;
  class PixModule;
  class IblScanOptions;

  //! Pix Controller Exception class; an object of this type is thrown in case of a ROD error
  class PPCppRodPixControllerExc : public PixControllerExc {
  public:
    // Todo: check how much the upstream system relies on them, and if we can discard some that are irrelevant.
    enum ErrorType{OK, BAD_SLOT_NUM, DMA_NOT_PERMITTED, TOO_MANY_MODULES, INVALID_MODID, NO_VME_INTERFACE,
		   ILLEGAL_HASHING_SCHEME, NON_UNIFORM_POINTS_NOT_IMPLEMENTED, IS, BAD_CONFIG, INCONSISTENCY,
		   NO_PPC_CONNECTION, OTHER};
    //! Constructor
      PPCppRodPixControllerExc(ErrorType type, ErrorLevel el, std::string name, int info1) :
	PixControllerExc(el, name), m_errorType(type), m_info1(info1) {};
	PPCppRodPixControllerExc(ErrorType type, ErrorLevel el, std::string name) :
	  PixControllerExc(el, name), m_errorType(type), m_info1(0) {};

	  //! Dump the error
	  virtual void dump(std::ostream &out) {
	    out << "PPCpp Controller " << getCtrlName();
	    out << " -- Type : " << dumpType();
	    out << " -- Level : " << dumpLevel() << std::endl;
	  }
	  //! m_errorType accessor
	  ErrorType getErrorType() { return m_errorType; };
  private:
	  std::string dumpType() {
	    std::string message;
	    switch (m_errorType) {
	    case OK :
	      return "No errors";
	    case BAD_SLOT_NUM :
	      std::ostringstream(message) <<  "Invalid ROD slot number (" << m_info1 << ")" ;
	      return message;
	    case BAD_CONFIG :
	      return "Error reading from module configuration";
	    case NO_PPC_CONNECTION :
	      return "No connection to the PPC server";
	    default :
	      return "Uknown error";
	    }
	  }
	  ErrorType m_errorType;
	  int m_info1; //, m_info2;
  };

  class PPCppRodPixController : public PixController {

#define MAX_GROUPS 4

#define MAX_LVL1ACCEPT 15
#define MAX_SKIPPEDLVL1 15
#define MAX_BITFLIPS 7
#define MAX_MCCFLAGS 255
#define MAX_FEFLAGS 255
#define MAX_HITS 255
#define MAX_MODULES 32

  public:
    PPCppRodPixController(PixModuleGroup &modGrp, DbRecord *dbRecord);       //! Constructor
    PPCppRodPixController(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag);       //! Constructor
    PPCppRodPixController(PixModuleGroup &modGrp);                           //! Constructor
    virtual ~PPCppRodPixController() = default;                                        //! Destructor

    virtual void initHW();                                              //! Hardware (re)init
    virtual std::string getipAddress(){return m_ipAddress;}             //! return the ROD ipAddress

    void writeModuleConfig(PixModule& mod, bool forceWrite = false);                              //! Write module configuration
    //virtual void readModuleConfig(PixModule& mod);                                              //! Read module configuration
    virtual void readModuleConfigIdentifier(int moduleId, char* idStr, char* tag, uint32_t* revision, uint32_t* crc32chksm);  //! Read module configuration identifier
    virtual void sendModuleConfig(unsigned int moduleMask = 0, unsigned int bitMask=0x3fff );         //! Send module configuration
    virtual void sendModuleConfigWithPreampOff(unsigned int moduleMask, unsigned int registerMask = 0x3fff);         //! Send module configuration
    
    //! read back the latches. This is vaid only for this controller
    virtual uint32_t getRxMaskFromModuleMask(uint32_t moduleMask);
   // bool isScanActive(){return m_scanActive;}

    virtual void setConfigurationMode();
    //virtual void setRunMode();
    void setRunMode();
    virtual void disableLink(int link);
    virtual void disableSLink( );
    virtual void enableLink(int link);
    virtual uint32_t getRunMask();
    virtual void writeScanConfig(PixScan &scn);                               //! Write scan parameters
    virtual void startScan();                                                 //! Start a scan
    //virtual bool fitHistos();                                                 //! Fit collected histograms
    virtual void getHisto(HistogramType type, unsigned int xmod, unsigned
			  int slv, std::vector< std::vector<Histo*> > &his);  //! Read an histogram
    virtual void getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> > &his, int loopStep);  //! Read an histogram

    virtual void getMonLeakscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his);
    virtual void getOptoscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his);
    virtual bool getRowColFromPixelIndex(int index, int FE, int &row, int &col);
    virtual void setRunNumber(unsigned int runN); //! Sets the run number
    virtual void setEcrCount(unsigned int ecrC);  //! Sets the ECR counter
    virtual unsigned int getEcrCount();           //! Read the ECR counter
    virtual void writeRunConfig(PixRunConfig* run); //! Sets the run configuration parameters from PixModuleGroup
    virtual void configureEventGenerator(PixRunConfig* run, bool active);//! Dumps config mask into FPGA registers
    virtual void startRun(PixRunConfig* run);           //! Start a run
    virtual void stopRun(PixHistoServerInterface *hsi);  //! Terminates a run
    void dumpVetoCounters();
    virtual int runStatus();                        //! Check the status of the run
    virtual int nTrigger();                         //! Returns the number of trigger processed so far
    
    bool moduleActive(int nmod); // is this module being used in the current running?
    virtual int moduleOutLink(int nmod); 
    std::pair< int, int> getModuleRx(int modId); // what is the rx for the module by modId?
    virtual int getSlaveFromModID(int modId);
    virtual int getModuleIDFromRx(int rxCh);
    virtual int getFeNumberFromRx(int rxCh);

    virtual void prepareBocScanDsp(PixScan *scn);
    //virtual void prepareMonLeakScanDsp(PixScan *scn);
    virtual void prepareMonLeakScan(PixScan *scn);

    //virtual void setConfigurationDebugMode();
    virtual void startHistogramming(int slave ,const IblSlvHistCfg &histCfg);
    virtual void stopHistogramming(int slave);
    virtual void setupHistogramming( );
    virtual void getRunOccHisto(PixHistoServerInterface *hsi);
    virtual void stopScan();
    virtual void abortScan();

    //virtual void setGroupId(int module, int group);
    //virtual void enableModuleReadout(int module);
    //virtual void disableModuleReadout(int module, bool trigger);

    //virtual void setVerboseLevel(int level, int buffernr);

    virtual void initBoc();
    virtual void resetBoc();
   // virtual int ctrlID() {  return m_ctrlID; };
    virtual  unsigned int &fmtLinkMap(int fmt);   

    virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Rod_Tmp", unsigned int revision=0xffffffff);
    virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);

    virtual int &grId(int pp0, int ph) {
      std::cout << __PRETTY_FUNCTION__ << std::endl;

      static int a;
      if (ph > 0 && ph <= 4 && pp0 >=0 && pp0 <4) {
	return m_grId[pp0][ph-1];
      }
      a = -1;
      return a;
    }

    uint32_t getTriggersReceived();

    //void downloadDspHistos(PixHistoServerInterface *hsi);

    int getMaskStage() {
      return m_scanResult.mask;
    }

    int getLoop0Parameter() {
      return m_scanResult.par;
    }

    int getLoop1Parameter() {
      return m_scanResult.par1;
    }

    int getLoop2Parameter() {
      return m_scanResult.par2;
    }
    virtual uint32_t getRodSerialNumber();
    virtual void resetFPGA( );
    void reset() {
      std::cout << __PRETTY_FUNCTION__ << std::endl;
      if(m_rod) m_rod->reset();
    };
    virtual unsigned int readCmdStat() {
      std::cout << __PRETTY_FUNCTION__ << std::endl;
      return m_rod->readRegister(RTR_CMND_STAT, 0, 32);
    };
    virtual void printScanOptions(IblScanOptions& opts);

    virtual void sendCommand(Command &cmd) const;
    void updateNTrigger();

  protected:
    virtual void configInit();         //! Init configuration structure
    virtual void initController();             // avoid duplication of initializiation in constructor
    void defaultInit();//Common init for Pixel and IBL

    /*Compares the passed configuration to the one currently loaded to the ROD for a given channel,
     * returns true if they match, false otherwise*/
    template<class TConf>
    bool compareRODHostHash(int channel, GetFEConfigHash::feFlavour fe_flavour, TConf const& config) const;

    virtual void outputMessageForConfig(SendModuleConfig &cmd);

    EnumScanParam::ScanParam m_scanLoopParam0; // Used to determine if Vcal-to-Charge converion needed in S-curve histograms
    std::unique_ptr<SctPixelRod::IblRodModule> m_rod;//! Pointer to RodModule
    int m_ctrlID;                     //! ROD Slot
    std::string m_mode;                //! ROD Operating Mode
    unsigned int m_nMod;               //! Number of active modules
    std::array<unsigned int, 32> m_modGrp = {};         //! Module group
    std::array<bool, 32> m_modActive = {};              //! Active flag for a module
    std::array<bool, 32> m_modTrigger = {};             //! Trigger flag for a module
    std::array<int, 32> m_autoDisabledLinks = {};       //! Original status of autodisabled links
    std::array <int, 32> m_modPosition = {};             //! Position of a moduleId in the internal vectors
    bool m_scanActive;                 //! true if a scan is active
    uint32_t m_runNumber;                   //! Run Number taken from SetRunNumber method
    std::array<unsigned int, 8> m_fmtLinkMap = {};      //! Formatters link map
    std::array<std::array<int, 4>, 4> m_grId = {};                  //! PP0 group id

    IblRodScanStatus m_scanResult;

    bool m_preAmpOff;         //! Pre-amp status
    // for sending the commands to the PPC
    std::string m_ipAddress;
 
    // fei4 specific variables
    uint32_t m_disableFeMaskConfig; // rx of channels disabled in their FE config for configuration
    uint32_t m_disableFeMaskDacs; // rx of channels disabled in their FE config for readback
    uint32_t m_disableFeMaskRun; // rx of channels disabled in their FE config for running
    uint32_t m_runRxMask; // rx ch of FE to be used for the current data run
    uint32_t m_scanRxMask; // rx ch of FE to be used for the current scan
    bool fakeScan; // don't actually talk to the ppc or front end, just go through the motions (for debug)
    int capType; // store cap type of scan for calculating charge from vcal later
    
    // Store scanId, scanExecId and scanTypeId to allow it to be aborted.
    uint32_t m_scanId;
    uint16_t m_scanTypeId;
    uint16_t m_scanExecId;

    bool m_histosTimedOut;
    bool m_runOccHisto;

    PixModule::FEflavour m_flavour;
    DataTakingConfig m_dtConfig;					//general DataTakingConfig object to store and passa configurations

    private:
    using PixController::sendCommand;
    void readBackFEI4Latches(unsigned int moduleMask);
    //! Activate ECRReconfiguration
    virtual void ActivateECRReconfiguration(uint32_t moduleMask, bool activate, uint32_t params = 0);
    void setPrmpVbpStandby( );
    // readBack service records?
    bool m_serviceRecordRead;
    std::array<std::pair<int,int>, MAX_MODULES> m_moduleRx; // store the module Rxs by module position
    static bool m_errPublishedConn;
    static bool m_errPublishedResponding;
    static bool m_errPublishedChannels;

  };
}
#endif // _PIXLIB_PPCPPRODPIXCONTROLLER
