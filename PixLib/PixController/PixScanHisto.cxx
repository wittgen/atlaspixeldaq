////////////////////////////////////////////////////////////////////
// PixScan.cxx
/////////////////////////////////////////////////////////////////////
//
// 17/06/05  Version 1.0 (PM)
//

// Still missing:
//  setting up of the rod scan structure before sending reference
//  presets
//  histogram downloads
//  histogram staging

#include "iblModuleConfigStructures.h"
#include "PixModule/PixModule.h"
#include "PixMcc/PixMcc.h"
#include "PixFe/PixFe.h"
#include "PixController/PixScan.h"
#include "RootDb/RootDb.h"
#include "PixConnectivity/ModuleConnectivity.h"

using namespace PixLib;

PixDbInterface* PixScanHisto::m_myDb = 0;

PixScanHisto::PixScanHisto() {
  m_histoMode = false;
  m_histo = NULL;
  m_map = NULL;
  m_id = -1;
}

PixScanHisto::PixScanHisto(Histo &h) {
  m_histoMode = true;
  m_map = NULL;
  m_id = -1;  
  m_histo = new std::map<unsigned int, Histo*>;
  (*m_histo)[0] = &h;
}

PixScanHisto::PixScanHisto(unsigned int ih, Histo &h) {
  m_histoMode = true;
  m_map = NULL;
  m_id = -1;
  m_histo = new std::map<unsigned int, Histo*>;
  (*m_histo)[ih] = &h;
}

PixScanHisto::PixScanHisto(unsigned int ix, PixScanHisto &sh) {
  m_histoMode = false;
  m_histo = NULL;
  m_id = -1;
  m_map = new std::map<unsigned int,PixScanHisto*>;
  (*m_map)[ix] = &sh;
}

PixScanHisto::~PixScanHisto() {
  clear();
}

void PixScanHisto::add(unsigned int ix, PixScanHisto &sh) {
  if (m_histoMode && m_histo != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in map mode");
  } else {
    if (m_map == NULL) {
      m_map = new std::map<unsigned int, PixScanHisto*>;
      m_histoMode = false;
    }
    if (m_map->find(ix) != m_map->end()) {
      delete (*m_map)[ix];
    }
    (*m_map)[ix] = &sh;
  }
}

void PixScanHisto::addNullPointer(unsigned int ih, Histo *h){
  if (!m_histoMode && m_map != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in histo mode");
  } else {
    if (m_histo == NULL) {
      m_histo = new std::map<unsigned int, Histo*>;
      m_histoMode = true;
    }
    if (m_histo->find(ih) != m_histo->end()) {
      delete (*m_histo)[ih];
    }
    (*m_histo)[ih] = h;
  }
}

void PixScanHisto::add(unsigned int ih, Histo &h) {
  if (!m_histoMode && m_map != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in histo mode");
  } else {
    if (m_histo == NULL) {
      m_histo = new std::map<unsigned int, Histo*>;
      m_histoMode = true;
    }
    if (m_histo->find(ih) != m_histo->end()) {
      delete (*m_histo)[ih];
    }
    (*m_histo)[ih] = &h;
  }
}

void PixScanHisto::add(unsigned int ih, std::vector< Histo * >&h) {
  if (m_histoMode && m_histo != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in map mode");
  } else {
    if (m_map == NULL) {
      m_map = new std::map<unsigned int, PixScanHisto*>;
      m_histoMode = false;
    }
    PixScanHisto *sh = new PixScanHisto(*(h[0]));
    if (m_map->find(ih) != m_map->end()) {
      delete (*m_map)[ih];
    }
    (*m_map)[ih] = sh;
    for (unsigned int iih = 1; iih < h.size(); iih++) {
      sh->add(iih, *(h[iih]));
    }
  }
}

bool PixScanHisto::exists(unsigned int ix) {
  if (m_histoMode && m_histo != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in map mode");
  } else {
    if (m_map != NULL) {
      if (m_map->find(ix) != m_map->end()) return true;
    }
  }
  return false;
}

bool PixScanHisto::histoExists(unsigned int ih) {
  if (!m_histoMode && m_map != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in histo mode");
  } else {
    if (m_histo != NULL) {
      if (m_histo->find(ih) != m_histo->end()) return true;
    }
  }
  return false;
}

unsigned int PixScanHisto::size() {
  if (m_histoMode && m_histo != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in map mode");
  } else {
    if (m_map != NULL) {
      return m_map->size();
    }
  }
  return 0;
}

unsigned int PixScanHisto::histoSize() {
  if (!m_histoMode && m_map != NULL) {
    throw PixScanExc(PixControllerExc::ERROR, "Not in histo mode");
  } else {
    if (m_histo != NULL) {
      return m_histo->size();
    }
  }
  return 0;
}

std::vector<unsigned int> PixScanHisto::listIds() {
  std::vector<unsigned int>v;
  if (!m_histoMode) {
    std::map<unsigned int, PixScanHisto*>::iterator it;
    for (it=m_map->begin(); it!=m_map->end(); it++) {
      v.push_back(it->first);
    }
  }
  return v;
}

std::vector<unsigned int> PixScanHisto::listHistoIds() {
  std::vector<unsigned int>v;
  if (m_histoMode) {
    std::map<unsigned int, Histo*>::iterator it;
    for (it=m_histo->begin(); it!=m_histo->end(); it++) {
      v.push_back(it->first);
    }
  }
  return v;
}

bool PixScanHisto::histoMode() {
  return m_histoMode;
}

PixScanHisto& PixScanHisto::operator[](unsigned int ix) {
  static PixScanHisto tmp;
  if (m_histoMode) {
    return *this;
  } else {
    if (m_map != NULL) {
      if (m_map->find(ix) != m_map->end()) return *((*m_map)[ix]);
    } else {
        throw PixScanExc(PixControllerExc::ERROR, "PixScan.cxx: m_map == NULL!!");
    }
    throw PixScanExc(PixControllerExc::ERROR, "Index not found in map");
  }
  return tmp;
}

Histo& PixScanHisto::histo(unsigned int ih) {
  static Histo tmp;
  if (m_histoMode) {
    if (m_histo != NULL) {
      if (m_histo->find(ih) != m_histo->end()) {
        if ((*m_histo)[ih] != NULL) {
          return *((*m_histo)[ih]);
        } else if (m_myRecordDecName != ""){
          std::cout << "load the histograms from file" << std::endl;
          if (m_myDb == 0) {
            std::cout << "initializing the histogram Db server" << std::endl;
            string retval = m_myRecordDecName;
            std::size_t colonPos = retval.find(":");
            if (colonPos != std::string::npos) {
              retval.erase(colonPos, retval.size());
            }
            std::cout << "++ " << retval << " = " << m_myRecordDecName << std::endl;
            m_myDb = new PixLib::RootDb(retval, "READ");
          }
          DbRecord* dbr = m_myDb->DbFindRecordByName(m_myRecordDecName);
          if(dbr){
            std::cout << "histogram loaded" << std::endl;
            readHistoLevel(dbr,m_myLevel);
            return histo(ih);
          }
        } else {
          if (m_id >= 0) {
            // ???? Load histogram
          }
          throw PixScanExc(PixControllerExc::ERROR, "Histogram not available in the remote server");
        }
      }
    } else {
      throw PixScanExc(PixControllerExc::ERROR, "Histogram not present");
    }
  }
  throw PixScanExc(PixControllerExc::ERROR, "Not in histogram mode");
  return tmp;
}

Histo& PixScanHisto::histo() {
  return histo(0);
}

void PixScanHisto::clear() {
  if (m_id >= 0) {
    m_id = -1;
  }
  if (m_histoMode) {
    if (m_histo != NULL) {
      std::map<unsigned int, Histo*>::iterator it;
      for (it = m_histo->begin(); it != m_histo->end(); ++it) {
        delete (*it).second;
      }
      m_histo->clear();
      delete m_histo;
      m_histo = NULL;
    }
  } else {
    if (m_map != NULL) {
      std::map<unsigned int, PixScanHisto*>::iterator it;
      for (it = m_map->begin(); it != m_map->end(); ++it) {
        delete (*it).second;
      }
      m_map->clear();
      delete m_map;
      m_map = NULL;
    }
  }
}

int PixScanHisto::countHistos()  {
  if (m_map) {
    return countHistos(*m_map);
  }
  else {
    return 0;
  }
}

int PixScanHisto::countHistos(std::map<unsigned int, PixScanHisto*> hMap) {

  int nHistos=0;
  for(std::map<unsigned int, PixScanHisto*>::iterator it=hMap.begin(); it!=hMap.end(); it++)  {
    if( (*it).second->histoMode() )  {
      nHistos += (*it).second->histoSize();
    }
    else {
      if ((*it).second->m_map) {
        nHistos += countHistos( *((*it).second->m_map) );
      }
    }
  }
  return nHistos;
}


void PixScanHisto::clearLocal() {
  if (m_id >= 0) {
    if (m_histoMode) {
      if (m_histo != NULL) {
        std::map<unsigned int, Histo*>::iterator it;
        for (it = m_histo->begin(); it != m_histo->end(); ++it) {
          delete (*it).second;
          (*m_histo)[(*it).first] = NULL;
        }
      }
    } else {
      if (m_map != NULL) {
        std::map<unsigned int, PixScanHisto*>::iterator it;
        for (it = m_map->begin(); it != m_map->end(); ++it) {
          ((*it).second)->clearLocal();
        }
      }
    }
  }
}


void PixScanHisto::update() {
}

void PixScanHisto::write(DbRecord *dbr) {
  writeHistoLevel(dbr,"",0);
}

void PixScanHisto::read(DbRecord *dbr, bool loadHisto) {
  readHistoLevel(dbr,0, loadHisto);
}


void PixScanHisto::writeHistoLevel(DbRecord *dbr, std::string lName, int lvl) {
  if (histoMode()) { // if PixScanHisto contains Histos
    std::map<unsigned int, Histo*>::iterator ih;
    if (m_histo == NULL) {
      //std::cout << "WARNING [PixScanHist::writeHistoLevel] Missing Histo map at level " << lvl << " named = " << lName << std::endl;
      return;
    }
    for (ih=m_histo->begin(); ih != m_histo->end(); ++ih) {
      std::stringstream fieldname;
      fieldname << lName << ih->first;
      DbField* dbfield = dbr->getDb()->makeField(fieldname.str());
      dbr->getDb()->DbProcess(dbfield,PixDb::DBCOMMIT, *(ih->second));
      dbr->pushField(dbfield); // write into file
    }  
  } else { // else if PixScanHisto contains PixScanHistos
    char letter = 'A'+lvl;
    std::map<unsigned int, PixScanHisto*>::iterator il; 
    if (m_map == NULL) {
      //std::cout << "WARNING [PixScanHist::writeHistoLevel] Missing PixScanHisto map at level " << lvl << " named = " << lName << std::endl
      return;
    }
    for (il=m_map->begin(); il != m_map->end(); ++il) {
      std::stringstream fieldname;
      fieldname << lName << letter << il->first;
      DbRecord* nlr = dbr->addRecord("PixScanHisto", fieldname.str());    
      il->second->writeHistoLevel(nlr, fieldname.str()+"_", lvl+1);
      delete nlr;
    }
  }
}

void PixScanHisto::writeHistoServer(PixHistoServerInterface *hsi, std::string folder, int lvl) {
  std::cout <<"Begin of: PixScanHisto::writeHistoServer" << std::endl;

  if (histoMode()) { // if PixScanHisto contains Histos
    std::map<unsigned int, Histo*>::iterator ih;
    if (m_histo == NULL) {
      std::cout << "WARNING [PixScanHisto::writeHistoServer] Missing Histo map at level " << lvl <<std::endl;
      return;
    }
    for (ih=m_histo->begin(); ih != m_histo->end(); ++ih) {
      std::cout << "I have a map of Histos. I pass the folder name: " << folder <<std::endl;

      std::stringstream final;
      final << folder << (*ih).first << ":";
      //std::string n = final.str();
      try {
        hsi->sendHisto(final.str(), *((*ih).second));
      } catch (...) {
        std::cout << "ERROR!!! OH!!!!  PixScanHisto::writeHistoServer" << std::endl;
      }
    }  
  } else { // else if PixScanHisto contains PixScanHistos
    char letter = 'A'+lvl;
    std::map<unsigned int, PixScanHisto*>::iterator il; 
    if (m_map == NULL) {
      std::cout << "WARNING [PixScanHist::writeHistoServer] Missing PixScanHisto map at level " << lvl << std::endl;
      return;
    }
    for (il=m_map->begin(); il != m_map->end(); ++il) {
      std::stringstream finalName;
      finalName << folder << letter << (*il).first << "/"; 
      std::cout << "I have a map of PixScanHistos. I pass the folder name: " << finalName.str() <<std::endl; 

      (*il).second->writeHistoServer(hsi, finalName.str(), lvl+1);
    }
  }

}

// *******************
// New methods to save the histograms during the scans
// To solve dsp memory problem 
void PixScanHisto::writeSingleHisto(PixHistoServerInterface *hsi, std::string histoName, int idx2, int idx1, int idx0, int ih)
{
    if (histoMode()) { // if PixScanHisto contains Histos
        if(idx0 != -1){
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] idx0 is not -1, but this PixScanHisto has no more levels." <<std::endl;
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] will ignore idx0, idx1 and idx2." <<std::endl;
        }
        if (m_histo == NULL) {
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Missing Histo map" <<std::endl;
            return;
        }
        if(m_histo->find(ih) == m_histo->end()){
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Missing histo on index " << ih <<std::endl;
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Won't delete anything" <<std::endl;
            return;
        }else{ 
            try {
                hsi->sendHisto(histoName, *((*m_histo)[ih]));
            } catch (...) {
                std::cout << "ERROR!!! OH!!!!  PixScanHisto::writeSingleHisto" << std::endl;
            }
        }
    } else { // else if PixScanHisto contains PixScanHistos
        if (m_map == NULL) {
            std::cout << "WARNING [PixScanHist::writeSingleHisto] Missing PixScanHisto map "  << std::endl;
            return;
        }
        if(idx2 != -1 && m_map->find(idx2) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Missing histo on index idx2 " << idx2 <<std::endl;
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx2 != -1){ 
            (*m_map)[idx2]->writeSingleHisto(hsi,histoName,-1, idx1, idx0, ih);
        }else if(idx1 != -1 && m_map->find(idx1) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Missing histo on index idx1 " << idx1 <<std::endl;
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx1 != -1){
            (*m_map)[idx1]->writeSingleHisto(hsi, histoName,-1, -1, idx0, ih);      
        }else if(idx0 != -1 && m_map->find(idx0) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] Missing histo on index idx0 " << idx0 <<std::endl;
            std::cout << "WARNING [PixScanHisto::writeSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx0 != -1){
            (*m_map)[idx0]->writeSingleHisto(hsi, histoName,-1, -1, -1, ih);      
        }
    }
}

void PixScanHisto::deleteSingleHisto(int idx2, int idx1, int idx0, int ih)
{
    if (histoMode()) { // if PixScanHisto contains Histos
        if(idx0 != -1){
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] idx0 is not -1, but this PixScanHisto has no more levels." <<std::endl;
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] will ignore idx0, idx1 and idx2." <<std::endl;
        }
        if (m_histo == NULL) {
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Missing Histo map" <<std::endl;
            return;
        }
        if(m_histo->find(ih) == m_histo->end()){
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Missing histo on index " << ih <<std::endl;
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Won't delete anything" <<std::endl;
            return;
        }else{ 
            Histo* toDelete = (*m_histo)[ih];
            m_histo->erase(m_histo->find(ih));
            delete toDelete;
        }
    } else { // else if PixScanHisto contains PixScanHistos
        if (m_map == NULL) {
            std::cout << "WARNING [PixScanHist::deleteSingleHisto] Missing PixScanHisto map " << std::endl;
            return;
        }
        if(idx2 != -1 && m_map->find(idx2) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Missing histo on index idx2 " << idx2 <<std::endl;
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx2 != -1){ 
            (*m_map)[idx2]->deleteSingleHisto(-1, idx1, idx0, ih);
        }else if(idx1 != -1 && m_map->find(idx1) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Missing histo on index idx1 " << idx1 <<std::endl;
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx1 != -1){
            (*m_map)[idx1]->deleteSingleHisto(-1, -1, idx0, ih);      
        }else if(idx0 != -1 && m_map->find(idx0) == m_map->end()){
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] Missing histo on index idx0 " << idx0 <<std::endl;
            std::cout << "WARNING [PixScanHisto::deleteSingleHisto] No histogram will be deleted" <<std::endl;
            return;
        }else if(idx0 != -1){
            (*m_map)[idx0]->deleteSingleHisto(-1, -1, -1, ih);      
        }
    }
}





// *******************




void PixScanHisto::readHistoLevel(DbRecord* dbr, int lvl, bool loadHisto) {
  clear();
  std::string parentName; 
  char letter = 'A'+lvl;
  bool recFound = false;

  if (lvl!=0) {
    parentName = dbr->getName()+"_"+letter;
  } else {
    parentName = letter;
  }
  // Loop on record
  for (dbRecordIterator it=dbr->recordBegin(); it!=dbr->recordEnd(); it++) {
    std::string name = (*it)->getName();
    if(name.find(parentName) == 0) {
      recFound = true;
      std::string sub = name.substr(parentName.size());
      unsigned int n;
      std::istringstream is(sub);
      is >> n;
      add(n,*(new PixScanHisto()));
      dbRecordIterator r = dbr->getDb()->DbProcess(it, PixDb::DBREAD);
      (*m_map)[n]->readHistoLevel(*r, lvl+1, loadHisto);
    }
  }
  if(!recFound) {
    if (lvl!=0) {
      parentName = dbr->getName()+"_";
    } else {
      parentName = "";
    }
    for(dbFieldIterator histoIt=dbr->fieldBegin(); histoIt!=dbr->fieldEnd(); histoIt++) {
      std::cout << "PixScanHisto::readHistoLevel. This PixScanHisto contains just Histo"<<std::endl;
      Histo *h = new Histo();
      histoIt = dbr->getDb()->DbProcess(histoIt, PixDb::DBREAD, *h);
      std::string name = (*histoIt)->getName();
      histoIt.releaseMem();
      std::cout << name <<std::endl;
      if(name.find(parentName) == 0) {
        std::string sub = name.substr(parentName.size());
        unsigned int n;
        std::istringstream is(sub);
        is >> n;
        std::cout << n << std::endl;
        if (loadHisto) {
          add(n,*h);
        } else {
          m_myRecordDecName = dbr->getDecName();
          m_myLevel = lvl;
          addNullPointer(n,0);
        }
      }
      if(!loadHisto) delete h;      
    }
  }
}

