/////////////////////////////////////////////////////////////////////
// PixScan.h 
/////////////////////////////////////////////////////////////////////
//
// 17/06/05  Version 1.2 (PM)
//           Explicit separation of scan loop start and
//           scan step preparation
//           Some modification to the proposal to allow
//           - execution of mask staging at the host level
//           - execution of loops with values dynamically
//             choosen by PixModuleGroup guring the scan
//

/*

PixScan is a structure describing a scan and containing 
scan results. It's not responsible for the execution of 
the scan; this task is performed by PixModuleGroup, which 
in turns uses PixController methods to activate the DSPs. 
PixScan however can contain helper funcions used during the 
execution of a scan (e.g. histogram manipulation and fit). 
PixScan can be used as a carrier to move scan results from a
processor to another, having the ability to save and retreive 
data from a permanent storage; it can also implement intelligent 
caching mechanism to allow a processor to access a large 
set of histograms minimizing memory occupation.

The structure is designed for a three-loop scan. The innermost 
loop (which implicitly includes the mask stagin as an extra 
loop) is usually performed by the ROD DSPs. The second loop can be 
performed by the ROD or by the host while the third loop is 
usually performed by the host.

PixScan defines four groups of modules. The histograms
of every group are collected and processed by the corresponding 
SDSP. Modules not assigned to any group are ignored during 
the scan. The activity of the modules of a group during a scan 
is controlled by four parameters:

configure : if true the modules are configured at the beginning of the scan
trigger   : if true the modules receive LVL1 commands
strobe    : if true the modules receive STROBE commands
readout   : if true the modules are read-out

It's possible to specify a fixed delay between strobe/trigger 
sequences sent to groups 1,2 and 3,4. Finally, it's possible 
to split a single event (which is usually made of 16 consecutive
LVL1) in two parts of N amd M (N+M<=16) consecutive LVL1 
separated by a fixed time gap. Two different settings are possible, 
one for groups 1,2 and the other for groups 3,4.

There is a set of scan attributes and a set of loop attributes. 
Scan attributes are parameters which are set at the beginning of 
a scan and remains constant during the entire scan. 
Loop attributes indicate specific properties of every loop,
like the variable being scanned during the loop or the operation 
to be performed at the end of the loop. PixScan includes a set of
hidden attributes; these are not intended to be modified by the 
user, so no interface is provided; hidden attributes are computed 
automatically when needed, based on scan and loop attributes, 
and passed to the PixController.
Scan attributes
---------------
- Configuration set to use
- Concurrent scan or FE by FE
- Mask stage mode
- Mask stage # of steps
- Mask stage # of steps to execute
- Inversion between mask stage and innermost loop
- Number of events in the innermost loop
- Enable self trigger
- Trigger/Strobe delay
- LVL1 latency
- Strobe duration
- Module mask (gr 1-4)
- Configuration enabled (gr 1-4)
- Trigger enabled (gr 1-4)
- Strobe enabled (gr 1-4)
- Readout enabled (gr 1-4)
- Trigger delay gr 1,2-3,4
- Number of consecutive LVL1 in Trigger group A (gr 1,2 3,4) 
- Number of consecutive LVL1 in Trigger group B (gr 1,2 3,4)
- Delay between trigger group 1 and 2 (gr 1,2 3,4)
- Type of histogramming routine to be used in the DSPs
- Histograms to be filled during the scan
- TOT histogams min, max and # of bins
- Raw histograms to be kept till the end of the scan
- Analog or digital injection
- Charge injection capacitor
- Column readout frequency
- TOT threshold mode
- TOT minimum
- TOT double hit threshold
- TOT or leading edge time stamp
- Bias voltage mode
- Bias voltage value
- Analog voltage mode
- Analog voltage value
- Digital voltage mode
- Digital voltage value

Loop attributes
---------------
- Parameter being scanned during the loop
- Loop execution in the DSP or in the Host
- Array of values for the scan parameter or
  Min, Max and number of steps or sequence not predefined
- Operation to be performed at the end of the loop (e.g. 
  s-curve fit or tdac adjust).
- Use of DSP for the end loop action

This examples illustrates how PixScan used to perform a
scan:

PixScan *scn;
PixModuleGroup *grp;

scn->resetScan();
grp->initScan(scn);

grp->scanLoopStart(2, scn);
while (scn->loop(2)) {
  grp->prepareStep(2,scn);
  grp->scanLoopStart(1, scn);
  while (scn->loop(1)) {
    grp->prepareStep(1,scn);
    grp->scanLoopStart(0, scn);
    while (scn->loop(0)) {
      grp->prepareStep(0,scn);
      grp->scanExecute(scn);
      std::cout << "Completed loop: " << scn->scanIndex(2) << "/";
      std::cout << scn->scanIndex(1) << "/" << scn->scanIndex(0) << std::endl;
      grp->scanTerminate(scn);
      scn->next(0);
    }
    grp->scanLoopEnd(0, scn);
    scn->next(1);
  }
  grp->scanLoopEnd(1, scn);
  scn->next(2);
}
grp->scanLoopEnd(2, scn);

grp->terminateScan(scn);

grp->initScan(scn) has to prepare the module group for the scn.
This includes, for egxample, the regulation of the voltages via DCS

grp->scanLoopStart(index, scn) prepares the module group for the 
beginning of a particular loop. This includes any hardware or
software setting to be performed once at the beginning of the loop,  

grp->prepareStep(index, scn) the typical operation to perform 
in this phase is the setting of the loop scan variable to the 
appropriate value. For the innermost loop (index = 0) this
operation has to be performed only if scn->newScanStep() returns
TRUE; in case the mask staging is under the responsability
of the host, this method will also setup the appropriate mask 
when scn->newMaskStep() returns TRUE. 

grp->scanExecute(scn) executes a step of the scan. If the loop is
executed in the DSP this method will setup the ROD and complete an 
entire loop.

grp->scanLoopEnd(index, scn) will perform the end-of-loop actions.
Typically this method will upload histograms or fit results from 
the ROD and store them in the appropriate PixScan structures; it
will also compute the results needed for the execution of the 
subsequent step.

grp->terminateScan(scn) executes end of scan actions, like resotring
the initial module configuration if needed.

*/

#ifndef _PIXLIB_PIXSCAN
#define _PIXLIB_PIXSCAN

#ifdef WIN32
#pragma warning(disable: 4786)
#pragma warning(disable: 4800)
#endif

#include <vector>
#include <list>
#include <map>
#include <string>

#include "PixController/PixController.h"
#include "PixController/PixScanHisto.h"
//#include "scanOptions_2.h"
#include "PixHistoServer/PixHistoServerInterface.h"
#include "PixScanBase.h"

class IPCPartition;
class ISInfoDictionary;

namespace PixLib {
  
  class Config;
  class Histo;
  class PixControllerExc;
  class PixDbInterface;
  
  //! Pix Controller Exception class; an object of this type is thrown in case of a controller error 
  class PixScanExc : public PixControllerExc {
  public:
    //! Constructor
    PixScanExc(ErrorLevel el, std::string name) : PixControllerExc(el, name) {}; 
    //! Destructor
    virtual ~PixScanExc() {};
  private:
  };
  

  

  class PixScan : public PixScanBase {
    
    friend class PixController;
    friend class RodPixController;
    friend class PixConnectivity;
    friend class PixScanHisto;

  public:


    
    
  private:
    // Global scan config
 
  
    // Loop configuration
  
    // Scan logs 
    std::string m_timeScanStart;
    std::string m_timeScanEnd;
    std::string m_timeHwriteStart;
    std::string m_timeHwriteEnd;
    std::string m_timeInitEnd;
    int m_timeHdownload;
    timeval m_timeLoop0Start;
    timeval m_timeLoop0End;
    int m_timeLoop0Total;
    std::string m_logsComment;
    std::string m_logsScanType;
    int m_logsScanId;
    std::string m_logsRodInfo;
    std::string m_tagsObjID;
    std::string m_tagsConn;
    std::string m_tagsData;
    std::string m_tagsAlias;
    std::string m_tagsCfg;
    std::string m_tagsEna;
    unsigned int m_tagsCfgRev;
    std::string m_rodName;
    PixConnectivity *m_tmpConn;
    PixModuleGroup *m_tmpGrp;
    std::string m_modNames[32];

    // DB related
    DbRecord* m_dbRecord;
    PixDbInterface *m_dbn;
    Config *m_conf;
    Config *m_info;

    // Histograms
    int m_nHisto;
    int m_nHistoSize;
    std::map<HistogramType, PixScanHisto> m_histo;
    std::map<std::string, int> m_histogramTypes;
    std::map<std::string, int> m_dspHistogramTypes;
    std::map<std::string, int> m_scanTypes;
    bool m_ownTmpConn;
    bool m_fastThrUsePseudoPix;
    bool m_useGrpThrRange;

    // Private methods
    void prepareIndexes(HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);
    std::string getTime();
    void readHisto(DbRecord* dbr, bool loadHisto = true);
    
  public:
    //! Constructors
    PixScan();
    PixScan(ScanType presetName, FEflavour feflav);
    PixScan(DbRecord *dbr);
    PixScan(const PixScan &scn);
    PixScan(PixScanBase& base):PixScanBase(base){ 
      initConfig();
      resetScan();
      m_actualFE = -1;
      m_tmpGrp = NULL;
      m_tmpConn = NULL;
    };

    //! Destructor
    ~PixScan();
    
    //! Init configuration
    void initConfig();
    void resetScan(); 
 
    //! Scan control
    bool loop(int index);
    int scanIndex(int index);
    void next(int index);
    void terminate(int index);
 
    void scanTimeStart();
    void scanTimeEnd();
    void initHistoDownloadTime();
    void initLoop0Time();
    void initTimeEnd();
    void loop0Start();
    void loop0End();
    
    string getTimeScanStart()        { return m_timeScanStart;   }
    string getTimeScanEnd()          { return m_timeScanEnd;     }
    string getTimeScanHwriteStart()  { return m_timeHwriteStart; }
    string getTimeScanHwriteEnd()    { return m_timeHwriteEnd;   }
    string getTimeScanInitEnd()      { return m_timeInitEnd;     }
    int getTimeHistoDownload()       { return m_timeHdownload;   }
    int getTimeLoop()                { return m_timeLoop0Total;  }


    PixConnectivity *getTmpConn() {
      return m_tmpConn;
    }
    PixModuleGroup *getTmpGrp();

    std::string getModNames(int im) {
      if (im>=0 && im<32) {
        return m_modNames[im];
      } else {
	return "==";
      }
    }
    bool getFastThrUsePseudoPix(){
      return m_fastThrUsePseudoPix;
    }
    bool getUseGrpThrRange(){
      return m_useGrpThrRange;
    }

    //! DataBase interaction
    void writeConfig(DbRecord *dbr);
    void writeHisto(DbRecord *dbr, PixModuleGroup *grp);
    void write(DbRecord* dbr, PixModuleGroup *grp);
    void readConfig(DbRecord *dbr);
    void readHisto(DbRecord* dbr, PixConnectivity *conn, bool loadHisto = true);
    void readHisto(DbRecord *dbr, PixModuleGroup *grp, bool loadHisto = true);
    void readInfo(DbRecord *dbr);
    void read(DbRecord *dbr, PixModuleGroup *grp, bool loadHisto = true);

    void writeHisto(PixHistoServerInterface *hsi, PixModuleGroup *grp, std::string folder);
    
    Config &config() { return *m_conf; };
    Config &info() { return *m_info; };
    std::string getLogsComment() {
      return m_logsComment;
    }
    void setLogsComment(std::string comm) {
      m_logsComment = comm;
    }
    std::string getLogsScanType() {
      return m_logsScanType;
    }
    void setLogsScanType(std::string type) {
      m_logsScanType = type;
    }
    int getLogsScanId() {
      return m_logsScanId;
    }
    void setLogsScanId(int id) {
      m_logsScanId = id;
    }
    //! Histogram handling
    void addHisto(PixScanHisto &psh, HistogramType type, unsigned int mod);
    void addHisto(Histo &his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);
    void addHisto(Histo &his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0, int ih);
    void addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0, int ih);
    void addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);
    void addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1);
    void addHisto(std::vector< Histo * >&his, unsigned int nh, HistogramType type, unsigned int mod, int ix2, int ix1);
    void addHisto(std::vector< Histo * >&his, unsigned int d2, unsigned int d1, HistogramType type, unsigned int mod, int ix2);
    void addHisto(std::vector< Histo * >&his, unsigned int d3, unsigned int d2, unsigned int d1, HistogramType type, unsigned int mod);
    void mergeFEHistos(Histo& histo, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);
    void mergeFEHistos(std::vector<Histo*> hVect, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0);
    void mergeFEHistos(std::vector<Histo*> hVect, int consecLvl1HistoA, HistogramType type, unsigned int mod, int ix2, int ix1);
    void mergeFEHistos(std::vector<Histo*> hVect, HistogramType type, unsigned int mod, int ix2, int ix1);
    void downloadHisto(PixController *ctrl, unsigned int mod, HistogramType type);
    void clearHisto(unsigned int mod, HistogramType type);
    Histo& getHisto(HistogramType type, PixModuleGroup *grp, int module, int idx2, int idx1, int idx0);
    Histo& getHisto(HistogramType type, PixModuleGroup *grp, int module, int idx2, int idx1, int idx0, int ih);

    // New methods for memory problem
    void writeHisto(PixHistoServerInterface *hsi, PixModuleGroup *grp, std::string folder, HistogramType type, int module, int idx2, int idx1, int idx0, int ih = 0);
    void deleteHisto(HistogramType type, int module, int idx2, int idx1, int idx0, int ih = 0);

    PixScanHisto& getHisto(HistogramType type);
    std::map<std::string, int> &getHistoTypes() {
      return m_histogramTypes;
    }
    int getNHisto(); // Returns total number of Histos contained in the PixScan  
    std::map<std::string, int> &getDspHistogramTypes() {
      return m_dspHistogramTypes;
    };
    std::map<std::string, int> &getScanTypes() {
      return m_scanTypes;
    };
    void dumpFmtLinkscanRes(int i2, int i1, int i0, std::ostream &out=std::cout);
    void dumpFmtLinkscanRes(int i2, int i1, int i0, int ih, std::ostream &out=std::cout);
    
    //! Helper functions
    void calcThr(PixController *ctrl, unsigned int mod, int ix2, int ix1);
    void fitSCurve(PixScanHisto &sc, Histo &thr, Histo &noise, Histo& chi2, int ih, int rep);
   
    //TB: New stuff coming from PixModuleGroup
    void initScan(PixModuleGroup *grp);
    void scanLoopStart(int nloop, PixModuleGroup *grp);
    void prepareStep(int nloop, PixModuleGroup *grp, PixHistoServerInterface *hInt, std::string folder);
    void scanTerminate(PixModuleGroup *grp);
    void scanLoopEnd(int nloop, PixModuleGroup *grp, PixHistoServerInterface *hInt, std::string folder);
    void terminateScan(PixModuleGroup *grp);
    void writeAndDeleteHisto(PixModuleGroup *grp, PixHistoServerInterface* hInt, std::string folder, 
			     unsigned int mod, int type, int idx2, int idx1, int idx0);
 
  };
  
}

#endif

 

