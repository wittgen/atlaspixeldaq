/////////////////////////////////////////////////////////////////////
// PixScan.h 
/////////////////////////////////////////////////////////////////////
//
// 17/06/05  Version 1.2 (PM)
//           Explicit separation of scan loop start and
//           scan step preparation
//           Some modification to the proposal to allow
//           - execution of mask staging a tthe host level
//           - execution of loops with values dynamically
//             choosen by PixModuleGroup guring the scan
//

/*

PixScan is a structure describing a scan and containing 
scan results. It's not responsible for the execution of 
the scan; this task is performed by PixModuleGroup, which 
in turns uses PixController methods to activate the DSPs. 
PixScan however can contain helper funcions used during the 
execution of a scan (e.g. histogram manipulation and fit). 
PixScan can be used as a carrier to move scan reults from a 
processor to another, having the ability to save and retreive 
data from a permanent storage; it can also implement intelligent 
caching mechanism to allow a processor to access a large 
set of histograms minimizing memory occupation.

The structure is designed for a three-loop scan. The innermost 
loop (which implicitly includes the mask stagin as an extra 
loop) is usually performed by the ROD DSPs. The second loop can be 
performed by the ROD or by the host while the third loop is 
usually performed by the host.

PixScan defines four groups of modules. The histograms
of every group are collected and processed by the corresponding 
SDSP. Modules not assigned to any group are ignored during 
the scan. The activity of the modules of a group during a scan 
is controlled by four parameters:

configure : if true the modules are configured at the beginning of the scan
trigger   : if true the modules receive LVL1 commands
strobe    : if true the modules receive STROBE commands
readout   : if true the modules are read-out

It's possible to specify a fixed delay between strobe/trigger 
sequences sent to groups 1,2 and 3,4. Finally, it's possible 
to split a single event (which is usually made of 16 consecutive
LVL1) in two parts of N amd M (N+M<=16) consecutive LVL1 
separated by a fixed time gap. Two different settings are possible, 
one for groups 1,2 and the other for groups 3,4.

There is a set of scan attributes and a set of loop attributes. 
Scan attributes are parameters which are set at the beginning of 
a scan and remains constant during the entire scan. 
Loop attributes indicates specific properties of every loop, 
like the variable being scanned during the loop or the operation 
to e performed at the end of the loop. PixScan includes a set of 
hidden attributes; these are not intended to be modified by the 
user, so no interface is provided; hidden attributes are computed 
automatically when needed, based on scan and loop attributes, 
and passed to the PixController.
Scan attributes
---------------
- Configuration set to use
- Concurrent scan or FE by FE
- Mask stage mode
- Mask stage # of steps
- Mask stage # of steps to execute
- Inversion between mask stage and innermost loop
- Number of events in the innermost loop
- Enable self trigger
- Trigger/Strobe delay
- LVL1 latency
- Strobe duration
- Module mask (gr 1-4)
- Configuration enabled (gr 1-4)
- Trigger enabled (gr 1-4)
- Strobe enabled (gr 1-4)
- Readout enabled (gr 1-4)
- Trigger delay gr 1,2-3,4
- Number of consecutive LVL1 in Trigger group A (gr 1,2 3,4) 
- Number of consecutive LVL1 in Trigger group B (gr 1,2 3,4)
- Delay between trigger group 1 and 2 (gr 1,2 3,4)
- Type of histogramming routine to be used in the DSPs
- Histograms to be filled during the scan
- TOT histogams min, max and # of bins
- Raw histograms to be kept till the end of the scan
- Analog or digital injection
- Charge injection capacitor
- Column readout frequency
- TOT threshold mode
- TOT minimum
- TOT double hit threshold
- TOT or leading edge time stamp
- Bias voltage mode
- Bias voltage value
- Analog voltage mode
- Analog voltage value
- Digital voltage mode
- Digital voltage value

Loop attributes
---------------
- Parameter being scanned during the loop
- Loop execution in the DSP or in the Host
- Array of values for the scan parameter or
  Min, Max and number of steps or sequence not predefined
- Operation to be performed at the end of the loop (e.g. 
  s-curve fit or tdac adjust).
- Use of DSP for the end loop action

This examples illustrates how PixScan used to perform a
scan:

PixScan *scn;
PixModuleGroup *grp;

scn->resetScan();
grp->initScan(scn);

grp->scanLoopStart(2, scn);
while (scn->loop(2)) {
  grp->prepareStep(2,scn);
  grp->scanLoopStart(1, scn);
  while (scn->loop(1)) {
    grp->prepareStep(1,scn);
    grp->scanLoopStart(0, scn);
    while (scn->loop(0)) {
      grp->prepareStep(0,scn);
      grp->scanExecute(scn);
      std::cout << "Completed loop: " << scn->scanIndex(2) << "/";
      std::cout << scn->scanIndex(1) << "/" << scn->scanIndex(0) << std::endl;
      grp->scanTerminate(scn);
      scn->next(0);
    }
    grp->scanLoopEnd(0, scn);
    scn->next(1);
  }
  grp->scanLoopEnd(1, scn);
  scn->next(2);
}
grp->scanLoopEnd(2, scn);

grp->terminateScan(scn);

grp->initScan(scn) has to prepare the module group for the scn.
This includes, for egxample, the regulation of the voltages via DCS

grp->scanLoopStart(index, scn) prepares the module group for the 
beginning of a particular loop. This includes any hardware or
software setting to be performed once at the beginning of the loop,  

grp->prepareStep(index, scn) the typical operation to perform 
in this phase is the setting of the loop scan variable to the 
appropriate value. For the innermost loop (index = 0) this
operation has to be performed only if scn->newScanStep() returns
TRUE; in case the mask staging is under the responsability
of the host, this method will also setup the appropriate mask 
when scn->newMaskStep() returns TRUE. 

grp->scanExecute(scn) executes a step of the scan. If the loop is
executed in the DSP this method will setup the ROD and complete an 
entire loop.

grp->scanLoopEnd(index, scn) will perform the end-of-loop actions.
Typically this method will upload histograms or fit results from 
the ROD and store them in the appropriate PixScan structures; it
will also compute the results needed for the execution of the 
subsequent step.

grp->terminateScan(scn) executes end od scan actions, like resotring
the initial module configuration if needed.

*/

#ifndef _PIXLIB_PIXSCANHISTO
#define _PIXLIB_PIXSCANHISTO

#include <vector>
#include <list>
#include <map>
#include <string>

#include "PixController/PixController.h"
#include "PixDbInterface/PixDbInterface.h"
#include "Histo/Histo.h"
#include "Config/Config.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixHistoServer/PixHistoServerInterface.h"

#define MAX_GROUPS 4
#define MAX_SGROUPS 2
#define MAX_LOOPS 3

namespace PixLib {
  class PixScanHisto {

  public:
    PixScanHisto();
    PixScanHisto(Histo &h);
    PixScanHisto(unsigned int ih, Histo &h);
    PixScanHisto(unsigned int ix, PixScanHisto &sh);
    virtual ~PixScanHisto();
    
    virtual void add(unsigned int ix, PixScanHisto &sh);
    virtual void add(unsigned int ih, Histo &h);
    virtual void add(unsigned int ih, std::vector< Histo * >&h);
    virtual bool exists(unsigned int ix);
    virtual bool histoExists(unsigned int ih);
    virtual unsigned int size();
    virtual std::vector<unsigned int> listIds();
    virtual std::vector<unsigned int> listHistoIds();
    virtual unsigned int histoSize();
    virtual bool histoMode(); 
    virtual PixScanHisto &operator[](unsigned int ix);
    virtual Histo &histo();
    virtual Histo &histo(unsigned int ih);
    virtual void clear();
    virtual int countHistos(); // helper: calls countHistos(m_map)
    virtual void clearLocal();
    virtual void update();
    virtual void write(DbRecord *dbr);
    virtual void read(DbRecord *dbr, bool loadHisto = true);
    virtual void writeHistoLevel(DbRecord *dbr, std::string lName, int lvl); 
    virtual void readHistoLevel(DbRecord *dbr, int lvl, bool loadHisto = true);
    virtual void writeHistoServer(PixHistoServerInterface *hsi, std::string folder, int lvl); 
    // mem problem
    virtual void writeSingleHisto(PixHistoServerInterface *hsi, std::string histoName, int idx2, int idx1, int idx0, int ih = 0);
    virtual void deleteSingleHisto(int idx2, int idx1, int idx0, int ih = 0);
    
  protected:
    std::map<unsigned int, Histo*> *m_histo;
    std::map<unsigned int, PixScanHisto*> *m_map;
    virtual void addNullPointer(unsigned int ih, Histo *h);    
    virtual int countHistos(std::map<unsigned int, PixScanHisto*> hMap); // Recursively loops into PixScanHisto maps and returns total number of Histo 
    bool m_histoMode;
    int m_id;

    static PixDbInterface* m_myDb;
    std::string m_myRecordDecName;    
    int m_myLevel;

  };
  
}

#endif

 

