/////////////////////////////////////////////////////////////////////
// PixRunConfig.h
/////////////////////////////////////////////////////////////////////
//
// 07/07/11  Version 1.0 (MW)
//           removed PixScanConfig.h moved class def for PixRunConfig 
//           to this file

#ifndef _PIXLIB_PIXRUNCONFIG
#define _PIXLIB_PIXRUNCONFIG

#define MAX_GROUPS 4
// TODO: MAX_GROUPS defined all over the place - move to one header file
#ifdef WIN32
#pragma warning(disable: 4786)
#pragma warning(disable: 4800)
#endif

#include <vector>
#include "Config/Config.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"

namespace PixLib {

class RunConfig : public Config {
  public:
  RunConfig(std::string name, PixRunConfig &runc) : Config(name), m_runConfig(runc) {};                          // Constructor
  RunConfig(std::string name, std::string type, PixRunConfig &runc) : Config(name,type), m_runConfig(runc) {};   // Constructor
  RunConfig(const RunConfig &cfg) : Config(cfg), m_runConfig(cfg.m_runConfig) {};                                // Copy constructor
  virtual ~RunConfig() {};                                                                                       // Destructor

  virtual void subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf); 

  private:
  PixRunConfig &m_runConfig;
};

class PixRunConfig  {

  friend class PixConnectivity;
 public:
  PixRunConfig(std::string name);
  ~PixRunConfig();

  // DB interactions
  void writeConfig(DbRecord* r);
  void writeConfig(PixConnectivity* conn, unsigned int rev = 0xffffffff);
  bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="RunConfig_Tmp", unsigned int revision=0xffffffff);
  void readConfig(DbRecord* rec);
  void readConfig(PixConnectivity* conn, unsigned int rev = 0xffffffff);
  bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);

  // Sub-config handling
  std::vector<std::string> listSubConf();
  void addSubConf(std::string name);
  void addSubConf(PixRunConfig *runc);
  void removeSubConf();
  void removeSubConf(std::string name);
  void selectSubConf(std::string name);
  bool subConfExists(std::string name);
  void selectSubConf(std::string partition, std::string crate, std::string rod);
  
  void defineGroup(int group, unsigned int mask);
  unsigned int getGroupMask(int group);
  void enableReadout(int group);
  void disableReadout(int group);
  bool readoutEnabled(int group);

  Config &config() { return *(m_cur->m_conf); };

  int  internalGen() const { return m_cur->m_internalGen; };
  int  consecutiveLVL1() const { return  m_cur->m_consecutiveLVL1; };
  int  consecutiveLVL1_IBL() const { return  m_cur->m_consecutiveLVL1_IBL; }
  int  latency() const { return m_cur->m_latency; };
  int  nPixInject() const { return m_cur->m_nPixInject; };
  bool digitalInject() const { return m_cur->m_digitalInject; };
  bool cInjHi() const { return m_cur->m_cInjHi; };
  int  vcal() const { return m_cur->m_vcal; };
  // 14 Aug the following 2 parameters added to strobe injections during datataking (noise tests) - GAO
  bool injectMode()  const { return m_cur->m_injectMode; };
  int mccCalDelay() const { return m_cur->m_mccCalDelay; };
  int rodCalDelay() const { return m_cur->m_rodCalDelay; };
  int mccCalWidth() const { return m_cur->m_mccCalWidth; };
  int rodBcidOffset() const { return m_cur->m_rodBcidOffset; };
  
  //26 Aug injection parameters
  enum InjectionType: int {NOINJ, RANDOM, ROW, COLUMN};
  InjectionType &injType() { return m_cur->m_injType; };
  int &injStep() { return m_cur->m_injStep; };
  int &injStartPoint() { return m_cur->m_injStartPoint; };
  // Accessors to ROD simulation params
  bool simActive() const { return m_cur->m_simActive; };
  bool simMode() const { return m_cur->m_simMode; };
  int simLVL1Accept() const { return m_cur->m_lVL1Accept; };
  int simSkippedLVL1() const { return m_cur->m_skippedLVL1; };
  int simBitFlip() const { return m_cur->m_bitFlipMask; };
  int simMCCFlags() const { return m_cur->m_mccFlags; };
  int simFEFlags() const { return m_cur->m_feFlags; };
  int simFormattersMask() const { return m_cur->m_formattersEnaMask; };
  int simModOccupancy() const  { return  m_cur->m_moduleOccupancy; };
  const std::string getName() const {return m_cur->m_conf->name();};
  unsigned int rodMonPrescale() const { return m_cur->m_rodMonPrescale; };
  unsigned int occupRate() const { return m_cur->m_occupRate; };
  unsigned int busyRate() const { return m_cur->m_busyRate; };
  unsigned int statusRate() const { return m_cur->m_statusRate; };
  unsigned int bufRate() const { return m_cur->m_buffRate; };
  unsigned int quickStatusMode() const { return m_cur->m_quickStatusMode; };
  unsigned int quickStatusConfig() const { return m_cur->m_quickStatusConfig; };
  unsigned int syncConfig() const { return m_cur->m_syncConfig; };
  unsigned int syncConfigROD() const { return m_cur->m_syncConfigROD; };
  bool rawDataMode() const { return m_cur->m_rawDataMode; };
  bool autoDisable() const { return m_cur->m_autoDisable; };
  bool autoRecover() const { return m_cur->m_autoRecover; };
  bool startPreAmpKilled() const { return m_cur->m_startPreAmpKilled; };
  bool startPreAmpAuto() const { return m_cur->m_startPreAmpAuto; };
  bool useHistogrammer() const { return m_cur->m_useHistogrammer; };
  unsigned int dataTakingSpeed() const { return m_cur->m_dtSpeed; };
  bool sendConfigAtECR() const { return m_cur->m_sendConfigAtECR; };
  unsigned int getFei3ECRMode() const { return m_cur->m_fei3ECRMode; };
    uint32_t getFei4ECRPixelMask() const {
      std::stringstream interpreter;
      uint32_t ch_int;
      interpreter << std::hex << m_fei4ECRPixelMask;
      interpreter >> ch_int;
      return ch_int;
    };

 protected:
  RunConfig *m_conf;

 private:
  void initConfig(std::string name);
 
  int m_internalGen;                 //! ROD Internal event generation
  int m_consecutiveLVL1;             //! Number of consecutive LVL1 
  int m_consecutiveLVL1_IBL;         //! Number of consecutive LVL1 for IBL
  int m_latency;                     //! Trigger latency
  int m_nPixInject;                  //! Number of pixels to strobe
  int m_vcal;                        //! Vcal 
  bool m_digitalInject;              //! Analog or digital injection
  bool m_cInjHi;                     //! Cinj low or high
  
  // 14 Aug the following parameters added to strobe injections during datataking (noise tests) and so on
  bool m_injectMode;                 //! Need to be set to 1 to write MCC registers to setup calibration pulse
  int m_mccCalDelay;                 //! MCC injection pulse, enable and delay settings 
  int m_rodCalDelay;
  int m_mccCalWidth;                 //! Width of injection pulse
 
  int m_rodBcidOffset;               //! BCID offset in ROD for error correction
  //27 Aug parameters for injection during datataking - VD
  InjectionType m_injType;
  int m_injStep;
  int m_injStartPoint;
  int m_groupMask[MAX_GROUPS];       //! Enabling mask for datataking
  bool m_readoutEnabled[MAX_GROUPS]; //! Readout enable mask for datataking
  // ROD events simulation - added GAO 25 Sept 07
  bool m_simActive;
  bool m_simMode;
  int m_lVL1Accept;
  int m_skippedLVL1;
  int m_bitFlipMask;
  int m_mccFlags;
  int m_feFlags;
  int m_moduleOccupancy;
  int m_formattersEnaMask;
  // ROD monitoring parameters
  bool m_rawDataMode;
  bool m_autoDisable;
  bool m_autoRecover;
  bool m_startPreAmpKilled;
  bool m_startPreAmpAuto;
  bool m_useHistogrammer;
  //RecoAtECR
  bool m_sendConfigAtECR;
  std::string m_fei4ECRPixelMask;
  unsigned int m_fei3ECRMode;
  //Others
  unsigned int m_rodMonPrescale;
  unsigned int m_occupRate;
  unsigned int m_busyRate;
  unsigned int m_statusRate;
  unsigned int m_buffRate;
  unsigned int m_quickStatusMode;
  unsigned int m_quickStatusConfig;
  unsigned int m_syncConfig;
  unsigned int m_syncConfigROD;
  unsigned int m_dtSpeed;
  PixRunConfig *m_cur;
  std::map<std::string, PixRunConfig*> m_sub;
};

}

#endif
