/////////////////////////////////////////////////////////////////////
// PixController.cxx/////////////////////////////////////////////////////////////////////
//
// 08/04/03  Version 1.0 (PM)
//           Initial release
//

#include "PixController/PixController.h"
#include "PixController/PPCppRodPixController.h"
#include "PixController/BarrelRodPixController.h"

using namespace PixLib;

PixController* PixController::make(PixModuleGroup &grp, DbRecord *dbRecord, std::string const & type) {
  PixController *ctrl = nullptr;

  if (type == "BarrelRodPixController") {
    ctrl = new BarrelRodPixController(grp, dbRecord);
  }
  else if (type == "PPCppRodPixController") {
    ctrl = new PPCppRodPixController(grp, dbRecord);
  }
  return ctrl;
}

PixController* PixController::make(PixModuleGroup &grp, PixDbServerInterface *dbServer, std::string const & dom, std::string const & tag, std::string const & type) {
  PixController *ctrl = nullptr;
  
  if (type == "BarrelRodPixController") {
    ctrl = new BarrelRodPixController(grp, dbServer, dom, tag);
  }
  else if (type == "PPCppRodPixController") {
    ctrl = new PPCppRodPixController(grp, dbServer, dom, tag);
  }
  return ctrl;
}

PixController* PixController::make(PixModuleGroup &grp, std::string const & type) {
  PixController *ctrl = nullptr;
  
  if (type == "BarrelRodPixController") {
    ctrl = new BarrelRodPixController(grp);
  }
  else if (type == "PPCppRodPixController") {
    ctrl = new PPCppRodPixController(grp);
  }
  return ctrl;
}

PixController::~PixController() {
  delete m_conf;
}

void PixController::loadConfig(DbRecord *rec) {
  m_dbRecord = rec;
  if (rec) m_conf->read(rec);
}

void PixController::saveConfig(DbRecord *rec) {
  m_dbRecord = rec;
  if (rec) m_conf->write(rec);
}

void PixController::disableSLink( ) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void PixController::getHisto(HistogramType type, unsigned int xmod, unsigned
	      int slv, std::vector< std::vector<Histo*> > &his, int loopStep) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void PixController::abortScan() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  this->stopScan(); // for non-IBL RODS, aborting was implemented the same way as stopping
} 
