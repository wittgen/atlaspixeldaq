//////////////////////////////////////////////////////////////////
// BarrelRodPixController.h
/////////////////////////////////////////////////////////////////////
//
// Author: Laser Kaplan
// Based on: RodPixController + PPCppRodPixController
//

//! Class for the Barrel ROD (PPCpp)

#ifndef _PIXLIB_BARRELRODPIXCONTROLLER
#define _PIXLIB_BARRELRODPIXCONTROLLER

#include "PixController.h"
#include "PixController/PPCppRodPixController.h"
#include "PixUtilities/PixMessages.h"
#include "RodCrate/AbstrRodModule.h"
#include "PixModule/PixModule.h"
#include "RodCrate/IblRodModule.h"
#include "IblRodScanStatus.h"
#include "DataTakingConfig.h"
/* cannot really include registeraddress.h clashes with BocAddrresses.h */
#include "RodCrate/registeraddress.h"
/* ugly hack */
#undef BOC_RESET
#undef BOC_STATUS
#undef BOC_MODULE_TYPE
#undef BOC_MANUFACTURER
#undef BOC_SERIAL_NUMBER

#include "PixDbServer/PixDbServerInterface.h"

class ServerInterface;

namespace PixLib {
    class Config;
    class PixDbServerInterface;
    class PixRunConfig;
    class PixModule;


    //! Pix Controller Exception class; an object of this type is thrown in case of a ROD error
    class BarrelRodPixControllerExc : public PixControllerExc {
        public:
            // Todo: check how much the upstream system relies on them, and if we can discard some that are irrelevant.
            enum ErrorType{OK, BAD_SLOT_NUM, DMA_NOT_PERMITTED, TOO_MANY_MODULES, INVALID_MODID, NO_VME_INTERFACE, ILLEGAL_HASHING_SCHEME, NON_UNIFORM_POINTS_NOT_IMPLEMENTED, IS, BAD_CONFIG, INCONSISTENCY, NO_PPC_CONNECTION, OTHER};
            //! Constructor
            BarrelRodPixControllerExc(ErrorType type, ErrorLevel el, std::string name, int info1) :
                PixControllerExc(el, name), m_errorType(type), m_info1(info1) {};
            BarrelRodPixControllerExc(ErrorType type, ErrorLevel el, std::string name) :
                PixControllerExc(el, name), m_errorType(type), m_info1(0) {};

            //! Dump the error
            virtual void dump(std::ostream &out) {
                out << "Barrel Controller " << getCtrlName();
                out << " -- Type : " << dumpType();
                out << " -- Level : " << dumpLevel() << std::endl;
            }
            //! m_errorType accessor
            ErrorType getErrorType() { return m_errorType; };
        private:
            std::string dumpType() {
                std::string message;
                switch (m_errorType) {
                    case OK :
                        return "No errors";
                    case BAD_SLOT_NUM :
                        std::ostringstream(message) <<  "Invalid ROD slot number (" << m_info1 << ")" ;
                        return message;
                    case BAD_CONFIG :
                        return "Error reading from module configuration";
                    case NO_PPC_CONNECTION :
                        return "No connection to the PPC server";
                    default :
                        return "Uknown error";
                }
            }
            ErrorType m_errorType;
            int m_info1;//, m_info2;
    };

    class BarrelRodPixController : public PPCppRodPixController {

#define MAX_GROUPS 4
#define MAX_LVL1ACCEPT 15
#define MAX_SKIPPEDLVL1 15
#define MAX_BITFLIPS 7
#define MAX_MCCFLAGS 255
#define MAX_FEFLAGS 255
#define MAX_HITS 255
#define MAX_MODULES 32 // 4 groups of possibly up to 8 modules

        public:
            BarrelRodPixController(PixModuleGroup &modGrp, DbRecord *dbRecord);       //! Constructor

            BarrelRodPixController(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag);       //! Constructor

            BarrelRodPixController(PixModuleGroup &modGrp);                           //! Constructor
            virtual ~BarrelRodPixController() = default;                                        //! Destructor

            virtual void initHW();                                              //! Hardware (re)init
            void writeModuleConfig(PixModule& mod, bool forceWrite = false);                  //! Write module configuration
            virtual void readModuleConfigIdentifier(int moduleId, char* idStr, char* tag, uint32_t* revision, uint32_t* crc32chksm);  //! Read module configuration identifier
            virtual void sendModuleConfig(unsigned int moduleMask = 0, unsigned int bitMask=0x3fff );         //! Send module configuration
            virtual void sendModuleConfigWithPreampOff(unsigned int moduleMask, unsigned int registerMask = 0x3fff);         //! Send module configuration
	    virtual void outputMessageForConfig(SendModuleConfig &cmd);

            //virtual void setCalibrationMode();
            virtual void setConfigurationMode();

            virtual void disableLink(int link);
            virtual void disableSLink( );
            virtual void enableLink(int link);

            virtual void writeScanConfig(PixScan &scn);                               //! Write scan parameters
	    //TODO:: proper inheritance
	    
	    virtual uint32_t getRxMaskFromModuleMask(uint32_t moduleMask);
	    virtual int nTrigger();
	    
            virtual void startScan();                                                 //! Start a scan
            virtual void getHisto(HistogramType type, unsigned int xmod, unsigned
                    int slv, std::vector< std::vector<Histo*> > &his);  //! Read an histogram
            virtual void getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> > &his, int loopStep);  //! Read an histogram
                //! Read a Fit from Dsp
            virtual void getOptoscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his);
            virtual void getMonLeakscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his);
            virtual bool getRowColFromPixelIndex(int index, int FE, int &row, int &col);


            virtual void writeRunConfig(PixRunConfig* run); //! Sets the run configuration parameters from PixModuleGroup
            virtual void configureEventGenerator(PixRunConfig* run, bool active);//! Dumps config mask into FPGA registers
            virtual void startRun(PixRunConfig* run);           //! Start a run
            virtual void stopRun(PixHistoServerInterface *hsi);  //! Terminates a run
            bool moduleActive(int nmod); // is this module being used in the current running?
            virtual int moduleOutLink(int nmod); 
            int getModuleRx(int modId); // what is the rx for the module by modId?
            virtual int getSlaveFromModID(int modId);
            virtual int getModuleIDFromRx(int rxCh);
            virtual int getFeNumberFromRx(int rxCh){return -1;}

            virtual void setGroupId(int module, int group);
            virtual void enableModuleReadout(int module);
            virtual void disableModuleReadout(int module, bool trigger);
            virtual void dumpRodBusyStatus( );

	    virtual void resetModules(uint32_t moduleMask, uint32_t syncW);

            virtual uint32_t getRunMask();

            virtual  unsigned int &fmtLinkMap(int fmt);   

            virtual bool writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag="Rod_Tmp", unsigned int revision=0xffffffff);
            virtual bool readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision=0xffffffff);

        protected:
            virtual void configInit();         //! Init configuration structure
            virtual void initController();
	    std::array<int,MAX_MODULES> m_moduleRx = {}; // store the module Rxs by module position

        private:
           virtual void ActivateECRReconfiguration(uint32_t moduleMask, bool activate, uint32_t params = 0);
           static bool m_errPublishedConn;
           static bool m_errPublishedResponding;
           static bool m_errPublishedChannels;
    };
}
#endif // _PIXLIB_PPCPPRODPIXCONTROLLER
