///////////////////////////////////////////////////////////////
// Ppcpprodpixcontroller.h
/////////////////////////////////////////////////////////////////////
//
// Authors: K. Potamianos <karolos.potamianos@cern.ch>
// Date: 2013-X-10
// Authors: Russell Smith, Laura Jeanty
// Date: 2014-X-02/03
// Description: This class interfaces with the PPCpp code (C++) running
// 		on the PPC (IBL ROD). It uses the command pattern.
// Based on: RodPixController
//
//! Class for the Pixel CPPROD

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

//todo clean up includes
#include "Fei4Cfg.h"
#include "Fei4Data.h"
#include "PixScan.h"

//needed for the templated version of compareRODHostHash()
#include "Fei3ModCfg.h"

//#include "Histo/Hist.h"
#include "PixModule/PixModule.h"
#include "PixFe/PixFeI4.h"
#include "PixController/PPCppRodPixController.h"
#include "PixConnectivity/RodBocConnectivity.h"

#include "PixUtilities/PixISManager.h"

#include "WriteConfig.h"
#include "SendModuleConfig.h"
#include "ReadBackFei4Registers.h"
#include "StartScan.h"
#include "PixScanBaseToRod.h"
#include "AbortScan.h"
#include "ResetRodBusyHistograms.h"
#include "SetScanPixelDisable.h"
#include "DumpRodRegisters.h"
#include "GetServiceRecords.h"
#include "DataTakingTools.h"
#include "ReadRodVetoCounters.h"
#include "RecoAtECR_manager.h"
#include "GetDSPResults.h"
#include "CleanDSPResults.h"
#include "SetNetwork.h"
#include "StartHisto.h"
#include "StopHisto.h"

#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>

#include "TCanvas.h"

using namespace PixLib;
using namespace SctPixelRod;

bool PPCppRodPixController::m_errPublishedConn       = false;
bool PPCppRodPixController::m_errPublishedResponding = false;
bool PPCppRodPixController::m_errPublishedChannels   = false;

PPCppRodPixController::PPCppRodPixController(PixModuleGroup &modGrp) : PixController(modGrp) { //! Constructor
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  configInit();
  initController();
}

PPCppRodPixController::PPCppRodPixController(PixModuleGroup &modGrp, DbRecord *dbRecord): PixController(modGrp, dbRecord) { //! Constructor
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  configInit();
  if (dbRecord != NULL) m_conf->read(dbRecord);
  initController();
}

PPCppRodPixController::PPCppRodPixController(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag) : PixController(modGrp) {
  m_ctrlVerbose = false;
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  configInit();
  if(!m_conf->read(dbServer, dom, tag,  getCtrlName()))
    std::cout << __FILE__ << ", " << __LINE__ << ": Problem in reading configuration" << std::endl;
  initController();
}

void PPCppRodPixController::defaultInit() {
  m_nMod = 0;
  m_preAmpOff = false;
  m_scanActive = false;
  m_histosTimedOut = false;
  m_autoDisabledLinks.fill(-1);
  m_modPosition.fill(-1);
}

//todo cleanup
void PPCppRodPixController::initController() {
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  defaultInit();
  // Server for sending commands
  // Todo: make this ENV dependent
  // todo hardcoded 32
  // todo hardcoded ip
  m_rod = std::make_unique<IblRodModule>(0, 0, 1, CMD_SRV_IP_PORT, m_ipAddress); 
  m_disableFeMaskRun = 0x0;
  m_disableFeMaskDacs = 0x0;
  m_disableFeMaskConfig = 0x0;
  fakeScan = false; //todo don't commit when true... keep false as default
  if(fakeScan) std::cout << "You are doing a fakeScan" <<std::endl;
  m_serviceRecordRead = false;
  m_flavour = PixModule::PM_FE_I4B;
  m_runOccHisto = false;
 }

void PPCppRodPixController::resetFPGA( ) {
  // Initializing FPGAs
  std::cout << __PRETTY_FUNCTION__ << std::endl;

  std::string rodName = getModGroup().getRodName().substr(0,(getModGroup().getRodName()).length()-4);

  // somewhat temporary vme reset of fpgas on ROD
  std::string yo = "/det/pix/daq/current/";
  if(getenv("PIX_HOME")) yo = getenv("PIX_HOME");
  yo += "scripts/iblRodFwReset "+rodName;
  std::string rstCmd = "SKIP_DIALOG=YES "+yo;
  const char *com = rstCmd.c_str();
  std::cout<<"our command now = "<<com<<std::endl;
  system(com);
}

void PPCppRodPixController::initHW() {
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Todo: ping IP, sanity check on HW connection

  // Initializing FPGAs
  resetFPGA();
  setPrmpVbpStandby();
}

void PPCppRodPixController::configInit() {
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Create the Config object
  m_conf = new Config("PPCppRodPixController"); 
  Config &conf = *m_conf;
  // Group general
  conf.addGroup("general");

  conf["general"].addInt("Slot", m_ctrlID, 16,"Rod slot", true);
  conf["general"].addString("IPaddress", m_ipAddress, "192.168.2.160","IPaddress", true); 
 
  conf.addGroup("Groups");
  for (int ob=0; ob<4; ob++) {
    for (int ph=0; ph<4; ph++) {
      std::ostringstream os, os1;
      os << "id" << ph+1 << "_" << ob;
      os1 << "PP0 " << ob << " Group Id phase " << ph+1;
      conf["Groups"].addInt(os.str(), m_grId[ob][ph], 1, os1.str(), true);
    }
  }

  // Group FMT
  conf.addGroup("fmt");
  for (unsigned int i=0; i<8; i++) {
    std::ostringstream fnum;
    fnum << i;
    std::string tit = "linkMap_" + fnum.str();
    conf["fmt"].addInt("linkMap_" + fnum.str(), m_fmtLinkMap[i], 0x54320000u,"Formatter "+fnum.str()+" link map", true);
  }
  m_modActive.fill(false);
  m_moduleRx.fill(std::make_pair<int, int>(-1,-1));

  conf.reset();
}

void PPCppRodPixController::writeModuleConfig(PixModule& mod, bool forceWrite) {     //! Write module configuration
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<<std::endl;
  // Adding a protection against configuration items with zero revision (i.e. defaults)
  if (mod.config().rev() == 0) {
      std::string msg = "Bad module config for " + mod.moduleName();
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
      throw PPCppRodPixControllerExc(PPCppRodPixControllerExc::BAD_CONFIG, PixControllerExc::FATAL,
				     getModGroup().getRodName(), m_ctrlID);
  }

  int moduleId = mod.moduleId();
  if (moduleId < 0 || moduleId >= MAX_MODULES) {
    std::string msg = "Invalid module ID for " + mod.moduleName();
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    throw PPCppRodPixControllerExc(PPCppRodPixControllerExc::INVALID_MODID, PixControllerExc::ERROR,  
				   getModGroup().getRodName(), m_ctrlID); 
  }else m_modActive[moduleId] = true;

  // read position of module and, if necessary, add a new module
  unsigned int pos;
  if (m_modPosition[moduleId] < 0 || m_modPosition[moduleId] >= MAX_MODULES) {
    pos = m_nMod++; // New module
    if (pos >= MAX_MODULES) { 
      std::string msg = "Too many modules: cannot load " + mod.moduleName();
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
      throw PPCppRodPixControllerExc(PPCppRodPixControllerExc::TOO_MANY_MODULES, PixControllerExc::FATAL,  
				     getModGroup().getRodName(), m_ctrlID); }
    m_modPosition[moduleId] = pos; 
  } else pos = m_modPosition[moduleId]; // Redefinition of a module

  std::pair< int, int> modRx(-1,-1);

  // loop over front ends and prepare config to send
  for(auto fe = mod.feBegin(); fe != mod.feEnd(); fe++){
    auto fei4 = dynamic_cast<PixFeI4*>(*fe);
    if (!fei4) continue; // ignore non fei4 front-ends

  WriteConfig<Fei4Cfg> writeCfg;
  writeCfg.setCalibConfig(); 

  Fei4Cfg feCfg; 

  uint8_t txCh;
  uint8_t rxCh;
  // this is potentially temporary -- may change to use In(Out)putLink instead of BocIn(Out)putLink
  uint8_t bocInputLink = ((ConfInt&)mod.config()["general"]["BocInputLink"]).getValue();
  uint8_t bocOutputLink = (*fe)->number() ? ((ConfInt&)mod.config()["general"]["BocOutputLink1"]).getValue() 
    : ((ConfInt&)mod.config()["general"]["BocOutputLink2"]).getValue();
  
  // HARD-CODED TMP UNTIL CONNECTIVITY IS FIXED
  /*
 *	A-side -- TX=0x ; RX1=1x ; RX2=0x
 *	C-side -- TX=3x ; RX1=2x ; RX2=3x
 *
 */

   if( bocOutputLink/10 < 2 ) { // C-side
       bocOutputLink = (*fe)->number() ? ((ConfInt&)mod.config()["general"]["BocOutputLink2"]).getValue()
	       : ((ConfInt&)mod.config()["general"]["BocOutputLink1"]).getValue();
       rxCh = 31 - ( (bocOutputLink%10) + (1-(bocOutputLink/10)) * 8 );
       txCh = 23 - (bocInputLink%10);
    } else { // A-side
       rxCh = (bocOutputLink%10) + ((bocOutputLink/10)-2) * 8;
       txCh = (bocInputLink%10);
    }

   if ( getModGroup().getRodName() == "ROD_C3_S11_ROD" || getModGroup().getRodName() == "ROD_C3_S18_ROD" ) {//Take connectivity from DB
     rxCh = (*fe)->number() ? ((ConfInt&)mod.config()["general"]["OutputLink1"]).getValue() : ((ConfInt&)mod.config()["general"]["OutputLink2"]).getValue();
   }

   if( !mod.pixFE(1) ) {
     std::cout << "Hello, I'm DBM" << std::endl;
     // Hack, hack, hack!

     // A-side
     if     (mod.moduleName() == "B000968") { txCh = 2  ; rxCh =  6; }
     else if(mod.moduleName() == "B000961") { txCh = 1  ; rxCh =  5; }
     else if(mod.moduleName() == "B000970") { txCh = 1  ; rxCh =  4; }
     else if(mod.moduleName() == "B002200") { txCh = 2  ; rxCh =  2; }
     else if(mod.moduleName() == "B000106") { txCh = 1  ; rxCh =  1; }
     else if(mod.moduleName() == "B002201") { txCh = 1  ; rxCh =  0; }
     else if(mod.moduleName() == "B000031") { txCh = 6  ; rxCh = 14; }
     else if(mod.moduleName() == "B002219") { txCh = 5  ; rxCh = 13; }
     else if(mod.moduleName() == "B002202") { txCh = 4  ; rxCh = 12; }
     else if(mod.moduleName() == "B002216") { txCh = 6  ; rxCh = 10; }
     else if(mod.moduleName() == "B002199") { txCh = 5  ; rxCh =  9; }
     else if(mod.moduleName() == "B002217") { txCh = 4  ; rxCh =  8; }
     // C-side
     else if(mod.moduleName() == "B000989") { txCh = 18 ; rxCh = 18; }
     else if(mod.moduleName() == "B000983") { txCh = 17 ; rxCh = 17; }
     else if(mod.moduleName() == "B000987") { txCh = 16 ; rxCh = 16; }
     else if(mod.moduleName() == "B002228") { txCh = 18 ; rxCh = 22; }
     else if(mod.moduleName() == "B002187") { txCh = 17 ; rxCh = 21; }
     else if(mod.moduleName() == "B001692") { txCh = 16 ; rxCh = 20; }
     else if(mod.moduleName() == "B000036") { txCh = 22 ; rxCh = 26; }
     else if(mod.moduleName() == "B000033") { txCh = 21 ; rxCh = 25; }
     else if(mod.moduleName() == "B000034") { txCh = 20 ; rxCh = 24; }
     else if(mod.moduleName() == "B000037") { txCh = 22 ; rxCh = 30; }
     else if(mod.moduleName() == "B000035") { txCh = 21 ; rxCh = 29; }
     else if(mod.moduleName() == "B002214") { txCh = 20 ; rxCh = 28; }

     // SR1
     // A-side 
     else if(mod.moduleName() == "B001688") { txCh = 0  ; rxCh =  0; }//MDBM-01
     else if(mod.moduleName() == "B002229") { txCh = 1  ; rxCh =  1; }//MDBM-13
     else if(mod.moduleName() == "B001686") { txCh = 2  ; rxCh =  2; }//MDBM-04
     else if(mod.moduleName() == "B000972") { txCh = 0  ; rxCh =  4; }//MSBM-25
     else if(mod.moduleName() == "B000013") { txCh = 1  ; rxCh =  5; }//MSBM-13
     else if(mod.moduleName() == "B000982") { txCh = 2  ; rxCh =  6; }//MSBM-28

     // C-side - > not sure if it is C-side, but using it as the default
     else if(mod.moduleName() == "B001688") { txCh = 16 ; rxCh =  16; }//MDBM-01
     else if(mod.moduleName() == "B002229") { txCh = 17 ; rxCh =  17; }//MDBM-13
     else if(mod.moduleName() == "B001686") { txCh = 18 ; rxCh =  18; }//MDBM-04
     else if(mod.moduleName() == "B000972") { txCh = 16 ; rxCh =  20; }//MSBM-25
     else if(mod.moduleName() == "B000013") { txCh = 17 ; rxCh =  21; }//MSBM-13
     else if(mod.moduleName() == "B000982") { txCh = 18 ; rxCh =  22; }//MSBM-28

   }


   if(m_ctrlVerbose)std::cout<<"Module "<<mod.moduleName()<<" with id: "<<moduleId<<", and position: "<<pos<<", fe: "<<(*fe)->number()<<" has BocInputLink: "<<(int)bocInputLink<<" txCh "<<(int)txCh<<" and BocOutputLink: "<<(int)bocOutputLink<<" rxCh "<<(int)rxCh<<std::endl;

   if (m_ctrlVerbose) std::cout<<"for fe with SN: "<<(int)(fei4->readGlobRegister(Fei4::GlobReg::EFUSE))
			     <<" and chipID = "<<(int)(((ConfInt&)(*fe)->config()["Misc"]["Address"]).getValue())
			     <<" transformed rxCh to: "<<(int)rxCh<<" and txCh to: "<<(int)txCh<<std::endl;

    feCfg.setTxCh(txCh);
    feCfg.setRxCh(rxCh); 
    writeCfg.setFeToWrite(rxCh); // to-do: remove one writeConfig gets rx from rx (this is redundant)
    
    (*fe)->number() ? modRx.second = rxCh : modRx.first = rxCh; // store the mod rx for use later 
    
    if ( !( (bool)((ConfBool&)(*fe)->config()["Misc"]["ConfigEnable"]).value()) )
      m_disableFeMaskConfig |= (1 << rxCh); // store disabled FE for later in sending configuration 
    if ( !( (bool)((ConfBool&)(*fe)->config()["Misc"]["ScanEnable"]).value()) ) // is this FE disabled in the config?
      m_disableFeMaskRun |= (1 << rxCh); // store disabled FE for later in sending running in data-taking or calibration
    if ( !( (bool)((ConfBool&)(*fe)->config()["Misc"]["DacsEnable"]).value()) ) // is this FE disabled in the config?
      m_disableFeMaskDacs |= (1 << rxCh); // store disabled FE for later in checking whether or not we successfully enabled this channel

    if (m_ctrlVerbose) std::cout << "Module " << m_modGroup.getModIdName(mod.moduleId()) << " (" << mod.moduleName() << ") with ID " << mod.moduleId() << " has FE #" << (*fe)->number() << " with TX " << (int)txCh << " and RX " << (int)rxCh << ", and chip ID " << (int)(((ConfInt&)(*fe)->config()["Misc"]["Address"]).getValue()) << std::endl;

    feCfg.setRevision(mod.config().rev());
    feCfg.setModuleID(mod.moduleId());

    feCfg.setChipID((int)((ConfInt&)(*fe)->config()["Misc"]["Address"]).getValue());
    feCfg.setEnabled((bool)((ConfBool&)(*fe)->config()["Misc"]["ConfigEnable"]).value());
    feCfg.setVcal0((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient0"]).value());
    feCfg.setVcal1((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient1"]).value());
    feCfg.setCinjLow((float)((ConfFloat&)(*fe)->config()["Misc"]["CInjLo"]).value());
    feCfg.setCinjMed((float)((ConfFloat&)(*fe)->config()["Misc"]["CInjMed"]).value());
    feCfg.setCinjHigh((float)((ConfFloat&)(*fe)->config()["Misc"]["CInjHi"]).value());

    // FEI4B global config variabales
    // Defaults defined in PixFe/PixFeI4Config.cxx
    feCfg.SME.write(fei4->readGlobRegister(Fei4::GlobReg::SME));
    feCfg.EventLimit.write(fei4->readGlobRegister(Fei4::GlobReg::EventLimit));
    feCfg.Trig_Count.write(fei4->readGlobRegister(Fei4::GlobReg::Trig_Count));
    feCfg.CAE.write(fei4->readGlobRegister(Fei4::GlobReg::CAE));
    feCfg.ErrorMask_0.write(fei4->readGlobRegister(Fei4::GlobReg::ErrorMask_0));
    feCfg.ErrorMask_1.write(fei4->readGlobRegister(Fei4::GlobReg::ErrorMask_1));
    feCfg.PrmpVbp_R.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbp_R));
    feCfg.BufVgOpAmp.write(fei4->readGlobRegister(Fei4::GlobReg::BufVgOpAmp));
    feCfg.PrmpVbp.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbp));
    feCfg.TDACVbp.write(fei4->readGlobRegister(Fei4::GlobReg::TDACVbp));
    feCfg.DisVbn.write(fei4->readGlobRegister(Fei4::GlobReg::DisVbn));
    feCfg.Amp2Vbn.write(fei4->readGlobRegister(Fei4::GlobReg::Amp2Vbn));
    feCfg.Amp2VbpFol.write(fei4->readGlobRegister(Fei4::GlobReg::Amp2VbpFol));
    feCfg.Amp2Vbp.write(fei4->readGlobRegister(Fei4::GlobReg::Amp2Vbp));
    feCfg.FDACVbn.write(fei4->readGlobRegister(Fei4::GlobReg::FDACVbn));
    feCfg.Amp2Vbpff.write(fei4->readGlobRegister(Fei4::GlobReg::Amp2Vbpff));
    feCfg.PrmpVbnFol.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbnFol));
    feCfg.PrmpVbp_L.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbp_L));
    feCfg.PrmpVbpf.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbpf));
    feCfg.PrmpVbnLCC.write(fei4->readGlobRegister(Fei4::GlobReg::PrmpVbnLCC));
    feCfg.S0.write(fei4->readGlobRegister(Fei4::GlobReg::S0));
    feCfg.S1.write(fei4->readGlobRegister(Fei4::GlobReg::S1));
    uint16_t pixelStrobe=0;
    for(unsigned int j=0; j< Fei4PixelCfg::n_Bit;j++ ) // 13 bits of pixel_strobe_latch general register 
      pixelStrobe |= (((fei4->readGlobRegister(Fei4::GlobReg::Pixel_latch_strobe)>>j) & 0x1)<<(12-j));
    feCfg.Pixel_latch_strobe.write(pixelStrobe);
    feCfg.LVDSDrvIref.write(fei4->readGlobRegister(Fei4::GlobReg::LVDSDrvIref));
    feCfg.GADCCompBias.write(fei4->readGlobRegister(Fei4::GlobReg::GADCCompBias));
    feCfg.PllIbias.write(fei4->readGlobRegister(Fei4::GlobReg::PllIbias));
    feCfg.LVDSDrvVos.write(fei4->readGlobRegister(Fei4::GlobReg::LVDSDrvVos));
    feCfg.TempSensIbias.write(fei4->readGlobRegister(Fei4::GlobReg::TempSensIbias));
    feCfg.PllIcp.write(fei4->readGlobRegister(Fei4::GlobReg::PllIcp));
    feCfg.PlsrIDACRamp.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrIDACRamp));
    feCfg.VrefDigTune.write(fei4->readGlobRegister(Fei4::GlobReg::VrefDigTune));
    feCfg.PlsrVgOpAmp.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrVgOpAmp));
    feCfg.PlsrDACbias.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrDACbias));
    feCfg.VrefAnTune.write(fei4->readGlobRegister(Fei4::GlobReg::VrefAnTune));
    feCfg.Vthin_Coarse.write(fei4->readGlobRegister(Fei4::GlobReg::Vthin_Coarse));
    feCfg.Vthin_Fine.write(fei4->readGlobRegister(Fei4::GlobReg::Vthin_Fine));
    feCfg.HLD.write(fei4->readGlobRegister(Fei4::GlobReg::HLD));
    feCfg.DJO.write(fei4->readGlobRegister(Fei4::GlobReg::DJO));
    feCfg.DHS.write(fei4->readGlobRegister(Fei4::GlobReg::DHS));
    feCfg.PlsrDAC.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrDAC));
    feCfg.CP.write(fei4->readGlobRegister(Fei4::MergedGlobReg::CP));
    feCfg.Colpr_Addr.write(fei4->readGlobRegister(Fei4::GlobReg::Colpr_Addr));
    feCfg.DisableColCnfg0.write(fei4->readGlobRegister(Fei4::GlobReg::DisableColCnfg0));
    feCfg.DisableColCnfg1.write(fei4->readGlobRegister(Fei4::GlobReg::DisableColCnfg1));
    feCfg.DisableColCnfg2.write(fei4->readGlobRegister(Fei4::GlobReg::DisableColCnfg2));
    feCfg.Trig_Lat.write(fei4->readGlobRegister(Fei4::GlobReg::Trig_Lat));
    feCfg.CMDcnt12.write((fei4->readGlobRegister(Fei4::GlobReg::CMDcnt12))&0x1FFF);
    feCfg.STC.write(fei4->readGlobRegister(Fei4::GlobReg::STC));
    feCfg.HD.write(fei4->readGlobRegister(Fei4::MergedGlobReg::HD));
    feCfg.PLL.write(fei4->readGlobRegister(Fei4::GlobReg::PLL));
    feCfg.EFS.write(fei4->readGlobRegister(Fei4::GlobReg::EFS));
    feCfg.STP.write(fei4->readGlobRegister(Fei4::GlobReg::STP));
    feCfg.RER.write(fei4->readGlobRegister(Fei4::GlobReg::RER));
    feCfg.ADC.write(fei4->readGlobRegister(Fei4::GlobReg::ADC));
    feCfg.SRR.write(fei4->readGlobRegister(Fei4::GlobReg::SRR));
    feCfg.HOR.write(fei4->readGlobRegister(Fei4::GlobReg::HOR));
    feCfg.CAL.write(fei4->readGlobRegister(Fei4::GlobReg::CAL));
    feCfg.SRC.write(fei4->readGlobRegister(Fei4::GlobReg::SRC));
    feCfg.LEN.write(fei4->readGlobRegister(Fei4::GlobReg::LEN));
    feCfg.SRK.write(fei4->readGlobRegister(Fei4::GlobReg::SRK));
    feCfg.M13.write((fei4->readGlobRegister(Fei4::GlobReg::CMDcnt12))&0x2000);
    feCfg.LV0.write(fei4->readGlobRegister(Fei4::GlobReg::LV0));
    feCfg.EN_40M.write(fei4->readGlobRegister(Fei4::GlobReg::EN_40M));
    feCfg.EN_80M.write(fei4->readGlobRegister(Fei4::GlobReg::EN_80M));
    feCfg.clk1.write(fei4->readGlobRegister(Fei4::MergedGlobReg::CLK1));
    feCfg.clk0.write(fei4->readGlobRegister(Fei4::MergedGlobReg::CLK0));
    feCfg.EN_160.write(fei4->readGlobRegister(Fei4::GlobReg::EN_160));
    feCfg.EN_320.write(fei4->readGlobRegister(Fei4::GlobReg::EN_320));
    feCfg.N8b.write(fei4->readGlobRegister(Fei4::GlobReg::N8b));
    feCfg.c2o.write(fei4->readGlobRegister(Fei4::GlobReg::c2o));
    feCfg.EmptyRecordCnfg.write(fei4->readGlobRegister(Fei4::GlobReg::EmptyRecordCnfg));
    feCfg.LVE.write(fei4->readGlobRegister(Fei4::GlobReg::LVE));
    feCfg.LV3.write(fei4->readGlobRegister(Fei4::GlobReg::LV3));
    feCfg.LV1.write(fei4->readGlobRegister(Fei4::GlobReg::LV1));
    feCfg.TM.write(fei4->readGlobRegister(Fei4::MergedGlobReg::TM));
    feCfg.TMD.write(fei4->readGlobRegister(Fei4::GlobReg::TMD));
    feCfg.ILR.write(fei4->readGlobRegister(Fei4::GlobReg::ILR));
    feCfg.PlsrRiseUpTau.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrRiseUpTau));
    feCfg.PPW.write(fei4->readGlobRegister(Fei4::GlobReg::PPW));
    feCfg.PlsrDelay.write(fei4->readGlobRegister(Fei4::GlobReg::PlsrDelay));
    feCfg.XDC.write(fei4->readGlobRegister(Fei4::GlobReg::XDC));
    feCfg.XAC.write(fei4->readGlobRegister(Fei4::GlobReg::XAC));
    feCfg.GADCSel.write(fei4->readGlobRegister(Fei4::GlobReg::GADCSel));
    feCfg.SELB0.write(fei4->readGlobRegister(Fei4::GlobReg::SELB0));
    feCfg.SELB1.write(fei4->readGlobRegister(Fei4::GlobReg::SELB1));
    feCfg.SELB2.write(fei4->readGlobRegister(Fei4::GlobReg::SELB2));
    feCfg.b7E.write(fei4->readGlobRegister(Fei4::GlobReg::b7E));
    feCfg.EFUSE.write(fei4->readGlobRegister(Fei4::GlobReg::EFUSE));
    
    ConfMask<short unsigned int> const & tdacReg = fei4->readTrim(Fei4::PixReg::TDAC);
    ConfMask<short unsigned int> const & fdacReg = fei4->readTrim(Fei4::PixReg::FDAC);
    auto const & enableReg = fei4->readPixRegister(Fei4::PixReg::Enable);
    auto const & largeCapReg = fei4->readPixRegister(Fei4::PixReg::LargeCap);
    auto const & smallCapReg = fei4->readPixRegister(Fei4::PixReg::SmallCap);
    auto const & ileakReg = fei4->readPixRegister(Fei4::PixReg::HitBus);
  
    // pixel configs store starting from col,row 0,0 while ppc stores starting from 1,1 (as in the fei4 manual)
    for(unsigned col=0; col < Fei4::nPixCol; ++col){
	    int const dc = col/2;
	    for(unsigned row=0; row < Fei4::nPixRow; ++row){
		    feCfg.outputEnable(dc).setPixel( row+1, col+1, enableReg.get(col,row));
		    feCfg.largeCapacitor(dc).setPixel( row+1, col+1, largeCapReg.get(col,row));
		    feCfg.smallCapacitor(dc).setPixel( row+1, col+1, smallCapReg.get(col,row));
		    feCfg.hitBusOut(dc).setPixel( row+1, col+1, ileakReg.get(col,row));
		    feCfg.TDAC(dc).setPixel( row+1, col+1, tdacReg.get(col,row));
		    feCfg.FDAC(dc).setPixel( row+1, col+1, fdacReg.get(col,row));
	    }
    }
    writeCfg.moduleConfig = feCfg;

    if(!forceWrite) {
        bool configsMatch  = compareRODHostHash(rxCh, GetFEConfigHash::fei4, feCfg);
        if (m_ctrlVerbose) {
          std::cout<<" DEBUG: Global config of FE "<<(int)rxCh<<" config I am going to send is: "<<std::endl;
          feCfg.dump();
        }
        if(!configsMatch) {
            std::cout << "Since the configs mismatch, writing new config for FE " << static_cast<int>(rxCh) << std::endl;
            sendCommand(writeCfg); // For now, have to send 1 FE per command... 2 commands per module
        } else {
            std::cout << "Since the configs are the same, skipped writing config for FE " << static_cast<int>(rxCh) << std::endl;
        }
    } else {
        sendCommand(writeCfg);
    }
  }//FE loop
  
  m_moduleRx[moduleId] = modRx; // store module rx for later use

  // Publishing HitDiscCnfg to IS
  uint32_t rodOfflineId = std::stoul( m_modGroup.rodConnectivity()->offlineId(), nullptr, 16 );
  std::cout << getModGroup().getRodName() << " == Offline ID: 0x" << std::hex << rodOfflineId << std::dec << std::endl;
  PixISManager *isPtr = m_modGroup.isManager();
  std::cout << "ModId map: " << moduleId << "\t" << m_modGroup.getModIdName(moduleId) << "\t" << mod.moduleName() << std::endl;
  for(auto fe = mod.feBegin(); fe != mod.feEnd(); fe++) {
    auto fei4 = dynamic_cast<PixFeI4*>(*fe);
    std::cout << "In Iterator Loop" << std::endl;
    if (dynamic_cast<PixFeI4*>(*fe) == 0) continue; // ignore non fei4 front-ends
    uint32_t hdcData = rodOfflineId;
    uint16_t feRxCh = moduleOutLink(moduleId) + (*fe)->number();
    hdcData |= ((feRxCh/8) & 0xF);
    hdcData |= ((feRxCh%8) & 0xF) << 24;
    // Format agreed upon with OFFLINE: 0xHLDDRRRR (because F is always zero for IBL)
    hdcData = (hdcData & 0x0FFFFFFF) | ((fei4->readGlobRegister(Fei4::MergedGlobReg::HD) & 0x3) << 28);
    // This alternative was finally DISCARDED: 0xFLDDHRRR
    // hdcData = (hdcData & 0xFFFF0FFF) | (((*fe)->readGlobRegister("HitDiscCnfg") & 0x3) << 12);
    // WARNING -- HARD-CODED -- Until we have a per-FE id
    std::string feId = std::to_string((*fe)->number()+1); // OK for A-side
    if(feRxCh > 15 ) feId = std::to_string(2-(*fe)->number()); // OK for C-side
    if(isPtr) isPtr->publish(m_modGroup.getISRoot()+m_modGroup.getModIdName(moduleId)+"_"+feId+"/HitDiscCnfgData", hdcData);
    //if(isPtr) isPtr->publish(m_modGroup.getISRoot()+mod.moduleName()+"_"+std::to_string((*fe)->number())+"/HitDiscCnfgData", hdcData);
    std::cout << getModGroup().getRodName() <<  " -- Publishing HitDiscCnfg value of 0x" << std::hex << hdcData << std::dec << std::endl;
  }

  if (fakeScan) return;
}

void  PPCppRodPixController::setPrmpVbpStandby( ) {

  // tmp LJ setting specific module standby configs
  // to-do: replace with module standby configs in extended config
  uint32_t chMask = 0;
  //  if (getModGroup().getRodName() == "ROD_I1_S7_ROD") chMask = 0x0000000F; // setting this guy back to lower prmpVbp value after
  // further oscillations at higher value...

  uint32_t prmVbp = 0x46;

  if (getModGroup().getRodName() == "ROD_I1_S6_ROD") { chMask = 0x0000000F; prmVbp = 0x35; }// lj and kerstin 10 Aug 15
  if (getModGroup().getRodName() == "ROD_I1_S7_ROD") { chMask = 0x000000F0; prmVbp = 0x30; }// lj 13 June 15  
  if (getModGroup().getRodName() == "ROD_I1_S8_ROD") chMask = 0x000000F0;
  if (getModGroup().getRodName() == "ROD_I1_S12_ROD") chMask = 0x0F000000;
  if (getModGroup().getRodName() == "ROD_I1_S14_ROD") {chMask = 0x0000000F; prmVbp = 56;} //MK 10 Sep 2018 
  if (getModGroup().getRodName() == "ROD_I1_S10_ROD") chMask = 0x00000F00; // 27 April 15
  if (getModGroup().getRodName() == "ROD_I1_S11_ROD") {chMask = 0x000F0000; prmVbp = 56; } // MK 22 Aug 2018
  
  std::cout << getModGroup().getRodName() << " == chMask = 0x" << std::hex << chMask << " prmpVbp = 0x"<<prmVbp<<std::dec << std::endl;
    if (chMask !=0) {
      DataTakingTools cmd;
      cmd.action = SetPrmpVbpStandby;
      cmd.channels = chMask;
      cmd.value = prmVbp;
      sendCommand(cmd);
      std::cout << getModGroup().getRodName() << " == Setting alternate value for prmpVbp = " << (int)cmd.value << std::endl;
    }
    else std::cout << getModGroup().getRodName() << " == Not overriding default prmpVbp" << std::endl;

  if (getModGroup().getRodName() == "ROD_I1_S7_ROD") { // lj 30 June 15  
    chMask = 0x0000000F; prmVbp = 0x36; 
    chMask = 0x0000000F; prmVbp = 0x2b; // Karolos: Sept 26, 2015 
    chMask = 0x0000000F; prmVbp = 0x28; //MK and KL 10/13/2015 
    chMask = 0x0000000F; prmVbp = 0x46; //MK and KL 10/23/2015 
    chMask = 0x0000000F; prmVbp = 0x33; //KP and KL 16/04/2016
    chMask = 0x0000000F; prmVbp = 0x2b; //KP and KL 21/06/2016
    chMask = 0x0000000F; prmVbp = 0x42; //CM and KL 26/07/2016

    DataTakingTools cmd;
    cmd.action = SetPrmpVbpStandby;
    cmd.channels = chMask;
    cmd.value = prmVbp;
    sendCommand(cmd);
    std::cout << getModGroup().getRodName() << " == Setting alternate value for prmpVbp = " << (int)cmd.value << std::endl;
  }

  // initialize masked pixels for scanning in one problematic FE
  if (getModGroup().getRodName() == "ROD_I1_S8_ROD")
    {  
      SetScanPixelDisable cmdDis;

      cmdDis.rx = 3;
      cmdDis.pixel = 58;
      cmdDis.dc = 13;
      cmdDis.clearAdd = true;      
      sendCommand(cmdDis);
      
      cmdDis.clearAdd = false;
      cmdDis.pixel = 59;
      sendCommand(cmdDis);
      
      cmdDis.pixel = 612;
      sendCommand(cmdDis);
      
      cmdDis.pixel = 613;
      sendCommand(cmdDis);
    }

  // Disabling 3DBM FBK FEs because they are le noisy
  DataTakingTools cmd;
  cmd.action = SetStandby3DBM;
  cmd.value = 0;
  cmd.channels = 0;
  if (getModGroup().getRodName() == "ROD_I1_S6_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S7_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S8_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S9_ROD")cmd.channels = 0x00018000;
  if (getModGroup().getRodName() == "ROD_I1_S10_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S11_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S14_ROD")cmd.channels = 0x00018000;
  if (getModGroup().getRodName() == "ROD_I1_S15_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S18_ROD")cmd.channels = 0x00008000;
  if (getModGroup().getRodName() == "ROD_I1_S20_ROD")cmd.channels = 0x00018000;
  //send command if anything needs disabling.
  // turn off everything except for slot 12 and 17 for running with BCM disabled
  //if (getModGroup().getRodName() != "ROD_I1_S12_ROD"
     //&&getModGroup().getRodName() != "ROD_I1_S17_ROD" )cmd.channels = 0x00018000;
  //if(cmd.channels!=0)sendCommand(cmd);
  sendCommand(cmd);
}

void PPCppRodPixController::readModuleConfigIdentifier(int moduleId, char* idStr, char* tag, uint32_t* revision, uint32_t* crc32chksm) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    *revision = 1; *crc32chksm = 1; //  These variables need to be initialized to something !!!
}

void PPCppRodPixController::sendModuleConfig(unsigned int moduleMask, unsigned int bitMask) {            //! Send module configuration
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (fakeScan) return;

  uint32_t rxMask = getRxMaskFromModuleMask(moduleMask);

  SendModuleConfig sendCfg; //SendModuleConfig command
  sendCfg.turnOnPreAmps(); // send with preAmps ON
  sendCfg.setCalibConfig(); 
  sendCfg.setModMask((rxMask & (~m_disableFeMaskConfig))); // which channels to send to
  sendCfg.setSendAll(true); // true: send global + pixel regs, false: send global regs only
  sendCfg.readBackSR(m_serviceRecordRead);
  sendCfg.setSleepDelay(100000); // delay in us between sending configs of each FE
  sendCommand(sendCfg);

  m_preAmpOff = false;
  outputMessageForConfig(sendCfg);
}

void PPCppRodPixController::readBackFEI4Latches(unsigned int moduleMask) {
  ReadBackFei4Registers readback_cmd;
  uint32_t rxMask = getRxMaskFromModuleMask(moduleMask);  
  readback_cmd.m_modMask = (rxMask & (~m_disableFeMaskConfig));
  readback_cmd.m_global  = 0;
  readback_cmd.m_DC      = 0x0;
  readback_cmd.m_CP      = 0x0;
  readback_cmd.rb_mode   = ReadBackFei4Registers::RB_PixelRegisters;
  sendCommand(readback_cmd);
  
  std::string filename = "/det/pix/logs/PixelRegisterDump/rbFe_"+getModGroup().getRodName()+"_";
    
  std::time_t time_result  = std::time(nullptr);
  struct tm tt = *(std::localtime(&time_result));
  std::string formattedtime = std::to_string(tt.tm_sec)+"_"+std::to_string(tt.tm_min)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_year+1900);
  //TFile outfile((filename+"_"+std::to_string(time_result)+".root").c_str(),"RECREATE");
  TFile outfile((filename+"_"+formattedtime+".root").c_str(),"RECREATE");

  for (std::map< uint32_t, Fei4PixelCfg>::iterator PixCfgMapIt = readback_cmd.result.PixCfgMap.begin(); PixCfgMapIt != readback_cmd.result.PixCfgMap.end(); ++PixCfgMapIt) {
    TH2F  tdac((std::to_string(PixCfgMapIt->first)+"_tdac"+getModGroup().getRodName()).c_str(),(std::to_string(PixCfgMapIt->first)+"_tdac").c_str(),80,-0.5,79.5,336,-0.5,335.5);
    TH2F  fdac((std::to_string(PixCfgMapIt->first)+"_fdac"+getModGroup().getRodName()).c_str(),(std::to_string(PixCfgMapIt->first)+"_tdac").c_str(),80,-0.5,79.5,336,-0.5,335.5);
    std::vector <TH2F> bit_h;
    for (int bit=0;bit<13;bit++){
      bit_h.push_back(TH2F((std::to_string(PixCfgMapIt->first)+"_bit_"+std::to_string(bit)+"_"+getModGroup().getRodName()).c_str(),(std::to_string(PixCfgMapIt->first)+"_bit_"+std::to_string(bit)).c_str(),80,-0.5,79.5,336,-0.5,335.5));
    }
    
    for (unsigned int irow=1; irow<=336; irow++) {
      for (unsigned int idc=0; idc<40; idc++) { 
	for (unsigned int col=1; col<=2; col++){
	  uint32_t TDAC = 0;
	  uint32_t FDAC = 0;

	  for (int bit=0;bit<13;bit++){
	      bit_h[bit].SetBinContent(2*idc+col,irow,(PixCfgMapIt->second).all(idc)[bit].getPixel(irow,col));
	    }
	  
	  for (int bit=4;bit>=0;bit--){
	    TDAC+=((PixCfgMapIt->second).TDAC(idc)[bit].getPixel(irow,col)<<(4-bit));}
	  
	  for (int bit=0;bit<4;bit++){
	    FDAC+=((PixCfgMapIt->second).FDAC(idc)[bit].getPixel(irow,col)<<(bit));}
	  
	  
	  
	  tdac.SetBinContent(2*idc+col,irow,TDAC);
	  fdac.SetBinContent(2*idc+col,irow,FDAC);
	  

	}//col
      }//idc     
   }//row
    


    tdac.Write();
    fdac.Write();
    for (int bit=0;bit<13;bit++){
      bit_h[bit].Write();}
   
    
  }//Config Map
  
  
  outfile.Close();
  

}


void PPCppRodPixController::sendModuleConfigWithPreampOff(unsigned int moduleMask, unsigned int bitMask) {            //! Send module configuration
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

  uint32_t rxMask = getRxMaskFromModuleMask(moduleMask);

  SendModuleConfig sendCfg; //SendModuleConfig command
  sendCfg.turnOffPreAmps(); // send with preAmps OFF
  sendCfg.setCalibConfig(); 
  sendCfg.setModMask((rxMask & (~m_disableFeMaskConfig))); // which channels to send to
  sendCfg.setSendAll(true); // true: send global + pixel regs, false: send global regs only
  sendCfg.readBackSR(m_serviceRecordRead);
  sendCfg.setSleepDelay(100000); // delay in us between sending configs of each FE
  sendCommand(sendCfg);

  m_preAmpOff = true;
  outputMessageForConfig(sendCfg);
}


void PPCppRodPixController::setConfigurationMode() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (m_mode != "CONFIGURATION") {
    /*// Karolos: should be enabled at some point
    DataTakingTools cmdDisableFmtLink;
    cmdDisableFmtLink.channels = 0xFFFFFFFF;
    sendCommand(cmdDisableFmtLink);*/
    /*// Karolos: commented out because it was doing nothing before the adjustments to disableLink
    for (int i=0; i<32; i++) {
      disableLink(moduleOutLink(i));
    }*/
    // Todo: Karolos -- Check config mode from RodController and see what's missing
  }
    m_mode = "CONFIGURATION";
}

void PPCppRodPixController::disableLink(int link) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << " for: "<<getModGroup().getRodName() << " with link " << link << std::endl;

  // We know that the link is the RxCh of the first FE
  uint32_t effectiveMask =  0;
  
  auto const [rx0, rx1] = getModuleRx(getModuleIDFromRx(link));
  
  if(rx0 >=0 ) effectiveMask |= 0x1 << rx0;
  if(rx1 >=0 ) effectiveMask |= 0x1 << rx1;
  
  std::cout << "Disabling FE with mask " << HEXF(8,effectiveMask) << std::endl;
  //bool isSlaveB = false;
 // if(effectiveMask & 0xFFFF0000)isSlaveB = true;

  DataTakingTools cmdDisableFmtLink;
  cmdDisableFmtLink.action = DisableFmtLink;
  cmdDisableFmtLink.channels = effectiveMask;
  sendCommand(cmdDisableFmtLink);

  if(rx0 != -1 ) m_autoDisabledLinks[rx0]=1;
  if(rx1 != -1 ) m_autoDisabledLinks[rx1]=1;
}

void PPCppRodPixController::disableSLink( ){

 if(getModGroup().rodDisabled())return;
 // read back current fmt link enable
 DumpRodRegisters dump;
 dump.dumpOnPPC = false;
 sendCommand(dump);

 uint32_t fmtALinkEnable = dump.result.FmtLinkEnableValue_A;
 uint32_t fmtBLinkEnable = dump.result.FmtLinkEnableValue_B;
 uint32_t enabledMask = ((fmtBLinkEnable << 16) | fmtALinkEnable);

 std::string RODNAME = getModGroup().getRodName();
   std::size_t pos = RODNAME.find("_ROD");
   RODNAME = RODNAME.substr(0,pos);
    if(enabledMask == 0x0 ){//All links disabled, disable ROD
        getModGroup().sendDisableMessage(RODNAME,"ROD");
        getModGroup().setRodDisableReason("Z");
    } else{
      if((enabledMask & 0xFF) == 0x0 && (m_runRxMask & 0xFF ) !=0  ){//Disable NORTH Slink0 
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(0))){
          std::cout << "All formatters associated to S-LINK 0 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(0),"ROD");
        }
      }
      if((enabledMask & 0xFF00) == 0x0 && (m_runRxMask & 0xFF00 ) !=0 ){//Disable NORTH Slink1 
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(1))){
          std::cout << "All formatters associated to S-LINK 1 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(1),"ROD");
        }
      }
      if((enabledMask &0xFF0000 ) == 0x0 && (m_runRxMask & 0xFF0000 ) !=0){//Disable SOUTH Slink0
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(2))){
          std::cout << "All formatters associated to S-LINK 2 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(2),"ROD");
        }
      }
      if((enabledMask &0xFF000000 ) == 0x0 && (m_runRxMask & 0xFF000000 ) !=0){//Disable SOUTH Slink1
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(3))){
          std::cout << "All formatters associated to S-LINK 3 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(3),"ROD");
        }
      }
    }
}

void PPCppRodPixController::enableLink(int link) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
}

uint32_t PPCppRodPixController::getRunMask() {
    return getRxMaskFromModuleMask(m_modGroup.getActiveModuleMask()) & ~(m_disableFeMaskRun); 
}   


void PPCppRodPixController::setRunMode() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

  if (m_mode == "RUN") {
    std::cout<<"RodPixController::setRunMode() sees that m_mode == RUN already, so it will do nothing -- exiting."<<std::endl;
    return;
  }

  // Enable TIM control and set normal data-taking routing path
  // Todo Karolos -- run on ROD IblRod::Master::RrifCtrlReg::write( RRIF_CTL_TIM_TRIG );

  // Enable formatter link

  // Enable S-Link

  // Todo Karolos -- determine whether we will use mode bits, or other features from RodPixController
  m_autoDisabledLinks.fill(-1);
  m_mode = "RUN";
}

void PPCppRodPixController::writeScanConfig(PixScan &scn) {  //! Write scan parameters
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (fakeScan) return;

  m_scanLoopParam0 = scn.getLoopParam(0);
  uint32_t scanModuleMask = scn.getModuleMask(0); // we should use group 0 for all ibl functions
  m_scanRxMask = 0;
  if (m_ctrlVerbose) std::cout<<"the getModuleMask from PixScan is: 0x"<<std::hex<<scanModuleMask<<std::dec<<std::endl;
  for (int i = 0; i < MAX_MODULES; i++) {
    if (scanModuleMask & (0x1 << i)) {
      if (((m_moduleRx[i].first) >= 0)) {
        if (m_ctrlVerbose) std::cout<<"For module i="<<i<<" moduleRx.second ="<<m_moduleRx[i].second<<std::endl;
	m_scanRxMask |= (0x1 << m_moduleRx[i].first);
        if (m_moduleRx[i].second >= 0) m_scanRxMask |= (0x1 << m_moduleRx[i].second);
        if (m_ctrlVerbose) std::cout<<"RxMask value for m_scanRxMask = 0x"<<std::hex<<m_scanRxMask<<std::dec<<std::endl;
      }
    }
    else m_modActive[i]  = false;
  }


  if (m_ctrlVerbose) std::cout<<"the fe rx channel map I built from the pixscan mask is: 0x"<<std::hex<<m_scanRxMask<<std::dec<<std::endl;
  m_scanRxMask = m_scanRxMask & ~(m_disableFeMaskRun);
  if (m_ctrlVerbose) std::cout<<"the fe rx channel map after applying the FE scan disable is: 0x"<<std::hex<<m_scanRxMask<<std::dec<<std::endl;

  // define ROD name in format needed
  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

  // publish to IS for fit server to pick up the scan rx channel mask
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming");
  ISInfoInt maskInfo (m_scanRxMask);
  dict.checkin( srvname + "." + RODNAME + "_ScanModuleMask", maskInfo );

  ISInfoInt feFlavour = (ISInfoInt)m_flavour;
  dict.checkin( srvname + "." + RODNAME + ".FEflavour", feFlavour);

  // better way to do this?
  if (!scn.getStrobeLVL1DelayOverride())
    scn.setStrobeLVL1Delay(((ConfInt&)m_modGroup.config()["general"]["TriggerDelay"]).getValue());

  // To-do: clean up casting
  PixScanBase * scnBase = &scn;
  SerializablePixScanBase serScnBase( static_cast<PixScanBase>(*scnBase) );

  PixScanBaseToRod pixScanBaseToRod; //same as  writeScanConfig command
  pixScanBaseToRod.setPixScanBase(serScnBase);

  sendCommand(pixScanBaseToRod);

  // save cap type for later if doing threshold scan; this is ugly; to-do: figure out a better way
  capType = 0;
  if (scn.getMaskStageMode() == PixScan::FEI4_ENA_LCAP) capType = 1 ;
  else if (scn.getMaskStageMode() == PixScan::FEI4_ENA_BCAP) capType = 2 ;
}

void PPCppRodPixController::startScan() {                      
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<<std::endl;
  
  // can create race condition with the start of scan. Enable when reset available via HCP
  // resetFPGA();

  m_scanActive = true;
  m_histosTimedOut = false; // reset the timeout on getting the histos from the fitserver
  if (fakeScan) return;

  // setting up IS stuff
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming"); // TODO: no hardocoding

  // define ROD name in format needed
  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

  // publishing startscan flag
  ISInfoString isgo(RODNAME);
  try {
    dict.checkin( srvname + "." + RODNAME + "_StartScan", isgo );
  } catch(...) {
    ERS_INFO("Dictionary checkin failed for Rod " << RODNAME);
  }
  ERS_LOG("Successfully checked Rod " << RODNAME << " into IS");

  // get scan ID and fit server port from IS
  ISInfoInt scanid;
  ISInfoString s0;
  ISInfoString s1;
  ISInfoString s2;
  ISInfoString s3;
  try{
    dict.getValue(srvname + "." + RODNAME + "_ScanId", scanid); 
    dict.getValue(srvname + "." + RODNAME + "_0_0", s0);
    dict.getValue(srvname + "." + RODNAME + "_0_1", s1);
    dict.getValue(srvname + "." + RODNAME + "_1_0", s2);
    dict.getValue(srvname + "." + RODNAME + "_1_1", s3);
  }
  catch(...) {
    std::string msg = "Could not get scanId and/or fitServerPort from Histogramming IS.";
    std::cerr << msg << std::endl;
    std::cerr<<"check that the fit server is running"<<std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
  }
  if (m_ctrlVerbose) std::cout<<" Got scan id: "<< (int)scanid << " and fit server ip:port combinations from IS: \n"
			    << (std::string)s0 << "\n" 
			    << (std::string)s1 << "\n" 
			    << (std::string)s2 << "\n" 
			    << (std::string)s3 << "\n" << std::endl;

  
  StartScan startScan; // start scan command
    startScan.channels = m_scanRxMask;
    startScan.scanId = scanid;

    for (int i = 0; i<4; i++) {
      std::stringstream s;
      if(i == 0) s.str(s0);
      if(i == 1) s.str(s1);
      if(i == 2) s.str(s2);
      if(i == 3) s.str(s3);
      int a,b,c,d,e;
      char ch;
      s >> a >> ch >> b >> ch >> c >> ch >> d >> ch >> e;
      startScan.fitPorts[i] = e;
      startScan.fitIps[i][0] = a;
      startScan.fitIps[i][1] = b;
      startScan.fitIps[i][2] = c;
      startScan.fitIps[i][3] = d;
    }
    //  Without GetStatus all status variables will remain as initialized here until the scan is done.
#ifdef __DO_IBL_GET_STATUS__
    m_scanResult.state = SCAN_IDLE;
#else
    m_scanResult.state = SCAN_IN_PROGRESS;
#endif
    m_scanResult.errorState = PPCERROR_NONE;
    m_scanResult.mask =  0;
    m_scanResult.par = 0;
    m_scanResult.par1 = 0;
    m_scanResult.par2 = 0;
    m_scanResult.dc = 0;
    m_scanResult.trig = 0;

    sendCommand(startScan);

#ifndef __DO_IBL_GET_STATUS__
    m_scanResult = startScan.result.m_scanResult;
#endif

    // Store IDs in case it needs to be stopped.
    m_scanId = scanid;
    m_scanTypeId = startScan.getTypeId();
    m_scanExecId = startScan.getExecId();
    // Reset error publishing flags
    m_errPublishedConn = 0;
    m_errPublishedResponding = 0;
    m_errPublishedChannels = 0;
}

bool PPCppRodPixController::moduleActive(int nmod) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

  if(nmod>=0 && nmod<32) {
    std::cout << "ModuleAct? " <<m_modActive[nmod] << std::endl;
    return m_modActive[nmod];
  }
  
  else return false;
}

void PPCppRodPixController::getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {  //! Read a histogram
  // if not specified, use loop -1 to not get any loop
  this->getHisto(type, xmod, slv, his, -1);
}

void PPCppRodPixController::getOptoscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {

  if (type != RAW_DATA_DIFF_2)return;
  while (his.size() < 32) {
    std::vector< Histo * > vh;
    his.push_back(vh);
  }
  if (mod >= 32)  {
    std::cout << "ATTENTION: "<< __PRETTY_FUNCTION__ <<" received module "<<mod<<std::endl;
    return;
  }
  his[mod].clear();

  std::array<int,2> rxCh;
  //Note DTO1 and DTO2 for IBL are mixed in north and south rxCh%2 == 0 --> DTO1
  if(getModuleRx(mod).first%2==0){
    rxCh[0] = getModuleRx(mod).first;
    rxCh[1] = getModuleRx(mod).second;
  } else {
    rxCh[1] = getModuleRx(mod).first;
    rxCh[0] = getModuleRx(mod).second;
  }

  Histo *hbest[2];

  for(int j=0;j<2;j++){

    std::ostringstream nam, tit;
    nam << "RAW_2_" << mod << "-Link" << j << "_0";
    tit << "Raw data 2 mod " << mod << "-Link" << j << " L2=0";
    hbest[j] = new Histo(nam.str(), tit.str(), 2, 4, -0.5, 3.5, 1 , 0 , 1);

    //For each Rx channel, return the ratio (between the received nframes and the expected nframes) and the errors from opto scan.
    GetDSPResults getDSP;
    getDSP.rxCh = rxCh[j];
    sendCommand(getDSP);

    if(getDSP.result.dspResults.size()!=4) continue;

    for(unsigned int i = 0; i < getDSP.result.dspResults.size(); i++){

      if(getDSP.result.dspResults.at(i).size() != 2) continue;
      uint32_t  HCP_nframe = getDSP.result.dspResults.at(i).at(0);
      uint32_t  HCP_nerrors = getDSP.result.dspResults.at(i).at(1);

      if((HCP_nframe!=0xFFFF) && (HCP_nerrors==0)){
	HCP_nerrors = 0xFFFF;
      }
      hbest[j]->set(i,0,(int)HCP_nerrors);
    }

    CleanDSPResults cleanDSP;
    cleanDSP.rxCh = rxCh[j];
    sendCommand(cleanDSP);
    his[mod].push_back(hbest[j]);
  }
}


void PPCppRodPixController::getMonLeakscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {

std::cout << "Getting MonLeak histogram for Module"<<(int)mod << std::endl;

if (type != MONLEAK_HISTO)return;

    while (his.size() < 32) {
      std::vector< Histo * > vh;
      //vh.clear();
      his.push_back(vh);
    }
    if (mod >= 32)  {
      std::cout << "ATTENTION: "<< __PRETTY_FUNCTION__ <<" received module "<<mod<<std::endl;
      return;
    }

    std::ostringstream nam, tit;
    nam << "MonLeak_Scan" <<  mod    << "_" << 0;//bin==0
    tit << "I(GADC), mod " << mod;
    Histo *h;
    h = new Histo(nam.str(), tit.str(),2, 160,-0.5,159.5,336,-0.5,335.5); 
    his[mod].clear();

    int rxCh[2];
    rxCh[0] = getModuleRx(mod).first;
    rxCh[1] = getModuleRx(mod).second;
    for(int ix=0;ix<2;ix++){

      GetDSPResults getDSP; // get Monleak results
      getDSP.rxCh = rxCh[ix];
      sendCommand(getDSP);

      uint32_t nPixPerFE = getDSP.result.dspResults.size();
      int col, row;
    
       for (uint32_t pix=0; pix<nPixPerFE; pix++) {//Loop over selected Pixels
        float ADC =0;
        float nIter = getDSP.result.dspResults[pix].size();//Number of iterations, should be 1...
	  for(uint32_t i=0;i<getDSP.result.dspResults[pix].size();i++)ADC += getDSP.result.dspResults[pix][i];
          if(nIter>0)ADC /= nIter;//Average over ADC iterations

          if(getRowColFromPixelIndex(pix,ix,row,col))h->set(col,row,ADC);
       }

      CleanDSPResults cleanDSP; // Clean up PPC
      cleanDSP.rxCh = rxCh[ix];
      sendCommand(cleanDSP);
    }

 his[mod].push_back(h);
}

bool PPCppRodPixController::getRowColFromPixelIndex(int index, int FE, int &row, int &col){

if (index >= 26880) return false;

  col = index/336;//Get col from Pixel Index
  row = index%336;//Get row from Pixel Index
  if(col%2 ==1)row = 335-row;//Inverted row for odd columns due to mask stage iteration
  col +=80*FE;//FE1 adding offset of 80 in col

return true;
}

void PPCppRodPixController::getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> >& his, int loopStep) {  //! Read a histogram
  //todo better description 
  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);
  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< " for rod: "<<RODNAME<<" with loop: "<<loopStep<<" and type: "<<type<<std::endl;
  
    if(type ==  MONLEAK_HISTO) {
      getMonLeakscanHisto(type, xmod, slv, his);
      return;
  }
    if (type == RAW_DATA_DIFF_2) {
      getOptoscanHisto(type, xmod, slv, his);
      return;
    }
  
  //setting up IS stuff
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming"); // TODO: no hardocoding  
  
  //check if FitServer has signalled that all histograms are available, if not keep querying IS
  ISInfoBool histoready = false; 
  dict.getValue(srvname + "." + RODNAME + "_FinishScan", histoready);
  int maxTimeOut = 200;
  int haveWaited = 0;

  // time out 1 min for non threshold histos; 3+ min for thresholds which take longer to fit by fit server
  if (type == SCURVE_MEAN || type == SCURVE_SIGMA || type == SCURVE_CHI2) maxTimeOut = 500;

  if (!m_histosTimedOut && !m_errPublishedResponding) { // if already timed out waiting for histos for this scan, don't wait again
    while(!histoready){ // wait for fit server to tell us that it's finished publishing histos
      dict.getValue(srvname + "." + RODNAME + "_FinishScan", histoready);
      sleep(1); haveWaited++;
      if (haveWaited > maxTimeOut) {
	m_histosTimedOut = true;
	PIX_ERROR("TIMED OUT WAITING FOR "<<type<<" HISTOS FROM FIT SERVER for ROD "<<RODNAME);
	break;
	}
    }
  }

  bool chargeConv = false; // convert x-axis in vcal to charge?
  bool delayConv = false; // convert x-axis in strobe delay to ns?
  bool isNoise = false; // flag whether scan involves noise histos
  std::string scanname, scanname1, scanname2;
  //int htype;
  PixModule *pmod=getModGroup().module(xmod);
  int nCol=pmod->nCol();
  int nCol_Loop = nCol;
  if(!pmod->pixFE(1)){
    std::cout << "This is DBM. Assumes that the second module pointer is invalid." << std::endl;
    nCol_Loop*=2;
  }
  int nRow=pmod->nRow();

  while (his.size() < 32) {
    std::vector< Histo * > vh;
    //vh.clear();
    his.push_back(vh);
  }
  switch (type) {
  case OCCUPANCY :
    scanname   = "Occup_";
    scanname1  = "Occupancy";
    scanname2  = scanname;
    //htype      = SCAN_OCCUPANCY;
    break;
  case TIMEWALK :
    scanname   = "Timew_";
    scanname1  = "Timewalk";
    scanname2  = "Thr_";
    delayConv = true;
    break;
  case SCURVE_MEAN :
    scanname   = "Thr_";
    scanname1  = "Thr";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_MEAN;
    if(m_scanLoopParam0 != EnumScanParam::STROBE_DELAY) chargeConv = true;
    break;
  case SCURVE_SIGMA :
    scanname   = "Noise_";
    scanname1  = "Noise";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_SIGMA;
    if(m_scanLoopParam0 != EnumScanParam::STROBE_DELAY) chargeConv = true;
    isNoise = true;
    break;
  case SCURVE_CHI2 :
    scanname   = "Chi2_";
    scanname1  = "Chi2";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_CHI2;
    chargeConv = false;
    break;
  case TOT_MEAN :
    scanname   = "ToTMean_";
    scanname1  = "ToTMean";
    scanname2  = scanname;
    //htype      = SCAN_TOT_MEAN;
    break;
  case TOT_SIGMA :
    scanname   = "ToTSigma_";
    scanname1  = "ToTSigma";
    scanname2  = scanname;
    //htype      = SCAN_TOT_SIGMA;
    break;
  default :
    std::string msg = "You requested a histogram type not currently supported";
    std::cerr << msg << std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    break;
  }

  // store variables for calculating charge from vcal
  float vcal_a1=0., vcal_b1=0., cinj1 = 0, delayCal1=1.;
  float vcal_a2=0., vcal_b2=0., cinj2 = 0, delayCal2=1.;
  if (chargeConv) {
    // fe 0
    if (pmod->pixFE(0)) {
      std::cout<<" getting vcal stuff for fe 0"<<std::endl; // tmp
      vcal_a1 = (dynamic_cast<ConfFloat &>((pmod->pixFE(0))->config()["Misc"]["VcalGradient1"])).value();
      vcal_b1 = (dynamic_cast<ConfFloat &>((pmod->pixFE(0))->config()["Misc"]["VcalGradient0"])).value();
      if(isNoise) vcal_b1 = 0;
      cinj1   = (dynamic_cast<ConfFloat &>((pmod->pixFE(0))->config()["Misc"]["CInjLo"])).value();
      if (capType==2){
	cinj1 = (dynamic_cast<ConfFloat &>((pmod->pixFE(0))->config()["Misc"]["CInjHi"])).value();
      } else if(capType==1){
	cinj1 = (dynamic_cast<ConfFloat &>((pmod->pixFE(0))->config()["Misc"]["CInjMed"])).value();
      }
    }
    if(delayConv){
      delayCal1 = ((ConfFloat&)pmod->pixFE(0)->config()["Misc"]["DelayCalib"]).value();
    }
    // fe 1
    if (pmod->pixFE(1)) {
      std::cout<<" getting vcal stuff for fe 1"<<std::endl; // tmp
      vcal_a2 = (dynamic_cast<ConfFloat &>((pmod->pixFE(1))->config()["Misc"]["VcalGradient1"])).value();
      vcal_b2 = (dynamic_cast<ConfFloat &>((pmod->pixFE(1))->config()["Misc"]["VcalGradient0"])).value();
      if(isNoise) vcal_b2 = 0;
      cinj2   = (dynamic_cast<ConfFloat &>((pmod->pixFE(1))->config()["Misc"]["CInjLo"])).value();
      if (capType==2){
	cinj2 = (dynamic_cast<ConfFloat &>((pmod->pixFE(1))->config()["Misc"]["CInjHi"])).value();
      } else if(capType==1){
	cinj2 = (dynamic_cast<ConfFloat &>((pmod->pixFE(1))->config()["Misc"]["CInjMed"])).value();
      }
    }
    if(delayConv){
      delayCal2 = ((ConfFloat&)pmod->pixFE(1)->config()["Misc"]["DelayCalib"]).value();
    }
  }

  std::ostringstream nam, tit;
  nam << scanname  <<  xmod    << "_" << 0;
  tit << scanname1 << " xmod " << xmod << " bin " << 0;

  if ( loopStep != -1) nam << "_"<< std::to_string(loopStep);

  //std::vector< Histo * > h;
  if (m_ctrlVerbose) std::cout << "nam = " << nam.str() << ", scanname = " << scanname << ", scanname1 = " << scanname1 << ", scanname2 = " << scanname2 << ", xmod = " << xmod << std::endl;
  

  //getting FitServer histograms from OH
  his[xmod].clear();
  //h.clear();
  ISInfoInt scanid;
  int sid = m_scanId;
  try {
    dict.getValue(srvname + "." + RODNAME + "_ScanId", scanid);
    sid=scanid;
  }
  catch (...) {
    std::cout<<"DEBUG::Exception caught while getting the scan ID"<<std::endl;
    std::cout<<"DEBUG::Using scanId="<<m_scanId<<std::endl;
  }
    
  
  int rx0 = getModuleRx(xmod).first;
  int rx1 = getModuleRx(xmod).second;

  //  std::cout<<"for module with id: "<<(int)xmod<<" got rx0 = "<<rx0<<" and rx1 = "<<rx1<<" and ROD: "<<RODNAME<<std::endl;

  std::string h1WildCard = "/" + boost::lexical_cast<std::string>(sid) + "/" + RODNAME + "/Rx" + boost::lexical_cast<std::string>(rx0) + "/" + scanname2 + ".*";
  std::string h2WildCard = "/" + boost::lexical_cast<std::string>(sid) + "/" + RODNAME + "/Rx" + boost::lexical_cast<std::string>(rx1) + "/" + scanname2 + ".*";

  //  std::cout<<"h1WildCard: "<<h1WildCard<<std::endl;

  OHHistogramIterator ith1(partition, srvname, ".*", h1WildCard);
  OHHistogramIterator ith2(partition, srvname, ".*", h2WildCard);
  //  std::vector< std::shared_ptr<Histo> > htmp;

  Histo *h_full = new Histo(nam.str(), tit.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
  std::map<int, Histo*> h_map;
  h_map[-1] = h_full;

  if (rx0 >= 0){
    while(ith1++) {
    try {

      std::cout<<"Hello Doug we are getting histo for Rx: "<<(int)rx0<<std::endl;
      std::string provName = ith1.provider();
      std::string histoName = ith1.name();
      if (m_ctrlVerbose) std::cout <<"histo to be read from OH ==>> "<< "srvname: " << srvname << "  provName: " << provName << "  histoName: " << histoName << std::endl;

      int histoLoop = -1;
      if (histoName.find("-")!=std::string::npos) {
	//int histoLoop = std::stoi(histoName.substr(histoName.length()-1,1));
	histoLoop = std::stoi(histoName.substr(histoName.find("-")+1, histoName.length()-histoName.find("-")-1));
	if (loopStep != -1 && histoLoop != loopStep) continue;
      }
      if(histoLoop!=-1){
	std::cout <<"this histo type was looped over - now at index " << histoLoop << std::endl;
	if(h_map.find(histoLoop)==h_map.end()){
	  std::ostringstream nam2, tit2;
	  nam2 << scanname  <<  xmod    << "_" << histoLoop;
	  tit2 << scanname1 << " xmod " << xmod << " bin " << histoLoop;
	  h_full = new Histo(nam2.str(), tit2.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
	  h_map[histoLoop] = h_full;
	  std::cout << "created new histo " << h_full->name() << std::endl;
	} else
	  h_full = h_map[histoLoop];
	std::cout << " -> filling histo " << h_full->name() << std::endl;
      } else
	h_full = h_map[histoLoop];
      
      OHRootHistogram hist=OHRootReceiver::getRootHistogram(partition, srvname, provName, histoName, -1);

      for(int xc=0;xc<nCol_Loop/2;xc++) {
	for(int xr=0;xr<nRow;xr++) {
	  if (chargeConv) {
	    float thr = getModGroup().getChargefromVCAL(hist.histogram->GetBinContent(xc+1,xr+1), vcal_a1, vcal_b1, cinj1);
	    if(hist.histogram->GetBinContent(xc+1,xr+1) < 0)  h_full->set(xc,xr,0); // fill 0 -> fit failed
	    else if(hist.histogram->GetBinContent(xc+1,xr+1) == 0) continue; // don't fill, pixel that wasn't scanned
	    else h_full->set(xc,xr,thr);
	    //	      std::cout<<" DEBUG: got threshold: "<<thr<<" e-"<<std::endl;
	  }
	  else if(delayConv)
	    h_full->set(xc,xr, delayCal1*hist.histogram->GetBinContent(xc+1,xr+1));
	  else h_full->set(xc,xr, hist.histogram->GetBinContent(xc+1,xr+1));
	}
      }
    } catch (const daq::oh::ObjectNotFound&) {
      //std::string mess = "Object not found in OH ";
      std::cout << "Object not found in OH " << std::endl;
    } catch (const daq::oh::InvalidObject &) {
      //std::string mess = "Invalid Object ";
      std::cout << " Invalid Obj " << std::endl;
    } catch (...) {
      //std::string mess = "Unknown exception thrown ";
    }
  }
  }
  if (rx1 >= 0) {
    while(ith2++) {

      std::cout<<"Hello Doug we are getting histo for Rx: "<<(int)rx1<<std::endl;

    try {
      std::string provName = ith2.provider();
      std::string histoName = ith2.name();

      int histoLoop = -1;
      if (histoName.find("-")!=std::string::npos) {
	//int histoLoop = std::stoi(histoName.substr(histoName.length()-1,1));
	histoLoop = std::stoi(histoName.substr(histoName.find("-")+1, histoName.length()-histoName.find("-")-1));
	if (loopStep != -1 && histoLoop != loopStep) continue;
      }
      if(histoLoop!=-1){
	std::cout <<"this histo type was looped over - now at index " << histoLoop << std::endl;
	if(h_map.find(histoLoop)==h_map.end()){
	  std::ostringstream nam2, tit2;
	  nam2 << scanname  <<  xmod    << "_" << histoLoop;
	  tit2 << scanname1 << " xmod " << xmod << " bin " << histoLoop;
	  h_full = new Histo(nam2.str(), tit2.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
	  h_map[histoLoop] = h_full;
	  std::cout << "created new histo " << h_full->name() << std::endl;
	} else
	  h_full = h_map[histoLoop];
	std::cout << " -> filling histo " << h_full->name() << std::endl;
      } else
	h_full = h_map[histoLoop];

      OHRootHistogram hist2=OHRootReceiver::getRootHistogram(partition, srvname, provName, histoName, -1);

      for(int xc=0;xc<nCol_Loop/2;xc++) {
	for(int xr=0;xr<nRow;xr++) {
	  if (chargeConv){
	    float thr = getModGroup().getChargefromVCAL(hist2.histogram->GetBinContent(xc+1,xr+1), vcal_a2, vcal_b2, cinj2);
	    if(hist2.histogram->GetBinContent(xc+1,xr+1) < 0)  h_full->set(xc+nCol/2,xr,0); // fill 0 -> fit failed
	    else if(hist2.histogram->GetBinContent(xc+1,xr+1) == 0) continue; // don't fill, pixel that wasn't scanned
	    else h_full->set(xc+nCol/2,xr,thr);
	    
	  }
	  else if(delayConv)
	    h_full->set(xc+nCol/2,xr, delayCal2*hist2.histogram->GetBinContent(xc+1,xr+1));
	  else h_full->set(xc+nCol/2,xr, hist2.histogram->GetBinContent(xc+1,xr+1));
	}
      }
    } catch (const daq::oh::ObjectNotFound &) {
      std::string msg = "In retrieving histograms, object not found in OH.";
      std::cout << msg << std::endl;
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    } catch (const daq::oh::InvalidObject &) {
      std::string msg = "In retrieving histograms, invalid object in OH.";
      std::cout << msg << std::endl;
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    } catch (...) {
      std::string msg = "Problem retrieving histograms in OH.";
      std::cout << msg << std::endl;
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    }
  }
  }

  // h.push_back(h_full);
  
  // for(unsigned int step=0; step<h.size(); step++){
  //   his[xmod].push_back(h[step]);}
  if(h_map.size()==1)
    his[xmod].push_back(h_map[-1]);
  else{
    int maxId = -1;
    for(std::map<int, Histo*>::iterator it=h_map.begin(); it!=h_map.end(); it++)
      if(it->first>maxId) maxId = it->first;
    maxId++;
    for(int step=0; step<maxId; step++){
      if(h_map.find(step)!=h_map.end())
	h_full = h_map[step];
      else{ // missing a histogram - fill default empty Histo
	std::ostringstream nam2, tit2;
	nam2 << scanname  <<  xmod    << "_" << step;
	tit2 << scanname1 << " xmod " << xmod << " bin " << step << " - EMPTY";
	h_full = new Histo(nam.str(), tit.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
      }
      his[xmod].push_back(h_full);
    }
    // remove default Histo, not used
    h_full = h_map[-1];
    h_map[-1] = 0;
    delete h_full;
  }

}

void PPCppRodPixController::setRunNumber(unsigned int runN) { //! Sets the run number
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  DataTakingTools cmd;
  cmd.action = SetRunNumber;
  cmd.value = runN;
  sendCommand(cmd);
  m_runNumber=runN;
}

void PPCppRodPixController::setEcrCount(unsigned int ecrC) { //! Sets the ECR counter
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  DataTakingTools cmd;
  cmd.action = SetECRCounter;
  cmd.value = ecrC;
  sendCommand(cmd);
}

unsigned int PPCppRodPixController::getEcrCount() { //! Read the ECR counter
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  DataTakingTools cmd;
  cmd.action = ReadECRCounter;
  sendCommand(cmd);
  return (cmd.result.value);

}


void PPCppRodPixController::writeRunConfig(PixRunConfig* run) { //! Sets the run configuration parameters
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  
  DataTakingConfig& cmd = m_dtConfig;

  // Putting the ROD in configuration mode
  setConfigurationMode();
  // Setting the ROD Offline ID
  std::string offlineId = (m_modGroup.rodConnectivity())->offlineId();
  uint32_t rodOfflineId = 0;
  std::istringstream str(offlineId);
  str >> std::hex >> rodOfflineId;
  m_runRxMask = getRxMaskFromModuleMask(m_modGroup.getActiveModuleMask());

  if (m_ctrlVerbose)   std::cout<<" disabled FE Run mask : 0x<<"<<std::hex<<m_disableFeMaskRun<<std::dec<<" for ROD: "<<getModGroup().getRodName()<<std::endl;
  if (m_ctrlVerbose)   std::cout<<" final run mask : 0x<<"<<std::hex<<(m_runRxMask & ~(m_disableFeMaskRun))<<std::dec<<" for ROD: "<<getModGroup().getRodName()<<std::endl;

  if (!run->internalGen() && !run->simActive()) {
  // Hard-coded values until we debug the BUSY issues

  // Not clear the reason of busy...not present anymore after TX fix
	  // LJ 22 May 2015 //  if (getModGroup().getRodName() == "ROD_I1_S7_ROD") m_runRxMask = 0xFFFFCFF0; // Disabling LI_S02_A_M1 as per Kerstin's request on Fri 03.04.2015 

  // DCS module (M3 C side) locked out
  //if (getModGroup().getRodName() == "ROD_I1_S7_ROD") m_runRxMask = 0xFF0FFFFF;

  // Debugging-Extra IDLEs --> goes busy at high rates
	  // LJ 22 May 2015  if (getModGroup().getRodName() == "ROD_I1_S8_ROD") m_runRxMask = 0xFFFFDFFF;
  //  if (getModGroup().getRodName() == "ROD_I1_S8_ROD") m_runRxMask = 0xFFFFFFF7; when no HV was on...

  // Extra IDLEs--> goes busy immediately
  // Adding Rx 28 to list because BOC reports extra frames + frame errors that seem correlated with EFB Busy 18 May 2015
	  // LJ 22 May 2015  if (getModGroup().getRodName() == "ROD_I1_S9_ROD") m_runRxMask = 0xEFFCFFFF;

  // Seems OK after TX fix--> temporary reexclude from the RUN(old fw test)
  //  if (getModGroup().getRodName() == "ROD_I1_S11_ROD") m_runRxMask = 0xFFFFFFFC; // Last change: 2014-IV-28 commented out by Karolos

  // Got BUSY (possibly due to the processor error)
	  // 22 May 2015  if (getModGroup().getRodName() == "ROD_I1_S12_ROD") m_runRxMask = 0xFFFF00FF; // Last change: 2014-IV-28 re-enabled by Karolos

  // Got BUSY (possibly due to the processor error) --> + RX0,1 temporary reexclude from the RUN(old fw test) 
  // if (getModGroup().getRodName() == "ROD_I1_S12_ROD") m_runRxMask = 0xFFFF00FC; // Last change: 2014-IV-28 commented out by Karolos

  // Seems OK after TX fix--> temporary reexclude from the RUN(old fw test)
  // if (getModGroup().getRodName() == "ROD_I1_S17_ROD") m_runRxMask = 0xFFFFFFFC; // Last change: 2014-IV-28 commented out by Karolos

  // Debugging-Extra IDLEs --> goes busy at high rates
	  // 22 May 2015  if (getModGroup().getRodName() == "ROD_I1_S18_ROD") m_runRxMask = 0xFFFFDFFF;

    //    m_runRxMask = 0xFFFC3FFF; // 12 July disable outermost 3D to reduce spikes in busy.... big HACK Laura and Kerstin

    //    if (getModGroup().getRodName() == "ROD_I1_S16_ROD") m_runRxMask = 0xFFFC11FF; // 11 July Laura and Kerstin Slot 16 went busy twice and this seems to keep it in...

    //    if (getModGroup().getRodName() == "ROD_I1_S6_ROD") m_runRxMask = 0xFFFDBFFF; // 12 July Laura and Kerstin keep 4 3Ds for monitoring beam
    //    if (getModGroup().getRodName() == "ROD_I1_S7_ROD") m_runRxMask = 0xFFFDBFFF; // 15 July Laura and Kerstin keep 4 3Ds for monitoring beam
    //    if (getModGroup().getRodName() == "ROD_I1_S15_ROD") m_runRxMask = 0xFFFDBFFF; // 12 July Laura and Kerstin keep 4 3Ds for monitoring beam

    //    if (getModGroup().getRodName() == "ROD_I1_S9_ROD") m_runRxMask = 0xFFFFFF00; // 23 July MBindi StaticDisableTests

    // DBM //updating DBM mask with 1 only module activated
    // if (getModGroup().getRodName() == "ROD_I1_S21_ROD") m_runRxMask = 0x06776077; // 2/3 DBM is 0x06776077
    //if (getModGroup().getRodName() == "ROD_I1_S21_ROD") m_runRxMask = 0x20027011;//m_runRxMask = 0x00020001; // DBM corresponding to M1_C2 C side
  
  }
  cmd.rodID = rodOfflineId;
  cmd.channels = (m_runRxMask & ~(m_disableFeMaskRun)); // enable / configure all channels
  cmd.physicsMode = true; // put the rod in physics mode
  cmd.fmtReadoutTimeoutLimit = 0x500; // PIT value = x500 ==> 1280 how long fmt links wait for data on a single channel (in 80 mhz clock)     ==> 640 BC
  cmd.fmtHeaderTrailerLimit =  0x700; // PIT value = 0x700 how many data+hit records to process per frame after which empty events are passed ==> 1792 words
  cmd.fmtRodBusyLimit =        0x776; // PIT value = 0x776 how full the fifo is before asserting busy (0x320 = 800 data-records)              ==> 1910 words
  cmd.fmtDataOverflowLimit =   0x320; // PIT value = 0x320 cut on the event size ?? very big..                                                ==> 800 hits 
  cmd.fmtTrailerTimeoutLimit = 0xf50; // PIT value = DEF value = 0xf50 ; I guess if no Trailer is identified the ROD close the event after a while...

  cmd.vetoAtECR =     0x1fa4; // 0x1fa4 = 820 microS --> Fast Command Veto time at ECR (100 ns time unit)
  cmd.busyAtECR = 0x2328;     // 0x2328 = 900  microS --> Busy time at ECR (100 ns time unit)  ==>  lowering it to 0 since CTP seems quite reliable; however monitoring of the counter needed
                              // needed for SR1 since there is only 500 microsecond window vetoed by LTP
  cmd.resetAfterECR = 0x1f40; // 0x1f40 = 800 microS --> Waiting time after ECR before the ROD internal FPGA ECR Reset is performed (100 ns time unit); the reset last 8 BC

  cmd.slinkMode = SlinkIgnoreFTK; // SlinkIgnoreFTK, SlinkEnableFTKXOff, SlinkEnableFTKLDown, SlinkEnableFTKBoth // Default mode if not specified in the Controller: SlinkIgnoreFTK
  cmd.frameCount = run->consecutiveLVL1_IBL(); // how many frames per trigger
  cmd.efbMaskValue = 0; // master reg. for disabled EFBs ** to-do: set this appropriately given FMT enable pattern

  // If the run configuration doesn't contain an override for IBL, use same value as consecutiveLVL1 (not IBL specific)
  if( cmd.frameCount == 255 ) {
    cmd.frameCount = run->consecutiveLVL1();
    std::string msg = "Using consecutive LVL1 from PIX (i.e. " + std::to_string((int)cmd.frameCount) + ") also for IBL  (since it's not overwritten by the config file parameter consecutiveLVL1_IBL)";
    ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
  } else std::cout << "Using IBL-specific consecutive LVL1 of " << cmd.frameCount << std::endl;
  cmd.trigLatency = (255-(run->latency())); 


  // Set ROD BCID offset
  cmd.bcidOffset = run->rodBcidOffset();

  // Reset ECR counter
  cmd.doECRReset = true; 
  sendCommand(cmd);

  //Set the BusyMask for Master and Slave based on the enablechannel mask (m_runRxMask & ~(m_disableFeMaskRun); 
  //If some FEs doesn't get enable during the EnableRx channel procedure, this won't be taken into account here 
  //since no feedback mechanism from the PPC to the Host was foreseen on the enableRxChannel procedure. 

  DataTakingTools cmd2;
  cmd2.action = SetRodBusyMask ;
  cmd2.channels = (m_runRxMask & ~(m_disableFeMaskRun));
  sendCommand(cmd2);

  ResetRodBusyHistograms cmd4;
  cmd4.dumpOnPPC = false;
  sendCommand(cmd4);

  m_serviceRecordRead = true; // read back service records the next time you config (which really should be unconfig!)

  m_runOccHisto = run->useHistogrammer();

}

void PPCppRodPixController::configureEventGenerator(PixRunConfig* run, bool active) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    //DEBUG: moved it from writeRunConfig to fix the module configuration issue.
    DataTakingConfig cmd;
    if (active) { // put ROD into simulation / internal generation mode
        cmd = m_dtConfig; // Not overwriting the local copy for this ...
        cmd.rolEmu = false;  // ROL emulator
        cmd.bocFeEmu = true; // BOC FE emulator (must be enabled for ROL emulator)
        cmd.initRealFE = false; // don't initialize real FE
        cmd.nHitsPerTrigger = run->simModOccupancy(); // how many hits per trigger for emu
        //cmd.channels = 0xFFFFFFFF;// enable all channels for emulator
    }
    else { // running with real detector
        cmd.initRealFE = true;
        cmd.rolEmu = false;
        cmd.bocFeEmu = false;
        // NOTE NOTE NOTE:: fei4 trig latency is actually 255 - latency
        // this is not true for fei3. Therefore, we take the complement of trig latency from run config
    }

	sendCommand(cmd);

}


void PPCppRodPixController::ActivateECRReconfiguration(uint32_t moduleMask, bool activate, uint32_t params) {

  RecoAtECR_manager RecoCmd;
  
    uint32_t rxMask = getRxMaskFromModuleMask(moduleMask);

    uint32_t pixMask = params & rxMask;//To avoid sendPixel in case that global is deactivated
    bool sendPixel = (pixMask != 0);

    std::cout<<getModGroup().getRodName()<<" ECR global active "<< activate<<" ECR pixelActive "<<sendPixel<<" with module mask 0x"<<std::hex<<moduleMask<<" with rxMask 0x"<<m_runRxMask<<" pixMask 0x"<<pixMask<<std::dec<<std::endl;

  if (activate) {
  
    //Configure the Reconfiguration at ECR thread
    RecoCmd.mode           = RecoAtECR::FULL_CONFIG;
    RecoCmd.m_recoConfig.verbosityLevel = 0;
    RecoCmd.m_recoConfig.pixRegCfg      = sendPixel; 
    RecoCmd.m_recoConfig.glbRegCfg      = true;
    RecoCmd.m_recoConfig.debug          = false;
    //RecoCmd.mask           = (m_runRxMask & (~m_disableFeMaskConfig));
    RecoCmd.m_recoConfig.mask           = rxMask;
    RecoCmd.m_recoConfig.pixmask        = pixMask;
    RecoCmd.m_recoConfig.readBackCheck  = true;
    RecoCmd.m_recoConfig.latchesMask    = 0x0; //10 latches. 0,1,2,3,4,5,9,10,11,12. HIT BUS always configured.
    RecoCmd.m_recoConfig.latchesRotation= 3;
    RecoCmd.m_recoConfig.customBS       = false;
    RecoCmd.m_recoConfig.allowReconfig  = false;
    RecoCmd.m_recoConfig.sleepDelay     = 100;

    sendCommand(RecoCmd);

    //RECO_ON_IDLE implies loading the FIFOs only once with global cfg
    //RECO_ON      implies loading the FIFOs on evry ECR
    if(pixMask)RecoCmd.mode = RecoAtECR::RECO_ON;
    else RecoCmd.mode = RecoAtECR::RECO_ON_IDLE;
    
    std::cout<<getModGroup().getRodName() << ": RecoAtECRCmd.mode = " << RecoCmd.mode << std::endl;
    sendCommand(RecoCmd);

  }
  
  else {
    std::cout<<getModGroup().getRodName()<<" ECR deactivated "<<std::endl;
    RecoCmd.mode = RecoAtECR::RECO_OFF;
    sendCommand(RecoCmd);
  }
    
  
						     
}

void PPCppRodPixController::startRun(PixRunConfig* run) { //! start run
  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  
   //Activate the ECR Reconfiguration
  bool sendConfigAtECR = run->sendConfigAtECR() && m_modGroup.preAmpsOn();
  
  uint32_t pixMask = run->getFei4ECRPixelMask();

  if(m_modGroup.isManager()->exists(m_modGroup.getISRoot()+"SEND_CONFIG_AT_ECR")) {
    PIX_INFO("Send config at ECR overwritten by IS SEND_CONFIG_AT_ECR");
    sendConfigAtECR = m_modGroup.isManager()->read<bool>(m_modGroup.getISRoot()+"SEND_CONFIG_AT_ECR") && m_modGroup.preAmpsOn();
  }

  if(getModGroup().isManager()->exists(getModGroup().getISRoot()+"RECO_ECR_PIX_MASK")){
    PIX_INFO("ECR PixMask overwritten by IS RECO_ECR_PIX_MASK");
    pixMask =getModGroup().isManager()->read<uint32_t>(getModGroup().getISRoot()+"RECO_ECR_PIX_MASK");
  }

  if(sendConfigAtECR)ActivateECRReconfiguration(m_modGroup.getActiveModuleMask(),true, pixMask);

  if(m_runOccHisto)setupHistogramming();

  // ntring argumment was not used in RodPixController!
  // to do: do we need to do anything here?
}

//Common to Pixel and IBL
void PPCppRodPixController::setupHistogramming(){

  // setting up IS stuff
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming"); // TODO: no hardocoding

  // define ROD name in format needed
  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

 ISInfoInt scanidinfo((int)m_runNumber);
 //The maskInfo is needed for the FitServer to be picked up. // Is this correct? Crosscheck with juanAn
 ISInfoInt maskInfo (m_runRxMask);
 // publishing startscan flag to signal FitServer it should start doing his shit.
 ISInfoString isgo(RODNAME);
 ISInfoInt feFlavour = (ISInfoInt)m_flavour;
  
  std::string FEflav = "";
  if(m_flavour == PixModule::PM_FE_I4A || m_flavour == PixModule::PM_FE_I4B) FEflav = "_I4";

    try {
      dict.checkin( srvname + "." + RODNAME + "_ScanId", scanidinfo );
      dict.checkin( srvname + "." + RODNAME + "_ScanModuleMask", maskInfo );
      dict.checkin( srvname + "." + RODNAME + "_StartScan", isgo );
      dict.checkin( srvname + "." + RODNAME + ".FEflavour", feFlavour);
      dict.checkin( srvname + "." + RODNAME + "_Scan_Repetitions", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_MaskTotalSteps", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_MaskSteps", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_ScurveMean_Fill", ISInfoBool(false));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_ScurveSigma_Fill", ISInfoBool(false));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_ScurveChi2_Fill", ISInfoBool(false));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_ToTMean_Fill", ISInfoBool(false));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_ToTSigma_Fill", ISInfoBool(false));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Hist_Occupancy_Fill", ISInfoBool(true));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Loop0_Steps", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Loop0_Min", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Loop0_Max", ISInfoInt(1));
      dict.checkin( srvname + "." + RODNAME + "_Scan_Loop0_DSPProcessing", ISInfoBool(false));
    }
    catch(...){
      std::string msg = "Failed check-in in Scan Info";
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    }

    ERS_LOG("Successfully checked Rod " << RODNAME << " into IS");

    ISInfoString s0;
    ISInfoString s1;
    ISInfoString s2;
    ISInfoString s3;
    try{
      dict.getValue(srvname + "." + RODNAME + "_0_0", s0);
      dict.getValue(srvname + "." + RODNAME + "_0_1", s1);
      dict.getValue(srvname + "." + RODNAME + "_1_0", s2);
      dict.getValue(srvname + "." + RODNAME + "_1_1", s3);
    }
    catch(...) {
      std::string msg = "Could not get scanId and/or fitServerPort from Histogramming IS,check that the fit server is running.";
      ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    }

  if (m_ctrlVerbose) std::cout<<" Got fit server ip:port combinations from IS: \n"
			    << (std::string)s0 << "\n" 
			    << (std::string)s1 << "\n" 
			    << (std::string)s2 << "\n" 
			    << (std::string)s3 << "\n" << std::endl;
  
  uint32_t fitPorts[4];
  uint8_t  fitIps[4][4];

    for (int i = 0; i<4; i++) {
      std::stringstream s;
      if(i == 0) s.str(s0);
      if(i == 1) s.str(s1);
      if(i == 2) s.str(s2);
      if(i == 3) s.str(s3);
      int a,b,c,d,e;
      char ch;
      s >> a >> ch >> b >> ch >> c >> ch >> d >> ch >> e;
      fitPorts[i] = e;
      fitIps[i][0] = a;
      fitIps[i][1] = b;
      fitIps[i][2] = c;
      fitIps[i][3] = d;
    }
    
    for (uint8_t iSlave = 0; iSlave < 2; iSlave++) {
      if(iSlave==0 && (m_runRxMask & 0xFFFF)==0)continue;
      if(iSlave==1 && (m_runRxMask & 0xFFFF0000) == 0)continue;

      SetNetwork setNetwork_cmd;
      setNetwork_cmd.slaveid = iSlave;
      setNetwork_cmd.usePpcDefaults = true; // use the default config for slaves IPs, subnet mask and gateway.
      setNetwork_cmd.ipA_1 = fitIps[2*iSlave][0];
      setNetwork_cmd.ipA_2 = fitIps[2*iSlave][1];
      setNetwork_cmd.ipA_3 = fitIps[2*iSlave][2];
      setNetwork_cmd.ipA_4 = fitIps[2*iSlave][3];
      setNetwork_cmd.portA = fitPorts[2*iSlave];

      setNetwork_cmd.ipB_1 = fitIps[2*iSlave+1][0];
      setNetwork_cmd.ipB_2 = fitIps[2*iSlave+1][1];
      setNetwork_cmd.ipB_3 = fitIps[2*iSlave+1][2];
      setNetwork_cmd.ipB_4 = fitIps[2*iSlave+1][3];
      setNetwork_cmd.portB = fitPorts[2*iSlave+1];

      sendCommand(setNetwork_cmd);
    }
   
   int nChips =0;
   if (m_flavour == PixModule::PM_FE_I4B)nChips =8;
   else if (m_flavour == PixModule::PM_FE_I2)nChips =128;
   
   IblSlvHistCfg histCfg;
   for (int slv=0;slv<2;slv++){
     if(slv==0 && (m_runRxMask & 0xFFFF)==0)continue;
     if(slv==1 && (m_runRxMask & 0xFFFF0000) == 0)continue;
   
     for (uint8_t iHist = 0; iHist<2; iHist++) {

       histCfg.cfg[iHist].nChips           = nChips;//Number of chips 8 for FE-I4 and 128 for FE-I3
       histCfg.cfg[iHist].type             = ONLINE_OCCUPANCY; //Histogram type OCCUPANCY
       histCfg.cfg[iHist].enable = 0;     //All histogram units disabled, enabled later on
       histCfg.cfg[iHist].maskStep      = 0;    //Number of mask steps = 0 --> No mask step
       histCfg.cfg[iHist].mStepEven     = 0;    //disable Even mask stepping
       histCfg.cfg[iHist].mStepOdd      = 0;    //disable Odd mask stepping
       histCfg.cfg[iHist].chipSel          = 1;    //Select all chips
       histCfg.cfg[iHist].expectedOccValue = 1;      //Not needed
       histCfg.cfg[iHist].addrRange        = 1;    //Address range in the histogram unit <1M --> Full range
       histCfg.cfg[iHist].scanId           = m_runNumber; //ScanID, or runNumber
       histCfg.cfg[iHist].binId            = 1;      //Bin ID, not used
       histCfg.cfg[iHist].maskId           = 0;      //maskID for the mask step, not used
     }
       if (slv == 0) {//Enable only histogram units used --> This has to match the run mask
         if ((m_runRxMask & 0x000000FF)) histCfg.cfg[0].enable = 1;//Enable histogram unit 0 slave 0
         if ((m_runRxMask & 0x0000FF00)) histCfg.cfg[1].enable = 1;//Enable histogram unit 1 slave 0
       } else if (slv == 1) {
         if ((m_runRxMask & 0x00FF0000)) histCfg.cfg[0].enable = 1;//Enable histogram unit 0 slave 1
         if ((m_runRxMask & 0xFF000000)) histCfg.cfg[1].enable = 1;//Enable histogram unit 1 slave 1
       }
     startHistogramming(slv, histCfg );
   }



}

void PPCppRodPixController::stopRun(PixHistoServerInterface *hsi) { //! Terminates a run
  
  if(m_runOccHisto){
    if(m_runRxMask & 0xFFFF)stopHistogramming(0);
    if(m_runRxMask & 0xFFFF0000)stopHistogramming(1);
    }

  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  // Stop ECR reconfiguration thread 
  ActivateECRReconfiguration(0xffffffff,false);
  // disable all Rx paths so no spurious data makes it to the rod
  DataTakingTools cmd;
  cmd.action = DisableRxPath;
  cmd.channels = 0xFFFFFFFF;
  sendCommand(cmd);

  // disable serial line so future triggers do not make it to this ROD
  DataTakingTools cmd2;
  cmd2.action = DisableSerialLine;

  // Put the rod in configuration mode
  setConfigurationMode();

  dumpVetoCounters();

  if(m_runOccHisto)getRunOccHisto(hsi);

}

void PPCppRodPixController::getRunOccHisto(PixHistoServerInterface *hsi){

  uint32_t L1Trigger = getTriggersReceived();
  uint32_t modMask = m_modGroup.getActiveModuleMask();
  std::stringstream folderstream;
  folderstream << "/R"<< std::setw(9) << std::setfill('0') << m_runNumber<<"/"<<m_modGroup.getName()<<"/";

  std::vector< std::vector<Histo*>> his;
    for (int i = 0; i < MAX_MODULES; i++)
      if( modMask & (0x1 << i) ){
        int slv = getSlaveFromModID(i);
          if(slv ==-1)continue;

        getHisto(OCCUPANCY, i, slv, his);
        std::string folder = folderstream.str()+m_modGroup.module(i)->connName()+"/OCCUPANCY/T"+std::to_string(L1Trigger)+"/";
        hsi->sendHisto(folder, *his[i][0]);
    }
    
    std::stringstream outstream;
    outstream << "/RUN_R"<< std::setw(9) << std::setfill('0') << m_runNumber;
    std::string outFileName = getenv("PIXSCAN_STORAGE_PATH");
    outFileName +="/"+outstream.str()+".root";
    
    std::cout<<"Folder name "<<folderstream.str()<<" output file: "<<outFileName<<std::endl;

    hsi->writeRootHistoServer(folderstream.str(), outFileName, false);

   m_runOccHisto = false;
}

uint32_t PPCppRodPixController::getTriggersReceived(){

    //get number of L1 Trigger from DF
    uint32_t L1Trigger = 0;
    std::string dt_partitionName = "";
    if( m_modGroup.isManager()->exists( m_modGroup.getISRoot()+"ALLOCATING-PARTITION" ) )
    dt_partitionName = m_modGroup.isManager()->read<std::string>( m_modGroup.getISRoot()+"ALLOCATING-PARTITION" );
    try {
      PixISManager *isDT = new PixISManager(dt_partitionName,"DF");
      std::cout<<"Events "<<isDT->read<unsigned long>("HLTSV.Events")<<std::endl;
      L1Trigger = isDT->read<unsigned long>("HLTSV.Events");
      delete isDT;
    }
    catch(...) {
      std::string msg = "Could not find L1 Trigger rate. L1 Trigger set to 0";
      std::cerr<<msg<<std::endl;
      ERS_INFO(msg);
      L1Trigger = 0;
    }

return L1Trigger;

}


void PPCppRodPixController::dumpVetoCounters(){

  ReadRodVetoCounters readVetoCounters;
  sendCommand(readVetoCounters);

  std::cout<<"Dumping ROD counters after stopping run "<<(int)m_runNumber <<" for ROD: "<<getModGroup().getRodName()<<std::endl;
  std::cout<<getModGroup().getRodName()<<" ECR VETO: "<<(int)readVetoCounters.result.ECR_Vetoed<<std::endl;
  std::cout<<getModGroup().getRodName()<<" BCR VETO: "<<(int)readVetoCounters.result.BCR_Vetoed<<std::endl;
  std::cout<<getModGroup().getRodName()<<" L1A VETO: "<<(int)readVetoCounters.result.L1A_Vetoed<<std::endl;
  std::cout<<getModGroup().getRodName()<<" ECR DROPPED: "<<(int)readVetoCounters.result.ECR_Dropped<<std::endl;
  std::cout<<getModGroup().getRodName()<<" BCR DROPPED: "<<(int)readVetoCounters.result.BCR_Dropped<<std::endl;
  std::cout<<getModGroup().getRodName()<<" L1A DROPPED: "<<(int)readVetoCounters.result.L1A_Dropped<<std::endl;

}
#if 0
void PPCppRodPixController::downloadDspHistos(PixHistoServerInterface *hsi) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

}
#endif
/*
  runStatus is called by PixThreadsScan::run()
  
*/
int PPCppRodPixController::runStatus() {              //! Check the status of the run
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (fakeScan) return (0x1 << SCAN_INIT)+(0x1 << SCAN_IN_PROGRESS) + (0x1 << SCAN_COMPLETE) + (0x1 << SCAN_TERMINATED ) ;

  if (m_scanActive) {
    this->nTrigger(); // update internal variables in nTrigger()
    if (m_ctrlVerbose) std::cout<<" scan state is: "<<m_scanResult.state<<std::endl;
    return (0x1<<m_scanResult.state); // return the bitwise mask corresponding to the state
  }
  return 0;
}

int PPCppRodPixController::nTrigger() {               //! Updates all scan status variables despite the name
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (fakeScan) return (((0) & 0xff) + (((8) & 0xff) << 8) + (((0) & 0xff) << 16) + (((0) & 0xff) << 24));
  if (m_scanActive) {
#ifdef __DO_IBL_GET_STATUS__
        GetStatus getStatus; // getStatus command
        sendCommand(getStatus);
        m_scanResult = getStatus.result.m_status;
#endif

    int ntrigger = (((m_scanResult.par) & 0xff) + (((m_scanResult.mask) & 0xff) << 8) + (((m_scanResult.par1) & 0xff) << 16) + (((m_scanResult.par2) & 0xff) << 24));

        if (m_ctrlVerbose) {
            std::cout << " at mask step: "         <<(int)m_scanResult.mask<<
                         ", at par 0 step: "       <<(int)m_scanResult.par<<
                         ", at par 1 step: "       <<(int)m_scanResult.par1<<
                         ", at par 2 step: "       <<(int)m_scanResult.par2<<
                         ", at DC step: "          <<(int)m_scanResult.dc<<
                         ", at trigger step: "     <<(int)m_scanResult.trig<<
                         ", with errorState: "     <<(int)m_scanResult.errorState<<std::endl;
            std::cout << "will return nTrigger = " << HEX(ntrigger) << std::endl;
        }


    if (m_scanResult.errorState != PPCERROR_NONE) {
      // m_errPublished flags prevent multiple consecutive messages from being printed.
      if (m_scanResult.errorState == NO_CONN_TO_FITSERVER && !m_errPublishedConn) {
	std::string msg = "ERROR: PPC REPORTED THAT THE SLAVE COULD NOT CONNECT TO THE FIT SERVER!!";
	ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
	std::cerr<< msg <<std::endl;
	std::cerr<<"Scan is continuing but you will not see any histos from at least one slave"<<std::endl;
	std::cerr<<"Check that both the slave and the fit server are running or restart both"<<std::endl;
	m_errPublishedConn = true;
      }
      else if (m_scanResult.errorState == SLAVE_NOT_RESPONDING && !m_errPublishedResponding) {
	std::string msg = "ERROR: PPC REPORTED THAT THE SLAVE TIMED OUT IN A COMMAND";
	ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
	std::cerr<< msg <<std::endl;
	std::cerr<<"The slave did not respond after 10 seconds to a command; perhaps you need to restart the slave"<<std::endl;
	m_errPublishedResponding = true;
      }
      else if (m_scanResult.errorState == NO_ENABLED_CHANNELS && !m_errPublishedChannels) {
	std::string msg = "ERROR: The PPC reported that no channels were successfully enabled for the scan.";
	ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
	std::cerr<< msg <<std::endl;
	std::cerr<<"The scan will continue, but you won't get any data! (and no connection to fit server will be attempted)"<<std::endl;
	std::cerr<<"Make sure the modules are powered on and that the optical connections are in a good state"<<std::endl;
	m_errPublishedChannels = true;
      }
    }
    return ntrigger;
  } 
  return 0;
}

void PPCppRodPixController::prepareBocScanDsp(PixScan *scn) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    SetDSPScan setOpto;
    setOpto.scanType = SetDSPScan::BOC_THR_DEL_DSP;
    sendCommand(setOpto);
}

void PPCppRodPixController::prepareMonLeakScan(PixScan *scn) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  
    SetDSPScan setMonLeak;
    setMonLeak.scanType = SetDSPScan::LEAK_SCAN_DSP;
    sendCommand(setMonLeak);
}
#if 0
void PPCppRodPixController::setConfigurationDebugMode() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  //===TO RE-IMPLEMENT
  if (m_mode != "CONFIGURATION_DBG") {
    m_mode = "CONFIGURATION_DBG";
  }
}
#endif

void PPCppRodPixController::startHistogramming(int slave, const IblSlvHistCfg &histCfg ) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  
    //Set Up the histogrammer configuration - This could actually be in the same comman
    // common/DspInterface/PIX/iblSlaveCmds.h -- Definition
      StartHisto StartHisto_cmd;
      StartHisto_cmd.whichSlave =slave;
      
      StartHisto_cmd.histConfig_in = histCfg;

      sendCommand(StartHisto_cmd);
}

void PPCppRodPixController::stopHistogramming(int slave) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  StopHisto StopHisto_cmd;
  StopHisto_cmd.whichSlave = slave;
  sendCommand(StopHisto_cmd);
}

unsigned int &PPCppRodPixController::fmtLinkMap(int fmt){
  //  printf("%s\n",__PRETTY_FUNCTION__);
  static unsigned int a;
  if (fmt >= 0 && fmt < 8) {
    return m_fmtLinkMap[fmt];
  }
  a = 0;
  return a;
}

void PPCppRodPixController::stopScan() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

  // LJ 5 June don't need to kill the scan and maybe it occasionally destructively interferes...
  // To-do: do we need to kill the scan for only stopping? doesn't hurt but is prob unnecessary
  //  KillCommand killScan(m_scanTypeId, m_scanExecId);
  //  sendCommand(killScan);
  // ND 16 Sept trying to improve by substituting use of KillScan with speccial AbortScan command
  m_scanActive = false;
}

void PPCppRodPixController::abortScan() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  std::cout<<"Abort scan requested!"<<std::endl;

  // first tell the ppc to stop the scan
  AbortScan abortScan(m_scanExecId, m_scanId);
  sendCommand(abortScan);

  // next tell the fit server the scan is finished
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming"); // TODO: no hardocoding   

  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

  ISInfoString isgo(RODNAME);
  try {
       dict.checkin( srvname + "." + RODNAME + "_AbortScan", isgo );
   } catch(...) {
       ERS_INFO("Dictionary checkin failed for Aborting Rod " << RODNAME);
   }
   ERS_LOG("Successfully checked Rod " << RODNAME << " into IS for Aborting");

  // lastly reset FPGA? Necessary? To-do: add if necessary
}
#if 0
void PPCppRodPixController::setGroupId(int module, int group) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  //===TO RE-IMPLEMENT
  unsigned int mod;
  if (module<0 || module>31) return;
  if (group<0 || group>7) return;
  if (m_modPosition[module] >= 0) {
    mod = m_modPosition[module];
    m_modGrp[mod] = group;
  }
}

void PPCppRodPixController::enableModuleReadout(int module) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  //===TO RE-IMPLEMENT
  std::cout << "!!==================   Not yet implemented " << std::endl;
  unsigned int mod;
  if (module<0 || module>31) return;
  if (m_modPosition[module] >= 0) {
    mod = m_modPosition[module];
    m_modActive[mod]  = true;
    m_modTrigger[mod] = true;
  }  
}

void PPCppRodPixController::disableModuleReadout(int module, bool trigger) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  //===TO RE-IMPLEMENT
  std::cout << "!!==================   Not yet implemented " << std::endl;
  unsigned int mod;
  if (module<0 || module>31) return;
  if (m_modPosition[module] >= 0) {
    mod = m_modPosition[module];
    m_modActive[mod]  = false;
    m_modTrigger[mod] = trigger;
  }
}
#endif
void PPCppRodPixController::initBoc(){
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
}

void PPCppRodPixController::resetBoc() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
}

bool PPCppRodPixController::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  unsigned int tmprev=revision;
  if (m_conf->write(DbServer, domainName, tag, getCtrlName(),"PPCppRodPixController", ptag, tmprev))
    return true;
  else {
    std::string msg = "PPCppRodPixController : ERROR in writing PPCppRodPixController config on DBserver";
    std::cout << msg << std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    return false;
  } 
}

bool PPCppRodPixController::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if(m_conf->read(DbServer, domainName, tag, getCtrlName(),revision))
    return true;
  else {
    std::string msg = "PPCppRodPixController : ERROR in reading PPCppRodPixController config from DBserver";
    std::cout << msg << std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
    return false;
  } 
}

uint32_t PPCppRodPixController::getRodSerialNumber() {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  return m_rod->getSerialNumber();

}

void PPCppRodPixController::printScanOptions(IblScanOptions& opts) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

}

void PPCppRodPixController::sendCommand(Command &cmd) const {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

  if (!m_rod->sendCommand(cmd)) {
     std::cout<<" Could not connect to server to talk to PPC"<<std::endl;
     
     std::string msg = "Could not connect to PPC server";
     PixLib::pix::daq issue(ERS_HERE, getModGroup().getRodName(), msg);
     ers::error(issue);
  }
}

int PPCppRodPixController::moduleOutLink(int nmod) {
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << " for: "<<getModGroup().getRodName() << " with nmod=" << nmod << std::endl;
      if (nmod >= 0 && nmod < MAX_MODULES) {
	if (m_ctrlVerbose) std::cout << "module is defined " << endl;
	if (m_modPosition[nmod]>=0) {
	  return m_moduleRx[nmod].first;
	}
      }
      return -1;
}

std::pair< int, int> PPCppRodPixController::getModuleRx(int modId) {
  //  if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  if (modId >= 0 && modId < MAX_MODULES) {
    if (m_moduleRx[modId].first >= 0 || m_moduleRx[modId].second >=0) {
      return m_moduleRx[modId];
    }
  }
  std::pair < int, int> emptyPair (-1,-1);
  return (emptyPair);
}

int PPCppRodPixController::getSlaveFromModID(int modId){

if (modId >= 0 && modId < MAX_MODULES) {
    if (m_moduleRx[modId].first >= 0) {
      return m_moduleRx[modId].first/16;
    }
  }
  std::string msg = "Cannot get slave ID form mod ID "+std::to_string(modId)+", check the code";
  ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), msg));
  return -1;

}

int PPCppRodPixController::getModuleIDFromRx(int rxCh) {
  //  if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  for (int modId = 0; modId < MAX_MODULES; modId++){
    if (m_moduleRx[modId].first == rxCh || m_moduleRx[modId].second == rxCh) {
      return modId;
    }
  }
  return -1;
}

int PPCppRodPixController::getFeNumberFromRx(int rxCh) {
  //  if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  for (int modId = 0; modId < MAX_MODULES; modId++){
    if (m_moduleRx[modId].first == rxCh) return 0;
    else if (m_moduleRx[modId].second == rxCh) return 1;
  }
  return -1;
}

template<class TConf>
bool PPCppRodPixController::compareRODHostHash(int channel, GetFEConfigHash::feFlavour fe_flavour, TConf const& config) const {
  GetFEConfigHash getHashCmd;
  getHashCmd.channel = channel;
  getHashCmd.configType  = GetFEConfigHash::Calib;
  getHashCmd.feFlav = fe_flavour;
  sendCommand(getHashCmd);

  auto currCfgHashPPC = getHashCmd.result.hash;
  auto currCfgHashHost = config.getHash();

  if (m_ctrlVerbose) { std::cout << "PPC hash: " << currCfgHashPPC << "\t  Host hash: " << currCfgHashHost << '\n'; }
  return (currCfgHashPPC == currCfgHashHost);
}

//ensure that the specified templated versions of the function are build
template bool PPCppRodPixController::compareRODHostHash<Fei4Cfg>(int, GetFEConfigHash::feFlavour, Fei4Cfg const&) const;
template bool PPCppRodPixController::compareRODHostHash<Fei3ModCfg>(int, GetFEConfigHash::feFlavour, Fei3ModCfg const&) const;

uint32_t PPCppRodPixController::getRxMaskFromModuleMask(uint32_t moduleMask) {

uint32_t rxMask = 0;
  for (int i = 0; i < MAX_MODULES; i++) {
    if (moduleMask & (0x1 << i)) {
      if (((m_moduleRx[i].first) >= 0)) {
	rxMask |= (0x1 << m_moduleRx[i].first);
      }
      if (((m_moduleRx[i].second) >= 0)) {
	rxMask |= (0x1 << m_moduleRx[i].second);
      }
    }
  }

if (m_ctrlVerbose) std::cout<<__PRETTY_FUNCTION__<<" received moduleMask : 0x<<"<<std::hex<<moduleMask<<" for ROD: "<<getModGroup().getRodName()<<" rx mask 0x"<<  rxMask<<std::dec<<std::endl;

return rxMask;

}

void PPCppRodPixController::outputMessageForConfig(SendModuleConfig &cmd) {

  uint32_t enabledMask = (cmd.result.enabledMask & ~(m_disableFeMaskDacs));
  uint32_t readBackOKMask = (cmd.result.readBackOKMask & ~(m_disableFeMaskDacs));
  uint32_t readBackBeforeSending = (cmd.result.readBackBeforeSending & ~(m_disableFeMaskDacs));

  std::stringstream msg_stream;
  uint32_t rxMask = getRxMaskFromModuleMask(m_modGroup.getActiveModuleMask());

  if (enabledMask == (rxMask & ~(m_disableFeMaskDacs))) {
      msg_stream<<"Successfully enabled FE Rx mask: 0x"<<std::hex<<(enabledMask)<<std::dec<<std::endl;
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
  }
  else {
    msg_stream<<"Could not succesfully enable FE Rx mask: 0x"<<std::hex<<(rxMask&~(m_disableFeMaskDacs))<<", could only enable mask: 0x"<<std::hex<<(enabledMask)<<std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
  }

  msg_stream.str("");

  if (readBackOKMask == (enabledMask)) {
      msg_stream<<"Successfully read back FE config for Rx mask: 0x"<<std::hex<<(enabledMask)<<std::dec<<std::endl;
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
    }
  else {
    msg_stream<<"Could not succesfully read back FE config for Rx mask: 0x"<<std::hex<<(enabledMask)<<", could only readback correctly for: 0x"<<std::hex<<(readBackOKMask)<<std::endl;
    ers::warning(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
  }

  msg_stream.str("");

    if (readBackBeforeSending == (enabledMask)) {
      msg_stream<<"Successfully read back before sending FE config for Rx mask: 0x"<<std::hex<<(enabledMask)<<std::dec<<std::endl;
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
    }
  else {
    msg_stream<<"Warning mistmatch of the read back FE config before sending for Rx mask: 0x"<<std::hex<<(enabledMask)<<", could only readback correctly for: 0x"<<std::hex<<(readBackBeforeSending)<<std::endl;
    ers::warning(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
  }

    // if reading service records
    if (m_serviceRecordRead) {
      GetServiceRecords srCmd;
      sendCommand(srCmd);
      std::string msg = "Reading out Service Records";
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
      for (int i = 0; i < 32; i++) {
	for (int j = 0; j < 32; j++) {
	  if (j == 9 || j == 14) continue; // mute SR 9 and 14 because they are not useful and always on 
	  uint16_t srValue = srCmd.result.srValues[i][j];
	  //if (j==16) srValue &= ~0x200; // for SR 16, "payload" is only bits 9:0 instead of 10:0 ... need to investigate when the TF flag seems to be always set high even if "payload" is 0
	  if ((srValue != 0 && srValue != 65535)) {
			std::stringstream ss;
			ss<<std::dec<<getModGroup().getRodName()<<", Rx: "<<i<<" has service record "<<(Fei4Data::m_serviceRecordMap.find(j)->second)<<" with payload: "<<srValue;
			if( j==16 ) ss << ", i.e. TF=" << ((srValue>>9)&0x1) << ", ETC=" << ((srValue>>4)&0x1F) << ", L1Req=" << (srValue&0xF);
			ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(), ss.str()));
		}
	  //std::cout<<getModGroup().getRodName()<<", Rx: "<<i<<" has service record "<<(Fei4Data::m_serviceRecordMap.find(j)->second)<<" with payload: "<<srValue<<std::endl; // tmp lj
	}
      }
    }
    else {
      std::string msg = "NOTE: Service Record values are readout and reset without being recorded";
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
    }
    m_serviceRecordRead = false; // don't read back the next time you config
}
