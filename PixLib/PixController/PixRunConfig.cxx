/////////////////////////////////////////////////////////////////////
// PixRunConfig.cxx
/////////////////////////////////////////////////////////////////////
//
// 07/07/11  Version 1.0 (MW)
//          Initial release

#include <sstream>
#include <math.h>
#include "PixController/PixRunConfig.h"

using namespace PixLib;

void RunConfig::subConfRead(std::map< std::string, std::string >fields, std::map<std::string, Config *>&subConf) {
  for (auto rec : fields) {
    if (rec.second == "PixRunConfig") {
      m_runConfig.addSubConf(rec.first);
    }
    //std::cout << rec.first << " - " << rec.second << std::endl;
  }
}

PixRunConfig::PixRunConfig(std::string name) {
  initConfig(name);
  m_cur = this;
}

PixRunConfig::~PixRunConfig() {
  std::map<std::string, PixRunConfig*>::iterator it;
  for (it = m_sub.begin(); it != m_sub.end(); it++) {
    delete it->second;
  }
  if (!m_conf->m_ownership) delete m_conf;
}

void PixRunConfig::initConfig(std::string name) {
  
  m_conf = new RunConfig(name, "PixRunConfig", *this); 
  Config &conf = *m_conf;
  
  conf.addGroup("RunParams");
 
  conf["RunParams"].addInt("internalGen", m_internalGen, 0, "Internal fragments generation", true);
  conf["RunParams"].addInt("consecutiveLVL1", m_consecutiveLVL1, 1, "ConsecutiveLVL1", true);
  // Default value set high on purpose to check if the parameter is set in the config
  conf["RunParams"].addInt("consecutiveLVL1_IBL", m_consecutiveLVL1_IBL, 255, "ConsecutiveLVL1_IBL", true);
  conf["RunParams"].addInt("latency", m_latency, 128, "Latency", true);
  conf["RunParams"].addInt("nPixInject", m_nPixInject, 0, "Number of strobed pixels per module", true);
  conf["RunParams"].addBool("digitalInject", m_digitalInject, false, "Digital injection", true);
  conf["RunParams"].addBool("cInjHi", m_cInjHi, false, "Use High injection cap", true);
  conf["RunParams"].addInt("vcal", m_vcal, 200, "VCAL value", true);
  // 14 Aug the following parameters added to strobe injections during datataking (noise tests) - GAO
  conf["RunParams"].addBool("injectMode",  m_injectMode, false,
			 "Enable the MCC registers to be set with CAL pulse parameters", true);  
  conf["RunParams"].addInt("mccCalDelay", m_mccCalDelay, 0,
			"MCC cal Delay", true);
  conf["RunParams"].addInt("rodCalDelay", m_rodCalDelay, 0,
			"ROD cal Delay", true);
  conf["RunParams"].addInt("mccCalWidth",  m_mccCalWidth, 0,
			"MCC CAL pulse lenght", true);
  conf["RunParams"].addInt("rodBcidOffset",  m_rodBcidOffset, 0,
			   "ROD Bcid offset", true);
 // 27 Aug parameters for injection during datataking - VD
  std::map<std::string, int> injTypeMap;
  injTypeMap["NOINJ"] = NOINJ;
  injTypeMap["RANDOM"] = RANDOM;
  injTypeMap["ROW"] = ROW;
  injTypeMap["COLUMN"] = COLUMN;
  conf["RunParams"].addList("injType", m_injType, NOINJ, injTypeMap,
			    "Injection Strategy", true);
  conf["RunParams"].addInt("injStep", m_injStep, 10, "Number of step in regular injection", true);
  conf["RunParams"].addInt("injStartPoint", m_injStartPoint, 0, "Starting point of regular injection", true);
  conf["RunParams"].addBool("RawDataMode", m_rawDataMode, false, "Enables RAW data mode during data taking", true);
  conf["RunParams"].addBool("StartPreAmpKilled", m_startPreAmpKilled, false, "Pre-Amp killed at run start", true);
  conf["RunParams"].addBool("StartPreAmpAuto", m_startPreAmpAuto, false, "Pre-Amp status according to LHC stable beam flag", true);
  conf["RunParams"].addInt("dataTakingSpeed", m_dtSpeed, 1u, "Data taking speed", true);


 // Group modGroups: From PixRunConfig class -> added readout enable for the 4 groups
  conf.addGroup("modGroups");
  conf["modGroups"].addBool("readoutEnable_0", m_readoutEnabled[0], true,
			    "Readout enable group 0", true);
  for (unsigned int i=1; i<MAX_GROUPS; i++) {
    std::ostringstream gnum;
    gnum << i;
    conf["modGroups"].addBool("readoutEnable_"+gnum.str(), m_readoutEnabled[i], false,
			      "Readout enable group "+gnum.str(), true);
  }
  
  // Group for ROD events simulation - added GAO 25 Sept 07
  conf.addGroup("EventSimulation");
  conf["EventSimulation"].addBool  ("SimulationEnabled", m_simActive, 0, "Enables or disables the writing of the register (ex. accessed by PixActionsSingleROD)", true);
  conf["EventSimulation"].addBool("SimulatorMode", m_simMode, 0, "Single/multipe simulator mode", true);
  conf["EventSimulation"].addInt("LVL1Accept", m_lVL1Accept, 0, "4-bit Number of LVL1 accept", true);
  conf["EventSimulation"].addInt("SkippedLVL1", m_skippedLVL1, 0, "4-bit Number of skipped LVL1", true);
  conf["EventSimulation"].addInt("BitFlipMask", m_bitFlipMask, 0, "3-bit Bit-flip header", true);
  conf["EventSimulation"].addInt("MCCFlags", m_mccFlags, 0, "8-bit MCC flags", true);
  conf["EventSimulation"].addInt("FEFlags", m_feFlags, 0, "8-bit FE flags", true);
  conf["EventSimulation"].addInt("FormattersEnaMask", m_formattersEnaMask, 255, "8-bit enable / disable generation in formatters", true);
  conf["EventSimulation"].addInt("ModuleOccupancy", m_moduleOccupancy, 0, "8-bit occupancy, from 0 to 255 hits per module", true);

  // Group for ROD monitoring
  conf.addGroup("RODMonitoring");
  conf["RODMonitoring"].addBool("AutoDisable", m_autoDisable, false, "Enables auto disable at run start", true);
  conf["RODMonitoring"].addBool("AutoRecover", m_autoRecover, false, "Enables automatic recovery during the run", true);
  conf["RODMonitoring"].addInt("RodMonPrescale", m_rodMonPrescale, 1u, "ROD Monitoring prescale factor", true);
  conf["RODMonitoring"].addInt("OccupancyRate", m_occupRate, 2u, "ROD Occupancy histograms sampling rate", true);
  conf["RODMonitoring"].addInt("BusyRate", m_busyRate, 1u, "ROD BUSY sampling rate", true);
  conf["RODMonitoring"].addInt("StatusRate", m_statusRate, 3u, "ROD status sampling rate", true);
  conf["RODMonitoring"].addInt("BuffRate", m_buffRate, 12u, "ROD buffers sampling rate", true);
  conf["RODMonitoring"].addInt("QuickStatusMode", m_quickStatusMode, 1u, "Determines QuickStatus mode of operation", true);
  conf["RODMonitoring"].addInt("QuickStatusConfig", m_quickStatusConfig, 2127062021u, "Determines QuickStatus operational parameters", true);
  conf["RODMonitoring"].addInt("ResyncConfig", m_syncConfig,1059302u,"Config auto MOD resynch.", true);
  conf["RODMonitoring"].addInt("ResyncConfigROD", m_syncConfigROD,1059302u,"Config auto ROD resynch.", true);
  conf["RODMonitoring"].addBool("UseHistogrammer", m_useHistogrammer, false, "Use ROD histogrammer during data taking (e.g. for noise mask).", true);
  // Group for sending config at ECR
  conf.addGroup("ConfigAtECR");
  conf["ConfigAtECR"].addBool("Enable", m_sendConfigAtECR, false, "Enable configuration at ECR, if enabled without other variables set, FE-I4 Global Config and FE-I3 SYNC-FE are sent", true);
  conf["ConfigAtECR"].addString("FEI4_SinglePixelReconfigMask", m_fei4ECRPixelMask, "0x0", "Enable mask to activate single FE-I4 pixel reconfiguration  --> 0xffffffff for entire ROD.", true);
  conf["ConfigAtECR"].addInt("FEI3_Mode", m_fei3ECRMode, 0u, "Mask to activate multiple operations at ECR [bit #0 for FEI3 soft reset; bit#1 to send MCCConfig; bit#2 to send FEI3GlobalConfig]", true);
  conf.reset();
}

std::vector<std::string> PixRunConfig::listSubConf() {
  std::vector<std::string> list;
  std::map<std::string, PixRunConfig*>::iterator it;
  for (it=m_sub.begin(); it!=m_sub.end(); it++) {
    list.push_back(it->first);
  }
  return list;
}

void PixRunConfig::addSubConf(std::string name) {
  std::map<std::string, PixRunConfig*>::iterator it=m_sub.find(name);
  if (it != m_sub.end()) {
    return;
  }
  m_sub.insert(std::make_pair(name, new PixRunConfig(name)));
  m_conf->addConfig(m_sub[name]->m_conf);
  //*(m_sub[name]->m_conf) = *(m_conf);
}

void PixRunConfig::addSubConf(PixRunConfig *runc) {
  std::string name = runc->getName();
  std::map<std::string, PixRunConfig*>::iterator it=m_sub.find(name);
  if (it != m_sub.end()) {
    return;
  }
  m_sub.insert(std::make_pair(name, runc));
  m_conf->addConfig(m_sub[name]->m_conf);
}

void PixRunConfig::removeSubConf(std::string name) {
  std::map<std::string, PixRunConfig*>::iterator it=m_sub.find(name);
  if (it == m_sub.end()) {
    return;
  }
  m_conf->removeConfig(name);
  delete m_sub[name];
  m_sub.erase(it);
}

void PixRunConfig::removeSubConf() {
  std::map<std::string, PixRunConfig*>::iterator it;
  for (it=m_sub.begin(); it!=m_sub.end(); it++) {
    m_conf->removeConfig(it->second->getName());
    delete m_sub[it->second->getName()];
  }
  m_sub.clear();
}

void PixRunConfig::selectSubConf(std::string name) {
  std::map<std::string, PixRunConfig*>::iterator it = m_sub.find(name);
  if (it != m_sub.end()) {
    m_cur = it->second;
    return;
  }
  m_cur = this;
}

void PixRunConfig::selectSubConf(std::string partition, std::string crate, std::string rod) {
  std::map<std::string, PixRunConfig*>::iterator it = m_sub.find(rod);
  if (it != m_sub.end()) {
    std::cout << "Found subconfig for rod: " << rod << std::endl;
    m_cur = it->second;
    return;
  }
  it = m_sub.find(crate);
  if (it != m_sub.end()) {
    std::cout << "Found subconfig for crate: " << crate << std::endl;
    m_cur = it->second;
    return;
  }
  it = m_sub.find(partition);
  if (it != m_sub.end()) {
    std::cout << "Found subconfig for partition: " << partition << std::endl;
    m_cur = it->second;
    return;
  }
  m_cur = this;
}

bool PixRunConfig::subConfExists(std::string name) {
  std::map<std::string, PixRunConfig*>::iterator it = m_sub.find(name);
  if (it != m_sub.end()) {
    return true;
  }
  return false;
}

void PixRunConfig::defineGroup(int group, unsigned int mask) {
  if (group>=0 && group<MAX_GROUPS) m_cur->m_groupMask[group] = mask;
  // Remove the enabled modules from other groups
  for (int i=0; i<MAX_GROUPS; i++) {
    if (i != group) {
      m_cur->m_groupMask[i] &= (~mask);
    }
  }
}

unsigned int PixRunConfig::getGroupMask(int group) {
if (group>=0 && group<MAX_GROUPS) return m_cur->m_groupMask[group];
return 0;
}

void PixRunConfig::enableReadout(int group) {
  if (group>=0 && group<MAX_GROUPS) m_cur->m_readoutEnabled[group] = true;
}

void PixRunConfig::disableReadout(int group) {
  if (group>=0 && group<MAX_GROUPS) m_cur->m_readoutEnabled[group] = false;
}

bool PixRunConfig::readoutEnabled(int group) {
  if (group>=0 && group<MAX_GROUPS) return m_cur->m_readoutEnabled[group];
  return false;
}

void PixRunConfig::writeConfig(PixConnectivity* conn, unsigned int rev) {// save current configuration into the database
  if (conn != NULL) {
    conn->writeRunConfig(this, m_conf->name());
  }
}

void PixRunConfig::writeConfig(DbRecord *r){ // save current configuration into the database
  if (r->getClassName() == "PixRunConfig") {
    // Write the config object
    m_conf->write(r); 
  }
}

bool  PixRunConfig::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
  std::string name = m_conf->name();
  if (!m_conf->write(DbServer, domainName, tag, name, "PixRunConfig", ptag, revision)) {
    std::cout << "PixRunConfig : ERROR in writing config on DBserver" << std::endl;
    return false;
  }
  return true;
}

void PixRunConfig::readConfig(DbRecord *r){ // read current configuration from the database
  if (r->getClassName() == "PixRunConfig") {
    // Read the config object
    m_conf->read(r); 
  }
}

void PixRunConfig::readConfig(PixConnectivity* conn, unsigned int rev){ // read current configuration from the database 
  if (conn != NULL) {
    conn->readRunConfig(this, m_conf->name());
  }
}

bool PixRunConfig::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
  std::string name = m_conf->name();
  if (!m_conf->read(DbServer, domainName, tag, name, revision)) {
    std::cout << "PixRunConfig : ERROR in reading config from DBserver" << std::endl;
    return false;
  } 
  return true;
}


