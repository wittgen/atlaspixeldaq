////////////////////////////////////////////////////////////////////
// PixScan.cxx
/////////////////////////////////////////////////////////////////////
//
// 17/06/05  Version 1.0 (PM)
//

// Still missing:
//  setting up of the rod scan structure before sending reference
//  presets
//  histogram downloads
//  histogram staging


#include "iblModuleConfigStructures.h"
#include "PixModule/PixModule.h"
#include "PixMcc/PixMcc.h"
#include "PixFe/PixFe.h"
#include "PixController/PixScan.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixUtilities/PixMessages.h"
#include "PixBoc/PixBoc.h"

using namespace PixLib;


PixScan::PixScan() {
  initConfig();
  resetScan();
  m_actualFE = -1;
  m_tmpGrp = NULL;
  m_tmpConn = NULL;
}

PixScan::PixScan(ScanType presetName, PixModule::FEflavour feflav) {                      // T: added new param (not being used now!)
  m_dbn = NULL;
  m_dbRecord = NULL;
  m_actualFE = -1;
  m_tmpGrp = NULL;
  m_tmpConn = NULL;
  initConfig();
  preset(presetName,feflav);
  resetScan();
}

PixScan::PixScan(DbRecord *dbr) : m_dbRecord(dbr) {
  m_dbn = m_dbRecord->getDb();
  m_actualFE = -1;
  m_tmpGrp = NULL;
  m_tmpConn = NULL;
  initConfig();
  readConfig(dbr);
  resetScan();
}

PixScan::PixScan(const PixScan &scn) {
  m_dbn = NULL;
  m_dbRecord = NULL;
  m_actualFE = -1;
  m_tmpGrp = NULL;
  m_tmpConn = NULL;
  initConfig();
  *m_conf = *(scn.m_conf);
  *m_info = *(scn.m_info);
  resetScan();
}

PixScan::~PixScan() {
  m_histo.clear();
  delete m_conf;
  delete m_info;
  if (m_tmpGrp != NULL && m_ownTmpConn) delete m_tmpGrp;
  if (m_tmpConn != NULL && m_ownTmpConn) delete m_tmpConn;
}

void PixScan::resetScan() {
  m_FECounter=0;
  m_actualFE = -1;
  // Reset indexes
  for (int i=0; i<MAX_LOOPS; i++) {
    m_loopIndex[i] = 0;
    m_loopTerminating[i] = false;
    m_loopEnded[i] = false;
  }
  m_maskStageIndex = 0;
  m_newScanStep = true;
  m_newFE = m_FEbyFE;
  if (m_dspProcessing[0]) m_newScanStep = false;
  if (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN) m_newMaskStage = false;
  // Reset histograms
  m_histo.clear();
}


void PixScan::initConfig() {
  // Initialize histogramTypes and dspHistogramTypes
  m_dspHistogramTypes=EnumDspHistogramTypeMap();
  m_histogramTypes=EnumHistogramTypeMap();
  m_scanTypes=EnumScanTypeMap();

  // Create the Info object
  m_info = new Config("ScanInfo");
  Config &info = *m_info;

  info.addGroup("Time");  
  info["Time"].addString("ScanStart", m_timeScanStart, "2000-01-01 00:00:00", "Scan start", true);
  info["Time"].addString("ScanEnd", m_timeScanEnd, "2000-01-01 00:00:00", "Scan end", true);
  info["Time"].addString("ScanHwriteStart", m_timeHwriteStart, "2000-01-01 00:00:00", "Histo write start", true);
  info["Time"].addString("ScanHwriteEnd", m_timeHwriteEnd, "2000-01-01 00:00:00", "Histo write end", true);
  info["Time"].addString("ScanInitEnd", m_timeInitEnd, "2000-01-01 00:00:00", "Config end", true);
  info["Time"].addInt("HistoDownloadTime", m_timeHdownload, -1, "Histo Download Time", true);
  info["Time"].addInt("Loop0Time", m_timeLoop0Total, -1, "Loop 0 Time", true);

  info.addGroup("Logs");  
  info["Logs"].addString("Comment", m_logsComment, "==", "Comment", true);
  info["Logs"].addString("ScanType", m_logsScanType, "==", "Scan type", true);
  info["Logs"].addInt("ScanId", m_logsScanId, 0, "Scan ID", true);
  info["Logs"].addString("RodInfo", m_logsRodInfo, "==", "ROD Info", true);
  info["Logs"].addString("RodName", m_rodName, "==", "PixModuleGroup name", true);

  info.addGroup("Tags");  
  info["Tags"].addString("ObjID", m_tagsObjID, "CT", "Object ID tag", true);
  info["Tags"].addString("Conn", m_tagsConn, "Base", "Conn tag", true);
  info["Tags"].addString("Data", m_tagsData, "Base", "Data tag", true);
  info["Tags"].addString("Alias", m_tagsAlias, "Base", "Alias tag", true);
  info["Tags"].addString("Cfg", m_tagsCfg, "Base", "Cfg tag", true);
  info["Tags"].addInt("CfgRev", m_tagsCfgRev, 0u, "Cfg revision", true);

  // Create the Config object
  m_conf = new Config("ScanConfig"); 
  Config &conf = *m_conf;

  // Insert the configuration parameters in the Config object
  std::map<std::string, int> tf;
  tf["FALSE"] = 0;
  tf["TRUE"] = 1;

  // Group general
  conf.addGroup("general");
  conf["general"].addList("scanType", m_scanTypeEnum,DIGITAL_TEST,m_scanTypes,"Scan Type",true);
  conf["general"].addInt("repetitions", m_repetitions, 200,
      "# of events per bin", true);
  conf["general"].addBool("scanFEbyFE", m_FEbyFE, false,
      "Perform a FEbyFE scan", true);
  conf["general"].addInt("scanFEbyFEMask", m_FEbyFEMask, 65535u, "Set the FE mask (all ON)", true);
  std::map<std::string, int> stageStepsMap= EnumMaskStageStepsMap();
  conf["general"].addList("maskStageTotalSteps", m_maskStageTotalSteps, STEPS_32, stageStepsMap, 
      "Total number of mask stages", true); 
  conf["general"].addInt("maskStageSteps", m_maskStageSteps, 3, 
      "Number of mask stages to actually execute in scan", true);
  conf["general"].addBool("tuningStartsFromConfigValues", m_tuningStartsFromConfigValues, false, 
      "TDAC and GDAC tunings starts from the values stored in the config instead of from initial values", true);
  conf["general"].addBool("useTuningPointsFile",m_useTuningPointsFile,false,"Load the module-wise tuning points from a txt files.",true);
  conf["general"].addString("tuningPointsFile",m_tuningPointsFile,"","path to the file where the tuning points are stored.",true);
  std::map<std::string, int> maskstgmodeMap=EnumMaskStageModeMap();
  conf["general"].addList("maskStageMode", m_maskStageMode, SEL_ENA, maskstgmodeMap,
      "Mask staging option i.e. which ctrl bits are staged", true); 
  conf["general"].addBool("restoreModuleConfig", m_restoreModuleConfig, true,
      "Original module configuration is restored at the end of the scan", true);
  conf["general"].addBool("useEmulator", m_useEmulator, false,
      "Use BOC emulator", true);
  conf["general"].addInt("nHitsEmu", m_nHitsEmu, 10,
      "Number of hits per frame while using BOC emulator", true);
  std::map<std::string, int> runType=EnumRunTypeMap();
  conf["general"].addList("runType", m_runType, NORMAL_SCAN, runType, 
      "Type of run to be executed", true); 
  std::vector<unsigned int> seedsDef;
  for (unsigned int ix=0; ix<32; ix++) {
    seedsDef.push_back(ix+(ix<<5)+(ix<<10)+(ix<<15)+(ix<<20));
  }
  conf["general"].addVector("patternSeeds", m_patternSeeds, seedsDef, 
      "seeds for the MCC pattern generation", true);
  conf["general"].addInt("useAltModLinks", m_useAltModLinks, 0u,
      "Use alternate module links if possible", true);
  conf["general"].addString("swapInLinks", m_swapInLinks, "-",
      "Swap in BOC links", true);
  conf["general"].addString("swapOutLinks", m_swapOutLinks, "-",
      "Swap out BOC links", true);
  conf["general"].addBool("cloneCfgTag",m_cloneCfgTag,false,"Write tuning result to cfg tag clone",true);
  conf["general"].addBool("cloneModCfgTag",m_cloneModCfgTag,false,"Write tuning result to modCfg tag clone",true);
  conf["general"].addString("tagSuffix",m_tagSuffix,"","Suffix for cloned cfg tag name",true);

  // Group histograms
  conf.addGroup("loops");

  std::map<std::string, int> scanvarMap=EnumScanParamMap(); 
  std::map<std::string, int> endactMap=EnumEndLoopActionMap();
  std::map<std::string, int> fitMethodMap=EnumScanFitMethodMap();
  conf["loops"].addBool("dspMaskStaging", m_dspMaskStaging, true, 
      "Mask staging executed by the DSP", true);
  conf["loops"].addBool("innerLoopSwap", m_innerLoopSwap, true, 
      "Mask staging is executed before Loop 0", true);
  conf["loops"].addList("fitMethod", m_fitMethod, FIT_LMMIN, fitMethodMap,
        "Fit method", true);
  for (unsigned int ll=0; ll<MAX_LOOPS; ll++) {
    std::ostringstream lnum;
    lnum << "Loop_" << ll;
    conf["loops"].addBool("active"+lnum.str(), m_loopActive[ll], false, 
        "Loop activation", true);
    conf["loops"].addBool("dspProcessing"+lnum.str(), m_dspProcessing[ll], false, 
        "Loop executed by the DSP", true);
    conf["loops"].addList("param"+lnum.str(), m_loopParam[ll], NO_PAR, scanvarMap,
        "Loop varaible", true);
    std::vector<float> defVal;
    conf["loops"].addVector("loopVarValues"+lnum.str(), m_loopVarValues[ll], defVal, 
        "Loop variable values", true);
    conf["loops"].addInt("loopVarNSteps"+lnum.str(), m_loopVarNSteps[ll], 0, 
        "Loop varaible step", true);
    conf["loops"].addFloat("loopVarMin"+lnum.str(), m_loopVarMin[ll], 0, 
        "Loop varaible min value", true);
    conf["loops"].addFloat("loopVarMax"+lnum.str(), m_loopVarMax[ll], 0, 
        "Loop varaible max value", true);
    conf["loops"].addBool("loopVarUniform"+lnum.str(), m_loopVarUniform[ll], true, 
        "Equally spaced loop variable points", true);
    conf["loops"].addBool("loopVarValuesFree"+lnum.str(), m_loopVarValuesFree[ll], false, 
        "Loop var valued dynamically calculated", true);
    conf["loops"].addList("endAction"+lnum.str(), m_loopAction[ll], NO_ACTION, endactMap,
        "End loop action", true);
    conf["loops"].addBool("dspAction"+lnum.str(), m_dspLoopAction[ll], true, 
        "End loop action executed by the DSP", true);
  }

  // Scan specific parameters
  conf.addGroup("scans");

  conf["scans"].addInt("thresholdTargedValue", m_thresholdTargetValue, 3000,
      "Threshold target value for TDAC tuning", true);
  conf["scans"].addInt("totTargedValue", m_totTargetValue, 30,
      "TOT target value for FDAC tuning", true);
  conf["scans"].addInt("totTargedCharge", m_totTargetCharge, 9000,
      "Reference charge for FDAC tuning", true);
  conf["scans"].addBool("fastThrUsePseudoPix", m_fastThrUsePseudoPix, false,
      "Fast threshold scan: determine VCAL pseudo-pixel-by-pixel", true);
  //conf["scans"].addBool("useGrpThrRange", m_useGrpThrRange, false,
  //    "Use group threshold range for regular threshold scan", true);

  // Group histograms
  conf.addGroup("histograms");
  for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
      itt != m_histogramTypes.end(); ++itt)                                 {
    std::string name = (*itt).first;
    int id = (*itt).second;
    if (name == "OCCUPANCY") {
      conf["histograms"].addBool("histogramFilled"+name, m_histogramFilled[id], true,
          "Fill histogram "+name, true);
      conf["histograms"].addBool("histogramKept"+name, m_histogramKept[id], true,
          "Keep histogram "+name, true);
    } else {
      conf["histograms"].addBool("histogramFilled"+name, m_histogramFilled[id], false,
          "Fill histogram "+name, true);
      conf["histograms"].addBool("histogramKept"+name, m_histogramKept[id], false,
          "Keep histogram "+name, true);
    }
  }

  // Group trigger        
  conf.addGroup("trigger");  
  conf["trigger"].addBool("self", m_selfTrigger, 0, 
      "Selects module self-triggering mode", true);
  conf["trigger"].addInt("LVL1Latency", m_LVL1Latency, 248, 
      "8-bit trigger latency as programmed on FEs", true);
  conf["trigger"].addBool("strobeLVL1DelayOverride", m_strobeLVL1DelayOverride, true,
      "Override ModuleGroup trigger delay", true);
  conf["trigger"].addInt("strobeLVL1Delay", m_strobeLVL1Delay, 240,
      "Sets the delay (in BCO units) between strover and LVL1", true);
  conf["trigger"].addInt("strobeDuration", m_strobeDuration, 50, 
      "Length of strobe in BCO units", true);
  conf["trigger"].addInt("strobeMCCDelay", m_strobeMCCDelay, 0,
      "Strobe delay setting on MCC", true);
  conf["trigger"].addBool("overrideStrobeMCCDelay", m_overrideStrobeMCCDelay, false,
      "Override strobe delay setting on MCC", true);
  conf["trigger"].addInt("strobeMCCDelayRange", m_strobeMCCDelayRange, 0,
      "Strobe delay range on MCC", true);
  conf["trigger"].addBool("overrideStrobeMCCDelayRange", m_overrideStrobeMCCDelayRange, false,
      "Override strobe delay range on MCC", true);
  conf["trigger"].addInt("superGroupTrigDelay", m_superGroupTrigDelay, -1,
      "Trigger delay between supergroups 0 and 1 (-1 means independent)", true);
  //Name with A_0 is from old readout, but left for compatibility
  conf["trigger"].addInt("consecutiveLevl1TrigA_0", m_consecutiveLvl1Trig, 16,
      "Number of LVL1 in block A for supergroup 0", true);

  // Group reset
  conf.addGroup("reset");  
  // To be defined

  // Group fe
  conf.addGroup("fe");  

  conf["fe"].addBool("chargeInjCapHigh", m_chargeInjCapHigh, false,
      "Use c-high injection capacitor", true);
  conf["fe"].addBool("digitalInjection", m_digitalInjection, true,
      "Select digital or analog injection", true);
  conf["fe"].addInt("vCal", m_feVCal, 0, 
      "Value for the VCAL DAC", true);
  conf["fe"].addInt("columnROFreq", m_columnROFreq, 2,
      "Column readout frequency", true);
  conf["fe"].addInt("totThrMode", m_totThrMode, 0,
      "Sets TOT threshold mode", true);
  conf["fe"].addInt("totDHThr", m_totDHThr, 32,  
      "Threshold for double-hit for correction", true);
  conf["fe"].addInt("totTimeStampMode", m_totTimeStampMode, 0, 
      "TOT or timestamp leading edge option", true);
  conf["fe"].addInt("totMin", m_totMin, 0,
      "TOT minimum", true);

  // Group mcc
  conf.addGroup("mcc");  

  std::map<std::string, int> mccbwMap=EnumMccBandwidthMap();
  conf["mcc"].addList("mccBandwidth", m_mccBandwidth, SINGLE_40, mccbwMap,
      "MCC output bandwidth", true);

  // Group modGroups
  conf.addGroup("modGroups");

  conf["modGroups"].addInt("moduleMask_0", m_moduleMask[0], 0xffffffff,
      "Modules in group 0", true);
  conf["modGroups"].addBool("configEnabled_0", m_configEnabled[0], true,
      "Config enable group 0", true);
  conf["modGroups"].addBool("strobeEnabled_0", m_strobeEnabled[0], true,
      "Strobe enable group 0", true);
  conf["modGroups"].addBool("triggerEnabled_0", m_triggerEnabled[0], true,
      "Trigger enable group 0", true);
  conf["modGroups"].addBool("readoutEnabled_0", m_readoutEnabled[0], true,
      "Readout enable group 0", true);
  for (unsigned int i=1; i<MAX_GROUPS; i++) {
    std::ostringstream gnum;
    gnum << i;
    std::string tit = "Threshold mod # "+gnum.str();
    conf["modGroups"].addInt("moduleMask_"+gnum.str(), m_moduleMask[i], 0xffffffff,
        "Modules in group "+gnum.str(), true);
    conf["modGroups"].addBool("configEnabled_"+gnum.str(), m_configEnabled[i], true,
        "Config enable group "+gnum.str(), true);
    conf["modGroups"].addBool("strobeEnabled_"+gnum.str(), m_strobeEnabled[i], true,
        "Strobe enable group "+gnum.str(), true);
    conf["modGroups"].addBool("triggerEnabled_"+gnum.str(), m_triggerEnabled[i], true,
        "Trigger enable group "+gnum.str(), true);
    conf["modGroups"].addBool("readoutEnabled_"+gnum.str(), m_readoutEnabled[i], true,
        "Readout enable group "+gnum.str(), true);
  }
  // Select default values
  conf.reset();

}

//! Scan control
bool PixScan::loop(int index) {
  if (index < MAX_LOOPS) {
    if (m_loopTerminating[index]) {
      m_loopTerminating[index] = false;
      m_loopEnded[index] = true;
      if (index == 0) {
        m_FECounter = 0;
        m_newScanStep = true;
        if (m_dspProcessing[0]) {
          m_newScanStep = false;
        }  if (m_dspMaskStaging || m_maskStageMode == STATIC || m_runType != NORMAL_SCAN) m_newMaskStage = false;
      }
      m_loopIndex[index] = 0;
      return false;
    } else {
      m_loopEnded[index] = false;
      return true;
    }
  }
  throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
  return false;
}

int PixScan::scanIndex(int index) {
  if (index < MAX_LOOPS) {
    return m_loopIndex[index];
  } 
  throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
  return 0;
}

void PixScan::next(int index) {
  bool noStaging = (m_runType != NORMAL_SCAN);
  //std::cout<<__PRETTY_FUNCTION__<<" Run type "<<(int)m_runType<<std::endl;

  if (index == 0) {
    if ((m_dspProcessing[0] || !m_loopActive[0]) && noStaging) {  // No loops in the host
      m_loopIndex[index] = m_loopVarNSteps[index] - 1;
      m_loopTerminating[0] = true;
      m_newScanStep = false;
    } else if (m_dspProcessing[0] || !m_loopActive[0]) { // Staging in the host
        m_loopTerminating[0] = true;
    } else if (noStaging) {  // Loop 0 in the host
      if (m_loopIndex[0] < m_loopVarNSteps[0] - 1) {
        m_loopIndex[0]++;
        m_newScanStep = true;
      } else {
        m_loopTerminating[0] = true;
      }
    } else { // Loop 0 and staging in the host
      if ( m_loopIndex[0] == (m_loopVarNSteps[0] - 1)) {
        m_loopTerminating[0] = true;
        m_newScanStep = false;
      } else {
        m_loopIndex[0]++;
        m_newScanStep = true;
      }
    }
  } else if (index < MAX_LOOPS) {
    if (m_dspProcessing[index] || !m_loopActive[index]) {
      m_loopIndex[index] = m_loopVarNSteps[index] - 1;
      m_loopTerminating[index] = true;
    } else {
      if ((m_loopIndex[index] < m_loopVarNSteps[index] - 1) || m_loopVarValuesFree[index]) {
        m_loopIndex[index]++;
      } else {
        m_loopTerminating[index] = true;
      }
    }
  } else {
    throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
  }
}


void PixScan::terminate(int index) {
  if (index > 0 && index < MAX_LOOPS) {
    if (m_loopVarValuesFree[index]) {
      m_loopTerminating[index] = true;
      m_loopVarNSteps[index] = m_loopIndex[index]+1;
    }
  } else {
    throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
  }
}

void PixScan::clearHisto(unsigned int mod, HistogramType type) {
  // Clear histograms
  if (m_histo.find(type) != m_histo.end()) {
    if (m_histo[type].exists(mod)) {
      m_histo[type][mod].clear();
    }
  }
}

void PixScan::prepareIndexes(HistogramType type, unsigned int mod, int ix2, int ix1, int ix0) {
  bool typeOk = false;
  for (auto const& hType: m_histogramTypes) {
    if (hType.second == type) {
      typeOk = true;
      break;
    }
  }

  if (!typeOk) {
    throw PixScanExc(PixControllerExc::ERROR, "Invalid histogram type");
  } 
  if (mod<0 || mod > 32) {
    throw PixScanExc(PixControllerExc::ERROR, "Invalid module number");
  }

  // Check if m_histo[type] is defined
  if (m_histo.find(type) == m_histo.end()) m_histo[type] = *(new PixScanHisto());
  // If ix2 is positive check if m_histo[type][mod] is defined as a list
  if (ix2 >= 0) {
    if (m_histo[type].exists(mod)) {
      if (m_histo[type][mod].histoMode()) {
        throw PixScanExc(PixControllerExc::ERROR, "Invalid loop 2 index");
      }
    } else {
      PixScanHisto *sh = new PixScanHisto();
      m_histo[type].add(mod, *sh);
    }
    // If ix1 is positive check if m_histo[type][mod][ix2] is defined as a list
    if (ix1 >= 0) {
      if (m_histo[type][mod].exists(ix2)) {
        if (m_histo[type][mod][ix2].histoMode()) {
          throw PixScanExc(PixControllerExc::ERROR, "Invalid loop 1 index");
        }
      } else {
        PixScanHisto *sh = new PixScanHisto();
        m_histo[type][mod].add(ix2, *sh);
      }
      // If ix0 is positive check if m_histo[type][mod][ix2] is defined as a list
      if (ix0 >= 0) {
          if (m_histo[type][mod][ix2].exists(ix1)) {
              if (m_histo[type][mod][ix2][ix1].histoMode()) {
                  throw PixScanExc(PixControllerExc::ERROR, "Invalid loop 0 index");
              }
          } else {
              PixScanHisto *sh = new PixScanHisto();
              m_histo[type][mod][ix2].add(ix1, *sh);
          }
      }
    }
  }
}

void PixScan::addHisto(PixScanHisto &psh, HistogramType type, unsigned int mod) {
    // Check if the list structure is in place and create the missing parts
    prepareIndexes(type, mod, -1, -1, -1);
    // Now add the PixScanHisto
    if (m_histo[type].exists(mod)) {
        throw PixScanExc(PixControllerExc::ERROR, "PixScanHisto already exists");
    } else {
        m_histo[type].add(mod, psh);
    }
}

void PixScan::addHisto(Histo &his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0) {
    // Check if the list structure is in place and create the missing parts
    prepareIndexes(type, mod, ix2, ix1, ix0);
    // Now add the histogram
    if (ix2 < 0) {
        if (m_histo[type].exists(mod)) {
          throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type].add(mod, *(new PixScanHisto(his)));
        }
    } else if (ix1 < 0) {
        if (m_histo[type][mod].exists(ix2)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod].add(ix2, *(new PixScanHisto(his)));
        }
    } else if (ix0 < 0) {
        if (m_histo[type][mod][ix2].exists(ix1)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod][ix2].add(ix1, *(new PixScanHisto(his)));
        }
    } else {
        if (m_histo[type][mod][ix2][ix1].exists(ix0)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod][ix2][ix1].add(ix0, *(new PixScanHisto(his)));
        }
    }
}

void PixScan::addHisto(Histo &his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0, int ih) {
    // Check if the list structure is in place and create the missing parts
    prepareIndexes(type, mod, ix2, ix1, ix0);
    // Now add the histogram
    if (ix2 < 0) {
        if (m_histo[type].exists(mod)) {
            if (m_histo[type][mod].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod].add(ih, his);
        } else {
            m_histo[type].add(mod, *(new PixScanHisto(ih, his)));
        }
    } else if (ix1 < 0) {
        if (m_histo[type][mod].exists(ix2)) {
            if (m_histo[type][mod][ix2].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2].add(ih, his);
        } else {
            m_histo[type][mod].add(ix2, *(new PixScanHisto(ih, his)));
        }
    } else if (ix0 < 0) {
        if (m_histo[type][mod][ix2].exists(ix1)) {
            if (m_histo[type][mod][ix2][ix1].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2][ix1].add(ih, his);
        } else {
            m_histo[type][mod][ix2].add(ix1, *(new PixScanHisto(ih, his)));
        }
    } else {
        if (m_histo[type][mod][ix2][ix1].exists(ix0)) {
            if (m_histo[type][mod][ix2][ix1][ix0].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2][ix1][ix0].add(ih, his);
        } else {
            m_histo[type][mod][ix2][ix1].add(ix0, *(new PixScanHisto(ih, his)));
        }
    }
}
void PixScan::addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0, int ih) {
    // Check if the list structure is in place and create the missing parts
    prepareIndexes(type, mod, ix2, ix1, ix0);
    // Now add the histogram
    if (ix2 < 0) {
        if (m_histo[type].exists(mod)) {
            if (m_histo[type][mod].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod].add(ih, his);
        } else {
            m_histo[type].add(mod, his);
        }
    } else if (ix1 < 0) {
        if (m_histo[type][mod].exists(ix2)) {
            if (m_histo[type][mod][ix2].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2].add(ih, his);
        } else {
            m_histo[type][mod].add(ix2, his);
        }
    } else if (ix0 < 0) {
        if (m_histo[type][mod][ix2].exists(ix1)) {
            if (m_histo[type][mod][ix2][ix1].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2][ix1].add(ih, his);
        } else {
            m_histo[type][mod][ix2].add(ix1,his);
        }
    } else {
        if (m_histo[type][mod][ix2][ix1].exists(ix0)) {
            if (m_histo[type][mod][ix2][ix1][ix0].histoExists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            }
            m_histo[type][mod][ix2][ix1][ix0].add(ih, his);
        } else {
            m_histo[type][mod][ix2][ix1].add(ix0, his);
        }
    }
}

void PixScan::addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0) {
    // Check if the list structure is in place and create the missing parts
    prepareIndexes(type, mod, ix2, ix1, ix0);
    // Now add the histogram
    if (ix2 < 0) {
        if (m_histo[type].exists(mod)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type].add(mod, his);
        }
    } else if (ix1 < 0) {
        if (m_histo[type][mod].exists(ix2)) {
           throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod].add(ix2, his);
        }
    } else if (ix0 < 0) {
        if (m_histo[type][mod][ix2].exists(ix1)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod][ix2].add(ix1, his);
        }
    } else {
        if (m_histo[type][mod][ix2][ix1].exists(ix0)) {
            throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
        } else {
            m_histo[type][mod][ix2][ix1].add(ix0, his);
        }
    }
}

void PixScan::addHisto(std::vector< Histo * >&his, HistogramType type, unsigned int mod, int ix2, int ix1) {

    for (unsigned int ih = 0; ih < his.size(); ih++) {
        // Check if the list structure is in place and create the missing parts
        if (ih == 0) {
            if (ix2 < 0) {
                prepareIndexes(type, mod, ih, -1, -1);
            } else if (ix1 < 0) {
                prepareIndexes(type, mod, ix2, ih, -1);
            } else {
                prepareIndexes(type, mod, ix2, ix1, ih);
            }
        }
        //std::cout<<__PRETTY_FUNCTION__<<" Adding histo with indexes "<<ix2<<" "<<ix1<<" "<<ih<<std::endl;
        // Now add the histograms
        if (ix2 < 0) {
            if (m_histo[type][mod].exists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            } else {
                m_histo[type][mod].add(ih, *(new PixScanHisto(*his[ih])));
            }
        } else if (ix1 < 0) {
            if (m_histo[type][mod][ix2].exists(ih)) {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            } else {
                m_histo[type][mod][ix2].add(ih, *(new PixScanHisto(*his[ih])));
            }
        } else {
            if (m_histo[type][mod][ix2][ix1].exists(ih)) {
               throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
            } else {
                m_histo[type][mod][ix2][ix1].add(ih, *(new PixScanHisto(*his[ih])));
            }
        }
    }
}

void PixScan::addHisto(std::vector< Histo * >&his, unsigned int nh, HistogramType type, unsigned int mod, int ix2, int ix1) {
    unsigned int nbin = his.size()/nh;
    for (unsigned int ib = 0; ib < nbin; ib++) {
        for (unsigned int ih = 0; ih < nh; ih++) { 
            // Check if the list structure is in place and create the missing parts
            if (ih == 0) {
                if (ix2 < 0) {
                    prepareIndexes(type, mod, ib, -1, -1);
                } else if (ix1 < 0) {
                    prepareIndexes(type, mod, ix2, ib, -1);
                } else {
                    prepareIndexes(type, mod, ix2, ix1, ib);
                }
                // Now add the histograms
                if (ix2 < 0) {
                    if (m_histo[type][mod].exists(ib)) {
                        throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                    } else {
                        m_histo[type][mod].add(ib, *(new PixScanHisto(0, *his[ib*nh])));
                    }
                } else if (ix1 < 0) {
                    if (m_histo[type][mod][ix2].exists(ib)) {
                        throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                    } else {
                        m_histo[type][mod][ix2].add(ib, *(new PixScanHisto(0, *his[ib*nh])));
                    }
                } else {
                    if (m_histo[type][mod][ix2][ix1].exists(ib)) {
                        throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                    } else {
                        m_histo[type][mod][ix2][ix1].add(ib, *(new PixScanHisto(0, *his[ib*nh])));
                    }
                }
            } else {
                if (ix2 < 0) {
                    m_histo[type][mod][ib].add(ih, *his[ib*nh+ih]);
                } else if (ix1 < 0) {
                    m_histo[type][mod][ix2][ib].add(ih, *his[ib*nh+ih]);
                } else {
                    m_histo[type][mod][ix2][ix1][ib].add(ih, *his[ib*nh+ih]);
                }
            }
        }
    }
}

void PixScan::addHisto(std::vector< Histo * >&his, unsigned int d2, unsigned int d1, HistogramType type, unsigned int mod, int ix2) {
    if (d1*d2 != his.size()) {
        throw PixScanExc(PixControllerExc::ERROR, "Number of histograms invalid");
    }
    unsigned int cnt = 0;
    for (unsigned int ih2 = 0; ih2 < d2; ih2++) { 
        for (unsigned int ih1 = 0; ih1 < d1; ih1++) { 
            // Check if the list structure is in place and create the missing parts
            if (ih1 == 0) {
                if (ix2 < 0) {
                    prepareIndexes(type, mod, ih2, ih1, -1);
                } else {
                    prepareIndexes(type, mod, ix2, ih2, ih1);
                }
            }
            // Now add the histograms
            if (ix2 < 0) {
                if (m_histo[type][mod][ih2].exists(ih1)) {
                    throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                } else {
                    m_histo[type][mod][ih2].add(ih1, *his[cnt]);
                }
            } else {
                if (m_histo[type][mod][ix2][ih2].exists(ih1)) {
                    throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                } else {
                    m_histo[type][mod][ix2][ih2].add(ih1, *(new PixScanHisto(*his[cnt])));
                }
            }
            cnt++;
        }
    }
}

void PixScan::addHisto(std::vector< Histo * >&his, unsigned int d3, unsigned int d2, unsigned int d1, HistogramType type, unsigned int mod) {
    if (d1*d2*d3 != his.size()) {
        throw PixScanExc(PixControllerExc::ERROR, "Number of histograms invalid");
    }
    unsigned int cnt = 0;
    for (unsigned int ih3 = 0; ih3 < d3; ih3++) {
        for (unsigned int ih2 = 0; ih2 < d2; ih2++) { 
            for (unsigned int ih1 = 0; ih1 < d1; ih1++) { 
                // Check if the list structure is in place and create the missing parts
                if (ih1 == 0) {
                    prepareIndexes(type, mod, ih3, ih2, ih1);
                }
                // Now add the histograms
                if (m_histo[type][mod][ih3][ih2].exists(ih1)) {
                  throw PixScanExc(PixControllerExc::ERROR, "Histogram already exists");
                } else {
                    m_histo[type][mod][ih3][ih2].add(ih1, *(new PixScanHisto(*his[cnt])));
                }
                cnt++;
            }
        }
    }
}

void PixScan::mergeFEHistos(Histo& histo, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0) {
    std::map<HistogramType, PixScanHisto>::iterator hist_iter=m_histo.find(type);
    if ( hist_iter == m_histo.end()
            || !hist_iter->second.exists(mod)
            || !hist_iter->second[mod].exists(ix2)
            || !hist_iter->second[mod][ix2].exists(ix1)
            || !m_histo[type][mod][ix2][ix1].exists(ix0) ) addHisto(histo, type, mod, ix2, ix1, ix0);  
    else{
        int colmod, rowmod;
        int ife=getFE();
        std::cout << "PixScan::mergeFEHistos. Doing histo on fe: " << ife << std::endl;
        for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
                if (ife < 8) {
                    rowmod = row;
                    colmod = col + 18*ife;
                } else {
                    rowmod = 319 - row;
                    colmod = 143 - col - 18*(ife-8);
                }
                double val = histo(colmod,rowmod);
                Histo &repositoryHisto = m_histo[type][mod][ix2][ix1][ix0].histo();
                repositoryHisto.set(colmod, rowmod, val);
            }
        }
        // }
        // else { 1d
        // }
}
// delete histo; 
}

void PixScan::mergeFEHistos(std::vector<Histo*> hVect, HistogramType type, unsigned int mod, int ix2, int ix1, int ix0) {

    std::map<HistogramType, PixScanHisto>::iterator hist_iter=m_histo.find(type);
    if ( hist_iter == m_histo.end()
            || !hist_iter->second.exists(mod)
            || !hist_iter->second[mod].exists(ix2)
            ||!m_histo[type][mod][ix2].exists(ix1) ) addHisto(hVect, type, mod, ix2, ix1, ix0);  
    else{
        int count=0;
        for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) {
            int colmod, rowmod;
            count++;
            int ife=getFE();
            std::cout << "PixScan::mergeFEHistos. Doing histo on fe: " << ife << std::endl; 
            for(int row=0;row<160;row++){
                for(int col=0;col<18;col++){
                    if (ife < 8) {
                        rowmod = row;
                        colmod = col + 18*ife;
                    } else {
                        rowmod = 319 - row;
                        colmod = 143 - col - 18*(ife-8);
                    }
                    Histo& tempHisto = *(*hVectIterator);
                    double val = tempHisto(colmod,rowmod);
                    Histo &repositoryHisto = m_histo[type][mod][ix2][ix1][ix0].histo(count);
                    repositoryHisto.set(colmod, rowmod, val);
                }
            }
            // }
            // else { 1d 
            // }
    }
} 
for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) delete (*hVectIterator);

}

void PixScan::mergeFEHistos(std::vector<Histo*> hVect, int consecLv1TrigA, HistogramType type, unsigned int mod, int ix2, int ix1) {
    std::map<HistogramType, PixScanHisto>::iterator hist_iter=m_histo.find(type);
    if (  hist_iter == m_histo.end()
            || !hist_iter->second.exists(consecLv1TrigA)
            || !hist_iter->second[consecLv1TrigA].exists(mod)
            || !hist_iter->second[consecLv1TrigA][mod].exists(ix2)
            ||!m_histo[type][consecLv1TrigA][mod][ix2].exists(ix1) ) addHisto(hVect, consecLv1TrigA, type, mod, ix2, ix1);  
    else{
        int count = 0;
        for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) {
            count++;
            int ife=getFE();
            std::cout << "PixScan::mergeFEHistos. Doing histo on fe: " << ife << std::endl; 
            int colmod, rowmod; // FIXME: what is meant here?
            for(int row=0;row<160;row++){
                for(int col=0;col<18;col++){
                    if (ife < 8) {
                        rowmod = row;
                        colmod = col + 18*ife;
                    } else {
                        rowmod = 319 - row;
                        colmod = 143 - col - 18*(ife-8);
                    }
                    Histo& tempHisto = *(*hVectIterator);
                    double val = tempHisto(colmod,rowmod);
                    Histo &repositoryHisto = m_histo[type][consecLv1TrigA][mod][ix2][ix1].histo(count);
                    repositoryHisto.set(colmod, rowmod, val);
                }
            }
            // }
            // else { 1d 
            // }
    }
}  
for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) delete (*hVectIterator);

}
void PixScan::mergeFEHistos(std::vector<Histo*> hVect, HistogramType type, unsigned int mod, int ix2, int ix1) {

    std::map<HistogramType, PixScanHisto>::iterator hist_iter=m_histo.find(type);
    if (  hist_iter == m_histo.end()
            || !hist_iter->second.exists(mod)
            || !hist_iter->second[mod].exists(ix2)
            || !m_histo[type][mod][ix2].exists(ix1) ) addHisto(hVect, type, mod, ix2, ix1);  
    else{
        int count = 0;
        for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) {
            count++;
      int colmod, rowmod;
      int ife=getFE(); 
      std::cout << "PixScan::mergeFEHistos. Doing histo on fe: " << ife << std::endl; 
      for(int row=0;row<160;row++){
        for(int col=0;col<18;col++){
          if (ife < 8) {
            rowmod = row;
            colmod = col + 18*ife;
          } else {
            rowmod = 319 - row;
            colmod = 143 - col - 18*(ife-8);
          }
          Histo& tempHisto = *(*hVectIterator);
          double val = tempHisto(colmod,rowmod);
          Histo &repositoryHisto = m_histo[type][mod][ix2][ix1].histo(count);
          repositoryHisto.set(colmod, rowmod, val);
        }
      }
      // }
      // else {// 1d
      // }
  }
}  
for( std::vector<Histo*>::iterator hVectIterator = hVect.begin(); hVectIterator != hVect.end(); ++hVectIterator) delete (*hVectIterator);
}

void PixScan::downloadHisto(PixController *ctrl, unsigned int mod, HistogramType type) {

  if(!m_histogramFilled[type]) {
    PIX_WARNING("No histograms filled found of type "<<type);
    return;
  }

  timeval start_time, end_time;
  int ix0 = m_loopIndex[0];
  int ix1 = m_loopIndex[1];
  int ix2 = m_loopIndex[2];
  int level = -1;

  gettimeofday(&start_time, 0);

  if (type == OCCUPANCY || type == TOT_MEAN || type == TOT_SIGMA || 
      type == SCURVE_MEAN || type == SCURVE_SIGMA || type == SCURVE_CHI2 || type == TIMEWALK ) {
    //Determine at which loop we are
    if (m_loopActive[2] && m_dspProcessing[2] && m_loopEnded[2]) {
      level = 0;
    } else if (m_loopActive[1] && m_dspProcessing[1] && m_loopEnded[1]) {
      level = 1;
    } else if (m_loopActive[0] && m_dspProcessing[0] && m_loopEnded[0]) {
      level = 2;
    } else if (type == OCCUPANCY || type == TOT_MEAN ||  type == TOT_SIGMA ) {
      level = 3;
    } else {
      //SCURVE_MEAN, SCURVE_SIGMA, SCURVE_CHI2, or TIMEWALK without a finished loop
      std::cout<<"Skipping download histo"<<std::endl;
      return;
    }
  } else if (type == MONLEAK_HISTO || type == RAW_DATA_DIFF_2 || type == RAW_DATA_REF) {
    level = 1;
  } else {
    PIX_ERROR("Not supported histogram type "<<(int)type);
    return;
  }

  // Check if the module is active
  int dsp = -1;
  bool active = false;
  for (size_t ndsp=0; ndsp<MAX_GROUPS; ndsp++) {
    if((m_moduleMask[ndsp] & (0x1<<mod)) && ctrl->moduleActive(mod)) {
      active = true;
      dsp = ndsp;
    }
  }

  if(!active) {
    return;
  }

  //std::cout<<"Histogram type "<<(int)type<<" level "<<level<<std::endl;

  bool numok = false;
  std::vector<std::vector<Histo*>> tmpHisto;

  if (m_scanTypeEnum == ScanType::TOT_CALIB) ctrl->getHisto((PixController::HistogramType)type, mod, dsp, tmpHisto, ix0);

  ctrl->getHisto((PixController::HistogramType)type, mod, dsp, tmpHisto);

  if (level == 0) {
      if ((int)tmpHisto[mod].size() == m_loopVarNSteps[0]*m_loopVarNSteps[1]*m_loopVarNSteps[2] || m_loopVarValuesFree[2]) {
        addHisto(tmpHisto[mod], m_loopVarNSteps[2], m_loopVarNSteps[1], m_loopVarNSteps[0], type, mod);  
        numok = true;
      }
  } else if (level == 1) {
    if (type == RAW_DATA_DIFF_2 || type == RAW_DATA_REF || type == MONLEAK_HISTO ) {
      if (!tmpHisto[mod].empty()) {
        if(type == RAW_DATA_REF) {
          addHisto(tmpHisto[mod], type, mod, ix2,ix1,-1);
	} else if(type == MONLEAK_HISTO) {
          addHisto(tmpHisto[mod], type, mod, ix2,ix1,ix0);
        } else {
          addHisto(tmpHisto[mod], type, mod, ix2,-1,-1);
	}
        numok = true;  
      }
    } else if (type == SCURVE_MEAN || type == SCURVE_SIGMA || type == SCURVE_CHI2 || type == TIMEWALK){
      addHisto(tmpHisto[mod], type, mod, ix2,-1,-1);
      numok = true;
    } else {
      if ((int)tmpHisto[mod].size() == m_loopVarNSteps[0]*m_loopVarNSteps[1] || m_loopVarValuesFree[1]) {
        addHisto(tmpHisto[mod], m_loopVarNSteps[1], m_loopVarNSteps[0], type, mod, ix2);  
        numok = true;
      }
    }
  } else if (level == 2) {
    if ( type == SCURVE_MEAN || type == SCURVE_SIGMA || type == SCURVE_CHI2 || type == TIMEWALK) {
        addHisto(tmpHisto[mod], type,  mod, ix2, ix1, -1);
        numok = true;
    } else {
      if ((int)tmpHisto[mod].size() == m_loopVarNSteps[0]) {
         std::cout<<__PRETTY_FUNCTION__<<"Adding histo with indexes "<<ix2<<" "<<ix1<<std::endl;
          addHisto(tmpHisto[mod], type, mod, ix2, ix1);
         numok = true;
     } else {
            PIX_ERROR ("MISSMATCH between: reserved space for histos =  " << (int)tmpHisto[mod].size() << " m_loopVarNSteps[0] = " << m_loopVarNSteps[0]);
     }
    }
  } else if (level == 3) {
    if (tmpHisto[mod].size() == 1) {
       addHisto(*tmpHisto[mod][0], type, mod, ix2, ix1, ix0);
       numok = true;
    }
  }
  if (!numok) throw PixScanExc(PixControllerExc::ERROR, "Wrong number of histograms downloaded");
  gettimeofday(&end_time, 0);
  m_timeHdownload += (end_time.tv_sec*1000000+end_time.tv_usec-start_time.tv_sec*1000000-start_time.tv_usec);

}

Histo& PixScan::getHisto(HistogramType type, PixModuleGroup *grp, int module, int idx2, int idx1, int idx0) {
  int nCol=144;
  int nRow=320;
  if(grp!=0){
    PixModule *pmod=grp->module(module);
    nCol=pmod->nCol();
    nRow=pmod->nRow();
  }
  static Histo tmp("Not Found","Not Found",nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
    try {
        Histo &h = m_histo[type][module][idx2][idx1][idx0].histo();
        return h;
    }
    catch (PixScanExc &ex) {
        tmp.clear();
        return tmp;
    }
    return tmp;
}

Histo& PixScan::getHisto(HistogramType type, PixModuleGroup *grp, int module, int idx2, int idx1, int idx0, int ih) {
  int nCol=144;
  int nRow=320;
  if(grp!=0){
    PixModule *pmod=grp->module(module);
    nCol=pmod->nCol();
    nRow=pmod->nRow();
  }
  static Histo tmp("Not Found","Not Found",nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
    try {
        Histo &h = m_histo[type][module][idx2][idx1][idx0].histo(ih);
        return h;
    }
    catch (PixScanExc &ex) {
        tmp.clear();
        return tmp;
    }
    return tmp;
}

PixScanHisto& PixScan::getHisto(HistogramType type) {
    bool typeOk = false;
    for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
            itt != m_histogramTypes.end(); ++itt)                                 {
        if ((*itt).second == type) typeOk = true;
    }
    if (!typeOk) {
        throw PixScanExc(PixControllerExc::ERROR, "Invalid histogram type");
    } 
    // Check if m_histo[type] is defined
    if (m_histo.find(type) == m_histo.end()) {
        throw PixScanExc(PixControllerExc::ERROR, "Histograms not found");
    } else {
        return m_histo[type];
    }
}

void PixScan::dumpFmtLinkscanRes(int i2, int i1, int i0, std::ostream &out) {
    dumpFmtLinkscanRes(i2, i1, i0, 0, out);
}

void PixScan::dumpFmtLinkscanRes(int i2, int i1, int i0, int ih, std::ostream &out) {

    out << "OL===Fmt0=Fmt1=Fmt2=Fmt3=Fmt4=Fmt5=Fmt6=Fmt7===";
    out << "Irx0==Irx3==Irx4==Irx7==Drx0==Drx1==Drx2==Drx3==";
    out << "Viset0=Iiset0=Viset1=Iiset1=Viset2=Iiset2=Viset3=Viset3=";
    out << endl;

    Histo &vh = getHisto(PixScan::FMTC_LINKMAP, 0, 0, i2, i1, i0, ih);

    for (unsigned int ol=0; ol<48; ol++) {
        if (ol==0 || ((ol%12)>1 && (ol%12)<10)) {
            // Dump on screen
            out << std::dec << std::setw(4) << ol << " ";
            for (int i=0; i<32; i++) {
                int val = (int)(0.5 + 10.0*vh(ol,i)/vh(ol,32));
                if (val == 0) {
                    out << "-";
                } else if (val == 10) {
                    out << "*";
                } else {
                    out << std::setw(1) << val;
                }
                if (i%4 == 3) out << " ";
            }
            out << " ";
            out << std::setw(5) << (int)(1000*vh(ol, 33+0)) << " ";
            out << std::setw(5) << (int)(1000*vh(ol, 33+3)) << " ";
            out << std::setw(5) << (int)(1000*vh(ol, 33+4)) << " ";
            out << std::setw(5) << (int)(1000*vh(ol, 33+7)) << " "; 
            out << std::setw(5) << (int)(1000*(vh(ol, 33+0)-vh(0, 33+0))) << " ";
            out << std::setw(5) << (int)(1000*(vh(ol, 33+3)-vh(0, 33+3))) << " ";
            out << std::setw(5) << (int)(1000*(vh(ol, 33+4)-vh(0, 33+4))) << " ";
            out << std::setw(5) << (int)(1000*(vh(ol, 33+7)-vh(0, 33+7))) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 48+0) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 49+0) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 48+7) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 49+7) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 48+14) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 49+14) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 48+21) << " ";
            out << std::fixed << std::setprecision(4) << vh(ol, 49+21) << " ";
            out << std::scientific << std::endl;
        }
    }
}

void PixScan::calcThr(PixController *ctrl, unsigned int mod, int ix2, int ix1) {
    if (mod<0 || mod > 32) {
        throw PixScanExc(PixControllerExc::ERROR, "Invalid module number");
    }
    // Check if the module is active
    bool active = false;
    for (unsigned int ndsp=0; ndsp<MAX_GROUPS; ndsp++) {
        if ((m_moduleMask[ndsp] & (0x1<<mod)) && ctrl->moduleActive(mod)) {
            active = true;
        }
    }
    if (active) {
        // Check if OCCUPANCY histos are present
        if (m_histo.find(OCCUPANCY) == m_histo.end()) {
            throw PixScanExc(PixControllerExc::ERROR, "OCCUPANCY histos not found");
        }
        double step = 1;
        double start = 0;
        int i1,i2;
        PixScanHisto *sc = NULL;
	bool delay = false;
        if (!m_histo[OCCUPANCY].exists(mod)) {
            throw PixScanExc(PixControllerExc::ERROR, "OCCUPANCY histos not found");
        } else {
            if (m_histo[OCCUPANCY][mod][0].histoMode()) {
                sc = &(m_histo[OCCUPANCY][mod]);
                if (m_loopVarNSteps[2] > 1) step = (m_loopVarMax[2]-m_loopVarMin[2])/(m_loopVarNSteps[2]-1);
                start = m_loopVarMin[2];
                i1 = i2 = -1;
		delay = getLoopParam(2) == STROBE_DELAY;
            } else if (m_histo[OCCUPANCY][mod][0][0].histoMode()) {
                sc = &(m_histo[OCCUPANCY][mod][ix2]);
                if (m_loopVarNSteps[1] > 1) step = (m_loopVarMax[1]-m_loopVarMin[1])/(m_loopVarNSteps[1]-1);
                start = m_loopVarMin[1];
                i1 = -1; 
                i2 = ix2;
		delay = getLoopParam(1) == STROBE_DELAY;
            } else if (m_histo[OCCUPANCY][mod][0][0][0].histoMode()) {
                sc = &(m_histo[OCCUPANCY][mod][ix2][ix1]);
                if (m_loopVarNSteps[0] > 1) step = (m_loopVarMax[0]-m_loopVarMin[0])/(m_loopVarNSteps[0]-1);
                start = m_loopVarMin[0];
                i1 = ix1; 
                i2 = ix2;
		delay = getLoopParam(0) == STROBE_DELAY;
            } else {
                throw PixScanExc(PixControllerExc::ERROR, "Histogram not found");
            }
        }
	PixModule *pmod=ctrl->getModGroup().module(mod);
	int nCol=pmod->nCol();
	int nRow=pmod->nRow();
	bool isFei4 = pmod->feFlavour()==PixModule::PM_FE_I4A || pmod->feFlavour()==PixModule::PM_FE_I4B;
        Histo *thr, *noise, *chi2;
        std::string nam, tit;
        std::ostringstream mnum;
        mnum << mod;
        nam = "Thr_" + mnum.str();
        tit = "Threshold mod " + mnum.str();
        thr = new Histo(nam, tit, nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
        nam = "Noise_" + mnum.str();
        tit = "Noise mod " + mnum.str();
        noise = new Histo(nam, tit, nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);
        nam = "Chi2_" + mnum.str();
        tit = "Chi2 mod " + mnum.str();
        chi2 = new Histo(nam, tit, nCol, -0.5, (float)nCol-0.5, nRow, -0.5, (float)nRow-0.5);

        fitSCurve(*sc, *thr, *noise, *chi2, 0, m_repetitions);

        for (int col = 0; col<nCol; col++) {
	  for (int row = 0; row<nRow; row++) {      
	    double th = (*thr)(col,row)*step+start;
	    double no = (*noise)(col,row)*step;
	    double c2 = (*chi2)(col,row);
	    PixCoord coord=pmod->geometry().coord(col,row);
	    int fe=coord.chip();
	    if(delay){
	      float conv = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["DelayCalib"])).value();
	      th *= conv;
	      no *= conv;
	    } else{
	      float vcalG0 = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["VcalGradient0"])).value();
	      float vcalG1 = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["VcalGradient1"])).value();
	      float vcalG2 = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["VcalGradient2"])).value();
	      float vcalG3 = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["VcalGradient3"])).value();
	      float cInj     = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["CInjLo"])).value();
	      if ((!isFei4 && getChargeInjCapHigh()) || (isFei4 && getMaskStageMode()==FEI4_ENA_BCAP)){
		cInj = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["CInjHi"])).value();
	      } else if(isFei4 && getMaskStageMode()==FEI4_ENA_LCAP){
		cInj = (dynamic_cast<ConfFloat &>(pmod->pixFE(fe)->config()["Misc"]["CInjMed"])).value();
	      }
	      th = (vcalG0 + th*vcalG1 + th*th*vcalG2 + th*th*th*vcalG3)*cInj/0.160218;
	      // noise must not use absolute calib. since it corresponds to difference
	      // -> calib. with threshold in higher orders and w/o the VCAL const.
	      no = (vcalG1 + th*vcalG2 + th*th*vcalG3)*no*cInj/0.160218;
	    }
	    // clip obviously nonsense results
	    if(th<0. || th>1.e5 || no<0. || c2<0.){
	      th = 0.;
	      no = 0.;
	    }
	    thr->set(col, row, th);
	    noise->set(col, row, no);
	    //std::cout << col << " " << row << " " << (*thr)(col,row) << " " << (*noise)(col,row) << " " << th << " " << no << std::endl;
	  }
        }
        addHisto(*thr, SCURVE_MEAN, mod, i2, i1, -1);
        addHisto(*noise, SCURVE_SIGMA, mod, i2, i1, -1);
        addHisto(*chi2, SCURVE_CHI2, mod, i2, i1, -1);  
        m_histogramFilled[SCURVE_MEAN] = true;
        m_histogramFilled[SCURVE_SIGMA] = true;
        m_histogramFilled[SCURVE_CHI2] = true;
    }
}

void PixScan::fitSCurve(PixScanHisto &sc, Histo &thr, Histo &noise, Histo& chi2, int ih, int rep) {   

    if (sc.histoMode()) {
        throw PixScanExc(PixControllerExc::ERROR, "Not in map mode");
    }
    if (!sc[0].histoMode()) {
        throw PixScanExc(PixControllerExc::ERROR, "Histogram not found");
    }
    if (sc.size() < 4) {
        throw PixScanExc(PixControllerExc::ERROR, "Not enough bins to fit");
    } 
    Histo **hsc = new Histo*[sc.size()];
    for (unsigned int i=0; i<sc.size(); i++) {
        hsc[i] = &sc[i].histo();
    }

    int nCol=thr.nBin(0);
    int nRow=thr.nBin(1);
   
    bool isFei4 = false;
    if (nRow == 336) isFei4 = true;

    for (int col = 0; col<nCol; col++) {
      for (int row = 0; row<nRow; row++) {
	bool low = true; 
	bool top = false;
	bool bot = false;
	if (isFei4) bot = true; // needed for Fei4 discbias scan
	bool fl = false;
	bool fh = false;
	unsigned int i;
	double ymax = rep;
	double xmax = sc.size();
	double xl=0,yl=0,xh=xmax,yh=ymax;
	for (i=1; i<sc.size()-1; i++) {
	  double his = (*hsc[i])(col,row);
	  double hisp= (*hsc[i-1])(col,row);
	  double hisn= (*hsc[i+1])(col,row);
	  if (low) {
	    if (his<0.1*ymax && hisn<0.1*ymax) bot =true; 
	    if (his > ymax/2) low = false;
	    if (!fl && his>0.18*ymax && hisn>0.18*ymax) {
	      fl = true;
	      xl = i;
	      yl = (hisp+his+hisn)/3.0;
	    }
	  } else {
	    if (his>0.9*ymax && hisn>0.9*ymax) top =true;
	    if (!fh && his>0.82*ymax && hisn>0.82*ymax) {
	      fh = true;
	      xh = i;
	      yh = (hisp+his+hisn)/3.0;
	    } 
	  }
	  if (top & bot) {
	    double a = (yh-yl)/(xh-xl);
	    double b = yl - a*xl;
	    if (a > 0) {
	      double threshold = (0.5*ymax/a - b/a);
	      double sigma = (0.3413*ymax/a);
	      double c2 = 0;
	      for (i=(unsigned int)xl; i<(unsigned int)xh+1; i++) {
		if(ymax!=0){
		  double ycurr = his;
		  double yerr  = sqrt(ycurr/ymax*(ymax-ycurr));
		  if(yerr!=0) {
		    c2 += pow((ycurr-b-a*(double)i)/yerr,2);
		  }
		}   
	      }
	      thr.set(col,row,threshold);
	      noise.set(col,row,sigma);  
	      chi2.set(col,row,c2);
	    } else {
	      thr.set(col,row,0.0);
	      noise.set(col,row,0.0);  
	      chi2.set(col,row,-2.0);
	    }
	    break;
	  }
	}
	if(!top || !bot){
	  thr.set(col,row,0.0);
	  noise.set(col,row,0.0);  
	  chi2.set(col,row,-3.0);
	}
      }
    }
}

void PixScan::readConfig(DbRecord *dbr){  // load configuration from the database
  m_dbRecord = dbr;
  m_conf->read(m_dbRecord);
}

void PixScan::writeConfig(DbRecord *dbr){  // save current configuration into the database
  m_dbRecord = dbr;
  m_conf->write(m_dbRecord);
}

void PixScan::writeHisto(DbRecord *dbr, PixModuleGroup *grp){
  printf("%s\n",__PRETTY_FUNCTION__);
  m_timeHwriteStart = getTime();
  for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
      itt != m_histogramTypes.end(); ++itt) {
    HistogramType type = (HistogramType)((*itt).second);
    if (!getHistogramKept(type)) continue;
    std::string typeName = (*itt).first;
    if ((m_histo.find(type)) != m_histo.end()) {
      DbRecord* typeR = dbr->addRecord("PixScan_Histo", typeName);
      // PixScanHisto contains Pixscanhisto, do the loop on ModId
      for (int mod=0; mod<31; mod++) {
        if (m_histo[type].exists(mod)) {
          ModuleConnectivity * mod_conn =grp->modConn(mod);
          std::string modName ;
          if (mod_conn != NULL) {
            modName = mod_conn->name();
          } else {
            std::stringstream a_name;
            a_name << "Mod" <<mod;
            modName= a_name.str();
          }
          DbRecord* modR = typeR->addRecord("PixScanHisto", modName);
          std::cout << "modName is " << modName << std::endl;
          m_histo[type][mod].write(modR);
          delete modR;
        }
      }
    }
  }

m_logsRodInfo = grp->getRodStat();

  
  if (grp->pixConnectivity()!=NULL){
    m_tagsObjID = grp->pixConnectivity()->getConnTagOI();
    m_tagsConn = grp->pixConnectivity()->getConnTagC();
    m_tagsAlias = grp->pixConnectivity()->getConnTagA();
    m_tagsData = grp->pixConnectivity()->getConnTagP();
    m_tagsCfg = grp->pixConnectivity()->getCfgTag();
    //m_tagsEna;
    m_tagsCfgRev = grp->pixConnectivity()->getCfgRev();
    if (m_tagsCfgRev == 0xffffffff) m_tagsCfgRev = time(NULL);
  }
  m_rodName = grp->getName();
  DbRecord* infoR = dbr->addRecord("PixScan_Info", "ScanInfo");
  m_timeHwriteEnd = getTime();
  m_info->write(infoR); 
}

void PixScan::deleteHisto(HistogramType type, int module, int idx2, int idx1, int idx0, int ih)
{
    for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
      itt != m_histogramTypes.end(); ++itt) {
    HistogramType model = (HistogramType)((*itt).second);
    if (!getHistogramKept(model)) continue;
    if (model == type) {
      m_histo[type][module].deleteSingleHisto(idx2, idx1, idx0, ih);
    }
  }
}

// HistogramServer Interface
void PixScan::writeHisto(PixHistoServerInterface *hsi, PixModuleGroup *grp, std::string folder){
  printf("%s\n",__PRETTY_FUNCTION__);
  std::cout << "Begin of  PixScan::writeHisto" <<std::endl;
  m_timeHwriteStart = getTime();
  for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
      itt != m_histogramTypes.end(); ++itt) {
    HistogramType type = (HistogramType)((*itt).second);
    if (!getHistogramKept(type)) continue;
    std::string typeName = (*itt).first;
    if ((m_histo.find(type)) != m_histo.end()) {
      // PixScanHisto contains Pixscanhisto, do the loop on ModId
      for (int mod=0; mod<31; mod++) {
        if (m_histo[type].exists(mod)) {
          if (type == FMTC_LINKMAP || type == INLINKMAP) {
            std::string newFolder = folder+typeName+"/";
            std::cout << "folder: " << newFolder << std::endl;
            m_histo[type][mod].writeHistoServer(hsi, newFolder, 0);
          } else {
            ModuleConnectivity *mod_conn = grp->modConn(mod);
            std::string modName ;
            if (mod_conn != NULL) {
              modName = mod_conn->name();
            } else {
              std::stringstream a_name;
              a_name << "Mod" <<mod;
              modName= a_name.str();
            }
            std::string newFolder = folder+modName+"/";
            std::cout << "folder with module: " << newFolder << std::endl;
            newFolder += typeName+"/"; 
            std::cout << "folder: " << newFolder << std::endl;  
            m_histo[type][mod].writeHistoServer(hsi, newFolder, 0);
          } //FMTC_LINKMAP
        }
      } 
    }
  }

m_logsRodInfo = grp->getRodStat();


  if (grp->pixConnectivity()!=NULL){
    m_tagsConn = grp->pixConnectivity()->getConnTagC();
    m_tagsAlias = grp->pixConnectivity()->getConnTagA();
    m_tagsData = grp->pixConnectivity()->getConnTagP();
    m_tagsCfg = grp->pixConnectivity()->getCfgTag();
    //m_tagsEna;
    m_tagsCfgRev = grp->pixConnectivity()->getCfgRev();
    if (m_tagsCfgRev == 0xffffffff) m_tagsCfgRev = time(NULL);
  }
  m_rodName = grp->getName();
  m_timeHwriteEnd = getTime();
}


// New Nicoletta
void PixScan::writeHisto(PixHistoServerInterface *hsi, PixModuleGroup *grp, std::string folder, HistogramType type, int module, int idx2, int idx1, int idx0, int ih)
{
    std::cout<<__PRETTY_FUNCTION__<<" "<<idx2<<" "<<idx1<<" "<<idx0<<" "<<ih<<std::endl;
    for (std::map<std::string, int>::iterator itt = m_histogramTypes.begin(); 
            itt != m_histogramTypes.end(); ++itt) {
        HistogramType model = (HistogramType)((*itt).second);
        if (!getHistogramKept(model)) continue;
        if (model == type) {
            ModuleConnectivity *mod_conn = grp->modConn(module);
            std::cout << "modcon " << mod_conn << std::endl;
            std::string modName = mod_conn->name();
            std::string typeName = (*itt).first;

            std::string newFolder = folder+modName+"/"+typeName+"/";
            std::stringstream finalName;
            finalName << newFolder << "A" << idx2 << "/";
            if (idx1 != -1){
                finalName << "B" << idx1 << "/"; 
                if (idx0 != -1){
                    finalName << "C" << idx0 << "/" << ih << ":";
                } else {
                    finalName << ih << ":";
                }
            } else {
                finalName << ih << ":";
            }
            std::cout << "PixScan, folder " << finalName.str() << std::endl;
            m_histo[type][module].writeSingleHisto(hsi, finalName.str(), idx2, idx1, idx0, ih);
            std::cout <<"PixScn, end call here"<< std::endl;
        }
    }
}



// --------------------------------------------------------




void PixScan::readInfo(DbRecord* dbr) {
    // Loop on record
    for (dbRecordIterator it=dbr->recordBegin(); it!=dbr->recordEnd(); it++) {
        std::string recName = (*it)->getName();
        if (recName == "ScanInfo") {
            m_info->read(*it);
        }
    }  
}

PixModuleGroup* PixScan::getTmpGrp() {
    if (m_tmpGrp == NULL) {
        // Load the configuration tag/rev
        m_tmpConn->setCfgTag(m_tagsCfg);
        m_tmpConn->setCfgRev(m_tagsCfgRev);
        // Look for the rod/boc in connectivity
        //PixModuleGroup *grp=NULL;
        if (m_tmpConn->rods.find(m_rodName) != m_tmpConn->rods.end()) {
            //grp = m_tmpGrp = m_tmpConn->getModGroup(m_rodName);
    }
  }
  return m_tmpGrp;
}

void PixScan::readHisto(DbRecord* dbr, bool loadHisto) {
  for (int i=0; i<32; i++) m_modNames[i] = "==";
  for (dbRecordIterator it=dbr->recordBegin(); it!=dbr->recordEnd(); it++) {
    std::string recName = (*it)->getName();
    std::map<std::string, int>::iterator typeit = m_histogramTypes.find(recName); 
    if (typeit!=m_histogramTypes.end()) {
      HistogramType type = (HistogramType)((*typeit).second);
      std::string name = (*typeit).first;                    
      m_histo[type] = *(new PixScanHisto()); 
      for (dbRecordIterator itt=(*it)->recordBegin(); itt!=(*it)->recordEnd(); itt++) {
        std::string recModName = (*itt)->getName();
        if (m_tmpConn->mods.find(recModName) != m_tmpConn->mods.end()) {
          int mod = m_tmpConn->mods[recModName]->modId;
          m_modNames[mod] = recModName;
          PixScanHisto *h = new PixScanHisto();
          m_histo[type].add(mod, *h);
          m_histo[type][mod].read(*itt, loadHisto);
        }
      }
    }
  }
}

void PixScan::readHisto(DbRecord* dbr, PixConnectivity *conn, bool loadHisto) {
  m_histo.clear();
  readInfo(dbr);
  m_tmpConn = conn;
  m_tmpGrp = NULL;
  m_ownTmpConn = false;
  // Load the connectivity db
  if (m_tmpConn->getConnTagC() != m_tagsConn ||
      m_tmpConn->getConnTagA() != m_tagsAlias ||
      m_tmpConn->getConnTagP() != m_tagsData) {
    m_tmpConn->clearConn();
    m_tmpConn->setConnTag(m_tagsConn, m_tagsData, m_tagsAlias);
    m_tmpConn->loadConn();
  }
  readHisto(dbr, loadHisto);
}

void PixScan::readHisto(DbRecord* dbr, PixModuleGroup *grp, bool loadHisto) {
  m_histo.clear();
  readInfo(dbr);
  // Create a mod group if needed
  if (grp == NULL) {
    // Load the connectivity db
    m_ownTmpConn = true;
    m_tmpConn = new PixConnectivity(m_tagsConn, m_tagsData, m_tagsAlias, m_tagsCfg);
    m_tmpConn->loadConn();
    // Load the configuration rev
    m_tmpConn->setCfgRev(m_tagsCfgRev);
    // Look for the rod/boc in connectivity
    if (m_tmpConn->rods.find(m_rodName) != m_tmpConn->rods.end()) {
      grp = m_tmpGrp = new PixModuleGroup(m_tmpConn, m_rodName, "");
    }
  }
  readHisto(dbr, loadHisto);
}    

void PixScan::write(DbRecord* dbr, PixModuleGroup *grp){
    // write config
    writeConfig(dbr);
    // write histograms
    writeHisto(dbr, grp);
}

void PixScan::read(DbRecord* dbr, PixModuleGroup *grp, bool loadHisto){
    // read config
    readConfig(dbr);
    // read histograms
    readHisto(dbr, grp, loadHisto);
}

std::string PixScan::getTime() {
    char t_s[100];
    time_t t;
    tm t_m;
    t = time(NULL);
    gmtime_r(&t, &t_m);
    strftime(t_s,100,"%Y-%m-%d %H:%M:%S",&t_m);
    std::string date = t_s;
    return date;
}

void PixScan::initTimeEnd() {
    m_timeInitEnd = getTime();
}

void PixScan::scanTimeStart() {
    m_timeScanStart = getTime();
}

void PixScan::scanTimeEnd() {
    m_timeScanEnd = getTime();
}

void PixScan::loop0Start() {
    gettimeofday(&m_timeLoop0Start,0);
}

void PixScan::loop0End() {
    gettimeofday(&m_timeLoop0End,0);
    m_timeLoop0Total += (m_timeLoop0End.tv_sec*1000000- m_timeLoop0Start.tv_sec*1000000+ m_timeLoop0End.tv_usec- m_timeLoop0Start.tv_usec);
}

void PixScan::initHistoDownloadTime() {
    m_timeHdownload = 0;
}

void PixScan::initLoop0Time() {
    m_timeLoop0Start.tv_sec = 0;
    m_timeLoop0Start.tv_usec = 0;
    m_timeLoop0End.tv_sec = 0;
    m_timeLoop0End.tv_usec = 0;
    m_timeLoop0Total = 0;
}


int PixScan::getNHisto() {
    int nHistos=0;
    if(m_histo.size() == 0) return 0;
    for (std::map<HistogramType, PixScanHisto>::iterator it=m_histo.begin(); it != m_histo.end(); it++)  {
        nHistos += (*it).second.countHistos();
    }
    return nHistos;
}

//TB: All the following lines are imported functions from PixModuleGroup


void PixScan::initScan(PixModuleGroup *grp){  
  std::cout << grp->getName() << " PixScan::initScan "<< std::endl;
  //This method has to prepare the module group for the scan. 
  scanTimeStart();
  scanTimeEnd();
  initHistoDownloadTime();
  initLoop0Time();

  grp->setm_useScanInSwap(false);
  grp->setm_useScanOutSwap(false);
  grp->setm_useScanModAltLink(false);

  m_swapInLinks = "-"; 
  m_swapOutLinks = "-"; 
  grp->setm_scanModAltLinks(0);

  if (m_swapInLinks != "-") {
    grp->setm_useScanInSwap(true);
  }
  if (m_swapOutLinks != "-") {
    grp->setm_useScanOutSwap(true);
  }
  if (m_useAltModLinks  != 0) {
    grp->setm_useScanModAltLink(true);
    grp->setm_scanModAltLinks(0xffffffff);
  }
  //  bool updateLinks = m_useScanInSwap | m_useScanOutSwap | m_useScanModAltLinks;
  //  if (m_curReadoutSpeed != (MccBandwidth)scn->getMccBandwidth() || updateLinks) {
  grp->setReadoutSpeed((MccBandwidth)m_mccBandwidth);
  //  }

  // Save module configurations if restore is needed at the end of the scan
  if (m_restoreModuleConfig) {
    for (unsigned int pmod=0; pmod<(unsigned int)grp->getmoduleSize(); pmod++){
    		for (int gr=0; gr<MAX_GROUPS;gr++){
    			if ((getModuleMask(gr)) & ((0x1)<<(grp->getmoduleId(pmod)))) { 
    			      	grp->storeConfig(pmod, "PreScanConfig");
    			}
    		}
    	}
   }
  

  /* kshmied: mod configuration has been found to be corrupterd in pit-scans several times -> this prevents that modules with corrupted configurations are configured scanned at all  */

  grp->moduleConfigIdentifier();
  /*
  if (m_runType == MONLEAKRUN) {
    // configure modules - only for non-BOC scans
    grp->sendModuleConfig();
  }*/

  // before scanning put all modules properties false


  for (unsigned int pmod=0; pmod<(unsigned int)grp->getmoduleSize(); pmod++){ 
    grp->setmoduleConfigActive(pmod, false);
    grp->setmoduleTriggerActive(pmod, false);
    grp->setmoduleStrobeActive(pmod, false);
    grp->setmoduleReadoutActive(pmod, false);

    // if the module in the configuration is the same in the group that I`m going to scan, switch on its properties
     for (int gr=0; gr<MAX_GROUPS;gr++){ 
     if (getModuleMask(gr) & ((0x1)<<grp->getmoduleId(pmod))) {

	if (getConfigEnabled(gr)) grp->setmoduleConfigActive(pmod, true); 
	if (getTriggerEnabled(gr)) grp->setmoduleTriggerActive(pmod, true);
	if (getStrobeEnabled(gr)) grp->setmoduleStrobeActive(pmod, true);
	if (getReadoutEnabled(gr)) grp->setmoduleReadoutActive(pmod, true);
      }
    }
  }
  initTimeEnd();
}



void PixScan::scanLoopStart(int nloop, PixModuleGroup *grp) {
  
if (nloop == 0) grp->setm_execToTerminate(false);
  // Do nothing else if loop is disabled or handled by the ROD
  if (getLoopActive(nloop)) {
    switch (getLoopAction(nloop)) {
    case PixScan::SCURVE_FIT:
      break;
    case PixScan::TDAC_TUNING:
      grp->prepareTDACTuning(nloop, this);
      break;
    case PixScan::GDAC_TUNING:
      break;
    case PixScan::GDAC_COARSE_FAST_TUNING:
      break;
    case PixScan::GDAC_FINE_FAST_TUNING:
      break;
    case PixScan::GDAC_FAST_TUNING:
      grp->prepareGDACFastTuning(nloop, this);
      break;
    case PixScan::FDAC_TUNING:
      // should the "Loop action specific methods" stay in PixModuleGroup (too many connections to FE!)
      grp->prepareFDACTuning(nloop, this);
      break;
    case PixScan::IF_TUNING:
      grp->prepareIFTuning(nloop, this);
      break;
    case PixScan::IF_FAST_TUNING:
      break;
    case PixScan::T0_SET:
      break;
    case PixScan::MCCDEL_FIT:
      break;
    case PixScan::NO_ACTION:
      break;
    default:
      break;
    }
  }
}


void PixScan::prepareStep(int nloop, PixModuleGroup *grp, PixHistoServerInterface *hInt, std::string folder) {
  std::cout <<"PS:prepareStep("<< nloop <<") "<< scanIndex(0)<<"/"<<scanIndex(1)<<"/"<< scanIndex(2) << std::endl;
  if (nloop == 0 && grp->getm_execToTerminate()) { 
    std::cout << "prepareStep: call scanTerminate" << std::endl;
scanTerminate(grp);
  }
  // Do nothing else if loop is disabled or handled by the ROD
  if (getLoopActive(nloop) && !getDspProcessing(nloop)) {
    // If you are in the innermost loop
    if (nloop == 0) {
      // If mask staging is under host control 
      if (!getDspMaskStaging()) {
	if (newMaskStep()) grp->setupMasks(0, this); //setup masks
      }
      if (newScanStep()) grp->setupScanVariable(0, this); //loop on variable
      // If you are in the other loops
    } else {
      std::cout << "before  grp->setupScanVariable nloop =  "  << nloop <<std::endl;
      grp->setupScanVariable(nloop, this);
       std::cout << "after grp->setupScanVariable nloop = " << nloop <<std::endl;
    }
    // Loop action specific settings
    std::cout << "loop action " << getLoopAction(nloop) <<std::endl;
   switch (getLoopAction(nloop)) { 
    case PixScan::TDAC_TUNING:
      grp->prepareTDACTuning(nloop, this);
      break;
    case PixScan::TDAC_TUNING_ITERATED:
      grp->prepareTDACTuningIterated(nloop, this);
      break;
    case PixScan::TDAC_FAST_TUNING:
      grp->prepareTDACFastTuning(nloop, this, hInt, folder);
      break;
   case PixScan::FDAC_FAST_TUNING:
     grp->prepareFDACFastTuning(nloop,this,hInt,folder);
     break;
    case PixScan::THR_FAST_SCANNING:
      grp->prepareThrFastScan(nloop, this, hInt, folder);
      break;
    case PixScan::GDAC_TUNING:
      grp->prepareGDACTuning(nloop, this);
      break;
    case PixScan::GDAC_TUNING_ITERATED:
      grp->prepareGDACTuningIterated(nloop, this);
      break;
    case PixScan::GDAC_COARSE_FAST_TUNING:
      grp->prepareFei4GDACCoarseTuning(nloop, this);
      break;
    case PixScan::GDAC_FINE_FAST_TUNING:
      grp->prepareFei4GDACFineTuning(nloop, this,hInt, folder);
      break;
    case PixScan::DISCBIAS_TUNING:
      grp->prepareDiscBiasTuning(nloop, this);
      break;
    case PixScan::FDAC_TUNING:
      grp->prepareFDACTuning(nloop, this);
      break;
    case PixScan::IF_TUNING:
      break;
    case PixScan::IF_FAST_TUNING:
      grp->prepareIFFastTuning(nloop, this, hInt, folder);
      break;
    case PixScan::T0_SET:
      grp->prepareT0Set(nloop, this);
      break;
    case PixScan::MIN_THRESHOLD:
      grp->prepareIncrTdac(nloop, this);
      break;
    case PixScan::MCCDEL_FIT:
      break;
    case PixScan::CHARGE_SET:
      grp->setCharge(nloop, this);
      break;
    case PixScan::NO_ACTION:
      for (int mod=0; mod<32; mod++) {
	if (nloop == 0 && scanIndex(0)>0 && (getLoopParam(0) == CHARGE) && grp->getpixCtrl_moduleActive(mod)){

	  writeHisto(hInt, grp, folder,(PixScan::HistogramType)TOT_MEAN , mod,scanIndex(2), scanIndex(1), scanIndex(0)-1); 
	  deleteHisto((PixScan::HistogramType)TOT_MEAN, mod,scanIndex(2), scanIndex(1), scanIndex(0)-1);

	 writeHisto(hInt, grp, folder,(PixScan::HistogramType)TOT_SIGMA , mod,scanIndex(2), scanIndex(1), scanIndex(0)-1); 
	 deleteHisto((PixScan::HistogramType)TOT_SIGMA, mod,scanIndex(2), scanIndex(1), scanIndex(0)-1); 
	}
      }
    default:
      break;
    }
  }
}


void PixScan::scanTerminate(PixModuleGroup *grp) {   
  // Backward comaptibility for apps without scanTerminate
  grp->setm_execToTerminate(false);
  // Check if the controller is a ROD 
  if (grp->checkRodController())   {
    // Stop histogramming and trapping tasks
    // rod->setConfigurationMode();   
    // Download histograms with the FILLED and KEPT flags (for loop 0 if executed by the HOST)
    if (getLoopActive(0) && !getDspProcessing(0)) {
      std::map<std::string, int> &hTypes = getDspHistogramTypes();
      std::map<std::string, int>::iterator it;
      for (it = hTypes.begin(); it != hTypes.end(); ++it)  {
	std::string name = (*it).first;
	PixScan::HistogramType type = (PixScan::HistogramType)((*it).second);
	if (getHistogramFilled(type) && getHistogramKept(type)) {
	  for (int mod=0; mod<32; mod++) {
	    downloadHisto(grp->getPixController(), mod, type);                                // CHANGE !!
	  }
	}
      }
      grp->rodStopScan();

    }
  }

}


void PixScan::scanLoopEnd(int nloop, PixModuleGroup *grp, PixHistoServerInterface *hInt, std::string folder) {

  // This method will perform the end-of-loop actions.
  // Typically this method will upload histograms or fit results from the 
  // ROD and store them in the appropriate PixScan structures.

  // Backward comaptibility for apps without scanTerminate
  if (nloop == 0 && grp->getm_execToTerminate()) scanTerminate(grp);
 

  // Nothing to do if the loop is inactive, unless it's loop 0
  if (!getLoopActive(nloop) && nloop != 0) return;

  // Nothing to do for loop 0 if loop 1 is executed by the ROD
  if (nloop == 0 && getDspProcessing(1)) return;

  // Nothing to do for loop 1 if loop 2 is executed by the ROD
  if (nloop == 1 && getDspProcessing(2)) return;


  // Restore original scan variables if needed

  if (getLoopParam(nloop) == OB_VISET) {
     std::cout << grp->getName() <<" PixScan::scanLoopEnd nloop, ok OB_VISET "<< std::endl;
     grp->resetViset();
  }

  // Download histograms with the FILLED and KEPT flags (for loop 0 and 1 if executed by the ROD)
    if ((nloop == 0 && getDspProcessing(0)) ||
        (nloop == 1 && getDspProcessing(1)) ||
        (nloop == 2 && getDspProcessing(2)) ||
        (nloop == 0 && !getLoopActive(0))) {

    // Download dsp histograms
    std::map<std::string, int> &hTypes = getDspHistogramTypes();
    std::map<std::string, int>::iterator it;
    for (it = hTypes.begin(); it != hTypes.end(); ++it)  {
      std::string name = (*it).first;
      HistogramType type = (HistogramType)((*it).second);
      if (getHistogramFilled(type) && 
	  (getHistogramKept(type) || (type==OCCUPANCY && getLoopAction(nloop)==SCURVE_FIT && !getDspLoopAction(nloop)))) {
	for (int mod=0; mod<32; mod++) {
	  downloadHisto(grp->getPixController(), mod, type);                        // CHANGE !!
	 
	  if(grp->getpixCtrl_moduleActive(mod)) {
	    
	    for(int i0 = 0; i0<getLoopVarNSteps(0); i0++){
	     
	      if(getHistogramKept(type)) writeHisto(hInt, grp, folder,type, mod, scanIndex(2), scanIndex(1), i0); 
	      // temporary for USBPix: need occupancy histos for S-curve fit off controller
	      //if(type!=OCCUPANCY || getLoopAction(nloop)!=SCURVE_FIT || getDspLoopAction(nloop))deleteHisto(type, mod, scanIndex(2), scanIndex(1), i0); 
	    }
	  }
	}
      }
    }
  }

  // Create loop-summary RAW histograms
  if (nloop == 0) {
    if (getHistogramFilled(RAW_DATA_0)) {
      // Module loop 
      for (unsigned int pmod=0; pmod<(unsigned int)grp->getmoduleSize(); pmod++) {
	if (grp->getmoduleReadoutActive(pmod)) {

	  unsigned int mod = grp->getmoduleId(pmod);
	  if (getHistogramFilled(RAW_DATA_1) || getHistogramFilled(RAW_DATA_DIFF_1)) {
	    for (unsigned int ilnk=0; ilnk<4; ilnk++) {
	      bool first = true;
	      Histo *h1=nullptr, *hDiff1=nullptr;
	      int nl = getLoopVarNSteps(0);
	      for (int il=0; il<nl; il++) {
		Histo& h = getHisto(RAW_DATA_0, grp, mod, scanIndex(2), scanIndex(1), il, ilnk);
		if (h.name() != "Not Found") {
		  if (first) {
		    if (getHistogramFilled(RAW_DATA_1)) {
		      std::ostringstream nam, tit;
		      nam << "RAW_1_" << mod << "-Link" << ilnk << "_" << scanIndex(1) << "_" << scanIndex(2);
		      tit << "Raw data 1 mod " << mod << "-Link" << ilnk << " L1=" << scanIndex(1) << " L2=" << scanIndex(2);
		      h1 = new Histo(nam.str(), tit.str(), h.nBin(0), -0.5, h.nBin(0)-0.5, nl, -0.5, nl-0.5);
		      addHisto(*h1, RAW_DATA_1, mod, scanIndex(2), scanIndex(1), -1, ilnk);
		    }
		    if (getHistogramFilled(RAW_DATA_DIFF_1)) {
		      std::ostringstream nam, tit;
		      nam << "RAW_DIFF_1_" << mod << "-Link" << ilnk << "_" << scanIndex(1) << "_" << scanIndex(2);
		      tit << "Raw data diff 1 mod " << mod << "-Link" << ilnk << " L1=" << scanIndex(1) << " L2=" << scanIndex(2);
		      hDiff1 = new Histo(nam.str(), tit.str(), 1, -0.5, 0.5, nl, -0.5, nl-0.5);
		      addHisto(*hDiff1, RAW_DATA_DIFF_1, mod, scanIndex(2), scanIndex(1), -1, ilnk);
		    } 
		    first = false;
		  }
		  if (getHistogramFilled(RAW_DATA_DIFF_1)) {
		    hDiff1->set(0, il, h(0, 1));  
		  } 
		  for (int id=0; id<h.nBin(0); id++) {
		    if (getHistogramFilled(RAW_DATA_1)) {
		      h1->set(id, il, h(id, 0));  
		    }
		  }
		}
	      }
	    }
	  }
	  if (!getHistogramKept(RAW_DATA_0)) clearHisto(mod, RAW_DATA_0);
	}
      }
    }
  } else if (nloop == 1) {
    if (getHistogramFilled(RAW_DATA_1)) {
      // Module loop
      for (unsigned int pmod=0; pmod<(unsigned int)grp->getmoduleSize(); pmod++) {
	if (grp->getmoduleReadoutActive(pmod)) {
	  unsigned int mod = grp->getmoduleId(pmod);
	  if (getHistogramFilled(RAW_DATA_2) || getHistogramKept(RAW_DATA_2)) {
	    for (unsigned int ilnk=0; ilnk<4; ilnk++) {
	      bool first = true;
	      Histo *hDiff2;
	      int nl = getLoopVarNSteps(1);
	      for (int il=0; il<nl; il++) {
		Histo& h = getHisto(RAW_DATA_1, grp, mod, scanIndex(2), il, -1, ilnk);
		if (h.name() != "Not Found") {
		  if (first) {
		    std::ostringstream nam, tit;
		    nam << "RAW_2_" << mod << "-Link" << ilnk << "_" << scanIndex(2);
		    tit << "Raw data 2 mod " << mod << "-Link" << ilnk << " L2=" << scanIndex(2);
		    hDiff2 = new Histo(nam.str(), tit.str(), h.nBin(1), -0.5, h.nBin(1)-0.5, nl, -0.5, nl-0.5);
		    addHisto(*hDiff2, RAW_DATA_2, mod, scanIndex(2), -1, -1, ilnk);
		    first = false;
		  }
		  for (int id=0; id<h.nBin(1); id++) {
		    hDiff2->set(id, il, h(0, id));   
		  }
		}
	      }
	    }
	  }
	  if (!getHistogramKept(RAW_DATA_1)) clearHisto(mod, RAW_DATA_1);
	}
      }
    }
    if (getHistogramFilled(RAW_DATA_DIFF_1)) {
      // Module loop 
      for (unsigned int pmod=0; pmod<(unsigned int)grp->getmoduleSize(); pmod++) {
	if (grp->getmoduleReadoutActive(pmod)) {
	  unsigned int mod = grp->getmoduleId(pmod);
	  if (getHistogramFilled(RAW_DATA_DIFF_2) || getHistogramKept(RAW_DATA_DIFF_2)) {
	    for (unsigned int ilnk=0; ilnk<4; ilnk++) {
	      bool first = true;
	      Histo *hDiff2;
	      int nl = getLoopVarNSteps(1);
	      for (int il=0; il<nl; il++) {
		Histo& h = getHisto(RAW_DATA_DIFF_1, grp, mod, scanIndex(2), il, -1, ilnk);
		if (h.name() != "Not Found") {
		  if (first) {
		    std::ostringstream nam, tit;
		    nam << "RAW_DIFF_2_" << mod << "-Link" << ilnk << "_" << scanIndex(2);
		    tit << "Raw data diff 2 mod " << mod << "-Link" << ilnk << " L2=" << scanIndex(2);
		    hDiff2 = new Histo(nam.str(), tit.str(), h.nBin(1), -0.5, h.nBin(1)-0.5, nl, -0.5, nl-0.5);
		    addHisto(*hDiff2, RAW_DATA_DIFF_2, mod, scanIndex(2), -1, -1, ilnk);
		    first = false;
		  }
		  for (int id=0; id<h.nBin(1); id++) {
		    hDiff2->set(id, il, h(0, id));   
		  }
		}
	      }
	    }
	  }
	  if (!getHistogramKept(RAW_DATA_DIFF_1)) clearHisto(mod, RAW_DATA_DIFF_1);
	}
      }
    }
  }
  //for BOC_THR_DEL_DSP: (RAW_DATA_DIFF_2 is not DSP histo type)
  if(nloop<2&&getDspProcessing(nloop)&&!getDspProcessing(nloop+1)&&getRunType()==RAW_EVENT&&getDspProcessing(0)){
    for (int mod=0; mod<32; mod++) {
      if (getHistogramFilled(RAW_DATA_DIFF_2))downloadHisto(grp->getPixController(), mod,  RAW_DATA_DIFF_2);
      else if(getHistogramFilled(RAW_DATA_REF))downloadHisto(grp->getPixController(), mod,  RAW_DATA_REF);//Slow turn on
    }
  }
  // Execute the end-loop action
  switch (getLoopAction(nloop)) {
      case SCURVE_FIT:
          if (getDspLoopAction(nloop)) {
              for (int mod=0; mod<32; mod++) {
		  if(nloop == 0 && getLoopParam(0) == STROBE_DELAY && grp->getpixCtrl_moduleActive(mod)){
		    downloadHisto(grp->getPixController(), mod, TIMEWALK);
		    if(getLoopAction(1)!=PixScan::T0_SET){
		      writeHisto(hInt, grp, folder,(PixScan::HistogramType)TIMEWALK , mod, scanIndex(2), scanIndex(1), -1); 
		      deleteHisto((PixScan::HistogramType)TIMEWALK, mod, scanIndex(2), scanIndex(1), -1); 
		    }
		  } else {
		    downloadHisto(grp->getPixController(), mod, SCURVE_MEAN);
		    downloadHisto(grp->getPixController(), mod, SCURVE_SIGMA);
		    downloadHisto(grp->getPixController(), mod, SCURVE_CHI2);
		    if (nloop == 0 && getLoopParam(0) == VCAL 
		        && getLoopAction(1) != GDAC_TUNING && getLoopAction(1) != TDAC_TUNING && grp->getpixCtrl_moduleActive(mod) ) {
		      writeHisto(hInt, grp, folder,(PixScan::HistogramType)SCURVE_MEAN , mod, scanIndex(2), scanIndex(1), -1); 
		      deleteHisto((PixScan::HistogramType)SCURVE_MEAN, mod, scanIndex(2), scanIndex(1), -1); 
                      if (!getLoopActive(1) && !getLoopActive(2)) { // THR scans
                          writeHisto(hInt, grp, folder,(PixScan::HistogramType)SCURVE_SIGMA , mod, scanIndex(2), scanIndex(1), -1); 
			  deleteHisto((PixScan::HistogramType)SCURVE_SIGMA, mod, scanIndex(2), scanIndex(1), -1); 
                          writeHisto(hInt, grp, folder,(PixScan::HistogramType)SCURVE_CHI2 , mod, scanIndex(2), scanIndex(1), -1); 
			  deleteHisto((PixScan::HistogramType)SCURVE_CHI2, mod, scanIndex(2), scanIndex(1), -1); 
                      } else if (getLoopParam(1) == CHARGE || getLoopParam(1) == VCAL) { //Timewalk measure
                          writeHisto(hInt, grp, folder,(PixScan::HistogramType)SCURVE_SIGMA, mod, scanIndex(2), scanIndex(1), -1); 
			  deleteHisto((PixScan::HistogramType)SCURVE_SIGMA, mod, scanIndex(2), scanIndex(1), -1); 
                          writeHisto(hInt, grp, folder,(PixScan::HistogramType)SCURVE_CHI2 , mod, scanIndex(2), scanIndex(1), -1); 
			  deleteHisto((PixScan::HistogramType)SCURVE_CHI2, mod, scanIndex(2), scanIndex(1), -1); 
                      }
		    }
		  }
              }
          } else {
	    for (int mod=0; mod<32; mod++) {
	      calcThr(grp->getPixController(), mod, scanIndex(2), scanIndex(1));
	      // clean up occupancy histos here if not requested to keep them
	      if (!getHistogramKept(OCCUPANCY)){
		if(grp->getpixCtrl_moduleActive(mod)) {
		  for(int i0 = 0; i0<getLoopVarNSteps(0); i0++){
		    deleteHisto(OCCUPANCY, mod, scanIndex(2), scanIndex(1), i0); 
		  }
		}
              }
	    }
	  }
          break;
      case TDAC_FAST_TUNING:
          grp->endTDACFastTuning(nloop, this, hInt, folder);
          break;
      case FDAC_FAST_TUNING:
	  grp->endFDACFastTuning(nloop,this,hInt,folder);
	  break;
      case THR_FAST_SCANNING:
          grp->endThrFastScan(nloop, this, hInt, folder);
          break;
      case TDAC_TUNING:
          grp->endTDACTuning(nloop, this);
          break;
      case TDAC_TUNING_ITERATED:
          grp->endTDACTuningIterated(nloop, this);
          break;
      case GDAC_TUNING:
          grp->endGDACTuning(nloop, this);
          break;
      case GDAC_FAST_TUNING:
          grp->endGDACTuningInterpolate(nloop, this);
          break;
      case GDAC_TUNING_ITERATED:
          grp->endGDACTuningInterpolate(nloop, this,true);
          break;
      case GDAC_COARSE_FAST_TUNING:
          break;
      case GDAC_FINE_FAST_TUNING:
	    grp->endFei4GDACFastFineTuning(nloop,this,hInt,folder);
          break;
      case DISCBIAS_TUNING:
	   grp->endDiscBiasTuning(nloop,this);
	  break;
      case FDAC_TUNING:
          grp->endFDACTuning(nloop, this);
          //grp->stepFDACTuning(-2, this, hInt, folder); // -2 is just a temporary workaround
          break;
      case IF_TUNING:
          grp->endIFTuning(nloop, this);
          break;
      case IF_FAST_TUNING:
          grp->endIFFastTuning(nloop, this, hInt, folder);
          break;
      case T0_SET:
          grp->endT0Set(nloop, this, hInt, folder);
          break;
      case MCCDEL_FIT:
          grp->mccDelFit(nloop, this, hInt, folder);
          break;
      case BOC_TUNING:
          grp-> endBOCTuning(nloop, this);
          break;
      case OPTO_TUNING:
          grp->endOPTOTuning(nloop, this); 
          break; 
      case NO_ACTION:
          break;
      default:
          break;
  }

    if ((nloop == 0 && getDspProcessing(0)) ||
     (nloop == 1 && getDspProcessing(1)) ||
     (nloop == 0 && !getLoopActive(0))) {
    grp->rodStopScan();
  }
  
  // Update scan termination time
  scanTimeEnd();
}



void PixScan::terminateScan(PixModuleGroup *grp){  

    // This method has to execute end-of-loop actions. 

    // Restore module configurations if needed
  if (getRestoreModuleConfig()) {
    std::cout<<"Restoring Pixel config"<<std::endl;
    grp->restoreModuleConfig(this, "PreScanConfig");
  }

  if(getCloneModCfgTag()){
    std::cout<<"Writing config after tuning"<<std::endl;
    grp->writeConfig( );
  }

  // Always configure module in the end of the scan. It can recover the module from undefined state after cross talk/monleak scan
  if ( !m_useEmulator && (getRunType() == NORMAL_SCAN || getRunType() == MONLEAKRUN))
    grp->sendModuleConfigWithPreampOff();//Always configure with preams-off at the end of the scan JuanAn

  if(getRunType()==RAW_EVENT){//Restore THR/DELAY/PHASES after BOC scan
    std::cout<<"Restoring BOC configuration after optoscan"<<std::endl;
    if(grp->getPixBoc())(grp->getPixBoc())->BocConfigure();
  }

  // Set correct readout speed
  //bool updateLinks = m_useScanInSwap | m_useScanOutSwap | m_useScanModAltLinks;
  grp->setm_useScanInSwap(false);
  grp->setm_useScanOutSwap(false);
  grp->setm_useScanModAltLink(false);
  m_swapInLinks = "-"; 
  m_swapOutLinks = "-"; 
  grp->setm_scanModAltLinks(0);



  // Update scan termination time
  scanTimeEnd();
}



void PixScan::writeAndDeleteHisto(PixModuleGroup *grp, PixHistoServerInterface* hInt, std::string folder, 
					 unsigned int mod, int type, int idx2, int idx1, int idx0)
{
  this->writeHisto(hInt, grp, folder, (HistogramType) type, mod, idx2, idx1, idx0); 
  this->deleteHisto((HistogramType) type, mod, idx2, idx1, idx0);
}

//TB End of functions imported from PixModuleGroup
