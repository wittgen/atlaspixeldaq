////////////////////////////////////////////////////////////////
// BarrelRodPixController.cxx
/////////////////////////////////////////////////////////////////////
//
// Author: Laser Kaplan
// Based on: RodPixController + BarrelRodPixController
//

//! Class for the Barrel ROD (Barrel)

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

// #define __MCC_TEST__ //  Comment out for regular operation !!!!
// #define DEBUG_ND
// #define DEBUG_ND_IS

#include "PixController/BarrelRodPixController.h"

#include "Fei3ModCfg.h"
#include "PixScan.h"

#include "PixMcc/PixMcc.h"
#include "PixMcc/PixMccI1.h"
#include "PixMcc/PixMccI2.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI3.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixUtilities/PixISManager.h"

#include "WriteConfig.h"
#include "SendModuleConfig.h"
#include "StartScanL12.h"
#include "PixScanBaseToRod.h"
#include "ReadRodMasterBusyStatus.h"
#include "ReadRodSlaveBusyStatus.h"
#include "ResetRodBusyHistograms.h"
#include "DumpRodRegisters.h"
#include "GetDSPResults.h"
#include "CleanDSPResults.h"
#include "DataTakingTools.h"
#include "ReadRodVetoCounters.h"
#include "RecoAtECR_manager.h"

#include <oh/OHRootReceiver.h>
#include <oh/OHIterator.h>

using namespace PixLib;
using namespace SctPixelRod;

bool BarrelRodPixController::m_errPublishedConn       = false;
bool BarrelRodPixController::m_errPublishedResponding = false;
bool BarrelRodPixController::m_errPublishedChannels   = false;

BarrelRodPixController::BarrelRodPixController(PixModuleGroup &modGrp) : PPCppRodPixController(modGrp) { //! Constructor
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
    configInit();
    initController();
}

BarrelRodPixController::BarrelRodPixController(PixModuleGroup &modGrp, DbRecord *dbRecord): PPCppRodPixController(modGrp, dbRecord) { //! Constructor
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
    configInit();
    if (dbRecord != NULL) m_conf->read(dbRecord);
    initController();
}

BarrelRodPixController::BarrelRodPixController(PixModuleGroup &modGrp, PixDbServerInterface *dbServer, std::string dom, std::string tag) : PPCppRodPixController(modGrp) {
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
    configInit();
    if(!m_conf->read(dbServer, dom, tag,  getCtrlName()))
        std::cout << __FILE__ << ", " << __LINE__ << ": Problem in reading configuration" << std::endl;
    initController();
}

//todo cleanup
void BarrelRodPixController::initController() {
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
    defaultInit();
    // Server for sending commands
    // Todo: make this ENV dependent
    // todo hardcoded 32

    m_rod = std::make_unique<IblRodModule>(0, 0, 1, CMD_SRV_IP_PORT, m_ipAddress); 
    m_flavour = PixModule::PM_FE_I2;
}

void BarrelRodPixController::initHW() {
  if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
  resetFPGA( );
}

void BarrelRodPixController::configInit() {
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << std::endl;
    // Create the Config object
    m_conf = new Config("BarrelRodPixController"); 
    Config &conf = *m_conf;
    // Group general
    conf.addGroup("general");
    conf["general"].addInt("Slot", m_ctrlID, 16,"Rod slot", true);
    conf["general"].addString("IPaddress", m_ipAddress, "192.168.2.160","IPaddress", true); 

    conf.addGroup("Groups");
    for (int ob=0; ob<4; ob++) {
        for (int ph=0; ph<4; ph++) {
            std::ostringstream os, os1;
            os << "id" << ph+1 << "_" << ob;
            os1 << "PP0 " << ob << " Group Id phase " << ph+1;
            conf["Groups"].addInt(os.str(), m_grId[ob][ph], 1, os1.str(), true);
        }
    }

    // Group FMT
    conf.addGroup("fmt");
    for (unsigned int i=0; i<8; i++) {
        std::ostringstream fnum;
        fnum << i;
        std::string tit = "linkMap_" + fnum.str();
        conf["fmt"].addInt("linkMap_" + fnum.str(), m_fmtLinkMap[i], 0x54320000u,"Formatter "+fnum.str()+" link map", true);
    }
    m_modActive.fill(false);
    m_moduleRx.fill(-1);

    conf.reset();
}

uint32_t BarrelRodPixController::getRunMask(){
   return getRxMaskFromModuleMask(m_modGroup.getActiveModuleMask());
}


void BarrelRodPixController::writeModuleConfig(PixModule& mod, bool forceWrite) {     //! Write module configuration
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<<std::endl;
    // Adding a protection against configuration items with zero revision (i.e. defaults)
    // This is strictly speaking not needed since the L12 code on the PPC protects us against this
    // but we add it here for consistency with the other controllers
    if (mod.config().rev() == 0) {
      throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::FATAL,
          getModGroup().getRodName(), m_ctrlID);
    }

    int moduleId = mod.moduleId();
    if (moduleId < 0 || moduleId >= MAX_MODULES) {
        throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::INVALID_MODID, PixControllerExc::ERROR,
                getModGroup().getRodName(), m_ctrlID); 
    }else m_modActive[moduleId] = true;

    // read position of module and, if necessary, add a new module
    unsigned int pos;
    if (m_modPosition[moduleId] < 0 || m_modPosition[moduleId] >= MAX_MODULES) {
        pos = m_nMod++; // New module
        if (pos >= MAX_MODULES) { 
            throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::TOO_MANY_MODULES, PixControllerExc::FATAL,  
                    getModGroup().getRodName(), m_ctrlID); }
        m_modPosition[moduleId] = pos; 
    } else pos = m_modPosition[moduleId]; // Redefinition of a module

    Fei3ModCfg feMod;

    // General stuff
    strncpy(feMod.idStr, mod.moduleName().c_str(), sizeof(feMod.idStr)-1);
    strncpy(feMod.tag, mod.config().tag().c_str(), sizeof(feMod.tag)-1);
    feMod.revision = mod.config().rev();
    feMod.active = true;
    feMod.moduleIdx = moduleId;
    feMod.groupId = mod.groupId();
    //uint8_t inputLink;
    uint8_t outputLink;
    uint8_t outputLink2=255;
    try {
        feMod.m_txCh = ((ConfInt&)mod.config()["general"]["InputLink"]).getValue();
        feMod.m_rxCh = ((ConfInt&)mod.config()["general"]["OutputLink1"]).getValue();
        //inputLink = ((ConfInt&)mod.config()["general"]["InputLink"]).getValue();
    	outputLink = ((ConfInt&)mod.config()["general"]["OutputLink1"]).getValue();
    	outputLink2 = ((ConfInt&)mod.config()["general"]["OutputLink2"]).getValue();
    }
    catch (...) {
        throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::ERROR, getModGroup().getRodName(), m_ctrlID);
    }

    uint8_t txCh=0;
    //uint8_t rxCh;
    // this is potentially temporary -- may change to use In(Out)putLink instead of BocIn(Out)putLink
    uint8_t bocInputLink = ((ConfInt&)mod.config()["general"]["BocInputLink"]).getValue();
    uint8_t bocOutputLink1 = ((ConfInt&)mod.config()["general"]["BocOutputLink1"]).getValue();
    uint8_t bocOutputLink2 = ((ConfInt&)mod.config()["general"]["BocOutputLink2"]).getValue();
    //uint8_t bocOutputLink = (*fe)->number() ? ((ConfInt&)mod.config()["general"]["BocOutputLink1"]).getValue()
    //  : ((ConfInt&)mod.config()["general"]["BocOutputLink2"]).getValue();
    string modConnName =((ConfString&)mod.config()["geometry"]["connName"]).m_value;

    if (m_ctrlVerbose)
      std::cout << "Module " << mod.moduleName()<< " and conn name "<<modConnName << " with id: " << moduleId << ", and position: "
        << pos << " has BocInputLink: " << (int)bocInputLink << " and BocOutputLink1: " << (int)bocOutputLink1 <<" and BocOutputLink2: " << (int)bocOutputLink2 << std::endl;

    // HARD-CODED TMP UNTIL CONNECTIVITY IS FIXED
    /*
     *  A-side -- TX=0x ; RX1=1x ; RX2=0x
     *  C-side -- TX=3x ; RX1=2x ; RX2=3x
     *
     */

    switch(bocInputLink/10) {
      case 0: txCh = 23 - (bocInputLink%10) ; break;
      case 1: txCh = 31 - (bocInputLink%10) ; break;
      case 2: txCh = 15 - (bocInputLink%10) ; break;
      case 3: txCh =  7 - (bocInputLink%10) ; break;
    }

     //In case we use the alternative link during scans, flag is cleared up after scan finish
      if(getModGroup().getm_useScanModAltLink() && outputLink2 >=0 && outputLink2 <32)
       feMod.m_rxCh = outputLink2;
      else
       feMod.m_rxCh = outputLink;
    feMod.m_txCh = txCh;//inputLink

    if (m_ctrlVerbose) std::cout << "Module " << feMod.idStr << " with id: " << (int) feMod.moduleIdx<<" in ROD: "<< getModGroup().getRodName()<<" , connName: " <<modConnName << " TxCh: " << (int) feMod.m_txCh << " RxCh: " << (int) feMod.m_rxCh << std::endl;

    if (mod.useAltLink())std::cout<<"Warning using altlink  in ROD: "<< getModGroup().getRodName()<<" , connName: " <<modConnName <<" Module "<< feMod.idStr<<std::endl;

    // MCC registers
    PixMcc *mcc = mod.pixMCC();
    feMod.mccFlavor = 0;
    if (mcc != 0) {
        try {
            feMod.mccRegisters[CSR] = mcc->readRegister("CSR");
            feMod.mccRegisters[LV1] = mcc->readRegister("LV1");
            feMod.mccRegisters[FEEN] = mcc->readRegister("FEEN");
            feMod.mccRegisters[WFE] = mcc->readRegister("WFE");
            feMod.mccRegisters[WMCC] = mcc->readRegister("WMCC");
            feMod.mccRegisters[CNT] = mcc->readRegister("CNT");
            feMod.mccRegisters[CAL] = mcc->readRegister("CAL");
            feMod.mccRegisters[PEF] = mcc->readRegister("PEF");
            feMod.mccRegisters[SBSR] = 0;
            feMod.mccRegisters[WBITD] = 0;
            feMod.mccRegisters[WRECD] = 0;
            if (dynamic_cast<PixMccI2*>(mcc) != 0) {
                feMod.mccFlavor = MCC_I2;
                feMod.mccRegisters[SBSR] = mcc->readRegister("SBSR");
                feMod.mccRegisters[WBITD] = mcc->readRegister("WBITD");
                feMod.mccRegisters[WRECD] = mcc->readRegister("WRECD");
            }
            else if (dynamic_cast<PixMccI1*>(mcc) != 0) {
                feMod.mccFlavor = MCC_I1;
            }
        }
        catch (...) {
            throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::ERROR, getModGroup().getRodName(), m_ctrlID);
        }
        if (feMod.mccFlavor == 0) {
            throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::ERROR, getModGroup().getRodName(), m_ctrlID);
        }
    }
    
    if (m_ctrlVerbose) std::cout << "MCC configured" << std::endl;

    feMod.maskEnableFEConfig = 0;
    feMod.maskEnableFEScan = 0;
    feMod.maskEnableFEDacs = 0;
    feMod.feFlavor = 0;

    // loop over front ends and prepare config to send
    for(auto fe = mod.feBegin(); fe != mod.feEnd(); fe++){
        try{
            auto fei3 = dynamic_cast<PixFeI3*>(*fe);
            if(fei3) feMod.feFlavor = FE_I2;
	    else throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::ERROR, getModGroup().getRodName(), m_ctrlID); 

            int number = (*fe)->number(); 

            if (m_ctrlVerbose) std::cout << "Writing configuration for FE number: " << number << " of type: " << (int)feMod.feFlavor << std::endl;

            // Extended Config
            feMod.m_chipCfgs[number].setAddress((int)((ConfInt&)(*fe)->config()["Misc"]["Address"]).getValue());
            feMod.m_chipCfgs[number].setCinjLow((float)((ConfFloat&)(*fe)->config()["Misc"]["CInjLo"]).value());
            feMod.m_chipCfgs[number].setCinjHigh((float)((ConfFloat&)(*fe)->config()["Misc"]["CInjHi"]).value());

            feMod.m_chipCfgs[number].setVcalCoeff0((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient0"]).value());
            feMod.m_chipCfgs[number].setVcalCoeff1((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient1"]).value());
            feMod.m_chipCfgs[number].setVcalCoeff2((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient2"]).value());
            feMod.m_chipCfgs[number].setVcalCoeff3((float)((ConfFloat&)(*fe)->config()["Misc"]["VcalGradient3"]).value());

            int enDac, enCfg, enScan;
            enCfg  = (int)((ConfBool&)(*fe)->config()["Misc"]["ConfigEnable"]).value();
            enScan = (int)((ConfBool&)(*fe)->config()["Misc"]["ScanEnable"]).value();
            enDac  = (int)((ConfBool&)(*fe)->config()["Misc"]["DacsEnable"]).value();
            feMod.maskEnableFEConfig &= (~(0x1<<number));  feMod.maskEnableFEConfig |= enCfg<<number;
            feMod.maskEnableFEScan   &= (~(0x1<<number));  feMod.maskEnableFEScan   |= enScan<<number;
            feMod.maskEnableFEDacs   &= (~(0x1<<number));  feMod.maskEnableFEDacs   |= enDac<<number;

            // Global Config
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::Latency, fei3->readGlobRegister(Fei3::GlobReg::Latency));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::SelfTriggerDelay, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::SelfTriggerWidth, fei3->readGlobRegister(Fei3::GlobReg::SelfTriggerWidth));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableSelfTrigger, fei3->readGlobRegister(Fei3::GlobReg::EnableSelfTrigger));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableHitParity, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::Select_DO, fei3->readGlobRegister(Fei3::GlobReg::Select_DO));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::Select_MonHit, fei3->readGlobRegister(Fei3::GlobReg::Select_MonHit));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TSI_TSC_Enable, fei3->readGlobRegister(Fei3::GlobReg::TSI_TSC_Enable));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::SelectDataPhase, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableEOEParity, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::HitBusScaler, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::MonLeakADC, fei3->readGlobRegister(Fei3::GlobReg::MonLeakADC));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ARegTrim, 1);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableARegMeas, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ARegMeas, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableAReg, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableLVDSReferenceMeas, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::DRegTrim, 1);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableDRegMeas, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::DRegMeas, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::CapMeasure, fei3->readGlobRegister(Fei3::GlobReg::CapMeasure));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCapTest, fei3->readGlobRegister(Fei3::GlobReg::EnableCapTest));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableAnalogOut, fei3->readGlobRegister(Fei3::GlobReg::EnableAnalogOut));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestPixelMUX, fei3->readGlobRegister(Fei3::GlobReg::TestPixelMUX));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableVCalMeas, fei3->readGlobRegister(Fei3::GlobReg::EnableVCalMeas));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableLeakMeas, fei3->readGlobRegister(Fei3::GlobReg::EnableLeakMeas));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableBufferBoost, fei3->readGlobRegister(Fei3::GlobReg::EnableBufferBoost));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol8, fei3->readGlobRegister(Fei3::GlobReg::EnableCol8));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IVDD2, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IVDD2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IVDD2, fei3->readGlobRegister(Fei3::GlobReg::IVDD2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ID, fei3->readGlobRegister(Fei3::GlobReg::ID));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_ID, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_ID));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol7, fei3->readGlobRegister(Fei3::GlobReg::EnableCol7));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IP2, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IP2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IP2, fei3->readGlobRegister(Fei3::GlobReg::IP2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IP, fei3->readGlobRegister(Fei3::GlobReg::IP));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IP, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IP));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol6, fei3->readGlobRegister(Fei3::GlobReg::EnableCol6));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_ITrimTh, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_ITrimTh));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ITrimTh, fei3->readGlobRegister(Fei3::GlobReg::ITrimTh));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IF, fei3->readGlobRegister(Fei3::GlobReg::IF));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IF, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IF));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol5, fei3->readGlobRegister(Fei3::GlobReg::EnableCol5));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_ITrimIf, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_ITrimIf));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ITrimIf, fei3->readGlobRegister(Fei3::GlobReg::ITrimIf));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::VCal, fei3->readGlobRegister(Fei3::GlobReg::VCal));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_VCal, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_VCal));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol4, fei3->readGlobRegister(Fei3::GlobReg::EnableCol4));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::HighInjCapSel, fei3->readGlobRegister(Fei3::GlobReg::HighInjCapSel));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableExtInj, fei3->readGlobRegister(Fei3::GlobReg::EnableExtInj));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestAnalogRef, fei3->readGlobRegister(Fei3::GlobReg::TestAnalogRef));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EOC_MUX, fei3->readGlobRegister(Fei3::GlobReg::EOC_MUX));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::CEU_Clock, fei3->readGlobRegister(Fei3::GlobReg::CEU_Clock));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableDigInj, fei3->readGlobRegister(Fei3::GlobReg::EnableDigInj));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol3, fei3->readGlobRegister(Fei3::GlobReg::EnableCol3));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_ITH1, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_ITH1));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ITH1, fei3->readGlobRegister(Fei3::GlobReg::ITH1));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ITH2, fei3->readGlobRegister(Fei3::GlobReg::ITH2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_ITH2, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_ITH2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol2, fei3->readGlobRegister(Fei3::GlobReg::EnableCol2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IL, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IL));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IL, fei3->readGlobRegister(Fei3::GlobReg::IL));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::IL2, fei3->readGlobRegister(Fei3::GlobReg::IL2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::TestDAC_IL2, fei3->readGlobRegister(Fei3::GlobReg::TestDAC_IL2));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol1, fei3->readGlobRegister(Fei3::GlobReg::EnableCol1));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::THRMIN, fei3->readGlobRegister(Fei3::GlobReg::THRMIN));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::THRDUB, fei3->readGlobRegister(Fei3::GlobReg::THRDUB));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::ReadMode, fei3->readGlobRegister(Fei3::GlobReg::ReadMode));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableCol0, fei3->readGlobRegister(Fei3::GlobReg::EnableCol0));

	    //The HitBus is always set to Enable for historical reasons. 
	    //The option to change it from Console has been removed
	    //This bit is useful only for special cases and should be set directly here in the code. 
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::HitBusEnable, 1);
	    
            if (feMod.feFlavor == FE_I2) feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::GlobalTDAC, fei3->readGlobRegister(Fei3::GlobReg::GlobalTDAC));
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableTune, 0);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableBiasCompensation, 1);
            feMod.m_chipCfgs[number].writeRegGlobal(Fei3::GlobReg::EnableIPMonitor, 0);

	    auto const & enablePixReg = fei3->readPixRegister(Fei3::PixReg::Enable);
	    auto const & selectPixReg = fei3->readPixRegister(Fei3::PixReg::Select);
	    auto const & preampPixReg = fei3->readPixRegister(Fei3::PixReg::Preamp);
	    auto const & hitbusPixReg = fei3->readPixRegister(Fei3::PixReg::HitBus);
	    auto const & tdacPixReg = fei3->readTrim(Fei3::PixReg::TDAC);
	    auto const & fdacPixReg = fei3->readTrim(Fei3::PixReg::FDAC);

            for (unsigned col = 0; col < Fei3::nPixCol; col++) {
                for (unsigned row = 0; row < Fei3::nPixRow; row++) {
			feMod.m_chipCfgs[number].writeBitPixel(Fei3::PixLatch::Enable, row, col, enablePixReg.get(col, row));
			feMod.m_chipCfgs[number].writeBitPixel(Fei3::PixLatch::Select, row, col, selectPixReg.get(col, row));
			feMod.m_chipCfgs[number].writeBitPixel(Fei3::PixLatch::Preamp, row, col, preampPixReg.get(col, row));
			feMod.m_chipCfgs[number].writeBitPixel(Fei3::PixLatch::HitBus, row, col, hitbusPixReg.get(col, row));
			feMod.m_chipCfgs[number].writeTDACPixel(row, col, tdacPixReg.get(col, row));
			feMod.m_chipCfgs[number].writeFDACPixel(row, col, fdacPixReg.get(col, row));
		}
	    }
        }
        catch(...){
            throw BarrelRodPixControllerExc(BarrelRodPixControllerExc::BAD_CONFIG, PixControllerExc::ERROR,  
                    getModGroup().getRodName(), m_ctrlID);
        }
    }//FE loop

#ifndef __MCC_TEST__
    WriteConfig<Fei3ModCfg> writeCfg;
    writeCfg.setCalibConfig();
    writeCfg.setFeToWrite(moduleId); // Todo: check that moduleid < 26 for PIX (should be)
    writeCfg.moduleConfig = feMod;

    if(!forceWrite) {
        bool configsMatch  = compareRODHostHash(moduleId, GetFEConfigHash::fei3, feMod);
        if(!configsMatch) {
            std::cout << "Since the configs mismatch, writing new config for module " << static_cast<int>(moduleId) << std::endl;
            sendCommand(writeCfg);
        } else {
            std::cout << "Since the configs are the same, skipped writing config for module " << static_cast<int>(moduleId) << std::endl;
        }
    } else {
        sendCommand(writeCfg);
    }

#endif

      if( m_modPosition[moduleId] >= 0){
        m_moduleRx[moduleId] = feMod.m_rxCh; // store module rx for later use
        //Set alternative link
        std::cout << __PRETTY_FUNCTION__ << " done: " << moduleId << "/" << m_moduleRx[moduleId] << std::endl;
      }
}

void BarrelRodPixController::readModuleConfigIdentifier(int moduleId, char* idStr, char* tag, uint32_t* revision, uint32_t* crc32chksm) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    *revision = 1; *crc32chksm = 1; //  These variables need to be initialized to something !!!
}

void BarrelRodPixController::sendModuleConfig(unsigned int moduleMask, unsigned int bitMask) {            //! Send module configuration
    if (m_ctrlVerbose) std::cout << __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (m_ctrlVerbose) std::cout << " received moduleMask : " << HEX(moduleMask) << std::endl;

    SendModuleConfig sendCfg; //SendModuleConfig command
    sendCfg.turnOnPreAmps(); // send with preAmps ON
    sendCfg.setCalibConfig();
    sendCfg.setModMask(moduleMask); // which channels to send to
    sendCfg.setSendAll(true); // true: send global + pixel regs, false: send global regs only
    //sendCfg.setSendAll(false); // Do not send pixel configuration
    //sendCfg.m_maxGlobalThreshold = true;
    sendCommand(sendCfg);

    m_preAmpOff = false;
    outputMessageForConfig(sendCfg);
}

void BarrelRodPixController::sendModuleConfigWithPreampOff(unsigned int moduleMask, unsigned int bitMask) {            //! Send module configuration

    std::vector<std::string> maskPixelStandBy ={//Hard-coded default
      "L0_B03_S1_C7_M5C","L0_B03_S2_A7_M5A","L0_B04_S1_A6_M4A","L0_B05_S1_A6_M6A","L0_B07_S1_A6_M6A",
      "L0_B07_S1_C7_M4C","L0_B09_S2_A7_M0" ,"L0_B09_S2_A7_M6A","L0_B10_S2_A7_M0" ,"L0_B11_S1_A6_M4A",
      "L0_B11_S1_A6_M5A","L0_B11_S1_A6_M6A","L0_B11_S2_C6_M5C","L0_B11_S2_C6_M2C","L0_B01_S1_C7_M2C",
      "L0_B01_S1_C7_M3C","L0_B01_S1_C7_M4C","L0_B01_S2_C6_M5C","L0_B02_S2_C6_M1C","L0_B03_S2_C6_M3C",
      "L0_B04_S1_C7_M1C","L0_B04_S2_A7_M4A","L0_B05_S1_A6_M3A","L0_B09_S1_A6_M5A","L0_B10_S1_A6_M6A",
      "L0_B11_S1_C7_M3C","L0_B11_S2_C6_M6C","L2_B05_S1_C7_M4C","L0_B11_S1_A6_M3A","L0_B04_S1_A6_M1A"
    };

    if( getModGroup().isManager()->exists("PIX_MODULES_MASK_Standby" ) ) {//In case is overwritten by IS
     std::string isValue = getModGroup().isManager()->read<std::string>("PIX_MODULES_MASK_Standby");
     PIX_INFO("PIX_MODULES_MASK_Standby overwritten in IS"); 
     maskPixelStandBy.clear();
     stringstream ss(isValue);
       while (ss.good()) {
        std::string modName;
        getline(ss, modName, ',');
        maskPixelStandBy.emplace_back(modName);
      }
    }

  uint32_t maskPixelEna = 0x0;
    for(size_t i=0;i<maskPixelStandBy.size();i++){
      maskPixelEna |=  getModGroup().getModMaskPosition(maskPixelStandBy[i]);
    }

    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
  //this->sendModuleConfig(moduleMask, bitMask);
  SendModuleConfig sendCfg; //SendModuleConfig command
  sendCfg.turnOffPreAmps(); // send with preAmps OFF
  sendCfg.setCalibConfig();
  sendCfg.setModMask(moduleMask); // which channels to send to
  sendCfg.setSendAll(true); // true: send global + pixel regs, false: send global regs only
  //sendCfg.setSendAll(false); // Do not send pixel configuration
  //sendCfg.m_maxGlobalThreshold = true;
  sendCfg.setEnaMaskStandby(maskPixelEna);
  sendCommand(sendCfg);

  m_preAmpOff = true;
  outputMessageForConfig(sendCfg);

    if (maskPixelEna) std::cout<<getModGroup().getRodName()<< " masking Pixels in Stand-by with mask 0x"<<std::hex<<maskPixelEna<<std::dec <<std::endl;
}

void BarrelRodPixController::outputMessageForConfig(SendModuleConfig &cmd){

  uint32_t enabledMask = (cmd.result.enabledMask);
  uint32_t readBackOKMask = (cmd.result.readBackOKMask);

  std::stringstream msg_stream;

    if (readBackOKMask == (enabledMask)) {
      msg_stream<<"Successfully read back FE Global config for Rx mask: 0x"<<std::hex<<getRxMaskFromModuleMask(enabledMask)<<std::dec<<std::endl;
      ers::info(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
    } else {
      msg_stream<<"Could not succesfully read back FE config for Rx mask: 0x"<<std::hex<<getRxMaskFromModuleMask(enabledMask)<<", could only readback correctly for: 0x"<<std::hex<<getRxMaskFromModuleMask(readBackOKMask)<<std::endl;
      ers::warning(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg_stream.str()));
    }

}


void BarrelRodPixController::resetModules(uint32_t moduleMask, uint32_t syncW) {            //! ResetModules 

      DataTakingTools tool;
      tool.action = ResetModules;
      tool.channels = moduleMask;
      tool.value = syncW;
      sendCommand(tool);
}

void BarrelRodPixController::dumpRodBusyStatus( ){

ReadRodSlaveBusyStatus slaveBusyStatus;
slaveBusyStatus.dumpOnPPC = false;
ReadRodMasterBusyStatus masterBusyStatus;
masterBusyStatus.dumpOnPPC = false;
sendCommand(masterBusyStatus);

//Slave A
slaveBusyStatus.targetIsSlaveB = false;
sendCommand(slaveBusyStatus);

uint32_t slaveABusy = slaveBusyStatus.result.slaveBusyCurrentStatusValue;
uint32_t slaveABusyMask = slaveBusyStatus.result.slaveBusyMaskValue;
//Slave B
slaveBusyStatus.targetIsSlaveB = true;
sendCommand(slaveBusyStatus);

//TO-DO compute hist?
std::cout<<"ROD Busy status "<<getModGroup().getRodName()<<std::endl;
std::cout<<" Master busy status 0x"<<std::hex<<masterBusyStatus.result.masterBusyCurrentStatusValue<<std::dec<<std::endl;
std::cout<<" SlaveA busy status 0x"<<std::hex<<slaveABusy<<std::dec<<std::endl;
std::cout<<" SlaveB busy status 0x"<<std::hex<<slaveBusyStatus.result.slaveBusyCurrentStatusValue<<std::dec<<std::endl;

std::cout<<" SlaveA busy mask 0x"<<std::hex<<slaveABusyMask<<std::dec<<std::endl;
std::cout<<" SlaveB busy mask 0x"<<std::hex<<slaveBusyStatus.result.slaveBusyMaskValue<<std::dec<<std::endl;

}

void BarrelRodPixController::setConfigurationMode() {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (m_mode != "CONFIGURATION") {
    /*// Karolos: should be enabled at some point
    DataTakingTools cmdDisableFmtLink;
    cmdDisableFmtLink.channels = 0xFFFFFFFF;
    sendCommand(cmdDisableFmtLink);*/
    /*// Karolos: commented out because it was doing nothing before the adjustments to disableLink
        for (int i=0; i<32; i++) {
            disableLink(moduleOutLink(i));
        }*/
        // Todo: Karolos -- Check config mode from RodController and see what's missing
    }
    m_mode = "CONFIGURATION";
}

void BarrelRodPixController::disableLink(int link) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    // We know that the link is the RxCh of the first FE
    uint32_t effectiveMask = 1<<link;
    std::cout << "Disabling FE with mask " << HEXF(8,effectiveMask) << std::endl;
    //bool isSlaveB = false;
    //if(link>15) isSlaveB = true;

    DataTakingTools cmdDisableFmtLink;
    cmdDisableFmtLink.action = DisableFmtLink;
    cmdDisableFmtLink.channels = effectiveMask;
    sendCommand(cmdDisableFmtLink);

 m_autoDisabledLinks[link] = 1;

}

void BarrelRodPixController::disableSLink( ){

   if(getModGroup().rodDisabled())return;
   // read back current fmt link enable
   DumpRodRegisters dump;
   dump.dumpOnPPC = false;
   sendCommand(dump);

   uint32_t fmtALinkEnable = dump.result.FmtLinkEnableValue_A;
   uint32_t fmtBLinkEnable = dump.result.FmtLinkEnableValue_B;
   uint32_t enabledMask = ((fmtBLinkEnable << 16) | fmtALinkEnable);

   std::string RODNAME = getModGroup().getRodName();
   std::size_t pos = RODNAME.find("_ROD");
   RODNAME = RODNAME.substr(0,pos);
    if(enabledMask == 0x0 ){//All links disabled, disable ROD
       getModGroup().sendDisableMessage(RODNAME,"ROD");
       getModGroup().setRodDisableReason("Z");
    } else{
      if((enabledMask & 0x0000FFFF) ==0 && (m_runRxMask & 0x0000FFFF) !=0){//Disable NORTH
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(0))){
          std::cout << "All formatters associated to S-LINK 0 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(0),"ROD");
        }
      }
      if((enabledMask & 0xFFFF0000) ==0 && (m_runRxMask & 0xFFFF0000) !=0){//Disable SOUTH
        if(!getModGroup().slinkDisabled(RODNAME + "_"+std::to_string(1))){
          std::cout << "All formatters associated to S-LINK 1 are disabled. The link will be disabled..." << std::endl;
          getModGroup().sendDisableMessage(RODNAME + "_"+std::to_string(1),"ROD");
        }
      }
    }

}

void BarrelRodPixController::enableLink(int link) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

}


void BarrelRodPixController::writeScanConfig(PixScan &scn) {  //! Write scan parameters
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    m_scanLoopParam0 = scn.getLoopParam(0);
    uint32_t scanModuleMask = 0;
    for(int i = 0; i < 4; i++) {
      uint32_t sMM = scn.getModuleMask(i);
      std::cout << "ModuleMask " << i << " " << HEX(sMM) << std::endl;
      scanModuleMask |= sMM;
    }
    if (m_ctrlVerbose) std::cout << "the getModuleMask from PixScan is: " << HEX(scanModuleMask) << std::endl;
    m_scanRxMask = 0;
    for (int i = 0; i < MAX_MODULES; i++) {
        if (scanModuleMask & (0x1 << i)) {
           if (0 <= m_moduleRx[i] && m_moduleRx[i] < 32) {
              m_scanRxMask |= (0x1 << m_moduleRx[i]);
            }
        }
        else m_modActive[i]  = false; // to-do: check that modActive should be false when the module is not in the scan
        // seems to be that way because something in PixScan called modActive when looking for histos....
    }
    if (m_ctrlVerbose) std::cout << "the fe rx channel map I built from the pixscan mask is: " << HEX(m_scanRxMask) << std::endl;

    // define ROD name in format needed
    std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

    // publish to IS for fit server to pick up the scan rx channel mask and type of FE
    IPCPartition partition(getenv("TDAQ_PARTITION"));
    ISInfoDictionary dict(partition);
    string srvname("Histogramming"); // TODO: no hardocoding
    ISInfoInt maskInfo (m_scanRxMask);
    dict.checkin( srvname + "." + RODNAME + "_ScanModuleMask", maskInfo );

    ISInfoInt feFlavour = (ISInfoInt)m_flavour;
    dict.checkin( srvname + "." + RODNAME + ".FEflavour", feFlavour);

    //PF Re-inserted the StrobeLVL1DelayOverride here to harmonise with what is done for IBL and old-readout
    //Not sure if there is a better way to do this.
    if (!scn.getStrobeLVL1DelayOverride())
      scn.setStrobeLVL1Delay(((ConfInt&)getModGroup().config()["general"]["TriggerDelay"]).getValue());
    
    std::cout<<"Using Alternative Module Links if possible? "<<scn.getUseAltModLinks()<<std::endl;
    

    // To-do: clean up casting
    PixScanBase * scnBase = &scn;
    SerializablePixScanBase serScnBase( static_cast<PixScanBase>(*scnBase) );

    PixScanBaseToRod pixScanBaseToRod; //same as  writeScanConfig command
    pixScanBaseToRod.setPixScanBase(serScnBase);

    sendCommand(pixScanBaseToRod);

    // save cap type for later if doing threshold scan; this is ugly; to-do: figure out a better way
    capType = scn.getChargeInjCapHigh();
}

void BarrelRodPixController::startScan() {                      
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<<std::endl;
    
// Can create race condition with the start of the scan. Enable when reset available via HCP
//    resetFPGA();
    
    m_scanActive = true;
    m_histosTimedOut = false; // reset the timeout on getting the histos from the fitserver

    // setting up IS stuff
    IPCPartition partition(getenv("TDAQ_PARTITION"));
    ISInfoDictionary dict(partition);
    string srvname("Histogramming"); // TODO: no hardocoding

    // define ROD name in format needed
    std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);

    // publishing startscan flag
    ISInfoString isgo(RODNAME);
    try {
        dict.checkin( srvname + "." + RODNAME + "_StartScan", isgo );
    } catch(...) {
        ERS_INFO("Dictionary checkin failed for Rod " << RODNAME);
    }
    ERS_LOG("Successfully checked Rod " << RODNAME << " into IS");

    // get scan ID and fit server port from IS
    ISInfoInt scanid;
    ISInfoString s0;
    ISInfoString s1;
    ISInfoString s2;
    ISInfoString s3;
    try{
        dict.getValue(srvname + "." + RODNAME + "_ScanId", scanid); 
        dict.getValue(srvname + "." + RODNAME + "_0_0", s0);
        dict.getValue(srvname + "." + RODNAME + "_0_1", s1);
        dict.getValue(srvname + "." + RODNAME + "_1_0", s2);
        dict.getValue(srvname + "." + RODNAME + "_1_1", s3);
    }
    catch(...) {
        std::string msg = "Could not get scanId and/or fitServerPort from Histogramming IS.";
        std::cerr << msg << std::endl;
        std::cerr<<"check that the fit server is running"<<std::endl;
        ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
    }
    if (m_ctrlVerbose) std::cout<<" Got scan id: "<< (int)scanid << " and fit server ip:port combinations from IS: \n"
        << (std::string)s0 << "\n" 
            << (std::string)s1 << "\n" 
            << (std::string)s2 << "\n" 
            << (std::string)s3 << "\n" << std::endl;

    StartScanL12 startScan; // start scan command
    startScan.channels = m_scanRxMask;
    startScan.scanId = scanid;

    for (int i = 0; i<4; i++) {
        std::stringstream s;
        if(i == 0) s.str(s0);
        if(i == 1) s.str(s1);
        if(i == 2) s.str(s2);
        if(i == 3) s.str(s3);
        int a,b,c,d,e;
        char ch;
        s >> a >> ch >> b >> ch >> c >> ch >> d >> ch >> e;
        startScan.fitPorts[i] = e;
        startScan.fitIps[i][0] = a;
        startScan.fitIps[i][1] = b;
        startScan.fitIps[i][2] = c;
        startScan.fitIps[i][3] = d;
    }
    //  Without GetStatus all status variables will remain as initialized here until the scan is done.
#ifdef __DO_GET_STATUS__
    m_scanResult.state = SCAN_IDLE;
#else
    m_scanResult.state = SCAN_IN_PROGRESS;
#endif
    m_scanResult.errorState = PPCERROR_NONE;
    m_scanResult.mask =  0;
    m_scanResult.par = 0;
    m_scanResult.par1 = 0;
    m_scanResult.par2 = 0;
    m_scanResult.dc = 0;
    m_scanResult.trig = 0;

    sendCommand(startScan);

#ifndef __DO_GET_STATUS__
    m_scanResult = startScan.result.m_scanResult;
#endif

    // Store IDs in case it needs to be stopped.
    m_scanId = scanid;
    m_scanTypeId = startScan.getTypeId();
    m_scanExecId = startScan.getExecId();
    // Reset error publishing flags
    m_errPublishedConn = 0;
    m_errPublishedResponding = 0;
    m_errPublishedChannels = 0;
}

bool BarrelRodPixController::moduleActive(int nmod) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    if(nmod>=0 && nmod<32) {
        std::cout << "ModuleAct? " <<m_modActive[nmod] << std::endl;
        return m_modActive[nmod];
    }

    else return false;
}

void BarrelRodPixController::getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {  //! Read a histogram
    // if not specified, use loop -1 to not get any loop
    this->getHisto(type, xmod, slv, his, -1);
}

void BarrelRodPixController::getMonLeakscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {

std::cout << "Getting MonLeak histogram for Module"<<(int)mod << std::endl;

if (type != MONLEAK_HISTO)return;

    while (his.size() < 32) {
      std::vector< Histo * > vh;
      //vh.clear();
      his.push_back(vh);
    }
    if (mod >= 32)  {
      std::cout << "ATTENTION: "<< __PRETTY_FUNCTION__ <<" received module "<<mod<<std::endl;
      return;
    }

    unsigned rxCh = m_moduleRx[mod];


    GetDSPResults getDSP; // get MonLeak results
    getDSP.rxCh = rxCh;
    sendCommand(getDSP);

    std::ostringstream nam, tit;//Name and title for the histo
    nam << "MonLeak_Scan" <<  mod    << "_" << 0;//bin==0
    tit << "I(*125 pA), mod " << mod;
    Histo *h;
    h = new Histo(nam.str(), tit.str(),2, 144,-0.5,143.5,320,-0.5,319.5); 
    his[mod].clear();
    
    uint32_t nPixPerFE = getDSP.result.dspResults.size();
    int col, row;
    
    for (uint32_t pix=0; pix<nPixPerFE; pix++) {//Loop over selected pixels, number of Pixels per FE
      for (uint32_t iFE=0; iFE<16; iFE++) {//Loop over FE, from 0 to 16, should be always there

       if(getRowColFromPixelIndex(pix, iFE, row, col) )h->set(col,row,getDSP.result.dspResults[pix][iFE]);
      
      }
    }

    his[mod].push_back(h);
    
    CleanDSPResults cleanDSP; // Clean up PPC
    cleanDSP.rxCh = rxCh;
    sendCommand(cleanDSP);

}

bool BarrelRodPixController::getRowColFromPixelIndex(int index, int FE, int &row, int &col){

if (index >= 2880) return false;

 col = index/160;//Get col from Pixel index
 row = index%160;//Get row from Pixel index

 if(col%2 ==1)row = 159-row;//Invert row if odd column due to the mask step iteration (moving along DC)

 if(FE>7){//FE > 7 is inverted in row and column
  col = 18*(16-FE) - (col+1);
  row = 319-row;
  } else {
  col += 18*FE;//Adding FE offset 
  }

return true;

}


void BarrelRodPixController::getOptoscanHisto(HistogramType type, unsigned int mod, unsigned int slv, std::vector< std::vector<Histo*> >& his) {  //! Read optocan histograms directly from ROD
 
  std::cout << "Getting OptoScan histogram for Module"<<(int)mod << std::endl;
 
  if (type == RAW_DATA_DIFF_2) {
    
    while (his.size() < 32) {
      std::vector< Histo * > vh;
      //vh.clear();
      his.push_back(vh);
    }
    if (mod >= 32)  {
      std::cout << "ATTENTION: "<< __PRETTY_FUNCTION__ <<" received module "<<mod<<std::endl;
      return;
    }
   
    unsigned rxCh = m_moduleRx[mod];
    
    // Get histogram from the ROD
    GetDSPResults getOpto; // get opto command
    getOpto.rxCh = rxCh;
    sendCommand(getOpto);

    uint32_t n_delay = getOpto.result.dspResults.size(); // Number of steps of the opto-scan
    uint32_t n_thr = getOpto.result.dspResults[0].size(); // Number of steps of the opto-scan    
    //FM int ilnk = mod; // BOC link number correspondig to module mod
    int ilnk = 0; //First link
    std::cout<<"nThr "<<n_thr<<" nDelay "<<n_delay<<std::endl;

    std::ostringstream nam, tit;
    nam << "RAW_2_" << mod << "-Link" << ilnk << "_0";
    tit << "Raw data 2 mod " << mod << "-Link" << ilnk << " L2=0";
    Histo *h;
    h = new Histo(nam.str(), tit.str(), n_delay, -0.5, n_delay-0.5, n_thr, -0.5, n_thr-0.5); 
    his[mod].clear();

    for (uint32_t i=0; i<n_delay; i++) {
      for (uint32_t j=0; j<n_thr; j++) {
        h->set(i,j,getOpto.result.dspResults[i][j]);
      }
    }
    his[mod].push_back(h);
    
    std::cout << "Histo data s1 (thr) " << (int)n_thr << " s2 (delay) "<< (int)n_delay << " ilink " << ilnk << std::endl;
    std::cout << "Vector size " << getOpto.result.dspResults.size() << std::endl;
    
    CleanDSPResults cleanOpto; // Clean up PPC
    cleanOpto.rxCh = rxCh;
    sendCommand(cleanOpto);

    rxCh = getModGroup().getAltRxChannel(mod);//Get alternative Rx channel for module
    if( rxCh<0 || rxCh>31)return;//No valid rxCh, so no second link
    
    getOpto.rxCh = rxCh;
    sendCommand(getOpto);

    //Second link FTW
    ilnk = 1; //Second link

    n_delay = getOpto.result.dspResults.size(); // Number of steps of the opto-scan
    n_thr = getOpto.result.dspResults[0].size(); // Number of steps of the opto-scan
    
    std::ostringstream namAlt, titAlt;   
    namAlt << "RAW_2_" << mod << "-Link" << ilnk << "_0";
    titAlt << "Raw data 2 mod " << mod << "-Link" << ilnk << " L2=0";
    Histo *hAlt;
    hAlt = new Histo(namAlt.str(), titAlt.str(), n_delay, -0.5, n_delay-0.5, n_thr, -0.5, n_thr-0.5); 

    for (uint32_t i=0; i<n_delay; i++) {
      for (uint32_t j=0; j<n_thr; j++) {
        hAlt->set(i,j,getOpto.result.dspResults[i][j]);
      }
    }
    his[mod].push_back(hAlt);
    
    std::cout << "Histo data s1 (thr) " << (int)n_thr << " s2 (delay) "<< (int)n_delay << " ilink " << ilnk << std::endl;
    std::cout << "Vector size " << getOpto.result.dspResults.size() << std::endl;

    // Clean up PPC
    cleanOpto.rxCh = rxCh;
    sendCommand(cleanOpto);

  }
}

void BarrelRodPixController::getHisto(HistogramType type, unsigned int xmod, unsigned int slv, std::vector< std::vector<Histo*> >& his, int loopStep) {  //! Read a histogram
  //todo better description 
  std::string RODNAME = (getModGroup().getRodName()).substr(0, (getModGroup().getRodName()).length() - 4);
  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< " for rod: "<<RODNAME<<" with loop: "<<loopStep<<" and type: "<<type<<std::endl;
  
  if (type == RAW_DATA_DIFF_2) {
      getOptoscanHisto(type, xmod, slv, his);
      return;
  }
  
  if(type ==  MONLEAK_HISTO) {
      getMonLeakscanHisto(type, xmod, slv, his);
      return;
  }
   
  //setting up IS stuff
  IPCPartition partition(getenv("TDAQ_PARTITION"));
  ISInfoDictionary dict(partition);
  string srvname("Histogramming"); // TODO: no hardocoding
  
  //check if FitServer has signalled that all histograms are available, if not keep querying IS
  ISInfoBool histoready = false; 
  dict.getValue(srvname + "." + RODNAME + "_FinishScan", histoready);
#ifdef DEBUG_ND_IS
  ERS_LOG("Got histoready(0): " << (int)histoready);
#endif
  int maxTimeOut = 200;
  int haveWaited = 0;

  if (!m_histosTimedOut && !m_errPublishedResponding) { // if already timed out waiting for histos for this scan, don't wait again
#ifdef DEBUG_ND_IS
    int wc(1);
#endif
    while(!histoready && !m_errPublishedResponding){ // wait for fit server to tell us that it's finished publishing histos
      dict.getValue(srvname + "." + RODNAME + "_FinishScan", histoready);
#ifdef DEBUG_ND_IS
      ERS_LOG("Got histoready(" << wc++ << "): " << (int)histoready);
#endif
      sleep(1); haveWaited++;
      if (haveWaited > maxTimeOut) {
	m_histosTimedOut = true;
	std::cout<<"TIMED OUT WAITING FOR "<<type<<" HISTOS FROM FIT SERVER for ROD "<<RODNAME<<std::endl;
	std::string msg = "Timed out waiting for histos from fit server... you will have (some) empty histos";
	ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
	break;
	}
    }
  }

  bool chargeConv = false; // convert x-axis in vcal to charge?
  bool delayConv = false; // convert x-axis in strobe delay to ns?
  bool isNoise = false; // flag whether scan involves noise histos
  std::string scanname, scanname1, scanname2;
  //int htype;
  PixModule *pmod=getModGroup().module(xmod);
  int nModCols = 8;
  int nModRows = 2;
  int nCol=pmod->nCol();
  int nCol_OH = nCol/nModCols;
  int nRow=pmod->nRow();
  int nRow_OH = nRow/nModRows;

  while (his.size() < 32) {
    std::vector< Histo * > vh;
    //vh.clear();
    his.push_back(vh);
  }
  switch (type) {
  case OCCUPANCY :
    scanname   = "Occup_";
    scanname1  = "Occupancy";
    scanname2  = scanname;
    //htype      = SCAN_OCCUPANCY;
    break;
  case TIMEWALK :
    scanname   = "Timew_";
    scanname1  = "Timewalk";
    scanname2  = "Thr_"; // FitServer actually thinks this is a threshold scan, so download Thr histo
    delayConv = true;
    break;
  case SCURVE_MEAN :
    scanname   = "Thr_";
    scanname1  = "Thr";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_MEAN;
    if(m_scanLoopParam0 != EnumScanParam::STROBE_DELAY) chargeConv = true;
    break;
  case SCURVE_SIGMA :
    scanname   = "Noise_";
    scanname1  = "Noise";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_SIGMA;
    if(m_scanLoopParam0 != EnumScanParam::STROBE_DELAY) chargeConv = true;
    isNoise = true;
    break;
  case SCURVE_CHI2 :
    scanname   = "Chi2_";
    scanname1  = "Chi2";
    scanname2  = scanname;
    //htype      = SCAN_SCURVE_CHI2;
    chargeConv = false;
    break;
  case TOT_MEAN :
    scanname   = "ToTMean_";
    scanname1  = "ToTMean";
    scanname2  = scanname;
    //htype      = SCAN_TOT_MEAN;
    break;
  case TOT_SIGMA :
    scanname   = "ToTSigma_";
    scanname1  = "ToTSigma";
    scanname2  = scanname;
    //htype      = SCAN_TOT_SIGMA;
    break;
  default :
    std::string msg = "You requested a histogram type not currently supported";
    std::cerr << msg << std::endl;
    ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
    break;
  }

  // store variables for calculating charge from vcal
  float vcal_a[16], vcal_b[16], vcal_c[16], vcal_d[16], cinj1[16] = {0}, delayCal=1.;
  if (chargeConv) {
    for (size_t iChip = 0; iChip < 16; iChip++) {
    if (pmod->pixFE(iChip)) {
      std::cout << " getting vcal stuff for fe " << iChip << std::endl; // tmp
      vcal_a[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["VcalGradient3"])).value();
      vcal_b[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["VcalGradient2"])).value();
      vcal_c[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["VcalGradient1"])).value();
      vcal_d[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["VcalGradient0"])).value();
      if(isNoise) vcal_d[iChip] = 0;
      cinj1[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["CInjLo"])).value();
      if (capType==1) cinj1[iChip] = (dynamic_cast<ConfFloat &>((pmod->pixFE(iChip))->config()["Misc"]["CInjHi"])).value();
    }
   }
  }
  if(delayConv){
    int delrg = pmod->pixMCC()->readRegister("CAL_Range");
    std::stringstream a;
    a<<delrg;
    delayCal = ((ConfFloat&)pmod->pixMCC()->config()["Strobe"]["DELAY_"+a.str()]).value();
  }

  std::ostringstream nam, tit;
  nam << scanname  <<  xmod    << "_" << 0;
  tit << scanname1 << " xmod " << xmod;// << " bin " << 0;

  if ( loopStep != -1) nam << "_"<< std::to_string(loopStep);

  //std::vector< Histo * > h;
  if (m_ctrlVerbose) std::cout << "nam = " << nam.str() << ", scanname = " << scanname << ", scanname1 = " << scanname1 << ", scanname2 = " << scanname2 << ", xmod = " << xmod << std::endl;
  
    
  //getting FitServer histograms from OH
  his[xmod].clear();
  //h.clear();

  ISInfoInt scanid;
  int sid = m_scanId;
  try{
    dict.getValue(srvname + "." + RODNAME  + "_ScanId", scanid);
    sid = scanid;
  }
  catch(...){
    std::cout<<"DEBUG::Exception caught while getting the scan ID"<<std::endl;
    std::cout<<"DEBUG::Using scanId="<<m_scanId<<std::endl;
  }
    
  int rx = getModuleRx(xmod);

  std::cout<<"for module with id: "<<(int)xmod<<" got rx = "<<rx<<" and ROD: "<<RODNAME<<std::endl;

  std::string h1WildCard = "/" + boost::lexical_cast<std::string>(sid) + "/" + RODNAME + "/MCC" + boost::lexical_cast<std::string>(rx) + "/Chip.*/" + scanname2 + ".*";

  std::cout<<"h1WildCard: " << h1WildCard << " --> tit.str(): " << tit.str() << std::endl;

  OHHistogramIterator ith1(partition, srvname, ".*", h1WildCard);
  //  std::vector< std::shared_ptr<Histo> > htmp;

  Histo *h_full = new Histo(nam.str(), tit.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
  std::map<int, Histo*> h_map;
  h_map[-1] = h_full;

  if (rx >= 0){
    while(ith1++) {

      for(int xmodCol = 0; xmodCol < nModCols; xmodCol++) {
        for(int xmodRow = 0; xmodRow < nModRows; xmodRow++) {
          try {

            std::cout<<"Hello Doug we are getting histo for Rx: "<<(int)rx<<std::endl;
            std::string provName = ith1.provider();
            std::string histoName = ith1.name();
            if (m_ctrlVerbose) std::cout <<"histo to be read from OH ==>> "<< "srvname: " << srvname << "  provName: " << provName << "  histoName: " << histoName << std::endl;
	    int histoLoop = -1;
	    if (histoName.find("-")!=std::string::npos) {
	      //int histoLoop = std::stoi(histoName.substr(histoName.length()-1,1));
	      histoLoop = std::stoi(histoName.substr(histoName.find("-")+1, histoName.length()-histoName.find("-")-1));
	      if (loopStep != -1 && histoLoop != loopStep) continue;
            }
	    if(histoLoop!=-1){
	      std::cout <<"this histo type was looped over - now at index " << histoLoop << std::endl;
	      if(h_map.find(histoLoop)==h_map.end()){
		std::ostringstream nam2, tit2;
		nam2 << scanname  <<  xmod    << "_" << histoLoop;
		tit2 << scanname1 << " xmod " << xmod << " bin " << histoLoop;
		h_full = new Histo(nam2.str(), tit2.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
		h_map[histoLoop] = h_full;
		std::cout << "created new histo " << h_full->name() << std::endl;
	      } else
		h_full = h_map[histoLoop];
	      std::cout << " -> filling histo " << h_full->name() << std::endl;
	    } else
	      h_full = h_map[histoLoop];

            OHRootHistogram hist=OHRootReceiver::getRootHistogram(partition, srvname, provName, histoName, -1);

            size_t stc_iChip = 4 + histoName.rfind("Chip");
            size_t szc_iChip =  histoName.rfind("/") - stc_iChip;
            int iChip = std::stoi(histoName.substr(stc_iChip, szc_iChip));
            int modCol = iChip % 8;
            if(iChip > 7) modCol = 7 - modCol; //  Top row of FEs ordered right-to-left !!
            int modRow = iChip / 8;
            for(int xc = 0; xc < nCol_OH; xc++) {
              int colOffset = modCol * nCol_OH + xc;
              if(iChip > 7) colOffset = modCol * nCol_OH + nCol_OH - xc - 1; //  Columns of top row of FEs ordered right-to-left !!
	      for(int xr = 0; xr < nRow_OH; xr++) {
                int rowOffset = modRow * nRow_OH + xr;
                if(iChip > 7) rowOffset = modRow * nRow_OH + nRow_OH - xr - 1; //  Rows of top row of FEs ordered top-to-bottom !!
	        if (chargeConv) {
	          float thr = getModGroup().getChargefromVCAL(hist.histogram->GetBinContent(xc+1,xr+1),
	                                                               vcal_a[iChip], vcal_b[iChip],
	                                                               vcal_c[iChip], vcal_d[iChip], cinj1[iChip]);
#ifdef DEBUG_ND
                  if(xmod == 0)std::cout << "iChip = " << iChip << ", xmodCol = " << xmodCol << ", xmodRow = " << xmodRow << ", modCol = " << modCol <<
                              ", modRow = " << modRow << ", xc = " << xc << ", colOffset = " << colOffset << ", xr = " << xr <<
                              ", rowOffset = " << rowOffset << ", Contents = " << hist.histogram->GetBinContent(xc+1,xr+1) <<
                              ", chargeConv = " << chargeConv << ", vcal_a[iChip] = " << vcal_a[iChip] << ", vcal_b[iChip] = " << vcal_b[iChip] <<
                              ", vcal_c[iChip] = " << vcal_c[iChip] << ", vcal_d[iChip] = " << vcal_d[iChip] <<
                              ", capType = " << capType << ", cinj1[iChip] = " << cinj1[iChip] << ", thr = " << thr << std::endl;
#endif
	          if(hist.histogram->GetBinContent(xc+1,xr+1) < 0)  h_full->set(colOffset,rowOffset,0); // fill 0 -> fit failed
	          else if(hist.histogram->GetBinContent(xc+1,xr+1) == 0) continue; // don't fill, pixel that wasn't scanned
	          else h_full->set(colOffset,rowOffset,thr);
	          //	      std::cout<<" DEBUG: got threshold: "<<thr<<" e-"<<std::endl;
	        }
		else if(delayConv){
                  h_full->set(colOffset,rowOffset, delayCal*hist.histogram->GetBinContent(xc+1,xr+1));
		}
	        else {
                  h_full->set(colOffset,rowOffset, hist.histogram->GetBinContent(xc+1,xr+1));
#ifdef DEBUG_ND
                  std::cout << "iChip = " << iChip << ", xmodCol = " << xmodCol << ", xmodRow = " << xmodRow << ", modCol = " << modCol <<
                               ", modRow = " << modRow << ", xc = " << xc << ", colOffset = " << colOffset << ", xr = " << xr <<
                               ", rowOffset = " << rowOffset << ", Contents = " << hist.histogram->GetBinContent(xc+1,xr+1) <<
                               ", chargeConv = " << chargeConv << std::endl;
#endif
                }
	      } //  xr
            } //  xc
          } catch (const daq::oh::ObjectNotFound &) {
            //std::string mess = "Object not found in OH ";
            std::cout << "Object not found in OH " << std::endl;
          } catch (const daq::oh::InvalidObject &) {
            //std::string mess = "Invalid Object ";
            std::cout << " Invalid Obj " << std::endl;
          } catch (...) {
            //std::string mess = "Unknown exception thrown ";
          }
          if(xmodCol < nModCols - 1 || xmodRow < nModRows - 1)ith1++;
        } //  modRow
      } //  modCol
    }
  }

  // h.push_back(h_full);
  
  // for(unsigned int step=0; step<h.size(); step++){
  //   his[xmod].push_back(h[step]);}
  if(h_map.size()==1)
    his[xmod].push_back(h_map[-1]);
  else{
    int maxId = -1;
    for(std::map<int, Histo*>::iterator it=h_map.begin(); it!=h_map.end(); it++)
      if(it->first>maxId) maxId = it->first;
    maxId++;
    for(int step=0; step<maxId; step++){
      if(h_map.find(step)!=h_map.end())
	h_full = h_map[step];
      else{ // missing a histogram - fill default empty Histo
	std::ostringstream nam2, tit2;
	nam2 << scanname  <<  xmod    << "_" << step;
	tit2 << scanname1 << " xmod " << xmod << " bin " << step << " - EMPTY";
	h_full = new Histo(nam.str(), tit.str(),4, nCol, -0.5, (float)nCol-0.5f,nRow, -0.5, (float)nRow-0.5);
      }
      his[xmod].push_back(h_full);
    }
    // remove default Histo, not used
    h_full = h_map[-1];
    h_map[-1] = 0;
    delete h_full;
  }

}

void BarrelRodPixController::writeRunConfig(PixRunConfig* run) { //! Sets the run configuration parameters
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    
    DataTakingConfig& cmd = m_dtConfig;
    // Putting the ROD in configuration mode
    setConfigurationMode();
    // Setting the ROD Offline ID
    std::string offlineId = (getModGroup().rodConnectivity())->offlineId();
    uint32_t rodOfflineId = 0;
    std::istringstream str(offlineId);
    str >> std::hex >> rodOfflineId;

    uint32_t runMask = 0x00000000;
    uint32_t activeModMask = getModGroup().getActiveModuleMask();

    for (int i = 0; i < MAX_MODULES; i++) {
      std::cout << "Module number: " << i << ", rxCh=" << m_moduleRx[i] << std::endl;
      if( activeModMask & (0x1 << i) ) {
        std::cout << "Module is in active mask" << std::endl;
        runMask |= (0x1 << m_moduleRx[i]);
      }
    }
    std::cout << __PRETTY_FUNCTION__ << ": runMask=" << HEX( runMask ) << std::endl;

    std::cout << "for " << getModGroup().getRodName() << " runMask = " << HEX(runMask) << std::endl;

    //    uint32_t timeout = 0x500; //defaul values for Ly1Ly2 (80 Mhz Clock unit)
    // uint32_t timeout = 0x1F40; //changing default values for Ly1Ly2 (8000 at 80 Mhz Clock unit) ==>4000 BC)
    //uint32_t timeout = 0x36b0; //changing default values for Ly1Ly2 (14000 at 80 Mhz Clock unit) ==>7000 BC)
    uint32_t timeout = 0x7530; //PIT Value = 0x7530 changing default values for Ly1Ly2 (30000 at 80 Mhz Clock unit) ==>15000 BC)
 
   //MB TEST --A side(Ly2) --C Side (Ly1) with timeout settings x9C40 (80 Mhz Clock unit)
   // if (getModGroup().getRodName() == "ROD_L1_S5_ROD") timeout=0x9C40; //20000BC
   // if (getModGroup().getRodName() == "ROD_L1_S7_ROD") timeout=0x9C40;
   //...
   // if (getModGroup().getRodName() == "ROD_L1_S20_ROD") timeout=0x9C40;
   // if (getModGroup().getRodName() == "ROD_L2_S5_ROD") timeout=0x9C40;
   //...
   // if (getModGroup().getRodName() == "ROD_L2_S20_ROD") timeout=0x9C40;



    uint32_t rodbusylimit = 0x776; //this corresponds to 1910 fifo words.. it was 0x700 in 2017 before Nico suggested new values 

    /*    if (getModGroup().getRodName() == "ROD_L2_S5_ROD")  rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S7_ROD")  rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S9_ROD")  rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S11_ROD") rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S14_ROD") rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S16_ROD") rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S18_ROD") rodbusylimit=0x700;
    if (getModGroup().getRodName() == "ROD_L2_S20_ROD") rodbusylimit=0x700;
    */

    cmd.rodID = rodOfflineId;
    cmd.channels = runMask; // enable / configure all channels
    cmd.physicsMode = true; // put the rod in physics mode


    cmd.fmtReadoutTimeoutLimit = timeout; // how long fmt links wait for data on a single channel (in 80 mhz clock)
    cmd.fmtHeaderTrailerLimit  = 0x700; // PIT Value= 0x700 how many data-records to process per frame
    cmd.fmtRodBusyLimit = rodbusylimit; // how full the fifo is before asserting busy (0x320 = 800 data-records)
    cmd.fmtDataOverflowLimit =   0x320; // PIT Value= 0x320 cut on the event size ?? very big..
    cmd.fmtTrailerTimeoutLimit = 0xf50; // PIT value = DEF value = 0xf50 ; I guess if no Trailer is identified the ROD close the event after a while...

    cmd.vetoAtECR = 0x1fa4;     // 0x1fa4 = 820 microS   --> Fast Command Veto time at ECR (100 ns time unit)
    cmd.busyAtECR = 0x2328;     // 0x2328 = 900  microS --> Busy time at ECR (100 ns time unit)  ==>  lowering it to 0 since CTP seems quite reliable; however monitoring of the counter needed
                                // needed for SR1 since there is only 500 microsecond window vetoed by LTP
    cmd.resetAfterECR = 0x1f40; // 0x1f40 = 800 microS --> Waiting time after ECR before the ROD internal FPGA ECR Reset is performed (100 ns time unit); the reset last 8 BC



    cmd.slinkMode = SlinkIgnoreFTK; // SlinkIgnoreFTK, SlinkEnableFTKXOff, SlinkEnableFTKLDown, SlinkEnableFTKBoth // Default mode if not specified in the Controller: SlinkIgnoreFTK
    cmd.frameCount = run->consecutiveLVL1(); // how many frames per trigger
    //if (getModGroup().getRodName() == "ROD_C1_S7_ROD") cmd.frameCount = 2;
    cmd.trigLatency = run->latency();

    // Set ROD BCID offset
    cmd.bcidOffset = run->rodBcidOffset();
    // temporary test for BCID check

    // Set Read-out Speed
    switch (run->dataTakingSpeed()){
      case 1:
        cmd.pixSpeed=IblBoc::Speed_40; //40
        std::cout << "Setting Speed to 40 " << std::endl;
        break;
      case 2:
        cmd.pixSpeed=IblBoc::Speed_80; //80
        std::cout << "Setting Speed to 80 " << std::endl;
        break;
      case 4:
        cmd.pixSpeed=IblBoc::Speed_160; //160
        std::cout << "Setting Speed to 160 " << std::endl;
        //Prevent to run if the mask is wrong, TODO implement Speed_160_Alt when ROD FW is updated
        if( !getModGroup().isSecondLinkAvailable( ) ){
        ers::warning(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),"The mask that you provide is not supported at 160, Setting Speed to 80 or you will go BUSY, please check your RUN CONFIG!!!"));
        std::cout << "The mask that you provide is not supported at 160, Setting Speed to 80 or you will go BUSY, check your RUN CONFIG" << std::endl;
        cmd.pixSpeed=IblBoc::Speed_80;
        }
        break;
      default:
        cmd.pixSpeed=IblBoc::Speed_40; //40
        std::cout << "Default!!!! Setting Speed to 40" << std::endl;
        break;
    }
       
    //cmd.bcidOffset = 11;

    // Reset ECR counter
    cmd.doECRReset = true;

    //FESync
    cmd.feSynchAtECR = !run->sendConfigAtECR();//If config at ECR is active we don't want FESYNC

      if( getModGroup().isManager()->exists(getModGroup().getISRoot()+"SYNC_AT_ECR") && !run->sendConfigAtECR( ) ){//Otherwise check IS variable
	cmd.feSynchAtECR = getModGroup().isManager()->read<bool>(getModGroup().getISRoot()+"SYNC_AT_ECR");
        PIX_INFO("FE SYNC_AT_ECR overwritten by IS ");
      }

    sendCommand(cmd);

    //Set the Busy Mask for Master and Slave based on the enablechannel mask(runMask);

    DataTakingTools cmd2;
    cmd2.action = SetRodBusyMask ;
    cmd2.channels =  runMask;
    sendCommand(cmd2); 
     
    ResetRodBusyHistograms cmd4;
    cmd4.dumpOnPPC = false;
    sendCommand(cmd4);

    m_runRxMask = getRunMask();
    m_runOccHisto = run->useHistogrammer();

}

void BarrelRodPixController::configureEventGenerator(PixRunConfig* run, bool active) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    
    
    //DEBUG: moved it from writeRunConfig to fix the module configuration issue.
    DataTakingConfig cmd;
    if (active) { // put ROD into simulation / internal generation mode
    		cmd = m_dtConfig; // Not overwriting the local copy for this ...
        cmd.rolEmu = false;  // ROL emulator
        cmd.bocFeEmu = true; // BOC FE emulator (must be enabled for ROL emulator)
        cmd.initRealFE = false; // don't initialize real FE
        cmd.nHitsPerTrigger = run->simModOccupancy(); // how many hits per trigger for emu
				//if (getModGroup().getRodName() == "ROD_C1_S7_ROD") cmd.nHitsPerTrigger = 10;
    }
    else { // running with real detector
        cmd.initRealFE = true;
        cmd.rolEmu = false;
        cmd.bocFeEmu = false;
        // NOTE NOTE NOTE:: fei4 trig latency is actually 255 - latency
        // this is not true for fei3. Therefore, we take the complement of trig latency from run config
    }
    
	sendCommand(cmd);

}

void BarrelRodPixController::ActivateECRReconfiguration(uint32_t moduleMask, bool activate, uint32_t params) {

  RecoAtECR_manager RecoCmd;
  
    std::cout<<getModGroup().getRodName()<<" ECR global active "<<" with module mask 0x"<<std::hex<<moduleMask<<"and params 0x"<<params<<std::dec<<std::endl;

  if (activate) {
  
    //Configure the Reconfiguration at ECR thread
    RecoCmd.mode           = RecoAtECR::FULL_CONFIG;
    RecoCmd.m_recoConfig.verbosityLevel = 0;
    RecoCmd.m_recoConfig.glbRegCfg      = true;
    RecoCmd.m_recoConfig.debug          = false;
    //RecoCmd.mask           = (m_runRxMask & (~m_disableFeMaskConfig));
    RecoCmd.m_recoConfig.mask           = moduleMask;//Contrary to IBL, Pixel follows module mask!
    RecoCmd.m_recoConfig.readBackCheck  = true;
    RecoCmd.m_recoConfig.customBS       = false;
    RecoCmd.m_recoConfig.allowReconfig  = false;
    RecoCmd.m_recoConfig.sleepDelay     = 100;
    RecoCmd.m_recoConfig.fei3GlobCfg    = params & 0x4;
    RecoCmd.m_recoConfig.MccCfg    	= params & 0x2;
    RecoCmd.m_recoConfig.softReset      = params & 0x1;

    sendCommand(RecoCmd);

    //RECO_ON_IDLE implies loading the FIFOs only once with global cfg
    //RECO_ON      implies loading the FIFOs on evry ECR
    if( RecoCmd.m_recoConfig.fei3GlobCfg )RecoCmd.mode = RecoAtECR::RECO_ON;
    else RecoCmd.mode = RecoAtECR::RECO_ON_IDLE;

    std::cout<<getModGroup().getRodName() << ": RecoAtECRCmd.mode = " << RecoCmd.mode << std::endl;
    sendCommand(RecoCmd);


  }
  
  else {
    std::cout<<getModGroup().getRodName()<<" ECR deactivated "<<std::endl;
    RecoCmd.mode = RecoAtECR::RECO_OFF;
    sendCommand(RecoCmd);
  }
						     
}

void BarrelRodPixController::startRun(PixRunConfig* run) { //! start run
  if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

   //Activate the ECR Reconfiguration
  bool sendConfigAtECR = run->sendConfigAtECR() && m_modGroup.preAmpsOn();
  
  if(m_modGroup.isManager()->exists(m_modGroup.getISRoot()+"SEND_CONFIG_AT_ECR")) {
    PIX_INFO("Send config at ECR overwritten by IS SEND_CONFIG_AT_ECR");
    sendConfigAtECR = m_modGroup.isManager()->read<bool>(m_modGroup.getISRoot()+"SEND_CONFIG_AT_ECR") && m_modGroup.preAmpsOn();
  }

  uint32_t mode = run->getFei3ECRMode();

  if(getModGroup().isManager()->exists(getModGroup().getISRoot()+"RECO_ECR_MODE")){
    PIX_INFO("ECR mode overwritten by IS RECO_ECR_MODE");
    mode =getModGroup().isManager()->read<uint32_t>(getModGroup().getISRoot()+"RECO_ECR_MODE");
  }

  if(sendConfigAtECR)ActivateECRReconfiguration(m_modGroup.getActiveModuleMask(),true, mode);

  if(m_runOccHisto)setupHistogramming();

}

void BarrelRodPixController::stopRun(PixHistoServerInterface *hsi) { //! Terminates a run
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;

    if(m_runOccHisto){
        if(m_runRxMask & 0xFFFF)stopHistogramming(0);
        if(m_runRxMask & 0xFFFF0000)stopHistogramming(1);
    }

    // Stop ECR reconfiguration thread 
    ActivateECRReconfiguration(0xffffffff,false);

    // disable all Rx paths so no spurious data makes it to the rod
    DataTakingTools cmd;
    cmd.action = DisableRxPath;
    cmd.channels = 0xFFFFFFFF;
    sendCommand(cmd);

    // disable serial line so future triggers do not make it to this ROD
    DataTakingTools cmd2;
    cmd2.action = DisableSerialLine;

    // Put the rod in configuration mode
    setConfigurationMode();

    ReadRodVetoCounters readVetoCounters;
    sendCommand(readVetoCounters);

    dumpVetoCounters();
    
    if(m_runOccHisto)getRunOccHisto(hsi);

}

uint32_t BarrelRodPixController::getRxMaskFromModuleMask(uint32_t moduleMask){
uint32_t rxMask = 0x0;

    for (int i = 0; i < MAX_MODULES; i++) {
      //std::cout << "Module number: " << i << ", rxCh=" << m_moduleRx[i] << std::endl;
      if( moduleMask & (0x1 << i) ) {
        rxMask |= (0x1 << m_moduleRx[i]);
      }
    }

if (m_ctrlVerbose) std::cout<<__PRETTY_FUNCTION__<<" received moduleMask : 0x<<"<<std::hex<<moduleMask<<" for ROD: "<<getModGroup().getRodName()<<" rx mask 0x"<<  rxMask<<std::dec<<std::endl;

return rxMask;

}

int BarrelRodPixController::nTrigger() {               //! Updates all scan status variables despite the name
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (m_scanActive) {
#ifdef __DO_GET_STATUS__
        GetStatus getStatus(1); // getStatus command
        sendCommand(getStatus);
        m_scanResult = getStatus.result.m_status;
#endif

        int ntrigger = (((m_scanResult.par) & 0xff) + (((m_scanResult.mask) & 0xff) << 8) + (((m_scanResult.par1) & 0xff) << 16) + (((m_scanResult.par1) & 0xff) << 24));

        if (m_ctrlVerbose) {
            std::cout << " at mask step: "         <<(int)m_scanResult.mask<<
                         ", at par 0 step: "       <<(int)m_scanResult.par<<
                         ", at par 1 step: "       <<(int)m_scanResult.par1<<
                         ", at par 2 step: "       <<(int)m_scanResult.par2<<
                         ", at DC step: "          <<(int)m_scanResult.dc<<
                         ", at trigger step: "     <<(int)m_scanResult.trig<<
                         ", with errorState: "     <<(int)m_scanResult.errorState<<std::endl;
            std::cout << "will return nTrigger = " << HEX(ntrigger) << std::endl;
        }


        if (m_scanResult.errorState != PPCERROR_NONE) {
            // m_errPublished flags prevent multiple consecutive messages from being printed.
            if (m_scanResult.errorState == NO_CONN_TO_FITSERVER && !m_errPublishedConn) {
                std::string msg = "ERROR: PPC REPORTED THAT THE SLAVE COULD NOT CONNECT TO THE FIT SERVER!!";
                ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
                std::cerr<< msg <<std::endl;
                std::cerr<<"Scan is continuing but you will not see any histos from at least one slave"<<std::endl;
                std::cerr<<"Check that both the slave and the fit server are running or restart both"<<std::endl;
                m_errPublishedConn = true;
            }
            else if (m_scanResult.errorState == SLAVE_NOT_RESPONDING && !m_errPublishedResponding) {
                std::string msg = "ERROR: PPC REPORTED THAT THE SLAVE TIMED OUT IN A COMMAND";
                ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
                std::cerr<< msg <<std::endl;
                std::cerr<<"The slave did not respond after 10 seconds to a command; perhaps you need to restart the slave"<<std::endl;
                m_errPublishedResponding = true;
            }
            else if (m_scanResult.errorState == NO_ENABLED_CHANNELS && !m_errPublishedChannels) {
                std::string msg = "ERROR: The PPC reported that no channels were successfully enabled for the scan.";
                ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
                std::cerr<< msg <<std::endl;
                std::cerr<<"The scan will continue, but you won't get any data! (and no connection to fit server will be attempted)"<<std::endl;
                std::cerr<<"Make sure the modules are powered on and that the optical connections are in a good state"<<std::endl;
                m_errPublishedChannels = true;
            }
        }
        return ntrigger;
    } 
    return 0;
}

unsigned int &BarrelRodPixController::fmtLinkMap(int fmt){
    //printf("%s\n",__PRETTY_FUNCTION__);
    static unsigned int a;
    if (fmt >= 0 && fmt < 8) {
        return m_fmtLinkMap[fmt];
    }
    a = 0;
    return a;
}

void BarrelRodPixController::setGroupId(int module, int group) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    //===TO RE-IMPLEMENT
    if (module<0 || module>31) return;
    if (group<0 || group>7) return;
    if (m_modPosition[module] >= 0) {
        auto const mod = m_modPosition[module];
        m_modGrp[mod] = group;
    }
}

void BarrelRodPixController::enableModuleReadout(int module) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    //===TO RE-IMPLEMENT
    std::cout << "!!==================   Not yet implemented " << std::endl;
    unsigned int mod;
    if (module<0 || module>31) return;
    if (m_modPosition[module] >= 0) {
        mod = m_modPosition[module];
        m_modActive[mod]  = true;
        m_modTrigger[mod] = true;
    }  
}

void BarrelRodPixController::disableModuleReadout(int module, bool trigger) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    //===TO RE-IMPLEMENT
    std::cout << "!!==================   Not yet implemented " << std::endl;
    unsigned int mod;
    if (module<0 || module>31) return;
    if (m_modPosition[module] >= 0) {
        mod = m_modPosition[module];
        m_modActive[mod]  = false;
        m_modTrigger[mod] = trigger;
    }
}


bool BarrelRodPixController::writeConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, std::string ptag, unsigned int revision) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    unsigned int tmprev=revision;
    if (m_conf->write(DbServer, domainName, tag, getCtrlName(),"BarrelRodPixController", ptag, tmprev))
        return true;
    else {
        std::string msg = "BarrelRodPixController : ERROR in writing BarrelRodPixController config on DBserver";
        std::cout << msg << std::endl;
        ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
        return false;
    } 
}

bool BarrelRodPixController::readConfig(PixDbServerInterface *DbServer, std::string domainName, std::string tag, unsigned int revision) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if(m_conf->read(DbServer, domainName, tag, getCtrlName(),revision))
        return true;
    else {
        std::string msg = "BarrelRodPixController : ERROR in reading BarrelRodPixController config from DBserver";
        std::cout << msg << std::endl;
        ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
        return false;
    } 
}

int BarrelRodPixController::moduleOutLink(int nmod) {
    if (m_ctrlVerbose) std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (nmod >= 0 && nmod < MAX_MODULES) {
        if (m_ctrlVerbose) std::cout << "module is defined " << endl;
        if (m_modPosition[nmod] >= 0) { ///FIX BY CHRIS 27.05.2016
            return m_moduleRx[nmod];
        }
    }
    return -1;
}

int BarrelRodPixController::getModuleRx(int modId) {
    if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (modId >= 0 && modId < MAX_MODULES) {
        if (m_moduleRx[modId] >= 0) {
            return m_moduleRx[modId];
        }
    }
    return -1;
}

int BarrelRodPixController::getSlaveFromModID(int modId){
    if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    if (modId >= 0 && modId < MAX_MODULES) {
        if (m_moduleRx[modId] >= 0) {
            return m_moduleRx[modId]/16;
        }
    }
  std::string msg = "Cannot get slave ID form mod ID "+std::to_string(modId)+", check the code";
  ers::error(PixLib::pix::daq (ERS_HERE, getModGroup().getRodName(),  msg));
    return -1;

}

int BarrelRodPixController::getModuleIDFromRx(int rxCh) {
    if (m_ctrlVerbose)  std::cout<< __PRETTY_FUNCTION__ << "for: "<<getModGroup().getRodName()<< std::endl;
    for (int i = 0; i < MAX_MODULES; i++)
           if(m_moduleRx[i] == rxCh) return i;
 
    std::cout<<"RxCh "<<rxCh<<" not found in connectivity "<<std::endl;
 
    return -1;
}

