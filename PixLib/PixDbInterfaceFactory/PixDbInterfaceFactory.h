// A helper class that knows how to create instantiate a
// PixDbInterface based on a text string specifying the database.
//
// Andrei Gaponenko <agaponenko@lbl.gov>, 2007


#ifndef PIXDBINTERFACEFACTORY_H
#define PIXDBINTERFACEFACTORY_H

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbInterface/PixDbCompoundTag.h"
#include "PixDbInterface/AutoCommit.h"

#include <string>


namespace PixLib {
#if 0
} // this brace is for better Emacs indentation
#endif


class IPixDbInterfaceFactory;

class PixDbInterfaceFactory {
  IPixDbInterfaceFactory* m_factory;
 public:

  // If connection_str ends in ".root", RootDb backend is used.
  // Otherwise, PixDbCoralDB is used, with the database type determined by Coral's
  // interpretation of connection_str.  It may look like
  // sqlite_file:filename or oracle://database_specification
  PixDbInterfaceFactory(const std::string& connection_str, AutoCommit ac = AutoCommit(false) );
  
  // Creates a new database. Does not overwrite existing data.
  PixDbInterface *openDB_new(const PixDbCompoundTag& newTag, const std::string& rootRecordID);

  // Overwrites existing database, if any, to create a new one.
  PixDbInterface *openDB_recreate(const PixDbCompoundTag& newTag, const std::string& rootRecordID);

  // These two methods access an existing database.  If no tag is specified, it is looked up in the history table (using current time).
  PixDbInterface *openDB_update(const PixDbCompoundTag& existingTag = PixDbCompoundTag() );
  PixDbInterface *openDB_readonly(const PixDbCompoundTag& existingTag = PixDbCompoundTag() );

  ~PixDbInterfaceFactory();
};

#if 0
{ // to make num braces even
#endif
} // namespace PixLib


#endif/*PIXDBINTERFACEFACTORY_H*/
