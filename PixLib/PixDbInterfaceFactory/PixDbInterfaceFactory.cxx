#include "PixDbInterfaceFactory.h"

#include "PixDbCoralDB/PixDbCoralDB.h"
#include "RootDb/RootDb.h"

//#include <sstream>
//#include <fstream>
//#include <iterator>
//#include <iomanip>
//#include <stdexcept>
//#include <cstdlib>
//#include <cassert>
//#include <sys/time.h>
//

namespace PixLib {
#if 0
} // this brace is for better Emacs indentation
#endif

//================================================================
class IPixDbInterfaceFactory {
public:
  // All returned pointers should be deleted by the client
  virtual PixDbInterface *openDB_new(const PixDbCompoundTag& newTag, const string& rootRecordID) = 0;
  virtual PixDbInterface *openDB_recreate(const PixDbCompoundTag& newTag, const string& rootRecordID) = 0;
  virtual PixDbInterface *openDB_update(const PixDbCompoundTag& existingTag) = 0;
  virtual PixDbInterface *openDB_readonly(const PixDbCompoundTag& existingTag) = 0;
  virtual ~IPixDbInterfaceFactory() {}
};

//----------------------------------------------------------------
class PixDbInterfaceFactory_Root : virtual public IPixDbInterfaceFactory {
  string m_filename;
public:
  PixDbInterfaceFactory_Root(const string& filename) : m_filename(filename) {}
  
  virtual PixDbInterface *openDB_new(const PixDbCompoundTag& newTag, const string& rootRecordID) {
    return new RootDb(m_filename, "NEW");
  }

  virtual PixDbInterface *openDB_recreate(const PixDbCompoundTag& newTag, const string& rootRecordID) {
    return new RootDb(m_filename, "RECREATE"); 
  }

  virtual PixDbInterface *openDB_update(const PixDbCompoundTag& existingTag) {
    return new RootDb(m_filename, "UPDATE"); 
  }

  virtual PixDbInterface *openDB_readonly(const PixDbCompoundTag& existingTag) { 
    return new RootDb(m_filename, "READ"); 
  }
};

//----------------------------------------------------------------
class PixDbInterfaceFactory_Coral : virtual public IPixDbInterfaceFactory {
  string m_connection;
  PixLib::AutoCommit m_autoCommit;
public:
  PixDbInterfaceFactory_Coral(const string& connstr, PixLib::AutoCommit ac) 
    : m_connection(connstr), 
      m_autoCommit(ac) 
  {}
  
  virtual PixDbInterface *openDB_new(const PixDbCompoundTag& newTag, const string& rootRecordID) { 
    return new PixDbCoralDB(m_connection, PixDbCoralDB::NEW, m_autoCommit, rootRecordID, newTag); 
  }

  virtual PixDbInterface *openDB_recreate(const PixDbCompoundTag& newTag, const string& rootRecordID) {
    return new PixDbCoralDB(m_connection, PixDbCoralDB::RECREATE, m_autoCommit, rootRecordID, newTag); 
  }

  virtual PixDbInterface *openDB_update(const PixDbCompoundTag& existingTag) {
    return new PixDbCoralDB(m_connection, PixDbCoralDB::UPDATE, m_autoCommit, existingTag);
  }

  virtual PixDbInterface *openDB_readonly(const PixDbCompoundTag& existingTag) {
    return new PixDbCoralDB(m_connection, PixDbCoralDB::READONLY, m_autoCommit, existingTag);
  }
};

//================================================================
PixDbInterface *PixDbInterfaceFactory::openDB_new(const PixDbCompoundTag& newTag, const string& rootRecordID) {
  return m_factory->openDB_new(newTag, rootRecordID);
}

PixDbInterface *PixDbInterfaceFactory::openDB_recreate(const PixDbCompoundTag& newTag, const string& rootRecordID) {
  return m_factory->openDB_recreate(newTag, rootRecordID);
}

PixDbInterface *PixDbInterfaceFactory::openDB_update(const PixDbCompoundTag& existingTag ) {
  return m_factory->openDB_update(existingTag);
}

PixDbInterface *PixDbInterfaceFactory::openDB_readonly(const PixDbCompoundTag& existingTag ) {
  return m_factory->openDB_readonly(existingTag);
}

//================================================================
PixDbInterfaceFactory::PixDbInterfaceFactory(const string& connection_str, AutoCommit ac) {
  std::string::size_type dotpos = connection_str.rfind('.');
  if((dotpos != std::string::npos) &&  (connection_str.substr(dotpos) == ".root" )) {
    cerr<<"PixDbInterfaceFactory: using RootDb"<<endl;
    m_factory = new PixDbInterfaceFactory_Root(connection_str);
  }
  else { // Assume Coral, but do a check for ':' to help to avoid confusion
    if(connection_str.find(':') == std::string::npos) {
      std::cerr<<std::endl<<"Warning: "
	"PixDbInterfaceFactory(connection_str, autoCommit): connection_str should contain a semicolon for CoralDB\n"
	"For example, \"sqlite_file:test.db\", or \"oracle://devdb10/ATLAS_PIXELDT_ANDR\"\n"
	"Or it can have the form \"filename.root\" to use RootDb.\n"
	"Got connection_str = \""+connection_str+"\"\n"
	"Passing it to Coral in case it can make sense out of it...\n"
	       <<std::endl;
    }
    m_factory = new PixDbInterfaceFactory_Coral(connection_str, ac);
  }
}

//================================================================
PixDbInterfaceFactory::~PixDbInterfaceFactory() {
  delete m_factory;
}

//================================================================
#if 0
{ // to make num braces even
#endif
} // namespace PixLib
