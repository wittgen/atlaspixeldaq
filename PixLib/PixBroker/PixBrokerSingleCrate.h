/////////////////////////////////////////////////////////////////////
// PixBrokerSingleCrate.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 08/09/06  Version 1.0 (CS)
//           Initial release
//

//! Class for the Pixel single crate resource broker

#ifndef _PIXLIB_BROKERSINGLECRATE
#define _PIXLIB_BROKERSINGLECRATE

#include "PixBroker/PixBroker.h"
#include "PixDbInterface/PixDbInterface.h"
#include "PixConnectivity/PixConnectivity.h"

// Forward declarations
class IPCPartition;


namespace PixLib {

  // Forward declarations
  class PixActions;
  class PixDbServerInterface;
  class PixHistoServerInterface;

  class PixBrokerSingleCrate : public PixBroker {

  public:
    
    // Constructor
    PixBrokerSingleCrate(IPCPartition *ipcPartition, std::string crateName, PixDbServerInterface *DbServer, PixHistoServerInterface *hInt, 
			 PixConnectivity* conn, std::string isServerName = "");

    // Destructor
    ~PixBrokerSingleCrate();

    // Allocation/deallocation
    virtual PixActions* allocateActions(std::set<std::string> actionNames);
    virtual void deallocateActions(PixActions* action);
    virtual void updateConnectivityDB(std::string connTag, std::string confTag);
    virtual void updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag);

    virtual void setConnectivityTags(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg);
    virtual void setConnTag(std::string tagConn, std::string tagCfg, std::string tagID, std::string tagModCfg);
    virtual void setCfgTag(std::string tagConn, std::string tagCfg, std::string tagID, std::string tagModCfg);


    virtual PixActions* allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName);
    virtual void deallocateActions(PixActions* action, const char* allocatingBrokerName);
 
    virtual void loadNewConn(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg);
    virtual void createActions(PixDbServerInterface *DbServer, PixHistoServerInterface *hInt);

    virtual void destroyActions();

    virtual void periodicTasks();

    MY_LONG_INT ipc_emergency(const char* action, const char* par);
    MY_LONG_INT emergency(std::string action, std::string par);
    void ipc_restartActions();
    unsigned int getCheckSumDb( );
    void generateCfgObjectMap();

  private:

    // PixLib objects
    PixConnectivity *m_conn;

    std::string m_isServerName;
    PixISManager *m_is;
    RodCrateConnectivity *getRodCrateConnectivity( );

    PixDbServerInterface *m_DbServer;
    PixHistoServerInterface *m_hInt;

    std::string m_flavour;
    unsigned int m_checkSum;
    std::map <std::string, std::vector<std::string>> m_cfgObjectMap;

    
  };
  
}

#endif // _PIXLIB_BROKERSINGLECRATE
