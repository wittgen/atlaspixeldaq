/////////////////////////////////////////////////////////////////////
// PixExpertBroker.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 15/02/08  Version 1.0 (DLM)
//           Initial release
//

//! Class for the Pixel expert broker

#ifndef _PIXLIB_EXPERTBROKER
#define _PIXLIB_EXPERTBROKER

// Forward declarations
class IPCPartition;


namespace PixLib {

  // Forward declarations
  //class PixActions;

  class PixExpertBroker {
    
  public:
    
    // Constructor
    PixExpertBroker(IPCPartition *ipcPartition);
    ~PixExpertBroker(){}
    
    // Resource allocation
    void deallocateAllMulti();
    void deallocateAllInterfaces();
    void deallocateAllSingleCrates();

  private:
    IPCPartition* m_ipcPartition;
    
  };
  
}

#endif // _PIXLIB_BROKERMULTICRATEEXPERT
