/////////////////////////////////////////////////////////////////////
// PixBroker.h
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 03/04/06  Version 1.0 (CS)
//           Initial release
//
// 12/09/06  Version 1.1 (CS)
//           First IPC version
//

//! Base class for the Pixel resource broker

#ifndef _PIXLIB_BROKER
#define _PIXLIB_BROKER

#include <string>
#include <set>

#include <ipc/partition.h>
#include <ipc/object.h>

#include "PixActions/PixActions.h"

#include "PixDbInterface/PixDbInterface.h"
#include "PixBrokerIDL.hh"
#include "PixUtilities/PixLock.h"

#ifndef MY_LONG_INT
#ifdef CORBALONGLONG
#define MY_LONG_INT int32_t
#else
#define MY_LONG_INT CORBA::Long
#endif
#endif

namespace PixLib {

  // Forward declarations

  class PixBroker : public IPCNamedObject<POA_ipc::PixBroker, ipc::single_thread> {

    friend class PixExpertBroker;    

  public:
    
    // Constructor
    PixBroker(IPCPartition *ipcPartition, std::string ipcName);
    
    // Destructor
    virtual ~PixBroker();
    
    // Resource listing
    virtual std::set<std::string> listSubBrokers();
    virtual std::set<std::string> listActions(bool checkAvailability = true,
					      PixActions::Type type = PixActions::ANY_TYPE,
					      std::string allocatingBrokerName="",
					      std::string managingBrokerName="", std::string caller="");

//    virtual std::set<PixActions::PixActionsDescriptor> listActions(bool checkAvailability = true,
//								   PixActions::Type type = PixActions::ANY_TYPE,
//								   PixActions::Assignment assignment=PixActions::ANY_ASSIGNMENT,
//								   std::string brokerName="") = 0;
//    

    // IPC resource listing
    virtual ipc::stringList* ipc_listSubBrokers();
    virtual ipc::stringList* ipc_listActionsNames(bool checkAvailability = true,
						  ipc::PixActionsType type = ipc::ANY_TYPE,
						  const char* allocatingBrokerName="",
						  const char* managingBrokerName="");

//   virtual ipc::descriptorList* ipc_listActionsDescriptors(bool checkAvailability = true,
//							    ipc::PixActionsType type = ipc::ANY_TYPE,
//							    ipc::PixActionsAssignment assignment=ipc::ANY_ASSIGNMENT,
//							    std::string brokerName="") {}
//

    // Resource allocation
    virtual PixActions* allocateActions(std::set<std::string> actionNames) = 0;
    virtual void deallocateActions(PixActions* action) = 0;

    // Resource management
    //virtual void destroyActions();
    //virtual void ipc_destroyActions() {} // TODO

    // Name
    char* ipc_name();

    // IPC shutdown
    virtual void shutdown() {}

    virtual void updateConnectivityDB(std::string connTag, std::string confTag); //If a broker will make use of this function
                                                                 //then it should implement it itself.

    virtual void updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag);

    void ipc_setCallback(ipc::PixBrokerCallback_ptr cb);
    void ipc_unsetCallback();

  protected:

    // SW interfaces
    IPCPartition *m_ipcPartition;

    // Broker name
    std::string m_name;

    // Sub brokers
    std::map<std::string,ipc::PixBroker_var> m_subBrokers;

    // Actions vector
    std::map<std::string,PixActions*> m_actions;

    //Callback variables
    ipc::PixBrokerCallback_var m_cb;

    //Mutex
    PixMutex* m_mutex;

    //Helper methods
    
    virtual void ipc_updateConnectivityDB(const char* connTag, const char* confTag); 
    virtual void ipc_updateConnectivityFromIS(const char* idTag, const char* connTag, const char* cfgTag, const char* modCfgTag); 
    
    // Resource allocation
    virtual char* ipc_allocateActions(const ipc::stringList &actionNames, const char* allocatingBrokerName);
    virtual bool ipc_deallocateAction(const char* actionName, const char* deallocatingBrokerName);
    virtual void ipc_deallocateAll();

    virtual PixActions* allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName) = 0;
    virtual void deallocateActions(PixActions* action, const char* deallocatingBrokerName) = 0;
    virtual void deallocateAll();
    // Resource management
    virtual void destroyActions();
    virtual void ipc_destroyActions() {} // TODO

    virtual MY_LONG_INT ipc_test();
    virtual MY_LONG_INT ipc_emergency(const char* action, const char* par);
    virtual void ipc_restartActions();
    virtual bool ipc_restartActionsCompleted();
    bool m_restartActionsCompleted;

  };

}

#endif // _PIXLIB_BROKER
