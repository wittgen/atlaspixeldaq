/////////////////////////////////////////////////////////////////////
// PixExpertBroker.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 15/02/08  Version 1.0 (CS)
//           Initial release
//


#include "PixBroker/PixExpertBroker.h"
#include "PixBroker/PixBroker.h"
#include "PixUtilities/PixMessages.h"


using namespace PixLib;



//! Constructor

PixExpertBroker::PixExpertBroker(IPCPartition *ipcPartition) :
  m_ipcPartition(ipcPartition)
{

}

void PixExpertBroker::deallocateAllMulti()
{

  try {
    std::map<std::string,ipc::PixBroker_var> brokers;
    std::cout << "Obtaining brokers from the partition" << std::endl;
    m_ipcPartition->getObjects<ipc::PixBroker>(brokers);
    std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=brokers.end();
    std::cout << "Finding MultiBrokers and deallocating their actions" << std::endl;
    for(broker=brokers.begin(); broker!=brokerEnd; broker++){
      if(std::string(broker->first,0,6)=="MULTI:") {
	std::cout << "Found Broker Multi " << broker->first << std::endl;

	ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::MULTI,"",broker->first.c_str());

	if(subActions == 0 || subActions->length() == 0){
	  std::cout << "Broker Multi " << broker->first << " is managing no actions " << std::endl;
	  if(subActions != 0) delete subActions;
	  continue;
	}

	for(unsigned int i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  std::cout << "Deallocating action " << temp << " in broker " << broker->first << std::endl;
	  broker->second->ipc_deallocateAction(temp.c_str(),broker->first.c_str());	  
	}
	delete subActions;
      }
    }
  } catch(...) {
    std::cout << "PixExpertBroker ERROR: Exception caught deallocating actions from Multi brokers" << std::endl;
  }

}

void PixExpertBroker::deallocateAllInterfaces()
{
  try {
    std::map<std::string,ipc::PixBroker_var> brokers;
    std::cout << "Obtaining brokers from the partition" << std::endl;
    m_ipcPartition->getObjects<ipc::PixBroker>(brokers);
    std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=brokers.end();
    std::cout << "Finding Single Crate Interface Brokers and deallocating their actions" << std::endl;
    for(broker=brokers.begin(); broker!=brokerEnd; broker++){
      if(std::string(broker->first,0,3)=="IF:") {
	std::cout << "Found Single Interface Broker " << broker->first << std::endl;

	ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::MULTI,"",broker->first.c_str());

	if(subActions == 0 || subActions->length() == 0){
	  std::cout << "Single Interface Broker " << broker->first << " is managing no actions " << std::endl;
	  if(subActions != 0) delete subActions;
	  continue;
	}

	for(unsigned int i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  std::cout << "Deallocating action " << temp << " in broker " << broker->first << std::endl;
	  broker->second->ipc_deallocateAction(temp.c_str(), m_ipcPartition->lookup<ipc::PixActions>(temp.c_str())->ipc_allocatingBrokerName());	  
	}
	delete subActions;
      }
    }
  } catch(...) {
    std::cout << "PixExpertBroker ERROR: Exception caught deallocating actions from Interface brokers" << std::endl;
  }

}

void PixExpertBroker::deallocateAllSingleCrates()
{

  try {
    std::map<std::string,ipc::PixBroker_var> brokers;
    std::cout << "Obtaining brokers from the partition" << std::endl;
    m_ipcPartition->getObjects<ipc::PixBroker>(brokers);
    std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=brokers.end();
    std::cout << "Finding Single Crates and deallocating their actions" << std::endl;
    for(broker=brokers.begin(); broker!=brokerEnd; broker++){
      if(std::string(broker->first,0,3)!="IF:" && std::string(broker->first,0,6)!="MULTI:") {
	std::cout << "Found Single Broker " << broker->first << std::endl;

	ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::ANY_TYPE,"",broker->first.c_str());

	if(subActions == 0 || subActions->length() == 0){
	  std::cout << "Single Crate Broker " << broker->first << " is managing no actions " << std::endl;
	  if(subActions != 0) delete subActions;
	  continue;
	}

	for(unsigned int i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  std::cout << "Deallocating action " << temp << " in broker " << broker->first << std::endl;
	  //broker->second->ipc_deallocateAction(temp.c_str(),broker->first.c_str());
	  // Need to call deallocate with allocating broker name
	  broker->second->ipc_deallocateAction(temp.c_str(), m_ipcPartition->lookup<ipc::PixActions>(temp.c_str())->ipc_allocatingBrokerName());
	}
	delete subActions;
      }
    }
  } catch(...) {
    std::cout << "PixExpertBroker ERROR: Exception caught deallocating actions from single crate brokers" << std::endl;
  }

}

