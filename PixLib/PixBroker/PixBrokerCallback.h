/////////////////////////////////////////////////////////////////////
// PixBrokerCallback.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 22/06/07  Version 1.0 (DLM)
//           Initial release
//

//! Class for the Pixel broker callback

#ifndef _PIXLIB_BROKER_CALLBACK
#define _PIXLIB_BROKER_CALLBACK

//#include <ipc/server.h>
// possible replacement
#include <owl/semaphore.h>
#include <ipc/object.h>

#include "PixBrokerIDL.hh"


namespace PixLib {
  
  //  class PixBrokerCallback : public IPCServer, public IPCObject<POA_ipc::PixBrokerCallback> {
  class PixBrokerCallback : public OWLSemaphore, public IPCObject<POA_ipc::PixBrokerCallback> {
    
  public:
    
    // Constructor
    PixBrokerCallback() : m_connections(0) {}
    
    // Destructor
    ~PixBrokerCallback() { }
    
    // Reset
    void reset() {m_connections=0;}

    // Connection counter handling
    void  increaseConnections() {m_connections++;}
    // JGK: changed after removing inheritance from ipc-server - correct replacement?
    void  decreaseConnections() {m_connections--; if(m_connections==0) this->post();}
    short countConnections() {return m_connections;}

  private:

    // Connections counter
    short m_connections;
    
  };
  
}

#endif // _PIXLIB_BROKER_CALLBACK
