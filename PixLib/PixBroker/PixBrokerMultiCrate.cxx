/////////////////////////////////////////////////////////////////////
// PixBrokerMultiCrate.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/09/06  Version 1.0 (CS)
//           Initial release
//

//! Class for the Pixel single crate interface broker

#include <unistd.h>
#include <sstream>
#include <iostream>

#include "PixActions/PixActionsMulti.h"

#include "PixBroker/PixBrokerMultiCrate.h"
#include "PixBroker/PixBrokerCallback.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;

//! Constructor
PixBrokerMultiCrate::PixBrokerMultiCrate(std::string brokerName, IPCPartition *ipcPartition) : PixBroker(ipcPartition, std::string("MULTI:")+gethoststr()+std::string(":")+getpidstr() +":"+brokerName)

{
  
  std::cout << "constructing broker " << std::endl;
  // Get IPC handle for sub brokers
  try {
    std::map<std::string,ipc::PixBroker_var> brokers;
    //    ipcPartition->getObjects<ipc::PixBroker>(brokers);	
    CosNaming::BindingList_var	bl;
    ers::info(PixLib::pix::daq(ERS_HERE,ipcPartition->name(),"Starting ipc listing"));
    ipc::util::list( ipcPartition->name(), ipc::util::getTypeName<ipc::PixBroker>(), bl );
    ers::info(PixLib::pix::daq(ERS_HERE,ipcPartition->name(),"Done with ipc listing"));

    if ( bl.operator->() == 0 )
      return;

    for ( unsigned int i = 0; i < bl->length(); i++ ){
      std::string object_name = (const char *)bl[i].binding_name[0].id;
      
      try{
	brokers.insert( std::make_pair( object_name, ipcPartition->lookup<ipc::PixBroker>( object_name ) ) );
      } catch (daq::ipc::InvalidPartition&) {
	// throw ??
	// messages??
	std::cout << "IPC server is unavailable " << std::endl;
      } catch (daq::ipc::InvalidObjectName&) {
	std::cout << "The name of the subAction is not valid " << std::endl;
      } catch (daq::ipc::ObjectNotFound&) {
	std::cout << "The subAction does not exist or is not registered with the partition server "<< std::endl; 
      } catch(...) {
	std::cout << "Unknown exception caught in PixBrokerMultiCrate constructor";
      }
      if (!ipcPartition->isObjectValid<ipc::PixBroker>(object_name)) {
	std::string msg = "The check of the existence of the remote object broker " + object_name + " failed ";
	ers::info(PixLib::pix::daq(ERS_HERE,ipcPartition->name(),msg));
      }
    }
    
    std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=brokers.end();
    for(broker=brokers.begin(); broker!=brokerEnd; broker++){
      if(std::string(broker->first,0,6)=="CRATE:" || std::string(broker->first,0,4)=="TIM:") {
	m_subBrokers.insert(std::make_pair(broker->first,broker->second));
	
	std::cout<<"SubBroker "<<broker->first<<" Contains:"<<std::endl;
	
	std::vector<std::string> actionsList;
	ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::SINGLE_ROD,"","");
      
	for(unsigned int i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  std::cout<<temp<<std::endl;
	  actionsList.push_back(temp);
	}

	ipc::stringList* subActionsTIM = broker->second->ipc_listActionsNames(false,ipc::SINGLE_TIM,"","");
      
	for(unsigned int i=0; i<subActionsTIM->length(); i++){
	  std::string temp = std::string((*subActionsTIM)[i]._retn());
	  std::cout<<temp<<std::endl;
	  actionsList.push_back(temp);
	}
	m_brokerActions.insert(std::make_pair(broker->first,actionsList));
	delete subActions;
	delete subActionsTIM;
      }
    }
    /*std::map<std::string,std::vector<std::string> >::iterator brokerActions, brokerActionsEnd=m_brokerActions.end();
    for(brokerActions=m_brokerActions.begin(); brokerActions != brokerActionsEnd; brokerActions++){
      for(int k = 0; k<(brokerActions->second).size(); k++){
	std::cout << "for broker " << brokerActions->first << " " << (brokerActions->second)[k] << std::endl;
      }
      }*/
  } catch(...) {
    // TODO: signal error if some action cannot be retrieved
  }

  // Set broker name
  m_name = std::string("MULTI:")+gethoststr()+std::string(":")+getpidstr() +":"+brokerName;
  m_actionsCount = 0;
  
}

PixBrokerMultiCrate::~PixBrokerMultiCrate()
{
  deallocateAll();
}

PixActions* PixBrokerMultiCrate::allocateActions(std::set<std::string> actionNames) {
  return allocateActions(actionNames, m_name.c_str());
}

void PixBrokerMultiCrate::deallocateActions(PixActions* action) {
  deallocateActions(action, m_name.c_str());
}

//! Resource allocation

PixActions* PixBrokerMultiCrate::allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName) {
  
  // TODO: add protections against non existing or already allocated actions
  if(actionNames.begin() == actionNames.end()){
    std::cout << "ERROR PixBrokerMultiCrate: No action to allocate provided. Ignoring request." << std::endl;
    return 0;
  }
  // Filter out actions not belonging to this broker
  std::set<std::string> myAllocatedActionNames; // List of actions to be allocated
  std::set<std::string> myAvailableActionNames=listActions(true,PixActions::ANY_TYPE,"","","PixBrokerMultiCrate"); // List of actions available for allocation
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
    if(myAvailableActionNames.find(*actionName)!=myAvailableActionNames.end()) {
      myAllocatedActionNames.insert(*actionName);
    } else {
      std::cout << "problem finding this action " << std::endl;
      //throw exception();//TODO: define what needs to be thrown, really
      //throw exception("PixBrokerSingleCrateInterface::allocateActions: One of the actions required to allocate does not exist in the associated sub broker.");
    }
  }
  
  //std::set<std::string> newAllocatedActionNames;

  // Translate list of names for each broker
  std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=m_subBrokers.end();
  std::ostringstream multiActionName; 
  multiActionName<<m_name;
  multiActionName<<":MULTI_";
  multiActionName<<m_actionsCount;
  m_actionsCount++;
  //ipc::stringList* ipcActionNames = 0;
  
  std::set <std::string> allocatedActions;
  std::set <std::string> allocatedBrokers;
  for(broker=m_subBrokers.begin(); broker!=brokerEnd; broker++) {

    //int i=0;
    
    std::cout<<"Allocated actions"<<std::endl;
    bool allocatedInThisBroker = false;
    for(actionName=myAllocatedActionNames.begin(), actionNameEnd=myAllocatedActionNames.end(); actionName!=actionNameEnd; actionName++) {
      std::cout<<std::string(*actionName)<<std::endl;
      if(broker->first == getBrokerNameFromAction(*actionName)){
        allocatedActions.insert(*actionName);
        insertActionInBrokerActionMap(broker->first,std::string(*actionName));
        allocatedInThisBroker = true;
      }
    }
    
    if(allocatedInThisBroker)allocatedBrokers.insert(broker->first);


  }
  
  updateConnectivityFromIS(allocatedBrokers);
  
  // Create multiple action
  PixActions* allocated = 0;
  if(allocatedActions.size() == 0) return 0;
  try {
    std::cout << "Creating Multi in Broker" << std::endl;
    allocated = new PixActionsMulti(multiActionName.str(), m_name, "",
				    allocatedActions,
				    m_ipcPartition);
  } catch(...) {
    // TODO: throw
    std::cout << "Problem creating multi in broker" << std::endl;
    return 0;
  }
  m_actions.insert(std::make_pair(multiActionName.str(),allocated));
  // Allocate action, catch exception and return zero pointer if action is already allocated
  //try {
    allocated->allocate(allocatingBrokerName);
    return allocated;
 //  } catch (std::runtime_error &e){
//     std::string info = "ERROR! [PixBrokerMulitCrate::allocateActions]" + e.what();
//     std::cout << info << std::endl;
//     return 0;
//   } catch(...){
//     std::string info = "ERROR! [PixBrokerMulitCrate::allocateActions]" + e.what();
//     std::cout << info << std::endl;
//     return 0;
    //}
}

void PixBrokerMultiCrate::deallocateActions(PixActions* action, const char* deallocatingBrokerName) {

  std::cout<<__PRETTY_FUNCTION__<<" "<<action->name()<<" "<< std::string(deallocatingBrokerName)<<std::endl;
  
  std::string actionName = action->name();

  // Check if requested action exists
  if(m_actions.find(actionName) == m_actions.end()){
   std::cout<<"Cannot deallocate action "<<actionName<<" not found!"<<std::endl;
   return; //throw some exception
   
   }
  
  // Recursively dellocate actions
  action->deallocate(deallocatingBrokerName);

  // Propagate dellocation to sub-brokers
  PixActionsMulti* multiAction = dynamic_cast<PixActionsMulti*>(action);
  if(multiAction){

    // Create list of subactions to be deallocated
    std::vector<std::string> actions = multiAction->listSubActions(PixActions::MULTI);
    // Loop on sub brokers
    std::vector<std::string>::iterator subActionStr, subActionStrEnd=actions.end();
    for(subActionStr=actions.begin(); subActionStr!=subActionStrEnd; subActionStr++) {
      std::string brokerName;
      brokerName = getBrokerNameFromAction(*subActionStr);
      if(brokerName != ""){
	std::map<std::string, ipc::PixBroker_var>::iterator broker, brokerEnd = m_subBrokers.end();
	for(broker = m_subBrokers.begin(); broker != brokerEnd; broker++){
	  if(broker->first == brokerName){
            std::cout << "Calling deallocateAction for " << *subActionStr << std::endl;
	    broker->second->ipc_deallocateAction(subActionStr->c_str(), deallocatingBrokerName);
	    deleteActionInBrokerActionMap(brokerName, *subActionStr);
	  }
	}
      } else{
         std::cout<<"ERROR no broker name provided"<<std::endl;
	//should throw an exception, this would imply a bug in the actions listSubActions method
      }
    }
  } 

  // Remove action
  action->_destroy();
  action = 0;
  m_actions.erase(actionName);
  std::cout << "brokers left" << std::endl;

  std::map<std::string, ipc::PixBroker_var>::iterator broker, brokerEnd = m_subBrokers.end();
  for(broker = m_subBrokers.begin(); broker != brokerEnd; broker++){
    std::cout << broker->first << " " << broker->second << std::endl;
  }

  std::cout << "End of deallocateActions Method!!!" << std::endl;
}

void PixBrokerMultiCrate::updateConnectivityDB(std::string connTag, std::string confTag)
{

  //Deallocate the multi actions
  // Filter out actions not belonging to this broker
  std::set<std::string> actionNames=listActions(false,PixActions::ANY_TYPE); // List of actions available for allocation
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();

  PixActions* allocated = 0;

  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
    if(m_actions.find(*actionName)!=m_actions.end()) {
      allocated = (*(m_actions.find(*actionName))).second;
      if(allocated->allocatingBrokerName() != "") deallocateActions(allocated,"EXPERT:");
    } else {
      //throw exception();//TODO: define what needs to be thrown, really
      //throw exception("PixBrokerSingleCrateInterface::allocateActions: One of the actions required to allocate does not exist in the associated sub broker.");
    }
  }

  std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=m_subBrokers.end();
  PixBrokerCallback *cb = new PixBrokerCallback;
  for(broker=m_subBrokers.begin(); broker!=brokerEnd; broker++) {
    std::cout << "Updating connDB in Broker " << m_subBrokers.size() << std::endl;
    try{
      broker->second->ipc_setCallback(cb->_this());
      broker->second->ipc_updateConnectivityDB(connTag.c_str(),confTag.c_str());
          //break; //Once these are caught, then if no exception is thrown 
	  //we can break the broker loop
    }catch(...){
      std::cout << "Exception caught in the broker" << std::endl;
          //catch exception for trying to deallocate an action not belonging to 
          //that subbroker.
    }
  } 
  // JGK: commented after removing inheritance from ipc-server - correct replacement?
  //      cb->run();
  cb->wait();
  cb->_destroy();
  std::cout << "done with broker" << std::endl;
  
}

void PixBrokerMultiCrate::updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag)
{
  //Deallocate the multi actions
  // Filter out actions not belonging to this broker
  std::set<std::string> actionNames=listActions(false,PixActions::ANY_TYPE); // List of actions available for allocation
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
  
  PixActions* allocated = 0;
  std::cout << "PixBrokerMultiCrate::updateConnectivityFromIS : Attempting to deallocate all" <<std::endl;
  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
    if(m_actions.find(*actionName)!=m_actions.end()) {
      allocated = (*(m_actions.find(*actionName))).second;
      if(allocated->allocatingBrokerName() != ""){
	deallocateActions(allocated,"EXPERT:");
	std::cout << "PixBrokerMultiCrate::updateConnectivityFromIS : deallocating actions." <<std::endl;
      }
    } else {
      //throw exception();//TODO: define what needs to be thrown, really
      //throw exception("PixBrokerSingleCrateInterface::allocateActions: One of the actions required to allocate does not exist in the associated sub broker.");
    }
  }
  
  std::map<std::string,ipc::PixBroker_var>::iterator broker, brokerEnd=m_subBrokers.end();
  PixBrokerCallback *cb = new PixBrokerCallback;
  for(broker=m_subBrokers.begin(); broker!=brokerEnd; broker++) {
    std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Updating connDB in Broker " << m_subBrokers.size() << std::endl;
    try{
      std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: For each sub-broker, calling ipc_updateConnectivityfromIS "<< std::endl;
      broker->second->ipc_setCallback(cb->_this());
      broker->second->ipc_updateConnectivityFromIS(idTag.c_str(),connTag.c_str(),cfgTag.c_str(),modCfgTag.c_str());
          //break; //Once these are caught, then if no exception is thrown 
	  //we can break the broker loop
    }catch(...){
      std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Exception caught in the broker" << std::endl;
          //catch exception for trying to deallocate an action not belonging to 
          //that subbroker.
    }
  } 
  // JGK: commented after removing inheritance from ipc-server - correct replacement?
  //      cb->run();
  cb->wait();
  cb->_destroy();
  std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Done with broker" << std::endl;
  
}


void PixBrokerMultiCrate::updateConnectivityFromIS(std::set<std::string> allocatedBrokerName)
{
  std::map<std::string,ipc::PixBroker_var>::iterator broker;
  PixBrokerCallback *cb = new PixBrokerCallback;
  std::set<std::string>::iterator brokerName;

  for(brokerName = allocatedBrokerName.begin(); brokerName != allocatedBrokerName.end(); brokerName++){
    
    broker = m_subBrokers.find(std::string(*brokerName));
    if(broker == m_subBrokers.end())continue;
    std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Updating connDB in Broker " << broker->first << std::endl;
    try{
      std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: For each sub-broker, calling ipc_updateConnectivityfromIS "<< std::endl;
      broker->second->ipc_setCallback(cb->_this());
      broker->second->ipc_updateConnectivityFromIS("","","","");
    }catch(...){
      std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Exception caught in the broker" << std::endl;
    }
  } 
  // JGK: commented after removing inheritance from ipc-server - correct replacement?
  //      cb->run();
  cb->wait();
  cb->_destroy();
  std::cout << "PixBrokerMultiCrate: updateConnectivityFromIS: Done with broker" << std::endl;
}

std::map<std::string,std::vector<std::string> > PixBrokerMultiCrate::getConnectivityTags()
{
  std::cout << "PixBrokerMultiCrate::getConnectivityTags(): Getting SINGLE_ROD actions" << std::endl;
  std::set<std::string> myActions=listActions(false,PixActions::SINGLE_ROD); 
  std::set<std::string>::iterator actionName, actionNameEnd=myActions.end();

  std::map<std::string,std::vector<std::string> > result;

  for(actionName = myActions.begin(); actionName != actionNameEnd; actionName++){
    std::string connTagC, connTagP, connTagA, connTagOI, connTagCF, connTagMCF, connTagCFOI;
    char* connTagCc = 0;
    char* connTagPc = 0;
    char* connTagAc = 0;
    char* connTagOIc = 0;
    char* connTagCFc = 0;
    char* connTagMCFc = 0;
    char* connTagCFOIc = 0;

    std::cout << "PixBrokerMultiCrate::getConnectivityTags(): Looking up action " << *actionName << " in partition" << std::endl;
    try{
      ipc::PixActions_var action = m_ipcPartition->lookup<ipc::PixActions,ipc::no_cache,ipc::narrow>(*actionName);


      std::cout << "PixBrokerMultiCrate::getConnectivityTags(): Found action " << *actionName << " in partition. Getting tags" << std::endl;
      connTagCc = action->ipc_getConnTagC(actionName->c_str());
      if(connTagCc != 0) connTagC = std::string(connTagCc);
      connTagPc = action->ipc_getConnTagP(actionName->c_str());	
      if(connTagPc != 0) connTagP = std::string(connTagPc);
      connTagAc = action->ipc_getConnTagA(actionName->c_str());	
      if(connTagAc != 0) connTagA = std::string(connTagAc);
      connTagCFc = action->ipc_getCfgTag(actionName->c_str());
      if(connTagCFc != 0) connTagCF = std::string(connTagCFc);
      connTagOIc = action->ipc_getConnTagOI(actionName->c_str());
      if(connTagOIc != 0) connTagOI = std::string(connTagOIc);
      connTagCFOIc = action->ipc_getCfgTagOI(actionName->c_str());
      if(connTagCFOIc != 0) connTagCFOI = std::string(connTagCFOIc);
      connTagMCFc = action->ipc_getModCfgTag(actionName->c_str());
      if(connTagMCFc != 0) connTagMCF = std::string(connTagMCFc);
    }catch(daq::ipc::InvalidPartition &e1){
      std::cout << "Caught InvalidPartition exception. Going to next action" << std::endl;
      continue;
    }catch(daq::ipc::ObjectNotFound &e2){
      std::cout << "Caught ObjectNotFound exception. Going to next action" << std::endl;
      continue;
    }catch(daq::ipc::InvalidObjectName &e3){
      std::cout << "Caught InvalidaObjectName exception. Going to next action" << std::endl;
      continue;
    }catch(CORBA::TRANSIENT& err){
      std::cout << "Caught CORBA::TRANSIENT exception. Going to next action" << std::endl;
      continue;

    }catch(CORBA::COMM_FAILURE &err){
      std::cout << "Caught CORBA::COM_FAILURE exception. Going to next action" << std::endl;
      continue;
    }catch(...){
      std::cout << "Caught unknown exception. Going to next action" << std::endl;
      continue;
    }
    std::vector<std::string> connTags;
    connTags.push_back(connTagC);
    connTags.push_back(connTagP);
    connTags.push_back(connTagA);
    connTags.push_back(connTagCF);
    connTags.push_back(connTagOI);
    connTags.push_back(connTagCFOI);
    connTags.push_back(connTagMCF);
    result.insert(std::pair<std::string,std::vector<std::string> >(*actionName,connTags));
    std::cout << "PixBrokerMultiCrate::getConnectivityTags(): Got tags for action " << *actionName << std::endl;
    if(connTagCc != 0) delete [] connTagCc;
    if(connTagPc != 0) delete [] connTagPc;
    if(connTagPc != 0) delete [] connTagAc;
    if(connTagCFc != 0) delete [] connTagCFc;
    if(connTagOIc != 0) delete [] connTagOIc;
    if(connTagCFOIc != 0) delete [] connTagCFOIc;
    if(connTagMCFc != 0) delete [] connTagMCFc;
    std::cout << "PixBrokerMultiCrate::getConnectivityTags(): Next action " << std::endl;
  }
  return result;
}

std::string PixBrokerMultiCrate::getBrokerNameFromAction(std::string actionName)
{
  std::map<std::string,std::vector<std::string> >::iterator brokerAction, brokerActionEnd = m_brokerActions.end();
  std::string brokerName = "";
  for(brokerAction = m_brokerActions.begin(); brokerAction != brokerActionEnd; brokerAction++){

    std::vector<std::string> actionList = brokerAction->second;
    std::vector<std::string>::iterator action, actionEnd = actionList.end();

    for(action = actionList.begin(); action != actionEnd; action++){
      if(actionName == (*action)){
	brokerName = brokerAction->first;
	break;
      }
    }
    if(brokerName != "") break;
  }

  if(brokerName != ""){
    std::map<std::string, ipc::PixBroker_var>::iterator broker, brokerEnd = m_subBrokers.end();
    for(broker = m_subBrokers.begin(); broker != brokerEnd; broker++){
      if(broker->first == brokerName) return broker->first;
    }
  }
  return "";
}
    
void PixBrokerMultiCrate::insertActionInBrokerActionMap(std::string brokerName, std::string actionName)
{

  std::map<std::string, std::vector<std::string> >::iterator actionList = m_brokerActions.find(brokerName);
  if(actionList != m_brokerActions.end()) actionList->second.push_back(actionName);

}

void PixBrokerMultiCrate::deleteActionInBrokerActionMap(std::string brokerName, std::string actionName)
{

  std::map<std::string, std::vector<std::string> >::iterator actionList = m_brokerActions.find(brokerName);
  if(actionList != m_brokerActions.end()){
    std::vector<std::string>::iterator action, actionEnd = actionList->second.end();
    for(action = actionList->second.begin(); action != actionEnd; action++){
      if(actionName == (*action)){
	actionList->second.erase(action);
	break;
      }
    }
  }

}

std::string PixBrokerMultiCrate::getpidstr()
{
  std::ostringstream oss;
  oss << getpid();
  return oss.str();
}

std::string PixBrokerMultiCrate::gethoststr()
{
  char host[64];
  int size = 64;
  if(gethostname(host,size) == 0){
    std::cout << "HOST in MultiCrate" << std::endl;
    std::cout << std::string(host) << std::endl;
    return std::string(host);
  }else{
    std::cout << "HOST is NULL!" << std::endl;
    return std::string();
  }
}
