/////////////////////////////////////////////////////////////////////
// PixBroker.cxx
// version 1.1
/////////////////////////////////////////////////////////////////////
//
// 03/04/06  Version 1.0 (CS)
//           Initial release
//
// 12/09/06  Version 1.1 (CS)
//           First IPC version
//

//! Base class for the Pixel resource broker

#include <sys/types.h>
#include <unistd.h>

#include "PixBroker/PixBroker.h"
#include "PixUtilities/PixMessages.h"

using namespace PixLib;


//! Constructor
PixBroker::PixBroker(IPCPartition *ipcPartition, std::string ipcName) :
  IPCNamedObject<POA_ipc::PixBroker, ipc::single_thread>(*ipcPartition,ipcName.c_str()),
  m_ipcPartition(ipcPartition) {
  // Create mutex
  std::string mutexName = "Broker_Mutex_"+ipcName;
  m_mutex = new PixMutex(mutexName);  
  // Publish in IPC partition
  publish();
}


//! Destructor
PixBroker::~PixBroker() {
  // Withdraw from IPC partition
  withdraw();
  // Destroy mutex
  delete m_mutex;
}


//! Resource listing

std::set<std::string> PixBroker::listSubBrokers() {
  
  PixLock lock(m_mutex);
  std::string msgText = "PixLock acquired";

  // Fill sub broker names vector
  std::set<std::string> subBrokerList;
  // Add sub brokers
  std::map<std::string,ipc::PixBroker_var>::iterator subBroker, subBrokerEnd=m_subBrokers.end();
  for(subBroker=m_subBrokers.begin(); subBroker!=subBrokerEnd; subBroker++) {
    subBrokerList.insert(subBroker->first);
  }

  // Add sub brokers of sub brokers
  for(subBroker=m_subBrokers.begin(); subBroker!=subBrokerEnd; subBroker++) {
    ipc::stringList* subSubBrokers = (subBroker->second)->ipc_listSubBrokers();
    for(unsigned int i=0; i<subSubBrokers->length(); i++){
      subBrokerList.insert(std::string((*subSubBrokers)[i]._retn()));
    }
  }

  // Return names list
  return subBrokerList;
}


std::set<std::string> PixBroker::listActions(bool checkAvailability,
					     PixActions::Type type,
					     std::string allocatingBrokerName,
					     std::string managingBrokerName,std::string caller) {
  
  // Fill action names vector
  std::set<std::string> actionsList;

  PixLock lock(m_mutex);
  std::string msgText = "PixLock acquired";

  // List broker actions
  std::map<std::string,PixActions*>::iterator actions, actionsEnd=m_actions.end();
  for(actions=m_actions.begin(); actions!=actionsEnd; actions++) {
    // Get descriptor
    PixActions::PixActionsDescriptor descr=actions->second->descriptor();
    // Perform checks
    if(!checkAvailability || descr.available) {
      if(type==PixActions::ANY_TYPE || descr.type==type) {
	if(allocatingBrokerName=="" || descr.allocatingBrokerName==allocatingBrokerName) {
	  if(managingBrokerName=="" || descr.managingBrokerName==managingBrokerName) {
	    actionsList.insert(descr.name);
	  }
	}
      }
    }
  }

  // Add sub broker actions
  ipc::PixActionsType ipc_type = (ipc::PixActionsType((int)type));

  std::map<std::string,ipc::PixBroker_var>::iterator subBroker, subBrokerEnd=m_subBrokers.end();
  for(subBroker=m_subBrokers.begin(); subBroker!=subBrokerEnd; subBroker++) {
    int retry = 3;
    while (retry > 0) {
      try {
        ipc::stringList* subActions = (subBroker->second)->ipc_listActionsNames(checkAvailability,
                                                                               ipc_type, allocatingBrokerName.c_str(),
									        managingBrokerName.c_str());
        for(unsigned int i=0; i<subActions->length(); i++){
          std::string temp = std::string((*subActions)[i]._retn());
          actionsList.insert(temp);
        }
        retry = 0;
      }
      catch (...) {
        retry--;
	if(retry==0){
	  std::string msgTextStr = "Retry failed.";
	  ers::info(PixLib::pix::daq (ERS_HERE,m_ipcPartition->name(),msgTextStr));
	}
      }
    }
  }

  return actionsList;
}


//! IPC resource listing

char* PixBroker::ipc_name() {
  int length = m_name.size();
  char* ret = new char[length+1];
  for(int i=0; i<=length; i++) ret[i]=(m_name.c_str())[i];
  return ret;
}

ipc::stringList* PixBroker::ipc_listSubBrokers() {
  
  PixLock lock(m_mutex);
  std::string msgText = "PixLock acquired";
  // Get list of sub-brokers
  std::set<std::string> tempSubBrokers = listSubBrokers();
  std::set<std::string>::iterator subBroker = tempSubBrokers.begin();
  
  // Copy strings in stringList and return
  ipc::stringList* subBrokers = new ipc::stringList(tempSubBrokers.size());
  subBrokers->length(tempSubBrokers.size());
  for(unsigned int i=0; i<tempSubBrokers.size(); i++, subBroker++) {
    (*subBrokers)[i] = subBroker->c_str();
  }
  return subBrokers;
  
}


ipc::stringList* PixBroker::ipc_listActionsNames(bool checkAvailability,
						 ipc::PixActionsType type,
						 const char* allocatingBrokerName,
						 const char* managingBrokerName) {
  
  PixLock lock(m_mutex);
  // Get list of actions
  PixActions::Type pixlib_type = (PixActions::Type((int)type));
  std::set<std::string> tempActions = listActions(checkAvailability, pixlib_type, allocatingBrokerName, managingBrokerName);
  std::set<std::string>::iterator action = tempActions.begin();
  
  // Copy strings in stringList and return
  ipc::stringList* actions = new ipc::stringList(tempActions.size());
  actions->length(tempActions.size());
  for(unsigned int i=0; i<tempActions.size(); i++, action++) {
    int length = action->length();
    char* ret = new char[length+1];
    for(int j=0; j<=length; j++) ret[j]=(action->c_str())[j];
    (*actions)[i] = ret;
  }

  return actions;
  
}


//! Resource allocation

char* PixBroker::ipc_allocateActions(const ipc::stringList &actionNames, const char* allocatingBrokerName) {
  PixLock lock(m_mutex);
  // Copy action names in set of string
  std::set<std::string> actionsList;
  for(unsigned int i=0; i<actionNames.length(); i++) {
    char* actionName = ((ipc::stringList)actionNames)[i]._retn();
    actionsList.insert(std::string(actionName));
  }

  // Call local allocateActions
  PixActions* action=0;
  try {
    action = allocateActions(actionsList,allocatingBrokerName);
  } catch(...) {
    // TODO
  }

  if(action==0) {
    char* ret = new char[2];
    ret[0] = '\n';
    ret[1] = '\0';
    return ret;
  } else { 
    int length = action->name().length();
    char* ret = new char[length+1];
    for(int i=0; i<=length; i++) ret[i]=(action->name().c_str())[i];
    return ret;
  }
}

bool PixBroker::ipc_deallocateAction(const char* actionName, const char* deallocatingBrokerName) {
  PixLock lock(m_mutex);  
  if(m_actions.find(actionName) == m_actions.end()){
    std::cout << "WARNING in PixBroker: action being deallocated is not managed by this broker " << actionName << std::endl;
    return true; //throw some exception
  }  
  try {
    deallocateActions((*(m_actions.find(actionName))).second,deallocatingBrokerName);
  } catch(...) {
    // TODO
    std::cout << "exception caught in ipc_deallocateAction" << std::endl;
  }
  return true;
}

void PixBroker::ipc_deallocateAll() {
  deallocateAll();
}

void PixBroker::deallocateAll() {
  PixLock lock(m_mutex);
  std::map<std::string,PixActions*>::iterator actions, actionsEnd=m_actions.end();
  for(actions=m_actions.begin(); actions!=actionsEnd; actions++) deallocateActions((*actions).second,(((*actions).second)->allocatingBrokerName()).c_str());
}

//! Callback handling
void PixBroker::ipc_setCallback(ipc::PixBrokerCallback_ptr cb) {
  PixLock lock(m_mutex);
  std::cout << "PixBroker::ipc_setCallback : START" << std::endl;
  // Get callback and save number of current connection
  m_cb = ipc::PixBrokerCallback::_duplicate(cb);
  // Increase connections counter
  m_cb->increaseConnections();
  std::cout << "PixBroker::ipc_setCallback : END" << std::endl;
}

void PixBroker::ipc_unsetCallback() {
  PixLock lock(m_mutex);
  // Get callback and save number of current connection
  if(m_cb != 0){
    m_cb->decreaseConnections();
    m_cb = 0;
  }
}


//! IPC connectivity DB update
void PixBroker::ipc_updateConnectivityDB(const char* connTag, const char* confTag){
  PixLock lock(m_mutex);
  try{
    updateConnectivityDB(std::string(connTag),std::string(confTag));
  }catch(...){
    std::cout << "exception in updateConnDB " << name() << std::endl;
  }
  std::cout << "returning from ipc_updateConnDB " << name() << std::endl;
}

// IPC connectivity update from IS
void PixBroker::ipc_updateConnectivityFromIS(const char* idTag, const char* connTag, const char* cfgTag, const char* modCfgTag){
  PixLock lock(m_mutex);
  std::cout << "In PixBroker::ipc_updateConnectivityFromIS " << name() << std::endl;
  try{
    updateConnectivityFromIS(std::string(idTag),std::string(connTag),std::string(cfgTag),std::string(modCfgTag));
    std::cout << "PixBroker::ipc_updateConnectivityFromIS : tried updateConnectivityFromIS" << name() << std::endl;
  }catch(...){
    std::cout << "PixBroker::ipc_updateConnectivityFromIS : Exception in updateConnectivityFromIS " << name() << std::endl;
  }
  std::cout << "Returning from PixBroker::ipc_updateConnectivityFromIS " << name() << std::endl;
}


//! Resource management

void PixBroker::destroyActions() {
  PixLock lock(m_mutex);
  // Destroy actions
  std::map<std::string,PixActions*>::iterator actions, actionsEnd=m_actions.end();
  for(actions=m_actions.begin(); actions!=actionsEnd; actions++) actions->second->_destroy();
  m_actions.clear();
}

void PixBroker::updateConnectivityDB(std::string /*connTag*/, std::string /*confTag*/) {}

void PixBroker::updateConnectivityFromIS(std::string /*idTag*/, std::string /*connTag*/, std::string /*cfgTag*/, std::string /*modCfgTag*/) {} 

MY_LONG_INT PixBroker::ipc_test() {
  return getpid();
}

MY_LONG_INT PixBroker::ipc_emergency(const char* action, const char* par) {
  PixLock lock(m_mutex);
  std::string action_s = action;
  std::string par_s = par;

  std::cout << "EMERGENCY Dummy " << action_s << " " << par_s << std::endl;

  return 0; 
}

void PixBroker::ipc_restartActions() {
  PixLock lock(m_mutex);
  std::cout << "RESTART_ACTIONS Dummy " << std::endl;
  m_restartActionsCompleted = true;
}

bool PixBroker::ipc_restartActionsCompleted() {
  return m_restartActionsCompleted;
}
