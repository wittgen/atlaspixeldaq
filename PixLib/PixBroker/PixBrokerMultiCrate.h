/////////////////////////////////////////////////////////////////////
// PixBrokerMultiCrate.h
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 12/09/06  Version 1.0 (CS)
//           Initial release
//

//! Class for the Pixel single crate resource broker

#ifndef _PIXLIB_BROKERMULTICRATE
#define _PIXLIB_BROKERMULTICRATE

#include "PixBroker/PixBroker.h"


// Forward declarations
class IPCPartition;


namespace PixLib {

  // Forward declarations
  class PixActions;

  class PixBrokerMultiCrate : public PixBroker {
    
  public:
    
    // Constructor
    PixBrokerMultiCrate(std::string brokerName, IPCPartition *ipcPartition);
    // Destructor
    ~PixBrokerMultiCrate();
    
    // Resource allocation
    virtual PixActions* allocateActions(std::set<std::string> actionNames);
    virtual void deallocateActions(PixActions* action);
    virtual void updateConnectivityDB(std::string connTag, std::string confTag);
    virtual void updateConnectivityFromIS(std::string idTag, std::string connTag, std::string cfgTag, std::string modCfgTag);
    void updateConnectivityFromIS(std::set<std::string> allocatedBrokerName);

    virtual std::map<std::string,std::vector<std::string> > getConnectivityTags();

  private:
    
    virtual PixActions* allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName);
    virtual void deallocateActions(PixActions* action, const char* allocatingBrokerName);
    std::string getBrokerNameFromAction(std::string actionName);

    void insertActionInBrokerActionMap(std::string brokerName, std::string actionName);
    void deleteActionInBrokerActionMap(std::string brokerName, std::string actionName);

    std::map<std::string,std::vector<std::string> > m_brokerActions;

    std::string getpidstr();
    std::string gethoststr();
    int m_actionsCount;
    
  };
  
}

#endif // _PIXLIB_BROKERMULTICRATE
