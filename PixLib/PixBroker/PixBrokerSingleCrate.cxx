/////////////////////////////////////////////////////////////////////
// PixBrokerSingleCrate.cxx
// version 1.0
/////////////////////////////////////////////////////////////////////
//
// 03/04/06  Version 1.0 (CS)
//           Initial release
//

//! Class for the Pixel local resource broker

#include <sstream>

#include "PixModuleGroup/PixModuleGroup.h"
#include "PixConnectivity/PartitionConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixConnectivity/TimConnectivity.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/OBConnectivity.h"
#include "PixConnectivity/Pp0Connectivity.h"
#include "PixConnectivity/ModuleConnectivity.h"
#include "PixUtilities/PixMessages.h"

#include "PixActions/PixActionsSingleIBL.h"
#include "PixActions/PixActionsMulti.h"
#include "PixActions/PixActionsSingleTIM.h"

#include "PixBroker/PixBrokerSingleCrate.h"
#include "PixThreads/PixThread.h"


using namespace PixLib;



//! Constructor

PixBrokerSingleCrate::PixBrokerSingleCrate(IPCPartition *ipcPartition, std::string crateName, 
					   PixDbServerInterface *DbServer, PixHistoServerInterface *hInt,
					   PixConnectivity* conn, 
					   std::string isServerName) :
  PixBroker(ipcPartition, crateName), m_conn(conn), m_isServerName(isServerName) {
  
  // Set broker name
  m_name = crateName;
  m_flavour = m_name.substr (0,m_name.find(":"));
  std::cout<<"Broker flavour "<<m_flavour<<std::endl;

  
  //Create actions from connectivity database and ISManager
  std::cout << "Called PixBrokerSingleCrate Constructor " << std::endl;
  createActions(DbServer, hInt);

  //?????
   try {
     //m_is = new PixISManager(ipcPartition->name(),"PixelRunParams");
     m_is = new PixISManager(ipcPartition,"PixelRunParams");
  } catch(...) {
    std::cout << "ERROR!! Problems while connecting to the partition!" << std::endl;
  }
  m_DbServer = DbServer;
  m_hInt = hInt;
  
  generateCfgObjectMap();
  m_checkSum =getCheckSumDb();

  std::cout << "  ..... exiting PixBrokerSingleCrate Constructor " << std::endl; 
}

PixBrokerSingleCrate::~PixBrokerSingleCrate()
{
  if(m_is != 0)  {
    //removeISVariables();
    delete m_is;
  } 
}

PixActions* PixBrokerSingleCrate::allocateActions(std::set<std::string> actionNames) {
  return allocateActions(actionNames, m_name.c_str());
}

void PixBrokerSingleCrate::deallocateActions(PixActions* action) {
  deallocateActions(action, m_name.c_str());
}

PixActions* PixBrokerSingleCrate::allocateActions(std::set<std::string> actionNames, const char* allocatingBrokerName) {
  // Filter out actions not belonging to this broker
  std::set<std::string> myActionNames, myActions = listActions(true,PixActions::ANY_TYPE);
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
  std::cout<<__PRETTY_FUNCTION__<<std::endl;
  
  PixActions* allocated;
  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
    std::cout<<std::string(*actionName)<<std::endl;
    if(myActions.find(*actionName)!=myActions.end()) {
      allocated = (*(m_actions.find(*actionName))).second;
      allocated->allocate(allocatingBrokerName);
    } else {
      std::cout<<__PRETTY_FUNCTION__<<" Action not found "<<std::endl;
      //throw exception();//TODO: define what needs to be thrown, really
      //throw exception("PixBrokerSingleCrate::allocateActions: One of the actions required to allocate does not exist in the associated sub broker.");
    }
  }
  return 0;
}

void PixBrokerSingleCrate::deallocateActions(PixActions* action, const char* deallocatingBrokerName) {

  if(m_actions.find(action->name()) == m_actions.end()){
    std::cout << "ERROR in PixBrokerSingleCrate: action being deallocated is not managed by this broker" << std::endl;
    return; //throw some exception
    }  
  // Deallocate action
  std::cerr << "deallocating SC" << std::endl;
  action->deallocate(deallocatingBrokerName);
  std::cerr << "deallocated SC" << std::endl;
}

void PixBrokerSingleCrate::updateConnectivityDB(std::string tagConn, std::string tagCfg)
{

  std::set<std::string> actionNames = listActions(false,PixActions::ANY_TYPE);
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
  PixActions* allocated;
  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
     allocated = (*(m_actions.find(*actionName))).second;
     if(allocated != 0) deallocateActions(allocated, allocated->ipc_allocatingBrokerName());
  }
  try{
    destroyActions();
  }catch(...){
    std::cout << "destroying actions threw exception" << std::endl;
  }
  m_conn->clearConn();
    std::cout << "deleting m_conn" << std::endl;
  try{
    delete m_conn;
  }catch(...){
    std::cout << "deleting m_conn threw exception" << std::endl;
  }
  std::cout << "creating conn" << std::endl;

  try{
    if (tagConn == "" && tagCfg == "") {
      m_conn = new PixConnectivity();
    } else {
      m_conn = new PixConnectivity(tagConn, tagConn, tagConn, tagCfg);
    }
  }catch(...){
    std::cout << "creating conn threw exception" << std::endl;
  }
    std::cout << "creating actions" << std::endl;
  try{
    m_conn->loadConn();
    generateCfgObjectMap();//Connectivity might have changed
    m_checkSum =getCheckSumDb();
    createActions(m_DbServer, m_hInt);
  }catch(...){
    std::cout << "creating actions threw exception" << std::endl;
  }
  std::cout << "in update conn DB" << std::endl;
  //sleep(10);
  std::cout << "done updating conn DB" << std::endl;
  m_cb->decreaseConnections();

}


void PixBrokerSingleCrate::updateConnectivityFromIS(std::string tagId, std::string tagConn, std::string tagCfg, std::string tagModCfg)
{
  std::cout << "Inside PixBrokerSingleCrate::updateConnectivityFromIS" << std::endl;
  // Try to update tags from IS, show message if not available
  if (tagId == "" && tagConn == "" && tagCfg == "" && tagModCfg == "") { 
    try {
      std::string idt = ""; 
      idt = m_is->read<string>("DataTakingConfig-IdTag");
      std::string cot = "";
      cot = m_is->read<string>("DataTakingConfig-ConnTag");
      std::string cft = "";
      cft = m_is->read<string>("DataTakingConfig-CfgTag");
      std::string mcft = "";
      mcft = m_is->read<string>("DataTakingConfig-ModCfgTag");
      if (idt != "") tagId = idt;
      if (cft != "") tagCfg  = cft;
      if (cot != "") tagConn = cot;
      if (mcft != "") tagModCfg  = mcft;
      std::cout << "updateConnFromIS: WARNING! Tags are now set to "+tagId+"/"+tagConn+"/"+tagCfg+"/"+tagModCfg << std::endl; 
    } catch (...) {
      ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::updateConnectivityFromIS: ","Problems getting new tags from IS!"));
      std::cout << "updateConnFromIS: ERROR! Problems getting new tags from IS!" << std::endl;
    }
  }
  
  //Update only if tags are not empty
  if (tagConn != "" && tagCfg != "" && tagId != "" && tagModCfg != ""){
    std::cout << "Attempting to call setConnectivityTags" << std::endl;
    setConnectivityTags(tagConn, tagCfg, tagId, tagModCfg);
  }
  
  m_cb->decreaseConnections();
}



void PixBrokerSingleCrate::setConnectivityTags(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg)
{
  std::cout << "Original ConnTag = " << m_conn->getConnTagC()<< std::endl;
  std::cout << "Original CfgTag = " << m_conn->getCfgTag()<< std::endl;
  std::cout << "Original IdTag = " << m_conn->getConnTagOI()<< std::endl;
  std::cout << "Original ModCfgTag = " << m_conn->getModCfgTag()<< std::endl;
  
  //If tagConn is different from original, destroy and recreate actions with new connectivity
  if (tagConn != m_conn->getConnTagC()){
    std::cout<< "PixBrokerSingleCrate::setConnectivityTags : Trying to change connectivity tags"<<std::endl;
    setConnTag(tagConn, tagCfg, tagId, tagModCfg);
  } else {
  // else destroy and recreate module group with new connectivity
   std::cout<< "PixBrokerSingleCrate::setConnectivityTags:  Trying to change cfg tags"<<std::endl;
   setCfgTag(tagConn, tagCfg, tagId, tagModCfg);
  }  
}


void PixBrokerSingleCrate::setConnTag(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg)
{
  loadNewConn(tagConn, tagCfg, tagId, tagModCfg);

  // Destroy Actions
  try{
    std::set<std::string> actionNames = listActions(false,PixActions::ANY_TYPE);
    std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
    PixActions* Action = 0;
    for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
      Action = (*(m_actions.find(*actionName))).second;
      m_actions.erase(*actionName);
      Action->_destroy();
      std::cout << "PixBrokerSingleCrate::setConnTag : Actions have been destroyed" << std::endl;
    }
  }catch(...){
    ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::setConnTag","Destroying actions threw exception"));
    std::cout << "PixBrokerSingleCrate::setConnTag : Destroying actions threw exception" << std::endl;
  }
  // Recreate actions with new connectivity
  try{
    createActions(m_DbServer, m_hInt);
    std::cout << "setConnTag: Creating actions" << std::endl;
  }catch(...){
    ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::setConnTag","Creating new actions threw exception"));
    std::cout << "setConnTag: Creating actions threw exception" << std::endl;
    }
}

void PixBrokerSingleCrate::setCfgTag(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg)
{ 
  //Check published tags and revisions
  if(m_conn->getCfgTag() == tagCfg && m_conn->getModCfgTag() == tagModCfg){
    unsigned int checkSumDb = getCheckSumDb();
    std::cout<<"CheckSum: Current: "<<m_checkSum<<" DB: "<<checkSumDb<<std::endl;
    if(m_checkSum == checkSumDb) return;
    m_conn->loadConn();
    m_checkSum =checkSumDb;
  }else loadNewConn(tagConn, tagCfg, tagId, tagModCfg);
  
  std::cout<<"Loading new Conn "<<std::endl;
  ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::setCfgTag","Updating new connectivity"));
  
  RodCrateConnectivity *crate = getRodCrateConnectivity();
  if(crate==NULL){
  ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::setCfgTag","Cannot get crate connectivity"));
  return;
  }

  std::string crateName = m_name.substr (m_name.find(":")+1,m_name.length());
  std::map<int, RodBocConnectivity*>::iterator k;
  //loop over rodBocs (RodBocConnectivity)
  if(m_flavour == "CRATE"){
    for (k=crate->rodBocs().begin(); k!=crate->rodBocs().end(); ++k) {
      std::cout << "    ROD " << ((*k).second)->name() << std::endl;
      std::cout << "    ((*k).second) " << ((*k).second) << std::endl;
      m_conn->rods[((*k).second)->name()] = (*k).second;
      RodBocConnectivity *rodBocConn = (*k).second; 
      std::string singleActionName = "SINGLE_ROD:";
      singleActionName+=crateName+"/"+rodBocConn->name();
      PixActionsSingleIBL *ActionsSingleROD = dynamic_cast<PixActionsSingleIBL*>((*(m_actions.find(singleActionName))).second);  
      if(ActionsSingleROD != 0){
	std::cout<< "PixBrokerSingleCrate::setCfgTag: Attemping to call setNewCfg() "<<singleActionName << std::endl;
	ActionsSingleROD->setNewCfg(m_conn, rodBocConn);
	std::cout<< "PixBrokerSingleCrate::setCfgTag: Called setNewCfg() "<<singleActionName << std::endl;
      }
    }//end loop over rodBocs
  }else if (m_flavour == "TIM"){
     std::string timName = string("SINGLE_TIM:");
     timName += crateName+"/"+crate->tim()->name();
     PixActionsSingleTIM* timAction = dynamic_cast<PixActionsSingleTIM*>((*(m_actions.find(timName))).second); 
     
     if(timAction != 0){
	std::cout<< "PixBrokerSingleCrate::setCfgTag: Attemping to call setNewCfg() "<<timName << std::endl;
	timAction->setNewCfg(m_conn);
	std::cout<< "PixBrokerSingleCrate::setCfgTag: Called setNewCfg() "<<timName << std::endl;
      }
  }

}

void PixBrokerSingleCrate::loadNewConn(std::string tagConn, std::string tagCfg, std::string tagId, std::string tagModCfg)
{
  
  // Clear old Connectivity
  m_conn->clearConn();
  try{
    delete m_conn;
    m_conn = 0;
    std::cout << "PixBrokerSingleCrate::loadNewConn: Deleting m_conn. Now m_conn=" << m_conn <<std::endl;
  }catch(...){
    std::cout << "PixBrokerSingleCrate::loadNewConn: Deleting m_conn threw exception" << std::endl;
    ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::loadNewConn()","Deleting connectivity threw exception"));
  }
  // Get new connectivity from DbServer
  try{
    m_conn = new PixConnectivity(m_DbServer, tagConn, tagConn, tagConn, tagCfg, tagId, tagModCfg);
    std::cout << "PixBrokerSingleCrate::loadNewConn: Creating m_conn. Now m_conn="<<m_conn << std::endl;
  }catch(...){
    std::cout << "PixBrokerSingleCrate::loadNewConn: Creating PixConnectivity with new tags threw exception" << std::endl;
    ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::loadNewConn","Creating connectivity with new tags threw exception"));
  }
  // Load new connectivity
  try{
    m_conn->loadConn();
    generateCfgObjectMap();//Connectivity might have changed
    m_checkSum =getCheckSumDb( );
    std::cout << "PixBrokerSingleCrate::loadNewConn: Loading connectivity. Now m_conn="<<m_conn << std::endl;
  }catch(...){
    std::cout << "PixBrokerSingleCrate::loadNewConn : Loading new connectivity threw exception" << std::endl;
    ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::loadNewConn","Loading new connectivity threw exception"));
  }
}


void PixBrokerSingleCrate::createActions(PixDbServerInterface *DbServer, PixHistoServerInterface *hInt)
{
  
  RodCrateConnectivity *c = getRodCrateConnectivity();
  if(c==NULL){
  ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::createActions","Cannot get crate connectivity"));
  return;
  }

  std::string crateName = m_name.substr (m_name.find(":")+1,m_name.length());


    // Check if crate is enabled
   if(c->enableReadout ) {
      if(m_flavour == "CRATE"){
        // Loop on RODs
        std::map<int, RodBocConnectivity*>::iterator k;
        std::cout << " crate " << crateName << " has " << c->rodBocs().size() << " rods" << std::endl;
        for (k=c->rodBocs().begin(); k!=c->rodBocs().end(); ++k) {
	  std::cout << "    ROD " << ((*k).second)->name() << std::endl;
	  m_conn->rods[((*k).second)->name()] = (*k).second;
	  RodBocConnectivity *r = (*k).second;
	  // Check if ROD is enabled
	  if(r->enableReadout && (r->rodBocType() == "CPPROD" || r->rodBocType() == "L12ROD")) {
	    PixActions* action;
	    std::string singleActionName = "SINGLE_ROD:";
	    singleActionName+=crateName+"/"+r->name();
	    try {
	        action = new PixActionsSingleIBL(singleActionName, crateName, DbServer, hInt, "",m_ipcPartition,r, m_conn);
	    } catch(...) {
	      // TODO: throw
	      std::cout << "ERROR!!! found in PixBrokerSingleCrate::PixBrokerSingleCrate" << std::endl;
	    }
	    m_actions.insert(std::make_pair(singleActionName,action));
	  }
        }
      } else if (m_flavour=="TIM"){
          if (c->tim() != NULL) {
	    //Create TIM action for now with dummy inquire
	    std::string timName = string("SINGLE_TIM:");
	    timName += crateName+"/"+c->tim()->name();
	    PixActions* timAction = new PixActionsSingleTIM(timName, crateName, DbServer, hInt, "",m_ipcPartition,c->tim(), m_conn);
	    m_actions.insert(std::make_pair(timName,timAction));
          } else {
            std::string msg ="Cannot check if is a TIM or a CRATE (RODs) from name" + m_name;
            ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::createActions",msg));
          }
      }
    }
}



void PixBrokerSingleCrate::destroyActions()
{
  std::set<std::string> actionNames = listActions(false,PixActions::ANY_TYPE);
  std::set<std::string>::iterator actionName, actionNameEnd=actionNames.end();
  PixActions* allocated = 0;
  for(actionName=actionNames.begin(); actionName!=actionNameEnd; actionName++) {
    allocated = (*(m_actions.find(*actionName))).second;
     deallocateActions(allocated, "EXPERT:");//in case it is allocated, even though it shouldn't,
                                             //only the EXPERT broker should have access to this,
                                             //and it should make sure that all are deallocated beforehand
     m_actions.erase(*actionName);
     allocated->_destroy();
  }

}


void PixBrokerSingleCrate::periodicTasks() {

    std::set <std::string> toDeallocate;
  
    std::map< std::string, std::set<std::string> > allocatingBrokerMap;
    std::map< std::string, std::set<std::string> >::iterator allBroker;
    PixActions* allocated;

    //Check allocated actions
    std::set<std::string> actionNames = listActions(false,PixActions::ANY_TYPE);
    std::set<std::string>::iterator actionName;
    for(actionName=actionNames.begin(); actionName!=actionNames.end(); actionName++) {
     allocated = (*(m_actions.find(*actionName))).second;
     if(allocated != 0)
       if(std::string(allocated->ipc_allocatingBrokerName()) != "")allocatingBrokerMap[std::string(allocated->ipc_allocatingBrokerName())].insert(std::string(*actionName));
  }

     std::map<std::string,ipc::PixBroker_var> brokers;
     std::map<std::string,ipc::PixBroker_var>::iterator broker;
     //std::cout << "Obtaining brokers from the partition" << std::endl;
     m_ipcPartition->getObjects<ipc::PixBroker>(brokers);

     if(allocatingBrokerMap.size()==0)return;

      //Check existence of allocating broker
     for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++){
       broker = brokers.find(allBroker->first);
       if(broker == brokers.end()){
         std::cout<<"Broker "<<allBroker->first<<" not found in ipc list, deallocating"<<std::endl;
         toDeallocate.insert(broker->first);
       } else {
         unsigned int count = 0;
         bool ok = true;
           do {
             try {
	       ipc::PixBroker_var ipc_null = 0;
	         if (broker->second == ipc_null) {
	          std::cout << allBroker->first << " PID = UNKNOWN (no connection available)" << std::endl;
	     } else {
               try {
	         std::cout << allBroker->first << " PID = " << broker->second->ipc_test() << std::endl;
                 ok = true;
               } catch (CORBA::TIMEOUT &ex) {
	         std::cout << allBroker->first << " TIMEOUT (CORBA::TIMEOUT)" << std::endl;
                 return;
               } catch (CORBA::TRANSIENT &ex) {
	         std::cout << allBroker->first << " TIMEOUT (CORBA::TRANSIENT) (count " << count++ << ")" << std::endl;
	         ok = false;
	         sleep(1);
               }
	    }
          } catch(...){
	    std::cout << "Cannot connect allocating broker for " << allBroker->first << " (count " << count++ << ")" << std::endl;
            ok = false;
          }
        } while (!ok && count < 5);

        if (!ok) {
          toDeallocate.insert(broker->first);
        }
       }
     }

  std::set <std::string>::iterator toDeall;
  //Deallocate actions
  try{
   for(toDeall=toDeallocate.begin();toDeall != toDeallocate.end();toDeall++){
     
       broker = brokers.find(std::string(*toDeall));
       allBroker = allocatingBrokerMap.find(std::string(*toDeall));
       if(broker == brokers.end() || allBroker == allocatingBrokerMap.end()){
       std::cout<<"Cannot get broker "<<std::string(*toDeall)<<std::endl;
       continue;
       }
       
       ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::MULTI,"",broker->first.c_str());
	if(subActions == 0 || subActions->length() == 0){
	  std::cout << "Broker Multi " << broker->first << " is managing no actions " << std::endl;
	  if(subActions != 0) delete subActions;
	  continue;
	}

	for(size_t i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  ipc::PixActions_var action = m_ipcPartition->lookup<ipc::PixActions,ipc::no_cache,ipc::narrow>(temp.c_str());
	  
	  
	  ipc::stringList* subActionsNames = 0;
	  
	  if (m_flavour== "CRATE")subActionsNames = action->ipc_listSubActions(ipc::SINGLE_ROD);
	  else if (m_flavour== "TIM")subActionsNames = action->ipc_listSubActions(ipc::SINGLE_TIM);
	  else continue;
	  
	  std::string msg = "Deallocating actions in broker"+allBroker->first;
	  ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::periodicTask",msg));
	  
	  for(size_t j=0; j<subActionsNames->length(); j++){
	  std::string temp2 = std::string((*subActionsNames)[i]._retn());
	    std::cout<<"SubAction "<<temp2<<std::endl;
	    if(allBroker->second.find(temp2) != allBroker->second.end()){
	      std::cout << "Deallocating action " << temp << " in broker " << broker->first << std::endl;
	      broker->second->ipc_deallocateAction(temp.c_str(),broker->first.c_str());
	      break;
	    }
	  }
	}
     }
     
     for(toDeall=toDeallocate.begin();toDeall != toDeallocate.end();toDeall++){
       allBroker = allocatingBrokerMap.find(std::string(*toDeall));
         if(allBroker != allocatingBrokerMap.end()){
           allocated = (*(m_actions.find(*actionName))).second;
             if(allocated != 0){
               deallocateActions(allocated, allocated->ipc_allocatingBrokerName());
               }
         }
     }
     
   } catch (...) {
       ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::periodicTask","ERROR was thrown"));
   }


}


MY_LONG_INT PixBrokerSingleCrate::ipc_emergency(const char* action, const char* par) {
  std::string action_s = action;
  std::string par_s = par;

  return emergency(action_s, par_s);

}

MY_LONG_INT PixBrokerSingleCrate::emergency(std::string actionStr, std::string par) {
  
  std::cout << "EMERGENCY " << actionStr << " " << par << std::endl;
  
  std::string msg = "EMERGENCY ACTION "+actionStr+" "+par;
  ers::error(PixLib::pix::daq(ERS_HERE,"PixBrokerSingleCrate::emergency ",msg));
  
  MY_LONG_INT err_count = 0;

  try{
  
    std::map< std::string, std::set<std::string> > allocatingBrokerMap;
    std::map< std::string, std::set<std::string> >::iterator allBroker;
    PixActions* allocated;

    //Check allocated actions
    std::set<std::string> actionNames = listActions(false,PixActions::ANY_TYPE);
    std::set<std::string>::iterator actionName;
    for(actionName=actionNames.begin(); actionName!=actionNames.end(); actionName++) {
     allocated = (*(m_actions.find(*actionName))).second;
     if(allocated != 0)
       if(std::string(allocated->ipc_allocatingBrokerName()) != "")allocatingBrokerMap[std::string(allocated->ipc_allocatingBrokerName())].insert(std::string(*actionName));
  }

    if (actionStr == "EMERGENCY_PREAMP_KILL"){
    
      bool done = false;

      std::map<std::string,ipc::PixBroker_var> brokers;
      std::map<std::string,ipc::PixBroker_var>::iterator broker;
      std::cout << "Obtaining brokers from the partition" << std::endl;
      m_ipcPartition->getObjects<ipc::PixBroker>(brokers);

      if(allocatingBrokerMap.size()==0){
        ers::warning(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::emergency","No actions allocated, doing nothing"));
        return err_count;
      }
      
      //Deallocate actions from allocating broker
      for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++){
       broker = brokers.find(allBroker->first);
       if(broker != brokers.end()){
       ipc::stringList* subActions = broker->second->ipc_listActionsNames(false,ipc::MULTI,"",broker->first.c_str());
	if(subActions == 0 || subActions->length() == 0){
	  std::cout << "Broker Multi " << broker->first << " is managing no actions " << std::endl;
	  if(subActions != 0) delete subActions;
	  continue;
	}

	for(size_t i=0; i<subActions->length(); i++){
	  std::string temp = std::string((*subActions)[i]._retn());
	  ipc::PixActions_var action = m_ipcPartition->lookup<ipc::PixActions,ipc::no_cache,ipc::narrow>(temp.c_str());
	  
	  
	  ipc::stringList* subActionsNames = 0;
	  
	  if (m_flavour== "CRATE")subActionsNames = action->ipc_listSubActions(ipc::SINGLE_ROD);
	  else if (m_flavour== "TIM")subActionsNames = action->ipc_listSubActions(ipc::SINGLE_TIM);
	  else continue;
	  
	  for(size_t j=0; j<subActionsNames->length(); j++){
	  std::string temp2 = std::string((*subActionsNames)[i]._retn());
	    if(allBroker->second.find(temp2) != allBroker->second.end()){
	      std::cout << "Deallocating action " << temp << " in broker " << broker->first << std::endl;
	      broker->second->ipc_deallocateAction(temp.c_str(),broker->first.c_str());
	      done=true;
	      break;
	    }
	  }
	}
       }
     }
     
     for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++)   for(actionName=allBroker->second.begin(); actionName!=allBroker->second.end(); actionName++) {
          allocated = (*(m_actions.find(*actionName))).second;
          if(allocated != 0)deallocateActions(allocated, allocated->ipc_allocatingBrokerName());
      }

      //Allocating actions in this broker
      for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++)allocateActions(allBroker->second,m_name.c_str());

      for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++)   for(actionName=allBroker->second.begin(); actionName!=allBroker->second.end(); actionName++) {
          allocated = (*(m_actions.find(*actionName))).second;
           //Send config for allocating RODs
           if(allocated != 0){
            allocated->configure(m_ipcPartition->name());
	    allocated->connect();
	    allocated->disconnect();
            deallocateActions(allocated, allocated->ipc_allocatingBrokerName());
          }
        }

     if(done){
     ers::error(PixLib::pix::daq(ERS_HERE,"PixBrokerSingleCrate",m_name+" COMPLETED, you may have to ask for a TTC restart"));
     }

    } else if (actionStr == "RESTART_ACTIONS"){
      destroyActions();
      #define ACTIONS_HARD_RESET 
      #ifdef ACTIONS_HARD_RESET
      int pid = getpid();
      std::ostringstream cmd;
      cmd << "kill -KILL " << pid;
      std::string scmd = cmd.str();
      std::cout << "Executing command: " << scmd << std::endl;
      system(scmd.c_str());
      #else
      createActions(m_DbServer, m_hInt);
      #endif
 
    } else if (actionStr == "FORCE_PREAMP_KILL"){
    
       for(allBroker=allocatingBrokerMap.begin();allBroker !=allocatingBrokerMap.end();allBroker++)  for(actionName=allBroker->second.begin(); actionName!=allBroker->second.end(); actionName++){
         allocated = (*(m_actions.find(*actionName))).second;
         if(allocated != 0){
           allocated->stopTrigger();
           sleep(10);
           allocated->warmReconfig("PIXEL_DOWN", "0");
           sleep(20);
           allocated->startTrigger();
           sleep(10);
         }
      }

    } else {
       ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::emergency ","EMERGENCY ACTION "+actionStr+" NOT FOUND "));
    
    }
  } catch (...) {
      err_count ++;
  }

  return err_count; 
}

void PixBrokerSingleCrate::ipc_restartActions() {
  //m_restartActionsCompleted = false;
  emergency("RESTART_ACTIONS","-");
  //m_restartActionsCompleted = true;  
}

unsigned int PixBrokerSingleCrate::getCheckSumDb( ){

 unsigned int checkSum=0;
 std::string domain = "Configuration-"+m_conn->getConnTagOI();

 if(m_DbServer){   
   std::map  <std::string, std::vector<std::string>>::iterator it;
      for (it=m_cfgObjectMap.begin(); it!=m_cfgObjectMap.end(); ++it)
  	for(size_t i=0;i<it->second.size();i++)checkSum+=m_DbServer->getLastRev(domain,it->first,it->second.at(i));
 }

  std::cout<<"CheckSum "<<checkSum<<std::endl;
  return checkSum;

}

void PixBrokerSingleCrate::generateCfgObjectMap(){

 std::string tagCfg =m_conn->getCfgTag(), tagModCfg = m_conn->getModCfgTag();
 std::string domain = "Configuration-"+m_conn->getConnTagOI();
 m_cfgObjectMap.clear();

  if(m_DbServer){

     RodCrateConnectivity *crate = getRodCrateConnectivity();
       if(crate==NULL){
         ers::error(PixLib::pix::daq(ERS_HERE, "PixBrokerSingleCrate::generateCfgObject","Cannot get crate connectivity"));
         return;
       }

     if(m_flavour == "TIM"){//For the TIM we just save TIM name
       if (crate->tim() != NULL) m_cfgObjectMap[tagCfg].push_back(crate->tim()->name());
       return;
     }

     std::map<int, RodBocConnectivity*>::iterator k;
     std::vector<std::string> rodList;
     std::vector<std::string> moduleList;
       for (k=crate->rodBocs().begin(); k!=crate->rodBocs().end(); ++k){
         //std::cout<<((*k).second)->name()<<std::endl;
         rodList.push_back(((*k).second)->name());
           for (int obslot = 0; obslot < 4; obslot++){
             OBConnectivity* obConn = ((*k).second)->obs(obslot);
             if(obConn == NULL)continue;
               for(int moduleslot = 1; moduleslot < 9; moduleslot++){
		 ModuleConnectivity * modConn = obConn->pp0()->modules(moduleslot);
		 if(modConn == NULL)continue;
		 moduleList.push_back(modConn->prodId());
	       }
	}
     }

     std::vector<std::string> objlist,types;
     std::vector<std::string>::iterator objit,objitType;
     std::vector<unsigned int> count;

       m_DbServer->listTypes(domain, tagCfg, types, count);
       for(objitType=types.begin();objitType!=types.end();objitType++) {//Add cfg tag belonging to my RODs
	std::string &type = *objitType;
	
         if(type ==  "PixRunConfig" || type ==  "PixRunConfigSub" || type ==  "PixDisable" || type ==  "TimPixTrigController"  )continue;//Skip runConfig and TimPixController
         //std::cout<<(*objitType)<<std::endl;
         m_DbServer->listObjectsRem(domain,tagCfg,type,objlist);
         for(objit=objlist.begin();objit!=objlist.end();objit++)
           for(size_t i=0;i<rodList.size();i++){
             std::string &objectName = *objit;
             if(objectName.find(rodList[i]) != std::string::npos){
               m_cfgObjectMap[tagCfg].push_back(objectName);
               //std::cout<<objectName<<std::endl;
             }
           }
       }

       m_DbServer->listObjectsRem(domain,tagModCfg,objlist);
       for(objit=objlist.begin();objit!=objlist.end();objit++)
         for(size_t i=0;i<moduleList.size();i++){
             std::string &objectName = *objit;
              if(objectName.find(moduleList[i]) != std::string::npos){
                m_cfgObjectMap[tagModCfg].push_back(objectName);
                //std::cout<<objectName<<std::endl;
              }
       }
  }


}


RodCrateConnectivity* PixBrokerSingleCrate::getRodCrateConnectivity( ){

  RodCrateConnectivity *crateConn = NULL;
  std::string crateName = m_name.substr (m_name.find(":")+1,m_name.length());
  std::map<std::string, RodCrateConnectivity*>::iterator it;
  for (it=m_conn->crates.begin(); it!=m_conn->crates.end(); ++it) {
    //std::cout << " crateName is " <<  it->second->name() << " and searched name is " << crateName << std::endl;
    if (it->second->name() == crateName) {
      crateConn = it->second;
      break;
    }
  }

return crateConn;

}



