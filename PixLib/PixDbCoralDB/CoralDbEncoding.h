// Dear emacs, this is -*-c++-*-
//
// Templates to convert data of types listed in PixDbInterface.h from/to strings
// that can be stored via CoralDB.
//
// A.Gaponenko, 2007

#ifndef CORALDBENCODING_H
#define CORALDBENCODING_H

#include <sstream>
#include <string>
#include <vector>

namespace PixLib {

  class Histo;
  
  namespace CoralDbEncoding {

    // This encodes type ID but not content of the variable
    template<class T> void make_heading(const T& arg, std::ostream& s);

    // The functions below deal with the content of arg

    // general declaration
    template<class T> void encode(const T& arg, std::ostream& s);
    template<class T> void decode(T *arg, std::istream& s);
  
    // partial specialization for vectors
    template<class T> void encode(const std::vector<T>& arg, std::ostream& s);
    template<class T> void decode(std::vector<T>* arg, std::istream& s);

    // specialization for strings
    template<> void encode(const std::string& arg, std::ostream& s);
    template<> void decode(std::string* arg, std::istream& s);

    // specialization for Histo
    template<> void encode(const Histo& arg, std::ostream& s);
    template<> void decode(Histo* arg, std::istream& s);

    // vector<bool> is broken in STL, needs special handling
    template<> void decode(std::vector<bool>* arg, std::istream& s);
    
    //================================================================
    // A decoding helper
    void eatWhiteSpace(std::istream& is);

  }
}

// Implementation of the templates
#include "CoralDbEncoding.icc"

#endif/*CORALDBENCODING_H*/
