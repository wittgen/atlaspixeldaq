#include "PixDbCoralDB/CoralDbEncoding.h"

//================================================================
namespace PixLib {
  namespace CoralDbEncoding {
    typedef std::map<std::string, PixLib::PixDb::DbDataType> DbTypeMap;
    extern DbTypeMap typeMap;

    DbTypeMap typeMap;
    
    class TypeMapInitializer {
    public:
      TypeMapInitializer() {
	typeMap[typeid(bool).name()] = DBBOOL;
	typeMap[typeid(std::vector<bool>).name()] = DBVECTORBOOL;
	typeMap[typeid(int).name()] = DBINT;
	typeMap[typeid(std::vector<int>).name()] = DBVECTORINT;
	typeMap[typeid(unsigned).name()] = DBULINT;
	typeMap[typeid(float).name()] = DBFLOAT;
	typeMap[typeid(std::vector<float>).name()] = DBVECTORFLOAT;
	typeMap[typeid(double).name()] = DBDOUBLE;
	typeMap[typeid(std::vector<double>).name()] = DBVECTORDOUBLE;
	typeMap[typeid(Histo).name()] = DBHISTO;
	typeMap[typeid(string).name()] = DBSTRING;
      }
    };
    
    TypeMapInitializer dummy;


    //================================================================
    // 
    // The encoding schema is described in CoralDbEncoding.icc
    //

    //================================================================
    template<> void encode(const std::string& arg, std::ostream& s) {
      s<<arg.size()<<" "<<arg<<" ";
    }

    //----------------------------------------------------------------
    //need to know where to stop reading
    template<> void decode(std::string *arg, std::istream& s) {
      std::string::size_type length;
      if(! (s>>length) ) {
	throw std::runtime_error("PixLib::CoralDbEncoding::decode(string*,istream&): could not read length from the stream");
      }
      eatWhiteSpace(s);

      arg->clear();
      if(length) {
	std::vector<char> tmp;
	tmp.reserve(length);
	if(!s.read( &tmp[0], length)) {
	  std::ostringstream os;
	  os<<"PixLib::CoralDbEncoding::decode(string*,istream&): could not read required "<<length<<" characters from the stream";
	  throw std::runtime_error(os.str());
	}
	arg->assign( &tmp[0], length);
      }
      
      eatWhiteSpace(s);
    }

    //================================================================
    template<> void encode(const Histo& arg, std::ostream& s) {
      encode(arg.nDim(), s);
      encode(arg.name(), s);
      encode(arg.title(), s);
      
      switch(arg.nDim()) {
      case 0:
	break;
      case 1:
	encode(arg.nBin(0), s);
	encode(arg.min(0), s);
	encode(arg.max(0), s);
	for(int i=0; i<arg.nBin(0); i++) {
	  encode(arg(i), s);
	}
	break;
      case 2:
	encode(arg.nBin(0), s);
	encode(arg.min(0), s);
	encode(arg.max(0), s);
	encode(arg.nBin(1), s);
	encode(arg.min(1), s);
	encode(arg.max(1), s);
	for(int i=0; i<arg.nBin(0); i++) {
	  for(int j=0; j<arg.nBin(1); j++) {
	    encode(arg(i,j), s);
	  }
	}
	break;
      default: 
	ostringstream os;
	os<<"PixLib::CoralDbEncoding::encode(const Histo&,ostream): unknown nDim="<<arg.nDim();
	throw std::runtime_error(os.str());
      }
    }

    //----------------------------------------------------------------
    //need to know where to stop reading
    template<> void decode(Histo *arg, std::istream& s) {
      int nDim;
      decode(&nDim, s);

      string name, title;
      decode(&name, s);
      decode(&title, s);
      
      switch(nDim) {
      case 0: {
	*arg = Histo();
	break;
      }
      case 1: {
	int nbins;
	double xmin, xmax;
	decode(&nbins, s);
	decode(&xmin, s);
	decode(&xmax, s);
	
	*arg = Histo(name, title, nbins, xmin, xmax);

	for(int i=0; i<nbins; i++) {
	  double tmp;
	  decode(&tmp, s);
	  arg->set(i, tmp);	
	}

	break;	
      }
      case 2:
	int nbin1, nbin2;
	double xmin1, xmin2, xmax1, xmax2;
	decode(&nbin1, s);
	decode(&xmin1, s);
	decode(&xmax1, s);

	decode(&nbin2, s);
	decode(&xmin2, s);
	decode(&xmax2, s);

	*arg = Histo(name, title, nbin1, xmin1, xmax1, nbin2, xmin2, xmax2);

	for(int i=0; i<nbin1; i++) {
	  for(int j=0; j<nbin2; j++) {
	    double tmp;
	    decode(&tmp, s);
	    arg->set(i, j, tmp);	
	  }
	}
	break;
      default: 
	ostringstream os;
	os<<"PixLib::CoralDbEncoding::decode(Histo*,istream): unknown nDim="<<nDim;
	throw std::runtime_error(os.str());
      }
    }

    //================================================================
    template<> void decode(std::vector<bool>* arg, std::istream& s) {
      std::vector<bool>::size_type numElements;
      if(! (s>>numElements) ) {
	throw std::runtime_error("PixLib::CoralDbEncoding::decode(vector<bool>*,istream&): could not read num elements from the stream");
      }
      eatWhiteSpace(s);

      arg->clear();
      for(unsigned i=0; i<numElements; i++) {
	bool value;
	decode(&value, s);
	arg->push_back(value);
      }
    }

    //================================================================
    void eatWhiteSpace(std::istream& is) {
      // Eat the white space
      char c;
      if(! (is.get(c)) ) {
	throw std::runtime_error("PixLib::CoralDbEncoding::eatWhiteSpace(istream&): could not read from the stream");
      }
      if(c != ' ') {
	std::ostringstream os;
	os<<"PixLib::CoralDbEncoding::eatWhiteSpace(): expecting white space, got '"<<c<<"'";
	throw std::runtime_error(os.str());
      }
    }

  } // namespace CoralDbEncoding
} // namespace PixLib

//================================================================
