// Dear emacs, this is -*-c++-*-
#ifndef PIXDBCORALDB_H
#define PIXDBCORALDB_H

#include "PixDbInterface/PixDbInterface.h"
#include "PixDbInterface/PixDbCompoundTag.h"
#include "PixDbInterface/PixDbTagStatus.h"
#include "PixDbInterface/AutoCommit.h"

#include "CoralDB/CoralDB.h"

#include <string>
#include <map>



namespace PixLib{

  class PixDbCoralDB;
  class CoralDbRecord;
  class CoralDbField;

  // conversion between PixDbInterface and CoralDB types
  CoralDB::CompoundTag toCoralDB(const PixDbCompoundTag& tag);
  PixDbCompoundTag toPixDb(const CoralDB::CompoundTag& tag);

  PixDbTagStatus toPixDb(const CoralDB::TagStatus& tag);
  PixDbAlias toPixDb(const CoralDB::Alias& a);

  //================================================================
  class CoralDbField : public DbField {
    friend class CoralDbRecord;
    friend class PixDbCoralDB;
    void DbRead() const;

    PixDbCoralDB *m_db;
    CoralDbRecord *m_fieldParent;
    std::string m_name;

    // Things that may be read on demand are mutable
    mutable enum DbDataType m_dataType; // field data type (int, vector<int> etc. (see definition of enum DbDataType in PixDbInterface.h
    mutable void *m_data;

    bool m_clobExists;
  public:
    CoralDbField(PixDbCoralDB *db, const std::string& name, DbDataType type=DBEMPTY, bool clobExists=false);
    CoralDbField(PixDbCoralDB *db, const std::string& name, DbDataType, bool clobExists, const std::string& data);
    virtual PixDbInterface* getDb() const;
    virtual enum DbDataType getDataType() const;
    virtual void dump(std::ostream& os) const;
    virtual string getName() const;
    virtual string getDecName() const;
    virtual ~CoralDbField();
 };
  //================================================================
  class CoralDbRecord : public DbRecord{
    friend class PixDbCoralDB;

    // FIXME: these classes should come from CoralDB
    class ID : public std::string { public: explicit ID(const std::string& s) : std::string(s) {} };
    class Slot : public std::string { public: explicit Slot(const std::string& s) : std::string(s) {} };

    PixDbCoralDB  *m_db;

    CoralDbRecord *m_parent;
    std::string m_className;

    struct DstInfo { 
      CoralDbRecord *dstObj;
      Slot dstSlotName;

      // Paolo requested that soft links are essentially
      // bidirectional.  That is, *soft* linking (srcObj,srcSlot) to
      // (dstObj,dstSlot) automatically creates a connection in the
      // other direction as well.  Such automatically created links
      // should not go into the connectivity DB.  Such fake records
      // are marked by the following field:
      enum LinkType { NORMAL_LINK, BIDIRECTIONAL_LINK_KLUDGE } link_type;

      DstInfo(CoralDbRecord *o, const Slot& s, LinkType t = NORMAL_LINK) : dstObj(o), dstSlotName(s), link_type(t) {}
    };

    typedef std::map<Slot,DstInfo> RecordMapType;    
    RecordMapType m_subrecords;

    mutable ID m_id;  // should be mutable so that getID() can set it from a field as per PixDbInterface
    typedef std::map<std::string,CoralDbField*> FieldMapType;
    FieldMapType m_subfields;
    
    // hard link an existing record
    dbRecordIterator pushRecord(CoralDbRecord* in, const Slot& srcSlotName);

  public:

    CoralDbRecord(PixDbCoralDB *db, const ID& id, const std::string& className);
    
    virtual PixDbInterface* getDb() const; // get the DataBase of this record;

    virtual dbRecordIterator recordBegin();
    virtual dbRecordIterator recordEnd();

    virtual dbFieldIterator fieldBegin(); // the iterator to the record data fields	
    virtual dbFieldIterator fieldEnd(); // the end iterator to the record data fields	

    virtual dbRecordIterator findRecord(const std::string& recordName); // find a subrecord by its slot name
    virtual dbFieldIterator findField(const std::string& fieldName); // find a field by its name 

    virtual void dump(std::ostream& os) const; // Dump - debugging purpose

    virtual string getClassName() const;
    virtual string getName() const { return getID(); }

    virtual string getDecName() const;
    virtual ID getID() const;

    virtual dbFieldIterator addField(const string& name); // Create a new field attached to this record.

    virtual void eraseField(dbFieldIterator it); // erase a field. What if arg points to a field in a different DbRecord?
    virtual bool eraseField(const string& name); // erase the named field in this record. Returns true if removed, false if no such field, may throw exception for other problems.

    virtual dbRecordIterator linkRecord(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName); // link an existing record and return the corresponding iterator
    virtual dbRecordIterator updateLink(DbRecord* dstRec, const string& srcSlotName, const string& dstSlotName); // resets an existing soft link from srcSlotName to the new dst
    virtual void eraseRecord(dbRecordIterator it); // erase a record. What if arg points to a record that is not ours?
    virtual bool eraseRecord(const string& slotName); // erase link at the named slot. For hard links, do recursive tree removal. Returns true if the link was removed.
    virtual DbRecord* addRecord(const string& className, const string& srcSlotName, const string& ID); // add an record

    virtual ~CoralDbRecord();

    // Deprecated
    virtual dbFieldIterator pushField(DbField* in); // add a field to the DbRecord fields and return the corresponding iterator
    virtual dbRecordIterator pushRecord(DbRecord* in, std::string sltName); // add a record to the record list and return the corresponding iterator

  private:

    //================================================================
    // private class implementing CoralDb specific record iterator
    class CoralDbRecordIterator : virtual public IRecordIterator {
      DbRecord *m_srcObject;
      CoralDbRecord::RecordMapType::iterator m_iter;
    public:
      CoralDbRecordIterator(DbRecord *srcObject, const CoralDbRecord::RecordMapType::iterator& i) : m_srcObject(srcObject), m_iter(i) {}
      
      // ----------------------------------------------------------------
      // Basic iterator functionality
      virtual DbRecord* operator->() { return m_iter->second.dstObj; }
      
      // prefix increment
      virtual IIterator<DbRecord>& operator++() { ++m_iter; return *this; }

      virtual bool operator==(const IIterator<DbRecord>& rhs) const;

      // ----------------------------------------------------------------
      // The connectivity part...
      virtual DbRecord* srcObject() const { return m_srcObject; }
      virtual string srcSlotName() const { return m_iter->first; }
      virtual DbRecord* dstObject() const { return m_iter->second.dstObj; }
      virtual string dstSlotName() const { return m_iter->second.dstSlotName; }
      virtual bool isLink() const;
      
      // ----------------------------------------------------------------
      // IRecordIterator requirement
      virtual IRecordIterator* clone() const { return new CoralDbRecordIterator(*this); }
    };
  
    //================================================================
    // private class implementing CoralDb specific field iterator
    class CoralDbFieldIterator : virtual public IFieldIterator {
      friend class CoralDbRecord;
      FieldMapType::iterator m_iter;
    public:
      CoralDbFieldIterator(const FieldMapType::iterator& i) : m_iter(i) {}
      
      virtual DbField* operator->() { return m_iter->second; }

      // prefix increment
      virtual IIterator<DbField>& operator++() { ++m_iter; return *this; }

      virtual bool operator==(const IIterator<DbField>& rhs) const;
      
      virtual IFieldIterator* clone() const { return new CoralDbFieldIterator(*this); }

      // cleanup
      virtual void releaseMem(){};
    };

  };

  //================================================================
  class PixDbCoralDB : public PixDbInterface {
    friend class CoralDbRecord;
    friend class CoralDbField;

    // object ID for a DbRecord may be set through a subfield with this name:
    static const std::string& idFieldName() { static const std::string s("name"); return s; }

    CoralDB::CoralDB m_coralDb;

    std::string m_rootRecordID;

    // The symlink capability needs a global registry of records. Similar for fields.
    // All the records in the registry are owned by PixDbCoralDB
    //
    // Note that we are using ID as decName.  Using something like a
    // path name would be extremely messy in the presence of
    // DbRecord::pushRecord() and DbRecord::eraseRecord() methods that
    // may attach or detach a subtree of records.
    // The pathname of every record in the subtree changes, and 
    // updates in many places would be needed to keep the structure consistent.
    typedef std::map<CoralDbRecord::ID, CoralDbRecord*> DbRecordMap; // ID => record
    DbRecordMap m_records;

    typedef std::map<std::string, CoralDbField*> DbFieldMap;
    DbFieldMap m_fields;
    
    template<class CoralDbArg, void (::CoralDB::CoralDB::*func)(CoralDbArg&) const, class PixDbArg > 
    void callForward(PixDbArg& p) const;
    
    template<class CoralDbArg, void (::CoralDB::CoralDB::*func)(CoralDbArg&, const std::string&) const, class PixDbArg > 
    void callForward(PixDbArg& p, const string& idTag) const;

    CoralDB::CoralDB& getCoralDB() { return m_coralDb; }

    // ----------------------------------------------------------------
  public:

    enum AccessMode { NEW, RECREATE, UPDATE, READONLY };
    
    // Constructors. Empty rootRecordID means take it from masterList.  Empty tag mean take them from the history table for time=now.
    // Both rootRecordID and tag must be provided for modes NEW or RECREATE.
    PixDbCoralDB(const std::string& connection, AccessMode mode, AutoCommit ac=AutoCommit(true), const std::string& rootRecordID="", const PixDbCompoundTag& tag = PixDbCompoundTag() );
    PixDbCoralDB(const std::string& connection, AccessMode mode, AutoCommit ac, const PixDbCompoundTag& tag );
    PixDbCoralDB(const std::string& connection, AccessMode mode, const std::string& rootRecordID, const PixDbCompoundTag& tag = PixDbCompoundTag() );

    void setMsgLevel(coral::MsgLevel outputLevel) { m_coralDb.setMsgLevel(outputLevel); }
    
    virtual CoralDbRecord* readRootRecord();
    
    virtual DbField* makeField(const string& name);

    virtual DbRecord* makeRecord(const string& className, const string& ID);

    // Methods to work with tags. How to set current tag is implementation specific - e.g. it can be done through PixDbCoralDB constructor.
    virtual PixDbCompoundTag compoundTag() { return toPixDb(m_coralDb.compoundTag()); }
    
    virtual void getExistingObjectDictionaryTags(vector<PixDbTagStatus>& output) const;
    virtual void getExistingConnectivityTags(vector<PixDbTagStatus>& output, const string& idTag) const;
    virtual void getExistingDataTags(vector<PixDbTagStatus>& output, const string& idTag) const;
    virtual void getExistingAliasTags(vector<PixDbTagStatus>& output, const string& idTag) const;
    
    // These methods define a new empty tag.  The tag used by the current instance of PixDbInterface are unchanged.
    virtual void makeNewObjectDictionaryTag(const string& newtag) { m_coralDb.makeNewObjectDictionaryTag(newtag); }
    virtual void makeNewConnectivityTag(const string& newtag, const string& idTag) { m_coralDb.makeNewConnectivityTag(newtag, idTag); }
    virtual void makeNewDataTag(const string& newtag, const string& idTag) { m_coralDb.makeNewDataTag(newtag, idTag); }
    virtual void makeNewAliasTag(const string& newtag, const string& idTag) { m_coralDb.makeNewAliasTag(newtag, idTag); }
    virtual void makeNewCompoundTag(const PixDbCompoundTag& newtags) { m_coralDb.makeNewCompoundTag(toCoralDB(newtags)); }
    
    // These methods define a new tag and copy the content of the current tag into it.
    // The tag used by the current instance of PixDbInterface are unchanged.
    virtual void copyObjectDictionaryTag(const string& newIdTag) { m_coralDb.copyObjectDictionaryTag(newIdTag); }
    virtual void copyConnectivityTag(const string& newtag, const string& idTag) { m_coralDb.copyConnectivityTag(newtag, idTag); }
    virtual void copyDataTag(const string& newtag, const string& idTag) { m_coralDb.copyDataTag(newtag, idTag); }
    virtual void copyAliasTag(const string& newtag, const string& idTag) { m_coralDb.copyAliasTag(newtag, idTag); }

    virtual void copyAllForObjectDictionaryTag(const string& newIdTag) { m_coralDb.copyAllForObjectDictionaryTag(newIdTag); }

    // These methods define a new tag and copy the content of the current tag into it.
    // The current instance of PixDbInterface is switched to the newtag.
    virtual void copyConnectivityTagAndSwitch(const string& newtag);
    virtual void copyDataTagAndSwitch(const string& newtag);
    virtual void copyAliasTagAndSwitch(const string& newtag);





    // Methods for the history table.

    virtual PixDbCompoundTag getHistoricTag(const PixDbTimeStamp& when ) const { return toPixDb(m_coralDb.getHistoricTag(when)); }

    // Transaction control

    virtual void transactionStart();
    virtual void transactionCommit();


    // Methods dealing with aliases for object IDs


    virtual string findAlias(const string& id, const string& convention) const { return m_coralDb.findAlias(id,convention); }



    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode);

    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, bool&); // read or commit the field pointed by the iterator
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<bool>&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, int&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<int>&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, unsigned int &);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, float&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<float>&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, double&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, vector<double>&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, Histo&);
    virtual dbFieldIterator DbProcess(DbField* theField, enum DbAccessType mode, string&);
    virtual DbRecord* DbProcess(DbRecord* theRecord, enum DbAccessType mode) {return theRecord;} // read or commit (possibly recursively) the record 

    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, bool& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode) { return DbProcess(*theField, mode); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<bool>& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, int& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<int>& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, unsigned int & data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, float& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<float>& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, double& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, vector<double>& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, Histo& data) { return DbProcess(*theField, mode, data); }
    virtual dbFieldIterator DbProcess(dbFieldIterator theField, enum DbAccessType mode, string& data) { return DbProcess(*theField, mode, data); }
    virtual dbRecordIterator DbProcess(dbRecordIterator theRecord, enum DbAccessType mode) { return theRecord; }

    virtual DbRecord* DbFindRecordByName(const string& decName); // find a record by its name
    virtual DbField* DbFindFieldByName(const string& decName); // find a data field by its name

    // Virtual destructor
    virtual ~PixDbCoralDB();

    // a "hard link" in PixDbInterface corresponds a CoralDB connection terminating in a slot with
    // the following name
    static const CoralDbRecord::Slot& hardLinkDstSlotName() { static const CoralDbRecord::Slot s("UP"); return s; }

  private:
    template<class T> dbFieldIterator DbProcessImp(DbField *f, enum DbAccessType mode, T&);
    void initialize(AccessMode mode, const std::string& rootRecordID, const PixDbCompoundTag& tag);
  };
}

#endif/*PIXDBCORALDB_H*/
