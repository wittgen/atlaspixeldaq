#include "PixDbCoralDB/PixDbCoralDB.h"
#include "PixDbCoralDB/CoralDbEncoding.h"

#include "CoralDB/Connection.h"
#include "CoralDB/Alias.h"
#include "PixDbInterface/PixDbAlias.h"

#include <iterator>
#include <future>

namespace PixLib {
#if 0
}
#endif

//#define PIXDBCORAL_DEBUG

#ifdef PIXDBCORAL_DEBUG
#define PIXDBCORAL_TRACE_CALL(f) do { cerr<<"PIXDBCORAL_TRACE: "; f; } while(0)
#else
#define PIXDBCORAL_TRACE_CALL(f)
#endif

//static const seal::Msg::Level PixDbCoralDB_VerbosityLevel = seal::Msg::Debug;
static const coral::MsgLevel PixDbCoralDB_VerbosityLevel = coral::Error;

//================================================================
PixDbCoralDB::PixDbCoralDB(const std::string& connection, AccessMode mode, AutoCommit ac, const std::string& rootRecordID, const PixDbCompoundTag& tag) 
  : m_coralDb(connection, (mode==READONLY ? coral::ReadOnly : coral::Update), PixDbCoralDB_VerbosityLevel, ac ),
    m_rootRecordID(rootRecordID)
{
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB[this="<<this<<"](dbConnectionString=\""<<connection<<"\", mode=\""<<mode<<"\", autoCommit="<<ac
			<<", rootRecordID=\""<<rootRecordID<<"\", tag ="<<tag<<")"<<endl);
  initialize(mode, rootRecordID, tag);
}

//================================================================
PixDbCoralDB::PixDbCoralDB(const std::string& connection, AccessMode mode, AutoCommit ac, const PixDbCompoundTag& tag) 
  : m_coralDb(connection, (mode==READONLY ? coral::ReadOnly : coral::Update), PixDbCoralDB_VerbosityLevel, ac )
{
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB[this="<<this<<"](dbConnectionString=\""<<connection<<"\", mode=\""<<mode<<"\", autoCommit="<<ac<<"\", tag="<<tag<<")"<<endl);
  initialize(mode, "", tag);
}

//================================================================
PixDbCoralDB::PixDbCoralDB(const std::string& connection, AccessMode mode, const std::string& rootRecordID, const PixDbCompoundTag& tag) 
  : m_coralDb(connection, (mode==READONLY ? coral::ReadOnly : coral::Update), PixDbCoralDB_VerbosityLevel, true /*autocommit*/),
    m_rootRecordID(rootRecordID)
{
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB[this="<<this<<"](dbConnectionString=\""<<connection<<"\",mode=\""<<mode<<"\",rootRecordID=\""<<rootRecordID<<"\", tag = "<<tag<<")"<<endl);
  initialize(mode, rootRecordID, tag);
}

//================================================================
void PixDbCoralDB::initialize(AccessMode mode, const std::string& rootRecordID, const PixDbCompoundTag& tag) 
{
  using namespace CoralDB;
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::initialize(AccessMode=\""<<mode<<"\", rootRecordID=\""<<rootRecordID<<"\")"<<endl);
  
  if( (mode==NEW) || (mode==RECREATE) ) {
    m_coralDb.transactionStart();

    if(rootRecordID.empty()) {
      throw PixDBException("PixDbCoralDB::initialize(accessMode, rootRecordID,tag): please provide a non-empty rootRecordID when creating a new database.\n"
			   "You may leave rootRecordID empty when reading or updating an existing database, where rootRecordID is already defined.\n"
			   );
    }
    
    if(tag == PixDbCompoundTag()) {
      throw PixDBException("PixDbCoralDB::initialize(accessMode, rootRecordID,tag): please provide a valid PixDbCompoundTag when creating a new database.\n");
    }

    if( mode==RECREATE ) {
      m_coralDb.dropSchema();
      //sleep(1); // Does this help to get fewer of "Error ORA-01466: unable to read data - table definition has changed"?
    }
    
    m_coralDb.createSchema();

    ::CoralDB::CompoundTag cdbTag(toCoralDB(tag));
    m_coralDb.makeNewCompoundTag(cdbTag);
    m_coralDb.setCompoundTag(cdbTag);
    m_coralDb.setHistoricTag(cdbTag /*, now */);

    m_coralDb.checkAndInsertObject(rootRecordID, rootRecordID);
    m_coralDb.insertMaster(rootRecordID);
    
    m_coralDb.transactionCommit();
  }
  //----------------------------------------------------------------
  else { // We access an existing database
    // Decide what tags to use
    if(PixDbCompoundTag() == tag) {
      // Nothing is specified by the user.  Look up tags in the history table.
      ::CoralDB::CompoundTag cdbTag = m_coralDb.getHistoricTag();
      m_coralDb.setCompoundTag(cdbTag);
    }
    else {
      // use the explicitly given tag
      m_coralDb.setCompoundTag(toCoralDB(tag));
    }
  }

  //----------------------------------------------------------------
  // Instantiate all DbRecords using the ID=>className map
  ObjectDictionaryMap objDict;
  //m_coralDb.getObjectDictionary(objDict, CompoundTag::OnlyIdTag(tag.objectDictionaryTag()) );
  m_coralDb.getObjectDictionary(objDict, toCoralDB(tag));
  for(ObjectDictionaryMap::const_iterator i=objDict.begin(); i!=objDict.end(); i++) {
    CoralDbRecord::ID id(i->first);
    m_records[id] = new CoralDbRecord(this, id, i->second);
    PIXDBCORAL_TRACE_CALL(cerr<<"initialize(): loaded object ID=\""<<id<<"\", class=\""<<i->second<<"\""<<endl);
  }
  
  // Establish connections between records.
  ConnectionTableMap connections;
  m_coralDb.getConnectionTable(connections);
  for(ConnectionTableMap::const_iterator i=connections.begin(); i!=connections.end(); i++) {
    const ::CoralDB::Connection &c = *i;
    PIXDBCORAL_TRACE_CALL(cerr<<"initialize(): Loading connection "<<c<<endl);
    
    CoralDbRecord::ID srcID(c.fromId());
    DbRecordMap::const_iterator srcIter = m_records.find(srcID);
    if(srcIter == m_records.end()) {
      throw PixDBException("PixDbCoralDB::initialize(): reading from the DB: src object ID=\""
			   +srcID+"\" from the connection table is not in the object dictionary!");
    }
    
    CoralDbRecord::ID dstID(c.toId());
    DbRecordMap::const_iterator dstIter = m_records.find(dstID);
    if(dstIter == m_records.end()) {
      throw PixDBException("PixDbCoralDB::initialize(): reading from the DB: dst object ID=\""
			   +dstID+"\" from the connection table is not in the object dictionary!");
    }
    
    CoralDbRecord *srcRecord = srcIter->second;
    CoralDbRecord *dstRecord = dstIter->second;

    if(c.toSlot() == hardLinkDstSlotName()) {
      PIXDBCORAL_TRACE_CALL(cerr<<"initialize(): Is a hard link"<<endl);
      //// Don't call pushRecord() as it tries to write back to the DB
      //srcIter->second->pushRecord(dstIter->second, CoralDbRecord::Slot(c.fromSlot()));

      // check and set parent of the dst record
      if(dstRecord->m_parent) {
	throw PixDBException(string("initialize(): record ID=")+dstRecord->getID()+" already has a parent");
      }
      dstRecord->m_parent = srcRecord;
	
      // Add the regular link
      std::pair<CoralDbRecord::RecordMapType::iterator,bool> res =
	srcRecord->m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)c.fromSlot(), CoralDbRecord::DstInfo(dstRecord,CoralDbRecord::Slot(c.toSlot()))));

      if(!res.second) {
	throw PixDBException("PixDbCoralDB::initialize(): slot \""+c.fromSlot()+"\" already has a record attached"
			     +"  in record ID=\""+srcRecord->getID()+"\""
			     );
      }
    }
    else {
      PIXDBCORAL_TRACE_CALL(cerr<<"initialize(): Is a soft link"<<endl);
      //// Don't call linkRecord() as it tries to write back to the DB.
      // srcIter->second->linkRecord(dstIter->second, c.fromSlot(), c.toSlot() );

      // Add the regular link
      std::pair<CoralDbRecord::RecordMapType::iterator,bool> res =
	srcRecord->m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)c.fromSlot(), CoralDbRecord::DstInfo(dstRecord,CoralDbRecord::Slot(c.toSlot()))));

      if(!res.second) {
	throw PixDBException("PixDbCoralDB::initialize(): slot \""+c.fromSlot()+"\" already has a record attached"
			     +"  in record ID=\""+srcRecord->getID()+"\""
			     );
      }
      
      // Here do the kludge: add a back link to the subrecord
      
      // NOTE: this call leaves PixDbInterface structure in a mess if
      // backlinking fails and an exception is thrown below. Note that the
      // normal link has already been added.
      std::pair<CoralDbRecord::RecordMapType::iterator,bool> backres = 
      	dstRecord->m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)c.toSlot(),
      						      CoralDbRecord::DstInfo(srcRecord,CoralDbRecord::Slot(c.fromSlot()), 
      									     CoralDbRecord::DstInfo::BIDIRECTIONAL_LINK_KLUDGE)));
      
      if(!backres.second) {
      	throw PixDBException("PixDbInterface::PixDbInterface() [backlinking] slot \""+c.toSlot()+"\" already has a record attached.");
      }
      
    }
  } // loop over connections

  auto writeFieldsFromCLOB = [&]( auto const & clob ) {
	  for(auto const & [id, clobvec]: clob){
    //std::cout << "Fetching record: " << id << " to add " << clobvec.size() << " CLOBS" << '\n';
		  auto record = m_records.find(CoralDbRecord::ID(id));
		  for(auto const & clobpair: clobvec){
      //std::cout << "Trying to push for " << id << " CLOB: " << clobpair.first << " with value: " << clobpair.second << '\n';
			  record->second->pushField(new CoralDbField(this, clobpair.first, DBEMPTY, true, clobpair.second));
      //std::cout << "Pushed for " << id << " CLOB: " << clobpair.first << " with value: " << clobpair.second << '\n';
		  }
	  }
  };

  std::cout << "Attempting all CLOBs: OPTOBOARD\n";
  auto OB_clobs = m_coralDb.getAllCLOBs("OPTOBOARD");
  std::cout << "Got all CLOBs. In total for: " << OB_clobs.size() << "items\n";

  std::cout << "Attempting all CLOBs: RODBOC\n";
  auto RODBOC_clobs = m_coralDb.getAllCLOBs("RODBOC");
  std::cout << "Got all CLOBs. In total for: " << RODBOC_clobs.size() << "items\n";

  std::cout << "Attempting all CLOBs: RODBOCLINKMAP\n";
  auto RODBOCLINKMAP_clobs = m_coralDb.getAllCLOBs("RODBOCLINKMAP");
  std::cout << "Got all CLOBs. In total for: " << RODBOCLINKMAP_clobs.size() << "items\n";

  std::cout << "Attempting all CLOBs: OBLINKMAP\n";
  auto OBLINKMAP_clobs = m_coralDb.getAllCLOBs("OBLINKMAP");
  std::cout << "Got all CLOBs. In total for: " << OBLINKMAP_clobs.size() << "items\n";

  std::cout << "Attempting all CLOBs: PP0\n";
  auto PP0_clobs = m_coralDb.getAllCLOBs("PP0");
  std::cout << "Got all CLOBs. In total for: " << PP0_clobs.size() << "items\n";

  std::cout << "Attempting all CLOBs: MODULE\n";
  auto MODULE_clobs = m_coralDb.getAllCLOBs("MODULE");
  std::cout << "Got all CLOBs. In total for: " << MODULE_clobs.size() << "items\n";

  ClobNameContainer fieldNames;
  auto all_clobs_future = std::async(std::launch::async, [&](){m_coralDb.getClobNames(fieldNames);});
  
  writeFieldsFromCLOB(OB_clobs);
  writeFieldsFromCLOB(RODBOC_clobs);
  writeFieldsFromCLOB(RODBOCLINKMAP_clobs);
  writeFieldsFromCLOB(OBLINKMAP_clobs);
  writeFieldsFromCLOB(PP0_clobs);
  writeFieldsFromCLOB(MODULE_clobs);
  
  all_clobs_future.get();
  
  // read field names for all DbRecords and create fields with clobExists=true.
  for(auto const i: fieldNames) {
    PIXDBCORAL_TRACE_CALL(cerr<<"initialize(): loading fieldname=\""<<i.second<<"\", ID=\""<<i.first<<"\""<<endl);

    auto pr = m_records.find(CoralDbRecord::ID(i.first));
    if(pr == m_records.end()) {
      throw PixDBException("PixDbCoralDB::initialize(): got field for unknown record ID=\""+i.first+"\"");
    }
    if(pr->second->findField(i.second) == pr->second->fieldEnd()) {
      pr->second->pushField(new CoralDbField(this, i.second, DBEMPTY, true ));
      std::cout << "Added to record " << i.first << " field " << i.second << " uninitialised\n"; 
    }
  }

}

//================================================================
PixDbCoralDB::~PixDbCoralDB() {
  for(DbRecordMap::iterator i=m_records.begin(); i!=m_records.end(); i++) {
    delete i->second;
  }
}

//================================================================
CoralDbRecord* PixDbCoralDB::readRootRecord() {
  if(m_rootRecordID.empty()) {
    // Get it from the master list
    std::vector<std::string> masterList = m_coralDb.masterList();
    if(masterList.empty()) {
      throw PixDBException("PixDbCoralDB::readRootRecord(): root record ID is not given in PixDbCoralDB constructor, and masterList is empty");
    }
    else if(masterList.size()>1) {
      throw PixDBException("PixDbCoralDB::readRootRecord(): root record ID is not given in PixDbCoralDB constructor, and masterList contains more than one record");
    }
    else {
      m_rootRecordID = masterList[0];
    }
  }
  
  CoralDbRecord *rootRecord = m_records[CoralDbRecord::ID(m_rootRecordID)];
  if(!rootRecord) {
    throw PixDBException("PixDbCoralDB::readRootRecord(): root record ID=\""+m_rootRecordID+"\" not found");
  }
  return rootRecord;
}

//================================================================
DbField* PixDbCoralDB::makeField(const string& name) {
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::makeField(name=\""<<name<<"\")"<<endl);
  // Don't enter the field in the global registry here - it needs a parent to 
  // have its decName() defined.
  return new CoralDbField(this, name);
}

//================================================================
DbRecord* PixDbCoralDB::makeRecord(const string& className, const string& strid) {
  PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::makeRecord(className=\""<<className<<"\", ID=\""<<strid<<"\")"<<endl);

  //FIXME: obsolete code and comment.
  
  // The record may be created in a different tag. Since OBJDICT is
  // not versioned, we will not insist that the requiested ID is not
  // defined.  However the corresponding object must be parentless and
  // empty, so that objects from makeRecord() are in the same state as
  // if the ID was originally defined by our call.

  CoralDbRecord::ID id(strid);

  DbRecordMap::const_iterator iter = m_records.find(id);

  //----------------------------------------------------------------
  if(iter == m_records.end() ) {     // create a new object

    auto newrec = std::make_unique<CoralDbRecord>(this, id, className);
    if(!m_records.insert(make_pair(id, newrec.get())).second) {
      throw PixDBException("PixDbCoralDB::makeRecord(): could not insert record with ID=\""+
			   id+"\" in m_records. Internal consistency problem?");
    }
    // Add this to the object dictionary
    m_coralDb.checkAndInsertObject(strid, className);

    return newrec.release();
  }
  //----------------------------------------------------------------
  else { // check the existing object
    CoralDbRecord *rec = iter->second;
    if(rec->m_parent) {
      throw PixDBException("PixDbCoralDB::makeRecord():  record with ID=\""+
			   id+"\" is already defined, and has a parent.");
    }
    if(!rec->m_subrecords.empty()) {
      throw PixDBException("PixDbCoralDB::makeRecord():  record with ID=\""+
			   id+"\" is already defined, and has subrecords.");
    }

    if(!rec->m_subfields.empty()) {
      throw PixDBException("PixDbCoralDB::makeRecord():  record with ID=\""+
			   id+"\" is already defined, and has fields.");
    }

    return rec;
  }
}

//================================================================

void PixDbCoralDB::copyConnectivityTagAndSwitch(const string& newtag) {
  m_coralDb.copyConnectivityTag(newtag);
  m_coralDb.setConnectivityTag(newtag); 
}

void PixDbCoralDB::copyDataTagAndSwitch(const string& newtag) {
  m_coralDb.copyDataTag(newtag);
  m_coralDb.setDataTag(newtag); 
}

void PixDbCoralDB::copyAliasTagAndSwitch(const string& newtag) {
  m_coralDb.copyAliasTag(newtag);
  m_coralDb.setAliasTag(newtag); 
}

//================================================================


void PixDbCoralDB::transactionStart() {
  PIXDBCORAL_TRACE_CALL(cerr<<"TRANSACTION_DEBUG: PixDbCoralDB::[this="<<this<<"]transactionStart()"<<endl);
  m_coralDb.transactionStart();
}

void PixDbCoralDB::transactionCommit() {
  PIXDBCORAL_TRACE_CALL(cerr<<"TRANSACTION_DEBUG: PixDbCoralDB::[this="<<this<<"]transactionCommit()"<<endl);
  m_coralDb.transactionCommit();
}


//================================================================
dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode) {
  PIXDBCORAL_TRACE_CALL(cerr<<"DbProcess(DbField*,mode="<<mode<<")"<<endl);

  CoralDbField *f = dynamic_cast<CoralDbField*>(theField);
  if(!f) {
    throw PixDBException("PixDbCoralDB::DbProcess(DbField*,mode): the field is not CoralDbField");
  }
  if(!f->m_fieldParent) {
    PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::DbProcess(DbField*,mode): got a parentless field"<<endl);
    throw PixDBException("PixDbCoralDB::DbProcess(DbField*,mode): got a parentless field");
  }

  dbFieldIterator iter = f->m_fieldParent->findField(f->getName());
  if(iter == f->m_fieldParent->fieldEnd()) {
    PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::DbProcess(DbField*,mode): can't find the field in the parent"<<endl);
    throw PixDBException("PixDbCoralDB::DbProcess(DbField*,mode): can't find the field in the parent");
  }
  
  f->DbRead();

  return iter;
}

//================================================================
// Sets m_data and m_dataType
void CoralDbField::DbRead() const {
  using namespace PixLib::CoralDbEncoding;

  if(m_data) { // already read
    return;
  }

  string data = m_db->getCoralDB().findCLOB(m_fieldParent->getID(), getName());
  if(data.empty()) {
    PIXDBCORAL_TRACE_CALL(cerr<<("CoralDbField::DbRead(): got no data for field named "+string(getName()))<<endl);
    throw PixDBException("CoralDbField::DbRead(): got no data for field named "+string(getName()));
  }

  std::istringstream is(data);
  std::string db_typeName;
  
  // Get PixDb type name of the stored data
  is>>db_typeName;
  m_dataType = PixDb::getDbDataType(db_typeName);

  eatWhiteSpace(is);

  // Pass the rest of the retrieved string in "is" to the decoding routine, giving the right type.
  switch(m_dataType) {
  case DBBOOL:
    m_data = new bool();
    decode((bool*)m_data, is);
    break;
    
  case DBVECTORBOOL:
    m_data = new std::vector<bool>();
    decode( static_cast<std::vector<bool>* >(m_data), is);
    break;

  case DBINT:
    m_data = new int();
    decode((int*)m_data, is);
    break;

  case DBVECTORINT:
    m_data = new std::vector<int>();
    decode((std::vector<int> *)m_data, is);
    break;

  case DBULINT:
    m_data = new unsigned();
    decode((unsigned*)m_data, is);
    break;

  case DBFLOAT:
    m_data = new float();
    decode((float*)m_data, is);
    break;

  case DBVECTORFLOAT:
    m_data = new std::vector<float>();
    decode((std::vector<float>*)m_data, is);
    break;

  case DBDOUBLE:
    m_data = new double();
    decode((double*)m_data, is);
    break;

  case DBVECTORDOUBLE:
    m_data = new std::vector<double>();
    decode((std::vector<double>*)m_data, is);
    break;

  case DBHISTO:
    m_data = new Histo();
    decode((Histo*)m_data, is);
    break;

  case DBSTRING:
    m_data = new string();
    decode((string*)m_data, is);
    break;

  case DBEMPTY:
    throw std::runtime_error("CoralDbField::DbRead(): got DBEMPTY field");
    
  default:
    throw std::runtime_error("CoralDbField::DbRead(): got unknown data type");
  }
}
//================================================================
template<class T> 
dbFieldIterator PixDbCoralDB::DbProcessImp(DbField* theField, enum DbAccessType mode, T& arg) {
  PIXDBCORAL_TRACE_CALL(cerr<<"DbProcessImp(DbField*,mode="<<mode<<", T&)"<<endl);
  
  using namespace PixLib::CoralDbEncoding;
  const char *typeinfo_name = typeid(arg).name();
  DbTypeMap::const_iterator p_dbType = typeMap.find(typeinfo_name);
  if(p_dbType == typeMap.end() ) {
    throw runtime_error("PixDbCoralDB::DbProcessImp(DbField*, mode, T&): can't find matching DbDataType for type name = "+string(typeinfo_name));
  }
  DbDataType typeinfo_dbType = p_dbType->second;
  PIXDBCORAL_TRACE_CALL(cerr<<"DbProcessImp(DbField*,mode="<<mode<<", T&): T is "<<PixDb::getTypeName(typeinfo_dbType)<<endl);
  
  CoralDbField *f = dynamic_cast<CoralDbField*>(theField);
  if(!f) {
    throw PixDBException("PixDbCoralDB::DbProcessImp(DbField*,mode,T&): the field is not CoralDbField");
  }
  if(!f->m_fieldParent) {
    PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::DbProcessImp(DbField*,mode,T&): got a parentless field"<<endl);
    throw PixDBException("PixDbCoralDB::DbProcessImp(DbField*,mode,T&): got a parentless field");
  }
  
  dbFieldIterator iter = f->m_fieldParent->findField(f->getName());
  if(iter == f->m_fieldParent->fieldEnd()) {
    PIXDBCORAL_TRACE_CALL(cerr<<"PixDbCoralDB::DbProcessImp(DbField*,mode,T&): can't find the field in the parent"<<endl);
    throw PixDBException("PixDbCoralDB::DbProcessImp(DbField*,mode,T&): can't find the field in the parent");
  }
  
  switch (mode){
  case DBREAD: {

    f->DbRead();

    // Check that user requested the correct type
    if(f->m_dataType != typeinfo_dbType) {
      PIXDBCORAL_TRACE_CALL(cerr<<("PixDbCoralDB::DbProcessImp(DbField*,DBREAD,T&): trying to read as "
				   +string(PixDb::getTypeName(typeinfo_dbType))+" a field of type "
				   +string(PixDb::getTypeName(f->m_dataType))+", field name "
				   +f->getName()
				   )<<endl);

      throw PixDBException("PixDbCoralDB::DbProcessImp(DbField*,DBREAD,T&): trying to read as "
			   +string(PixDb::getTypeName(typeinfo_dbType))+" a field of type "
			   +string(PixDb::getTypeName(f->m_dataType))+", field name "
			   +f->getName()
			   );
    }
    // Now we have m_data pointing to the value. Assing it to the output variable.
    arg = *(T*)f->m_data;
    break;
  }
  case DBCOMMIT: {
    if(f->m_clobExists) {
      f->DbRead();
    }
    
    if(f->m_dataType == DBEMPTY) {
      f->m_dataType = typeinfo_dbType;
    }
    else if(f->m_dataType != typeinfo_dbType) {
      throw PixDBException("PixDbCoralDB::DbProcessImp(DbField*,DBCOMMIT,T&): attempting to re-define field type from "+string(PixDb::getTypeName(f->m_dataType))
			   +" to "+PixDb::getTypeName(typeinfo_dbType)+", field name "+f->getName() );
    }

    // Prepare data for DB write
    std::ostringstream os;
    make_heading(arg, os);
    encode(arg, os);
    
    // Store new value in the memory cache
    if(!f->m_data) {
      f->m_data = new T(arg);
    }
    else {
      *static_cast<T*>(f->m_data) = arg;
    }
    
    // Write to DB
    if(!f->m_clobExists) {
      PIXDBCORAL_TRACE_CALL(cerr<<"DbProcessImp(): inserting clob "<<f->m_fieldParent->getID()<<", "<<f->getName()<<", "<<os.str()<<endl);
      m_coralDb.insertCLOB(f->m_fieldParent->getID(), f->getName(), os.str());
      f->m_clobExists = true;
    }
    else {
      bool status __attribute__ ((unused)) = m_coralDb.updateCLOB(f->m_fieldParent->getID(), f->getName(), os.str());
      PIXDBCORAL_TRACE_CALL(cerr<<"DbProcessImp(): CLOB update (status="<<status<<"): "<<f->m_fieldParent->getID()<<", "<<f->getName()<<", "<<os.str()<<endl);
    }
    
    break;
  }
  default:
      std::ostringstream os;
      os << "PixDbCoralDB::DbProcessImp(DbField*,mode,T&): unknown mode " << mode;
      throw PixDBException(os.str());
  }
  return iter;
}

//================================================================
dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, bool& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, vector<bool>& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, int& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, vector<int>& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, unsigned int& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, float& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, vector<float>& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, double& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, vector<double>& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, Histo& arg) {
  return DbProcessImp(theField, mode, arg);
}

dbFieldIterator PixDbCoralDB::DbProcess(DbField* theField, enum DbAccessType mode, string& arg) {
  return DbProcessImp(theField, mode, arg);
}

//================================================================
DbRecord* PixDbCoralDB::DbFindRecordByName(const std::string &strid) {
  DbRecordMap::const_iterator i = m_records.find(CoralDbRecord::ID(strid));
  return (i==m_records.end()) ? 0 : i->second;
}

//================================================================
DbField* PixDbCoralDB::DbFindFieldByName(const std::string &decName) {
  std::map<std::string, CoralDbField*>::const_iterator i = m_fields.find(decName);
  return (i==m_fields.end()) ? 0 : i->second;
}

//================================================================
// CoralDbField implementation start

CoralDbField::CoralDbField(PixDbCoralDB *db, const std::string& name, DbDataType type, bool clobExists) 
  : m_db(db), m_fieldParent(0), m_name(name), m_dataType(type), m_data(0), m_clobExists(clobExists)
{}


CoralDbField::CoralDbField(PixDbCoralDB *db, const std::string& name, DbDataType type, bool clobExists, const std::string& data) 
  : m_db(db), m_fieldParent(0), m_name(name), m_dataType(type), m_data(0), m_clobExists(clobExists) {
  using namespace PixLib::CoralDbEncoding;

  if(data.empty()) {
    throw PixDBException("CoralDbField::DbRead(): got no data for field named "+string(getName()));
  }

  std::istringstream is(data);
  std::string db_typeName;
  
  // Get PixDb type name of the stored data
  is>>db_typeName;
  m_dataType = PixDb::getDbDataType(db_typeName);

  eatWhiteSpace(is);

  // Pass the rest of the retrieved string in "is" to the decoding routine, giving the right type.
  switch(m_dataType) {
  case DBBOOL:
    m_data = new bool();
    decode((bool*)m_data, is);
    break;
    
  case DBVECTORBOOL:
    m_data = new std::vector<bool>();
    decode( static_cast<std::vector<bool>* >(m_data), is);
    break;

  case DBINT:
    m_data = new int();
    decode((int*)m_data, is);
    break;

  case DBVECTORINT:
    m_data = new std::vector<int>();
    decode((std::vector<int> *)m_data, is);
    break;

  case DBULINT:
    m_data = new unsigned();
    decode((unsigned*)m_data, is);
    break;

  case DBFLOAT:
    m_data = new float();
    decode((float*)m_data, is);
    break;

  case DBVECTORFLOAT:
    m_data = new std::vector<float>();
    decode((std::vector<float>*)m_data, is);
    break;

  case DBDOUBLE:
    m_data = new double();
    decode((double*)m_data, is);
    break;

  case DBVECTORDOUBLE:
    m_data = new std::vector<double>();
    decode((std::vector<double>*)m_data, is);
    break;

  case DBHISTO:
    m_data = new Histo();
    decode((Histo*)m_data, is);
    break;

  case DBSTRING:
    m_data = new string();
    decode((string*)m_data, is);
    break;

  case DBEMPTY:
    throw std::runtime_error("CoralDbField::DbRead(): got DBEMPTY field");
    
  default:
    throw std::runtime_error("CoralDbField::DbRead(): got unknown data type");
  }
}
//================================================================
CoralDbField::~CoralDbField() {
  switch(m_dataType) {
  case DBBOOL:          delete static_cast<bool*>(m_data); break;
  case DBVECTORBOOL:    delete static_cast<std::vector<bool>*>(m_data); break;
  case DBINT:           delete static_cast<int*>(m_data); break;
  case DBVECTORINT:     delete static_cast<std::vector<int>*>(m_data); break;
  case DBULINT:         delete static_cast<unsigned*>(m_data); break;
  case DBFLOAT:         delete static_cast<float*>(m_data); break;
  case DBVECTORFLOAT:   delete static_cast<std::vector<float>*>(m_data); break;
  case DBDOUBLE:        delete static_cast<double*>(m_data); break;
  case DBVECTORDOUBLE:  delete static_cast<std::vector<double>*>(m_data); break;
  case DBHISTO:         delete static_cast<Histo*>(m_data); break;
  case DBSTRING:        delete static_cast<string*>(m_data); break;
  case DBEMPTY: break;
  default: break;
  }
}

//================================================================
PixDbInterface* CoralDbField::getDb() const { return m_db; }

//================================================================
enum DbDataType CoralDbField::getDataType() const {
  if(m_clobExists) {
    DbRead();
  }
  return m_dataType;
}

//================================================================
void CoralDbField::dump(std::ostream& os) const { os<<"CoralDbField::dump()"; }

//================================================================
string CoralDbField::getName() const {return m_name;}

//================================================================
string CoralDbField::getDecName() const {
  if(!m_fieldParent) {
    throw PixDBException("CoralDbField::getDecName() called on a parentless field.");
  }
  return m_fieldParent->getDecName() + "/" + getName();
}

//================================================================
// CoralDbRecord implementation start

CoralDbRecord::CoralDbRecord(PixDbCoralDB *db, const ID& id, const std::string& className) 
  : m_db(db), m_parent(0), m_className(className), m_id(id)
{
// FIXME:
// Do we need this?   if(!m_id.empty()) { // Duplicate ID to the field for legacy code.
// Do we need this?     dbFieldIterator fi = addField("name");
// Do we need this?     std::string tmpid(id);
// Do we need this?     //std::cerr<<"AG: FIXME: DbProcess() in CoralDbRecord()"<<std::endl;
// Do we need this?     //m_db->DbProcess(*fi, DBCOMMIT, tmpid);
// Do we need this?   }
}

//================================================================
PixDbInterface* CoralDbRecord::getDb() const { return m_db; }

//================================================================
dbRecordIterator CoralDbRecord::recordBegin() {
  return dbRecordIterator(new CoralDbRecordIterator(this, m_subrecords.begin()));
}

//================================================================
dbRecordIterator CoralDbRecord::recordEnd() {
  return new CoralDbRecordIterator(this, m_subrecords.end());
}

//================================================================
dbFieldIterator CoralDbRecord::fieldBegin() {
  return new CoralDbFieldIterator(m_subfields.begin());
}

//================================================================
dbFieldIterator CoralDbRecord::fieldEnd() {
  return new CoralDbFieldIterator(m_subfields.end());
}

//================================================================
dbRecordIterator CoralDbRecord::findRecord(const std::string& slotName) {
  return new 
    CoralDbRecordIterator(this, m_subrecords.find(CoralDbRecord::Slot(slotName)));
}

//================================================================
dbFieldIterator CoralDbRecord::findField(const std::string& id) {
  return new CoralDbFieldIterator(m_subfields.find(id));
}

//================================================================
void PixLib::CoralDbRecord::dump(std::ostream& os) const {
  os<<"CoralDbRecord(className="<<getClassName()
    <<", ID="<<m_id
    <<", parent = "<<(m_parent ? string(m_parent->getID()) : "(null)")
    <<", subrecord srcSlot/ID/dstSlot {";
  for(RecordMapType::const_iterator i=m_subrecords.begin(); i!=m_subrecords.end(); i++) {
    os<<i->first<<"/"<<i->second.dstObj->getID()<<"/"<<i->second.dstSlotName<<", ";
  }
  os<<"}, "
    <<", field names {";
  for(FieldMapType::const_iterator i=m_subfields.begin(); i!=m_subfields.end(); i++) {
    os<<i->first<<", ";
  }
  os<<"}"
    <<")";
}

//================================================================
dbFieldIterator CoralDbRecord::pushField(DbField* infield) {
  PIXDBCORAL_TRACE_CALL(cerr<<"CoralDbRecord[ID="<<getID()<<"]::pushField(name=\""<<infield->getName()<<"\")"<<endl);
  CoralDbField *in = dynamic_cast<CoralDbField*>(infield);
  if(!in) {
    throw PixDBException("CoralDbRecord::pushField(): invalid input pointer.");
  }
  if(in->m_fieldParent) {
    throw PixDBException("CoralDbRecord::pushField(): field name="+in->getName()+" already has a parent.");
  }

  pair<FieldMapType::iterator,bool> res = m_subfields.insert(make_pair(in->getName(), in));
  if(!res.second) {
    throw PixDBException("CoralDbRecord::pushField(): field with name=\""
			 +in->getName()+"\" already exists for this record.");
  }
  // Now we know field's decName - register it
  in->m_fieldParent = this;
  m_db->m_fields.insert(make_pair(in->getDecName(), in));

  return new CoralDbFieldIterator(res.first);
}

//================================================================
dbFieldIterator CoralDbRecord::addField(const string& name) {
  PIXDBCORAL_TRACE_CALL(cerr<<"CoralDbRecord[ID="<<getID()<<"]::addField(name=\""<<name<<"\")"<<endl);
  return pushField(m_db->makeField(name));
}

//================================================================
void CoralDbRecord::eraseField(dbFieldIterator it) {
  eraseField( (*it)->getName() );
}

//================================================================
bool CoralDbRecord::eraseField(const string& name) {
  FieldMapType::iterator fi = m_subfields.find(name);
  if(fi != m_subfields.end()) {
    CoralDbField *field = fi->second;
    
    // Delete from the list of fields of the given record
    m_subfields.erase(fi);
    
    // Delete from the global registry by dec name
    m_db->m_fields.erase(field->getDecName());
    
    // Delete the CLOB
    m_db->m_coralDb.deleteCLOB(field->m_fieldParent->getID(), field->getName());
    
    // Free memory
    delete field;

    // the name in arg did correspond to a field of ours...
    return true;
  }
  else {
    return false; // the named field not found
  }
}

//================================================================

// The deprecated interface method.
dbRecordIterator CoralDbRecord::pushRecord(DbRecord* in, std::string sltName) {
  CoralDbRecord *rec = dynamic_cast<CoralDbRecord*>(in);
  if(!rec) {
    throw PixDBException("CoralDbRecord::pushRecord(in): invalid DbRecord ponter: does not cast to CoralDbRecord*");
  }
  return pushRecord(rec, Slot(sltName.empty() ? rec->getID() : sltName) );
}

//================================================================
// Implementation internal pushRecord()

dbRecordIterator CoralDbRecord::pushRecord(CoralDbRecord* in, const Slot& srcSlotName) {
  if(in->m_parent) {
    throw PixDBException("CoralDbRecord::pushRecord(): the argument record has its parent already set!");
  }

  std::pair<RecordMapType::iterator,bool> res = m_subrecords.insert(std::make_pair(srcSlotName,DstInfo(in,m_db->hardLinkDstSlotName())));
  if(!res.second) {
    throw PixDBException("CoralDbRecord::pushRecord(): slot \""+srcSlotName+"\" already has a record attached."
			 +"  in record ID=\""+this->getID()+"\""
			 );
  }

  m_db->getCoralDB().insertConnection(getID(), srcSlotName, in->getID(), PixDbCoralDB::hardLinkDstSlotName() );

  return new CoralDbRecordIterator(this, res.first);
}

//================================================================
dbRecordIterator CoralDbRecord::linkRecord(DbRecord* inRec, const string& srcSlotName, const string& dst_slot_name) {
  PIXDBCORAL_TRACE_CALL(cerr<<"linkRecord(this->getID()=\""<<getID()<<"\", in->getID()=\""<<inRec->getName()<<"\", srcSlot=\""<<srcSlotName<<"\", dstSlot=\""<<dst_slot_name<<"\")"<<endl);
  CoralDbRecord* in = dynamic_cast<CoralDbRecord*>(inRec);
  if(!in) {
    throw PixDBException("CoralDbRecord::linkRecord(): invalid input pointer: does not cast to CoralDbRecord*");
  }

  std::string dstSlotName = dst_slot_name.empty()? this->getID() : dst_slot_name;

  if(dstSlotName == m_db->hardLinkDstSlotName()) {
    throw PixDBException("CoralDbRecord::linkRecord() called with dstSlotName==\""+m_db->hardLinkDstSlotName()+"\".  Hard linking (pushRecord()) should be used instead.");
  }

  // Add the regular link
  std::pair<RecordMapType::iterator,bool> res = m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)srcSlotName,DstInfo(in,Slot(dstSlotName))));
  if(!res.second) {
    // Check if the existing link is the same as we are trying to make
    DstInfo old = res.first->second;
    throw PixDBException("CoralDbRecord::linkRecord(): attempt to overwrite an existing link from (srcID=\""+getID()+"\",srcSlot=\""+srcSlotName+"\") "
			 +"to (dstID=\""+old.dstObj->getID()+"\", dstSlot=\""+old.dstSlotName+"\") "
			 +"with a link to (dstID=\""+in->getID()+"\", dstSlot=\""+dstSlotName+"\")"
			 );
  }
  else { // Did insert.
    // Here do the kludge: add a back link to the subrecord
    std::pair<RecordMapType::iterator,bool> backres = in->m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)dstSlotName,DstInfo(this,Slot(srcSlotName), DstInfo::BIDIRECTIONAL_LINK_KLUDGE)));
    if(!backres.second) {
      throw PixDBException("CoralDbRecord::linkRecord() [backlinking] slot \""+dstSlotName+"\" already has a record attached.");
    }

    // Write to DB here, having both forward and backlink successfully made in memory.
    m_db->getCoralDB().insertConnection(getID(), srcSlotName, in->getID(), dstSlotName );
  }
  
  return new CoralDbRecordIterator(this, res.first);
}

//================================================================
dbRecordIterator CoralDbRecord::updateLink(DbRecord* inRec, const string& srcSlotName, const string& dst_slot_name) {
  PIXDBCORAL_TRACE_CALL(cerr<<"updateLink(this->getID()=\""<<getID()<<"\", in->getID()=\""<<inRec->getName()<<"\", srcSlot=\""<<srcSlotName<<"\", dstSlot=\""<<dst_slot_name<<"\")"<<endl);

  CoralDbRecord* in = dynamic_cast<CoralDbRecord*>(inRec);
  if(!in) {
    throw PixDBException("CoralDbRecord::linkRecord(): invalid input pointer: does not cast to CoralDbRecord*");
  }

  std::string dstSlotName = dst_slot_name.empty()? this->getID() : dst_slot_name;

  if(dstSlotName == m_db->hardLinkDstSlotName()) {
    throw PixDBException("CoralDbRecord::updateLink() called with dstSlotName==\""+m_db->hardLinkDstSlotName()+"\", which is reserved for hard links.");
  }

  // Locate existing info for the slot
  RecordMapType::iterator p = m_subrecords.find(Slot(srcSlotName));
  if(p != m_subrecords.end()) {
    // Delete backlink from the old object
    CoralDbRecord* oldDstObj = p->second.dstObj;
    string oldDstSlot = p->second.dstSlotName;

    PIXDBCORAL_TRACE_CALL(cerr<<"updating link this->getID()=\""<<getID()<<"\", in->getID()=\""<<inRec->getName()<<"\", srcSlot=\""<<srcSlotName<<"\", dstSlot=\""<<dst_slot_name<<"\", "
			  "oldDstID="<<oldDstObj->getID()<<", oldDstSlot="<<oldDstSlot<<endl);

    RecordMapType::iterator backlink = oldDstObj->m_subrecords.find(Slot(oldDstSlot));
    if(backlink != oldDstObj->m_subrecords.end()) {
      oldDstObj->m_subrecords.erase(backlink);
    }
    else {
      throw PixDBException("CoralDbRecord::updateLink() [ID="+getID()+"]: srcSlotName="+srcSlotName
			   +": old backlink dstSlotName="+oldDstSlot+" not found");
    }
  }
  else {
    throw PixDBException("CoralDbRecord::updateLink() [ID="+getID()+"]: srcSlotName="+srcSlotName+" not found");
  }

  // Update the exising in-memory link
  p->second = DstInfo(in, Slot(dstSlotName) );

  // Add a new in-memory  backlink
  std::pair<RecordMapType::iterator,bool> backres = in->m_subrecords.insert(std::make_pair((PixLib::CoralDbRecord::Slot)dstSlotName,DstInfo(this,Slot(srcSlotName), DstInfo::BIDIRECTIONAL_LINK_KLUDGE)));
  if(!backres.second) {
    throw PixDBException("CoralDbRecord::updateRecord(), ID="+getID()+", srcSlotName="+srcSlotName+": [backlinking] slot \""+dstSlotName+"\" in ID="+in->getID()+" already has a record attached.");
  }

  // Update the database
  m_db->getCoralDB().updateConnection(getID(), srcSlotName, in->getID(), dstSlotName );
  
  return new CoralDbRecordIterator(this, p);
}

//================================================================
void CoralDbRecord::eraseRecord(dbRecordIterator it) {
  if(!eraseRecord(it.srcSlotName())) {
    ostringstream os;
    os<<"CoralDbRecord::eraseRecord() [ID="<<getID()<<"]: slotName="<<it.srcSlotName()<<" not found";
    throw PixDBException(os.str());
  }
}

//================================================================
bool CoralDbRecord::eraseRecord(const string& slotName) {
  RecordMapType::iterator p = m_subrecords.find(Slot(slotName));
  if(p != m_subrecords.end()) {
    CoralDbRecord* rec = p->second.dstObj;
    bool hardLink = (p->second.dstSlotName == PixDbCoralDB::hardLinkDstSlotName());

    // For soft links, remove the "kludge" backlink (exists in memory only)
    if(!hardLink) {
      RecordMapType::iterator backlink = rec->m_subrecords.find(p->second.dstSlotName);
      if(backlink != rec->m_subrecords.end()) {
	rec->m_subrecords.erase(backlink);
      }
      else {
	throw PixDBException("CoralDbRecord::eraseRecord(string&) [ID="+getID()+"]: slotName="+slotName
			     +": backlink dstSlotName="+p->second.dstSlotName+" not found");
      }
    }
    
    // Erase the connection corresponding to the iterator for both hard and soft links
    
    // Erase the connection (this to rec) in memory:
    m_subrecords.erase(p);
    
    // Erase the connection (this to rec) in the database:
    m_db->getCoralDB().deleteConnection(getID(), slotName, p->second.link_type == DstInfo::NORMAL_LINK );

    if(hardLink) {
      
      // Delete fields of rec (not of this!)
      for(FieldMapType::iterator f=rec->m_subfields.begin(); f!=rec->m_subfields.end(); ) {
	string fieldName = f++ ->first;
	rec->eraseField(fieldName);
      }
      
      // Delete all subrecords of rec (not of this!)
      for(RecordMapType::iterator r = rec->m_subrecords.begin(); r != rec->m_subrecords.end(); ) {
	// Can't just call 
	//     rec->eraseRecord(r++ ->first);
	// because r->first would be passed by const ref, but r is deleted inside the call.
	// Causes a crash.  Have to make a temporary.
	string slotNameStr = r++ ->first;
	rec->eraseRecord(slotNameStr);
      }
      
      // Remove from the global DecName registry, which is a mirror of objectDictionary. 
      m_db->m_records.erase(rec->getID());

      // FIXME: deleteObject() may fail if there are other references to the ID,
      // for example from the aliases table.  Better to use deleteAllForObject(rec->getID());
      // that takes care of all refs.  We still need to update out memory cache above,
      // but actual DB deletions above will become unnecessary.

      // Remove rec from objectDictionary.
      m_db->getCoralDB().deleteObject(rec->getID()); 

      // free memory
      delete rec;
    }

    return true;
  }
  else {
    return false; // no such slot
  }
}

//================================================================
DbRecord* CoralDbRecord::addRecord(const string& className, const string& srcSlotName, const string& tmpID) {
  PIXDBCORAL_TRACE_CALL(cerr<<"addRecord(this->getID()=\""<<getID() <<"\", class=\""<<className<<"\", srcSlot=\""<<srcSlotName<<"\", ID=\""<<tmpID<<"\")"<<endl);
  std::string IDStr = tmpID.empty() ? srcSlotName : tmpID;
  return *pushRecord(dynamic_cast<CoralDbRecord*>(m_db->makeRecord(className, IDStr)), Slot(srcSlotName) );
}

//================================================================
std::string CoralDbRecord::getClassName() const {
  return m_className;
}

//================================================================
std::string CoralDbRecord::getDecName() const {
  return getID();
}

//================================================================
CoralDbRecord::ID CoralDbRecord::getID() const {
  // ID may be set through a field as per PixDbInterface definition.
  // Try getting it from there.

// FIXME:
// Do we need this?   std::string id_from_field;
// Do we need this?   FieldMapType::const_iterator i = m_subfields.find(m_db->idFieldName());
// Do we need this?   if(i!=m_subfields.end()) {
// Do we need this?     //std::cerr<<"AG: FIXME: DbProcess() in getID()"<<std::endl;
// Do we need this?     //FIXME:    m_db->DbProcess(i->second, DBREAD, id_from_field);
// Do we need this?   }
// Do we need this? 
// Do we need this?   // If internal record ID has not been set, define it now:
// Do we need this?   if(m_id.empty()) {
// Do we need this?     m_id = ID(id_from_field);
// Do we need this?   }
// Do we need this?   
// Do we need this?   // Don't allow redefinitions of internal ID.  Don't require one to have the special field for ID.
// Do we need this?   if(!id_from_field.empty() && (m_id != id_from_field) ) {
// Do we need this?     throw PixDBException("DbRecord::getID(): attempt to redefine record ID: current ID=\""+m_id+"\", in the field \""+id_from_field+"\"");
// Do we need this?   }

  return m_id;
}

//================================================================
CoralDbRecord::~CoralDbRecord() {
  for(FieldMapType::iterator f=m_subfields.begin(); f!=m_subfields.end(); f++) {
    delete f->second;
  }
}

//================================================================
// Iterators

bool CoralDbRecord::CoralDbRecordIterator::
operator==(const IIterator<DbRecord>& rhs) const { 
  const CoralDbRecordIterator* right = dynamic_cast<const CoralDbRecordIterator*>(&rhs);
  return right ? (m_iter == right->m_iter) : false;
}

//================================================================
bool CoralDbRecord::CoralDbFieldIterator::
operator==(const IIterator<DbField>& rhs) const { 
  const CoralDbFieldIterator* right = dynamic_cast<const CoralDbFieldIterator*>(&rhs);
  return right ? (m_iter == right->m_iter) : false;
}

//================================================================
bool CoralDbRecord::CoralDbRecordIterator::isLink() const {
  return dstSlotName() != PixDbCoralDB::hardLinkDstSlotName();
}

//================================================================

CoralDB::CompoundTag toCoralDB(const PixDbCompoundTag& tag) {
  return CoralDB::CompoundTag(tag.objectDictionaryTag(), tag.connectivityTag(), tag.dataTag(), tag.aliasTag() );
}

PixDbCompoundTag toPixDb(const CoralDB::CompoundTag& tag) {
  return PixDbCompoundTag(tag.objectDictionaryTag(), tag.connectivityTag(), tag.dataTag(), tag.aliasTag() );
}

PixDbTagStatus toPixDb(const CoralDB::TagStatus& t) {
  return PixDbTagStatus(t.tag(), t.locked() );
}

PixDbAlias toPixDb(const CoralDB::Alias& a) {
  return PixDbAlias(a.alias(), a.convention(), a.id() );
}

//================================================================
template<class T1, class T2>  
void vectorToPixDb(std::vector<T1>& to, const std::vector<T2>& from) {
  to.clear();
  to.reserve(from.size());
  for(typename std::vector<T2>::const_iterator i=from.begin(); i!=from.end(); ++i) {
    to.push_back(toPixDb(*i));
  }
}

//================================================================
template<class CoralDbArg, 
	 void (::CoralDB::CoralDB::*func)(CoralDbArg&) const, 
	 class PixDbArg > 
void PixDbCoralDB::callForward(PixDbArg& p) const {
  CoralDbArg c;
  (m_coralDb.*func)(c);
  vectorToPixDb(p,c);
}

template<class CoralDbArg, 
	 void (::CoralDB::CoralDB::*func)(CoralDbArg&, const std::string&) const, 
	 class PixDbArg > 
void PixDbCoralDB::callForward(PixDbArg& p, const string& idTag) const {
  CoralDbArg c;
  (m_coralDb.*func)(c, idTag);
  vectorToPixDb(p,c);
}

//================================================================
void PixDbCoralDB::getExistingObjectDictionaryTags(std::vector<PixLib::PixDbTagStatus>& output) const {
  callForward< CoralDB::TagList, &::CoralDB::CoralDB::getExistingObjectDictionaryTags>(output);
}

void PixDbCoralDB::getExistingConnectivityTags(std::vector<PixLib::PixDbTagStatus>& output, const std::string& idTag) const {
  callForward< CoralDB::TagList, &::CoralDB::CoralDB::getExistingConnectivityTags>(output, idTag);
}

void PixDbCoralDB::getExistingDataTags(std::vector<PixLib::PixDbTagStatus>& output, const std::string& idTag) const {
  callForward< CoralDB::TagList, &::CoralDB::CoralDB::getExistingDataTags>(output, idTag);
}

void PixDbCoralDB::getExistingAliasTags(std::vector<PixLib::PixDbTagStatus>& output, const std::string& idTag) const {
  callForward< CoralDB::TagList, &::CoralDB::CoralDB::getExistingAliasTags>(output, idTag);
}


//================================================================

#if 0
{
#endif
} // <-- namespace PixLib

// EOF
