/* Common Header */

/* rodxface.h */
#ifndef _RODXFACE_
#define _RODXFACE_

/* changed to remove code being overwritten in the stack */
#define MAGIC_SLAVE_LOCATION 0x0200
#define MAGIC_MASTER_LOCATION 0x80000000

#define I_AM_ALIVE 0xC0FFEE
#define STILLBORN_BOOT 0xdeaddead

#define MAX_PRIMITIVE_NAME_LENGTH 64

/* expert mode debugging only. not used for 
	normal operation */
#define DEBUG_SPORT_BUFFER 1
#define DEBUG_INMEM_BUFFER 2


typedef struct {
	uint32_t sportDone[2], sportAdd[2], sportLen[2], sportCtrl[2], sportMode[2];
} DebugStruct;

typedef struct {
	uint32_t tail, head, base, length;
} GenericQueue;

typedef struct {
	uint32_t nTextBuffers;
	uint32_t nTextBufferSlots;
	uint32_t textBuffer;

//	uint32_t slaveBuffer[4]; //Change to N_SLAVES later (ARS)

	
	uint32_t nPrimitives;
	uint32_t primitives;

/* server side */	
	uint32_t nRequestChannels;
	uint32_t requestChannel;
	
	uint32_t requestChannelControl[2];
	uint32_t requestChannelStatus[2];
	uint32_t requestChannelLoc[2]; /* queues work with a buffer on the server */
	uint32_t requestChannelLen[2]; /* size of prim list buffer on server */
	uint32_t requestChannelReplyBufferLoc[2];
	uint32_t requestChannelReplyBufferLen[2];

/* client side */
	uint32_t nCommandChannels;
	uint32_t commandChannel;

	uint32_t commandChannelControl[2];
	uint32_t commandChannelStatus[2];
	uint32_t commandChannelLoc[2]; /* queues work with a buffer on the server */
	uint32_t commandChannelLen[2]; /* size of prim list buffer on server */
	uint32_t commandChannelReplyBufferLoc[2];
	uint32_t commandChannelReplyBufferLen[2];

	uint32_t nGpControl;
	uint32_t gpControl;
	uint32_t nGpStatus;
	uint32_t gpStatus;
	
/* uncommitted are for debugging use. if you don't know what this is, fuggedaboutit */
	uint32_t debugStruct; /* internal expert use. details are omitted on purpose */
	
} Xface;

typedef struct {
	Xface xface;
	uint32_t slaveInfo[4]; /* pointers to slaveInfo structures */
} MasterXface;

/* begin: slated for disappearance */

typedef struct {
	Xface xface;
	uint32_t frameBuffer, nFrames;
} SlaveXface;

typedef struct {
	uint32_t bootSignature, memoryMap;
/* uncommitted regs for debugging. must be dispensable */
	uint32_t scratch[30];
} CommRegs;

/* end: slated for disappearance */

typedef struct {
        uint32_t tail, head, base, length;
} TxtBuffer;


#endif
