/* Common Header */

#ifndef SCAN_OPTIONS_2_H
#define SCAN_OPTIONS_2_H

//#include "sysParams.h"
#include <cstdint>
#include "scanEnums.h"
#include "sysParams.h"

typedef struct {
  uint32_t optionMask;
  enum BinSize binSize;
} HistoOptions;


#define MAXGROUPS 8
typedef struct {
  uint16_t groupActionMask[MAXGROUPS];
  uint8_t  superGroupAMask;   
  uint8_t  superGroupBMask;
  uint16_t spare;   
} GroupOptions;



typedef struct {
  enum TriggMode triggerMode;
  uint32_t nEvents;
  uint8_t  nL1AperEvent;
  uint8_t  Lvl1_Latency;
  uint16_t strobeDuration;
  uint32_t strobeMCCDelay;    
  uint32_t strobeMCCDelayRange;    
  uint32_t CalL1ADelay;
  uint32_t eventInterval;
  uint32_t superGroupDelay;
  uint8_t  superGroupAnL1A[2];
  uint8_t  superGroupBnL1A[2];
  uint16_t trigABDelay[2];
  uint32_t vcal_charge;
  uint32_t optionsMask; /* this is TriggOptions */
  uint32_t customTriggerSequence[10];
} TriggerOptions;

typedef struct {
  /* global register parameters which need setting: */
  uint8_t  phi;                 /* Column readout frequency */
  uint8_t  totThresholdMode;    /* Sets TOT threshold mode */
  uint8_t  totMinimum;          /* TOT minimum */
  uint8_t  totTwalk;            /* TOT double-hit for digital timewalk correction */
  uint8_t  totLeMode;           /* TOT or timestamp leading edge option */
  uint8_t  hitbus;
  
} PixelScanFE; /* FE specific options during scans */



typedef struct {
  enum ScanAct Action;
  enum FitFunc fitFunction;
  uint32_t targetThreshold;
} ActionOptions;



typedef struct {
  enum ScanParam     scanParameter;
  ActionOptions endofLoopAction; 
  uint32_t        nPoints;
  uint32_t        dataPointsPtr;
} ScanLoop;




typedef struct {
	uint32_t maskStage;
	uint32_t kMaskStage;
	uint32_t nMaskStages;
	uint32_t loop0Parameter;
	uint32_t nLoop0Parameters;	
	uint32_t loop1Parameter;
	uint32_t nLoop1Parameters;	
	uint32_t loop2Parameter;
	uint32_t nLoop2Parameters;	
	uint32_t scanType;
	uint32_t options; /* points to a copy of the input structure */
	uint32_t activeModules;
	uint32_t progress;
	uint32_t state;	
	uint32_t task[N_SLAVES]; /* contains the (histogramming) task for each of the slaves */
} ScanStatus;

/* Main Scan interface */
#define SCAN_MAXLOOPS 3

typedef struct {
  enum MaskStageMode  stagingMode;
  enum MaskStageSteps maskStageTotalSteps;
  uint16_t              nMaskStages;
  int		      maskLoop; 
  uint16_t              FEbyFEMask;
  uint8_t               spare;
  uint8_t               nLoops;
  GroupOptions        groupOpt;
  TriggerOptions      trigOpt;
  PixelScanFE         feOpt;
  ScanLoop            scanLoop[SCAN_MAXLOOPS];
  HistoOptions        histOpt[SCAN_MAXHIST];
} ScanOptions;
 

#endif
