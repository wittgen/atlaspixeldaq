/* Common Header */

#ifndef PRIMXFACE_H
#define PRIMXFACE_H

#include <sysParams.h>
#include <cstdint> /* hg tbd neatening includes on the slave */
#include <moduleConfigStructures.h>
#include <quickStatusInfo.h>
#include <primXface_common.h> //NK: shifted some parts of primXface.h into CommonNewIblDsp/primXface_common.h


/*** READ CONFIGURATION ***/

typedef struct {
        uint32_t dataPtr; /* if 0 data follows. otherwise, is pixel structure */
        uint16_t pixelRegister[N_CHIPS][N_COLS][N_ROWS];
        uint32_t globalRegister[N_CHIPS][8];
} ReadModuleConfigOut;

#endif
