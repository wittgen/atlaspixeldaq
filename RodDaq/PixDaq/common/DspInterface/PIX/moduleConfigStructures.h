/**************************************************************************
 *
 * Title: Master/PIX moduleConfigStructures.h
 * Version: 11th November 2002
 *
 * Description:
 * ROD Master DSP Pixel module configuration structures
 * and constant definitions.
 *
 * Author: John Richardson; john.richardson@cern.ch
 *
 *   - Adjusted to make compatible with the SCT module structure &
 *     existing c code by adding members to the PixelModule structure.
 *     Structure PixelFETrims expanded to a full array
 *     (was a bit-field).                                    14.03.03 dpsf
 *   - Added FEIndex into PixelFEConfig struct               16 Apr 03
 *   - Added in the (virtual) MCC strobe register, so that the
 *     count register modifications done in the pixel serial stream
 *     building functions do not interfere with placing the module
 *     in data taking mode.                                  10 Feb 04 dpsf
 *   - Added the calCoeff structure, a separate structure which the
 *     MDSP transfers to the SDSPs during run-time (module loading &
 *     SDSP start-up), so they have enough information to perform
 *     the vcal to electrons conversion.                     28 Jan 05 dpsf
 **************************************************************************/
#ifndef _PIXEL_CONFIG_STRUCTURES_	 /* multiple inclusion protection */
#define _PIXEL_CONFIG_STRUCTURES_
#include <cstdint>

/*  Up to 28 pixel modules, with gaps in module structure set where
    the corresponding data links are not connected */
// #define N_PIXEL_MODULES     28   
// #define N_PIXEL_EXTMODULES  0    /* no redundant command links */
// #define N_PIXEL_TOTMODULES  ((N_PIXEL_MODULES)+(N_PIXEL_EXTMODULES))

#define N_PIXEL_COLUMNS  18
#define N_PIXEL_ROWS     160
#define N_PIXEL_FE_CHIPS 16

#define FE_I1  1
#define FE_I2  2
#define FEI1   1
#define FEI2   2
#define MCC_I1 1
#define MCC_I2 2
#define MCCI1  1
#define MCCI2  2

/* MCC options */

#define MCC_SINGLE_40 0
#define MCC_DOUBLE_40 1
#define MCC_SINGLE_80 2
#define MCC_DOUBLE_80 3

/* Default & off-ROD positions of a module's TTC fibres. The default primary
   TTC fibre location for a module is simply the module's position inside the
   structure set. */
#define DEFAULT_TTC    0x80

/* Default Link & Data Links which are turned off: */
#define DEFAULT_DATA_LINK    0x80
#define DATA_LINK_OFF        0xff

#define PIXEL_STATIC_CONFIG 0
#define PIXEL_SCAN_CONFIG   1

/************* STRUCTURE DEFINITIONS ***********/
/* A small calibration coefficient structure containing just the vcal
   to electron conversion coefficients, for fitting on the SDSPs. */
typedef struct {
	float vcal[4];
	float cap[2];
	uint8_t   capSel;
	uint8_t   unused[3];
	uint32_t  unused1;
} ChipCoeff;

typedef struct {
	ChipCoeff fe[N_PIXEL_FE_CHIPS];
} CalCoeff;


typedef struct { //FE Command register
	uint32_t address;		       /* 5 bits */
	uint32_t command;            /* 32 bits */
} PixelFECommand;

typedef struct { //FE global register
	//This is valid for both FE-I1 & 2, since the FE-I1 is a sub-set.
	uint32_t  latency             : 8;                 
	uint32_t  dacIVDD2            : 8;                    
	uint32_t  dacIP2              : 8;                      
	uint32_t  dacID               : 8;
	
	uint32_t  dacIP               : 8;                       
	uint32_t  dacITRIMTH          : 8;                  
	uint32_t  dacIF               : 8;                       
	uint32_t  dacITH1             : 8;
	
	uint32_t  dacITH2             : 8;                     
	uint32_t  dacIL               : 8;                       
	uint32_t  dacIL2              : 8;                      
	uint32_t  dacITRIMIF          : 8; 
	
	uint32_t  dacSpare            : 8;                        
	uint32_t  threshTOTMinimum    : 8;            
	uint32_t  threshTOTDouble     : 8;             
	uint32_t  hitbusScaler        : 8;
	
	uint32_t  capMeasure          : 6; 
	uint32_t  gdac                : 5;       
	uint32_t  selfWidth           : 4;
	uint32_t  selfLatency         : 4;
	uint32_t  muxTestPixel        : 2;
	uint32_t  spare               : 2;
	uint32_t  aregTrim            : 2;
	uint32_t  aregMeas            : 2;
	uint32_t  dregTrim            : 2;
	uint32_t  dregMeas            : 2;
	uint32_t  parity              : 1;                
	
	uint32_t  dacMonLeakADC       : 9;
	uint32_t  dacVCAL             : 10; /* extended to 10 bit for FE2/3 */          
	uint32_t  widthSelfTrigger    : 4;        
	uint32_t  muxDO               : 4; /* note: dynamically defined */                      
	uint32_t  muxMonHit           : 4;
	uint32_t  muxEOC              : 2;                      
	
	uint32_t  frequencyCEU        : 2;                
	uint32_t  modeTOTThresh       : 2;               
	uint32_t  enableTimestamp     : 1;             
	uint32_t  enableSelfTrigger   : 1;        
	uint32_t  enableHitParity     : 1;                    
	uint32_t  monMonLeakADC       : 1;            
	uint32_t  monADCRef           : 1;                
	uint32_t  enableMonLeak       : 1;            
	uint32_t  statusMonLeak       : 1;            
	uint32_t  enableCapTest       : 1;                
	uint32_t  enableBuffer        : 1;                 
	uint32_t  enableVcalMeasure   : 1;            
	uint32_t  enableLeakMeasure   : 1;            
	uint32_t  enableBufferBoost   : 1;            
	uint32_t  enableCP8           : 1;                    
	uint32_t  monIVDD2            : 1;                     
	uint32_t  monID               : 1;                        
	uint32_t  enableCP7           : 1;                    
	uint32_t  monIP2              : 1;                       
	uint32_t  monIP               : 1;                        
	uint32_t  enableCP6           : 1;                    
	uint32_t  monITRIMTH          : 1;                   
	uint32_t  monIF               : 1;                        
	uint32_t  enableCP5           : 1;                    
	uint32_t  monITRIMIF          : 1;                   
	uint32_t  monVCAL             : 1;                      
	uint32_t  enableCP4           : 1;                    
	uint32_t  enableCinjHigh      : 1;               
	uint32_t  enableExternal      : 1;               
	uint32_t  enableTestAnalogRef : 1;
	
	uint32_t  enableDigital       : 1;                
	uint32_t  enableCP3           : 1;                    
	uint32_t  monITH1             : 1;                      
	uint32_t  monITH2             : 1;                      
	uint32_t  enableCP2           : 1;                    
	uint32_t  monIL               : 1;                        
	uint32_t  monIL2              : 1;                       
	uint32_t  enableCP1           : 1;                                 
	uint32_t  enableCP0           : 1;   
	uint32_t  enableHitbus        : 1;                 
	uint32_t  monSpare            : 1;                     
	uint32_t  enableAregMeas      : 1;
	uint32_t  enableAreg          : 1;
	uint32_t  enableLvdsRegMeas   : 1;
	uint32_t  enableDregMeas      : 1;
	uint32_t  enableTune          : 1;
	uint32_t  enableBiasComp      : 1;
	uint32_t  enableIpMonitor     : 1;
} PixelFEGlobal;				     
							    

typedef struct { //Pixel-level control bits
	uint32_t maskEnable[5][N_PIXEL_COLUMNS]; /* 32 bits, one bit per pixel thus 5 words per column */
	uint32_t maskSelect[5][N_PIXEL_COLUMNS];
	uint32_t maskPreamp[5][N_PIXEL_COLUMNS];
	uint32_t maskHitbus[5][N_PIXEL_COLUMNS];
} PixelFEMasks;  
   

typedef struct { //Trim DACs:
	uint8_t dacThresholdTrim[N_PIXEL_ROWS][N_PIXEL_COLUMNS];  /*: 5; Currently 5 bits per pixel, will change in next iteration of FE */
	uint8_t  dacFeedbackTrim[N_PIXEL_ROWS][N_PIXEL_COLUMNS];  /*: 5;                 ""                                              */
} PixelFETrims; 


typedef struct { 
/* Sub-structure for calibration of injection-capacitors, VCAL-DAC and
   leakage current measurement */
	float cinjLo;
	float cinjHi;
	float vcalCoeff[4];
	float chargeCoeffClo;   //dpsf: ?
	float chargeCoeffChi;
	float chargeOffsetClo;
	float chargeOffsetChi;
	float monleakCoeff;
	/* need MCC time calibration also //dpsf: ? */
} PixelFECalib; 


typedef struct { //MCC registers
   uint16_t regCSR;                  
   uint16_t regLV1;                  
   uint16_t regFEEN;				 
   uint16_t regWFE;					 

   uint16_t regWMCC; 				
   uint16_t regCNT;					 
   uint16_t regCAL;					 
   uint16_t regPEF;		

   uint16_t regWBITD;
   uint16_t regWRECD;
   uint16_t regSBSR;

   /* Strobe duration is a virtual register (shared with CNT); when preparing the
      modules for data this value is substituted for the count. */
   uint16_t regSTR;
} PixelMCCRegisters;

typedef struct { //FE level parameters
	uint32_t FEIndex; 	
	PixelFECommand FECommand;
	PixelFEGlobal FEGlobal;
	PixelFEMasks FEMasks;
	PixelFETrims FETrims;
	PixelFECalib FECalib;
} PixelFEConfig;

typedef struct {
	uint8_t  tdac, prevTdac;
	uint8_t  fdac, prevFdac;
} PixelTrimScanData;

typedef struct {
	/* FE module-level options: */
	uint16_t maskEnableFEConfig; /* 16 bits, one per FE */
	uint16_t maskEnableFEScan;
	uint16_t maskEnableFEDacs;
	uint8_t feFlavour;
	uint8_t mccFlavour;
	
	/* FE configurations: */
	PixelFEConfig FEConfig[N_PIXEL_FE_CHIPS+1];
	//PixelFEConfig chip[N_PIXEL_FE_CHIPS]; //dpsf: ?
	
	/* MCC configuration */
	PixelMCCRegisters MCCRegisters;
	
	/* The current (uniform) value for the DACs of each pixel during a DAC scan */
	PixelTrimScanData  trimScanData;
	char  idStr[128]; /* Module identification string */
	char    tag[128]; /* Module configuration tag */
	uint32_t  revision; /* Module configuration revision */

	uint8_t present;    /* Module is physically present. Does not need setting
	                     externally; handled by the Master DSP. */
	
	uint8_t active;     /* 1 -> participates in scans */
	                  /* 0 -> registers unchanged during scanning */
	uint8_t moduleIdx;  /* Copy of the module's index for access from a pointer */
	uint8_t groupId;    /* The ID of the module's group. This is used to indicate
	                     which slave DSP will receive the module's data (if group
	                     based distribution is set), and also to allow different
	                     module groups to be triggered independently (for
	                     cross-talk studies). valid range: [0,7] */
	uint8_t pTTC;       /* primary TX channel  */
	uint8_t rx;         /* data link used by module */
//	uint8_t unused1[2];
	
//	uint32_t unused2[0x653]; /* align module structures on convenient boundary */

} PixelModuleConfig; /* declare N_PIXEL_CONFIG_SETS structure for each of N_PIXEL_MODULES */

#endif   /* multiple inclusion protection */


