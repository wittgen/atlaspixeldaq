#ifndef SCTPIXELROD_DAVEMODULE_H
#define SCTPIXELROD_DAVEMODULE_H

#include "VmeModule.h"
#include "VmePort.h"

namespace DaveModuleRegisters {
  /// Status block, starts at 0x000
  enum {
    SANITY       = 0x000,
    VERSION_ID   = 0x004,
    HARDWARE_ID  = 0x006,
    BOARD_STAT   = 0x008,
    LED_STAT     = 0x00a,
    LEMOIN_STAT  = 0x00c,
    LEMOOUT_STAT = 0x00e,
    L1ID_LO      = 0x010,
    L1ID_HI      = 0x012,
    SSAD_LO      = 0x014,
    SSAD_HI      = 0x016,
    SSCOUNT_LO   = 0x018,
    SSCOUNT_HI   = 0x01a,
  };

  /// Register block, starts at 0x100
  enum {
    LEMOIN_ENA  = 0x100,
    LEMOOUT_ENA = 0x102,
    INT_ENA     = 0x104,
    RND_SEED_LO = 0x106,
    RND_SEED_HI = 0x108,
    RND_RATE    = 0x10a,
    SDT_LENGTH  = 0x10c,
    CDT_LEVEL   = 0x10e,
    CDT_RATE    = 0x110,
    BCID_OFFSET = 0x112,
    LEMOIN_INV  = 0x114,
    LEMOOUT_INV = 0x116,
    SEQ_END_LO  = 0x118,
    SEQ_END_HI  = 0x11a,
    SEQ_ENA     = 0x11c,
  };
}

class DaveModule : public SctPixelRod::VmeModule {
  DaveModule(const DaveModule& rhs);
  DaveModule &operator=(const DaveModule & rhs);

 public:

  // Constructors, destructor, and assignment
  DaveModule(uint32_t baseAddr, uint32_t mapSize, SctPixelRod::VmeInterface & ourInterface)
    : VmeModule(baseAddr, mapSize, ourInterface),
      rawPort(baseAddr, mapSize, SctPixelRod::VmeInterface::A32, ourInterface)
  {
    rawPort.setExceptionTrapping(true);
  }

  virtual ~DaveModule() {}

  /// Implement VmeModule
  void initialize() {}

  /// Implement VmeModule
  void reset() {}

  /// Implement VmeModule
  void status() {}

  uint16_t readReg(uint32_t address) { return rawPort.read16(address); }
  void writeReg(uint32_t address, uint16_t val) { rawPort.write16(address, val); }

 private:

  SctPixelRod::VmePort rawPort;
};

#endif
