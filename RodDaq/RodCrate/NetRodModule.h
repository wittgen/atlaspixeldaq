//File: NetRodModule.h

#ifndef SCTPIXELROD_NETRODMODULE_H 
#define SCTPIXELROD_NETRODMODULE_H

#include <ctime>     // Pick up clock() function and clock_t typedef
#include <new>
#include <unistd.h>

#include "AbstrRodModule.h"
#ifdef IBL
#include "iblSysParams.h"
#else
#include "sysParams.h"
#endif

namespace SctPixelRod {


//------------------------------------------------------------------------------              
//! NetRodModule: 
/*! @class NetRodModule
 *
 *  @brief This is a derived class providing the software interface for ethernet ROD modules.
 *
 *  @author Andreas Kugel
 */

class NetRodModule : public AbstrRodModule, public VmeModule{
  NetRodModule(const NetRodModule& rhs);
  NetRodModule &operator=(const NetRodModule & rhs);

public:

  // Constructors, destructor, and assignment
  NetRodModule( uint32_t baseAddr, uint32_t mapSize, VmeInterface & ourInterface, 
             int32_t numSlaves) ;
  virtual ~NetRodModule();                                  // Destructor
  
  // Accessor functions


  bool isSane(); 
  uint32_t fmtVersion();
  uint32_t efbVersion();
  uint32_t rtrVersion();
  uint32_t rcfVersion();

  uint32_t getBaseAddress() {return m_baseAddress; }

/*! initialize() performs all actions necessary to place a properly loaded ROD 
in its initialized state, except for initializing the slave DSPs. This must be done
separately using initSlaveDsp().

Activities:
    Reset the ROD
    Read ROD serial number
    Load 0x000l000l into Master DSP HPIC register. (Says low order 16-bit word
                                                    comes first)
    Read back HPIC to verify
    Retrieve pointers to text buffer structs
    Initialize PrimList and TextBuff state variables
*/
  void initialize();
  void initialize(bool resetFlag) ;
  
/*! reset() issues a reset signal to the ROD, clears IDRAM, resets all DSPs, and
 *  re-initializes all state variables.
*/
  void reset() ;

/*! fpgaReset() resets individual FPGAs on the ROD. The ROD can reset only one or all at a time
 */
  void fpgaReset(FpgaType targetFpga) ;

/*! dspReset() resets individual DSPs on the ROD. The ROD can reset only one or all at a time
 */
  void dspReset(DspType targetDsp);

/*! status() reports the status of the ROD.
 *  For now, it simply prints to standard output. Later we will add a structured
 *  report of name=value pairs.
*/
  void status();

/*! verify checks the h.o. byte of the serial number register to see that it contains
 *  the hex value 'ad', which is the signature of a ROD. It returns true if this is\
 *  the case, false otherwise.
*/
  bool verify();
  
/*! loadAndStartSlaves() reads a binary file from disk and loads it into the slave
 *  memory space and boots up the specified Slaves
 *  !!!THIS IS THE PREFERRED WAY TO LOAD A SLAVE IMAGE!!! - do not use
 *  hpiLoadSlaveImage without having a good reason to do so.
 */
  int loadAndStartSlaves(const std::string & filename, 
			 const int slaveMask) {return -1;};
  int loadAndStartFpgaSlaves(const std::string & filename, 
			 const int slaveMask) 
       ;

/*! startSlave() just starts the slaves and checks that it has booted
 */

  int startSlave(const int slaveId);
     
/*! loadSlaveImage() reads a binary file from disk and loads it into the slave
 *  memory space at the specified location using the RW_SLAVE_MEMORY primitive.
 *  !!!THIS IS THE PREFERRED WAY TO LOAD A SLAVE IMAGE!!! - do not use
 *  hpiLoadSlaveImage without having a good reason to do so.
 *  Usually, the optional last argument should be omitted.
 */
  void loadSlaveImage(const std::string & filename, const uint32_t address,
        const int32_t slaveNumber, char opt='n') 
     ;
                      
/*! hpiLoadSlaveImage() reads a binary file from disk and loads it into the slave
 *  memory space at the specified location.
 *  !!!THIS IS NO LONGER THE PREFERRED WAY. YOU'D BETTER HAVE A GOOD READSON TO USE
 *  IT !!!
 *  Usually, the optional last argument should be omitted.
 */
  void hpiLoadSlaveImage(const std::string & filename, const uint32_t address,
        const int32_t slaveNumber, char opt='n') 
        ;
                      
/*! startSlave() sends a START_SLAVE_EXECUTING primitive to start a given slave
 *  DSP. Usually, the optional last argument should be omitted.
 */
  //void startSlave(const int32_t slaveNumber, char mode='s') throw(RodException &, VmeException &);
  
/*! synchSendPrim() synchronously sends a single primitive to the MDSP and 
 *  handles the calls to primHandler() to finish the transfer before returning 
 *  control to its calling routine.  It is not the normal way to send 
 *  primitives, but is useful for initialization where time criticality is not 
 *  so much of a factor.
 *
 *  The present version writes to cout. This needs to be changed.
 */
  void synchSendPrim(RodPrimitive & prim) ;

/*! sendPrimList and sendSlavePrimList send a PrimList to the respective buffers
 */
  void sendPrimList(RodPrimList *l)  ;
  void sendSlavePrimList(RodPrimList *slvPrimList, int slave)  ;

/*! validateReplyBuffer() checks the consistency of the reply buffer */
int validateReplyBuffer() ; 

/*! primHandler() handles steps 2-4 in sending a primitive list to
 *  the master DSP. Its action depends on the value of the m_myPrimState state
 *  variable.
 */
  RodPrimList::PrimState primHandler() ;
  
//! Delete reply buffer object
  void deleteOutList();

/*! getTextBuffer() reads a complete text buffer into the return 
 *  string rettxtBuff. buffnum selects  0 for ERR buffer, 1 for INFO buffer. 
 *  If reset is true, the textBuffers on the DSP are emptied after 
 *  beeing read.
 */
  bool getTextBuffer(std::string &rettxtBuf, unsigned int buffnum, 
		     bool reset) ;
  
//! Inline method to set VmeCommand Register Bit
  inline void setVmeCommandRegBit(const int32_t bitNumber) {
    //AKDEBUG
/*     m_vmeCommandReg = mdspSingleRead(COMMAND_REGISTER); */
/*     setBit(&m_vmeCommandReg, bitNumber); */
/*     mdspSingleWrite(COMMAND_REGISTER, m_vmeCommandReg); */
    return;
  };
  
//! Inline method to clear VmeCommand Register Bit
  inline void clearVmeCommandRegBit(const int32_t bitNumber) {
    //AKDEBUG
/*     m_vmeCommandReg = mdspSingleRead(COMMAND_REGISTER); */
/*     clearBit(&m_vmeCommandReg, bitNumber); */
/*     mdspSingleWrite(COMMAND_REGISTER, m_vmeCommandReg); */
    return;
  };
  
/*! hpiLoad() loads a 32-bit value into the HPIA or HPID register. Although 
 *  the HPI has a 16-bit data path, we use a 32-bit VME access and the word is 
 *  broken into two 16-bit values in the ROD firmware.
*/
  void hpiLoad(const uint32_t hpiReg, const uint32_t hpiValue) 
      ;

/*! hpiFetch() fetches a 32-bit value from one of the HPI registers. Although 
 *  the HPI has a 16-bit data path, we use a 32-bit VME access, with the ROD 
 *  firmware doing the necessary conversion.
*/
  uint32_t hpiFetch(const uint32_t hpiReg) ;

/*! mdspSingleRead() reads a single 32-bit word from the Master DSP SDRAM via 
 *  its Host Port Interface (HPI).
*/
  uint32_t mdspSingleRead(const uint32_t dspAddr) ;

/*! mdspSingleWrite() writes a single 32-bit word to the Master DSP SDRAM via 
 *  its Host Port Interface (HPI).
*/
  void mdspSingleWrite(uint32_t dspAddr, uint32_t buffer) 
  ;

/*! mdspBlockRead() reads a block of size wordCount 32-bit words from the Master 
 *  DSP SDRAM via its Host Port Interface (HPI). 
 *  The present version uses a loop over single word reads instead of a block
 *  read.
*/
  void mdspBlockRead(const uint32_t dspAddr, uint32_t *buffer, 
       int32_t wordCount, HpidMode mode=DYNAMIC) 
      ;

/*! mdspBlockWrite() writes a buffer of wordCount 32-bit words to the Master DSP 
 *  SDRAM via its Host Port Interface (HPI).
 *  The present version uses a loop over single word writes instead of a block
 *  write.
*/
  void mdspBlockWrite(const uint32_t dspAddr, uint32_t *buffer, 
       int32_t wordCount, HpidMode mode=DYNAMIC) 
    ;

/*! mdspBlockDump() reads a block of MDSP memory and write it to a binary file 
 *  (debug use only)
 */
  void mdspBlockDump(const uint32_t firstAddress, 
       const uint32_t lastAddress, const std::string & fileName) 
     ;
                     
/*! slvHpiLoad() loads a 32-bit value into a slave's HPIA or HPID register. 
 *  Endian swapping, if needed, is done in mdspSingleWrite.
 */
  void slvHpiLoad(uint32_t hpiReg, uint32_t hpiValue,
                           int32_t slaveNum);
                           
/*! slvHpiFetch() fetches a 32-bit value from a slave's HPI registers. 
 *  Endian swapping, if needed, is done in mdspSingleRead.
 */
  uint32_t slvHpiFetch(uint32_t hpiReg, int32_t slaveNum) 
               ;

/*! slvHpiSingleRead() reads a single 32-bit word from the the memory space of a 
 *  slave DSP. Endian swapping, if needed, is done in mdspSingleRead.
 *  Note: this is not the method you should normally use; it risks
 *  conflicts with MDSP access to the slave memory. Use slvSingleRead() instead.
 */
  uint32_t slvHpiSingleRead(uint32_t dspAddr, int32_t slaveNum ) 
                ;
                                          
/*! slvHpiSingleWrite() writes a single 32-bit word to a slave DSP SDRAM via its
 *  Host Port Interface (HPI). Endian swapping, if needed, is done in 
 *  mdspSingleWrite.  Note: this is not the method you should normally use; it risks
 *  conflicts with MDSP access to the slave memory. Use slvSingleWrite() instead.
 */
  void slvHpiSingleWrite(uint32_t dspAddr, uint32_t buffer, 
       int32_t slaveNum);

/*! slvHpiBlockRead() reads a block of size wordCount 32-bit words from a slave 
 *  DSP's SDRAM via its Host Port Interface (HPI). Endian swapping, if needed, 
 *  is done in mdspBlockRead. Usually, the optional last argument should be
 *  omitted. Note: this is not the method you should normally use; it risks
 *  conflicts with MDSP access to the slave memory. Use slvBlockRead() instead.
 */
  void slvHpiBlockRead(const uint32_t dspAddr, uint32_t buffer[],
       const int32_t wordCount, int32_t slaveNum, HpidMode mode=DYNAMIC) 
      ;
                
/*! slvHpiBlockWrite() writes a buffer of wordCount 32-bit words to a slave DSP's 
 *  SDRAM via its Host Port Interface (HPI). Endian swapping, if needed, is 
 *  done in mdspBlockWrite. Usually, the optional last argument should be
 *  omitted. Note: this is not the method you should normally use; it risks
 *  conflicts with MDSP access to the slave memory. Use slvBlockWrite() instead.
 */
  void slvHpiBlockWrite(uint32_t dspAddr, uint32_t buffer[], 
       const int32_t wordCount, int32_t slaveNum, HpidMode mode=DYNAMIC) 
       ;
         
/*! slvSingleRead() reads a single 32-bit word from the the memory space of a 
 *  slave DSP via the RW_SLAVE_MEMORY primitive. This is method is STRONGLY preferred
 *  over using slvHpiSingleRead since it does not risk conflicting with MDSP access of 
 *  the slave's memory at the same time.
 */
  uint32_t slvSingleRead(uint32_t dspAddr, int32_t slaveNum ) 
               ;
                                          
/*! slvSingleWrite() writes a single 32-bit word to a slave DSP SDRAM via the 
 *  RW_SLAVE_MEMORY primitive. This is method is STRONGLY preferred over using 
 *  slvHpiSingleWrite since it does not risk conflicting with MDSP access of the slave's 
 *  memory at the same time.
 */
  void slvSingleWrite(uint32_t dspAddr, uint32_t buffer, 
       int32_t slaveNum);

/*! slvBlockRead() reads a block of size wordCount 32-bit words from a slave 
 *  DSP's SDRAM via the RW_SLAVE_MEMORY primitive. This is method is STRONGLY
 *  preferred over using slvHpiBlockRead since it does not risk conflicting with MDSP
 *  access of the slave's memory at the same time. Usually, the optional last 
 *  argument should be omitted.
 */
  void slvBlockRead(const uint32_t dspAddr, uint32_t buffer[],
       const int32_t wordCount, int32_t slaveNum, HpidMode mode=DYNAMIC) 
      ;
                
/*! slvPrimBlockWrite() writes a buffer of wordCount 32-bit words to a slave DSP's 
 *  SDRAM via the RW_SLAVE_MEMORY primitive. This is method is STRONGLY
 *  preferred over using slvHpiBlockWrite since it does not risk conflicting with MDSP
 *  access of the slave's memory at the same time.
 *  Usually, the optional last argument should be omitted.
 */
  void slvBlockWrite(uint32_t dspAddr, uint32_t buffer[], 
       const int32_t wordCount, int32_t slaveNum, HpidMode mode=DYNAMIC) 
       ;
         
/*! resetMasterDsp() resets the Master DSP via FPGA Control Register 2
 */
  void resetMasterDsp() ;
  
/*! resetSlaveDsp() resets a slave DSP via FPGA Control Register 2
 */
  void resetSlaveDsp(int32_t slaveNumber) ;
  
/*! resetAllDsps() resets all five DSPs via FPGA Control Register 2
 */
  void resetAllDsps();
  
/*! chipEraseHpi(VmeException &) erases the master program flash memory chip.
 */
  void chipEraseHpi();
  
/*! sectorErase() erases a sector of FPGA flash memory.
 */
  void sectorErase(uint32_t sectorBaseAddress) 
       ;
  
/*! writeByteToFlash() writes a byte to FPGA flash memory.
 */
  void writeByteToFlash(uint32_t address, uint8_t data) 
      ;
  
/*! writeBlockToFlash() writes a block to FPGA flash memory.
 */
  void writeBlockToFlash(uint32_t address, uint8_t *data, 
                      uint32_t numBytes) ;
                      
/*! writeBlockToFlashHpi() writes a block to the master program flash memory.
 */
  void writeBlockToFlashHpi(uint32_t address, uint8_t *data, uint32_t numBytes)
                           ;
                            
/*! readByteFromFlash( reads a byte from FPGA flash memory.
 */  
  uint8_t readByteFromFlash(uint32_t address, int32_t updateAddress) 
                           ;
                            
/*! vmeWriteElementFlash() does a VME write of an element to an FPGA flash.
 */  
  void vmeWriteElementFlash(uint8_t value, uint32_t address, 
                 int32_t handshakeBit) ;
                 
/*! readBlockFromFlash() reads a block from FPGA flash memory.
 */  
  void readBlockFromFlash(uint32_t address, uint8_t *buffer, uint32_t
                          numBytes) ;
                          
/*! commonEraseCommands() implements common erase commands for the FPGA flash.
 */  
  void commonEraseCommands(uint32_t flashBaseAddr) ;
  
/*! commonEraseCommandsHpi() implements common erase commands for the master 
 *  program flash.
 */  
  void commonEraseCommandsHpi(uint32_t flashBaseAddr) ;
   
/*! getFlashSectorSize() test the board revision and returns the correct value
 *  for the flash sector size.
 */
  uint32_t getFlashSectorSize();
  
/*! sleep() sleeps for a given number of milliseconds, VmeException &
*/
  //void sleep(const double milliSecs);
  inline void sleep(const uint32_t milliSecs){usleep(milliSecs*1000);}
/*! checkSum() calculates a bit-wise XOR checksum on an array
*/
  uint32_t checkSum(const uint32_t *sourceArray, const int32_t wordCount);
  
/*! endianReverse32( reverses the endian-ness of a 32 bit value
*/
  uint32_t endianReverse32(const uint32_t inVal);
  
/*! Checks BOC status. If no BOC clock or BOC is busy, it returns 0; if BOC is OK it returns 1.
*/
  uint32_t bocCheck();
  
void readMasterMem(int startAddr, int nWords, std::vector<unsigned int> &out) ;
void writeMasterMem(int startAddr, int nWords, std::vector<unsigned int> &in) ;
void readSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) ;
void writeSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) ;
void readSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) ;
void writeSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) ;
void readFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &out) ;
void writeFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &in) ;
void writeRegister(int regId, int offset, int size, unsigned int value) ;
unsigned int readRegister(int regId, int offset, int size) ;
void executeMasterPrimitive(RodPrimitive &prim) ;
void executeMasterPrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out) ;
void executeMasterPrimitiveSync(RodPrimitive &prim, RodOutList* &out) ;
void executeMasterPrimitiveSync(RodPrimitive &prim) ;
void executeSlavePrimitive(RodPrimitive &prim, int slave) ;
void executeSlavePrimitiveOnAll(RodPrimitive &prim) ;
void executeSlavePrimitiveOnAllSync(RodPrimitive &prim) ;
void executeSlavePrimitiveOnAllSync(RodPrimitive &prim, std::vector<unsigned int> &out) ;
void executeSlavePrimitiveOnAllSync(RodPrimitive &prim, RodOutList* &out) ;
void executeSlavePrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out, int slave) ;
void executeSlavePrimitiveSync(RodPrimitive &prim, RodOutList* &out, int slave) ;
void executeSlavePrimitiveSync(RodPrimitive &prim, int slave) ;
void waitPrimitiveCompletion() ;
void waitPrimitiveCompletion(std::vector<unsigned int> &out) ;
void waitPrimitiveCompletion(RodOutList* &out) ;

void initStatusRead();
QuickStatusInfo* talkStatusRead();

private:

    
}; // End of class RodModule declaration

} //  End namespace SctPixelRod

//---------------------------- Overload operator<< -----------------------------
/* This overloaded operator lets us use cout to print the status of the ROD
 * and exception information.
*/
  std::ostream& operator<<(std::ostream& os, SctPixelRod::NetRodModule& rod);

#endif // SCTPIXELROD_RODMODULE_H

