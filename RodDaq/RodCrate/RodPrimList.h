// File: RodPrimList.h

#ifndef SCTPIXELROD_RODPRIMLIST_H
#define SCTPIXELROD_RODPRIMLIST_H

#include <list>
#include <string>
#include <fstream> 
#include <iostream>
#include "RodPrimitive.h"
#include "BaseException.h"
#include <cstdint>


namespace SctPixelRod {

/*!
 * @class PrimListException
 *
 * @brief This is a class to handle exceptions in the PrimList Class.
 *
 * @author Tom Meyer (meyer@iastate.edu) - originator
 */

class PrimListException : public BaseException {
public:
  PrimListException( std::string descriptor, uint32_t data1, uint32_t data2);
  uint32_t getData1() const {return m_data1;};
  uint32_t getData2() const {return m_data2;};
  virtual void what(std::ostream&) const {};
  virtual std::ostream& what(std::ostream&, PrimListException&) const;
  
private:
  uint32_t m_data1;    // First data value returned
  uint32_t m_data2;    // Second data value returned
};                                                                            

/*!
 * @class RodPrimList
 *
 * @brief This is a list of RodPrimitives to be sent to the RodModule.
 *
 * This class is a list of the primitives that control ROD behavior.  
 * It is sent to the RodModule via its Send function.  As it is derived 
 * from the STL list, it inherits all the usual list functionality.
 *
 * @author Tom Meyer (meyer@iastate.edu) - originator
 */

class RodPrimList : public std::list<RodPrimitive> {
  private:
    //! Buffer to send
    uint32_t *m_buffer;
    //! Buffer size
    uint32_t m_bufferSize;
    //! List header
    MsgListHead m_listhead;
    //! List tail
    MsgListTail m_listtail; 

  public:
    enum PrimState {IDLE, LOADED, EXECUTING, WAITING, PAUSED};

    //! Constructor with specified index
    RodPrimList(uint32_t theIndex) : 
      std::list<RodPrimitive>(), m_buffer(0), m_bufferSize(0) 
      {m_listhead.index = theIndex;};
    //! Default constructor
    RodPrimList() : 
      std::list<RodPrimitive>(), m_buffer(0), m_bufferSize(0) 
      {m_listhead.index = 0;};
    //! Copy constructor 
    RodPrimList(const RodPrimList &rhs); 
    //! Overloaded assignment
    RodPrimList &operator=(const RodPrimList&);
    //! Destructor
    ~RodPrimList();

    //! Compute list checksum
    uint32_t checkSum();   
    //! Compute number of words in list
    uint32_t numWords();   

    //! Create the buffer to send
    void bufferBuild();
    
    //! Write the buffer to an XML file. Buffer must already be built when this is called.
    void writeToXml(std::string& xmlFile);

    //! Create the buffer from an XML file
    void buildFromXml(std::string& xmlFile);
    
    //! Return the buffer lingth.
    int32_t getBufferLength() { return m_buffer[0]; };

    //! Return the buffer.
    uint32_t *getBuffer() { return m_buffer; };

    //! Print the primitive list
    void print();

    //! Clear the primitive list
    void clear();

    //! Set the list identifier index
    void setIndex(uint32_t i) { m_listhead.index = i; }  
    //! Get the list identifier index
    uint32_t getIndex() const { return m_listhead.index; } ;    

    // Set the version number
    // void setVersion(uint32_t version) { m_listhead.index=version; }  
    // Get the version number
    // uint32_t getVersion() const { return m_version; } ;    
};

} // End namespace SctPixelRod

  std::ostream& operator<<(std::ostream& os, SctPixelRod::PrimListException& primExcept);

#endif // SCTPIXELROD_RODPRIMLIST_H
