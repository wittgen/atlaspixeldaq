//File: IblRodModule.cxx
// $Header$

/* 
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 *          R. Smith <russell.smith@cern.ch>
 * Date: 2014-III-05
 *
 * Description: low-level class to handle the IBL ROD
 */

#define R_BOGUS 0xFFFF

#include "IblRodModule.h"

#include <string.h>
#include <sys/stat.h>

//#ifdef IBL
#define NUM_SLAVES N_IBL_SLAVES
#include "iblSlaveCmds.h"

int crc32(char *p, int nr, uint32_t *cval);
//#else
//#define NUM_SLAVES N_SLAVES
//#endif

//#define VERBOSE

//! Namespace for the common routines for SCT and PIXEL ROD software.
namespace SctPixelRod {


  // Flag to force endian-ness swap before calls to HPI registers.  This should be
  // a temporary patch.

  static int32_t endianSwap = 0;
  //static const int32_t rodInitTimeout = 10;
  //todo clean up all this 
  //todo either delete these or put in initialization list
  IblRodModule::IblRodModule( uint32_t baseAddr, uint32_t mapSize, int32_t numSlaves, uint16_t srvIpPort, std::string ipAddress) :
    AbstrRodModule(baseAddr>>24 , numSlaves), m_srvIpPort(srvIpPort), m_ipAddress(ipAddress)   {

    m_serialNumber = 0xFFFFFFFF;  // Will be overwritten during init
    m_revision = 0;               // Will be overwritten during init
    m_finBufferSize = 4096;
    m_masterImageName = std::string("");
    m_myOutList = 0;
    m_vmeCommandReg = 0;
    m_rodStatusReg[0] = 0;
    m_rodStatusReg[1] = 0;
    m_debug = 0;
    m_debugFile = "rodprimitives.bin";
    m_textData = 0;
    m_btFailure = false;

    memset(&m_masterXface, 0, sizeof(m_masterXface));

    m_myPrimState = RodPrimList::IDLE;

    // AKU
    //std::cout << "creating of BOC supressed" << std::endl;
    try{
      m_server = new ServerInterface();
    }
    catch (const std::bad_alloc & ba) {
      throw RodException("Failed to get VME Port.");
    }
  }

  //---------------------------------Destructor------------------------------
  /*
    Be sure to delete all VME Ports and other "new" things in constructor.
  */

  IblRodModule::~IblRodModule() {
    if (m_server != NULL) delete m_server;

    if (m_textData){
      for (unsigned int i=0; i< n_textbuffers; i++) {
	if (m_textData[i]) delete [](m_textData[i]);
      }
      delete []m_textData;
      m_textData=NULL;
    }
    return;
  }

  //  Member methods

  //--------------------------------initialize---------------------------------

  /* This method performs all actions necessary to place a properly loaded ROD
     in its initialized state, except for initializing the slave DSPs. This must be done
     separately using initSlaveDsp().

     Activities:
     Reset the ROD
     Read ROD serial number
     Load 0x000l000l into Master DSP HPIC register. (Says low order 16-bit word
     comes first)
     Read back HPIC to verify
     Load MDSP memory map
     Retrieve pointers to text buffer structs
     Initialize PrimList and TextBuff state variables
  */
  void IblRodModule::initialize() {initialize(false); return;}

  void IblRodModule::initialize(bool resetFlag)  {
    //uint32_t hpicReadback;   // for debugging
    uint32_t mdspStart;


    std::cout << "IblRodModule::initialize" << std::endl;


    int i_count = 0;
    bool i_done = false;
    while(!i_done) {
      i_count++;
      // If reset flag is set reset the ROD so it starts in a known state
      if (resetFlag) {
	int r_count = 0;
	bool r_done = false;
	while (!r_done) {
	  try {
	    r_count++;
	    reset();
	    r_done = true;
	  }
	  catch (const RodException &r) {
	    if (r_count > 2) {
	      throw r;
	    }
	    std::cout << "S" << m_slot << " retry ROD reset (count = " << r_count << ")" << std::endl;
	  }
	}
	// Wait an extra half a second
	sleep(500);
      }

      // Read ROD serial number
      //K m_serialNumber = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
      if (endianSwap) m_serialNumber = endianReverse32(m_serialNumber); // Temporary (I hope)
      m_revision = (m_serialNumber&0x00ff0000)>>16;
      m_serialNumber = m_serialNumber & 0x3ff;   // Mask to get 10 least significant bits

      //	Initialize Master HPIC register to set half-word order


      mdspStart = mdspSingleRead(MEMORYMAP_REG);
      std::cout << "S" << m_slot << " reading MEMORYMAP_REG: " << std::hex << MEMORYMAP_REG
		<< " start " << std::hex << mdspStart << std::endl;

      // read interface structure
      mdspBlockRead(mdspStart, (uint32_t*)&m_masterXface,
		    sizeof(MasterXface)/4);

      if (m_masterXface.xface.nTextBuffers > 10) {
	if (i_count > 2) {
	  throw RodException("Invalid number of text buffers returned by the ROD, aborting", m_slot);
	} else {
	  resetFlag = true;
	  std::cout << "S" << m_slot << " retry ROD initialize (count = " << i_count << ")" << std::endl;
	  continue;
	}
      }
#ifdef VERBOSE
      std::cout << "S" << m_slot << " nTextBuffers: " << m_masterXface.xface.nTextBuffers
		<< std::endl;
      std::cout << "S" << m_slot << " nTextBufferSlots: " << m_masterXface.xface.nTextBufferSlots
		<< std::endl;
#endif

      // number of textbuffers per master and 4 slaves
      n_textbuffers = m_masterXface.xface.nTextBuffers*m_masterXface.xface.nTextBufferSlots;
      std::cout << "Total number of text buffers: " << std::dec << n_textbuffers << std::endl; // AKU

      m_textBuffer     = new uint32_t[n_textbuffers];
      m_textBufferSize = new uint32_t[n_textbuffers];

      // read pointers to text buffer queue structures
      mdspBlockRead(m_masterXface.xface.textBuffer,
		    (uint32_t*)m_textBuffer,
		    n_textbuffers);

      // AKU
      /*
	for (int i=0;i<n_textbuffers;i++){
        std::cout << "Text buffer " << i << ": " << m_textBuffer[i] << std::endl;
	}*/

      TextQueue textQueue;
      if(m_textData){
	for (unsigned int i=0; i< n_textbuffers; i++) {
	  if (m_textData[i]) delete [](m_textData[i]);
	}
	delete []m_textData;
	m_textData=NULL;
      }
      m_textData = new char*[n_textbuffers];
      for (unsigned int i=0; i < n_textbuffers; i++) m_textData[i] = 0;

      for (unsigned int i=0; i< n_textbuffers; i++) {
	mdspBlockRead(m_textBuffer[i], (uint32_t*)&textQueue,
		      sizeof(TextQueue)/4);
	m_textBufferSize[i] = textQueue.length;
	m_textData[i] = new char[sizeof(uint32_t)*m_textBufferSize[i]];
#ifdef VERBOSE
	std::cout << "S" << m_slot << " RodModule TextBuffer " << i << " size: " << m_textBufferSize[i] << std::endl;
#endif
      }
      i_done = true;
    }

    //  Reinitialize state variables
    m_myPrimState = RodPrimList::IDLE;

    std::cout << "S" << m_slot << " IblRodModule::initialized finished" << std::endl;
    return;
  }

  bool IblRodModule::isSane() {
    bool sane = false;

    uint32_t fpgaStatus6 = 0xdeadbeef; //K m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
    uint32_t maskedCor   = fpgaStatus6 & 0xff000000; // correct byte order
    uint32_t maskedRev   = fpgaStatus6 & 0x000000ff; // wrong byte order
    sane = ((maskedCor == 0xad000000) || (maskedRev == 0x000000ad));

    return sane;
  }

  uint32_t IblRodModule::fmtVersion(){
    uint32_t version = mdspSingleRead(0x00400088);
    return ((version & 0xFFF)<<4) + ((version & 0xf00000)>>20);
  }

  uint32_t IblRodModule::efbVersion(){
    uint32_t version = mdspSingleRead(0x0040221C);
    return (version & 0x00008FFF);
  }

  uint32_t IblRodModule::rtrVersion(){
    uint32_t version = mdspSingleRead(0x0040250C);
    return (version & 0x00008FFF);
  }

  uint32_t IblRodModule::rcfVersion(){
    uint32_t version = mdspSingleRead(0x0040440C);
    return (version & 0x00000FFF);
  }


  //---------------------------------reset------------------------------------
  /* */
  /* This method resets the ROD. */
  /* */

  void IblRodModule::reset() {
    // Reset all FPGAs and DSPs (bit 6 = 0x40)
    //const uint32_t rodResetValue = 0x00000040;
    //uint32_t rodReset;
    uint32_t rodRegValue=0, dspRegValue;
    clock_t startTime;
    unsigned int count;

    std::cout << "S" << m_slot << " IblRodModule::reset " << std::endl;
    //if (endianSwap) {
    //  rodReset = endianReverse32(rodResetValue);
    //}
    //else {
    //  rodReset = rodResetValue;
    //}

    // mark MDSP as in unknown state
    mdspSingleWrite(MAGIC_LOCATION, 0xDEADDEAD);
    std::cout << "S" << m_slot << " IblRodModule::reset MAGIC_LOCATION " << std::hex
	      << MAGIC_LOCATION << std::endl;

    // reset all
    //K m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);
    sleep(1000);

    std::cout << "S" << m_slot << " IblRodModule::reset RESET" << std::endl;
    // check  for finish of FPGA reset
    startTime = time(0);
    count = 0;
    do {
      //K rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[0]);
      std::cout << "reset status: 0x" << std::hex << rodRegValue << std::endl;
      if ((time(0)-startTime) > 5 && count > 10) {
	throw RodException("FPGA reset timeout in reset(), DSP_RESET_TIMEOUT=5", m_slot);
      }
      count++;

      // Check that reset has finished
      // leave if PRM configuration mode active
    } while (((rodRegValue&0x3f) != FPGA_RESET_COMPLETE) &&
	     ((rodRegValue&0x20) != 0x20));

    // check  for finish of DSP reset
    rodRegValue = 0;
    startTime = time(0);
    count = 0;
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
      if ((time(0)-startTime) > 5 && count > 10) {
	throw RodException("DSP reset timeout in reset(), DSP_RESET_TIMEOUT=5", m_slot);
      }
      count++;
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);

    // wait for MDSP boot indication
    startTime = time(0);
    count = 0;
    do {
      dspRegValue = mdspSingleRead(MAGIC_LOCATION);
      if ((time(0)-startTime) > 15 && count > 10) {
	throw RodException("MDSP boot timeout in reset(), DSP_RESET_TIMEOUT=15", m_slot);
      }
      count++;
    } while(dspRegValue != I_AM_ALIVE);

    //  Reinitialize state variables
    m_myPrimState = RodPrimList::IDLE;
    return;
  }

  //---------------------------------fpgaReset------------------------------------

  /*! This method resets individual FPGAs on the ROD. The ROD can reset only
    one FPGA or else all FPGAs at a time.
  */
  void IblRodModule::fpgaReset(FpgaType targetFpga) {
    /* Reset one FPGA via FPGA Reset Control Register (0xC00004)
       The values for the targetFpga are set in an enum called FpgaType. The values are:
       0 = Controller
       1 = Formatter
       2 = Formatter
       3 = EFB
       4 = Router
       5 = All
    */
    uint32_t resetValue;
    uint32_t reset;
    uint32_t rodRegValue;
    clock_t startTime;

    if ((targetFpga != 0) && (targetFpga != 1) && (targetFpga != 2) && (targetFpga != 3) &&
	(targetFpga != 4) && (targetFpga != 5)) {
      std::cout << "Invalid argument to fpgaReset. Only one bit can be set." << std::endl;
      return;
    }
    resetValue = 0x00000001<<targetFpga;
    std::cout << "IblRodModule::fpgaReset " << std::endl;
    if (endianSwap) {
      reset = endianReverse32(resetValue);
    }
    else {
      reset = resetValue;
    }
    std::cout << "Set the reset bit (it is self-clearing)" << std::endl;
    // Set the reset bit (it is self-clearing)
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[1], reset);

    std::cout << "check  for finish of FPGA reset." << std::endl;
    // check  for finish of FPGA reset. The FPGA Reset Status Register is at VME 0xc00024.
    startTime = time(0);
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[1]);
      if ((time(0)-startTime) > DSP_RESET_TIMEOUT) {
	throw RodException("FPGA reset timeout in reset(), DSP_RESET_TIMEOUT=",
			   DSP_RESET_TIMEOUT);
      }

      // leave if PRM configuration mode active (check bit 5 in FPGA Configuration Status
      // Register at 0xc00020.
    } while (((rodRegValue&0x3f) != FPGA_RESET_COMPLETE) &&
	     ((rodRegValue&0x20) != 0x20));

    std::cout << "Reinitialize state variables" << std::endl;
    //  Reinitialize state variables
    m_myPrimState = RodPrimList::IDLE;
    return;
  }

  //---------------------------------dspReset------------------------------------

  /*! This method resets individual DSPs on the ROD. The ROD can reset only one DSP
    or else all DSPs at a time.
  */
  void IblRodModule::dspReset(DspType targetDsp)  {
    /* Reset one DSP via DSP Reset Control Register (0xC00008)
       The values for the targetDsp are set in an enum called DspType. The values are::
       1 = Master
       2 = Slave 0
       3 = Slave 1
       4 = Slave 2
       5 = Slave 3
    */
    uint32_t resetValue;
    uint32_t reset;
    uint32_t rodRegValue;
    clock_t startTime;

    if ((targetDsp != 0) && (targetDsp != 1) && (targetDsp != 2) && (targetDsp != 3) &&
	(targetDsp != 4) && (targetDsp != 5)) {
      std::cout << "Invalid argument to dspReset. Only one bit can be set." << std::endl;
      return;
    }
    resetValue = 0x00000001<<targetDsp;
    std::cout << "IblRodModule::fpgaReset " << std::endl;
    if (endianSwap) {
      reset = endianReverse32(resetValue);
    }
    else {
      reset = resetValue;
    }

    // Set the reset bit (it is self-clearing)
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], reset);

    // check  for finish of DSP reset.  The DSP Reset Status Register is at VME 0xc00028.
    rodRegValue = 0;
    startTime = clock();
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
      if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
	throw RodException("DSP reset timeout in reset(), DSP_RESET_TIMEOUT=",
			   (int32_t)DSP_RESET_TIMEOUT);
      }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);

    //  Reinitialize state variables
    m_myPrimState = RodPrimList::IDLE;
    return;
  }

  //----------------------------------verify----------------------------------

  /* This method checks the value of the h.o. byte of the
   */

  bool IblRodModule::verify() {
    uint32_t regVal;

    // Read status register 6
    regVal = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
    if (endianSwap) regVal = endianReverse32(regVal);
    regVal = regVal & 0xff000000;            // Mask to get most significant byte
    if (regVal!=0xad000000) return false;
    return true;
  }

  //---------------------------------status-----------------------------------

  /* This method reports the status of the ROD.
     For now, it simply prints to standard output.
  */
  void IblRodModule::status() {
    std::cout << "Slot: "                   << m_slot;
    std::cout << "; Serial Number:"         << m_serialNumber;
    std::cout << "; Number of slave DSPs: " << m_numSlaves;
    std::cout << std::endl;

    std::cout << std::endl;

    std::dec(std::cout);
    std::cout << "Primitive state: " << getPrimState() ;
    std::cout << std::endl;
    return;
  }




  // AKU: version for spartan-6 slaves. processes only one slave, indexed by number, not mask!
  int IblRodModule::loadAndStartFpgaSlaves(const std::string & fileName, const int slaveId)
     {
   int nWords;
    uint32_t size, offs;
    FILE *fp;

    /* first initialize the dsp after the hardware reset */
    SlaveCtrlIn slaveCtrlInI ;
    slaveCtrlInI.flag = SLAVE_INIT;
    slaveCtrlInI.slaveMask = slaveId;

    RodPrimitive *slvRWPrimI = new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t),
						1, SLAVE_CTRL, R_BOGUS, (int32_t*)&slaveCtrlInI);
    executeMasterPrimitiveSync(*slvRWPrimI);
    delete slvRWPrimI;

    // AKU: test for NULL file
    if (fileName.empty()){
      throw NoImageFile("Missing file name thrown from loadAndStartSlaves", fileName);
      return -1; /* no such file */
    }

    /* the largest primitive we can send */
    if((fp = fopen(fileName.c_str(), "r1")) == NULL) {
      throw NoImageFile("NoImageFile thrown from loadAndStartSlaves", fileName);
      return -1; /* no such file */
    }
    /* load the code into the slaves */

    // AKU: the file is raw binary . no dld headers etc
    struct stat fileStat;
    fstat(fileno(fp), &fileStat);
    size = fileStat.st_size;
    if (0 == size) {
      throw NoImageFile("No data in ImageFile", fileName);
      fclose(fp);
      return -1; /* bad file */
    }

    // read and load file in portions
    const uint32_t maxBlkSize = 1024; // must be power of 2
    uint32_t accMax = m_masterXface.xface.requestChannelLen[0] * sizeof(uint32_t); // jsv words or bytes
    uint32_t blkSize = maxBlkSize < accMax ? maxBlkSize : accMax;
    static unsigned char fBuf[sizeof(SlaveCtrlIn) + sizeof(DldHeader) + maxBlkSize];
    SlaveCtrlIn *svlCtrlIn = (SlaveCtrlIn*)&fBuf[0];
    DldHeader *dldHeader = (DldHeader*)&svlCtrlIn[1];
    uint32_t *data = (uint32_t*)&dldHeader[1];
    //char *fileBuf;
    int bsize;
    uint32_t crcVal1, crcVal2;

    // allocate buffer
    //if (0 == (fileBuf = (char*)malloc(size + 3))){  // allocate a bit more
    //  throw RodException("Failed to allocate file buffer");
    //}
    std::vector<char> fileBuf(size + 3,'\0');
    //// clear area

    // read file
    if (1 != fread(fileBuf.data(), size, 1, fp)){
      throw RodException("Failed to read slave file");
    }
    fclose(fp);
    // we have the full content, so compute local crc
    crc32(fileBuf.data(),size,&crcVal1);
    std::cout << "Local crc32: 0x" << std::hex << crcVal1 << std::endl;

    // download in portions
    std::cout << "Loading file ";
    offs = 0;
    while (offs < size){
      //bsize = fread(data, sizeof(uint32_t), blkSize/sizeof(uint32_t), fp);
      bsize = size - offs > blkSize ? blkSize : size - offs;
      memcpy(data, &fileBuf[offs], bsize);
      dldHeader->length = bsize / sizeof(uint32_t);
      //dldHeader->length = bsize; // size - offs > blkSize ? blkSize : size - offs;	// set size: max is blkSize
      //dldHeader->length = (dldHeader->length + 3) / sizeof(uint32_t);	// must be in words, rounded up
      dldHeader->addr = offs;
      dldHeader->fInternal = 0;
      svlCtrlIn->flag      = LOAD_COFF;
      svlCtrlIn->slaveMask = slaveId; // AKU: this is now a number. mask is not used any more
      svlCtrlIn->nSections = 1; // only 1 section always

      // nWords = (dldHeader->length + sizeof(DldHeader) + sizeof(SlaveCtrlIn)) / sizeof(int32_t); //  + 3 ????
      nWords = dldHeader->length + (sizeof(DldHeader) + sizeof(SlaveCtrlIn)) / sizeof(int32_t);

      RodPrimitive *slvCtrlPrimL =
	new RodPrimitive(nWords, 1, SLAVE_CTRL, R_BOGUS, (int32_t *)svlCtrlIn);

      executeMasterPrimitiveSync(*slvCtrlPrimL);
      delete slvCtrlPrimL;

      offs += dldHeader->length * sizeof(uint32_t);
      std::cout << "." << std::flush;
    }
    std::cout << std::endl;
    std::cout << "File loaded" << std::endl;

    /* check crc */

    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    RodOutList* outList;
    uint32_t *outBody;
    SendSlavePrimitiveIn *slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slaveId; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    IblSlvRdWr *slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_CRC;
    slvCmd->addr = 0;
    slvCmd->count = 1;
    slvCmd->data = size/sizeof(int32_t);
    RodPrimitive *sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
						    1, SEND_SLAVE_PRIMITIVE, 0, (int32_t*)slvCmdPrim);
    executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    outBody = (uint32_t *) outList->getMsgBody(1);
    crcVal2 = outBody[1];
    std::cout << "Slave CRC: " << std::hex << crcVal2 << std::endl;
    deleteOutList();
    delete sendSlvCmdPrim;

    if (crcVal1 != crcVal2){
      throw RodException("CRC mismatch");
    }
    std::cout << "CRC check OK" << std::endl;

    return startSlave(slaveId);

  }

  // AKU: version for spartan-6 slaves. processes only one slave, indexed by number, not mask!
  // Karolos: Purpose is to have separate functionality in separate functions
  int IblRodModule::startSlave(const int slaveId) {

    /* start */

    SlaveCtrlIn slaveCtrlInS;
    slaveCtrlInS.flag      = LOAD_DONE;
    slaveCtrlInS.slaveMask = slaveId;  // AKU: this is now a number. mask is not used any more
    RodPrimitive *slvRWPrimS =
      new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t), 1,
		       SLAVE_CTRL, R_BOGUS, (int32_t*)&slaveCtrlInS);
    std::cout << "Starting slave" << std::endl;
    executeMasterPrimitiveSync(*slvRWPrimS);
    delete slvRWPrimS;

    /* wait for boot */

    SlaveCtrlIn slaveCtrlInB;
    slaveCtrlInB.flag      = WAIT_BOOT;
    slaveCtrlInB.slaveMask = slaveId;
    RodPrimitive *slvRWPrimB =
      new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t), 1,
		       SLAVE_CTRL, R_BOGUS, (int32_t*)&slaveCtrlInB);
    std::cout << "Waiting for slave boot" << std::endl;
    executeMasterPrimitiveSync(*slvRWPrimB);
    delete slvRWPrimB;

    return 0;
  }


  //------------------------------loadSlaveImage-----------------------------
  void IblRodModule::loadSlaveImage(const std::string & fileName, const uint32_t address,
				    const int32_t slaveNum, char opt)  {
    std::cout << "In function "  << __PRETTY_FUNCTION__ << std::endl;
  }

  //--------------------------------hpiLoadSlaveImage-------------------------------
  void IblRodModule::hpiLoadSlaveImage(const std::string & fileName, const uint32_t address,
				       const int32_t slaveNum, char opt)  {
    std::cout << "In function "  << __PRETTY_FUNCTION__ << std::endl;
  }

  /*---------------------------------synchSendPrim------------------------------
    This version writes to cout. This needs to be changed.
  */

  //---------------------------------sendPrimList-------------------------------


  //---------------------------------sendSlavePrimList---------------------

  //---------------------------validateReplyBuffer ----------------


  //---------------------------------primHandler-------------------------------


  //--------------------------------deleteOutList--------------------------------

  //---------------------------------getTextBuffer-------------------------------

  bool IblRodModule::getTextBuffer(std::string &rettxtBuf, unsigned int buffnum,  bool reset) {

    TextQueue textQueue;
    std::string tmptxtBuf = "";
    bool read = false;

    std::cout << __FUNCTION__ << " " << __LINE__ << std::endl;

    // bail out if non existing textbuffer requested
    if (buffnum >  n_textbuffers) return read;

    //  for (int buffnum=0; buffnum< m_masterXface.xface.nTextBuffers; buffnum++) {
    mdspBlockRead(m_textBuffer[buffnum], (uint32_t *)&textQueue,
		  sizeof(TextQueue)/sizeof(uint32_t));
    // check if we found text
    uint32_t tail = textQueue.tail;
    int32_t nWords, nChars;
    if(0 != (nWords = textQueue.head - textQueue.tail)){
      std::cout << "DEBUG: IblRodModule::getTextBuffer(): trying to read " << nWords<<" words into "
		<< rettxtBuf<<std::endl;
      if (nWords > 4096){
	std::cerr <<"ERROR: IblRodModule::getTextBuffer(): nWords is larger than 4096!!! skipping download of text buffers"<<std::endl;
	return false;
      }
      read = true;
      std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
      // handle wrap
      if(nWords<0)nWords += textQueue.length;
      nChars = nWords*sizeof(uint32_t);
      int32_t nTop = textQueue.length - tail;
      uint32_t *buffPtr = (uint32_t*) m_textData[buffnum];
      if(nWords >  nTop){
	mdspBlockRead(textQueue.base + sizeof(uint32_t)*tail, buffPtr, nTop);
	buffPtr += nTop;
	nWords  -= nTop;
	tail = 0;
      }
      if(nWords){
	mdspBlockRead(textQueue.base + sizeof(uint32_t)*tail, buffPtr, nWords);
	tail += nWords;
      }

      std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
      if(reset){
	// reset tail to read position on DSP
	//TextQueue *DSPtextQueue = (TextQueue *)m_textBuffer[buffnum];
	//    tailoffset = &(queue.tail) - &queue;
	// AKDEBUG-> DANGEROUS, change interface
	// AKU mdspSingleWrite((uint32_t)&(DSPtextQueue->tail), tail);
	uint32_t tailoffset = &(textQueue.tail) - (uint32_t *)(&textQueue);
	mdspSingleWrite(m_textBuffer[buffnum]+tailoffset, tail);
      }
      // copy into output string, no real need to keep local buffer anymore,
      // but useful for debug

      for(int i = 0; i < nChars; i++){
	tmptxtBuf += m_textData[buffnum][i];
      }
    }
    //  }

    std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
    //copy constructor to output string
    rettxtBuf = tmptxtBuf;

    return read;
  }


  //----------------------------------hpiLoad------------------------------------

  //--------------------------------hpiFetch-------------------------------------


  //-----------------------------mdspSingleRead---------------------------------


  //------------------------------mdspSingleWrite------------------------------------


  //-----------------------------mdspBlockReadDMA---------------------------------

  //------------------------------mdspBlockWrite-----------------------------------


  //-----------------------------mdspBlockDump--------------------------------



  //--------------------------------slvHpiLoad-----------------------------------


  //------------------------------slvHpiFetch------------------------------------


  //-------------------------------slvHpiSingleRead---------------------------------


  //------------------------------slvHpiSingleWrite------------------------------------


  //-----------------------------slvHpiBlockRead---------------------------------

  //------------------------------slvHpiBlockWrite-----------------------------------


  //-------------------------------slvSingleRead---------------------------------


  //------------------------------slvSingleWrite------------------------------------


  //-----------------------------slvBlockRead---------------------------------

  //------------------------------slvBlockWrite-----------------------------------


  //------------------------------resetMasterDsp--------------------------------

  void IblRodModule::resetMasterDsp() {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 1);
    if (endianSwap) {
      value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
      if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
	throw RodException("DSP reset timeout in resetMasterDsp(), DSP_RESET_TIMEOUT=",
			   (int32_t)DSP_RESET_TIMEOUT);
      }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);
    return;
  }

  //------------------------------resetSlaveDsp--------------------------------

  void IblRodModule::resetSlaveDsp(int32_t slaveNumber) {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 2 + slaveNumber);
    if (endianSwap) {
      value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
      if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
	throw RodException("DSP reset timeout in resetSlaveDsp(), DSP_RESET_TIMEOUT=",
			   (int32_t)DSP_RESET_TIMEOUT);
      }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);
    return;
  }


  //------------------------------resetAllDsps--------------------------------

  void IblRodModule::resetAllDsps()  {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 6);
    if (endianSwap) {
      value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
      rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
      if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
	throw RodException("DSP reset timeout in resetAllDsps(), DSP_RESET_TIMEOUT=",
			   (int32_t)DSP_RESET_TIMEOUT);
      }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);
    return;
  }

  //--------------------------------sectorErase---------------------------------



  //---------------_-------------writeByteToFlash-------------------------------

  //----------------------------writeBlockToFlash-------------------------------

  //---------------------------writeBlockToFlashHpi-----------------------------


  //-----------------------------readByteFromFlash------------------------------



  //---------------------------vmeWriteElementFlash------------------------------


  //-----------------------------readBlockFromFlash------------------------------


  //-----------------------------commonEraseCommands----------------------------

  //----------------------------commonEraseCommandsHpi--------------------------


  //---------------------------getFlashSectorSize---------------------------

  //----------------------------------sleep-------------------------------------
  // ks: replaced buy an inline call to usleep(), which will suspend this thread and
  //     gives other threads the chances tpo talk to there rods
  /*void IblRodModule::sleep(const double milliSecs) {
    clock_t start, delay;
    delay = CLOCKS_PER_SEC;
    delay = clock_t(milliSecs * CLOCKS_PER_SEC / 1000 ); //need explicit type cast
    //to avoid warning.
    start = clock();
    while (clock()-start< delay)   // wait until time elapses
    ;
    return;
    }*/
  //--------------------------------checkSum------------------------------------

  uint32_t IblRodModule::checkSum(const uint32_t *sourceArray, const int32_t wordCount) {
    uint32_t result=0;
    int32_t i;

    for (i=0; i<wordCount; ++i) {
      result ^= sourceArray[i];
    }
    return result;
  }

  //-----------------------------endianReverse32----------------------------------

  uint32_t IblRodModule::endianReverse32(const uint32_t inVal) {
    uint32_t outVal;
    outVal  = (inVal & 0x000000ff) << 24;
    outVal |= (inVal & 0x0000ff00) << 8;
    outVal |= (inVal & 0x00ff0000) >> 8;
    outVal |= (inVal & 0xff000000) >> 24;
    return outVal;
  }

  //----------------------------------bocCheck------------------------------------
  /*! Checks BOC status. If no BOC clock or BOC is busy, it returns 0; if BOC is OK it returns 1.
    It also sets the data member m_bocFlag as follows:
    0 = BOC OK
    1 = No clock from BOC
    2 = BOC is busy
  */
  uint32_t IblRodModule::bocCheck() {
    uint32_t fpgaReg=0x404420;
    uint32_t clockBit=0x2, bocBusyBit=0x4;
    uint32_t regVal;
    regVal = mdspSingleRead(fpgaReg);
    //AKDEBUG careful this depends on the FPGA version due to a previous bug ....
    if ((regVal&clockBit)==1) {            // No clock
      m_bocFlag = 1;
      return 0;
    }
    if ((regVal&bocBusyBit)==1) {          // BOC busy
      m_bocFlag = 2;
      return 0;
    }
    m_bocFlag = 0;
    return 1;
  }

  void IblRodModule::readMasterMem(int startAddr, int nWords, std::vector<unsigned int> &out) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    mdspBlockRead(startAddr, buf, nWords);
    for (int i=0; i<nWords; i++) out.push_back(buf[i]);
    delete[] buf;
  }

  void IblRodModule::writeMasterMem(int startAddr, int nWords, std::vector<unsigned int> &in) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    for (int i=0; i<nWords; i++) buf[i] = in[i];
    mdspBlockWrite(startAddr, buf, nWords);
    delete[] buf;
  }

  void IblRodModule::readSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) {
    RwSlaveMemoryIn RSlaveMemoryIn;
    int slaveAddr = startAddr;
    int readWords = 0;

    out.clear();
    while((nWords>readWords)){

      // TBD JD: Find, whether we're good here.
      // Debug
      //    std::cout << "Just testing whether this procedure does anything at all..." << std::endl;
      //    std::cout << " IblRodModule::readSlaveMem nWords: " << nWords << " readWords: " << readWords << std::endl;
      RSlaveMemoryIn.slave      = slaveId;
      RSlaveMemoryIn.flags      = RW_READ;
      RSlaveMemoryIn.slaveAddr  = (uint32_t)slaveAddr;
      RSlaveMemoryIn.masterAddr = (uint32_t)NULL ;
      RSlaveMemoryIn.nWords     = nWords - readWords;

      RodPrimitive* rwsmPrim = new RodPrimitive(sizeof(RwSlaveMemoryIn)/sizeof(uint32_t), 0,
						RW_SLAVE_MEMORY, R_BOGUS,
						(int32_t*)&RSlaveMemoryIn);
      RodOutList* outList;
      executeMasterPrimitiveSync(*rwsmPrim, outList);
      delete rwsmPrim;
      if (outList == NULL) {
	return;
      } else {
	uint32_t *outBody = (uint32_t*)outList->getMsgBody(1);
	RwMemoryReplyHeader * RwMemoryReplyHeader_Out = (RwMemoryReplyHeader *) outBody;
	//Debug
	// std::cout << "Header says - Read nWords: " << RwMemoryReplyHeader_Out->nWords << " starting from 0x" << std::hex << RwMemoryReplyHeader_Out->addr << std::dec << " on Slave " << RwMemoryReplyHeader_Out->id << std::endl;
	readWords += RwMemoryReplyHeader_Out->nWords;
	slaveAddr += sizeof(uint32_t) * RwMemoryReplyHeader_Out->nWords;
	/* break if we don't read anything */
	if(0==RwMemoryReplyHeader_Out->nWords)readWords += nWords;
	for (unsigned int i=(sizeof(RwMemoryReplyHeader)/sizeof(uint32_t)); i<((sizeof(RwMemoryReplyHeader)/sizeof(uint32_t)) + RwMemoryReplyHeader_Out->nWords); i++) {
	  out.push_back(outBody[i]);
	}
	deleteOutList();
      }
    }
  }

  void IblRodModule::writeSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) {
    int32_t* rwsmData = new int32_t[nWords + sizeof(RwSlaveMemoryIn)/4];
    RwSlaveMemoryIn* WSlaveMemoryIn = (RwSlaveMemoryIn*)rwsmData;
    WSlaveMemoryIn->slave      = slaveId;
    WSlaveMemoryIn->flags      = RW_WRITE;
    WSlaveMemoryIn->slaveAddr  = (uint32_t)startAddr;
    WSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
    WSlaveMemoryIn->nWords     = nWords;

    int i;
    for (i=0; i<nWords; i++) rwsmData[sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)+i] = in[i];
    RodPrimitive* rwsmPrim = new RodPrimitive(sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)+nWords
					      , 0, RW_SLAVE_MEMORY, R_BOGUS, rwsmData);
    executeMasterPrimitiveSync(*rwsmPrim);
    delete rwsmPrim;
    delete[] rwsmData;
  }


  void IblRodModule::readFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &out) {
    RwFifoIn RwFifo_In;
    RwFifo_In.fifoId      = fifo;
    RwFifo_In.fRead       = RW_READ;
    RwFifo_In.length      = nWords;
    RwFifo_In.dataPtr     = (uint32_t )NULL;
    RodPrimitive* rwfPrim = new RodPrimitive(sizeof(RwFifoIn)/sizeof(uint32_t), 0,
					     RW_FIFO, R_BOGUS, (int32_t*)&RwFifo_In);
    RodOutList* outList;
    //  executeMasterPrimitiveSync(*rwfPrim, outList);
    //  executeMasterPrimitiveSync(*rwfPrim);
    synchSendPrim(*rwfPrim);
    delete rwfPrim;
    out.clear();
    outList = getOutList();
    uint32_t *outBody = (uint32_t *) outList->getMsgBody(1);
    for (int i=0; i<outList->getMsgLength(1); i++) {
      out.push_back(outBody[i]);
    }
    deleteOutList();
  }

  void IblRodModule::writeFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &in) {
    int32_t* rwfData = new int32_t[sizeof(RwFifoIn)/sizeof(uint32_t) + nWords];
    RwFifoIn *RwFifo_In = (RwFifoIn*) rwfData;
    RwFifo_In->fifoId   = fifo;
    RwFifo_In->fRead    = RW_WRITE;
    RwFifo_In->length    = nWords;
    RwFifo_In->dataPtr   = (uint32_t)NULL;

    int i;
    for (i=0; i<nWords; i++) rwfData[sizeof(RwFifoIn)/sizeof(uint32_t)+i] = in[i];
    RodPrimitive* rwfPrim = new RodPrimitive(sizeof(RwFifoIn)/sizeof(uint32_t) + nWords,
					     0, RW_FIFO, R_BOGUS, rwfData);
    executeMasterPrimitiveSync(*rwfPrim);
    delete rwfPrim;
    delete[] rwfData;
  }

#if 1
  void IblRodModule::writeRegister(int regId, int offset, int size, unsigned int value) {
    RwRegFieldIn 	RwRegField_In;
    RwRegField_In.regAddr    = regId;
    RwRegField_In.offset     = offset;
    RwRegField_In.width      = size;
    RwRegField_In.flags      = RW_WRITE;
    RwRegField_In.value      = value;
    RodPrimitive* rwrPrim = new RodPrimitive(sizeof(RwRegFieldIn)/sizeof(uint32_t),
					     0, RW_REG_FIELD, R_BOGUS,
					     (int32_t*)&RwRegField_In);
    //schsu: There was a 100ms sleep between RodPrimList::EXECUTING and RodPrimList::WAITING
    //It slows down the register write access.
    //executeMasterPrimitiveSync(*rwrPrim);

    //Since RW_WRITE primitive has relative smaller size.
    //No oberservation of failure by removing the 100ms sleep.
    executeMasterPrimitive(*rwrPrim);

    RodPrimList::PrimState returnPState;
    int count = 0;
    clock_t startTime;

    startTime = time(0);
    count = 0;
    do {
      if ((time(0)-startTime)>50 && count>20) {
	throw RodException("Cannot start primitive execution, TIMEOUT=50", m_slot);
      }
      returnPState = primHandler();
      count++;
    } while (returnPState != RodPrimList::EXECUTING);

    startTime = time(0);
    count = 0;
    do {
      if ((time(0)-startTime)>100 && count>20) {
	throw RodException("Primitive execution did not complete, TIMEOUT=100", m_slot);
      }
      returnPState = primHandler();
      count++;
    } while ((returnPState != RodPrimList::WAITING)&&(returnPState != RodPrimList::IDLE));

    if (returnPState == RodPrimList::WAITING) {
      getOutList();
      deleteOutList();
    }

    delete rwrPrim;
  }

  unsigned int IblRodModule::readRegister(int regId, int offset, int size) {
    RwRegFieldIn 	RwRegField_In;
    RwRegField_In.regAddr    = regId;
    RwRegField_In.offset     = offset;
    RwRegField_In.width      = size;
    RwRegField_In.flags      = RW_READ;
    RwRegField_In.value      = (uint32_t)NULL;
    RodPrimitive* rwrPrim = new RodPrimitive(sizeof(RwRegFieldIn)/sizeof(uint32_t),
					     0, RW_REG_FIELD, R_BOGUS,
					     (int32_t*)&RwRegField_In);
    //std::vector<unsigned int> outs;
    RodOutList* outs;
    executeMasterPrimitiveSync(*rwrPrim, outs);
    delete rwrPrim;
    RwRegFieldOut *reply = (RwRegFieldOut *)outs->getMsgBody(1);
    return reply->value;
  }
#endif


#if 1
  void IblRodModule::initStatusRead() {

    StartTaskIn *startTaskIn = (StartTaskIn*) new uint32_t[((sizeof(StartTaskIn))/sizeof(uint32_t))];

    startTaskIn->id = STATUS_READ;
    startTaskIn->idMinor = 0;
    startTaskIn->where   = DSP_THIS;
    startTaskIn->dataPtr = 0;

    RodPrimitive* startStatusRead=nullptr;
    try {
      startStatusRead = new RodPrimitive(((sizeof(StartTaskIn))/sizeof(uint32_t)), 0, START_TASK, 0, (int32_t*)startTaskIn);
    }
    catch (const std::bad_alloc&) {
      std::cout << "Unable to allocate startStatusRead primitive in main." << std::endl;
    }
    executeMasterPrimitiveSync(*startStatusRead);
    delete startStatusRead;
    delete []startTaskIn;

  }
#endif
  QuickStatusInfo* IblRodModule::talkStatusRead() {

    TalkTaskIn *talkTaskIn = (TalkTaskIn*) new uint32_t[((sizeof(TalkTaskIn))/sizeof(uint32_t))];

    talkTaskIn->task = STATUS_READ;
    talkTaskIn->topic = 0;
    talkTaskIn->item = 0;

    RodPrimitive* talkStatusRead=nullptr;
    try {
      talkStatusRead = new RodPrimitive(((sizeof(talkTaskIn))/sizeof(uint32_t)), 0, TALK_TASK, 0, (int32_t*)talkTaskIn);
    }
    catch (const std::bad_alloc&) {
      std::cout << "Unable to allocate talkStatusRead primitive in main." << std::endl;
    }

    RodOutList* statusInfoAddress;

    sleep(5);

    executeMasterPrimitiveSync(*talkStatusRead,statusInfoAddress);
    uint32_t* messageBody = (uint32_t*)statusInfoAddress->getMsgBody(1);
    TalkTaskOut *tout = (TalkTaskOut *)messageBody;
    uint32_t address = 0;
    if(0==tout->dataPtr){
      address = messageBody[sizeof(TalkTaskOut)/sizeof(uint32_t)];
    }else{
      address = tout->dataPtr;
    }
    printf("******Address of statusInfo struct: %x \n", address);

    uint32_t numWords = 60;

    uint32_t * buffer = new uint32_t[numWords];
    mdspBlockRead(address, buffer, numWords);
    QuickStatusInfo* quickStatusInfo = (QuickStatusInfo*)buffer;

    delete talkStatusRead;
    delete []talkTaskIn;

    return quickStatusInfo;

  }

  bool IblRodModule::sendCommand(Command &cmd) {
    //try to connect to server, if not, throw exception                                                                                                 
    if (!m_server->connectToServer(m_ipAddress.c_str(), m_srvIpPort)){
      //      throw RodException("Could not connect to server to talk to PPC");
      return false;
    }
    else{
      std::cout << "Connected to server." << std::endl;
    }
   
    m_server->sendCommand(cmd);
    m_server->disconnectFromServer();

    return true;
  }
} //  End namespace SctPixelRod

//------------------------- Overload insertion operators "<<" -------------------------
/* This overloaded operator lets us use cout to print the status of the ROD
 */
std::ostream& operator<<(std::ostream& os, SctPixelRod::IblRodModule& rod) {
  os << "Slot: " << rod.getSlot() << std::endl;
  os << "Serial Number(hex):" << std::hex << rod.getSerialNumber() << std::endl;
  os << "Number of slave DSPs: " << rod.getNumSlaves() << std::endl;

  os << "Primitive state: " << rod.getPrimState()
     << std::dec << std::endl;
  return os;
}


