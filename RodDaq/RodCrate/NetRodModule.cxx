//File: NetRodModule.cxx
// $Header$

#define R_BOGUS 0xFFFF

#include "NetRodModule.h"
#include <sys/time.h>
#include "BocCard.h"
#include <iostream>
#include <string.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

// AKU
void dumpXface(MasterXface *mxf);

//#ifdef IBL
#define NUM_SLAVES N_IBL_SLAVES
#include "iblSlaveCmds.h"
#include "iblSysParams.h"
#include <stdint.h>
int crc32(char *p, int nr, uint32_t *cval);
//#else
//#define NUM_SLAVES N_SLAVES
//#endif

//#define VERBOSE

//! Namespace for the common routines for SCT and PIXEL ROD software.
namespace SctPixelRod {

// Flag to force endian-ness swap before calls to HPI registers.  This should be
// a temporary patch.

static int32_t endianSwap = 0;
static const int32_t rodInitTimeout = 10;

NetRodModule::NetRodModule( uint32_t baseAddr, uint32_t mapSize,
			    VmeInterface & ourInterface, int32_t numSlaves)
     :
     VmeModule(baseAddr, mapSize, ourInterface) {
  m_slot = baseAddr>>24;
  m_serialNumber = 0xFFFFFFFF;  // Will be overwritten during init
  m_revision = 0;               // Will be overwritten during init
  m_numSlaves = numSlaves;
  m_finBufferSize = 4096;
  m_masterImageName = std::string("");
  m_myOutList = 0;
  m_vmeCommandReg = 0;
  m_rodStatusReg[0] = 0;
  m_rodStatusReg[1] = 0;
  m_debug = 0;
  m_debugFile = "rodprimitives.bin";
  m_textData = 0;
  m_btFailure = false;

  try {
      m_myVmePort = new VmePort(m_baseAddress, m_mapSize, VmeInterface::A32,
				m_ourInterface);
  }
  catch (std::bad_alloc & ba) {
    throw RodException("Failed to get VME Port.");
  }

  m_myOutList = 0;

  memset(&m_masterXface, 0, sizeof(m_masterXface));

  m_myPrimState = RodPrimList::IDLE;

  // Make the vme library generate exceptions on bus errors
  getVmePort()->setExceptionTrapping(true);

  m_myBoc = NULL;
  // AKU
  std::cout << "creating of BOC supressed" << std::endl;
  /*
  if (bocCheck()==1) {
    m_myBoc = new BocCard(*this);
  }
   */
}

//---------------------------------Destructor------------------------------
/*
Be sure to delete all VME Ports and other "new" things in constructor.
*/

NetRodModule::~NetRodModule() {
  delete(m_myVmePort); m_myVmePort = 0;
  // AKU if (m_myBoc != NULL) delete m_myBoc;

  if (m_textData){
      for (unsigned int i=0; i< n_textbuffers; i++) {
          if (m_textData[i]) delete [](m_textData[i]);
      }
      delete []m_textData;
      m_textData=NULL;
  }
  return;
}

//  Member methods

//--------------------------------initialize---------------------------------

/* This method performs all actions necessary to place a properly loaded ROD
in its initialized state, except for initializing the slave DSPs. This must be done
separately using initSlaveDsp().

Activities:
    Reset the ROD
    Read ROD serial number
    Load 0x000l000l into Master DSP HPIC register. (Says low order 16-bit word
                                                    comes first)
    Read back HPIC to verify
    Load MDSP memory map
    Retrieve pointers to text buffer structs
    Initialize PrimList and TextBuff state variables
*/
void NetRodModule::initialize() {initialize(false); return;}

void NetRodModule::initialize(bool resetFlag) {

  //uint32_t hpicReadback;   // for debugging
  uint32_t mdspStart;


  std::cout << "NetRodModule::initialize" << std::endl;


  int i_count = 0;
  bool i_done = false;
  while(!i_done) {
    i_count++;
    // If reset flag is set reset the ROD so it starts in a known state
    if (resetFlag) {
      int r_count = 0;
      bool r_done = false;
      while (!r_done) {
	try {
	  r_count++;
	  reset();
	  r_done = true;
	}
	catch (RodException &r) {
	  if (r_count > 2) {
	    throw r;
	  }
	  std::cout << "S" << m_slot << " retry ROD reset (count = " << r_count << ")" << std::endl;
	}
      }
      // Wait an extra half a second
      sleep(500);
    }

    // Read ROD serial number
    m_serialNumber = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
    if (endianSwap) m_serialNumber = endianReverse32(m_serialNumber); // Temporary (I hope)
    m_revision = (m_serialNumber&0x00ff0000)>>16;
    m_serialNumber = m_serialNumber & 0x3ff;   // Mask to get 10 least significant bits

    //	Initialize Master HPIC register to set half-word order


    mdspStart = mdspSingleRead(MEMORYMAP_REG);
    std::cout << "S" << m_slot << " reading MEMORYMAP_REG: " << std::hex << MEMORYMAP_REG
	      << " start " << std::hex << mdspStart << std::endl;

    // read interface structure
    mdspBlockRead(mdspStart, (uint32_t*)&m_masterXface,
		  sizeof(MasterXface)/4);

#ifdef VERBOSE
    dumpXface(&m_masterXface); //AKU
#endif

    if (m_masterXface.xface.nTextBuffers > 10) {
      if (i_count > 2) {
	throw RodException("Invalid number of text buffers returned by the ROD, aborting", m_slot);
      } else {
	resetFlag = true;
	std::cout << "S" << m_slot << " retry ROD initialize (count = " << i_count << ")" << std::endl;
        continue;
      }
    }
#ifdef VERBOSE
    std::cout << "S" << m_slot << " nTextBuffers: " << m_masterXface.xface.nTextBuffers
	      << std::endl;
    std::cout << "S" << m_slot << " nTextBufferSlots: " << m_masterXface.xface.nTextBufferSlots
	      << std::endl;
#endif

    // number of textbuffers per master and 4 slaves
    n_textbuffers = m_masterXface.xface.nTextBuffers*m_masterXface.xface.nTextBufferSlots;
    std::cout << "Total number of text buffers: " << std::dec << n_textbuffers << std::endl; // AKU

    m_textBuffer     = new uint32_t[n_textbuffers];
    m_textBufferSize = new uint32_t[n_textbuffers];

    // read pointers to text buffer queue structures
    mdspBlockRead(m_masterXface.xface.textBuffer,
		  (uint32_t*)m_textBuffer,
		  n_textbuffers);

    // AKU
    /*
    for (int i=0;i<n_textbuffers;i++){
        std::cout << "Text buffer " << i << ": " << m_textBuffer[i] << std::endl;
     }*/

    TextQueue textQueue;
    if(m_textData){
      for (unsigned int i=0; i< n_textbuffers; i++) {
	if (m_textData[i]) delete [](m_textData[i]);
      }
      delete []m_textData;
      m_textData=NULL;
    }
    m_textData = new char*[n_textbuffers];
    for (unsigned int i=0; i < n_textbuffers; i++) m_textData[i] = 0;

    for (unsigned int i=0; i< n_textbuffers; i++) {
      mdspBlockRead(m_textBuffer[i], (uint32_t*)&textQueue,
		    sizeof(TextQueue)/4);
      m_textBufferSize[i] = textQueue.length;
      m_textData[i] = new char[sizeof(uint32_t)*m_textBufferSize[i]];
#ifdef VERBOSE
      std::cout << "S" << m_slot << " RodModule TextBuffer " << i << " size: " << m_textBufferSize[i] << std::endl;
#endif
    }
    i_done = true;
  }

  //  Reinitialize state variables
  m_myPrimState = RodPrimList::IDLE;

  std::cout << "S" << m_slot << " NetRodModule::initialized finished" << std::endl;
return;
}

bool NetRodModule::isSane() {
  bool sane = false;

  uint32_t fpgaStatus6 = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
  uint32_t maskedCor   = fpgaStatus6 & 0xff000000; // correct byte order
  uint32_t maskedRev   = fpgaStatus6 & 0x000000ff; // wrong byte order
  sane = ((maskedCor == 0xad000000) || (maskedRev == 0x000000ad));

  return sane;
}

uint32_t NetRodModule::fmtVersion(){
  uint32_t version = mdspSingleRead(0x00400088);
  return ((version & 0xFFF)<<4) + ((version & 0xf00000)>>20);
}

uint32_t NetRodModule::efbVersion(){
  uint32_t version = mdspSingleRead(0x0040221C);
  return (version & 0x00008FFF);
}

uint32_t NetRodModule::rtrVersion(){
  uint32_t version = mdspSingleRead(0x0040250C);
  return (version & 0x00008FFF);
}

uint32_t NetRodModule::rcfVersion(){
  uint32_t version = mdspSingleRead(0x0040440C);
  return (version & 0x00000FFF);
}


//---------------------------------reset------------------------------------
/* */
/* This method resets the ROD. */
/* */

void NetRodModule::reset() {
  // Reset all FPGAs and DSPs (bit 6 = 0x40)
  const uint32_t rodResetValue = 0x00000040;
  uint32_t rodReset;
  uint32_t rodRegValue, dspRegValue;
  clock_t startTime;
  unsigned int count;

  std::cout << "S" << m_slot << " NetRodModule::reset " << std::endl;
  if (endianSwap) {
    rodReset = endianReverse32(rodResetValue);
  }
  else {
    rodReset = rodResetValue;
  }

  // mark MDSP as in unknown state
  mdspSingleWrite(MAGIC_LOCATION, 0xDEADDEAD);
  std::cout << "S" << m_slot << " NetRodModule::reset MAGIC_LOCATION " << std::hex
       << MAGIC_LOCATION << std::endl;

 // reset all
  m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);
  sleep(1000);

  std::cout << "S" << m_slot << " NetRodModule::reset RESET" << std::endl;
  // check  for finish of FPGA reset
  startTime = time(0);
  count = 0;
  do {
    rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[0]);
    std::cout << "reset status: 0x" << std::hex << rodRegValue << std::endl;
    if ((time(0)-startTime) > 5 && count > 10) {
      throw RodException("FPGA reset timeout in reset(), DSP_RESET_TIMEOUT=5", m_slot);
    }
    count++;

    // Check that reset has finished
    // leave if PRM configuration mode active
  } while (((rodRegValue&0x3f) != FPGA_RESET_COMPLETE) &&
	   ((rodRegValue&0x20) != 0x20));

  // check  for finish of DSP reset
  rodRegValue = 0;
  startTime = time(0);
  count = 0;
  do {
    rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
    if ((time(0)-startTime) > 5 && count > 10) {
      throw RodException("DSP reset timeout in reset(), DSP_RESET_TIMEOUT=5", m_slot);
    }
    count++;
  }
  while (rodRegValue != ALLDSP_RESET_COMPLETE);

 // wait for MDSP boot indication
  startTime = time(0);
  count = 0;
  do {
    dspRegValue = mdspSingleRead(MAGIC_LOCATION);
    if ((time(0)-startTime) > 15 && count > 10) {
      throw RodException("MDSP boot timeout in reset(), DSP_RESET_TIMEOUT=15", m_slot);
    }
    count++;
  } while(dspRegValue != I_AM_ALIVE);

  //  Reinitialize state variables
  m_myPrimState = RodPrimList::IDLE;

return;
}

//---------------------------------fpgaReset------------------------------------

/*! This method resets individual FPGAs on the ROD. The ROD can reset only
    one FPGA or else all FPGAs at a time.
 */
void NetRodModule::fpgaReset(FpgaType targetFpga) {
  /* Reset one FPGA via FPGA Reset Control Register (0xC00004)
     The values for the targetFpga are set in an enum called FpgaType. The values are:
       0 = Controller
       1 = Formatter
       2 = Formatter
       3 = EFB
       4 = Router
       5 = All
  */
  uint32_t resetValue;
  uint32_t reset;
  uint32_t rodRegValue;
  clock_t startTime;

if ((targetFpga != 0) && (targetFpga != 1) && (targetFpga != 2) && (targetFpga != 3) &&
      (targetFpga != 4) && (targetFpga != 5)) {
    std::cout << "Invalid argument to fpgaReset. Only one bit can be set." << std::endl;
    return;
  }
  resetValue = 0x00000001<<targetFpga;
  std::cout << "NetRodModule::fpgaReset " << std::endl;
  if (endianSwap) {
    reset = endianReverse32(resetValue);
  }
  else {
    reset = resetValue;
  }
	std::cout << "Set the reset bit (it is self-clearing)" << std::endl;
  // Set the reset bit (it is self-clearing)
  m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[1], reset);

	std::cout << "check  for finish of FPGA reset." << std::endl;
  // check  for finish of FPGA reset. The FPGA Reset Status Register is at VME 0xc00024.
  startTime = time(0);
  do {
    rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[1]);
    if ((time(0)-startTime) > DSP_RESET_TIMEOUT) {
      throw RodException("FPGA reset timeout in reset(), DSP_RESET_TIMEOUT=",
			 DSP_RESET_TIMEOUT);
    }

  // leave if PRM configuration mode active (check bit 5 in FPGA Configuration Status
  // Register at 0xc00020.
  } while (((rodRegValue&0x3f) != FPGA_RESET_COMPLETE) &&
	   ((rodRegValue&0x20) != 0x20));

	std::cout << "Reinitialize state variables" << std::endl;
  //  Reinitialize state variables
  m_myPrimState = RodPrimList::IDLE;

return;
}

//---------------------------------dspReset------------------------------------

/*! This method resets individual DSPs on the ROD. The ROD can reset only one DSP
    or else all DSPs at a time.
 */
void NetRodModule::dspReset(DspType targetDsp) {
  /* Reset one DSP via DSP Reset Control Register (0xC00008)
     The values for the targetDsp are set in an enum called DspType. The values are::
       1 = Master
       2 = Slave 0
       3 = Slave 1
       4 = Slave 2
       5 = Slave 3
  */
  uint32_t resetValue;
  uint32_t reset;
  uint32_t rodRegValue;
  clock_t startTime;

  if ((targetDsp != 0) && (targetDsp != 1) && (targetDsp != 2) && (targetDsp != 3) &&
      (targetDsp != 4) && (targetDsp != 5)) {
    std::cout << "Invalid argument to dspReset. Only one bit can be set." << std::endl;
    return;
  }
  resetValue = 0x00000001<<targetDsp;
  std::cout << "NetRodModule::fpgaReset " << std::endl;
  if (endianSwap) {
    reset = endianReverse32(resetValue);
  }
  else {
    reset = resetValue;
  }

  // Set the reset bit (it is self-clearing)
  m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], reset);

  // check  for finish of DSP reset.  The DSP Reset Status Register is at VME 0xc00028.
  rodRegValue = 0;
  startTime = clock();
  do {
    rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
    if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
      throw RodException("DSP reset timeout in reset(), DSP_RESET_TIMEOUT=",
      (int32_t)DSP_RESET_TIMEOUT);
    }
  }
  while (rodRegValue != ALLDSP_RESET_COMPLETE);

  //  Reinitialize state variables
  m_myPrimState = RodPrimList::IDLE;

return;
}

//----------------------------------verify----------------------------------

/* This method checks the value of the h.o. byte of the
*/

bool NetRodModule::verify() {
  uint32_t regVal;

// Read status register 6
  regVal = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
  if (endianSwap) regVal = endianReverse32(regVal);
  regVal = regVal & 0xff000000;            // Mask to get most significant byte
  if (regVal!=0xad000000) return false;
  return true;
}

//---------------------------------status-----------------------------------

/* This method reports the status of the ROD.
For now, it simply prints to standard output.
*/
void NetRodModule::status() {
    std::cout << "Slot: "                   << m_slot;
    std::cout << "; Serial Number:"         << m_serialNumber;
    std::cout << "; Number of slave DSPs: " << m_numSlaves;
    std::cout << std::endl;

    std::cout << std::endl;

    std::dec(std::cout);
    std::cout << "Primitive state: " << getPrimState() ;
    std::cout << std::endl;
    return;
}




// AKU: version for spartan-6 slaves. processes only one slave, indexed by number, not mask!
int NetRodModule::loadAndStartFpgaSlaves(const std::string & fileName, const int slaveId)
   {
  int nWords;//, islaveMask;
  uint32_t size, offs;
  FILE *fp;

/* first initialize the dsp after the hardware reset */
  SlaveCtrlIn slaveCtrlInI ;
  slaveCtrlInI.flag = SLAVE_INIT;
  slaveCtrlInI.slaveMask = slaveId;

  RodPrimitive *slvRWPrimI = new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t),
					      1, SLAVE_CTRL, R_BOGUS, (int32_t*)&slaveCtrlInI);
  executeMasterPrimitiveSync(*slvRWPrimI);
  delete slvRWPrimI;

	// AKU: test for NULL file
	if (fileName.empty()){
		throw NoImageFile("Missing file name thrown from loadAndStartSlaves", fileName);
		return -1; /* no such file */
	}

	/* the largest primitive we can send */
	if((fp = fopen(fileName.c_str(), "r1")) == NULL) {
		throw NoImageFile("NoImageFile thrown from loadAndStartSlaves", fileName);
		return -1; /* no such file */
	}
	/* load the code into the slaves */

	// AKU: the file is raw binary . no dld headers etc
	struct stat fileStat;
	fstat(fileno(fp), &fileStat);
	size = fileStat.st_size;
	if (0 == size) {
		throw NoImageFile("No data in ImageFile", fileName);
		fclose(fp);
		return -1; /* bad file */
	}

	// read and load file in portions
	const uint32_t maxBlkSize = 1024; // must be power of 2
	uint32_t accMax = m_masterXface.xface.requestChannelLen[0] * sizeof(uint32_t); // jsv words or bytes
	uint32_t blkSize = maxBlkSize < accMax ? maxBlkSize : accMax;
	static unsigned char fBuf[sizeof(SlaveCtrlIn) + sizeof(DldHeader) + maxBlkSize];
	SlaveCtrlIn *svlCtrlIn = (SlaveCtrlIn*)&fBuf[0];
	DldHeader *dldHeader = (DldHeader*)&svlCtrlIn[1];
	uint32_t *data = (uint32_t*)&dldHeader[1];
	char *fileBuf;
	int bsize;
    uint32_t crcVal1, crcVal2;

    // allocate buffer
	if (0 == (fileBuf = (char*)malloc(size + 3))){  // allocate a bit more
        throw RodException("Failed to allocate file buffer");
	}
	// clear area
	memset(fileBuf,0,sizeof(fileBuf));
	// read file
	if (1 != fread(fileBuf, size, 1, fp)){
        throw RodException("Failed to read slave file");
	}
	fclose(fp);
	// we have the full content, so compute local crc
	crc32(fileBuf,size,&crcVal1);
	std::cout << "Local crc32: 0x" << std::hex << crcVal1 << std::endl;

    // download in portions
	std::cout << "Loading file ";
	offs = 0;
	while (offs < size){
		//bsize = fread(data, sizeof(uint32_t), blkSize/sizeof(uint32_t), fp);
		bsize = size - offs > blkSize ? blkSize : size - offs;
		memcpy(data, fileBuf + offs, bsize);
		dldHeader->length = bsize / sizeof(uint32_t);
		//dldHeader->length = bsize; // size - offs > blkSize ? blkSize : size - offs;	// set size: max is blkSize
		//dldHeader->length = (dldHeader->length + 3) / sizeof(uint32_t);	// must be in words, rounded up
		dldHeader->addr = offs;
		dldHeader->fInternal = 0;
		svlCtrlIn->flag      = LOAD_COFF;
		svlCtrlIn->slaveMask = slaveId; // AKU: this is now a number. mask is not used any more
		svlCtrlIn->nSections = 1; // only 1 section always

		// nWords = (dldHeader->length + sizeof(DldHeader) + sizeof(SlaveCtrlIn)) / sizeof(int32_t); //  + 3 ????
		nWords = dldHeader->length + (sizeof(DldHeader) + sizeof(SlaveCtrlIn)) / sizeof(int32_t);

		RodPrimitive *slvCtrlPrimL =
		  new RodPrimitive(nWords, 1, SLAVE_CTRL, R_BOGUS, (int32_t *)svlCtrlIn);

       	executeMasterPrimitiveSync(*slvCtrlPrimL);
        delete slvCtrlPrimL;

		offs += dldHeader->length * sizeof(uint32_t);
        std::cout << "." << std::flush;
	}
    std::cout << std::endl;
	std::cout << "File loaded" << std::endl;

    /* check crc */

    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    RodOutList* outList;
    uint32_t *outBody;
    SendSlavePrimitiveIn *slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slaveId; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    IblSlvRdWr *slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_CRC;
    slvCmd->addr = 0;
    slvCmd->count = 1;
    slvCmd->data = size/sizeof(int32_t);
    RodPrimitive *sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (int32_t*)slvCmdPrim);
    executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    outBody = (uint32_t *) outList->getMsgBody(1);
    crcVal2 = outBody[1];
	std::cout << "Slave CRC: " << std::hex << crcVal2 << std::endl;
    deleteOutList();
    delete sendSlvCmdPrim;

    if (crcVal1 != crcVal2){
        throw RodException("CRC mismatch");
    }
	std::cout << "CRC check OK" << std::endl;

	return startSlave(slaveId);

}

// AKU: version for spartan-6 slaves. processes only one slave, indexed by number, not mask!
// Karolos: Purpose is to have separate functionality in separate functions
int NetRodModule::startSlave(const int slaveId) {

	/* start */

	SlaveCtrlIn slaveCtrlInS;
	slaveCtrlInS.flag      = LOAD_DONE;
	slaveCtrlInS.slaveMask = slaveId;  // AKU: this is now a number. mask is not used any more
	RodPrimitive *slvRWPrimS =
	new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t), 1,
		 SLAVE_CTRL, R_BOGUS, (int32_t *)&slaveCtrlInS);
	std::cout << "Starting slave" << std::endl;
	executeMasterPrimitiveSync(*slvRWPrimS);
	delete slvRWPrimS;

	/* wait for boot */

	SlaveCtrlIn slaveCtrlInB;
	slaveCtrlInB.flag      = WAIT_BOOT;
	slaveCtrlInB.slaveMask = slaveId;
	RodPrimitive *slvRWPrimB =
	new RodPrimitive(sizeof(SlaveCtrlIn)/sizeof(uint32_t), 1,
		 SLAVE_CTRL, R_BOGUS, (int32_t *)&slaveCtrlInB);
	std::cout << "Waiting for slave boot" << std::endl;
	executeMasterPrimitiveSync(*slvRWPrimB);
	delete slvRWPrimB;

	return 0;
}


//------------------------------loadSlaveImage-----------------------------
void NetRodModule::loadSlaveImage(const std::string & fileName, const uint32_t address,
				  const int32_t slaveNum, char opt)  {


  int32_t numWords;
  uint32_t readBack;
  int fileSize, i;
  const uint32_t DSP_BUSY_MASK=0x00000002;
  std::ifstream fin;
// Cannot load image if slave is running.  Check it and throw a rodException if it is
  readBack = slvHpiFetch(SLAVE_HPIC_BASE, slaveNum);
  if (readBack & DSP_BUSY_MASK) throw RodException(
     "loadSlaveImage tried to load to a running SDSP. slavenum = ", slaveNum);

  fin.open(fileName.c_str(),std::ios::binary);
  if (fin.is_open()) {

// Get size of file
    fin.seekg(0, std::ios::end);  // go to end of file
    fileSize = fin.tellg();  // file size is current location
    fin.seekg(0, std::ios::beg);  // go back to beginning of file

// Create buffer and fill it with file contents
    char * buffer=nullptr;
    try {
      buffer = new char[fileSize];
    }
    catch (std::bad_alloc & ba) {
      throw RodException(
                  "primLoadSlaveImage buffer failed to allocate. buffer, fileSize:",
                  (unsigned long)buffer, (unsigned long)fileSize);
    }
    fin.read(buffer, fileSize);

// Use RW_SLAVE_MEMORY to transfer buffer into slave DSP memory
    int32_t* writeSlvBuff;
    numWords = (fileSize+3)/sizeof(uint32_t);
    writeSlvBuff = new int32_t[numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)];
    RwSlaveMemoryIn* WSlaveMemoryIn = (RwSlaveMemoryIn*)writeSlvBuff;
    WSlaveMemoryIn->slave         = slaveNum;
    WSlaveMemoryIn->flags         = RW_WRITE;
    WSlaveMemoryIn->slaveAddr     = (uint32_t)address;
    WSlaveMemoryIn->masterAddr    = (uint32_t)NULL;
    WSlaveMemoryIn->nWords        = numWords;
    int32_t tempWord;
    for (i = 0; i< numWords; i++) {
      tempWord = 0;
      tempWord = tempWord | (buffer[(i)*4+3]&0xff)<<24;
      tempWord = tempWord | (buffer[(i)*4+2]&0xff)<<16;
      tempWord = tempWord | (buffer[(i)*4+1]&0xff)<<8;
      tempWord = tempWord | (buffer[(i)*4]&0xff);
      writeSlvBuff[i + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)] = tempWord;
    }
    RodPrimitive* slvRWPrim;
    slvRWPrim = new RodPrimitive(numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
				 1, RW_SLAVE_MEMORY, R_BOGUS, writeSlvBuff);
    synchSendPrim(*slvRWPrim);
    delete [] writeSlvBuff;
    delete slvRWPrim;

// Retrieve output buffer, if there is one
    RodOutList* outList = getOutList();
    if (outList) {
/*      uint32_t outListLength = uint32_t(outList->getLength());
      uint32_t* outBody = outList->getBody();
      uint32_t outLength       = uint32_t(outBody[0]);
      uint32_t outIndex        = uint32_t(outBody[1]);
      uint32_t outNumPrims     = outBody[2];
      uint32_t outPrimVersion  = outBody[3];
      uint32_t primLength      = outBody[4];
      uint32_t primIndex       = outBody[5];
      uint32_t primId          = outBody[6];
      uint32_t primVersion     = outBody[7];
*/
      deleteOutList();
    }


// Read back buffer for verification if 'v' option was given
    if (opt=='v') {
      char* buffer2;
      try {
        buffer2 = new char[fileSize];
      }
      catch (std::bad_alloc & ba) {
        throw RodException(
                  "loadSlaveImage buffer2 failed to allocate. buffer, fileSize:",
                  (unsigned long)buffer, (unsigned long)fileSize);
      }
      int32_t * readSlvBuff;
      readSlvBuff = new int32_t[numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)];
      RwSlaveMemoryIn* RSlaveMemoryIn  = (RwSlaveMemoryIn*)readSlvBuff;
      RSlaveMemoryIn->slave      = slaveNum;
      RSlaveMemoryIn->flags      = RW_READ;
      RSlaveMemoryIn->slaveAddr  = (uint32_t)address;
      RSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
      RSlaveMemoryIn->nWords     = numWords;
      slvRWPrim = new RodPrimitive(numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
				   1, RW_SLAVE_MEMORY, R_BOGUS, readSlvBuff);
      synchSendPrim(*slvRWPrim);
      delete [] readSlvBuff;

// Retrieve output buffer - there should be only one primitive in the reply list
      RodOutList* outList = getOutList();
      if (outList) {
//        uint32_t outListLength = uint32_t(outList->getLength());
        uint32_t* outBody = outList->getBody();
        uint32_t outLength = uint32_t(outBody[0]);
//        uint32_t outIndex = uint32_t(outBody[1]);
//        uint32_t outNumPrims = outBody[2];
//        uint32_t outPrimVersion = outBody[3];
//        uint32_t primLength = outBody[4];
//        uint32_t primIndex = outBody[5];
//        uint32_t primId = outBody[6];
//        uint32_t primVersion = outBody[7];
        int fileSize2 = (outLength-15)*4;
        if (fileSize2 != fileSize) throw RodException("primLoadSlaveImage read back wrong file size; expected, got =",
          fileSize, fileSize2);
        char tempChar0, tempChar1, tempChar2, tempChar3;
        for (int i=0; i<((fileSize+3)/4); i++) {
          tempWord = outBody[i+8];
          tempChar0 = tempWord & 0x000000ff;
          tempChar1 = (tempWord & 0x0000ff00)>>8;
          tempChar2 = (tempWord & 0x00ff0000)>>16;
          tempChar3 = (tempWord & 0xff000000)>>24;
          buffer2[4*i] = tempChar0;
          buffer2[4*i+1] = tempChar1;
          buffer2[4*i+2] = tempChar2;
          buffer2[4*i+3] = tempChar3;
        }
        for (int i=0; i<fileSize; i++) {
          if (buffer[i] != buffer2[i]) {
            deleteOutList();
            delete [] buffer;
            delete [] buffer2;
            delete slvRWPrim;
            fin.close();
            char message[] = "primLoadSlaveImage buffer readback invalid for file, slavenum, char index: ";
            throw RodException( message+fileName, slaveNum, i);
          }
        }
        deleteOutList();
        delete [] buffer2;
        delete slvRWPrim;
        std::cout << "Image verification check complete\n";
      }
    }
// Close the file and delete the buffer
    fin.close();
    delete [] buffer;
  }
  else {
    throw NoImageFile("NoImageFile thrown from primLoadSlaveImage", fileName);
  }
  return;
}

//--------------------------------hpiLoadSlaveImage-------------------------------
void NetRodModule::hpiLoadSlaveImage(const std::string & fileName, const uint32_t address,
				     const int32_t slaveNum, char opt) {


  int32_t numWords;
  uint32_t readBack;
  int fileSize;
  const uint32_t DSP_BUSY_MASK=0x00000002;
  std::ifstream fin;
// Cannot load image if slave is running.  Check it and throw a rodException if it is
  readBack = slvHpiFetch(SLAVE_HPIC_BASE, slaveNum);
  if (readBack & DSP_BUSY_MASK) throw RodException(
     "loadSlaveImage tried to load to a running SDSP. slavenum = ", slaveNum);

  fin.open(fileName.c_str(),std::ios::binary);
  if (fin.is_open()) {

// Get size of file
    fin.seekg(0, std::ios::end);  // go to end of file
    fileSize = fin.tellg();  // file size is current location
    fin.seekg(0, std::ios::beg);  // go back to beginning of file

// Create buffer and fill it with file contents
    char * buffer;
    try {
      buffer = new char[fileSize];
    }
    catch (std::bad_alloc & ba) {
      throw RodException(
                  "loadSlaveImage buffer failed to allocate. buffer, fileSize:",
                  (unsigned long)buffer, (unsigned long)fileSize);
    }
    fin.read(buffer, fileSize);

// Use slvHpiBlockWrite to transfer buffer into memory
    numWords = (fileSize+3)/4;
    slvHpiBlockWrite(address, (uint32_t*) buffer, numWords, slaveNum);

// Read back buffer for verification if 'v' option was given
    if (opt=='v') {
      char* buffer2;
      try {
        buffer2 = new char[fileSize];
      }
      catch (std::bad_alloc & ba) {
        throw RodException(
                  "HpiLoadSlaveImage buffer2 failed to allocate. buffer, fileSize:",
                  (unsigned long)buffer, (unsigned long)fileSize);
      }
      slvHpiBlockRead(address, (uint32_t*)buffer2, numWords, slaveNum);

      for (int i=0; i<fileSize-1; i++) {
        if (buffer[i] != buffer2[i]) {
          delete [] buffer;
          delete [] buffer2;
          fin.close();
          char message[] = "HpiLoadSlaveImage buffer readback invalid for file, slavenum, char index: ";
          throw RodException( message+fileName, slaveNum, i);
        }
      }
      delete [] buffer2;
      std::cout << "Image verification check complete\n";
    }

// Close the file and delete the buffer
    fin.close();
    delete [] buffer;
  }
  else {
    throw NoImageFile("NoImageFile thrown from loadSlaveImage", fileName);
  }
  return;
}

/*---------------------------------synchSendPrim------------------------------
This version writes to cout. This needs to be changed.
*/
void NetRodModule::synchSendPrim(RodPrimitive & prim)  {
  RodPrimList primList(1);  // Private primList (local to this routine)
  RodPrimList::PrimState returnPState;

// Create an error text buffer in case of error message
    primList.insert(primList.begin(), prim);
    try {
      primList.bufferBuild();
    }
    catch (PrimListException &p) {
//      throw (RodException(p.getDescriptor,p.getData1(), p.getData2());
      std::cout << p.getDescriptor() << " ";
      std::cout << p.getData1() << ", " << p.getData2() << "\n";
    }
    try {
      this->sendPrimList(&primList);
    }
    catch (BaseException &h) {
      std::cout << h;
    }

    do {
       try {
         returnPState = this->primHandler();
       }
       catch (RodException &r) {
       std::cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
               << '\n';
       }
    } while (returnPState != RodPrimList::WAITING &&
	     returnPState != RodPrimList::IDLE);

  return;
}

//---------------------------------sendPrimList-------------------------------
void NetRodModule::sendPrimList(RodPrimList *plist) {

  //  char filename[256]; // AK
  FILE *fp; // AK
  //static int jsv_index2 = 0;   //AK
  uint32_t * buffAddr;
  uint32_t statusword;
  int32_t buffCount;
  bool timeout = false;
  clock_t startTime;
  unsigned int count = 0;

  if (!plist->getBuffer()) {
    plist->bufferBuild();
  }

  buffCount = plist->getBufferLength();


  if (((unsigned int)abs(buffCount)) > m_masterXface.xface.requestChannelLen[0]) {
    throw PrimListException("PrimList is bigger than DSP buffer", buffCount,
			    m_masterXface.xface.requestChannelLen[0]);
  }
  buffAddr = plist->getBuffer();

  startTime = clock();
  count = 0;
  // wait until previous primlist is finished
  startTime = time(0);
  do {
    statusword = mdspSingleRead(m_masterXface.xface.requestChannelStatus[0]);
    if ((time(0)-startTime) > 300 && count>20) timeout = true;
    count++;
  }while(0!=statusword && timeout == false);

  // write primitive into DSP buffer
  if(!timeout){
#ifdef VERBISE
    std::cout << "Send primitive S" << m_slot << std::hex << m_masterXface.xface.requestChannelLoc[0]
              << buffAddr << std::dec << " " << buffCount << std::endl;
#endif
    mdspBlockWrite(m_masterXface.xface.requestChannelLoc[0], buffAddr, buffCount);
    m_myPrimState = RodPrimList::LOADED;
  }else{
    throw PrimListException("PrimList Timeout, previous list still executing", 0, 0);
  }

  if(getDebug()){
    //AK
    //    sprintf(filename, "prim%2.2d.bin", jsv_index2++);
    fp = fopen(m_debugFile.c_str(), "ab");
    if(fp!=NULL){
      fwrite(buffAddr, buffCount, 4, fp);
      fclose(fp);
    }
    //AK
  }

  return;
}

//---------------------------------sendSlavePrimList---------------------
void NetRodModule::sendSlavePrimList(RodPrimList *slvPrimList, int slave)
{

  slvPrimList->bufferBuild();

  int32_t buffLength = slvPrimList->getBufferLength();
  uint32_t* slavePrimList = new uint32_t[buffLength];
  uint32_t* bufferStart = slvPrimList->getBuffer();
  for (int i=0; i< buffLength; i++) {
    slavePrimList[i] = bufferStart[i];
  }

  // Create Send Slave List primitive
  RodPrimList primList(1);
  int32_t *slaveData = new int32_t[sizeof(SendSlaveListIn)/sizeof(uint32_t) +
				     buffLength];
  SendSlaveListIn* SendSlaveList_In = (SendSlaveListIn*)slaveData;
  SendSlaveList_In->slave         = slave;
  SendSlaveList_In->nWords        = buffLength;
  SendSlaveList_In->queueIndex    = 0;

  for (int i=0; i< buffLength; i++) {
    slaveData[i+sizeof(SendSlaveListIn)/4] = slavePrimList[i];
  }
  delete slavePrimList;
  RodPrimitive* send;
  send = new RodPrimitive(sizeof(SendSlaveListIn)/sizeof(uint32_t) + buffLength,
			  0, SEND_SLAVE_LIST, R_BOGUS, slaveData);
  primList.insert(primList.begin(), *send);

  primList.bufferBuild();
  sendPrimList(&primList);
  delete slaveData;
}

//---------------------------validateReplyBuffer ----------------
int NetRodModule::validateReplyBuffer()  {
  ReplyListHead replyListHead;
  ReplyListTail replyListTail;
  ReplyHead     replyHead;
  uint32_t        curr_addr;

  curr_addr = m_masterXface.xface.requestChannelReplyBufferLoc[0];

  uint32_t mbuffer2[100];

  //AKDEBUG fix probelm reading SDSP data, first word corrupted
  mdspBlockRead(m_masterXface.xface.requestChannelReplyBufferLoc[0], &mbuffer2[0],18);

  //Debug
  // for(int i=0;i<3;i++)std::cout << std::hex << "Pre DATA: " << mbuffer2[i]<< std::dec << std::endl;


  mdspBlockRead(curr_addr, (uint32_t*)&replyListHead,
		sizeof(ReplyListHead)/sizeof(uint32_t));

  curr_addr += sizeof(ReplyListHead);

  //read replies for each primitive
  for (unsigned int i = 0; i< replyListHead.nMsgs; i++){
    mdspBlockRead(curr_addr, (uint32_t*)&replyHead,
		  sizeof(ReplyHead)/sizeof(uint32_t));
    //    curr_addr += replyHead.length*sizeof(uint32_t) + sizeof(ReplyHead);
    curr_addr += replyHead.length*sizeof(uint32_t) + sizeof(ReplyHead);
   if(curr_addr>(m_masterXface.xface.requestChannelReplyBufferLoc[0] +
		 m_masterXface.xface.requestChannelReplyBufferLen[0]*sizeof(uint32_t)))return -1;
  }
  mdspBlockRead(curr_addr, (uint32_t*)&replyListTail,
		sizeof(ReplyListTail)/sizeof(uint32_t));

  curr_addr += sizeof(ReplyListTail);
  //  uint32_t length = (curr_addr - m_masterXface.xface.requestChannelReplyBufferLoc[0]
  //		   - sizeof(ReplyListHead) - sizeof(ReplyListTail))/sizeof(uint32_t);
  //
  //Debug
  // std::cout << " expected length from Tail " << replyListTail.length
  //          << " from Head " << replyListHead.length
  //	      << " counted:  " << length << std::endl;

//   uint32_t mbuffer[1000];
//   mdspBlockRead(m_masterXface.xface.requestChannelReplyBufferLoc[0], &mbuffer[0],
// 		(curr_addr - m_masterXface.xface.requestChannelReplyBufferLoc[0])/sizeof(uint32_t));
//   for(int i=0;i<length;i++)std::cout << std::hex << "POST DATA" << mbuffer[i]<< std::dec << std::endl;


  //check consistency
  if(replyListHead.length==replyListTail.length){
    return replyListHead.length;
  }else{
    return -1;
  }

}

//---------------------------------primHandler-------------------------------

RodPrimList::PrimState NetRodModule::primHandler()  {
uint32_t status;
uint32_t command=0;

  switch (m_myPrimState) {
    case RodPrimList::LOADED: {
      try {
	status = mdspSingleRead(m_masterXface.xface.requestChannelStatus[0]);
      }
      catch (VmeException ex) {
	std::cout << "NetRodModule::primHandler: Warning : BUS error reading status reg"
		  << std::endl;
        break;
      }
      if (0==status) {
	command = 0;
	mdspSingleWrite(m_masterXface.xface.requestChannelControl[0], 1);
        command = mdspSingleRead(m_masterXface.xface.requestChannelControl[0]);
        m_myPrimState = RodPrimList::EXECUTING;
	// std::cout << "Control " << command << std::endl;
      }
    break;
    }
    case RodPrimList::EXECUTING: {
      try {
         status = mdspSingleRead(m_masterXface.xface.requestChannelStatus[0]);
      }
      catch (VmeException ex) {
	std::cout << "NetRodModule::primHandler: Warning : BUS error reading status reg"
		  << std::endl;
        break;
      }
      unsigned int nDone  = status & 0xFFFF;
      unsigned int nTotal = (status >> 16) & 0xFFFF;
      if(nTotal && (nDone == nTotal)){
	// we have processed all primitives
	m_myPrimState = RodPrimList::IDLE;

	// need to handle primitive replies
	int  rstatus = validateReplyBuffer();
	// std::cout << " validateReplyBuffer rstatus: " << rstatus << std::endl;
	int replylength =  rstatus + (sizeof(ReplyListHead) + sizeof(ReplyListTail))/sizeof(uint32_t);
	// std::cout << "Have a ReplyBuffer of "<< replylength << " words" << std::endl;
	if(replylength > 0){
	  m_myPrimState = RodPrimList::WAITING;
	  if (m_myOutList) delete m_myOutList;
	  m_myOutList = new RodOutList(replylength);
	  mdspBlockRead(m_masterXface.xface.requestChannelReplyBufferLoc[0],
			m_myOutList->getBody(), replylength);
	  //AKDEBUG
	  // std::cout << "Have " << m_myOutList->nMsgs() << " replies" << std::endl;
	}
	// inform DSP that we are done
	mdspSingleWrite(m_masterXface.xface.requestChannelControl[0], 0);
      } else {
// Cout to debug Primitive Lists not finishing....
//        std::cout << "NetRodModule::primHandler() not acting: " << nDone << "/" << nTotal << std::endl;
      }
    break;
    }
  case RodPrimList::WAITING: {
    m_myPrimState = RodPrimList::IDLE;
    break;
    }
  default: {
    break;
    }
  }
  return m_myPrimState;
}

//--------------------------------deleteOutList--------------------------------

  void NetRodModule::deleteOutList() {
  delete m_myOutList;
  m_myOutList = 0;
  return;
  }

//---------------------------------getTextBuffer-------------------------------

bool NetRodModule::getTextBuffer(std::string &rettxtBuf, unsigned int buffnum,
			      bool reset) {

  TextQueue textQueue;
  std::string tmptxtBuf = "";
  bool read = false;

  std::cout << __FUNCTION__ << " " << __LINE__ << std::endl;

  // bail out if non existing textbuffer requested
  if (buffnum >  n_textbuffers) return read;

  //  for (int buffnum=0; buffnum< m_masterXface.xface.nTextBuffers; buffnum++) {
    mdspBlockRead(m_textBuffer[buffnum], (uint32_t *)&textQueue,
		  sizeof(TextQueue)/sizeof(uint32_t));
    // check if we found text
    uint32_t tail = textQueue.tail;
    int32_t nWords, nChars;
    if(0 != (nWords = textQueue.head - textQueue.tail)){
        std::cout << "DEBUG: NetRodModule::getTextBuffer(): trying to read " << nWords<<" words into "
                                                         << rettxtBuf<<std::endl;
        if (nWords > 4096){
            std::cerr <<"ERROR: NetRodModule::getTextBuffer(): nWords is larger than 4096!!! skipping download of text buffers"<<std::endl;
            return false;
     }
     read = true;
     std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
     // handle wrap
      if(nWords<0)nWords += textQueue.length;
      nChars = nWords*sizeof(uint32_t);
      int32_t nTop = textQueue.length - tail;
      uint32_t *buffPtr = (uint32_t*) m_textData[buffnum];
      if(nWords >  nTop){
	mdspBlockRead(textQueue.base + sizeof(uint32_t)*tail, buffPtr, nTop);
	buffPtr += nTop;
	nWords  -= nTop;
	tail = 0;
      }
      if(nWords){
	mdspBlockRead(textQueue.base + sizeof(uint32_t)*tail, buffPtr, nWords);
	tail += nWords;
      }

      std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
      if(reset){
	// reset tail to read position on DSP
	TextQueue *DSPtextQueue = (TextQueue *)m_textBuffer[buffnum];
	//    tailoffset = &(queue.tail) - &queue;
	// AKDEBUG-> DANGEROUS, change interface
	// AKU mdspSingleWrite((uint32_t)&(DSPtextQueue->tail), tail);
	uint32_t tailoffset = &(textQueue.tail) - (uint32_t *)(&textQueue);
	mdspSingleWrite(m_textBuffer[buffnum]+tailoffset, tail);
      }
      // copy into output string, no real need to keep local buffer anymore,
      // but useful for debug

      for(int i = 0; i < nChars; i++){
	tmptxtBuf += m_textData[buffnum][i];
      }
    }
  //  }

    std::cout << __FUNCTION__ << " "  << __LINE__ << std::endl;
  //copy constructor to output string
  rettxtBuf = tmptxtBuf;

  return read;
}

#if 1
//----------------------------------hpiLoad------------------------------------

void NetRodModule::hpiLoad(uint32_t hpiReg, uint32_t hpiValue)
      {

  if (endianSwap) hpiValue = endianReverse32(hpiValue);
  m_myVmePort->write32(hpiReg, hpiValue);
}

//--------------------------------hpiFetch-------------------------------------

uint32_t NetRodModule::hpiFetch(uint32_t hpiReg)
              {

  uint32_t hpiValue;

  hpiValue=m_myVmePort->read32(hpiReg);
  if (endianSwap) hpiValue = endianReverse32(hpiValue);

  return hpiValue;
}
#endif
//-----------------------------mdspSingleRead---------------------------------

uint32_t NetRodModule::mdspSingleRead(const uint32_t dspAddr)
               {
  uint32_t value;

//	Do the read
  value = m_myVmePort->read32(dspAddr);
  if (endianSwap) {
    value = endianReverse32(value);
  }
  return value;
}

//------------------------------mdspSingleWrite------------------------------------

void NetRodModule::mdspSingleWrite(uint32_t dspAddr, uint32_t buffer)
      {

//	Do the write
  if (endianSwap) {
      buffer = endianReverse32(buffer);
  }

  m_myVmePort->write32(dspAddr, buffer);

  return;
     }

//-----------------------------mdspBlockReadDMA---------------------------------


void NetRodModule::mdspBlockRead(const uint32_t dspAddr, uint32_t buffer[],
        const int32_t wc, HpidMode mode) 
            {
  m_myVmePort->blockRead32(dspAddr,buffer,wc);
            return;
        }


//------------------------------mdspBlockWrite-----------------------------------

void NetRodModule::mdspBlockWrite(uint32_t dspAddr, uint32_t buffer[],
        int32_t wc, HpidMode mode) 
            {
  m_myVmePort->blockWrite32(dspAddr,buffer,wc);

            return;
        }

//-----------------------------mdspBlockDump--------------------------------

void NetRodModule::mdspBlockDump(const uint32_t firstAddress,
        const uint32_t lastAddress,
        const std::string & fileName)  {

    int32_t numBytes;
    std::ofstream fout;
    numBytes = lastAddress - firstAddress + 1;
    fout.open(fileName.c_str(), std::ios::binary);
    if (fout.is_open()) {
        uint32_t * buffer;
        try {
            buffer = new uint32_t[numBytes];
        }
        catch (std::bad_alloc & ba) {
            throw RodException(
                    "mdspBlockDump failed to allocate buffer; buffer size in bytes=", numBytes);
        }
        mdspBlockRead(firstAddress, buffer, numBytes/4);
        fout.write((char*)buffer, numBytes);
        fout.close();
        delete [] buffer;
    }
    else {
        throw RodException("mdspBlockDump failed to open file");
    }
    return;
}

//--------------------------------slvHpiLoad-----------------------------------

void NetRodModule::slvHpiLoad(uint32_t hpiReg, uint32_t hpiValue,
        int32_t slaveNum) {

    uint32_t address;
    address = hpiReg + slaveNum*SLAVE_HPI_OFFSET;

    mdspSingleWrite(address, hpiValue);
}

//------------------------------slvHpiFetch------------------------------------

uint32_t NetRodModule::slvHpiFetch(uint32_t hpiReg, int32_t slaveNum)
     {

        uint32_t hpiValue;
        uint32_t address;
        address = hpiReg + slaveNum*SLAVE_HPI_OFFSET;

        hpiValue=mdspSingleRead(address);

        return hpiValue;
    }

//-------------------------------slvHpiSingleRead---------------------------------
uint32_t NetRodModule::slvHpiSingleRead(uint32_t dspAddr, int32_t slaveNum )
    {

        uint32_t slvHpia, slvHpid;
        uint32_t value;
        slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
        slvHpid = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

        // Check that address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
        if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
                "Slave address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

        //  Load the DSP address into the HPIA register
        mdspSingleWrite(slvHpia, dspAddr);

        //	Do the read
        value = mdspSingleRead(slvHpid);

        return value;
    }

//------------------------------slvHpiSingleWrite------------------------------------

void NetRodModule::slvHpiSingleWrite(uint32_t dspAddr, uint32_t buffer,
        int32_t slaveNum)  {

    uint32_t slvHpia, slvHpid;
    slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpid = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

    // Check that address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    mdspSingleWrite(slvHpia, dspAddr);

    //	Do the write
    mdspSingleWrite(slvHpid, buffer);

    return;
}

//-----------------------------slvHpiBlockRead---------------------------------

void NetRodModule::slvHpiBlockRead(const uint32_t dspAddr,
        uint32_t buffer[], const int32_t wordCount,
        int32_t slaveNum, HpidMode mode)
 {

    uint32_t hpidAddr, dspAddrLocal;
    int32_t myCount, blockWordCount, wordIncr;
    uint32_t slvHpia, slvHpidAuto, slvHpidNoAuto;

    // Check that starting address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave start address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    // Check that ending address is not in forbidden area as per TI errata doc SPRZ191F, p. 11

    uint32_t endAddr = dspAddr + 4*wordCount;

    if ((endAddr >= 0x60000000) && (endAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave stop address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", endAddr);

    // Check that addresses do not straddle the forbidden area as per TI errata doc SPRZ191F, p. 11

    if ((dspAddr < 0x60000000) && (endAddr > 0x7FFFFFFF)) throw RodException(
            "Block of slave addresses straddles forbidden range of 0x60000000 to 0x7FFFFFFF; addresses =", dspAddr, endAddr);

    slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpidAuto = SLAVE_HPID_AUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpidNoAuto = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

    // Determine the HPI mode to use.
    switch (mode) {
        case AUTO:
            hpidAddr= slvHpidAuto;
            break;
        case NO_AUTO:
            hpidAddr = slvHpidNoAuto;
            break;
        case DYNAMIC:
        default:
            if (wordCount == 1){
                hpidAddr = slvHpidNoAuto;
            }
            else {
                hpidAddr = slvHpidAuto;
            }
            break;
    }

    // TI Errata SPRZ191F, p. 17, requires that every autoincrement read/write be terminated with a fixed
    // mode read/write. This routine has been modified to comply with this.

    int32_t modCount = wordCount -1;
    if (modCount == 0) modCount = 1;

    //  Load the DSP address into the HPIA register
    mdspSingleWrite(slvHpia, dspAddr);

    // Check to see that we don't access too large a block. HPID has 20 bits of
    // address space available. MAX_HPID_WORD_ELEMENTS is defined in RodVmeAddresses.h

    blockWordCount = std::min(modCount, MAX_HPID_WORD_ELEMENTS);

    //	Set up the transfer as a series of block transfers
    for (myCount=modCount, wordIncr=0; myCount>0;
            myCount-=blockWordCount, wordIncr+=blockWordCount) {

        //	Recheck each block
        blockWordCount = std::min(myCount, MAX_HPID_WORD_ELEMENTS);

        //	Do the transfer for this block
        dspAddrLocal = dspAddr+ (4*wordIncr);
        mdspBlockRead(hpidAddr, &buffer[wordIncr], blockWordCount, NO_AUTO);
    }

    //     Do the final fixed-mode transfer
    if (hpidAddr == slvHpidAuto) {
        hpidAddr = slvHpidNoAuto;
        mdspSingleWrite(slvHpia, modCount*4);
        buffer[modCount] = mdspSingleRead(hpidAddr);
    }

    return;
}

//------------------------------slvHpiBlockWrite-----------------------------------

void NetRodModule::slvHpiBlockWrite(uint32_t dspAddr, uint32_t buffer[],
        int32_t wordCount, int32_t slaveNum, HpidMode mode)
 {

    uint32_t hpidAddr, dspAddrLocal;
    int32_t myCount, blockWordCount, wordIncr;
    uint32_t slvHpia, slvHpidAuto, slvHpidNoAuto;

    // Check that starting address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave start address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    uint32_t endAddr = dspAddr + 4*wordCount;

    // Check that ending address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((endAddr >= 0x60000000) && (endAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave stop address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", endAddr);

    // Check that addresses do not straddle the forbidden area as per TI errata doc SPRZ191F, p. 11

    if ((dspAddr < 0x60000000) && (endAddr > 0x7FFFFFFF)) throw RodException(
            "Block of slave addresses straddles forbidden range of 0x60000000 to 0x7FFFFFFF; addresses =", dspAddr, endAddr);

    slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpidAuto = SLAVE_HPID_AUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpidNoAuto = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

    //  Load the initial dsp address into the HPIA register
    mdspSingleWrite(slvHpia, dspAddr);

    // Determine the HPI mode to use.
    switch (mode) {
        case AUTO:
            hpidAddr= slvHpidAuto;
            break;
        case NO_AUTO:
            hpidAddr = slvHpidNoAuto;
            break;
        case DYNAMIC:
        default:
            if (wordCount == 1){
                hpidAddr = slvHpidNoAuto;
            }
            else {
                hpidAddr = slvHpidAuto;
            }
            break;
    }

    // TI Errata SPRZ191F, p. 17, requires that every autoincrement read/write be terminated with a fixed
    // mode read/write. This routine has been modified to comply with this.

    int32_t modCount = wordCount -1;
    if (modCount == 0) modCount = 1;

    // Check to see that we don't access too large a block. HPID has 20 bits of
    // address space available. MAX_HPID_WORD_ELEMENTS is defined in vmeAddressMap.h

    blockWordCount = std::min(modCount, MAX_HPID_WORD_ELEMENTS);

    //	Set up the transfer as a series of block transfers
    for (myCount=modCount, wordIncr=0; myCount>0;
            myCount-=blockWordCount, wordIncr+=blockWordCount) {

        //	Recheck each block
        blockWordCount = std::min(myCount, MAX_HPID_WORD_ELEMENTS);

        //	Do the transfer for this block
        dspAddrLocal = dspAddr+ (4*wordIncr);
        mdspBlockWrite(hpidAddr, &buffer[wordIncr], blockWordCount, NO_AUTO);
    }

    //    If autoincrement was used, do the final fixed-mode transfer
    if (hpidAddr == slvHpidAuto) {
        hpidAddr = slvHpidNoAuto;
        mdspSingleWrite(slvHpia, modCount*4);
        mdspSingleWrite(hpidAddr, buffer[modCount]);
    }

    return;
}

//-------------------------------slvSingleRead---------------------------------
uint32_t NetRodModule::slvSingleRead(uint32_t dspAddr, int32_t slaveNum )
{

        uint32_t slvHpia, slvHpid;
        uint32_t value;
        int32_t numWords;
        slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
        slvHpid = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

        // Check that address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
        if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
                "Slave address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

        // Use RW_SLAVE_MEMORY to read slave DSP memory
        int32_t* writeSlvBuff;
        numWords = 1;

        writeSlvBuff = new int32_t[numWords + sizeof(RwSlaveMemoryIn)/4];
        RwSlaveMemoryIn* RSlaveMemoryIn = (RwSlaveMemoryIn*)writeSlvBuff;
        RSlaveMemoryIn->slave      = slaveNum;
        RSlaveMemoryIn->flags      = 1;
        RSlaveMemoryIn->slaveAddr  = (uint32_t)dspAddr;
        RSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
        RSlaveMemoryIn->nWords     = numWords;
        writeSlvBuff[5] = 0;
        RodPrimitive* slvRWPrim;
        slvRWPrim = new RodPrimitive(numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
                1, RW_SLAVE_MEMORY, R_BOGUS, writeSlvBuff);
        synchSendPrim(*slvRWPrim);
        delete [] writeSlvBuff;
        delete slvRWPrim;

        // Get reply buffer and extract the value
        RodOutList* outList = getOutList();
        if (outList) {
            uint32_t* outBody = outList->getBody();
            value = outBody[sizeof(RwMemoryReplyHeader)/sizeof(uint32_t)];
            deleteOutList();
        }
        else {
            throw RodException("No outList in slvPrimSingleRead");
        }
        return value;
    }

//------------------------------slvSingleWrite------------------------------------

void NetRodModule::slvSingleWrite(uint32_t dspAddr, uint32_t buffer,
        int32_t slaveNum)  {

    uint32_t slvHpia, slvHpid;
    int32_t numWords;
    slvHpia = SLAVE_HPIA_BASE + slaveNum*SLAVE_HPI_OFFSET;
    slvHpid = SLAVE_HPID_NOAUTO_BASE + slaveNum*SLAVE_HPI_OFFSET;

    // Check that address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    // Use RW_SLAVE_MEMORY to write to slave DSP memory
    int32_t* writeSlvBuff;
    numWords = 1;
    writeSlvBuff = new int32_t[numWords + sizeof(RwSlaveMemoryIn)/4];
    RwSlaveMemoryIn* WSlaveMemoryIn = (RwSlaveMemoryIn*)writeSlvBuff;
    WSlaveMemoryIn->slave      = slaveNum;
    WSlaveMemoryIn->flags      = RW_WRITE;
    WSlaveMemoryIn->slaveAddr  = (uint32_t)dspAddr;
    WSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
    WSlaveMemoryIn->nWords     = numWords;
    writeSlvBuff[5] = buffer;
    RodPrimitive* slvRWPrim;
    slvRWPrim = new RodPrimitive(numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
            1, RW_SLAVE_MEMORY, R_BOGUS, writeSlvBuff);
    synchSendPrim(*slvRWPrim);
    delete [] writeSlvBuff;
    delete slvRWPrim;

    return;
}

//-----------------------------slvBlockRead---------------------------------

void NetRodModule::slvBlockRead(const uint32_t dspAddr, uint32_t buffer[],
        const int32_t wordCount, int32_t slaveNum, HpidMode mode)
 {

    int32_t numWords;
    int32_t readWords = 0;
    uint32_t slaveAddr = dspAddr;

    // Check that starting address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave start address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    // Check that ending address is not in forbidden area as per TI errata doc SPRZ191F, p. 11

    uint32_t endAddr = dspAddr + 4*wordCount;

    if ((endAddr >= 0x60000000) && (endAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave stop address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", endAddr);

    // Check that addresses do not straddle the forbidden area as per TI errata doc SPRZ191F, p. 11

    if ((dspAddr < 0x60000000) && (endAddr > 0x7FFFFFFF)) throw RodException(
            "Block of slave addresses straddles forbidden range of 0x60000000 to 0x7FFFFFFF; addresses =", dspAddr, endAddr);

    // Use RW_SLAVE_MEMORY to read slave DSP memory
    int32_t* writeSlvBuff;
    numWords = wordCount;
    int k = 0;

    while(wordCount > readWords){
        writeSlvBuff = new int32_t[sizeof(RwSlaveMemoryIn)/4];
        RwSlaveMemoryIn* RSlaveMemoryIn = (RwSlaveMemoryIn*)writeSlvBuff;
        RSlaveMemoryIn->slave      = slaveNum;
        RSlaveMemoryIn->flags      = RW_READ;
        RSlaveMemoryIn->slaveAddr  = (uint32_t)slaveAddr;
        RSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
        RSlaveMemoryIn->nWords     = numWords - readWords;
        RodPrimitive* slvRWPrim;
        slvRWPrim = new RodPrimitive(sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
                1, RW_SLAVE_MEMORY, R_BOGUS, writeSlvBuff);
        synchSendPrim(*slvRWPrim);
        delete [] writeSlvBuff;
        delete slvRWPrim;

        // Get reply buffer and extract the value
        RodOutList* outList = getOutList();
        if (outList) {
            uint32_t* outBody = (uint32_t*)outList->getMsgBody(1);
            RwMemoryReplyHeader * RwMemoryReplyHeader_Out = (RwMemoryReplyHeader *) outBody;
            readWords += RwMemoryReplyHeader_Out->nWords;
            slaveAddr += sizeof(uint32_t)*RwMemoryReplyHeader_Out->nWords;
            /* break if we don't read anything */
            if(0==RwMemoryReplyHeader_Out->nWords)readWords += wordCount;
            for (int i = 0; i< readWords; i++) {
                buffer[k++] = outBody[i+(sizeof(RwMemoryReplyHeader)/sizeof(uint32_t))];
            }
            deleteOutList();
        }
        else {
            throw RodException("No outList in slvPrimBlockRead");
        }
    }

    return;
}

//------------------------------slvBlockWrite-----------------------------------

void NetRodModule::slvBlockWrite(uint32_t dspAddr, uint32_t buffer[],
        int32_t wordCount, int32_t slaveNum, HpidMode mode)
 {

    int32_t numWords;

    // Check that starting address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((dspAddr >= 0x60000000) && (dspAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave start address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", dspAddr);

    uint32_t endAddr = dspAddr + 4*wordCount;

    // Check that ending address is not in forbidden area as per TI errata doc SPRZ191F, p. 11
    if ((endAddr >= 0x60000000) && (endAddr <= 0x7FFFFFFF)) throw RodException(
            "Slave stop address is in forbidden range of 0x60000000 to 0x7FFFFFFF; address =", endAddr);

    // Check that addresses do not straddle the forbidden area as per TI errata doc SPRZ191F, p. 11

    if ((dspAddr < 0x60000000) && (endAddr > 0x7FFFFFFF)) throw RodException(
            "Block of slave addresses straddles forbidden range of 0x60000000 to 0x7FFFFFFF; addresses =", dspAddr, endAddr);

    // Use RW_SLAVE_MEMORY to write to slave DSP memory
    int32_t* writeSlvBuff;
    numWords = wordCount;

    writeSlvBuff = new int32_t[numWords + sizeof(RwSlaveMemoryIn)/4];
    RwSlaveMemoryIn* WSlaveMemoryIn = (RwSlaveMemoryIn*)writeSlvBuff;
    WSlaveMemoryIn->slave      = slaveNum;
    WSlaveMemoryIn->flags      = RW_WRITE;
    WSlaveMemoryIn->slaveAddr  = (uint32_t)dspAddr;
    WSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
    WSlaveMemoryIn->nWords     = numWords;
    for (int i=0; i<numWords; i++) {
        writeSlvBuff[i + sizeof(RwSlaveMemoryIn)/4] = buffer[i];
    }
    RodPrimitive* slvRWPrim;
    slvRWPrim = new RodPrimitive(numWords + sizeof(RwSlaveMemoryIn)/sizeof(uint32_t),
            1, RW_SLAVE_MEMORY, R_BOGUS, writeSlvBuff);
    synchSendPrim(*slvRWPrim);
    delete [] writeSlvBuff;
    delete slvRWPrim;

    return;
}

//------------------------------resetMasterDsp--------------------------------

void NetRodModule::resetMasterDsp()  {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 1);
    if (endianSwap) {
        value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
        rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
        if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
            throw RodException("DSP reset timeout in resetMasterDsp(), DSP_RESET_TIMEOUT=",
                    (int32_t)DSP_RESET_TIMEOUT);
        }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);

    return;
}

//------------------------------resetSlaveDsp--------------------------------

void NetRodModule::resetSlaveDsp(int32_t slaveNumber)  {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 2 + slaveNumber);
    if (endianSwap) {
        value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
        rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
        if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
            throw RodException("DSP reset timeout in resetSlaveDsp(), DSP_RESET_TIMEOUT=",
                    (int32_t)DSP_RESET_TIMEOUT);
        }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);

    return;
}


//------------------------------resetAllDsps--------------------------------

void NetRodModule::resetAllDsps()  {
    uint32_t value=0;
    uint32_t rodRegValue;
    clock_t startTime;

    setBit(&value, 6);
    if (endianSwap) {
        value = endianReverse32(value);
    }
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[2], value);
    sleep(4000);
    rodRegValue = 0;
    startTime = clock();
    do {
        rodRegValue = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
        if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
            throw RodException("DSP reset timeout in resetAllDsps(), DSP_RESET_TIMEOUT=",
                    (int32_t)DSP_RESET_TIMEOUT);
        }
    }
    while (rodRegValue != ALLDSP_RESET_COMPLETE);

    return;
}

//-------------------------------chipEraseHpi---------------------------------

void NetRodModule::chipEraseHpi()  {
    uint32_t value = 0x10;
    uint32_t flashBase;

    //  flashBase = m_myMdspMap->flashBase();

    // Commands 1 to 5
    commonEraseCommandsHpi(flashBase);

    // Command 6
    mdspSingleWrite(flashBase+(0x5555<<2), value);

    // Wait for operation to complete
    sleep(CHIP_ERASE_TIME_MS);
    return;
}

//--------------------------------sectorErase---------------------------------

void NetRodModule::sectorErase(uint32_t sectorBaseAddress)
    {
        uint32_t flashBaseAddress, valueD32;
        bool busyBit;

        // Get flash base address
        switch (m_revision) {
            case 0x0f:
            case 0x0e:
                flashBaseAddress = FPGA_FLASH_REL_ADDR_REVE;
                break;
            case 0x0b:
            case 0x0c:
            default:
                if ((sectorBaseAddress<FPGA_FLASH_0_BOTTOM)||
                        (sectorBaseAddress>FPGA_FLASH_2_BOTTOM+FLASH_MEMORY_SIZE)) {
                    throw RodException("Flash sector base addr out of range, sectorBaseAddress=",
                            sectorBaseAddress);
                }
                if (sectorBaseAddress<FPGA_FLASH_1_BOTTOM) {
                    flashBaseAddress = FPGA_FLASH_0_BOTTOM;
                }
                else if (sectorBaseAddress<FPGA_FLASH_2_BOTTOM) {
                    flashBaseAddress = FPGA_FLASH_1_BOTTOM;
                }
                else flashBaseAddress = FPGA_FLASH_2_BOTTOM;
                break;
        }
        // Commands 1 to 5
        commonEraseCommands(flashBaseAddress);

        // Set write bit
        vmeWriteElementFlash(0x30, sectorBaseAddress, WRITE_COMMAND_HANDSHAKE_BIT);

        // Wait for operation to complete
        switch (m_revision ) {
            case 0x0f:
            case 0x0e:
                do {                           /* Poll busy */
                    valueD32 = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[4]);
                    busyBit = readBit(valueD32, 3);
                } while (busyBit);
                break;
            case 0x0b:
            case 0x0c:
            default:
                sleep(SECTOR_ERASE_TIME_MS);
                break;
        }

        return;
    }

//---------------_-------------writeByteToFlash-------------------------------

void NetRodModule::writeByteToFlash(uint32_t address, uint8_t data)
     {
        uint8_t readData;
        clock_t startTime;   // start time for DQ7 data polling, in seconds
        int32_t updateAddress;

        if (data!=0xFF) {   // if erased, don't rewrite. 0xFF is default after erasing
            // write data
            vmeWriteElementFlash(data, address, WRITE_DATA_HANDSHAKE_BIT);
            updateAddress = 0;
        }
        else {
            updateAddress = 1;
        }

        // Wait for operation to complete - data polling
        startTime = clock();
        while(1) {
            readData = readByteFromFlash(address, updateAddress);
            if (readData == data) return;
            else if((clock()-startTime)/CLOCKS_PER_SEC > FLASH_TIMEOUT) {
                throw RodException("Flash timeout in writeByteToFlash, FLASH_TIMEOUT=",
                        (int32_t)FLASH_TIMEOUT);
            }
        }
        return;
    }

//----------------------------writeBlockToFlash-------------------------------

void NetRodModule::writeBlockToFlash(uint32_t address, uint8_t *data,
        uint32_t numBytes)  {
    uint32_t index, sectorSize;
    switch (m_revision) {
        case 0x0f:
        case 0x0e:
            sectorSize = FLASH_SECTOR_SIZE_REVE;
            break;
        case 0x0b:
        case 0x0c:
        default:
            sectorSize = FLASH_SECTOR_SIZE;
            break;
    }
    for (index=0; index<numBytes; ++index) {
        if ((index%sectorSize)==0) {
            sectorErase(address+index);
        }
        writeByteToFlash(address+index, *(data+index));
    }
    return;
}

//---------------------------writeBlockToFlashHpi-----------------------------

void NetRodModule::writeBlockToFlashHpi(uint32_t address, uint8_t *data,
        uint32_t numBytes)  {
    uint32_t index, sectorAddr;
    uint32_t flashBase = 0x01400000;
    const uint32_t eraseData=0x30;
    const uint32_t data1=0xAA;
    const uint32_t data2=0x55;
    const uint32_t data3=0xA0;
    const uint32_t addr1=flashBase+(0x5555<<2);
    const uint32_t addr2=flashBase+(0x2AAA<<2);
    uint32_t byteRelAddr;
    int32_t words;
    uint8_t dataVal, verifyVal;
    uint32_t longVal, busyVal;

    for (index=0; index<numBytes; ++index) {
        byteRelAddr = address+index-flashBase;
        if ((index%FLASH_SECTOR_SIZE)==0) {
            // implement sectorEraseHpi(address+index) inline.
            //    commands 1 to 5
            commonEraseCommandsHpi(flashBase);
            // 6th command
            sectorAddr = flashBase+
                (byteRelAddr<<2);
            mdspSingleWrite(sectorAddr, eraseData);
            // Wait for operation to complete
            sleep(SECTOR_ERASE_TIME_MS);
        }

        // implement writeByteToFlashHpi(address+index, *(data+index)) inline.
        dataVal = *(data+index);
        longVal = dataVal&0x000000FF;
        if (dataVal != 0xFF) {
            mdspSingleWrite(addr1, data1);                      // 1st command
            mdspSingleWrite(addr2, data2);                      // 2nd command
            mdspSingleWrite(addr1, data3);                      // 3rd command
            mdspSingleWrite(flashBase+(byteRelAddr<<2), longVal);
            // Verify data by reading it back before continuing. Flash memory may need up to
            // 20 microseconds to complete the write sequence.
            for (int i=0; i<4000; i++) {
                busyVal = mdspSingleRead(flashBase+byteRelAddr);
                switch (byteRelAddr%4) {
                    case 1: busyVal = busyVal >> 8;
                            break;
                    case 2: busyVal = busyVal >> 16;
                            break;
                    case 3: busyVal = busyVal >> 24;
                            break;
                    default: break;
                }
                busyVal = busyVal & 0x000000FF;
                if (busyVal == longVal) break;
            }
        }
    }

    // Verification
    words = (numBytes+3)/4;       // round up
    uint8_t *buffer;
    try {
        buffer = new uint8_t[words*4];
    }
    catch (std::bad_alloc & ba) {
        throw RodException("writeBlockToFlashHpi unable to get buffer.");
    }
    mdspBlockRead(address, (uint32_t*)buffer, -words);
    bool vfailed = false;
    for (index=0; index<numBytes; ++index) {
        dataVal = *(data+index);
        verifyVal = *(buffer+index);
        if (dataVal != verifyVal) {
            //      delete [] buffer;
            int dread =  verifyVal;
            int dwritten = dataVal;
            std::cout << "at " << index << " written "<< dwritten <<  " read " << dread
                << " written "<< dataVal <<  " read " << verifyVal << std::endl;
            vfailed = true;
        }
    }
    if(vfailed) throw RodException("writeBlockToFlashHpi verify failed. index, data:", index, dataVal);
    delete [] buffer;
    return;
}

//-----------------------------readByteFromFlash------------------------------

uint8_t NetRodModule::readByteFromFlash(uint32_t address, int32_t updateAddress)
   {
        uint8_t dataByte;
        uint32_t commandReg;
        clock_t startTime;
        uint32_t handshakeBitValue = 0;
        uint32_t valueD32;

        if (updateAddress) {
            m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[4], address);
        }
        // Set rd bit
        setBit(&handshakeBitValue, READ_HANDSHAKE_BIT);
        m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[3], handshakeBitValue);

        // Wait for valid data
        startTime = clock();
        while (1) {
            commandReg = m_myVmePort->read32(FPGA_CONTROL_REG_REL_ADDR[3]);
            if (0==readBit(commandReg, READ_HANDSHAKE_BIT)) break;
            if ((clock()-startTime)>FLASH_TIMEOUT) throw RodException(
                    "Timeout in readByteFromFlash. Address=", address);
        }

        // Read valid data
        valueD32 = m_myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[7]);
        dataByte = (uint8_t)(valueD32&0xFF);
        return dataByte;
    }

//---------------------------vmeWriteElementFlash------------------------------

void NetRodModule::vmeWriteElementFlash(uint8_t value, uint32_t address,
        int32_t handshakeBit)  {
    uint32_t ctrlReg4Val;  // address(23:0) + data(31:24)
    uint32_t commandReg;
    clock_t startTime;
    uint32_t handshakeBitValue=0;

    ctrlReg4Val = (value<<24)| address;
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[4], ctrlReg4Val);

    // Set wr bit
    setBit(&handshakeBitValue, handshakeBit);
    m_myVmePort->write32(FPGA_CONTROL_REG_REL_ADDR[3], handshakeBitValue);

    // Wait for ack
    startTime = clock();
    while(1) {
        commandReg = m_myVmePort->read32(FPGA_CONTROL_REG_REL_ADDR[3]);
        if (0==readBit(commandReg, handshakeBit)) break;
        if ((clock()-startTime)>FLASH_TIMEOUT) throw RodException(
                "Timeout in vmeWriteElementFlash. Address, value=",
                address, (uint32_t)value);
    }
    return;
}

//-----------------------------readBlockFromFlash------------------------------

void NetRodModule::readBlockFromFlash(uint32_t address, uint8_t *buffer,
        uint32_t numBytes)  {
    uint32_t index;
    for (index=0; index<numBytes; ++index) {
        buffer[index] = readByteFromFlash(address+index, 1);
    }
    return;
}

//-----------------------------commonEraseCommands----------------------------

void NetRodModule::commonEraseCommands(uint32_t flashBaseAddr)
     {

        const uint32_t addr1 = flashBaseAddr+0x5555;
        const uint32_t addr2 = flashBaseAddr+0x2AAA;

        // 1st command
        vmeWriteElementFlash(0xAA, addr1, WRITE_COMMAND_HANDSHAKE_BIT);

        // 2nd command
        vmeWriteElementFlash(0x55, addr2, WRITE_COMMAND_HANDSHAKE_BIT);

        // 3rd command
        vmeWriteElementFlash(0x80, addr1, WRITE_COMMAND_HANDSHAKE_BIT);

        // 4th command
        vmeWriteElementFlash(0xAA, addr1, WRITE_COMMAND_HANDSHAKE_BIT);

        // 5th command
        vmeWriteElementFlash(0x55, addr2, WRITE_COMMAND_HANDSHAKE_BIT);

        return;
    }

//----------------------------commonEraseCommandsHpi--------------------------

void NetRodModule::commonEraseCommandsHpi(uint32_t flashBaseAddr)
    {

        uint32_t buffer;
        const uint32_t addr1 = flashBaseAddr+(0x5555<<2);
        const uint32_t addr2 = flashBaseAddr+(0x2AAA<<2);

        // 1st command
        buffer = 0xAA;
        mdspSingleWrite(addr1, buffer);

        // 2nd command
        buffer = 0x55;
        mdspSingleWrite(addr2, buffer);

        // 3rd command
        buffer = 0x80;
        mdspSingleWrite(addr1, buffer);

        // 4th command
        buffer = 0xAA;
        mdspSingleWrite(addr1, buffer);

        // 5th command
        buffer = 0x55;
        mdspSingleWrite(addr2, buffer);

        return;
    }

//---------------------------getFlashSectorSize---------------------------

uint32_t NetRodModule::getFlashSectorSize() {
    uint32_t sectorSize;
    switch (m_revision) {
        case 0x0f:
        case 0x0e:
            sectorSize = FLASH_SECTOR_SIZE_REVE;
            break;
        case 0x0b:
        case 0x0c:
        default:
            sectorSize = FLASH_SECTOR_SIZE;
            break;
    }
    return sectorSize;
}

//----------------------------------sleep-------------------------------------
// ks: replaced buy an inline call to usleep(), which will suspend this thread and
//     gives other threads the chances tpo talk to there rods
/*void NetRodModule::sleep(const double milliSecs) {
    clock_t start, delay;
    delay = CLOCKS_PER_SEC;
    delay = clock_t(milliSecs * CLOCKS_PER_SEC / 1000 ); //need explicit type cast
    //to avoid warning.
    start = clock();
    while (clock()-start< delay)   // wait until time elapses
        ;
    return;
}*/
//--------------------------------checkSum------------------------------------

uint32_t NetRodModule::checkSum(const uint32_t *sourceArray, const int32_t wordCount) {
    uint32_t result=0;
    int32_t i;

    for (i=0; i<wordCount; ++i) {
        result ^= sourceArray[i];
    }
    return result;
}

//-----------------------------endianReverse32----------------------------------

uint32_t NetRodModule::endianReverse32(const uint32_t inVal) {
    uint32_t outVal;
    outVal  = (inVal & 0x000000ff) << 24;
    outVal |= (inVal & 0x0000ff00) << 8;
    outVal |= (inVal & 0x00ff0000) >> 8;
    outVal |= (inVal & 0xff000000) >> 24;
    return outVal;
}

//----------------------------------bocCheck------------------------------------
/*! Checks BOC status. If no BOC clock or BOC is busy, it returns 0; if BOC is OK it returns 1.
  It also sets the data member m_bocFlag as follows:
  0 = BOC OK
  1 = No clock from BOC
  2 = BOC is busy
 */
uint32_t NetRodModule::bocCheck() {
    uint32_t fpgaReg=0x404420;
    uint32_t clockBit=0x2, bocBusyBit=0x4;
    uint32_t regVal;
    regVal = mdspSingleRead(fpgaReg);
    //AKDEBUG careful this depends on the FPGA version due to a previous bug ....
    if ((regVal&clockBit)==1) {            // No clock
        m_bocFlag = 1;
        return 0;
    }
    if ((regVal&bocBusyBit)==1) {          // BOC busy
        m_bocFlag = 2;
        return 0;
    }
    m_bocFlag = 0;
    return 1;
}

void NetRodModule::readMasterMem(int startAddr, int nWords, std::vector<unsigned int> &out) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    mdspBlockRead(startAddr, buf, nWords);
    for (int i=0; i<nWords; i++) out.push_back(buf[i]);
    delete[] buf;
}

void NetRodModule::writeMasterMem(int startAddr, int nWords, std::vector<unsigned int> &in) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    for (int i=0; i<nWords; i++) buf[i] = in[i];
  mdspBlockWrite(startAddr, buf, nWords);
  delete[] buf;
}

void NetRodModule::readSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) {
  RwSlaveMemoryIn RSlaveMemoryIn;
  int slaveAddr = startAddr;
  int readWords = 0;

  out.clear();
  while((nWords>readWords)){

 // TBD JD: Find, whether we're good here.
 // Debug
 //    std::cout << "Just testing whether this procedure does anything at all..." << std::endl;
 //    std::cout << " NetRodModule::readSlaveMem nWords: " << nWords << " readWords: " << readWords << std::endl;
    RSlaveMemoryIn.slave      = slaveId;
    RSlaveMemoryIn.flags      = RW_READ;
    RSlaveMemoryIn.slaveAddr  = (uint32_t)slaveAddr;
    RSlaveMemoryIn.masterAddr = (uint32_t)NULL ;
    RSlaveMemoryIn.nWords     = nWords - readWords;

    RodPrimitive* rwsmPrim = new RodPrimitive(sizeof(RwSlaveMemoryIn)/sizeof(uint32_t), 0,
					      RW_SLAVE_MEMORY, R_BOGUS,
					      (int32_t*)&RSlaveMemoryIn);
    RodOutList* outList;
    executeMasterPrimitiveSync(*rwsmPrim, outList);
    delete rwsmPrim;
    if (outList == NULL) {
      return;
    } else {
      uint32_t *outBody = (uint32_t*)outList->getMsgBody(1);
      RwMemoryReplyHeader * RwMemoryReplyHeader_Out = (RwMemoryReplyHeader *) outBody;
      //Debug
      // std::cout << "Header says - Read nWords: " << RwMemoryReplyHeader_Out->nWords << " starting from 0x" << std::hex << RwMemoryReplyHeader_Out->addr << std::dec << " on Slave " << RwMemoryReplyHeader_Out->id << std::endl;
      readWords += RwMemoryReplyHeader_Out->nWords;
      slaveAddr += sizeof(uint32_t) * RwMemoryReplyHeader_Out->nWords;
      /* break if we don't read anything */
      if(0==RwMemoryReplyHeader_Out->nWords)readWords += nWords;
      for (unsigned int i=(sizeof(RwMemoryReplyHeader)/sizeof(uint32_t)); i<((sizeof(RwMemoryReplyHeader)/sizeof(uint32_t)) + RwMemoryReplyHeader_Out->nWords); i++) {
	out.push_back(outBody[i]);
      }
      deleteOutList();
    }
  }
}

void NetRodModule::writeSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) {
  int32_t* rwsmData = new int32_t[nWords + sizeof(RwSlaveMemoryIn)/4];
  RwSlaveMemoryIn* WSlaveMemoryIn = (RwSlaveMemoryIn*)rwsmData;
  WSlaveMemoryIn->slave      = slaveId;
  WSlaveMemoryIn->flags      = RW_WRITE;
  WSlaveMemoryIn->slaveAddr  = (uint32_t)startAddr;
  WSlaveMemoryIn->masterAddr = (uint32_t)NULL ;
  WSlaveMemoryIn->nWords     = nWords;

  int i;
  for (i=0; i<nWords; i++) rwsmData[sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)+i] = in[i];
  RodPrimitive* rwsmPrim = new RodPrimitive(sizeof(RwSlaveMemoryIn)/sizeof(uint32_t)+nWords
					    , 0, RW_SLAVE_MEMORY, R_BOGUS, rwsmData);
  executeMasterPrimitiveSync(*rwsmPrim);
  delete rwsmPrim;
  delete[] rwsmData;
}

void NetRodModule::readSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) {
  std::cout << "NetRodModule::readSlaveMemDirect  NOT fully implemented" << std::endl;
//   unsigned int cr0 = mdspSingleRead(COMMAND_REG_0);
//   mdspSingleWrite(COMMAND_REG_0, cr0+(0x1<<CR_DMA_ACCESS_REQ));
//   unsigned int sr0, repCount = 10000;
//   do {
//     sr0 = mdspSingleRead(STATUS_REG_0);
//   } while ((sr0 & (0x1<<SR_DMA_ACCESS_ACK)) == 0 && repCount-- > 0);

//   if (repCount > 0) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    slvBlockRead(startAddr, buf, nWords, slaveId);
    for (int i=0; i<nWords; i++) out.push_back(buf[i]);
    delete[] buf;
//     mdspSingleWrite(COMMAND_REG_0, cr0);
//   } else {
//   }
}

void NetRodModule::writeSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) {
  std::cout << "NetRodModule::writeSlaveMemDirect NOT fully implemented" << std::endl;
//   unsigned int cr0 = mdspSingleRead(COMMAND_REG_0);
//   mdspSingleWrite(COMMAND_REG_0, cr0+CR_DMA_ACCESS_REQ);
//   unsigned int sr0, repCount = 10000;
//   do {
//     sr0 = mdspSingleRead(STATUS_REG_0);
//   } while ((sr0 & (0x1<<SR_DMA_ACCESS_ACK)) == 0 && repCount-- > 0);

//   if (repCount > 0) {
    uint32_t *buf;
    buf = new uint32_t[nWords];
    for (int i=0; i<nWords; i++) buf[i] = in[i];
    slvBlockWrite(startAddr, buf, nWords, slaveId);
    delete[] buf;
//   } else {
//   }
}

void NetRodModule::readFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &out) {
  RwFifoIn RwFifo_In;
  RwFifo_In.fifoId      = fifo;
  RwFifo_In.fRead       = RW_READ;
  RwFifo_In.length      = nWords;
  RwFifo_In.dataPtr     = (uint32_t )NULL;
  RodPrimitive* rwfPrim = new RodPrimitive(sizeof(RwFifoIn)/sizeof(uint32_t), 0,
					   RW_FIFO, R_BOGUS, (int32_t*)&RwFifo_In);
  RodOutList* outList;
  //  executeMasterPrimitiveSync(*rwfPrim, outList);
  //  executeMasterPrimitiveSync(*rwfPrim);
  synchSendPrim(*rwfPrim);
  delete rwfPrim;
  out.clear();
  outList = getOutList();
  uint32_t *outBody = (uint32_t *) outList->getMsgBody(1);
  for (int i=0; i<outList->getMsgLength(1); i++) {
    out.push_back(outBody[i]);
  }
  deleteOutList();
}

void NetRodModule::writeFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &in) {
  int32_t* rwfData = new int32_t[sizeof(RwFifoIn)/sizeof(uint32_t) + nWords];
  RwFifoIn *RwFifo_In = (RwFifoIn*) rwfData;
  RwFifo_In->fifoId   = fifo;
  RwFifo_In->fRead    = RW_WRITE;
  RwFifo_In->length    = nWords;
  RwFifo_In->dataPtr   = (uint32_t)NULL;

  int i;
  for (i=0; i<nWords; i++) rwfData[sizeof(RwFifoIn)/sizeof(uint32_t)+i] = in[i];
  RodPrimitive* rwfPrim = new RodPrimitive(sizeof(RwFifoIn)/sizeof(uint32_t) + nWords,
					   0, RW_FIFO, R_BOGUS, rwfData);
  executeMasterPrimitiveSync(*rwfPrim);
  delete rwfPrim;
  delete[] rwfData;
}

#if 1
void NetRodModule::writeRegister(int regId, int offset, int size, unsigned int value) {
  RwRegFieldIn 	RwRegField_In;
  RwRegField_In.regAddr    = regId;
  RwRegField_In.offset     = offset;
  RwRegField_In.width      = size;
  RwRegField_In.flags      = RW_WRITE;
  RwRegField_In.value      = value;
  RodPrimitive* rwrPrim = new RodPrimitive(sizeof(RwRegFieldIn)/sizeof(uint32_t),
					   0, RW_REG_FIELD, R_BOGUS,
					   (int32_t*)&RwRegField_In);
  //schsu: There was a 100ms sleep between RodPrimList::EXECUTING and RodPrimList::WAITING
  //It slows down the register write access.
  //executeMasterPrimitiveSync(*rwrPrim);

   //Since RW_WRITE primitive has relative smaller size.
   //No oberservation of failure by removing the 100ms sleep.
   executeMasterPrimitive(*rwrPrim);

   RodPrimList::PrimState returnPState;
   int count = 0;
   clock_t startTime;

   startTime = time(0);
   count = 0;
   do {
     if ((time(0)-startTime)>50 && count>20) {
       throw RodException("Cannot start primitive execution, TIMEOUT=50", m_slot);
     }
     returnPState = primHandler();
     count++;
   } while (returnPState != RodPrimList::EXECUTING);

   startTime = time(0);
   count = 0;
   do {
     if ((time(0)-startTime)>100 && count>20) {
       throw RodException("Primitive execution did not complete, TIMEOUT=100", m_slot);
     }
     returnPState = primHandler();
     count++;
   } while ((returnPState != RodPrimList::WAITING)&&(returnPState != RodPrimList::IDLE));

   if (returnPState == RodPrimList::WAITING) {
     getOutList();
     deleteOutList();
   }

   delete rwrPrim;
}

unsigned int NetRodModule::readRegister(int regId, int offset, int size) {
  RwRegFieldIn 	RwRegField_In;
  RwRegField_In.regAddr    = regId;
  RwRegField_In.offset     = offset;
  RwRegField_In.width      = size;
  RwRegField_In.flags      = RW_READ;
  RwRegField_In.value      = (uint32_t)NULL;
  RodPrimitive* rwrPrim = new RodPrimitive(sizeof(RwRegFieldIn)/sizeof(uint32_t),
					   0, RW_REG_FIELD, R_BOGUS,
					   (int32_t*)&RwRegField_In);
  //std::vector<unsigned int> outs;
  RodOutList* outs;
  executeMasterPrimitiveSync(*rwrPrim, outs);
  delete rwrPrim;
  RwRegFieldOut *reply = (RwRegFieldOut *)outs->getMsgBody(1);
  return reply->value;
}
#endif

void NetRodModule::executeMasterPrimitive(RodPrimitive &prim) {
  RodPrimList primList(1);
  primList.insert(primList.begin(), prim);
  primList.bufferBuild();
  sendPrimList(&primList);
}

void NetRodModule::executeMasterPrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out) {
  executeMasterPrimitive(prim);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeMasterPrimitiveSync(RodPrimitive &prim, RodOutList* &out) {
  executeMasterPrimitive(prim);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeMasterPrimitiveSync(RodPrimitive &prim) {
  executeMasterPrimitive(prim);
  waitPrimitiveCompletion();
}

void NetRodModule::executeSlavePrimitive(RodPrimitive &prim, int slave) {
  int i;

  RodPrimList slvPrimList(1);
  slvPrimList.insert(slvPrimList.begin(), prim);
  slvPrimList.bufferBuild();

  int32_t buffLength = slvPrimList.getBufferLength();
  uint32_t* slavePrimList = new uint32_t[buffLength];
  uint32_t* bufferStart = slvPrimList.getBuffer();
  for (i=0; i< buffLength; i++) {
    slavePrimList[i] = bufferStart[i];
  }

  // Create Send Slave List primitive
  RodPrimList primList(1);
  int32_t *slaveData = new int32_t[sizeof(SendSlaveListIn)/sizeof(uint32_t) +
				     buffLength];
  SendSlaveListIn* SendSlaveList_In = (SendSlaveListIn*)slaveData;
  SendSlaveList_In->slave         = slave;
  SendSlaveList_In->nWords        = buffLength;
  SendSlaveList_In->queueIndex    = 0;

  for (i=0; i< buffLength; i++) {
    slaveData[i+sizeof(SendSlaveListIn)/4] = slavePrimList[i];
  }
  delete slavePrimList;
  RodPrimitive* send;
  send = new RodPrimitive(sizeof(SendSlaveListIn)/sizeof(uint32_t) + buffLength,
			  0, SEND_SLAVE_LIST, R_BOGUS, slaveData);
  primList.insert(primList.begin(), *send);

  primList.bufferBuild();
  sendPrimList(&primList);
  delete send;
  delete []slaveData;

}

void NetRodModule::executeSlavePrimitiveOnAll(RodPrimitive &prim) {

  RodPrimList slvPrimList(1);
  slvPrimList.insert(slvPrimList.begin(), prim);
  slvPrimList.bufferBuild();

  int32_t buffLength = slvPrimList.getBufferLength();
  uint32_t* slavePrimList = new uint32_t[buffLength];
  uint32_t* bufferStart = slvPrimList.getBuffer();
  for (int i=0; i< buffLength; i++) {
    slavePrimList[i] = bufferStart[i];
  }

  // Create Send Slave List primitive
    RodPrimList primList(1);

    int32_t *slaveData = new int32_t[sizeof(SendSlaveListIn)/sizeof(uint32_t) + buffLength];
    SendSlaveListIn* SendSlaveList_In = (SendSlaveListIn*)slaveData;
    SendSlaveList_In->slave       = SLAVE_MASK | 0xf;
    SendSlaveList_In->nWords      = buffLength;
    SendSlaveList_In->nPrimitives = 1;
    SendSlaveList_In->queueIndex  = 0;

    for (int i=0; i< buffLength; i++) {
      slaveData[i+sizeof(SendSlaveListIn)/sizeof(uint32_t)] = slavePrimList[i];
    }
    RodPrimitive* send;
    send = new RodPrimitive(sizeof(SendSlaveListIn)/sizeof(uint32_t)+buffLength,
			    0, SEND_SLAVE_LIST, R_BOGUS, slaveData);
    primList.insert(primList.begin(), *send);

    primList.bufferBuild();

    delete slavePrimList;
    sendPrimList(&primList);
    delete send;
}

void NetRodModule::executeSlavePrimitiveOnAllSync(RodPrimitive &prim) {
  executeSlavePrimitiveOnAll(prim);
  waitPrimitiveCompletion();
}

void NetRodModule::executeSlavePrimitiveOnAllSync(RodPrimitive &prim, std::vector<unsigned int> &out) {
  executeSlavePrimitiveOnAll(prim);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeSlavePrimitiveOnAllSync(RodPrimitive &prim, RodOutList* &out) {
  executeSlavePrimitiveOnAll(prim);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeSlavePrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out, int slave) {
  executeSlavePrimitive(prim, slave);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeSlavePrimitiveSync(RodPrimitive &prim, RodOutList* &out, int slave) {
  executeSlavePrimitive(prim, slave);
  waitPrimitiveCompletion(out);
}

void NetRodModule::executeSlavePrimitiveSync(RodPrimitive &prim, int slave) {
  executeSlavePrimitive(prim, slave);
  waitPrimitiveCompletion();
}

void NetRodModule::waitPrimitiveCompletion() {
  RodPrimList::PrimState returnPState;
  int count = 0;
  clock_t startTime;

  // wait until previous primlist is finished
  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>50 && count>20) {
      throw RodException("Cannot start primitive execution, TIMEOUT=50", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while (returnPState != RodPrimList::EXECUTING);

  //schsu: 100ms sleep is selected from several experimental trials
  //In the case of NetRodModule::loadAndStart or the RodPixController::writeModuleConfig
  //both has large size of primitives. The primHandler() can sometimes returns EXECUTING
  //state without this 100ms delay
  sleep(100);

  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>100 && count>20) {
      throw RodException("Primitive execution did not complete, TIMEOUT=100", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while ((returnPState != RodPrimList::WAITING)&&(returnPState != RodPrimList::IDLE));
  if (returnPState == RodPrimList::WAITING) {
    getOutList();
    deleteOutList();
  }
}

void NetRodModule::waitPrimitiveCompletion(std::vector<unsigned int> &out) {
  RodPrimList::PrimState returnPState;
  int count = 0;
  clock_t startTime;

  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>50 && count>20) {
      throw RodException("Cannot start primitive execution, TIMEOUT=50", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while (returnPState != RodPrimList::EXECUTING);

//  sleep(100);

  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>100 && count>20) {
      throw RodException("Primitive execution did not complete, TIMEOUT=100", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while ((returnPState != RodPrimList::WAITING)&&(returnPState != RodPrimList::IDLE));
  RodOutList* outList = getOutList();
  out.clear();
  if (returnPState == RodPrimList::WAITING) {
    uint32_t *outBody = outList->getBody();
    for (int i=0; i<outList->getLength(); i++) {
      out.push_back(outBody[i]);
    }
    deleteOutList();
  }
}

void NetRodModule::waitPrimitiveCompletion(RodOutList* &out) {
  RodPrimList::PrimState returnPState;
  int count = 0;
  clock_t startTime;

  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>50 && count>20) {
      throw RodException("Cannot start primitive execution, TIMEOUT=50", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while (returnPState != RodPrimList::EXECUTING);

//  sleep(100);

  startTime = time(0);
  count = 0;
  do {
    if ((time(0)-startTime)>100 && count>20) {
      throw RodException("Primitive execution did not complete, TIMEOUT=100", m_slot);
    }
    returnPState = primHandler();
    count++;
  } while ((returnPState != RodPrimList::WAITING)&&(returnPState != RodPrimList::IDLE));
  out = getOutList();
  if (returnPState != RodPrimList::WAITING) {
    deleteOutList();
    out = NULL;
  }
}

#if 1
void NetRodModule::initStatusRead() {

    StartTaskIn *startTaskIn = (StartTaskIn*) new uint32_t[((sizeof(StartTaskIn))/sizeof(uint32_t))];

    startTaskIn->id = STATUS_READ;
    startTaskIn->idMinor = 0;
    startTaskIn->where   = DSP_THIS;
    startTaskIn->dataPtr = 0;

    RodPrimitive* startStatusRead;
    try {
      startStatusRead = new RodPrimitive(((sizeof(StartTaskIn))/sizeof(uint32_t)), 0, START_TASK, 0, (int32_t*)startTaskIn);
    }
    catch (std::bad_alloc) {
      std::cout << "Unable to allocate startStatusRead primitive in main." << std::endl;
    }
    executeMasterPrimitiveSync(*startStatusRead);
    delete startStatusRead;
    delete []startTaskIn;

}
#endif
QuickStatusInfo* NetRodModule::talkStatusRead() {

    TalkTaskIn *talkTaskIn = (TalkTaskIn*) new uint32_t[((sizeof(TalkTaskIn))/sizeof(uint32_t))];

    talkTaskIn->task = STATUS_READ;
    talkTaskIn->topic = 0;
    talkTaskIn->item = 0;

    RodPrimitive* talkStatusRead;
    try {
      talkStatusRead = new RodPrimitive(((sizeof(talkTaskIn))/sizeof(uint32_t)), 0, TALK_TASK, 0, (int32_t*)talkTaskIn);
    }
    catch (std::bad_alloc) {
      std::cout << "Unable to allocate talkStatusRead primitive in main." << std::endl;
    }

    RodOutList* statusInfoAddress;

    sleep(5);

    executeMasterPrimitiveSync(*talkStatusRead,statusInfoAddress);
    uint32_t* messageBody = (uint32_t*)statusInfoAddress->getMsgBody(1);
    TalkTaskOut *tout = (TalkTaskOut *)messageBody;
    uint32_t address = 0;
    if(0==tout->dataPtr){
      address = messageBody[sizeof(TalkTaskOut)/sizeof(uint32_t)];
    }else{
      address = tout->dataPtr;
    }
    printf("******Address of statusInfo struct: %x \n", address);

    uint32_t numWords = 60;

    uint32_t * buffer = new uint32_t[numWords];
    mdspBlockRead(address, buffer, numWords);
    QuickStatusInfo* quickStatusInfo = (QuickStatusInfo*)buffer;

    delete talkStatusRead;
    delete []talkTaskIn;

    return quickStatusInfo;

}

} //  End namespace SctPixelRod

//------------------------- Overload insertion operators "<<" -------------------------
/* This overloaded operator lets us use cout to print the status of the ROD
*/
  std::ostream& operator<<(std::ostream& os, SctPixelRod::NetRodModule& rod) {
    os << "Slot: " << rod.getSlot() << std::endl;
    os << "Serial Number(hex):" << std::hex << rod.getSerialNumber() << std::endl;
    os << "Number of slave DSPs: " << rod.getNumSlaves() << std::endl;

    os << "Primitive state: " << rod.getPrimState()
       << std::dec << std::endl;
    return os;
  }


// AKU
void dumpXface(MasterXface *mxf){
    std::cout << std::endl << "Dumping Master XFACE" << std::endl;

	std::cout << "nTextBuffers " << mxf->xface.nTextBuffers << std::endl;
	std::cout << "nTextBuffer sots" << mxf->xface.nTextBufferSlots << std::endl;
	std::cout << "nTextBuffer " << mxf->xface.textBuffer << std::endl;

	std::cout << "nPrimitives " << mxf->xface.nPrimitives << std::endl;
	std::cout << "primities " << mxf->xface.primitives << std::endl;

/* server side */
	 std::cout << "nrequestchannels " << mxf->xface.nRequestChannels << std::endl;
	 std::cout << "requestchannel " << mxf->xface.requestChannel << std::endl;

	 std::cout << "reg chan ctl 0 " << mxf->xface.requestChannelControl[0] << std::endl;
	 std::cout << "reg chan ctl 1 "<< mxf->xface.requestChannelControl[1] << std::endl;
	 std::cout << "requestChannelStatus 0 " << mxf->xface.requestChannelStatus[0] << std::endl;
	 std::cout << "requestChannelStatus 1 " << mxf->xface.requestChannelStatus[1] << std::endl;
	 std::cout << "requestChannelLoc 0 " << mxf->xface.requestChannelLoc[0] << std::endl;
	 std::cout << "requestChannelLoc 1 " << mxf->xface.requestChannelLoc[1] << std::endl;
	 std::cout << "requestChannelLen 0 " << mxf->xface.requestChannelLen[0] << std::endl;
	 std::cout << "requestChannelLen1 " << mxf->xface.requestChannelLen[1] << std::endl;
	 std::cout << "requestChannelReplyBufferLoc0 " << mxf->xface.requestChannelReplyBufferLoc[0] << std::endl;
	 std::cout << "requestChannelReplyBufferLoc1 " << mxf->xface.requestChannelReplyBufferLoc[1] << std::endl;
	 std::cout << "requestChannelReplyBufferLen0 " << mxf->xface.requestChannelReplyBufferLen[0] << std::endl;
	 std::cout << "requestChannelReplyBufferLen1 " << mxf->xface.requestChannelReplyBufferLen[1] << std::endl;

/* client side */
	 std::cout << "nCommandChannels " << mxf->xface.nCommandChannels << std::endl;
	 std::cout << "commandChannel " << mxf->xface.commandChannel << std::endl;

    /*
	 std::cout << commandChannelControl[2];
	 std::cout << commandChannelStatus[2];
	 std::cout << commandChannelLoc[2];
	 std::cout << commandChannelLen[2];
	 std::cout << commandChannelReplyBufferLoc[2];
	 std::cout << commandChannelReplyBufferLen[2];

	 std::cout << nGpControl;
	 std::cout << gpControl;
	 std::cout << nGpStatus;
	 std::cout << gpStatus;

	 std::cout << debugStruct;
*/
}


