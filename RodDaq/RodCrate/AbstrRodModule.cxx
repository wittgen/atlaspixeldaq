#include "AbstrRodModule.h"

// implementation of general stuff used both by RodModule and NetRodModule

namespace SctPixelRod {

//******************************Class NoImageFile**************************
//
// Description:
//  This class is thrown if If we try to load a binary image for a master or 
//  slave DSP and the file is not found
//   
//  Author(s):
//    Tom Meyer (meyer@iastate.edu) - originator
//	Constructors. Use defaults for destructor, copy, and assignment.

NoImageFile::NoImageFile(std::string descriptor, std::string fileName) : BaseException(descriptor) {
  m_fileName = fileName;
  setType(NOIMAGEFILE);
  }
  
void NoImageFile::what(std::ostream& os)  const {
  os << "NoImageFile Exception. Descriptor, File name: " << getDescriptor() << ";" << m_fileName << std::endl;
}
  
//******************************Class HpiException**************************
//
// Description:
//  This class is thrown if an error in an HPI read/write operation is detected.
//   
//  Author(s):
//    Tom Meyer (meyer@iastate.edu) - originator

//	Constructors. Use defaults for destructor, copy, and assignment.

HpiException::HpiException( std::string descriptor, uint32_t calcAddr,
                            uint32_t readAddr) : BaseException(descriptor) {
  m_calcAddr = calcAddr;
  m_readAddr = readAddr;
  setType(HPI);
  }

void HpiException::what(std::ostream& os) const {
    os << "HpiException: " << getDescriptor() << std::endl;
    os << "Calculated Address:" << std::hex << getCalcAddr() << std::endl;
    os << "Read Address:" << std::hex << getReadAddr() << std::endl;
}  
//***************************Class RodException**************************
//
// Description:
//  This class is thrown if an error in a ROD operation is detected.
//   
//  Author(s):
//    Tom Meyer (meyer@iastate.edu) - originator

//	Constructors. Use defaults for destructor, copy, and assignment.

RodException::RodException( std::string descriptor) : BaseException(descriptor) {
  m_numData = 0;
  m_data1 = m_data2 = 0;
  setType(ROD);
  }
RodException::RodException( std::string descriptor, uint32_t data1) :
                            BaseException(descriptor) {
  m_numData = 1;
  m_data1 = data1;
  m_data2 = 0;
  setType(ROD);
  }
RodException::RodException( std::string descriptor, uint32_t data1,
                            uint32_t data2) : BaseException(descriptor) {
  m_numData = 2;
  m_data1 = data1;
  m_data2 = data2;
  setType(ROD);
  }

void RodException::what(std::ostream& os) const {
  uint32_t numData;
  numData = getNumData();
  os << "RodException: " << getDescriptor() << std::endl;
  if (0 == numData) return;
  os << "Data1:" << getData1() << std::endl;
  if (1 == numData) return;
  os << "Data2:" << getData2() << std::endl;
}  

}
