//File: TimModule.h

#ifndef SCTPIXELROD_TIMMODULE_H
#define SCTPIXELROD_TIMMODULE_H

/*! \file
 * \brief TimModule: A derived class for VME TIM modules.
 *
 * This file declares a TIM class derived from the VmeModule base class.
 *
 * Contributors: John Lane <jbl@hep.ucl.ac.uk> - originator
 *
 * $Id$
 *
 * $Log$
 * Revision 1.12.4.1  2008/02/18 18:10:42  akorn
 * initial release of main differences for New DSP merged code
 *
 * Revision 1.12  2005/11/18 17:44:50  jchill
 * Revert to single enum
 *
 * Revision 1.11  2005/11/18 16:47:07  jchill
 * Extra primitives defined to allow for TimRegister and TimRegister3
 *
 * Revision 1.10  2005/05/19 13:16:20  jchill
 * Changes for TIM 3C
 *
 * Revision 1.9  2004/10/01 22:44:33  jbl
 * TimModule new methods
 *
 * Revision 1.8  2004/07/29 20:08:56  jbl
 * TimModule minor bug fixes
 *
 * Revision 1.7  2003/12/04 19:10:49  jbl
 * TimModule uses BaseException
 *
 * Revision 1.6  2003/06/04 15:04:47  tmeyer
 * Removed explicit directory structure from includes
 *
 * Revision 1.5  2003/05/20 19:26:25  jbl
 * TimModule uint8_t & uint16_t
 *
 * Revision 1.4  2002/12/11 21:30:50  jbl
 * TimModule major update
 *
 *
 *
 * NB define eg I_AM_LINUX_HOST for processor.h (eg typedef uint32_t)
 */

#include "TimDefine.h"
#include "VmeInterface.h"
#include "VmePort.h"
#include "VmeModule.h"
#include "BaseException.h"

#include <cstdint>
namespace SctPixelRod {

// ------------------------- TimException ------------------------------------

//! TimException: a general exception class for TIM errors.
/*!
  This class is thrown if an error in a TIM operation is detected.

  Contributors: John Lane <jbl@hep.ucl.ac.uk> - originator
*/

class TimException : public BaseException {

public:

  TimException( std::string descriptor, int data1, int data2 );
  int         getData1()   const   { return m_data1; };
  int         getData2()   const    { return m_data2; };
  virtual void what( std::ostream & ) const;

private:

  int         m_data1;       //!< First  data value returned
  int         m_data2;       //!< Second data value returned
};

// ------------------------- TimModule ---------------------------------------

//! TimModule: A derived class for VME TIM modules.
/*!
  This is the implementation of a TIM class derived from the VmeModule base
  class. It should be the sole interface for VME communication with TIM.

  Contributors: John Lane <jbl@hep.ucl.ac.uk> - originator
*/

class TimModule : public VmeModule {

public:

  // Constructor and destructor
  // There are no copy or assignment operators due to VME restrictions.

  TimModule( uint32_t baseAddr, uint32_t mapSize, VmeInterface & ourInterface );

  virtual ~TimModule();

  // Accessor functions

  uint32_t   getFirmware()     { return m_firmware; };
  uint32_t   getSerialNumber() { return m_serialNumber; };
  VmePort* getVmePort()      { return m_vmePort; };

  // Methods

  void initialize();
  void reset() ;
  void status();

  uint32_t fetchL1ID();                     //!< Read trigger number
  uint16_t fetchTimID();                    //!< Read TIM ID register

  void intTrigStart( const double frequency );           //!< Internal Trigger
  void intTrigStart( const TimMaskFrequency frequency ); //!< Internal Trigger
  void intTrigStop();                               //!< Stop Internal Trigger

  void issueCommand( const TimBitCommand mask );  //!<       Issue TIM command
  void issueVCAL( const uint8_t pipelineDelay );   //!< Issue VCAL + L1A command

  void loadBitClear( const TimRegister addr, const uint16_t mask );//!< Clear bit
  void loadBitSet( const TimRegister addr,   const uint16_t mask );//!<   Set bit
  void loadByteHi( const TimRegister addr, const uint8_t byte ); //!< Upper byte
  void loadByteLo( const TimRegister addr, const uint8_t byte ); //!< Lower byte
  void loadFrequencyFECR( const double frequency ); //!< Load FER/ECR frequency
  void loadFrequencyTrig( const double frequency ); //!< Load trigger frequency

  void msleep( const double milliSecs );

  void ttcRxWrite(const uint8_t  address, const uint8_t data);
  uint8_t ttcRxRead(const uint8_t address);

  uint16_t regFetch( const TimRegister addr );
                                          //!< Read from a 16-bit VME register

  void   regLoad( const TimRegister addr, const uint16_t data );
                                          //!<  Write to a 16-bit VME register
  int  regTimeout( const TimRegister addr, const int mask1, const int mask2,
                   const int timeout );

  void seqFetch( const uint16_t size,      uint16_t buffer[] ); //!< Read sequencer
  void seqLoad( const uint16_t size, const uint16_t buffer[] ); //!< Load sequencer
  void seqRun(  const uint16_t size );                        //!< Run  sequencer

  void setupTTC();                                     //!< Setup TTC operation
  void setupVME();                                     //!< Setup VME operation

  void statusPrint( std::ostream& os );                //!< Print TIM status

  uint16_t vmeFetch( const uint32_t addr )
        ;          //!< Read from a 16-bit VME register

  void   vmeLoad(  const uint32_t addr, const uint16_t data )
        ;          //!<  Write to a 16-bit VME register

private:

  uint32_t   m_firmware;     //!< Firmware version number
  uint32_t   m_serialNumber; //!< Board serial number
  VmePort* m_vmePort;      //!< VME Port handle
  uint8_t ttcRxAccess(bool write, const uint8_t address, const uint8_t data);
}; // End class TimModule declaration

}  // End namespace SctPixelRod

// ------------------------- Overload operator<< -----------------------------

//! Overloaded operator to print TIM status

namespace std {

ostream& operator<<( ostream& os, SctPixelRod::TimModule& tim );

}  // End namespace std

#endif // SCTPIXELROD_TIMMODULE_H
