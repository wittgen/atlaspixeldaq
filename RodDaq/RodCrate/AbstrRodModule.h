//File: AbstrRodModule.h

#ifndef SCTPIXELROD_ABSTRRODMODULE_H 
#define SCTPIXELROD_ABSTRRODMODULE_H

#include <fstream> 
#include <iostream>
#include <string>
#include <vector>

#include "BaseException.h"
#include "RodPrimitive.h" 
#include "RodPrimList.h"
#include "RodDspAddresses.h"
#include "RodVmeAddresses.h"
#include "RodOutList.h"
#include "rodXface.h"
#include "VmeInterface.h" 
#include "VmePort.h"
#include "VmeModule.h"

namespace SctPixelRod  {
enum HpidMode {DYNAMIC, AUTO, NO_AUTO};

//------------------------------NoImageFile--------------------------------- 

/*! @class NoImageFile
 *
 *  @brief This is an exception class for when a binary image file is not found.
 *  It inherits from BaseException, adding the name of the missing file as a data member.
 *
 *  If we try to load a binary image for a master or slave DSP and the file is 
 *  not found, an object of this class is thrown.
 *
 *  @author Tom Meyer (meyer@iastate.edu) - originator
 */

class NoImageFile : public BaseException {
public:
  NoImageFile( std::string descriptor, std::string fileName);
  std::string getFileName() const {return m_fileName;};
  virtual void what(std::ostream&) const;

private:
  std::string m_fileName;
};                                                                             

//------------------------------HpiException--------------------------------- 

/*! @class HpiException
 *
 *  @brief This is an exception class for Host Port Interface errors.
 *  It inherits from BaseException, adding the expected and actual addresses.

 *  This class is thrown if an error in an HPI read/write operation is detected.
 *
 *  @author Tom Meyer (meyer@iastate.edu) - originator
 */

class HpiException : public BaseException {
public:
  HpiException( std::string descriptor, uint32_t calcAddr, uint32_t readAddr);
  uint32_t getCalcAddr() const {return m_calcAddr;};
  uint32_t getReadAddr() const {return m_readAddr;};
  virtual void what(std::ostream&) const;

private:
  uint32_t m_calcAddr;    // The calculated (expected) address
  uint32_t m_readAddr;    // The address actually read
};                                                                            

//------------------------------RodException------------------------------ 

/*! @class rodException
 *
 *  @brief This is a general exception class for ROD errors.
 *
 *  This class is thrown if an error in a ROD operation is detected.
 *  It inherits from BaseException, adding zero, one, or two parameters.
 *
 *  @author Tom Meyer (meyer@iastate.edu) - originator
 */

class RodException : public BaseException {
public:
  RodException( std::string descriptor);
  RodException( std::string descriptor, uint32_t data1);
  RodException( std::string descriptor, uint32_t data1, uint32_t data2);
  uint32_t getData1() const {return m_data1;};
  uint32_t getData2() const {return m_data2;};
  uint32_t getNumData() const {return m_numData;};
  virtual void what  (std::ostream&) const;
  
private:
  uint32_t m_numData;  // Number of data values returned
  uint32_t m_data1;    // First data value returned
  uint32_t m_data2;    // Second data value returned
};                                                                            


enum FpgaType {CONTROLLER, FMTA, FMTB, EFR, ROUTER, ALL};
enum DspType {MODE, MASTER, SLV0, SLV1, SLV2, SLV3};

//------------------------------------------------------------------------------              
//! AbstrRodModule: 
/*! @class AbstrRodModule
 *
 *  @brief abstract class to allow parallel use of RodModule and NetRodModule for IBL ROD
 *
 *  This class provides an abstract interface to RodModule (VME-based) and
 *  NetRodModule (network-based) in order to equally use both implementation
 *  for the IBL ROD PixController (and other applications?)
 *  It simply defines all functions previously defined in RodModule so that
 *  the PixController doesn't have to care about the flavour after having called
 *  the constructor
 *
 *  @author Joern Grosse-Knetter
 */

 class AbstrRodModule {

 public:

  virtual ~AbstrRodModule(){};

  virtual bool isSane() = 0; 
  virtual uint32_t fmtVersion() = 0;
  virtual uint32_t efbVersion() = 0;
  virtual uint32_t rtrVersion() = 0;
  virtual uint32_t rcfVersion() = 0;

  virtual uint32_t getBaseAddress() = 0;
  uint32_t getVmeCommandRegVal() {return m_vmeCommandReg;};
  VmePort* getVmePort() {return m_myVmePort; };
    
  void setDebug(unsigned int debug) {m_debug = debug; }
  unsigned int getDebug() {return m_debug; }
  void setDebugFile(const std::string &debugFile) {m_debugFile = debugFile; }
  int32_t getSlot() const { return m_slot; }
  uint32_t getSerialNumber() {return m_serialNumber;};
  uint32_t getRevision() {return m_revision;};
  void setNumSlaves( int32_t numSlaves) { m_numSlaves = numSlaves; }
  int32_t getNumSlaves() const { return m_numSlaves; }
  void setFinBufferSize( int32_t finBufferSize) { m_finBufferSize = finBufferSize;}
  int32_t getFinBufferSize() const { return m_finBufferSize; }
  std::string getMasterImageName() {return m_masterImageName; }
  void setOutList(RodOutList* outList) {m_myOutList = outList;}
  RodOutList* getOutList() {return m_myOutList;};
  RodPrimList::PrimState getPrimState() {return m_myPrimState;}
  uint32_t getBocFlag() {return m_bocFlag; }

/*! initialize() performs all actions necessary to place a properly loaded ROD 
in its initialized state, except for initializing the slave DSPs. This must be done
separately using initSlaveDsp().

Activities:
    Reset the ROD
    Read ROD serial number
    Load 0x000l000l into Master DSP HPIC register. (Says low order 16-bit word
                                                    comes first)
    Read back HPIC to verify
    Retrieve pointers to text buffer structs
    Initialize PrimList and TextBuff state variables
*/
  virtual void initialize() = 0;
  virtual void initialize(bool resetFlag) = 0;

/*! reset() issues a reset signal to the ROD, clears IDRAM, resets all DSPs, and
 *  re-initializes all state variables.
*/
  virtual void reset() = 0;

/*! fpgaReset() resets individual FPGAs on the ROD. The ROD can reset only one or all at a time
 */
  virtual void fpgaReset(FpgaType targetFpga) = 0;

/*! dspReset() resets individual DSPs on the ROD. The ROD can reset only one or all at a time
 */
  virtual void dspReset(DspType targetDsp) = 0 ;

/*! status() reports the status of the ROD.
 *  For now, it simply prints to standard output. Later we will add a structured
 *  report of name=value pairs.
*/
  virtual void status() = 0;

/*! verify checks the h.o. byte of the serial number register to see that it contains
 *  the hex value 'ad', which is the signature of a ROD. It returns true if this is\
 *  the case, false otherwise.
*/
  virtual bool verify() = 0;
  
/*! loadAndStartSlaves() reads a binary file from disk and loads it into the slave
 *  memory space and boots up the specified Slaves
 *  !!!THIS IS THE PREFERRED WAY TO LOAD A SLAVE IMAGE!!! - do not use
 *  hpiLoadSlaveImage without having a good reason to do so.
 */
  virtual int loadAndStartSlaves(const std::string & filename, 
				 const int slaveMask) = 0;
  virtual int loadAndStartFpgaSlaves(const std::string & filename, 
				 const int slaveMask) = 0;

/*! startSlave() just starts the slave and checks that it has booted
 */

  virtual int startSlave(const int slaveId) = 0;


/*! hpiLoad() loads a 32-bit value into the HPIA or HPID register. Although 
 *  the HPI has a 16-bit data path, we use a 32-bit VME access and the word is 
 *  broken into two 16-bit values in the ROD firmware.
*/
  virtual void hpiLoad(const uint32_t hpiReg, const uint32_t hpiValue) = 0;

/*! hpiFetch() fetches a 32-bit value from one of the HPI registers. Although 
 *  the HPI has a 16-bit data path, we use a 32-bit VME access, with the ROD 
 *  firmware doing the necessary conversion.
*/
  virtual uint32_t hpiFetch(const uint32_t hpiReg)= 0;

/*! mdspSingleRead() reads a single 32-bit word from the Master DSP SDRAM via 
 *  its Host Port Interface (HPI).
*/
  virtual uint32_t mdspSingleRead(const uint32_t dspAddr) = 0;

/*! mdspSingleWrite() writes a single 32-bit word to the Master DSP SDRAM via 
 *  its Host Port Interface (HPI).
*/
  virtual void mdspSingleWrite(uint32_t dspAddr, uint32_t buffer) = 0;

/*! mdspBlockRead() reads a block of size wordCount 32-bit words from the Master 
 *  DSP SDRAM via its Host Port Interface (HPI). 
 *  The present version uses a loop over single word reads instead of a block
 *  read.
*/
  virtual void mdspBlockRead(const uint32_t dspAddr, uint32_t *buffer, 
			     int32_t wordCount, HpidMode mode=DYNAMIC) = 0;

/*! mdspBlockWrite() writes a buffer of wordCount 32-bit words to the Master DSP 
 *  SDRAM via its Host Port Interface (HPI).
 *  The present version uses a loop over single word writes instead of a block
 *  write.
*/
  virtual void mdspBlockWrite(const uint32_t dspAddr, uint32_t *buffer, 
			      int32_t wordCount, HpidMode mode=DYNAMIC) = 0;

/*! mdspBlockDump() reads a block of MDSP memory and write it to a binary file 
 *  (debug use only)
 */
  virtual void mdspBlockDump(const uint32_t firstAddress, 
			     const uint32_t lastAddress, const std::string & fileName) = 0;

/*! synchSendPrim() synchronously sends a single primitive to the MDSP and 
 *  handles the calls to primHandler() to finish the transfer before returning 
 *  control to its calling routine.  It is not the normal way to send 
 *  primitives, but is useful for initialization where time criticality is not 
 *  so much of a factor.
 *
 *  The present version writes to cout. This needs to be changed.
 */
  virtual void synchSendPrim(RodPrimitive & prim) = 0;

/*! sendPrimList and sendSlavePrimList send a PrimList to the respective buffers
 */
  virtual void sendPrimList(RodPrimList *l) = 0;
  virtual void sendSlavePrimList(RodPrimList *slvPrimList, int slave) = 0;

/*! validateReplyBuffer() checks the consistency of the reply buffer */
  virtual int validateReplyBuffer() = 0;

/*! primHandler() handles steps 2-4 in sending a primitive list to
 *  the master DSP. Its action depends on the value of the m_myPrimState state
 *  variable.
 */
  virtual RodPrimList::PrimState primHandler() = 0;
  
//! Delete reply buffer object
  virtual void deleteOutList() = 0;

/*! getTextBuffer() reads a complete text buffer into the return 
 *  string rettxtBuff. buffnum selects  0 for ERR buffer, 1 for INFO buffer. 
 *  If reset is true, the textBuffers on the DSP are emptied after 
 *  beeing read.
 */
  virtual bool getTextBuffer(std::string &rettxtBuf, unsigned int buffnum, bool reset) = 0 ;

  virtual void readMasterMem(int startAddr, int nWords, std::vector<unsigned int> &out) = 0;
  virtual void writeMasterMem(int startAddr, int nWords, std::vector<unsigned int> &in) = 0;
  virtual void readSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) = 0;
  virtual void writeSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) = 0;
  virtual void readSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) = 0;
  virtual void writeSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) = 0;
  virtual void readFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &out) = 0;
  virtual void writeFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &in) = 0;
  virtual void writeRegister(int regId, int offset, int size, unsigned int value) = 0;
  virtual unsigned int readRegister(int regId, int offset, int size) = 0;
  virtual void executeMasterPrimitive(RodPrimitive &prim) = 0;
  virtual void executeMasterPrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out) = 0;
  virtual void executeMasterPrimitiveSync(RodPrimitive &prim, RodOutList* &out) = 0;
  virtual void executeMasterPrimitiveSync(RodPrimitive &prim) = 0;
  virtual void executeSlavePrimitive(RodPrimitive &prim, int slave) = 0;
  virtual void executeSlavePrimitiveOnAll(RodPrimitive &prim) = 0;
  virtual void executeSlavePrimitiveOnAllSync(RodPrimitive &prim) = 0;
  virtual void executeSlavePrimitiveOnAllSync(RodPrimitive &prim, std::vector<unsigned int> &out) = 0;
  virtual void executeSlavePrimitiveOnAllSync(RodPrimitive &prim, RodOutList* &out) = 0;
  virtual void executeSlavePrimitiveSync(RodPrimitive &prim, std::vector<unsigned int> &out, int slave) = 0;
  virtual void executeSlavePrimitiveSync(RodPrimitive &prim, RodOutList* &out, int slave) = 0;
  virtual void executeSlavePrimitiveSync(RodPrimitive &prim, int slave) = 0;
  virtual void waitPrimitiveCompletion() = 0;
  virtual void waitPrimitiveCompletion(std::vector<unsigned int> &out) = 0;
  virtual void waitPrimitiveCompletion(RodOutList* &out) = 0;

  virtual void initStatusRead()= 0;
  virtual QuickStatusInfo* talkStatusRead()= 0;

typedef struct {
    uint32_t tail, head, base, length;
} TextQueue;

protected:
 AbstrRodModule(){};
 AbstrRodModule(int32_t slot , int32_t numSlaves) : m_slot(slot) , m_numSlaves(numSlaves){};

  unsigned int m_debug;              //!< Debug flag (bits of levels)
  std::string m_debugFile;           //!< Debug output filename
  int32_t m_slot;                       //!< Slot number in VME crate
  uint32_t m_serialNumber;      //!< Board serial number (read during init)
  uint32_t m_revision;          //!< Board version (read during init)
  int32_t m_numSlaves;                  //!< Number of slave DSPs
  unsigned int n_textbuffers;        //!< number of available textbuffers
  bool m_btFailure;                  //!< TRUE in case of  block transfer failure
  MasterXface m_masterXface;         //!< Master DSP interface
  VmePort* m_myVmePort;              //!< VME Port handle

  // pointer to buffer array for Text data
  char   **m_textData;
  uint32_t  *m_textBuffer;
  uint32_t  *m_textBufferSize;
     
  //! Size of file input buffer, in bytes. Default=4096 bytes, probably good enough 
  int32_t m_finBufferSize;

  //! Cached copy of VME command registers and ROD status registers
  uint32_t m_vmeCommandReg;
  uint32_t m_rodStatusReg[2];

  //! string holding name of master image file
  std::string m_masterImageName;

  //! A pointer to the array holding a reply buffer from a PrimList
  RodOutList* m_myOutList;
  
  //! State variable for sending primitive lists
  RodPrimList::PrimState m_myPrimState;
      
  //! BOC status flag (0=OK, 1=no clock, 2=busy)
  uint32_t m_bocFlag;

  //! inline bit manipulation functions
  inline void setBit(uint32_t *var, int32_t bitNumber) {
    *var = *var | (1<<bitNumber);
    return;
  }
  
  inline void clearBit(uint32_t *var, int32_t bitNumber) {
    *var&=(~(1<<bitNumber));
    return;
  }
  
  inline bool readBit(uint32_t var, int32_t bitNumber) {
    return ((var>>bitNumber)&1);
  }
    
 };

} // end SctPixelRod namespace
#endif // SCTPIXELROD_ABSTRRODMODULE_H 
