//File: IblRodModule.h

/* 
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 *	    R. Smith <russell.smith@cern.ch>
 * Date: 2014-III-05
 *
 * Description: low-level class to handle the IBL ROD
 * 	This class is based on the RodModulec class but the VmeInterface components were stripped.
 *  It is therefore required to re-introduce low-level ROD flashing mechanisms (cf. functions in RodModule)
 *  Also, the Primitive function call mechanisms have been removed and the 'commmand pattern' is now used.
 * To do items:
 *	* Add VME flashing mechanisms for IBL ROD (check with FW developers)
 *  * Implement PrimList sending through Commands ?
 *  * Update text buffer to get logs back from PPC/MB on ROD.
 *  * Implement HPI/Flash read/write functionality
 *  * Implement read/write PPC/MB memory access
 */

#ifndef SCTPIXELROD_IBLRODMODULE_H 
#define SCTPIXELROD_IBLRODMODULE_H

#include "AbstrRodModule.h"
#include "ServerInterface.h"

#include <ctime>     // Pick up clock() function and clock_t typedef
#include <new>
#include <unistd.h>

namespace SctPixelRod { // Todo: redefine namespaces ? Or keep for historical reasons ?


//------------------------------------------------------------------------------              
//! IblRodModule: 
/*! @class IblRodModule
 *
 *  @brief This is a derived class providing the software interface for ethernet ROD modules.
 *
 *  @author Karolos Potamianos
 */

class IblRodModule : public AbstrRodModule { // Todo: will we need Vme access to the PRM or other ?
    //class IblRodModule { // Todo: will we need Vme access to the PRM or other ?
  IblRodModule (const IblRodModule& rhs);
  IblRodModule &operator=(const IblRodModule & rhs);

public:

  // Constructors, destructor, and assignment
  // IblRodModule() throw (RodException&){};
  //todo do we need all these in the constructor?
  IblRodModule( uint32_t baseAddr, uint32_t mapSize, int32_t numSlaves, uint16_t srvIpPort, std::string ipAddress) ;
  virtual ~IblRodModule();                                  // Destructor
  
  // Accessor functions

  bool isSane(); 
  uint32_t fmtVersion();
  uint32_t efbVersion();
  uint32_t rtrVersion();
  uint32_t rcfVersion();

  ServerInterface* getServer(){return m_server;}  //todo move to AbstractRodModule?
  bool sendCommand(Command &cmd);
  uint32_t getBaseAddress() {return m_baseAddress; }
 
/*! initialize() performs all actions necessary to place a properly loaded ROD 
in its initialized state, except for initializing the slave DSPs. This must be done
separately using initSlaveDsp().

Activities:
    Reset the ROD
    Read ROD serial number
    Load 0x000l000l into Master DSP HPIC register. (Says low order 16-bit word
                                                    comes first)
    Read back HPIC to verify
    Retrieve pointers to text buffer structs
    Initialize PrimList and TextBuff state variables
*/
  void initialize();
  void initialize(bool resetFlag);
  
/*! reset() issues a reset signal to the ROD, clears IDRAM, resets all DSPs, and
 *  re-initializes all state variables.
*/
  void reset();

/*! fpgaReset() resets individual FPGAs on the ROD. The ROD can reset only one or all at a time
 */
  void fpgaReset(FpgaType targetFpga);

/*! dspReset() resets individual DSPs on the ROD. The ROD can reset only one or all at a time
 */
  void dspReset(DspType targetDsp);

/*! status() reports the status of the ROD.
 *  For now, it simply prints to standard output. Later we will add a structured
 *  report of name=value pairs.
*/
  void status();

/*! verify checks the h.o. byte of the serial number register to see that it contains
 *  the hex value 'ad', which is the signature of a ROD. It returns true if this is\
 *  the case, false otherwise.
*/
  bool verify();
  
/*! loadAndStartSlaves() reads a binary file from disk and loads it into the slave
 *  memory space and boots up the specified Slaves
 *  !!!THIS IS THE PREFERRED WAY TO LOAD A SLAVE IMAGE!!! - do not use
 *  hpiLoadSlaveImage without having a good reason to do so.
 */
  int loadAndStartSlaves(const std::string & filename, const int slaveMask) {return -1;};
  int loadAndStartFpgaSlaves(const std::string & filename, const int slaveMask) ;
/*! startSlave() just starts the slaves and checks that it has booted
 */

  int startSlave(const int slaveId);
     
/*! loadSlaveImage() reads a binary file from disk and loads it into the slave
 *  memory space at the specified location using the RW_SLAVE_MEMORY primitive.
 *  !!!THIS IS THE PREFERRED WAY TO LOAD A SLAVE IMAGE!!! - do not use
 *  hpiLoadSlaveImage without having a good reason to do so.
 *  Usually, the optional last argument should be omitted.
 */
  void loadSlaveImage(const std::string & filename, const uint32_t address,
		      const int32_t slaveNumber, char opt='n') ;
                      
/*! hpiLoadSlaveImage() reads a binary file from disk and loads it into the slave
 *  memory space at the specified location.
 *  !!!THIS IS NO LONGER THE PREFERRED WAY. YOU'D BETTER HAVE A GOOD READSON TO USE
 *  IT !!!
 *  Usually, the optional last argument should be omitted.
 */
  void hpiLoadSlaveImage(const std::string & filename, const uint32_t address,
			 const int32_t slaveNumber, char opt='n') ;

                      
/*! startSlave() sends a START_SLAVE_EXECUTING primitive to start a given slave
 *  DSP. Usually, the optional last argument should be omitted.
 */
  //void startSlave(const int32_t slaveNumber, char mode='s') throw(RodException &);
 
// Todo: implement (if we need them) sendPrimList sendSlavePrimList, etc.
 

// Todo: decide how logs are transferred back from the PPC !

/*! getTextBuffer() reads a complete text buffer into the return 
 *  string rettxtBuff. buffnum selects  0 for ERR buffer, 1 for INFO buffer. 
 *  If reset is true, the textBuffers on the DSP are emptied after 
 *  beeing read.
 */
  bool getTextBuffer(std::string &rettxtBuf, unsigned int buffnum, bool reset);
  
// Todo: check pertinence of the hpi-related functions: hpiLoad, hpiFetch, slvHpiLoad, slvHpiFetch; do we need equivalents?

// To check: probably we don't need the mdsp Single/Block functions (master and slave versions)

// To check: keep DSP names or move to MB and PPC ?


/*! resetMasterDsp() resets the Master DSP via FPGA Control Register 2
 */
  void resetMasterDsp();
  
/*! resetSlaveDsp() resets a slave DSP via FPGA Control Register 2
 */
  void resetSlaveDsp(int32_t slaveNumber);
  
/*! resetAllDsps() resets all five DSPs via FPGA Control Register 2
 */
  void resetAllDsps();
  
// Todo: assess whether we need all reset, chiperase and read/write to/from flash functions

/*! sleep() sleeps for a given number of milliseconds
*/
  //void sleep(const double milliSecs);
  inline void sleep(const uint32_t milliSecs){usleep(milliSecs*1000);}
/*! checkSum() calculates a bit-wise XOR checksum on an array
*/
  uint32_t checkSum(const uint32_t *sourceArray, const int32_t wordCount);
  
/*! endianReverse32( reverses the endian-ness of a 32 bit value
*/
  uint32_t endianReverse32(const uint32_t inVal);
  
/*! Checks BOC status. If no BOC clock or BOC is busy, it returns 0; if BOC is OK it returns 1.
*/
  uint32_t bocCheck();
 
// Todo: implement commands for these!
 
void readMasterMem(int startAddr, int nWords, std::vector<unsigned int> &out) ;
void writeMasterMem(int startAddr, int nWords, std::vector<unsigned int> &in) ;
void readSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) ;
void writeSlaveMem(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) ;
//void readSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &out) ;
//void writeSlaveMemDirect(int slaveId, int startAddr, int nWords, std::vector<unsigned int> &in) ;
void readFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &out) ;
void writeFifo(int fifo, int bank, int nWords, std::vector<unsigned int> &in) ;
void writeRegister(int regId, int offset, int size, unsigned int value) ;
unsigned int readRegister(int regId, int offset, int size) ;

// Todo: implement (if neeeded!) QuickStatus functionality within IBL ROD

void initStatusRead();
QuickStatusInfo* talkStatusRead();

//blank implementations of pure virtuals from AbstrRodModule.h
//todo implement the ones that we actually need!

// virtual uint32_t getBaseAddress();
 virtual void hpiLoad(uint32_t, uint32_t){};
 virtual uint32_t hpiFetch(uint32_t){return 0;};
 virtual uint32_t mdspSingleRead(uint32_t){return 0;};
 virtual void mdspSingleWrite(uint32_t, uint32_t){};
 virtual void mdspBlockRead(uint32_t, uint32_t*, int32_t){};
 virtual void mdspBlockWrite(uint32_t, uint32_t*, int32_t){};
 virtual void mdspBlockRead(uint32_t, uint32_t*, int32_t, HpidMode){}; //todo copied since pure virtual in AbstrRodModule
 virtual void mdspBlockWrite(uint32_t, uint32_t*, int32_t, HpidMode){}; //todo should this be empty?  
 virtual void mdspBlockDump(uint32_t, uint32_t, const std::string&){};
 virtual void synchSendPrim(SctPixelRod::RodPrimitive&){};
 virtual void sendPrimList(SctPixelRod::RodPrimList*){};
 virtual void sendSlavePrimList(SctPixelRod::RodPrimList*, int){};
 virtual int validateReplyBuffer(){return 0;};
 virtual RodPrimList::PrimState primHandler(){return RodPrimList::IDLE;};
 virtual void deleteOutList(){};
 virtual void readSlaveMemDirect(int, int, int, std::vector<unsigned int, std::allocator<unsigned int> >&){};
 virtual void writeSlaveMemDirect(int, int, int, std::vector<unsigned int, std::allocator<unsigned int> >&){};
 virtual void executeMasterPrimitive(SctPixelRod::RodPrimitive&){};
 virtual void executeMasterPrimitiveSync(SctPixelRod::RodPrimitive&, std::vector<unsigned int, std::allocator<unsigned int> >&){};
 virtual void executeMasterPrimitiveSync(SctPixelRod::RodPrimitive&, SctPixelRod::RodOutList*&){};
 virtual void executeMasterPrimitiveSync(SctPixelRod::RodPrimitive&){};
 virtual void executeSlavePrimitive(SctPixelRod::RodPrimitive&, int){};
 virtual void executeSlavePrimitiveOnAll(SctPixelRod::RodPrimitive&){};
 virtual void executeSlavePrimitiveOnAllSync(SctPixelRod::RodPrimitive&){};
 virtual void executeSlavePrimitiveOnAllSync(SctPixelRod::RodPrimitive&, std::vector<unsigned int, std::allocator<unsigned int> >&){};
 virtual void executeSlavePrimitiveOnAllSync(SctPixelRod::RodPrimitive&, SctPixelRod::RodOutList*&){};
 virtual void executeSlavePrimitiveSync(SctPixelRod::RodPrimitive&, std::vector<unsigned int, std::allocator<unsigned int> >&, int){};
 virtual void executeSlavePrimitiveSync(SctPixelRod::RodPrimitive&, SctPixelRod::RodOutList*&, int){};
 virtual void executeSlavePrimitiveSync(SctPixelRod::RodPrimitive&, int){};
 virtual void waitPrimitiveCompletion(){};
 virtual void waitPrimitiveCompletion(std::vector<unsigned int, std::allocator<unsigned int> >&){};
 virtual void waitPrimitiveCompletion(SctPixelRod::RodOutList*&){};

private:
 ServerInterface * m_server;
 uint32_t m_baseAddress;
 uint16_t m_srvIpPort;
 std::string m_ipAddress;
    
}; // End of class RodModule declaration

} //  End namespace SctPixelRod

//---------------------------- Overload operator<< -----------------------------
/* This overloaded operator lets us use cout to print the status of the ROD
 * and exception information.
*/
  std::ostream& operator<<(std::ostream& os, SctPixelRod::IblRodModule& rod);

#endif // SCTPIXELROD_IBLRODMODULE_H

