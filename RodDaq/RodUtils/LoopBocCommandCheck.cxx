#include "RCCVmeInterface.h"
#include "RodModule.h"
#include "BocCard.h"
//#include "registerIndices.h"
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <unistd.h>

#define COUNTER_BASE            0x00400090
#define FMT_DIST                0x00000400
#define FMT_CNT_CONTROL(fmt)    COUNTER_BASE + (fmt * FMT_DIST)
#define FMT_GLB_COUNTER(fmt)    COUNTER_BASE + (fmt * FMT_DIST) + 0x00000004
#define FMT_STREAM_01(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000008
#define FMT_STREAM_23(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000000C
// More definitions for SCT convenience
#define FMT_STREAM_45(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000010
#define FMT_STREAM_67(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000014
#define FMT_STREAM_89(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000018
#define FMT_STREAM_AB(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000001C

// Definition to bypass use of registerindices.h
#define FMT_LINK_EN(fmt)              (fmt)
#define FMT_EXP_MODE_EN(fmt)          (1 +(FMT_LINK_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_CONFIG_MODE_EN(fmt)       (1 +(FMT_EXP_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_EDGE_MODE_EN(fmt)         (1 +(FMT_CONFIG_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_READOUT_TIMEOUT(fmt)      (1 +(FMT_EDGE_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_DATA_OVERFLOW_LIMIT(fmt)  (1 +(FMT_READOUT_TIMEOUT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_LIMIT(fmt) (1 +(FMT_DATA_OVERFLOW_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_LIMIT(fmt)       (1 +(FMT_HEADER_TRAILER_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_LINK_L1A_CNT(fmt)     (1 +(FMT_ROD_BUSY_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_BANDWIDTH(fmt)        (1 +(FMT_PXL_LINK_L1A_CNT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_DATA_TEST_MUX(fmt)   (33 +(FMT_PXL_BANDWIDTH(FORMATTERS_PER_ROD-1)) +fmt)
/* +4*8 reserved IDs */
#define FMT_MB_DIAG_REN(fmt)          (1 +(FMT_LINK_DATA_TEST_MUX(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_OCC_CNT(fmt,lnk)     (1 +(FMT_MB_DIAG_REN(FORMATTERS_PER_ROD-1)) \
                                         +(fmt*LINKS_PER_FORMATTER) +lnk)

#define FMT_TIMEOUT_ERR(fmt)          (1 +(FMT_LINK_OCC_CNT((FORMATTERS_PER_ROD-1), \
                                                            (LINKS_PER_FORMATTER-1))) \
                                        +fmt)
#define FMT_DATA_OVERFLOW_ERR(fmt)    (1 +(FMT_TIMEOUT_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_ERR(fmt)   (1 +(FMT_DATA_OVERFLOW_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_ERR(fmt)         (1 +(FMT_HEADER_TRAILER_ERR(FORMATTERS_PER_ROD-1)) +fmt)

#define FMT_DATA_FMT_STATUS(fmt)      (1 +(FMT_ROD_BUSY_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_STATUS(fmt)               (1 +(FMT_DATA_FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_VERSION(fmt)              (1 +(FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_05(fmt)      (1 +(FMT_VERSION(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_6B(fmt)      (1 +(FMT_MODEBIT_STAT_05(FORMATTERS_PER_ROD-1)) +fmt)


/* Event Fragment Builder FPGA registers */
#define EFB_ERROR_MASK(efb, lnk) (25 + FMT_MODEBIT_STAT_6B(FORMATTERS_PER_ROD-1) + \
                                  efb*DATA_LINKS_PER_EFB + lnk)
#define FORMAT_VRSN_LSB      (1 + EFB_ERROR_MASK((EFBS_PER_ROD-1),(DATA_LINKS_PER_EFB-1)))
#define FORMAT_VRSN_MSB      (1 + FORMAT_VRSN_LSB)
#define SOURCE_ID_LSB        (1 + FORMAT_VRSN_MSB)
#define SOURCE_ID_MSB        (1 + SOURCE_ID_LSB)
#define EFB_CMND_0           (1 + SOURCE_ID_MSB)
#define EFB_FORMATTER_STAT   (1 + EFB_CMND_0)
#define EFB_RUNTIME_STAT_REG (1 + EFB_FORMATTER_STAT)
#define EVENT_HEADER_DATA    (1 + EFB_RUNTIME_STAT_REG)
#define EV_FIFO_DATA1        (1 + EVENT_HEADER_DATA)
#define EV_FIFO_DATA2        (1 + EV_FIFO_DATA1)

#define EVT_MEM_MODE         (1 + EV_FIFO_DATA2)
#define EVT_MEM_CMND_STAT    (1 + EVT_MEM_MODE)
#define EVT_MEM_RESET        (1 + EVT_MEM_CMND_STAT)
#define EVT_MEM_FLAGS        (1 + EVT_MEM_RESET)
#define EVT_MEM_A_WRD_CNT    (1 + EVT_MEM_FLAGS)
#define EVT_MEM_B_WRD_CNT    (1 + EVT_MEM_A_WRD_CNT)
#define EVT_MEM_PLAY_EVENT   (1 + EVT_MEM_B_WRD_CNT)
#define EVT_MEM_STATUS       (1 + EVT_MEM_PLAY_EVENT)
#define EFB_EVT_CNT          (1 + EVT_MEM_STATUS)
#define EFB_BANDWIDTH_CNT    (1 + EFB_EVT_CNT)
#define EFB_CODE_VERSION     (1 + EFB_BANDWIDTH_CNT)

/* Router FPGA registers- note that addresses in router are done in 4 blocks
 * (1 block per slv) of CMD0(slv), CMD1(slv), .... INT_DELAY _CNT, ..., while here
 * they are indexed (used in the rodRegister structure in accessRegister.c) as
 * CMD0(0), ... CMD0(3), CMD1(0)...  The addressing for rodRegister is done in
 * accessRegister.c  dpsf. */
#define RTR_TRAP_CMND_0(slv)       (14 + EFB_CODE_VERSION + slv)
#define RTR_TRAP_CMND_1(slv)       (1 + RTR_TRAP_CMND_0(N_SDSP-1)  + slv)
#define RTR_TRAP_RESET(slv)        (1 + RTR_TRAP_CMND_1(N_SDSP-1)  + slv)
#define RTR_TRAP_STATUS(slv)       (1 + RTR_TRAP_RESET(N_SDSP-1)   + slv)
#define RTR_TRAP_MATCH_0(slv)      (1 + RTR_TRAP_STATUS(N_SDSP-1)  + slv)
#define RTR_TRAP_MOD_0(slv)        (1 + RTR_TRAP_MATCH_0(N_SDSP-1) + slv)
#define RTR_TRAP_MATCH_1(slv)      (1 + RTR_TRAP_MOD_0(N_SDSP-1)   + slv)
#define RTR_TRAP_MOD_1(slv)        (1 + RTR_TRAP_MATCH_1(N_SDSP-1) + slv)
#define RTR_TRAP_XFR_FRM_SIZE(slv) (1 + RTR_TRAP_MOD_1(N_SDSP-1)   + slv)
#define RTR_TRAP_FIFO_WRD_CNT(slv) (1 + RTR_TRAP_XFR_FRM_SIZE(N_SDSP-1) + slv)
                   /* 1 unused half-word/slv in router, reserved here (1 word/hw) */
#define RTR_TRAP_EVT_CNT(slv)      (5 + RTR_TRAP_FIFO_WRD_CNT(N_SDSP-1) + slv)
#define RTR_TRAP_INT_DELAY_CNT(slv) (1 + RTR_TRAP_EVT_CNT(N_SDSP-1) + slv)
                      /* 3 unused hw before beginning of next slave address block */
#define RTR_CMND_STAT              (13 + RTR_TRAP_INT_DELAY_CNT(N_SDSP-1))
#define RTR_SLNK_ATLAS_DUMP_MATCH  (1 + RTR_CMND_STAT)
#define RTR_SLNK_ROD_DUMP_MATCH    (1 + RTR_SLNK_ATLAS_DUMP_MATCH)

#define RTR_CODE_VERSION           (1 + RTR_SLNK_ROD_DUMP_MATCH)
#define RTR_OUTPUT_SIGNAL_MUX      (1 + RTR_CODE_VERSION)

/* ROD resources interface FPGA (RRIF) registers */
#define RRIF_CODE_VERSION         (16 + RTR_OUTPUT_SIGNAL_MUX)
#define RRIF_CMND_1               (1 + RRIF_CODE_VERSION)
#define RRIF_CMND_0               (1 + RRIF_CMND_1)
#define ROD_MODE_REG              (1 + RRIF_CMND_0)
#define FE_MASK_LUT_SELECT        (1 + ROD_MODE_REG)
#define RRIF_STATUS_1             (1 + FE_MASK_LUT_SELECT)
#define RRIF_STATUS_0             (1 + RRIF_STATUS_1)
#define FE_CMND_MASK_0_LO         (3 + RRIF_STATUS_0)
#define FE_CMND_MASK_0_HI         (1 + FE_CMND_MASK_0_LO)
#define FE_CMND_MASK_1_LO         (1 + FE_CMND_MASK_0_HI)
#define FE_CMND_MASK_1_HI         (1 + FE_CMND_MASK_1_LO)
#define CALSTROBE_DELAY           (1 + FE_CMND_MASK_1_HI)
#define CAL_CMND                  (1 + CALSTROBE_DELAY)
#define FRMT_RMB_STATUS           (3 + CAL_CMND)
#define EFB_DM_FIFO_FLAG_STA      (2 + FRMT_RMB_STATUS)
#define EFB_DM_WC_STA_REG         (1 + EFB_DM_FIFO_FLAG_STA)
#define INP_MEM_CTRL              (1 + EFB_DM_WC_STA_REG)
#define DBG_MEM_CTRL              (1 + INP_MEM_CTRL)
#define CFG_READBACK_CNT          (1 + DBG_MEM_CTRL)
#define IDE_MEM_CTRL              (2 + CFG_READBACK_CNT)
#define IDE_MEM_STAT              (1 + IDE_MEM_CTRL)
#define INTRPT_TO_SLV             (7 + IDE_MEM_STAT)
#define INTRPT_FROM_SLV           (1 + INTRPT_TO_SLV)

#define DFLT_ROD_EVT_TYPE         (1 + INTRPT_FROM_SLV)
#define CRTV_ROD_EVT_TYPE         (1 + DFLT_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_0        (1 + CRTV_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_1        (1 + CAL_L1_TRIG_TYPE_0)
#define CAL_L1_ID_0               (1 + CAL_L1_TRIG_TYPE_1)
#define CAL_L1_ID_1               (1 + CAL_L1_ID_0)
#define CAL_BCID                  (1 + CAL_L1_ID_1)


using namespace SctPixelRod;

struct ScanBin {
	signed long stream[32];
};

struct ScanBin* GetScanBin(RodModule *m_rod, int numofbits) {
  int formatter;
  unsigned long stream01, stream23;

  struct ScanBin * scanpoint = (struct ScanBin *) malloc(sizeof(struct ScanBin));

  for(formatter=0;formatter<8;formatter++) {
    m_rod->mdspSingleWrite(FMT_CNT_CONTROL(formatter), 0x0); // Turn of counting to make the GLB Counter Registers writeable
    m_rod->mdspSingleWrite(FMT_GLB_COUNTER(formatter), (numofbits & 0xFFFF));// Write highest possible value to GLB Counter Registers
    m_rod->mdspSingleWrite(FMT_CNT_CONTROL(formatter), 0x1); // Enable Counting...
  }


  for(formatter=0;formatter<8;formatter++) {
    while (((m_rod->mdspSingleRead(FMT_GLB_COUNTER(formatter)) >> 16) & 0xFFFF) > 0); // Wait for global counter to finish
    stream01 = m_rod->mdspSingleRead(FMT_STREAM_01(formatter));// Read back data for Streams 0/1
    stream23 = m_rod->mdspSingleRead(FMT_STREAM_23(formatter));// Read back data for Streams 2/3
    (*scanpoint).stream[(formatter<<2)+0] = stream01 & 0xFFFF;
    (*scanpoint).stream[(formatter<<2)+1] = (stream01>>16) & 0xFFFF;
    (*scanpoint).stream[(formatter<<2)+2] = stream23 & 0xFFFF;
    (*scanpoint).stream[(formatter<<2)+3] = (stream23>>16) & 0xFFFF;
//		printf("%ld\t%ld\t%ld\t%ld\t", (*scanpoint).stream[(formatter<<2)+0], (*scanpoint).stream[(formatter<<2)+1], (*scanpoint).stream[(formatter<<2)+2], (*scanpoint).stream[(formatter<<2)+3]);
  }
//	printf("\n");
  return scanpoint;
}


void sendCommand(RodModule *m_rod) { //! Send command no out
  int i;
  int packSize = 1;
  int pLen = sizeof(SendSerialStreamIn)/sizeof(uint32_t);
  uint32_t *tsdData = new uint32_t[pLen+packSize];
  for (i=0; i<packSize; i++) {
    tsdData[i+pLen] = 0x100;
  }

  // Set the output mask
  unsigned int mask1 = 0x0, mask2 = 0x0;
  mask1 = 0xffffffff;
  mask2 = 0xffffffff;
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_LO, mask1);
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_HI, mask2);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_LO, 0x00000000);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_HI, 0x00000000);
//   m_rod->writeRegister(RRIF_CMND_1, 20, 1, 0x1);
//   m_rod->writeRegister(RRIF_CMND_1, 2, 1, 0x1);

//  std::cout << "RodPixControllerFAKE::sendCommand" 
//	    << " mask1 " << std::hex << mask1
//	    << " mask2 " << std::hex << mask2
//	    << std::dec <<  std::endl;

  SendSerialStreamIn *sendSerialStreamIn = (SendSerialStreamIn*)tsdData;
  sendSerialStreamIn->sportId  = 0;
  sendSerialStreamIn->nBits    = packSize*32;
  sendSerialStreamIn->fCapture = 1;
  sendSerialStreamIn->nCount   = 1;
  sendSerialStreamIn->dataPtr  = 0;
  sendSerialStreamIn->maskLo   = mask1;
  sendSerialStreamIn->maskHi   = mask2;

//  std::cout << "Creating ROD Primitive" << std::endl;
  
  RodPrimitive* sendStream;
  sendStream = new RodPrimitive(7+packSize, 0, SEND_SERIAL_STREAM, 0, (long*)sendSerialStreamIn);
//  std::cout << "Sending ROD Primitive" << std::endl;
  m_rod->executeMasterPrimitiveSync(*sendStream);
//  std::cout << "Done with ROD Primitive" << std::endl;
  delete sendStream;
  delete[] tsdData;
}

int main(int argc, char **argv) {
  RCCVmeInterface *vme;
  
  const int VHalfClock = 0x2;
  const int VInvertClock = 0x1;
  
  int slot = -1;
  std::string option;
  unsigned long baseAddress;
  int numofbits = 0xFFFF;
  int setcurrent = 300;
  
  unsigned int interlock, status;
  uint32_t localEnable;
  uint32_t remoteEnable;
  
  std::string unit, type;
  std::string outfile = "~/dump.csv";

  FILE *output;

  struct ScanBin *CoarseScans[50];
  struct ScanBin *FineScans[512];

  signed long diffsum[32];
  float minimum[32];
  int optimal[32];


  uint32_t INLINKS[32] = { 2,3,4,5,6,7,8,9,14,15,16,17,18,19,20,21,26,27,28,29,30,31,32,33,38,39,40,41,42,43,44,45 };
  uint32_t OUTLINKS[32] = { 2,3,4,5,6,7,8,9,38,39,40,41,42,43,44,45,50,51,52,53,54,55,56,57,86,87,88,89,90,91,92,93 };


  double pin_current[32][256]; // Variable to store Curves of IFset versus IPin

  int usechannel = -1;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
      case 's': {
	slot = atoi(option.substr(2).c_str());
	break;
      }
      case 'c': {
	setcurrent = atoi(option.substr(2).c_str());
	break;
      }
      case 'o':{
	outfile = argv[i + 1];
	i++;
	break;
      }
      case 'u':{
	usechannel = atoi(option.substr(2).c_str());
	break;
      }
      default: {
	break;
      }
      }
    }
  }
  
  // Prompt for slot number
  if (slot < 0 ) {
    std::cout << "Enter slot number (decimal):";
    std::cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      std::cout << "Slot number out or range [4-12:14-21], re-enter: ";
      std::cin >> slot;
    }
  } else {
    if (slot < 4 || slot == 13 || slot > 21 ) {
      std::cout << "Slot number out of range [4-12:14-21], re-enter: ";
      std::cin >> slot;
      while ((slot < 1) || (slot > 21)) {
        std::cout << "Slot number out or range [4-12:14-21], re-enter: ";
        std::cin >> slot;
      }
    }
  }
  
  output = fopen(outfile.c_str(), "a+");
  
  std::cout << "You do know what you're doing (yes/no) ? [no]" << std::endl;
  
  sleep(5);
  
  std::cout << "Anyway, let's pretend you do..." << std::endl;
  
  std::cout << "Initializing VmeInterface ... ";
  
  try {
    vme = new RCCVmeInterface();
  } catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  } catch (BaseException & exc){
    std::cout << exc << std::endl;
    exit(1);
  } catch (...){
    std::cout << "error during VME initialization - exiting..." << std::endl;
    exit(1);
  }
  
  baseAddress = slot<<24;
  
  std::cout << "[done]" << std::endl;
  
  std::cout << "Initializing RodModule in slot number " << slot << " ... ";

  RodModule *rod = NULL;
  // SLOT Number is hardcoded in here...
  try{
    //~ app = new application(vme,tdb);
    rod = new RodModule(baseAddress, (unsigned long) 0x01000000, *vme, (long) 4);
    rod->initialize();
  } catch (BaseException & exc){
    std::cout << exc << std::endl;
    exit(1);
  } catch (...){
    std::cout << "error initializing the ROD, exiting" << std::endl;
    exit(1);
  }

  std::cout << "[done]" << std::endl;

  std::cout << "Giving you a Rod->Status()" << std::endl;

  rod->status();
  
  std::cout << "Initializing BocCard (we're nearly done) ... ";
  
  BocCard *my_boc = NULL;
  try{
    my_boc = rod->getBocCard();
    my_boc->initialize();
    my_boc->readInfo();
  } catch (...) {
    std::cout << "Error during first communication with the BOC... Damnit !" << std::endl;
    exit(1);
  }

  std::cout << "[done] (with initializing)" << std::endl;


//  std::cout << "Resetting Monitor ADCs" << std::endl;
//
//  my_boc->readInfo();
//  my_boc->resetMonitorAdc();

  std::cout << "BOC Status:" << std::endl;

  std::hex(std::cout);
  std::cout << "BOC, Serial number: 0x" << (my_boc->getSerialNumber() & 0xff) << std::endl;
  std::cout << "Manufacturer      : " << ((my_boc->getManufacturer() & 0xff)==0xcb ? "Cambridge" : "Not Cambridge - strange") << std::endl;
  std::cout << "Module type       : 0x" << (my_boc->getModuleType() & 0xff) << std::endl;
  std::cout << "Firmware revision : 0x" << (my_boc->getFirmwareRevision() & 0xff) << std::endl;
  std::cout << "Hardware revision : 0x" << (my_boc->getHardwareRevision() & 0xff) << std::endl;
  std::cout << "Status Register   : 0x" << (status = (my_boc->getBocStatusRegister() & 0xff)) << std::endl;
  std::cout << "Status Register can be decoded to:" << std::endl;
  std::cout << "Remote Interlock " << ((remoteEnable = ((status>>7)&0x1)) ? "Enabled" : "Disabled") << std::endl;
  std::cout << "Local Interlock " << ((localEnable = ((status>>6)&0x1)) ? "Enabled" : "Disabled") << std::endl;
  std::cout << "Readout Driver " << (((status>>5)&0x1) ? "Not sensed (strange)" : "Sensed") << std::endl;
  std::cout << "Err Flag (Sensing Clocks on RX): " << ((status>>4)&0x1) << std::endl;
  std::cout << "Supply Voltages reading " << (((status>>2)&0x3==0x3) ? "OK" : "NOT ok") << std::endl;
  std::cout << "Remote Clock " << (((status>>1)&0x1) ? "OK since last reset" : "has been wrong since last reset") << std::endl;

  if (my_boc->getFirmwareRevision() < 0x83) {
    std::cout << "BOC Firmware Revision to old (Found Version 0x" << my_boc->getFirmwareRevision() << " for these Scans to pass - should be upgraded to 0x84 or later..." << std::endl << "Exiting" << std::endl;
    return 0;
  }


  std::dec(std::cout);

  interlock = my_boc->getInterlockStatus(&localEnable,&remoteEnable);

  if (!(interlock)) {
    switch (((localEnable<<1)+remoteEnable) & 0x3) {
    case 0:
      std::cout << "Both Interlocks need to be switched";
      break;
    case 1:
      std::cout << "Local Interlock needs to be switched";
      break;
    case 2:
      std::cout << "Remote Interlock needs switching";
    default:
      std::cout << "Interlocks look fine, even thought getInterlockStatus doesn't say so...";
    }
    std::cout << std::endl << "Exiting and to let you fix it..." << std::endl;
    return 0;
  }

  my_boc->resetMonitorAdc();

  for(int i=0;i<12;i++) {
    std::cout << "Monitor channel "<< i << " : " << my_boc->getMonitorAdc(i, unit, type) << " " << unit << "  " << type <<std::endl;
  }

  std::cout << std::endl << "Arriving at the real testing - we need some power:" << std::endl;

  for(int i=0; i<32; i++) {
    my_boc->setLaserCurrent(INLINKS[i], 0);
  }

  if (usechannel < 0) { //beginning without usechannel
    
    for(int i=0; i<32; i++) {
      for(int j=0; j<256; j+=4) {
	my_boc->setLaserCurrent(INLINKS[i], j);
	usleep(1);
	pin_current[i][j] = my_boc->getMonitorAdc(((i/8)*2) + ((i/8)%2));
      }
      my_boc->setLaserCurrent(INLINKS[i], 0);
    }
    
    for(int j=0; j<256; j+=4) {
      fprintf(output, "%d", j);
      for(int i=0; i<32; i++) {
	fprintf(output, "\t%f", (float) pin_current[i][j]);
      }
      fprintf(output, "\n");
      fflush(output);
    }
    
    for(int i=0; i<32; i++) {
      int j;
      for(j=0; j<256; j+=4) {
	if (pin_current[i][j] > 0.300) {
	  my_boc->setLaserCurrent(INLINKS[i], j);
	  std::cout << "Setting laser " << INLINKS[i] << " with measured PiN current of " << std::setw(2) << std::setprecision(2) << pin_current[i][j] << std::endl;
	  j = 260;
	}
      }
      if (j < 260) {
	std::cout << "Didn't set any laser current on " << INLINKS[i] << " cause measured PiN current was never higher than 300uA - j=" << j << std::endl;
	my_boc->setLaserCurrent(INLINKS[i], 0);
      }
      my_boc->setRxThreshold(OUTLINKS[i], 255);
    }
    
    
    
    //  setConfigurationMode(rod);
    
     for (uint32_t formatter=0; formatter<8;formatter++) {
       rod->mdspSingleWrite(FMT_LINK_EN(formatter),0x0F); // Look wether there has to be some 89ab !!!
     }
//     rod->writeRegister(RRIF_CMND_1, 25, 4, 4);
    
    my_boc->setRxDataMode(6);
    
    //set V half clock 2
    my_boc->setClockControl(VHalfClock);
    my_boc->setVernierFinePhase(0);
    for (int i = 0; i < 50; i++) {

      //         Invert V-Clock as soon as Delay is bigger than 24ns for having full range of 0-49ns Delay
      if (i == 25)
	my_boc->setClockControl(VHalfClock | VInvertClock);
      my_boc->setVernierClockPhase0((i % 25));
      CoarseScans[i] = GetScanBin(rod, 0xFFFF);
    }
    
    std::cout << "DEBUG: Calculating pulse width..." << std::endl;
    
    for(int delay=0; delay<50; delay++) {
      for(int i=0; i<32;i++) {
	std::cout << std::setw(6) << std::setprecision(5) << (*CoarseScans[delay]).stream[i];
	if (delay > 0) (*CoarseScans[0]).stream[i] += (*CoarseScans[delay]).stream[i];
      }
      std::cout << std::endl;
    }
    
    std::cout << std::endl << "Printing Pulse width:" << std::endl;
    for(int i=0; i<32;i++) {
      std::cout << std::setw(10) << std::setprecision(9) << ((*CoarseScans[0]).stream[i] / 65535);
    }
    
    for(int i=0;i<50;i++) {
      delete CoarseScans[i];
    }
    
    for(int i=0;i<32;i++) my_boc->setBpmFineDelay(INLINKS[i], 30);

    my_boc->setClockControl(VHalfClock);
    my_boc->setVernierFinePhase(0);
    std::cout << "I set all finedelays to 30..." << std::endl;
    
    for (int i = 0; i < 50; i++) {

      //         Invert V-Clock as soon as Delay is bigger than 24ns for having full range of 0-49ns Delay
      if (i == 25)
	my_boc->setClockControl(VHalfClock | VInvertClock);
      my_boc->setVernierClockPhase0((i % 25));
      CoarseScans[i] = GetScanBin(rod, 0xFFFF);
    }
    
    std::cout << "DEBUG: Calculating pulse width..." << std::endl;
    
    for(int delay=0; delay<50; delay++) {
      for(int i=0; i<32;i++) {
	std::cout << std::setw(6) << std::setprecision(5) << (*CoarseScans[delay]).stream[i];
	if (delay > 0) (*CoarseScans[0]).stream[i] += (*CoarseScans[delay]).stream[i];
      }
      std::cout << std::endl;
    }
    
    std::cout << std::endl << "Printing Pulse width:" << std::endl;
    for(int i=0; i<32;i++) {
      std::cout << std::setw(10) << std::setprecision(9) << ((*CoarseScans[0]).stream[i] / 65535);
    }
    
    for(int i=0;i<50;i++) {
      delete CoarseScans[i];
    }

    sendCommand(rod);

    my_boc->setClockControl(VHalfClock);
    my_boc->setVernierFinePhase(0);
    std::cout << "Just sent a single 1..." << std::endl;
    
    for (int i = 0; i < 50; i++) {

      //         Invert V-Clock as soon as Delay is bigger than 24ns for having full range of 0-49ns Delay
      if (i == 25)
	my_boc->setClockControl(VHalfClock | VInvertClock);
      my_boc->setVernierClockPhase0((i % 25));
      CoarseScans[i] = GetScanBin(rod, 0xFFFF);
    }
    
    std::cout << "DEBUG: Calculating pulse width..." << std::endl;
    
    for(int delay=0; delay<50; delay++) {
      for(int i=0; i<32;i++) {
	std::cout << std::setw(6) << std::setprecision(5) << (*CoarseScans[delay]).stream[i];
	if (delay > 0) (*CoarseScans[0]).stream[i] += (*CoarseScans[delay]).stream[i];
      }
      std::cout << std::endl;
    }
    
    std::cout << std::endl << "Printing Pulse width:" << std::endl;
    for(int i=0; i<32;i++) {
      std::cout << std::setw(10) << std::setprecision(9) << ((*CoarseScans[0]).stream[i] / 65535);
    }
    
    for(int i=0;i<50;i++) {
      delete CoarseScans[i];
    }

    std::cout << std::endl;
    
  } else {  // else of if usechannel
    
    for(int j=0; j<256; j+=1) {
      my_boc->setLaserCurrent(INLINKS[usechannel], j);
      usleep(1);
      pin_current[usechannel][j] = my_boc->getMonitorAdc(((usechannel/8)*2) + ((usechannel/8)%2));
    }
    
    for(int j=0; j<256; j+=1) {
      printf("\t%f", (float) pin_current[usechannel][j]);
    }
    
    std::cout << std::endl;
    int j;
    for(j=0; j<256; j+=1) {
      if (pin_current[usechannel][j] > ((float) setcurrent / 1000.0)) {
	my_boc->setLaserCurrent(INLINKS[usechannel], j);
	std::cout << "Setting laser " << INLINKS[usechannel] << " with measured PiN current of " << std::setw(2) << std::setprecision(2) << pin_current[usechannel][j] << std::endl;
      j = 260;
      }
    }
    if (j < 260) {
      std::cout << "Didn't set any laser current on " << INLINKS[usechannel] << " cause measured PiN current was never higher than 300uA" << std::endl;
      my_boc->setLaserCurrent(INLINKS[usechannel], 0);
    }
    for(int j=0; j<32; j++) my_boc->setLaserCurrent(INLINKS[j], 120);
    
    int pulsewidth =0;
    
    my_boc->setRxThreshold(OUTLINKS[usechannel], 255);
    
    //    setConfigurationMode(rod);
    int i;
    my_boc->setBpmMarkSpace(INLINKS[usechannel], 0x13);
    
    int formatter;
    for (i = 0; i < 32; i++) {
      minimum[i] = 10240;
    }
    
    int Outlink_modnum[32] = { 2, 3, 4, 5, 6, 7, 8, 9, 38, 39, 40, 41, 42, 43, 44, 45, 50, 51, 52, 53, 54, 55, 56, 57, 86, 87, 88, 89, 90, 91, 92, 93 };
    
    std::cout << "DEBUG: Setting BOC RX mode to 6" << std::endl;
    my_boc->setRxDataMode(6);
     for (uint32_t formatter=0; formatter<8;formatter++) {
       rod->mdspSingleWrite(FMT_LINK_EN(formatter),0x0F); // Look wether there has to be some 89ab !!!
     }
//     rod->writeRegister(RRIF_CMND_1, 25, 4, 4);
    
    
    std::cout << "DEBUG: Scanning..." << std::endl;
    // Bloeder Jens
    //    std::cout << "DEBUG: sending Command 1" << std::endl;
    //    sendCommand(rod);
    for (int scanstep = 0; scanstep < 128; scanstep++) {
//      std::cout << "DEBUG: sending Command 1" << std::endl;
      sendCommand(rod);
//      std::cout << "U made it past sending a Command, Hooray!" << std::endl;
      my_boc->setBpmFineDelay(INLINKS[usechannel], scanstep);
      //      my_boc->setRxThreshold(OUTLINKS[usechannel], scanstep);
      //      my_boc->setBpmMarkSpace(INLINKS[usechannel], scanstep);
      for (int modnum = 0; modnum < 32; modnum++) {
	my_boc->setRxDataDelay(Outlink_modnum[modnum], ((uint32_t) 0));
      }
      
      fprintf (output, "%d\t", (scanstep % 256));
      
      my_boc->setClockControl(VHalfClock);
      my_boc->setVernierClockPhase0(0);
      my_boc->setVernierFinePhase(0);
      int pulsewidth =0;
      for (i = 0; i < 50; i++) {
	if (i == 25)
	  my_boc->setClockControl(VHalfClock | VInvertClock);
	my_boc->setVernierClockPhase0(i%25);
	CoarseScans[i] = GetScanBin (rod, numofbits);
	pulsewidth += (*CoarseScans[i]).stream[usechannel];
	//	  std::cout << scanstep << "\t" << (*CoarseScans[i]).stream[usechannel] << std::endl;
      }
      
      fprintf (output, "%f\t", ((float) (pulsewidth / (float) numofbits)));
      
      int middle[32];
      for (int modnum = 0; modnum < 32; modnum++) {
	int onesleft[50], zerosleft[50], falling = 49, rising = 49;
	
	for (int delay = 0; delay < 50; delay++) {
	  zerosleft[delay] = onesleft[delay] = 0;
	  for (i = delay; i < 50; i++) {
	    if ((*CoarseScans[i]).stream[modnum] < numofbits / 2) {
	      zerosleft[delay] = 1;
	    }
	    else {
	      onesleft[delay] = 1;
	    }
	  }
	  if (((*CoarseScans[delay]).stream[modnum] < numofbits / 2)
	      && (onesleft[delay] == 1))
	    rising = delay;
	  if (((*CoarseScans[delay]).stream[modnum] > (numofbits / 2) - 1) && (zerosleft[delay] == 1))
	    falling = delay;
	}
	
	middle[modnum] = ((((rising + falling) / 2) + 20) % 25);
	
	// Commented to find spikes
	//	  my_boc->setRxDataDelay(Outlink_modnum[modnum], ((25 - middle[modnum]) % 25));
	
	if (modnum == usechannel) {
	  std::cout << "Rising Edge at: " << rising << " Falling at: " << falling << " Setting Phase 0 to: " << ((rising + 49) % 25) << " with Inversion equal " << (((rising + 49) / 25) % 2) << std::endl;
	  my_boc->setVernierClockPhase0((rising + 49) % 25);
	  my_boc->setClockControl((((rising + 49) / 25) % 2) | VHalfClock); 
	}
      }
      
      my_boc->setVernierClockPhase1(0);
      
      for (i = 0; i < 50; i++) {
	free (CoarseScans[i]);
      }
      
      //	my_boc->setClockControl(0x0);
	for (i = 0; i < 256; i++) {
          my_boc->setVernierFinePhase(i%256);
	  for (int k = 0; k < 1; k++) {
	    FineScans[i] = GetScanBin (rod, numofbits);
	  }
	}
	
	diffsum[usechannel] = 0;
	for (i = 0; i < 256; i++) {
	  diffsum[usechannel] += (*FineScans[i]).stream[usechannel];
	}
	if (fabs ((float) (diffsum[usechannel] * 40.0 / (float) numofbits)) < minimum[usechannel]) {
	  optimal[usechannel] = scanstep;
	  minimum[usechannel] = fabs ((float) (diffsum[usechannel] * 40.0 / (float) numofbits));
	}
	fprintf (output, "%f\t", (float) (diffsum[usechannel] * 40.0 / (float) numofbits));
	
	for (int i = 0; i < 256; i++) {
	  free (FineScans[i]);
	}
	
	fprintf (output, "\n");
	fflush (output);
    }				// END MSR LOOP !
    
  } //end of if usechannel
  
  fclose(output);
  
  delete rod;
  delete vme;
  
  return 0;
}
