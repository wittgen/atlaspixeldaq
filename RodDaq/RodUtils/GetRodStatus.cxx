/* Test of the RodStatus class
 * written by Fredrik Tegenfeldt <fredrik.tegenfeldt@cern.ch>
 * 
 * $Id$
 */

#include <iostream>
using namespace std;

#include "RodModule.h"
#include "RodStatus.h"

//#include "primParams.h"
#include "RCCVmeInterface.h"
#include "parameters.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  std::string fileName(""), option;
  //bool verbose = false;
  int slot = -1;
  uint32_t baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'v': {
          //verbose = true;
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());

          if((slot < 1) || (slot > 21)) {
            cout << "Slot number out or range [1:21] (input: " << slot << ")\n";
            return 0;
          }

          break;
        }
        default: {
          break;
        }
      }
    }
  }
  
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme=nullptr;
  try {
    vme = new RCCVmeInterface();
  } catch (...) {
    std::cout << "Caught an exception when creating a VME interface." << std::endl;
  }
  
// Create RodModule
  RodModule *rod = NULL;
  RodStatus *rodStatus = NULL;
  try {
    rod = new RodModule(baseAddress, mapSize, *vme, numSlaves);
    rodStatus = new RodStatus(*rod);
  } catch (RodException &r) {
    std::cout << __FUNCTION__ 
      << "ROD exception thrown when creating the RodModule/RodStatus objects" 
      << std::endl
      << r;
  } catch (VmeException &v) {
    std::cout << __FUNCTION__ 
      << ": VME exception thrown when creating the RodModule/RodStatus objects\n "
      << v
      << "RodModule not found on VME (wrong slot number or h/w error)!"
      << std::endl;
  } catch (...) {
    std::cout << __FUNCTION__ 
      << ": Some whatever exception occured..." 
      << std::endl;
  }

  /* If rodStatus couldn't been created, then we skip the attempt to 
   * reset the module */
  if (rodStatus) {
    if (!rodStatus->isSane()) {
      std::cout << "Looks like we have an insane ROD module on our hands..." 
        << std::endl;
      std::cout << "With an initialize() it will be more happy." << std::endl;
    }

    try {
      std::cout << "First doing initialize()" << std::endl;
      rod->initialize(false);
      std::cout << "Secondly, make the snapshot of the status" << std::endl;
      rodStatus->snapShot(*rod);
      std::cout << "Done." << std::endl;
    } catch (BaseException &e) {
      std::cout << __FUNCTION__ 
        << ": Exception when initializing RodModule:\n ";
      e.what(std::cout);
      std::cout << std::endl;
    } catch (...) {
      std::cout << "An exception was generated when initializing the RodModule." 
        << std::endl;
    }


    if (!rodStatus->isSane()) {
      std::cout << "Still insane... check the slotnumber." << std::endl;
    } else {
      std::cout << "We've got a live one!" << std::endl;
      std::cout << "Print the status:" << std::endl;
      std::cout << *rodStatus;
      std::vector<unsigned int> buffer;
      uint32_t * buffer1 = new uint32_t[sizeof(CommRegs)/sizeof(int)];
      CommRegs  master_CommRegs;
      CommRegs slave_CommRegs[4];
      try {  
	rod->mdspBlockRead((uint32_t)MAGIC_MASTER_LOCATION,buffer1,sizeof(master_CommRegs)/sizeof(int));		      
	for(unsigned i=0;i<sizeof(CommRegs)/sizeof(int);i++)
	  ((unsigned int*) &master_CommRegs)[i]=buffer1[i];
	for(unsigned i=0;i<4;i++) {
	  rod->readSlaveMem(i,(int) MAGIC_SLAVE_LOCATION,sizeof(CommRegs)/sizeof(int),buffer);  
	  for(unsigned j=0;j<sizeof(CommRegs)/sizeof(int);j++)
	    ((unsigned int*) &slave_CommRegs[i])[j]=buffer[j];
	}
      }
      catch (HpiException &h) {
	cout << h;
      }
      char *s;
      cout << "==================== MASTER VERSION ======================" <<endl;
      s=(char *)&master_CommRegs.scratch[0];
      cout << "Image MD5 sum: ";
      for(int i=0;i<32;i++) {
	cout << s[i];
      } 
      cout <<endl;
      cout << "Link Time: ";
      s=(char *)&master_CommRegs.scratch[8];
      for(int i=0;i<12;i++) {
	cout << s[i];
      } 
      cout <<endl;
      cout << "CVS Tag: ";
      s=(char *)&master_CommRegs.scratch[11];
      int i=0;
      while (s[i] && (i <64) ) {
	cout << s[i];
	i++;
      } 
      cout << endl;
      for(int j=0;j<4;j++) {
	cout << "=============== SLAVE Number " << j << ": " << " Version =================" << endl;
	s=(char *) &slave_CommRegs[j].scratch[0];
	cout << "Image MD5 sum: ";
	for(i=0;i<32;i++) {
	  cout << s[i];
	} 
	cout <<endl;
	cout << "Link Time: " ;
	s=(char *)&slave_CommRegs[j].scratch[8];
	for(i=0;i<12;i++) {
	  cout << s[i];
	} 
	cout <<endl;
	cout << "CVS TAG: ";
	s=(char *)&slave_CommRegs[j].scratch[11];
	i=0;
	while (s[i] && (i <64) ) {
	  cout << s[i];
	  i++;
	} 
	cout <<endl;
      }
    }
  } /* if(RodStatus) */
  
// Delete the ROD and VME objects before exiting
  delete rod;
  delete rodStatus;
  delete vme;

  return 0;  
}

