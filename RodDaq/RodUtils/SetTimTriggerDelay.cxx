#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {
  int delay;
  if (argc!=2) {
    cout<<"Use syntax: SetTimTriggerDelay [delay]"<<endl;
    return 0;
  } else delay = atoi(argv[1]);
  if(delay < 0 || delay > 255) {
    std::cout << "Invalid delay " << delay << ". Values allowed 0-255" << std::endl;
    return 0;
  }
  VmeInterface *vme;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  try{
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    
    //set vme RODBusy to stop triggers at LTP level (not TTCvi!!!)
    tim->loadBitSet(TIM_REG_COMMAND, TIM_VROD_BUSY);
   
    //tim->loadByteLo( TIM_REG_DELAY, delay);
    //2 coarse 16 fine 
    float delay_ns = 66;
    int fineDelay = (int)(fmod(delay_ns,25)*9.619+0.5);
    int coarseDelay = (int)(delay_ns/25);
    int n, m;
    if (coarseDelay>=0) {
      coarseDelay = ((coarseDelay & 0xf) << 4) + (coarseDelay & 0xf); //same delay for both delay groups
      tim->ttcRxWrite(0x2,coarseDelay & 0xff);
      std::cout << "Coarse delay in TTCrx chip set to "<< (int)(delay_ns/25) << std::endl;
    }
    if (fineDelay>=0) {
      n = fineDelay % 15;
      m = ((fineDelay/15)-n+14) % 16;
      tim->ttcRxWrite(0x0, ((n <<4) + m) & 0xff);
      std::cout << "Fine delay in TTCrx chip set to "<< fmod(delay_ns,25) << std::endl;
    }

    //release vme RODBusy to stop triggers at LTP level
    tim->loadBitClear(TIM_REG_COMMAND, TIM_VROD_BUSY);
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}
