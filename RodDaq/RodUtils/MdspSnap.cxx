/*! A program to dump standard buffers in the Master DSP memory space. 
   The buffers are PrimBuff, RepBuff, all four text buffers, and the
   status and control registers.
   It does NOT initialize the ROD!! This program is intended to be 
   a diagnostic used to examine the MDSP after another
   program, e.g. EchoTest or LedTest, has run.
*/

#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  unsigned long numWords;                       // Number of words to dump
  long dspStart;                                // Address limits to dump
  unsigned long statusVal;
  unsigned long txtAddress[4], primBuffSize, replyBuffSize;
  void* genPtr;
  char* textBuff;

  std::string fileName(""), option;
  int slot = -1, ibuf, is;
  unsigned int i, itext;
  unsigned long baseAddress;
  unsigned int mdspStart, mdspMapSize, sdspStart, sdspEnd;
  VmePort* myVmePort;
  MdspMemoryMap* myMdspMap;
  SdspMemoryMap* mySdspMap;

  if (argc > 1) {
    for (is=1; is<argc; is++) {
      option = argv[is];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule but do not initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  myVmePort = rod0->getVmePort();
  mdspStart = rod0->mdspSingleRead(MEMORY_MAP_REG);
  mdspMapSize = rod0->mdspSingleRead(mdspStart);
  myMdspMap = rod0->getMdspMap();
  mySdspMap = rod0->getSdspMap();
  sdspStart = myMdspMap->load(mdspStart, myVmePort);
  sdspEnd = mySdspMap->load(sdspStart, myVmePort);
  for (i=0; i<N_TXT_BUFFS; i++) {
    txtAddress[i] = myMdspMap->txtBuff(i);
  }

  dspStart = myMdspMap->primBuffer();
  numWords = rod0->mdspSingleRead(dspStart);
  primBuffSize = myMdspMap->primBufferSize();
  if (numWords > primBuffSize) numWords = primBuffSize;
  unsigned long * buffer = new unsigned long[numWords];
  rod0->mdspBlockRead(dspStart, buffer, numWords);
  
// output the data and delete the buffer
  cout << "PRIM Buffer:" << endl;
  hex(cout);
  cout.fill('0');
  for (i=0; i<numWords; i+=8) {
    cout.width(8); cout << buffer[i]   << ' ';
    cout.width(8); cout << buffer[i+1] << ' ';
    cout.width(8); cout << buffer[i+2] << ' ';
    cout.width(8); cout << buffer[i+3] << ' ';
    cout.width(8); cout << buffer[i+4] << ' ';
    cout.width(8); cout << buffer[i+5] << ' ';
    cout.width(8); cout << buffer[i+6] << ' ';
    cout.width(8); cout << buffer[i+7] << endl;
  }
  dec(cout);
  cout.fill(' ');
  delete [] buffer;
    
// Get the REPLY buffer
    dspStart = myMdspMap->replyBuffer();
    numWords = rod0->mdspSingleRead(dspStart);
    replyBuffSize = myMdspMap->replyBufferSize();
    if (numWords > replyBuffSize) numWords = replyBuffSize;
    buffer = new unsigned long[numWords];
    rod0->mdspBlockRead(dspStart, buffer, numWords);
  
// output the data and delete the buffer
    cout << "REPLY Buffer:" << endl;
  hex(cout);
  cout.fill('0');
  for (i=0; i<numWords; i+=8) {
    cout.width(8); cout << buffer[i]   << ' ';
    cout.width(8); cout << buffer[i+1] << ' ';
    cout.width(8); cout << buffer[i+2] << ' ';
    cout.width(8); cout << buffer[i+3] << ' ';
    cout.width(8); cout << buffer[i+4] << ' ';
    cout.width(8); cout << buffer[i+5] << ' ';
    cout.width(8); cout << buffer[i+6] << ' ';
    cout.width(8); cout << buffer[i+7] << endl;
  }
  dec(cout);
  cout.fill(' ');
  delete [] buffer;

// Read and print the status and command registers  
  hex(cout);
  for (is=0; is<3; is++) {
    statusVal = rod0->mdspSingleRead(STATUS_REG[is]);
    cout << "Status register " << i << " (hex)  = ";
    cout.width(8);
    cout << statusVal << endl;
  }
  statusVal = rod0->mdspSingleRead(COMMAND_REG_0);
  cout << "Command register = ";
  cout.width(8);
  cout << statusVal << endl;
  dec(cout);
    
// Read and print all four text buffers in both hex and ascii
  numWords = 512;  
  for (ibuf=0; ibuf<4; ibuf++) {
    dspStart = txtAddress[ibuf];
    unsigned long * buffer = new unsigned long[numWords];
    rod0->mdspBlockRead(dspStart, buffer, numWords);
    switch(ibuf) {
      case 0: cout << endl << "Hexadecimal dump of ERR buffer:" << endl;
              break;
      case 1: cout << endl << "Hexadecimal dump of INFO buffer:" << endl;
              break;
      case 2: cout << endl << "Hexadecimal dump of DIAG buffer:" << endl;
              break;
      case 3: cout << endl << "Hexadecimal dump of XFER buffer:" << endl;
              break;
      }
    hex(cout);
    cout.fill('0');
    for (i=0; i<numWords; i+=8) {
      cout.width(8); cout << buffer[i]   << ' ';
      cout.width(8); cout << buffer[i+1] << ' ';
      cout.width(8); cout << buffer[i+2] << ' ';
      cout.width(8); cout << buffer[i+3] << ' ';
      cout.width(8); cout << buffer[i+4] << ' ';
      cout.width(8); cout << buffer[i+5] << ' ';
      cout.width(8); cout << buffer[i+6] << ' ';
      cout.width(8); cout << buffer[i+7] << endl;
    }
    dec(cout);
    cout.fill(' ');
    genPtr = static_cast<void*>(buffer);
    textBuff = static_cast<char*>(genPtr);
    switch(ibuf) {
      case 0: cout << endl << "ASCII dump of ERR buffer:" << endl;
              break;
      case 1: cout << endl << "ASCII dump of INFO buffer:" << endl;
              break;
      case 2: cout << endl << "ASCII dump of DIAG buffer:" << endl;
              break;
      case 3: cout << endl << "ASCII dump of XFER buffer:" << endl;
              break;
      }
      for (itext = 0; itext< 4*numWords; itext++) {
        if ((textBuff[itext]<' ') || (textBuff[itext] > '~')) textBuff[itext] = '.';
        cout << textBuff[itext];
        if ((itext+1)%64 == 0) cout << endl;
      }
      cout << endl;
        
    delete [] buffer;
  }
    
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


