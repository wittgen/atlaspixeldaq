#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {
  VmeInterface *vme;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  try{
    uint16_t rbStat, rbMask , rbNewMask , timID, timfirm, timSN;
    int rn, inp;
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    
    //set vme RODBusy to stop triggers at LTP level (not TTCvi!!!)
    tim->loadBitSet(TIM_REG_COMMAND, TIM_VROD_BUSY);
    //---- Here we taking over QPLL control and reset into into the right operation mode (0) - not to be confused with the "mode" bit, which stays "1"! 
    
    tim->regLoad(TIM_REG_QPLL_CTL,0xa0);  
    tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_QPLLCONTROL);
    tim->loadBitClear(TIM_REG_QPLL_CTL,0x0020);
    usleep(10);
    tim->loadBitSet(TIM_REG_QPLL_CTL,0x0020);

    //release vme RODBusy to stop triggers at LTP level
    tim->loadBitClear(TIM_REG_COMMAND, TIM_VROD_BUSY);
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}
