/* Test of the RodStatus class
 * written by Fredrik Tegenfeldt <fredrik.tegenfeldt@cern.ch>
 * 
 * $Id$
 */

#include <iostream>
#include <unistd.h>

#include "RCCVmeInterface.h"
#include "parameters.h"

#include "RodModule.h"
#include "RodStatus.h"

using namespace std;
using namespace SctPixelRod;

#define MAX_THREADS 3
pthread_t threads[MAX_THREADS];
unsigned int threadData[MAX_THREADS] = { 0, 1, 2 };
unsigned int * threadPointer[MAX_THREADS];
unsigned int slotN[MAX_THREADS] = { 6, 8, 11 };
bool used[MAX_THREADS] = { false, true, true };
pthread_mutex_t threadLock;
pthread_rwlock_t masterLock;
unsigned int nThreadsCompleted=0;

RCCVmeInterface *vme;

void testBT(unsigned int slot) {
  std::string fileName(""), option;
  uint32_t baseAddress;

  baseAddress = slot << 24;

  // Create RodModule
  RodModule *rod = NULL;
  RodStatus *rodStatus = NULL;
  try {
    rod = new RodModule(baseAddress, mapSize, *vme, numSlaves);
    rodStatus = new RodStatus(*rod);
  } catch (RodException &r) {
    std::cout << __FUNCTION__ 
      << "ROD exception thrown when creating the RodModule/RodStatus objects" 
      << std::endl
      << r;
  } catch (VmeException &v) {
    std::cout << __FUNCTION__ 
      << ": VME exception thrown when creating the RodModule/RodStatus objects\n "
      << v
      << "RodModule not found on VME (wrong slot number or h/w error)!"
      << std::endl;
  } catch (...) {
    std::cout << __FUNCTION__ 
      << ": Some whatever exception occured..." 
      << std::endl;
  }

  /* If rodStatus couldn't been created, then we skip the attempt to 
   * reset the module */
  if (rodStatus) {
    if (!rodStatus->isSane()) {
      std::cout << "Looks like we have an insane ROD module on our hands..." 
        << std::endl;
      std::cout << "With an initialize() it will be more happy." << std::endl;
    }

    try {
      std::cout << "First doing initialize()" << std::endl;
      rod->initialize(false);
      std::cout << "Secondly, make the snapshot of the status" << std::endl;
      rodStatus->snapShot(*rod);
      std::cout << "Done." << std::endl;
    } catch (BaseException &e) {
      std::cout << __FUNCTION__ 
        << ": Exception when initializing RodModule:\n ";
      e.what(std::cout);
      std::cout << std::endl;
    } catch (...) {
      std::cout << "An exception was generated when initializing the RodModule." 
        << std::endl;
    }


    if (!rodStatus->isSane()) {
      std::cout << "Still insane... check the slotnumber." << std::endl;
    } else {
      // Test read of busy status in block tranfer modE
      uint32_t* mbuf = new uint32_t[1024];
      for (int i=0; i<100000; i++) {
        if (i%100 == 0) std::cout << std::dec << "Slot " << slot << " Block read #" << i << std::endl;
	rod->getVmePort()->blockRead32(0xc01000, mbuf, 1024*4);
      }
    }
  }
  
// Delete the ROD and VME objects before exiting
  delete rod;
  delete rodStatus;
  delete vme;

}

void *runTest (void *threadid) {
  // Get the master read lock
  pthread_rwlock_rdlock(&masterLock);

  bool loop = true;
  while (loop) {
    // Exedcute one ROD
    unsigned int rodN = -1; 
    pthread_mutex_lock(&threadLock);
    loop = false;
    for (unsigned int ir=0; ir<MAX_THREADS; ir++) {
      if (!used[ir]) {
	rodN = slotN[ir];
	loop = true;
	used[ir] = true;
	break;
      }
    }
    pthread_mutex_unlock(&threadLock);
    if (loop) {
      testBT(rodN);
    }
  }

  // Release the master lock
  pthread_mutex_lock(&threadLock);
  nThreadsCompleted++;
  pthread_mutex_unlock(&threadLock);
  pthread_rwlock_unlock(&masterLock);
  // Terminate the thread
  pthread_exit(NULL);

}

int main( int argc, char **argv ) {

  // Create VME interface
  try {
    vme = new RCCVmeInterface();
  } catch (...) {
    std::cout << "Caught an exception when creating a VME interface." << std::endl;
  }
  
  // Init locks
  pthread_mutex_init(&threadLock,0);
  pthread_rwlock_init(&masterLock,0);

  // Start threads
  unsigned int it;
  for (it=0; it<MAX_THREADS; it++) {
    threadPointer[it] = &(threadData[it]);    
  }
  for (it=0; it<MAX_THREADS; it++) {
    pthread_create(&threads[it], NULL, runTest, (void *) threadPointer[it]);
  }

  // Wait for completion
  usleep(5000);
  pthread_rwlock_wrlock(&masterLock);
  cout << "# of completed threads = " << nThreadsCompleted << endl;

  return 0;
}


