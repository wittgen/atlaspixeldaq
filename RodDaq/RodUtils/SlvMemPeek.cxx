#include <iostream>
using namespace std;

#include "RodModule.h"
//#include "primParams.h"
#include "RCCVmeInterface.h"
//#include "parameters.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  unsigned long numWords, startAddr;
  void* genPtr;
  char* textBuff;
  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  long slvNumber = -1;
  std::string fileName(""), option;
  int slot = -1;
  int numWordsSet=-1;
  int startAddrSet=-1;
  int dumpToFile=0;
  std::ofstream *outp=nullptr;
  unsigned long baseAddress;
  //  unsigned long *buffer;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          slvNumber = atoi(option.substr(2).c_str());
          break;
        }
        case 'a': {
          startAddrSet=sscanf(option.substr(2).c_str(),"%lx",&startAddr);
          break;
        }
        case 'l': {
          numWordsSet=sscanf(option.substr(2).c_str(),"%lx",&numWords);
          break;
        }
      case 'f': {
	outp=new std::ofstream(option.substr(2).c_str(), std::ios::out | std::ios::binary);
	if(outp->is_open()) dumpToFile=1;
      }
      default: 
	break;
      }
    }
  }

  
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1=nullptr;
  try {
    vme1 = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    cout << "VmeException creating VME interface." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 
  }
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize(false);
  }
  catch (HpiException &h) {
    cout << h;

    return 1;
  }
  catch (VmeException &v) {
    cout << "VmeException initialising RodModule." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 

    return 1;
  }
  catch (RodException &r) {
    cout << r;

    return 1;
  }
  catch (BaseException &b) {
    cout << b;

    return 1;
  }

// Get Slave number to read
  if (slvNumber < 0 ) {
    cout << "Enter slave number (0-3):"; 
    cin >> slvNumber;
    while ((slvNumber < 0) || (slvNumber > 3)) {
      cout << "Slave number out or range [0:3], re-enter: ";
      cin >> slvNumber;
    }
  }
     
  if(startAddrSet<=0) {
  cout << "Enter starting address (hex without leading 0x):";
  cin >> hex >> startAddr;
  }
  if(numWordsSet<=0) {
  cout << "Enter number of data words (hex without leading 0x): ";
  cin >> hex >> numWords;
  }
//   try {
//     buffer = new unsigned long[numWords];
//   }
//   catch (std::bad_alloc) {
//     cout << "Unable to allocate echoData in main.";
//   }
  
  std::vector<unsigned int> buffer;
  try {
    rod0->RodModule::readSlaveMem(slvNumber, startAddr, numWords, buffer);
   //    rod0->slvBlockRead(startAddr, buffer, numWords, slvNumber);
 }
  catch (HpiException &h) {
    cout << h;
  }
  catch (VmeException &v) {
    cout << "VmeException in SendPrimList." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 
  }
  
// Print result
  if(!dumpToFile) {
    cout << "RW_SLAVE_MEMORY data: " << endl;
    hex(cout);
    for (uint32_t i=0; i<numWords; i++) {
      cout.width(8);
      cout << buffer[i] <<" ";
      if (0 == (i+1)%8) cout << endl; 
    }
    if (0 != (numWords)%8) cout << endl;
    dec(cout);
    // Display as text also
    cout.fill(' ');
    genPtr = static_cast<void*>(&buffer[0]);
    textBuff = static_cast<char*>(genPtr);
    cout << endl << "ASCII dump :" << endl;
    for (unsigned int itext = 0; itext< 4*numWords; itext++) {
      if ((textBuff[itext]<' ') || (textBuff[itext] > '~')) textBuff[itext] = '.';
      cout << textBuff[itext];
      if ((itext+1)%64 == 0) cout << endl;
    }
    cout << endl;
  } else {
    for (uint32_t i=0; i<numWords; i++) {
      outp->write((char *)&buffer[i],sizeof(uint32_t));
    }
    outp->close();
  }
// Delete the ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


