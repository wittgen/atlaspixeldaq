//! A program to dump blocks of memory in Master DSP memory space. 
// It does NOT initialize the ROD!! This program is intended to be 
// a diagnostic used to examine memory contents after another
// program, e.g. EchoTest or LedTest, has run.

#include <iostream>
using namespace std;

#include <ctype.h>
#include <fstream>

#include "RodModule.h"

#include "RCCVmeInterface.h"

using namespace SctPixelRod;

int main(int argc, char *argv[]) {

  const uint32_t mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  uint32_t location;                       // Address limits to dump
  char response;                                // Y/n response
  void* genPtr;
  char* textBuff;
  std::string fileName(""), option;
  int slot = -1;
  uint32_t baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if ((slot < 5) || (slot == 13) || (slot > 21)) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 5) || (slot == 13) || (slot > 21)) {
      cout << "Slot number out or range [5:12][14:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule but do not initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);

// get starting address and number of bytes to read

/*  hex(cin);
  cout << "Starting address (hex, without leading 0x): ";
  cin >> location;
  dec(cin);*/
  location = 0x29c1c88;//29c1b08;//0x29c1888;
  busyRecord history[1024];
  uint32_t numWords=sizeof(busyRecord)*256;                                // Number of words to dump
  std::cout << "Dumping " << numWords * 4 << " Bytes" << std::endl;
// read data
  rod0->mdspBlockRead(location, (uint32_t *) &(history[0]), numWords);
  std::cout << std::hex;
// output data
/*  for(int k=0; k<1024; k++) {
    for(int i=0;i<8;i++) {
      for(int j=0;j<4;j++)
        std::cout << history[k].fmt[i].linkOccCount[j] << "\t";
    }
    std::cout << history[k].efb_evtMemFlags << "\t";
    std::cout << history[k].historyIndex ;
    std::cout << std::endl;
  }
  std::cout << std::dec;*/

  string fname("History.bin");
  cout << "Saving ROD PRM Status to " << fname << endl;

  ofstream outFile(fname.c_str(), ios::binary);
    
  // Save file 
  outFile.write((char *)&(history[0]), 4*numWords);

// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


