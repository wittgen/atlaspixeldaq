//------------------------------FlashSave------------------------------ 

/*! 
 *  @brief This is a utility program for dumping the flash memories which
 *  hold the program for the FPGAs to a set of files. 
 *
 *  It prompts the user for the VME slot number of the ROD
 *  This can also be specified on the command line
 *
 *  The Flash data is then read to 5 files.
 *
 *  @author Bruce Gallop after Tom Meyer (meyer@iastate.edu)
 */

#include <iostream>
using namespace std;


#include "RodModule.h"
#include "RCCVmeInterface.h"

using namespace SctPixelRod;

const unsigned long flashStart[5]={0xe00000, 0xe00000, 0xe80000, 0xf00000, 0xf80000};
const long flashSize[5] = {0, 495204, 495212, 336680, 336680};
//const unsigned long flashStart[5]={0xe00000, 0xe01000, 0xe80000, 0xed3000, 0xf26000};
//const long flashSize[5] = {24, 495204, 336688, 336680, 234456};
std::string flashName[5] = {"Location", "ROD Controller", "Formatter", 
                            "Event Fragment Builder", "Router"};
std::string flashFileName[5] = {"Location", "RODController", "Formatter", 
                            "EventFragmentBuilder", "Router"};



int main(int argc, char *argv[]) {
  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves

//   char response;                                // cin response
//   std::string binFileName;                      // Name of binary file to load
//   ifstream binFile;                             // Pointer to binary frile
//   unsigned long flashAddr;                      // Location in target flash
//   int numBytes;                                 // Size of binary file in bytes
//   const unsigned long flashStart[5]={0xe00000, 0xe01000, 0xe80000, 0xed3000, 0xf26000};
//   unsigned long inputAddr;

//   std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      string option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  for(int f=0; f<5; f++) {
    string fname(flashFileName[f] + ".bin");
    cout << "Saving " << flashName[f] << " To " << fname << endl;

    ofstream outFile(fname.c_str(), ios::binary);
//   binFile.open(binFileName.c_str(), ios::binary);

    int byteLength = flashSize[f];
    unsigned int location = flashStart[f];
    
// Create a buffer
    char * buffer;
    try {
      buffer = new char[byteLength];
    }
    catch (std::bad_alloc & ba) {
      cout << "Unable to allocate buffer." << endl;
      exit(1);
    }

// read buffer from flash
    for (int i=0; i<byteLength; i++) {
      buffer[i]=rod0->readByteFromFlash(location++, 1);
    }

    // Save file 
    outFile.write(buffer, byteLength);

    
    delete [] buffer;
  }

  delete rod0;
  delete vme1;

  return 0;  
}


