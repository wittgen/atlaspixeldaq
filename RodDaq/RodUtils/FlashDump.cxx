//------------------------------FlashDump------------------------------ 

/*! 
 *  @brief This is a utility program for dumping the flash memories which
 *  hold the program for the FPGAs onto the screen. 
 *
 *  It prompts the user for the VME slot number of the ROD, for the flash
 *  memory to load, and for the number of bytes to read. The data is written
 *  to the screen in hexadecimal format.  After 256 values are written the
 *  user is given the option to quit or continue.
 *
 *  @author Tom Meyer (meyer@iastate.edu)
 */

#include <iostream>
using namespace std;

#include <ctype.h>
#include <string>
#include <fstream>

#include "RodModule.h"
#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  char response;                                // cin response
  std::string binFileName;                      // Name of binary file to load
  ifstream binFile;                             // Pointer to binary frile
  unsigned long flashAddr;                      // Location in target flash
  int numBytes;                                 // Size of binary file in bytes
  const unsigned long flashStart[5]={0xe00000, 0xe01000, 0xe80000, 0xed3000, 0xf26000};
  unsigned long inputAddr;
  
  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  cout << "Enter address of target flash memory (without leading 0x) or " << endl;
  cout << "     1 for Location File" << endl;
  cout << "     2 for ROD Controller" << endl;
  cout << "     3 for Formatter" << endl;
  cout << "     4 for Event Fragment Builder" << endl;
  cout << "     5 for Router" << endl;
  cout << "Your choice? ";
  cin >> hex >> inputAddr;
  if (inputAddr < 6) {
    flashAddr = flashStart[inputAddr-1];
  }
  else {
    flashAddr = inputAddr;
  }
  cout << "Enter number of bytes to read: ";
  cin >> dec >> numBytes;

  binFile.open(binFileName.c_str(), ios::binary);
    
// Create a buffer
  char * buffer;
  try {
    buffer = new char[numBytes];
  }
  catch (std::bad_alloc & ba) {
    cout << "Unable to allocate buffer." << endl;
    exit(1);
  }
  
// read buffer from flash
  for (int i=0; i<numBytes; i++) {
    buffer[i]=rod0->readByteFromFlash(flashAddr++, 1);
  }

// print contents to stdout
  int itop, tempVal;
  const int rowSize = 16;
  int numRows = (numBytes-1)/rowSize + 1;
  const int rowsPerBlock = 16;
  for (int j=0; j<numRows; j++) {
    if (j == numRows-1) {
      itop = numBytes - j*rowSize;
    }
    else {
      itop = rowSize;
    }
    for (int i=0; i< itop; i++) {
      cout.width(3);
      tempVal = int(buffer[i+rowSize*j]);
      tempVal = 0x00ff&tempVal;
      cout << hex << tempVal << " ";
    }
    cout << endl;
    if (0==(j+1)%rowsPerBlock) { 
      if (j+1 >= numRows) break;
      cout << "'q' to quit, 'c' to continue:";
      cin >> response;
      if ('q' == response | 'Q' == response) break;
    }
  }
  dec(cout);
 
// Clean up before exiting
  delete [] buffer;
  delete rod0;
  delete vme1;

  return 0;  
}


