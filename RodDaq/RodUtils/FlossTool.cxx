// Tool for measuring return currents from a FLOSS PP0 with no light, half clock and full return

#include "RCCVmeInterface.h"
#include "RodModule.h"
#include "BocCard.h"
//#include "registerIndices.h"
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <unistd.h>

#define FMT_DIST                0x00000400
#define FMT_LINK_EN(fmt)        0x00400000 + (fmt * FMT_DIST)

#define COUNTER_BASE            0x00400090
#define FMT_CNT_CONTROL(fmt)    COUNTER_BASE + (fmt * FMT_DIST)
#define FMT_GLB_COUNTER(fmt)    COUNTER_BASE + (fmt * FMT_DIST) + 0x00000004
#define FMT_STREAM_01(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000008
#define FMT_STREAM_23(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000000C
// More definitions for SCT convenience
#define FMT_STREAM_45(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000010
#define FMT_STREAM_67(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000014
#define FMT_STREAM_89(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000018
#define FMT_STREAM_AB(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000001C

// FE Command Mask
#define FE_CMND_MASK_0_LO         0x00404430
#define FE_CMND_MASK_0_HI         (4 + FE_CMND_MASK_0_LO)
#define FE_CMND_MASK_1_LO         (4 + FE_CMND_MASK_0_HI)
#define FE_CMND_MASK_1_HI         (4 + FE_CMND_MASK_1_LO)


// Program to scan all BOC Serial Numbers, Firmware revisions and stuff in one crate


#include <iostream>
#include <iomanip>
using namespace std;

#include <ctype.h>

#include "../RodCrate/BocCard.h"
#include "../../VmeInterface/RCCVmeInterface.h"
#include "parameters.h"

using namespace SctPixelRod;

void sendReturnCommand(RodModule *m_rod, int active, int half=0, int channel=0) { //! Send command no out
  int packSize = 2;
  int pLen = sizeof(SendSerialStreamIn)/sizeof(uint32_t);
  uint32_t *tsdData = new uint32_t[pLen+packSize];
  tsdData[pLen] = (active == 1) ? 0x16b00002 : 0x16b00000;
  tsdData[pLen+1] = (half == 1) ? 0x00000000 : 0x10000000;

  // Set the output mask
  unsigned int mask_lo = 0x0, mask_hi = 0x0;
  if (channel < 32) {
    mask_lo = (1<<channel);
  } else {
    mask_hi = (1<<(channel-32));
  }
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_LO, mask_lo);
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_HI, mask_hi);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_LO, 0x00000000);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_HI, 0x00000000);

  SendSerialStreamIn *sendSerialStreamIn = (SendSerialStreamIn*)tsdData;
  sendSerialStreamIn->sportId  = 0;
  sendSerialStreamIn->nBits    = packSize*32;
  sendSerialStreamIn->fCapture = 1;
  sendSerialStreamIn->nCount   = 1;
  sendSerialStreamIn->dataPtr  = 0;
  sendSerialStreamIn->maskLo   = mask_lo;
  sendSerialStreamIn->maskHi   = mask_hi;

  RodPrimitive* sendStream;
  sendStream = new RodPrimitive(7+packSize, 0, SEND_SERIAL_STREAM, 0, (long*)sendSerialStreamIn);
  try{
    m_rod->executeMasterPrimitiveSync(*sendStream);
  } catch(PrimListException &p) {
    cout << "Caught PrimList exception: " << p << endl;  
  } catch (RodException &r) {
    cout << "Caught ROD exception: " << r << endl;
  } catch (BaseException &b) {
    cout << "Caught BASE exception" << b << endl;
  } catch (...) {
    cout << "Caught unknown exception" << endl;
  }
  delete sendStream;
  delete[] tsdData;
}

int dumpInfo(ostream *outputhandle, BocCard *boc0) {
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(0) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(3) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(4) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(7) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(1) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(2) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(5) << "\t";
  (*outputhandle) << setw(2) << setprecision(2) << boc0->getMonitorAdc(6) << endl;
  return 0;
}

int main(int argc, char *argv[]) {

  unsigned long baseAddress;
  std::string option;

  std::vector<int> slots;

  ofstream outputfile;
  ostream *outputhandle;
  
  outputhandle=&cout;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
      case 's': {
	if (slots.size() < 16) {
	  int slot = atoi(option.substr(2).c_str());
	  if ((slot < 5) || (slot == 13) || (slot > 21)) std::cout << "WARNING: I will ignore stupid slot numbers like " << std::dec << slot << std::endl;
	  else
	    if (std::find(slots.begin(), slots.end(), slot)==slots.end()) {
	      slots.push_back(atoi(option.substr(2).c_str()));
	    } else std::cout << "You already asked for Slot " << std::dec << slot << " - I will not do that twice" << std::endl;
	} else std::cout << "WARNING: Slot numbers given multiple times" << std::endl;
	break;
      }
      case 'a': {
	if (slots.size() == 0) {
	  for (int i=5;i<22;i++) {
	    if (i == 13) continue;
	    slots.push_back(i);
	  }
	} else std::cout << "WARNING: Slot numbers given multiple times" << std::endl;
	break;
      }
      case 'o': {
	outputfile.open(argv[++i], fstream::out);
        outputhandle = &outputfile;
      }
      default: {
	break;
      }
      }
    }
  }

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();

// Prompt for slot number
  if (slots.size() != 0 ) {
    cout << "Slot\tROD\tSerial\tRev.\tBOC\tIRX A\tIRX B\tIRX C\tIRX D\tVRX A\tVRX B\tVRX C\tVRX D" << endl;
    std::vector<int>::iterator it = slots.begin();
    for (;it != slots.end(); it++) {
      baseAddress = (*it) << 24;

// Create RodModule and initialize it
      RodModule *rod0 = NULL;
      try{
        rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
        rod0->initialize();
      } catch (...) {
        cout << (*it) << "\t \t--\t--\t \t--\t--\t--\t--\t--\t--\t--\t--" << endl;
	delete rod0;
        continue;
      }

// Ask the ROD, whether a BOC is available for comm:
      unsigned long rrif_status;
      rrif_status = rod0->mdspSingleRead(0x00404420);
      if (!(rrif_status & 0x4)) {
// Now create BocCard
        BocCard* boc0 = rod0->getBocCard();
// Hard-read the Hardware info into software buffers
        boc0->readInfo();

// Initialise
//         if (bocinitialize) {
// 	  boc0->initialize();
// 	}
// 	if (resetVPin) {
// 	  boc0->resetVpin();
// 	}

//
// Read the "hard-coded" information about the ROD.
        cout << (*it) << "\t \t" << rod0->getSerialNumber() << "\t" << rod0->getRevision() << "\t \t";
        hex(cout);
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(0) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(3) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(4) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(7) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(1) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(2) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(5) << "\t";
        cout << setw(2) << setprecision(2) << boc0->getMonitorAdc(6) << endl;
        dec(cout);

	for (uint32_t formatter=0; formatter<8;formatter++) {
	      rod0->mdspSingleWrite(FMT_LINK_EN(formatter),0x0F); // Look wether there has to be some 89ab !!!
	}

	cout << "Testing Slot " << (*it) << " and writing out..." << endl;

        for(int i=0; i<48; i++) {
          (*outputhandle) << "Channel " << i << " in Slot " << (*it) << endl;
// Make the Module return nada
	  sendReturnCommand(rod0, 0, 0, i);
	  usleep(100);
          dumpInfo(outputhandle, boc0);
// Make the Module return half-clock
	  sendReturnCommand(rod0, 1, 1, i);
	  usleep(100);
          dumpInfo(outputhandle, boc0);
// Make the Module return constant 1
	  sendReturnCommand(rod0, 1, 0, i);
	  usleep(100);
          dumpInfo(outputhandle, boc0);
	  sendReturnCommand(rod0, 0, 0, i);
        }

      } else {
        cout << (*it) << "\t \t" << rod0->getSerialNumber() << "\t" << rod0->getRevision() << "\t \t--\t--\t--\t--\t--\t--\t--\t--" << endl;
      }

      delete rod0;
    }
  } else {
    std::cout << "Instructions (as you haven't given me what I wanted):" << std::endl;
    std::cout << argv[0] << " [-a] [-s<slotnumber>] [-i] [-r]" << std::endl;
    std::cout << "   a : Scan all slots of the crate" << std::endl;
    std::cout << "   r : Reset PiN Voltages on the BOCs RX plugin sites" << std::endl;
    std::cout << "   i : initialize the BOC" << std::endl;
    std::cout << "   s<slotnumber> :" << std::endl
              << "       scan one or multiple (-s<x> -s<y> -s<z>) specific slot(s)" <<std::endl;
    std::cout << "Running without -s/-a gives you this info, as the program doesn't know which slot to run on" << std::endl << "If you use both switches, the later one sets the behaviour..." << std::endl;

    return 0;
  }

  outputfile.close();
  delete vme1;

  return 0;
}


