#include <iostream>
using namespace std;

#include <ctype.h>

#include <sys/time.h>
#include "unistd.h"

#include "RodModule.h"
#include "BocCard.h"

#include "primXface.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  RodPrimList primList(5);                      // Primitive List
  //  std::string SlaveFile("/home/akorn/siDsp_190606/slave.dld");
  std::string SlaveFile("/home/akorn/slave_dmaworksagain.dld");
  std::string infobuf("");
  std::string errorbuf("");
  std::string infobufS("");
  std::string errorbufS("");
  

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  int slaveNumber=-1;
  std::string fileName(""), option;
  bool startScan = true;
  bool initBoc = true;
  int slot = -1;

  unsigned long baseAddress;
  
  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'n': {
          startScan = false;
          break;
        }
        case 'e': {
          initBoc = false;
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          slaveNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    cout << "LED Start init" << endl;
    rod0->initialize(true);
  }
  catch (HpiException &h) {
    hex(cout);
    cout << h.getDescriptor() << '\n';
    cout << "calcAddr: " << h.getCalcAddr() << ", readAddr: " << 
            h.getReadAddr() << '\n';
    dec(cout);
  }
  catch (VmeException &v) {
    cout << "VmeException creating RodModule." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 
  }
  catch (RodException &r) {
  cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
          << '\n';
  }

  //  rod0->loadAndStartSlaves(SlaveFile,0xf);


  cout << "SLAVES LOADED!" << endl;
    for(int i=0; i<5;i++){
      rod0->getTextBuffer(infobufS,i*2,true);
      rod0->getTextBuffer(errorbufS,1+i*2,true);
      if(i>0){
	if(infobufS.length()>0)cout << "INFO S  " << i << " : " << infobufS << endl << endl; 
	if(errorbufS.length()>0)cout << "ERROR S " << i << " : " << errorbufS << endl << endl; 
      }else{
	if(infobufS.length()>0)cout << "INFO M  " << " : " << infobufS << endl << endl; 
	if(errorbufS.length()>0)cout << "ERROR M " << " : " << errorbufS << endl << endl; 
      }
      infobufS.clear();
      errorbufS.clear();
    }


	for(int i=0; i<5;i++){
	  rod0->getTextBuffer(infobufS,i*2,true);
	  rod0->getTextBuffer(errorbufS,1+i*2,true);
	  if(i>0){
	    if(infobufS.length()>0)cout << "INFO S  " << i << " : " << infobufS << endl << endl; 
	    if(errorbufS.length()>0)cout << "ERROR S " << i << " : " << errorbufS << endl << endl; 
	  }else{
	    if(infobufS.length()>0)cout << "INFO M  " << " : " << infobufS << endl << endl; 
	    if(errorbufS.length()>0)cout << "ERROR M " << " : " << errorbufS << endl << endl; 
	  }
	  infobufS.clear();
	  errorbufS.clear();
	}
	  
	RodPrimitive* dummyRead;

	StartTaskIn *startTaskIn = (StartTaskIn*) new uint32_t[((sizeof(StartTaskIn))/sizeof(uint32_t))];
	
	startTaskIn->id = DUMMY_TASK;
	startTaskIn->idMinor = 0;
	startTaskIn->where   = DSP_THIS;
	startTaskIn->dataPtr = 0;
	  
	try {
	  dummyRead = new RodPrimitive(((sizeof(StartTaskIn))/sizeof(uint32_t)), 0, START_TASK, 0, (long*)startTaskIn);
	}
	catch (std::bad_alloc) {
	  cout << "Unable to allocate dummyRead primitive in main." << endl;
	}
	rod0->executeMasterPrimitiveSync(*dummyRead);
	delete dummyRead;

	TalkTaskIn *talkTaskIn = (TalkTaskIn*) new uint32_t[((sizeof(TalkTaskIn))/sizeof(uint32_t))];
	
	talkTaskIn->task = DUMMY_TASK;
	talkTaskIn->topic = 1;
	talkTaskIn->item = 1;

	try {
	  dummyRead = new RodPrimitive(((sizeof(TalkTaskIn))/sizeof(uint32_t)), 0, TALK_TASK, 0, (long*)talkTaskIn);
	}
	catch (std::bad_alloc) {
	  cout << "Unable to allocate talkStatusRead primitive in main." << endl;
	}

	RodOutList* dummyReadAddy; 

  std::string textBufferReturned;

  for(int j=0;j<25;j++){
    sleep(5);

    cout << "Trying to talk to task about: " << talkTaskIn->topic << " with Item: " << talkTaskIn->item << endl;
    rod0->executeMasterPrimitiveSync(*dummyRead,dummyReadAddy);
    uint32_t* messageBody = (uint32_t*)dummyReadAddy->getMsgBody(1);
    TalkTaskOut *tout = (TalkTaskOut *)messageBody;
    uint32_t address = 0;
    if(0==tout->dataPtr){
      address = messageBody[sizeof(TalkTaskOut)/sizeof(uint32_t)];
    }else{
      address = tout->dataPtr;
    }

    textBufferReturned.clear();
    try {
      rod0->getTextBuffer(textBufferReturned, 0, 1);
      cout << "Got this response from the freaking task:" << endl << textBufferReturned << endl;
    } catch (VmeException &v) {
      cout << "Caught VmeException: " << v << endl;
    } catch (...) {
      cout << "Caught something... " << endl;
    }
    rod0->mdspSingleWrite(0x00404400, j);
  }
	
  delete rod0;
  delete vme1;

  return 0;  
}


