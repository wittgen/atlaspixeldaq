/*! A program to dump standard buffers in a Slave DSP memory space. 
   The buffers are PrimBuff, RepBuff, all four text buffers, and the
   status and control registers.
   It does NOT initialize the ROD!! This program is intended to be 
   a diagnostic used to examine the slave DSP after another
   program, e.g. EchoTest or LedTest, has run.
*/

#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  unsigned long numWords;                       // Number of words to dump
  long dspStart;                                // Address limits to dump
  void* genPtr;
  char* textBuff;
  unsigned long statusVal;
  unsigned long txtAddress[4];
  unsigned long slvHpic;
  unsigned long hpicValue;
  clock_t startTime;

  long slvNumber = -1;
  std::string fileName(""), option;
  int slot = -1, ibuf, is;
  unsigned int i, itext, primBuffSize, replyBuffSize;
  unsigned long baseAddress;
  unsigned int mdspStart, mdspMapSize, sdspStart, sdspEnd;
  MdspMemoryMap* myMdspMap;
  SdspMemoryMap* mySdspMap;
  VmePort* myVmePort;
  bool slaveRunning;

  if (argc > 1) {
    for (is=1; is<argc; is++) {
      option = argv[is];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          slvNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule but do not initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  myMdspMap = rod0->getMdspMap();
  mdspStart = rod0->mdspSingleRead(MEMORY_MAP_REG);
  mdspMapSize = rod0->mdspSingleRead(mdspStart);
  myVmePort = rod0->getVmePort();
  mySdspMap = rod0->getSdspMap();
  sdspStart = myMdspMap->load(mdspStart, myVmePort);
  sdspEnd = mySdspMap->load(sdspStart, myVmePort);
  for (i=0; i<N_TXT_BUFFS; i++) {
    txtAddress[i] = myMdspMap->txtBuff(i);
  }

// Get Slave number to read
  if (slvNumber < 0 ) {
    cout << "Enter slave number (0-3):"; 
    cin >> slvNumber;
    while ((slvNumber < 0) || (slvNumber > 3)) {
      cout << "Slave number out or range [0:3], re-enter: ";
      cin >> slvNumber;
    }
  }

// Check if slave is executing. Stop MDSP DMA access, if so.
  slaveRunning = false;
  slvHpic = SLAVE_HPIC_BASE + slvNumber*SLAVE_HPI_OFFSET;
  hpicValue = rod0->mdspSingleRead(slvHpic);
  if (hpicValue == 0xb000b) {
    slaveRunning = true;
    rod0->setVmeCommandRegBit(CR_DMA_ACCESS_REQ);
    startTime = clock();
    do {
      statusVal = rod0->slvSingleRead(STATUS_REG[0], slvNumber);
      if (statusVal == 0x1>>SR_DMA_ACCESS_ERR) {
        cout << "Error setting slave DMA ACCESS bit. Program terminating" << endl;
        return 0;
      }
      if ((clock()-startTime)/CLOCKS_PER_SEC > DSP_RESET_TIMEOUT) {
        cout << "Timeout waiting for SR_ACCESS_ACK" << endl;
      }
    } while (statusVal & 0x1>>SR_DMA_ACCESS_ACK);
  }
       
  dspStart = myMdspMap->flashBase();
  numWords = rod0->slvSingleRead(dspStart, slvNumber);
  primBuffSize = myMdspMap->primBufferSize();
  if (numWords > primBuffSize) numWords = primBuffSize;
  unsigned long * buffer = new unsigned long[numWords];
  rod0->slvBlockRead(dspStart, buffer, numWords, slvNumber);
  
// output the data and delete the buffer
  cout << "PRIM Buffer:" << endl;
  hex(cout);
  cout.fill('0');
  for (i=0; i<numWords; i+=8) {
    cout.width(8); cout << buffer[i]   << ' ';
    cout.width(8); cout << buffer[i+1] << ' ';
    cout.width(8); cout << buffer[i+2] << ' ';
    cout.width(8); cout << buffer[i+3] << ' ';
    cout.width(8); cout << buffer[i+4] << ' ';
    cout.width(8); cout << buffer[i+5] << ' ';
    cout.width(8); cout << buffer[i+6] << ' ';
    cout.width(8); cout << buffer[i+7] << endl;
  }
  dec(cout);
  cout.fill(' ');
  delete [] buffer;
    
// Get the REPLY buffer
    dspStart = myMdspMap->replyBuffer();
    numWords = rod0->slvSingleRead(dspStart, slvNumber);
    replyBuffSize = myMdspMap->replyBufferSize();
    if (numWords > replyBuffSize) numWords = replyBuffSize;
    buffer = new unsigned long[numWords];
    rod0->slvBlockRead(dspStart, buffer, numWords, slvNumber);
  
// output the data and delete the buffer
    cout << "REPLY Buffer:" << endl;
  hex(cout);
  cout.fill('0');
  for (i=0; i<numWords; i+=8) {
    cout.width(8); cout << buffer[i]   << ' ';
    cout.width(8); cout << buffer[i+1] << ' ';
    cout.width(8); cout << buffer[i+2] << ' ';
    cout.width(8); cout << buffer[i+3] << ' ';
    cout.width(8); cout << buffer[i+4] << ' ';
    cout.width(8); cout << buffer[i+5] << ' ';
    cout.width(8); cout << buffer[i+6] << ' ';
    cout.width(8); cout << buffer[i+7] << endl;
  }
  dec(cout);
  cout.fill(' ');
  delete [] buffer;

// Read and print the status and command registers  
  hex(cout);
  for (is=0; is<3; is++) {
    statusVal = rod0->slvSingleRead(STATUS_REG[is], slvNumber);
    cout << "Status register " << is << " (hex)  = ";
    cout.width(8);
    cout << statusVal << endl;
  }
  statusVal = rod0->slvSingleRead(COMMAND_REGISTER, slvNumber);
  cout << "Command register = ";
  cout.width(8);
  cout << statusVal << endl;
  dec(cout);
    
// Read and print all four text buffers in both hex and ascii
  numWords = 512;  
  for (ibuf=0; ibuf<4; ibuf++) {
    dspStart = txtAddress[ibuf];
    unsigned long * buffer = new unsigned long[numWords];
    rod0->slvBlockRead(dspStart, buffer, numWords, slvNumber);
    switch(ibuf) {
      case 0: cout << endl << "Hexadecimal dump of ERR buffer:" << endl;
              break;
      case 1: cout << endl << "Hexadecimal dump of INFO buffer:" << endl;
              break;
      case 2: cout << endl << "Hexadecimal dump of DIAG buffer:" << endl;
              break;
      case 3: cout << endl << "Hexadecimal dump of XFER buffer:" << endl;
              break;
      }
    hex(cout);
    cout.fill('0');
    for (i=0; i<numWords; i+=8) {
      cout.width(8); cout << buffer[i]   << ' ';
      cout.width(8); cout << buffer[i+1] << ' ';
      cout.width(8); cout << buffer[i+2] << ' ';
      cout.width(8); cout << buffer[i+3] << ' ';
      cout.width(8); cout << buffer[i+4] << ' ';
      cout.width(8); cout << buffer[i+5] << ' ';
      cout.width(8); cout << buffer[i+6] << ' ';
      cout.width(8); cout << buffer[i+7] << endl;
    }
    dec(cout);
    cout.fill(' ');
    genPtr = static_cast<void*>(buffer);
    textBuff = static_cast<char*>(genPtr);
    switch(ibuf) {
      case 0: cout << endl << "ASCII dump of ERR buffer:" << endl;
              break;
      case 1: cout << endl << "ASCII dump of INFO buffer:" << endl;
              break;
      case 2: cout << endl << "ASCII dump of DIAG buffer:" << endl;
              break;
      case 3: cout << endl << "ASCII dump of XFER buffer:" << endl;
              break;
      }
      for (itext = 0; itext< 4*numWords; itext++) {
        if ((textBuff[itext]<' ') || (textBuff[itext] > '~')) textBuff[itext] = '.';
        cout << textBuff[itext];
        if ((itext+1)%64 == 0) cout << endl;
      }
      cout << endl;
        
    delete [] buffer;
  }

// Restart master dma access if it was halted
  if (slaveRunning) {
    slaveRunning = false;
    rod0->clearVmeCommandRegBit(CR_DMA_ACCESS_REQ);
    do {
      statusVal = rod0->slvSingleRead(STATUS_REG[0], slvNumber);
      if (statusVal == 0x1>>SR_DMA_ACCESS_ERR) {
        cout << "Error setting slave DMA ACCESS bit. Program terminating" << endl;
        return 0;
      }
    } while ((statusVal & 0x1>>SR_DMA_ACCESS_ACK));
  } 
  
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


