#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves

  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  unsigned long flashAddr, flashAddrInit = 0xE00100;
  uint8_t readData, dataVal, dataInit=0xA0;
  
  flashAddr = flashAddrInit;
  dataVal = dataInit;

// Erase sector
  unsigned long sectorAddress=0xE00000;
  rod0->sectorErase(sectorAddress);
  
// write sixteen bytes to flash and read them back
  for (int i=0; i<16; i++) {
    rod0->writeByteToFlash(flashAddr, dataVal++);
    flashAddr+=1;
  }

  flashAddr = flashAddrInit;
  dataVal = dataInit;
  for (int i=0; i<16; i++) {
    readData = rod0->readByteFromFlash(flashAddr, 1);
    flashAddr+=1;
    if (readData != dataVal++) {
      cout << "Byte I/O error" << endl;
    }
  } 

// Erase sector
  rod0->sectorErase(sectorAddress);
  
// create 256-byte buffer, write it to flash and read it back
  unsigned long blockLength=256;
  uint8_t outBlock[blockLength], inBlock[blockLength];
  for (int i; i<256; i++) {
    outBlock[i] = i;
  }
  
  rod0->writeBlockToFlash(flashAddr, outBlock, blockLength);
  rod0->readBlockFromFlash(flashAddr, inBlock, blockLength);
 
  for (unsigned int i=0; i<blockLength; i++) {
    if (inBlock[i] != outBlock[i]) {
      cout << "Block I/O error." << endl;
    }
  } 

// Erase chip
  rod0->chipEraseHpi();
  
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


