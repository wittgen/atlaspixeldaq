#include <unistd.h>
#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {
  VmeInterface *vme=nullptr;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  int bcidOffset;
  int delay;
  float tfreq;
  int fftvthr, fftvtol, emergthr, emergcntleak, bmode, bthr, ttcdebugmode;
  int reg_val;
  try{
    uint16_t timID, timfirm, timSN;
    int a_opmode=0;
    int i,regnum;
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    const int NumOfTries = 10;
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    timID = tim->fetchTimID();
    timSN = (timID & 0xFF);
    timfirm = (timID >> 8);
    //if (timSN >= 0x20) timfirm += 0xF00; // FPGA
    hex(cout);
    cout << "General TIM information (all dec unless specified):" << endl;
    cout << "TIM serial number (hex): " << hex <<timSN << endl;
    cout << "TIM firmware (hex): " << hex << timfirm << endl;
    dec(cout);
    //==============================================================
    tim->loadBitSet(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_FVDISABLE); // bypass repetitive trigger veto
    do {
	cout << "Main TIM operation menu: " << endl;
        cout << "     0 - quit " << endl;
	cout << "     1 - init TIM" << endl;
	cout << "     2 - Configure TTC keeping TIM busy" << endl;
	cout << "     3 - Release Busy in TTC mode" << endl;
        cout << "     4 - test registers" << endl;
	cout << "     5 - Clear an FFTV-T emergency state" << endl;
	cout << "     6 - Clear an FFTV-B emergency state" << endl;
	cout << "     7 - Configure TIM internal" << endl;
	cout << "     8 - Release Busy in internal mode" << endl;
	cout << "     9 - Set FFTV match threshold" << endl;
	cout << "    10 - Set FFTV match tolerance" << endl;
	cout << "    11 - Set FFTV emergency threshold" << endl;
	cout << "    12 - Set FFTV-B snapshot bit" << endl;
	cout << "    13 - Clear FFTV-B snapshot bit" << endl;
	cout << "    14 - Clear counters of veto'ed triggers, FFTV and RODBusy" << endl;
	cout << "    15 - Enable & Set TTC debug mode" << endl; 
	cout << "    16 - Disable TTC debug mode" << endl; 
	cin >> dec >> a_opmode;
	switch (a_opmode)
	  {
	  case 1:
	    tim->reset(); // this reset disables clk-switch busy!
	    tim->loadBitClear(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_CSBDISABLE); //turn clock-switching BUSY back on, which is off by reset
	    tim->loadBitSet(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_SARBDISABLE);  //disable SA-mode setting RodBusy
	    tim->loadBitSet(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_FVDISABLE);    //disable FFTVs
	    //
	    //reset QPLL
	    //
	    //tim->ttcRxRead(0x06);
	    //tim->ttcRxWrite(0x6,0x1);
	    break;
	  case 2: 
	    tim->loadBitClear(TIM_REG_DEBUG_CTL, TIM_BIT_DEBUGCTL_FVDISABLE); //enable FFTVs
	    tim->loadBitSet(TIM_REG_COMMAND, TIM_VROD_BUSY); //set vme busy
	    tim->ttcRxWrite(0x3,0xb3);
	    tim->setupTTC();
	    tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY);
	    tim->loadBitSet(TIM_REG_COMMAND, TIM_VROD_BUSY);
	    tim->regLoad(TIM_REG_QPLL_CTL,0xa0);
	    tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_QPLLCONTROL);
	    tim->loadBitClear(TIM_REG_QPLL_CTL,0x0020);
	    usleep(10);
	    tim->loadBitSet(TIM_REG_QPLL_CTL,0x0020);
	    /*
	    //============ Setup TIM into RUN mode =======================
	    //tim->loadBitSet(TIM_REG_BUSY_EN3, 0x4); //enable vme busy stop triggers in TIM
	    tim->setupTTC(); //enable TTC signals and output streams, ensure no external/internal SA signals
	    if (delay>0) tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY); //Busy from RODs will not stop TIM internal triggers 
	    //tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_ECR);
	    //tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_BCR);
	    //tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_FER);
	    //tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_CAL);
	    //tim->loadBitClear(TIM_REG_RUN_ENABLES, TIM_BIT_EN_TTC_SPA);
	    //================ Enable TTCrx output bus to TIM ================================
	    tim->regLoad(TIM_REG_TTCRX_CTL, 0xc3b3); //enable TTCrx output bus to TIM
	    //    c - I2C_GO, I2C_ABORT, write , 0
	    //    3 - command register of TTCrx                                          
	    //    b3 - enable External bus for Dout and DoutStr
	    while ((i<timeout)&&(busy==1)) {
	      //std::cout << "Still busy...  " << std::endl;
	      i++;
	      reg_val = tim->regFetch(TIM_REG_TTCRX_CTL);
	      busy = (reg_val & 0x8000) >> 15;
	    }
	    if (i >= timeout) std::cout << "TIMEOUT: TTCrx: " << tim->regFetch(TIM_REG_TTCRX_CTL) << " i: "<< i << std::endl; 
	    //==============================================================
	    */
	    /*
	    //std::cout << "Let it run during data taking, then press 'q'...  " << std::endl;
	    //char ch;
	    //while (true) {ch = getchar();if(ch == 'q') break;}
	    */
	 
	    //temporary for ROD 20
	    //tim->loadBitSet(TIM_REG_ROD_MASK, 0x4000);
	    //============ get&set RUN parameters =======================
	    cout << "Enter BCID offset (0-4095): ";
	    cin >> bcidOffset;
	    regnum = 0x66;
	    tim->vmeLoad(regnum,(bcidOffset & 0xfff));
	    //tim->regLoad( TIM_REG_BCID, (bcidOffset & 0xf) << 12 ); // set BCID offset to 2
	    cout << "Enter trigger delay (0-255): ";
	    cin >> delay;
	    tim->loadByteLo( TIM_REG_DELAY, (delay & 0xff));
	    std::cout << "Conf TIM in TTC mode with delay "<< delay << " and BCID offset: "<< bcidOffset << " done...  " << std::endl;
	    //
	    // set FFTV-T parameters
	    //
	    cout << "Enter FFTV-T threshold (2-10): ";
	    cin >> fftvthr;
	    tim->loadByteLo( TIM_REG_FV_MATCH, (fftvthr & 0x000f));
	    std::cout << "FFTV-T match threshold: "<<fftvthr << std::endl;
	    std::cout << "Enter FFTV-T threshold tolerance: (400-40): ";
	    cin >> fftvtol;
	    //            tim->loadByteLo( TIM_REG_FV_DELTAT, (fftvtol & 0x00ff));
	    tim->regLoad( TIM_REG_FV_DELTAT, (fftvtol & 0xffff));
	    std::cout << "FFTV-T threshold tolerance: "<<fftvtol << std::endl;
	    cout << "Enter FFTV-T Emergency threshold (10-255): ";
            cin >> emergthr;
	    std::cout << "FFTV-T emergency threshold: "<<emergthr << std::endl;
	    tim->loadByteHi( TIM_REG_FV_MATCH, emergthr & 0xff);
	    cout << "Enter FFTV-T Emergency counter leak rate in s (0-65536): ";
            cin >> emergcntleak;
	    std::cout << "FFTV-T Emergency counter leak rate in s: "<<emergcntleak << std::endl;
	    tim->regLoad( TIM_REG_FV_EMERG_TO, emergcntleak & 0xffff);
	    //
	    //set FFTV-B parameters
	    //
	    cout << "Enter FFTV-B threshold (0-31): ";
            cin >> bthr;
	    std::cout << "FFTV-B threshold threshold: "<<bthr << std::endl;
	    tim->regLoad( TIM_REG_FVB_MATCH, (bthr & 0x00ff));
	    cout << "Decrement by 2 mode (0-1): ";
            cin >> bmode;
	    std::cout << "Decrement by 2 mode: "<<bmode << std::endl;
	    if (bmode) tim->loadBitSet(TIM_REG_CONTROL,TIM_BIT_CTL_FVB_DECR_MODE); //set +1,-2 counting option
	    else tim->loadBitClear(TIM_REG_CONTROL,TIM_BIT_CTL_FVB_DECR_MODE);
	    break;
	  case 3:
	    /*
	    cout << "Enter ROD BUSY mask: ";
	    cin >> hex >> rbmask;
	    tim->loadBitSet(TIM_REG_RB_MASK, rbmask);
	    std::cout << "Setting ROD BUSY mask: "<< rbmask << std::endl;
	    */
	    sleep(2);
	    tim->loadBitClear(TIM_REG_COMMAND, TIM_VROD_BUSY);
	    tim->regLoad(TIM_REG_BCOUNTL,0x0000); //clear the busy duration register
	    tim->regLoad(TIM_REG_BCOUNTH,0x0000);
	    tim->regLoad(TIM_REG_BCOUNTX,0x0000);
	    break;
 	  case 4:
	    regnum=0;
	    while (regnum<160) {
	      if ((regnum < 0x0c) && (regnum > 0x18))
	      for (i=0;i<NumOfTries;i++) {
		reg_val = rand()% 0x10000;
		tim->vmeLoad(regnum, reg_val);
		cout << hex << i <<"written "<< reg_val << " into reg. 0x" << hex << regnum << " read: " << tim->vmeFetch(regnum) << endl;
	      }
	      regnum+=2;
	    }
	    break;
	  case 5:
            tim->loadBitSet(TIM_REG_CONTROL, TIM_BIT_CTL_FVTI_EMRGCY_CLEAR);
	    tim->loadBitClear(TIM_REG_CONTROL, TIM_BIT_CTL_FVTI_EMRGCY_CLEAR);
            break;
	  case 6:
	    //tim->loadBitSet(TIM_REG_CONTROL, 0x0008);	      
	    //read (10+3)*(10+2) triggers (+more)
	    //for (int idx=0;idx<readDepth;idx++) {
	    /*
	    do {
	      fifoinfo = (unsigned int)(tim->regFetch( TIM_REG_TP_FIFOL ) + (tim->regFetch( TIM_REG_TP_FIFOH )<<16));
	      cout << fifoinfo << endl;
	    } while (!((fifoinfo>>30)&0x1));
	    cout << "trigger period: " << fifoinfo << endl;
	    cout << "trigger frequency: " << 1.0/((double)fifoinfo*25e-9) << endl;
	    */
	    tim->loadBitSet(TIM_REG_CONTROL, TIM_BIT_CTL_FV_EMRGCY_CLEAR);
	    tim->loadBitClear(TIM_REG_CONTROL, TIM_BIT_CTL_FV_EMRGCY_CLEAR);

            break;
	  case 7:
	    tim->setupVME();

	    tim->loadBitSet(TIM_REG_BUSY_EN3, TIM_BIT_BUSY_EN3_VME_BUSY4SA_L1ASTOP | TIM_BIT_BUSY_EN3_VME_BUSY2TB_FP);
	    tim->loadBitSet(TIM_REG_COMMAND, TIM_VBUSY);
	    /*
	    cout << "Enter trigger delay (0-255): ";
	    cin >> delay;
	    tim->loadByteLo( TIM_REG_DELAY, (delay & 0xff));
	    */
	    cout << "Enter trigger frequency in kHz (0.0000001-40000): ";
            cin >> tfreq;
	    unsigned long clkPeriodBCO;
	    clkPeriodBCO = (unsigned long)(40000.0/tfreq);
	    tim->regLoad(TIM_REG_TROSC2_PERL, clkPeriodBCO & 0x0000ffff);
	    tim->regLoad(TIM_REG_TROSC2_PERH, (clkPeriodBCO & 0xffff0000) >> 16);

	    //setECRFreq(m_ECRFrequency);

	    //cout << "Enter trigger type (0-4095): ";
	    //cin >> ttype;
	    //tim->regLoad( TIM_REG_TTID, ttype );

	    //enableRodBusyStopSATriggers();
	    tim->loadBitSet( TIM_REG_RUN_ENABLES, TIM_BIT_EN_ROD_BUSY );
	    tim->loadBitSet(TIM_REG_ENABLES3, TIM_BIT_EN_OSC2);

	    break;
	  case 8:
	    //release VME TIM BUSY
	    tim->loadBitClear(TIM_REG_COMMAND, TIM_VBUSY);
	    break;
	  case 9:
	    cout << "Enter FFTV threshold (2-10, def 10): ";
            cin >> fftvthr;
            tim->loadByteLo( TIM_REG_FV_MATCH, (fftvthr & 0x000f));
	    std::cout << "FFTV match threshold: "<<fftvthr << std::endl;
	    break;
	  case 10:
	    cin >> fftvtol;
            tim->regLoad( TIM_REG_FV_DELTAT, (fftvtol & 0xffff));
	    std::cout << "FFTV threshold tolerance (36-65535, def 400): "<<fftvtol << std::endl;
	    break;
	  case 11:
	    cout << "Enter FFTV Emergency threshold (10-255, def 10): ";
            cin >> emergthr;
	    std::cout << "FFTV emergency threshold: "<<emergthr << std::endl;
            tim->loadByteHi( TIM_REG_FV_MATCH, emergthr & 0xff);
            break;
	  case 12:
            tim->loadBitSet(TIM_REG_CONTROL, TIM_BIT_CTL_FVB_SNAP);
            break;
	  case 13:
	    tim->loadBitClear(TIM_REG_CONTROL, TIM_BIT_CTL_FVB_SNAP);
            break;
	  case 14:
	    //                                          clear:
	    tim->regLoad(TIM_REG_FV_COUNTL,0x0000);  //FFTV-T BUSY
	    tim->regLoad(TIM_REG_FV_IDL,0x0000);     //FFTV-T counts
	    
	    tim->regLoad(TIM_REG_FVB_COUNTL,0x0000); //FFTV-B BUSY
	    tim->regLoad(TIM_REG_FVB_IDL,0x0000);    //FFTV-B counts 
	    
	    tim->regLoad(TIM_REG_BCOUNTL,0x0000);    //overall BUSY
	    tim->regLoad(TIM_REG_FV_TCOUNTL,0x0000); //FFTVeto'ed triggers
	    break;
	  case 15:
	    cout << "Enter debug mode 0-clock, 1-strobes: ";
            cin >> ttcdebugmode;
	    if (ttcdebugmode) tim->loadBitSet(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUGMODE_TRO);
	    else tim->loadBitClear(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUGMODE_TRO);
	    tim->loadBitSet(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUG_TRIO);
	    break;
	  case 16:
	    tim->loadBitClear(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_ENABLE_TTCDEBUG_TRIO);
	    break;
	  default: 
	    cout << "Please correct your choice ..." << endl;
	    break;
	  }
    }
    while (a_opmode);
    delete tim;
    delete vme;
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}

uint16_t YesNo(uint16_t regstatus)
{
  if (regstatus & 0x0001) cout << "Yes"<< endl;
      else cout << "No"<< endl;
      return regstatus>>1; 
}
