#include <iostream>
using namespace std;

#include "RodModule.h"
#include "TimModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  const int rodReset = 0x40;
    
  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;
  unsigned long time = 0;
  unsigned long rate = 1;
  int pattern = 0;
  unsigned long value;
  TimModule* tim0=nullptr;
  char use_tim;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
// Prompt for time to run
  cout << "Enter time to send events (in seconds):";
  cin >> time;

// Prompt for number of pattern sets
  cout << "Enter number of patterns per event:";
  cin >> pattern;
//Turn this into the value to set into the register
  pattern--;
  pattern<<=24;

//Prompt for TIM use
  cout << "Use TIM for triggers (y/n)? ";
  cin >> use_tim;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  if(use_tim == 'y') {
//Create TimModule
    cout << "Using TIM for triggers" << endl;
    tim0 = new TimModule(0x0d000000,0x10000,*vme1);
    tim0->reset();
    tim0->initialize();
    tim0->regLoad(TIM_REG_ROD_MASK,0xFFFF);
    tim0->loadBitSet(TIM_REG_BUSY_EN3,TIM_BIT_BUSY_EN3_ENRBBUSY);
    cout << hex << "TIM Busy Enable3 register: " << tim0->regFetch(TIM_REG_BUSY_EN3) << dec << endl;
    tim0->loadBitSet(TIM_REG_DEBUG_CTL,TIM_BIT_DEBUGCTL_FVDISABLE);
    cout << hex << "TIM Debug Control register: " << tim0->regFetch(TIM_REG_DEBUG_CTL) << dec << endl;
//Set bit 22 in register.
    pattern |= 0x400000;
  }

// Reset the ROD
  cout << "Reset ROD" << endl;
  rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);
  rod0->sleep(5000);

// Set bits 24-29 in IDE_MEM_CTRL register. This sets the number
// of times the pattern repeats in an event.
// Set bit 20 in IDE_MEM_CTRL register. This sets events going
// at 105kHz. Then sleep for requested period before stopping.
//

  cout << "Start sending events" << endl; 
  rod0->hpiLoad(HPIC,0x00010001);

  rod0->mdspSingleWrite(0x404470,pattern);

  value = rod0->mdspSingleRead(0x404470);
  cout << "MEM_IDE_CTRL Register contains: " << hex << value << dec << endl;

  rod0->mdspSingleWrite(0x404470,(pattern|0x100000));

  if(use_tim == 'y') {
    cout << "Enter TIM Trigger rate in kHz (1, 10 or 100): ";
    cin >> rate;
    if(rate == 100) {
      tim0->intTrigStart(TIM_MASK_TRIG_100_KHZ);
    }
    else if(rate == 10) {
      tim0->intTrigStart(TIM_MASK_TRIG_10_0KHZ);
    }
    else {
      tim0->intTrigStart(TIM_MASK_TRIG_1_00KHZ);
    }
  }

  rod0->sleep((u_long)(time*1000));

  if(use_tim == 'y') {
    tim0->intTrigStop();
    rod0->sleep(1000);
    value = tim0->fetchL1ID();
    cout << "TIM L1ID register = " << value << endl;
    value = tim0->regFetch(TIM_REG_BCOUNTL);
    cout << "TIM Busy count register = " << value << endl;
  }
  value = rod0->mdspSingleRead(0x402218);
  cout << "EFB_RUNTIME_STAT_REG Register contains: " << hex << value << dec << endl;
  rod0->mdspSingleWrite(0x404470,(pattern|0x000000));

// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


