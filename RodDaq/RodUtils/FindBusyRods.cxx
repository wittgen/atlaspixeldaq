#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {
  VmeInterface *vme;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  try{
    uint16_t rbStat, rbMask , rbNewMask , timID, timfirm, timSN;
    int rn, inp;
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    
    rbMask=tim->regFetch(TIM_REG_RB_MASK);
    rbNewMask = rbMask;
    rbStat=tim->regFetch(TIM_REG_RB_STAT);
    for (int i=0;i<16;i++) {
      rn = i+5; if (rn >= 13) rn++;
      if (rbStat&0x1 && rbMask&0x1) {
	cout << "Found ROD "<< rn << " busy. Disable ? (1/0) ";
	cin>>inp;
	if (inp == 1) rbNewMask&= ~(0x1<<i);
      }
      //cout << hex <<~(0x1<<i) << dec << endl;
      rbStat>>=1;
      rbMask>>=1;
    }
    tim->regLoad(TIM_REG_RB_MASK,rbNewMask);
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}
