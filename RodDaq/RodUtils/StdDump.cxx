#include <iostream>
#include <iomanip>

using namespace std;

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const uint32_t mapSize=0xc00040;         // Map size
  const int32_t numSlaves=4;                       // Number of slaves
  
  std::string option;
  int slot = -1;
  uint32_t baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  int32_t numWords[4]         = {0x2000, 0x270, 0x120, 0x800};
  uint32_t dspStart[4]    = {0x400000,0x402000, 0x402400, 0x404400 };
  string filename[4]               = {"Formatter", "Efb", "Router", "Rcf"};

  for(int i = 0;i<4;i++){
// create buffer and read data
    uint32_t * buffer = new uint32_t[numWords[i]];
    rod0->mdspBlockRead(dspStart[i], buffer, numWords[i]);
    std::cout << "Dumping " << filename[i] << std::endl;
    
    std::ostringstream onam;
    onam << filename[i];
    onam << ".bin";
    std::ofstream fout(onam.str().c_str(),ios::binary);
    fout.write((char *)buffer, 4*numWords[i]); 
  }

//read input FIFO, inMEM
    for(int i=0;i<2;i++){
 
      std::vector<unsigned int> out;
      std::ostringstream onam;
      if(i==0){
          //OldDsp: rod0->readFifo(INPUT_MEM, BANK_A, 0x8000, out);
          rod0->readFifo(FIFO_INMEMA, 0, 0x8000, out);
          onam << "IN_MEM_A.bin";
      }
      else if(i==1){
          //OldDsp: rod0->readFifo(INPUT_MEM, BANK_B, 0x8000, out);
          rod0->readFifo(FIFO_INMEMB, 0, 0x8000, out);
 
          onam << "IN_MEM_B.bin";
      }
      std::ofstream fout(onam.str().c_str(),ios::binary);
      for (unsigned int j=0; j<out.size(); j++) {
        fout.write((char *)&(out[j]),4);
      }
      fout.close();
    }
  
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


