/* routine to decode a primitive list buffer that      */
/* is send to the ROD                                  */
/* required argument is the binary input file          */
/* written by A. Korn LBNL: Andreas.Korn@cern.ch       */
/* $Id: */

#include <unistd.h>
#include <stdio.h>
#include <iostream>

#include "moduleConfigStructures.h"
/* Prim List Header structs*/
#include "primXface.h"
#include "scanOptions_2.h"

#define MSG_HEAD MsgHead
#define MSG_LIST_HEAD MsgListHead
#define MSG_LIST_TAIL MsgListTail

/*=============================================================================
 *                               writeConfigModule()
 *=============================================================================*/
int32_t writeConfigModule(PixelModuleConfig *Module){

	int chipIndex, i, j;
  
  	PixelFEConfig *FEConfig;

	//----------------------------
	printf("struct PixelModule:\n===================\n");
    
	printf( "maskEnableFEConfig: 0x%x\n", Module->maskEnableFEConfig);
	printf( "maskEnableFEScan  : 0x%x\n", Module->maskEnableFEScan);
	printf( "maskEnableFEDacs  : 0x%x\n\n", Module->maskEnableFEDacs);
	
	printf( "present: %d\n", Module->present); 
	printf( "active : %d\n", Module->active);
	printf( "groupId: %d\n", Module->groupId);

	/* MCC configuration */	
	printf("\nMCCRegisters:\n-------------------\n");
	
	printf( "regCSR : 0x%x\n", Module->MCCRegisters.regCSR); 
	printf( "regLV1 : 0x%x\n", Module->MCCRegisters.regLV1);
	printf( "regFEEN: 0x%x\n", Module->MCCRegisters.regFEEN);
	printf( "regWFE : 0x%x\n", Module->MCCRegisters.regWFE);
	printf( "regWMCC: 0x%x\n", Module->MCCRegisters.regWMCC);
	printf( "regCNT : 0x%x\n", Module->MCCRegisters.regCNT);
	printf( "regCAL : 0x%x\n", Module->MCCRegisters.regCAL);
	printf( "regPEF : 0x%x\n", Module->MCCRegisters.regPEF);
	  	
	
    /* FE configurations: */      
    for(chipIndex=0; chipIndex<N_PIXEL_FE_CHIPS; ++chipIndex){
    
    	printf("\nFEConfig[%d]:\n-------------------\n", chipIndex);
    	
    	FEConfig=&Module->FEConfig[chipIndex];
    	printf( "FEConfig.FEIndex: %d\n\n", FEConfig->FEIndex);
    	
		printf( "FECommand.address: %d\n", FEConfig->FECommand.address); 
		printf( "FECommand.command: 0x%x\n", FEConfig->FECommand.command);

		printf( "\nFEGlobal.latency            : %d\n", FEConfig->FEGlobal.latency          );
		printf( "FEGlobal.dacIVDD2           : %d\n", FEConfig->FEGlobal.dacIVDD2           );
		printf( "FEGlobal.dacIP2             : %d\n", FEConfig->FEGlobal.dacIP2             );
		printf( "FEGlobal.dacID              : %d\n", FEConfig->FEGlobal.dacID              );
		printf( "FEGlobal.dacIP              : %d\n", FEConfig->FEGlobal.dacIP              );
		printf( "FEGlobal.dacITRIMTH         : %d\n", FEConfig->FEGlobal.dacITRIMTH         );
		printf( "FEGlobal.dacIF              : %d\n", FEConfig->FEGlobal.dacIF              );
		printf( "FEGlobal.dacITH1            : %d\n", FEConfig->FEGlobal.dacITH1            );
		printf( "FEGlobal.dacITH2            : %d\n", FEConfig->FEGlobal.dacITH2            );
		printf( "FEGlobal.dacIL              : %d\n", FEConfig->FEGlobal.dacIL              );
		printf( "FEGlobal.dacIL2             : %d\n", FEConfig->FEGlobal.dacIL2             );
		printf( "FEGlobal.dacITRIMIF         : %d\n", FEConfig->FEGlobal.dacITRIMIF         );
		printf( "FEGlobal.dacSpare           : %d\n", FEConfig->FEGlobal.dacSpare           );
		printf( "FEGlobal.threshTOTMinimum   : %d\n", FEConfig->FEGlobal.threshTOTMinimum   );
		printf( "FEGlobal.threshTOTDouble    : %d\n", FEConfig->FEGlobal.threshTOTDouble    );
		printf( "FEGlobal.capMeasure         : %d\n", FEConfig->FEGlobal.capMeasure         );
		printf( "FEGlobal.gdac               : %d\n", FEConfig->FEGlobal.gdac               );
		printf( "FEGlobal.muxTestPixel       : %d\n", FEConfig->FEGlobal.muxTestPixel       );
		printf( "FEGlobal.dacMonLeakADC      : %d\n", FEConfig->FEGlobal.dacMonLeakADC      );
		printf( "FEGlobal.dacVCAL            : %d\n", FEConfig->FEGlobal.dacVCAL            );
		printf( "FEGlobal.widthSelfTrigger   : %d\n", FEConfig->FEGlobal.widthSelfTrigger   );
		printf( "FEGlobal.muxDO              : %d\n", FEConfig->FEGlobal.muxDO              );
		printf( "FEGlobal.muxMonHit          : %d\n", FEConfig->FEGlobal.muxMonHit          );
		printf( "FEGlobal.muxEOC             : %d\n", FEConfig->FEGlobal.muxEOC             );
		printf( "FEGlobal.frequencyCEU       : %d\n", FEConfig->FEGlobal.frequencyCEU       );
		printf( "FEGlobal.modeTOTThresh      : %d\n", FEConfig->FEGlobal.modeTOTThresh      );
		printf( "FEGlobal.enableTimestamp    : %d\n", FEConfig->FEGlobal.enableTimestamp    );
		printf( "FEGlobal.enableSelfTrigger  : %d\n", FEConfig->FEGlobal.enableSelfTrigger  );
		printf( "FEGlobal.spare              : %d\n", FEConfig->FEGlobal.spare              );
		printf( "FEGlobal.monMonLeakADC      : %d\n", FEConfig->FEGlobal.monMonLeakADC      );
		printf( "FEGlobal.monADCRef          : %d\n", FEConfig->FEGlobal.monADCRef          );
		printf( "FEGlobal.enableMonLeak      : %d\n", FEConfig->FEGlobal.enableMonLeak      );
		printf( "FEGlobal.statusMonLeak      : %d\n", FEConfig->FEGlobal.statusMonLeak      );
		printf( "FEGlobal.enableCapTest      : %d\n", FEConfig->FEGlobal.enableCapTest      );
		printf( "FEGlobal.enableBuffer       : %d\n", FEConfig->FEGlobal.enableBuffer       );
		printf( "FEGlobal.enableVcalMeasure  : %d\n", FEConfig->FEGlobal.enableVcalMeasure  );
		printf( "FEGlobal.enableLeakMeasure  : %d\n", FEConfig->FEGlobal.enableLeakMeasure  );
		printf( "FEGlobal.enableBufferBoost  : %d\n", FEConfig->FEGlobal.enableBufferBoost  );
		printf( "FEGlobal.enableCP8          : %d\n", FEConfig->FEGlobal.enableCP8          );
		printf( "FEGlobal.monIVDD2           : %d\n", FEConfig->FEGlobal.monIVDD2           );
		printf( "FEGlobal.monID              : %d\n", FEConfig->FEGlobal.monID              );
		printf( "FEGlobal.enableCP7          : %d\n", FEConfig->FEGlobal.enableCP7          );
		printf( "FEGlobal.monIP2             : %d\n", FEConfig->FEGlobal.monIP2             );
		printf( "FEGlobal.monIP              : %d\n", FEConfig->FEGlobal.monIP              );
		printf( "FEGlobal.enableCP6          : %d\n", FEConfig->FEGlobal.enableCP6          );
		printf( "FEGlobal.monITRIMTH         : %d\n", FEConfig->FEGlobal.monITRIMTH         );
		printf( "FEGlobal.monIF              : %d\n", FEConfig->FEGlobal.monIF              );
		printf( "FEGlobal.enableCP5          : %d\n", FEConfig->FEGlobal.enableCP5          );
		printf( "FEGlobal.monITRIMIF         : %d\n", FEConfig->FEGlobal.monITRIMIF         );
		printf( "FEGlobal.monVCAL            : %d\n", FEConfig->FEGlobal.monVCAL            );
		printf( "FEGlobal.enableCP4          : %d\n", FEConfig->FEGlobal.enableCP4          );
		printf( "FEGlobal.enableCinjHigh     : %d\n", FEConfig->FEGlobal.enableCinjHigh     );
		printf( "FEGlobal.enableExternal     : %d\n", FEConfig->FEGlobal.enableExternal     );
		printf( "FEGlobal.enableTestAnalogRef: %d\n", FEConfig->FEGlobal.enableTestAnalogRef);
		printf( "FEGlobal.enableDigital      : %d\n", FEConfig->FEGlobal.enableDigital      );
		printf( "FEGlobal.enableCP3          : %d\n", FEConfig->FEGlobal.enableCP3          );
		printf( "FEGlobal.monITH1            : %d\n", FEConfig->FEGlobal.monITH1            );
		printf( "FEGlobal.monITH2            : %d\n", FEConfig->FEGlobal.monITH2            );
		printf( "FEGlobal.enableCP2          : %d\n", FEConfig->FEGlobal.enableCP2          );
		printf( "FEGlobal.monIL              : %d\n", FEConfig->FEGlobal.monIL              );
		printf( "FEGlobal.monIL2             : %d\n", FEConfig->FEGlobal.monIL2             );
		printf( "FEGlobal.enableCP1          : %d\n", FEConfig->FEGlobal.enableCP1          );
		printf( "FEGlobal.enableCP0          : %d\n", FEConfig->FEGlobal.enableCP0          );
		printf( "FEGlobal.monSpare           : %d\n", FEConfig->FEGlobal.monSpare           );
		
		printf( "\n[%d]FEMasks.maskEnable:\n", chipIndex);
		for(i=0; i<N_PIXEL_COLUMNS; ++i){
			printf( "%2d: ", i);
			for(j=0; j<5; ++j){
				printf( "%8x ", FEConfig->FEMasks.maskEnable[j][i]);		
			}
			printf( "\n");  
		}
		
		printf( "\n[%d]FEMasks.maskSelect:\n", chipIndex);
		for(i=0; i<N_PIXEL_COLUMNS; ++i){
			printf( "%2d: ", i);
			for(j=0; j<5; ++j){
				printf( "%8x ", FEConfig->FEMasks.maskSelect[j][i]);		
			}
			printf( "\n");  
		}
		
		printf( "\n[%d]FEMasks.maskPreamp:\n", chipIndex); 
		for(i=0; i<N_PIXEL_COLUMNS; ++i){
			printf( "%2d: ", i);
			for(j=0; j<5; ++j){
				printf( "%8x ", FEConfig->FEMasks.maskPreamp[j][i]);		
			}
			printf( "\n");  
		}
		
		printf( "\n[%d]FEMasks.maskHitbus:\n", chipIndex); 
		for(i=0; i<N_PIXEL_COLUMNS; ++i){
			printf( "%2d: ", i);
			for(j=0; j<5; ++j){
				printf( "%8x ", FEConfig->FEMasks.maskHitbus[j][i]);		
			}
			printf( "\n");  
		}	

		printf( "\n[%d]FEMasks.dacThresholdTrim:\n", chipIndex); 
		for(i=0; i<N_PIXEL_ROWS; ++i){
			printf( "%3d: ", i);
			for(j=0; j<N_PIXEL_COLUMNS; ++j){
				printf( "%2d ", FEConfig->FETrims.dacThresholdTrim[i][j]);		
			}
			printf( "\n");  
		}	
		
		printf( "\n[%d]FEMasks.dacFeedbackTrim:\n", chipIndex); 
		for(i=0; i<N_PIXEL_ROWS; ++i){
			printf( "%3d: ", i);
			for(j=0; j<N_PIXEL_COLUMNS; ++j){
				printf( "%2d ", FEConfig->FETrims.dacFeedbackTrim[i][j]);		
			}
			printf( "\n");  
		}	
		
		printf( "\n[%d]:\n", chipIndex);
		printf( "FECalib.cinjLo;        : %f\n", 
			FEConfig->FECalib.cinjLo       );
		printf( "FECalib.cinjHi;        : %f\n", 
			FEConfig->FECalib.cinjHi         ); 
		printf( "FECalib.vcalCoeff 0;  : %f\n", 
			FEConfig->FECalib.vcalCoeff[0]   ); 
		printf( "FECalib.vcalCoeff 1;  : %f\n", 
			FEConfig->FECalib.vcalCoeff[1]   ); 
		printf( "FECalib.vcalCoeff 2;  : %f\n", 
			FEConfig->FECalib.vcalCoeff[2]   ); 
		printf( "FECalib.vcalCoeff 3;  : %f\n", 
			FEConfig->FECalib.vcalCoeff[3]   ); 
		printf( "FECalib.chargeCoeffClo;: %f\n", 
			FEConfig->FECalib.chargeCoeffClo ); 
		printf( "FECalib.chargeCoeffChi;: %f\n", 
			FEConfig->FECalib.chargeCoeffChi ); 
		printf( "FECalib.chargeOffsetClo: %f\n", 
			FEConfig->FECalib.chargeOffsetClo); 
		printf( "FECalib.chargeOffsetChi: %f\n", 
			FEConfig->FECalib.chargeOffsetChi); 
		printf( "FECalib.monleakCoeff;  : %f\n", 
			FEConfig->FECalib.monleakCoeff   ); 
	}	

  //-------------------------------

  return 0;
}
/*******************************************************************************/


char* textforprim(int prim_id){
  static char description[100];

  switch(prim_id) {

/*
 cat primXface.h|awk '/#define PRIM_/ {sub(/PRIM_/,"");sub(/_STRING/,""); 
 printf("case %s:\n  sprintf(description,\"%s\");\n  break;\n", $2, $2)}' 
*/

//   case ECHO:
//     sprintf(description,"ECHO");
//     break;
//   case RW_MEMORY:
//     sprintf(description,"RW_MEMORY");
//     break;
//   case SET_LED:
//     sprintf(description,"SET_LED");
//     break;
//   case RW_REG_FIELD:
//     sprintf(description,"RW_REG_FIELD");
//     break;
//   case MODULE_MASK:
//     sprintf(description,"MODULE_MASK");
//     break;
//   case SLAVE_CTRL:
//     sprintf(description,"SLAVE_CONTROL");
//     break;
//   case TEST_GLOBAL_REGISTER:
//     sprintf(description,"TEST_GLOBAL_REGISTER");
//     break;
//   case LOAD_MODULE_CONFIG:
//     sprintf(description,"LOAD_MODULE_CONFIG");
//     break;
//   case READ_MODULE_CONFIG:
//     sprintf(description,"READ_MODULE_CONFIG");
//     break;
//   case SEND_MODULE_CONFIG:
//     sprintf(description,"SEND_MODULE_CONFIG");
//     break;
//   case RW_SLAVE_MEMORY:
//     sprintf(description,"RW_SLAVE_MEMORY");
//     break;
//   case SEND_SERIAL_STREAM:
//     sprintf(description,"SEND_SERIAL_STREAM");
//     break;
//   case RW_FIFO:
//     sprintf(description,"RW_FIFO");
//     break;
//   case SEND_SLAVE_LIST:
//     sprintf(description,"SEND_SLAVE_LIST");
//     break;
//   case WRITE_FLASH:
//     sprintf(description,"WRITE_FLASH");
//     break;
//   case QUICKXFER:
//     sprintf(description,"QUICKXFER");
//     break;
// //   case EVENT_TRAP_SETUP:
// //     sprintf(description,"EVENT_TRAP_SETUP");
// //     break;
//   case START_TASK:
//     sprintf(description,"START_TASK");
//     break;
// //   case STOP_TASK:
// //     sprintf(description,"STOP_TASK");
// //     break;
//    case TALK_TASK:
//      sprintf(description,"TALK_TASK");
//      break;
//   case SEND_TRIGGER:
//     sprintf(description,"SEND_TRIGGER");
//     break;
//   case SETUP_GROUP:
//     sprintf(description,"SETUP_GROUP");
//     break;
//   case TEST_PIXEL_REGISTER:
//     sprintf(description,"TEST_PIXEL_REGISTER");
//     break;
//   case LOAD_DATA_FRAME:
//     sprintf(description,"LOAD_DATA_FRAME");
//     break;
//   case EXPERT:
//     sprintf(description,"EXPERT");
//     break;
//   case FIND_LINK:
//     sprintf(description,"FIND_LINK");
//     break;
//   case MCC_EVENT_BUILD:
//     sprintf(description,"MCC_EVENT_BUILD");
//     break;
//   case SET_ROD_MODE:
//     sprintf(description,"SET_ROD_MODE");
//     break;


  case ECHO:
    sprintf(description,"ECHO");
    break;
  case RW_MEMORY:
    sprintf(description,"RW_MEMORY");
    break;
  case SET_LED:
    sprintf(description,"SET_LED");
    break;
  case RW_REG_FIELD:
    sprintf(description,"RW_REG_FIELD");
    break;
  case MODULE_MASK:
    sprintf(description,"MODULE_MASK");
    break;
  case SLAVE_CTRL:
    sprintf(description,"SLAVE_CONTROL");
    break;
  case TEST_GLOBAL_REGISTER:
    sprintf(description,"TEST_GLOBAL_REGISTER");
    break;
  case LOAD_MODULE_CONFIG:
    sprintf(description,"LOAD_MODULE_CONFIG");
    break;
  case READ_MODULE_CONFIG:
    sprintf(description,"READ_MODULE_CONFIG");
    break;
  case SEND_MODULE_CONFIG:
    sprintf(description,"SEND_MODULE_CONFIG");
    break;
  case RW_SLAVE_MEMORY:
    sprintf(description,"RW_SLAVE_MEMORY");
    break;
  case SEND_SERIAL_STREAM:
    sprintf(description,"SEND_SERIAL_STREAM");
    break;
  case RW_FIFO:
    sprintf(description,"RW_FIFO");
    break;
  case SEND_SLAVE_PRIMITIVE:
    sprintf(description,"SEND_SLAVE_PRIMITIVE");
    break;
  case WRITE_FLASH:
    sprintf(description,"WRITE_FLASH");
    break;
  case START_TASK:
    sprintf(description,"START_TASK");
    break;
  case TALK_TASK:
    sprintf(description,"TALK_TASK");
    break;
  case SEND_TRIGGER:
    sprintf(description,"SEND_TRIGGER");
    break;
  case SETUP_GROUP:
    sprintf(description,"SETUP_GROUP");
    break;
  case TEST_PIXEL_REGISTER:
    sprintf(description,"TEST_PIXEL_REGISTER");
    break;
  case LOAD_DATA_FRAME:
    sprintf(description,"LOAD_DATA_FRAME");
    break;
  case EXPERT:
    sprintf(description,"EXPERT");
    break;
  case FIND_LINK:
    sprintf(description,"FIND_LINK");
    break;
  case MCC_EVENT_BUILD:
    sprintf(description,"MCC_EVENT_BUILD");
    break;
  case SET_ROD_MODE:
    sprintf(description,"SET_ROD_MODE");
    break;
  case RW_MCC:
    sprintf(description,"RW_MCC");
    break;
  case CONFIGURE_ENVIRONMENT:
    sprintf(description,"CONFIGURE_ENVIRONMENT");
    break;
  case SEND_SLAVE_LIST:
    sprintf(description,"SEND_SLAVE_LIST");
    break;

  default:  
    sprintf(description,"THE_UNKNOWN");
    break;

  }
  return description;
}

int decode_moduledata(PixelModuleConfig *configData){
  printf("found Module: %s with ID %d in Group%d\n",
	 configData->idStr,configData->moduleIdx,configData->groupId);
  writeConfigModule(configData);
  return 0;
}


int decode_HISTOGRAM_CTRL_TASK(StartTaskIn taskData){

  return 0;
}

int decode_scanoptions(FILE *inputhandle){
  ScanOptions scanoptions;
  int status;
  int point;
  status = (fread(&scanoptions, 1, sizeof(ScanOptions), inputhandle));

  //groupOpt;
         
  printf(" scanoptions.trigOpt.triggerMode %d \n",	    scanoptions.trigOpt.triggerMode);	    
  printf(" scanoptions.trigOpt.nEvents %d \n",		    scanoptions.trigOpt.nEvents);		    
  printf(" scanoptions.trigOpt.nL1AperEvent %d \n",	    scanoptions.trigOpt.nL1AperEvent);	    
  printf(" scanoptions.trigOpt.Lvl1_Latency %d \n",	    scanoptions.trigOpt.Lvl1_Latency);	    
  printf(" scanoptions.trigOpt.strobeDuration %d \n",	    scanoptions.trigOpt.strobeDuration);	    
  printf(" scanoptions.trigOpt.strobeMCCDelay %d \n",       scanoptions.trigOpt.strobeMCCDelay);    	    
  printf(" scanoptions.trigOpt.strobeMCCDelayRange %d \n",  scanoptions.trigOpt.strobeMCCDelayRange);    
  printf(" scanoptions.trigOpt.CalL1ADelay %d \n",	    scanoptions.trigOpt.CalL1ADelay);	    
  printf(" scanoptions.trigOpt.eventInterval %d \n",	    scanoptions.trigOpt.eventInterval);	    
  printf(" scanoptions.trigOpt.superGroupDelay %d \n",	    scanoptions.trigOpt.superGroupDelay);	    
  printf(" scanoptions.trigOpt.superGroupAnL1A[2] %d \n",   scanoptions.trigOpt.superGroupAnL1A[2]);	    
  printf(" scanoptions.trigOpt.superGroupBnL1A[2] %d \n",   scanoptions.trigOpt.superGroupBnL1A[2]);	    
  printf(" scanoptions.trigOpt.trigABDelay[2] %d \n",	    scanoptions.trigOpt.trigABDelay[2]);	    
  printf(" scanoptions.trigOpt.vcal_charge %d \n",	    scanoptions.trigOpt.vcal_charge);	    
  printf(" scanoptions.trigOpt.optionsMask %x \n",	    scanoptions.trigOpt.optionsMask);	    
//scanoptions.trigOpt.customTriggerSequence[10];
  
  printf(" scanoptions.groupOpt.superGroupAMask %x\n", scanoptions.groupOpt.superGroupAMask);  
  printf(" scanoptions.groupOpt.superGroupBMask %x\n", scanoptions.groupOpt.superGroupBMask);  
                                                        
  for(int i=0;i<MAXGROUPS;i++){
    if(scanoptions.groupOpt.groupActionMask[i]!=0)
      printf("Including group %d action mask %x\n",i,scanoptions.groupOpt.groupActionMask[i]);
  }
  
  printf("Have %d loops %d nmaskstages using %d mask\n", 
	scanoptions.nLoops,scanoptions.nMaskStages, scanoptions.maskStages); 
 //scanoptions.histOpt[SCAN_MAXHIST];

  for(int k=0;k<scanoptions.nLoops;k++){
    //scanoptions.scanLoop[i];
    char  *parameter, *action;
    switch(scanoptions.scanLoop[k].scanParameter) {
    case SCAN_NONE: 
      parameter = "SCAN_NONE";
      break; 
    case SCAN_VCAL: 
      parameter = "SCAN_VCAL";
      break; 
    case SCAN_TDAC: 
      parameter = "SCAN_TDAC";
      break; 
    case SCAN_FDAC: 
      parameter = "SCAN_FDAC";
      break; 
    case SCAN_GDAC: 
      parameter = "SCAN_GDAC";
      break; 
    case SCAN_MONLEAK_DAC: 
      parameter = "SCAN_MONLEAK_DAC";
      break; 
    case SCAN_STROBE_DELAY: 
      parameter = "SCAN_STROBE_DELAY";
      break; 
    case SCAN_TRIGGER_DELAY: 
      parameter = "SCAN_TRIGGER_DELAY";
      break; 
    case SCAN_L1ALATENCY: 
      parameter = "SCAN_L1ALATENCY";
      break; 
    case SCAN_BOC_RX_DELAY: 
      parameter = "SCAN_BOC_RX_DELAY";
      break; 
    case SCAN_BOC_RX_THRESH: 
      parameter = "SCAN_BOC_RX_THRESH";
      break; 
    case SCAN_BOC_TX_MSR: 
      parameter = "SCAN_BOC_TX_MSR";
      break; 
    case SCAN_BOC_V_CLOCK:
      parameter = "SCAN_BOC_V_CLOCK";
      break;
      
    default:
      parameter = "THE UNKNOWN";
      break;
    }
    
    switch(scanoptions.scanLoop[k].endofLoopAction.Action){
      case SCAN_NO_ACTION:
	action = "SCAN_NO_ACTION";
	break;
      case SCAN_FIT:
	action = "SCAN_FIT";
	break;
      case SCAN_TUNE_THRESH:
	action = "SCAN_TUNE_THRESH";
	break;
      case SCAN_CALC_THRESH:
	action = "SCAN_CALC_THRESH";
	break;

    default:
      action = "THE UNKNOWN";
      break;
    } 
    //    dataPointsPtr;
    printf("scanning %s followed by %s\n",parameter,action);
    
    for(int i=0;i<scanoptions.scanLoop[0].nPoints;i++){
      status = (fread(&point, 1, sizeof(uint32_t), inputhandle));
      printf("Loop %d point %d value %d\n", k, i, point);
    }
  }
  return 0;
}

int decode_task(FILE *inputhandle){
  int status;
  char description[255];
  StartTaskIn taskData;
  int (* decode_options)(FILE *inputhandle) = NULL;

  status = (fread(&taskData, 1, sizeof(StartTaskIn), inputhandle));

  switch(taskData.id) {

	case DIAGNOSTIC_TASK:
 	  sprintf(description,"DIAGNOSTIC_TASK");
	  break;            
	case SCAN_TASK:
 	  sprintf(description,"SCAN_TASK");
	  break;            
	case SCAN_TASK_2:
 	  sprintf(description,"SCAN_TASK_2");
	  //	  decode_scanoptions(inputhandle);
	  decode_options = decode_scanoptions;
	  break;            
// 	case TALK_TASK:
//  	  sprintf(description,"TALK_TASK");
// 	  break;            
	case HISTOGRAM_TASK:
 	  sprintf(description,"HISTOGRAM_TASK");
	  break;            
	default:
 	  sprintf(description,"THE_UNKNOWN");
	  break;
        }
  printf("Starting task %s minor id %d\n",
	 description, taskData.idMinor);	    
  if(decode_options!=NULL)decode_options(inputhandle);

  return 0;
}


void decode_binary(uint32_t word){
  int bit,i;
  for(i = sizeof(word)*8;i>0;i--){ 
    bit = (word >> i) & 1; 
    printf("%1d",bit);
  }
}

int decode_list(FILE *inputhandle){
   	int status;
	unsigned long word;
	int i,j;
	MSG_LIST_HEAD  priml_head;
	MSG_LIST_TAIL  priml_tail;
	MSG_HEAD      prim_head;
        std::string scommand("");
 
   status = 0;
/* read prim listheader */
  status = fread(&priml_head, 1, sizeof(priml_head), inputhandle);
  if(feof(inputhandle)) return 0; 
/*
  priml_head->length           
  priml_head->index            
  priml_head->numMsgs          
  priml_head->primListRevision 
*/
  printf("Primlist with %d messages\n",
	 //	 priml_head.numMsgs,priml_head.primListRevision);	    
	 priml_head.nMsgs);	    

   for(i=0;i<priml_head.nMsgs;i++){	    

    status = fread(&prim_head, 1, sizeof(prim_head), inputhandle);
    
    printf("Primitive %s with %d data words , rev %d\n",
	   textforprim(prim_head.id),(prim_head.length-sizeof(prim_head)/4),
	   prim_head.revision);	    

    switch(prim_head.id) {
    case SEND_SLAVE_LIST:
      SendSlaveListIn SendSlaveList_In;
      status = (fread(&SendSlaveList_In, 1, sizeof(SendSlaveListIn), inputhandle));
      printf("Sending %d prims to slave %d on queue %d\n",
	     SendSlaveList_In.nPrimitives,
	     SendSlaveList_In.slave,
	     SendSlaveList_In.queueIndex);
      printf("\nBegin of SlavePrimitiveList\n");
      decode_list(inputhandle);
      printf("\nEnd of SlavePrimitiveList\n");
      break;  

    case LOAD_MODULE_CONFIG:
      LoadModuleConfigIn loadModuleCfg;
      status = (fread(&loadModuleCfg, 1, sizeof(LoadModuleConfigIn), inputhandle));
      printf("loading module Config %d for module %d\n",
	     loadModuleCfg.cfgId,loadModuleCfg.module);
      printf("using size %d expected: %d\n",
	     loadModuleCfg.cfgSize, sizeof(PixelModuleConfig)/4);
      PixelModuleConfig configData;
      printf("\nBegin of ModuleData\n");
      status = (fread(&configData, 1, sizeof(PixelModuleConfig), inputhandle));
      decode_moduledata(&configData);
      printf("\nEnd of ModuleData\n");
      break;  
    case MODULE_MASK:
      ModuleMaskIn modMask;
      ModuleMaskInAux moduleMaskAux;
      status = (fread(&modMask, 1, sizeof(ModuleMaskIn), inputhandle));
      printf("Module mask for %d Modules\n", modMask.nModules);
      printf("\nBegin of ModuleMask\n");
      if(modMask.dataPtr==0){
	for(i=0;i<modMask.nModules;i++){
	  status = (fread(&moduleMaskAux, 1, sizeof(ModuleMaskInAux), inputhandle));
	  printf(" Module %d connected on Formatter/Link %x:%x cmdLine %d detLine %d\n",
		 moduleMaskAux.moduleId,
		 moduleMaskAux.moduleMask.formatterId,
		 moduleMaskAux.moduleMask.formatterLink,
		 moduleMaskAux.moduleMask.cmdLine,
		 moduleMaskAux.moduleMask.detLine);
	}
      }
      printf("\nEnd of ModuleMask\n");
      break;  
    case START_TASK:
      //      StartTaskIn taskData;
      printf("\nBegin of TaskData\n");
      //      decode_task(taskData);
      decode_task(inputhandle);
      printf("\n");
      printf("\nEnd of TaskData\n");
      break;

    case SLAVE_CTRL:
      SlaveCtrlIn slaveCtrl;
      status = (fread(&slaveCtrl, 1, sizeof(SlaveCtrlIn), inputhandle));
      scommand = ""; 
      if(slaveCtrl.flag & DSP_RESET ) scommand += "DSP_RESET ";
      if(slaveCtrl.flag & SLAVE_INIT) scommand += "SLAVE_INIT ";
      if(slaveCtrl.flag & LOAD_COFF ) scommand += "LOAD_COFF ";
      if(slaveCtrl.flag & LOAD_DONE ) scommand += "LOAD_DONE ";
      if(slaveCtrl.flag & WAIT_BOOT ) scommand += "WAIT_BOOT ";

      printf("sending %s and %d sections to slaves %x\n",
	     scommand.c_str(), slaveCtrl.nSections, slaveCtrl.slaveMask);
      for(j=0;j<(prim_head.length-((sizeof(prim_head)+sizeof(SlaveCtrlIn))/4));j++){
	status = (fread(&word, 1, 4, inputhandle));
	printf(" %08x ",word);
	if(((j+1)%8)==0)printf("\n");
      }
      break;

    default:
      for(j=0;j<(prim_head.length-sizeof(prim_head)/4);j++){
	status = (fread(&word, 1, 4, inputhandle));
	printf(" %08x ",word);
	if(((j+1)%8)==0)printf("\n");
      }
      break;
    }
     
    printf("\nEnd of Primitive\n");
  }
	    
  status = fread(&priml_tail, 1, sizeof(priml_tail), inputhandle);
  if(priml_tail.length != priml_head.length)printf("Size mismatch: %d vs %d\n",priml_tail.length,priml_head.length);

  return status;
}

int main(int argc, char **argv){
  int status;
  FILE *inputhandle;
  uint32_t debuginfo[5];

  if(argc < 2){

    printf("usage: decode_prim primbinfile [skip]");

  }else{
   inputhandle  = fopen(argv[1], "r");
   if(argc > 2) {
     printf("skipping 5 bytes of debug info\n");
     status = fread(debuginfo, 5, sizeof(uint32_t), inputhandle);
     printf("address primListCtrl 0x%x\n", debuginfo[0]);
     printf("sport0Done           0x%x\n", debuginfo[1]);
     printf("sport1Done           0x%x\n", debuginfo[2]);
     printf("address primBuff     0x%x\n", debuginfo[4]);
     printf("nWords  %d\n", debuginfo[3]);

   }
  }

  do{
    status = decode_list(inputhandle);
    printf("\nEnd of PrimitiveList\n");
  } while(!feof(inputhandle));

  fclose(inputhandle);

  return 0;
	 
}   
