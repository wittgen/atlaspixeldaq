// Test program for the BOC.

#include <iostream>
using namespace std;

#include <ctype.h>

#include "../RodCrate/BocCard.h"
#include "../../VmeInterface/RCCVmeInterface.h"
#include "parameters.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  unsigned int i;

  uint32_t localEnable;
  uint32_t remoteEnable;

  uint32_t buffer1[100];
  uint32_t buffer2[100];

  unsigned long baseAddress;
  std::string option;

  long dataLength = -1;
  int slot = -1;
  int repetitions = 1;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'l': {
          dataLength = atoi(option.substr(2).c_str());
          break;
        }
        case 'r': {
          repetitions = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):";
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }

  catch (HpiException &h) {
    hex(cout);
    cout << h.getDescriptor() << '\n';
    cout << "calcAddr: " << h.getCalcAddr() << ", readAddr: " << 
            h.getReadAddr() << '\n';
    dec(cout);
  }


// Now create BocCard

  BocCard* boc0 = rod0->getBocCard();
//
// Initialise
//
  boc0->initialize();
//
// Read the "hard-coded" information about the ROD.

  hex(cout);
  cout << "BOC, Serial number: " << boc0->getSerialNumber() << endl;
  cout << "Manufacturer: " << boc0->getManufacturer() << endl;
  cout << "Module type: " << boc0->getModuleType() << endl;
  cout << "Firmware revision: " << boc0->getFirmwareRevision() << endl;
  cout << "Hardware revision: " << boc0->getHardwareRevision() << endl;
  cout << "Status: " << boc0->getBocStatusRegister() << endl << endl;
//
  cout << "Information from status method:" << endl;
  boc0->status();
//
// Now report the interlock status
//
  cout << "Interlock status: " << boc0->getInterlockStatus(&localEnable,&remoteEnable) << endl;
  cout << "Local enable: " << localEnable << " Remote enable: " << remoteEnable << endl;
//
// Now check the important functionality. The code is a bit messy,
// but is done this way to try and test as many functions as possible.

// Check the thresholds load correctly.
// Set special values in a range of channels.
//
  for(i=0;i<BOC_RECEIVE_CHANNELS;i++) {
    buffer1[i] = 0xa5;
    buffer2[i] = 0;
  }

  boc0->setRxThreshold(0,buffer1,BOC_RECEIVE_CHANNELS);

  for(i=0;i<10;i++) {
    buffer1[i] = i<<4;
  }
  boc0->setRxThreshold(5,buffer1,10);
  boc0->setRxThreshold(15,10<<4);
  boc0->getRxThreshold(0,buffer2,BOC_RECEIVE_CHANNELS);
  cout << "Checking Thresholds - only discrepancies reported" << endl;
  for(i=0;i<BOC_RECEIVE_CHANNELS;i++) {
    if((i<5||i>15)&&(buffer2[i]!=0xa5)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << 0xa5 << endl;
    }
    if((i>4&&i<16)&&(buffer2[i]!=(i-5)<<4)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << ((i-5)<<4) << endl;
    }
  }
// Provoke an exception
  try {
    boc0->setRxThreshold(15,500);
  }
  catch (BocException &b) {
    cout << "!!BocException when setting threshold" << endl;
    cout << b.getDescriptor() << " ";
    cout << b.getData1() << " : " << b.getData2() << endl;
  }

// Now check the laser currents set OK.
//

  for(i=0;i<BOC_TRANSMIT_CHANNELS;i++) {
    buffer1[i] = 0x5a;
    buffer2[i] = 0;
  }

  boc0->setLaserCurrent(0,buffer1,BOC_TRANSMIT_CHANNELS);

  for(i=0;i<16;i++) {
    buffer1[i] = i<<3;
  }
  boc0->setLaserCurrent(8,buffer1,16);
  boc0->setLaserCurrent(24,16<<3);
  boc0->getLaserCurrent(0,buffer2,BOC_TRANSMIT_CHANNELS);
  cout << "Checking Laser Currents - only discrepancies reported" << endl;
 
  for(i=0;i<BOC_TRANSMIT_CHANNELS;i++) {
    if((i<8||i>24)&&(buffer2[i]!=0x5a)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << 0x5a << endl;
    }
    if((i>7&&i<25)&&(buffer2[i]!=(i-8)<<3)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << ((i-8)<<3) << endl;
    }
  }
//
// Provoke an exception
  try {
    boc0->setLaserCurrent(100,0);
  }
  catch (BocException &b) {
    cout << "!!BocException when setting laser current" << endl;
    cout << b.getDescriptor() << " ";
    cout << b.getData1() << " : " << b.getData2() << endl;
  }

// Check the data delay functions look right.
// Set special values in a range of channels.
//
  for(i=0;i<BOC_RECEIVE_CHANNELS;i++) {
    buffer1[i] = 0xf;
    buffer2[i] = 0;
  }

  boc0->setRxDataDelay(0,buffer1,BOC_RECEIVE_CHANNELS);

  for(i=0;i<13;i++) {
    buffer1[i] = i;
  }
  boc0->setRxDataDelay(21,buffer1,13);
  boc0->setRxDataDelay(34,13);
  boc0->getRxDataDelay(0,buffer2,BOC_RECEIVE_CHANNELS);
  cout << "Checking Data Delays - only discrepancies reported" << endl;
  for(i=0;i<BOC_RECEIVE_CHANNELS;i++) {
    if((i<21||i>34)&&(buffer2[i]!=0xf)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << 0xF << endl;
    }
    if((i>20&&i<35)&&(buffer2[i]!=i-21)) {
      cout << dec << i << " : "<< hex << buffer2[i] << " : " << (i-21) << endl;
    }
  }

// Provoke an exception
  try {
    boc0->setRxDataDelay(15,500);
  }
  catch (BocException &b) {
    cout << "!!BocException when setting data delay" << endl;
    cout << b.getDescriptor() << " ";
    cout << b.getData1() << " : " << b.getData2() << endl;
  }

  dec(cout);
//
// Now see what the monitoring channels report
//

  showpoint(cout);
  for(i=0;i<12;i++) {
    cout << "Monitor channel "<< i << " : " << boc0->getMonitorAdc(i) << endl;
  }
  noshowpoint(cout);
 
// Delete the BOC, ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


