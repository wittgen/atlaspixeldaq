//! A program to dump blocks of memory in Master DSP memory space. 
// It does NOT initialize the ROD!! This program is intended to be 
// a diagnostic used to examine memory contents after another
// program, e.g. EchoTest or LedTest, has run.

#include <iostream>
#include <iomanip>
using namespace std;

#include <ctype.h>
#include <fstream>
typedef unsigned int uint32_t;
#include "quickStatus.h"

int main(int argc, char *argv[]) {
	
	std::string option;
	
	busyRecord history[1024];
	
	std::string binFileName("History.bin");  // Name of binary file to load
	ifstream binFile;                             // Pointer to binary frile
	int fileSize;                                 // Size of binary file in bytes
	
	if (argc > 1) {
		binFileName = argv[1];
		/*    for (int i=1; i<argc; i++) {
		 option = argv[i];
		 if (option[0] != '-') break;
		 switch (option[1]) {
		 case 's': {
		 slot = atoi(option.substr(2).c_str());
		 break;
		 }
		 default: {
		 break;
		 }
		 }
		 }*/
	}
	
	binFile.open(binFileName.c_str(), ios::binary);
	if (!binFile.is_open()) {
		cout << "Unable to open binary file:" << binFileName << endl;
		return 1;
	}
	
	// Get size of file
	binFile.seekg(0, ios::end);           // go to end of file
	fileSize = binFile.tellg();          // file size given by current location
	binFile.seekg(0, ios::beg);          // go back to beginning of file
	
	binFile.read((char *)&(history[0]), fileSize);
	long numWords=sizeof(busyRecord)*256;                                // Number of words to dump
	
	std::cout << std::setfill('0') << std::dec;
	// output data
	int histOffset = 0;
	for(int k=0; k<1024; k++) {
		if (history[k].historyIndex == 0) {
			histOffset = k;
			break;
		}
	}
	
	std::cout << "Busy?\t";
	std::cout << "XOFF?\t";
	std::cout << "RCF-Busy\t";
	std::cout << "RRIFStatus1[29:22]\t";
	for(int i=0;i<4;i++) {
		std::cout << std::hex << "|\t" << "fmt[" << i << "].status" << "\t" << "fmt[" << i << "].linkEnable" << "\t" << "fmt[" << i << "].modeBitStatus[31:28]" << "\t" << "fmt[" << i << "].timeoutError" << "\t" << "fmt[" << i << "].headerTrailerError" << "\t" << "fmt[" << i << "].rodBusyError" << "\t" << std::dec;
	}
	std::cout << "|\t";
	for(int i=0;i<4;i++) {
		for(int j=0;j<4;j++)
			std::cout << "fmt[" << i << "].linkOccCount[" << j << "]" << "\t";
	}
	for(int i=4;i<8;i++) {
		std::cout << std::hex << "|\t" << "fmt[" << i << "].status" << "\t" << "fmt[" << i << "].linkEnable" << "\t" << "fmt[" << i << "].modeBitStatus[31:28]" << "\t" << "fmt[" << i << "].timeoutError" << "\t" << "fmt[" << i << "].headerTrailerError" << "\t" << "fmt[" << i << "].rodBusyError" << "\t" << std::dec;
	}
	std::cout << "|\t";
	for(int i=4;i<8;i++) {
		for(int j=0;j<4;j++)
			std::cout << "fmt[" << i << "].linkOccCount[" << j << "]" << "\t";
	}
	std::cout << std::hex << "efb.evtMemFlags[31:24]" << "\t";
	std::cout << std::hex << "efb.evtMemFlags[23:0]"<< std::dec << "\t";
	std::cout << std::hex << "efb.dmWordCount" << "\t";
	std::cout << std::hex << "efb.runtimeStat" << "\t";
	std::cout << std::hex << "rtr.cmndStat" << std::dec << std::setw(1) << "\t";
	std::cout << "historyIndex" << "\t";
	std::cout << "cycletime";
	std::cout << std::endl;


	for(int i=0; i<1024; i++) {
		int k = (histOffset + i) % 1024;
		if ((history[k].rcf_rodBusy & 0x1001) > 0) std::cout << "--BUSY--";
		else std::cout << "NOT-BUSY";
		std::cout << "\t";
		if (history[k].rtr_cmndStat & 0x2000) std::cout << "XOFF";
		else std::cout << "XON";
		std::cout << "\t";
		std::cout << std::hex << std::setw(8) << (history[k].rcf_rodBusy & 0x7fffff) << std::dec << "\t";
		std::cout << std::hex << std::setw(2) << (history[k].rcf_rrifStatus1 >> 22 & 0xff) << std::dec << "\t";
//		std::cout << std::hex << std::setw(6) << history[k].fmt[0].status << std::setw(1) << std::dec << "\t";
		for(int i=0;i<4;i++) {
			std::cout << std::hex << "|\t" << std::setw(6) << history[k].fmt[i].status << "\t" << std::setw(1) << (history[k].fmt[i].linkEnable & 0xf) << "\t" << ((history[k].fmt[i].modeBitStatus >> 28) & 0xf) << "\t" << (history[k].fmt[i].timeoutError & 0xf) << "\t" << (history[k].fmt[i].headerTrailerError & 0xf) << "\t" << (history[k].fmt[i].rodBusyError /*headerTrailerError /*linkEnable*/ & 0xf) << "\t" << std::dec;
		}
		std::cout << "|\t";
		for(int i=0;i<4;i++) {
			for(int j=0;j<4;j++)
				std::cout << std::setw(4) << std::setfill(' ') << history[k].fmt[i].linkOccCount[j] << "\t";
		}
		std::cout << std::setfill('0') << std::dec;
//		std::cout << std::hex << std::setw(6) << history[k].fmt[4].status << std::setw(1) << std::dec << "\t";
		for(int i=4;i<8;i++) {
			std::cout << std::hex << "|\t" << std::setw(6) << history[k].fmt[i].status << "\t" << std::setw(1) << (history[k].fmt[i].linkEnable & 0xf) << "\t" << ((history[k].fmt[i].modeBitStatus >> 28) & 0xf) << "\t" << (history[k].fmt[i].timeoutError & 0xf) << "\t" << (history[k].fmt[i].headerTrailerError & 0xf) << "\t" << (history[k].fmt[i].rodBusyError /*headerTrailerError /*linkEnable*/ & 0xf) << "\t" << std::dec;
		}
		std::cout << "|\t";
		for(int i=4;i<8;i++) {
			for(int j=0;j<4;j++)
				std::cout << std::setw(4) << std::setfill(' ') << history[k].fmt[i].linkOccCount[j] << "\t";
		}
		std::cout << std::setfill('0') << std::dec;
		std::cout << std::hex << std::setw(2) << ((history[k].efb_evtMemFlags >> 24) & 0xff) << "\t";
		std::cout << std::hex << (history[k].efb_evtMemFlags & 0xFFFFFF) << std::dec << "\t";
		std::cout << std::hex << std::setw(8) << history[k].efb_dmWordCount << std::dec << std::setw(1) << "\t";
		std::cout << std::hex << std::setw(8) << history[k].efb_runtimeStat << std::dec << std::setw(1) << "\t";
		std::cout << std::hex << std::setw(8) << history[k].rtr_cmndStat << std::dec << std::setw(1) << "\t";
		std::cout << history[k].historyIndex;
		if (k>0) std::cout << "\t" << history[k].historyIndex - history[k-1].historyIndex;
		std::cout << std::endl;
	}
	std::cout << std::dec;
	
	return 0;  
}


