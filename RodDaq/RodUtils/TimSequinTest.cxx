//File: TimSequinTest.cxx

/*! \file
 * \brief Test TimSequin class
 *
 * This is a test program for TIM Sequencer information stuff.
 * In particular it tests the scan code.
 *
 * Contributors: John Lane <jbl@hep.ucl.ac.uk> - originator
 *
 *  $Id$
 * $Log$
 */

#include <iostream>
#include <iomanip>

#include "TimDefine.h"
#include "TimSequin.h"

using namespace std;
using namespace SctPixelRod;

const int SIZE = 106;

TimSequin *seq = new TimSequin();

void testscan();
void scanner( const int size, TimScanControl control );

int main() {

  if (seq == 0) cout << "No object " << seq << endl;
  testscan();
  delete seq;
  return 0;
}

void testscan() {

  TimScanControl control = seq->scanDefaults();

  const int offset = TIM_BCID_OFFSET;
  const int Delays1[2] = { 3, 5 };
  const int Delays0[2] = { 0, 0 }; // start bit at L1A or stop bit index!
  uint32_t ids1[3] = { 1, 0, 0x3FF };
  uint32_t ids2[3] = { 2, 0, 0x3FF };
  int iBCR1, iCAL1, iL1A1, iBCR2, iL1A2;

  cout << "scan test for no errors" << endl;

  iBCR1 = -1;
  iCAL1 =  0;
  iL1A1 = iCAL1 + TIM_CAL_DEADTIME;
  iBCR2 = iL1A1 + TIM_L1A_DEADTIME;
  iL1A2 = iBCR2 + TIM_BCR_DEADTIME;
  ids1[1] = seq->getBCID( iL1A1, iBCR1, offset );
  ids2[1] = seq->getBCID( iL1A2, iBCR2, offset );

  seq->reset();
  seq->addByIndex( TIM_CAL, iCAL1 );
  seq->addByIndex( TIM_BCR, iBCR2 );
  seq->addTrigger( iL1A1, ids1, Delays1 );
  seq->addTrigger( iL1A2, ids2, Delays1 );

  control.offset   = offset;
  control.pipeline = TIM_CAL_DEADTIME;
  control.orbit    = iBCR2 - iBCR1;

  scanner( SIZE, control );

  cout << "scan test for 16 errors" << endl;

  iL1A1 = iL1A1 - 1;
  iBCR2 = iBCR2 - 2;
  iL1A2 = iL1A2 - 3;

  ids1[0] = 0; // l1id1

  seq->reset();
  seq->addByIndex( TIM_CAL, iCAL1 );
  seq->addByIndex( TIM_BCR, iBCR2 );
  seq->addByIndex( TIM_FER, iBCR2 );
  seq->addTrigger( iL1A1, ids1, Delays0 );
  seq->addTrigger( iL1A2, ids2, Delays0 );

  control.nextTTID = 0;

  scanner( iL1A1 + 2 * 37 + Delays0[0], control );
  // set size = 2nd stop bit index to give overflow error
  //     (add 1 for actual size to give no overflow error)
}

// ------------------------- scanner -----------------------------------------

void scanner( const int size, TimScanControl control ) {

  uint16_t buffer[SIZE];
  const int m = 20;
  const int n = (size < 200) ? size : 200;

  control.size  = seq->getBuffer( size, buffer );
  control.byte  = 0;
  control.print = 1;

  for (int i = 0; i < n; i++) {
      cout << hex << setw(2) << buffer[i]
           << ( (i%m==m-1 || i==n-1) ? '\n' : ' ')
           << dec;
  }
  TimScanResults results = seq->scan( control );

  cout << "scan errors = " << results.errors << endl;
}
