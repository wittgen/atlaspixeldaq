// Program to scan all BOC Serial Numbers, Firmware revisions and stuff in one crate

#include <iostream>
#include <unistd.h>
#include <fstream>


using namespace std;

#include "RCCVmeInterface.h"

//include "RodVmeAddresses.h"

using namespace SctPixelRod;

// Enter various defines here to simplify and make the code a bit more readable...
// All given as an Offset from the PRM Address Space:
#define FPGA_CONFIGURATION_CONTROL_REG     0x0000
#define FPGA_RESET_CONTROL_REG             0x0004
#define DSP_RESET_COTNROL_REG              0x0008
#define FLASH_CONTROL_REG                  0x000C
#define FLASH_ADDRESS_AND_WRITE_REG        0x0010
#define FLASH_STATUS_REGISTER              0x0030
#define FLASH_MEMORY_READ_DATA             0x003C

// The following are Specific for RevE and RevF RODs
// Addresses for different FPGA configurations in flash memory
#define RCF_BEGIN 0xE00000
#define RCF_END   0xE78E63
#define FMT_BEGIN 0xE80000
#define FMT_END   0xEF8E6B
#define EFB_BEGIN 0xF00000
#define EFB_END   0xF52327
#define RTR_BEGIN 0xF80000
#define RTR_END   0xFD2327
int main(int argc, char *argv[]) {

  unsigned long baseAddress;

  std::string option;
  int slot = 14;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme = new RCCVmeInterface();

  VmePort* VmeRegPort = NULL;
  try {
    VmeRegPort = new VmePort(baseAddress+0xc00000, 0x1fff, VmeInterface::A32, *vme); 
  } catch (...) {
    std::cout << "Some Exception as always..." << std::endl;
  }

  unsigned long *buffer;

  try {
    buffer = new unsigned long[0x800];
  } catch (...) {
    std::cout << "Error reserving memory for data readout" << std::endl;
    return 1;
  }

  std::cout << "PRM Registers:" << std::endl;
  for (int i=0;i<0x800;i++) {
    std::cout << "Address: 0x" << hex << (i*4) << "\tData: 0x" << (buffer[i] = vme->readS32(VmeRegPort->getHandle(), i*4)) << dec << std::endl;
  }

  string fname("prmstate.bin");
  cout << "Saving ROD PRM Status to " << fname << endl;

  ofstream outFile(fname.c_str(), ios::binary);
    
  // Save file 
  outFile.write((char *)buffer, 0x2000);
  
  delete VmeRegPort;
  delete vme;

  return 0;
}
