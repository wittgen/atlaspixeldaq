#include "RCCVmeInterface.h"
#include "RodModule.h"
#include "BocCard.h"
//#include "registerIndices.h"
#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>
#include <time.h>
#include <math.h>

#include <unistd.h>

#define COUNTER_BASE            0x00400090
#define FMT_DIST                0x00000400
#define FMT_CNT_CONTROL(fmt)    COUNTER_BASE + (fmt * FMT_DIST)
#define FMT_GLB_COUNTER(fmt)    COUNTER_BASE + (fmt * FMT_DIST) + 0x00000004
#define FMT_STREAM_01(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000008
#define FMT_STREAM_23(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000000C
// More definitions for SCT convenience
#define FMT_STREAM_45(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000010
#define FMT_STREAM_67(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000014
#define FMT_STREAM_89(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000018
#define FMT_STREAM_AB(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000001C

// Definition to bypass use of registerindices.h
#define FMT_LINK_EN(fmt)              (fmt)
#define FMT_EXP_MODE_EN(fmt)          (1 +(FMT_LINK_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_CONFIG_MODE_EN(fmt)       (1 +(FMT_EXP_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_EDGE_MODE_EN(fmt)         (1 +(FMT_CONFIG_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_READOUT_TIMEOUT(fmt)      (1 +(FMT_EDGE_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_DATA_OVERFLOW_LIMIT(fmt)  (1 +(FMT_READOUT_TIMEOUT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_LIMIT(fmt) (1 +(FMT_DATA_OVERFLOW_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_LIMIT(fmt)       (1 +(FMT_HEADER_TRAILER_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_LINK_L1A_CNT(fmt)     (1 +(FMT_ROD_BUSY_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_BANDWIDTH(fmt)        (1 +(FMT_PXL_LINK_L1A_CNT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_DATA_TEST_MUX(fmt)   (33 +(FMT_PXL_BANDWIDTH(FORMATTERS_PER_ROD-1)) +fmt)
/* +4*8 reserved IDs */
#define FMT_MB_DIAG_REN(fmt)          (1 +(FMT_LINK_DATA_TEST_MUX(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_OCC_CNT(fmt,lnk)     (1 +(FMT_MB_DIAG_REN(FORMATTERS_PER_ROD-1)) \
                                         +(fmt*LINKS_PER_FORMATTER) +lnk)

#define FMT_TIMEOUT_ERR(fmt)          (1 +(FMT_LINK_OCC_CNT((FORMATTERS_PER_ROD-1), \
                                                            (LINKS_PER_FORMATTER-1))) \
                                        +fmt)
#define FMT_DATA_OVERFLOW_ERR(fmt)    (1 +(FMT_TIMEOUT_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_ERR(fmt)   (1 +(FMT_DATA_OVERFLOW_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_ERR(fmt)         (1 +(FMT_HEADER_TRAILER_ERR(FORMATTERS_PER_ROD-1)) +fmt)

#define FMT_DATA_FMT_STATUS(fmt)      (1 +(FMT_ROD_BUSY_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_STATUS(fmt)               (1 +(FMT_DATA_FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_VERSION(fmt)              (1 +(FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_05(fmt)      (1 +(FMT_VERSION(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_6B(fmt)      (1 +(FMT_MODEBIT_STAT_05(FORMATTERS_PER_ROD-1)) +fmt)

/* Event Fragment Builder FPGA registers */
#define EFB_ERROR_MASK(efb, lnk) (25 + FMT_MODEBIT_STAT_6B(FORMATTERS_PER_ROD-1) + \
                                  efb*DATA_LINKS_PER_EFB + lnk)
#define FORMAT_VRSN_LSB      (1 + EFB_ERROR_MASK((EFBS_PER_ROD-1),(DATA_LINKS_PER_EFB-1)))
#define FORMAT_VRSN_MSB      (1 + FORMAT_VRSN_LSB)
#define SOURCE_ID_LSB        (1 + FORMAT_VRSN_MSB)
#define SOURCE_ID_MSB        (1 + SOURCE_ID_LSB)
#define EFB_CMND_0           (1 + SOURCE_ID_MSB)
#define EFB_FORMATTER_STAT   (1 + EFB_CMND_0)
#define EFB_RUNTIME_STAT_REG (1 + EFB_FORMATTER_STAT)
#define EVENT_HEADER_DATA    (1 + EFB_RUNTIME_STAT_REG)
#define EV_FIFO_DATA1        (1 + EVENT_HEADER_DATA)
#define EV_FIFO_DATA2        (1 + EV_FIFO_DATA1)

#define EVT_MEM_MODE         (1 + EV_FIFO_DATA2)
#define EVT_MEM_CMND_STAT    (1 + EVT_MEM_MODE)
#define EVT_MEM_RESET        (1 + EVT_MEM_CMND_STAT)
#define EVT_MEM_FLAGS        (1 + EVT_MEM_RESET)
#define EVT_MEM_A_WRD_CNT    (1 + EVT_MEM_FLAGS)
#define EVT_MEM_B_WRD_CNT    (1 + EVT_MEM_A_WRD_CNT)
#define EVT_MEM_PLAY_EVENT   (1 + EVT_MEM_B_WRD_CNT)
#define EVT_MEM_STATUS       (1 + EVT_MEM_PLAY_EVENT)
#define EFB_EVT_CNT          (1 + EVT_MEM_STATUS)
#define EFB_BANDWIDTH_CNT    (1 + EFB_EVT_CNT)
#define EFB_CODE_VERSION     (1 + EFB_BANDWIDTH_CNT)

/* Router FPGA registers- note that addresses in router are done in 4 blocks
 * (1 block per slv) of CMD0(slv), CMD1(slv), .... INT_DELAY _CNT, ..., while here
 * they are indexed (used in the rodRegister structure in accessRegister.c) as
 * CMD0(0), ... CMD0(3), CMD1(0)...  The addressing for rodRegister is done in
 * accessRegister.c  dpsf. */
#define RTR_TRAP_CMND_0(slv)       (14 + EFB_CODE_VERSION + slv)
#define RTR_TRAP_CMND_1(slv)       (1 + RTR_TRAP_CMND_0(N_SDSP-1)  + slv)
#define RTR_TRAP_RESET(slv)        (1 + RTR_TRAP_CMND_1(N_SDSP-1)  + slv)
#define RTR_TRAP_STATUS(slv)       (1 + RTR_TRAP_RESET(N_SDSP-1)   + slv)
#define RTR_TRAP_MATCH_0(slv)      (1 + RTR_TRAP_STATUS(N_SDSP-1)  + slv)
#define RTR_TRAP_MOD_0(slv)        (1 + RTR_TRAP_MATCH_0(N_SDSP-1) + slv)
#define RTR_TRAP_MATCH_1(slv)      (1 + RTR_TRAP_MOD_0(N_SDSP-1)   + slv)
#define RTR_TRAP_MOD_1(slv)        (1 + RTR_TRAP_MATCH_1(N_SDSP-1) + slv)
#define RTR_TRAP_XFR_FRM_SIZE(slv) (1 + RTR_TRAP_MOD_1(N_SDSP-1)   + slv)
#define RTR_TRAP_FIFO_WRD_CNT(slv) (1 + RTR_TRAP_XFR_FRM_SIZE(N_SDSP-1) + slv)
/* 1 unused half-word/slv in router, reserved here (1 word/hw) */
#define RTR_TRAP_EVT_CNT(slv)      (5 + RTR_TRAP_FIFO_WRD_CNT(N_SDSP-1) + slv)
#define RTR_TRAP_INT_DELAY_CNT(slv) (1 + RTR_TRAP_EVT_CNT(N_SDSP-1) + slv)
/* 3 unused hw before beginning of next slave address block */
#define RTR_CMND_STAT              (13 + RTR_TRAP_INT_DELAY_CNT(N_SDSP-1))
#define RTR_SLNK_ATLAS_DUMP_MATCH  (1 + RTR_CMND_STAT)
#define RTR_SLNK_ROD_DUMP_MATCH    (1 + RTR_SLNK_ATLAS_DUMP_MATCH)

#define RTR_CODE_VERSION           (1 + RTR_SLNK_ROD_DUMP_MATCH)
#define RTR_OUTPUT_SIGNAL_MUX      (1 + RTR_CODE_VERSION)

/* ROD resources interface FPGA (RRIF) registers */
#define RRIF_CODE_VERSION         (16 + RTR_OUTPUT_SIGNAL_MUX)
#define RRIF_CMND_1               (1 + RRIF_CODE_VERSION)
#define RRIF_CMND_0               (1 + RRIF_CMND_1)
#define ROD_MODE_REG              (1 + RRIF_CMND_0)
#define FE_MASK_LUT_SELECT        (1 + ROD_MODE_REG)
#define RRIF_STATUS_1             (1 + FE_MASK_LUT_SELECT)
#define RRIF_STATUS_0             (1 + RRIF_STATUS_1)
#define FE_CMND_MASK_0_LO         (3 + RRIF_STATUS_0)
#define FE_CMND_MASK_0_HI         (1 + FE_CMND_MASK_0_LO)
#define FE_CMND_MASK_1_LO         (1 + FE_CMND_MASK_0_HI)
#define FE_CMND_MASK_1_HI         (1 + FE_CMND_MASK_1_LO)
#define CALSTROBE_DELAY           (1 + FE_CMND_MASK_1_HI)
#define CAL_CMND                  (1 + CALSTROBE_DELAY)
#define FRMT_RMB_STATUS           (3 + CAL_CMND)
#define EFB_DM_FIFO_FLAG_STA      (2 + FRMT_RMB_STATUS)
#define EFB_DM_WC_STA_REG         (1 + EFB_DM_FIFO_FLAG_STA)
#define INP_MEM_CTRL              (1 + EFB_DM_WC_STA_REG)
#define DBG_MEM_CTRL              (1 + INP_MEM_CTRL)
#define CFG_READBACK_CNT          (1 + DBG_MEM_CTRL)
#define IDE_MEM_CTRL              (2 + CFG_READBACK_CNT)
#define IDE_MEM_STAT              (1 + IDE_MEM_CTRL)
#define INTRPT_TO_SLV             (7 + IDE_MEM_STAT)
#define INTRPT_FROM_SLV           (1 + INTRPT_TO_SLV)

#define DFLT_ROD_EVT_TYPE         (1 + INTRPT_FROM_SLV)
#define CRTV_ROD_EVT_TYPE         (1 + DFLT_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_0        (1 + CRTV_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_1        (1 + CAL_L1_TRIG_TYPE_0)
#define CAL_L1_ID_0               (1 + CAL_L1_TRIG_TYPE_1)
#define CAL_L1_ID_1               (1 + CAL_L1_ID_0)
#define CAL_BCID                  (1 + CAL_L1_ID_1)

using namespace SctPixelRod;

void initVme(RCCVmeInterface *& vme) {
	std::cout << "Initializing VME Interface ... ";
	try {
		vme = new RCCVmeInterface();
	} catch (VmeException & v) {
		std::cout << "  VmeException:" << std::endl;
		std::cout << "  ErrorClass = " << v.getErrorClass();
		std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
	} catch (BaseException & exc) {
		std::cout << exc << std::endl;
		exit(1);
	} catch (...) {
		std::cout << "Error during VME initialization - exiting!" << std::endl;
		exit(1);
	}
	std::cout << " done!" << std::endl;
}

RodModule *initRod(unsigned long baseAddress, int slot, RCCVmeInterface *& vme) {
	std::cout << "Initializing ROD ... ";
	baseAddress = slot << 24;
	RodModule *rod = NULL;
	// SLOT Number is hardcoded in here...
	try {
		//~ app = new application(vme,tdb);
		rod = new RodModule(baseAddress, (unsigned long) (((0x01000000))),
				*vme, (long) (((4))));
		rod->initialize();
	} catch (BaseException & exc) {
		std::cout << exc << std::endl;
		exit(1);
	} catch (...) {
		std::cout << "Error initializing the ROD - exiting!" << std::endl;
		exit(1);
	}
	std::cout << " done!" << std::endl;
	return rod;
}

BocCard *initBoc(RodModule *& rod) {
	std::cout << "Initializing BOC ... ";
	BocCard *my_boc = NULL;
	try {
		my_boc = rod->getBocCard();
		my_boc->initialize();
		my_boc->readInfo();
	} catch (...) {
		std::cout
				<< "Error during first communication with the BOC... damn'it !"
				<< std::endl;
		exit(1);
	}
	std::cout << " done!" << std::endl;
	return my_boc;
}

void makeSummary(std::string outfile, char pluginSerial[6],
		char powerTestResult[3], char msrTestResult[3]) {
	std::fstream summaryFile("loopTestResult/TestSummary.csv",
			std::fstream::app | std::fstream::out);
	time_t timer = time(NULL);
	summaryFile << timer << "\t" << outfile;
	summaryFile << "\t" << pluginSerial << "\t" << powerTestResult << "\t"
			<< msrTestResult;
	summaryFile << std::endl;
	std::cout << " ... added test to TestSummary.csv !" << std::endl;
	summaryFile.close();
}

void makeHeader(std::string outfile, char pluginSerial[6], char testResult[3],
		char msrTestResult[3]) {
	char tmpOutfile[50] = "loopTestResult/";
	std::fstream testFile(strcat(tmpOutfile, outfile.c_str()),
			std::fstream::app | std::fstream::out);
	time_t timer = time(NULL);
	testFile << "#Time: " << timer << std::endl;
	testFile << "#Serial: " << pluginSerial << std::endl;
	testFile << "#Power: " << testResult << std::endl;
	testFile << "#MSR: " << msrTestResult << std::endl;
	testFile << std::endl;
	std::cout << " ... header written to " << outfile << " !" << std::endl;
	testFile.close();
}

struct ScanBin {
	signed long stream[32];
};

struct ScanBin* GetScanBin(RodModule *m_rod, int numofbits, int m) {
	int formatter;
	unsigned long stream01, stream23;

	struct ScanBin * scanpoint = (struct ScanBin *) malloc(
			sizeof(struct ScanBin));

	for (formatter = m * 2; formatter < ((m * 2) + 2); formatter++) {
		m_rod->mdspSingleWrite(FMT_CNT_CONTROL(formatter), 0x0); // Turn of counting to make the GLB Counter Registers writeable
		m_rod->mdspSingleWrite(FMT_GLB_COUNTER(formatter), (numofbits & 0xFFFF));// Write highest possible value to GLB Counter Registers
		m_rod->mdspSingleWrite(FMT_CNT_CONTROL(formatter), 0x1); // Enable Counting...
	}

	for (formatter = m * 2; formatter < ((m * 2) + 2); formatter++) {
		while (((m_rod->mdspSingleRead(FMT_GLB_COUNTER(formatter)) >> 16)
				& 0xFFFF) > 0)
			; // Wait for global counter to finish
		stream01 = m_rod->mdspSingleRead(FMT_STREAM_01(formatter));// Read back data for Streams 0/1
		stream23 = m_rod->mdspSingleRead(FMT_STREAM_23(formatter));// Read back data for Streams 2/3
		(*scanpoint).stream[(formatter << 2) + 0] = stream01 & 0xFFFF;
		(*scanpoint).stream[(formatter << 2) + 1] = (stream01 >> 16) & 0xFFFF;
		(*scanpoint).stream[(formatter << 2) + 2] = stream23 & 0xFFFF;
		(*scanpoint).stream[(formatter << 2) + 3] = (stream23 >> 16) & 0xFFFF;
		//		printf("%ld\t%ld\t%ld\t%ld\t", (*scanpoint).stream[(formatter<<2)+0], (*scanpoint).stream[(formatter<<2)+1], (*scanpoint).stream[(formatter<<2)+2], (*scanpoint).stream[(formatter<<2)+3]);
	}
	//	printf("\n");
	return scanpoint;
}

void sendCommand(RodModule *m_rod) { //! Send command no out
  int i;
  int packSize = 1;
  int pLen = sizeof(SendSerialStreamIn)/sizeof(uint32_t);
  uint32_t *tsdData = new uint32_t[pLen+packSize];
  for (i=0; i<packSize; i++) {
    tsdData[i+pLen] = 0x100;
  }

  // Set the output mask
  unsigned int mask1 = 0x0, mask2 = 0x0;
  mask1 = 0xffffffff;
  mask2 = 0xffffffff;
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_LO, mask1);
  m_rod->mdspSingleWrite(FE_CMND_MASK_0_HI, mask2);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_LO, 0x00000000);
  m_rod->mdspSingleWrite(FE_CMND_MASK_1_HI, 0x00000000);
//   m_rod->writeRegister(RRIF_CMND_1, 20, 1, 0x1);
//   m_rod->writeRegister(RRIF_CMND_1, 2, 1, 0x1);

//  std::cout << "RodPixControllerFAKE::sendCommand" 
//	    << " mask1 " << std::hex << mask1
//	    << " mask2 " << std::hex << mask2
//	    << std::dec <<  std::endl;

  SendSerialStreamIn *sendSerialStreamIn = (SendSerialStreamIn*)tsdData;
  sendSerialStreamIn->sportId  = 0;
  sendSerialStreamIn->nBits    = packSize*32;
  sendSerialStreamIn->fCapture = 1;
  sendSerialStreamIn->nCount   = 1;
  sendSerialStreamIn->dataPtr  = 0;
  sendSerialStreamIn->maskLo   = mask1;
  sendSerialStreamIn->maskHi   = mask2;

//  std::cout << "Creating ROD Primitive" << std::endl;
  
  RodPrimitive* sendStream;
  sendStream = new RodPrimitive(7+packSize, 0, SEND_SERIAL_STREAM, 0, (long*)sendSerialStreamIn);
//  std::cout << "Sending ROD Primitive" << std::endl;
  m_rod->executeMasterPrimitiveSync(*sendStream);
//  std::cout << "Done with ROD Primitive" << std::endl;
  delete sendStream;
  delete[] tsdData;
}

int main(int argc, char **argv) {
	//Initialization variables
	RCCVmeInterface *vme;
	unsigned long baseAddress;
	uint32_t localEnable;
	uint32_t remoteEnable;
	unsigned int interlock, status;
	std::string unit, type;

	//Start Option Variables
	int slot = -1;
	std::string option;
	bool debug = false;
	bool forceBpm = false;

	//Laser Channels
	int INLINKS[4][8] = { { 2, 3, 4, 5, 6, 7, 8, 9 }, { 14, 15, 16, 17, 18, 19,
			20, 21 }, { 26, 27, 28, 29, 30, 31, 32, 33 }, { 38, 39, 40, 41, 42,
			43, 44, 45 } };
	int OUTLINKS[4][8] = { { 2, 3, 4, 5, 6, 7, 8, 9 }, { 38, 39, 40, 41, 42,
			43, 44, 45 }, { 50, 51, 52, 53, 54, 55, 56, 57 }, { 86, 87, 88, 89,
			90, 91, 92, 93 } };

	//plugin Serials
	char pluginSerial[4][6] = { "dummy", "dummy", "dummy", "dummy" }; // default plugin serial = dummy
	//test results
	char powerTestResult[4][3] = { "NA", "NA", "NA", "NA" }; // default test result = NA
	char msrTestResult[4][3] = { "NA", "NA", "NA", "NA" }; // default test result = NA
	int allWarnings = 0;


	time_t timer = time(NULL);

	int level[50][8][4];
	const int VHalfClock = 0x2;
	const int VInvertClock = 0x1;

	struct ScanBin *CoarseScans[50][4];

	double rxCurrent = 0.300;
	int bpmMarkSpace = 16;
	int fineDelay = 0;
	int forcedBpmMarkSpace = 16;

	// execute program like:
	// ./LoopBocTest -s11 -o -p 123 345 678 901
	// ./LoopBocTest -p 123 456 -s15 -c 0.5

	if (argc > 1) {
		for (int i = 1; i < argc; i++) {
			option = argv[i];
			if (option[0] != '-') {
				continue;
			}
			switch (option[1]) {
			case 's': //slotnumber
			{
				std::stringstream tmp(option.substr(2));
				tmp >> slot;
				break;
			}
			case 'c': //set pin current for MSR test
			{
				std::stringstream tmp(argv[i + 1]);
				tmp >> rxCurrent;
				i++;
				break;
			}
			case 'p': //plugin serials (bottom to top)
			{
				for (int itr = 0; (itr < 4) && (itr + i + 1) < argc; itr++) {
					if (argv[i + 1 + itr][0] == '-') {
						break;
					}
					strcpy(pluginSerial[itr], argv[i + 1 + itr]);
				}
				break;
			}
			case 'b': //set bpmMarkSpace
			{
				forceBpm = true;
				std::stringstream tmp(argv[i + 1]);
				tmp >> forcedBpmMarkSpace;
				i++;
				break;
			}
			case 'd': //debug mode, no putput
			{
				debug = true;
				break;
			}
			default: {
				break;
			}
			}
		}
	}

	// Prompt for slot number
	if (slot < 0) {
		std::cout << "Enter slot number (decimal):";
		std::cin >> slot;
		while ((slot < 1) || (slot > 21)) {
			std::cout << "Slot number out or range [4-12:14-21], re-enter: ";
			std::cin >> slot;
		}
	} else {
		if (slot < 4 || slot == 13 || slot > 21) {
			std::cout << "Slot number out of range [4-12:14-21], re-enter: ";
			std::cin >> slot;
			while ((slot < 1) || (slot > 21)) {
				std::cout
						<< "Slot number out or range [4-12:14-21], re-enter: ";
				std::cin >> slot;
			}
		}
	}

	// Serial check
	std::string really = "NA";
	for (int i = 0; i < 4; i++) {
		std::cout << "plugin nr.  " << i + 1 << " : " << pluginSerial[i]
				<< std::endl;
	}
	std::cout << "Is that right? (yes/no) ";
	std::cin >> really;
	if (!(really == "yes" || really == "y")) {
		std::cout << "Enter new serials (bottom to top) (empty = dummy) ... "
				<< std::endl;
		for (int i = 0; i < 4; i++) {
			std::cout << "Serial of plugin nr. " << i + 1 << " : ";
			std::cin >> pluginSerial[i];
			if (pluginSerial[i] == " ")
				strcpy(pluginSerial[i], "dummy");
		}
	}

	std::cout << std::endl << "Starting initialization in 3s !" << std::endl;

	sleep(3);

	std::cout << "Here we go ..." << std::endl;

	//Initialize VME Interface
	initVme(vme);

	//Initialize ROD
	RodModule *rod = initRod(baseAddress, slot, vme);

	//ROD Status
	std::cout << "Rod Status ..." << std::endl;
	rod->status();
	//ROD Firmware
	unsigned long controllerVersion, formatterVersion, efbVersion,
			routerVersion;
	controllerVersion = 0x0fff & (rod->mdspSingleRead(0x0040440c));
	std::cout << "ROD Controller: " << std::hex << controllerVersion
			<< std::endl;

	formatterVersion = 0x0fff & (rod->mdspSingleRead(0x00400088));
	std::cout << "ROD Formatter:  " << std::hex << formatterVersion
			<< std::endl;

	efbVersion = 0x0fff & (rod->mdspSingleRead(0x0040221c));
	std::cout << "ROD EFB:        " << std::hex << efbVersion << std::endl;

	routerVersion = 0x0fff & (rod->mdspSingleRead(0x0040250c));
	std::cout << "ROD Router:     " << std::hex << routerVersion << std::endl;
	std::cout << std::dec;

	//Initialize BOC
	BocCard *my_boc = initBoc(rod);

	//BOC Status
	std::cout << "Boc Status..." << std::endl;
	std::hex(std::cout);
	std::cout << "BOC, Serial number: 0x" << (my_boc->getSerialNumber() & 0xff)
			<< std::endl;
	std::cout << "Manufacturer      : " << ((my_boc->getManufacturer() & 0xff)
			== 0xcb ? "Cambridge" : "Not Cambridge - strange") << std::endl;
	std::cout << "Module type       : 0x" << (my_boc->getModuleType() & 0xff)
			<< std::endl;
	std::cout << "Firmware revision : 0x" << (my_boc->getFirmwareRevision()
			& 0xff) << std::endl;
	std::cout << "Hardware revision : 0x" << (my_boc->getHardwareRevision()
			& 0xff) << std::endl;
	std::cout << "Status Register   : 0x" << (status
			= (my_boc->getBocStatusRegister() & 0xff)) << std::endl;
	std::cout << "Status Register can be decoded to:" << std::endl;
	std::cout << "Remote Interlock "
			<< ((remoteEnable = ((status >> 7) & 0x1)) ? "Enabled" : "Disabled")
			<< std::endl;
	std::cout << "Local Interlock "
			<< ((localEnable = ((status >> 6) & 0x1)) ? "Enabled" : "Disabled")
			<< std::endl;
	std::cout << "Readout Driver "
			<< (((status >> 5) & 0x1) ? "Not sensed (strange)" : "Sensed")
			<< std::endl;
	std::cout << "Err Flag (Sensing Clocks on RX): " << ((status >> 4) & 0x1)
			<< std::endl;
	std::cout << "Supply Voltages reading "
			<< (((status >> 2) & 0x3 == 0x3) ? "OK" : "NOT ok") << std::endl;
	std::cout << "Remote Clock "
			<< (((status >> 1) & 0x1) ? "OK since last reset"
					: "has been wrong since last reset") << std::endl;

	//Boc Firmware
	if (my_boc->getFirmwareRevision() < 0x83) {
		std::cout << "BOC Firmware Revision to old (Found Version 0x"
				<< my_boc->getFirmwareRevision()
				<< " for these Scans to pass - should be upgraded to 0x84 or later..."
				<< std::endl << "Exiting" << std::endl;

		return 0;
	}

	std::cout << "Initialization successful!" << std::endl << std::endl;

	//Interlock check
	std::cout << "Checking interlocks ...";
	std::dec(std::cout);
	interlock = my_boc->getInterlockStatus(&localEnable, &remoteEnable);
	if (!(interlock)) {
		switch (((localEnable << 1) + remoteEnable) & 0x3) {
		case 0:
			std::cout << "... both interlocks need to be switched";
			break;
		case 1:
			std::cout << "... local interlock needs to be switched";
			break;
		case 2:
			std::cout << "... remote interlock needs switching";
		default:
			std::cout
					<< "... interlocks look fine, even thought getInterlockStatus doesn't say so...";
		}
		std::cout << std::endl << "... exiting and to let you fix it!"
				<< std::endl;
		return 0;
	}
	std::cout << "... done" << std::endl;

	//print ADC monitors
	my_boc->resetMonitorAdc();
	for (int i = 0; i < 12; i++) {
		std::cout << "Monitor channel " << i << " : " << my_boc->getMonitorAdc(
				i, unit, type) << " " << unit << "  " << type << std::endl;
	}

	//reset laser currents
	std::cout << "Resetting laser currents." << std::endl;
	for (int m = 0; m < 4; m++) {
		for (int i = 0; i < 8; i++) {
			my_boc->setLaserCurrent(INLINKS[m][i], 0);
		}
	}

	//TEST
	for (int m = 0; m < 4; m++) {
		int warnings = 0;
		//Laserpower test
		std::cout << std::endl
				<< "Laserpower test begins ... fasten your seatbelts!"
				<< std::endl;

		double pin_current[8][256]; // Variable to store Curves of IFset versus IPin
		std::vector<double> slope[8]; // Variable to store Slope
		double stdDev[8]; // Variable to store Standard Deviation
		double averageSlope[8];
		//check for empty plugin
		if (strcmp(pluginSerial[m], "dummy") == 0) {
			std::cout << " ... empty plugin, skipping laserpower test ..."
					<< std::endl;
		} else {
			std::cout << " ... testing plugin: " << pluginSerial[m] << " ..."
					<< std::endl;
			//setting laser currents and get current
			for (int i = 0; i < 8; i++) {
				averageSlope[i]=0;
				stdDev[i] = 0;
				for (int j = 0; j < 256; j += 4) {
					my_boc->setLaserCurrent(INLINKS[m][i], j);
					usleep(1);
					pin_current[i][j]
							= my_boc->getMonitorAdc((m * 2) + (m % 2));
					if(j>=100){
						(slope[i]).push_back(pin_current[i][j]-pin_current[i][j-4]);
						averageSlope[i] = averageSlope[i] + (pin_current[i][j]- pin_current[i][j-4]);
					}
				}
				my_boc->setLaserCurrent(INLINKS[m][i], 0);
				averageSlope[i] = averageSlope[i]/double(slope[i].size());
			}
			std::cout << " ... done, analysing data ..." << std::endl;
			int checkCurrent = 0;
			for (int i = 0; i < 8; i++) {
				if (pin_current[i][252] >= 1)
					checkCurrent++;
				if (pin_current[i][252] < 1.5){
					std::cout << "WARNING! Pin Current of Plugin "
					<< pluginSerial[m] << " Channel " << i << " is under 1.5mA!" << std::endl;
					allWarnings++;
					warnings++;
				}
				for(int j = 0; j < (slope[i]).size(); j++){
					stdDev[i] = stdDev[i]+ pow(((slope[i])[j]-averageSlope[i]), 2);
				}
				stdDev[i] = sqrt(1/(double(slope[i].size())-1.0)*stdDev[i]);
				std::cout <<  "Average Slope: " << averageSlope[i] << " Std Dev : " << stdDev[i] << std::endl;
				if (stdDev[i]/averageSlope[i] > 0.75){
					std::cout <<"WARNING! Standard Deviation of Slope of Plugin "
					<< pluginSerial[m] << " Channel " << i << " is too high!" << std::endl;
					allWarnings++;
					warnings++;
				}
			}
			if (checkCurrent == 8 && warnings == 0) {
				strcpy(powerTestResult[m], "OK"); // test passed = OK
			} else if(warnings != 0 && checkCurrent == 8){
				strcpy(powerTestResult[m], "NG"); // test was not so good = NG
			} else {
				strcpy(powerTestResult[m], "NP"); //test not passed = NP
			}
			std::cout << " ... done. Test was " << powerTestResult[m]
					<< std::endl;
		}

		//MSR Test
		std::cout << "MSR test begins ... pls hold the line!" << std::endl;
		if (strcmp(pluginSerial[m], "dummy") == 0 || strcmp(powerTestResult[m],
				"NA") == 0) {
			std::cout << " ... empty or broken plugin, skipping MSR test ..."
					<< std::endl;
		} else {
			std::cout << " ... testing plugin: " << pluginSerial[m] << " ..."
					<< std::endl;
			int power = 0;

			for (int i = 0; i < 8; i++) {
				//find power where the pin current is just bigger than rxCurrent (default 300µA)
				for (int j = 0; j < 256; j += 2) {
					if (pin_current[i][j] > rxCurrent) {
						power = j;
						break;
					}
				}
				//setting laser current > 300µA
				my_boc->setLaserCurrent(INLINKS[m][i], power);
				std::cout << " ... Laserpower of channel " << i << " set to "
						<< power << " => " << pin_current[i][power] << "mA ..."
						<< std::endl;
				usleep(1);
			}

			//??
			//setConfigurationMode(rod);
			for (uint32_t formatter = 0; formatter < 8; formatter++) {
				rod->mdspSingleWrite(FMT_LINK_EN(formatter),0x0F); // Look wether there has to be some 89ab !!!
			}
			//writeRegister(rod, RRIF_CMND_1, 25, 4, 4);
			my_boc->setRxDataMode(6);

			//set V half clock 2
			my_boc->setVernierFinePhase(0);

			//4 runs, with differnt finedelays and MarkSpaces
			for (int run = 0; run < 4; run++) {
				//reset clock if it was inverted
				my_boc->setClockControl(VHalfClock);
				switch (run) {
				case (0): {
					bpmMarkSpace = 0;
					fineDelay = 0;
					break;
				}
				case (1): {
					bpmMarkSpace = 31;
					fineDelay = 0;
					break;
				}
				case (2): {
					bpmMarkSpace = 16;
					fineDelay = 0;
					break;
				}
				case (3): {
					bpmMarkSpace = 16;
					fineDelay = 20;
					break;
				}
				}
				//set BpmMarkSpace and BpmFineDelay
				for (int i = 0; i < 8; i++) {
					if (forceBpm) {
						my_boc->setBpmMarkSpace(INLINKS[m][i],
								forcedBpmMarkSpace);
					} else {
						my_boc->setBpmMarkSpace(INLINKS[m][i], bpmMarkSpace);
					}
					my_boc->setBpmFineDelay(INLINKS[m][i], fineDelay);
				}
				for (int delay = 0; delay < 50; delay++) {
					for (int i = 0; i < 8; i++) {
						level[delay][i][run] = 256;
					}

					//Invert V-Clock as soon as Delay is bigger than 24ns for having full range of 0-49ns Delay
					if (delay == 25) {
						//invert clock
						my_boc->setClockControl(VHalfClock | VInvertClock);
					}
					my_boc->setVernierClockPhase0((delay % 25));

					//get rxThreshold in dependency of the delay, where GetScanBin = 0
					for (int rxThreshold = 0; rxThreshold < 255; rxThreshold
							+= 2) {
						//Set rxThreshold
						for (int i = 0; i < 8; i++) {
							my_boc->setRxThreshold(OUTLINKS[m][i], rxThreshold);
						}
						//Get Bins
						CoarseScans[delay][run] = GetScanBin(rod, 0xFFFF, m);
						//Analyse Bins
						for (int i = 0; i < 8; i++) {
							if ((*CoarseScans[delay][run]).stream[(8* m ) + i]
									== 0 && level[delay][i][run] == 256) {
								level[delay][i][run] = rxThreshold;
							}
						}
					}
				}
			}
			std::cout << " ... done, analysing data ..." << std::endl;

			//get offset
			int level_offset[8][4];
			for (int run = 0; run < 4; run++) {
				for (int i = 0; i < 8; i++) {
					level_offset[i][run] = 300;
					for (int delay = 0; delay < 50; delay++) {
						if (level_offset[i][run] > level[delay][i][run]) {
							level_offset[i][run] = level[delay][i][run];
						}
					}
					if(255 - level_offset[i][run] <= 100){
						std::cout << "Something wrong in MSR Test Run " << run
						<< " on Channel " << i+1 << " !" << std::endl;
						warnings++;
						allWarnings++;
					}
				}
			}

			//get pulse width and rising edge
			int pulseWidth[8][4];
			int risingEdge[8][4];
			int checkEdge = 0;
			bool foundIt = false;
			for (int run = 0; run < 4; run++) {
				for (int i = 0; i < 8; i++) {
					pulseWidth[i][run] = 0;
					foundIt = false;
					int isOne = (((255* 2 )+level_offset[i][run]) /3 );

					for (int delay = 0; delay < 50; delay++) {
						if (level[delay][i][run] >= isOne) {
							pulseWidth[i][run]++;
						}
					}

					for(int delay = 0; delay <50; delay++) {
						if(level[delay][i][run] < isOne && level[(delay+1)%50][i][run] >= isOne && !foundIt){
							risingEdge[i][run] = ((delay+1)%50);
							foundIt = true;
						}
					}
				}
			}

			//Added Criteria to not accept step back and yet care for wrap around at 50
			for(int i=0; i<8; i++) {
				if((((risingEdge[i][3] + 50 -risingEdge[i][2]) % 50) >=5 ) && (((risingEdge[i][3] + 50 -risingEdge[i][2]) % 50) <= 30)) {
					checkEdge++;
				}
			}

			if (checkEdge == 8) {
				strcpy(msrTestResult[m], "OK"); //test passes = OK
			} else {
				strcpy(msrTestResult[m], "NP");//test not passes = NP
			}

			//debug terminal output
			if(debug){
				for (int delay = 0; delay < 50; delay++) {
					for (int i = 0; i < 8; i++) {
						std::cout << std::setw(6) << std::setprecision(5)
						<< level[delay][i][2];
					}
					std::cout << std::endl;
				}

				for (int delay = 0; delay < 50; delay++) {
					for (int i = 0; i < 8; i++) {
						std::cout << std::setw(6) << std::setprecision(5)
						<< (*CoarseScans[delay][2]).stream[(8* m ) + i];
						if (delay > 0)
						(*CoarseScans[0][2]).stream[(8* m ) + i]
									+= (*CoarseScans[delay][2]).stream[(8* m ) + i];
					}
					std::cout << std::endl;
				}

				for (int run = 0; run < 4; run++) {
					std::cout << std::endl
					<< "Rising Edge of Run: "<< run << std::endl;
					for (int i = 0; i < 8; i++) {
						std::cout << std::setw(10) << std::setprecision(9)
						<< risingEdge[i][run];
					}
				}

				std::cout << std::endl << "Printing Pulse width of Run 1:" << std::endl;
				for (int i = 0; i < 8; i++) {
					std::cout << std::setw(10) << std::setprecision(9)
					<< ((*CoarseScans[0][2]).stream[(8* m ) + i] / 65535);
				}
				std::cout << std::endl;

				std::cout << std::endl << "Printing Pulse width of Run 2:" << std::endl;
				for (int i = 0; i < 8; i++) {
					std::cout << std::setw(10) << std::setprecision(9)
					<< pulseWidth[i][2];
				}
				std::cout << std::endl;
			//End debug terminal Output
			}

			std::cout << " ... done. Test was " << msrTestResult[m]
			<< std::endl;

			std::cout << "Test complete!" << std::endl;

			//Output
			if (debug) {
				std::cout << "Debug Mode, skipping file output!" << std::endl;
			} else {
				//prepare file
				std::string Output;
				std::string tmpTime;
				std::stringstream ss;
				ss << timer;
				tmpTime = ss.str();
				Output.append(pluginSerial[m]);
				Output.append("_");
				Output.append(tmpTime);
				Output.append(".csv");

				makeHeader(Output, pluginSerial[m], powerTestResult[m],
						msrTestResult[m]);

				char dir[50] = "loopTestResult/";
				std::fstream fileOut(strcat(dir, Output.c_str()),
						std::fstream::app | std::fstream::out);
				//Laserpower test Output
				fileOut << "#Laserpowertest" << std::endl;
				for (int j = 0; j < 256; j += 4) {
					fileOut << std::setw(10) << std::setprecision(5) << j;
					for (int i = 0; i < 8; i++) {
						fileOut << std::setw(10) << std::setprecision(5)
						<< pin_current[i][j];
					}
					fileOut << std::endl;
				}

				//MSR test Output
				for(int run=0; run<4; run++){
					fileOut << "#MSRtestRun" << run << " ";
					switch (run) {
					case (0): {
						fileOut << "#FineDelay=0 #BPMMarkSpace=0 " << std::endl;
						break;
					}
					case (1): {
						fileOut << "#FineDelay=0 #BPMMarkSpace=31 " << std::endl;
						break;
					}
					case (2): {
						fileOut << "#FineDelay=0 #BPMMarkSpace=16 " << std::endl;
						break;
					}
					case (3): {
						fileOut << "#FineDelay=20 #BPMMarkSpace=16 " << std::endl;
						break;
					}
					}
					for (int delay = 0; delay < 50; delay++) {
						fileOut << std::setw(10) << std::setprecision(5) << delay;
						for (int i = 0; i < 8; i++) {
							fileOut << std::setw(10) << std::setprecision(5)
							<< level[delay][i][run];
						}
						fileOut << std::endl;
					}

					fileOut << "#PulseWidth" << std::endl;
					for (int i = 0; i < 8; i++) {
						fileOut << std::setw(10) << std::setprecision(5)
						<< pulseWidth[i][run];
					}
					fileOut << std::endl;

					fileOut << "#Rising Edge" << std::endl;
					for (int i = 0; i < 8; i++) {
						fileOut << std::setw(10) << std::setprecision(5)
						<< risingEdge[i][run];
					}
					fileOut << std::endl;
				}
				std::cout << " ... data written to " << Output << " ! "
				<< std::endl;

				//Summary Output
				makeSummary(Output, pluginSerial[m], powerTestResult[m],
						msrTestResult[m]);
				fileOut.close();
			}
		}
	}

	//reset laser currents
	std::cout << "Resetting laser currents." << std::endl;
	for (int m = 0; m < 4; m++) {
		for (int i = 0; i < 8; i++) {
			my_boc->setLaserCurrent(INLINKS[m][i], 0);
		}
	}

	if(allWarnings > 0){
		std::cout << "#####################" << std::endl << "There were " << allWarnings << " warnings during the test!" <<
		std::endl << "Self destruction in 60 seconds, if you dont fix the problem!!" << std::endl <<
		"#####################" << std::endl;
	}

	std::cout << "Shutting down ... " << std::endl;
	delete rod;
	delete vme;

	return 0;
}

