#include <cstdlib>
#include <fstream>

#include "boost/bind.hpp"
#include "boost/format.hpp"
#include "boost/function.hpp"
#include "boost/lexical_cast.hpp"

#include "DaveModule.h"
#include "RCCVmeInterface.h"

using SctPixelRod::VmeInterface;
using SctPixelRod::BaseException;

namespace Regs = DaveModuleRegisters;

/// A menu has a name, and a function to run if selected
struct MenuItem {
  std::string name;
  boost::function<void ()> f;
};

void runMenu(boost::function<void ()> header, MenuItem *menu)
{
  while(1) {
    std::cout << "===============================\n";
    header();
    std::cout << " Select an option:\n";
    int count = 1;
    for(MenuItem *iter = menu; iter->name != ""; iter++) {
      std::cout << "  " << count++ << " " << iter->name << std::endl;
    }
    std::cout << "  " << 0 << " Exit menu" << std::endl;

    if(!std::cin) {
      std::cout << "Bad input stream\n";
      return;
    }

    int item = -1;

    std::cin >> item;

    if(!std::cin) {
      std::cout << "Bad input stream\n";
      return;
    }

    if(item == 0) {
      std::cout << "Exit menu\n";
      return;
    }

    if(item == -1) {
      std::cout << "Bad input\n";
    } else if(item >= count) {
      std::cout << "Invalid option (max " << (count-1) << ")\n";
    } else {
      std::cout << " Executing " << menu[item-1].name << "\n";

      try {
        menu[item-1].f();
      } catch(SctPixelRod::VmeException &e) {
        std::cout << "Caught VME exception:\n";
        printVmeException(e, std::cout);
      } catch(std::exception &e) {
        std::cout << "Caught C++ exception: " << e.what() << std::endl;
      }
    }
  } // while (1)
}

void readAddress(uint32_t &address) {
  std::string input;
  std::cin >> input;

  if(input[1] == 'x') {
    // hex number
    address = strtoul(&input[2], NULL, 16);
    //    address = boost::lexical_cast<uint32_t>(input);
  } else {
    address = boost::lexical_cast<uint32_t>(input);
  }
}

void badMenuFunction() {
  throw BaseException("Bad menu option");
}

void briefHeader(const std::string str) {
  std::cout << str << std::endl;
}

void readDaveRegister(DaveModule &dave) {
  uint32_t address;
  std::cout << "Enter register addres to read (0-0x5BC)\n" << std::flush;

  readAddress(address);

  std::cout << "Read from address: 0x" << std::hex << address << std::dec << std::endl;

  uint16_t val = dave.readReg(address);
  std::cout << (boost::format("0x%04x %d") % val % val) << std::endl;
}

void writeDaveAddress(DaveModule &dave) {
  uint32_t address;
  std::cout << "Enter register addres to write to (0-0x5BC)\n" << std::flush;

  readAddress(address);

  uint32_t value;
  std::cout << "Enter value to write to address 0x" << std::hex << address << std::dec << std::endl;

  readAddress(value);

  std::cout << "Writing data...\n";

  dave.writeReg(address, value);
  // std::cout << (boost::format("0x%04x %d") % val % val) << std::endl;

  std::cout << " ...written data\n";
}

void EnableSRAMrecording(DaveModule &dave) {
  uint16_t SINK_ADDR_CLR = 1 << 9;
  uint16_t SINK_MODE     = 1 << 8;
  int port = Regs::INT_ENA;
  int u = dave.readReg(port);

  dave.writeReg(port, u & (~SINK_MODE)); // Disable SRAM recording

  dave.writeReg(port, u | SINK_ADDR_CLR);     // SRAM Sink mode address clear
  dave.writeReg(port, u & (~SINK_ADDR_CLR));  // set to offset 0x000000

  dave.writeReg(port, u | SINK_MODE); // Enable SRAM recording
}

void DisableSRAMrecording(DaveModule &dave) {
  uint16_t SINK_MODE     = 1 << 8;
  int port = Regs::INT_ENA;
  int u = dave.readReg(port);
  dave.writeReg(port, u & (~SINK_MODE)); // Disable SRAM recording
}

void PrintSRAM(DaveModule &dave) {
  uint SeqSink_Lo = dave.readReg(Regs::SSAD_LO);
  uint SeqSink_Hi = dave.readReg(Regs::SSAD_HI);
  uint SeqSink = SeqSink_Hi << 16 | SeqSink_Lo;

  printf("%d count\n", SeqSink);
  for (uint k = 0; k < SeqSink; k=k+2) {
    uint u = dave.readReg(0x800000 + k);

    printf("%4d: %04x %05d\n", k, u, u);
  }
}

void PrintSRAMDecoded(DaveModule &dave) {
  uint16_t SeqSink_Lo = dave.readReg(Regs::SSAD_LO);
  uint16_t SeqSink_Hi = dave.readReg(Regs::SSAD_HI);
  uint16_t SeqSink = SeqSink_Hi << 16 | SeqSink_Lo;

  printf("%d count\n", SeqSink);
  for (uint k = 0; k < SeqSink; k=k+4) {
    uint16_t p_lo = dave.readReg(0x800000 + k);
    uint16_t v = dave.readReg(0x800000 + k + 2);

    uint16_t sig = v >> 12;
    uint16_t p_hi = v & 0xfff;

    printf("%08x: %01x %03x%04x\n", k, sig, p_hi, p_lo);
  }
}

void dumpSRAMBlock(DaveModule &dave, std::ostream &os, uint32_t offset, int events, bool sixteenNot32) {
  int bytesPerEntry = sixteenNot32?2:4;

  for(uint32_t o=offset; o<offset + events*bytesPerEntry; o+=bytesPerEntry) {
    uint32_t addr = 0x800000+o;

    uint32_t val = dave.readReg(addr);
    if(!sixteenNot32) {
      val |= (dave.readReg(addr + 2) << 16);
    }

    uint32_t period;
    if(sixteenNot32) period = val & 0xfff;
    else period = val & 0xfffffff;

    uint32_t flags;
    if(sixteenNot32) flags = val >> 12;
    else flags = val >> 28;

    std::string flagString = "";

    if(flags == 0) flagString += " Busy off";
    if(flags & 1) flagString += " Trigger";
    if(flags & 2) flagString += " BCR";
    if(flags & 4) flagString += " ECR";
    if(flags & 8) flagString += " Busy on";

    os << (boost::format(sixteenNot32?"0x%08x: 0x%04x: %d%s\n":"0x%08x: 0x%08x: %d%s\n") % addr % val % period % flagString);
  }
}

void dumpSRAM(DaveModule &dave, bool sixteenNot32) {
  std::string filename("DaveSRAM.bin");

  std::ofstream ofs(filename.c_str(), std::ios::binary | std::ios::trunc);

  // Next address to write
  uint32_t sinkAddress = (dave.readReg(Regs::SSAD_HI) << 16) | dave.readReg(Regs::SSAD_LO);
  // Number of events recorded
  uint32_t sinkCount = (dave.readReg(Regs::SSCOUNT_HI) << 16) | dave.readReg(Regs::SSCOUNT_LO);

  std::cout << (boost::format("Using address 0x%08x count 0x%08x\n") % sinkAddress % sinkCount);

  const uint32_t sramMaxOffset = 0x800000;

  int bytesPerEntry = sixteenNot32?2:4;

  if(sinkCount * bytesPerEntry > sinkAddress) {
    // Maximum events
    sinkCount = sramMaxOffset / bytesPerEntry;
    std::cout << (boost::format("SRAM wrapped, only readout the 0x%x stored events\n") % sinkCount);

    uint32_t secondBlockEvents = sinkAddress/bytesPerEntry;

    uint32_t firstBlockEvents = sinkCount - secondBlockEvents;
    uint32_t firstOffset = sramMaxOffset - (firstBlockEvents*bytesPerEntry);

    std::cout << (boost::format("Wrapped block offset 0x%08x counts 0x%08x+0x%08x\n") % firstOffset % firstBlockEvents % secondBlockEvents);

    dumpSRAMBlock(dave, ofs, firstOffset, firstBlockEvents, sixteenNot32);
    dumpSRAMBlock(dave, ofs, 0, secondBlockEvents, sixteenNot32);
  } else {
    uint32_t offset = sinkAddress - sinkCount * bytesPerEntry;

    std::cout << (boost::format("Normal block offset 0x%08x\n") % offset);

    dumpSRAMBlock(dave, ofs, offset, sinkCount, sixteenNot32);
  }

  ofs.close();

  std::cout << "SRAM saved to " << filename << std::endl;
}

void loadSRAM(DaveModule &dave) {
  std::string filename("DaveSRAM.bin");

  std::ifstream ifs(filename.c_str());

  int linenum = 0;
  while(ifs) {
    // 0x008e95c0: 0x1005: 5 Trigger
    char line[64];
    ifs.getline(line, 64);

    if(!ifs) break;

    if(line[10] != ':') {
      std::cout << "Parse error, line " << linenum << std::endl;
      return;
    }

    if(line[22] == ':') {
      std::cout << "Parse error, expecting 16-bit data on line " << linenum << std::endl;
      return;
    }

    if(line[18] != ':') {
      std::cout << "Parse error, line " << linenum << std::endl;
      return;
    }

    uint16_t value = strtoul(&line[14], NULL, 16);

    dave.writeReg(0x800000 + linenum * 2, value);

    linenum ++;
  }

  std::cout << "Written " << linenum << " entries\n";

  // Write end address
  dave.writeReg(Regs::SEQ_END_LO, (linenum * 2) & 0xffff);
  dave.writeReg(Regs::SEQ_END_HI, ((linenum * 2) >> 16) & 0xffff);
}

void runSRAM(DaveModule &dave) {
  std::cout << "Enabling sequencer and all outputs\n";
  // Enable all outputs from sequencer
  dave.writeReg(Regs::SEQ_ENA, 0x187);

  std::cout << "Running sequence\n";
  // Run sequence
  dave.writeReg(0x300, 0x100);
}

void waitForSRAMComplete(DaveModule &dave) {
  uint32_t seqAddress = (dave.readReg(Regs::SEQ_END_HI) << 16) | dave.readReg(Regs::SEQ_END_LO);

  uint32_t sramAddress = 0; // Impossible

  while (1) {
    if(sramAddress == seqAddress) {
      std::cout << "SRAM sequencer complete\n";
      break;
    }

    uint32_t newSramAddress = (dave.readReg(Regs::SSAD_HI) << 16) | dave.readReg(Regs::SSAD_LO);

    if(newSramAddress == sramAddress) {
      std::cout << "SRAM sequencer stalled\n";
      break;
    }

    sramAddress = newSramAddress;

    // 2 ms is more than enough to play out the longest delta
    usleep(2000);
  }
}

const char *statusNames[16] = {
  "Sanity 0xDAFE",
   "",
  "Firmware Version",
  "Board + hardware Version",
  "Board status",
  "FP LEDs",
  "Lemo Input status",
  "Lemo Output status",
  "L1ID Lo",
  "L1ID Hi",
  "Seq add Lo",
  "Seq add Hi",
  "Seq count Lo",
  "Seq count Hi",
  "",
  "",
};

const char *registerNames[16] = {
  "Lemo input enables",
  "Lemo output enables",
  "Internal enables",
  "Random seed lo",
  "Random seed hi",
  "Random rate",
  "Simple dead-time",
  "Complex dead-time level",
  "Complex dead-time rate",
  "BCID offset",
  "Lemo inputs invert",
  "Lemo outputs invert",
  "",
  "",
  "",
  "",
};

void printDaveRegisters(DaveModule &dave, std::ostream &os) {
  os << "Status registers (0x000)\n";

// *** Matt changed from 32 to 16 to make screen space
  for(int i=0; i<16; i++) {
    uint16_t val = dave.readReg(i * 2);
    os << (boost::format("%3d: 0x%04x %5d %s\n") % i % val % val % statusNames[i]);
  }

  os << "Register block (0x100)\n";

// *** Matt changed from 32 to 16 to make screen space
  for(int i=0; i<16; i++) {   

    uint16_t val = dave.readReg(0x100 + i * 2);
    os << (boost::format("%3d: 0x%04x %5d %s\n") % i % val % val % registerNames[i]);
  }

  os << "Delay25 I2C register (0x200): " << dave.readReg(0x200) << std::endl;

// *** Matt removed bunch-groups as not readable yet
#if 0
  os << "Bunch group register block\n";

  for(int i=0; i<222; i++) {
    if((i%10) == 0) {
      os.width(3);
      os << i << ":";
    }

    os << " " << dave.readReg(0x400 + i * 2);

    if((i%10) == 9) {
      os << std::endl;
    }
  }
#endif

  os << std::endl;
}

void printDaveRegisters(DaveModule &dave) {
  printDaveRegisters(dave, std::cout);
}

void saveDaveRegisters(DaveModule &dave) {
  std::string filename("DaveRegisters.bin");

  std::ofstream ofs(filename.c_str(), std::ios::trunc);

  printDaveRegisters(dave, ofs);

  ofs.close();

  std::cout << "Registers saved to " << filename << std::endl;
}

void loadDefaultConfig(DaveModule &dave) {
  int port;
  port = Regs::LEMOIN_ENA;  dave.writeReg(port, 0x0080); printf("0x%3X: Lemo Input:  %4d\n", port, dave.readReg(port));
  port = Regs::LEMOOUT_ENA; dave.writeReg(port, 0x0103); printf("0x%3X: Lemo Output: %4d\n", port, dave.readReg(port));
  port = Regs::INT_ENA;     dave.writeReg(port, 0x2c87); printf("0x%3X: Internal:    %4d\n", port, dave.readReg(port));
  port = Regs::RND_RATE;    dave.writeReg(port,   8); printf("0x%3X: Random Rate: %4d\n", port, dave.readReg(port));
  port = Regs::SDT_LENGTH;  dave.writeReg(port,   5); printf("0x%3X: Simple Dead-time Length: %1d\n", port, dave.readReg(port));
  port = Regs::CDT_LEVEL;   dave.writeReg(port, 0x0104);
  printf("0x%3X: Complex Dead-time Level: %1d\n", port, dave.readReg(port) & 0xff);
  printf("0x%3X: Complex Dead-time Fill step: %1d\n", port, (dave.readReg(port)>>8) & 7);
  port = Regs::CDT_RATE;    dave.writeReg(port, 415); printf("0x%3X: Complex Dead-time Rate: %1d\n", port, dave.readReg(port));
  port = Regs::BCID_OFFSET; dave.writeReg(port, 197); printf("0x%3X: BCID offset: %4d\n", port, dave.readReg(port));
  port = Regs::SEQ_ENA;     dave.writeReg(port, 0);   printf("0x%3X: SEQ enables: %4d\n", port, dave.readReg(port));
}

void loadCollidingBCID(DaveModule &dave) {
  std::string filename("CollidingBCID.txt");
  const int slen = 20000;
  char s[slen];
  bool error = false;

  {
    int fill;
    std::ifstream ifs(filename.c_str(), std::ifstream::in);
    std::cout << "Fills: ";

    while (!ifs.eof() && !ifs.fail()) {
      ifs >> fill;
      ifs.getline(s, slen);
       //ifs.ignore(100000, '\n');
      std::cout << fill << " ";
    }
    std::cout << std::endl;
    ifs.close();
  }

  std::cout << "Enter Fill number:" << std::flush;
  std::string input;
  std::cin >> input;

  int FillNum = std::atoi(input.c_str());

  {
    int fill;
    bool found = false;
    std::ifstream ifs(filename.c_str(), std::ifstream::in);

    while (!ifs.eof() && !ifs.fail() && fill != FillNum) {
      ifs >> fill;
      if (fill == FillNum) {
        found = true;
        continue;
      }
      ifs.getline(s, slen);
      //ifs.ignore(100000, '\n');
    }

    uint16_t BunchBlock[223];
    for (int i = 0; i < 223; i++) BunchBlock[i] = 0;

    bool loaded = false;
    if (found) {
      loaded = true;
      int bcid;
      char c;
      c = ' ';
      while (c == ' ' && ifs.good()) ifs.get(c);
      if (ifs.fail() || c != '[') {
        std::cout << " stream error or cannot find '[', found '" << c << "'" << std::endl;
      } else {
        printf("\n[");
        c = ',';
        while (c == ',' && !ifs.fail()) {
          ifs >> bcid;
          c = ' ';
          while (c == ' ' && ifs.good()) ifs.get(c);
          if (c != ',' && c != ']') loaded = false;
          std::cout << bcid << c;
          if (0 < bcid && bcid <= 223*16) {
            bcid--;
            int reg = bcid / 16;
            int bit = bcid % 16;
            BunchBlock[reg] |= 1 << bit;
          }
          else loaded = false;
        }
        printf("\n");
      }
    } // found
    loaded = loaded && !ifs.fail();
    ifs.close();

    if (loaded) {
      for(int i=0; i<223; i++) {
        if (i % 10 == 0) printf("\n%3d ", i);
        dave.writeReg(0x400 + i * 2, BunchBlock[i]);
        printf(" %04X", BunchBlock[i]);
      }
      printf("\n");
    }

    printf("Fill %d", FillNum);
    if (!found) printf(" not found.");
    else {
      if (loaded) printf(" loaded.");
      else        printf(" load ERROR.");
    }
    printf("\n");
  }
  if (error) printf("Stream ERROR.\n");
}

void startDaveMenu(DaveModule &dave)
{
  MenuItem daveMenu[] = {
                         {"Load Default config for TTC2LAN", boost::bind(&loadDefaultConfig, boost::ref(dave))},
                         {"Read register", boost::bind(&readDaveRegister, boost::ref(dave))},
                         {"Write register", boost::bind(&writeDaveAddress, boost::ref(dave))},
                         {"Print registers", boost::bind(&printDaveRegisters, boost::ref(dave))},
                         {"Dump registers to file", boost::bind(&saveDaveRegisters, boost::ref(dave))},
                         {"Dump SRAM to file (32-bit sink)", boost::bind(&dumpSRAM, boost::ref(dave), false)},
                         {"Dump SRAM to file (16-bit sink)", boost::bind(&dumpSRAM, boost::ref(dave), true)},
                         {"Load SRAM from file (16-bit sink)", boost::bind(&loadSRAM, boost::ref(dave))},
                         {"Execute SRAM", boost::bind(&runSRAM, boost::ref(dave))},
                         {"Await SRAM", boost::bind(&waitForSRAMComplete, boost::ref(dave))},
                         {"Load Colliding BCIDs", boost::bind(&loadCollidingBCID, boost::ref(dave))},
                         {"Enable  SRAM recording", boost::bind(&EnableSRAMrecording, boost::ref(dave))},
                         {"Disable SRAM recording", boost::bind(&DisableSRAMrecording, boost::ref(dave))},
                         {"Print SRAM (16-bit)", boost::bind(&PrintSRAM, boost::ref(dave))},
                         {"Print SRAM Decoded (32-bit sink)", boost::bind(&PrintSRAMDecoded, boost::ref(dave))},
                         {"", &badMenuFunction}};

  runMenu(boost::bind(&briefHeader, "Dave card access"), daveMenu);
}

void selectDave(VmeInterface &vme) {
  uint32_t address;
  std::cout << "Enter card id (0-255)\n" << std::flush;
  readAddress(address);

  if(!std::cin) {
    std::cout << "bad input\n";
    return;
  }

  // This is enough for the SRAM (0x00800000-0x00FFFFFE) as well as registers (below 0x1000)
  DaveModule d(address << 24 , 0x01000000, vme);

  startDaveMenu(d);
}

void startTopMenu(VmeInterface &vme)
{
  MenuItem topMenu[] = {// {"List cards", boost::bind(&listCards, var)},
                        {"Select card address to debug", boost::bind(&selectDave, boost::ref(vme))},
                        {"", &badMenuFunction}};

  runMenu(boost::bind(&briefHeader, "Top-level Dave card access"), topMenu);
}

int main(int argc, char *argv[]) {
  SctPixelRod::RCCVmeInterface vme;

  startTopMenu(vme);
}
