#include <iostream>
#include <iomanip>

#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

uint16_t YesNo(uint16_t regstatus);

int main(int argc, char **argv) {

  bool minimalStatus = false;
  bool shifterMode = false;
  for(int i = 0 ; i < argc ; ++i) {
    if( string(argv[i]) == "--minimal" ) minimalStatus = true;
    else if( string(argv[i]) == "--min" ) minimalStatus = true;
    else if( string(argv[i]) == "--shifter" ) shifterMode = true;
  }

  if(std::string(argv[0]).find("TimStatusShifter") != std::string::npos) shifterMode = true;
  if(shifterMode) minimalStatus = true;

  VmeInterface *vme=nullptr;
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }

  /*
  try {
   
  } catch (...) {
    throw TimException("Exception caught in TimModule creation", 0, 0 );
  }
  */
  try{
    uint16_t regstatus, timID, timfirm, timSN;
    uint32_t regstatus32;
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    timID = tim->fetchTimID();
    timSN = (timID & 0xFF);
    timfirm = (timID >> 8);
    //if (timSN >= 0x20) timfirm += 0xF00; // FPGA
    hex(cout);
    cout << "General TIM information (all dec unless specified):" << endl;
    cout << "TIM serial number (hex): " << hex <<timSN << endl;
    cout << "TIM firmware (hex): " << hex << timfirm << endl;
    dec(cout);
    //==============================================================
    if (!(tim->regFetch(TIM_REG_STATUS)& 0x2000)) cout << "TIM is not OK (clk not running)!";
    else {
      cout << "Running clock source: ";
      if (timfirm>12) {                          // to be precise following is valid for TIM3
	regstatus=tim->regFetch(TIM_REG_STATUS3);      
	if ((regstatus >>7) & 0x1) cout << "TTC" << endl;
	if ((regstatus >>8) & 0x1) cout << "External" << endl;
	if ((regstatus >>9) & 0x1) cout << "Internal" << endl;
	if (!((regstatus >>10) & 0x1)) cout << "Error: Clock PLL not stable after switching clocks" << endl;
      }
      else {
	regstatus=tim->regFetch(TIM_REG_STATUS);
	if ((tim->regFetch(TIM_REG_RUN_ENABLE) & 0x1) && (regstatus & 0x0100)) cout << "TTC" << endl; // TTC clk enabled && present
	else if ((tim->regFetch(TIM_REG_ENABLES)>>8) & 0x1) cout << "External" << endl;
	else if (regstatus & 0x0200) cout << "Internal" << endl;
      }


    cout << "====================== RodBusy status =================== " << endl;
    regstatus=tim->regFetch(TIM_REG_RB_MASK);
    cout << "ROD Slot#:  5  6  7  8  9 10 11 12 14 15 16 17 18 19 20 21" << endl;
    cout << "OR'ed       ";
    for (int i=0;i<16;i++) {
      cout << (regstatus & 0x1) << "  ";
      regstatus>>=1;
    }
    cout << endl;
    regstatus=tim->regFetch(TIM_REG_RB_STAT);
    cout << "BUSY        ";
    for (int i=0;i<16;i++) {
      cout << (regstatus & 0x1) << "  ";
      regstatus>>=1;
    }
    cout << endl;

      if (timfirm>12) {                          // to be precise following is valid for TIM3
	if ((regstatus >>12) & 0x1) {
          cout << "!!!!!==============================================!!!!!" << endl;
          cout << "!!!!!===FFTV-B(UNCH) BUSY VETO EMERGENCY ACTION====!!!!!" << endl;
          cout << "!!!!!==============================================!!!!!" << endl;
	}
	if ((regstatus >>13) & 0x1) {
	  cout << "!!!!!==============================================!!!!!" << endl;
	  cout << "!!!!!===FFTV-T(RIGGER) BUSY VETO EMERGENCY ACTION==!!!!!" << endl;
	  cout << "!!!!!==============================================!!!!!" << endl;
	}
	regstatus=tim->regFetch(TIM_REG_BUSY_STAT3);
	if ((regstatus >>13) & 0x1) {
	  cout << "!!!!!==================================================!!!!!" << endl;
	  cout << "!!!!!===FFTV EMERGENCY ACTION, TRIGGER ARRIVED @ VETO==!!!!!" << endl;
	  cout << "!!!!!==================================================!!!!!" << endl;
	}
      }
    }
    //==============================================================
    cout << "Trigger source: ";
    regstatus=tim->regFetch(TIM_REG_RUN_ENABLE);
    if ((regstatus >>1) & 0x1) cout << "TTC" << endl;
    else {
      if ((timfirm > 13) && (tim->regFetch(TIM_REG_ENABLES3) & 0x0800)) cout << "TTC delayed" <<endl;
      else {
	regstatus=tim->regFetch(TIM_REG_ENABLES);
	if ((regstatus >>9) & 0x1) cout << "External" << endl; // just enables the input
	else
	  if ((regstatus >>1) & 0x1) cout << "Internal with frequency: " << endl; //tim->triggerFrequency() << endl; // not correctly determined 
	  else
	    if ((timfirm > 12) && (tim->regFetch(TIM_REG_ENABLES3) & 0x0040)) cout << "Internal improved randomizer" << endl;
	    else cout << "FP button" << endl;
      }
      cout << "Trigger delay value (bcos): " << dec << tim->regFetch(TIM_REG_DELAY) << endl;
    }
    //==============================================================
    regstatus=tim->regFetch(TIM_REG_BUSY_STAT3);
    if (regstatus & 0x0400) {
      cout << "====> TIM sends BUSY signal upstream !!!, reason:"<< endl;
      if (regstatus & 0x0001) cout << "====> one of the RODs is BUSY. "<< endl;
      if (regstatus & 0x0002) cout << "====> BUSY sourced by extrnal RODbusy input. "<< endl;
      if (regstatus & 0x0004) cout << "====> VME RODbusy command is issued. "<< endl;
      if (!(tim->regFetch(TIM_REG_STATUS) & 0x0400) && !(tim->regFetch(TIM_REG_DEBUG_CTL)&TIM_BIT_DEBUGCTL_SARBDISABLE)) 
	cout << "====> TIM is not in TTC operation mode and RodBusy signal is set (reset state). "<< endl;
      if (regstatus & 0x4000) cout << "====> FFTV BUSY is active. "<< endl;
      if (regstatus & 0x0100) cout << "====> Clock switch BUSY is active. "<< endl;
    }

    //==============================================================
    regstatus=tim->regFetch(TIM_REG_STATUS);
    if ((regstatus & 0x0004) && (!(tim->regFetch(TIM_REG_RUN_ENABLE) & 0x0002))) cout << "TIM stopping triggers internally" << endl;
    if (regstatus & 0x0008) cout << "TIM is internally busy" << endl;
    //==============================================================
    if(minimalStatus) return EXIT_SUCCESS;

    cout << "External inputs used (enabled): " << endl;
    regstatus=tim->regFetch(TIM_REG_ENABLES);
    if ((regstatus >>8) & 0x1) cout << "Clock ";
    if ((regstatus >>9) & 0x1)  cout << "L1A Trigger ";
    if ((regstatus >>10) & 0x1)  cout << "ECReset ";
    if ((regstatus >>11) & 0x1)  cout << "BCReset ";
    if ((regstatus >>12) & 0x1)  cout << "Cal strobe ";
    if ((regstatus >>13) & 0x1)  cout << "FEReset ";
    if ((regstatus >>14) & 0x1)  cout << "Sequencer Go ";
    if ((regstatus >>15) & 0x1)  cout << "Ext Busy ";
    cout << endl;	   
    //==============================================================	 
    cout << "TTC inputs used (enabled): " << endl;
    regstatus=tim->regFetch(TIM_REG_RUN_ENABLE);
    if (regstatus & 0x1) cout << "Clock ";
    if ((regstatus >>1) & 0x1)  cout << "L1A Trigger ";
    if ((regstatus >>2) & 0x1)  cout << "ECReset ";
    if ((regstatus >>3) & 0x1)  cout << "BCReset ";
    if ((regstatus >>4) & 0x1)  cout << "Cal strobe ";
    if ((regstatus >>5) & 0x1)  cout << "FEReset ";
    if ((regstatus >>6) & 0x1)  cout << "Spare ";
    cout << endl;
    cout << "Serial outputs enabled: " << endl;
    if ((regstatus >>9) & 0x1)  cout << "L1A&BCID";
    if ((regstatus >>10) & 0x1)  cout << " TType";
    cout << endl;
    //==============================================================
    if (tim->regFetch(TIM_REG_STATUS) & 0x0020) cout << "Sequencer is running" << endl;
    //==============================================================    
    cout << "L1ID counter value: " << tim->fetchL1ID() << endl;
    regstatus = tim->regFetch( TIM_REG_L1IDH );
    cout << "ECR counter value: " << ((regstatus & 0xff00) >> 8) << endl;
    regstatus = tim->regFetch( TIM_REG_TTID );
    cout << "TTC trigger type: " << (regstatus & 0x00ff) << " TIM TT: "<< ((regstatus & 0xff00) >> 8) << endl;
    regstatus = tim->regFetch( TIM_REG_BCID );
    cout << "BCID value: " << (regstatus & 0x0fff) << " offset: "<< ((regstatus & 0xf000) >> 12) << endl;
    regstatus = tim->regFetch( TIM_REG_TTC_BCID );
    cout << "(crosscheck) TTC BCID value: " << (regstatus & 0x0fff) << endl;
    cout << endl;

    unsigned long long int den = tim->regFetch(TIM_REG_BCOUNTL);
    den |= ((unsigned long long int)tim->regFetch(TIM_REG_BCOUNTH))<<16;
    den |= ((unsigned long long int)tim->regFetch(TIM_REG_BCOUNTX))<<32;
    cout << "BUSY (->ATLAS) in s: " << setprecision(3) << (double)den * 25.0e-9 << endl;
    //    cout << "BUSY cnts: "<< hex << den << dec << endl;
    cout << endl;

    cout << "FFTV-T(rigger) configuration: " << endl;
    cout << "THR: " << (tim->regFetch(TIM_REG_FV_MATCH) & 0x000f)
	 << ", DELTAT: " << tim->regFetch(TIM_REG_FV_DELTAT)
	 << ", EMERG_THR: " << ((tim->regFetch(TIM_REG_FV_MATCH)>>8) & 0xff);
    if (timfirm>0x4e) cout << ", EMERG_LEAK [s]: " << (tim->regFetch(TIM_REG_FV_EMERG_TO) & 0xffff); 
    cout << endl;
    cout << "FFTV-T(rigger) monitoring: "<< endl;
    regstatus32 = (tim->regFetch( TIM_REG_FV_IDH )<<16) + tim->regFetch( TIM_REG_FV_IDL );
    if (timfirm>0x4f) {
      cout << "# of FFTV-Ts: " << regstatus32 << endl;
      cout << "Emergency cnts: "<< tim->regFetch(TIM_REG_FV_EMERG_CNT) <<endl;
    } else cout << "# of FFTV-Ts: " << regstatus32 << endl;
    
    unsigned long long int num = tim->regFetch(TIM_REG_FV_COUNTL);
    num |= ((unsigned long long int)tim->regFetch(TIM_REG_FV_COUNTH))<<16;
    num |= ((unsigned long long int)tim->regFetch(TIM_REG_FV_COUNTX))<<32;
    //cout << num << " num|den "<< den << endl;
    cout << "FFTV-T BUSY in s: " << setprecision(3) << (double)num * 25.0e-9 << endl;
    if (den!=0) cout << "FFTV-T fraction of BUSY: "<< (double)num/(double)den<< endl;
    if (timfirm>0x40) { 
      cout << endl;
      cout << "FFTV-B(unch) configuration: "<< endl;
      cout << "Threshold for FFTV-B action: "<< tim->regFetch(TIM_REG_FVB_MATCH)<< endl;
      if (timfirm>0x48) {
	regstatus=tim->regFetch(TIM_REG_CONTROL);
	cout << "Option (+1,-2): "<< (regstatus & 0x1)<< endl;
      }
      cout << "FFTV-B(unch) monitoring: "<< endl;
      regstatus32 = (tim->regFetch( TIM_REG_FVB_IDH )<<16) + tim->regFetch( TIM_REG_FVB_IDL );
      cout << "# of FFTV-Bs: " << regstatus32 << endl;
      num = tim->regFetch(TIM_REG_FVB_COUNTL);
      num |= ((unsigned long long int)tim->regFetch(TIM_REG_FVB_COUNTH))<<16;
      num |= ((unsigned long long int)tim->regFetch(TIM_REG_FVB_COUNTX))<<32;
      cout << "FFTV-B BUSY in s: " << setprecision(3) << (double)num * 25.0e-9 << endl;    
      if (den!=0) cout << "FFTV-B fraction of BUSY: "<< (double)num/(double)den<< endl;
    }
    cout << endl;
    regstatus32 = (tim->regFetch( TIM_REG_FV_TCOUNTH )<<16) + tim->regFetch( TIM_REG_FV_TCOUNTL );
    cout << "# of FFTVeto'ed triggers (by all FFTVs): " << regstatus32 << endl;
    cout << endl;
    if (timfirm>0x48) {
      regstatus=tim->regFetch(TIM_REG_CONTROL);
      if ((regstatus>>1) & 0x1) { 
	cout << "FFTV-B snapshot mode is ACTIVE: " << endl;
	cout << "BCID: "<< tim->regFetch(TIM_REG_FVB_SSBCID) <<" (trigger)\t"<< tim->regFetch(TIM_REG_FVB_SS0CNTR)<<" (int. counter)"<<  " Lvl1ID: "<< tim->regFetch(TIM_REG_FVB_SSL1ID)<<endl;
	cout << "FFTV-B counters" << endl;
	const int NCntRegs=14;
	const TimRegister SectCnts[NCntRegs] = {TIM_REG_FVB_SECTCNTS0, TIM_REG_FVB_SECTCNTS1, TIM_REG_FVB_SECTCNTS2,
						TIM_REG_FVB_SECTCNTS3, TIM_REG_FVB_SECTCNTS4, TIM_REG_FVB_SECTCNTS5,
						TIM_REG_FVB_SECTCNTS6, TIM_REG_FVB_SECTCNTS7, TIM_REG_FVB_SECTCNTS8,
						TIM_REG_FVB_SECTCNTS9, TIM_REG_FVB_SECTCNTS10, TIM_REG_FVB_SECTCNTS11,
						TIM_REG_FVB_SECTCNTS12, TIM_REG_FVB_SECTCNTS13};
	uint16_t add(0), cnt(0);
	for (int is=0;is<NCntRegs;is++) {
	  regstatus = tim->regFetch(SectCnts[is]);
	  for (int ii=0;ii<4;ii++) {
	    cnt = regstatus&0xf;
	    cout << dec <<"\tbcid("<<setw(4)<<256*is+add<<"-"<<setw(4)<<256*is+add+96<<"): "<< hex<< cnt; 
	    regstatus=(regstatus>>4);
	    add+=64;
	  }
	  cout << endl;
	  add = 0;
	}
	cout << dec << endl;
      }
    }
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}

uint16_t YesNo(uint16_t regstatus)
{
  if (regstatus & 0x0001) cout << "Yes"<< endl;
      else cout << "No"<< endl;
      return regstatus>>1; 
}
