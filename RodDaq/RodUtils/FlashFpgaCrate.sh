#!/bin/bash
#
source /det/pix/scripts/Default-pixel.sh 
$ROD_DAQ/RodUtils/CrateFpgaFlash FMT /det/pix/Binary/FPGA/fmt_p47-2f.bin
$ROD_DAQ/RodUtils/CrateFpgaFlash RTR /det/pix/Binary/FPGA/rtr_v33f.bin
$ROD_DAQ/RodUtils/CrateFpgaFlash RCF /det/pix/Binary/FPGA/rcf_v54f.bin
$ROD_DAQ/RodUtils/CrateFpgaFlash EFB /det/pix/Binary/FPGA/efb_v45f.bin
