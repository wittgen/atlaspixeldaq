//--------------------------------CrateFlash-------------------------------- 

/*! 
 *  @brief This is a utility to reflash an entire crate
 */

#include <iostream>
using namespace std;

#include "RodModule.h"
#include "RodStatus.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // VME map size 
  const long numSlaves=4;                       // Number of slaves
  ifstream *binFile;                            // Pointer to binary frile
  int fileSize;                                 // Size of binary file in bytes
  const unsigned long prmStart=0x01400000;

  std::string option;
  int slot;
  unsigned long baseAddress;
  std::string binFileName = "/det/pix/Binary/NewDSP/master.bin";
  std::string mask = "";

  // Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();

  if (argc > 1) {
    binFileName = argv[1];
  }
  if (argc > 2) {
    mask = argv[2];
  }
 
  for (slot = 5; slot < 22; slot++) {
    int pos = slot -5;
    if (slot != 13 && (mask=="" || mask[pos] == 'Y' || mask[pos] == 'y')) {
      baseAddress = slot << 24;  
      std::cout << std::dec << "======== SLOT " << slot << std::endl;

      // Check if the ROD is responding
      RodModule* rod0=nullptr;
      try {
	// Create RodModule and initialize it
	rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
	RodStatus *rodStatus = new RodStatus(*rod0);
	if (rodStatus) {
	  if (!rodStatus->isSane()) {
	    // ROD not responding, skip it
	    std::cout << "======== ROD in slot " << slot << " does not respond; skip it" << std::endl;
	    continue;
	  }
	}
	delete rodStatus;
      }
      catch (...) {
	// ROD not responding, skip it
	std::cout << "======== ROD in slot " << slot << " does not respond; skip it" << std::endl;
        continue;
      }

      // Flash the ROD
      try {
	//unsigned long int fpgaHold = 0x40;
	// JCH 19 July 2005. Also set the boot mode to HPI, else the corrupt MDSP
	// will hang the RODBUS and prevent the flash from loading.
	unsigned long int mdspBootMode = 0x1;
	unsigned long int rodReset = 0x40;
	unsigned long int mdspResetStatus = 0x0;
	
	// JMJ 25 July 2005.  Modified front end reset algorithm to monitor 
	// MDSP Reset Status Bit before proceeding with Flash load.
	
	// Reset MDSP in HPI boot mode.
	cerr << "Reset MDSP in HPI Boot Mode\n";
	cout << std::hex << "writing at " << FPGA_CONTROL_REG_REL_ADDR[2] 
	     << " value " << mdspBootMode << std::endl;
	cout << std::hex << "writing at " << FPGA_CONTROL_REG_REL_ADDR[2] 
	     << " value " << (rodReset|mdspBootMode)<< std::endl;
	rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], mdspBootMode);
	rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], (rodReset|mdspBootMode));
	
	// Poll on the MDSP Reset Status bit.  Fetch the status word and mask
	while (mdspResetStatus !=  0x2) {
	  mdspResetStatus = rod0->hpiFetch(FPGA_STATUS_REG_REL_ADDR[2]) & 0x2;
	}
	
	// Read HPIC to see if Host Port is ready
	cerr << "Initialise the MDSP Host Port" << endl;
	unsigned long hpicValue = 0x0;
	hpicValue = rod0->hpiFetch(HPIC);
	if (hpicValue != 0x80008) {
	  cerr << "MDSP Host Port is not ready, exiting Flash Load Program" << endl;
	  cerr << "Try running this program again" << endl;	
	  exit(0);
	}
	
	// Initialize HPIC
	hpicValue = 0x00010001;
	rod0->hpiLoad(HPIC, hpicValue);

	cerr << "Write MDSP Flash Parameters to EMIF Control Register\n";
	rod0->mdspSingleWrite(0x01800004, 0xffff3f03);
	rod0->mdspSingleWrite(0x01800010, 0xffff3f33);
	rod0->mdspSingleWrite(0x01800014, 0xffff3f33);
	
	cerr << "Read from EMIF Control Registers\n";
	
	// mdspSingleRead/Write use HPID not HPID++ (not allowed) NOT TRUE
	// Block Writes to the HPI are NOT ALLOWED, block reads ARE allowed
	
	for(unsigned long addr = 0x01800000; addr < 0x0180000c; addr+=4) {
	  unsigned long val = rod0->mdspSingleRead(addr);
	  cerr << "0x" << hex << addr << ": 0x" << val << dec << endl;
	  if ((addr == 0x01800000 && val != 0x3779) ||
	      (addr == 0x01800004 && val != 0xffff3f03) ||
	      (addr == 0x01800008 && val != 0xffff3f23)){
	    cout << "EMIF Values Corrupt - Exiting MDSP Flash Load!!!" << endl;
	    cout << "Try running this program again" << endl;
	    exit(0);
	  }
	} 
	for(unsigned long addr = 0x01800010; addr < 0x01800018; addr+=4) {
	  unsigned long val = rod0->mdspSingleRead(addr);
	  cerr << "0x" << hex << addr << ": 0x" << val << dec << endl;
	  if (val != 0xffff3f33) {
	    cout << "EMIF Values Corrupt - Exiting MDSP Flash Load!!!" << endl;
	    cout << "Try running this program again" << endl;
	    exit(0);
	  }
	} 
      }
      catch (BaseException &b) {
	cout << "Exception \"initialising\" ROD:\n" << b << endl;
	exit(0);
      }
      
      binFile = new ifstream(binFileName.c_str(), ios::binary);
      if (!binFile->is_open()) {
	cout << "Unable to open binary file." << endl;
	exit(1);
      }
  
      // Get size of file
      binFile->seekg(0, ios::end);          // go to end of file
      fileSize = binFile->tellg();          // file size given by current location
      binFile->seekg(0, ios::beg);          // go back to beginning of file
      
      // Create a buffer and read file into itprmSize = fileSize
      
      uint8_t * buffer;
      try {
	buffer = new uint8_t[fileSize];
      }
      catch (std::bad_alloc & ba) {
	cout << "Unable to allocate buffer for binary file." << endl;
	exit(2);
      }
      binFile->read((char *)buffer, fileSize);
      
      cout << "Read file of size " << fileSize << endl;
      
      // write buffer to MDSP flash memory
      try {
	rod0->writeBlockToFlashHpi(prmStart, buffer, fileSize);
	cout << fileSize << " bytes written to the MDSP flash memory"<< endl << endl;
      } catch (VmeException &v) {
	cout << "VmeException creating Writing flash." << endl;
	cout << "ErrorClass = " << v.getErrorClass() << endl;
	cout << "ErrorCode = " << v.getErrorCode() << endl; 
      } catch (RodException &r) {
	cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
	     << '\n';
      }
      
      // Clean up before exiting
      delete [] buffer;
      delete binFile;
      
      // Reset the ROD
      // Create RodModule and initialize it
      sleep(2);
      delete rod0;
      rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
      try {
	unsigned long int rodReset = 0x40;
	unsigned long int mdspResetStatus = 0x0;
	
	rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);
	
	// Poll on the MDSP Reset Status bit.  Fetch the status word and mask
	while (mdspResetStatus !=  0x2) {
	  mdspResetStatus = rod0->hpiFetch(FPGA_STATUS_REG_REL_ADDR[2]) & 0x2;
	}
	
	// Initialize HPIC
	unsigned long hpicValue = 0x00010001;
	rod0->hpiLoad(HPIC, hpicValue);
	
      } catch (BaseException &b) {
	cout << "Exception \"initialising\" ROD:\n" << b << endl;
	exit(0);
      }

      // Dump the ROD status
      sleep(2);
      delete rod0;
      rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
      RodStatus* rodStatus = new RodStatus(*rod0);
      if (rodStatus) {
	if (!rodStatus->isSane()) {
	  // ROD not responding anymore
	  std::cout << "======== ROD in slot " << slot << " does not respond anumore" << std::endl;
          break;
	}
        rod0->initialize(false);
	rodStatus->snapShot(*rod0);
	std::cout << *rodStatus;
      }
      delete rodStatus;

      delete rod0;
    }
  }

  delete vme1;

  return 0;  
}


