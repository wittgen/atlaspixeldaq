#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"
#include <ipc/partition.h>
#include <ipc/core.h>
#include <is/infodictionary.h>
#include <is/infoT.h>

using namespace std;
using namespace SctPixelRod;

void publishIS(ISInfoDictionary* infoDict, std::string name, int value);
int readIS(ISInfoDictionary* infoDict, std::string name);

int main(int argc, char **argv) {
  std::string ipcPartitionName;
  std::string isServerName;
  std::string isTimName;

  if(argc < 4){
    std::cout<<"Will use default values for reading/publishing FFTV variables in IS" << std::endl;
    std::cout<<"otherwise use the following syntax: "<<std::endl;
    std::cout<<"./ClearFFTVEmergencyBUSY [ParitionName] [ISserverName] [TIMname]"<<std::endl;
    if(getenv("PIX_PART_INFR_NAME")) ipcPartitionName = getenv("PIX_PART_INFR_NAME");
    else if(getenv("TDAQ_PARTITION_INFRASTRUCTURE")) ipcPartitionName = getenv("TDAQ_PARTITION_INFRASTRUCTURE");
    isServerName = "RunParams";
    isTimName = "TIM_I1";
  } else if (argc > 4){
    std::cout<< "Too many arguments provided, exiting..."<<std::endl;
    return -1;
  } else {
    ipcPartitionName = argv[1];
    isServerName = argv[2];
    isTimName = argv[3];
  }
  std::cout<<"Will read/publish FFTV variables from/into "<<std::endl;
  std::cout<< ipcPartitionName<< "[ParitionName], "<< isServerName << "[ISserverName], "<< isTimName <<" [TIMname]"<<std::endl;


  IPCCore::init(argc, argv);
  IPCPartition* m_ipcPartition = new IPCPartition(ipcPartitionName.c_str());
  ISInfoDictionary* m_infoDictionary = new ISInfoDictionary(*m_ipcPartition);

  std::string varnameFFTVstate = isServerName + std::string(".") + isTimName + std::string("/FFTVemergencyState");
  std::string varnameFFTVproc  = isServerName + std::string(".") + isTimName + std::string("/FFTVdumpProcessing");
  int conf;
  while (readIS(m_infoDictionary,varnameFFTVproc)==1) {
    std::cout<<"FFTV emergency trigger dump is still being analyzed O(~1 min), do you really want to clear BUSY ? (1=yes / 0=check again) "<<std::endl;
    std::cin >> conf;
    if (conf==1) break;
  }

  VmeInterface *vme(NULL);
  try {
    vme = new RCCVmeInterface();
  }
  catch (VmeException &v) {
    std::cout << "VmeException:" << std::endl;
    std::cout << "  ErrorClass = " << v.getErrorClass();
    std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
  }
  catch (BaseException & exc){
    std::cout << exc << std::endl; 
    exit(1);
  }
  catch (...){
    cout << "error during the pre-initialing of the application, exiting" << endl;
  }


  try{
    uint16_t timID, timfirm, timSN;
  
    const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
    const uint32_t  mapSize =    0x10000;  // VME window size
    TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim
    timID = tim->fetchTimID();
    timSN = (timID & 0xFF);
    timfirm = (timID >> 8);
    hex(cout);
    cout << "General TIM information (all dec unless specified):" << endl;
    cout << "TIM serial number (hex): " << hex <<timSN << endl;
    cout << "TIM firmware (hex): " << hex << timfirm << endl;
    dec(cout);

    //clear veto and veto busy counters
    //
    tim->regLoad(TIM_REG_FV_IDL,0x0000);
    tim->regLoad(TIM_REG_FV_COUNTL,0x0000);

    //clear bEmergency state 
    //
    tim->loadBitSet(TIM_REG_CONTROL, 0x0004);
    tim->loadBitClear(TIM_REG_CONTROL, 0x0004);

    //tell IS bEmergency cleared
    //
    publishIS(m_infoDictionary,varnameFFTVstate,0);

    std::cout << "FFTV BUSY cleared." << std::endl;
  }
  catch (BaseException & exc){
    cout << exc << endl;
    exit(1);
  }
  catch (...){
    cout << "error running the application, exiting" << endl;
    exit(1);
  }

  return 0;
}

void publishIS(ISInfoDictionary* infoDict, std::string name, int value)
{
  ISInfoInt val(value);
  if(infoDict->contains(name)){
    infoDict->update(name,val);
  }else{
    infoDict->insert(name,val);
  }
}

int readIS(ISInfoDictionary* infoDict, std::string name)
{
  ISInfoInt val;
  if(infoDict->contains(name)){
    infoDict->getValue(name,val);
  }else{
    val=-1;
  }
  return val;
}
