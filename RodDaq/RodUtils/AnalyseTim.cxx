// 
// Decode binary files containing dump of Tim registers, created by TimDump for Diagnostics
// 15.05.07 Iskander Ibragimov

#include <iostream>
#include <iomanip>
#include <fstream>


using namespace std;
void YesNo(unsigned short regstatus, int mode = 1);

int main(int argc, char *argv[]) {

  unsigned int numWords[3] = {0x20, 0x30, 0x4000};
  unsigned short temp;
  unsigned short seqEnd = 0; //Address of the last sequencer byte
  string filename[3]       = {"TIMRegister", "TIM3ExtraRegister", "TIMSequencer"};
  
  for (unsigned int index=0;index<3;index++) {
    unsigned int size = numWords[index];
    int j;
    unsigned short* buffer = new unsigned short[size];
    std::cout << "Analysing " << filename[index] << std::endl;
    std::ostringstream onam;
    onam << filename[index];
    onam << ".bin";

    std::ifstream fin(onam.str().c_str(),ios::binary);
    
    fin.read((char *)buffer, 2*size);
    for (unsigned int i=0; i<size;i++) {
      temp = buffer[i];
      //cout << hex <<"0x"<< temp << endl;
      if ((index==2) && ((i+1) == seqEnd )) { cout << "#            " << endl; break;}
      switch (index) {
      case 0:
	if (i==0) {
	  cout << "---------- Signal Enables Register-------------" << endl;
	  cout << "internal repetitive ECReset \t"; YesNo(temp>>=1, 1);
	  cout << "internal repetitive BCReset \t"; YesNo(temp>>=1, 1);
	  cout << "internal trigger randomizer \t"; YesNo(temp>>=1, 1);
	  cout << "internal repetitive FEReset \t"; YesNo(temp>>=1, 1);
	  cout << "trigger window \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "internal Busy \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "external clock inputs \t\t"; YesNo(temp>>=1, 1);
	  cout << "external trigger inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external ECReset inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external BCReset inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external Calibrate inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external FEReset inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external Sequencer Go inputs \t"; YesNo(temp>>=1, 1);
	  cout << "external Busy inputs \t\t"; YesNo(temp>>=1, 1);
	}
	else if (i==1) {
	  cout << "---------- Command & Mode Register -------------" << endl;
	  cout << "Single trigger \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Single ECReset \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Single BCReset \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Single Calibrate strobe \t"; YesNo(temp>>=1, 2);
	  cout << "Single FEReset \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Single Spare command \t\t"; YesNo(temp>>=1, 2);
	  cout << "Set Busy \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Set RodBusy \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Set BURST mode - disable triggers \t"; YesNo(temp>>=1, 2);
	  cout << "Start BURST triggers - Int or Ext \t"; YesNo(temp>>=1, 2);
	  
	  cout << "Enable Run Mode \t\t"; YesNo(temp>>=2, 2);
	  cout << "Enable Busy set by next trigger (TestBusy) \t"; YesNo(temp>>=1, 2);
	  cout << "Clear TestBusy \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Overall TIM RESET \t\t"; YesNo(temp>>=1, 2);
	}
	else if (i==2) {
	  cout << "---------- Burst Count Register -------------" << endl;
	  cout << "Number of triggers in BURST \t" << temp << endl;  
	}
	else if (i==3) {
	  cout << "---------- Frequency Select Register -------------" << endl;
	  cout << "Internal trigger look-up table \t" << (temp&0x1f) << endl;
	  cout << "Internal ECR/FER look-up table \t" << (temp>>8 & 0x1f) << endl;
	}  
	else if (i==4) {
	  cout << "---------- Trigger Window Register  -------------" << endl;
	  cout << "Trigger window size (0.5ns steps) \t" << (temp&0x3f) << endl;
	  cout << "Trigger window delay (0.5ns steps) \t" << (temp>>8 & 0x3f) << endl;
	} 
	else if (i==5) {
	  cout << "---------- Delay Register  -------------" << endl;
	  cout << "Stand-alone trigger pipeline delay (clock steps) \t" << (temp&0xff) << endl;
	  cout << "Stand-alone clock delay (0.5ns steps) \t\t\t" << (temp>>8 & 0xff) << endl;
	}
	else if (i==6) {
	  cout << "---------- Status Register-------------" << endl;
	  cout << "Front-panel Busy Input (post-enable) \t"; YesNo(temp, 1);
	  cout << "Masked ExtBusy Output \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "TIM stopping triggers internally \t"; YesNo(temp>>=1, 1);
	  cout << "Front-panel Busy Output \t\t"; YesNo(temp>>=1, 1);
	  cout << "Burst is active (e.g. running: post-go, pre-done) \t"; YesNo(temp>>=1, 1);
	  cout << "Sequencer Source Running \t\t"; YesNo(temp>>=1, 1);
	  cout << "Sequencer Sink Running \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "Front-panel RodBusy Output \t\t"; YesNo(temp>>=1, 1);
	  cout << "TTC system clock present \t\t"; YesNo(temp>>=1, 1);
	  cout << "Stand-alone clock present \t\t"; YesNo(temp>>=1, 1);
	  cout << "TTC system operation \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "Stand-alone operation \t\t\t"; YesNo(temp>>=1, 1);
	  cout << "Selected Clock is running \t\t"; YesNo(temp>>=2, 1);
	  cout << "Test-Busy Triggered \t\t\t"; YesNo(temp>>=1, 1);
	}
	else if (i==7) {
	  cout << "---------- FIFO Status Register -------------" << endl;
	  cout << "Event ID FIFO Count \t" << (temp&0x3f) << endl;
	  cout << "Event ID Empty Flag \t"; YesNo(temp>>=6, 2);
	  cout << "Event ID Full Flag \t"; YesNo(temp>>=1, 2);
	  cout << "Trigger Type FIFO Count " << (temp>>=1 & 0x3f) << endl;
	  cout << "Trigger Type Empty Flag "; YesNo(temp>>=6, 2);
	  cout << "Trigger Type Full Flag \t"; YesNo(temp>>=1, 2);
	}
	else if (i==8) {
	  cout << "---------- Trigger ID Lo Register  -------------" << endl;
	  cout << "Last trigger number (bits 0-15) \t" << (temp&0xffff) << endl;
	} 
	else if (i==9) {
	  cout << "---------- Trigger ID Hi Register  -------------" << endl;
	  cout << "Last trigger number (bits 16-23) \t" << (temp&0x00ff) << endl;
	  cout << "ECR counter \t" << (temp>>8 & 0xff) << endl;
	}
	else if (i==10) {
	  cout << "---------- Trigger BCID Register  -------------" << endl;
	  cout << "Bunch Crossing number of last trigger \t" << (temp&0x0fff) << endl;
	  cout << "BCID offset \t" << (temp>>12 & 0xf) << endl;
	}
	else if (i==11) {
	  cout << "---------- Trigger Type ID Register  -------------" << endl;
	  cout << "TTC Trigger Type \t" << (temp&0x00ff) << endl;
	  cout << "TIM Trigger Type \t" << (temp>>8 & 0x0f) << endl;
	}
	else if (i==12) {
	  cout << "---------- Run Enables Register-------------" << endl;
	  cout << "Enable TTC clock \t"; YesNo(temp, 1);
	  cout << "Enable TTC L1Accept Trigger \t"; YesNo(temp>>=1, 1);
	  cout << "Enable TTC ECReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable TTC BCReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable TTC Calibrate Strobe \t"; YesNo(temp>>=1, 1);
	  cout << "Enable TTC FEReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable TTC Spare command \t"; YesNo(temp>>=1, 1);
	  cout << "Enable RodBusy into Busyout \t"; YesNo(temp>>=1, 1);
	  cout << "Enable external RodBusy input \t"; YesNo(temp>>=1, 1);
	  cout << "Enable trigger & bunch ID numbers \t"; YesNo(temp>>=1, 1);
	  cout << "Enable trigger type \t"; YesNo(temp>>=1, 1);
	  cout << "Enable counter ECR \t"; YesNo(temp>>=4, 1);
	  cout << "Enable counter BCR \t"; YesNo(temp>>=1, 1);
	}
	else if (i==13) {
	  cout << "---------- Sequencer Control Register-------------" << endl;
	  cout << "Enable L1Accept trigger \t"; YesNo(temp, 1);
	  cout << "Enable ECReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable BCReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable Calibrate strobe \t"; YesNo(temp>>=1, 1);
	  cout << "Enable SerialID \t"; YesNo(temp>>=1, 1);
	  cout << "Enable SerialTT \t"; YesNo(temp>>=1, 1);
	  cout << "Enable FEReset \t"; YesNo(temp>>=1, 1);
	  cout << "Enable Spare command \t"; YesNo(temp>>=1, 1);
	  cout << "Reset Sequencer \t"; YesNo(temp>>=2, 1);
	  cout << "Start Sequencer \t"; YesNo(temp>>=1, 2);
	  cout << "Enable cyclic operation \t"; YesNo(temp>>=1, 1);
	  cout << "Reset Sink \t"; YesNo(temp>>=2, 1);
	  cout << "Start Sink (and Source) \t"; YesNo(temp>>=1, 1);
	  cout << "Enable Sink trigger \t"; YesNo(temp>>=1, 1);
	}
	else if (i==14) {
	  cout << "---------- Sequencer End Register-------------" << endl;
	  seqEnd = temp & 0x3fff;
	  cout << "End/Recycle address \t" << hex << seqEnd << endl;
	}
	else if (i==15) {
	  cout << "---------- Rod Busy Mask Register-------------" << endl;
	  cout << "RodBusy mask \t" << hex << temp << endl;
	}
	else if (i==16) {
	  cout << "---------- Rod Busy Status Register-------------" << endl;
	  cout << "RodBusy status \t" << hex << temp << endl;
	}
	else if (i==17) {
	  cout << "---------- Rod Busy Latch Register-------------" << endl;
	  cout << "RodBusy latch \t" << hex << temp << endl;
	}
	else if (i==18) {
	  cout << "---------- Rod Busy Monitor Register-------------" << endl;
	  cout << "RodBusy monitor \t" << hex << temp << endl;
	}
	else if (i==19) {
	  cout << "---------- TTC Data Register  -------------" << endl;
	  cout << "Long-format data \t" << (temp&0x00ff) << endl;
	  cout << "Sub-address \t" << (temp>>8 & 0xff) << endl;
	}
	else if (i==20) {
	  cout << "---------- TTC Select Register  -------------" << endl;
	  cout << "Select long-format Data Qualifier \t" << (temp&0x000f) << endl;
	}
	else if (i==21) {
	  cout << "---------- TTC BCID Register  -------------" << endl;
	  cout << "TTC bunch number \t" << (temp&0x0fff) << endl;
	}
	else if (i==22) {
	  cout << "---------- TTCrx Control Register  -------------" << endl;
	  cout << "TTCrx data (read or write) \t" << (temp&0x000f) << endl;
	  cout << "TTCrx register number \t" << (temp>>4 & 0x1f) << endl;
	  cout << "I2C Read (not Write) \t"; YesNo(temp>>=13, 1);
	  cout << "T2: I2C clock enable/T3 R: Error Status, W: Abort \t"; YesNo(temp>>=1, 1);
	  cout << "R: I2C Busy / W: I2C Go \t"; YesNo(temp>>=1, 1);
	} 
	else if (i==23) {
	  cout << "---------- TTC Status Register -------------" << endl;
	  cout << "BCReset \t"; YesNo(temp, 2);
	  cout << "ECReset \t"; YesNo(temp>>=1, 2);
	  cout << "FEReset bit - system message \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast bit - system message \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast bit - system message \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast bit - system message \t"; YesNo(temp>>=1, 2);
	  cout << "Calibrate bit - user message \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast bit - user message \t"; YesNo(temp>>=1, 2);
	  cout << "Trigger \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast strobe - system message \t"; YesNo(temp>>=1, 2);
	  cout << "Broadcast strobe - user message \t"; YesNo(temp>>=1, 2);
	  cout << "Long-format data strobe \t"; YesNo(temp>>=1, 2);
	  cout << "Double error or frame error occured \t"; YesNo(temp>>=1, 0);
	  cout << "Single error occured \t"; YesNo(temp>>=1, 0);
	  cout << "TTCrx operating correctly(see Status3 Reg) \t"; YesNo(temp>>=1, 0);
	}
	else if (i==24) {
	  cout << "---------- TTC Status Register -------------" << endl;
	  cout << "L1Accept trigger \t"; YesNo(temp, 2);
	  cout << "ECReset \t"; YesNo(temp>>=1, 2);
	  cout << "BCReset \t"; YesNo(temp>>=1, 2);
	  cout << "Calibrate strobe \t"; YesNo(temp>>=1, 2);
	  cout << "SerialID \t"; YesNo(temp>>=1, 2);
	  cout << "SerialTT \t"; YesNo(temp>>=1, 2);
	  cout << "FEReset \t"; YesNo(temp>>=1, 2);
	  cout << "Spare command \t"; YesNo(temp>>=1, 2);
	}
	else if (i==25) {
	  cout << "---------- TIM ID Register   -------------" << endl;
	  cout << "Serial number \t" << (temp & 0x00ff) << endl;
	  cout << "Version number \t" << (temp>>8 & 0x0ff) << endl;
	}
	break;
      case 1:
	if (i==1) {
	  cout << "---------- Enables3 Register -------------" << endl;
	  cout << "Enable L1ID rollover to increment ECR-ID \t"; YesNo(temp>>=4, 0);
	  cout << "Enable output QPLL lines to TTCrq (see QPLL_Ctl Reg) \t"; YesNo(temp>>=1, 0);
	  cout << "Enable Randomiser 2 (see Control Reg) \t"; YesNo(temp>>=1, 0);
	  cout << "Enables Trigger Oscillator 2 (see TrigOsc2) \t"; YesNo(temp>>=1, 0);
	  cout << "Stand-alone trigs set to generate SeqGo (only) \t"; YesNo(temp>>=1, 2);
	  cout << "Sets Front-panel L1A,ECR,BCR,CAL,FER & Spare signals pulse width to 25ns \t"; YesNo(temp>>=1, 0);
	  cout << "Burst counts triggers prior to busy action \t"; YesNo(temp>>=1, 2);
	  cout << "Enable TTCrx L1A into SA Mode trigger stream (incl. busy + deadtime) \t"; YesNo(temp>>=1, 0);
	  cout << "Enable TTCrx TType in SA Mode (only if using SA TTC L1A) \t"; YesNo(temp>>=1, 0);
	}
	else if (i==3) {
	  cout << "---------- Control Register -------------" << endl;
	  cout << "Clear an FFTV emergency state \t"; YesNo(temp>>=3, 2);
	  cout << "Average frequency of Randomiser 2 \t" << ((temp>>=1) & 0x001f) << endl;
	  cout << "Randomiser 2 Clock divider for larger bunch spacing tests \t" << ((temp>>=5) & 0x7f) << endl;
	}
	else if (i==5) {
	  cout << "---------- Status3 Register -------------" << endl;
	  cout << "TTCrx operating correctly \t\t\t"; YesNo(temp, 0);
	  cout << "TTCrq/QPLL chip present \t\t\t"; YesNo(temp>>=1, 0);
	  cout << "QPLL Chip Error \t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "QPLL Locked \t\t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "TTC clock is present \t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "SA clock is present \t\t\t\t"; YesNo(temp>>=2, 0);
	  cout << "TTC Clock Selected and Running ('TT' LED) \t"; YesNo(temp>>=1, 0);
	  cout << "Ext Clock Selected and Running \t\t\t"; YesNo(temp>>=1, 0);
	  cout << "Internal Clock Selected and Running \t\t"; YesNo(temp>>=1, 0);
	  cout << "Clock PLL(s) stable after switching clocks \t"; YesNo(temp>>=1, 0);
	  cout << "Spare Input \t\t\t\t\t"; YesNo(temp>>=4, 2);
	  cout << "Spare Link \t\t\t\t\t"; YesNo(temp>>=1, 2);
	}
	else if (i==6) {
	  cout << "---------- Status Change Latch Register -------------" << endl;
	  cout << "Latches changes in Status Register (6) since last write  \t" << hex << temp << endl;
	}
	else if (i==7) {
	  cout << "---------- Status3 Change Latch Register -------------" << endl;
	  cout << "Latches changes in Status3 since last write  \t" << hex << temp << endl;
	}
	else if (i==9) {
	  cout << "---------- QPLL Control Register -------------" << endl;
	  cout << "QPLL f0 Select (bits 3:0)  \t" << hex << (temp&0xf) << endl;
	  cout << "QPLL Auto-restart/f0 sel bit 4 \t"; YesNo(temp>>=4, 2);
	  cout << "QPLL Reset/f0 Select bit 5 \t"; YesNo(temp>>=1, 2);
	  cout << "QPLL External Control Enable \t"; YesNo(temp>>=1, 2);
	  cout << "QPLL Mode \t\t\t"; YesNo(temp>>=1, 2);
	}
	else if (i==11) {
	  cout << "---------- Busy Enable3 Register -------------" << endl;
	  cout << "Enable RodBusy into Busy \t\t"; YesNo(temp, 0);
	  cout << "Enable External-RodBusy into Busy \t"; YesNo(temp>>=1, 0);
	  cout << "Enable VME-RodBusy into Busy \t\t"; YesNo(temp>>=1, 0);
	  cout << "Enable External-Busy into Busy \t\t"; YesNo(temp>>=1, 0);
	  cout << "Enable VME-Busy into Busy \t\t"; YesNo(temp>>=1, 0);
	  cout << "Enable RodBusy into Busyout \t\t"; YesNo(temp>>=4, 0);
	  cout << "Enable External-RodBusy into Busyout \t"; YesNo(temp>>=1, 0);
	  cout << "Enable VME-RodBusy into Busyout \t"; YesNo(temp>>=1, 0);
	  cout << "Enable External-Busy into Busyout \t"; YesNo(temp>>=1, 0);
	  cout << "Enable VME-Busy into Busyout \t\t"; YesNo(temp>>=1, 0);
	  cout << "Enable Burst-Busy into Busyout \t\t"; YesNo(temp>>=4, 2);
	  cout << "Enable Test-Busy into Busyout \t\t"; YesNo(temp>>=1, 2);
	  cout << "Enable Deadtime-Busy into Busyout \t"; YesNo(temp>>=1, 2);
	}
	else if (i==13) {
	  cout << "---------- Busy Status3 Register -------------" << endl;
	  cout << "RB: OR of all RodBusy backplane inputs (post masking) \t"; YesNo(temp, 0);
	  cout << "XRB: External RodBusy (post-enable) \t\t\t"; YesNo(temp>>=1, 0);
	  cout << "VRB: VME-RodBusy \t\t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "XB: Front Panel Busy Input (post-enable) \t\t"; YesNo(temp>>=1, 0);
	  cout << "VB: VME-Busy \t\t\t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "BSTB: Burst Ready - awaiting Burst-Go/Burst Done \t"; YesNo(temp>>=1, 0);
	  cout << "TSTB: Test-Busy Triggered \t\t\t\t"; YesNo(temp>>=1, 0);
	  cout << "DTB: Stand-Alone Signal Dead-Time \t\t\t"; YesNo(temp>>=1, 0);
	  cout << "Timeout after clock-switch/reset to ensure downstream stable \t"; YesNo(temp>>=1, 0);
	  cout << "TIM stopping triggers internally \t\t\t"; YesNo(temp>>=1, 0);
	  cout << "Front-panel RodBusy Output \t\t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Front-panel Busy Output \t\t\t\t"; YesNo(temp>>=1, 2);
	  cout << "Front-panel Masked ExtBusy Output \t\t\t"; YesNo(temp>>=1, 2);
	  cout << "FFTV System In Emergency State, Triggers Stopped \t"; YesNo(temp>>=1, 2);
	  cout << "Fixed Frequency Trigger Veto Busy Asserted \t\t"; YesNo(temp>>=1, 2);
	  cout << "FFTV Disable Link Inserted \t\t\t\t"; YesNo(temp>>=1, 2);
	  //cout << endl << "The rest is not yet implemented!" << endl;
	}
	else if (i==15) {
	  cout << "---------- Busy Status3 Change Latch Register -------------" << endl;
	  cout << "Catches changes in BSTAT3 since last reset/clear(hex)  \t" << hex << temp << endl;
	}
	else if (i==16) {
	  cout << "---------- Overall Busy Count (3 words) -------------" << endl;
	  cout << "Number of clocks RodBusy is asserted(dec)  \t" << dec << (unsigned long)(temp + (buffer[i+1]<<16) + ((unsigned long)buffer[i+2]<<32)) << endl;
	}
	else if (i==21) {
	  cout << "---------- FFTV Match Threshhold Register -------------" << endl;
	  cout << "Number of Matching Periods needed to Generate Veto(dec)  \t" << dec << (temp & 0xf) << endl;
	}
	else if (i==26) {
	  cout << "---------- FFTV Count Register (3 words) -------------" << endl;
	  cout << "Number of Clocks in FFTVeto Counter(dec)  \t" << dec << (unsigned long)(temp + (buffer[i+1]<<16) + ((unsigned long)buffer[i+2]<<32)) << endl;
	}
	else if (i==30) {
	  cout << "---------- FFTV Trigger Count Register (2 words) -------------" << endl;
	  cout << "Number of Triggers FFTVeto'd(dec)  \t" << dec << temp + (buffer[i+1]<<16) << endl;
	}
	else if (i==32) {
	  cout << "---------- Trigger Period FIFO Register (2 words) -------------" << endl;
	  cout << "Trigger Period FIFO(dec)  \t" << dec << temp + (buffer[i+1]<<16) << endl;
	}
	else if (i==34) {
	  cout << "---------- Veto ID Register (2 words) -------------" << endl;
	  cout << "Counts Number of times Veto is Asserted(dec)  \t" << dec << temp + (buffer[i+1]<<16) << endl;
	}
	else if (i==36) {
	  cout << "---------- Trigger Osc2 Period Register (2 words) -------------" << endl;
	  cout << "Counter-based Trigger Oscillator(dec)  \t" << dec << temp + (buffer[i+1]<<16) << endl;
	}
	else if (i==39) {
	  cout << "---------- Burst Count Hi Register -------------" << endl;
	  cout << "Burst Count Hi Register(hex)  \t" << hex << temp << endl;
	}
	else if (i==42) {
	  cout << "---------- F2 Timestamp Register (2 words) -------------" << endl;
	  cout << "F2 Timestamp(dec)  \t" << dec << temp + (buffer[i+1]<<16) << endl;
	}
	else if (i==46) {
	  cout << "---------- F2 Debug Control Register -------------" << endl;
	  cout << "Disable Clk-Switch Busy \t\t\t"; YesNo(temp>>=8, 0);
	  cout << "Disable SA-Mode setting RodBusy \t\t"; YesNo(temp>>=1, 0);
	  cout << "FFTV Veto Disable (needs link in place too) \t"; YesNo(temp>>=3, 0);
	}
	else if (i==47) {
	  cout << "---------- F2 Debug Status Register -------------" << endl;
	  cout << "Nothing implemented in firmware so far(hex)  \t" << hex << temp << endl;
	}
	break;
      case 2:
	if (seqEnd == 0) cout << "Sequencer memory is empty..." << endl;
	else {
	  if (i==0) {
	    cout << "# SFTI CBEL i" << endl;
	    cout << "# PETD ACC1 n" << endl;
	    cout << "# RR   LRRA d" << endl;
	    cout << "#           e" << endl;
	    cout << "#           x" << endl;
	    cout << "#            " << endl;
	  }
	  cout << "  ";
	  for(j=7;j>=0;j--) {
	    YesNo(temp>>j,3);
	    if (j==4) cout << " ";	
	  }	      
	  cout <<" "<<dec<< i << endl;
	}
	break;
      }
    } 
    cout << endl;
    fin.close();
    delete[] buffer;
  }
  return 0;  
}

void YesNo(unsigned short regstatus, int mode)
{
  if (regstatus & 0x0001) {
    if (mode==0) cout << "Yes"<< endl;
    if (mode==1) cout << "Enabled"<< endl;
    if (mode==2) cout << "Set"<< endl;
    if (mode==3) cout << "1";
  }
  else {
    if (mode==0) cout << "No"<< endl;
    if (mode==1) cout << "Disabled" << endl;
    if (mode==2) cout << "Not set"<< endl;
    if (mode==3) cout << "0";
  }
}
