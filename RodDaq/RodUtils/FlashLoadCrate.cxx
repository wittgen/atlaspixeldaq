//--------------------------FlashLoadCrate-----------------------------
/*!
 * @brief Update multiple RODs with flash contents.
 * Loop over slots to locate and identify RODs, then update the RODs
 * that are requested by the user with the image required.
 * Assume only Rev E/Rev F RODs are to be handled by this program.
 * All 4 FPGA and MDSP flashes can be updated by this procedure.
 * JCH. 21 February 2005.
 *
 * @author John Hill (hill@hep.phy.cam.ac.uk)
 */

#include <iostream>

using namespace std;

#include "RodModule.h"

//#include "primParams.h"
#include "RCCVmeInterface.h"
#include "parameters.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  unsigned long serialNumber[22], masked1, masked2, rodRevision[22];
  VmePort *myVmePort;
  
  std::string binFileName("");
  ifstream binFile;
  unsigned long selectAddr;
  unsigned long flashAddr;
  int fileSize;
  int selectSize;
  int iselect = 0;
  int safeMode = false;

  const unsigned long flashStartE[5]={0x1400000, 0xe00000, 0xe80000, 0xf00000, 0xf80000};
  const long flashSizeE[5] = {0, 495204, 495212, 336680, 336680};

  std::string flashName[5] = {"Master DSP", "ROD Controller", "Formatter",
       "Event Fragment Builder", "Router"};

  int slot = -1;
  int numRods = 0;
  int rodUpdate[22];
  std::string option;
  unsigned long baseAddress;
//
// Check for safe mode option being requested for MDSP loading.
//

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'f': {
          safeMode = true;
          break;
        }
        default: {
          break;
        }
      }
    }
  }

//
// Initialise some arrays.
//
  for(int i=0;i<22;i++) {
    rodUpdate[i] = 0;
    rodRevision[i] = 0;
    serialNumber[i] = 0;
  }

// Create VME interface  - this remains until the end of the program.
//
  RCCVmeInterface *vme1 = new RCCVmeInterface();

//
// Loop over RODs, identifying Rev E/F RODs for flashing.
//
  for(slot=5; slot<=21; slot++) {
   if(slot != 13) {  //Skip the TIM in slot 13.
    baseAddress = slot << 24;

// Create RodModule and initialize it
    RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
    myVmePort = rod0->getVmePort();

    try {
// Read the FPGA Status Register 6 (counting from 0)
      serialNumber[slot] = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
      masked1 = serialNumber[slot]&0xFF000000;
      masked2 = serialNumber[slot]&0x000000FF;
      if (0xad000000 == masked1) {
// Get the revision level and serial number of the ROD.
        rodRevision[slot] = (serialNumber[slot]&0x00FF0000)>>16;
        serialNumber[slot] = serialNumber[slot]&0x3FF;
//If this is a Rev E or Rev F, mark the ROD for updating.
	if ((rodRevision[slot] == 0xE) || (rodRevision[slot] == 0xF)) {
          numRods++;
	  rodUpdate[slot] = 1;
        }
      }
      else if (0x000000ad == masked2) {
        cout << "Byte order wrong, ROD in slot: " << slot << endl;
      }
      else {
        cout << "Sanity check failed, ROD in slot: " << slot << endl;
      }
    } catch(BaseException &b) {
      cout << "Exception touching ROD in slot:\n" << slot << b << endl;
    }

// Delete the ROD object - avoids having up to 16 of these created at once.
    delete rod0;
   }
  }    //end of loop over slot
//
// Report our findings - list the RODs to be updated, and those which will not.
//
  cout << endl;
  cout << "Rev E/F RODs in slots: ";
  for (int i=5;i<=21;i++) {
    if (rodUpdate[i] == 1) {
      cout << i << " ";
    }
  }
  cout << " will be updated" << endl;
//
  cout << "Other revision RODs in slots: ";
  for (int i=5;i<=21;i++) {
    if ((i!=13) && (rodUpdate[i] == 0)) {
      cout << i << " ";
    }
  }
  cout << " will NOT be updated" << endl << endl;
//
// Now do the update on all RevE and F boards.
//
// Do MDSP flash first, and keep separate from the loop over VHDL flash, as
// the details are different.
//
// Check whether we do want to update the MDSP flash.
    option[0] = 'n';
    cout << "==>Updating " << flashName[iselect] << " flash<==" << endl;
    cout << "Do you wish to update this flash (Y/n)? " <<endl;
    getline(cin,option);
    if ((option[0] == 'y') || (option[0] == 'Y') || (option.size() == 0)){
      cout << "Flash will be updated" << endl;
      selectAddr = flashStartE[iselect];
      selectSize = flashSizeE[iselect];
//
// Get the file information
//
    cout << "Enter binary file name, including extension: ";
    getline(cin,binFileName);

    binFile.open(binFileName.c_str(), ios::binary);
    if (!binFile.is_open()) {
      cout << "Unable to open binary file." << endl;
      exit(1);
    }

// Get size of file
    binFile.seekg(0, ios::end);           // go to end of file
    fileSize = binFile.tellg();          // file size given by current location
    binFile.seekg(0, ios::beg);          // go back to beginning of file

// Create a buffer and read file into itprmSize = fileSize

    uint8_t * buffer;
    try {
      buffer = new uint8_t[fileSize];
    }
    catch (std::bad_alloc & ba) {
      cout << "Unable to allocate buffer for binary file." << endl;
      exit(2);
    }
    binFile.read((char *)buffer, fileSize);

    cout << "Read file of size " << fileSize << endl;
//
    option[0] = 'n';
    cout << flashName[iselect] << " flash will be updated with file "
         << binFileName << endl;
    cout << "Are you sure that you want to update this flash (y/n)? " <<endl;
    getline(cin,option);
    if ((option[0] == 'y') || (option[0] == 'Y')){
      cout << "Flash will be updated" << endl;
//
// Do we use safe mode or not??
//
      for(slot=5;slot<22;slot++) {
        if(rodUpdate[slot] == 1) {    //Only do this if this ROD is to be updated
          cout << "Updating ROD in slot " << slot << ",serial number "
            << (serialNumber[slot]&0xFF) << endl;
// Create RodModule and initialize it
          baseAddress = slot <<24;
          RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
          if(!safeMode) {
            try{
              rod0->initialize();
            }
            catch (HpiException &h) {
              cout << "HPI exception initialising ROD\n";
              hex(cout);
              cout << h.getDescriptor() << '\n';
              cout << "calcAddr: " << h.getCalcAddr() << ", readAddr: " <<
                h.getReadAddr() << '\n';
              dec(cout);

              cout << "Try the \"safe\" mode (-f)\n";
              return 1;
            }
            catch (BaseException &b) {
              cout << "Other exception initialising ROD\n";
              cout << b << endl;

              cout << "Try the \"safe\" mode (-f)\n";
              return 1;
            }
          } else {   // Safe mode (doesn't try to talk to MDSP (for when the MDSP is corrupt))
            try {
              unsigned long int fpgaHold = 0x40;
              unsigned long int mdspReset = 0x2;

              cerr << "Put FPGAs on reset hold\n";
// Put FPGAs on hold
              rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[1], fpgaHold);
              rod0->sleep(1000);
// Reset MDSP
              rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], mdspReset);

              rod0->sleep(1000);

              unsigned long hpicValue = 0x00010001;
              rod0->hpiLoad(HPIC, hpicValue);

              rod0->sleep(1000);
// mdspSingleRead/Write use HPID not HPID++ (not allowed)

              cerr << "Read from EMIF (1)\n";
              for(unsigned long addr = 0x01800000; addr < 0x0180001c; addr+=4) {
                unsigned long val = rod0->mdspSingleRead(addr);
                cerr << "0x" << hex << addr << ": 0x" << val << dec << endl;
              }

              rod0->sleep(100);

              cerr << "Write CE space setup\n";
              rod0->mdspSingleWrite(0x01800004, 0xffff3f03);
              rod0->mdspSingleWrite(0x01800008, 0x00000040);
              rod0->mdspSingleWrite(0x01800010, 0xffff3f33);
              rod0->mdspSingleWrite(0x01800014, 0xffff3f33);

              rod0->sleep(500);

              cerr << "Read from EMIF (2)\n";

// mdspSingleRead/Write use HPID not HPID++ (not allowed)

              for(unsigned long addr = 0x01800000; addr < 0x0180001c; addr+=4) {
                unsigned long val = rod0->mdspSingleRead(addr);
                cerr << "0x" << hex << addr << ": 0x" << val << dec << endl;
              }
            }
            catch (BaseException &b) {
              cout << "Exception \"initing\" ROD:\n" << b << endl;
              exit(0);
            }

            rod0->sleep(3000);
          }

// write buffer to MDSP flash memory
          try {
            flashAddr = selectAddr;
            rod0->writeBlockToFlashHpi(flashAddr, buffer, fileSize);

            cout << fileSize << " bytes written to the MDSP flash memory"<< endl;
          } catch (VmeException &v) {
            cout << "VmeException creating Writing flash." << endl;
            cout << "ErrorClass = " << v.getErrorClass() << endl;
            cout << "ErrorCode = " << v.getErrorCode() << endl;
          } catch (RodException &r) {
            cout << r.getDescriptor() <<", " << r.getData1() << ", " 
               << r.getData2() << '\n';
          }

// Clean up before exiting
          delete rod0;
        }
      }       // end of loop over slot
    }         //confirmatory if!!
//Tidy up the buffer and close the file
  delete [] buffer;
  binFile.close();
  }
  cout << endl << endl;
//
// Loop over the flash to update in the outer loop (then the binary file need
// only be identified once).
//
  for(iselect=1;iselect<5;iselect++) {
    option[0] = 'n';
    cout << "==>Updating " << flashName[iselect] << " flash<==" << endl;
    cout << "Do you wish to update this flash (Y/n)? " <<endl;
    getline(cin,option);
    if ((option[0] == 'y') || (option[0] == 'Y') || (option.size() == 0)){
      cout << "Flash will be updated" << endl;
      selectAddr = flashStartE[iselect];
      selectSize = flashSizeE[iselect];
      cout << "Enter binary file name, including extension: ";
      getline(cin,binFileName);

      binFile.open(binFileName.c_str(), ios::binary);

      if (!binFile.is_open()) {
        cout << "Unable to open binary file." << endl;
      }
      else {

// Get size of file
        binFile.seekg(0, ios::end);      // go to end of file
        fileSize = binFile.tellg();      // file size given by current location
        binFile.seekg(0, ios::beg);      // go back to beginning of file
        if (fileSize != selectSize) {
        cout << "File size is incorrect. Expected: " << dec << selectSize 
        << " Found: " << dec << fileSize << endl;
        exit(3);
        }

// Create a buffer and read file into it
        uint8_t * buffer;
        try {
          buffer = new uint8_t[fileSize];
        }
        catch (std::bad_alloc & ba) {
          cout << "Unable to allocate buffer for binary file." << endl;
          exit(2);
        }
        binFile.read((char *)buffer, fileSize);
//
        option[0] = 'n';
        cout << flashName[iselect] << " flash will be updated with file "
           << binFileName << endl;
        cout << "Are you sure that you want to update this flash (y/n)? " <<endl;
        getline(cin,option);
        if ((option[0] == 'y') || (option[0] == 'Y')){
          cout << "Flash will be updated" << endl;
// write buffer to flash - loop over RODs at this point.
          for(slot=5;slot<22;slot++) {
            if(rodUpdate[slot] == 1) {      //Only if this ROD should be updated.
              cout << "Updating ROD in slot " << slot << ", serial number "
                << (serialNumber[slot]&0xFF) << endl;
// Create RodModule and initialize it
              baseAddress = slot << 24;
              RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
              try{
                rod0->initialize();
              }
              catch (HpiException *h) {
                hex(cout);
                cout << h->getDescriptor() << '\n';
                cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " <<
                  h->getReadAddr() << '\n';
                dec(cout);
              }

              try {
                flashAddr = selectAddr;
                rod0->writeBlockToFlash(flashAddr, buffer, fileSize);

                cout << dec << selectSize << " bytes written to the "
                  << flashName[iselect] << " flash memory" << endl;
              } catch(BaseException &b) {
                cout << "******* Exception during FLASH upload!********\n";
                cout << b << endl;
              }
              delete rod0;
            }
          }       //end of loop over slot
        }       // end of confirmatory if
// Clean up before exiting this flash update.
        delete [] buffer;
	binFile.close();    //close the file
      }
    }
    else {
      cout << "Update of this flash is skipped" << endl;
    }
//
  cout << endl << endl;
  }     // end of loop over select
//
// On exit, delete the VME object.
  delete vme1;

  return 0;  
}


