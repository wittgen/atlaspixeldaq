// A program to dump blocks of memory in either Master DSP memory space or a 
// Slave DSP memory space.

#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  long numWords;                                // Number of words to dump
  long dspStart;                                // Starting address to dump
  char response;                                // Y/n response
  long dspNumber=-2;                            // DSP number (-1 for MDSP)
  std::string ipramFile("../Dsp/Binary/slaveRunBios_IPRAM.bin");
  std::string idramFile("../Dsp/Binary/slaveRunBios_IDRAM.bin");
  std::string extFile("../Dsp/Binary/slaveRunBios_xcode.bin");

  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          dspNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }
  
// get DSP number (-1 for MDSP) starting address and number of bytes to read

// Get Slave number to read
  do {
    if (dspNumber < -1 ) {
      cout << "Enter DSP number (-1:3):"; 
      cin >> dspNumber;
      while ((dspNumber < -1) || (dspNumber > 3)) {
        cout << "DSP number out or range [-1:3], re-enter: ";
        cin >> dspNumber;
      }
    }

// Initialize slave DSP
  if (dspNumber !=-1) {
    rod0->initSlaveDsp(ipramFile, idramFile, extFile, dspNumber);
  }

    hex(cin);
    cout << "Starting address (hex, without leading 0x): ";
    cin >> dspStart;
    cout << "Number of words (hex, without leading 0x): ";
    cin >> numWords;
    dec(cin);
  
// create buffer and read data
    unsigned long * buffer = new unsigned long[numWords];
    
    if (dspNumber == -1) {
      rod0->mdspBlockRead(dspStart, buffer, numWords);
    }
    else {
      rod0->slvBlockRead(dspStart, buffer, numWords, dspNumber);
    }
  
// output the data and delete the buffer
    hex(cout);
    cout.fill('0');
    for (int i=0; i<numWords; i+=8) {
      cout.width(8);
      cout << buffer[i]   << ' ';
      cout.width(8);
      cout << buffer[i+1] << ' ';
      cout.width(8);
      cout << buffer[i+2] << ' ';
      cout.width(8);
      cout << buffer[i+3] << ' ';
      cout.width(8);
      cout << buffer[i+4] << ' ';
      cout.width(8);
      cout << buffer[i+5] << ' ';
      cout.width(8);
      cout << buffer[i+6] << ' ';
      cout.width(8);
      cout << buffer[i+7] << endl;
    }
    dec(cout);
    cout.fill(' ');
    delete [] buffer;
    
// do another?
    cout << "Do another? [Y/n]: ";
    cin >> response;
  } while (response != 'n');

// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


