#include <iostream>
using namespace std;

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const uint32_t mapSize=0xc00040;         // Map size
  const int32_t numSlaves=4;                       // Number of slaves
  int32_t numWords;                                // Number of words to dump
  uint32_t dspStart, dspEnd;               // Address limits to dump
  char response;                                // Y/n response
  
  std::string fileName(""), option;
  int slot = -1;
  uint32_t baseAddress;
  int i, j, k, limit, itext;
  char ch;
  void* genPtr;
  char* textBuff;

  if (argc > 1) {
    for (i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

// get starting address and number of bytes to read

  do {
    hex(cin);
    cout << "Starting address (hex, without leading 0x): ";
    cin >> dspStart;
    cout << "Ending address (hex, without leading 0x): ";
    cin >> dspEnd;
    dec(cin);
    numWords = (dspEnd +1 - dspStart)/4;
  
// create buffer and read data
    uint32_t * buffer = new uint32_t[numWords];
    rod0->mdspBlockRead(dspStart, buffer, numWords);
  
// output the data and delete the buffer
// Do this is in a slightly more sophisticated way, so that part
// lines are printed sensibly.
// JCH. 12 Sep 2003.
//
    hex(cout);
    cout.fill('0');
    for (i=0; i<numWords; i+=8) {
      limit = i<(numWords-7) ? 8 : numWords%8;
      for(j=0; j<limit; j++) {
        cout.width(8);
        cout << buffer[i+j]   << ' ';
      }
      cout << endl;
    }
    dec(cout);
    cout.fill(' ');
//
// Dump as ASCII characters
//
    genPtr = static_cast<void*>(buffer);
    textBuff = static_cast<char*>(genPtr);
      cout << endl << "ASCII dump of buffer:" << endl;
      for (itext = 0; itext< 4*numWords; itext++) {
        if ((textBuff[itext]<' ') || (textBuff[itext] > '~')) textBuff[itext] = '.';
        cout << textBuff[itext];
        if ((itext+1)%64 == 0) cout << endl;
      }
      cout << endl;

    for (i=0; i<numWords; i+=8) {
      limit = i<(numWords-7) ? 8 : numWords%8;
      for(j=0; j<limit; j++) {
        for(k=0; k<3; k++) {
          ch = (buffer[i+j]>>8*k)|0x000000FF;
          cout << ch;
        }
      }
      cout << endl;
    }
    dec(cout);
    delete [] buffer;
    
// do another?
    cout << "Do another? [Y/n]: ";
    cin >> response;
  } while (response != 'n');

// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


