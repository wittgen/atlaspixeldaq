#include "DataDecoder.h"
#include "NewBits.h"

#include <iostream>

DataDecoder::DataDecoder(){}
  
unsigned int DataDecoder::rdRegister(NewBits *bits, unsigned int hdIndex) {
  return stringToUInt(bits->toString(hdIndex+5, hdIndex+21));
}
  
unsigned int DataDecoder::stringToUInt(std::string input) {
  if (input.length() > 32) return 0;
  unsigned int temp = 0;
  for (unsigned int i=0; i<input.length();i++) {
    if (input[input.length()-i-1] == '1') temp |= (1 << i);
  }
  return temp;
}

DataDecoder::~DataDecoder(){}
