#ifndef COMMANDGENERATOR_H
#define COMMANDGENERATOR_H
#include <iostream>
#include <string>
#include <vector>
#include "NewBits.h"

class commandGenerator {
public:
  commandGenerator();
  
  NewBits *wordTest(NewBits *bits=0);
  NewBits *trigger(NewBits *bits=0); //lvl 1 trigger
  NewBits *bcr(NewBits *bits=0); //bunch counter reset
  NewBits *ecr(NewBits *bits=0); //event counter reset
  NewBits *cal(NewBits *bits=0); //calibration pulse
  NewBits *sync(NewBits *bits=0); //sync front end
  NewBits *wrRegister(int address, unsigned int data, NewBits *bits=0); //write data to address in the register
  NewBits *rdRegister(int address, NewBits *bits=0); //read from an address in the register
  NewBits *wrFifo(unsigned int data, NewBits *bits=0); //write a word to the enabled fifo(s)
  NewBits *rdFifo(int address, NewBits *bits=0); //read a word from the enabled fifo(s)
  NewBits *wrFE(int data, NewBits *bits=0); //write data to the front end chips
  NewBits *rdFE(int data, NewBits *bits=0); //read or r/w from the front end chips
  NewBits *enDataTake(NewBits *bits=0); //enable data taking
  NewBits *resetMCC(NewBits *bits=0); //global mcc reset
  NewBits *resetFE(int syncW, NewBits *bits=0); //global front end reset. setting syncW to 15 resets full front end.
  
  ~commandGenerator();
};

#endif
