#ifndef DATADECODER_H
#define DATADECODER_H
#include <iostream>
#include <string>
#include <vector>
#include "NewBits.h"

class DataDecoder {
public:
  DataDecoder();
  
  unsigned int rdRegister(NewBits *bits, unsigned int hdIndex); //read from an address in the register
  
  ~DataDecoder();
  
private:
  unsigned int stringToUInt(std::string input);
};

#endif
