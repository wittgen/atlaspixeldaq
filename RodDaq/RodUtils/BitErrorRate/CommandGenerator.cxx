#include "CommandGenerator.h"
#include "NewBits.h"

#include <iostream>

commandGenerator::commandGenerator(){}
  
NewBits * commandGenerator::wordTest(NewBits *bits) {
  if (!(bits)) bits=new NewBits();
  bits->append("11111111");
  
  return bits;
}
NewBits * commandGenerator::trigger(NewBits *bits) { //lvl 1 trigger
  if (!(bits)) bits=new NewBits();
  bits->append("11101");
    
  return bits;
}
  
NewBits * commandGenerator::bcr(NewBits *bits) { //bunch counter reset
  if (!(bits)) bits=new NewBits();
  bits->append("101100001");
    
  return bits;
}
  
NewBits * commandGenerator::ecr(NewBits *bits) { //event counter reset
  if (!(bits)) bits=new NewBits();
  bits->append("1011000010");
    
  return bits;
}
  
NewBits * commandGenerator::cal(NewBits *bits) { //calibration pulse
  if (!(bits)) bits=new NewBits();
  bits->append("101100100");
    
  return bits;
}
  
NewBits * commandGenerator::sync(NewBits *bits) { //sync front end
  if (!(bits)) bits=new NewBits();
  bits->append("101101000");
    
  return bits;
}
  
NewBits * commandGenerator::wrRegister(int address, unsigned int data, NewBits *bits) { //write data to address in the register
  if (!(bits)) bits=new NewBits();
  std::string addressString=bits->decToBin(address);
  std::string dataString=bits->decToBin(data);
  
  if (addressString.length()>4) {
    std::cout << "Error: Address too long" << std::endl;
    return bits;
  }
  
  if (dataString.length()>16) {
    std::cout << "Error: Data too long" << std::endl;
    return bits;
  }
  
  while (addressString.length()<4)
    addressString="0"+addressString;
  while (dataString.length()<16)
    dataString="0"+dataString;
  
//  std::cout << "Address and Data: " << addressString << " : " << dataString << std::endl;

  bits->append("1011010110000");
  bits->append(addressString);
  bits->append(dataString);

//  int pos = bits->size() - 33;
//  std::cout << "Command String is: " << bits->toString(pos, pos+13) << " : " << bits->toString(pos+13, pos+17) << " : " << bits->toString(pos+17, bits->size()) << std::endl;
  
  return bits;
}
  
NewBits * commandGenerator::rdRegister(int address, NewBits *bits) { //read from an address in the register
  if (!(bits)) bits=new NewBits();
  std::string addressString=bits->decToBin(address);
  
  if (addressString.length()>4) {
    std::cout << "Error: Address too long" << std::endl;
    return bits;
  }
  
  while (addressString.length()<4)
    addressString="0"+addressString;
    
  bits->append("1011010110001");
  bits->append(addressString);
  bits->append("0000000000000000");
    
  return bits;
}
  
NewBits * commandGenerator::wrFifo(unsigned int data, NewBits *bits) { //write a word to the enabled fifo(s)
  if (!(bits)) bits=new NewBits();
  bits->append("10110101100100000");
  std::string dataString=bits->decToBin(data);
  
  if (dataString.length()>27) {
    std::cout << "Error: Data too long" << std::endl;
    return bits;
  }
  
  while (dataString.length()<27)
    dataString="0"+dataString;
  
  bits->append(dataString);
  
  return bits;
}
  
NewBits * commandGenerator::rdFifo(int address, NewBits *bits) { //read a word from the enabled fifo(s)
  if (!(bits)) bits=new NewBits();
  std::string addressString=bits->decToBin(address);
  
  if (addressString.length()>4) {
    std::cout << "Error: Address too long" << std::endl;
    return bits;
  }
  
  while (addressString.length()<4)
    addressString="0"+addressString;
  bits->append("1011010110011");
  bits->append(addressString);
  bits->append("000000000000000000000000000");
    
  return bits;
}
  
NewBits * commandGenerator::wrFE(int data, NewBits *bits) { //write data to the front end chips
  if (!(bits)) bits=new NewBits();
  bits->append("10110101101000000");
  bits->append(data);
  
  return bits;
}
  
NewBits * commandGenerator::rdFE(int data, NewBits *bits) { //read or r/w from the front end chips
  if (!(bits)) bits=new NewBits();
  bits->append("10110101101010000");
  bits->append(data);
  
  return bits;
}
  
NewBits * commandGenerator::enDataTake(NewBits *bits) { //enable data taking
  if (!(bits)) bits=new NewBits();
  bits->append("10110101110000000");
    
  return bits;
}
  
NewBits * commandGenerator::resetMCC(NewBits *bits) { //global mcc reset
  if (!(bits)) bits=new NewBits();
  bits->append("10110101110010000");
  bits->append("000000000000000000000000000");
    
  return bits;
}
  
NewBits * commandGenerator::resetFE(int syncW, NewBits *bits) { //global front end reset
  if (!(bits)) bits=new NewBits();
  std::string syncWstring=bits->decToBin(syncW);
  
  if (syncWstring.length()>4) {
    std::cout << "Error: SyncW too long" << std::endl;
    return bits;
  }
  
  while (syncWstring.length()<4)
    syncWstring="0"+syncWstring;
  bits->append("10110101110100000");
  bits->append(syncWstring);
  bits->append("000000000000000000000000000");
  
  return bits;
}
  
commandGenerator::~commandGenerator(){}
