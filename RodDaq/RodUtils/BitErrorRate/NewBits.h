#ifndef NEWBITS_H
#define NEWBITS_H
#include <string>
#include <vector>
#include <stdlib.h>

//namespace PixLib {

  class NewBits {
  public:
    NewBits(); //default constructor
    NewBits(std::string binIn); //construct from string

    void dump(); //outputs binary stream
    void dump(int f, int l);  //outputs binary stream in range f to l noninclusive
    void clear();
    void append(std::string binIn);
    void append(int decIn);
    void append(std::vector<bool> bitsIn); //adds more elements to end of bits vector
    void append(const char *binIn) {append(std::string(binIn));}
    int findBin(std::string littleBin, bool accurate=false, int foundMax=1);
    int getNumWords();
    int size(); //returns size of bits vector
    unsigned int returnWord(int wordIndex);
    std::string decToBin(unsigned int decIn);
    std::vector<bool> getBits(); //returns bits vector
    void remove(int first, int last); //removes elements from the bits vector in the given range
    void remove(int index); //removes element from bits vector at specified index
    std::string toString(int s, int f);
    std::string toString();
    std::string toString(int i);
    int compare(std::string binIn, std::string binOut); //bitwise compare. returns number of different bits
    ~NewBits() {}

  private:
    std::vector<bool> bits;
    int binToDec(std::string binIn);
  };

//}

#endif
