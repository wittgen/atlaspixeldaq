#include "NewBits.h"

#include <iostream>
#include <sstream>
#include <math.h>
#include <string>
#include <algorithm>

//using namespace PixLib;

NewBits::NewBits() { //default constructor
  bits.clear();
}

NewBits::NewBits(std::string binIn) { //construct from string
  bits.clear();
  int startPoint=0;
  int numOfBits=binIn.length();
  if (binIn[1] == 'X' || binIn[1] == 'x') 
    startPoint=2;
  for (int i = startPoint; i<numOfBits;i++) {
    if (binIn[i]=='0')
      bits.push_back(false);
    else 
      bits.push_back(true);
  }
}

void NewBits::clear() {
  bits.clear();
}

int NewBits::size() { //returns size of bits vector
  return (int) bits.size();
}

std::vector<bool> NewBits::getBits() { //returns bits vector
  return bits;
}

void NewBits::append(std::vector<bool> bitsIn) { //adds more elements to end of bits vector
  bits.insert(bits.end(), bitsIn.begin(), bitsIn.end());
}

void NewBits::remove(int first, int last) { //removes elements from the bits vector in the given range
  bits.erase(bits.begin()+first, bits.begin()+last);
}

void NewBits::remove(int index) { //removes element from bits vector at specified index
  bits.erase(bits.begin()+index);
}

std::string NewBits::decToBin(unsigned int decIn) { //converts decimal numbers to binary
  std::string binOut = "";

    while (decIn)
    {
        if ( (decIn & 1) == 0 )
            binOut += "0";
        else
            binOut += "1";

        decIn >>= 1;
    };

    std::reverse(binOut.begin(), binOut.end());
    return binOut;
}

int NewBits::binToDec(std::string binIn) {
  unsigned int decOut=0;
  int numAtPos;
  int binLength=binIn.length();
  
  for (int i=0;i<binLength;i++) {
    numAtPos=(binIn[binLength-i-1]=='1' ? 1 : 0);
    decOut+=pow(2,i)*numAtPos;
  }
  
  return decOut;
}


void NewBits::dump() { //outputs binary stream to console
  for (int i=0;i<this->size();i++)
    std::cout << (bits[i] ? "1" : "0");
  std::cout << std::endl;
}

void NewBits::dump(int f, int l) { //outputs binary stream in range f to l noninclusive
  for (int i=f;i<l;i++)
    std::cout << (bits[i] ? "1" : "0");
  std::cout << std::endl;
}

void NewBits::append(std::string binIn) { //appends to right side of binary stream w/ string input
  for (unsigned int i=0;i<binIn.length();i++) {
    if (binIn[i]=='0')
      bits.push_back(false);
    else
      bits.push_back(true);
  }
}

void NewBits::append(int decIn) { //appends to right side of binary stream w/ decimal input
  append(decToBin(decIn));
}

std::string NewBits::toString() {
  std::string returnStr="";
  for (int i=0; i<this->size();i++) 
    returnStr+=(bits[i] ? "1" : "0");
  
  
  return returnStr;
}

std::string NewBits::toString(int s, int f) {
  std::string returnStr="";
  if (f<s || s<0 || f>this->size())
    std::cout << "Error: Bounds out of range or out of order" << std::endl;

  for (int i=s; i<f; i++)
    returnStr+=(bits[i] ? "1" : "0");
  
  return returnStr;
}

std::string NewBits::toString(int i) {
  return (bits[i] ? "1" : "0");
}

int NewBits::findBin(std::string littleBin, bool accurate, int foundMax) {
  std::string bigBin=toString();
  size_t found = bigBin.find(littleBin);
  int counter=1;
  int closeness=0;
  int oldCloseness=0;
  int closePlace=-1;
  if (found!=std::string::npos) {
   while (counter < foundMax) {
    bigBin.erase(found, 31);
    found = bigBin.find(littleBin);
    if (found==std::string::npos && accurate) return -1;
    counter++;
   }
    return int(found) + 31*(foundMax-1);
  }
  else {
    if (accurate) return -1;
    for (unsigned int i=0; i<bigBin.length()-littleBin.length();i++) {
      for (unsigned int j=0;j<littleBin.length();j++) {
	if (littleBin[j]==bigBin[j+i])
	  closeness++;
      }
      if (closeness>oldCloseness) {
	oldCloseness=closeness;
	closePlace=i;
      }
      closeness=0;
    }
    return closePlace;
  }
    
}

int NewBits::getNumWords() {
  int rem=this->size()%32;
  int numWords=(this->size()-rem)/32;
  
  if (rem!=0) numWords++;
  
  return numWords;
}

unsigned int NewBits::returnWord(int wordIndex) {
  int rem=this->size()%32;
  unsigned int word=-1;
  int numWords=getNumWords();
  std::string wordString;
  while (wordIndex>=numWords || wordIndex<0)
    std::cout << "Error: Index out of bounds" << std::endl;
      
  if (wordIndex<numWords-1 || (rem==0 && wordIndex==numWords-1)) {
    word=binToDec(toString(wordIndex*32, wordIndex*32 +32 ));
  }
  else {
    wordString=toString(wordIndex*32, wordIndex*32+rem);
    for (int i=rem;i<32;i++)
      wordString+="0";
    word=binToDec(wordString);
  }
  return word;
}

int NewBits::compare(std::string binIn, std::string binOut) {
  int bitDiff=0;
  for (unsigned int i=0; i<binOut.size();i++) {
    if (binIn[i]!=binOut[i]) bitDiff++;
  }
  return bitDiff;
}
