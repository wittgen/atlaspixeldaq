#include "NewBits.h"
#include "CommandGenerator.h"
#include "DataDecoder.h"

#include "RCCVmeInterface.h"
#include "RodModule.h"
#include "BocCard.h"
#include "RodPrimitive.h"
//#include "registerIndices.h"

#include <string>
#include <iomanip>
#include <iostream>
#include <fstream>
#include <sstream>

#include <unistd.h>
#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

#define COUNTER_BASE            0x00400090
#define FMT_DIST                0x00000400
#define FMT_CNT_CONTROL(fmt)    COUNTER_BASE + (fmt * FMT_DIST)
#define FMT_GLB_COUNTER(fmt)    COUNTER_BASE + (fmt * FMT_DIST) + 0x00000004
#define FMT_STREAM_01(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000008
#define FMT_STREAM_23(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000000C
// More definitions for SCT convenience
#define FMT_STREAM_45(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000010
#define FMT_STREAM_67(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000014
#define FMT_STREAM_89(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x00000018
#define FMT_STREAM_AB(fmt)      COUNTER_BASE + (fmt * FMT_DIST) + 0x0000001C

// Definition to bypass use of registerindices.h
#define FMT_LINK_EN(fmt)              (fmt)
#define FMT_EXP_MODE_EN(fmt)          (1 +(FMT_LINK_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_CONFIG_MODE_EN(fmt)       (1 +(FMT_EXP_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_EDGE_MODE_EN(fmt)         (1 +(FMT_CONFIG_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_READOUT_TIMEOUT(fmt)      (1 +(FMT_EDGE_MODE_EN(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_DATA_OVERFLOW_LIMIT(fmt)  (1 +(FMT_READOUT_TIMEOUT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_LIMIT(fmt) (1 +(FMT_DATA_OVERFLOW_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_LIMIT(fmt)       (1 +(FMT_HEADER_TRAILER_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_LINK_L1A_CNT(fmt)     (1 +(FMT_ROD_BUSY_LIMIT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_PXL_BANDWIDTH(fmt)        (1 +(FMT_PXL_LINK_L1A_CNT(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_DATA_TEST_MUX(fmt)   (33 +(FMT_PXL_BANDWIDTH(FORMATTERS_PER_ROD-1)) +fmt)
/* +4*8 reserved IDs */
#define FMT_MB_DIAG_REN(fmt)          (1 +(FMT_LINK_DATA_TEST_MUX(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_LINK_OCC_CNT(fmt,lnk)     (1 +(FMT_MB_DIAG_REN(FORMATTERS_PER_ROD-1)) \
+(fmt*LINKS_PER_FORMATTER) +lnk)

#define FMT_TIMEOUT_ERR(fmt)          (1 +(FMT_LINK_OCC_CNT((FORMATTERS_PER_ROD-1), \
(LINKS_PER_FORMATTER-1))) \
+fmt)
#define FMT_DATA_OVERFLOW_ERR(fmt)    (1 +(FMT_TIMEOUT_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_HEADER_TRAILER_ERR(fmt)   (1 +(FMT_DATA_OVERFLOW_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_ROD_BUSY_ERR(fmt)         (1 +(FMT_HEADER_TRAILER_ERR(FORMATTERS_PER_ROD-1)) +fmt)

#define FMT_DATA_FMT_STATUS(fmt)      (1 +(FMT_ROD_BUSY_ERR(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_STATUS(fmt)               (1 +(FMT_DATA_FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_VERSION(fmt)              (1 +(FMT_STATUS(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_05(fmt)      (1 +(FMT_VERSION(FORMATTERS_PER_ROD-1)) +fmt)
#define FMT_MODEBIT_STAT_6B(fmt)      (1 +(FMT_MODEBIT_STAT_05(FORMATTERS_PER_ROD-1)) +fmt)


/* Event Fragment Builder FPGA registers */
#define EFB_ERROR_MASK(efb, lnk) (25 + FMT_MODEBIT_STAT_6B(FORMATTERS_PER_ROD-1) + \
efb*DATA_LINKS_PER_EFB + lnk)
#define FORMAT_VRSN_LSB      (1 + EFB_ERROR_MASK((EFBS_PER_ROD-1),(DATA_LINKS_PER_EFB-1)))
#define FORMAT_VRSN_MSB      (1 + FORMAT_VRSN_LSB)
#define SOURCE_ID_LSB        (1 + FORMAT_VRSN_MSB)
#define SOURCE_ID_MSB        (1 + SOURCE_ID_LSB)
#define EFB_CMND_0           (1 + SOURCE_ID_MSB)
#define EFB_FORMATTER_STAT   (1 + EFB_CMND_0)
#define EFB_RUNTIME_STAT_REG (1 + EFB_FORMATTER_STAT)
#define EVENT_HEADER_DATA    (1 + EFB_RUNTIME_STAT_REG)
#define EV_FIFO_DATA1        (1 + EVENT_HEADER_DATA)
#define EV_FIFO_DATA2        (1 + EV_FIFO_DATA1)

#define EVT_MEM_MODE         (1 + EV_FIFO_DATA2)
#define EVT_MEM_CMND_STAT    (1 + EVT_MEM_MODE)
#define EVT_MEM_RESET        (1 + EVT_MEM_CMND_STAT)
#define EVT_MEM_FLAGS        (1 + EVT_MEM_RESET)
#define EVT_MEM_A_WRD_CNT    (1 + EVT_MEM_FLAGS)
#define EVT_MEM_B_WRD_CNT    (1 + EVT_MEM_A_WRD_CNT)
#define EVT_MEM_PLAY_EVENT   (1 + EVT_MEM_B_WRD_CNT)
#define EVT_MEM_STATUS       (1 + EVT_MEM_PLAY_EVENT)
#define EFB_EVT_CNT          (1 + EVT_MEM_STATUS)
#define EFB_BANDWIDTH_CNT    (1 + EFB_EVT_CNT)
#define EFB_CODE_VERSION     (1 + EFB_BANDWIDTH_CNT)

/* Router FPGA registers- note that addresses in router are done in 4 blocks
 * (1 block per slv) of CMD0(slv), CMD1(slv), .... INT_DELAY _CNT, ..., while here
 * they are indexed (used in the rodRegister structure in accessRegister.c) as
 * CMD0(0), ... CMD0(3), CMD1(0)...  The addressing for rodRegister is done in
 * accessRegister.c  dpsf. */
#define RTR_TRAP_CMND_0(slv)       (14 + EFB_CODE_VERSION + slv)
#define RTR_TRAP_CMND_1(slv)       (1 + RTR_TRAP_CMND_0(N_SDSP-1)  + slv)
#define RTR_TRAP_RESET(slv)        (1 + RTR_TRAP_CMND_1(N_SDSP-1)  + slv)
#define RTR_TRAP_STATUS(slv)       (1 + RTR_TRAP_RESET(N_SDSP-1)   + slv)
#define RTR_TRAP_MATCH_0(slv)      (1 + RTR_TRAP_STATUS(N_SDSP-1)  + slv)
#define RTR_TRAP_MOD_0(slv)        (1 + RTR_TRAP_MATCH_0(N_SDSP-1) + slv)
#define RTR_TRAP_MATCH_1(slv)      (1 + RTR_TRAP_MOD_0(N_SDSP-1)   + slv)
#define RTR_TRAP_MOD_1(slv)        (1 + RTR_TRAP_MATCH_1(N_SDSP-1) + slv)
#define RTR_TRAP_XFR_FRM_SIZE(slv) (1 + RTR_TRAP_MOD_1(N_SDSP-1)   + slv)
#define RTR_TRAP_FIFO_WRD_CNT(slv) (1 + RTR_TRAP_XFR_FRM_SIZE(N_SDSP-1) + slv)
/* 1 unused half-word/slv in router, reserved here (1 word/hw) */
#define RTR_TRAP_EVT_CNT(slv)      (5 + RTR_TRAP_FIFO_WRD_CNT(N_SDSP-1) + slv)
#define RTR_TRAP_INT_DELAY_CNT(slv) (1 + RTR_TRAP_EVT_CNT(N_SDSP-1) + slv)
/* 3 unused hw before beginning of next slave address block */
#define RTR_CMND_STAT              (13 + RTR_TRAP_INT_DELAY_CNT(N_SDSP-1))
#define RTR_SLNK_ATLAS_DUMP_MATCH  (1 + RTR_CMND_STAT)
#define RTR_SLNK_ROD_DUMP_MATCH    (1 + RTR_SLNK_ATLAS_DUMP_MATCH)

#define RTR_CODE_VERSION           (1 + RTR_SLNK_ROD_DUMP_MATCH)
#define RTR_OUTPUT_SIGNAL_MUX      (1 + RTR_CODE_VERSION)

/* ROD resources interface FPGA (RRIF) registers */
#define RRIF_CODE_VERSION         (16 + RTR_OUTPUT_SIGNAL_MUX)
#define RRIF_CMND_1               (1 + RRIF_CODE_VERSION)
#define RRIF_CMND_0               (1 + RRIF_CMND_1)
#define ROD_MODE_REG              (1 + RRIF_CMND_0)
#define FE_MASK_LUT_SELECT        (1 + ROD_MODE_REG)
#define RRIF_STATUS_1             (1 + FE_MASK_LUT_SELECT)
#define RRIF_STATUS_0             (1 + RRIF_STATUS_1)
#define FE_CMND_MASK_0_LO         (3 + RRIF_STATUS_0)
#define FE_CMND_MASK_0_HI         (1 + FE_CMND_MASK_0_LO)
#define FE_CMND_MASK_1_LO         (1 + FE_CMND_MASK_0_HI)
#define FE_CMND_MASK_1_HI         (1 + FE_CMND_MASK_1_LO)
#define CALSTROBE_DELAY           (1 + FE_CMND_MASK_1_HI)
#define CAL_CMND                  (1 + CALSTROBE_DELAY)
#define FRMT_RMB_STATUS           (3 + CAL_CMND)
#define EFB_DM_FIFO_FLAG_STA      (2 + FRMT_RMB_STATUS)
#define EFB_DM_WC_STA_REG         (1 + EFB_DM_FIFO_FLAG_STA)
#define INP_MEM_CTRL              (1 + EFB_DM_WC_STA_REG)
#define DBG_MEM_CTRL              (1 + INP_MEM_CTRL)
#define CFG_READBACK_CNT          (1 + DBG_MEM_CTRL)
#define IDE_MEM_CTRL              (2 + CFG_READBACK_CNT)
#define IDE_MEM_STAT              (1 + IDE_MEM_CTRL)
#define INTRPT_TO_SLV             (7 + IDE_MEM_STAT)
#define INTRPT_FROM_SLV           (1 + INTRPT_TO_SLV)

#define DFLT_ROD_EVT_TYPE         (1 + INTRPT_FROM_SLV)
#define CRTV_ROD_EVT_TYPE         (1 + DFLT_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_0        (1 + CRTV_ROD_EVT_TYPE)
#define CAL_L1_TRIG_TYPE_1        (1 + CAL_L1_TRIG_TYPE_0)
#define CAL_L1_ID_0               (1 + CAL_L1_TRIG_TYPE_1)
#define CAL_L1_ID_1               (1 + CAL_L1_ID_0)
#define CAL_BCID                  (1 + CAL_L1_ID_1)

using namespace SctPixelRod;

std::vector<unsigned int>* sendCommand (RodModule *m_rod, NewBits *bitsIn, int channel, bool returnStream=false, unsigned int nBits=0x8000) {
	int packSize = bitsIn->getNumWords();
	int pLen = sizeof(SendSerialStreamIn)/sizeof(uint32_t);
	uint32_t *tsdData = new uint32_t[pLen+packSize];
//    std::cout << "Stream: " << bitsIn->toString(0, bitsIn->size()) << std::endl;
	for (int i=pLen;i<pLen+packSize;i++) {
		tsdData[i]=bitsIn->returnWord(i-pLen);
//        int fillZeros = 32-bitsIn->decToBin(tsdData[i]).size();
//        std::cout << "Word " << i << " : ";
//        for (int j=0;j<fillZeros;j++) std::cout << "0";
//        std::cout << bitsIn->decToBin(tsdData[i]) << std::endl;
	}
	
	// Set the output mask
	unsigned int mask1 = 0x0, mask2 = 0x0;
	if ( channel < 0 || channel >= 48 ) {
		mask1 = 0xffffffff;
		mask2 = 0xffffffff;
	} else {
		if (channel < 32) {
			mask1 = (1 << channel);
			mask2 = 0x0;
		} else {
			mask1 = 0x0;
			mask2 = (1 << (channel - 32));
		}
	}
	m_rod->mdspSingleWrite(FE_CMND_MASK_0_LO, mask1);
	m_rod->mdspSingleWrite(FE_CMND_MASK_0_HI, mask2);
	m_rod->mdspSingleWrite(FE_CMND_MASK_1_LO, 0x00000000);
	m_rod->mdspSingleWrite(FE_CMND_MASK_1_HI, 0x00000000);
	
	SendSerialStreamIn *sendSerialStreamIn = (SendSerialStreamIn*)tsdData;
	sendSerialStreamIn->sportId  = 0;
	sendSerialStreamIn->nBits    = packSize*32;
	sendSerialStreamIn->fCapture = returnStream ? 1 : 0;
	sendSerialStreamIn->nCount   = 1;
	sendSerialStreamIn->dataPtr  = 0;
	sendSerialStreamIn->maskLo   = mask1;
	sendSerialStreamIn->maskHi   = mask2;
	
	RodPrimitive* sendStream = new RodPrimitive(7+packSize, 0, SEND_SERIAL_STREAM, 0, (int*)sendSerialStreamIn);
//    if (returnStream) {
	  m_rod->executeMasterPrimitiveSync(*sendStream);
//    } else {
//      m_rod->executeMasterPrimitive(*sendStream);
//    }
	
	delete sendStream;
	delete[] tsdData;
	
	
	std::vector<unsigned int>* out = new std::vector<unsigned int>;
	if (!returnStream) return out;
	std::vector<unsigned int> outA, outB;
	std::ostringstream onamA, onamB;
	//OldDsp: rod0->readFifo(INPUT_MEM, BANK_A, 0x8000, out);
	m_rod->readFifo(FIFO_INMEMA, 0, nBits, outA);
//    onamA << "IN_MEM_A.bin";
//    std::ofstream foutA(onamA.str().c_str(),std::ios::binary);
//    for (unsigned int i=0; i<outA.size(); i++) {
//        foutA.write((char *)&(outA[i]),4);
//    }
//    foutA.close();
	//OldDsp: rod0->readFifo(INPUT_MEM, BANK_B, 0x8000, out);
	m_rod->readFifo(FIFO_INMEMB, 0, nBits, outB);
//    onamB << "IN_MEM_B.bin";
//    std::ofstream foutB(onamB.str().c_str(),std::ios::binary);
//    for (unsigned int i=0; i<outB.size(); i++) {
//        foutB.write((char *)&(outB[i]),4);
//    }
//    foutB.close();
	
//    std::cout << "Size of out vectors is: " << outA.size() << std::endl;
	
#if 1
	int OUTLINKS[32] = { 0, 1, 2, 3, 12, 13, 14, 15, 32, 33, 34, 35, 36, 37, 38, 39,
		48, 49, 50, 51, 60, 61, 62, 63, 80, 81, 82, 83, 84, 85, 86, 87};
#else
	int OUTLINKS[32];
	for (int i=0;i<32;i++) OUTLINKS[i] = i;
#endif
	
	unsigned int i = 0;
	while (i<(outA.size()-3)){
		unsigned int temp=0;
		for(int j=0; j<32;j++) {
			if (OUTLINKS[j] < 32)
				temp |= (outA[i] & (1 << (OUTLINKS[j]))) ? (1 << j) : 0;
			else if (OUTLINKS[j] < 48)
				temp |= (outA[i+1] & (1 << (OUTLINKS[j]-32))) ? (1 << j) : 0;
			else if (OUTLINKS[j] < 80)
				temp |= (outB[i] & (1 << (OUTLINKS[j]-48))) ? (1 << j) : 0;
			else
				temp |= (outB[i+1] & (1 << (OUTLINKS[j]-80))) ? (1 << j) : 0;
		}
		out->push_back(temp);
		temp=0;
		for(int j=0; j<32;j++) {
			if (OUTLINKS[j] < 16)
				temp |= (outA[i+1] & (1 << (OUTLINKS[j]+16))) ? (1 << j) : 0;
			else if (OUTLINKS[j] < 48)
				temp |= (outA[i+2] & (1 << (OUTLINKS[j]-16))) ? (1 << j) : 0;
			else if (OUTLINKS[j] < 64)
				temp |= (outB[i+1] & (1 << (OUTLINKS[j]-32))) ? (1 << j) : 0;
			else
				temp |= (outB[i+2] & (1 << (OUTLINKS[j]-64))) ? (1 << j) : 0;
		}
		out->push_back(temp);
		i += 3;
	}
	
	return out;
}

NewBits* getChannelContent(std::vector<unsigned int>* out, int channel) {
	NewBits* nb = new NewBits();
	for(unsigned int i=0; i<out->size(); i++)
		nb->append((out->at(i) & (1 << channel)) ? std::string("1") : std::string("0"));
	return nb;
}

std::string decToString(unsigned int val) {
	std::stringstream ss;
	for (int i=0; i<32; i++) ss << ((val & (1<<i)) > 0 ? "1" : "0");
	return ss.str();
}

void dumpOut(std::vector<unsigned int>* out) {
	for (unsigned int i=0; i<out->size(); i++) std::cout << decToString(out->at(i)) << std::endl;
}

unsigned int encode(unsigned int codeword) {
	unsigned int temp = 0xffff & ((((~codeword) & 0xff) << 8) + (codeword & 0xff));
//    unsigned int temp = 0;
//    int i = 0;
//    while (codeword) {
//        temp += (codeword & 1) * 3 * (pow(2, i*2));
//        codeword >>= 1;
//        i++;
//    }
//    std::cout << std::hex << "Encoded: " << codeword << " into " << temp << std::dec << std::endl;
	return temp;
}

unsigned int decode(unsigned int codeword) {
	unsigned int tempA, tempB;
	tempA = codeword & 0xff;
	tempB = ~codeword & 0xff00;
	tempB >>= 8;
//    std::cout << std::hex << "Return " << codeword << " parts say: " << tempA << " and " << tempB << std::dec << std::endl;
	if (tempA == tempB) return tempA;
	else return (codeword);
}

std::vector<unsigned int> *reduce(std::vector<unsigned int> *reduceMe) {
	std::vector<unsigned int> *fillMe = new std::vector<unsigned int>();
	fillMe->clear();
	for (std::vector<unsigned int>::iterator itr=reduceMe->begin(); itr != reduceMe->end(); itr++) {
		bool found = false;
		for (std::vector<unsigned int>::iterator itf = fillMe->begin(); itf != fillMe->end(); itf++) {
			if (*itr == *itf) found = true;
		}
		if (!found) fillMe->push_back(*itr);
	}
	return fillMe;
}

int main(int argc, char *argv[]) {
	RCCVmeInterface *vme = NULL;
	
	int slot = -1;
	std::string option, search;
	unsigned long baseAddress;
	
	std::string unit, type;
	std::string outfile = "~/dump.csv";
	
	int  constant=0, channel=-1;
	
	option = argv[0];
	search = "ConstantReturn";
	if (((int) option.find(search)) >= 0) {
		//    std::cout << "DEBUG: Found ConstantReturn in <<" << option << ">> at position " << ((int) option.find(search)) << std::endl;
		constant = 1;
	}
	
	int active = 0;
	if (argc > 1) {
		for (int i=1; i<argc; i++) {
			option = argv[i];
			if (option[0] != '-') break;
			switch (option[1]) {
				case 's': {
					slot = atoi(option.substr(2).c_str());
					break;
				}
				case 'u': {
					channel = atoi(option.substr(2).c_str());
					break;
				}
				case 'a':{
					active = 1;
					break;
				}
				default: {
					break;
				}
			}
		}
	}
	
	// Prompt for slot number
	if (slot < 0 ) {
		std::cout << "Enter slot number (decimal):";
		std::cin >> slot;
		while ((slot < 1) || (slot > 21)) {
			std::cout << "Slot number out or range [4-12:14-21], re-enter: ";
			std::cin >> slot;
		}
	} else {
		if (slot < 4 || slot == 13 || slot > 21 ) {
			std::cout << "Slot number out of range [4-12:14-21], re-enter: ";
			std::cin >> slot;
			while ((slot < 1) || (slot > 21)) {
				std::cout << "Slot number out or range [4-12:14-21], re-enter: ";
				std::cin >> slot;
			}
		}
	}
	
	std::cout << "Initializing VmeInterface ... ";
	
	try {
		vme = new RCCVmeInterface();
	} catch (VmeException &v) {
		std::cout << "VmeException:" << std::endl;
		std::cout << "  ErrorClass = " << v.getErrorClass();
		std::cout << "  ErrorCode = " << v.getErrorCode() << std::endl;
	} catch (BaseException & exc){
		std::cout << exc << std::endl;
		exit(1);
	} catch (...){
		std::cout << "error during VME initialization - exiting..." << std::endl;
		exit(1);
	}
	
	baseAddress = slot<<24;
	
	std::cout << "[done]" << std::endl;
	
	std::cout << "Initializing RodModule in slot number " << slot << " ... ";
	
	RodModule *rod = NULL;
	// SLOT Number is hardcoded in here...
	try{
		//~ app = new application(vme,tdb);
		rod = new RodModule(baseAddress, (unsigned long) 0x01000000, *vme, (long) 4);
		rod->initialize();
	} catch (BaseException & exc){
		std::cout << exc << std::endl;
		exit(1);
	} catch (...){
		std::cout << "error initializing the ROD, exiting" << std::endl;
		exit(1);
	}
	
	std::cout << "[done]" << std::endl;
	
	BocCard *my_boc = NULL;
	try{
		my_boc = rod->getBocCard();
		my_boc->readInfo();
	} catch (...) {
		std::cout << "Error during first communication with the BOC... Damnit !" << std::endl;
		exit(1);
	}
	
	my_boc->setRxDataMode(0);
	
	NewBits *nb=new NewBits();
	commandGenerator *cg=new commandGenerator();
	DataDecoder *dd = new DataDecoder();
	
	srand(time(NULL));
	std::string binIn;
	
	for(unsigned int txlink=0; txlink < 48; txlink++) {
		cg->resetMCC(nb);
		cg->wrRegister(2, encode(txlink), nb);
	//  for (int i=0; i<; i++) cg->wrFifo(0x1a00000<<2, nb);
		sendCommand(rod, nb, txlink);
		nb->clear();
	}
	cg->rdRegister(2, nb);

	std::vector<unsigned int> *rxLinks[32];
	std::vector<unsigned int> *meanThresholds[32];
	int nSettings = 0;
	for (int k = 0; k<32; k++) {
		rxLinks[k] = new std::vector<unsigned int>();
		meanThresholds[k] = new std::vector<unsigned int>();
	}
	for (unsigned int delay=0; delay < 25; delay += 9) {
		for (unsigned int k=0;k<96;k++) {my_boc->setRxDataDelay(k, delay);}
		for(unsigned int threshold=30; threshold < 256; threshold += (256-30)/5) {
			for (unsigned int k=0;k<96;k++) {my_boc->setRxThreshold(k, threshold);}
			nSettings++;
			std::vector<unsigned int>* mapOut = sendCommand(rod, nb, -1, true, 0x100);
			std::vector<unsigned int>* regOut = new std::vector<unsigned int>();
			for (int i=0; i<32; i++) {
				NewBits* chan = getChannelContent(mapOut, i);
				int pos = chan->findBin(std::string("0000011101"), true);
				if (pos < 0) continue;
				unsigned int response = decode(dd->rdRegister(chan, pos+5));
				if (response > 0xff) continue;
				regOut->push_back(i);
				rxLinks[i]->push_back(response);
				meanThresholds[i]->push_back(threshold);
				std::cout << "Link " << std::hex << (i%8+2)+(i/8*16)+0xA0 << " responded for TX link " << (response%12)+(response/12*16)+0xA0 << std::dec << std::endl;
//  	        std::cout << "Raw stream was: " << chan->toString(pos-5, pos+5) << " " << chan->toString(pos+5, pos+21) << " " << chan->toString(pos+21, pos+26) << std::endl;
				delete chan;
			}
			std::cout << "Found " << std::dec << regOut->size() << " channels responding at " << threshold << " uA Threshold and " << delay << "ns Delay" << std::endl;
		}
	}

	for (int k = 0; k < 32; k++) {
		if (rxLinks[k]->size()) {
			std::vector<unsigned int> *singleLinks = reduce(rxLinks[k]);
			unsigned int mean =0;
			for (std::vector<unsigned int>::iterator it = meanThresholds[k]->begin(); it != meanThresholds[k]->end(); it++) {
				mean += *it;
			}
			mean /= meanThresholds[k]->size();
			std::cout << "RX link " << std::hex << ((k%8+2)+(k/8*16)+0xA0) << " has responded! (TX links returned were:" << std::hex;
			for (std::vector<unsigned int>::iterator it = singleLinks->begin(); it != singleLinks->end(); it++) {std::cout << " " << ((*it)%12)+((*it)/12*16)+0xA0 ; }
			std::cout << std::dec << " - Working Settings found were " << rxLinks[k]->size() << " out of " << nSettings << " - Mean Threshold was " << mean << ")" << std::endl;
			delete singleLinks;
		}
	}
	nb->clear();

	return 0;
}
