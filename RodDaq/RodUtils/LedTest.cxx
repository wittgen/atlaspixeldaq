#include <iostream>
using namespace std;

#include <sys/time.h>

#include "RodModule.h"

#include "RCCVmeInterface.h"
//#include "parameters.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  RodPrimList primList(1);                      // Primitive List
  std::string SlaveFile("../PixDaq/Binary/slave.dld");
  std::string infobuf("");
  std::string errorbuf("");
  

  const uint32_t mapSize=0xc00040;         // Map size
  const int32_t numSlaves=4;                       // Number of slaves
  //int slaveNumber=-1;
  std::string fileName(""), option;
  //bool initRod = true;
  int slot = -1;

  uint32_t baseAddress;
  
  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'n': {
          //initRod = false;
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
	case 'f': {
	  SlaveFile = argv[++i];
	  break;
	}
        case 'v': {
          //slaveNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    cout << "LED Start init" << endl;
    rod0->initialize(true);
  }
  catch (const HpiException &h) {
    hex(cout);
    cout << h.getDescriptor() << '\n';
    cout << "calcAddr: " << h.getCalcAddr() << ", readAddr: " << 
            h.getReadAddr() << '\n';
    dec(cout);
  }
  catch (const VmeException &v) {
    cout << "VmeException creating RodModule." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 
  }
  catch (const RodException &r) {
  cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
          << '\n';
  }

  rod0->loadAndStartSlaves(SlaveFile,0xf);


  cout << "SLAVES LOADED!" << endl;
  rod0->getTextBuffer(infobuf,0,false);
  rod0->getTextBuffer(errorbuf,1,false);
  cout << "INFO: " << infobuf << endl << endl; 
  cout << "ERROR: " << errorbuf << endl << endl; 

//   RodOutList *outlist = rod0->getOutList();
//   cout << " RODOUTLIST:::::: " << outlist->nMsgs() << " Msgs" << endl;
//   unsigned long * out = outlist->getBody();
//   for(int i = 0; i<outlist->getLength();i++){
//     cout << "out[" << i << "] = " << out[i] << endl;
//   }
  
// Create and Send a FLASH_LED primitive 
  SetLedIn ledData;
//   ledData.num   = YELLOW_LED;
//   ledData.state = TOGGLE;
  ledData.num   = RED_LED;
  ledData.state = ON;
  RodPrimitive* led0;
  try {
    led0 = new RodPrimitive(sizeof(SetLedIn)/sizeof(uint32_t), 
			    0, SET_LED, 0, (int32_t*)&ledData);
  }
  catch (const std::bad_alloc&) {
    cout << "Unable to allocate led0 primitive in main." << endl;
  }

  cout << "ALIVE?" << endl;

  primList.insert(primList.begin(), *led0);

  struct timeval startTime;
  gettimeofday(&startTime, NULL);

  try {
    rod0->sendPrimList(&primList);
  }
  catch (const HpiException &h) {
    hex(cout);
    cout << h.getDescriptor() << '\n';
    cout << "calcAddr: " << h.getCalcAddr() << ", readAddr: " << 
            h.getReadAddr() << '\n';
    dec(cout);
  }
  catch (VmeException &v) {
    cout << "VmeException in SendPrimList." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl; 
  }

// Wait for ROD to begin executing and then wait for it to finish executing
// Check for error messages in text buffer and read them out if they exist.
// Note: this is NOT how primHandler and textHandler will be used once we
// go to threads.  This is for debugging the code only.
  RodPrimList::PrimState returnPState=RodPrimList::IDLE;
  do {
    try {
      returnPState = rod0->primHandler();
    }
    catch (VmeException &v) {
      cout << "VmeException in first primHandler call." << endl;
      cout << "ErrorClass = " << v.getErrorClass() << endl;
      cout << "ErrorCode = " << v.getErrorCode() << endl; 
    }
    cout << "PRIMSTATE: " << returnPState << endl;
  } while (returnPState != RodPrimList::EXECUTING); 
  rod0->getTextBuffer(infobuf,0,true);
  rod0->getTextBuffer(errorbuf,1,true);
  cout << "INFO: " << infobuf << endl << endl; 
  cout << "ERROR: " << errorbuf << endl << endl; 
  do {
    try {
      returnPState = rod0->primHandler();
    }
    catch (RodException *r) {
    cout << r->getDescriptor() <<", " << r->getData1() << ", " << r->getData2()
            << '\n';
    }
    catch (VmeException &v) {
      cout << "VmeException in second primHandler call." << endl;
      cout << "ErrorClass = " << v.getErrorClass() << endl;
      cout << "ErrorCode = " << v.getErrorCode() << endl; 
    }
    cout << "PRIMSTATE: " << returnPState << endl;
    rod0->getTextBuffer(infobuf,0,true);
    rod0->getTextBuffer(errorbuf,1,true);
    cout << "INFO: " << infobuf << endl << endl; 
    cout << "ERROR: " << errorbuf << endl << endl; 
  } while ((returnPState != RodPrimList::WAITING)&&
	   (returnPState != RodPrimList::IDLE)); 
    
  struct timeval endTime;
  gettimeofday(&endTime, NULL);

  int32_t diff = (endTime.tv_sec - startTime.tv_sec) * 1000000;
  diff += (endTime.tv_usec - startTime.tv_usec);

  cout << "LED flashing took " << ((float(diff))/1e6) << " seconds\n";

// Delete the ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


