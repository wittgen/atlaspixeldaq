//! A program to dump status and command registers in Master DSP memory space. 
// It does NOT initialize the ROD!! This program is intended to be 
// a diagnostic used to examine register contents after another
// program, e.g. EchoTest or LedTest, has run.

#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"
#include "RodDspAddresses.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  unsigned long statusVal;
  
  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule but do not initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
    
    hex(cout);
    for (int i=0; i<3; i++) {
      statusVal = rod0->mdspSingleRead(STATUS_REG[i]);
      cout << "Status register " << i << " (hex)  = ";
      cout.width(8);
      cout << statusVal << endl;
    }
    for (int i=0; i<2; i++) {
      statusVal = rod0->mdspSingleRead(COMMAND_REGISTER);
      cout << "Command register " << i << " (hex) = ";
      cout.width(8);
      cout << statusVal << endl;
    }
    dec(cout);
    
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


