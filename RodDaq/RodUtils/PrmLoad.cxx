#include <iostream>
using namespace std;

#include <ctype.h>
#include <string>
#include <fstream>

#include "RodModule.h"
#include "RCCVmeInterface.h"

int main() {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  std::string binFileName;                      // Name of binary file to load
  ifstream binFile;                             // Pointer to binary frile
  int fileSize;                                 // Size of binary file in bytes
  
  const unsigned long prmStart=0x00000000;
  unsigned long slot, baseAddress;

// Prompt for slot number
  cout << "Enter slot number (decimal):"; 
  cin >> slot;
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

  cout << "Enter binary file name, including extension (""q"" to quit): ";
  cin >>  binFileName;

  if (binFileName.c_str() == "q") exit(0);
  binFile.open(binFileName.c_str(), ios::binary);
  if (!binFile.is_open()) {
    cout << "Unable to open binary file." << endl;
    exit(1);
  }
  
// Get size of file
  binFile.seekg(0, ios::end);           // go to end of file
  fileSize = binFile.tellg();          // file size given by current location
  binFile.seekg(0, ios::beg);          // go back to beginning of file
    
// Create a buffer and read file into itprmSize = fileSize
  
  char * buffer;
  try {
    buffer = new char[fileSize];
  }
  catch (std::bad_alloc & ba) {
    cout << "Unable to allocate buffer for binary file." << endl;
    exit(2);
  }
  binFile.read(buffer, fileSize);
  
// write buffer to Proram Reset Manager
  rod0->writeBlockToFlashHpi(prmStart, (uint8_t*)buffer, fileSize);

  cout << fileSize << " bytes written to the Program Reset Manager"<< endl;
        
// Clean up before exiting
  delete [] buffer;
  delete rod0;
  delete vme1;

  return 0;  
}


