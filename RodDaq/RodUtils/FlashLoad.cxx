//------------------------------FlashLoad------------------------------ 

/*! 
 *  @brief This is a utility program for loading the flash memories which
 *  hold the program for the FPGAs. Do not use this to load the MDSP flash 
 *  memory; use MdspFlashLoad. 
 *
 *  It prompts the user for the VME slot number of the ROD, for the flash
 *  memory to load, and for the name of the binary file to load, including 
 *  the path to it. The entire contents of the file are written to the FPGA
 *  flash memory.
 *
 *  @author Tom Meyer (meyer@iastate.edu)
 */

#include <iostream>
using namespace std;


#include "RodModule.h"
#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  std::string binFileName = "";                 // Name of binary file to load
  ifstream binFile;                             // Pointer to binary frile
  unsigned long selectAddr;                     // Start address of seleced flash
  unsigned long flashAddr;                      // Location in target flash
  int fileSize;                                 // Size of binary file in bytes
  int selectSize;                               // Size of selected flash
  int iselect = -1;
  
  const unsigned long flashStart[5]={0xe00000, 0xe01000, 0xe80000, 0xed3000, 0xf26000};
  const long flashSize[5] = {24, 495204, 336688, 336680, 234456};

  const unsigned long flashStartE[5]={0xe00000, 0xe00000, 0xe80000, 0xf00000, 0xf80000};
  const long flashSizeE[5] = {0, 495204, 495212, 336680, 336680};

  std::string flashName[5] = {"Location", "ROD Controller", "Formatter", 
       "Event Fragment Builder", "Router"};
  
  std::string fileName(""), option;
  int slot = -1;
  unsigned long baseAddress, controllerVersion, formatterVersion, efbVersion, routerVersion,
                rodRevision, serialNumber;

  VmePort *myVmePort;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'f': {
          iselect = atoi(option.substr(2).c_str());
          break;
        }
	case 'i':{
	  binFileName = argv[i + 1];
	  i++;
	  break;
	}
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);

  try{
    rod0->initialize();
  }
  catch (HpiException *h) {
    hex(cout);
    cout << h->getDescriptor() << '\n';
    cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
            h->getReadAddr() << '\n';
    dec(cout);
  }

if (iselect<0) { // TNV Jan. 9, 2005 Added "0" option to print installed FPGA versions
  cout << endl;
  cout << "Enter Flash to program (or \"0\" to determine the versions currently installed):" << endl;
  cout << "    0 = Determine Installed FPGA Versions" << endl;
  cout << "    1 = Location file" << endl;
  cout << "    2 = ROD Controller" << endl;
  cout << "    3 = Formatter" << endl;
  cout << "    4 = Event Fragment Builder" << endl;
  cout << "    5 = Router" << endl;
  cout << "Your choice (0-5)? ";
  cin >> dec >> iselect;
}
  if(iselect > 5) {
    cout << "Invalid selection (too high)\n";
    return 1;
  }

  /*if(iselect < 1) {
    cout << "Invalid selection (too low)\n";
    return 1;
  }*/

  if(iselect == 0) {
    try {
      rod0->initialize();
    }
    catch (HpiException &h) {
      cout << h;
      return 1;
    }
    myVmePort = rod0->getVmePort();
    serialNumber = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
    rodRevision = (serialNumber&0x00FF0000)>>16;
    serialNumber = serialNumber&0x3FF;

    cout << endl;
    cout << "ROD Serial Number: " << hex << serialNumber << endl;
    cout << "FPGA versions currently loaded on this Rev." << hex << rodRevision<< " ROD are..." << endl;
    controllerVersion = 0x0fff & (rod0->mdspSingleRead(0x0040440c));
    cout << "ROD Controller: " << hex << controllerVersion << endl;
    
    formatterVersion = 0x0fff & (rod0->mdspSingleRead(0x00400088));
    cout << "ROD Formatter:  " << hex << formatterVersion << endl;
    
    efbVersion = 0x0fff & (rod0->mdspSingleRead(0x0040221c));
    cout << "ROD EFB:        " << hex << efbVersion << endl;
    
    routerVersion = 0x0fff & (rod0->mdspSingleRead(0x0040250c));
    cout << "ROD Router:     " << hex << routerVersion << endl;
    exit(4);
  }

  if(rod0->getRevision() >= 0xE && iselect == 1) {
    cout << "Invalid selection (no location file on Rev E/F)\n";
    return 1;
  }

// JCH 21 Feb 2005. Change default to Rev E/F (explicitly check for B/C).

  if(false){//(rod0->getRevision() == 0xB)||(rod0->getRevision() == 0xC)) {
    cout << "Loading flash to Rev B/C\n";
    selectAddr = flashStart[iselect-1];
    selectSize = flashSize[iselect-1];
  } else {
    cout << "Loading flash to Rev E/F\n";
    selectAddr = flashStartE[iselect-1];
    selectSize = flashSizeE[iselect-1];
  }

  if (binFileName.length() == 0) {
    cout << "Enter binary file name, including extension (""q"" to quit): ";
    cin >>  binFileName;
  }
  if (binFileName == "q") exit(0);
  binFile.open(binFileName.c_str(), ios::binary);
  if (!binFile.is_open()) {
    cout << "Unable to open binary file." << endl;
    exit(1);
  }
  
// Get size of file
  binFile.seekg(0, ios::end);           // go to end of file
  fileSize = binFile.tellg();          // file size given by current location
  binFile.seekg(0, ios::beg);          // go back to beginning of file
  if (fileSize != selectSize) {
    cout << "File size is incorrect. Expected: " << dec << selectSize << " Found: " 
    << dec << fileSize << endl;
    exit(3);
  }
    
// Create a buffer and read file into it
  uint8_t * buffer;
  try {
    buffer = new uint8_t[fileSize];
  }
  catch (std::bad_alloc & ba) {
    cout << "Unable to allocate buffer for binary file." << endl;
    exit(2);
  }
  binFile.read((char *)buffer, fileSize);
  
// write buffer to flash
  flashAddr = selectAddr;

  try {
    rod0->writeBlockToFlash(flashAddr, buffer, fileSize);

    cout << dec << selectSize << " bytes written to the "<< flashName[iselect-1]
         << " flash memory" << endl;
  } catch(BaseException &b) {
    cout << "******* Exception during FLASH upload!********\n";
    cout << b << endl;
  }
        
// Clean up before exiting
  delete [] buffer;
  delete rod0;
  delete vme1;

  return 0;  
}


