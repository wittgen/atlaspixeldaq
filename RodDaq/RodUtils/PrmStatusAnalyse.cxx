// Program to scan all BOC Serial Numbers, Firmware revisions and stuff in one crate

#include <iostream>
#include <fstream>


using namespace std;



// Enter various defines here to simplify and make the code a bit more readable...
// All given as an Offset from the PRM Address Space:
#define FPGA_CONFIGURATION_CONTROL_REG     0x0000
#define FPGA_RESET_CONTROL_REG             0x0004
#define DSP_RESET_COTNROL_REG              0x0008
#define FLASH_CONTROL_REG                  0x000C
#define FLASH_ADDRESS_AND_WRITE_REG        0x0010
#define FLASH_STATUS_REGISTER              0x0030
#define FLASH_MEMORY_READ_DATA             0x003C

// The following are Specific for RevE and RevF RODs
// Addresses for different FPGA configurations in flash memory
#define RCF_BEGIN 0xE00000
#define RCF_END   0xE78E63
#define FMT_BEGIN 0xE80000
#define FMT_END   0xEF8E6B
#define EFB_BEGIN 0xF00000
#define EFB_END   0xF52327
#define RTR_BEGIN 0xF80000
#define RTR_END   0xFD2327
int main(int argc, char *argv[]) {

  std::string option;

  unsigned long *buffer;

  try {
    buffer = new unsigned long[0x800];
  } catch (...) {
    std::cout << "Error reserving memory for data readout" << std::endl;
    return 1;
  }
// /home/jdopke/daq/RodDaq/RodUtils/
  std::string binFileName("prmstate.bin");  // Name of binary file to load
  ifstream binFile;                             // Pointer to binary frile
  int fileSize;                                 // Size of binary file in bytes
  
//  cout << "Enter binary file name, including extension (""q"" to quit, ""y"" to use prmstate.bin): ";
//  cin >>  option;

  binFile.open(binFileName.c_str(), ios::binary);
  if (!binFile.is_open()) {
    cout << "Unable to open binary file:" << binFileName << endl;
    exit(1);
  }
  
// Get size of file
  binFile.seekg(0, ios::end);           // go to end of file
  fileSize = binFile.tellg();          // file size given by current location
  binFile.seekg(0, ios::beg);          // go back to beginning of file
  if (fileSize != 0x2000) {
    cout << "File size is incorrect. Expected: " << dec << 0x2000 << " Found: " 
    << dec << fileSize << endl;
    exit(3);
  }

  binFile.read((char *)buffer, fileSize);
  
// Beginning to analyse the data read from file:
  std::string formatterMode[4] = {"00 = 40MHz Mode", "01 = 80MHz Mode", "10 = 160MHz Mode", "11 = 40MHz Mode (HW)"};

  cout << "################################################################################################" << endl;
  cout << "###     Program Reset Monitor Status Overview:" << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA Configuration Control Register - Value :      0x" << hex << buffer[0] << dec << endl;
  cout << "Configure Rod Controller :                         " << hex << ((buffer[0] & 0x1)==0x1 ? "Configure Rod Controller" : "Idle" ) << dec << endl;
  cout << "Configure Formatter A&B :                          " << hex << (((buffer[0]>>1) & 0x1)==0x1 ? "Configure Formatter" : "Idle" ) << dec << endl;
  cout << "Configure Formatter A&B :                          " << hex << (((buffer[0]>>2) & 0x1)==0x1 ? "Configure Formatter" : "Idle" ) << dec << endl;
  cout << "Configure Event Fragment Builder :                 " << hex << (((buffer[0]>>3) & 0x1)==0x1 ? "Configure EFB" : "Idle" ) << dec << endl;
  cout << "Configure Router :                                 " << hex << (((buffer[0]>>4) & 0x1)==0x1 ? "Configure Router" : "Idle" ) << dec << endl;
  cout << "Configure All :                                    " << hex << (((buffer[0]>>5) & 0x1)==0x1 ? "Configure All" : "Idle" ) << dec << endl;
  cout << "Configuration Override :                           " << hex << (((buffer[0]>>6) & 0x1)==0x1 ? "Active" : "Inactive" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA Reset Control Register - Value :              0x" << hex << buffer[1] << dec << endl;
  cout << "Reset Rod Controller FPGA :                        " << hex << (((buffer[1]>>0) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Formatter A :                                " << hex << (((buffer[1]>>1) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Formatter B :                                " << hex << (((buffer[1]>>2) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Event Fragment Builder :                     " << hex << (((buffer[1]>>3) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Router :                                     " << hex << (((buffer[1]>>4) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset All :                                        " << hex << (((buffer[1]>>5) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Hold all FPGAs in Reset :                          " << hex << (((buffer[1]>>6) & 0x1)==0x1 ? "Active" : "Inactive" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "DSP Reset Control Register - Value :               0x" << hex << buffer[2] << dec << endl;
  cout << "MDSP Boot Mode Select :                            " << hex << (((buffer[2]>>0) & 0x1)==0x1 ? "HPI Boot" : "ROM Boot" ) << dec << endl;
  cout << "Reset Master DSP :                                 " << hex << (((buffer[2]>>1) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Slave DSP0 :                                 " << hex << (((buffer[2]>>2) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Slave DSP1 :                                 " << hex << (((buffer[2]>>3) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Slave DSP2 :                                 " << hex << (((buffer[2]>>4) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset Slave DSP3 :                                 " << hex << (((buffer[2]>>5) & 0x1)==0x1 ? "Reset" : "Idle" ) << dec << endl;
  cout << "Reset the ROD :                                    " << hex << (((buffer[2]>>6) & 0x1)==0x1 ? "Active" : "Inactive" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "Flash Control Register - Value :                   0x" << hex << buffer[3] << dec << endl;
  cout << "Read Flash Direct VME :                            " << hex << (((buffer[3]>>0) & 0x1)==0x1 ? "Read" : "Idle" ) << dec << endl;
  cout << "Write Flash Direct VME :                           " << hex << (((buffer[3]>>1) & 0x1)==0x1 ? "Write Command" : "Idle" ) << dec << endl;
  cout << "Write Flash PRM Control :                          " << hex << (((buffer[3]>>2) & 0x1)==0x1 ? "Write Data" : "Idle" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "Flash Address and Write Data Register :            0x" << hex << buffer[4] << dec << endl;
  cout << "Address :                                          0x" << hex << (buffer[4] & 0xFFFFFF) << dec << endl;
  cout << "Data :                                             0x" << hex << ((buffer[4]>>24) & 0xFF) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Miscellaneous Status Register - Value :        0x" << hex << buffer[5] << dec << endl;
  cout << "ROD Clock Select Status :                          " << hex << (((buffer[5]>>0) & 0x1)==0x1 ? "Internal" : "BOC" ) << dec << endl;
  cout << "ROD Clock DLL Locked :                             " << hex << (((buffer[5]>>1) & 0x1)==0x1 ? "DLL Locked" : "FAIL" ) << dec << endl;
  cout << "VME Clock DLL Locked :                             " << hex << (((buffer[5]>>2) & 0x1)==0x1 ? "DLL Locked" : "FAIL" ) << dec << endl;
  cout << "ROD Busy :                                         " << hex << (((buffer[5]>>3) & 0x1)==0x1 ? "ROD Busy" : "OK" ) << dec << endl;
  cout << "ROD Type In :                                      " << hex << (((buffer[5]>>4) & 0x1)==0x1 ? "Pixel" : "SCT" ) << dec << endl;
  cout << "Formatter Type (Pixel Only) :                      " << hex << formatterMode[((buffer[5]>>5) & 0x3)] << dec << endl;
  cout << "SLink LFF (Latching, Self Clearing) :              " << hex << (((buffer[5]>>7) & 0x1)==0x1 ? "Error (FIFO FULL)" : "OK" ) << dec << endl;
  cout << "SLink LDOWN :                                      " << hex << (((buffer[5]>>8) & 0x1)==0x1 ? "Error (SLINK DOWN)" : "OK" ) << dec << endl;
  cout << "DSP0 Detect In :                                   " << hex << (((buffer[5]>>9) & 0x1)==0x1 ? "DSP0 Present" : "DSP0 Out" ) << dec << endl;
  cout << "DSP1 Detect In :                                   " << hex << (((buffer[5]>>10) & 0x1)==0x1 ? "DSP1 Present" : "DSP1 Out" ) << dec << endl;
  cout << "DSP2 Detect In :                                   " << hex << (((buffer[5]>>11) & 0x1)==0x1 ? "DSP2 Present" : "DSP2 Out" ) << dec << endl;
  cout << "DSP3 Detect In :                                   " << hex << (((buffer[5]>>12) & 0x1)==0x1 ? "DSP3 Present" : "DSP3 Out" ) << dec << endl;
  cout << "MDSP HRDY_N :                                      " << hex << (((buffer[5]>>13) & 0x1)==0x1 ? "HPI Busy" : "HPI Ready" ) << dec << endl;
  cout << "MDSP ARDY (Configuration Done) :                   " << hex << (((buffer[5]>>15) & 0x1)==0x1 ? "1" : "0" ) << dec << endl;
  cout << "IRQ ID :                                           " << hex << ((buffer[5]>>16) & 0xF) << dec << endl;
  cout << "VME AM Code :                                      " << hex << ((buffer[5]>>20) & 0x3F) << dec << endl;
// JD_DEBUG: Some more in this register, which first has to be understood
  cout << "################################################################################################" << endl;
  cout << "VME Time Out Value Register - Value :              0x" << hex << buffer[6] << dec << endl;
  cout << "      Corresponding to a timeout of :              ~" << hex << ((float) (buffer[6])*1.4/((float) (0x1CB90))) << " [ms]"<< dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Busy Histogram Control Register - Value :      0x" << hex << buffer[7] << dec << endl;
  cout << "ROD Busy Histogram Control :                       " << hex << (((buffer[7]>>0) & 0x1)==0x1 ? "Enabled" : "Disabled" ) << dec << endl;
  cout << "ROD Busy Histogram Triggered Start Enabled :       " << hex << (((buffer[7]>>1) & 0x1)==0x1 ? "Trigger" : "Auto" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA Configuration Status Register - Value :       0x" << hex << buffer[8] << dec << endl;
  cout << "ROD Controller Configuration :                     " << hex << (((buffer[8]>>0) & 0x1)==0x1 ? "Done" : "FAIL" ) << dec << endl;
  cout << "Formatter A Configuration :                        " << hex << (((buffer[8]>>1) & 0x1)==0x1 ? "Done" : "FAIL" ) << dec << endl;
  cout << "Formatter B Configuration :                        " << hex << (((buffer[8]>>2) & 0x1)==0x1 ? "Done" : "FAIL" ) << dec << endl;
  cout << "Event Fragment Builder Configuration :             " << hex << (((buffer[8]>>3) & 0x1)==0x1 ? "Done" : "FAIL" ) << dec << endl;
  cout << "Router Configuration :                             " << hex << (((buffer[8]>>4) & 0x1)==0x1 ? "Done" : "FAIL" ) << dec << endl;
  cout << "Configuration Active :                             " << hex << (((buffer[8]>>5) & 0x1)==0x1 ? "Configure Mode" : "Normal" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA Reset Status Register - Value :               0x" << hex << buffer[9] << dec << endl;
  cout << "ROD Controller Reset Signal State :                " << hex << (((buffer[9]>>0) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Formatter A Reset Signal State :                   " << hex << (((buffer[9]>>1) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Formatter B Reset Signal State :                   " << hex << (((buffer[9]>>2) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Event Fragment Builder Reset Signal State :        " << hex << (((buffer[9]>>3) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Router Reset Signal State :                        " << hex << (((buffer[9]>>4) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "DSP Reset Status Register - Value :                0x" << hex << buffer[10] << dec << endl;
  cout << "Unused ... ??? (RodMan App B) :                    " << hex << (((buffer[10]>>0) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Master DSP Reset Control Signal State :            " << hex << (((buffer[10]>>1) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Slave DSP0 Reset Control Signal State :            " << hex << (((buffer[10]>>2) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Slave DSP1 Reset Control Signal State :            " << hex << (((buffer[10]>>3) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "Slave DSP2 Reset Control Signal State :            " << hex << (((buffer[10]>>4) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
// JD_DEBUG: Rod Manual Appendix B says strange things about the next Bit...
  cout << "Slave DSP3 Reset Control Signal State :            " << hex << (((buffer[10]>>5) & 0x1)==0x1 ? "OK" : "Reset" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA INIT_N Status Register - Value :              0x" << hex << buffer[11] << dec << endl;
  cout << "ROD Controller INIT_N Signal State :               " << hex << (((buffer[11]>>0) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Formatter A INIT_N Signal State :                  " << hex << (((buffer[11]>>1) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Formatter B INIT_N Signal State :                  " << hex << (((buffer[11]>>2) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Event Fragment Builder INIT_N Signal State :       " << hex << (((buffer[11]>>3) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Router INIT_N Signal State :                       " << hex << (((buffer[11]>>4) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "Flash Status Register - Value :                    0x" << hex << buffer[12] << dec << endl;
  cout << "Read Flash Direct VME Status :                     " << hex << (((buffer[12]>>0) & 0x1)==0x1 ? "Read" : "NOP" ) << dec << endl;
  cout << "Write Flash Direct VME Status :                    " << hex << (((buffer[12]>>1) & 0x1)==0x1 ? "Write Command" : "NOP" ) << dec << endl;
  cout << "Write Flash PRM Control Status :                   " << hex << (((buffer[12]>>2) & 0x1)==0x1 ? "Write Data" : "NOP" ) << dec << endl;
  cout << "Flash Busy / Erase in Progress :                   " << hex << (((buffer[12]>>3) & 0x1)==0x1 ? "Active" : "NOP" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "FPGA Configuration Halt Status Register - Value :  0x" << hex << buffer[13] << dec << endl;
  cout << "ROD Controller Config Halt Signal State :          " << hex << (((buffer[13]>>0) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Formatter A Config Halt Signal State :             " << hex << (((buffer[13]>>1) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Formatter B Config Halt Signal State :             " << hex << (((buffer[13]>>2) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Event Fragment Builder Config Halt Signal State :  " << hex << (((buffer[13]>>3) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "Router Config Halt Signal State :                  " << hex << (((buffer[13]>>4) & 0x1)==0x1 ? "OK" : "Clear" ) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Serial Number Register - Value :               0x" << hex << buffer[14] << dec << endl;
  cout << "Serial Number :                                    0x" << hex << ((buffer[14]>>0) & 0x3FF) << dec << endl;
  cout << "FPGA Code Version :                                0x" << hex << ((buffer[14]>>12) & 0xF) << dec << endl;
  cout << "ROD Board Revision :                               0x" << hex << ((buffer[14]>>16) & 0xFF) << dec << endl;
  cout << "ROD ID :                                           0x" << hex << ((buffer[14]>>24) & 0xFF) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "Flash Data Read Register - Value :                 0x" << hex << buffer[15] << dec << endl;
  cout << "Flash Data :                                       0x" << hex << ((buffer[15]>>0) & 0xFF) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Source ID - Value :                            0x" << hex << buffer[16] << dec << endl;
  cout << "Serial Number :                                    0x" << hex << ((buffer[16]>>0) & 0xFF) << dec << endl;
  cout << "Sub-Detector ID :                                  0x" << hex << ((buffer[16]>>8) & 0xFF) << dec << endl;
  cout << "Module Type (Should be 0x0) :                      0x" << hex << ((buffer[16]>>16) & 0xFF) << dec << endl;
  cout << "Reserved (Should be 0x0) :                         0x" << hex << ((buffer[16]>>24) & 0xFF) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Manufacturer ID - Value :                      0x" << hex << buffer[17] << dec << endl;
  cout << "LBNL ID :                                          0x" << hex << ((buffer[17]>>0) & 0xFFFFFF) << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Board ID - Value (Full Register used) :        0x" << hex << buffer[18] << dec << endl;
  cout << "################################################################################################" << endl;
  cout << "ROD Revision ID - Value :                          0x" << hex << buffer[19] << dec << endl;
  cout << "ROD Revision ID :                                  0x" << hex << ((buffer[19]>>4) & 0xF) << dec << endl;
  cout << "ROD Serial Number :                                0x" << hex << ((buffer[19]>>8) & 0x3FF) << dec << endl;
  cout << "Code Version :                                     0x" << hex << ((buffer[19]>>24) & 0xFF) << dec << endl;

  
  
  

  return 0;
}
