//--------------------------------RodReset-------------------------------- 

/*! 
 *  @brief This is a utility program to Reset the ROD

 *  It 
 *    o) prompts the user for the VME slot number of the ROD.
 *    o) toggles the VME register to reset the DSPs
 *    o) waits for the reset to complete
 *    o) loads the HPIC register
 *
 *  @author John Joseph
 */

#include <iostream>
using namespace std;

#include "RodModule.h"
#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // VME map size 
  const long numSlaves=4;                       // Number of slaves

  std::string option;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }
// JMJ 25 July 2005.  Modified to only allow slots 5-12 and 14-21
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter valid ROD slot number [5-12:14-21]  (decimal):"; 
    cin >> slot;
    while ((slot < 5) || (slot > 21) || (slot == 13)) {
      cout << "Slot number out of range [5-12:14-21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  
// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);

  try {
    unsigned long int rodReset = 0x40;
    unsigned long int mdspResetStatus = 0x0;
    unsigned long int dspRegValue = 0x0;

    // mark MDSP as in unknown state
    rod0->mdspSingleWrite(MAGIC_LOCATION, 0xDEADDEAD);

    rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);

    // Poll on the MDSP Reset Status bit.  Fetch the status word and mask
    while (mdspResetStatus !=  0x2) {
      mdspResetStatus = rod0->hpiFetch(FPGA_STATUS_REG_REL_ADDR[2]) & 0x2;
    }

    // Initialize HPIC
    unsigned long hpicValue = 0x00010001;
    rod0->hpiLoad(HPIC, hpicValue);

    do {
      dspRegValue = rod0->mdspSingleRead(MAGIC_LOCATION);
      std::cout << std::hex << MAGIC_LOCATION << " = " << dspRegValue << std::dec << std::endl;
    } while(dspRegValue != I_AM_ALIVE);


  } catch (BaseException &b) {
      cout << "Exception \"initialising\" ROD:\n" << b << endl;
      exit(0);
  }
        
// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


