//! A quick utility to read a ROD's serial number register and do a sanity check.

#include <iostream>
#include <string>
#include <vector>

using namespace std;

#include <ctype.h>
#include "RodModule.h"
//#include "primParams.h" //obsolete, now everything should be inside rodConfiguration.h
#include "rodConfiguration.h"
#include "RCCVmeInterface.h"
#include "parameters.h"

void printString(string);

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  unsigned long rodSerialNumber, masked1, masked2, rodRevision, controllerVersion,
	        formatterVersion, efbVersion, routerVersion, rodMiscStatusReg, 
                fpgaConfigStatusReg, fpgaResetStatusReg, fpgaConfigHaltStatusReg, 
                rodSourceId, rodManufacturerId, rodBoardId, rodRevisionId, 
                fpgaInitNStatusRegister, dspResetStatusReg;

  VmePort *myVmePort;

  int the_mode;
  vector<string> mode(4);
  mode[0] = "40 MHz";  mode[1] = "80 MHz";  mode[2] = "160 MHz";  mode[3] = "40 MHz (HW)"; 
  
  std::string fileName(""), option;
  bool verbose = false;
  bool details = false;
  int slot = -1;
  unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'v': {
          verbose = true;
          break;
        }
		// TNV Jan. 6, 2006 added details option
		case 'd': {
		  details = true;
		  break;
		}
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }
  
// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule and initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
  myVmePort = rod0->getVmePort();

  try {
// Read the FPGA Status Register 6 (counting from 0)
    rodSerialNumber = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
    cout << "FPGA Status Reg[6] = " << hex << rodSerialNumber << endl;
    masked1 = rodSerialNumber&0xFF000000;
    masked2 = rodSerialNumber&0x000000FF;
    if (0xad000000 == masked1) {
      cout << "Sanity check passed" << endl;
      rodRevision = (rodSerialNumber&0x00FF0000)>>16;
      cout << "Board revision = " << rodRevision << endl;
      rodSerialNumber = rodSerialNumber&0x3FF;
      cout << "Board serial number = " << rodSerialNumber << endl;
    }
    else if (0x000000ad == masked2) {
      cout << "Byte order wrong." << endl;
    }
    else {
      cout << "Sanity check failed!!!" << endl;
    }

    if(verbose) {
      cout << "HPIC: 0x" << hex << rod0->hpiFetch(HPIC) << dec << endl;

      cerr << "Load HWOB to HPIC\n";
      unsigned long hpicValue = 0x00010001;
      rod0->hpiLoad(HPIC, hpicValue);
      cerr << "...done\n";

      rod0->sleep(1000);

      cerr << "Read from EMIF\n";
      // mdspSingleRead/Write use HPID not HPID++ (not allowed)
      for(unsigned long addr = 0x01800000; addr < 0x01800020; addr+=4) {
        unsigned long val = rod0->mdspSingleRead(addr);
        cerr << "0x" << hex << addr << ": 0x" << val << dec << endl;
      } 
      cerr << "...done\n";
    }

	if(details) {
	  try {
	    rod0->initialize();
	  }
	  catch (HpiException &h) {
	    cout << h;
	    return 1;
	  }
	  // FPGA Versions:
	  cout << endl;
	  cout << "--------------- FPGA Versions ---------------" << endl;
	  controllerVersion = 0x0fff & (rod0->mdspSingleRead(0x0040440c));
	  cout << "ROD Controller: " << hex << controllerVersion << endl;
	  
	  formatterVersion = 0x0fff & (rod0->mdspSingleRead(0x00400088));
	  cout << "ROD Formatter:  " << hex << formatterVersion << endl;

	  efbVersion = 0x0fff & (rod0->mdspSingleRead(0x0040221c));
	  cout << "ROD EFB:        " << hex << efbVersion << endl;

	  routerVersion = 0x0fff & (rod0->mdspSingleRead(0x0040250c));
	  cout << "ROD Router:     " << hex << routerVersion << endl;
	  	  
	  // Additional details:
	  cout << endl;
	  cout << "--------- ROD Misc. Status Register ---------" << endl;
	  // rodMiscStatusReg
	  rodMiscStatusReg = myVmePort->read32(FPGA_CONTROL_REG_REL_ADDR[5]);
	  cout << "ROD Clock Select Status: ";
	  (0x00000001 & rodMiscStatusReg) ? printString("Internal") : printString("BOC");
	  cout << "ROD Clock DLL Locked:    ";
	  (0x00000002 & rodMiscStatusReg) ? printString("DLL Locked") : printString("Fail"); 
	  cout << "VME Clock DLL Locked:    ";
	  (0x00000004 & rodMiscStatusReg) ? printString("DLL Locked") : printString("Fail");
	  cout << "ROD busy status:         ";
	  (0x00000008 & rodMiscStatusReg) ? printString("ROD Busy") : printString("OK");
	  cout << "ROD Type In:             ";
	  (0x00000010 & rodMiscStatusReg) ? printString("Pixel") : printString("SCT");
	  if (0x00000010 & rodMiscStatusReg){
	    the_mode = (0x00000060 & rodMiscStatusReg) >> 5;
	    cout << "Formatter Type In:    " << mode[the_mode] << endl;
	  }
	  cout << "S-Link LFF:              ";
	  (0x00000080 & rodMiscStatusReg) ? printString("Error") : printString("OK");
	  cout << "S-Link LDOWN:            ";
	  (0x00000100 & rodMiscStatusReg) ? printString("Error") : printString("OK");
          cout << "SDSP-0 Present Status:   ";
	  (0x00000200 & rodMiscStatusReg) ? printString("Present") : printString("Out");
	  cout << "SDSP-1 Present Status:   ";
	  (0x00000400 & rodMiscStatusReg) ? printString("Present") : printString("Out");
	  cout << "SDSP-2 Present Status:   ";
	  (0x00000800 & rodMiscStatusReg) ? printString("Present") : printString("Out");
	  cout << "SDSP-3 Present Status:   ";
	  (0x00001000 & rodMiscStatusReg) ? printString("Present") : printString("Out");
	  cout << "MDSP HRDY_N:             ";
	  (0x00002000 & rodMiscStatusReg) ? printString("HPI Busy") : printString("HPI Ready"); 
	  cout << "Configuration Done:      ";
	  (0x00008000 & rodMiscStatusReg) ? printString("YES") : printString("NO");
	  cout << "IRQ ID:                  " << ((0x000f0000 & rodMiscStatusReg) >> 16) << endl;
	  cout << "VME AM Code:             " << ((0x03f00000 & rodMiscStatusReg) >> 20) << endl;
	  cout << endl;
	  
	  // fpgaConfigStatusReg
	  cout << "-------- FPGA Config Status Register --------" << endl;
	  fpgaConfigStatusReg = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[0]);
	  cout << "ROD Controller FPGA Configuration Done: ";
	  (0x00000001 & fpgaConfigStatusReg) ? printString("Done") : printString("Fail");
	  cout << "Formatter A Configuration Done:         ";
	  (0x00000002 & fpgaConfigStatusReg) ? printString("Done") : printString("Fail");
	  cout << "Formatter B Configuration Done:         ";
	  (0x00000004 & fpgaConfigStatusReg) ? printString("Done") : printString("Fail");
	  cout << "EFB Configuration Done:                 ";
	  (0x00000008 & fpgaConfigStatusReg) ? printString("Done") : printString("Fail");
	  cout << "Router Configuration Done:              ";
	  (0x00000010 & fpgaConfigStatusReg) ? printString("Done") : printString("Fail");
	  cout << "Configuration Active:                   ";
	  (0x00000020 & fpgaConfigStatusReg) ? printString("Configure Mode") : printString("Normal");
	  cout << endl;

	  // fpgaResetStatusReg
	  cout << "-------- FPGA Reset Status Register ---------" << endl;
	  fpgaResetStatusReg = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[1]);
	  cout << "ROD Controller FPGA Reset Control Signal State: ";
	  (0x00000001 & fpgaResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Formatter A Reset Control Signal State:         ";
	  (0x00000002 & fpgaResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Formatter B Reset Control Signal State:         ";
	  (0x00000004 & fpgaResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "EFB Reset Control Signal State:                 ";
	  (0x00000008 & fpgaResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Router Reset Control Signal State:              ";
	  (0x00000010 & fpgaResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << endl;

	  // dspResetStatusReg
	  cout << "--------- DSP Reset Status Register ---------" << endl;
	  dspResetStatusReg = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[2]);
	  //cout << "(Not Used):                            ";
	  //(0x00000001 & dspResetStatusReg) ? printString("OK") : printString("Reset") << endl;
	  cout << "Master DSP Reset Control Signal State: ";
	  (0x00000002 & dspResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Slave DSP0 Reset Control Signal State: ";
	  (0x00000004 & dspResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Slave DSP1 Reset Control Signal State: ";
	  (0x00000008 & dspResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Slave DSP2 Reset Control Signal State: ";
	  (0x00000010 & dspResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << "Slave DSP3 Reset Control Signal State: ";
	  (0x00000020 & dspResetStatusReg) ? printString("OK") : printString("Reset");
	  cout << endl;

	  // fpgaInitNStatusRegister
	  cout << "-------- FPGA INIT_N Status Register --------" << endl;
	  fpgaInitNStatusRegister = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[3]);
	  cout << "ROD Controller FPGA INIT_N Control Signal State: ";
	  (0x00000001 & fpgaInitNStatusRegister) ? printString("OK") : printString("Clear");
	  cout << "Formatter A INIT_N Control Signal State:         ";
	  (0x00000002 & fpgaInitNStatusRegister) ? printString("OK") : printString("Clear");
	  cout << "Formatter B INIT_N Control Signal State:         ";
	  (0x00000004 & fpgaInitNStatusRegister) ? printString("OK") : printString("Clear");
	  cout << "EFB INIT_N Control Signal State:                 ";
	  (0x00000008 & fpgaInitNStatusRegister) ? printString("OK") : printString("Clear");
	  cout << "Router INIT_N Control Signal State:              ";
	  (0x00000010 & fpgaInitNStatusRegister) ? printString("OK") : printString("Clear");
	  cout << endl;

	  // fpgaConfigHaltStatusReg
	  cout << "------ FPGA Config Halt Status Register -----" << endl;
	  fpgaConfigHaltStatusReg = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[5]);
	  cout << "ROD Controller FPGA Configuration Halt Signal State: ";
	  (0x00000001 & fpgaConfigHaltStatusReg) ? printString("Fail") : printString("OK");
	  cout << "Formatter A Configuration Halt Signal State:         ";
	  (0x00000002 & fpgaConfigHaltStatusReg) ? printString("Fail") : printString("OK");
	  cout << "Formatter B Configuration Halt Signal State:         ";
	  (0x00000004 & fpgaConfigHaltStatusReg) ? printString("Fail") : printString("OK");
	  cout << "EFB Configuration Halt Signal State:                 ";
	  (0x00000008 & fpgaConfigHaltStatusReg) ? printString("Fail") : printString("OK");
	  cout << "Router Configuration:                                "; 
	  (0x00000010 & fpgaConfigHaltStatusReg) ? printString("Fail") : printString("OK");
	  cout << endl;

	  // rodSerialNumber
	  cout << "-------- ROD Serial Number Register --------" << endl;
	  rodSerialNumber = myVmePort->read32(FPGA_STATUS_REG_REL_ADDR[6]);
	  cout << "Serial Number:      " <<  (0x000003ff & rodSerialNumber) << endl;
	  cout << "FPGA Code Version:  " << ((0x0000f000 & rodSerialNumber) >> 12) << endl;
	  cout << "ROD Board Revision: " << ((0x00ff0000 & rodSerialNumber) >> 16) << endl;
	  cout << "ROD ID:             " << ((0xff000000 & rodSerialNumber) >> 24) << endl;

// 	  // ATLAS/CERN defined ID Registers:
// 	  cout << endl;
// 	  cout << "-------- ATLAS/CERN-defined ID Info ---------" << endl;
// 	  // rodSourceId
// 	  rodSourceId = myVmePort->read32(ATLAS_DEF_ID_REG_REL_ADDR[0]);
// 	  cout << "rodSourceId = " << rodSourceId << endl;
// 	  cout << "ATLAS_DEF_ID_REG_REL_ADDR[0] = " << ATLAS_DEF_ID_REG_REL_ADDR[0] << endl;
// 	  cout << "Serial Number:     " <<  (0x000000ff & rodSourceId) << endl;
// 	  cout << "Sub-detector ID:   " << ((0x0000ff00 & rodSourceId) >> 8) << endl;
// 	  cout << "Module Type:       " << ((0x00ff0000 & rodSourceId) >> 16) << endl;
// 	  cout << "Reserved:          " << ((0xff000000 & rodSourceId) >> 24) << endl;

// 	  // rodManufacturerId
// 	  rodManufacturerId = myVmePort->read32(ATLAS_DEF_ID_REG_REL_ADDR[1]);
// 	  cout << "LBNL ID:           " << (0x00ffffff & rodManufacturerId) << endl;

// 	  // rodBoardId
// 	  rodBoardId = myVmePort->read32(ATLAS_DEF_ID_REG_REL_ADDR[2]);
// 	  cout << "ROD Board ID:      " << rodBoardId << endl;

// 	  // rodRevisionId
// 	  rodRevisionId = myVmePort->read32(ATLAS_DEF_ID_REG_REL_ADDR[3]);
// 	  cout << "(Not Used):        " <<  (0x0000000f & rodRevisionId) << endl;
// 	  cout << "ROD Revision ID:   " << ((0x000000f0 & rodRevisionId) >> 4) << endl;
// 	  cout << "ROD Serial Number: " << ((0x0003ff00 & rodRevisionId) >> 8) << endl;
// 	  cout << "Code Version:      " << ((0xff000000 & rodRevisionId) >> 24) << endl;
	}

  } catch(BaseException &b) {
    cout << "Exception touching ROD:\n" << b << endl;
  }

// Delete the ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;  
}

void printString(string blah){
  std::cout << blah << std::endl;
}

