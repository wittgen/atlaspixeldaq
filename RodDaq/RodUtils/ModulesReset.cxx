#include <iostream>
using namespace std;

#include "RodModule.h"
#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

  using namespace SctPixelRod;

  const unsigned long mapSize=0xc00040;         // VME map size 
  const long numSlaves=4;                       // Number of slaves

  int slot = -1;
  unsigned long baseAddress;
  //Get the Slot Number

  if (argc > 1) {
    slot = atoi(argv[1]);
    if ((slot < 5) || (slot > 21) || (slot == 13)) {
      cout <<"Wrong slot number valid slots are 5-12 and 14-21"<<endl;
      return 1;
    }
  }



  cout <<std::hex<<std::endl;
  cout <<"All Slots displayed in hex."<<std::endl;

  for(int useslot =5; useslot<22; useslot++)
    {
      if(slot == useslot || slot ==-1)
	{
	  
	  RCCVmeInterface *vme1 = new RCCVmeInterface();	    
	  int theslot = useslot;
	  baseAddress = theslot << 24;
	  // Create VME interface
	  
	  cout <<"Running on slot number:  "<<useslot<<"aaa   "<<baseAddress<<" mapSize" << mapSize<< std::endl;  
	  // Create RodModule and initialize it
	  RodModule* m_rod = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
	  if (useslot !=13)
	    {
	      if(m_rod->verify())
		{
		  cout <<"The following Rod has been found:  "<<useslot<< std::endl;
		  try{
		    m_rod->initialize(false);
		  }
		  catch (HpiException &h) {
		    cout << h;
		  }

		  int packSize = 1;
		  int pLen = sizeof(SendSerialStreamIn)/sizeof(uint32_t);
		  uint32_t *tsdData = new uint32_t[pLen+packSize];
		  tsdData[0+pLen] = 0x16ba0f0;
		  
		  SendSerialStreamIn *sendSerialStreamIn = (SendSerialStreamIn*)tsdData;
		  sendSerialStreamIn->sportId  = 0;
		  sendSerialStreamIn->nBits    = 32;
		  sendSerialStreamIn->fCapture = 1;
		  sendSerialStreamIn->nCount   = 1;
		  sendSerialStreamIn->dataPtr  = 0;
		  sendSerialStreamIn->maskLo   = 0xfffffff;
		  sendSerialStreamIn->maskHi   = 0xffff;
		  RodPrimitive* sendStream;
		  sendStream = new RodPrimitive(packSize+pLen, 0, SEND_SERIAL_STREAM, 0, (int*)sendSerialStreamIn);
		  //sleep(1);
		  if (useslot !=13)
		  {
		    m_rod->executeMasterPrimitiveSync(*sendStream);
		  }
		  delete sendStream;
		  delete[] tsdData;
		  delete m_rod;  		 
		  delete vme1;
		}
	      else
		{
		  cout <<"The following Slot does not seem to contain a rod:  "<<useslot<< std::endl;
		}
	    } 
	  


	}
      
    }

}
