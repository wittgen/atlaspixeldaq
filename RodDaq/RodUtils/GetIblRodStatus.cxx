/* Test of the RodStatus class
 * written by Fredrik Tegenfeldt <fredrik.tegenfeldt@cern.ch>
 * 
 * $Id: GetIblRodStatus.cxx 9996 2012-08-31 08:56:06Z akugel $
 */

#include <iostream>
using namespace std;

#include <ctype.h>

#include "RodModule.h"
#include "RodStatus.h"

#include "NetRodModule.h"
#include "NetVmeInterface.h"

#include "RCCVmeInterface.h"
#include "parameters.h"

#include "iblSlaveCmds.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  std::string fileName(""), option;
  bool verbose = false;
  int slot = -1;
  unsigned long baseAddress;
  bool sbcVme = false;
  std::string rodIp = "127.0.0.1";

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'm': {
          sbcVme = true;
          break;
        }
        case 'n': {
          sbcVme = false;
          break;
        }
        case 'v': {
          verbose = true;
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());

          if((slot < 1) || (slot > 21)) {
            cout << "Slot number out or range [1:21] (input: " << slot << ")\n";
            return 0;
          }

          break;
        }
        default: {
          break;
        }
      }
    }
  }
  
// Prompt for slot number
  if (slot < 0 && sbcVme) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;
  if(!sbcVme){
    cout << "Enter ROD IP:";
    cin >> rodIp; 
  }

  // Create VME interface and RodModule
  VmeInterface *vme = 0;
  AbstrRodModule* rod = 0;
  RodStatus *rodStatus = 0;
  const int numIblSlaves=0;

  try {
    if(sbcVme){
      vme = new RCCVmeInterface();
      rod = new RodModule(baseAddress, mapSize, *vme, numIblSlaves);
    } else{
      vme = new NetVmeInterface(rodIp.c_str());
      rod= new NetRodModule(baseAddress, mapSize, *vme, numIblSlaves);
    }
    rod->initialize(false);
    // RodStatus currently doesn't work for net-version
    rodStatus = 0;
    if(sbcVme) rodStatus = new RodStatus(*rod);
  } catch (RodException &r) {
    std::cout << __FUNCTION__ 
	      << "ROD exception thrown when creating the RodModule/RodStatus objects" 
	      << std::endl
	      << r;
  } catch (VmeException &v) {
    std::cout << __FUNCTION__ 
	      << ": VME exception thrown when creating the RodModule/RodStatus objects\n "
	      << v
	      << "RodModule not found on " << (sbcVme?"VME":"network") 
	      <<" (wrong slot number/IP or h/w error)!"
	      << std::endl;
  } catch (...) {
    std::cout << __FUNCTION__ 
	      << ": Some whatever exception occured..." 
	      << std::endl;
  }

  /* If rodStatus couldn't been created, then we skip the attempt to 
   * reset the module */
  if (rodStatus) {
    if (!rodStatus->isSane()) {
      std::cout << "Looks like we have an insane ROD module on our hands..." 
        << std::endl;
      std::cout << "With an initialize() it will be more happy." << std::endl;
    }

    try {
      std::cout << "First doing initialize()" << std::endl;
      //rod->initialize(false);
      rod->initialize(true);  
      exit(0);
      std::cout << "Secondly, make the snapshot of the status" << std::endl;
      rodStatus->snapShot(*rod);
      std::cout << "Done." << std::endl;
    } catch (BaseException &e) {
      std::cout << __FUNCTION__ 
        << ": Exception when initializing RodModule:\n ";
      e.what(std::cout);
      std::cout << std::endl;
    } catch (...) {
      std::cout << "An exception was generated when initializing the RodModule." 
        << std::endl;
    }

    if (!rodStatus->isSane()) {
      std::cout << "Still insane... check the slotnumber." << std::endl;
    } else {
      std::cout << "We've got a live one!" << std::endl;
      std::cout << "Print the status:" << std::endl;
      std::cout << *rodStatus;
      std::vector<unsigned int> buffer;
      unsigned long * buffer1 = new unsigned long[sizeof(CommRegs)/sizeof(int)];
      CommRegs  master_CommRegs;
      //      CommRegs slave_CommRegs[4];
      int i;//,j;
      try {  
	rod->mdspBlockRead((unsigned long)MAGIC_MASTER_LOCATION,buffer1,sizeof(master_CommRegs)/sizeof(int));		      
	for(i=0;i<(int)(sizeof(CommRegs)/sizeof(int));i++)
	  ((unsigned int*) &master_CommRegs)[i]=buffer1[i];
	//for(i=0;i<4;i++) {
	//  rod->readSlaveMem(i,(int) MAGIC_SLAVE_LOCATION,sizeof(CommRegs)/sizeof(int),buffer);  
	//  for(j=0;j<sizeof(CommRegs)/sizeof(int);j++)
	//    ((unsigned int*) &slave_CommRegs[i])[j]=buffer[j];
	//}
      }
      catch (HpiException &h) {
	cout << h;
      }
      char *s;
      cout << "==================== MASTER VERSION ======================" <<endl;
      s=(char *)&master_CommRegs.scratch[0];
      cout << "Image MD5 sum: ";
      for(i=0;i<32;i++) {
	cout << s[i];
      } 
      cout <<endl;
      cout << "Link Time: ";
      s=(char *)&master_CommRegs.scratch[8];
      for(i=0;i<12;i++) {
	cout << s[i];
      } 
      cout <<endl;
      cout << "CVS Tag: ";
      s=(char *)&master_CommRegs.scratch[11];
      i=0;
      while (s[i] && (i <64) ) {
	cout << s[i];
	i++;
      } 
      cout << endl;
//      for(j=0;j<4;j++) {
// 	cout << "=============== SLAVE Number " << j << ": " << " Version =================" << endl;
// 	s=(char *) &slave_CommRegs[j].scratch[0];
// 	cout << "Image MD5 sum: ";
// 	for(i=0;i<32;i++) {
// 	  cout << s[i];
// 	} 
// 	cout <<endl;
// 	cout << "Link Time: " ;
// 	s=(char *)&slave_CommRegs[j].scratch[8];
// 	for(i=0;i<12;i++) {
// 	  cout << s[i];
// 	} 
// 	cout <<endl;
// 	cout << "CVS TAG: ";
// 	s=(char *)&slave_CommRegs[j].scratch[11];
// 	i=0;
// 	while (s[i] && (i <64) ) {
// 	  cout << s[i];
// 	  i++;
// 	} 
// 	cout <<endl;
//       }
    }
  } /* if(RodStatus) */
  else{
    for (int slave = 0; slave < 2 ; slave++){
      std::cout << "Slave " << slave << " ----------" << std::endl;
      // copied from histTest
      IblSlvNetCfg *slvNetCmd;
      // we need an outlist too
      unsigned long *outBody;
      iblSlvStat *rodStatus;
      SendSlavePrimitiveIn *slvCmdPrim;
      IblSlvRdWr *slvCmd;
      RodPrimitive *sendSlvCmdPrim;
      // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
      uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
      // we need an outlist too
      RodOutList* outList;
      iblSlvStat *rodStatus2;
      slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
      slvCmdPrim->slave = slave; // this is a number, not a mask
      slvCmdPrim->primitiveId = 0;	// ignored
      slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
      slvCmdPrim->fetchReply = 1;
      slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
      slvCmd->cmd = SLV_CMD_STAT;
      slvCmd->addr = 0;
      slvCmd->count = 1;
      slvCmd->data = SLV_STAT_TYPE_STD;
      sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
					1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
      rod->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
      rodStatus2 = (iblSlvStat*) outList->getMsgBody(1);
      cout << "Status: program type     " << hex << rodStatus2->progType  << endl;
      cout << "Status: program version  " << hex << rodStatus2->progVer  << endl;
      cout << "Status: status value     " << hex << rodStatus2->status  << endl;
      rod->deleteOutList();
      delete sendSlvCmdPrim;
      free(slvPrimBuf);
    }
  }
  
// Delete the ROD and VME objects before exiting
  delete rod;
  delete rodStatus;
  delete vme;

  return 0;  
}



