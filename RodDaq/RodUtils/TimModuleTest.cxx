//File: TimModuleTest.cxx

/*! \file
 * \brief Test TimModule
 *
 * This is a prototype test program for TimModule.
 *
 * Contributors: John Lane <jbl@hep.ucl.ac.uk> - originator
 *
 * $Id$
 *
 * $Log$
 * Revision 1.2  2004/07/29 20:08:56  jbl
 * TimModule minor bug fixes
 *
 * Revision 1.1  2003/10/30 16:42:06  sctpixel
 * Moved all RodTests files to RodUtils
 *
 * Revision 1.4  2003/09/11 14:44:21  pixeldaq
 * Removed path info from #includes
 *
 * Revision 1.3  2002/12/11 21:30:50  jbl
 * TimModule major update
 *
 */

#include <iostream>
#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

void test();

int main() {

  try {
    test();
  }
  catch (bad_alloc) {
    cout << "Caught bad_alloc" << endl;
  }
  catch (exception) {
    cout << "Caught exception" << endl;
  }
  catch (TimException x) {
    cout << "Caught TimException: " << x.getDescriptor() << " ";
    cout << x.getData1() <<     " " << x.getData2() << endl;
  }
  catch (VmeException x) {
    cout << "Caught VmeException:" << endl;

    cout << "Vme ErrorClass " <<        x.getErrorClass() << endl;
    cout << "Vme ErrorCode  " << hex << x.getErrorCode()  << endl;

    VmePort *p = x.getPort();

   if (p == 0) {
    cout << "VmePort object missing" << endl;
   } else {
    cout << "Vme ErrorMess " << p->getErrorMessage( x.getErrorCode() ) << endl;
    cout << "Vme BusErrors " << p->getBusErrors() << endl;
    cout << "Vme LastCode  " << hex << p->getLastErrcode() << endl;
    cout << "Vme ErrorMess " << p->getErrorMessage( p->getLastErrcode() );
    cout << endl;
   }
  }
  catch (...) {
    cout << "Caught unknown exception" << endl;
  }

  return 0;
}

void test() {

  const uint32_t baseAddr = 0x0D000000;  // VME base address for TIM slot 13
  const uint32_t  mapSize =    0x10000;  // VME window size

  VmeInterface *vme = new RCCVmeInterface();  // Create VME interface

  TimModule    *tim = new TimModule( baseAddr, mapSize, *vme );  // Create tim

  if (vme == 0 || tim == 0) cout << "Object missing" << vme << tim << endl;

  tim->reset();
  tim->initialize();

  hex(cout);
  cout << "serial " << tim->getSerialNumber()          << endl;
  cout << "TIM ID " << tim->regFetch( TIM_REG_TIM_ID ) << endl;

  tim->issueCommand( TIM_VTRG );
  tim->issueCommand( TIM_VTRG );
  int l1id = tim->fetchL1ID();
  if (l1id != 1) cout << "ERROR: L1ID " << l1id << endl;

  tim->issueVCAL( 99 );

  tim->intTrigStart( 10.0 ); // kHz
  tim->intTrigStop();

  const int size = 10;
  short unsigned buffer[ TIM_SEQ_SIZE ];
  int i;
  for (i = 0; i < size; i++) buffer[i] = i << 8;
  buffer[4] = TIM_BCR;

  tim->seqLoad(  size, buffer );
  tim->seqRun(   size );
  tim->seqFetch( size, buffer );

  cout << "buffer:" << endl;
  for (i = 0; i < size; i++) {
    cout << " " << buffer[i];
  }
  cout << endl;

  tim->status();

  cout << *tim;

  cout << "soak test..." << endl;
  for (i = 0; i < 999999; i++) {
    tim->seqLoad(  size, buffer );
    tim->seqRun(   size );
    tim->seqFetch( size, buffer );
  }

  delete tim;
  delete vme;

  cout << "Let's provoke a crash!" << endl;
  vme = 0;
  TimModule( baseAddr, mapSize, *vme );
}
