/* Allows access to the registers within the FPGAs on the old Pixel RODs
 * 
 */

#include <iostream>
using namespace std;

#include <ctype.h>
//#include <sys/types.h>
//#include <sys/stat.h>
#include <iomanip>

#include "RodModule.h"
#include "RodStatus.h"

#include "RCCVmeInterface.h"

const unsigned long mapSize=0xc00040;         // Map size
const long numSlaves=4;                       // Number of slaves
SctPixelRod::RodModule *rod = NULL;

struct command {
    string name;
    string desc;
    void (*func)(string);
};

std::vector<command> commands;

void printHelp()
{
  cout << "Available commands:" << endl;
  
  for(uint32_t i=0; i<commands.size(); i++)
  {
    cout << left << setfill(' ') << setw(12) << commands.at(i).name;
    cout << commands.at(i).desc << endl;
  }
}

void rdBlock(string cmd)
{
  uint32_t size;
  uint32_t addr;
  
  if( cmd.length() <= string("rdBlock").length())
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
  istringstream ss( cmd.substr(string("rdBlock").length() + 1) );
  ss.unsetf(std::ios_base::basefield);
  
  if( !(ss >> addr) )
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
  if( !(ss >> size) )
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
  uint32_t *rdBuffer = new uint32_t [size];
  
  rod->mdspBlockRead(addr, rdBuffer, size);
  
//  if(size <= 64)
  {
    cout << "the following block has been read from address 0x" << hex << addr << " :" << endl;
    
    for(uint32_t i=0; i< size; i++)
      cout << right << hex << setfill('0') << "0x" << setw(8) << addr+4*i << " : " << "0x" << setw(8) << rdBuffer[i] << endl;
    
    cout << endl;
  }
  delete[] rdBuffer;
}

void wrSingle(string cmd)
{
  uint32_t addr;
  uint32_t value;
  
  if( cmd.length() <= string("wrSingle").length())
  {
    cout << "Please specify valid address and data!" << endl;
    return;
  }
  
  istringstream ss( cmd.substr(string("wrSingle").length() + 1) );
  ss.unsetf(std::ios_base::basefield);
  
  if( !(ss >> addr) )
  {
    cout << "Please specify valid address and data!" << endl;
    return;
  }
  
  if( !(ss >> value) )
  {
    cout << "Please specify valid address and data!" << endl;
    return;
  }
  
  rod->mdspSingleWrite(addr,value);
  
  cout << "Data written." << endl;
}

void rdSingle(string cmd)
{
  uint32_t addr;
  uint32_t value;
  
  if( cmd.length() <= string("rdSingle").length())
  {
    cout << "Please specify a valid address to read from!" << endl;
    return;
  }
  
  istringstream ss( cmd.substr(string("rdSingle").length() + 1) );
  ss.unsetf(std::ios_base::basefield);
  
  if( !(ss >> addr) )
  {
    cout << "Please specify a valid address to read from!" << endl;
    return;
  }
  
  value  = rod->mdspSingleRead(addr);
           
  cout << right << hex << "address 0x" << setfill('0') << setw(8) << addr << " contains: 0x" << setw(8) << value << endl;
}

void rdFifo(string cmd)
{
  uint32_t size;
  uint32_t addr;
  uint32_t value;
  
  if( cmd.length() <= string("rdFifo").length())
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
  istringstream ss( cmd.substr(string("rdFifo").length() + 1) );
  ss.unsetf(std::ios_base::basefield);
  
  if( !(ss >> addr) )
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
  if( !(ss >> size) )
  {
    cout << "Please specify valid address and data amount!" << endl;
    return;
  }
  
//  uint32_t *rdBuffer = new uint32_t [size];
  
//  rod->mdspBlockRead(addr, rdBuffer, size);
  
//  if(size <= 64)
  {
    cout << "the following block has been read from address 0x" << hex << addr << " :" << endl;
    
    for(uint32_t i=0; i< size; i++)
    {
      value  = rod->mdspSingleRead(addr);
      cout << right << hex << setfill('0') << "0x" << setw(8) << 4*i << " : " << "0x" << setw(8) << value << endl;
    }
    
    cout << endl;
  }
//  delete[] rdBuffer;
}

void initCommands()
{
  command cmd;
  
  cmd.name = "help";
  cmd.desc = "Prints this command overview";
  cmd.func = NULL;
  commands.push_back(cmd);
  
  cmd.name = "exit";
  cmd.desc = "Terminates the program";
  cmd.func = NULL;
  commands.push_back(cmd);
  
  cmd.name = "quit";
  cmd.desc = "Terminates the program";
  cmd.func = NULL;
  commands.push_back(cmd);
  
/*
  cmd.name = "wrBlock";
  cmd.desc = "writes a block to the ROD, parameters are address, size and mode. Valid modes are 0: all set to zero, 1: all set to one, 2: ascending numbered";
  cmd.func = wrBlock;
  commands.push_back(cmd);
*/
  
  cmd.name = "rdBlock";
  cmd.desc = "reads a block from the ROD, parameters are address and size";
  cmd.func = rdBlock;
  commands.push_back(cmd);
  
  cmd.name = "wrSingle";
  cmd.desc = "writes a single 32bit word to a given address, parameters are address and value";
  cmd.func = wrSingle;
  commands.push_back(cmd);
  
  cmd.name = "rdSingle";
  cmd.desc = "reads a single 32bit word from a given address, parameter is address";
  cmd.func = rdSingle;
  commands.push_back(cmd);
  
  cmd.name = "rdFifo";
  cmd.desc = "reads a fifo from the ROD, parameters are address and size";
  cmd.func = rdFifo;
  commands.push_back(cmd);
  
}


int main(int argc, char *argv[])
{
/*  string cmdFifo = "/tmp/RodRegAccessCmdFifo";
  string retFifo = "/tmp/RodRegAccessRetFifo";
  
  int ret = mkfifo(cmdFifo.c_str(), 0666);
  
  if(ret != 0 )
  {
    cout << "Unable to create command fifo!";
    return ret;
  }
  
  ret = mkfifo(retFifo.c_str(), 0666);
  if(ret != 0 )
  {
    cout << "Unable to create return fifo!";
    unlink(cmdFifo.c_str());
    return ret;
  }*/
  
  
  using namespace SctPixelRod;
  
  std::string option, cmd;
//  bool verbose = false;
  int32_t slot = -1;
  uint32_t baseAddress;
  bool cmdGiven = false;

  if (argc > 1)
  {
    for (int i=1; i<argc; i++)
    {
      option = argv[i];
      if (option[0] != '-')
        break;
      
      switch (option[1])
      {
/*        case 'v':
        {
          verbose = true;
          break;
        }
*/
        case 's':
        {
          slot = atoi(option.substr(2).c_str());

          if((slot < 1) || (slot > 21))
          {
            cout << "Slot number out or range [1:21] (input: " << slot << ")\n";
            return 0;
          }

          break;
        }
        
        case 'c':
        {
          i++;
          while(i < argc)
          {
            if(argv[i][0] == '-')
              break;
            cmd.append(argv[i++]);
            cmd.append(" ");
          }
          cmdGiven = true;
          break;
        }
        
        default:
        {
          break;
        }
      }
    }
  }
  
// Prompt for slot number
  if (slot < 0 )
  {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21))
    {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme;
  try
  {
    vme = new RCCVmeInterface();
  }
  catch (...)
  {
    std::cout << "Caught an exception when creating a VME interface." << std::endl;
  }
  
// Create RodModule
//  RodModule *rod = NULL;
  RodStatus *rodStatus = NULL;
  try
  {
    rod = new RodModule(baseAddress, mapSize, *vme, numSlaves);
    rodStatus = new RodStatus(*rod);
  }
  catch (RodException &r)
  {
    std::cout << __FUNCTION__ 
      << "ROD exception thrown when creating the RodModule/RodStatus objects" 
      << std::endl
      << r;
  }
  catch (VmeException &v)
  {
    std::cout << __FUNCTION__ 
      << ": VME exception thrown when creating the RodModule/RodStatus objects\n "
      << v
      << "RodModule not found on VME (wrong slot number or h/w error)!"
      << std::endl;
  }
  catch (...)
  {
    std::cout << __FUNCTION__ 
      << ": Some whatever exception occured..." 
      << std::endl;
  }

  /* If rodStatus couldn't been created, then we skip the rest */
  if (rodStatus)
  {
    if (!rodStatus->isSane())
    {
      std::cout << "Looks like we have an insane ROD module on our hands..." 
        << std::endl;
      std::cout << "With an initialize() it will be more happy." << std::endl;
    }

    try
    {
      std::cout << "First doing initialize()" << std::endl;
      rod->initialize(false);
      std::cout << "Secondly, make the snapshot of the status" << std::endl;
      rodStatus->snapShot(*rod);
      std::cout << "Done." << std::endl;
    }
    catch (BaseException &e)
    {
      std::cout << __FUNCTION__ 
        << ": Exception when initializing RodModule:\n ";
      e.what(std::cout);
      std::cout << std::endl;
    }
    catch (...)
    {
      std::cout << "An exception was generated when initializing the RodModule." 
        << std::endl;
    }


    if (!rodStatus->isSane())
    {
      std::cout << "Still insane... check the slotnumber." << std::endl;
    }
    else
    {
      std::cout << "We've got a live one!" << std::endl;
      std::cout << "Print the status:" << std::endl;
      std::cout << *rodStatus;
      
/*      
      fstream cfd, rfd;
      cout << "Lets open the first Fifo.." << endl;
      cfd.open(cmdFifo,ifstream::in);
      cout << "Lets open the second Fifo.." << endl;
      rfd.open(retFifo,ifstream::out);
*/      
//      if(!cfd.good() || !rfd.good())
//        cout << "Can't open command and/or return fifo" << endl;
//      else
      {
//        cout << "Fifos successfully opened!" << endl;
//        string cmd;
//        uint32_t addr;
        uint32_t cmdCount = 0;
        initCommands();
        
/*        if(cmdGiven)
        {
          cout << "command given: " << cmd << endl;
          cmd.clear();
          cmdGiven = false;
        }
*/        
        try
        {
          while(1)
          {
            cout << dec << cmdCount++ << " > ";
            if(cmdGiven)
            {
              cout << cmd << endl;
              cmdGiven = false;
            }
            else
            {
              getline(cin,cmd);
            }
            
            if(cmd.find("exit") == 0 || cmd.find("quit") == 0)
              break;
            
             if(cmd.find("help") == 0)
             {
               printHelp();
               continue;
             }
              
            for(uint32_t i=0; i<commands.size(); i++)
            {
              if(cmd.find(commands.at(i).name) == 0)
              {
                commands.at(i).func(cmd);
                break;
              }
            }
            
/*
            getline(cfd, cmd);
            if(cmd.find("exit") == 0 || cmd.find("quit") == 0)
              break;
            
            if(cmd.find("rdSingle") == 0)
            {
              istringstream ss( cmd.substr(string("read").length() + 1) );
              ss.unsetf(std::ios_base::basefield);
              
              if( !(ss >> addr) )
              {
                cout << "Please specify a valid address to read from!" << endl;
                continue;
              }
  
//              cout << hex << rod->mdspSingleRead(addr) << endl;
              continue;
            }
            
            if(cmd.find("rdBlock") == 0)
            {
              continue;
            }
            
            if(cmd.find("wrSingle") == 0)
            {
              continue;
            }
            
            if(cmd.find("wrBlock") == 0)
            {
              continue;
            }
            
            if (is.eof())                      // check for EOF
            {
              cout << "[EoF reached]\n";
              break;
            }
*/
          }
        }
        catch (HpiException &h)
        {
    	    cout << h;
        }
        
        cout << "Bye!" << endl;
      }
      
    }
  } /* if(RodStatus) */
  
// Delete the ROD and VME objects before exiting
  delete rod;
  delete rodStatus;
  delete vme;
  
//  unlink(cmdFifo.c_str());
//  unlink(retFifo.c_str());
  
  return 0;  
}

