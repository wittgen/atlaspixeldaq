//--------------------------------CrateFlash-------------------------------- 

/*! 
 *  @brief This is a utility to reflash an entire crate
 */

#include <iostream>
using namespace std;

#include "RodModule.h"
#include "RodStatus.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;


  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=4;                       // Number of slaves
  ifstream binFile;                             // Pointer to binary frile
  unsigned long selectAddr;                     // Start address of seleced flash
  unsigned long flashAddr;                      // Location in target flash
  int fileSize;                                 // Size of binary file in bytes
  int selectSize;                               // Size of selected flash
  int iselect;
  unsigned int baseAddress;

  std::string binFileName = "";
  std::string fpgaName = "";
  std::string mask = "";

     
  //const unsigned long flashStart[5]={0xe00000, 0xe01000, 0xe80000, 0xed3000, 0xf26000};
  //const long flashSize[5] = {24, 495204, 336688, 336680, 234456};

  const unsigned long flashStartE[5]={0xe00000, 0xe00000, 0xe80000, 0xf00000, 0xf80000};
  const long flashSizeE[5] = {0, 495204, 495212, 336680, 336680};

  std::string flashName[5] = {"Loc", "RCF", "FMT", "EFB", "RTR"};
  
  std::string fileName(""), option;
  int slot = -1;


  // Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();

  if (argc > 1) {
    fpgaName = argv[1];
  }
  if (argc > 2) {
    binFileName = argv[2];
  }
  if (argc > 3) {
    mask = argv[3];
  }
 

  iselect = 0;
  for (int i=0; i<5; i++) {
    if (fpgaName == flashName[i]) iselect = i; 
  }
  if (iselect == 0) {
    std::cout << "FPGA name not recognized" << std::endl;
    exit(1);
  }
  if (binFileName == "") {
    std::cout << "Binary File name not specified" << std::endl;
    exit(1);
  }

  // JCH 21 Feb 2005. Change default to Rev E/F (explicitly check for B/C).
  
  cout << "Loading flash to Rev E/F\n";
  selectAddr = flashStartE[iselect];
  selectSize = flashSizeE[iselect];
  
  binFile.open(binFileName.c_str(), ios::binary);
  if (!binFile.is_open()) {
    cout << "Unable to open binary file." << endl;
    exit(1);
  }
  
  // Get size of file
  binFile.seekg(0, ios::end);           // go to end of file
  fileSize = binFile.tellg();          // file size given by current location
  binFile.seekg(0, ios::beg);          // go back to beginning of file
  if (fileSize != selectSize) {
    cout << "File size is incorrect. Expected: " << dec << selectSize << " Found: " 
	 << dec << fileSize << endl;
    exit(3);
  }
  // Create a buffer and read file into it
  uint8_t * buffer;
  try {
    buffer = new uint8_t[fileSize];
  }
  catch (std::bad_alloc & ba) {
    cout << "Unable to allocate buffer for binary file." << endl;
    exit(2);
  }
  binFile.read((char *)buffer, fileSize);
  
  for (slot = 5; slot < 22; slot++) {
    int pos = slot -5;
    if (slot != 13 && (mask=="" || mask[pos] == 'Y' || mask[pos] == 'y')) {
      baseAddress = slot << 24;  
      std::cout << std::dec << "======== SLOT " << slot << std::endl;
      // Check if the ROD is responding
      RodStatus* rodStatus1=nullptr;
      RodModule* rod0=nullptr;
      try {
	// Create RodModule and initialize it
	rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
	RodStatus *rodStatus2 = new RodStatus(*rod0);
	if (rodStatus2) {
	  if (!rodStatus2->isSane()) {
	    // ROD not responding, skip it
	    std::cout << "======== ROD in slot " << slot << " does not respond; skip it" << std::endl;
	    continue;
	  }
	}
	delete rodStatus2;
      }
      catch (...) {
	// ROD not responding, skip it
	std::cout << "======== ROD in slot " << slot << " does not respond; skip it" << std::endl;
        continue;
      }

      // Flash the FPGA
      try{
	rod0->initialize();
      }
      catch (HpiException *h) {
	hex(cout);
	cout << h->getDescriptor() << '\n';
	cout << "calcAddr: " << h->getCalcAddr() << ", readAddr: " << 
	  h->getReadAddr() << '\n';
	dec(cout);
      }

      // write buffer to flash
      flashAddr = selectAddr;
      
      try {
	rod0->writeBlockToFlash(flashAddr, buffer, fileSize);
	
	cout << dec << selectSize << " bytes written to the "<< flashName[iselect-1]
	     << " flash memory" << endl;
      } catch(BaseException &b) {
	cout << "******* Exception during FLASH upload!********\n";
	cout << b << endl;
      }
        
      // Reset the ROD
      // Create RodModule and initialize it
      sleep(2);
      delete rod0;
      rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);

      try {
	unsigned long int fpgaReset = 0x20;
	unsigned long int rodReset = 0x40;
	unsigned long int mdspResetStatus = 0x0;
	unsigned long int busyStatus = 0x8;

        // Reset FPGA
	rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[0], fpgaReset);
	
	// Poll BUSY
	while (busyStatus !=  0x0) {
	  busyStatus = rod0->getVmePort()->read32(0xc00014);
	  busyStatus = (busyStatus & 0x00000008);
  	}

        //Reset ROD
	rod0->hpiLoad(FPGA_CONTROL_REG_REL_ADDR[2], rodReset);
	
	// Poll on the MDSP Reset Status bit.  Fetch the status word and mask
	while (mdspResetStatus !=  0x2) {
	  mdspResetStatus = rod0->hpiFetch(FPGA_STATUS_REG_REL_ADDR[2]) & 0x2;
	}
	
	// Initialize HPIC
	unsigned long hpicValue = 0x00010001;
	rod0->hpiLoad(HPIC, hpicValue);
	
      } catch (BaseException &b) {
	cout << "Exception \"initialising\" ROD:\n" << b << endl;
	exit(0);
      }

      // Dump the ROD status
      sleep(2);
      delete rod0;
      rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);
      rodStatus1 = new RodStatus(*rod0);
      if (rodStatus1) {
	if (!rodStatus1->isSane()) {
	  // ROD not responding anymore
	  std::cout << "======== ROD in slot " << slot << " does not respond anumore" << std::endl;
          break;
	}
        rod0->initialize(false);
	rodStatus1->snapShot(*rod0);
	std::cout << *rodStatus1;
      }
      delete rodStatus1;

      delete rod0;
    }
  }

  delete vme1;

  return 0;  
}


