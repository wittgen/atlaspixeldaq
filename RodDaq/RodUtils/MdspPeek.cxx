//! A program to dump blocks of memory in Master DSP memory space. 
// It does NOT initialize the ROD!! This program is intended to be 
// a diagnostic used to examine memory contents after another
// program, e.g. EchoTest or LedTest, has run.

#include <iostream>
using namespace std;

#include "RodModule.h"

#include "RCCVmeInterface.h"

int main(int argc, char *argv[]) {

using namespace SctPixelRod;

  const uint32_t mapSize=0xc00040;         // Map size
  const int32_t numSlaves=4;                       // Number of slaves
  int32_t numWords;                                // Number of words to dump
  uint32_t dspStart;                       // Address limits to dump
  char response;                                // Y/n response
  void* genPtr;
  char* textBuff;
  std::string fileName(""), option;
  int slot = -1;
  uint32_t baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  baseAddress = slot << 24;

// Create VME interface
  RCCVmeInterface *vme1 = new RCCVmeInterface();
  
// Create RodModule but do not initialize it
  RodModule* rod0 = new RodModule(baseAddress, mapSize, *vme1, numSlaves);

// get starting address and number of bytes to read

  do {
    hex(cin);
    cout << "Starting address (hex, without leading 0x): ";
    cin >> dspStart;
    cout << "Number of 32-bit words (hex, without leading 0x): ";
    cin >> numWords;
    dec(cin);
  
// create buffer and read data
    uint32_t * buffer = new uint32_t[numWords];
    rod0->mdspBlockRead(dspStart, buffer, numWords);
  
// output the data and delete the buffer
    hex(cout);
    cout.fill('0');
    for (int i=0; i<numWords; i+=8) {
      cout.width(8);
      cout << buffer[i]   << ' ';
      cout.width(8);
      cout << buffer[i+1] << ' ';
      cout.width(8);
      cout << buffer[i+2] << ' ';
      cout.width(8);
      cout << buffer[i+3] << ' ';
      cout.width(8);
      cout << buffer[i+4] << ' ';
      cout.width(8);
      cout << buffer[i+5] << ' ';
      cout.width(8);
      cout << buffer[i+6] << ' ';
      cout.width(8);
      cout << buffer[i+7] << endl;
    }
    dec(cout);
// Display as text also
    cout.fill(' ');
    genPtr = static_cast<void*>(buffer);
    textBuff = static_cast<char*>(genPtr);
    cout << endl << "ASCII dump :" << endl;
    for (int itext = 0; itext< 4*numWords; itext++) {
      if ((textBuff[itext]<' ') || (textBuff[itext] > '~')) textBuff[itext] = '.';
        cout << textBuff[itext];
      if ((itext+1)%64 == 0) cout << endl;
    }
    cout << endl;
    delete [] buffer;
    
// do another?
    cout << "Do another? [Y/n]: ";
    cin >> response;
  } while (response != 'n');

// Clean up before exiting
  delete rod0;
  delete vme1;

  return 0;  
}


