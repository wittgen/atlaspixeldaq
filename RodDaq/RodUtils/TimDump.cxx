#include <iostream>
#include <iomanip>
#include <fstream>

#include "TimModule.h"
#include "RCCVmeInterface.h"

using namespace std;
using namespace SctPixelRod;

int main(int argc, char *argv[]) {

  const uint32_t baseAddr = 0x0D000000;  
  const uint32_t  mapSize =    0x10000;  
 
  RCCVmeInterface *vme = new RCCVmeInterface();
  try {
    TimModule *tim = new TimModule(baseAddr, mapSize, *vme ); 
  
 
  unsigned int numWords[3]         = {0x20, 0x30, TIM_SEQ_SIZE};
  unsigned int blockMemStart[3]    = {0x0,0x0040,TIM_SEQ_ADDR};
  string filename[3]               = {"TIMRegister", "TIM3ExtraRegister", "TIMSequencer"};


  for (int index=0;index<3;index++) {
    std::ostringstream onam;
    unsigned int size = numWords[index];
    unsigned short* buffer = new unsigned short[size];

    if (index==2) tim->seqFetch(TIM_SEQ_SIZE, buffer);
    else
      for (unsigned int i = 0; i < size; i++) {
	buffer[i] = tim->vmeFetch(blockMemStart[index] + i*2 );
	//if (index==0) cout<< hex<<buffer[i]<<endl;
      }
    
    std::cout << "Dumping " << filename[index] << std::endl;
    onam << filename[index];
    onam << ".bin";
    
    std::ofstream fout(onam.str().c_str(),ios::binary);
    fout.write((char *)buffer, 2*size);  // 2 bytes
    fout.close();
    delete[] buffer; 
  }

  delete tim;
  } catch (...) {
    throw TimException("Exception caught in TimModule creation", 0, 0 );
  }
  delete vme;

  return 0;  
}


