/*********************************************************/
/* Decode StdDump file of RodRegisters for Diagnostics   */
/*                                                       */
/*  checks enabled formatter links and dumps status      */
/*  checks XOFF status and backpressure                  */
/*  checks FIFO status                                   */
/*                                                       */
/* uses:                                                 */
/* FMT_LINK_EN(i)		                         */
/* FMT_TIMEOUT_ERR(i)		                         */
/* FMT_DATA_OVERFLOW_ERR(i)	                         */
/* FMT_HEADER_TRAILER_ERR(i)	                         */
/* FMT_ROD_BUSY_ERR(i)		                         */
/* FMT_MODEBIT_STAT(i)		                         */
/* FMT_LINK_OCC_CNT(i,j)	                         */
/* FMT_PXL_BANDWIDTH(i)		                         */
/* RRIF_STATUS_1		                         */
/* RRIF_CMND_1                                           */
/* EVENT_HEADER_DATA                                     */
/* EV_FIFO_DATA1	                                 */
/* EV_FIFO_DATA2                                         */
/* RTR_CMND_STAT                                         */
/* EFB_RUNTIME_STAT_REG                                  */
/* EVT_MEM_FLAGS                                         */
/*                                                       */
/*                                                       */
/*     Andreas Korn LBNL (Andreas.Korn(AT)cern.ch) 12/06 */
/*                                                       */
/*********************************************************/

#include <iostream>
#include <iomanip>
#include <fstream>

#include "registeraddress.h"
//#include "sysParams.h"

using namespace std;


#define N_FORMATTERS  8

int main(int argc, char *argv[]) {

  bool verbose = false;
  std::ostringstream onam;
  if(argc>1){
    onam << argv[1];
  }else{
    onam << "Formatter";
    onam << ".bin";
    std::cout << "usage: " << argv[0] << " RodDump.bin" << std::endl;
    std::cout << "using " << onam.str() << " as default" 
	      << std::endl<< std::endl;
    
  }

  // start address of FPGA sections of ROD
//  long int numWords[4]             = {0x2000, 0x270, 0x120, 0x800};
  long unsigned int dspStart[4]    = {0x400000,0x402000, 0x402400, 0x404400 };
//  string filename[4]               = {"Formatter", "Efb", "Router", "Rcf"};

  /*********************************************************************************8*/
  string modebits[4]               = {"play", "mask", "skip", "drop"};
  string fmtfifostat[16]           = {"unknown", "reset", "idle" , "fetch modebits", 
				      "wait for MB", "wait for token", "read out link", "wait for data",
				      "dump event", "next link", "timeout", "overflow", 
				      "backpressure", "pass token", "other", "others"};

  string rrif_status_bit[32]      = {" TIM CLOCK OK",
				     " BOC CLOCK OK",
				     " BOC busy",
				     " Inmem Done",
				     " CAL test ready",
				     " FE Trigger FIFO empty",
				     " FE Trigger FIFO full",
 				     " Formatter bank A: EFB stop output",
				     " Formatter bank A: modebit FIFO full",
 				     " Formatter bank B: EFB stop output",
				     " Formatter bank B: modebit FIFO full",
				     " Formatter bank A: Header/Trailer limit",
				     " Formatter bank B: Header/Trailer limit",
				     " Formatter bank A: Busy limit",
				     " Formatter bank B: Busy limit",
				     " EFB dyn mask FIFO empty",
				     " EFB dyn mask FIFO full",
				     " EFB Event ID empty ERROR",
				     " Event memory A FIFO empty",
				     " Event memory A FIFO full",
				     " Event memory B FIFO empty",
				     " Event memory B FIFO full",
				     "",
				     "",
				     "",
				     "",
				     "",
				     "",
				     "",
				     " FE Occ counters ALL ZERO",
				     " Command Mask READY"};

  string rrif_cmnd_bit[32]        = {" FE command output enabled",
				     " TIM is input source",
				     " New mask READY",
				     " FE occ Counters decrement on trailer",
				     " ?",
				     " FE Command Pulse Counter ENABLED",
				     " FE Command Pulse Counter LOAD",
 				     " ?",
				     " TIM EVENT ID Trigger decoder ENABLED",
 				     " ?",
				     " FMT mode bits Encoder ENABLED",
				     " ?",
				     " EFB dyn mask Encoder  ENABLED",
				     " ?",
				     " ?",
				     " ?",
				     " Test Bench ENABLED",
				     " Test Bench RUN",
				     " Trigger command decoder ENABLED",
				     " Configuration read back ENABLED",
				     " Command mask LOAD",
				     " STATIC BCID",
				     " STATIC L1ID",
				     " corrective mask READY",
				     " Inhibit Inmem Playpack",
				     " ?",
				     " ?",
				     " ?",
				     " ?",
				     " internal scan engine START",
				     " internal scan engine CLEAR",
				     ""};

  string control_signal_mux[16]  = {" DISABLE FE inputs",
				    " LOAD/READ FIFO's with DSP",
				    " FE inputs to TestBench",
				    " DISABLE FE inputs",
				    " NORMAL DATA PATH",
				    " CONFIGURATION READBACK MODE",
				    " CALIBRATION TEST MODE",
				    " CAPTURE data in INMEMS",
				    "",
				    "",
				    "",
				    "",
				    "",
				    "",
				    "",
				    ""};

  string rtr_cmnd_stat_bit[16]   = {" DUMP ATLAS event",
				    " DUMP TIM event",
				    " DUMP ROD event",
				    " S-Link write INHIBIT",
				    " S-Link RESET",
				    " S-Link TEST",
				    " Backpressure to EFB ENABLED",
				    " S-Link down MASKED",
				    " S-Link XOFF",
				    " S-Link DOWN",
				    " BACKPRESSURE to EFB",
				    " 40MHz DLL LOCKED",
				    " 80MHz DLL LOCKED",
				    " S-Link lateched XOFF",
				    " S-Link little Endian",
				    ""};

  string efb_runtime_stat_bit[16]   = {" Event FIFO A almost FULL",
				       " Error summary FIFO A almost FULL",
				       " Event ID Engine A EMPTY",
				       " Engine A Pause Formatters",
				       " Event FIFO B almost FULL",
				       " Error summary FIFO B almost FULL",
				       " Event ID Engine B EMPTY",
				       " Engine B Pause Formatters",
				       " Router HALT output",
				       " Event ID Data FIFO almost FULL",
				       " ROL test block ENABLED",
				       " Synch Event ERROR",
				       " ",
				       " ",
				       " ",
				       " EFB DLL LOCKED" };

  string evt_mem_flags_bit[24]   = {" FIFO A EMPTY",
				    " FIFO A almost EMPTY",
				    " FIFO A almost FULL",
				    " FIFO A FULL",
				    " FIFO B EMPTY",
				    " FIFO B almost EMPTY",
				    " FIFO B almost FULL",
				    " FIFO B FULL",
				    " Header/Trailer FIFO EMPTY",
				    " Header/Trailer FIFO FULL",
				    " Event ID FIFO A EMPTY",
				    " Event ID FIFO A almost FULL",
				    " Event ID FIFO B EMPTY",
				    " Event ID FIFO B almost FULL",
				    " Header Event ID FIFO EMPTY",
				    " Header Event ID FIFO almost FULL",
				    " Header Event ID FIFO FULL",
				    " Event Data FIFO FULL",
				    " EVENT ID FIFO A FULL",
				    " EVENT ID FIFO B FULL",
				    " Word count FIFO A FULL",
				    " Word count FIFO B FULL",
				    " Error Summary FIFO A FULL",
				    " Error Summary FIFO B FULL"};


  /* by default the formatter dump contains ALL rod registers */
  /* load saved register dump into buffer for decoding        */ 
#define registerlength 0x2000
  unsigned long * buffer = new unsigned long[registerlength];

  std::ifstream fin(onam.str().c_str(),ios::binary);
  fin.read((char *)buffer, 4*registerlength); 

  /* loop over enabled Formatters Links and dump status   */
  for(int i = 0; i<N_FORMATTERS;i++){
    //    printf(" Enabled Links:  ");
    printf(" FMT %d: status %s", i, fmtfifostat[(buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4] >> 28 ) & 0xF].c_str()); 
    if((buffer[(EFB_FORMATTER_STAT-dspStart[0])/4] & (1 << i)))printf(" STUCK");
    printf("\n");
    for(int j = 0; j<4;j++){
      if(buffer[(FMT_LINK_EN(i)-dspStart[0])/4] & (1<<j)){
	printf("  ENABLED %x:%x ", i, j);
	if(buffer[(FMT_TIMEOUT_ERR(i)-dspStart[0])/4] & (1<<j))       printf(" TIMEOUT ");
	if(buffer[(FMT_DATA_OVERFLOW_ERR(i)-dspStart[0])/4] & (1<<j)) printf(" OVERFLOW ");
	if(buffer[(FMT_HEADER_TRAILER_ERR(i)-dspStart[0])/4] & (1<<j))printf(" HEADER/TRAILERLIMIT ");
	if(buffer[(FMT_ROD_BUSY_ERR(i)-dspStart[0])/4] & (1<<j))      printf(" BUSY ");
	printf(" MB %s", modebits[((buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4] >> (2*j) ) & 0x3)].c_str()); 
	printf(" 0x%x words pending ", (unsigned int) buffer[(FMT_LINK_OCC_CNT(i,j)-dspStart[0])/4]);
	printf("\n");
      }
    }
    if(verbose){
      printf(" FMT_LINK_EN  FMT %d: 0x%x 0x%x\n", i, FMT_LINK_EN(i), (unsigned int) buffer[(FMT_LINK_EN(i)-dspStart[0])/4]);
      printf(" Link mapping FMT %d: 0x%x 0x%x\n", i, FMT_PXL_BANDWIDTH(i), (unsigned int) buffer[(FMT_PXL_BANDWIDTH(i)-dspStart[0])/4]);
      printf(" FMT_MODEBIT_STAT %d: 0x%x 0x%x\n", i, FMT_MODEBIT_STAT(i), (unsigned int) buffer[(FMT_MODEBIT_STAT(i)-dspStart[0])/4]);
    }
  }
  printf("============================================================\n\n");


  /* Dump Status bits  */
  printf(" RRIF_STATUS_1 0x%x 0x%x\n", RRIF_STATUS_1, (unsigned int) buffer[(RRIF_STATUS_1-dspStart[0])/4]);
  printf("   RRIF_STATUS_1 FE Command Pulse counters: 0x%x\n", (unsigned int) (buffer[(RRIF_STATUS_1-dspStart[0])/4]>>29)& 0xFF);
  for(int i=0;i<22;i++){
    if((buffer[(RRIF_STATUS_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_status_bit[i].c_str());
  }
  for(int i=30;i<32;i++){
    if((buffer[(RRIF_STATUS_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_status_bit[i].c_str());
  }


  /* Dump Command (Rod Configuration) bits  */
  printf(" RRIF_CMND_1 0x%x 0x%x\n", RRIF_CMND_1, (unsigned int) buffer[(RRIF_CMND_1-dspStart[0])/4]);

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & 0x1){
    printf("    FE Command Output ENABLED\n");
  }else{
    printf("    FE Command Output DISABLED\n");
  }

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & 0x2){
    printf("    Command input source is TIM\n");
  }else{
    printf("    Command input source is DSP\n");
  }
  for(int i=2;i<24;i++){
    if((buffer[(RRIF_CMND_1-dspStart[0])/4]) & (1<<i))printf("   %s\n",rrif_cmnd_bit[i].c_str());
  }

  if(buffer[(RRIF_CMND_1-dspStart[0])/4] & (1<<24)){
    printf("    Inmem playback INHIBIT\n");
  }else{
    printf("    PLAYBACK Inmems\n");
  }

   printf("   %s\n", control_signal_mux[(buffer[(RRIF_CMND_1-dspStart[0])/4] >>25) & 0xF].c_str());


   /* Dump used event ID FIFO data */
  printf("=========================================================================\n\n");
  printf(" last EVENT used in EFB  : L1ID: 0x%04x  BCID: 0x%012x\n", 
	 (unsigned int) (buffer[(EVENT_HEADER_DATA-dspStart[0])/4] >> 12) & 0xF,
	 (unsigned int) (buffer[(EVENT_HEADER_DATA-dspStart[0])/4] ) & 0xFFF);
  printf(" last EVENT used in EFB 0: L1ID: 0x%04x  BCID: 0x%012x ROD: 0x%04x\n", 
	 (unsigned int) (buffer[(EV_FIFO_DATA1-dspStart[0])/4] >> 8) & 0xF,
	 (unsigned int) (buffer[(EV_FIFO_DATA1-dspStart[0])/4] ) & 0xFF,
	 (unsigned int) (buffer[(EV_FIFO_DATA1-dspStart[0])/4] >> 12) & 0xF);
  printf(" last EVENT used in EFB 1: L1ID: 0x%04x  BCID: 0x%012x ROD: 0x%04x\n\n", 
	 (unsigned int) (buffer[(EV_FIFO_DATA2-dspStart[0])/4] >> 8) & 0xF,
	 (unsigned int) (buffer[(EV_FIFO_DATA2-dspStart[0])/4] ) & 0xFF,
	 (unsigned int) (buffer[(EV_FIFO_DATA2-dspStart[0])/4] >> 12) & 0xF);

  printf("=========================================================================\n\n");

   /* Dump Router and S-Link status*/
  printf(" RTR_CMND_STAT %x  %x\n", RTR_CMND_STAT ,(unsigned int) buffer[(RTR_CMND_STAT-dspStart[0])/4]);
  for(int i=0;i<16;i++){
    if((buffer[(RTR_CMND_STAT-dspStart[0])/4]) & (1<<i))printf("   %s\n",rtr_cmnd_stat_bit[i].c_str());
  }

  printf("=========================================================================\n\n");

  printf(" EFB_RUNTIME_STAT_REG %x  %x\n", EFB_RUNTIME_STAT_REG ,(unsigned int) buffer[(EFB_RUNTIME_STAT_REG-dspStart[0])/4]);
  for(int i=0;i<16;i++){
    if((buffer[(EFB_RUNTIME_STAT_REG-dspStart[0])/4]) & (1<<i))printf("   %s\n",efb_runtime_stat_bit[i].c_str());
  }

  printf("=========================================================================\n\n");

  printf(" EVT_MEM_FLAGS %x  %x\n", EVT_MEM_FLAGS ,(unsigned int) buffer[(EVT_MEM_FLAGS-dspStart[0])/4]);
  for(int i=0;i<24;i++){
    if((buffer[(EVT_MEM_FLAGS-dspStart[0])/4]) & (1<<i))printf("   %s\n",evt_mem_flags_bit[i].c_str());
  }

  printf("=========================================================================\n\n");


  printf("=========================================================================\n\n");
  printf(" Unknown Reg     %x %x\n", 0x40444c, (unsigned int) buffer[(0x40444c-dspStart[0])/4]);
  printf(" RRIF_STATUS_0   %x %x\n", 0x404424, (unsigned int) buffer[(0x404424-dspStart[0])/4]);
  printf(" ECR_COUNTER_VAL %x %x\n", 0x404448, (unsigned int) buffer[(0x404448-dspStart[0])/4]);

  return 0;  
}


