#include <iostream>
#include <unistd.h>

#include "RCCVmeInterface.h"

using namespace SctPixelRod;
using namespace std;

timeval t0,ts;
VmePort* port[32];
RCCVmeInterface *vmeIf;

int ret;

void initVme(int i) {
  port[i] = new VmePort(i<<24, 0x00C00040, VmeInterface::A32, *vmeIf);
  port[i]->setExceptionTrapping(true);
  // zero all PRM control registers that is get stuck in at a non-zero value
  //port[i]->write32(0xc00000, 0x0); //FPGA Configuration Control Register
  //port[i]->write32(0xc00004, 0x0); //FPGA Reset Control Regsiter
  port[i]->write32(0xc00008, 0x0); //DSP Reset Control Register

}

void closeVme(int i) {
  delete port[i];
}

int main(int argc, char *argv[]) {

  std::string option; 
  int slot = -1;
  //unsigned long baseAddress;

  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'A': { // All slots
	  cout << "Reconfig on all Rods in this crate is not yet supported... " << endl;
	  break;
	}
        case 's': { // Single slot 
          slot = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  // Prompt for slot number
  if (slot < 0 ) {
    cout << "Enter slot number (decimal):"; 
    cin >> slot;
    while ((slot < 1) || (slot > 21)) {
      cout << "Slot number out or range [1:21], re-enter: ";
      cin >> slot;
    }
  }
  //baseAddress = slot << 24;

  /*
  if (argc > 1 && argc < 22) {
    srod = new int[argc-1];
    for (i=1; i<argc; i++) {
      std::istringstream ss(argv[i]);
      ss >> irod;
      if (irod>=2 && irod<=21) {
        srod[nrod++] = irod;
      } 
    }
  }
  std::cout << std::endl << " ************ CONFIGURE AND RESET ALL FPGAS ON RODS ==> ";
  for (i=0; i<nrod; i++) { 
    std::cout << srod[i] << "  " ;
  }
  std::cout << std::endl;
  vmeIf = new RCCVmeInterface();
  for (i=0; i<nrod; i++) { 
    initVme(srod[i]);
  }
  sleep(2); 
  for (i=0; i<nrod; i++) closeVme(srod[i]);
  delete vmeIf;
  return 0;
  */ // JJ

  std::cout << std::endl << " ************ CONFIGURE AND RESET ALL FPGAS ON ROD ==> ";
  std::cout << slot << "  " ;
  std::cout << std::endl;
  vmeIf = new RCCVmeInterface();
  initVme(slot);
  sleep(2); 
  closeVme(slot);
  delete vmeIf;
  return 0;
}
