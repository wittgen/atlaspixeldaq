#!/usr/bin/env python
import os
import os.path as path
import shutil
from glob import glob
import Queue
import threading
import subprocess
import sys
import time

pixelcratenames =["l-1","l-2","l-3","l-4","d-1","d-2","b-1","b-2","b-3"]


class MassiveSend(threading.Thread):
    def __init__(self, valuetouse):
        threading.Thread.__init__(self)
        self.sbcname="sbc-pix-rcc-"+pixelcratenames[valuetouse]
        
        
    def run(self):
        while 1:
           stringforsbc = "source /det/pix/scripts/Default-pixel.sh;/atlas-home/0/edevetak/daq/RodDaq/RodUtils/ModulesReset"
           print "B", self.sbcname,stringforsbc
           returncode = subprocess.call('ssh  %s \' %s \' ' % (self.sbcname,stringforsbc) , shell=True)            
           print "C", self.sbcname,stringforsbc
           
def main():
    NUMTHREADS = 9
    for entry in range(NUMTHREADS):
        time.sleep(1)
        print "A", entry
        t = MassiveSend(entry)
        t.setDaemon(True)
        t.start()
    time.sleep(2)
if __name__ == '__main__':
    main()
