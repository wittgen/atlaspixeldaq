/* Master Header */

#ifndef _QUICKSTATUSINFO_
#define _QUICKSTATUSINFO_

#include <cstdint>
typedef uint32_t DSPUINT32P; // 32 bit "pointer"

typedef struct {
	struct {
		uint32_t linkMap;
		uint32_t linkEnable;
		uint32_t timeoutError;
		uint32_t dataOverflowError;
		uint32_t headerTrailerError;
		uint32_t rodBusyError;
		uint32_t modeBitStatus;
		uint32_t QSDisabled;
		uint32_t nRST[4];
		uint32_t nCFG[4];
	} formatter[8];

	uint32_t nActiveFMTs;
	uint32_t systemTime;
	uint32_t QuickStatusMode;

	struct {
		uint32_t rodBusy;
		uint32_t headerTrailerLimitError;
		uint32_t rodMode;
		uint32_t slavePresent[4];
		uint32_t ECRID;
		uint32_t L1TriggerCount;
		uint32_t L1IDEfb;
		uint32_t BCIDEfb;
	} rodStatus;

	struct {
		uint32_t scanRunning;
		uint32_t scanType;
		uint32_t scanState;
		uint32_t nActiveModules;
		uint32_t maskStage;
		uint32_t innerParameter;
		uint32_t outerParameter;
	} scanInfo;

	struct {
		uint32_t clockCycles;
		uint32_t dataWords;
		uint32_t fragments;
		uint32_t hits;
		uint32_t timeOutErrors;
		uint32_t endOfColumnErrors;
		uint32_t aFTs;
		uint32_t rowColumnErrors;
		uint32_t syncErrors;
		uint32_t bCIDErrors;
		uint32_t l1IDErrors;
		uint32_t hOFs;
		uint32_t globalRegParityErrors;
		uint32_t bitFlips;
		uint32_t hammingErrors;
		uint32_t totalErrors;
		uint32_t largeEvents;
	} routerCounters;

	DSPUINT32P historyAddress;
	uint32_t historyIndex;
	uint32_t historyStatus;

	struct{
		uint32_t ECRCount;
		uint32_t hitCount;
		uint32_t eventCount;
		uint32_t errorCount;
		uint32_t inProcCount;
		uint32_t clkCount;
		uint32_t skipReport;
		uint32_t skipTrig;
	} modCounts[8][4];

} QuickStatusInfo;

typedef struct {

  uint32_t historyIndex;

  uint32_t rcf_rrifStatus1;
  uint32_t rcf_rrifStatus0;
  uint32_t rcf_rodBusy;

  uint32_t efb_runtimeStat;
  uint32_t efb_dmFifoFlagStat;
  uint32_t efb_eventHeaderData;
  uint32_t efb_eventFifoData1;
  uint32_t efb_eventFifoData2;
  uint32_t efb_evtMemFlags;
  uint32_t efb_dmWordCount;

  uint32_t rtr_cmndStat;

  struct{
    uint32_t status;
    uint32_t linkEnable;
    uint32_t timeoutError;
    uint32_t headerTrailerError;
    uint32_t rodBusyError;
    uint32_t modeBitStatus;
    uint32_t linkOccCount[4];
  } fmt[8];

} busyRecord;


#endif

