/* Common Header */

#ifndef PRIMXFACE_COMMON_H
#define PRIMXFACE_COMMON_H
#include <cstdint>
#define PRIM_ECHO_STRING "echo"
#define PRIM_RW_MEMORY_STRING "rw memory"
#define PRIM_SET_LED_STRING "set led"
#define PRIM_RW_REG_FIELD_STRING "rw reg field"
#define PRIM_MODULE_MASK_STRING "module mask"
#define PRIM_SLAVE_CONTROL_STRING "slave control"
#define PRIM_TEST_GLOBAL_REGISTER_STRING "test global register"
#define PRIM_TEST_ALL_REGISTERS_STRING "test all module registers"
#define PRIM_LOAD_MODULE_STRING "load module configuration"
#define PRIM_READ_MODULE_STRING "read module configuration"
#define PRIM_SEND_MODULE_STRING "send module configuration"
#define PRIM_SEND_MODULE_WITH_PREAMP_OFF_STRING "send modCfg Preamp Off"
#define PRIM_RW_SLAVE_MEMORY_STRING "rw slave memory"
#define PRIM_SEND_SERIAL_STRING "send serial stream"
#define PRIM_RW_FIFO_STRING "rw fifo"
#define PRIM_SEND_SLAVE_PRIMITIVE_STRING "send slave primitive"
#define PRIM_WRITE_FLASH_STRING "write flash"
#define PRIM_QUICK_XFER_STRING "quick xfer"
#define PRIM_EVENT_TRAP_SETUP_STRING "event trap setup"
#define PRIM_START_TASK_STRING "start task"
#define PRIM_TALK_TASK_STRING "talk task"
#define PRIM_SEND_TRIGGER_STRING "send trigger"
#define PRIM_SETUP_GROUP_STRING "setup group"
#define PRIM_TEST_PIXEL_REGISTER_STRING "test pixel register"
#define PRIM_LOAD_DATA_FRAME_STRING "load data frame"
#define PRIM_EXPERT_STRING "expert"
#define PRIM_FIND_LINK_STRING "find link"
#define PRIM_MCC_EVENT_BUILD_STRING "mcc event build"
#define PRIM_SET_ROD_MODE_STRING "set rod mode"
#define PRIM_RW_MCC_STRING "rw mcc"
#define PRIM_CONFIGURE_ENVIRONMENT_STRING "configure environment"
#define PRIM_SEND_SLAVE_LIST_STRING "send slave list"
#define PRIM_FIT_STRING "fit"
#define PRIM_BOC_CONTROL_STRING "BOC control"
#define PRIM_SLAVE_REG_ACCESS_STRING "slave register access"
#define PRIM_DEVICE_REG_ACCESS_STRING "device register access"

#define CHIP_MASK 0x10000
#define MODULE_FLAG	0x80000000
#define SLAVE_FLAG	0x40000000
#define MAST_FREEMEMORY_BASE  0x02d00000



enum PrimitiveIds {
	ECHO = 1,
	RW_MEMORY,
	SET_LED,
	RW_REG_FIELD,
	MODULE_MASK,
	SLAVE_CTRL,
	TEST_GLOBAL_REGISTER,
	LOAD_MODULE_CONFIG,
	READ_MODULE_CONFIG,
	SEND_MODULE_CONFIG,
	RW_SLAVE_MEMORY,
	SEND_SERIAL_STREAM,
	RW_FIFO,
	SEND_SLAVE_PRIMITIVE,
	WRITE_FLASH,
	START_TASK,
	TALK_TASK,
	SEND_TRIGGER,
	SETUP_GROUP,
	TEST_PIXEL_REGISTER,
	LOAD_DATA_FRAME,
	EXPERT,
	FIND_LINK,
	MCC_EVENT_BUILD,
	SET_ROD_MODE,
	RW_MCC,
	CONFIGURE_ENVIRONMENT,
	SEND_SLAVE_LIST,
	QUICKXFER, /* generic quick transfer */
	TEST_ALL_REGISTERS,
	SEND_MODULE_CONFIG_WITH_PREAMP_OFF,
	BOC_CTRL,
	SLAVE_REG_ACCESS,
	DEVICE_REG_ACCESS,
//	LOAD_FRAME = SLAVE_PRIMITIVE_BASE,
//	FIT_SCURVE,
//	INVOKE_INTERRUPT,
//	QUICKSLAVEXFER, /* transfer from slave */
/*
	SETUP_HISTOGRAM,
	START_HISTOGRAM,
	DOWNLOAD_HISTOGRAM,
	SETUP_FIT,
	START_FIT,
	DOWNLOAD_FIT,
 */
	LAST_PRIMITIVE /* dummy place holder */
};

/*** TASK DEFINITIONS ***/

enum {
	DIAGNOSTIC_TASK = 0,
	SCAN_TASK,
	SCAN_TASK_2,
	HISTOGRAM_TASK,
	BOC_SCANM,
	LEAK_SCAN,
	STATUS_READ,
	DUMMY_TASK,
	N_PREDEFINED_TASKS
};

/*** primitive structures ***/
enum {
	OPEN_FILE,
	POSITION_FILE,
	WRITE_FILE_DATA,
	READ_FILE_DATA,
	CLOSE_FILE,
	LAST_HOST_PRIMITIVE
};

/*** INVOKE INTERRUPT ( diagnostic ) ***/
typedef struct {
	uint32_t irq;
} InvokeInterruptIn;

/* used for RW flags across various primitives */
#define RW_WRITE 1
#define RW_READ 2

/* section header */
typedef struct {
	uint32_t id; /* slave = 0, 1, 2, 3; master = 4 */
	uint32_t nWords;
	uint32_t addr;
} RwMemoryReplyHeader;

/*** RW SLAVE MEMORY ***/
/* if SLAVE_MASK is set, RwSlaveMemory::slave is interpreted as a mask */
#define SLAVE_MASK 0x10

typedef struct {
	uint32_t slave, flags, slaveAddr, nWords, masterAddr;
} RwSlaveMemoryIn;

/*** TEST GLOBAL REGISTER ***/
typedef struct {
	uint32_t moduleIndex, chipIndex;
} TestGlobalRegisterIn;

/*** TEST ALL REGISTERS ***/
typedef struct {
  uint32_t moduleMask, chipMask, doSEU;
} TestAllRegistersIn;

/*** TEST PIXEL REGISTER ***/
typedef struct {
	uint32_t moduleIndex, chipIndex;
} TestPixelRegisterIn;

/*** LEAK CURRENT SCAN ***/
typedef struct {
  uint32_t moduleMask, chipMask, control, nmaskStages;
} LeakCurrentScanIn;

/*** BOC SCAN ***/
typedef struct {
	uint32_t moduleMask, nThrs, nDelays, ThrMin,ThrMax,DelMin,DelMax,pattern_in, nHits, control,nevnts,loop2ID,iLink;
} BocScanIn;

/*** SEND SLAVE LIST ***/
/* back by popular demand */
typedef struct {
	uint32_t slave, queueIndex, nPrimitives, nWords;
} SendSlaveListIn;

/*** SEND SLAVE PRIMITIVE ***/
typedef struct {
	uint32_t slave, primitiveId, nWords, fetchReply;
} SendSlavePrimitiveIn;

/*** RW MEMORY ***/

#define RW_SPECIFY_LENGTH 1

typedef struct {
	uint32_t src, dst, count, flag;
} RwMemoryIn;

/*** LOAD SLAVE CODE ***/

typedef struct {
	uint32_t addr, length, fInternal;
} DldHeader;

#define DSP_RESET		0x01
#define SLAVE_INIT	0x02
#define LOAD_COFF		0x04
#define LOAD_DONE		0x08
#define WAIT_BOOT		0x10

typedef struct {
	uint32_t slaveMask, flag, nSections;
} SlaveCtrlIn;

/*** SET LED ***/

typedef struct {
	uint32_t  num, state;
} SetLedIn;

#define YELLOW_LED 0
#define GREEN_LED  1
#define RED_LED    2

#define OFF     0
#define ON      1
#define TOGGLE  2

/*** RW REG FIELD ***/

typedef struct {
	uint32_t regAddr;
	uint32_t value;
	uint32_t offset;
	uint32_t width;
	uint32_t flags;
} RwRegFieldIn;

typedef struct {
	uint32_t value;
} RwRegFieldOut;

/*** MODULE MASK ***/

typedef struct {
	uint32_t nModules;
	uint32_t dataPtr;
} ModuleMaskIn;

typedef struct {
	uint32_t cmdLine;
	uint32_t formatterLink;
	uint32_t formatterId;
	uint32_t detLine;
//AKDEBUG	uint32_t name[SIZEOF_NAME]; /* interpreted as a character string including null termination */
} ModuleMask;

typedef struct {
	uint32_t moduleId;
	ModuleMask moduleMask;
} ModuleMaskInAux;

/*** LOAD MODULE CONFIG ***/

enum {
	MODULE_CONFIG_FORMAT_LEGACY = 0, /* want to keep as zero so it is the default if noone specifies */
	MODULE_CONFIG_FORMAT_NATIVE
};

typedef struct {
	uint32_t cfgSize;
	uint32_t cfgId;
	uint32_t module;
	uint32_t format;
	uint32_t dataPtr;
} LoadModuleConfigIn;

typedef struct {
	uint32_t dataPtr; /* returns address of configuration data */
	uint32_t cfgSize; /* size of configuration structure */
} LoadModuleConfigOut;

/*** READ MODULE CONFIG ***/

enum {
	CONFIG_SOURCE_MODULE,
	CONFIG_SOURCE_MEMORY,
	CONFIG_IDENTIFIER
};


/*** SEND SERIAL STREAM ***/

typedef struct {
	uint32_t sportId, nBits, fCapture, nCount;
	uint32_t dataPtr, maskLo, maskHi;
} SendSerialStreamIn;

/*** SEND MODULE CONFIGURATION ***/

typedef struct {
	uint32_t moduleId;
	uint32_t cfgId;
	uint32_t chipMask;
	uint32_t bitMask;
} SendModuleConfigIn;

typedef struct {
	uint32_t length;
	uint32_t dataPtr;
} SendModuleConfigOut;

/*** SET LEMO ***/

typedef struct {
	uint32_t fmtLink;
} SetLemoIn;

/*** WRITE FLASH ***/

typedef struct {
	uint32_t dataPtr;
} WriteFlashIn;

/*** ECHO MEMORY ***/

typedef struct {
	uint32_t nWords, dataPtr;
} EchoIn;

/*** INMEMS ***/

typedef struct {
	uint32_t fRead, length, fifoId, dataPtr;
} RwFifoIn;

enum {
	FIFO_INMEMS, /* both inmems */
	FIFO_INMEMA,
	FIFO_INMEMB,
	FIFO_EVTMEMS, /* evtmems A & B */
	FIFO_EVTMEMA,
	FIFO_EVTMEMB,
	FIFO_EVTMEMC
};

/*** LOAD SLAVE CODE ***/

typedef struct {
	uint32_t slaveMask;
	uint32_t dataPtr;
} LoadSlaveCodeIn;

/*** QUICK TRANSFER ***/
typedef struct {
	uint32_t src, nWords;
} QuickXferIn;

typedef struct {
	uint32_t control, dst0, dst1;
} QuickXferOut;


/*** START TASK ***/

enum {
	DSP_THIS = 0, /* used to identify current processor */
	DSP_MASTER,
	DSP_SLAVE0,
	DSP_SLAVE1,
	DSP_SLAVE2,
	DSP_SLAVE3
};

typedef struct {
	uint32_t id;
	uint32_t idMinor;
	uint32_t where;
	uint32_t dataPtr;
} StartTaskIn;

typedef struct {
	uint32_t task;
	uint32_t status;
} StartTaskOut;

/*** SEND TRIGGER ***/
typedef struct {
	uint32_t groupId;
	uint32_t calDelay; /* if !0 this is delay between CAL and TRIGGER */
} SendTriggerIn;

/*** SETUP GROUP ***/
typedef struct {
	uint32_t id; /* id for either module or slave */
} SetupGroupInAux;

typedef struct {
	uint32_t groupId;
	uint32_t nModules;
	uint32_t nSlaves;
	uint32_t dataPtr;
} SetupGroupIn;

/*** EVENT TRAP SETUP ***/

typedef struct {
	uint32_t group;
	uint32_t dataPtr;
} EventTrapSetupIn;

/*** LOAD DATA FRAME ***/

typedef struct {
	uint32_t slaveMask;
	uint32_t dataPtr;
} LoadDataFrameIn;

/*** EXPERT MODE ***/

enum {
	RELEASE_PPC_CTL,
	SPORT0_MODE,
	SPORT1_MODE,
	INTERRUPT_SLAVE,
	ROUTER_ADDR,
	UART_SRC    // set the source for the ROD USB UART. 0 = PPC, 1 = slave0, 2 = slave1
};

enum {
	SPORT_MODE_NORMAL,
	SPORT_MODE_DEBUG
};

typedef struct {
	uint32_t item, arg[4];
} ExpertIn;

/*** FIND LINK ***/

/* link < N_LINKS searches the specified link. link = N_LINKS searches all links */
typedef struct {
	uint32_t link;
} FindLinkIn;

typedef struct {
	uint32_t detLine[96];
} FindLinkOut;

/*** MCC EVENT BUILD ***/

typedef struct {
	uint32_t module;
	uint32_t chip;
	uint32_t nData;
	uint32_t dataPtr;
} MccEventBuildIn;

/*** SET ROD MODE ***/

enum {
	ROD_MODE_DATA_TAKING,
	ROD_MODE_DATA_TAKING_SPORT0_CONFIGURATION,
	ROD_MODE_CALIBRATION,
	ROD_MODE_SPORT0_SPORT1_CONFIGURATION,
	ROD_MODE_SETUP,
	ROD_MODE_SPORT0_SPORT1_ACTIVE_FOR_TRIGGER,
	ROD_MODE_TEST
};

typedef struct {
	uint32_t mode;
} SetRodModeIn;

/*** Read/Write MCC ***/

typedef struct {
	uint32_t module;
	uint32_t fRead;
	uint32_t reg;
	uint32_t data;
} RwMCCIn;

typedef struct {
	uint32_t nRegs;
	uint32_t dataPtr;
} RwMCCOut;

/*** Talk To Task ***/

typedef struct {
	uint32_t task;
	uint32_t topic;
	uint32_t item;
} TalkTaskIn;

typedef struct {
	uint32_t length;
	uint32_t dataPtr;
} TalkTaskOut;

/*** Host Primitives ***/

typedef struct {
	uint32_t mode;
	uint32_t dataPtr; /* filename */
} OpenFileIn;

typedef struct {
	uint32_t item;
	uint32_t nArgs;
	uint32_t dataPtr;
} ConfigureEnvironmentIn;

typedef struct {
	uint32_t status;
} ConfigureEnvironmentOut;

enum {
	SET_VERBOSE_LEVEL = 0
};
/*** QuickStatus Modes and Actions ***/

enum{
  QS_INFO = 0,
  QS_OFF = 1,
  QS_IDLE = 2,  
  QS_MON = 3,
  QS_RST = 4,
  QS_RST_DIS = 5,
  QS_RST_CFG_ON = 6,
  QS_RST_CFG_OFF = 7,
  QS_SINGLE_RST = 11,
  QS_SINGLE_CFG_ON = 12,
  QS_SINGLE_CFG_OFF = 13,
  QS_SINGLE_DIS = 14,
  QS_SINGLE_ENA = 15,
  QS_SINGLE_RST_ALL = 16,
  QS_SINGLE_CFG_ON_ALL = 17,
  QS_SINGLE_CFG_OFF_ALL = 18,
  N_FC50_MAX = 21, //multiplied by 50 to get fine count
  N_BU_MAX = 22, //max number of busies
  N_TO_MAX = 23, //max number of busies
  N_RST_MAX = 24, //max number of reset attempts
  N_CFG_MAX = 25, // max number reconfig attempts
  QS_TEST = 26,  // Config QS parameters 
  QS_TEST_SYNCH = 27, // try to resynch
  N_HT_MAX = 28 //max number of ht
};

//basic common primitive structres for Ibldsp Code and Newdsp code (originally been in primXface.h)
/* header for primitive list */
typedef struct {
	uint32_t length;/* total number of words in the list payload */
	uint32_t nMsgs; /* number of primitives */
	uint32_t index;
} MsgListHead;

/* header for each primitive in list */
typedef struct {
	uint32_t length;
	uint32_t id; /* identifies primitive */
	uint32_t index; /* used for reply */
	uint32_t revision;
} MsgHead;

/* trailer for primitive list */
typedef struct {
	uint32_t length; /* should = length in the header, a check */
	uint32_t crc; /* checksum for entire message */
} MsgListTail;

/* header for reply buffer */
typedef struct {
	uint32_t length; /* total length of this reply, through trailer */
	uint32_t nMsgs; /* number of messages */
} ReplyListHead;

/* header for each reply in the buffer */
typedef struct {
	uint32_t length;
	uint32_t id; /* identifies primitive to which this is the reply */
} ReplyHead;

typedef struct {
	uint32_t length; /* an echo of the length in the header */
	uint32_t crc;
} ReplyListTail;

/* the host needs to see the available primitives */
typedef struct {
	int id, structSize;
} PrimitiveXface;

typedef struct {
	PrimitiveXface xface;
	int (*handler)(void *primData);
	char *name;
} PrimitiveEntry;

/*** READ MODULE CONFIG ***/

typedef struct {
	uint32_t moduleId;
	uint32_t source;
	uint32_t cfgId;
} ReadModuleConfigIn;

/**** BOC CONTROL ***/
typedef struct {
	uint32_t regNum, data, mode; //mode: 1=read, 1=0
} BocCtrlIn;

/**** slave register CONTROL ***/
enum { SLAVE_REG_WRITE, SLAVE_REG_READ, SLAVE_REG_READ_FIFO };
/* SLAVE_REG_READ_FIFO meant for reading back all what is in a FIFO */
typedef struct {
        uint32_t slaveNum, regNum, data, mode; /* mode from enum */
} SlaveRegAccessIn;

typedef struct {
        uint32_t deviceNum, regNum, data, mode; //mode: 1=read, 1=0
} DeviceRegAccessIn;


#endif
