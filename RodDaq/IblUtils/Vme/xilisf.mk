

BSP := standalone_bsp_xilisf
CPU := ppc440_0
BSP_DIR := $(PIXELDAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/bsp/$(BSP)/$(CPU)
INCLUDES += -I$(BSP_DIR)/include

XILINX_COMPONENTS = spi standalone xilisf
$(foreach C,$(XILINX_COMPONENTS),$(eval VPATH += $(wildcard $(BSP_DIR)/libsrc/$(C)*/src)))

