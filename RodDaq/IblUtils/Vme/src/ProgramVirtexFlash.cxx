/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/

/***************************** Include Files *********************************/

#include "xilisf.h"		/* Serial Flash Library header file */
#include "RCCVmeInterface.h"
#include <cstring>
#include <boost/program_options.hpp>
#include "VmeUtils.hh"

using namespace std;

/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01


// total flash size in bytes
#define FLASH_SIZE 0x840000
#define FLASH_HALF (0x840000/2)



// size of SREC file
#define SREC_FILE_SIZE 964018



/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

static int IsfWaitForFlashNotBusy();
void VMEUARTControl( int slotNr, bool lock);
void tokenize(const string& str, vector<string>& tokens, const string& delimiters = " ");
int ip_str2int(const string &tr, uint32_t &ip);
int mac_str2array(const string &str, uint8_t* mac);
void help();

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
static XIsf Isf;
static XSpi Spi;

/*
 * The user needs to allocate a buffer to be used by the In-system and Serial
 * Flash Library to perform any read/write operations on the Serial Flash
 * device.
 * User applications must pass the address of this memory to the Library in
 * Serial Flash Initialization function, for the Library to work.
 * For Write operations:
 * - The size of this buffer should be equal to the Number of bytes to be
 * written to the Serial Flash + XISF_CMD_MAX_EXTRA_BYTES.
 * - The size of this buffer should be large enough for usage across all the
 * applications that use a common instance of the Serial Flash.
 * - A minimum of one byte and a maximum of ISF_PAGE_SIZE bytes can be written
 * to the Serial Flash, through a single Write operation.
 * The size of this buffer should be equal to XISF_CMD_MAX_EXTRA_BYTES, if the
 * application only reads from the Serial Flash (no write operations).
 */
u8 IsfWriteBuffer[/*ISF_PAGE_SIZE*/1056 + XISF_CMD_SEND_EXTRA_BYTES];


/*
 * Page size of the Serial Flash.
 */
unsigned int ISF_PAGE_SIZE;
// start address where SREC file is stored
unsigned int FLASH_START_ADDRESS;
/*
 * Buffers used during read and% write transactions.
 */
u8 gReadBuffer[/*ISF_PAGE_SIZE*/1056 + XISF_CMD_SEND_EXTRA_BYTES] ; /* Read Buffer */
/*
union filesize_binfile
{
  int sizeofbin;
  u8 sizeofbin_8bit;
}filesize_bin;
*/


// structure that needs to match the first stage boot loader and uboot rodconfig coommand
struct service_block {
    service_block() : 
      HEADER_IPMAC (0xADDADD) {}   // unique ID of service header
    uint32_t filesize;
    uint32_t const HEADER_IPMAC;
    uint32_t ip;
    uint32_t serverip;
    uint8_t mac[6];
    char hostname[25];
    uint32_t netmask;
    uint32_t gateway;
    uint32_t serverip2;
};

/*****************************************************************************/
int main(int argc, char *argv[]) {
 
  using namespace SctPixelRod;

  ifstream binFile;                             // Pointer to binary file 
  unsigned long selectAddr;                     // Start address of selected flash 
  //unsigned long flashAddr;                      // Location in target flash
  int fileSize;                                 // Size of binary file in bytes 
  int selectSize;                               // Size of selected flash 
  //int iselect;

  int slot;
  string  binFileName;
  string  target;
  struct service_block service;

  auto usage = [](){
      stringstream ss;
      ss << "\nProgramVirtexFlash slot file target [--ip IP --mac MAC --hostname ROD --serverip SERVERIP --gateway GATEWAY --netmask NETMASK ] [--serverip2 SERVERIP]\n"
          << "  slot\t\t\t Vmeslot\n"
          << "  file\t\t\t Ace/img file\n"
          << "  target\t\t Master/slave"
          ;
      return ss.str();
  };

  try  {

      namespace po = boost::program_options;

      po::options_description generic = po::options_description("Generic");
      generic.add_options()
          ("help,h", "Prints this help")
          ("dry,d",  "Dry run");

      po::options_description hidden = po::options_description("Hidden options");
      hidden.add_options()
                      ("file_slot", po::value< vector<string> >(),
                      "bin/ace file, vme slot ");

      po::options_description network = po::options_description("Network configuration");
      network.add_options()
          ("ip" , po::value<string>(), "Ip address in format X.X.X.X or 0xX")
          ("mac", po::value<string>(), "Mac address in format XX:XX:XX:XX:XX:XX")
          ("hostname", po::value<string>(), "Rod name, e.g. rod_c3_s18, rod-pix-l1-s05-master")
          ("serverip", po::value<string>(), "Tftp/nfs server ip")
          ("gateway", po::value<string>(), "Gateway ip")
          ("netmask", po::value<string>(), "Netmask")
          ("serverip2", po::value<string>()-> default_value("0.0.0.0"), "Backup Tftp/nfs server ip");

      po::options_description cmdline_options;
      cmdline_options.add(generic).add(network).add(hidden);

      po::options_description visible;
      visible.add(generic).add(network);

      po::positional_options_description p;
      p.add("file_slot", -1);

      po::variables_map vm;
      store(po::command_line_parser(argc, argv).
        options(cmdline_options).positional(p).run(), vm);
      notify(vm);

      if(vm.count("help")) {
          cout << usage() << "\n" << visible << endl;
          return 0;
      }

      // check validity slot file target

      if( !vm.count("file_slot"))
          throw po::error("Specify: slot, file, target");

      auto & file_slot = vm["file_slot"].as< vector<string> >();

      if(file_slot.size()!=3)
          throw po::error("Exactly three position-free arguments required: slot, file, target");

      stringstream ss; ss<< file_slot[0];
      // test string to int conversion and basic range check
      if( ! (ss >> slot) || (slot>21 || slot <5|| slot ==13) )
          throw po::error("Invalid slot number");

      binFileName = file_slot[1];
      target 	  = file_slot[2];

      binFile.open(binFileName.c_str(), ios::binary);
      if (!binFile.is_open()) {
          throw po::error("File does not exist");
      }
      binFile.close();

      if(!(target == "master" || target == "slave")) {
          throw po::error("Target needs to be \"master\" or \"slave\"");
      }

      bool net_one = vm.count("mac") || vm.count("ip") || vm.count("hostname") 
                    || vm.count("serverip") || vm.count("gateway") || vm.count("netmask");

      bool net_all = vm.count("mac") & vm.count("ip") & vm.count("hostname") 
                    & vm.count("serverip") & vm.count("gateway") & vm.count("netmask");

      if(target == "slave" && net_one)
          throw po::error("Network configuration only allowed for master");

      // check validity of networking options
      if( net_one && ! net_all)
          throw po::error("Network configuration requires all options specified");

      if( vm.count("mac")) {

          // mac x:x:x:x:x:x -> uint8[6], error if !0
          if(mac_str2array(vm["mac"].as<string>(), service.mac))
              throw po::error("Mac address has wrong format");

          // ip - x.x.x.x -> uint32, error if !0
          if(ip_str2int(vm["ip"].as<string>(), service.ip))
              throw po::error("Ip address has wrong format");
          if(ip_str2int(vm["serverip"].as<string>(), service.serverip))
              throw po::error("Serverip address has wrong format");
          if(ip_str2int(vm["gateway"].as<string>(), service.gateway))
              throw po::error("Gateway address has wrong format");
          if(ip_str2int(vm["netmask"].as<string>(), service.netmask))
              throw po::error("Netmask has wrong format");
          if(vm.count("serverip2"))
             if(ip_str2int(vm["serverip2"].as<string>(), service.serverip2))
                throw po::error("Serverip2 has wrong format");



          // hostname
          strcpy(service.hostname, vm["hostname"].as<string>().c_str());
      }

      // options parsed - summary
      cout << "\nConfiguration:\n";
      cout << "Slot: " << slot << "\n"
                << "File: " << binFileName << "\n"
                << "target: " << target << "\n";
      if(net_one) {
          cout << "hostname: " << service.hostname << endl;
          // net config
          printf("ip: %s (%x)\n", vm["ip"].as<string>().c_str(), service.ip);
          printf("serverip: %s (%x)\n", vm["serverip"].as<string>().c_str(), service.serverip);
          printf("serverip2: %s (%x)\n", vm["serverip2"].as<string>().c_str(), service.serverip2);
          printf("netmask: %s (%x)\n", vm["netmask"].as<string>().c_str(), service.netmask);
          printf("gateway: %s (%x)\n", vm["gateway"].as<string>().c_str(), service.gateway);
          cout << "mac: " << vm["mac"].as<string>() << " (" ;
          for(int i=0; i<5; i++) cout << hex << (int)service.mac[i] << ":";
          cout << hex << (int)service.mac[5] << dec <<")\n";
      }

      if(vm.count("dry")) return 0;

  }
  catch (exception &e)
  {
      cout << e.what() << "\n"
           << usage() << "\n";
      return 1;
  }


  VmeUtils::checkPrograms("ProgramVirtexFlash");

  VmeUtils::VmeUartControl( slot , true);//lock VME UART

// Create VME interface
 RCCVmeInterface *vme = 0;
 try {
   vme = new RCCVmeInterface();

 } catch (...) {
 
   cout << "Caught an exception when creating  VME interface." << endl; 
 } 
  if(vme == 0)return 0; 
 
  // Create PPC interface through VME 
  
  unsigned long baseAddress = (slot << 24);
 const unsigned long mapSize     = 0x01000000;
   
try {
 thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
 // thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
 // by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...

 }  catch (...) {
   cout << "Caught an exception when creating Ppc interface." << endl;
 }


/*
	 * Initialize the SPI driver so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h.
	 */
	 int Status;

    cout << "Initialize XSpi: " ;
	Status = XSpi_Initialize(&Spi, SPI_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		cout << " Failed" << endl;
		 return 1;
	}
	cout << " Done" << endl;
	
	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	cout << "Start XSpi driver: " ;
	Status = XSpi_Start(&Spi);
	if(Status != XST_SUCCESS) {
		 cout << " Failed" << endl;
		return 1;
	}
	cout << " Done" << endl;

	/*
	 * Disable Global interrupt to use the Spi driver in polled mode
	 * operation.
	 */
	XSpi_IntrGlobalDisable(&Spi);

	/*
	 * Initialize the In-system and Serial Flash Library.
	 */
	cout << "Initialize ISF Library: " ;
	Status = XIsf_Initialize(&Isf, &Spi, ISF_SPI_SELECT, IsfWriteBuffer);
	if(Status != XST_SUCCESS) {
		cout << " Failed" << endl;
		return 1;
	}
	cout << " Done" << endl;
	
        u8 DeviceInfo[5];
	cout << "Checking Flash Infos: \n" ;
	//Isf.IsReady = TRUE;
	Status = XIsf_GetDeviceInfo(&Isf, DeviceInfo);
	//Isf.IsReady = FALSE; 
	if(Status != XST_SUCCESS || DeviceInfo[1]!= 31 || DeviceInfo[2] != 40 || DeviceInfo[3] != 0) {
		cout << " Failed, Aborting..." << endl;
		return 1;
	}
	if(DeviceInfo[4]==1) {
		cout << "AT45DB641E Flash identified; Page size = 264 bytes" << endl;
		ISF_PAGE_SIZE = 264;
	}
	else if(DeviceInfo[4]==0) {
		cout << "AT45DB642D Flash identified; Page size = 1056 bytes" << endl;
		ISF_PAGE_SIZE = 1056;
	}
	else {
		printf("Error in reading Flash Infos\r\n");
		return XST_FAILURE;
	}	

	// Prepare Flash File// start address where SREC file is stored
	unsigned long flashStartPage= 0;
	if (target=="slave")flashStartPage = FLASH_HALF/ISF_PAGE_SIZE;
	const unsigned long flashStart = 0x0;//FLASH_START_ADDRESS; 
	const long flashSize = SREC_FILE_SIZE; 
	
	cout << "Loading flash to ROD\n"; 
	selectAddr = flashStart; 
	selectSize = flashSize; 
	
	binFile.open(binFileName.c_str(), ios::binary); 
	if (!binFile.is_open()) { 
	  cout << "Unable to open binary file." << endl; 
	  exit(1); 
	} 
	
	// Get size of file 
	binFile.seekg(0, ios::end);           // go to end of file 
	fileSize = binFile.tellg();          // file size given by current location 
	binFile.seekg(0, ios::beg);          // go back to beginning of file 
	selectSize = fileSize;
    service.filesize=fileSize;


	// Create a buffer and read file into it 
	u8 * buffer; 
	try { 
	  buffer = new u8[fileSize]; 
	} 
	catch (bad_alloc & ba) { 
	  cout << "Unable to allocate buffer for binary file." << endl; 
	  exit(2); 
	} 
	binFile.read((char *)buffer, fileSize); 
	
	// prepare loop over flash pages 
	
	int numofpages = (int) (fileSize/ISF_PAGE_SIZE) +( (fileSize%ISF_PAGE_SIZE != 0)? 1 : 0); 
	long remainingSize = selectSize; 
	const u32 relative_start_address = selectAddr; 
	XIsf_WriteParam WriteParam;
	XIsf_ReadParam ReadParam;
	u32 Address; 
    cout << "Will be written " << fileSize << " bytes on " << numofpages;
	cout << " flash pages (" << ISF_PAGE_SIZE << " bytes per page)" << endl; 

	for(int ipage = 0; ipage < numofpages; ipage++,remainingSize-=ISF_PAGE_SIZE){ 
	  
	  
	  if(ISF_PAGE_SIZE == 1056){
	  	Address = (((ipage + flashStartPage) & 0x1FFF) << 11) +  (relative_start_address );     //& 0x7FF); 
	  }
	  else if(ISF_PAGE_SIZE == 264){
	  	Address = (((ipage + flashStartPage) & 0x7FFF) << 9) +  (relative_start_address );     //& 0x7FF); 
	  }
	  else {
		cout << "Flash Page Size undefined" << endl;
		return 1;
	  }	  
	  //prepare data to be written on page 
	  
	  WriteParam.Address = Address;
	  WriteParam.NumBytes = (remainingSize < ISF_PAGE_SIZE)? remainingSize : ISF_PAGE_SIZE;
     WriteParam.WritePtr = buffer+ISF_PAGE_SIZE*ipage;

	  /*
	   * Perform the Write operation.
	   */
	  Status = XIsf_Write(&Isf, XISF_WRITE, (void*) &WriteParam);
	  if(Status != XST_SUCCESS) {
	    cout << hex << WriteParam.Address << endl;
	    cout << hex << WriteParam.NumBytes << endl;
	    cout << hex << WriteParam.WritePtr << endl;
	    cout << "Failure writing page " << ipage << " ... Abort!" << endl;
	    return 1;
	  }
	  
	  /*
	   * Wait till the Serial Flash is ready.
	   */
	  Status = IsfWaitForFlashNotBusy();
	  if(Status != XST_SUCCESS) {
	    cout << "Failure on wating NotBusy after writing page  " << ipage << " ... Abort!" << endl;
	    return 1;
	  } 
	  
	  ReadParam.Address = Address;
	  ReadParam.NumBytes = (remainingSize < ISF_PAGE_SIZE)? remainingSize : ISF_PAGE_SIZE;
	  ReadParam.ReadPtr = gReadBuffer;
	  
	  /*
	   * Perform the Read operation.
	   */
	  Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
	  if(Status != XST_SUCCESS) {
	    cout << "Failure reading page " << ipage << " ... Abort!" << endl;
	    return 1;
	    
	  }
	  
	  /*
	   * Compare the Data Read with the Data Written to the Serial Flash.
	   */
	  for(int Index = 0; Index < (int) ReadParam.NumBytes; Index++) {
	    if(ReadParam.ReadPtr[Index + XISF_CMD_SEND_EXTRA_BYTES] != WriteParam.WritePtr[Index]) {
	      cout << "Failure checking BYTE " << Index << " on page " << ipage << endl;
	      cout << "Written 0x" << hex << WriteParam.WritePtr[Index] ;
	      cout << "  - Read 0x" << hex << ReadParam.ReadPtr[Index + XISF_CMD_SEND_EXTRA_BYTES] << "... Abort!" << endl;
	      return 1;
	    }
	  }
	  
      cout << "\rProcessed pages = " << dec <<  ipage+1 << '/' << dec << numofpages << flush;
	  
	  
	} // end loop over pages 



   // Flash service information
   cout << "\nFlashing service configuration\n";

   if(ISF_PAGE_SIZE == 1056){
       if(target == "master")Address = ((4095 & 0x1FFF) << 11);
       else Address = ((8190 & 0x1FFF) << 11);
   }
   else if(ISF_PAGE_SIZE == 264){
       if(target == "master") Address = (16380 << 9);
       else Address = (32760 << 9);
   }
   else {
       cout << "Flash Page Size undefined" << endl;
       return 1;
   }
	
	
	Status = IsfWaitForFlashNotBusy();
	
	if(Status != XST_SUCCESS) {
	  //cout << "Failure on wating NotBusy after erasing page " << ipage << " ... Abort!" << endl;
	  return 1;
	} 
	
	//prepare data to be written on page 
	
    uint32_t offset = 0;
    /*
    uint32_t ip  = 0xc0a80dda;
    uint8_t mac[6] = {0x00, 0x0a, 0x50, 0x00, 0x3, 0x18};
    char rodid[15] = "ROD_C3_S18";
    */

    memcpy(buffer+offset, &service.filesize, sizeof(service.filesize)); offset+= sizeof(service.filesize);
    if(service.ip) {
        cout << "Network configuration included in service block\n";
        memcpy(buffer+offset, &service.HEADER_IPMAC, sizeof(service.HEADER_IPMAC)); offset+=sizeof(service.HEADER_IPMAC);
        memcpy(buffer+offset, &service.ip, 		  sizeof(service.ip)); offset+=sizeof(service.ip);
        memcpy(buffer+offset, &service.serverip, 	  sizeof(service.serverip)); offset+=sizeof(service.serverip);
        memcpy(buffer+offset, &service.mac, 		  sizeof(service.mac)); offset+=sizeof(service.mac);
        memcpy(buffer+offset, &service.hostname, 	  sizeof(service.hostname)); offset+=sizeof(service.hostname);
        memcpy(buffer+offset, &service.netmask, 	  sizeof(service.netmask)); offset+=sizeof(service.netmask);
        memcpy(buffer+offset, &service.gateway, 	  sizeof(service.gateway)); offset+=sizeof(service.gateway);
        memcpy(buffer+offset, &service.serverip2, 	  sizeof(service.serverip2)); offset+=sizeof(service.serverip2);
    }
    cout << "Will be written " << offset << "bytes on 1 flash page" <<  endl;


    WriteParam.Address = Address;
    WriteParam.NumBytes = offset;
    WriteParam.WritePtr = buffer;

    /*
     * Perform the Write operation.
     */
    Status = XIsf_Write(&Isf, XISF_WRITE, (void*) &WriteParam);
    if(Status != XST_SUCCESS) {
      cout << hex << WriteParam.Address << endl;
      cout << hex << WriteParam.NumBytes << endl;
      cout << hex << WriteParam.WritePtr << endl;
      //cout << "Failure writing page " << ipage << " ... Abort!" << endl;
      return 1;
    }

    Status = IsfWaitForFlashNotBusy();

    if(Status != XST_SUCCESS) {
      //cout << "Failure on wating NotBusy after erasing page " << ipage << " ... Abort!" << endl;
      return 1;
    }

	
	try{
	  delete thePpc;
	}  catch (...) {
	  cout << "Caught an exception when deleting Ppc interface." << endl;
	}
	
    if(buffer) delete buffer;

	
	VmeUtils::VmeUartControl( slot , false);//Unlock VME UART
	return 0;  
	
}


/*****************************************************************************/
/**
 *
* This function waits till the Serial Flash is ready to accept next command.
*
* @param	None
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
static int IsfWaitForFlashNotBusy()
{
	int Status;
	u8 StatusReg;
	u8 ReadBufferArr[2];

	while(1) {

		/*
		 * Get the Status Register.
		 */
		Status = XIsf_GetStatus(&Isf, ReadBufferArr);
		if(Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Check if the Serial Flash is ready to accept the next
		 * command. If so break.
		 */
		StatusReg = ReadBufferArr[BYTE2];
		if(StatusReg & XISF_SR_IS_READY_MASK) {
			break;
		}
	}

	return XST_SUCCESS;
}


void VMEUARTControl( int slotNr, bool lock){

	//Shared memory for control kill program and flashing
  // ftok to generate unique key 
  key_t key = ftok("shmfile",65); 
  // shmget returns an identifier in shmid 
  int shmid = shmget(key,1024,0666|IPC_CREAT); 
  // shmat to attach to shared memory 
  char *str = (char*) shmat(shmid,(void*)0,0);

  uint32_t slotConnMask,slotMask,slaveMaskN,slaveMaskS;
  if( sscanf(str,"0x%08X 0x%08X 0x%08X 0x%08X",&slotConnMask,&slotMask,&slaveMaskN,&slaveMaskS) != 4 ){
    cout<<"Program not running or memory corruption happened since I got "<<str<<endl;
    shmdt(str);
    return;
  }

  //cout<<"I got readback mask "<<hex<<slotConnMask<<"SlotMask "<<slotMask<<dec<<endl;

  if((slotConnMask  & (1 << slotNr)) == 0){
  cout<<"Slot "<<slotNr<<" is not supported in the current connectivityMask "<<hex<<slotConnMask<<"SlotMask "<<slotMask<<dec<<endl;
  cout<<"Doing nothing"<<endl;
  } else {
  uint32_t mask = (1 << slotNr);
    if(!lock){
    cout<<"Unlocking UART"<<endl;
    slotMask |= mask;
    } else {
    cout<<"Locking UART"<<endl;
    slotMask &= ~mask;
    }
  sprintf(str, "0x%08X 0x%08X 0x%08X 0x%08X",slotConnMask,slotMask,slaveMaskN,slaveMaskS); 
  cout<<"Data written in memory: "<<str<<endl;
  }
  //detach from shared memory  
  shmdt(str);
  sleep(1);

}


//! Tokenize the given string str with given delimiter. If no delimiter is given whitespace is used.
//local function
void tokenize(const string& str, vector<string>& tokens, const string& delimiters)
{
    tokens.clear();
    // Skip delimiters at beginning.
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (string::npos != pos || string::npos != lastPos)
    {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        // Skip delimiters.  Note the "not_of"
        lastPos = str.find_first_not_of(delimiters, pos);
        // Find next "non-delimiter"
        pos = str.find_first_of(delimiters, lastPos);
    }
}


//! Convert string ip to uint32, fail if !0
int ip_str2int(const string &str, uint32_t &ip)
{
    int ndot = count_if( str.begin(), str.end(), [](char c){ return c =='.';});
    if(ndot!=3) return 1;

    vector<string> tokens;
    tokenize(str, tokens, ".");
    if(tokens.size()!=4) return 2;
    ip = 0;
    // create uint32
    for(int i=0; i< 4; i++) {
        try {
            ip |= (stoi(tokens[i])&0xFF) << 8*(3-i) ;
        } catch (exception &e) {
            return 3;
        }

    }
    return 0;
}


//! Convert string mac to uint32[6], fail if !0
int mac_str2array(const string &str, uint8_t* mac)
{

    int ncollon = count_if( str.begin(), str.end(), [](char c){ return c ==':';});
    if(ncollon!=5) return 1;

    vector<string> tokens;
    tokenize(str, tokens, ":");
    if(tokens.size()!=6) return 1;
    for(int i=0; i< 6; i++) {
        try {
            mac[i] = stoi(tokens[i], 0, 16);
        } catch (exception &e) {
            return 1;
        }
    }

    return 0;
}

