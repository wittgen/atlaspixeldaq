
/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/

/***************************** Include Files *********************************/

#include "rodMaster.hxx"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <iostream>
#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
#include "xparameters.h"
#include <stdlib.h>
#include "counterNames.h"
#include <iomanip>
#include "xil_io.h"

using namespace SctPixelRod; 
using namespace std;
/************************** Constant Definitions *****************************/

//#define XPAR_XPS_EPC_0_PRH0_BASEADD 0x40000000
//#define SLV_A_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x0)
//#define SLV_B_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x40000)
#define CNT_CONFIG_REG  0x06D8
#define CNT_START 0x0600
#define CNT_TOT_DESYNCH 0x0750
#define CNT_RES_DESYNCH 0x0790
#define CNT_SKIPPED 0x07D0


/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
static XIsf Isf;
int main(int argc, char *argv[]) 
{ 
	volatile unsigned int epcSlvARegs;
	volatile unsigned int epcSlvBRegs;
	epcSlvARegs = (volatile unsigned int )SLV_A_REG_BASE;
	epcSlvBRegs = (volatile unsigned int )SLV_B_REG_BASE;
	unsigned int slot;
	if(argc > 1)
	{
		sscanf(argv[1],"%d",&slot);
	}

	else
	{
		printf("Error: no slot number\n");
		return 1;
	}
        

	// Create VME interface
	RCCVmeInterface *vme = 0;
	try 
	{
		vme = new RCCVmeInterface();

	} 
	catch (...) 
	{
		std::cout << "Caught an exception when creating  VME interface." << std::endl; 
	} 
	
	if(vme == 0)
		return 0; 

	// Create PPC interface through VME 

	unsigned long baseAddress = (slot << 24);
	const unsigned long mapSize     = 0x01000000;

	try 
	{
		thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
		// thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
		// by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...
	} 
	 catch (...) 
	{
		std::cout << "Caught an exception when creating Ppc interface." << std::endl;
	}


	//reading cnt CONFIG
	int cntConfig_A = thePpc->ppcSingleRead(epcSlvARegs + CNT_CONFIG_REG*4);
	int cntConfig_B = thePpc->ppcSingleRead(epcSlvBRegs + CNT_CONFIG_REG*4);
	int register_value, N_TRIG_A, N_TRIG_B;
	
	//   enabling cntRead
	cntConfig_A = cntConfig_A | (1<<30);
	cntConfig_B = cntConfig_B | (1<<30);
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);     

	cout << "cntConfigA: 0x" << hex << cntConfig_A<<endl;
	cout << "cntConfigB: 0x" << hex << cntConfig_B<<endl;
	
	N_TRIG_A = cntConfig_A & 0xFFFFFF;
	N_TRIG_B = cntConfig_B & 0xFFFFFF;

	if(cntConfig_A & 0x80000000)
	{
		cout << "WARNING: counters on slave North are in reset state\n";
	}
	if(cntConfig_B & 0x80000000)
	{
		cout << "WARNING: counters on slave South are in reset state\n";
	}
	cout << dec;
	//   readind counters
	cout.width(144); cout  <<left<<"_______________________________________________________________________________________________________________________________________________\n";
	cout.width(144); cout  <<left<<"|                 Slave A                                              |                       Slave B                                        |\n";
	cout.width(144); cout  <<left<<"|______________________________________________________________________|______________________________________________________________________|\n";
	cout.width(144); cout  <<left<<"| Link |    N events   |  Average Occ  |  Tot Desynch  |  Res Desynch  | Link |    N events   |  Average Occ  |  Tot Desynch  |  Res Desynch  |\n";
	cout.width(144); cout  <<left<<"|______|_______________|_______________|_______________|_______________|______|_______________|_______________|_______________|_______________|\n";
	for(int i=0; i<16; i++)
	{	
		cout <<"|  ";
		cout.width(2); cout  <<right<<i;
		cout <<"  | ";
		register_value = thePpc->ppcSingleRead(epcSlvARegs + CNT_START*4+i*16);
		N_TRIG_A = thePpc->ppcSingleRead(epcSlvARegs + CNT_START*4+(i+16)*16);
		cout.width(13); cout  <<right<< N_TRIG_A;
		cout <<" | ";
		cout.width(13); cout << setprecision(6) << (N_TRIG_A == 0 ? 0 :(double)(register_value)/(double)(N_TRIG_A));
		cout <<" | ";
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvARegs + CNT_TOT_DESYNCH*4+i*16);
		cout <<" | ";		
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvARegs + CNT_RES_DESYNCH*4+i*16);
		cout <<" |  ";
		cout.width(2); cout  <<right <<i;
		cout <<"  | ";
		register_value = thePpc->ppcSingleRead(epcSlvBRegs + CNT_START*4+i*16);
		N_TRIG_B = thePpc->ppcSingleRead(epcSlvBRegs + CNT_START*4+(i+16)*16);
		cout.width(13); cout  <<right<< N_TRIG_B;
		cout <<" | ";
		cout.width(13); cout << setprecision(6) << (N_TRIG_B == 0 ? 0 :(double)(register_value)/(double)(N_TRIG_B));
		cout <<" | ";
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvBRegs + CNT_TOT_DESYNCH*4+i*16);
		cout <<" | ";		
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvBRegs + CNT_RES_DESYNCH*4+i*16);
		cout <<" |\n";
	}
	cout.width(144); cout  <<left<<"|______|_______________|_______________|_______________|_______________|______|_______________|_______________|_______________|_______________|\n";
	cout.width(144); cout  <<left<<"|     N of skipped (all channels)      |          Occurrency           |     N of skipped (all channels)      |          Occurrency           |\n";
	cout.width(144); cout  <<left<<"|______________________________________|_______________________________|______________________________________|_______________________________|\n";
	for(int i=1; i<16; i++)
	{	
		cout <<"|                  ";
		cout.width(2); cout  <<right<<i;
		cout <<"                  |         ";
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvARegs + CNT_SKIPPED*4+i*16);
		cout <<"         |                  ";
		cout.width(2); cout  <<right<<i;
		cout <<"                  |         ";
		cout.width(13); cout << thePpc->ppcSingleRead(epcSlvBRegs + CNT_SKIPPED*4+i*16);
		cout <<"         |\n";	
	}
	
	cout.width(144); cout  <<left<<"|______________________________________|_______________________________|______________________________________|_______________________________|\n";
	//   disabling cntRead
	cntConfig_A = cntConfig_A & 0xBFFFFFFF;
	//std::cout<<cntConfig_A<<std::endl;
	cntConfig_B = cntConfig_B & 0xBFFFFFFF;
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);      
 
	cout << "cntConfigA: 0x" << hex << cntConfig_A<<endl;
	cout << "cntConfigB: 0x" << hex << cntConfig_B<<endl;

	try
	{
		 delete thePpc;
	} 
	 catch (...) 
	{
		 std::cout << "Caught an exception when deleting Ppc interface." << std::endl;	
	} 


	return 0;  
	
}

