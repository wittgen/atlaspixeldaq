#define TRUE 1
#define FALSE 0
#define STATUS_ADDR 0


#define LATTICE_REG 0xC00070 


#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <time.h>
#include <signal.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

unsigned int error_number;

unsigned long slot_number;
unsigned long result,data32;
int devHandle;
int target=-1;
#if __x86_64__
unsigned long* vmememory;
#else
unsigned int* vmememory;
#endif
VME_ErrorCode_t error_code;
VME_MasterMap_t master_map;

void signal_callback_handler(int signum){

  if (VME_MasterUnmap(devHandle) != VME_SUCCESS)
    fprintf(stderr,"Error - VME_MasterUnmap failed\n");
  VME_Close();
  exit (0);
}



int main (int argc, char *argv[])
{
  
  if(argc < 2)
  {
	 std::cout << "No slot number provided. Please run: \n\tLattice_check_refsel $VME_SLOT\n";
     return -1;
  }

  signal(SIGINT,signal_callback_handler);
  slot_number=atoi(argv[1]);
   
    
  if (VME_Open() != VME_SUCCESS)
    devHandle= -1;
  else
    devHandle=0;


  unsigned long vme_address=slot_number<<24;
 
  master_map.vmebus_address = vme_address & 0xff000000;
  vme_address &= 0x00ffffff; //Da CAMBIARE!
  master_map.window_size =    0x01000000;
  master_map.address_modifier = VME_A32;
  master_map.options = 0;

  if (VME_MasterMap(&master_map, &devHandle) != VME_SUCCESS)
    return -1;//result=-1;//
  else
    result=0;
#if __x86_64__
  if (VME_MasterMapVirtualLongAddress(devHandle, (unsigned long*)&vmememory) != VME_SUCCESS)
#else
  if (VME_MasterMapVirtualAddress(devHandle, (unsigned int *)&vmememory) != VME_SUCCESS)
#endif
    return -1;//result= -1;//
  else
    result=0;


 // vme_address=vme_address+ 0xc00000; // Internal PRM register_block offset negative!!! //DA CAMBIARE
  VME_WriteSafeUInt(devHandle,vme_address+LATTICE_REG, 0x00000001);
  //vmememory[LATTICE_REG/4] = 0x00000001;
  sleep(2);


  unsigned int status;
  VME_ReadSafeUInt(devHandle,vme_address+LATTICE_REG, &status);
  std::cout<< std::hex << "Status: 0x" << status <<std::dec << std::endl;
  if (status & 0xFFFFFFFC)
  {
     std::cout<<"Error!! VME return value not recognized, aborting\n";
     VME_WriteSafeUInt(devHandle,vme_address+LATTICE_REG, 0x00000000);

     if (VME_MasterUnmap(devHandle) != VME_SUCCESS)
       fprintf(stderr,"Error - VME_MasterUnmap failed\n");
     VME_Close();
   
     return -2;
  }
  if(status & 0x00000002)
	std::cout<<"Warning!! Lattice NOT LOCKED\n";
  else
	std::cout<<"Lattice LOCKED\n";
  if(status & 0x00000001)
	std::cout<<"Using LOCAL CLOCK\n";
  else
	std::cout<<"Using BOC CLOCK\n";

  VME_WriteSafeUInt(devHandle,vme_address+LATTICE_REG, 0x00000000);

  if (VME_MasterUnmap(devHandle) != VME_SUCCESS)
    fprintf(stderr,"Error - VME_MasterUnmap failed\n");
  VME_Close();
   
  return 0;
}
