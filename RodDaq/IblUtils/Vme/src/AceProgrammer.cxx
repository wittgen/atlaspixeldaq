#define TRUE 1
#define FALSE 0
#define STATUS_ADDR 0


#define ACE_CTRL_REG 0xC00068 
#define ACE_STATUS_REG 0xC0006c
#define ACE_DATA_REG 0xD00000 //?????

#include <iostream>

#include <signal.h>
#include <unistd.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"
#include "VmeUtils.hh"

unsigned int error_number;

unsigned int slot_number;
unsigned int result,data32;
int devHandle;
int target=-1;
#if __x86_64__
unsigned int* vmememory;
#else
unsigned long* vmememory;
#endif
VME_ErrorCode_t error_code;
VME_MasterMap_t master_map;

void signal_callback_handler(int signum){
  VME_WriteSafeUInt(devHandle,ACE_CTRL_REG,0x00000003);
  //vmememory[ACE_CTRL_REG/4]=0x00000003;

  if (VME_MasterUnmap(devHandle) != VME_SUCCESS)
    fprintf(stderr,"Error - VME_MasterUnmap failed\n");
  VME_Close();
  exit (0);
}

int main (int argc, char *argv[])
{
  
  signal(SIGINT,signal_callback_handler);
  slot_number=atoi(argv[1]);
   
    
  if (VME_Open() != VME_SUCCESS)
    devHandle= -1;
  else
    devHandle=0;

  VmeUtils::checkPrograms("AceProgrammer");
  
  VmeUtils::VmeUartControl( slot_number , true);//lock VME UART
  sleep(1);

  unsigned int vme_address=slot_number<<24;
 
  master_map.vmebus_address = vme_address & 0xff000000;
  vme_address &= 0x00ffffff; //Da CAMBIARE!
  master_map.window_size =    0x01000000;
  master_map.address_modifier = VME_A32;
  master_map.options = 0;

  if (VME_MasterMap(&master_map, &devHandle) != VME_SUCCESS)
    return -1;//result=-1;//
  else
    result=0;
#if __x86_64__
  if (VME_MasterMapVirtualLongAddress(devHandle, (unsigned long*)&vmememory) != VME_SUCCESS)
#else
    if (VME_MasterMapVirtualAddress(devHandle, (unsigned int *)&vmememory) != VME_SUCCESS)
#endif
      return -1;//result= -1;//
  else
    result=0;


  //  vme_address=vme_address; // Internal PRM register_block offset negative!!! //DA CAMBIARE

  std::streampos size, buff_size;
  char * memblock;
  
  std::ifstream file (argv[2],std::ios::in|std::ios::binary|std::ios::ate);
  if (file.is_open())
    {
      size = file.tellg();
      memblock = new char [size];
      file.seekg (0, std::ios::beg);
      file.read (memblock, size);
      file.close();
    }
  else
    return -2;
  //std::cout<<"prova0"<<std::endl;
  //std::cout.flush();
  VME_WriteSafeUInt(devHandle,vme_address+ACE_CTRL_REG,0x00000003);
  //vmememory[vme_address+ACE_CTRL_REG/4]=0x00000003;
  //sleep(1);
  unsigned int status=0;
  VME_ReadSafeUInt(devHandle,vme_address+ACE_STATUS_REG,&status);
  //unsigned int status=vmememory[vme_address+ACE_STATUS_REG/4];
  //  std::cout<<"status= "<<std::hex<<status<<std::endl;
  if ((status & 0x1)!= 0)
    std::cout<<"error while reading back TDO\n";
  if ((status & 0x2)!= 0)
    std::cout<<"DONE (this is asserted with or without errors)\n";
  if ((status & 0x4)!= 0)
    std::cout<<"FIFO empty\n";
  if ((status & 0x8)!= 0)
    std::cout<<"FIFO full\n";
  std::cout<<"the state machine is at point 0x"<<std::hex<<(status >> 4)<<std::dec<<std::endl;
  VME_WriteSafeUInt(devHandle,vme_address+ACE_CTRL_REG,0x00000000);
  //vmememory[vme_address+ACE_CTRL_REG/4]=0x00000000;
  //sleep(1);
  std::cout<<"PROGRAMMING"<<std::endl;
  unsigned int i=0;
  VME_ReadSafeUInt(devHandle,vme_address+ACE_STATUS_REG,&status);
  //status=vmememory[vme_address+ACE_STATUS_REG/4];
  //  std::cout<<"status= "<<std::hex<<status<<std::endl;
  if ((status & 0x1)!= 0)
    std::cout<<"error while reading back TDO\n";
  if ((status & 0x2)!= 0)
    std::cout<<"DONE (this is asserted with or without errors)\n";
  if ((status & 0x4)!= 0)
    std::cout<<"FIFO empty\n";
  if ((status & 0x8)!= 0)
    std::cout<<"FIFO full\n";
  std::cout<<"the state machine is at point 0x"<<std::hex<<(status >> 4)<<std::dec<<std::endl;
  //sleep(1);
  std::cout<<"the size is : "<<size<<std::endl;

  while ( i<size && (status & 0x3)==0 ) 
    {
      //      sleep(1);
      if (i+3 <= size)
	{
	  data32=0;
	  //std::cout<<"memblock["<<i<<"]= "<<std::hex<<(int)memblock[i]<<(int)memblock[i+1]<<(int)memblock[i+2]<<(int)memblock[i+3]<<std::endl;
	  data32= ((memblock[i]<<24)&0xff000000) | ((memblock[i+1]<<16)&0x00ff0000) | ((memblock[i+2]<<8)&0x0000ff00) | ((memblock[i+3])&0x000000ff);
	  //std::cout<<"data= "<<std::hex<<data32<<std::endl;
	}
      else
	{

	  data32=0;
	  for (int j=0; j<4; j++)
	    if (i+j < size )
	      {
	        //std::cout<<"data is "<<std::hex<<(int)memblock[i+j]<<std::endl;
		//std::cout.flush();
		data32|=(memblock[i+j]<<(24-j*8));
	      }
	      
	}
      //      i=i+4;
      if ( (status & 0x4) != 0 )
	{
	  //std::cout<<"dato 32= "<<std::hex<<data32<<std::endl;
	  VME_WriteSafeUInt(devHandle,vme_address+ACE_DATA_REG,data32);
	  //vmememory[vme_address+ACE_DATA_REG/4]=data32;
	  i=i+4;
	  //std::cout.flush();
	}
      VME_ReadSafeUInt(devHandle,vme_address+ACE_STATUS_REG,&status);
      //status=//vmememory[vme_address+ACE_STATUS_REG/4];

   if(i%10000==0)std::cout << "\rProgress = " << i << '/' << size << std::flush;
      if ((status & 0x3)){
	//	std::cout<<"STATUS= "<<std::hex<<(status & 0x3)<<std::endl;
	   std::cout<<std::endl;
	    if ((status & 0x1)!= 0)
	      std::cout<<"error while reading back TDO\n";
	    if ((status & 0x2)!= 0)
	      std::cout<<"DONE (this is asserted with or without errors)\n";
	    if ((status & 0x4)!= 0)
	      std::cout<<"FIFO empty\n";
	    if ((status & 0x8)!= 0)
	      std::cout<<"FIFO full\n";
	     std::cout<<"the state machine is at point 0x"<<std::hex<<(status >> 4)<<std::dec<<std::endl;
	   }

    }


  while ((status & 0x03) == 0)
    {
      VME_ReadSafeUInt(devHandle,vme_address+ACE_STATUS_REG,&status);
      //      status=//vmememory[ACE_STATUS_REG/4];
      //std::cout<<std::setw(10)<<"\rSTATUS= "<<std::setw(10)<<std::hex<<(status);
      if ((status & 0x1)!= 0)
	std::cout<<"error while reading back TDO\n";
      if ((status & 0x2)!= 0)
	std::cout<<"DONE (this is asserted with or without errors)\n";
      if ((status & 0x4)!= 0)
	std::cout<<"FIFO empty\n";
      if ((status & 0x8)!= 0)
	std::cout<<"FIFO full\n";
      std::cout<<"the state machine is at point 0x"<<std::hex<<(status >> 4)<<std::dec<<std::endl;
    }
  //  std::cout<<"STATUS= "<<std::hex<<(status & 0x3)<<std::endl;
  //  std::cin.get();
  std::cout<<std::endl;
  
  VME_WriteSafeUInt(devHandle,vme_address+ACE_CTRL_REG,0x3);
  //vmememory[vme_address+ACE_CTRL_REG/4]=0x00000003;
  delete[] memblock;

  if (VME_MasterUnmap(devHandle) != VME_SUCCESS)
    fprintf(stderr,"Error - VME_MasterUnmap failed\n");
  VME_Close();
  
  VmeUtils::VmeUartControl( slot_number , false);//Unlock VME UART
   
  return 0;
}
