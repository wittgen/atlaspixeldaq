#include "RCCVmeInterface.h"
#include "VmeUtils.hh"

std::vector <std::string> supportedSBCs;

void help(){

  std::cout<<"This program control the VME UART reader and runs on an SBC"<<std::endl;
  std::cout<<"Usage VmeUartControl -s slotNumber -v (start|stop|master|north|south) [-e]"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"Options:"<<std::endl;
  std::cout<<"-s slotNumber"<<std::endl;
  std::cout<<"-v value [ start: start reading(remove protection), stop: stop reading (activate protection), master: read just master (release slaves), north: read slave logs, south: read slave logs]"<<std::endl;
  std::cout<<"-e : Exit the Vme UART reader program e.g. VmeUartControl -e "<<std::endl;
  std::cout<<"Example (slot 11): VmeUartControl -s 11 -v stop "<<std::endl;
  std::cout<<std::endl;

  exit(0);
}

int main(int argc, const char** argv) {

	int slotNr = 0;
	std::string value =" ";
	bool exit = false;

	if(argc>=2)
	{
		for(int i = 1; i < argc; i++)
			if( *argv[i] == '-')
			{
				argv[i]++;
					switch ( *argv[i] )
					{
						case 's' : slotNr = atoi(argv [i+1]); break;
						case 'v' : value = argv [i+1]; break;
						case 'e' : exit = true; break;
						default : help();
					}
			}
	}

  std::string hostName = VmeUtils::getHostName();
  /*
  if(hostName.find("pixrcc") == std::string::npos && hostName.find("sbc-pix-rcc") == std::string::npos){
    std::cout<<"Unsupported SBC "<<hostName<<std::endl;
    help();
  }*/

  if(!exit){
    if (value =="start" || value =="stop" || value == "north" || value == "south" || value =="master" ){
      std::cout<<"Writting "<<value <<" to slot "<<slotNr<<std::endl;
    } else {
      std::cout<<"Please provide a valid value: start/stop/north/south/master "<<std::endl;
      help();
    }

  }else {
    VmeUtils::VmeUartControl(slotNr, false, true); 
   return 0;
   }

  if(value == "start"){
    VmeUtils::VmeUartControl(slotNr, false);//Unlock UART
  } else if(value == "stop"){
    VmeUtils::VmeUartControl(slotNr, true);//Lock UART
  } else if (value == "north"){
    VmeUtils::VmeUartControlSlave(slotNr,0, false);//Unlock north
  } else if (value == "south"){
    VmeUtils::VmeUartControlSlave(slotNr,1, false);//Unlock south
  } else if (value == "master"){
    VmeUtils::VmeUartControlSlave(slotNr,0, true);//Lock north
    VmeUtils::VmeUartControlSlave(slotNr,1, true);//Lock south
  }


  return 0;

}


