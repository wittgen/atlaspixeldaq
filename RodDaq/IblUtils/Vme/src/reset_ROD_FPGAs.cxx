#include <iostream>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>

#include <time.h>

#include <ctype.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/ioctl.h>
#include "rcc_error/rcc_error.h"
#include "vme_rcc/vme_rcc.h"

#define ENABLE 0x00000040
#define PROC_CORE 0x00000020
#define MASTER 0x00000004
#define SLAVEN 0x00000002
#define SLAVES 0x00000001

unsigned int error_number;

unsigned int slot_number;
unsigned int data=0;
int result;
int devHandle;
int target=-1;
int proc_sel=-1;
unsigned long* vmememory;
VME_ErrorCode_t error_code;
VME_MasterMap_t master_map;

int main (int argc, char *argv[])
{

  if (argc != 4)
    {
      printf ("Wrong number of arguments\n");
      printf ("Usage: reset_ROD_FPGAs  <slot_number> <master/north/south/all> <reset_processor_select: yes or not> \n" );
      return 0;
    }

  slot_number=atoi(argv[1]);
  if (strcmp(argv[2], "master") == 0)
    target=1;
  else if (strcmp(argv[2], "north") == 0)
    target=2;
  else if (strcmp(argv[2], "south") == 0)
    target=4;
  else if (strcmp(argv[2], "all") == 0)
    target=7;
 
  if (strcmp(argv[3], "yes") == 0)
    proc_sel=1;
  else proc_sel=0;
    
  if (VME_Open() != VME_SUCCESS)
    devHandle= -1;
  else
    devHandle=0;


  unsigned int vme_address=slot_number<<24;
 
  master_map.vmebus_address = vme_address & 0xff000000;
  vme_address &= 0x00ffffff;
  master_map.window_size =    0x01000000;
  master_map.address_modifier = VME_A32;
  master_map.options = 0;

	for(int i = 1 ; i <= 10 ; ++i) {
		if (VME_MasterMap(&master_map, &devHandle) != VME_SUCCESS)
			result= -1;
		else
			result=0;
		
		// if(result==0){ std::cout << "Obtained VmeMap at attempt " << i << std::endl; }
		// else std::cout << "Couldn't get VmeMap at attempt " << i << std::endl;
		
		// if (VME_MasterMapVirtualLongAddress(devHandle, (unsigned long *)&vmememory) != VME_SUCCESS)
		// 	result= -1;
		// else
		// 	result=0;
		
		if(result==0){ std::cout << "Obtained VmeMap at attempt " << i << std::endl; break; }
		else std::cout << "Couldn't get VmeMap at attempt " << i << std::endl;
		sleep(1);

	}
	// Couldn't get access to the ROD over VME: not doing anything
	if(result) return 1;

  vme_address=vme_address+ 0xc00000; // Internal PRM register_block offset negative!!!

	if(target & 0x2) {
		std::cout<<"Resetting North...canguri " << (proc_sel?"and its MB":"") <<std::endl;
		VME_WriteSafeUInt(devHandle ,vme_address+0x04,ENABLE+SLAVEN+(proc_sel?PROC_CORE:0x0));
		//vmememory[vme_address+0x01]=ENABLE+SLAVEN+(proc_sel?PROC_CORE:0x0); // North & MB
	}
	if(target & 0x4) {
		std::cout<<"Resetting South... " << (proc_sel?"and its MB":"") <<std::endl;
		VME_WriteSafeUInt(devHandle ,vme_address+0x04,ENABLE+SLAVES+(proc_sel?PROC_CORE:0x0));
		//vmememory[vme_address+0x01]=ENABLE+SLAVES+(proc_sel?PROC_CORE:0x0); // South & MB
	}
	if(target & 0x1) {
		std::cout<<"Resetting Master... " << (proc_sel?"and its PPC":"") <<std::endl;
		VME_WriteSafeUInt(devHandle ,vme_address+0x04,ENABLE+MASTER+(proc_sel?PROC_CORE:0x0));
		//vmememory[vme_address+0x01]=ENABLE+MASTER+(proc_sel?PROC_CORE:0x0); // Master & PPC
	}
	
	VME_ReadSafeUInt(devHandle,vme_address+0x04,&data);
	std::cout << "Checking register value: 0x" << std::hex << data << std::dec << std::endl;
	return result;
 
  if (target==1)
    {
      if (proc_sel==1)
	{
	  std::cout<<"Resetting Master and PPC... " <<std::endl;
	  VME_WriteSafeUInt(devHandle ,vme_address+0x04,0xc1);
	  //vmememory[vme_address+0x04]=0x41+0x80;// master
	  std::cout<<"Done. " <<std::endl;
	}
      else 
	{
	  std::cout<<"Resetting Master... NO PPC will be reset" <<std::endl;
	  VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x41);
	  //vmememory[vme_address+0x01]=0x41; // master
	  std::cout<<"Done. " <<std::endl;
	}

    }
  else if (target==2)
    {
      if (proc_sel==1)      
	{
      std::cout<<"Resetting North... and its MB " <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x82);
      //vmememory[vme_address+0x01]=0x02+0x80; // north
      std::cout<<"Done. " <<std::endl;
	}
      else 
	{
      std::cout<<"Resetting North... NO MB will be reset" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x02);
      //vmememory[vme_address+0x01]=0x02; // north
      std::cout<<"Done. " <<std::endl;
	}
    }
  else if (target==4)
    {
      if (proc_sel==1)
	{      
      std::cout<<"Resetting South... and its MB " <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x84);
      //vmememory[vme_address+0x01]=0x04+0x80; // south
      std::cout<<"Done. " <<std::endl;
        }
      else
	{
	  std::cout<<"Resetting South... NO MB will be reset" <<std::endl;
	  VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x04);
	  //vmememory[vme_address+0x01]=0x04; // south
	  std::cout<<"Done. " <<std::endl;
	}
    }
  else if (target==7)
    {
      if (proc_sel==1)
	{
      std::cout<<"Resetting North... and its MB" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x82);
      //vmememory[vme_address+0x01]=0x02+0x80; // north
      std::cout<<"Resetting South... and its MB" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x84);
      //vmememory[vme_address+0x01]=0x04+0x80; // south
      std::cout<<"Resetting Master... and its PPC" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0xc1);
      //vmememory[vme_address+0x01]=0x41+0x80; // master
      std::cout<<"All done. " <<std::endl;
	}
      else
	{
      std::cout<<"Resetting North... NO MB will be reset" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x02);
      //vmememory[vme_address+0x01]=0x02; // north
      std::cout<<"Resetting South... NO MB will be reset" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x04);
      //vmememory[vme_address+0x01]=0x04; // south
      std::cout<<"Resetting Master... NO PPC will be reset" <<std::endl;
      VME_WriteSafeUInt(devHandle ,vme_address+0x04,0x41);
      //vmememory[vme_address+0x01]=0x41; // master
      std::cout<<"All done. " <<std::endl;
     }
    }
  else
    std::cout<< "Error in target";

      
  return result;
}

