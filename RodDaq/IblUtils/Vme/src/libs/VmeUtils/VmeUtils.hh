
#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <vector>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <dirent.h>
#include <memory>
#include <map>
#include <ctime>
#include <sys/stat.h>
#include <unistd.h>

namespace VmeUtils{

    void VmeUartControl(int slot, bool lock, bool exit=false);
    void VmeUartControlSlave(int slot, int slaveNr ,bool lock);
    bool getSharedMemory( int &shmid, char **str);
    std::string getHostName();
    void checkPrograms(std::string program);
    int getFileSize(std::string filename);
    std::string  getcurrentDateTime(bool fileFormat = false);
    bool mkpath( const std::string &path );

}

