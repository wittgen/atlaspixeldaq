
#include "VmeUtils.hh"
#include <sstream>

static std::map <std::string, int> cmdList = {{"AceProgrammer",0},{"ProgramVirtexFlash",0},{"VmeUartReader",-1},{"simpleVmeUart",0}};

namespace VmeUtils{

void checkPrograms(std::string program){

  std::map <std::string, int>::iterator it;

  it = cmdList.find(program);
    if (it == cmdList.end()){
      std::cout<<"Program "<<program<<" doesn't belong to the command list: "<<std::endl;
      for(it = cmdList.begin(); it!= cmdList.end();it++) std::cout<<it->first<<"; ";
      std::cout<<std::endl;
      return;
    } else cmdList[program] = -1;//Adding -1 since is the the program currently running.

  DIR* dir;
  struct dirent* ent;
  char* endptr;

   if (!(dir = opendir("/proc"))) {
      std::cerr<<"Cannot open /proc"<<std::endl;
      exit(1);
   }

    while((ent = readdir(dir)) != NULL) {
        /* if endptr is not a null character, the directory is not
         * entirely numeric, so ignore it */
      long lpid = strtol(ent->d_name, &endptr, 10);
        if (*endptr != '\0') {
            continue;
        }

        /* try to open the cmdline file */
      std::string fName =  "/proc/"+std::to_string(lpid)+"/status";

      std::ifstream fin (fName,std::ifstream::in);
        if (fin.fail()){
           std::cerr<<"Cannot open "+ fName +" skipping..."<<std::endl;
	  continue;
        }

      std::string line;
      std::getline(fin, line);
      std::stringstream str;
      str<<line;
      std::string head,binary;
      str>>head>>binary;
        for(it = cmdList.begin(); it!= cmdList.end();it++)
          if(it->first.substr(0,15) == binary){
            cmdList[it->first]++;
            std::cout<<it->first<<" pid: "<<lpid<<std::endl;//" "<<line<<" "<<binary<<" "<< it->first.substr(0,15)<<std::endl;
        }
      fin.close();
    }
    closedir(dir);

  bool anyCmdRun = false;
    for(it = cmdList.begin(); it!= cmdList.end();it++)
      if(it->second > 0){
	std::cerr<<"Another "+ it->first +" is running, exiting..."<<std::endl;
	anyCmdRun = true;
      }

  if(anyCmdRun)exit(1);

}

std::string getHostName(){

  // Getting host name
  char hostname[200];
  if( gethostname( hostname, sizeof(hostname) ) ) {
    std::cout<<"ERROR cannot get hostName"<<std::endl;
    exit(1);
  }

 return std::string(hostname);

}

bool getSharedMemory( int &shmid, char **str){

  // ftok to generate unique key
  static key_t key = ftok("shmfile",65);

  try{
    // shmget returns an identifier in shmid
    shmid = shmget(key,1024,0666|IPC_CREAT);
    // shmat to attach to shared memory
    *str = (char*) shmat(shmid,(void*)0,0);
  } catch(...) {
    std::cerr << "Cannot get shared memory!!!" << std::endl;
    return false;
  }

return true;

}

void VmeUartControl(int slot, bool lock, bool exit){

  int shmid;
  char *str = NULL;

    if(!getSharedMemory(shmid,&str) ){
      std::cout<<"ERROR, cannot get shared memory"<<std::endl;
      return;
    }

  uint32_t slotConnMask,slotMask,slaveMaskN,slaveMaskS;

    if( sscanf(str,"0x%08X 0x%08X 0x%08X 0x%08X",&slotConnMask,&slotMask,&slaveMaskN,&slaveMaskS) != 4 ){
      std::cout<<"VMEUartReader is not running or memory corruption happened since I got "<<str<<std::endl;
      shmdt(str);
      return;
    }

  std::cout<<"I got readback mask "<<std::hex<<slotConnMask<<" SlotMask "<<slotMask<<std::dec<<std::endl;

    if(exit){
      sprintf(str, "EXIT");
      std::cout<<"Exiting VmeUartReader application "<<std::endl;
      shmdt(str);
      return;
    }

    if((slotConnMask  & (1 << slot)) == 0){
      std::cout<<"Slot "<<slot<<" is not supported in the current connectivityMask "<<std::hex<<slotConnMask<<"SlotMask "<<slotMask<<std::dec<<std::endl;
      std::cout<<"Doing nothing"<<std::endl;
    } else {
      uint32_t mask = (1 << slot);
        if(!lock){
          std::cout<<"Unlocking UART"<<std::endl;
          slotMask |= mask;
        } else {
          std::cout<<"Locking UART"<<std::endl;
          slotMask &= ~mask;
        }

      sprintf(str, "0x%08X 0x%08X 0x%08X 0x%08X",slotConnMask,slotMask,slaveMaskN,slaveMaskS);
      std::cout<<"Data written in memory: "<<str<<std::endl;
    }
  //detach from shared memory  
  shmdt(str);
  sleep(1);

}

void VmeUartControlSlave(int slot, int slaveNr ,bool lock){

  int shmid;
  char *str = NULL;

  if(!getSharedMemory(shmid,&str) ){
    std::cout<<"ERROR, cannot get shared memory"<<std::endl;
    return;
  }

  uint32_t slotConnMask,slotMask,slaveMaskN,slaveMaskS;
  if( sscanf(str,"0x%08X 0x%08X 0x%08X 0x%08X",&slotConnMask,&slotMask,&slaveMaskN,&slaveMaskS) != 4 ){
    std::cout<<"VMEUartReader is not running or memory corruption happened since I got "<<str<<std::endl;
    shmdt(str);
    return;
  }

  if((slotConnMask  & (1 << slot)) == 0){
    std::cout<<"Slot "<<slot<<" is not supported in the current connectivityMask "<<std::hex<<slotConnMask<<"SlotMask "<<slotMask<<std::dec<<std::endl;
    std::cout<<"Doing nothing"<<std::endl;
    shmdt(str);
    return;
  }

  uint32_t mask = (1 << slot);

    if(!lock){
      std::cout<<"Unlocking UART for slave "<<slaveNr<<std::endl;
        if (slaveNr ==0)slaveMaskN |= mask;
        else if (slaveNr ==1) slaveMaskS |= mask;
    } else {
      std::cout<<"Locking UARTfor slave "<<slaveNr<<std::endl;
        if (slaveNr ==0)slaveMaskN &= ~mask;
        else if (slaveNr ==1) slaveMaskS &= ~mask;
    }

  sprintf(str, "0x%08X 0x%08X 0x%08X 0x%08X",slotConnMask,slotMask,slaveMaskN,slaveMaskS);
  std::cout<<"Data written in memory: "<<str<<std::endl;

  //detach from shared memory
  shmdt(str);

}

bool mkpath( const std::string &path )
{
    bool bSuccess = false;
    int nRC = mkdir( path.c_str(), 0775 );
    if( nRC == -1 )
    {
        switch( errno )
        {
            case ENOENT:
                //parent didn't exist, try to create it
                if( mkpath( path.substr(0, path.find_last_of('/')) ) )
                    //Now, try to create again.
                    bSuccess = 0 == mkdir( path.c_str(), 0775 );
                else
                    bSuccess = false;
                break;
            case EEXIST:
                //Done!
                bSuccess = true;
                break;
            default:
                bSuccess = false;
                break;
        }
    }
    else
        bSuccess = true;
    return bSuccess;
}

int getFileSize(std::string filename){

  struct stat stat_buf;
  int rc = stat(filename.c_str(), &stat_buf);
  //std::cout<<"File size is "<<stat_buf.st_size<<std::endl;
  return rc == 0 ? stat_buf.st_size : -1;

}


std::string  getcurrentDateTime(bool fileFormat) {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);

    if(fileFormat)strftime(buf, sizeof(buf), "%Y_%m_%d_%OH_%OM_%OS", &tstruct);
    else strftime(buf, sizeof(buf), "%Y_%m_%d_%X", &tstruct);

    return buf;
}


}
