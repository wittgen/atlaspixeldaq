#include "VmeUartManager.hh"
#include "VmeUtils.hh"

#include "PixConnectivity/SbcConnectivity.h"
#include "PixConnectivity/RodCrateConnectivity.h"

VmeUartManager::VmeUartManager( ): m_appName("VmeUartManager"){
  m_isInfr = false;
  this->checkPrograms();
  this->init();
}

VmeUartManager::VmeUartManager( std::string ipcPartitionName ): m_appName("VmeUartManager"){
  m_isInfr = this->connectToInfr(ipcPartitionName);
  this->checkPrograms();
  this->init();
}

VmeUartManager::~VmeUartManager( ){
  //Destroy shared memory
  std::cout<<"Destroying shared memory"<<std::endl;
  char *str = NULL;
  int shmid;

    if(VmeUtils::getSharedMemory(shmid,&str)){
      sprintf(str, "EXITED");
      shmdt(str);
      shmctl(shmid,IPC_RMID,NULL);
    }
}

void VmeUartManager::init() {

  m_rodMask =0x0;

  m_hostName = VmeUtils::getHostName();

  std::string logdir;
  if(getenv("PIX_LOGS")) logdir = getenv("PIX_LOGS");
  else logdir = "/det/pix/logs/";
  logdir += "/VmeUart/" + m_hostName + "/";

  if(m_isInfr){
    for(auto& pixRod: m_pixConnectivityPtr->rods) {
      std::string& sbcHostName =  pixRod.second->crate()->sbc()->ipName();
      if( m_hostName.find(sbcHostName) == std::string::npos ) continue;
      m_crateName = pixRod.second->crate()->name();
        if(pixRod.second->rodBocType() == "CPPROD" || pixRod.second->rodBocType() == "L12ROD"){
          m_rodMask |= 0x1 << pixRod.second->vmeSlot();
          ers::info(PixLib::pix::daq (ERS_HERE, m_appName, pixRod.first));
          m_rodMap[pixRod.first] =  std::make_shared<VmeUartReader>(pixRod.second->vmeSlot(), pixRod.first, logdir);
        }
     }
  } else {
    //Read cfg file
    std::string fileName;
    if(getenv("ROD_DAQ"))fileName = getenv("ROD_DAQ");
    else fileName = "/det/pix/daq/current/RodDaq";
    fileName += "/IblUtils/Vme/cfg/Connectivity.cfg";

    std::ifstream file(fileName );
    if(!file.is_open()){
      ers::fatal(PixLib::pix::daq (ERS_HERE, m_appName," cannot get connectivity file "+ fileName ));
      exit(1);
    }

    std::string line,SBC,crate,slots,sN,trim;
    char t[10];
    sprintf(t," \t\n\r\f\v");

      while(std::getline(file,line)){ 
        if(line.empty())continue;
        trim = line;
        trim.erase(0, trim.find_first_not_of(t));
        if(trim.find("#")==0) continue;
        std::stringstream ssl(line);
        ssl>>SBC>>crate>>slots;
        if(m_hostName.find(SBC) == std::string::npos)continue;
        m_crateName = "CRATE_"+crate;
        std::stringstream ssSlot(slots);

          while( std::getline(ssSlot,sN,',') ){
            int slotNr = atoi(sN.c_str());
            m_rodMask |= 0x1 << slotNr;
            std::string rodName = "ROD_"+crate+"_S"+std::to_string(slotNr);
            ers::info(PixLib::pix::daq (ERS_HERE, m_appName, rodName));
            m_rodMap[rodName] =  std::make_shared<VmeUartReader>(slotNr, rodName,logdir);
          }
      }
    file.close();
  }

  std::stringstream ss;
  ss<<std::hex<<m_rodMask;
  ers::info(PixLib::pix::daq (ERS_HERE, m_appName,m_crateName+" rodMask 0x"+ss.str()));

  if(m_rodMap.empty()){
    ers::fatal(PixLib::pix::daq (ERS_HERE, m_appName, "Couldn't get any rod for "+m_hostName+" are you running on SBC?"));
    exit(1);
  }

  std::map<std::string, std::shared_ptr<VmeUartReader>>::iterator it;
    for(it = m_rodMap.begin();it!= m_rodMap.end();it++)
      if(!it->second->init()){
	ers::fatal(PixLib::pix::daq (ERS_HERE, m_appName,"Problem while initializing "+it->first));
	exit(1);
      }

}

void VmeUartManager::run() {

  uint32_t slotMask = m_rodMask;//Default all slots readed
  uint32_t slaveMaskN = 0;//Default slave north disabled
  uint32_t slaveMaskS = 0;//Default slave north disabled

  //Shared memory for control kill program and flashing
  char *str = NULL;
  int shmid;
  if(!VmeUtils::getSharedMemory(shmid,&str))exit(1);

  sprintf(str, "0x%08X 0x%08X 0x%08X 0x%08X",m_rodMask,slotMask,slaveMaskN,slaveMaskS);
  ers::info(PixLib::pix::daq (ERS_HERE, m_appName,"Data written in memory: "+std::string(str) ));
  //detach from shared memory  
  shmdt(str);

  auto begin = std::chrono::system_clock::now();
  auto end = std::chrono::system_clock::now();
  int duration =0;

  std::map<std::string, std::shared_ptr<VmeUartReader>>::iterator it;

  while(1) {
    bool skip = false;
    str = (char*) shmat(shmid,(void*)0,0);
    uint32_t sM;
     if( sscanf(str,"0x%08X 0x%08X 0x%08X 0x%08X",&sM,&slotMask,&slaveMaskN,&slaveMaskS) != 4 ){
       ers::error(PixLib::pix::daq (ERS_HERE, m_appName,"Program exited or memory corruption happened since I got "+std::string(str)));
       exit(1);
     }
    //detach from shared memory
    shmdt(str);

      for(it = m_rodMap.begin();it!= m_rodMap.end();it++){
       int slot = it->second->getSlotNumber();
        if((slotMask  & (1 << slot)) == 0){//Check if slot is being flashed
          end = std::chrono::system_clock::now();
          duration = std::chrono::duration_cast<std::chrono::seconds>(end-begin).count();
           if(duration >= 60){
             ers::info(PixLib::pix::daq (ERS_HERE, m_appName,"Skipping slot "+std::to_string(slot)+" being used for other program"));
             begin = std::chrono::system_clock::now();
           }

         continue;
         }

	for(int uartSourceId = 0 ; uartSourceId < 3 ; ++uartSourceId) {
	  if(uartSourceId ==1 && ((slaveMaskN  & (1 << slot)) == 0))continue;
	  if(uartSourceId ==2 && ((slaveMaskS  & (1 << slot)) == 0))continue;
	    if(!it->second->readVmeUart(uartSourceId, skip)){
	     ers::error(PixLib::pix::daq (ERS_HERE, m_appName,"Cannot read VMEUART"));
	     exit(1);
	     }

	     if(skip){
               VmeUtils::VmeUartControl(slot,true);
	       ers::error(PixLib::pix::daq (ERS_HERE, m_appName,"Skipping slot "+std::to_string(slot)+" too many non-ascii characters" ));
	       break;
	     }
	  }
      }
    std::this_thread::sleep_for( std::chrono::milliseconds(500) );
  }


}

bool VmeUartManager::connectToInfr( std::string ipcPartitionName ) {

  // Accessing the TDAQ partition
  try {
    m_ipcPartitionPtr = std::make_shared<IPCPartition>(ipcPartitionName);
  } catch(...) {
    ers::error(PixLib::pix::daq (ERS_HERE, m_appName,"Couldn't connect to the partition " + ipcPartitionName));
    return false;
  }

  // Interfacing to IS
  m_isManagerPtr = std::make_shared<PixISManager>(m_ipcPartitionPtr.get(), "PixelRunParams");

  // Now we are reading the connectiviyt, configuration and ID tags first from the environment, and then from IS
  std::string connTag, cfgTag, idTag,modCfgTag;

  if(getenv("PIX_CONN_DEFTAG")) connTag = getenv("PIX_CONN_DEFTAG");
  try {
    if(m_isManagerPtr->exists("DataTakingConfig-ConnTag"))
    connTag = m_isManagerPtr->read<std::string>("DataTakingConfig-ConnTag");
  } catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE, m_appName,"Cannot read DataTakingConfig-ConnTag from IS"));
    return false;
  }

  if(getenv("PIX_CFG_DEFTAG")) cfgTag = getenv("PIX_CFG_DEFTAG");
  try {
    if(m_isManagerPtr->exists("DataTakingConfig-CfgTag"))
    cfgTag = m_isManagerPtr->read<std::string>("DataTakingConfig-CfgTag");
  } catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE, m_appName, "Cannot read DataTakingConfig-CfgTag from IS"));
    return false;
  }

  if(getenv("PIX_ID_DEFTAG")) idTag = getenv("PIX_ID_DEFTAG");
  try {
    if(m_isManagerPtr->exists("DataTakingConfig-IdTag"))
    idTag = m_isManagerPtr->read<std::string>("DataTakingConfig-IdTag");
  } catch(...) {
    ers::info(PixLib::pix::daq (ERS_HERE, m_appName,"Cannot read DataTakingConfig-IdTag from IS"));
    return false;
  }

  if(getenv("PIX_MODCFG_DEFTAG")) modCfgTag = getenv("PIX_MODCFG_DEFTAG");
  try {
    if(m_isManagerPtr->exists("DataTakingConfig-ModCfgTag"))
    modCfgTag = m_isManagerPtr->read<std::string>("DataTakingConfig-ModCfgTag");
  } catch(...) {
    ers::warning(PixLib::pix::daq (ERS_HERE, m_appName,"Cannot read DataTakingConfig-ModCfgTag from IS"));
    return false;
  }

  ers::info(PixLib::pix::daq (ERS_HERE, m_appName, "Continuing with the following tags: " + idTag + ", " + connTag + ", " + cfgTag + ", "+modCfgTag));
  

  bool ready=false;
  int nTry=0;
  std::string dbsrv_name = "PixelDbServer";//harcoded
  std::string msg = "Trying to connect to DbServer with name "+dbsrv_name ;
  do {
      m_DbServerPtr = std::make_shared<PixDbServerInterface>(m_ipcPartitionPtr.get(), dbsrv_name, PixDbServerInterface::CLIENT, ready);
      if(ready)break;
      std::stringstream info;
      info << " ..... attempt number: " << nTry;
      msg = info.str();
      ers::debug(PixLib::pix::daq (ERS_HERE, m_appName,msg));
      nTry++;
      sleep(1);
    } while(nTry<20);
    if(!ready) {
        msg = " Impossible to connect to DbServer " + dbsrv_name;
        ers::error(PixLib::pix::daq (ERS_HERE, m_appName,msg));
        return false;
    } else {
        msg = "Successfully connected to DbServer with name " + dbsrv_name;
        ers::log(PixLib::pix::daq (ERS_HERE, m_appName,msg));
    }

  // Creating and loading the connectivity
  m_pixConnectivityPtr = std::make_shared<PixConnectivity>(m_DbServerPtr.get(), connTag, connTag, connTag, cfgTag, idTag, modCfgTag );

  try {
    m_pixConnectivityPtr->loadConn();
  }
  catch (const PixLib::PixConnectivityExc &e){
    ers::error(PixLib::pix::daq (ERS_HERE, m_appName, "Exception caught at loadConn():" + e.getDescr()));
    return false;
  }
  catch (...){
    ers::error(PixLib::pix::daq (ERS_HERE, m_appName, "Exception caught at loadConn()"));
    return false;
  }

    ers::info(PixLib::pix::daq (ERS_HERE, m_appName, "Connectivity tag " + connTag + " loaded successfully"));

return true;

}

void VmeUartManager::checkPrograms(){

  //VmeUartReader initialize to -1 due to the fact that should be the program currently running

  VmeUtils::checkPrograms("VmeUartReader");

}

