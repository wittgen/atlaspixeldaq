#include "PixUtilities/PixISManager.h"
#include "PixDbServer/PixDbServerInterface.h"
#include "PixConnectivity/PixConnectivity.h"
#include "PixConnectivity/RodBocConnectivity.h"
#include "PixUtilities/PixMessages.h"

#include "VmeUartReader.hh"

#include <memory>
#include <string>
#include <vector>
#include <map>

using namespace SctPixelRod;
using PixLib::PixConnectivity;
using PixLib::PixISManager;
using PixLib::PixDbServerInterface;

class VmeUartManager {

public:
  VmeUartManager( );
  VmeUartManager( std::string ipcPartitionName );
  ~VmeUartManager();
  void init();
  void run();

private:
  std::unique_ptr<PpcVmeInterface> thePpc;
  std::unique_ptr<RCCVmeInterface> vme;

protected:
  bool connectToInfr( std::string ipcPartitionName );
  void checkPrograms();

  std::shared_ptr<IPCPartition> m_ipcPartitionPtr;
  std::shared_ptr<PixISManager> m_isManagerPtr;
  std::shared_ptr<PixConnectivity> m_pixConnectivityPtr;
  std::shared_ptr<PixDbServerInterface> m_DbServerPtr;
  std::map<std::string, std::shared_ptr<VmeUartReader>> m_rodMap;
  std::string m_crateName;
  std::string m_hostName;
  std::string m_appName;
  uint32_t m_rodMask;
  bool m_isInfr;

private:

};


