#include "VmeUartReader.hh"
#include "VmeUtils.hh"
#include "PixUtilities/PixMessages.h"

#include <ctime>
#include <sys/stat.h> 

static const unsigned long mapSize = 0x01000000;

VmeUartReader::VmeUartReader(int slot, std::string rodName, std::string logDir){

	m_rodName = rodName;
	m_slot = slot;
	m_logDir = logDir;
        m_nTries = 0;
	m_newLine = true;
}

VmeUartReader::~VmeUartReader(){
   if(m_fs.is_open())m_fs.close();
}

bool VmeUartReader::init(){
	//init VME interface
	try {
		vme = std::make_unique<RCCVmeInterface>();

        } catch (...) {

                ers::fatal(PixLib::pix::daq (ERS_HERE,"VmeReader", "Caught an exception when creating  VME interface."));
		return false;
        }

	return createLogFile( );
}

bool VmeUartReader::readVmeUart(int sourceId, bool &skip){

   //init PPC VME interface
   unsigned long baseAddress = (m_slot << 24);
   PpcVmeInterface *thePpc;
     try {
       thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
       // thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
       // by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...
      } catch (...) {
        ers::fatal(PixLib::pix::daq (ERS_HERE,"VmeReader","Caught an exception when creating Ppc interface."));
	return false;
      }

    m_now = std::chrono::system_clock::now();
    int duration = std::chrono::duration_cast<std::chrono::seconds>(m_now -m_startFile).count();

    if(VmeUtils::getFileSize(m_logFileName) > MAX_FILE_SIZE || duration > MAX_FILE_DURATION) createLogFile( );//new file

    if(!m_fs.is_open()){
      ers::fatal(PixLib::pix::daq (ERS_HERE,"VmeReader","File "+ m_logFileName +" is not open exiting..."));
      return false;
    }

  std::string dateTime = VmeUtils::getcurrentDateTime();
  uint32_t addr = uartWdCountAddr[sourceId];
  uint32_t value = thePpc->ppcSingleRead(addr);
  uint16_t size = thePpc->ppcSingleRead(addr);
  int nonAscii =0;

  addr = uartDataAddr[sourceId];
    for(int i = 0 ; i < size ; ++i) {
      value = thePpc->ppcSingleRead(addr);
      if(!isascii((int)value)){nonAscii++;continue;}
      if(value==13)continue;
      if(m_newLine)m_fs<<"\n"<< dateTime <<"> ";//Add timeStamp after new line
      if(value==10)m_newLine=true;
      else {m_fs << (char)(value&0xFF);m_newLine=false;}
    }

    if(nonAscii>0){
      m_fs<< "Warning: "<<nonAscii<<" non-ascii characters detected\n";

      if(nonAscii ==65535)m_nTries++;//Maximum size uint16_t
      else m_nTries = 0;

        if(m_nTries >  MAX_TRIES_NONASCII ){
          m_fs<<dateTime <<"> "<< "This VMEUart reader will be skipped\n";
          skip = true;
        }
      m_fs<< dateTime <<"> ";
    }

   if(size>0)m_fs.flush();
   delete thePpc;
 return true;

}

bool VmeUartReader::createLogFile( ){

  std::string dirName = m_logDir + "/"+m_rodName;
    if(!VmeUtils::mkpath(dirName) ){
      ers::fatal(PixLib::pix::daq (ERS_HERE,"VmeReader","Cannot create dir "+dirName));
      return false;
    }
  std::string logFileName = dirName +"/" + VmeUtils::getcurrentDateTime(true) + ".log";
  m_logFileName = logFileName;

  if(m_fs.is_open())m_fs.close();

  m_fs.open(m_logFileName, std::ios::out | std::ios::app);

    if (m_fs.fail()){
      ers::fatal(PixLib::pix::daq (ERS_HERE,"VmeReader","Cannot open "+ m_logFileName +" exiting..."));
      return false;
    }

  m_fs<< VmeUtils::getcurrentDateTime( ) <<"> Starting VmeUartReader\n";
  m_fs.flush();

  std::string link =  dirName + "/PPClog_current";
  std::cout<<m_logFileName<<" "<<link<<std::endl;
  unlink(link.c_str());
  symlink(m_logFileName.c_str(),link.c_str());

  m_startFile = std::chrono::system_clock::now();

  return true;

}

