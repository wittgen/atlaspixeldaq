#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
#include "rodMaster.hxx"

#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <memory>

using namespace SctPixelRod;

#define MAX_FILE_SIZE 1024*1024*10 //Bits
#define MAX_FILE_DURATION 24*7*3600 //seconds
#define MAX_TRIES_NONASCII 3 //number of tries after skipping the reading of the UART manager

// Adding definition of make_unique that didn't make it in C++11
#if __cplusplus < 201400L
namespace std {

template<typename T, typename... Args>
  std::unique_ptr<T> make_unique(Args&&... args) {
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
  }

}
#endif

class VmeUartReader{

	private:
	const uint32_t uartWdCountAddr[3] = {PPC_UART_MASTER_STATUS_REG,  PPC_UART_S6A_STATUS_REG, PPC_UART_S6B_STATUS_REG};
	const uint32_t uartDataAddr[3] = {PPC_UART_MASTER_DATA_REG, PPC_UART_S6A_DATA_REG, PPC_UART_S6B_DATA_REG};

	// Using unique_ptr because these objects are owned by this VmeUartReader
	// Use shared_ptr in case you intend to copy them
	std::unique_ptr<RCCVmeInterface> vme;

	public:
	VmeUartReader(int slot, std::string rodName, std::string logDir);
	~VmeUartReader();
	bool init();
	bool readVmeUart(int sourceId, bool &skip);
	bool createLogFile( );
	int getSlotNumber(){return m_slot;}

	std::chrono::time_point<std::chrono::system_clock> m_startFile, m_now;
	std::string m_rodName;
	std::string m_logFileName;
	std::string m_logDir;
	std::ofstream m_fs;
	int m_slot;
        int m_nTries;
	bool m_newLine;

 };



