
/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/

/***************************** Include Files *********************************/

#include "rodMaster.hxx"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <iostream>
#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
#include "xparameters.h"
#include <stdlib.h>
#include "counterNames.h"
#include <iomanip>
#include "xil_io.h"

using namespace SctPixelRod; 
using namespace std;
/************************** Constant Definitions *****************************/

//#define XPAR_XPS_EPC_0_PRH0_BASEADD 0x40000000
//#define SLV_A_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x0)
//#define SLV_B_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x40000)
#define CNT_CONFIG_REG  0x06D8
#define CNT_START 0x0600


/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
static XIsf Isf;
int main(int argc, char *argv[]) 
{ 
	volatile unsigned int epcSlvARegs;
	volatile unsigned int epcSlvBRegs;
	epcSlvARegs = (volatile unsigned int )SLV_A_REG_BASE;
	epcSlvBRegs = (volatile unsigned int )SLV_B_REG_BASE;
	unsigned int slot, reset;
	if(argc > 1)
	{
		sscanf(argv[1],"%d",&slot);
		//sscanf(argv[2],"%d",&reset);
	}

	else
	{
		printf("*****************************************************\n\nUsage: countersReset slot_number\n*****************************************************\n\n");
		return 1;
	}
        

	// Create VME interface
	RCCVmeInterface *vme = 0;
	try 
	{
		vme = new RCCVmeInterface();

	} 
	catch (...) 
	{
		std::cout << "Caught an exception when creating  VME interface." << std::endl; 
	} 
	
	if(vme == 0)
		return 0; 

	// Create PPC interface through VME 

	unsigned long baseAddress = (slot << 24);
	const unsigned long mapSize     = 0x01000000;

	try 
	{
		thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
		// thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
		// by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...
	} 
	 catch (...) 
	{
		std::cout << "Caught an exception when creating Ppc interface." << std::endl;
	}


	//reading cnt CONFIG
	int cntConfig_A = thePpc->ppcSingleRead(epcSlvARegs + CNT_CONFIG_REG*4);
	int cntConfig_B = thePpc->ppcSingleRead(epcSlvBRegs + CNT_CONFIG_REG*4);	
	/*std::cout << left << "Before operation\n cntConfig_A: 0x";
	std::cout.fill('0');
	std::cout.width(8); 
	std::cout<< hex << right << cntConfig_A << "\t\tcntConfig_B: 0x";
	std::cout.fill('0');
	std::cout.width(8); 
	cout << hex <<cntConfig_B << std::endl;*/
	cntConfig_A = (cntConfig_A & 0x7FFFFFFF) | (1 << 31);
	cntConfig_B = (cntConfig_B & 0x7FFFFFFF) | (1 << 31);
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);  
	/*std::cout << left << "After operation \n cntConfig_A: 0x";
	std::cout.fill('0');
	std::cout.width(8); 
	std::cout<< hex << right << cntConfig_A << "\t\tcntConfig_B: 0x";
	std::cout.fill('0');
	std::cout.width(8); 
	cout << hex <<cntConfig_B << std::endl;
	sleep(2);*/
	cntConfig_A = (cntConfig_A & 0x7FFFFFFF);
	cntConfig_B = (cntConfig_B & 0x7FFFFFFF);
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);  
	cout << "Reset done \n";

 
	try
	{
		 delete thePpc;
	} 
	 catch (...) 
	{
		 std::cout << "Caught an exception when deleting Ppc interface." << std::endl;	
	} 


	return 0;  
	
}

