
/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/

/***************************** Include Files *********************************/

#include "rodMaster.hxx"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <iostream>
#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
#include "xparameters.h"
#include <stdlib.h>
#include "BUSYcountersName.h"
#include <iomanip>
#include "xil_io.h"

using namespace SctPixelRod; 
using namespace std;
/************************** Constant Definitions *****************************/

//#define XPAR_XPS_EPC_0_PRH0_BASEADD 0x40000000
//#define SLV_A_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x0)
//#define SLV_B_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADD + 0x40000)
#define CNT_CONFIG_REG  0x06D8
//#define CNT_NID_REG  0x06DC
#define BUSY_CNT_START 0x06E0


/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
static XIsf Isf;
int main(int argc, char *argv[]) 
{ 
	volatile unsigned int epcSlvARegs;
	volatile unsigned int epcSlvBRegs;
	epcSlvARegs = (volatile unsigned int )SLV_A_REG_BASE;
	epcSlvBRegs = (volatile unsigned int )SLV_B_REG_BASE;
	unsigned int slot;
	if(argc > 1)
	{
		sscanf(argv[1],"%d",&slot);
	}

	else
	{
		printf("Error: no slot number\n");
		return 1;
	}
        

	// Create VME interface
	RCCVmeInterface *vme = 0;
	try 
	{
		vme = new RCCVmeInterface();

	} 
	catch (...) 
	{
		std::cout << "Caught an exception when creating  VME interface." << std::endl; 
	} 
	
	if(vme == 0)
		return 0; 

	// Create PPC interface through VME 

	unsigned long baseAddress = (slot << 24);
	const unsigned long mapSize     = 0x01000000;

	try 
	{
		thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
		// thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
		// by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...
	} 
	 catch (...) 
	{
		std::cout << "Caught an exception when creating Ppc interface." << std::endl;
	}


	//reading cnt CONFIG
	int cntConfig_A = thePpc->ppcSingleRead(epcSlvARegs + CNT_CONFIG_REG*4);
	int cntConfig_B = thePpc->ppcSingleRead(epcSlvBRegs + CNT_CONFIG_REG*4);

	
	//   enabling cntRead
	cntConfig_A = cntConfig_A | 0x20000000;
	cntConfig_B = cntConfig_B | 0x20000000;
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);     

	cout << "cntConfigA: " <<hex<< cntConfig_A<<endl;
	cout << "cntConfigB: " <<hex<< cntConfig_B<<endl;

	unsigned int normalization_value_A = thePpc->ppcSingleRead(epcSlvARegs + BUSY_CNT_START*4);
	unsigned int normalization_value_B = thePpc->ppcSingleRead(epcSlvARegs + BUSY_CNT_START*4);
	unsigned int register_value;

	//   readind counters
	cout.width(90); cout  <<left<<"_______________________________________________________________________________________________________________________________\n";
	cout.width(90); cout  <<left<<"|                 Slave A                                      |               Slave B                                        |\n";
	cout.width(90); cout  <<left<<"|______________________________________________________________|______________________________________________________________|\n";
	cout.width(90); cout  <<left<<"|       Register        |       Value       |   Percentage(%)  |       Register        |       Value       |   Percentage(%)  |\n";
	cout.width(90); cout  <<left<<"|_______________________|___________________|__________________|_______________________|___________________|__________________|\n";
	for(int i=0; i<28; i++)
	{	
		if(i == 6 || i == 22)
		{	
			cout.width(90); cout  <<left<<"|_______________________|___________________|__________________|_______________________|___________________|__________________|\n";
		}
		cout.width(4); cout  <<left<<"| ";
		cout.width(20); cout  <<left<<BUSYcountersName[i];
		cout.width(5); cout  <<left<<"|  0x";
		register_value = thePpc->ppcSingleRead(epcSlvARegs + BUSY_CNT_START*4+i*16);
		cout.width(15); cout  <<left<<hex<< register_value;
		cout.width(4); cout  <<left<<"|  ";
		cout.width(15); cout  <<left<< setprecision(5) << (double)register_value / (double)normalization_value_A * 100.;
		cout.width(4); cout  <<left<<"|  ";
		cout.width(20); cout  <<left<<BUSYcountersName[i];
		cout.width(5); cout  <<left<<"|  0x";
		register_value = thePpc->ppcSingleRead(epcSlvBRegs + BUSY_CNT_START*4+i*16);
		cout.width(15); cout <<left<<hex<< register_value;
		cout.width(4); cout  <<left<<"|  ";
		cout.width(15); cout  <<left<< setprecision(5) << (double)register_value / (double)normalization_value_B * 100.;
		cout.width(4); cout  <<left<<"|  "<<endl;
	}
	cout.width(90); cout  <<left<<"|_______________________|___________________|__________________|_______________________|___________________|__________________|\n";
	//   disabling cntRead
	cntConfig_A = cntConfig_A & 0xDFFFFFFF;
	cntConfig_B = cntConfig_B & 0xDFFFFFFF;
	thePpc->ppcSingleWrite(epcSlvARegs + CNT_CONFIG_REG*4, cntConfig_A);      
	thePpc->ppcSingleWrite(epcSlvBRegs + CNT_CONFIG_REG*4, cntConfig_B);      

	cout << "cntConfigA: " <<hex<< cntConfig_A<<endl;
	cout << "cntConfigB: " <<hex<< cntConfig_B<<endl;
 
	try
	{
		 delete thePpc;
	} 
	 catch (...) 
	{
		 std::cout << "Caught an exception when deleting Ppc interface." << std::endl;	
	} 


	return 0;  
	
}

