INCLUDES += $(addprefix -I, $(shell $(FIND) src -mindepth 1 -type d) inc)
INCLUDES += -I$(TDAQ_INST_PATH)/include
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/CommonWithDsp
INCLUDES += -I$(PIXEL_DAQ_ROOT)/PixLibInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/PixDbServer
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/PixDaq/common/DspInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/PixDaq/common/DspInterface/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common/DspInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common/DspInterface/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/System
INCLUDES += -I$(BOOST_INC)


# For building the MDSP code
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/Common/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/PPC


INCLUDES += -I$(TDAQ_INST_PATH)/$(CMTCONFIG)/include
INCLUDES += -I$(TDAQC_INST_PATH)/include
INCLUDES += -I$(PIX_LIB)

# These directories also contain libraries to link against
DEPDIRS += $(PIXEL_DAQ_ROOT)/RodDaq/RodCrate
DEPDIRS += $(PIXEL_DAQ_ROOT)/VmeInterface

LDFLAGS += $(foreach DIR, $(DEPDIRS), $(call extDeps,$(DIR)))
INCLUDES += $(addprefix -I, $(DEPDIRS))

INCLUDES += -I$(shell root-config --incdir)

LDFLAGS += -L$(PIX_LIB)/$(CMTCONFIG) -lPixLib
LDFLAGS += -L$(PIXEL_DAQ_ROOT)/PixLibInterface/$(CMTCONFIG) -lPixLibInterface

INCLUDES += -I$(ROD_DAQ)/RodCrate
INCLUDES += -I$(ROD_DAQ)/IblDaq/RodMaster/Software/PPCpp/CmdPattern/inc
