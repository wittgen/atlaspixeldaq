#!/bin/sh
#By Pesse & Blade
#Installs the Firmware taken from Binaries/*.stapl on the provided slot numbers
#Beware not to modify them unless you know what you are doing!
#
if [ $# -eq 0 ]
then
    echo ;
    echo 'syntax: '$0' [-h help][-c compile][-i install(notice without -n installs all slots)][-n <number of slot to program> (only with -i option)]';
    echo ;
    exit 1;
fi;

while getopts "ichan:" o;
  do
  case "${o}" in
      i) INSTALL=1 ;;
      c) COMPILE=1 ;;
      h) HELP=1 ;;
      n) SLOTS="$SLOTS ${OPTARG}"  ;;
      ?) HELP=1 ;;
  esac 
done

if [ -n "$SLOTS" ] && [ -z "$INSTALL" ]; then
    HELP=1
fi

if [ -n "$HELP" ]; then
    echo "This is a script that provides the initial installation of ROD basic firmware via VME"
    echo "The following requirements must be met to properly run this script:"
    echo ""
    echo 'syntax: '$0' [-h help][-c compile][-i install(notice without -n installs all slots)][-n <number of slot to program> (only with -i option)]';
    echo ""
    echo "- pwd command result must be: ~/daq/IblRod/IblUtils"
    echo "- \"-i\" option must be executed on the SBC"
    echo "- \"-n\" option without \"-i\" is useless"
    echo "- considering the speed of SBC it's recommended to run the compilation on the host"
#    echo "$(dirname $(readlink -f $0))"
    exit 1;
fi 

cd "$(dirname $(readlink -f $0))"
JP25="Jp_25"
echo $JP25
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$PWD/v1/lib:$ROD_DAQ/RodCrate/:$folder/VmeInterface

if [ -n "$COMPILE" ]; then
    echo COMPILING
    #MAKE EVERYTHING!!!
 

    cd ../
    export ROD_DAQ="$(pwd)"
    cd -

    folder=`echo "$ROD_DAQ/.."`


		make -C $JP25 -j4

    cd $folder
    make -C PixLibInterface/ all inst
    make -C VmeInterface/ all inst
    make -C RodDaq/RodCrate/ all inst
    cd -

    #cd v1 #compile everything
    cd Vme
    make
    cd -
fi



if [ -n "$INSTALL" ]; then
    
    if [ -n "$SLOTS" ]; then

	echo "INSTALLING into slots $SLOTS "
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming RODMASTER in slot $i with rodmaster_flashloader.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodmaster_flashloader.stapl;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Loading srec file on V5's flash in slot $i"
	  v1/bin/ProgramVirtexFlash $i Binaries/iblDsp_ppc.srec;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming RODMASTER in slot $i with rodmaster_bootloader.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodmaster_bootloader.stapl;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming Spartan6 Slave 0 in slot $i with rodslave_slave0.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodslave_slave0.stapl;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming Spartan6 Slave 1 in slot $i with rodslave_slave1.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodslave_slave1.stapl;
	done


    else
	SLOTS="$(seq 5 1 12) $(seq 14 1 21)"
	echo ""
	echo ""
	echo ""
	echo "INSTALLING into slots $SLOTS"
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming RODMASTER in slot $i with rodmaster_flashloader.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodmaster_flashloader.stapl;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Loading srec file on V5's flash in slot $i"
	  v1/bin/ProgramVirtexFlash $i Binaries/iblDsp_ppc.srec;
	done
	
	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming RODMASTER in slot $i with rodmaster_bootloader.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodmaster_bootloader.stapl;
	done

	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming Spartan6 Slave 0 in slot $i with rodslave_slave0.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodslave_slave0.stapl;
	done

	for i in $SLOTS
	  do
	  echo ""
	  echo ""
	  echo ""
	  echo "Programming Spartan6 Slave 1 in slot $i with rodslave_slave1.stapl"
	  $JP25/jp_25 -X$i -v -aRUN_XILINX_PROC Binaries/rodslave_slave1.stapl;
	done
    fi
    
fi
