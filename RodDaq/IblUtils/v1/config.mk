ifndef ROOTSYS
$(error ROOT not set up. Please set ROOTSYS)
endif

ifdef DEBUG_SHELL
       SHELL = /bin/sh -x
endif

UNAME := $(shell uname)
ifeq ($(UNAME), Linux)
ifneq ($(shell lsmod | grep -c ni488), 0)
HAS_GPIB=1
endif
endif

OUT_DIR ?= .

BUILD_DIR ?= $(OUT_DIR)/build/$(CMTCONFIG)
BIN_DIR ?= $(OUT_DIR)/bin/$(CMTCONFIG)
LIB_DIR ?= $(OUT_DIR)/lib/$(CMTCONFIG)

ifeq ($(DEBUG), 1)
CMTCONFIG_DBG=$(subst opt,dbg,$(CMTCONFIG))
BUILD_DIR = $(OUT_DIR)/build/$(CMTCONFIG_DBG)
BIN_DIR = $(OUT_DIR)/bin/$(CMTCONFIG_DBG)
LIB_DIR = $(OUT_DIR)/lib/$(CMTCONFIG_DBG)
endif


RM ?= rm -f
MKDIR ?= mkdir -p
FIND ?= find -L

ifeq ($(UNAME), Linux)
	CXX ?= g++
endif
ifeq ($(UNAME), Darwin)
ifneq "$(MAKECMDGOALS)" "clean"
$(warning Allowing undefined symbols in shared libaries for OS X)
endif
# On MacOS X, CXX defaults to c++
CXX = clang++
CXX ?= llvm-g++ 
#CXX += -flat_namespace -undefined suppress
CXX += -undefined dynamic_lookup
NO_VME=1
endif

ifndef PIXEL_DAQ_ROOT
PIXEL_DAQ_ROOT := $(shell pwd | sed 's@/RodDaq.*@@g')
ifndef PIXEL_DAQ_ROOT
PIXEL_DAQ_ROOT := $(shell echo $PIXEL_DAQ_ROOT)
endif
endif

ifeq ($(NO_VME), 1)
CXXFLAGS += -DNO_VME
endif

TDAQ_PATH = $(dir $(TDAQ_INST_PATH))

# Default system boost
ifneq ($(BOOSTSUF),)

BOOST_INC=$(BOOSTINC)
BOOST_LIB=$(BOOSTLIB)
BOOST_LIB_SUFFIX=$(BOOSTSUF)

else

BOOST_INC ?= /daq/slc5/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/include/boost-1_44
BOOST_LIB ?= /daq/slc5/sw/lcg/external/Boost/1.44.0_python2.6/i686-slc5-gcc43-opt/lib
BOOST_LIB_SUFFIX ?= -gcc43-mt

ifdef BOOST_DIR
BOOST_INC = $(BOOST_DIR)
BOOST_LIB = $(BOOST_DIR)/stage/lib
BOOST_LIB_SUFFIX = 
endif

ifeq ($(UNAME), Darwin)
BOOST_INC = /opt/local/include
BOOST_LIB = /opt/local/lib
BOOST_LIB_SUFFIX = -mt
endif

endif
