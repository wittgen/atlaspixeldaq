
# Helper functions
src2dep = $(patsubst %.cxx, $(BUILD_DIR)/%.d, $(1:%.cpp=%.cxx))
src2obj = $(patsubst %.cxx, $(BUILD_DIR)/%.o, $(1:%.cpp=%.cxx))
src2hdr = $(patsubst %.cxx, %.h, $(1:%.cpp=%.cxx))
src2bin = $(patsubst src/%.cxx, $(BIN_DIR)/%, $(1:%.cpp=%.cxx))
obj2dep = $(patsubst %.o, %.d, $(1))

subpath = $(patsubst %/module.mk, %, $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))
dirname = $(lastword $(subst /, , $(1)))

# Provies de right library path for a library named lib$(1).so
libPath = $(patsubst %, $(LIB_DIR)/lib%.so, $(1))

hdr2lnk = $(addprefix -l, $(patsubst %/, %, $(dir $(shell echo $(1) | grep '/'))) $(patsubst %.h, %, $(shell echo $(1) | grep -v '/')))
hdr2lib = $(patsubst -l%, $(LIB_DIR)/lib%.so, $(call hdr2lnk, $(1)))
inc2lnk = $(foreach F, $(1), $(call hdr2lnk, $(patsubst src/libs/%, %, $(F))))
inc2lib = $(patsubst -l%, $(LIB_DIR)/lib%.so, $(call inc2lnk, $(1)))

extDeps = -L$(1) $(patsubst $(1)/lib%.so, -l%, $(wildcard $(1)/lib*so))

getHdrs = $(filter %h, $(1))
getSrcs = $(filter %.cxx, $(1)) $(filter %.cpp, $(1))

define mkLib
libs += $(1)
srcs += $(2)
objs += $(call src2obj, $(2))
deps += $(call src2dep, $(2))

$(1): $(call src2obj, $(2))
	@echo "<Assembling  $$@>"
	$(CXX) $(LFLAGS) -shared -o $$@ $$+
endef

