
/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/
//#define BOLOGNA

/***************************** Include Files *********************************/
//#ifdef BOLOGNA
//#include "v5_ppc_bo.h"
//#else
#include "rodMaster.hxx"	/* EDK generated parameters */
//#endif
#include "xilisf.h"		/* Serial Flash Library header file */
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01

/*
 * Page size of the Serial Flash.
 */
#define ISF_PAGE_SIZE		1056

// start address where SREC file is stored
#define FLASH_START_ADDRESS 0x0

// total flash size in bytes
#define FLASH_SIZE 8650752

// size of SREC file
#define SREC_FILE_SIZE 964018



/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

static int IsfWaitForFlashNotBusy();

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
static XIsf Isf;
static XSpi Spi;

/*
 * The user needs to allocate a buffer to be used by the In-system and Serial
 * Flash Library to perform any read/write operations on the Serial Flash
 * device.
 * User applications must pass the address of this memory to the Library in
 * Serial Flash Initialization function, for the Library to work.
 * For Write operations:
 * - The size of this buffer should be equal to the Number of bytes to be
 * written to the Serial Flash + XISF_CMD_MAX_EXTRA_BYTES.
 * - The size of this buffer should be large enough for usage across all the
 * applications that use a common instance of the Serial Flash.
 * - A minimum of one byte and a maximum of ISF_PAGE_SIZE bytes can be written
 * to the Serial Flash, through a single Write operation.
 * The size of this buffer should be equal to XISF_CMD_MAX_EXTRA_BYTES, if the
 * application only reads from the Serial Flash (no write operations).
 */
u8 IsfWriteBuffer[ISF_PAGE_SIZE + XISF_CMD_SEND_EXTRA_BYTES];

/*
 * Buffers used during read and% write transactions.
 */
u8 ReadBuffer[ISF_PAGE_SIZE + XISF_CMD_SEND_EXTRA_BYTES] ; /* Read Buffer */
u8 WriteBuffer[ISF_PAGE_SIZE]; 				   /* Write buffer */

/*****************************************************************************/ 
int main(int argc, char *argv[]) { 
 
using namespace SctPixelRod; 
 using namespace std;
 
 
 
  ifstream binFile;                             // Pointer to binary frile 
  unsigned long selectAddr;                     // Start address of seleced flash 
  unsigned long flashAddr;                      // Location in target flash 
  int fileSize;                                 // Size of binary file in bytes 
  int selectSize;                               // Size of selected flash 
  int iselect; 

  unsigned int slot;

  std::string binFileName = "";
 
  if(argc > 1){
   sscanf(argv[1],"%d",&slot);
 
	   if (argc > 2) { 
		binFileName = argv[2]; 
	  }

  }
  else{
    printf("Error: no slot number\n");
    return 1;
  }
  
  if (binFileName == "") { 
    std::cout << "Binary File name not specified" << std::endl; 
    exit(1); 
  }



// Create VME interface
 RCCVmeInterface *vme = 0;
 try {
   vme = new RCCVmeInterface();

 } catch (...) {
 
   std::cout << "Caught an exception when creating  VME interface." << std::endl; 
 } 
  if(vme == 0)return 0; 
 
  // Create PPC interface through VME 
  
  unsigned long baseAddress = (slot << 24);
 const unsigned long mapSize     = 0x01000000;
   
try {
 thePpc = new PpcVmeInterface(baseAddress, mapSize, *vme);
 // thePpc is a global defined in xil_io.h: any W/R in xpsi and xilisf libraries uses it
 // by defining XilOut and XilIn macro as thePpc->write and thePpc->read ...

 }  catch (...) {
   std::cout << "Caught an exception when creating Ppc interface." << std::endl;
 }


/*
	 * Initialize the SPI driver so that it's ready to use,
	 * specify the device ID that is generated in xparameters.h.
	 */
	 int Status;

    std::cout << "Initialize XSpi: " ;
	Status = XSpi_Initialize(&Spi, SPI_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		std::cout << " Failed" << std::endl;
		 return 1;
	}
	std::cout << " Done" << std::endl;
	
	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	std::cout << "Start XSpi driver: " ;
	Status = XSpi_Start(&Spi);
	if(Status != XST_SUCCESS) {
		 std::cout << " Failed" << std::endl;
		return 1;
	}
	std::cout << " Done" << std::endl;

	/*
	 * Disable Global interrupt to use the Spi driver in polled mode
	 * operation.
	 */
	XSpi_IntrGlobalDisable(&Spi);

	/*
	 * Initialize the In-system and Serial Flash Library.
	 */
	std::cout << "Initialize ISF Library: " ;
	Status = XIsf_Initialize(&Isf, &Spi, ISF_SPI_SELECT, IsfWriteBuffer);
	if(Status != XST_SUCCESS) {
		std::cout << " Failed" << std::endl;
		return 1;
	}
	std::cout << " Done" << std::endl;

	// Prepare Flash File

  const unsigned long flashStart = FLASH_START_ADDRESS; 
  const long flashSize = SREC_FILE_SIZE; 
 
  cout << "Loading flash to ROD\n"; 
  selectAddr = flashStart; 
  selectSize = flashSize; 
   
  binFile.open(binFileName.c_str(), ios::binary); 
  if (!binFile.is_open()) { 
    cout << "Unable to open binary file." << endl; 
    exit(1); 
  } 
   
  // Get size of file 
  binFile.seekg(0, ios::end);           // go to end of file 
  fileSize = binFile.tellg();          // file size given by current location 
  binFile.seekg(0, ios::beg);          // go back to beginning of file 
  if (fileSize != selectSize) { 
	  cout << "WARNING: File size is incorrect. Expected: " << dec << selectSize << " Found: "  
	 << dec << fileSize << endl; 
	  cout << "press Y to continue, otherwise abort >"; 
	  char yesno; 
	  cin >> yesno; 
	  if(yesno != 'Y') exit(3); 
	  selectSize = fileSize;
  } 
  // Create a buffer and read file into it 
  u8 * buffer; 
  try { 
    buffer = new u8[fileSize]; 
  } 
  catch (std::bad_alloc & ba) { 
    cout << "Unable to allocate buffer for binary file." << endl; 
    exit(2); 
  } 
  binFile.read((char *)buffer, fileSize); 
 
  // prepare loop over flash pages 
 
  int numofpages = (int) (fileSize/ISF_PAGE_SIZE) +( (fileSize%ISF_PAGE_SIZE != 0)? 1 : 0); 
  long remainingSize = selectSize; 
  const u32 relative_start_address = 0x0; 
  XIsf_WriteParam WriteParam;
  XIsf_ReadParam ReadParam;
  u32 Address; 
  cout << "Will be written " << fileSize << " bytes on " << numofpages; 
  cout << " flash pages (" << ISF_PAGE_SIZE << " bytes per page)" << endl; 
 
  for(int ipage = 0; ipage < numofpages; ipage++,remainingSize-=ISF_PAGE_SIZE){ 
 

 
 
	  Address = ((ipage & 0x1FFF) << 11) |  (relative_start_address & 0x7FF); 
 
#ifdef FLASH_ERASE
	  Status = XIsf_Erase(&Isf, XISF_PAGE_ERASE, Address); 
	  if(Status != XST_SUCCESS) {
		  cout << "Failure erasing page " << ipage << " ... Abort!" << endl;
		  return 1;
	  } 
	  Status = IsfWaitForFlashNotBusy();

	  if(Status != XST_SUCCESS) {
	      cout << "Failure on wating NotBusy after erasing page " << ipage << " ... Abort!" << endl;
		  return 1;
	  } 
#endif
	  //prepare data to be written on page 
 
	  WriteParam.Address = Address;
	  WriteParam.NumBytes = (remainingSize < ISF_PAGE_SIZE)? remainingSize : ISF_PAGE_SIZE;
	  WriteParam.WritePtr = WriteBuffer; 
	  
/*
		 * Prepare the write buffer. Fill in the data need to be written into
		 * Serial Flash.
		 */
	  for(int Index = 0; Index < (int) WriteParam.NumBytes; Index++) {
		  WriteParam.WritePtr[Index] = buffer[Index + ISF_PAGE_SIZE*ipage];
	  } 
 
	  		/*
		 * Perform the Write operation.
		 */
	  Status = XIsf_Write(&Isf, XISF_WRITE, (void*) &WriteParam);
	  if(Status != XST_SUCCESS) {
	    cout << hex << WriteParam.Address << endl;
	    cout << hex << WriteParam.NumBytes << endl;
	    cout << hex << WriteParam.WritePtr << endl;
		  cout << "Failure writing page " << ipage << " ... Abort!" << endl;
		  return 1;
	  }

		/*
		 * Wait till the Serial Flash is ready.
		 */
	  Status = IsfWaitForFlashNotBusy();
	  if(Status != XST_SUCCESS) {
		  cout << "Failure on wating NotBusy after writing page  " << ipage << " ... Abort!" << endl;
		  return 1;
	  } 
 
	  ReadParam.Address = Address;
	  ReadParam.NumBytes = (remainingSize < ISF_PAGE_SIZE)? remainingSize : ISF_PAGE_SIZE;
	  ReadParam.ReadPtr = ReadBuffer;

	  	/*
	 * Perform the Read operation.
	 */
	Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
	if(Status != XST_SUCCESS) {
		  cout << "Failure reading page " << ipage << " ... Abort!" << endl;
		  return 1;

	}

	/*
	 * Compare the Data Read with the Data Written to the Serial Flash.
	 */
	for(int Index = 0; Index < (int) ReadParam.NumBytes; Index++) {
		if(ReadParam.ReadPtr[Index + XISF_CMD_SEND_EXTRA_BYTES] !=
			WriteParam.WritePtr[Index]) {
		    cout << "Failure checking BYTE " << Index << " on page " << ipage << endl;
			cout << "Written 0x" << hex << WriteParam.WritePtr[Index] ;
			cout << "  - Read 0x" << hex << ReadParam.ReadPtr[Index + XISF_CMD_SEND_EXTRA_BYTES] << "... Abort!" << endl;
			return 1;
		}
	}

	cout << "\rProcessed pages = " << ipage+1 << '/' << numofpages << std::flush;
 
 
  } // end loop over pages 

  cout << endl;
 
 
  	 try{
	   delete thePpc;
	 }  catch (...) {
	   std::cout << "Caught an exception when deleting Ppc interface." << std::endl;
	 } 
 
	 cout << "Flash correctly programmed!" << endl; 
	 return 0;  

}


/*****************************************************************************/
/**
*
* This function waits till the Serial Flash is ready to accept next command.
*
* @param	None
*
* @return	XST_SUCCESS if successful else XST_FAILURE.
*
* @note		None.
*
******************************************************************************/
static int IsfWaitForFlashNotBusy()
{
	int Status;
	u8 StatusReg;
	u8 ReadBuffer[2];

	while(1) {

		/*
		 * Get the Status Register.
		 */
		Status = XIsf_GetStatus(&Isf, ReadBuffer);
		if(Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Check if the Serial Flash is ready to accept the next
		 * command. If so break.
		 */
		StatusReg = ReadBuffer[BYTE2];
		if(StatusReg & XISF_SR_IS_READY_MASK) {
			break;
		}
	}

	return XST_SUCCESS;
}
