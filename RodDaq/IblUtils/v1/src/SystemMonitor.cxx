
/*
Utility for loading SREC file into the ATMEL Flash through VME

version 1.0: RT 14/05/2013
access to Flash is through SPI used in polled mode (without interrupt)

*/
//#define BOLOGNA

/***************************** Include Files *********************************/
//#ifdef BOLOGNA
//#include "v5_ppc_bo.h"
//#else
#include "rodMaster.hxx"	/* EDK generated parameters */
//#endif
#include "xil_io.h"		/* Serial Flash Library header file */
#include <iostream>
#include <unistd.h>
#include <stdio.h>
#include <ncurses.h>
#include <stdlib.h>
#include <signal.h>
#include "RCCVmeInterface.h"
#include "PpcVmeInterface.h"
/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01

/*
 * Page size of the Serial Flash.
 */
#define ISF_PAGE_SIZE		1056

// start address where SREC file is stored
#define FLASH_START_ADDRESS 0x0

// total flash size in bytes
#define FLASH_SIZE 8650752

// size of SREC file
#define SREC_FILE_SIZE 0

#define BASEADDR_SYSMON 0x83800000
/**************************** Type Definitions *******************************/
/************************** Function Prototypes ******************************/

//static int IsfWaitForFlashNotBusy();

/************************** Variable Definitions *****************************/

/*
 * The instances to support the device drivers are global such that they
 * are initialized to zero each time the program runs.
 */
//static XIsf Isf;
//static XSpi Spi;

/*
 * The user needs to allocate a buffer to be used by the In-system and Serial
 * Flash Library to perform any read/write operations on the Serial Flash
 * device.
 * User applications must pass the address of this memory to the Library in
 * Serial Flash Initialization function, for the Library to work.
 * For Write operations:
 * - The size of this buffer should be equal to the Number of bytes to be
 * written to the Serial Flash + XISF_CMD_MAX_EXTRA_BYTES.
 * - The size of this buffer should be large enough for usage across all the
 * applications that use a common instance of the Serial Flash.
 * - A minimum of one byte and a maximum of ISF_PAGE_SIZE bytes can be written
 * to the Serial Flash, through a single Write operation.
 * The size of this buffer should be equal to XISF_CMD_MAX_EXTRA_BYTES, if the
 * application only reads from the Serial Flash (no write operations).
 */
//u8 IsfWriteBuffer[ISF_PAGE_SIZE + XISF_CMD_SEND_EXTRA_BYTES];

/*
 * Buffers used during read and% write transactions.
 */
//u8 ReadBuffer[ISF_PAGE_SIZE + XISF_CMD_FAST_READ_EXTRA_BYTES] ; /* Read Buffer */
//u8 WriteBuffer[ISF_PAGE_SIZE]; 				   /* Write buffer */

/*****************************************************************************/ 
int stop=0;
void signal_callback_handler(int signum){
stop=1;
}


int main(int argc, char *argv[]) { 
 
using namespace SctPixelRod; 
 using namespace std;
 
 
 
  ifstream binFile;                             // Pointer to binary frile 
  unsigned long selectAddr;                     // Start address of seleced flash 
  unsigned long flashAddr;                      // Location in target flash 
  int fileSize;                                 // Size of binary file in bytes 
  int selectSize;                               // Size of selected flash 
  int iselect; 

  unsigned int slot;

  std::string binFileName = "";
 
if(argc < 2)
cout<<"syntax: "<<argv[0]<<" slots to check"<<endl,exit(1);
#define NCURSES
// Create VME interface
 RCCVmeInterface *vme = 0;
 try {
   vme = new RCCVmeInterface();

 } catch (...) {
 
   std::cout << "Caught an exception when creating  VME interface." << std::endl; 
 } 
  if(vme == 0)return 0; 


const unsigned long mapSize     = 0x01000000;


 SctPixelRod::PpcVmeInterface* SLOT;//[argc-1];
float Tmax[argc-1],VAuxMax[argc-1],VIntMax[argc-1],T[argc-1],VAux[argc-1],VInt[argc-1],Tmin[argc-1],VAuxMin[argc-1],VIntMin[argc-1];
for(int j=0;j<argc-1;j++){		//INIT VARIABLES
  // sscanf(argv[j+1],"%d",&slot);
       	Tmin[j]=100;
	VAuxMin[j]=100;
	VIntMin[j]=100;
} 

  // Create PPC interface through VME 
#ifdef NCURSES  
initscr();
start_color();

init_pair(1,COLOR_BLUE,COLOR_WHITE);
init_pair(2,COLOR_RED,COLOR_WHITE);
init_pair(3,COLOR_BLACK,COLOR_WHITE);

#endif
signal(SIGINT,signal_callback_handler);
while(stop==0){

for(int j=0;j<argc-1;j++){	//CYCLE ON SLOTS
	sscanf(argv[j+1],"%d",&slot);

	SLOT =new PpcVmeInterface((slot << 24),mapSize,*vme);//SLOT[j]
	unsigned long baseAddress = (slot << 24);
	const unsigned long mapSize     = 0x01000000;

	SLOT->ppcSingleWrite(BASEADDR_SYSMON,0x0000000A);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x304, (SLOT->ppcSingleRead(BASEADDR_SYSMON+0x304)&0x00000FFF)|0x2000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x300, (SLOT->ppcSingleRead(BASEADDR_SYSMON+0x300)&0x0000CFFF)|0x000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x330, 0x00000000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x334, 0x00000001);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x338, 0x00000000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x33C, 0x00008001);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x328, 0x00000100);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x32C, 0x00000000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x320, 0x00000700);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x324, 0x00000000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x308, 0x2000);//SLOT[j]
	SLOT->ppcSingleWrite(BASEADDR_SYSMON+0x304,(SLOT->ppcSingleRead(BASEADDR_SYSMON+0x304)&0x00000FFF)|0x2000 );//SLOT[j]
	while ((SLOT->ppcSingleRead(BASEADDR_SYSMON+0x04)&0x40) != 0x40){//SLOT[j]
	  static int m=0;
	  if (m==1000) {m=0;break;}
	  m++;
#ifndef NCURSES
	  cout<<"ERR!!! read register 04	"<<SLOT->ppcSingleRead(BASEADDR_SYSMON+0x04)<<endl;//SLOT[j]
#endif
	}
	T[j]=((float)(SLOT->ppcSingleRead(BASEADDR_SYSMON+0x200))/65536.0/0.00198421639)-273.15;//SLOT[j]
	VAux[j]=((float)(SLOT->ppcSingleRead(BASEADDR_SYSMON+0x208)))/65536.0f*3.0f;//SLOT[j]
	VInt[j]=((float)(SLOT->ppcSingleRead(BASEADDR_SYSMON+0x204)))/65536.0f*3.0f;//SLOT[j]

	std::streambuf* cout_sbuf = std::cout.rdbuf(); // save original sbuf
	std::ofstream fout("/dev/null");
	std::cout.rdbuf(fout.rdbuf()); // redirect 'cout' to a 'fout'
	
	delete SLOT;

	std::cout.rdbuf(cout_sbuf); // restore the original stream buffer


	Tmax[j]= (Tmax[j]<T[j] || Tmax[j]> 200) ? T[j] : Tmax[j];
	VAuxMax[j]= (VAuxMax[j]<VAux[j]) ? VAux[j] : VAuxMax[j];
	VIntMax[j]= (VIntMax[j]<VInt[j]) ? VInt[j] : VIntMax[j];
	Tmin[j]= (Tmin[j]<T[j]) ? Tmin[j] : T[j];
	VAuxMin[j]= (VAuxMin[j]<VAux[j]) ? VAuxMin[j] : VAux[j];
	VIntMin[j]= (VIntMin[j]<VInt[j]) ? VIntMin[j] : VInt[j];
	
	VAuxMax[j]= (VAuxMax[j]>100) ? 0 : VAuxMax[j];
	VIntMax[j]= (VIntMax[j]>100) ? 0 : VIntMax[j];
#ifdef NCURSES
	attron(COLOR_PAIR(3));
	mvprintw(j+1,0,"SLOT %d",slot);
	mvprintw(0,8,"TempMin");
	if(Tmin[j]<20) attron(COLOR_PAIR(1)); 
	mvprintw(j+1,8,"%3.4f",Tmin[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,18,"Temp");
	if(T[j]<20) attron(COLOR_PAIR(1)); 
	if(T[j]>60) attron(COLOR_PAIR(2)); 
	mvprintw(j+1,18,"%3.4f",T[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,28,"TempMax");
	if(T[j]>60) attron(COLOR_PAIR(2)); 
	mvprintw(j+1,28,"%3.4f",Tmax[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,38,"VccAUXMin");
	if(VAuxMin[j]<2.375) attron(COLOR_PAIR(1));
	mvprintw(j+1,38,"%3.4f",VAuxMin[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,48,"VccAUX");
	if(VAux[j]>2.625) attron(COLOR_PAIR(1));
	if(VAux[j]<2.375) attron(COLOR_PAIR(2));
	mvprintw(j+1,48,"%3.4f",VAux[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,58,"VccAUXMax");
	if(VAuxMax[j]>2.625) attron(COLOR_PAIR(2));
	mvprintw(j+1,58,"%3.4f",VAuxMax[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,68,"VccINTMin");
	if(VIntMin[j]<0.95) attron(COLOR_PAIR(1));
	mvprintw(j+1,68,"%3.4f",VIntMin[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,78,"VccINT");
	if(VInt[j]<0.95) attron(COLOR_PAIR(1));
	if(VInt[j]>1.05) attron(COLOR_PAIR(2));
	mvprintw(j+1,78,"%3.4f",VInt[j]);
	attron(COLOR_PAIR(3));
	mvprintw(0,88,"VccINTMax");
	if(VIntMax[j]>1.05) attron(COLOR_PAIR(2));
	mvprintw(j+1,88,"%3.4f",VIntMax[j]);
	attron(COLOR_PAIR(3));
#else
	//cout<<"TEMP: "<<((float)(Xil_In32(BASEADDR_SYSMON+0x200))/65536.0/0.00198421639)-273.15<<endl;
#endif
	
 }
 refresh();
 sleep(2);
#ifdef NCURSES 
 erase();
#endif
 }
#ifdef NCURSES 
 endwin();
#endif
 // for(int j=0;j<argc-1;j++){
 //   delete SLOT;//SLOT[j]
   // } 
 return 0;  
 
}

