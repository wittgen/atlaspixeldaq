#include <iostream>
#include <string>
#include <iomanip>
/*
#include "PixModule.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI1.h"
#include "PixFe/PixFeI4A.h"
*/
#include <ctype.h>

#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>

#include <cstdint>  //AKU

#define IBL 1


#include "NetRodModule.h"
#include "NetVmeInterface.h"

#ifdef USE_VME
#include "RodModule.h"
#include "RCCVmeInterface.h"
#endif

#ifdef IBL
#include "iblSlaveCmds.h"
#include "iblBocInt.h"  // BOC registers
#include "iblModuleConfigStructures.h"
#include <stdint.h>

#include "FEI4.h"
#include "FEI4Tools.h"
#include "SlaveTools.h"

void crc32(char *p, int nr, uint32_t *cval);
#endif

//#define LOAD_SLAVES

#ifndef ROD_SLAVE_H 
// CTL_BOC_ENABLE_BIT: 0: input from local FE-I4. 1: BOC input
#define CTL_BOC_ENABLE_BIT 0
// CTL_BOC_TEST_BIT: Select test input for boc. Requires BOC_ENABLE to be 1. Writes to BOC_TEST_ADDR supply data
#define CTL_BOC_TEST_BIT 1
// Histogrammer bits
// CTL_HIST_MODE: 0: sample mode, 1: readout mode
#define CTL_HIST_MODE0_BIT 2
#define CTL_HIST_MODE1_BIT 3
// CTL_HIST_MUX: 0: test input via write to MB_HISTx_TEST_REG. 1: EFB input
#define CTL_HIST_MUX0_BIT 4
#define CTL_HIST_MUX1_BIT 5
// DMA bits
// CTL_INMEM_SEL0_BIT, CTL_INMEM_SEL1_BIT: Select inmem channel via inmem sel bits 0 and 1. 00:A, 01:B, 10:C, 11:D
#define CTL_INMEM_SEL0_BIT 6
#define CTL_INMEM_SEL1_BIT 7
#define CTL_MASTER_MODE_BIT 30 // read-only
#define CTL_MASTER_RESET_BIT 31 // read-only
#endif 

//#define VERBOSE

using namespace std;
//using namespace PixLib;

using namespace SctPixelRod;


int main(int argc, char *argv[]) {

  int i;
  RodPrimList primList(1);                      // Primitive List
  std::string SlaveFile; // AKU ("/home/jsvirzi/slave.dld");
  std::string infobuf("");
  std::string errorbuf("");


  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=2;                       // Number of slaves
  int slaveNumber=-1;
  std::string fileName(""), option;
  bool initRod = true;
  bool sbcVme = false;
  std::string rodIp = "127.0.0.1";
  rodIp = "192.168.1.80";
  int slot = -1;

  unsigned long baseAddress; //, txtBuffSize;

  if (sizeof(int)> 4){
	  std::cout << "Int size not 4. Exiting" << std::endl;
	  exit(1);
  }
  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'b': {
          sbcVme = true;
          break;
        }
        case 'n': {
          initRod = false;
          break;
        }
        case 'i': {
          //rodIp = (char*)option.substr(2).c_str();
          rodIp = option.substr(2); //.c_str();
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          slaveNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Create VME interface and RodModule
  VmeInterface *vme1 = 0;
  AbstrRodModule* rod0 = 0;
  //FEI4 fei4_module;
  //fei4_module.setupRod(argc, argv); //2nd connect
  //AbstrRodModule* rod0 = fei4_module.getRod();

  vme1 = new NetVmeInterface(rodIp.c_str()); //3rd connect
  //vme1 = NetVmeInterface::makeNetVmeInterface();
  rod0 = new NetRodModule(baseAddress, mapSize, *vme1, numSlaves);

  // initialise RodModule
  try{
    cout << "LED Start init" << endl;
    rod0->initialize(true);
  }

  catch (VmeException &v) {
    cout << "VmeException creating RodModule." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl;
  }
  catch (RodException &r) {
  cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
          << '\n';
  }

#ifdef IBL
  // send a slave primitive
  SendSlavePrimitiveIn *slvCmdPrim;
  IblSlvRdWr *slvCmd;
  IblSlvNetCfg *slvNetCmd;
  RodPrimitive *sendSlvCmdPrim;
  // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
  uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
  // we need an outlist too
  RodOutList* outList;
  unsigned long *outBody;
  iblSlvStat *rodStatus;

  SlaveFile = "../IblDsp/SLAVE/bin/histClient.bin";	// prog is executed in Debug
  int slv;
  for (slv = 0; slv < N_IBL_SLAVES ; slv++){
      cout << "Using " << SlaveFile << " on slave" << slv << endl;
      getStatus(rod0,SLV_STAT_TYPE_STD,slv);

      // next 2 lines generate max. output from the slave. Comment, if you don't want
      setVerbose(rod0,1,slv);
      setUart(rod0,slv + 1);
      // ----------------------------------------------------------------------------

      //loadSlave(rod0, SlaveFile, slv);
      // Slave binary already loaded when flashing the ELF file.
      startSlave(rod0, slv);
      getStatus(rod0,SLV_STAT_TYPE_STD,slv);
      setVerbose(rod0,0,slv);
      setId(rod0, slv);
  }

  // set Uart Output back to PPC
  setUart(rod0,0);

  // ##########  TEST WRITE/READ FOR BOTH SLAVES ################### 
  //for (int slv = 0; slv < N_IBL_SLAVES ; slv++){
  for (int slv = 0; slv < 1 ; slv++){//lynn
    cout << endl << "Wr/rd test slave " << slv << endl;
    uint32_t wBuf[10] = {1,2,3,4,0x5aa56cc6,0x12345678};
    uint32_t rBuf[10] = {0};
    sendData(rod0,SLV_CMD_WRITE,0,6,wBuf,slv);
    getData(rod0,SLV_CMD_READ,0,6,0,rBuf,slv);
    for (i=0; i<6;i++)
      cout << "Read data " << i << ": 0x" << hex << rBuf[i] << endl;
  }
  // ############################################################## 

  setUart(rod0, 1);
  setVerbose(rod0,1,0);
  sleep(1);

  //########### CONFIGURE NETWORK FOR SLAVE 0 #################
  IblSlvNetCfg netCfgOut, netCfgIn;
  unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x04};
  unsigned char lIp[4] = {192, 168,   1, 81};
  unsigned char maskIp[4] = {255,255,0,0};
  unsigned char gwIp[4] = {192, 168,   0, 1};
  unsigned char serverIp[4] = {192, 168,   0, 1};

  unsigned char n_mac[8], n_lIp[4], n_maskIp[4], n_gwIp[4], n_serverIp[4];
  *(uint32_t*)n_mac = htonl(*(uint32_t*)mac);
  *((uint32_t*)n_mac+1) = htonl(*((uint32_t*)mac+1));
  *(uint32_t*)n_lIp = htonl(*(uint32_t*)lIp);
  *(uint32_t*)n_maskIp = htonl(*(uint32_t*)maskIp);
  *(uint32_t*)n_gwIp = htonl(*(uint32_t*)gwIp);
  *(uint32_t*)n_serverIp = htonl(*(uint32_t*)serverIp);

  memcpy(&netCfgOut.localMac,n_mac,sizeof(mac));
  memcpy(&netCfgOut.localIp,n_lIp,sizeof(lIp));
  memcpy(&netCfgOut.maskIp,n_maskIp,sizeof(maskIp));
  memcpy(&netCfgOut.gwIp,n_gwIp,sizeof(gwIp));
  memcpy(&netCfgOut.targetIp,n_serverIp,sizeof(serverIp));
  netCfgOut.targetPort = htonl(5002);
  setNetCfg(rod0,&netCfgOut,0);

  std::cout << "press enter when connection to fitfarm server is established..." << std::endl;
  std::string s;
  std::getline(std::cin, s);
  std::cout << "moving on..." << std::endl;

  getNetCfg(rod0,&netCfgIn,0);
  //#####################################################



  //#################### SET HISTO CONFIG ####################################
  cout << endl << "Histo config slave 0" << endl;
  // set histogram config
  IblSlvHistCfg histCfgOut, histCfgIn;
  histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
  histCfgOut.cfg[0].enable = 1;
  histCfgOut.cfg[0].type = 1;
  //histCfgOut.cfg[0].colStep = 0;
  histCfgOut.cfg[0].maskStep = 0;
  histCfgOut.cfg[0].chipSel = 0;
  histCfgOut.cfg[0].addrRange = 0;
  histCfgOut.cfg[0].scanId = 0x1234;
  histCfgOut.cfg[0].binId = 1;
  histCfgOut.cfg[0].nTrigs = 60;
  memcpy(&histCfgOut.cfg[1],&histCfgOut.cfg[0],sizeof(IblSlvHistUnitCfg));
  // enable hist0 only
  histCfgOut.cfg[1].enable = 0;


  //#################### START/TEST HISTO ####################################
  cout << endl << "Start/Test histogramming on slave 0" <<"\n"<< endl;
  startHisto(rod0, &histCfgOut, 0);
  getHistCfg(rod0, &histCfgIn, 0);

  // send some histogramm test data
#define TESTSIZE 100
#define HISTSIZE (ROW_SIZE_IMP * COL_SIZE * (1 << CHIP_BITS))
  uint32_t testData [TESTSIZE];
  uint32_t results [HISTSIZE];
  for (i = 0; i< TESTSIZE; i++){
    testData[i] = 0;
  }
  testData[0] = PIXVAL(0, 1, 1, 1); // chip 0, row 1, col 1 => pixel number 0
  testData[1] = PIXVAL(0, 1, 2, 1);
  testData[2] = PIXVAL(0, 1, 3, 1);
  testData[3] = PIXVAL(0, 1, 4, 1);
  testData[4] = PIXVAL(0, 1, 5, 1);
  testData[5] = PIXVAL(0, 1, 6, 1);
  testData[6] = PIXVAL(0, 1, 7, 1);
  testData[7] = PIXVAL(0, 1, 8, 1);
  testData[8] = PIXVAL(0, 1, 9, 1);
  testData[9] = PIXVAL(0, 1,10, 1);

  testData[10] = PIXVAL(0, 2, 1, 2);
  testData[11] = PIXVAL(0, 2, 2, 2);
  testData[12] = PIXVAL(0, 3, 3, 4);
  testData[13] = PIXVAL(0, 3, 4, 4);
  testData[14] = PIXVAL(0, 1,10, 5);    // this pixel is set twice !
  testData[15] = PIXVAL(0, 1,11, 6);
  testData[16] = PIXVAL(0, 1,12, 7);
  testData[17] = PIXVAL(0, 1,13, 8);
  testData[18] = PIXVAL(0,10,14, 9);
  testData[19] = PIXVAL(0,10,15,10);

  /*for(int x=0;x<80;x++){
    testData[x] = PIXVAL(0, 1, x+1, 1);
    }*/

  sendData(rod0,SLV_CMD_HIST_TEST,0,20,testData,0);
  //sendData(rod0,SLV_CMD_HIST_TEST,0,10,testData,0);
  //sendData(rod0,SLV_CMD_HIST_TEST,0,5,testData,0);

  IblSlvHistTerm histTermCfg;
  unsigned char targetIp[4] = {192, 168,   0, 1};
  memcpy(&histTermCfg.targetIp, &targetIp, sizeof(targetIp));
  histTermCfg.targetPort = 5002;
  histTermCfg.pcol = 1;
  stopHisto(rod0, &histTermCfg, 0);

  // testing if can send multiple histos...
  /*for(int x=0;x<4;x++){
    startHisto(rod0, &histCfgOut, 0);
    sendData(rod0,SLV_CMD_HIST_TEST,0,20,testData,0);
    stopHisto(rod0, &histTermCfg, 0);
    }*/

  //setVerbose(rod0,0,0);
  //setUart(rod0, 0);
  //sleep(1);

  // the very end
  free(slvPrimBuf);

  // try to send a module configuration
#endif

// Delete the ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;
}

