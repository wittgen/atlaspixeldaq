
#include <RodController.h>
#include <SlaveTools.h>
#include <FEI4Tools.h>
#include <BocTools.h>


#include <iostream>
#include <cstdlib>
using namespace std;

int main(int argc, char** argv) {
#if 0
        cout << "Starting ROD..." << flush;
        int ret = system("iblRodPPC start+slaves >/dev/null");
        if(ret) {
                cout << "Could not restart ROD." << endl;
                return 1;
        }
        cout << "Done." << endl;
#endif

	//RodController rodCtrl(argc, argv);
 	//AbstrRodModule* rod0 = rodCtrl.getRod();

	FEI4 fei4_module;
	AbstrRodModule* rod0 = fei4_module.getRod();
	int channel = 1;
	int slave = 0;
	slave = 1;
        int hex_base = 0x8000;
        if(slave) hex_base = 0x4000;
/*
	for(int c = 0 ; c < 4 ; ++c) {
		cout << "Disabling channel " << c << endl;
		writeDevice(rod0, 5, 0xC+(slave<<1), 0x0);
		writeBoc(rod0, hex_base | 0xC00+(c<<5), 0x0);
		writeBoc(rod0, hex_base | 0x800+(c<<5), 0x0);
		writeBoc(rod0, hex_base | 0x801+(c<<5), 0x0);
	}
*/
	bool isOptical(true);
	isOptical = false;
	cout << "SETUP TYPE: " << (isOptical?"OPTICAL":"ELECTRICAL") << endl;
	
        int tx_ch = channel;
        int rx_ch = channel-1;
	int chipID = 0;
	//initPixelLabIblSetup(fei4_module, slave, tx_ch, rx_ch, chipID);

	// Second Module
	tx_ch = 2;
	rx_ch = 2;

	// Not working because readout is not implemented in the ROD
	// tx_ch = 3;
	// rx_ch = 4;

	if(slave==1) { tx_ch = 1; rx_ch = 1; }
	if(slave==1) { tx_ch = 0; rx_ch = 0; }

	chipID = 0;
	chipID = 6;
#if 0
	for(int c = 0 ; c < 8 ; ++c) {
		cout << "Disabling Rx(" << c << ")" << endl;
		writeBoc(rod0, hex_base | 0x800+(c<<5), 0x0);
	}
#endif 
	initPixelLabIblSetup(fei4_module, slave, tx_ch, rx_ch, chipID, isOptical);

	return 0;


	for(int c = 0 ; c < 8 ; ++c) {
		writeDevice(rod0, 5, 0xC+(slave<<1), 0x0);
		writeBoc(rod0, hex_base | (0xC00+(c<<5)), 0x0);
		writeBoc(rod0, hex_base | (0x800+(c<<5)), 0x0);
		writeBoc(rod0, hex_base | (0x801+(c<<5)), 0x0);
	}

	//for(int c = 0 ; c < 8 ; ++c) _setSlaveReadBack(rod0, 0, c);

	return 0;

}
