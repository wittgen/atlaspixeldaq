lcl_srcs := $(wildcard src/*.c[xp][xp])

ifndef HAS_GPIB
#MiniDCSClient can be built without GPIB
lcl_srcs := $(filter-out src/MiniDCS.cxx, $(lcl_srcs))
endif

srcs += $(lcl_srcs)
bins += $(call src2bin, $(lcl_srcs))
objs += $(call src2obj, $(lcl_srcs))
deps += $(call src2dep, $(lcl_srcs))
