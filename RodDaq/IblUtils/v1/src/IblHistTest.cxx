#include <iostream>
#include <string>
#include <iomanip>
/*
#include "PixModule.h"
#include "PixFe/PixFe.h"
#include "PixFe/PixFeI1.h"
#include "PixFe/PixFeI4A.h"
*/
#include <ctype.h>

#include <sys/time.h>
#include <stdlib.h>
#include <unistd.h>

#include <string.h>

#include <cstdint>  //AKU

#define IBL 1



#include "NetRodModule.h"
#include "NetVmeInterface.h"

#ifdef USE_VME
#include "RodModule.h"
#include "RCCVmeInterface.h"
#endif

#ifdef IBL
#include "iblSlaveCmds.h"
#include "iblBocInt.h"  // BOC registers
#include "iblModuleConfigStructures.h"
#include <stdint.h>

#include "FEI4.h"
#include "FEI4Tools.h"
#include "SlaveTools.h"

void crc32(char *p, int nr, uint32_t *cval);
#endif

#define ROW_BITS 9
#define COL_BITS 7
#define CHIP_BITS 3
#define TOT_BITS 4
#define ROW_SHIFT 0
#define COL_SHIFT (ROW_BITS + ROW_SHIFT)
#define CHIP_SHIFT (COL_BITS + COL_SHIFT)
#define TOT_SHIFT (CHIP_BITS + CHIP_SHIFT)
#define ROW_SIZE_IMP 336  // 4 if simulation
#define ROW_SIZE_SIM 4    // 4 if simulation
#define COL_SIZE 80

// results
#define OCC_RESULT_BITS 7
#define OCC_RESULT_SHIFT 0
#define TOT_RESULT_BITS 10 // totWidth + totIterBits;
#define TOT_RESULT_SHIFT 8 //totRamOffs
#define TOTSQR_RESULT_BITS 14 // 2*totWidth + totIterBits;
#define TOTSQR_RESULT_SHIFT 18 //totSqrRamOffs

// conversions
#define PIXVAL(chip, row, col, tot) ((tot << TOT_SHIFT) + (chip << CHIP_SHIFT) + (row << ROW_SHIFT) + (col << COL_SHIFT))
#define PIXCOL(data) (((data >> COL_SHIFT) & ((1 << COL_BITS) - 1)) - 1)
#define PIXROW(data) (((data >> ROW_SHIFT) & ((1 << ROW_BITS) - 1)) - 1)
#define PIXCHIP(data) ((data >> CHIP_SHIFT) & ((1 << CHIP_BITS) - 1))
#define PIXNUM(data) ((PIXCHIP(data) * ROW_SIZE_IMP * COL_SIZE) + (PIXROW(data) * COL_SIZE) + PIXCOL(data))
//#define LOAD_SLAVES

#ifndef ROD_SLAVE_H 
// CTL_BOC_ENABLE_BIT: 0: input from local FE-I4. 1: BOC input
#define CTL_BOC_ENABLE_BIT 0
// CTL_BOC_TEST_BIT: Select test input for boc. Requires BOC_ENABLE to be 1. Writes to BOC_TEST_ADDR supply data
#define CTL_BOC_TEST_BIT 1
// Histogrammer bits
// CTL_HIST_MODE: 0: sample mode, 1: readout mode
#define CTL_HIST_MODE0_BIT 2
#define CTL_HIST_MODE1_BIT 3
// CTL_HIST_MUX: 0: test input via write to MB_HISTx_TEST_REG. 1: EFB input
#define CTL_HIST_MUX0_BIT 4
#define CTL_HIST_MUX1_BIT 5
// DMA bits
// CTL_INMEM_SEL0_BIT, CTL_INMEM_SEL1_BIT: Select inmem channel via inmem sel bits 0 and 1. 00:A, 01:B, 10:C, 11:D
#define CTL_INMEM_SEL0_BIT 6
#define CTL_INMEM_SEL1_BIT 7
#define CTL_MASTER_MODE_BIT 30 // read-only
#define CTL_MASTER_RESET_BIT 31 // read-only
#endif 

//#define VERBOSE

using namespace std;
//using namespace PixLib;

using namespace SctPixelRod;

//void getStatus(AbstrRodModule* rod0, int type, int slave);
//void setVerbose(AbstrRodModule* rod0, int on, int slave);
//void setId(AbstrRodModule* rod0, int slave);
//void sendData(AbstrRodModule* rod0, int type, int addr, int nWords, uint32_t *data, int slave);
//void getData(AbstrRodModule* rod0, int type, int addr, int nWords, uint32_t parm, uint32_t *data, int slave);
//void getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
//void setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
//void startHisto(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave);
//void stopHisto(AbstrRodModule* rod0, IblSlvHistTerm *cfg, int slave);
//void getHistCfg(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave);
//void loadSlave(AbstrRodModule* rod0, std::string SlaveFile, int slvId);
//void setUart(AbstrRodModule* rod0, int source);
//void sendSport(AbstrRodModule* rod0, uint32_t* data, int bits);
//uint32_t readBoc(AbstrRodModule* rod0, int regNum);
//void writeBoc(AbstrRodModule* rod0, int regNum, uint32_t data);

int main(int argc, char *argv[]) {


  int i;
  RodPrimList primList(1);                      // Primitive List
  std::string SlaveFile; // AKU ("/home/jsvirzi/slave.dld");
  std::string infobuf("");
  std::string errorbuf("");


  const unsigned long mapSize=0xc00040;         // Map size
  const long numSlaves=2;                       // Number of slaves
  int slaveNumber=-1;
  std::string fileName(""), option;
  bool initRod = true;
  bool sbcVme = false;
  std::string rodIp = "127.0.0.1";
  rodIp = "192.168.1.150";
  int slot = -1;

  unsigned long baseAddress; //, txtBuffSize;

  if (sizeof(int)> 4){
	  std::cout << "Int size not 4. Exiting" << std::endl;
	  exit(1);
  }
  if (argc > 1) {
    for (int i=1; i<argc; i++) {
      option = argv[i];
      if (option[0] != '-') break;
      switch (option[1]) {
        case 'b': {
          sbcVme = true;
          break;
        }
        case 'n': {
          initRod = false;
          break;
        }
        case 'i': {
          //rodIp = (char*)option.substr(2).c_str();
          rodIp = option.substr(2); //.c_str();
          break;
        }
        case 's': {
          slot = atoi(option.substr(2).c_str());
          break;
        }
        case 'v': {
          slaveNumber = atoi(option.substr(2).c_str());
          break;
        }
        default: {
          break;
        }
      }
    }
  }

// Create VME interface and RodModule
  VmeInterface *vme1 = 0;
  //AbstrRodModule* rod0 = 0;
  FEI4 fei4_module;
  fei4_module.setupRod(argc, argv);
  AbstrRodModule* rod0 = fei4_module.getRod();

  vme1 = new NetVmeInterface(rodIp.c_str());
  //vme1 = NetVmeInterface::makeNetVmeInterface();
  rod0= new NetRodModule(baseAddress, mapSize, *vme1, numSlaves);

  // initialise RodModule
  try{
    cout << "LED Start init" << endl;
    rod0->initialize(true);
  }

  catch (VmeException &v) {
    cout << "VmeException creating RodModule." << endl;
    cout << "ErrorClass = " << v.getErrorClass() << endl;
    cout << "ErrorCode = " << v.getErrorCode() << endl;
  }
  catch (RodException &r) {
  cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
          << '\n';
  }

#ifdef IBL
  // send a slave primitive
  SendSlavePrimitiveIn *slvCmdPrim;
  IblSlvRdWr *slvCmd;
  IblSlvNetCfg *slvNetCmd;
  RodPrimitive *sendSlvCmdPrim;
  // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
  uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
  // we need an outlist too
  RodOutList* outList;
  unsigned long *outBody;
  iblSlvStat *rodStatus;

  SlaveFile = "../IblDsp/SLAVE/bin/histClient.bin";	// prog is executed in Debug
  for (int slv = 0; slv < N_IBL_SLAVES ; slv++){
      cout << "Using " << SlaveFile << " on slave" << slv << endl;
      getStatus(rod0,SLV_STAT_TYPE_STD,slv);

      // next 2 lines generate max. output from the slave. Comment, if you don't want
      setVerbose(rod0,1,slv);
      setUart(rod0,slv + 1);
      // ----------------------------------------------------------------------------

      //loadSlave(rod0, SlaveFile, slv);
      // Slave binary already loaded when flashing the ELF file.
      startSlave(rod0, slv);
      getStatus(rod0,SLV_STAT_TYPE_STD,slv);
      setVerbose(rod0,0,slv);
      setId(rod0, slv);
  }

  // set Uart Output back to PPC
  setUart(rod0,0);

  // ##########  TEST WRITE/READ FOR BOTH SLAVES ################### 
  for (int slv = 0; slv < N_IBL_SLAVES ; slv++){
    cout << endl << "Wr/rd test slave " << slv << endl;
    uint32_t wBuf[10] = {1,2,3,4,0x5aa56cc6,0x12345678};
    uint32_t rBuf[10] = {0};
    sendData(rod0,SLV_CMD_WRITE,0,6,wBuf,slv);
    getData(rod0,SLV_CMD_READ,0,6,0,rBuf,slv);
    for (i=0; i<6;i++)
      cout << "Read data " << i << ": 0x" << hex << rBuf[i] << endl;
  }
  // ############################################################## 

  //########### CONFIGURE NETWORK FOR BOTH SLAVES #################
  IblSlvNetCfg netCfgOut, netCfgIn;
  unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x04};
  unsigned char lIp[4] = {192, 168,   1, 11};
  unsigned char maskIp[4] = {255,255,0,0};
  unsigned char gwIp[4] = {192, 168,   0, 1};
  memcpy(&netCfgOut.localMac,mac,sizeof(mac));
  memcpy(&netCfgOut.localIp,lIp,sizeof(lIp));
  memcpy(&netCfgOut.maskIp,maskIp,sizeof(maskIp));
  memcpy(&netCfgOut.gwIp,gwIp,sizeof(gwIp));
  for (int slv = 0; slv < N_IBL_SLAVES ; slv++){
    cout << endl << "Network config on slave "<< slv <<"\n"<< endl;
    setNetCfg(rod0,&netCfgOut,slv);
    getNetCfg(rod0,&netCfgIn,slv);
    netCfgOut.localMac[7] ++;
    netCfgOut.localIp[3] ++;
  }
  //#####################################################


  //#################### SLAVE SELECTION ################
  //int slave = 0;
  int slave = 1;
  setUart(rod0, slave+1);
  setVerbose(rod0,1,slave);
  //  setUart(rod0, 1);
  //  setVerbose(rod0,1,0);
  sleep(1);
  //#####################################################


  //#################### SET SLAVE CTL REG ######################################
  int reg = 0x42;
  
  //int value = (1 << CTL_MASTER_MODE_BIT) + (1 << CTL_BOC_ENABLE_BIT) | (1 << CTL_INMEM_SEL0_BIT) | (1 << CTL_HIST_MUX0_BIT) | (1 << CTL_HIST_MODE0_BIT);
  int value = 0;
  //  writeDevice(rod0, slave, reg, value);
  cout << "Writing SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
  value = readDevice(rod0, slave, reg);
  cout << "Reading SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
  
  
  /*
  std::cout << "chage to MUX1." << std::endl; 
  value = (1 << CTL_MASTER_MODE_BIT) + (1 << CTL_BOC_ENABLE_BIT);
  writeDevice(rod0, slave, reg, value);
  cout << "Writing SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
  value = readDevice(rod0, slave, reg);
  cout << "Reading SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;

  std::cout << "chage to MUX1." << std::endl;

  //  value = (1 << CTL_MASTER_MODE_BIT) + (1 << CTL_BOC_ENABLE_BIT) | (1 << CTL_INMEM_SEL0_BIT) | (1 << CTL_HIST_MUX1_BIT);
  int value = (1 << CTL_MASTER_MODE_BIT) | (1 << CTL_BOC_ENABLE_BIT) | (1 << CTL_HIST_MUX0_BIT);
  writeDevice(rod0, slave, reg, value);
  cout << "Writing SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
  value = readDevice(rod0, slave, reg);
  cout << "Reading SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
  */
  //  return 0;
  //##############################################################################


  //#################### SET HISTO CONFIG ####################################
  cout << endl << "Histo config on slave " << slave << endl;
  int ntrigger=60; 
  IblSlvHistCfg histCfgOut, histCfgIn;
  histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
  histCfgOut.cfg[0].enable = 1;     //enable histo0 unit
  histCfgOut.cfg[0].type = 1;       //0 for occupancy histo (1 for ToT)
  // histCfgOut.cfg[0].colStep = 0;    //0 for single chip(or 1,2,3,4 quarter) 
  histCfgOut.cfg[0].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
  histCfgOut.cfg[0].chipSel = 0;    //0 for single chip(1 for all chips)
  histCfgOut.cfg[0].addrRange = 0;  //single chip <32K( full range < 1M)
  histCfgOut.cfg[0].scanId = 0x1234;//remote use only
  histCfgOut.cfg[0].binId = 1;
  histCfgOut.cfg[0].nTrigs = ntrigger;
  // copy the same config from histo0 to histo1
  memcpy(&histCfgOut.cfg[1],&histCfgOut.cfg[0],sizeof(IblSlvHistUnitCfg));
  // enable hist1
  histCfgOut.cfg[1].enable = 1;
  //##########################################################################
  cout << "before start hist; push return!" << std::endl;
  cin.get();

  //#################### START/TEST HISTO ####################################
  cout << endl << "Start/Test histogramming on slave " << slave <<"\n"<< endl;
  startHisto(rod0, &histCfgOut, slave);

  cout << "after start hist; push return!" << std::endl;
  cin.get();

  getHistCfg(rod0, &histCfgIn, slave);

  // define the histogramm test data to be sent
#define TESTSIZE 20
#define HISTSIZE (ROW_SIZE_IMP * COL_SIZE * (1 << CHIP_BITS))

  uint32_t testData[2][TESTSIZE];
  uint32_t results [HISTSIZE*2];

  for (i = 0; i< TESTSIZE/2; i++){
    testData[0][i]            = PIXVAL(0, 1, 1+i, 5);
    testData[0][TESTSIZE-i-1] = PIXVAL(0, 2, 1+i, 6);
    testData[1][i]            = PIXVAL(0, 1, 1+i, 7);
    testData[1][TESTSIZE-i-1] = PIXVAL(0, 2, 1+i, 8);
  }

  //#################### WRITING PSEUDO DATA ################################################
  // write test data in rod0, histid, TESTSIZE words, vector of data, in slave 
  for(int histid=0; histid<2; histid++){
    sendData(rod0,SLV_CMD_HIST_TEST, histid, TESTSIZE,testData[histid],slave);
  }
  //##########################################################################################
 

  //#################### CONFIGURIN THE CIP  #################################################
  int channel = 1;
  int rx_ch(0), tx_ch(0), chipID(0);
  bool isOptical(true);
  rx_ch = channel + 8; //8-f for RXB/D(higher RX), 0-7 for RXA/C(lower RX)
  tx_ch = channel;
  initPixelLabIblSetup(fei4_module, slave, tx_ch, rx_ch, chipID, isOptical);

  /*
  //###########################################################################################

  //#################### SENDING REAL DATA   #################################################
  // this part has to be verified
  ////digitalHistoInjection(fei4_module, ntrigger);
  ///digitalInjection(fei4_module);
  //###########################################################################################

  */

  IblSlvHistTerm histTermCfg;
  unsigned char targetIp[4] = {192, 168,   1, 9};
  memcpy(&histTermCfg.targetIp, &targetIp, sizeof(targetIp));
  histTermCfg.targetPort = 5002;
  histTermCfg.pcol = 1;
  stopHisto(rod0, &histTermCfg, slave);

  cout << "after stopping hist; push return!" << std::endl;
  cin.get();

  //#################### RETRIEVING DATA FROM SLAVE MEMORY ######################################
  for(int histid=0; histid<2; histid++){
    cout << endl <<"Test histogramming "<< histid <<" on slave "<< slave << endl;
    for (int i=0;i<TESTSIZE; i++){
      int pixNum;
      pixNum = PIXNUM(testData[1][i]);   // this is the pixel address, from 0 to 8*336*80, needed to read in the righ memory location.
      //getData(rod0,SLV_CMD_HIST_READ,pixNum,1,0,&results[pixNum],slave); // read individual pix from histo 0
      //getData(rod0,SLV_CMD_HIST_READ,pixNum,1,1,&results[pixNum],slave); // read individual pix from histo 1
      //printf("pixnum: %d\n", pixNum);
      getData(rod0,SLV_CMD_HIST_READ,pixNum,1,histid,&results[pixNum],slave); // read from addr "pixnum" the individual pix "1" from "histid" in "slave"
      printf("Hist %d, col %d, row %d, result 0x%x: occ %d, tot %d, tot² %d\n", histid, PIXCOL(testData[histid][i]), PIXROW(testData[histid][i]), results[pixNum], (results[pixNum] >> OCC_RESULT_SHIFT) & ((1 << (OCC_RESULT_BITS)) - 1),
	     (results[pixNum] >> TOT_RESULT_SHIFT) & ((1 << (TOT_RESULT_BITS)) - 1), (results[pixNum] >> TOTSQR_RESULT_SHIFT) & ((1 << (TOTSQR_RESULT_BITS)) - 1));
    }
  }
  cout << "after reading data; push return!" << std::endl;
  cin.get();

  //############################################################################################

  setVerbose(rod0,0,slave);
  setUart(rod0, 0);
  sleep(1);

  // the very end
  free(slvPrimBuf);

  // try to send a module configuration
#endif

// Delete the ROD and VME objects before exiting
  delete rod0;
  delete vme1;

  return 0;
}

/*
void getStatus(AbstrRodModule* rod0, int type, int slave){
  // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    // we need an outlist too
    RodOutList* outList;
    iblSlvStat *rodStatus;
#ifdef VERBOSE
    cout << "Sending slave " << slave << " command status via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_STAT;
    slvCmd->addr = 0;
    slvCmd->count = 1;
    slvCmd->data = type;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
						1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    rodStatus = (iblSlvStat*) outList->getMsgBody(1);
    cout << "Status: program type     " << hex << rodStatus->progType  << endl;
    cout << "Status: program version  " << hex << rodStatus->progVer  << endl;
    cout << "Status: status value     " << hex << rodStatus->status  << endl;
    rod0->deleteOutList();
    delete sendSlvCmdPrim;
    free(slvPrimBuf);
}
*/
/*
void setVerbose(AbstrRodModule* rod0, int on, int slave){
  // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave " << slave << " command verbose via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_VERBOSE;
    slvCmd->addr = 0;
    slvCmd->count = 1;
    slvCmd->data = on;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
						1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
    free(slvPrimBuf);
}
*/
/*
void setId(AbstrRodModule* rod0, int slave){
  // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave " << slave << " command setId via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_ID_SET;
    slvCmd->addr = 0;
    slvCmd->count = 1;
    slvCmd->data = slave;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
						1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
    free(slvPrimBuf);
}
*/
/*
void sendData(AbstrRodModule* rod0, int type,  int addr, int nWords, uint32_t *data, int slave){
  // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    int i;
#ifdef VERBOSE
    cout << "Sending slave " << slave << " data, type " << type << " via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    // NB: nWords needs to be adapter to actual data size
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->fetchReply = 0;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = type;
    slvCmd->addr = addr;
    slvCmd->count = nWords;
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2) + slvCmd->count - 1;
    for (i=0; i < nWords; i++){
        ((uint32_t*)&slvCmd->data)[i] = data[i];
    //cout << "Addr " << hex << i << ": 0x " << hex << ((uint32_t*)cBuf)[i] << endl;
    }
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2) + slvCmd->count - 1,
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
    free(slvPrimBuf);
}
*/
/*
void getData(AbstrRodModule* rod0, int type, int addr, int nWords, uint32_t parm, uint32_t *data, int slave){
  // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    // we need an outlist too
    RodOutList* outList;
    unsigned long *outBody;
    int i;
#ifdef VERBOSE
    cout << "Reading slave " << slave << " data, type " << type << " via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = type;
    slvCmd->addr = addr;
    slvCmd->count = nWords;
    slvCmd->data = parm;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
						1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    for (i=0; i < nWords; i++){
        data[i] = ((uint32_t*)outList->getMsgBody(1))[i];
    //cout << "Addr " << hex << i << ": 0x " << hex << ((uint32_t*)cBuf)[i] << endl;
    }
    rod0->deleteOutList();
    delete sendSlvCmdPrim;
    free(slvPrimBuf);
}
*/
/*
void setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave){
    // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    IblSlvNetCfg *slvNetCmd;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave command net config set via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvNetCfg) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvNetCmd = (IblSlvNetCfg*)&slvCmdPrim[1];
    memcpy(slvNetCmd,cfg,sizeof(IblSlvNetCfg));
    slvNetCmd->cmd = SLV_CMD_NET_CFG_SET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvNetCfg) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
}
*/
/*
void getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave){
    // send a slave primitive
    int i;
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    IblSlvNetCfg *slvNetCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    // we need an outlist too
    RodOutList* outList;
    iblSlvStat *rodStatus;
#ifdef VERBOSE
    cout << "Sending slave " << slave <<" command net config get via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_NET_CFG_GET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    slvNetCmd = (IblSlvNetCfg*)outList->getMsgBody(1);
    memcpy(cfg, slvNetCmd, sizeof(IblSlvNetCfg));
    printf("Local MAC :  ");
    for (i=0;i<5;i++) printf("%x:", (unsigned char)cfg->localMac[2 + i]);
    printf("%x\n", (unsigned char)cfg->localMac[2 + i]);
    printf("Local IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->localIp[i]);
    printf("%d\n", (unsigned char)cfg->localIp[i]);
    printf("Mask IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->maskIp[i]);
    printf("%d\n", (unsigned char)cfg->maskIp[i]);
    printf("GW IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->gwIp[i]);
    printf("%d\n", (unsigned char)cfg->gwIp[i]);

    rod0->deleteOutList();
    delete sendSlvCmdPrim;
}
*/
/*
void startHisto(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave){
    // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    IblSlvHistCfg *slvHistCmd;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave " << slave << " command histo config set via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvHistCfg) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvHistCmd = (IblSlvHistCfg*)&slvCmdPrim[1];
    memcpy(slvHistCmd,cfg,sizeof(IblSlvHistCfg));
    slvHistCmd->cmd = SLV_CMD_HIST_CFG_SET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvHistCfg) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
}
*/
/*
void stopHisto(AbstrRodModule* rod0, IblSlvHistTerm *cfg, int slave){
    // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    RodPrimitive *sendSlvCmdPrim;
    IblSlvHistTerm *slvHistCmd;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave " << slave << " command histo term via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvHistTerm) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvHistCmd = (IblSlvHistTerm*)&slvCmdPrim[1];
    memcpy(slvHistCmd,cfg,sizeof(IblSlvHistTerm));
    slvHistCmd->cmd = SLV_CMD_HIST_CMPLT;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvHistTerm) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
}
*/
/*
void getHistCfg(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave){
    // send a slave primitive
    int i;
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    IblSlvHistCfg *slvHistCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
    // we need an outlist too
    RodOutList* outList;
    iblSlvStat *rodStatus;
#ifdef VERBOSE
    cout << "Sending slave " << slave <<" command hist config get via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;	// ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_HIST_CFG_GET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    slvHistCmd = (IblSlvHistCfg*)outList->getMsgBody(1);
    memcpy(cfg, slvHistCmd, sizeof(IblSlvHistCfg));
    for (i=0; i<2;i++){
        printf("Histo %d config\n",i);
        printf("  nChips %d \n",cfg->cfg[i].nChips);
        printf("  enable %d \n",cfg->cfg[i].enable);
        printf("  type %d \n",cfg->cfg[i].type);
        //printf("  colStep %d \n",cfg->cfg[i].colStep);
        printf("  chipSel %d \n",cfg->cfg[i].chipSel);
        printf("  addrRange %d \n",cfg->cfg[i].addrRange);
        printf("  scanId %d \n",cfg->cfg[i].scanId);
        printf("  binId %d \n",cfg->cfg[i].binId);
        printf("  nTrigs %d \n",cfg->cfg[i].nTrigs);
    }

    rod0->deleteOutList();
    delete sendSlvCmdPrim;
}
*/

// this function uses the slave number starting from 0, as all other functions
// only one slave is loaded per call
/*
void loadSlave(AbstrRodModule* rod0, std::string SlaveFile, int slvId){
  cout << "CWD is " << get_current_dir_name() << endl;

  rod0->loadAndStartSlaves(SlaveFile,slvId);
  cout << "SLAVE " << slvId << " LOADED!" << endl;
  }*/
/*
void setUart(AbstrRodModule* rod0, int source){
  // set uart source via EXPERT  primitive
    ExpertIn uartPrim;
    RodPrimitive *sendExpertPrim;
#ifdef VERBOSE
    cout << "Sending expert command " << endl;
#endif
    uartPrim.item = UART_SRC;
    uartPrim.arg[0] = source;
    sendExpertPrim = new RodPrimitive((sizeof(ExpertIn) >> 2), 1, EXPERT, 0, (long*)&uartPrim);
    rod0->executeMasterPrimitiveSync(*sendExpertPrim);
    delete sendExpertPrim;
    }*/

/// sendSPort: send serial stream. Parms: module, data buffer(32 bits per location), number of bits
/*
void sendSport(AbstrRodModule* rod0, uint32_t* data, int bits){
    // sportId must be 0
    // if dataPtr == 0, data follows immediately
    SendSerialStreamIn *sportPrim;
    RodPrimitive *sendSportPrim;
    uint32_t *buf;
    int primSize;
    int nWords = (bits + 31)/32;
    primSize = sizeof(SendSerialStreamIn) + nWords * sizeof(uint32_t);
    buf = (uint32_t*)malloc(primSize);
    if (0 == buf){
        cout << "sendSPort: malloc failed " << endl;
        return;
    }
    // pointer to primitive
    sportPrim = (SendSerialStreamIn*)buf;
    // copy input data
    memcpy(&sportPrim[1],data, nWords * sizeof(uint32_t));
    // set paramters
    sportPrim->sportId = 0; // must be 0
    sportPrim->nBits = bits;    // bits. nWords is /32
    sportPrim->fCapture = 0;    // if to capture via inmem fifo. not used yet
    sportPrim->nCount = 0;      // repetitions. not used yet
    sportPrim->dataPtr = 0;     // must be 0 for data to follow immediately, like here
    sportPrim->maskLo = 0;      // FE-I4 mask. not used yet
    sportPrim->maskHi = 0;      // FE-I4 mask. not used yet
    // create rod primitive
    sendSportPrim = new RodPrimitive(primSize/sizeof(uint32_t), 1, SEND_SERIAL_STREAM, 0, (long*)sportPrim);
    // execute
    rod0->executeMasterPrimitiveSync(*sendSportPrim);
    delete sendSportPrim;
    free(buf);
}*/
/*
void writeBoc(AbstrRodModule* rod0, int regNum, uint32_t data){
  // write to boc register
    BocCtrlIn bocCtlPrim;
    RodPrimitive *sendBocCtlPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
#ifdef VERBOSE
    cout << "Writing BOC register " << regNum << ", data " << data << " via BOC_CTL_PRIMITIVE " << endl;
#endif
    bocCtlPrim.regNum = regNum;
    bocCtlPrim.data = data;
    bocCtlPrim.mode = 0;    // write
    sendBocCtlPrim = new RodPrimitive((sizeof(BocCtrlIn) >> 2), 1, BOC_CTRL, 0, (long int*)&bocCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendBocCtlPrim);
    delete sendBocCtlPrim;
}
*/
/*
uint32_t readBoc(AbstrRodModule* rod0, int regNum){
  // write to boc register
    BocCtrlIn bocCtlPrim;
    RodPrimitive *sendBocCtlPrim;
    RodOutList* outList;
    uint32_t result;
#ifdef VERBOSE
    cout << "Reading BOC register " << regNum << " via BOC_CTL_PRIMITIVE " << endl;
#endif
    bocCtlPrim.regNum = regNum;
    bocCtlPrim.data = 0;    // is void
    bocCtlPrim.mode = 1;    // read
    sendBocCtlPrim = new RodPrimitive((sizeof(BocCtrlIn) >> 2), 1, BOC_CTRL, 0, (long int*)&bocCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendBocCtlPrim, outList);
    result = *(uint32_t*) outList->getMsgBody(1);
    rod0->deleteOutList();
    delete sendBocCtlPrim;
    return result;
}
*/


