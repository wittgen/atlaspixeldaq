
#include <RodController.h>
#include <SlaveTools.h>
#include <FEI4Tools.h>
#include <BocTools.h>


#include <iostream>
using std::cout;
using std::endl;
using std::hex;
using std::dec;

void _setSlaveReadBack(AbstrRodModule* rod0, int slave, int channel) {
        int bocResult;
        int data_readback;

        int tx_ch = channel;
        int rx_ch = channel-1;
        int V5mask = 1 << tx_ch;

	rx_ch = channel;

	int hex_base = 0x8000;
	if(slave) hex_base = 0x4000;

	int reg = 0xC+(slave<<1);

        cout << "Virtex5 mask: 0x" << hex << V5mask << endl;

        cout << "Setting on slave " << slave << " and reading back: link_enabled = 0x0003" << endl;
        //writeSlave(rod0, slave, 0x0, 0x00000003)
        writeDevice(rod0, slave, 0x0, 0x00000003);
        bocResult = readSlave(rod0, slave, 0x0);
        cout << "Slave " << slave << " register 0x" << hex << 0x0 << " read: 0x" << hex << bocResult << endl;

        //writeDevice(rod0, 5, reg, 0x00000003);
        writeDevice(rod0, 5, reg, V5mask);
        cout << "Writing Virtex5 register 0x" << hex << reg << " to 0x" << V5mask << dec << endl;

	reg = hex_base | (0xC00 + (tx_ch<<5));

        // setting up TX control register on BOC BMF-N for channel
        cout << "Setting up TX control register on BOC-BMF-North for channel " << tx_ch << endl;
        //writeBoc(rod0, 0x8C00+(channel<<5), 0x01);
        writeBoc(rod0, reg, 0x01); // Enabling BPM
        bocResult = readBoc(rod0, reg);
        cout << "Boc register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;

	reg = hex_base | (0x800 + (rx_ch<<5));

        // setting up RX control register on BOC BMF-N for channel
        cout << "Setting up RX control register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, reg, 0x03); // North BMF
        bocResult = readBoc(rod0, reg);
        cout << "Boc register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;


	reg = hex_base | (0x801 + (rx_ch<<5));

        // setting up RX status register on BOC BMF-N for channel
        cout << "Setting up RX status register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, reg, 0x0F);
        bocResult = readBoc(rod0, reg);
        cout << "Boc register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;
        // setting up RX status register on BOC BMF-S for channel
}

int main(int argc, char** argv) {

	RodController rodCtrl(argc, argv);
 	AbstrRodModule* rod0 = rodCtrl.getRod();
	int reg, bocResult;


	for(int c = 0 ; c < 4 ; ++c) {
		cout << "Configuring channel " << c << endl;
		writeDevice(rod0, 5, 0xC, 0x0);
		writeBoc(rod0, 0x8C00+(c<<5), 0x0);
		writeBoc(rod0, 0x8800+(c<<5), 0x0);
		writeBoc(rod0, 0x8801+(c<<5), 0x0);
	}
	//for(int c = 0 ; c < 8 ; ++c) _setSlaveReadBack(rod0, 0, c);

        cout << "Laser Enable" << endl;
        reg = 0x7;
        writeBoc(rod0, reg, 0xAB); // Enable BCF laser
        bocResult = readBoc(rod0, reg);
        cout << "Boc BCF register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;


	return 0;

}
