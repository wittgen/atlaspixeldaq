#ifndef __UDP_SOCKET_H__
#define __UDP_SOCKET_H__

#include <boost/thread.hpp>
#include <boost/asio.hpp>
using boost::asio::ip::udp;

class UdpServer {

protected:
	boost::asio::io_service io_service;
	udp::endpoint sender_endpoint;
	std::shared_ptr<udp::socket> sock;
	std::shared_ptr<boost::thread> theThread;

	std::vector<boost::uint8_t> data;
	virtual void rxData();
	virtual void txData();
	virtual void rxDecode() = 0;
	virtual void txEncode() = 0;

	virtual void run();
	
	virtual void printData();
public:

	UdpServer(unsigned int port);

	virtual void start();
	virtual void stop();

	virtual std::shared_ptr<boost::thread>& getThread();
};

#define __UDP_ASYNC_SERVER__
#ifdef __UDP_ASYNC_SERVER__
class UdpAsyncServer: public UdpServer {

protected:
        virtual void run();
	virtual void rxData();
	virtual void handleReceive(const boost::system::error_code&, size_t);
	virtual void handleSend(const boost::system::error_code&, std::size_t);

public:
	UdpAsyncServer(unsigned int port);
	~UdpAsyncServer();
};
#endif

class UdpClient {

protected:

    boost::asio::io_service io_service;
    std::shared_ptr<udp::socket> sock;
    std::shared_ptr<udp::resolver> resolver;
    std::shared_ptr<udp::resolver::query> query;
    udp::resolver::iterator iterator;
    udp::endpoint sender_endpoint;

public:

    UdpClient(const char* host, const char* port);

    virtual void run();
};

#endif
