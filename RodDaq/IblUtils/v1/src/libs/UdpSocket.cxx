#include "UdpSocket.h"

#include <iostream>

const size_t max_length = 1024;

UdpServer::UdpServer(unsigned int port) {
	data.resize(max_length);
	sock = std::shared_ptr<udp::socket>(new udp::socket(io_service, udp::endpoint(udp::v4(), port)));
}

void UdpServer::start() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	theThread = std::shared_ptr<boost::thread>(new boost::thread(boost::bind(&UdpServer::run, this)));
}

void UdpServer::stop() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	io_service.stop();
}

void UdpServer::run() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	while(1) {
		rxData();
		txData();
	}
}

void UdpServer::rxData() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	data.resize(max_length);
	size_t length = sock->receive_from(boost::asio::buffer(data, max_length), sender_endpoint);
	data.resize(length);
	printData();
}

void UdpServer::txData() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	printData();
	sock->send_to(boost::asio::buffer(data, data.size()), sender_endpoint);
}

void UdpServer::printData() {
	std::cout << "[" << data.size() << "]->\t" << std::hex;
	std::copy(data.begin(), data.end(), std::ostream_iterator<int>( std::cout, "\t"));
	std::cout << std::dec << std::endl;
}

std::shared_ptr<boost::thread>& UdpServer::getThread() {
	return theThread;
}

#ifdef __UDP_ASYNC_SERVER__
UdpAsyncServer::UdpAsyncServer(unsigned int port): UdpServer(port) {
	//rxData();
}

UdpAsyncServer::~UdpAsyncServer() {

}

void UdpAsyncServer::run() {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
	rxData();
	io_service.run();
        //while(1) {
	//rxData();
	//	usleep(1000); // 1 ms
        //}
}


void UdpAsyncServer::rxData() {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        data.clear();
        data.resize(max_length);
	sock->async_receive_from(boost::asio::buffer(data), sender_endpoint, 
		boost::bind(&UdpAsyncServer::handleReceive, this, 
			boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void UdpAsyncServer::handleReceive(const boost::system::error_code& error, std::size_t length/* bytes_transferred */) {
        std::cout << __PRETTY_FUNCTION__ << std::endl;
	//std::cout << "--> Data size: " << data.size() << " received " << length << std::endl;
	// Address data 
	rxDecode();
	if(!error || error == boost::asio::error::message_size) {
		sock->async_send_to(boost::asio::buffer(data), sender_endpoint,
				boost::bind(&UdpAsyncServer::handleSend, this,
					boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
		rxData();
	}
}

void UdpAsyncServer::handleSend(const boost::system::error_code& /* error */, std::size_t /* bytes_transferred */) {
}
#endif

UdpClient::UdpClient(const char *host, const char* port) {
	sock = std::shared_ptr<udp::socket>( new udp::socket(io_service, udp::endpoint(udp::v4(), 0)) );
	resolver = std::shared_ptr<udp::resolver>( new udp::resolver(io_service) );
	query = std::shared_ptr<udp::resolver::query>( new udp::resolver::query(udp::v4(), host, port) );
	iterator = resolver->resolve(*query);
}

void UdpClient::run() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::vector<boost::uint8_t> data, thisData;
	data.reserve(max_length);
	
	thisData.push_back(0x5A);
	thisData.push_back(0x08);
	thisData.push_back(0x1B);
	thisData.push_back(0xFF);
	thisData.push_back(0xFF);
	sock->send_to(boost::asio::buffer(thisData, thisData.size()), *iterator);
	size_t length = sock->receive_from(boost::asio::buffer(data, max_length), sender_endpoint);

	thisData.clear();

	thisData.push_back(0x5A);
	thisData.push_back(0x04);
	thisData.push_back(0x1B);
	sock->send_to(boost::asio::buffer(thisData, thisData.size()), *iterator);
	length = sock->receive_from(boost::asio::buffer(data, max_length), sender_endpoint);

}
