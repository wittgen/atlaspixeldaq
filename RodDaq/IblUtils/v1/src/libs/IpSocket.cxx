#include "IpSocket.h"
#include "boost/asio/write.hpp"
#include "boost/asio/read.hpp"


IpSocket::IpSocket(unsigned int port):
	rx_ios( new boost::asio::io_service() ),
	tx_ios( new boost::asio::io_service() ),
        rx_end( new boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port) ),
        tx_end( new boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port) ),
        rx_acc( new boost::asio::ip::tcp::acceptor(*rx_ios, *rx_end) ),
        tx_acc( new boost::asio::ip::tcp::acceptor(*tx_ios, *tx_end) ),
        rx_soc( new boost::asio::ip::tcp::socket(*rx_ios) ),
        tx_soc( new boost::asio::ip::tcp::socket(*tx_ios) ) {
}

IpSocket::~IpSocket() {
}

void IpSocket::rxData() {
	boost::asio::write(*rx_soc, boost::asio::buffer(data));
}

void IpSocket::txData() {
	boost::asio::read(*tx_soc, boost::asio::buffer(data));
}
