#include <FileLock.h>
#include <iostream>

FILE* FileLock::getLock() {
        opLock = fopen("/tmp/lock-GPIB", "r");
        if(opLock) { // Getting a handle means somebody create that file.
                std::cerr << "Could not get file lock (/tmp/lock-GPIB). Please check that no other instance is running." << std::endl;
                fclose(opLock);
                exit(0);
        } else opLock = fopen("/tmp/lock-GPIB", "w");
        return opLock;
}

void FileLock::releaseLock(FILE *f) {
        fclose(f);
        remove("/tmp/lock-GPIB"); // Deleting the file
}

void FileLock::releaseLock() {
	FileLock::releaseLock(opLock);
	opLock = NULL;
}
