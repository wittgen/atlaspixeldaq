#include <UdpSocket.h>
#include <FileLock.h>
#include <ChannelControl.h>

#include <string>
#include <tr1/unordered_map>

using std::string;
using std::tr1::unordered_map;


#define MINIDCS_PORT 3042

class MiniDCS: public UdpAsyncServer {

	unordered_map<string, ChannelControl> chCtrl;
	int delay;

	std::map<PixGPIBDevice*, int> devNum;
	PixGPIB *boardController;

	FileLock lock;
	boost::mutex gpibMutex;

protected:

	virtual void rxDecode();
	virtual void txEncode();
        virtual void run();
	virtual void runMonitoring();

	virtual void printData();
	virtual void printValues(std::ostream& tee = std::cout);

public:

	MiniDCS();
	virtual ~MiniDCS();

	virtual void setDelay(int d);
	virtual int getDelay();

	virtual void deactivateAll();
	virtual void cleanExit(int signal);
	virtual void resetPower(int signal);

};
