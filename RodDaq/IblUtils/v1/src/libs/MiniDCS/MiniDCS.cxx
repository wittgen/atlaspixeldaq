#include <MiniDCS.h>
#include <iostream>

bool gDebug = false;

MiniDCS::MiniDCS(): UdpAsyncServer(MINIDCS_PORT), boardController(nullptr) {
	lock.getLock();
	// Setting up default pixel-lab channels
	chCtrl["FEI4B-1"].chGPIB = 5;
	chCtrl["FEI4B-1"].devCh = 0;
	chCtrl["FEI4B-1"].aSetting = 0;
	chCtrl["FEI4B-1"].vSetting = 2.0;
	chCtrl["FEI4B-1"].aLimit = 0.700;
	chCtrl["FEI4B-1"].vLimit = 2.1;

	chCtrl["FEI4B-2"] = chCtrl["FEI4B-1"];
	chCtrl["FEI4B-2"].devCh = 1;

	chCtrl["VPin"].chGPIB = 10;
	chCtrl["VPin"].devCh = 0;
	chCtrl["VPin"].aSetting = 0;
	chCtrl["VPin"].vSetting = 10.0;
	chCtrl["VPin"].aLimit = 0.010;
	chCtrl["VPin"].vLimit = 10.0;

	chCtrl["VVDC"].chGPIB = 6;
	chCtrl["VVDC"].devCh = 0;
	chCtrl["VVDC"].aSetting = 0;
	chCtrl["VVDC"].vSetting = 2.400;
	chCtrl["VVDC"].aLimit = 0.800;
	chCtrl["VVDC"].vLimit = 2.500;

	chCtrl["ViSet"].chGPIB = 6;
	chCtrl["ViSet"].devCh = 1;
	chCtrl["ViSet"].aSetting = 0;
	chCtrl["ViSet"].vSetting = 0.700;
	chCtrl["ViSet"].aLimit = 0.010;
	chCtrl["ViSet"].vLimit = 0.900;

	delay = 1;

	unordered_map<std::string, ChannelControl>::iterator it;
        for( it = chCtrl.begin() ; it != chCtrl.end() ; ++it ) 
                strcpy(it->second.chDescr, it->first.c_str());

}

MiniDCS::~MiniDCS() {

}

void MiniDCS::setDelay(int d) {
	delay = d;
}

int MiniDCS::getDelay() {
	return delay;
}
void MiniDCS::rxDecode() {

	//rxData(); // Already done!

	std::cout <<  __PRETTY_FUNCTION__ << ": " << reinterpret_cast<const char*>(&data[0]) << std::endl;

	const char* chName = "# EMPTY #";
	unordered_map<std::string, ChannelControl>::iterator it;
	for( it = chCtrl.begin() ; it != chCtrl.end() ; ++it ) {
		if(strstr(reinterpret_cast<const char*>(&data[0]), it->second.chDescr)) {
			chName = it->second.chDescr;
			break;
		}
	}
	std::cout << "Found channel: " << chName << std::endl;

	const char* actions[] = { "ON", "OFF", "RST", "STATUS" };
	void (ChannelControl::*actions_fptr[])()  = { 
		&ChannelControl::powerOn,
		&ChannelControl::turnOff, // Not cleanPowerOff
		&ChannelControl::resetPower,
        	&ChannelControl::checkDevice};
    	unsigned int status = 2;
    	std::vector<float> vOutVec;
    	std::vector<float> aOutVec;

	if( it == chCtrl.end() ) {
		if(strstr(reinterpret_cast<const char*>(&data[0]), "ALL")) {
			gpibMutex.lock();
			for(size_t i = 0 ; i < sizeof(actions)/sizeof(const char*) ; ++i) {
				if(strstr(reinterpret_cast<const char*>(&data[0]), actions[i])) {
					for( it = chCtrl.begin() ; it != chCtrl.end() ; ++it ) {
						(it->second.*actions_fptr[i])();
						if(strstr(reinterpret_cast<const char*>(&data[0]), "STATUS")) { // ALL STATUS -> save data
						    status = 0;	
						    vOutVec.push_back(it->second.vOut);
						    aOutVec.push_back(it->second.aOut);
						} else {
						    status += it->second.isActive();
						}
					}
				}
			}
			gpibMutex.unlock();
			if (status ==0) {
				// Return reading
				data.clear();
				data.resize(sizeof(float)*(aOutVec.size()+vOutVec.size()));
				for (unsigned int j=0; j<aOutVec.size(); j++) {
					char *vOutBytes = (char*) &vOutVec[j];
					char *aOutBytes = (char*) &aOutVec[j];
					memcpy(&data[(j*2*sizeof(float))],vOutBytes,sizeof(float));
					memcpy(&data[(j*2*sizeof(float))+sizeof(float)], aOutBytes,sizeof(float));
				}
			} else {
				data.resize(1);
				data[0] = (uint8_t) status;
			}
		}

	} else {
		gpibMutex.lock();
		for(size_t i = 0 ; i < sizeof(actions)/sizeof(const char*) ; ++i) {
			if(strstr(reinterpret_cast<const char*>(&data[0]), actions[i])) {
				(it->second.*actions_fptr[i])();
				status = it->second.isActive(); // Check status and return it
			}
		}
		gpibMutex.unlock();
		data.resize(1);
		data[0] = (uint8_t) status; // 1 = On, 0 = Off, 2 = Something wrong
		
	}

	//txEncode();
}

void MiniDCS::txEncode() {

	char doneStr[] = "DONE";
	data.clear();
	for(std::size_t i = 0 ; i < sizeof(doneStr) ; ++i)
		data.push_back(doneStr[i]);

	//txData(); // Already done

}

void MiniDCS::run() {
	if(gDebug) std::cout << __PRETTY_FUNCTION__ << std::endl;

        boardController = new PixGPIB(0);
        if(!boardController) {
                std::cerr << "Error: could not find GPIB controller. Quitting." << std::endl;
                return;
        } else boardController->printDevices();

	unordered_map<std::string, ChannelControl>::iterator it;

        for( it = chCtrl.begin(); it != chCtrl.end(); ++it ) {
                strcpy(it->second.chDescr, it->first.c_str());
                it->second.dev = 0;
                for(int i = 0 ; i < boardController->getNDevices() ; ++i) {
                        PixGPIBDevice *tmpDev = boardController->getDevice(i);
                        if(it == chCtrl.begin()) tmpDev->resetDevice();
                        //std::cout << "Device " << i << ": CH" << tmpDev->getPAD() << std::endl;
                        if(tmpDev->getPAD() == it->second.chGPIB) {
                                it->second.dev = tmpDev;
                                try {
                                        it->second.initDevice();
                                } catch(std::string& e) {
                                        std::cout << "Exception: " << e << std::endl;
                                        cleanExit(1);
                                } catch(...) {
                                        cleanExit(1);
                                }
                                devNum[tmpDev] = i;
                        }
                }
        }

        for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
                if( it->second.dev == 0 ) {
                        std::cout << "No device for " << it->first << ". Exitting." << std::endl;
                        cleanExit(0);
                }
	/*
        for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
                it->second.powerOn();
	*/

	//boost::thread T = boost::thread(boost::bind(&UdpAsyncServer::run, this));
	boost::thread T = boost::thread(boost::bind(&MiniDCS::runMonitoring, this));
	UdpAsyncServer::run();
	//T.join();
	std::cout << "Sever started!" << std::endl;
	
}

void MiniDCS::runMonitoring() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	while(1) {
		gpibMutex.lock();
		unordered_map<std::string, ChannelControl>::iterator it;
		for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
			if(it->second.isActive()) it->second.checkDevice();
		printValues();
		gpibMutex.unlock();
		sleep(delay);
	}

}

void MiniDCS::printData() {
        std::vector<boost::uint8_t>::iterator it;
        std::cout << data.size() << "--> ";
        for( it = data.begin() ; it != data.end() ; ++it )
                std::cout << *it;
        std::cout << std::endl;
}

void MiniDCS::printValues(std::ostream& tee) {
        char buff[200];
        time_t now = time(NULL);
        strftime(buff, 200, "%Y-%m-%d %H:%M:%S", localtime(&now));
        tee << "[" << buff << "]: ";
	unordered_map<std::string, ChannelControl>::iterator it;
        for( it = chCtrl.begin(); it != chCtrl.end(); ++it ) {
                snprintf(buff, 200, "%s (%.4fV, %.4fA); ", it->second.chDescr, it->second.vOut, it->second.aOut);
                tee << buff;
                //tee << setprecision(2) << it->second.chDescr << "(" << it->second.vOut << "V, " << it->second.aOut << "A); ";
                //tee << "[" << buff << "] " << it->second.chDescr << " readout:";
                //tee << setprecision(3) << "\tV" << it->second.devCh << ": " << it->second.vOut << "\tA" << it->second.devCh << ": " << it->second.aOut; //<< "\033[0m";
                //tee << endl;
        }
        tee << endl;
}

void MiniDCS::deactivateAll() {
	unordered_map<std::string, ChannelControl>::iterator it;
        for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
		it->second.active = false;

}

void MiniDCS::cleanExit(int signal) {
	unordered_map<std::string, ChannelControl>::iterator it;
        for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
                it->second.cleanPowerOff();
        if(boardController) {
		boardController->printDevices();
		delete boardController;
		boardController = nullptr;
	}
	lock.releaseLock();
        exit(signal);
}

void MiniDCS::resetPower(int signal) {
	unordered_map<std::string, ChannelControl>::iterator it;
        for( it = chCtrl.begin(); it != chCtrl.end(); ++it )
                it->second.resetPower();
        if(boardController) boardController->printDevices();
}

