#include "UdpSocket.h"
#include <string>

class MiniDCSClient: public UdpClient {

public:
	MiniDCSClient(const char* host, const char* port);
	virtual ~MiniDCSClient();

	virtual std::vector<char> sendCommand(std::string cmd);
	virtual void run();

};
