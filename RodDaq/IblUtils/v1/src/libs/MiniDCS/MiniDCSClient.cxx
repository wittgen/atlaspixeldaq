#include <MiniDCSClient.h>

#include <iostream>
#include <vector>
using std::cout;
using std::endl;
using std::vector;

const size_t max_length = 1024;

#define DEBUG

void pushBack(std::vector<boost::uint8_t>& thisData, const char* charStr, size_t size) {
	for(size_t i = 0 ; i < size ; ++i)
		thisData.push_back(*charStr++);
}

MiniDCSClient::MiniDCSClient(const char* host, const char* port): UdpClient(host, port) {

}

MiniDCSClient::~MiniDCSClient() {

}

std::vector<char> MiniDCSClient::sendCommand(std::string cmd) {
#ifdef DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << "Command: " << cmd << std::endl;
#endif
	std::vector<boost::uint8_t> thisData;
	pushBack(thisData, cmd.c_str(), cmd.size());

	sock->send_to(boost::asio::buffer(thisData, thisData.size()), *iterator);

	boost::array<char, max_length> recv_buf;

	size_t length = sock->receive_from(boost::asio::buffer(recv_buf), sender_endpoint);
#ifdef DEBUG
    std::cout << "Reply: [" << length << "]: " << &recv_buf[0] << std::endl;
#endif
    std::vector<char> reply;
    reply.resize(length);
    memcpy(&reply[0], &recv_buf[0], length);
    return reply;
}

void MiniDCSClient::run() {
	sendCommand("VPin ON");
	sendCommand("VVDC ON");
	sendCommand("FEI4B-1 ON");
}
