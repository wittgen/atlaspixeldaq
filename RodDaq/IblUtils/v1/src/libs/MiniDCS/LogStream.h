#include <teestream.h>
#include <string>
#include <fstream>

struct LogStream {
        std::string file;
        std::ofstream logstream;
        operator std::ostream&() { return logstream; }
        LogStream(const char* initLogFile = "/tmp/log_MiniDCS"): file(initLogFile), logstream(initLogFile, ios::app) {}
        void operator() (const char* newFile) { logstream.close(); file = newFile; logstream.open(newFile, ios::app); }
} logStream;

teestream teeOut(std::cout, logStream); // Uses initial log file
