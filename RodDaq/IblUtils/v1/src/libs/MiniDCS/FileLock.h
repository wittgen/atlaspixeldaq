#include <cstdlib>
#include <cstdio>

class FileLock {
	FILE *opLock; // Ensures that only a single instance of the program is running
public:
	static void releaseLock(FILE *F);
	FILE* getLock();
	void releaseLock();
};
