
#include "boost/asio/io_service.hpp"
#include "boost/asio/ip/tcp.hpp"

using namespace boost::asio::ip;

class IpSocket {
	std::shared_ptr<boost::asio::io_service>		rx_ios, tx_ios;
	std::shared_ptr<boost::asio::ip::tcp::endpoint> 	rx_end, tx_end;
	std::shared_ptr<boost::asio::ip::tcp::acceptor> 	rx_acc, tx_acc;
	std::shared_ptr<boost::asio::ip::tcp::socket> 	rx_soc, tx_soc;

protected:
	size_t msgLength;
	std::vector<uint8_t> data;
public:
	IpSocket(unsigned int port);
	~IpSocket();

	void rxData();
	void txData();

};

