#include <ChannelControl.h>
#include <iostream>

ChannelControl::ChannelControl(): active(true), dev(NULL) {

}

ChannelControl::~ChannelControl() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	cleanPowerOff();
}

void ChannelControl::powerOn() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	active = true;
	if(!dev) return;
	dev->setState(PGD_ON);
	dev->measureVoltages(); // Need to do something for the device to turn on!
}

void ChannelControl::turnOff() {
	std::cout << __PRETTY_FUNCTION__ << "\t" << chDescr << std::endl;
	if(!dev) return;
	dev->setState(PGD_OFF);
	dev->measureVoltages(); // Enfore visual. Todo: fix
}

void ChannelControl::cleanPowerOff() {
	std::cout << __PRETTY_FUNCTION__ << "\t" << chDescr << std::endl;
	active = false;
	if(!dev) return;
	dev->setState(PGD_OFF);
	dev->measureVoltages(); // Enfore visual. Todo: fix
	dev = nullptr;
}

void ChannelControl::resetPower() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	if(!dev) return;
	std::cout << "Soft-reset requested" << std::endl;
	cleanPowerOff();
	powerOn();
}

void ChannelControl::initDevice() {
	if(!dev) return;
	//dev->resetDevice();

	std::cout << chDescr << ": settings = [" << vSetting << "V, " << aSetting << "A];";
	std::cout << " limits = [" << vLimit << "V, " << aLimit << "A]." << std::endl;

/*
	std::cout << chDescr << "\t";
	std::cout << "DEV"<< chGPIB << ", CH" << devCh << std::endl;
	std::cout << "Settings (V/A): " << vSetting << ", " << aSetting << std::endl;
	std::cout << "Limits: (V/A):" << vLimit << ", " << aLimit << std::endl;
*/
	if(vSetting > 0) {
		dev->setVoltage(devCh, vSetting);
	} else if(aSetting > 0) {
		std::cout << "Code to define current setting is not yet implemented." << std::endl;
		//dev->setCurrent(devCh, aSetting);
	}

	dev->setCurrentLimit(devCh, aLimit);
	dev->setVoltageLimit(devCh, vLimit);

	/*// Code for error readback
	char answer[120];
	char command[120];
	sprintf(command, "SYST:ERR?");
	dev->writeDevice(command);
	sleep(1);
	dev->readDevice(answer);
	cerr << "ERROR: " << answer << std::endl;
	*/

	//powerOn();

	//throw std::string("An exception.");

}


void ChannelControl::checkDevice() {
	if(!dev) return;
	dev->measureVoltages();
	dev->measureCurrents();
	
	aOut = dev->getCurrent(devCh);
	vOut = dev->getVoltage(devCh);
	
	//std::cout << chDescr << "(" << "DEV"<< chGPIB << ", CH" << devCh << ") values: " << vOut << ", " << aOut << std::endl;

	if( aOut > aLimit )  {
		cerr << chDescr << ": High current. Shutting down." << endl;
		cleanPowerOff();
	} else if ( vOut > vLimit ) {
		cerr << chDescr << ": High voltage. Shutting down." << endl;
		cleanPowerOff();
	}

}

bool ChannelControl::isActive() {
	return active;
}

