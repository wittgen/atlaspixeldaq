ifeq ($(HAS_GPIB),1)
lcl_srcs := $(wildcard $(subpath)/*.cxx $(subpath)/*.cpp)
$(eval $(call mkLib, $(call inc2lib, $(subpath)), $(lcl_srcs)))
endif
