
#include "PixGPIB.h"
#include <string>

struct ChannelControl {
	char chDescr[100];
	std::size_t chGPIB;
	int devCh;
	float aOut;
	float vOut;
	float aLimit;
	float vLimit;
	float aSetting;
	float vSetting;
	bool active;
	PixGPIBDevice* dev;
	ChannelControl();
	~ChannelControl();
	void powerOn();
	void turnOff();
	void cleanPowerOff();
	void resetPower();
	void initDevice();
	void checkDevice();
	bool isActive();
};
