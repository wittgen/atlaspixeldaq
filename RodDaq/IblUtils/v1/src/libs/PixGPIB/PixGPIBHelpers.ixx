#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iomanip>

using namespace std;

#include "PixGPIB.h"
#include <teestream.h>

struct LogStream {
	std::string file;
	std::ofstream logstream;
	operator std::ostream&() { return logstream; }
	LogStream(const char* initLogFile = "/tmp/log_GPIB-Optoboard"): file(initLogFile), logstream(initLogFile, ios::app) {}
	void operator() (const char* newFile) { logstream.close(); file = newFile; logstream.open(newFile, ios::app); }
} logStream;

teestream tee(std::cout, logStream); // Uses initial log file

FILE *opLock; // Ensures that only a single instance of the program is running

FILE* getLock() {
	FILE *f;
	f = fopen("/tmp/lock-GPIB", "r");
	if(f) { // Getting a handle means somebody create that file.
		cerr << "Could not get file lock (/tmp/lock-GPIB). Please check that no other instance is running." << endl;
		fclose(f);
		exit(0);
	} else f = fopen("/tmp/lock-GPIB", "w");
	return f;
}

void releaseLock(FILE *f) {
	fclose(f);
	remove("/tmp/lock-GPIB"); // Deleting the file
}	
