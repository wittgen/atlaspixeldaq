#ifndef __BOC_TOOLS_H__
#define __BOC_TOOLS_H__

#include <iblBocInt.h>

#include "AbstrRodModule.h"
using SctPixelRod::AbstrRodModule;

uint32_t readBoc(AbstrRodModule* rod0, int regNum);
void writeBoc(AbstrRodModule* rod0, int regNum, uint32_t data);
void scanBocRegisters(AbstrRodModule* rod0);

#endif // __BOC_TOOLS_H__
