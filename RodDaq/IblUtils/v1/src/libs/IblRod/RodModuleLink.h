#ifndef __ROD_MODULE_LINK_H__
#define __ROD_MODULE_LINK_H__

/*
 * Author: Karolos Potamianos (karolos.potamianos@cern.ch)
 * Date: 08-X-2012
 * Description: Absctract class to manage communicaton to the module.
 */

#include "AbstrModuleLink.h"
#include "RodController.h"

class RodModuleLink: public AbstrModuleLink, public RodController {
private:
protected:
public:	
	virtual void prepCommand(uint8_t value);
	virtual void prepCommand16(uint16_t value);
	virtual void sendCommand();
	uint8_t readLink() const;
};

#endif // __ROD_MODULE_LINK_H__
