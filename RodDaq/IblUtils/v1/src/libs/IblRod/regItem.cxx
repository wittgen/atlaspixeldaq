// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 16.08.11
// Version: 0.1

#include "regItem.h"

// Default constructor
regItem::regItem(int address_arg, int subAddress_arg, string name_arg, int mask_arg, int value_arg, bool end_arg) {
    address = address_arg;
    subAddress = subAddress_arg;
    name = name_arg;
    mask = mask_arg;
    value = value_arg;
    defValue = value_arg;
    bigEndian = end_arg;
}

// Deconstrcutor
regItem::~regItem() {
    
}

// Getter functions
int regItem::getAddress() {
    return address;
}

int regItem::getSubAddress() {
    return subAddress;
}

string regItem::getName() {
    return name;
}

int regItem::getMask() {
    return mask;
}

int regItem::getDefValue() {
    return defValue;
}

int regItem::getValue() {
    return value;
}

bool regItem::getEndianness() {
    return bigEndian;
}


// Setter functions
void regItem::setAddress(int address_arg) {
    address = address_arg;
}

void regItem::setSubAddress(int subAddress_arg) {
    subAddress = subAddress_arg;
}

void regItem::setName(string name_arg) {
    name = name_arg;
}

void regItem::setMask(int mask_arg) {
    mask = mask_arg;
}

void regItem::setDefValue(int defValue_arg) {
    defValue = defValue_arg;
}

void regItem::setValue(int value_arg) {
    value = value_arg;
}

void regItem::setEndianness(bool bigEndian_arg) {
    bigEndian = bigEndian_arg;
}

// Reset value to default
void regItem::reset() {
    value = defValue;
}
