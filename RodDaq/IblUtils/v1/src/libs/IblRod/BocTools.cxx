#include "BocTools.h"
#include "RodPrimitive.h"

using namespace SctPixelRod;

#include <iostream>
using namespace std;


void writeBoc(AbstrRodModule* rod0, int regNum, uint32_t data){
  // write to boc register
    BocCtrlIn bocCtlPrim;
    RodPrimitive *sendBocCtlPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
#ifdef VERBOSE
    cout << "Writing BOC register " << regNum << ", data " << data << " via BOC_CTL_PRIMITIVE " << endl;
#endif
    bocCtlPrim.regNum = regNum;
    bocCtlPrim.data = data;
    bocCtlPrim.mode = 0;    // write
    sendBocCtlPrim = new RodPrimitive((sizeof(BocCtrlIn) >> 2), 1, BOC_CTRL, 0, (long int*)&bocCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendBocCtlPrim);
    delete sendBocCtlPrim;
}

uint32_t readBoc(AbstrRodModule* rod0, int regNum){
  // write to boc register
    BocCtrlIn bocCtlPrim;
    RodPrimitive *sendBocCtlPrim;
    RodOutList* outList;
    uint32_t result;
#ifdef VERBOSE
    cout << "Reading BOC register " << regNum << " via BOC_CTL_PRIMITIVE " << endl;
#endif
    bocCtlPrim.regNum = regNum;
    bocCtlPrim.data = 0;    // is void
    bocCtlPrim.mode = 1;    // read
    sendBocCtlPrim = new RodPrimitive((sizeof(BocCtrlIn) >> 2), 1, BOC_CTRL, 0, (long int*)&bocCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendBocCtlPrim, outList);
    result = *(uint32_t*) outList->getMsgBody(1);
    rod0->deleteOutList();
    delete sendBocCtlPrim;
    return result;
}


//#include "AddressMap.h";
void scanBocRegisters(AbstrRodModule* rod0) {
	int i;
	int bocResult;


	//for(int j = 0 ; j < 10 ; ++j) {	
	for (i=0;i<9;i++){
		bocResult = readBoc(rod0, i);
		cout << "Boc register " << i << " read: 0x" << hex << bocResult << dec << endl;
	}
	//}

	for(int i = 0 ; i < 80 ; ++i) {
		bocResult = readBoc(rod0, i);
		cout << "Boc register " << i << " read: 0x" << hex << bocResult << dec << endl;
	}

	return;
	i = BOC_REG_BCF_FW;
	bocResult = readBoc(rod0, i);
	cout << "Boc register 0x" << hex << i << " read: 0x" << hex << bocResult << endl;
	i = BOC_BMF_CTL_REG(BOC_BMFS_ID,BOC_REG_BMF_FW);
	bocResult = readBoc(rod0, i);
	cout << "Boc register 0x" << hex << i << " read: 0x" << hex << bocResult << endl;
	i = BOC_BMF_TX_REG(BOC_BMFN_ID,4,BOC_REG_TX_STATUS);
	bocResult = readBoc(rod0, i);
	cout << "Boc register 0x" << hex << i << " read: 0x" << hex << bocResult << endl;

	return;

	//readBoc(rod, Addr::Bcf::ProgSouthCrc2);
}

