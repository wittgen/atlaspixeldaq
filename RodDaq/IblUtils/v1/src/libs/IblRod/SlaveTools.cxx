#include "SlaveTools.h"

#include <cstdlib>
#include <iostream>
#include <cstring>
#include <iterator>
#include <algorithm>
#include <queue>
#include <unistd.h>

using namespace SctPixelRod;
using namespace std;

void setUart(AbstrRodModule* rod0, int source) {
	RodController::setUart(rod0, source);
}

void setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave) { 
	RodController::setNetCfg(rod0, cfg, slave); 
}

void getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave) { 
	RodController::getNetCfg(rod0, cfg, slave); 
}

void writeSlave(AbstrRodModule* rod0, int slave, int regNum, uint32_t data){
#ifdef __DIRTY_HACK__
    slave = 1;
#endif

  // write to slave register
    SlaveRegAccessIn slaveCtlPrim;
    RodPrimitive *sendSlaveCtlPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));

#ifdef VERBOSE
    cout << "Writing slave register " << regNum << ", data " << data << " via SLAVE_REG_ACCESS " << endl;
#endif
    slaveCtlPrim.regNum = regNum;
    slaveCtlPrim.data = data;
    slaveCtlPrim.mode = SLAVE_REG_WRITE;    // write
    slaveCtlPrim.slaveNum = slave;

    sendSlaveCtlPrim = new RodPrimitive((sizeof(SlaveRegAccessIn) >> 2), 1, SLAVE_REG_ACCESS, 0, (long int*)&slaveCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendSlaveCtlPrim);
    delete sendSlaveCtlPrim;
}

uint32_t readSlave(AbstrRodModule* rod0, int slave, int regNum){
  // read from slave register
    static queue<uint32_t> fifo;
    SlaveRegAccessIn slaveCtlPrim;
    RodPrimitive *sendSlaveCtlPrim;
    RodOutList* outList;
    uint32_t result, res_length;
#ifdef VERBOSE
    cout << "Reading slave register " << regNum  << " via SLAVE_REG_ACCESS " << endl;
#endif

    if(!fifo.size()) { // Local buffer is empty

	    slaveCtlPrim.regNum = regNum;
	    slaveCtlPrim.data = 0;
	    slaveCtlPrim.mode = SLAVE_REG_READ_FIFO;    // read
	    slaveCtlPrim.slaveNum = slave;

	    sendSlaveCtlPrim = new RodPrimitive((sizeof(SlaveRegAccessIn) >> 2), 1, SLAVE_REG_ACCESS, 0, (long int*)&slaveCtlPrim);
	    rod0->executeMasterPrimitiveSync(*sendSlaveCtlPrim, outList);
	    uint32_t* res_ptr = (uint32_t*) outList->getMsgBody(1);
	    res_length = outList->getMsgLength(1);
	    for(uint32_t i = 0 ; i < res_length ; ++i)
		fifo.push(res_ptr[i]);
#ifdef VERBOSE
	    cout << "---> Result: " << hex;
	    copy(res_ptr, res_ptr+res_length, ostream_iterator<uint32_t>(cout," "));
	    cout << dec << endl;
#endif
	    rod0->deleteOutList();
	    delete sendSlaveCtlPrim;
    }
    result = fifo.front(); fifo.pop();
    //printf("%s:%08x\n", __PRETTY_FUNCTION__, result);

    return result;
}


void writeDevice(AbstrRodModule* rod0, int device, int regNum, uint32_t data){
  // write to device register
    DeviceRegAccessIn deviceCtlPrim;
    RodPrimitive *sendDeviceCtlPrim;
    // uint32_t* devPrimBuf = (uint32_t*)malloc(sizeof(SendDevicePrimitiveIn) + sizeof(IblSlvRdWr));

#ifdef VERBOSE
    cout << "Writing device " << device << " register " << regNum << ", data " << data << " via DEVICE_REG_ACCESS " << endl;
#endif
    deviceCtlPrim.regNum = regNum;
    deviceCtlPrim.data = data;
    deviceCtlPrim.mode = 0;    // write
    deviceCtlPrim.deviceNum = device;

    sendDeviceCtlPrim = new RodPrimitive((sizeof(DeviceRegAccessIn) >> 2), 1, DEVICE_REG_ACCESS, 0, (long int*)&deviceCtlPrim);
    rod0->executeMasterPrimitiveSync(*sendDeviceCtlPrim);
    delete sendDeviceCtlPrim;
}



#if 1
uint32_t readDevice(AbstrRodModule* rod0, int device, int regNum){
	static queue<uint32_t> fifo;

	// read from device register
	DeviceRegAccessIn deviceCtlPrim;
	RodPrimitive *sendDeviceCtlPrim;
	RodOutList* outList;
	uint32_t result, res_length;

	if(!fifo.size()) { // Local buffer is empty

#ifdef VERBOSE
		cout << "Reading device " << device << " register " << regNum  << " via DEVICE_REG_ACCESS " << endl;
#endif
		deviceCtlPrim.regNum = regNum;
		deviceCtlPrim.data = 0;
		deviceCtlPrim.mode = 1;    // read
		deviceCtlPrim.deviceNum = device;

		sendDeviceCtlPrim = new RodPrimitive((sizeof(DeviceRegAccessIn) >> 2), 1, DEVICE_REG_ACCESS, 0, (long int*)&deviceCtlPrim);
		rod0->executeMasterPrimitiveSync(*sendDeviceCtlPrim, outList);
		//result = *(uint32_t*) outList->getMsgBody(1);
		//std::cout << "Message length: " << outList->getMsgLength(1) << std::endl;
		uint32_t* res_ptr = (uint32_t*) outList->getMsgBody(1);
		res_length = outList->getMsgLength(1);
		for(uint32_t i = 0 ; i < res_length ; ++i)
			fifo.push(res_ptr[i]);

#ifdef VERBOSE
		cout << "---> Result: " << hex;
		copy(res_ptr, res_ptr+res_length, ostream_iterator<uint32_t>(cout," "));
		cout << dec << endl;
#endif
		rod0->deleteOutList();
		delete sendDeviceCtlPrim;
	}
	result = fifo.front(); fifo.pop();
	return result;
}
#else 
uint32_t readDevice(AbstrRodModule* rod0, int device, int regNum){
	// read from device register
	DeviceRegAccessIn deviceCtlPrim;
	RodPrimitive *sendDeviceCtlPrim;
	RodOutList* outList;
	uint32_t result;
#ifdef VERBOSE
	cout << "Reading device " << device << " register " << regNum  << " via DEVICE_REG_ACCESS " << endl;
#endif
	deviceCtlPrim.regNum = regNum;
	deviceCtlPrim.data = 0;
	deviceCtlPrim.mode = 1;    // read
	deviceCtlPrim.deviceNum = device;

	sendDeviceCtlPrim = new RodPrimitive((sizeof(DeviceRegAccessIn) >> 2), 1, DEVICE_REG_ACCESS, 0, (long int*)&deviceCtlPrim);
	rod0->executeMasterPrimitiveSync(*sendDeviceCtlPrim, outList);
	result = *(uint32_t*) outList->getMsgBody(1);

	rod0->deleteOutList();
	delete sendDeviceCtlPrim;

	return result;
}
#endif


void histTestSlaves(RodController& rodCtrl) {
	std::string SlaveFile;
	if(rodCtrl.getIp()=="127.0.0.1") 
		SlaveFile = "../../Binary/histClient.bin";
	histTestSlaves(rodCtrl.getRod());
}

void histTestSlaves(AbstrRodModule* rod0, std::string SlaveFile) {
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);	// large enough for DPR
	int i;

	int slave;

	for (slave = 0; slave < N_IBL_SLAVES ; slave++){
		//	if(slave>0) break; // Disabling use of slave0
		if(SlaveFile!="") cout << "Using " << SlaveFile << " on slave" << slave << endl;
		getStatus(rod0,SLV_STAT_TYPE_STD,slave);
		//	continue;
		// the next two line generate maximum output from the slave. Comment, if you don't want to see it
		setVerbose(rod0,1,slave);
		setUart(rod0,slave + 1);
		// -----------------------------------------------------
		loadSlave(rod0, SlaveFile, slave);
		getStatus(rod0,SLV_STAT_TYPE_STD,slave);
		setVerbose(rod0,0,slave);
		setId(rod0, slave);
	}
	setUart(rod0,0);

	// test write/read slave 0
	slave = 0;
	cout << endl << "Wr/rd test slave " << slave << endl;
	uint32_t wBuf[10] = {1,2,3,4,0x5aa56cc6,0x12345678};
	uint32_t rBuf[10] = {0};
	sendData(rod0,SLV_CMD_WRITE,0,6,wBuf,slave);
	getData(rod0,SLV_CMD_READ,0,6,0,rBuf,slave);
	for (i=0; i<6;i++)
		cout << "Read data " << i << ": 0x" << hex << rBuf[i] << endl;

	// test write/read slave 1
	slave = 1;
	cout << endl << "Wr/rd test slave " << slave << endl;
	sendData(rod0,SLV_CMD_WRITE,0,6,wBuf,slave);
	getData(rod0,SLV_CMD_READ,4,6,0,rBuf,slave);
	for (i=0; i<6;i++)
		cout << "Read data " << i << ": 0x" << hex << rBuf[i] << endl;

	cout << endl << "Network config slave 0" << endl;
	// configure network
	IblSlvNetCfg netCfgOut, netCfgIn;
	unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x03};
	unsigned char lIp[4] = {192, 168, 1, 11};
	unsigned char maskIp[4] = {255,255, 0,0};
	unsigned char gwIp[4] = {192, 168, 0, 1};
	memcpy(&netCfgOut.localMac,mac,sizeof(mac));
	memcpy(&netCfgOut.localIp,lIp,sizeof(lIp));
	memcpy(&netCfgOut.maskIp,maskIp,sizeof(maskIp));
	memcpy(&netCfgOut.gwIp,gwIp,sizeof(gwIp));

	setUart(rod0, 1);
	setVerbose(rod0,1,0);
	sleep(1);

	setNetCfg(rod0,&netCfgOut,0);
	getNetCfg(rod0,&netCfgIn,0);
	cout << endl << "Network config slave 1" << endl;
	netCfgOut.localMac[7] ++;
	netCfgOut.localIp[3] ++;
	setNetCfg(rod0,&netCfgOut,1);
	getNetCfg(rod0,&netCfgIn,1);


	cout << endl << "Histo config slave 0" << endl;
	// set histogram config
	IblSlvHistCfg histCfgOut, histCfgIn;
	histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
	histCfgOut.cfg[0].enable = 1;
	histCfgOut.cfg[0].type = 1;
	//histCfgOut.cfg[0].colStep = 0;
	histCfgOut.cfg[0].maskStep = 0;
	histCfgOut.cfg[0].chipSel = 0;
	histCfgOut.cfg[0].addrRange = 0;
	histCfgOut.cfg[0].scanId = 0x1234;
	histCfgOut.cfg[0].binId = 1;
	histCfgOut.cfg[0].nTrigs = 60;
	memcpy(&histCfgOut.cfg[1],&histCfgOut.cfg[0],sizeof(IblSlvHistUnitCfg));
	// enable hist0 only
	histCfgOut.cfg[1].enable = 0;

	startHisto(rod0, &histCfgOut, 0);
	getHistCfg(rod0, &histCfgIn, 0);

	cout << endl << "Test histogramming slave 0" << endl;
	// send some histogramm test data
#define TESTSIZE 100
#define HISTSIZE (ROW_SIZE_IMP * COL_SIZE * (1 << CHIP_BITS))
	uint32_t testData [TESTSIZE];
	uint32_t results [HISTSIZE];
	for (i = 0; i< TESTSIZE; i++){
		testData[i] = 0;
	}
	testData[0] = PIXVAL(0, 1, 1, 1); // chip 0, row 1, col 1 => pixel number 0
	testData[1] = PIXVAL(0, 1, 2, 1);
	testData[2] = PIXVAL(0, 1, 3, 1);
	testData[3] = PIXVAL(0, 1, 4, 1);
	testData[4] = PIXVAL(0, 1, 5, 1);
	testData[5] = PIXVAL(0, 1, 6, 1);
	testData[6] = PIXVAL(0, 1, 7, 1);
	testData[7] = PIXVAL(0, 1, 8, 1);
	testData[8] = PIXVAL(0, 1, 9, 1);
	testData[9] = PIXVAL(0, 1,10, 1);

	testData[10] = PIXVAL(0, 2, 1, 2);
	testData[11] = PIXVAL(0, 2, 2, 2);
	testData[12] = PIXVAL(0, 3, 3, 4);
	testData[13] = PIXVAL(0, 3, 4, 4);
	testData[14] = PIXVAL(0, 1,10, 5);    // this pixel is set twice !
	testData[15] = PIXVAL(0, 1,11, 6);
	testData[16] = PIXVAL(0, 1,12, 7);
	testData[17] = PIXVAL(0, 1,13, 8);
	testData[18] = PIXVAL(0,10,14, 9);
	testData[19] = PIXVAL(0,10,15,10);

	sendData(rod0,SLV_CMD_HIST_TEST,0,20,testData,0);
	sendData(rod0,SLV_CMD_HIST_TEST,0,10,testData,0);
	sendData(rod0,SLV_CMD_HIST_TEST,0,5,testData,0);

	IblSlvHistTerm histTermCfg;
	unsigned char targetIp[4] = {192, 168,   1, 9};
	memcpy(&histTermCfg.targetIp, &targetIp, sizeof(targetIp));
	histTermCfg.targetPort = 5002;
	histTermCfg.pcol = 1;
	stopHisto(rod0, &histTermCfg, 0);

	for (i=0;i<20;i++){
		int pixNum;
		pixNum = PIXNUM(testData[i]);   // this is the pixel address
		getData(rod0,SLV_CMD_HIST_READ,pixNum,1,0,&results[pixNum],0);   // read individual pixel results
		printf("Pixel %d result 0x%x: occ %d, tot %d, tot² %d\n", pixNum, results[pixNum], (results[pixNum] >> OCC_RESULT_SHIFT) & ((1 << (OCC_RESULT_BITS)) - 1),
				(results[pixNum] >> TOT_RESULT_SHIFT) & ((1 << (TOT_RESULT_BITS)) - 1), (results[pixNum] >> TOTSQR_RESULT_SHIFT) & ((1 << (TOTSQR_RESULT_BITS)) - 1));
	}
	setVerbose(rod0,0,0);

	setUart(rod0, 0);
	sleep(1);

	// the very end
	free(slvPrimBuf);

}


void getStatus(AbstrRodModule* rod0, int type, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
	// we need an outlist too
	RodOutList* outList;
	iblSlvStat *rodStatus;
#ifdef VERBOSE
	cout << "Sending slave " << slave << " command status via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
	slvCmdPrim->fetchReply = 1;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = SLV_CMD_STAT;
	slvCmd->addr = 0;
	slvCmd->count = 1;
	slvCmd->data = type;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
	rodStatus = (iblSlvStat*) outList->getMsgBody(1);
	cout << "Status: program type     " << hex << rodStatus->progType  << endl;
	cout << "Status: program version  " << hex << rodStatus->progVer  << endl;
	cout << "Status: status value     " << hex << rodStatus->status  << endl;
	rod0->deleteOutList();
	delete sendSlvCmdPrim;
	free(slvPrimBuf);
}

void setVerbose(AbstrRodModule* rod0, int on, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
#ifdef VERBOSE
	cout << "Sending slave " << slave << " command verbose via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
	slvCmdPrim->fetchReply = 0;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = SLV_CMD_VERBOSE;
	slvCmd->addr = 0;
	slvCmd->count = 1;
	slvCmd->data = on;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
	delete sendSlvCmdPrim;
	free(slvPrimBuf);
}

void setId(AbstrRodModule* rod0, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
#ifdef VERBOSE
	cout << "Sending slave " << slave << " command setId via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
	slvCmdPrim->fetchReply = 0;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = SLV_CMD_ID_SET;
	slvCmd->addr = 0;
	slvCmd->count = 1;
	slvCmd->data = slave;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
	delete sendSlvCmdPrim;
	free(slvPrimBuf);
}

void sendData(AbstrRodModule* rod0, int type,  int addr, int nWords, uint32_t *data, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
	int i;
#ifdef VERBOSE
	cout << "Sending slave " << slave << " data, type " << type << " via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	// NB: nWords needs to be adapter to actual data size
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->fetchReply = 0;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = type;
	slvCmd->addr = addr;
	slvCmd->count = nWords;
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2) + slvCmd->count - 1;
	for (i=0; i < nWords; i++){
		((uint32_t*)&slvCmd->data)[i] = data[i];
		//cout << "Addr " << hex << i << ": 0x " << hex << ((uint32_t*)data)[i] << endl;
	}
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2) + slvCmd->count - 1,
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
	delete sendSlvCmdPrim;
	free(slvPrimBuf);

}

void getData(AbstrRodModule* rod0, int type, int addr, int nWords, uint32_t parm, uint32_t *data, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
	// we need an outlist too
	RodOutList* outList;
	unsigned long *outBody;
	int i;
#ifdef VERBOSE
	cout << "Reading slave " << slave << " data, type " << type << " via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
	slvCmdPrim->fetchReply = 1;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = type;
	slvCmd->addr = addr;
	slvCmd->count = nWords;
	slvCmd->data = parm;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
	for (i=0; i < nWords; i++){
		data[i] = ((uint32_t*)outList->getMsgBody(1))[i];
		cout << "Addr " << hex << i << ": 0x " << hex << ((uint32_t*)data)[i] << endl;
	}
	rod0->deleteOutList();
	delete sendSlvCmdPrim;
	free(slvPrimBuf);
}

void startHisto(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	RodPrimitive *sendSlvCmdPrim;
	IblSlvHistCfg *slvHistCmd;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
#ifdef VERBOSE
	cout << "Sending slave " << slave << " command histo config set via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvHistCfg) >> 2);
	slvCmdPrim->fetchReply = 0;
	slvHistCmd = (IblSlvHistCfg*)&slvCmdPrim[1];
	memcpy(slvHistCmd,cfg,sizeof(IblSlvHistCfg));
	slvHistCmd->cmd = SLV_CMD_HIST_CFG_SET;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvHistCfg) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
	delete sendSlvCmdPrim;
}

void stopHisto(AbstrRodModule* rod0, IblSlvHistTerm *cfg, int slave){
	// send a slave primitive
	SendSlavePrimitiveIn *slvCmdPrim;
	RodPrimitive *sendSlvCmdPrim;
	IblSlvHistTerm *slvHistCmd;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
#ifdef VERBOSE
	cout << "Sending slave " << slave << " command histo term via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvHistTerm) >> 2);
	slvCmdPrim->fetchReply = 0;
	slvHistCmd = (IblSlvHistTerm*)&slvCmdPrim[1];
	memcpy(slvHistCmd,cfg,sizeof(IblSlvHistTerm));
	slvHistCmd->cmd = SLV_CMD_HIST_CMPLT;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvHistTerm) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
	delete sendSlvCmdPrim;
}

void getHistCfg(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave){
	// send a slave primitive
	int i;
	SendSlavePrimitiveIn *slvCmdPrim;
	IblSlvRdWr *slvCmd;
	IblSlvHistCfg *slvHistCmd;
	RodPrimitive *sendSlvCmdPrim;
	// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
	uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
	// we need an outlist too
	RodOutList* outList;
	iblSlvStat *rodStatus;
#ifdef VERBOSE
	cout << "Sending slave " << slave <<" command hist config get via SEND_SLAVE_PRIMITIVE " << endl;
#endif
	slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
	slvCmdPrim->slave = slave; // this is a number, not a mask
	slvCmdPrim->primitiveId = 0;        // ignored
	slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
	slvCmdPrim->fetchReply = 1;
	slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
	slvCmd->cmd = SLV_CMD_HIST_CFG_GET;
	sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
			1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
	rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
	slvHistCmd = (IblSlvHistCfg*)outList->getMsgBody(1);
	memcpy(cfg, slvHistCmd, sizeof(IblSlvHistCfg));
	for (i=0; i<2;i++){
		printf("Histo %d config\n",i);
		printf("  nChips %d \n",cfg->cfg[i].nChips);
		printf("  enable %d \n",cfg->cfg[i].enable);
		printf("  type %d \n",cfg->cfg[i].type);
		//printf("  colStep %d \n",cfg->cfg[i].colStep);
		printf("  chipSel %d \n",cfg->cfg[i].chipSel);
		printf("  addrRange %d \n",cfg->cfg[i].addrRange);
		printf("  scanId %d \n",cfg->cfg[i].scanId);
		printf("  binId %d \n",cfg->cfg[i].binId);
		printf("  nTrigs %d \n",cfg->cfg[i].nTrigs);
	}

	rod0->deleteOutList();
	delete sendSlvCmdPrim;
}

// this function uses the slave number starting from 0, as all other functions
// only one slave is loaded per call
void loadSlave(AbstrRodModule* rod0, std::string SlaveFile, int slvId){
#ifdef _GNU_SOURCE
	char* cwd  = get_current_dir_name();
#else
	char* cwd = (char*) malloc(255);
	getcwd(cwd, 255);
#endif
	cout << "CWD is " << cwd << endl;
	free(cwd);


	/*  crc32 is not available on all systems. not needed anyway
	    char sysCmd[1024];
	    sprintf(sysCmd,"crc32 %s\n",SlaveFile.c_str());
	    cout << endl << endl << SlaveFile << " crc: " << endl;
	    system(sysCmd);
	 */ 

	try {
		// Don't attempt loading the slaves unless filename is provided
		if(SlaveFile != "")  rod0->loadAndStartSlaves(SlaveFile,slvId);
		else rod0->startSlave(slvId); // Binary already loaded.
	} catch( RodException& r ) {
		cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2() << std::endl;
		exit(1);
	}

	cout << "SLAVE " << slvId << " LOADED!" << endl;

}

void startSlave(AbstrRodModule* rod0, int slvId){
	loadSlave(rod0, "", slvId);
}


