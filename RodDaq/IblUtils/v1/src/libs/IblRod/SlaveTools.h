#ifndef __SLAVE_TOOLS_H__
#define __SLAVE_TOOLS_H__

#include "RodController.h"

#include <cstdint>
#include "iblSysParams.h"
#include "iblSlaveCmds.h"

// This definition is needed because we don't want to include xparameter.h from rodSlave.hxx
// XPAR_AXI_EPC_0_PRH0_BASEADDR should not be called from user code
#define XPAR_AXI_EPC_0_PRH0_BASEADDR 0x44000000
#include "rodSlave.hxx"

#include "AbstrRodModule.h"
using SctPixelRod::AbstrRodModule;

#include <string>
using std::string;

#define ROW_BITS 9
#define COL_BITS 7
#define CHIP_BITS 3
#define TOT_BITS 4
#define ROW_SHIFT 0
#define COL_SHIFT (ROW_BITS + ROW_SHIFT)
#define CHIP_SHIFT (COL_BITS + COL_SHIFT)
#define TOT_SHIFT (CHIP_BITS + CHIP_SHIFT)
#define ROW_SIZE_IMP 336  // 4 if simulation
#define ROW_SIZE_SIM 4    // 4 if simulation
#define COL_SIZE 80

// results
#define OCC_RESULT_BITS 7
#define OCC_RESULT_SHIFT 0
#define TOT_RESULT_BITS 10 // totWidth + totIterBits;
#define TOT_RESULT_SHIFT 8 //totRamOffs
#define TOTSQR_RESULT_BITS 14 // 2*totWidth + totIterBits;
#define TOTSQR_RESULT_SHIFT 18 //totSqrRamOffs

// conversions
#define PIXVAL(chip, row, col, tot) ((tot << TOT_SHIFT) + (chip << CHIP_SHIFT) + (row << ROW_SHIFT) + (col << COL_SHIFT))
#define PIXCOL(data) ((data >> COL_SHIFT) & ((1 << COL_BITS) - 1))
#define PIXROW(data) ((data >> ROW_SHIFT) & ((1 << ROW_BITS) - 1))
#define PIXCHIP(data) ((data >> CHIP_SHIFT) & ((1 << CHIP_BITS) - 1))
#define PIXNUM(data) ((PIXCHIP(data) * ROW_SIZE_IMP * COL_SIZE) + ((PIXROW(data)-1) * COL_SIZE) + (PIXCOL(data)-1))


void writeSlave(AbstrRodModule* rod0, int slave, int regNum, uint32_t data);
uint32_t readSlave(AbstrRodModule* rod0, int slave, int regNum);
void writeDevice(AbstrRodModule* rod0, int device, int regNum, uint32_t data);
uint32_t readDevice(AbstrRodModule* rod0, int device, int regNum);
void histTestSlaves(RodController &rod);
void histTestSlaves(AbstrRodModule* rod0, std::string SlaveFile = "");
void getStatus(AbstrRodModule* rod0, int type, int slave);
void setVerbose(AbstrRodModule* rod0, int on, int slave);
void setId(AbstrRodModule* rod0, int slave);
void sendData(AbstrRodModule* rod0, int type,  int addr, int nWords, uint32_t *data, int slave);
void getData(AbstrRodModule* rod0, int type, int addr, int nWords, uint32_t parm, uint32_t *data, int slave);
void setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
void getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
void startHisto(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave);
void stopHisto(AbstrRodModule* rod0, IblSlvHistTerm *cfg, int slave);
void setUart(AbstrRodModule* rod0, int source);
void getHistCfg(AbstrRodModule* rod0, IblSlvHistCfg *cfg, int slave);
void loadSlave(AbstrRodModule* rod0, std::string SlaveFile, int slvId);
void startSlave(AbstrRodModule* rod0, int slvId);



#endif //__SLAVE_TOOLS_H__
