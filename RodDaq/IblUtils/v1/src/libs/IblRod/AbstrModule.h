#ifndef __ABSTR_MODULE_H__
#define __ABSTR_MODULE_H__

/*
 * Author: Karolos Potamianos (karolos.potamianos@cern.ch)
 * Date: 08-X-2012
 * Description: Absctract class to manage functionaly associated to a module.
 */

#include <cstdint>

#include <map>
#include <string>
#include <vector>
#include <ostream>

class gReg;

class AbstrModule {
public:
	// No need for a constructor!
	// Nor a destructor!

	virtual void init() = 0;

	virtual bool getRunMode() = 0;
	virtual void setRunMode(bool mode) = 0;// true for run mode, false for config mode

	virtual int getRegister(int address, int subAddress) = 0;
	virtual int getRegister(int address) = 0;
	virtual void setRegister(int address, int subAddress, int value) = 0;

	virtual void printServiceRecordErrors()  = 0;

	virtual void globalReset() = 0;
	virtual void globalPulse(int width) = 0;
	virtual const std::vector<int>& readFrame() const = 0;

	virtual void calTrigger(int) = 0;
	virtual int decodeConfRead(std::vector<int>) = 0;
	virtual void decodeServiceRecord(const std::vector<int>&, unsigned int) = 0;

	// MODULE_ABST fast cmds
	virtual void trigger() = 0;
	virtual void bcr() = 0;
	virtual void ecr() = 0;
	virtual void cal() = 0;

	// FEI4 slow cmds
	virtual void wrRegister(int address, int value) = 0;
	virtual void rdRegister(int address) = 0;
	virtual void wrFrontEnd(unsigned int bitstream[21]) = 0;
	virtual void runMode(bool mode) = 0;
protected:
	gReg *myGReg;
	int chipID;
	int BOCchannel;
	bool runModus;
	std::ofstream *logfile;

	std::map<int, std::string> serviceRecordMap;
	std::vector<int> serviceRecordCount;
	mutable std::vector<int> answer;
private:
};

#endif // __MODULE_ABSTR_H__

