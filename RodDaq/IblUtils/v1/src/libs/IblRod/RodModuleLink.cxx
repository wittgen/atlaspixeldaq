#include "RodModuleLink.h"

#include "SlaveTools.h"

#include "BocTools.h" // TODO: clear

#include <iostream>
#include <iterator>
#include <algorithm>

template <typename T>
void printBuffer(std::vector<T>& buffer, std::ostream &os = std::cout) {
	os << std::hex;
	std::copy( buffer.begin(), buffer.end(), std::ostream_iterator<int>(os, "\t") );
	os << std::dec;
	os << std::endl;
}

void RodModuleLink::prepCommand(uint8_t value) {
	// Always writes one byte
	//std::cout << "Appending 0x" << std::hex << (int) value << std::dec << " to buffer." << std::endl;
	cmdBuffer.push_back(value);
	//std::cout << "Buffer content: ";
	//printBuffer(cmdBuffer);
}

void RodModuleLink::prepCommand16(uint16_t value) {
	cmdBuffer.push_back((value&0xFF00)>>8);
	cmdBuffer.push_back(value&0xFF);
}

#if 0
void WriteReg(AbstrRodModule* rod0, unsigned char reg, unsigned short value)
{
        // WREG = 0x005A0800
        uint32_t spData[2];
        unsigned long data = value;
        spData[0] = WREG | (reg & 0x3F);
        spData[1] = (data << 16) & 0xFFFF0000;
#ifdef VERBOSE
        cout << "WriteReg: 0x" << hex << spData[0] << "\t" << spData[1] << dec << endl;
#endif  
        sendSport(rod0, spData, 64);
}
#endif

void RodModuleLink::sendCommand() {

	std::vector<uint32_t> cBuffer(cmdBuffer.size()/4+1);
	for(size_t i = 0 ; i < cmdBuffer.size() ; ++i)  {
		cBuffer[i/4] |= cmdBuffer[i] << (3-i%4)*8;
		//std::cout << std::hex << (int)cmdBuffer[i] << "<<" << std::dec << (3-i%4)*8 << std::hex << std::endl;
	}
/*
	std::cout << "Transformed: ";
	std::cout << std::hex;
	std::copy(cBuffer.begin(), cBuffer.end(), std::ostream_iterator<uint32_t>(std::cout, "\t"));
	std::cout << std::dec << std::endl;
*/

/* Example case:
	uint8_t a[] = {0x5A, 0x28, 0x07, 0x00};
	uint32_t b = 0x0;
	b |= a[0] << 24;
	b |= a[1] << 16;
	b |= a[2] << 8;
	b |= a[3];
*/
#ifdef VERBOSE
        std::cout << __PRETTY_FUNCTION__ << ": "; printBuffer(cBuffer);
#endif
	sendSport(&cBuffer[0], cBuffer.size()*32);
	cmdBuffer.clear(); // Clearing the command buffer
}

uint8_t RodModuleLink::readLink() const {
	int slaveId = 1; // HARD-CODED
//#define USEBOCFIFO
//#define VERBOSE
#ifdef USEBOCFIFO
	int rx_ch = 0;
        int bocResult;
        int hex_base = 0x8000;
	if(slaveId) hex_base = 0x4000;
	int reg = hex_base | (0x800 + (rx_ch<<5)) + 0x3;
	uint8_t rval = readBoc(rod, reg);
	std::cerr << "BOC Output: 0x" << std::hex << (int)rval << std::dec << std::endl;
	rval = readDevice(rod, slaveId, 0x40) & 0xFF;
	std::cerr << "ROD Output: 0x" << std::hex << (int)rval << std::dec << std::endl;
#else
#ifndef VERBOSE
	uint16_t val = readSlave(rod, slaveId, 0x40);
	//uint16_t val = readDevice(rod, slaveId, 0x40);
	uint8_t rval = val & 0xFF;
	//if( (val & 0xF00) ) std::cerr << "FIFO FLAGS: 0x" << std::hex << ((val&0xF00)>>8) << std::dec << std::endl;
#else
	uint16_t out = readSlave(rod, slaveId, 0x40);
	//uint16_t out = readDevice(rod, slaveId, 0x40);
	std::cout << "FullRead: 0x" << std::hex << out << std::dec << std::endl;
	uint8_t rval = out & 0xFF;
	std::cout << __PRETTY_FUNCTION__ << " output: 0x" << std::hex << (int)rval << std::dec << std::endl;
#endif
#endif
/*
	if(!readQueue.size()) {
		uint32_t rep = readSlave(rod, slaveId, 0x40);
		std::cout << "Queue empty: fetching 0x" << std::hex << rep << std::dec <<std::endl;
		for(int i = 0 ; i < sizeof(uint32_t)/sizeof(uint8_t) ; ++i) {
			uint8_t val = (rep>>(3-i%4)*8) & 0xF;
			std::cout << "Pushing in " << (int)val << std::endl;
			readQueue.push(val);
		}
	}

	std::cout << "Queue size: " << readQueue.size() << std::endl;
	uint8_t rval = readQueue.front();
	std::cout << "Returning 0x" << std::hex << (int)rval << std::dec << std::endl;
	readQueue.pop();
*/
	return rval;
}

