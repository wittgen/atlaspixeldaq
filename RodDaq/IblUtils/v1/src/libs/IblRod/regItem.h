#ifndef REGITEM_H
#define REGITEM_H

#include <string>

// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 16.08.11
// Version: 0.1

// Description:
// Class is a virtual instance of one Register of the FE-I4 module

using namespace std;

class regItem {
public:
    regItem(int address_arg, int subAddress_arg, string name_arg, int mask_arg, int value_arg, bool end_arg);
    ~regItem();
    
    int getAddress();
    int getSubAddress();
    string getName();
    int getMask();
    int getDefValue();
    int getValue();
    bool getEndianness();
    
    void setAddress(int address_arg);
    void setSubAddress(int subAddress_arg);
    void setName(string name_arg);
    void setMask(int mask_arg);
    void setDefValue(int defValue_arg);
    void setValue(int value_arg);
    void setEndianness(bool bigEndian_arg);
    
    void reset();
    
private:
    int address;
    int subAddress;
    string name;
    int mask;
    int defValue;
    int value;
    bool bigEndian;
};

#endif
