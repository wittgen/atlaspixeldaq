// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 17.08.11
// Version: 0.1

#include "pixData.h"

pixData::pixData(int lv1ID_arg, int bcID_arg) {
    lv1ID = lv1ID_arg;
    bcID_arg = bcID_arg;
	std::string name = "PixDataToTMap";
	name += lv1ID;
	ToTmap = new TH2F(name.c_str(), name.c_str(), 80, 0.5, 80.5, 336, 0.5, 336.5);
	name = "PixDataToTHisto";
	name += lv1ID;
	ToThisto = new TH1F(name.c_str(), name.c_str(), 16, 0.5, 16.5);
}

pixData::~pixData() {
    
}

int pixData::getSize() {
    return col.size();
}

int pixData::getLv1ID() {
    return lv1ID;
}

int pixData::getBcID() {
    return bcID;
}

void pixData::addData(int col_arg, int row_arg, int tot1_arg, int tot2_arg) {
    col.push_back(col_arg);
    row.push_back(row_arg);
    tot1.push_back(tot1_arg);
    tot2.push_back(tot2_arg);
	ToTmap->Fill(col_arg, row_arg, tot1_arg);
	ToTmap->Fill(col_arg, row_arg+1, tot2_arg);
	ToThisto->Fill(tot1_arg);
	ToThisto->Fill(tot2_arg);
}

int pixData::getCol(unsigned int i) {
    if (i<col.size()) {
        return col[i];
    }
    return -1;
}

int pixData::getRow(unsigned int i) {
    if (i<row.size()) {
        return row[i];
    }
    return -1;
}

int pixData::getTot1(unsigned int i) {
    if (i<tot1.size()) {
        return tot1[i];
    }
    return -1;
}

int pixData::getTot2(unsigned int i) {
    if (i<tot2.size()) {
        return tot2[i];
    }
    return -1;
}

TH2F* pixData::getToTmap() {
	return ToTmap;
}

TH1F* pixData::getToThisto() {
	return ToThisto;
}




