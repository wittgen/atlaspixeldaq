#ifndef __MODULE_LINK_H__
#define __MODULE_LINK_H__

/*
 * Author: Karolos Potamianos (karolos.potamianos@cern.ch)
 * Date: 08-X-2012
 * Description: Absctract class to manage communicaton to the module.
 */

#include <cstdint>
#include <vector> // Maybe use a list?
#include <queue>

class AbstrModuleLink {
private:
protected:
	typedef std::vector<uint8_t> CommandBuffer; // In case someone needs to change this later.
	CommandBuffer cmdBuffer;
	mutable std::queue<uint32_t> readQueue;
public:	
	virtual void prepCommand(uint8_t value) = 0;
	virtual void prepCommand16(uint16_t value) = 0;
	virtual void sendCommand() = 0;
	virtual uint8_t readLink() const = 0;
};

#endif // __MODULE_LINK_H__
