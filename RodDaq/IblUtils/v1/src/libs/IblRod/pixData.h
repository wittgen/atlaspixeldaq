#ifndef PIXDATA_H
#define PIXDATA_H

#include <vector>
#include "TH2F.h"
#include "TH1F.h"
#include <string>

// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 17.08.11
// Version: 0.1

// Description:
// Class stores Hit data from the FE-I4 pixel module

using namespace std;

class pixData {
public:
    pixData(int, int);
    ~pixData();
    
    int getSize();
    
    int getLv1ID();
    int getBcID();
    
    void addData(int, int, int, int);
    int getCol(unsigned int);
    int getRow(unsigned int);
    int getTot1(unsigned int);
    int getTot2(unsigned int);

	TH2F* getToTmap();
	TH1F* getToThisto();
    
private:
    
    int lv1ID;
    int bcID;
    vector<int> col;
    vector<int> row;
    vector<int> tot1;
    vector<int> tot2;
    
	TH1F *ToThisto;
	TH2F *ToTmap;
};

#endif
