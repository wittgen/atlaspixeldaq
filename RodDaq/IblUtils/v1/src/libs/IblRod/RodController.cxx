#include "RodController.h"

#define VEBOSE_LEVEL 10
#ifndef NO_VME
#include "RCCVmeInterface.h"
#endif
#include "RodModule.h"
#include "NetRodModule.h"
#include "NetVmeInterface.h"

#include <string>
#include <cstring>
#include <cstdlib>
#include <iostream>

using namespace std;
using namespace SctPixelRod;

unsigned long baseAddress; 		      // Base address

RodController::RodController(int argc, char** argv): vme(nullptr), rod(nullptr), rodStatus(nullptr), slot(-2), rodIp(""), rodOwner(true) {
	setupRod(argc, argv);
}

void RodController::cleanUp() {
		if(rodStatus) { delete rodStatus; rodStatus=nullptr; }
		if(rodOwner && rod) { delete rod; rod=nullptr; }
		if(vme) { delete vme; vme=nullptr; }
}


void RodController::setupRod(int argc, char** argv) {
	cleanUp();

	float rodInit = true;
	std::string option;
	if (argc > 1) {
		for (int i=1; i<argc; i++) {
			option = argv[i];
			if (option[0] != '-') break;
			switch (option[1]) {
				case 'n':
					rodInit = false;
					break;
				case 'i': 
					// Case where there is a space between -i and the IP
					if( option[2] == '\0' ) { rodIp = argv[i+1]; i++; }
					else rodIp = option.substr(2);
					break;
				case 'S': 
					sleep( atoi(option.substr(2).c_str()) );
					break;
				case 's': 
					if( option[2] == '\0' ) { slot = atoi(argv[i+1]); i++; }
					else slot = atoi(option.substr(2).c_str());
					rodIp="";
					std::cout << "Slot=" << slot << std::endl;
					break;
				default:
					break;
			}
		}
	}

	if(slot < 0 && getenv("IBL_ROD_IP") && !rodIp.size()) {
		rodIp = getenv("IBL_ROD_IP");
		std::cout << "Got ROD IP from IBL_ROD_IP: " << rodIp << std::endl;
	}

	// Preferred way is IP
	if(rodIp.size() || -2==slot) initNet();
	else initVme(slot);
	if(rodInit) initRod();
	std::cout << "Addresses: 0x" << std::hex << rod << "\t0x" << vme << std::dec << std::endl;
}

RodController::RodController(int s): vme(nullptr), rod(nullptr), rodStatus(nullptr), slot(s), rodIp("") {
	setupRod();
}

RodController::RodController(): vme(nullptr), rod(nullptr), rodStatus(nullptr), slot(-2), rodIp("127.0.0.1") {
	//setupRod();
        if(getenv("IBL_ROD_IP")) {
                rodIp = getenv("IBL_ROD_IP");
                std::cout << "Got ROD IP from IBL_ROD_IP: " << rodIp << std::endl;
	}
	initNet();
	initRod();
}

void RodController::initRod() {
#if VERBOSE_LEVEL >= 3
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
	// initialise RodModule
	try{
		cout << "LED Start init" << endl;
		if(slot>0) {
			rod->initialize(false);
			rodStatus->snapShot(*rod);
		} else rod->initialize(true);

	} catch (VmeException &v) {
		cout << "VmeException creating RodModule." << endl;
		cout << "ErrorClass = " << v.getErrorClass() << endl;
		cout << "ErrorCode = " << v.getErrorCode() << endl;
	} catch (RodException &r) {
		cout << r.getDescriptor() <<", " << r.getData1() << ", " << r.getData2()
			<< '\n';
	} catch (BaseException &e) {
		std::cout << __FUNCTION__
			<< ": Exception when initializing RodModule:\n ";
		e.what(std::cout);
		std::cout << std::endl;
	} catch (...) {
		std::cout << "An exception was generated when initializing the RodModule."
			<< std::endl;
	}

}

void RodController::initVme(int slot) {
#if VERBOSE_LEVEL >= 3
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif

#ifndef NO_VME
	if (slot < 0 ) {
		cout << "Enter slot number (decimal):";
		cin >> slot;
		while ((slot < 1) || (slot > 21)) {
			cout << "Slot number out or range [1:21], re-enter: ";
			cin >> slot;
		}
	}
	baseAddress = slot << 24;
	try {
		vme = new RCCVmeInterface();
	} catch (...) {
		std::cout << "Caught an exception when creating a VME interface." << std::endl;
	}
	try {   
		rod = new RodModule(baseAddress, mapSize, *vme, numSlaves);
		rodStatus = new RodStatus(*rod);
	} catch (RodException &r) {
		std::cout << __FUNCTION__
			<< "ROD exception thrown when creating the RodModule/RodStatus objects"
			<< std::endl
			<< r;
	} catch (VmeException &v) {
		std::cout << __FUNCTION__
			<< ": VME exception thrown when creating the RodModule/RodStatus objects\n "
			<< v
			<< "RodModule not found on VME (wrong slot number or h/w error)!"
			<< std::endl;
	} catch (...) {
		std::cout << __FUNCTION__
			<< ": Some whatever exception occured..."
			<< std::endl;
	}
#else
	std::cout << "VME functionality not added at compile time" << endl;
#endif

}

void RodController::initNet() {
#if VERBOSE_LEVEL >= 3
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
	vme = new NetVmeInterface(rodIp.c_str());
	rod = new NetRodModule(baseAddress, mapSize, *vme, numSlaves);
	rodOwner = true;
}


void RodController::sendSport(AbstrRodModule* rod0, uint32_t* data, int bits) {
	//SEND_SERIAL_STREAM,
	/*typedef struct {
	  uint32_t sportId, nBits, fCapture, nCount;
	  uint32_t dataPtr, maskLo, maskHi;
	  } SendSerialStreamIn;
	 */

	// sportId must be 0
	// if dataPtr == 0, data follows immediately
	SendSerialStreamIn *sportPrim;
	RodPrimitive *sendSportPrim;
	uint32_t *buf;
	int primSize;
	int nWords = (bits + 31)/32;
	primSize = sizeof(SendSerialStreamIn) + nWords * sizeof(uint32_t);
	buf = (uint32_t*)malloc(primSize);
	if (0 == buf){
		cout << "sendSPort: malloc failed " << endl;
		return;
	}


	std::cout << "Sending through serial port:" << std::hex;
	if(data[nWords-1] == 0x0 && nWords > 2 && data[nWords-2] == 0x0) nWords--;
	//std::copy(data,data+nWords,std::ostream_iterator<uint32_t>(std::cout, "\t"));
	for( int i = 0 ; i < nWords ; ++i )
		printf("%08x\t", data[i]);
	std::cout << std::dec;
	for (int i = 0 ; i < nWords ; ++i) {
		int j = 0;
		for (uint32_t z = (1 << (sizeof(uint32_t)*8-1)) ; z != 0 ; z >>= 1) {
			std::cout << (((data[i]&z)==z)?"1":"0");
			if((3-j++%4)==0) std::cout << " ";
		}
	}
	std::cout << std::endl;
/*
	std::cout << "Waiting for you to continue" << std::endl;
	std::cin.get();
*/

	// pointer to primitive
	sportPrim = (SendSerialStreamIn*)buf;
	// copy input data
	memcpy(&sportPrim[1],data, nWords * sizeof(uint32_t));
	// set paramters
	sportPrim->sportId = 0; // must be 0
	sportPrim->nBits = bits;    // bits. nWords is /32
	sportPrim->fCapture = 0;    // if to capture via inmem fifo. not used yet
	sportPrim->nCount = 0;      // repetitions. not used yet
	sportPrim->dataPtr = 0;     // must be 0 for data to follow immediately, like here
	sportPrim->maskLo = 0;      // FE-I4 mask. not used yet
	sportPrim->maskHi = 0;      // FE-I4 mask. not used yet
	// create rod primitive
	sendSportPrim = new RodPrimitive(primSize/sizeof(uint32_t), 1, SEND_SERIAL_STREAM, 0, (long*)sportPrim);
	// execute
	rod0->executeMasterPrimitiveSync(*sendSportPrim);
	delete sendSportPrim;
	free(buf);
}

void RodController::setUart(AbstrRodModule* rod0, int source){
	// set uart source via EXPERT  primitive
	ExpertIn uartPrim;
	RodPrimitive *sendExpertPrim;
#ifdef VERBOSE
	cout << "Sending expert command " << endl;
#endif
	uartPrim.item = UART_SRC;
	uartPrim.arg[0] = source;
	sendExpertPrim = new RodPrimitive((sizeof(ExpertIn) >> 2), 1, EXPERT, 0, (long*)&uartPrim);
	rod0->executeMasterPrimitiveSync(*sendExpertPrim);
	delete sendExpertPrim;
}

void RodController::showStatus() {

	if(slot<0) {
		for (int slave = 0; slave < 2 ; slave++){
			std::cout << "Slave " << slave << " ----------" << std::endl;
			// copied from histTest
			IblSlvNetCfg *slvNetCmd;
			iblSlvStat *rodStatus;
			SendSlavePrimitiveIn *slvCmdPrim;
			IblSlvRdWr *slvCmd;
			RodPrimitive *sendSlvCmdPrim;
			// uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
			uint32_t* slvPrimBuf = (uint32_t*)malloc(2048);       // large enough for DPR
			// we need an outlist too
			RodOutList* outList;
			iblSlvStat *rodStatus2;
			slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
			slvCmdPrim->slave = slave; // this is a number, not a mask
			slvCmdPrim->primitiveId = 0;      // ignored
			slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
			slvCmdPrim->fetchReply = 1;
			slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
			slvCmd->cmd = SLV_CMD_STAT;
			slvCmd->addr = 0;
			slvCmd->count = 1;
			slvCmd->data = SLV_STAT_TYPE_STD;
			sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
					1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
			rod->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
			rodStatus2 = (iblSlvStat*) outList->getMsgBody(1);
			cout << "Status: program type     " << hex << rodStatus2->progType  << endl;
			cout << "Status: program version  " << hex << rodStatus2->progVer  << endl;
			cout << "Status: status value     " << hex << rodStatus2->status  << endl;
			rod->deleteOutList();
			delete sendSlvCmdPrim;
			free(slvPrimBuf);
		}

	} else {

		std::cout << "We've got a live one!" << std::endl;
		std::cout << "Print the status:" << std::endl;
		std::cout << *rodStatus;
		std::vector<unsigned int> buffer;
		unsigned long * buffer1 = new unsigned long[sizeof(CommRegs)/sizeof(int)];
		CommRegs  master_CommRegs;
		size_t i;
		try {
			rod->mdspBlockRead((unsigned long)MAGIC_MASTER_LOCATION,buffer1,sizeof(master_CommRegs)/sizeof(int));
			for(i=0;i<sizeof(CommRegs)/sizeof(int);i++)
				((unsigned int*) &master_CommRegs)[i]=buffer1[i];
			//for(i=0;i<4;i++) {
			//  rod->readSlaveMem(i,(int) MAGIC_SLAVE_LOCATION,sizeof(CommRegs)/sizeof(int),buffer);  
			//  for(j=0;j<sizeof(CommRegs)/sizeof(int);j++)
			//    ((unsigned int*) &slave_CommRegs[i])[j]=buffer[j];
			//}
		}
		catch (HpiException &h) {
			cout << h;
		}
		char *s;
		cout << "==================== MASTER VERSION ======================" <<endl;
		s=(char *)&master_CommRegs.scratch[0];
		cout << "Image MD5 sum: ";
		for(i=0;i<32;i++) {
			cout << s[i];
		}
		cout <<endl;
		cout << "Link Time: ";
		s=(char *)&master_CommRegs.scratch[8];
		for(i=0;i<12;i++) {
			cout << s[i];
		}
		cout <<endl;
		cout << "CVS Tag: ";
		s=(char *)&master_CommRegs.scratch[11];
		i=0;
		while (s[i] && (i <64) ) {
			cout << s[i];
			i++;
		}
		cout << endl;

#if 0
		// This for PIXEL ROD

		CommRegs slave_CommRegs[4];
		size_t j;
		for(j=0;j<4;j++) {
			cout << "=============== SLAVE Number " << j << ": " << " Version =================" << endl;
			s=(char *) &slave_CommRegs[j].scratch[0];
			cout << "Image MD5 sum: ";
			for(i=0;i<32;i++) {
				cout << s[i];
			} 
			cout <<endl;
			cout << "Link Time: " ;
			s=(char *)&slave_CommRegs[j].scratch[8];
			for(i=0;i<12;i++) {
				cout << s[i];
			} 
			cout <<endl;
			cout << "CVS TAG: ";
			s=(char *)&slave_CommRegs[j].scratch[11];
			i=0;
			while (s[i] && (i <64) ) {
				cout << s[i];
				i++;
			} 
			cout <<endl;
		}


#endif
	}

}


void RodController::getTextBuffer() {

	if(!rod){ cout << "No ROD" << endl; return; }

	// Wait for ROD to begin executing and then wait for it to finish executing
	// Check for error messages in text buffer and read them out if they exist.
	// Note: this is NOT how primHandler and textHandler will be used once we
	// go to threads.  This is for debugging the code only.
	std::string infobuf("");
	std::string errorbuf("");

	RodPrimList::PrimState returnPState;
#if 0
	do {
		try {
			returnPState = rod->primHandler();
		}
		catch (VmeException &v) {
			cout << "VmeException in first primHandler call." << endl;
			cout << "ErrorClass = " << v.getErrorClass() << endl;
			cout << "ErrorCode = " << v.getErrorCode() << endl;
		}
		cout << "PRIMSTATE: " << returnPState << endl;
	} while (returnPState != RodPrimList::EXECUTING);
#endif
	rod->getTextBuffer(infobuf,0,true);
	rod->getTextBuffer(errorbuf,1,true);
	cout << "INFO: " << infobuf << endl << endl;
	cout << "ERROR: " << errorbuf << endl << endl;
	cout << string(80,'-') << endl;
	do {  
		try {
			returnPState = rod->primHandler();
		}
		catch (RodException *r) {
			cout << r->getDescriptor() <<", " << r->getData1() << ", " << r->getData2()
				<< '\n';
		}
		catch (VmeException &v) {
			cout << "VmeException in second primHandler call." << endl;
			cout << "ErrorClass = " << v.getErrorClass() << endl;
			cout << "ErrorCode = " << v.getErrorCode() << endl;
		}
		cout << "PRIMSTATE: " << returnPState << endl;
		rod->getTextBuffer(infobuf,0,true);
		rod->getTextBuffer(errorbuf,1,true);
		cout << "INFO: " << infobuf << endl << endl;
		cout << "ERROR: " << errorbuf << endl << endl;
	} while ((returnPState != RodPrimList::WAITING)&&
			(returnPState != RodPrimList::IDLE));

	////////////////////////////////////////////////////////////////////////////////////////////

}

void RodController::setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave){
    // send a slave primitive
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    RodPrimitive *sendSlvCmdPrim;
    IblSlvNetCfg *slvNetCmd;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
#ifdef VERBOSE
    cout << "Sending slave command net config set via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;        // ignored
    slvCmdPrim->nWords = (sizeof(IblSlvNetCfg) >> 2);
    slvCmdPrim->fetchReply = 0;
    slvNetCmd = (IblSlvNetCfg*)&slvCmdPrim[1];
    memcpy(slvNetCmd,cfg,sizeof(IblSlvNetCfg));
    slvNetCmd->cmd = SLV_CMD_NET_CFG_SET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvNetCfg) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim);
    delete sendSlvCmdPrim;
}


void RodController::getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave){
    // send a slave primitive
    int i;
    SendSlavePrimitiveIn *slvCmdPrim;
    IblSlvRdWr *slvCmd;
    IblSlvNetCfg *slvNetCmd;
    RodPrimitive *sendSlvCmdPrim;
    // uint32_t* slvPrimBuf = (uint32_t*)malloc(sizeof(SendSlavePrimitiveIn) + sizeof(IblSlvRdWr));
    uint32_t* slvPrimBuf = (uint32_t*)malloc(2048); // large enough for DPR
    // we need an outlist too
    RodOutList* outList;
    iblSlvStat *rodStatus;
#ifdef VERBOSE
    cout << "Sending slave " << slave <<" command net config get via SEND_SLAVE_PRIMITIVE " << endl;
#endif
    slvCmdPrim = (SendSlavePrimitiveIn*)slvPrimBuf;
    slvCmdPrim->slave = slave; // this is a number, not a mask
    slvCmdPrim->primitiveId = 0;        // ignored
    slvCmdPrim->nWords = (sizeof(IblSlvRdWr) >> 2);
    slvCmdPrim->fetchReply = 1;
    slvCmd = (IblSlvRdWr*)&slvCmdPrim[1];
    slvCmd->cmd = SLV_CMD_NET_CFG_GET;
    sendSlvCmdPrim = new RodPrimitive((sizeof(SendSlavePrimitiveIn) >> 2) + (sizeof(IblSlvRdWr) >> 2),
                          1, SEND_SLAVE_PRIMITIVE, 0, (long int*)slvCmdPrim);
    rod0->executeMasterPrimitiveSync(*sendSlvCmdPrim, outList);
    slvNetCmd = (IblSlvNetCfg*)outList->getMsgBody(1);
    memcpy(cfg, slvNetCmd, sizeof(IblSlvNetCfg));
    printf("Local MAC :  ");
    for (i=0;i<5;i++) printf("%x:", (unsigned char)cfg->localMac[2 + i]);
    printf("%x\n", (unsigned char)cfg->localMac[2 + i]);
    printf("Local IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->localIp[i]);
    printf("%d\n", (unsigned char)cfg->localIp[i]);
    printf("Mask IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->maskIp[i]);
    printf("%d\n", (unsigned char)cfg->maskIp[i]);
    printf("GW IP :  ");
    for (i=0;i<3;i++) printf("%d.", (unsigned char)cfg->gwIp[i]);
    printf("%d\n", (unsigned char)cfg->gwIp[i]);

    rod0->deleteOutList();
    delete sendSlvCmdPrim;
}




