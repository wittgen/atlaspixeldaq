#ifndef __ROD_CONTROLLER_H__
#define __ROD_CONTROLLER_H__

#include "iblSlaveCmds.h"

#include "VmeInterface.h"
using SctPixelRod::VmeInterface;
#include "AbstrRodModule.h"
using SctPixelRod::AbstrRodModule;
#include "RodStatus.h"
using SctPixelRod::RodStatus;

#include <string>
using std::string;

const unsigned long mapSize=0xc00040;         // Map size
const long numSlaves=2;                       // Number of slaves

class RodController {
private: // Not really needed: keeping for clarity
protected:

	VmeInterface *vme;
	AbstrRodModule *rod;
	RodStatus *rodStatus;
	int slot;
	std::string rodIp;
	bool rodOwner;		// Whether the Rod is owned or not by the controller (default true, but not in case of assignRod)

	void initVme(int slot = -1);
	void initNet();
	void initRod();

public:

	RodController(); // Default for IP operation
	RodController(int argc, char** argv); // The preferred way
	RodController(int slot);
	RodController(AbstrRodModule *rod); // Connects to an existing ROD. WARNING: the destructor will destroy the object!!!!
	~RodController() {
		cleanUp();
	}

	//AbstrRodModule* operator AbstrRodModule*(RodController) { return vme; }
	VmeInterface* getVme() const { return vme; }
	AbstrRodModule* getRod() const { return rod; }
	const std::string& getIp() const { return rodIp; }

	// Attach to already initialized ROD
	void assignRod(AbstrRodModule* r) { rod = r; rodOwner = false; }

	void cleanUp();
	void setupRod(int argc = 0, char** argv = nullptr);

	void showStatus(); // From GetRodStatus
	void getTextBuffer();

	// Dirty hack but maintains compatibility with other code (from test dir)
	static void sendSport(AbstrRodModule* rod0, uint32_t* data, int bits);
	static void setUart(AbstrRodModule* rod0, int source);
	static void setNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
	static void getNetCfg(AbstrRodModule* rod0, IblSlvNetCfg *cfg, int slave);
	void sendSport(uint32_t* data, int bits) { sendSport(getRod(), data, bits); }
	void setUart(int source) { setUart(getRod(), source); }
	void setNetCfg(IblSlvNetCfg *cfg, int slave) { setNetCfg(getRod(), cfg, slave); }
	void getNetCfg(IblSlvNetCfg *cfg, int slave) { getNetCfg(getRod(), cfg, slave); }
};


#endif // __ROD_CONTROLLER_H__
