thepath := $(patsubst %/modules.mk, %, $(word $(words $(MAKEFILE_LIST)),$(MAKEFILE_LIST)))
lcl_srcs := $(wildcard $(thepath)/*.c*)
$(foreach F, $(lcl_srcs), $(eval $(call mkLib, $(call inc2lib, $(call src2hdr, $(F))), $(F))))
