#ifndef GREG_H
#define GREG_H

#include "regItem.h"

#include <string>
#include <vector>
#include <iostream>

// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 16.08.11
// Version: 0.1

using namespace std;

class gReg {
public:
    gReg();
    ~gReg();

    int getSize();
    
    int getGRegValue(int address);
    void setGRegValue(int address, int value);
    
    int getRegValue(int address, int subAddress);
    int getRegValue(string name);
    
    void setRegValue(int address, int subAddress, int value);
    void setRegValue(string name, int value);
    
    void reset();

    void printConf();
    
    void setVerboseMode(bool mode = true) { gVerboseMode = mode; }
protected:
    bool gVerboseMode;

private:
    vector<regItem*> FEmem;
    
    void addItem(int address, int subAddress, string name, int mask, int value, bool endianness);

    
};

#endif
