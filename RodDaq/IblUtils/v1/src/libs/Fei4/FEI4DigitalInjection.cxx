#include "FEI4Tools.h"
#include "FEI4DigitalInjection.h"

#include "MiniDCSClient.h"

#include "RodController.h"
#include "BocTools.h"
#include "SlaveTools.h"

#include <boost/lexical_cast.hpp>

#include <TFile.h>

#include <iostream>
#include <cstdlib>
using namespace std;
using namespace SctPixelRod;

#include <FEI4.h>
#include <TH2.h>
#include <TCanvas.h>

TH2F* digitalInjection(FEI4& fei4) {
	TH2F* hist = new TH2F("ToTMap", "ToTMap", 80, 0.5, 80.5, 336, 0.5, 336.5);
	digitalInjection(fei4, hist);
	return hist;
}

void digitalInjection(FEI4& fei4, TH2F* hist)
{	

	const int trig_delay = 2;
	const int trig_count = 5;

        AbstrRodModule* rod0 = fei4.getRod();
        int slave = 0;
	slave = 1;
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	if (!hist) {
		std::cout << "Error: No histogram provided for saving output." << std::endl;
		return;
	}

        // resetting the INMEM FIFO
        cout << "Resetting the INMEM FIFO" << endl;
        writeSlave(rod0, slave, 0x41, 0x00000001);

	fei4.setRunMode(false);

#ifdef FEI4B
	//fei4.setRegister();
#else
	// Cal pulse width and delay
	fei4.setRegister(26, 0, 0x00008); // Reg: CNT
	fei4.setRegister(27, 11, 0x0);    // Reg: SR_CLOCK
	fei4.setRegister(26, 2, 0x1);     // Reg: HitDiscCnfg

	// Trigger 
	fei4.setRegister(2, 0, trig_count);        // Reg: Trigger_Count

	// Really high discriminator threshold
	//fei4.setRegister(7, 1, 255);

	// maximal Trigger Latency
	// fei4.setRegister(25, 0, 167);     // Reg: LatCnfg
	fei4.setRegister(25, 0, 255 - 8 - trig_delay*8 - (trig_count/2));     // Reg: LatCnfg
#endif


	/*
	   ProgressBar myBar(39);
	   myBar.resize(39);
	 */

	unsigned int pixelMask[21] = { 0 };
	for(int i = 0 ; i < 21 ; ++i) {
		pixelMask[i] = 0x0;
		//pixelMask[i] = 0xF0F0F0F0;
		//if(i == 0) pixelMask[i] = 0xFFFFFFFF;
		//if(i < 10) pixelMask[i] = 0xFFFFFFFF;
		//else if( i == 10) pixelMask[i] = 0x0000FFFF;
		//if( i>10 ) pixelMask[i] = 0x0F0F0F0F;
		//if( i == 10) pixelMask[i] = 0x0f0ff0f0;
		//if( i<10 ) pixelMask[i] = 0xf0f0f0f0;
		//if (0==i) {
		//	pixelMask[i] = 0x55555555;	
		//} else {
		//	pixelMask[i] = 0x0;
		//}
	}

	// fei4.ecr(); // Event Couter Reset
	int startCol = 0;
	int maxCol = 1;
	//startCol = 10;
	//maxCol = 15;
	for (int colAddr = startCol ; colAddr < maxCol ; colAddr++) {
		fei4.setRunMode(false);        

		// Cal pulse width and delay
		//fei4.setRegister(26, 0, (colAddr/3)+1);
		fei4.setRegister(26, 0, 10);
		//fei4.setRegister(26, 0, colAddr+1);
		// SR ALL 1
		// Select Double Column Mode, 0x11 = all
		fei4.setRegister(22, 1, 0x00);    // Reg: Colpr_Mode
		// Column Address
		fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr

		// Select S0
		fei4.setRegister(13, 1, 0x0);     // Reg: S0

		if(pixelMask[0] == 0) {
/*
		// Following table 32 of FEI4B manual, page 87
		fei4.setRegister(13, 0, 0x0);     // Reg: S1
		fei4.setRegister(13, 1, 0x0);     // Reg: S0
		fei4.setRegister(21, 1, 0x0);     // Reg: HLD
		fei4.setRegister(13, 2, 0x1);     // Reg: PxStrobes (0)
		fei4.setRegister(27,10, 0x1);     // Reg: LatchEn
*/
		// Select S0
		fei4.setRegister(13, 1, 0x1);     // Reg: S0
		// Sr Clock
		fei4.setRegister(27,11, 0x1);     // Reg: SR_CLOCK
		// Global pulse
		fei4.globalPulse(1);
		// Clear S0
		fei4.setRegister(13, 1, 0x0);     // Reg: S0
		// Clear Sr Clock
		fei4.setRegister(27,11, 0x0);     // Reg: SR_CLOCK

/*
		// Following table 32 of FEI4B manual, page 87
		fei4.setRegister(13, 2, 0x0);	// Reg: PixelStrobes
		fei4.setRegister(21, 3, 0x1);	// Reg: DIGHITIN_SEL

		fei4.setRegister(26, 0, 0x0);	// CMDcnt(12:0)
		fei4.setRegister(27, 12, 0x0);	// CMDcnt(13)
*/

		// Pixel Reg Output en
		// Select Double Column Mode, 0x11 = all
		fei4.setRegister(22, 1, 0x00);    // Reg: Colpr_Mode
		// Column Address
		fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr

		} else {
//#define WITH_SR_READ
#ifdef WITH_SR_READ
		  // resetting the INMEM FIFO
		  fei4.setRegister(27, 14, 0x00); // SRRead
		  //for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = 0x66666666;
		  for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = 0xFFFFFFFF;
		  for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = i;
		  fei4.wrFrontEnd(pixelMask);
		  
		  cout << "Resetting the INMEM FIFO" << endl;
		  writeSlave(rod0, slave, 0x41, 0x00000001);

		  fei4.setRegister(27, 14, 0x01); // SRRead
		  //for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = 0x66666666;
		  //for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = 0xFFFFFFFF;
		  for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = i;
		  std::cout << "Do something!" << std::endl;
		  std::cin.get();
#endif
		  fei4.wrFrontEnd(pixelMask);
	
#ifdef WITH_SR_READ
		  for(int k = 0 ; k < 6 ; ++k) fei4.readFrame();
		  
		  fei4.setRegister(27, 14, 0x00); // SRRead
		  //return;
#endif		  

		}


		// Set Pixel strobes
		fei4.setRegister(13, 2, 0x2801);  // Reg: PixelStrobes (no HitOr)
		// Set LatchEn
		fei4.setRegister(27,10, 0x1);     // Reg: LatchEn
		// Global pulse
		fei4.globalPulse(10);
		// Deselect LatchEn
		fei4.setRegister(27,10, 0x0);     // Reg: LatchEn

		// Select S0
		fei4.wrRegister(13, 0x0);     // Reg: S0


/*
		// Deselect Pixel strobes
		fei4.setRegister(15, 2, 0x0);     // Reg: <nothing>
*/

		// resetting the INMEM FIFO
		cout << "Resetting the INMEM FIFO" << endl;
		writeSlave(rod0, slave, 0x41, 0x00000001);

		fei4.setRunMode(true);
		//while(1) {
		fei4.calTrigger(trig_delay);

		for(int i = 0 ; i < trig_count ; ++i) {
			usleep(100);
			pixData *pd = decodePixData(fei4, fei4.readFrame());
			if (pd) hist->Add(pd->getToTmap());
		}

		//}
		//myBar.update(colAddr);
	}

#ifdef ROOT_VERSION_CODE
	TCanvas *canv = new TCanvas("cToTMap", "ToTMap", 800, 600);
	///*
	   string TitleName = "ToT map                  CalPulseLength = 8 (";
	   TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 0));
	   TitleName += ")   HitDiscCnf = 1 (";
	   TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 2));
	   TitleName += ")";
	   hist->SetTitle(TitleName.c_str());
	 //*/
	hist->SetXTitle("column");
	hist->SetYTitle("row");

	TFile f("hist.root", "recreate");
	hist->Write("", TObject::kOverwrite);
	f.Close();

	hist->Draw("Colz");
	canv->Print("ToTmap.pdf");
#endif	
}

