#include "FEI4Tools.h"
#include "MiniDCSClient.h"

#include "RodController.h"
#include "BocTools.h"
#include "SlaveTools.h"

#include <boost/lexical_cast.hpp>

#include <TFile.h>

#include <iostream>
#include <cstdlib>

#include <TCanvas.h>
#include <TH2.h>
//#include <TPave.h>
#include <TStyle.h>

using namespace std;
using namespace SctPixelRod;

void sendSport(AbstrRodModule* rod0, uint32_t* data, int bits){
	RodController::sendSport(rod0, data, bits);
}

unsigned int FeI4_reg[35];
unsigned int FeI4_addr[35];
unsigned short int inmem_reg[8];

struct FEI4ToolsInit__ {

	FEI4ToolsInit__() {

		for(int i = 0; i <= 34; i++)
		{
			FeI4_addr[i] = i;
		}

		// setting FeI4A register values for the global registers
		FeI4_reg[FeI4_addr[2]] =  0x1800;
		FeI4_reg[FeI4_addr[3]] =  0xFFFF;
		FeI4_reg[FeI4_addr[4]] =  0xFFFF;
		FeI4_reg[FeI4_addr[5]] =  0xD4FF;
		FeI4_reg[FeI4_addr[6]] =  0x7CD4;
		FeI4_reg[FeI4_addr[7]] =  0xFF58;
		FeI4_reg[FeI4_addr[8]] =  0xF258;
		FeI4_reg[FeI4_addr[9]] =  0x00AA;
		FeI4_reg[FeI4_addr[10]] = 0x4CB0;
		FeI4_reg[FeI4_addr[11]] = 0x5B50;
		FeI4_reg[FeI4_addr[12]] = 0x2800;
		FeI4_reg[FeI4_addr[13]] = 0x0000;
		FeI4_reg[FeI4_addr[14]] = 0xABED;
		FeI4_reg[FeI4_addr[15]] = 0x1A69;
		FeI4_reg[FeI4_addr[16]] = 0x0038;
		FeI4_reg[FeI4_addr[17]] = 0x00AB;
		FeI4_reg[FeI4_addr[18]] = 0x00FF;
		FeI4_reg[FeI4_addr[19]] = 0x0C00;
		FeI4_reg[FeI4_addr[20]] = 0x00E2;
		FeI4_reg[FeI4_addr[21]] = 0x0050;
		FeI4_reg[FeI4_addr[22]] = 0x00F0;
		FeI4_reg[FeI4_addr[23]] = 0x0000;
		FeI4_reg[FeI4_addr[24]] = 0x0000;
		FeI4_reg[FeI4_addr[25]] = 0xD200;
		FeI4_reg[FeI4_addr[26]] = 0x0058;
		FeI4_reg[FeI4_addr[27]] = 0x8000;
		FeI4_reg[FeI4_addr[28]] = 0x8206;
		FeI4_reg[FeI4_addr[29]] = 0x0007;
		// register 30 is missing
		FeI4_reg[FeI4_addr[31]] = 0xF640;
		FeI4_reg[FeI4_addr[32]] = 0x0000;
		FeI4_reg[FeI4_addr[33]] = 0x0000;
		FeI4_reg[FeI4_addr[34]] = 0x000D;
	}
} _fei4ToolsInit;

void enterConfigMode(AbstrRodModule* rod0)
{
	uint32_t spData[2];
	spData[0] = CONFMODE;
	spData[1] = 0;
	sendSport(rod0, spData, 64);
}

void enterRunMode(AbstrRodModule* rod0)
{
	uint32_t spData[2];
	spData[0] = RUNMODE;
	spData[1] = 0;
	sendSport(rod0, spData, 64);
}

void WriteReg(AbstrRodModule* rod0, unsigned char reg, unsigned short value)
{
	// WREG = 0x005A0800
	uint32_t spData[2];
	unsigned long data = value;
	spData[0] = WREG | (reg & 0x3F);
	spData[1] = (data << 16) & 0xFFFF0000;
#ifdef VERBOSE
	cout << "WriteReg: 0x" << hex << spData[0] << "\t" << spData[1] << dec << endl;
#endif
	sendSport(rod0, spData, 64);
}

void ReadRegister(AbstrRodModule* rod0, unsigned char reg)
{
	uint32_t spData_read[2];
	spData_read[0] = READREG | (reg & 0x3F);
	spData_read[1] = 0;
	sendSport(rod0, spData_read, 64);
}

void sendL1A(AbstrRodModule* rod0)
{
	uint32_t spData_LV1[2];
	spData_LV1[0] = LV1;
	spData_LV1[1] = 0;
	sendSport(rod0,spData_LV1,64);
}

void setSlaveReadBack(AbstrRodModule* rod0, int slave, int channel) {
	int bocResult;
	int data_readback;

	int tx_ch = channel;
	int rx_ch = channel-1;
	int V5mask = 1 << tx_ch;

	cout << "Virtex5 mask: 0x" << hex << V5mask << endl;

        cout << "Setting on slave " << slave << " and reading back: linki_enabled = 0x0003" << endl;
        //writeSlave(rod0, slave, 0x0, 0x00000003)
	writeDevice(rod0, slave, 0x0, 0x6);
        bocResult = readSlave(rod0, slave, 0x0);
        cout << "Slave " << slave << " register 0x" << hex << 0x0 << " read: 0x" << hex << bocResult << endl;

	//writeDevice(rod0, 5, 0xC+(slave<<1), 0x00000003);
	writeDevice(rod0, 5, 0xC+(slave<<1), V5mask);
	cout << "Writing Virtex5 register 0x" << hex << 0xC+(slave<<1) << " to 0x" << V5mask << dec << endl;

        // setting up TX control register on BOC BMF-N for channel
        cout << "Setting up TX control register on BOC-BMF-North for channel " << tx_ch << endl;
        //writeBoc(rod0, 0x8C00+(channel<<5), 0x01);
        writeBoc(rod0, 0x8C00+(tx_ch<<5), 0x01); // Enabling BPM
        bocResult = readBoc(rod0, 0x8C00+(tx_ch<<5));
        cout << "Boc register 0x" << hex << 0x8C00+(tx_ch<<5) << " read: 0x" << hex << bocResult << endl;
        // setting up TX control register on BOC BMF-S for channel
        cout << "Setting up TX control register on BOC-BMF-South for channel " << tx_ch << endl;
        //writeBoc(rod0, 0x8C00+(channel<<5), 0x01);
        writeBoc(rod0, 0x4C00+(tx_ch<<5), 0x01); // Enabling BPM
        bocResult = readBoc(rod0, 0x4C00+(tx_ch<<5));
        cout << "Boc register 0x" << hex << 0x4C00+(tx_ch<<5) << " read: 0x" << hex << bocResult << endl;


        // setting up RX control register on BOC BMF-N for channel
        cout << "Setting up RX control register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, 0x8800+(rx_ch<<5), 0x03); // North BMF
        bocResult = readBoc(rod0, 0x8800+(rx_ch<<5));
        cout << "Boc register 0x" << hex << 0x8800+(rx_ch<<5) << " read: 0x" << hex << bocResult << endl;
        // setting up RX control register on BOC BMF-S for channel
        cout << "Setting up RX control register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, 0x4800+(rx_ch<<5), 0x03); // North BMF
        bocResult = readBoc(rod0, 0x4800+(rx_ch<<5));
        cout << "Boc register 0x" << hex << 0x4800+(rx_ch<<5) << " read: 0x" << hex << bocResult << endl;


        // reset the RX status register on BOC BMF-N for channel
        cout << "Setting up RX status register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, 0x8801+(rx_ch<<5), 0xFF);
        bocResult = readBoc(rod0, 0x8801+(rx_ch<<5));
        cout << "Boc register 0x" << hex << 0x8801+(rx_ch<<5) << " read: 0x" << hex << bocResult << endl;
        // reset the RX status register on BOC BMF-S for channel
        cout << "Setting up RX status register on BOC for channel " << rx_ch << endl;
        writeBoc(rod0, 0x4801+(rx_ch<<5), 0xFF);
        bocResult = readBoc(rod0, 0x4801+(rx_ch<<5));
        cout << "Boc register 0x" << hex << 0x4801+(rx_ch<<5) << " read: 0x" << hex << bocResult << endl;

/*
        // setting on slave 0 and reading back: link_enabled = 0x0001
        cout << "Setting on slave " << slave << " and reading back: link_enabled = 0x0001" << endl;
        writeSlave(rod0, slave, 0x0, 0x00000001);
        bocResult = readSlave(rod0, slave, 0x0);
        cout << "Slave " << slave << " register 0x" << hex << 0x0 << " read: 0x" << hex << bocResult << endl;

        // setting up TX control register on BOC for channel 0
        cout << "Setting up TX control register on BOC for channel 0" << endl;
        writeBoc(rod0, 0x8C00, 0x01);
        bocResult = readBoc(rod0, 0x8C00);
        cout << "Boc register 0x" << hex << 0x8C00 << " read: 0x" << hex << bocResult << endl;

        // setting up RX control register on BOC for channel 0
        cout << "Setting up RX control register on BOC for channel 0" << endl;
        writeBoc(rod0, 0x8800, 0x03); // North BMF
        bocResult = readBoc(rod0, 0x8800);
        cout << "Boc register 0x" << hex << 0x8800 << " read: 0x" << hex << bocResult << endl;

        // setting up RX status register on BOC for channel 0
        cout << "Setting up RX status register on BOC for channel 0" << endl;
        writeBoc(rod0, 0x8801, 0xFF);
        bocResult = readBoc(rod0, 0x8801);
        cout << "Boc register 0x" << hex << 0x8801 << " read: 0x" << hex << bocResult << endl;
*/
}


void configRegisters(AbstrRodModule* rod0, int slave) {

        // resetting the INMEM FIFO
        writeSlave(rod0, slave, 0x41, 0x00000001);

	setSlaveReadBack(rod0,slave);
	
        cout << "Sending CONFIG MODE command to FeI4A" << endl;
        enterConfigMode(rod0);

	cout << "Configuring FeI4A registers from 2 to 34 (it takes a minute or so ...)" << endl;
	WriteReg(rod0,FeI4_addr[2], FeI4_reg[2]);
	WriteReg(rod0,FeI4_addr[3], FeI4_reg[3]);
	WriteReg(rod0,FeI4_addr[4], FeI4_reg[4]);
	WriteReg(rod0,FeI4_addr[5], FeI4_reg[5]);
	WriteReg(rod0,FeI4_addr[6], FeI4_reg[6]);
	WriteReg(rod0,FeI4_addr[7], FeI4_reg[7]);
	WriteReg(rod0,FeI4_addr[8], FeI4_reg[8]);
	WriteReg(rod0,FeI4_addr[9], FeI4_reg[9]);
	WriteReg(rod0,FeI4_addr[10],FeI4_reg[10]);
	WriteReg(rod0,FeI4_addr[11],FeI4_reg[11]);
	WriteReg(rod0,FeI4_addr[12],FeI4_reg[12]);
	WriteReg(rod0,FeI4_addr[13],FeI4_reg[13]);
	WriteReg(rod0,FeI4_addr[14],FeI4_reg[14]);
	WriteReg(rod0,FeI4_addr[15],FeI4_reg[15]);
	WriteReg(rod0,FeI4_addr[16],FeI4_reg[16]);
	WriteReg(rod0,FeI4_addr[17],FeI4_reg[17]);
	WriteReg(rod0,FeI4_addr[18],FeI4_reg[18]);
	WriteReg(rod0,FeI4_addr[19],FeI4_reg[19]);
	WriteReg(rod0,FeI4_addr[20],FeI4_reg[20]);
	WriteReg(rod0,FeI4_addr[21],FeI4_reg[21]);
	WriteReg(rod0,FeI4_addr[22],FeI4_reg[22]);
	WriteReg(rod0,FeI4_addr[23],FeI4_reg[23]);
	WriteReg(rod0,FeI4_addr[24],FeI4_reg[24]);
	WriteReg(rod0,FeI4_addr[25],FeI4_reg[25]);
	WriteReg(rod0,FeI4_addr[26],FeI4_reg[26]);
	WriteReg(rod0,FeI4_addr[27],FeI4_reg[27]);
	WriteReg(rod0,FeI4_addr[28],FeI4_reg[28]);
	WriteReg(rod0,FeI4_addr[29],FeI4_reg[29]);
	// 0x1E (30d) address missing
	WriteReg(rod0,FeI4_addr[31],FeI4_reg[31]);
	WriteReg(rod0,FeI4_addr[32],FeI4_reg[32]);
	WriteReg(rod0,FeI4_addr[33],FeI4_reg[33]);
	WriteReg(rod0,FeI4_addr[34],FeI4_reg[34]);
}

void readRegister(AbstrRodModule *rod0, int slave, unsigned int address) {

	ReadRegister(rod0, address);

	// reading back the INMEM FIFO
	cout << "Accessing INMEM to read FeI4 register #" << dec << address  << endl;
	for(int j = 0; j <= 7; j++)
	{
		//inmem_reg[j] = readSlave(rod0, slave, 0x40);
		inmem_reg[j] = readDevice(rod0, slave, 0x40);
		//cout << "INMEM value " << hex << j << " read: 0x" << hex << (inmem_reg[j] & 255) << endl;
	}
	cout << " " << hex << (inmem_reg[0] & 255) << " " << hex << (inmem_reg[1] & 255) << " " << hex << (inmem_reg[2] & 255)
		<< " " << hex << (inmem_reg[3] & 255) << " " << hex << (inmem_reg[4] & 255) << " " << hex << (inmem_reg[5] & 255)
		<< " " << hex << (inmem_reg[6] & 255) << " " << hex << (inmem_reg[7] & 255)
		<< endl;

}

void readBackRegisters(AbstrRodModule* rod0, int slave) {
	int bocResult;
/*
	// setting up RX control register on BOC for channel 0
	cout << "Setting up RX control register on BOC for channel 0" << endl;
	writeBoc(rod0, 0x8800, 0x03); // North BMF
	bocResult = readBoc(rod0, 0x8800);
	cout << "Boc register 0x" << hex << 0x8800 << " read: 0x" << hex << bocResult << endl;

	// setting up RX status register on BOC for channel 0
	cout << "Setting up RX status register on BOC for channel 0" << endl;
	writeBoc(rod0, 0x8801, 0xFF);
	bocResult = readBoc(rod0, 0x8801);
	cout << "Boc register 0x" << hex << 0x8801 << " read: 0x" << hex << bocResult << endl;
*/

	// resetting the INMEM FIFO
	cout << "Resetting the INMEM FIFO" << endl;
	writeSlave(rod0, slave, 0x41, 0x00000001);

	cout << "Reading back the first 10 FeI4A registers (reading them all takes a while ...)" << endl;
	for (int i=2;i<=12;i++){
		readRegister(rod0, slave, FeI4_addr[i]);
	}

	cout << "Sending RUNMODE command to FeI4A" << endl;
	enterRunMode(rod0);

	cout << "Sending 100 triggers to FeI4A" << endl;
	for (int i=0;i<=100;i++){
		sendL1A(rod0);
	}

}

// Copied from IBLRodPixController::sendModuleConfigWithPreampOff
void configureModule(AbstrRodModule *m_rod, unsigned int moduleMask, unsigned int bitMask) {

	int m_modPosition[32];
	int m_modNum[32];

	// FEI4 Config starts
	int mod;
	if (moduleMask == 0) moduleMask = 0xffffffff;

	//IBLchange: FEI4 has 13 bit Pixel Register, thus bitMask = 1FFF
	//NKtodo: bitmask must come in a correct way from ModuleConfig, right now it's 3fff, but must be 1fff
	if (bitMask != 0x1fff) std::cout << "sendModuleConfigWithPremapOff for ibl register mask: 0x"<< std::hex << bitMask << std::dec <<std::endl;

	//for (mod=0; mod<8; mod++) {
	for (mod=0; mod<2; mod++) { //just send for one module for testing 
		m_modPosition[mod] = 1;
		m_modNum[mod] = 1;
		if ((m_modPosition[mod]>=0) && (moduleMask&(0x1<<mod))) {
			std::cout << "IBLRodPixController::sendModuleConfigWithPreampOff send moduleConfig for ibl module " <<mod<<std::endl;
			int pos = m_modPosition[mod];
			SendModuleConfigIn sendModuleConfigIn;
			std::cout << "m_modNum[pos] is: " <<m_modNum[pos]<<std::endl;
			sendModuleConfigIn.moduleId  = m_modNum[pos];
			sendModuleConfigIn.cfgId     = PHYSICS_CFG;
			sendModuleConfigIn.chipMask  = 0; /* Reserved for future */
			sendModuleConfigIn.bitMask   = bitMask;

			RodPrimitive* sendConfP;
			sendConfP = new RodPrimitive(sizeof(SendModuleConfigIn)/sizeof(uint32_t),
					0, SEND_MODULE_CONFIG_WITH_PREAMP_OFF, 0, (long*)&sendModuleConfigIn);
			m_rod->executeMasterPrimitiveSync(*sendConfP);
			delete sendConfP;
		}
	}

}


#include <FEI4.h>

pixData* decodePixData(FEI4& fei4, const vector<int>& frame)
{
	// Empty frame or no pix data
	if (frame.size() == 0 || frame[0] != 0xe9)
		return NULL;

	// int flag = ((frame[1]&0x80)>>7); unused?
	int lv1ID = (frame[1]&0x7F);
	int bcID = (frame[2]&0xFF);
	pixData *decodedData = new pixData(lv1ID, bcID);
	int col = 0;
	int row = 0;
	int tot1 = 0;
	int tot2 = 0;
	for (unsigned int i=3; i<frame.size(); i++) {
		int field = (i-3)%3;
		switch (field) {
			case 0: 
				col = 0;
				row = 0;
				tot1 = 0;
				tot2 = 0;
				if ( frame[i] == 0xef) {
					fei4.decodeServiceRecord(frame, i);
					i+=2;
				}
				col = ((0xFE & frame[i]) >> 1);
				row = ((0x01 & frame[i]) << 8);
				break;

			case 1: 
				row += (0xFF & frame[i]);
				break;

			case 2: 
				tot1 = decodeToT(fei4, ((0x00F0 & frame[i]) >> 4));
				tot2 = decodeToT(fei4, (0x000F & frame[i]));
				decodedData->addData(col, row, tot1, tot2);
				break;

			default:
				break;
		}
	}
	return decodedData;
}


// Decode the ToT values in dependance of the HitDiscCnf
int decodeToT(FEI4& fei4, int ToT) {
	int HitDiscCnf = fei4.getRegister(26, 2);

	if (HitDiscCnf >= 0 && HitDiscCnf <= 2) {
		if (ToT >= 0 && ToT <= 13)
			return ToT + HitDiscCnf + 1;
		else if (ToT == 14)
			return HitDiscCnf;
	}
	return 0;
}


void readBocFifo(AbstrRodModule* rod0, int slave, int rx_ch) {

	int reg, bocResult;
        int hex_base = 0x8000;
	if(slave) hex_base = 0x4000;

	cout << "READING BOC FIFO CH" << rx_ch << endl;
	int bocCnt(0);
	while(1) {
		reg = hex_base | (0x800 + (rx_ch<<5)) | 0x3;
		bocResult = readBoc(rod0, reg);
		cout << "Boc register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;
		if(bocResult == 0xbc) break;
		if((bocResult == 0xff || bocResult == 0x00) && bocCnt++>5) break;
	}
	cout << string(80,'-') << endl;


}

int initPixelLabIblSetupByMask(FEI4& fei4_module, int slave, uint8_t tx_mask, uint16_t rx_mask, int chipID, bool isOptical) {

	if(getenv("NO_FEI4_INIT")) {
		std::cout << "Skipping " << __PRETTY_FUNCTION__ << std::endl;
		return 0;
	}

	if( (uint8_t)(rx_mask&0xFF) != tx_mask && (uint8_t)(rx_mask>>8) != tx_mask ) {
                std::cout << "rx_mask(0x" << std::hex << rx_mask << ") is not compatible with tx_mask(0x" << tx_mask << std::dec << ")." << std::endl;
                std::cerr << "Readback won't be physically possible!" << std::endl;
                exit(1);
        }

#ifdef HAS_GPIB
	MiniDCSClient miniDcsClient("localhost", "3042");
#endif

        AbstrRodModule* rod0 = fei4_module.getRod();

        int bocResult, reg, value;
	int V5mask = tx_mask;

        int hex_base = 0x8000;
        if(slave) hex_base = 0x4000;

	std::cout << "SLAVE " << slave << ", TX mask 0x" << std::dec << tx_mask << ", RX mask 0x" << rx_mask << std::dec << std::endl;
	

        reg = 0xC+(slave<<1);
        writeDevice(rod0, 5, reg, V5mask);
        cout << "Writing Virtex5 register 0x" << hex << reg << " to 0x" << V5mask << dec << endl;

	/*
	// Bit 7-8 is for reading channels: 00 ch [0-3], 01 ch [4-7], 10 ch [8-11], 11 ch [12-15]
	
	reg = 0x42; 
	value = (1 << CTL_MASTER_MODE_BIT) + (((rx_ch/4)<<CTL_INMEM_SEL0_BIT) + (1 << CTL_BOC_ENABLE_BIT)); 
	writeDevice(rod0, slave, reg, value);
	cout << "Writing SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
	value = readDevice(rod0, slave, reg);
	cout << "Reading SLAVE" << slave << " control register (0x" << hex << reg << "): 0x" << value << dec << endl;
	*/

	for(size_t tx_ch = 0 ; tx_ch < 8 ; ++tx_ch) {

		if( !(tx_mask&(1<<tx_ch)) ) continue; // Skipping channel if not in mask

		reg = hex_base | (0xC00 + (tx_ch<<5));
		// setting up TX control register on BOC BMF for channel
		cout << "Setting up TX control register on BOC-BMF for channel " << tx_ch << endl;
		writeBoc(rod0, reg, 0x01); // Enable, with BPM
		bocResult = readBoc(rod0, reg);
		cout << "Boc TX Control register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;
		bocResult = readBoc(rod0, reg);
		cout << "Boc TX Control register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;

	}

	if(isOptical) {
		cout << "Laser Enable" << endl;
		reg = 0x7;
		writeBoc(rod0, reg, 0xAB); // Enable BCF laser
		//cout << "Laser Disable" << endl;
		//writeBoc(rod0, reg, 0x00); // Disable BCF laser
		bocResult = readBoc(rod0, reg);
		cout << "Boc BCF register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;
		cout << "Please reset the optoboard ..." << endl;
		cin.get();
#ifdef HAS_GPIB
		//miniDcsClient.sendCommand("VVDC RST"); // Either this or ViSet, which belong to the same device
#endif
	}

	cout << "Please power-up the chip ..." << endl;
	cin.get();

#ifdef HAS_GPIB
	//miniDcsClient.sendCommand("FEI4B-1 ON"); // Either this or FEI4B-2, which belong to the same device
#endif

	cout << "Now configuring the chip!" << endl;
	fei4_module.setChipID(chipID);
	fei4_module.init();


	for(size_t rx_ch = 0 ; rx_ch < 16 ; ++rx_ch) {

		if( !(rx_mask&(1<<rx_ch)) ) continue; // Skipping channel if not in mask

		reg = hex_base | (0x800 + (rx_ch<<5));

		// setting up RX control register on BOC BMF for channel
		cout << "Setting up RX control register on BOC for channel " << rx_ch << endl;
		writeBoc(rod0, reg, 0x00);
		writeBoc(rod0, reg, 0x01); // RX_EN
		bocResult = readBoc(rod0, reg);
		cout << "Boc RX Control register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;

		reg = hex_base | (0x801 + (rx_ch<<5));
		bocResult = 0xFF;
		// setting up RX status register on BOC BMF for channel
		int cnt(0);
		while ((bocResult & 0xF) != 0x1) {
			cout << "Setting up RX status register on BOC for channel " << rx_ch << endl;
			writeBoc(rod0, reg, 0xFF);
			bocResult = readBoc(rod0, reg);
			cout << "Boc RX Status register 0x" << hex << reg << " read: 0x" << bocResult << dec <<endl;
			//if(++cnt>10) { cerr << "Error: BOC is not synced." << endl; exit(1); }
			if(++cnt>2) { cerr << "Error: BOC is not synced on channel " << rx_ch << endl; break; }
		}

		reg = hex_base | (0x800 + (rx_ch<<5));

		// setting up RX control register on BOC BMF for channel
		cout << "Setting up RX control register on BOC for channel " << rx_ch << endl;
		writeBoc(rod0, reg, 0x03);
		writeBoc(rod0, reg, 0x07); //+ monitoring (FIFO in BOC)
		bocResult = readBoc(rod0, reg);
		cout << "Boc RX Control register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;

	}

	int linkEn = rx_mask;
	cout << "Setting on slave " << slave << " and reading back: link_enabled = 0x" << hex << linkEn << dec << endl;
	writeDevice(rod0, slave, 0x0, linkEn);
	bocResult = readSlave(rod0, slave, 0x0);
	cout << "Slave " << slave << " register 0x" << hex << 0x0 << " read: 0x" << hex << bocResult << endl;

	// resetting the INMEM FIFO
	cout << "Resetting the INMEM FIFO" << endl;
	writeSlave(rod0, slave, 0x41, 0x00000001);
/*
	cout << "CLEARING BOC FIFO" << endl;
	bocResult = 0;
	// Flush the FIFO empty
	while((bocResult&0xF0) != 0x20) {
		reg = hex_base | (0x800 + (rx_ch<<5)) + 0x3;
		bocResult = readBoc(rod0, reg);
		cout << "Boc register 0x" << hex << reg << " read: 0x" << hex << bocResult << endl;
		//std::cout << bocResult << "\t";
		reg = hex_base | (0x801 + (rx_ch<<5));
                writeBoc(rod0, reg, 0xFF);
                bocResult = readBoc(rod0, reg);
	}
	cout << endl;
	cout << "BOC FIFO is now empty." << endl;
*/
	while(1){
		readRegister(rod0, slave, 2);
		//readRegister(rod0, slave, 3);
		//readRegister(rod0, slave, 4);
		if( (inmem_reg[0]&0xFF) != 0xfc || (inmem_reg[7]&0xFF) != 0xbc ) {
			std::cerr << "Error: register readback failed!" << std::endl;
			std::cerr << "Please check that the system is in good shape. You may need to reset the BMF" << std::endl;
			exit(1);
		}

		//readBocFifo(rod0, slave, rx_ch);		
		break;
	}

#if 0
	while(0) {
		int bocResult;
		int hex_base = 0x8000;
		if(slave) hex_base = 0x4000;
		int reg = hex_base | (0x800 + (rx_ch<<5)) | 0x2;
		printf("Read BOC register 0x%04x\n", reg);
		uint8_t rval = readBoc(rod0, reg);
		uint16_t data = ((uint16_t)rval << 8) + readBoc(rod0, reg+1);
		std::cout << data << "\t";
		if((data&0x1FF) == 0x1BC) break;
	}
	std::cout << std::endl;
#endif

#ifdef HAS_GPIB
	//miniDcsClient.sendCommand("FEI4B-1 OFF"); // Either this or FEI4B-2, which belong to the same device
	//miniDcsClient.sendCommand("VVDC OFF"); // Either this or ViSet, which belong to the same device
#endif

	return 0;
}

int initPixelLabIblSetup(FEI4& fei4_module, int slave, int tx_ch, int rx_ch, int chipID, bool isOptical) {
	initPixelLabIblSetupByMask(fei4_module, slave, 1<<tx_ch, 1<<rx_ch, chipID, isOptical);
}


int disableAllChannels(AbstrRodModule* rod0, int slave) {
  int hex_base = 0x8000;
  if (slave) hex_base = 0x4000;
  
  cout << "Disabling all serial stream on Virtex5 for slave" << slave << endl;
  writeDevice(rod0, 5, 0xC+(slave<<1), 0x0);

  for(int c = 0 ; c < 4 ; ++c) {
    cout << "Disabling channel on BOC TX " << c << endl;
    writeBoc(rod0, hex_base | (0xC00+(c<<5)), 0x0);
    cout << "Disabling BOC RX in channel " << c << endl;
    writeBoc(rod0, hex_base | (0x800+(c<<5)), 0x0);
    cout << "Resetting BOC RX_CTL in channel " << c << endl;
    writeBoc(rod0, hex_base | (0x801+(c<<5)), 0x0);
  }
  
  return 0;
}

TH2F* digitalInjectionForHistogrammer(FEI4& fei4, int calpulse) {
	TH2F* hist = new TH2F("ToTMap", "ToTMap", 80, 0.5, 80.5, 336, 0.5, 336.5);
	digitalInjectionForHistogrammer(fei4, hist, calpulse);
	return hist;
}

void digitalInjectionForHistogrammer(FEI4& fei4, TH2F* hist, int calpulse)//, int ntrigger)
{	
  const int trig_delay = 2;
  const int trig_count = 4;
  
  AbstrRodModule* rod0 = fei4.getRod();
  int slave = 0;
  slave = 1;
  std::cout << __PRETTY_FUNCTION__ << std::endl;

  /*
  if (!hist) {
    std::cout << "Error: No histogram provided for saving output." << std::endl;
    return;
    }
  */

  for (int iMask=0; iMask<1; ++iMask){
    
    fei4.setRunMode(false);
    
    // Cal pulse width and delay
    //fei4.setRegister(26, 0, 0x008); // Reg: CNT
    fei4.setRegister(27, 11, 0x0);    // Reg: SR_CLOCK
    fei4.setRegister(26, 2, 0x2);     // Reg: HitDiscCnfg
  
    // Trigger
    fei4.setRegister(2, 0, trig_count);        // Reg: Trigger_Count
    
    // Maxi. Trigger Latency
    fei4.setRegister(25, 0, 255 - 8 - trig_delay*8 - (trig_count/2));     // Reg: LatCnfg
  
    unsigned int pixelMask[21] = { 0 };
    
    for(int i = 0 ; i < 21 ; ++i) {
      //pixelMask[i] = 0x55555555;
      //pixelMask[i] = 0xF0;
      pixelMask[i] = 0x0;
    }

    //pixelMask[0]  = 0x000000F0;
    //pixelMask[20] = 0x0F000000;
    //pixelMask[10] = 0x00F00F00;
    
    //pixelMask[0]  = 0x000000A0;//--->1(col 2,row 336)0100000--->.........
    //pixelMask[10] = 0x00500A00;//.........................--->0000101(col2,row 2)0--->01(col1,row2)010000--->......
    //pixelMask[20] = 0x05000000;//.....--->00000101(col 1,row 336)---> 

    
    if (iMask==0){

       //Firing only the even rows...
	std::cout << "Firing even rows= " <<iMask<< std::endl;
       for(int i = 0 ; i < 1; ++i) {
	 pixelMask[i] = 0xAAAAAAAA;
         //pixelMask[i] = 0x000000A0;
       }

       pixelMask[10] = 0x5555AAAA;//...........................--->0000101(col2,row 2)0--->01(col1,row2)010000--->......
       //pixelMask[10] = 0x0005A000;//.........................--->0000101(col2,row 2)0--->01(col1,row2)010000--->......

       for(int i = 20 ; i < 21 ; ++i) {
	 pixelMask[i] = 0x55555555;
	 //pixelMask[i] = 0x05000000;
       }

    } else if (iMask==1){

      //Firing only the odd rows...
	std::cout << "Firing odd rows= " <<iMask<< std::endl;
      for(int i = 0 ; i < 10 ; ++i) {
	pixelMask[i] = 0x55555555;
	//pixelMask[i] = 0x00000050;
      }
 
     pixelMask[10] = 0xAAAA5555;//............................--->0000101(col2,row 2)0--->01(col1,row2)010000--->......
      //pixelMask[10] = 0x000A5000;//.........................--->0000101(col2,row 2)0--->01(col1,row2)010000--->......

      for(int i = 11 ; i < 21 ; ++i) {
	pixelMask[i] = 0xAAAAAAAA;
	//pixelMask[i] = 0x0A000000;
      }
    }

    // fei4.ecr(); // Event Couter Reset
    int startDCol = 0;
    int numDCol = 1;
    //  int numDCol = 10;
  
    // Colpr_Mode:0b00, ColAddr:0
    for (int colAddr = startDCol ; colAddr < numDCol ; colAddr++) {
      fei4.setRunMode(false);
    
      // Cal pulse width and delay
      //fei4.setRegister(26, 0, 10);
      fei4.setRegister(26, 0, calpulse);
      
      // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
      fei4.setRegister(22, 1, 0x0);    // Reg: Colpr_Mode
      
      // Column Address
      fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr
      
      // Select S0
      fei4.setRegister(13, 1, 0x0);     // Reg: S0
      
      if(pixelMask[0] == 0) {

 	std::cout << "wFE PixelMask (=0x0)= " <<pixelMask<< std::endl;
      // set all pixel shift register to 1
      // Select S0
	fei4.setRegister(13, 1, 0x1);     // Reg: S0
      // Sr Clock
	fei4.setRegister(27,11, 0x1);     // Reg: SR_CLOCK, to write in parallel in the whole SR in 1 clock 

      // Global pulse
	fei4.globalPulse(1);

      // Clear S0
	fei4.setRegister(13, 1, 0x0);     // Reg: S0
      // Clear Sr Clock
	fei4.setRegister(27,11, 0x0);     // Reg: SR_CLOCK
      // Select Double Column Mode, 0x00 deafult; 0x11 = all
	fei4.setRegister(22, 1, 0x00);    // Reg: Colpr_Mode
      // Column Address
	fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr

      } else {

//#define WITH_SR_READ
#ifdef WITH_SR_READ
	fei4.setRegister(27, 14, 0x00); // SRRead
	for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = 0xFFFFFFFF;
	for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = i;
	std::cout << "wFE PixelMask (with SR_READ) = " <<pixelMask<< std::endl;
	fei4.wrFrontEnd(pixelMask);
	fei4.setRegister(27, 14, 0x01); // SRRead
	for(int i = 0 ; i < 21 ; ++i) pixelMask[i] = i;
	std::cout << "Do something!" << std::endl;
	std::cin.get();
#endif


	std::cout << "wFE PixelMask(diff from 0x0)= " <<pixelMask<< std::endl;
	fei4.wrFrontEnd(pixelMask);
      

#ifdef WITH_SR_READ
	for(int k = 0 ; k < 6 ; ++k) fei4.readFrame();      
	fei4.setRegister(27, 14, 0x00); // SRRead
#endif		  
    } //endif pixelMask[0]!=0x0
    
      // Set Pixel strobes
      fei4.setRegister(13, 2, 0x2801);  // Reg: PixelStrobes (no HitOr)
      // Set LatchEn
      fei4.setRegister(27,10, 0x1);     // Reg: LatchEn bit set to high
      // Global pulse
      fei4.globalPulse(10);             // Pulse Load Latches line (Clock cycles lenght of the pulse) 
      // Deselect LatchEn
      fei4.setRegister(27,10, 0x0);     // Reg: LatchEn
      // Set all pixel strobes to 0 and select S0 and S1 to 0
      fei4.wrRegister(13, 0x0);         // Regs: S1,S0,PixleStrobes
      
      // resetting the INMEM FIFO
      cout << "Resetting the INMEM FIFO" << endl;
      writeSlave(rod0, slave, 0x41, 0x00000001);
    
      cout << "Setting RUN MODE" << endl;
      fei4.setRunMode(true);
      
      cout << "Sending Cal Trigger" << endl;
      fei4.calTrigger(trig_delay);

      for(int i = 0 ; i < trig_count ; ++i) {
	usleep(100);
	cout << "decoding "<<i << endl;
	//pixData *pd = decodePixData(fei4, fei4.readFrame());
	//if (pd) hist->Add(pd->getToTmap());
      }

    }//endfor DC LOOP
    
  }// end of MASK LOOP

  TCanvas *canv = new TCanvas("cToTMap", "ToTMap", 800, 600);
  ///*
  string TitleName = "ToT map         CalPulseLength = ";
  TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 0));
  TitleName += "   HitDiscCnf = ";
  TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 2));
  TitleName += " ";
  hist->SetTitle(TitleName.c_str());
  //*/
  hist->SetXTitle("column");
  hist->SetYTitle("row");
  
  //TPaveStats *st=(TPaveStats*)hist->FindObject("stats");
  //st->SetX1NDC(0.7);
  //st->SetX2NDC(0.7);

  gStyle->SetOptStat(11);
  gStyle->SetStatX(0.8);
  gStyle->SetStatY(0.8);
  

  TFile f("hist.root", "recreate");
  hist->Write("", TObject::kOverwrite);
  f.Close();

  hist->Draw("Colz");
  string filename="ToTmap_";
  filename+=boost::lexical_cast<std::string>(calpulse);
  filename+=".pdf";
  canv->Print(filename.c_str());
}


TH2F* analogInjectionForHistogrammer(FEI4& fei4) {
	TH2F* hist = new TH2F("ToTMap", "ToTMap", 80, 0.5, 80.5, 336, 0.5, 336.5);
	analogInjectionForHistogrammer(fei4, hist);
	return hist;
}

void analogInjectionForHistogrammer(FEI4& fei4, TH2F* hist)//, int ntrigger)
{	
  const int trig_delay = 2;
  const int trig_count = 16;
  
  AbstrRodModule* rod0 = fei4.getRod();

  int slave = 0;
  slave = 1;
  std::cout << __PRETTY_FUNCTION__ << std::endl;

  fei4.setRunMode(false);
	
  fei4.setRegister(21, 3, 0x0);   // Reg: DIGHIN_SEL --> move to Analog Injection; default is Digital(21,3,0x1)

  fei4.setRegister(27, 11, 0x0);  // Reg: SR_CLOCK

  // fei4.setRegister(26, 2, 0x2);   // Reg: HitDiscCnfg
  
  // Trigger
  fei4.setRegister(2, 0, trig_count);        // Reg: Trigger_Count
  
  // Maxi. Trigger Latency
  fei4.setRegister(25, 0, 255 - 8 - trig_delay*8 - (trig_count/2));     // Reg: LatCnfg
  
  unsigned int pixelMask[21] = { 0 };

  //for(int i = 0 ; i < 21 ; ++i) {
  //  pixelMask[i] = 0x0;
  //}

  pixelMask[0]  = 0xFFFFFFFF;

  //pixelMask[0] = 0xF0;
  //pixelMask[10] = 0x00F00F00;
  //pixelMask[20] = 0x0F000000;
  
  int startCol = 0;
  int maxCol = 1;
  
  for (int colAddr = startCol ; colAddr < maxCol ; colAddr++) {

    fei4.setRunMode(false);        
    
    // Select Double Column Mode, 0x00=AddrDC, 0x11 = all
    fei4.setRegister(22, 1, 0x00);    // Reg: Colpr_Mode

    // Select Column Address
    fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr
    
    // Select S0,S1,HitLd=0
    fei4.setRegister(13, 1, 0x0);     // Reg: S0
    fei4.setRegister(13, 0, 0x0);     // Reg: S1
    fei4.setRegister(21, 1, 0x0);     // Reg: HitLd

    // Set Pixel Strobes to select Small and High Capacitance and to set SR(enable Dij Inj.) to 0
    fei4.setRegister(13, 2, 0x8C5);     // Reg: PixelStrobes for Analog FDAC=8 TDAC=8
    // Set Pixel strobes
    //    fei4.setRegister(13, 2, 0x2801);  // Reg: PixelStrobes (no HitOr) for Dijital

    if(pixelMask[0] == 0) {
      // set all pixel shift register to 1
      // Select S0
      fei4.setRegister(13, 1, 0x1);     // Reg: S0
      // Sr Clock
      fei4.setRegister(27,11, 0x1);     // Reg: SR_CLOCK
      // Global pulse
      fei4.globalPulse(1);
      // Clear S0
      fei4.setRegister(13, 1, 0x0);     // Reg: S0
      // Clear Sr Clock
      fei4.setRegister(27,11, 0x0);     // Reg: SR_CLOCK
      // Select Double Column Mode, 0x00 deafult; 0x11 = all
      fei4.setRegister(22, 1, 0x00);    // Reg: Colpr_Mode
      // Column Address
      fei4.setRegister(22, 2, colAddr); // Reg: Colpr_Addr
    } else {

      std::cout << "wFE PixelMask(diff from 0x0)= " <<pixelMask<< std::endl;
      fei4.wrFrontEnd(pixelMask);

    } //endif pixelMask[0]!=0x0

      
    // Set Vthcourse and fine
    fei4.setRegister(20, 0, 0x1);     // Reg: Vthin Coarse
    fei4.setRegister(20, 1, 0xC0);     // Reg: Vthin Fine

    // Set LatchEn
    fei4.setRegister(27,10, 0x1);     // Reg: LatchEn

    // Global pulse
    fei4.globalPulse(10);             // Bunch Crossing Lenght

    // Deselect LatchEn
    fei4.setRegister(27,10, 0x0);     // Reg: LatchEn

    // Set all pixel strobes to 0 and select S0 and S1 to 0
    fei4.wrRegister(13, 0x0);         // Regs: S1,S0,PixleStrobes

    // Column Address to Analog Inject
    int AnacolAddr=colAddr+1;
    fei4.setRegister(22, 1, 0x0); // Reg: Colpr_Addr

    fei4.setRegister(22, 2, AnacolAddr); // Reg: Colpr_Addr

    // resetting the INMEM FIFO
    cout << "Resetting the INMEM FIFO" << endl;
    writeSlave(rod0, slave, 0x41, 0x00000001);

    // Cal pulse width and delay
    fei4.setRegister(26, 0, 0x5);

    cout << "Setting RUN MODE" << endl;
    fei4.setRunMode(true);

    cout << "Sending Cal Trigger" << endl;
    fei4.calTrigger(trig_delay);

    for(int i = 0 ; i < trig_count ; ++i) {
      usleep(100);
      cout << "decoding "<<i << endl;
      pixData *pd = decodePixData(fei4, fei4.readFrame());
      if (pd) hist->Add(pd->getToTmap());
    }// filling the histogram

  }//endfor DC LOOP

  TCanvas *canv = new TCanvas("cToTMap", "ToTMap", 800, 600);
  ///*
  string TitleName = "Analog ToT map                  CalPulseLength = 8 (";
  TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 0));
  TitleName += ")   HitDiscCnf = 1 (";
  TitleName += boost::lexical_cast<std::string>(fei4.getRegister(26, 2));
  TitleName += ")";
  hist->SetTitle(TitleName.c_str());
  //*/
  hist->SetXTitle("column");
  hist->SetYTitle("row");
  
  TFile f("hist.root", "recreate");
  hist->Write("", TObject::kOverwrite);
  f.Close();

  hist->Draw("Colz");
  canv->Print("Analog_ToTmap.pdf");
}
