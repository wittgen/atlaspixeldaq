// Author: Timon Heim
// Institute: University of Wuppertal
// E-Mail: heim@physik.uni-wuppertal.de
// Date: 16.08.11
// Version: 0.1

#include "gReg.h"

using namespace std;

// Constructor + GReg initilization
gReg::gReg() {
	gVerboseMode = false;
    // #2
    this->addItem(2, 0, "Trigger_Count",       0xF000, 1, 1);
    this->addItem(2, 1, "Conf_Addr_Enable",    0x0800, 1, 1);
    this->addItem(2, 2, "CFGspare2",           0x07FF, 0, 0);
    // #3
    this->addItem(3, 0, "ErrorMask(15:0)",     0xFFFF, 0, 1); 
    // #4
    this->addItem(4, 0, "ErrorMask(31:16)",     0xFFFF, 0, 1);
    // #5
    this->addItem(5, 0, "PrmpVbp_R",            0xFF00, 43, 0);
#ifdef FEI4A
    this->addItem(5, 1, "Vthin",                0x00FF, 255, 0);
#else
    this->addItem(5, 1, "BufVgOpAmp",                0x00FF, 255, 0);
    //this->addItem(5, 1, "BufVgOpAmp",                0x00FF, 170, 0);
    //this->addItem(5, 1, "GADCVref",                  0x00FF, 170, 0);
#endif
    // #6
    this->addItem(6, 0, "DisVbn_CPP",           0xFF00, 62, 0);
    this->addItem(6, 1, "PrmpVbp",              0x00FF, 43, 0);
    // #7
    this->addItem(7, 0, "TdacVbp",              0xFF00, 255, 0);
    this->addItem(7, 1, "DisVbn",               0x00FF, 26, 0);
    // #8
    this->addItem(8, 0, "Amp2Vbn",              0xFF00, 79, 0);
    this->addItem(8, 1, "Amp2VbnFol",           0x00FF, 26, 0);
    // #9
    this->addItem(9, 0, "PrmpVbp_T",            0xFF00, 0, 0);
    this->addItem(9, 1, "Amp2Vbp",              0x00FF, 85, 0);
    // #10
    this->addItem(10, 0, "FdacVbn",             0xFF00, 50, 0);
    this->addItem(10, 1, "Amp2Vbpff",           0x00FF, 13, 0);
    // #11
#if 0
    this->addItem(11, 0, "PrmpVbnFol",          0xFF00, 218, 0);
    this->addItem(11, 1, "PrmpVbp_L",           0x00FF, 10, 0);
#else
    this->addItem(11, 0, "PrmpVbnFol",          0xFF00, 106, 0);
    this->addItem(11, 1, "PrmpVbp_L",           0x00FF, 43, 0);
#endif
    // #12
    this->addItem(12, 0, "PrmpVbpf",            0xFF00, 20, 0);
    this->addItem(12, 1, "PrmpVbnLcc",          0x00FF, 0, 0);
    // #13
    this->addItem(13, 0, "S1",                  0x8000, 0, 1);
    this->addItem(13, 1, "S0",                  0x4000, 0, 1);
    this->addItem(13, 2, "PixelStrobes",        0x3FFE, 0, 0);
    this->addItem(13, 3, "Spare1",              0x0001, 0, 1);
    // #14
    this->addItem(14, 0, "LvdsDrvIref",         0xFF00, 171, 0);
    this->addItem(14, 1, "BonnDac",             0x00FF, 237, 0);
    // #15
    this->addItem(15, 0, "PllIbias",            0xFF00, 88, 0);
    this->addItem(15, 1, "LvdsDrvVos",          0x00FF, 105, 0);
    // #16
    this->addItem(16, 0, "TempSensIbias",       0xFF00, 0, 0);
    this->addItem(16, 1, "PllIcp",              0x00FF, 28, 0);
    // #17
    this->addItem(17, 0, "DAC8SPARE1",          0xFF00, 0, 1);
    this->addItem(17, 1, "PlsrIdacRamp",        0x00FF, 213, 0);
    // #18
    this->addItem(18, 0, "DAC8SPARE2",          0xFF00, 0, 1);
    this->addItem(18, 1, "PlsrVgOPamp",         0x00FF, 255, 1);
    // #19
    this->addItem(19, 0, "PlsrDacBias",         0xFF00, 96, 1);
    this->addItem(19, 1, "DAC8SPARE5",          0x00FF, 0, 1);
    // #20
    this->addItem(20, 0, "Vthin_AltCoarse",     0xFF00, 255, 0);
    this->addItem(20, 1, "VthinAltFine",        0x00FF, 153, 0);
    // #21
    this->addItem(21, 0, "Spare2",              0xE000, 0, 1);
    this->addItem(21, 1, "HITLD_IN",            0x1000, 0, 1);
    this->addItem(21, 2, "DINJ_OVERRIDE",       0x0800, 0, 1);
    this->addItem(21, 3, "DIGHITIN_SEL",        0x0400, 1, 1);
    this->addItem(21, 4, "PlsrDAC",             0x03FF, 150, 0);
    // #22
    this->addItem(22, 0, "Spare3",              0xFC00, 0, 1);
    this->addItem(22, 1, "Colpr_Mode",          0x0300, 0, 0);
    this->addItem(22, 2, "Colpr_Addr",          0x00FC, 0, 0);
    this->addItem(22, 3, "Spare4",              0x0003, 0, 1);
    // #23
    this->addItem(23, 0, "DisableColumnCnfg(15:00)", 0xFFFF, 0, 1);
    // #24
    this->addItem(24, 0, "DisableColumnCnfg(31:16)", 0xFFFF, 0, 1);
    // #25
    this->addItem(25, 0, "LatCnfg",             0xFF00, 210, 1);
    this->addItem(25, 1, "DisableColumnCnfg(39:32)", 0x00FF, 0, 1);
    // #26
    this->addItem(26, 0, "CNT(12:0)",           0xFFF8, 0, 1);
    this->addItem(26, 1, "StopModeCnfg",        0x0004, 0, 1);
    this->addItem(26, 2, "HitDiscCnfg",         0x0003, 2, 1);
    // #27
    this->addItem(27, 0, "PllEn",               0x8000, 1, 1);
    this->addItem(27, 1, "Efuse_Sense",         0x4000, 0, 1);
    this->addItem(27, 3, "StopSlkPulse",        0x2000, 0, 1);
    this->addItem(27, 4, "ReadErrorReq",        0x1000, 0, 1);
    this->addItem(27, 5, "ReadSkipped",         0x0800, 0, 1);
    this->addItem(27, 6, "Spare5",              0x03C0, 0, 1);
    this->addItem(27, 7, "GateHitOr",           0x0020, 0, 1);
    this->addItem(27, 8, "CalEn",               0x0010, 0, 1);
    this->addItem(27, 9, "SrClr",               0x0008, 0, 1);
    this->addItem(27, 10, "LatchEn",            0x0004, 0, 1);
    this->addItem(27, 11, "SR_CLOCK",           0x0002, 0, 1);
    this->addItem(27, 12, "CNT(13)",            0x0001, 0, 1);
    this->addItem(27, 13, "GADCStart",          0x0400, 0, 1);
    this->addItem(27, 14, "SRR",	        0x0200, 0, 1);
    // #28
    this->addItem(28, 0, "lvdsDrvSet06",         0x8000, 1, 1);
    this->addItem(28, 1, "Spare6",              0x7C00, 0, 1);
    this->addItem(28, 2, "EN_40M",              0x0200, 1, 1);
    this->addItem(28, 3, "EN_80M",              0x0100, 0, 1);
    this->addItem(28, 4, "CLK1_S0",             0x0080, 0, 1);
    this->addItem(28, 5, "CLK1_S1",             0x0040, 0, 1);
    this->addItem(28, 6, "CLK1_S2",             0x0020, 0, 1);
    this->addItem(28, 7, "CLK0_S0",             0x0010, 0, 1);
    this->addItem(28, 8, "CLK0_S1",             0x0008, 0, 1);
    this->addItem(28, 9, "CLK0_S2",             0x0004, 1, 1);
    this->addItem(28, 10, "EN_160M",            0x0002, 1, 1);
    this->addItem(28, 11, "EN_320M",            0x0001, 0, 1);
    // #29
    this->addItem(29, 0, "Spare7",              0xC000, 0, 1);
    this->addItem(29, 1, "no8b10bModeCnfg",     0x2000, 0, 1);
    this->addItem(29, 2, "clkToOutCnfg",        0x1000, 0, 1);
    this->addItem(29, 3, "EmptyRecordCnfg",     0x0FF0, 0, 0);
    this->addItem(29, 4, "Spare8",              0x0008, 0, 1);
    this->addItem(29, 5, "lvdsDrvEn",           0x0004, 1, 1);
    this->addItem(29, 6, "lvdsDrvSet30",        0x0002, 1, 1);
    this->addItem(29, 7, "lvdsDrvSet12",        0x0001, 1, 1);
    // #31
    this->addItem(31, 0, "PlsrRiseUpTau",       0xE000, 7, 1);
    this->addItem(31, 1, "PlsrPwr",             0x1000, 1, 1);
    this->addItem(31, 2, "PlsrDelay",           0x0FC0, 2, 0);
    this->addItem(31, 3, "ExtDigCalSW",         0x0020, 0, 1);
    this->addItem(31, 4, "ExtAnaCalSW",         0x0010, 0, 1);
    this->addItem(31, 5, "Spare9",              0x0008, 0, 1);
    this->addItem(31, 6, "GADC_Sel",            0x0007, 0, 1);

    this->addItem(35, 0, "chip_SN",             0xFFFF, 15, 1);

    this->addItem(40, 0, "GADC_Out",            0x03FF, 0, 1);

    if (gVerboseMode)
        this->printConf();
}

// Deconstrcutor
gReg::~gReg() {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        delete FEmem[i];
    }
}

//Pipe out Config
void gReg::printConf() {
	for (unsigned int j=0; j<FEmem.size(); j++) {
		//cout << FEmem[j]->getAddress() << "\t" << FEmem[j]->getSubAddress() << "\t" << FEmem[j]->getName() << "\t" << FEmem[j]->getMask() << "\t" << FEmem[j]->getValue() << "\t" << FEmem[j]->getDefValue() << endl;
		// latex stlye
		cout << dec << FEmem[j]->getAddress() << " & " << FEmem[j]->getSubAddress() << " & " << FEmem[j]->getName() << " & 0x" << hex << FEmem[j]->getMask() << " & 0x" << FEmem[j]->getValue()  << endl;
	}
}

// Add Register Item to Global Register FE memory
void gReg::addItem(int address, int subAddress, string name, int mask, int value, bool endianness) {
    regItem *item = new regItem(address, subAddress, name, mask, value, endianness);
    FEmem.push_back(item);
}

// Get global register value
int gReg::getGRegValue(int address) {
	int regValue = 0;
	int cnt = 0;
	for (unsigned int j=0; j<FEmem.size(); j++) {
		if (FEmem[j]->getAddress() == address) {
			cnt++;
			int lowbound = 0;
			int bitlength = 0;
			int itemValue = 0;

			for (int temp=FEmem[j]->getMask(); temp%2 != 1 && temp != 0; temp = temp >> 1) 
				lowbound++;

			if (FEmem[j]->getEndianness()) {
				itemValue = FEmem[j]->getValue();
			} 
			else {
				for(int i=0;i<16;i++)
					bitlength += (FEmem[j]->getMask()>>i) & 1;
				for(int i=0; i<bitlength; i++) 
					itemValue |= (((FEmem[j]->getValue() >> i) & 1) << (bitlength - i - 1)); 
			}

			regValue |= ((FEmem[j]->getMask() & (itemValue << lowbound)) & 0xffff);
		}
	}
  if(cnt != 0) {
		return regValue;
  } 
	else {
		return -1;
  }
}

// Set global register value
void gReg::setGRegValue(int address, int value) {
    for (unsigned int j=0; j<FEmem.size(); j++) {
        if (FEmem[j]->getAddress() == address) {
            int itemValue = 0;
            int lowbound = 0;
            int bitlength = 0;
            for (int temp=FEmem[j]->getMask(); temp%2 != 1 && temp != 0; temp = temp >> 1) lowbound++;
            itemValue = ((FEmem[j]->getMask() & (value >> lowbound)) & 0xffff);
            if (FEmem[j]->getEndianness()) {
                FEmem[j]->setValue(itemValue);
            } else {
                for(int i=0;i<16;i++) bitlength += (FEmem[j]->getMask()>>i) & 1;
                int temp = 0;
                for(int i=0; i<bitlength; i++) 
                    temp |= (((itemValue >> i) & 1) << (bitlength - i - 1));
                FEmem[j]->setValue(temp);
            }
            
        }
    }
}

// Getter

// Get number of regItems
int gReg::getSize() {
    return FEmem.size();
}
int gReg::getRegValue(int address, int subAddress) {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        if (FEmem[i]->getAddress() == address && FEmem[i]->getSubAddress() == subAddress) {
            return FEmem[i]->getValue();
        }
    }
    return -1;
}

int gReg::getRegValue(string name) {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        if (FEmem[i]->getName().compare(name) == 0) {
            return FEmem[i]->getValue();
        }
    }
    return -1;
}

// Setter
void gReg::setRegValue(int address, int subAddress, int value) {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        if (FEmem[i]->getAddress() == address && FEmem[i]->getSubAddress() == subAddress) {
            FEmem[i]->setValue(value);
        }
    }
}

void gReg::setRegValue(string name, int value) {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        if (FEmem[i]->getName().compare(name) == 0) {
            FEmem[i]->setValue(value);
        }
    }
}

void gReg::reset() {
    for (unsigned int i=0; i<FEmem.size(); i++) {
        FEmem[i]->reset();
    }
}
