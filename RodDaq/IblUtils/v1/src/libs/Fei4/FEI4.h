#ifndef __FEI4_H__
#define __FEI4_H__

/*
 * Original contribution from Timon Heim, University of Wuppertal (heim@physik.uni-wuppertal.de)
 * Date: 17.08.11
 * Edited by Karolos Potamianos (karolos.potamianos@cern.ch) on 08-X-2012
 * * Moving into IblRodUtils, de-coupling FEI4 commands from link commands
 */

#include "AbstrModule.h"
#include "RodModuleLink.h"

#include "gReg.h"
#include "pixData.h"

#define MODE_RUNMODE 0x38
#define MODE_CONFMODE 0x07

class FEI4: public AbstrModule, public RodModuleLink {
public:
	FEI4();//BOC *thatBOC, int chipID_arg, int BOCchannel_arg);
	~FEI4();

	void init();

	bool getRunMode();
	void setRunMode(bool mode); // true for run mode, false for config mode

	int getRegister(int address, int subAddress);
	int getRegister(int address);
	void setRegister(int address, int subAddress, int value);

	void printServiceRecordErrors();

	void globalReset();
	void globalPulse(int width);
	const std::vector<int>& readFrame() const;

	void calTrigger(int);
	int decodeConfRead(vector<int>);
	void decodeServiceRecord(const vector<int>&, unsigned int);
	
	// FEI4 fast cmds
	void trigger();
	void bcr();
	void ecr();
	void cal();
	// FEI4 slow cmds
	void wrRegister(int address, int value);
	void rdRegister(int address);
	void wrFrontEnd(unsigned int bitstream[21]);
	void runMode(bool mode);
	void setVerboseMode(bool mode = true) { gVerboseMode = mode; }

	void setChipID(int id);

protected:
	bool gVerboseMode;
private:
};

#endif // __FEI4_H__

