/*
 * Original contribution from Timon Heim, University of Wuppertal (heim@physik.uni-wuppertal.de)
 * Date: 17.08.11
 * Edited by Karolos Potamianos (karolos.potamianos@cern.ch) on 08-X-2012
 * * Moving into IblRodUtils
 */

#include "FEI4.h"
#include "SlaveTools.h"
#include "FEI4Tools.h"

#include <iomanip>
#include <iterator>
using namespace std;

// Constructor
FEI4::FEI4() { //BOC *thatBOC, int chipID_arg, int BOCchannel_arg)
	myGReg = new gReg();
	myGReg->setVerboseMode(false);
	//chipID = chipID_arg & 0x7;
	chipID = 0;
	//chipID = 6; // Geneva DC board: 6 & 7 (dead)
	logfile = new std::ofstream("fei4_log.bin", ios_base::out | ios_base::binary);

	serviceRecordCount.resize(32);

	serviceRecordMap[31] = "EFUSE_errorflag";
	serviceRecordMap[30] = "AddrErr";
	serviceRecordMap[29] = "CmdSeu";
	serviceRecordMap[28] = "BitFlip";
	serviceRecordMap[27] = "CmdErr";
	serviceRecordMap[26] = "AddrErr";
	serviceRecordMap[25] = "WrRegDataErr";
	serviceRecordMap[24] = "ErrorFlagb";
	serviceRecordMap[23] = "Ref2Fast";
	serviceRecordMap[22] = "FB2Fast";
	serviceRecordMap[21] = "RA2b";
	serviceRecordMap[10] = "HitOr";
	serviceRecordMap[9] = "Fifo_Full";
	serviceRecordMap[8] = "ReadOut_Processor_Error";
	serviceRecordMap[7] = "L1_Trig_Id_Error";
	serviceRecordMap[6] = "L1_Register_Error";
	serviceRecordMap[5] = "L1Req_Counter_Error";
	serviceRecordMap[4] = "L1In_Crounter_Error";
	serviceRecordMap[3] = "Hamming_Error2";
	serviceRecordMap[2] = "Hamming_Error1";
	serviceRecordMap[1] = "Hamming_Error0";
	serviceRecordMap[0] = "BC_Counter_Error";
}

FEI4::~FEI4() {
	delete myGReg;
	logfile->close();
	delete logfile;
}

// Inititalize the FEI4 with a standard configuration
void FEI4::init() {
	std::cout << __PRETTY_FUNCTION__ << ", chipID = " << chipID << std::endl;
	if(gVerboseMode) cout << __PRETTY_FUNCTION__ << endl;
	this->runMode(false);
	for (int i=0; i<32; i++) {
		int regValue = myGReg->getGRegValue(i);
		if (regValue != -1) {
			this->wrRegister(i, regValue);
			if (gVerboseMode)
				cout << "ADDR = " << i << "\tValue = 0x" << hex << regValue << dec << endl;
		}
	}
}

// Trigger the FE-I4
void FEI4::trigger() {
//	cout << __PRETTY_FUNCTION__ << endl;
//       myBOC->fillCmdFIFO(BOCchannel, 0x1d, 0);
	prepCommand(0x1D);
	sendCommand();
}

// Bunch Crossing counter reset 
void FEI4::bcr() {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x16);
	prepCommand(0x10);
	sendCommand();
}

// Event counter reset
void FEI4::ecr() {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x16);
	prepCommand(0x20);
	sendCommand();
}

// Calibration Pulse, width and delay set in gReg
void FEI4::cal() {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x16);
	prepCommand(0x40);
	sendCommand();
}

// Combined calibration pulse and trigger, needed for calibration scans
// real delay is given by = delay * 8 + 3
// The trig latency should then be = 255 - real delay
void FEI4::calTrigger(int delay) {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x16);
	prepCommand(0x40);
	for (int i=0; i<delay; i++) {
		prepCommand(0x00);
	}
	prepCommand(0x1D);
	sendCommand();
}

// Write a whole global register with a given value
void FEI4::wrRegister(int address, int value) {
//	cout << __PRETTY_FUNCTION__ << "address=0x" << hex << address << ", value=0x" << value << dec << endl;
	prepCommand(0x00);
	prepCommand(0x5A);
	prepCommand(0x0F&(8+((chipID>>2)&0x1)));
	prepCommand(((chipID<<6)&0xC0)+(address&0x3F));
	prepCommand((value>>8)&0xFF);
	prepCommand(value&0x00FF);
	prepCommand(0x00);
	sendCommand();
/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x0F&(8+((chipID>>2)&0x1))), 0);
	myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(address&0x3F)), 0);
	myBOC->fillCmdFIFO(BOCchannel, ((value>>8)&0xFF), 0);
	myBOC->fillCmdFIFO(BOCchannel, (value&0x00FF), 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
*/
}

// Read a whole global register
// Just sends the command, does not receive the data
void FEI4::rdRegister(int address) {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x00);
	prepCommand(0x5A);
	prepCommand(0x0F&(4+((chipID>>2)&0x1)));
	prepCommand(((chipID<<6)&0xC0)+(address&0x3F));
	sendCommand();
/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x0F&(4+((chipID>>2)&0x1))), 0);
	myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(address&0x3F)), 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->enableTransmitter(BOCchannel);
	usleep(10);
	myBOC->disableTransmitter(BOCchannel);
*/
}

// Write the Pixel Shift register
// 672 bits of data = 21 * 32 bit
void FEI4::wrFrontEnd(unsigned int bitstream[21]){
	cout << __PRETTY_FUNCTION__ << endl;

	prepCommand(0x00);
	prepCommand(0x5a);
	prepCommand(0x10+(0x0F&(0+((chipID>>2)&0x1))));
	prepCommand(((chipID<<6)&0xC0)+(0x0&0x3F));
	//Flipping the order in order to send bit 671-0, and not bit 31-0, 63-21, etc.
	for(int i = 20 ; i >= 0 ; --i) {
		prepCommand((bitstream[i]>>24)&0x000000FF);	
		prepCommand((bitstream[i]>>16)&0x000000FF);
		prepCommand((bitstream[i]>>8 )&0x000000FF);
		prepCommand((bitstream[i]>>0 )&0x000000FF);
	}
        prepCommand(0x00);
	sendCommand();

/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x10+(0x0F&(0+((chipID>>2)&0x1)))), 0);
	myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(0x0&0x3F)), 0);
	for (int i=0; i<21; i++) {
		myBOC->fillCmdFIFO(BOCchannel, ((bitstream[i]>>24)&0x000000FF), 0);
		myBOC->fillCmdFIFO(BOCchannel, ((bitstream[i]>>16)&0x000000FF), 0);
		myBOC->fillCmdFIFO(BOCchannel, ((bitstream[i]>>8 )&0x000000FF), 0);
		myBOC->fillCmdFIFO(BOCchannel, ((bitstream[i]>>0 )&0x000000FF), 0);
	}
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->enableTransmitter(BOCchannel);
	usleep(10);
	myBOC->disableTransmitter(BOCchannel);
*/
}

// Set FEI4 into run mode or conf mode
void FEI4::runMode(bool mode) {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x00);
	prepCommand(0x5A);
	prepCommand(0x20+(0x0F&(8+((chipID>>2)&0x1))));
	mode ? prepCommand(((chipID<<6)&0xC0)+(MODE_RUNMODE&0x3F)) : prepCommand(((chipID<<6)&0xC0)+(MODE_CONFMODE&0x3F));
	runModus = mode;
	prepCommand(0x00);
	sendCommand();
/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x20+(0x0F&(8+((chipID>>2)&0x1)))), 0);
	if (mode) {
		myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(MODE_RUNMODE&0x3F)), 0);
		runModus = true;

	} else {
		myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(MODE_CONFMODE&0x3F)), 0);
		runModus = false;
	}
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->enableTransmitter(BOCchannel);
	usleep(1);
	myBOC->disableTransmitter(BOCchannel); 
*/
}

// Reset FEI4 into intial state
// No idead what is resetet
void FEI4::globalReset() {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x00);
	prepCommand(0x5A);
	prepCommand(0x20+((chipID>>2)&0x1));
	prepCommand(((chipID<<6)&0xC0));
	prepCommand(0x00);
	sendCommand();
/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x20+((chipID>>2)&0x1)), 0);
	myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)), 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->enableTransmitter(BOCchannel);
	usleep(10);
	myBOC->disableTransmitter(BOCchannel);
*/
}

// Send a global pulse with given length
// Used to activate global pulse regs (#27)
void FEI4::globalPulse(int width) {
//	cout << __PRETTY_FUNCTION__ << endl;
	prepCommand(0x00);
	prepCommand(0x5A);
	prepCommand(0x20+(0x0F&(4+((chipID>>2)&0x1))));
	prepCommand(((chipID<<6)&0xC0)+(width&0x3F));
	prepCommand(0x00);
	sendCommand();
/*
	myBOC->disableTransmitter(BOCchannel);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x5a, 0);
	myBOC->fillCmdFIFO(BOCchannel, (0x20+(0x0F&(4+((chipID>>2)&0x1)))), 0);
	myBOC->fillCmdFIFO(BOCchannel, (((chipID<<6)&0xC0)+(width&0x3F)), 0);
	myBOC->fillCmdFIFO(BOCchannel, 0x00, 0);
	myBOC->enableTransmitter(BOCchannel);
	usleep(10);
	myBOC->disableTransmitter(BOCchannel); 
*/
}

// Read 1 frame of data from the ROD FIFO
const vector<int>& FEI4::readFrame() const {
//	cout << __PRETTY_FUNCTION__ << endl;
	answer.clear();
	uint8_t byte = readLink();
	uint8_t last_byte = 0;
	static int cnt(0);
	int same_count(0);
	if(0xFC == byte) {
		cnt = 0;
		//std::cout << "readLink(" << ++cnt << "): ";
		byte = readLink();
//there:
		int i = 0;
		while(!(0xBC==byte && (i%3)==0)) {
			i++;
			answer.push_back(0xFF & byte);
			//std::cout << "readLink(" << ++cnt << "): ";
			byte = readLink();
			if(byte == last_byte) same_count++;
			else { last_byte = byte; same_count = 0; }
			if(same_count>10) {
				std::cout << "Read the same output 10 times: breaking loop." << std::endl;
				answer.erase(answer.end()-9, answer.end()); // Removing 9 last values, because propably msg was too big for FIFO
				break;
			}
			
		}
		static int frameCounter(0);
		printf("[%03d] ", ++frameCounter);
		std::cout << "Frame(" << answer.size() << "): fc\t" << std::hex;
		for(size_t i = 0 ; i < answer.size() ; ++i) {
			//std::cout << answer[i] << "\t";
			printf("%02x\t", answer[i]);
			if((i%3)==2) std::cout << std::endl;
		}
/*
		size_t lineSize = 3;
		std::vector<int>::iterator it1 = answer.begin(), it2 = it1 + lineSize;
		while( it2 != answer.end() ) {
			std::copy(it1, it2, std::ostream_iterator<int>(std::cout, "\t"));
			if(it1!=answer.begin()) std::cout << std::endl;
			it1 = it2+1;
			it2 += lineSize+1;
			if(it2-answer.end() > 0 ) it2 = answer.end();
		}
*/
		//std::copy(answer.begin(), answer.end(), std::ostream_iterator<int>(std::cout, "\t"));
		std::cout << "bc" << std::dec << std::endl;

	} else {
		std::cout << "ERROR: Frame has no 0xFC" << std::endl;
		std::cout << "\t--->"; for(int i = 0 ; i < 10 ; ++i) std::cout << std::hex << (int)byte << std::dec << "\t"; std::cout << std::endl;
		//while( (byte = readLink()) != 0xFC ); goto there;	
	}

/*
	int temp = myBOC->readRodFIFO(BOCchannel); logfile->put(0xff & temp);
	if (temp == 0xfc) {
		temp = myBOC->readRodFIFO(BOCchannel); logfile->put(0xff & temp);
		int i = 0;
		while (!(temp == 0xbc && (i%3==0))) {
			i++;
			if (i > 5000) 
				break;
			answer.push_back(temp);
			temp = myBOC->readRodFIFO(BOCchannel); logfile->put(0xff & temp);
		}
	}
*/
	return answer;
}

// Decode a configuration read-back and return the value
int FEI4::decodeConfRead(vector<int> frame) {
//	cout << __PRETTY_FUNCTION__ << endl;
	if (myGReg->getRegValue(2, 1) == 1 && frame.size() == 6) {
		return ((frame[4]<<8)+frame[5]);
	} else if (myGReg->getRegValue(2, 1) == 0 && frame.size() == 3) {
		return ((frame[2]<<8)+frame[3]);
	} else {
		return -1;
	}
}

void FEI4::decodeServiceRecord(const vector<int>& frame, unsigned int i) {
//	cout << __PRETTY_FUNCTION__ << endl;
	if (frame[i] == 0xef && frame.size() == i+3) {
		int code = ((frame[i+1]&0xfc)>>2);
		int number = (((frame[i+1]&0x03)<<8) + (frame[i+2]&0xff));
		serviceRecordCount[code] += number;
		cout << "Service Record: [" << code << "], i.e. " << serviceRecordMap[code] << " CNT: " << number << endl;
	}
}

void FEI4::printServiceRecordErrors()
{
//	cout << __PRETTY_FUNCTION__ << endl;
	std::cout << "Service record count:\n";
	for (size_t i=0; i < serviceRecordCount.size(); ++i) {
		if (serviceRecordCount[i] > 0)
			std::cout << std::setw(24) << serviceRecordMap[i] << ":\t" << serviceRecordCount[i] << "\n";
	}
}

// Set/Get the Run mode of the FEI4
bool FEI4::getRunMode() {
//	cout << __PRETTY_FUNCTION__ << endl;
	return runModus;
}

void FEI4::setRunMode(bool mode) {
//	cout << __PRETTY_FUNCTION__ << endl;
	this->runMode(mode);
}

// Set/Get a register value
int FEI4::getRegister(int address, int subAddress)
{
//	cout << __PRETTY_FUNCTION__ << endl;
	//getRegister(address); // update gReg copy
	return myGReg->getRegValue(address, subAddress);
}

int FEI4::getRegister(int address)
{
	cout << __PRETTY_FUNCTION__ << ": address=0x" << hex << address << dec << endl;
	this->rdRegister(address);
	int val = this->decodeConfRead(this->readFrame());
	cout << "FEI4::getRegister() output: 0x" << hex << val << dec <<endl;
	// Update the gReg
	myGReg->setGRegValue(address, val);

	return val;
}

void FEI4::setRegister(int address, int subAddress, int value) {
//	//cout << __PRETTY_FUNCTION__ << "address=0x" << hex << address << ", subAddress=0x" << subAddress << ", value=0x" << value << dec << endl;
	//if (myGReg->getRegValue(address, subAddress) != value) {
		myGReg->setRegValue(address, subAddress, value);
		this->wrRegister(address, myGReg->getGRegValue(address));
		//readRegister(this->getRod(), 1, address);

		//sleep(1);
		//getRegister(address);

	//}
}

void FEI4::setChipID(int id) {
	chipID = id;
}
