#ifndef __FEI4_TOOLS_H__
#define __FEI4_TOOLS_H__

#include "AbstrRodModule.h"
using SctPixelRod::AbstrRodModule;

void enterConfigMode(AbstrRodModule* rod0);
void enterRunMode(AbstrRodModule* rod0);
void WriteReg(AbstrRodModule* rod0, unsigned char reg, unsigned short value);
void ReadRegister(AbstrRodModule* rod0, unsigned char reg);
void sendL1A(AbstrRodModule* rod0);

#define WREG 0x005A0800
#define READREG 0x005A0400
#define CONFMODE 0x0168A01C
#define RUNMODE 0x0168A0E0
#define LV1 0x00001D00

void setSlaveReadBack(AbstrRodModule* rod0, int slave, int channel = 0);
void configureModule(AbstrRodModule *m_rod, unsigned int moduleMask, unsigned int bitMask);


void configRegisters(AbstrRodModule* rod0, int slave = 0);
void readBackRegisters(AbstrRodModule* rod0, int slave = 0);
void readRegister(AbstrRodModule *rod0, int slave, unsigned int address);


#include <FEI4.h>
#include <pixData.h>
pixData* decodePixData(FEI4& fei4, const vector<int>& frame);
int decodeToT(FEI4& fei4, int ToT);

int initPixelLabIblSetupByMask(FEI4& fei4_module, int slave, uint8_t tx_mask, uint16_t rx_mask, int chipID, bool isOptical = true);
int initPixelLabIblSetup(FEI4& fei4_module, int slave = 0, int tx_ch = 1, int rx_ch = 0, int chipID = 0, bool isOptical = true);
int disableAllChannels(AbstrRodModule* rod0, int slave);

TH2F* digitalInjectionForHistogrammer(FEI4& fei4, int calpulse);
void digitalInjectionForHistogrammer(FEI4& fei4, TH2F* hist,int calpulse);
//void digitalInjectionForHistogrammer(FEI4& fei4, int ntrigger);

TH2F* analogInjectionForHistogrammer(FEI4& fei4);
void analogInjectionForHistogrammer(FEI4& fei4, TH2F* hist);
//void analoglInjectionForHistogrammer(FEI4& fei4, int ntrigger);

#endif // __FEI4_TOOLS_H__

