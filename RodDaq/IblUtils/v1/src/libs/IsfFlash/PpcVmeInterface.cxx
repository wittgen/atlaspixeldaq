//File: PpcVmeInterface.cxx
// $Header$



#include "PpcVmeInterface.h"

//! Namespace for the common routines for SCT and PIXEL ROD software.
namespace SctPixelRod {

static long endianSwap = 0;


//******************************Class HpiException**************************
//
// Description:
//  This class is thrown if an error in an HPI read/write operation is detected.
//   
//  Author(s):
//    Tom Meyer (meyer@iastate.edu) - originator

//	Constructors. Use defaults for destructor, copy, and assignment.

HpiException::HpiException( std::string descriptor, unsigned long calcAddr,
                            unsigned long readAddr) : BaseException(descriptor) {
  m_calcAddr = calcAddr;
  m_readAddr = readAddr;
  setType(HPI);
  }

void HpiException::what(std::ostream& os) const {
    os << "HpiException: " << getDescriptor() << std::endl;
    os << "Calculated Address:" << std::hex << getCalcAddr() << std::endl;
    os << "Read Address:" << std::hex << getReadAddr() << std::endl;
}  
//***************************Class RodException**************************
//
// Description:
//  This class is thrown if an error in a ROD operation is detected.
//   
//  Author(s):
//    Tom Meyer (meyer@iastate.edu) - originator

//	Constructors. Use defaults for destructor, copy, and assignment.

RodException::RodException( std::string descriptor) : BaseException(descriptor) {
  m_numData = 0;
  m_data1 = m_data2 = 0;
  setType(ROD);
  }
RodException::RodException( std::string descriptor, unsigned long data1) :
                            BaseException(descriptor) {
  m_numData = 1;
  m_data1 = data1;
  m_data2 = 0;
  setType(ROD);
  }
RodException::RodException( std::string descriptor, unsigned long data1,
                            unsigned long data2) : BaseException(descriptor) {
  m_numData = 2;
  m_data1 = data1;
  m_data2 = data2;
  setType(ROD);
  }

void RodException::what(std::ostream& os) const {
  unsigned long numData;
  numData = getNumData();
  os << "RodException: " << getDescriptor() << std::endl;
  if (0 == numData) return;
  os << "Data1:" << getData1() << std::endl;
  if (1 == numData) return;
  os << "Data2:" << getData2() << std::endl;
}  
//********************************Class PpcVmeInterface***************************
//
// Description:
//  This is a derived class providing the software interface for VME ROD modules.
//   
//  Author(s):
//    

//---------------------------------Constructor------------------------------                                    /*
PpcVmeInterface::PpcVmeInterface( unsigned long baseAddr, unsigned long mapSize, 
				  VmeInterface & ourInterface) { 


  try {
      m_myVmePort = new VmePort(baseAddr , mapSize, VmeInterface::A32, 
				ourInterface); 
    
  }
  catch (std::bad_alloc & ba) {
    throw RodException("Failed to get VME Port.");
  }
m_myVmePort->setExceptionTrapping(true);

  /*  m_slot = baseAddr>>24;
  m_serialNumber = 0xFFFFFFFF;  // Will be overwritten during init
  m_revision = 0;               // Will be overwritten during init
  m_numSlaves= numSlaves; 
  m_finBufferSize = 4096; 
  m_masterImageName = std::string("");
  m_myOutList = 0;
  m_vmeCommandReg = 0;
  m_rodStatusReg[0] = 0;
  m_rodStatusReg[1] = 0;
  m_debug = 0;
  m_debugFile = "rodprimitives.bin";
  m_textData = 0;
  m_btFailure = false;

  try {
    if (m_mapSize > 0xc00000) {
      unsigned long nexcl = 3;
      unsigned long exclS[3] = {   0x1000, 0x201000, 0x800000 };
      unsigned long exclE[3] = { 0x1fffff, 0x3fffff, 0xbfffff };
      m_myVmePort = new VmePort(m_baseAddress, m_mapSize, nexcl, exclS, exclE, 
				VmeInterface::A32, m_ourInterface); 
    } else {
      m_myVmePort = new VmePort(m_baseAddress, m_mapSize, VmeInterface::A32, 
				m_ourInterface); 
    }
  }
  catch (std::bad_alloc & ba) {
    throw RodException("Failed to get VME Port.");
  }

  m_myOutList = 0;

  memset(&m_masterXface, 0, sizeof(m_masterXface));

  m_myPrimState = RodPrimList::IDLE;

  // Make the vme library generate exceptions on bus errors
  getVmePort()->setExceptionTrapping(true);

  m_myBoc = NULL;
  if (bocCheck()==1) {
    m_myBoc = new BocCard(*this);
    }*/
}

//---------------------------------Destructor------------------------------                                    
/*
Be sure to delete all VME Ports and other "new" things in constructor.
*/

PpcVmeInterface::~PpcVmeInterface() {
   delete(m_myVmePort); m_myVmePort = 0;
     /*
  if (m_myBoc != NULL) delete m_myBoc; 
    
  if (m_textData){ 
      for (unsigned int i=0; i< n_textbuffers; i++) {
          if (m_textData[i]) delete [](m_textData[i]);
      }  
      delete []m_textData;
      m_textData=NULL;
};  }
  return;*/
}

//----------------------------------hpiLoad------------------------------------                                

void PpcVmeInterface::hpiLoad(unsigned long hpiReg, unsigned long hpiValue) 
      {

  if (endianSwap) hpiValue = endianReverse32(hpiValue); 
  m_myVmePort->write32(hpiReg, hpiValue);
}

//--------------------------------hpiFetch-------------------------------------                                

unsigned long PpcVmeInterface::hpiFetch(unsigned long hpiReg) 
              {

  unsigned long hpiValue;

  hpiValue=m_myVmePort->read32(hpiReg);
  if (endianSwap) hpiValue = endianReverse32(hpiValue);
  
  return hpiValue;
}

//-----------------------------mdspSingleRead---------------------------------                                

unsigned long PpcVmeInterface::ppcSingleRead(const unsigned long ppcAddr) 
               {
  unsigned long value; 

//  Load the DSP address into the HPIA register 
  if(ppcAddr != _current_hpia)hpiLoad(HPIA, ppcAddr);
  _current_hpia = ppcAddr;

//	Do the read 
  value = hpiFetch(HPID_NOAUTO);
  if (endianSwap) {
    value = endianReverse32(value);
  }
  return value;
}

//------------------------------mdspSingleWrite------------------------------------                                   

void PpcVmeInterface::ppcSingleWrite(unsigned long ppcAddr, unsigned long buffer) 
     { 

//  Load the HPID address into the HPIA register 
  if(ppcAddr != _current_hpia)hpiLoad(HPIA, ppcAddr);
 _current_hpia = ppcAddr;
//	Do the write 
  if (endianSwap) {
      buffer = endianReverse32(buffer);
  }

  hpiLoad(HPID_NOAUTO,buffer);

  return;
     }


unsigned long PpcVmeInterface::endianReverse32(const unsigned long inVal) {
    unsigned long outVal;
    outVal  = (inVal & 0x000000ff) << 24;
    outVal |= (inVal & 0x0000ff00) << 8;
    outVal |= (inVal & 0x00ff0000) >> 8;
    outVal |= (inVal & 0xff000000) >> 24;
    return outVal;
}



}
