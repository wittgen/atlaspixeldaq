//File: RodVmeAddresses.h

#ifndef SCTPIXELROD_PPCVMEADDRESSES_H 
#define SCTPIXELROD_PPCVMEADDRESSES_H



namespace SctPixelRod {

#include "vmeAddressMap.h"

  const unsigned long PPC_HPI_OFFSET = 0x100000;
// Give some shorter aliases to the HPI registers
  const unsigned long HPIC = HPI_CONTROL_REG_REL_ADDR + PPC_HPI_OFFSET;
  const unsigned long HPIA = HPI_ADDRESS_REG_REL_ADDR + PPC_HPI_OFFSET;
  const unsigned long HPID_AUTO = HPI_DATA_REG_WITH_AUTOINC_REL_ADDR + PPC_HPI_OFFSET;
  const unsigned long HPID_NOAUTO = HPI_DATA_REG_WITHOUT_AUTOINC_REL_ADDR + PPC_HPI_OFFSET;


}

#endif
