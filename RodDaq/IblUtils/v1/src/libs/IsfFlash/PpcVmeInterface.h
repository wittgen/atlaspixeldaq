#ifndef SCTPIXELROD_PPCVMEINTERFACE_H 
#define SCTPIXELROD_PPCVMEINTERFACE_H  

#include <fstream> 
#include <iostream>
#include <string>
#include <ctime>     // Pick up clock() function and clock_t typedef
#include <new>
#include <unistd.h>

#include "BaseException.h"

#include "VmeInterface.h" 
#include "VmePort.h"
#include "PpcVmeAddresses.h"



namespace SctPixelRod {

enum HpidMode {DYNAMIC, AUTO, NO_AUTO};

//------------------------------HpiException--------------------------------- 

/*! @class HpiException
 *
 *  @brief This is an exception class for Host Port Interface errors.
 *  It inherits from BaseException, adding the expected and actual addresses.

 *  This class is thrown if an error in an HPI read/write operation is detected.
 *
 *  @author Tom Meyer (meyer@iastate.edu) - originator
 */

class HpiException : public BaseException {
public:
  HpiException( std::string descriptor, unsigned long calcAddr, unsigned long readAddr);
  unsigned long getCalcAddr() const {return m_calcAddr;};
  unsigned long getReadAddr() const {return m_readAddr;};
  virtual void what(std::ostream&) const;

private:
  unsigned long m_calcAddr;    // The calculated (expected) address
  unsigned long m_readAddr;    // The address actually read
};  
                                                                          
//------------------------------RodException------------------------------ 

/*! @class rodException
 *
 *  @brief This is a general exception class for ROD errors.
 *
 *  This class is thrown if an error in a ROD operation is detected.
 *  It inherits from BaseException, adding zero, one, or two parameters.
 *
 *  @author Tom Meyer (meyer@iastate.edu) - originator
 */
class RodException : public BaseException {
public:
  RodException( std::string descriptor);
  RodException( std::string descriptor, unsigned long data1);
  RodException( std::string descriptor, unsigned long data1, unsigned long data2);
  unsigned long getData1() const {return m_data1;};
  unsigned long getData2() const {return m_data2;};
  unsigned long getNumData() const {return m_numData;};
  virtual void what(std::ostream&) const;
  
private:
  unsigned long m_numData;  // Number of data values returned
  unsigned long m_data1;    // First data value returned
  unsigned long m_data2;    // Second data value returned
};                                                                            

 class PpcVmeInterface 
 {

 public:
  PpcVmeInterface( unsigned long baseAddr, unsigned long mapSize, VmeInterface & ourInterface) 
    ;
  virtual ~PpcVmeInterface();                                  // Destructor

/*! hpiLoad() loads a 32-bit value into the HPIA or HPID register.
*/
  void hpiLoad(const unsigned long hpiReg, const unsigned long hpiValue) 
    ;

/*! hpiFetch() fetches a 32-bit value from one of the HPI registers. 
*/
  unsigned long hpiFetch(const unsigned long hpiReg) ;

/*! endianReverse32( reverses the endian-ness of a 32 bit value
*/
  unsigned long endianReverse32(const unsigned long inVal);

  unsigned long ppcSingleRead(const unsigned long ppcAddr) ;

                                

  void ppcSingleWrite(unsigned long ppcAddr, unsigned long buffer) ;


 private:
  VmePort* m_myVmePort;              //!< VME Port handle
  unsigned long _current_hpia;
 };// End of class PpcVmeInterface  declaration


} //  End namespace SctPixelRod



#endif
