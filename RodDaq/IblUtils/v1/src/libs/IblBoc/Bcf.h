#ifndef BCF_H
#define BCF_H

#include "AddressMap.h"
#include <string>
#include <cstdint>

class Boc;

class Bcf {
	public:
		void Init();
		Bcf(Boc& b);


		void SetLaserOn(bool on);
		void JtagOverride(bool on);
		void WritePllRegister(unsigned int address, unsigned int value);
		unsigned int ReadPllRegister(unsigned int address);
		unsigned int ResetPll();
		bool IsPllLocked();
		unsigned int GetIdelay();
		void SetIdelay(unsigned int value);
		void FlashBmf(Addr::Bmf::Location loc, const std::string &filename);
		void FastFlashBmf(Addr::Bmf::Location loc, const std::string &filename);
		void WriteRegister(boost::uint16_t Reg, boost::uint8_t value);
		boost::uint8_t ReadRegister(uint16_t Reg);
		bool ProgIsBusy(Addr::Bmf::Location loc);
	private:
		friend class Boc;

		inline void SetProgFlash(Addr::Bmf::Location loc, int value);
		inline uint32_t ReadCrc(Addr::Bmf::Location loc);
		Boc& boc;
};

#endif

