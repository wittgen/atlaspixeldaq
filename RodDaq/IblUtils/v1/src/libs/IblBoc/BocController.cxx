#include "BocController.h"

#include <iostream>
using namespace std;

BocController::BocController(): Boc("192.168.0.10"), bmfNorth(GetBmf(Addr::Bmf::North)), bmfSouth(GetBmf(Addr::Bmf::North)) {
	bmf[0] = &bmfNorth;
	bmf[1] = &bmfSouth;
	for(int slave = 0 ; slave <2 ; ++slave) {
		// Only first channel (ch0) for now.
		txChan[slave][0] = new TxChannel(0, *bmf[slave]);
		rxChan[slave][0] = new RxChannel(0, *bmf[slave]);
		setupTx(txChan[slave][0]);
		setupRx(rxChan[slave][0]);
	}
}

BocController::~BocController() {
	// Still need to cleanup properly
        for(int slave = 0 ; slave <2 ; ++slave) {
		stopRx(slave, 0);
		stopTx(slave, 0);
	}
}

void BocController::sendIdle(TxChannel* txChPtr, unsigned int count) {
	while(count--!=0) txChPtr->FillFifo(0x3C, true); 
}

void BocController::sendFrame(TxChannel* txChPtr, uint8_t *frame, bool kWord) {
	sendIdle(txChPtr, 9);
	while(*frame != 0xBC) txChPtr->FillFifo(*(frame++), kWord);
	txChPtr->FillFifo(*frame, kWord); // For the 0xBC
	sendIdle(txChPtr, 9);
	txChPtr->SendFromFifo(true);
}

// Should fit in TxChannel and RxChannel 
void BocController::setupTx(TxChannel* txChPtr) {
	// Config
	txChPtr->DisableBpm(); // No BPM
	txChPtr->InputSelect(true); // Injection on BOC
	txChPtr->SendRawData(false); // 8b10b encoding on
	txChPtr->EnableTransmitter(Addr::Tx::MHz160); // 160MHz
}

// Should fit in TxChannel and RxChannel 
void BocController::setupTx(int slave, int tx_ch) {
	setupTx(txChan[slave][tx_ch]);
}

// Should fit in TxChannel and RxChannel 
void BocController::setupRx(RxChannel* rxChPtr) {
	rxChPtr->SetLoopbackMode(true); // Internal (True) or external (Flase) loopback
	rxChPtr->SetMonitorRawMode(false);
	rxChPtr->SetAllowIdle(false);
	rxChPtr->SetMux(true);
	rxChPtr->DrainMonitorFifo();
	rxChPtr->SetMonitor(true);
	rxChPtr->Enable();
}

// Should fit in TxChannel and RxChannel 
void BocController::setupRx(int slave, int rx_ch) {
	setupRx(rxChan[slave][rx_ch]);
}

// Should fit in TxChannel and RxChannel 
void BocController::stopTx(TxChannel *txChPtr) {
	txChPtr->LoopFifo(false);
	usleep(500);
	txChPtr->SendFromFifo(false);
	txChPtr->DisableTransmitter();
}

// Should fit in TxChannel and RxChannel 
void BocController::stopTx(int slave, int tx_ch) {
	stopTx(txChan[slave][tx_ch]);
}

// Should fit in TxChannel and RxChannel 
void BocController::stopRx(RxChannel *rxChPtr) {
	rxChPtr->Disable();
}

// Should fit in TxChannel and RxChannel 
void BocController::stopRx(int slave, int rx_ch) {
	stopRx(rxChan[slave][rx_ch]);
}

// Not very nice currently
void BocController::sendVirtualFrame(int slave, int tx_ch, uint8_t *frame) {
	TxChannel *txChPtr = txChan[slave][tx_ch];
	txChPtr->LoopFifo(false);
        // This is a hard-coded frame for testing purposes
	// Don't forget the 0xBC in the frame.
	uint8_t testFrame[] = { 0xFC, 0xE9, 0x0F, 0xFF, 0x02, 0x01, 0x55, 0xBC };
	if(!frame) frame = testFrame;
	sendFrame(txChPtr, frame, true);
	txChPtr->LoopFifo(true);
}

void BocController::stopBocLoop(int slave, int tx_ch) {
	TxChannel *txChPtr = txChan[slave][tx_ch];
	stopTx(txChPtr);
}

void BocController::readFrame(int slave, int rx_ch) {
	RxChannel *rxChPtr = rxChan[slave][rx_ch];
	if(!rxChPtr){ cerr << "RxChannel " << rx_ch << " on slave " << slave << " not yet initialized." << endl; return; }
	uint8_t data = rxChPtr->ReadData();
	while(data != 0xFC) {
		static int cnt(0);
		if(cnt++>10) break;
		cout << hex << (int) data << dec << "\t";
		data = rxChPtr->ReadData();
	}
}

