#include "AddressMap.h"
#include "Boc.h"
#include "Bmf.h"
#include "TxChannel.h"
#include "RxChannel.h"
#include "VirtualTx.h"
#include "VirtualRx.h"

#include <map>
using std::map;

class BocController: public Boc {
	// Too lazy to fix boclib
	Bmf bmfNorth, bmfSouth;
	Bmf *bmf[2];
	map<int, map<int,TxChannel*> > txChan;
	map<int, map<int,RxChannel*> > rxChan;

	// These should be made static at some point
	// Do we want them public as well ?
	void setupTx(TxChannel* txChPtr);
	void stopTx(TxChannel *txChPtr);
	void setupRx(RxChannel* rxChPtr);
	void stopRx(RxChannel *rxChPtr);
	void sendIdle(TxChannel* txChPtr, unsigned int count);
	void sendFrame(TxChannel* txChPtr, uint8_t *frame, bool kWord);


public:
	BocController();
	~BocController();

	void setupTx(int slave, int tx_ch);
	void stopTx(int slave, int tx_ch);
	void setupRx(int slave, int tx_ch);
	void stopRx(int slave, int tx_ch);

	void sendVirtualFrame(int slave, int tx_ch, uint8_t *frame = nullptr);
	void stopBocLoop(int slave, int tx_ch);
	void readFrame(int slave, int rx_ch);


};
