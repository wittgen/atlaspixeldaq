#include "Boc.h"

//#include <regex>
#include <stdexcept>

#include "boost/lexical_cast.hpp"
#include <array>


using boost::uint32_t;
using boost::uint16_t;
using boost::uint8_t;
using boost::asio::ip::udp;
using std::tr1::shared_ptr;

union ipbus_header_t
{
    uint32_t header_32;

    uint8_t header_8[4];

    struct  
    {
        unsigned int vers     :  4;
        unsigned int trans_id : 11;
        unsigned int words    :  9;
        unsigned int type     :  5;
        unsigned int d        :  1;
        unsigned int res      :  2;
    } trans;
};


Boc::Boc(){
	std::cout <<"ctor begin\n";
	connected = false;
	deadline_ = shared_ptr<deadline_timer> (new deadline_timer(io_service));
	deadline_->expires_at(boost::posix_time::pos_infin);
	check_deadline();
	std::cout << "ctor end\n";
}


Bcf Boc::GetBcf()
{
    Bcf bcf(*this);
    return bcf;
}

Bmf Boc::GetBmf(Addr::Bmf::Location loc)
{
    Bmf bmf(*this, loc);
    return bmf;
}

Boc::Boc(const std::string& ip) 
{
    connected = false;
    deadline_ = shared_ptr<deadline_timer> (new deadline_timer(io_service));
    deadline_->expires_at(boost::posix_time::pos_infin);
    check_deadline();
    bocAddr = ip;
    port = 50000;
    Connect();
}

void Boc::SetIp(std::string& ip) 
{
    bocAddr = ip;
    port = 50000;
    Connect();
}


Boc::~Boc()
{
    Disconnect();
}

static void print_header(ipbus_header_t *header)
{
#ifdef DEBUG
    printf("------------------------------------------------------------\n");
    printf("Version: %d\n", header->vers);
    printf("Transaction-ID: %03X\n", header->trans_id);
    printf("Words: %d\n", header->words);
    printf("Type: %02X\n", header->type);
    printf("Direction: %s\n", (header->d == 0) ? "to BOC" : "from BOC");
    switch(header->res)
    {
        case 0: printf("Result: OK\n"); break;
        case 1: printf("Result: PARTIAL\n"); break;
        case 2: printf("Result: FAIL\n"); break;
        case 3: printf("Result: RESERVED\n"); break;
        default: printf("Result: undefined\n"); break;
    }
    printf("------------------------------------------------------------\n");
#else
    (void)header;
#endif
}

static void header2bytes(ipbus_header_t *header, unsigned char *buf)
{
    uint32_t tmp;

    tmp = (header->trans.vers << 28) |
          (header->trans.trans_id << 17) |
          (header->trans.words << 8) |
          (header->trans.type << 3) |
          (header->trans.d << 2) |
          (header->trans.res << 0);

    buf[3] = (tmp & 0xFF000000) >> 24;
    buf[2] = (tmp & 0x00FF0000) >> 16;
    buf[1] = (tmp & 0x0000FF00) >> 8;
    buf[0] = (tmp & 0x000000FF);
}


static void bytes2header(ipbus_header_t *header, unsigned char* buf)
{
    uint32_t tmp = 0;

    tmp = (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];

    header->trans.vers = ((tmp & 0xF0000000) >> 28);
    header->trans.trans_id = ((tmp & 0x0FFE0000) >> 17);
    header->trans.words = ((tmp & 0x0001FF00) >> 8);
    header->trans.type = ((tmp & 0x000000F8) >> 3);
    header->trans.d = ((tmp & 0x00000004) >> 2);
    header->trans.res = (tmp & 0x00000003);
}

void Boc::Disconnect() 
{
    connected = false;
    //close(socketfd);

    socket.reset();
}


void Boc::Connect() 
{
    
    udp::resolver resolver(io_service);
    std::string _port = boost::lexical_cast<std::string> (port);
    udp::resolver::query query (udp::v4(),bocAddr,_port);
    endpoint = std::tr1::shared_ptr<udp::endpoint> (new udp::endpoint(*resolver.resolve(query)));
    
    
    socket = std::tr1::shared_ptr<udp::socket> (new udp::socket(io_service));
    socket->open(udp::v4());

    

    connected = true;
}


void Boc::SingleWrite(boost::uint16_t addr, boost::uint8_t value)
{
    if (!connected) {
        throw std::runtime_error("Not connected");
        return;
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[12];
    uint8_t rxdata[4];

    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = 1;
    txheader.trans.type = 0x04;
    txheader.trans.d = 0;
    txheader.trans.res = 0;


    // copy header into byte-array
    header2bytes(&txheader, txdata);

    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;
    
    // copy data into bytearray
    txdata[11] = 0;
    txdata[10] = 0;
    txdata[9] = 0;
    txdata[8] = value;
    
    // send
    //send(socketfd, txdata, 12, 0);
    std::array<uint8_t, 12>  send_ary;
    for (int i = 0; i < 12; i++) {
      send_ary[i] = txdata[i];
    }

    socket->send_to(boost::asio::buffer(send_ary), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 4, 0);
    std::array<char, 128> recv_buf;
    udp::endpoint sender_endpoint;
   /* socket->receive(
        boost::asio::buffer(recv_buf), sender_endpoint);*/

    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);

    if(ec) {
         std::cout << "Receive error: " << ec.message() << "\n";
    }

    for (int i = 0; i < 4; i++) {
      rxdata[i] = recv_buf[i];
    }
    
    // copy into header
    bytes2header(&rxheader, rxdata);
#ifdef BOCDEBUG
    std::cout << "Write to 0x" << std::hex << addr << ": 0x" << (0xff & value) << std::endl;
#endif
    transaction_id = transaction_id + 1;
}



int Boc::NonIncWrite(boost::uint16_t addr, boost::uint8_t *value, boost::uint32_t words)
{
    if (!connected) {
        throw std::runtime_error("Not connected");
        return 1;
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[256];
    uint8_t rxdata[4];
    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = words;
    txheader.trans.type = 0x09;
    txheader.trans.d = 0;
    txheader.trans.res = 0;
    
    
    
    // copy header into byte-array
    header2bytes(&txheader, txdata);
    
    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;
    
    std::copy(value,value+words*4,txdata+8);

    
    // send
    //send(socketfd, txdata, 12, 0);
    //boost::array<uint8_t, 256>  send_ary;
    //for (int i = 0; i < 8+4*words; i++) {
    //    send_ary[i] = txdata[i];
    //}
    
    socket->send_to(boost::asio::buffer(txdata, 8+4*words), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 4, 0);
    boost::array<char, 256> recv_buf;
    udp::endpoint sender_endpoint;
    /* socket->receive(
     boost::asio::buffer(recv_buf), sender_endpoint);*/
    
    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);
    
    if(ec) {
        std::cout << "Receive error: " << ec.message() << "\n";
    }
    
    for (int i = 0; i < 4; i++) {
        rxdata[i] = recv_buf[i];
    }
    
    // copy into header
    bytes2header(&rxheader, rxdata);
#ifdef BOCDEBUG
    std::cout << "Write to 0x" << std::hex << addr << ": 0x" << (0xff & value) << std::endl;
#endif
    transaction_id = transaction_id + 1;
    
    return 0;
}

int Boc::NonIncWrite(boost::uint16_t addr, char *value, boost::uint32_t words)
{
    if (!connected) {
        throw std::runtime_error("Not connected");
        return 1;
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[256];
    uint8_t rxdata[4];
    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = words;
    txheader.trans.type = 0x09;
    txheader.trans.d = 0;
    txheader.trans.res = 0;
    
    
    
    // copy header into byte-array
    header2bytes(&txheader, txdata);
    
    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;
        
    std::copy(value,value+words*4,txdata+8);
    
    // send
    //send(socketfd, txdata, 12, 0);
    //boost::array<uint8_t, 256>  send_ary;
    //for (int i = 0; i < 8+4*words; i++) {
    //    send_ary[i] = txdata[i];
    //}
    
    socket->send_to(boost::asio::buffer(txdata, 8+4*words), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 4, 0);
    boost::array<char, 256> recv_buf;
    udp::endpoint sender_endpoint;
    /* socket->receive(
     boost::asio::buffer(recv_buf), sender_endpoint);*/
    
    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);
    
    if(ec) {
        std::cout << "Receive error: " << ec.message() << "\n";
    }
    
    for (int i = 0; i < 4; i++) {
        rxdata[i] = recv_buf[i];
    }
    
    // copy into header
    bytes2header(&rxheader, rxdata);
#ifdef BOCDEBUG
    std::cout << "Write to 0x" << std::hex << addr << ": 0x" << (0xff & value) << std::endl;
#endif
    transaction_id = transaction_id + 1;
    
    return 0;
}

boost::uint8_t Boc::SingleRead(boost::uint16_t addr)
{
     if (!connected) {
        throw std::runtime_error("Not connected");
        return 0;
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[8];
    uint8_t rxdata[8];

    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = 1;
    txheader.trans.type = 0x03;
    txheader.trans.d = 0;
    txheader.trans.res = 0;

    // print header
    print_header(&txheader);

    // copy header into byte-array
    header2bytes(&txheader, txdata);

    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;

    // send
    //send(socketfd, txdata, 8, 0);
    boost::array<uint8_t, 8>  send_ary;
    for (int i = 0; i < 8; i++) {
      send_ary[i] = txdata[i];
    }

    socket->send_to(boost::asio::buffer(send_ary), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 8, 0);
    boost::array<char, 256> recv_buf;
    udp::endpoint sender_endpoint;
    /*socket->receive_from(
        boost::asio::buffer(recv_buf), sender_endpoint);*/
    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);

    if(ec) {
         throw std::runtime_error(ec.message());
         exit(1);
    }


    for (int i = 0; i < 8; i++) {
      rxdata[i] = recv_buf[i];
    }
    // copy into header
    bytes2header(&rxheader, rxdata);

    // print header
    print_header(&rxheader);

    transaction_id = transaction_id + 1;


#ifdef BOCDEBUG
    std::cout << "Read from 0x" << std::hex << addr << ": 0x" << (0xff & rxdata[4]) << std::endl;
#endif
    return rxdata[4];
}

int Boc::NonIncRead(boost::uint16_t addr, boost::uint8_t* data, boost::uint32_t words)
{
    if (!connected) {
        throw std::runtime_error("Not connected");
        exit(1);
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[8];
    uint8_t rxdata[4];

    
    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = words;
    txheader.trans.type = 0x08;
    txheader.trans.d = 0;
    txheader.trans.res = 0;
    
    // print header
    print_header(&txheader);
    
    // copy header into byte-array
    header2bytes(&txheader, txdata);
    
    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;
    
    // send
    //send(socketfd, txdata, 8, 0);
    boost::array<uint8_t, 8>  send_ary;
    for (int i = 0; i < 8; i++) {
        send_ary[i] = txdata[i];
    }
    
    socket->send_to(boost::asio::buffer(send_ary), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 8, 0);
    //boost::array<char, 256> recv_buf;
    uint8_t recv_buf[256];
    udp::endpoint sender_endpoint;
    /*socket->receive_from(
     boost::asio::buffer(recv_buf), sender_endpoint);*/
    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);
    
    if(ec) {
        throw std::runtime_error(ec.message());
        exit(1);
        
    }
    
    
    for (int i = 0; i < 4; i++) {
        rxdata[i] = recv_buf[i];
    }
    
    // copy into header
    bytes2header(&rxheader, rxdata);
    
    // print header
    print_header(&rxheader);
    
    for (int i = 0; i < words; i++) {
        data[i] = recv_buf[4+i];
    }
    transaction_id = transaction_id + 1;
    
    if (rxheader.trans.words != words) {
        return 1;
    }
    
#ifdef BOCDEBUG
    std::cout << "Read from 0x" << std::hex << addr << ": 0x" << (0xff & rxdata[4]) << std::endl;
#endif
    return 0;
}

int Boc::NonIncRead(boost::uint16_t addr, char* data, boost::uint32_t words)
{
    if (!connected) {
        throw std::runtime_error("Not connected");
        exit(1);
    }
    ipbus_header_t txheader;
    ipbus_header_t rxheader;
    uint8_t txdata[8];
    uint8_t rxdata[4];
    
    
    // prepare packet
    txheader.trans.vers = 1;
    txheader.trans.trans_id = transaction_id;
    txheader.trans.words = words;
    txheader.trans.type = 0x08;
    txheader.trans.d = 0;
    txheader.trans.res = 0;
    
    // print header
    print_header(&txheader);
    
    // copy header into byte-array
    header2bytes(&txheader, txdata);
    
    // copy address into bytearray
    txdata[7] = 0;
    txdata[6] = 0;
    txdata[5] = addr >> 8;
    txdata[4] = addr & 0xff;
    
    // send
    //send(socketfd, txdata, 8, 0);
    boost::array<uint8_t, 8>  send_ary;
    for (int i = 0; i < 8; i++) {
        send_ary[i] = txdata[i];
    }
    
    socket->send_to(boost::asio::buffer(send_ary), *(endpoint.get()));
    // receive
    //recv(socketfd, rxdata, 8, 0);
    //boost::array<char, 256> recv_buf;
    uint8_t recv_buf[256];
    udp::endpoint sender_endpoint;
    /*socket->receive_from(
     boost::asio::buffer(recv_buf), sender_endpoint);*/
    boost::system::error_code ec;
    receive(boost::asio::buffer(recv_buf), boost::posix_time::seconds(1), ec);
    
    if(ec) {
        throw std::runtime_error(ec.message());
        exit(1);
        
    }
    
    
    for (int i = 0; i < 4; i++) {
        rxdata[i] = recv_buf[i];
    }
    
    // copy into header
    bytes2header(&rxheader, rxdata);
    
    // print header
    print_header(&rxheader);
    
    for (int i = 0; i < words; i++) {
        data[i] = recv_buf[4+i];
    }
    transaction_id = transaction_id + 1;
    
    if (rxheader.trans.words != words) {
        return 1;
    }
    
#ifdef BOCDEBUG
    std::cout << "Read from 0x" << std::hex << addr << ": 0x" << (0xff & rxdata[4]) << std::endl;
#endif
    return 0;
}


void Boc::check_deadline()
{
    if (deadline_->expires_at() <= deadline_timer::traits_type::now()) {
        socket->cancel();
        deadline_->expires_at(boost::posix_time::pos_infin);

    }
    deadline_->async_wait(boost::bind(&Boc::check_deadline, this));
}




size_t Boc::receive(const boost::asio::mutable_buffer &buffer,
        boost::posix_time::time_duration timeout, boost::system::error_code &ec) 
{
    deadline_->expires_from_now(timeout);
    ec = boost::asio::error::would_block;
    size_t length = 0;
    socket->async_receive(boost::asio::buffer(buffer),
        boost::bind(&Boc::handle_receive, _1, _2, &ec, &length));
    do {
        io_service.run_one();
    } while( ec == boost::asio::error::would_block);

    return length;
}








