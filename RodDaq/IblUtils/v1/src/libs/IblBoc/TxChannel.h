#ifndef TxChannel_H
#define TxChannel_H

//#include "Bcf.h"
//#include "Bmf.h"
#include "AddressMap.h"
#include <cstdint>
#include <map>

#ifdef JSON
#include <functional>
#endif

class Bmf;
using boost::uint8_t;

class TxChannel {
public:

    TxChannel(int c, Bmf  &b);

    int GetChannel() {
        return channel;
    }
    // Tx control functions
    void DisableTransmitter();
    void EnableTransmitter(int speed);
    void EnableTransmitter();
    int GetTransmitterStatus();

    void SendFromFifo(bool on);
    bool GetSendFromFifoStatus();

    void LoopFifo(bool on);
    bool GetLoopFifo();

    void DisableBpm();
    void EnableBpm();
    void SetBpm(bool on);
    bool GetBpmDisabledStatus();

    void SendRawData(bool on);
    bool GetSendRawData();

    void InputSelect(bool internal);
    bool GetInputSelect();

    void WriteTxControlRegister(Addr::Tx::Tx_Control Reg);
    Addr::Tx::Tx_Control ReadTxControlRegister();

    void SetCoarse(int coarse);
    int GetCoarse();

    void SetFineDelayA(int delay);
    void SetFineDelayB(int delay);
    int GetFineDelayA();
    int GetFineDelayB();

    void SetMsr(int msr);
    int GetMsr();


    // Tx status functions
    bool HasData();
    bool FifoFull();
    bool FifoIsReadOnly();
    Addr::Tx::Tx_Status GetStatusRegister();


    // Write to FIFO
    void FillFifo(uint8_t value, bool kword);
    // Load and save functions
    void ReadFile(const std::string& filename);
    void SaveFile(const std::string& filename);
private:

    void WriteToTxControl(Addr::Tx::Tx_Control Reg);
    void WriteToTxRegister(int type, uint8_t value); // Give it something like Addr::Tx::FifoControl
    boost::uint8_t ReadTxReg(int type);
    inline void LoadControlReg();

    Addr::Tx::Tx_Control control;

#ifdef JSON
    std::map<std::string, std::function<void (bool)>  > callbacks;
    std::map<std::string, std::function<void (int)>  > callbacks_ints;
#endif

    int channel;
    Bmf &bmf;
    int defaultSpeed; 
 
};

#endif
