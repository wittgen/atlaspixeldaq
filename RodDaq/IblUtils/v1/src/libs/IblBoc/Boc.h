#ifndef BOC_H
#define BOC_H

#include <string>
#include <tr1/memory>
#include <iostream>

#include "boost/asio.hpp"

#include <cstdint>

#include "Options.h"
#include "Bcf.h"
#include "AddressMap.h"
#include "Bmf.h"

#include "boost/date_time/posix_time/posix_time_types.hpp"

using boost::asio::ip::udp;
using boost::asio::deadline_timer;

//class Bmf;

using std::tr1::shared_ptr;

//extern Options g_opt;

class Boc {
public:
    Boc(const std::string& ip);
    Boc();
    ~Boc();
    Bcf GetBcf();
    Bmf GetBmf(Addr::Bmf::Location loc);

    void SetIp(std::string& ip);

    // Basic Read and write functions

    void SingleWrite(boost::uint16_t address, boost::uint8_t value);
    boost::uint8_t SingleRead(boost::uint16_t address);
    int NonIncRead(boost::uint16_t address,boost::uint8_t *data, uint32_t words);
    int NonIncRead(boost::uint16_t address,char *data, uint32_t words);
    int NonIncWrite(boost::uint16_t addr, boost::uint8_t *value, boost::uint32_t words);
    int NonIncWrite(boost::uint16_t addr, char *value, boost::uint32_t words);



private:
    std::string bocAddr;
    void Connect();
    void Disconnect();
    void check_deadline();

    size_t receive(const boost::asio::mutable_buffer &buffer,
        boost::posix_time::time_duration timeout, boost::system::error_code &ec);
    bool connected;
    int port;
    unsigned int transaction_id;
    boost::asio::io_service io_service;
    shared_ptr<udp::socket> socket;
    shared_ptr<udp::endpoint> endpoint;
    shared_ptr<deadline_timer> deadline_;

static void handle_receive(const boost::system::error_code& ec, std::size_t length,
      boost::system::error_code* out_ec, std::size_t* out_length)
  {
    *out_ec = ec;
    *out_length = length;
  }
};

#endif

