#ifndef VirtualRx_H
#define VirtualRx_H

#include <vector>
#include <tr1/array>
#include <tr1/memory>

#include "AddressMap.h"
#include <cstdint>
#include "RxChannel.h"
#include "Bmf.h"

using std::tr1::shared_ptr;
typedef shared_ptr<RxChannel> Rx_ptr;

class VirtualRx{
public:
	VirtualRx(Bmf &b, int be, int e): bmf(b), begin(be), end(e){
		for(size_t i = begin; i < end; i++){
			channels.push_back(Rx_ptr (new RxChannel(i, bmf)));
		}
	};

	VirtualRx(Bmf &b, std::tr1::array<int, 24> ary): bmf(b) {
		for(size_t i = 0; i < ary.size(); i++) {
			if ( ary[i] >=0 && ary[i] <=24)
				channels.push_back(Rx_ptr (new RxChannel(i, bmf)));
		}
	}

	RxChannel GetRx(int i);
	
	void Enable();
	void Disable();
	void SetEnabled(bool on);
	void SetMux(bool on);
	void SetMonitor(bool on);
	void SetMonitorRawMode(bool on);
	void SetLoopbackMode(bool internal);
	void SetAllowIdle(bool on);

	bool IsSynced(bool accumulated);
	bool HasDecodingError(bool accumulated);
	bool HasRunningDisparityError(bool accumulated);
	bool HasFrameError(bool accumulated);
	bool MonitorFifoIsFull(bool accumulated);
	bool MonitorFifoIsEmpty(bool accumulated);
    
    void DrainMonitorFifo();

private:
	Bmf &bmf;
	std::vector<Rx_ptr> channels;
	size_t begin;
	size_t end;
};


#endif 
