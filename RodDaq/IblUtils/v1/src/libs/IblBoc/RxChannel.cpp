#include "RxChannel.h"
#include "Boc.h"
#include "Bmf.h"

#ifdef JSON
    #include "boost/property_tree/ptree.hpp"
    #include "boost/property_tree/json_parser.hpp"
#endif
#include "boost/lexical_cast.hpp"
#include "boost/optional.hpp"

#include <iostream>
#include <fstream>
using namespace Addr::Rx;
RxChannel::RxChannel(int c,Bmf &b) : channel(c), bmf(b) {
    DrainMonitorFifo();

#ifdef JSON
    callbacks["Enabled"] = [=](bool on){this->SetEnabled(on);};
    callbacks["Mux"] = [=](bool on){this->SetMux(on);};
    callbacks["Monitor"] = [=](bool on){this->SetMonitor(on);};
    callbacks["MonitorRawMode"] = [=](bool on){this->SetMonitorRawMode(on);};
    callbacks["LoopbackMode"] = [=](bool on){this->SetLoopbackMode(on);};
    callbacks["AllowIdle"] = [=](bool on){this->SetAllowIdle(on);};
#endif
}

void RxChannel::Enable()
{
    LoadControlReg();
    control.Enabled = true;
    WriteRxControlRegister(control);
}

void RxChannel::Disable()
{
    LoadControlReg();
    control.Enabled = false;
    WriteRxControlRegister(control);
}

void RxChannel::SetEnabled(bool on)
{
    if (on) {
        Enable();
    } else {
        Disable();
    }
}

bool RxChannel::GetEnabled()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.Enabled;
}

void RxChannel::SetMux(bool on)
{
    LoadControlReg();
    control.Mux = on; 
    WriteRxControlRegister(control);
}

bool RxChannel::GetMux()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.Mux;
}

void RxChannel::SetMonitor(bool on)
{
    LoadControlReg();
    control.Monitor = on;
    WriteRxControlRegister(control);
}

bool RxChannel::GetMonitor()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.Monitor;
}

void RxChannel::SetMonitorRawMode(bool on)
{
    LoadControlReg();
    control.MonitorMode = on;
    WriteRxControlRegister(control);
}

bool RxChannel::GetMonitorRawMode()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.MonitorMode;
}

void RxChannel::SetLoopbackMode(bool internal)
{
    LoadControlReg();
    control.Loopback = internal;
    WriteRxControlRegister(control);
}

bool RxChannel::GetLoopbackMode()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.Loopback;
}

void RxChannel::SetAllowIdle(bool on)
{
    LoadControlReg();
    control.AllowIdle = on;
    WriteRxControlRegister(control);
}

bool RxChannel::GetAllowIdle()
{
    int reg = ReadRxRegister(Control);
    Rx_Control contr = *reinterpret_cast<Rx_Control*> (&reg);
    return contr.AllowIdle;
}

bool RxChannel::IsSynced(bool accumulated)
{
    int reg;
    uint8_t null = 0;
    Rx_Status wrStat = *reinterpret_cast<Rx_Status*>(&null);
    wrStat.Synchronization = 1;
    if (!accumulated) {
        WriteRxStatRegister(wrStat);
    }
    reg = ReadRxRegister(Status);
    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);
    WriteRxStatRegister(wrStat);
    return stat.Synchronization;
}

bool RxChannel::HasDecodingError(bool accumulated)
{
    int reg;
    uint8_t null = 0;
    Rx_Status wrStat = *reinterpret_cast<Rx_Status*>(&null);
    wrStat.DecodingError = 1;
    if (!accumulated) {
        WriteRxStatRegister(wrStat);
    }
    reg = ReadRxRegister(Status);
    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);
    WriteRxStatRegister(wrStat);
    return stat.DecodingError;
}

bool RxChannel::HasRunningDisparityError(bool accumulated)
{
    int reg;
    uint8_t null = 0;
    Rx_Status wrStat = *reinterpret_cast<Rx_Status*>(&null);
    wrStat.RunningDisparity = 1;
    if (!accumulated) {
        WriteRxStatRegister(wrStat);
    }
    reg = ReadRxRegister(Status);
    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);
    WriteRxStatRegister(wrStat);
    return stat.RunningDisparity;
}

bool RxChannel::HasFrameError(bool accumulated)
{
    int reg;
    uint8_t null = 0;
    Rx_Status wrStat = *reinterpret_cast<Rx_Status*>(&null);
    wrStat.FrameError = 1;
    if (!accumulated) {
        WriteRxStatRegister(wrStat);
    }
    reg = ReadRxRegister(Status);
    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);

    WriteRxStatRegister(wrStat);
    return stat.FrameError;
}

bool RxChannel::MonitorFifoIsFull(bool accumulated)
{
    (void) accumulated;
    int reg = ReadRxRegister(Status);

    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);
    return stat.FifoFull;
}

bool RxChannel::MonitorFifoIsEmpty(bool accumulated)
{
    (void) accumulated;
    int reg = ReadRxRegister(Status);
    
    Rx_Status stat = *reinterpret_cast<Rx_Status*> (&reg);
    return stat.FifoEmpty;
}

uint16_t RxChannel::ReadData()
{
    uint16_t high = ReadRxRegister(DataHigh); // read high 8-bits of data
    uint16_t low = ReadRxRegister(DataLow); // read lower 8-bits of data and automaticaly read the next values to these registers
    high <<= 8;     // put "high" into the higher 8-bits
    high &= 0xFF00; // clear lower 8-bits of "high" this should have been done by the bitshift, but better save than sorry
    low &= 0x00FF;  // clear higer 8-bits of "low"  
    return high |= low; // combine "high" and "low"
}

uint16_t RxChannel::ReadRodFIFO()
{
    return ReadData();
}

uint32_t RxChannel::GetFrameCount()
{
    uint32_t value = 0;

    value |= (0xff & ReadRxRegister(FrameCnt0)) << 24;
    value |= (0xff & ReadRxRegister(FrameCnt1)) << 16;
    value |= (0xff & ReadRxRegister(FrameCnt2)) << 8;
    value |= (0xff & ReadRxRegister(FrameCnt3));

    return value;
}

uint32_t RxChannel::GetFrameErrCount()
{
    uint32_t value = 0;

    value |= (0xff & ReadRxRegister(FrameErrCnt0)) << 24;
    value |= (0xff & ReadRxRegister(FrameErrCnt1)) << 16;
    value |= (0xff & ReadRxRegister(FrameErrCnt2)) << 8;
    value |= (0xff & ReadRxRegister(FrameErrCnt3));

    return value;
}


inline void RxChannel::LoadControlReg()
{
#ifndef NOLOADREG
    __attribute__((unused)) int reg = ReadRxRegister(Control);
    control = *reinterpret_cast<Rx_Control*> (&reg);
#endif
}

void RxChannel::WriteRxControlRegister(Addr::Rx::Rx_Control Reg)
{
    (void) Reg;
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 0;
    add.Channel = channel;
    add.Register = Addr::Rx::Control;
    bmf.GetBoc().SingleWrite(*reinterpret_cast<boost::uint16_t*>(&add), *reinterpret_cast<boost::uint8_t*>(&control));
}

void RxChannel::WriteRxStatRegister(Addr::Rx::Rx_Status Reg)
{
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 0;
    add.Channel = channel;
    add.Register = Addr::Rx::Status;
    bmf.GetBoc().SingleWrite(*reinterpret_cast<boost::uint16_t*>(&add), *reinterpret_cast<boost::uint8_t*>(&Reg));
}

boost::uint8_t  RxChannel::ReadRxRegister(int type)
{
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 0;
    add.Channel = channel;
    add.Register = type;
    int reg = bmf.GetBoc().SingleRead(*reinterpret_cast<boost::uint16_t*>(&add));
    return reg;
}



void RxChannel::ReadFile(const std::string& filename)
{
#ifdef JSON
    std::ifstream in(filename);
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(in, pt);
    std::string base = "Rx.";
    if (bmf.GetLocation() == Addr::Bmf::Location::North) {
        base += "North.";
    } else {
        base += "South.";
    }
    base += boost::lexical_cast<std::string>(channel) + ".";
    std::cout << "Reading " << base << std::endl;
    for (auto it=callbacks.begin() ;it != callbacks.end() ;it++) {
        try {
            std::string pname = base;
            pname+= (*it).first;
            boost::optional<bool> op= pt.get_optional<bool>(pname);
            if (op) {
                std::cout << "Setting " << (*it).first << " to " << op << std::endl;
                (*it).second(*op);
            }
        } catch (...) {
            std::cerr << "Error while reading file " << filename << std::endl;
        }
    }
#else 
    (void) filename;
    std::cerr << "Library compiled without JSON support. Please enable -DJSON to use this feature.\n";
#endif
}

void RxChannel::SaveFile(const std::string& filename)
{
#ifdef JSON
    boost::property_tree::ptree pt;
    
    std::string base = "Rx.";
    if (bmf.GetLocation() == Addr::Bmf::Location::North) {
        base += "North.";
    } else {
        base += "South.";
    }
    base += boost::lexical_cast<std::string>(channel) + ".";

    std::cout << "Saving " << base << std::endl;
    pt.put(base + "Enabled", GetEnabled());
    pt.put(base + "Mux", GetMux());
    pt.put(base + "Monitor", GetMonitor());
    pt.put(base + "MonitorRawMode", GetMonitorRawMode());
    pt.put(base + "LoopbackMode", GetLoopbackMode());
    pt.put(base + "AllowIde", GetAllowIdle());
    boost::property_tree::write_json(filename, pt);
#else 
    (void) filename;
    std::cerr << "Library compiled without JSON support. Please enable -DJSON to use this feature.\n";
#endif
}



void RxChannel::DrainMonitorFifo()
{
    SetMonitor(false);
    while(MonitorFifoIsEmpty(false) == false)
    {
        __attribute__((unused)) volatile int i = ReadData();
    }
}
