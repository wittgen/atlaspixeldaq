#include "TxChannel.h"
#include "Bmf.h"
#include "Boc.h"

#ifdef JSON
    #include "boost/property_tree/ptree.hpp"
    #include "boost/property_tree/json_parser.hpp"
#endif
#include "boost/lexical_cast.hpp"
#include "boost/optional.hpp"

#include <iostream>
#include <fstream>
using namespace Addr::Tx;
TxChannel::TxChannel(int c, Bmf & b) : channel(c), bmf(b)
{
    defaultSpeed = 1;

#ifdef JSON
    callbacks_ints["EnableTransmitter"] = [=](int speed){EnableTransmitter(speed);};
    callbacks["SendFromFifo"] = [=](bool on){SendFromFifo(on);};
    callbacks["LoopFifo"] = [=](bool on){LoopFifo(on);};
    callbacks["Bpm"] = [=](bool on){SetBpm(on);};
    callbacks["SendRawData"] = [=](bool on){SendRawData(on);};
    callbacks["InputSelect"] = [=](bool on){InputSelect(on);};
    callbacks_ints["Coarse"] = [=](int coarse){SetCoarse(coarse);};
    callbacks_ints["FineDelayA"] = [=](int delay){SetFineDelayA(delay);};
    callbacks_ints["FineDelayB"] = [=](int delay){SetFineDelayB(delay);};
    callbacks_ints["Msr"] = [=](int msr){SetMsr(msr);};
#endif


}

void TxChannel::DisableTransmitter()
{
    LoadControlReg();
    control.Clock = Off;
    WriteToTxControl(control);
}

void TxChannel::EnableTransmitter()
{
    LoadControlReg();
    control.Clock = defaultSpeed;
    WriteToTxControl(control);
}

void TxChannel::EnableTransmitter(int speed)
{
    LoadControlReg();
    control.Clock = speed;
    WriteToTxControl(control);
}

int TxChannel::GetTransmitterStatus()
{
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr.Clock;
}

void TxChannel::SendFromFifo(bool on)
{
    LoadControlReg();
    control.TransmitFromFifo = on;
    WriteToTxControl(control);
}

bool TxChannel::GetSendFromFifoStatus()
{
    LoadControlReg();
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr.TransmitFromFifo;
}

void TxChannel::LoopFifo(bool on)
{
    LoadControlReg();
    control.LoopFifo = on;
    WriteToTxControl(control);
}

bool TxChannel::GetLoopFifo()
{
    LoadControlReg();
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr.LoopFifo;
}

void TxChannel::DisableBpm()
{
    LoadControlReg();
    control.BpmDisable = true;
    WriteToTxControl(control);
}

void TxChannel::EnableBpm()
{
    LoadControlReg();
    control.BpmDisable = false;
    WriteToTxControl(control);
}
void TxChannel::SetBpm(bool on)
{
    if (on) { 
        EnableBpm();
    } else {
        DisableBpm();
    }
}

bool TxChannel::GetBpmDisabledStatus()
{
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr.BpmDisable;
}

void TxChannel::SendRawData(bool on)
{
    LoadControlReg();
    control.Mode = !on;
    WriteToTxControl(control);
}

bool TxChannel::GetSendRawData()
{
    LoadControlReg();
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return !contr.Mode;
}

void TxChannel::InputSelect(bool internal)
{
    LoadControlReg();
    control.InputSelect = internal;
    WriteToTxControl(control);
}

bool TxChannel::GetInputSelect()
{
    LoadControlReg();
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr.InputSelect;
}

void TxChannel::WriteTxControlRegister(Addr::Tx::Tx_Control Reg)
{
    WriteToTxControl(Reg);
}

Addr::Tx::Tx_Control TxChannel::ReadTxControlRegister()
{
    int reg = ReadTxReg(Control);
    Tx_Control contr = *reinterpret_cast<Tx_Control*> (&reg);
    return contr;
}

void TxChannel::SetCoarse(int delay)
{
    WriteToTxRegister(CoarseDelay, delay);
}

int TxChannel::GetCoarse()
{
    return ReadTxReg(CoarseDelay);
}

void TxChannel::SetFineDelayA(int delay)
{
    WriteToTxRegister(FineDelayA, delay);
}

int TxChannel::GetFineDelayA()
{
    return ReadTxReg(FineDelayA);
}

void TxChannel::SetFineDelayB(int delay)
{
    WriteToTxRegister(FineDelayA, delay);
}

int TxChannel::GetFineDelayB()
{
    return ReadTxReg(FineDelayA);
}

void TxChannel::SetMsr(int msr)
{
    WriteToTxRegister(Msr, msr);
}

int TxChannel::GetMsr()
{
    return ReadTxReg(Msr);
}

bool TxChannel::HasData()
{
    int reg = ReadTxReg(Status);
    Tx_Status contr = *reinterpret_cast<Tx_Status*> (&reg);
    return !contr.FifoEmpty;
}

bool TxChannel::FifoFull()
{
    int reg = ReadTxReg(Status);
    Tx_Status contr = *reinterpret_cast<Tx_Status*> (&reg);
    return contr.FifoFull;
}

bool TxChannel::FifoIsReadOnly()
{
    int reg = ReadTxReg(Status);
    Tx_Status contr = *reinterpret_cast<Tx_Status*> (&reg);
    return contr.FifoReadOnly;
}

Tx_Status TxChannel::GetStatusRegister()
{
    int reg = ReadTxReg(Status);
    Tx_Status contr = *reinterpret_cast<Tx_Status*> (&reg);
    return contr;
}
void TxChannel::FillFifo(uint8_t value, bool kword){
    if (kword) {
        WriteToTxRegister(FifoControl, value);
    } else {
        WriteToTxRegister(FifoData, value);
    }
}

inline void TxChannel::LoadControlReg()
{
#ifndef NOLOADREG
    int reg = ReadTxReg(Control);
    control = *reinterpret_cast<Tx_Control*> (&reg);
#endif
}

void TxChannel::WriteToTxControl(Addr::Tx::Tx_Control Reg)
{
    (void) Reg;
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 1;
    add.Channel = channel;
    add.Register = Addr::Rx::Control;
    bmf.GetBoc().SingleWrite(*reinterpret_cast<boost::uint16_t*>(&add), *reinterpret_cast<boost::uint8_t*>(&control));
}

void TxChannel::WriteToTxRegister(int type, boost::uint8_t value)
{
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 1;
    add.Channel = channel;
    add.Register = type;
    bmf.GetBoc().SingleWrite(*reinterpret_cast<boost::uint16_t*>(&add), value);
}


boost::uint8_t TxChannel::ReadTxReg(int type)
{
    Addr::Bmf::Bmf_Address add;
    if (bmf.GetLocation() == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 1;
    add.Tx = 1;
    add.Channel = channel;
    add.Register = type;
    boost::uint8_t reg = bmf.GetBoc().SingleRead(*reinterpret_cast<boost::uint16_t*>(&add));
    return reg;
}



void TxChannel::ReadFile(const std::string& filename)
{
#ifdef JSON
    std::ifstream in(filename);
    boost::property_tree::ptree pt;
    boost::property_tree::read_json(in, pt);
    std::string base = "Tx.";
    if (bmf.GetLocation() == Addr::Bmf::Location::North) {
        base += "North.";
    } else {
        base += "South.";
    }
    base += boost::lexical_cast<std::string>(channel) + ".";
    std::cout << "Reading " << base << std::endl;
    for (auto it=callbacks.begin() ;it != callbacks.end() ;it++) {
        try {
            std::string pname = base;
            pname+= (*it).first;
            boost::optional<bool> op= pt.get_optional<bool>(pname);
            if (op) {
                std::cout << "Setting " << (*it).first << " to " << op << std::endl;
                (*it).second(*op);
            }
        }catch (...) {
            std::cerr << "Error reading file " << filename << std::endl;
        }
    }
    for (auto it=callbacks_ints.begin() ;it != callbacks_ints.end() ;it++) {
        try {
            std::string pname = base;
            pname+= (*it).first;
            boost::optional<int> op= pt.get_optional<int>(pname);
            if (op) {
                std::cout << "Setting " << (*it).first << " to " << op << std::endl;
                (*it).second(*op);
            }
        } catch (...) {
             std::cerr << "Error reading file " << filename << std::endl;
        }
    }
#else 
    (void) filename;
    std::cerr << "Library compiled without JSON support. Please enable -DJSON to use this feature.\n";
#endif
}

void TxChannel::SaveFile(const std::string& filename)
{
#ifdef JSON
    boost::property_tree::ptree pt;
    
    std::string base = "Tx.";
    if (bmf.GetLocation() == Addr::Bmf::Location::North) {
        base += "North.";
    } else {
        base += "South.";
    }
    base += boost::lexical_cast<std::string>(channel) + ".";

    pt.put(base + "EnableTransmitter", GetTransmitterStatus());
    pt.put(base + "SendFromFifo", GetSendFromFifoStatus());
    pt.put(base + "LoopFifo", GetLoopFifo());
    pt.put(base + "Bpm", !GetBpmDisabledStatus());
    pt.put(base + "SendRawData", GetSendRawData());
    pt.put(base + "InputSelect", GetInputSelect());
    pt.put(base + "Coarse", GetCoarse());
    pt.put(base + "FineDelayA", GetFineDelayA());
    pt.put(base + "FineDelayB", GetFineDelayB());
    pt.put(base + "Msr", GetMsr());
    boost::property_tree::write_json(filename, pt);
#else 
    (void) filename;
    std::cerr << "Library compiled without JSON support. Please enable -DJSON to use this feature.\n";
#endif
}

