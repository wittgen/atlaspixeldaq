#ifndef RXCHANNEL_H
#define RXCHANNEL_H

#include "AddressMap.h"
#include <cstdint>
#include <map>
#ifdef JSON
#include <functional>
#endif

class Bmf;


class RxChannel  {
public:
    RxChannel(int c,Bmf &b);
    // Rx control functions
    void Enable();
    void Disable();
    void SetEnabled(bool on);
    bool GetEnabled();


    void SetMux(bool on);
    bool GetMux();

    void SetMonitor(bool on);
    bool GetMonitor();

    void SetMonitorRawMode(bool on);
    bool GetMonitorRawMode();

    void SetLoopbackMode(bool internal);
    bool GetLoopbackMode();

    void SetAllowIdle(bool on);
    bool GetAllowIdle();

    // Rx status functions

    bool IsSynced(bool accumulated);
    bool HasDecodingError(bool accumulated);
    bool HasRunningDisparityError(bool accumulated);
    bool HasFrameError(bool accumulated);
    bool MonitorFifoIsFull(bool accumulated);
    bool MonitorFifoIsEmpty(bool accumulated);

    uint16_t ReadData();

    uint16_t ReadRodFIFO(); 

    uint32_t GetFrameCount();
    uint32_t GetFrameErrCount();

    void WriteRxControlRegister(Addr::Rx::Rx_Control Reg);
    void WriteRxStatRegister(Addr::Rx::Rx_Status Reg);
    uint8_t ReadRxRegister(int type);

    // Load and save functions
    void ReadFile(const std::string& filename);
    void SaveFile(const std::string& filename);

    void DrainMonitorFifo();

    int GetChannel() {
        return channel;
    }
private:

    inline void LoadControlReg();
    int channel;

#ifdef JSON
    std::map<std::string, std::function<void (bool)>  > callbacks;
#endif

    Addr::Rx::Rx_Control control;
    Bmf& bmf;

};

#endif
