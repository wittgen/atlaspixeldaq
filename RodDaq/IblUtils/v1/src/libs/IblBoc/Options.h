#ifndef OPTIONS_H
#define OPTIONS_H

#include <iostream>
#include <string>

#include "AddressMap.h"

#include "boost/program_options.hpp"

namespace po = boost::program_options;

class Options {
	public:
		Options(int c, char* v[]);
        Options();
        void init(int c=0, char* v[]=0);
		std::string version;
		std::string GetIp();
		int GetFei4Channel();
		std::string GetImageName();
		bool Verbose();
		std::string GetFlashFile();
		Addr::Bmf::Location GetTarget();
    std::string GetTargetName();

	private:
		po::options_description *desc;
		po::variables_map vm;

		int argc;
		char** argv;
		int Fei4Channel; 
		std::string Ip;
		bool verbose;
		std::string ImageName;
		std::string flashfile;
		std::string target;
};

#endif // OPTIONS_H
