#include "Bmf.h"
#include "Boc.h"
#include <iostream>


Bmf::Bmf(Boc& boc, Addr::Bmf::Location loc)
    : boc(boc), loc(loc)
    {}

void Bmf::WriteRegister(boost::uint16_t address, boost::uint8_t value)
{
    boc.SingleWrite(address, value);
}

boost::uint8_t Bmf::ReadRegister(int type)
{   
    Addr::Bmf::Bmf_Address add;
    if (loc == Addr::Bmf::North) {
        add.FpgaSelect = 0x2;
    } else {
        add.FpgaSelect = 0x1;
    }
    add.None = 0;
    add.General = 0;
    add.Tx = 0;
    add.Channel = 0;
    add.Register = type;
    return boc.SingleRead(*reinterpret_cast<boost::uint16_t*>(&add));
}

TxChannel Bmf::GetTxChannel(int channel)
{
    return TxChannel(channel, *this);
}

RxChannel Bmf::GetRxChannel(int channel)
{
    return RxChannel(channel, *this);
}

Boc& Bmf::GetBoc()
{
    return boc;
}

Addr::Bmf::Location Bmf::GetLocation()
{
    return loc;
}
