#ifndef VirtualTx_H
#define VirtualTx_H

#include <vector>
#include <tr1/array>
#include <tr1/memory>

#include "AddressMap.h"
#include <cstdint>
#include "TxChannel.h"
#include "Bmf.h"

using std::tr1::shared_ptr;
typedef shared_ptr<TxChannel> Tx_ptr;

class VirtualTx{
public:
	VirtualTx(Bmf &b, int be, int e): bmf(b), begin(be), end(e){
		for(size_t i = begin; i < end; i++){
			channels.push_back(Tx_ptr (new TxChannel(i, bmf)));
		}
	};

	VirtualTx(Bmf &b, std::tr1::array<int, 12> ary): bmf(b) {
		for(size_t i = 0; i < ary.size(); i++) {
			if ( ary[i] >=0 && ary[i] <=12)
				channels.push_back(Tx_ptr (new TxChannel(i, bmf)));
		}
	}

	TxChannel GetTx(int i);
	
	void DisableTransmitter();
	void EnableTransmitter(int speed);
	void EnableTransmitter();
	void SendFromFifo(bool on);
	void LoopFifo(bool on);
	void DisableBpm();
	void EnableBpm();
	void SetBpm(bool on);
	void SendRawData(bool on);
	void InputSelect(bool internal);
	void WriteTxControlRegister(Addr::Tx::Tx_Control Reg);
	void SetCoarse(int coarse);
	void SetFineDelayA(int delay);
	void SetFineDelayB(int delay);
	void SetMsr(int msr);
	void FillFifo(uint8_t value, bool kword);

private:
	Bmf &bmf;
	std::vector<Tx_ptr> channels;
	size_t begin;
	size_t end;
};


#endif 
