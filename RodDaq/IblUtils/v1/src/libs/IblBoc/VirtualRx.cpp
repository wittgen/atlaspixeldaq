#include "VirtualRx.h"
#include <algorithm>
using std::tr1::shared_ptr;
typedef shared_ptr<RxChannel> Rx_ptr;

RxChannel VirtualRx::GetRx(int i)
{
        std::foreach(Rx_ptr c, channels) {
		if (c->GetChannel() == i) {
			return *c.get();
		}
	}
}

void VirtualRx::Enable()
{
	std::foreach(Rx_ptr c, channels) {
		c->Enable();
	}
}
void VirtualRx::Disable()
{
	std::foreach(Rx_ptr c, channels) {
		c->Disable();
	}
}

void VirtualRx::SetEnabled(bool on)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetEnabled(on);
	}
}

void VirtualRx::SetMux(bool on)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetMux(on);
	}
}

void VirtualRx::SetMonitor(bool on)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetMonitor(on);
	}
}

void VirtualRx::SetMonitorRawMode(bool on)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetMonitorRawMode(on);
	}
}

void VirtualRx::SetLoopbackMode(bool internal)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetLoopbackMode(internal);
	}
}

void VirtualRx::SetAllowIdle(bool on)
{
	std::foreach(Rx_ptr c, channels) {
		c->SetAllowIdle(on);
	}
}



bool VirtualRx::IsSynced(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (!c->IsSynced(accumulated)) {
			return false;
		}
	}
	return true;
}
bool VirtualRx::HasDecodingError(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (c->HasDecodingError(accumulated)) {
			return true;
		}
	}
	return false;
}

bool VirtualRx::HasRunningDisparityError(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (c->HasRunningDisparityError(accumulated)) {
			return true;
		}
	}
	return false;
}

bool VirtualRx::HasFrameError(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (c->HasFrameError(accumulated)) {
			return true;
		}
	}
	return false;
}

bool VirtualRx::MonitorFifoIsFull(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (!c->MonitorFifoIsFull(accumulated)) {
			return false;
		}
	}
	return true;
}

bool VirtualRx::MonitorFifoIsEmpty(bool accumulated)
{
	std::foreach(Rx_ptr c, channels) {
		if (!c->MonitorFifoIsEmpty(accumulated)) {
			return false;
		}
	}
	return true;
}

void VirtualRx::DrainMonitorFifo() {
    std::foreach(Rx_ptr c, channels) {
        c->DrainMonitorFifo();
    }
}
