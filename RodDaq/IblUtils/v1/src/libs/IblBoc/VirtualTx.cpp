#include "VirtualTx.h"
#include <algorithm>
using std::tr1::shared_ptr;
typedef shared_ptr<TxChannel> Tx_ptr;

TxChannel VirtualTx::GetTx(int i)
{
	std::foreach(Tx_ptr c, channels) {
		if (c->GetChannel() == i) {
			return *c.get();
		}
	}
}

void VirtualTx::DisableTransmitter()
{
	std::foreach(Tx_ptr c, channels) {
		c->DisableTransmitter();
	}
}

void VirtualTx::EnableTransmitter(int speed)
{
	std::foreach(Tx_ptr c, channels) {
		c->EnableTransmitter(speed);
	}
}
void VirtualTx::EnableTransmitter()
{
	std::foreach(Tx_ptr c, channels) {
		c->EnableTransmitter();
	}
}
void VirtualTx::SendFromFifo(bool on)
{
	std::foreach(Tx_ptr c, channels) {
		c->SendFromFifo(on);
	}
}
void VirtualTx::LoopFifo(bool on)
{
	std::foreach(Tx_ptr c, channels) {
		c->LoopFifo(on);
	}
}
void VirtualTx::DisableBpm()
{
	std::foreach(Tx_ptr c, channels) {
		c->DisableBpm();
	}
}
void VirtualTx::EnableBpm()
{
	std::foreach(Tx_ptr c, channels) {
		c->EnableBpm();
	}
}
void VirtualTx::SetBpm(bool on)
{
	std::foreach(Tx_ptr c, channels) {
		c->SetBpm(on);
	}
}
void VirtualTx::SendRawData(bool on)
{
	std::foreach(Tx_ptr c, channels) {
		c->SendRawData(on);
	}
}
void VirtualTx::InputSelect(bool internal)
{
	std::foreach(Tx_ptr c, channels) {
		c->InputSelect(internal);
	}
}
void VirtualTx::WriteTxControlRegister(Addr::Tx::Tx_Control Reg)
{
	std::foreach(Tx_ptr c, channels) {
		c->WriteTxControlRegister(Reg);
	}
}

void VirtualTx::SetCoarse(int coarse)
{
	std::foreach(Tx_ptr c, channels) {
		c->SetCoarse(coarse);
	}
}
void VirtualTx::SetFineDelayA(int delay)
{
	std::foreach(Tx_ptr c, channels) {
		c->SetFineDelayA(delay);
	}
}
void VirtualTx::SetFineDelayB(int delay)
{
	std::foreach(Tx_ptr c, channels) {
		c->SetFineDelayB(delay);
	}
}
void VirtualTx::SetMsr(int msr)
{
	std::foreach(Tx_ptr c, channels) {
		c->SetMsr(msr);
	}
}

void VirtualTx::FillFifo(uint8_t value, bool kword)
{
	std::foreach(Tx_ptr c, channels) {
		c->FillFifo(value, kword);
	}
}
