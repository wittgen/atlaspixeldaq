#ifndef ADDRESS_MAP_H
#define ADDRESS_MAP_H
#include <string>
#include <iostream>

#include <cstdint>

typedef unsigned int uint_t;

namespace Addr {
    namespace Bmf{
        enum Location{
            North, South
        };
        // absolute addresses for bmf registers
        enum {
            Firmware                 = 0x000, // 8-bit version Number 0x01 = alpha, 0x10 = rc
            BuildYear               = 0x001, // Build year w/ base 2000 (e.g. 0x0C = 2012)
            BuildMonth              = 0x002, // Build month ( 0x01 for Jan etc.)
            BuildDay                = 0x003, // Build day   (0x01 for first day of month)
            BuildHour               = 0x004, // Build hour in 24h representation
            BuildMinute             = 0x005, // Build minute
            BuildSecond             = 0x006  // Build second
        };
        // Address Bus For Bmf
        typedef struct Address {
            uint_t Register            : 5; // Register Number
            uint_t Channel             : 5; // Channel address
            uint_t Tx                  : 1; // Select between Rx and Tx registers. 0 = RX 1 = Tx
            uint_t General             : 1; // Select between General Purpose Bmf and Rx/Tx registers. 0 = BMF Control 1 = Rx/Tx register
            uint_t None                : 2; // Empty do not use!
            uint_t FpgaSelect          : 2; // Select north or south BMF. no convention yet
        } Bmf_Address;
    }
    namespace Rx {
        // relative addresses for Rx path registers
        enum {
            Control                 = 0x0, // Rx control register
            Status                  = 0x1, // Live status register
            DataHigh                = 0x2, // Data register (upper 8-bits)
            DataLow                 = 0x3, // Data register (lower 8-bits)
            FrameCnt0               = 0x4, // Frame counter (bits 31:24)
            FrameCnt1               = 0x5, // Frame counter (bits 23:16)
            FrameCnt2               = 0x6, // Frame counter (bits 15:8)
            FrameCnt3               = 0x7, // Frame counter (bits 7:0)
            FrameErrCnt0                = 0x8, // Frame error counter (bits 31:24)
            FrameErrCnt1                = 0x9, // Frame error counter (bits 23:16)
            FrameErrCnt2                = 0xA, // Frame error counter (bits 15:8)
            FrameErrCnt3                = 0xB // Frame error counter (bits 7:0)            
        };
        // Rx Control Register  
        typedef struct Control {
            uint_t Enabled             : 1; // 0 = channel disabled 1 = channel enabled
            uint_t Mux                 : 1; // 0 = Mux output disabled 1 = Mux output enabled
            uint_t Monitor             : 1; // 0 = monitor output disabled 1 = monitor output enabled
            uint_t MonitorMode         : 1; // 0 = decoded date  1 = raw 8b10b data
            uint_t AllowIdle           : 1; // 1 = allow idles in fifo
            uint_t Loopback            : 1; // 0 = use external data 1 = internal loopback fromo corresponding Tx channel
            std::ostream & operator<< (std::ostream & out) {
                out << "0x" << std::hex << *reinterpret_cast<int*> (this);
                return out;
            }
        } Rx_Control;
        // Rx Status Register
        typedef struct Status {
            uint_t Synchronization     : 1; // 0 = no sync 1 = synchronized
            uint_t DecodingError       : 1; // 0 = no error 1 = decoding error
            uint_t RunningDisparity    : 1; // 0 = no error 1 = running disparity error
            uint_t FrameError          : 1; // 0 = valid FE-I4 frames 1 = Frame errors 
            uint_t FifoFull            : 1; // 0 = RX monitor FIFO not full  1 =  Rx monitor fifo full
            uint_t FifoEmpty           : 1; // 0 = RX monitor FIFO has data  1 =  Rx monitor fifo is empty
            std::ostream & operator<< (std::ostream & out) {
                out << "0x"<< std::hex << *reinterpret_cast<int*> (this);
                return out;
            }
        } Rx_Status;
    }

    namespace Tx {
        // relative addresses for Tx path registers
        enum {
            Control                 = 0x0, // Tx control register
            Status                  = 0x1, // Tx status register
            FifoControl             = 0x2, // Insert a K-word into the FIFO
            FifoData                = 0x3, // Insert a data word into the FIFO
            LaserCurrent            = 0x4, // Lasser current for the old Tx Plugins
            CoarseDelay             = 0x5, // Coarse delay
            FineDelayA              = 0x6, // First fine delay register
            FineDelayB              = 0x7, // Second fine delay register
            Msr                     = 0x8 // Mark-Space-Ratio Adjustment
        };
        // TX control register
        typedef struct Control {
            uint8_t Clock               : 2; // OO = TX path diabled 01 = 40 MHz 10 = 80 MHz 11 = 160 MHz
            uint8_t TransmitFromFifo    : 1; // 0 = internel transmitter will transmit zeros (raw) or IDLE (8b10b) 1 = internal transmitter uses the FIFO data
            uint8_t LoopFifo            : 1; // 0 = FIFO data is transmitted once 1 = FIFO data is transmitted infinetly
            uint8_t BpmDisable          : 1; // 0 = BPM coding enabled  1 = BPM coding disabled
            uint8_t Mode                : 1; // 0 = transmit raw data 1 = transmit 8b10b encoded data
            uint8_t InputSelect         : 1; // 0 = use external input from ROD 1 = use internal input from serializer 
            std::ostream & operator<< (std::ostream & out) {
                out << "0x"<< std::hex << *reinterpret_cast<int*> (this);
                return out;
            }
        } Tx_Control;

        // TX status
        typedef struct Status {
            uint8_t FifoEmpty           : 1; // 0 = Tx FIFO has data 1 = Tx FIFO is empty
            uint8_t FifoFull            : 1; // 0 = Tx FIFO can accept data 1 = Tx FIFO is full and will not accept new data
            uint8_t FifoReadOnly        : 1; // 0 = One can write to the TX FIFO with FIFO_CONTROL/FIFO_DATA 1 = write access is denied (Loop bit in Control register is set)
            std::ostream & operator<< (std::ostream & out) {
                out << "0x"<< std::hex << *reinterpret_cast<int*> (this);
                return out;
            }
        } Tx_Status;    
        // TX Speed Settings
        enum {
            Off                     = 0,
            MHz40                   = 1,
            MHz80                   = 2,
            MHz160                  = 3    
        };
    }
    namespace Bcf {
        enum {
            Firmware                = 0x000, // 8-bit version Number 0x01 = alpha, 0x10 = rc
            BuildYear               = 0x001, // Build year w/ base 2000 (e.g. 0x0C = 2012)
            BuildMonth              = 0x002, // Build month ( 0x01 for Jan etc.)
            BuildDay                = 0x003, // Build day   (0x01 for first day of month)
            BuildHour               = 0x004, // Build hour in 24h representation
            BuildMinute             = 0x005, // Build minute
            BuildSecond             = 0x006,  // Build second
            LaserEnable             = 0x007, // Enables the lasers (Interlock)
            JtagOverride            = 0x008, // Allow external access to the BMF
            Spare                   = 0x009,
            PllCtrl                 = 0x030, //PLL config control
            PllStatus               = 0x031, // PLL status
            PllData0                = 0x032, // PLL Data
            PllData1                = 0x033, // PLL Data
            PllData2                = 0x034, // PLL Data
            PllData3                = 0x035, // PLL Data
            PllIdelay0              = 0x036, // PLL input delay <7:0>
            PllIdelay1              = 0x037, // PLL Input delay <10:8>
            ProgNorthCrc0           = 0x038, // CRC32 <7:0>
            ProgNorthCrc1           = 0x039, // CRC32 <15:8>
            ProgNorthCrc2           = 0x03A, // CRC32 <23:16>
            ProgNorthCrc3           = 0x03B, // CRC32 <31:24>
            ProgSouthCrc0           = 0x03C, // CRC32 <7:0>
            ProgSouthCrc1           = 0x03D, // CRC32 <7:0>
            ProgSouthCrc2           = 0x03E, // CRC32 <7:0>
            ProgSouthCrc3           = 0x03F, // CRC32 <7:0>
            ProgNorthCtrl           = 0x040, // North Control
            ProgNorthStatus         = 0x041, // North Status
            ProgNorthDCNT           = 0x042, // Data count
            ProgNorthData           = 0x043, // Page FIFO data
            ProgNorthFladdr0        = 0x044, // Flash address <7:0>
            ProgNorthFladdr1        = 0x045, // Flash address <15:8>
            ProgNorthFladdr2        = 0x046, // Flash address <23:16>
            ProgSouthCtrl           = 0x048, // South Control
            ProgSouthStatus         = 0x049, // South Status
            ProgSouthDCNT           = 0x04A, // Data count
            ProgSouthData           = 0x04B, // Page FIFO data
            ProgSouthFladdr0        = 0x04C, // Flash address <7:0>
            ProgSouthFladdr1        = 0x04D, // Flash address <15:8>
            ProgSouthFladdr2        = 0x04E // Flash address <23:16>
        };
    	typedef struct PllControlReg_struct {
    	    uint_t Reg		    : 4; // Register address
    	    uint_t Read		    : 1; // Read flag
    	    uint_t Write		: 1; // Write flag
    	    uint_t Reset		: 1; // Reset flag
    	    uint_t Reserved	    : 1; // reserved for future use
    	} PllControlReg;
    	typedef struct PllStatusReg_struct {
    	    uint_t Busy		    : 1; // PLL configuration is busy
    	    uint_t Locked		: 1; // PLL is locked
    	    uint_t Reserved	    : 6; // reserved for future use
    	} PllStatusReg;
            typedef struct Control {
                uint_t ChipErase           : 1; // Erase Chip
                uint_t ProgramPage         : 1; // Program Page
                uint_t Jtag                : 1; // Start JTAG programming
                uint_t Reset               : 1; // Soft Reset
                uint_t CalcCrc             : 1; // Calc CRC32
    	        uint_t Reserved	           : 3; // reserved for future use
            } Prog_Control;

        typedef struct Status {
            uint_t FifoEmpty           : 1; // Page FIFO empty
            uint_t FifoFull            : 1; // Page FIFO full
            uint_t Busy                : 1; // Busy Bee
            uint_t JtagError           : 1; // JTAG error
            uint_t JtagEof             : 1; // JTAG end of file
            uint_t JtagDone            : 1; // JTAG done
        } Prog_Status;
    }
    namespace Boc {
        enum{
            Base = 0x2A
        };
    }

    namespace Fei4 {
        enum {
            ConfMode                = 0x07, // Magic number for FE-I4 configuration mode
            RunMode                 = 0x38 // Magic number for FE-I4 run mode
        };
    }

}


#ifdef CPP11_UL

int operator "" _MHz(unsigned long long hz)
{
    switch (hz){
        case 0:
            return 0;
            break;
        case 40:
            return 1;
            break;
        case 80: 
            return 2;
            break;
        case 160: 
            return 3;
            break;
        default:
            return 0;
            break;

    }
}


#endif
#endif //ADDRESS_MAP_H

