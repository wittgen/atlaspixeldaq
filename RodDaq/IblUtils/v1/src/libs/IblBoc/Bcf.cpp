#include "Bcf.h"
#include "Boc.h"
#include "boost/filesystem.hpp"
#include "boost/crc.hpp"
#include "boost/progress.hpp"
#include <fstream>
#include <iostream>

namespace fs = boost::filesystem;

Bcf::Bcf(Boc& b)
:boc(b)
{   
	Init();
}


void Bcf::Init()
{
    SetLaserOn(true);
}

void Bcf::SetLaserOn(bool on)
{
    if (on) {
        boc.SingleWrite(Addr::Bcf::LaserEnable, 0xAB);
    } else {
        boc.SingleWrite(Addr::Bcf::LaserEnable, 0x00);   
    }
}

void Bcf::JtagOverride(bool on)
{
	if (on)	{
		boc.SingleWrite(Addr::Bcf::JtagOverride, 0xAB);
	} else {
		boc.SingleWrite(Addr::Bcf::JtagOverride, 0x00);
	}
}

void Bcf::WritePllRegister(unsigned int address, unsigned int value)
{
	using Addr::Bcf::PllStatusReg;
	using Addr::Bcf::PllControlReg;
	
	// write value to the PLL data register
	boc.SingleWrite(Addr::Bcf::PllData0, value & 0xFF);
	boc.SingleWrite(Addr::Bcf::PllData1, (value >> 8) & 0xFF);
	boc.SingleWrite(Addr::Bcf::PllData2, (value >> 16) & 0xFF);
	boc.SingleWrite(Addr::Bcf::PllData3, (value >> 24) & 0x0F);

	// start write cycle
	PllControlReg ctrl;
	ctrl.Reg = address & 0x0F;
	ctrl.Read = 0;
	ctrl.Write = 1;
	ctrl.Reset = 0;
	ctrl.Reserved = 0;
	boc.SingleWrite(Addr::Bcf::PllCtrl, *reinterpret_cast<int*> (&ctrl));

	// wait until ready
	PllStatusReg stat;
	do
	{
		int stat_int = boc.SingleRead(Addr::Bcf::PllStatus);	
		stat = *reinterpret_cast<PllStatusReg*> (&stat_int);
	}
	while(stat.Busy == 1);	
}

unsigned int Bcf::ReadPllRegister(unsigned int address)
{
	using Addr::Bcf::PllStatusReg;
	using Addr::Bcf::PllControlReg;

	unsigned int ret;

	// start read cycle
	PllControlReg ctrl;
	ctrl.Reg = address & 0x0F;
	ctrl.Read = 1;
	ctrl.Write = 0;
	ctrl.Reset = 0;
	ctrl.Reserved = 0;
	boc.SingleWrite(Addr::Bcf::PllCtrl, *reinterpret_cast<int*> (&ctrl));

	// wait until ready
	PllStatusReg stat;
	do
	{
		int stat_int = boc.SingleRead(Addr::Bcf::PllStatus);	
		stat = *reinterpret_cast<PllStatusReg*> (&stat_int);
	}
	while(stat.Busy == 1);

	// read value
	ret = boc.SingleRead(Addr::Bcf::PllData0) & 0xFF;
	ret |= (boc.SingleRead(Addr::Bcf::PllData1) & 0xFF) << 8;
	ret |= (boc.SingleRead(Addr::Bcf::PllData2) & 0xFF) << 16;
	ret |= (boc.SingleRead(Addr::Bcf::PllData3) & 0x0F) << 24;

	return ret;
}

unsigned int Bcf::ResetPll()
{
	using Addr::Bcf::PllStatusReg;
	using Addr::Bcf::PllControlReg;

	// start reset cycle
	PllControlReg ctrl;
	ctrl.Reg = 0;
	ctrl.Read = 0;
	ctrl.Write = 0;
	ctrl.Reset = 1;
	ctrl.Reserved = 0;
	boc.SingleWrite(Addr::Bcf::PllCtrl, *reinterpret_cast<int*> (&ctrl));

	// wait until ready
	PllStatusReg stat;
	do
	{
		int stat_int = boc.SingleRead(Addr::Bcf::PllStatus);	
		stat = *reinterpret_cast<PllStatusReg*> (&stat_int);
	}
	while(stat.Busy == 1);
    return 0;
}

bool Bcf::IsPllLocked()
{
	if(ReadPllRegister(0x02) & 0x0040)
		return true;
	else
		return false;
}

unsigned int Bcf::GetIdelay()
{
	return 0;
}

void Bcf::SetIdelay(unsigned int value)
{
    (void) value;
}

void Bcf::FastFlashBmf(Addr::Bmf::Location loc, const std::string &filename) {
    using Addr::Bcf::Prog_Control;
    using Addr::Bcf::Prog_Status;
    fs::path progfile(filename);
    
    if (!exists(progfile)) {
        std::cerr << "File " <<filename <<" does not exist\n";
        return;
    }
    
	// disable jtag override
	JtagOverride(false);
    
    unsigned int filesize = fs::file_size(progfile);
    std::ifstream file(filename.c_str());
	std::cout << "Performing soft reset...\t";
    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.Reset = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
        ctrl.Reset = 0;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
    } else {
        Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.Reset = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
        ctrl.Reset = 0;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
    }
    
    while (ProgIsBusy(loc)) {
        // Wait
    }
    std::cout << "done!" << std::endl;
    
    std::cout << "Perfoming chiperase... " << std::flush;
    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
		ctrl.Reset = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.ChipErase = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
    } else {
        Prog_Control ctrl;
		ctrl.Reset = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.ChipErase = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
    }
    
    while (ProgIsBusy(loc)) {
        sleep(1);
        // Wait really long
    }
    
    std::cout << "done!\n";
    
    boost::crc_32_type crc_calc;
    
    std::cout << "Flashing firmware...\t";
	//std::cin.get();
    boost::progress_display pbar(filesize);
    unsigned int progcnt = 0;
    while (progcnt < filesize) {
        char c[128];
        
        // read 128 bytes (or the rest) from file
        unsigned int wrtcount = 0;
        if ((filesize-progcnt) >= 128) {
            wrtcount = 128;
        } else {
            wrtcount = (filesize-progcnt)%128;
        }
        //file.get(c,wrtcount);
        for(int i =0 ; i < 128 ; ++i) {
            c[i] = 0x0;
            if (file) file.get(c[i]);
            else break;
        }
        
        progcnt+=wrtcount;
        
                
        // process data for crc calculation
        crc_calc.process_bytes(c, wrtcount);
        
        // Logic to have words properly filled
        while (wrtcount%4 != 0) {
            c[wrtcount] = 0;
            wrtcount++;
        }
        
        // increment progcnt
        pbar+=wrtcount;
        
        // write data to page fifo
        if(loc == Addr::Bmf::North)
        {
            boc.NonIncWrite(Addr::Bcf::ProgNorthData, c, wrtcount/4);
            if(progcnt%256 == 0)
            {
                Prog_Control ctrl;
				ctrl.ChipErase = 0;
				ctrl.Reset= 0;
				ctrl.Jtag = 0;
				ctrl.CalcCrc = 0;
				ctrl.Reserved = 0;
                ctrl.ProgramPage = 1;
                WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
                while(ProgIsBusy(loc)) {
                    //Wait
                }
            }
        }
        if(loc == Addr::Bmf::South)
        {
            boc.NonIncWrite(Addr::Bcf::ProgSouthData, c, wrtcount/4);
            if(progcnt%256 == 0)
            {
                Prog_Control ctrl;
				ctrl.ChipErase = 0;
				ctrl.Reset= 0;
				ctrl.Jtag = 0;
				ctrl.CalcCrc = 0;
				ctrl.Reserved = 0;
                ctrl.ProgramPage = 1;
                WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
                while(ProgIsBusy(loc)) {
                    //Wait
                }
            }
        }
    }
    
    // need to perform a last page programming?
    if(progcnt % 256 != 0)
    {
        if(loc == Addr::Bmf::North)
        {
            Prog_Control ctrl;
            ctrl.ChipErase = 0;
			ctrl.Reset= 0;
			ctrl.Jtag = 0;
			ctrl.CalcCrc = 0;
			ctrl.Reserved = 0;
            ctrl.ProgramPage = 1;
            WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        if(loc == Addr::Bmf::South)
        {
            Prog_Control ctrl;
			ctrl.ChipErase = 0;
			ctrl.Reset= 0;
			ctrl.Jtag = 0;
			ctrl.CalcCrc = 0;
			ctrl.Reserved = 0;
            ctrl.ProgramPage = 1;
            WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
    }
    std::cout << std::endl << "Flashing has been done!\n";
    
    // start verify
    std::cout << "Starting verification of flash content..." << std::endl;
    SetProgFlash(loc, filesize - 1);
    
    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
        ctrl.ChipErase = 0;
        ctrl.Reset= 0;
        ctrl.Jtag = 0;
        ctrl.ProgramPage = 0;
        ctrl.Reserved = 0;
        ctrl.CalcCrc = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
        while(ProgIsBusy(loc)) {
            //Wait
        }
    } else {
        Prog_Control ctrl;
        ctrl.ChipErase = 0;
        ctrl.Reset= 0;
        ctrl.Jtag = 0;
        ctrl.ProgramPage = 0;
        ctrl.Reserved = 0;
        ctrl.CalcCrc = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
        while(ProgIsBusy(loc)) {
            //Wait
        }
    }
    uint32_t flash_crc = ReadCrc(loc);
    uint32_t file_crc = crc_calc.checksum();
    std::cout << "CRC-File: 0x" << std::hex << file_crc << std::dec << std::endl;
    std::cout << "CRC-Flash: 0x" << std::hex << flash_crc << std::dec << std::endl;
    if(flash_crc != file_crc)
    {
        std::cout << "Verification failed!\n";
    } else {
        std::cout << "done!\n";
        // start jtag programming
        std::cout << "Start JTAG-programming\t";
        SetProgFlash(loc, 0);
        if (loc == Addr::Bmf::North) {
            Prog_Control ctrl;
            ctrl.ChipErase = 0;
            ctrl.Reset= 0;
       		ctrl.ProgramPage = 0;
        	ctrl.CalcCrc = 0;
            ctrl.Reserved = 0;
            ctrl.Jtag = 1;
            WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        if (loc == Addr::Bmf::South) {
            Prog_Control ctrl;
            ctrl.ChipErase = 0;
            ctrl.Reset= 0;
       		ctrl.ProgramPage = 0;
        	ctrl.CalcCrc = 0;
            ctrl.Reserved = 0;
            ctrl.Jtag = 1;
            WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        std::cout << "done\n";
    }
}

void Bcf::FlashBmf(Addr::Bmf::Location loc, const std::string &filename)
{
    using Addr::Bcf::Prog_Control;
    using Addr::Bcf::Prog_Status;
    fs::path progfile(filename);

    if (!exists(progfile)) {
        std::cerr << "File " <<filename <<" does not exist\n";
        return;
    }

	// disable jtag override
	JtagOverride(false);

    unsigned int filesize = fs::file_size(progfile);
    std::ifstream file(filename.c_str());
	std::cout << "Performing soft reset...\t";
    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.Reset = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
        ctrl.Reset = 0;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
    } else {
        Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.Reset = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
        ctrl.Reset = 0;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
    }

    while (ProgIsBusy(loc)) {
        // Wait
    }
   
	std::cout << "done\n";
    std::cout << "Initialisation of ProgUnit has been done!\n";

    std::cout << "Perfoming chiperase... " << std::flush;
    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
		ctrl.Reset = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.ChipErase = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
    } else {
        Prog_Control ctrl;
		ctrl.Reset = 0;
		ctrl.ProgramPage= 0;
		ctrl.Jtag = 0;
		ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
        ctrl.ChipErase = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
    }

    while (ProgIsBusy(loc)) {
        // Wait (long)
        sleep(1);
    }

    std::cout << "done!\n";

    boost::crc_32_type crc_calc;

    std::cout << "Flashing firmware...\t";
	//std::cin.get();
    boost::progress_display pbar(filesize);
    unsigned int progcnt = 0;
    while (progcnt < filesize) {
        char c;

        // read one byte from file
        file.get(c);

        // process data for crc calculation
        crc_calc.process_byte(c);

        // increment progcnt
        progcnt++;
        ++pbar;

        // write data to page fifo
        if(loc == Addr::Bmf::North)
        {
            WriteRegister(Addr::Bcf::ProgNorthData, c);
            if(progcnt%256 == 0)
            {
                Prog_Control ctrl;
				ctrl.ChipErase = 0;
				ctrl.Reset= 0;
				ctrl.Jtag = 0;
				ctrl.CalcCrc = 0;
				ctrl.Reserved = 0;
                ctrl.ProgramPage = 1;
                WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
                while(ProgIsBusy(loc)) {
                    //Wait
                }
            }
        }
        if(loc == Addr::Bmf::South)
        {
            WriteRegister(Addr::Bcf::ProgSouthData, c);
            if(progcnt%256 == 0)
            {
                Prog_Control ctrl;
				ctrl.ChipErase = 0;
				ctrl.Reset= 0;
				ctrl.Jtag = 0;
				ctrl.CalcCrc = 0;
				ctrl.Reserved = 0;
                ctrl.ProgramPage = 1;
                WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
                while(ProgIsBusy(loc)) {
                    //Wait
                }
            }
        }
    }

    // need to perform a last page programming?
    if(progcnt % 256 != 0)
    {
        if(loc == Addr::Bmf::North)
        {
            Prog_Control ctrl;
            ctrl.ChipErase = 0;
			ctrl.Reset= 0;
			ctrl.Jtag = 0;
			ctrl.CalcCrc = 0;
			ctrl.Reserved = 0;
            ctrl.ProgramPage = 1;
            WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        if(loc == Addr::Bmf::South)
        {
            Prog_Control ctrl;
			ctrl.ChipErase = 0;
			ctrl.Reset= 0;
			ctrl.Jtag = 0;
			ctrl.CalcCrc = 0;
			ctrl.Reserved = 0;
            ctrl.ProgramPage = 1;
            WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }   
    }
    std::cout << "Flashing has been done!\n";

    // start verify
    std::cout << "Starting verification of flash content...\t";
    SetProgFlash(loc, filesize - 1);

    if (loc == Addr::Bmf::North) {
        Prog_Control ctrl;
	ctrl.ChipErase = 0;
	ctrl.Reset= 0;
	ctrl.Jtag = 0;
        ctrl.ProgramPage = 0;
	ctrl.Reserved = 0;
        ctrl.CalcCrc = 1;
        WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
        while(ProgIsBusy(loc)) {
            //Wait
        }
    } else {
        Prog_Control ctrl;
	ctrl.ChipErase = 0;
	ctrl.Reset= 0;
	ctrl.Jtag = 0;
        ctrl.ProgramPage = 0;
	ctrl.Reserved = 0;
        ctrl.CalcCrc = 1;
        WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
        while(ProgIsBusy(loc)) {
            //Wait
        }
    }
    uint32_t flash_crc = ReadCrc(loc);
    uint32_t file_crc = crc_calc.checksum();
    std::cout << "CRC-File: 0x" << std::hex << file_crc << std::dec << std::endl;
    std::cout << "CRC-Flash: 0x" << std::hex << flash_crc << std::dec << std::endl;
    if(flash_crc != file_crc)
    {
        std::cout << "Verification failed!\n";
    } else {
        std::cout << "done!\n";
        // start jtag programming
        std::cout << "Start JTAG-programming\t";
        SetProgFlash(loc, 0);
        if (loc == Addr::Bmf::North) {
            Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.Reset= 0;
       		ctrl.ProgramPage = 0;
        	ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
            ctrl.Jtag = 1;
            WriteRegister(Addr::Bcf::ProgNorthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        if (loc == Addr::Bmf::South) {
            Prog_Control ctrl;
		ctrl.ChipErase = 0;
		ctrl.Reset= 0;
       		ctrl.ProgramPage = 0;
        	ctrl.CalcCrc = 0;
		ctrl.Reserved = 0;
            ctrl.Jtag = 1;
            WriteRegister(Addr::Bcf::ProgSouthCtrl, *reinterpret_cast<int*> (&ctrl));
            while(ProgIsBusy(loc)) {
                //Wait
            }
        }
        std::cout << "done\n";
    }


}

void Bcf::WriteRegister(boost::uint16_t address, boost::uint8_t value)
{

	//printf("Writing to 0x%04X value 0x%02X\n", address, value); 
    boc.SingleWrite(address, value);
}

boost::uint8_t Bcf::ReadRegister(uint16_t Reg)
{
	boost::uint8_t ret = boc.SingleRead(Reg);
	//printf("Read from 0x%04X value 0x%02X\n", Reg, ret);
    return ret;
}

 bool Bcf::ProgIsBusy(Addr::Bmf::Location loc)
 {
    using Addr::Bcf::Prog_Control;
    using Addr::Bcf::Prog_Status;
    int statint;
    if (loc == Addr::Bmf::North) {
        statint = ReadRegister(Addr::Bcf::ProgNorthStatus);
    } else {
        statint = ReadRegister(Addr::Bcf::ProgSouthStatus);   
    }
    Prog_Status stat = *reinterpret_cast<Prog_Status*> (&statint);
    return stat.Busy;

 }

 inline void Bcf::SetProgFlash(Addr::Bmf::Location loc, int value) 
 {

    using Addr::Bcf::Prog_Control;
    using Addr::Bcf::Prog_Status;
    if (loc == Addr::Bmf::North) {
        boc.SingleWrite(Addr::Bcf::ProgNorthFladdr0, value & 0xFF); 
        boc.SingleWrite(Addr::Bcf::ProgNorthFladdr1, (value >> 8) & 0xFF); 
        boc.SingleWrite(Addr::Bcf::ProgNorthFladdr2, (value >> 16) & 0xFF); 
    } else {
        boc.SingleWrite(Addr::Bcf::ProgSouthFladdr0, value & 0xFF); 
        boc.SingleWrite(Addr::Bcf::ProgSouthFladdr1, (value >> 8) & 0xFF); 
        boc.SingleWrite(Addr::Bcf::ProgSouthFladdr2, (value >> 16) & 0xFF); 
    }
 }

 inline uint32_t Bcf::ReadCrc(Addr::Bmf::Location loc)
 {

    using Addr::Bcf::Prog_Control;
    using Addr::Bcf::Prog_Status;
    uint32_t ret = 0;
    if (loc == Addr::Bmf::North) {
        ret = ReadRegister(Addr::Bcf::ProgNorthCrc0);
        ret |= ReadRegister(Addr::Bcf::ProgNorthCrc1) << 8;
        ret |= ReadRegister(Addr::Bcf::ProgNorthCrc2) << 16;
        ret |= ReadRegister(Addr::Bcf::ProgNorthCrc3) << 24;
    } else {
        ret = ReadRegister(Addr::Bcf::ProgSouthCrc0);
        ret |= ReadRegister(Addr::Bcf::ProgSouthCrc1) << 8;
        ret |= ReadRegister(Addr::Bcf::ProgSouthCrc2) << 16;
        ret |= ReadRegister(Addr::Bcf::ProgSouthCrc3) << 24;
    }
    return ret;
 }

