#ifndef BMF_H
#define BMF_H

#include <cstdint>
#include "TxChannel.h"
#include "RxChannel.h"


class Boc;

class Bmf {
public:

    void Init();

    void WriteRegister(boost::uint16_t address, boost::uint8_t value);
    boost::uint8_t ReadRegister(int type);

    TxChannel GetTxChannel(int channel);
    RxChannel GetRxChannel(int channel);
    Addr::Bmf::Location GetLocation();
    Boc& GetBoc();
private:
    friend class Boc;

    Bmf(Boc& boc, Addr::Bmf::Location loc);

    Boc& boc;
    const Addr::Bmf::Location loc;

};

#endif

