#include "Options.h"
#include <string>
#include "boost/algorithm/string.hpp"

Options::Options() {
    
}

Options::Options(int c, char* v[]) : argc(c), argv(v) {
    this->init();
}

void Options::init(int c, char* v[])
{
	if(c) argc = c;
    if(v) argv = v;
    
    version = "1.0";
	desc = new po::options_description("Options");
	desc->add_options()
	("help,h", "Print help and exit")
	("version,v","Print version and exit")
	("fei4channel,c", po::value<int>(&Fei4Channel)->default_value(2), "Specify the channel to with the FE-I4 is connected")
	("ip", po::value<std::string>(&Ip)->default_value("192.168.0.10"), "Specify the BOC's IP address" )
	("image",  po::value<std::string>(&ImageName)->default_value("uni_wuppertal.bmp"), "Image To load into the FE-I4")
	("verbose,V", po::value<bool>(&verbose)->default_value(false), "Verbose output")
	("flashfile,f", po::value<std::string>(&flashfile)->default_value(""), "File to use to flash")
	("target", po::value<std::string>(&target), "north or south BMF")
	;

	try {
		po::store(po::parse_command_line(argc, argv, *desc), vm);
		po::notify(vm);
	} catch (...) {
		std::cout << *desc << std::endl;
		exit(1);
	}

	if (vm.count("help")) {
		std::cout << *desc << std::endl;
		exit(0);
	}

	if (vm.count("version")) {
		std::cout << version << std::endl; 
		exit(0);
	}
}


std::string Options::GetIp()
{
	return Ip;
}

int Options::GetFei4Channel()
{
	return Fei4Channel;
}

std::string Options::GetImageName()
{
	return ImageName;
}

bool Options::Verbose()
{
	return verbose;
}

std::string Options::GetFlashFile()
{
	return flashfile;
}

Addr::Bmf::Location Options::GetTarget()
{
	boost::to_lower_copy(target);
	if (boost::contains(target, "north")){
		return Addr::Bmf::North;
	} else {
		return Addr::Bmf::South;
	}
}

std::string Options::GetTargetName() {
    return target;
}
