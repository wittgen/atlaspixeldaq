#include <MiniDCSClient.h>

#include <string>
#include <iostream>

int main(int argc, char** argv) {

	MiniDCSClient miniDcsClient("localhost", "3042");
	
	while(1) {
		std::string cmd;
		std::cout << "> ";
		std::getline(std::cin, cmd);
		if(cmd.find("quit") == 0 || cmd.find("exit") == 0) break;
		
		miniDcsClient.sendCommand(cmd);
	}

	return 0;	
}
