#include <FEI4.h>
#include <FEI4Tools.h>
#include <SlaveTools.h>

int main(int argc, char** argv) {
	
	FEI4 fei4_module;
	fei4_module.setupRod(argc, argv);
	AbstrRodModule* rod0 = fei4_module.getRod();
	int slave = 0; // North
	//slave = 1; // South

        int linkEn = 0x1;
	int bocResult;

        cout << "Setting on slave " << slave << " and reading back: link_enabled = 0x" << hex << linkEn << dec << endl;
        writeDevice(rod0, slave, 0x0, linkEn);
        bocResult = readSlave(rod0, slave, 0x0);
        cout << "Slave " << slave << " register 0x" << hex << 0x0 << " read: 0x" << hex << bocResult << endl;

        cout << "Resetting the INMEM FIFO" << endl;
        writeSlave(rod0, slave, 0x41, 0x00000001);
	
	for(int i = 0 ; i < 10 ; i++)
		cout << i << "\t0x" << hex << readDevice(rod0, slave, i) << dec << endl;	
	int i = 40; cout << i << "\t0x" << hex << readDevice(rod0, slave, i) << dec << endl;	
	i = 43; cout << i << "\t0x" << hex << readDevice(rod0, slave, i) << dec << endl;	

	return 0;

	cout << "Waiting for you..." << endl;
	cin.get();
	cout << "Let's go." << endl;
	int v;
	bool fifoEmpty (false);
	while(!fifoEmpty) {
		//int v = readSlave(rod0, slave, 0x40);
		static int cnt(0);
		if(cnt == 0xFF) break;
		//v = readDevice(rod0, slave, 0x40);
		v = readSlave(rod0, slave, 0x40);
		cout << hex << (int) v << dec << "\t";
		if(v==0x800) fifoEmpty = true;
		if(!((++cnt)%16)) cout << endl;
		else cout.flush();
	}
	cout << endl;
	if(fifoEmpty) cout << "Reading aborted because FIFO is empty." << endl;

	return 0;
}
