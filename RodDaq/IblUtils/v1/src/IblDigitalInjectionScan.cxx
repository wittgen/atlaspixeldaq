#include <FEI4.h>
#include <FEI4Tools.h>
#include <SlaveTools.h>
#include <FEI4DigitalInjection.h>

#include <FEI4.h>

#include <cstdlib>

int main(int argc, char** argv) {

#if 0
	cout << "Starting ROD..." << flush;
	int ret = system("iblRodPPC start+slaves >/dev/null");
	if(ret) {
		cout << "Could not restart ROD." << endl;
		return 1;
	}
	cout << "Done." << endl;
#endif

	FEI4 fei4_module;
	fei4_module.setupRod(argc, argv);
	AbstrRodModule* rod0 = fei4_module.getRod();
	int slave = 0;
	slave = 1;
	int channel = 1;
	int rx_ch(0), tx_ch(0), chipID(0);
	bool isOptical(true);

	rx_ch = channel;
	tx_ch = channel;


/*


	isOptical = false;

	channel = 0;
	chipID = 6;

	if(isOptical) 

	switch(channel) {
		case 1: rx_ch = 0; tx_ch = 1; break;
		case 2: rx_ch = 2; tx_ch = 2; break;
		case 3: rx_ch = 4; tx_ch = 3; break;
		case 4: rx_ch = 6; tx_ch = 4; break;
		default:
			cout << "Channel " << channel << " is not implemented yet." << endl;
			return 1;
	}

	else

	switch(channel) {
		case 0: rx_ch = 0; tx_ch = 0; break;
		case 1: rx_ch = 1; tx_ch = 1; break;
	}

	//disableAllChannels(rod0, slave);
*/
	if(getenv("NO_FEI4_INIT")) { std::cout << "FEI4 initialization skipped by ENV NO_FEI4_INIT" << std::endl; }
	else initPixelLabIblSetup(fei4_module, slave, tx_ch, rx_ch, chipID, isOptical);


	TH2F *hist = digitalInjection(fei4_module);
	//if(hist->GetEntries()) hist->Print("all");

	return 0;

	//for(slave=0;slave<2;slave++) { }
	for(channel=1;channel<2;channel++) {
	std::cout << "SLAVE " << slave << ", CHANNEL " << channel << std::endl;
	switch(channel) {
		case 0:
		case 1:
		  fei4_module.setChipID(0); // Timon
			break;
		case 2:
		  fei4_module.setChipID(6); // Yosuke
			break;
		default:
		  fei4_module.setChipID(0); // Timon
			break;
	}
	setSlaveReadBack(rod0, slave, channel);
	fei4_module.init();
	//continue;

	/*
	cout << "Setting value of reg #2 to 0x1800 << endl;
        WriteReg(rod0, 2, 0x1800);
	*/

        // resetting the INMEM FIFO
        cout << "Resetting the INMEM FIFO" << endl;
        writeSlave(rod0, slave, 0x41, 0x00000001);
///*
        readRegister(rod0, slave, 2);
//        fei4_module.getRegister(2);
//*/
	//return 0;
	//break;
	TH2F *hist = digitalInjection(fei4_module);
	if(hist->GetEntries()) hist->Print("all");


	}
	return 0;
}
