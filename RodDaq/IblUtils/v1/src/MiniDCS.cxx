#include <MiniDCS.h>
#include <LogStream.h>
#include <MiniDCSClient.h>

#include <signal.h>
#include <iostream>

MiniDCS miniDcs; // Needs to be global for signal handling

void cleanExit(int signal) {
	miniDcs.deactivateAll();
	//sleep(miniDcs.getDelay());
	miniDcs.cleanExit(signal);
}

void resetPower(int signal) {
	miniDcs.resetPower(signal);
}

int main(int argc, char** argv) {
	int delay(5);
	char c;

	signal(SIGINT,cleanExit);	
	signal(SIGTERM,cleanExit);	
	signal(SIGPIPE,cleanExit);	
	signal(SIGUSR1,resetPower);
	signal(SIGUSR2,cleanExit);

	while ( (c = getopt(argc, argv, "vd:l:") ) != -1) {
		switch(c) {
			case 'd':
				delay = atoi(optarg);
				break;
			case 'l':
				logStream(optarg); // Close default location and open as requested
				cout << "Log file set to " << logStream.file << endl;
				break;
			case '?':
				cout << "Unkown option" << endl;
		}
	}

	teeOut << string(40,'-') << endl;
	teeOut << "MiniDCS v1.0" << endl;
	teeOut << "Contact: karolos.potamianos@cern.ch" << endl;
	teeOut << string(40,'-') << endl;
	
	teeOut << "Setting refresh rate to: " << delay << " seconds (use -d change the rate)." << endl;
	
	miniDcs.setDelay(delay);
	miniDcs.start();
	miniDcs.getThread()->join();

	cleanExit(0);

	return 0;
}
