INCLUDES += $(addprefix -I, $(shell $(FIND) src -mindepth 1 -type d) inc)
INCLUDES += -I$(TDAQ_PATH)/rcc_error
INCLUDES += -I$(TDAQ_PATH)/vme_rcc
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/CommonWithDsp
INCLUDES += -I$(PIXEL_DAQ_ROOT)/PixLibInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/PixDaq/common/DspInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/PixDaq/common/DspInterface/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common/DspInterface
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common/DspInterface/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/System
INCLUDES += -I$(BOOST_INC)


# For building the MDSP code
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/Common/PIX
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/common
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodDaq/IblDaq/RodMaster/Software/PPC
INCLUDES += -I$(PIXEL_DAQ_ROOT)/RodMaster/Software/bsp/standalone_bsp_lwip/ppc440_0/include



# These directories also contain libraries to link against
DEPDIRS += $(PIXEL_DAQ_ROOT)/RodDaq/RodCrate
DEPDIRS += $(PIXEL_DAQ_ROOT)/VmeInterface

LDFLAGS += $(foreach DIR, $(DEPDIRS), $(call extDeps,$(DIR)))
INCLUDES += $(addprefix -I, $(DEPDIRS))

INCLUDES += -I$(shell root-config --incdir)
