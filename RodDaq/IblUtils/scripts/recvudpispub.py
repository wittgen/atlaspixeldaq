#!/bin/env python
# Author: K. Potamianos <karolos.potamianos@cern.ch>
# Date: 2017-II-9
# Description: quick and easy implementation of publishing
#              of information obtained from UDP into IS, for
#              example RODsysmon or scan status information
# Setting up TDAQ infrastructure
import os,sys,time
import threading

from ispy import *
partName = os.getenv("PIX_PART_INFR_NAME","PixelInfr")
ipcPartition = IPCPartition(partName)
if not ipcPartition.isValid():
    print "Partition",partName,"does not exit"
    sys.exit(1)
isInfoDict = ISInfoDictionary(ipcPartition)
varnames=["maskStep","par0","par1","par2","errorState","scanState"]

#Update Is variables according to values contained in a list
def updateIsArray(addr, data,varnames=[],vartype='U32'):
    print data
    for ientry in xrange(len(data)):
        updateIs(addr,data[ientry],vartype,varnames[ientry])

# Main routine, defines how to process the payload from UDP packets
def updateIs( addr, data , vartype='String', varname=''):
    val = ISInfoDynAny(ipcPartition,vartype)
    val["value"] = data
    rodIsString = getRodStringFromIp(addr) + "/UDPInfo/"+varname
    #print "Will write to IS:"
    #print rodIsString,val
    if isInfoDict.contains(rodIsString):
        isInfoDict.update(rodIsString, val)
    else: isInfoDict.insert(rodIsString, val)
# Helper to get the IS base string from the IP address
def getRodStringFromIp( ip_addr ):
    ip_addr_c = ip_addr[0].split('.')
    if ip_addr[0] == '127.0.0.1': # Loopback Address, for testing
        return "RunParams.TestFolder"
        rodNumber = 18
    elif ip_addr_c[0] == '192' and ip_addr_c[1] == '168': # SR1
        #crateNumber = int(ip_addr_c[2]) - 10  # we are still at the old numbering scheme
        crateNumber = int(ip_addr_c[2])
        rodNumber   = int(ip_addr_c[3]) / 10

        if (rodNumber !=9):
            crateNumber=3
        if (rodNumber==7):
            crateNumber=1
            
        crateShort = "C" + str(crateNumber)
        #crateName = "ROD_CRATE_" + crateShort
        crateName = "ROD_CRATE"
        #rodNumber = int(ip_addr_c[3]) - 200
        
    elif ip_addr_c[0] == '10' and ip_addr_c[1] == '145': # P1
        crateNumber = int(ip_addr_c[2]) - 96
        crateShort = 'L' + str(crateNumber)
        crateName = "ROD_CRATE_" + crateShort
        rodNumber = int(ip_addr_c[3]) - 200
    return "RunParams.{}/ROD_{}_S{}".format(crateName, crateShort, rodNumber)
# Listen for UDP datagrams
import socket
#UDP_IP = "127.0.0.1"
UDP_IP=""
UDP_PORT = 10426
sock = socket.socket(socket.AF_INET, # Internet
socket.SOCK_DGRAM) # UDP
sock.bind((UDP_IP, UDP_PORT))


while True:
    data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
    print "received message from {}:".format(addr)
    convertedData = map(ord,data) #To convert from hexadecimal string to a list of integers            
    #print convertedData
    #updateIsArray(addr, convertedData,varnames,'U32')
    
    #multi-Threading server
    t = threading.Thread(target=updateIsArray,args=(addr,convertedData,varnames,'U32',))
    t.setDaemon(True) #Thread keeps on going if server dies or whatever
    t.start()
    
    
    
    
