#!/bin/env python

from scapy.all import *
from random import randint

conf.L3socket=L3RawSocket

DST_ADDR = "192.168.1.141"
UDP_PORT = 10425

for c in range(1,5):
    for s in [x for x in range(5,22) if x != 13]:
        SRC_ADDR = "192.168." + str(10+c) + "." + str(100+s)
        # Adjust data as needed
        data = "This is ROD_L" + str(c) + "_S" + str(s)
        SRC_PORT = random.randint(1025,65535)
        spoofed_packet = IP(src=SRC_ADDR, dst=DST_ADDR) / UDP(sport=SRC_PORT, dport=UDP_PORT) / data
        send(spoofed_packet)
