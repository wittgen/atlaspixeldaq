#!/bin/env python

import socket

UDP_IP=""
UDP_PORT = 10425

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
sock.bind((UDP_IP,UDP_PORT))

while True:
    data,addr = sock.recvfrom(1024)

    print "received message from ", addr, ":", data
