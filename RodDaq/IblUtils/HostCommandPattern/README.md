# HostCommand pattern

Executables from HOST to ROD, you can provide hostName, rodName or ip to talk to the ROD. Rest of arguments depend on the command issued.

E.g. HCP_xxx --rodName ROD_C1_S17


# Description of the different HCP

- HCP_mccConnScan

Connectivity scan for FE-I3, it runs an opto scan per single Tx channel and retrieve the information of the number of good data points (no communication errors) in the Rx channels. The ouput is a table with the Tx/Rx connectivity.
```
Optional arguments:
--ch arg (=0xFFFFFFFF)     Specify the Rx channel mask
--txMask arg (=0xFFFFFFFF) Specify Tx mask
--thrStart arg (=1000)     Threshold start
--thrStop arg (=41000)     Threshold stop
--thrStep arg (=5000)      Threshold step
--delStart arg (=0)        Delay start
--delStop arg (=900)       Delay stop
--delStep arg (=100)       Delay step
```

- HCP_iblChannelMapping

Connectivity scan for FE-I4, it send some default configuration to registers 20 (Threshold high),14-16 (LVDS and PLL) and 26-29 (PLL, speed, CLK, LVx). This is because iblChannelMapping is meant to work even with unconfigured modules. Afterwards, single Tx channels are scanned, in which different chip IDs (4-7) are loop over, finally the chip serial number is retrieved per Rx channel. The output are two table with the Tx/Rx mapping, one with the chipID and another one with the serial number.
```
Optional arguments:
--ch arg (=0xFFFFFFFF)     Specify the Rx channel mask
--txMask arg (=0xFF00FF) Specify Tx mask
```

- Add more...

