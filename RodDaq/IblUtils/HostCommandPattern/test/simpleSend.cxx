// Steve <alkire@cern.ch>
// 2014.03.01
#include "DefaultClient.h"
#include "RodCommand.h"
#include "PixScanBaseToRod.h"
#include "PixScanBase.h"
#include "StartScan.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv) {
  Options opt(argc, argv);

  //make your command class 
  PixScanBaseToRod command;
  StartScan command2;
  command2.channels = opt.getChannels();
  SerializablePixScanBase sb;

  PixLib::EnumMaskSteps::MaskStageSteps enumsareannoying;
  enumsareannoying = PixLib::EnumMaskSteps::STEPS_8;
  sb.setMaskStageTotalSteps(enumsareannoying);
  sb.setMaskStageSteps(1);
  //  sb.setMaskStageMode(SCAN_SEL); // who cares for now...
  sb.setRepetitions(2);
  sb.setConsecutiveLvl1Trig(3);
  sb.setStrobeLVL1Delay(16);
  sb.setStrobeLVL1DelayOveride(0);
  sb.setLVL1Latency(255 - 8 - sb.getStrobeLVL1Delay() - sb.getConsecutiveLvl1Trig/2);

  sb.setStrobeDuration(10);
  sb.setStrobeMCCDelay(0);
  sb.setDigitalInjection(true);
  sb.setRestoreModuleConfig(false);

  //set stuff
  command.setPixScanBase(sb);
  
  //the rest
  DefaultClient client(opt);
  client.run(command);
  sleep(5);
  client.run(command2);

  std::cout << command.result.success << " <-sent pix scan base?" << std::endl;
  
  return 0;
}
