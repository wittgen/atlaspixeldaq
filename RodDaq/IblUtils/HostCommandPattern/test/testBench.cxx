// Steve <alkire@cern.ch> 2014.03.13
// To start collecting tests.
#include "Server.h"
#include "Client.h"

#include "GetStatus.h"
#include "StartScan.h"
#include "TestBenchCommand.h"
#include "Get.h"
#include "Stage.h"
#include "WritePixelRegisterConfig.h"
#include "WriteConfig.h"
#include "ItersAndAlgorithms.h"

#include <iostream>
#include <cstring>
#include <unistd.h>
#include <string>


#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

class TestBenchClient: public Client {
 public:
  TestBenchClient(const char* a): Client(a){}
  virtual void run(){}
  template<typename T> void run(T& command){
    if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
      return;
    }
    std::cout << "setting client IP = " << srvIpAddr() << std::endl;
    std::cout << "Connected to server." << std::endl;
    std::cout<<"about to send command"<<std::endl;
    server.sendCommand(command);
    std::cout<<"...command sent"<<std::endl;
    std::cout << "Disconnecting from server...";
    server.disconnectFromServer();
    std::cout << "Done." << std::endl;
  }  
};

#ifndef _IS_TEST
#define _IS_TEST(YourTest) strcmp(argv[1], #YourTest ) == 0
#endif

int main(int argc, const char** argv) {
  //Add tests here
  //-----------------------------------

  if(_IS_TEST(TestGetSpyStatus)){
    GetStatus command;
    TestBenchClient client(IBL_ROD_IP);
    client.run(command);
    std::cout<<"               at mask step: "<<(int)command.result.m_status.mask<<
      ", at par step: "<<(int)command.result.m_status.par<<
      ", at DC step: "<<(int)command.result.m_status.dc<<
      ", at trigger step: "<<(int)command.result.m_status.trig<<
      ", and state enum: "<<(int)command.result.m_status.state<<std::endl;
  }else if(_IS_TEST(TestBenchCommand)){
    TestBenchCommand command;
    TestSerialize ts;
    TestSerialize2<4> ts2;
    ts2.testBits[0] = 0;
    ts2.testBits[1] = 1;
    ts2.testBits[2] = 2;
    ts2.testBits[3] = 3;
    
    ts2.testBit = 5;
    ts.testSerialize = ts2;
    command.m_ts = ts;
    
    TestBenchClient client(IBL_ROD_IP);
    client.run(command);
//  }else if(_IS_TEST(HowDoIFeel)){
//    Set<std::string,Stage::getHowDoIFeel> command;
//    command.object = "Pretty pumped!";
//    TestBenchClient client(IBL_ROD_IP);
//    client.run(command);
//    Get<std::string,Stage::getHowDoIFeel> command2;
//    client.run(command2);
//    std::cout << command2.result.object <<std::endl;
  }else if(_IS_TEST(TestConfig)){
//     Fei4PixelCfg fe1;
//     Fei4ExtCfg fe3;
//     Fei4Cfg fe4;
    
//     DoubleColumnBit mydcb;
//     mydcb.set(0xABCD);
//     fe1.dcb(0,0) = mydcb;
//     WriteConfig<Fei4PixelCfg> command1;
//     command1.moduleConfig.push_back(fe1);


//     uint16_t blank_cfg[Fei4Cfg::numberOfRegisters]={0};
    
//     //WriteConfig<Fei4GlobalCfg> command2;
//     //command2.moduleConfig.push_back(fe2);
//     WriteConfig<Fei4ExtCfg> command3;
//     fe3.setCfg(blank_cfg);
//     command3.moduleConfig.push_back(fe3);

//     WriteConfig<Fei4Cfg> command4;
//     fe4.dcb(0,0) = mydcb;
//     fe4.setCfg(blank_cfg);
//     command4.moduleConfig.push_back(fe4);
    
//     TestBenchClient client(IBL_ROD_IP);

//     client.run(command1);
//     client.run(command3);
//     client.run(command4);

  }else if(_IS_TEST(SetPixelTest)){

    Fei4PixelCfg fe;
    DoubleColumnBit dcb;

    DoubleColumnBit dcbf_array[4];
    DoubleColumnBitField<4,true> dcbf(dcbf_array) ;

    fe.dcb(0,0).setPixel(1,1,1);
    std::cout << fe.dcb(0,0).getPixel(1,1) << std::endl;

    fe.TDAC(0).setPixel(2,1,4);
    std::cout << fe.TDAC(0).getPixel(2,4) << std::endl;

    dcb.setPixel(3,4,1);
    std::cout <<  dcb.getPixel(3,4) <<std::endl;

    dcbf.setPixel(8 , 10 , 3);
    std::cout <<  dcbf.getPixel(8, 10) << std::endl;


  }else if(_IS_TEST(SendDoubleColumnBit)){

    /*
    Set<DoubleColumnBit,Stage::testFei4PixelCfg_f> command;
    command.object.set(0x5555);
    TestBenchClient client(IBL_ROD_IP);
    client.run(command);
    Get<DoubleColumnBit,Stage::testFei4PixelCfg_f> command2;
    client.run(command2);
    command2.result.object.dump();
    */
  }else if(_IS_TEST(WholePixel)){
    /*
    ExtFei4PixelCfg fe;
    ExtFei4PixelCfg fe2;
    DoubleColumnBit dc1;
    dc1.set(0x5555);
    fe.dcb[0][1] = dc1;
    fe.dcb[5][5] = dc1;
    CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> it(&fe.dcb[0][0]);
    CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> it2(&fe2.dcb[0][0]);
    
    TestBenchClient client(IBL_ROD_IP);
    IterSet<DoubleColumnBit,Stage::testFei4PixelCfg_f,CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> > command(it);
    IterGet<DoubleColumnBit,Stage::testFei4PixelCfg_f,CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> > command2(it2);
    unsigned count = 0;
    do{
      if(count%1 == 0) std::cout << "Command #: " << ++count << std::endl;
      command.object = *(DoubleColumnBit*)command.shared.home();
      client.run(command);
    }while((DoubleColumnBit*)command.shared.home() != fe.dcb[0]);
    count = 0; 
    do{
      if(count%1 == 0) std::cout << "Command #: " << ++count << std::endl;
      client.run(command2);
      *(DoubleColumnBit*)command2.shared.home() = command2.result.object;
    }while((DoubleColumnBit*)command2.shared.home() != fe2.dcb[0]);
    std::cout << "store: ";
    fe2.dcb[0][1].dump();
    std::cout << std::endl;
    */
  }else if(_IS_TEST(TestIter)){
    /*
    TestBenchClient client(IBL_ROD_IP);
    DoubleColumnBit one[5];
    one[0].set(0x5555); one[3].set(0x1111);
    DoubleColumnBit two[5];
    CircularIterator<DoubleColumnBit, 5> it(&one[0]);
    CircularIterator<DoubleColumnBit, 5> it2(&two[0]);
    std::cout << "From CircularIterator " << (DoubleColumnBit*)it << std::end;
    unsigned count =0;
    IterSet<DoubleColumnBit,Stage::testIter_f,CircularIterator<DoubleColumnBit,5> > command(it);
    IterGet<DoubleColumnBit,Stage::testIter_f,CircularIterator<DoubleColumnBit,5> > command2(it2);
    std::cout << "Actual pointer " << &one[0] << " " << &one[1] <<std::endl;
    do{
      if(count%1 == 0) std::cout << "Command #: " << ++count << std::endl;
      std::cout << "Casting pointer or referencing? " << (DoubleColumnBit*)command.shared.home() << std::endl;
      std::cout << "Actual pointer " << &one[0] << " " << &one[1] <<std::endl;
      //(*(DoubleColumnBit*)command.shared.home()).dump();
      //command.object = *(DoubleColumnBit*)command.shared.home();
      client.run(command);
    }while((DoubleColumnBit*)command.shared.home() != &one[5]);
    do{
      if(count%1 == 0) std::cout << "Command #: " << ++count << std::endl;
      client.run(command2);
      *(DoubleColumnBit*)command2.shared.home() = command2.result.object;
    }while((DoubleColumnBit*)command2.shared.home() != &two[5]);
    std::cout << "store: ";
    two[3].dump();
    */
  }
  else{
    std::cout << "./testBench YourTest" << std::endl;
    return 0;
  }

  //------------------------------------
  std::cout << "Done." << std::endl;  

  return 0;
}
