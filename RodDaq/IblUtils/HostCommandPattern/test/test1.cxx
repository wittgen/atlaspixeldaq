#include "RodCommand.h"
#include "Server.h"
#include "Client.h"

#include <cstring>
#include <iostream>
using namespace std;

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

void usage(const char *programName) {
    cout << "Run as server: " << programName << " -s\n";
    cout << "Run as client: " << programName << " -c\n";
}

int main(int argc, const char** argv) {

    if (argc != 2) {
        usage(argv[0]);
        return 0;
    }

    if (strcmp(argv[1], "-s") == 0) {
        Server server;
        server.run();
    } else if (strcmp(argv[1], "-c") == 0) {
        //Client client("192.168.1.80", 7); // Connecting to echo server
        Client client(IBL_ROD_IP);
        client.run();
    } else {
        usage(argv[0]);
    }

    return 0;

}
