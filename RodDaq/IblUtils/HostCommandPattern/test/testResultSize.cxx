#include "DefaultClient.h"
#include "TestResultSize.h"

#include <ctime>
#include <cstring>
#include <iostream>

int main(int argc,  char** argv) {
  Options opt(argc, argv);

  TestResultSize cmd;
  
  DefaultClient client(opt);
  client.run(cmd);

  for (int i=0; i < ARRAY_SIZE; i++) 
    std::cout<<"Array element "<<i<<" is: "<<cmd.result.myArray[i]<<std::endl;
  


    return 0;

}
