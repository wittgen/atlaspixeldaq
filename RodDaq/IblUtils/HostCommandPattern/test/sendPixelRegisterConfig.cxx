// Russell Smith
// 17 March 2014
// 
// Will write the PixelRegister configuration to the ppc 

#include "DefaultClient.h"
#include "RodCommand.h"
#include "SendPixelRegisterConfig.h"
//#include "StartScan.h"
//#include "WriteModuleConfig.h"
#include "Fei4PixelCfg.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

int main(int argc, char** argv) {
  Options opt(argc, argv);

  uint32_t channels = opt.getChannels();

  SendPixelRegisterConfig sendPixRegCfg;  //when making send command, will probably want to call this also
  Fei4PixelCfg  dbPixelRegCfg; //make a test one to check that we can change and then send

//   for(  unsigned dc = 0; dc < Fei4PixelCfg::numberOfDoubleColumns ; dc++ ){
//     DoubleColumnBitField<5,true> testTDAC;
//     testTDAC.set(0xd , 0);
//     testTDAC.set(0x8 , 1);
 
//     dbPixelRegCfg.setTDAC(dc,  testTDAC);
//   }
//   //will need to serialize pixelcfg
//   SerializableFei4PixelCfg&  serPixelRegCfg = static_cast<SerializableFei4PixelCfg&>(dbPixelRegCfg);
 
//   serPixelRegCfg.dumpPix(0);
//   serPixelRegCfg.dumpPix(1);

  
//   writePixRegCfg.setPixRegConfig(serPixelRegCfg);
//   writePixRegCfg.setFeMask(channels);

  DefaultClient client(opt);
  client.run(sendPixRegCfg);

  std::cout << sendPixRegCfg.result.success << " <-sent Pixel Register Config?" << std::endl;

  return 0;
}

  
