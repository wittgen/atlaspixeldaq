#include "DefaultClient.h"
#include "RodCommand.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc,  char** argv) {
  Options opt(argc, argv);

  NoWaitRodCommand cmd;

  DefaultClient client(opt);  
  client.run(cmd);

  return 0;
}
