#include "KillCommand.h"
#include "RodCommand.h"
#include "DefaultClient.h"

#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdlib.h>

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv){
  int nCmds = 1;
  int nKills = 1;
  int wait = 0;

  for(int i = 0; i < argc; i++){
    std::string str = std::string(argv[i]);
    if      (str.find("--nCmds=")  != std::string::npos) nCmds  = atoi(str.substr(str.find("--nCmds=")+8).c_str());
    else if (str.find("--nKills=") != std::string::npos) nKills = atoi(str.substr(str.find("--nKills=")+9).c_str());
    else if (str.find("--wait=")   != std::string::npos) wait   = atoi(str.substr(str.find("--wait=")+7).c_str());
  }

  Options opt(argc, argv);
  
  if (nCmds < nKills) {
    std::cout << "Number of kills cannot be less than the number of commands. "<< std::endl;
    return 1;
  }

  std::vector<NoWaitRodCommand> running;

  DefaultClient client(opt);  

  for(int i=0; i < nCmds; i++) {
    running.push_back(NoWaitRodCommand());
    client.run(running.back());
  }

  std::cout << wait << std::endl;
  if (wait) usleep(wait*1000000);

  for(int i=0; i < nKills; i++){
    KillCommand killCmd(running[i]);
    client.run(killCmd);
  }

  return 0;
}
