#include "DefaultClient.h"
#include "VeryLargeRodCommand.h"

#include <iostream>
#include <algorithm>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc,  char** argv) {
  Options opt(argc, argv);

  VeryLargeRodCommand cmd;
  for( size_t i = 0 ; i < sizeof(cmd.result.large_array)/sizeof(uint32_t) ; ++i ) cmd.large_array[i] = i+1; // +1 to avoid zero (easy check!)

  DefaultClient client(opt);
  if( client.run(cmd) ) {
	  std::cout << "Execution ID was " << cmd.getExecId() << std::endl;

	  size_t length = sizeof(cmd.result.large_array)/sizeof(uint32_t);
	  std::cout << "Array length is " << length << " elements." << std::endl;
	  while(!cmd.result.large_array[length]) length--;
	  std::cout << length << " items have nonzero value!" << std::endl;
	  //std::copy( cmd.result.large_array, cmd.result.large_array+length, std::ostream_iterator<uint32_t>( std::cout, " " ) );
	  //std::cout << std::endl;
	  if( std::equal( cmd.large_array, cmd.large_array + sizeof(cmd.large_array)/sizeof(uint32_t), cmd.result.large_array ) )
		std::cout << "You'll be glad to learn that the returned large array of " << sizeof(cmd.large_array) << " bytes is the same as the one we sent!" << std::endl;
	  else std::cerr << "ERROR: the array returned by the PPC is not the same as the one we sent!" << std::endl;
  }
  return 0;
}
