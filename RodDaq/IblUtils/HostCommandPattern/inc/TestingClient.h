#ifndef _TESTINGCLIENT_H_
#define _TESTINGCLIENT_H_
// Author: Laura Jeanty
// Moved to its own file: Russell Smith
// April 20 2014

#include "Server.h"
#include "Client.h"
#include "RodCommand.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

class TestingClient: public Client {
 public:
  TestingClient(const char* a): Client(a){}

  template<typename T> void run(T& command){
    if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
      return;
    }
    std::cout << "setting client IP = " << srvIpAddr() << std::endl;
    std::cout << "Connected to server." << std::endl;
    std::cout<<"about to send command"<<std::endl;
    server.sendCommand(command);
    std::cout<<"...command sent"<<std::endl;
    std::cout << "Disconnecting from server...";
    server.disconnectFromServer();
    std::cout << "Done." << std::endl;
  }
  
};
#endif // _TESTINGCLIENT_H_
