// L Jeanty <laura.jeanty@cern.ch>
// Generic options class for command line functions that take ip and/or channels as argument
#ifndef HCMDP_OPTIONS_H
#define HCMDP_OPTIONS_H

#include <arpa/inet.h>
#include <sys/socket.h>
#include <netdb.h>
#include <boost/program_options.hpp>
#include <boost/make_shared.hpp>
#include <string>
#include <iostream>
#include <iomanip>

namespace po = boost::program_options;

using namespace std;
class Options {
public:
  Options(int c, char* v[], std::string t = "", bool fromChild = false);
  Options();
  void register_options();
  virtual void extend_options() { } // To be used by derived classes to extend the options
  void init(int c=0, char* v[]=0);
  ~Options() { };
  void usage();
  static const std::string getIpFromHostName(const std::string & hName);
  const std::string getIpFromHostName();
  static const std::string getHostNameFromRodName(const std::string & rName);
  static const std::string getIpFromRodName(const std::string & rName);
  const std::string getIpFromRodName();
  const std::string getIp();
  void setIp(std::string);
  const uint32_t getChannels();
  const uint8_t getUart();
  const bool getVerbose();
  const uint8_t getSlot();
  const int getReadBackMode();
  const int getShiftRegLeft();
  const bool getGlobal();
  const uint32_t getDelay();
  const uint32_t getValue();
  const uint32_t getAddress();
  const int getSlave();
  const int getSlink();
  const int getRx();
  const int getPrmpVbp();
  const int getPlsrDac();
  const int getGadcInConsole();
  const int getIter();
  void setSlot(uint8_t);
  const int getUserMask();
  const int getnDC();
  const int getcpMode();
  const int getTemp0();
  const int getTemp1();
  const unsigned int getTime();
  const bool getTestInj(); 
  const std::string getFeType();

  // for setNetwork tool
  int getDefault();
  std::string getSIp();
  std::string getSNetmask();
  std::string getSGw();
  std::string getFitFarmIp1();
  std::string getFitFarmIp2();
  int getFitFarmPort1();
  int getFitFarmPort2();

  // for dummyScan
  uint32_t getInjType();
  uint32_t getMultiHit();
  uint32_t getScanType();
  uint32_t getNumOfChips();
  uint32_t getExpOcc();

  // for testRxHisto
  uint32_t getHistoUnit();
  uint32_t getModMask();
  uint32_t getNTrig();
  uint32_t getMode();
  uint32_t getFiltOn();
  uint32_t getReadoutType();
  uint32_t getNMasks();

  void setRodName(std::string rName);

  const std::string& getRodName();
  const std::string& getHostName();
 
  template<typename _T>
  void override_option_default(std::string option_name, _T option_default) {
    const po::option_description& opt_desc = desc->find(option_name, false);
    boost::shared_ptr<const po::value_semantic> cvalue=opt_desc.semantic();
    boost::shared_ptr<po::value_semantic> value = const_pointer_cast<po::value_semantic>(cvalue);
    boost::shared_ptr<po::typed_value<_T>> tvalue = dynamic_pointer_cast<po::typed_value<_T>>(value);
    if(tvalue) tvalue->default_value(option_default);
  }

protected:
  boost::shared_ptr<po::options_description> desc;
  po::variables_map vm;
  
  int argc;
  char** argv;  
  std::string type;
  std::string ip;
  std::string channels;
  int slot;
  int stave;
  int slave;
  int slink;
  int uart;
  bool verbose;
  std::string value_str;
  std::string address;
  int rx;
  int prmp;
  int plsr;
  int gadcin;
  int iter;
  int umask;
  int ndc;
  int cpmode;
  int tm0;
  int tm1;
  unsigned int sleeptime;
  bool testInj;
  std::string feType;

  // for setNetwork tool
  int defaultSettings;
  std::string sIp;
  std::string sNetmask;
  std::string sGw;
  std::string fitFarmIp1;
  std::string fitFarmIp2;
  int fitFarmPort1;
  int fitFarmPort2;
  
  std::string validateIp(std::string);

  uint32_t readoutType;

  // for dummyScan
  uint32_t injType;
  uint32_t multiHit;
  uint32_t scanType;
  uint32_t nChips;
  uint32_t expOcc;

  // for testRxHisto
  uint32_t histoUnit;
  std::string modMasks;
  uint32_t nTrig;
  uint32_t mode;
  uint32_t nMasks;
  std::string rodName;
  std::string hostName;
  uint32_t filter;

  //readback
  int readbackMode;
  uint32_t delay;
  int ShiftRegLeft;
  bool Global;
  
private:
};

Options::Options()
{
}

Options::Options(int c, char* v[], std::string t, bool fromChild)
{
  defaultSettings = 0;
  type = t;
  register_options();
  if(!fromChild) init(c, v);
}

void Options::register_options()
{
  desc = boost::make_shared<po::options_description>("Options");

  desc->add_options()
    ("help,h", "Print help and exit")
    ("ch", po::value<std::string>(&channels)->default_value("0xFFFFFFFF"), "Specify the channel mask")
    ("slot", po::value<int>(&slot)->default_value(0), "Specify the slot (IP or slot, don't need both, slot takes precedence if specified, only works for pit)")
    ("stave", po::value<int>(&stave)->default_value(0), "Specify the stave (stave takes precedence over slot or ip; only works for pit)")
    ("ip", po::value<std::string>(&ip)->default_value("0"), "Specify the ROD's IP address, default to 0" )
    ("rodName", po::value<std::string>(&rodName)->default_value(""), "Specify the ROD's name" )
    ("hostName", po::value<std::string>(&hostName)->default_value(""), "Specify the ROD host name" )
    ("uart", po::value<int>(&uart)->default_value(0), "Specify the Uart (0 = PPC, 1 = Slave 0, 2 = Slave 1)")
    ("verbose", po::value<bool>(&verbose)->default_value(true), "Specify if slave is verbose (1 = on, 0 = off, default is 1)")
    ("value", po::value<std::string>(&value_str)->default_value("0x0"), "value if writing a reg 0x0")
    ("addr", po::value<std::string>(&address)->default_value("0x0"), "address if writing a reg 0x0")
    ("slave", po::value<int>(&slave)->default_value(2), "Specify which slave (0 or 1) or both (2)")
    ("slink", po::value<int>(&slink)->default_value(2), "Specify which slink (0 or 1) or both (2)")
    ("rx", po::value<int>(&rx)->default_value(0), "Specify which rx (one channel only)")
    ("prmp", po::value<int>(&prmp)->default_value(43), "prmp if writing a reg 0")
    ("plsr", po::value<int>(&plsr)->default_value(0), "plsr if writing a reg 0")
    ("gadcin", po::value<int>(&gadcin)->default_value(0), "gadcin if writing a reg 0")
    ("iter", po::value<int>(&iter)->default_value(0), "iter if writing a reg 0")
    ("umask", po::value<int>(&umask)->default_value(0), "umask if writing a reg 0")
    ("ndc", po::value<int>(&ndc)->default_value(0), "ndc if writing a reg 0")
    ("cpmode", po::value<int>(&cpmode)->default_value(0), "cpmode if writing a reg 0")
    ("tm0", po::value<int>(&tm0)->default_value(0), "tm0 if writing a reg 0")
    ("tm1", po::value<int>(&tm1)->default_value(0), "tm1 if writing a reg 0")
    ("sleeptime", po::value<unsigned int>(&sleeptime)->default_value(100), "Specify the sleeping time")
    ("testInj", po::value<bool>(&testInj)->default_value(false), "Specify whether some injection is done or not")
    ("feType", po::value<std::string>(&feType)->default_value("I4"), "Specify the FE type [I3 or I4]")
    //Read back
    ("readbackMode", po::value<int>(&readbackMode)->default_value(0), "Which read back mode you want to use: 0-ReadShiftRegister 1-ClearAndReadBackSR 2-WriteCustomPattern 3-ClearSR 4-ReadPixelRegisters")
    ("delay",po::value<uint32_t>(&delay)->default_value(0),"Which Delay to apply between clearing and readback")
    ("global",po::value<bool>(&Global)->default_value(false),"Read back the global configuration")
    ("shiftregleft",po::value<int>(&ShiftRegLeft)->default_value(0),"Leave the shift register with particular value inside after the readback")
    
    
    ;

  if (type == "setNetwork") {
  desc->add_options()
    ("default,d", "Use default network configuration for slave as produced by the PPC logic.")
    ("slave-ip", po::value<std::string>(&sIp)->default_value("0.0.0.0"), "Slave IP" )
    ("slave-netmask", po::value<std::string>(&sNetmask)->default_value("0.0.0.0"), "Slave subnet mask" )
    ("slave-gateway", po::value<std::string>(&sGw)->default_value("0.0.0.0"), "Slave default gateway" )
    ("fitfarm-ip-1", po::value<std::string>(&fitFarmIp1)->default_value("0.0.0.0"), "FitFarm 1 IP" )
    ("fitfarm-ip-2", po::value<std::string>(&fitFarmIp2)->default_value("0.0.0.0"), "FitFarm 2 IP" )
    ("fitfarm-port-1", po::value<int>(&fitFarmPort1)->default_value(0), "FitFarm 1 port" )
    ("fitfarm-port-2", po::value<int>(&fitFarmPort2)->default_value(0), "FitFarm 2 port" )
    ;
  }


  if (type == "dummyScan") {
  desc->add_options()
    ("injtype,i", po::value<uint32_t>(&injType)->default_value(0), "Specify test data injection type (0 = memory, 1 = histogrammer, 2 = formatter, default is 0)" )
    ("multihit,m", po::value<uint32_t>(&multiHit)->default_value(1), "Specify whether to send more than one injection at a time to the slave (1 = true, 0 = false, default is 1)" )
    ("scantype,t", po::value<uint32_t>(&scanType)->default_value(0), "Specify the type of dummy scan (0 = Occupancy (Checkerboard pattern), 1 = Occupancy (Pixel Address), 2 = Threshold (S-curve fit), default is 0)" )
    ("chips,c", po::value<uint32_t>(&nChips)->default_value(8), "Specify the number of chips connected to slave (default is 8, FE-I4)" )
    ("occ,o", po::value<uint32_t>(&expOcc)->default_value(1), "Specify the expected triggers for SHORT_TOT readout mode (default is 1)" )
    ("readouttype,r", po::value<uint32_t>(&readoutType)->default_value(0), "Specify the histogrammer readout type, (0 = ONLINE_OCCUPANCY, 1 = OFFLINE_OCCUPANCY, 2 = SHORT_TOT, 3 = LONG_TOT, default is 0)" )
    ;
  }

  if (type == "testRxHisto") {
  desc->add_options()
    ("unit,u", po::value<uint32_t>(&histoUnit)->default_value(2), "Specify which histo unit (0 or 1) or both (2), default is 2" )
    ("modmask,m", po::value<std::string>(&modMasks)->default_value("0xFFFFFFFF"), "Specify the module mask, default is 0xFFFFFFFF" )
    ("ntrig", po::value<uint32_t>(&nTrig)->default_value(1), "Specify the number of triggers, default is 1" )
    ("mode", po::value<uint32_t>(&mode)->default_value(0), "Specify which dummy mode to use, (2 = bypass MCC, 1 = bypass FE, , 0 = use FE, default is 0)" )
    ("readouttype,r", po::value<uint32_t>(&readoutType)->default_value(0), "Specify the histogrammer readout type, (0 = ONLINE_OCCUPANCY, 1 = OFFLINE_OCCUPANCY, 2 = SHORT_TOT, 3 = LONG_TOT, default is 0)" )
    ("ms", po::value<uint32_t>(&nMasks)->default_value(0), "Specify the number of mask steps to inject, (default is 0 = all)" )
    ("filter,f", po::value<uint32_t>(&filter)->default_value(0), "Specify whether to turn on histogrammer filtering, (1 = true, 0 = false, default is 0)" )
    ;
  }
  
}	

void Options::init(int c, char* v[]) {
  if(c) { argc = c; }
  if(v) { argv = v; }

  try {
    po::store(po::command_line_parser( argc, argv).options( *desc ).allow_unregistered().run(), vm);
    po::notify(vm);
  } catch (...) {
    std::cout << *desc << std::endl;
    exit(1);
  }
  
  if(type == "setUart" && !vm.count("uart")){
    std::cout << "You must give the Uart option to set the Uart" << std::endl;
    usage();
    std::cout << *desc << std::endl;
    exit(0);
  }

  if (type == "setNetwork" && vm.count("default")) {
    defaultSettings = 1;
  }

  if (vm.count("help")) {
    usage();
    std::cout << *desc << std::endl;
    exit(0);
  }
}

const std::string Options::getIpFromHostName(){
return getIpFromHostName(hostName);
}

const std::string Options::getIpFromHostName(const std::string & hName){

  struct addrinfo hints, *servinfo, *p;
  struct sockaddr_in *h =NULL;
  int rv;

  memset(&hints, 0, sizeof hints);
  hints.ai_family = AF_INET; // use AF_INET to force IP4
  hints.ai_socktype = SOCK_STREAM;

    if ( (rv = getaddrinfo( hName.c_str() , NULL , &hints , &servinfo)) != 0) {
      std::cerr<<"getaddrinfo: "<< gai_strerror(rv) <<std::endl;
      return "";
    }

    // loop through all the results and connect to the first we can
    // loop through all the results and connect to the first we can
    for(p = servinfo; p != NULL; p = p->ai_next) {
      h = (struct sockaddr_in *) p->ai_addr;
    }

  freeaddrinfo(servinfo); // all done with this structure
  return inet_ntoa( h->sin_addr );

}

const std::string Options::getHostNameFromRodName(const std::string & rName){

   if(rName.empty() || rName.find("ROD_") == std::string::npos){
      std::cout<<"ROD Name is empty or doesn't contains ROD_XX_XX"<<std::endl;
      return "";
    }

  size_t pos;
  std::string crate;
  int slotNr=0;
  if((pos = rName.find_first_of("_")) !=std::string::npos)crate = rName.substr(pos+1,2);
  if((pos = rName.find_last_of("_"))  !=std::string::npos)slotNr = std::atoi(rName.substr(pos+2).c_str());
  
  if(crate=="I1")crate="IBL";//Hostname doesnt follow crate name for IBL

  std::ostringstream ss;
  ss <<"ROD-PIX-"<<crate<<"-S" << std::setw(2) << std::setfill('0') << slotNr<<"-MASTER";
  
  std::string hName = ss.str();
    std::for_each(hName.begin(), hName.end(), [](char & c){
      c = ::tolower(c);
    });

  std::cout<<rName<<" --> "<<hName<<std::endl;

  return hName;

}

const std::string Options::getIpFromRodName(){

  if(!rodName.size()) return "0.0.0.0";

  hostName = getHostNameFromRodName(rodName);

  return getIpFromHostName();

}

const std::string Options::getIpFromRodName(const std::string & rName){
  if(rName.empty()) return "0.0.0.0";

  return getIpFromHostName(getHostNameFromRodName(rName));

}

const std::string Options::getIp(){

// rodName takes precedence
  if ( !rodName.empty() ) return this->getIpFromRodName();
  else if ( !hostName.empty() ) return this->getIpFromHostName();
  else return ip;

}

void Options::setIp(std::string i){
     ip = i;
}

const uint8_t Options::getSlot() {
  return slot;
}

const int Options::getReadBackMode() {
    return readbackMode;
}

const int Options::getShiftRegLeft() {
      return ShiftRegLeft;
      }	  

const bool Options::getGlobal() {
      return Global;
      }	  


const uint32_t Options::getDelay() {
    return delay;
}

void Options::setSlot(uint8_t s) {
  slot = s;
}

const uint8_t Options::getUart()
{
  return uart;
}

const bool Options::getVerbose()
{
  return verbose;
}

const uint32_t Options::getValue()
{
  if ((value_str.substr(0,2) == "0x" && value_str.length() > 10) || (value_str.substr(0,2) != "0x" && value_str.length() > 8)) {
    cout<<"ERROR: your channel mask is too long. Must have 8 digits or fewer"<<endl;
    return 0;
  }
  std::stringstream interpreter;
  uint32_t value_int;
  interpreter << std::hex << value_str;
  interpreter >> value_int;
  return value_int;
}

const int Options::getPrmpVbp()
{
  return prmp;
}

const int Options::getPlsrDac()
{
  return plsr;
}

const int Options::getGadcInConsole()
{
  return gadcin;
}

const int Options::getIter()
{
  return iter;
}

const int Options::getUserMask()
{
  return umask;
}

const int Options::getcpMode()
{
  return cpmode;
}

const int Options::getnDC()
{
  return ndc;
}

const int Options::getTemp0()
{
  return tm0;
}

const int Options::getTemp1()
{
  return tm1;
}


const uint32_t Options::getAddress()
{
  if ((address.substr(0,2) == "0x" && address.length() > 10) || (address.substr(0,2) != "0x" && address.length() > 8)) {
    cout<<"ERROR: your channel mask is too long. Must have 8 digits or fewer"<<endl;
    return 0;
  }
  std::stringstream interpreter;
  uint32_t address_int;
  interpreter << std::hex << address;
  interpreter >> address_int;
  return address_int;
}

const int Options::getSlave()
{
  return slave;
}

const int Options::getSlink()
{
  return slink;
}

const int Options::getRx()
{
  return rx;
}

const unsigned int Options::getTime()
{
  return sleeptime;
}

const bool Options::getTestInj()
{
  return testInj;
}

const std::string Options::getFeType()
{
  return feType;
}




const uint32_t Options::getChannels() 
{
  if ((channels.substr(0,2) == "0x" && channels.length() > 10) || (channels.substr(0,2) != "0x" && channels.length() > 8)) {
    cout<<"ERROR: your channel mask is too long. Must have 8 digits or fewer"<<endl;
    return 0;
  }
  std::stringstream interpreter;
  uint32_t ch_int;
  interpreter << std::hex << channels;
  interpreter >> ch_int;
  return ch_int;
}


void Options::usage() {
  {
    cout<<endl;
    //todo make switch/case
    if (type == "writeConfig") {
      cout<<"For which channels would you like to write their config to the PPC?"<<endl;
      cout<<"To edit the config you are going to send, open tools/writeConfig.cxx and edit the config there"<<endl;
      cout<<"Don't forget to make in HostCommandPattern after editing the config"<<endl;
    }
    else if (type == "sendConfig") {
      cout<<"To which channels would you like to send their config?"<<endl;
    }
    else if (type == "readConfig") {
      cout<<"From which channels would you like to read the config?"<<endl;
    }
    else if (type == "enable") {
      cout<<"What channel mask would you like to enable?"<<endl;
    }
    else if (type == "setUart"){
      cout << "to what do you want to set the uart? " <<endl;
	cout<<"0 for master, 1 for north slave, 2 for south slave"<<endl;
	return;
    }
    else if (type == "setNetwork") {
      cout << "Choose a slave to set the network on via --slave" << endl;
      cout << "The default network settings will be applied to this slave as determined by the PPC by default." << endl;
      cout << "You can specify local slave network configuration and FitFarm targets if needed" << endl;
    }
    
    cout<<"Ex: 00010000 is Rx ch 16, ie first channel on south slave"<<endl;
    cout<<"Default channel mask is: 0x"<<std::hex<<this->getChannels()<<std::dec<<endl;
    cout<<"Default ip is: "<<this->getIp()<<endl;
  }
  cout<<endl;
}

int Options::getDefault() {return defaultSettings;}
std::string Options::getSIp() {return validateIp(sIp);} 
std::string Options::getSNetmask() {return validateIp(sNetmask);}
std::string Options::getSGw() {return validateIp(sGw);}
std::string Options::getFitFarmIp1() {return validateIp(fitFarmIp1);}
std::string Options::getFitFarmIp2() {return validateIp(fitFarmIp2);}


/* Checks if ports are in valid range. */
int Options::getFitFarmPort1() {
    if (fitFarmPort1 <= 65535 && fitFarmPort1 > 0) return fitFarmPort1;
    else return 0;
}

int Options::getFitFarmPort2() {
    if (fitFarmPort2 <= 65535 && fitFarmPort2 > 2) return fitFarmPort2;
    else return 0;
}

/* Checks if IP address is valid IPv4. */
std::string Options::validateIp(std::string ip_arg) {
    if(!ip_arg.size()) ip_arg = this->ip;
    struct sockaddr_in sa;
    if (1 == inet_pton(AF_INET, ip_arg.c_str(), &(sa.sin_addr))) return ip_arg;
    else return "0.0.0.0";
}

uint32_t Options::getInjType()
{
  return injType;
}

uint32_t Options::getMultiHit()
{
  return multiHit;
}

uint32_t Options::getScanType()
{
  return scanType;
}

uint32_t Options::getNumOfChips()
{
  return nChips;
}

uint32_t Options::getExpOcc()
{
  return expOcc;
}

uint32_t Options::getHistoUnit()
{
  return histoUnit;
}

uint32_t Options::getModMask()
{
  uint32_t modMask;
  sscanf(modMasks.c_str(),"%x",&modMask);
  return modMask;
}

uint32_t Options::getNTrig()
{
  return nTrig;
}

uint32_t Options::getMode()
{
  return mode;
}

uint32_t Options::getFiltOn()
{
  return filter;
}

uint32_t Options::getReadoutType()
{
  return readoutType;
}

uint32_t Options::getNMasks()
{
  return nMasks;
}

void Options::setRodName(std::string rName) {
  rodName = rName;
}

const std::string& Options::getRodName(){
  return rodName;
}

const std::string & Options::getHostName(){
  return hostName;
}

#endif 
