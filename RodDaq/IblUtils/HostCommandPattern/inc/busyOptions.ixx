// M Backhaus, backhaus@cern.ch
// Generic options class for command line functions that take ip and/or channels are argument
#ifndef BUSY_OPTIONS_H
#define BUSY_OPTIONS_H

#include "boost/program_options.hpp"
#include <string>
#include <iostream>
#include "boost/algorithm/string.hpp"

namespace po = boost::program_options;
using std::string;
using std::cout;
using std::endl;

class busyOptions{
public:
  busyOptions(int c, char* v[], std::string t = "");
  busyOptions();
  void init(int c=0, char* v[]=0);
  ~busyOptions() { };
  void usage();
  const char* getIp();
  const uint32_t getChannels();
  const int getUart();
  const bool getSlave();
  const int getMask();
  const int getForce();
  const unsigned int getTime();

private:
  po::options_description *desc;
  po::variables_map vm;
  
  int argc;
  char** argv;  
  std::string type;
  string ip;
  std::string channels;
  bool slave;
  int uart;
  int mask;
  int force;
  unsigned int sleeptime;
};

busyOptions::busyOptions()
{
}

busyOptions::busyOptions(int c, char* v[], std::string t)
{
  type = t;
  this->init(c, v);
}

void busyOptions::init(int c, char* v[])
{
  if(c) { argc = c; }
  if(v) { argv = v; }

  desc = new po::options_description("Options");
  desc->add_options()
    ("help,h", "Print help and exit")
    ("ch", po::value<std::string>(&channels)->default_value("0xFFFFFFFF"), "Specify the channel mask")
    ("ip", po::value<std::string>(&ip)->default_value(std::string("10.145.87.201")), "Specify the ROD's IP address, default is not valid!" )
    ("uart", po::value<int>(&uart)->default_value(0), "Specify the Uart (0 = PPC, 1 = Slave 0, 2 = Slave 1)")
    ("slave", po::value<bool>(&slave)->default_value(0), "Specify which slave (0 or 1) ")
    ("mask", po::value<int>(&mask)->default_value(0), "Specify the busyMask")
    ("force", po::value<int>(&force)->default_value(0), "Specify the busyForce")
    ("time", po::value<unsigned int>(&sleeptime)->default_value(0), "Specify the sleepin time")
    ;

  try {
    po::store(po::command_line_parser( argc, argv).options( *desc ).allow_unregistered().run(), vm);
    po::notify(vm);
  } catch (...) {
    std::cout << *desc << std::endl;
    exit(1);
  }

  if (vm.count("help")) {
    usage();
    std::cout << *desc << std::endl;
    exit(0);
  }
}

const char* busyOptions::getIp()
{
  return ip.c_str();
}

const int busyOptions::getUart()
{
  return uart;
}

const bool busyOptions::getSlave()
{
  return slave;
}

const int busyOptions::getMask() 
{
  return mask;
}

const int busyOptions::getForce() 
{
  return force;
}

const unsigned int busyOptions::getTime()
{
  return sleeptime;
}

const uint32_t busyOptions::getChannels() 
{
  if ((channels.substr(0,2) == "0x" && channels.length() > 10) || (channels.substr(0,2) != "0x" && channels.length() > 8)) {
    cout<<"ERROR: your channel mask is too long. Must have 8 digits or fewer"<<endl;
    return 0;
  }
  std::stringstream interpreter;
  uint32_t ch_int;
  interpreter << std::hex << channels;
  interpreter >> ch_int;
  return ch_int;
}

void busyOptions::usage() {
  {
    cout<<endl;
    //todo make switch/case
    if (type == "writeConfig") {
      cout<<"For which channels would you like to write their config to the PPC?"<<endl;
      cout<<"To edit the config you are going to send, open tools/writeConfig.cxx and edit the config there"<<endl;
      cout<<"Don't forget to make in HostCommandPattern after editing the config"<<endl;
    }
    else if (type == "sendConfig") {
      cout<<"To which channels would you like to send their config?"<<endl;
    }
    else if (type == "readConfig") {
      cout<<"From which channels would you like to read the config?"<<endl;
    }
    else if (type == "enable") {
      cout<<"What channel mask would you like to enable?"<<endl;
    }
    else if (type == "setUart"){
      cout << "to what do you want to set the uart? " <<endl;
	cout<<"0 for master, 1 for north slave, 2 for south slave"<<endl;
	return;
    }
    
    cout<<"Ex: 00010000 is Rx ch 16, ie first channel on south slave"<<endl;
    cout<<"Default channel mask is: 0x"<<std::hex<<this->getChannels()<<std::dec<<endl;
    cout<<"Default ip is: "<<this->getIp()<<endl;
  }
  cout<<endl;
}

#endif 
