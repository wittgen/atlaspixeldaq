#ifndef _DEFAULTCLIENT_H_
#define _DEFAULTCLIENT_H_
// Russell Smith
// May 5 2014
// Client which uses options.ixx

#include "Client.h"
#include "options.ixx"
#include "TraitsHelper.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

class DefaultClient: public Client {

public:
  DefaultClient(const char* ip): Client(ip){
  }

  DefaultClient(Options opt): Client(opt.getIp().c_str()), m_opt(opt) {//preferred way
  }

  template<typename T> bool run(T& command){
    if (!server.connectToServer(srvIpAddr(), srvIpPort()) ) {
      std::cerr << "ABORTING! Someone please fix me in every single tool!" << std::endl;
      std::cerr << "The tool should check for the return status of this function and stop processing if we couldn't talk talk to the PPC!" << std::endl;
      exit(1);
      return false;
    }

    std::cout << "setting client IP = " << srvIpAddr() << std::endl;
    std::cout << "Connected to server." << std::endl;
    std::cout<<"about to send command"<<std::endl;

    bool retVal = true;
    if( server.sendCommand(command) || has_empty_result<T>::value == true ) std::cout << "... command sent" << std::endl;
    else { std::cerr << "... couldn't send command" << std::endl; retVal = false; }
    std::cout << "Disconnecting from server...";
    server.disconnectFromServer();
    std::cout << "Done." << std::endl;
    return retVal;
  }

protected:
  Options m_opt;
};
#endif // _DEFAULTCLIENT_H_
