#ifndef __TRAITS_HELPER_H__
#define __TRAITS_HELPER_H__

#include "GeneralResults.h"
#include "CommandTraits.h"

/*
template <typename T, typename U>
struct is_same {
      static const bool value = false;
};

template <typename T>
struct is_same<T, T> {
      static const bool value = true;
};
 */

/*
 * Traits structure to check for the presence of an EmptyResult member in a class/struct
 * has_empty_result<Command>::value is either true of false
 */

template <typename T, typename = void>
struct has_empty_result {
      static const bool value = true; // Assume there's no result by default (if command has no ResultType)
};

template <typename T>
struct has_empty_result<T, typename std::enable_if<has_result_type<T>::value>::type> {
      static const bool value = is_same<typename T::ResultType,EmptyResult>::value;
};

#endif // __TRAITS_HELPER_H__
