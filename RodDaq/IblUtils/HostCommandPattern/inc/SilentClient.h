#ifndef __SILENT_CLIENT_H__
#define __SILENT_CLIENT_H__
/*
 * Author K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2015-III-8
 * Description: this client NEVER outputs anything, EXCEPT in case of errors
 */

#include "Server.h"
#include "Client.h"
#include "options.ixx"
#include "TraitsHelper.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif

class SilentClient: public Client {

public:
	SilentClient(const char* ip): Client(ip){
	}

	SilentClient(Options opt): Client(opt.getIp().c_str()), m_opt(opt) {
	}

	template<typename T> bool run(T& command){
		streambuf *old = cout.rdbuf();
		stringstream ss;
		cout.rdbuf (ss.rdbuf());

	  if (!server.connectToServer(srvIpAddr(), srvIpPort()) ) {
	    std::cerr << "ABORTING! Someone please fix me in every single tool!" << std::endl;
	    std::cerr << "The tool should check for the return status of this function and stop processing if we couldn't talk talk to the PPC!" << std::endl;
	    exit(1);
	    return false;
	  }

	  bool retVal = true;
	  if( !server.sendCommand(command) && has_empty_result<T>::value == false ) { std::cerr << "... couldn't send command" << std::endl; retVal = false; }
	  server.disconnectFromServer();

		cout.rdbuf (old);

	  return retVal;
	}

protected:
	Options m_opt;
};
#endif // __SILENT_CLIENT_H__
