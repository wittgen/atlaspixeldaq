/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: January-2018
 *
 * MCCConScann using opto-scan in the PPC
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "VerifyOptoTune.h"
#include <string>

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("speed", po::value<int>(&speed)->default_value(0), "Readout speed (0->40, 1->2x40, 2->80, 3->160)")
				("nTriggers", po::value<int>(&nTriggers)->default_value(10), "Number of triggers sent");

		}

		int getSpeed(){return speed;}
		int getnTriggers(){return nTriggers;}

	protected:

		int speed;
		int nTriggers;

	private:

};

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);
  
  std::cout<<" Speed "<<opt.getSpeed()<<"; nTriggers "<<opt.getnTriggers()<<std::endl;

  VerifyOptoTune cmd;
  
  cmd.speed =opt.getSpeed();
  cmd.nTriggers =opt.getnTriggers();
  client.run(cmd);

for (std::map<uint8_t, uint32_t >::iterator it = cmd.result.errors.begin(); it!=cmd.result.errors.end(); ++it){
std::cout<<"RxCh "<<(int)it->first<<" Errors "<<(int)it->second<<std::endl;

}

  return 0;
}
