#define BOOST_TEST_DYN_LINK        // this is optional
#define BOOST_TEST_MODULE Tools   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

//todo maybe put them all in one file together
#include "EnableChannels.h"
#include "WriteModuleConfig.h"
#include "SendModuleConfig.h"
#include "ReadModuleConfig.h"
#include "Fei4ExtCfg.h"

#include "TestingClient.h"


struct SetupToolsTests{

  SetupToolsTests(uint32_t channels = 0x00010000, bool sendCommands = true): 
    m_channels(channels) , client(IBL_ROD_IP), m_sendCommands(sendCommands) {//this is run before each test case

  }

  ~SetupToolsTests(){//this is run after each test case

  }

  TestingClient client;
  uint32_t m_channels;
  bool m_sendCommands;
};


BOOST_FIXTURE_TEST_SUITE(toolsTests, SetupToolsTests) 


  BOOST_AUTO_TEST_CASE(enableChannels)
{
  EnableChannels enableChannels;

  enableChannels.Channels = m_channels;
  if(m_sendCommands) client.run(enableChannels);

  BOOST_REQUIRE( enableChannels.result.enabledChannels== m_channels);
}

BOOST_AUTO_TEST_CASE(writeConfig)
{
  //todo test multiple FrontEnds
  WriteModuleConfig writeCfg;
  writeCfg.setCalibConfig();

  Fei4ExtCfg newConfig;
  newConfig.setEnabled(true);//todo check what this does
  newConfig.setTestingCfg();//set to the the testing config

  for (uint32_t i = 0; i<32; i++) {
    if (m_channels & (0x1<<i)) {
      newConfig.setRxCh(i);
      newConfig.setTxCh(i); // Works for Pixel Lab!! Not for SR!                                                                              
      writeCfg.moduleConfigs.push_back(newConfig);
    }
  }
  writeCfg.moduleConfigs.at(0).dump();
  if(m_sendCommands) client.run(writeCfg);

  ReadModuleConfig readCfg;
  readCfg.readCalibConfig();
  readCfg.setFeMask(m_channels);
  readCfg.setReadFrontEnd(false);
  if(m_sendCommands) client.run(readCfg);
  readCfg.result.moduleConfigsFei4.at(0).dump();//dump();

  BOOST_REQUIRE(writeCfg.moduleConfigs.at(0) == readCfg.result.moduleConfigsFei4.at(0));


}

BOOST_AUTO_TEST_CASE(sendConfig)
{
  //todo test multiple FrontEnds
  WriteModuleConfig writeCfg;
  writeCfg.setCalibConfig();

  Fei4ExtCfg newConfig;
  newConfig.setEnabled(true);//todo chCeck what this does
  newConfig.setTestingCfg();//set to the the testing config

  for (uint32_t i = 0; i<32; i++) {
    if (m_channels & (0x1<<i)) {
      newConfig.setRxCh(i);
      newConfig.setTxCh(i); // Works for Pixel Lab!! Not for SR!                                                                              
      writeCfg.moduleConfigs.push_back(newConfig);
    }
  }
  writeCfg.moduleConfigs.at(0).dump();
  if(m_sendCommands)  client.run(writeCfg);

  //todo test multiple FrontEnds
  SendModuleConfig sendCfg;
  sendCfg.setCalibConfig();
  if(m_sendCommands)  client.run(sendCfg);

  ReadModuleConfig readCfg;
  readCfg.readCalibConfig();
  readCfg.setFeMask(m_channels);
  readCfg.setReadFrontEnd(true); //main difference from writeConfig test
  if(m_sendCommands)  client.run(readCfg);
  readCfg.result.moduleConfigsFei4.at(0).dump();//dump();

  BOOST_REQUIRE(writeCfg.moduleConfigs.at(0) == readCfg.result.moduleConfigsFei4.at(0));


}


BOOST_AUTO_TEST_SUITE_END()
