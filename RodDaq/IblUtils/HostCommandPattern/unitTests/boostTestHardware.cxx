#define BOOST_TEST_DYN_LINK        // this is optional
#define BOOST_TEST_MODULE Hardware   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

#include "TestSlave.h"
#include "TestRodRegisters.h"
#include "WriteRodRegisters.h"
#include "SetUart.h"

#include "TestingClient.h"


struct SetupHardwareTests{

  SetupHardwareTests(uint32_t channels = 0x00010000, bool sendCommands = false): 
    m_channels(channels) , client(IBL_ROD_IP), m_sendCommands(sendCommands) {//this is run before each test case

  }

  ~SetupHardwareTests(){//this is run after each test case

  }

  TestingClient client;
  uint32_t m_channels;
  bool m_sendCommands;
};


BOOST_FIXTURE_TEST_SUITE(hardwareTests, SetupHardwareTests) 

  BOOST_AUTO_TEST_CASE(testSlave)
{ 
  TestSlave testSlave;
  testSlave.talkToFitFarm = false;//todo do we want one where we talk to fitfarm also?
  
  client.run(testSlave);
  
  BOOST_REQUIRE(testSlave.result.success==true);
}

  BOOST_AUTO_TEST_CASE(testUart)
{ //todo add some tests
  SetUart setUart0;  
  setUart0.uart = 0;
  client.run(setUart0);
  //  BOOST_REQUIRE(

  //SetUart setUart
  //  testSlave.talkToFitFarm = false;//todo we want one where we talk to fitfarm also?
  
}

BOOST_AUTO_TEST_CASE(testRodRegisters)
{
  WriteRodRegisters writeRodRegisters;

  writeRodRegisters.epcBaseValue        = 0x0;
  writeRodRegisters.epcLocalValue       = 0x0; 
  writeRodRegisters.epcSlvARegsValue    = 0x0;
  writeRodRegisters.epcSlvARamValue     = 0xc0ffee;
  writeRodRegisters.epcSlvBRegsValue    = 0x0;
  writeRodRegisters.epcSlvBRamValue     = 0x0;
  writeRodRegisters.epcBocValue         = 0x40;
  writeRodRegisters.epcLocalCtrlValue   = 0x80000000;
  //  writeRodRegisters.epcLocalStatValue   = 0x0;//maybe readonly?
  writeRodRegisters.epcLocalSpMaskValue = 0x5;//todo test others if they can change
  writeRodRegisters.dspRegBaseValue     = 0x0;
  writeRodRegisters.fmt0RegBaseValue    = 0x0;
  writeRodRegisters.fmt1RegBaseValue    = 0x0;

  TestRodRegisters testRodRegisters;


  client.run(writeRodRegisters);
  sleep(2);
  client.run(testRodRegisters); //todo maybe change name to read?

  BOOST_REQUIRE( testRodRegisters.result.epcBaseValue        ==  writeRodRegisters.epcBaseValue);
  BOOST_REQUIRE( testRodRegisters.result.epcLocalValue       ==  writeRodRegisters.epcLocalValue);       
  BOOST_REQUIRE( testRodRegisters.result.epcSlvARegsValue    ==  writeRodRegisters.epcSlvARegsValue);    
  BOOST_REQUIRE( testRodRegisters.result.epcSlvARamValue     ==  writeRodRegisters.epcSlvARamValue);     
  BOOST_REQUIRE( testRodRegisters.result.epcSlvBRegsValue    ==  writeRodRegisters.epcSlvBRegsValue);    
  BOOST_REQUIRE( testRodRegisters.result.epcSlvBRamValue     ==  writeRodRegisters.epcSlvBRamValue);     
  BOOST_REQUIRE( testRodRegisters.result.epcBocValue         ==  writeRodRegisters.epcBocValue);         
  BOOST_REQUIRE( testRodRegisters.result.epcLocalCtrlValue   ==  writeRodRegisters.epcLocalCtrlValue);   
  //  BOOST_REQUIRE( testRodRegisters.result.epcLocalStatValue   ==  writeRodRegisters.epcLocalStatValue);   
  BOOST_REQUIRE( testRodRegisters.result.epcLocalSpMaskValue ==  writeRodRegisters.epcLocalSpMaskValue); 
  BOOST_REQUIRE( testRodRegisters.result.dspRegBaseValue     ==  writeRodRegisters.dspRegBaseValue);     
  //BOOST_REQUIRE( testRodRegisters.designRegPPCValue   ==  designRegPPCValue);   //todo I think this is readonly
}

BOOST_AUTO_TEST_SUITE_END()
