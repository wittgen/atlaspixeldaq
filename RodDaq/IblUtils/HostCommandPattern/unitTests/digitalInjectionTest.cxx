// L Jeanty <laura.jeanty@cern.ch> 
//
// This program is for firmware tests using digital injections
//
// To use:
// * compile the RodMaster/Software/PPCpp directory 
// * compile in RodDaq/RodCrate
// * compile the IblUtils/HostCommandPattern directory
// * load the master firmare .bit file and start the ppc by using: ~/daq/scripts/iblRodCtrl start cpp 
// * ./bin/digitalInjectionTest (from HostComandPattern directory)
//
// options are:
// --ip IP (default is based on what host you compiled HostCommandPattern)
// --ch ChannelMask (in hex) if different from 0x00010000 (default)
//
// Set various parameters of the injection test using this file, and then recompile in HostCommandPattern
//
// If doBocFifoHisto = true, the scan will return a limited histogram from the boc fifo
// and save it in a local root file
//
// To edit the scan more seriously or add new parameters, edit RodMaster/Software/PPCpp/CmdFactory/UnitTests/src/DigitalInjectionTest
// and recompile in PPCpp directory after editing (and re-load ppc software with iblRodCtrl start cpp)
// and recompile in RodDaq/RodCrate (make clean) if you edit the .h file of the DigitalInjectionTest.h

#include "DigitalInjectionTest.h"
#include "DefaultClient.h"
#include "iblSlaveCmds.h"

#include <ctime>
#include <cstring>
#include <iostream>
#include "TH2.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TFile.h"

#include <cstring>
#include <iostream>

// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {
  //0)
  Options opt(argc, argv);
  std::cout<<"channels = "<<std::hex<<opt.getChannels()<<std::dec<<std::endl;

  //1)
  DigitalInjectionTest cmd;
  //2)
  cmd.Channels = opt.getChannels(); // channels set by options (command line)
  cmd.ToT_Base = 7; // Base ToT of each digital hit; used to set CalPulse
  cmd.ToT_MaskStepIncrease = 0; // ToT increase for each mask step
  cmd.ToT_NextChipIncrease = 0; // ToT increase for each additional chip
  cmd.TrigDelay = 50; // Delay between trigger and readout
  cmd.TrigCount = 8; // Number of frames to read back per trigger
  cmd.HitDiscCnfg = 1; // Hit Disc Cng
  cmd.HistogrammerMode = SHORT_TOT; // Histogrammer mode
  cmd.MaskStepsInMask = 8; // Number of mask steps in mask (options are 1, 2, 4, 8, 16, 32)
  cmd.MaskStepsToDo = 8; // Number of mask steps to execute (out of total)
  cmd.DoubleColumnMode = 0x0; // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
  cmd.NDCLoops = 40; // Number of double columns to scan over
  cmd.StartDCCol = 0; // double column to begin with
  cmd.NTriggers = 10; //Number of triggers to send
  cmd.sendToFitServer = false; // send data to Fit server?
  cmd.fitServerPort = 5901; // fit server port (make sure this is the same as the port you run the fit server with!)
  cmd.doBocFifoHisto = true; // return a histo from the boc fifo?
  cmd.bocFifoChannel = 27; // from which Rx channel to take boc fifo histo
  cmd.verbose = false; // print out data on kermit?

  //3)
  DefaultClient client(opt);
  //4)
  client.run(cmd);  
  //5)

  //6)
  std::string name = "DigitalInjectionData_Occ";
  TH2F * OccHisto = new TH2F(name.c_str(), name.c_str(), 80, 0.5, 80.5, 336, 0.5, 336.5);

  if(cmd.doBocFifoHisto) {	  
    std::cout<<" The scan may have been succesful. Some results follow: "<<std::endl;

    for (int i=0; i < 80; i++) {
      for (int j=0; j < 336; j++) {
	if (i==0) if (cmd.result.Histo[i][j]) std::cout<<"For column "<<i+1<<", and row "
					   <<j+1<<", the Occ is: "<<int(cmd.result.Histo[i][j])<<std::endl;
	OccHisto->Fill(i+1,j+1,cmd.result.Histo[i][j]);
      }
    }
       
    TFile f("digitalInjectionOcc.root","recreate");
    OccHisto->Write();
    f.Close();
  }

  std::cout << "Done." << std::endl;

  delete OccHisto;

return 0;
}
