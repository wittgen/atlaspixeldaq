/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: October-2018
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "Fei4MonLeak.h"
#include <string>

#include "TFile.h"
#include "TH2F.h"

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("nPixels", po::value<int>(&nPixels)->default_value(1), "Number of Pixels");

		}

		int getnPixels(){return nPixels;}

	protected:

		int nPixels;

	private:

};

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  std::cout<<"Starting Mon leak scan "<<std::endl;
  DefaultClient client(opt);
  
  std::time_t time_result  = std::time(nullptr);
  struct tm tt = *(std::localtime(&time_result));
  std::cout<<"Execution Time: "<<std::asctime(std::localtime(&time_result))<<std::endl;
  
  Fei4MonLeak cmd;
  cmd.m_rxMask = opt.getChannels();
  cmd.m_nPixPerFE = opt.getnPixels();

  client.run(cmd);

  std::string rodName = opt.getRodName();
  if(rodName.empty())rodName = opt.getHostName();
  if(rodName.empty())rodName = opt.getIp();

  std::cout<<"Using RODName "<<rodName<<std::endl;

    std::string formattedtime = std::to_string(tt.tm_year+1900)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_min)+"_"+ std::to_string(tt.tm_sec);
    //TFile outfile((filename+"_"+std::to_string(time_result)+".root").c_str(),"RECREATE");

    std::string filename = "MON_LEAK_"+rodName +"_"+formattedtime+".root";
    TFile outfile(filename.c_str(),"RECREATE");

  for (std::map<uint8_t,std::vector<std::vector<uint32_t> > >::iterator it = cmd.result.MON_ILEAK.begin(); it != cmd.result.MON_ILEAK.end(); ++it){
  
  
  std::cout<<"Rx "<<(int)it->first<<" NPixels "<<(int)it->second.size()<<std::endl;
  
   std::string histName = "RX_"+std::to_string(it->first);
   TH2F  leakHisto(histName.c_str(),histName.c_str(),80,-0.5,79.5,336,-0.5,335.5);

   std::cout<<"NPixels "<<(int)it->second.size()<<" nIter "<<(int )it->second[0].size()<<std::endl;
   for(uint16_t pix=0;pix<it->second.size();pix++){
     float ADC =0;
     float nIter = it->second[pix].size();

     for(uint32_t i=0;i<it->second[pix].size();i++)ADC += it->second[pix][i];
     if(nIter>0)ADC /= nIter;
     
     uint32_t col = pix/336;
     uint32_t row = pix%336;

     if(col%2 ==1)row = 335-row;

       std::cout<<" Rx "<<(int)it->first<<" index "<<(int)pix<<" col "<<(int)col<<" row "<<(int)row<<" "<<ADC<<std::endl;

     leakHisto.SetBinContent(col+1,row+1 ,ADC);
   }

  leakHisto.Write();
  
  }
  
  
  outfile.Close();
  
  return 0;
}


