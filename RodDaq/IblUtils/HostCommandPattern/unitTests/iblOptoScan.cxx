/*
 * Da Xu
 * Date: March-2019
 *
 * using opto-scan in the PPC
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "IblOptoScan.h"
#include <string>

class LocalOptions: public Options {
public:
  
  LocalOptions(int c, char* v[], std::string t = ""):
    Options(c, v, t, true) { 
    this->extend_options();
    this->init(c,v);
  }
  virtual void extend_options() {
    desc->add_options()
      ("nTriggers", po::value<int>(&nTriggers)->default_value(10), "Number of triggers sent");
  }
  int getnTriggers(){return nTriggers;}
  
protected:
  
  int nTriggers;
  
private:
  
};

int main(int argc, char** argv) {
  
  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);
  IblOptoScan cmd;
  
  cmd.nTriggers =opt.getnTriggers();
  cmd.Channels = opt.getChannels(); 
  client.run(cmd);

  // print out nice table
  std::cout << std::endl << std::endl;
  std::cout<< "IMPORTANT @ Please make sure the ROD is initialized after flash!"  << std::endl << std::endl;
  std::cout<< "Only print Rxch belong to the requested mask! "  << std::endl << std::endl;
  std::cout << "Decoding errors:" << std::endl << std::endl;
  std::cout << "          | Phase 0              | Phase 1              | Phase 2              | Phase 3" << std::endl;
  std::cout << "RX        | norm_frame[errors]   | norm_frame[errors]   | norm_frame[errors]   | norm_frame[errors]" << std::endl;
  std::cout << "----------+----------------------+----------------------+----------------------+---------------------" << std::endl;

  int HCP_rxch;
  //int HCP_phase;
  
  std::map< uint8_t, std::vector< std::vector< uint32_t > > > errmap = cmd.result.errmap;
  std::map< uint8_t, std::vector< std::vector< uint32_t > > >::iterator map_Iter; 

  if(errmap.size()){
  for ( map_Iter = errmap.begin(); map_Iter != errmap.end(); map_Iter++  ){
    HCP_rxch = map_Iter->first ;
    std::cout << "       " << std::setw(2) << HCP_rxch;

    if(map_Iter->second.size()!=4) continue;

    for(size_t i = 0; i < map_Iter->second.size(); i++){
      //HCP_phase = i;
      std::vector< uint32_t > HCP_Vphase = map_Iter->second.at(i);
      if(HCP_Vphase.size() != 2) continue;
      int HCP_nframe = HCP_Vphase.at(0)/(int)(0xFFFF);
      int HCP_nerrors = HCP_Vphase.at(1);
      std::cout << " | ";
      std::cout <<  std::setw(6) << HCP_nframe<<"   ["<< std::setw(9) <<  HCP_nerrors<<"]";
    }

    std::cout << std::endl;
  }
  std::cout << std::endl << std::endl;
  }

  return 0;
}
