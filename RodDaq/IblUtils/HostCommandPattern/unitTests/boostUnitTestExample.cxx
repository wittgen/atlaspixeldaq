#define BOOST_TEST_DYN_LINK        // this is optional
#define BOOST_TEST_MODULE MyTest   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

#include "TestingClient.h"

BOOST_AUTO_TEST_SUITE(test_suite)

  int add(int i, int j){return i+j;}

BOOST_AUTO_TEST_CASE(my_test_1)    // specify a test case
{
  /* Test something... */
  const int expected = 12;
  const int another = 12;
  const int failure = 2;

  BOOST_CHECK_EQUAL( expected, another );
  BOOST_CHECK_EQUAL( expected, failure );
 
  // seven ways to detect and report the same error:
  BOOST_CHECK( add( 2,2 ) == 4 );        // #1 continues on error

  BOOST_REQUIRE( add( 2,2 ) == 4 );      // #2 throws on error

  if( add( 2,3 ) != 4 )
    BOOST_ERROR( "Ouch..." );            // #3 continues on error

  if( add( 2,2 ) != 4 )
    BOOST_FAIL( "Ouch..." );             // #4 throws on error

  if( add( 2,2 ) != 4 ) throw "Ouch..."; // #5 throws on error

  BOOST_CHECK_MESSAGE( add( 2,2 ) == 4,  // #6 continues on error
		       "add(..) result: " << add( 2,2 ) );

  BOOST_CHECK_EQUAL( add( 2,2 ), 4 );  // #7 continues on error

}

 BOOST_AUTO_TEST_SUITE_END()
