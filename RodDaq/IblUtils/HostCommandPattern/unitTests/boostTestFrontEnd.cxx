#define BOOST_TEST_DYN_LINK        // this is optional
#define BOOST_TEST_MODULE Hardware   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

#include "TestingClient.h"

struct SetupFrontEndTests{

  SetupFrontEndTests(uint32_t channels = 0x00010000, bool sendCommands = false): 
    m_channels(channels) , m_sendCommands(sendCommands) {//this is run before each test case
  }

  ~SetupFrontEndTests(){//this is run after each test case

  }

  Fei4GlobalCfg globalCfg;
  Fei4PixelCfg pixelCfg;

  static const uint32_t wordOn = 0xFFFFFFFF;
  static const uint32_t wordOff = 0x0;

  uint32_t m_channels;
  bool m_sendCommands;

};
const uint32_t SetupFrontEndTests::wordOn;
const uint32_t SetupFrontEndTests::wordOff;

BOOST_FIXTURE_TEST_SUITE(fei4GlobalCfgTests, SetupFrontEndTests) 

  BOOST_AUTO_TEST_CASE(numRegs)
{
  BOOST_CHECK_EQUAL(Fei4GlobalCfg::numberOfRegisters, 36);

}
BOOST_AUTO_TEST_CASE(defCfg)
{
  for(unsigned i = 0; i < Fei4GlobalCfg::numberOfRegisters; ++i)
    BOOST_CHECK_EQUAL(globalCfg.rdGlobalRegister(i), Fei4GlobalCfg::defaultCfg[i]);
}

BOOST_AUTO_TEST_SUITE_END()//fei4GlobalCfgTests

  BOOST_FIXTURE_TEST_SUITE(doubleColumnBitTests, SetupFrontEndTests)

  BOOST_AUTO_TEST_CASE(dcbSet)
{
  DoubleColumnBit dcb;
  uint32_t word = 0x5234;
  dcb.set(word);

  for(unsigned i = 0; i < DoubleColumnBit::nWords; i++){
    BOOST_CHECK_EQUAL(dcb.getWord(i), word);
  }
}
  BOOST_AUTO_TEST_CASE(dcbSetAll)
{
  DoubleColumnBit dcb;
  uint32_t word_array[DoubleColumnBit::nWords] = {0};
  word_array[1] = 0x2;
  word_array[13] = 0x5;

  dcb.set(word_array);

  for(unsigned i = 0; i < DoubleColumnBit::nWords; i++){
    BOOST_CHECK_EQUAL(dcb.getWord(i), word_array[i]);
  }
}
BOOST_AUTO_TEST_CASE(dcbSetAllDcb)
{
  DoubleColumnBit dcb1, dcb2;
  
  uint32_t word= 0xa1;
  dcb1.set(word);
  
  dcb2.set(dcb1);
  
  for(unsigned i = 0; i < DoubleColumnBit::nWords; i++){
    BOOST_CHECK_EQUAL(dcb2.getWord(i), word);
  }
}
BOOST_AUTO_TEST_CASE(rowColumnToBitArray)
{//todo probably add some more to this
   BOOST_CHECK( (DoubleColumnBit::rowColumnToArrayBit(0,0) < 672) & (DoubleColumnBit::rowColumnToArrayBit(0,0) >= 0) );
   BOOST_CHECK( (DoubleColumnBit::rowColumnToArrayBit(0,80) < 672) & (DoubleColumnBit::rowColumnToArrayBit(0,80) >= 0) );
   BOOST_CHECK( (DoubleColumnBit::rowColumnToArrayBit(335,0) < 672) & (DoubleColumnBit::rowColumnToArrayBit(335,0) >= 0) );
   BOOST_CHECK( (DoubleColumnBit::rowColumnToArrayBit(335,80) < 672) & (DoubleColumnBit::rowColumnToArrayBit(335,80) >= 0) );
}

BOOST_AUTO_TEST_CASE(getSetPixel)
{//todo add some boundary cases to make sure dc <--> col conversions are obvious
  DoubleColumnBit dcb;
  unsigned pixVal = 1;//todo this maybe should be bool
  unsigned n = 45;
  unsigned row = 5;  unsigned col = 10;

  dcb.setPixel(n, pixVal);
  BOOST_REQUIRE(dcb.getPixel(n) == pixVal);

  dcb.setPixel(row,col,pixVal);
  BOOST_REQUIRE(dcb.getPixel(row,col) == pixVal);

  pixVal = 0;
 
  dcb.setPixel(n,pixVal);
  BOOST_REQUIRE( dcb.getPixel(n) == pixVal);

  dcb.setPixel(row,col , pixVal);
  BOOST_REQUIRE(dcb.getPixel(row,col) == pixVal);

}

BOOST_AUTO_TEST_CASE(andOperator)
{//todo maybe move off/on words to setup
  DoubleColumnBit dcb1, dcb2;
  unsigned wordNum = 10;

  dcb1.set(wordOff);
  dcb2.set(wordOn);

  BOOST_REQUIRE( dcb1.getWord(wordNum) == (dcb1 & dcb2).getWord(wordNum) );

  DoubleColumnBit testDcb;
  uint32_t testWord = 0xA5;
  testDcb.set(testWord);

  BOOST_REQUIRE( dcb1.getWord(wordNum) == (dcb1 & testDcb).getWord(wordNum));//all off 
  BOOST_REQUIRE( testDcb.getWord(wordNum) == (testDcb & dcb2).getWord(wordNum));//testWord mask

}

BOOST_AUTO_TEST_CASE(xorOperator)
{
  DoubleColumnBit dcb1, dcb2;
  unsigned wordNum = 10;

  dcb1.set(wordOff);
  dcb2.set(wordOn);

  BOOST_REQUIRE( wordOn == (dcb1 ^ dcb2).getWord(wordNum) );

  DoubleColumnBit testDcb;
  uint32_t testWord = 0xA5;
  testDcb.set(testWord);

  BOOST_REQUIRE( wordOff == (testDcb ^ testDcb).getWord(wordNum));
  BOOST_REQUIRE( ~(testDcb.getWord(wordNum)) == (testDcb ^ dcb2).getWord(wordNum) );

}

BOOST_AUTO_TEST_SUITE_END()//doubleColumnBitTests

BOOST_FIXTURE_TEST_SUITE(maskTests, SetupFrontEndTests) 

BOOST_AUTO_TEST_CASE(maskInit){
  Mask mask;

  for(unsigned i = 0; i < DoubleColumnBit::nWords; i++){
    BOOST_REQUIRE(mask.getWord(i) == wordOn);
  }
}

BOOST_AUTO_TEST_CASE(maskSet){
  Mask mask;
  unsigned wordNum = 11;//todo maybe loop this
  unsigned wordNumEnd = 20;

  mask.set(MASK_HALF);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x55555555);

  mask.set(MASK_QUARTER);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x11111111);

  mask.set(MASK_EIGHTH);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101);

  mask.set(MASK_16);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x00010001);

  mask.set(MASK_32);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x00000001);

  mask.set(MASK_672);
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x00000000);
  BOOST_REQUIRE(mask.getWord(wordNumEnd) == 0x00000001);//for MASK_672, this is the only one that should be nonzero
}

BOOST_AUTO_TEST_CASE(shift)
{
  Mask mask;
  unsigned wordNum = 12;

  mask.set(MASK_EIGHTH);
  mask.shift();
  mask.shiftBack();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101 );

  mask.shift();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x02020202 );

  mask.shift();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x04040404 );

  mask.set(MASK_EIGHTH);
  for(unsigned i = 0; i < 8*sizeof(uint32_t)*DoubleColumnBit::nWords ; i++ ) mask.shift(); //672 shifts
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101 );

  mask.set(MASK_EIGHTH);
  for(unsigned i = 0; i < 8 ; i++) mask.shift(); //8 shifts
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101);
}

BOOST_AUTO_TEST_CASE(shiftBack)
{
  Mask mask;
  unsigned wordNum = 12;

  mask.set(MASK_EIGHTH);
  mask.shiftBack();
  mask.shift();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101 );

  mask.shiftBack();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x80808080 );

  mask.shiftBack();
  BOOST_REQUIRE(mask.getWord(wordNum) == 0x40404040 );

  mask.set(MASK_EIGHTH);
  for(unsigned i = 0; i < 8*sizeof(uint32_t)*DoubleColumnBit::nWords ; i++ ) mask.shiftBack(); //672 shifts

  BOOST_REQUIRE(mask.getWord(wordNum) == 0x01010101 );
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(doubleColumnBitFieldTests, SetupFrontEndTests)

BOOST_AUTO_TEST_CASE(setPointer){
  const unsigned dcbfLength = 5;
  
  DoubleColumnBit dcbf_array1 [dcbfLength];//empty
  DoubleColumnBitField<dcbfLength , true> tdac(dcbf_array1);
  uint32_t testWord = 0xABCD;
  unsigned numWord = 13;
  unsigned numBit = 1;

  BOOST_REQUIRE( 0 == tdac[numBit].getWord(numWord) );

  DoubleColumnBit dcbf_array2 [dcbfLength];
  dcbf_array2[numBit].set(testWord);
  tdac.setPointer(dcbf_array2);

  BOOST_REQUIRE( 0xABCD == tdac[numBit].getWord(numWord));
}

BOOST_AUTO_TEST_CASE(shiftLeft){
  const unsigned dcbfLength = 5;
  DoubleColumnBit dcbf_array1 [dcbfLength];//empty
  DoubleColumnBitField<dcbfLength , true> tdac(dcbf_array1);

}

BOOST_AUTO_TEST_CASE(pixelCfgTest){
  pixelCfg.TDAC(1).setPixel( 1 , 3 , 10);
  BOOST_REQUIRE(pixelCfg.TDAC(1).getPixel(1,3) == 10);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_FIXTURE_TEST_SUITE(fei4PixelCfgTests, SetupFrontEndTests) 

BOOST_AUTO_TEST_SUITE_END()//fei4PixelCfgTests
