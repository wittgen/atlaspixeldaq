#include "DumpRodRegisters.h"
#include <iostream>
#include "DefaultClient.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv){

  DumpRodRegisters cmd;
  cmd.dumpOnPPC = true;
  
  Options options(argc , argv);
  DefaultClient client(options);
  client.run(cmd);

  // print result on host
  std::cout<<" epcBaseValue = "<<HEX(cmd.result.epcBaseValue)<<std::endl;	        
  std::cout<<" epcLocalValue = "<<HEX(cmd.result.epcLocalValue)<<std::endl;	        
  std::cout<<" epcSlvARegsValue = "<<HEX(cmd.result.epcSlvARegsValue)<<std::endl;	        
  std::cout<<" epcSlvARamValue = "<<HEX(cmd.result.epcSlvARamValue)<<std::endl;	        
  std::cout<<" epcSlvBRegsValue = "<<HEX(cmd.result.epcSlvBRegsValue)<<std::endl;	        
  std::cout<<" epcSlvBRamValue = "<<HEX(cmd.result.epcSlvBRamValue)<<std::endl;	        
  std::cout<<" epcBocValue = "<<HEX(cmd.result.epcBocValue)<<std::endl;	        
  std::cout<<" epcLocalCtrlValue = "<<HEX(cmd.result.epcLocalCtrlValue)<<std::endl;	        
  std::cout<<" epcLocalStatValue = "<<HEX(cmd.result.epcLocalStatValue)<<std::endl;	        
  std::cout<<" epcLocalSpMaskValue = "<<HEX(cmd.result.epcLocalSpMaskValue)<<std::endl;  
  std::cout<<" dspRegBaseValue = "<<HEX(cmd.result.dspRegBaseValue)<<std::endl;	        
  std::cout<<" designRegPPCValue = "<<HEX(cmd.result.designRegPPCValue)<<std::endl;         

  return 0;
}
