/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 20-September-2015
 *
 * Test Opto Links
 *
 */

#include <iomanip>

#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>

#include "DefaultClient.h"
#include "DummyOptoTune.h"

//\\\\\\////////
//  Root include libraries
#include <TApplication.h>
// #include <TROOT.h>
#include <TStyle.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TMarker.h>
#include <TList.h>
#include <TPaletteAxis.h>
////////\\\\\\\\

int main(int argc, char** argv) {

  TApplication theApp("Analysis", &argc, argv); //  For run-time root graphics

  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  DummyOptoTune cmd;

  //  Default argument values
  cmd.optoEmu = 0;
  cmd.nTriggers = 11200;
  int thrMin = 0;
  int thrMax = 132;
  int thrStep = 1;

  //  Pick up any changes supplied via command line
  for(int i = 0; i < argc; i++){
    std::string str = std::string(argv[i]);
    if      (str.find("--optoEmu")      != std::string::npos) cmd.optoEmu       = 1;
    else if (str.find("--nTrig=")       != std::string::npos) cmd.nTriggers     = atoi(str.substr(str.find("--nTrig=")+8).c_str());
    else if (str.find("--thrMin=")      != std::string::npos) thrMin            = atoi(str.substr(str.find("--thrMin=")+9).c_str());
    else if (str.find("--thrMax=")      != std::string::npos) thrMax            = atoi(str.substr(str.find("--thrMax=")+9).c_str());
    else if (str.find("--thrStep=")     != std::string::npos) thrStep           = atoi(str.substr(str.find("--thrStep=")+10).c_str());
  }

  //  Generate host copies of variables to be tranferred to PPC
  for(uint32_t step = thrMin; step <= thrMax; step+=thrStep) {
    cmd.thrSteps.push_back(step);
  }

  cmd.nThr = cmd.thrSteps.size();

  //  It would be nice if we could actually generate nice distributions on the PPC, but alas, not for now
  if(cmd.optoEmu){
    DummyResultArray zeroArray;
    for(size_t i = 0; i < 2; i++) for(size_t j = 0; j < 8; j++) zeroArray.values[i][j]=0;

    /* Use a mersenne twister as the random number generator engine. Seed with random value. */
    boost::random_device rdev;
    boost::mt19937 gener(rdev());

    /* Create expected optimal Delay and Threshold using regular random numbers */
    srand (time(NULL)); //  Set seed for random number generator
    long long d=rand();
    double ed = (double)((uint32_t)(d-10*(d/10)));
    ed=7.0/10.0*ed;
    long long t=rand();
    double et = (double)((uint32_t)(t-100*(t/100)));
    et=((double)cmd.nThr-1.0)/100.0*et;

    /* Define the distributions that will later be used together with the generator */
    boost::normal_distribution<> ndDelay(ed, 2.0);
    boost::normal_distribution<> ndThreshold(et, (double)cmd.nThr/1.5);

    /* Use reference to generator, otherwise the variate would use a copy resulting in the same number sequences */
    boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > rngDelay(gener, ndDelay);
    boost::variate_generator<boost::mt19937 &, boost::normal_distribution<> > rngThreshold(gener, ndThreshold);

    for(int nt = 0; nt < cmd.nTriggers; nt++){
      int d = (int)rngDelay();
      int t = (int)rngThreshold();
      if(-1 < d && d < 8 && -1 < t && t < cmd.nThr){
        for(int i = (int)cmd.ErrorRate.size() - 1 ; i < t; i++){
          cmd.ErrorRate.push_back(zeroArray);
        }
        cmd.ErrorRate.at(t).values[d][1]++;
      }
    }
    int mv = cmd.nTriggers;
    for(size_t i = 0; i < cmd.nThr; i++){
      if(i < (int)cmd.ErrorRate.size() - 1){
        for(size_t j = 0; j < 8; j++){
          cmd.ErrorRate.at(i).values[j][0] = cmd.nTriggers;
          cmd.ErrorRate.at(i).values[j][1] = cmd.nTriggers - cmd.ErrorRate.at(i).values[j][1];
          if(mv > cmd.ErrorRate.at(i).values[j][1])mv=cmd.ErrorRate.at(i).values[j][1];
        }
      }
      else{
        cmd.ErrorRate.push_back(zeroArray);
        for(size_t j = 0; j < 8; j++){
          cmd.ErrorRate.at(i).values[j][0] = cmd.nTriggers;
          cmd.ErrorRate.at(i).values[j][1] = cmd.nTriggers;
        }
      }
    }
    for(size_t i=0; i < 8; i++) {
      for(size_t j=0; j < cmd.nThr; j++) {
        cmd.ErrorRate.at(j).values[i][1] = cmd.ErrorRate.at(j).values[i][1] - (int)(0.7*(double)cmd.nTriggers);
      }
    }
  } //  cmd.optoEmu

  //  Send cmd from host to PPC and retrieve cmd.result back from it
  client.run(cmd);

  //  Look at result output generated here and make sure ratios printed are consistent with what PPC reports
  // std::cout << " Randomly generated error rates:" << std::endl << std::endl;

  //\\\\\\////////
  //  Root graphical display set up

//   gROOT->SetStyle("Plain"); //  Why this?  Its font does not fit on canvas and, and the default seems good enought to me.
//   gStyle->SetPalette(1); //  Not bad, in case this is not the default
  gStyle->SetPalette(53); //  This is close to, but not quite like the current BOC scan colors
  gStyle->SetOptStat(0); //  Suppress statistics, which are meaningless in this situation
//   gStyle->SetOptTitle(0); //  Suppress title, but why?
  TH2D bidi_h("bidi_h",
              "Bit Error Rate; Delay; Threshold",
              8.0, 0.0, 8.0,                                              // X axis
              (double)cmd.nThr, (double)thrMin, ((double)thrMax) + .001); // Y axis
  ////////\\\\\\\\

//  Manipulate result data here.
//  E.g. compute error rate and coordinates of optimal value
  double er;
  double ov=1.0; //  Optimal value
  double cx=4.0, cy=((double)cmd.result.nThr)/2.0; //  Find actual current values for this when running a real scan.
  double ox=0.0, oy=0.0;
  for(size_t iDelay=0; iDelay < 8; iDelay++) {
    // std::cout << "Delay " << iDelay << ": ";
    for(size_t iThr=0; iThr < cmd.result.nThr; iThr++) {
      er = (double)(cmd.result.ErrorRate.at(iThr).values[iDelay][1])/(double)cmd.result.ErrorRate.at(iThr).values[iDelay][0];
      //  This complicated syntax for er would just read
      //
      //       er = cmd.result.ErrorRate[iThr][iDelay][1] / cmd.result.ErrorRate[iThr][iDelay][0]
      //
      //       if ErrorRate were not a vector of arrays wrapped in a struct (as c++ requires for a vector of arrays)
      //       and c++ converted all values in numeric expressions to double automatically.
      if(ov > er){
        ov=er;
        ox=(double)iDelay;
        oy=(double)iThr;
      }

  //\\\\\\////////
  //  Root histogram fill up
      double binW = ((double)(thrMax - thrMin))/(double)(cmd.nThr - 1);
      int thrBin = (int)((double)thrMin + ((double)iThr)*binW + binW/2.0);
      bidi_h.Fill(iDelay, thrBin, er); //  Trick Root to display an arbitrary color-coded map of a function over a 2-dimensional domain as a "histogram".
  ////////\\\\\\\\

      // std::cout << er << " ";
    }
    // std::cout << std::endl;
  }
  std::cout << "Current Delay: " << cx << " Current Threshold: " << cy << std::endl;
  std::cout << "Optimal Delay: " << ox << " Optimal Threshold: " << oy << " Optimal Rate: " << setprecision(4) << ov << std::endl;

  //\\\\\\////////
  //  Root output

  TCanvas* c = new TCanvas ("Canvas" ,"Canvas" ,800 ,800);
  bidi_h.DrawClone("Colz");
  //  Try to control number of digits in palette scale but no luck so far.
  TPaletteAxis *pal = (TPaletteAxis*) (bidi_h.GetListOfFunctions()->FindObject("palette")); 
  pal->GetAxis()->SetMaxDigits(3); //  This seems to do nothing, and the next two cause seg faults!
  // pal->GetAxis()->SetLabelSize(.3);
  // pal->GetAxis()->SetDecimals();

  TMarker *cm = new TMarker(cx+.5, cy+.5, 20);
  cm->SetMarkerColor(kBlue);
  cm->Draw();

  TMarker *om = new TMarker(ox+.5, oy+.5, 25);
  om->SetMarkerColor(kRed);
  om->SetMarkerSize(2);

  om->Draw();

  theApp.Run(kTRUE);
  ////////\\\\\\\\

  return 0;
}
