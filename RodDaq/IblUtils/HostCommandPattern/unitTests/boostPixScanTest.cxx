// Laura (laura.jeanty@cern.ch)
// 11 March 2014
// 
// Russell Smith
// 24 April 2014
// Update: moved to use boost unit testing 
// todo update below with relevant changes
//
// Sends a pixscanbase to the ppc and
// starts a scan based on that pixscanbase
// As of 14 March 2014, supports digital, analog and threshold scans
// The parameters you may want to change are highlighted with ****** user may edit ******
// 
// Note: for the moment, this code will seg fault on exit
// because Fei4 is not properly destructed when on the host (fine for the ppc)
// This will be fixed in release branch 2.0.0 
// and for the moment does not affect operation
//
// Because this is designed to imitate scans sent from the calibration console,
// parameters other than those in pixscan are not sent
//
// For example, if you want to send your data to the fit farm, you have to open up
// RodMaster/Software/PPCpp/Scan/IblScan.cxx and change the bool talkToFitFarm to true
// in the Scan constructor, and then recompile there
//
// To run from IblUtils/HostCommandPattern, do:
// make (after changing parameters here)
// iblRodCtrl start cpp
// iblRodCtrl start s1 or s0
// ./bin/pixScanTest
//
// This program doesn't return any data onto the host -- you can choose a few different types
// of kermit debug print-outs by editing the contstructor bools in RodMaster/Software/PPCpp/Scan/IblScan.cxx
// However, it is designed to send data to the fit farm as the main way of seeing scan output
//
// The scan code is still under heavy development and testing, so there may still be bugs!
// For example, at the moment the last mask step (8 of 8 or 4 or 4) doesn't give you all hits...
// Under investigation...

#include "Server.h"
#include "RodCommand.h"
#include "PixScanBaseToRod.h"
#include "PixScanBase.h"
#include "StartScan.h"
#include "WriteModuleConfig.h"
#include "Fei4.h"

#include <iostream>
#include <cstring>
#include <unistd.h>

#ifndef IBL_ROD_IP
#define IBL_ROD_IP "192.168.2.80"
#endif
  
#define BOOST_TEST_DYN_LINK        // this is optional                                                                                                                  
#define BOOST_TEST_MODULE scanTests   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

#include "TestingClient.h"

struct SetupScanTests{

  SetupScanTests(uint32_t channels = 0x00010000): m_channels(channels) {
    // ***** user may edit *****
    // Set which front end channels you would like to run over!!!
    // Until merged with release 2.0.0, will not necessarily work for multiple channels
    // For now, just chooose 1 channel
    // 0xFFFF0000 are slave 1 (south)
    // 0x0000FFFF are slave 0 (north)
    // 0x000F0000 is Rx channels 16-19
    // PixelLab: 0x00010000 works for channel 0

  
    myCfg = m_fei4; // Type conversion
    myCfg.setRxCh(16);
    myCfg.setTxCh(16);
    myCfg.setChipID(8);
    myCfg.setEnabled(true);
    writeCfg.setCalibConfig();
    
    sendStart.channels = channels;
    sendStart.scanId = 4321;
    for (int i = 0; i<4;i++) {
      sendStart.fitPorts[i] = 5101;
      sendStart.fitIps[i][0] = 192; 
      sendStart.fitIps[i][1] = 168;
      sendStart.fitIps[i][2] = 0; 
      sendStart.fitIps[i][3] = 100;  
    }  

    sb.setLoopActive(0,0);

    // ***** user may edit *****
    // choose how many mask stages you would like to complete out of the total 
    sb.setMaskStageSteps(1);
    // how many triggers per iteration? don't be greedy
    
    sb.setRepetitions(1);
    // *************************              
    // use an fei4 to get the default cfg set there
    // keep default for digital scan            
    // will modify below for analog or threshold scan                                      

  }

  void sendCommands();
  void initializeAnalog();
  
  ~SetupScanTests(){//this is run after each test case
    sendCommands();
  }


  uint32_t m_channels;

  PixScanBaseToRod sendScanCfg;
  SerializablePixScanBase sb;
  StartScan sendStart;
  WriteModuleConfig writeCfg;

  Fei4ExtCfg myCfg;
  Fei4 m_fei4;



};

void SetupScanTests::sendCommands(){
  sendScanCfg.setPixScanBase(sb);
  TestingClient client(IBL_ROD_IP);
  
  std::cout<<"my config is: "<<std::endl;
  myCfg.dump();
  writeCfg.moduleConfigs.push_back(myCfg);
  BOOST_REQUIRE( writeCfg.moduleConfigs[0].PrmpVbpf.value() == myCfg.PrmpVbpf.value() ); 
  //  client.run(writeCfg);
  sleep(1);
  std::cout <<"sent analog config" << std::endl;
  //client.run(sendScanCfg);
  std::cout << "sent pix scan base?" << std::endl;
  sleep(1);
  //  client.run(sendStart);
 
}

void SetupScanTests::initializeAnalog(){
  myCfg.PrmpVbpf.write(100);
  myCfg.Amp2Vbpff.write(50);
  myCfg.DisVbn.write(26);
  myCfg.GADCCompBias.write(100);
  myCfg.PlsrIDACRamp.write(180);
  myCfg.Vthin_Coarse.write(1);
  myCfg.Vthin_Fine.write(46);
}

BOOST_FIXTURE_TEST_SUITE(pixScanTests, SetupScanTests) 
    
BOOST_AUTO_TEST_CASE(digitalScan)
{
  // ***** user may edit *****
  // supported mask stage steps are STEPS_8, STEPS_4, STEPS_2, and STEPS_1
  // supported mask stage steps are also STEPS_8_DC, STEPS_4_DC, STEPS_2_DC, and STEPS_1_DC
  // DC includes a loop over double columns too (one DC at a time)
  sb.setMaskStageTotalSteps(PixLib::EnumMaskSteps::STEPS_8); // 8 mask step units (1 out of every 8)
  // ************************** 
  sb.setMaskStageMode(PixLib::EnumMaskStageMode::FEI4_ENA_NOCAP); 
  sb.setDigitalInjection(true);


}
BOOST_AUTO_TEST_CASE(analogScan)
{
  /* Test something... */

  // ***** user may edit *****
  // supported mask stage steps are STEPS_8_DC, STEPS_4_DC, STEPS_2_DC, and STEPS_1_DC
  // don't analog inject too many pixels at a time! for safety, keep the DC loop
  sb.setMaskStageTotalSteps(PixLib::EnumMaskSteps::STEPS_8_DC); // 8 mask steps + do a loop over DC too
  // ************************** 
  sb.setMaskStageMode(PixLib::EnumMaskStageMode::FEI4_ENA_BCAP); 
  sb.setDigitalInjection(false);
  sb.setFeVCal(500);

  initializeAnalog();

}
BOOST_AUTO_TEST_CASE(thresholdScan)
{
  /* Test something... */
  // ***** user may edit *****
  // supported mask stage steps are STEPS_8_DC, STEPS_4_DC, STEPS_2_DC, and STEPS_1_DC
  // don't analog inject too many pixels at a time! for safety, keep the DC loop
  sb.setMaskStageTotalSteps(PixLib::EnumMaskSteps::STEPS_8_DC); // 8 mask steps + do a loop over DC too
  // setLoopVarValues(first, second, third, fourth)
  // don't change the first 0
  // the second value is the parameter min
  // the third value is the parameter max
  // the fourth value is the nsteps for the parameter scan
  sb.setLoopVarValues(0,0,500,20); 
  // ************************** 
  sb.setMaskStageMode(PixLib::EnumMaskStageMode::FEI4_ENA_BCAP); 
  sb.setDigitalInjection(false);
  sb.setFeVCal(400);
  sb.setLoopActive(0,1);
  sb.setLoopParam(0,PixLib::EnumScanParam::VCAL);
  initializeAnalog(); 
}

BOOST_AUTO_TEST_SUITE_END()//pixScanTests
