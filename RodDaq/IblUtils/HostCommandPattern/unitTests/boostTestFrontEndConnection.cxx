#define BOOST_TEST_DYN_LINK        // this is optional
#define BOOST_TEST_MODULE Hardware   // specify the name of your test module
#include "boost/test/included/unit_test.hpp"

#include "TestingClient.h"
#include "ReadConfig.h"
#include "WriteConfig.h"
#include "SendConfig.h"

struct SetupFrontEndConnectionTests{

  SetupFrontEndConnectionTests(uint32_t channels = 0x00010000, bool sendCommands = false): 
    m_channels(channels) , client(IBL_ROD_IP), m_sendCommands(sendCommands) {//this is run before each test case

  }

  ~SetupFrontEndConnectionTests(){//this is run after each test case

  }

  static const uint32_t wordOn = 0xFFFFFFFF;
  static const uint32_t wordOff = 0x0;
  TestingClient client;

  uint32_t m_channels;
  bool m_sendCommands;

};
const uint32_t SetupFrontEndConnectionTests::wordOn;
const uint32_t SetupFrontEndConnectionTests::wordOff;


BOOST_FIXTURE_TEST_SUITE(feConnection, SetupFrontEndConnectionTests) 

BOOST_AUTO_TEST_CASE(writeExtConfig){
  //todo try in sr1 with all fes

  std::vector<WriteConfig<Fei4ExtCfg> > writeCfgVec;
  std::vector<ReadConfig> readCfgVec;

  for(uint32_t i = 0 ; i < 32; ++i ){
    WriteConfig<Fei4ExtCfg> writeCfg;

    writeCfg.setCalibConfig();
    writeCfg.setFeToWrite(i);

    Fei4ExtCfg cfg;
    cfg.setRxCh(i); cfg.setTxCh(i);

    //todo decide if we need channels here
    //    if(m_channels & (0x1<<i)){


    cfg.ErrorMask_0.write( i);//for example
  

    writeCfg.moduleConfig = cfg;
    client.run(writeCfg);
    writeCfgVec.push_back(writeCfg);
  }

  for (int i = 0; i < 32; ++i) {
    std::cout<< i  << "*************************"<<std::endl;
    ReadConfig readCfg;
    readCfg.readCalibConfig();
    readCfg.setReadFrontEnd(false);
    readCfg.setFeToRead(i);
    
    client.run(readCfg);
    readCfgVec.push_back( readCfg);

    BOOST_CHECK( writeCfgVec.at(i).moduleConfig == readCfgVec.at(i).result.moduleConfig );
 }

}

BOOST_AUTO_TEST_CASE(writeConfig)
{
  //todo try in sr1 with all fes

  std::vector<WriteConfig<Fei4Cfg> > writeCfgVec;
  std::vector<ReadConfig> readCfgVec;

  for(uint32_t i = 0 ; i < 32; ++i ){
    WriteConfig<Fei4Cfg> writeCfg;

    writeCfg.setCalibConfig();
    writeCfg.setFeToWrite(i);

    Fei4Cfg cfg;
    cfg.setRxCh(i); cfg.setTxCh(i);

    //todo decide if we need channels here
    //    if(m_channels & (0x1<<i)){


    cfg.ErrorMask_0.write( i);//for example
    //todo loop to check every pixel
    cfg.TDAC(0).setPixel( 0 , 1, i   ); //max value of TDAC is 2^5-1, i.e 31!
    cfg.FDAC(0).setPixel( 0 , 1, i   ); //max value of FDAC is 2^4-1, i.e 15!
    //    cfg.outputEnable(0).setPixel(0 , 1 , 1 );
    // cfg.largeCapacitor(0).setPixel(0, 1, 1);
    //cfg.smallCapacitor(0).setPixel(0, 1, 1);
    //cfg.hitBusOut(0).setPixel(0, 1, 1);
    //    if(i == 16){

    std::cout <<  std::hex << "TDAC changed: 0x" << cfg.TDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "FDAC changed: 0x" << cfg.FDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "all  changed: 0x" << cfg.all(0).getPixel(0,1) << std::endl; 

      //    }
    //0 and 1 are dc we are writing to

    writeCfg.moduleConfig = cfg;
    client.run(writeCfg);
    writeCfgVec.push_back(writeCfg);
  }

  for (int i = 0; i < 32; ++i) {
    std::cout<< i  << "*************************"<<std::endl;
    ReadConfig readCfg;
    readCfg.readCalibConfig();
    readCfg.setReadFrontEnd(false);
    readCfg.setFeToRead(i);
    
    client.run(readCfg);
    readCfgVec.push_back( readCfg);
    //    readCfg.result.moduleConfig.dump();
    //    if(i == 16){
      std::cout << "TDAC changed: 0x" << readCfg.result.moduleConfig.TDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "FDAC changed: 0x" << readCfg.result.moduleConfig.FDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "all  changed: 0x" << readCfg.result.moduleConfig.all(0).getPixel(0,1) << std::endl; 

      //    }    //    readCfg.result.moduleConfig.TDAC(0).getPixel(0, 1 , i);

    BOOST_CHECK( writeCfgVec.at(i).moduleConfig == readCfgVec.at(i).result.moduleConfig );
 }
}

BOOST_AUTO_TEST_CASE(sendConfig)
{
  //todo try in sr1 with all fes
  //todo full chain

  std::vector<SendConfig> sendCfgVec;
  std::vector<ReadConfig> readCfgVec;

  for(uint32_t i = 0 ; i < 32; ++i ){
    SendConfig sendCfg;

    sendCfg.setCalibConfig();
    sendCfg.setFeToSend(i);

    Fei4Cfg cfg;
    cfg.setRxCh(i); cfg.setTxCh(i);

    //todo decide if we need channels here
    //    if(m_channels & (0x1<<i)){


    cfg.ErrorMask_0.write( i);//for example
    //todo loop to check every pixel
    cfg.TDAC(0).setPixel( 0 , 1, i   ); //max value of TDAC is 2^5-1, i.e 31!
    cfg.FDAC(0).setPixel( 0 , 1, i   ); //max value of FDAC is 2^4-1, i.e 15!
    //    cfg.outputEnable(0).setPixel(0 , 1 , 1 );
    // cfg.largeCapacitor(0).setPixel(0, 1, 1);
    //cfg.smallCapacitor(0).setPixel(0, 1, 1);
    //cfg.hitBusOut(0).setPixel(0, 1, 1);
    //    if(i == 16){

    std::cout <<  std::hex << "TDAC changed: 0x" << cfg.TDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "FDAC changed: 0x" << cfg.FDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "all  changed: 0x" << cfg.all(0).getPixel(0,1) << std::endl; 
      
      //    }
    //0 and 1 are dc we are writing to

      //    sendCfg.moduleConfig = cfg;
    client.run(sendCfg);
    //    sendCfgVec.push_back(sendCfg);
  }

  for (int i = 0; i < 32; ++i) {
    std::cout<< i  << "*************************"<<std::endl;
    ReadConfig readCfg;
    readCfg.readCalibConfig();
    readCfg.setReadFrontEnd(true);
    readCfg.setFeToRead(i);
    
    client.run(readCfg);
    readCfgVec.push_back( readCfg);
    //    readCfg.result.moduleConfig.dump();
    //    if(i == 16){
      std::cout << "TDAC changed: 0x" << readCfg.result.moduleConfig.TDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "FDAC changed: 0x" << readCfg.result.moduleConfig.FDAC(0).getPixel(0,1) << std::endl; 
      std::cout << "all  changed: 0x" << readCfg.result.moduleConfig.all(0).getPixel(0,1) << std::endl; 

      //    }    //    readCfg.result.moduleConfig.TDAC(0).getPixel(0, 1 , i);

      //    BOOST_CHECK( sendCfgVec.at(i).moduleConfig == readCfgVec.at(i).result.moduleConfig );
 }
}

BOOST_AUTO_TEST_SUITE_END()//feConnection

