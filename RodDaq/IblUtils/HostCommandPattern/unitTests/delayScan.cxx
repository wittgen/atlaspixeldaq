/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: January-2018
 *
 * MCCConScann using opto-scan in the PPC
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "DelayScan.h"
#include <string>

//  Root include libraries
#include <TStyle.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TMarker.h>
#include <TList.h>
#include <TPaletteAxis.h>
#include <TFile.h>
#include <TColor.h>
#include <TROOT.h>

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("delStart", po::value<float>(&delStart)->default_value(0), "Delay start (ns)")
				("delStop", po::value<float>(&delStop)->default_value(25), "Delay stop (ns)")
				("nDelays", po::value<uint32_t>(&nDelays)->default_value(25), "Number of delays");

		}

		float getDelStart(){return delStart;}
		float getDelStop(){return delStop;}
		uint32_t getnDelays(){return nDelays;}

	protected:

		float delStart;
		float delStop;
		uint32_t nDelays;

	private:

};

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);
  
  std::cout<<" Delay Start "<<opt.getDelStart()<<" ns; Delay Stop "<<opt.getDelStop()<<"(ns), Number of delays "<<opt.getnDelays()<<" Number of scanned points "<<opt.getnDelays()*opt.getnDelays()<<std::endl;

  DelayScan cmd;
  
  cmd.delayStart =opt.getDelStart();
  cmd.delayStop =opt.getDelStop();//Coarse delay*100 + Fine delay
  cmd.nDelays =opt.getnDelays();

  client.run(cmd);

//  Root graphical display set up
//    Int_t i;
    Float_t colfrac;
    TColor *color;
    int    palette[100];// 2D-map colour palette black-red-yellow
      // black-red-yellow
      // start with black-to-red
      for(int i=0;i<60;i++){
	colfrac = (Float_t)i/60;
	if(! gROOT->GetColor(201+i)){
	  color = new TColor (201+i,colfrac,0,0,"");
	}else{
	  color = gROOT->GetColor(201+i);
	  color->SetRGB(colfrac,0,0);
	}
	palette[i]=201+i;
      }
      // red-to-yellow now
      for(int i=0;i<40;i++){
	colfrac = (Float_t)i/40;
	if(! gROOT->GetColor(261+i)){
	  color = new TColor (261+i,1,colfrac,0,"");
	}else{
	  color = gROOT->GetColor(261+i);
	  color->SetRGB(1,colfrac,0);
	}
	palette[i+60]=261+i;
      }
  gStyle->SetPalette(100,palette);
  gStyle->SetOptStat(0); //  Suppress statistics, which are meaningless in this situation

  string filename = "DelayScan_";
     if( opt.getRodName() != "" ) filename += opt.getRodName();
      else filename += opt.getSIp();
  filename += ".root" ;
  
  TFile delayResults(filename.c_str(),"RECREATE");


  for(std::map <uint8_t, std::vector< std::vector<uint32_t> > >::iterator it = cmd.result.DelayMap.begin(); it!=cmd.result.DelayMap.end(); ++it){
    string histName = "Delay_Rx" + std::to_string(it->first);

    std::cout<<"Rx "<<(int)it->first<<std::endl;
    string canvas_name = "DelayScan_";
     if( opt.getRodName() != "" ) canvas_name += opt.getRodName();
      else canvas_name += opt.getSIp();
      canvas_name += "Rx"+std::to_string(it->first) ;

    uint32_t nDelays = it->second.size();

    TCanvas c (canvas_name.c_str(), canvas_name.c_str() ,800 ,800);
    TH2D bidi_h(histName.c_str(),
              histName.c_str(),
              nDelays, cmd.delayStart, cmd.delayStop,   // X axis
              nDelays, cmd.delayStart, cmd.delayStop); // Y axis

    for(size_t i=0;i<it->second.size();i++) {
    	for(size_t j=0;j<it->second[i].size();j++) {
    		bidi_h.SetBinContent(i+1, j+1, it->second[i][j]); 
        }
    }
  //  Trick Root to display an arbitrary color-coded map of a function over a 2-dimensional domain as a "histogram".
  bidi_h.GetXaxis()->SetTitle("DTO1 Rx delay (ns)");
  bidi_h.GetYaxis()->SetTitle("DTO2 Rx delay (ns)");

  c.cd();
  bidi_h.DrawClone("Colz");
  delayResults.cd();
  c.Write();
  //c.Print(".png");
  }

 delayResults.Write();

  return 0;
}

