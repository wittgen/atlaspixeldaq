/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: January-2018
 *
 * MCCConScann using opto-scan in the PPC
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "MCCConnScan.h"
#include <string>



class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("txMask", po::value<std::string>(&txMask)->default_value("0xFFFFFFFF"), "Specify Tx mask")
				("thrStart", po::value<uint32_t>(&thrStart)->default_value(1000), "Threshold start")
				("thrStop", po::value<uint32_t>(&thrStop)->default_value(41000), "Threshold stop")
				("thrStep", po::value<uint32_t>(&thrStep)->default_value(5000), "Threshold step")
				("delStart", po::value<uint32_t>(&delStart)->default_value(0), "Delay start")
				("delStop", po::value<uint32_t>(&delStop)->default_value(900), "Delay stop")
				("delStep", po::value<uint32_t>(&delStep)->default_value(100), "Delay step")
				;

		}

		uint32_t getThrStart(){return thrStart;}
		uint32_t getThrStop(){return thrStop;}
		uint32_t getThrStep(){return thrStep;}
		uint32_t getDelStart(){return delStart;}
		uint32_t getDelStop(){return delStop;}
		uint32_t getDelStep(){return delStep;}

		uint32_t getTxMask(){
		   if ((txMask.substr(0,2) == "0x" && txMask.length() > 10) || (txMask.substr(0,2) != "0x" && txMask.length() > 8)){
			cout<<"ERROR: your Tx mask is too long. Must have 8 digits or fewer"<<endl;
			return 0;
		   }

		  std::stringstream interpreter;
		  uint32_t ch_int;
		  interpreter << std::hex << txMask;
		  interpreter >> ch_int;
		  return ch_int;
		}

	protected:

		std::string txMask;
		uint32_t thrStart;
		uint32_t thrStop;
		uint32_t thrStep;
		uint32_t delStart;
		uint32_t delStop;
		uint32_t delStep;

	private:

};

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);
  
  std::cout<<"RxMask 0x"<<std::hex<<opt.getChannels()<<" TxMask 0x"<<opt.getTxMask()<<std::dec<<" ThrStart "<<opt.getThrStart()<<" ThrStop "<<opt.getThrStop()<<" ThrStep "<<opt.getThrStep()<<" DelStart "<<opt.getDelStart()<<" DelStop "<<opt.getDelStop()<<" DelStep "<<opt.getDelStep()<<std::endl;

  MCCConnScan cmd;
  
  cmd.txMask = opt.getTxMask();
  cmd.rxMask = opt.getChannels();
  cmd.thrStart =opt.getThrStart();
  cmd.thrStop =opt.getThrStop();
  cmd.thrStep =opt.getThrStep();
  cmd.delayStart =opt.getDelStart();
  cmd.delayStop =opt.getDelStop();//Coarse delay*100 + Fine delay
  cmd.delayStep =opt.getDelStep();

  client.run(cmd);

int nThrSteps = (cmd.thrStop - cmd.thrStart) / cmd.thrStep;
int nDelSteps = (cmd.delayStop - cmd.delayStart) / cmd.delayStep;

std::cout<<"Thr steps "<<nThrSteps<<" Del steps "<<nDelSteps<<" Number of scanned points: "<<nThrSteps*nDelSteps<<std::endl;

std::cout << "Activity Map:" << std::endl;
    std::cout << "TX/RX";
    for(int rxch =0; rxch < 32; rxch++){if((cmd.rxMask & (1<<rxch))) std::cout << " |" << std::setw(3) << rxch;}
    std::cout<<" |"<< std::endl;
    std::cout << "=====";
    for(int rxch = 0; rxch < 32; rxch++){if((cmd.rxMask & (1<<rxch))) std::cout << "=+===";} std::cout<<"=+";
    std::cout << std::endl;
   for(int txch = 0; txch < 32; txch++)
    {   if(!(cmd.txMask & (1<<txch)))continue;
        std::cout << "   " << std::setw(2) << txch << " ";
        for(int rxch = 0; rxch < 32; rxch++){
        	if(!(cmd.rxMask & (1<<rxch)))continue;
		if(cmd.result.MapActivity[txch][rxch] >0)std::cout << "| "<<cmd.result.MapActivity[txch][rxch]<<" ";
		else std::cout << "|    ";
	}
     std::cout<<"|" << std::endl;
    }
  std::cout << std::endl;

  return 0;
}
