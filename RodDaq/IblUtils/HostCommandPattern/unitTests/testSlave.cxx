#include "DefaultClient.h"
#include "TestSlave.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv) {
  Options opt(argc, argv);

  TestSlave cmd;
  cmd.talkToFitFarm = true;

  DefaultClient client(opt);
  cmd.whichSlave = opt.getSlave();

  // fit server ips
  // Only works in PIT for the moment!
  for (int i = 0; i<4;i++) {
  cmd.fitIps[i][0] = 10;
  cmd.fitIps[i][1] = 145;
  cmd.fitIps[i][2] = 89;
  cmd.fitIps[i][3] = 16;
  }

  // fit server ports
  const uint8_t slot = opt.getSlot();
  // Only works in PIT for the moment!
  if( slot < 5 || slot == 13 || slot > 21 ) { 
	  std::cerr << "ERROR: Invalid slot number!" << std::endl;
	  return 1;
  }
  uint32_t pitStartBase = 6008; // There are two instances reserverd for SR1 // todo: fix in PixFitInstanceConfig
  uint32_t portBase = (slot >= 10) ? pitStartBase + 4 * (slot-10) : pitStartBase + 4 * (slot+5) ;
  if(slot > 13) portBase -= 4;
  cmd.fitPorts[0] = portBase; //  slave 0 histo 0
  cmd.fitPorts[1] = portBase+1; //  slave 0 histo 1
  cmd.fitPorts[2] = portBase+2; //  slave 1 histo 0
  cmd.fitPorts[3] = portBase+3; //  slave 1 histo 1
  std::cout << "slot=" << (int)slot << ", portBase=" << portBase << std::endl;

  client.run(cmd);

  cmd.result.success ? std::cout << "TestSlave command successful" << std::endl
                     : std::cout << "TestSlave command failed"     << std::endl;


  return 0;
}
