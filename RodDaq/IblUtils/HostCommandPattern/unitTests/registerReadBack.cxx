// L Jeanty <laura.jeanty@cern.ch> 
// Writes a value to the global FE register errorMask0
// Reads back the value
// command line usage: ./bin/registerReadBack --ip 192.168.2.90
//
// This command enables the Rx channels
// but does not configure the modules (you have to configure before-hand)

#include "DefaultClient.h"
#include "RegisterReadBack.h"

#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "testRegister");

  RegisterReadBack cmd;

  cmd.valueToWrite = 0x1555;
  
  DefaultClient client(opt);
  client.run(cmd);

  std::cout<<"I wrote value: 0x"<<std::hex<<cmd.valueToWrite<<std::dec<<std::endl;

  for (int i = 0; i < 32; i++) {

    if (cmd.result.regValues[i] == 0xdead) std::cout<<"Rx: "<<i<<" does not have a good status "<<std::endl;
    else std::cout<<"For rx: "<<i<<", read value: 0x"<<std::hex<<cmd.result.regValues[i]<<std::dec<<std::endl;
  }
  return 0;
}
