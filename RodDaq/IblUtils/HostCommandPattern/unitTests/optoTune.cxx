/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * * F. Meloni <federico.meloni@cern.ch>
 * * K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 1-December-2015
 *
 * Opto Tune
 *
 */

#include <iomanip>
#include <fstream>

#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>

#include "DefaultClient.h"
#include "OptoScan.h"
#include "SetRxDelayThreshold.h"
#include <string>
#include "GeneralResults.h"

//  Root include libraries
#include <TStyle.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TMarker.h>
#include <TList.h>
#include <TPaletteAxis.h>
#include <TFile.h>

int main(int argc, char** argv) {

  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  OptoScan cmd;

  //  Default argument values
  cmd.m_gain = 0;
  cmd.m_rxMask = 0xFFFFFFFF;
  cmd.m_nTriggers = 10;
  cmd.m_speed = 0;
  cmd.m_bitPattern = 0xBFE;  


  std::string cfgValOutFile = "OptoScan_ConfigValues.txt";
  
  //  Pick up any changes supplied via command line
  for(int i = 0; i < argc - 1; i++){
    std::string argId = std::string(argv[i]);
    
    int argValue = atoi(argv[i+1]);
    if      (argId.find("--gain")        != std::string::npos) cmd.m_gain        = argValue;
    else if (argId.find("--speed")   	 != std::string::npos) cmd.m_speed     	 = argValue;
    else if (argId.find("--nTrig")       != std::string::npos) cmd.m_nTriggers   = argValue;
    else if (argId.find("--thrMax")      != std::string::npos) cmd.m_maxThr      = argValue;
    else if (argId.find("--nThr")     != std::string::npos) cmd.m_nThr        = argValue;
    else if (argId.find("--delMax")      != std::string::npos) cmd.m_maxDelay    = argValue;
    else if (argId.find("--nDel")     != std::string::npos) cmd.m_nDelay      = argValue;
    else if (argId.find("--nBitPattern") != std::string::npos) cmd.m_nBitPattern = argValue;
    else if (argId.find("--outFile") 	 != std::string::npos) cfgValOutFile     = argValue;
    else if (argId.find("--noTune") 	 != std::string::npos) {
    }
  }
 
  //  Send cmd from host to PPC and retrieve cmd.result back from it
  std::cout << "Sending command..." << std::endl;  
  client.run(cmd);
  
  std::cout << "Command executed" << std::endl;
  
  //  Look at result output generated here and make sure ratios printed are consistent with what PPC reports

  //  Root graphical display set up
  gStyle->SetPalette(56);
  gStyle->SetOptStat(0); //  Suppress statistics, which are meaningless in this situation
  
  string canvas_name = "OptoResults_";
  if( opt.getRodName() != "" ) canvas_name += opt.getRodName();
  else canvas_name +=  opt.getSIp();
  string filename = canvas_name + ".root";
  TFile opto_results(filename.c_str(),"RECREATE");

   for (std::map<uint8_t, std::vector< std::vector< uint32_t> > >::iterator it = cmd.result.errors.begin(); it!=cmd.result.errors.end(); ++it){
  
  size_t nDel = it->second.size();
  size_t nThr = it->second[0].size();
  std::string histName = "RX_" + std::to_string(it->first);
  std::cout<<histName<<std::endl;
  TH2D bidi_h(histName.c_str(),
              histName.c_str(),
              nDel-1, 0.0, cmd.m_maxDelay,                                              // X axis
              nThr-1, 0, cmd.m_maxThr); // Y axis

  for(size_t iDelay=0; iDelay < nDel; iDelay++)
    for(size_t iThr=0; iThr < nThr; iThr++) {
      //std::cout<<(int)iDelay<<" "<<(int)iThr<<" "<<(int)it->second[iDelay][iThr]<<std::endl;
      bidi_h.SetBinContent(iDelay+1,iThr+1,it->second[iDelay][iThr]);
    }

  bidi_h.Write();
 }
 opto_results.Close(); 
  
  return 0;
}
