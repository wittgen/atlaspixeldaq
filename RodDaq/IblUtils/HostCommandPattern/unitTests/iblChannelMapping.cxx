/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: January-2018
 *
 * MCCConScann using opto-scan in the PPC
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "IBLChannelMapping.h"
#include <string>



class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("txMask", po::value<std::string>(&txMask)->default_value("0xFF00FF"), "Specify Tx mask");
		}

		uint32_t getTxMask(){
		   if ((txMask.substr(0,2) == "0x" && txMask.length() > 10) || (txMask.substr(0,2) != "0x" && txMask.length() > 8)){
			cout<<"ERROR: your Tx mask is too long. Must have 8 digits or fewer"<<endl;
			return 0;
		   }

		  std::stringstream interpreter;
		  uint32_t ch_int;
		  interpreter << std::hex << txMask;
		  interpreter >> ch_int;
		  return ch_int;
		}

	protected:

		std::string txMask;

	private:

};

void printMap(const int txMask, const int rxMask, const std::string & name, const std::array< std::array<int,32>,32 > & map){

std::cout << name<< std::endl;
    std::cout << "TX/RX";
    for(int rxch =0; rxch < 32; rxch++){
      if((rxMask & (1<<rxch))) std::cout <<"|"<<std::internal << std::setw(4)<< rxch;
    }
    std::cout<<"|"<< std::endl;
    std::cout << "====";
    for(int rxch = 0; rxch < 32; rxch++){
      if((rxMask & (1<<rxch))) std::cout << "=+===";
    } std::cout<<"=+";
    std::cout << std::endl;
   for(int txch = 0; txch < 32; txch++)
    {   if(!(txMask & (1<<txch)))continue;
        std::cout<<std::internal <<std::setw(5) << txch;
        for(int rxch = 0; rxch < 32; rxch++){
        	if(!(rxMask & (1<<rxch)))continue;
		if(map[txch][rxch])std::cout << "|"<<std::internal<<std::setw(4)<<map[txch][rxch];
		else std::cout << "|"<<std::setfill(' ')<<std::setw(5);
	}
     std::cout<<"|" << std::endl;
    }
  std::cout << std::endl;


}

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);
  
  std::cout<<"RxMask 0x"<<std::hex<<opt.getChannels()<<" TxMask 0x"<<opt.getTxMask()<<std::dec<<std::endl;

  IBLChannelMapping cmd;
  
  cmd.txMask = opt.getTxMask();

  client.run(cmd);

  printMap(cmd.txMask,cmd.rxMask,"Chip ID",cmd.result.chipNumber);
  printMap(cmd.txMask,cmd.rxMask,"Serial Number",cmd.result.serialNumber);



  return 0;
}
