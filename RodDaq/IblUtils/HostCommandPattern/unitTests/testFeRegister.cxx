// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "TestRegister.h"

#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "testRegister");

  TestRegister cmd;

  cmd.rx = opt.getRx();
  cmd.nReads = 100;
  cmd.valueToWrite = 0;
  
  DefaultClient client(opt);
  client.run(cmd);

  return 0;
}
