#include "Client.h"
#include "ResetRodBusyHistograms.h"
#include "busyOptions.ixx"

#include <iostream>
#include <cstdlib>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	ResetRodBusyHistograms cmd;

	// set command paramters
	cmd.dumpOnPPC = true;

	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;
	
	std::cout << "Disconnecting from server..."<<std::endl<<std::endl;

	// disconnect from server after command returns
	server.disconnectFromServer();

	// print result on host
	if(cmd.result.resetSuccessful)
          std::cout<<"* Rod Histograms reset successfully :-D *"<<std::endl<<std::endl;
        else  
          std::cout<<"* Rod Histograms reset NOT successful :-( *"<<std::endl<<std::endl;

	
	std::cout << "Done." << std::endl;

}

int main(int argc, char** argv) {
  busyOptions o(argc, argv, "enable");
  MyClient client(o.getIp());
  client.run();
  return 0;
}

//int main(int argc, char* argv[]) {

//        MyClient client(IBL_ROD_IP);	

//	std::cout << "setting client IP = " << IBL_ROD_IP << std::endl;

//	client.run();

//	return 0;

//}


//int main(int argc, char** argv) {
//  Options o(argc, argv, "enable");
//  MyClient client(o.getIp());
//  client.run(o.getChannels());  
// return 0;
//}

