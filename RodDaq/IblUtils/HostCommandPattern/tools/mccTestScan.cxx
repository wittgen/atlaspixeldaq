/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 7-December-2015
 *
 * Test MCC digital Scan functionality
 *
 */

#include "DefaultClient.h"
#include "MCCTestScan.h"

int main(int argc, char** argv) {
  Options opt(argc, argv);
  DefaultClient client(opt);

  MCCTestScan scanCmd;

  scanCmd.histConfig_in.cfg[0].nChips = 128;
  scanCmd.histConfig_in.cfg[0].enable = 0;
  scanCmd.histConfig_in.cfg[0].type = 0;
  scanCmd.histConfig_in.cfg[0].maskStep = 5;
  scanCmd.histConfig_in.cfg[0].mStepEven = 0;
  scanCmd.histConfig_in.cfg[0].mStepOdd = 0;
  scanCmd.histConfig_in.cfg[0].chipSel = 1;
  scanCmd.histConfig_in.cfg[0].expectedOccValue = 0;
  scanCmd.histConfig_in.cfg[0].addrRange = scanCmd.histConfig_in.cfg[0].chipSel;
  scanCmd.histConfig_in.cfg[0].scanId = 29;
  scanCmd.histConfig_in.cfg[0].binId = 0;

  scanCmd.setNTrig(1);
  size_t numMaskSteps = 1 << scanCmd.histConfig_in.cfg[0].maskStep;
//   for(size_t iMask = 0; iMask < numMaskSteps; iMask++){
  for(size_t iMask = 0; iMask < 1; iMask++){
    std::cout << std::endl << "Starting to work on mask step " << iMask << std::endl;
    scanCmd.iMask = iMask;
    client.run(scanCmd);
  }
  std::cout << std::endl;

  return 0;
}
