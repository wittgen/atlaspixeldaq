// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "ReconfigModules.h"

#include <iostream>
#include <cstring>


// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results


int main(int argc, char** argv) {
  //0)
  Options opt(argc, argv, "sendConfig");
  DefaultClient client(opt);

  //1)
  ReconfigModules recfgMod;
  
  //2)
  recfgMod.setRxMask(opt.getChannels()); 
  
  client.run(recfgMod);  
  //3)
 
  //4)

  //5)
  std::cout<<"To check that the channels are really configured, check the current they are drawing"<< std::endl;

  //6) 

  return 0;
}
