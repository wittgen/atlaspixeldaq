#include "DefaultClient.h"
#include "GetVersionInfo.h"

#include <iomanip>

int main(int argc, char** argv) {

    Options opt(argc, argv, "getVersionInfo");
    DefaultClient client(opt);
    GetVersionInfo version;

    client.run(version);
    std::cout << "Git hashes of ROD software and firmware:" << std::endl;
    version.printHash(version.result.masterSw, "Master SW");
    version.printHash(version.result.masterFw, "Master FW");
    version.printHash(version.result.slaveASw, "SlaveA SW");
    version.printHash(version.result.slaveAFw, "SlaveA FW");
    version.printHash(version.result.slaveBSw, "SlaveB SW");
    version.printHash(version.result.slaveBFw, "SlaveB FW");

    //std::cout << std::hex << getTime.result.coarseCount << " " << getTime.result.fineCount << std::endl;
    return 0;

}
