#include "DefaultClient.h"
#include "RecoAtECR_manager.h"

#include <iostream>
#include <string>
#include <map>


class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):Options(c, v, t, true) { // Initializing Options from derived class
		  // register_options already callled, so are the extend_options from intermediate inheritance
		  this->extend_options();
		  this->init(c,v);
		}

		virtual void extend_options() {
		  desc->add_options()
                    ("RecoAtECR_mode",po::value<int>(&mode)->default_value(0),"Action of the RecoAtECR_manager")
                    ("debug",po::value<bool>(&recoConfig.debug)->default_value(false),"Toggle Debug mode")	
                    ("readBackCheck",po::value<bool>(&recoConfig.readBackCheck)->default_value(false),"Toggle check ECR Bitstream written in the TX Fifos")	
                    ("customBS",po::value<bool>(&recoConfig.customBS)->default_value(false),"Toggle usage of Custom BitStream writing")	
                    ("allowReconfig",po::value<bool>(&recoConfig.allowReconfig)->default_value(false),"Toggle bypass of Reconfiguration Veto")	
                    ("sleepDelay",po::value<uint32_t>(&recoConfig.sleepDelay)->default_value(false),"Set internal sleep delay")
                    ("verbosity",po::value<int>(&recoConfig.verbosityLevel)->default_value(0),"Sets up verbosity")
                    ("glbRegCfg",po::value<bool>(&recoConfig.glbRegCfg)->default_value(false),"Toggle configure globalRegisters at ECR  FE-I4")
                    ("pixRegCfg",po::value<bool>(&recoConfig.pixRegCfg)->default_value(false),"Toggle configure pixelRegisters at ECR FE-I4")
                    ("pixRegCfgMask",po::value<std::string>(&pixchannels)->default_value("0x0"),"Set mask for pixel reconfig  FE-I4")
                    ("latchesMask",po::value<std::string>(&latchesMask)->default_value("0x0"),"Set reconfiguration latches Mask FE-I4")
                    ("latchesRotation",po::value<int>(&recoConfig.latchesRotation)->default_value(1),"Sets up the rotation on the latches  FE-I4")
                    ("doHitBus",po::value<bool>(&recoConfig.doHitBus)->default_value(true),"Send hit bus at every ECR  FE-I4")
                    ("mccCfg",po::value<bool>(&recoConfig.MccCfg)->default_value(false),"Send MCC configuration FE-I3")
                    ("fei3GlobCfg",po::value<bool>(&recoConfig.fei3GlobCfg)->default_value(false),"Send Global FE config  FE-I3")
                    ("softReset",po::value<bool>(&recoConfig.softReset)->default_value(true),"Front end SOFT reset (otherwise sync is sent)  FE-I3")
                    ;
                 }
 

      std::string pixchannels;
      std::string latchesMask;
      std::string latchesRotation;
	
     RecoAtECR::RecoAtECR_mode getRecoAtECRMode(){return static_cast<RecoAtECR::RecoAtECR_mode>(mode);}
     uint16_t getLatchesMask(){
       if ((latchesMask.substr(0,2) == "0x" && latchesMask.length() > 6) || (latchesMask.substr(0,2) != "0x" && latchesMask.length() > 4)) {
       std::cout<<"ERROR: your latches mask is too long. Must have 4 digits or fewer"<<std::endl;
       return 0;
       }
         std::stringstream interpreter;
         interpreter << std::hex << latchesMask;
         interpreter >> recoConfig.latchesMask;
         return recoConfig.latchesMask;
     }

     const uint32_t getPixChannels(){
         if ((pixchannels.substr(0,2) == "0x" && pixchannels.length() > 10) || (pixchannels.substr(0,2) != "0x" && pixchannels.length() > 8)) {
           cout<<"ERROR: your channel mask is too long. Must have 8 digits or fewer"<<endl;
           return 0;
         }
       std::stringstream interpreter;
       interpreter << std::hex << pixchannels;
       interpreter >> recoConfig.pixmask;
       return recoConfig.pixmask;
     }
     
     const uint32_t getMask(){
         recoConfig.mask = getChannels();
         return recoConfig.mask;
     }
     
     RecoAtECR::RecoConfig getRecoConfig(){return recoConfig;}

	protected:

        int mode;
        RecoAtECR::RecoConfig recoConfig;

};

int main(int argc, char** argv) {
  LocalOptions opt(argc,argv,"RecoAtECR");
  DefaultClient client(opt);
  
  RecoAtECR_manager cmd;
  std::cout<< opt.getRecoAtECRMode()<<std::endl;
  cmd.mode = opt.getRecoAtECRMode();
  
  opt.getMask();
  opt.getPixChannels();
  opt.getLatchesMask();
  
  cmd.m_recoConfig.PrintCfg();
  
  cmd.m_recoConfig = opt.getRecoConfig();
  
  client.run(cmd);

  std::cout<<"Sending RecoAtECR configuration:"<<std::endl;
  std::cout<<"mode            ="<<static_cast<int>(cmd.mode)             <<std::endl;
  std::cout<<"Latches Mask    =0x"<<std::hex<<cmd.m_recoConfig.latchesMask<<std::dec<<std::endl;

  return 0;

}
