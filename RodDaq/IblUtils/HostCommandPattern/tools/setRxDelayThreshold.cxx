/*
 * * F. Meloni <federico.meloni@cern.ch>
 * Tool to set Rx Config
 */

#include <iomanip>

#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>

#include "DefaultClient.h"
#include "SetRxDelayThreshold.h"

int main(int argc, char** argv) {

  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  SetRxDelayThreshold cmd;

  //  Default argument values
  cmd.gain = 65500;
  cmd.rxCh = 19;
  cmd.txCh = 19;
  cmd.delay = 45;
  cmd.threshold = 20000;
  
  //  Pick up any changes supplied via command line
  for(int i = 0; i < argc - 1; i++){
    std::string argId = std::string(argv[i]);
    int argValue = atoi(argv[i+1]);
    if      (argId.find("--gain")        != std::string::npos) cmd.gain          = argValue;
    else if (argId.find("--delay") 	 != std::string::npos) cmd.delay   	 = argValue;
    else if (argId.find("--threshold")   != std::string::npos) cmd.threshold     = argValue;
    else if (argId.find("--rxCh")        != std::string::npos) cmd.rxCh          = argValue;
    else if (argId.find("--txCh")        != std::string::npos) cmd.txCh          = argValue;
  }
  
  //  Send cmd from host to PPC
  std::cout << "Sending command..." << std::endl;  
  client.run(cmd);
  
  std::cout << "Command executed" << std::endl;

  return 0;
}
