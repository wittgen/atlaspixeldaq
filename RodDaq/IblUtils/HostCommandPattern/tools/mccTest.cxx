/*
 * mccTest
 * By: Laser
 * Executable for MCCTest
 */

#include "DefaultClient.h"
#include "MCCTest.h"

#include <iostream>
#include <cstring>

int main(int argc, char **argv) {
  Options opt(argc, argv);

  MCCTest mcct;

  mcct.setModMask(opt.getChannels());
  mcct.setNTrig(1);

  DefaultClient client(opt);
  client.run(mcct);

  return 0;
}
