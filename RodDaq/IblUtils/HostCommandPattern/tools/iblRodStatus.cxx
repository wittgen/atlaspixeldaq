#include "Client.h"
#include "IblRodStatus.h"
#include "busyOptions.ixx"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec


void dump_busy_status(uint32_t busy_status) {
	uint32_t mask = 0x80000000;
	char line[200];
	while (mask) { // The idea is that mask will be 0 when we have done the last shift
		snprintf(line, sizeof(line), " %s", (busy_status & mask) ? "X" : "-");
		std::cout << line;
		mask >>= 1;
	}
	std::cout << std::hex << "\t Value: 0x" << busy_status << std::dec;
	std::cout << std::endl;
}


// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		std::cout << "no connection to server" << std::endl;
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	IblRodStatus cmd;

	// set command paramters
	cmd.dumpOnPPC = true;

	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;
	
	std::cout << "Disconnecting from server..."<<std::endl;

	// disconnect from server after command returns
	server.disconnectFromServer();

	// print result on host
	std::cout << "STATUS\t"; dump_busy_status(cmd.result.masterBusyCurrentStatusValue & 0xFF);
	std::cout << "MASK  \t"; dump_busy_status(cmd.result.masterBusyMaskValue & 0xFF );
	std::cout << "FORCE \t"; dump_busy_status((cmd.result.masterBusyMaskValue >> 8) & 0xFF);
	
	//std::cout<<" Master Busy Current Status Value = "<<HEX(0x000000ff & cmd.result.masterBusyCurrentStatusValue)<<", this means: "<<std::endl;
	/*if (BUSY_MACRO(cmd.result.masterBusyCurrentStatusValue))
		std::cout<<"\t ROD BUSY !!!"<<std::endl<<std::endl;
	else
		std::cout<<"\t ROD NOT BUSY !!!"<<std::endl<<std::endl;        
        
*/
	
        // print results for both slaves
        std::cout << "SLAVE A" << std::endl;
	std::cout << "STATUS\t"; dump_busy_status(cmd.result.slave1BusyCurrentStatusValue);
	std::cout << "MASK  \t"; dump_busy_status(cmd.result.slave1BusyMaskValue);
	std::cout << "FORCE \t"; dump_busy_status(cmd.result.slave1BusyForceValue);
        std::cout << "SLAVE B" << std::endl;
	std::cout << "STATUS\t"; dump_busy_status(cmd.result.slave2BusyCurrentStatusValue);
	std::cout << "MASK  \t"; dump_busy_status(cmd.result.slave2BusyMaskValue);
	std::cout << "FORCE \t"; dump_busy_status(cmd.result.slave2BusyForceValue);
    

	std::cout << "Done." << std::endl;

}

int main(int argc, char** argv) {

  busyOptions o(argc, argv, "enable");
  std::cout<< "options read"<<std::endl;
  MyClient client(o.getIp());
  client.run();
  return 0;
}

