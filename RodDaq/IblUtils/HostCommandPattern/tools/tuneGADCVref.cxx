/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: October-2018
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "TuneGADCVref.h"
#include <string>

#include "TFile.h"
#include "TH2F.h"
#include "TGraph.h"


void getIBLModNameFromRxCh(std::string rodN, int rxCh, std::string &modName, int &feNr);

int main(int argc, char** argv) {

  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  std::time_t time_result  = std::time(nullptr);
  struct tm tt = *(std::localtime(&time_result));
  std::cout<<"Execution Time: "<<std::asctime(std::localtime(&time_result))<<std::endl;
  
  TuneGADCVref cmd;
  cmd.m_rxMask = opt.getChannels();
  
  client.run(cmd);

  std::string rodName = opt.getRodName();
  if(rodName.empty())rodName = opt.getHostName();
  if(rodName.empty())rodName = opt.getIp();

  std::cout<<"Using RODName "<<rodName<<std::endl;

    std::string formattedtime = std::to_string(tt.tm_year+1900)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_min)+"_"+ std::to_string(tt.tm_sec);

    std::string filename = "GADCVRefTune_"+rodName +"_"+formattedtime+".root";
    TFile outfile(filename.c_str(),"RECREATE");

  for (std::map<uint8_t,std::vector<GADCVrefResults> > ::iterator it = cmd.result.GADCVref.begin(); it != cmd.result.GADCVref.end(); ++it){

  //std::cout<<rodName<<" "<<(int)it->first<<" "<<std::endl;
  
  	double score=0, bestScore=0;
  	int vRef=160;
  	
  	TGraph gr1;
        std::string grName = "VcalRange_RX_"+std::to_string(it->first);
        gr1.SetName(grName.c_str());
        gr1.SetTitle(grName.c_str());
        
        TGraph gr2;
        grName = "ILEAK_RX_"+std::to_string(it->first);
        gr2.SetName(grName.c_str());
        gr2.SetTitle(grName.c_str());
  	
  	TGraph gr3;
        grName = "SCORE_RX_"+std::to_string(it->first);
        gr3.SetName(grName.c_str());
        gr3.SetTitle(grName.c_str());
  	
  	for(size_t s=0;s<it->second.size();s++){

  	uint32_t ILeak = it->second.at(s).ILeak;
  	uint32_t range = it->second.at(s).vCalRange;
  	//std::cout<<"Vref "<<it->second.at(s).Vref<<" ILeak "<<ILeak<<" VcalRange "<<range<<std::endl;
  	
  	gr1.SetPoint(s,it->second.at(s).Vref,range);
        gr2.SetPoint(s,it->second.at(s).Vref,ILeak);

           if(it->second.at(s).Vref<4)continue;
	   if(ILeak < 200 || ILeak >950){gr3.SetPoint(s,it->second.at(s).Vref,0); continue;} //Cut on ILeak range
           if( range >800 || range <40 ){gr3.SetPoint(s,it->second.at(s).Vref,0); continue;}//Cut on vcal range

           score = 100./range;
           gr3.SetPoint(s,it->second.at(s).Vref,score);
           
           if(score==0){
           vRef = it->second.at(s).Vref;
           bestScore = score;
           }
           else if(score > bestScore){
           vRef = it->second.at(s).Vref;
           bestScore = score;
           }
  	
  	
  	}
     
     outfile.cd();
     gr1.Write();
     gr2.Write();
     gr3.Write();

     std::string modName;
     int feNr;
     getIBLModNameFromRxCh(rodName, it->first, modName, feNr);
     std::cout<<modName<<" "<<feNr<<" "<<vRef<<std::endl;
  
  }

  outfile.Close();
 
  return 0;
}

//TODO pick up from the connectivity, we have to fix RODBOCLINKMAPS to do so
void getIBLModNameFromRxCh(std::string rodN, int rxCh, std::string &modName, int &feNr){

  int stave,module;
  string side;
      
      if(rxCh<16)side="A";
      else side = "C";
      
    if(rodN == "ROD_C3_S11" || rodN == "ROD_C3_S18" ){//SR1
      
      if(rodN == "ROD_C3_S11") stave = 18;
      else stave = 17;
      
      if(side == "A")module= 8-rxCh/2;
      else module= 1 +(rxCh-16)/2;
      
      feNr = (rxCh+1)%2;
      
    } else {//PIT
      int slot = atoi(rodN.substr(8,rodN.length()).c_str());
      if(slot>13)stave = slot-6;
      else stave = slot -5;
      
      if(side == "A")module= 1 + rxCh/2;
      else module= 8 - (rxCh-16)/2;

      feNr = rxCh%2;
    }
      
      int group = (module-1)/2 +1;
      
      std::ostringstream oss;
      oss<<std::setw(2)<<std::setfill('0')<<stave;
      
      modName = "LI_S"+oss.str()+"_"+side+"_M"+std::to_string(group)+"_"+side+std::to_string(module);
      
      //std::cout<<rxCh<<" "<<modName<<" "<<feNr<<std::endl;

}




