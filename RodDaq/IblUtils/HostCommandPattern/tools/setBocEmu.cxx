// L. Jeanty <laura.jeanty@cern.ch>
//
// Turns on or off boc FE emulator (can also be called from dataTakingTools.cxx)

#include "DataTakingTools.h"
#include "DefaultClient.h"
#include <iostream>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("enable", po::value<int>(&enable)->default_value(0), "Enables (1) or disables (0) the BOC emulator");
		}

		int getEnable() { return enable; }

	protected:

	private:
		int enable;
};


int main(int argc, char** argv){

  DataTakingTools cmd;
  cmd.action = SetBocEmu;

  LocalOptions options(argc , argv);
  cmd.value = options.getEnable();

  DefaultClient client(options);
  client.run(cmd);
  
  return 0;
}
