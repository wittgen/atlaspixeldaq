#include "PrintCommands.h"
#include "RodCommand.h"
#include "DefaultClient.h"

#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdlib.h>

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv){
  Options opt(argc, argv);
  DefaultClient client(opt);  

  PrintCommands printCmd;
  client.run(printCmd);

  return 0;
}
