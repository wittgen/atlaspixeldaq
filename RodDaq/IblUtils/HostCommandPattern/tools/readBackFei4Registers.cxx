#include "DefaultClient.h"
#include "ReadBackFei4Registers.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include "TFile.h"
#include "TH2F.h"

//#include <ctime>


int getBitFlips(uint16_t a, uint16_t b);

//USAGE Example:
//readBackFei4Registers --ip 192.168.13.211 --global 0  --readbackMode 0 --ch 0xf0000000 --shiftregleft 0


int main(int argc, char** argv) {
  
  Options opt(argc,argv);
  DefaultClient client(opt);
  
  std::string ip = opt.getIp();
  std::cout<<"The IP: "<<ip<<std::endl;
  if (ip.find("89")==std::string::npos) {
    std::cout<<"Attempt to run on a non-IBL ROD. Exiting"<<std::endl;
    return 1;
  }
  
  stringstream ss;
  int ROD_SLOT = -1;
  ss<<ip.substr(10,3);
  ss>>ROD_SLOT;
  ROD_SLOT-=200;
  ss.str("");
  ss.clear();

  bool debug = false;

  std::time_t time_result  = std::time(nullptr);
  
  struct tm tt = *(std::localtime(&time_result));
  std::cout<<"Execution Time: "<<std::asctime(std::localtime(&time_result))<<std::endl;
  
  
  ReadBackFei4Registers readback_cmd;
  //readback_cmd.m_modMask = 0xf0fff030; //ALL C3_S11 modules
  //Who to read back.
  readback_cmd.m_modMask   = opt.getChannels();
  //Read back global or pixel
  readback_cmd.m_global    = opt.getGlobal();
  //Used for what to leave in the shift register
  readback_cmd.m_DC        = opt.getShiftRegLeft();
  //not used
  readback_cmd.m_CP        = 0x0;
  //Delay between the shift register read backs
  readback_cmd.delay       = opt.getDelay();
  //Read back mode
  readback_cmd.rb_mode       = (ReadBackFei4Registers::ReadBackMode) opt.getReadBackMode(); 
  //readback_cmd.rb_mode     = ReadBackFei4Registers::RB_ClearSR;
  //readback_cmd.rb_mode     = ReadBackFei4Registers::RB_WriteCustomPattern;

  std::cout<<"ShiftRegLeft: "<< (int)readback_cmd.m_DC<<std::endl;
  std::cout<<"rbmode "<<readback_cmd.rb_mode<<std::endl;
  
  client.run(readback_cmd);



  
  if (readback_cmd.m_global) {
    ofstream grResults;
    std::string filename = "GR_ROD_I1_S"+std::to_string(ROD_SLOT)+"_Result_";
    std::string formattedtime = std::to_string(tt.tm_sec)+"_"+std::to_string(tt.tm_min)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_year+1900);
    filename+=formattedtime+".txt";
    grResults.open(filename);
    grResults<<"GRindex"<<" Channel " <<" Original "<<" ReadBack "<<" Matches" <<" nFlips"<<std::endl;
    
    for (std::map<uint32_t,std::vector<uint32_t> >::iterator it = readback_cmd.result.GRCfgMap.begin(); it!=readback_cmd.result.GRCfgMap.end(); ++it)
      {
	int j = 1;
	for (std::vector<uint32_t>::iterator it2 = (it->second).begin(); it2!=(it->second).end();++it2) {
	  uint32_t original = (*it2)>>16;
	  uint32_t readback = (*it2)&0xffff;
	  bool matches      = (original == readback);
	  int nbitFlips     = getBitFlips(original,readback);
	  
	  grResults<<j<<" "<<std::hex<<"0x"<<it->first<<" 0x"<<original<<" 0x"<<readback<<std::dec<<" "<<matches<<" "<<nbitFlips<<std::endl;
	  //grResults<<std::hex<<"0x"<<std::to_string(it->first)<<std::dec<<" "<<std::to_string(j)<<" "<<std::hex<<" 0x"<<std::to_string((*it2))<<std::dec<<"\n";
	  //grResults<<std::to_string(it->first)<<" "<<(readback_cmd.result.GRcontent[i]>>16)<<" "<<(readback_cmd.result.GRcontent[i] & 0xff)<<"\n";
	  ++j;
	}
	grResults<<std::endl;
	grResults<<std::endl;
	grResults<<std::endl;
      }
    grResults.close();
  }
  
  
  // dump of the information into root files.
  
  
  //all(DC)[BIT]
  
  if (debug)
    {
      for (std::map< uint32_t, Fei4PixelCfg>::iterator PixCfgMapIt = readback_cmd.result.PixCfgMap.begin(); PixCfgMapIt != readback_cmd.result.PixCfgMap.end(); ++PixCfgMapIt) {
	(PixCfgMapIt->second).all(0)[0].dump();
	(PixCfgMapIt->second).all(0)[1].dump();
	(PixCfgMapIt->second).all(0)[2].dump();
	(PixCfgMapIt->second).all(0)[3].dump();
	(PixCfgMapIt->second).all(0)[4].dump();
      }
    }
  
  if (!readback_cmd.m_global &&  (readback_cmd.rb_mode == ReadBackFei4Registers::RB_ReadShiftRegister || readback_cmd.rb_mode == ReadBackFei4Registers::RB_PixelRegisters)) {
    
    std::string filename = "rbFe_";
    
    if (readback_cmd.rb_mode == ReadBackFei4Registers::RB_ReadShiftRegister)
      filename = "ShiftRegisterRb_";
    
    filename+="ROD_I1_S"+std::to_string(ROD_SLOT)+"_";
    
    std::string formattedtime = std::to_string(tt.tm_sec)+"_"+std::to_string(tt.tm_min)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_year+1900);
    //TFile outfile((filename+"_"+std::to_string(time_result)+".root").c_str(),"RECREATE");
    TFile outfile((filename+"_"+formattedtime+".root").c_str(),"RECREATE");
    
    
    for (std::map< uint32_t, Fei4PixelCfg>::iterator PixCfgMapIt = readback_cmd.result.PixCfgMap.begin(); PixCfgMapIt != readback_cmd.result.PixCfgMap.end(); ++PixCfgMapIt) {
      TH2F  tdac((std::to_string(PixCfgMapIt->first)+"_tdac").c_str(),(std::to_string(PixCfgMapIt->first)+"_tdac").c_str(),80,-0.5,79.5,336,-0.5,335.5);
      TH2F  fdac((std::to_string(PixCfgMapIt->first)+"_fdac").c_str(),(std::to_string(PixCfgMapIt->first)+"_tdac").c_str(),80,-0.5,79.5,336,-0.5,335.5);
      std::vector <TH2F> bit_h;
      for (int bit=0;bit<13;bit++){
	bit_h.push_back(TH2F((std::to_string(PixCfgMapIt->first)+"_bit_"+std::to_string(bit)).c_str(),(std::to_string(PixCfgMapIt->first)+"_bit_"+std::to_string(bit)).c_str(),80,-0.5,79.5,336,-0.5,335.5));
      }
      
      
      for (unsigned int irow=1; irow<=336; irow++) {
	for (unsigned int idc=0; idc<40; idc++) { 
	  for (unsigned int col=1; col<=2; col++){
	    uint32_t TDAC = 0;
	    uint32_t FDAC = 0;
	    
	    for (int bit=0;bit<13;bit++){
	      bit_h[bit].SetBinContent(2*idc+col,irow,(PixCfgMapIt->second).all(idc)[bit].getPixel(irow,col));
	    }
	    
	    for (int bit=4;bit>=0;bit--){
	      TDAC+=((PixCfgMapIt->second).TDAC(idc)[bit].getPixel(irow,col)<<(4-bit));}
	    
	    for (int bit=0;bit<4;bit++){
	      FDAC+=((PixCfgMapIt->second).FDAC(idc)[bit].getPixel(irow,col)<<(bit));}
	    
	    
	    
	    tdac.SetBinContent(2*idc+col,irow,TDAC);
	    fdac.SetBinContent(2*idc+col,irow,FDAC);
	    
	    
	}//col
	}//idc     
      }//row
     
      if (readback_cmd.rb_mode == ReadBackFei4Registers::RB_ReadShiftRegister) {
	std::string hname = "ShiftRegister";
	
	bit_h[12].Write((hname+bit_h[12].GetName()).c_str());
      }
      else {
	tdac.Write();
	fdac.Write();
      	for (int bit=0;bit<13;bit++){
	  bit_h[bit].Write();
	}
      }
      
      
    }//Config Map
    
    
    outfile.Close();
    
  }
}

int getBitFlips(uint16_t a, uint16_t b) {
  
  int bf = 0;
  int n  = a^b;
  while (n) {
    bf+= n & 1;
    n>>=1;
  }
  return bf;
    
}

