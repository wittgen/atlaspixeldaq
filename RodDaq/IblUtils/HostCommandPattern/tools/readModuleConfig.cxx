// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "ReadModuleConfig.h"

#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "readConfig");

  bool fromFE = true;

  ReadModuleConfig readCfg;

  readCfg.readCalibConfig();
  readCfg.setFeMask(opt.getChannels());
  readCfg.setReadFrontEnd(fromFE);
  readCfg.setFeType(opt.getFeType());

  DefaultClient client(opt);
  client.run(readCfg);

  cout<<"Read back configs for mask: 0x"<<std::hex<<(int)opt.getChannels()<<std::dec<<endl;
  cout<<"Reading "<<(int)readCfg.result.nMod<<" modules"<<endl;
  for (int i = 0; i < readCfg.result.nMod; i++) {
    std::cout<<"*************************"<<std::endl;
    std::cout<<"For FE "<<i<<", the config is: "<<std::endl;
    if (opt.getFeType() == "I3") {
      if (readCfg.result.moduleConfigsFei3[i].revision == 0) continue;
      readCfg.result.moduleConfigsFei3[i].dump();
      std::cout << "VcalCoeff1: " << readCfg.result.moduleConfigsFei3[i].m_chipCfgs[0].getVcalCoeff1() << std::endl;
      std::cout << "GlobalTDAC: " << (int) readCfg.result.moduleConfigsFei3[i].m_chipCfgs[0].readRegGlobal(Fei3::GlobReg::GlobalTDAC) << std::endl;
      std::cout << "globcfg[7]: 0x" << std::hex << readCfg.result.moduleConfigsFei3[i].m_chipCfgs[0].globcfg[7] << std::dec << std::endl;
      std::cout << "TDAC(row = 0, col = 0): " << readCfg.result.moduleConfigsFei3[i].m_chipCfgs[0].readTDACPixel(0, 0) << std::endl;
    }
    else {
      readCfg.result.moduleConfigsFei4[i].dump();
    }
    std::cout<<"*************************"<<std::endl;
  }
  return 0;
}
