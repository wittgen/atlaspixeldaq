#include "DefaultClient.h"
#include "GetStatus.h"

#include <iostream>
#include <cstdlib>

int main(int argc,  char** argv) {

  Options opt(argc , argv, "");

  GetStatus cmd;

  DefaultClient client(opt);

  while (1) {
  client.run(cmd);

  IblRodScanStatus ppcScanStatus = cmd.result.m_status;

  std::cout<<" at mask step: "    <<(int)ppcScanStatus.mask<<
    ", at par step: "    <<(int)ppcScanStatus.par<<
    ", at DC step: "     <<(int)ppcScanStatus.dc<<
    ", at trigger step: "<<(int)ppcScanStatus.trig<<
    std::endl;

  usleep(10);
  }

  return 0;
}
