/*
 * mccInjectPattern
 * By: Laser
 * Executable for MCCInjectPattern
 */

#include "DefaultClient.h"
#include "MCCInjectPattern.h"

#include <iostream>
#include <cstring>

int main(int argc, char **argv) {
  Options opt(argc, argv);

  MCCInjectPattern mccip;

  mccip.setVerbosity(11);
  mccip.setPattern(0x86fdffc0);
  mccip.setPattern(0xBFE);
  mccip.setNPattern(1);
  mccip.setRxMask(0x80000);

  DefaultClient client(opt);
  client.run(mccip);

  return 0;
}
