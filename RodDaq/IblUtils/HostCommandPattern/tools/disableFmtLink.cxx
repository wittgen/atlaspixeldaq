/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2015-III-8
 * Description: this tool allows disabling individual formatter links (via the ch mask)
 * 		In case no mask is provided (0x0) the tool tries to determine which links it should disable
 * 		The tool will ask for confirmation before performing any action
 * Update: 2015-V-16	K. Potamianos -- adding check on EFB status
 */ 

#include "DataTakingTools.h"
#include "ReadRodSlaveBusyStatus.h"
#include "WriteRodSlaveBusyMask.h"
#include "SilentClient.h"
#include <iostream>
#include <iomanip>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec 

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			// Extending options (but only for this command)
			desc->add_options()
				("force", po::value<bool>(&force)->implicit_value(1), "Disables the prompt for confirmation when disabling links");

			// Overriding value for option
			override_option_default<std::string>("ch", "0x00000000");
		}

		bool force;

	protected:

	private:

};

int main(int argc, char** argv){

	LocalOptions options(argc , argv);
	SilentClient client(options);

	DataTakingTools cmdDisableFmtLink;
	cmdDisableFmtLink.action = DisableFmtLink;
	cmdDisableFmtLink.value = options.getValue(); // value to write if relevant
	cmdDisableFmtLink.address = options.getAddress(); // address to read/write if relevant
	cmdDisableFmtLink.channels = options.getChannels(); // channels to send commands to if relevant

	ReadRodSlaveBusyStatus getRodSlaveBusyStatus;
	WriteRodSlaveBusyMask writeRodSlaveBusyMask;
	uint32_t rodSlaveBusyMask;
	uint32_t fmtBusyMask, efbBusyMask1;

	if( cmdDisableFmtLink.channels == 0 ) { // Guess from BUSY status
		std::cout << "You didn't specify any channel, so I would assume you want to figure it out..." << std::endl;

		for(int sId = 0 ; sId < 2 ; ++sId) {
			bool isSlaveA = !sId;
			// Probing slave A
			getRodSlaveBusyStatus.targetIsSlaveB = !isSlaveA;
			client.run(getRodSlaveBusyStatus);
			// Adjusting list of formatter links to disable
			fmtBusyMask = (getRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 13); // Hard-coded: keeping only the FMT/LINK flags
			fmtBusyMask |= (getRodSlaveBusyStatus.result.slaveBusyForceValue >> 13); // Hard-coded: keeping only the FMT/LINK flags
			// Checking whether any EFB is BUSY
			efbBusyMask1 = (getRodSlaveBusyStatus.result.slaveBusyCurrentStatusValue >> 5) & 0xF; // Hard-coded: keeping only the EFB flags
			efbBusyMask1 |= (getRodSlaveBusyStatus.result.slaveBusyForceValue >> 5) & 0xF; // Hard-coded: keeping only the EFB flags

			uint8_t eBM = efbBusyMask1 & 0xFF;
			uint16_t fBM = 0x0; // Formattery "BUSY" mask form efbBusyMask1 value
			// Update the fmtBusyMask based on the EFBs that are busy
			for(uint32_t i = 0 ; i < 4 ; ++i) {
				if(i) fBM <<= 4;
				if( eBM & 0x8 ) fBM |= 0xF;
				eBM <<= 1;
			}

			if(fBM) std::cout << "[Slave " << (isSlaveA ? 'A' : 'B') << "] The following formatter disable mask comes from BUSY EFBs: " << HEXF( 4, fBM ) << std::endl;
			fmtBusyMask |= fBM;

			cmdDisableFmtLink.channels |= fmtBusyMask << (isSlaveA ? 0 : 16);
		}

		std::cout << "I've determined that the following mask is busy: " << HEXF( 8, cmdDisableFmtLink.channels ) << std::endl;
		if( cmdDisableFmtLink.channels ) {
			if(!options.force) {
				std::string response;
				std::cout << "Shall we disable the corresponding links ? y/N" << std::endl;
				std::getline(std::cin,response);
				if(response != "yes" && response != "y") return 0;
			}
		} else { std::cout << "Everything is allright! Nothing to be done right now. Goodbye!" << std::endl; return 0; }
	}

	// Disable the formatter links
	client.run(cmdDisableFmtLink);

	uint8_t slink_mask = 0x0;

  // At this point cmdDisableFmtLink.channels contains the mask of the links that just got disabled [31:0]
	for(uint32_t chMask = cmdDisableFmtLink.channels ; chMask != 0 ; ) {
    // Determine which slave we are using
		if(chMask&0x0000FFFF) getRodSlaveBusyStatus.targetIsSlaveB = false;
		else getRodSlaveBusyStatus.targetIsSlaveB = true;

		// Get the current mask information from ROD
		client.run(getRodSlaveBusyStatus);
		rodSlaveBusyMask = getRodSlaveBusyStatus.result.slaveBusyMaskValue >> 13; // Hard-coded: keeping only the FMT/LINK flags
		std::cout << "Got the following BUSY FMT mask from slave " << (getRodSlaveBusyStatus.targetIsSlaveB?"B":"A") << ": " << HEXF( 4, rodSlaveBusyMask );
		std::cout << ", i.e. (in full) " << HEXF( 8, getRodSlaveBusyStatus.result.slaveBusyMaskValue ) << std::endl;;

		// Adjust the mask to what we need
    rodSlaveBusyMask |= getRodSlaveBusyStatus.targetIsSlaveB ? ((chMask & 0xFFFF0000) >> 16) : (chMask & 0x0000FFFF);
    std::cout << "Updating it to " << HEXF( 4, rodSlaveBusyMask );
    writeRodSlaveBusyMask.targetIsSlaveB = getRodSlaveBusyStatus.targetIsSlaveB;
    // Re-adding the lower end of the maskValue
    writeRodSlaveBusyMask.maskValue = (getRodSlaveBusyStatus.result.slaveBusyMaskValue & 0x1FFF) + (rodSlaveBusyMask << 13);

		// Update the mask with a proper masking of the EFB bits when 4 formatter links are disabled
		uint8_t efbBusyMask2 = 0x0;
		for(uint32_t i = 0 ; i < 4 ; ++i) {
			if(((rodSlaveBusyMask>>(i*4)) & 0xF) == 0xF)
				efbBusyMask2 |= (0x1 << i);
		}
		writeRodSlaveBusyMask.maskValue |= (efbBusyMask2 << 5);
		
		// Disabling the S-Link down and Xoff corresponding to the EFB
		if(efbBusyMask2 == 0x3 || efbBusyMask2 == 0xc || efbBusyMask2 == 0xf) {
			writeRodSlaveBusyMask.maskValue |= (efbBusyMask2 << 1);
			slink_mask |= (efbBusyMask2 << (getRodSlaveBusyStatus.targetIsSlaveB ? 4 : 0));
		}

		std::cout << ", i.e. (in full, including EFB and SLINK masking) " << HEXF( 8, writeRodSlaveBusyMask.maskValue ) << std::endl;
		if(slink_mask) { //We've disabled an SLINK, so we need to inform that it should be disabled in ATLAS or the DAQSlice
			for(int i = 0 ; i < 4 ; ++i)
				if( ((slink_mask>>(2*i)) & 0x3) == 0x3 )
					std::cerr << "INFO: Please run the following command ${PIX_LIB}/Examples/sendDisableMessage ATLAS ROD_I1_S"
						<< (int)options.getSlot() << "_" << i << " Appl_PixelRCD-IBL ROD" << std::endl;
		}

		// Update the mask on the ROD
		client.run(writeRodSlaveBusyMask);
		
		// Clearing the mask for the slave we already processed
		chMask &= getRodSlaveBusyStatus.targetIsSlaveB ? 0x00000000 : 0xFFFF0000;
	}

	return 0;
}
