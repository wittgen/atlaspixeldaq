
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2017-III-1
 */

#include "DefaultClient.h"
#include "QSTestHelper.h"

#include <iostream>
#include <cstdlib>

int main(int argc,  char** argv) {

  Options opt(argc , argv, "qsTestHelper");

  QSTestHelper qsTestHelper;

  qsTestHelper.m_qsTestId = QSTestHelper::Timeout;
  qsTestHelper.m_rxMask = opt.getChannels();

  DefaultClient client(opt);
  client.run(qsTestHelper);

  return 0;

}
