/*
 * * F. Meloni <federico.meloni@cern.ch>
 * Date: 16-March-2016
 *
 * Gets Rx configuration
 *
 */

#include <iomanip>
#include <fstream>

#include <boost/random/random_device.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/variate_generator.hpp>
#include <boost/random/normal_distribution.hpp>

#include "DefaultClient.h"
#include "GetRxConfig.h"
#include <string>
#include "GeneralResults.h"

int main(int argc, char** argv) {

  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  GetRxConfig cmd;

  //  Default argument values
  cmd.rxCh = 19;

  //  Pick up any changes supplied via command line
  for(int i = 0; i < argc - 1; i++){
    std::string argId = std::string(argv[i]);
    int argValue = atoi(argv[i+1]);
    if (argId.find("--rxCh")        != std::string::npos) cmd.rxCh          = argValue;
  }
  
  //  Send cmd from host to PPC and retrieve cmd.result back from it
  std::cout << "Sending command..." << std::endl;  
  client.run(cmd);
  
  std::cout << "Command executed" << std::endl;
  
  //  Look at result output generated here and make sure ratios printed are consistent with what PPC reports
  std::cout << "Rx Configuration" << std::endl;
  std::cout << "Rx coarse delay: " << (int)cmd.result.coarseDelay << std::endl;
  std::cout << "Rx fine delay: " << (int)cmd.result.fineDelay << std::endl;
  std::cout << "Rx threshold: " << (int)cmd.result.rxThreshold << std::endl;
  std::cout << "Rx gain: " << (int)cmd.result.rxGain << std::endl;

  return 0;
}
