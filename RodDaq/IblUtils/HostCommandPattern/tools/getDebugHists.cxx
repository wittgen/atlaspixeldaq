#include "GetDebugHists.h"
#include "DefaultClient.h"
#include "Fei4Data.h"

#include <algorithm>
#include <functional>
#include <boost/lexical_cast.hpp>
#include "TH2.h"
#include "TFile.h"

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results



TH2F* fei4ToROOT(const Fei4Hist& hist) {
  std::string name = "";
  std::string labels = ";Col;Row;";
  switch (hist.data) {
  case Fei4Data::OCC: 
    name += "occ";
    labels += "Occupancy";
    break;
  case Fei4Data::TOT: 
    name += "tot";
    labels += "Sum ToT";
    break;
  }

  switch (hist.fifo) {
  case Fei4Data::BOC: 
    name += "_boc";
    break;
  case Fei4Data::INMEM: 
    name += "_inmem";
    break;
  }
  
  name += "_rx" + boost::lexical_cast<std::string>(static_cast<unsigned>(hist.rxCh));

  TH2F* rHist = new TH2F(name.c_str(), (name+labels).c_str(), Fei4Hist::ncol, 0.5, Fei4Hist::ncol + 0.5, Fei4Hist::nrow, 0.5, Fei4Hist::nrow + 0.5);

  for (int i=1; i<=Fei4Hist::ncol; ++i) {
    for(int j=1; j<=Fei4Hist::nrow; ++j) {
      rHist->SetBinContent(i, j, hist[i-1][j-1]);
    }
  }
  return rHist;
}

int main(int argc, char** argv){
  GetDebugHists cmd;
  
  Options options(argc , argv);
  DefaultClient client(options);
  client.run(cmd);

  std::cout << "Found " << cmd.result.hists.size() << " debug histograms." << std::endl;

  // Save all histograms to a root file.
  TFile* output = TFile::Open("debugHists.root", "recreate");
  std::vector<TH2F*> rHists;
  rHists.resize(cmd.result.hists.size());

  std::transform(cmd.result.hists.begin(), cmd.result.hists.end(), rHists.begin(), fei4ToROOT); 
  for (std::vector<TH2F*>::iterator it = rHists.begin(); it != rHists.end(); ++it) (*it)->Write();

  output->Close();
  return 0;
}

