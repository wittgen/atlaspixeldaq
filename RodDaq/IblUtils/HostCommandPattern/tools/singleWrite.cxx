#include "SilentClient.h"
#include "SingleWrite.h"
#include "SingleRead.h"

#include <iostream>
#include <cstdlib>

class LocalOptions: public Options {

  public:

    LocalOptions(int c, char* v[], std::string t = ""):
      Options(c, v, t, true) { // Initializing Options from derived class
        // register_options already callled, so are the extend_options from intermediate inheritance
        this->extend_options();
        this->init(c,v);
      }
    virtual void extend_options() {
      // Overriding value for option
      override_option_default<std::string>("addr", "0xffffffff");
      override_option_default<int>("slave", 0);
    }

  protected:

  private:

};

int main(int argc,  char** argv) {

  LocalOptions opt(argc , argv, "singlewrite");

  if( opt.getAddress() == 0xffffffff ) {
    std::cerr << "ERROR: it seems you didn't specify the --addr argument" << std::endl;
    return 1;
  }

  SingleWrite cmd;
  SingleRead cmd2;

  cmd.address = opt.getAddress();
  cmd.value = opt.getValue();
  cmd2.address = opt.getAddress();

  std::cout << "SlaveId=" << opt.getSlave() << std::endl;
  if(opt.getSlave()==1 && opt.getAddress() <= 0x10000) {
    std::cout << "Adding 0x10000 to the address because you requested to write to slave B!" << std::endl;
    cmd.address += 0x10000;
    cmd2.address += 0x10000;
  }

  if(opt.getSlave()==0 && opt.getAddress() >= 0x10000) {
    std::cerr << "Warning: you asked to write to slave A but you specified an address that refers to slave B!" << std::endl;
  }

  if(cmd.address < 0x10000)
    std::cout << "Info: writing to slave A!" << std::endl;  
  else if(cmd.address < 0x20000)
    std::cout << "Info: writing to slave B!" << std::endl;  
  else {
    std::cout << "You are not writing to any slave registers:";
    std::cout << "I hope you know what you are doing!!!" << std::endl;
  }

  SilentClient client(opt);
  client.run(cmd);
  client.run(cmd2);

  std::cout<<"After writing, from reg address: "<<std::hex<<(int)cmd.address<<" read value: 0x"<<(int)cmd2.result.value<<std::dec<<std::endl;

  return 0;
}


