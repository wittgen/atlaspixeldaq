#include "DefaultClient.h"
#include "SetUart.h"

#include <iostream>
#include <cstdlib>

int main(int argc,  char** argv) {

  Options opt(argc , argv, "setUart");

  SetUart setUart;

  setUart.uart = opt.getUart();

  DefaultClient client(opt);
  client.run(setUart);

  return 0;

}
