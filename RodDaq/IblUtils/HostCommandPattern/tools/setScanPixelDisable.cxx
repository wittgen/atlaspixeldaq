// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "SetScanPixelDisable.h"

#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "");

  SetScanPixelDisable cmd;

  cmd.rx = 3;
  cmd.pixel = 58;
  cmd.dc = 13;
  cmd.clearAdd = true;

  DefaultClient client(opt);
  client.run(cmd);

  cmd.clearAdd = false;
  cmd.pixel = 59;
  client.run(cmd);

  cmd.pixel = 612;
  client.run(cmd);

  cmd.pixel = 613;
  client.run(cmd);


  return 0;
}
