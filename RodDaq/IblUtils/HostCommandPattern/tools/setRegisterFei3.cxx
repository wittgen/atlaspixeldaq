#include "DefaultClient.h"
#include "WriteRegister.h"

#include <iomanip>

int main(int argc, char** argv) {

    Options opt(argc, argv, "xyz");
    DefaultClient client(opt);

    for(size_t ix = 0; ix < 32; ++ix) {
      WriteRegisterFEI3<Fei3::GlobReg> writeGlobReg;

      writeGlobReg.reg = Fei3::GlobReg::GlobalTDAC;
      writeGlobReg.i_mod = ix;

      for(auto& fe_val: writeGlobReg.values) {
        fe_val = 5;
      } 
      client.run(writeGlobReg);
    }
    //std::cout << std::hex << getTime.result.coarseCount << " " << getTime.result.fineCount << std::endl;
    return 0;

}
