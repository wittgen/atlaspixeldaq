#include "DefaultClient.h"
#include "ReadRodMasterBusyStatus.h"
#include "options.ixx"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

#define BUSY_MACRO(X)		(0x00000001 & X ) ? false : true	// active low logic

//#define MASTER_OUT_STATUS_MACRO((X)			(0x00000001 & X ) ? false : true	// active low logic
#define ROD_BUSY_N_IN_P_0_STATUS_MACRO(X)		(0x00000002 & X ) ? true : false
#define ROD_BUSY_N_IN_P_1_STATUS_MACRO(X)		(0x00000004 & X ) ? true : false
#define EFB_HEADER_PAUSE_I_STATUS_MACRO(X)		(0x00000008 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_0_STATUS_MACRO(X)	(0x00000010 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_1_STATUS_MACRO(X)	(0x00000020 & X ) ? true : false
#define ROL_TEST_PAUSE_I_STATUS_MACRO(X)		(0x00000040 & X ) ? true : false
#define SPARE_CTRL_REG_I_STATUS_MACRO(X)		(0x00000080 & X ) ? true : false


#define FORCE_BUSY_MASK_MACRO(X)		(0x00000001 & X ) ? true : false
#define ROD_BUSY_N_IN_P_0_MASK_MACRO(X)		(0x00000002 & X ) ? true : false
#define ROD_BUSY_N_IN_P_1_MASK_MACRO(X)		(0x00000004 & X ) ? true : false
#define EFB_HEADER_PAUSE_I_MASK_MACRO(X)	(0x00000008 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_0_MASK_MACRO(X)	(0x00000010 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_1_MASK_MACRO(X)	(0x00000020 & X ) ? true : false
#define ROL_TEST_PAUSE_I_MASK_MACRO(X)		(0x00000040 & X ) ? true : false
#define SPARE_CTRL_REG_I_MASK_MACRO(X)		(0x00000080 & X ) ? true : false

#define FORCE_BUSY_FORCE_MACRO(X)			(0x00000100 & X ) ? true : false
#define ROD_BUSY_N_IN_P_0_FORCE_MACRO(X)		(0x00000200 & X ) ? true : false
#define ROD_BUSY_N_IN_P_1_FORCE_MACRO(X)		(0x00000400 & X ) ? true : false
#define EFB_HEADER_PAUSE_I_FORCE_MACRO(X)		(0x00000800 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_0_FORCE_MACRO(X)	(0x00001000 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_1_FORCE_MACRO(X)	(0x00002000 & X ) ? true : false
#define ROL_TEST_PAUSE_I_FORCE_MACRO(X)			(0x00004000 & X ) ? true : false
#define SPARE_CTRL_REG_I_FORCE_MACRO(X)			(0x00008000 & X ) ? true : false

int main(int argc, char** argv){
  
	// intatintiate command
	ReadRodMasterBusyStatus cmd;

	// set command paramters
	cmd.dumpOnPPC = true;

	Options options(argc , argv);
	DefaultClient client(options);
	client.run(cmd);
	
	// print result on host
	
	std::cout<<std::endl<<" Master Busy Current Status Value = "<<HEX(0x000000ff & cmd.result.masterBusyCurrentStatusValue)<<", \n this means: "<<std::endl;
	unsigned int returnval = BUSY_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" MASTER_OUT_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_0_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" ROD_BUSY_N_IN_P_0_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_1_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" ROD_BUSY_N_IN_P_1_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = EFB_HEADER_PAUSE_I_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" EFB_HEADER_PAUSE_I_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_0_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_0_STATUS:\t\t\t"	<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_1_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_1_STATUS:\t\t\t"	<<returnval<<std::endl;
	returnval = ROL_TEST_PAUSE_I_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" ROL_TEST_PAUSE_I_STATUS:\t\t\t"		<<returnval<<std::endl;
	returnval = SPARE_CTRL_REG_I_STATUS_MACRO(cmd.result.masterBusyCurrentStatusValue);
	std::cout<<" SPARE_CTRL_REG_I_STATUS:\t\t\t"		<<returnval<<std::endl<<std::endl;	
	
	
	
	
	std::cout<<" Busy input counters on master: "<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyOutHistValue;
	std::cout<<" BusyOutHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyRodBusyNinP0HistValue;
	std::cout<<" BusyRodBusyNinP0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyRodBusyNinP1HistValue;
	std::cout<<" BusyRodBusyNinP1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyEfbHeaderPauseHistValue;
	std::cout<<" BusyEfbHeaderPauseHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyFormatterMB0HistValue;
	std::cout<<" BusyFormatterMB0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyFormatterMB1HistValue;
	std::cout<<" BusyFormatterMB1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.masterBusyRolTestPauseHistValue;
	std::cout<<" BusyRolTestPauseHist:\t\t\t"<<returnval<<std::endl<<std::endl;

	
	
	
	std::cout<<" Master Busy Mask Value = "<<HEX(0x000000ff & cmd.result.masterBusyMaskValue)<<", this means: "<<std::endl;
	returnval = FORCE_BUSY_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORCE_BUSY_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_0_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROD_BUSY_N_IN_P_0_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_1_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROD_BUSY_N_IN_P_1_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = EFB_HEADER_PAUSE_I_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" EFB_HEADER_PAUSE_I_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_0_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_0_MASK:\t\t\t"	<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_1_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_1_MASK:\t\t\t"	<<returnval<<std::endl;
	returnval = ROL_TEST_PAUSE_I_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROL_TEST_PAUSE_I_MASK:\t\t\t"		<<returnval<<std::endl;
	returnval = SPARE_CTRL_REG_I_MASK_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" SPARE_CTRL_REG_I_MASK:\t\t\t"		<<returnval<<std::endl<<std::endl;



	std::cout<<" Master Busy Force Value = "<<HEX((0x0000ffff & cmd.result.masterBusyMaskValue)>>8)<<", this means: "<<std::endl;
	returnval = FORCE_BUSY_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORCE_BUSY_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_0_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROD_BUSY_N_IN_P_0_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = ROD_BUSY_N_IN_P_1_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROD_BUSY_N_IN_P_1_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = EFB_HEADER_PAUSE_I_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" EFB_HEADER_PAUSE_I_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_0_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_0_FORCE:\t\t\t"	<<returnval<<std::endl;
	returnval = FORMATTER_MB_FEF_N_IN_P_1_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" FORMATTER_MB_FEF_N_IN_P_1_FORCE:\t\t\t"	<<returnval<<std::endl;
	returnval = ROL_TEST_PAUSE_I_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" ROL_TEST_PAUSE_I_FORCE:\t\t\t"		<<returnval<<std::endl;
	returnval = SPARE_CTRL_REG_I_FORCE_MACRO(cmd.result.masterBusyMaskValue);
	std::cout<<" SPARE_CTRL_REG_I_FORCE:\t\t\t"		<<returnval<<std::endl<<std::endl;

	        
	//std::cout<<" Master Busy Current Status Value = "<<HEX(0x000000ff & cmd.result.masterBusyCurrentStatusValue)<<", this means: "<<std::endl;
	if (BUSY_MACRO(cmd.result.masterBusyCurrentStatusValue))
		std::cout<<"\t ROD BUSY !!!"<<std::endl<<std::endl;
	else
		std::cout<<"\t ROD NOT BUSY !!!"<<std::endl<<std::endl;        
        

	
	std::cout << "Done." << std::endl;




}
