/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 28-August-2015
 *
 * Sets up slave to be ready for histogramming
 *
 */

#include "DefaultClient.h"
#include "StopHisto.h"

int main(int argc, char** argv) {
  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  StopHisto cmd;
  unsigned slvId = opt.getSlave(); //  Slave to receive histConfig

  std::cout<<"Slave to be stopped: "<<slvId<<std::endl;

  cmd.whichSlave = 0; //  Default
  if(slvId < 2) cmd.whichSlave = slvId; //  Only one slave at a time allowed now (just the default used if more asked for)
  client.run(cmd);
  
  return 0;
}
