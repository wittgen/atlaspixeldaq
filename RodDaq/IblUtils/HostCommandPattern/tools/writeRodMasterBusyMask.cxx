#include "Client.h"
#include "WriteRodMasterBusyMask.h"
#include "busyOptions.ixx"

#include <iostream>
#include <cstdlib>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define FORCE_BUSY_MASK_MACRO(X)		(0x00000001 & X ) ? true : false
#define ROD_BUSY_N_IN_P_0_MASK_MACRO(X)		(0x00000002 & X ) ? true : false
#define ROD_BUSY_N_IN_P_1_MASK_MACRO(X)		(0x00000004 & X ) ? true : false
#define EFB_HEADER_PAUSE_I_MASK_MACRO(X)	(0x00000008 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_0_MASK_MACRO(X)	(0x00000010 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_1_MASK_MACRO(X)	(0x00000020 & X ) ? true : false
#define ROL_TEST_PAUSE_I_MASK_MACRO(X)		(0x00000040 & X ) ? true : false







unsigned int myMaskValue;

// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	WriteRodMasterBusyMask cmd;

	// set command paramters
	cmd.dumpOnPPC = true;
	

	// set busy mask to write
	cmd.maskValue = myMaskValue;
	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;
	
	std::cout << "Disconnecting from server...";

	// disconnect from server after command returns
	server.disconnectFromServer();

	// print result on host

        std::cout<<"RodMaster Busy Mask written"<<std::endl;
        

	
	std::cout << "Done." << std::endl;

}

//int main(int argc, char** argv) {
//  busyOptions o(argc, argv, "enable");
//  MyClient client(o.getIp());
//  cout << "Force: " << o.getForce() << endl;
//  cout << "Mask: " << o.getMask() << endl;
//  myMaskValue = o.getForce()<<8;		
//  myMaskValue += o.getMask();

//  client.run();
//  return 0;
//}

int main(int argc, char* argv[]) {

        MyClient client(argv[3]);
	myMaskValue = 0x00000000;		

	std::cout << "setting client IP = " << argv[3] << std::endl;

	if(argc == 4){
		string myMaskStringMSB = "";
		string myMaskStringLSB = "";

		myMaskStringMSB = (string)argv[2];		//MSB will be mask value, LSB will be force. Not necessary *bytes*
		myMaskStringLSB = (string)argv[1];
		
		std::stringstream ssMSB;
		ssMSB << std::hex << myMaskStringMSB;
		unsigned int myMaskValueMSB = 0x00000000;
		ssMSB >> myMaskValueMSB;
		
		std::stringstream ssLSB;
		ssLSB << std::hex << myMaskStringLSB;
		unsigned int myMaskValueLSB = 0x00000000;
		ssLSB >> myMaskValueLSB;
		
		myMaskValue = myMaskValueMSB<<8;		
		myMaskValue += myMaskValueLSB;

        	std::cout << "gonna write busy mask to " << myMaskValue << std::endl;
		client.run();
		}
	else{
		std::cout << "Please give three arguments: First in hex format (8 bit each, no *0x*) the busy mask value, then busy force value, and then the IP address of the ROD"<< std::endl;
		std::cout << "No command sent!"<< std::endl;
		}
	return 0;

}


//int main(int argc, char** argv) {
//  Options o(argc, argv, "enable");
//  MyClient client(o.getIp());
//  client.run(o.getChannels());  
// return 0;
//}

