/*
 * AUthor: L. Jeanty <laura.jeanty@cern.ch>
 * Date: 3-Sept-2014
 *
 * This command puts the ROD into data taking mode (physics or calibration)
 * with a number of user-defined parameters
 */

#include "DefaultClient.h"
#include "DataTakingConfig.h"
#include "options.ixx"

#include <cstring>
#include <iostream>

using namespace std;

int main(int argc,  char** argv) {

  Options opt(argc , argv, "dataTakingConfig");

  DataTakingConfig cmd;

  cmd.channels = opt.getChannels();// which channels to configure / enable
  cmd.rodID = 0x120608; // ROD ID
  cmd.nHitsPerTrigger = 2; // number of hits per trigger if using BOC FE emulator
  cmd.fmtReadoutTimeoutLimit = 0x802; // how long the formatter should wait if 1 channel doens't send data
  cmd.frameCount = 1; // number of frames to read/expect per trigger
  cmd.trigLatency = 12; // front end trigger latency
  cmd.bcidOffset = 0; // bcid offset 
  cmd.slinkMode = SlinkEnableFTKXOff; // options are: SlinkIgnoreFTK,SlinkEnableFTKXOff, SlinkEnableFTKLDown, SlinkEnableFTKBoth
  cmd.bocFeEmu = false; // use the boc fe emulator?
  cmd.initRealFE = false; // initialize the real FE?
  cmd.rolEmu = false; // use the ROL emulator?
  cmd.physicsMode = true; // set the ROD into physics mode? (false = calibration mode)
  cmd.doECRReset = true; // reset the ECR counters to 0?
  cmd.fmtHeaderTrailerLimit = 0x802;
  cmd.fmtRodBusyLimit = 0x3c2;
  cmd.pixSpeed = IblBoc::Speed_80; // MCC speed
  cmd.fmtDataOverflowLimit = 0x802;
  
  DefaultClient client(opt);
  client.run(cmd);

  return 0;

}
