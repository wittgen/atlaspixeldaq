// Resets the slave
// if no command line argument given, resets histogrammer and network connection to fit server
// if you specify --network or --histo, it will only reset one of those
//
// by default, will reset both slaves
// can specify only one slave with --slave 0 or 1
// 
// L. Jeanty 20 Aug 2014

#include "DefaultClient.h"
#include "ResetSlave.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv) {
  Options opt(argc, argv);

  ResetSlave cmd;
 
  DefaultClient client(opt);
  cmd.whichSlave = opt.getSlave();

  bool network = true;
  bool histo = true;

 for (int i = 1; i < argc; i++) {
    std::string arg = argv[i];
    if (arg == "--network") { network = true; histo = false; }
    else if (arg == "--histo") { network = false; histo = true; }
 }

  cmd.resetHisto = histo;
  cmd.resetNetwork = network;

  client.run(cmd);

  return 0;
}
