/*
 * * JuanAn Garcia <jgarciap@cern.ch>
 * Date: October-2018
 *
 */

#include <iomanip>
#include <fstream>

#include "DefaultClient.h"
#include "GetGADCOutput.h"
#include <string>

#include "TFile.h"
#include "TH2F.h"
#include "TGraph.h"

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
		        //std::string helpGADC = "GADC Select Options : ";
		        //for(int i=0;i<GADC_SELECT_CODES; i++){helpGADC += std::to_string(i) + GADCSelectNames[i];}

			std::string select = "GADCSelect options: \n";
			for (int i = 0;i<8;i++){
			select += std::to_string(i)+ " --> "+GADCSelectNames[i] + " \n";
			}
			desc->add_options()
				("GADCSelect", po::value<int>(&GADCSel)->default_value(0), select.c_str());
			desc->add_options()
				("GADCVref", po::value<int>(&GADCVref)->default_value(-1), "GADCVref from 0-255");

		}

		int getGADCSel(){return GADCSel;}
		int getGADCVref(){return GADCVref;}

	protected:

		int GADCSel;
		int GADCVref;

	private:

};

int main(int argc, char** argv) {

  LocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  if(opt.getGADCSel() <0 || opt.getGADCSel() >7){
  std::cout<<"GADC select out of range, from 0 to 7 "<<std::endl;
  exit(0);
  }

  std::cout<<"Starting GADC "<<GADCSelectNames[opt.getGADCSel()]<<std::endl;

  
  std::time_t time_result  = std::time(nullptr);
  struct tm tt = *(std::localtime(&time_result));
  std::cout<<"Execution Time: "<<std::asctime(std::localtime(&time_result))<<std::endl;
  
  GetGADCOutput cmd;
  cmd.m_rxMask = opt.getChannels();
  cmd.m_GADCSel = (GetGADCOutput::GADCSelect)opt.getGADCSel();
  cmd.m_GADCVref = opt.getGADCVref();

  client.run(cmd);

  std::string rodName = opt.getRodName();
  if(rodName.empty())rodName = opt.getHostName();
  if(rodName.empty())rodName = opt.getIp();

  std::cout<<"Using RODName "<<rodName<<std::endl;

    std::string formattedtime = std::to_string(tt.tm_year+1900)+"_"+std::to_string(tt.tm_mon+1)+"_"+std::to_string(tt.tm_mday)+"_"+std::to_string(tt.tm_hour)+"_"+std::to_string(tt.tm_min)+"_"+ std::to_string(tt.tm_sec);
    //TFile outfile((filename+"_"+std::to_string(time_result)+".root").c_str(),"RECREATE");

    std::string filename = "GADC_"+GADCSelectNames[cmd.m_GADCSel]+"_Vref_"+std::to_string(cmd.m_GADCVref)+"_"+rodName +"_"+formattedtime+".root";
    TFile outfile(filename.c_str(),"RECREATE");

  for (std::map<uint8_t,std::vector<std::vector<uint32_t> > >::iterator it = cmd.result.GADC.begin(); it != cmd.result.GADC.end(); ++it){

  std::cout<<"Rx "<<(int)it->first<<" Size "<<(int)it->second.size()<<std::endl;

   TGraph gr;
    std::string grName = "RX_"+std::to_string(it->first);
    gr.SetName(grName.c_str());
    gr.SetTitle(grName.c_str());
    for(uint16_t i=0;i<it->second.size();i++){
    //std::cout<<"Size "<<(int)it->second.size()<<" nIter "<<(int )it->second[i].size()<<std::endl;
     for(uint32_t j=0;j<it->second[i].size();j++){
     gr.SetPoint(j,j,it->second[i][j]);
     std::cout<<i<<" "<<j<<" "<<it->second[i][j]<<std::endl;
     }
     outfile.cd();
     gr.Write();
    }
  }

  outfile.Close();
  
  return 0;
}


