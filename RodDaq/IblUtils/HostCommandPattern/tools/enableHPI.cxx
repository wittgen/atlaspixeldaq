#include <iomanip>

#include "DefaultClient.h"
#include "EnableHPI.h"
#include <string>
#include "GeneralResults.h"

class EnableHPILocalOptions: public Options {

	private:
		bool enableHPIbit;
	public:

		EnableHPILocalOptions(int c, char* v[], std::string t = ""):
		Options(c, v, t, true) { // Initializing Options from derived class
			   // register_options already callled, so are the extend_options from intermediate inheritance
			   this->extend_options();
			   this->init(c,v);
		}
		virtual void extend_options() {
			   desc->add_options()
					           ("enableHPI", po::value<bool>(&enableHPIbit)->default_value(false), "Enable or Disable HPI bit in PPC_CTRL_REG");
		}

		uint32_t getEnableHPI() { return enableHPIbit; }

	protected:
};



int main(int argc, char** argv) {

  EnableHPILocalOptions opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  bool enableHPIbit = opt.getEnableHPI();

  //Call the instance of your command
  EnableHPI cmd;

  //Set the enableHPI flag default
  cmd.enableHPIbit = enableHPIbit;
  
  //  Send cmd from host to PPC and retrieve cmd.result back from it
  std::cout << "Sending command..." << std::endl;  
  client.run(cmd);
  
  std::cout << "Command executed" << std::endl;
  
  //  Look at result output generated here and make sure ratios printed are consistent with what PPC reports
  std::cout << "PPC_CTRL_REG: 0x" << std::hex << (int) cmd.result.PPCctrlRegValue<<std::endl;
  
  return 0;
}
