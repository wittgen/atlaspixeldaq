/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 7-December-2015
 *
 * Tests data path from MCC to histogrammer
 *
 */

#include "DefaultClient.h"
#include "TestRxHisto.h"

int main(int argc, char** argv) {
  Options opt(argc, argv, "testRxHisto");
  DefaultClient client(opt);

  TestRxHisto scanCmd;
  uint32_t slvId = opt.getSlave(); //  Slave to run scan on
  scanCmd.histoUnit = opt.getHistoUnit(); //  Histo Unit to run scan on
  uint32_t readoutType = opt.getReadoutType(); //  Histogrammer readout format

  scanCmd.whichSlave=0;
  int stopSlave = 2;
  if (slvId == 1) scanCmd.whichSlave = 1;
  else if (slvId == 0) stopSlave = 1;

  scanCmd.modMask = opt.getModMask();
  scanCmd.nTrig = opt.getNTrig();
  scanCmd.mode = opt.getMode();
  scanCmd.filter = opt.getFiltOn();
  scanCmd.nMasks = opt.getNMasks();

  for (scanCmd.whichSlave; scanCmd.whichSlave < stopSlave; scanCmd.whichSlave++) {
    uint32_t nChips = 128; //  Always looks for all possible chips on all possible modules (up to 7 of the latter = 112 chips)
    uint32_t maskStepAssumed = 3;
    if(nChips > 8) maskStepAssumed = 5;
    for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
      scanCmd.histConfig_in.cfg[slvUnit].nChips = nChips;
      scanCmd.histConfig_in.cfg[slvUnit].enable = 0; //  To be enabled for needed unit(s) on PPC via TestRxHisto code
      scanCmd.histConfig_in.cfg[slvUnit].type = readoutType;
      scanCmd.histConfig_in.cfg[slvUnit].maskStep = maskStepAssumed;
      scanCmd.histConfig_in.cfg[slvUnit].mStepEven = 0;
      scanCmd.histConfig_in.cfg[slvUnit].mStepOdd = 0;
      scanCmd.histConfig_in.cfg[slvUnit].chipSel = 1;
      scanCmd.histConfig_in.cfg[slvUnit].expectedOccValue = 0;
      scanCmd.histConfig_in.cfg[slvUnit].addrRange = scanCmd.histConfig_in.cfg[slvUnit].chipSel;
      scanCmd.histConfig_in.cfg[slvUnit].scanId = 29;
      scanCmd.histConfig_in.cfg[slvUnit].binId = 0;
    }
    std::cout << std::endl << "Histgramming units  on slave " << scanCmd.whichSlave << " to be configured as follows:" << std::endl << std::endl;
    std::cout << "  nChips =           " << scanCmd.histConfig_in.cfg[0].nChips << std::endl;
    std::cout << "  enable =           " << scanCmd.histConfig_in.cfg[0].enable << std::endl;
    std::cout << "  type =             " << scanCmd.histConfig_in.cfg[0].type << std::endl;
    std::cout << "  maskStep =         " << scanCmd.histConfig_in.cfg[0].maskStep << std::endl;
    std::cout << "  mStepEven =        " << scanCmd.histConfig_in.cfg[0].mStepEven << std::endl;
    std::cout << "  mStepOdd =         " << scanCmd.histConfig_in.cfg[0].mStepOdd << std::endl;
    std::cout << "  chipSel =          " << scanCmd.histConfig_in.cfg[0].chipSel << std::endl;
    std::cout << "  expectedOccValue = " << scanCmd.histConfig_in.cfg[0].expectedOccValue << std::endl;
    std::cout << "  addrRange =        " << scanCmd.histConfig_in.cfg[0].addrRange << std::endl;
    std::cout << "  scanId =           " << scanCmd.histConfig_in.cfg[0].scanId << std::endl;
    std::cout << "  binId =            " << scanCmd.histConfig_in.cfg[0].binId << std::endl;
    std::cout << std::endl;

    client.run(scanCmd);
  }

  return 0;
}
