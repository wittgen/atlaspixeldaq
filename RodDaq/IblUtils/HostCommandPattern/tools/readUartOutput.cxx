#include "DefaultClient.h"
#include "ReadUartOutput.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public DefaultClient {
public:
	MyClient(Options& opt);
	void run();
};

MyClient::MyClient(Options& opt): DefaultClient(opt) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  

	// intatintiate command
	ReadUartOutput cmd;
	// set command paramters
	cmd.dumpOnPPC = true;

	DefaultClient::run(cmd);

	// print result on host
	
	for(size_t i = 0 ; i < sizeof(cmd.result.master_output) ; i++)
		std::cout << cmd.result.master_output[i];
	std::cout << std::endl;

	for(size_t i = 0 ; i < sizeof(cmd.result.slvA_output) ; i++)
		std::cout << cmd.result.slvA_output[i];
	std::cout << std::endl;

	for(size_t i = 0 ; i < sizeof(cmd.result.slvB_output) ; i++)
		std::cout << cmd.result.slvB_output[i];
	std::cout << std::endl;

	std::cout << "Done." << std::endl;

}

int main(int argc, char** argv) {

	Options opt(argc , argv);

	MyClient client(opt);
	client.run();

	return 0;

}
