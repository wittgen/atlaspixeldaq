#include "DefaultClient.h"
#include "SetNetwork.h"

#include <iostream>
#include <cstdio>

static void ipStringToOctets(std::string ip, uint8_t &oct1, uint8_t &oct2, uint8_t &oct3, uint8_t &oct4) {
    // using ints, as scanf doesn't have a uint8 specifier
    int o1, o2, o3, o4;
    o1 = oct1; o2 = oct2; o3 = oct3; o4 = oct4;
    sscanf(ip.c_str(), "%d.%d.%d.%d", &o1, &o2, &o3, &o4);
    oct1 = o1; oct2 = o2; oct3 = o3; oct4 = o4;
}

int main(int argc, char** argv) {
  Options opt(argc, argv, "setNetwork");
  SetNetwork net;

  /* Get settings from command line options. */
  int slaveid = opt.getSlave();
  if (slaveid != 0 && slaveid != 1) {
    slaveid = 0;
  }
  net.slaveid = slaveid;

  net.usePpcDefaults = opt.getDefault();

  ipStringToOctets(opt.getSIp(), net.ip_1, net.ip_2, net.ip_3, net.ip_4); 
  ipStringToOctets(opt.getSNetmask(), net.subnet_1, net.subnet_2, net.subnet_3, net.subnet_4);
  ipStringToOctets(opt.getSGw(), net.gw_1, net.gw_2, net.gw_3, net.gw_4); 
  ipStringToOctets(opt.getFitFarmIp1(), net.ipA_1, net.ipA_2, net.ipA_3, net.ipA_4);
  ipStringToOctets(opt.getFitFarmIp2(), net.ipB_1, net.ipB_2, net.ipB_3, net.ipB_4);

  net.portA = opt.getFitFarmPort1();
  net.portB = opt.getFitFarmPort2();

  DefaultClient client(opt);
  std::cout << "Setting network on slave " << slaveid << std::endl;
  client.run(net);
  return 0;
}
