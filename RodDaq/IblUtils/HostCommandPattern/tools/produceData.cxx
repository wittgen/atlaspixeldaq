/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 12 Aug 2014
 * Description: this command will inject a small number of pixels with a calibration pulse and then trigger with a user defined pause
 * If the noise parameter is true, then rather than injecting the chip will only send noise hits
 * For fastest data taking, use the noise option
 * 
 */

#include "ProduceData.h"
#include "DefaultClient.h"
#include <iostream>

int main(int argc, char** argv){

  ProduceData cmd;
  
  Options options(argc , argv);

  // use these two options for cal pulse tests
  cmd.sleep = 100; // sleep in usecs bewteen triggers
  cmd.trigDelay = 244; // delay between injection and trigger


  cmd.channels = options.getChannels(); // channels to send data
  cmd.nTriggers = 10; // number of triggers to execute
  cmd.trigCount = 8; // number of frames per trigger
  cmd.trigLatency = 195; // trigger latency
  cmd.maskSteps = 8; // Number of mask steps in mask (options are 1, 2, 4, 8, 16, 32, 672) 
  cmd.dcMode = 0; // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col 
  cmd.rawToT = 5; // ToT for digital injections
  cmd.noise = false; // instead of digital injection only pick up noies hits  
  cmd.debug = false; // print out onto kermit?
  cmd.debugRx = 18; // if debugging, which rx

  DefaultClient client(options);
  client.run(cmd);
  
  return 0;
}
