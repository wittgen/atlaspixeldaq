/*
 * AUthor: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-V-05
 *
 */

#include "Client.h"
#include "IblDataTakingWithRoLEmulator.h"
#include "options.ixx"

#include <cstring>
#include <iostream>

using namespace std;

class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {
}

void MyClient::run() {

	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		return;
	}
	cout << "Connected to serverr" << endl;

	IblDataTakingWithRoLEmulator cmd;
	server.sendCommand(cmd);
	server.disconnectFromServer();
	std::cout << "Ready for triggers!" << std::endl;
}

int main(int argc, char** argv) {
  Options o(argc, argv, "enable");
  MyClient client(o.getIp());
  client.run();
  return 0;
}
