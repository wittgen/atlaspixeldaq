/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 7-October-2015
 *
 * Sets up slave to be ready for histogramming
 *
 */

#include "DefaultClient.h"
#include "DummyScan.h"

int main(int argc, char** argv) {
  Options opt(argc, argv, "dummyScan"); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  uint32_t nChips = opt.getNumOfChips();
  uint32_t slvId = opt.getSlave(); //  Slave to run scan on
  uint32_t readoutType = opt.getReadoutType(); //  Histogrammer readout format
  uint32_t expOcc = opt.getExpOcc(); //  Expected Occupancy for SHOTR_TOT readout type


  DummyScan scanCmd;
  scanCmd.whichSlave=0; //  Default
  if(slvId < 2) scanCmd.whichSlave = slvId; //  Only one slave at a time allowed now (just the default used if more asked for)

  uint32_t maskStepAssumed = 3;
  if(opt.getNumOfChips() > 8) maskStepAssumed = 5;
  for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
    scanCmd.histConfig_in.cfg[slvUnit].nChips = nChips;
    scanCmd.histConfig_in.cfg[slvUnit].enable = ((slvUnit == 0) ? 1 : 0); //  Only one unit works now in FitServer emulator mode
    scanCmd.histConfig_in.cfg[slvUnit].type = readoutType;
    scanCmd.histConfig_in.cfg[slvUnit].maskStep = maskStepAssumed;
    scanCmd.histConfig_in.cfg[slvUnit].mStepEven = 0;
    scanCmd.histConfig_in.cfg[slvUnit].mStepOdd = 0;
    scanCmd.histConfig_in.cfg[slvUnit].chipSel = 1;
    scanCmd.histConfig_in.cfg[slvUnit].expectedOccValue = expOcc;
    scanCmd.histConfig_in.cfg[slvUnit].addrRange = scanCmd.histConfig_in.cfg[slvUnit].chipSel;
    scanCmd.histConfig_in.cfg[slvUnit].scanId = 29;
    scanCmd.histConfig_in.cfg[slvUnit].binId = 0;
  }

  scanCmd.histoUnit=0; //  Note:  Just Unit 0 hardcoded here for now.
  scanCmd.inj_Type=opt.getInjType();
  scanCmd.multiHit=opt.getMultiHit();
  scanCmd.scanType=opt.getScanType();
  client.run(scanCmd);

  return 0;
}
