
// Russell Smith
// June 2 2014
#include "DefaultClient.h"
#include "ReadConfig.h"

#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "readConfig");

  //  std::vector<ReadConfig> readCfg(1);//vector to read since we can't send everything at once

  DefaultClient client(opt);

  std::cout<<"Read back configs for mask: 0x"<<std::hex<<(int)opt.getChannels()<<std::dec<<std::endl;

  for (int i = 0; i < 32; ++i) {
    std::cout<< i  << "*************************"<<std::endl;
    if( opt.getChannels()  & (1<<i) ){
       std::cout << __PRETTY_FUNCTION__ <<" at line : " << __LINE__ << std::endl;
       ReadConfig readCfg;
       readCfg.readCalibConfig();
       readCfg.setReadFrontEnd(false);
       readCfg.setFeToRead(i);
       client.run(readCfg);
       std::cout<<"For FE "<<i<<", the config is: "<<std::endl;
       readCfg.result.moduleConfig.dump();
    }
  }
  std::cout << "returning" <<std::endl;
  return 0;
}
