/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 28-August-2015
 *
 * Sets up slave to be ready for histogramming
 *
 */

#include "DefaultClient.h"
#include "StartHisto.h"

int main(int argc, char** argv) {
  Options opt(argc, argv); //  Parse the standard command line arguments like IP of PPC and slave number
  DefaultClient client(opt);

  StartHisto cmd;
  unsigned slvId = opt.getSlave(); //  Slave to receive histConfig

  cmd.whichSlave = 0; //  Default
  if(slvId < 2) cmd.whichSlave = slvId; //  Only one slave at a time allowed now (just the default used if more asked for)
  client.run(cmd);

    std::cout << "Histgramming on slave " << cmd.whichSlave << " started as follows:" << std::endl << std::endl;
    for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
      std::cout << "Configuration read back for Unit " << slvUnit << std::endl;

      std::cout << "  nChips =           " << cmd.result.histConfig.cfg[slvUnit].nChips << std::endl;
      std::cout << "  enable =           " << cmd.result.histConfig.cfg[slvUnit].enable << std::endl;
      std::cout << "  maskStep =         " << cmd.result.histConfig.cfg[slvUnit].maskStep << std::endl;
      std::cout << "  type =             " << cmd.result.histConfig.cfg[slvUnit].type << std::endl;
      std::cout << "  mStepEven =        " << cmd.result.histConfig.cfg[slvUnit].mStepEven << std::endl;
      std::cout << "  mStepOdd =         " << cmd.result.histConfig.cfg[slvUnit].mStepOdd << std::endl;
      std::cout << "  chipSel =          " << cmd.result.histConfig.cfg[slvUnit].chipSel << std::endl;
      std::cout << "  expectedOccValue = " << cmd.result.histConfig.cfg[slvUnit].expectedOccValue << std::endl;
      std::cout << "  addrRange =        " << cmd.result.histConfig.cfg[slvUnit].addrRange << std::endl;
      std::cout << "  scanId =           " << cmd.result.histConfig.cfg[slvUnit].scanId << std::endl;
      std::cout << "  binId =            " << cmd.result.histConfig.cfg[slvUnit].binId << std::endl;
    }

  return 0;
}
