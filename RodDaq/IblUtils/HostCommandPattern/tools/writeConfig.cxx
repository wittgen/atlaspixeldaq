// Russell Smith
// June 2014
#include "DefaultClient.h"
#include "Fei4Cfg.h"
#include "WriteConfig.h"
#include "IblConnectivity.h"
#include <iostream>

// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {
  //0) 
  Options opt(argc, argv, "writeConfig");

  DefaultClient client(opt);

  for(uint32_t i = 0 ; i < 32; ++i ){
  //1)
    WriteConfig<Fei4Cfg> writeCfg;

  //2)
    writeCfg.setCalibConfig();
    writeCfg.setFeToWrite(i);

    Fei4Cfg cfg;
    cfg.setRxCh(i); cfg.setTxCh(IblConnectivity::getTxChFromRxCh(i));
    cfg.ErrorMask_0.write(0xABCD);//for example

    if(opt.getChannels() & (0x1<<i)){

       cfg.ErrorMask_0.write(0xABCD);//for example
       cfg.TDAC(0).setPixel(0,1 , i);
       cfg.FDAC(0).setPixel(0,1 , i/2 + 1);
       std::cout<<"about to write config for rx: "<<i<<"with the following values: "<<std::endl;
       cfg.dump(); 
       
       writeCfg.moduleConfig = cfg;
       client.run(writeCfg);

    }
  }

  return 0;
}
