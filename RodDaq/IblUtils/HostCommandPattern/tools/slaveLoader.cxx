#include "SlaveLoader.h"
#include <iostream>
#include "DefaultClient.h"

#include <unistd.h>
#include <sys/stat.h>

#include <exception>

class BaseException : std::exception {
public:
  virtual void what(std::ostream& os) const = 0;
};

std::ostream& operator<<(std::ostream& os, const BaseException& e) {
  e.what(os);
  return os;
}

class NoImageFile : BaseException {
public:
  NoImageFile(std::string descriptor, std::string fileName):
	m_descriptor(descriptor), m_fileName(fileName) { }
  virtual ~NoImageFile() throw () { }
  virtual void what(std::ostream& os) const {
	os << "NoImageFile exception. File: " << m_fileName << "; Description: " << m_descriptor;
  }
private:
  std::string m_descriptor;
  std::string m_fileName;
};

class RodException : BaseException {
public:
  RodException(std::string descriptor): m_descriptor(descriptor) { }
  virtual ~RodException() throw () { } 
  virtual void what(std::ostream& os) const {
	os << "RodException thrown: " << m_descriptor;
  }
private:
  std::string m_descriptor;
};

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv){

  SlaveLoader cmd;
  cmd.dumpOnPPC = true;
  cmd.slaveSW_bin_size = 0;

  Options options(argc , argv);
  DefaultClient client(options);

  FILE *fp;
  try {
    std::string fileName;
    if(getenv("ROD_DAQ"))
  	fileName=std::string(getenv("ROD_DAQ"))+"/IblDaq/RodSlave/Software/bin/histClient.bin";
    for(int i = 0 ; i < argc ; ++i)
	if(strcmp(argv[i], "-f")==0&&(i+1)<argc) fileName=argv[i+1];
    std::cout << "File name: " << fileName << std::endl;

    if (fileName.empty()) {
  	  throw NoImageFile("Missing file name", fileName);
  	  return -1; /* no such file */
    }
  
    if((fp = fopen(fileName.c_str(), "r")) == NULL) {
  	  throw NoImageFile("Could not open image", fileName);
  	  return -1;
    }
  
    struct stat fileStat;
    fstat(fileno(fp), &fileStat);
    cmd.slaveSW_bin_size = fileStat.st_size;
    std::cout << "File size: " << cmd.slaveSW_bin_size << " bytes" << std::endl;
    if (!cmd.slaveSW_bin_size) {
  	  throw NoImageFile("No data in file", fileName);
  	  fclose(fp);
  	  return -1; /* bad file */
    }
  
    if(cmd.slaveSW_bin_size > sizeof(cmd.slaveSW_data)) {
  	std::cerr << "ERROR: file is too large for command. Can only fit " << sizeof(cmd.slaveSW_data) << " bytes, not " << cmd.slaveSW_bin_size << std::endl;
  	return -1;
    }
  
    if (1 != fread(cmd.slaveSW_data, cmd.slaveSW_bin_size, 1, fp)){
  	  throw RodException("Failed to read slave binary");
    }

    std::cout << "Read file!" << std::endl;

  } catch(BaseException& e) {
     std::cout << e << std::endl;
  }

  fclose(fp);
  client.run(cmd);

  // print result on host

  std::cout << cmd.result.status << std::endl;

  return 0;
}
