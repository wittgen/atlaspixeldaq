/*
 * halfClockReturn
 * By: Laser
 * Executable for HalfClockReturn
 */

#include "DefaultClient.h"
#include "HalfClockReturn.h"

#include <iostream>
#include <cstring>

int main(int argc, char **argv) {
  Options opt(argc, argv);

  HalfClockReturn hcr;

  hcr.setModMask(opt.getChannels());

  DefaultClient client(opt);
  client.run(hcr);

  std::cout << "Half clock set for " << (int) hcr.result.nMod << " modules" << std::endl;
  for (int i = 0; i < hcr.result.nMod; i++) {
    std::cout << "For MCC " << i << " the data is " << hcr.result.regs[i] << std::endl;
  }

  return 0;
}
