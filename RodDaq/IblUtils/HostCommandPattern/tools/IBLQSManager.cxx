/* this script can be use to define user actions for QS */

#include "DefaultClient.h"
#include "QSTalkTask.h"
#include <iostream>
#include <string>
#include <map>
#include <boost/algorithm/string/case_conv.hpp>

class LocalOptions: public Options {

  public:

    LocalOptions(int c, char* v[], std::string t = ""):
      Options(c, v, t, true) { // Initializing Options from derived class
      // register_options already callled, so are the extend_options from intermediate inheritance
      this->extend_options();
      this->init(c,v);
    }
    virtual void extend_options() {
      // Extending options (but only for this command)
      desc->add_options()
        ("action", po::value<std::string>(&whichAction)->default_value(""), "Set IBL QS action");

      // Overriding value for option
      override_option_default<std::string>("ch", "0x00000000");
      override_option_default<std::string>("value", "");
    }

    std::string whichAction;
  
  std::string getValueStr(){return value_str;}
  protected:

  private:

};

// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
  public:
    MyClient(LocalOptions opt);
    virtual void run();
    void printOutActions();
  private:
    LocalOptions opt;
  // Supported actions
  // Todo: use dynamic container !!!
  const std::vector<std::string> mode = {"QS_INFO", "QS_OFF" ,"QS_IDLE","QS_MON","QS_RST","QS_RST_DIS","QS_RST_CFG_ON","QS_RST_CFG_OFF"}; // the ordering here has to be the same as for the enum of the QS Modes
  const std::vector<std::string> user = {"QS_SINGLE_RST", "QS_SINGLE_CFG","QS_SINGLE_ENA","QS_SINGLE_DIS","QS_SINGLE_RST_ALL", "RESAMPLING", "RECONFIGURE", "RESET_USER_ACTION", "ENABLE", "DISABLE", "EXPERT_RECONFIGURE_ON", "EXPERT_RECONFIGURE_OFF", "ENABLEBOC", "DISABLEBOC"};
  const std::vector<std::string> qsPar = {"N_FC50_MAX","N_BU_MAX","N_TO_MAX","N_RST_MAX","N_TO_CFG_MAX","N_BU_CFG_MAX", "VERBOSITY", "SPYMODE", "SLEEPDELAY", "WRITE_REG_AT_ECR"};
  const std::vector<std::string> syncModPar = {"MODsyncErrThresh", "MODsyncISReadDelay", "MODsyncMaxTrials", "MODsyncMinIdle"};
  const std::vector<std::string> syncRodPar = {"RODsyncErrThresh", "RODsyncISReadDelay", "RODsyncMaxTrials", "RODsyncMinIdle"};
};

void MyClient::printOutActions(){
  std::cout << "==== To change QS mode ====" <<std::endl;
  for(size_t idx=0;idx<mode.size();idx++)
    std::cout << "\t" << mode[idx] <<std::endl;
  std::cout << "==== To execute QS action ====" <<std::endl;
  for(size_t idx=0;idx<user.size();idx++)
    std::cout << "\t" << user[idx] <<std::endl;
  std::cout << "==== To adjust QS parameters ====" <<std::endl;
  for(size_t idx=0;idx<qsPar.size();idx++)
    std::cout << "\t" << qsPar[idx] <<std::endl;
}

MyClient::MyClient(LocalOptions opt_arg)
 : Client(opt_arg.getIp().c_str()), opt(opt_arg)
 { }

void MyClient::run()
{
  // connect to ppc
  if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
                std::cout << "no connection to server" << std::endl;
		std::cout << "Please provide a valid ROD Name through \"--rodName\"." << std::endl;
                return;
        }
        std::cout << "Connected to server." << std::endl;

	const std::string qsAction=boost::algorithm::to_upper_copy<std::string>(opt.whichAction); 

  if(qsAction==""){
    std::cout<<"ERROR: Please provide one and only one action from the following list through \"--action\" option"<<std::endl;
    printOutActions();
    return;
  }
  int actionID=-1;
  if(std::find(mode.begin(), mode.end(), qsAction) != mode.end()) actionID=1;
  else if(std::find(user.begin(), user.end(), qsAction) != user.end()) actionID=2;
  else if(std::find(qsPar.begin(), qsPar.end(), qsAction) != qsPar.end()) actionID=3;
  // These features are not implemented yet
  // else if(Module auto sync) actionID=4;
  // else if(Change auto sync parameters) actionID=5;
  // else if(ROD auto sync) actionID=6;
  // else if(ROD auto sync parameters) actionID=7;
  else if(qsAction=="SHOWSTATUS") actionID=8;
  else{
    std::cout << "ERROR: Unknown action \"" << qsAction << "\". Please choose from the following" << std::endl;
    printOutActions();
    return;
  }
  // initialize QS
  QSTalkTask qsTalkTaskCmd;

  unsigned int input;
  std::string line;
  int value;
  uint32_t mask = 0;

  switch(actionID) {
    case 1:
      std::cout << "You want to change the QS Mode" << std::endl;
      input=std::find(mode.begin(), mode.end(), qsAction)-mode.begin();
      std::cout << "QSMode is changed to " << mode[input] << std::endl;
      // change QS Mode
      qsTalkTaskCmd.quickStatusMode = input;
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_MODE; 
      server.sendCommand(qsTalkTaskCmd);
      break;
 
    case 2:
      if( !opt.getChannels() ) {
        std::cout << "Enter a module mask (in hex): " << std::endl;
        std::cin >> std::hex >> mask;
      } else mask = opt.getChannels();
      std::cout << "Action " << qsAction << " will be used with module mask 0x" << std::hex << mask 
		<< std::dec << std::endl;
      // performe this specific action
      if (qsAction == "RESET_USER_ACTION") {
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_RESETUSERACTION;	
      }
      else {
        qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_USERACTION;
        qsTalkTaskCmd.module_mask = mask;
        if (qsAction == "RESAMPLING")
          qsTalkTaskCmd.action = QuickStatus::MON_RESAMPLE_PHASE;
        else if (qsAction == "RECONFIGURE" || qsAction == "QS_SINGLE_CFG")
	  qsTalkTaskCmd.action = QuickStatus::MON_RECONFIGURE;
        else if (qsAction == "ENABLE" || qsAction == "QS_SINGLE_ENA")
	  qsTalkTaskCmd.action = QuickStatus::MON_ENABLE;
        else if (qsAction == "DISABLE" || qsAction == "QS_SINGLE_DIS")
	  qsTalkTaskCmd.action = QuickStatus::MON_DISABLE;
        else if (qsAction == "EXPERT_RECONFIGURE_ON")
	  qsTalkTaskCmd.action = QuickStatus::MON_EXPERT_RECONFIGURE_ON;
        else if (qsAction == "EXPERT_RECONFIGURE_OFF")
	  qsTalkTaskCmd.action = QuickStatus::MON_EXPERT_RECONFIGURE_OFF;
        else if (qsAction == "ENABLEBOC")
	  qsTalkTaskCmd.action = QuickStatus::MON_ENABLE_BOC;
        else if (qsAction == "DISABLEBOC")
	  qsTalkTaskCmd.action = QuickStatus::MON_DISABLE_BOC;
        else
	  std::cout << "this action is not supported for IBL so far" << std::endl;
      }
      server.sendCommand(qsTalkTaskCmd);
      break;

    case 3:
      std::cout << "You want to change the QS parameters" << std::endl;
      if(opt.getValueStr()==""){
	std::cout << "Enter the new value for "<<qsAction<<": " << std::endl;
	value = 0;
	std::cin >> value;
      }
      else value=atoi(opt.getValueStr().c_str());
      std::cout << "Parameter " << qsAction << " is set to " << value << std::endl;
      // change the QS parameters
      // "N_FC50_MAX","N_BU_MAX","N_TO_MAX","N_RST_MAX","N_TO_CFG_MAX","N_BU_CFG_MAX", "N_RESAMPLING_THRESHOLD", "N_RESAMPLING_MAX", "VERBOSITY", "SPYMODE", "SLEEPDELAY"
      if(qsAction=="N_FC50_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_FC;
	qsTalkTaskCmd.fc_max = value;
      }
      else if(qsAction=="N_BU_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_BU;
	qsTalkTaskCmd.bu_max = value;
      }
      else if(qsAction=="N_TO_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_TO;
	qsTalkTaskCmd.to_max = value;
      }
      else if(qsAction=="N_RST_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_RST;
	qsTalkTaskCmd.rst_max = value;
      }
      else if(qsAction=="N_TO_CFG_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_TO_CFG;
	qsTalkTaskCmd.cfg_max = value;
      }
      else if(qsAction=="N_BU_CFG_MAX"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_BU_CFG;
	qsTalkTaskCmd.cfg_max = value;
      }
      else if(qsAction=="VERBOSITY"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_VERBOSITY;
	qsTalkTaskCmd.fc_max = value;
      }
      else if(qsAction=="SPYMODE"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_SPY_MODE;
	qsTalkTaskCmd.fc_max = value;
      }
      else if(qsAction=="SLEEPDELAY"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_SLEEP_DELAY;
	qsTalkTaskCmd.fc_max = value;
      }
      else if(qsAction=="WRITE_REG_AT_ECR"){
	qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_WRITE_REG_AT_ECR;
	qsTalkTaskCmd.fc_max = value;
      }
      else std::cout << "no valid parameter is chosen" << std::endl;

      server.sendCommand(qsTalkTaskCmd);

      break;

    case 4:
      std::cout << "You want to enable or disable the Module auto-sync" << std::endl;
      std::cout << "Enter the new value (ON/OFF): " << std::endl;
      std::getline(std::cin, line);
      if (line=="ON" || line=="OFF") {
        std::cout << "Module auto-sync is set to: " << line << std::endl;
	// set it
        std::cout << "This is not supported for IBL, so it will do nothing!" << std::endl;
      }
      else {
        std::cout << "no correct value is chosen, nothing will be changed!" << std::endl;
      }
      break;

    case 5:
      std::cout << "You want to change the Module auto-sync parameters" << std::endl;
      for (int i=0; i<4; i++)
        std::cout << i << ") " << syncModPar[i] << std::endl;
      std::cin >> input;
      std::cout << "Enter new value: " << std::endl;
      std::getline(std::cin, line);
      std::cout << "Change " << syncModPar[input] << " to " << line << std::endl;
      // how to do this? add helpful tips what value can be given?
      std::cout << "This is not supported for IBL, so it will do nothing!" << std::endl;
      break;

    case 6:
      std::cout << "You want to enable or disable the ROD auto-sync" << std::endl;
      std::cout << "Enter the new value (ON/OFF): " << std::endl;
      std::getline(std::cin, line);
      if (line=="ON" || line=="OFF") {
        std::cout << "ROD auto-sync is set to: " << line << std::endl;
	// set it
        std::cout << "This is not supported for IBL, so it will do nothing!" << std::endl;
      }
      else {
        std::cout << "no correct value is chosen, nothing will be changed!" << std::endl;
      }
      break;

    case 7:
      std::cout << "You want to change the ROD auto-sync parameters" << std::endl;
      for (int i=0; i<4; i++)
        std::cout << i << ") " << syncRodPar[i] << std::endl;
      std::cin >> input;
      std::cout << "Enter new value: " << std::endl;
      std::getline(std::cin, line);
      std::cout << "Change " << syncRodPar[input] << " to " << line << std::endl;
      // how to do this? add helpful tips what value can be given?
      std::cout << "This is not supported for IBL, so it will do nothing!" << std::endl;
      break;
    case 8:
      qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_GET_STATUS;
      server.sendCommand(qsTalkTaskCmd);
      break;
    default:
      std::cout << "You choose a wrong input number, please start again..." << std::endl;
  }
  std::cout << "Disconnecting from server..."<<std::endl;
  // disconnect from server after command returns
  server.disconnectFromServer();
  std::cout << "Done." << std::endl;
}

int main(int argc, char** argv) {

  LocalOptions o(argc, argv, "enable");
  std::cout<< "options read"<<std::endl;
  MyClient client(o);
  client.run();
  return 0;
}


