#include "KillCommand.h"
#include "DefaultClient.h"

#include <vector>
#include <string>
#include <iostream>
#include <unistd.h>
#include <stdlib.h>

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv){
  if (argc < 3) {
    std::cout << "Usage: killCommand typeId execId" << std::endl;
    return 0;
  }

  uint16_t typeId = atoi(argv[1]);
  uint16_t execId = atoi(argv[2]);

  Options opt(argc, argv);
  DefaultClient client(opt);  

  KillCommand killCmd(typeId, execId);
  client.run(killCmd);

  return 0;
}
