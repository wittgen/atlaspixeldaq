// N Savic <natascha.savic@cern.ch> 
#include "DefaultClient.h"
#include "GetGADCLeakage.h"
#include <fstream>
#include <iostream>
#include <ctime>
#include <string>
#include <cstdio>
#include <ostream>
#include <sstream>

//  Root include libraries
#include <TApplication.h>
#include <TObject.h>
#include <TString.h>
#include <TROOT.h>
#include <TStyle.h>
#include <TH2.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TKey.h>
#include <TDatime.h>

using namespace std;

#define HEX(x) std::hex << "0x" << (x) << std::dec

int main(int argc, char** argv) {
TApplication theApp("AppGadcOutput_Histos", &argc, argv);
  Options opt(argc, argv, "GetGADCLeakage");

  GetGADCLeakage cmd;

  //change values of different registers  

  cmd.Channels = opt.getChannels(); 
//0xFFFFFFFF? 
  cmd.IterInput = opt.getIter();
//Choose number of iteration of GADC outputs, additionally type --iter 'value from 1 to whatever' in command line
  cmd.UserMask = opt.getUserMask();
  cmd.cpMode=opt.getcpMode();
  cmd.nDC=opt.getnDC();
  //set pixel mask


//print results on host
   
  std::cout<<"The Leakage currents :"<<std::endl;
  std::cout<<"Number of iterations of GadcOutput: "<<cmd.IterInput<<std::endl;
  std::cout << "Summary of GADCOutpus for Rx 0-31 :" << std::endl; 


//  Root graphical display set up

  gStyle->SetPalette(1); 
  TH2F gadc_h ("gadc_h", "Gadc Output Leakage; Rx Channel; Stave" ,32, -0.5, 31.5, 14, 1, 14);
  for(size_t i=1; i < 15; i++) { //staves

#ifdef SR1
    if (i > 1) continue; // testing
    //    if (i<7)     opt.setSlot(i+5);
    //    else opt.setSlot(i+6);

    //if (i == 1) opt.setIp("192.168.2.90");
    if (i == 1) opt.setIp("192.168.2.160");

    //if (i != 1) continue;
#else
    //    if (i != 14) continue; // testing

    if (i<=7)    opt.setSlot(i+5);
    else opt.setSlot(i+6);
#endif

    DefaultClient client(opt);
    client.run(cmd);

// current date/time based on current system for file output
time_t now = time(0);

struct tm *t =localtime (& now);
stringstream ss; //convert into a string
ss << (t->tm_year + 1900) << '-'
<< (t->tm_mon + 1) << '-'
<<  t->tm_mday << '-' << '-' 
<< t->tm_hour << '-'
<< t->tm_min << '-'
   <<t-> tm_sec << '-'
<<"stave"
   << i ;

for (int n=0; n<32; n++) { 

if (cmd.result.resultarrayMean[n]==9999)

{std::cout << "For Rx "<< n << ": " << -1 << std::endl;}

 else { std::cout << "For Rx "<< n << ": " << cmd.result.resultarrayMean[n] << "+/-" << cmd.result.resultarrayStd[n] << std::endl;
    }
 }

 std::ofstream writeLeakage;
 writeLeakage.open(std::string("GadcLeakageCurrents/GadcLeakage_") +(ss.str())+ ".dat" , ios::out);
 writeLeakage << "This file summarizes the leakage current outputs of the GADC for Rx 0-31 :"<< "\n" << "The local date and time is: " << (t->tm_year + 1900) << '-' << (t->tm_mon + 1) << '-' <<  t->tm_mday << '-' << '-' << t->tm_hour << '-' << t->tm_min << '-' << t-> tm_sec <<
    "\n" << "Number of iterations of GadcOutput: "<< cmd.IterInput << "\n" << 
   "Every " <<cmd.UserMask<<"th pixel per DC is read out" << "\n"<< "You chose to enable " <<cmd.nDC<< " DCs to be read out" <<"\n" << "CP mode is set to " <<cmd.cpMode<< "\n" <<
endl;


for (int m=0; m<32; m++) {

if (cmd.result.resultarrayMean[m]==9999) {
 writeLeakage << "For Rx " << m << " : " << -1 << "\n" << endl;
 }

 else { writeLeakage << "For Rx " << m << " : " << cmd.result.resultarrayMean[m] << "+/-" << cmd.result.resultarrayStd[m] << "\n" << endl;
}

 }
writeLeakage.close();  

for(size_t j=0; j < 32 ; j++) { 
  //gadco = (int)(cmd.result.resultarrayMean[j]);

if (cmd.result.resultarrayMean[j]==9999) {
  gadc_h.SetBinContent(j+1,i, 0);}
 else {gadc_h.SetBinContent(j+1,i, cmd.result.resultarrayMean[j]);}

}
      //gadc_h.Fill(j, i, gadco); 
      
}
//}
//  Root output
 
//  TCanvas* c = new TCanvas ("Canvas" ,"Canvas" ,800 ,800);
    gadc_h.Draw("colztext");
    gadc_h.Print("colztext");
    
    std::string filename = "GadcLeakageCurrents/GadcLeakageHisto";
    
TDatime datime;
 int date = datime.GetDate();
 int time = datime.GetTime();

//create a c++ string with the name of the file + date and time
std::stringstream st;
st << filename << "_" << date << "_" << time << "_";

TFile *file = new TFile(st.str().c_str(),"NEW",st.str().c_str());
 
    gadc_h.Write();

    //f.Print();
  
    gadc_h.SaveAs("GadcLeakageCurrents/currentHisto.pdf");

    //gadc_h.SaveAs(st.str().c_str());
    //file->SaveAs(sf.str().c_str());

    theApp.Run(kTRUE);
 file->Close();
   
  return 0;
}
