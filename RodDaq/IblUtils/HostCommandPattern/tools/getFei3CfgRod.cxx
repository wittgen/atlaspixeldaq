#include "DefaultClient.h"
#include "GetFei3CfgRod.h"
#include "Fei3GlobalCfg.h"
#include "Fei3PixelCfg.h"

#include <iostream>
#include <iomanip>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::manip) << std::setfill('0') << static_cast<int>(y) << std::dec 

class LocalOptions: public Options {

  public:
    LocalOptions(int c, char* v[], std::string t = ""):
      Options(c, v, t, true) { // Initializing Options from derived class
      // register_options already callled, so are the extend_options from intermediate inheritance
      this->extend_options();
      this->init(c,v);
    }
    virtual void extend_options() {
      // Extending options (but only for this command)
      desc->add_options()
        ("feMask", po::value<std::string>(&feMaskStr)->default_value("0xF"), "Sets the FE mask (one bit for each of the 16 FEs per module)")
        ("opMode", po::value<std::string>(&opModeStr)->default_value("FullFE"), "Sets the op mode of Fei3Readback: GlobalRegReadback, PixelRegReadback, FullFE")
        ("printMaps", po::value<std::string>(&printMapsStr)->default_value("tfhpes"), "Lower case letters for printing (t)dac, (f)dac, (h)itbus, (p)reamp, (e)nable and (s)elect maps")
        ;

      // Overriding value for option
      override_option_default<std::string>("ch", "0xFFFFFFFF");
    }

    const uint32_t getFeMask()
    {
      return getMask(feMaskStr);
    }

    const uint32_t getMask(const std::string &maskStr) {
      if ((maskStr.substr(0,2) == "0x" && maskStr.length() > 6) || (maskStr.substr(0,2) != "0x" && maskStr.length() > 4)) {
        std::cout << "ERROR: the front-end mask is too long. Must have 4 digits or fewer" << std::endl;
        exit(1);
      }

      std::stringstream ss;
      uint32_t mask;
      ss << std::hex << maskStr;
      ss >> mask;
      return mask;
    }

    std::string feMaskStr;
    std::string opModeStr;
    std::string printMapsStr;

  protected:

  private:

};


int main(int argc, char **argv) {
  LocalOptions opt(argc, argv, "Options");

  GetFei3CfgRod fei3rdbck;
  fei3rdbck.m_rxMask = opt.getChannels();
  fei3rdbck.m_feMask = opt.getFeMask();

  if( opt.opModeStr == "GlobalRegReadback" )
    fei3rdbck.m_opMode = GetFei3CfgRod::GlobalRegReadback;
  else if( opt.opModeStr == "PixelRegReadback" )
    fei3rdbck.m_opMode = GetFei3CfgRod::PixelRegReadback;
  else if (opt.opModeStr == "FullFE")
    fei3rdbck.m_opMode = GetFei3CfgRod::FullFE;

  DefaultClient client(opt);
  client.run(fei3rdbck);

  if(fei3rdbck.m_opMode == GetFei3CfgRod::GlobalRegReadback || fei3rdbck.m_opMode == GetFei3CfgRod::FullFE) {
    for(auto& [rxCh , map] : fei3rdbck.result.GlobalCfg){
      for (auto& [fe , GlobalConfig] : map){
        std::cout << "\nGlobal registers for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
        GlobalConfig.dumpFei3GlobalCfg();
      }
    }
  }
  if(fei3rdbck.m_opMode == GetFei3CfgRod::PixelRegReadback || fei3rdbck.m_opMode == GetFei3CfgRod::FullFE) {
    for(auto& [rxCh , map] : fei3rdbck.result.PixelCfg) {
      for (auto& [fe , PixelConfig] : map){

	if(opt.printMapsStr.find('t') != std::string::npos) {
          std::cout << "\nTDAC map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout << std::setw(3) << (int)PixelConfig.readTDACPixel(row,col)<<" ";
            }
            std::cout << '\n';
          }
	}

	if(opt.printMapsStr.find('f') != std::string::npos) {
          std::cout << "\nFDAC map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout<< std::setw(3) << (int)PixelConfig.readFDACPixel(row,col) << " ";
            }
            std::cout << '\n';
          }
	}

	if(opt.printMapsStr.find('e') != std::string::npos) {
          std::cout << "\nEnable map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout<< std::setw(3) << (int)PixelConfig.readBitPixel(Fei3::PixLatch::Enable, row,col)<<" ";
            }
            std::cout << '\n';
          }
	}
  
	if(opt.printMapsStr.find('s') != std::string::npos) {
          std::cout << "\nSelect map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout<< std::setw(3) << (int)PixelConfig.readBitPixel(Fei3::PixLatch::Select, row,col)<<" ";
            }
            std::cout << '\n';
          }
	}
  
	if(opt.printMapsStr.find('h') != std::string::npos) {
          std::cout << "\nHitbus map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n"; 
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout<< std::setw(3) << (int)PixelConfig.readBitPixel(Fei3::PixLatch::HitBus, row,col)<<" ";
            }
            std::cout << '\n';
          }
	}
  
	if(opt.printMapsStr.find('p') != std::string::npos) {
          std::cout << "\nPreamp map for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
          for(int row=0;row<160;row++){
            for(int col=0;col<18;col++){
              std::cout<< std::setw(3) << (int)PixelConfig.readBitPixel(Fei3::PixLatch::Preamp, row,col)<<" ";
            }
            std::cout << '\n';
          }
        }
      }
    }
  }
  return 0;
}
