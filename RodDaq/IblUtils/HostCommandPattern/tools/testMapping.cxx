// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "TestMapping.h"

#include <iostream>
#include <cstring>

int main(int argc, char** argv) {
  Options opt(argc, argv, "testMapping");

  TestMapping cmd;

  cmd.channels = opt.getChannels();
  cmd.configType = Calib;

  DefaultClient client(opt);
  client.run(cmd);

  return 0;
}
