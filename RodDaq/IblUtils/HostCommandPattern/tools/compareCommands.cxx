#include "DefaultClient.h"
#include "ListCommands.h"
#include "CommandManager.h"

#include <iostream>
#include <iomanip>

int main(int argc, char** argv){

  ListCommands cmd;
  
  Options options(argc , argv);
  DefaultClient client(options);
  client.run(cmd);

  bool problem = false;
  int id_width = 6;    // For output message formatting
  int name_width = 57; // For output message formatting

  CommandManager::CommandMap::const_iterator hostCommand = CommandManager::getCommandMap().begin();


  std::cout << "------------------------------------------------------------------------------------------------------------------------" << std::endl;
  std::cout << setw(id_width) << "HostID" << setw(name_width) << "HostName" << setw(name_width) << "RodName" << std::endl;
  std::cout << "------------------------------------------------------------------------------------------------------------------------" << std::endl;
  
  for(; hostCommand != CommandManager::getCommandMap().end(); ++hostCommand){
    LocalCommands::CommandMap::const_iterator rodCommand = cmd.result.commands.find(hostCommand->first);
    if (rodCommand == cmd.result.commands.end()) {
      std::cout << setw(id_width+2*name_width) << "***************" << std::endl;
      std::cout << setw(id_width) << hostCommand->first << setw(name_width) << hostCommand->second->toString().substr(0,name_width-1);
      std::cout << setw(name_width) << "NONE" << std::endl;
      std::cout << setw(id_width+2*name_width) << "***************" << std::endl;
      problem = true;
    }
    else if (rodCommand->second != hostCommand->second->toString()) {
      std::cout << setw(id_width+2*name_width) << "***************" << std::endl;
      std::cout << setw(id_width) << hostCommand->first << setw(name_width) << hostCommand->second->toString().substr(0,name_width-1);
      std::cout << setw(name_width) << rodCommand->second.substr(0,name_width-1) << std::endl;
      std::cout << setw(id_width+2*name_width) << "***************" << std::endl;
      problem = true;
    }
    else {
      std::cout << setw(id_width) << hostCommand->first << setw(name_width) << hostCommand->second->toString().substr(0,name_width-1);
      std::cout << setw(name_width) << rodCommand->second.substr(0,name_width-1) << std::endl;
    }
  }

  std::cout << "------------------------------------------------------------------------------------------------------------------------" << std::endl;
  std::cout << std::endl;

  if (problem) {
    std::cout << "************************************************************************************************************************" << std::endl;
    std::cout << "WARNING: The two command maps are not compatible, there may be errors." << std::endl;
    std::cout << "         Check above for details." << std::endl;
    std::cout << "************************************************************************************************************************" << std::endl;
    return 1;
  }

  if (cmd.result.commands.size() > CommandManager::getCommandMap().size()) {
    std::cout << "There are additional commands defined on the rod." << std::endl;
    std::cout << "This should not cause any problems." << std::endl;
  }
  return 0;
}
