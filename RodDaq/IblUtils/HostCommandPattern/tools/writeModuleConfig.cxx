// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "WriteModuleConfig.h"
#include <iostream>

// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {
  //0) 
  Options opt(argc, argv, "writeConfig");

  //1)
  WriteModuleConfig writeCfg;

  //todo cleanup
  //2)
  writeCfg.setCalibConfig();
  Fei4ExtCfg newCfg;
  newCfg.ErrorMask_0.write(0xABCD);//for example
  for(uint32_t i = 0 ; i < 32; i++ ){
    // Works for Pixel Lab!! Not for SR!
    if(opt.getChannels() & (0x1<<i)){
      newCfg.setRxCh(i); newCfg.setTxCh(i);
      writeCfg.moduleConfigs.push_back(newCfg);
    }
  }

  //3
  DefaultClient client(opt);
  //4
  client.run(writeCfg);  
  //5

  //6 
  
  return 0;
}
