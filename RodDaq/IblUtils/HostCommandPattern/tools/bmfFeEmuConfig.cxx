/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-IV-28
 *
 */

#include "BmfFeEmuConfig.h"
#include "DefaultClient.h"

#include <cstring>
#include <iostream>

class LocalOptions: public Options {

	public:

		LocalOptions(int c, char* v[], std::string t = ""):
			Options(c, v, t, true) { // Initializing Options from derived class
			// register_options already callled, so are the extend_options from intermediate inheritance
			this->extend_options();
			this->init(c,v);
		}
		virtual void extend_options() {
			desc->add_options()
				("occ", po::value<int>(&occ)->default_value(1), "Sets the FE occupancy for the BOC emulator");
		}

		int getOcc() { return occ; }

	protected:

		int occ;

	private:

};

int main(int argc, char** argv) {
  LocalOptions opt(argc , argv, "Options");
  BmfFeEmuConfig cmd;
  cmd.hitsPerTrigger = opt.getOcc();
  cmd.channels = opt.getChannels();

  DefaultClient client(opt);
  client.run(cmd);
  std::cout << "BOC FE emulator configured!" << std::endl;

  return 0;
}
