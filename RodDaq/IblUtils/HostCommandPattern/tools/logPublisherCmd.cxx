#include "LogPublisherCmd.h"
//#ifndef IBL
//#define IBL_ROD_IP "192.168.2.110"   // crate 3, slot 18
//#define IBL_ROD_IP "192.168.2.110"   // crate 1, slot 7
//#endif
#include "DefaultClient.h"

#include <iostream>
#include <sstream>
#include <string>

void usage(const po::options_description & desc) {

        std::cout << "Usage: logPublisherCmd "<< "--ip RODIP [-h --mode MODE --host-ip IP --host-port PORT\n"
                  << "                       --time-standby-mac TIME --log-switch SWITCH --slave-verbose SLAVE_VERBOSITY\n"
                  << "                       --sleep SLEEP --verbosity VERBOSITY] \n";
        std::cout << desc << std::endl;

}

int main(int argc, const char** argv) {

    std::string rodip;
    LPConfig config;

    po::options_description desc =  po::options_description("Options");

    using po::value;
    using std::string;

    desc.add_options()
                    ("help,h", "Print help and exit")
                    ("ip,i", 		value<string>(&rodip)->required()	, "ROD's IP address (e.g. 192.168.2.XX in SR1)" )
                    ;

    desc.add_options()
                    ("mode,m"			, value(&config.mode)			, "LogPublisher mode: OFF(0), ON(1), KILL(1000)" )
                    ("host-ip"			, value(&config.receiver_ip)	, "Host IP" )
                    ("host-port"		, value(&config.receiver_port)	, "Host port" )
                    ("time-standby-max" , value(&config.time_standby_max)	, "Time interval [sec] for which ROD can be without activity. PING packet sent after that." )
                    ("log-switch" 		, value<string>()				, "Log source. Bitwise: Master(\"100\"), Slave A(\"010\"), Slave B(\"001\") and their or e.g. \"101\".")
                    ("slave-verbose"    , value(&config.slave_verbose)	, "Verbose logging for slaves (0/1)" )
                    ("sleep"    	    , value(&config.sleep)			, "Sleep [ms]" )
                    ("verbosity"     , value(&config.verbosity)	, "Log verbosity" )
                    ;

    po::variables_map vm;
    try {
        po::store(po::parse_command_line( argc, argv, desc), vm );
        po::notify(vm);

        if(vm.count("help")) {usage(desc); exit(0); }

        // special handle of the bitset
        if(vm.count("log-switch")) { config.log_switch = std::bitset<3>(vm["log-switch"].as<std::string>()).to_ulong(); }

    } catch (...) {
        usage(desc);
        exit(1);
    }

    LogPublisherCmd logcmd;
    logcmd.config = config;

    std::cout << "Sending to " << rodip << " the following setting:\n" << logcmd.config.print() << "\n";

    DefaultClient client(rodip.c_str());
    client.run(logcmd);

    std::cout  << "Obtained result: " << logcmd.result.rconfig.print() << std::endl;

    return 0;
}
