#include "Client.h"
#include "ReadRodSlaveBusyStatus.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

#define BUSY_MACRO(X)				(0x00000001 & X ) ? false : true	// active low logic
#define SLINK0LFF_STATUS_MACRO(X)		(0x00000002 & X ) ? true : false
#define SLINK0DOWN_STATUS_MACRO(X)		(0x00000004 & X ) ? true : false
#define SLINK1LFF_STATUS_MACRO(X)		(0x00000008 & X ) ? true : false
#define SLINK1DOWN_STATUS_MACRO(X)		(0x00000010 & X ) ? true : false
#define EFB1STTS0_STATUS_MAKRO(X)		(0x00000020 & X ) ? true : false
#define EFB1STTS1_STATUS_MAKRO(X)		(0x00000040 & X ) ? true : false
#define EFB2STTS0_STATUS_MAKRO(X)		(0x00000080 & X ) ? true : false
#define EFB2STTS1_STATUS_MAKRO(X)		(0x00000100 & X ) ? true : false
#define ROUTER0_STATUS_MACRO(X)			(0x00000200 & X ) ? true : false
#define ROUTER1_STATUS_MACRO(X)			(0x00000400 & X ) ? true : false
#define HISTFULL0_STATUS_MACRO(X)		(0x00000800 & X ) ? true : false
#define HISTFULL1_STATUS_MACRO(X)		(0x00001000 & X ) ? true : false


#define FORCE_BUSY_MASK_MACRO(X)		(0x00000001 & X ) ? true : false
#define SLINK0LFF_MASK_MACRO(X)			(0x00000002 & X ) ? true : false
#define SLINK0DOWN_MASK_MACRO(X)		(0x00000004 & X ) ? true : false
#define SLINK1LFF_MASK_MACRO(X)			(0x00000008 & X ) ? true : false
#define SLINK1DOWN_MASK_MACRO(X)		(0x00000010 & X ) ? true : false
#define EFB1STTS0_MASK_MAKRO(X)			(0x00000020 & X ) ? true : false
#define EFB1STTS1_MASK_MAKRO(X)			(0x00000040 & X ) ? true : false
#define EFB2STTS0_MASK_MAKRO(X)			(0x00000080 & X ) ? true : false
#define EFB2STTS1_MASK_MAKRO(X)			(0x00000100 & X ) ? true : false
#define ROUTER0_MASK_MACRO(X)			(0x00000200 & X ) ? true : false
#define ROUTER1_MASK_MACRO(X)			(0x00000400 & X ) ? true : false
#define HISTFULL0_MASK_MACRO(X)			(0x00000800 & X ) ? true : false
#define HISTFULL1_MASK_MACRO(X)			(0x00001000 & X ) ? true : false

#define FORCE_BUSY_FORCE_MACRO(X)		(0x00010000 & X ) ? true : false
#define SLINK0LFF_FORCE_MACRO(X)		(0x00020000 & X ) ? true : false
#define SLINK0DOWN_FORCE_MACRO(X)		(0x00040000 & X ) ? true : false
#define SLINK1LFF_FORCE_MACRO(X)		(0x00080000 & X ) ? true : false
#define SLINK1DOWN_FORCE_MACRO(X)		(0x00100000 & X ) ? true : false
#define EFB1STTS0_FORCE_MAKRO(X)		(0x00200000 & X ) ? true : false
#define EFB1STTS1_FORCE_MAKRO(X)		(0x00400000 & X ) ? true : false
#define EFB2STTS0_FORCE_MAKRO(X)		(0x00800000 & X ) ? true : false
#define EFB2STTS1_FORCE_MAKRO(X)		(0x01000000 & X ) ? true : false
#define ROUTER0_FORCE_MACRO(X)			(0x02000000 & X ) ? true : false
#define ROUTER1_FORCE_MACRO(X)			(0x04000000 & X ) ? true : false
#define HISTFULL0_FORCE_MACRO(X)		(0x08000000 & X ) ? true : false
#define HISTFULL1_FORCE_MACRO(X)		(0x10000000 & X ) ? true : false 


string mySlaveNumber = "";  



// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run(){  
  

  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	ReadRodSlaveBusyStatus cmd;

	// set command paramters
	cmd.dumpOnPPC = true;
	if (mySlaveNumber == "A")
		cmd.targetIsSlaveB = false;
	else if (mySlaveNumber == "B")
		cmd.targetIsSlaveB = true;
	else {
		std::cout << "\n\n !!! No valid target slave given, command not executed !!! \n\n";
		return;
	} 

	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;
	
	std::cout << "Disconnecting from server..."<<std::endl;

	// disconnect from server after command returns
	server.disconnectFromServer();

	// print result on host
	
	std::cout<<std::endl<<" Slave Busy Current Status Value = "<<HEX(0xffffffff & cmd.result.slaveBusyCurrentStatusValue)<<", \n this means: "<<std::endl;
	unsigned int returnval = BUSY_MACRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" SLAVE_OUT_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000002 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = SLINK0LFF_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" SLINK0LFF_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000004 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = SLINK0DOWN_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" SLINK0DOWN_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000008 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = SLINK1LFF_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" SLINK1LFF_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000010 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = SLINK1DOWN_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" SLINK1DOWN_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000020 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = EFB1STTS0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" EFB1STTS0_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000040 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = EFB1STTS1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" EFB1STTS1_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000080 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = EFB2STTS0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" EFB2STTS0_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000100 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = EFB2STTS1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" EFB2STTS1_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000200 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = ROUTER0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" ROUTER0_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000400 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = ROUTER1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" ROUTER1_STATUS:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000800 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = HISTFULL0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" HISTFULL0_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00001000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = HISTFULL1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" HISTFULL1_STATUS:\t\t\t"			<<returnval<<std::endl;

	returnval = (0x00002000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT0FF0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT0FF0_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00004000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT0FF1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT0FF1_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00008000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT0FF2_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT0FF2_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00010000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT0FF3_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT0FF3_STATUS:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00020000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT1FF0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT1FF0_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00040000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT1FF1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT1FF1_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00080000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT1FF2_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT1FF2_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00100000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT1FF3_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT1FF3_STATUS:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00200000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT2FF0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT2FF0_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00400000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT2FF1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT2FF1_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00800000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT2FF2_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT2FF2_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x01000000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT2FF3_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT2FF3_STATUS:\t\t\t"			<<returnval<<std::endl;

	returnval = (0x02000000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT3FF0_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT3FF0_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x04000000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT3FF1_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT3FF1_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x08000000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT3FF2_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT3FF2_STATUS:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x10000000 & cmd.result.slaveBusyCurrentStatusValue ) ? true : false;
	//returnval = FMT3FF3_STATUS_MAKRO(cmd.result.slaveBusyCurrentStatusValue);
	std::cout<<" FMT3FF3_STATUS:\t\t\t"			<<returnval<<std::endl;

	
	std::cout<<" Busy input counters on slave: "<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyOutHistValue;
	std::cout<<" BusyOutHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusySlink0LffHistValue;
	std::cout<<" BusySlink0LffHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusySlink0DownHistValue;
	std::cout<<" BusySlink0DownHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusySlink1LffHistValue;
	std::cout<<" BusySlink1LffHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusySlink1DownHistValue;
	std::cout<<" BusySlink1DownHist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyEfb1Stts0HistValue;
	std::cout<<" BusyEfb1Stts0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyEfb1Stts1HistValue;
	std::cout<<" BusyEfb1Stts1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyEfb2Stts0HistValue;
	std::cout<<" BusyEfb2Stts0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyEfb2Stts1HistValue;
	std::cout<<" BusyEfb2Stts1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyRouter0HistValue;
	std::cout<<" BusyRouter0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyRouter1HistValue;
	std::cout<<" BusyRouter1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyHistfull0HistValue;
	std::cout<<" BusyHistfull0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyHistfull1HistValue;
	std::cout<<" BusyHistfull1Hist:\t\t\t"<<returnval<<std::endl;

	returnval = 0x0000ffff & cmd.result.slaveBusyFmt0ff0HistValue;
	std::cout<<" BusyFmt0ff0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt0ff1HistValue;
	std::cout<<" BusyFmt0ff1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt0ff2HistValue;
	std::cout<<" BusyFmt0ff2Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt0ff3HistValue;
	std::cout<<" BusyFmt0ff3Hist:\t\t\t"<<returnval<<std::endl;

	returnval = 0x0000ffff & cmd.result.slaveBusyFmt1ff0HistValue;
	std::cout<<" BusyFmt1ff0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt1ff1HistValue;
	std::cout<<" BusyFmt1ff1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt1ff2HistValue;
	std::cout<<" BusyFmt1ff2Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt1ff3HistValue;
	std::cout<<" BusyFmt1ff3Hist:\t\t\t"<<returnval<<std::endl;

	returnval = 0x0000ffff & cmd.result.slaveBusyFmt2ff0HistValue;
	std::cout<<" BusyFmt2ff0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt2ff1HistValue;
	std::cout<<" BusyFmt2ff1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt2ff2HistValue;
	std::cout<<" BusyFmt2ff2Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt2ff3HistValue;
	std::cout<<" BusyFmt2ff3Hist:\t\t\t"<<returnval<<std::endl;	

	returnval = 0x0000ffff & cmd.result.slaveBusyFmt3ff0HistValue;
	std::cout<<" BusyFmt3ff0Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt3ff1HistValue;
	std::cout<<" BusyFmt3ff1Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt3ff2HistValue;
	std::cout<<" BusyFmt3ff2Hist:\t\t\t"<<returnval<<std::endl;
	returnval = 0x0000ffff & cmd.result.slaveBusyFmt3ff3HistValue;
	std::cout<<" BusyFmt3ff3Hist:\t\t\t"<<returnval<<std::endl;
	





	std::cout<<" Slave Busy Mask Value = "<<HEX(0xffffffff & cmd.result.slaveBusyMaskValue)<<", this means: "<<std::endl;
	returnval = (0x00000001 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FORCE_BUSY_MASK_MACRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FORCE_BUSY_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000002 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = SLINK0LFF_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" SLINK0LFF_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000004 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = SLINK0DOWN_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" SLINK0DOWN_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000008 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = SLINK1LFF_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" SLINK1LFF_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000010 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = SLINK1DOWN_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" SLINK1DOWN_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000020 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = EFB1STTS0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" EFB1STTS0_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000040 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = EFB1STTS1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" EFB1STTS1_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000080 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = EFB2STTS0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" EFB2STTS0_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000100 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = EFB2STTS1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" EFB2STTS1_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000200 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = ROUTER0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" ROUTER0_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000400 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = ROUTER1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" ROUTER1_MASK:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000800 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = HISTFULL0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" HISTFULL0_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00001000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = HISTFULL1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" HISTFULL1_MASK:\t\t\t"			<<returnval<<std::endl;



	returnval = (0x00002000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT0FF0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT0FF0_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00004000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT0FF1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT0FF1_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00008000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT0FF2_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT0FF2_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00010000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT0FF3_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT0FF3_MASK:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00020000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT1FF0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT1FF0_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00040000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT1FF1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT1FF1_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00080000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT1FF2_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT1FF2_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00100000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT1FF3_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT1FF3_MASK:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00200000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT2FF0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT2FF0_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00400000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT2FF1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT2FF1_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00800000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT2FF2_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT2FF2_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x01000000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT2FF3_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT2FF3_MASK:\t\t\t"			<<returnval<<std::endl;

	returnval = (0x02000000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT3FF0_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT3FF0_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x04000000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT3FF1_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT3FF1_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x08000000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT3FF2_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT3FF2_MASK:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x10000000 & cmd.result.slaveBusyMaskValue ) ? true : false;
	//returnval = FMT3FF3_MASK_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" FMT3FF3_MASK:\t\t\t"			<<returnval<<std::endl;



	std::cout<<" Slave Busy Force Value = "<<HEX((0xffffffff & cmd.result.slaveBusyForceValue))<<", this means: "<<std::endl;
	returnval = (0x00000001 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FORCE_BUSY_FORCE_MACRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FORCE_BUSY_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000002 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = SLINK0LFF_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" SLINK0LFF_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000004 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = SLINK0DOWN_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" SLINK0DOWN_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000008 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = SLINK1LFF_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" SLINK1LFF_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000010 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = SLINK1DOWN_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" SLINK1DOWN_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000020 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = EFB1STTS0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" EFB1STTS0_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000040 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = EFB1STTS1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" EFB1STTS1_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000080 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = EFB2STTS0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" EFB2STTS0_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000100 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = EFB2STTS1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" EFB2STTS1_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000200 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = ROUTER0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" ROUTER0_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000400 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = ROUTER1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" ROUTER1_FORCE:\t\t\t"			<<returnval<<std::endl;
	returnval = (0x00000800 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = HISTFULL0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" HISTFULL0_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00001000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = HISTFULL1_FORCE_MAKRO(cmd.result.slaveBusyMaskValue);
	std::cout<<" HISTFULL1_FORCE:\t\t\t"			<<returnval<<std::endl;



	returnval = (0x00002000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT0FF0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT0FF0_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00004000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT0FF1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT0FF1_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00008000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT0FF2_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT0FF2_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00010000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT0FF3_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT0FF3_FORCE:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00020000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT1FF0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT1FF0_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00040000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT1FF1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT1FF1_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00080000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT1FF2_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT1FF2_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00100000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT1FF3_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT1FF3_FORCE:\t\t\t"			<<returnval<<std::endl;	

	returnval = (0x00200000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT2FF0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT2FF0_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00400000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT2FF1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT2FF1_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x00800000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT2FF2_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT2FF2_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x01000000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT2FF3_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT2FF3_FORCE:\t\t\t"			<<returnval<<std::endl;

	returnval = (0x02000000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT3FF0_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT3FF0_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x04000000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT3FF1_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT3FF1_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x08000000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT3FF2_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT3FF2_FORCE:\t\t\t"			<<returnval<<std::endl;	
	returnval = (0x10000000 & cmd.result.slaveBusyForceValue ) ? true : false;
	//returnval = FMT3FF3_FORCE_MAKRO(cmd.result.slaveBusyForceValue);
	std::cout<<" FMT3FF3_FORCE:\t\t\t"			<<returnval<<std::endl;


	        
	//std::cout<<" Slave Busy Current Status Value = "<<HEX(0x000000ff & cmd.result.slaveBusyCurrentStatusValue)<<", this means: "<<std::endl;
	if (BUSY_MACRO(cmd.result.slaveBusyCurrentStatusValue))
		std::cout<<"\t SLAVE BUSY !!!"<<std::endl<<std::endl;
	else
		std::cout<<"\t SLAVE NOT BUSY !!!"<<std::endl<<std::endl;        
        

	
	std::cout << "Done." << std::endl;
}

int main(int argc, char* argv[]) {

        MyClient client(argv[2]);	
	mySlaveNumber = "";
	std::cout << "setting client IP = " << argv[2] << std::endl;

	if(argc == 3){
		mySlaveNumber = (string)argv[1];
		std::cout << "gonna read status of slave " << mySlaveNumber << std::endl;
		client.run();
		}
	else{
		std::cout << "Please give the slave identifier (A or B) as argument"<< std::endl;
		std::cout << "No command sent!"<< std::endl;
		}
	return 0;
}
