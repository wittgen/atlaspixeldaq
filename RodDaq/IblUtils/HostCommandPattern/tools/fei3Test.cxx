/*
 * fei3Test
 * By: Laser
 *    K. Potamianos <karolos.potamianos@cern.ch>
 * Executable for Fei3Test
 */

#include "DefaultClient.h"
#include "Fei3Test.h"

#include <iostream>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec 

class LocalOptions: public Options {

  public:

    LocalOptions(int c, char* v[], std::string t = ""):
      Options(c, v, t, true) { // Initializing Options from derived class
      // register_options already callled, so are the extend_options from intermediate inheritance
      this->extend_options();
      this->init(c,v);
    }
    virtual void extend_options() {
      // Extending options (but only for this command)
      desc->add_options()
        ("txShift", po::value<int16_t>(&txShift)->default_value(0), "Sets the shift of the Tx channel w.r.t. the Rx Channel (useful for simple cases)")
        ("feMask", po::value<std::string>(&feMaskStr)->default_value("0x0001"), "Sets the FE mask (one bit for each of the 16 FEs per module)")
        ("latency", po::value<uint16_t>(&latency)->default_value(248), "Sets the FE latency")
        ("trigDelay", po::value<uint16_t>(&trigDelay)->default_value(240), "Sets the delay between and of CAL and end of LVL1")
        ("pulseDuration", po::value<uint16_t>(&pulseDuration)->default_value(50), "Sets the FE strobe pulse duration")
        ("opMode", po::value<std::string>(&opModeStr)->default_value("DigitalInjection"), "Sets the op mode of Fei3Test")
        ("nTrigMCC", po::value<uint16_t>(&nTrigMCC)->default_value(16), "Sets the number of MCC internal triggers")
        ("nTrig", po::value<uint16_t>(&nTrig)->default_value(1), "Sets the number of triggers")
        ("vCal", po::value<uint16_t>(&vCal)->default_value(400), "Sets the VCal value")
        ("initModuleConfig", po::bool_switch(&initModuleConfig)->default_value(true), "Initialize module config (set this option to enable it)")
        ;

      // Overriding value for option
      override_option_default<std::string>("ch", "0x00000000");
    }

    const uint32_t getFeMask()
    {
      if ((feMaskStr.substr(0,2) == "0x" && feMaskStr.length() > 6) || (feMaskStr.substr(0,2) != "0x" && feMaskStr.length() > 4)) {
        std::cout << "ERROR: the front-end mask is too long. Must have 4 digits or fewer" << std::endl;
        exit(1);
      }

      std::stringstream ss;
      uint32_t feMask;
      ss << std::hex << feMaskStr;
      ss >> feMask;
      return feMask;
    }

    std::string feMaskStr;
    uint16_t latency;
    uint16_t trigDelay;
    uint16_t pulseDuration;
    uint16_t vCal;
    uint16_t nTrigMCC;
    uint16_t nTrig;
    int16_t txShift;
    bool initModuleConfig;

    std::string opModeStr;

  protected:

  private:

};


int main(int argc, char **argv) {
  LocalOptions opt(argc, argv, "Options");

  Fei3Test fei3t;
  fei3t.m_rxMask = opt.getChannels();

  fei3t.m_feMask = opt.getFeMask();
  fei3t.m_latency = static_cast<uint8_t>( opt.latency );
  fei3t.m_trigDelay = static_cast<uint8_t>( opt.trigDelay );
  fei3t.m_VCal = opt.vCal;
  fei3t.m_pulseDuration = static_cast<uint8_t>( opt.pulseDuration );
  fei3t.m_nConsecutiveTrig = static_cast<uint8_t>( opt.nTrigMCC );
  fei3t.m_nTriggers = opt.nTrig;
  fei3t.m_txShift = opt.txShift;
  fei3t.m_initModuleConfig = opt.initModuleConfig;


  if( opt.opModeStr == "DigitalInjection" )
    fei3t.m_opMode = Fei3Test::DigitalInjection;
  else if( opt.opModeStr == "AnalogInjection" )
    fei3t.m_opMode = Fei3Test::AnalogInjection;
  else if( opt.opModeStr == "GlobalRegReadback" )
    fei3t.m_opMode = Fei3Test::GlobalRegReadback;
  else if( opt.opModeStr == "PixelRegReadback" )
    fei3t.m_opMode = Fei3Test::PixelRegReadback;
  else if( opt.opModeStr == "MccInjection" )
    fei3t.m_opMode = Fei3Test::MccInjection;
  else if( opt.opModeStr == "MccRegReadback" )
    fei3t.m_opMode = Fei3Test::MccRegReadback;
  else if( opt.opModeStr == "DumpModConfigs" )
    fei3t.m_opMode = Fei3Test::DumpModConfigs;
  else if( opt.opModeStr == "ResetModules" )
    fei3t.m_opMode = Fei3Test::ResetModules;
  else if( opt.opModeStr == "ConfigureGlobal" )
    fei3t.m_opMode = Fei3Test::ConfigureGlobal;
  else if( opt.opModeStr == "MccRegTest" )
    fei3t.m_opMode = Fei3Test::MccRegTest;
  else if( opt.opModeStr == "ConfigureAmpsOn" )
    fei3t.m_opMode = Fei3Test::ConfigureAmpsOn;

  fei3t.printParams();

  DefaultClient client(opt);
  client.run(fei3t);

  return 0;
}
