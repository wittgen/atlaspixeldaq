/*
 * Author: K. Potamianos
 * Date: 2014-XI-20
 */

#include "SilentClient.h"
#include "busyOptions.ixx"
#include "ReadRodMasterBusyStatus.h"
#include "ReadRodSlaveBusyStatus.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// Todo: use C++11 features
void dump_busy_status(uint32_t busy_status) {
	uint32_t mask = 0x80000000;
	char line[200];
	while (mask) { // The idea is that mask will be 0 when we have done the last shift
		snprintf(line, sizeof(line), " %s", (busy_status & mask) ? "X" : "-");
		std::cout << line;
		mask >>= 1;
	}
	std::cout << std::hex << "\t Value: 0x" << busy_status << std::dec;
	std::cout << std::endl;
}

void show_busy_header_master() {
	std::cout << "\t       MASTER                                    S R F F E S S O" << std::endl;
	std::cout << "\t                                                 P O M M F L L U" << std::endl;
	std::cout << "\t                                                 A L T T B V V T" << std::endl;
	std::cout << "\t                                                 R T 1 0   1 0  " << std::endl;
}
/*

MASTER_OUT_STATUS:			0
ROD_BUSY_N_IN_P_0_STATUS:			0
ROD_BUSY_N_IN_P_1_STATUS:			0
EFB_HEADER_PAUSE_I_STATUS:			0
FORMATTER_MB_FEF_N_IN_P_0_STATUS:			0
FORMATTER_MB_FEF_N_IN_P_1_STATUS:			1
ROL_TEST_PAUSE_I_STATUS:			0
SPARE_CTRL_REG_I_STATUS:			0
 */

void show_busy_header_slave(bool isSlaveA) {
	std::cout << "\t                                                                " << std::endl;
	if(isSlaveA)
	std::cout << "\t       SLAVE A-side / NORTH            H H R R E E E E 1 1 0 0 O" << std::endl;
	else
	std::cout << "\t       SLAVE C-side / SOUTH            H H R R E E E E 1 1 0 0 O" << std::endl;

	std::cout << "\t                                       I I T T 2 2 1 1 D L D L U" << std::endl;
	std::cout << "\t         FMT3    FMT2    FMT1    FMT0  S S R R S S S S W F W F T" << std::endl;
	std::cout << "\t       3 2 1 0 3 2 1 0 3 2 1 0 3 2 1 0 1 0 1 0 1 0 1 0 N F N F  " << std::endl;
}

/*
 FORCE_BUSY_MASK:			0
 SLINK0LFF_MASK:			0
 SLINK0DOWN_MASK:			0
 SLINK1LFF_MASK:			0
 SLINK1DOWN_MASK:			0
 EFB1STTS0_MASK:			0
 EFB1STTS1_MASK:			0
 EFB2STTS0_MASK:			0
 EFB2STTS1_MASK:			0
 ROUTER0_MASK:			0
 ROUTER1_MASK:			0
 HISTFULL0_MASK:			1
 HISTFULL1_MASK:			1
 FMT0FF0_MASK:			0
 FMT0FF1_MASK:			0
 FMT0FF2_MASK:			0
 FMT0FF3_MASK:			0
 FMT1FF0_MASK:			0
 FMT1FF1_MASK:			0
 FMT1FF2_MASK:			0
 FMT1FF3_MASK:			0
 FMT2FF0_MASK:			0
 FMT2FF1_MASK:			0
 FMT2FF2_MASK:			0
 FMT2FF3_MASK:			0
 FMT3FF0_MASK:			0
 FMT3FF1_MASK:			0
 FMT3FF2_MASK:			0
 FMT3FF3_MASK:			0
 */

void dump_busy_hist(ReadSlaveBusyStatusResult& result) {
  std::cout<<"slaveBusyOutHistValue: "<<HEX(result.slaveBusyOutHistValue)<<std::endl;

  std::cout<<"slaveBusySlink0LffHistValue: "<<HEX(result.slaveBusySlink0LffHistValue)<<std::endl;
  std::cout<<"slaveBusySlink0DownHistValue: "<<HEX(result.slaveBusySlink0DownHistValue)<<std::endl;
  std::cout<<"slaveBusySlink1LffHistValue: "<<HEX(result.slaveBusySlink1LffHistValue)<<std::endl;
  std::cout<<"slaveBusySlink1DownHistValue: "<<HEX(result.slaveBusySlink1DownHistValue)<<std::endl;
  std::cout<<"slaveBusyEfb1Stts0HistValue: "<<HEX(result.slaveBusyEfb1Stts0HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb1Stts1HistValue: "<<HEX(result.slaveBusyEfb1Stts1HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb2Stts0HistValue: "<<HEX(result.slaveBusyEfb2Stts0HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb2Stts1HistValue: "<<HEX(result.slaveBusyEfb2Stts1HistValue)<<std::endl;
  std::cout<<"slaveBusyRouter0HistValue: "<<HEX(result.slaveBusyRouter0HistValue)<<std::endl;
  std::cout<<"slaveBusyRouter1HistValue: "<<HEX(result.slaveBusyRouter1HistValue)<<std::endl;

  std::cout<<"slaveBusyHistfull0HistValue: "<<HEX(result.slaveBusyHistfull0HistValue)<<std::endl;
  std::cout<<"slaveBusyHistfull1HistValue: "<<HEX(result.slaveBusyHistfull1HistValue)<<std::endl;
  std::cout<<"slaveBusyHistoverrun0HistValue: "<<HEX(result.slaveBusyHistoverrun0HistValue)<<std::endl;
  std::cout<<"slaveBusyHistoverrun1HistValue: "<<HEX(result.slaveBusyHistoverrun1HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt0ff0HistValue: "<<HEX(result.slaveBusyFmt0ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff1HistValue: "<<HEX(result.slaveBusyFmt0ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff2HistValue: "<<HEX(result.slaveBusyFmt0ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff3HistValue: "<<HEX(result.slaveBusyFmt0ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt1ff0HistValue: "<<HEX(result.slaveBusyFmt1ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff1HistValue: "<<HEX(result.slaveBusyFmt1ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff2HistValue: "<<HEX(result.slaveBusyFmt1ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff3HistValue: "<<HEX(result.slaveBusyFmt1ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt2ff0HistValue: "<<HEX(result.slaveBusyFmt2ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff1HistValue: "<<HEX(result.slaveBusyFmt2ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff2HistValue: "<<HEX(result.slaveBusyFmt2ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff3HistValue: "<<HEX(result.slaveBusyFmt2ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt3ff0HistValue: "<<HEX(result.slaveBusyFmt3ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff1HistValue: "<<HEX(result.slaveBusyFmt3ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff2HistValue: "<<HEX(result.slaveBusyFmt3ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff3HistValue: "<<HEX(result.slaveBusyFmt3ff3HistValue)<<std::endl;
}

int main(int argc, char* argv[]) {

	Options opt(argc, argv);
	SilentClient client(opt);

#ifdef USE_ROD_CRATE
	SctPixelRod::IblRodModule m_rod = new IblRodModule( 0 , 0 , 1 , srvIpPort() , srvIpAddr() );
#endif

	ReadRodMasterBusyStatus cmdMaster;
	cmdMaster.dumpOnPPC = true;
	if( ! client.run(cmdMaster) ) {
		std::cout << "ERROR: Cannot talk to the PPC. Aborting." << std::endl;
		return EXIT_FAILURE;
	}

	// print result on host
	show_busy_header_master();
	std::cout << "STATUS\t"; dump_busy_status(cmdMaster.result.masterBusyCurrentStatusValue & 0xFF);
	std::cout << "MASK  \t"; dump_busy_status(cmdMaster.result.masterBusyMaskValue & 0xFF );
	std::cout << "FORCE \t"; dump_busy_status((cmdMaster.result.masterBusyMaskValue >> 8) & 0xFF);

	// intatintiate command
	ReadRodSlaveBusyStatus cmdSlave;

	// set command paramters
	cmdSlave.dumpOnPPC = true;
	if ( opt.getSlave() == 0 || opt.getSlave() == 2 ) // We will process the 2nd slave later if need be
		cmdSlave.targetIsSlaveB = false;
	else if ( opt.getSlave() == 1 )
		cmdSlave.targetIsSlaveB = true;
	else {
		std::cerr << "\n\n !!! No valid target slave given, command not executed !!! \n\n";
		return 1;
	}

	client.run(cmdSlave);

	// print result on host
	show_busy_header_slave(true); // Slave A
	std::cout << "STATUS\t"; dump_busy_status(cmdSlave.result.slaveBusyCurrentStatusValue);
	std::cout << "MASK  \t"; dump_busy_status(cmdSlave.result.slaveBusyMaskValue);
	std::cout << "FORCE \t"; dump_busy_status(cmdSlave.result.slaveBusyForceValue);

	if(getenv("VERBOSE")) dump_busy_hist(cmdSlave.result);

	if( opt.getSlave() != 2 ) return 0;

	cmdSlave.targetIsSlaveB = true;
	client.run(cmdSlave);
#ifndef USE_SINGLE_CONNECTION
	// disconnect from server after command returns
	// server.disconnectFromServer();
#endif
	// print result on host
	show_busy_header_slave(false); // Slave B
	std::cout << "STATUS\t"; dump_busy_status(cmdSlave.result.slaveBusyCurrentStatusValue);
	std::cout << "MASK  \t"; dump_busy_status(cmdSlave.result.slaveBusyMaskValue);
	std::cout << "FORCE \t"; dump_busy_status(cmdSlave.result.slaveBusyForceValue);

	if(getenv("VERBOSE")) dump_busy_hist(cmdSlave.result);

	return 0;
}
