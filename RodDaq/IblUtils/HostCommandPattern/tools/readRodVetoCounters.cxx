
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2017-V-3
 */

#include "DefaultClient.h"
#include "ReadRodVetoCounters.h"

#include <iostream>
#include <cstdlib>

int main(int argc,  char** argv) {

	Options opt(argc , argv, "");

	ReadRodVetoCounters readVetoCounters;

	DefaultClient client(opt);
	client.run(readVetoCounters);

    std::cout<<"Dumping ROD counters"<<std::endl;
    std::cout<<"ECR VETO: "<<(int)readVetoCounters.result.ECR_Vetoed<<std::endl;
    std::cout<<"BCR VETO: "<<(int)readVetoCounters.result.BCR_Vetoed<<std::endl;
    std::cout<<"L1A VETO: "<<(int)readVetoCounters.result.L1A_Vetoed<<std::endl;
    std::cout<<"ECR DROPPED: "<<(int)readVetoCounters.result.ECR_Dropped<<std::endl;
    std::cout<<"BCR DROPPED: "<<(int)readVetoCounters.result.BCR_Dropped<<std::endl;
    std::cout<<"L1A DROPPED: "<<(int)readVetoCounters.result.L1A_Dropped<<std::endl;

	return 0;

}
