// Russell Smith 5/6/14
// Command to write ROD registers

#include "WriteRodRegisters.h"
#include "DefaultClient.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// what the client (the host) should do
// in summary, the host does this:
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {

  //1)instantiate the command
  WriteRodRegisters writeRodRegisters;

  //2) configure command
  writeRodRegisters.epcBaseValue        = 0x0;
  writeRodRegisters.epcLocalValue       = 0x0; 
  writeRodRegisters.epcSlvARegsValue    = 0x0;
  writeRodRegisters.epcSlvARamValue     = 0xc0ffee;
  writeRodRegisters.epcSlvBRegsValue    = 0x0;
  writeRodRegisters.epcSlvBRamValue     = 0x0;
  writeRodRegisters.epcBocValue         = 0x40;
  writeRodRegisters.epcLocalCtrlValue   = 0x80000000;
  //writeRodRegisters.epcLocalStatValue   = 0x0;//maybe readonly?
  writeRodRegisters.epcLocalSpMaskValue = 0x0;
  writeRodRegisters.dspRegBaseValue     = 0x0;
  writeRodRegisters.fmt0RegBaseValue    = 0x0;
  writeRodRegisters.fmt1RegBaseValue    = 0x0;
  //writeRodRegisters.designRegPPCValue = 0x20008 readonly

  //3)connect to ppc
  Options options(argc , argv);
  DefaultClient client(IBL_ROD_IP);
  //4)run (send) the command
  client.run(writeRodRegisters);
  //5)disconnect from server (automatic)

  //6)process results below if necessary

  return 0;

}
