#include "SilentClient.h"
#include "GetServiceRecords.h"

#include <iostream>
#include <iomanip>
#include <string>

int main(int argc,  char** argv) {

    Options opt(argc , argv);
    SilentClient client(opt);
    GetServiceRecords cmd;

    client.run(cmd);

    for (int i = 0; i<32; i++) {
      std::cout<<"***************"<<std::endl;
      std::cout<<"For RX: "<<i<<std::endl;
      for (int j = 0; j<32; j++) {
	std::cout<<"SR "<<j<<" = "<<cmd.result.srValues[i][j]<<std::endl;
    }
      std::cout<<"***************"<<std::endl;
      std::cout<<std::endl;
    }
    return 0;
    
}


