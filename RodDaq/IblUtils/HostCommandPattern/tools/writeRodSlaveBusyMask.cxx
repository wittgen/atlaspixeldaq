#include "Client.h"
#include "WriteRodSlaveBusyMask.h"

#include <iostream>
#include <cstdlib>
#include <string>
using namespace std;

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define FORCE_BUSY_MASK_MACRO(X)		(0x00000001 & X ) ? true : false
#define ROD_BUSY_N_IN_P_0_MASK_MACRO(X)		(0x00000002 & X ) ? true : false
#define ROD_BUSY_N_IN_P_1_MASK_MACRO(X)		(0x00000004 & X ) ? true : false
#define EFB_HEADER_PAUSE_I_MASK_MACRO(X)	(0x00000008 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_0_MASK_MACRO(X)	(0x00000010 & X ) ? true : false
#define FORMATTER_MB_FEF_N_IN_P_1_MASK_MACRO(X)	(0x00000020 & X ) ? true : false
#define ROL_TEST_PAUSE_I_MASK_MACRO(X)		(0x00000040 & X ) ? true : false


int myMaskValue;
int myForceValue;

string mySlaveNumber;

// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(const char*);
	virtual void run();
};

MyClient::MyClient(const char* a): Client(a) {

}

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	WriteRodSlaveBusyMask cmd;

	// set command paramters
	cmd.dumpOnPPC = true;
	if (mySlaveNumber == "A")
		cmd.targetIsSlaveB = false;
	else if (mySlaveNumber == "B")
		cmd.targetIsSlaveB = true;
	else {
		std::cout << "\n\n !!! No valid target slave given, command not executed !!! \n\n";
		return;
	} 		

	// set busy mask to write
	cmd.maskValue = myMaskValue;
	// set busy force to write
	cmd.forceValue = myForceValue;

	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;
	
	std::cout << "Disconnecting from server...";

	// disconnect from server after command returns
	server.disconnectFromServer();

	// print result on host

        std::cout<<"RodSlave Busy Mask written"<<std::endl;
        

	
	std::cout << "Done." << std::endl;

}

int main(int argc, char* argv[]) {

        MyClient client(argv[4]);
	myMaskValue = 0x00000000;	
	myForceValue = 0x00000000;
	mySlaveNumber = "";
	std::cout << "setting client IP = " << argv[4] << std::endl;

	if(argc == 5){
		mySlaveNumber = (string)argv[1];
		
		string myMaskString = "";
		string myForceString = "";
		
		myMaskString = (string)argv[2];
		myForceString = (string)argv[3];
		
		std::stringstream ssMask;
		ssMask << std::hex << myMaskString;
		ssMask >> myMaskValue;
		
		std::stringstream ssForce;
		ssForce << std::hex << myForceString;
		ssForce >> myForceValue;
		
        	std::cout << "gonna write slave " << mySlaveNumber << " busy mask to " << myMaskValue << " and busy force to " << myForceValue << std::endl;
		client.run();
		}
	else{
		std::cout << "Please give four arguments: first the slave you want to write to (A or B), second the busy mask value, then busy force value, both 16bit hex format (no *0x*), and finally the IP address of the ROD"<< std::endl;
		std::cout << "No command sent!"<< std::endl;
		}
	return 0;

}


//int main(int argc, char** argv) {
//  Options o(argc, argv, "enable");
//  MyClient client(o.getIp());
//  client.run(o.getChannels());  
// return 0;
//}

