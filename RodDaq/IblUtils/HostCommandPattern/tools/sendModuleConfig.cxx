// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "SendModuleConfig.h"

#include <iostream>
#include <cstring>

class LocalOptions: public Options {

public:

  LocalOptions(int c, char* v[], std::string t = ""):
    Options(c, v, t, true) { // Initializing Options from derived class                                                                                                                 
    // register_options already callled, so are the extend_options from intermediate inheritance                                                                                        
    this->extend_options();
    this->init(c,v);
    this->post_init(c,v);
  }
  virtual void extend_options() {
    desc->add_options()
      ("ampsOn", "SendModuleConfig with amps ON")
      ("ampsOff", "SendModuleConfig with amps OFF")
      ;
      }

  void post_init(int c, char* v[]) {

  if (vm.count("ampsOff")) {
    std::cout<<"yo amps off"<<std::endl;
    m_ampsOff = true;
  }
  else if (vm.count("ampsOn")) {
    std::cout<<"yo amps on"<<std::endl;
    m_ampsOff = false;
  }
  else {
    std::cout<<"Please choose ampsOn or ampsOff!"<<std::endl;
    exit(0);
  }

  }

  int getAmpsOff() { return m_ampsOff; }
  
private:
  bool m_ampsOff;
};


int main(int argc, char** argv) {
  LocalOptions options(argc , argv);

  SendModuleConfig sendCfg;
  
  options.getAmpsOff() ? sendCfg.turnOffPreAmps() : sendCfg.turnOnPreAmps() ;
  std::cout<<" Sending config with preamps "<< (options.getAmpsOff() ? "OFF" : "ON") <<std::endl;

  sendCfg.setVerbose();
  sendCfg.setCalibConfig();
  sendCfg.setRxMask(options.getChannels());
  sendCfg.setSendAll(true); // true: send global + pixel regs, false: send global regs only
  sendCfg.readBackSR(true); // read back service records?
  sendCfg.setSleepDelay(100000); // how long to sleep between configuring each global FE

  DefaultClient client(options);

  client.run(sendCfg);  

  std::cout<<"To check that the channels are really configured, check the current they are drawing"<< std::endl;

  std::cout<<"You requested to send the module config to channels: 0x"<<std::hex<<options.getChannels()<<std::endl;
  std::cout<<"Successfully enabled channels: 0x"<<sendCfg.result.enabledMask<<std::endl;
  std::cout<<"Global config read back OK on channels: 0x"<<sendCfg.result.readBackOKMask<<std::dec<<std::endl;

  return 0;
}
