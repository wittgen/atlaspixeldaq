
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 */

#include "DefaultClient.h"
#include "QSTalkTask.h"
#include "primXface_common.h"

#include <iostream>
#include <iomanip>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv) {

  Options options(argc , argv);
  DefaultClient client(options);


   QSTalkTask qsTalkTaskCmd;
   qsTalkTaskCmd.quickStatusMode = QS_MON; // Todo: get from ROD
   qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_MODE;
   if(!client.run(qsTalkTaskCmd)) {
     std::cout << "ERROR: couldn't set mode" << std::endl;
     return EXIT_FAILURE;
   }

   qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_SET_VERBOSITY;
   qsTalkTaskCmd.fc_max = 0; //opt.getVerbosityLevel();
   if(!client.run(qsTalkTaskCmd)) {
     std::cout << "ERROR: couldn't set verbosity" << std::endl;
     return EXIT_FAILURE;
   }

   qsTalkTaskCmd.quickStatusMode = QS_MON; // Todo: get from ROD
   qsTalkTaskCmd.qsttmode = QSTalkTask::QSTT_INFO;

  qsTalkTaskCmd.dumpOnPPC = true;
  if( getenv("NO_DUMP_ON_PPC") )
    qsTalkTaskCmd.dumpOnPPC = false;

  if(!client.run(qsTalkTaskCmd)) return EXIT_FAILURE;

  QuickStatus::monitoring_infos m_qsMonInfos = qsTalkTaskCmd.result.m_qsMonInfos;

  std::cout << "Slave BUSY counters:" << std::endl;
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.slaveBusyCounters)/sizeof(uint32_t) ; ++i)
    std::cout << std::setw(2) << i << ": " << m_qsMonInfos.slaveBusyCounters[i] << "; ";
  std::cout << std::endl;

  std::cout << "Link occupancy counters:" << std::endl;
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkOccCounters)/sizeof(uint32_t) ; ++i)
    std::cout << std::setw(2) << i << ": " << m_qsMonInfos.linkOccCounters[i] << "; ";
  std::cout << std::endl;

  bool sLink0XOff = ( m_qsMonInfos.slaveBusyCounters[2] - m_qsMonInfos.slaveBusyCounters[0] <= 1000 );
  bool sLink0Down = ( m_qsMonInfos.slaveBusyCounters[3] - m_qsMonInfos.slaveBusyCounters[0] <= 1000 );
  bool sLink1XOff = ( m_qsMonInfos.slaveBusyCounters[4] - m_qsMonInfos.slaveBusyCounters[0] <= 1000 );
  bool sLink1Down = ( m_qsMonInfos.slaveBusyCounters[5] - m_qsMonInfos.slaveBusyCounters[0] <= 1000 );

  bool sLinkXOffOrDown = sLink0XOff || sLink0Down || sLink1XOff || sLink1Down;

  int busyPerc = 0;
  if(m_qsMonInfos.slaveBusyCounters[0]) {
    busyPerc = 100 * ((double)m_qsMonInfos.slaveBusyCounters[1]/m_qsMonInfos.slaveBusyCounters[0]);
  }

  if (sLinkXOffOrDown) busyPerc += 1000;

  std::cout << "busyPerc = " << busyPerc << std::endl;

  // Assuming Ly2 for now
  // Todo: fix me
  double avgOcc = std::accumulate( m_qsMonInfos.linkOccCounters, m_qsMonInfos.linkOccCounters + 32, 0.);
  uint32_t nActiveLinks = std::count_if(m_qsMonInfos.linkTrigCounters,m_qsMonInfos.linkTrigCounters + 32, [](int n){return n > 0;});
  // Alternative: populate m_qsMonInfos.nTriggersOccCounters in PPC QuickStatus with Master triggers (but need to get it right)
  uint32_t nTriggers = std::accumulate(m_qsMonInfos.linkTrigCounters, m_qsMonInfos.linkTrigCounters + 32, 0.) / nActiveLinks;

  constexpr float nPixInFei3 = 16. * 18. * 160.;
  //constexpr float nPixInFei4 = 336. * 80.;

  // Normalize the occupancy per trigger and per pixel
  if( nTriggers && nActiveLinks ) {
    avgOcc /= nTriggers;
    avgOcc /= nActiveLinks;
    avgOcc /= nPixInFei3;	// Assume layer 1/2
  }

  std::cout << "avgOcc = " << avgOcc << std::endl;


}


