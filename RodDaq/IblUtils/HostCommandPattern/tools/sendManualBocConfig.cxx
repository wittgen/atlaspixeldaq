
#include "DataTakingTools.h"
#include "SendModuleConfig.h"
#include "DefaultClient.h"
#include <iostream>
#include <iomanip>
#include <cstring>


int main(int argc, char** argv){

	Options options(argc , argv);

	DataTakingTools sendManualConfig;
	sendManualConfig.action = BocManualConfig;
	sendManualConfig.channels = options.getChannels();
	
	std::cout<<"channel mask 0x"<<std::hex<<options.getChannels()<<std::dec<<std::endl;
	DefaultClient client(options);

	client.run(sendManualConfig);

	return 0;
}
