#include "DefaultClient.h"
#include "GetFei4CfgRod.h"
#include "Fei4GlobalCfg.h"
#include "Fei4PixelCfg.h"

#include <iostream>
#include <iomanip>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec
#define HEXF(x,y) std::hex << "0x" << std::hex << std::manip) << std::setfill('0') << static_cast<int>(y) << std::dec 

class LocalOptions: public Options {

  public:
    LocalOptions(int c, char* v[], std::string t = ""):
      Options(c, v, t, true) { // Initializing Options from derived class
      // register_options already callled, so are the extend_options from intermediate inheritance
      this->extend_options();
      this->init(c,v);
    }
    virtual void extend_options() {
      // Extending options (but only for this command)
      desc->add_options()
        ("opMode", po::value<std::string>(&opModeStr)->default_value("FullFE"), "Sets the op mode of Fei3Readback: GlobalRegReadback, PixelRegReadback, FullFE")
        ("printMaps", po::value<std::string>(&printMapsStr)->default_value("tfhesl"), "Lower case letters for printing (t)dac, (f)dac, (h)itbus, (e)nable, (s)mall and (l)arge injection cap maps")
        ;

      // Overriding value for option
      override_option_default<std::string>("ch", "0xFFFFFFFF");
    }


    const uint32_t getMask(const std::string &maskStr) {
      if ((maskStr.substr(0,2) == "0x" && maskStr.length() > 6) || (maskStr.substr(0,2) != "0x" && maskStr.length() > 4)) {
        std::cout << "ERROR: the front-end mask is too long. Must have 4 digits or fewer" << std::endl;
        exit(1);
      }

      std::stringstream ss;
      uint32_t mask;
      ss << std::hex << maskStr;
      ss >> mask;
      return mask;
    }

    std::string opModeStr;
    std::string printMapsStr;
};

int main(int argc, char **argv) {
  LocalOptions opt(argc, argv, "Options");

  GetFei4CfgRod fei4rdbck;
  fei4rdbck.m_rxMask = opt.getChannels();

  if( opt.opModeStr == "GlobalRegReadback" )
    fei4rdbck.m_opMode = GetFei4CfgRod::GlobalRegReadback;
  else if( opt.opModeStr == "PixelRegReadback" )
    fei4rdbck.m_opMode = GetFei4CfgRod::PixelRegReadback;
  else if (opt.opModeStr == "FullFE")
    fei4rdbck.m_opMode = GetFei4CfgRod::FullFE;

  DefaultClient client(opt);
  client.run(fei4rdbck);
  
  if(fei4rdbck.m_opMode == GetFei4CfgRod::GlobalRegReadback || fei4rdbck.m_opMode == GetFei4CfgRod::FullFE) {
    for(auto& [rxCh , map] : fei4rdbck.result.GlobalCfg){
      for (auto& [fe , GlobalConfig] : map){
        std::cout << "\nGlobal registers for channel " << (int)rxCh<< " (FE " << (int)fe << ")\n";
        GlobalConfig.dump();
      }
    }
  }
  if(fei4rdbck.m_opMode == GetFei4CfgRod::PixelRegReadback || fei4rdbck.m_opMode == GetFei4CfgRod::FullFE) {
    for(auto& [rxCh , map] : fei4rdbck.result.PixelCfg) {
      for (auto& [fe , PixelConfig] : map){

	if(opt.printMapsStr.find('t') != std::string::npos) {
          std::cout << "\nTDAC map for channel " << (int)rxCh << "\n\n";

	  std::cout << "TDAC| ";
	  for(unsigned col=0; col < 40; ++col) std::cout << std::setw(4) << col;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 40; ++col) std::cout << std::setw(4) << "----";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 40; ++col){
			  std::cout << std::setw(4) << PixelConfig.TDAC( col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << '\n';
	  }//column loop

	  std::cout << "\nTDAC| ";
	  for(unsigned col=40; col < 80; ++col) std::cout << std::setw(4) << col;
	  std::cout << "\n----- ";
	  for(unsigned col=40; col < 80; ++col) std::cout << std::setw(4) << "----";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=40; col < 80; ++col){
			  std::cout << std::setw(4) << PixelConfig.TDAC( col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << '\n';
	  }//column loop
	}

	if(opt.printMapsStr.find('f') != std::string::npos) {
          std::cout << "\nFDAC map for channel " << (int)rxCh << "\n\n";

	  std::cout << "FDAC| ";
	  for(unsigned col=0; col < 40; ++col) std::cout << std::setw(4) << col;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 40; ++col) std::cout << std::setw(4) << "----";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 40; ++col){
			  std::cout << std::setw(4) << PixelConfig.FDAC( col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << '\n';
	  }//column loop

	  std::cout << "\nFDAC| ";
	  for(unsigned col=40; col < 80; ++col) std::cout << std::setw(4) << col;
	  std::cout << "\n----- ";
	  for(unsigned col=40; col < 80; ++col) std::cout << std::setw(4) << "----";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=40; col < 80; ++col){
			  std::cout << std::setw(4) << PixelConfig.FDAC( col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << '\n';
	  }//column loop
	}

	if(opt.printMapsStr.find('e') != std::string::npos) {
          std::cout << "\nENABLE map for channel " << (int)rxCh << "\n\n";

	  std::cout << "ENA | ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::left << std::setw(8) << col << std::right;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::setw(4) << "--------";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 80; ++col){
			  std::cout << std::left << std::setw(2) << PixelConfig.outputEnable(col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << std::right << '\n';
	  }//column loop
	}

	if(opt.printMapsStr.find('h') != std::string::npos) {
          std::cout << "\nHITBUS map for channel " << (int)rxCh << "\n\n";

	  std::cout << "HITB| ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::left << std::setw(8) << col << std::right;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::setw(4) << "--------";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 80; ++col){
			  std::cout << std::left << std::setw(2) << PixelConfig.hitBusOut(col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << std::right << '\n';
	  }//column loop
	}

	if(opt.printMapsStr.find('l') != std::string::npos) {
          std::cout << "\nlarge CAP map for channel " << (int)rxCh << "\n\n";

	  std::cout << "lCAP| ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::left << std::setw(8) << col << std::right;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::setw(4) << "--------";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 80; ++col){
			  std::cout << std::left << std::setw(2) << PixelConfig.largeCapacitor(col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << std::right << '\n';
	  }//column loop
	}


	if(opt.printMapsStr.find('s') != std::string::npos) {
          std::cout << "\nsmall CAP map for channel " << (int)rxCh << "\n\n";

	  std::cout << "sCAP| ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::left << std::setw(8) << col << std::right;
	  std::cout << "\n----- ";
	  for(unsigned col=0; col < 80; col=col+4) std::cout << std::setw(4) << "--------";
	  std::cout << '\n';
	  for(unsigned row=0; row < 336; ++row){
		  std::cout << std::setw(3) << row << " | ";
		  for(unsigned col=0; col < 80; ++col){
			  std::cout << std::left << std::setw(2) << PixelConfig.smallCapacitor(col/2).getPixel(row+1, col+1);
		  }//row loop
		  std::cout << std::right << '\n';
	  }//column loop
	}
      }
    }
  }
  return 0;
}
