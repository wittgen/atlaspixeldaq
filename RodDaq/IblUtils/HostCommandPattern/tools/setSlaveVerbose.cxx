#include "DefaultClient.h"
#include "SetSlaveVerbose.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv) {
  Options opt(argc, argv, "setSlaveVerbose");

  SetSlaveVerbose cmd;

  DefaultClient client(opt);
  cmd.whichSlave = opt.getSlave();
  cmd.verbose = opt.getVerbose();
  client.run(cmd);

  return 0;
}
