// L Jeanty <laura.jeanty@cern.ch> 
#include "DefaultClient.h"
#include "EnableChannels.h"

#include <cstring>
#include <iostream>

// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {
  //0)
  Options opt(argc, argv, "enable");

  //1)
  EnableChannels enCh;

  //2)
  //todo maybe make get/set rather than this
  enCh.Channels = opt.getChannels();

  //3)  
  DefaultClient client(opt);
  //4)
  client.run(enCh);
  //5)

  //6)
  std::cout<<std::hex<<"Enabled channels with this mask: 0x"<<enCh.result.enabledChannels<<std::dec<<std::endl;
  return 0;
}
