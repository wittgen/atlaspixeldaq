
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2017-V-3
 */

#include "DefaultClient.h"
#include "ReadBocCounters.h"

#include <iostream>
#include <cstdlib>

int main(int argc,  char** argv) {

	Options opt(argc , argv, "");

	ReadBocCounters readBocCounters;

	DefaultClient client(opt);
	client.run(readBocCounters);

  BocRegisters& result = readBocCounters.result;
	for (int ch=0; ch<32; ch++) {
		std::cout << "Channel " << ch << std::endl;
		std::cout << "\t" << "decodingError = " << result.decodingError[ch] << std::endl;
		std::cout << "\t" << "frameError = " << result.frameError[ch] << std::endl;
		std::cout << "\t" << "frameCount = " << result.frameCount[ch] << std::endl;
		std::cout << "\t" << "meanOccupancy = " << result.meanOccupancy[ch] / 65535.0 << std::endl;
		std::cout << "\t" << "liveOccupancy = " << result.liveOccupancy[ch] / 65535.0 << std::endl;
	}

	return 0;

}
