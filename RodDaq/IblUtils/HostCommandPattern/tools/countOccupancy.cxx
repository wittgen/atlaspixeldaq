#include "DefaultClient.h"
#include "CountOccupancy.h"
//#include "busyOptions.ixx"

#include <iostream>


// host is client that is sending to command
// this should be there for every command (no need to change)
class MyClient: public Client {
public:
	MyClient(Options opt);
	virtual void run();
private:
	Options opt;
};

MyClient::MyClient(Options opt_arg)
: Client(opt_arg.getIp().c_str()), opt(opt_arg)
{ }

// what the client (the host) should do
// in summary, the host does this:
// 1) connect to the ppc (server)
// 2) instantiate a command
// 3) configure the command
// 4) send the command to the ppc
// 5) disconnect from server
// 6) process results
void MyClient::run() {
  
  //connect to ppc server
	if (!server.connectToServer(srvIpAddr(), srvIpPort())) {
		std::cout << "no connection to server" << std::endl;
		return;
	}
	std::cout << "Connected to server." << std::endl;


	// intatintiate command
	CountOccupancy cmd;
	cmd.sleepTime = opt.getTime();
	cmd.testInj = opt.getTestInj();
	std::cout << "Test injection will be used " << cmd.testInj << std::endl;

	// set all inputs for the injection
	cmd.row = -1;
	cmd.col = 20;
	cmd.chip = 0;
	cmd.address = 1;

	std::cout << "row:col = " << cmd.row << ":" << cmd.col << std::endl;
	std::cout << "chip = " << cmd.chip << ", address = " << cmd.address << std::endl;

	// change what is read out:
	// cmd.feMin
	// cmd.feMax
	// cmd.rowMin
	// cmd.rowMax

	// send command to ppc
	// host will wait at this step until command returns
	server.sendCommand(cmd);

	std::cout<<"...command sent"<<std::endl;

	std::cout << "Counter: " << cmd.result.counter << std::endl;
	
	std::cout << "Disconnecting from server..."<<std::endl;

	// disconnect from server after command returns
	server.disconnectFromServer();
	
	std::cout << "Done." << std::endl;

}

int main(int argc, char** argv) {

  Options o(argc, argv, "enable");
  std::cout<< "options read"<<std::endl;
  MyClient client(o);
  client.run();
  return 0;
}

