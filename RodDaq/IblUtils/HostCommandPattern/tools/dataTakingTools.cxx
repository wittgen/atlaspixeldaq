// L. Jeanty <laura.jeanty@cern.ch>
//
// A set of specific tools for data taking support
//
// for information about which actions are available
// do: ./dataTakingTools --help 
//
// always choose your action below before compiling!
// for value, address, and channels options, use command line options

#include "DataTakingTools.h"
#include "DefaultClient.h"
#include <iostream>
#include <cstring>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

int main(int argc, char** argv){

  DataTakingTools cmd;

  //  cmd.action = SetECRCounter; // ALWAYS CHOOSE AN ACTION!
  //  cmd.action = EnableFmtLink;
  // cmd.action = DisableFmtLink;
  //   cmd.action = EnableBocRx;
  //    cmd.action = SetPrmpVbpStandby;
  //  cmd.action = FeSetRunMode;
  //  cmd.action = BocResampleRxPhase;
  cmd.action = DisableBocRx;
  //  cmd.action = SetRODVetoAtECR;
  //  cmd.action = SetEfbMaskReg;

  for (int i = 1; i< argc; i++) {
    if ((strcmp(argv[i],"-h") == 0) ||  (strcmp(argv[i],"--help") == 0)) cmd.print();
  }
  
  Options options(argc , argv);

  cmd.value = options.getValue(); // value to write if relevant
  cmd.address = options.getAddress(); // address to read/write if relevant
  cmd.channels = options.getChannels(); // channels to send commands to if relevant

  //  cmd.value = SLINKEnableFTKXOff; // overwrite command line options if you want
  //  cmd.address = ;
  //  cmd.channels = 0xFFF0FFFF;

  DefaultClient client(options);
  client.run(cmd);
  
  std::cout<<"If your action had a result, the result is: 0x"<<std::hex<<(int)cmd.result.value<<std::endl;

  return 0;
}
