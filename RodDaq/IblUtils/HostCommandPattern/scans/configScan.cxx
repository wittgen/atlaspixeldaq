// L Jeanty <laura.jeanty@cern.ch>
#include "DefaultClient.h"
#include "ConfigScan.h"
#include "DataTakingTools.h"

#include <iostream>
#include <cstdlib>

// Simple way to check for an option.
#include <string>
#include <algorithm>
#include <boost/algorithm/string.hpp>

std::string get_option(char ** begin, char ** end, const std::string & option) {
  char ** itr = std::find(begin, end, option);
  if (itr != end && ++itr != end) return *itr;
  return "";
}

bool check_flag(char** begin, char** end, const std::string& flag)
{
  return std::find(begin, end, flag) != end;
}

void usage() {
  std::cout << "Options: "<<std::endl;
  std::cout << "--verbose : scan will be set to verbose mode"<<std::endl;
  std::cout << "--noVerbose : scan will be set to verbose mode (default)"<<std::endl;
  std::cout << "--dBoc : scan will readback from boc fifo and print to kermit"<<std::endl;
  std::cout << "--noBoc : scan will readback from boc fifo and print to kermit (default)"<<std::endl;
  std::cout << "--dInmem : scan will readback from inmem fifo and print to kermit"<<std::endl;
  std::cout << "--noInmem : scan will readback from inmem fifo and print to kermit (default)"<<std::endl;
  std::cout << "--rx : which rxCh to readback from on kermit? (default = 0)"<<std::endl;
  std::cout << "--sr : readback the service records (use --rx also to set channel) onto kermit"<<std::endl;
  std::cout << "--debugHist : values to use for a debug hist, specified as DataType,FIFOType,rxCh "<<std::endl;
  std::cout << "--clearDebugHists : remove all debugging hists from the scan" << std::endl;
  std::cout <<" --dcLoops : specify # of DC loops to run over (default set by console)"<<std::endl;
  std::cout <<" --ch : specify channels to run over (default set by console)"<<std::endl;
}

int main(int argc, char** argv) {

  if (argc < 2 ) {
    usage();
    return 0;
  }

  bool verbose = false;
  bool histo = true;
  bool debugBoc = false;
  bool debugInmem = false;
  bool sr = false;
  uint8_t rx = 0;
  uint32_t channels = 0;
  uint8_t dcLoops = 0;

  for (int i = 1; i < argc; i++) {
    std::string arg = argv[i];
    if (arg == "--verbose") verbose = true;
    else if (arg == "--noVerbose") verbose = false;
    else if (arg == "--dBoc") debugBoc = true;
    else if (arg == "--noBoc") debugBoc = false;
    else if (arg == "--dInmem") debugInmem = true;
    else if (arg == "--noInmem") debugInmem = false;
    else if (arg == "--sr") sr = true;
    else if (arg == "--sr") sr = true;
    else if (arg == "--dcLoops") {
      if (argc > i) {dcLoops = atoi(argv[i+1]);
	i++; 
      }
      else std::cout<<"please provide a dc loops number;"<<std::endl;
    }
    else if (arg == "--ch") {
      if (argc > i) {channels = atoi(argv[i+1]);
	i++; 
      }
      else std::cout<<"please provide the channels;"<<std::endl;
    }
    else if (arg == "--help"){usage(); return 0;}
    else if (arg == "--rx") {
      if (argc > i) {rx = atoi(argv[i+1]);
	i++; }
      else std::cout<<"please provide an rx channel; using default = 0"<<std::endl;
    }
  }

  // Additional options for debug hists:
  Fei4Data::DataType data = Fei4Data::OCC;
  Fei4Data::FIFOType fifo = Fei4Data::BOC;
  uint8_t histRx = 0xFF;
  std::string debugHist = get_option(argv, argv+argc, "--debugHist");
  if (debugHist != ""){
    std::vector<std::string> histOptions;
    boost::split(histOptions, debugHist, boost::is_any_of(",.;"));

    if (histOptions.size() != 3) {
      std::cout << "Could not understand the argument for debugHist: " << debugHist << std::endl;
      return 1;
    }

    if (histOptions[0] == "OCC") data = Fei4Data::OCC;
    else if (histOptions[0] == "TOT") data = Fei4Data::TOT;
    else {
      std::cout << "Unrecognize argument for DataType, using default OCC" << std::endl;
    }

    if (histOptions[1] == "BOC") fifo = Fei4Data::BOC;
    else if (histOptions[1] == "INMEM") fifo = Fei4Data::INMEM;
    else {
      std::cout << "Unrecognize argument for FIFOType, using default BOC" << std::endl;
    }
    
    histRx = atoi(histOptions[2].c_str());
    if (histRx > 32) {
      std::cout << "Unrecognized argument for histogram rxCh, no debug hist will be added." << std::endl;
      histRx = 0xFF;
    }
  }
  
  bool clear = check_flag(argv, argv+argc, "--clearDebugHists");

  ConfigScan cmd;
  cmd.verboseScan = verbose;
  cmd.debugReadBackBoc = debugBoc;
  cmd.debugReadBackInmem = debugInmem;
  cmd.debugRxCh = rx;
  cmd.histData = data;
  cmd.histFIFO = fifo;
  cmd.histRxCh = histRx;
  cmd.clearDebugHists = clear;
  cmd.debugReadSR = sr;
  cmd.debugDCLoops = dcLoops;
  cmd.debugChannels = channels;

  std::cout<<std::endl;
  std::cout<<"=================="<<std::endl;
  std::cout<<"Config parameters:"<<std::endl;
  std::cout<<std::endl;

  cmd.verboseScan   ? std::cout<<"scan in verbose mode"<<std::endl :  std::cout<<"scan in quiet mode"<<std::endl;
  if (cmd.debugReadSR) std::cout<<"scan will print SR for Rx: "<<(int)rx<<std::endl;
  cmd.debugReadBackBoc ? std::cout<<"scan will print onto kermit from boc fifo ch: "<<(int)rx<<std::endl :  std::cout<<"no boc fifo read back on kermit"<<std::endl;  
  cmd.debugReadBackInmem ? std::cout<<"scan will print onto kermit from inmem fifo ch: "<<(int)rx<<std::endl :  std::cout<<"no inmem fifo read back on kermit"<<std::endl;  
  if (dcLoops != 0) std::cout<<"scan will only execute "<<(int)cmd.debugDCLoops<<" DC loops"<<std::endl;
  if (channels != 0) std::cout<<"scan will run on these channels: 0x"<<std::hex<<(int)cmd.debugChannels<<std::dec<<std::endl;
  std::cout<<"=================="<<std::endl;

  std::cout<<std::endl;

  Options opt(argc, argv, "configScan");

  DefaultClient client(opt);
  client.run(cmd);

  return 0;
	
}
