#include "DefaultClient.h"
#include "IblThreshScan.h"
#include "iblSlaveCmds.h"

#include <ctime>
#include <cstring>
#include <iostream>
#include "TH2.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TColor.h"
#include "TCanvas.h"
#include "TFile.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// what the client (the host) should do
// in summary, the host does this:
// 0) getOptions if necessary
// 1) instantiate a command
// 2) configure the command
// 3) connect to the ppc i.e. server  (done automatically by DefaultClient)
// 4) send the command to the ppc
// 5) disconnect from server (done automatically by DefaultClient)
// 6) process results

int main(int argc, char** argv) {
  //0)
  Options opt(argc, argv);

  //1)
  IblThreshScan iblScan;

  //2)
  iblScan.Channels = opt.getChannels(); // which channels to scan  for now, this just sets the channel enable mask
  iblScan.DCLoop = 2; //  number of double columns per each command
  iblScan.TrigLoop = 1; // number of triggers to send
  iblScan.TrigCount = 8; // number of frames to read?
  iblScan.MaskLoop = 8; // number of mask loops
  iblScan.ToTMaskStep = 0; // how much to increase the tot for each maskstep
  iblScan.StartCol = 0; // which DC column to start at
  iblScan.ToT = 7; // true ToT (not raw)
  iblScan.DoubleColumnMode = 0; // 0 = addressed DC, 1 = every 4, 2 = every 8, 3 = a;;
  iblScan.VCalRange = 500;
  iblScan.VCal_Steps = 1;
  iblScan.readBackFromBoc = true; // read back from the boc fifo 
  iblScan.readBackFromInmem = false; // read back from the rod inmem fifo on the ppc
  iblScan.readBackFromSlave = false; // print the slave histo on the ppc
  iblScan.talkToFitFarm = false; // connect to the fit farm
  iblScan.histogrammerMode = ONLINE_OCCUPANCY; // SHORT_TOT, ONLINE_OCCUPANCY, OFFLINE_OCCUPANCY, LONG_TOT
  iblScan.setExecId(0);

  //3)
  DefaultClient client(opt);
  //4)
  client.run(iblScan);	
  //5)

  //6)
  std::string name = "ThresholdScanData_ToT";
  //	TH1F *ToTvsVCal = new TH1F(name.c_str(), name.c_str(),32,0,1024);
  TH2F * ToTHisto = new TH2F(name.c_str(), name.c_str(), 80, 0.5, 80.5, 336, 0.5, 336.5);

  if (!iblScan.result.success) {
    std::cout<<" The scan was not succesful. Probably failed in enabling channels."<<std::endl;
    return 0;
  }

  std::cout<<" The scan may have been succesful. Some results follow: "<<std::endl;
  //cout<<" size of the vector is: "<<iblScan.result.Threshold.size()<<endl;
  //int icounter=0;
  //for(std::vector<threshOcc>::iterator iii= iblScan.result.Threshold.begin();
  //    iii!=iblScan.result.Threshold.end(); iii++ ){
  //	icounter++;
  //	std::cout<<"I'm in the loop, counter value is: "<<icounter<<std::endl;		
  for (int i=0; i < 4; i++) {
    for (int j=0; j < 336; j++) {
      if (iblScan.result.ToT[i][j]){
	std::cout<<"For column "<<i+1<<", and row "<<j+1<<", the ToT is: "<<int(iblScan.result.ToT[i][j])<<std::endl;
	//ToTvsVCal->AddBinContent(icounter);
	ToTHisto->SetBinContent(i+1,j+1,iblScan.result.ToT[i][j]);
      }
    }
    //}   
  }
	
  TFile f("thresholdScanOutput.root","recreate");
  ToTHisto->Write();
  f.Close();
  std::cout << "Done." << std::endl;






  return 0;

}
