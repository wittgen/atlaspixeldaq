/**************************************************************************
 *
 * Title: Master/PIX iblModuleConfigStructures.h
 * Version: 22nd November 2011
 *
 * Description:
 * ROD Master DSP Pixel module configuration structures
 * and constant definitions.
 *
 *   These structures are just for the use to copy the Module parameters from
 *   host (IblRodPixController.cxx) to MDSP using a primitive loadModuleConfig.
 *   The structs are compact, such that least possible space is used while shifting 
 *   the information from host to mdsp.
 *   In unpackCfg() this Module parameters are then copied to the IblModuleConfig
 *   which is further used on the MDSP in case of scan processing and sending bitstreams
 *   to the FEI4.
 * 
 * Author: Nina Krieger, Nina.Krieger@cern.ch
 *
 *  *   - 22.11.2011    
 *        originally adapted from NewDsp code's moduleConfigStructures.h to IblDsp code
 *     
 **************************************************************************/
#ifndef _IBLPIXEL_CONFIG_STRUCTURES_	 /* multiple inclusion protection */
#define _IBLPIXEL_CONFIG_STRUCTURES_
#include <cstdint>
//#include "PixEnumBase.h"

/*  Up to 28 pixel modules, with gaps in module structure set where
    the corresponding data links are not connected */
// #define N_PIXEL_MODULES     28   
// #define N_PIXEL_EXTMODULES  0    /* no redundant command links */
// #define N_PIXEL_TOTMODULES  ((N_PIXEL_MODULES)+(N_PIXEL_EXTMODULES))

#define N_FEI4_ROWS 336
#define N_FEI4_COLS 80
#define N_FEI4_DC 40
#define N_FEI4_CHIPS 1  //for SCC, later switch to 2 for 2 chip modules
#define N_FEI4_PIXELS_PER_CHIP (N_FEI4_ROWS*N_FEI4_COLS)
#define N_FEI4_PIXELS (N_FEI4_PIXELS_PER_CHIP*N_FEI4_CHIPS)
#define N_FEI4_PIXEL_REGISTER_BITS 13
#define N_FEI4_GLOBAL_REGISTER_BITS 114 //take max, so the # of FEI4B GR bits
#define N_IBL_MODULES 8
#define FE_PER_MODULE 2
//already defined in iblsysParams_specific.h, but must be defined here, otherwise not seen in IblRodPixController

//must be in analogy to PixEnumBase.h
#define FEI4A  0x81
#define FEI4B  0x82

/* Default & off-ROD positions of a module's TTC fibres. The default primary
   TTC fibre location for a module is simply the module's position inside the
   structure set. */
//IBL#define DEFAULT_TTC    0x80

/* Default Link & Data Links which are turned off: */
//IBL#define DEFAULT_DATA_LINK    0x80
//IBL#define DATA_LINK_OFF        0xff

//IBL#define PIXEL_STATIC_CONFIG 0
//IBL#define PIXEL_SCAN_CONFIG   1

/************* STRUCTURE DEFINITIONS ***********/
/* A small calibration coefficient structure containing just the vcal
   to electron conversion coefficients, for fitting on the SDSPs. */
/* typedef struct { */
/* 	float vcal[4]; */
/* 	float cap[2]; */
/* 	uint8_t   capSel; */
/* 	uint8_t   unused[3]; */
/* 	uint32_t  unused1; */
/* } ChipCoeff; */

/* typedef struct { */
/* 	ChipCoeff fe[1]; */
/* } CalCoeff; */


/* typedef struct { //FE Command register */
/* 	uint32_t address;		       /\* 5 bits *\/ */
/* 	uint32_t command;            /\* 32 bits *\/ */
/* } PixelFECommand; */

/* typedef struct { //FE global register */
/* 	//This is valid for both FE-I1 & 2, since the FE-I1 is a sub-set. */
/* 	uint32_t  latency             : 8;                  */
/* 	uint32_t  dacIVDD2            : 8;                     */
/* 	uint32_t  dacIP2              : 8;                       */
/* 	uint32_t  dacID               : 8; */
	
/* 	uint32_t  dacIP               : 8;                        */
/* 	uint32_t  dacITRIMTH          : 8;                   */
/* 	uint32_t  dacIF               : 8;                        */
/* 	uint32_t  dacITH1             : 8; */
	
/* 	uint32_t  dacITH2             : 8;                      */
/* 	uint32_t  dacIL               : 8;                        */
/* 	uint32_t  dacIL2              : 8;                       */
/* 	uint32_t  dacITRIMIF          : 8;  */
	
/* 	uint32_t  dacSpare            : 8;                         */
/* 	uint32_t  threshTOTMinimum    : 8;             */
/* 	uint32_t  threshTOTDouble     : 8;              */
/* 	uint32_t  hitbusScaler        : 8; */
	
/* 	uint32_t  capMeasure          : 6;  */
/* 	uint32_t  gdac                : 5;        */
/* 	uint32_t  selfWidth           : 4; */
/* 	uint32_t  selfLatency         : 4; */
/* 	uint32_t  muxTestPixel        : 2; */
/* 	uint32_t  spare               : 2; */
/* 	uint32_t  aregTrim            : 2; */
/* 	uint32_t  aregMeas            : 2; */
/* 	uint32_t  dregTrim            : 2; */
/* 	uint32_t  dregMeas            : 2; */
/* 	uint32_t  parity              : 1;                 */
	
/* 	uint32_t  dacMonLeakADC       : 9; */
/* 	uint32_t  dacVCAL             : 10; /\* extended to 10 bit for FE2/3 *\/           */
/* 	uint32_t  widthSelfTrigger    : 4;         */
/* 	uint32_t  muxDO               : 4; /\* note: dynamically defined *\/                       */
/* 	uint32_t  muxMonHit           : 4; */
/* 	uint32_t  muxEOC              : 2;                       */
	
/* 	uint32_t  frequencyCEU        : 2;                 */
/* 	uint32_t  modeTOTThresh       : 2;                */
/* 	uint32_t  enableTimestamp     : 1;              */
/* 	uint32_t  enableSelfTrigger   : 1;         */
/* 	uint32_t  enableHitParity     : 1;                     */
/* 	uint32_t  monMonLeakADC       : 1;             */
/* 	uint32_t  monADCRef           : 1;                 */
/* 	uint32_t  enableMonLeak       : 1;             */
/* 	uint32_t  statusMonLeak       : 1;             */
/* 	uint32_t  enableCapTest       : 1;                 */
/* 	uint32_t  enableBuffer        : 1;                  */
/* 	uint32_t  enableVcalMeasure   : 1;             */
/* 	uint32_t  enableLeakMeasure   : 1;             */
/* 	uint32_t  enableBufferBoost   : 1;             */
/* 	uint32_t  enableCP8           : 1;                     */
/* 	uint32_t  monIVDD2            : 1;                      */
/* 	uint32_t  monID               : 1;                         */
/* 	uint32_t  enableCP7           : 1;                     */
/* 	uint32_t  monIP2              : 1;                        */
/* 	uint32_t  monIP               : 1;                         */
/* 	uint32_t  enableCP6           : 1;                     */
/* 	uint32_t  monITRIMTH          : 1;                    */
/* 	uint32_t  monIF               : 1;                         */
/* 	uint32_t  enableCP5           : 1;                     */
/* 	uint32_t  monITRIMIF          : 1;                    */
/* 	uint32_t  monVCAL             : 1;                       */
/* 	uint32_t  enableCP4           : 1;                     */
/* 	uint32_t  enableCinjHigh      : 1;                */
/* 	uint32_t  enableExternal      : 1;                */
/* 	uint32_t  enableTestAnalogRef : 1; */
	
/* 	uint32_t  enableDigital       : 1;                 */
/* 	uint32_t  enableCP3           : 1;                     */
/* 	uint32_t  monITH1             : 1;                       */
/* 	uint32_t  monITH2             : 1;                       */
/* 	uint32_t  enableCP2           : 1;                     */
/* 	uint32_t  monIL               : 1;                         */
/* 	uint32_t  monIL2              : 1;                        */
/* 	uint32_t  enableCP1           : 1;                                  */
/* 	uint32_t  enableCP0           : 1;    */
/* 	uint32_t  enableHitbus        : 1;                  */
/* 	uint32_t  monSpare            : 1;                      */
/* 	uint32_t  enableAregMeas      : 1; */
/* 	uint32_t  enableAreg          : 1; */
/* 	uint32_t  enableLvdsRegMeas   : 1; */
/* 	uint32_t  enableDregMeas      : 1; */
/* 	uint32_t  enableTune          : 1; */
/* 	uint32_t  enableBiasComp      : 1; */
/* 	uint32_t  enableIpMonitor     : 1; */
/* } PixelFEGlobal;				      */
							    

/* typedef struct { //Pixel-level control bits */
/* 	uint32_t maskEnable[5][2]; /\* 32 bits, one bit per pixel thus 5 words per column *\/ */
/* 	uint32_t maskSelect[5][2]; */
/* 	uint32_t maskPreamp[5][2]; */
/* 	uint32_t maskHitbus[5][2]; */
/* } PixelFEMasks;   */
   

/* typedef struct { //Trim DACs: */
/* 	uint8_t dacThresholdTrim[2][2];  /\*: 5; Currently 5 bits per pixel, will change in next iteration of FE *\/ */
/* 	uint8_t  dacFeedbackTrim[2][2];  /\*: 5;                 ""                                              *\/ */
/* } PixelFETrims;  */


/* typedef struct {  */
/* /\* Sub-structure for calibration of injection-capacitors, VCAL-DAC and */
/*    leakage current measurement *\/ */
/* 	float cinjLo; */
/* 	float cinjHi; */
/*   //	float vcalCoeff[4]; */
/* 	float chargeCoeffClo;   //dpsf: ? */
/* 	float chargeCoeffChi; */
/* 	float chargeOffsetClo; */
/* 	float chargeOffsetChi; */
/* 	float monleakCoeff; */
/* 	/\* need MCC time calibration also //dpsf: ? *\/ */
/* } PixelFECalib;  */


/* typedef struct { //MCC registers */
/*    uint16_t regCSR;                   */
/*    uint16_t regLV1;                   */
/*    uint16_t regFEEN;				  */
/*    uint16_t regWFE;					  */

/*    uint16_t regWMCC; 				 */
/*    uint16_t regCNT;					  */
/*    uint16_t regCAL;					  */
/*    uint16_t regPEF;		 */

/*    uint16_t regWBITD; */
/*    uint16_t regWRECD; */
/*    uint16_t regSBSR; */

/*    /\* Strobe duration is a virtual register (shared with CNT); when preparing the */
/*       modules for data this value is substituted for the count. *\/ */
/*    uint16_t regSTR; */
/* } PixelMCCRegisters; */

/* typedef struct { //FE level parameters */
/* 	uint32_t FEIndex; 	 */
/*   //	PixelFECommand FECommand; //NK:not used in code */
/* 	PixelFEGlobal FEGlobal; */
/* 	PixelFEMasks FEMasks; */
/* 	PixelFETrims FETrims; */
/*   //	PixelFECalib FECalib; //NKtodo: used in iblGroup to fill histogramOptions */
/* } PixelFEConfig; */

/* typedef struct { */
/* 	uint8_t  tdac, prevTdac; */
/* 	uint8_t  fdac, prevFdac; */
/* } PixelTrimScanData; */

/* typedef struct { */
/* 	/\* FE module-level options: *\/ */
/* 	uint16_t maskEnableFEConfig; /\* 16 bits, one per FE *\/ */
/* 	uint16_t maskEnableFEScan; */
/* 	uint16_t maskEnableFEDacs; */
/* 	uint8_t feFlavour; */
/* 	uint8_t mccFlavour; */
	
/* 	/\* FE configurations: *\/ */
/* 	PixelFEConfig FEConfig[1+1]; */
	
/* 	/\* MCC configuration *\/ */
/* 	PixelMCCRegisters MCCRegisters; */
	
/* 	/\* The current (uniform) value for the DACs of each pixel during a DAC scan *\/ */
/* 	PixelTrimScanData  trimScanData; */
/* 	char  idStr[128]; /\* Module identification string *\/ */
/* 	char    tag[128]; /\* Module configuration tag *\/ */
/* 	uint32_t  revision; /\* Module configuration revision *\/ */

/* 	uint8_t present;    /\* Module is physically present. Does not need setting */
/* 	                     externally; handled by the Master DSP. *\/ */
	
/* 	uint8_t active;     /\* 1 -> participates in scans *\/ */
/* 	                  /\* 0 -> registers unchanged during scanning *\/ */
/* 	uint8_t moduleIdx;  /\* Copy of the module's index for access from a pointer *\/ */
/* 	uint8_t groupId;    /\* The ID of the module's group. This is used to indicate */
/* 	                     which slave DSP will receive the module's data (if group */
/* 	                     based distribution is set), and also to allow different */
/* 	                     module groups to be triggered independently (for */
/* 	                     cross-talk studies). valid range: [0,7] *\/ */
/* 	uint8_t pTTC;       /\* primary TX channel  *\/ */
/* 	uint8_t rx;         /\* data link used by module *\/ */
/* //	uint8_t unused1[2]; */
	
/* //	uint32_t unused2[0x653]; /\* align module structures on convenient boundary *\/ */

/* } PixelModuleConfig; /\* declare N_PIXEL_CONFIG_SETS structure for each of N_PIXEL_MODULES *\/ */



/* FEI4 config starts*/


//have 16 bit package order from FEI4B and add FEI4A 
//values just existing for FEI4B(FEI4A) are marked with _B(_A) 
typedef struct {
  uint32_t  spareGR0_B         :16;
  uint32_t  spareGR1_B         :7;
  uint32_t  smallHitErase_B    :1; //not exists for FEI4A
  uint32_t  eventLimit_B       :8; //not exists for FEI4A

  uint32_t  trigger_Count      :4;
  uint32_t  conf_Addr_Enable   :1;
  uint32_t  cfgSpare2          :11; 
  uint32_t  errMask0           :16; //ServiceRecords 14,15 not used for FEI4A
  
  uint32_t  errMask1           :16; //Service Records 16 not used for FEI4A
  uint32_t  prmpVbp_R          :8; 
  uint32_t  bufVgOpAmp_B       :8; //not exists for FEI4A

  uint32_t  spareGR6_B         :8;//not exists for FEI4A
  uint32_t  prmpVbp            :8;
  uint32_t  tdacVbp            :8; 
  uint32_t  disVbn             :8; 

  uint32_t  amp2Vbn            :8; 
  uint32_t  amp2VbpFol         :8; 
  uint32_t  spareGR9_B         :8; //not exists for FEI4A
  uint32_t  amp2Vbp            :8; 
  
  uint32_t  fdacVbn            :8; 
  uint32_t  amp2Vbpff          :8; 
  uint32_t  prmpVbnFol         :8; 
  uint32_t  prmpVbp_L          :8; 

  uint32_t  prmpVbpf           :8; 
  uint32_t  prmpVbnLCC         :8; 
  uint32_t  shiftRegMS0        :1; 
  uint32_t  shiftRegMS1        :1; 
  uint32_t  pixelStrobe0_12    :13;
/*   uint32_t  pixelStrobe0       :1;  */
/*   uint32_t  pixelStrobe1       :1;  */
/*   uint32_t  pixelStrobe2       :1;  */
/*   uint32_t  pixelStrobe3       :1;  */
/*   uint32_t  pixelStrobe4       :1;  */
/*   uint32_t  pixelStrobe5       :1;  */
/*   uint32_t  pixelStrobe6       :1;  */
/*   uint32_t  pixelStrobe7       :1;  */
/*   uint32_t  pixelStrobe8       :1;  */
/*   uint32_t  pixelStrobe9       :1;  */
/*   uint32_t  pixelStrobe10      :1;  */
/*   uint32_t  pixelStrobe11      :1;  */
/*   uint32_t  pixelStrobe12      :1;  */
  uint32_t  spareGR13          :1; 

  uint32_t  lvdsDrvIref        :8; 
  uint32_t  gAdcCompBias_B     :8; //not exists for FEI4A
  uint32_t  pllIbias           :8; 
  uint32_t  lvdsDrvVos         :8; 

  uint32_t  tempSensIbias      :8; 
  uint32_t  pllIcp             :8; 
  uint32_t  spareGR17          :8; 
  uint32_t  plsrIDACRamp       :8; 

  uint32_t  vrefDigTune_B      :8; //not exists for FEI4A
  uint32_t  plsrVgOpAmp        :8; 
  uint32_t  plsrDACbias        :8; 
  uint32_t  vRefAnTune_B       :8; //not exists for FEI4A

  uint32_t  vthinCoarse        :8; 
  uint32_t  vthinFine          :8; 
  uint32_t  spareGR21          :3; 
  uint32_t  hitLd              :1; 
  uint32_t  dinjOverride       :1; 
  uint32_t  digHitIn           :1; 
  uint32_t  plsrDAC            :10; 
  
  uint32_t  spareGR22_2        :6; 
  uint32_t  colPmode           :2; 
  uint32_t  colprAddr          :6; 
  uint32_t  spareGR22_1        :2; 
  uint32_t  disColCfg0         :1; 
  uint32_t  disColCfg1         :1; 
  uint32_t  disColCfg2         :1; 
  uint32_t  disColCfg3         :1; 
  uint32_t  disColCfg4         :1; 
  uint32_t  disColCfg5         :1; 
  uint32_t  disColCfg6         :1; 
  uint32_t  disColCfg7         :1; 
  uint32_t  disColCfg8         :1; 
  uint32_t  disColCfg9         :1; 
  uint32_t  disColCfg10        :1; 
  uint32_t  disColCfg11        :1; 
  uint32_t  disColCfg12        :1; 
  uint32_t  disColCfg13        :1; 
  uint32_t  disColCfg14        :1; 
  uint32_t  disColCfg15        :1; 


  uint32_t  disColCfg16        :1; 
  uint32_t  disColCfg17        :1; 
  uint32_t  disColCfg18        :1; 
  uint32_t  disColCfg19        :1; 
  uint32_t  disColCfg20        :1; 
  uint32_t  disColCfg21        :1; 
  uint32_t  disColCfg22        :1; 
  uint32_t  disColCfg23        :1; 
  uint32_t  disColCfg24        :1; 
  uint32_t  disColCfg25        :1; 
  uint32_t  disColCfg26        :1; 
  uint32_t  disColCfg27        :1; 
  uint32_t  disColCfg28        :1; 
  uint32_t  disColCfg29        :1; 
  uint32_t  disColCfg30        :1; 
  uint32_t  disColCfg31        :1; 
  uint32_t  trigLat            :8;  //FEI4A:"LatCnfg"
  uint32_t  disColCfg32        :1; 
  uint32_t  disColCfg33        :1; 
  uint32_t  disColCfg34        :1; 
  uint32_t  disColCfg35        :1; 
  uint32_t  disColCfg36        :1; 
  uint32_t  disColCfg37        :1; 
  uint32_t  disColCfg38        :1; 
  uint32_t  disColCfg39        :1; 
  
  //  uint32_t  cmdCnt0_12         :13; 
  uint32_t  cmdCnt             :14; 
  uint32_t  stopModeCfg        :1; 
  uint32_t  hitDiscCfg         :2; 
  uint32_t  pllEn              :1; 
  uint32_t  efuseSenseEn       :1; 
  uint32_t  stopClkPulse       :1; 
  uint32_t  readErrReq         :1; 
  uint32_t  spareGR27_1_B      :1; //not exists for FEI4A
  uint32_t  gDacEnStart_B      :1; //not exists for FEI4A
  uint32_t  srReadBack_B       :1; //not exists for FEI4A
  uint32_t  spareGR27_2_B      :3; 
  uint32_t  hitOr              :1; 
  uint32_t  calEn              :1; //FEI4A "Dig_inj"
  uint32_t  srClrEn            :1; 
  uint32_t  latchEn            :1;
  uint32_t  srClkEn            :1; //FEI4A "FE_clk_pulse"
  //uint32_t  cmdCnt13           :1;

  
  uint32_t  lvdsDrvSet06       :1; 
  uint32_t  spareGR28          :5;
  uint32_t  en40M              :1; 
  uint32_t  en80M              :1;
  uint32_t  clk1               :3; 
  uint32_t  clk0               :3; 
  //uint32_t  clk1_s0            :1; 
  //uint32_t  clk1_s1            :1;
  //uint32_t  clk1_s2            :1; 
  //uint32_t  clk0_s0            :1;
  //uint32_t  clk0_s1            :1; 
  //uint32_t  clk0_s2            :1;
  uint32_t  en160M             :1; 
  uint32_t  en320M             :1;
  uint32_t  spareGR29_1        :2; 
  uint32_t  no8b10b            :1;
  uint32_t  clk2outCfg         :1;
  uint32_t  emptyRecordCfg     :8;
  uint32_t  spareGR29_2        :1;
  uint32_t  lvdsDrvEn          :1; 
  uint32_t  lvdsDrvSet30       :1;
  uint32_t  lvdsDrvSet12       :1;

  
  uint32_t  tmpSensD0_B        :1; //not exists for FEI4A, no GR address 30 for FEI4A
  uint32_t  tmpSensD1_B        :1; //not exists for FEI4A
  uint32_t  tmpSensDis_B       :1; //not exists for FEI4A
  uint32_t  iLeakRange_B       :1; //not exists for FEI4A
  uint32_t  spareGR30_B        :12;//not exists for FEI4A
  uint32_t  plsrRiseUpTau      :3; 
  uint32_t  plsrPwr            :1;
  uint32_t  plsrDelay          :6; 
  uint32_t  extDigCalSW        :1;
  uint32_t  extAnaCalSW        :1; 
  uint32_t  spareGR31_B        :1; //not exists for FEI4A
  uint32_t  gAdcSel_B          :3; //not exists for FEI4A

  //EFUSE
  uint32_t  efuse0             :1;
  uint32_t  efuse1             :1;
  uint32_t  efuse2             :1;
  uint32_t  efuse3             :1;
  uint32_t  efuse4             :1;
  uint32_t  efuse5             :1;
  uint32_t  efuse6             :1;
  uint32_t  efuse7             :1;
  uint32_t  efuse8             :1;
  uint32_t  efuse9             :1;
  uint32_t  efuse10            :1;
  uint32_t  efuse11            :1;
  uint32_t  efuse12            :1;
  uint32_t  efuse13            :1;
  uint32_t  efuse14            :1;
  uint32_t  efuse15            :1;
  uint32_t  efuse16            :1;
  uint32_t  efuse17            :1;
  uint32_t  efuse18            :1;
  uint32_t  efuse19            :1;
  uint32_t  efuse20            :1;
  uint32_t  efuse21            :1;
  uint32_t  efuse22            :1;
  uint32_t  efuse23            :1;
  uint32_t  efuse24            :1;
  uint32_t  efuse25            :1;
  uint32_t  efuse26            :1;
  uint32_t  efuse27            :1;
  uint32_t  efuse28            :1;
  uint32_t  efuse29            :1;
  uint32_t  efuse30            :1;
  uint32_t  efuse31            :1;

 
  uint32_t  efuse32            :1;
  uint32_t  efuse33            :1;
  uint32_t  efuse34            :1;
  uint32_t  efuse35            :1;
  uint32_t  efuse36            :1;
  uint32_t  efuse37            :1;
  uint32_t  efuse38            :1;
  uint32_t  efuse39            :1;
  uint32_t  spareGR34_1_B      :3; //not exists for FEI4A
  uint32_t  prmpVbpMsbEn_B     :1; //not exists for FEI4A
  uint32_t  spareGR34_2_B      :4; //not exists for FEI4A
  uint32_t  efuseSerialNo      :16;
  
  //FEI4A specific registers, neglect GRAddr 0 and 1 for A-flavour
  uint32_t  spareGR18_A        :16;
  uint32_t  spareGR19_A        :16; 


  uint32_t  vthin_A            :8;
  uint32_t  disVbnCPPM_A       :8; 
  uint32_t  prmVbpT_A          :8;
  uint32_t  bonnDac_A          :8; 
 
  uint32_t  padding            :14; //fill up to a complete 32 bit word
  uint32_t  spareGR27_A        :5;   
  uint32_t  spareGR31_A        :4; 
  uint32_t  efuseVref_A        :4;
  uint32_t  efuseCref_A        :4;
  uint32_t  rdSkip_A           :1;

 

} FEI4GlobalRegisterTemp;

typedef struct { //Pixel-level control bits
  uint32_t maskEnable[11][N_FEI4_COLS]; /* 32 bits, one bit per pixel thus 11 words per column */
  uint32_t maskCAP1[11][N_FEI4_COLS];  /*  336 pixel per colum => 10.5 words per column*/
  uint32_t maskCAP0[11][N_FEI4_COLS];
  uint32_t maskILeak[11][N_FEI4_COLS]; 
} FEI4Masks;

typedef struct { //Trim DACs:
	uint8_t  dacThresholdTrim[N_FEI4_ROWS][N_FEI4_COLS];  /*: 5; [0]=MSB or in complete Pixel Segment (with all PR values) [1]=MSB      */
	uint8_t  dacFeedbackTrim[N_FEI4_ROWS][N_FEI4_COLS];  /*: 4;  [3]=MSB                                                  [12]=MSB      */
} FEI4Trims;                                   /* for FDAC: uint8_t can keep 2 such values!!*/

/* //just needed for analysing scans, histo communication sent to "slave" via Mdsp? or directly from host */
/* typedef struct{ */
/*   uint32_t  CInjLo;             //Charge injection CAP1 */
/*   uint32_t  CInjMed;            //Charge injection CAP0 */
/*   uint32_t  CInjHi;             //Charge injection CAP0+CAP1 */
/*   uint32_t  VcalGradient0;      //VCAL gradient */
/*   uint32_t  VcalGradient1;      //VCAL gradient */
/*   uint32_t  VcalGradient2;      //VCAL gradient */
/*   uint32_t  VcalGradient3;      //VCAL gradient */
/*   //uint32_t  OffsetCorrection;   //Internal injection offset correction */
/*   //uint32_t  DelayCalib;         //Calibration of PlsrDelay to ns */
/* } FEI4Calib; */

typedef struct { 
/* Sub-structure for calibration of injection-capacitors, VCAL-DAC and
   leakage current measurement */
	float cinjLo;        //Charge injection CAP1
	float cinjHi;        //Charge injection CAP0+CAP1
	float vcalCoeff[4];  //VCAL gradients
  //float chargeCoeffClo;   //dpsf: ?
  //float chargeCoeffChi;
  //float chargeOffsetClo;  //NKtodo
  //float chargeOffsetChi;  //NKtodo
  //float monleakCoeff;     //NKtodo
} FEI4Calib; 

typedef struct{
  FEI4GlobalRegisterTemp  fei4GlobRegTemp; 
  FEI4Masks               fei4Masks; //PixelRegister part1
  FEI4Trims               fei4Trims; //PixelRegister part2
  FEI4Calib               fei4Calib;  
  uint32_t                  feIndex;
  uint8_t                   address  :3;   //FE geographical address
  uint8_t                   cfgEn    :1;     //FE configuration enable
  uint8_t                   scanEn   :1;    //FE scan/readout enable
  uint8_t                   dacsEn   :1;    //FE DACs enable
  
} FEI4ConfigTemp;



//just insert multiples of 32 bit words in typedef structs, otherwise your code won't work!! 
typedef struct{
  char       idStr[128];  /* Module identification string */
  char       tag[128];    /* Module configuration tag */
  uint32_t     revision;    /* Module configuration revision */

  uint8_t      active;      /* 1 -> participates in scans */
	                   /* 0 -> registers unchanged during scanning */
  uint8_t      moduleIdx;  /* Copy of the module's index for access from a pointer */
  uint8_t      groupId;    /* groupId might be useful for histogramming selection in Spartan6s */
  uint8_t      feFlavour;     //could be used to distinguish between A and B FEI4 type

  uint8_t      pTTC;       /* primary TX channel  */
  uint8_t      rx;         /* data link used by ibl module */
  uint8_t      present;     /*Module is physically present. Does not need setting
			       externally; handled by the Master DSP. */ //NK:todo not used yet,used for boundary condition 
  uint8_t      nFe;          /*number of FE per module to differ between 3D and Planar*/
  FEI4ConfigTemp fei4ConfigTemp[N_FEI4_CHIPS+1];//NKtodo: why +1 ?
  //must also be a multiple of 32 bit words!
  
} IblModuleConfigTemp;



/* FEI4 config ends*/



#endif   /* multiple inclusion protection */


