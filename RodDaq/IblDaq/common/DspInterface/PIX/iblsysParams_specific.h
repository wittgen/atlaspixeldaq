/* Common Header */

#ifndef IBLSYSPARAMS_SPECIFIC_H
#define IBLSYSPARAMS_SPECIFIC_H

#define NMODULES_PER_SLAVE 7
#define NMODULES_PER_ROD 26

#define SIZEOF_NAME 32

//IBLchange:remove
/* #define N_ROWS 160 */
/* #define N_COLS 18 */
/* #define N_CHIPS 16 */
/* #define N_PIXELS_PER_CHIP (N_ROWS*N_COLS) */
/* #define N_PIXELS (N_PIXELS_PER_CHIP*N_CHIPS) */
/* #define N_PIXEL_REGISTER_BITS 14 */
/* #define N_MODULES 32 */

//NKtodo: is this file necessary at all, try to move everything to iblModuleConfigStructures?
/* #define N_FEI4_ROWS 336 */
/* #define N_FEI4_COLS 80 */
/* //#define N_FEI4_CHIPS 1 //later two chips, for now written for SCC, already declared in iblModuleConfigStructures */
/* #define N_FEI4_PIXELS_PER_CHIP (NFEI4_ROWS*NFEI4_COLS) */
/* #define N_FEI4_PIXELS (NFEI4_PIXELS_PER_CHIP*NFEI4_CHIPS) */
/* #define N_FEI4_PIXEL_REGISTER_BITS 13 */
/* #define N_IBL_MODULES 8 */

#endif
