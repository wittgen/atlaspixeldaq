#ifndef IBLSLAVECMDS_H_
#define IBLSLAVECMDS_H_

#include <stdint.h>


// oroginal FE-I3 definitions
//#define MAGIC_SLAVE_LOCATION 0x0200
//#define I_AM_ALIVE 0xC0FFEE
//#define STILLBORN_BOOT

// command definitions
#define SLV_CMD_LOCATION    0
#define SLV_CMD_BOOT        0xdeaddead  // boot marker from master
#define SLV_CMD_ALIVE       0x00c0ffee  // alive marker from slave
#define SLV_CMD_IDLE        0xbeef	// if command completed
#define SLV_CMD_BUSY        0x001	// working on command
#define SLV_CMD_ACK         0x002	// command finished and data available. Set by slave only! Host resets to IDLE after taking data
#define SLV_CMD_VERBOSE     0x010   // following word: rdwr structure (valid word: data. 1=verbose, 0=silent)
#define SLV_CMD_WRITE       0x011	// following data: rdwr structure (valid words: address, count, data)
#define SLV_CMD_READ        0x012	// following data: rdwr structure (valid words: address, count). Count is number of words. Returns data from "data"
#define SLV_CMD_CRC	       	0x013	// following data: rdwr structure (valid word: address, count). Count is number of words. return endian flag and crc
#define SLV_CMD_START       0x014	// following data: rdwr structure (valid word: address)
#define SLV_CMD_ID_SET      0x015	// following data: rdwr structure (valid word: data)
#define SLV_CMD_ID_GET      0x016	// following data: rdwr structure (valid word: data)
#define SLV_CMD_HIST_CFG_SET   0x020   // following data: histo cfg structure with 2 histo unit cfg elements
#define SLV_CMD_HIST_CFG_GET   0x021   // returns data: histo cfg structure with 2 histo unit cfg elements
#define SLV_CMD_HIST_CMPLT  0x022   // following data: hist termination structure
#define SLV_CMD_HIST_TEST   0x023   // following data: rdwr structure (valid words: address, count, data). Selects histo with addr. write histogrammer test data
#define SLV_CMD_HIST_READ   0x024   // following data: rdwr structure (valid words: address, count, data). Count is number of words. Selects histo with data. Addr is pixNum. Returns data from "data"
#define SLV_CMD_BOC_TEST    0x025   // following data: rdwr structure (valid words: address, count, data). write boc test data
#define SLV_CMD_HIST_ABORT  0x026   // abort the histogramming (reset histogrammers, no readout phase)

#define SLV_CMD_NET_CFG_SET     0x030   // following data: net cfg structure
#define SLV_CMD_NET_CFG_GET     0x031   // return data: net cfg structure
#define SLV_CMD_GET_HASHES  0x040   // return firmware and software git hashes
#define SLV_CMD_CLEAR_CB    0x041   // clears the circular buffer and restarts the network
#define SLV_CMD_CTL         0x099   // following data: rdwr structure (valid word: data. meaning tbd)
#define SLV_CMD_STAT        0x09a   // following data: rdwr structure (valid word: data is status type). Returns data from "data"

// status specifiers
// status types
#define SLV_STAT_TYPE_STD   0x01    // default status
#define SLV_STAT_TYPE_NET   0x02    // network status
#define SLV_STAT_TYPE_FIT   0x03    // status of connection to fit server
#define SLV_STAT_TYPE_HIST  0x04    // hisotgrammer status
// status codes
#define SLV_STAT_OK         SLV_CMD_IDLE     // no errors, idle
#define SLV_STAT_BUSY       SLV_CMD_BUSY     // no errors, busy or active. for histogramming: active
#define SLV_STAT_DATA       SLV_CMD_ACK     // no errors, data available. for histogramming: transferring, if network enabled
#define SLV_STAT_ERROR      0xffffffff  // something wrong
// program types
#define SLV_TYPE_LOADER     0x10ad  // boot loader
#define SLV_TYPE_PROG       0xface  // real slave program

// endianness
#define ENDIAN_FLAG 0x12345678


// status structures
/// program status
typedef struct {
    uint32_t progType;        // program type: loader, slave, ???
    uint32_t progVer;         // program version, > 0
    uint32_t status;          // actual status word, or size of status words following. Depends on statType
} iblSlvStat;

// command structures
///set slave network configuration.
typedef struct {
	uint32_t cmd;         // command code
	uint32_t localMac[2];   // MSB 2 bytes are 0, followed by 6 byte MAC address
	uint32_t localIp;    // e.g. 192.168.1.100
	uint32_t maskIp;    	// e.g. 192.168.1.100
	uint32_t gwIp;    	// e.g. 192.168.1.100

	/* Target IP addresses (e.g. 192.168.1.200) and ports (e.g. 5001) for histogramming units A and B. */
    uint32_t targetIpA;
    uint32_t targetPortA;
    uint32_t targetIpB;
    uint32_t targetPortB;
} IblSlvNetCfg;

///read/write commands
typedef struct {
	uint32_t cmd;         // command code
	uint32_t addr;
	uint32_t count;
	uint32_t data;        // first data word. more may follow
} IblSlvRdWr;

///set histogrammer configuration.

typedef struct {
    uint32_t nChips;      // Number of Chips connected to the histograming unit
    uint32_t enable;      // 1 = enabled
    uint32_t type;        // Type of operation mode (enum histoOpMode)
    uint32_t maskStep;    // Histogram parameter: row selection (enum rowMaskMode)
    /* Set the injection pattern for odd and even rows.
     * This way the histogrammer can ignore unwanted hits during mask stepping. 
     * Values are 0: to disable, 1-8: to set filter. */
    uint32_t mStepEven;   // Set to 0 to disable.
    uint32_t mStepOdd;
    uint32_t chipSel;     // Number of active chips
                        // 0: single chip. (force chip = 0)
                        // 1: all nChips connected
    uint32_t expectedOccValue;    // number of expected triggers. needed for SHORT_TOT mode
    uint32_t addrRange;   // Address range (=size) of histogram
                        // 0: single chip range (< 32k)
                        // 1: full range (< 1M)
    uint32_t scanId;      // Identifies the current scan (remote use only)
    uint32_t binId;       // Identifies the current bin. (remote use only)
    uint32_t maskId;      // Identifies the current mask step. (remote use only)
} IblSlvHistUnitCfg;


/* Communicate the last DMA transfer details after executing SLV_CMD_HIST_CMPLT. */
typedef struct {
    uint32_t address[2];   // contains the offset to the RAM base address
    uint32_t size[2];      // number of *bytes* in the transfer (0 if deactivated)
} IblSlvDmaTransfer;

/* these enum should be used instead of hardcoding... */
enum histoOpMode {
    ONLINE_OCCUPANCY = 0,
    OFFLINE_OCCUPANCY = 1, 
    SHORT_TOT = 2,
    LONG_TOT = 3
};

enum rowMaskMode {
    ROWMASKNONE = 0,
    ROWMASK2 = 1,
    ROWMASK4 = 2,
    ROWMASK8 = 3,
    ROWMASK16 = 4, // DO NOT USE (until implemented in firmware)
    ROWMASK32 = 5, //  Only for FE-I3
    ROWMASK64 = 6  //  Only for FE-I3
};

typedef struct {
	uint32_t cmd;
	IblSlvHistUnitCfg cfg[2];  // configuration for 2 units
} IblSlvHistCfg;

/* Terminate histogramming and initiate network transfer on slave. */
typedef struct {
    uint32_t cmd;
} IblSlvHistTerm;

// contains both firmware and software git hashes
typedef struct {
    uint32_t firmwareHash[5];
    uint32_t softwareHash[5];
} IblSlvHashes;

// Flags passed (usually from PPC) via slave command "data" to control testHistogramming() function with its internal "status" variable
// Note:  The HISTO_TEST_SETUP* series can be used as an "injectionType driver" (see the injectionType argument of the fillDummyHisto function)
enum TestHistoStatus {
    HISTO_TEST_CMPLT = 0,		//  Compelte (but limited) internal test using single invocation of testHistogramming

    HISTO_TEST_SETUP = 1,		//  Setup for INTERNAL injection test using histogrammer (but NOT slave formatter)
    HISTO_TEST_INPROGRESS = 2,		//  Send data for INTERNAL injection test using histogrammer (but NOT slave formatter)

    HISTO_TEST_FINISH = 3,		//  Finish INTERNAL or EXTERNAL injection tests involving histogrammer, by transfering stored results to FitServer

    HISTO_TEST_SETUP_NOHISTO = 4,	//  Setup for INTERNAL injection test without touching histogrammer
    HISTO_TEST_INPROGRESS_NOHISTO = 5,	//  Send data for INTERNAL injection test without touching histogrammer
    HISTO_TEST_FINISH_NOHISTO = 6,	//  Finish INTERNAL injection test not involving histogrammer, by transfering stored results to FitServer

    HISTO_TEST_SETUP_EXT = 7,		//  Setup for EXTERNAL injection test involving BOTH histogrammer AND slave formatter
    HISTO_TEST_INPROGRESS_EXT = 8	//  Send data for EXTERNAL injection test involving both histogrammer AND slave formatter
};

// types of dummy scans supported by slave sotware
enum DummyScanType {
     CHECKERBOARD = 0,
     PIXELADDRESS = 1,
     THR_SCURVE_FIT = 2
};

#endif // IBLSLAVECMDS_H_
