/* Common Header */

#ifndef IBLSCAN_OPTIONS_2_H
#define IBLSCAN_OPTIONS_2_H

#include "iblSysParams.h"
#include <cstdint>
#include "scanEnums.h"

/* several histogram options can be enabled at once, using a bit mask */
/* the bining can be set for each histogram according to need, a
   default setting provides a sensible choice   */

typedef struct {
  uint32_t optionMask;
  enum BinSize binSize;
} IblHistoOptions;

#define MAXGROUPS 8
typedef struct {
  uint16_t groupActionMask[MAXGROUPS];
  uint8_t  superGroupAMask;   
  uint8_t  superGroupBMask;
  uint16_t spare;   
} IblGroupOptions;

typedef struct {
  enum TriggMode triggerMode;
  uint32_t nEvents;
  uint8_t  nL1AperEvent;
  uint8_t  Lvl1_Latency;
  uint16_t strobeDuration;
  uint32_t strobeMCCDelay;    
  uint32_t strobeMCCDelayRange;    
  uint32_t CalL1ADelay;
  uint32_t eventInterval;
  uint32_t superGroupDelay;
  uint8_t  superGroupAnL1A[2];
  uint8_t  superGroupBnL1A[2];
  uint16_t trigABDelay[2];
  uint32_t vcal_charge;
  uint32_t optionsMask; /* this is TriggOptions */
  uint32_t customTriggerSequence[10];
} IblTriggerOptions;

/* nkformer typedef struct { */
/*   /\* global register parameters which need setting: *\/ */
/*   uint8_t  phi;                 /\* Column readout frequency, @FEI4: not necessary *\/ */
/*   uint8_t  totThresholdMode;    /\* Sets TOT threshold mode *\/ */
/*   uint8_t  totMinimum;          /\* TOT minimum *\/ */
/*   uint8_t  totTwalk;            /\* TOT double-hit for digital timewalk correction *\/ */
/*   uint8_t  totLeMode;           /\* TOT or timestamp leading edge option *\/ */
/*   uint8_t  hitbus; */
  
/* } PixelScanFEI4; /\* FE specific options during scans *\/ */

typedef struct {
  enum ScanAct Action;
  enum FitFunc fitFunction;
  uint32_t targetThreshold;
} IblActionOptions;


typedef struct {
  enum ScanParam     scanParameter;
  IblActionOptions endofLoopAction; 
  uint32_t        nPoints;
  uint32_t        dataPointsPtr;
} IblScanLoop;

//The iblScanStatus struct is filled/stored inside scanCtrl->status (scanCtrl is of type ScanCtrl_2). The status is updated within iblScan_2.
//The address of the struct is given to the IblRodPixController at startup of the scan (IblRodPixController::startScan() htalktaskprim)
//and print out in IblRodPixController::runStatus() and IBLRodPixController::nTrigger()
typedef struct {
  //     uint32_t dcLoop;   //current DC loop
  //        uint32_t kDcLoop;  //NKtodo: nedded for dcLoop? is scanCtrl->status->kMaskStage = (uint32_t)&scanCtrl->loopCtrl[scanCtrl->maskStageLoop].parameterIndex; for maskStage
  //      uint32_t nDcLoops;  //max DC loop
        uint32_t maskStage;
	uint32_t kMaskStage;
	uint32_t nMaskStages;
	uint32_t loop0Parameter;
	uint32_t nLoop0Parameters;	
	uint32_t loop1Parameter;
	uint32_t nLoop1Parameters;	
	uint32_t loop2Parameter;
	uint32_t nLoop2Parameters;	
	uint32_t scanType;
	uint32_t options; /* points to a copy of the input structure */
	uint32_t activeModules;
	uint32_t progress;
	uint32_t state;	
	uint32_t task[N_IBL_SLAVES]; /* contains the (histogramming) task for each of the slaves */
} IblScanStatus;

/* Main Scan interface */
#define SCAN_MAXLOOPS 3

//IblScanOptions are partially filled inside IBLRodPixController::writeScanConfig()
typedef struct {
  enum MaskStageMode  stagingMode;
  enum MaskStageSteps maskStageTotalSteps;
  uint16_t              nMaskStages;
  int		      maskLoop; 
  //  int                 enableSingleDCloop; //FEI4 specific, just write into one single DC at a time (colpr_mode 00)
  uint16_t              FEbyFEMask;
  uint8_t               spare;
  uint8_t               nLoops;
  IblGroupOptions     groupOpt;
  IblTriggerOptions   trigOpt;
  //PixelScanFEI4       feOpt;
  IblScanLoop         scanLoop[SCAN_MAXLOOPS];
  IblHistoOptions     histOpt[SCAN_MAXHIST];
} IblScanOptions;
 

#endif
