#ifndef IBLSLAVEPRIMXFACE_H
#define IBLSLAVEPRIMXFACE_H

#include <iblScanOptions_2.h>


typedef struct {
  long long hitCount;
  long long eventCount;
  uint32_t data[N_FEI4_PIXELS];
  uint32_t tot[8*256];
} IblPixelData;

//** HISTOGRAM SETUP **

typedef struct {
	uint32_t nBins;
	uint32_t binOffset;
	uint32_t moduleOffset;
	uint32_t size;
	uint32_t base;
} IblHistMemInfo; //NK: used in slave's histogramTot e.g. talk()

typedef struct {
	uint32_t validModules;
	IblHistMemInfo histMemInfo[SCAN_MAXHIST];
} IblHistogramLayout;

#endif
