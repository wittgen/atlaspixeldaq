/*
 * Filename:   iblSlaveNetCmds.h
 * Author:     Kretz
 *
 * Summary:
 *   Structures and command code definitions relevant to the
 *   ROD slave <-> external server network communication.
 *   Not to be confused with netCmds.h.
 */

#ifndef IBLSLAVENETCMDS_H_
#define IBLSLAVENETCMDS_H_

#include <stdint.h>

/* For all commands, scanId is used to hold a unique identifier that is also known to the FitFarm
 *     bins contains the current bin number of the scan
 *     payloadSize is the byte count of the payload
 */

typedef struct {
	uint32_t magic;
	uint32_t command;
	uint32_t bins;
	uint32_t scanId;
	uint32_t maskId;
	uint32_t payloadSize;
} RodSlvTcpCmd;

/* Using SLVNET prefix to avoid naming collisions with defines from netCmds.h */
#define SLVNET_MAGIC 0x51525354
#define SLVNET_HIST_DATA_CMD 0x40


#endif /* IBLSLAVENETCMDS_H_ */
