# Component to setup Xilinx tools
# Author: K. Potamianos (karolos.potamianos@cern.ch)

HOST=$(shell /bin/hostname)

# Using Xilinx environment variable if set
ifdef XILINX_EDK
XIL_EDK_DIR = $(XILINX_EDK)
endif

LIN=lin
LIN64=lin64

# Default to 14.7 if nothing's there
XIL_EDK_DIR ?= /opt/Xilinx/14.7/ISE_DS/EDK
XIL_MICROBLAZE_BIN = $(XIL_EDK_DIR)/gnu/microblaze/$(LIN)/bin/
XILINX_PATH := $(dir $(XIL_EDK_DIR))
export PATH := $(XIL_MICROBLAZE_BIN):$(PATH)
# Todo: move this to xilconf.mk or somewhere more global
ifeq ($(wildcard $(XILINX_PATH)), )
$(error Cannot find $(XILINX_PATH). Please make sure the path exists or adjust xilconf.mk)
endif



# Todo: move this to xilconf.mk or somewhere more global
ifeq ($(wildcard $(XILINX_PATH)), )
$(error Cannot find $(XILINX_PATH). Please make sure the path exists or adjust xilconf.mk)
endif

XIL_ENV := XILINX=$(subst EDK,ISE,$(XIL_EDK_DIR))
XIL_ENV += LD_LIBRARY_PATH=$(subst EDK,common/lib/$(LIN64),$(XIL_EDK_DIR))


# Transparent change if directory from XIL_MICROBLAZE_BIN does not exist
ifeq ($(shell ls -d $(XIL_MICROBLAZE_BIN)), )
XIL_MICROBLAZE_BIN =
endif

MB-CC = $(XIL_MICROBLAZE_BIN)mb-gcc
MB-OBJDUMP = $(XIL_MICROBLAZE_BIN)mb-objdump
MB-OBJCOPY = $(XIL_MICROBLAZE_BIN)mb-objcopy
MB-SIZE = $(XIL_MICROBLAZE_BIN)mb-size

LIBGEN = $(XIL_EDK_DIR)/bin/$(LIN64)/libgen
ELFCHECK = $(XIL_EDK_DIR)/bin/$(LIN64)/elfcheck

ifeq ($(wildcard $(MB-CC)), )
$(error Cannot find MB-CC. File not found -- $(MB-CC))
endif

