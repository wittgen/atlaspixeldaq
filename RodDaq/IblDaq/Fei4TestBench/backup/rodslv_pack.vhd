library IEEE;
use IEEE.STD_LOGIC_1164.all;

package rodslv_pack is

	constant clk40_period : time := 25 ns;
	
	procedure read_slv_bus(
		signal address : in std_logic_vector(15 downto 0);
		signal rod_bus_ce 	: out std_logic;
		signal rod_bus_addr : out std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out std_logic;
		signal rod_bus_hwsb : out std_logic;
		signal rod_bus_ds		: out std_logic);

   procedure write_slv_bus(
		signal address : in std_logic_vector(15 downto 0);
		signal data_in : in std_logic_vector(31 downto 0);
		signal rod_bus_data : out std_logic_vector(15 downto 0);
		signal rod_bus_ce 	: out std_logic;
		signal rod_bus_addr : out std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out std_logic;
		signal rod_bus_hwsb : out std_logic;
		signal rod_bus_ds		: out std_logic);

end rodslv_pack;

package body rodslv_pack is

	procedure read_slv_bus(
		signal address : in std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '1'; rod_bus_hwsb <= '0';
		rod_bus_ds <= '0';
	  wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	  rod_bus_hwsb <= '1'; wait for clk40_period*2;
    rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';		
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
    wait for clk40_period; rod_bus_ce <= '0';	wait for clk40_period*8;	
	end read_slv_bus;

   procedure write_slv_bus(
		signal address : in std_logic_vector(15 downto 0);
		signal data_in : in std_logic_vector(31 downto 0);
		signal rod_bus_data : out std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '0'; rod_bus_hwsb <= '0';
		rod_bus_data <= data_in(15 downto 0);
    wait for clk40_period*3; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	  rod_bus_hwsb <= '1'; rod_bus_data <= data_in(31 downto 16);
		wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
    wait for clk40_period; 
		rod_bus_ce <= '0'; rod_bus_rnw <= '1'; rod_bus_data <= (others => 'Z');	
		wait for clk40_period*8;			
	end write_slv_bus;
  
end rodslv_pack;
