library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
use IEEE.std_logic_textio.all;


--library work;
use work.fei4_pack.all;
use work.rodslv_pack.all;


entity tb_rodslv_boc_fei4 is
end tb_rodslv_boc_fei4;


architecture behavior of tb_rodslv_boc_fei4 is

  component fei4_top
    port(
		  -- reset signals, active low
      RD1bar, RD2bar : in  std_logic;
			RA1bar, RA2bar : in  std_logic;
      Ref_Clk_P      : in  std_logic;
      Ref_Clk_N      : in  std_logic;
      Pixel_Array    : in  std_logic_vector(26879 downto 0);
			CMD_DCI_P   : in  std_logic;
      CMD_DCI_N      : in  std_logic;
      Cmd_ExtTrigger : in  std_logic;
      Cmd_ChipId     : in  std_logic_vector(2 downto 0);
      HitOr_P        : out std_logic;
      DOB_OUT_P      : out std_logic;
      DOB_OUT_N      : out std_logic
		);
  end component;

  component cdr_and_8b10bdecode is
    port(
      clk40mhz       : in  std_logic;
      din_fei4       : in  std_logic;
      reset          : in  std_logic;
      dec_dout       : out std_logic_vector(7 downto 0);
      dec_kchar      : out std_logic;
      dec_dout_valid : out std_logic;
      dec_decErr     : out std_logic;
      dec_dispErr    : out std_logic
		);
  end component;
	
	component Mux_BOC2ROD is
		port(
			clk160 : in std_logic; -- 160MHz -- actually, connect to 80 MHz in this testbench
			clk40 : in std_logic;
			ce80 : in std_logic; -- chip enable signal
			rst : in std_logic;
			-- From 8b10b decoder
			fei4_data_0 : in std_logic_vector(7 downto 0);
			fei4_data_1 : in std_logic_vector(7 downto 0);
			fei4_data_2 : in std_logic_vector(7 downto 0);
			fei4_data_3 : in std_logic_vector(7 downto 0);
			fei4_data_valid_0 : in std_logic;
			fei4_data_valid_1 : in std_logic;
			fei4_data_valid_2 : in std_logic;
			fei4_data_valid_3 : in std_logic;
			fei4_data_k_0 : in std_logic;
			fei4_data_k_1 : in std_logic;
			fei4_data_k_2 : in std_logic;
			fei4_data_k_3 : in std_logic;
			-- ROD RX data bus
			rx_rod_data 	: out std_logic_vector(7 downto 0);
			rx_rod_addr 	: out std_logic_vector(1 downto 0);
			rx_rod_valid	: out std_logic;
			rx_rod_ctrl 	: out std_logic
		);
	end component;

	component rodSlave_top is
		port(
			clk40in_p, clk40in_n				: in 	std_logic;
			clk100in_p, clk100in_n			: in 	std_logic;
			reset                    		: in  std_logic;
			xc_in                       : in  std_logic_vector(7 downto 0);
			xc													: out std_logic_vector(7 downto 0);
			rxdata_boc2rod							: in  std_logic_vector(47 downto 0);
			rod_busy                   	: out std_logic;
			rod_bus_ce		             	: in  std_logic;
			rod_bus_rnw              		: in  std_logic;
			rod_bus_hwsb            		: in  std_logic;
			rod_bus_ds              		: in  std_logic;
			rod_bus_addr             		: in  std_logic_vector(15 downto 0);
			rod_bus_data             		: inout std_logic_vector(15 downto 0);
			rod_bus_ack_out          		: out std_logic;
			mode_bits_in 								: in  std_logic_vector(11 downto 0);
			modebits_fifo_wen_n_in  		: in  std_logic;
			modebits_fifo_rst_n_in  		: in  std_logic;
			modebits_fifo_ef_n_out  		: out std_logic;
			modebits_fifo_ff_n_out  		: out std_logic;
			ev_data_from_ppc						: in  std_logic_vector(15 downto 0);
			ev_data_wen_n								: in  std_logic;
			ev_data_rst_n								: in  std_logic;   
			ev_data_almost_full_n				: out std_logic_vector(1 downto 0);
			ev_data_ready								: out std_logic_vector(1 downto 0); 
			ev_id_fifo_empty_error			: out std_logic_vector(1 downto 0);
			slink0_utest, slink1_utest 	: out std_logic;		
			slink0_ureset, slink1_ureset: out std_logic;
			slink0_uctrl, slink1_uctrl	: out std_logic;
			slink0_lff, slink1_lff			: in  std_logic;
			slink0_ldown, slink1_ldown	: in  std_logic;
			slink0_data_rod2boc 				: out std_logic_vector(15 downto 0);
			slink1_data_rod2boc					: out std_logic_vector(15 downto 0);
			slink0_we, slink1_we				: out std_logic;
			slink0_uclk, slink1_uclk		: out std_logic
		);
	end component;

	constant ref_clk40_period 	: time := 25 ns;
	constant ref_clk80_period 	: time := 12.5 ns;
	constant ref_clk100_period 	: time := 10 ns;

  --Inputs
  signal RD1bar : std_logic := '0';
  signal RD2bar : std_logic := '0';
  signal RA1bar : std_logic := '0';
  signal RA2bar : std_logic := '0';
  signal Ref_Clk_P          : std_logic                        := '0';
  signal Ref_Clk_N          : std_logic                        := '0';
  signal Pixel_Array_TB     : std_logic_vector(26879 downto 0) := (others => '0');
  signal Pixel_Array_TB_inv : std_logic_vector(0 to 26879)     := (others => '0');
  signal CMD_DCI_P          : std_logic                        := '0';
  signal CMD_DCI_N          : std_logic                        := '0';
  signal Cmd_ExtTrigger     : std_logic                        := '0';
	signal Cmd_ChipId         : std_logic_vector(2 downto 0)     := (others => '0');
	

  signal fe_mask : std_logic_vector(671 downto 0) := (others => '1');

  signal DCI       : std_logic := '0';
  signal reset_cdr : std_logic := '0';

  --Outputs
  signal HitOr_P   : std_logic;
  signal DOB_OUT_P : std_logic;--_vector(3 downto 0):= (others => '0');
  signal DOB_OUT_N : std_logic;--_vector(3 downto 0):= (others => '0');

  signal dec_dout       : std_logic_vector(7 downto 0) := (others => '0');
  signal dec_kchar      : std_logic;
  signal dec_dout_valid : std_logic;
  signal dec_decErr     : std_logic;
  signal dec_dispErr    : std_logic;


	-- BOC to ROD signals
	signal bocmux_en	: std_logic := '0';
	
--	signal data2rod 	: b2rDataType := (others=>(others=>'0'));--:= (others => '0');
--	signal addr2rod		: b2rAddrType := (others=>(others=>'0'));--:= (others => '0');
--	signal valid2rod	: std_logic_vector(0 downto 0) := (others => '0');
--	signal ctl2rod		: std_logic_vector(0 downto 0) := (others => '0');

	signal data2rod 	: std_logic_vector(7 downto 0) := (others => '0');
	signal addr2rod		: std_logic_vector(1 downto 0) := (others => '0');
	signal valid2rod	: std_logic := '0';
	signal ctl2rod		: std_logic := '0';

	-- ROD signals
	signal reset 			: std_logic := '0';
	signal clk100_p 	: std_logic := '0';
	signal clk100_n 	: std_logic := '0';
	signal clk80_p 	: std_logic := '0';
	signal clk80_n 	: std_logic := '0';
	signal rod_busy		: std_logic := '0';
	signal b2r_data 	: std_logic_vector(11 downto 0) := (others => '0'); 
	signal rxdata_boc2rod : std_logic_vector(47 downto 0) := (others => '0');
	signal xc_in  		: std_logic_vector(7 downto 0) := (others => '0');
	signal xc					: std_logic_vector(7 downto 0) := (others => '0');

	signal rod_bus_ce		  	: std_logic := '0';
	signal rod_bus_rnw    	: std_logic := '0';
	signal rod_bus_hwsb   	: std_logic := '0';
	signal rod_bus_ds    		: std_logic := '0';
	signal rod_bus_addr  		: std_logic_vector(15 downto 0) := (others => '0');
	signal rod_bus_data   	: std_logic_vector(15 downto 0) := (others => 'Z');
	signal rod_bus_ack_out	: std_logic := '0';

	signal ev_data_from_ppc					: std_logic_vector(15 downto 0) := (others => '0');
	signal ev_data_wen_n						: std_logic := '0';
	signal ev_data_rst_n						: std_logic := '0';   
	signal ev_data_almost_full_n		: std_logic_vector(1 downto 0) := (others => '0');
	signal ev_data_ready						: std_logic_vector(1 downto 0) := (others => '0'); 
	signal ev_id_fifo_empty_error 	: std_logic_vector(1 downto 0) := (others => '0');

	signal slink0_utest					: std_logic := '0';
	signal slink1_utest					: std_logic := '0';     
	signal slink0_ureset				: std_logic := '0';
	signal slink1_ureset				: std_logic := '0';
	signal slink0_uctrl				 	: std_logic := '0';
	signal slink1_uctrl				 	: std_logic := '0';
	signal slink0_lff					 	: std_logic := '0';
	signal slink1_lff					 	: std_logic := '0';
	signal slink0_ldown				 	: std_logic := '0';
	signal slink1_ldown					: std_logic := '0';
	signal slink0_data_rod2boc	: std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_data_rod2boc	: std_logic_vector(15 downto 0) := (others => '0');
	signal slink0_we						: std_logic := '0';
	signal slink1_we						: std_logic := '0';

  procedure clk_dly (
    signal clk : in std_logic;
    count      :    integer := 1
    ) is
  begin
    for i in 1 to count loop
      wait until rising_edge(clk);
    end loop;
  end procedure;

begin

  fei4_model : fei4_top
    port map (
      RD1bar 	=> RD1bar,
      RD2bar 	=> RD2bar,
      RA1bar 	=> RA1bar,
      RA2bar 	=> RA2bar,
      Ref_Clk_P      => Ref_Clk_P,
      Ref_Clk_N      => Ref_Clk_N,
      Pixel_Array    => Pixel_Array_TB,
      CMD_DCI_P      => CMD_DCI_P,
      CMD_DCI_N      => CMD_DCI_N,
      Cmd_ExtTrigger => Cmd_ExtTrigger,
      Cmd_ChipId     => Cmd_ChipId,
      HitOr_P        => HitOr_P,
      DOB_OUT_P      => DOB_OUT_P,
      DOB_OUT_N      => DOB_OUT_N
    );

  cdr_and_8b10bdecode_i : cdr_and_8b10bdecode 
		port map(
      clk40mhz       => Ref_Clk_P,
      din_fei4       => DOB_OUT_P,
      reset          => reset_cdr,
      dec_dout       => dec_dout,
      dec_kchar      => dec_kchar,
      dec_dout_valid => dec_dout_valid,
      dec_decErr     => dec_decErr,
      dec_dispErr    => dec_dispErr
    );        

	boc_2_rod_mux: Mux_BOC2ROD
		port map (
			clk160 	=> clk80_p, -- 160MHz -- actually, connect to 80 MHz in this testbench
			clk40 	=> Ref_Clk_P,
			ce80		=> bocmux_en, -- chip enable signal
			rst 		=> reset,
			-- From 8b10b decoder
			fei4_data_0 			=> dec_dout,
			fei4_data_1 			=> dec_dout,
			fei4_data_2 			=> dec_dout,
			fei4_data_3 			=> dec_dout,
			fei4_data_valid_0 => dec_dout_valid,
			fei4_data_valid_1 => dec_dout_valid,
			fei4_data_valid_2 => dec_dout_valid,
			fei4_data_valid_3 => dec_dout_valid,
			fei4_data_k_0 		=> dec_kchar,
			fei4_data_k_1 		=> dec_kchar,
			fei4_data_k_2 		=> dec_kchar,
			fei4_data_k_3 		=> dec_kchar,
			-- ROD RX data bus
			rx_rod_data 			=>	data2rod,
			rx_rod_addr 			=>	addr2rod,
			rx_rod_valid			=>	valid2rod,
			rx_rod_ctrl 			=>	ctl2rod
		);

	rod_slave: rodSlave_top
		port map(
			clk40in_p				=> Ref_Clk_P,
			clk40in_n				=> Ref_Clk_N,
			clk100in_p      => clk100_p,
			clk100in_n			=> clk100_n,
			reset           => reset,        		
			xc_in           => xc_in,           
			xc							=> xc,			
			rxdata_boc2rod	=> rxdata_boc2rod,					
			rod_busy        => rod_busy,                   
			rod_bus_ce		  => rod_bus_ce,		             
			rod_bus_rnw     => rod_bus_rnw,             		
			rod_bus_hwsb    => rod_bus_hwsb,           		
			rod_bus_ds      => rod_bus_ds,             		
			rod_bus_addr    => rod_bus_addr,            		
			rod_bus_data    => rod_bus_data,            		
			rod_bus_ack_out => rod_bus_ack_out,         		
			mode_bits_in 		=> X"000", 							
			modebits_fifo_wen_n_in	=> '1', 		
			modebits_fifo_rst_n_in  => '0',		
			modebits_fifo_ef_n_out  => open,
			modebits_fifo_ff_n_out	=> open,
			ev_data_from_ppc				=> ev_data_from_ppc,					
			ev_data_wen_n						=> ev_data_wen_n,						
			ev_data_rst_n						=> ev_data_rst_n,						
			ev_data_almost_full_n		=> ev_data_almost_full_n,		
			ev_data_ready						=> ev_data_ready,						
			ev_id_fifo_empty_error	=> ev_id_fifo_empty_error, 	
			slink0_utest				=> slink0_utest,		
			slink1_utest				=> slink1_utest,		
			slink0_ureset				=> slink0_ureset,		
			slink1_ureset				=> slink1_ureset,		
			slink0_uctrl				=> slink0_uctrl,		
			slink1_uctrl				=> slink1_uctrl,		
			slink0_lff					=> slink0_lff,			
			slink1_lff					=> slink1_lff,			
			slink0_ldown				=> slink0_ldown,		
			slink1_ldown				=> slink1_ldown,		
			slink0_data_rod2boc	=> slink0_data_rod2boc,
			slink1_data_rod2boc	=> slink1_data_rod2boc,
			slink0_we						=> slink0_we,				
			slink1_we						=> slink1_we,				
			slink0_uclk					=> open,			
			slink1_uclk					=> open 			
		);

  -- Clock process definitions
  clock40_process : process
  begin
    Ref_Clk_P <= '0';
    Ref_Clk_N <= '1';
    wait for ref_clk40_period/2;
    Ref_Clk_P <= '1';
    Ref_Clk_N <= '0';
    wait for ref_clk40_period/2;
  end process;
	
	clock80_process : process
  begin
    clk80_p <= '0';
    clk80_n <= '1';
    wait for ref_clk80_period/2;
    clk80_p <= '1';
    clk80_n <= '0';
    wait for ref_clk80_period/2;
  end process;

	clock100_process : process
  begin
    clk100_p <= '0';
    clk100_n <= '1';
    wait for ref_clk100_period/2;
    clk100_p <= '1';
    clk100_n <= '0';
    wait for ref_clk100_period/2;
  end process;

  CMD_DCI_P <= DCI;
  CMD_DCI_N <= not DCI;

 	-- duplicate the FEI4 DOB signal for BOC demo
	--	fei42boc <= DOB_OUT_P & DOB_OUT_P & DOB_OUT_P & DOB_OUT_P;
	-- duplicate BOC demo 12b output signal for three other b2r lines
	b2r_data <= ctl2rod & valid2rod & addr2rod & data2rod;
	rxdata_boc2rod <= b2r_data & b2r_data & b2r_data & b2r_data;
	
	
	
	
  -- Stimulus process
  stim_proc : process
  begin
    -- hold reset state
    RD1bar    <= '0';
    RD2bar    <= '0';
    RA1bar    <= '0';
    RA2bar    <= '0';
    reset_cdr <= '1';
		reset <= '1';

    wait for 1000 ns;
    -- leave reset state
    RD1bar    <= '1';
    RD2bar    <= '1';
    RA1bar    <= '1';
    RA2bar    <= '1';
    reset_cdr <= '0';
		reset <= '0';

--		bocmux_en <= '1';

    Cmd_ChipID <= "101";
    wait for 500 ns;

    fei4_wr_reg(DCI, "0101", "011011", X"8020");  -- enable PLL and HOR output
    fei4_wr_reg(DCI, "0101", "011100", X"0266");  -- configure PLL
    fei4_wr_reg(DCI, "0101", "011101", X"0004");  -- enable LVDS driver

    fei4_wr_reg(DCI, "0101", "000010", X"1000");  -- Configure Trigger Multiplier

    fei4_wr_reg(DCI, "0101", "010110", X"0300");  -- enable all columns

    -- sr clear pulse
    fei4_sr_clear_pulse(DCI, "0101", "000000");

    wait for 1000 ns;

    fei4_wr_reg(DCI, "0101", "010111", X"FFFF");  -- Column config 23
    fei4_wr_reg(DCI, "0101", "011000", X"FFFF");  -- Column config 24   
    fei4_wr_reg(DCI, "0101", "011001", X"F1FF");  -- Column config 25 + trig lat

    -- px strobes
    fei4_wr_reg(DCI, "0101", "001101", "0011111111111110");  -- px strobes + S0 + S1

    -- set mask
    fei4_set_mask(DCI, "0101", fe_mask);

    fei4_wr_reg(DCI, "0101", "010111", X"0000");  -- Column config 23
    fei4_wr_reg(DCI, "0101", "011000", X"0000");  -- Column config 24                                                  
    fei4_wr_reg(DCI, "0101", "011001", X"F100");  -- Column config 25 + trig lat=241

    fei4_runmode_on(DCI, "0101");       -- put the FEI4 chip in run mode

		
		

    wait for 500 ns;
    fei4_ECR(DCI);                      -- FEI4 ECR reset

    -- send L1A trigger request via CMD interface	
    wait for 3000 ns;
    fei4_L1A(DCI);    

		--------------------------------------------------------
		bocmux_en <= '1';
		reset <= '1';
		wait for 40 ns;
		reset <= '0';
		--------------------------------------------------------
		
		
		-- send L1A trigger request via FEI4 Cmd_ExtTrigger input pin
		wait for 1000 ns;
		Cmd_ExtTrigger <= '1';
		wait for 25 ns;
		Cmd_ExtTrigger <= '0';
		
		wait for 100 ns;
		

		
		
		
	
    wait;
		
		
  end process;

  Pixel_Array_TB_process : process
  begin
    wait for 50 us;
    while true loop

      -- inject a hit into pixel(0,0) with TOT=2
      Pixel_Array_TB_inv(0) <= '1';
      wait for 55 ns;
      Pixel_Array_TB_inv(0) <= '0';
      wait for 5000 ns;

      -- inject several hits into pixel array with TOT=3
      Pixel_Array_TB_inv(100)   <= '1';
      Pixel_Array_TB_inv(101)   <= '1';
      Pixel_Array_TB_inv(300)   <= '1';
      Pixel_Array_TB_inv(400)   <= '1';
      Pixel_Array_TB_inv(5000)  <= '1';
      Pixel_Array_TB_inv(26879) <= '1';
      wait for 80 ns;
      Pixel_Array_TB_inv(100)   <= '0';
      Pixel_Array_TB_inv(101)   <= '0';
      Pixel_Array_TB_inv(300)   <= '0';
      Pixel_Array_TB_inv(400)   <= '0';
      Pixel_Array_TB_inv(5000)  <= '0';
      Pixel_Array_TB_inv(26879) <= '0';

      wait for 1500 ns;
    end loop;
  end process;

  Pixel_Array_TB <= Pixel_Array_TB_inv;

end;
