onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label RA1bar /tb_rodslv_boc_fei4/RA1bar
add wave -noupdate -label RA2bar /tb_rodslv_boc_fei4/RA2bar
add wave -noupdate -label RD1bar /tb_rodslv_boc_fei4/RD1bar
add wave -noupdate -label RD2bar /tb_rodslv_boc_fei4/RD2bar

add wave -noupdate -color Pink -label {Ref Clk 40 MHz (P)} /tb_rodslv_boc_fei4/Ref_Clk_P
add wave -noupdate -label {Ref Clk 40 MHz (N)} /tb_rodslv_boc_fei4/Ref_Clk_N

add wave -noupdate -label {FEI4 Cmd_ChipID} /tb_rodslv_boc_fei4/Cmd_ChipId
add wave -noupdate -label {FEI4 Cmd_ExtTrigger} /tb_rodslv_boc_fei4/Cmd_ExtTrigger
add wave -noupdate -label {FEI4 DCI} /tb_rodslv_boc_fei4/DCI
add wave -noupdate -color Thistle -label {FEI4 DOB_OUT_P} /tb_rodslv_boc_fei4/DOB_OUT_P
add wave -noupdate -color Thistle -label {FEI4 DOB_OUT_N} /tb_rodslv_boc_fei4/DOB_OUT_N
add wave -noupdate -label HitOr_P /tb_rodslv_boc_fei4/HitOr_P
add wave -noupdate -label Pixel_Array /tb_rodslv_boc_fei4/Pixel_Array_TB

add wave -noupdate -color Cyan -label {Clk 80 MHz} /tb_rodslv_boc_fei4/clk80_p
add wave -noupdate -color Cyan -label {Clk 100 MHz} /tb_rodslv_boc_fei4/clk100_p

add wave -noupdate -color Gold -label {8B10 Decoder Out} -radix hexadecimal /tb_rodslv_boc_fei4/dec_dout
add wave -noupdate -color Cyan -label {8B10 dout valid} /tb_rodslv_boc_fei4/dec_dout_valid
add wave -noupdate -color Cyan -label {8B10 Kchar} /tb_rodslv_boc_fei4/dec_kchar
add wave -noupdate -color Cyan -label {8B10 DispErr} /tb_rodslv_boc_fei4/dec_dispErr
add wave -noupdate -label {8B10 DecErr} /tb_rodslv_boc_fei4/dec_decErr


add wave -noupdate -label {reset} /tb_rodslv_boc_fei4/reset
add wave -noupdate -label {MUC ce80} /tb_rodslv_boc_fei4/bocmux_en
add wave -noupdate -label {MUX Rx state} /tb_rodslv_boc_fei4/boc_2_rod_mux/rx_state
add wave -noupdate -label {MUX FIFO empty} /tb_rodslv_boc_fei4/boc_2_rod_mux/fifo_empty
add wave -noupdate -label {MUX FIFO read enable} /tb_rodslv_boc_fei4/boc_2_rod_mux/fifo_rd_en







add wave -noupdate -color Cyan -label {Ctrl to ROD} /tb_rodslv_boc_fei4/ctl2rod
add wave -noupdate -color Cyan -label {Valid to ROD} /tb_rodslv_boc_fei4/valid2rod
add wave -noupdate -color Cyan -label {Addr to ROD} -radix hexadecimal /tb_rodslv_boc_fei4/addr2rod
add wave -noupdate -color Cyan -label {Data to ROD} -radix hexadecimal /tb_rodslv_boc_fei4/data2rod




TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9028500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {21 us}
