#vmap unisims_ver $XILINX/verilog/questasim/10.1c/lin64/unisims_ver

proc vcom_all {} {	

#	create libraries
	vlib decode_8b10b
	vlib work

	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_pkg.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_lut_base.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_disp.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_lut.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_bram.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_rtl.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_top.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_wrapper.vhd"

	vcom  "./boc_cdr_8b10b/bocpack.vhd"
	vcom  "./boc_cdr_8b10b/rx_clkcross.vhd"

	vcom  "./boc_cdr_8b10b/data_alignment.vhd"
	vcom  "./boc_cdr_8b10b/cdr_deg90.vhd"
	vcom  "./boc_cdr_8b10b/cdr_and_8b10bdecode.vhd"
	
	
	## compile BOC components
	vcom  "./temp_files/rx_fifo.vhd"
	vcom  "./temp_files/Mux_BOC2ROD.vhd"
#	vcom "../../Boc/Firmware/rtl/bmf/Mux_BOC2ROD.vhd"
	
	## compile ROD slave
	vcom  "../ibl_rodSlave/rodSlave_top.vhd"

	vcom  "./fei4_pack.vhd"
	vcom  "./rodslv_pack.vhd"
	vcom  "./tb_rodslv_boc_fei4.vhd"
	
}



proc vsim_top_modelsim {} {
	vmap FEI4B_modelsim FEI4B_modelsim
	vsim -t 10ps -L FEI4B_modelsim -L decode_8b10b tb_rodslv_boc_fei4 -L xilinxcorelib_ver -L unisims_ver
    do rbf_wave.do
    vcd file myvcd1.vcd
		vcd add -r /tb_rodslv_boc_fei4/*
		run 100 us
}
