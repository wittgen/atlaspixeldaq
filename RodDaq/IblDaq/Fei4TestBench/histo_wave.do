onerror {resume}
quietly WaveActivateNextPane {} 0


add wave -noupdate -color Cyan -label {data2rtr1_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave1/k_word_i          
add wave -noupdate -color Green -label {data2rtr1_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_chip_id_i   
add wave -noupdate -color Pink -label {efb_data2rtr1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid_to_router_i

add wave -noupdate -color Yellow -label {histo1 write a} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/write_a
add wave -noupdate -color Yellow -label {histo1 write b} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/write_b
add wave -noupdate -color Yellow -label {histo1 rfd} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/histo_rfd
add wave -noupdate -color Yellow -label {histo1 pop a} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/pop_a
add wave -noupdate -color Yellow -label {histo1 pop b} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/pop_b
add wave -noupdate -color Orange -label {histo1 fifo a valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/fifo_a_valid
add wave -noupdate -color Orange -label {histo1 fifo b valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/fifo_b_valid

add wave -noupdate -color Yellow -label {histo1  chip ID} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/chip		
add wave -noupdate -color Yellow -label {histo1  hitValid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/hitValid
add wave -noupdate -color Yellow -label {histo1  Row} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/row     
add wave -noupdate -color Yellow -label {histo1  Col} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/col	
add wave -noupdate -color Yellow -label {histo1  ToT} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/totVal   

add wave -noupdate -color Cyan -label {data2rtr2_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave2/k_word_i          
add wave -noupdate -color Green -label {data2rtr2_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_chip_id_i  
add wave -noupdate -color Pink -label {efb_data2rtr2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr2} /tb_rodslv_boc_fei4/rod_slave/half_slave2/valid_to_router_i

add wave -noupdate -color Yellow -label {histo2 rfd} /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/histo_rfd
add wave -noupdate -color Yellow -label {histo2 pop a} /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/pop_a
add wave -noupdate -color Yellow -label {histo2 pop b} /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/pop_b
add wave -noupdate -color Yellow -label {histo2  chip ID} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/chip		
add wave -noupdate -color Yellow -label {histo2  hitValid} /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/hitValid
add wave -noupdate -color Yellow -label {histo2  Row} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/row     
add wave -noupdate -color Yellow -label {histo2  Col} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/col	
add wave -noupdate -color Yellow -label {histo2  ToT} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/router_instance/totVal  

add wave -noupdate -color Green -label {cpuclk100} /tb_rodslv_boc_fei4/rod_slave/cpuclk100
add wave -noupdate -color Green -label {SSRAM2 clk} /tb_rodslv_boc_fei4/sram2_clk_ssram
add wave -noupdate -color Green -label {SSRAM2 clk fb} /tb_rodslv_boc_fei4/sram2_clk_fb




TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9028500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {21 us}
