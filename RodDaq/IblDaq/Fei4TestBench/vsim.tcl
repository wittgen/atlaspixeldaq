#vmap unisims_ver $XILINX/verilog/questasim/10.1c/lin64/unisims_ver

proc vcom_all {} {	
#	create libraries
	vlib work
	vlib decode_8b10b
	vlib xcores

	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_pkg.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_lut_base.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_disp.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_lut.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_bram.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_rtl.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_top.vhd"
	vcom  -work decode_8b10b "./boc_cdr_8b10b/decode_8b10b/decode_8b10b_wrapper.vhd"
	
	vcom  "./boc_cdr_8b10b/bocpack.vhd"
	vcom  "./boc_cdr_8b10b/rx_clkcross.vhd"
	vcom  "./boc_cdr_8b10b/data_alignment.vhd"
	vcom  "./boc_cdr_8b10b/cdr_deg90.vhd"
	vcom  "./boc_cdr_8b10b/cdr_and_8b10bdecode.vhd"
	
	## compile BOC components
	vcom  -bindAtCompile "./boc_files/*.vhd"
	
	## compile ROD slave
	vcom  -work xcores "../ibl_rodSlave/cores/*.vhd"

	vcom  "../ibl_rodSlave/src/formatter/link_encoder.vhd"
	vcom  "../ibl_rodSlave/src/formatter/mb_data_decoder.vhd"
	vcom  "../ibl_rodSlave/src/formatter/fifo_readout_controller.vhd"
	vcom  "../ibl_rodSlave/src/formatter/formatter_top.vhd"
	
	vcom  "../ibl_rodSlave/src/txt_util.vhd"
	vcom  "../ibl_rodSlave/src/histo/rodHistoPack.vhd"
	vcom  "../ibl_rodSlave/src/histo/*.vhd"
	
	vcom  "../ibl_rodSlave/src/efb/bc_l1_check.vhd"
	vcom  "../ibl_rodSlave/src/efb/err_detect_new.vhd"
	vcom  "../ibl_rodSlave/src/efb/ev_data_decode.vhd"
	vcom  "../ibl_rodSlave/src/efb/format_data.vhd"
	vcom  "../ibl_rodSlave/src/efb/gen_fragment.vhd"
	vcom  "../ibl_rodSlave/src/efb/outmem.vhd"
	vcom  "../ibl_rodSlave/src/efb/efb_top.vhd"
	
	vcom  "../ibl_rodSlave/src/router/router_top.vhd"

	vcom  "../ibl_rodSlave/src/slv_register/ibl_slv_register.vhd"
	vcom  "../ibl_rodSlave/tb/CY7C1370D.vhd"
	vcom  "../ibl_rodSlave/tb/WireDelay2.vhd"
	vcom  "../ibl_rodSlave/tb/ZeroOhmArray.vhd"	
	
	vcom  "../ibl_rodSlave/src/ssram_clk_gen.vhd"
	vcom  "../ibl_rodSlave/src/ibl_halfSlave.vhd"
	vcom  "../ibl_rodSlave/src/rodSlave_top.vhd"

	vcom  "./fei4_pack.vhd"
	vcom  "./rodslv_pack.vhd"
	vcom  "./tb_rodslv_boc_fei4.vhd"
}


proc vsim_top_modelsim {} {
	vmap FEI4B_modelsim FEI4B_modelsim
	vsim -t 1ps -L FEI4B_modelsim -L decode_8b10b -L xcores tb_rodslv_boc_fei4 -L xilinxcorelib_ver -L unisims_ver
		do rbf_wave.do
#   vcd file tb_rodslv_boc_fei4.vcd
#		vcd add -r /tb_rodslv_boc_fei4/*
		run 80 us
}

proc vsim_top_modelsim_histo {} {
	vmap FEI4B_modelsim FEI4B_modelsim
	vsim -t 1ps -L FEI4B_modelsim -L decode_8b10b -L xcores tb_rodslv_boc_fei4 -L xilinxcorelib_ver -L unisims_ver
		do histo_wave.do
		run 80 us
}

# used for waveform debug
proc vsim_top_debug {} {
	vmap FEI4B_modelsim FEI4B_modelsim
	vsim -t 1ps -L FEI4B_modelsim -L decode_8b10b -L xcores tb_rodslv_boc_fei4 -L xilinxcorelib_ver -L unisims_ver
		do debug_wave.do
		run 80 us
}

