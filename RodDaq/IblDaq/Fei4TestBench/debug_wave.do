onerror {resume}
quietly WaveActivateNextPane {} 0

add wave -noupdate -color Pink -label {Ref Clk 40 MHz} /tb_rodslv_boc_fei4/Ref_Clk_P
add wave -noupdate -color Gold -label {8B10 Decoder Out} -radix hexadecimal /tb_rodslv_boc_fei4/dec_dout
add wave -noupdate -color Cyan -label {8B10 dout valid} /tb_rodslv_boc_fei4/dec_dout_valid
add wave -noupdate -color Cyan -label {8B10 Kchar} /tb_rodslv_boc_fei4/dec_kchar

add wave -noupdate -color Cyan -label {Clk 80 MHz} /tb_rodslv_boc_fei4/clk80_p

add wave -noupdate -color Yellow -label {ROD slv clk40} /tb_rodslv_boc_fei4/rod_slave/clk40 
add wave -noupdate -color Yellow -label {ROD slv clk80} /tb_rodslv_boc_fei4/rod_slave/clk80 

add wave -noupdate -color Pink -label {data2rod} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data2rod1 					
add wave -noupdate -color Pink -label {addr2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/addr2rod1 					
add wave -noupdate -color Pink -label {valid2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid2rod1 					
add wave -noupdate -color Pink -label {ctl2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/ctl2rod1 					

add wave -noupdate -color Pink -label {fmt1 cntrl_from_boc_i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/cntrl_from_boc_i
add wave -noupdate -color Yellow -label {fmt1 data from boc} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/data_from_boc_i
add wave -noupdate -color Cyan -label {fmt1 reg_enable} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/reg_enable




add wave -noupdate -color Red -label {fmt1 link0 clk} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/clk80            						
add wave -noupdate -color Green -label {fmt1 link0 reg enable} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/reg_enable(0)
add wave -noupdate -color Green -label {fmt1 link0 ctrl sig} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/cntrl_from_boc_i	
add wave -noupdate -color Yellow -label {fmt1 link0 data_in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/data_in
add wave -noupdate -color Yellow -label {fmt1 link0 counter} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/counter
add wave -noupdate -color cyan -label {fmt1 link0 data reg} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/data_reg
add wave -noupdate -color cyan -label {fmt1 link0 reg_en1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/register_enable1
add wave -noupdate -color Pink -label {fmt1 link0 link data} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/link_data			
add wave -noupdate -color Pink -label {fmt1 link0 link valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i(0)	




add wave -noupdate -color Yellow -label {fmt1 link valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i
add wave -noupdate -color Yellow -label {fmt1 fifo valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_valid
add wave -noupdate -color Yellow -label {fmt1 link data 0} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_0
add wave -noupdate -color Yellow -label {fmt1 link data 1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_1
add wave -noupdate -color Yellow -label {fmt1 link data 2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_2
add wave -noupdate -color Yellow -label {fmt1 link data 3} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_3



add wave -noupdate -color Pink -label {fmt1 Link_valid_i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i
add wave -noupdate -color Pink -label {fmt1 fifo ren_i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_ren_i
add wave -noupdate -color Pink -label {fmt1 link_fifo_data_i} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_fifo_data_i

add wave -noupdate -color Red -label {FIFO ctrlr readout state} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/readout_state
add wave -noupdate -color Red -label {FIFO ctrlr link mb i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/link_mb_i
add wave -noupdate -color Red -label {FIFO ctrlr hold output in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/hold_output_in
add wave -noupdate -color Red -label {FIFO ctrlr fifo ren i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/fifo_ren_i
add wave -noupdate -color Red -label {FIFO ctrlr link num} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/link_num

add wave -noupdate -color Pink -label {fmt1 data to efb} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_to_efb1_i
add wave -noupdate -color Pink -label {fmt1 valid to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid_to_efb1_i


add wave -noupdate -color Cyan -label {data2rtr1_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave1/k_word_i          
add wave -noupdate -color Green -label {data2rtr1_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_chip_id_i   
add wave -noupdate -color Pink -label {efb_data2rtr1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid_to_router_i

add wave -noupdate -color Cyan -label {data2rtr2_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave2/k_word_i          
add wave -noupdate -color Green -label {data2rtr2_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_chip_id_i  
add wave -noupdate -color Pink -label {efb_data2rtr2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr2} /tb_rodslv_boc_fei4/rod_slave/half_slave2/valid_to_router_i









TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9028500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {21 us}
