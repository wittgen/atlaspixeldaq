# create binary images of project code
# usage makebinary <project>
#echo $1
#echo `basename $1`
export PROG=`basename $1`
echo "Creating binary for project $1. Source file:"
ls -l $1/Debug/$PROG.elf
mb-objdump -S $1/Debug/$PROG.elf > $1/Debug/$PROG.lst
mb-objcopy  -O binary -R .vectors.reset -R .vectors.sw_exception -R .vectors.interrupt -R .vectors.hw_exception $1/Debug/$PROG.elf $1/Debug/$PROG.bin
bin2c -c -n slvProg -t int $1/Debug/$PROG.bin > $1/Debug/$PROG.hxx
echo "Results:"
ls -l $1/Debug/$PROG.bin
ls -l $1/Debug/$PROG.hxx
#cp bootTest.hxx ../../../../rodMasterPpc/cpu/rodMaster
