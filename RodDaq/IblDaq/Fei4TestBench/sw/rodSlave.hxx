// IBL ROD spartan-6 formatter slave address and bit definitions
#ifndef ROD_SLAVE_H
#define ROD_SLAVE_H
// This is a generated file, do not edit!

// make size of ram available for master PPC
#define SLV_RAM_WORDS 512
// formatter register numbers, for conveniency 
#define SLV_VERSION_REG 0x22
#define SLV_INMEM_REG 0x40
#define SLV_CTL_REG 0x42
#define SLV_CTL_RESET_BIT 31
#define SLV_STAT_REG 0x43
// if not PPC, define all other things as well 

#ifndef PPC // Design parameters
// The following register addresses sit on top of the EPC area, defined in xparameters.h
#ifndef XPAR_AXI_EPC_0_PRH0_BASEADDR
#include "xparameters.h"
#endif
// Number of EPC address bits is an alternative way to identify the address range
#define MB_EPC_ADDRESS_BITS 22
// EPC address range for registers
#define MB_EPC_REG_BASE (XPAR_AXI_EPC_0_PRH0_BASEADDR)
// EPC address range for registers
#define MB_EPC_RAM_BASE (XPAR_AXI_EPC_0_PRH0_BASEADDR + 0x10000)
// Design identification
#define MB_DESIGN_REG (MB_EPC_REG_BASE + 0x0)
// Design identification
#define HDL_FPGA_VERSION 0x00040001
// Control register
#define MB_CTL_REG (MB_EPC_REG_BASE + 0x4)
// Status register
#define MB_STAT_REG (MB_EPC_REG_BASE + 0x8)
// Status bits
#define STAT_SIMULATION_BIT 31
#define STAT_EXT_HISTRAM_BIT 30
#define STAT_DMA_RDY_BIT 29
#define STAT_HIST_RFR0_BIT 28
#define STAT_HIST_RFR1_BIT 27
#define STAT_HIST_RFD0_BIT 26
#define STAT_HIST_RFD1_BIT 25
#define STAT_HIST_ROC0_BIT 24
#define STAT_HIST_ROC1_BIT 23
#define STAT_LAST_BIT 23
// BOC test input address
#define MB_BOC_TEST_REG (MB_EPC_REG_BASE + 0xC)
// BOC serial transmit to master address
#define MB_TX_REG (MB_EPC_REG_BASE + 0x10)
// Histogrammer 0 test input address
#define MB_HIST0_TEST_REG (MB_EPC_REG_BASE + 0x18)
// Histogrammer 1 test input address
#define MB_HIST1_TEST_REG (MB_EPC_REG_BASE + 0x1C)
// Histogrammer 0 control address
#define MB_HIST0_CTL_REG (MB_EPC_REG_BASE + 0x20)
// Histogrammer 1 control address
#define MB_HIST1_CTL_REG (MB_EPC_REG_BASE + 0x24)

#endif // PPC
// Control register bits
// Input control bits
// CTL_BOC_ENABLE_BIT: 0: input from local FE-I4. 1: BOC input
#define CTL_BOC_ENABLE_BIT 0
// CTL_BOC_TEST_BIT: Select test input for boc. Requires BOC_ENABLE to be 1. Writes to BOC_TEST_ADDR supply data
#define CTL_BOC_TEST_BIT 1
// Histogrammer bits
// CTL_HIST_MODE: 11: sample mode, 10: wait, 00: readout mode
#define CTL_HIST_MODE0_LO 2
#define CTL_HIST_MODE0_HI 3
#define CTL_HIST_MODE1_LO 4
#define CTL_HIST_MODE1_HI 5
// CTL_HIST_MUX: 0: test input via write to MB_HISTx_TEST_REG. 1: EFB input
#define CTL_HIST_MUX0_BIT 6
#define CTL_HIST_MUX1_BIT 7
// DMA bits
// CTL_INMEM_SEL0_BIT, CTL_INMEM_SEL1_BIT: Select inmem channel via inmem sel bits 0 and 1. 00:A, 01:B, 10:C, 11:D
#define CTL_INMEM_SEL0_BIT 8
#define CTL_INMEM_SEL1_BIT 9
#define CTL_MASTER_MODE_BIT 30 // read-only
#define CTL_MASTER_RESET_BIT 31 // read-only
#endif //ROD_SLAVE_H
