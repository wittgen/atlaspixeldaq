library IEEE;
use IEEE.STD_LOGIC_1164.all;

package fei4_pack is

  -- field 1
  constant CMD_LV1    : std_logic_vector(4 downto 0) := "11101"; -- as (only) field 1, ultra-fast cmd
  constant CMD_FIELD1 : std_logic_vector(4 downto 0) := "10110"; -- field 1 for fast AND slow cmds
  
	-- field 2
	constant CMD_BCR    : std_logic_vector(3 downto 0) := "0001";
  constant CMD_ECR    : std_logic_vector(3 downto 0) := "0010";
  constant CMD_CAL    : std_logic_vector(3 downto 0) := "0100";
  constant CMD_FIELD2 : std_logic_vector(3 downto 0) := "1000"; -- field 2 for slwo cmds
	
	-- field 3
  constant CMD_RD_REG  : std_logic_vector(3 downto 0) := "0001";
  constant CMD_WR_REG  : std_logic_vector(3 downto 0) := "0010";
  constant CMD_WR_FE   : std_logic_vector(3 downto 0) := "0100";
  constant CMD_GRST    : std_logic_vector(3 downto 0) := "1000";
  constant CMD_GPULSE  : std_logic_vector(3 downto 0) := "1001";
  constant CMD_RUNMODE : std_logic_vector(3 downto 0) := "1010";

  procedure fei4_wr_reg(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0);
    constant wr_reg :     std_logic_vector(5 downto 0);
    constant data   :     std_logic_vector(15 downto 0)
    );

  procedure fei4_ECR(signal DCI : out std_logic);
  
  procedure fei4_L1A(signal DCI : out std_logic);

  procedure fei4_BCR(signal DCI : out std_logic);
  
  procedure fei4_sr_clear_pulse(
    signal DCI           : out std_logic;
    constant chipid      :     std_logic_vector(3 downto 0);
    constant pulse_width :     std_logic_vector(5 downto 0));

  procedure fei4_set_mask(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0);
    constant mask   :     std_logic_vector(671 downto 0));

  procedure fei4_runmode_on(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0)) ;

  procedure fei4_runmode_off(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0));

end fei4_pack;

package body fei4_pack is

  procedure fei4_field1(
    signal DCI : out std_logic; constant field1 : in std_logic_vector(4 downto 0)) is
  begin
    for I in field1'high downto field1'low loop
      DCI <= field1(I);
      wait for 25 ns;
    end loop;
  end procedure;
  
  procedure fei4_field2(
    signal DCI : out std_logic; constant field2 : in std_logic_vector(3 downto 0)) is
  begin

    for I in field2'high downto field2'low loop
      DCI <= field2(I);
      wait for 25 ns;
    end loop;
  end procedure;

  procedure fei4_field3(
    signal DCI : out std_logic; constant field3 : in std_logic_vector(3 downto 0)) is
  begin

    for I in field3'high downto field3'low loop
      DCI <= field3(I);
      wait for 25 ns;
    end loop;
  end procedure;

  procedure fei4_chipid(
    signal DCI : out std_logic; constant chipid : in std_logic_vector(3 downto 0)) is
  begin
    for I in chipid'high downto chipid'low loop
      DCI <= chipid(I);
      wait for 25 ns;
    end loop;
  end procedure;

  procedure fei4_addr(
    signal DCI : out std_logic; constant addr : in std_logic_vector (5 downto 0)) is
  begin

    for I in addr'high downto addr'low loop
      DCI <= addr(I);
      wait for 25 ns;
    end loop;
  end procedure;

  procedure fei4_field5(
    signal DCI : out std_logic; constant field5 : in std_logic_vector (5 downto 0)) is
  begin

    for I in field5'high downto field5'low loop
      DCI <= field5(I);
      wait for 25 ns;
    end loop;
  end procedure;

  procedure fei4_field6_wr_fe(
    signal DCI       : out std_logic;
    constant fe_mask : in  std_logic_vector (671 downto 0)) is
  begin

    for I in fe_mask'high downto fe_mask'low loop
      DCI <= fe_mask(I);
      wait for 25 ns;
    end loop;
  end procedure;
  
  procedure fei4_data(
    signal DCI : out std_logic; constant data : in std_logic_vector(15 downto 0)) is
  begin
    for I in data'high downto data'low loop
      DCI <= data(I);
      wait for 25 ns;
    end loop;
  end procedure;
  
  
  procedure fei4_L1A(
    signal DCI : out std_logic) is
  begin
    fei4_field1(DCI, CMD_LV1);
    -- RTZ
    DCI <= '0';
  end fei4_L1A;

  procedure fei4_ECR(
    signal DCI : out std_logic) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_ECR);

    -- RTZ
    DCI <= '0';

  end fei4_ECR;

  procedure fei4_BCR(
    signal DCI : out std_logic) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_BCR);

    -- RTZ
    DCI <= '0';

  end fei4_BCR;

  procedure fei4_wr_reg(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0);
    constant wr_reg :     std_logic_vector(5 downto 0);
    constant data   :     std_logic_vector(15 downto 0)
    ) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_FIELD2);
    fei4_field3(DCI, CMD_WR_REG);
    fei4_chipid(DCI, chipid);

    fei4_addr(DCI, wr_reg);
    fei4_data(DCI, data);

    -- RTZ
    DCI <= '0';

  end fei4_wr_reg;
  
  procedure fei4_pulse(
    signal DCI           : out std_logic;
    constant chipid      :     std_logic_vector(3 downto 0);
    constant pulse_width :     std_logic_vector(5 downto 0)
    ) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_FIELD2);
    fei4_field3(DCI, CMD_GPULSE);
    fei4_chipid(DCI, chipid);

    fei4_field5(DCI, pulse_width);
    -- RTZ
    DCI <= '0';

  end fei4_pulse;
  
  procedure fei4_sr_clear_pulse(
    signal DCI           : out std_logic;
    constant chipid      :     std_logic_vector(3 downto 0);
    constant pulse_width :     std_logic_vector(5 downto 0)
    ) is
  begin

    fei4_wr_reg(DCI, chipid, "011011", X"8028");  -- enable PLL and HOR output + SR CLR pulse

    fei4_pulse(DCI, chipid, pulse_width);
    
  end fei4_sr_clear_pulse;
  
  procedure fei4_set_mask(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0);
    constant mask   :     std_logic_vector(671 downto 0)
    ) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_FIELD2);
    fei4_field3(DCI, CMD_WR_FE);
    fei4_chipid(DCI, chipid);

    fei4_field5(DCI, "000000");
    fei4_field6_wr_fe(DCI, mask);

    fei4_wr_reg(DCI, chipid, "011011", X"8024");  -- enable PLL and HOR output + LEN
    fei4_pulse(DCI, chipid, "000000");
    fei4_wr_reg(DCI, chipid, "011011", X"8020");  -- enable PLL and HOR output
    
  end fei4_set_mask;

  
  procedure fei4_runmode_on(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0)
    ) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_FIELD2);
    fei4_field3(DCI, CMD_RUNMODE);
    fei4_chipid(DCI, chipid);

    fei4_field5(DCI, "111000");
    -- RTZ
    DCI <= '0';

  end fei4_runmode_on;

  procedure fei4_runmode_off(
    signal DCI      : out std_logic;
    constant chipid :     std_logic_vector(3 downto 0)
    ) is
  begin

    fei4_field1(DCI, CMD_FIELD1);
    fei4_field2(DCI, CMD_FIELD2);
    fei4_field3(DCI, CMD_RUNMODE);
    fei4_chipid(DCI, chipid);

    fei4_field5(DCI, "000111");
    -- RTZ
    DCI <= '0';

  end fei4_runmode_off;
  
end fei4_pack;
