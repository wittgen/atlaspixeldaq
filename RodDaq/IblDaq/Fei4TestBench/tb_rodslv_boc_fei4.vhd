library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
use IEEE.std_logic_textio.all;

--library work;
use work.fei4_pack.all;
use work.rodslv_pack.all;

entity tb_rodslv_boc_fei4 is
end tb_rodslv_boc_fei4;

architecture behavior of tb_rodslv_boc_fei4 is

  component fei4_top
    port(
      RD1bar, RD2bar 	: in  std_logic; -- reset, active low
			RA1bar, RA2bar 	: in  std_logic; -- reset, active low
      Ref_Clk_P      	: in  std_logic;
      Ref_Clk_N      	: in  std_logic;
      Pixel_Array    	: in  std_logic_vector(26879 downto 0);
			CMD_DCI_P			 	: in  std_logic;
      CMD_DCI_N      	: in  std_logic;
      Cmd_ExtTrigger 	: in  std_logic;
      Cmd_ChipId     	: in  std_logic_vector(2 downto 0);
      HitOr_P        	: out std_logic;
      DOB_OUT_P      	: out std_logic;
      DOB_OUT_N      	: out std_logic
		);
  end component;

  component cdr_and_8b10bdecode is
    port(
      clk40mhz       : in  std_logic;
      din_fei4       : in  std_logic;
      reset          : in  std_logic;
      dec_dout       : out std_logic_vector(7 downto 0);
      dec_kchar      : out std_logic;
      dec_dout_valid : out std_logic;
      dec_decErr     : out std_logic;
      dec_dispErr    : out std_logic
		);
  end component;
	
	component Mux_BOC2ROD is
		port(
			clk160	: in std_logic; -- 160MHz -- actually, connect to 80 MHz in this testbench
			clk40 	: in std_logic;
			ce80 		: in std_logic; -- chip enable signal
			rst 		: in std_logic;
			-- From 8b10b decoder
			fei4_data_0 : in std_logic_vector(7 downto 0);
			fei4_data_1 : in std_logic_vector(7 downto 0);
			fei4_data_2 : in std_logic_vector(7 downto 0);
			fei4_data_3 : in std_logic_vector(7 downto 0);
			fei4_data_valid_0 : in std_logic;
			fei4_data_valid_1 : in std_logic;
			fei4_data_valid_2 : in std_logic;
			fei4_data_valid_3 : in std_logic;
			fei4_data_k_0 : in std_logic;
			fei4_data_k_1 : in std_logic;
			fei4_data_k_2 : in std_logic;
			fei4_data_k_3 : in std_logic;
			-- ROD RX data bus
			rx_rod_data 	: out std_logic_vector(7 downto 0);
			rx_rod_addr 	: out std_logic_vector(1 downto 0);
			rx_rod_valid	: out std_logic;
			rx_rod_ctrl 	: out std_logic
		);
	end component;

	-- missing component: ROD Master
	
	component rodSlave_top is
		port(
			clk40in_p, 	clk40in_n				: in 	std_logic;
			clk100in_p, clk100in_n			: in 	std_logic;
			clk200out										: out std_logic_vector( 1 downto 0);
	    clk200in										: in  std_logic_vector( 1 downto 0);
			reset                    		: in  std_logic;
			xc_in                       : in  std_logic_vector( 7 downto 0);
			xc													: out std_logic_vector( 7 downto 0);
			rxdata_boc2rod							: in  std_logic_vector(47 downto 0);
			rod_busy                   	: out std_logic;
--			clk40mon 										: out std_logic;
--			clk100mon 									: out std_logic;
--			cpuclk_out 									: out std_logic;
			rod_bus_ce		             	: in  std_logic;
			rod_bus_rnw              		: in  std_logic;
			rod_bus_hwsb            		: in  std_logic;
			rod_bus_ds              		: in  std_logic;
			rod_bus_addr             		: in  std_logic_vector(15 downto 0);
			rod_bus_data             		: inout std_logic_vector(15 downto 0);
			rod_bus_ack_out          		: out std_logic;
			mode_bits_in 								: in  std_logic_vector(11 downto 0);
			modebits_fifo_wen_n_in  		: in  std_logic;
			modebits_fifo_rst_n_in  		: in  std_logic;
			modebits_fifo_ef_n_out  		: out std_logic;
			modebits_fifo_ff_n_out  		: out std_logic;
			ev_data_from_ppc						: in  std_logic_vector(15 downto 0);
			ev_data_wen_n								: in  std_logic;
			ev_data_rst_n								: in  std_logic;   
			ev_data_almost_full_n				: out std_logic_vector(1 downto 0);
			ev_data_ready								: out std_logic_vector(1 downto 0); 
			ev_id_fifo_empty_error			: out std_logic_vector(1 downto 0);
			slink0_utest,  slink1_utest : out std_logic;		
			slink0_ureset, slink1_ureset: out std_logic;
			slink0_uctrl,  slink1_uctrl	: out std_logic;
			slink0_lff,    slink1_lff		: in  std_logic;
			slink0_ldown,  slink1_ldown	: in  std_logic;
			slink0_data_rod2boc 				: out std_logic_vector(15 downto 0);
			slink1_data_rod2boc					: out std_logic_vector(15 downto 0);
			slink0_we,     slink1_we		: out std_logic;
			slink0_uclk,   slink1_uclk	: out std_logic;
			rfr0,		rfr1		: in  std_logic;												
			mode0,	mode1		: in  std_logic_vector( 1 downto 0);		
			sram1_io,				sram2_io				:	inout std_logic_vector(35 DOWNTO 0);   	
			sram1_a,				sram2_a					:	out	std_logic_vector(17 DOWNTO 0);   		
			sram1_lbo,			sram2_lbo				:	out	std_logic; 													
			sram1_AdvLd_n,	sram2_AdvLd_n		:	out	std_logic;                    			
			sram1_bw1_n,		sram2_bw1_n			:	out	std_logic;                          
			sram1_bw2_n,		sram2_bw2_n			:	out	std_logic;                          
			sram1_bw3_n,		sram2_bw3_n			:	out	std_logic;                          
			sram1_bw4_n,		sram2_bw4_n			:	out	std_logic;                          
			sram1_we_n,			sram2_we_n			:	out	std_logic;                          
			sram1_oe_n,			sram2_oe_n			:	out	std_logic;                          
			sram1_cke_n,		sram2_cke_n			:	out	std_logic;                          
			sram1_zz,				sram2_zz				:	out	std_logic;                          
			sram1_cs1,			sram2_cs1				:	out	std_logic                           
		);
	end component;
	
	component CY7C1370 is
		generic(
			addr_bits : integer := 18;
			data_bits : integer := 36);
	  port(
			Dq			: inout std_logic_vector (data_bits - 1 downto 0) := (others => 'Z');
			Addr		: in  std_logic_vector (addr_bits - 1 downto 0) := (others => '0');
			Mode		: in 	std_logic	:= '1';	-- Burst Mode
			Clk			: in 	std_logic;        -- Clk
			CEN_n		: in 	std_logic;        -- CEN#
			AdvLd_n	: in 	std_logic;        -- Adv/Ld#
			Bwa_n		: in 	std_logic;        -- Bwa#
			Bwb_n		: in 	std_logic;        -- BWb#
			Bwc_n		: in 	std_logic;        -- Bwc#
			Bwd_n		: in 	std_logic;        -- BWd#
			Rw_n		: in 	std_logic;        -- RW#
			Oe_n		: in 	std_logic;        -- OE#
			Ce1_n		: in 	std_logic;        -- CE1#
			Ce2			: in 	std_logic;        -- CE2
			Ce3_n		: in 	std_logic;        -- CE3#
			Zz			: in	std_logic         -- Snooze Mode
    );
	end component;

	component ZeroOhmArray 
		generic(
			a_to_b_loss : time := 1 ns; 
			b_to_a_loss : time := 2 ns;
			NBIT   			: natural := 1); 
		port(
			A : inout Std_Logic_vector(NBIT-1 downto 0); 
			B : inout Std_Logic_vector(NBIT-1 downto 0) 
    ); 
	end component;

	constant ref_clk40_period 	: time := 25 ns;
	constant ref_clk80_period 	: time := 12.5 ns;
	constant ref_clk100_period 	: time := 10 ns;

  --Inputs
  signal RD1bar : std_logic := '0';
  signal RD2bar : std_logic := '0';
  signal RA1bar : std_logic := '0';
  signal RA2bar : std_logic := '0';
  signal Ref_Clk_P          : std_logic                        := '0';
  signal Ref_Clk_N          : std_logic                        := '0';
  signal Pixel_Array_TB     : std_logic_vector(26879 downto 0) := (others => '0');
  signal Pixel_Array_TB_inv : std_logic_vector(0 to 26879)     := (others => '0');
  signal CMD_DCI_P          : std_logic                        := '0';
  signal CMD_DCI_N          : std_logic                        := '0';
  signal Cmd_ExtTrigger     : std_logic                        := '0';
	signal Cmd_ChipId         : std_logic_vector(2 downto 0)     := (others => '0');
	
  signal fe_mask		: std_logic_vector(671 downto 0) := (others => '1');
  signal DCI       	: std_logic := '0';
  signal reset_cdr 	: std_logic := '0';

  --Outputs
  signal HitOr_P   : std_logic;
  signal DOB_OUT_P : std_logic;--_vector(3 downto 0):= (others => '0');
  signal DOB_OUT_N : std_logic;--_vector(3 downto 0):= (others => '0');

  signal dec_dout       : std_logic_vector(7 downto 0) := (others => '0');
  signal dec_kchar      : std_logic;
  signal dec_dout_valid : std_logic;
  signal dec_decErr     : std_logic;
  signal dec_dispErr    : std_logic;

	-- Signals to determine L1ID & BCID
	signal dec_dout_d1		: std_logic_vector(7 downto 0) := (others => '0');
	signal dec_dout_d2		: std_logic_vector(7 downto 0) := (others => '0');	
	signal bcid_vector		: std_logic_vector(9 downto 0) := (others => '0');
	signal l1id						: integer := 0; 
	signal bcid						: integer := 0;

	-- BOC to ROD signals
	signal clk80_p 		: std_logic := '0';
	signal clk80_n 		: std_logic := '0';
	signal boc_rst 		: std_logic := '0';
	signal bocmux_en	: std_logic := '0';
	signal data2rod 	: std_logic_vector(7 downto 0) := (others => '0');
	signal addr2rod		: std_logic_vector(1 downto 0) := (others => '0');
	signal valid2rod	: std_logic := '0';
	signal ctl2rod		: std_logic := '0';

	-- ROD signals
	signal rodslv_rst : std_logic := '0';
	signal clk100_p 	: std_logic := '0';
	signal clk100_n 	: std_logic := '0';
	signal rod_busy		: std_logic := '0';
	signal b2r_data 	: std_logic_vector(11 downto 0) := (others => '0'); 
	signal rxdata_boc2rod : std_logic_vector(47 downto 0) := (others => '0');
	signal xc_in  		: std_logic_vector(7 downto 0) := (others => '0');
	signal xc					: std_logic_vector(7 downto 0) := (others => '0');

	signal rod_bus_ce		  	: std_logic := '0';
	signal rod_bus_rnw    	: std_logic := '0';
	signal rod_bus_hwsb   	: std_logic := '0';
	signal rod_bus_ds    		: std_logic := '0';
	signal rod_bus_addr  		: std_logic_vector(15 downto 0) := (others => '0');
	signal rod_bus_data   	: std_logic_vector(15 downto 0);
	signal rod_bus_ack_out	: std_logic := '0';

	signal ev_data_from_ppc					: std_logic_vector(15 downto 0) := (others => '0');
	signal ev_data_wen_n						: std_logic := '1';
	signal ev_data_rst_n						: std_logic := '0';   
	signal ev_data_almost_full_n		: std_logic_vector(1 downto 0);
	signal ev_data_ready						: std_logic_vector(1 downto 0); 
	signal ev_id_fifo_empty_error 	: std_logic_vector(1 downto 0);

	signal slink0_utest					: std_logic := '0';
	signal slink1_utest					: std_logic := '0';     
	signal slink0_ureset				: std_logic := '0';
	signal slink1_ureset				: std_logic := '0';
	signal slink0_uctrl				 	: std_logic := '0';
	signal slink1_uctrl				 	: std_logic := '0';
	signal slink0_lff					 	: std_logic := '0';
	signal slink1_lff					 	: std_logic := '0';
	signal slink0_ldown				 	: std_logic := '0';
	signal slink1_ldown					: std_logic := '0';
	signal slink0_data_rod2boc	: std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_data_rod2boc	: std_logic_vector(15 downto 0) := (others => '0');
	signal slink0_we						: std_logic := '0';
	signal slink1_we						: std_logic := '0';
	signal rfr0,					rfr1						: std_logic := '0';												
	signal mode0,					mode1						: std_logic_vector( 1 downto 0) := (others => '0');		
	signal sram1_io,			sram2_io				: std_logic_vector(35 downto 0) := (others => 'Z');   	
	signal sram1_a,				sram2_a					:	std_logic_vector(17 downto 0);   	
	signal sram1_lbo,			sram2_lbo				:	std_logic; 												
	signal sram1_AdvLd_n,	sram2_AdvLd_n		:	std_logic;                    		
	signal sram1_bw1_n,		sram2_bw1_n			:	std_logic;                        
	signal sram1_bw2_n,		sram2_bw2_n			:	std_logic;                        
	signal sram1_bw3_n,		sram2_bw3_n			:	std_logic;                        
	signal sram1_bw4_n,		sram2_bw4_n			:	std_logic;                        
	signal sram1_we_n,		sram2_we_n			:	std_logic;                        
	signal sram1_oe_n,		sram2_oe_n			:	std_logic;                        
	signal sram1_cke_n,		sram2_cke_n			:	std_logic;                        
	signal sram1_zz,			sram2_zz				:	std_logic;                        
	signal sram1_cs1,			sram2_cs1				:	std_logic;

	constant addr_bits	: integer := 18;
  constant data_bits	: integer := 36;
	
	constant propagation_pcb_clock_1	: time := 415 ps;
	constant propagation_pcb_clock_2 	: time := 180 ps;
	constant propagation_pcb_data 		: time := 350 ps;
	
  signal sram1_io_b, 	sram2_io_b		: std_logic_vector(data_bits - 1 downto 0) := (others => 'Z');
  signal sram1_a_dly, sram2_a_dly		: std_logic_vector(addr_bits - 1 downto 0) := (others => '0');
	signal sram1_clk, 	sram2_clk			: std_logic;
  signal sram1_clk_ssram, sram2_clk_ssram : std_logic;
	signal sram1_clk_fb, 		sram2_clk_fb		: std_logic;
  signal sram1_we_n_dly,	sram2_we_n_dly	: std_logic;
	signal sram1_bw1_n_dly,sram1_bw2_n_dly,sram1_bw3_n_dly,sram1_bw4_n_dly : std_logic;
	signal sram2_bw1_n_dly,sram2_bw2_n_dly,sram2_bw3_n_dly,sram2_bw4_n_dly : std_logic;
                                                                           
	signal rodslv_bus_state : string(1 to 10) := "idle      ";

--  procedure clk_dly (
--    signal clk : in std_logic;
--    count      :    integer := 1
--    ) is
--  begin
--    for i in 1 to count loop
--      wait until rising_edge(clk);
--    end loop;
--  end procedure

begin

  fei4_model : fei4_top
    port map (
      RD1bar 					=> RD1bar,
      RD2bar 					=> RD2bar,
      RA1bar 					=> RA1bar,
      RA2bar 					=> RA2bar,
      Ref_Clk_P      	=> Ref_Clk_P,
      Ref_Clk_N      	=> Ref_Clk_N,
      Pixel_Array    	=> Pixel_Array_TB,
      CMD_DCI_P      	=> CMD_DCI_P,
      CMD_DCI_N      	=> CMD_DCI_N,
      Cmd_ExtTrigger	=> Cmd_ExtTrigger,
      Cmd_ChipId     	=> Cmd_ChipId,
      HitOr_P        	=> HitOr_P,
      DOB_OUT_P      	=> DOB_OUT_P,
      DOB_OUT_N      	=> DOB_OUT_N
    );

  cdr_and_8b10bdecode_i : cdr_and_8b10bdecode 
		port map(
      clk40mhz       	=> Ref_Clk_P,
      din_fei4       	=> DOB_OUT_P,
      reset          	=> reset_cdr,
      dec_dout       	=> dec_dout,
      dec_kchar      	=> dec_kchar,
      dec_dout_valid 	=> dec_dout_valid,
      dec_decErr     	=> dec_decErr,
      dec_dispErr    	=> dec_dispErr
    );        

	boc_2_rod_mux: Mux_BOC2ROD
		port map (
			clk160 	=> clk80_p, -- 160MHz -- actually, connect to 80 MHz in this testbench
			clk40 	=> Ref_Clk_P,
			ce80		=> bocmux_en, -- chip enable signal
			rst 		=> boc_rst,
			-- From 8b10b decoder
			fei4_data_0 			=> dec_dout,
			fei4_data_1 			=> dec_dout,
			fei4_data_2 			=> dec_dout,
			fei4_data_3 			=> dec_dout,
			fei4_data_valid_0 => dec_dout_valid,
			fei4_data_valid_1 => dec_dout_valid,
			fei4_data_valid_2 => dec_dout_valid,
			fei4_data_valid_3 => dec_dout_valid,
			fei4_data_k_0 		=> dec_kchar,
			fei4_data_k_1 		=> dec_kchar,
			fei4_data_k_2 		=> dec_kchar,
			fei4_data_k_3 		=> dec_kchar,
			-- ROD RX data bus
			rx_rod_data 			=>	data2rod,
			rx_rod_addr 			=>	addr2rod,
			rx_rod_valid			=>	valid2rod,
			rx_rod_ctrl 			=>	ctl2rod
		);

	rod_slave: rodSlave_top
		port map(
			clk40in_p				=> Ref_Clk_P,
			clk40in_n				=> Ref_Clk_N,
			clk100in_p      => clk100_p,
			clk100in_n			=> clk100_n,
			clk200out(0)		=> sram1_clk,
			clk200out(1)		=> sram2_clk,
			clk200in(0)			=> sram1_clk_fb,
			clk200in(1)			=> sram2_clk_fb,
			reset           => rodslv_rst,        		
			xc_in           => xc_in,           
			xc							=> xc,			
			rxdata_boc2rod	=> rxdata_boc2rod,					
			rod_busy        => rod_busy,                   
			rod_bus_ce		  => rod_bus_ce,		             
			rod_bus_rnw     => rod_bus_rnw,             		
			rod_bus_hwsb    => rod_bus_hwsb,           		
			rod_bus_ds      => rod_bus_ds,             		
			rod_bus_addr    => rod_bus_addr,            		
			rod_bus_data    => rod_bus_data,            		
			rod_bus_ack_out => rod_bus_ack_out,         		
			mode_bits_in 		=> X"000", 							
			modebits_fifo_wen_n_in	=> '1', 		
			modebits_fifo_rst_n_in  => '1',		
			modebits_fifo_ef_n_out  => open,
			modebits_fifo_ff_n_out	=> open,
			ev_data_from_ppc				=> ev_data_from_ppc,					
			ev_data_wen_n						=> ev_data_wen_n,						
			ev_data_rst_n						=> ev_data_rst_n,						
			ev_data_almost_full_n		=> ev_data_almost_full_n,		
			ev_data_ready						=> ev_data_ready,						
			ev_id_fifo_empty_error	=> ev_id_fifo_empty_error, 	
			slink0_utest				=> slink0_utest,		
			slink1_utest				=> slink1_utest,		
			slink0_ureset				=> slink0_ureset,		
			slink1_ureset				=> slink1_ureset,		
			slink0_uctrl				=> slink0_uctrl,		
			slink1_uctrl				=> slink1_uctrl,		
			slink0_lff					=> slink0_lff,			
			slink1_lff					=> slink1_lff,			
			slink0_ldown				=> slink0_ldown,		
			slink1_ldown				=> slink1_ldown,		
			slink0_data_rod2boc	=> slink0_data_rod2boc,
			slink1_data_rod2boc	=> slink1_data_rod2boc,
			slink0_we						=> slink0_we,				
			slink1_we						=> slink1_we,				
			slink0_uclk					=> open,			
			slink1_uclk					=> open,
			rfr0								=> rfr0,					
			rfr1			          => rfr1,			    
			mode0               => mode0,         
			mode1		            => mode1,		      
			sram1_io            => sram1_io,      
			sram2_io				    => sram2_io,			
			sram1_a             => sram1_a,       
			sram2_a					    => sram2_a,				
			sram1_lbo           => sram1_lbo,     
			sram2_lbo				    => sram2_lbo,			
			sram1_AdvLd_n       => sram1_AdvLd_n, 
			sram2_AdvLd_n		    => sram2_AdvLd_n,	
			sram1_bw1_n         => sram1_bw1_n,   
			sram2_bw1_n			    => sram2_bw1_n,		
			sram1_bw2_n         => sram1_bw2_n,   
			sram2_bw2_n			    => sram2_bw2_n,		
			sram1_bw3_n					=> sram1_bw3_n,		
			sram2_bw3_n			    => sram2_bw3_n,		
			sram1_bw4_n         => sram1_bw4_n,   
			sram2_bw4_n			    => sram2_bw4_n,		
			sram1_we_n          => sram1_we_n,    
			sram2_we_n			    => sram2_we_n,		
			sram1_oe_n          => sram1_oe_n,    
			sram2_oe_n			    => sram2_oe_n,		
			sram1_cke_n         => sram1_cke_n,   
			sram2_cke_n			    => sram2_cke_n,		
			sram1_zz            => sram1_zz,      
			sram2_zz				    => sram2_zz,			
			sram1_cs1           => sram1_cs1,     
			sram2_cs1				    => sram2_cs1		
		);                    

	SSRAM1: CY7C1370
		generic map(addr_bits => addr_bits, data_bits => data_bits)
		port map(
			Dq 			=> sram1_io_b, 
			Addr 		=> sram1_a_dly, 
			Clk 		=> sram1_clk_ssram, 
			CEN_n 	=> sram1_cke_n,
			Mode 		=> sram1_lbo, 
			AdvLd_n => sram1_AdvLd_n, 
			Bwa_n 	=> sram1_bw1_n_dly, 
			Bwb_n 	=> sram1_bw2_n_dly, 
			Bwc_n 	=> sram1_bw3_n_dly, 
			Bwd_n		=> sram1_bw4_n_dly,
			Rw_n 		=> sram1_we_n_dly, 
			Oe_n 		=> sram1_oe_n, 
			Ce1_n 	=> sram1_cs1, 
			Ce3_n 	=> '0', 
			Ce2 		=> '1', 
			Zz 			=> sram1_zz
		);
		
	sram1_clk_fb 		<= transport sram1_clk_ssram after propagation_pcb_clock_1;
	sram1_clk_ssram <= transport sram1_clk after propagation_pcb_clock_1;
	sram1_a_dly 		<= transport sram1_a after propagation_pcb_data;
	sram1_we_n_dly 	<= transport sram1_we_n after propagation_pcb_data;
	sram1_bw1_n_dly <= transport sram1_bw1_n after propagation_pcb_data;
	sram1_bw2_n_dly <= transport sram1_bw2_n after propagation_pcb_data;
	sram1_bw3_n_dly <= transport sram1_bw3_n after propagation_pcb_data;
	sram1_bw4_n_dly <= transport sram1_bw4_n after propagation_pcb_data;
	
	SSRAM2: CY7C1370
		generic map(addr_bits => addr_bits, data_bits => data_bits)
		port map(
			Dq 			=> sram2_io_b, 
			Addr 		=> sram2_a_dly, 
			Clk 		=> sram2_clk_ssram, 
			CEN_n 	=> sram2_cke_n,
			Mode 		=> sram2_lbo, 
			AdvLd_n => sram2_AdvLd_n, 
			Bwa_n 	=> sram2_bw1_n_dly, 
			Bwb_n 	=> sram2_bw2_n_dly, 
			Bwc_n 	=> sram2_bw3_n_dly, 
			Bwd_n 	=> sram2_bw4_n_dly,
			Rw_n 		=> sram2_we_n_dly, 
			Oe_n 		=> sram2_oe_n, 
			Ce1_n 	=> sram2_cs1, 
			Ce3_n 	=> '0', 
			Ce2 		=> '1', 
			Zz 			=> sram2_zz
		);
	
	sram2_clk_fb 		<= transport sram2_clk_ssram after propagation_pcb_clock_2;
	sram2_clk_ssram <= transport sram2_clk after propagation_pcb_clock_2;
	sram2_a_dly 		<= transport sram2_a after propagation_pcb_data;
	sram2_we_n_dly 	<= transport sram2_we_n after propagation_pcb_data;
	sram2_bw1_n_dly <= transport sram2_bw1_n after propagation_pcb_data;
	sram2_bw2_n_dly <= transport sram2_bw2_n after propagation_pcb_data;
	sram2_bw3_n_dly <= transport sram2_bw3_n after propagation_pcb_data;
	sram2_bw4_n_dly <= transport sram2_bw4_n after propagation_pcb_data;
	
	Inst_bidirbus_1: ZeroOhmArray
		generic map(
			a_to_b_loss => propagation_pcb_data,
      b_to_a_loss => propagation_pcb_data,
			NBIT => 36) 
		port map(
			A => sram1_io,
			B => sram1_io_b
    ); 
	  
	Inst_bidirbus_2: ZeroOhmArray
		generic map(
			a_to_b_loss => propagation_pcb_data,
      b_to_a_loss => propagation_pcb_data,
			NBIT => 36) 
		port map(
			A => sram2_io,
			B => sram2_io_b
    ); 
												 
  -- Clock process definitions
  clock40_process : process
  begin
    Ref_Clk_P <= '0';
    Ref_Clk_N <= '1';
    wait for ref_clk40_period/2;
    Ref_Clk_P <= '1';
    Ref_Clk_N <= '0';
    wait for ref_clk40_period/2;
  end process;
	
	clock80_process : process
  begin
    clk80_p <= '1';
    clk80_n <= '0';
    wait for ref_clk80_period/2;
    clk80_p <= '0';
    clk80_n <= '1';
    wait for ref_clk80_period/2;
  end process;

	clock100_process : process
  begin
    clk100_p <= '1';
    clk100_n <= '0';
    wait for ref_clk100_period/2;
    clk100_p <= '0';
    clk100_n <= '1';
    wait for ref_clk100_period/2;
  end process;

  CMD_DCI_P <= DCI;
  CMD_DCI_N <= not DCI;

 	-- duplicate the FEI4 DOB signal for BOC demo
	-- fei42boc <= DOB_OUT_P & DOB_OUT_P & DOB_OUT_P & DOB_OUT_P;
	-- duplicate BOC demo 12b output signal for three other b2r lines
	b2r_data <= ctl2rod & valid2rod & addr2rod & data2rod;
	rxdata_boc2rod <= b2r_data & b2r_data & b2r_data & b2r_data;
	
	calc_l1bcid : process (boc_rst, Ref_Clk_P, dec_dout)
	begin
		if(boc_rst = '1') then
			dec_dout_d1 <= X"FF";			
			dec_dout_d2 <= X"FF";			
		elsif(rising_edge(Ref_Clk_P) AND (dec_dout_d1 /= dec_dout)) then
			dec_dout_d1 <= dec_dout;
			dec_dout_d2 <= dec_dout_d1;
		end if;
		bcid_vector <= dec_dout_d1(1 downto 0) & dec_dout;
		if (dec_dout_d2 = X"E9") then
			l1id <= conv_integer(unsigned(dec_dout_d1(6 downto 2)));
			bcid <= conv_integer(unsigned(bcid_vector));
		end if;
	end process;

  start_FEI4_BOC : process
  begin
    -- hold reset state
    RD1bar    <= '0';
    RD2bar    <= '0';
    RA1bar    <= '0';
    RA2bar    <= '0';
    reset_cdr <= '1';
    wait for 1000 ns;
    -- leave reset state
    RD1bar    <= '1';
    RD2bar    <= '1';
    RA1bar    <= '1';
    RA2bar    <= '1';
    reset_cdr <= '0';
    Cmd_ChipID <= "101";
    wait for 500 ns;
		
    fei4_wr_reg(DCI, "0101", "011011", X"8020");  -- enable PLL and HOR output
    fei4_wr_reg(DCI, "0101", "011100", X"0266");  -- configure PLL
    fei4_wr_reg(DCI, "0101", "011101", X"0004");  -- enable LVDS driver
    fei4_wr_reg(DCI, "0101", "000010", X"1000");  -- Configure Trigger Multiplier
    fei4_wr_reg(DCI, "0101", "010110", X"0300");  -- enable all columns
    -- sr clear pulse
    fei4_sr_clear_pulse(DCI, "0101", "000000");

    wait for 1000 ns;
    fei4_wr_reg(DCI, "0101", "010111", X"FFFF");  -- Column config 23
    fei4_wr_reg(DCI, "0101", "011000", X"FFFF");  -- Column config 24   
    fei4_wr_reg(DCI, "0101", "011001", X"F1FF");  -- Column config 25 + trig lat

    -- px strobes
    fei4_wr_reg(DCI, "0101", "001101", "0011111111111110");  -- px strobes + S0 + S1
    -- set mask
    fei4_set_mask(DCI, "0101", fe_mask);

    fei4_wr_reg(DCI, "0101", "010111", X"0000");  -- Column config 23
    fei4_wr_reg(DCI, "0101", "011000", X"0000");  -- Column config 24                                                  
    fei4_wr_reg(DCI, "0101", "011001", X"F100");  -- Column config 25 + trig lat=241

    fei4_runmode_on(DCI, "0101");       -- put the FEI4 chip in run mode

    wait for 500 ns;
    fei4_ECR(DCI);                      -- FEI4 event counter reset
		wait for 500 ns;
    fei4_BCR(DCI);                      -- FEI4 bunch counter (trigger ID) reset

    -- send L1A trigger request via CMD interface	
    wait for 3000 ns;
    fei4_L1A(DCI);    
		
		-- send L1A trigger request via FEI4 Cmd_ExtTrigger input pin
--		wait for 1000 ns;
--		Cmd_ExtTrigger <= '1';
--		wait for 25 ns;
--		Cmd_ExtTrigger <= '0';
--		
--		wait for 100 ns;
    wait;
  end process;

	-- Inject charge 
  Pixel_Array_TB_process : process 
  begin
    wait for 50 us;

      -- inject a hit into pixel(0,0) with TOT=2
			Pixel_Array_TB_inv(0) <= '1';
      wait for 55 ns;	-- 2 clock cycles
      Pixel_Array_TB_inv(0) <= '0';
      wait for 3000 ns;

      -- inject several hits into pixel array with TOT=3
      Pixel_Array_TB_inv(0)   <= '1';
			Pixel_Array_TB_inv(1)   <= '1';
			Pixel_Array_TB_inv(2)   <= '1';
			Pixel_Array_TB_inv(5)   <= '1';
			Pixel_Array_TB_inv(6)   <= '1';
			Pixel_Array_TB_inv(7)   <= '1';
--			Pixel_Array_TB_inv(100)   <= '1';
--			Pixel_Array_TB_inv(101)   <= '1';
--			Pixel_Array_TB_inv(102)   <= '1';
--			Pixel_Array_TB_inv(103)   <= '1';
--      Pixel_Array_TB_inv(300)   <= '1';
--      Pixel_Array_TB_inv(400)   <= '1';
--			Pixel_Array_TB_inv(1120)  <= '1';
--      Pixel_Array_TB_inv(5000)  <= '1';
      wait for 80 ns;	-- 3 clock cycles
      Pixel_Array_TB_inv(0)   <= '0';
			Pixel_Array_TB_inv(1)   <= '0';
			Pixel_Array_TB_inv(2)   <= '0';
			Pixel_Array_TB_inv(5)   <= '0';
			Pixel_Array_TB_inv(6)   <= '0';
			Pixel_Array_TB_inv(7)   <= '0';			
--      Pixel_Array_TB_inv(100)   <= '0';
--			Pixel_Array_TB_inv(101)   <= '0';
--			Pixel_Array_TB_inv(102)   <= '0';
--			Pixel_Array_TB_inv(103)   <= '0';			
--      Pixel_Array_TB_inv(300)   <= '0';
--      Pixel_Array_TB_inv(400)   <= '0';
--			Pixel_Array_TB_inv(1120)  <= '0';
--      Pixel_Array_TB_inv(5000)  <= '0';
			
      wait for 3000 ns;  				-- TOT = 4
      Pixel_Array_TB_inv(100)   <= '1';
      Pixel_Array_TB_inv(300)   <= '1';
      Pixel_Array_TB_inv(410)   <= '1';
      Pixel_Array_TB_inv(5000)  <= '1';
			Pixel_Array_TB_inv(8921)  <= '1';
      Pixel_Array_TB_inv(26879) <= '1';
      wait for 110 ns; -- 4 clock cycles
      Pixel_Array_TB_inv(100)   <= '0';
      Pixel_Array_TB_inv(300)   <= '0';
      Pixel_Array_TB_inv(410)   <= '0';
      Pixel_Array_TB_inv(5000)  <= '0';
			Pixel_Array_TB_inv(8921)  <= '0';
      Pixel_Array_TB_inv(26879) <= '0';
      	
      wait for 3000 ns;					-- TOT = 6
      Pixel_Array_TB_inv(100)   <= '1';
      Pixel_Array_TB_inv(120)   <= '1';
      Pixel_Array_TB_inv(300)   <= '1';
      Pixel_Array_TB_inv(410)   <= '1';
      Pixel_Array_TB_inv(5000)  <= '1';
      Pixel_Array_TB_inv(24400) <= '1';
      Pixel_Array_TB_inv(26879) <= '1';
      wait for 155 ns; -- 6 clock cycles
      Pixel_Array_TB_inv(100)   <= '0';
      Pixel_Array_TB_inv(120)   <= '0';
      Pixel_Array_TB_inv(300)   <= '0';
      Pixel_Array_TB_inv(410)   <= '0';
      Pixel_Array_TB_inv(5000)  <= '0';
      Pixel_Array_TB_inv(24400) <= '0';
      Pixel_Array_TB_inv(26879) <= '0';
      
      wait for 6000 ns;
 --   end loop;
  end process;

  Pixel_Array_TB <= Pixel_Array_TB_inv;
	
	boc_start_proc: process
	begin
		boc_rst <= '1';
		wait for 1 us;
		boc_rst <= '0';
		wait for 24 us;
		bocmux_en <= '1'; -- start BOC
		wait;
	end process;	

	rodslv_rst_proc: process
	begin
		rodslv_rst <= '1';
		wait for 1 us;
		rodslv_rst <= '0';
		wait;
	end process;

	rodslv_bus_proc: process
  begin	
	
		rodslv_bus_state <= "idle      ";	
		
		rod_bus_data <=(others => '1');
    wait for 100 ns;
		rod_bus_data <=(others => 'Z');
		wait for 5 us; wait for 12.5 ns;
		
		rodslv_bus_state <= "read fmt  ";	

		read_fmt_stts_reg : read_slv_bus(X"0022", rod_bus_ce, rod_bus_addr,			-- read fmt stts reg (reserved)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		read_fmt_ctrl_reg0 : read_slv_bus(X"0020", rod_bus_ce, rod_bus_addr,		-- read fmt ctrl reg
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		write_fmt_ctrl_reg : write_slv_bus(X"0020", X"20131122", rod_bus_data, 	-- write fmt ctrl reg
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		wait for 2*ref_clk40_period;
		read_fmt_ctrl_reg1 : read_slv_bus(X"0020", rod_bus_ce, rod_bus_addr,		-- read back fmt ctrl reg
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
				
		-- EFB Error Mask Tests 	
		-- Link 0-15 in slave0, Link 16-31 in slave1
		rodslv_bus_state <= "test emask";
    read_slv_bus(X"2000", rod_bus_ce, rod_bus_addr,		-- read link 0 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"2014", rod_bus_ce, rod_bus_addr,		-- read link 5 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"2028", rod_bus_ce, rod_bus_addr,		-- read link 10 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);			
		write_slv_bus(X"2000", X"12345678", rod_bus_data, -- write link 0 
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		        
		write_slv_bus(X"2014", X"ABCD1234", rod_bus_data, -- write link 5
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		         
		write_slv_bus(X"2028", X"EF1023FE", rod_bus_data, -- write link 10
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		
		read_slv_bus(X"2000", rod_bus_ce, rod_bus_addr,		-- read link 0 
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"2014", rod_bus_ce, rod_bus_addr,		-- read link 5 
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"2028", rod_bus_ce, rod_bus_addr,		-- read link 10 
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"2030", rod_bus_ce, rod_bus_addr,		-- read link 12 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"2100", rod_bus_ce, rod_bus_addr,		-- read link 16 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"2114", rod_bus_ce, rod_bus_addr,		-- read link 21 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"213C", rod_bus_ce, rod_bus_addr, 	-- read link 31 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
			
		write_slv_bus(X"0005", X"00000001", rod_bus_data, -- change slave id from to 1
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	
		write_slv_bus(X"213C", X"EFBCABCD", rod_bus_data, -- write link 31
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		read_slv_bus(X"2014", rod_bus_ce, rod_bus_addr, -- read link 5 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"213C", rod_bus_ce, rod_bus_addr, -- read link 31
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);

		write_slv_bus(X"0005", X"00000000", rod_bus_data, -- change slave id back to 0
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		read_slv_bus(X"203C", rod_bus_ce, rod_bus_addr,		-- read link 15 (expect same entry as link 31)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);  

		-- reset all error masks
		rodslv_bus_state <= "rst emask "; -- doesn't actually reset, just a pulse. to reset need to rewrite RAM
		write_slv_bus(X"2000", X"00000000", rod_bus_data, 
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		        
		write_slv_bus(X"2014", X"00000000", rod_bus_data,
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		         
		write_slv_bus(X"2028", X"00000000", rod_bus_data,
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	
		write_slv_bus(X"203C", X"00000000", rod_bus_data,
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		read_slv_bus(X"2000", rod_bus_ce, rod_bus_addr,		-- read link 0 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"2014", rod_bus_ce, rod_bus_addr,		-- read link 5 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"2028", rod_bus_ce, rod_bus_addr,		-- read link 10 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);                                      
		read_slv_bus(X"203C", rod_bus_ce, rod_bus_addr, 	-- read link 15 (expect 0's)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		
	  rodslv_bus_state <= "rst imfifo";
		write_slv_bus(X"0042", X"00000001", rod_bus_data,										-- INMEM FIFO reset
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		write_slv_bus(X"0040", X"00000001", rod_bus_data,										-- INMEM FIFO select channel 0
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		write_slv_bus(X"2200", X"0000FFFF", rod_bus_data,										-- Set S-Link data format version
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);    -- (can only configure lower 16 bits)
		write_slv_bus(X"2204", X"BC1F0001", rod_bus_data,										-- Enter Source ID (subdetector & IBL module ID)
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	
		read_slv_bus(X"221C", rod_bus_ce, rod_bus_addr,											-- read out code & board version (041B)
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		wait for 21 us;
		write_slv_bus(X"2248", X"00000001", rod_bus_data,										-- flush EFB output FIFO1
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	
		write_slv_bus(X"224A", X"00000001", rod_bus_data,										-- flush EFB output FIFO2
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	
		write_slv_bus(X"0000", X"0000ABCD", rod_bus_data,										-- enable formatter links
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
			
		wait for 14 us;
--		write_slv_bus(X"0006", X"00000001", rod_bus_data,										-- put in calibration mode
--			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);

		-- Read out INMEM FIFO 
		wait for 10 us;
		read_slv_bus(X"0044", rod_bus_ce, rod_bus_addr,											
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		read_slv_bus(X"0044", rod_bus_ce, rod_bus_addr,											
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
		read_slv_bus(X"0044", rod_bus_ce, rod_bus_addr,											
			rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);
			
    wait;
	end process;

	rod_evtID_proc: process
  begin	
		wait for 28 us;
		ev_data_rst_n <= '0';
		wait for 1 us;
		ev_data_rst_n	<= '1';
		wait for 200 ns;
		--wait for 23.5 us;
		wait for 20.8 us;
		-- need to replace L1ID and BCID
	  --             L1ID   ECRID  BCID    Dynamic Link Masks
		write_evt_id(X"0002", X"34", X"1FE", X"0000000000000000", ev_data_wen_n, ev_data_from_ppc);
		wait for 3000 ns;
		write_evt_id(X"0004", X"34", X"278", X"0000000000000000", ev_data_wen_n, ev_data_from_ppc);
		wait for 3000 ns;
		write_evt_id(X"0007", X"34", X"2F3", X"0000000000000000", ev_data_wen_n, ev_data_from_ppc);
		wait for 3000 ns;
		write_evt_id(X"000C", X"34", X"370", X"0000000000000000", ev_data_wen_n, ev_data_from_ppc);
		wait for 3000 ns;
		
		wait;
	end process;

	-- S-Link bac pressure to ROD
	slink_proc: process
	begin
		slink0_lff					<= '1';			
		slink1_lff					<= '1';			
		slink0_ldown				<= '0';		
		slink1_ldown				<= '0';	
		wait for 56 us;	
		slink0_ldown				<= '1';		
		slink1_ldown				<= '1';		
		wait;
	end process;

	histo_rfd: process
	begin	
		wait for 60 us;	
		mode0 		<= "11";
		mode1			<= "11";
		wait for 3 us;
		mode0			<= "01";
		mode1			<= "01";
		wait;
	end process;





end;
