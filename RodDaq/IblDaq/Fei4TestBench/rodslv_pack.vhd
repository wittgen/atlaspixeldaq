library IEEE;
use IEEE.STD_LOGIC_1164.all;

package rodslv_pack is

	constant clk40_period : time := 25 ns;
	
	procedure read_slv_bus(
		constant address : in std_logic_vector(15 downto 0);
		signal rod_bus_ce 	: out std_logic;
		signal rod_bus_addr : out std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out std_logic;
		signal rod_bus_hwsb : out std_logic;
		signal rod_bus_ds		: out std_logic
	);

	procedure write_slv_bus(
		constant address : in std_logic_vector(15 downto 0);
		constant data_in : in std_logic_vector(31 downto 0);
		signal rod_bus_data : out std_logic_vector(15 downto 0);
		signal rod_bus_ce 	: out std_logic;
		signal rod_bus_addr : out std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out std_logic;
		signal rod_bus_hwsb : out std_logic;
		signal rod_bus_ds		: out std_logic
	);

	procedure write_evt_id(
		constant L1ID				: in std_logic_vector(15 downto 0);
		constant ECRID			: in std_logic_vector( 7 downto 0);
		constant BCID  			: in std_logic_vector(11 downto 0);
		constant link_masks	: in std_logic_vector(63 downto 0);
		signal ev_data_wen_n 		: out std_logic;
		signal ev_data_from_ppc : out std_logic_vector(15 downto 0)
	);

end rodslv_pack;

package body rodslv_pack is

	procedure read_slv_bus(
		constant address : in std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '1'; rod_bus_hwsb <= '0';
		rod_bus_ds <= '0';
	  wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	  rod_bus_hwsb <= '1'; wait for clk40_period*2;
    rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';		
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
    wait for clk40_period; rod_bus_ce <= '0';	wait for clk40_period*8;	
	end read_slv_bus;

  procedure write_slv_bus(
		constant address : in std_logic_vector(15 downto 0);
		constant data_in : in std_logic_vector(31 downto 0);
		signal rod_bus_data : out std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '0'; rod_bus_hwsb <= '0';
		rod_bus_data <= data_in(15 downto 0);
    wait for clk40_period*3; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	  rod_bus_hwsb <= '1'; rod_bus_data <= data_in(31 downto 16);
		wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
    wait for clk40_period; 
		rod_bus_ce <= '0'; rod_bus_rnw <= '1'; rod_bus_data <= (others => 'Z');	
		wait for clk40_period*8;			
	end write_slv_bus;
  
	-- Event ID format from ROD Master to ROD Slave
	-- word 0 : L1 ID[15:0]                                ev_data_sh_reg(15: 0)	
	-- word 1 : ECR ID[7:0] & L1 ID[23:16]                 ev_data_sh_reg(31:16)
	-- word 2 : BC ID[11:0] & x & RoL & BOC OK & TIM OK    ev_data_sh_reg(47:32)  -- RoL: readout link test
	-- word 3 : TT - ROD[5:0] & TIM[1:0] & ATLAS[7:0]      ev_data_sh_reg(63:48)
	-- word 4 : Dynamic Mask for Links [ 7: 0]             ev_data_sh_reg(79:64)
	-- word 5 : Dynamic Mask for Links [ 15: 8]            ev_data_sh_reg(95:80)
	-- word 6 : Dynamic Mask for Links [ 23: 16]           ev_data_sh_reg(111:96)
	-- word 7 : Dynamic Mask for Links [ 31: 24]           ev_data_sh_reg(127:112)
	
	procedure write_evt_id(
		constant L1ID				: in std_logic_vector(15 downto 0);
		constant ECRID			: in std_logic_vector( 7 downto 0);
		constant BCID  			: in std_logic_vector(11 downto 0);
		constant link_masks	: in std_logic_vector(63 downto 0);
		signal ev_data_wen_n 		: out std_logic;
		signal ev_data_from_ppc : out std_logic_vector(15 downto 0))is
	begin	
	  ev_data_wen_n <= '0'; 
		ev_data_from_ppc <= L1ID;         	
		wait for clk40_period;
		ev_data_from_ppc <= ECRID & X"00";	
		wait for clk40_period;		
		ev_data_from_ppc <= BCID & X"0";		
		wait for clk40_period;		
		ev_data_from_ppc <= X"ABCD";				
		wait for clk40_period;		
		ev_data_from_ppc <= link_masks(15 downto  0);	wait for clk40_period;	
		ev_data_from_ppc <= link_masks(31 downto 16); wait for clk40_period;	
		ev_data_from_ppc <= link_masks(47 downto 32);	wait for clk40_period;	
		ev_data_from_ppc <= link_masks(63 downto 48);	wait for clk40_period;		
		ev_data_wen_n <= '1'; 
		wait for clk40_period*8;
	end write_evt_id;
	
	
end rodslv_pack;
