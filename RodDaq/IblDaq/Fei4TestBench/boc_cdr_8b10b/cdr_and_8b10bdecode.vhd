library IEEE;
use IEEE.STD_LOGIC_1164.ALL; 

library work;
use work.bocpack.all;

entity cdr_and_8b10bdecode is
	port
	(
		clk40mhz : IN std_logic;
		din_fei4 : IN std_logic;
		reset : IN std_logic;          
	 	dec_dout : OUT std_logic_vector(7 downto 0);
	 	dec_kchar : OUT std_logic;
	 	dec_dout_valid : OUT std_logic;
	 	dec_decErr : OUT std_logic;
	 	dec_dispErr : OUT std_logic
	);
end entity;

architecture cdr_and_8b10bdecode_arch of cdr_and_8b10bdecode is
   
   constant clk160mhz_period : time := 6.25 ns;
   constant clk640mhz_period : time := 1.5625 ns;

   signal clk160mhz : std_logic := '0';
   signal clk160mhz_90 : std_logic := '0';
   signal clk640mhz : std_logic := '0';

	
	signal data_cdr_out : CDR_record;
	
	signal align_din : std_logic_vector(1 downto 0) := "00";
	signal align_din_valid : std_logic_vector(1 downto 0) := "00";	
	signal align_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal align_dout_valid : std_logic := '0';
	signal align_sync : std_logic := '0';
	
	signal clkcross_din : std_logic_vector(9 downto 0) := (others => '0');
	signal clkcross_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal clkcross_rden : std_logic := '0';
	signal clkcross_wren : std_logic := '0';
	signal clkcross_full : std_logic := '0';
	signal clkcross_empty : std_logic := '0';

	signal dec_din : std_logic_vector(9 downto 0) := (others => '0');
	signal dec_din_valid : std_logic := '0';
	signal dec_din_valid_async : std_logic := '0';

	component cdr_deg90
		port
		(
			-- clocks
			clk160 : in std_logic;
			clk160_90 : in std_logic;

			-- reset
			reset : in std_logic;

			-- data input
			din : in std_logic;

			-- data output
			data : out CDR_record
		);
	end component;
	

	component data_alignment
		port
		(
			clk : in std_logic;
			reset : in std_logic;
		
			din : in std_logic_vector(1 downto 0);
			din_valid : in std_logic_vector(1 downto 0);
		
			dout : out std_logic_vector(9 downto 0);
			dout_valid : out std_logic;
			dout_sync : out std_logic
		);
	end component;

	COMPONENT rx_clkcross
	  PORT (
		 wr_clk : IN STD_LOGIC;
		 rd_clk : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC
	  );
	END COMPONENT;

	COMPONENT decode_8b10b_wrapper
	PORT(
		CLK : IN std_logic;
		DIN : IN std_logic_vector(9 downto 0);
		CE : IN std_logic;
		SINIT : IN std_logic;          
		DOUT : OUT std_logic_vector(7 downto 0);
		KOUT : OUT std_logic;
		CODE_ERR : OUT std_logic;
		DISP_ERR : OUT std_logic;
		ND : OUT std_logic
		);
	END COMPONENT;
	
begin
	
   -- Clock process definitions
   clock160_process :process
   begin
		clk160mhz <= '0';
		wait for clk160mhz_period/2;
		clk160mhz <= '1';
		wait for clk160mhz_period/2;

   end process;

	clk160mhz_90 <= clk160mhz after 1.5625 ns;
 
  -- Clock process definitions
   clock640_process :process
   begin
		clk640mhz <= '0';
		wait for clk640mhz_period/2;
		clk640mhz <= '1';
		wait for clk640mhz_period/2;

   end process;

	cdr_deg90_i: cdr_deg90 PORT MAP(
		clk160 => clk160mhz,
		clk160_90 => clk160mhz_90,
		reset => reset,
		din => din_fei4,
		data => data_cdr_out
	);
	
	align_din <= data_cdr_out.value;
	align_din_valid <= data_cdr_out.valid;

-- data alignment
align: data_alignment PORT MAP(
	clk => clk160mhz,
	reset => reset,
	din => align_din,
	din_valid => align_din_valid,
	dout => align_dout,
	dout_valid => align_dout_valid,
	dout_sync => align_sync
);

-- fill clock crossing FIFO
clkcross_din <= align_dout;
clkcross_wren <= align_dout_valid and not clkcross_full and align_sync;

-- clock crossing FIFO
clkcross_i: rx_clkcross
  PORT MAP (
    wr_clk => clk160mhz,
    rd_clk => clk40mhz,
    din => clkcross_din,
    wr_en => clkcross_wren,
    rd_en => clkcross_rden,
    dout => clkcross_dout,
    full => clkcross_full,
    empty => clkcross_empty
  );

-- register input data to the decoder
process begin
	wait until rising_edge(clk40mhz);

	-- we need to reverse the bit order of 8b10b data
	for I in 0 to 9 loop
		dec_din(I) <= clkcross_dout(9-I);
	end loop;

	clkcross_rden <= not clkcross_empty and not dec_din_valid;
	dec_din_valid <= not clkcross_empty and not dec_din_valid;
	--dec_din_valid_delay <= dec_din_valid;
	--dec_dout_valid <= dec_din_valid_delay;
	dec_dout_valid <= dec_din_valid;
end process;

-- 8b10b decoder
dec: decode_8b10b_wrapper PORT MAP(
	CLK => clk40mhz,
	DIN => dec_din,
	CE => dec_din_valid,
	SINIT => '0',
	DOUT => dec_dout,
	KOUT => dec_kchar,
	CODE_ERR => dec_decErr,
	DISP_ERR => dec_dispErr,
	ND => open
);

end architecture;
