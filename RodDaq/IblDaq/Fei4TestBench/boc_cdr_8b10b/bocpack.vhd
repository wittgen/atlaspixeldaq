----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				Mighty Eval BOC MAgic Number Lib
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 29.11.2011 - Initial Version
-- 14.12.2011 - Added FE-I4 words enc/dec
----------------------------------------------------------------------------------
-- TODO:
-- 22.03.2011 - Get all magic numbers, do the right mapping and keep up2date
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use ieee.Numeric_Std.all;
use IEEE.std_logic_unsigned.all;

package bocpack is
	-- register bank types
	type RegBank_wr_t is array (natural range <>) of std_logic_vector(7 downto 0);
	type RegBank_rd_t is array (natural range <>) of std_logic_vector(7 downto 0);
	type RegBank_wren_t is array (natural range <>) of std_logic;
	type RegBank_rden_t is array (natural range <>) of std_logic;

	-- mux types
	type Mux_BOC2ROD_data is array (natural range <>) of std_logic_vector(7 downto 0);
	type Mux_BOC2ROD_addr is array (natural range <>) of std_logic_vector(1 downto 0);
	type Mux_BOC2ROD_valid is array (natural range <>) of std_logic;
	type Mux_BOC2ROD_ctrl is array (natural range <>) of std_logic;

	-- CDR data
	type CDR_record is record
		value : std_logic_vector(1 downto 0);
		valid : std_logic_vector(1 downto 0);
	end record;
	type CDR_record_array is array (natural range <>) of CDR_record;
	
	-- FE-I4 Encoded Komma Words used for Alignment of the Decoder
	constant idle_word : std_logic_vector(9 downto 0) := "0011111001";
	constant sof_word : std_logic_vector(9 downto 0) := "0011111000";
	constant eof_word : std_logic_vector(9 downto 0) := "0011111010";
	
	-- Count of Bits before a new sync word is necessary (currently arbitrary)
	constant fei4syncPeriod: integer := 120000;

	-- use SERDES as input for clock-data recovery
	constant use_SERDES : boolean := true;

	-- number of TX/RX/Mux
	constant nTX : integer := 8;
	constant nRX : integer := 16;
	constant nMux : integer := 4;

	-- board revision string
	constant board_revision : string := "B";
	
	-- include BERT
	constant IncludeBERT : boolean := false;
end bocpack;
