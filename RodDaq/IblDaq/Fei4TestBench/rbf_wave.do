onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -label {FEI4 RST_N} /tb_rodslv_boc_fei4/RA1bar
add wave -noupdate -color Pink -label {Ref Clk 40 MHz (P)} /tb_rodslv_boc_fei4/Ref_Clk_P
add wave -noupdate -label {FEI4 Cmd_ExtTrigger} /tb_rodslv_boc_fei4/Cmd_ExtTrigger
add wave -noupdate -label {FEI4 DCI} /tb_rodslv_boc_fei4/DCI
add wave -noupdate -color Thistle -label {FEI4 DOB_OUT_P} /tb_rodslv_boc_fei4/DOB_OUT_P
add wave -noupdate -label HitOr_P /tb_rodslv_boc_fei4/HitOr_P

add wave -noupdate -color Gold -label {8B10 Decoder Out} -radix hexadecimal /tb_rodslv_boc_fei4/dec_dout
add wave -noupdate -color Gold -label {8B10 Decoder Out d1} -radix hexadecimal /tb_rodslv_boc_fei4/dec_dout_d1
add wave -noupdate -color Gold -label {8B10 Decoder Out d2} -radix hexadecimal /tb_rodslv_boc_fei4/dec_dout_d2
add wave -noupdate -color Gold -label {Calculated L1ID} /tb_rodslv_boc_fei4/l1id
add wave -noupdate -color Gold -label {Calculated BCID} /tb_rodslv_boc_fei4/bcid
add wave -noupdate -color Cyan -label {8B10 dout valid} /tb_rodslv_boc_fei4/dec_dout_valid
add wave -noupdate -color Cyan -label {8B10 Kchar} /tb_rodslv_boc_fei4/dec_kchar

add wave -noupdate -color Cyan -label {Clk 80 MHz} /tb_rodslv_boc_fei4/clk80_p

add wave -noupdate -label {BOC MUX Rx state} /tb_rodslv_boc_fei4/boc_2_rod_mux/rx_state
add wave -noupdate -label {BOC MUX FIFO read enable} /tb_rodslv_boc_fei4/boc_2_rod_mux/fifo_rd_en

add wave -noupdate -color Cyan -label {Addr to ROD} -radix hexadecimal /tb_rodslv_boc_fei4/addr2rod
add wave -noupdate -color Cyan -label {Data to ROD} -radix hexadecimal /tb_rodslv_boc_fei4/data2rod
add wave -noupdate -color Cyan -label {Ctrl to ROD} /tb_rodslv_boc_fei4/ctl2rod
add wave -noupdate -color Cyan -label {Valid to ROD} /tb_rodslv_boc_fei4/valid2rod

add wave -noupdate -color Yellow -label {ROD slv top rst} /tb_rodslv_boc_fei4/rod_slave/reset
add wave -noupdate -color Yellow -label {ROD slv dcm locked} /tb_rodslv_boc_fei4/rod_slave/boc_dcm_locked_i
add wave -noupdate -color Yellow -label {ROD slv rst i} /tb_rodslv_boc_fei4/rod_slave/rst_i
add wave -noupdate -color Yellow -label {ROD slv clk40} /tb_rodslv_boc_fei4/rod_slave/clk40 
add wave -noupdate -color Yellow -label {ROD slv clk80} /tb_rodslv_boc_fei4/rod_slave/clk80 


add wave -noupdate -color Green -label {ROD Bus ce} /tb_rodslv_boc_fei4/rod_bus_ce
add wave -noupdate -color Green -label {ROD Bus addr} -radix hexadecimal /tb_rodslv_boc_fei4/rod_bus_addr
add wave -noupdate -color Green -label {ROD Bus rnw} /tb_rodslv_boc_fei4/rod_bus_rnw
add wave -noupdate -color Green -label {ROD Bus hwsb} /tb_rodslv_boc_fei4/rod_bus_hwsb
add wave -noupdate -color Green -label {ROD Bus ds} /tb_rodslv_boc_fei4/rod_bus_ds
add wave -noupdate -color Green -label {ROD Bus data} -radix hexadecimal /tb_rodslv_boc_fei4/rod_bus_data
add wave -noupdate -color Green -label {ROD Bus ack} /tb_rodslv_boc_fei4/rod_bus_ack_out


add wave -noupdate -color Blue -label {INMEM FIFO rst} /tb_rodslv_boc_fei4/rod_slave/inmem_rb_rst_i
add wave -noupdate -color Blue -label {INMEM FIFO sel} /tb_rodslv_boc_fei4/rod_slave/inmemSel_i
add wave -noupdate -color Blue -label {INMEM FIFO wen} /tb_rodslv_boc_fei4/rod_slave/inmemFifo_wen_i
add wave -noupdate -color Blue -label {INMEM FIFO din} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/inmemFifo_din_i
add wave -noupdate -color Blue -label {INMEM FIFO ren} /tb_rodslv_boc_fei4/rod_slave/inmemFifo_ren_i
add wave -noupdate -color Blue -label {INMEM FIFO dout} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/inmem_regData_i
add wave -noupdate -color Blue -label {INMEM FIFO empty} /tb_rodslv_boc_fei4/rod_slave/inmemFifo_ef_i


add wave -noupdate -color Cyan -label {ROD reg slave id} /tb_rodslv_boc_fei4/rod_slave/slave_id_i
add wave -noupdate -color Cyan -label {ROD reg fmtlink enabled} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/register_block/link_enabled_i

add wave -noupdate -color Cyan -label {ROD slv ev_data_ready(0)} /tb_rodslv_boc_fei4/rod_slave/ev_data_ready(0)
add wave -noupdate -color Pink -label {PPC Event Data} -radix hexadecimal /tb_rodslv_boc_fei4/ev_data_from_ppc
add wave -noupdate -color Pink -label {Event Data wen_n} /tb_rodslv_boc_fei4/ev_data_wen_n
add wave -noupdate -color Pink -label {Event Data rst_n} /tb_rodslv_boc_fei4/ev_data_rst_n
add wave -noupdate -color Pink -label {Event Data afull_n} /tb_rodslv_boc_fei4/ev_data_almost_full_n
add wave -noupdate -color Pink -label {Event Data ready} /tb_rodslv_boc_fei4/ev_data_ready					
add wave -noupdate -color Pink -label {Event ID FIFO empty err} /tb_rodslv_boc_fei4/ev_id_fifo_empty_error

add wave -noupdate -color Red  -label {Evt Data in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_data_in
add wave -noupdate -color Red  -label {Evt Data wen_n} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_data_wen_n
add wave -noupdate -color Red  -label {Evt Header ID} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/header_ev_id_data
add wave -noupdate -color Red  -label {Decoded Evt ID 1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_data1
add wave -noupdate -color Red  -label {Decoded ID wen 1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_wen1
add wave -noupdate -color Red  -label {Decoded Evt ID 2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_data2
add wave -noupdate -color Red  -label {Decoded ID wen 2} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_wen2
add wave -noupdate -color Red  -label {fmt link enabled} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/fmt_link_enabled
add wave -noupdate -color Red  -label {Evt Data Empty} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_data_empty
add wave -noupdate -color Red  -label {Mask Sel expect 01} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/mask_sel

add wave -noupdate -color Pink -label {data2rod} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data2rod1 					
add wave -noupdate -color Pink -label {addr2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/addr2rod1 					
add wave -noupdate -color Pink -label {valid2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid2rod1 					
add wave -noupdate -color Pink -label {ctl2rod} /tb_rodslv_boc_fei4/rod_slave/half_slave1/ctl2rod1 					


add wave -noupdate -color Red -label {fmt1 link0 clk} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/clk80            						
add wave -noupdate -color Green -label {fmt1 link0 reg enable} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/reg_enable(0)
add wave -noupdate -color Green -label {fmt1 link0 ctrl sig} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/cntrl_from_boc_i	
add wave -noupdate -color Yellow -label {fmt1 link0 data_in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/data_in
add wave -noupdate -color Yellow -label {fmt1 link0 counter} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/counter
add wave -noupdate -color cyan -label {fmt1 link0 data reg} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/data_reg
add wave -noupdate -color cyan -label {fmt1 link0 reg_en1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/register_enable1
add wave -noupdate -color Pink -label {fmt1 link0 link data} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_encoder0/link_data			
add wave -noupdate -color Pink -label {fmt1 link0 link valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i(0)	


add wave -noupdate -color Yellow -label {fmt1 link valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i
add wave -noupdate -color Yellow -label {fmt1 fifo valid} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_valid
add wave -noupdate -color Yellow -label {fmt1 link data 0} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_0
add wave -noupdate -color Yellow -label {fmt1 link data 1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_1
add wave -noupdate -color Yellow -label {fmt1 link data 2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_2
add wave -noupdate -color Yellow -label {fmt1 link data 3} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_data_3

add wave -noupdate -color Pink -label {fmt1 Link_valid_i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_valid_i
add wave -noupdate -color Pink -label {fmt1 fifo ren_i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_ren_i
add wave -noupdate -color Pink -label {fmt1 link_fifo_data_i} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/link_fifo_data_i

add wave -noupdate -color Red -label {FIFO ctrlr readout state} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/readout_state
add wave -noupdate -color Red -label {FIFO ctrlr link mb i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/link_mb_i
add wave -noupdate -color Red -label {FIFO ctrlr hold output in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/hold_output_in
add wave -noupdate -color Red -label {FIFO ctrlr fifo ren i} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/fifo_ren_i
add wave -noupdate -color Red -label {FIFO ctrlr link num} /tb_rodslv_boc_fei4/rod_slave/half_slave1/formatter_instance1/fifo_readout_controller_instance/link_num

add wave -noupdate -color Pink -label {fmt1 data to efb} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_to_efb1_i
add wave -noupdate -color Pink -label {fmt1 valid to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid_to_efb1_i

add wave -noupdate -color Red -label {boe1 to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/boe1_i
add wave -noupdate -color Red -label {boe2 to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/boe2_i
add wave -noupdate -color Red -label {boe1 to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/eoe1_i
add wave -noupdate -color Red -label {boe2 to efb} /tb_rodslv_boc_fei4/rod_slave/half_slave1/eoe2_i



add wave -noupdate -color Orange -label {efb fmt din1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/data_in1         
add wave -noupdate -color Orange -label {efb fmt din2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/data_in2         
add wave -noupdate -color Orange -label {efb fmt link enabled} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/fmt_link_enabled	 
add wave -noupdate -color Orange -label {efb fmt data valid2} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/fmt_data_valid_in1
add wave -noupdate -color Orange -label {efb fmt data valid2} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/fmt_data_valid_in2
add wave -noupdate -color Orange -label {efb emask sel1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/err_mask_sel1
add wave -noupdate -color Orange -label {efb ev_data_from_ppc} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_data_from_ppc      
add wave -noupdate -color Orange -label {efb ev_data_wen_n} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_data_wen_n         
add wave -noupdate -color Orange -label {efb ev_data_rst_n} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_data_rst_n         
add wave -noupdate -color Orange -label {efb ev_data_almost_full_n} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_data_almost_full_n 
add wave -noupdate -color Orange -label {efb ev_data_ready} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_data_ready         
add wave -noupdate -color Orange -label {efb ev_id_fifo_empty_error} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/ev_id_fifo_empty_error
add wave -noupdate -color Orange -label {efb halt output} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/gatherer_halt_output

add wave -noupdate -color Orange -label {efb evid din} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_data_in
add wave -noupdate -color Orange -label {efb evid wen1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_wen1
add wave -noupdate -color Orange -label {efb evid id} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/ev_id_data1
add wave -noupdate -color Orange -label {efb evid hdr} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_data_decode/header_ev_id_data

add wave -noupdate -color Orange -label {efb bcl1 din} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/data_in         
add wave -noupdate -color Orange -label {efb bcl1 valid in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/data_valid_in 
add wave -noupdate -color Orange -label {efb bcl1 fmt boe} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/fmt_beg_of_event
add wave -noupdate -color Orange -label {efb bcl1 fmt eoe} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/fmt_end_of_event

add wave -noupdate -color Green -label {efb bcl1 din d3} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/data_in_d3 
add wave -noupdate -color Yellow -label {efb bcl1 expected l1id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/expected_l1  
add wave -noupdate -color Yellow -label {efb bcl1 expected bcid} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/bc_l1_check1/num_accept_bc 

add wave -noupdate -color Cyan -label {fifo header_ev_id_data_in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_data_in
add wave -noupdate -color Cyan -label {fifo header_ev_id_wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_wen
add wave -noupdate -color Cyan -label {fifo header_ev_id_ren} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_ren
add wave -noupdate -color Cyan -label {fifo header_ev_id_data_out} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_data_out



add wave -noupdate -color Orange -label {efb errd1 data_in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/err_detect1/data_in       
add wave -noupdate -color Orange -label {efb errd1 data_valid_in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/err_detect1/data_valid_in 
add wave -noupdate -color Orange -label {efb errd1 data_out} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/err_detect1/data_out      
add wave -noupdate -color Orange -label {efb errd1 data_valid_out} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/err_detect1/data_valid_out 

add wave -noupdate -color Orange -label {efb fmtd1 data_in} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data1/data_in         
add wave -noupdate -color Orange -label {efb fmtd1 data_valid_in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data1/data_valid_in   
add wave -noupdate -color Orange -label {efb fmtd1 data_out} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data1/data_out        
add wave -noupdate -color Orange -label {efb fmtd1 data_valid_out_n} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data1/data_valid_out_n 

add wave -noupdate -color Orange -label {efb fmtd1 count fifo data} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data1/count_fifo_data 
add wave -noupdate -color Orange -label {efb fmtd1 count fifo wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo1_wen 
add wave -noupdate -color Orange -label {efb fmtd2 count fifo data} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/format_data2/count_fifo_data 
add wave -noupdate -color Orange -label {efb fmtd2 count fifo wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo2_wen 

add wave -noupdate -color Orange -label {efb count1 fifo din} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo1_data_in 
add wave -noupdate -color Orange -label {efb count1 fifo wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo1_wen 
add wave -noupdate -color Orange -label {efb count1 fifo ren} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo1_ren 
add wave -noupdate -color Orange -label {efb count1 fifo dout} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo1_data_out

add wave -noupdate -color Orange -label {efb count2 fifo din} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo2_data_in 
add wave -noupdate -color Orange -label {efb count2 fifo wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo2_wen 
add wave -noupdate -color Orange -label {efb count2 fifo ren} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo2_ren 
add wave -noupdate -color Orange -label {efb count2 fifo dout} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/count_fifo2_data_out

add wave -noupdate -color Yellow -label {header_ev_id_data_in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_data_in     
add wave -noupdate -color Yellow -label {header_ev_id_wen} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_wen
add wave -noupdate -color Yellow -label {header_ev_id_ren} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_ren
add wave -noupdate -color Yellow -label {header_ev_id_data_out} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_data_out
add wave -noupdate -color Yellow -label {header_ev_id_full} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_full
add wave -noupdate -color Yellow -label {header_ev_id_almost_full} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_almost_full
add wave -noupdate -color Yellow -label {header_ev_id_empty} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/header_ev_id_empty

add wave -noupdate -color Orange -label {efb frag link en} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/fmt_link_enabled
add wave -noupdate -color Orange -label {efb frag start_frag_xmit} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/start_frag_xmit
add wave -noupdate -color Orange -label {efb frag err_sumry_fifo1_empty_d1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/err_sumry_fifo1_empty_d1 
add wave -noupdate -color Orange -label {efb frag err_sumry_fifo2_empty_d1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/err_sumry_fifo2_empty_d1 
add wave -noupdate -color Orange -label {efb frag count_fifo1_empty_d1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/count_fifo1_empty_d1
add wave -noupdate -color Orange -label {efb frag count_fifo2_empty_d1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/count_fifo2_empty_d1
add wave -noupdate -color Orange -label {efb frag hold_next_event_in} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/hold_next_event_in


add wave -noupdate -color Orange -label {efb1 frag state} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/gen_fragment_state
add wave -noupdate -color Orange -label {efb1 frag omem1 play} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/out_mem1_play
add wave -noupdate -color Orange -label {efb1 frag omem1 done} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/out_mem1_done
add wave -noupdate -color Orange -label {efb1 frag omem2 play} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/out_mem2_play
add wave -noupdate -color Orange -label {efb1 frag omem2 done} /tb_rodslv_boc_fei4/rod_slave/half_slave1/efb_instance/event_fragment/out_mem2_done


add wave -noupdate -color Orange -label {efb2 frag state} /tb_rodslv_boc_fei4/rod_slave/half_slave2/efb_instance/event_fragment/gen_fragment_state
add wave -noupdate -color Orange -label {efb2 frag omem1 play} /tb_rodslv_boc_fei4/rod_slave/half_slave2/efb_instance/event_fragment/out_mem1_play
add wave -noupdate -color Orange -label {efb2 frag omem1 done} /tb_rodslv_boc_fei4/rod_slave/half_slave2/efb_instance/event_fragment/out_mem1_done
add wave -noupdate -color Orange -label {efb2 frag omem2 play} /tb_rodslv_boc_fei4/rod_slave/half_slave2/efb_instance/event_fragment/out_mem2_play
add wave -noupdate -color Orange -label {efb2 frag omem2 done} /tb_rodslv_boc_fei4/rod_slave/half_slave2/efb_instance/event_fragment/out_mem2_done



add wave -noupdate -color Cyan -label {data2rtr1_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave1/k_word_i          
add wave -noupdate -color Green -label {data2rtr1_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_chip_id_i   
add wave -noupdate -color Pink -label {efb_data2rtr1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr1} /tb_rodslv_boc_fei4/rod_slave/half_slave1/valid_to_router_i
add wave -noupdate -color Yellow -label {toHisto0 chip ID} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/chip0_i		
add wave -noupdate -color Yellow -label {toHisto0 hitValid} /tb_rodslv_boc_fei4/rod_slave/hitValid0_i
add wave -noupdate -color Yellow -label {toHisto0 Row} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/row0_i     
add wave -noupdate -color Yellow -label {toHisto0 Col} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/col0_i		
add wave -noupdate -color Yellow -label {toHisto0 ToT} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/totVal0_i   

add wave -noupdate -color Cyan -label {data2rtr2_kword} /tb_rodslv_boc_fei4/rod_slave/half_slave2/k_word_i          
add wave -noupdate -color Green -label {data2rtr2_chip_id} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_chip_id_i  
add wave -noupdate -color Pink -label {efb_data2rtr2} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave2/data_to_router_i
add wave -noupdate -color Pink -label {efb_valid2rtr2} /tb_rodslv_boc_fei4/rod_slave/half_slave2/valid_to_router_i
add wave -noupdate -color Yellow -label {toHisto1 chip ID} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/chip1_i	
add wave -noupdate -color Yellow -label {toHisto1 hitValid} /tb_rodslv_boc_fei4/rod_slave/hitValid1_i
add wave -noupdate -color Yellow -label {toHisto1 Row} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/row1_i   
add wave -noupdate -color Yellow -label {toHisto1 Col} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/col1_i	
add wave -noupdate -color Yellow -label {toHisto1 ToT} -radix unsigned /tb_rodslv_boc_fei4/rod_slave/totVal1_i 


add wave -noupdate -color Blue -label {calib mode} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/calib_mode 
add wave -noupdate -color Blue -label {slink_fifo_indata} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/slink_fifo_indata
add wave -noupdate -color Blue -label {slink_fifo_write} /tb_rodslv_boc_fei4/rod_slave/half_slave1/router_instance/slink_fifo_write


add wave -noupdate -color Red -label {S-Link utest} /tb_rodslv_boc_fei4/slink0_utest
add wave -noupdate -color Red -label {S-Link ureset} /tb_rodslv_boc_fei4/slink0_ureset
add wave -noupdate -color Red -label {S-Links full} /tb_rodslv_boc_fei4/slink0_lff
add wave -noupdate -color Red -label {S-Links down} /tb_rodslv_boc_fei4/slink0_ldown

add wave -noupdate -color Pink -label {S-Link0 uCtrl} /tb_rodslv_boc_fei4/slink0_uctrl
add wave -noupdate -color Pink -label {S-Link0 dout} -radix hexadecimal /tb_rodslv_boc_fei4/slink0_data_rod2boc
add wave -noupdate -color Pink -label {S-Link0 we} /tb_rodslv_boc_fei4/slink0_we

add wave -noupdate -color Cyan -label {S-Link1 uCtrl} /tb_rodslv_boc_fei4/slink1_uctrl
add wave -noupdate -color Cyan -label {S-Link1 dout} -radix hexadecimal /tb_rodslv_boc_fei4/slink1_data_rod2boc
add wave -noupdate -color Cyan -label {S-Link1 we} /tb_rodslv_boc_fei4/slink1_we



add wave -noupdate -color Green -label {cpuclk100} /tb_rodslv_boc_fei4/rod_slave/cpuclk100
add wave -noupdate -color Green -label {SSRAM2 clk} /tb_rodslv_boc_fei4/sram2_clk_ssram
add wave -noupdate -color Green -label {SSRAM2 clk fb} /tb_rodslv_boc_fei4/sram2_clk_fb

add wave -noupdate -color Yellow -label {valid to histo fifo0} /tb_rodslv_boc_fei4/rod_slave/hitValid1_i
add wave -noupdate -color Yellow -label {data to histo fifo0} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/histo0_fifo_din
add wave -noupdate -color Yellow -label {valid to histo fifo1} /tb_rodslv_boc_fei4/rod_slave/hitValid2_i
add wave -noupdate -color Yellow -label {data to histo fifo1} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/histo1_fifo_din

add wave -noupdate -color Yellow -label {histo fifo0 rden} /tb_rodslv_boc_fei4/rod_slave/rfd0

add wave -noupdate -color Reg -label {histo fifo0 valid} /tb_rodslv_boc_fei4/rod_slave/histo0_fifo_valid  
add wave -noupdate -color Red -label {histo fifo0 data hit} /tb_rodslv_boc_fei4/rod_slave/histo0_fifo_dout(23) 

add wave -noupdate -color Yellow -label {histo fifo0 valid hit} /tb_rodslv_boc_fei4/rod_slave/hfifo0_valid_hit
add wave -noupdate -color Yellow -label {histo fifo0 dout} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/histo0_fifo_dout
add wave -noupdate -color Yellow -label {histo fifo0 valid hit} /tb_rodslv_boc_fei4/rod_slave/hfifo1_valid_hit
add wave -noupdate -color Yellow -label {histo fifo1 dout} -radix hexadecimal /tb_rodslv_boc_fei4/rod_slave/histo1_fifo_dout

add wave -noupdate -color Green -label {add converter tCol0} /tb_rodslv_boc_fei4/rod_slave/histo_0/histogrammer/histUnit/converter/tCol0





TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {9028500 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 210
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {21 us}
