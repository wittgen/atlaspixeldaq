----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				BMF RX BOC 2 ROD Module
-- Description:		Logical block to receive 4 FE-I4 decoded data streams
--							and mux them with a round-robin arbiter onto the ROD RX
--							path. Original design by Andreas Kugel
----------------------------------------------------------------------------------
-- Changelog:
-- 24.02.2011 - Initial Version
-- 24.02.2011 - Added FIFO's, signals and arbiter process
-- 24.06.2011 - Updating FIFO IP to 7.2 and mapping k_word onto rod_ctrl
-- 07.07.2011 - Changed fifo read en to only be 1 for 1 clk cycle per fifo,
--					 should compensate a delay on the empty flag and prevent double
--					 writes to ROD.
-- 14.12.2011 - Deleted monitoring FIFO outputs they are unnecessary
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

--library work;
--use work.bocpack.all;

entity Mux_BOC2ROD is
	port (
		clk160 : in std_logic; -- 160MHz
		clk40 : in std_logic;
		ce80 : in std_logic;
		rst : in std_logic;

		-- From 8b10b decoder
		fei4_data_0 : in std_logic_vector(7 downto 0);
		fei4_data_1 : in std_logic_vector(7 downto 0);
		fei4_data_2 : in std_logic_vector(7 downto 0);
		fei4_data_3 : in std_logic_vector(7 downto 0);
		fei4_data_valid_0 : in std_logic;
		fei4_data_valid_1 : in std_logic;
		fei4_data_valid_2 : in std_logic;
		fei4_data_valid_3 : in std_logic;
		fei4_data_k_0 : in std_logic;
		fei4_data_k_1 : in std_logic;
		fei4_data_k_2 : in std_logic;
		fei4_data_k_3 : in std_logic;

		-- ROD RX data bus
		rx_rod_data : out std_logic_vector(7 downto 0);
		rx_rod_addr : out std_logic_vector(1 downto 0);
		rx_rod_valid : out std_logic;
		rx_rod_ctrl : out std_logic
	);
end Mux_BOC2ROD;

architecture structure of Mux_BOC2ROD is

	component rx_fifo
		port (
		rst: IN std_logic;
		wr_clk: IN std_logic;
		rd_clk: IN std_logic;
		din: IN std_logic_VECTOR(8 downto 0);
		wr_en: IN std_logic;
		rd_en: IN std_logic;
		dout: OUT std_logic_VECTOR(8 downto 0);
		full: OUT std_logic;
		empty: OUT std_logic
		);
	end component;

	attribute syn_black_box : boolean;
	attribute syn_black_box of rx_fifo: component is true;

	-- Internal signals
	-- FIFO signals
	signal wr_clk 				: std_logic;
	signal rd_clk 				: std_logic;
	
	signal fifo_rd_en 		: std_logic_vector(3 downto 0);
	
	signal fifo_dout_0 		: std_logic_vector(8 downto 0);
	signal fifo_dout_1 		: std_logic_vector(8 downto 0);
	signal fifo_dout_2 		: std_logic_vector(8 downto 0);
	signal fifo_dout_3 		: std_logic_vector(8 downto 0);
	signal fifo_din_0			: std_logic_vector(8 downto 0);
	signal fifo_din_1			: std_logic_vector(8 downto 0);
	signal fifo_din_2			: std_logic_vector(8 downto 0);
	signal fifo_din_3			: std_logic_vector(8 downto 0);
	
	signal fifo_full 			: std_logic_vector(3 downto 0);
	signal fifo_empty 		: std_logic_vector(3 downto 0);
	
	--signal ce80 : std_logic := '0';	

	-- Arbiter states
	type rx_state_type is (F0, F1, F2, F3);
	signal rx_state: rx_state_type := F0;
	
begin
	-- Signal Mapping
	wr_clk <= clk40;
	rd_clk <= clk160;

	-- generate internal CE
	--ce80 <= not ce80 when rising_edge(clk);

	fifo_din_0 <= fei4_data_k_0 & fei4_data_0;
	rx_fifo_0 : rx_fifo
			port map (
				rst => rst,
				wr_clk => wr_clk,
				rd_clk => rd_clk,
				din => fifo_din_0,
				wr_en => fei4_data_valid_0,
				rd_en => fifo_rd_en(0),
				dout => fifo_dout_0,
				full => fifo_full(0),
				empty => fifo_empty(0)
			);

	fifo_din_1 <= fei4_data_k_1 & fei4_data_1;
	rx_fifo_1 : rx_fifo
			port map (
				rst => rst,
				wr_clk => wr_clk,
				rd_clk => rd_clk,
				din => fifo_din_1,
				wr_en => fei4_data_valid_1,
				rd_en => fifo_rd_en(1),
				dout => fifo_dout_1,
				full => fifo_full(1),
				empty => fifo_empty(1)
			);	

	fifo_din_2 <= fei4_data_k_2 & fei4_data_2;
	rx_fifo_2 : rx_fifo
			port map (
				rst => rst,
				wr_clk => wr_clk,
				rd_clk => rd_clk,
				din => fifo_din_2,
				wr_en => fei4_data_valid_2,
				rd_en => fifo_rd_en(2),
				dout => fifo_dout_2,
				full => fifo_full(2),
				empty => fifo_empty(2)
			);	

	fifo_din_3 <= fei4_data_k_3 & fei4_data_3;
	rx_fifo_3 : rx_fifo
			port map (
				rst => rst,
				wr_clk => wr_clk,
				rd_clk => rd_clk,
				din => fifo_din_3,
				wr_en => fei4_data_valid_3,
				rd_en => fifo_rd_en(3),
				dout => fifo_dout_3,
				full => fifo_full(3),
				empty => fifo_empty(3)
			);
	
	rx_arbiter: process (rd_clk, rst)
	begin
		if (rst = '1') then
			rx_state <= F0;
			rx_rod_valid <= '0';
			rx_rod_addr <= "00";
			rx_rod_data <= (others => '0');
			fifo_rd_en <= (others => '0');
		elsif (rd_clk'event and rd_clk = '1') then
			fifo_rd_en <= (others => '0');
			if ce80 = '1' then
				case rx_state is
					when F0 =>
						if (fifo_empty(1) = '0') then
								rx_state <= F1;
						elsif (fifo_empty(2) = '0') then
								rx_state <= F2;
						elsif (fifo_empty(3) = '0') then
								rx_state <= F3;
						end if;
						if (fifo_empty(0) = '0' and fifo_rd_en(0) = '0') then
								fifo_rd_en <= (0 => '1', others => '0');
								rx_rod_valid <= '1';
								rx_rod_ctrl <= fifo_dout_0(8);
								rx_rod_addr <= "00";
								rx_rod_data <= fifo_dout_0(7 downto 0);
						else
								fifo_rd_en <= (others => '0');
								rx_rod_valid <= '0';
								rx_rod_ctrl <= '1';
						end if;	
					
					when F1 =>
						if (fifo_empty(2) = '0') then
								rx_state <= F2;
						elsif (fifo_empty(3) = '0') then
								rx_state <= F3;
						elsif (fifo_empty(0) = '0') then
								rx_state <= F0;
						end if;
						if (fifo_empty(1) = '0' and fifo_rd_en(1) = '0') then
								fifo_rd_en <= (1 => '1', others => '0');
								rx_rod_valid <= '1';
								rx_rod_ctrl <= fifo_dout_1(8);
								rx_rod_addr <= "01";
								rx_rod_data <= fifo_dout_1(7 downto 0);
						else
								fifo_rd_en <= (others => '0');
								rx_rod_valid <= '0';
								rx_rod_ctrl <= '1';
						end if;	
					
					when F2 =>
						if (fifo_empty(3) = '0') then
								rx_state <= F3;
						elsif (fifo_empty(0) = '0') then
								rx_state <= F0;
						elsif (fifo_empty(1) = '0') then
								rx_state <= F1;
						end if;
						if (fifo_empty(2) = '0' and fifo_rd_en(2) = '0') then
								fifo_rd_en <= (2 => '1', others => '0');
								rx_rod_valid <= '1';
								rx_rod_ctrl <= fifo_dout_2(8);
								rx_rod_addr <= "10";
								rx_rod_data <= fifo_dout_2(7 downto 0);
						else
								fifo_rd_en <= (others => '0');
								rx_rod_valid <= '0';
								rx_rod_ctrl <= '1';
						end if;	
					
					when F3 =>
						if (fifo_empty(0) = '0') then
								rx_state <= F0;
						elsif (fifo_empty(1) = '0') then
								rx_state <= F1;
						elsif (fifo_empty(2) = '0') then
								rx_state <= F2;
						end if;
						if (fifo_empty(3) = '0' and fifo_rd_en(3) = '0') then
								fifo_rd_en <= (3 => '1', others => '0');
								rx_rod_valid <= '1';
								rx_rod_ctrl <= fifo_dout_3(8);
								rx_rod_addr <= "11";
								rx_rod_data <= fifo_dout_3(7 downto 0);
						else
								fifo_rd_en <= (others => '0');
								rx_rod_valid <= '0';
								rx_rod_ctrl <= '1';
						end if;	
					
					when others =>
						rx_state <= F0;
						rx_rod_valid <= '0';
						rx_rod_ctrl <= '1';
						fifo_rd_en <= (others => '0');
				end case;
			end if;
		end if;
	end process rx_arbiter;
	
end structure;
