# Component to setup Xilinx tools
# Author: K. Potamianos (karolos.potamianos@cern.ch)

HOST=$(shell /bin/hostname)
ifneq (,$(filter pc-pix-scr% pc-atlas-cr% sbc-pix% pc-pix-ros%,$(HOST)))
$(error Please run me from a pc-pix-srv or pc-pix-fit machine!)
endif

# Using Xilinx environment variable if set
ifdef XILINX_EDK
XIL_EDK_DIR = $(XILINX_EDK)
endif

LIN=lin

# Default to 14.7 if nothing's there
XIL_EDK_DIR ?= /opt/Xilinx/14.7/ISE_DS/EDK
XIL_PPC_BIN = $(XIL_EDK_DIR)/gnu/powerpc-eabi/$(LIN)/bin/
XILINX_PATH = $(dir $(dir $(XIL_EDK_DIR)))

XIL_ENV := XILINX=$(subst EDK,ISE,$(XIL_EDK_DIR))
XIL_ENV += LD_LIBRARY_PATH=$(subst EDK,common/lib/$(LIN),$(XIL_EDK_DIR))

# Transparent change if directory from XIL_PPC_BIN does not exist
ifeq ($(shell ls -d $(XIL_PPC_BIN)), )
XIL_PPC_BIN =
endif

PPC-CC = $(XIL_PPC_BIN)powerpc-eabi-gcc
PPC-CXX = $(XIL_PPC_BIN)powerpc-eabi-g++
PPC-OBJDUMP = $(XIL_PPC_BIN)powerpc-eabi-objdump
PPC-OBJCOPY = $(XIL_PPC_BIN)powerpc-eabi-objcopy

PPC-SIZE = $(XIL_PPC_BIN)powerpc-eabi-size

LIBGEN = $(XIL_EDK_DIR)/bin/$(LIN)/libgen
ELFCHECK = $(XIL_EDK_DIR)/bin/$(LIN)/elfcheck

ifeq ($(wildcard $(PPC-CC)), )
$(error Cannot find PPC-CC. File not found -- $(PPC-CC))
endif

