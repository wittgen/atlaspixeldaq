

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <rodMaster.hxx>

#include <sleep.h>



extern void outbyte(char c); 

#ifdef __cplusplus
}
#endif



/* We don't use interrupts/exceptions. 
   Dummy definitions to reduce code size on MicroBlaze */
#ifdef __MICROBLAZE__
void _interrupt_handler () {}
void _exception_handler () {}
void _hw_exception_handler () {}
#endif


int main()
{
    int ret = 0;
    volatile unsigned long *epcLocalCtl;

    init_stdout();


    epcLocalCtl = (volatile unsigned long *)PPC_CTL_REG;
    *epcLocalCtl |= (1 << PPC_CTL_MASTER_BIT);
    *epcLocalCtl |= (1 << PPC_CTL_HPIENABLE_BIT);
   
    


    return ret;
}


