/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009 Xilinx, Inc. All Rights Reserved.
//
// You may copy and modify these files for your own internal use solely with
// Xilinx programmable logic devices and  Xilinx EDK system or create IP
// modules solely for Xilinx programmable logic devices and Xilinx EDK system.
// No rights are granted to distribute any files unless they are distributed in
// Xilinx programmable logic devices.
//
/////////////////////////////////////////////////////////////////////////////////

/*
 *      Simple SREC Bootloader
 *      This simple bootloader is provided with Xilinx EDK for you to easily re-use in your
 *      own software project. It is capable of booting an SREC format image file 
 *      (Mototorola S-record format), given the location of the image in memory.
 *      In particular, this bootloader is designed for images stored in non-volatile flash
 *      memory that is addressable from the processor. 
 *
 *      Please modify the define "FLASH_IMAGE_BASEADDR" in the blconfig.h header file 
 *      to point to the memory location from which the bootloader has to pick up the 
 *      flash image from.
 *
 *      You can include these sources in your software application project in XPS and 
 *      build the project for the processor for which you want the bootload to happen.
 *      You can also subsequently modify these sources to adapt the bootloader for any
 *      specific scenario that you might require it for.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "blconfig.h"
#include "portab.h"
#include "errors.h"
#include "srec.h"
#include "xparameters.h"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <sleep.h>


/* Defines */
#define CR       13

/* Comment the following line, if you want a smaller and faster bootloader which will be silent */
#define VERBOSE
//#define LL_VERBOSE

/* Declarations */
static void display_progress (uint32_t lines);
static uint8_t load_exec ();
static uint8_t flash_get_srec_line (uint8_t *buf);
extern void init_stdout();
static u8 getByteFromFlash();

extern int srec_line;

#ifdef __cplusplus
extern "C" {
#endif

extern void outbyte(char c); 

#ifdef __cplusplus
}
#endif

/* Data structures */
static srec_info_t srinfo;
static uint8_t sr_buf[SREC_MAX_BYTES];
static uint8_t sr_data_buf[SREC_DATA_MAX_BYTES];

//static uint8_t *flbuf;


/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01

/*
 * Page size of the Serial Flash.
 */
#define ISF_PAGE_SIZE		1056

// start address where SREC file is stored
#define FLASH_START_ADDRESS 0x0

// total flash size in bytes
#define FLASH_SIZE 8650752

// size of SREC file
#define SREC_FILE_SIZE 0

// RT global
//static u8 pageImage[ISF_PAGE_SIZE]; //comment: use ReadBuffer instead
static int currentPage = -1;
static u32 currentFlashByteAddr;

static XIsf Isf;
static XSpi Spi;
u8 IsfWriteBuffer[ISF_PAGE_SIZE + XISF_CMD_SEND_EXTRA_BYTES];

static int Status;

/*
 * Buffers used during read and write transactions.
 */
u8 ReadBuffer[ISF_PAGE_SIZE + XISF_CMD_SEND_EXTRA_BYTES] ; /* Read Buffer */
u8 WriteBuffer[ISF_PAGE_SIZE]; 				   /* Write buffer */

/************************** Function Prototypes ******************************/

static int IsfWaitForFlashNotBusy();

// end RT global

#ifdef VERBOSE
static int8_t *errors[] = { 
    "",
    "Error while copying executable image into RAM",
    "Error while reading an SREC line from flash",
    "SREC line is corrupted",
    "SREC has invalid checksum."
};
#endif

/* We don't use interrupts/exceptions. 
   Dummy definitions to reduce code size on MicroBlaze */
#ifdef __MICROBLAZE__
void _interrupt_handler () {}
void _exception_handler () {}
void _hw_exception_handler () {}
#endif


int main()
{
    uint8_t ret;

    init_stdout();

#ifdef VERBOSE    
    print ("\r\nSREC Bootloader\r\n");
    print ("Loading SREC image from flash @ address: ");    
    putnum (FLASH_START_ADDRESS);
    print ("\r\n");        
#endif

 //   flbuf = (uint8_t*)FLASH_IMAGE_BASEADDR;
    currentFlashByteAddr = FLASH_START_ADDRESS;
    ret = load_exec ();

    /* If we reach here, we are in error */
    
#ifdef VERBOSE
    if (ret > LD_SREC_LINE_ERROR) {
        print ("ERROR in SREC line: ");
        putnum (srec_line);
        print (errors[ret]);    
    } else {
        print ("ERROR: ");
        print (errors[ret]);
    }
#endif

    return ret;
}

#ifdef VERBOSE
static void display_progress (uint32_t count)
{
    /* Send carriage return */
    outbyte (CR);  
    print  ("Bootloader: Processed (0x)");
    putnum (count);
    print (" S-records");
}
#endif

static uint8_t load_exec ()
{
    uint8_t ret;
    void (*laddr)();
    int8_t done = 0;
    


    // init Flash

	print("Wait 1 ms\r\n");
	usleep(1000);
	Status = XSpi_Initialize(&Spi, SPI_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		 print("XSpi Initialize Error\r\n");
		 return XST_FAILURE;
	}

	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	Status = XSpi_Start(&Spi);
	if(Status != XST_SUCCESS) {
		 print("XSpi Start Error\r\n");
		return XST_FAILURE;
	}

	/*
	 * Disable Global interrupt to use the Spi driver in polled mode
	 * operation.
	 */
	XSpi_IntrGlobalDisable(&Spi);

	/*
	 * Initialize the In-system and Serial Flash Library.
	 */
	Status = XIsf_Initialize(&Isf, &Spi, ISF_SPI_SELECT, IsfWriteBuffer);
	if(Status != XST_SUCCESS) {
		 print("XIsf Initialize Error\r\n");
		return XST_FAILURE;
	}

    // end init Flash

    srinfo.sr_data = sr_data_buf;
    
    while (!done) {
        if ((ret = flash_get_srec_line (sr_buf)) != 0) 
            return ret;

        if ((ret = decode_srec_line (sr_buf, &srinfo)) != 0)
            return ret;
        
#ifdef LL_VERBOSE
        display_progress (srec_line);
#endif
        switch (srinfo.type) {
            case SREC_TYPE_0:
                break;
            case SREC_TYPE_1:
            case SREC_TYPE_2:
            case SREC_TYPE_3:
                memcpy ((void*)srinfo.addr, (void*)srinfo.sr_data, srinfo.dlen);
                break;
            case SREC_TYPE_5:
                break;
            case SREC_TYPE_7:
            case SREC_TYPE_8:
            case SREC_TYPE_9:
                laddr = (void (*)())srinfo.addr;
                done = 1;
                ret = 0;
                break;
        }
    }

#ifdef VERBOSE
    print ("\r\nExecuting program starting at address: ");
    putnum ((uint32_t)laddr);
    print ("\r\n");
#endif

    (*laddr)();                 
  
    /* We will be dead at this point */
    return 0;
}


static uint8_t flash_get_srec_line (uint8_t *buf)
{
    uint8_t c;
    int count = 0;



    while (1) {
        c  = getByteFromFlash(); //*flbuf++;
      	if(Status != XST_SUCCESS) return XST_FAILURE;

        //  required page and byte address



        if (c == 0xD) {   
            /* Eat up the 0xA too */
        	c  = getByteFromFlash();
        	if(Status != XST_SUCCESS) return XST_FAILURE;
            return 0;
        }
        
        *buf++ = c;
        count++;
        if (count > SREC_MAX_BYTES) 
            return LD_SREC_LINE_ERROR;
    }
}


static u8 getByteFromFlash()
{
    u32 requiredPage;
    u32 requiredAddress;
    u32 Address;
	XIsf_ReadParam ReadParam;

    requiredPage = (int) (currentFlashByteAddr / ISF_PAGE_SIZE);
    requiredAddress = (u32) (currentFlashByteAddr%ISF_PAGE_SIZE);

     if(requiredPage != currentPage){
     	Address = ((((u32)requiredPage) & 0x1FFF) << 11) & (~0x7FF);

    	ReadParam.Address = Address;
    	ReadParam.NumBytes = ISF_PAGE_SIZE;
    	ReadParam.ReadPtr = ReadBuffer;

    	/*
    	 * Perform the Read operation.
    	 */
    	Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
       	if(Status != XST_SUCCESS) {
        		 print("XIsf Read Error\r\n");
        		 return 0x0;
        }
       	Status = IsfWaitForFlashNotBusy();
       			if(Status != XST_SUCCESS) {
       				 print("IsfWaitForFlashNotBusy Error\r\n");
       				return 0x0;
       			}
       	currentPage = requiredPage;

     }
     currentFlashByteAddr++;
     return ReadBuffer[requiredAddress + XISF_CMD_SEND_EXTRA_BYTES];

}

static int IsfWaitForFlashNotBusy()
{
	int Status;
	u8 StatusReg;
	u8 ReadBuffer[2];

	while(1) {

		/*
		 * Get the Status Register.
		 */
		Status = XIsf_GetStatus(&Isf, ReadBuffer);
		if(Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Check if the Serial Flash is ready to accept the next
		 * command. If so break.
		 */
		StatusReg = ReadBuffer[BYTE2];
		if(StatusReg & XISF_SR_IS_READY_MASK) {
			break;
		}
	}

	return XST_SUCCESS;
}

#ifdef __PPC__

#include <unistd.h>

/* Save some code and data space on PowerPC 
   by defining a minimal exit */
void exit (int ret)
{
    _exit (ret);
}
#endif
