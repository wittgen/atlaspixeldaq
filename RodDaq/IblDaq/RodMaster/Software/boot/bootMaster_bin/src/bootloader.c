
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "blconfig.h"
#include "portab.h"
#include "errors.h"
#include "xparameters.h"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <sleep.h>


/* Defines */
#define CR       13

/* Comment the following line, if you want a smaller and faster bootloader which will be silent */
#define VERBOSE

/* Declarations */

static uint8_t load_exec ();

extern void init_stdout();
static u8 getByteFromFlash();

extern int srec_line;

#ifdef __cplusplus
extern "C" {
#endif

extern void outbyte(char c); 

#ifdef __cplusplus
}
#endif

/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01

// start address where SREC file is stored
#define FLASH_START_ADDRESS 0x0

// total flash size in bytes
#define FLASH_SIZE 8650752

// size of SREC file
#define SREC_FILE_SIZE 0

// RT global
static int currentPage = -1;
static u32 currentFlashByteAddr;

static XIsf Isf;
static XSpi Spi;
u8 IsfWriteBuffer[1056/*ISF_PAGE_SIZE*/ + XISF_CMD_SEND_EXTRA_BYTES];

/*
 * Page size of the Serial Flash.
 */
int ISF_PAGE_SIZE;

static int Status;

/*
 * Buffers used during read and write transactions.
 */
u8 ReadBuffer[1056/*ISF_PAGE_SIZE*/ + XISF_CMD_SEND_EXTRA_BYTES] ; /* Read Buffer */
u8 WriteBuffer[1056/*ISF_PAGE_SIZE*/]; 				   /* Write buffer */


union sizeofbin_t{
  int readlength; 
  u8 bytes[4]; 
}sizeofbin; 



/************************** Function Prototypes ******************************/

static int IsfWaitForFlashNotBusy();

/* We don't use interrupts/exceptions. 
   Dummy definitions to reduce code size on MicroBlaze */
#ifdef __MICROBLAZE__
void _interrupt_handler () {}
void _exception_handler () {}
void _hw_exception_handler () {}
#endif
uint8_t * ram=0x0;

int main()
{
    uint8_t ret;

    init_stdout();

#ifdef VERBOSE    
    xil_printf ("\r\nBIN Bootloader\r\n");
    xil_printf ("Loading BIN image from flash @ address: ");    
    putnum (FLASH_START_ADDRESS);
    xil_printf ("\r\n");        
#endif

 //   flbuf = (uint8_t*)FLASH_IMAGE_BASEADDR;
    currentFlashByteAddr = FLASH_START_ADDRESS;
    ret = load_exec ();

    /* If we reach here, we are in error */
    
    
    return ret;
}

static uint8_t load_exec ()
{
  unsigned long* entry_p;
  unsigned long ipage;    
  u32 Address;
  XIsf_ReadParam ReadParam;
  u32 ul;

  void (*boot)();
    // init Flash
  
  usleep(1000);
  Status = XSpi_Initialize(&Spi, SPI_DEVICE_ID);
	if(Status != XST_SUCCESS) {
	  xil_printf("XSpi Initialize Error\r\n");
	  return XST_FAILURE;
	}
	
	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	Status = XSpi_Start(&Spi);
	if(Status != XST_SUCCESS) {
	  xil_printf("XSpi Start Error\r\n");
	  return XST_FAILURE;
	}

	/*
	 * Disable Global interrupt to use the Spi driver in polled mode
	 * operation.
	 */

	XSpi_IntrGlobalDisable(&Spi);

	/*
	 * Initialize the In-system and Serial Flash Library.
	 */

	Status = XIsf_Initialize(&Isf, &Spi, ISF_SPI_SELECT, IsfWriteBuffer);
	if(Status != XST_SUCCESS) {
	  xil_printf("XIsf Initialize Error\r\n");
	  return XST_FAILURE;
	}

	u8 deviceInfo[5];
	Status = XIsf_GetDeviceInfo(&Isf, deviceInfo);
	if(Status != XST_SUCCESS || deviceInfo[1] != 0x1F || deviceInfo[2] != 0x28 || deviceInfo[3] != 0x00) {
		xil_printf("XIsf Initialize Error\r\n");
		return XST_FAILURE;
	}
	if(deviceInfo[4] == 0x01) {
		ISF_PAGE_SIZE = 264;
		currentFlashByteAddr = 16380 *ISF_PAGE_SIZE ;
	}
	else if(deviceInfo[4] == 0x00) {
		ISF_PAGE_SIZE = 1056;
		currentFlashByteAddr = 4095 *ISF_PAGE_SIZE ;
	}
	else {
		xil_printf("Error in reading Flash Pagew Size\r\n");
		return XST_FAILURE;
	}	


	int i;

	for(i=0;i<4;++i)
	  {
	    sizeofbin.bytes[3-i]=getByteFromFlash();
	  }

	xil_printf("size del bin: %d\r\n",sizeofbin.readlength);



	for (ipage=0; ipage <=((sizeofbin.readlength)/ISF_PAGE_SIZE); ++ipage) // number of pages
	  {
	   if(ISF_PAGE_SIZE == 1056){
	    	  Address = ((((u32)ipage) & 0x1FFF) << 11) & (~0x7FF);   
	    }
	    else if(ISF_PAGE_SIZE == 264){
	  	  Address = ((((u32)ipage) & 0x7FFF) << 9) & (~0x1FF);       
	    }
	    else {
		   xil_printf("Flash Page Size undefined\r\n");
		  return 1;
	    }	  
	    ReadParam.Address = Address;
	    ReadParam.NumBytes = ISF_PAGE_SIZE;
	    ReadParam.ReadPtr = ReadBuffer;

	    /*
	     * Perform the Read operation.
	     */


	    Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
	    if(Status != XST_SUCCESS) {
	      xil_printf("XIsf Read Error\r\n");
	      return 0x0;
	    }
	    Status = IsfWaitForFlashNotBusy();
	    if(Status != XST_SUCCESS) {
	      xil_printf("IsfWaitForFlashNotBusy Error\r\n");
	      return 0x0;
	    }

	    for (ul=0;ul<ISF_PAGE_SIZE;ul++)
	      ram[ul+ISF_PAGE_SIZE*ipage]=ReadBuffer[4+ul];

	  }
	xil_printf("Booting... \r\n");

	entry_p=(unsigned long*) 0x4b0;
	boot=(void*) entry_p;
	(*boot)();

	xil_printf ("starting Bin...");
	return 0;

}



static u8 getByteFromFlash()
{
    u32 requiredPage;
    u32 requiredAddress;
    u32 Address;
	XIsf_ReadParam ReadParam;

    requiredPage = (int) (currentFlashByteAddr / ISF_PAGE_SIZE);
    requiredAddress = (u32) (currentFlashByteAddr%ISF_PAGE_SIZE);

     if(requiredPage != currentPage){
     	if(ISF_PAGE_SIZE == 1056){
		Address = ((((u32)requiredPage) & 0x1FFF) << 11) & (~0x7FF);    
	}
	else if(ISF_PAGE_SIZE == 264){
		Address = ((((u32)requiredPage) & 0x7FFF) << 9) & (~0x1FF);       
	}
	else {
		xil_printf("Flash Page Size undefined\r\n");
		return 1;
	}	

    	ReadParam.Address = Address;
    	ReadParam.NumBytes = ISF_PAGE_SIZE;
	ReadParam.ReadPtr = ReadBuffer;
		//ReadParam.ReadPtr = pageImage;

    	/*
    	 * Perform the Read operation.
    	 */
    	Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
       	if(Status != XST_SUCCESS) {
        		 xil_printf("XIsf Read Error\r\n");
        		 return 0x0;
        }
       	Status = IsfWaitForFlashNotBusy();
       			if(Status != XST_SUCCESS) {
       				 xil_printf("IsfWaitForFlashNotBusy Error\r\n");
       				return 0x0;
       			}
       	currentPage = requiredPage;

     }
     currentFlashByteAddr++;
     return ReadBuffer[requiredAddress + XISF_CMD_SEND_EXTRA_BYTES];

}

static int IsfWaitForFlashNotBusy()
{
	int Status;
	u8 StatusReg;
	u8 ReadBuffer[2];

	while(1) {

		/*
		 * Get the Status Register.
		 */
		Status = XIsf_GetStatus(&Isf, ReadBuffer);
		if(Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Check if the Serial Flash is ready to accept the next
		 * command. If so break.
		 */
		StatusReg = ReadBuffer[BYTE2];
		if(StatusReg & XISF_SR_IS_READY_MASK) {
			break;
		}
	}

	return XST_SUCCESS;
}

#ifdef __PPC__

#include <unistd.h>

/* Save some code and data space on PowerPC
   by defining a minimal exit */
void exit (int ret)
{
    _exit (ret);
}
#endif
