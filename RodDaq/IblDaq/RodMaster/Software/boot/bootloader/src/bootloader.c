/*
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A 
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR 
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION 
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE 
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO 
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO 
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE 
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY 
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * 
 *
 * This file is a generated sample test application.
 *
 * This application is intended to test and/or illustrate some 
 * functionality of your system.  The contents of this file may
 * vary depending on the IP in your system and may use existing
 * IP driver functions.  These drivers will be generated in your
 * SDK application project when you run the "Generate Libraries" menu item.
 *
 */

#include <stdio.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xintc.h"
#include "xil_exception.h"
#include "intc_header.h"
#include "xlltemac.h"
#include "xbasic_types.h"
#include "iic_header.h"
#include "xspi.h"
#include "spi_header.h"
#include "xtmrctr.h"

#include "rodMaster.hxx"

int simulation;

void prints(char *s) { if (!simulation) print(s); }


// !!!!!!!!!!!!! Don't use watchdog test unless you realy want to test the Wdog !!!!!!!!!!!!!!!
// all other functionality wight fail ...
//#define WATCHDOG_TEST
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

int main() 
{

   static XIntc intc;

   Xil_ICacheEnable();
   Xil_DCacheEnable();

   simulation = (*(volatile u32*)PPC_STAT_REG & (1 << PPC_STAT_SIMULATION_BIT))?1:0;
   
   //prints("---Entering main---\n\r");

#ifdef WATCHDOG_TEST
   {
     volatile u32* wdReg = (volatile u32*)PPC_WDOG_REG;
     volatile u32* wdGate1 = (volatile u32*)PPC_WDOG_GATE1_REG;
     volatile u32* wdGate2 = (volatile u32*)PPC_WDOG_GATE2_REG;
     prints("Testing watchdog\r\n");
     // test wd write with invalid gates
     *wdReg = 0; // should not activate wd. check with chipscope or simulation
     // set gates
     *wdGate1 = PPC_WDOG_GATE1_KEY;
     *wdGate2 = PPC_WDOG_GATE2_KEY;
     *wdReg = 0; // should activate wd now
     // if we don't loop from here PPC will be reset
   }
#endif   
   
   
   {
     // check git hash
     int i;
     char id[20];
     volatile u32 *gid = (volatile u32 *)PPC_DESIGN_REG;
     *gid = 0;	// reset
     for (i=0;i<20;i++){
       id[i] = (char)(*gid & 0xff);
     }
     prints("Git ID: ");
     for (i=0;i<20;i++){
       xil_printf("%x",id[i]>>4);
       xil_printf("%x",id[i]&0xf);
     }
     prints("\r\n");
   }

   {
      int status;
      
      prints("\r\n Running IntcSelfTestExample() for xps_intc_0...\r\n");
      
      status = IntcSelfTestExample(XPAR_XPS_INTC_0_DEVICE_ID);
      
      if (status == 0) {
         prints("IntcSelfTestExample PASSED\r\n");
      }
      else {
         prints("IntcSelfTestExample FAILED\r\n");
      }
   } 
	
   {
       int Status;

       Status = IntcInterruptSetup(&intc, XPAR_XPS_INTC_0_DEVICE_ID);
       if (Status == 0) {
          prints("Intc Interrupt Setup PASSED\r\n");
       } 
       else {
         prints("Intc Interrupt Setup FAILED\r\n");
      } 
   }
   
      
   

   {
      int status;
            
      
      prints("\r\n Running IicSelfTestExample() for xps_iic_0...\r\n");
      
      status = IicSelfTestExample(XPAR_XPS_IIC_0_DEVICE_ID);
      
      if (status == 0) {
         prints("IicSelfTestExample PASSED\r\n");
      }
      else {
         prints("IicSelfTestExample FAILED\r\n");
      }
   }
   

   {
      XStatus status;
      
      prints("\r\n Runnning SpiSelfTestExample() for xps_spi_0...\r\n");
      
      status = SpiSelfTestExample(XPAR_XPS_SPI_0_DEVICE_ID);
      
      if (status == 0) {
         prints("SpiSelfTestExample PASSED\r\n");
      }
      else {
         prints("SpiSelfTestExample FAILED\r\n");
      }
   }
   


   prints("---Exiting main---\n\r");

   Xil_DCacheDisable();
   Xil_ICacheDisable();

   return 0;
}

