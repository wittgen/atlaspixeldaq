/*
 * flashOperation.c
 *
 *  Created on: Jul 16, 2012
 *      Author: kugel
 */


#include "xparameters.h"
#include "xilisf.h"
#include "xspi.h"

extern int simulation;

void flashTest()
{
	// based on "low level" access
	unsigned char spiBuf[100 + XISF_CMD_SEND_EXTRA_BYTES], rdBuf[100];
	XSpi flash;
	XIsf flashApi;
	XIsf_ReadParam rdParms;
	XIsf_BufferReadParam rdBufParms;
	XIsf_WriteParam wrParms;
	XIsf_BufferWriteParam wrBufParms;
	u32 offset = 0;
	u32 fselect = 1;
	int rc;
	int i;

#if 0
	rc = SpiSelfTestExample(XPAR_XPS_SPI_0_DEVICE_ID);
	if (XST_SUCCESS != rc){
		if (!simulation) {
			xil_printf("Spi selftest failed with rc 0x%x\r\n",rc);
			return;
		}
	} else {
		if (!simulation) {
			xil_printf("Spi selftest OK\r\n");
		}
	}
#endif
	// initialize SPI
	rc = XSpi_Initialize(&flash,XPAR_XPS_SPI_0_DEVICE_ID);
	if (XST_FAILURE == rc){
		if (!simulation) {
			xil_printf("Spi init failed\r\n");
			return;
		}
	} else {
		if (!simulation) {
			xil_printf("Spi init returned 0x%x\r\n",rc);
		}
	}
	// Start SPI
	rc = XSpi_Start(&flash);
	if (XST_DEVICE_NOT_FOUND == rc){
		if (!simulation) {
			xil_printf("Spi start failed\r\n");
			return;
		}
	} else {
		if (!simulation) {
			xil_printf("Spi start returned 0x%x\r\n",rc);
		}
	}
	// disable interrupts to go for polled mode
	XSpi_IntrGlobalDisable(&flash);
	if (!simulation) {
		xil_printf("Polling mode. IRQ disabled\r\n");
	}

	// init flash api
	rc = XIsf_Initialize(&flashApi, &flash, fselect, spiBuf);
	//rc = XIsf_Initialize_ak(&flashApi, &flash, fselect, rdBuf);
	if (XST_DEVICE_NOT_FOUND == rc){
		if (!simulation) {
			xil_printf("Xisf start failed\r\n");
			return;
		}
	} else {
		if (!simulation) {
			xil_printf("Xisf start returned 0x%x\r\n",rc);
		}
	}
	// test buffer write
	rdBuf[0] = 0x1a;
	rdBuf[1] = 0xc6;
	rdBuf[2] = 0xa5;
	rdBuf[3] = 0x7f;
	for (i=0;i<8;i++){
		if (!simulation) {
			xil_printf("Xisf write buffer by[%d]: 0x%x\r\n",i,rdBuf[i]);
		}
	}
	print("Press key\r\n");getchar();
	wrBufParms.BufferNum = XISF_PAGE_BUFFER1;
	wrBufParms.ByteOffset = 0;		// Start address in the buffer
	wrBufParms.WritePtr = rdBuf; 	// Write buffer pointer where data needs to be stored
	wrBufParms.NumBytes = 4;		// Number of bytes to be written to the Serial  Flash
	//rc = XIsf_Write(&flashApi, XISF_WRITE, &wrParms);
	rc = XIsf_Write(&flashApi, XISF_BUFFER_WRITE, &wrBufParms);
	if (XST_FAILURE == rc){
		if (!simulation) {
			xil_printf("Write buffer failed\r\n");
		}
	} else {
		for (i=0;i<8;i++){
			if (!simulation) {
				xil_printf("Xisf write buffer by[%d]: 0x%x\r\n",i,rdBuf[i]);
			}
		}
	}

	print("Press key\r\n");getchar();
	// test buffer read
	rdBufParms.BufferNum = XISF_PAGE_BUFFER1;
	rdBufParms.ByteOffset = 0;		// Start address in the buffer
	rdBufParms.ReadPtr = rdBuf; 	// Read buffer pointer where data needs to be stored
	rdBufParms.NumBytes = 8;		// Number of bytes to read
	//rc = XIsf_Read(&flashApi, XISF_READ, &rdParms);
	rc = XIsf_Read(&flashApi, XISF_BUFFER_READ, &rdBufParms);
	if (XST_FAILURE == rc){
		if (!simulation) {
			xil_printf("Read buffer failed\r\n");
		}
	} else {
		for (i=0;i<8;i++){
			if (!simulation) {
				xil_printf("Xisf read buffer by[%d]: 0x%x\r\n",i,rdBuf[XISF_CMD_SEND_EXTRA_BYTES + i]); // offset on read
			}
		}
	}

	// test write
	print("Press key\r\n");getchar();
	rdBuf[0] = 0x1a;
	rdBuf[1] = 0xc6;
	rdBuf[2] = 0xa5;
	rdBuf[3] = 0x7f;
	for (i=0;i<8;i++){
		if (!simulation) {
			xil_printf("Xisf write by[%d]: 0x%x\r\n",i,rdBuf[i]);
		}
	}
	wrParms.Address = 0;		// Start address in the Serial Flash
	wrParms.WritePtr = rdBuf; 	// Write buffer pointer where data needs to be stored
	wrParms.NumBytes = 4;		// Number of bytes to be written to the Serial  Flash
	rc = XIsf_Write(&flashApi, XISF_WRITE, &wrParms);
	if (XST_FAILURE == rc){
		if (!simulation) {
			xil_printf("Write failed\r\n");
		}
	} else {
		for (i=0;i<8;i++){
			if (!simulation) {
				xil_printf("Xisf write by[%d]: 0x%x\r\n",i,rdBuf[i]);
			}
		}
	}

	// test read
	print("Press key\r\n");getchar();
	rdParms.Address = 0;		// Start address in the Serial Flash
	rdParms.ReadPtr = rdBuf; 	// Read buffer pointer where data needs to be stored
	rdParms.NumBytes = 8;		// Number of bytes to read
	rdParms.NumDummyBytes = 0;	// Valid only for dual, quad operations
	rc = XIsf_Read(&flashApi, XISF_READ, &rdParms);
	if (XST_FAILURE == rc){
		if (!simulation) {
			xil_printf("Read failed\r\n");
		}
	} else {
		for (i=0;i<8;i++){
			if (!simulation) {
				xil_printf("Xisf read by[%d]: 0x%x\r\n",i,rdBuf[XISF_CMD_SEND_EXTRA_BYTES + i]); // offset on read
			}
		}
	}

	// Stop SPI
	rc = XSpi_Stop(&flash);
	if (XST_SUCCESS == rc){
		if (!simulation) {
			xil_printf("Spi stop OK\r\n");
		}
	} else {
		if (!simulation) {
			xil_printf("Spi stop returned 0x%x\r\n",rc);
		}
	}
	// reset SPI
	XSpi_Reset(&flash);

}
