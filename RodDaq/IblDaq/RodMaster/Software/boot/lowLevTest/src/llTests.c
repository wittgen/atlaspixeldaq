/*
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 *
 *
 * This file is a generated sample test application.
 *
 * This application is intended to test and/or illustrate some
 * functionality of your system.  The contents of this file may
 * vary depending on the IP in your system and may use existing
 * IP driver functions.  These drivers will be generated in your
 * SDK application project when you run the "Generate Libraries" menu item.
 *
 */


#include <stdio.h>
#include "xparameters.h"
#include "xil_cache.h"
#include "xintc.h"
#include "xil_exception.h"
#include "intc_header.h"
#include "xlltemac.h"
#include "xbasic_types.h"
#include "iic_header.h"
#include "xspi.h"
#include "spi_header.h"
#include "xtmrctr.h"
#include <stdint.h>

#define PRM_SPI_DEVICE XPAR_SPI_1_DEVICE_ID

#include "rodMaster.hxx"


// define what should be tested
#define ENABLE_STD_SELFTESTS
#define ENABLE_PRM_SPI
#define ENABLE_WDOG
#define ENABLE_UARTS

int simulation;

int main()
{

   static XIntc intc;

   Xil_ICacheEnable();
   Xil_DCacheEnable();

#ifdef ENABLE_UARTS
    while (1){
        uint32_t data;
        data = *(uint32_t*)PPC_UART_S6A_STATUS_REG;
        data = *(uint32_t*)PPC_UART_S6A_DATA_REG;
        data = *(uint32_t*)PPC_UART_S6B_STATUS_REG;
        data = *(uint32_t*)PPC_UART_S6B_DATA_REG;
        data = *(uint32_t*)PPC_UART_MASTER_STATUS_REG;
        data = *(uint32_t*)PPC_UART_MASTER_DATA_REG;
    }
#endif

   print("---Entering main---\n\r");


#ifdef ENABLE_STD_SELFTESTS
   {
      int status;

      print("\r\n Running IntcSelfTestExample() for xps_intc_0...\r\n");

      status = IntcSelfTestExample(XPAR_XPS_INTC_0_DEVICE_ID);

      if (status == 0) {
         print("IntcSelfTestExample PASSED\r\n");
      }
      else {
         print("IntcSelfTestExample FAILED\r\n");
      }
   }

   {
       int Status;

       Status = IntcInterruptSetup(&intc, XPAR_XPS_INTC_0_DEVICE_ID);
       if (Status == 0) {
          print("Intc Interrupt Setup PASSED\r\n");
       }
       else {
         print("Intc Interrupt Setup FAILED\r\n");
      }
   }




   {
      int status;


      print("\r\n Running IicSelfTestExample() for xps_iic_0...\r\n");

      status = IicSelfTestExample(XPAR_XPS_IIC_0_DEVICE_ID);

      if (status == 0) {
         print("IicSelfTestExample PASSED\r\n");
      }
      else {
         print("IicSelfTestExample FAILED\r\n");
      }
   }


   {
      XStatus status;

      print("\r\n Runnning SpiSelfTestExample() for xps_spi_0...\r\n");

      status = SpiSelfTestExample(XPAR_XPS_SPI_0_DEVICE_ID);

      if (status == 0) {
         print("SpiSelfTestExample PASSED\r\n");
      }
      else {
         print("SpiSelfTestExample FAILED\r\n");
      }
   }

   {
      XStatus status;

      print("\r\n Runnning SpiSelfTestExample() for xps_spi_1...\r\n");

      status = SpiSelfTestExample(PRM_SPI_DEVICE);

      if (status == 0) {
         print("SpiSelfTestExample PASSED\r\n");
      }
      else {
         print("SpiSelfTestExample FAILED\r\n");
      }
   }

#endif

#ifdef ENABLE_PRM_SPI

   {
      XSpi Spi; /* The instance of the SPI device */
      XStatus Status;
      int scnt = 0;
      int i;

      print("\r\n Testing PRM communication via SPI...\r\n");

        XSpi_Config *ConfigPtr;	/* Pointer to Configuration data */
        XSpi *InstancePtr = &Spi;
        int Result;
        u32 Register;
        u32 ControlReg, StatusReg;
	u32 spiRxWord;

        ConfigPtr = XSpi_LookupConfig(PRM_SPI_DEVICE);
        if (ConfigPtr == NULL) {
            return XST_DEVICE_NOT_FOUND;
        }

        Status = XSpi_CfgInitialize(InstancePtr, ConfigPtr,
                      ConfigPtr->BaseAddress);
        if (Status != XST_SUCCESS) {
            return XST_FAILURE;
        }

        XSpi_Reset(InstancePtr);

	/*
	As described earlier, SCK must be stable before the assertion of slave select. Therefore, when manual slave select
	mode is utilized, the SPI master must be enabled first (SPICR bit(24) = 1) to assert SCK to the idle state prior to
	asserting slave select.
	Note that the master transfer inhibit (SPICR bit(23)) can be utilized to inhibit master transactions until the slave
	select is asserted manually and all data registers of FIFOs are initialized as desired. This can be utilized before the
	first transaction and after any transaction that is allowed to complete.
	When the above rules are followed, the timing is the same as presented for the automatic slave select assertion
	mode with the exception that assertion of slave select signal and the number of elements transferred is controlled by
	the user.
	*/
        ControlReg = XSpi_GetControlReg(InstancePtr);
        xil_printf("SPI control register initial value: 0x%x\r\n",ControlReg);
        XSpi_SetControlReg(InstancePtr, ControlReg | XSP_CR_MASTER_MODE_MASK | XSP_MANUAL_SSELECT_OPTION | XSP_CR_TRANS_INHIBIT_MASK);

        /*
         * We do not need interrupts for this loopback test.
         */
        XSpi_IntrGlobalDisable(InstancePtr);

        // reset ss
        XSpi_SetSlaveSelectReg(InstancePtr, ~0);

        // wait for tx ready
	do {
		StatusReg = XSpi_GetStatusReg(InstancePtr);
	} while ((StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);

	/*
	* Start the transfer by not inhibiting the transmitter and
	* enabling the device.
	*/
	ControlReg = XSpi_GetControlReg(InstancePtr) & (~XSP_CR_TRANS_INHIBIT_MASK);
	XSpi_SetControlReg(InstancePtr, ControlReg | XSP_CR_ENABLE_MASK);

	while (1){
	  // set ss
	  XSpi_SetSlaveSelectReg(InstancePtr, ~1);

      // write word
      XSpi_WriteReg(InstancePtr->BaseAddr, XSP_DTR_OFFSET, 0 );

      // wait for rx ready
      do {
          StatusReg = XSpi_GetStatusReg(InstancePtr);
      } while ((StatusReg & XSP_SR_RX_FULL_MASK) == 0);
      // } while ((StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);

      // read byte
      spiRxWord = XSpi_ReadReg(InstancePtr->BaseAddr, XSP_DRR_OFFSET);

      if (0x5 != (spiRxWord >> 28)) {
        xil_printf("\r\n SPI read error: 0x%x\r\n",spiRxWord);
      } else {
        print("\r\n PRM word is 0x (NB: hex switch bits are inverted) ");
        putnum(spiRxWord);
        print("\r\n");
      }

      // if git id present
      for (i = 0; i < 5; i++){
          // write word
	  // warning: the new spi mode at PRM will control the jtag mapping
	  // if bit 3 in the last byte is set, jtag will go to the XVC mapping
	  // and become unavailable for IMPACT/XMD
          // XSpi_WriteReg(InstancePtr->BaseAddr, XSP_DTR_OFFSET, 0x08 );
          XSpi_WriteReg(InstancePtr->BaseAddr, XSP_DTR_OFFSET, 0x00 );

          // wait for rx ready
          do {
              StatusReg = XSpi_GetStatusReg(InstancePtr);
          } while ((StatusReg & XSP_SR_RX_FULL_MASK) == 0);
          // } while ((StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);

          // read byte
          spiRxWord = XSpi_ReadReg(InstancePtr->BaseAddr, XSP_DRR_OFFSET);

            putnum(spiRxWord);
      }
      print("\r\n");

      // reset ss
      XSpi_SetSlaveSelectReg(InstancePtr, ~0);


	  if (scnt++ > 100) break;
	}
   }

#endif

#ifdef ENABLE_WDOG

   // watchdog
   {
     int wdCnt = 0;
      print("\r\n Testing watchdog\r\n");
      // clear masks initially
      print("\r\n Clearing enable masks\r\n");
      *(u32*)PPC_WDOG_ENABLE_MASK0 = 0;
      *(u32*)PPC_WDOG_ENABLE_MASK1 = 0;

      print("\r\n About to enable WD with invalids masks. shouldn't trigger\r\n");
      *(u32*)PPC_WDOG_REG = 0;
      print("\r\n WD written\r\n");
      print("\r\nWaiting 2 s\r\n");
      sleep(2);
      print("\r\n WD didn't trigger. good.\r\n");

      // set wd masks
      print("\r\n Now setting enable masks\r\n");
      *(u32*)PPC_WDOG_ENABLE_MASK0 = PPC_WDOG_ENABLE_VAL0;
      *(u32*)PPC_WDOG_ENABLE_MASK1 = PPC_WDOG_ENABLE_VAL1;

      print("\r\n WD not yet enabled. Waiting 2 s\r\n");
      sleep(2);

      print("\r\n About to enable WD. Testing short delays first\r\n");
      *(u32*)PPC_WDOG_REG = 0;
      print("\r\n Enabled\r\n");
      while (1) {
	*(u32*)PPC_WDOG_REG = 0;
	putnum(wdCnt ++);
	print("\r\n");
	if (wdCnt > 1000) break;
      }

      print("\r\n Long delay, should trigger reset\r\n");
      sleep(2);

   }

#endif


   print("---Exiting main---\n\r");

   Xil_DCacheDisable();
   Xil_ICacheDisable();

   return 0;
}

