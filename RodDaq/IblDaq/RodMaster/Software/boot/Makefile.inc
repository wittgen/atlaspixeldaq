IBLDAQ_BASEDIR = $(PIXELDAQ_ROOT)
RODMASTER_BASEDIR = ../../../Firmware

include $(RODMASTER_BASEDIR)/Makefile.inc
include ../../../xilconf.mk

# this is already somehow defined in system/Makefile
SYSTEM_XML = $(RODMASTER_BASEDIR)/sw/SDK_Export/hw/v5gmac125.xml

PROJECT = $(notdir $(CURDIR))

ifeq ($(BSP), )
$(error Please specify BSP)
endif

CPU = ppc440_0
BSP_DIR = ../../bsp/$(BSP)/$(CPU)

# include directories
INCS += ../../../Firmware/inc
INCS += $(BSP_DIR)/include

INCS += ../../../../inc
INCS += ../../../../../inc

CFLAGS += -Wall -O3 -c -fmessage-length=0
CFLAGS += -mcpu=440
CFLAGS += -MMD -MP

LDFLAGS += -Wl,-T -Wl,src/lscript.ld -L $(BSP_DIR)/lib
LDFLAGS += -mcpu=440
#LDFLAGS += -Wl,--start-group,-lxil,-lgcc,-lc,--end-group
LDFLAGS += -Wl,--start-group,-lxilisf,-lxil,-lgcc,-lc,--end-group

SOURCES = $(wildcard src/*.c)

OBJECTS = $(SOURCES:%.c=%.o)
DEPS = $(SOURCES:%.c=%.d)

.PHONY: all clean
all: $(BSP_DIR)
$(BSP_DIR): 
	@$(MAKE) -C $(@D)
all: $(PROJECT).elf $(PROJECT).elf.size $(PROJECT).elf.elfcheck

$(PROJECT).elf: $(OBJECTS)
	$(XIL_ENV) $(TIME) $(PPC-CC) -o $@ $(OBJECTS) $(LDFLAGS)

$(PROJECT).elf.size: $(PROJECT).elf
	@$(SIZE) $(PROJECT).elf  | tee "$(PROJECT).elf.size"

$(PROJECT).elf.elfcheck: $(PROJECT).elf $(SYSTEM_XML)
	@$(ELFCHECK) $(PROJECT).elf -hw $(SYSTEM_XML) -pe ppc440_0  | tee "$(PROJECT).elf.elfcheck"

%.o: %.c
	@echo $(addprefix -I, $(INCS))
	$(PPC-CC) $(CFLAGS) $(addprefix -I, $(INCS)) $< -o $@

clean:
	@rm -f *.time
	@cd src; rm -f *.o *.d *.time
	@rm -f $(PROJECT).elf*
	@$(MAKE) -C $(dir $(BSP_DIR)) clean

.SECONDARY: $(DEPS)
-include $(DEPS)
