/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009 Xilinx, Inc. All Rights Reserved.
//
// You may copy and modify these files for your own internal use solely with
// Xilinx programmable logic devices and  Xilinx EDK system or create IP
// modules solely for Xilinx programmable logic devices and Xilinx EDK system.
// No rights are granted to distribute any files unless they are distributed in
// Xilinx programmable logic devices.
//
/////////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "blconfig.h"
#include "portab.h"
#include "errors.h"
#include "xparameters.h"	/* EDK generated parameters */
#include "xilisf.h"		/* Serial Flash Library header file */
#include <sleep.h>
#include "rodMaster.hxx"
#include "iblSlavemds.h"
#include "ppcSpecific.h"
//#include "iblRegisters.h"

//these need to be definid in another way
#define CE0_BASE 0x40000000
#define FMT0_BASE             (CE0_BASE)
#define FMT1_BASE             (CE0_BASE + (1 << 18))
#define RAM_OFFS  (1<<14)   //(MB_SLV_RAM_WORDS * 4)
#define SLV0_RAM  (FMT0_BASE + RAM_OFFS)
#define SLV1_RAM  (FMT1_BASE + RAM_OFFS)

volatile unsigned long *epcBase;
volatile unsigned long *epcLocal;
volatile unsigned long *epcSlvARegs;
volatile unsigned long *epcSlvARam;
volatile unsigned long *epcSlvBRegs;
volatile unsigned long *epcSlvBRam;
volatile unsigned long *epcBoc;

volatile unsigned long *epcCtl;

/* Defines () */
#define CR       13

/* Comment the following line, if you want a smaller and faster bootloader which will be silent */
#define VERBOSE

/* Declarations */

//static uint8_t load_exec ();
void init_epc();
uint8_t bootMB();
unsigned long littleendian(u32);
extern void init_stdout();
static u8 getByteFromFlash();

extern int srec_line;

#ifdef __cplusplus
extern "C" {
#endif

	extern void outbyte(char c);

#ifdef __cplusplus
}
#endif

/* Data structures */
//static srec_info_t srinfo;
//static uint8_t sr_buf[SREC_MAX_BYTES];
//static uint8_t sr_data_buf[SREC_DATA_MAX_BYTES];

//static uint8_t *flbuf;


/************************** Constant Definitions *****************************/

/*
 * The following constants map to the XPAR parameters created in the
 * xparameters.h file. They are defined here such that a user can easily
 * change all the needed parameters in one place.
 */
#define SPI_DEVICE_ID		XPAR_SPI_0_DEVICE_ID

/*
 * The following constant defines the slave select signal that is used to
 * select the Serial Flash device on the SPI bus, this signal is typically
 * connected to the chip select of the device.
 */
#define ISF_SPI_SELECT		0x01

// total flash size in bytes
#define FLASH_SIZE 8650752
#define HALF_FLASH FLASH_SIZE/2
// start address where SREC file is stored
#define FLASH_START_ADDRESS 0x0

// size of SREC file
#define SREC_FILE_SIZE 0

// RT global
//static u8 pageImage[ISF_PAGE_SIZE]; //comment: use ReadBuffer instead
static int currentPage = -1;
static u32 currentFlashByteAddr;

static XIsf Isf;
static XSpi Spi;
u8 IsfWriteBuffer[1056/*ISF_PAGE_SIZE*/ + XISF_CMD_SEND_EXTRA_BYTES];

static int Status;


/*
 * Page size of the Serial Flash.
 */
int ISF_PAGE_SIZE;
int HALF_FLASH_PAGE;

/*
 * Buffers used during read and write transactions.
 */
u32* buffer_cast32;
u8 ReadBuffer[1056/*ISF_PAGE_SIZE*/ + XISF_CMD_SEND_EXTRA_BYTES] ; /* Read Buffer */
u8 WriteBuffer[1056/*ISF_PAGE_SIZE*/]; 				   /* Write buffer */

/************************** Function Prototypes ******************************/

static int IsfWaitForFlashNotBusy();

// end RT global


/* We don't use interrupts/exceptions.
   Dummy definitions to reduce code size on MicroBlaze */
#ifdef __MICROBLAZE__
void _interrupt_handler () {}
void _exception_handler () {}
void _hw_exception_handler () {}
#endif
volatile u32* timer = 0x83c20000;
union sizeofbin_t{
	int readlength;
	u8 bytes[4];
}sizeofbin;

int main()
{
	uint8_t ret=0;

	init_stdout();
	init_epc();

	usleep(1000);
	Status = XSpi_Initialize(&Spi, SPI_DEVICE_ID);
	if(Status != XST_SUCCESS) {
		xil_printf("XSpi Initialize Error\r\n");
		return XST_FAILURE;
	}

	/*
	 * Start the SPI driver so that the device is enabled.
	 */
	Status = XSpi_Start(&Spi);
	if(Status != XST_SUCCESS) {
		xil_printf("XSpi Start Error\r\n");
		return XST_FAILURE;
	}

	/*
	 * Disable Global interrupt to use the Spi driver in polled mode
	 * operation.
	 */
	XSpi_IntrGlobalDisable(&Spi);

	/*
	 * Initialize the In-system and Serial Flash Library.
	 */

	Status = XIsf_Initialize(&Isf, &Spi, ISF_SPI_SELECT, IsfWriteBuffer);
	if(Status != XST_SUCCESS) {
		xil_printf("XIsf Initialize Error\r\n");
		return XST_FAILURE;
	}

	u8 deviceInfo[5];
	Status = XIsf_GetDeviceInfo(&Isf, deviceInfo);
	if(Status != XST_SUCCESS || deviceInfo[1] != 0x1F || deviceInfo[2] != 0x28 || deviceInfo[3] != 0x00) {
		xil_printf("XIsf Initialize Error\r\n");
		return XST_FAILURE;
	}
	if(deviceInfo[4] == 0x01) {
		ISF_PAGE_SIZE = 264;
		HALF_FLASH_PAGE = (HALF_FLASH/ISF_PAGE_SIZE);
		currentFlashByteAddr = 32760 *ISF_PAGE_SIZE ;
	}
	else if(deviceInfo[4] == 0x00) {
		ISF_PAGE_SIZE = 1056;
		HALF_FLASH_PAGE = (HALF_FLASH/ISF_PAGE_SIZE);
		currentFlashByteAddr = 8190 *ISF_PAGE_SIZE ;
	}
	else {
		xil_printf("Error in reading Flash Pagew Size\r\n");
		return XST_FAILURE;
	}	

	int i;
	for(i=0;i<4;++i)
	{

		sizeofbin.bytes[3-i]=getByteFromFlash();
	}

	//now we know the size!!! sizeofbin.readlength

	currentFlashByteAddr = HALF_FLASH;

	*epcCtl = 0x80000000; //UART on MASTER
	xil_printf("epcCtl %8X, \r\n",*epcCtl);
	*epcCtl |= (1 << PPC_CTL_HPIENABLE_BIT);

	//usleep(1000);
	//*epcCtl |= (1 << PPC_CTL_UARTA_BIT);
	//*epcCtl &= ~(1 << PPC_CTL_UARTA_BIT);
	//*epcCtl |= (1 << PPC_CTL_UARTB_BIT);

	xil_printf("epcCtl %8X, \r\n",*epcCtl);


	xil_printf("RAM_OFFS 0x%08x\r\n", RAM_OFFS);
	xil_printf("CE0_BASE 0x%08x\r\n", CE0_BASE);
	xil_printf("SLV0_RAM 0x%08x\r\n", SLV0_RAM);
	xil_printf("SLV1_RAM 0x%08x\r\n", SLV1_RAM);
	xil_printf("SlvA 0x%08x\r\n", &epcSlvARam[0]);
	xil_printf("SlvB 0x%08x\r\n", &epcSlvBRam[0]);

	xil_printf("Switching off verbosity\r\n");
	epcSlvARam[3]=0x0;
	epcSlvARam[0]=SLV_CMD_VERBOSE;
	epcSlvBRam[3]=0x0;
	epcSlvBRam[0]=SLV_CMD_VERBOSE;
	usleep(100000);

	while (epcSlvARam[0]!=SLV_CMD_IDLE || epcSlvBRam[0]!=SLV_CMD_IDLE)
	{
		xil_printf("slave A wrote: 0x%X\r\n", epcSlvARam[0]);
		xil_printf("slave B wrote: 0x%X\r\n", epcSlvBRam[0]);
		//		return 1;
	}

	xil_printf("Setting slave ID\r\n");
	epcSlvARam[3]=0x0;
	epcSlvARam[0]=SLV_CMD_ID_SET;
	epcSlvBRam[3]=0x1;
	epcSlvBRam[0]=SLV_CMD_ID_SET;

	usleep(100000);
	while(epcSlvARam[0]!=SLV_CMD_IDLE || epcSlvBRam[0]!=SLV_CMD_IDLE)
	{
		xil_printf("slave A wrote: 0x%X\r\n", epcSlvARam[0]);
		xil_printf("slave B wrote: 0x%X\r\n", epcSlvBRam[0]);
		//return 1;
	}

	ret=bootMB();

	return ret;
}


uint8_t bootMB()
{
	uint8_t ret=0;
	u32 Address;

	XIsf_ReadParam ReadParam;

	xil_printf("sizeofbin= %d \r\n",sizeofbin.readlength);

	u32 ipage;

	u32 pippo;
	for  (ipage=0;ipage<=sizeofbin.readlength/ISF_PAGE_SIZE;++ipage)
	{

		if(ISF_PAGE_SIZE == 1056){
	  		Address = (((ipage + HALF_FLASH_PAGE) & 0x1FFF) << 11) ;
		}
	  	else if(ISF_PAGE_SIZE == 264){
	  		Address = (((ipage + HALF_FLASH_PAGE) & 0x7FFF) << 9);
	  	}
	  	else {
			xil_printf("Flash Page Size undefined\r\n");
			return 1;
	  	}
		ReadParam.Address = Address;
		ReadParam.NumBytes = ISF_PAGE_SIZE;
		ReadParam.ReadPtr = ReadBuffer;
		buffer_cast32=ReadParam.ReadPtr;
		/*
		 * Perform the Read operation.
		 */

		Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
		if(Status != XST_SUCCESS)
		{
			xil_printf("XIsf Read Error\r\n");
			return 0x0;
		}
		//wait for flash
		Status = IsfWaitForFlashNotBusy();
		if(Status != XST_SUCCESS)
		{
			xil_printf("IsfWaitForFlashNotBusy Error\r\n");
			return 0x0;
		}

		{
			int l;
			for (l=0; l<=ISF_PAGE_SIZE/4 ; ++l)
			{
				epcSlvARam[l+3]=littleendian(buffer_cast32[l+1]);
				epcSlvBRam[l+3]=littleendian(buffer_cast32[l+1]);
			}
			epcSlvARam[2]=ISF_PAGE_SIZE/4;//count
			epcSlvARam[1]=ipage*ISF_PAGE_SIZE;//addr
			epcSlvARam[0]=SLV_CMD_WRITE;//cmd
			epcSlvBRam[2]=ISF_PAGE_SIZE/4;//count
			epcSlvBRam[1]=ipage*ISF_PAGE_SIZE;//addr
			epcSlvBRam[0]=SLV_CMD_WRITE;//cmd
		}
		while (epcSlvARam[0]!=SLV_CMD_IDLE  || epcSlvBRam[0]!=SLV_CMD_IDLE)
		{
#ifdef LL_DEBUG
			xil_printf("wait for IDLE at page %d\r\n", ipage);
			xil_printf("Command SLV A: %X \r\n",epcSlvARam[0]);
			xil_printf("Command SLV B: %X \r\n",epcSlvBRam[0]);
#endif
			usleep(1000);//wait MB to be ready... need a better solution
		}


	}//end for ipage
	*epcCtl |= (1 << PPC_CTL_UARTA_BIT);

	xil_printf("Starting slave A\n");
	epcSlvARam[1]=0x0;
	epcSlvARam[0]=SLV_CMD_START;

	xil_printf("Starting slave B\n");
	epcSlvBRam[1]=0x0;
	epcSlvBRam[0]=SLV_CMD_START;

	usleep(2000000);

	epcSlvARam[3]=0x0;
	epcSlvARam[0]=SLV_CMD_ID_SET;
	epcSlvBRam[3]=0x1;
	epcSlvBRam[0]=SLV_CMD_ID_SET;

	return ret;
}

unsigned long littleendian(u32 number)
{
	u8* pippo=&number;
	return (u32)(((pippo[3]<<24)&0xFF000000)+((pippo[2]<<16)&0x00FF0000)+((pippo[1]<<8)&0x0000FF00)+(pippo[0]&0x000000FF));
}

void init_epc()
{

#ifdef PPCEMU
	epcBase = malloc(1 << EPC_ADDRESS_BITS); // EPC_WORD_SIZE * sizeof(long));
	xil_printf("EPC emulation area allocated at 0x%x, size %d bytes\n",epcBase, EPC_WORD_SIZE * sizeof(long));
#else
	epcBase    = (volatile unsigned long *)CE0_BASE; // epcBase + PPC_REGS_BASE ;
#endif
	epcLocal    = (volatile unsigned long *) COMMON_REG_BASE; // epcBase + PPC_REGS_BASE ;

	epcSlvARegs = (volatile unsigned long *)SLV_A_REG_BASE;
	epcSlvARam  = (volatile unsigned long *)SLV0_RAM;
	epcSlvBRegs = (volatile unsigned long *)SLV_B_REG_BASE;
	epcSlvBRam  = (volatile unsigned long *)SLV1_RAM;
	epcBoc      = (volatile unsigned long *)BOC_REG_BASE;

	epcCtl      = (volatile unsigned long *)PPC_CTL_REG;
}




static u8 getByteFromFlash()
{
	u32 requiredPage;
	u32 requiredAddress;
	u32 Address;
	XIsf_ReadParam ReadParam;

	requiredPage = (int) (currentFlashByteAddr / ISF_PAGE_SIZE);
	requiredAddress = (u32) (currentFlashByteAddr%ISF_PAGE_SIZE);

	if(requiredPage != currentPage){
		if(ISF_PAGE_SIZE == 1056){
			Address = ((((u32)requiredPage) & 0x1FFF) << 11) & (~0x7FF);    
		}
		else if(ISF_PAGE_SIZE == 264){
			Address = ((((u32)requiredPage) & 0x7FFF) << 9) & (~0x1FF);       
		}
		else {
			xil_printf("Flash Page Size undefined\r\n");
			return 1;
		}	

		ReadParam.Address = Address;
		ReadParam.NumBytes = ISF_PAGE_SIZE;
		ReadParam.ReadPtr = ReadBuffer;

		/*
		 * Perform the Read operation.
		 */
		Status = XIsf_Read(&Isf, XISF_READ, (void*) &ReadParam);
		if(Status != XST_SUCCESS) {
			xil_printf("XIsf Read Error\r\n");
			return 0x0;
		}
		Status = IsfWaitForFlashNotBusy();
		if(Status != XST_SUCCESS) {
			xil_printf("IsfWaitForFlashNotBusy Error\r\n");
			return 0x0;
		}
		currentPage = requiredPage;

	}
	currentFlashByteAddr++;
	return ReadBuffer[requiredAddress + XISF_CMD_SEND_EXTRA_BYTES];

}

static int IsfWaitForFlashNotBusy()
{
	int Status;
	u8 StatusReg;
	u8 ReadBuffer[2];

	while(1) {

		/*
		 * Get the Status Register.
		 */
		Status = XIsf_GetStatus(&Isf, ReadBuffer);
		if(Status != XST_SUCCESS) {
			return XST_FAILURE;
		}

		/*
		 * Check if the Serial Flash is ready to accept the next
		 * command. If so break.
		 */
		StatusReg = ReadBuffer[BYTE2];
		if(StatusReg & XISF_SR_IS_READY_MASK) {
			break;
		}
	}

	return XST_SUCCESS;
}

#ifdef __PPC__

#include <unistd.h>

/* Save some code and data space on PowerPC
   by defining a minimal exit */
void exit (int ret)
{
	_exit (ret);
}
#endif
