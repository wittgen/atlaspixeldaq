/*
 * Authors: 
 * Daniela Boerner
 *
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "QuickStatus.h"

class QSTalkTaskResult: public CommandCRTP<QSTalkTaskResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host
	uint32_t quickStatusMode;
	 QuickStatus::monitoring_infos m_qsMonInfos;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
	
};

class QSTalkTask: public CommandCRTP<QSTalkTask > { // declares type of command
public:
	typedef QSTalkTaskResult ResultType;
	ResultType result;

	typedef QuickStatus::MON_MODES MON_MODES;
 
	void dump();
	QSTalkTask();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~QSTalkTask() {}

	// the only parameter of this command
 	bool	dumpOnPPC;
	uint32_t quickStatusMode;
	

	int fc_max;
	int bu_max;
	int to_max;
	int ht_max;
	int rst_max;
	int cfg_max;
	uint32_t module_mask;
	MON_MODES action;

	enum QSTTMode {
	  QSTT_INFO, QSTT_GET_STATUS, QSTT_SET_MODE, QSTT_SET_FC, QSTT_SET_BU, QSTT_SET_TO, QSTT_SET_HT, QSTT_SET_RST, QSTT_SET_TO_CFG, QSTT_SET_HT_CFG, QSTT_SET_BU_CFG, QSTT_USERACTION, QSTT_RESETUSERACTION, QSTT_SET_VERBOSITY, QSTT_SET_SPY_MODE, QSTT_SET_SLEEP_DELAY, QSTT_SET_WRITE_REG_AT_ECR, QSTT_SET_QS_SLEEP, QSTT_RESET
	}; // use QSTT_ in front in order to avoid overlapping enums
	QSTTMode qsttmode;
};

