#ifndef QSNETWORK_H
#define QSNETWORK_H

#include "CppCompatibility.h"


#undef LWIP_POSIX_SOCKETS_IO_NAMES
#define LWIP_POSIX_SOCKETS_IO_NAMES 0

#include <arpa/inet.h>
#include <sys/socket.h>

#include <pthread.h>

//This I think can live in the .cxx
#include "QuickStatus.h"
#include "QSTalkTask.h"

//I think this can be both UDP or TCP/IP
#define STAT_NET_BUFLEN 255


union address
{
  struct sockaddr* sa2;
  struct sockaddr_in* sa2_in;
};


class QSNetwork { 

 public:
  
  bool initializeServer();
  static QSNetwork* getInstance();
  void run();
  uint32_t get32(uint8_t* buffer);
  uint16_t get16(uint8_t* buffer);
  uint8_t  get8 (uint8_t* buffer);
  void SetupQSThread(const char* ipAddress, uint16_t port); 
  void clear(){m_len = 0;}
   
 private:
  
  uint8_t buffer[STAT_NET_BUFLEN];
  uint32_t m_len;
  int socketId,n;
  socklen_t clientlen;
  QuickStatus* qs;
  
  
#ifdef __XMK__
  struct sockaddr_in server;
  struct sockaddr_in client;
#endif

  //enum QSTTMode {
  //QSTT_INFO, QSTT_GET_STATUS, QSTT_SET_MODE, QSTT_SET_FC, QSTT_SET_BU, QSTT_SET_TO, QSTT_SET_RST, QSTT_SET_CFG, QSTT_SET_RESAMPLING_THRESHOLD, QSTT_SET_RESAMPLING_MAX, QSTT_USERACTION, QSTT_RESETUSERACTION, QSTT_SET_VERBOSITY, QSTT_SET_SPY_MODE, QSTT_SET_SLEEP_DELAY
  //}; // use QSTT_ in front in order to avoid overlapping enums
  //QSTTMode qsttmode;
  
  
};

#endif
