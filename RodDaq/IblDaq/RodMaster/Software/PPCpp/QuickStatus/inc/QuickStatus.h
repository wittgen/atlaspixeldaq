/* 
 * File:   QuickStatus.h
 * 
 * Represents the Quick Status class.
 */

#ifndef QUICK_STATUS_H
#define	QUICK_STATUS_H

#include "pthread.h"
#include <arpa/inet.h>
#include <sys/socket.h>
#include <mutex>
#include <condition_variable>

#include <list>
#include "IblBoc.h"

class RecoAtECR;

class QuickStatus {
public:
#ifdef __XMK__
    static QuickStatus* getInstance();
#endif
    enum MON_MODES { MON_IDLE, MON_RESAMPLE_PHASE, MON_RECONFIGURE, MON_DISABLE, MON_ENABLE, MON_EXPERT_RECONFIGURE_ON, MON_EXPERT_RECONFIGURE_OFF, MON_DISABLE_BOC, MON_ENABLE_BOC };
    static std::string ModeToString(int);

    void init();

    inline int getQSMode() { return m_quickStatusMode; }
    inline void setQSMode(int newMode) {
      m_quickStatusMode = newMode;
      cv.notify_one();
    }

    inline int getQSSleep() { return m_quickStatusSleep; }
    inline void setQSSleep(int newSleep) { m_quickStatusSleep = newSleep; }

    inline int getFCMax() { return m_fc_max; }
    inline void setFCMax(int new_fc) { m_fc_max = new_fc; }

    inline int getBUMax() { return m_bu_max; }
    inline void setBUMax(int new_bu) { m_bu_max = new_bu; }

    inline int getToMax() { return m_to_max; }
    inline int getHtMax() { return m_ht_max; }

    inline void setToMax(int new_to) { m_to_max = new_to; }
    inline void setHtMax(int new_ht) { m_ht_max = new_ht; }

    inline int getRstMax() { return m_rst_max;}
    inline void setRstMax(int new_rst) { m_rst_max = new_rst; }

    inline int getBUCfgMax() { return m_bu_cfg_max; }
    inline void setBUCfgMax(int new_cfg) { m_bu_cfg_max = new_cfg; }

    inline int getTOCfgMax() { return m_to_cfg_max; }
    void setTOCfgMax(int new_cfg) { m_to_cfg_max = new_cfg; }

    inline void addUserAction(MON_MODES action) {
      userActions.push_back(action);
    }

    inline void resetUserAction() {
      userActions.clear();
    }

    inline void removeUserAction() {
      userActions.pop_front();
    }

    inline std::list<MON_MODES> getUserActions() {
      return userActions;
    }

    inline int getNUserActions() {
     return nUserActions;
    } 

    inline int increaseNUserActions() {
      nUserActions++;
      return nUserActions;
    }

    inline void resetMask() { m_mask = 0; }
    inline void setMask(uint32_t newMask) { m_mask = newMask; }
    inline uint32_t getMask() { return m_mask; }

    inline void setSpyMode(bool flag) { m_spy_mode = flag; }
    inline bool getSpyMode() { return m_spy_mode; }

    inline void setWriteRegAtECR(bool flag) { m_writeRegAtECR = flag; }
    inline bool getWriteRegAtECR() { return m_writeRegAtECR; }

    inline void setSleepDelay(uint32_t delay) { m_sleepDelay = delay; }

    inline uint32_t getSleepDelay() { return m_sleepDelay; }
private:
    QuickStatus();
    /**
     * Runs the server.
     */

    void run();

public:

    static pthread_mutex_t m_rwMutex;

    struct monitoring_infos {
      uint8_t nCFG_rodBusy[32];
      uint8_t nCFG_timeout[32];
      uint8_t nCFG_ht[32];
      /* ROD master busy */
      uint16_t masterBusy;
      uint32_t masterBusyCounters[8];
      /* ROD counters */
      uint32_t slaveBusyCounters[64];
      uint32_t linkTrigCounters[32];
      uint32_t linkOccCounters[32];
      uint16_t totDesyncCounters[32];
      uint16_t timeoutCounters[32];
      uint32_t htCounters[32];
      /* BOC counters */
      uint32_t decodingErrorBOC[32];
      uint32_t frameErrorBOC[32];
      uint32_t frameCountBOC[32];
      uint16_t meanOccBOC[32];
      IblBoc::SLinkInfo SLinkInfo;
      /* Bit masks */
      uint32_t fmtLinkEn;
      uint32_t QSDisabled;
      uint32_t waitingForECR;
    };
    void resetQSMonInfo(monitoring_infos &infos, bool resetQSActionCounters=false, bool resetQSBookkeeping=false, bool resetBOCCounters=false);
    void resetQSCounters(bool resetQSActionCounters){
      
      resetQSMonInfo(m_infos, resetQSActionCounters); 
      if(resetQSActionCounters) m_counter_coarse=0; 
      m_counter_fine=0;
      
    }    

    void getBOCCounters();
    void getRODMasterBusy();

    void dumpMasks();
#ifdef __XMK__
    const monitoring_infos getMonitoringInfos() {
      const monitoring_infos monInfos = getInstance()->m_infos;
      return monInfos;
    }
    
    void setVerbosityLevel(int level) {
      m_verbosityLevel = level;
    }
    
    int getVerbosityLevel() {
      int vLevel = m_verbosityLevel;
      return vLevel;
    }
    
    void setNetwork(char* ipAddress, uint16_t port) {
      m_ipAddress = ipAddress;
      m_port = port;
      m_setup = true;
    }
    const char* getMonIp() {return m_ipAddress.c_str();}
    
    uint16_t getMonPort() {return m_port;}
    
#endif

private:

    std::mutex cv_m;			// wake/sleep the thread
    std::condition_variable cv;

    monitoring_infos m_infos;
#ifdef __XMK__
    MON_MODES m_monitoring_mode;
    uint32_t m_monitoring_mask;
    uint32_t m_timeoutError;
    uint32_t m_htError;
    uint32_t m_rodBusyError;
#endif
    void monitoring();
    int coarseCounteReaction();
    void reconfigModule(uint32_t reconfigMask, uint32_t &QSDisabled, uint32_t &waitingForECR);
    void expertReconfigModule(uint32_t reconfigMask, bool preAmpsOn, uint32_t &QSDisabled, uint32_t &waitingForECR);

    // change these variables only by using the corresponding set-functions
    // otherwise the locking is not handled correctly
    int m_quickStatusMode;
    int m_quickStatusSleep;
#ifdef __XMK__
    int m_verbosityLevel;
#endif
    std::list<MON_MODES> userActions;
    int nUserActions;   
    uint32_t m_mask;

    //Networking..
    std::string m_ipAddress;
#ifdef __XMK__
    bool m_setup;
    uint16_t m_port;
#endif
    unsigned int m_fc_max;
    unsigned int m_bu_max;
    unsigned int m_to_max;
    unsigned int m_ht_max;
    unsigned int m_rst_max;
    unsigned int m_bu_cfg_max;
    unsigned int m_to_cfg_max;
#ifdef __XMK__
    unsigned int m_ht_cfg_max;

    /* Counters used for deciding QS automatic actions. Will not be reset by high-level code */
    /* Use separate counters in monitoring_info to propagate information to high-level code */

    uint32_t m_rodbusyCounter[32];
    uint32_t m_timeoutCounter[32];
    uint32_t m_htCounter[32];
    uint32_t m_nTriggers[32];
    uint32_t m_nClocks;
#endif

    uint32_t m_counter_coarse;
    uint32_t m_counter_fine;
    bool m_spy_mode;
#ifdef __XMK__
    bool m_hasReconfigured;
#endif
    bool m_writeRegAtECR;

    uint32_t m_sleepDelay;

    //Reco at ECR
};

#endif	/* QUICK_STATUS_H */

