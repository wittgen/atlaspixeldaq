#include "QSTalkTask.h"
#include "SLinkInfoSerializer.h"
#include "CountOccupancy.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#include "primXface_common.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void QSTalkTask::dump() {

}

QSTalkTask::QSTalkTask(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
  quickStatusMode = 0;
  fc_max = 0;
  bu_max = 0;
  to_max = 0;
  rst_max = 0;
  cfg_max = 0;
  module_mask = 0;
  action = QuickStatus::MON_IDLE;
  qsttmode = QSTT_INFO;
}

// inside execute() the actual meat of the command happens on the ppc
void QSTalkTask::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
 pthread_mutex_lock(&QuickStatus::m_rwMutex);
 //xil_printf("QSTalk locking QS thread\n");
 
  QuickStatus* qs = QuickStatus::getInstance();	

  
  switch (qsttmode) {
    case QSTT_SET_MODE:
      qs->resetUserAction();
      qs->setQSMode(quickStatusMode);
      std::cout << "QSTT_SET_MODE: " << QuickStatus::ModeToString(qs->getQSMode()) << std::endl;
      break;
    case QSTT_USERACTION:
      qs->addUserAction(action);
      qs->setMask(module_mask);
      break;
    case QSTT_RESETUSERACTION:
      qs->resetUserAction();
      break;
    case QSTT_SET_FC:
      qs->setFCMax(fc_max);
      break;
    case QSTT_SET_BU:
      qs->setBUMax(bu_max);
      break;
    case QSTT_SET_TO:
      qs->setToMax(to_max);
      break;
    case QSTT_SET_HT:
      qs->setHtMax(ht_max);
      break;
    case QSTT_SET_RST:
      qs->setRstMax(rst_max);
      break;
    case QSTT_SET_BU_CFG:
      qs->setBUCfgMax(cfg_max);
      break;
    case QSTT_SET_TO_CFG:
      qs->setTOCfgMax(cfg_max);
      break;
    case QSTT_GET_STATUS:
      xil_printf("########## QS dump info ##########\n");
      xil_printf("Quick Status information (read out by the QSTalkTask)\n");
      xil_printf("Quick Status mode: %s\n", QuickStatus::ModeToString(qs->getQSMode()).c_str());
      xil_printf("QS sleep (in ms) btw fine cnt: %d\n", qs->getQSSleep());
      xil_printf("Max Value for fc: %d\n", qs->getFCMax());
      xil_printf("Threshold for busy error: %d\n", qs->getBUMax());
      xil_printf("Threshold for timeout error: %d\n", qs->getToMax());
      xil_printf("Max Value for rst: %d\n", qs->getRstMax());
      xil_printf("Max Value for TO cfg: %d\n", qs->getTOCfgMax());
      xil_printf("Max Value for BU cfg: %d\n", qs->getBUCfgMax());
      xil_printf("Sleep delay (FE-I4): %d\n", qs->getSleepDelay());
      xil_printf("Verbosity level: %d\n", qs->getVerbosityLevel());
      qs->dumpMasks();
      xil_printf("##########  End of dump ##########\n");
      break;
    case QSTT_INFO: // Do nothing, just retrieve the monitoring info (below)
      break;
    case QSTT_SET_VERBOSITY:
      qs->setVerbosityLevel(fc_max); // Re-using variable
      CountOccupancy::setVerbosityLevel(fc_max);
      xil_printf("Setting QS verbosity level to: %d\n", qs->getVerbosityLevel());
      break;
    case QSTT_SET_SPY_MODE:
      qs->setSpyMode(fc_max>0);
      xil_printf("Setting QS spy mode to: %d\n", (qs->getSpyMode()?"ON":"OFF"));
      break;
    case QSTT_SET_SLEEP_DELAY:
      qs->setSleepDelay(fc_max);
      xil_printf("Setting sleep delay (FE-I4) to: %d\n", qs->getSleepDelay());
      break;
    case QSTT_SET_WRITE_REG_AT_ECR:
      qs->setWriteRegAtECR(fc_max);
      xil_printf("Setting write register at ECR to: %d\n", (qs->getWriteRegAtECR()?"ON":"OFF"));
      break;
    case QSTT_SET_QS_SLEEP:
      qs->setQSSleep(fc_max);
      xil_printf("Setting QS sleep to (ms): %d\n", qs->getQSSleep());
      break;
    case QSTT_RESET:
      qs->init();
      xil_printf("Resetting QS variables\n");
      break;
    default:
      xil_printf("no correct QuickStatusTalkTask mode was chosen, nothing will be changed!\n");
      break;
  }
  
  result.quickStatusMode = qs->getQSMode();
  result.m_qsMonInfos = qs->getMonitoringInfos();
  qs->resetQSCounters(fc_max);	// After each talk task, reset the counters
  if(dumpOnPPC) dump();

 //xil_printf("QSTalk unlocking QS thread\n");
 pthread_mutex_unlock(&QuickStatus::m_rwMutex);

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void QSTalkTask::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
	Serializer<uint32_t>::serialize(out, offset, quickStatusMode);
	Serializer<int>::serialize(out, offset, fc_max);
	Serializer<int>::serialize(out, offset, bu_max);
	Serializer<int>::serialize(out, offset, to_max);
	Serializer<int>::serialize(out, offset, rst_max);
	Serializer<int>::serialize(out, offset, cfg_max);
	Serializer<uint32_t>::serialize(out, offset, module_mask);
	Serializer<MON_MODES>::serialize(out, offset, action);
	Serializer<QSTTMode>::serialize(out, offset, qsttmode);
}

// Deseralize the data members of the command
// In this case, only one boolean
void QSTalkTask::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
	quickStatusMode = Serializer<uint32_t>::deserialize(in, offset);
	fc_max = Serializer<int>::deserialize(in, offset);
	bu_max = Serializer<int>::deserialize(in, offset);
	to_max = Serializer<int>::deserialize(in, offset);
	rst_max = Serializer<int>::deserialize(in, offset);
	cfg_max = Serializer<int>::deserialize(in, offset);
	module_mask = Serializer<uint32_t>::deserialize(in, offset);
	action = Serializer<MON_MODES>::deserialize(in, offset);
	qsttmode = Serializer<QSTTMode>::deserialize(in, offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t QSTalkTask::serializedSize() const
{
	return (sizeof(bool) + 2*sizeof(uint32_t) + 5*sizeof(int) + sizeof(MON_MODES) + sizeof(QSTTMode));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void QSTalkTaskResult::serialize(uint8_t *out) const {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  uint32_t offset = 0;

  // QS mode
  Serializer<uint32_t>::serialize(out, offset, quickStatusMode);

  // QS bookkeeping
  Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.fmtLinkEn);
  Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.QSDisabled);
  Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.waitingForECR);

  // ROD registers
  Serializer<uint16_t>::serialize(out, offset, m_qsMonInfos.masterBusy);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.slaveBusyCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.slaveBusyCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkOccCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.linkOccCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkTrigCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.linkTrigCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.totDesyncCounters)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint16_t>::serialize(out, offset, m_qsMonInfos.totDesyncCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.timeoutCounters)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint16_t>::serialize(out, offset, m_qsMonInfos.timeoutCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.htCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.htCounters[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.masterBusyCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.masterBusyCounters[i]);

  // QS action counters
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.nCFG_rodBusy)/sizeof(uint8_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint8_t>::serialize(out, offset, m_qsMonInfos.nCFG_rodBusy[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.nCFG_timeout)/sizeof(uint8_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint8_t>::serialize(out, offset, m_qsMonInfos.nCFG_timeout[i]);

  // BOC registers
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.decodingErrorBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.decodingErrorBOC[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameErrorBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.frameErrorBOC[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameCountBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint32_t>::serialize(out, offset, m_qsMonInfos.frameCountBOC[i]);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.meanOccBOC)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::serialize overload
    Serializer<uint16_t>::serialize(out, offset, m_qsMonInfos.meanOccBOC[i]);

  Serializer<IblBoc::SLinkInfo>::serialize(out, offset, m_qsMonInfos.SLinkInfo);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void QSTalkTaskResult::deserialize(const uint8_t *in) {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  uint32_t offset = 0;

  // QS mode
  quickStatusMode = Serializer<uint32_t>::deserialize(in, offset);

  // QS bookkeeping
  m_qsMonInfos.fmtLinkEn = Serializer<uint32_t>::deserialize(in, offset);
  m_qsMonInfos.QSDisabled = Serializer<uint32_t>::deserialize(in, offset);
  m_qsMonInfos.waitingForECR = Serializer<uint32_t>::deserialize(in, offset);

  // ROD registers
  m_qsMonInfos.masterBusy = Serializer<uint16_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.slaveBusyCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.slaveBusyCounters[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkOccCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.linkOccCounters[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.linkTrigCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.linkTrigCounters[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.totDesyncCounters)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.totDesyncCounters[i] = Serializer<uint16_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.timeoutCounters)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.timeoutCounters[i] = Serializer<uint16_t>::deserialize(in, offset);
	 
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.htCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.htCounters[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.masterBusyCounters)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.masterBusyCounters[i] = Serializer<uint32_t>::deserialize(in, offset);
 
  // Action counters
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.nCFG_rodBusy)/sizeof(uint8_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.nCFG_rodBusy[i] = Serializer<uint8_t>::deserialize(in, offset);
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.nCFG_timeout)/sizeof(uint8_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.nCFG_timeout[i] = Serializer<uint8_t>::deserialize(in, offset);

  // BOC registers
  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.decodingErrorBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.decodingErrorBOC[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameErrorBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.frameErrorBOC[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.frameCountBOC)/sizeof(uint32_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.frameCountBOC[i] = Serializer<uint32_t>::deserialize(in, offset);

  for(size_t i = 0 ; i < sizeof(m_qsMonInfos.meanOccBOC)/sizeof(uint16_t) ; ++i) // Todo: use Serialzer::deserialize overload
    m_qsMonInfos.meanOccBOC[i] = Serializer<uint16_t>::deserialize(in, offset);

  m_qsMonInfos.SLinkInfo = Serializer<IblBoc::SLinkInfo>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t QSTalkTaskResult::serializedSize() const {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  return (4*sizeof(uint32_t) + sizeof(uint16_t)
	  + sizeof(m_qsMonInfos.slaveBusyCounters)
	  + sizeof(m_qsMonInfos.linkOccCounters) + sizeof(m_qsMonInfos.linkTrigCounters) 
	  + sizeof(m_qsMonInfos.totDesyncCounters) + sizeof(m_qsMonInfos.timeoutCounters) 
          + sizeof(m_qsMonInfos.htCounters) + sizeof(m_qsMonInfos.masterBusyCounters) 
          + sizeof(m_qsMonInfos.nCFG_rodBusy) + sizeof(m_qsMonInfos.nCFG_timeout)
	  + sizeof(m_qsMonInfos.decodingErrorBOC) + sizeof(m_qsMonInfos.frameErrorBOC) + sizeof(m_qsMonInfos.frameCountBOC) + sizeof(m_qsMonInfos.meanOccBOC) + sizeof(m_qsMonInfos.SLinkInfo)
	  );
}
