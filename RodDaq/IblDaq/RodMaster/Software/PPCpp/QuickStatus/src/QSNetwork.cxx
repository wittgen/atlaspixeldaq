#include <stdint.h>
#include <unistd.h>
#include <cstring>
#include "QSNetwork.h"


uint8_t QSNetwork::get8(uint8_t* intbuf){
  uint8_t value = intbuf[m_len];
  m_len+=1;
  return value;
}

uint16_t QSNetwork::get16(uint8_t *intbuf) {
  uint16_t value = (intbuf[m_len] << 8) | intbuf[m_len+1];
  m_len+=2;
  
  return value; 
}

uint32_t QSNetwork::get32(uint8_t *intbuf) {
  uint32_t value = (((intbuf[m_len] << 24) | (intbuf[m_len+1] << 16)) | intbuf[m_len+2] << 8  ) | intbuf[m_len+3];
  m_len+=4;
  
  return value;
}


void QSNetwork::SetupQSThread(const char* ipAddress, uint16_t port)
{

#ifdef __XMK__

  //ugly. Todo: fix
  char* ipAddr = strdup(ipAddress);
  xil_printf("Setup QS...%s:%d \n",ipAddr,port);
  qs->setNetwork(ipAddr,port);

#endif
}


#ifdef __XMK__

bool QSNetwork::initializeServer() {
  
  
  
  //I think I can put a TCP/IP here..
  socketId = socket(AF_INET,SOCK_DGRAM,0);
  
  if (socketId<0) {
    xil_printf("Error in QSNetwork socket creation\n");
    return false;
  }
  
  xil_printf("Created QSNetwork socket %d\n",socketId);
  bzero(&server,sizeof(server));
  
  
  server.sin_family = AF_INET;
  server.sin_addr.s_addr=INADDR_ANY;
  server.sin_port=htons(10776);
  
  //bind the socket
  
  if (bind(socketId, (struct sockaddr*) &server, sizeof(server))<0)
    {      
      xil_printf("Error in binding socket\n");
      return false;
    }
  return false;
  
  
  
}

QSNetwork* QSNetwork::getInstance() {
  
  static QSNetwork *instance = 0;
    if(!instance) {
        instance = new QSNetwork();
        instance->run();
    }
    if(!instance)
      std::cerr << "ERROR: couldn't create Quick Status Network instance!!!" << std::endl;
    return instance;
}
#endif 

void QSNetwork::run() {
  
#ifdef __XMK__
  
  //You can put this in the UDPSocket class. Just instead initialize the server?
  initializeServer();
  sleep(2);
  
  //Set the clienlen
  clientlen = sizeof client;

  //Get the QS instance
  qs = QuickStatus::getInstance();
  if (qs)
    xil_printf("Acquired QS pointer at %04x \n", qs );
  else
    {
      xil_printf("Null pointer to qs");
      return;
    }

  
  
  //Now just loop to wait for what to do from the Actions
  
  while (1) {
    
    //wait for Actions connection to set the QS Mode
    
    
    n = recvfrom(socketId,buffer,sizeof(buffer),0,(struct sockaddr*) &client,&clientlen);
    
    

    if (n<0)
      xil_printf("Error receiving packet");
    
    
    //Temporary packet structure
    //-> Not implemented yet //32-bit header
    //8-bit qsttmode
    //16-bit port
    //8-bit qsmode
    
    
    uint8_t qsttmode = get8(buffer);
    uint16_t port = get16(buffer);
    uint8_t quickStatusMode = get8(buffer);
    uint8_t rstActionCounters = get8(buffer);
    //conversion from uint8_t to boolean... (just to be sure...)
    bool rstActionCounters_b = false;
    if (rstActionCounters > 0)
      rstActionCounters_b = true;
    

    char ipAddress[INET_ADDRSTRLEN];
    inet_ntop(AF_INET, &(client.sin_addr), ipAddress, sizeof(ipAddress));
    //xil_printf("IP:%s",ip);
    //xil_printf("qsttmode %d\n",qsttmode);
    //xil_printf("ipAddress %s\n",ipAddress);
    //xil_printf("port %d\n",port);
    //xil_printf("quickStatusMode %d\n",quickStatusMode);

    switch (qsttmode) {
      //Deprecated
      //case QSTT_INIT:
      //SetupQSThread(ipAddress,port);
      //break;
    case QSTalkTask::QSTT_INFO:
      
      xil_printf("Retrieve qs_monitoring info\n");
      
      break;
      
    case QSTalkTask::QSTT_GET_STATUS:
      xil_printf("########## QS dump info ##########\n");
      xil_printf("Quick Status information (read out by the QSTalkTask)\n");
      xil_printf("Quick Status mode: %s\n", QuickStatus::ModeToString(qs->getQSMode()).c_str());
      xil_printf("QS sleep (in ms) btw fine cnt: %d\n", qs->getQSSleep());
      xil_printf("Max Value for fc: %d\n", qs->getFCMax());
      xil_printf("Threshold for busy error: %d\n", qs->getBUMax());
      xil_printf("Threshold for timeout error: %d\n", qs->getToMax());
      xil_printf("Max Value for rst: %d\n", qs->getRstMax());
      xil_printf("Max Value for TO cfg: %d\n", qs->getTOCfgMax());
      xil_printf("Max Value for BU cfg: %d\n", qs->getBUCfgMax());
      xil_printf("Sleep delay (FE-I4): %d\n", qs->getSleepDelay());
      xil_printf("Verbosity level: %d\n", qs->getVerbosityLevel());
      xil_printf("Networking %s:%d\n",qs->getMonIp(),qs->getMonPort());
      qs->dumpMasks();
      xil_printf("##########  End of dump ##########\n");
      break;
    case QSTalkTask::QSTT_SET_MODE:
      qs->resetUserAction();
      qs->setQSMode(quickStatusMode);
      std::cout << "QSTT_SET_MODE: " << QuickStatus::ModeToString(qs->getQSMode()) << std::endl;
      break;
    default:
      xil_printf("Wrong selected QS mode");
      break;
	
    }
    
    if (qsttmode == QSTalkTask::QSTT_INFO)
      {						
	
	sockaddr_in client_addr;
	client_addr.sin_family = AF_INET;
	client_addr.sin_addr.s_addr=inet_addr(ipAddress);
	client_addr.sin_port=htons(port);

	QSTalkTask dummyQSTalkTask;
	dummyQSTalkTask.execute();
	//dummyQSTalkTask.result.m_qsMonInfos = qs->getMonitoringInfos();
	const uint32_t buflen = dummyQSTalkTask.result.serializedSize();
	xil_printf("buflen = %d\n",buflen);
	uint8_t buf[buflen];

	//memset(buf,0,buflen);

	dummyQSTalkTask.result.serialize(buf);
	//xil_printf("Buffer looks like %d %d %d %d\n",buf[0],buf[1],buf[2],buf[3]); 
	
	n = sendto(socketId,buf,buflen,0,(const struct sockaddr *) &client_addr, sizeof(struct sockaddr_in));
	
	xil_printf("Sent data to the host\n");
	
	if (n<0)
	  xil_printf("Send error\n");
	
	//xil_printf("Packet sent\n");

	qs->resetQSCounters(rstActionCounters_b);
      }

        
    clear();

  }
  
  
#endif
  
}
