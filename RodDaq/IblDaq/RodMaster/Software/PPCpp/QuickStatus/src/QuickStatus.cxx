#include "CppCompatibility.h" // Keep this header first!
#include "CountOccupancy.h"
#include "QuickStatus.h"
#include "ReadBocCounters.h"
#include "ReconfigModules.h"
#include "RecoAtECR.h"
#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodResourceManager.h"
#include "DataTakingTools.h"
#include "UDPSocket.h"
#endif
#include "RodMasterRegisters.h"
#include <iterator>

#include "primXface_common.h"


#ifdef __XMK__
pthread_mutex_t QuickStatus::m_rwMutex = PTHREAD_MUTEX_INITIALIZER;
#endif

std::string QuickStatus::ModeToString(int mode)
{
  switch (mode) {
  case QS_OFF: return "QS_OFF";
  case QS_INFO: return "QS_INFO";
  case QS_IDLE: return "QS_IDLE";
  case QS_MON: return "QS_MON";
  case QS_RST: return "QS_RST";
  case QS_RST_DIS: return "QS_RST_DIS";
  case QS_RST_CFG_ON: return "QS_RST_CFG_ON";
  case QS_RST_CFG_OFF: return "QS_RST_CFG_OFF";
  }
  return std::string("UNKNOWN");
}

QuickStatus::QuickStatus() {

    //Todo:check for error
#ifdef __XMK__
    m_quickStatusMode = QS_OFF;
    init();
#endif 
}

void QuickStatus::init(){
#ifdef __XMK__
  m_quickStatusSleep = 100; 	// in ms
  m_verbosityLevel = 0;
  m_setup=false;
  m_ipAddress="255.255.255.255";
  m_port     =11111;
  m_monitoring_mask = 0;
  m_monitoring_mode = MON_IDLE;

  // these are the defaults from Pixel, still good for IBL?
  m_fc_max = 250;
  m_bu_max = 100;
  m_to_max = 10;
  m_ht_max = 10;
  m_rst_max = 7;
  m_bu_cfg_max = 3;
  m_to_cfg_max = 1;
  m_ht_cfg_max = 1;
  
  for (size_t i=0; i < 32; ++i) {
    m_rodbusyCounter[i] = 0;
    m_timeoutCounter[i] = 0;
    m_nTriggers[i] = 0;
    m_htCounter[i] = 0;
  }
  m_timeoutError = 0;
  m_htError = 0;
  m_rodBusyError = 0;
  m_nClocks = 0;

  // =================================
  // Initializing monitoring variables
  QuickStatus::resetQSMonInfo(m_infos, true, true, true);
  // =================================    

  m_counter_coarse=0;		// Coarse counter
  m_counter_fine=0;		// Fine counter

  m_hasReconfigured=false;
  m_spy_mode=false;		// Running in spy mode by default
  m_sleepDelay=100;	// Default value used in data-taking

  m_mask=0;
  nUserActions=0;
  userActions.clear();
#endif 
}

#ifdef __XMK__
QuickStatus* QuickStatus::getInstance() {
    static QuickStatus *instance = 0;
    if(!instance) {
        instance = new QuickStatus();
        instance->run();
    }
    if(!instance)
        std::cerr << "ERROR: couldn't create QuickStatus instance!!!" << std::endl;
    return instance;
}
#endif



void QuickStatus::monitoring() {
#ifdef __XMK__
  if(m_quickStatusMode == QS_IDLE || m_quickStatusMode == QS_OFF) return;
  if(getVerbosityLevel() > 20 ) xil_printf("%s\n", __PRETTY_FUNCTION__);

  // do  monitoring part
  // if some criteria is fullfilled (this means some action should be done)
  // return state of the state machine, go back in the while loop
  // solve the problem, set the state back (start monitoring again)
  //
  // IMPORTANT NOTE:
  // if some criteria is fullfilled it directly tries to recover this 
  // (if QS allows this)
  // CHECK: the ordering for the checks have to be prioritized
  // (things which are most important should go up!)

  // initilize the monitoring information, which are returned
  // they give back the correct mode and a mask

  //buildBusyHistoInSw();
  //buildBusyHistoInFw();

  QuickStatus::monitoring_infos queryInfos;
  resetQSMonInfo(queryInfos, 1);

  // Using in-FW counters (different from those from the BUSY logic, which provide only the number of times an item was BUSY)
  CountOccupancy::getBusyCountersFromFW(queryInfos.slaveBusyCounters, true); // true because counters are en-/dis-abled

  // Also handles the desynch registers
  CountOccupancy::getOccCountersFromFW(queryInfos.linkTrigCounters, queryInfos.linkOccCounters, queryInfos.totDesyncCounters, queryInfos.timeoutCounters, queryInfos.htCounters, queryInfos.masterBusyCounters, true); // true because counters are en-/dis-abled

  // Accumulate busy, timeout and desynchronization counters etc. for QS analysis
  // Only do it for ROD registers
  for(size_t i = 0 ; i < sizeof(m_infos.slaveBusyCounters)/sizeof(uint32_t) ; i++)
    m_infos.slaveBusyCounters[i] += queryInfos.slaveBusyCounters[i];

  for(int i=0; i<32; i++){
    m_infos.linkTrigCounters[i]+=queryInfos.linkTrigCounters[i];
    m_infos.linkOccCounters[i]+=queryInfos.linkOccCounters[i];
    m_infos.totDesyncCounters[i]+=queryInfos.totDesyncCounters[i];
    m_infos.timeoutCounters[i]+=queryInfos.timeoutCounters[i];
    m_infos.htCounters[i]+=queryInfos.htCounters[i];
    m_nTriggers[i]=queryInfos.linkTrigCounters[i];
  }

  //master busy
  m_infos.masterBusyCounters[0] += queryInfos.masterBusyCounters[0];
  m_infos.masterBusyCounters[1] += queryInfos.masterBusyCounters[1];
  m_infos.masterBusyCounters[2] += queryInfos.masterBusyCounters[2];
  m_infos.masterBusyCounters[3] += queryInfos.masterBusyCounters[3];
  m_infos.masterBusyCounters[4] += queryInfos.masterBusyCounters[4];
  m_infos.masterBusyCounters[5] += queryInfos.masterBusyCounters[5];
  m_infos.masterBusyCounters[6] += queryInfos.masterBusyCounters[6];
  m_infos.masterBusyCounters[7] += queryInfos.masterBusyCounters[7];


  m_infos.fmtLinkEn = (IblRod::SlaveA::FmtLinkEnable::read() & 0xFFFF) | ((IblRod::SlaveB::FmtLinkEnable::read() & 0xFFFF) << 16);
  // Update QS disable mask if some FMT links have been switched on by other machineries
  m_infos.QSDisabled &= (~m_infos.fmtLinkEn);
  // Update the link mask waiting for ECR
  m_infos.waitingForECR &= (~(m_infos.fmtLinkEn | m_infos.QSDisabled));
  
  

  m_nClocks=queryInfos.slaveBusyCounters[0];
  // Also aggregate the counters for QS automatic action
  const uint32_t slvBusyCounterOffset=6;
  for(size_t sId = 0 ; sId < 2; ++sId) {
    for(size_t i = 0 ; i < 16 ; i++) {
      int iMod=sId*16+i;
      int slvBusyIdx=sId*32+slvBusyCounterOffset+i;
      m_timeoutCounter[iMod] = queryInfos.timeoutCounters[iMod];
      m_rodbusyCounter[iMod] = queryInfos.slaveBusyCounters[slvBusyIdx];    
    }
  }

  getRODMasterBusy();

  if( m_verbosityLevel >= 10 ) {
    const uint32_t slvBusyCounterOffset=6;
    for(size_t sId = 0 ; sId < 2; ++sId) {
      for(size_t i = 0 ; i < 16 ; i++) {
	int iMod=sId*16+i;
	int slvBusyIdx=sId*32+slvBusyCounterOffset+i;
	if(((1<<iMod) & m_infos.fmtLinkEn)){
	  // At every ECR, the busy counter will increase by 2. Therefore we require it to be larger than 2 to avoid pick up busy caused by ECR.
	  if(m_infos.slaveBusyCounters[slvBusyIdx]>2) xil_printf("---# ATTENTION: Slave BUSY counters: Link %2d: %d \n", iMod, m_infos.slaveBusyCounters[slvBusyIdx]);
	}
      }
    }

    for(size_t i = 0 ; i < sizeof(m_infos.timeoutCounters)/sizeof(uint32_t) ; ++i){
      if(((1<<i) & m_infos.fmtLinkEn)){
	if(m_infos.timeoutCounters[i]>0) xil_printf("---# ATTENTION: Timeout counters: Link %2d: %d \n", i, m_infos.timeoutCounters[i]);
      }
    }

    if(m_verbosityLevel>=20){
      for(size_t i = 0 ; i < sizeof(m_infos.totDesyncCounters)/sizeof(uint32_t) ; ++i){
	if(((1<<i) & m_infos.fmtLinkEn)){
	  if(m_infos.totDesyncCounters[i]>0) xil_printf("---# ATTENTION: Total desync counters: Link %2d: %d \n", i, m_infos.totDesyncCounters[i]);
	}
      }

      for(size_t i = 0 ; i < sizeof(m_infos.linkOccCounters)/sizeof(uint32_t) ; ++i){
	if(((1<<i) & m_infos.fmtLinkEn)){
	  if(m_infos.linkOccCounters[i]>0) xil_printf("---# ATTENTION: Link occupancy counters: Link %2d: %d \n", i, m_infos.linkOccCounters[i]);
	}
      }

      for(size_t i = 0 ; i < sizeof(m_infos.linkTrigCounters)/sizeof(uint32_t) ; ++i){
	if(((1<<i) & m_infos.fmtLinkEn)){
	  if(m_infos.linkTrigCounters[i]>0) xil_printf("---# ATTENTION: Link trigger counters: Link %2d: %d \n", i, m_infos.linkTrigCounters[i]);
	}
      }

      for(size_t i = 0 ; i < sizeof(m_infos.htCounters)/sizeof(uint32_t) ; ++i){
	if(((1<<i) & m_infos.fmtLinkEn)){
	  if(m_infos.htCounters[i]>0) xil_printf("---# ATTENTION: Header-trailer counters: Link %2d: %d \n", i, m_infos.htCounters[i]);
	}
      }

    }
  }


  // if nothing has happend
  m_monitoring_mask = 0;
  m_monitoring_mode = MON_IDLE;
#endif
}


void QuickStatus::run() {

#ifdef __XMK__
  
  //Todo implement a conditional variable.
  //bool printout=true;
  //while (!m_setup)
  //{
  //  if (printout)
  //	xil_printf("QS waiting for setup...\n");
  //  sleep(2);
  //  printout  = false;
  //}

  //static UDPSocket udpSocket;
  //static bool udpInitOK = udpSocket.initializeClient(m_ipAddress,m_port);

  
  xil_printf("Starting QuickStatus daemon...\n");
  std::unique_lock<std::mutex> lk(cv_m);
  int qs_mode;

  bool wokenup = 0;
    while(1) {
      
      //pthread_mutex_lock(&QuickStatus::m_rwMutex);
      qs_mode = getQSMode();
      if(getVerbosityLevel() > 20 ) xil_printf("QS Mode = %s\n", QuickStatus::ModeToString(qs_mode).c_str());
      //pthread_mutex_unlock(&QuickStatus::m_rwMutex);


      // has the thread just woke up?
      wokenup = 0;

      // condition functon for condition variable cv
      auto cnd_fce = [this, &wokenup]() {
          if(getQSMode() >= QS_MON) return 1;
          else {
              wokenup = 1;
              return 0;
          }
      };
      cv.wait(lk,cnd_fce);

      // re-acquire mode, qs_mode might be obsolete if thread was sleeping
      qs_mode = getQSMode();
      
      if(qs_mode >= QS_MON) monitoring();

      // reset counters if thread just woke up
      if(wokenup){
          QuickStatus::resetQSMonInfo(m_infos, true, true, true);
          continue;
      }

      
      // Kill task if coarse counter gets too large (hasn't been reset by talk task)

      if (m_counter_coarse > 3000){ //  5 min

      	xil_printf("QS_LST_WRDS: Timeout for being read...Self-terminating.(ccnt =  %d)\n",m_counter_coarse);
      	m_quickStatusMode = QS_IDLE; // if the task is restarted this seems to matter
      	m_counter_coarse =0;
      }

      // if Quick status is turned off, in info or idle -->  doing nothing
      if (qs_mode != QS_INFO && qs_mode != QS_OFF && qs_mode != QS_IDLE){
	if(m_counter_coarse%10 ==0) getBOCCounters();	  // Read BOC registers every 10 coarse count ~1s
	  
	
	  
	  if (qs_mode == QS_RST || qs_mode == QS_RST_DIS || qs_mode == QS_RST_CFG_ON || qs_mode == QS_RST_CFG_OFF){
	    coarseCounteReaction();
	}
        std::list<MON_MODES> userActionList = getUserActions();
        if (userActionList.size()) {
          m_monitoring_mode = userActionList.front();
          m_monitoring_mask = getMask();
          xil_printf("QS User action will be executed %d\n", m_monitoring_mode);
          removeUserAction();
          increaseNUserActions();
        }

	if(m_monitoring_mode==MON_RECONFIGURE
	   ||m_monitoring_mode==MON_EXPERT_RECONFIGURE_ON
	   ||m_monitoring_mode==MON_EXPERT_RECONFIGURE_OFF
	   ){
	  if((m_infos.waitingForECR&m_monitoring_mask)!=0x0){
	    xil_printf("QSINFO: FMT links 0x%.8X still waiting to be enabled at next ECR. Reduce the mask from 0x%.8X to 0x%.8X\n", m_infos.waitingForECR&m_monitoring_mask, m_monitoring_mask, (~m_infos.waitingForECR)&m_monitoring_mask);

	    m_monitoring_mask&=(~m_infos.waitingForECR);
	  }
	}

        switch (m_monitoring_mode)
        {
	case MON_IDLE:
	  // QS_MON is done always (if QS is not off/idle/info)
	  break;
	  
	case MON_RESAMPLE_PHASE: {
	  xil_printf("QSINFO: resampling and resetting the corresponding Boc counter for 0x%.8X\n", m_monitoring_mask);
	  DataTakingTools::bocResampleRxPhase(m_monitoring_mask);
	  ReadBocCounters bocCounterCmd;
	  bocCounterCmd.resetBocCounters();
	}  break;

	case MON_RECONFIGURE: {
	  xil_printf("QSINFO: reconfigure modules 0x%.8X\n", m_monitoring_mask);
	  reconfigModule(m_monitoring_mask, m_infos.QSDisabled, m_infos.waitingForECR);
	  m_monitoring_mask &= m_infos.waitingForECR;
	  m_infos.fmtLinkEn &= (~(m_infos.QSDisabled | m_infos.waitingForECR));
	}  break;
	  
	case MON_EXPERT_RECONFIGURE_ON: {
	  xil_printf("QSINFO: reconfigure modules 0x%.8X in expert mode. Enforce pre-amp status ON\n", m_monitoring_mask);
	  expertReconfigModule(m_monitoring_mask, true, m_infos.QSDisabled, m_infos.waitingForECR);
	  m_monitoring_mask &= m_infos.waitingForECR;
	  m_infos.fmtLinkEn &= (~(m_infos.QSDisabled | m_infos.waitingForECR));
	}  break;
	  
	case MON_EXPERT_RECONFIGURE_OFF: {
	  xil_printf("QSINFO: reconfigure modules 0x%.8X in expert mode. Enforce pre-amp status OFF\n", m_monitoring_mask);
	  expertReconfigModule(m_monitoring_mask, false, m_infos.QSDisabled, m_infos.waitingForECR);
	  m_monitoring_mask &= m_infos.waitingForECR;
	  m_infos.fmtLinkEn &= (~(m_infos.QSDisabled | m_infos.waitingForECR));
	}  break;
	case MON_DISABLE: {
	  xil_printf("QSINFO: disable FMT links 0x%.8X\n", m_monitoring_mask);
	  ReconfigModules::disableFMTLinks(m_monitoring_mask, m_infos.waitingForECR);
	  m_infos.QSDisabled|=(m_monitoring_mask&(m_infos.fmtLinkEn|m_infos.waitingForECR));
	  m_infos.fmtLinkEn=ReconfigModules::checkFMTLinks();
	  break;
	}
	case MON_ENABLE: {
	  xil_printf("QSINFO: enable FMT links 0x%.8X\n", m_monitoring_mask);
	  ReconfigModules::enableFMTLinks(m_monitoring_mask, m_infos.waitingForECR, m_writeRegAtECR);
	  m_infos.QSDisabled &= ~(m_monitoring_mask & ~m_infos.fmtLinkEn);
	  m_infos.waitingForECR |= (m_monitoring_mask & ~m_infos.fmtLinkEn);
	  break;
	}
	case MON_DISABLE_BOC: {
	  xil_printf("QSINFO: disable BOC RX links 0x%.8X\n", m_monitoring_mask);
	  DataTakingTools::disableBocRx(m_monitoring_mask);
	  break;
	}
	case MON_ENABLE_BOC: {
	  xil_printf("QSINFO: enable BOC RX 0x%.8X\n", m_monitoring_mask);
	  DataTakingTools::enableBocRx(m_monitoring_mask|m_infos.fmtLinkEn);
	  break;
	}
	}
      }

      //pthread_mutex_unlock(&QuickStatus::m_rwMutex);
      std::this_thread::sleep_for( std::chrono::milliseconds(m_quickStatusSleep));

    }

    // Should never be reached

  xil_printf("\nQuickStatus daemon shutting down...\n");

#endif
}

int QuickStatus::coarseCounteReaction()
{
#ifdef __XMK__
  const uint32_t N_MODULES=32; // it equals to 32

  uint32_t ENtemp=m_infos.fmtLinkEn, QDtemp=m_infos.QSDisabled, WEtemp=m_infos.waitingForECR;
  uint32_t TOtemp=0x0, BUtemp=0x0;
  uint32_t reconfigMask=0x0, disableMask=0x0;

  // // *** CHECK IF S-LINK XOFF (RTR_CMND_STAT, bit 8)
  const int slvClockCntIdx=0;
  const uint32_t slaveBusyClockCnt[2] = {m_infos.slaveBusyCounters[slvClockCntIdx], m_infos.slaveBusyCounters[slvClockCntIdx+32]};
  // Subtractions faster than division: using them since we done need the exact ratio
  bool sLink0XOff = ( slaveBusyClockCnt[0] - m_infos.slaveBusyCounters[2] <= 1000 ) || ( slaveBusyClockCnt[1] - m_infos.slaveBusyCounters[2+32] <= 1000 );
  bool sLink0Down = ( slaveBusyClockCnt[0] - m_infos.slaveBusyCounters[3] <= 1000 ) || ( slaveBusyClockCnt[1] - m_infos.slaveBusyCounters[3+32] <= 1000 );
  bool sLink1XOff=false, sLink1Down=false;
  if(RodResMgr::isFei4()){
    sLink1XOff = ( slaveBusyClockCnt[0] - m_infos.slaveBusyCounters[4] <= 1000 ) || ( slaveBusyClockCnt[1] - m_infos.slaveBusyCounters[4+32] <= 1000 );
    sLink1Down = ( slaveBusyClockCnt[0] - m_infos.slaveBusyCounters[5] <= 1000 ) || ( slaveBusyClockCnt[1] - m_infos.slaveBusyCounters[5+32] <= 1000 );
  }

  bool sLinkXOffOrDown = sLink0XOff || sLink0Down || sLink1XOff || sLink1Down;	
  bool isTimeout=false;

  // *** CHECK ARRAYS FOR UPDATED LINK STAT AND TIMEOUT ERR COUNTS    
  for(uint8_t iMod =0; iMod<N_MODULES; iMod++){
    unsigned int toPerc = 0;
    if ((m_timeoutCounter[iMod]) > 5) toPerc = (unsigned int)(1000.*m_timeoutCounter[iMod]/(m_timeoutCounter[iMod]+m_nTriggers[iMod]));

    // *** DEAL WITH TIMEDOUT LINKS ***
    if( (ENtemp & (1 << iMod)) && toPerc > m_to_max){
      isTimeout = true;		// Flag existance of timeout error (not to a particular FMT link according to old QS implementation)
      int fmt=iMod/4, lnk=iMod%4;

      xil_printf("PROB::TO Cnt %d Over Thr %d on FMT %d, Link %d\n ",toPerc,m_to_max,fmt,lnk);
      xil_printf("I found %d timeouts over %d events\n",m_timeoutCounter[iMod],m_nTriggers[iMod]);

      // *** DISABLE LINK ***
      if ( m_quickStatusMode == QS_RST_DIS ){
	if(sLinkXOffOrDown == 1){
	  xil_printf("XOFF,NO ACTION:: Would have DIS FMT %d, Link %d\n ",fmt,lnk);
	}
	else{
	  xil_printf("ACTION:: DIS FMT %d, Link %d\n ",fmt,lnk);
	  if(!m_spy_mode) disableMask |= (ENtemp & (1 << iMod));
	  ENtemp &= (~(1 << iMod)); 
	  QDtemp |= (1 << iMod);
	  m_timeoutCounter[iMod] = 0;  
	}
      }
				
      // *** RECONFIGUE MOD ***
      if ( m_quickStatusMode == QS_RST_CFG_ON || m_quickStatusMode == QS_RST_CFG_OFF ){
					
	if(sLinkXOffOrDown){
	  xil_printf("XOFF,NO ACTION:: Would have RECFG/DIS FMT %d, Link %d\n ",fmt,lnk);
	} else if(m_hasReconfigured){
      	  xil_printf("NO ACTION, after reconf in past coarse count, would have RECFG FMT %d, Link %d\n ",fmt,lnk);
        } else if( m_infos.nCFG_timeout[iMod] < m_to_cfg_max ){
	  std::string preAmpsStat=(m_quickStatusMode == QS_RST_CFG_ON) ? "PA-ON" : "PA-OFF";
	  xil_printf("ACTION:: RECFG(%s) MOD %d, FMT %d, Link %d, # %d\n ", preAmpsStat.c_str(), iMod, fmt, lnk, m_infos.nCFG_timeout[iMod]);
	  if(!m_spy_mode) reconfigMask |= (ENtemp & (1 << iMod)); // reconfigModule(ENtemp & (1 << iMod));
	  m_timeoutCounter[iMod] = 0;
	  m_infos.nCFG_timeout[iMod]++;
	}
	else{
	  xil_printf("ACTION:: Exc max CFG # on FMT %d, Link %d, DIS link\n ",fmt,lnk);
	  if(!m_spy_mode) disableMask |= (ENtemp & (1 << iMod));
	  ENtemp &= (~(1 << iMod)); 
	  QDtemp |= (1 << iMod);
	  m_timeoutCounter[iMod] = 0;  
	}
      } // End TO
    }// LNK
  } // FMT
	
  for(uint8_t iMod =0; iMod<N_MODULES; iMod++){
    unsigned int  buPerc = 0;
    if(m_nClocks > 1)buPerc =(unsigned int)(1000.* m_rodbusyCounter[iMod]/m_nClocks);

    if(  (ENtemp & (1 << iMod)) && buPerc > m_bu_max ){
      int fmt=iMod/4, lnk=iMod%4;

      xil_printf("PROB:: BU Cnt %d Over Thr %d on FMT %d, Link %d\n ",buPerc, m_bu_max,fmt,lnk);
      xil_printf("I found %d clocks busy over %d clocks\n",m_rodbusyCounter[iMod], m_nClocks);
	
      if(sLinkXOffOrDown){
	xil_printf("XOFF,NO ACTION:: Would have perf RECFG/DIS on FMT %d, Link %d\n ",fmt,lnk);
      } else if(m_hasReconfigured){
      	  xil_printf("NO ACTION, after reconf in past coarse count, would have RECFG FMT %d, Link %d\n ",fmt,lnk);
       } else{
	if(isTimeout){
	  xil_printf("PROB:: TO, no BU corrective action\n ");
	}
	else if((m_quickStatusMode == QS_RST || m_quickStatusMode == QS_RST_DIS ||  m_quickStatusMode == QS_RST_CFG_OFF || m_quickStatusMode == QS_RST_CFG_ON)){
	  if( m_infos.nCFG_rodBusy[iMod] < m_bu_cfg_max ){
	    std::string preAmpsStat=(m_quickStatusMode == QS_RST_CFG_ON) ? "PA-ON" : "PA-OFF";
	    xil_printf("ACTION:: RECFG(%s) MOD %d, FMT %d, Link %d, # %d\n ", preAmpsStat.c_str(), iMod, fmt, lnk, m_infos.nCFG_rodBusy[iMod]);
	    if(!m_spy_mode) reconfigMask |= (ENtemp & (1 << iMod)); //reconfigModule(ENtemp & (1 << iMod));
	    m_rodbusyCounter[iMod] = 0; 
	    m_infos.nCFG_rodBusy[iMod]++;
	  }
	  else{
	    xil_printf("ACTION:: Exc max CFG # on FMT %d, Link %d, DIS link\n ",fmt,lnk);
	    if(!m_spy_mode) disableMask |= (ENtemp & (1 << iMod));
	    ENtemp &= (~(1 << iMod)); 
	    QDtemp |= (1 << iMod);
	    m_rodbusyCounter[iMod] = 0;  
	  }
	}
      } // End BU
    } // LNK
  } // FMT

  if(reconfigMask!=0x0){
    reconfigModule(reconfigMask, QDtemp, WEtemp);
    ENtemp &= ~(QDtemp | WEtemp);
    m_hasReconfigured= true;
  } else m_hasReconfigured= false;

  if(disableMask!=0x0) ReconfigModules::disableFMTLinks(disableMask, WEtemp);
  // *** Desynchronization action will be migrated from high level code once the algorithm is stable ***

  // ***  FILL QS INFO ****
  if(m_verbosityLevel>=10) xil_printf("QSINFO:: From proc on Coarse Count %d\n", m_counter_coarse);

  for(uint8_t iMod=0; iMod<N_MODULES; iMod++){
    if(m_timeoutCounter[iMod]>0) TOtemp |= (1<<iMod);
    if(m_rodbusyCounter[iMod]>0) BUtemp |= (1<<iMod);
    // *** RESET ARRAYS: the period of coarse count should be long enough to allow triggering auto actions ***
    m_timeoutCounter[iMod]=0;
    m_rodbusyCounter[iMod]=0;
  }

  if(!m_spy_mode){
    m_infos.fmtLinkEn=ENtemp;	
    m_infos.QSDisabled=QDtemp;
    m_infos.waitingForECR=WEtemp;
  }
  m_timeoutError=TOtemp;
  m_rodBusyError=BUtemp;

  // Will not want to print this during normal operation
  if(m_verbosityLevel>=10) dumpMasks();
#endif	
  m_counter_coarse++;
  return m_counter_coarse;
  //xil_printf("----------------------------\n");
}

void QuickStatus::reconfigModule(uint32_t reconfigMask, uint32_t &QSDisabled, uint32_t &waitingForECR){
#ifdef __XMK__
  ReconfigModules reconfigCmd;
  reconfigCmd.setWriteRegAtECR(m_writeRegAtECR);
  reconfigCmd.setRxMask(reconfigMask);
  reconfigCmd.setWaitingForECR(waitingForECR);
  reconfigCmd.setSleepDelay(m_sleepDelay);
  reconfigCmd.setForcePreAmpsStatus(false);
  reconfigCmd.execute();
  QSDisabled|=reconfigCmd.getNotReconfiguredRxMask();
  waitingForECR|=reconfigCmd.getWaitingForECRRxMask();
#endif
}

void QuickStatus::expertReconfigModule(uint32_t reconfigMask, bool preAmpsOn, uint32_t &QSDisabled, uint32_t &waitingForECR){
#ifdef __XMK__
  ReconfigModules reconfigCmd;
  reconfigCmd.setWriteRegAtECR(m_writeRegAtECR);
  reconfigCmd.setRxMask(reconfigMask);
  reconfigCmd.setWaitingForECR(waitingForECR);
  reconfigCmd.setSleepDelay(m_sleepDelay);
  reconfigCmd.setForcePreAmpsStatus(true);
  reconfigCmd.setPreAmpsOn(preAmpsOn);
  reconfigCmd.execute();
  QSDisabled|=reconfigCmd.getNotReconfiguredRxMask();
  waitingForECR|=reconfigCmd.getWaitingForECRRxMask();
#endif
}

void QuickStatus::resetQSMonInfo(monitoring_infos &infos, bool resetQSActionCounters, bool resetQSBookkeeping, bool resetBOCCounters){
  // Reset info counters: happen at each time when high level code reads PPC
#ifdef __XMK__
  for(size_t i = 0 ; i < sizeof(infos.slaveBusyCounters)/sizeof(uint32_t) ; i++)
    infos.slaveBusyCounters[i] = 0;
  
  for(int i=0; i<32; i++){
    infos.linkTrigCounters[i]=0;
    infos.linkOccCounters[i]=0;
    infos.totDesyncCounters[i]=0;
    infos.timeoutCounters[i]=0;
    infos.htCounters[i]=0;
  }

  //Reset master busy, now is an OR between 2 readings readings
  infos.masterBusy=0;
  for(int i = 0 ; i < 8 ; i++){
    infos.masterBusyCounters[i] = 0;
  }

  // Reset action counters: only do so upon request
  if(resetQSActionCounters){
    for(int i=0; i<32; i++){
      infos.nCFG_rodBusy[i]=0;
      infos.nCFG_timeout[i]=0;
      infos.nCFG_ht[i]=0;
    }
  }

  // Reset QS bookkeeping: only do so upon request
  if(resetQSBookkeeping){
    infos.fmtLinkEn=0;
    infos.QSDisabled=0;
    infos.waitingForECR=0;
  }

  // Reset BOC counters: only do so upon request
  if(resetBOCCounters){
    for(int i=0; i<32; i++){
      infos.decodingErrorBOC[i]=0;
      infos.frameErrorBOC[i]=0;
      infos.frameCountBOC[i]=0;
      infos.meanOccBOC[i]=0;
    }
    for(int i=0; i<4; i++){
      for(int j=0; j<2; j++){
	infos.SLinkInfo.sLinkCounters[i].Hola[j].Lff=0;
	infos.SLinkInfo.sLinkCounters[i].Hola[j].LDown=0;
      }
    }
  }
#endif
}

void QuickStatus::dumpMasks(){
#ifdef __XMK__
  xil_printf("QSINFO:: QS spy mode %s\n", (m_spy_mode?"ON":"OFF"));
  xil_printf("QSINFO:: Wr Reg ECR  %s\n", (m_writeRegAtECR?"ON":"OFF"));
  xil_printf("QSINFO:: Link Enable 0x%.8X\n", m_infos.fmtLinkEn);
  xil_printf("QSINFO:: QS Disabled 0x%.8X\n", m_infos.QSDisabled);
  xil_printf("QSINFO:: WF ECR      0x%.8X\n", m_infos.waitingForECR);
  xil_printf("QSINFO:: Timeout Err 0x%.8X\n", m_timeoutError);
  xil_printf("QSINFO:: HT      Err 0x%.8X\n", m_htError);
  xil_printf("QSINFO:: Busy    Err 0x%.8X\n", m_rodBusyError);
  xil_printf("QSINFO:: Master busy 0x%.4X\n", m_infos.masterBusy);
#endif
}

void QuickStatus::getBOCCounters(){
#ifdef __XMK__
  for(int ch=0;ch<32;ch++){
    m_infos.decodingErrorBOC[ch]=IblBoc::GetCounter(ch, IblBoc::DecoError);
    m_infos.frameErrorBOC[ch]=IblBoc::GetCounter(ch, IblBoc::FrameError);
    m_infos.frameCountBOC[ch]=IblBoc::GetFrameCount(ch);
    m_infos.meanOccBOC[ch]=IblBoc::GetLinkOccMean(ch);
  }
  IblBoc::GetSlinkCounters(m_infos.SLinkInfo);
#endif
}

//TODO: Implement new master busy counters when ready
void QuickStatus::getRODMasterBusy(){
#ifdef __XMK__
  uint32_t MasterBusyMask=IblRod::MasterBusyMask::read();
  uint32_t MasterBusyCurrentStatus=IblRod::MasterBusyCurrentStatus::read();
  m_infos.masterBusy |=(MasterBusyCurrentStatus & (~MasterBusyMask & 0xFF)) | ((MasterBusyMask >> 8) & 0xFF);
#endif
}

