project(CmdFactory)

add_library(${PROJECT_NAME} INTERFACE
)

#target_include_directories(${PROJECT_NAME}
#    PUBLIC inc
#)

target_link_libraries(${PROJECT_NAME}
    INTERFACE Production
    INTERFACE Production
    INTERFACE Tools
    INTERFACE UnitTests
    )

add_subdirectory(Production)
add_subdirectory(TestCommands)
add_subdirectory(Tools)
add_subdirectory(UnitTests)

