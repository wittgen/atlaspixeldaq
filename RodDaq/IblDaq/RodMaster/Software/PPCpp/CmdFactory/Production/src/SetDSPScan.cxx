/*
 * Changed from SetAsOptoL12 in a generic way in order support different scan types
 * JuanAn 20-Nov-2018
 */

#include "SetDSPScan.h"
#include "ScanBoss.h"

SetDSPScan::SetDSPScan(){
scanType = NONE;
}

void SetDSPScan::execute(){
#ifdef __XMK__
 
 if(RodResMgr::isFei4())ScanBoss::getScan().setDSPScan(scanType);
 else ScanBoss::getScanL12().setDSPScan(scanType);

#endif
}

void SetDSPScan::serialize(uint8_t *out) const{
  uint32_t offset = 0; 
  Serializer<ScanTypeDSP>::serialize(out, offset, scanType);
}
void SetDSPScan::deserialize(const uint8_t *in){
  uint32_t offset = 0; 
  scanType = Serializer<ScanTypeDSP>::deserialize(in, offset);
}
uint32_t SetDSPScan::serializedSize() const{
  return (sizeof(ScanTypeDSP));
}

