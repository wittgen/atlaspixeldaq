#include "WriteRegister.h"
#include "RodResourceManager.h"

template <> WriteRegisterFEI3<Fei3::GlobReg>::WriteRegisterFEI3(): 
	reg( Fei3::GlobReg::NGLOBALREGISTERS ), i_mod(0) {
    setResultPtr();
}

template <> void WriteRegisterFEI3<Fei3::GlobReg>::execute() {
  #ifdef __XMK__
    Fei3Mod* fe = (configType == Physics) ? RodResourceManagerPIX::getInstance().getPhysicsModuleConfigs()
                                          : RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
    for(size_t feIx = 0; feIx < Fei3ModCfg::nFE; ++feIx) {
       std::cout << "Writing module " << i_mod << " FE " << feIx << " reg: " << static_cast<int>(reg) << " value " << values[feIx] << " ...\n";
       fe[i_mod].m_chipCfgs[feIx].writeRegGlobal(reg, values[feIx]);
    }
    RodResourceManagerPIX::updateConnectivity(fe);
    result.success = true;
  #endif
}

template <> WriteRegisterFEI3<Fei3::PixReg>::WriteRegisterFEI3():
	reg( Fei3::PixReg::NPIXELREGS ), i_mod(0) {
    setResultPtr();
}

template <> void WriteRegisterFEI3<Fei3::PixReg>::execute() {
  #ifdef __XMK__
    Fei3Mod* fe = (configType == Physics) ? RodResourceManagerPIX::getInstance().getPhysicsModuleConfigs()
                                          : RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
    for(size_t feIx = 0; feIx < Fei3ModCfg::nFE; ++feIx) {
       Fei3Cfg& currentConfig = static_cast<Fei3ModCfg*>(fe)->m_chipCfgs[feIx];
       for(size_t ixRow = 0; ixRow < Fei3::nPixRow; ++ixRow) {
           for(size_t ixCol = 0; ixCol < Fei3::nPixCol; ++ixCol) {
               switch(reg){
                   case Fei3::PixReg::HitBus:
                       currentConfig.writeBitPixel(Fei3::PixLatch::HitBus, ixRow, ixCol, values[feIx]);
                       break;
                   case Fei3::PixReg::Select:
                       currentConfig.writeBitPixel(Fei3::PixLatch::Select, ixRow, ixCol, values[feIx]);
                       break;
                   case Fei3::PixReg::Enable:
                       currentConfig.writeBitPixel(Fei3::PixLatch::Enable, ixRow, ixCol, values[feIx]);
                       break;
                   case Fei3::PixReg::Preamp:
                       currentConfig.writeBitPixel(Fei3::PixLatch::Preamp, ixRow, ixCol, values[feIx]);
                       break;
                   case Fei3::PixReg::TDAC:
                       currentConfig.writeTDACPixel(ixRow, ixCol, values[feIx]);
                       break;
                   case Fei3::PixReg::FDAC:
                       currentConfig.writeFDACPixel(ixRow, ixCol, values[feIx]);
                       break;
                   default:
                       return; //with result.success == false
               }
           }
       }
    }
    RodResourceManagerPIX::updateConnectivity(fe);
    result.success = true;
  #endif // __XMK__
}


//PIXEL: // HitBus = 0, Select, Enable, TDAC, FDAC, Preamp, NPIXELREGS
// LATCHES: // HitBus = 0, Select, Enable, TDAC0, TDAC1, TDAC2, TDAC3, TDAC4, TDAC5, TDAC6, FDAC0, FDAC1, FDAC2, Preamp, NPIXELREGISTERS
// GLOBAL: // GlobalParity = 0, Latency, SelfTriggerDelay, SelfTriggerWidth, EnableSelfTrigger, EnableHitParity, Select_DO, Select_MonHit, TSI_TSC_Enable, SelectDataPhase, EnableEOEParity, HitBusScaler, MonLeakADC, ARegTrim, EnableARegMeas, ARegMeas, EnableAReg, EnableLVDSReferenceMeas, DRegTrim, EnableDRegMeas, DRegMeas, CapMeasure, EnableCapTest, EnableAnalogOut, TestPixelMUX, EnableVCalMeas, EnableLeakMeas, EnableBufferBoost, EnableCol8, TestDAC_IVDD2, IVDD2, ID, TestDAC_ID, EnableCol7, TestDAC_IP2, IP2, IP, TestDAC_IP, EnableCol6, TestDAC_ITrimTh, ITrimTh, IF, TestDAC_IF, EnableCol5, TestDAC_ITrimIf, ITrimIf, VCal, TestDAC_VCal, EnableCol4, HighInjCapSel, EnableExtInj, TestAnalogRef, EOC_MUX, CEU_Clock, EnableDigInj, EnableCol3, TestDAC_ITH1, ITH1, ITH2, TestDAC_ITH2, EnableCol2, TestDAC_IL, IL, IL2, TestDAC_IL2, EnableCol1, THRMIN, THRDUB, ReadMode, EnableCol0, HitBusEnable, GlobalTDAC, EnableTune, EnableBiasCompensation, EnableIPMonitor, NGLOBALREGISTERS
//
//

template <> WriteRegisterFEI4<Fei4::GlobReg>::WriteRegisterFEI4():
	reg( Fei4::GlobReg::NGLOBALREGS ), i_mod(0) {
    setResultPtr();
}

template <> void WriteRegisterFEI4<Fei4::GlobReg>::execute() {
  #ifdef __XMK__
    Fei4* fe = (configType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs()
                                       : RodResourceManagerIBL::getInstance().getCalibModuleConfigs();
    result.success = fe->writeRegGlobal(reg, value);
    RodResourceManagerIBL::updateConnectivity(fe);
  #endif // __XMK__
}

template <> WriteRegisterFEI4<Fei4::PixReg>::WriteRegisterFEI4():
	reg( Fei4::PixReg::NPIXELREGS ), i_mod(0) {
    setResultPtr();
}

template <> void WriteRegisterFEI4<Fei4::PixReg>::execute() {
  #ifdef __XMK__
    Fei4* fe = (configType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs()
                                       : RodResourceManagerIBL::getInstance().getCalibModuleConfigs();
    for(size_t col = 0; col < Fei4::nPixCol; ++col){
        for(size_t row =0 ; row < Fei4::nPixRow; ++row){
            switch(reg) {
                case Fei4::PixReg::TDAC:
                    fe->TDAC(col/2).setPixel(row+1, col+1, value);
                    break;
                case Fei4::PixReg::FDAC:
                    fe->FDAC(col/2).setPixel(row+1, col+1, value);
                    break;
                default:
                    return; //with result.success == false
            }
        }//row loop
     }//column loop
    RodResourceManagerIBL::updateConnectivity(fe);
    result.success = true;
  #endif // __XMK__
}
