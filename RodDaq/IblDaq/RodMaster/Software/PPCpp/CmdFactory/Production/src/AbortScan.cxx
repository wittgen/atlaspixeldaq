#include "AbortScan.h"
#include "RodResourceManager.h"
#include "ScanBoss.h"
#ifdef __XMK__
#include "sys/socket.h"
#include <pthread.h>

/*
#include <sys/kpthread.h>
// Imported from XilKernel sys_arch.c
struct thread_start_param {
	struct sys_thread *thread;
	void (*function)(void*);
	void *arg;
};

struct sys_thread {
	pthread_t tid;
	struct thread_start_param tp;
};

extern "C" pthread_info_t* pthread_get_info (pthread_t thr);
*/
#endif 


AbortScan::AbortScan() {
  targetScanId = 7777;
  targetExecId = 7777;
}

AbortScan::AbortScan(uint16_t execId, uint32_t scanId) {
  targetScanId = scanId;
  targetExecId = execId;
}

void AbortScan::execute() {
#ifdef __XMK__
 if( RodResMgr::isFei3() ) ScanBoss::getScanL12().abortScan(targetScanId);
 else ScanBoss::getScan().abortScan(targetScanId);
  std::cout << "Aborting scan with scan ID: " << targetScanId << " and exec ID: " << targetExecId << "." << std::endl;
#endif
}

void AbortScan::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint16_t>::serialize(out, offset, targetExecId);
  Serializer<uint32_t>::serialize(out, offset, targetScanId);
}

void AbortScan::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  targetExecId = Serializer<uint16_t>::deserialize(in, offset);
  targetScanId = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t AbortScan::serializedSize() const {
  return sizeof(uint16_t) + sizeof(uint32_t);
}
