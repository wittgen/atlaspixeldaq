#include "ReconfigModules.h"
#include "ReconfigModules.h"
#include "SendModuleConfig.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "DataTakingTools.h"
#include "IblConnectivity.h"
#include "BarrelConnectivity.h"
#include "IblBocLink.h"
#include "Fei4Proxy.h"
#endif

#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

/* static */ uint32_t ReconfigModules::checkFMTLinks(){
#ifdef __XMK__
  return (IblRod::SlaveA::FmtLinkEnable::read() & 0xFFFF) | ((IblRod::SlaveB::FmtLinkEnable::read() & 0xFFFF) << 16);
#endif
  return 0;
}

/* static */ void ReconfigModules::disableFMTLinks(uint32_t mask, uint32_t waitingForECR){
#ifdef __XMK__
  uint32_t formatterStatus = ReconfigModules::checkFMTLinks() | waitingForECR;
  uint32_t modified_mask=mask&formatterStatus;

  if(modified_mask==0){
    xil_printf("FMT links 0x%.8X are already off\n", mask);
    return;
  }
  else{
    xil_printf("FMT links 0x%.8X will be disabled \n", modified_mask);
  }

  SerialPort* sPort = NULL;
  if( RodResMgr::isFei3() ) sPort = RodResourceManagerPIX::getSerialPort();
  else sPort = RodResourceManagerIBL::getSerialPort();

  if(sPort==NULL){
    xil_printf("ERROR: cannot get serial port!");
    return;
  }

  uint32_t TxMask = sPort->getTxMask();
  if( RodResMgr::isFei3() ) TxMask = BarrelConnectivity::getTxMaskFromRxMask(formatterStatus & (~modified_mask));
  else TxMask = IblConnectivity::getTxMaskFromRxMask(formatterStatus & (~modified_mask));

  IblRod::MasterBusyMask::set(0x0100); // force is MSbyte, mask is LSbyte
  usleep(2000); // 2ms
  sPort->setTxMask(TxMask);
  DataTakingTools::disableFmtLink(modified_mask);
  IblRod::MasterBusyMask::reset(0x0100); // force is MSbyte, mask is LSbyte

  if(waitingForECR!=0) ReconfigModules::enableFMTLinks((formatterStatus & (~modified_mask)), (waitingForECR & (~modified_mask)), true);
#endif
}

/* static */ void ReconfigModules::enableFMTLinks(uint32_t mask, uint32_t waitingForECR, bool writeRegAtECR){
#ifdef __XMK__
  uint32_t formatterStatus = ReconfigModules::checkFMTLinks() | waitingForECR;
  uint32_t modified_mask=mask|formatterStatus;

  xil_printf("FMT links 0x%.8X will be enabled \n", modified_mask);
 
  SerialPort* sPort = NULL;
  if( RodResMgr::isFei3() ) sPort = RodResourceManagerPIX::getSerialPort();
  else sPort = RodResourceManagerIBL::getSerialPort();

  if(sPort==NULL){
    xil_printf("ERROR: cannot get serial port!");
    return;
  }

  uint32_t TxMask = sPort->getTxMask();
  if( RodResMgr::isFei3() ) TxMask = BarrelConnectivity::getTxMaskFromRxMask(modified_mask);
  else TxMask = IblConnectivity::getTxMaskFromRxMask(modified_mask);

  IblRod::MasterBusyMask::set(0x0100); // force is MSbyte, mask is LSbyte
  sPort->setTxMask(TxMask);
  usleep(1000); // 1ms
  IblRod::MasterBusyMask::reset(0x0100); // force is MSbyte, mask is LSbyte

  if(writeRegAtECR) DataTakingTools::enableFmtLinkAtECR(modified_mask);
  else DataTakingTools::enableFmtLink(modified_mask);
#endif
}

ReconfigModules::ReconfigModules() : m_ConfigType(Physics),  m_rxMask(0x0), m_waitingForECR(0x0), m_forcePreAmpsStatus(false), m_preAmpsOn(false), m_sleepDelay(100000), m_notReconfigured(0), m_writeRegAtECR(false)
{
  setResultPtr();
}

/* static */ uint32_t ReconfigModules::augmentMask(uint32_t mask)
{
#ifdef __XMK__
  // Canonical tranformation in the case of Fei3
  if( RodResMgr::isFei3() ) return mask;

  // if only one fe and not one fe module is set in the mask enable also the second fe
  for (uint32_t i_fe = 0; i_fe < RodResourceManagerIBL::getNModuleConfigs(); i_fe+=2) {
    if ((mask & (1 << i_fe)) || (mask & (1 << (i_fe+1)))) {
      mask |= (1<<i_fe);
      mask |= (1<<(i_fe+1));
    }
  }
#endif
  return mask;
}

// Returns the mask of reconfigured modules (in terms of RX)
uint32_t ReconfigModules::reconfigure(uint32_t reconfigMask) {
  uint32_t notReconfigured = reconfigMask;
  //sleep(10000);
#ifdef __XMK__
  bool sendConfigAtECR=false;
  if(!m_forcePreAmpsStatus){
    // preAmpsOn is now read directly from module configuration, unless user want to force it to a certainty state
    if(RodResMgr::isFei3()){
      Fei3Mod* mods = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
      for(size_t iMod = 0 ; iMod < RodResourceManagerPIX::getNModuleConfigs() ; ++iMod) {
	// Check that the bit at position iMod IS SET in the m_rxMask, otherwise continue
	// Note: in the cases where both links are used in the module, only the first one is releveant (the BOC addresses the merging of the 2nd link)
	if( (reconfigMask & (0x1 << mods[iMod].m_rxCh)) == 0  || mods[iMod].revision == 0 ) continue;
	m_preAmpsOn=mods[iMod].isPreAmpsOn();
	xil_printf("Pre-amps status determined to be %d from module number %d\n", m_preAmpsOn, iMod);
	break;
      }
    }
    else if(RodResMgr::isFei4()){
      Fei4* fe = RodResourceManagerIBL::getInstance().getCalibModuleConfigs();
      for (uint32_t i_fe = 0; i_fe < RodResourceManagerIBL::getNModuleConfigs(); i_fe++) {
	if ((1<<i_fe) & reconfigMask) {
	  m_preAmpsOn=fe[i_fe].isPreAmpsOn();
	  xil_printf("Pre-amps status determined to be %d from module number %d\n", m_preAmpsOn, i_fe);
	  break;
	}
      }
    }
  }
  SendModuleConfig sendModCfg;
  sendModCfg.setRxMask(reconfigMask);
  if(m_preAmpsOn) sendModCfg.turnOnPreAmps();
  else sendModCfg.turnOffPreAmps();
  if(RodResMgr::isFei4())sendModCfg.sendConfigAtECR(sendConfigAtECR);//Keep here to not forget the logic...
  sendModCfg.setSendAll(true);
  sendModCfg.readBackSR(false);
  sendModCfg.readBackGlobal(false);
  sendModCfg.setSleepDelay(m_sleepDelay);
  sendModCfg.setCalibConfig();
  sendModCfg.execute();

  // Need to put the MCC back to data taking mode
  if(RodResMgr::isFei3()){
    Fei3Mod* mods = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
    for(size_t iMod = 0 ; iMod < RodResourceManagerPIX::getNModuleConfigs() ; ++iMod) {
      // Check that the bit at position iMod IS SET in the reconfigMask, otherwise continue
      // Note: in the cases where both links are used in the module, only the first one is releveant (the BOC addresses the merging of the 2nd link)
      if( (reconfigMask & (0x1 << mods[iMod].m_rxCh)) == 0  || mods[iMod].revision == 0 ) continue;
      mods[iMod].mccEnDataTake();
    }
  }
  else if(RodResMgr::isFei4()){
    Fei4Proxy fei4 = Fei4Proxy(reconfigMask);
    fei4.setRunMode(true);
  }
  // For now must assume that reconfiguration happened: no way to report from SendModuleConfig
  if(RodResMgr::isFei3()) notReconfigured = sendModCfg.getNotConfiguredRxMask(); // For FE-I3, depend on read back check (which is not happening yet)
  else if(RodResMgr::isFei4()) notReconfigured = (~sendModCfg.result.enabledMask)&reconfigMask; // For FE-I4, just check whether the Rx channels are enabled or not in the BOC
#else
  notReconfigured=0x0;
#endif
  return notReconfigured;
}

void ReconfigModules::execute() {
#ifdef LL_DEBUG
  std::cout<<"in LL_DEBUG execute of command: "<<__PRETTY_FUNCTION__<<std::endl;
#endif
  result.success = false;

#ifdef __XMK__

  uint32_t reconfigMask = ReconfigModules::augmentMask(m_rxMask);
  //std::cout << HEXF(8, m_rxMask) << " was selected to reconfigure fes" << std::endl;
  //std::cout << HEXF(8, reconfigMask) << " will be used to reconfigure modules" << std::endl;
  xil_printf("Mask 0x%08x selected to reconfigure fes, Mask 0x%08x will we used to reconfigure modules\n",m_rxMask,reconfigMask);
  // read status of both formatter links
  uint32_t statusSlaveA = IblRod::SlaveA::FmtLinkEnable::read();
  uint32_t statusSlaveB = IblRod::SlaveB::FmtLinkEnable::read();

  uint32_t formatterStatus = statusSlaveA | (statusSlaveB << 16);
  //std::cout << "Formatter status: " <<  HEXF(8, formatterStatus) << std::endl;
  //std::cout << "Augmented formatter status: " << HEXF(8, ReconfigModules::augmentMask(formatterStatus)) << std::endl;
  xil_printf("Formatter status 0x%08x, augmented formatter status Mask 0x%08x\n",formatterStatus,ReconfigModules::augmentMask(formatterStatus));
  // Sanity check: the reconfiguration mask must be subset of formatter link
  if( (reconfigMask & formatterStatus) != reconfigMask ){
    if( (reconfigMask & formatterStatus) == 0){
      xil_printf("Modules requested to be reconfigured (0x%.8X) are not corresponding to any active FMT links (0x%.8X). Exiting without taking any actions. \n", reconfigMask, formatterStatus);
      result.success=true;
      m_rxMask=0;
      m_notReconfigured=0;
      return;
    }
    else{
      std::cout<<"WARNING: requested reconfiguration mask " << HEXF(8, reconfigMask)
	       <<" is not subset of the formatter status " << HEXF(8, formatterStatus)
	       <<". Will reduce the reconfiguration mask to " << HEXF(8, reconfigMask & formatterStatus)
	       <<std::endl;
      reconfigMask &= formatterStatus;
    }
  }
  m_rxMask = reconfigMask;	// Update the Rx mask in the class

  ReconfigModules::disableFMTLinks(reconfigMask); // No need to take care of FMT links waiting for ECR yet

  m_notReconfigured = this->reconfigure(reconfigMask);

  if( m_notReconfigured )
    std::cout << "WARNING: some links couldn't be reconfigured: " << HEXF(8, m_notReconfigured) 
	      << ". The formatter status is " << HEXF(8, formatterStatus)
	      << std::endl;
  // enable the formatter link after reconfigure the module
  // use the status from before (dead modules should not be activated)
  // disable the modules which are reconfigured only if the configuration was successful
  uint32_t endStatus = formatterStatus & (~m_notReconfigured);

  ReconfigModules::enableFMTLinks(endStatus, m_waitingForECR, m_writeRegAtECR);

  //std::cout << "Reconfiguration works good? " << std::boolalpha << (!m_notReconfigured) << std::endl;
  //std::cout << "Mask which will be set in the end " << HEXF(8, endStatus) << std::endl;  
  xil_printf("Mask that will be set at the end 0x%08x\n",endStatus);
  
  // check whether the formatter mask in the beginning is the same as in the end
  result.success = (formatterStatus == endStatus);
#endif
}

void ReconfigModules::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,m_rxMask);
	Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
	Serializer<bool>::serialize(out, offset, m_preAmpsOn);
	Serializer<bool>::serialize(out, offset, m_forcePreAmpsStatus);
	Serializer<uint32_t>::serialize(out, offset, m_sleepDelay);
}

void ReconfigModules::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
	m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
	m_preAmpsOn = Serializer<bool>::deserialize(in, offset);
	m_forcePreAmpsStatus = Serializer<bool>::deserialize(in, offset);
	m_sleepDelay = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t ReconfigModules::serializedSize() const
{
  return (sizeof(uint32_t)*2 + sizeof(ConfigType) + sizeof(bool)*2);
}
