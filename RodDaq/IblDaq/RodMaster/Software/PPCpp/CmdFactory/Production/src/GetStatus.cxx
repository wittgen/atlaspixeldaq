#include "GetStatus.h"

#ifdef __XMK__

#endif

GetStatus::GetStatus(uint32_t usingFei3){
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
  setResultPtr();
  targetUsingFei3 = usingFei3;
}

void GetStatus::execute() {
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << m_ExecId << std::endl;
  // Modify the result member as appropriate                                                                                                                                                          
  std::cout << "Result address = " << &result << std::endl;
  std::cout << "Pointer to result: " << m_ResultPtr << std::endl;
#endif
  if(targetUsingFei3)
#ifdef __DO_GET_STATUS__
  result.m_status = ScanBoss::getScanL12().getStatus()
#endif
  ;else
#ifdef __DO_IBL_GET_STATUS__
  result.m_status = ScanBoss::getScan().getStatus()
#endif
  ;
#ifdef LL_DEBUG
  std::cout << "Scan status: " << result.m_status.state <<", with error state: "<< result.m_status.errorState<<std::endl;
#endif
}

void GetStatus::serialize(uint8_t *out) const{
#ifdef LL_DEBUG
std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, targetUsingFei3);
}
void GetStatus::deserialize(const uint8_t *in){
#ifdef LL_DEBUG
std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
  uint32_t offset = 0;
  targetUsingFei3 = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t GetStatus::serializedSize() const{
#ifdef LL_DEBUG
std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
return sizeof(uint32_t);
}

void ReturnStatus::serialize(uint8_t *out) const
{
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, m_status.state);
        Serializer<uint32_t>::serialize(out, offset, m_status.errorState);
        Serializer<int32_t>::serialize(out, offset, m_status.mask);
        Serializer<int32_t>::serialize(out, offset, m_status.par);
        Serializer<int32_t>::serialize(out, offset, m_status.par1);
        Serializer<int32_t>::serialize(out, offset, m_status.par2);
        Serializer<int8_t>::serialize(out, offset, m_status.dc);
        Serializer<int32_t>::serialize(out, offset, m_status.trig);

}
void ReturnStatus::deserialize(const uint8_t *in)
{
#ifdef LL_DEBUG
  std::cout << "Status--mask: " << (int) m_status.mask << " par: " << (int) m_status.par << " par1: " << (int) m_status.par << " par2: " << (int) m_status.par2 << " dc: " << (int) m_status.dc << " trig: " << (int) m_status.trig << " error state = "<<(int) m_status.errorState <<std::endl;
#endif
        uint32_t offset = 0;
        m_status.state = (ScanState)Serializer<uint32_t>::deserialize(in, offset);
        m_status.errorState = (PPCErrorType)Serializer<uint32_t>::deserialize(in, offset);
        m_status.mask = Serializer<int32_t>::deserialize(in, offset);
        m_status.par = Serializer<int32_t>::deserialize(in, offset);
        m_status.par1 = Serializer<int32_t>::deserialize(in, offset);
        m_status.par2 = Serializer<int32_t>::deserialize(in, offset);
        m_status.dc = Serializer<int8_t>::deserialize(in, offset);
        m_status.trig = Serializer<int32_t>::deserialize(in, offset);
}
uint32_t ReturnStatus::serializedSize() const
{
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
        return (2*sizeof(uint32_t) + 5*sizeof(int32_t) + sizeof(int8_t));
}
