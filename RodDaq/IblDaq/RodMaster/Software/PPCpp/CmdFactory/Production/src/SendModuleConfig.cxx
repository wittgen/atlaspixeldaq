#include "SendModuleConfig.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblConnectivity.h"
#include "BarrelConnectivity.h"
#include "IblBocLink.h"
#include "IblBoc.h"
#include "Fei4Proxy.h"
#include "Fei3ModProxy.h"
#include "RecoAtECR.h"
#endif
#include <mutex>

#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

SendModuleConfig::SendModuleConfig() {
  //defaults!
  setResultPtr();
  m_ConfigType = Physics;
  verbose = false;
  m_sendAll = true;
  m_modMask = 0xffffffff; // feMask for IBL
  m_preAmpsOn = false;
  m_readBackSR = false;
  m_sleepDelay = 0; // how long to sleep between sending each FE config in us
  m_maxGlobalThreshold = false; // Low-level protection
  m_sendConfigAtECR = false; //default
  m_readBackGlobal = true; //default Not serialized
  m_maskPixelEna = 0x0;
}

void SendModuleConfig::execute() {
#ifdef LL_DEBUG
  std::cout<<"in LL_DEBUG execute of command: "<<__PRETTY_FUNCTION__<<std::endl;
#endif
#ifdef __XMK__
  result.success = true;
  if (RodResMgr::isFei3()) sendModuleConfigFei3();
  else sendModuleConfigFei4();
#endif
}

void SendModuleConfig::setRxMask(uint32_t rxMask) {
#ifdef __XMK__
  if(RodResMgr::isFei3()) {
    // Convert the Rx mask to module mask (they are not the same for FEI3)
    m_modMask=BarrelConnectivity::getModMaskFromRxMask(rxMask);
    //std::cout << __PRETTY_FUNCTION__ << ": for Rx mask = "<< HEXF( 8, rxMask) <<", modMask = " << HEXF( 8, m_modMask ) << std::endl;
    xil_printf("setRxMask: for Rx mask 0x%08x modMask 0x%08x\n",rxMask,m_modMask);
  }
  else m_modMask=rxMask;
#else
  // TMEMPORARY: to be sorted out
  setModMask(rxMask);
#endif
}

void SendModuleConfig::sendModuleConfigFei3() {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__

// this line is currently a lie
//  std::cout << (m_sendAll ? ("Sending MCC and FE-I3 configurations") : ("Sending MCC configurations only")) << std::endl;

  RodResourceManagerPIX &rm = RodResourceManagerPIX::getInstance();

  IblBocLink *iblBocLink = RodResourceManagerPIX::getIblBocLink();
  uint32_t rxMask = BarrelConnectivity::getRxMaskFromModMask(m_modMask);
  iblBocLink->enableChannels(rxMask );
  
  // Mutex RecoAtECR
  //xil_printf("SendModConfig locking RecoAtECR thread\n");
  // OKUPDATE Bad style c++ locking
  std::lock_guard<std::mutex>(RecoAtECR::mtx_cfg);

  uint32_t TxMask = BarrelConnectivity::getTxMaskFromRxMask(rxMask);
  //I use this BOC register to stop the ECR reconfiguration thread from sending the configuration at ECR
  iblBocLink->ECRVetoTxChannels(TxMask,true);

  result.enabledMask = m_modMask;

  int maxModules = rm.getNModuleConfigs();

  Fei3Mod *fes = rm.getCalibModuleConfigs();
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
  //std::cout << "Module mask m_modMask = " << HEXF(8, m_modMask) << std::endl;
  xil_printf("Module mask m_modMask = 0x%08x\n",m_modMask);
  
  if(m_maxGlobalThreshold) {
    //std::cout << "WARNING -- setting global threshold to maximum " << std::endl;
    xil_printf("WARNING -- setting global threshold to maximum\n");
  }

  
  if(m_maskPixelEna)std::cout<<"Masking Pixels in Stand-by with mask 0x"<<std::hex<<m_maskPixelEna<<std::dec <<std::endl;

  for (int i = 0; i < maxModules; i++)
    if ((m_modMask & (1 << i))){
     //Global MCC reset and FE soft reset before sending the configuration
     fes[i].mccGlobalResetMCC();
     fes[i].mccGlobalResetFE(0x4);
     //std::cout<<"Resetting MCC and FE (soft) for Module: "<<fes[i].idStr<<std::endl;
     }

  // Get readbackOKMask for FEI3
  uint32_t success = 0;

  for (int i = 0; i < maxModules; i++) {
    //std::cout << "Processing " << fes[i].idStr << ": ";
    //std::cout << i << " " << fes[i].revision << " " << HEXF(8, 0x1 << fes[i].getRxCh()) << std::endl;
    xil_printf("Processing %s \n",fes[i].idStr);
    xil_printf("%d  %d 0x%08x\n",i,fes[i].revision,0x1<<fes[i].getRxCh());
    if ((m_modMask & (1 << i)) && fes[i].revision != 0) {
      //std::cout << "Configuring...with preAmps " << (m_preAmpsOn ? "ON" : "OFF") << " ... and " << (m_sendAll ? "" : "NOT") << " sending the pixel config" << std::endl;
      xil_printf("Configuring...with preAmps %s ... and %s sending the pixel cfg \n",(m_preAmpsOn ? "ON" : "OFF"),(m_sendAll ? "" : "NOT"));
      
      bool GRCheck=true;
      fes[i].sendMCCConfig();

      // Global bit for all FEs in a module
      fes[i].setPreAmpsOn(m_preAmpsOn);
      
      if(!m_preAmpsOn){
        if(m_maskPixelEna  & (1 << i) )
        fes[i].maskPixelEna(true);
        else
         fes[i].maskPixelEna(false);
      }

      for (int j = 0; j < 16; j++) {
        if(m_maxGlobalThreshold) fes[i].writeGlobalReg(j, Fei3::GlobReg::GlobalTDAC, 0x1F);
        // Comment these out to dump the module configurations (condensed and Human Readable (HR)
        //fes[i].dumpGlobalConfig(j);
        //fes[i].dumpGlobalConfigHR(j);

        if(m_readBackGlobal)
           if(!fes[i].checkConfig(j))GRCheck = false;

        fes[i].sendGlobalConfig(j);

        if(m_sendAll) fes[i].sendPixelConfig(j);
      }
      if(GRCheck) success |= (1 << i);
    }
  }
  result.readBackOKMask=success; 
  if (result.readBackOKMask != m_modMask) {
    //std::cout<<"ERROR: Not all the global configs read back properly. readBackOKMask = "
    //	     <<HEXF(8, result.readBackOKMask)<<", modMask = "<<HEXF(8, m_modMask)
    //	     <<std::endl;
    xil_printf("ERROR: Not all the global cfgs read back properly\n");
    xil_printf("readBackOKMask=0x%08x  modMask=0x%08x\n",result.readBackOKMask,m_modMask);
    result.success = false;
  }
  else 
    {
      //std::cout<<"INFO: The global configs for channels "<< HEXF(8, m_modMask) <<" all read back OK"<<std::endl;
      xil_printf("INFO: The global configs for channels 0x%08x read back OK\n",m_modMask);
    }

  if(m_preAmpsOn)
  iblBocLink->ECRVetoTxChannels(TxMask,false);

  //xil_printf("SendModConfig unlocking RecoAtECR thread\n");

#endif
}

void SendModuleConfig::sendModuleConfigFei4() {
#ifdef __XMK__

  //std::cout<< ( m_sendAll ? ("Sending global and pixel registers") : ("Sending global registers only")) << std::endl;
  xil_printf("%s \n",( m_sendAll ? ("Sending global and pixel registers") : ("Sending global registers only")));
  

  // Mutex RecoAtECR
  //xil_printf("SendModConfig locking RecoAtECR thread\n");
  // OKUPDATE Bad style c++ locking
  std::lock_guard<std::mutex>(RecoAtECR::mtx_cfg);

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  
  uint32_t TxMask = IblConnectivity::getTxMaskFromRxMask(m_modMask);

  iblBocLink->enableTxChannels(TxMask);

  //I use this BOC register to stop the ECR reconfiguration thread from sending the configuration at ECR
  iblBocLink->ECRVetoTxChannels(TxMask,true);
  
  SerialPort* sPort = NULL;
  sPort = RodResourceManagerIBL::getSerialPort();

  //reading back the actual value of the serial port in the Fw; needed to restore the serialPort in the ROD to the value previous to the configuration
  uint32_t sportMask = sPort->readsPort(); 

  if(sPort==NULL){
    xil_printf("ERROR: cannot get serial port!\n");
    return;
  }

  // Disabling the serial port on the modules that are going to be configured but avoid turning on the serial Port in the not existing lines
  sPort->setTxMask( (sportMask) & (~TxMask)); 

  Fei4Proxy fes = Fei4Proxy(m_modMask);

if(m_readBackGlobal){
  #define DO_READBACK_BEFORE_SENDING
  #ifdef DO_READBACK_BEFORE_SENDING
  fes.setRunMode(false);
  iblBocLink->enableRxChannels(m_modMask);
  uint32_t testmask = iblBocLink->getEnChMask();
  if(testmask != m_modMask)
    {
      //std::cout<<"ERROR: I cannot enable mod_mask = "<<HEXF(8, m_modMask)<<" for readback before sending, I just can enable "<<HEXF(8, testmask)<<std::endl;
      xil_printf("ERROR: I cannot enable mod_mask = 0x%08x for readback before sending, I just can enable 0x%08x\n",m_modMask,testmask);
    }
  Fei4Proxy fesReadback = Fei4Proxy(testmask);
  result.readBackBeforeSending = fesReadback.checkGlobalConfig(m_preAmpsOn,false,m_GRreadback);
  if(result.readBackBeforeSending != m_modMask)
    {
      //std::cout<<"Warning mistmatch of the current configuration and the one that is going to be send: to send 0x"<<std::hex<<m_modMask<<" while readback mask 0x"<<result.readBackBeforeSending<<std::dec<<std::endl;
      xil_printf("Warning mismatch of the current configuration and the one that is going to be send: to send 0x%08x while readback mask 0x%08x\n",m_modMask,result.readBackBeforeSending);
    }
  else 
    {
      //std::cout<<"Readback before sending OK"<<std::endl;
      xil_printf("Readback before sending OK\n");
    }
  #endif
}

  if (m_preAmpsOn) fes.sendGlobalConfigAmpsOn(!m_readBackSR, m_sleepDelay);
  else fes.sendGlobalConfigAmpsOff(!m_readBackSR, m_sleepDelay);
  
  iblBocLink->enableRxChannels(m_modMask);
  if (m_readBackSR) {
    fes.readBackSR(); // read back service records
    iblBocLink->enableRxChannels(m_modMask); // try again to enable in case it failed the first time because not all service records were clear
  }

  uint32_t enabledMask = iblBocLink->getEnChMask();
  result.enabledMask = enabledMask;

  fes.setFeMask(enabledMask);
  if (enabledMask != m_modMask) result.success = false;
  
  //std::cout<<"Enabled channel mask = 0x"<<HEXF(8, enabledMask)<<std::endl;
  xil_printf("Enabled channel mask = 0x%08x\n",enabledMask);

    if(m_readBackGlobal){
      result.readBackOKMask = fes.checkGlobalConfig(m_preAmpsOn,false,m_GRreadback); // check status of global config 
      if (result.readBackOKMask != m_modMask) {
       std::cout<<"ERROR: Not all the global configs read back properly. readBackOKMask = "
	     <<HEXF(8, result.readBackOKMask)<<", modMask = "<<HEXF(8, m_modMask)
	     <<std::endl;
       result.success = false;
      }
      else std::cout<<"INFO: The global configs for channels "<< HEXF(8, m_modMask) <<" all read back OK"<<std::endl;
    }
 
  if (m_sendAll) {
    //std::cout<<"Sending Pixel configs"<<std::endl;
    xil_printf("Sending Pixel configs\n");
    fes.sendPixelConfig();
  }
  
  sPort->setTxMask(sportMask); // re-enabling the serial port on the modules that have just been  re-configured

  //Remove the Veto At ECR
  if(m_preAmpsOn)
    iblBocLink->ECRVetoTxChannels(TxMask,false);
  
  // after configuring, request that the boc re-sample the rx phase
  iblBocLink->resamplePhase(enabledMask);

  //xil_printf("SendModConfig unlocking RecoAtECR thread\n");
#else
  //Todo: emulator
#endif
}

uint32_t SendModuleConfig::getNotConfiguredRxMask(){
#ifdef __XMK__
  uint32_t notConfiguredModMask = (~result.readBackOKMask) & m_modMask;
  uint32_t notConfiguredRxMask = 0x0;
  if(RodResMgr::isFei3()){
    // Need to convert the module mask into Rx mask
    RodResourceManagerPIX &rm = RodResourceManagerPIX::getInstance();
    Fei3Mod *fes = rm.getCalibModuleConfigs();
    int maxModules = rm.getNModuleConfigs();

    for (int i = 0; i < maxModules; i++) {
      uint32_t rxCh=fes[i].getRxCh();
      // std::cout << "Module number: " << i << ", rxCh=" << rxCh << std::endl;
      if( (notConfiguredModMask & (0x1 << i)) && fes[i].revision != 0 ) {
        // std::cout << "Module is not configured properly!!!" << std::endl;
        notConfiguredRxMask |= (0x1 << rxCh);
      }
    }
    //std::cout << __PRETTY_FUNCTION__ << ": not configured Rx mask =" << HEXF( 8, notConfiguredRxMask ) << std::endl;
  }
  else notConfiguredRxMask = notConfiguredModMask;
  return notConfiguredRxMask;
#endif
  return 0x0; //To suppress warning about missing return if __XMK__ is not set
}

void SendModuleConfig::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,m_modMask); 
	Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
	Serializer<bool>::serialize(out,offset,m_sendAll); 
	Serializer<bool>::serialize(out,offset,m_preAmpsOn); 
	Serializer<bool>::serialize(out,offset,m_readBackSR); 
	Serializer<uint32_t>::serialize(out,offset,m_sleepDelay); 
        Serializer<bool>::serialize(out, offset, m_maxGlobalThreshold);
        Serializer<bool>::serialize(out, offset,m_sendConfigAtECR);
        Serializer<uint32_t>::serialize(out, offset, m_maskPixelEna);
}

void SendModuleConfig::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
  m_modMask = Serializer<uint32_t>::deserialize(in, offset);
  m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
  m_sendAll = Serializer<bool>::deserialize(in, offset);
  m_preAmpsOn = Serializer<bool>::deserialize(in, offset);
  m_readBackSR = Serializer<bool>::deserialize(in, offset);
  m_sleepDelay = Serializer<uint32_t>::deserialize(in, offset);
  m_maxGlobalThreshold = Serializer<bool>::deserialize(in, offset);
  m_sendConfigAtECR = Serializer<bool>::deserialize(in, offset);
  m_maskPixelEna = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t SendModuleConfig::serializedSize() const
{
  return (3*sizeof(uint32_t) + sizeof(ConfigType) + 5*sizeof(bool) );
}

void SentModuleConfig::serialize(uint8_t *out) const {
	  uint32_t offset = 0;
	  Serializer<bool>::serialize(out, offset, success);
	  Serializer<uint32_t>::serialize(out, offset, enabledMask);
	  Serializer<uint32_t>::serialize(out, offset, readBackOKMask);
	  Serializer<uint32_t>::serialize(out, offset, readBackBeforeSending);

}

void SentModuleConfig::deserialize(const uint8_t *in) {
	  uint32_t offset = 0;
	  success = Serializer<bool>::deserialize(in, offset);
	  enabledMask = Serializer<uint32_t>::deserialize(in, offset);
	  readBackOKMask = Serializer<uint32_t>::deserialize(in, offset);
	  readBackBeforeSending = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t SentModuleConfig::serializedSize() const {
  return (sizeof(bool)+3*sizeof(uint32_t));
}

