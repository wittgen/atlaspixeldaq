#include "SendConfig.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#endif

SendConfig::SendConfig() : m_ConfigType(Physics),  i_mod(16)
{
  setResultPtr();
}

void SendConfig::execute() {

// lj: remove this whole command?

#ifdef LL_DEBUG
  std::cout<<"in LL_DEBUG execute of command: "<<__PRETTY_FUNCTION__<<std::endl;
#endif
  result.success = false;

#ifdef __XMK__

 IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink(); 

 if( !iblBocLink->enableChannels( (0x1 << i_mod)) ) {
   //todo check this is the correct behavior
   std::cout<<"WARNING: could not enable requested FE. Will return"<<std::endl;
   return;
 }

 Fei4* fe = ((m_ConfigType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs() 
	     : RodResourceManagerIBL::getInstance().getCalibModuleConfigs());
 
  //send the config! 
 //   (fe[i_mod]).sendFullConfig();
 (fe[i_mod]).sendGlobalConfigAmpsOn(); 

//todo add real success or failure?
  result.success = true;
#else
  //Todo: emulator
#endif
}

void SendConfig::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,i_mod); 
	Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
}

void SendConfig::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	i_mod = Serializer<uint32_t>::deserialize(in, offset);
	m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
}

uint32_t SendConfig::serializedSize() const
{
  return (sizeof(uint32_t) + sizeof(ConfigType));
}
