/*
 * * JuanAn Garcia
 * Date: 20-Nov-2018
 *
 * Get DSP results back
 *
 */
 
#include "GetDSPResults.h"
#include "ScanBoss.h"

GetDSPResults::GetDSPResults(){

  setResultPtr();
  
  rxCh=-1;

}  

void GetDSPResults::execute() {
#ifdef __XMK__
  std::cout << "Getting DSP results" << std::endl;
  std::cout << __PRETTY_FUNCTION__ << std::endl;

  if(RodResMgr::isFei4())
  result.dspResults = ScanBoss::getScan().getDSPResults(rxCh);
  else
  result.dspResults = ScanBoss::getScanL12().getDSPResults(rxCh);


#endif
}

void GetDSPResults::serialize(uint8_t *out) const{
  uint32_t offset = 0;
  
  Serializer<uint8_t>::serialize(out, offset, rxCh);
  
}
void GetDSPResults::deserialize(const uint8_t *in){
  uint32_t offset = 0;
  
  rxCh = Serializer<uint8_t>::deserialize(in, offset); 
}

uint32_t GetDSPResults::serializedSize() const{
  
  return sizeof(uint8_t);

}

void DSPScanResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer< std::vector< std::vector< uint32_t > > >::serialize(out, offset, dspResults); 
      
}
void DSPScanResults::deserialize(const uint8_t *in)
{

	uint32_t offset = 0;
	dspResults =  Serializer< std::vector< std::vector< uint32_t > > >::deserialize(in, offset);

}

uint32_t DSPScanResults::serializedSize() const
{
        uint32_t vectorSize = Serializer<std::vector< std::vector< uint32_t > > >::size(dspResults);
        return (vectorSize ); 
}

