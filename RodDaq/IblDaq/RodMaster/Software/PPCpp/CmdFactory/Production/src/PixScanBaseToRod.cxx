#include "PixScanBaseToRod.h"
#include "ScanBoss.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#endif

PixScanBaseToRod::PixScanBaseToRod(){
  setResultPtr();
}

void PixScanBaseToRod::execute() {
  result.success = false;
#ifdef __XMK__
  ScanBoss::setPixScanBase(m_scanBase);
#endif
  result.success = true;
}

void PixScanBaseToRod::serialize(uint8_t *out) const{
  uint32_t offset = 0;
  Serializer<SerializablePixScanBase>::serialize(out,offset,m_scanBase);
}

void PixScanBaseToRod::deserialize(const uint8_t *in){
  uint32_t offset = 0;
  Serializer<SerializablePixScanBase>::deserialize(in,offset,m_scanBase);
}

uint32_t PixScanBaseToRod::serializedSize() const{
  return(m_scanBase.serializedSize());
}

void WrotePixScanBase::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<bool>::serialize(out, offset, success);
}

void WrotePixScanBase::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  success = Serializer<bool>::deserialize(in, offset);
}

uint32_t WrotePixScanBase::serializedSize() const {
  return sizeof(bool);
}
