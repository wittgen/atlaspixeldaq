/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "CountOccupancy.h"
#include "RodResourceManager.h"
#include "ScanBoss.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#include "IblRodSlave.h"
#include <arpa/inet.h>
#include <chrono>
#include <thread>
#endif


CountOccupancy::CountOccupancy(){ 
  setResultPtr(); 
  talkToFitFarm= false;  // attempt connection to fit farm?
  whichSlave = 1;
  for (int i = 0; i<4; i++) {
    fitPorts[i] = 0;
    fitIps[i][0] = 0; fitIps[i][1] = 0;
    fitIps[i][2] = 0; fitIps[i][3] = 0;
  }
  sleepTime = 1000;
  testInj = false;

  feMin = 0;
  feMax = 16;
  rowMin = 0;
  rowMax = 56; // N_ROW6


  // some "random" numbers here for the injection
  row = 10;
  col = 30;
  address = 0;
  chip = 3;

  nTriggers = 0;
}

bool CountOccupancy::startSlavesAndFitfarm() {
  bool success = true;
  #ifdef __XMK__
  std::cout<<"about to send set id command for slave "<<(int)whichSlave<<std::endl;
  success &= slave.setId(whichSlave);
  std::cout<< (success ? "set slave id" : "failed to set slave id") <<std::endl;
  success &= slave.setVerbose(whichSlave,1);
  std::cout<< (success ? "set slave verbose" : "failed to set slave verbose") <<std::endl;

  IblSlvNetCfg netCfgOut;

  IblRodSlave::getDefaultNetConfig(whichSlave, netCfgOut);

  uint32_t tPA = whichSlave ? fitPorts[2] : fitPorts[0];
  uint32_t tPB = whichSlave ? fitPorts[3] : fitPorts[1];

  std::cout<<std::dec<<"Fit farm target port A, B = "<<(int)tPA<<", "<<(int)tPB<<std::endl; 

  netCfgOut.targetPortA = htonl(tPA);
  netCfgOut.targetPortB = htonl(tPB);

  char buffer[100];

  // fit server target ip from scan config
  if (whichSlave) {
    IblRodSlave::createIp4Address(fitIps[2][0], fitIps[2][1], fitIps[2][2], fitIps[2][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(fitIps[3][0], fitIps[3][1], fitIps[3][2], fitIps[3][3], &netCfgOut.targetIpB);
    snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", fitIps[2][0], fitIps[2][1], fitIps[2][2], fitIps[2][3]); std::cout << buffer << std::endl;
    snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", fitIps[3][0], fitIps[3][1], fitIps[3][2], fitIps[3][3]); std::cout << buffer << std::endl;
  } else {
    IblRodSlave::createIp4Address(fitIps[0][0], fitIps[0][1], fitIps[0][2], fitIps[0][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(fitIps[1][0], fitIps[1][1], fitIps[1][2], fitIps[1][3], &netCfgOut.targetIpB);
     snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", fitIps[0][0], fitIps[0][1], fitIps[0][2], fitIps[0][3]); std::cout << buffer << std::endl;
     snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", fitIps[1][0], fitIps[1][1], fitIps[1][2], fitIps[1][3]); std::cout << buffer << std::endl;
  }

  if (talkToFitFarm)   {
    success &= slave.setNetCfg(&netCfgOut,whichSlave);
    std::cout<< (success ? "connected to fit farm" : "failed to connect to fit farm") <<std::endl;
  }
  #endif
  return success;
}

bool CountOccupancy::injectHit() {
  bool success = true;
  // these numbers can be set from outside for tests
  /* uint32_t address = 0; // 0 or 1 for HistoUnit
  unsigned row = 10; // 0 to 336
  unsigned col = 30; // 0 to 80
  unsigned chip = 3; // 0 to 7 */
  unsigned tot = 15; 

  unsigned int colStart =  1;
  unsigned int colEnd =   81;
  if (col >= 0) {
    colStart = col;
    colEnd = col+1;
  }
  unsigned int rowStart = 1;
  unsigned int rowEnd = 337;
  if (row >= 0) {
    rowStart = row;
    rowEnd = row+1;
  }

  for (unsigned c = colStart; c < colEnd; c++) {
    for (unsigned r = rowStart; r < rowEnd; r++) {
      success = slave.injectHitInHistoUnit(whichSlave, address, r, c, tot, chip);
    }
  }
 
  return success;
}

uint32_t getNumberOfEnabledPixels() {
  Fei4 *fes = RodResourceManagerIBL::getPhysicsModuleConfigs();
  std::vector<uint16_t> enabledPixelsCount(RodResourceManagerIBL::getNModuleConfigs(), 0);
  uint32_t numberOfEnabledPixels = 0;
  for (uint16_t i = 0 ; i <  RodResourceManagerIBL::getNModuleConfigs() ; ++i) {
    for (uint16_t dc = 0 ; dc < 40 ; ++dc) {
      for (uint16_t dcR = 0 ; dcR < 672 ; ++dcR) {
	    if ( fes[i].outputEnable(dc).getPixel(dcR) ) enabledPixelsCount[i]++;
      }
    }
    //std::cout << "Number of enabled pixels for front end " << i << ": " << enabledPixelsCount[i] << std::endl;
    numberOfEnabledPixels += enabledPixelsCount[i];
  }
  if (numberOfEnabledPixels == 0) { 
    // TODO: fix this function, as it gives always zero
    numberOfEnabledPixels = 336 * 80; // the number of total pixels per frontend
  }
  return numberOfEnabledPixels;
}

bool CountOccupancy::startHistogrammer() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  bool success = true;
  std::vector<IblSlvHistCfg> histCfg(2);
  for (uint16_t sId = 0; sId < 2; ++sId) {
    for (uint16_t hId = 0; hId < 2; ++hId) {
      histCfg[sId].cfg[hId].nChips = 8; //  Fei4 only for now
      histCfg[sId].cfg[hId].enable = 1; // enable histo unit
      histCfg[sId].cfg[hId].type = ONLINE_OCCUPANCY;
      histCfg[sId].cfg[hId].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
      histCfg[sId].cfg[hId].chipSel = 1;    //0 for single chip(1 for all chips)
      histCfg[sId].cfg[hId].addrRange = 0;  //single chip <32K( full range < 1M)
      histCfg[sId].cfg[hId].scanId = 0x1234;//remote use only
      histCfg[sId].cfg[hId].binId = 1;
      histCfg[sId].cfg[hId].expectedOccValue = 1;
    }
    success &= slave.startHisto(&histCfg[sId], sId);
  }
  nTriggers = IblRod::Master::CalL1Id0Reg::read();
  std::cout << __PRETTY_FUNCTION__ << " completed!" << std::endl;
  return success;
}

bool CountOccupancy::stopHistogrammer() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  bool success = true;
  uint32_t nTriggersNow = IblRod::Master::CalL1Id0Reg::read();
  if( nTriggersNow < nTriggers ) // Wrap-around occured
    nTriggers = 0xFFFFFFFF - nTriggers + nTriggersNow;
  else nTriggers = nTriggersNow - nTriggers;

  success = slave.stopHisto(whichSlave);
  return success;
  std::cout << __PRETTY_FUNCTION__ << " completed!" << std::endl;
}

uint32_t CountOccupancy::readBufferAndFillHists(uint32_t numberOfEnabledPixels) {
  // initialize ScanBoss to get debug Histograms (clear the list before, as there can be other histograms stored)
  // TODO: find a better way to get these histograms which cannot disturb e.g. scans
  std::vector<Fei4Hist> &dbgHists = ScanBoss::getDebugHists();
  if ( dbgHists.size() ) dbgHists.clear();

  for (uint16_t i = 0 ; i <  RodResourceManagerIBL::getNModuleConfigs()/2 ; ++i) // 32FEs per Stave, 2slaves per Stave --> 16 FEs per slave
    dbgHists.push_back( Fei4Hist( Fei4Data::OCC, Fei4Data::INMEM, i));


  // initialize constants and loop over all selected pixel
  const int BUFFERSIZE = 6*80; //size of the buffer 
  const int N_ROW6 = 56; // 330 = 56 * 6(from the buffer size) gives the number of rows
  uint32_t buff[BUFFERSIZE]; 
  uint32_t counter = 0;
  unsigned FE_PER_HISTOGRAMMER = 8;

  for (uint16_t fe=feMin; fe<feMax; fe++) { // loop over all selected FrontEnds (max 8 FEs per HistoUnit) --> loop over 16 FEs per slave
    for (uint16_t rowpart=rowMin; rowpart<rowMax; rowpart++) { // loop over all rows (max N_ROW6 times)
      int effectiveFe = fe % FE_PER_HISTOGRAMMER;
      uint32_t relAddress = ((effectiveFe * N_ROW6) + rowpart) * BUFFERSIZE;
      bool success = slave.getOccHisto(whichSlave, buff, relAddress, BUFFERSIZE, fe/FE_PER_HISTOGRAMMER);
      if(!success) { std::cout << "ERROR calling getOccHisto" << std::endl; return counter; }
      // fill the debug histogram with the corresponding informations
      for (unsigned row_ix=0; row_ix < 6; ++row_ix) {
	for (unsigned col_ix=0; col_ix < 80; ++col_ix) {
          dbgHists[fe](col_ix,row_ix + rowpart*6) += (buff[row_ix*80 + col_ix] & 0x00FFFFFF);
	}
      }
      // do the actual counting
      for (int i=0; i<BUFFERSIZE; i++) {
        // for each pixel 24 bits are stored (remaining 8 are empty)
        counter += (buff[i] & 0x00FFFFFF);
      }
    }
  }  

  return counter;
}

uint32_t CountOccupancy::getOcc(bool verbose) {
  uint32_t occupancy = 0;
  // Counting number of enabled pixels per FE
  uint32_t numberOfEnabledPixels =  getNumberOfEnabledPixels();
  //Temporary, as getNumberOfEnabledPixels returns the number of total pixels per frontend
  numberOfEnabledPixels *= (feMax-feMin);

  // number of send triggers
  uint32_t calL1Id0 = IblRod::Master::CalL1Id0Reg::read(); // TODO: check that this is the correct number
  std::cout << "calL1Id0=" << calL1Id0 << std::endl;

  // read the buffer and fill the debug Histograms
  if (verbose)   
    std::cout << "try to get the histogram buffer" << std::endl;
  uint32_t counter = readBufferAndFillHists(numberOfEnabledPixels);
  if (verbose ) {
    std::cout << "Occupancy counter: " << counter << ", number of pixels " << numberOfEnabledPixels << std::endl;
    std::cout << "Number of enabled pixel: " << numberOfEnabledPixels << ", trigger value: " << nTriggers << std::endl;
  }

  if ((numberOfEnabledPixels == 0) || (calL1Id0 == 0) || (nTriggers == 0))
    occupancy = 0;
  else
    occupancy = counter; // Todo: handle casting to float static_cast<float>(counter) / (numberOfEnabledPixels * nTriggers);
  return occupancy;
}

void CountOccupancy::setNTrigRodFwCounters(uint32_t nTriggers) {
  if(nTriggers > 0x00FFFFFF)
    nTriggers = 0x00FFFFFF; // Maximum value is 24-bits
  nTriggers &= 0x00FFFFFF;
  std::cout << __PRETTY_FUNCTION__ << ": setting nTriggers to " << nTriggers << " (24-bit)" << std::endl;

  IblRod::SlaveA::CntConfigReg::reset(0x00FFFFFF);
  IblRod::SlaveB::CntConfigReg::reset(0x00FFFFFF);
  IblRod::SlaveA::CntConfigReg::set(nTriggers);
  IblRod::SlaveB::CntConfigReg::set(nTriggers);
}

uint32_t CountOccupancy::getNTrigRodFwCounters() {
  // Assuming that we always keep the same values for SlaveA and SlaveB
  // This breaks down if something else sets CntConfigReg for either slave!
  if( (IblRod::SlaveA::CntConfigReg::read() - IblRod::SlaveB::CntConfigReg::read()) & 0x00FFFFFF ) {
    std::cout << "WARNING: nTriggers for the occupancy counters is NOT the same for SlaveA and SlaveB" << std::endl;
    std::cout << "I will report only the value for SlaveA" << std::endl;
  }
  return IblRod::SlaveA::CntConfigReg::read() & 0x00FFFFFF;
}

void CountOccupancy::resetRodFwOccCounters() {

  IblRod::SlaveA::CntConfigReg::set(0x1 << 31);
  IblRod::SlaveB::CntConfigReg::set(0x1 << 31);
  std::this_thread::sleep_for(std::chrono::microseconds(10));

 
  IblRod::SlaveA::CntConfigReg::reset(0x1 << 31);
  IblRod::SlaveB::CntConfigReg::reset(0x1 << 31);

}

void CountOccupancy::enRodFwCounters(bool enCounters, RodFwCntMode rodFwCntMode) {

  uint32_t configRegToSet = 0x0;
  if( rodFwCntMode == Occ || rodFwCntMode == All ) configRegToSet |= 0x1 << 30;
  if( rodFwCntMode == Busy || rodFwCntMode == All ) configRegToSet |= 0x1 << 29;

  if(enCounters) {
    // Enabling the counters
    IblRod::SlaveA::CntConfigReg::set(configRegToSet);
    IblRod::SlaveB::CntConfigReg::set(configRegToSet);
  } else {
    // Disabling the counters
    IblRod::SlaveA::CntConfigReg::reset(configRegToSet);
    IblRod::SlaveB::CntConfigReg::reset(configRegToSet);
  }

  if( CountOccupancy::getVerbosityLevel() > 10 ) {
    // Todo: use run-time verbosity level
    std::cout << __PRETTY_FUNCTION__ << ": enCounters = " << std::boolalpha << enCounters << std::endl;
    std::cout << "IblRod::SlaveA::CntConfigReg = " << std::hex << IblRod::SlaveA::CntConfigReg::read() << std::dec << std::endl;
    std::cout << "IblRod::SlaveB::CntConfigReg = " << std::hex << IblRod::SlaveB::CntConfigReg::read() << std::dec << std::endl;
  }
}

void CountOccupancy::getOccCountersFromFW(uint32_t linkTrigCounters[32], uint32_t linkOccCounters[32], uint16_t totDesyncCounters[32], uint16_t timeoutCounters[32], uint32_t htCounters[32], uint32_t masterBusyCounters[8], bool enCounters) {
  if( CountOccupancy::getVerbosityLevel() > 20 ) std::cout << __PRETTY_FUNCTION__ << std::endl;
  // Enabling the counters
  if(enCounters) enRodFwCounters(true, Occ);

  uint32_t i = 0;
  // Note: could replace the code below with a for loop starting with offset on HitsCnt0Reg
  // for(uint32_t i = 0 ; i < 16 ; ++i) linkOccCounters[i] = IblRod::SlaveA::HitsCnt0Reg::read(i);

  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt0Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt1Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt2Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt3Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt4Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt5Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt6Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt7Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt8Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt9Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt10Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt11Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt12Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt13Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt14Reg::read();
  linkOccCounters[i++] = IblRod::SlaveA::HitsCnt15Reg::read();

  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt0Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt1Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt2Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt3Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt4Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt5Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt6Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt7Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt8Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt9Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt10Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt11Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt12Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt13Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt14Reg::read();
  linkOccCounters[i++] = IblRod::SlaveB::HitsCnt15Reg::read();

  // Number of triggers per link (since last reset)
  // Note: repurposed from V1 of occupancy counters
  // First slave A [0-15], then slave B [16-31]
  i = 0;

  linkTrigCounters[i++] = IblRod::SlaveA::CntNId0::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId1::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId2::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId3::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId4::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId5::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId6::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId7::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId8::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId9::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId10::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId11::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId12::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId13::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId14::read();
  linkTrigCounters[i++] = IblRod::SlaveA::CntNId15::read();

  linkTrigCounters[i++] = IblRod::SlaveB::CntNId0::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId1::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId2::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId3::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId4::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId5::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId6::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId7::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId8::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId9::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId10::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId11::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId12::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId13::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId14::read();
  linkTrigCounters[i++] = IblRod::SlaveB::CntNId15::read();

  // header-trailer  counters (per link)
  // First slave A [0-15], then slave B [16-31]
  i = 0;

  htCounters[i++] = IblRod::SlaveA::HtLimitCnt0Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt1Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt2Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt3Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt4Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt5Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt6Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt7Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt8Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt9Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt10Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt11Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt12Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt13Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt14Reg::read();
  htCounters[i++] = IblRod::SlaveA::HtLimitCnt15Reg::read();

  htCounters[i++] = IblRod::SlaveB::HtLimitCnt0Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt1Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt2Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt3Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt4Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt5Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt6Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt7Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt8Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt9Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt10Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt11Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt12Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt13Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt14Reg::read();
  htCounters[i++] = IblRod::SlaveB::HtLimitCnt15Reg::read();


  // master busy counters 
  i=0;
  IblRod::Master::BusyCntReadenReg::set(1);                               // ask nico

  IblRod::Master::BusyCntReadenReg::set(0); 			          // ask nico
  masterBusyCounters[i++] = IblRod::Master::BusyClkCntReg::read();	          // i=0
  masterBusyCounters[i++] = IblRod::Master::BusyTotCntReg::read();            // i=1
  masterBusyCounters[i++] = IblRod::Master::BusySlv0CntReg::read();           // i=2
  masterBusyCounters[i++] = IblRod::Master::BusySlv1CntReg::read();           // i=3
  masterBusyCounters[i++] = IblRod::Master::BusyEfbHeaderpauseCntReg::read(); // i=4
  masterBusyCounters[i++] = IblRod::Master::BusyFmtMbFef0CntReg::read();      // i=5
  masterBusyCounters[i++] = IblRod::Master::BusyFmtMbFef1CntReg::read();      // i=6
  masterBusyCounters[i++] = IblRod::Master::BusyRolPauseCntReg::read();       // i=7


  // Total descynch counters (per link)
  //First slave A [0-15], then slave B [16-31]
  i = 0;

  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt0Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt1Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt2Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt3Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt4Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt5Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt6Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt7Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt8Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt9Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt10Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt11Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt12Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt13Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt14Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TotDesynchCnt15Reg::read()), uint32_t(0xffff));

  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt0Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt1Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt2Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt3Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt4Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt5Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt6Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt7Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt8Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt9Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt10Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt11Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt12Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt13Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt14Reg::read()), uint32_t(0xffff));
  totDesyncCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TotDesynchCnt15Reg::read()), uint32_t(0xffff));

  // Timeout histogram (one bin per link)
  // First slave A [0-15], then slave B [16-31]
  i = 0;

  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt0Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt1Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt2Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt3Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt4Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt5Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt6Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt7Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt8Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt9Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt10Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt11Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt12Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt13Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt14Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveA::TimeoutCnt15Reg::read()), uint32_t(0xffff));

  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt0Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt1Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt2Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt3Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt4Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt5Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt6Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt7Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt8Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt9Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt10Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt11Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt12Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt13Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt14Reg::read()), uint32_t(0xffff));
  timeoutCounters[i++] = std::min(uint32_t(IblRod::SlaveB::TimeoutCnt15Reg::read()), uint32_t(0xffff));

#ifdef LL_DEBUG
  // Note: use the calling code (e.g. QuickStatus) to display data
  bool testCounters = true;
  for(uint32_t j = 0 ; j < i ; ++j)
  if( slvOccCounters[j] != IblRod::SlaveA::HitsCnt0Reg::read(j) ) {
    std::cout << j << ": " << slvOccCounters[j] << std::endl;
    testCounters = false;
    break;
  }
  std::cout << "Test counters: " << std::boolalpha << testCounters << std::endl;
#endif

  // Disabling the counters
  if(enCounters) enRodFwCounters(false, Occ);
}

void CountOccupancy::getBusyCountersFromFW(uint32_t slvBusyCounters[64], bool enCounters) {
  if( CountOccupancy::getVerbosityLevel() > 20 ) std::cout << __PRETTY_FUNCTION__ << std::endl;

  // Enabling the counters
  if(enCounters) enRodFwCounters(true, Busy);

  uint32_t i = 0;
  // Note: could replace the code below with a for loop starting with offset on BusyCntReg

  // Clock register and total BUSY counter
  slvBusyCounters[i++] = IblRod::SlaveA::ClockCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyCntReg::read();

  // S-LINK X-OFF and LDOWN counters
  slvBusyCounters[i++] = IblRod::SlaveA::BusySlink0LffCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusySlink0LdownCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusySlink1LffCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusySlink1LdownCntReg::read();

  // Formatter BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt00CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt01CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt02CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt03CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt10CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt11CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt12CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt13CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt20CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt21CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt22CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt23CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt30CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt31CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt32CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyFmt33CntReg::read();

  // EFB BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveA::BusyEfb10CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyEfb11CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyEfb20CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyEfb21CntReg::read();

  // Router BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveA::BusyRouter0CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveA::BusyRouter1CntReg::read();

  i = 32; // Marker

  // Clock register and total BUSY counter
  slvBusyCounters[i++] = IblRod::SlaveB::ClockCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyCntReg::read();

  // S-LINK X-OFF and LDOWN counters
  slvBusyCounters[i++] = IblRod::SlaveB::BusySlink0LffCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusySlink0LdownCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusySlink1LffCntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusySlink1LdownCntReg::read();

  // Formatter BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt00CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt01CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt02CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt03CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt10CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt11CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt12CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt13CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt20CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt21CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt22CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt23CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt30CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt31CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt32CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyFmt33CntReg::read();

  // EFB BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveB::BusyEfb10CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyEfb11CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyEfb20CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyEfb21CntReg::read();

  // Router BUSY counters
  slvBusyCounters[i++] = IblRod::SlaveB::BusyRouter0CntReg::read();
  slvBusyCounters[i++] = IblRod::SlaveB::BusyRouter1CntReg::read();

  // Disabling the counters
  if(enCounters) enRodFwCounters(false, Busy);

#ifdef LL_DEBUG
  // Note: use the calling code (e.g. QuickStatus) to display data
  for(size_t j = 0 ; j < i ; ++j)
    std::cout << std::setw(2) << j << ": " << slvBusyCounters[j] << std::endl;
#endif
}

// inside execute() the actual meat of the command happens on the ppc
void CountOccupancy::execute() {

  result.success = true;
#ifdef __XMK__
  // start histogrammer, fitfarm
  bool success = true;
  success &= startSlavesAndFitfarm();
  std::cout << "try to start the histogrammer" << std::endl;
  success = startHistogrammer();
  std::cout<< (success ? "started histogramming" : "failed to start histogramming") <<std::endl;
  result.success &= success;

  if (testInj) {
    // for test purpose to get some injection and have the possibility to see some occupancy
    success = injectHit();
    std::cout << (success ? "had some injection" : "failed to inject") << std::endl;
    result.success &= success;
  }

  // wait some time
  std::cout << "wait some time: " << sleepTime << std::endl;
  std::this_thread::sleep_for( std::chrono::microseconds(sleepTime));

  std::cout << "don't want to wait anymore" << std::endl;
 
  // stop histogrammer 
  std::cout << "try to stop the histogrammer" << std::endl;
  success = stopHistogrammer();
  std::cout<< (success ? "stopped histogramming" : "failed to stop histogramming") <<std::endl;
  result.success &= success;

  // Return the occupancy
  // Todo: use firmware feature
  result.counter = getOcc(true);
  // Display, do not propagate
  std::cout << "Occupancy: " << static_cast<float>(result.counter) / (getNumberOfEnabledPixels() * nTriggers) << std::endl;

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
void CountOccupancy::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<unsigned int>::serialize(out, offset, sleepTime);
	Serializer<bool>::serialize(out,offset,testInj);
	Serializer<bool>::serialize(out,offset,talkToFitFarm);
	Serializer<bool>::serialize(out,offset, whichSlave);
	Serializer<uint16_t>::serialize(out, offset, feMin);
	Serializer<uint16_t>::serialize(out, offset, feMax);
	Serializer<uint16_t>::serialize(out, offset, rowMin);
	Serializer<uint16_t>::serialize(out, offset, rowMax);
	Serializer<uint32_t>::serialize(out, offset, row);
	Serializer<uint32_t>::serialize(out, offset, col);
	Serializer<uint32_t>::serialize(out, offset, chip);
	Serializer<uint32_t>::serialize(out, offset, address);
	
  for (int i = 0; i<4; i++) {
      Serializer<uint32_t>::serialize(out, offset, fitPorts[i]);
      for (int j = 0; j<4; j++) {
        Serializer<uint8_t>::serialize(out, offset, fitIps[i][j]);
     }
  }
}

// Deseralize the data members of the command
void CountOccupancy::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	sleepTime = Serializer<unsigned int>::deserialize(in, offset);
	testInj = Serializer<bool>::deserialize(in,offset);
	talkToFitFarm = Serializer<bool>::deserialize(in,offset);
	whichSlave = Serializer<bool>::deserialize(in,offset);
	feMin = Serializer<uint16_t>::deserialize(in, offset);
	feMax = Serializer<uint16_t>::deserialize(in, offset);
	rowMin = Serializer<uint16_t>::deserialize(in, offset);
	rowMax = Serializer<uint16_t>::deserialize(in, offset);
	row = Serializer<uint32_t>::deserialize(in, offset);
	col = Serializer<uint32_t>::deserialize(in, offset);
	chip = Serializer<uint32_t>::deserialize(in, offset);
	address = Serializer<uint32_t>::deserialize(in, offset);
    for (int i = 0; i<4; i++) {
      fitPorts[i] = Serializer<uint32_t>::deserialize(in, offset);
      for (int j = 0; j<4; j++) {
        fitIps[i][j] = Serializer<uint8_t>::deserialize(in, offset);
      }
    }
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t CountOccupancy::serializedSize() const
{
  return (3*sizeof(bool)+ 8*sizeof(uint32_t)+16*sizeof(uint8_t)+4*sizeof(uint16_t)+sizeof(unsigned int));
}


// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void SuccessAndCounter::serialize(uint8_t *out) const {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<bool>::serialize(out, offset, success);
	  Serializer<uint32_t>::serialize(out, offset, counter);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void SuccessAndCounter::deserialize(const uint8_t *in) {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  success = Serializer<bool>::deserialize(in, offset);
	  counter = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t SuccessAndCounter::serializedSize() const {
		//std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (1*sizeof(uint32_t)+1*sizeof(bool));
}
