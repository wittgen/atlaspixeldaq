/*
 * Russell Smith
 * Date: 2014-Feb-26
 */
//Steve Alkire <alkire@cern.ch> 2014.03.13

#include "StartScan.h"


#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

StartScan::StartScan(){ 
  m_scanDebug = _NormalScan;
  channels = 0xFFFFFFFF;
  scanId = 7777;
  for (int i = 0; i<4; i++) {
    fitPorts[i] = 0;
    fitIps[i][0] = 0; fitIps[i][1] = 0;
    fitIps[i][2] = 0; fitIps[i][3] = 0;
  }
}

StartScan::StartScan(ScanDebug_t ts){
  m_scanDebug = ts;
}
void StartScan::execute(){

#ifndef __DO_IBL_GET_STATUS__
  setResultPtr();
#endif

#ifdef __XMK__
  switch(m_scanDebug){//Default: Normal scan. Others for testing.
  case _NormalScan:
#ifdef __DO_IBL_GET_STATUS__
    ScanBoss::startScan(channels, scanId, fitPorts, fitIps);
#else
    result.m_scanResult = ScanBoss::startScan(channels, scanId, fitPorts, fitIps);
#endif
    break;
  case _TestRegisters:
    ScanBoss::startRegisterScan();
    //ScanBoss::startRegisterScan(channels);
    break;
  }
#endif // __XMK__
}

void StartScan::serialize(uint8_t *out) const{
  uint32_t offset = 0; 
  Serializer<ScanDebug_t>::serialize(out, offset, m_scanDebug);
  Serializer<uint32_t>::serialize(out, offset, channels);
  Serializer<uint32_t>::serialize(out, offset, scanId);
  for (int i = 0; i<4; i++) {
      Serializer<uint32_t>::serialize(out, offset, fitPorts[i]); 
      for (int j = 0; j<4; j++) {
	Serializer<uint8_t>::serialize(out, offset, fitIps[i][j]); 
     }
  }
}
void StartScan::deserialize(const uint8_t *in){
  uint32_t offset = 0; 
  m_scanDebug = Serializer<ScanDebug_t>::deserialize(in, offset);
  channels = Serializer<uint32_t>::deserialize(in, offset);
  scanId = Serializer<uint32_t>::deserialize(in, offset);
    for (int i = 0; i<4; i++) {
      fitPorts[i] = Serializer<uint32_t>::deserialize(in, offset); 
      for (int j = 0; j<4; j++) {
	fitIps[i][j] = Serializer<uint8_t>::deserialize(in, offset); 
      }
    }
}

uint32_t StartScan::serializedSize() const{
  return (sizeof(ScanDebug_t) + 6*sizeof(uint32_t)+16*sizeof(uint8_t));
}

#ifndef __DO_IBL_GET_STATUS__
void ScanResults::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, m_scanResult.state);
        Serializer<uint32_t>::serialize(out, offset, m_scanResult.errorState);
        Serializer<int32_t>::serialize(out, offset, m_scanResult.mask);
        Serializer<int32_t>::serialize(out, offset, m_scanResult.par);
        Serializer<int32_t>::serialize(out, offset, m_scanResult.par1);
        Serializer<int32_t>::serialize(out, offset, m_scanResult.par2);
        Serializer<int8_t>::serialize(out, offset, m_scanResult.dc);
        Serializer<int32_t>::serialize(out, offset, m_scanResult.trig);

}
void ScanResults::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        m_scanResult.state = (ScanState)Serializer<uint32_t>::deserialize(in, offset);
        m_scanResult.errorState = (PPCErrorType)Serializer<uint32_t>::deserialize(in, offset);
        m_scanResult.mask = Serializer<int32_t>::deserialize(in, offset);
        m_scanResult.par = Serializer<int32_t>::deserialize(in, offset);
        m_scanResult.par1 = Serializer<int32_t>::deserialize(in, offset);
        m_scanResult.par2 = Serializer<int32_t>::deserialize(in, offset);
        m_scanResult.dc = Serializer<int8_t>::deserialize(in, offset);
        m_scanResult.trig = Serializer<int32_t>::deserialize(in, offset);
}
uint32_t ScanResults::serializedSize() const
{
        return (2*sizeof(uint32_t) + 5*sizeof(int32_t) + sizeof(int8_t));
}
#endif
