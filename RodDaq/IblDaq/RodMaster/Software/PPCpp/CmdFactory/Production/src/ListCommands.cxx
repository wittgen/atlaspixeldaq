#include "ListCommands.h"
#include "CommandManager.h"

void ListCommands::execute() {
#ifdef __XMK__
  CommandManager::CommandMap::const_iterator it = CommandManager::getCommandMap().begin();
  for(; it != CommandManager::getCommandMap().end(); ++it) {
    result.commands[it->first] = it->second->toString();
  }
#endif
}

void LocalCommands::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<LocalCommands::CommandMap>::serialize(out, offset, commands);
}

void LocalCommands::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  commands = Serializer<LocalCommands::CommandMap>::deserialize(in, offset);
}

uint32_t LocalCommands::serializedSize() const {
  uint32_t size = sizeof(uint32_t); // For commands.size()
  // Need to get the size of each string in the map. 
  for(LocalCommands::CommandMap::const_iterator it = commands.begin(); it != commands.end(); ++it) {
    size += sizeof(uint16_t); // For the integer key
    size += sizeof(uint8_t); // For string.size()
    size += it->second.size()*sizeof(char);
  }
  return size;
}
