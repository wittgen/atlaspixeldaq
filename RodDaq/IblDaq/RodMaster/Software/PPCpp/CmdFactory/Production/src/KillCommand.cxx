#include "KillCommand.h"

#ifdef __XMK__
#include <arpa/inet.h>
#include <sys/socket.h>
#include <pthread.h>
#endif

/* OKUPDATE
#include <sys/kpthread.h>
// Imported from XilKernel sys_arch.c
struct thread_start_param {
	struct sys_thread *thread;
	void (*function)(void*);
	void *arg;
};

struct sys_thread {
	pthread_t tid;
	struct thread_start_param tp;
};

extern "C" pthread_info_t* pthread_get_info (pthread_t thr);
#endif 
*/


KillCommand::KillCommand() {
  targetTypeId = 7777;
  targetExecId = 7777;
}

KillCommand::KillCommand(uint16_t typeId, uint16_t execId) {
  targetTypeId = typeId;
  targetExecId = execId;
}

KillCommand::KillCommand(const Command& target) {
  targetTypeId = target.getTypeId();
  targetExecId = target.getExecId();
}

void KillCommand::execute() {
    /* OKUPDATE
#ifdef __XMK__
  if (Link::s_threads.find(targetTypeId) == Link::s_threads.end() or Link::s_threads[targetTypeId].find(targetExecId) == Link::s_threads[targetTypeId].end()){
    std::cout << "No command found with type ID: " << targetTypeId << " and exec ID: " << targetExecId << std::endl;
    return;
  }
  else if (targetTypeId == this->getTypeId() && targetExecId == this->getExecId()) {
    std::cout << "KillCommand cannot be used to kill itself." << std::endl;
    return;
  }

  std::cout << "Killing command with type ID: " << targetTypeId << " and exec ID: " << targetExecId << "." << std::endl;

  // Close the connection, terminate the thread, and remove it from the map.
  int sd = Link::s_threads[targetTypeId][targetExecId].second;
  sys_thread_t thread = Link::s_threads[targetTypeId][targetExecId].first;

  close(sd);
  pthread_terminate(pthread_get_info(thread->tid));
  thread->tid = TID_FREE;
  // Mutex lock to remove entry from map.
  pthread_mutex_lock(&Link::s_mutex);
  Link::s_threads[targetTypeId].erase(targetExecId);
  pthread_mutex_unlock(&Link::s_mutex);
#endif
  */
}

void KillCommand::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint16_t>::serialize(out, offset, targetTypeId);
  Serializer<uint16_t>::serialize(out, offset, targetExecId);
}

void KillCommand::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  targetTypeId = Serializer<uint16_t>::deserialize(in, offset);
  targetExecId = Serializer<uint16_t>::deserialize(in, offset);
}

uint32_t KillCommand::serializedSize() const {
  return 2*sizeof(uint16_t);
}


void KillAllCommands::execute() {
    /* UPDATE
#ifdef __XMK__
  for(Link::ThreadMap::iterator outer = Link::s_threads.begin(); outer != Link::s_threads.end(); ++outer){
    Link::ThreadMapInner &innerMap = outer->second;
    for(Link::ThreadMapInner::iterator inner = innerMap.begin(); inner != innerMap.end(); ++inner){
      // Don't kill this command.
      if (outer->first == this->getTypeId() && inner->first == this->getExecId()) continue;
      // Get socketId and thread pointer from the stored pair.
      int sd = inner->second.second;
      sys_thread_t thread = inner->second.first;
      // Close the connection and terminate the thread.
      close(sd);
      pthread_terminate(pthread_get_info(thread->tid));
      thread->tid = TID_FREE;
    }
  }
  // Mutex lock to clear the map.
  pthread_mutex_lock(&Link::s_mutex);
  Link::s_threads.clear();
  pthread_mutex_unlock(&Link::s_mutex);

#endif
  */
}
