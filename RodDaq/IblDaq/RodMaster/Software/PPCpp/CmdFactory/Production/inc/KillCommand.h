#ifndef _KILLCOMMAND_H_
#define _KILLCOMMAND_H_
/*
 * Author: 
 * B. Axen <bradley.axen@cern.ch>
 * Date: 2014-May-30
 *
 * This command cancels the thread of a running command.
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Serializer.h"
#include "Command.h"
#include "GeneralResults.h"

class KillCommand: public CommandCRTP<KillCommand > {
 public:
  InstantiateResult(SuccessResult)

  KillCommand();
  KillCommand(uint16_t typeId, uint16_t execId);
  KillCommand(const Command& target);
  virtual ~KillCommand() {}

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
 
  uint16_t targetTypeId;
  uint16_t targetExecId;
};

class KillAllCommands: public CommandCRTP<KillAllCommands > {
 public:
  InstantiateResult(SuccessResult)

  virtual ~KillAllCommands() {}

  virtual void execute();
  virtual void serialize(uint8_t *out) const {}
  virtual void deserialize(const uint8_t *in) {}
  virtual uint32_t serializedSize() const {return 0;}
};
#endif //_KILLCOMMAND_H_

