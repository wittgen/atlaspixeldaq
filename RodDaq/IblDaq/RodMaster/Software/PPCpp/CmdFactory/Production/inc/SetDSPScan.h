/*
 * Changed from SetAsOptoL12 in a generic way in order support different scan types
 * JuanAn 20-Nov-2018
 */

#ifndef _SET_DSP_SCAN_H_
#define _SET_DSP_SCAN_H_

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class SetDSPScan: public CommandCRTP<SetDSPScan > {
 public:
  enum ScanTypeDSP {NONE, BOC_THR_DEL_DSP, LEAK_SCAN_DSP};
  
  InstantiateResult(EmptyResult);

  virtual const Command* getResult() const {
      return &result;
  }
   
  SetDSPScan();
  virtual ~SetDSPScan() {} 
  
 ScanTypeDSP scanType;

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

};
#endif //_SET_DSP_SCAN_


