#ifndef _WRITEREGISTER_H_
#define _WRITEREGISTER_H_

#include "Command.h"
#include "Serializer.h"

#include "Fei3ModCfg.h"
#include "Fei4.h"

class WriteRegisterResult: public CommandCRTP<WriteRegisterResult> {
  public:
    bool success = false;

    virtual void serialize(uint8_t *out) const {
        uint32_t offset = 0;
        Serializer<bool>::serialize(out, offset, success);
    };

    virtual void deserialize(const uint8_t *in) {
        uint32_t offset = 0;
        success  = Serializer<bool>::deserialize(in, offset);
    };

    virtual uint32_t serializedSize() const {
        return sizeof(bool);
    };
};

template <typename TReg>
class WriteRegisterFEI3: public CommandCRTP<WriteRegisterFEI3<TReg> > {
//#define CommandInstance(Type) Type: public CommandCRTP<Type >
  public:
    InstantiateResult(WriteRegisterResult);

    virtual void execute();

    virtual const Command* getResult() const {
        return &result;
    }

    WriteRegisterFEI3();
    virtual ~WriteRegisterFEI3() = default;

    TReg reg;
    std::array<int,Fei3ModCfg::nFE> values;
    //This we also need to get the correst service on the ROD
    enum ConfigType { Calib, Physics };
    ConfigType configType = ConfigType::Calib;
    uint32_t i_mod;

    virtual void serialize(uint8_t *out) const {
        uint32_t offset = 0;
        Serializer<TReg>::serialize(out, offset, reg);
        Serializer<std::array<int, Fei3ModCfg::nFE>>::serialize(out, offset, values);
        Serializer<ConfigType>::serialize(out, offset, configType);
        Serializer<uint32_t>::serialize(out, offset, i_mod);
    };

    virtual void deserialize(const uint8_t *in) {
        uint32_t offset = 0;
        reg  = Serializer<TReg>::deserialize(in, offset);
        values  = Serializer<std::array<int,Fei3ModCfg::nFE>>::deserialize(in, offset);
        configType = Serializer<ConfigType>::deserialize(in, offset);
        i_mod = Serializer<uint32_t>::deserialize(in, offset);
    };

    virtual uint32_t serializedSize() const {
        return sizeof(TReg)+sizeof(std::array<int,Fei3ModCfg::nFE>)+sizeof(ConfigType)+sizeof(uint32_t);
    };
};

template class WriteRegisterFEI3<Fei3::GlobReg>;
template class WriteRegisterFEI3<Fei3::PixReg>;

template <typename TReg>
class WriteRegisterFEI4: public CommandCRTP<WriteRegisterFEI4<TReg> > {
  public:
    InstantiateResult(WriteRegisterResult);

    virtual void execute();

    virtual const Command* getResult() const {
        return &result;
    }

    WriteRegisterFEI4();
    virtual ~WriteRegisterFEI4() = default;

    TReg reg;
    int value;
    //This we also need to get the correst service on the ROD
    enum ConfigType { Calib, Physics };
    ConfigType configType = ConfigType::Calib;
    uint32_t i_mod;

    virtual void serialize(uint8_t *out) const {
        uint32_t offset = 0;
        Serializer<TReg>::serialize(out, offset, reg);
        Serializer<int>::serialize(out, offset, value);
        Serializer<ConfigType>::serialize(out, offset, configType);
        Serializer<uint32_t>::serialize(out, offset, i_mod);
    };

    virtual void deserialize(const uint8_t *in) {
        uint32_t offset = 0;
        reg  = Serializer<TReg>::deserialize(in, offset);
        value  = Serializer<int>::deserialize(in, offset);
        configType = Serializer<ConfigType>::deserialize(in, offset);
        i_mod = Serializer<uint32_t>::deserialize(in, offset);
    };

    virtual uint32_t serializedSize() const {
        return sizeof(TReg)+sizeof(int)+sizeof(ConfigType)+sizeof(uint32_t);
    };
};

template class WriteRegisterFEI4<Fei4::GlobReg>;
template class WriteRegisterFEI4<Fei4::PixReg>;

#endif
