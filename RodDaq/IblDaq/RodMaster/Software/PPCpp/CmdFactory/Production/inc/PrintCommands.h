#ifndef _PRINTCOMMANDS_H_
#define _PRINTCOMMANDS_H_
/*
 * Author: 
 * B. Axen <bradley.axen@cern.ch>
 * Date: 2014-Jun-04
 *
 * This command prints all currently running commands.
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class PrintCommands: public CommandCRTP<PrintCommands > {
 public:
  InstantiateResult(SuccessResult)

  virtual ~PrintCommands() {}

  virtual void execute();
  virtual void serialize(uint8_t *out) const {}
  virtual void deserialize(const uint8_t *in) {}
  virtual uint32_t serializedSize() const {return 0;}
};
#endif //_PRINTCOMMANDS_H_

