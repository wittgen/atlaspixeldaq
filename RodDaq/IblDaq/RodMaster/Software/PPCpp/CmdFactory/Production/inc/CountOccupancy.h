#ifndef _COUNTOCCUPANCY_H_
#define _COUNTOCCUPANCY_H_
/*
 * * D. Boerner <daniela.boerner@cern.ch>
 * Date: 2014-March-10
 *
 * Tests that the communication between ppc and slave is OK
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "TestSlave.h"

class SuccessAndCounter: public CommandCRTP<SuccessAndCounter > {
  public:
    bool success;
    uint32_t counter;
	
  // necessary inherited functions
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class CountOccupancy: public CommandCRTP<CountOccupancy > { // declares type of command
public:
	typedef SuccessAndCounter ResultType;
	ResultType result;

	CountOccupancy();
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~CountOccupancy() {}

	// functions for starting/stopping the histogrammer, read the buffer,...
	bool startHistogrammer();
        bool stopHistogrammer();
	bool injectHit();
	bool startSlavesAndFitfarm();
        uint32_t readBufferAndFillHists(uint32_t numberOfEnabledPixels);
	uint32_t  getOcc(bool verbose=false);

	enum RodFwCntMode { All, Busy, Occ };

	static void setNTrigRodFwCounters(uint32_t nTriggers);
	static uint32_t getNTrigRodFwCounters();
	static void resetRodFwOccCounters();
	static void enRodFwCounters(bool enCounters, RodFwCntMode rodFwCntMode = All);
	static void getOccCountersFromFW(uint32_t linkTrigCounters[32], uint32_t linkOccCounters[32], uint16_t totDesyncCounters[32], uint16_t timeoutCounters[32], uint32_t htCounters[32], uint32_t masterBusyCounters[8], bool enCounters);
	static void getBusyCountersFromFW(uint32_t slvBusyCounters[64], bool enCounters);

	// Singleton-like variable, avoids setting a static member
	static int& getVerbosityLevel() { static int verbosityLevel = 0; return verbosityLevel; }
	static void setVerbosityLevel(int level) { CountOccupancy::getVerbosityLevel() = level; }


	// command parameters of this command
	IblRodSlave slave;
	bool talkToFitFarm;
	bool whichSlave;
	uint32_t fitPorts[4];
	uint8_t fitIps[4][4];
	unsigned int sleepTime;
	bool testInj;

  	uint16_t feMin;
	uint16_t feMax;
	uint16_t rowMin;
	uint16_t rowMax;

	int32_t row;
	int32_t col;
	uint32_t chip;
	uint32_t address;

  uint32_t nTriggers;
};
#endif// _COUNTOCCUPANCY_H_

