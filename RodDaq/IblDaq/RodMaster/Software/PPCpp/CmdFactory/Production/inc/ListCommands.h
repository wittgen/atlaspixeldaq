#ifndef _LISTCOMMANDS_H_
#define _LISTCOMMANDS_H_
/*
 * Author: 
 * B. Axen <laura.jeanty@cern.ch>
 * Date: 2014-Jun-10
 *
 * This command retrieves the local command map from the PPC.
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Serializer.h"
#include "Command.h"

#include <map>
#include <string>

class LocalCommands: public CommandCRTP<LocalCommands > {
 public:
  typedef std::map<uint16_t, std::string> CommandMap;
  CommandMap commands;
	
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

class ListCommands: public CommandCRTP<ListCommands > {
public:
  InstantiateResult(LocalCommands)

  virtual void execute();
  virtual const Command* getResult() const {
    return &result;
  }
  
  /// Inhertited functions
  virtual void serialize(uint8_t *out) const {}
  virtual void deserialize(const uint8_t *in) {}
  virtual uint32_t serializedSize() const {return 0;}
  
  virtual ~ListCommands() {}
};
#endif //_LISTCOMMANDS_H_

