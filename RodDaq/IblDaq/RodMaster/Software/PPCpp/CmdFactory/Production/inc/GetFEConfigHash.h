#ifndef _GETFECONFIGHASH_H_
#define _GETFECONFIGHASH_H_

#include "Command.h"
#include "Serializer.h"

class GetFEConfigHashResult: public CommandCRTP<GetFEConfigHashResult > {
  public:
    //The hash is the combined hash of all the various registers, on IBL these are the pixel,
    //global, and external ones, on Pixel also the MCC config
    uint32_t hash;

    virtual void serialize(uint8_t *out) const {
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, hash);
    };

    virtual void deserialize(const uint8_t *in) {
        uint32_t offset = 0;
        hash  = Serializer<uint32_t>::deserialize(in, offset);
    };

    virtual uint32_t serializedSize() const {
        return sizeof(uint32_t);
    };
};

class GetFEConfigHash: public CommandCRTP<GetFEConfigHash > {
  public:
    InstantiateResult(GetFEConfigHashResult);

    virtual void execute(){
        #ifdef __XMK__
        if(feFlav == fei4) {
            Fei4* fe = ((configType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs() :
                                                  RodResourceManagerIBL::getInstance().getCalibModuleConfigs());
            result.hash = static_cast<Fei4Cfg*>(&fe[channel])->getHash();
        } else if(feFlav == fei3) {
            Fei3Mod* fe = ((configType == Physics) ? RodResourceManagerPIX::getInstance().getPhysicsModuleConfigs() :
                                                     RodResourceManagerPIX::getInstance().getCalibModuleConfigs());
            result.hash = static_cast<Fei3ModCfg*>(&fe[channel])->getHash();
        }
        #endif
    };

    virtual const Command* getResult() const {
        return &result;
    }

    GetFEConfigHash(): channel(0), feFlav(fei3), configType(Calib) {
        setResultPtr();
    }

    virtual ~GetFEConfigHash(){}; // = default;

    //The channel is either the addressed FE (for IBL) or the module (Pixel)
    int32_t channel;
    //We need to know the FE flavour to get the corresponding interfacte to the FE on the ROD
    enum feFlavour { fei3, fei4 };
    int32_t feFlav;
    //This we also need to get the correst service on the ROD
    enum ConfigType { Calib, Physics };
    ConfigType configType;

    virtual void serialize(uint8_t *out) const {
        uint32_t offset = 0;
        Serializer<int32_t>::serialize(out, offset, channel);
        Serializer<int32_t>::serialize(out, offset, feFlav);
        Serializer<ConfigType>::serialize(out, offset, configType);
    };

    virtual void deserialize(const uint8_t *in) {
        uint32_t offset = 0;
        channel  = Serializer<int32_t>::deserialize(in, offset);
        feFlav  = Serializer<int32_t>::deserialize(in, offset);
        configType  = Serializer<ConfigType>::deserialize(in, offset);
    };

    virtual uint32_t serializedSize() const {
        return 2*sizeof(int32_t)+sizeof(ConfigType);
    };
};
#endif //_GETCFGHASH_H_


