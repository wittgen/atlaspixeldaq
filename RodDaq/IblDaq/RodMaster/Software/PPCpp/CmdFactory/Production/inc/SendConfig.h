#ifndef _SENDCONFIG_H_
#define _SENDCONFIG_H_
/*
 * Authors: 
 *
 * Russell Smith
 * Laura Jeanty
 * Date: 2014-6-25
 *
 * This command sends the full (global + pixel + extended) configuration 
 * from the ROD to one FE.
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "GeneralResults.h"
#include "Command.h"
#ifdef __XMK__
#include "Fei4.h"
#endif
#include <vector>

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class SendConfig: public CommandCRTP<SendConfig > {
public:
  InstantiateResult(SuccessResult)

	enum ConfigType { Physics, Calib };

	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	SendConfig();
	virtual ~SendConfig() {}

	void setPhysicsConfig(){ m_ConfigType = Physics;};
	void setCalibConfig(){ m_ConfigType = Calib;};
	void setFeToSend(uint32_t nFe){i_mod = nFe;}

protected:

private:
	ConfigType m_ConfigType;
	uint32_t i_mod;
};
#endif //_SENDCONFIG_H_

