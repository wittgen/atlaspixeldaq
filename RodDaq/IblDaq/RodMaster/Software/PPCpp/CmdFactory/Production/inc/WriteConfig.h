#ifndef _WRITECONFIG_H_
#define _WRITECONFIG_H_
/*
  Steve Alkire <alkire@cern.ch> 2014.03.27
  Use if you want to write any type config.
  Usage: Choose your mask. Push back Cfgs corresponding lowest masked bit first.
*/

#include "CppCompatibility.h"
#include "Command.h"
//todo cleanup
#include "Fei4Cfg.h"
#include "Fei3ModCfg.h"
//#include "RodResourceManager.h"
#include <string>
#include <vector>
#include <memory>
#include "GeneralResults.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "RecoAtECR.h"
#endif


#include <iostream>

//todo a lot of this should probably be moved to private (members especially)
template<typename _FeModCfgType>
class WriteConfig: public Command, public EasySerializable{
 public:
 
  virtual shared_ptr<Command> getCopy() const {
    shared_ptr<Command> cmd = shared_ptr<Command>( new WriteConfig<_FeModCfgType>(static_cast<WriteConfig<_FeModCfgType> const&>(*this)) );
    cmd->setResultPtr();
    return cmd;
  }
  uint16_t getTypeId() const { return TypeId<WriteConfig<_FeModCfgType> >::getTypeId(); }

  //  uint32_t m_channels;
  uint32_t i_mod;
  enum ConfigType { Calib, Physics };
  ConfigType configType;

 InstantiateResult(EmptyResult)

  WriteConfig(uint32_t i_mod_arg, ConfigType configType_rhs):i_mod(i_mod_arg),configType(configType_rhs){setResultPtr();}
  WriteConfig():i_mod(16), configType(Physics){setResultPtr();} //todo do we want to force the user to set channels?

  virtual ~WriteConfig() {}

  _FeModCfgType moduleConfig;
  //todo add another one since we want to send a full module
  //todo make private?
  
  void setCalibConfig(){configType = Calib;}
  void setFeToWrite(uint32_t nFe){i_mod = nFe;}
  
  //todo maybe move to .cxx? although it's a template...
  virtual void execute() {}

  void deserialize(const uint8_t *in){
#ifdef __XMK__
    uint32_t offset = 0;
    configType = Serializer<ConfigType>::deserialize(in,offset);
    i_mod      = Serializer<uint32_t>::deserialize(in,offset);
    moduleConfig=Serializer<_FeModCfgType>::deserialize(in,offset);//this is deserializing moduleConfigs!!
#endif
  }

public://todo check if this public is neeeded
  
  SERIAL_H(prep(configType); prep(i_mod); prep(moduleConfig);)
};


template<>
inline void WriteConfig<Fei4Cfg>::execute() {
  
#ifdef __XMK__
  Fei4 *fe = ((configType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs() : RodResourceManagerIBL::getInstance().getCalibModuleConfigs());

#if LL_DEBUG
    std::cout << "loop : " << i_mod << std::endl;
    std::cout<<"Config before writing: "<<std::endl;
    //fe[i_mod].Fei4CfgExtCfg::dump();
    std::cout<<std::endl;
#endif
  //lock Reco at ECR thread
  // OKUPDATE Bad style c++ locking
  std::lock_guard<std::mutex>(RecoAtECR::mtx_cfg);
 // xil_printf("WriteConfig locking RecoAtECR thread\n");
    //todo do this a nicer way
    *((Fei4Cfg*)(&fe[i_mod])) = moduleConfig;
    // Hard-coded safeguard against writing Vthin_Fine < 64 when Vthin_Coarse is zero
    // safe for b-field 
    if ((fe[i_mod].getValue(&Fei4::Vthin_Coarse) == 0) && ((fe[i_mod].getValue(&Fei4::Vthin_Fine)&0x3) == 0)) {
      std::cout << "WARNING: For rx: "<<(int)i_mod<<" Ignoring instruction to set FE threshold below (Vthin_Coarse, Vthin_Fine) = (0,64)." << std::endl;
      std::cout << "WARNING: Setting Vthin to 64" << std::endl;
      fe[i_mod].writeRegister(&Fei4::Vthin_Fine, 64);
    }

#if LL_DEBUG
    std::cout<<"Config after writing: "<<std::endl;
    fe[i_mod].dump();
#endif

  RodResourceManagerIBL::updateConnectivity(fe);

//xil_printf("WriteConfig unlocking RecoAtECR thread\n");


#endif // __XMK__

}


template<>
inline void WriteConfig<Fei3ModCfg>::execute() {

#ifdef __XMK__
  Fei3Mod* fe = ((configType == Physics) ? RodResourceManagerPIX::getInstance().getPhysicsModuleConfigs() : RodResourceManagerPIX::getInstance().getCalibModuleConfigs());
  //std::cout << "Module " << moduleConfig.idStr << " has " << "i_mod=" << i_mod << ", rxCh=" << static_cast<uint32_t>(moduleConfig.m_rxCh) << ", txCh=" << static_cast<uint32_t>(moduleConfig.m_txCh) << ", and FEEN=" << HEX( moduleConfig.mccRegisters[FEEN] ) << std::endl;
  xil_printf("Module %s has i_mod %d rxCh=%d txCh=%d and FEEN=0x%08x\n",moduleConfig.idStr,i_mod,(int)moduleConfig.m_rxCh,(int)moduleConfig.m_txCh,moduleConfig.mccRegisters[FEEN]);

  //lock Reco at ECR thread
  // OKUPDATE Bad style c++ locking
  std::lock_guard<std::mutex>(RecoAtECR::mtx_cfg);


  //todo do this a nicer way
  *((Fei3ModCfg*)(&fe[i_mod])) = moduleConfig;

  RodResourceManagerPIX::updateConnectivity(fe);


#endif // __XMK__

}

typedef WriteConfig<Fei3ModCfg> Fei3ModWriteConfig;
typedef WriteConfig<Fei4Cfg> Fei4WriteConfig;
#endif // _WRITECONFIG_H_

/*  LocalWords:  maskless
 */
