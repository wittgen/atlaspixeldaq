#ifndef _RECONFIG_MODULES_H_
#define _RECONFIG_MODULES_H_
/*
 * Authors: 
 *
 * K. Potamianos <karolos.potamianos@cern.ch>
 * D. Boerner <daniela.boerner@cern.ch>
 * Date: 2015-II-5
 *
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "GeneralResults.h"
#include "Command.h"
#ifdef __XMK__
#include "Fei4.h"
#endif
#include <vector>

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class ReconfigModules: public CommandCRTP<ReconfigModules > {
public:
  InstantiateResult(SuccessResult)

	enum ConfigType { Physics, Calib };

	// These functions return the modules that have NOT been succesfully reconfigured
	virtual uint32_t reconfigure(uint32_t reconfigMask);

	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	static uint32_t augmentMask(uint32_t mask);
	static void disableFMTLinks(uint32_t mask, uint32_t waitingForECR=0x0);
	static void enableFMTLinks(uint32_t mask, uint32_t waitingForECR=0x0, bool writeRegAtECR=true);
	static uint32_t checkFMTLinks();

	ReconfigModules();
	virtual ~ReconfigModules() {}

	void setRxMask(uint32_t rxMask){ m_rxMask = rxMask; }
	void setWaitingForECR(uint32_t waitingForECR){ m_waitingForECR = waitingForECR; }
	void setPreAmpsOn(bool preAmpsOn){ m_preAmpsOn = preAmpsOn; }
	void setForcePreAmpsStatus(bool forcePreAmpsStatus){ m_forcePreAmpsStatus = forcePreAmpsStatus; }
	void setSleepDelay(uint32_t delay){m_sleepDelay=delay;}
	void setWriteRegAtECR(bool flag){m_writeRegAtECR=flag;}
	uint32_t getNotReconfiguredRxMask(){return m_notReconfigured;}
	uint32_t getWaitingForECRRxMask(){return m_rxMask & ~m_notReconfigured;}
protected:

private:
	ConfigType m_ConfigType;
	uint32_t m_rxMask;
	uint32_t m_waitingForECR;
	bool m_forcePreAmpsStatus;
	bool m_preAmpsOn;
	uint32_t m_sleepDelay;
	uint32_t m_notReconfigured;
	bool m_writeRegAtECR;
};
#endif //_RECONFIG_MODULES_H_

