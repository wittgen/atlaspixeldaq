#ifndef _SENDMODULECONFIG_H_
#define _SENDMODULECONFIG_H_
/*
 * Author: 
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Nov-25
 *
 * This command is used to send the configuration of modules from the ROD
 * to the front end
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Serializer.h"
#include "Command.h"
#include <vector>

// Todo: check that theconfig was correctly sent?
class SentModuleConfig: public CommandCRTP<SentModuleConfig > {
 public:
	bool success;
	uint32_t enabledMask;
	uint32_t readBackOKMask;
	uint32_t readBackBeforeSending;
	
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class SendModuleConfig: public CommandCRTP<SendModuleConfig > {
public:
	InstantiateResult(SentModuleConfig)

	enum ConfigType { Physics, Calib };

virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	SendModuleConfig();
	virtual ~SendModuleConfig() {}

	void sendModuleConfigFei3();
	void sendModuleConfigFei4();

	void setPhysicsConfig(){ m_ConfigType = Physics;};
	void setCalibConfig(){ m_ConfigType = Calib;};
	void setVerbose(bool yn = true) {verbose = yn;};
	void setModMask(uint32_t mask) {m_modMask = mask;};
	void setRxMask(uint32_t mask);
	void setSendAll(bool yn = true) {m_sendAll = yn;};
	void turnOnPreAmps() {m_preAmpsOn = true;};
	void turnOffPreAmps() {m_preAmpsOn = false;};
	void readBackSR(bool yn = true) {m_readBackSR = yn;};
	void readBackGlobal(bool yn = true) {m_readBackGlobal = yn;};
	void sendConfigAtECR(bool yn = true) {m_sendConfigAtECR = yn;};
	void setEnaMaskStandby(uint32_t mask) {m_maskPixelEna = mask;};

	void setSleepDelay (uint32_t delay) {m_sleepDelay = delay;};

	bool m_readBackSR;
	std::map<uint32_t, std::vector<uint32_t > > m_GRreadback;
	
	uint32_t getNotConfiguredRxMask();
protected:

private:
	ConfigType m_ConfigType;
	bool verbose;
	uint32_t m_modMask;
	bool m_sendAll;
	bool m_preAmpsOn;
	uint32_t m_sleepDelay;
	bool m_maxGlobalThreshold;
	bool m_sendConfigAtECR;
	bool m_readBackGlobal;
	uint32_t m_maskPixelEna;
};
#endif //_SENDMODULECONFIG_H_

