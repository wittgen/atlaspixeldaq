#ifndef _ABORTSCAN_
#define _ABORTSCAN_
/*
 * Author: 
 * N. Dreyer <dreyern@cu.washington.edu>
 * Date: 2015-Sep-16
 *
 * This command tries to abort a scan more cleanly than by simply killing the StartScan thread
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class AbortScan: public CommandCRTP<AbortScan > {
 public:
  InstantiateResult(SuccessResult)

  AbortScan();
  AbortScan(uint16_t execId, uint32_t scanId);
  virtual ~AbortScan() {}

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  uint16_t targetExecId;
  uint32_t targetScanId;
};

#endif //_ABORTSCAN_

