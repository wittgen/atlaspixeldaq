#ifndef _PIXSCANBASETOROD_H_
#define _PIXSCANBASETOROD_H_
// Steve Alkire <alkire@cern.ch> 2014.02.10
// Ships a PixScanBase to the Rod and places it in ScanBoss

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "SerializablePixScanBase.h"
#include <string>

#ifdef __XMK__
#include "Fei4.h"
#endif

class WrotePixScanBase: public CommandCRTP<WrotePixScanBase > {
 public:
	bool success;
	
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class PixScanBaseToRod: public CommandCRTP<PixScanBaseToRod > {
public:
  InstantiateResult(WrotePixScanBase)
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}
	
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;
	
	PixScanBaseToRod();  
	virtual ~PixScanBaseToRod() {}
	void setPixScanBase(SerializablePixScanBase sb){m_scanBase = sb;}
	
 protected:
	
 private:
	SerializablePixScanBase m_scanBase;
};
#endif //# _PIXSCANBASETOROD_H_

