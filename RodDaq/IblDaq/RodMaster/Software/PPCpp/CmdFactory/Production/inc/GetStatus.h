#ifndef _GETSTATUS_H_
#define _GETSTATUS_H_
// Steve <alkire@cern.ch>
// 2014.03.10
// Requests the return of an object.

#include "CppCompatibility.h" // Keep this header first!
#include <string>
#include "Command.h"
#include "Serializer.h"
#include "IblRodScanStatus.h"

#ifdef __XMK__
#include "Fei4.h"
#endif

class ReturnStatus: public CommandCRTP<ReturnStatus > {
 private:
 public:	
  virtual ~ReturnStatus() {}
  IblRodScanStatus m_status;
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};


class GetStatus: public CommandCRTP<GetStatus > {
 
 public:
  InstantiateResult(ReturnStatus)
  //And others in the future

  virtual void execute();
  virtual const Command* getResult() const {
    return &result;
  }
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  uint32_t targetUsingFei3;

  GetStatus(uint32_t usingFei3 = 0);  
  virtual ~GetStatus() {}  
};
#endif //_GETSTATUS_H_

