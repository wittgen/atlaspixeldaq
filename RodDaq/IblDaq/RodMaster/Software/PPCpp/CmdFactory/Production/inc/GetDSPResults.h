/*
 * * JuanAn Garcia
 * Date: 20-Nov-2018
 *
 * Get DSP results back
 */

#ifndef _GET_DSP_RESULTS_H_
#define _GET_DSP_RESULTS_H_

#include "CppCompatibility.h" // Keep this header first!
#include "Serializer.h"
#include "Command.h"
#include <vector>

class DSPScanResults: public CommandCRTP<DSPScanResults > {
 public:
        virtual ~DSPScanResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        // Results returned by this command     
	std::vector< std::vector< uint32_t > > dspResults;
};

class GetDSPResults: public CommandCRTP<GetDSPResults > {

 public:
  InstantiateResult(DSPScanResults)

  virtual const Command* getResult() const {
    return &result;
  }

  GetDSPResults();  
  virtual ~GetDSPResults() {}  

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  uint8_t rxCh;

};
#endif //_GET_DSP_RESULTS_H_

