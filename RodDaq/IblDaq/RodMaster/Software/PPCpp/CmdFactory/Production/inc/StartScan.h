#ifndef _STARTSCAN_H_
#define _STARTSCAN_H_
/*
 * Author: Russell Smith
 * Date: 2014-Feb-26
 */
//Steve Alkire <alkire@cern.ch> 2014.03.13

#include "CppCompatibility.h" // Keep this header first!
#include "Serializer.h"
#include "Command.h"
#include "ScanBoss.h"

#ifndef __DO_IBL_GET_STATUS__
class ScanResults: public CommandCRTP<ScanResults > {
 private:
 public:

  IblRodScanStatus m_scanResult;

  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};
#endif

class StartScan: public CommandCRTP<StartScan > {
// class StartScan: public CommandCRTP<StartScan> {
 public:
#ifndef __DO_IBL_GET_STATUS__
  InstantiateResult(ScanResults)
#endif
  enum ScanDebug_t{_NormalScan, _TestRegisters};//add whatever test scan you want. Default is normal scan operation.
  StartScan();
  StartScan(ScanDebug_t ts);
  virtual void execute();
#ifndef __DO_IBL_GET_STATUS__
  virtual const Command* getResult() const {
    return &result;
  }
#endif
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  virtual ~StartScan() {}

  uint32_t channels;
  uint32_t scanId;
  uint32_t fitPorts[4];
  uint8_t fitIps[4][4];
  
 private:
  ScanDebug_t m_scanDebug;
};
#endif //_START_SCAN_

