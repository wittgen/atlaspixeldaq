#ifndef _TESTRESULTSIZE_H_
#define _TESTRESULTSIZE_H_
/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 17-Feb-2014
 *
 * A test command to see how big the return size can be before the ppc crashes
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#define ARRAY_SIZE 1000

class TestResult: public CommandCRTP<TestResult > { // declares type of command result
 public:
  // All the data we want back from the command
  uint8_t myArray[ARRAY_SIZE];

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class TestResultSize: public CommandCRTP<TestResultSize > { // declares type of command
public:
	typedef TestResult ResultType;
	ResultType result;
	TestResultSize();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	uint8_t dummy;

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~TestResultSize() {}

};
#endif //_TESTRESULTSIZE_H_

