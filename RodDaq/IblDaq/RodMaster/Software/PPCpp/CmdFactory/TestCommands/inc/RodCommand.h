#ifndef __ROD_COMMAND_H__
#define __ROD_COMMAND_H__

#include "Serializer.h"
#include "Command.h"
#include "stdint.h"
#include "Fei4Cfg.h"
#include "IblRodSlave.h"

#include <iostream>

#include <chrono>
#include <thread>



struct RodCommandResult: public CommandCRTP<RodCommandResult > {
	uint32_t value1;
	uint16_t value2;

	/// Inhertited functions
	void serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		Serializer<uint32_t>::serialize(out, offset, value1);
		Serializer<uint16_t>::serialize(out, offset, value2);
	}
	void deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		value1 = Serializer<uint32_t>::deserialize(in,offset);
		value2 = Serializer<uint16_t>::deserialize(in,offset);
	}
	uint32_t serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(uint32_t)+sizeof(uint16_t);
	}

	RodCommandResult() {}
	virtual ~RodCommandResult() {}
};

class RodCommand: public CommandCRTP<RodCommand > {
public:
	InstantiateResult(RodCommandResult)

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;


	RodCommand() { setResultPtr(); }
	virtual ~RodCommand() {}

protected:

private:

};

class NoWaitRodCommand: public CommandCRTP<NoWaitRodCommand > {
public:
	// Don't call InstantiateResult!
	virtual void execute() { 
#ifdef __XMK__
		std::cout << __PRETTY_FUNCTION__ << std::endl; 
		for(uint32_t i = 0 ; i < 20 ; ++i) {
			std::cout << "Iteration " << i << std::endl;
         std::this_thread::sleep_for(std::chrono::seconds(1));
		}
		std::cout << __PRETTY_FUNCTION__ << ": done!" << std::endl; 
#endif //__XMK__
	}
	/// Inhertited functions
	virtual void serialize(uint8_t *out) const { }
	virtual void deserialize(const uint8_t *in) { }
	virtual uint32_t serializedSize() const { return 0; }
protected:

private:

};
#endif // __ROD_COMMAND_H__

