#ifndef _TESTBENCHCOMMAND_H_
#define _TESTBENCHCOMMAND_H_
// Steve <alkire@cern.ch>
// 2014.02.10
// Testing Serializable Functionality.

#include "CppCompatibility.h" // Keep this header first!
#include <string>
#include "Command.h"
#include "Status.h"
#include "PixScanBase.h"
#include "Serializer.h"
#include "EasySerializable.h"

#ifdef __XMK__
#include "Fei4.h"
#endif

template<uint8_t N>
class TestSerialize2: public EasySerializable{
 public:
  TestSerialize2(){}
  virtual ~TestSerialize2(){}
  uint32_t testBit;
  uint32_t testBits[N];
  void printN(){
    std::cout << "TestBits:";
    for(uint8_t i = 0; i < N; i++){
      std::cout << " " << testBits[i];
    }
    std::cout <<std::endl;
  }

  SERIAL_H(
       prep(testBit);
       prep(testBits,N);
       )
};

class TestSerialize: public EasySerializable{
 public:
  TestSerialize(){}
  virtual ~TestSerialize(){}
  TestSerialize2<4> testSerialize;
  virtual void serial();
  virtual void serial() const;
};

class TestBenchReturn: public CommandCRTP<TestBenchReturn > {
 public:
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

class TestBenchCommand: public CommandCRTP<TestBenchCommand > {
public:
  InstantiateResult(TestBenchReturn)
  virtual void execute();
  virtual const Command* getResult() const {
    return &result;
  }
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  TestBenchCommand();
  virtual ~TestBenchCommand() {}
  
  TestSerialize m_ts;
};
#endif //_TESTBENCHCOMMAND_H_

