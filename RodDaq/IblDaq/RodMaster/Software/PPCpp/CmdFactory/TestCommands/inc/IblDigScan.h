#ifndef _IBLDIGSCAN_H
#define _IBLDIGSCAN_H
/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 *          L. Jeanty <laura.jeanty@cern.ch>
 * Date: 9-X-2013
 * Description: Basic IBL digital scan to show feature usage
 */

// Inclusion necessary for command definitions
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif

// This result 'type' will probably be used so often that 
// it may be justified for it to have its own header.
class IblModuleMap: public CommandCRTP<IblModuleMap > {
public:
  uint8_t ToT[4][336];
  virtual ~IblModuleMap() {}
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;

  bool success;

};

class IblDigScan: public CommandCRTP<IblDigScan > {
//class IblDigScan: public CommandCRTP<IblDigScan> public CommandBase {
public:
        InstantiateResult(IblModuleMap)
	  
	  uint32_t Channels;

	// Members are public for ease of access! Todo: encapsulate when needed
	//Scan options
	uint8_t ToT;
	uint8_t ToTMaskStep;
	uint8_t RowSeparation; // Todo: Make ENUM

	uint8_t TrigDelay;
	uint8_t TrigCount;

	uint8_t HitDiscCnfg;

	uint8_t MaskLoop;
	uint8_t DCLoop;
	uint8_t StartCol;
	uint8_t TrigLoop ;
	uint8_t DoubleColumnMode ;

	//Other options
	bool readBackFromBoc;
	bool readBackFromInmem;
	bool readBackFromSlave;
	bool talkToFitFarm;

	uint32_t histogrammerMode;

	void serialize(uint8_t *out) const;
        void deserialize(const uint8_t *in);
        uint32_t serializedSize() const;
        IblDigScan();
	void execute(); // Todo: make int ?
        virtual ~IblDigScan() {}

protected:
private:
};
#endif // _IBLDIGSCAN_H

