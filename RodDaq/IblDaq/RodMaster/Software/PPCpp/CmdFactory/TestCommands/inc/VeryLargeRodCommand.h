#ifndef __VERY_LARGE_ROD_COMMAND_H__
#define __VERY_LARGE_ROD_COMMAND_H__

#include "Serializer.h"
#include "Command.h"
#include "stdint.h"
#include "Fei4Cfg.h"
#include "IblRodSlave.h"

#include <iostream>
#include <algorithm>
#include <tr1/functional>


struct VeryLargeRodCommandResult: public CommandCRTP<VeryLargeRodCommandResult > {
	uint32_t large_array[512*1024]; // 2MB should be sufficient enough!

	/// Inhertited functions
	void serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		//std::for_each( large_array, large_array+sizeof(large_array)/sizeof(uint32_t), 
		//	std::tr1::bind( &Serializer<uint32_t>::serialize, out, offset, std::tr1::placeholders::_1 ) );
		for( size_t i = 0 ; i < sizeof(large_array)/sizeof(uint32_t) ; ++i )
			Serializer<uint32_t>::serialize(out, offset, large_array[i]);
	}

	void deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		//std::generate( large_array, large_array+sizeof(large_array)/sizeof(uint32_t),
		//	std::tr1::bind( (uint32_t(*)(const uint8_t*, uint32_t&))Serializer<uint32_t>::deserialize, in, offset) );
		for( size_t i = 0 ; i < sizeof(large_array)/sizeof(uint32_t) ; ++i )
			Serializer<uint32_t>::deserialize(in, offset, large_array[i]);
	}

	uint32_t serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(large_array);
	}

	VeryLargeRodCommandResult() {}
	virtual ~VeryLargeRodCommandResult() {}
};

class VeryLargeRodCommand: public CommandCRTP<VeryLargeRodCommand > {
public:
	InstantiateResult(VeryLargeRodCommandResult)
	uint32_t large_array[sizeof(((VeryLargeRodCommandResult*)0)->large_array)/sizeof(uint32_t)]; // Same width as result

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;


	VeryLargeRodCommand() { setResultPtr(); }
	virtual ~VeryLargeRodCommand() {}

protected:

private:

};

#endif // __VERY_LARGE_ROD_COMMAND_H__

