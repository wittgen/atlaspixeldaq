#ifndef _IBLTHRESHSCAN_H_
#define _IBLTHRESHSCAN_H_
/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 *          L. Jeanty <laura.jeanty@cern.ch>
 * Date: 9-X-2013
 * Description: Basic IBL digital scan to show feature usage
 */

// Inclusion necessary for command definitions
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "threshOcc.h"
#include <vector>
#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif
using std::vector;
/*struct threshOcc {
    uint8_t arr[4][336];
};*/
// This result 'type' will probably be used so often that
// it may be justified for it to have its own header.
class TIblModuleMap: public CommandCRTP<TIblModuleMap > {
public:
    //uint8_t Threshold[4][336];
    std::vector<threshOcc> Threshold;
    uint8_t ToT[4][336];
    virtual ~TIblModuleMap() {}
    
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;

    bool success;
    
    bool verbose;
};

class IblThreshScan: public CommandCRTP<IblThreshScan > {
    //class IblThreshScan: public CommandCRTP<IblThreshScan> public CommandBase {
public:
    InstantiateResult(TIblModuleMap)
    
    uint32_t Channels;
    
	// Members are public for ease of access! Todo: encapsulate when needed
	//Scan options
	uint8_t ToT;
	uint8_t ToTMaskStep;
	uint8_t RowSeparation; // Todo: Make ENUM
	uint8_t TrigDelay;
	uint8_t TrigCount;
    	uint16_t VCalRange;
	uint8_t VCal_Steps;
	uint8_t VCal_StepSize;
	uint8_t HitDiscCnfg;
    
	uint8_t MaskLoop;
	uint8_t DCLoop;
	uint8_t StartCol;
	uint8_t TrigLoop ;
	uint8_t DoubleColumnMode ;
    
	//Other options
	bool readBackFromBoc;
	bool readBackFromInmem;
	bool readBackFromSlave;
	bool talkToFitFarm;
    
	uint32_t histogrammerMode;
    
	void serialize(uint8_t *out) const;
    void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;
    IblThreshScan();
	void execute(); // Todo: make int ?
    virtual ~IblThreshScan() {}
    
	bool verbose;
	
protected:
private:
};
#endif //_IBLTHRESHSCAN_H_

