#include "IblThreshScan.h"
#include "Serializer.h"
#include "IblRodSlave.h"

#ifdef __XMK__
#include "IblBoc.h"
#include "IblRodInmemFifo.h"
#include "RodResourceManager.h"
#include <chrono>
#include <thread>
#include <arpa/inet.h>
#endif

IblThreshScan::IblThreshScan() {
    
    verbose = false;
    
    setResultPtr();
    result.verbose = verbose;
    
    //Default values for scan parameters
    
    // Channels to scan
    Channels = 0x00010000;
    // ToT of each Thresh hit; used to set CalPulse
    ToT = 9;
    // ToT increase for each mask step
    ToTMaskStep = 1;
    // Row separation for mask
    RowSeparation = 8;
    TrigDelay = 16;
    TrigCount = 15; // what should this be?
    // VCal Range: max possible value
    VCalRange = 500;
    VCal_Steps = 1;
    // VCal Steps
    
    VCal_StepSize = VCalRange/VCal_Steps;
    
    HitDiscCnfg = 2;
    
    // Number of mask steps
    MaskLoop = 8;
    // Number of double columns to scan over
    DCLoop = 1;
    // double column to begin with
    StartCol = 0;
    //Number of triggers to send
    TrigLoop = 1;
    // Double column mode
    // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
    DoubleColumnMode = 0x0;
    
    readBackFromBoc = true;
    readBackFromInmem = false;
    readBackFromSlave = true;
    talkToFitFarm = false;
    
    histogrammerMode = SHORT_TOT;
    result.success = false;
    
}


void IblThreshScan::execute() {
    
#ifdef __XMK__
    
    // *****************************
    // Initialize Slave for Scan
    // *****************************
    
    IblRodSlave s;
    int slave = 1;
    s.setUart(slave+1);
    s.setId(slave);
    s.setVerbose(slave,0);
    
    // enable the Rx link going to the formatter
    s.setUart(0);
    //    s.enableRxLink(1,0x0000FFFF);
    s.enableRxLink(1,0x1);
    //    s.enableRxLink(0, 0x0000FFFF);
    s.setUart(slave+1);
    
    // s.testHisto();
    
    //  std::cout<<"tested histo..."<<std::endl;
    
    int ntrigger = TrigLoop;
    
    IblSlvNetCfg netCfgOut, netCfgIn;
    unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x03}; // 3 instead of 4??
    unsigned char lIp[4] = {192, 168, 1, 81}; // 81 for pc 1, 151 for pc 2
    unsigned char maskIp[4] = {255,255, 0,0};
    unsigned char gwIp[4] = {192, 168, 0, 100};
    unsigned char tIp[4] = {192, 168, 0, 100};
    uint32_t tP = 5002;
    
    IblRodSlave::createMacAddress(mac[2], mac[3], mac[4], mac[5], mac[6], mac[7], netCfgOut.localMac);
    IblRodSlave::createIp4Address(lIp[0], lIp[1], lIp[2], lIp[3], &netCfgOut.localIp);
    IblRodSlave::createIp4Address(maskIp[0], maskIp[1], maskIp[2], maskIp[3], &netCfgOut.maskIp);
    IblRodSlave::createIp4Address(gwIp[0], gwIp[1], gwIp[2], gwIp[3], &netCfgOut.gwIp);
      
    IblRodSlave::createIp4Address(tIp[0], tIp[1], tIp[2], tIp[3], &netCfgOut.targetIpA);
    netCfgOut.targetPortA = htonl(tP);
    
    ////////////////////////////////////
    if (talkToFitFarm)  s.setNetCfg(&netCfgOut,slave);
    //////////////////////////////////
    if (talkToFitFarm)  std::cout << "----setNetCfg" << std::endl;
    if (talkToFitFarm)  netCfgIn = s.getNetCfg(slave);

    IblSlvHistCfg histCfgOut;
    histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
    histCfgOut.cfg[0].enable = 1;     //enable histo0 unit
    histCfgOut.cfg[0].type = histogrammerMode;
    if (MaskLoop == 1) histCfgOut.cfg[0].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
    else if (MaskLoop == 8) histCfgOut.cfg[0].maskStep = 3;   //0 for single chip(or 1 if divide row by 2)
    histCfgOut.cfg[0].chipSel = 0;    //0 for single chip(1 for all chips)
    histCfgOut.cfg[0].addrRange = 0;  //single chip <32K( full range < 1M)
    histCfgOut.cfg[0].scanId = 0x1234;//remote use only
    histCfgOut.cfg[0].binId = 1;
    histCfgOut.cfg[0].expectedOccValue = ntrigger;
    
    // histCfgOut.cfg[1] = histogrammer 0;
    // histCfgOut.cfg[1] = histogrammer 1;
    
    //    histCfgIn = s.getHistCfg(slave);
    
    s.setUart(0); // back to ppc
    
    // *****************************
    // Initialize Scan Variables
    // *****************************
    
    // initialize scan results
    uint8_t trash[4][336] = {{0}};

  uint32_t i = 0;
  for (; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
    if (Channels & (1<<i)) break;
  }
  Fei4 & fei4 = RodResourceManagerIBL::getCalibModuleConfig(i);   
  
  fei4.setVerbose(verbose);
  Fei4Data::setRawDataDump(false);
  
  if (verbose) { std::cout<<"executing Ibl Threshold Scan on chip "<<fei4.getChipID()<<", yahoo"<<std::endl;
        std::cout<<"By the way, I'm in verbose mode. "<<std::endl; }
    
    std::cout<<"--------------------------------"<<std::endl;
    std::cout<<"--------------------------------"<<std::endl;
    std::cout<<"Scan variables are: "<<std::endl;
    std::cout<<"ToT = "<< (int)ToT<<std::endl;
    std::cout<<"ToTMaskStep = "<< (int)ToTMaskStep<<std::endl;
    std::cout<<"RowSeperation = "<< (int)RowSeparation<<std::endl;
    std::cout<<"TrigDelay = "<< (int)TrigDelay<<std::endl;
    std::cout<<"TrigCount = "<< (int)TrigCount<<std::endl;
    std::cout<<"HitDiscCnfg = "<< (int)HitDiscCnfg<<std::endl;
    std::cout<<"MaskLoop = "<< (int)MaskLoop<<std::endl;
    std::cout<<"DCLoop = "<< (int)DCLoop<<std::endl;
    std::cout<<"StartCol = "<< (int)StartCol<<std::endl;
    std::cout<<"VCal_Step = "<< (int)VCal_Steps<<std::endl;
    std::cout<<"VCalRange = "<< (int)VCalRange<<std::endl;
    std::cout<<"TrigLoop = "<< (int)TrigLoop <<std::endl;
    std::cout<<"DoubleColumnMode = "<< (int)DoubleColumnMode <<std::endl;
    std::cout<<"histogrammerMode = "<< (int)histogrammerMode <<std::endl;
    std::cout<<"readBackFromBoc = "<< (int)readBackFromBoc <<std::endl;
    std::cout<<"readBackFromInmem = "<< (int)readBackFromInmem <<std::endl;
    std::cout<<"readBackFromSlave = "<< (int)readBackFromSlave <<std::endl;
    std::cout<<"talkToFitFarm = "<< (int)talkToFitFarm <<std::endl;
    std::cout<<"--------------------------------"<<std::endl;
    std::cout<<"--------------------------------"<<std::endl;
    
    
    // *****************************
    // Initialize ROD for Scan
    // *****************************
    
  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  if( !iblBocLink->enableChannels(Channels) ) return; 
    
    // tmp!!!!
    // this shouldn't be neccesary, but for the moment
    // you have to enable all 4 Rx channels on the BOC
    // to get data in the histogrammer;
    // after enabling once, you can then only enable the
    // channel that you want to use
    
    IblBoc::write(1, 0, IblBoc::RxControl, 0x0);
    IblBoc::write(1, 1, IblBoc::RxControl, 0x0);
    IblBoc::write(1, 2, IblBoc::RxControl, 0x0);
    IblBoc::write(1, 3, IblBoc::RxControl, 0x0);
    
    IblBoc::write(1, 0, IblBoc::RxControl, 0x7); // HARD CODED 
    //     IblBoc::write(1, 1, IblBoc::RxControl, 0x7);
    //      IblBoc::write(1, 2, IblBoc::RxControl, 0x7);
    //      IblBoc::write(1, 3, IblBoc::RxControl, 0x7);
    
    
    IblRodInmemFifo fifo;
    fifo.selectGathererFromRxCh(16); // hard-coded!
    fifo.setRxCh(16); // hard-coded!
    fifo.reset();
    
    // *****************************
    // Initialize Fei4 for Scan
    // *****************************
    
    // send the default fei4 configuration
    fei4.sendGlobalConfigAmpsOn();
    
    // set into config mode in order to configure the chip
    fei4.setRunMode(false);


    
    // HitDiscCnfg
    fei4.writeRegister(&Fei4::HD, HitDiscCnfg);
    // Trigger
    fei4.writeRegister(&Fei4::Trig_Count, TrigCount);
    // Maxi. Trigger Latency
    fei4.writeRegister(&Fei4::Trig_Lat, 255 - 8 - TrigDelay - (TrigCount/2));
    // Preampliflier default settings
    fei4.writeRegister(&Fei4::PrmpVbp, 43);
    fei4.writeRegister(&Fei4::PrmpVbp_L, 43);
    fei4.writeRegister(&Fei4::PrmpVbp_R, 43);
    fei4.writeRegister(&Fei4::PrmpVbnFol, 106);
    fei4.writeRegister(&Fei4::PrmpVbpf, 100);
    fei4.writeRegister(&Fei4::PrmpVbnLCC, 0);
    // AMP2 default settings
    fei4.writeRegister(&Fei4::Amp2Vbn, 79);
    fei4.writeRegister(&Fei4::Amp2Vbp, 85);
    fei4.writeRegister(&Fei4::Amp2VbpFol, 26);
    fei4.writeRegister(&Fei4::Amp2Vbpff, 50);
    fei4.writeRegister(&Fei4::DisVbn, 26);
    fei4.writeRegister(&Fei4::TDACVbp, 255);
    fei4.writeRegister(&Fei4::FDACVbn, 50);
    fei4.writeRegister(&Fei4::GADCVref, 170); // n/a as suggested value
    fei4.writeRegister(&Fei4::GADCCompBias, 100);
    fei4.writeRegister(&Fei4::PlsrDACbias, 96);
    fei4.writeRegister(&Fei4::PlsrDAC, 800); //default high value
    fei4.writeRegister(&Fei4::PlsrDelay, 2);
    fei4.writeRegister(&Fei4::PlsrIDACRamp, 180);
    fei4.writeRegister(&Fei4::PlsrRiseUpTau, 7);
    fei4.writeRegister(&Fei4::PlsrVgOpAmp, 255);
    fei4.writeRegister(&Fei4::BufVgOpAmp, 170);
    fei4.writeRegister(&Fei4::LVDSDrvIref, 171);
    fei4.writeRegister(&Fei4::LVDSDrvVos, 105);
    fei4.writeRegister(&Fei4::PllIbias, 88);
    fei4.writeRegister(&Fei4::PllIcp, 28);

    
    if (verbose) std::cout << "finished initializing scan; about to begin mask stage " << std::endl;

    // *****************************
    // Loop over Mask
    // *****************************
    for(uint32_t iMask = 0 ; iMask < MaskLoop ; ++iMask) {
            if (verbose) std::cout<<" beginning mask stage: "<<iMask<<" out of "<<int(MaskLoop)<<" total loops"<<std::endl;
            
            fei4.setRunMode(false);

            // Must set CP to 0x3 to write the mask to the full chip!
            
            // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
            fei4.writeRegister(&Fei4::CP,0x3);
            fei4.writeRegister(&Fei4::Colpr_Addr, 0x0);    	    
            uint32_t pixelMask[21];
	    for(int i=0; i<21; i++) pixelMask[i] = 0x01010101<<iMask; //convert the mask from iMaskDec
            fei4.wrFrontEnd(pixelMask);
	    fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x0c1);
            fei4.writeRegister(&Fei4::LEN,0x1);
	    fei4.writeRegister(&Fei4::SRK,0x0); // Set SR Clock back to 0 after shifting
	    fei4.globalPulse(10);  
	    fei4.writeRegister(&Fei4::LEN,0x0);
	    fei4.writeRegister(&Fei4::S0,0x0);
    	    fei4.writeRegister(&Fei4::S1,0x0);
            fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x0);

    fei4.writeRegister(&Fei4::DHS,0x0);
            

            
            uint8_t CalPulse = ToT - 1;
	    


            //loop over the VCal parameters
            for (uint8_t iVCal=0; iVCal<VCal_Steps; iVCal++) {
              fei4.setRunMode(false);

      	      fei4.writeRegister(&Fei4::CMDcnt12,CalPulse);
	      histCfgOut.cfg[0].binId = iVCal;
	      std::cout<<"histCfgOut.cfg[0] = "<<int(histCfgOut.cfg[0].binId)<<std::endl;

	      //////////////////////////////////////////////
	      s.startHisto(&histCfgOut,slave);
	      /////////////////////////////////////////////////
	      std::cout<<"STARTED HISTO on bin: "<<int(iVCal)<<std::endl;
                // loop over double columns
                // colAddr = 0 --> column 1
                // colAddr = 39 --> volumns 78, 79, 80
                // colAddres = n --> columns 2n and 2n+1
                for(uint32_t colAddr = StartCol ; colAddr < StartCol + DCLoop ; colAddr++) {
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                if (verbose) std::cout<<"*******STARTING DC LOOP with address: "<<colAddr<<std::endl;
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                if (verbose) std::cout<<"***************************************** "<<colAddr<<std::endl;
                fei4.setRunMode(false);
                
                // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
                if (verbose) std::cout<<"setting DC mode: 0x"<<std::hex<<int(DoubleColumnMode)<<
                    ", and colAddr = 0x"<<int(colAddr)<<std::dec<<std::endl;
		fei4.writeRegister(&Fei4::CP,DoubleColumnMode);
                fei4.writeRegister(&Fei4::Colpr_Addr,colAddr);//DoubleColumnMode);
                // Column Address
                //fei4.writeRegister(&Fei4::Colpr_Addr, 0x1);
		//fei4.writeRegister(&Fei4::PlsrDAC,); // sets height of analog pulse
		fei4.writeRegister(&Fei4::PlsrDAC,iVCal*VCal_StepSize); // sets height of analog pulse
		fei4.writeRegister(&Fei4::PlsrDelay,2);
                fei4.writeRegister(&Fei4::CMDcnt12,CalPulse); // sets width of pulse --> ToT
	        fei4.writeRegister(&Fei4::M13,0x0);           // sets the 13th bit of CMDcnt
               	fei4.writeRegister(&Fei4::Vthin_Coarse, 1);  
	        fei4.writeRegister(&Fei4::Vthin_Fine, 46);  
                fei4.setRunMode(true);
                
                for(uint32_t iTrig = 0 ; iTrig < TrigLoop ; ++iTrig) {
		  //                    if (verbose) std::cout<<"iTrig = "<<int(iTrig)<<std::endl;
                    fei4.calTrigger(TrigDelay);
                    for(uint8_t iTrigCount = 0 ; iTrigCount < TrigCount ; ++iTrigCount) {
		      //                        if (verbose) std::cout<<"iTrigCount = "<<int(iTrigCount)<<std::endl;
		      //                        usleep(500);
                        
			//                        std::cout<<"about to read frame: "<<int(iTrigCount)<<std::endl;
			if (iTrigCount < 32 && iTrig < 2 && iVCal < 10 && colAddr < 2 ) { // only read out the first few frames
			  std::vector<int> answer;
			  if (readBackFromBoc|| readBackFromInmem) answer = fei4.readFrame();
        
//                         std::cout<<"**********************************"<<std::endl;
//                         std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo, ANSWER is: "<<std::endl;
//                         std::cout<<"**********************************"<<std::endl;
                        unsigned int answerSize = answer.size();
			  for (unsigned int i = 0; i < answerSize; i++) {
                            if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<answer[i];
                            else if (i%3 == 1) std::cout<<std::hex<<answer[i];
                            else std::cout<<std::hex<<answer[i]<<std::dec<<std::endl;
			  }
			  Fei4Data::fillPixDataArray(answer, fei4.getValue(&Fei4Cfg::HD), trash, StartCol);
			} // read back a few frames
		    } // iTrigCount
		} // iTrig
            	} // colAddr
		
    //////////////////////////////////////////////
    s.stopHisto(slave);
    //////////////////////////////////////////////
    std::cout << "----stop histo" << std::endl;
            } // iVcal
        } // iMask;
    
    
    // Reset to default values:
    
    fei4.setRunMode(false);
    fei4.writeRegister(&Fei4::CP, 0x3);
    fei4.writeRegister(&Fei4::Colpr_Addr, 0x0);
    fei4.writeRegister(&Fei4::HD, 0x0);
    fei4.writeRegister(&Fei4::Trig_Lat, 0x210);
    fei4.writeRegister(&Fei4::Trig_Count, 0x1);
    
    fei4.setRunMode(true);
    
    // copy data to command result
    std::copy(trash[0], trash[0]+1344, (result.ToT)[0]);
//    std::copy(threshold.begin(),threshold.end(),result.Threshold.begin());

    
    // wait?
    std::this_thread::sleep_for(std::chrono::milliseconds(100));

    
    
    
    // *****************************
    // Stop Histogrammer
    // *****************************
    
    if (readBackFromSlave) s.setUart(slave+1);
    
    
    // see that we got something, debugging
    uint32_t histoOutput[6*80] = {};
    
    
    if (readBackFromSlave) {
        s.getHisto(slave, 0, histoOutput);
        s.setUart(0);
        for(int i = 0; i < 6*80; i++){
            std::cout << "--getHisto-data--" << i << "--- " << histoOutput[i] << std::endl;
        }
        std::cout << "----get histo" << std::endl;
    }
    s.setUart(0);
    
    result.success = true;
    
    // *****************************
    
    if (verbose) std::cout<<"Done with executing the Threshold Scan! Hopefully it worked"<<std::endl;
#else
    std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
    
}

void IblThreshScan::serialize(uint8_t *out) const
{
    if (verbose) std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    uint32_t offset = 0;
    Serializer<uint32_t>::serialize(out,offset,Channels);
    Serializer<uint8_t>::serialize(out,offset,ToT);
    Serializer<uint8_t>::serialize(out,offset,ToTMaskStep);
    Serializer<uint8_t>::serialize(out,offset,RowSeparation);
    Serializer<uint8_t>::serialize(out,offset,TrigDelay);
    Serializer<uint8_t>::serialize(out,offset,TrigCount);
    Serializer<uint8_t>::serialize(out,offset,HitDiscCnfg);
    Serializer<uint8_t>::serialize(out,offset,MaskLoop);
    Serializer<uint8_t>::serialize(out,offset,DCLoop);
    Serializer<uint8_t>::serialize(out,offset,StartCol);
    Serializer<uint8_t>::serialize(out,offset,VCal_Steps);
    Serializer<uint16_t>::serialize(out,offset,VCalRange);
    Serializer<uint8_t>::serialize(out,offset,TrigLoop);
    Serializer<uint8_t>::serialize(out,offset,DoubleColumnMode);
    Serializer<bool>::serialize(out,offset,readBackFromBoc);
    Serializer<bool>::serialize(out,offset,readBackFromInmem);
    Serializer<bool>::serialize(out,offset,readBackFromSlave);
    Serializer<bool>::serialize(out,offset,talkToFitFarm);
    Serializer<uint32_t>::serialize(out,offset,histogrammerMode);
    
}

void IblThreshScan::deserialize(const uint8_t *in)
{
    if (verbose)  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    uint32_t offset = 0;
    Channels = Serializer<uint32_t>::deserialize(in,offset);
    ToT = Serializer<uint8_t>::deserialize(in,offset);
    ToTMaskStep = Serializer<uint8_t>::deserialize(in,offset);
    RowSeparation = Serializer<uint8_t>::deserialize(in,offset);
    TrigDelay = Serializer<uint8_t>::deserialize(in,offset);
    TrigCount = Serializer<uint8_t>::deserialize(in,offset);
    HitDiscCnfg = Serializer<uint8_t>::deserialize(in,offset);
    MaskLoop = Serializer<uint8_t>::deserialize(in,offset);
    DCLoop = Serializer<uint8_t>::deserialize(in,offset);
    StartCol = Serializer<uint8_t>::deserialize(in,offset);
    VCal_Steps = Serializer<uint8_t>::deserialize(in,offset);
    VCalRange = Serializer<uint16_t>::deserialize(in,offset);
    TrigLoop = Serializer<uint8_t>::deserialize(in,offset);
    DoubleColumnMode = Serializer<uint8_t>::deserialize(in,offset);
    readBackFromBoc = Serializer<bool>::deserialize(in,offset);
    readBackFromInmem = Serializer<bool>::deserialize(in,offset);
    readBackFromSlave = Serializer<bool>::deserialize(in,offset);
    talkToFitFarm = Serializer<bool>::deserialize(in,offset);
    histogrammerMode = Serializer<uint32_t>::deserialize(in,offset);
}

uint32_t IblThreshScan::serializedSize() const
{
    if (verbose)  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    return (2*sizeof(uint32_t)+12*sizeof(uint8_t)+sizeof(uint16_t)+4*sizeof(bool));
}

void TIblModuleMap::serialize(uint8_t *out) const {
    if (verbose)  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    uint32_t offset = 0;
    Serializer<bool>::serialize(out, offset, success);
	//for(int k=0; k <32; k++){
		for (int i=0; i < 4; i++) {
        		for (int j=0; j < 336/*10*/; j++) {
        		    Serializer<uint8_t>::serialize(out, offset, ToT[i][j]);
        		} // loop over columns
    		} // loop over row
	//} //loop over Vcalstep
}

void TIblModuleMap::deserialize(const uint8_t *in) {
    uint32_t offset = 0;  
    if (verbose)  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    success = Serializer<bool>::deserialize(in, offset);
	//for(int k=0; k <32; k++){
		for (int i=0; i<4; i++) {
        		for (int j=0; j < 336/*10 proper Thresh*/; j++) { // not all the columns included to allow PPC-Host communication
				ToT[i][j] = Serializer<uint8_t>::deserialize(in, offset);
        		    //Threshold.at(k).arr[i][j] = Serializer<uint8_t>::deserialize(in, offset);
        		} // loop over columns
    		} // loop over row
	//} //loop over Vcalstep
}

uint32_t TIblModuleMap::serializedSize() const {
    if (verbose)  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
    return (sizeof(bool)+4*336*sizeof(uint8_t));
}
