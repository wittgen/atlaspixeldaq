#include "IblDigScan.h"
#include "Serializer.h"
#include "IblRodSlave.h"
#include "RodResourceManager.h"
#include "Fei4Proxy.h"
#ifdef __XMK__
#include "IblBoc.h"
#include "IblRodInmemFifo.h"
#include <chrono>
#include <thread>
#include <arpa/inet.h>
#endif

IblDigScan::IblDigScan() { 

  setResultPtr();

  //Default values for scan parameters

  // Channels to scan
  Channels = 0x00010000;
  // ToT of each digital hit; used to set CalPulse
  ToT = 9; 	
  // ToT increase for each mask step
  ToTMaskStep = 1; 
  // Row separation for mask 
  RowSeparation = 8;
  TrigDelay = 16;
  TrigCount = 3; // what should this be?

  HitDiscCnfg = 2;

  // Number of mask steps
  MaskLoop = 1;
  // Number of double columns to scan over
  DCLoop = 1;
  // double column to begin with
  StartCol = 0;
  //Number of triggers to send
  TrigLoop = 1;
  // Double column mode
  // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
  DoubleColumnMode = 0x0;

  readBackFromBoc = true;
  readBackFromInmem = false;
  readBackFromSlave = true;
  talkToFitFarm = false;

  histogrammerMode = SHORT_TOT;
  result.success = false;

  // Channels to scan
  Channels = 0x00010000; // 0x00010000 = channel Rx 0 on south slave
  result.success = false;
}


void IblDigScan::execute() {

#ifdef __XMK__

  // *****************************
  // Initialize Scan Variables
  // *****************************

  // initialize scan results
  uint8_t tot[4][336] = {{0}};
  
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"Starting IblDigScan"<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"Scan variables are: "<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"ToT = "<< (int)ToT<<std::endl;
  std::cout<<"ToTMaskStep = "<< (int)ToTMaskStep<<std::endl;
  std::cout<<"RowSeperation = "<< (int)RowSeparation<<std::endl; 
  std::cout<<"TrigDelay = "<< (int)TrigDelay<<std::endl;
  std::cout<<"TrigCount = "<< (int)TrigCount<<std::endl;
  std::cout<<"HitDiscCnfg = "<< (int)HitDiscCnfg<<std::endl;
  std::cout<<"MaskLoop = "<< (int)MaskLoop<<std::endl;
  std::cout<<"DCLoop = "<< (int)DCLoop<<std::endl;
  std::cout<<"StartCol = "<< (int)StartCol<<std::endl;
  std::cout<<"TrigLoop = "<< (int)TrigLoop <<std::endl;
  std::cout<<"DoubleColumnMode = "<< (int)DoubleColumnMode <<std::endl;
  std::cout<<"histogrammerMode = "<< (int)histogrammerMode <<std::endl;
  std::cout<<"readBackFromBoc = "<< (int)readBackFromBoc <<std::endl;
  std::cout<<"readBackFromInmem = "<< (int)readBackFromInmem <<std::endl;
  std::cout<<"readBackFromSlave = "<< (int)readBackFromSlave <<std::endl;
  std::cout<<"talkToFitFarm = "<< (int)talkToFitFarm <<std::endl;
  std::cout<<"--------------------------------"<<std::endl;

  // *****************************
  // Initialize Slave for Scan
  // *****************************

  IblRodSlave s;
  int slave = 1;
  s.setUart(slave+1);
  s.setId(slave);
#ifdef LL_DEBUG
s.setVerbose(slave,1);
#endif
  s.setUart(0);

  // enable the Rx link going to the formatter
  s.enableRxLink(Channels);

  int ntrigger = TrigLoop;

  IblSlvNetCfg netCfgOut;
  unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x03}; // 3 instead of 4??
  unsigned char lIp[4] = {192, 168, 1, 81}; // 81 for pc 1, 151 for pc 2
  unsigned char maskIp[4] = {255,255, 0,0};
  unsigned char gwIp[4] = {192, 168, 0, 100};
  unsigned char tIp[4] = {192, 168, 0, 100};
  uint32_t tP = 5002;

  IblRodSlave::createMacAddress(mac[2], mac[3], mac[4], mac[5], mac[6], mac[7], netCfgOut.localMac);
  IblRodSlave::createIp4Address(lIp[0], lIp[1], lIp[2], lIp[3], &netCfgOut.localIp);
  IblRodSlave::createIp4Address(maskIp[0], maskIp[1], maskIp[2], maskIp[3], &netCfgOut.maskIp);
  IblRodSlave::createIp4Address(gwIp[0], gwIp[1], gwIp[2], gwIp[3], &netCfgOut.gwIp);
  
  IblRodSlave::createIp4Address(tIp[0], tIp[1], tIp[2], tIp[3], &netCfgOut.targetIpA);
  netCfgOut.targetPortA = htonl(tP);
  
  s.setUart(slave+1);
   ////////////////////////////////////
    if (talkToFitFarm)  s.setNetCfg(&netCfgOut,slave);
   //////////////////////////////////
   IblSlvHistCfg histCfgOut;
  histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
  histCfgOut.cfg[0].enable = 1;     //enable histo0 unit
  histCfgOut.cfg[0].type = histogrammerMode;
  histCfgOut.cfg[0].maskStep = 0;   //0 for single chip(or 1 if divide row by 2, 3 if every 8 pixels)
  histCfgOut.cfg[0].chipSel = 1;    //0 for single chip(1 for all chips)
  histCfgOut.cfg[0].addrRange = 0;  //single chip <32K( full range < 1M)
  histCfgOut.cfg[0].scanId = 0x1234;//remote use only
  histCfgOut.cfg[0].binId = 1;
  histCfgOut.cfg[0].expectedOccValue = ntrigger;
 
  //////////////////////////////////////////////
  s.startHisto(&histCfgOut,slave); 
  s.setUart(0);
  /////////////////////////////////////////////////
  std::cout << "++ Started histogrammer 0" << std::endl;

  // *****************************
  // Initialize ROD for Scan
  // *****************************

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  if( !iblBocLink->enableChannels(Channels) ) return; 
  
  std::cout<<"enabled channels 0x"<<std::hex<<Channels<<" for scan "<<std::dec<<std::endl;

  // tmp!!!! 
  // this shouldn't be neccesary, but for the moment
  // you have to enable all 4 Rx channels on the BOC
  // to get data in the histogrammer;
  // after enabling once, you can then only enable the
  // channel that you want to use

  //  IblBoc::write(1, 0, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 1, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 2, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 3, IblBoc::RxControl, 0x0);

	//  IblBoc::write(1, 0, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 1, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 2, IblBoc::RxControl, 0x0);
  //  IblBoc::write(1, 3, IblBoc::RxControl, 0x0);

  IblRodInmemFifo fifo;
  fifo.selectGathererFromRxCh(16); // hard-coded!
  fifo.setRxCh(16); // hard-coded!
  fifo.reset(); 

  // *****************************
  // Initialize Fei4 for Scan
  // *****************************


  // Initialize an Fei4Proxy with the channel mask I want to use for the scan
  Fei4Proxy fei4 = Fei4Proxy(Channels);

  // TMP: getting full vector of Fei4s
  // In final version, should just use Fei4Proxy with a mask to execute all config and commands
  // For now, Fei4s themselves allow us to readback using readFrame which is not yet ported to Fei4Proxy

    std::vector<Fei4> fei4s;
    int nFes = 0;

    uint32_t i = 0;
      for (; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
	if (Channels & (1<<i)) {
	 fei4s.push_back(RodResourceManagerIBL::getCalibModuleConfig(i));
	nFes++;
      }
      }

#ifdef LL_DEBUG
      std::cout<<"Got "<<(int)nFes<<" front end(s) from the rod resource manager"<<std::endl;
#endif

#ifdef LL_DEBUG
    std::cout<<"+++++++++++++++++++++++++++"<<std::endl;
    std::cout<<"ABOUT TO DUMP MY FEI4 CFG for the first Fei4 in my mask: "<<std::endl;
    if (nFes > 0) fei4s[0].dump();
#endif

#ifdef LL_DEBUG
    if (nFes > 0) fei4s[0].setVerbose(true);
#endif
  
#ifdef LL_DEBUG
    if (nFes > 0) std::cout<<"The first Fei4 in my mask has chip ID "<<fei4s[0].getChipID()<<std::endl;
#endif
 
#ifdef LL_DEBUG
    std::cout<<"ABOUT TO READ REGISTER 2 for the first Fei4 in my mask"<<std::endl;
    if (nFes > 0) fei4s[0].rdRegister(2);
    if (nFes > 0) fei4s[0].readFrame();
#endif
  
  // set into config mode in order to configure the chip
  fei4.setRunMode(false);

  // SR_CLOCK 
  fei4.writeRegister(&Fei4::SRK, 0x0);

  // HitDiscCnfg
  fei4.writeRegister(&Fei4::HD, HitDiscCnfg);

  // Trigger
    fei4.writeRegister(&Fei4::Trig_Count, TrigCount);

  // Maxi. Trigger Latency
  fei4.writeRegister(&Fei4::Trig_Lat, 255 - 8 - TrigDelay - (TrigCount/2));

#ifdef LL_DEBUG
 std::cout << "finished initializing scan; about to begin mask stage " << std::endl;
#endif

  // *****************************
  // Loop over Mask
  // *****************************

  for(uint32_t iMask = 0 ; iMask < MaskLoop ; ++iMask) {
    std::cout<<std::endl;
    std::cout<<"++++ Beginning mask stage: "<<(int)iMask<<" out of "<<int(MaskLoop)<<" total loops"<<std::endl;
    std::cout<<std::endl;
    
    fei4.setRunMode(false); 

    // Must set CP to 0x3 to write the mask to the full chip!

    // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
    fei4.writeRegister(&Fei4::CP,0x3);
    // Column Address
    fei4.writeRegister(&Fei4::Colpr_Addr, 0);
    // Move strobe bits low again
    fei4.writeRegister(&Fei4::S0,0x0);

    if(!iMask) {
      // Setup Mask
      uint32_t pixelMask[21] = {0};
      // RowSeparation is either 2, 4, or 8
      uint8_t maskElement = 0x1+((0x1<<RowSeparation)&0xF);
      maskElement += (maskElement<<((RowSeparation>4)?8:4));
      maskElement &= 0xFF;
      for(int i = 0 ; i < 21 ; ++i)
	for(int j = 0 ; j < 4 ; ++j)
	  pixelMask[i] += (maskElement << j*8);
#ifdef LL_DEBUG
      for (int i = 0; i < 21; ++i)
	std::cout<<std::hex<<"pixelMask["<<i<<"]: "<<(int)pixelMask[i]<<std::endl; 
#endif
      fei4.wrFrontEnd(pixelMask);
    }
    else { // Mask shifting done internally

      // Select the Pixel Latches to copy on the SR
      fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x1);
      // Select SR to PARALLEL INPUT MODE
      fei4.writeRegister(&Fei4::S1,0x1);
      fei4.writeRegister(&Fei4::S0,0x1);
      fei4.writeRegister(&Fei4::HLD,0x0);// Regs: Set HitLd to 0
      fei4.writeRegister(&Fei4::SRK,0x1); // Reg:  Set SR_CLOCK to 1
      fei4.writeRegister(&Fei4::LEN,0x0); // Set LatchDisable

      // Copy from Latches to SR
      fei4.globalPulse(10);          // Pulse for READ FROM Latches FIRST Time

      // Return SR in NORMAL mode
      fei4.writeRegister(&Fei4::S1,0x0);
      fei4.writeRegister(&Fei4::S0,0x0);
      fei4.writeRegister(&Fei4::HLD,0x0);
      fei4.writeRegister(&Fei4::SRK,0x0);
      fei4.writeRegister(&Fei4::LEN,0x1);

      // Write to Latches the complement-to-1 via issuing a Global pulse
      fei4.globalPulse(10);           // Pulse for LOAD Latches FIRST (Clock cycles lenght of the pulse)
      // Set LatchDisable
      fei4.writeRegister(&Fei4::LEN,0x0);

      // Select SR to PARALLEL INPUT MODE
      fei4.writeRegister(&Fei4::S1,0x1);
      fei4.writeRegister(&Fei4::S0,0x1);
      fei4.writeRegister(&Fei4::HLD,0x0);
      fei4.writeRegister(&Fei4::SRK,0x1);

      // Cpy from Latches to SR
      fei4.globalPulse(10);              // Pulse for READ FROM Latches SECOND time

      // Move back the Pixel Strobes to 0
      fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x0);

      //Shift by one CLK the SR
      fei4.writeRegister(&Fei4::S1,0x0);
      fei4.writeRegister(&Fei4::S0,0x0);
      fei4.writeRegister(&Fei4::HLD,0x0);
      fei4.writeRegister(&Fei4::SRK,0x1);
	
      fei4.globalPulse(10);          // Pulse for shifting the SR
    }

    // Set Pixel strobes
    fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x2801);
    fei4.writeRegister(&Fei4::LEN,0x1); // Set LatchEn
    fei4.writeRegister(&Fei4::SRK,0x0); // Set SR Clock back to 0 after shifting
		
    fei4.globalPulse(10);              // Pulse Load Latches line

    fei4.writeRegister(&Fei4::LEN,0x0);// Deselect LatchEn
    // Set all pixel strobes to 0 and select S0 and S1 to 0
    fei4.writeRegister(&Fei4::S0,0x0);
    fei4.writeRegister(&Fei4::S1,0x0);
    fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x0);
			
    // To-do: protect for over and undervalues
    uint8_t CalPulse = ToT - 1;
    for(uint32_t iCalPulse = CalPulse ; iCalPulse <= CalPulse ; ++iCalPulse) {
		  
      fei4.setRunMode(false);


     //      fei4.writeRegister(&Fei4::CMDcnt12,iCalPulse+(iMask*ToTMaskStep)); // sets width of digital pulse --> ToT

      // Set ToT with calibration pulse width
      // For testing purposes, give each FE a different ToT pattern

      int totIncrease = 0;
      for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
	if (Channels & (1<<i)) {
#ifdef LL_DEBUG
	  std::cout<<"For Fe # "<<(int)i<<" with mask: 0x"<<
	    std::hex<<(int)(Channels & (1<<i))<<" setting cal pulse width reg to: "<<
	    std::dec<<(int)(iCalPulse+(iMask*ToTMaskStep)+totIncrease)<<std::endl;
#endif
	  Fei4Proxy::writeRegister(&Fei4::CMDcnt12, (iCalPulse+(iMask*ToTMaskStep)+totIncrease), (Channels & (1<<i)));
	  totIncrease++;
	}
      }
      
      // loop over double columns
      // Digital:
      // colAddr = 0 --> columns 1 and 2
      // colAddres = n --> columns 2n+1 and 2n+2
      // Analog:
      // colAddr = 0 --> column 1
      // colAddres = n --> columns 2n and 2n+1
      // coldAdd = 39 --> columns 78,79,80
		  
      for(uint32_t colAddr = StartCol ; colAddr < StartCol + DCLoop ; colAddr++) {
	std::cout<<"++++++ Beginning DC LOOP with column address: "<<(int)colAddr<<std::endl;
	fei4.setRunMode(false);

	// Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
#ifdef LL_DEBUG
	std::cout<<"setting DC mode: 0x"<<std::hex<<int(DoubleColumnMode)<<
	  ", and colAddr = 0x"<<int(colAddr)<<std::dec<<std::endl;
#endif
	fei4.writeRegister(&Fei4::CP,DoubleColumnMode);
	// Column Address
	fei4.writeRegister(&Fei4::Colpr_Addr, colAddr);
		    
	fei4.setRunMode(true);
		    
	for(uint32_t iTrig = 0 ; iTrig < TrigLoop ; ++iTrig) {
#ifdef LL_DEBUG
	  std::cout<<std::endl;
	  std::cout<<"about to send trigger # "<<int(iTrig)<<std::endl;
	  std::cout<<std::endl;
#endif
	  fei4.calTrigger(TrigDelay);
	  for(uint8_t iTrigCount = 0 ; iTrigCount < TrigCount ; ++iTrigCount) {

	    //To-do: check if this is necessary??
        std::this_thread::sleep_for(std::chrono::microseconds(100));

	    // reading back from inmem prints out on screen
	    // reading back from boc prints out on the screen and sends data back the first chip

	    //Read back from boc, saves data from first chip
	    std::vector<int> answer;

	    if (readBackFromBoc && nFes > 0) answer = fei4s[0].readFrame();			

#ifdef LL_DEBUG
	    // debug, output to screen
	    std::cout<<"**********************************"<<std::endl;
	    std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo for Chip 0, ANSWER is: "<<std::endl;
	    std::cout<<"**********************************"<<std::endl;

	    for (unsigned int i = 0; i < answer.size(); i++) {
	      if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<answer[i];
	      else if (i%3 == 1) std::cout<<std::hex<<answer[i];
	      else std::cout<<std::hex<<answer[i]<<std::dec<<std::endl;
	    }
	    std::cout<<"**********************************"<<std::endl;
	    std::cout<<"**********************************"<<std::endl;

	    // reading back from chips other than 1 from Boc
	      for (int j = 1; j < nFes; j++) {
		std::vector<int> answer2 = fei4s[j].readFrame();
		std::cout<<"**********************************"<<std::endl;
		std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo for Chip "<<(int)j<<", ANSWER is: "<<std::endl;
		std::cout<<"**********************************"<<std::endl;
		
		for (unsigned int i = 0; i < answer2.size(); i++) {
		  if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<answer2[i];
		  else if (i%3 == 1) std::cout<<std::hex<<answer2[i];
		  else std::cout<<std::hex<<answer2[i]<<std::dec<<std::endl;
		}
	    std::cout<<"**********************************"<<std::endl;
	    std::cout<<"**********************************"<<std::endl;			      
	      }
#endif

	      // To-do: make inmem not hard-coded
	      // For now, hard-coded to read from channel 16
	      if (readBackFromInmem) {
	      uint32_t fifoByte = 0xc0ffee;
		fifoByte = (fifo.read()&0xFF);
	      for (int i = 0; i < 1000; i++) {
		fifoByte = (fifo.read()&0xFF);
		if (i%3 == 0) std::cout<<"read back from inmem fifo: Word "<<int(i/3)<<" = 0x"<<std::hex<<fifoByte;
		else if (i%3 == 1) std::cout<<std::hex<<fifoByte;
		else std::cout<<std::hex<<fifoByte<<std::dec<<std::endl;
					
		if ((fifoByte == 0xBC) || (fifoByte == 0x4BC) || (fifoByte == 0xCBC)) break;
		if (fifoByte == 0x800) break;
	      }
	    }

	    // Only sends back the data from the first chip!
	    // The ppc can not handle too much data, sorry
	      if (nFes > 0) Fei4Data::fillPixDataArray(answer, fei4s[0].getValue(&Fei4Cfg::HD), tot, StartCol);

	  } // iTrigCount
	} // iTrig
      } // colAddr
    } // iCalPulse

  } // iMask;

  // Reset to default values:

  fei4.setRunMode(false);
  fei4.writeRegister(&Fei4::CP, 0x0);
  fei4.writeRegister(&Fei4::Colpr_Addr, 0x0);
  fei4.writeRegister(&Fei4::HD, 0x0);
  fei4.writeRegister(&Fei4::Trig_Lat, 0x210);
  fei4.writeRegister(&Fei4::Trig_Count, 0x1);

  fei4.setRunMode(true);

  // copy debug ToT to command result
  std::copy(tot[0], tot[0]+1344, (result.ToT)[0]);

  // *****************************
  // Stop Histogrammer (after every mask stage)
  // *****************************
  //////////////////////////////////////////////
    s.stopHisto(slave); // TMP LJ
  //////////////////////////////////////////////
  std::cout << "++ Stopped histogrammer" << std::endl;

  // *****************************
  // Read back from Histogrammer
  // *****************************

  // see that we got something, debugging
  uint32_t histoOutput[6*80] = {};
 
  if (readBackFromSlave) {
    s.getHisto(slave, 0, histoOutput);
    s.setUart(0);
    for(int i = 0; i < 6*80; i++){
      std::cout << "--getHisto-data--" << i << "--- " << histoOutput[i] << std::endl;
    }
    std::cout << "----get histo" << std::endl;
  }

  result.success = true;

  std::cout<<std::endl;
  std::cout<<" *****************************"<<std::endl;
  std::cout<<"Done with executing the Digital Scan! Hopefully it worked"<<std::endl;
  std::cout<<" *****************************"<<std::endl;
  std::cout<<std::endl;

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
 
}

void IblDigScan::serialize(uint8_t *out) const
{
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out,offset,Channels);
  Serializer<uint8_t>::serialize(out,offset,ToT);
  Serializer<uint8_t>::serialize(out,offset,ToTMaskStep);
  Serializer<uint8_t>::serialize(out,offset,RowSeparation);
  Serializer<uint8_t>::serialize(out,offset,TrigDelay);
  Serializer<uint8_t>::serialize(out,offset,TrigCount);
  Serializer<uint8_t>::serialize(out,offset,HitDiscCnfg);
  Serializer<uint8_t>::serialize(out,offset,MaskLoop);
  Serializer<uint8_t>::serialize(out,offset,DCLoop);
  Serializer<uint8_t>::serialize(out,offset,StartCol);
  Serializer<uint8_t>::serialize(out,offset,TrigLoop);
  Serializer<uint8_t>::serialize(out,offset,DoubleColumnMode);
  Serializer<bool>::serialize(out,offset,readBackFromBoc);
  Serializer<bool>::serialize(out,offset,readBackFromInmem);
  Serializer<bool>::serialize(out,offset,readBackFromSlave);
  Serializer<bool>::serialize(out,offset,talkToFitFarm);
  Serializer<uint32_t>::serialize(out,offset,histogrammerMode);

}

void IblDigScan::deserialize(const uint8_t *in)
{
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  uint32_t offset = 0;
  Channels = Serializer<uint32_t>::deserialize(in,offset);
  ToT = Serializer<uint8_t>::deserialize(in,offset);
  ToTMaskStep = Serializer<uint8_t>::deserialize(in,offset);
  RowSeparation = Serializer<uint8_t>::deserialize(in,offset);
  TrigDelay = Serializer<uint8_t>::deserialize(in,offset);
  TrigCount = Serializer<uint8_t>::deserialize(in,offset);
  HitDiscCnfg = Serializer<uint8_t>::deserialize(in,offset);
  MaskLoop = Serializer<uint8_t>::deserialize(in,offset);
  DCLoop = Serializer<uint8_t>::deserialize(in,offset);
  StartCol = Serializer<uint8_t>::deserialize(in,offset);
  TrigLoop = Serializer<uint8_t>::deserialize(in,offset);
  DoubleColumnMode = Serializer<uint8_t>::deserialize(in,offset);
  readBackFromBoc = Serializer<bool>::deserialize(in,offset);
  readBackFromInmem = Serializer<bool>::deserialize(in,offset);
  readBackFromSlave = Serializer<bool>::deserialize(in,offset);
  talkToFitFarm = Serializer<bool>::deserialize(in,offset);
  histogrammerMode = Serializer<uint32_t>::deserialize(in,offset);
}

uint32_t IblDigScan::serializedSize() const
{
#ifdef LL_DEBUG
  std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  return (2*sizeof(uint32_t)+11*sizeof(uint8_t)+4*sizeof(bool));
}

void IblModuleMap::serialize(uint8_t *out) const {
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  uint32_t offset = 0;
  Serializer<bool>::serialize(out, offset, success);
  for (int i=0; i < 4; i++) {
    for (int j=0; j < 336; j++) {
      Serializer<uint8_t>::serialize(out, offset, ToT[i][j]);
    } // loop over columns
  } // loop over row
}

void IblModuleMap::deserialize(const uint8_t *in) {
  uint32_t offset = 0;  
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  success = Serializer<bool>::deserialize(in, offset);
  for (int i=0; i < 4; i++) {
    for (int j=0; j < 336; j++) {
      ToT[i][j] = Serializer<uint8_t>::deserialize(in, offset);
    } // loop over columns
  } // loop over row
}

uint32_t IblModuleMap::serializedSize() const {
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  return (sizeof(bool)+4*336*sizeof(uint8_t));
}
