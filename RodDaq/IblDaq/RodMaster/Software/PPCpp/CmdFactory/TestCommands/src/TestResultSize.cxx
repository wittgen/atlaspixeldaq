#include "TestResultSize.h"

TestResultSize::TestResultSize(){ 
  setResultPtr(); 
  dummy = 7;
}

void TestResultSize::execute() {

#ifdef __XMK__

  uint8_t testArray[ARRAY_SIZE] = {0};
  std::cout<<"ARRAY_SIZE = "<<int(ARRAY_SIZE)<<std::endl;

  std::copy(testArray, testArray+ARRAY_SIZE, result.myArray);

 #else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

void TestResultSize::serialize(uint8_t *out) const
{
	  uint32_t offset = 0;
  Serializer<uint8_t>::serialize(out, offset, dummy);
}

void TestResultSize::deserialize(const uint8_t *in)
{
	  uint32_t offset = 0;
dummy = Serializer<uint8_t>::deserialize(in, offset);
}

uint32_t TestResultSize::serializedSize() const
{
  return (sizeof(uint8_t));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
void TestResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
    for (int j=0; j < ARRAY_SIZE; j++) {
      Serializer<uint8_t>::serialize(out, offset, myArray[j]);
    }
}

void TestResult::deserialize(const uint8_t *in) {
	  uint32_t offset = 0;
    for (int j=0; j < ARRAY_SIZE; j++) {
myArray[j] = Serializer<uint8_t>::deserialize(in, offset);
    }
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t TestResult::serializedSize() const {
  return (ARRAY_SIZE*sizeof(uint8_t));
}
