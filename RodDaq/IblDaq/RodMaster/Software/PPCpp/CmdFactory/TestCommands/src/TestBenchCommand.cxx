#include "TestBenchCommand.h"
#ifdef __XMK__

#endif

SERIAL(TestSerialize,
       prep(testSerialize);
       )

TestBenchCommand::TestBenchCommand(){
  setResultPtr();
}

void TestBenchCommand::execute() {

}

void TestBenchCommand::serialize(uint8_t *out) const{
  //
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  Serializer<TestSerialize>::serialize(out,offset,m_ts);
}

void TestBenchCommand::deserialize(const uint8_t *in){
  //
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  Serializer<TestSerialize>::deserialize(in,offset,m_ts);
  std::cout<<"testBit : " << m_ts.testSerialize.testBit << std::endl;
  m_ts.testSerialize.printN();
  //std::cout<<"testBits : " << m_ts.testSerialize.testBits[0] << " " << m_ts.testSerialize.testBits[1] << " "<< m_ts.testSerialize.testBits[2] << " "<< m_ts.testSerialize.testBits[3] << " " << std::endl;
}

uint32_t TestBenchCommand::serializedSize() const{
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  std::cout<< " m_ts.serializedSize : " << m_ts.serializedSize() << std::endl;
  return(m_ts.serializedSize());
}

void TestBenchReturn::serialize(uint8_t *out) const {
}

void TestBenchReturn::deserialize(const uint8_t *in) {
}

uint32_t TestBenchReturn::serializedSize() const {
  return(0);
}
