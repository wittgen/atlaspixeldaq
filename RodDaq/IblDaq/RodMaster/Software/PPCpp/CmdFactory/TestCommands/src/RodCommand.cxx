#include "CppCompatibility.h"

#include "RodCommand.h"

// Todo: think return type through
void RodCommand::execute()
{	
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << "I'm here\n" ;
	std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << m_ExecId << std::endl;
	// Modify the result member as appropriate
	std::cout << "Result address = " << &result << std::endl;
	std::cout << "Pointer to result: " << m_ResultPtr << std::endl;

	result.value1 = 10;
	result.value2 = 200;
	std::cout << "value1 = " << result.value1 << std::endl;
	std::cout << "value2 = " << result.value2 << std::endl;
	
	std::cout << "WTF?" << std::endl;
// STUPID COMMENT

	/*
	Fei4Cfg fc;
        fc.SME.write(1);
	fc.PlsrRiseUpTau.write(0xF);
	
	Fei4 f;
	f.sendRegister(&Fei4::SME);
	f.writeRegister(&Fei4::SME,0x1);
	f.sendRegister(5);
	f.writeRegister(5,5);

	f.dump();

        fc.dump();
	*/
	IblRodSlave s;
	s.testHisto();
	//Fei4PixelCfg pcfg;
	//pcfg.dump(0);
    

}

/// Inhertited functions for RodCommand
void RodCommand::serialize(uint8_t *out) const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void RodCommand::deserialize(const uint8_t *in)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

uint32_t RodCommand::serializedSize() const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	return 0;
}


