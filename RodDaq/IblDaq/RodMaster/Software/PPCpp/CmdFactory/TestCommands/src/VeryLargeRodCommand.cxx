#include "CppCompatibility.h"

#include "VeryLargeRodCommand.h"

// Todo: think return type through
void VeryLargeRodCommand::execute()
{	
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << m_ExecId << std::endl;
	for( size_t i = 0 ; i < sizeof(result.large_array)/sizeof(uint32_t) ; ++i ) result.large_array[i] = large_array[i]; // +1 to avoid zero (easy check!)
	std::cout << "Done filling in result array!" << std::endl;

}

/// Inhertited functions for VeryLargeRodCommand
void VeryLargeRodCommand::serialize(uint8_t *out) const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	uint32_t offset = 0;
	std::for_each( large_array, large_array+sizeof(large_array)/sizeof(uint32_t), 
			std::tr1::bind( &Serializer<uint32_t>::serialize, out, offset, std::tr1::placeholders::_1 ) );
}

void VeryLargeRodCommand::deserialize(const uint8_t *in)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	uint32_t offset = 0;
	std::generate( large_array, large_array+sizeof(large_array)/sizeof(uint32_t),
			std::tr1::bind( (uint32_t(*)(const uint8_t*, uint32_t&))Serializer<uint32_t>::deserialize, in, offset) );
}

uint32_t VeryLargeRodCommand::serializedSize() const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	return sizeof(large_array);
}


