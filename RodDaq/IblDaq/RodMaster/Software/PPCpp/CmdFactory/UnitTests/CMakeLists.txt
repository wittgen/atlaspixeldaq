project(UnitTests)

add_library(${PROJECT_NAME} STATIC
    src/DelayScan.cxx
    src/DigitalInjectionTest.cxx
    src/DummyOptoTune.cxx
    src/MCCConnScan.cxx
    src/IBLChannelMapping.cxx
    src/RegisterReadBack.cxx
    src/TestRegister.cxx
    src/TestRodBusy.cxx
    src/TestRodRegisters.cxx
    src/TestSlave.cxx
    src/VerifyOptoTune.cxx
    src/WriteRodRegisters.cxx
)

target_include_directories(${PROJECT_NAME}
    PUBLIC inc
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE CmdPattern
    PRIVATE SysConnect
    PRIVATE Tools
    )
