#ifndef _TESTSLAVE_H_
#define _TESTSLAVE_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2014-March-10
 *
 * Tests that the communication between ppc and slave is OK
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class Success: public CommandCRTP<Success > { // declares type of command result
 public:

  bool success;
  
	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class TestSlave: public CommandCRTP<TestSlave > { // declares type of command
public:
	typedef Success ResultType;
	ResultType result;

	TestSlave();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~TestSlave() {}

	// command parameters of this command
	bool talkToFitFarm;
	bool whichSlave;
	uint32_t fitPorts[4];
	uint8_t fitIps[4][4];

};
#endif// _TESTSLAVE_H_

