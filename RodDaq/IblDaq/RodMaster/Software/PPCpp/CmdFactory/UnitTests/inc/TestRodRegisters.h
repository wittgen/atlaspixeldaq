#ifndef _TESTRODREGISTERS_H_
#define _TESTRODREGISTERS_H_
/*
 * Authors: 
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 6 April 2014
 *
 * This command prints the current values of the registers on the ROD
 * to both the PPC output stream as well as to the host via serialized results.
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/unitTests/testRodRegisters.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class RegisterResult: public CommandCRTP<RegisterResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t epcBaseValue;
	uint32_t epcLocalValue;
	uint32_t epcSlvARegsValue;
	uint32_t epcSlvARamValue;
	uint32_t epcSlvBRegsValue;
	uint32_t epcSlvBRamValue;
	uint8_t epcBocValue;
	uint32_t epcLocalCtrlValue;
	uint32_t epcLocalStatValue;
	uint32_t epcLocalSpMaskValue;
	uint32_t dspRegBaseValue;
	uint32_t designRegPPCValue;

	uint32_t slvA_Fmt0StatusReg;
	uint32_t slvA_Fmt1StatusReg;
	uint32_t slvA_Fmt2StatusReg;
	uint32_t slvA_Fmt3StatusReg;

	uint32_t slvB_Fmt0StatusReg;
	uint32_t slvB_Fmt1StatusReg;
	uint32_t slvB_Fmt2StatusReg;
	uint32_t slvB_Fmt3StatusReg;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class TestRodRegisters: public CommandCRTP<TestRodRegisters > { // declares type of command
public:
	typedef RegisterResult ResultType;
	ResultType result;
 
	void dump();
	TestRodRegisters();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~TestRodRegisters() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

};
#endif //_TESTRODREGISTERS_H_

