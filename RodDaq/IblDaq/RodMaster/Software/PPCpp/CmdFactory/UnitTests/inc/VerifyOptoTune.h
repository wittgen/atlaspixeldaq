#ifndef _VERIFY_OPTO_TUNE_H_
#define _VERIFY_OPTO_TUNE_H_
/*
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class VerifyOptoTuneResults: public CommandCRTP<VerifyOptoTuneResults > {
 public:
        virtual ~VerifyOptoTuneResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map<uint8_t, uint32_t > errors;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class VerifyOptoTune: public CommandCRTP<VerifyOptoTune > { // declares type of command
 public:
  InstantiateResult(VerifyOptoTuneResults);

  virtual const Command* getResult() const {
      return &result;
  }

  VerifyOptoTune();
  virtual ~VerifyOptoTune() {}

  virtual void execute();

  uint32_t CreateCompareBitStream(uint32_t pattern, uint8_t nPattern, std::vector<uint8_t> &CompareBitStream, std::vector<uint8_t> &MaskBitStream);

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  // command parameters of this command

  int nTriggers;
  int speed;

};
#endif //  _VERIFY_OPTO_TUNE_H_

