#ifndef _DELAYSCAN_H_
#define _DELAYSCAN_H_
/*
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class DelayScanResults: public CommandCRTP<DelayScanResults > {
 public:
        virtual ~DelayScanResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map <uint8_t, std::vector< std::vector<uint32_t> > > DelayMap;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class DelayScan: public CommandCRTP<DelayScan > { // declares type of command
 public:
  InstantiateResult(DelayScanResults);

  virtual const Command* getResult() const {
      return &result;
  }

  DelayScan();
  virtual ~DelayScan() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  // command parameters of this command
  float delayStart;
  float delayStop;//Coarse delay*100 + Fine
  int nDelays;

};
#endif //  _DELAYSCAN_H_

