#ifndef _WRITERODREGISTERS_H_
#define _WRITERODREGISTERS_H_
/*
 * Authors: 
 * Russell Smith
 * Date: 29 April 2014
 * 
 * Todo 
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class WriteRodRegisters: public CommandCRTP<WriteRodRegisters > { // declares type of command
public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything                                                                         
 
	WriteRodRegisters();
	
	/// Inhertited functions
	virtual void execute();
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~WriteRodRegisters() {}

	uint32_t epcBaseValue;
	uint32_t epcLocalValue;
	uint32_t epcSlvARegsValue;
	uint32_t epcSlvARamValue;
	uint32_t epcSlvBRegsValue;
	uint32_t epcSlvBRamValue;
	uint8_t epcBocValue;
	uint32_t epcLocalCtrlValue;
	uint32_t epcLocalStatValue;
	uint32_t epcLocalSpMaskValue;
	uint32_t dspRegBaseValue;
	uint32_t fmt0RegBaseValue;
	uint32_t fmt1RegBaseValue;
	uint32_t designRegPPCValue;

};
#endif //_WRITERODREGISTERS_H_

