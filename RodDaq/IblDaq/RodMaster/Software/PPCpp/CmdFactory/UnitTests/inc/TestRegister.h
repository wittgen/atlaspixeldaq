/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 27 Oct 2014
 * Description: This command writes a varying pattern to the error mask of the FE and
 * reads it back. It prints out the frame if it differs from what is expected.
 */

#ifndef __TEST_REGISTER_H__
#define __TEST_REGISTER_H__

#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class TestRegister: public CommandCRTP<TestRegister >, public EasySerializable { 
 public:
  InstantiateResult(EmptyResult)
  
    virtual void execute();
  TestRegister(); 
  virtual ~TestRegister() {}

  uint32_t rx;
  uint32_t nReads;
  uint16_t valueToWrite;

  SERIAL_H(prep(rx); prep(nReads); prep(valueToWrite);)
    };

#endif // 

