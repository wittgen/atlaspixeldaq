/*
 * Authors: 
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 6 April 2014
 *
 * This command prints the current values of the registers on the ROD
 * to both the PPC output stream as well as to the host via serialized results.
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/unitTests/TestRodBusy.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class BusyResult: public CommandCRTP<BusyResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t masterBusyMaskValue;
	uint32_t masterBusyCurrentStatusValue;


	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class TestRodBusy: public CommandCRTP<TestRodBusy > { // declares type of command
public:
	typedef BusyResult ResultType;
	ResultType result;
 
	void dump();
	TestRodBusy();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~TestRodBusy() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

};

