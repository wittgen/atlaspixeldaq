#ifndef _MCCCONNSCAN_H_
#define _MCCCONNSCAN_H_
/*
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class MCCConnScanResults: public CommandCRTP<MCCConnScanResults > {
 public:
        virtual ~MCCConnScanResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	uint16_t MapActivity[32][32];
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class MCCConnScan: public CommandCRTP<MCCConnScan > { // declares type of command
 public:
  InstantiateResult(MCCConnScanResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  MCCConnScan();
  virtual ~MCCConnScan() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  // command parameters of this command
  uint32_t txMask;
  uint32_t rxMask;
  uint32_t thrStart;
  uint32_t thrStop;
  uint32_t thrStep;
  uint32_t delayStart;
  uint32_t delayStop;//Coarse delay*100 + Fine
  uint32_t delayStep;

};
#endif //  _MCCCONNSCAN_H_

