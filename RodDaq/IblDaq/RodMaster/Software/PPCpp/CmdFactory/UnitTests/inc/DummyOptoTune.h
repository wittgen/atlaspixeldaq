#ifndef _DUMMYOPTOTUNE_H_
#define _DUMMYOPTOTUNE_H_
/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 17-Septemer-2015
 *
 * Get going with L1/L2 Opto-Link Tunning with low-level host command feature
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
// #include "IblRodSlave.h" //  To be replaced with header(s) for BOC classs

struct DummyResultArray { uint32_t values[8][2]; };

class DummyOptoTuneResults: public CommandCRTP<DummyOptoTuneResults > {
 public:
        virtual ~DummyOptoTuneResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        // Results returned by this command
        uint32_t nThr;
        std::vector<DummyResultArray> ErrorRate;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class DummyOptoTune: public CommandCRTP<DummyOptoTune > { // declares type of command
 public:
  InstantiateResult(DummyOptoTuneResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  DummyOptoTune() {setResultPtr();}
  virtual ~DummyOptoTune() {}
  
  virtual void execute();
  void rand100k(size_t n, std::vector<uint32_t> &rv);

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  uint32_t optoEmu;
  uint32_t nTriggers;
  uint32_t nThr;
  std::vector<uint32_t> thrSteps;
  std::vector<DummyResultArray> ErrorRate;
  
};
#endif // _DUMMYOPTOTUNE_H_

