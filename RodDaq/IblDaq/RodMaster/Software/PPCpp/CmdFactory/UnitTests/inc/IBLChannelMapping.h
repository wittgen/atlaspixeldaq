#ifndef _IBL_CHANNEL_MAPPING_H_
#define _IBL_CHANNEL_MAPPING_H_
/*
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK
#include "Fe4i.h"
#endif

class IBLChannelMappingResults: public CommandCRTP<IBLChannelMappingResults > {
 public:
        virtual ~IBLChannelMappingResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::array< std::array<int,32>,32 > chipNumber;
	std::array< std::array<int,32>,32 > serialNumber;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class IBLChannelMapping: public CommandCRTP<IBLChannelMapping > { // declares type of command
 public:
  InstantiateResult(IBLChannelMappingResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  IBLChannelMapping();
  virtual ~IBLChannelMapping() {}
  
  virtual void execute();
  
  static void sendTx(int txCh, uint32_t cmd ,uint32_t value=0);
  void sendDefaultConfig(int txCh, int chipID);
  uint16_t readRxFifo(int rxCh);

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  // command parameters of this command
  uint32_t txMask;
  uint32_t rxMask;

};
#endif //  _IBL_CHANNEL_MAPPING_H_

