#ifndef _DIGITALINJECTIONTEST_H
#define _DIGITALINJECTIONTEST_H
/*
 * Authors:  L. Jeanty <laura.jeanty@cern.ch>
 * Date: 28-April-2014
 * Description: IBL Digital Injection for testing Firmware
 */

// Inclusion necessary for command definitions
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "EasySerializable.h"
#include "IblRodSlave.h"

#ifdef __XMK__
#include "Fei4Proxy.h"
#include "IblBocLink.h"
#include "Fei4PixelCfg.h"
#endif

// This result 'type' will probably be used so often that 
// it may be justified for it to have its own header.
class OccHisto: public CommandCRTP<OccHisto >, public EasySerializable {
public:
  uint16_t Histo[80][336];
  virtual ~OccHisto() {};
  
  SERIAL_H(prep(Histo,80);)

};

class DigitalInjectionTest: public CommandCRTP<DigitalInjectionTest >, public EasySerializable {
public:
  InstantiateResult(OccHisto)
    
    uint32_t Channels;

  uint8_t ToT_Base;
  uint8_t ToT_MaskStepIncrease;
  uint8_t ToT_NextChipIncrease;
  
  uint8_t TrigDelay;
  uint8_t TrigCount;
  uint8_t HitDiscCnfg;
  uint32_t HistogrammerMode;
  
  uint32_t MaskStepsInMask;
  uint8_t MaskStepsToDo;
  uint8_t DoubleColumnMode;
  uint8_t NDCLoops;
  uint8_t StartDCCol;
  uint8_t NTriggers;

  bool sendToFitServer;
  uint32_t fitServerPort;
  bool doBocFifoHisto;
  uint8_t bocFifoChannel;

  bool verbose;
        DigitalInjectionTest();
	void execute(); 
        virtual ~DigitalInjectionTest() {};

	SERIAL_H(prep(Channels); prep(ToT_Base); prep(ToT_MaskStepIncrease); prep(ToT_NextChipIncrease);
		 prep(TrigDelay); prep(TrigCount); prep(HitDiscCnfg); prep(HistogrammerMode);
		 prep(MaskStepsInMask); prep(MaskStepsToDo); prep(DoubleColumnMode); 
		 prep(NDCLoops); prep(StartDCCol); prep(NTriggers); prep(sendToFitServer);
		 prep(fitServerPort); prep(doBocFifoHisto); prep(bocFifoChannel);
		 prep(verbose););
	  
 protected:
 private:
	void printOutParams();
	void initializeSlaves();
	void initializeFrontEnd();
#ifdef __XMK__
	void configureHistogrammer(uint8_t slaveID, IblSlvHistCfg & histCfgOut, IblSlvNetCfg & netCfgOut);
#endif
	void setupMask(uint32_t maskLoop);
	void readBackData(uint16_t (&histoArray)[80][336]);
	void startHistogramming(uint32_t maskLoop);
	void stopHistogramming(uint32_t maskLoop);

  // Links to front ends for sending configs and commands
#ifdef __XMK__
  Fei4 * m_fei4ReadBack; // todo tmp LJ for reading back from one front-end
  Fei4Proxy m_fei4; // Abstract front end for broadcasting commands
#endif

  // Pixel Mask
#ifdef __XMK__
  Mask m_mask;
#endif

  // Histogrammer related classes
#ifdef __XMK__
  IblRodSlave m_slave;
  IblSlvHistCfg m_histCfgOut0, m_histCfgOut1; // one for each slave (can enable different histogrammering units on each)
  IblSlvNetCfg m_netCfgOut0, m_netCfgOut1;
  bool m_useSlave0, m_useSlave1;
#endif


};
#endif // _DIGITIALINJECTIONTEST_H

