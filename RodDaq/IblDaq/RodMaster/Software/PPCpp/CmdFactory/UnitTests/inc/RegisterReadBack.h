/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 6 Nov 2014
 * Description: This command is intended for low level tests of the FE communication.
 * It write a value to the error mask register (which value is a parameter of the command)
 * and reads back the value. It sends back the value read to the host and prints out the
 * value on the uart if different from expected value. The number of channels (32) is 
 * hard-coded in the array, desolee.
 */

#ifndef __REGISTER_READBACK_H__
#define __REGISTER_READBACK_H__

#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"


class RegisterValues: public CommandCRTP<RegisterValues >, public EasySerializable {
 public:
  uint16_t regValues[32];
  virtual ~RegisterValues() {};

  SERIAL_H(prep(regValues,32);)

};

class RegisterReadBack: public CommandCRTP<RegisterReadBack >, public EasySerializable { 
 public:
  InstantiateResult(RegisterValues)
  
    virtual void execute();
  RegisterReadBack(); 
  virtual ~RegisterReadBack() {}

  uint16_t valueToWrite;

  SERIAL_H(prep(valueToWrite);)
    };

#endif // 

