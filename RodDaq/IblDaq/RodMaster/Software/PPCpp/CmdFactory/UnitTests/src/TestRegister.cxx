#include "TestRegister.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "IblConnectivity.h"
#include "IblBocLink.h"
#include <iomanip>
#endif

TestRegister::TestRegister() {
  rx = 1; // channel to test
  nReads = 100; // number of read back per reg write
  valueToWrite = 0; // 0 means loop through values
}

void  TestRegister::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
  
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  iblBocLink->enableChannels(0xFFFFFFFF);

  Fei4 * fe = &(RodResourceManagerIBL::getCalibModuleConfig(rx));

  uint16_t current_value;

  if (valueToWrite != 0) {
    fe->writeRegister(&Fei4::ErrorMask_0,valueToWrite);
    std::cout<<"Wrote: 0x"<<std::hex<<(int)valueToWrite<<" to error mask register 0"<<std::dec<<std::endl;
    for (uint32_t j = 0; j < nReads; j++) {
      fe->rdRegister(3);
    }
  }
  else {
    for (int k = 1; k<65535; k++) {

    current_value = k;
    fe->writeRegister(&Fei4::ErrorMask_0,current_value);
    if (k%100 == 0) std::cout<<"Wrote: 0x"<<std::hex<<(int)k<<" to error mask register 0"<<std::dec<<std::endl;

    uint16_t regValue = 0; 
    std::vector<int> current_frame;
    
  for (uint32_t j = 0; j < nReads; j++) {
    fe->rdRegister(3);
    continue; // tmp lj; using chipscope to debug because it crashes on unusual data

        current_frame = fe->readFrame();

    if (current_frame.size() < 5) continue;
   
    regValue = ((current_frame[current_frame.size()-3]&0x00FF)<<8) + (((current_frame[current_frame.size() - 2])&0xFF));


    if (regValue != current_value) {
      std::cout<<"ERROR! Read back 0x"<<std::hex<<(int)regValue<<" when I expected: 0x"<<(int)current_value<<std::dec<<std::endl;
      std::cout<<"Dumping raw frame: 0x";
      for (size_t i = 0; i < current_frame.size(); i++) 
	std::cout<<std::hex<<(int)current_frame[i];
      
      std::cout<<std::dec<<std::endl;
    }
  } // nReads
  } // loop over value setting

  } // loop over values


  std::cout<<"Hey Marius and Laura, I'm finised!"<<std::endl;
#endif
}

