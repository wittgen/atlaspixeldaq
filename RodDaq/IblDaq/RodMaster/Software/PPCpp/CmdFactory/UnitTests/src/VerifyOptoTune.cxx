/*
 *
 */
#include <unistd.h>
#include <bitset>
#include "VerifyOptoTune.h"
#include "RodResourceManager.h"
#include "OptoScan.h"
#include "PixScanBase.h"
#include "BarrelConnectivity.h"

VerifyOptoTune::VerifyOptoTune() {

	setResultPtr();
	nTriggers =100;
	speed = 0;
}

void VerifyOptoTune::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

	uint32_t bitPattern = 0xBFE;
	
	std::cout << "Running VerifyOptoTune" << std::endl;

PixLib::PixScanBase::MccBandwidth MCCSpeed = PixLib::PixScanBase::SINGLE_40;
	switch(speed){
	  case 0:
	    MCCSpeed =  PixLib::PixScanBase::SINGLE_40;
	  break;
	  case 1:
	    MCCSpeed =  PixLib::PixScanBase::DOUBLE_40;
	  break;
	  case 2:
	    MCCSpeed =  PixLib::PixScanBase::SINGLE_80;
	  break;
	  case 3:
	    MCCSpeed =  PixLib::PixScanBase::DOUBLE_80;
	  break;
	  default:
	  std::cout<<"Speed can be only 0 (40 Mbps) or 1 (2x80 Mbps), 2 (80 Mbps) and 3 (2x80 Mbps)  while you provide "<< speed <<" setting to 40 Mbps by default"<<std::endl;
	}
	
	uint32_t rxMask =0;

	Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

	 for(uint8_t iMod = 0 ; iMod < 32 ; ++iMod){
	    //Only channels belonging to my mask!!!
	    if (fes[iMod].revision ==0 )continue;

              uint32_t rxCh = fes[iMod].getRxCh();

              rxMask |= (1<<rxCh);
              //Skip alternative Rx channel for 160 Mbps
              if(MCCSpeed ==  PixLib::PixScanBase::DOUBLE_80 || MCCSpeed ==  PixLib::PixScanBase::DOUBLE_40)continue;

	      if( IblBoc::getSlaveFibreFromRxCh(rxCh)%24 != 0){
	      
	      rxCh = BarrelConnectivity::getAltRxLink(rxCh);
	      rxMask |= (1<<rxCh);
	      
	      }
	  }

	IblBoc::PixSetSpeed( MCCSpeed );

	SerialPort* sPort =NULL;
	sPort = RodResourceManagerPIX::getSerialPort();
	uint32_t txMask = BarrelConnectivity::getTxMaskFromRxMask(rxMask);
	sPort->setTxMask(txMask);
	Fei3Mod Fei3;
	Fei3.setTxLink(sPort);

	RodResourceManagerIBL::getIblBocLink()->enableRxChannels(rxMask);

	//create bitStream to compare
	std::vector <uint8_t> CompareBitStream;
	std::vector <uint8_t> MaskBitStream;

	uint32_t nBitPattern = 11;
	uint32_t nBitsCheck = OptoScan::createCompareBitStream (bitPattern, nBitPattern, CompareBitStream, MaskBitStream);

	std::cout<<"Compare stream size: "<<nBitsCheck<<" bits"<<std::endl;

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	  if (rxMask & (1<<rxCh)){
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareLoad(rxCh, CompareBitStream, MaskBitStream);
	  IblBoc::PixCompareEnable(rxCh);
	}
	
	
	Fei3.writeMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay
	sleep(10);
	Fei3.mccGlobalResetMCC();
	Fei3.writeMCCRegister(CSR, 0x0040 | (int8_t)MCCSpeed );//setMCCspeed
	sleep(5);

	result.errors = OptoScan::getOptoErrors(rxMask, nTriggers, nBitsCheck, bitPattern, nBitPattern, Fei3);

	OptoScan::cleanUp(rxMask,Fei3);

#endif
}

void VerifyOptoTune::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<int>::serialize(out, offset, nTriggers);
	Serializer<int>::serialize(out, offset, speed);
	
}

void VerifyOptoTune::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<int>::deserialize(in, offset, nTriggers);
	Serializer<int>::deserialize(in, offset, speed);
}

uint32_t VerifyOptoTune::serializedSize() const
{
	return ( 2* sizeof(int) );
}

void VerifyOptoTuneResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer< std::map<uint8_t, uint32_t > >::serialize(out, offset, errors);
}

void VerifyOptoTuneResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	errors = Serializer< std::map<uint8_t, uint32_t > >::deserialize(in, offset);

}

uint32_t VerifyOptoTuneResults::serializedSize() const
{

	return Serializer< std::map<uint8_t, uint32_t > >::size(errors);
}

