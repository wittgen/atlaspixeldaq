#include "DigitalInjectionTest.h"

#ifdef __XMK__
#include "IblBoc.h"
#include <chrono>
#include <thread>
#include <arpa/inet.h>
#endif

DigitalInjectionTest::DigitalInjectionTest() { 

  setResultPtr();

  //Default values 

  // Channels to scan
  Channels = 0x00010000;
  // Base ToT of each digital hit; used to set CalPulse
  ToT_Base = 5; 	
  // ToT increase for each mask step
  ToT_MaskStepIncrease = 1; 
  // ToT increase for each additional chip
  ToT_NextChipIncrease = 1; 
  // Delay between trigger and readout
  TrigDelay = 16;
  // Number of frames to read back per trigger
  TrigCount = 4;
  // Hit Disc Cng
  HitDiscCnfg = 2;
  // Histogrammer mode
  HistogrammerMode = SHORT_TOT;
  // Number of mask steps in mask (options are 1, 2, 4, 8, 16, 32, 672)
  MaskStepsInMask = 8;
  // Number of mask steps to execute (out of total)
  MaskStepsToDo = 1;
  // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col
  DoubleColumnMode = 0x0;
  // Number of double columns to scan over
  NDCLoops = 1;
  // double column to begin with
  StartDCCol = 0;
  //Number of triggers to send
  NTriggers = 1;
  // send data to Fit server?
  sendToFitServer = false;
  // port to connect to fit server
  fitServerPort = 5901;
  // return a histo from the boc fifo?
  doBocFifoHisto = true;
  // from which Rx channel to take boc fifo histo
  bocFifoChannel = 16;

  verbose = false;
}


void DigitalInjectionTest::execute() {

#ifdef __XMK__

  // *****************************
  // Initialize Scan Variables
  // *****************************

  // initialize scan results
  uint16_t histo[80][336] = {{0}};
  
  printOutParams();

  // *****************************
  // Initialize Slaves and Histogrammer for Scan
  // *****************************

  initializeSlaves();

  // *****************************
  // Initialize FE for scan and enable Boc channels
  // *****************************

  initializeFrontEnd();

  // *****************************
  // Loop over Mask
  // *****************************

  for(uint32_t iMask = 0 ; iMask < MaskStepsToDo ; ++iMask) {
    std::cout<<std::endl;
    std::cout<<"++++ Beginning mask stage: "<<(int)iMask<<" out of "<<int(MaskStepsToDo)<<" total loops"<<std::endl;
    std::cout<<std::endl;
    
    m_fei4.setRunMode(false); 
    setupMask(iMask);
    startHistogramming(iMask);

  // *****************************
  // Loop over DC
  // *****************************

    for(uint32_t colAddr = StartDCCol ; colAddr < StartDCCol + NDCLoops ; colAddr++) {
      if (verbose) std::cout<<"++++++ Beginning DC LOOP with column address: "<<(int)colAddr<<std::endl;
      
      m_fei4.setRunMode(false);
      m_fei4.writeRegister(&Fei4::CP,DoubleColumnMode);
      m_fei4.writeRegister(&Fei4::Colpr_Addr, colAddr);
      m_fei4.setRunMode(true);
      
	for(uint32_t iTrig = 0 ; iTrig < NTriggers ; ++iTrig) {
	  m_fei4.calTrigger(TrigDelay);
	  
     std::this_thread::sleep_for(std::chrono::microseconds(100));
	  readBackData(histo);
	  
	} // iTrig
      } // colAddr
    stopHistogramming(iMask);
  } // iMask;

  m_fei4.setRunMode(false);

  // copy debug ToT to command result
  std::copy(histo[0], histo[0]+26880, (result.Histo)[0]);

  std::cout<<std::endl;
  std::cout<<" *****************************"<<std::endl;
  std::cout<<"Done with executing the Digital Scan! Hopefully it worked"<<std::endl;
  std::cout<<" *****************************"<<std::endl;
  std::cout<<std::endl;

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
 
}

void DigitalInjectionTest::printOutParams() {
  std::cout<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"Starting DigitalInjectionTest"<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"Scan variables are: "<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<"Channels = 0x"<< std::hex<<(int)Channels<<std::dec<<std::endl;
  std::cout<<"ToT_Base = "<< (int)ToT_Base<<std::endl;
  std::cout<<"ToT_MaskStepIncrease = "<< (int)ToT_MaskStepIncrease<<std::endl;
  std::cout<<"ToT_NextChipIncrease = "<< (int)ToT_NextChipIncrease<<std::endl;
  std::cout<<std::endl;
  std::cout<<"TrigDelay = "<< (int)TrigDelay<<std::endl;
  std::cout<<"TrigCount = "<< (int)TrigCount<<std::endl;
  std::cout<<"HitDiscCnfg = "<< (int)HitDiscCnfg<<std::endl;
  std::cout<<"HistogrammerMode = "<< (int)HistogrammerMode <<std::endl;
  std::cout<<std::endl;
  std::cout<<"MaskStepsInMask = "<< (int)MaskStepsInMask<<std::endl;
  std::cout<<"MaskStepsToDo = "<< (int)MaskStepsToDo<<std::endl;
  std::cout<<"DoubleColumnMode = "<< (int)DoubleColumnMode <<std::endl;
  std::cout<<"NDCLoops = "<< (int)NDCLoops<<std::endl;
  std::cout<<"StartDCCol = "<< (int)StartDCCol<<std::endl;
  std::cout<<"NTriggers = "<< (int)NTriggers<<std::endl;
  std::cout<<std::endl;
  std::cout<<"sendToFitServer = "<< (int)sendToFitServer <<std::endl;
  std::cout<<"doBocFifoHisto = "<< (int)doBocFifoHisto <<std::endl;
  std::cout<<"bocFifoChannel = "<< (int)bocFifoChannel<<std::endl;
  std::cout<<std::endl;
  std::cout<<"verbose = "<<(int)verbose<<std::endl;
  std::cout<<"--------------------------------"<<std::endl;
  std::cout<<std::endl;
}

void DigitalInjectionTest::initializeSlaves() {
#ifdef __XMK__
  // enable link between BOC and ROD
  m_slave.enableRxLink(Channels);

 if ((Channels & 0x0000FFFF) > 0) {
   m_useSlave0 = true;
   m_slave.setId(0);
   configureHistogrammer(0, m_histCfgOut0, m_netCfgOut0);
 }
 if ((Channels & 0xFFFF0000) > 0) {
   m_useSlave1 = true;
   m_slave.setId(1);
   configureHistogrammer(1, m_histCfgOut1, m_netCfgOut1);
 }
#endif
 }

#ifdef __XMK__
void DigitalInjectionTest::configureHistogrammer(uint8_t slaveID, IblSlvHistCfg & histCfgOut, IblSlvNetCfg & netCfgOut) {
  // Network parameters for fit-farm
  unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x03}; 
  unsigned char lIp[4] = {192, 168, 1, 81}; // 81 for pc 1, 151 for pc 2
  unsigned char maskIp[4] = {255,255, 0,0};
  unsigned char gwIp[4] = {192, 168, 0, 100};
  unsigned char tIpA[4] = {192, 168, 0, 100};
  unsigned char tIpB[4] = {192, 168, 0, 101};

  uint32_t tPA = fitServerPort;
  uint32_t tPB = fitServerPort+1;
  std::cout<<std::dec<<"Fit farm target port A, B = "<<(int)tPA<<", "<<(int)tPB<<std::endl; 

  IblRodSlave::createMacAddress(mac[2], mac[3], mac[4], mac[5], mac[6], mac[7], netCfgOut.localMac);
  IblRodSlave::createIp4Address(lIp[0], lIp[1], lIp[2], lIp[3], &netCfgOut.localIp);
  IblRodSlave::createIp4Address(maskIp[0], maskIp[1], maskIp[2], maskIp[3], &netCfgOut.maskIp);
  IblRodSlave::createIp4Address(gwIp[0], gwIp[1], gwIp[2], gwIp[3], &netCfgOut.gwIp);
  
  IblRodSlave::createIp4Address(tIpA[0], tIpA[1], tIpA[2], tIpA[3], &netCfgOut.targetIpA);
  IblRodSlave::createIp4Address(tIpB[0], tIpB[1], tIpB[2], tIpB[3], &netCfgOut.targetIpB);

  netCfgOut.targetPortA = htonl(tPA);
  netCfgOut.targetPortB = htonl(tPB);

  if (sendToFitServer)  m_slave.setNetCfg(&netCfgOut,slaveID);

    for (int i = 0; i<2; i++) {
      histCfgOut.cfg[i].nChips = 8; //  FeI4 only for now
      histCfgOut.cfg[i].enable = 0;    
      histCfgOut.cfg[i].type = HistogrammerMode;
      if (MaskStepsInMask ==1) histCfgOut.cfg[i].maskStep = 0;  
      else if (MaskStepsInMask ==2) histCfgOut.cfg[i].maskStep = 1;
      else if (MaskStepsInMask ==4) histCfgOut.cfg[i].maskStep = 2;
      else if (MaskStepsInMask ==8) histCfgOut.cfg[i].maskStep = 3;
      else histCfgOut.cfg[i].maskStep = 0;

      histCfgOut.cfg[i].chipSel = 1;  
      histCfgOut.cfg[i].addrRange = 0; 
      histCfgOut.cfg[i].scanId = 1234;
      histCfgOut.cfg[i].binId = 0; 
      histCfgOut.cfg[i].expectedOccValue =NTriggers;
    }
    
    if (slaveID == 0) {
      if ((Channels & 0x000000FF) > 0)  histCfgOut.cfg[0].enable = 1;
      if ((Channels & 0x0000FF00) > 0)  histCfgOut.cfg[1].enable = 1;
    }
    else if (slaveID == 1) {
      if ((Channels & 0x00FF0000) > 0)  histCfgOut.cfg[0].enable = 1;
      if ((Channels & 0xFF000000) > 0)  histCfgOut.cfg[1].enable = 1;
    }
}
#endif

void DigitalInjectionTest::initializeFrontEnd() {
#ifdef __XMK__
  // enable channels
   IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
   if( !iblBocLink->enableChannels(Channels) ) 
     std::cout<<"WARNING: could not enable all the desired channels. Will continue scan anyway"<<std::endl;
   
  // Setup fei4Proxy with the channel map
  // fei4Proxy will forward all commands to all channels (in the map) simultaneously
  // for register reads and writes, it will loop through each fei4 seperately
  // from the set of fei4s held by RodResourceManager
 m_fei4 = Fei4Proxy(Channels);

 // One fei4 object for reading back (not yet implemented through Fei4Proxy) (tmp)
  m_fei4ReadBack = &(RodResourceManagerIBL::getCalibModuleConfig(bocFifoChannel)); 

  m_fei4.setRunMode(false);
  m_fei4.setVerbose(false); 
  m_fei4ReadBack->setVerbose(false); 
  Fei4Data::setVerboseSR(false); // read back Service records 
  if (verbose) Fei4Data::setVerboseSR(true); // read back Service records 

  // Send config to the modules
  m_fei4.sendGlobalConfigAmpsOn();

  // SR_CLOCK 
  m_fei4.writeRegister(&Fei4::SRK, 0x0);
  // HitDiscCnfg
  m_fei4.writeRegister(&Fei4::HD, HitDiscCnfg);
  // Trigger
    m_fei4.writeRegister(&Fei4::Trig_Count, TrigCount);
    m_fei4.writeRegister(&Fei4::PlsrDelay, TrigDelay);
  // Maxi. Trigger Latency
  m_fei4.writeRegister(&Fei4::Trig_Lat, 255 - 8 - TrigDelay - (TrigCount/2));
  // clear pixel capicator regs.... just in case!
  m_mask.set(MASK_CLEAR);
  m_fei4.writeRegister(&Fei4::CP,0x3); // set DC mode to all DC
  m_fei4.strobePixelLatches(m_mask, 0xc1, 0); // write capicitor pixel latches 

  //turn on digital injection
  m_fei4.writeRegister(&Fei4::DHS, 1);
  // Set delay between pulse command and actual pulse to 0
  m_fei4.writeRegister(&Fei4::CalPulseDelay, 0);

#endif 
}


void DigitalInjectionTest::setupMask(uint32_t maskLoop) {
  #ifdef __XMK__
  //Define or shift pixel mask
  //default mask is set to all enable
  if (maskLoop == 0) {
    if (MaskStepsInMask == 2) m_mask.set(MASK_HALF);
    else if (MaskStepsInMask == 4) m_mask.set(MASK_QUARTER);
    else if (MaskStepsInMask == 8) m_mask.set(MASK_EIGHTH);
    else if (MaskStepsInMask == 16) m_mask.set(MASK_16);
    else if (MaskStepsInMask == 32) m_mask.set(MASK_32);
    else if (MaskStepsInMask == 672) m_mask.set(MASK_672);
  }
  else m_mask.shift();

  m_fei4.setRunMode(false); 
  m_fei4.writeRegister(&Fei4::CP,0x3); // set DC mode to all DC
  m_fei4.strobePixelLatches(m_mask, 0x1, 0); // write pixel latches with 1 for output enable
  m_fei4.writeRegister(&Fei4::CP,DoubleColumnMode); // return to desired DC mode

  // set ToT
  uint8_t CalPulse = ToT_Base - 1;
  
  // For testing purposes, give each FE and mask step a different ToT pattern
  CalPulse = CalPulse + maskLoop*ToT_MaskStepIncrease; // mask step increase in ToT
  
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
    if (Channels & (1<<i)) {
      CalPulse += ToT_NextChipIncrease; // FE increase in ToT
      Fei4Proxy::writeRegister(&Fei4::CMDcnt12, (CalPulse), (Channels & (1<<i))); 
    }
  }
#endif  
}

void DigitalInjectionTest::readBackData(uint16_t (&histoArray)[80][336]) {
  // for now, only reads back from Boc fifo
  // todo will need to be updated for other readbacks

#ifdef __XMK__
  if (!doBocFifoHisto) return;
  
  for(uint8_t iTrigCount = 0 ; iTrigCount < TrigCount ; ++iTrigCount) {

    //Read back from boc for one chip
    std::vector<int> answer;
    
    answer = m_fei4ReadBack->readFrame();			

    // debug, output to screen
    if (verbose) {
      std::cout<<"**********************************"<<std::endl;
    std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo for Chip 0, ANSWER is: "<<std::endl;
    std::cout<<"**********************************"<<std::endl;
    
    for (unsigned int i = 0; i < answer.size(); i++) {
      if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<answer[i];
      else if (i%3 == 1) std::cout<<std::hex<<answer[i];
      else std::cout<<std::hex<<answer[i]<<std::dec<<std::endl;
    }
    std::cout<<"**********************************"<<std::endl;
    std::cout<<"**********************************"<<std::endl;
    }
    
    // Only sends back the data from one chip!
    Fei4Data::fillPixDataArray(answer, m_fei4ReadBack->getValue(&Fei4Cfg::HD), histoArray);
  }
#endif
}

void DigitalInjectionTest::startHistogramming(uint32_t maskLoop) {
#ifdef __XMK__
  // only start and stop histogrammer at each mask loop step for masks 2, 4, 8
  if (!(MaskStepsInMask == 2 || MaskStepsInMask == 4 || MaskStepsInMask == 8) && !(maskLoop == 0)) return;
  if (m_useSlave0) m_slave.startHisto(&m_histCfgOut0,0);
  if (m_useSlave1) m_slave.startHisto(&m_histCfgOut1,1);
  std::cout<<"----started histogrammer(s)"<<std::endl;
#endif
}

void DigitalInjectionTest::stopHistogramming(uint32_t maskLoop) {
#ifdef __XMK__
  // only start and stop histogrammer at each mask loop step for masks 2, 4, 8
  if (!(MaskStepsInMask == 2 || MaskStepsInMask == 4 || MaskStepsInMask == 8) && !((int)maskLoop == (MaskStepsToDo-1))) return;
  if (m_useSlave0) m_slave.stopHisto(0);
  if (m_useSlave1) m_slave.stopHisto(1);
  std::cout << "----stopped histogrammer(s)" << std::endl;
#endif  
}
