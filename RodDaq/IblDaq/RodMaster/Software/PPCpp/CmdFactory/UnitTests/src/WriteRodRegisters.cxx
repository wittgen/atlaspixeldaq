#include "WriteRodRegisters.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

WriteRodRegisters::WriteRodRegisters()
{
   setResultPtr(); // necessary step to tell command where the result class is
}

// Inside execute() the actual meat of the command happens on the ppc
void WriteRodRegisters::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);

  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

  IblRod::EpcBase::write(epcBaseValue);
  IblRod::EpcLocal::write(epcLocalValue);
  IblRod::SlaveA::RegBase::write(epcSlvARegsValue);
  IblRod::SlaveA::Ram::write(epcSlvARamValue);
  IblRod::SlaveB::RegBase::write(epcSlvBRegsValue);
  IblRod::SlaveB::Ram::write(epcSlvBRamValue);
  IblRod::EpcBoc::write(epcBocValue);
  IblRod::EpcLocalCtrl::write(epcLocalCtrlValue);
  // IblRod::EpcLocalStat::write(epcLocalStatValue);//todo readonly? but doesn't say it is...
  IblRod::EpcLocalSpMask::write(epcLocalSpMaskValue);
  IblRod::DspRegBase::write(dspRegBaseValue);
  IblRod::SlaveA::Fmt0StatusReg::write(fmt0RegBaseValue);
  IblRod::SlaveA::Fmt1StatusReg::write(fmt1RegBaseValue);
  //  IblRod::DesignRegPPC::write(designRegPPCValue);//todo check this is readonly 

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
void WriteRodRegisters::serialize(uint8_t *out) const
{
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, epcBaseValue);
  Serializer<uint32_t>::serialize(out, offset, epcLocalValue);
  Serializer<uint32_t>::serialize(out, offset, epcSlvARegsValue);
  Serializer<uint32_t>::serialize(out, offset, epcSlvARamValue);
  Serializer<uint32_t>::serialize(out, offset, epcSlvBRegsValue);
  Serializer<uint32_t>::serialize(out, offset, epcSlvBRamValue);
  Serializer<uint8_t>::serialize(out, offset,  epcBocValue);
  Serializer<uint32_t>::serialize(out, offset, epcLocalCtrlValue);
  Serializer<uint32_t>::serialize(out, offset, epcLocalStatValue);
  Serializer<uint32_t>::serialize(out, offset, epcLocalSpMaskValue);
  Serializer<uint32_t>::serialize(out, offset, dspRegBaseValue);
  Serializer<uint32_t>::serialize(out, offset, fmt0RegBaseValue);
  Serializer<uint32_t>::serialize(out, offset, fmt1RegBaseValue);
  Serializer<uint32_t>::serialize(out, offset, designRegPPCValue);
}

// Deseralize the data members of the command
void WriteRodRegisters::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  epcBaseValue = Serializer<uint32_t>::deserialize(in, offset);
  epcLocalValue = Serializer<uint32_t>::deserialize(in, offset);
  epcSlvARegsValue = Serializer<uint32_t>::deserialize(in, offset);
  epcSlvARamValue = Serializer<uint32_t>::deserialize(in, offset);
  epcSlvBRegsValue = Serializer<uint32_t>::deserialize(in, offset);
  epcSlvBRamValue = Serializer<uint32_t>::deserialize(in, offset);
  epcBocValue = Serializer<uint8_t>::deserialize(in, offset);
  epcLocalCtrlValue = Serializer<uint32_t>::deserialize(in, offset);
  epcLocalStatValue = Serializer<uint32_t>::deserialize(in, offset);
  epcLocalSpMaskValue = Serializer<uint32_t>::deserialize(in, offset);
  dspRegBaseValue = Serializer<uint32_t>::deserialize(in, offset);
  fmt0RegBaseValue = Serializer<uint32_t>::deserialize(in, offset);
  fmt1RegBaseValue = Serializer<uint32_t>::deserialize(in, offset);
  designRegPPCValue = Serializer<uint32_t>::deserialize(in, offset);  

}

// Define the size of the serialized command
uint32_t WriteRodRegisters::serializedSize() const
{
  return  (13*sizeof(uint32_t)+ sizeof(uint8_t));
}
