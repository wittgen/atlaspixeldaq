#include "RegisterReadBack.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "IblConnectivity.h"
#include "IblBocLink.h"
#include <iomanip>
#include <chrono>
#include <thread>
#endif

RegisterReadBack::RegisterReadBack() {
  setResultPtr();
  valueToWrite = 0; 
}

void  RegisterReadBack::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
  
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  iblBocLink->enableChannels(0xFFFFFFFF);
  uint32_t channels = iblBocLink->getEnChMask();
  
   std::cout<<"enabled channel mask = 0x"<<std::hex<<channels<<std::dec<<std::endl;

   Fei4 * fes = RodResourceManagerIBL::getCalibModuleConfigs();

    uint16_t regValue = 0; 

    std::cout<<"Writing: 0x"<<std::hex<<(int)valueToWrite<<" to error mask register 0"<<std::dec<<std::endl;

    for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
      regValue = 0xdead;

    if (channels & (1<<i)) {
      regValue = 0;
      fes[i].writeRegister(&Fei4::ErrorMask_0,valueToWrite);
      fes[i].rdRegister(3);
      std::this_thread::sleep_for(std::chrono::microseconds(100));
      
    std::vector<int> current_frame;
    
    current_frame = fes[i].readFrame(true);

    if (current_frame.size() > 5)
      regValue = ((current_frame[current_frame.size()-3]&0x00FF)<<8) + (((current_frame[current_frame.size() - 2])&0xFF));

    if (regValue != valueToWrite) {
      std::cout<<"Rx: "<<(int)i<<": ERROR! Read back 0x"<<std::hex<<(int)regValue<<" when I expected: 0x"<<(int)valueToWrite<<std::dec<<std::endl;
      std::cout<<"Dumping raw frame: 0x";
      for (size_t i = 0; i < current_frame.size(); i++) 
	std::cout<<std::hex<<(int)current_frame[i];
      std::cout<<std::dec<<std::endl;
    }
    }
    result.regValues[i] = regValue;
    }
#endif
}

