/*
 *
 */
#include <bitset>
#include "DelayScan.h"
#include "RodResourceManager.h"
#include "PixScanBase.h"
#include "OptoScan.h"

DelayScan::DelayScan() {

	setResultPtr();
	delayStart =0;//ns
	delayStop =50;//ns
	nDelays =100;
}

void DelayScan::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

	uint32_t bitPattern = 0xBFE;
	uint32_t nBitPattern = 11;

	std::cout << "Running DelayScan" << std::endl;

	uint32_t rxMask =0;
	uint32_t txMask =0;

	Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

	 for(uint8_t iMod = 0 ; iMod < 32 ; ++iMod){
	    //Only channels belonging to my mask!!!
	    if (fes[iMod].revision ==0 )continue;

              uint32_t txCh = fes[iMod].getTxCh();
              uint32_t rxCh = fes[iMod].getRxCh();

              rxMask |= (1<<rxCh);
              txMask |= (1<<txCh);

	      std::cout << "Mod: " << (int)iMod <<" RX: "<<(int)rxCh<< " Master Fibre: " << (int)IblBoc::getMasterFibreFromRxCh(rxCh)<< " Slave Fibre: " << (int)IblBoc::getSlaveFibreFromRxCh(rxCh)<<std::endl;

	      if(IblBoc::getSlaveFibreFromRxCh(rxCh)%24 == 0){
	      std::cout<<"No alt link available"<<std::endl;
	      return;
	      }
	  }

	if ((txMask & 0x00FF00FF) == 0 || txMask == 0 || (rxMask & 0xFF00FF00) == 0 || rxMask == 0){
	std::cout<<"No alt link available or wrong mask: Tx 0x"<<std::hex<<txMask<<" rxMask 0x"<<rxMask<<std::dec<<std::endl;
	return;
	}

	PixLib::PixScanBase::MccBandwidth MCCSpeed = PixLib::PixScanBase::DOUBLE_80;
	IblBoc::PixSetSpeed( MCCSpeed );

	RodResourceManagerIBL::getIblBocLink()->enableRxChannels(rxMask);

	SerialPort* sPort =NULL;
	sPort = RodResourceManagerPIX::getSerialPort();
	sPort->setTxMask(txMask);
	Fei3Mod Fei3;
	Fei3.setTxLink(sPort);

	//create bitStream to compare
	std::vector <uint8_t> CompareBitStream;
	std::vector <uint8_t> MaskBitStream;

	uint32_t nBitsCheck = OptoScan::createCompareBitStream (bitPattern, nBitPattern, CompareBitStream, MaskBitStream);

	std::cout<<"Compare stream size: "<<nBitsCheck<<" bits"<<std::endl;

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	  if (rxMask & (1<<rxCh)){
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareLoad(rxCh, CompareBitStream, MaskBitStream);
	  IblBoc::PixCompareEnable(rxCh);
	}

	float delayDelta = (delayStop - delayStart)/(float)nDelays;
	//Master delay loop
	for (int nDelM = 0; nDelM < nDelays; nDelM++ ){
	  float iDelayMaster = (delayStart + nDelM*delayDelta + delayDelta/2.);
	  //std::cout << " -> Scanning Delay " <<  iDelay << std::endl;
	  int coarseDM = (int)(1000*iDelayMaster*8/25000);
	  int fineDM = (int)((1000*iDelayMaster - coarseDM*3125)/35);
	  //Setting Master fibre delay
	   OptoScan::setDelay(rxMask,coarseDM, fineDM);
	   std::cout<<"Setting master delay: "<<iDelayMaster<<"ns, Coarse "<<(int)coarseDM<<" Fine "<<(int)fineDM<<std::endl;
	   std::map< uint8_t, std::vector< uint32_t > > slaveDel;

	      //Slave delay loop
	      for (int nDelS = 0; nDelS < nDelays; nDelS++ ){
	        Fei3.writeMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay

	        float iDelaySlave = (delayStart + nDelS*delayDelta + delayDelta/2.);
	  	int coarseDS = (int)(1000*iDelaySlave*8/25000);
	  	int fineDS =  (int) ((1000*iDelaySlave - coarseDS*3125)/35);
		//Setting Slave fibre delay
	   	OptoScan::setDelaySlaveFibre(rxMask,coarseDS, fineDS);
	   	Fei3.mccGlobalResetMCC();
	   	Fei3.writeMCCRegister(CSR, 0x0040 | (int8_t)MCCSpeed );//setMCCspeed

	   	std::map<uint8_t, uint32_t> err = OptoScan::getOptoErrors(rxMask, 10, nBitsCheck, bitPattern, nBitPattern, Fei3);

		for (std::map<uint8_t, uint32_t >::iterator it = err.begin(); it!=err.end(); ++it)slaveDel[it->first].push_back(it->second);
		}//Delay Slave loop
	     for (std::map<uint8_t, std::vector<uint32_t> >::iterator it2 = slaveDel.begin(); it2!=slaveDel.end(); ++it2)result.DelayMap[it2->first].push_back(it2->second);
	    }//Delay Master loop

	OptoScan::cleanUp(rxMask,Fei3);

#endif
}

void DelayScan::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<float>::serialize(out, offset, delayStart);
	Serializer<float>::serialize(out, offset, delayStop);
	Serializer<int>::serialize(out, offset, nDelays);
	
}

void DelayScan::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<float>::deserialize(in, offset, delayStart);
	Serializer<float>::deserialize(in, offset, delayStop);
	Serializer<int>::deserialize(in, offset, nDelays);
}

uint32_t DelayScan::serializedSize() const
{
	return ( 1* sizeof(int) + 2* sizeof(float) );
}

void DelayScanResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer< std::map <uint8_t, std::vector< std::vector<uint32_t> > > >::serialize(out, offset, DelayMap);
}

void DelayScanResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<std::map <uint8_t, std::vector< std::vector<uint32_t> > > >::deserialize(in, offset, DelayMap);
}

uint32_t DelayScanResults::serializedSize() const
{
	uint32_t sizeMap =  Serializer< std::map<uint8_t, std::vector< std::vector<uint32_t> > > >::size(DelayMap);
	return sizeMap;
}

