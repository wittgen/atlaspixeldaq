/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 17-Septemer-2015
 *
 * Get going with L1/L2 Opto-Link Tunning with low-level host command feature
 *
 */

#include <stdlib.h>
#include <iomanip>
#include "DummyOptoTune.h"

void DummyOptoTune::execute() {
#ifdef __XMK__

//  When known, PPC code to send bit pattern to FE via BOC,
//              and then generate result histogram data goes here.
//  This will involve:
//    1)  Loading BOC class objects here with threshold and other parameters sentfrom host (e.g.ntriggers).
//    2)  Starting BOC class functions from here to send pattern and receive results.
//    3)  If (2) does not already, make sure result data is available here to be loaded into result.ErrorRate vector.

//  Until we have real result data to work with to do the above, 
//  generate fake results to stuff into the result.ErrorRate vector for now:

  DummyResultArray filleresultArray;
  for(size_t i = 0; i < 2; i++) for(size_t j = 0; j < 8; j++) filleresultArray.values[j][i]=0;

  std::vector<uint32_t> randResults[8];
     
  //  It would be nice if we could actually generate the distributions on the PPC, but alas, not for now
  //  Instead just send back the distributions generated on the host:
  if(optoEmu){
    for (size_t i = 0; i < nThr; i++) {
      for (size_t j = 0; j < 8; j++) {
        filleresultArray.values[j][0] = ErrorRate.at(i).values[j][0];
        filleresultArray.values[j][1] = ErrorRate.at(i).values[j][1];
      }
      result.ErrorRate.push_back(filleresultArray);
    }
  }else{ //  At least we can use rand() on the PPC
    srand (time(NULL)); //  Set seed for random number generator
    for(size_t i = 0; i < 8; i++) {
      rand100k(nThr, randResults[i]);
    }
    //  Generate PPC copies of result variables to be tranferred back to host
    for (size_t i = 0; i < nThr; i++) {
      for (size_t j = 0; j < 8; j++) {
        filleresultArray.values[j][0] = nTriggers;
        filleresultArray.values[j][1] = (uint32_t)(((double)nTriggers)*((double)randResults[j].at(i)/100000.0f));
      }
      result.ErrorRate.push_back(filleresultArray);
    }
  } //  No optoEmu

  result.nThr=result.ErrorRate.size();

  // //  Dump out result values if you want to make sure they are the same as reported by the host
  // for(size_t i=0; i < 8; i++) {
  //   for(size_t j=0; j < result.nThr; j++) {
  //     std::cout << result.ErrorRate.at(j).values[i][1] << " ";
  //   }
  //   std::cout << std::endl;
  // }

 
#endif
}

void DummyOptoTune::rand100k(size_t n, std::vector<uint32_t> &rv) {
  for(size_t i=0; i < n; i++) {
    long long v=rand();
    rv.push_back((uint32_t)(v-100000*(v/100000)));
  }
}

void DummyOptoTune::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, optoEmu);
        Serializer<uint32_t>::serialize(out, offset, nTriggers);
        Serializer<uint32_t>::serialize(out, offset, nThr);
        size_t sz=thrSteps.size();
        for(size_t i=0; i < sz; i++) {
          Serializer<uint32_t>::serialize(out, offset, thrSteps.at(i));
        }
        if(optoEmu){
          for(size_t i=0; i < nThr; i++) {
            for (size_t j = 0; j < 2; j++) {
              for (size_t k = 0; k < 8; k++) {
                Serializer<uint32_t>::serialize(out, offset, ErrorRate.at(i).values[k][j]); //  To forward Opto Tune random numbers to PPC
              }
            }
          }
        }
}
void DummyOptoTune::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        uint32_t dummy = 0;
        DummyResultArray filleresultArray;
        Serializer<uint32_t>::deserialize(in, offset, optoEmu);
        Serializer<uint32_t>::deserialize(in, offset, nTriggers);
        Serializer<uint32_t>::deserialize(in, offset, nThr);
        for(size_t i=0; i < nThr; i++) {
          thrSteps.push_back(dummy); //  Vector on PPC hasn't been created yet.
          Serializer<uint32_t>::deserialize(in, offset, thrSteps.at(i));
        }
        if(optoEmu){
          for(size_t i=0; i < nThr; i++) {
            ErrorRate.push_back(filleresultArray); //  Random number vector on PPC hasn't been created yet.
            for (size_t j = 0; j < 2; j++) {
              for (size_t k = 0; k < 8; k++) {
                Serializer<uint32_t>::deserialize(in, offset, ErrorRate.at(i).values[k][j]); //  To forward Opto Tune random numbers to PPC
              }
            }
          }
        }
}
uint32_t DummyOptoTune::serializedSize() const
{
        if(optoEmu) return (3 + thrSteps.size() + 8*2*nThr)*sizeof(uint32_t);
        else return (3 + thrSteps.size())*sizeof(uint32_t);
}

void DummyOptoTuneResults::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, nThr);
        for(size_t i=0; i < nThr; i++) {
          for (size_t j = 0; j < 2; j++) {
            for (size_t k = 0; k < 8; k++) {
              Serializer<uint32_t>::serialize(out, offset, ErrorRate.at(i).values[k][j]); //  To forward Opto Tune Results to host
            }
          }
        }

}
void DummyOptoTuneResults::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        DummyResultArray filleresultArray;
        Serializer<uint32_t>::deserialize(in, offset, nThr);
        for(size_t i=0; i < nThr; i++) {
          ErrorRate.push_back(filleresultArray); //  Result vector on host hasn't been created yet.
          for (size_t j = 0; j < 2; j++) {
            for (size_t k = 0; k < 8; k++) {
              Serializer<uint32_t>::deserialize(in, offset, ErrorRate.at(i).values[k][j]); //  To forward Opto Tune Results to host
            }
          }
        }
}
uint32_t DummyOptoTuneResults::serializedSize() const
{
        return (1 + 8*2*nThr)*sizeof(uint32_t);
}
