/*
 *
 */

#include <iomanip>
#include "MCCConnScan.h"
#include "RodResourceManager.h"
#include "PixScanBase.h"
#include "OptoScan.h"
MCCConnScan::MCCConnScan() {

	setResultPtr();
	txMask = 0xFFFFFFFF;
        rxMask = 0xFFFFFFFF;
	thrStart =1000;
	thrStop =41000;
	thrStep =5000;
	delayStart =0;
	delayStop =900;//Coarse delay*100 + Fine delay
	delayStep =100;
}

void MCCConnScan::execute() {
#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

	uint32_t bitPattern = 0xBFE;
	uint32_t nBitPattern = 1;
	uint32_t nTriggers =1;

	for(int i=0;i<32;i++)for(int j=0;j<32;j++)result.MapActivity[i][j]=0;
	std::cout << "Running MCCConnScan" << std::endl;

	PixLib::PixScanBase::MccBandwidth MCCSpeed = PixLib::PixScanBase::SINGLE_40;
	IblBoc::PixSetSpeed( MCCSpeed );

	RodResourceManagerIBL::getIblBocLink()->enableRxChannels(rxMask);


	//create bitStream to compare
	std::vector <uint8_t> CompareBitStream;
	std::vector <uint8_t> MaskBitStream;

	uint32_t nBitsCheck = OptoScan::createCompareBitStream (bitPattern, nBitPattern, CompareBitStream, MaskBitStream);

	std::cout<<"Compare stream size: "<<nBitsCheck<<" bits"<<std::endl;

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	  if (rxMask & (1<<rxCh)){
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareLoad(rxCh, CompareBitStream, MaskBitStream);
	  IblBoc::PixCompareEnable(rxCh);
	}

	Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
	Fei3Mod dummyCfg = fes[0];

	for(uint8_t txCh=0;txCh<32;txCh++){
	 if ( !(txMask & (1<<txCh))) continue;

	 dummyCfg.setTxCh(txCh);
	 std::cout<<"Scanning TxCh: "<<(int)txCh<<std::endl;

	//Start with the loops (threshold, delay)		   	
	for (uint32_t iDelay = delayStart;iDelay <= delayStop ;iDelay +=delayStep) {		
	  //std::cout << " -> Scanning Delay " <<  iDelay << std::endl;
	  dummyCfg.writeMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay
  	  OptoScan::setDelay(rxMask,iDelay/100, iDelay%100);
	  dummyCfg.mccGlobalResetMCC();
	  dummyCfg.writeMCCRegister(CSR, 0x0040 | (int8_t)MCCSpeed );//setMCCspeed

	  	for (  uint32_t iThr = thrStart;iThr<= thrStop; iThr += thrStep) {
		  //std::cout << "Scanning Threshold " << iThr << std::endl;

		  OptoScan::setThreshold(rxMask, iThr);

	               std::map<uint8_t, uint32_t> err = OptoScan::getOptoErrors(rxMask, nTriggers, nBitsCheck, bitPattern, nBitPattern, dummyCfg);
			for (std::map<uint8_t, uint32_t >::iterator it = err.begin(); it!=err.end(); ++it)
			  if(it->second ==0){
				result.MapActivity[txCh][it->first]++;
				//std::cout<<"Activity found on TX: "<<(int)txCh<<" and RX: "<<(int)it->first<<std::endl;
			  }

		}//Thr loop
	    }//Delay loop

	}//Tx loop

	SerialPort* sPort =NULL;
	sPort = RodResourceManagerPIX::getSerialPort();
	Fei3Mod Fei3;
	sPort->setTxMask(txMask);
	Fei3.setTxLink(sPort);

	OptoScan::cleanUp(rxMask, Fei3);


#endif
}

void MCCConnScan::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset, rxMask);
	Serializer<uint32_t>::serialize(out, offset, txMask);
	Serializer<uint32_t>::serialize(out, offset, thrStart);
	Serializer<uint32_t>::serialize(out, offset, thrStop);
	Serializer<uint32_t>::serialize(out, offset, thrStep);
	Serializer<uint32_t>::serialize(out, offset, delayStart);
	Serializer<uint32_t>::serialize(out, offset, delayStop);
	Serializer<uint32_t>::serialize(out, offset, delayStep);
	
}

void MCCConnScan::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<uint32_t>::deserialize(in, offset, rxMask);
	Serializer<uint32_t>::deserialize(in, offset, txMask);
	Serializer<uint32_t>::deserialize(in, offset, thrStart);
	Serializer<uint32_t>::deserialize(in, offset, thrStop);
	Serializer<uint32_t>::deserialize(in, offset, thrStep);
	Serializer<uint32_t>::deserialize(in, offset, delayStart);
	Serializer<uint32_t>::deserialize(in, offset, delayStop);
	Serializer<uint32_t>::deserialize(in, offset, delayStep);
}

uint32_t MCCConnScan::serializedSize() const
{
	return 8 * sizeof(uint32_t);
}

void MCCConnScanResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	for(int i=0;i<32;i++)for(int j=0;j<32;j++)Serializer<uint16_t>::serialize(out, offset, MapActivity[i][j]);
}

void MCCConnScanResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	for(int i=0;i<32;i++)for(int j=0;j<32;j++)Serializer<uint16_t>::deserialize(in, offset, MapActivity[i][j]);
}

uint32_t MCCConnScanResults::serializedSize() const
{
		return 32*32* sizeof(uint16_t);
}

