/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "TestSlave.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#include "IblRodSlave.h"
#include <arpa/inet.h>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

TestSlave::TestSlave(){ 
  setResultPtr(); 
  talkToFitFarm= false;  // attempt connection to fit farm?
  whichSlave = 1;
  for (int i = 0; i<4; i++) {
    fitPorts[i] = 0;
    fitIps[i][0] = 0; fitIps[i][1] = 0;
    fitIps[i][2] = 0; fitIps[i][3] = 0;
  }
}

// inside execute() the actual meat of the command happens on the ppc
void TestSlave::execute() {

  result.success = true;
#ifdef __XMK__

  bool success = true;

  IblRodSlave s;
  std::cout<<"about to send set id command for slave "<<(int)whichSlave<<std::endl;
  success = s.setId(whichSlave);
  std::cout<< (success ? "set slave id" : "failed to set slave id") <<std::endl;
  result.success &= success;
  success = s.setVerbose(whichSlave,1);
  std::cout<< (success ? "set slave verbose" : "failed to set slave verbose") <<std::endl;
  result.success &= success;


 IblSlvNetCfg netCfgOut;

 IblRodSlave::getDefaultNetConfig(whichSlave, netCfgOut);

  uint32_t tPA = whichSlave ? fitPorts[2] : fitPorts[0];
  uint32_t tPB = whichSlave ? fitPorts[3] : fitPorts[1];

  std::cout<<std::dec<<"Fit farm target port A, B = "<<(int)tPA<<", "<<(int)tPB<<std::endl; 

  netCfgOut.targetPortA = htonl(tPA);
  netCfgOut.targetPortB = htonl(tPB);

  char buffer[100];

  // fit server target ip from scan config
  if (whichSlave) {
    IblRodSlave::createIp4Address(fitIps[2][0], fitIps[2][1], fitIps[2][2], fitIps[2][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(fitIps[3][0], fitIps[3][1], fitIps[3][2], fitIps[3][3], &netCfgOut.targetIpB);
    snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", fitIps[2][0], fitIps[2][1], fitIps[2][2], fitIps[2][3]); std::cout << buffer << std::endl;
    snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", fitIps[3][0], fitIps[3][1], fitIps[3][2], fitIps[3][3]); std::cout << buffer << std::endl;
  } else {
    IblRodSlave::createIp4Address(fitIps[0][0], fitIps[0][1], fitIps[0][2], fitIps[0][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(fitIps[1][0], fitIps[1][1], fitIps[1][2], fitIps[1][3], &netCfgOut.targetIpB);
     snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", fitIps[0][0], fitIps[0][1], fitIps[0][2], fitIps[0][3]); std::cout << buffer << std::endl;
     snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", fitIps[1][0], fitIps[1][1], fitIps[1][2], fitIps[1][3]); std::cout << buffer << std::endl;
  }

  if (talkToFitFarm)   {
    success = s.setNetCfg(&netCfgOut,whichSlave);
    std::cout<< (success ? "connected to fit farm" : "failed to connect to fit farm") <<std::endl;
    result.success &= success;
  }

  IblSlvHistCfg histCfgOut;
  histCfgOut.cfg[0].nChips = 8; //  Fei4 only for now
  histCfgOut.cfg[0].enable = 1;     //enable histo0 unit
  histCfgOut.cfg[0].type = ONLINE_OCCUPANCY;
  histCfgOut.cfg[0].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
  histCfgOut.cfg[0].chipSel = 1;    //0 for single chip(1 for all chips)
  histCfgOut.cfg[0].addrRange = 0;  //single chip <32K( full range < 1M)
  histCfgOut.cfg[0].scanId = 0x1234;//remote use only
  histCfgOut.cfg[0].binId = 1;
  histCfgOut.cfg[0].expectedOccValue = 1;
 
  histCfgOut.cfg[1].nChips = 8; //  Fei4 only for now
  histCfgOut.cfg[1].enable = 0;     //enable histo1 unit
  histCfgOut.cfg[1].type = ONLINE_OCCUPANCY;
  histCfgOut.cfg[1].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
  histCfgOut.cfg[1].chipSel = 1;    //0 for single chip(1 for all chips)
  histCfgOut.cfg[1].addrRange = 0;  //single chip <32K( full range < 1M)
  histCfgOut.cfg[1].scanId = 0x1234;//remote use only
  histCfgOut.cfg[1].binId = 1;
  histCfgOut.cfg[1].expectedOccValue = 1;

    success = s.startHisto(&histCfgOut,whichSlave); 
    std::cout<< (success ? "started histogramming" : "failed to start histogramming") <<std::endl;
  result.success &= success;

  success &= s.stopHisto(whichSlave);
  std::cout<< (success ? "stopped histogramming" : "failed to stop histogramming") <<std::endl;
  result.success &= success;

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
void TestSlave::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,talkToFitFarm);
	Serializer<bool>::serialize(out,offset, whichSlave);
  for (int i = 0; i<4; i++) {
      Serializer<uint32_t>::serialize(out, offset, fitPorts[i]);
      for (int j = 0; j<4; j++) {
        Serializer<uint8_t>::serialize(out, offset, fitIps[i][j]);
     }
  }
}

// Deseralize the data members of the command
void TestSlave::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	talkToFitFarm = Serializer<bool>::deserialize(in,offset);
	whichSlave = Serializer<bool>::deserialize(in,offset);
    for (int i = 0; i<4; i++) {
      fitPorts[i] = Serializer<uint32_t>::deserialize(in, offset);
      for (int j = 0; j<4; j++) {
        fitIps[i][j] = Serializer<uint8_t>::deserialize(in, offset);
      }
    }
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t TestSlave::serializedSize() const
{
  return (2*sizeof(bool)+ 4*sizeof(uint32_t)+16*sizeof(uint8_t));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
void Success::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<bool>::serialize(out, offset, success);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void Success::deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  success= Serializer<bool>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t Success::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(bool);
}
