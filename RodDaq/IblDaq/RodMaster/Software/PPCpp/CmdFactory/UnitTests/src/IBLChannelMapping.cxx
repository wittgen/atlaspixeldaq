/*
 *
 */

#include <iomanip>
#include <chrono>
#include <thread>

#include "IBLChannelMapping.h"
#include "RodResourceManager.h"

IBLChannelMapping::IBLChannelMapping() {

	setResultPtr();
	txMask = 0xFF00FF;
        rxMask = 0xFFFFFFFF;
}

void IBLChannelMapping::execute() {
#ifdef __XMK__

  if(!RodResMgr::isFei4()) return;

  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->enableTxChannels(txMask);

   for(int txCh=0;txCh<32;txCh++){
     for(int rxCh=0;rxCh<32;rxCh++){
       result.chipNumber[txCh][rxCh]=0;
       result.serialNumber[txCh][rxCh]=0;
     }
     if ((txMask & (1<<txCh) ) )sendDefaultConfig(txCh, 8);
   }

   //Set phases to auto
   for(int rxCh=0;rxCh<32;rxCh++)IblBoc::CDRAutoPhase(IblBoc::getFibreFromRxCh(rxCh));

   if(!iblBocLink->enableRxChannels(rxMask))rxMask = iblBocLink->getEnChMask();

    for(int txCh=0;txCh<32;txCh++){
      if ( !(txMask & (1<<txCh) ) )continue;
        for(int chipID=4;chipID<8;chipID++){          
          // Writing to GR #27 with PLL enable
          sendTx(txCh, 0x005a0800|27|(chipID<<6), 0xC0000000);
          //Global pulse
          sendTx(txCh,  0x005a2401|(chipID<<6));
          // Writing to GR #27 with PLL enable
          sendTx(txCh, 0x005a0800|27|(chipID<<6), 0x80000000);
          //read back register 35 (SN) using chipID
          sendTx(txCh,(0x005a0423)|(chipID<<6), 0x0);
            for(int rxCh=0;rxCh<32;rxCh++){
              if ( !(rxMask & (1<<rxCh) ) )continue;
              uint16_t val = readRxFifo(rxCh);
                if(val != 0xdead){
                  result.chipNumber[txCh][rxCh] = chipID;
                  result.serialNumber[txCh][rxCh] = val;
                  std::cout<<"Tx "<<txCh<<" Rx "<<rxCh<<" "<<std::hex<<val<<std::dec<<" "<<chipID<<std::endl;
                }
            }
        }
    }

    //Set phases back to manual, note that the phase is not set!
    for(int rxCh=0;rxCh<32;rxCh++)IblBoc::CDRManPhase(IblBoc::getFibreFromRxCh(rxCh));
#endif
}

void IBLChannelMapping::sendTx(int txCh, uint32_t cmd ,uint32_t value){

  for(int i = 24 ; i >= 0 ; i -= 8)
    IblBoc::write( txCh/16, txCh%16, IblBoc::FifoData, 0xFF&(cmd>>i) );

  for(int i = 24 ; i >= 0 ; i -= 8)
    IblBoc::write( txCh/16, txCh%16, IblBoc::FifoData, 0xFF&(value>>i) );

  uint8_t txCtrl = IblBoc::read( txCh/16, txCh%16, IblBoc::TxControl );
  txCtrl |= 0x44;
  IblBoc::write( txCh/16, txCh%16, IblBoc::TxControl, txCtrl );
    while (!(IblBoc::read(txCh/16,  txCh%16, IblBoc::TxStatus) & 0x1)) {
        std::this_thread::sleep_for(std::chrono::microseconds(10));
    }

   txCtrl &= ~0x44;
   IblBoc::write( txCh/16, txCh%16, IblBoc::TxControl, txCtrl );

}

uint16_t IBLChannelMapping::readRxFifo(int rxCh){

  uint16_t value=0;
  int timeout = 0;
    while(( (value & 0xFF) != 0xEC) && (timeout++<10000)){
      IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataHigh);
      value = IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataLow) & 0xFF;
    }

  value = 0xdead;
    if(timeout<10000 ){
      IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataHigh);
      value = (IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataLow) & 0xFF) <<8;
      IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataHigh);
      value |= IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataLow) & 0xFF;
    }

  //Drain Rx fifo
  uint8_t rxControl = IblBoc::read(rxCh/16, rxCh%16, IblBoc::RxControl);
  IblBoc::write(rxCh/16, rxCh%16, IblBoc::RxControl, rxControl & (~0x1));
    while( (IblBoc::read(rxCh/16, rxCh%16, IblBoc::RxStatus) & 0x20) != 0x20 ){
      IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataHigh);
      IblBoc::read(rxCh/16, rxCh%16, IblBoc::DataLow);
    }
  IblBoc::write(rxCh/16, rxCh%16, IblBoc::RxControl, rxControl);

  return  value;

}

void IBLChannelMapping::sendDefaultConfig(int txCh, int chipID){

  //Set run mode to false
  sendTx(txCh, 0x005a2807|(chipID<<6));
  //Register 20 set THR high
  sendTx(txCh, 0x005a0800|20|(chipID<<6), 0xffff0000);
  //Registers  14-16 LVDS and PLL
  sendTx(txCh, 0x005a0800|14|(chipID<<6), 0xd5260000);
  sendTx(txCh, 0x005a0800|15|(chipID<<6), 0x1a960000);
  sendTx(txCh, 0x005a0800|16|(chipID<<6), 0x00380000);
  //Registers  26-29 PLL, speed, CLK, LVx,...
  sendTx(txCh, 0x005a0800|26|(chipID<<6), 0x01900000);
  sendTx(txCh, 0x005a0800|27|(chipID<<6), 0x80000000);
  sendTx(txCh, 0x005a0800|28|(chipID<<6), 0x82060000);
  sendTx(txCh, 0x005a0800|29|(chipID<<6), 0x00070000);

}

void IBLChannelMapping::serialize(uint8_t *out) const{
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, rxMask);
  Serializer<uint32_t>::serialize(out, offset, txMask);
}

void IBLChannelMapping::deserialize(const uint8_t *in){
  uint32_t offset = 0;
  Serializer<uint32_t>::deserialize(in, offset, rxMask);
  Serializer<uint32_t>::deserialize(in, offset, txMask);
}

uint32_t IBLChannelMapping::serializedSize() const{
  return 2 * sizeof(uint32_t);
}

void IBLChannelMappingResults::serialize(uint8_t *out) const{
  uint32_t offset = 0;
  Serializer<std::array< std::array<int,32>,32 > >::serialize(out,offset,chipNumber);
  Serializer<std::array< std::array<int,32>,32 > >::serialize(out,offset,serialNumber);

}

void IBLChannelMappingResults::deserialize(const uint8_t *in){
  uint32_t offset = 0;
  chipNumber = Serializer<std::array< std::array<int,32>,32 > >::deserialize(in,offset);
  serialNumber = Serializer<std::array< std::array<int,32>,32 > >::deserialize(in,offset);
  
}

uint32_t IBLChannelMappingResults::serializedSize() const{
 size_t sizeMap = Serializer<std::array< std::array<int,32>,32 > >::size(chipNumber) + Serializer<std::array< std::array<int,32>,32 > >::size(serialNumber);
 return sizeMap;
}

