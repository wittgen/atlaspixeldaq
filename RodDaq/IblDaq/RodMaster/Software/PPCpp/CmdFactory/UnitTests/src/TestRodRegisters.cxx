#include "TestRodRegisters.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void TestRodRegisters::dump() {

  std::cout<<"I'm about to output the current"<<
    "value of registers on the ROD"<<std::endl;

  std::cout<<"EpcBase: "<<HEX(result.epcBaseValue)<<std::endl;
  std::cout<<"EpcLocal: "<<HEX(result.epcLocalValue)<<std::endl;
  std::cout<<"SlaveA::RegBase: "<<HEX(result.epcSlvARegsValue)<<std::endl;
  std::cout<<"SlaveA::Ram: "<<HEX(result.epcSlvARamValue)<<std::endl;
  std::cout<<"SlaveB::RegBase: "<<HEX(result.epcSlvBRegsValue)<<std::endl;
  std::cout<<"SlaveB::Ram: "<<HEX(result.epcSlvBRamValue)<<std::endl;
  std::cout<<"EpcBoc: "<<HEX(result.epcBocValue)<<std::endl;
  std::cout<<"EpcLocalCtrl: "<<HEX(result.epcLocalCtrlValue)<<std::endl;
  std::cout<<"EpcLocalStat: "<<HEX(result.epcLocalStatValue)<<std::endl;
  std::cout<<"EpcLocalSpMask: "<<HEX(result.epcLocalSpMaskValue)<<std::endl;
  std::cout<<"DspRegBase: "<<HEX(result.dspRegBaseValue)<<std::endl;
  std::cout<<"DesignRegPPC: "<<HEX(result.designRegPPCValue)<<std::endl;
}

TestRodRegisters::TestRodRegisters(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void TestRodRegisters::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  unsigned value;

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);

  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern
  value = IblRod::EpcBase::read();
  result.epcBaseValue=value;

  value = IblRod::EpcLocal::read();
  result.epcLocalValue=value;

  value = IblRod::SlaveA::RegBase::read();
  result.epcSlvARegsValue=value;

  value = IblRod::SlaveA::Ram::read();
  result.epcSlvARamValue=value;

  value = IblRod::SlaveB::RegBase::read();
  result.epcSlvBRegsValue=value;

  value = IblRod::SlaveB::Ram::read();
  result.epcSlvBRamValue=value;

  value = IblRod::EpcBoc::read();
  result.epcBocValue=value;

  value = IblRod::EpcLocalCtrl::read();
  result.epcLocalCtrlValue=value;

  value = IblRod::EpcLocalStat::read();
  result.epcLocalStatValue=value;

  value = IblRod::EpcLocalSpMask::read();
  result.epcLocalSpMaskValue=value;

  value = IblRod::DspRegBase::read();
  result.dspRegBaseValue=value;

  value = IblRod::DesignRegPPC::read();
  result.designRegPPCValue=value;

  value = IblRod::SlaveA::Fmt0StatusReg::read();
  result.slvA_Fmt0StatusReg = value;

  value = IblRod::SlaveA::Fmt1StatusReg::read();
  result.slvA_Fmt1StatusReg = value;

  value = IblRod::SlaveA::Fmt2StatusReg::read();
  result.slvA_Fmt2StatusReg = value;

  value = IblRod::SlaveA::Fmt3StatusReg::read();
  result.slvA_Fmt3StatusReg = value;

  value = IblRod::SlaveB::Fmt0StatusReg::read();
  result.slvB_Fmt0StatusReg = value;

  value = IblRod::SlaveB::Fmt1StatusReg::read();
  result.slvB_Fmt1StatusReg = value;

  value = IblRod::SlaveB::Fmt2StatusReg::read();
  result.slvB_Fmt2StatusReg = value;

  value = IblRod::SlaveB::Fmt3StatusReg::read();
  result.slvB_Fmt3StatusReg = value;


  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void TestRodRegisters::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
}

// Deseralize the data members of the command
// In this case, only one boolean
void TestRodRegisters::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t TestRodRegisters::serializedSize() const
{
	return sizeof(bool);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void RegisterResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, epcBaseValue);
	  Serializer<uint32_t>::serialize(out, offset, epcLocalValue);
	  Serializer<uint32_t>::serialize(out, offset, epcSlvARegsValue);
	  Serializer<uint32_t>::serialize(out, offset, epcSlvARamValue);
	  Serializer<uint32_t>::serialize(out, offset, epcSlvBRegsValue);
	  Serializer<uint32_t>::serialize(out, offset, epcSlvBRamValue);
	  Serializer<uint8_t>::serialize(out, offset, epcBocValue);
	  Serializer<uint32_t>::serialize(out, offset, epcLocalCtrlValue);
	  Serializer<uint32_t>::serialize(out, offset, epcLocalStatValue);
	  Serializer<uint32_t>::serialize(out, offset, epcLocalSpMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, dspRegBaseValue);
	  Serializer<uint32_t>::serialize(out, offset, designRegPPCValue);

	  Serializer<uint32_t>::serialize(out, offset, slvA_Fmt0StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvA_Fmt1StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvA_Fmt2StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvA_Fmt3StatusReg);

	  Serializer<uint32_t>::serialize(out, offset, slvB_Fmt0StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvB_Fmt1StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvB_Fmt2StatusReg);
	  Serializer<uint32_t>::serialize(out, offset, slvB_Fmt3StatusReg);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void RegisterResult::deserialize(const uint8_t *in) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  epcBaseValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcLocalValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcSlvARegsValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcSlvARamValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcSlvBRegsValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcSlvBRamValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcBocValue = Serializer<uint8_t>::deserialize(in, offset);
	  epcLocalCtrlValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcLocalStatValue = Serializer<uint32_t>::deserialize(in, offset);
	  epcLocalSpMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  dspRegBaseValue = Serializer<uint32_t>::deserialize(in, offset);
	  designRegPPCValue = Serializer<uint32_t>::deserialize(in, offset);

	  slvA_Fmt0StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvA_Fmt1StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvA_Fmt2StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvA_Fmt3StatusReg = Serializer<uint32_t>::deserialize(in, offset);

	  slvB_Fmt0StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvB_Fmt1StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvB_Fmt2StatusReg = Serializer<uint32_t>::deserialize(in, offset);
	  slvB_Fmt3StatusReg = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t RegisterResult::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (19*sizeof(uint32_t)+ sizeof(uint8_t));
}
