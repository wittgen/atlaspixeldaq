/*
  Steve Alkire <alkire@cern.ch> 2014.03.27
  General result patterns. 
*/
#ifndef __GENERAL_RESULTS_H__
#define __GENERAL_RESULTS_H__
#include "EasySerializable.h"
#include "Command.h"

class EmptyResult: public CommandCRTP<EmptyResult >, public EasySerializable{public: SERIAL_H()};

template<class _Command>class EmptyTemplatedResult: public CommandCRTP<EmptyTemplatedResult<_Command> >, public EasySerializable{public: SERIAL_H()};

class SuccessResult: public CommandCRTP<SuccessResult >, public EasySerializable{
 public:
  bool success;
  SERIAL_H(prep(success);)
};
#endif //__GENERAL_RESULTS_H__

