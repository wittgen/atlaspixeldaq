#ifndef _SETNETWORK_H_
#define _SETNETWORK_H_
/*
 * M. Kretz <moritz.kretz@cern.ch>
 * Date: 2014-November-13
 *
 * Sets default network configuration to both slaves.
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class SetNetwork: public CommandCRTP<SetNetwork > { // declares type of command
public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything

	SetNetwork();
	
	virtual void execute();
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~SetNetwork() {}

        /* The slave we want to configure. */
	uint8_t	slaveid;

	/* Do we want to take default settings as calculated by PPC or take the
	 * ones that are supplied here? */
	uint8_t usePpcDefaults;

	/* Maybe the IP arrays are EasySerializable. Don't care for now... */

        /* Slave IP address */
        uint8_t ip_1;
        uint8_t ip_2;
        uint8_t ip_3;
        uint8_t ip_4;

        /* Slave subnet mask */
        uint8_t subnet_1;
        uint8_t subnet_2;
        uint8_t subnet_3;
        uint8_t subnet_4;

        /* Slave default gateway */
        uint8_t gw_1;
        uint8_t gw_2;
        uint8_t gw_3;
        uint8_t gw_4;

        /* FitFarm settings for histogrammer A */
        uint8_t ipA_1;
        uint8_t ipA_2;
        uint8_t ipA_3;
        uint8_t ipA_4;
        uint32_t portA;

        /* FitFarm settings for histogrammer B */
        uint8_t ipB_1;
        uint8_t ipB_2;
        uint8_t ipB_3;
        uint8_t ipB_4;
        uint32_t portB;
};
#endif

