#ifndef _COUNTFMTOCCUPANCY_H_
#define _COUNTFMTOCCUPANCY_H_
/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>,
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "EasySerializable.h"

class RodSlaveRegisters: public CommandCRTP<RodSlaveRegisters >, public EasySerializable { // declares type of command result
 public:

  // All the data we want back from the command
	unsigned long slvA_Fmt0OccupancyMSB;
	unsigned long slvA_Fmt0Occupancy;
	unsigned long slvA_Fmt1OccupancyMSB;
	unsigned long slvA_Fmt1Occupancy;
	unsigned long slvA_Fmt2OccupancyMSB;
	unsigned long slvA_Fmt2Occupancy;
	unsigned long slvA_Fmt3OccupancyMSB;
	unsigned long slvA_Fmt3Occupancy;


	int slvA_Fmt0Occupancy_Link0;
	int slvA_Fmt1Occupancy_Link0;
	int slvA_Fmt2Occupancy_Link0;
	int slvA_Fmt3Occupancy_Link0;
	int slvA_Fmt0Occupancy_Link1;
	int slvA_Fmt1Occupancy_Link1;
	int slvA_Fmt2Occupancy_Link1;
	int slvA_Fmt3Occupancy_Link1;
	int slvA_Fmt0Occupancy_Link2;
	int slvA_Fmt1Occupancy_Link2;
	int slvA_Fmt2Occupancy_Link2;
	int slvA_Fmt3Occupancy_Link2;
	int slvA_Fmt0Occupancy_Link3;
	int slvA_Fmt1Occupancy_Link3;
	int slvA_Fmt2Occupancy_Link3;
	int slvA_Fmt3Occupancy_Link3;


	unsigned long slvB_Fmt0OccupancyMSB;
	unsigned long slvB_Fmt0Occupancy;
	unsigned long slvB_Fmt1OccupancyMSB;
	unsigned long slvB_Fmt1Occupancy;
	unsigned long slvB_Fmt2OccupancyMSB;
	unsigned long slvB_Fmt2Occupancy;
	unsigned long slvB_Fmt3OccupancyMSB;
	unsigned long slvB_Fmt3Occupancy;

	int slvB_Fmt0Occupancy_Link0;
	int slvB_Fmt1Occupancy_Link0;
	int slvB_Fmt2Occupancy_Link0;
	int slvB_Fmt3Occupancy_Link0;
	int slvB_Fmt0Occupancy_Link1;
	int slvB_Fmt1Occupancy_Link1;
	int slvB_Fmt2Occupancy_Link1;
	int slvB_Fmt3Occupancy_Link1;
	int slvB_Fmt0Occupancy_Link2;
	int slvB_Fmt1Occupancy_Link2;
	int slvB_Fmt2Occupancy_Link2;
	int slvB_Fmt3Occupancy_Link2;
	int slvB_Fmt0Occupancy_Link3;
	int slvB_Fmt1Occupancy_Link3;
	int slvB_Fmt2Occupancy_Link3;
	int slvB_Fmt3Occupancy_Link3;

	//	SERIAL_H();

	SERIAL_H( prep(slvA_Fmt0Occupancy_Link0); prep(slvA_Fmt1Occupancy_Link0); prep(slvA_Fmt2Occupancy_Link0); prep(slvA_Fmt3Occupancy_Link0);
	          prep(slvA_Fmt0Occupancy_Link1); prep(slvA_Fmt1Occupancy_Link1); prep(slvA_Fmt2Occupancy_Link1); prep(slvA_Fmt3Occupancy_Link1);
	          prep(slvA_Fmt0Occupancy_Link2); prep(slvA_Fmt1Occupancy_Link2); prep(slvA_Fmt2Occupancy_Link2); prep(slvA_Fmt3Occupancy_Link2);
	          prep(slvA_Fmt0Occupancy_Link3); prep(slvA_Fmt1Occupancy_Link3); prep(slvA_Fmt2Occupancy_Link3); prep(slvA_Fmt3Occupancy_Link3);
		  prep(slvB_Fmt0Occupancy_Link0); prep(slvB_Fmt1Occupancy_Link0); prep(slvB_Fmt2Occupancy_Link0); prep(slvB_Fmt3Occupancy_Link0); 
		  prep(slvB_Fmt0Occupancy_Link1); prep(slvB_Fmt1Occupancy_Link1); prep(slvB_Fmt2Occupancy_Link1); prep(slvB_Fmt3Occupancy_Link1);
		  prep(slvB_Fmt0Occupancy_Link2); prep(slvB_Fmt1Occupancy_Link2); prep(slvB_Fmt2Occupancy_Link2); prep(slvB_Fmt3Occupancy_Link2); 
		  prep(slvB_Fmt0Occupancy_Link3); prep(slvB_Fmt1Occupancy_Link3); prep(slvB_Fmt2Occupancy_Link3); prep(slvB_Fmt3Occupancy_Link3); )

	  };

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class CountFmtOccupancy: public CommandCRTP<CountFmtOccupancy >, public EasySerializable { // declares type of command
public:
	typedef RodSlaveRegisters ResultType;
	ResultType result;

	static void dump();
	CountFmtOccupancy();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	virtual ~CountFmtOccupancy() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

	SERIAL_H(prep(dumpOnPPC);)
};
#endif //_COUNTFMTOCCUPANCY_H_

