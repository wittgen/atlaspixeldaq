#ifndef _READMODULECONFIG_H_
#define _READMODULECONFIG_H_
/*
 * Author: 
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Nov-25
 *
 * This command is used to read the configuration of modules from the PPC
 * For now, it reads the stored configuation on the PPC
 * Todo: add read back from modules themselves
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Fei4ExtCfg.h" // needed for serialize functions
#include "Fei3ModCfg.h" // needed for serialize functions

#ifdef __XMK__
#include "Fei4.h"
#include "Fei3Mod.h"
#include "IblBocLink.h"
#endif

#include <vector>

// Todo: check that theconfig was correctly sent?
class RedModuleConfig: public CommandCRTP<RedModuleConfig > {
 public:
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;

	uint8_t nMod;
  bool isFei3;
	std::vector<Fei3ModCfg> moduleConfigsFei3;
  std::vector<Fei4ExtCfg> moduleConfigsFei4;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class ReadModuleConfig: public CommandCRTP<ReadModuleConfig > {
public:
  InstantiateResult(RedModuleConfig)

	enum ConfigType { Physics, Calib };

	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

  uint32_t Channels;  //hard coded right now

	ReadModuleConfig();  
	virtual ~ReadModuleConfig() {}

	void readPhysicsConfig(){ m_ConfigType = Physics;};
	void readCalibConfig(){ m_ConfigType = Calib;};

	void setFeMask(uint32_t mask) {m_feMask = mask;};
	void setReadFrontEnd(bool readFrontEnd) {m_readFrontEnd = readFrontEnd;}
  void setFeType(std::string feType) {if (feType == "I3") m_isFei3 = true; else m_isFei3 = false;}

protected:

private:
	uint32_t m_feMask;
	bool m_readFrontEnd; //read from front end or rod
	ConfigType m_ConfigType;
  bool m_isFei3;
};
#endif //_READMODULECONFIG_H_

