/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>,
 * A. Serrano <ana.serrano@cern.ch>
 * Date: 2014-VII-14
 *
 * 
 */

#ifndef __READ_UART_OUTPUT_H__
#define __READ_UART_OUTPUT_H__

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class UartOutput: public CommandCRTP<UartOutput > { // declares type of command result
public:

	// All the data we want back from the command
	char master_output[16*1024+4]; // Todo: use std::string
	char slvA_output[16*1024+4]; // Todo: use std::string
	char slvB_output[16*1024+4]; // Todo: use std::string

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class ReadUartOutput: public CommandCRTP<ReadUartOutput > { // declares type of command
public:
	typedef UartOutput ResultType;
	ResultType result;

	void dump();
	ReadUartOutput();

	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~ReadUartOutput() {}

	// the only parameter of this command
	bool	dumpOnPPC;

};

#endif // __READ_UART_OUTPUT_H__

