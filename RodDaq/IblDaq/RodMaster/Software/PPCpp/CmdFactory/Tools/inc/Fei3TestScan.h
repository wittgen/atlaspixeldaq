/*
 * Fei3TestScan
 * By: Nick Dreyer
 * Inject hits into FE for testing digital scan functionality
 */

#ifndef _FEI3TESTSCAN_H_
#define _FEI3TESTSCAN_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"
#include "IblRodSlave.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class Fei3TestScanValue: public CommandCRTP<Fei3TestScanValue > {
  public:
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;

    uint8_t nMod;
    std::vector<uint8_t> data;
};

class Fei3TestScan: public CommandCRTP<Fei3TestScan > {
  public:
    InstantiateResult(Fei3TestScanValue)

    virtual void execute();
    virtual const Command* getResult() const {
	    return &result;
    }

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    Fei3TestScan();
    virtual ~Fei3TestScan() {}

    void showTriggerData( const std::vector<uint8_t>& rxData );
    void initializeMCC();
    void setModMask(uint32_t mask) {m_modMask = mask;}
    void setNTrig(uint32_t nTrig) {m_nTrig = nTrig;}

    uint32_t iMask;
    uint32_t histoFilter;
    IblSlvHistCfg histConfig_in;

  protected:

  private:
    uint32_t m_modMask;
    uint32_t m_nTrig;
};

#endif //  _FEI3TESTSCAN_H_

