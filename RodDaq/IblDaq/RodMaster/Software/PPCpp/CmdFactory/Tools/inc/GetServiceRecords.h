/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 25 Sept 2015
 * Description: Get value of most recent service records read back (usually read back after configuring, 
 * depending on parameters of SendModuleConfig command) for a single module = 2 FE.
 */

#ifndef __SERVICERECORD_READBACK_H__
#define __SERVICERECORD_READBACK_H__

#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"


class SRValues: public CommandCRTP<SRValues >, public EasySerializable {
 public:
  uint16_t srValues[32][32];
  virtual ~SRValues() {};

  SERIAL_H(prep(srValues,32);)

};

class GetServiceRecords: public CommandCRTP<GetServiceRecords >, public EasySerializable { 
 public:
  InstantiateResult(SRValues)
  
    virtual void execute();
  GetServiceRecords(); 
  virtual ~GetServiceRecords() {}

  SERIAL_H();

    };

#endif // 

