#ifndef _STARTHISTO_H_
#define _STARTHISTO_H_
/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 28-August-2015
 *
 * Sets up slave to be ready for histogramming
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
#include "IblRodSlave.h"

class HistCfgCheck: public CommandCRTP<HistCfgCheck > {
 public:
        virtual ~HistCfgCheck() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        uint32_t whichSlave;
        IblSlvHistCfg histConfig;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class StartHisto: public CommandCRTP<StartHisto > { // declares type of command
 public:
  InstantiateResult(HistCfgCheck);

  virtual const Command* getResult() const {
      return &result;
  }
  
  StartHisto() {setResultPtr();}
  virtual ~StartHisto() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  uint32_t whichSlave;
  IblSlvHistCfg histConfig_in;
  IblSlvHistCfg histConfig_out;
  
};
#endif // _STARTHISTO_H_

