/* Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 25 June 2015
 * Small command that sets pixels we want to expliclty disable during scanning
 */

#ifndef __SET_SCAN_PIXEL_DISABLE__
#define __SET_SCAN_PIXEL_DISABLE__

#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class SetScanPixelDisable: public CommandCRTP<SetScanPixelDisable >, public EasySerializable { 
 public:
  InstantiateResult(EmptyResult)
  
    virtual void execute();
  SetScanPixelDisable(); 
  virtual ~SetScanPixelDisable() {}

  uint32_t rx;
  uint8_t dc;
  uint32_t pixel;
  bool clear;
  bool clearAdd;

    SERIAL_H(prep(rx); prep(dc); prep(pixel); prep(clear); prep(clearAdd);)
    };

#endif // 

