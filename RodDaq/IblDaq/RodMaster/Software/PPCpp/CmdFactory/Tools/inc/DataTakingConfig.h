/*
 * Author: L. Jeanty <laura.jeanty@cern.ch>
 * Date: 3-Sept-2014
 * Description: this command initializes data taking configuration with different parameters,
 * including different emulator modes. It is designed to replace the various IBLDataTakingWith.... commands
 *
 * To-do: there is a lot of overlap of functionality between the functions available here (DataTakingConfig + DataTakingTools)
 * and the functions available in IblScan. At some point in the future, we should consolidate, put the functionality
 * in a central location, and use the same functions in both places.
 * 
 */

#ifndef __DATA_TAKING_CONFIG__
#define __DATA_TAKING_CONFIG__

#include "CppCompatibility.h"
#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"
#include "iblEnums.h"
#include "EmuUtils.h"

#include "IblBoc.h"

class DataTakingConfig: public CommandCRTP<DataTakingConfig >, public EasySerializable { 
 public:
  InstantiateResult(EmptyResult)

  virtual void execute();
  DataTakingConfig(); 
  virtual ~DataTakingConfig() {;};

  void printOut();
  static void initialize();
  static void setRodID(uint32_t rodID);
  static void initRodFeEmu(uint32_t hits, uint8_t frameCount, IblBoc::PixSpeed speed);
  static void initSlaves();
  static void setRoLMode(bool emu);
  static void setPhysicsMode(bool physics);
  static void setFmtReadoutTimeoutLimit(uint32_t limit);
  static void setRODFrameCount(uint32_t count);
  static void initFrontEnd(uint32_t &channels,uint8_t frameCount, uint8_t trigLatency, IblBoc::PixSpeed speed);
  static void initBocEmu(bool bocEmu, uint32_t &channels, uint8_t frameCount, IblBoc::PixSpeed speed, uint32_t hits, EmuUtils::OCC_MODE occMode = EmuUtils::OCC_MODE::LANDAU);
  static void setFmtHeaderTrailerLimit(uint32_t limit);
  static void setFmtRodBusyLimit(uint32_t limit);
  static void setFmtDataOverflowLimit(uint32_t limit);

  uint32_t channels; // which channels to configure / enable 
  uint32_t rodID; // ROD ID 
  uint32_t nHitsPerTrigger; // number of hits per trigger if using BOC FE emulator
  uint32_t fmtReadoutTimeoutLimit; // how long the formatter should wait if 1 channel doens't send data
  uint32_t fmtHeaderTrailerLimit; // how many hits to process per frame
  uint32_t fmtRodBusyLimit; // how full the fifo is before asserting busy
  uint32_t fmtDataOverflowLimit; // how full the fifo is before asserting busy
  uint32_t fmtTrailerTimeoutLimit; // how full the fifo is before asserting busy
  uint32_t efbMaskValue; // efb mask in master for disabled EFBs
  uint32_t vetoAtECR ; // Leng of time of VETO fast command from TIM 
  uint32_t busyAtECR; // Lenght of busy time at each ECR...should be < 1 ms to not introduce dead time in ATLAS
  uint32_t resetAfterECR; // Time after ECR when the ROD FPGA ECR reset (8 BC long) is performed
  uint8_t frameCount; // number of frames to read/expect per trigger
  uint8_t trigLatency; // front end trigger latency  
  uint32_t bcidOffset; // offset between the BCID arriving to the ROD Master from the TIM and the BCID sent back from the module to the Fmt
  SLinkEnableMode slinkMode; //  SlinkIgnoreFTK,SlinkEnableFTKXOff, SlinkEnableFTKLDown, SlinkEnableFTKBoth  mode
  bool bocFeEmu; // use the BOC FE-I4/MCC emulator
  bool rodFeEmu; // use the ROD MCC emulator
  bool initRealFE; // initialize the real FE?
  bool rolEmu; // use the ROL emulator?
  bool physicsMode; // set the ROD into physics mode? (false = calibration mode) 
  bool doECRReset; // reset ECR counter to 0?
  IblBoc::PixSpeed pixSpeed;
  bool feSynchAtECR; // Perform FE sync (Fast command at ECR), just for FE-I3

  SERIAL_H(prep(channels); prep(rodID); prep(nHitsPerTrigger); prep(fmtReadoutTimeoutLimit); prep(frameCount); prep(trigLatency);
	   prep(bcidOffset); prep(slinkMode); prep(bocFeEmu); prep(rodFeEmu); prep(initRealFE); prep(rolEmu); prep(physicsMode);  prep(doECRReset); prep(fmtHeaderTrailerLimit);
	   prep(fmtRodBusyLimit); prep(fmtDataOverflowLimit); prep(fmtTrailerTimeoutLimit); prep(efbMaskValue); prep(vetoAtECR); prep(busyAtECR); prep(resetAfterECR);prep(pixSpeed); prep(feSynchAtECR);)
    
};

#endif 

