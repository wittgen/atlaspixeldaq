#ifndef LogPublisherCmd_H
#define LogPublisherCmd_H

#include "CppCompatibility.h"
#include "Command.h"
#include "LogPublisher.h"

/// NoValue object defines undefined value. Note that value 99 is interpreted as "NotDefined" state for numeric types

 class LPNoValue
 {
 public:
        LPNoValue() {}

        template <class T>
        T as() const { return 999; }

        template <class T>
        void set(T & x) { x = as<T>(); }
 };

 template <>
 inline std::string LPNoValue::as() const { return ""; }
 template <>
 inline uint8_t LPNoValue::as() const { return 99; } // for char 99 = c !

 // comparison operators
template<class T>
bool operator == (const T& x, const LPNoValue &y){ return (x==y.as<T>()) ;}
template<class T>
bool operator != (const T& x, const LPNoValue &y){ return (x!=y.as<T>()) ;}


//---------------------------------------------------------------------------


struct LPConfig {
    LPConfig();

    int mode;
    std::string receiver_ip;
    uint16_t 	receiver_port;
    int	time_standby_max;
    int log_switch;
    int slave_verbose;
    int sleep;
    int verbosity;
    std::string print() const;
};

//---------------------------------------------------------------------------

class LPCmdResult: public CommandCRTP<LPCmdResult > {
    public:

    LPCmdResult() { setResultPtr(); }

    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;

    LPConfig rconfig;

};



class LogPublisherCmd: public CommandCRTP<LogPublisherCmd > {

public:


    InstantiateResult(LPCmdResult)

    LogPublisherCmd()
    {
        setResultPtr();

    }

    virtual void execute();

    void setConfig(LogPublisher * logger) const;
    void getConfig(LogPublisher * logger);   // passes information to result object

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    LPConfig config;



protected:

private:

};



#endif // RODUDPLOG_H

