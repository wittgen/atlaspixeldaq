
#ifndef _GETGADC_OUTPUT_H_
#define _GETGADC_OUTPUT_H_

#include "Command.h"
#include "Serializer.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif

//GADC Select code Names
const static std::string GADCSelectNames[8] = { "TEMP", "REF", "GROUND", "ANALOG_MUX", "ANALOG_REG", "DAC_PULSER", "ANALOG_VOL", "ILEAK" };

class GetGADCOutputResults: public CommandCRTP<GetGADCOutputResults > {
 public:
        virtual ~GetGADCOutputResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map <uint8_t, std::vector<std::vector <uint32_t > > > GADC;
};

class GetGADCOutput: public CommandCRTP<GetGADCOutput > { // declares type of command
 public:

 //GADC Select code, enum identify different select to be multiplexer to the generic ADC in FEI4
 enum GADCSelect {TEMP=0, REF, GROUND, ANALOG_MUX, ANALOG_REG, DAC_PULSER, ANALOG_VOL, ILEAK, GADC_SELECT_CODES};

  InstantiateResult(GetGADCOutputResults);

  virtual const Command* getResult() const {
      return &result;
  }

  GetGADCOutput();
  virtual ~GetGADCOutput() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  std::map < uint8_t, std::vector < uint32_t > > getGADC(uint32_t rxMask,  GADCSelect select);

  uint32_t m_rxMask;
  GADCSelect m_GADCSel;
  uint32_t m_GADCVref;

};
#endif // _GETGADC_OUTPUT_H_


