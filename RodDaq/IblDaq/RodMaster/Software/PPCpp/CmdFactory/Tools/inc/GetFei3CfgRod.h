#ifndef _GetFei3CfgRod_H_
#define _GetFei3CfgRod_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"
#include "Fei3GlobalCfg.h"
#include "Fei3PixelCfg.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class GetFei3CfgRodResult: public CommandCRTP<GetFei3CfgRodResult > {
 public:
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  //Results returned by this command
  std::map <uint32_t, std::map<uint32_t, Fei3GlobalCfg> > GlobalCfg;
  std::map <uint32_t, std::map<uint32_t, Fei3PixelCfg > > PixelCfg;
};

class GetFei3CfgRod: public CommandCRTP<GetFei3CfgRod > {
  public:
  
    typedef GetFei3CfgRodResult ResultType;
    ResultType result;
  
    virtual const Command* getResult() const {
      return &result;
    }

    virtual void execute();
    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    void printParams();

    GetFei3CfgRod();
    virtual ~GetFei3CfgRod() {}

    enum OpMode { GlobalRegReadback, PixelRegReadback, FullFE };
    OpMode m_opMode;
    static std::string opMode[];
    static std::string pixRegName[];
    uint32_t m_rxMask; // To be refined with better assignment
    uint16_t m_feMask;

  protected:
  private:
};
#endif
