/*
 * Authors: 
 * Daniela Boerner
 *
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class ReadIblRodStatusResult: public CommandCRTP<ReadIblRodStatusResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t masterBusyMaskValue;
	uint32_t masterBusyCurrentStatusValue;
 	uint32_t rodMasterBusy;

	uint32_t slave1BusyMaskValue;
	uint32_t slave1BusyForceValue;
	uint32_t slave1BusyCurrentStatusValue;
  	uint32_t outputBusyMask_slvA;
	uint32_t slave2BusyMaskValue;
	uint32_t slave2BusyForceValue;
	uint32_t slave2BusyCurrentStatusValue;
  	uint32_t outputBusyMask_slvB;
	uint32_t iblRodFmtBusyStatus;
	uint16_t efbBusyMask;

        uint32_t slvA_FmtLinkEnable;
	uint32_t slvB_FmtLinkEnable;
	uint32_t iblRodFmtLinkEnable;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class IblRodStatus: public CommandCRTP<IblRodStatus > { // declares type of command
public:
	typedef ReadIblRodStatusResult ResultType;
	ResultType result;
 
	void dump();
	IblRodStatus();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~IblRodStatus() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

};

