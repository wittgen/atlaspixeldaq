/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 12 Aug 2014
 * Description: this command will inject a small number of pixels with a calibration pulse and then trigger with a user defined pause
 * If the noise parameter is true, then rather than injecting the chip will only send noise hits
 * 
 */

#ifndef __PRODUCE_DATA__
#define __PRODUCE_DATA__

#include "CppCompatibility.h"
#include "Command.h"
#include "RodResourceManager.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class ProduceData: public CommandCRTP<ProduceData >, public EasySerializable { 
 public:
  InstantiateResult(EmptyResult)

    virtual void execute();
  ProduceData(); 
  virtual ~ProduceData() {}

  void debugRead(uint8_t trigCount, Fei4* fe, uint8_t rx);

  uint32_t sleep;
  uint32_t nTriggers;
  uint32_t channels;
  uint8_t trigCount;
  uint8_t trigDelay;
  uint8_t trigLatency;
  uint32_t maskSteps;
  uint8_t dcMode;
  uint8_t rawToT;
  bool noise;
  bool debug;
  uint8_t debugRx;

  SERIAL_H(prep(sleep); prep(nTriggers); prep(channels);
	   prep(trigCount); prep(trigDelay); prep(trigLatency);
	   prep(maskSteps); prep(dcMode); prep(rawToT);
	   prep(noise); prep(debug); prep(debugRx);)
    };

#endif // __IBL_DATA_TAKING_WITH_EMULATOR__

