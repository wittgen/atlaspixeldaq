/*
 * Authors: 
 * M. Backhaus <backhaus@cern.ch>
 * Date: 5 May 2014
 *
 * This resets the busy histogram in the ROD master and ROD slave
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/tools/resetRodBusyHistograms.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "EasySerializable.h"
#include "Command.h"

class ResetRodBusyHistogramsResult: public CommandCRTP<ResetRodBusyHistogramsResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	bool	resetSuccessful;


	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class ResetRodBusyHistograms: public CommandCRTP<ResetRodBusyHistograms > { // declares type of command
public:
	typedef ResetRodBusyHistogramsResult ResultType;
	ResultType result;
 
	void dump();
	ResetRodBusyHistograms();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~ResetRodBusyHistograms() {}

	// the only parameter of this command
 	bool	dumpOnPPC;
};

