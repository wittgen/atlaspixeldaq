#ifndef _STOPHISTO_H_
#define _STOPHISTO_H_



#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
#include "IblRodSlave.h"

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class StopHisto: public CommandCRTP<StopHisto > { // declares type of command
 public:

  StopHisto() {  whichSlave = 0;}
  
  virtual ~StopHisto() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  uint32_t whichSlave;
  
  
};
#endif // _STOPHISTO_H_

