#ifndef __STAGE_H__
#define __STAGE_H__
/*
  Steve Alkire <alkire@cern.ch> 2014.03.18
  This is a nice place to put functions for returning XMK-only pointers.
*/
#include "ScanBoss.h"
#include "Fei4PixelCfg.h"

//extern static CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_Bit,Fei4PixelCfg::n_DC> ScanBoss::iterFei4PixelCfg;
struct Stage{
  //examples
  //static std::string* getHowDoIFeel();
  //testing
  static DoubleColumnBit* testIter_f(){return xmk(&ScanBoss::testIterSB[0]);}

  template<typename T> static T* xmk(T* const o){
    #ifdef __XMK__
    return o;
    #else
    return NULL;
    #endif
  }
};

#endif //__STAGE_H__
