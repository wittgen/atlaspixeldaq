/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 5-V-2014
 * Description: this command prepares data-taking using the BOC FE EMU
 * 
 */

#ifndef __IBL_DATA_TAKING_WITH_EMULATOR__
#define __IBL_DATA_TAKING_WITH_EMULATOR__

#include "CppCompatibility.h"
#include "Command.h"
#include "RodResourceManager.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class IblDataTakingWithEmulator: public CommandCRTP<IblDataTakingWithEmulator >, public EasySerializable { 
 public:
  InstantiateResult(EmptyTemplatedResult<IblDataTakingWithEmulator>)

  virtual void execute();
  IblDataTakingWithEmulator(){ setResultPtr(); } 
  virtual ~IblDataTakingWithEmulator() {}

  SERIAL_H()
};

#endif // __IBL_DATA_TAKING_WITH_EMULATOR__

