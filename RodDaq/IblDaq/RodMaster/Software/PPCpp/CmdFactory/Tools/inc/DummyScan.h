#ifndef _DUMMYSCAN_H_
#define _DUMMYSCAN_H_
/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 13-November-2015
 *
 * PPC Dummy Scan Driver
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
#include "IblRodSlave.h"

class DummyHistCfgCheck: public CommandCRTP<DummyHistCfgCheck > {
 public:
        virtual ~DummyHistCfgCheck() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        IblSlvHistCfg histConfig;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class DummyScan: public CommandCRTP<DummyScan > { // declares type of command
 public:
  InstantiateResult(DummyHistCfgCheck);

  virtual const Command* getResult() const {
      return &result;
  }
  
  DummyScan() {setResultPtr();}
  virtual ~DummyScan() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  enum TestHistoStatus injectionType;

  // command parameters of this command
  uint32_t whichSlave;
  uint32_t histoUnit;
  uint32_t inj_Type;
  uint32_t multiHit;
  uint32_t scanType;
  IblSlvHistCfg histConfig_in;
  IblSlvHistCfg histConfig_out;
  
};
#endif // _DUMMYSCAN_H_

