/*
 * F. Meloni <federico.meloni@cern.ch>
 * Clean Opto results
 */

#ifndef _CLEAN_DSP_RESULTS_H_
#define _CLEAN_DSP_RESULTS_H_

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class CleanDSPResults: public CommandCRTP<CleanDSPResults > { // declares type of command
 public:
  InstantiateResult(EmptyResult);

  virtual const Command* getResult() const {
      return &result;
  }
  
  CleanDSPResults();
  virtual ~CleanDSPResults() {}
  
  virtual void execute();
  uint8_t rxCh;

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

};
#endif // _CLEAN_DSP_RESULTS_H_

