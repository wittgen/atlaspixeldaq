#ifndef _GETRXCONFIG_H_
#define _GETRXCONFIG_H_
/*
 * F. Meloni <federico.meloni@cern.ch>
 * Get Rx Threshold and Delay (and Gain)
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class GetRxConfigResults: public CommandCRTP<GetRxConfigResults > {
 public:
        virtual ~GetRxConfigResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        // Results returned by this command
	uint8_t coarseDelay;
	uint8_t fineDelay;	 
	uint16_t rxThreshold;
	uint16_t rxGain;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class GetRxConfig: public CommandCRTP<GetRxConfig > { // declares type of command
 public:
  InstantiateResult(GetRxConfigResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  GetRxConfig() { setResultPtr();}
  virtual ~GetRxConfig() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  int32_t rxCh;

};
#endif // _GETRXCONFIG_H_

