#ifndef _WRITEPIXELREGISTERCONFIG_H_ 
#define _WRITEPIXELREGISTERCONFIG_H_
/*
  Steve Alkire <alkire@cern.ch> 2014.03.27
  Use if you want to write just pixel registers.
  Usage: Choose your mask. Push back Fei4PixelCfgs corresponding lowest masked bit first.
*/
#include "CppCompatibility.h"
#include "Command.h"
#include "Fei4PixelCfg.h"
#include "RodResourceManager.h"
#include <string>
#include <vector>
#include "GeneralResults.h"
#include "EasySerializable.h"
#ifdef __XMK__
#include "Fei4.h"
#endif

//To-do: Check that mask and # of Configs is =
class WritePixelRegisterConfig: public CommandCRTP<WritePixelRegisterConfig >, public EasySerializable { 
 public:
  InstantiateResult(EmptyTemplatedResult<WritePixelRegisterConfig>)
  uint32_t feMask;
  enum ConfigType { Calib, Physics };
  ConfigType configType;
  
  virtual void execute(){}
  WritePixelRegisterConfig(uint32_t feMask_rhs, ConfigType configType_rhs):feMask(feMask_rhs),configType(configType_rhs){setResultPtr();}
  WritePixelRegisterConfig():feMask(0x00010000),configType(Physics){setResultPtr();} 
  virtual ~WritePixelRegisterConfig() {}
  std::vector<Fei4PixelCfg> pixelCfg; //a vector is not ideal and maybe should change
  SERIAL_H(prep(configType); prep(feMask); prep(pixelCfg);)
  void deserialize(const uint8_t *in);
};
#endif // _WRITEPIXELREGISTERCONFIG_H_

