#ifndef _SETUART_H_
#define _SETUART_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2014-April-4
 *
 * Sets the uart for the kermit
 * 0 is master, 1 is slave 0, 2 is slave 2
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class SetUart: public CommandCRTP<SetUart > { // declares type of command
public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything

	SetUart();
	
	virtual void execute();
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~SetUart() {}

	// the only parameter of this command
	uint8_t	uart;
};
#endif

