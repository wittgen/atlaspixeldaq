/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2016-II-23
 * Description: tools to readback registers from Fei3 modules
 */

#ifndef _FEI3READBACK_H_
#define _FEI3READBACK_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"
#include "Fei3GlobalCfg.h"
#include "Fei3PixelCfg.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class Fei3ReadbackResult: public CommandCRTP<Fei3ReadbackResult > {
 public:
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  //Results returned by this command
  std::map <uint32_t, std::map<uint32_t, Fei3GlobalCfg> > GlobalCfg;
  std::map <uint32_t, std::map<uint32_t, Fei3PixelCfg > > PixelCfg;
};

class Fei3Readback: public CommandCRTP<Fei3Readback > {
  public:
  
    typedef Fei3ReadbackResult ResultType;
    ResultType result;
  
    virtual const Command* getResult() const {
      return &result;
    }

    virtual void execute();
    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    void printParams();

    Fei3Readback();
    virtual ~Fei3Readback() {}

    uint32_t m_rxMask; // To be refined with better assignment
    uint16_t m_feMask;

    enum OpMode { GlobalRegReadback, PixelRegReadback, FullFE };
    OpMode m_opMode;
    static std::string opMode[];
    static std::string pixRegName[];

  protected:

  private:
};

#endif // _FEI3READBACK_H_

