
#ifndef _TUNE_GADC_VREF_H_
#define _TUNE_GADC_VREF_H_

#include "Command.h"
#include "Serializer.h"
#include "EasySerializable.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif

class GADCVrefResults: virtual public EasySerializable{
  public:
  uint32_t Vref;
  uint32_t ILeak;
  uint32_t vCalRange;
  const std::size_t size()const {return 3 * sizeof(uint32_t);}
  SERIAL_H(prep(Vref);prep(ILeak);prep(vCalRange); )
};


class TuneGADCVrefResults: public CommandCRTP<TuneGADCVrefResults > {
 public:
        virtual ~TuneGADCVrefResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map< uint8_t, std::vector <GADCVrefResults> > GADCVref;
};

class TuneGADCVref: public CommandCRTP<TuneGADCVref > { // declares type of command
 public:

  InstantiateResult(TuneGADCVrefResults);

  virtual const Command* getResult() const {
      return &result;
  }

  TuneGADCVref();
  virtual ~TuneGADCVref() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  void getRange(uint32_t rxCh, uint32_t vref, GADCVrefResults &res);

  uint32_t m_rxMask;

};
#endif // _TUNE_GADC_VREF_H_


