#ifndef _RESET_SLAVE_H_
#define _RESET_SLAVE_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 20-AUG-2014
 *
 * Resets slave
 * Two options: 1) reset slave network (connection to fit server)
 * 2) Clear and resetart histogramming
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class ResetSlave: public CommandCRTP<ResetSlave >, public EasySerializable { 
 public:

  InstantiateResult(EmptyResult);
  ResetSlave();
  
  virtual void execute();
  
  virtual ~ResetSlave() {};
  
  int whichSlave; // 0 = slave 0, 1 = slave 1, 2 = both slaves
  bool resetHisto;
  bool resetNetwork;

  SERIAL_H(prep(whichSlave); prep(resetHisto); prep(resetNetwork);)
};
#endif

