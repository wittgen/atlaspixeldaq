/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 5-V-2014
 * Description: this command prepares data-taking using the BOC FE EMU
 * 
 */

#ifndef __IBL_DATA_TAKING_WITH_ROLEMULATOR__
#define __IBL_DATA_TAKING_WITH_ROLEMULATOR__

#include "CppCompatibility.h"
#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class IblDataTakingWithRoLEmulator: public CommandCRTP<IblDataTakingWithRoLEmulator >, public EasySerializable { 
 public:
  InstantiateResult(EmptyTemplatedResult<IblDataTakingWithRoLEmulator>)

  virtual void execute();
  IblDataTakingWithRoLEmulator(){ setResultPtr(); } 
  virtual ~IblDataTakingWithRoLEmulator() {}

  SERIAL_H()
};

#endif // __IBL_DATA_TAKING_WITH_ROLEMULATOR__

