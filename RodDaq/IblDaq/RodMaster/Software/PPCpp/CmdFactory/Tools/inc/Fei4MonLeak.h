
#ifndef _FEI4_MON_LEAK_H_
#define _FEI4_MON_LEAK_H_

#include "Command.h"
#include "Serializer.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif

class Fei4MonLeakResults: public CommandCRTP<Fei4MonLeakResults > {
 public:
        virtual ~Fei4MonLeakResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map <uint8_t, std::vector<std::vector <uint32_t > > > MON_ILEAK;
};

class Fei4MonLeak: public CommandCRTP<Fei4MonLeak > { // declares type of command
 public:
  InstantiateResult(Fei4MonLeakResults);

  virtual const Command* getResult() const {
      return &result;
  }

  Fei4MonLeak();
  virtual ~Fei4MonLeak() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  static uint32_t initMonLeak(uint32_t rxMask);
  static void getMonLeakADC(uint32_t rxMask, uint16_t pixelIndex, uint32_t nIter,std::map < uint8_t, std::vector < uint32_t > > &MonADC);
  static void cleanUp(uint32_t rxMask);

  uint32_t m_rxMask;
  uint16_t m_nPixPerFE;

};
#endif //  _FEI4_MON_LEAK_H_


