/*
 * HalfClockReturn
 * By: Laser Kaplan
 * Return half-clock on the MCC
 */

#ifndef _HALFCLOCKRETURN_H_
#define _HALFCLOCKRETURN_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class HalfClockReturnValue: public CommandCRTP<HalfClockReturnValue > {
  public:
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;

    uint8_t nMod;
    std::vector<uint16_t> regs;
};

class HalfClockReturn: public CommandCRTP<HalfClockReturn > {
  public:
    InstantiateResult(HalfClockReturnValue)

    virtual void execute();
    virtual const Command* getResult() const {
	    return &result;
    }

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    HalfClockReturn();
    virtual ~HalfClockReturn() {}

    void setModMask(uint32_t mask) {m_modMask = mask;}

  protected:

  private:
    uint32_t m_modMask;
};

#endif

