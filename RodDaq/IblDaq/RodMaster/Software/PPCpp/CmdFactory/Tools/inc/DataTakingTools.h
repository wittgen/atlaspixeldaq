/*
 * Author: L. Jeanty <laura.jeanty@cern.ch>
 * Date: 4-Sept-2014
 * Description: this command contains various short tools that are useful when configuring the system for data taking
 * Choose which tool you'd like to use with an enum
 * 
 */

#ifndef __DATA_TAKING_TOOLS__
#define __DATA_TAKING_TOOLS__

#include "CppCompatibility.h"
#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"
#include "SingleRead.h"
#include "iblEnums.h"
#include "EmuUtils.h"

#include "IblBoc.h"
#include "IblRodSlave.h"

enum PPCActionType { Print, 
		     FeBCR, FeECR, FeTrigger, FeSendGlobalConfigAmpsOn, FeSendGlobalConfigAmpsOff, FeSendPixelConfig,
		     FeWriteReg, FeSendReg, FeSetRunMode, EnableBocTx,
		     EnableRxPath, DisableRxPath, EnableBocRx, DisableBocRx, EnableFmtLink, EnableFmtLinkAtECR, DisableFmtLink,
		     SetRunNumber, SetECRCounter, ReadECRCounter, SetBcidOffset, SLinkEnable, SetBocEmu, SetRodEmu, 
                     DisableSerialLine, BocResampleRxPhase, SetAmp2VbnStandby, SetPrmpVbpStandby, SetDisVbnStandby,SetEfbMaskReg, SetEfbMaskRegAtECR,
		     SetHitDiscCnfg, SetStandby3DBM, SendConfigAtECR, BocManualConfig, ResetModules,
		     SetRODVetoTimeAtECR, SetRODBusyTimeAtECR, SetRODResetTimeAfterECR, 
                     SetFmtDataOverflowLimit, SetFmtTrailerTimeoutLimit,SetRodBusyMask};


class DataTakingTools: public CommandCRTP<DataTakingTools >, public EasySerializable { 
 public:
  InstantiateResult(RegValue)

  virtual void execute();
  DataTakingTools(); 
  virtual ~DataTakingTools() {;};

  void print();

  static void feBCR(uint32_t channels);
  static void feECR(uint32_t channels);
  static void feTrigger(uint32_t channels);
  static void feSendGlobalConfigAmpsOn(uint32_t channels);
  static void feSendGlobalConfigAmpsOff(uint32_t channels);
  static void feSendPixelConfig(uint32_t channels);
  static void feWriteReg(uint32_t channels, uint32_t address, uint32_t value);
  static void feSendReg(uint32_t channels, uint32_t address);
  static void feSetRunMode(uint32_t channels, uint32_t value);
  static void setHitDiscCnfg(uint32_t channels, uint32_t value);
  static void writeHitDiscConfig(uint32_t channels);
  static void trickHitDiscConfigCalib();

  static void enableRxPath(uint32_t channels);
  static void disableRxPath(uint32_t channels);
  static void enableFmtLink(uint32_t channels);
  static void enableFmtLinkAtECR(uint32_t channels);
  static void disableFmtLink(uint32_t channels);
  static void enableBocRx(uint32_t channels);
  static void enableBocTx(uint32_t channels);
  static void disableBocRx(uint32_t channels);

  static void disableSerialLine();

  static void setRunNumber(uint32_t value);
  static void setECRCounter(uint32_t value);
  static uint32_t readECRCounter();
  static void sLinkEnable(SLinkEnableMode mode);

  static void setBocEmu(bool mode, uint32_t channels, uint8_t frameCount=0x0, IblBoc::PixSpeed speed=IblBoc::Speed_40, uint32_t nHits=0,EmuUtils::OCC_MODE occMode = EmuUtils::OCC_MODE::OCC_FIXED);
  static void setRodEmu(bool mode, uint8_t frameCount, IblBoc::PixSpeed speed);

  static void setBcidOffset(uint32_t value);
  static void bocResampleRxPhase(uint32_t channels);

  static void setAmp2VbnStandby(uint32_t channels, uint8_t value);
  static void setPrmpVbpStandby(uint32_t channels, uint8_t value);
  static void setDisVbnStandby(uint32_t channels, uint8_t value);
  static void setStandby3DBM(uint32_t channels, uint8_t value);

  static void setEfbMaskReg(uint32_t value);
  static void setEfbMaskRegAtECR(uint32_t value);
  static uint8_t computeMasterEfbReg(uint32_t channels);
  static void sendConfigAtECR(uint32_t channels, uint32_t value);
  static void bocManualConfig(uint32_t channels);
  static void resetModules(uint32_t channels, uint32_t value);
  static void setRODVetoTimeAtECR(uint32_t value);
  static void setRODBusyTimeAtECR(uint32_t value);
  static void setRODResetTimeAfterECR(uint32_t value);
  static void setFmtDataOverflowLimit(uint32_t value);
  static void setFmtTrailerTimeoutLimit(uint32_t value);
  static void setRodBusyMask(uint32_t value);
  static uint32_t  computeSlaveBusyMask(uint16_t slavechannels,uint8_t slaveId);
  static uint32_t  computeMasterBusyMask(uint32_t channels);


  PPCActionType action;

  uint32_t channels; // which channels to do your action on, if applicable
  uint32_t value; // what value to write if writing something
  uint32_t address; // what address to write if applicable
  uint8_t frameCount; // number of frames to read/expect per trigger
  IblBoc::PixSpeed pixSpeed; // speed of the communication (40, 80, 160 Mbs)
  

  SERIAL_H(prep(action); prep(channels); prep(value); prep(address); prep(frameCount); prep(pixSpeed);)
    
};

#endif 

