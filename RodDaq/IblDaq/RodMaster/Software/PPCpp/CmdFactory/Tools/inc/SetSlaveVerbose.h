#ifndef _SETSLAVEVERBOSE_H_
#define _SETSLAVEVERBOSE_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2-July-2014
 *
 * Sets slave into verbose mode
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "GeneralResults.h"

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class SetSlaveVerbose: public CommandCRTP<SetSlaveVerbose > { // declares type of command
 public:
  InstantiateResult(EmptyResult); // dummy result doesn't return anything                                                                                                                   
  SetSlaveVerbose();
  
  virtual void execute();
  
  /// Inhertited functions
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  virtual ~SetSlaveVerbose() {}
  
  // command parameters of this command
  uint8_t whichSlave;
  bool verbose;
};
#endif

