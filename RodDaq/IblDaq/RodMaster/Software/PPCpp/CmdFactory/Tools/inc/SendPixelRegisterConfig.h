#ifndef _SENDPIXELREGISTERCONFIG_H_
#define _SENDPIXELREGISTERCONFIG_H_
// Author: Russell Smith
// 2014.03.14
// Write pixel register information to the ROD

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Fei4PixelCfg.h"
#include <string>

#ifdef __XMK__
#include "Fei4.h"
#endif

class SentPixelRegisterConfig: public CommandCRTP<SentPixelRegisterConfig > {
 public:
	bool success;
	
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class SendPixelRegisterConfig: public CommandCRTP<SendPixelRegisterConfig > {
public:
  InstantiateResult(SentPixelRegisterConfig)

        enum ConfigType { Physics, Calib };


	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}
	
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;
	
	SendPixelRegisterConfig();  
	virtual ~SendPixelRegisterConfig() {}


	//	void setPixRegConfig(SerializableFei4PixelCfg pixCfg){m_pixelCfg = pixCfg;}
	void setFeMask(uint32_t mask) {m_feMask = mask;};
 protected:
	
 private:
	//       SerializableFei4PixelCfg m_pixelCfg;
       uint32_t m_feMask;
       //ConfigType m_ConfigType;

};
#endif // _SENDPIXELREGISTERCONFIG_H_

