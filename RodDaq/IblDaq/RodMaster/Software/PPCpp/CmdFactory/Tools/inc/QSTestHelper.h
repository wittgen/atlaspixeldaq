#ifndef __QS_TEST_HELPER_H___
#define __QS_TEST_HELPER_H___
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2017-III-1
 * Description: code to cause trouble (to test QS actions)
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

class QSTestHelper: public CommandCRTP<QSTestHelper > { // declares type of command
public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything

  enum QSTestType { Timeout };

  QSTestHelper();

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  virtual ~QSTestHelper() {}

  QSTestType m_qsTestId;
  uint32_t m_rxMask;
};

#endif // __QS_TEST_HELPER_H___

