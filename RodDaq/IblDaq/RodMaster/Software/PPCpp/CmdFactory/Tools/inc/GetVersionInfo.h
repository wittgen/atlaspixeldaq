#ifndef _GETVERSIONINFO_H_
#define _GETVERSIONINFO_H_
/*
 * M. Kretz <moritz.kretz@googlemail.com>
 * Date: 2015-May-13
 * Get version info for ROD master/slave, FW/SW.
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "EasySerializable.h"
#include "IblRodVersionInfo.h"
#include <cstring> //memcpy

class VersionInfoResult: public CommandCRTP<VersionInfoResult >, public EasySerializable {
    private:
    public:   
	/** How many 32 bit words make up a git hash. */
	static const int hashWords = IblRodVersionInfo::c_hashWords;

	/** Data members containing the hashes. Using simple arrays for easy serialization. */
	uint32_t masterFw[hashWords];
	uint32_t masterSw[hashWords];
	uint32_t slaveAFw[hashWords];
	uint32_t slaveASw[hashWords];
	uint32_t slaveBFw[hashWords];
	uint32_t slaveBSw[hashWords];

	SERIAL_H( prep(masterFw, hashWords); prep(masterSw, hashWords); prep(slaveAFw, hashWords); prep(slaveASw, hashWords); prep(slaveBFw, hashWords); prep(slaveBSw, hashWords);)
};


class GetVersionInfo: public CommandCRTP<GetVersionInfo >, public EasySerializable {

    public:
	InstantiateResult(VersionInfoResult)

	virtual void execute();
	virtual const Command* getResult() const {
	    return &result;
	}

	virtual void serialize(uint8_t *out) const {}
	virtual void deserialize(const uint8_t *in) {}
	virtual uint32_t serializedSize() const {return 0;}
	GetVersionInfo();  
	virtual ~GetVersionInfo() {}  
	bool test;
	SERIAL_H(prep(test);)
	void printHash(uint32_t *hash, std::string desc);

    private:
	static void gitHashToResult(IblRodVersionInfo::GitHash_t const& hash, uint32_t *res);
};
#endif //_GETVERSIONINFO_H_


