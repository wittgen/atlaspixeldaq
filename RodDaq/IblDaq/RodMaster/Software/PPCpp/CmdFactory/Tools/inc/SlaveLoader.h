#ifndef __SLAVE_LOADER_H__
#define __SLAVE_LOADER_H__
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>,
 * Date: 2014-VIII-19
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"

class SlaveLoaderResult: public CommandCRTP<SlaveLoaderResult > { // declares type of command result
 public:

	// All the data we want back from the command
	uint32_t status;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class SlaveLoader: public CommandCRTP<SlaveLoader > { // declares type of command
public:
	typedef SlaveLoaderResult ResultType;
	ResultType result;

	void dump();
	SlaveLoader();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~SlaveLoader() {}

	// the only parameter of this command
 	bool	dumpOnPPC;
	uint32_t slaveSW_bin_size;
	uint8_t slaveSW_data[500*1024];

};
#endif //__SLAVE_LOADER_H__

