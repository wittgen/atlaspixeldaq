#ifndef _OPTO_SCAN_H_
#define _OPTO_SCAN_H_
/*
 * JuanAn Garcia
 * Date: Nov-2018
 *
 * Generic class to run an Opto scan
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
#include "IblBoc.h"

#ifdef __XMK__
#include "Fei3Mod.h"
#include "PixScanBase.h"
#include "IblBocLink.h"

#endif


class OptoScanResults: public CommandCRTP<OptoScanResults > {
 public:
        virtual ~OptoScanResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map <uint8_t, std::vector<std::vector <uint32_t > > > errors;  	

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class OptoScan: public CommandCRTP<OptoScan > { // declares type of command
 public:
  InstantiateResult(OptoScanResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  OptoScan();
  virtual ~OptoScan() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
#ifdef __XMK__
  static uint32_t initOpto(uint32_t rxMask, PixLib::PixScanBase::MccBandwidth MCCSpeed, uint32_t gain, uint32_t bitPattern, uint32_t nBitPattern, uint32_t &nBitsCheck);
  static uint32_t createCompareBitStream (uint32_t pattern, uint8_t nPatterns ,std::vector<uint8_t> &CompareBitStream, std::vector<uint8_t> &MaskBitStream);
  static void setThreshold(uint32_t rxMask,uint16_t threshold);
  static void setDelay(uint32_t rxMask, uint8_t coarseD, uint8_t fineD);
  static void setDelaySlaveFibre(uint32_t rxMask, uint8_t coarseD, uint8_t fineD);
  static std::map<uint8_t, uint32_t> getOptoErrors(uint32_t rxMask, uint32_t nTriggers, uint32_t nBitsCheck ,uint32_t bitPattern, uint32_t nBitPattern, Fei3Mod &sPort);
  static void cleanUp(uint32_t rxMask, Fei3Mod &sPort);
  static uint32_t getErrorsFromRxFIFO(uint8_t rxCh, uint32_t pattern);
#endif

  // command parameters of this command
  uint32_t m_gain;
  uint32_t m_rxMask;
  uint32_t m_nTriggers;
  uint32_t m_speed;
  uint32_t m_bitPattern;
  uint32_t m_nBitPattern;
  float m_maxThr;
  float m_maxDelay;
  uint32_t m_nThr;
  uint32_t m_nDelay;

};
#endif // _OPTO_SCAN_H_

