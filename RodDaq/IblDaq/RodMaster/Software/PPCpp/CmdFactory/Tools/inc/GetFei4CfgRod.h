#ifndef _GetFei4CfgRod_H_
#define _GetFei4CfgRod_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"
#include "Fei4GlobalCfg.h"
#include "Fei4PixelCfg.h"

#ifdef __XMK
#include "Fei4Mod.h"
#endif

class GetFei4CfgRodResult: public CommandCRTP<GetFei4CfgRodResult > {
 public:
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  //Results returned by this command
  std::map <uint32_t, std::map<uint32_t, Fei4GlobalCfg> > GlobalCfg;
  std::map <uint32_t, std::map<uint32_t, Fei4PixelCfg > > PixelCfg;
};

class GetFei4CfgRod: public CommandCRTP<GetFei4CfgRod > {
  public:
  
    typedef GetFei4CfgRodResult ResultType;
    ResultType result;
  
    virtual const Command* getResult() const {
      return &result;
    }

    virtual void execute();
    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    void printParams();

    GetFei4CfgRod();
    virtual ~GetFei4CfgRod() {}

    enum OpMode { GlobalRegReadback, PixelRegReadback, FullFE };
    OpMode m_opMode;
    static std::string opMode[];
    uint32_t m_rxMask; // To be refined with better assignment
};
#endif
