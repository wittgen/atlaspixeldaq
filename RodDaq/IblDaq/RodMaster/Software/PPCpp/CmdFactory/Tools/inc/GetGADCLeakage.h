/*
 * Author: N Savic <natascha.savic@cern.ch>
 * Date: 01 July 2015
 * Description: ...This command outputs the ILeak from the GADC
 */
#ifndef __Get_GADC_Leakage__
#define __Get_GADC_Leakage__

#include "CppCompatibility.h"
#include "Command.h"
#include "EasySerializable.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif


class GADCLeakage: public CommandCRTP<GADCLeakage >, public EasySerializable { // declares type of command result
 public:

  // All the data we want back from the command

  uint32_t resultarrayMean[32];
  uint32_t resultarrayStd[32];
  
  //	SERIAL_H();
  
  SERIAL_H(prep(resultarrayMean,32);prep(resultarrayStd,32););
    
};

class GetGADCLeakage: public CommandCRTP<GetGADCLeakage >, public EasySerializable { 
 public:
  
  InstantiateResult(GADCLeakage);
  
  virtual void execute();
  GetGADCLeakage(); 
  virtual ~GetGADCLeakage() {}

  uint32_t Channels;
  uint32_t IterInput;
  uint32_t UserMask;
  uint32_t nDC;
  uint32_t cpMode;
 
  SERIAL_H(prep(Channels);prep(IterInput);prep(UserMask);prep(nDC);prep(cpMode););
};


#endif

