#ifndef _ENABLEHPI_H_
#define _ENABLEHPI_H_

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "RodCommand.h"
#include "GeneralResults.h"

class GetEnableHPIResult: public CommandCRTP<GetEnableHPIResult > {
 public:
        virtual ~GetEnableHPIResult() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        // Results returned by this command
	uint32_t PPCctrlRegValue;
	
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class EnableHPI: public CommandCRTP<EnableHPI > { // declares type of command
 public:
  InstantiateResult(GetEnableHPIResult);

  virtual const Command* getResult() const {
      return &result;
  }
  
  EnableHPI() { setResultPtr();}
  virtual ~EnableHPI() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  bool enableHPIbit;

};
#endif // _ENABLEHPI_H_

