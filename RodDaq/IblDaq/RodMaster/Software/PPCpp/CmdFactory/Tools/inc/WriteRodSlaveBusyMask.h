/*
 * Authors: 
 * M. Backhaus <backhaus@cern.ch>
 * Date: 5 May 2014
 *
 * This command prints the current values of the registers on the ROD
 * to both the PPC output stream as well as to the host via serialized results.
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/unitTests/TestRodBusy.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "EasySerializable.h"
#include "Command.h"

class WriteRodSlaveBusyResult: public CommandCRTP<WriteRodSlaveBusyResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t slaveBusyMaskValue;
	uint32_t slaveBusyForceValue;
	uint32_t slaveBusyCurrentStatusValue;


	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class WriteRodSlaveBusyMask: public CommandCRTP<WriteRodSlaveBusyMask > { // declares type of command
public:
	typedef WriteRodSlaveBusyResult ResultType;
	ResultType result;
 
	void dump();
	WriteRodSlaveBusyMask();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~WriteRodSlaveBusyMask() {}

	// the only parameter of this command
 	bool	dumpOnPPC;
	uint32_t maskValue;
	uint32_t forceValue;
	bool targetIsSlaveB;
};

