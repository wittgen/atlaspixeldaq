#ifndef _DUMPBOCLINKSTATUS_H_
#define _DUMPBOCLINKSTATUS_H_
/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-X-29
 *
 * This command prints the current values of the BMF registers on the BOC
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"

class BocLinkStatus: public CommandCRTP<BocLinkStatus > {
 public:
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class DumpBocLinkStatus: public CommandCRTP<DumpBocLinkStatus > {
public:
	typedef BocLinkStatus ResultType;
	ResultType result;

	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	DumpBocLinkStatus() { setResultPtr(); }
	virtual ~DumpBocLinkStatus() {}

protected:
private:

};
#endif //_DUMPBOCLINKSTATUS_H_

