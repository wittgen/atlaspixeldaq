#ifndef __TEMPLATE_ROD_COMMAND_H__
#define __TEMPLATE_ROD_COMMAND_H__

#include "Serializer.h"
#include "Command.h"
#include "stdint.h"
#include "Fei4Cfg.h"
#include "IblRodSlave.h"

#include <iostream>

struct TemplateRodCommandResult: public CommandCRTP<TemplateRodCommandResult > {
	uint32_t value1;
	uint16_t value2;

	/// Inhertited functions
	void serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		Serializer<uint32_t>::serialize(out, offset, value1);
		Serializer<uint16_t>::serialize(out, offset, value2);
	}
	void deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		value1 = Serializer<uint32_t>::deserialize(in,offset);
		value2 = Serializer<uint16_t>::deserialize(in,offset);
	}
	uint32_t serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(uint32_t)+sizeof(uint16_t);
	}

	TemplateRodCommandResult() {}
	virtual ~TemplateRodCommandResult() {}
};

template <typename T>
class TemplateRodCommand: public CommandCRTP<TemplateRodCommand<T> > {
//class TemplateRodCommand: public CommandCRTP<TemplateRodCommand > {
public:
	InstantiateResult(TemplateRodCommandResult)

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;


	TemplateRodCommand() { setResultPtr(); }
	virtual ~TemplateRodCommand() {}

	protected:

	private:
};

template <typename T>
void TemplateRodCommand<T>::execute()
{
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        std::cout << "I'm here\n" ;
        std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << this->m_ExecId << std::endl;
        // Modify the result member as appropriate
        std::cout << "Result address = " << &result << std::endl;
        std::cout << "Pointer to result: " << this->m_ResultPtr << std::endl;

        result.value1 = 10;
        result.value2 = 200;
        std::cout << "value1 = " << result.value1 << std::endl;
        std::cout << "value2 = " << result.value2 << std::endl;

}

/// Inhertited functions for TemplateRodCommand
template <typename T>
void TemplateRodCommand<T>::serialize(uint8_t *out) const
{
        std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template <typename T>
void TemplateRodCommand<T>::deserialize(const uint8_t *in)
{
        std::cout << __PRETTY_FUNCTION__ << std::endl;
}

template <typename T>
uint32_t TemplateRodCommand<T>::serializedSize() const
{
        std::cout << __PRETTY_FUNCTION__ << std::endl;
        return 0;
}


#endif // __TEMPLATE_ROD_COMMAND_H__

