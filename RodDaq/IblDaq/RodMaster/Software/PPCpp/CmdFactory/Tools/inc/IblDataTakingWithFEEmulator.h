/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 5-V-2014
 * Description: this command prepares data-taking using the BOC FE EMU
 * 
 */

#ifndef __IBL_DATA_TAKING_WITH_FEEMULATOR__
#define __IBL_DATA_TAKING_WITH_FEEMULATOR__

#include "CppCompatibility.h"
#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class IblDataTakingWithFEEmulator: public CommandCRTP<IblDataTakingWithFEEmulator >, public EasySerializable { 
 public:
  InstantiateResult(EmptyTemplatedResult<IblDataTakingWithFEEmulator>)

  virtual void execute();
  IblDataTakingWithFEEmulator(){ setResultPtr(); } 
  virtual ~IblDataTakingWithFEEmulator() {}

  SERIAL_H()
};

#endif // __IBL_DATA_TAKING_WITH_FEEMULATOR__

