#ifndef _SINGLE_READ_H_
#define _SINGLE_READ_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 3-Sept-2014
 *
 * This is a (hopefully) temporary implementation of single register read
 * by address, to be used for immediate implementation of data-taking configuration
 * but should be replaced by smarter classes
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class RegValue: public CommandCRTP<RegValue > {
 public:
  uint32_t value;

  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

class SingleRead: public CommandCRTP<SingleRead > {
public:
  InstantiateResult(RegValue) 

	SingleRead();
	
	virtual void execute();
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~SingleRead() {}

	uint32_t address;
};
#endif

