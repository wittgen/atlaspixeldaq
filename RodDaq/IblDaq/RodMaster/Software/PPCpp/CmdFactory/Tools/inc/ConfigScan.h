#ifndef _CONFIGSCAN_H_
#define _CONFIGSCAN_H_
/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 4-April-2014
 *
 * Updated 13 July 2014: B. Axen <bradley.axen@cern.ch>
 *   Added option to add a debugHist to ScanBoss
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"
#include "Fei4Data.h"

class ConfigScan: public CommandCRTP<ConfigScan > {
 public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything

  ConfigScan();

  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  virtual ~ConfigScan() {};

  bool verboseScan;
  bool debugReadBackBoc;
  bool debugReadBackInmem;
  bool debugReadSR;
  uint8_t debugRxCh;
  uint8_t debugDCLoops;
  uint32_t debugChannels;

  Fei4Data::DataType histData;
  Fei4Data::FIFOType histFIFO;
  uint8_t histRxCh;
  bool clearDebugHists;
};
#endif

