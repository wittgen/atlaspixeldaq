/*
 * Author: L Jeanty <laura.jeanty@cern.ch>
 * Date: 01 Sept 2014
 * Description: This command tests the mapping of the FE by comparing the serial number on the chip with the 
 * serial number stored in its config file stored on the ppc (which you should update from pixlib via the Console before testing)
 */

#ifndef __TEST_MAPPING__
#define __TEST_MAPPING__

#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

enum ConfigType { Physics, Calib };

class TestMapping: public CommandCRTP<TestMapping >, public EasySerializable { 
 public:
  InstantiateResult(EmptyResult)
  
    virtual void execute();
  TestMapping(); 
  virtual ~TestMapping() {}

  uint32_t channels;
  ConfigType configType;

  SERIAL_H(prep(channels); prep(configType);)
    };

#endif // 

