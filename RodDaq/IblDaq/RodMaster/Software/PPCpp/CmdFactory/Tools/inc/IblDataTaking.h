/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 5-V-2014
 * Description: this command prepares data-taking using the BOC FE EMU
 * 
 */

#ifndef __IBL_DATA_TAKING__
#define __IBL_DATA_TAKING__

#include "CppCompatibility.h"
#include "Command.h"
#include "GeneralResults.h"
#include "EasySerializable.h"

class IblDataTaking: public CommandCRTP<IblDataTaking >, public EasySerializable { 
 public:
  InstantiateResult(EmptyTemplatedResult<IblDataTaking>)

  virtual void execute();
  IblDataTaking(){ setResultPtr(); } 
  virtual ~IblDataTaking() {}

  SERIAL_H()
};

#endif // __IBL_DATA_TAKING__

