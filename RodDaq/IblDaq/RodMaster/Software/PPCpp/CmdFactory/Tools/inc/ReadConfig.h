#ifndef _READCONFIG_H_
#define _READCONFIG_H_
/*
 * Author: 
 * Russell Smith
 * Date: 2014-June-06
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Fei4.h"

#ifdef __XMK__
#include "IblBocLink.h"
#endif

class ReadedConfig: public CommandCRTP<ReadedConfig > {
 public:
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	Fei4Cfg moduleConfig;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class ReadConfig: public CommandCRTP<ReadConfig > {
public:
  InstantiateResult(ReadedConfig)

	enum ConfigType { Physics, Calib };

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	ReadConfig();  
     	virtual ~ReadConfig(){}

	void readPhysicsConfig(){ m_ConfigType = Physics;};
	void readCalibConfig(){ m_ConfigType = Calib;};

	void setFeToRead(int feN) {i_fe = feN;};
	void setReadFrontEnd(bool readFrontEnd) {m_readFrontEnd = readFrontEnd;}

protected:

private:
	//	int m_nModules;
	uint32_t i_fe;
	bool m_readFrontEnd; //read from front end or rod
	ConfigType m_ConfigType;
};
#endif //_READCONFIG_H_

//  LocalWords:  ifndef

