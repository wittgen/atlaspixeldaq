#ifndef _TESTRXHISTO_H_
#define _TESTRXHISTO_H_
/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 13-November-2015
 *
 * PPC Dummy Scan Driver
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"
#include "IblRodSlave.h"

class TestRxHistoResult: public CommandCRTP<TestRxHistoResult > {
 public:
        virtual ~TestRxHistoResult() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class TestRxHisto: public CommandCRTP<TestRxHisto > { // declares type of command
 public:
  InstantiateResult(EmptyResult);
  // InstantiateResult(TestRxHistoResult);

  // virtual const Command* getResult() const {
  //     return &result;
  // }
  
  TestRxHisto() {setResultPtr();}
  virtual ~TestRxHisto() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  enum TestHistoStatus injectionType;
  IblSlvHistCfg histConfig_out;

  // command parameters of this command
  uint32_t whichSlave;
  uint32_t histoUnit;
  uint32_t modMask;
  uint32_t nTrig;
  uint32_t mode;
  uint32_t filter;
  uint32_t nMasks;
  IblSlvHistCfg histConfig_in;
  
};
#endif // _TESTRXHISTO_H_

