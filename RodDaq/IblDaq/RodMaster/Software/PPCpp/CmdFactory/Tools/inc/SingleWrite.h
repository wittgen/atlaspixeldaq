#ifndef _SINGLE_WRITE_H_
#define _SINGLE_WRITE_H_
/*
 * * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 3-Sept-2014
 *
 * This is a (hopefully) temporary implementation of single register write
 * by address and value, to be used for immediate implementation of data-taking configuration
 * but should be replaced by smarter classes
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

class SingleWrite: public CommandCRTP<SingleWrite > {
public:
  InstantiateResult(EmptyResult) // dummy result doesn't return anything

	SingleWrite();
	
	virtual void execute();
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~SingleWrite() {}

	uint32_t address;
	uint32_t value;
};
#endif

