#ifndef _ENABLECHANNELS_H_
#define _ENABLECHANNELS_H_
/*
 * Author:  L. Jeanty <laura.jeanty@cern.ch>
 * Date: 15-Dec-2013
 * Description: Enables RX and TX channels based on a FE mask
 * 
 */

// Inclusion necessary for command definitions
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif

class Enabled: public CommandCRTP<Enabled > {
public:
  virtual ~Enabled() {}
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;

  uint32_t enabledChannels;
};

class EnableChannels: public CommandCRTP<EnableChannels > {
public:
        InstantiateResult(Enabled)

	  uint32_t Channels;

	void serialize(uint8_t *out) const;
        void deserialize(const uint8_t *in);
        uint32_t serializedSize() const;
        EnableChannels();
	void execute(); // Todo: make int ?
        virtual ~EnableChannels() {}

protected:
private:
};
#endif //_ENABLECHANNELS_H_

