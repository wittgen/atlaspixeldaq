/*
 * Fei3Test
 * By: Laser Kaplan
 * Testing code for Fei3
 */

#ifndef _FEI3TEST_H_
#define _FEI3TEST_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class Fei3Test: public CommandCRTP<Fei3Test > {
  public:
    InstantiateResult(EmptyResult)

    virtual void execute();
    virtual const Command* getResult() const {
	    return &result;
    }

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    void printParams();

    Fei3Test();
    virtual ~Fei3Test() {}


    uint32_t m_verbosity;
    uint32_t m_rxMask; // To be refined with better assignment
    uint16_t m_feMask;
    uint8_t m_latency;
    uint8_t m_trigDelay;
    uint16_t m_nTriggers; // Number of triggers sent to the MCC
    uint8_t m_pulseDuration;
    uint8_t m_nConsecutiveTrig;	// Number of consecutive triggers sent by the MCC to the FEs
    bool m_useHighCap;
    uint16_t m_VCal;
    int16_t m_txShift;
    bool m_sendViaSerialPort;
    bool m_initModuleConfig;

    enum OpMode { DigitalInjection, AnalogInjection, GlobalRegReadback, PixelRegReadback, MccInjection, MccRegReadback, DumpModConfigs, ResetModules, ConfigureGlobal, MccRegTest, ConfigureAmpsOn };
    OpMode m_opMode;
    static std::string opMode[];


  protected:

  private:
};

#endif

