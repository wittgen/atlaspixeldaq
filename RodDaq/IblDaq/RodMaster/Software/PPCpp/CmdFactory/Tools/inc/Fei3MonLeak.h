
#ifndef _FEI3_MON_LEAK_H_
#define _FEI3_MON_LEAK_H_

//#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK__
#include "Fei3Mod.h"
#include "Fei3ModProxy.h"
#include "Fei3GlobalCfg.h"
#endif

#define MAX_STEPS_MON_LEAK 10

class Fei3MonLeakResults: public CommandCRTP<Fei3MonLeakResults > {
 public:
        virtual ~Fei3MonLeakResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	std::map <uint8_t, std::vector<std::vector <uint32_t > > > MON_LEAK;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class Fei3MonLeak: public CommandCRTP<Fei3MonLeak > { // declares type of command
 public:
  InstantiateResult(Fei3MonLeakResults);

  virtual const Command* getResult() const {
      return &result;
  }

  Fei3MonLeak();
  virtual ~Fei3MonLeak() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  static uint32_t initMonLeak(uint32_t rxMask);
  static uint32_t loadCompareBits(uint32_t rxMask);
  static void getMonLeakADC(uint32_t rxMask, uint16_t pixelIndex, int nSteps, int steps[MAX_STEPS_MON_LEAK], uint32_t nBitsCheck, uint32_t &skipRxMask, std::map < uint8_t, std::vector < uint32_t > > &MonADC, bool useBOCComp = true);
  static void cleanUp(uint32_t rxMask, bool useBOCComp = true);

  // command parameters of this command
  #ifdef __XMK__
  static uint32_t createCompareBitStream(std::vector<uint8_t> &CompareBitStream, std::vector<uint8_t> &MaskBitStream, Fei3GlobalCfg compCfg);
  #endif

  uint32_t m_rxMask;
  uint16_t m_nPixPerFE;
  uint8_t m_nSteps;

};
#endif //  _FEI3_MON_LEAK_H_


