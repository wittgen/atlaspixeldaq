#ifndef _CHECKHISTCFG_H_
#define _CHECKHISTCFG_H_
/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 11-September-2015
 *
 * List slave histogrammer configuration
 * 
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"
#include "IblRodSlave.h"

class CheckHistResult: public CommandCRTP<CheckHistResult > {
 public:
        virtual ~CheckHistResult() {}
 
        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        uint32_t whichSlave;
        IblSlvHistCfg histConfig;

};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class CheckHistCfg: public CommandCRTP<CheckHistCfg > { // declares type of command
 public:
        InstantiateResult(CheckHistResult);

        virtual const Command* getResult() const {
            return &result;
        }

        CheckHistCfg() {setResultPtr();}
        virtual ~CheckHistCfg() {}
  
        virtual void execute();

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

        // command parameters of this command
        uint32_t whichSlave;
        IblSlvHistCfg histConfig;
  
};
#endif // _CHECKHISTCFG_H_

