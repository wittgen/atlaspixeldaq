/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 28-IV-2014
 * Description: this command configures the BMF FE emulator
 * 
 */

#ifndef __BMF_FE_EMU_CONFIG__
#define __BMF_FE_EMU_CONFIG__

// Inclusion necessary for command definitions
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"

#ifdef __XMK__
#include "Fei4.h"
#include "IblBocLink.h"
#endif


class BmfFeEmuConfig: public CommandCRTP<BmfFeEmuConfig > {
public:

	void serialize(uint8_t *out) const;
	void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
	BmfFeEmuConfig();
	void execute(); // Todo: make int ?
	virtual ~BmfFeEmuConfig() {}

	uint8_t hitsPerTrigger;
	uint32_t channels;

protected:

private:

};

#endif // __BMF_FE_EMU_CONFIG__

