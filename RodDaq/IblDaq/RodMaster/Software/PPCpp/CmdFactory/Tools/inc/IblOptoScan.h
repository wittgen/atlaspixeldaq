#ifndef _IBL_OPTO_SCAN_H_
#define _IBL_OPTO_SCAN_H_
/*
 * Da Xu
 * Data: March-2019
 * Generic class to run an OptoScan in IBL
 */

#include "CppCompatibility.h" 
#include "Command.h"
#include "Serializer.h"
#include "IblBocLink.h"
#ifdef __XMK__
#include "Fei4.h"
#endif

class IblOptoScanResults: public CommandCRTP<IblOptoScanResults > {
 public:
        virtual ~IblOptoScanResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;
	std::map< uint8_t, std::vector< std::vector< uint32_t > > > errmap;
};

class IblOptoScan: public CommandCRTP<IblOptoScan > {
 public:
  InstantiateResult(IblOptoScanResults);

  virtual const Command* getResult() const {
      return &result;
  }

  IblOptoScan();
  virtual ~IblOptoScan() {}

  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

#ifdef __XMK__
  static  void getOptoErrors(uint32_t rxMask, uint32_t nTriggers,uint32_t txMask, std::map< uint8_t, std::vector< std::vector< uint32_t > > > & optoErrors );
#endif

  int nTriggers;
  uint32_t Channels;
};
#endif 

