#ifndef _READBACKFEI4REGISTERS_H_
#define _READBACKFEI4REGISTERS_H_

#include "CppCompatibility.h"
#include "Serializer.h"
#include "Command.h"
#include "Fei4.h"



class ReadBackFei4RegistersResult: public CommandCRTP<ReadBackFei4RegistersResult > {
 public:
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  //Results returned by this command

  std::map <uint32_t, std::vector<uint32_t> > GRCfgMap;
  std::map <uint32_t, Fei4PixelCfg> PixCfgMap;

};

class ReadBackFei4Registers: public CommandCRTP<ReadBackFei4Registers > {

 public:
  typedef ReadBackFei4RegistersResult ResultType;
  ResultType result;
  
  virtual const Command* getResult() const {
    return &result;
  }
  ReadBackFei4Registers();
  virtual ~ReadBackFei4Registers(){}
  
  virtual void execute();
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  //command parameters of this command
  
  bool m_clear;
  bool m_global;
  uint32_t m_modMask;
  uint8_t  m_DC;
  uint8_t  m_CP;
  uint32_t delay;
  int ShiftRegLeft;

  //operational mode
  
  enum ReadBackMode { 
    RB_ReadShiftRegister, RB_ClearAndReadBackSR, RB_WriteCustomPattern,RB_ClearSR, RB_PutOnesInSR, RB_PixelRegisters}; 
  ReadBackMode rb_mode;

};


#endif //_READBACKFEI4REGISTERS_H_

