#ifndef _READRODVETOCOUNTERS_H_
#define _READRODVETOCOUNTERS_H_


#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Serializer.h"

#ifdef __XMK__
#include "RodMasterRegisters.h"
#endif

class ReadRodVetoCountersResults: public CommandCRTP<ReadRodVetoCountersResults > {
 public:
        virtual ~ReadRodVetoCountersResults() {}

        virtual void serialize(uint8_t *out) const;
        virtual void deserialize(const uint8_t *in);
        virtual uint32_t serializedSize() const;

	uint32_t ECR_Vetoed;
	uint32_t BCR_Vetoed;
	uint32_t L1A_Vetoed;
	uint32_t ECR_Dropped;
	uint32_t BCR_Dropped;
	uint32_t L1A_Dropped;
	
};


class ReadRodVetoCounters: public CommandCRTP<ReadRodVetoCounters > { // declares type of command
 public:
  InstantiateResult(ReadRodVetoCountersResults);

  virtual const Command* getResult() const {
      return &result;
  }
  
  ReadRodVetoCounters();
  virtual ~ReadRodVetoCounters() {}

  virtual void execute();

//  virtual void serialize(uint8_t *out) const; 
//  virtual void deserialize(const uint8_t *in);
//  virtual uint32_t serializedSize() const;

};
#endif // _READRODVETOCOUNTERS_H_

