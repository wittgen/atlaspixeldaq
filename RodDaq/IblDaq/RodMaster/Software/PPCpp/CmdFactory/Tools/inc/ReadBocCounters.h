#ifndef _READBOCCOUNTERS_H_
#define _READBOCCOUNTERS_H_
/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>,
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "EasySerializable.h"

#include "IblBoc.h"

class BocRegisters: public CommandCRTP<BocRegisters > { // declares type of command result
 public:
  // All the data we want back from the command
  uint32_t decodingError[32];
  uint32_t frameError[32];
  uint32_t frameCount[32];
  uint32_t meanOccupancy[32];
  uint32_t liveOccupancy[32];
  IblBoc::SLinkInfo sLinkInfo;
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class ReadBocCounters: public CommandCRTP<ReadBocCounters >, public EasySerializable { // declares type of command
public:
	typedef BocRegisters ResultType;
	ResultType result;

	static void dump();
	ReadBocCounters();
	
	virtual void execute();
	void resetBocCounters();
	virtual const Command* getResult() const {
		return &result;
	}

	virtual ~ReadBocCounters() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

	SERIAL_H(prep(dumpOnPPC);)
};
#endif //_READBOCCOUNTERS_H_

