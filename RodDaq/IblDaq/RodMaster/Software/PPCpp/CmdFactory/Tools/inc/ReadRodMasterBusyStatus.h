/*
 * Authors: 
 * M. Backhaus <backhaus@cern.ch>
 * Date: 5 May 2014
 *
 * This command prints the current values of the registers on the ROD
 * to both the PPC output stream as well as to the host via serialized results.
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/unitTests/TestRodBusy.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */
#ifndef __READ_ROD_MASTER_BUSY_STATUS__
#define __READ_ROD_MASTER_BUSY_STATUS__

#include <stdint.h>
#include <memory>

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "Command.h"

class ReadMasterBusyStatusResult: public CommandCRTP<ReadMasterBusyStatusResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t masterBusyMaskValue;
	uint32_t masterBusyCurrentStatusValue;

	uint32_t masterBusyOutHistValue;
	uint32_t masterBusyRodBusyNinP0HistValue;
	uint32_t masterBusyRodBusyNinP1HistValue;
	uint32_t masterBusyEfbHeaderPauseHistValue;
	uint32_t masterBusyFormatterMB0HistValue;
	uint32_t masterBusyFormatterMB1HistValue;
	uint32_t masterBusyRolTestPauseHistValue;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class ReadRodMasterBusyStatus: public CommandCRTP<ReadRodMasterBusyStatus > { // declares type of command
public:
	typedef ReadMasterBusyStatusResult ResultType;
	ResultType result;
 
	void dump();
	ReadRodMasterBusyStatus();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	uint32_t getBusyOutput();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~ReadRodMasterBusyStatus() {}

	// the only parameter of this command
 	bool	dumpOnPPC;

};
#endif
