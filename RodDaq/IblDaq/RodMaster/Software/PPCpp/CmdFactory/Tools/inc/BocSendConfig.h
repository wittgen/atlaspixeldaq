#ifndef _BOCSENDCONFIG_H_
#define _BOCSENDCONFIG_H_
/*
 * F. Meloni <federico.meloni@cern.ch>
 * P. Morettini <Paolo.Morettini@ge.infn.it>
 * Configure BOC
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif


enum BocSendConfigActionType { Dump, SetRxFibreMapping, SetRxThresholdDelay, SetTxCoarseDelay, SetIBLPhases};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class BocSendConfig: public CommandCRTP<BocSendConfig > { // declares type of command
 public:
  InstantiateResult(EmptyResult);

  virtual const Command* getResult() const {
      return &result;
  }
  
  BocSendConfig();
  virtual ~BocSendConfig() {}

  virtual void execute();

  virtual void setRxFibreMapping( );
  virtual void setRxThresholdDelay( );
  virtual void setTxCoarseDelay( );
  virtual void setIBLPhases( );

  virtual void dump( );

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;

  // command parameters of this command
  BocSendConfigActionType action;
  uint32_t nTx;
  uint32_t nRx;
  uint32_t txCh[32];
  uint32_t rxCh[32];
  uint32_t masterFibre[32];
  uint32_t slaveFibre[32];
  uint32_t txCoarseDelay[32];
  float    threshold[32];
  float    delay[32];
  uint32_t gain[32];
  uint8_t phases[32];

};
#endif // _BOCSENDCONFIG_H_

