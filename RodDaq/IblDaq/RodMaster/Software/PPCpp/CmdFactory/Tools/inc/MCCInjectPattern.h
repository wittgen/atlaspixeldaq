/*
 * MCCInjectPattern
 * By: Laser Kaplan
 * Inject a pattern a number of times into all FEs on an MCC
 */

#ifndef _MCCINJECTPATTERN_H_
#define _MCCINJECTPATTERN_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class MCCInjectPattern: public CommandCRTP<MCCInjectPattern > {
  public:
    InstantiateResult(EmptyResult)

    virtual void execute();

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    MCCInjectPattern();
    virtual ~MCCInjectPattern() {}

    void setVerbosity(uint32_t verbosity) {m_verbosity = verbosity;}
    void setPattern(uint32_t pattern) {m_pattern = pattern;}
    void setNPattern(uint32_t nPattern) {m_nPattern = nPattern;}
    void setRxMask(uint32_t rxMask) {m_rxMask = rxMask;}

  protected:

  private:
    uint32_t m_verbosity;
    uint32_t m_pattern;
    uint32_t m_nPattern;
    uint32_t m_rxMask;
};

#endif

