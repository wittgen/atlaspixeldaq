/*
 * Authors: 
 * M. Backhaus <backhaus@cern.ch>
 * Date: 10 June 2014
 *
 * This command prints the current values of the registers on the ROD
 * to both the PPC output stream as well as to the host via serialized results.
 * An example of how to run it is available in:
 * RodDaq/IblUtils/HostCommandPattern/unitTests/TestRodBusy.cxx
 * If dumpOnPPC = true, then the value of the registers is output to the kermit logger
 * as well as returned to the host via the serial stream
 * 
 */

#ifndef __READ_ROD_SLAVE_BUSY_STATUS_H__
#define __READ_ROD_SLAVE_BUSY_STATUS_H__
#include <stdint.h>
#include <memory>

#include "CppCompatibility.h" // Keep this header first!
#include "RodCommand.h"
#include "Command.h"

class ReadSlaveBusyStatusResult: public CommandCRTP<ReadSlaveBusyStatusResult > { // declares type of command result
 public:

  // All the data we want back from the command
  // If you add something here, need to add it to
  // RodRegistersReturn::serialize(), deserialize(), and serializedSize() too
  // otherwise it won't be transmitted back from the PPC to the host

	uint32_t slaveBusyMaskValue;
	uint32_t slaveBusyForceValue;
	uint32_t slaveBusyCurrentStatusValue;

	uint32_t slaveBusyOutHistValue;
	uint32_t slaveBusySlink0LffHistValue;
	uint32_t slaveBusySlink0DownHistValue;
	uint32_t slaveBusySlink1LffHistValue;
	uint32_t slaveBusySlink1DownHistValue;
	uint32_t slaveBusyEfb1Stts0HistValue;
	uint32_t slaveBusyEfb1Stts1HistValue;
	uint32_t slaveBusyEfb2Stts0HistValue;
	uint32_t slaveBusyEfb2Stts1HistValue;
	uint32_t slaveBusyRouter0HistValue;
	uint32_t slaveBusyRouter1HistValue;
	uint32_t slaveBusyHistfull0HistValue;
	uint32_t slaveBusyHistfull1HistValue;
	uint32_t slaveBusyHistoverrun0HistValue;
	uint32_t slaveBusyHistoverrun1HistValue;
	
	uint32_t slaveBusyFmt0ff0HistValue;
	uint32_t slaveBusyFmt0ff1HistValue;
	uint32_t slaveBusyFmt0ff2HistValue;
	uint32_t slaveBusyFmt0ff3HistValue;

	uint32_t slaveBusyFmt1ff0HistValue;
	uint32_t slaveBusyFmt1ff1HistValue;
	uint32_t slaveBusyFmt1ff2HistValue;
	uint32_t slaveBusyFmt1ff3HistValue;

	uint32_t slaveBusyFmt2ff0HistValue;
	uint32_t slaveBusyFmt2ff1HistValue;
	uint32_t slaveBusyFmt2ff2HistValue;
	uint32_t slaveBusyFmt2ff3HistValue;

	uint32_t slaveBusyFmt3ff0HistValue;
	uint32_t slaveBusyFmt3ff1HistValue;
	uint32_t slaveBusyFmt3ff2HistValue;
	uint32_t slaveBusyFmt3ff3HistValue;

	// necessary inherited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	uint32_t serializedSize() const;
};

class ReadRodSlaveBusyStatus: public CommandCRTP<ReadRodSlaveBusyStatus > { // declares type of command
public:
	typedef ReadSlaveBusyStatusResult ResultType;
	ResultType result;
 
	void dump();
	ReadRodSlaveBusyStatus();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

  uint32_t getBusyOutput();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	virtual ~ReadRodSlaveBusyStatus() {}

	// the only parameter of this command
 	bool	dumpOnPPC;
	bool targetIsSlaveB;
};
#endif
