/*
 * MCCTest
 * By: Laser Kaplan
 * Inject hits into FE and read back from MCC
 */

#ifndef _MCCTEST_H_
#define _MCCTEST_H_

#include "CppCompatibility.h"
#include "GeneralResults.h"
#include "Command.h"
#include "IblRodSlave.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

class MCCTestValue: public CommandCRTP<MCCTestValue > {
  public:
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    uint32_t serializedSize() const;

    uint8_t nMod;
    std::vector<uint8_t> data;
};

class MCCTest: public CommandCRTP<MCCTest > {
  public:
    InstantiateResult(MCCTestValue)

    virtual void execute();
    virtual const Command* getResult() const {
	    return &result;
    }

    /// Inhertited functions
    virtual void serialize(uint8_t *out) const;
    virtual void deserialize(const uint8_t *in);
    virtual uint32_t serializedSize() const;

    MCCTest();
    virtual ~MCCTest() {}

    void initializeMCC();
    void setModMask(uint32_t mask) {m_modMask = mask;}
    void setNTrig(uint32_t nTrig) {m_nTrig = nTrig;}

  protected:

  private:
    uint32_t m_modMask;
    uint32_t m_nTrig;
};

#endif

