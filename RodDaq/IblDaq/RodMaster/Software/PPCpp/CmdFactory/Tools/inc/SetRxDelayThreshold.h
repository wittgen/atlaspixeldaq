#ifndef _SETRXDELAYTHRESHOLD_H_
#define _SETRXDELAYTHRESHOLD_H_
/*
 * F. Meloni <federico.meloni@cern.ch>
 * Set Rx Threshold and Delay (and Gain)
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "GeneralResults.h"

#ifdef __XMK
#include "Fei3Mod.h"
#endif

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class SetRxDelayThreshold: public CommandCRTP<SetRxDelayThreshold > { // declares type of command
 public:
  InstantiateResult(EmptyResult);

  virtual const Command* getResult() const {
      return &result;
  }
  
  SetRxDelayThreshold() { setResultPtr();}
  virtual ~SetRxDelayThreshold() {}
  
  virtual void execute();

  virtual void serialize(uint8_t *out) const; 
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  // command parameters of this command
  int32_t txCh;
  int32_t rxCh;
  uint32_t threshold;
  uint32_t delay;
  uint32_t gain;

};
#endif // _SETRXDELAYTHRESHOLD_H_

