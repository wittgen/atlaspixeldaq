#ifndef _WRITEMODULECONFIG_H_
#define _WRITEMODULECONFIG_H_
/*
 * Author: 
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Nov-25
 *
 * This command is used to send the global configuration of n FEs from the host to the ROD.
 * The placing of the FEs into the set controlled by RodResourceManager is determined
 * by the rx channel set in the config and by the config type chosen (physics or calib).
 * If the rx channel is invalid, no config is stored on the ppc.
 * It is called WriteModuleConfig but actually sends multiple FE configs 
 */

#include "CppCompatibility.h" 
#include "Command.h"
#include "Fei4ExtCfg.h" 
#include "GeneralResults.h"

#include <vector>

class WriteModuleConfig: public CommandCRTP<WriteModuleConfig > {
public:
  InstantiateResult(EmptyResult)

	enum ConfigType { Physics, Calib };

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;

	WriteModuleConfig();  
	virtual ~WriteModuleConfig() {}

	std::vector<Fei4ExtCfg> moduleConfigs;

	void setPhysicsConfig(){ m_ConfigType = Physics;};
	void setCalibConfig(){ m_ConfigType = Calib;};
	
protected:

private:
	ConfigType m_ConfigType;
};
#endif //_WRITEMODULECONFIG_H_

