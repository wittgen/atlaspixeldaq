#ifndef _GETDEBUGHISTS_H_
#define _GETDEBUGHISTS_H_
/*
 * Author: B. Axen <bradley.axen@cern.ch>
 * Date: 2014-Jul-11
 *
 * Retrieves any debugging histograms stored in ScanBoss.
 */

#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "Fei4Data.h"
#include "Serializer.h"

#include <vector>

// Specialization for Fei4Hist
template < >
class Serializer<Fei4Hist> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const Fei4Hist& obj) {
    Serializer<Fei4Data::DataType>::serialize(data, offset, obj.data);
    Serializer<Fei4Data::FIFOType>::serialize(data, offset, obj.fifo);
    Serializer<uint8_t>::serialize(data, offset, obj.rxCh);
    for (unsigned i = 0; i<Fei4Hist::ncol; ++i) {
      for (unsigned j = 0; j<Fei4Hist::nrow; ++j) {
	Serializer<uint16_t>::serialize(data, offset, obj(i,j));
      }
    }
  }

  static Fei4Hist deserialize(const uint8_t* data, uint32_t& offset) {
    Fei4Hist obj;
    deserialize(data, offset, obj); 
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, Fei4Hist& obj) {
    obj.data = Serializer<Fei4Data::DataType>::deserialize(data, offset);
    obj.fifo = Serializer<Fei4Data::FIFOType>::deserialize(data, offset);
    obj.rxCh = Serializer<uint8_t>::deserialize(data, offset);
    for (unsigned i = 0; i<Fei4Hist::ncol; ++i) {
      for (unsigned j = 0; j<Fei4Hist::nrow; ++j) {
	obj(i,j) = Serializer<uint16_t>::deserialize(data, offset);
      }
    }
  }

  static std::size_t size(const Fei4Hist& obj) {
    return 2*sizeof(uint32_t) + sizeof(uint8_t) + Fei4Hist::ncol * Fei4Hist::nrow * sizeof(uint16_t);
  }
};

class DebugHists: public CommandCRTP<DebugHists > { // declares type of command result
 public:
  std::vector<Fei4Hist> hists;

  // necessary inherited functions
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
};

// If inheriting from RodCommand, the command will also have it's ID, until the TypeId class is updated
class GetDebugHists: public CommandCRTP<GetDebugHists > { // declares type of command
public:
	typedef DebugHists ResultType;
	ResultType result;

	GetDebugHists();
	
	virtual void execute();
	virtual const Command* getResult() const {
		return &result;
	}

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const {}
	virtual void deserialize(const uint8_t *in) {}
	virtual uint32_t serializedSize() const {return 0;}

	virtual ~GetDebugHists() {}

};
#endif //_GETDEBUGHISTS_H_

