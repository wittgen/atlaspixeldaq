#ifndef _GET_H_
#define _GET_H_

/*
  Steve Alkire <alkire@cern.ch> 2014.03.18
  Get or set a static object. Put its pointer in Stage.h
*/
#include "CppCompatibility.h" // Keep this header first!
#include "Command.h"
#include "EasySerializable.h"
#include "Stage.h"
#include "ScanBoss.h"
#include "ItersAndAlgorithms.h"

template<typename T>
class GetResult:public Command, public EasySerializable{
 public:
  //copy pasta from CommandCRTP is a "temporary" measure
  uint16_t getTypeId() const { return TypeId<GetResult<T> >::getTypeId(); }
  virtual shared_ptr<Command> getCopy() const {
    shared_ptr<Command> cmd = shared_ptr<Command>( new GetResult<T>(static_cast<GetResult<T> const&>(*this)) );
    cmd->setResultPtr();
    return cmd;
  }
  T object;
  SERIAL_H(prep(object);)
};

template<typename T, T* (*F)()>
  class Get: public Command, public EasySerializable{
 public:
    uint16_t getTypeId() const { return TypeId<Get<T,F> >::getTypeId(); }
  virtual shared_ptr<Command> getCopy() const {
    shared_ptr<Command> cmd = shared_ptr<Command>( new Get<T,F>(static_cast<Get<T,F> const&>(*this)) );
    cmd->setResultPtr();
    return cmd;
  }
  GetResult<T> result; 
  typedef GetResult<T> ResultType;					\
  virtual void setResultPtr() { m_ResultPtr = &result; }
  virtual void execute(){result.object = *(F());}
  virtual const Command* getResult() const {return &result;}
  Get(){setResultPtr();}
  virtual ~Get() {}
  SERIAL_H()
};

class SetResult:public Command, public EasySerializable{
 public:
  //copy pasta from CommandCRTP is a "temporary" measure
  uint16_t getTypeId() const { return TypeId<SetResult>::getTypeId(); }
  virtual shared_ptr<Command> getCopy() const {
    shared_ptr<Command> cmd = shared_ptr<Command>( new SetResult(static_cast<SetResult const&>(*this)) );
    cmd->setResultPtr();
    return cmd;
  }
  SERIAL_H()
};

template<typename T, T* (*F)()>
class Set: public Command, public EasySerializable{
 public:
    uint16_t getTypeId() const { return TypeId<Set<T,F> >::getTypeId(); }
  virtual shared_ptr<Command> getCopy() const {
    shared_ptr<Command> cmd = shared_ptr<Command>( new Set<T,F>(static_cast<Set<T,F> const&>(*this)) );
    cmd->setResultPtr();
    return cmd;
  }
  T object;
  SetResult result; 
  typedef SetResult ResultType;					\
  virtual void setResultPtr() { m_ResultPtr = &result; }
  virtual void execute(){T* o = F(); *o = object;}
  virtual const Command* getResult() const {return &result;}
  Set(){setResultPtr();}
  virtual ~Set() {}
  SERIAL_H(prep(object);)
};

#endif // _GET_H_
