
#include <iomanip>
#include "ReadRodVetoCounters.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#endif

ReadRodVetoCounters::ReadRodVetoCounters(){
setResultPtr();
}

void ReadRodVetoCounters::execute() {

#ifdef __XMK__
result.ECR_Vetoed = IblRod::Master::EcrVetoedAfterEcrReg::read();
result.BCR_Vetoed = IblRod::Master::BcrVetoedAfterEcrReg::read();
result.L1A_Vetoed = IblRod::Master::L1AVetoedAfterEcrReg::read();
result.ECR_Dropped = IblRod::Master::EcrDroppedReg::read();
result.BCR_Dropped = IblRod::Master::BcrDroppedReg::read();
result.L1A_Dropped = IblRod::Master::L1ADroppedReg::read();
#endif

}

void ReadRodVetoCountersResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset, ECR_Vetoed);
	Serializer<uint32_t>::serialize(out, offset, BCR_Vetoed);
	Serializer<uint32_t>::serialize(out, offset, L1A_Vetoed);
	Serializer<uint32_t>::serialize(out, offset, ECR_Dropped);
	Serializer<uint32_t>::serialize(out, offset, BCR_Dropped);
	Serializer<uint32_t>::serialize(out, offset, L1A_Dropped);

}

void ReadRodVetoCountersResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<uint32_t>::deserialize(in, offset, ECR_Vetoed);
	Serializer<uint32_t>::deserialize(in, offset, BCR_Vetoed);
	Serializer<uint32_t>::deserialize(in, offset, L1A_Vetoed);
	Serializer<uint32_t>::deserialize(in, offset, ECR_Dropped);
	Serializer<uint32_t>::deserialize(in, offset, BCR_Dropped);
	Serializer<uint32_t>::deserialize(in, offset, L1A_Dropped);	
}

uint32_t ReadRodVetoCountersResults::serializedSize() const
{
		return 6 * sizeof(uint32_t);
}

