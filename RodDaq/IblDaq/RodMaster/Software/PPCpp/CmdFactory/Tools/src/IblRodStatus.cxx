#include "IblRodStatus.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void IblRodStatus::dump() {

  std::cout<<"I'm about to output the current"<<
    "value of registers on the ROD"<<std::endl;

  std::cout<<"masterBusy Mask: "<<HEX(result.masterBusyMaskValue)<<std::endl;
  std::cout<<"masterBusy Status: "<<HEX(result.masterBusyCurrentStatusValue)<<std::endl;

  std::cout<<"slave A Busy Mask: "<<HEX(result.slave1BusyMaskValue)<<std::endl;
  std::cout<<"slave A Busy Force: "<<HEX(result.slave1BusyForceValue)<<std::endl;
  std::cout<<"slave A Busy Status: "<<HEX(result.slave1BusyCurrentStatusValue)<<std::endl;
  std::cout<<"slave B Busy Mask: "<<HEX(result.slave2BusyMaskValue)<<std::endl;
  std::cout<<"slave B Busy Force: "<<HEX(result.slave2BusyForceValue)<<std::endl;
  std::cout<<"slave B Busy Status: "<<HEX(result.slave2BusyCurrentStatusValue)<<std::endl;
}

IblRodStatus::IblRodStatus(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void IblRodStatus::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  unsigned value;

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);


  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

  // read the busy mask register
  value = IblRod::MasterBusyMask::read();
  result.masterBusyMaskValue=value;

  // read the busy current status ro register
  value = IblRod::MasterBusyCurrentStatus::read();
  result.masterBusyCurrentStatusValue=value;

  // get the rod Master Busy value
  uint32_t rodMasterBusyForceValue = (result.masterBusyMaskValue >> 8) & 0xFF;
  uint32_t rodMasterBusy = (result.masterBusyCurrentStatusValue & ~result.masterBusyMaskValue) | rodMasterBusyForceValue;
  rodMasterBusy = (~rodMasterBusy) & 0x1; // ROD master busy is negative logic
  result.rodMasterBusy = rodMasterBusy;

  // read the busy values for slave1
  value = IblRod::SlaveA::BusyMaskReg::read();
  result.slave1BusyMaskValue=value;
  value = IblRod::SlaveA::BusyForceReg::read();
  result.slave1BusyForceValue=value;
  value = IblRod::SlaveA::BusyStatusReg::read();
  result.slave1BusyCurrentStatusValue=value;

  // read the busy values for slave1
  value = IblRod::SlaveB::BusyMaskReg::read();
  result.slave2BusyMaskValue=value;
  value = IblRod::SlaveB::BusyForceReg::read();
  result.slave2BusyForceValue=value;
  value = IblRod::SlaveB::BusyStatusReg::read();
  result.slave2BusyCurrentStatusValue=value;

  // output busy mask for slave A and B
  uint32_t outputBusyMask_slvA = result.slave1BusyCurrentStatusValue & (~result.slave1BusyMaskValue) | result.slave1BusyForceValue;
  uint32_t outputBusyMask_slvB = result.slave2BusyCurrentStatusValue & (~result.slave2BusyMaskValue) | result.slave2BusyForceValue;
  result.outputBusyMask_slvA = outputBusyMask_slvA;
  result.outputBusyMask_slvB = outputBusyMask_slvB;


  // storing the formatter busy status for both slaves
  uint32_t fmtBusyStatus = (result.slave1BusyCurrentStatusValue & ~result.slave1BusyMaskValue) >> 13;
  fmtBusyStatus |= result.slave1BusyForceValue >> 13;
  fmtBusyStatus |= ((result.slave2BusyCurrentStatusValue & ~result.slave2BusyMaskValue) >> 13) << 16;
  fmtBusyStatus |= (result.slave2BusyForceValue >> 13) << 16;
  result.iblRodFmtBusyStatus = fmtBusyStatus;

  // check the EFB status
  uint8_t efbMask = 0x0; // [7-4]: slave B ; [3-0]: slave A
  efbMask |= (result.slave1BusyCurrentStatusValue >> 5) & 0xF;
  efbMask |= (result.slave1BusyForceValue >> 5) & 0xF;
  efbMask |= ((result.slave2BusyCurrentStatusValue >> 5) & 0xF) << 4;
  efbMask |= ((result.slave2BusyForceValue >> 5) & 0xF) << 4;
  result.efbBusyMask = efbMask;

  // fetching the list of enabled channels
  result.slvA_FmtLinkEnable = IblRod::SlaveA::FmtLinkEnable::read();
  result.slvB_FmtLinkEnable = IblRod::SlaveB::FmtLinkEnable::read();
  uint32_t fmtLinkEnable = result.slvA_FmtLinkEnable;
  fmtLinkEnable |= (result.slvB_FmtLinkEnable << 16);
  result.iblRodFmtLinkEnable = fmtLinkEnable;

  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void IblRodStatus::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
}

// Deseralize the data members of the command
// In this case, only one boolean
void IblRodStatus::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t IblRodStatus::serializedSize() const
{
	return sizeof(bool);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void ReadIblRodStatusResult::serialize(uint8_t *out) const {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, masterBusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyCurrentStatusValue);
	  Serializer<uint32_t>::serialize(out, offset, rodMasterBusy);
	  Serializer<uint32_t>::serialize(out, offset, slave1BusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, slave1BusyForceValue);
	  Serializer<uint32_t>::serialize(out, offset, slave1BusyCurrentStatusValue);
  	  Serializer<uint32_t>::serialize(out, offset, outputBusyMask_slvA);
	  Serializer<uint32_t>::serialize(out, offset, slave2BusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, slave2BusyForceValue);
	  Serializer<uint32_t>::serialize(out, offset, slave2BusyCurrentStatusValue);
  	  Serializer<uint32_t>::serialize(out, offset, outputBusyMask_slvB);
	  Serializer<uint32_t>::serialize(out, offset, iblRodFmtBusyStatus);
	  Serializer<uint16_t>::serialize(out, offset, efbBusyMask);
	  Serializer<uint32_t>::serialize(out, offset, slvA_FmtLinkEnable);
	  Serializer<uint32_t>::serialize(out, offset, slvB_FmtLinkEnable);
      	  Serializer<uint32_t>::serialize(out, offset, iblRodFmtLinkEnable);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void ReadIblRodStatusResult::deserialize(const uint8_t *in) {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  masterBusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
	  rodMasterBusy = Serializer<uint32_t>::deserialize(in, offset);
	  slave1BusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  slave1BusyForceValue = Serializer<uint32_t>::deserialize(in, offset);
	  slave1BusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
  	  outputBusyMask_slvA = Serializer<uint32_t>::deserialize(in, offset);
	  slave2BusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  slave2BusyForceValue = Serializer<uint32_t>::deserialize(in, offset);
	  slave2BusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
  	  outputBusyMask_slvB = Serializer<uint32_t>::deserialize(in, offset);
	  iblRodFmtBusyStatus = Serializer<uint32_t>::deserialize(in, offset);
	  efbBusyMask = Serializer<uint16_t>::deserialize(in, offset);
	  slvA_FmtLinkEnable = Serializer<uint32_t>::deserialize(in, offset);
	  slvB_FmtLinkEnable = Serializer<uint32_t>::deserialize(in, offset);
	  iblRodFmtLinkEnable = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t ReadIblRodStatusResult::serializedSize() const {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (15*sizeof(uint32_t)+1*sizeof(uint16_t));
}
