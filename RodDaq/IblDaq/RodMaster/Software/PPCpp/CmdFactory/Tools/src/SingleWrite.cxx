/*
 * L. Jeanty <laura.jeanty@cern.ch>
 */

#include "SingleWrite.h"

#ifdef __XMK__
#include "RodMasterRegisters.h"
#endif

SingleWrite::SingleWrite(){ 
  setResultPtr(); 
  address = 0;
  value = 0;
}

void SingleWrite::execute() {
#ifdef __XMK__
  std::cout<<"Writing to address: 0x"<<std::hex<<(int)address<<" with value: 0x"<<(int)value<<std::endl;
  IblRod::EpcBase::write(address,value);
  std::cout<<"Read back value: 0x"<<std::hex<<IblRod::EpcBase::read(address)<<std::dec<<std::endl;
#endif
}

void SingleWrite::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,address);
	Serializer<uint32_t>::serialize(out,offset,value);
}
void SingleWrite::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	address = Serializer<uint32_t>::deserialize(in,offset);
	value = Serializer<uint32_t>::deserialize(in,offset);
}
uint32_t SingleWrite::serializedSize() const
{
  return (2*sizeof(uint32_t));
}
