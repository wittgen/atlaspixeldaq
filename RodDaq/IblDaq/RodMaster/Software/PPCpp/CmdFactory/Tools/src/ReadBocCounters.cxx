/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "ReadBocCounters.h"
#include "RodMasterRegisters.h"

#include "SLinkInfoSerializer.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec



void ReadBocCounters::dump() {

}

ReadBocCounters::ReadBocCounters(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host

  for (int ch=0; ch<32; ch++) {
    result.decodingError[ch] = 0;
    result.frameError[ch]=0;
    result.frameCount[ch]=0;
    result.meanOccupancy[ch]=0;
    result.liveOccupancy[ch]=0;
  }

}

void ReadBocCounters::resetBocCounters() {
  IblBoc::ResetAllCounters(); //use this function only if you want to reset all Boc error counter
  //IblBoc::ResetCounter(IblBoc::DecoError);
}

// inside execute() the actual meat of the command happens on the ppc
void ReadBocCounters::execute() {

  // each RX channel has an error count register at the offset 0x1D
  // if one want to read a counter, one has to write the counter to that register
  // possible are:
  // 0x0 lock loss counter
  // 0x1 sync loss counter
  // 0x2 decoding error counter
  // 0x3 disparity error counter
  // 0x4 frame error counter
  // this gives the counter value (32bit) from MSB to LSB

  IblBoc::GetSlinkCounters(result.sLinkInfo);
  for (int ch=0; ch<32; ch++) {
    result.decodingError[ch] = IblBoc::GetCounter(ch, IblBoc::DecoError);
    result.frameError[ch]    = IblBoc::GetCounter(ch, IblBoc::FrameError);
    result.frameCount[ch]    = IblBoc::GetFrameCount(ch);
    // Note: these need to be divided by 56535.0 to get fraction
    result.meanOccupancy[ch] = IblBoc::GetLinkOccMean(ch);
    result.liveOccupancy[ch] = IblBoc::GetLinkOccLive(ch);
  }

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__


  if(dumpOnPPC) dump();

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

void BocRegisters::serialize(uint8_t *out) const {
	uint32_t offset = 0;

	for (size_t i=0; i < 32; ++i){
	  Serializer<uint32_t>::serialize(out, offset, decodingError[i]);
	  Serializer<uint32_t>::serialize(out, offset, frameError[i]);
	  Serializer<uint32_t>::serialize(out, offset, frameCount[i]);
	  // Note: these need to be divided by 56535.0 to get fraction
	  Serializer<uint32_t>::serialize(out, offset, meanOccupancy[i]);
	  Serializer<uint32_t>::serialize(out, offset, liveOccupancy[i]);
	}
	Serializer<IblBoc::SLinkInfo>::serialize(out, offset, sLinkInfo);
}

void BocRegisters::deserialize(const uint8_t *in) {
	uint32_t offset =0;
	for (size_t i=0; i < 32; i++){
	  decodingError[i] = Serializer<uint32_t>::deserialize(in, offset);
	  frameError[i] = Serializer<uint32_t>::deserialize(in, offset);
	  frameCount[i] = Serializer<uint32_t>::deserialize(in, offset);
	  // Note: these need to be divided by 56535.0 to get fraction
	  meanOccupancy[i] = Serializer<uint32_t>::deserialize(in, offset);
	  liveOccupancy[i] = Serializer<uint32_t>::deserialize(in, offset);
	}
	sLinkInfo = Serializer<IblBoc::SLinkInfo>::deserialize(in, offset);
}

uint32_t BocRegisters::serializedSize() const {
	return (32*sizeof(uint32_t)*5+sizeof(IblBoc::SLinkInfo));
}

//  LocalWords:  endl
