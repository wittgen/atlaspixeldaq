/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "ReadUartOutput.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void ReadUartOutput::dump() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << std::string(20,'-') << std::endl;
	std::cout << "Slave A output: " << std::endl;
	std::cout << result.slvA_output << std::endl;
	std::cout << std::string(20,'-') << std::endl;
	std::cout << "Slave B output: " << std::endl;
	std::cout << result.slvB_output << std::endl;
	std::cout << std::string(20,'-') << std::endl;
}

ReadUartOutput::ReadUartOutput(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void ReadUartOutput::execute() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

	std::cout << "Clearing buffers first" << std::endl;
	for(size_t i = 0 ; i < sizeof(result.master_output) ; ++i) result.master_output[i] = 0;
	for(size_t i = 0 ; i < sizeof(result.slvA_output) ; ++i) result.slvA_output[i] = 0;
	for(size_t i = 0 ; i < sizeof(result.slvB_output) ; ++i) result.slvB_output[i] = 0;
	std::cout << "Done!" << std::endl;

#ifdef __XMK__

	uint32_t master_output_size = 0;
	while( master_output_size < sizeof(result.master_output) ) {
		uint32_t master_uart_status = IblRod::PpcUartMasterStatusReg::read();
		std::cout << "master_uart_status=" << master_uart_status << std::endl;
		static int cnt = 0 ; if(cnt++>10) break;
		if(!master_uart_status) break ; //Nothing to read
		if(master_output_size + master_uart_status < sizeof(result.master_output)) {
			std::cout << "Reading " << master_uart_status << " bytes from master FIFO" << std::endl;
			for(size_t i = 0 ; i < master_uart_status ; ++i)
				result.master_output[i] = IblRod::PpcUartMasterDataReg::read();
			master_output_size += master_uart_status;
		} else break;
	}

	uint32_t slvA_output_size = 0;
	while( slvA_output_size < sizeof(result.slvA_output) ) {
		uint32_t slvA_uart_status = IblRod::PpcUartS6AStatusReg::read();
		std::cout << "slvA_uart_status=" << slvA_uart_status << std::endl;
		static int cnt = 0 ; if(cnt++>10) break;
		if(!slvA_uart_status) break ; //Nothing to read
		if(slvA_output_size + slvA_uart_status < sizeof(result.slvA_output)) {
			std::cout << "Reading " << slvA_uart_status << " bytes from slvA FIFO" << std::endl;
			for(size_t i = 0 ; i < slvA_uart_status ; ++i)
				result.slvA_output[i] = IblRod::PpcUartS6ADataReg::read();
			slvA_output_size += slvA_uart_status;
		} else break;
	}

	uint32_t slvB_output_size = 0;
	while( slvB_output_size < sizeof(result.slvB_output) ) {
		uint32_t slvB_uart_status = IblRod::PpcUartS6AStatusReg::read();
		std::cout << "slvB_uart_status=" << slvB_uart_status << std::endl;
		static int cnt = 0 ; if(cnt++>10) break;
		if(!slvB_uart_status) break ; //Nothing to read
		if(slvB_output_size + slvB_uart_status < sizeof(result.slvB_output)) {
			std::cout << "Reading " << slvB_uart_status << " bytes from slvB FIFO" << std::endl;
			for(size_t i = 0 ; i < slvB_uart_status ; ++i)
				result.slvB_output[i] = IblRod::PpcUartS6ADataReg::read();
			slvB_output_size += slvB_uart_status;
		} else break;
	}

	if(dumpOnPPC) dump();
#else
	std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void ReadUartOutput::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
}

// Deseralize the data members of the command
// In this case, only one boolean
void ReadUartOutput::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t ReadUartOutput::serializedSize() const
{
	return sizeof(bool);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
void UartOutput::serialize(uint8_t *out) const {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	uint32_t offset = 0;
	for(size_t i = 0 ; i < sizeof(master_output) ; ++i)
		Serializer<uint8_t>::serialize(out, offset, master_output[i]);
	for(size_t i = 0 ; i < sizeof(slvA_output) ; ++i)
		Serializer<uint8_t>::serialize(out, offset, slvA_output[i]);
	for(size_t i = 0 ; i < sizeof(slvB_output) ; ++i)
		Serializer<uint8_t>::serialize(out, offset, slvB_output[i]);

}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void UartOutput::deserialize(const uint8_t *in) {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	uint32_t offset = 0;
	for(size_t i = 0 ; i < sizeof(master_output) ; ++i)
		master_output[i] = Serializer<uint8_t>::deserialize(in, offset);
	for(size_t i = 0 ; i < sizeof(slvA_output) ; ++i)
		slvA_output[i] = Serializer<uint8_t>::deserialize(in, offset);
	for(size_t i = 0 ; i < sizeof(slvB_output) ; ++i)
		slvB_output[i] = Serializer<uint8_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t UartOutput::serializedSize() const {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	return sizeof(master_output)+sizeof(slvA_output)+sizeof(slvB_output);
}
