/*
 * K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2017-III-1
 */

#include "QSTestHelper.h"
#include "RodResourceManager.h"
#include "ReconfigModules.h"
#include "BarrelConnectivity.h"
#include "IblConnectivity.h"
#ifdef __XMK__
#include "IblRodSlave.h"
#endif

#define HEXF(x,y) std::hex << "0x" << std::hex << std::setw(x) << std::setfill('0') << static_cast<int>(y) << std::dec

QSTestHelper::QSTestHelper(){
  setResultPtr();
  m_rxMask = 0x00000000;
  m_qsTestId = Timeout;
}

void QSTestHelper::execute() {
#ifdef __XMK__
  switch (m_qsTestId) {
    case Timeout: {
      uint32_t qsTestMask = ReconfigModules::augmentMask(m_rxMask);
      std::cout << HEXF(8, m_rxMask) << " was selected as rxMask" << std::endl;
      std::cout << HEXF(8, qsTestMask) << " will be used as augmented rxMask" << std::endl;

      // read status of both formatter links
      uint32_t statusSlaveA = IblRod::SlaveA::FmtLinkEnable::read();
      uint32_t statusSlaveB = IblRod::SlaveB::FmtLinkEnable::read();

      uint32_t formatterStatus = statusSlaveA | (statusSlaveB << 16);
      std::cout << "Formatter status: " <<  HEXF(8, formatterStatus) << std::endl;
      std::cout << "Augmented formatter status: " << HEXF(8, ReconfigModules::augmentMask(formatterStatus)) << std::endl;

      // stop trigger, for this stop tx serial line
      SerialPort* sPort = NULL;
      if( RodResMgr::isFei3() ){
        sPort = RodResourceManagerPIX::getSerialPort();
        sPort->setTxMask(BarrelConnectivity::getTxMaskFromRxMask(formatterStatus & (~qsTestMask)));
      }
      else{
        sPort = RodResourceManagerIBL::getSerialPort();
        sPort->setTxMask(IblConnectivity::getTxMaskFromRxMask(formatterStatus & (~qsTestMask)));
      }
    }; break;
    default:
      std::cerr << "ERROR: qsTestId " << m_qsTestId << " is not recognized" << std::endl;
  }
#endif
}

void QSTestHelper::serialize(uint8_t *out) const
{
  uint32_t offset = 0;
  Serializer<QSTestType>::serialize(out,offset,m_qsTestId);
  Serializer<uint32_t>::serialize(out,offset,m_rxMask);
}
void QSTestHelper::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  m_qsTestId = Serializer<QSTestType>::deserialize(in,offset);
  m_rxMask = Serializer<uint32_t>::deserialize(in,offset);
}
uint32_t QSTestHelper::serializedSize() const
{
  return sizeof(QSTestType)+sizeof(uint32_t);
}
