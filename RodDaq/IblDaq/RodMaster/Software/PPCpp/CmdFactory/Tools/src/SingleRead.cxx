/*
 * L. Jeanty <laura.jeanty@cern.ch>
 */

#include "SingleRead.h"

#ifdef __XMK__
#include "RodMasterRegisters.h"
#endif

SingleRead::SingleRead(){ 
  setResultPtr(); 
  address = 0;
  result.value = 0;
}

void SingleRead::execute() {
#ifdef __XMK__
  uint32_t value =  IblRod::EpcBase::read(address);
  std::cout<<"Read back value: 0x"<<std::hex<<value<<std::dec<<std::endl;
  result.value = value;
#endif
}

void SingleRead::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,address);
}
void SingleRead::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	address = Serializer<uint32_t>::deserialize(in,offset);
}
uint32_t SingleRead::serializedSize() const
{
  return (sizeof(uint32_t));
}

void RegValue::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,value);
}
void RegValue::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	value = Serializer<uint32_t>::deserialize(in,offset);
}
uint32_t RegValue::serializedSize() const
{
  return (sizeof(uint32_t));
}
