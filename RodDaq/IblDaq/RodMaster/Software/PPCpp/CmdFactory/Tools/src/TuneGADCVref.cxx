#include "TuneGADCVref.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "Fei4Cmd.h"
#include "Fei4Proxy.h"
#include <iomanip>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <functional>
#endif

TuneGADCVref::TuneGADCVref() {

	setResultPtr();
	m_rxMask =0xFFFFFFFF;
}

void TuneGADCVref::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei4()) return;

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->enableRxChannels(m_rxMask);
  uint32_t rxMask = iblBocLink->getEnChMask();
  
  m_rxMask = rxMask;
  
  Fei4Proxy fei4 = Fei4Proxy(rxMask);
  
  fei4.setRunMode(false);
  
  Mask dc_mask;
  uint32_t bitMask [DoubleColumnBit::nWords];
  uint32_t iPix=0, pixelIndex=0;

   //Enable first Pixel in first double column to check ILEAK range
   for(unsigned iDC=0;iDC<40;iDC++){
     for(unsigned j=0;j<DoubleColumnBit::nWords;j++){
        if(iPix == pixelIndex){
	  bitMask[j] = ((1<<pixelIndex%32));
	   //if(pixelIndex%100 == 0)std::cout<<"Scanning Pixel "<<pixelIndex<<" DC "<<(int)iDC <<" nWord "<<(int)j <<" mask "<<std::hex<<bitMask[j]<<std::dec<<std::endl;
        } else{
	    bitMask[j] = 0x0;
	}
      iPix++;
     }
    dc_mask.set(bitMask);
    fei4.strobePixelLatches(dc_mask, 0x100, iDC); //write pixel registers
   }

 //Copy from Latches to SR
 fei4.globalPulse(10);

 GADCVrefResults res;
  
   for (uint32_t rxCh = 0; rxCh < 32; rxCh++){
     if ( !(rxMask & (1<<rxCh) ) )continue;
       std::cout<<"Scanning Rx "<<(int)rxCh<<std::endl;
         for(uint32_t vref=0;vref<255; vref+=2){
           if(vref==0)getRange(rxCh,vref,res);//First reading is somehow wrong
           getRange(rxCh,vref,res);
           result.GADCVref[rxCh].push_back(res);
        }
   }


  iblBocLink->disableRxChannels(rxMask);
  
#endif

}

void TuneGADCVref::getRange(uint32_t rxCh, uint32_t vref, GADCVrefResults &res){

#ifdef __XMK__

  uint32_t ILeak, vcalStart, vcalStop;

  Fei4 *fes = RodResourceManagerIBL::getCalibModuleConfigs();
  Fei4 fei4;
  
  //std::cout<<"Vref "<<(int)vref<<std::endl;

   for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++)
    if(rxCh == fes[i].getRxCh()){fei4= fes[i]; break;}

  fei4.writeRegister(&Fei4::GADCVref,vref);
  fei4.writeRegister(&Fei4::GADCSel, 7);
  fei4.writeRegister(&Fei4::ILR,0);//ILR=1 for 10kOhm, ILR=0 for 90kOhm ILeak Hitbus 

  fei4.writeRegister(&Fei4::ADC,1);
  fei4.globalPulse(63);
  ILeak = (fei4.getRegisterValueFromFE(40) & 0xFFFF)  >> 4;
  fei4.writeRegister(&Fei4::ADC,0);

  //std::cout<<"ILeak "<<(int)ILeak<<std::endl;
  
  int vcalStep = 5,vcal=0;
  vcalStart=1024; vcalStop=0;
  
  fei4.writeRegister(&Fei4::GADCSel, 5);

    while(vcal<1024){
      fei4.writeRegister(&Fei4::PlsrDAC,vcal);
      fei4.writeRegister(&Fei4::ADC,1);
      fei4.globalPulse(63);
      
      uint32_t val = (fei4.getRegisterValueFromFE(40) & 0xFFFF) >> 4;
      fei4.writeRegister(&Fei4::ADC,0);
      
      if(val>0 && vcalStart == 1024){
      vcalStart= vcal;
      }
      
      if(val == 1023){vcalStop= vcal; break;}
      else vcalStop = vcal;
      
      //if(vcal%100 ==0)std::cout<<vcal<<" "<<(int)val<<std::endl; 
      
      vcal+=vcalStep;
    }
    
    res.ILeak=ILeak;
    res.Vref = vref;
    res.vCalRange = vcalStop - vcalStart;

#endif

}

void TuneGADCVref::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset,  m_rxMask);
}

void TuneGADCVref::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t TuneGADCVref::serializedSize() const
{
	return sizeof(uint32_t);
}

void TuneGADCVrefResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<std::map <uint8_t, std::vector <GADCVrefResults> > >::serialize(out,offset,GADCVref);
}

void TuneGADCVrefResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
        GADCVref = Serializer<std::map <uint8_t, std::vector <GADCVrefResults> > >::deserialize(in,offset);
}

uint32_t TuneGADCVrefResults::serializedSize() const
{
	std::size_t sizeMap = Serializer<std::map <uint8_t, std::vector <GADCVrefResults> > >::size(GADCVref);
	return sizeMap;
}

