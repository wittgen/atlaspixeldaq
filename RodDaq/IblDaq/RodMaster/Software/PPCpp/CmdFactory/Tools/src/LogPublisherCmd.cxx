#include "LogPublisherCmd.h"
#include "Serializer.h"

//---------------------------------------------------------------------------

// Specialization for LogPublisher
template < >
class Serializer<LPConfig> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const LPConfig& obj) {
      Serializer<int>::serialize(data, offset, obj.mode);
      Serializer<std::string>::serialize(data, offset, obj.receiver_ip);
      Serializer<uint16_t>::serialize(data, offset, obj.receiver_port);
      Serializer<int>::serialize(data, offset, obj.time_standby_max);
      Serializer<int>::serialize(data, offset, obj.log_switch);
      Serializer<int>::serialize(data, offset, obj.slave_verbose);
      Serializer<int>::serialize(data, offset, obj.sleep);
      Serializer<int>::serialize(data, offset, obj.verbosity);
  }

  static LPConfig deserialize(const uint8_t* data, uint32_t& offset) {
    LPConfig obj;
    deserialize(data, offset, obj);
    return obj;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, LPConfig& obj) {
        Serializer<int>::deserialize(data, offset, obj.mode);
        Serializer<std::string>::deserialize(data, offset, obj.receiver_ip);
        Serializer<uint16_t>::deserialize(data, offset, obj.receiver_port);
        Serializer<int>::deserialize(data, offset, obj.time_standby_max);
        Serializer<int>::deserialize(data, offset, obj.log_switch);
        Serializer<int>::deserialize(data, offset, obj.slave_verbose);
        Serializer<int>::deserialize(data, offset, obj.sleep);
        Serializer<int>::deserialize(data, offset, obj.verbosity);
  }
  static std::size_t size(const LPConfig& obj) {

        return sizeof(obj.mode)
                +Serializer<std::string>::size(obj.receiver_ip)
                +sizeof(obj.receiver_port)
                +sizeof(obj.time_standby_max)
                +sizeof(obj.log_switch)
                +sizeof(obj.slave_verbose)
                +sizeof(obj.sleep)
                +sizeof(obj.verbosity);
  }
};

//---------------------------------------------------------------------------

LPConfig::LPConfig() {
    LPNoValue nv;
    nv.set(mode);
    nv.set(receiver_ip);
    nv.set(receiver_port);
    nv.set(time_standby_max);
    nv.set(log_switch);
    nv.set(slave_verbose);
    nv.set(sleep);
    nv.set(verbosity);

}

// helper, print NONE if not defined
template<class T>
std::string inline IFNONE(T var) {
    if(var==LPNoValue()) return "NONE";
    else {
        std::stringstream ss;
        ss << var;
        return ss.str();
    }
}

std::string LPConfig::print() const {

    std::stringstream ss;
    ss << " mode:"  			   << IFNONE(mode)
       << " rec_ip:"  		   << IFNONE(receiver_ip)
       << " rec_port:"       	<< IFNONE(receiver_port)
       << " time_standby_max:"  << IFNONE(time_standby_max)
       << " log_switch:"  		<< IFNONE(log_switch)
       << " slave_verbose:"  	<< IFNONE(slave_verbose)
       << " sleep:"  			<< IFNONE(sleep)
       << " verbosity:"  	   << IFNONE(verbosity);

    return ss.str();
}

//---------------------------------------------------------------------------

void LogPublisherCmd::execute()
{

    LogPublisher * logger = LogPublisher::getInstance();
    if(logger) {
        logger-> setConfig(config);
        logger-> getConfig(result.rconfig);  // read back the configuration
    }
}


void LogPublisherCmd::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<LPConfig>::serialize(out, offset, config);
}


void LogPublisherCmd::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        config = LPConfig();  // initialize to empty values
        Serializer<LPConfig>::deserialize(in,offset, config);
}


uint32_t LogPublisherCmd::serializedSize() const
{
    return Serializer<LPConfig>::size(config);
}


void LPCmdResult::serialize(uint8_t *out) const
{
    uint32_t offset = 0;
    Serializer<LPConfig>::serialize(out, offset, rconfig);
}


void LPCmdResult::deserialize(const uint8_t *in)
{
    uint32_t offset = 0;
    rconfig = LPConfig();  // initialize to empty values
    Serializer<LPConfig>::deserialize(in, offset, rconfig);
}


uint32_t LPCmdResult::serializedSize() const
{
    return Serializer<LPConfig>::size(rconfig);
}

