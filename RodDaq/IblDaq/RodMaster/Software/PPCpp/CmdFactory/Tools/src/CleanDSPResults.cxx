 
#include "CleanDSPResults.h"
#include "RodResourceManager.h"
#include "ScanBoss.h"
CleanDSPResults::CleanDSPResults(){
  setResultPtr();
  rxCh=-1;
}  

void CleanDSPResults::execute() {
#ifdef __XMK__
  if(rxCh >32){
    if(RodResMgr::isFei4())ScanBoss::getScan().cleanDSPResults( );
    else ScanBoss::getScanL12().cleanDSPResults( );
  } else {
    if(RodResMgr::isFei4())ScanBoss::getScan().cleanDSPResults(rxCh);
    else ScanBoss::getScanL12().cleanDSPResults(rxCh);
  }
#endif
}

void CleanDSPResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint8_t>::serialize(out, offset, rxCh);
}

void CleanDSPResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	rxCh = Serializer<uint8_t>::deserialize(in, offset);
}

uint32_t CleanDSPResults::serializedSize() const
{
	return sizeof(uint8_t);
}
