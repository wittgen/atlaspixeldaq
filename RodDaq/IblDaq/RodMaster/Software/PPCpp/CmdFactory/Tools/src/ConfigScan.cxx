#include "ConfigScan.h"
#include "ScanBoss.h"

ConfigScan::ConfigScan(){ 
  verboseScan = false;
  debugReadBackBoc = false;
  debugReadBackInmem = false;
  debugReadSR = false;
  debugRxCh = 0;
  debugDCLoops = 0;
  debugChannels = 0;

  histData = Fei4Data::OCC;
  histFIFO = Fei4Data::BOC;
  histRxCh = 0xFF;
}

void ConfigScan::execute(){
#ifdef __XMK__
  ScanBoss::verbose 	  = verboseScan;
  ScanBoss::readBackBoc   = debugReadBackBoc;
  ScanBoss::readBackInmem = debugReadBackInmem;
  ScanBoss::readSR = debugReadSR;
  ScanBoss::rxCh 	  = debugRxCh;
  ScanBoss::debugDCLoops  = debugDCLoops;
  ScanBoss::debugChannels  = debugChannels;
  
  if (histRxCh != 0xFF) ScanBoss::addDebugHist(histData, histFIFO, histRxCh);
  if (clearDebugHists) ScanBoss::clearDebugHists();
#endif
}

void ConfigScan::serialize(uint8_t *out) const{
  uint32_t offset = 0; 
  Serializer<bool>::serialize(out, offset, verboseScan);
  Serializer<bool>::serialize(out, offset, debugReadBackBoc);
  Serializer<bool>::serialize(out, offset, debugReadBackInmem);
  Serializer<bool>::serialize(out, offset, debugReadSR);
  Serializer<uint8_t>::serialize(out, offset, debugRxCh);
  Serializer<uint8_t>::serialize(out, offset, debugDCLoops);
  Serializer<uint32_t>::serialize(out, offset, debugChannels);
  Serializer<Fei4Data::DataType>::serialize(out, offset, histData); 
  Serializer<Fei4Data::FIFOType>::serialize(out, offset, histFIFO); 
  Serializer<uint8_t>::serialize(out, offset, histRxCh);		
  Serializer<bool>::serialize(out, offset, clearDebugHists);   
}

void ConfigScan::deserialize(const uint8_t *in){
  uint32_t offset = 0; 
  verboseScan = Serializer<bool>::deserialize(in, offset);
  debugReadBackBoc = Serializer<bool>::deserialize(in, offset);
  debugReadBackInmem = Serializer<bool>::deserialize(in, offset);
  debugReadSR = Serializer<bool>::deserialize(in, offset);
  debugRxCh = Serializer<uint8_t>::deserialize(in, offset);
  debugDCLoops = Serializer<uint8_t>::deserialize(in, offset);
  debugChannels = Serializer<uint32_t>::deserialize(in, offset);
  histData = Serializer<Fei4Data::DataType>::deserialize(in, offset);
  histFIFO = Serializer<Fei4Data::FIFOType>::deserialize(in, offset);
  histRxCh = Serializer<uint8_t>::deserialize(in, offset);
  clearDebugHists = Serializer<bool>::deserialize(in, offset);
}

uint32_t ConfigScan::serializedSize() const{
  return 5*sizeof(bool) + 3*sizeof(uint8_t) + 3*sizeof(uint32_t);
}


