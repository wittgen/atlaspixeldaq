#include "DataTakingTools.h"
#include "Fei4Proxy.h"
#include "Fei3ModProxy.h"

#ifdef __XMK__
#include "IblBoc.h"
#include "RodResourceManager.h"
#include "IblConnectivity.h"
#endif

DataTakingTools::DataTakingTools() {
  setResultPtr();
  action = Print;
  channels = 0xFFFFFFFF;
  value = 0;
  address = 0;
  result.value = 0;
  pixSpeed = IblBoc::Speed_40;
  frameCount = 1;
}

void  DataTakingTools::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  
#ifdef __XMK__

  switch(action) {
  case Print : 
    print();
    break;
  case FeBCR :
    feBCR(channels);
    break;
  case FeECR :
    feECR(channels);
    break;
  case FeTrigger :
    feTrigger(channels);
    break;
  case FeSendGlobalConfigAmpsOn :
    feSendGlobalConfigAmpsOn(channels);
    break;
  case FeSendGlobalConfigAmpsOff :
    feSendGlobalConfigAmpsOff(channels);
    break;
  case FeSendPixelConfig :
    feSendPixelConfig(channels);
    break;
  case FeWriteReg :
    feWriteReg(channels, address, value);
    break;
  case FeSendReg :
    feSendReg(channels, address);
    break;
  case FeSetRunMode :
    feSetRunMode(channels, value);
    break;
  case EnableRxPath :
    enableRxPath(channels);
    break;   
  case DisableRxPath :
    disableRxPath(channels);
    break;   
  case EnableBocRx :
    enableBocRx(channels);
    break;
  case EnableBocTx :
    enableBocTx(channels);
    break;
  case DisableBocRx :
    disableBocRx(channels);
    break;   
  case EnableFmtLink :
    enableFmtLink(channels);
    break;
  case EnableFmtLinkAtECR :
    enableFmtLinkAtECR(channels);
    break;
  case DisableFmtLink :
    disableFmtLink(channels);
    break;   
  case SetRunNumber : 
    setRunNumber(value);
    break;
  case SetECRCounter : 
    setECRCounter(value);
    break;
  case ReadECRCounter : 
    result.value = readECRCounter();
    break;
  case SetBcidOffset : 
    setBcidOffset(value);
    break;
  case SLinkEnable : 
    sLinkEnable((SLinkEnableMode)value);
    break;
  case SetBocEmu :
    setBocEmu(value, channels, frameCount, pixSpeed);
    break;
  case SetRodEmu :
    setRodEmu(value, frameCount, pixSpeed);
    break;
  case DisableSerialLine :
    disableSerialLine();
    break;   
  case BocResampleRxPhase :
    bocResampleRxPhase(channels);
    break;
  case SetAmp2VbnStandby :
    setAmp2VbnStandby(channels, value);
    break;
  case SetPrmpVbpStandby :
    setPrmpVbpStandby(channels, value);
    break;
  case SetDisVbnStandby :
    setDisVbnStandby(channels, value);
    break;
  case SetEfbMaskReg :
    setEfbMaskReg(value);
    break;
  case SetEfbMaskRegAtECR :
    setEfbMaskRegAtECR(value);
    break;
  case SetHitDiscCnfg : 
    setHitDiscCnfg(channels, value);
    break;
  case SetStandby3DBM :
    setStandby3DBM(channels, value);
    break;
  case SendConfigAtECR :
    sendConfigAtECR(channels, value);
    break;
  case BocManualConfig :
    bocManualConfig(channels);
    break;
  case ResetModules :
    resetModules( channels, value);
    break;
  case SetRODVetoTimeAtECR :
    setRODVetoTimeAtECR(value);
    break;
  case SetRODBusyTimeAtECR :
    setRODBusyTimeAtECR(value);
    break;
  case SetRODResetTimeAfterECR :
    setRODResetTimeAfterECR(value);
    break;
  case SetFmtDataOverflowLimit :
    setFmtDataOverflowLimit(value);
    break;
  case SetFmtTrailerTimeoutLimit :
    setFmtTrailerTimeoutLimit(value);
    break;
  case SetRodBusyMask :
    setRodBusyMask(channels);
    break;
  default : 
    std::cout<<"You didn't choose a sensible action. Please try again. You sent action type: "<<(int)action<<std::endl;
    print();
  }
	
#endif
}

void DataTakingTools::print() {

  std::cout<<std::endl;
  std::cout<<"===================================================================="<<std::endl;
  std::cout<<"This command allows you to perform a number of specific actions."<<std::endl;
  std::cout<<"===================================================================="<<std::endl;
  std::cout<<"You can set the command parameters: channels, address, and value, which are relevant for different options."<<std::endl;
  std::cout<<"Choose your action with command.action = ENUM;"<<std::endl;
  std::cout<<"Your enum options are: (parameters or return values considered for each option shown in paranthesis)"<<std::endl; 
  std::cout<<std::endl;
  std::cout<<"FeBCR : issue a bcr to the front end (channels)"<<std::endl;
  std::cout<<"FeECR : issue an ecr to the front end (channels)"<<std::endl;
  std::cout<<"FeTrigger : issue a single trigger to the front end (channels)"<<std::endl;
  std::cout<<"FeSendGlobalConfigAmpsOn : send global register values w/ pre-amps ON to the front end (channels)"<<std::endl;
  std::cout<<"FeSendGlobalConfigAmpsOff : send global register values w/ pre-amps OFF to the front end (channels)"<<std::endl;
  std::cout<<"FeSendPixelConfig : send pixel register values to the front end (channels)"<<std::endl;
  std::cout<<"FeWriteReg : write a specific front end reg by address (channels, address, value)"<<std::endl;
  std::cout<<"FeSendReg : send a specific front end reg by address (channels, address)"<<std::endl;
  std::cout<<"FeSetRunMode : set FE run mode (channels, value (0 or 1))"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"EnableRxPath : enable full rx path (channels)"<<std::endl;
  std::cout<<"DisableRxPath : disable full rx path (channels)"<<std::endl;
  std::cout<<"EnableBocRx : enable the Rx BOC links (channels)"<<std::endl;
  std::cout<<"EnableBocTx : enable the Tx BOC links (channels)"<<std::endl;
  std::cout<<"DisableBocRx : disable the rx BOC links (channels)"<<std::endl;
  std::cout<<"EnableFmtLink : enable fmt link (channels)"<<std::endl;
  std::cout<<"EnableFmtLinkAtECR : enable fmt link at ECR (channels)"<<std::endl;
  std::cout<<"DisableFmtLink : disable fmt link (channels)"<<std::endl;
  std::cout<<std::endl;
  std::cout<<"SetRunNumber : set the slave run number (value)"<<std::endl;  
  std::cout<<"SetECRCounter : set the master ecr counter (value)"<<std::endl;  
  std::cout<<"ReadECRCounter : read the ecr counter (returns result.value)"<<std::endl;  
  std::cout<<"SetBcidOffset : set the slave bcid offset (value)"<<std::endl;  
  std::cout<<"SLinkEnable : enable the slink (value, use SlinkIgnoreFTK,SlinkEnableFTKXOff, SlinkEnableFTKLDown, SlinkEnableFTKBoth enum)"<<std::endl;  
  std::cout<<std::endl;
  std::cout<<"SetBocEmu : set the boc FE emulator mode to true or false (value)"<<std::endl;
  std::cout<<"SetRodEmu : set the rod FE emulator mode to true or false (value)"<<std::endl;
  std::cout<<"BocResampleRxPhase : ask the BOC to resample the RX phase for the given channels (channels)"<<std::endl;
  std::cout<<"DisableSerialLine : set tx serial port to 0x0"<<std::endl; 
  std::cout<<"SetAmp2VbnStandby : change the warm-start preAmpOFF value for amp2VnbStandby (value, channels)"<<std::endl;
  std::cout<<"SetPrmpVbpStandby : change the warm-start preAmpOFF value for prmpVbpStandby (value, channels)"<<std::endl;
  std::cout<<"SetStandby3DBM : set whether or not to enable the outermost 3DBM outermost DC in standby (value, channels)"<<std::endl;
  std::cout<<"SetDisVbnStandby : change the warm-start preAmpOFF value for disVbnStandby (value, channels)"<<std::endl;
  std::cout<<"SetEfbMaskReg : set the EfbMaskReg value (value)"<<std::endl;
  std::cout<<"SetEfbMaskRegAtECR : set the EfbMaskReg value at ECR (value)"<<std::endl;
  std::cout<<"ComputeMasterEfbReg : compute EfbMaskReg (channels)"<<std::endl;
  std::cout<<"SetHitDiscCfng : set the FE Hit Disc Cnfg value (useful for calibration) (value, channels)"<<std::endl;
  std::cout<<"SetRodBusyMask : set the ROD Busy Mask on Master and Slaves based on the enable modules (channels)"<<std::endl;
}

void DataTakingTools::feBCR(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending an BCR"<<std::endl;
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.bcr();
#endif
}

void DataTakingTools::feECR(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending an ECR"<<std::endl;
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.ecr();
#endif
}

void DataTakingTools::feTrigger(uint32_t channels) {
#ifdef __XMK__
  Fei4Proxy fei4 = Fei4Proxy(channels);
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending a trigger"<<std::endl;
  fei4.trigger();
#endif
}

void DataTakingTools::feSendGlobalConfigAmpsOn(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending global config"<<std::endl;
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.sendGlobalConfigAmpsOn();
#endif
}

void DataTakingTools::feSendGlobalConfigAmpsOff(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending global config"<<std::endl;
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.sendGlobalConfigAmpsOff();
#endif
}

void DataTakingTools::feSendPixelConfig(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" sending pixel config"<<std::endl;
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.sendPixelConfig(false);
#endif
}

void DataTakingTools::setHitDiscCnfg(uint32_t channels, uint32_t value) {
#ifdef __XMK__
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.writeRegister(&Fei4::HD, value);
  if (value == 0) fei4.writeRegister(&Fei4::SME, 1); // set small hit erase to 1 when hit disc config = 0
  else fei4.writeRegister(&Fei4::SME, 0); // set small hit erase to 0 when hit disc config != 0
  // set ROD register appropriately as well
  writeHitDiscConfig(channels);
#endif
}

void DataTakingTools::writeHitDiscConfig(uint32_t channels) {
  // write hit disc confg value from FE to slave registers                                                                                                               
  uint32_t hdcValue0 = 0;
  uint32_t hdcValue1 = 0;

  Fei4 *fes = RodResourceManagerIBL::getCalibModuleConfigs(); // to-do: get calib or physics configs

  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
    if (channels & (1<<i)) {
      (i < 16) ? (hdcValue0 = hdcValue0 | (fes[i].getValue(&Fei4::HD)<<(i*2))) :
	(hdcValue1 = hdcValue1 | (fes[i].getValue(&Fei4::HD)<<((i-16)*2)));
    }
  }

  IblRod::SlaveA::HitDiscConfig::write(hdcValue0);
  IblRod::SlaveB::HitDiscConfig::write(hdcValue1);

  std::cout<<" setting Slave A hitdiscconfig to: 0x"<<std::hex<<(int)IblRod::SlaveA::HitDiscConfig::read()<<std::dec<<std::endl; // tmp
  std::cout<<" setting Slave B hitdiscconfig to: 0x"<<std::hex<<(int)IblRod::SlaveB::HitDiscConfig::read()<<std::dec<<std::endl; // tmp
}

void DataTakingTools::trickHitDiscConfigCalib( ) {
  // Force HitdisConfig to 0 in the ROD, for calibration purposes
  IblRod::SlaveA::HitDiscConfig::write(0x0);
  IblRod::SlaveB::HitDiscConfig::write(0x0);

  std::cout<<" setting Slave A hitdiscconfig to: 0x"<<std::hex<<(int)IblRod::SlaveA::HitDiscConfig::read()<<std::dec<<std::endl; // tmp
  std::cout<<" setting Slave B hitdiscconfig to: 0x"<<std::hex<<(int)IblRod::SlaveB::HitDiscConfig::read()<<std::dec<<std::endl; // tmp
}


void DataTakingTools::feWriteReg(uint32_t channels, uint32_t addr, uint32_t val) {
#ifdef __XMK__
  Fei4Proxy fei4 = Fei4Proxy(channels);
  fei4.setRunMode(false);
  uint16_t address = 0x0000FFFF & addr;
  uint16_t value = 0x0000FFFF & val;

  std::cout<<"for channels: 0x"<<std::hex<<" setting reg 0x"<<(int)address<<" to: "<<std::dec<<(int)value<<std::endl;
  fei4.writeRegister(address, value);
#endif
}

void DataTakingTools::feSendReg(uint32_t channels, uint32_t addr) {
#ifdef __XMK__
  Fei4Proxy fei4 = Fei4Proxy(channels);
  uint16_t address = 0x0000FFFF & addr;

  std::cout<<"for channels: 0x"<<std::hex<<" sending reg 0x"<<(int)address<<std::dec<<std::endl;
  fei4.sendRegister(address);
#endif
}

void DataTakingTools::feSetRunMode(uint32_t channels, uint32_t value) {
#ifdef __XMK__
  Fei4Proxy fei4 = Fei4Proxy(channels);
  bool mode = (0xFFFFFFFD & value);
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<" setting run mode to: "<<std::dec<<(int)mode<<std::endl;
  fei4.setRunMode(mode);
#endif
}

void DataTakingTools::disableRxPath(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" disabling full Rx path"<<std::endl;
  DataTakingTools::disableBocRx(channels);
  DataTakingTools::disableFmtLink(channels);
#endif
}

void DataTakingTools::enableRxPath(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" enabling full Rx path"<<std::endl;
  DataTakingTools::enableBocRx(channels);
  DataTakingTools::enableFmtLink(channels);
#endif
}

void DataTakingTools::enableBocRx(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" enabling BOC RX channels"<<std::endl;
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  iblBocLink->enableRxChannels(channels);
#endif
}

void DataTakingTools::enableBocTx(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" enabling BOC TX channels"<<std::endl;
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(channels));
#endif
}

void DataTakingTools::disableBocRx(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" disabling BOC RX channels"<<std::endl;
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  iblBocLink->disableRxChannels(channels);
#endif
}

void DataTakingTools::enableFmtLink(uint32_t channels) {
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" enabling FMT Links"<<std::endl;
  IblRod::SlaveA::FmtLinkEnable::write((channels & 0x0000FFFF));
  IblRod::SlaveB::FmtLinkEnable::write((channels & 0xFFFF0000)>>16);

  uint32_t masterEfbReg = DataTakingTools::computeMasterEfbReg(channels);
  DataTakingTools::setEfbMaskReg(masterEfbReg);

}

void DataTakingTools::enableFmtLinkAtECR(uint32_t channels) {
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" enabling FMT Links at ECR"<<std::endl;
  IblRod::SlaveA::FmtLinkEnableEcr::write((channels & 0x0000FFFF));
  IblRod::SlaveB::FmtLinkEnableEcr::write((channels & 0xFFFF0000)>>16);

  DataTakingTools::setEfbMaskRegAtECR(DataTakingTools::computeMasterEfbReg(channels));
}

void DataTakingTools::disableFmtLink(uint32_t channels) {
  uint32_t enabledLinksRemaining = 0x0;
  std::cout<<"for channels: 0x"<<std::hex<<(int)channels<<std::dec<<" disabling FMT Links"<<std::endl;
  uint32_t currentValue = IblRod::SlaveA::FmtLinkEnable::read();
  std::cout<<"for slave A: current links: 0x"<<std::hex<<(int)currentValue<<" and new links will be: 0x"<<(int)((currentValue)&(~(channels&0x0000FFFF)))<<std::dec<<std::endl;
  IblRod::SlaveA::FmtLinkEnable::write((currentValue)&(~(channels&0x0000FFFF)));
  enabledLinksRemaining |= ((currentValue)&(~(channels&0x0000FFFF)));
  currentValue = IblRod::SlaveB::FmtLinkEnable::read();
  std::cout<<"for slave B: current links: 0x"<<std::hex<<(int)currentValue<<" and new links will be: 0x"<<(int)((currentValue)&(~((channels & 0xFFFF0000)>>16)))<<std::dec<<std::endl;  
  IblRod::SlaveB::FmtLinkEnable::write((currentValue)&(~((channels & 0xFFFF0000)>>16)));
  enabledLinksRemaining |= (((currentValue)&(~((channels & 0xFFFF0000)>>16)))<<16);

  uint32_t masterEfbReg = DataTakingTools::computeMasterEfbReg(enabledLinksRemaining);
  DataTakingTools::setEfbMaskReg(masterEfbReg);
  std::cout << "Remaining links mask: 0x" << std::hex << enabledLinksRemaining << ", with a master EFB mask of 0x" << masterEfbReg << std::dec << std::endl;

}

void DataTakingTools::setRunNumber(uint32_t value) {
#ifdef __XMK__
  std::cout<<"setting run number to: 0x"<<std::hex<<(int)value<<std::dec<<std::endl;
  IblRod::SlaveA::RunNumber::write(value);
  IblRod::SlaveB::RunNumber::write(value);
#endif
}

void DataTakingTools::setECRCounter(uint32_t value) {
#ifdef __XMK__
  std::cout<<"setting ECR counter to: 0x"<<std::hex<<(int)value<<std::dec<<std::endl;
  uint32_t current = IblRod::Master::SprCtrlReg::read();
  uint32_t newValue = (current | 0x00001000);
  IblRod::Master::SprCtrlReg::write(newValue);
  IblRod::Master::EcrCntIReg::write(value);
  IblRod::Master::SprCtrlReg::write(newValue);
  IblRod::Master::SprCtrlReg::write(current);
#endif
}

void DataTakingTools::setEfbMaskReg(uint32_t value) {
#ifdef __XMK__
  std::cout<<"setting EfbMaskReg to: 0x"<<std::hex<<(int)value<<std::dec<<std::endl;
  //uint32_t regValue = IblRod::Master::EfbMaskReg::read();
  //std::cout<<" before writing, value = "<<regValue<<std::endl;
  IblRod::Master::EfbMaskReg::write(value);
  //regValue = IblRod::Master::EfbMaskReg::read();
  //std::cout<<" after writing, value = "<<regValue<<std::endl;
#endif
}

void DataTakingTools::setEfbMaskRegAtECR(uint32_t value) {
#ifdef __XMK__
  std::cout<<"setting EfbMaskRegEcr to: 0x"<<std::hex<<(int)value<<std::dec<<std::endl;
  //uint32_t regValue = IblRod::Master::EfbMaskRegEcr::read();
  //std::cout<<" before writing, value = "<<regValue<<std::endl;
  IblRod::Master::EfbMaskRegEcr::write(value);
  //regValue = IblRod::Master::EfbMaskRegEcr::read();
  //std::cout<<" after writing, value = "<<regValue<<std::endl;
#endif
}

uint8_t DataTakingTools::computeMasterEfbReg(uint32_t channels) {
// Added by: Chris Martin for new FW on 07/03/2016
  uint8_t masterEfbReg = 0x0;
#ifdef __XMK__
//Added by JuanAn internal adjustment of channel mask depending on the speed
//TODO change the logic if the ROD FW is properly updated with IblBoc::Speed_160_Alt (which FMT in the enable mask)
  if(IblRod::SlaveA::FmtFei3RegS1::read() == 0x42 || IblRod::SlaveB::FmtFei3RegS1::read() == 0x42 ){
   uint32_t rodMask=0;

   for(uint32_t j=0; j<8; j++){
      if(IblRod::SlaveA::FmtFei3RegS1::read() == 0x42){
      rodMask += (((channels >> (8 + j))&0x1) << (j*2));
      rodMask += (((channels >> (8 + j))&0x1) << (j*2+1));
      }
      if(IblRod::SlaveB::FmtFei3RegS1::read() == 0x42){
      rodMask += (((channels >> (24 + j))&0x1) << (j*2+16));
      rodMask += (((channels >> (24 + j))&0x1) << (j*2+17));
      }
    }
  channels =rodMask;
  std::cout<<"Computing new EFB at 160, assuming new FMT mask: "<<std::hex<<channels<<std::dec<<std::endl;
  }


  if((channels & 0xFF) == 0x0) {
        masterEfbReg |= 0x1;
        }
  if(((channels >> 8) & 0xFF) == 0x0) {
        masterEfbReg |= 0x2;
        }
  if(((channels >> 16) & 0xFF) == 0x0) {
        masterEfbReg |= 0x4;
        }
  if(((channels >> 24) & 0xFF) == 0x0) {
        masterEfbReg |= 0x8;
        }
  std::cout << "Computed masterEfbReg: " << HEX(masterEfbReg)<<" for channels "<<HEX(channels) << std::endl;
#endif
  return masterEfbReg;
}

void DataTakingTools::setRodBusyMask(uint32_t channels) {
#ifdef __XMK__
  std::cout<<"Setting ROD Busy Mask using input channels: "<<HEX(channels)<<std::endl;

  uint16_t slaveChannels=0;
  uint32_t  maskValue=0;
  uint32_t forceValue=0; // no need to set the force to anything different from 0...only different for 0 for special custom tests..

  slaveChannels = channels & 0xFFFF;
  maskValue=DataTakingTools::computeSlaveBusyMask(slaveChannels,0);
  IblRod::SlaveA::BusyMaskReg::write(maskValue);
  IblRod::SlaveA::BusyForceReg::write(forceValue);
  std::cout<<"\n\t Set slave A busy mask to "<<HEX(maskValue)<<" and force to "<<HEX(forceValue)<<std::endl<<std::endl;
  std::cout<<"Reading back: " << HEX( IblRod::SlaveA::BusyMaskReg::read() ) << " and " << HEX( IblRod::SlaveA::BusyForceReg::read() ) << std::endl;

  slaveChannels = (channels>>16) & 0xFFFF;
  maskValue=DataTakingTools::computeSlaveBusyMask(slaveChannels,1);
  IblRod::SlaveB::BusyMaskReg::write(maskValue);
  IblRod::SlaveB::BusyForceReg::write(forceValue);
  std::cout<<"\n\t Set slave B busy mask to "<<HEX(maskValue)<<" and force to "<<HEX(forceValue)<<std::endl<<std::endl;
  std::cout<<"Reading back: " << HEX( IblRod::SlaveA::BusyMaskReg::read() ) << " and " << HEX( IblRod::SlaveA::BusyForceReg::read() ) << std::endl;

  maskValue=DataTakingTools::computeMasterBusyMask(channels);
  IblRod::MasterBusyMask::write(maskValue);
  //IblRod::MasterBusyMask::write(forceValue);
  std::cout<<"\n\t set busy mask for the Master FPGA to "<<HEX(maskValue)<<std::endl<<std::endl;
#endif
}

uint32_t DataTakingTools::computeMasterBusyMask(uint32_t channels) {
  uint32_t masterBusyMask = 0x0;
#ifdef __XMK__
  if ((channels & 0x0000FFFF) == 0) {
    masterBusyMask |= 0x2; //masking Slave0 busy input in the Master busy logic
    std::cout<<"\n\t Masking the Slave North busy input into the Master FPGA since all the relative channels are disabled; masterBusyMask = 0x"<<HEX(masterBusyMask)<<std::endl<<std::endl;
  }
  if ((channels & 0xFFFF0000) == 0) {
    masterBusyMask |= 0x4; //masking Slave1 busy input in the Master busy logic
    std::cout<<"\n\t Maasking the Slave South busy input into the Master FPGA since all the relative channels are disabled; masterBusyMask = 0x"<<HEX(masterBusyMask)<<std::endl<<std::endl;
  }
#endif
  return masterBusyMask;
}

uint32_t DataTakingTools::computeSlaveBusyMask(uint16_t slaveChannels, uint8_t slaveId) {
  uint32_t slaveBusyMask = 0x0;
#ifdef __XMK__

// Added by MB; this function computes the Slave Busy Mask based on the link enable channels for that particular Slave; 
// it should be used also internally from the disableLink function (getting as input the updated mask of the still enable channels) after disabling a module.
// This function should update corresponding slave busy mask amd also the master if necessary (only if the entire slave is disabled).

  uint32_t speed=0;
  uint16_t slaveEnableMask=0;
  uint16_t fmtBusyMask   = 0x0;
  uint16_t  efbBusyMask   = 0x0;
  uint16_t routerslinkBusyMask = 0x0;

  if (slaveId==0) speed=(IblRod::SlaveA::FmtFei3RegS1::read());
  else speed=IblRod::SlaveB::FmtFei3RegS1::read();

  if (speed == 0x42){
    //160 Mb/s readout mode..some ginnastic is needed in the FMT link enable    
    for(uint32_t j=0; j<8; j++){
      slaveEnableMask += (((slaveChannels >> (8 + j))&0x1) << 4*(j/2)+(j%2));
    }
    std::cout<<"Computed new FMT Enable mask at 160 Mb/s: "<<HEX(slaveEnableMask)<<" starting from SlaveEnableChannels = "<<HEX(slaveChannels) <<" for SlaveId= "<<(int)slaveId<<std::endl;
  }else{slaveEnableMask=slaveChannels;}

  // Start setting the busy mask for the FMT links that are not enabled..this is ok for every case, IBL or Pixel 40/80/160
  fmtBusyMask = ~slaveEnableMask;
  slaveBusyMask |= fmtBusyMask<<13;
  std::cout << "Slave Busy Mask after setting just the FTM busy mask looks like this: "<<HEX(slaveBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;
  
  // Moving to set the busy mask for the EFB that are not enable; special treatment required here for Pixel at 160 Mb/s
  
  if( RodResMgr::isFei4() || speed!=0x42 ) {//IBL and Pixel at 40 or 80 Mb/s
    // Update the mask with a proper masking of the EFB bits when the 4 formatter links are disabled inside a formatter
    for(uint8_t i = 0 ; i < 4 ; ++i) {
      if( ( (fmtBusyMask>>(i*4)) & 0xF) == 0xF) efbBusyMask |= (0x1 << i);
    }
    slaveBusyMask |= (efbBusyMask << 5);
    std::cout << "Slave Busy Mask after setting also the EFB busy mask looks like this (IBL/Pixel at 40-80 Mb/s case): " <<HEX(slaveBusyMask)<<" for SlaveId= "<< (int)slaveId <<std::endl;

  } else if ( (!RodResMgr::isFei4()) && (speed==0x42) ) {//Pixel 160 Mb/s
    // Update the mask with a proper masking of the EFB bits when the 2 formatter links are disabled inside a formatter
    for(uint8_t i = 0 ; i < 4 ; ++i) {
      if( ( (fmtBusyMask>>(i*4)) & 0x3) == 0x3) efbBusyMask |= (0x1 << i);
    }
    slaveBusyMask |= (efbBusyMask << 5);
    std::cout << "Slave Busy Mask after setting also the EFB busy mask looks like this (Pixel 160 Mb/s case): "<<HEX(slaveBusyMask)<<" for SlaveId= "<< (int)slaveId <<std::endl;  
  }
  std::cout << "EFB Busy Mask looks like: "<<HEX(efbBusyMask)<<" for SlaveId= "<<(int)slaveId <<std::endl;  

  // Masking the histogrammers always during data taking 
  routerslinkBusyMask |= 0xc00;//masking histo 0 and histo1
  std::cout << "HistoRouterSlink Busy Mask after setting the histo looks like: " <<HEX(routerslinkBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;  

  // Masking the router first
  if ((efbBusyMask & 0x3) == 0x3) routerslinkBusyMask |= (0x1<<8); //masking router 0
  std::cout << "HistoRouterSlink Busy Mask after setting also the Router0 looks like : "<<HEX(routerslinkBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;  
  if ((efbBusyMask & 0xC) == 0xC) routerslinkBusyMask |= (0x1<<9); //masking router 1
  std::cout << "HistoRouterSlink Busy Mask after setting also the Router1 looks like : " <<HEX(routerslinkBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;  

  // Masking the corresponding Slink  
  if(RodResMgr::isFei4()){//IBL..very linear
    if ( ( (efbBusyMask & 0x3) == 0x3) || ((efbBusyMask & 0xC) == 0xC) ) routerslinkBusyMask |= efbBusyMask; //maskink the corresponding Slink Xoff and Ldown
    std::cout << "IBL HistoRouterSlinkBusy Mask after setting the proper Slink busy mask : " <<HEX(routerslinkBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;  
  }else{//Pixel
    routerslinkBusyMask |= 0xC; //masking always Slink1 Xoff and Ldown since are not used in Pixel
    std::cout << "Pixel HistoRouterSlinkBusy Mask after setting the Slink1: "<<HEX(routerslinkBusyMask)<<" for SlaveId= "<<(int)slaveId<<std::endl;  
    if ((efbBusyMask & 0xF) == 0xF) routerslinkBusyMask |= 0x3; //maskink Slink0 Xoff and Slink0 Ldown
  }

  slaveBusyMask |= (routerslinkBusyMask<<1);
  std::cout << "Slave Busy Mask after setting also the Router and Slink busy mask looks like: "<<HEX(slaveBusyMask)<<" for SlaveId= "<<(int)slaveId <<std::endl;  
  
  if ( (RodResMgr::isFei4()) && ((routerslinkBusyMask & 0x300)!=0) ) { 
    //We've disabled a Router --> an SLink in IBL, so we need to inform that it should be disabled in ATLAS or the DAQSlice
    for(int i = 0 ; i < 2 ; ++i){
      if( ( (routerslinkBusyMask>>(2*i)) & 0x3) == 0x3 ){
	  std::cerr << "INFO: Please run the following command ${PIX_LIB}/Examples/sendDisableMessage ATLAS ROD_I1_S?_" << i << " Appl_PixelRCD-IBL ROD" << std::endl;
      }
    }
  }	
  std::cout << "Final Computed SlaveBusyMask: " <<HEX(slaveBusyMask)<<" for channels "<<HEX(slaveChannels)<<" for SlaveId= "<<(int)slaveId<<std::endl;
#endif
  return slaveBusyMask;
}

uint32_t DataTakingTools::readECRCounter() {
return IblRod::Master::EcrCntInReg::read();
}

void DataTakingTools::setBcidOffset(uint32_t value) {
#ifdef __XMK__
  value = 0x000003FF & value;
  std::cout<<"setting bcidOffset to: 0x"<<std::hex<<(int)value<<std::dec<<std::endl;
  uint32_t currentValue = IblRod::SlaveA::Efb1CtrlReg::read();
  IblRod::SlaveA::Efb1CtrlReg::write((currentValue & 0x0c00FFFF) | (value<<16) | (1<<13) ); // 13th bit includes BCID rollover
  currentValue = IblRod::SlaveA::Efb2CtrlReg::read();
  IblRod::SlaveA::Efb2CtrlReg::write((currentValue & 0x0c00FFFF) | (value<<16) | (1<<13) ) ;

  currentValue = IblRod::SlaveB::Efb1CtrlReg::read();
  IblRod::SlaveB::Efb1CtrlReg::write((currentValue & 0x0c00FFFF) | (value<<16) | (1<<13) );
  currentValue = IblRod::SlaveB::Efb2CtrlReg::read();
  IblRod::SlaveB::Efb2CtrlReg::write((currentValue & 0x0c00FFFF) | (value<<16) | (1<<13) );

#endif
}

void DataTakingTools::sLinkEnable(SLinkEnableMode mode) {
#ifdef __XMK__
  for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) 
    for(uint8_t linkId = 0 ; linkId < 2 ; ++linkId) 
      IblBoc::sLinkEnable(slaveId, linkId, mode);
#endif
}

//Set BOC Emulator
void DataTakingTools::setBocEmu(bool mode, uint32_t channels, uint8_t frameCount, IblBoc::PixSpeed speed, uint32_t nHits, EmuUtils::OCC_MODE occMode) {
#ifdef __XMK__
RodResourceManagerIBL::setBocEmu(mode);

std::vector <uint8_t> lut;
std::vector <uint8_t> lutReadBack;

  if(mode && (IblBoc::isNewEmulator(0) || IblBoc::isNewEmulator(1) ) ){
    EmuUtils::getLUTFromMode(occMode,lut,nHits, sqrt(nHits));
    //std::cout<<"LUT "<<std::endl;
      //for(size_t s =0;s<lut.size();s++)std::cout<<s<<" "<<(int)lut[s]<<std::endl;
  }

  if( RodResMgr::isFei4() ) {
    std::cout<<"INFO: "<< (mode ? "setting IBL boc FE emulator ON" : "setting IBL boc FE emulator OFF") <<std::endl;
    for(int ch=0;ch<32;ch++){
        if (mode && (channels & (1<<ch))) {
          IblBoc::IBLFeEmu::init(ch/16,ch%16); // init emulator
            if(!IblBoc::isNewEmulator(ch/16)){ //BOC FW with old emulator
             IblBoc::IBLFeEmu::setHitCount(ch/16,ch%16,nHits);//set number of Hits 
           } else{
             IblBoc::IBLFeEmu::SetTotValue(ch/16,ch%16,8);
             IblBoc::IBLFeEmu::SetTot2Value(ch/16,ch%16,8);
             IblBoc::IBLFeEmu::SetExtraFrames(ch/16,ch%16,0);
             IblBoc::IBLFeEmu::WriteLUT(ch/16,ch%16,lut);
           }

        } else {
          IblBoc::IBLFeEmu::reset(ch/16,ch%16); // reset emulator to off
        }
      }
  } else {//FE-I3
      std::cout<<"INFO: "<< (mode ? "setting PIX boc FE emulator ON" : "setting PIX boc FE emulator OFF") <<std::endl;	//GDamen: added support for PIX FE
      //set the BOC emulator registers according to the speed
      uint8_t speedFeEmuConf = 0x00;

        switch(speed) {
    	  case IblBoc::Speed_40:
      	    speedFeEmuConf = 0x00;
            break;
          case IblBoc::Speed_80:
      	    speedFeEmuConf = 0x02;
      	    break;
     	  case IblBoc::Speed_160:
     	    speedFeEmuConf = 0x04; // bit 0/1 drive the read out speed; bit 6/7 drive the inversion of the 4 lines in
            break;
       }

      for(int ch=0;ch<32;ch++){
        if (mode && (channels & (1<<ch))) {
            IblBoc::PixFeEmu::init(ch/16,ch%16, frameCount, speedFeEmuConf); // init emulator // TODO: revisit
            
            if(!IblBoc::isNewEmulator(ch/16)){ //BOC FW with old emulator
             IblBoc::PixFeEmu::setHitCount(ch/16,ch%16,nHits);//set number of Hits
           } else{
             IblBoc::PixFeEmu::WriteLUT(ch/16,ch%16,lut);
             //IblBoc::PixFeEmu::ReadLUT(ch/16,ch%16,lutReadBack);
             //for(int i=0;i<lut.size();i++)std::cout<<ch<<" "<<(int)lut[i]<<" "<<(int)lutReadBack[i]<<std::endl;
           }
        } else {
          IblBoc::PixFeEmu::reset(ch/16,ch%16); // reset emulator to off
        }
      }
    }

#endif
}

//Set ROD Emulator
void DataTakingTools::setRodEmu(bool mode, uint8_t frameCount, IblBoc::PixSpeed speed) {
#ifdef __XMK__
//RodResourceManagerIBL::setBocEmu(mode);		//to be checked
  if( RodResMgr::isFei4() ) {
    std::cout<<"ERROR: "<< "IBL does not support ROD Emulator. Please use BOC Emulator or ROL Emulator" <<std::endl;
  } else {
    std::cout<<"INFO: "<< (mode ? "setting PIX ROD FE emulator ON" : "setting PIX ROD FE emulator OFF") <<std::endl;
    //set the BOC emulator registers according to the speed
    uint8_t speedFeEmuConf = 0x00;

    switch(speed) {
    	case IblBoc::Speed_40:
      	speedFeEmuConf = 0x00;
        break;
      case IblBoc::Speed_80:
      	speedFeEmuConf = 0x02;
      	break;
     	case IblBoc::Speed_160:
     		speedFeEmuConf = 0x04; // bit 0/1 drive the read out speed; bit 6/7 drive the inversion of the 4 lines in
        break;
    }
    
    RodResourceManagerIBL::setBocEmu(mode);
  	for (int slaveId = 0; slaveId < 2; slaveId++) {
  	
  		if (mode) {
    		IblRodSlave::MCCEmu::init(slaveId, frameCount, speedFeEmuConf); // init emulator // TODO: revisit
        for(int ch = 0; ch < 16; ch++) {
        	IblBoc::set(slaveId, ch, IblBoc::TxControl2, 0x1); // mute Tx to real FE
        }
      } else {
	     	IblRodSlave::MCCEmu::reset(slaveId); // reset emulator to off
	     	for(int ch = 0; ch < 16; ch++) {
        	IblBoc::reset(slaveId, ch, IblBoc::TxControl2, 0x1); // mute Tx to real FE
        	}
        } 
     }
  }
#endif
}


void DataTakingTools::disableSerialLine() {
#ifdef __XMK__
  SerialPort* sPort = RodResourceManagerIBL::getSerialPort();
  sPort->setTxMask(0x0);
#endif
}

void DataTakingTools::setAmp2VbnStandby(uint32_t channels, uint8_t value) {
#ifdef __XMK__
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++)
    if (channels & (1<<i))
      RodResourceManagerIBL::getCalibModuleConfig(i).setAmp2VbnStandby(value);
#endif
}

void DataTakingTools::setPrmpVbpStandby(uint32_t channels, uint8_t value) {
#ifdef __XMK__
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++)
    if (channels & (1<<i))
      RodResourceManagerIBL::getCalibModuleConfig(i).setPrmpVbpStandby(value);
#endif
}

void DataTakingTools::setStandby3DBM(uint32_t channels, uint8_t value) {
#ifdef __XMK__
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++)
    if (channels & (1<<i))
      RodResourceManagerIBL::getCalibModuleConfig(i).setStandby3DBM(value);
#endif
}

void DataTakingTools::setDisVbnStandby(uint32_t channels, uint8_t value) {
#ifdef __XMK__
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++)
    if (channels & (1<<i))
      RodResourceManagerIBL::getCalibModuleConfig(i).setDisVbnStandby(value);
#endif
}

void DataTakingTools::bocResampleRxPhase(uint32_t channels) {
#ifdef __XMK__
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->resamplePhase(channels);
#endif
}

void DataTakingTools::sendConfigAtECR(uint32_t channels, uint32_t value){
#ifdef __XMK__
 if(!RodResMgr::isFei4())return;
 //TO BE IMPLEMENTED

#endif
}

void DataTakingTools::bocManualConfig(uint32_t channels){
#ifdef __XMK__
 if(!RodResMgr::isFei4())return;
 //TO BE IMPLEMENTED
#endif
}

void DataTakingTools::resetModules(uint32_t channels, uint32_t value){
#ifdef __XMK__
if( !(RodResMgr::isFei3()) )return; 

Fei3ModProxy Fei3;
Fei3.setFeMask(channels);

std::cout<<"Resetting modules: Module mask 0x"<<std::hex<<channels<<std::dec<<std::endl;

Fei3.resetModules(value);
#endif
}

void DataTakingTools::setRODVetoTimeAtECR(uint32_t value){

#ifdef __XMK__

IblRod::Master::VetoAfterEcrReg::write(value);


#endif
}

void DataTakingTools::setRODBusyTimeAtECR(uint32_t value){
#ifdef __XMK__

IblRod::Master::BusytimeAfterEcrReg::write(value);

#endif
}

void DataTakingTools::setRODResetTimeAfterECR(uint32_t value){
#ifdef __XMK__

IblRod::Master::ResetAfterEcrReg::write(value);

#endif
}

void DataTakingTools::setFmtDataOverflowLimit(uint32_t value){
#ifdef __XMK__

IblRod::SlaveA::FmtDataOverflowLimit::write(value);
IblRod::SlaveB::FmtDataOverflowLimit::write(value);

#endif
}

void DataTakingTools::setFmtTrailerTimeoutLimit(uint32_t value){
#ifdef __XMK__

IblRod::SlaveA::FmtTrailerTimeoutLimit::write(value);
IblRod::SlaveB::FmtTrailerTimeoutLimit::write(value);

#endif
}
