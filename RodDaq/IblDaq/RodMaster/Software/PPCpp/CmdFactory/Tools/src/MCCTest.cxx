/*
 * Author: L. Kaplan <lkaplan@cern.ch>
 *         K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2015-XI-23
 * Description: this command can be used to test and validate MCC functionality
 */

//  Developement Note:  Currently this is only set up for a single Rx channel, i.e. 19, in spite of
//                      the feeble attempt to use a Channel mask by passing it in as a parameter.
//                      Much work is still needed to fully make use of this channel mask, if ever needed.
//                      The code remains obviously a work-in-progress in that regard, and whoever
//                      takes on the excercise of generalizing the use of Rx chanels will want to
//                      eliminate lots of useless redundancy left over from the initial developement.

#include "MCCTest.h"
#include <unistd.h>
#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "Serializer.h"
#endif


#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

MCCTest::MCCTest() {
  setResultPtr();
  m_modMask = 0xFFFFFFFF;
  result.data.clear();
}

void MCCTest::initializeMCC() {

#ifdef __XMK__
  // Seeting MCC readout speed and corresponding BOC registers
  // Karolos: right now the BOC and MCC have different values for the readback setting
  IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;
  uint8_t mccRxSpeed = rxSpeed + (rxSpeed ? 1 : 0);
  IblBoc::PixSetSpeed( rxSpeed );
  std::cout << "MCC speed: " << HEX(mccRxSpeed) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

  uint8_t rxCh = 19;

  std::cout << "m_modMask=" << HEX( m_modMask ) << std::endl;
  if(__builtin_popcount(m_modMask) == 1) {
    std::cout << "m_modMask=" << HEX( m_modMask ) << std::endl;
    rxCh = 0;
    while( rxCh < 32 ) {
      if( (1 << (rxCh)) & m_modMask )
        break;
      rxCh++;
    }
  }

  rxCh = 19; //  The above makes no sense since 19 is hardcoded elsewhere.

  std::cout << "RX channel = " << static_cast<int>(rxCh) << std::endl;

  uint8_t sPoint, dPoint;
  uint8_t fibreNr = IblBoc::getMasterFibreFromRxCh(rxCh);
  IblBoc::GetRxSampleDelay( fibreNr, sPoint, dPoint );
  std::cout << "Sampling point: " << static_cast<int>(sPoint) << std::endl;
  IblBoc::SetRxSampleDelay( fibreNr, sPoint, 0);

  // Multiplexing ...

  for (int i = 0 ; i < 32; i++) {
    IblBoc::write(i/16, i%16, IblBoc::FibreMaster, fibreNr%24);
    IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x01);
  }
#endif

}

void MCCTest::execute() {
  std::cout << "in function: " << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__

  result.nMod = 0;

//  IblBocLink *iblBocLink = RodResourceManagerPIX::getIblBocLink();
//  iblBocLink->enableChannels(m_modMask, false);

  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

  IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;
  uint8_t mccRxSpeed = rxSpeed + (rxSpeed ? 1 : 0);
  IblBoc::PixSetSpeed( rxSpeed );
  std::cout << "MCC speed: " << HEX(mccRxSpeed) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

  uint8_t rxCh = 19;

  SerialPort* sPort = RodResourceManagerPIX::getSerialPort();
  sPort->setTxMask(1<<rxCh);
  sPort->setTxMask(0xFFFFFFFF);
  fes[0].setTxLink(sPort);

  this->initializeMCC();

  rxCh = 19; //  Because the way it is, initializeMCC() has the ptoential of messing up the hard-coded value (it's ALL ugly, I know !!!)

  // Drain BOC RX FIFO
  IblBocRxFifo rxFifo;
  rxFifo.setRxCh(rxCh);
  // Drain RX FIFO
  // Todo: use IblBocRxFifo::reset
  // Todo: check why while loop doesn't work while fixed size read is OK
  //while( IblBoc::read(rxCh/16, rxCh%16, IblBoc::RxStatus) & 0x20 != 0x20 )
  for(int k = 0 ; k < 4096 ; ++k)
    IblBoc::read( rxCh/16, rxCh%16, IblBoc::DataLow );

  std::cout << "Sending test pattern" << std::endl;
  result.nMod++;
  for (int i = 0 ; i < 32; i++) {
    if(i != rxCh) continue;

  // Enabling the RX and TX channels
	IblBoc::write(i/16, i%16, IblBoc::TxControl, 0x01);
	IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x0F);

  fes[0].setTxCh(i);
  fes[0].setRxCh(i);
  fes[0].mccGlobalResetMCC();
  fes[0].writeMCCRegister(CSR, 0x0040 | mccRxSpeed ); // write to CSR to enable play back
  fes[0].writeMCCRegister(CNT, 0x0004); // write to CNT to set length of data

#if 0
  //uint32_t data[2];
  // data hit = 10000110111111011111111111 = 0x21bf7ff
  // EoE      = 1000011110000              = 0x10f0
  // in 32 bit words: 0x086fdffe 0x1e200000
  //data[0] = 0x086fdffe;
  //data[1] = 0x1e200000;

  // Karolos: low-level code to debug functionality
  fes[0].getTxLink()->write(0x002D6C12);
  fes[0].getTxLink()->write(0xDFBFF800);
  fes[0].getTxLink()->write(0x2D6C12F0);
  fes[0].getTxLink()->write(0x00016B80);
  fes[0].getTxLink()->write(0x0001D000);
  fes[0].getTxLink()->send();
#endif

  uint32_t cnt = 1;

  uint32_t data1[1]; data1[0] = 0x86fdffc0;
  uint32_t data2[1]; data2[0] = 0x832152C0;
  uint32_t data3[1]; data3[0] = 0x87800000;

  uint16_t mccFeMask = 0x0001;

  uint8_t bcid = 0;

  for(size_t row = 0 ; row < 1 ; row += 2 ) {
    for(size_t col = 0 ; col < 18 ; ++col) {

  std::cout << "Sending " << m_nTrig << " triggers" << std::endl;
  std::cout << "ROW=" << row << ", COL=" << col << std::endl;

  do {

  //fes[0].writeMCCRegister(FEEN, 0x0001); // write to FEEN to enable front end 1
  //fes[0].mccWrReceiver(data1); // write data pattern to receiver

#if 0
  fes[0].mccWrReceiver(data2); // write data pattern to receiver
  fes[0].mccWrReceiver(data3); // write EoE pattern to receiver
#else
  for(size_t p = 0 ; p < 2 ; ++p)
    fes[0].addHit(mccFeMask, bcid, row, col+p, (row*10+col) & 0xFF);
  fes[0].addEOE(mccFeMask, bcid);
#endif

#if 0

#if 0
  fes[0].writeMCCRegister(FEEN, 0x0700);
  fes[0].mccWrReceiver(data2); // write data pattern to receiver
  fes[0].mccWrReceiver(data3); // write EoE pattern to receiver
#else
  fes[0].writeMCCRegister(FEEN, 0x0700);
  fes[0].addHit(0x0700, 0, row, col, row*10+col);
  fes[0].addEOE(0x0700, 0);
#endif

#endif

#if 0
  fes[0].writeMCCRegister(FEEN, 0x0700);
  fes[0].addHit(0x0700, 0, 100, 6, 75);
  fes[0].addEOE(0x0700, 0);
#endif

  fes[0].writeMCCRegister(FEEN, mccFeMask);

#if 0
  // addHit(uint16_t fe_mask, uint8_t bcid, uint8_t row, uint8_t col, uint8_t tot)
  fes[0].addHit(0x0001, 0, 120, 6, 72);
  fes[0].addEOE(0x0001, 0);

  fes[0].addHit(0x0100, 0, 100, 5, 75);
  fes[0].addEOE(0x0100, 0);

#endif

  fes[0].mccEnDataTake(); // enable data taking

#if 0
  for(int j = 19 ; j < 20 ; j++) {
    rxFifo.setRxCh(j);
    std::cout << j;
    for(size_t k = 0 ; k < 10 ; ++k )
      //std::cout << "\t" << HEX( IblBoc::read(j/16, j%16, IblBoc::DataLow) & 0xFF );
      std::cout << "\t" << HEX( rxFifo.read() );
    std::cout << std::endl;
  }
#endif

#if TEST_MCC_REGISTER_READ_BACK
  for( size_t k = 0 ; k < 10 ; ++k ) {
    fes[0].mccRdRegister(0x0);
    for(size_t k = 0 ; k < 10 ; ++k )
      std::cout << "\t" << HEX( rxFifo.read() );
    std::cout << std::endl;
  }
#endif

  usleep(100000);

  if(!m_nTrig) continue;
  fes[0].sendLV1(); // trigger and start event builder

  std::cout << cnt << " --- ";
#if 0
  for(size_t k = 0 ; k < 10 ; ++k )
    std::cout << "\t" << HEX( rxFifo.read() );
  std::cout << std::endl;
#else
  std::vector<Fei3HitData> hd = fes[0].readHitData();
  Fei3ModData::decodeRawHitData( hd );
#endif
  } while (cnt++ < m_nTrig);

  usleep(100000);

    } // col
  } // row

  } // i as RX channel

  for (int i = 0 ; i < 32; i++) {
    IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x00);
  }

  result.data.push_back(0);

#endif

}

void MCCTest::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_modMask);
  Serializer<uint32_t>::serialize(out, offset, m_nTrig);
}

void MCCTest::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_modMask = Serializer<uint32_t>::deserialize(in, offset);
  m_nTrig = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t MCCTest::serializedSize() const {
  return (sizeof(uint32_t)*2);
}

void MCCTestValue::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint8_t>::serialize(out, offset, nMod);
  for (int i = 0; i < nMod; i++) Serializer<uint8_t>::serialize(out, offset, data[i]);
}

void MCCTestValue::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  nMod = Serializer<uint8_t>::deserialize(in, offset);
  data.resize(nMod);
  for (int i = 0; i < nMod; i++) {
    uint8_t reg;
    Serializer<uint8_t>::deserialize(in, offset, reg);
    data[i] = reg;
  }
}

uint32_t MCCTestValue::serializedSize() const {
  return (sizeof(uint8_t) + nMod * sizeof(uint8_t));
}
