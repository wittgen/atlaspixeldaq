#include "IblDataTakingWithEmulator.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#endif

void  IblDataTakingWithEmulator::execute() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
	static bool dataTakingEnabled = false;

	if(!dataTakingEnabled) {
		std::cout << "Enabling data-taking with emulator" << std::endl;

		// Move to initialization
		IblRod::Master::RrifCtrlReg::write(0x80000000);
		std::cout << "RrifCtrlReg: " << HEX( IblRod::Master::RrifCtrlReg::read() ) << std::endl;

		IblBoc::dumpFwInfo();

		std::cout << "SlaveA setting FmtLinkEnable to 0" << std::endl;
		IblRod::SlaveA::FmtLinkEnable::write(0x00000000);
		std::cout << "SlaveB setting FmtLinkEnable to 0" << std::endl;
		IblRod::SlaveB::FmtLinkEnable::write(0x00000000);

		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::SlaveId::addr() ) << ": " << HEX( IblRod::SlaveA::SlaveId::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::CalibrationMode::addr() ) << ": " << HEX( IblRod::SlaveA::CalibrationMode::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::FmtReadoutTimeoutLimit::addr() ) << ": " << HEX( IblRod::SlaveA::FmtReadoutTimeoutLimit::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::FmtDataOverflowLimit::addr() ) << ": " << HEX( IblRod::SlaveA::FmtDataOverflowLimit::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::FmtHeaderTrailerLimit::addr() ) << ": " << HEX( IblRod::SlaveA::FmtHeaderTrailerLimit::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::FmtRodBusyLimit::addr() ) << ": " << HEX( IblRod::SlaveA::FmtRodBusyLimit::read() ) << std::endl;
		std::cout << "SlaveA register @ " << HEX( IblRod::SlaveA::FmtCtrlReg::addr() ) << ": " << HEX( IblRod::SlaveA::FmtCtrlReg::read() ) << std::endl;

		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::SlaveId::addr() ) << ": " << HEX( IblRod::SlaveB::SlaveId::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::CalibrationMode::addr() ) << ": " << HEX( IblRod::SlaveB::CalibrationMode::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::FmtReadoutTimeoutLimit::addr() ) << ": " << HEX( IblRod::SlaveB::FmtReadoutTimeoutLimit::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::FmtDataOverflowLimit::addr() ) << ": " << HEX( IblRod::SlaveB::FmtDataOverflowLimit::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::FmtHeaderTrailerLimit::addr() ) << ": " << HEX( IblRod::SlaveB::FmtHeaderTrailerLimit::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::FmtRodBusyLimit::addr() ) << ": " << HEX( IblRod::SlaveB::FmtRodBusyLimit::read() ) << std::endl;
		std::cout << "SlaveB register @ " << HEX( IblRod::SlaveB::FmtCtrlReg::addr() ) << ": " << HEX( IblRod::SlaveB::FmtCtrlReg::read() ) << std::endl;

		int hitsPerTrigger = 8; // Hard-coded!!!

		for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
			std::cout << "Configuring BMF FE emulator for Slave " << (int)slaveId << std::endl;
			for(uint8_t ch = 0 ; ch < 16 ; ++ch) {
				std::cout << "--- Preparing ch " << (int)ch << std::endl;
				// Enabling channel
				IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x23);
				IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x1);

				// Initializing FE emulator, keeping whatever RxControl(0:3) were set before
				IblBoc::IBLFeEmu::init(slaveId,ch);

				// Setting hit count per trigger
				IblBoc::IBLFeEmu::setHitCount(slaveId, ch, hitsPerTrigger);
			}
			std::cout << "Done!" << std::endl;
		}


		SerialPort* sPort = RodResourceManagerIBL::getSerialPort();
		sPort->setTxMask(0xFFFFFFFF);
		Fei4 fe; fe.setTxLink(sPort); fe.setChipID(8);
		fe.setRunMode(true);

		RodMaster::setFeCmdMask(0, 0x00FF);
		RodMaster::setFeCmdMask(1, 0x00FF);

//		IblRod::Master::DebugModeCnfgReg::write( 0x42000000 ); // ROL test mode active
		IblRod::Master::DebugModeCnfgReg::write( 0x02000000 ); // ROL test mode NOT active
		std::cout << "DebugModeCnfgReg @ " << HEX( IblRod::Master::DebugModeCnfgReg::addr() ) << ": " << HEX( IblRod::Master::DebugModeCnfgReg::read() ) << std::endl;

// Todo: move appropriately
#define RRIF_CTL_TIM_TRIG  0x88001527
#define RRIF_CTL_PPC_TRIG 0x80041425

//		IblRod::Master::RrifCtrlReg::write( RRIF_CTL_PPC_TRIG );
		IblRod::Master::RrifCtrlReg::write( RRIF_CTL_TIM_TRIG );
		std::cout << "RrifCtrlReg @ " << HEX( IblRod::Master::RrifCtrlReg::addr() ) << ": " << HEX( IblRod::Master::RrifCtrlReg::read() ) << std::endl;

		IblRod::Master::SprCtrlReg::write( 0x000008FF );
		std::cout << "SprCtrlReg @ " << HEX( IblRod::Master::SprCtrlReg::addr() ) << ": " << HEX( IblRod::Master::SprCtrlReg::read() ) << std::endl;

		


		IblRod::SlaveA::SlaveId::write(0x0);
		std::cout << "SlaveA::SlaveId register @ " << HEX( IblRod::SlaveA::SlaveId::addr() ) << ": " << HEX( IblRod::SlaveA::SlaveId::read() ) << std::endl;
		IblRod::SlaveB::SlaveId::write(0x1);
		std::cout << "SlaveB::SlaveId register @ " << HEX( IblRod::SlaveB::SlaveId::addr() ) << ": " << HEX( IblRod::SlaveB::SlaveId::read() ) << std::endl;






		IblRod::SlaveA::FmtLinkEnable::write(0x0000FFFF);
		std::cout << "SlaveA::FmtLinkEnable register @ " << HEX( IblRod::SlaveA::FmtLinkEnable::addr() ) << ": " << HEX( IblRod::SlaveA::FmtLinkEnable::read() ) << std::endl;
		IblRod::SlaveB::FmtLinkEnable::write(0x0000FFFF);
		std::cout << "SlaveB::FmtLinkEnable register @ " << HEX( IblRod::SlaveB::FmtLinkEnable::addr() ) << ": " << HEX( IblRod::SlaveB::FmtLinkEnable::read() ) << std::endl;

		for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) 
			for(uint8_t linkId = 0 ; linkId < 2 ; ++linkId) 
				IblBoc::sLinkEnable(slaveId, linkId);

		dataTakingEnabled = true;
	} else {

		std::cout << "Data-taking already running. Dumping status information!" << std::endl;
		std::cout << "RrifCtrlReg @ " << HEX( IblRod::Master::RrifCtrlReg::addr() ) << ": " << HEX( IblRod::Master::RrifCtrlReg::read() ) << std::endl;
		std::cout << "SprCtrlReg @ " << HEX( IblRod::Master::SprCtrlReg::addr() ) << ": " << HEX( IblRod::Master::SprCtrlReg::read() ) << std::endl;
		std::cout << "RrifSttsReg @ " << HEX( IblRod::Master::RrifSttsReg::addr() ) << ": " << HEX( IblRod::Master::RrifSttsReg::read() ) << std::endl;

		uint32_t calL1Id0 = IblRod::Master::CalL1Id0Reg::read() & 0x00FFFFFF;
		static uint32_t calL1IdCounter = 0;
		std::cout << "CalL1Id0Reg @ " << HEX( IblRod::Master::CalL1Id0Reg::addr() ) << ": " << HEX( calL1Id0 ) << std::endl;
		std::cout << "--- Delta: " << calL1Id0 - calL1IdCounter << std::endl;
		calL1IdCounter = calL1Id0;
		std::cout << "CalL1Id1Reg @ " << HEX( IblRod::Master::CalL1Id1Reg::addr() ) << ": " << HEX( IblRod::Master::CalL1Id1Reg::read() ) << std::endl;
		
		std::cout << "SlaveA::Efb1MiscStatusReg @ " << HEX( IblRod::SlaveA::Efb1MiscStatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Efb1MiscStatusReg::read() ) << std::endl;
		std::cout << "SlaveA::Efb2MiscStatusReg @ " << HEX( IblRod::SlaveA::Efb2MiscStatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Efb2MiscStatusReg::read() ) << std::endl;
		std::cout << "SlaveA::Fmt0StatusReg @ " << HEX( IblRod::SlaveA::Fmt0StatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt0StatusReg::read() ) << std::endl;
		std::cout << "SlaveA::Fmt1StatusReg @ " << HEX( IblRod::SlaveA::Fmt1StatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt1StatusReg::read() ) << std::endl;
		std::cout << "SlaveA::Fmt2StatusReg @ " << HEX( IblRod::SlaveA::Fmt2StatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt2StatusReg::read() ) << std::endl;
		std::cout << "SlaveA::Fmt3StatusReg @ " << HEX( IblRod::SlaveA::Fmt3StatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt3StatusReg::read() ) << std::endl;

		std::cout << "SlaveB::Efb1MiscStatusReg @ " << HEX( IblRod::SlaveB::Efb1MiscStatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Efb1MiscStatusReg::read() ) << std::endl;
		std::cout << "SlaveB::Efb2MiscStatusReg @ " << HEX( IblRod::SlaveB::Efb2MiscStatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Efb2MiscStatusReg::read() ) << std::endl;
		std::cout << "SlaveB::Fmt0StatusReg @ " << HEX( IblRod::SlaveB::Fmt0StatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt0StatusReg::read() ) << std::endl;
		std::cout << "SlaveB::Fmt1StatusReg @ " << HEX( IblRod::SlaveB::Fmt1StatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt1StatusReg::read() ) << std::endl;
		std::cout << "SlaveB::Fmt2StatusReg @ " << HEX( IblRod::SlaveB::Fmt2StatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt2StatusReg::read() ) << std::endl;
		std::cout << "SlaveB::Fmt3StatusReg @ " << HEX( IblRod::SlaveB::Fmt3StatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt3StatusReg::read() ) << std::endl;

	}

#endif
}
