#include "ReadConfig.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "Serializer.h"
#endif

ReadConfig::ReadConfig() { //todo parameter list
  setResultPtr(); 
  m_ConfigType = Physics;
  i_fe = 0x1;
  m_readFrontEnd = false;
  //  result.moduleConfig.clear();
}

void ReadConfig::execute() {
  bool verbose = true;//todo move to be an option
  if (verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<< " at line: " << __LINE__ << std::endl;

#ifdef __XMK__


  if (verbose){
    std::cout<<"config Type to read is: "<<(int)m_ConfigType<<std::endl;
    std::cout<<"FE mask to read is: 0x"<< std::hex << (int)i_fe << std::dec << std::endl;
    std::cout<<"read FE directly?: "<<(int)m_readFrontEnd<<std::endl;
  }

  Fei4* fe = (m_ConfigType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs()
	                             : RodResourceManagerIBL::getInstance().getCalibModuleConfigs();

  //read the configs!
  if(m_readFrontEnd==false){
    if(verbose)    std::cout << "reading FE number: " << i_fe << std::endl;
    result.moduleConfig = fe[i_fe]; 
    //    if(verbose)  result.moduleConfig.dump();
  }//todo reimplement reading from the fes directly
  else{
    std::cout << "We are reading directly from the FE" << std::endl;

    //this sends the proper sequence of commands to read back from the FE
    result.moduleConfig = Fei4::rdFullConfig(i_fe);
//     for (int l = 0; l < maxModules; l++) {
//       if (i_fe & (1<<l)) {
// 	result.nMod++;

// 	uint16_t translated_cfg[Fei4Cfg::numberOfRegisters];

// 	for(int i = 0; i < Fei4Cfg::numberOfRegisters ; i++){
// 	  if (verbose) std::cout << "Sending Cmd to rdRegister "  << i << std::endl;

// 	  fes[l].rdRegister(i);

// 	  std::vector<int> current_frame = fes[l].readFrame();
// 	  translated_cfg[i] = (0x100)*(current_frame[current_frame.size()-2]) + current_frame[current_frame.size() - 1];

// 	}

// 	if (verbose) std::cout<<"About to read config #: "<<int(l)<<std::endl;

// 	std::cout << "result.nMod is" << (int)result.nMod	<< std::endl;

// 	Fei4Cfg realFe;  //just a way to return the real Fei4Cfg	
// 	realFe.setCfg(translated_cfg);

// 	result.moduleConfig.push_back(realFe);
//       }//fe mask
//     }//module loop

//      if (verbose){
//        std::cout << "Going to dump the configs" << std::endl;
//        for(int j = 0; j < result.moduleConfig.size(); j++){
//  	result.moduleConfig[j].dump();
//        }
    
//        std::cout << "Just dumped the configs" << std::endl;
//      }
  }//read directly from front end else
#else
  //Todo: emulator
#endif

}

void ReadConfig::serialize(uint8_t *out) const
{
  uint32_t offset = 0;
  Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
  Serializer<bool>::serialize(out, offset, m_readFrontEnd);
  Serializer<uint32_t>::serialize(out, offset, i_fe);
}

void ReadConfig::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
  m_readFrontEnd = Serializer<bool>::deserialize(in, offset);
  i_fe = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t ReadConfig::serializedSize() const
{
  return (sizeof(ConfigType)+sizeof(bool)+sizeof(uint32_t) );
}

void ReadedConfig::serialize(uint8_t *out) const {
   uint32_t offset = 0;
   ///Serializer<uint8_t>::serialize(out, offset, moduleConfig.size());
   Serializer<Fei4Cfg>::serialize(out,offset,moduleConfig);
}
 
 void ReadedConfig::deserialize(const uint8_t *in) {
   uint32_t offset = 0;
   Fei4Cfg module;
   Serializer<Fei4Cfg>::deserialize(in,offset, module);
   moduleConfig = module;
}

uint32_t ReadedConfig::serializedSize() const {
  Fei4Cfg dummy;
  return (dummy.serializedSize());
}
