/*
 * Da Xu
 * Data: March-2019
 * Generic class to run an OptoScan in IBL
 */

#include <stdlib.h>
#include <bitset>
#include "IblOptoScan.h"
#include "RodResourceManager.h"
#include "IblConnectivity.h"

#include "IBLChannelMapping.h"

IblOptoScan::IblOptoScan() {

	setResultPtr();
	nTriggers =100;
	Channels = 0xFFFFFFFF;
}

void IblOptoScan::execute() {

#ifdef __XMK__ 
 if(!RodResMgr::isFei4()) return;
 
 uint32_t rxMask =Channels;
 IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink();
 uint32_t txMask =IblConnectivity::getTxMaskFromRxMask(rxMask);
 iblBocLink->enableTxChannels(txMask);


 // result.errmap = getOptoErrors(rxMask,nTriggers,Fei4);     
 getOptoErrors (rxMask, nTriggers, txMask, result.errmap);
#endif
}

#ifdef __XMK__

void IblOptoScan::getOptoErrors(uint32_t rxMask, uint32_t nTriggers,uint32_t txMask, std::map< uint8_t, std::vector< std::vector< uint32_t > > > & optoErrors )
{
  //  std::map< uint8_t, std::vector< std::vector< uint32_t > > >  optoErrors;
  for(int phase = 0; phase < 4; phase++)
    {
      for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh){
	if (rxMask & (1<<rxCh)){
	  int fibre = IblBoc::getFibreFromRxCh(rxCh);
	  IblBoc::CDRManPhase(fibre,phase);//set the phase, using the fibre as input

	  IblBoc::write(rxCh/16,rxCh%16,IblBoc::RxControl,0x0);//disable Rx channel
	  IblBoc::write(rxCh/16,rxCh%16,IblBoc::RxControl,0x7);//enable Rx channel in monitoring mode
	}
      }

   uint32_t txMask = IblConnectivity::getTxMaskFromRxMask(rxMask);

      IblBoc::ResetAllCounters();
      for (uint32_t iTrig = 0; iTrig < nTriggers; iTrig++) {
        for(uint8_t txCh = 0 ; txCh < 32 ; ++txCh){
          if (txMask & (1<<txCh)){
            //read back register 3 in broadcasting
            IBLChannelMapping::sendTx(txCh, 0x005a0603, 0x0);
            //read back register 4 in broadcasting
            IBLChannelMapping::sendTx(txCh, 0x005a0604, 0x0);
          }
        }
      }

      std::vector< uint32_t > Vphase;
      for(uint32_t rxCh = 0 ; rxCh < 32 ; ++rxCh){
	if (rxMask & (1<<rxCh)){
	  uint32_t nFrames = IblBoc::GetFrameCount( rxCh);
	  float ratio = nFrames/(2.*nTriggers);//compute the ratio between the received nframes and the expected nframes
	  uint32_t normFrame = (uint32_t) (ratio * 0xFFFF);
	  Vphase.push_back(normFrame);
	  Vphase.push_back(IblBoc::GetCounter( rxCh, IblBoc::DecoError));
	}
       optoErrors[rxCh].push_back(Vphase);
       Vphase.clear();
     }

   }
}
#endif

void IblOptoScan::serialize(uint8_t *out) const 
{
	uint32_t offset = 0;
	Serializer<int>::serialize(out, offset, nTriggers);
	Serializer<uint32_t>::serialize(out, offset, Channels);
	
}

void IblOptoScan::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<int>::deserialize(in, offset, nTriggers);
	Serializer<uint32_t>::deserialize(in, offset, Channels);

}

uint32_t IblOptoScan::serializedSize() const
{
  return (sizeof(int) + sizeof(uint32_t) );
}

void IblOptoScanResults::serialize(uint8_t *out) const
{
  
	uint32_t offset = 0;
	Serializer< std::map< uint8_t, std::vector< std::vector< uint32_t > > > >::serialize(out, offset, errmap);
}

void IblOptoScanResults::deserialize(const uint8_t *in)
{
  
	uint32_t offset = 0;
	errmap = Serializer< std::map< uint8_t, std::vector< std::vector< uint32_t > > > >::deserialize(in, offset);
}

uint32_t IblOptoScanResults::serializedSize() const
{
  size_t sizeMap = Serializer< std::map< uint8_t, std::vector< std::vector< uint32_t > > > >::size(errmap) ;
  return sizeMap;
}

