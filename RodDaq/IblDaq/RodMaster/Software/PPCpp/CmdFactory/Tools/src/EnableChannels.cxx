#include "EnableChannels.h"
#include "RodResourceManager.h"

EnableChannels::EnableChannels() {

	setResultPtr();
	result.enabledChannels = 0x0;
	Channels = 0x00010000;
}


void EnableChannels::execute() {

#ifdef __XMK__

// assume this is all done by calling IblBocLink::enableChannels(mask)
// however, should this be done in the command, or in the ppc main?

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  if( iblBocLink->enableChannels(Channels) ) result.enabledChannels = Channels;
  else result.enabledChannels = iblBocLink->getEnChMask();

 #else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
 
}

void EnableChannels::serialize(uint8_t *out) const
{
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,Channels);
}

void EnableChannels::deserialize(const uint8_t *in)
{
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
	uint32_t offset = 0;
	Channels = Serializer<uint32_t>::deserialize(in,offset);
}

uint32_t EnableChannels::serializedSize() const
{
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
 return (sizeof(uint32_t));
}

void Enabled::serialize(uint8_t *out) const {
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
 uint32_t offset = 0;
 Serializer<uint32_t>::serialize(out, offset, enabledChannels);
}

void Enabled::deserialize(const uint8_t *in) {
 uint32_t offset = 0;  
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
 enabledChannels = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t Enabled::serializedSize() const {
#ifdef LL_DEBUG
std::cout << "hey, I'm in function: "<<__PRETTY_FUNCTION__ << std::endl;
#endif
  return (sizeof(uint32_t));
}
