/*
 * M. Kretz <moritz.kretz@googlemail.com>
 * Date: 2015-May-13
 */

#include "GetVersionInfo.h"
#include "EnumUtils.h"
#include <iomanip>

GetVersionInfo::GetVersionInfo() {
#ifdef LL_DEBUG
    std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
    setResultPtr();
}

void GetVersionInfo::execute() {
#ifdef LL_DEBUG
    std::cout << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << m_ExecId << std::endl;
    std::cout << "Result address = " << &result << std::endl;
    std::cout << "Pointer to result: " << m_ResultPtr << std::endl;
#endif

#ifdef __XMK__
    gitHashToResult(IblRodVersionInfo::getMasterHashFw(), result.masterFw);
    gitHashToResult(IblRodVersionInfo::getMasterHashSw(), result.masterSw);
    gitHashToResult(IblRodVersionInfo::getSlaveHashFw(0), result.slaveAFw);
    gitHashToResult(IblRodVersionInfo::getSlaveHashSw(0), result.slaveASw);
    gitHashToResult(IblRodVersionInfo::getSlaveHashFw(1), result.slaveBFw);
    gitHashToResult(IblRodVersionInfo::getSlaveHashSw(1), result.slaveBSw);

#ifdef LL_DEBUG
    std::cout << "Git hashes of ROD software and firmware:" << std::endl;
    printHash(result.masterSw, "Master SW");
    printHash(result.masterFw, "Master FW");
    printHash(result.slaveASw, "SlaveA SW");
    printHash(result.slaveAFw, "SlaveA FW");
    printHash(result.slaveBSw, "SlaveB SW");
    printHash(result.slaveBFw, "SlaveB FW");
#endif

#endif
}

void GetVersionInfo::gitHashToResult(IblRodVersionInfo::GitHash_t const& hash, uint32_t *res) {
    memcpy(res, hash.word, IblRodVersionInfo::c_hashWords * 4);
}

void GetVersionInfo::printHash(uint32_t *hash, std::string desc) {
    std::ios::fmtflags f(std::cout.flags());
    std::cout << desc << ": " << std::hex << std::setw(8) << std::setfill('0');
    for (int i = 0; i < VersionInfoResult::hashWords; i++) {
        std::cout << std::hex << std::setw(8) << std::setfill('0') << hash[i];
    }
    std::cout << std::dec << std::endl;
    std::cout.flags(f);
}
