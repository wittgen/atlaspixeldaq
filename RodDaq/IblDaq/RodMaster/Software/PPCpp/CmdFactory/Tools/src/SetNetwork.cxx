/*
 * M. Kretz <moritz.kretz@cern.ch>
 * Date: 2014-November-13
 */

#include "SetNetwork.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#include <arpa/inet.h>
#endif

SetNetwork::SetNetwork(){ 
  setResultPtr(); 
}

void SetNetwork::execute() {
#ifdef __XMK__
  IblRodSlave slave;
  IblSlvNetCfg netCfgOut;

/* Use defaults if needed, specify local settings otherwise. 
 * We get the defaults in any case for the MAC address, and overwrite
 * the IP settings if wanted. */
  IblRodSlave::getDefaultNetConfig(slaveid, netCfgOut);
  if (usePpcDefaults == 0) {
      IblRodSlave::createIp4Address(ip_1, ip_2, ip_3, ip_4, &netCfgOut.localIp);
      IblRodSlave::createIp4Address(subnet_1, subnet_2, subnet_3, subnet_4, &netCfgOut.maskIp);
      IblRodSlave::createIp4Address(gw_1, gw_2, gw_3, gw_4, &netCfgOut.gwIp);
  }

/* Settings for FitFarm endpoints. */
  IblRodSlave::createIp4Address(ipA_1, ipA_2, ipA_3, ipA_4, &netCfgOut.targetIpA);
  netCfgOut.targetPortA = htonl(portA);

  IblRodSlave::createIp4Address(ipB_1, ipB_2, ipB_3, ipB_4, &netCfgOut.targetIpB);
  netCfgOut.targetPortB = htonl(portB);

  char buffer[100];
  snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", ipA_1, ipA_2, ipA_3, ipA_4);
  std::cout << buffer << "              Port: " << static_cast<int>(portA) << std::endl;
  snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", ipB_1, ipB_2, ipB_3, ipB_4);
  std::cout << buffer << "              Port: " << static_cast<int>(portB) << std::endl << std::endl;

  bool success = slave.setId((uint32_t)slaveid); //  Set Histogrammer Id in slave software (is this crirical or only for display?)
  std::cout<< (success ? "    set slave histogrammer Id " : " !! failed to set slave histogrammer Id ") << (uint32_t)slaveid << std::endl;

  success = slave.setNetCfg(&netCfgOut, slaveid);
  if (success)  {
      std::cout << "** Connected to the fit server ** " << std::endl;
  }
  else {
      std::cout << "***ERROR***" << std::endl;
      std::cout << "ERROR: NO CONNECTION TO FIT SERVER" << std::endl;
  }

#endif
}

void SetNetwork::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint8_t>::serialize(out,offset,slaveid);
	Serializer<uint8_t>::serialize(out,offset,usePpcDefaults);

	Serializer<uint8_t>::serialize(out,offset,ip_1);
	Serializer<uint8_t>::serialize(out,offset,ip_2);
	Serializer<uint8_t>::serialize(out,offset,ip_3);
	Serializer<uint8_t>::serialize(out,offset,ip_4);

	Serializer<uint8_t>::serialize(out,offset,subnet_1);
	Serializer<uint8_t>::serialize(out,offset,subnet_2);
	Serializer<uint8_t>::serialize(out,offset,subnet_3);
	Serializer<uint8_t>::serialize(out,offset,subnet_4);

	Serializer<uint8_t>::serialize(out,offset,gw_1);
	Serializer<uint8_t>::serialize(out,offset,gw_2);
	Serializer<uint8_t>::serialize(out,offset,gw_3);
	Serializer<uint8_t>::serialize(out,offset,gw_4);

	Serializer<uint8_t>::serialize(out,offset,ipA_1);
	Serializer<uint8_t>::serialize(out,offset,ipA_2);
	Serializer<uint8_t>::serialize(out,offset,ipA_3);
	Serializer<uint8_t>::serialize(out,offset,ipA_4);
	Serializer<uint32_t>::serialize(out,offset,portA);

	Serializer<uint8_t>::serialize(out,offset,ipB_1);
	Serializer<uint8_t>::serialize(out,offset,ipB_2);
	Serializer<uint8_t>::serialize(out,offset,ipB_3);
	Serializer<uint8_t>::serialize(out,offset,ipB_4);
	Serializer<uint32_t>::serialize(out,offset,portB);

}
void SetNetwork::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	slaveid = Serializer<uint8_t>::deserialize(in,offset);
	usePpcDefaults = Serializer<uint8_t>::deserialize(in,offset);

	ip_1 = Serializer<uint8_t>::deserialize(in,offset);
	ip_2 = Serializer<uint8_t>::deserialize(in,offset);
	ip_3 = Serializer<uint8_t>::deserialize(in,offset);
	ip_4 = Serializer<uint8_t>::deserialize(in,offset);

	subnet_1 = Serializer<uint8_t>::deserialize(in,offset);
	subnet_2 = Serializer<uint8_t>::deserialize(in,offset);
	subnet_3 = Serializer<uint8_t>::deserialize(in,offset);
	subnet_4 = Serializer<uint8_t>::deserialize(in,offset);

	gw_1 = Serializer<uint8_t>::deserialize(in,offset);
	gw_2 = Serializer<uint8_t>::deserialize(in,offset);
	gw_3 = Serializer<uint8_t>::deserialize(in,offset);
	gw_4 = Serializer<uint8_t>::deserialize(in,offset);

	ipA_1 = Serializer<uint8_t>::deserialize(in,offset);
	ipA_2 = Serializer<uint8_t>::deserialize(in,offset);
	ipA_3 = Serializer<uint8_t>::deserialize(in,offset);
	ipA_4 = Serializer<uint8_t>::deserialize(in,offset);
	portA = Serializer<uint32_t>::deserialize(in,offset);

	ipB_1 = Serializer<uint8_t>::deserialize(in,offset);
	ipB_2 = Serializer<uint8_t>::deserialize(in,offset);
	ipB_3 = Serializer<uint8_t>::deserialize(in,offset);
	ipB_4 = Serializer<uint8_t>::deserialize(in,offset);
	portB = Serializer<uint32_t>::deserialize(in,offset);
}
uint32_t SetNetwork::serializedSize() const
{
	return (2+5*4) * sizeof(uint8_t) + 2 * sizeof(uint32_t);
}
