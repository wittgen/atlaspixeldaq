/*
 * Author: L. Kaplan <lkaplan@cern.ch>
 *         K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2015-XI-23
 * Description: testing pad for Fei3
 */
#include <unistd.h>
#include "Fei3Test.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "Serializer.h"

#include "BitUtils.h"
#include <bitset>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

std::string Fei3Test::opMode[] = { "DigitalInjection", "AnalogInjection", "GlobalRegReadback", "PixelRegReadback", "MccInjection", "MccRegReadback", "DumpModConfigs", "ResetModules", "ConfigureGlobal", "MccRegTest", "ConfigureAmpsOn" };

void showFeData( const std::vector<uint8_t>& rxData, size_t nDC ) {

#ifdef __XMK__
    dumpData(rxData);

    size_t k = 34;
    for(size_t dc = 0 ; dc < nDC ; ++dc) {
      std::bitset<320> dcContent;
      uint16_t dcBitPos = 0;
      for( ; k < rxData.capacity() ; ++k) {
        if( (rxData[k] & 0xF) == 0xF && (rxData[k+1] & 0xF0) == 0xF0 )
          dcContent.set(dcBitPos, 1);
        dcBitPos++;
        if(dcBitPos>320) break;
      }

      std::cout << "DC=" << dc << ": " << dcContent << std::endl;
    }
#endif

    return;
}

void showTriggerData( const std::vector<uint8_t>& rxData ) {
    size_t cntr = 0;
    size_t mccHeaderPos = 0;
    size_t nTrig = 1;
    for(size_t k = 0 ; k < rxData.size() ; ++k) {
      bool isMccHeader = false;
      uint8_t skipBit = 0;
      if( rxData[k] == 0xe8 || rxData[k] == 0x74 || rxData[k] == 0x3a || rxData[k] == 0x1d ) {
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (rxData[k] & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      uint8_t firstWord = (rxData[k] << 4) + ((rxData[k+1] & 0xF0) >> 4);
      if( firstWord == 0xe8 || firstWord == 0x74 || firstWord == 0x3a || firstWord == 0x1d ) {
        skipBit += 4;
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (firstWord & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      if( isMccHeader ) {
        cntr = 0;
      } else skipBit = 0;

      for(size_t b = skipBit ; b < 8 ; ++b) {
        std::cout << (( rxData[k] & (0x1 << (7-b) )) ? "1" : "0" );
        if( (++cntr)%4 == 0 ) std::cout << " ";
      }
    }
}

Fei3Test::Fei3Test() {
  setResultPtr();
  m_feMask = 0x0001;
  m_latency = 248;
  m_trigDelay = 240;
  m_nTriggers = 1;
  m_pulseDuration = 50;
  m_useHighCap = false;
  m_VCal = 400;
  m_nConsecutiveTrig = 16;
  m_rxMask = 0x00080000; // rxCh = 19

  m_opMode = DigitalInjection;
  m_sendViaSerialPort = true;
  m_initModuleConfig = false; // Enabling this flag will OVERWRITE the module configuration currently in the ROD

}

void Fei3Test::execute() {
  std::cout << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__
  printParams();

  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

if( DumpModConfigs == m_opMode || ResetModules == m_opMode || ConfigureGlobal == m_opMode || ConfigureAmpsOn == m_opMode ) {
  for(size_t iMod = 0 ; iMod < RodResourceManagerPIX::getNModuleConfigs() ; ++iMod) {
    std::cout << "iMod=" << iMod << std::endl;
    if( (m_rxMask & (0x1 << fes[iMod].m_rxCh)) == 0 ) continue;
    std::cout << "Processing iMod=" << iMod << std::endl;
    for(size_t iFE = 0 ; iFE < 16 ; ++iFE) {
      Fei3Mod &fe = fes[iMod];
      if( DumpModConfigs == m_opMode ) {
        std::cout << "GLOBAL config FE #" << iFE << std::endl;
        dumpData(fe.m_chipCfgs[iFE].globcfg, 8);
      } else if( ResetModules == m_opMode ) {
        // Complete reset of the MCC and all FEs
        fe.mccGlobalResetMCC();
        fe.mccGlobalResetFE(31); // Hard FE reset
      } else if( ConfigureGlobal == m_opMode || ConfigureAmpsOn == m_opMode ) {
        size_t regWord = 0;
        // This default config is good to go for most cases
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x08c00101;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x08100000;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x20202000;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x28090101;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x09a71044;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x80800016;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0xf8381c80;
        fe.m_chipCfgs[iFE].globcfg[regWord++] = 0x0000002f;

        // Skip configurations that didn't come from the connectivity
        if( fe.revision == 0 ) continue;
        std::cout << "GLOBAL config for iMod=" << iMod << " (" << fe.idStr << "), fe # " << iFE << std::endl;
        //dumpData(fe.m_chipCfgs[iFE].globcfg, 8);
        fe.sendGlobalConfig(iFE);

        // Sending NULL pixel configuration
        // Todo: change loop order for speedup (until 5MHz feature is there)
        for (int reg = 0; reg < Fei3::nPixLatches; reg++) {
          fe.strobePixelLatchesDC(iFE, static_cast<Fei3::PixLatch>(reg), &Fei3PixelCfg::nullPixelSR[0]);
        }

        uint32_t oneStream[1][Fei3PixelCfg::nWordsPerDC];
        for(size_t i = 0 ; i < Fei3PixelCfg::nWordsPerDC ; ++i)
          oneStream[0][i] = 0xFFFFFFFF;

        if( ConfigureAmpsOn == m_opMode ) {
          fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::Preamp, oneStream);
        }

        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::Enable, oneStream);
        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::Select, oneStream);
        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::TDAC5, oneStream);
        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::TDAC6, oneStream);
        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::FDAC1, oneStream);
        fe.strobePixelLatchesDC(iFE, Fei3::PixLatch::FDAC2, oneStream);

        // Re-enabling ALL double columns
        fe.setAllPixelShiftRegisters(iFE, true); // Enable all DCs on the pixel for writing
        fe.sendGlobalConfig(iFE); // send the global config with these DCs enabled

      }
    }
  }

  return;
}



  std::cout << "Sending test pattern" << std::endl;
  for (int iRxCh = 0 ; iRxCh < 32; iRxCh++) {
    if( (m_rxMask & (1<<iRxCh)) == 0 ) continue;
    int iTxCh = iRxCh + m_txShift;
    std::cout << "Processing rxCh=" << iRxCh << " with txCh=" << iTxCh << std::endl;

    IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;
    uint8_t mccRxSpeed = rxSpeed + (rxSpeed ? 1 : 0);
    IblBoc::PixSetSpeed( rxSpeed );
    std::cout << "MCC speed: " << HEX(mccRxSpeed) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

    int8_t fibreNr = IblBoc::getMasterFibreFromRxCh(iRxCh);
    IblBoc::PixSetRxThreshold(fibreNr, 30000);
    IblBoc::PixSetRxRGS(fibreNr, 65500);
    // Drain BOC RX FIFO
    IblBocRxFifo rxFifo;
    rxFifo.setRxCh(iRxCh);
    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );

    // Enabling the RX and TX channels
    IblBoc::write(iTxCh/16, iTxCh%16, IblBoc::TxControl, 0x01);

    Fei3Mod &fe = fes[iRxCh];
  
    std::cout << "Processing rxCh=" << iRxCh << " with txCh=" << iTxCh << std::endl;
    fe.setTxCh(iTxCh);
    fe.setRxCh(iRxCh);

    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x00);

    if (GlobalRegReadback == m_opMode || PixelRegReadback == m_opMode)
      IblBoc::PixSetTrailerLength(iRxCh, 10000); // Increasing number of zeros allowed in BOC RX monitoring FIFO before it shuts down
    else
      IblBoc::PixSetTrailerLength(iRxCh, 22);

    // Sending half-clock before setting the fine delay, to allow it to kick in
    std::cout << "Setting RxDelay...";
    fe.writeMCCRegister(CSR, 0x0020);
    IblBoc::SetRxSampleDelay( fibreNr, 4, 10);
    //fe.writeMCCRegister(CSR, 0x0040 | mccRxSpeed ); // write to CSR to enable play back
    fe.writeMCCRegister(CSR, 0x0000 | mccRxSpeed ); // write to CSR to DISABLE play back
    std::cout << "Done" << std::endl;

    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );
    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x07);
    std::cout << "rxCh=" << static_cast<uint32_t>(iRxCh) << "; RxControl=" << HEX( IblBoc::read(iRxCh/16, iRxCh%16, IblBoc::RxControl) ) << std::endl;

    std::vector<Fei3HitData> hd;

#if 0
    fe.mccGlobalResetMCC();

    fe.writeMCCRegister(CSR, 0x0040 | mccRxSpeed ); // write to CSR to enable play back

    fe.writeMCCRegister(CNT, 0x0004); // write to CNT to set length of data

    fe.addHit(0x0001, 0, 100, 5, 75);
    fe.addEOE(0x0001, 0);
    fe.writeMCCRegister(FEEN, 0x0001);

    fe.mccEnDataTake(); // enable data taking

    fe.sendLV1(); // trigger and start event builder
    hd = fe.readHitData();
    Fei3ModData::decodeRawHitData( hd );
#endif

      // Use this for loops
      // Leave indented to avoid re-indenting whole code
      for(size_t theLatency = 255 ; theLatency > 0 ; theLatency--) {
      theLatency = 248;
      theLatency = m_latency;

    // Complete reset of the MCC and all FEs
    fe.mccGlobalResetMCC();
    fe.mccGlobalResetFE(10); // Hard FE reset

    if(MccRegReadback == m_opMode || MccRegTest == m_opMode) {
      for(size_t i = 0 ; i < 2 ; ++i) {

        if( MccRegTest == m_opMode ) {
          uint16_t mccPattern = 0xAAAA;
          if(i) mccPattern = 0x5555;
          for(size_t mccReg = 0; mccReg < 11; ++mccReg)
            fe.writeMCCRegister((MCCRegisters)mccReg, mccPattern);
        }
        // Read back
        for(size_t mccReg = 0; mccReg < 11; ++mccReg) {
          fe.mccRdRegister(mccReg);

          std::vector<uint8_t> rxData;
          rxData.reserve(8);
          for(size_t k = 0 ; k < rxData.capacity() ; ++k)
            rxData.push_back( IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow ) );

          uint32_t rxDataWord = 0x0;
          for(size_t k = 0 ; k < rxData.size() ; ++k) {
            if( rxData[k] == 0x00 ) continue;
            rxDataWord = rxData[k] << 24 | rxData[k+1] << 16 | rxData[k+2] << 8 | rxData[k+3];
            break;
          }

          uint16_t mccRegValue = ((rxDataWord >> 11) & 0xFFFF);
          //dumpData(rxData);
          std::cout << "Register #" << mccReg << " value: " << mccRegValue << "(" << HEX( mccRegValue ) << ")" << std::endl;

        }

      }

      return;
    }

    // Row and column start counting from 0 and not 1
    uint32_t hitRow = 0;
    uint32_t hitCol = 5;
    uint32_t posInDC = ((hitCol%2) ?  (319-hitRow) : hitRow);
    uint32_t nDC = 1;

    // 18 rows of 160 pixels, accessible via 9 double columns of 320 pixels
    Fei3::GlobReg colEnReg[] = { Fei3::GlobReg::EnableCol0, Fei3::GlobReg::EnableCol1, Fei3::GlobReg::EnableCol2,
                                 Fei3::GlobReg::EnableCol3, Fei3::GlobReg::EnableCol4, Fei3::GlobReg::EnableCol5,
                                 Fei3::GlobReg::EnableCol6, Fei3::GlobReg::EnableCol7, Fei3::GlobReg::EnableCol8 };

    for(size_t iFE = 0 ; iFE < 16 ; ++iFE) {
    //if( iFE != 9 ) continue;
    if( (m_feMask & ( 0x1 << iFE )) == 0x0 ) continue;

    if( m_initModuleConfig ) {
      // Loading global config (dispenses from re-initializing the ROD)
      fe.m_chipCfgs[iFE].globcfg[0] = 0x08c001f0;
      fe.m_chipCfgs[iFE].globcfg[1] = 0x08100000;
      fe.m_chipCfgs[iFE].globcfg[2] = 0x20200000;
      fe.m_chipCfgs[iFE].globcfg[3] = 0x68010100;
      fe.m_chipCfgs[iFE].globcfg[4] = 0x08001003;
      fe.m_chipCfgs[iFE].globcfg[5] = 0x8000000c;
      fe.m_chipCfgs[iFE].globcfg[6] = 0x01000080;
      fe.m_chipCfgs[iFE].globcfg[7] = 0x00000020;

      // Set global threshold to maximum value
      fe.writeGlobalReg(iFE, Fei3::GlobReg::GlobalTDAC, 0x1F);

      fe.writeGlobalReg(iFE, Fei3::GlobReg::HitBusEnable, 0);
      fe.writeGlobalReg(iFE, Fei3::GlobReg::TSI_TSC_Enable, 1); // Necessary to propagate timing information to double column !!!

      fe.writeGlobalReg(iFE, Fei3::GlobReg::EOC_MUX, 0);
      fe.writeGlobalReg(iFE, Fei3::GlobReg::THRMIN, 0);
      fe.writeGlobalReg(iFE, Fei3::GlobReg::ReadMode, 0);

      fe.writeGlobalReg(iFE, Fei3::GlobReg::CEU_Clock, 1);
    }

    std::cout << "theLatency = " << theLatency << std::endl;
    fe.writeGlobalReg(iFE, Fei3::GlobReg::Latency, theLatency); // scanCtrl->L1Latency

    if (DigitalInjection == m_opMode) {
      fe.writeGlobalReg(iFE, Fei3::GlobReg::EnableDigInj, 1);
      fe.writeGlobalReg(iFE, Fei3::GlobReg::VCal, 0);
    } else if (AnalogInjection == m_opMode) {
      fe.writeGlobalReg(iFE, Fei3::GlobReg::EnableDigInj, 0);
      if(m_useHighCap) fe.writeGlobalReg(iFE, Fei3::GlobReg::HighInjCapSel, 1);
      else fe.writeGlobalReg(iFE, Fei3::GlobReg::HighInjCapSel, 0);
      fe.writeGlobalReg(iFE, Fei3::GlobReg::VCal, m_VCal);
    }

    //fe.writeGlobalReg(iFE, Fei3::GlobReg::EnableHitParity, 1); // hit parity

    if (PixelRegReadback == m_opMode)
      fe.writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 11); // pixel register
    else if (GlobalRegReadback == m_opMode)
      fe.writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 15); // global register
    else
      fe.writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 8); // hit data

    //fe.writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 9); // command register

    fe.setAllPixelShiftRegisters(iFE, false);
    //fe.writeGlobalReg(iFE, Fei3::GlobReg::EnableCol1, true);
    fe.writeGlobalReg(iFE, colEnReg[hitCol/2], true);

#if 0
    nDC = 1;
    // Enabling all DCs
    fe.setAllPixelShiftRegisters(iFE, false);
    for(size_t dc = 0 ; dc < nDC ; ++dc)
      fe.writeGlobalReg(iFE, colEnReg[dc], true);
#endif


    std::cout << "GLOBAL config" << std::endl;
    dumpData(fe.m_chipCfgs[iFE].globcfg, 8);
    fe.sendGlobalConfig(iFE);

    std::vector<uint32_t> wrNullCmd;
    wrNullCmd.push_back(0x000000);

      if (MccInjection == m_opMode) {

    fe.writeMCCRegister(CNT, 0x0004); // write to CNT to set length of data
    for(size_t row = 0 ; row < 1 ; row += 2 ) {
      for(size_t col = 0 ; col < 18 ; ++col) {
        std::cout << "Sending " << m_nTriggers << " triggers" << std::endl;
        std::cout << "ROW=" << row << ", COL=" << col << std::endl;

        uint32_t nT = 1;

        do {
          for(size_t p = 0 ; p < 2 ; ++p)
            fe.addHit(m_feMask, 0, row+p, col, (row*10+col) & 0xFF);
          fe.addEOE(m_feMask, 0);

          fe.writeMCCRegister(FEEN, m_feMask);
          fe.mccEnDataTake(); // enable data taking

          if(!m_nTriggers) continue;
          fe.sendLV1(); // trigger and start event builder
          std::vector<Fei3HitData> hd = fe.readHitData();
          Fei3ModData::decodeRawHitData( hd );
        } while( nT++ < m_nTriggers );
      }
    }

    return;
      }

/*
    fe.mccWrFrontEnd(wrNullCmd);
    fe.getTxLink()->send();
*/

      if (GlobalRegReadback == m_opMode) {
    fe.readGlobalConfig(iFE);

    std::vector<uint8_t> rxData;
    rxData.reserve(400);

    for(size_t k = 0 ; k < rxData.capacity() ; ++k) rxData.push_back(rxFifo.read());
    std::cout << "Contents of global register for front-end " << iFE << std::endl;
    dumpData(rxData);

    // 0xe8 + 231 bytes
    size_t cnt = 0;
    for(size_t j = 32 ; j < rxData.capacity() ; j++) {
      uint8_t v = ((rxData[j] & 0x3F) << 2) + ((rxData[j+1] & 0xC0) >> 6);
      if( v == 0xFF ) std::cout << "1";
      else if( v == 0x00 ) std::cout << "0";
      else std::cout << "[" << HEX( v ) << "]";
      if( j%4 == 3 ) std::cout << " ";
      ++cnt;
      if(cnt == 231) break;
      else if(cnt % 32 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
    
    return;
      }

#if 0
    std::cout << "Hello" << std::endl;
    //fe.writeGlobalReg(0, Fei3::GlobReg::EnableIPMonitor, 1);

    std::cout << "Goodbye" << std::endl;
    fe.writeGlobalReg(0, Fei3::GlobReg::EnableTune, 1);
    fe.sendGlobalConfig(0);
    fe.readGlobalConfig(0);
#endif

#if 0
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> softR;
    Fei3Cmd::softReset(iFE, softR);
    fe.mccWrFrontEnd(softR);
#endif

    // Preparing the contents of the PixelShifRegister (SR)

    uint32_t bitstream[nDC][Fei3PixelCfg::nWordsPerDC];
    for (uint32_t k = 0; k < nDC ; k++) {
      for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++)
        bitstream[k][j] = 0x00000000;
      bitstream[k][posInDC/32] = 0x1 << (posInDC%32);

      //for (int j = 0; j < Fei3PixelCfg::nWordsPerDC; j++)
      //  bitstream[k][j] = 0xAAAAAAAA;
    }

    std::cout << "SR content" << std::endl;
    for (uint32_t k = 0; k < nDC ; k++) {
      std::cout << "DC=" << k << std::endl;
      dumpData(bitstream[k], Fei3PixelCfg::nWordsPerDC);
    }

    // CLOCK PIXEL   
    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    std::vector<uint32_t> clockPixelData;
    Fei3Cmd::clockPixel(iFE, clockPixelData, bitstream, nDC);
    std::cout << "clockPixel size=" << clockPixelData.size() << std::endl;
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.getTxLink()->send();

      if (PixelRegReadback == m_opMode) {

    fe.writeMCCRegister(CNT, 0x4);

    // WRITE HITBUS
    std::vector<uint32_t> wrEnData;
    Fei3Cmd::writeEnable(iFE, wrEnData);
    std::cout << "wrEn size=" << wrEnData.size() << std::endl;
    fe.mccWrFrontEnd(wrEnData, false);
    fe.getTxLink()->send();

/*
    fe.mccWrFrontEnd(wrNullCmd);
    fe.getTxLink()->send();
    usleep(100000);
*/


    uint32_t bitstream0[nDC][Fei3PixelCfg::nWordsPerDC];
    for (uint32_t k = 0; k < nDC ; k++) {
      for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++)
        bitstream0[k][j] = 0x00000000;
    }

    // Clock pixel shift register with zeros
    // CLOCK PIXEL NULL
    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    std::vector<uint32_t> clockPixelData0;
    Fei3Cmd::clockPixel(iFE, clockPixelData0, bitstream0, nDC);
    fe.mccWrFrontEnd(clockPixelData0);

    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> rdEnData;

    rdEnData.clear();
    // READ PIXEL
    rdEnData.push_back((0x800001 << 5) + (iFE & 0xF));
    //rdEnData.push_back((0x800200 << 5) + (iFE & 0xF));
    //rdEnData.push_back((0x800301 << 5) + (iFE & 0xF));
    //rdEnData.push_back(0x0);
    //fe.writeMCCRegister(CNT, (32 << 3) | 0x4);
    fe.mccWrFrontEnd(rdEnData);

    // WRITE HITBUS + READ PIXEL
    rdEnData.clear();
    rdEnData.push_back((0x800200 << 5) + (iFE & 0xF));
    //rdEnData.push_back(0x0);
    fe.mccWrFrontEnd(rdEnData);

    fe.writeMCCRegister(CNT, (1 << 3) | 0x4);
    // CLOCK PIXEL + WRITE HITBUS + READ PIXEL
    rdEnData.clear();
    rdEnData.push_back((0x800301 << 5) + (iFE & 0xF));
    fe.mccWrFrontEnd(rdEnData);

/*
    fe.writeMCCRegister(CNT, 0x4);
    rdEnData.clear();
    // WRITE HITBUS + READ PIXEL
    rdEnData.push_back((0x800200 << 5) + (iFE & 0xF));
    fe.mccWrFrontEnd(rdEnData);
*/

/*
    // READ PIXEL
    rdEnData.clear();
    rdEnData.push_back((0x800001 << 5) + (iFE & 0xF));
    rdEnData.push_back(0x0);
    fe.mccWrFrontEnd(rdEnData);

    fe.mccWrFrontEnd(wrNullCmd);
    fe.getTxLink()->send();
 */

    //usleep(1000000);
    //fe.mccWrFrontEnd(rdEnData);

    std::vector<uint32_t> rdFeData;

    fe.writeMCCRegister(FEEN, 0x1 << iFE);
/*
    fe.writeMCCRegister(CNT, 0x4);
    rdFeData.push_back((0x800301 << 5) + (iFE & 0xF));
    fe.mccRdFrontEnd(rdFeData);
 */

    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // CLOCK PIXEL NULL
    fe.mccRdFrontEnd(clockPixelData0);
    usleep(10000);

/*
    fe.writeMCCRegister(CNT, 0x4);
    rdFeData.clear();
    rdFeData.push_back((0x800001 << 5) + (iFE & 0xF));
    fe.mccRdFrontEnd(rdFeData);
 */

/*
    fe.writeMCCRegister(CNT, (0 << 3) | 0x4);
    rdFeData.clear();
    // CLOCK PIXEL + READ PIXEL
    rdFeData.push_back((0x800100 << 5) + (iFE & 0xF));

    fe.mccRdFrontEnd(rdFeData);
    usleep(1000000);
 */

    // Get data from RX FIFO
    std::vector<uint8_t> rxData;
    rxData.reserve(4*80*nDC+40);
    for(size_t k = 0 ; k < rxData.capacity() ; ++k) {
      rxData.push_back(rxFifo.read());
    }
    showFeData(rxData, nDC);

/* MOVED TO FUNCTION
    dumpData(rxData);


    size_t k = 34;
    for(size_t dc = 0 ; dc < nDC ; ++dc) {
      std::bitset<320> dcContent;
      uint16_t dcBitPos = 0;
      for( ; k < rxData.capacity() ; ++k) {
        if( (rxData[k] & 0xF) == 0xF && (rxData[k+1] & 0xF0) == 0xF0 )
          dcContent.set(dcBitPos, 1);
        dcBitPos++;
        if(dcBitPos>320) break;
      }

      std::cout << "DC=" << dc << ": " << dcContent << std::endl;
    }
 */

    return;
      }

    // Enabling the pixels referenced to by the mask
    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // Re-using vector prepared above
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> maskData;
    Fei3Cmd::writeMask(iFE, maskData);
    std::cout << "maskData size=" << maskData.size() << std::endl;
    fe.mccWrFrontEnd(maskData, false);
    fe.getTxLink()->send();
    
    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // Re-using vector prepared above
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> selectData;
    Fei3Cmd::writeSelect(iFE, selectData);
    std::cout << "selectData size=" << selectData.size() << std::endl;
    fe.mccWrFrontEnd(selectData, false);
    fe.getTxLink()->send();
 

    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // Re-using vector prepared above
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> killData;
    Fei3Cmd::writeKill(iFE, killData);
    std::cout << "killData size=" << killData.size() << std::endl;
    fe.mccWrFrontEnd(killData, false);
    fe.getTxLink()->send();

    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // Re-using vector prepared above
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> tdacData;
    Fei3Cmd::writeTDAC6(iFE, tdacData);
    std::cout << "tdacData size=" << tdacData.size() << std::endl;
    fe.mccWrFrontEnd(tdacData, false);
    fe.getTxLink()->send();

    fe.writeMCCRegister(CNT, ((320*nDC) << 3) | 0x4);
    // Re-using vector prepared above
    fe.mccWrFrontEnd(clockPixelData, false);
    fe.writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> fdacData;
    Fei3Cmd::writeFDAC2(iFE, fdacData);
    std::cout << "fdacData size=" << fdacData.size() << std::endl;
    fe.mccWrFrontEnd(fdacData, false);
    fe.getTxLink()->send();

    fe.writeMCCRegister(LV1,(m_nConsecutiveTrig-1)<<8);

    //To loop over delay between end of CAL command and beginning of strobe
    //for(size_t calDel = 0 ; calDel < 0x3F ; calDel++) {
    uint8_t calDelay = 0;
    uint8_t calDelayRange = 5;
    //calDelay = calDel;
    uint32_t regVal = (1 << 10) | ((calDelayRange&0xF) << 6) | ((calDelay&0x3F));
    std::cout << "calDelay=" << static_cast<uint32_t>(calDelay) << ", calDelayRange=" << static_cast<uint32_t>(calDelayRange) << std::endl;

    fe.writeMCCRegister(CAL,regVal);

      //for(size_t cnt = 40 ; cnt < 60; cnt++) {
        //for(size_t delay = 255 ; delay > 0 ; --delay) {
        size_t delay = m_trigDelay;

    //fe.sendGlobalConfig(iFE);

    fe.writeMCCRegister(CNT,m_pulseDuration);
    fe.writeMCCRegister(FEEN,0x1 << iFE);

    fe.mccEnDataTake(); // enable data taking

#if 0
    IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x00);
    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );
    IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x0F);
    std::cout << "iRxCh=" << static_cast<uint32_t>(iRxCh) << "; RxControl=" << HEX( IblBoc::read(i/16, i%16, IblBoc::RxControl) ) << std::endl;
    std::vector<uint8_t> tmpRxData;
    for(int k = 0 ; k < 100 ; ++k) tmpRxData.push_back( IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow ) );
    dumpData( tmpRxData );
#endif

    // Disabling FIFO and clearing it
    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x01);
    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );
    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x07);

    SerialPort* sPort = RodResourceManagerPIX::getSerialPort();
    if(m_sendViaSerialPort) {
      // Attention, channels 0xF0F0 on a given slave will receive
      // the same data as 0x0F0F (this is done in the BOC)
      // Therefore, 0x0F0F == 0xFFFF (a.k.a. 0xF0F0 are useless bits)
      sPort->setTxMask(1<<iRxCh); // Mask to set on the serial Port
      fe.setTxLink(sPort);
    }

    fe.sendCALandLVL1(delay,m_nTriggers);
    usleep(100000);


#if 0
    hd = fe.readHitData();
    Fei3ModData::decodeRawHitData( hd );
#else
    std::vector<uint8_t> tmpRxData;
    tmpRxData.reserve(m_nConsecutiveTrig*8);
    for(size_t k = 0 ; k < tmpRxData.capacity() ; ++k) tmpRxData.push_back( IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow ) );
    std::cout << "tmpRxData.size()=" << tmpRxData.size() << std::endl;
    dumpData( tmpRxData );

    showTriggerData(tmpRxData);

#endif
        //} // delay
      //} //cnt

    //} // calDel

    } // iFE

    break;
    } // theLatency
  }

#if 0
for (size_t l = 0; l < 30; ++l) {
for (size_t k = 0; k < 10; ++k) std::cout << "\t" << HEX(rxFifo.read());
std::cout << std::endl;
}
#endif

#endif

}

void Fei3Test::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_verbosity);
  Serializer<uint32_t>::serialize(out, offset, m_rxMask);
  Serializer<uint16_t>::serialize(out, offset, m_feMask);
  Serializer<uint8_t>::serialize(out, offset, m_latency);
  Serializer<uint8_t>::serialize(out, offset, m_trigDelay);
  Serializer<uint16_t>::serialize(out, offset, m_nTriggers);
  Serializer<uint8_t>::serialize(out, offset, m_nConsecutiveTrig);
  Serializer<uint8_t>::serialize(out, offset, m_pulseDuration);
  Serializer<bool>::serialize(out, offset, m_useHighCap);
  Serializer<uint16_t>::serialize(out, offset, m_VCal);
  Serializer<OpMode>::serialize(out, offset, m_opMode);
  Serializer<int16_t>::serialize(out, offset, m_txShift);
  Serializer<bool>::serialize(out, offset, m_sendViaSerialPort);
  Serializer<bool>::serialize(out, offset, m_initModuleConfig);
}

void Fei3Test::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_verbosity = Serializer<uint32_t>::deserialize(in, offset);
  m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
  m_feMask = Serializer<uint16_t>::deserialize(in, offset);
  m_latency = Serializer<uint8_t>::deserialize(in, offset);
  m_trigDelay = Serializer<uint8_t>::deserialize(in, offset);
  m_nTriggers = Serializer<uint16_t>::deserialize(in, offset);
  m_nConsecutiveTrig = Serializer<uint8_t>::deserialize(in, offset);
  m_pulseDuration = Serializer<uint8_t>::deserialize(in, offset);
  m_useHighCap = Serializer<bool>::deserialize(in, offset);
  m_VCal = Serializer<uint16_t>::deserialize(in, offset);
  m_opMode = Serializer<OpMode>::deserialize(in, offset);
  m_txShift = Serializer<int16_t>::deserialize(in, offset);
  m_sendViaSerialPort = Serializer<bool>::deserialize(in, offset);
  m_initModuleConfig = Serializer<bool>::deserialize(in, offset);
}

uint32_t Fei3Test::serializedSize() const {
  return 2 * sizeof(uint32_t) + 4 * sizeof(uint16_t) + 4 * sizeof(uint8_t) + 3 * sizeof(bool) + sizeof(OpMode);
}

void Fei3Test::printParams() {
  std::cout << "m_feMask=" << HEX( m_feMask ) << std::endl;
  std::cout << "m_latency=" << static_cast<uint32_t>(m_latency) << std::endl;
  std::cout << "m_trigDelay=" << static_cast<uint32_t>(m_trigDelay) << std::endl;
  std::cout << "m_nTriggers=" << m_nTriggers << std::endl;
  std::cout << "m_nConsecutiveTrig=" << static_cast<uint32_t>(m_nConsecutiveTrig) << std::endl;
  std::cout << "m_pulseDuration=" << static_cast<uint32_t>(m_pulseDuration) << std::endl;
  std::cout << "m_useHighCap=" << std::boolalpha << static_cast<uint32_t>(m_useHighCap) << std::endl;
  std::cout << "m_VCal=" << static_cast<uint32_t>(m_VCal) << std::endl;
  std::cout << "m_opMode=" << opMode[m_opMode] << " (" << static_cast<int>(m_opMode) << ")" << std::endl;
  std::cout << "m_txShift=" << m_txShift << std::endl;
  std::cout << "m_sendViaSerialPort=" << std::boolalpha << static_cast<uint32_t>(m_sendViaSerialPort) << std::endl;
  std::cout << "m_initModuleConfig=" << std::boolalpha << static_cast<uint32_t>(m_initModuleConfig) << std::endl;
}
