#include "WritePixelRegisterConfig.h"
#include "ItersAndAlgorithms.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#endif

//WARNING! Does not check if your mask and vector match in length so get it right! :P
void WritePixelRegisterConfig::deserialize(const uint8_t *in){
#ifdef __XMK__
  uint32_t offset = 0;
  configType = Serializer<ConfigType>::deserialize(in,offset);
  feMask = Serializer<uint32_t>::deserialize(in,offset);
  uint8_t length = Serializer<uint8_t>::deserialize(in,offset);
  Fei4* fe = ((configType == Physics) ? RodResourceManagerIBL::getInstance().getPhysicsModuleConfigs() : RodResourceManagerIBL::getInstance().getCalibModuleConfigs());
  uint32_t i_loc = 0;
  for (uint32_t i_mod = 0;i_loc < length && i_mod < RodResourceManagerIBL::getInstance().getNModuleConfigs(); ++i_mod) 
    if(maskInterpret(feMask, i_mod))
      *((Fei4PixelCfg*)(&fe[i_mod])) = Serializer<Fei4PixelCfg>::deserialize(in,offset);    
#endif
}
