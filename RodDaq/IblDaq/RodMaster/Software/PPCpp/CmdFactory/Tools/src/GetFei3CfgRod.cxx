#include "GetFei3CfgRod.h"
#include "Fei3Helpers.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "Serializer.h"

#include "BitUtils.h"
#include <bitset>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

std::string GetFei3CfgRod::opMode[] = { "GlobalRegReadback", "PixelRegReadback", "FullFE" };

GetFei3CfgRod::GetFei3CfgRod() {
  setResultPtr();
  m_rxMask = 0xFFFFFFFF;
  m_feMask = 0xF;
  m_opMode = GlobalRegReadback;
}

void GetFei3CfgRod::execute(){

  std::cout << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__
  printParams();

  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

  for(size_t iMod = 0 ; iMod < RodResourceManagerPIX::getNModuleConfigs() ; ++iMod) {
    Fei3Mod fe = fes[iMod];
    if( !fe.revision ) continue; // Todo: add warning message
    //std::cout << "iMod=" << iMod << ", revision=" << fe.revision << std::endl;
    uint8_t iRxCh = fe.m_rxCh;
    if( (m_rxMask & ( 0x1 << iRxCh )) == 0x0 ) continue;

    for(uint8_t iFE = 0 ; iFE < 16 ; ++iFE) {
      if( (m_feMask & ( 0x1 << iFE )) == 0x0 ) continue;
      std::cout << "iMod " << iMod << " FE " << (int) iFE << std::endl;

      if (GlobalRegReadback == m_opMode) {
        Fei3GlobalCfg globCfg = static_cast<Fei3GlobalCfg>(fe.m_chipCfgs[iFE]);
        result.GlobalCfg[fe.m_rxCh][iFE] = globCfg; 
      }
      else if (PixelRegReadback == m_opMode) {
        Fei3PixelCfg pixelCfg = static_cast<Fei3PixelCfg>(fe.m_chipCfgs[iFE]);
        result.PixelCfg[fe.m_rxCh][iFE] = pixelCfg;
      }
      else if (FullFE == m_opMode) {
          Fei3GlobalCfg globCfg = static_cast<Fei3GlobalCfg>(fe.m_chipCfgs[iFE]);
          Fei3PixelCfg pixelCfg = static_cast<Fei3PixelCfg>(fe.m_chipCfgs[iFE]);
          result.GlobalCfg[fe.m_rxCh][iFE] = globCfg;
          result.PixelCfg[fe.m_rxCh][iFE] = pixelCfg;
      }

    }
  }

#endif

}

void GetFei3CfgRod::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_rxMask);
  Serializer<uint16_t>::serialize(out, offset, m_feMask);
  Serializer<OpMode>::serialize(out, offset, m_opMode);
}

void GetFei3CfgRod::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
  m_feMask = Serializer<uint16_t>::deserialize(in, offset);
  m_opMode = Serializer<OpMode>::deserialize(in, offset);
}

uint32_t GetFei3CfgRod::serializedSize() const {
  return sizeof(uint32_t) + sizeof(uint16_t) + sizeof(OpMode);
}

void GetFei3CfgRod::printParams() {
  std::cout << "m_rxMask=" << HEX( m_rxMask ) << std::endl;
  std::cout << "m_feMask=" << HEX( m_feMask ) << std::endl;
  std::cout << "m_opMode=" << opMode[m_opMode] << " (" << static_cast<int>(m_opMode) << ")" << std::endl;
}

void GetFei3CfgRodResult::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<std::map <uint32_t, std::map<uint32_t, Fei3GlobalCfg> > >::serialize(out,offset,GlobalCfg);
  Serializer<std::map <uint32_t, std::map<uint32_t, Fei3PixelCfg > > >::serialize(out,offset,PixelCfg);
}

void GetFei3CfgRodResult::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  GlobalCfg = Serializer< std::map <uint32_t, std::map<uint32_t, Fei3GlobalCfg> > >::deserialize(in,offset);
  PixelCfg  = Serializer< std::map <uint32_t, std::map<uint32_t, Fei3PixelCfg > > >::deserialize(in,offset);
}

uint32_t GetFei3CfgRodResult::serializedSize() const {
  
  std::size_t sizeMap = Serializer< std::map <uint32_t, std::map<uint32_t, Fei3GlobalCfg> > >::size(GlobalCfg)
                      + Serializer< std::map <uint32_t, std::map<uint32_t, Fei3PixelCfg > > >::size(PixelCfg);
  return sizeMap;
}

