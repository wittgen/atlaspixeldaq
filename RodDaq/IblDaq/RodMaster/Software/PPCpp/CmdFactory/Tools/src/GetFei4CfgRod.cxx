#include "GetFei4CfgRod.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "Serializer.h"

#include "BitUtils.h"
#include <bitset>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

std::string GetFei4CfgRod::opMode[] = { "GlobalRegReadback", "PixelRegReadback", "FullFE" };

GetFei4CfgRod::GetFei4CfgRod() {
  setResultPtr();
  m_rxMask = 0xFFFFFFFF;
  m_opMode = GlobalRegReadback;
}

void GetFei4CfgRod::execute(){

  std::cout << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__
  printParams();

  Fei4 *fes = RodResourceManagerIBL::getInstance().getCalibModuleConfigs();

  for(size_t iMod = 0 ; iMod < RodResourceManagerIBL::getNModuleConfigs() ; ++iMod) {
    Fei4 fe = fes[iMod];
    if( !fe.getRevision() ) continue; // Todo: add warning message
    //std::cout << "iMod=" << iMod << ", revision=" << fe.revision << std::endl;
    uint8_t iRxCh = fe.getRxCh();
    if( (m_rxMask & ( 0x1 << iRxCh )) == 0x0 ) continue;

      std::cout << "iMod " << iMod << std::endl;
      if (GlobalRegReadback == m_opMode) {
        Fei4GlobalCfg globCfg = static_cast<Fei4GlobalCfg>(fe);
        result.GlobalCfg[iRxCh][0] = globCfg; 
      }
      else if (PixelRegReadback == m_opMode) {
        Fei4PixelCfg pixelCfg = static_cast<Fei4PixelCfg>(fe);
        result.PixelCfg[iRxCh][0] = pixelCfg;
      }
      else if (FullFE == m_opMode) {
          Fei4GlobalCfg globCfg = static_cast<Fei4GlobalCfg>(fe);
          Fei4PixelCfg pixelCfg = static_cast<Fei4PixelCfg>(fe);
          result.GlobalCfg[iRxCh][0] = globCfg;
          result.PixelCfg[iRxCh][0] = pixelCfg;
      }
  }
#endif
}

void GetFei4CfgRod::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_rxMask);
  Serializer<OpMode>::serialize(out, offset, m_opMode);
}

void GetFei4CfgRod::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
  m_opMode = Serializer<OpMode>::deserialize(in, offset);
}

uint32_t GetFei4CfgRod::serializedSize() const {
  return sizeof(uint32_t) + sizeof(OpMode);
}

void GetFei4CfgRod::printParams() {
  std::cout << "m_rxMask=" << HEX( m_rxMask ) << std::endl;
  std::cout << "m_opMode=" << opMode[m_opMode] << " (" << static_cast<int>(m_opMode) << ")" << std::endl;
}

void GetFei4CfgRodResult::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<std::map <uint32_t, std::map<uint32_t, Fei4GlobalCfg> > >::serialize(out,offset,GlobalCfg);
  Serializer<std::map <uint32_t, std::map<uint32_t, Fei4PixelCfg > > >::serialize(out,offset,PixelCfg);
}

void GetFei4CfgRodResult::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  GlobalCfg = Serializer< std::map <uint32_t, std::map<uint32_t, Fei4GlobalCfg> > >::deserialize(in,offset);
  PixelCfg  = Serializer< std::map <uint32_t, std::map<uint32_t, Fei4PixelCfg > > >::deserialize(in,offset);
}

uint32_t GetFei4CfgRodResult::serializedSize() const {
  
  std::size_t sizeMap = Serializer< std::map <uint32_t, std::map<uint32_t, Fei4GlobalCfg> > >::size(GlobalCfg)
                      + Serializer< std::map <uint32_t, std::map<uint32_t, Fei4PixelCfg > > >::size(PixelCfg);
  return sizeMap;
}

