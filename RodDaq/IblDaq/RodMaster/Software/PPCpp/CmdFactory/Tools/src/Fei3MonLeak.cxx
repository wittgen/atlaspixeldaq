/*
 *
 */
#include <stdlib.h>
#include "Bitstream.h"

#include "Fei3MonLeak.h"
#ifdef __XMK__
#include "IblBoc.h"
#include "IblBocLink.h"
#include "RodResourceManager.h"
#include "BarrelConnectivity.h"
#include "Fei3Mod.h"
#include "Fei3ModProxy.h"
#include "Fei3PixelCfg.h"
#include "Fei3ModCfg.h"
#endif

Fei3MonLeak::Fei3MonLeak() {

	setResultPtr();
	m_rxMask =0xFFFFFFFF;
	m_nPixPerFE =1;
	m_nSteps =8;
}

void Fei3MonLeak::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

uint32_t newRxMask = initMonLeak(m_rxMask);
uint32_t nBitsCheck = loadCompareBits(newRxMask);

int steps[10]={512,256,128,64,32,16,8,4,2,1};

  constexpr uint32_t maxPixPerFE = Fei3PixelCfg::nDC*Fei3PixelCfg::nWordsPerDC*32;

   if(m_nPixPerFE > maxPixPerFE  ){
   m_nPixPerFE = maxPixPerFE;
   std::cout<<"You requested "<<(int)m_nPixPerFE<<" Pixel per FE but maximum number of Pixels per FE is "<<(int)maxPixPerFE<<" setting to this value "<<std::endl;
   }

  uint32_t skipRxMask=0;
  std::map< uint8_t, std::vector <uint32_t > > MonADC;
  
  for(int index=0;index<m_nPixPerFE; index++){
    MonADC.clear();

    getMonLeakADC(newRxMask, index, m_nSteps, steps,nBitsCheck,skipRxMask, MonADC);

    for (std::map<uint8_t, std::vector <uint32_t > >::iterator it = MonADC.begin(); it!=MonADC.end(); ++it)result.MON_LEAK[it->first].push_back(it->second);
  }

cleanUp(m_rxMask);

#endif

}

uint32_t Fei3MonLeak::initMonLeak(uint32_t rxMask){

uint32_t newRxMask=0;

#ifdef __XMK__
   Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
   int maxModules = RodResourceManagerPIX::getInstance().getNModuleConfigs();

   IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;//Needed for the readBack
   IblBoc::PixSetSpeed( rxSpeed );

   RodResourceManagerIBL::getIblBocLink()->enableTxChannels(BarrelConnectivity::getTxMaskFromRxMask(rxMask));
   RodResourceManagerIBL::getIblBocLink()->enableRxChannels(rxMask);

   for(uint8_t iMod = 0 ; iMod < maxModules ; ++iMod){
	//Only channels belonging to my mask!!!
	if (fes[iMod].revision ==0)continue;
	uint8_t rxCh = fes[iMod].getRxCh();
	if( !(rxMask & (1 << rxCh)) )continue;

       	    newRxMask |= 1 << rxCh;
       	    
       	    fes[iMod].mccGlobalResetMCC();

	    uint16_t feMask = fes[iMod].maskEnableFEScan | fes[iMod].maskEnableFEConfig;
	    for (int iFE = 0; iFE < Fei3ModCfg::nFE; iFE++) {
	    if( !(feMask & (1 << iFE)) )continue;//Skip disabled FE

	     //Send Pixel config, TDAC high rest of Pixel regs (HitBus,Select, Enable, FDAC and Preamp to zero)
	     Fei3PixelCfg dummyPixCfg;
	     for (int reg = 0; reg < Fei3::nPixLatches; reg++) {
	       auto regLatch = static_cast<Fei3::PixLatch>(reg);
	       uint32_t bitstream[Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];
	       for (uint8_t i = 0; i < Fei3PixelCfg::nDC; i++){
	         for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
	             int word = dummyPixCfg.getWordNumber(regLatch, i, j);
	             if( regLatch == Fei3::PixLatch::FDAC0 || regLatch == Fei3::PixLatch::FDAC1 ||
	                 regLatch == Fei3::PixLatch::FDAC2 || regLatch == Fei3::PixLatch::HitBus ||
	                 regLatch == Fei3::PixLatch::Enable) {//FDAC and Hitbus to 0
	                  bitstream[i][j] = 0x0;
	             } else if (regLatch == Fei3::PixLatch::TDAC0 || regLatch == Fei3::PixLatch::TDAC1 ||
	                        regLatch == Fei3::PixLatch::TDAC2 || regLatch == Fei3::PixLatch::TDAC3 ||
	                        regLatch == Fei3::PixLatch::TDAC4 || regLatch == Fei3::PixLatch::TDAC5 ||
	                        regLatch == Fei3::PixLatch::TDAC6) {//TDAC high
	                  bitstream[i][j] = 0xFFFFFFFF;
	              } else {
	            	bitstream[i][j] = fes[iMod].m_chipCfgs[iFE].pixcfg[word];//Rest of regs to default values
	              }
	          }
		}
	      fes[iMod].strobePixelLatches(iFE, regLatch, Fei3PixelCfg::nDC, bitstream);
	     }
	    //Send global config for MonLeak configuration
	    fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::IF, 0x1);//Set IF to 1
	    fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::ITrimIf,0x1);//Set IF to 1
	    fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::MonLeakADC, 0x1200);//Set MonLeakADC register starting value for the binary search
	    fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::EnableLeakMeas, 0x1);//Enable Leak Measurement
	    fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 0xF);//Needed for the readback
	    fes[iMod].sendGlobalConfig(iFE);
	   }
    }


#endif

return newRxMask;

}

uint32_t Fei3MonLeak::loadCompareBits(uint32_t rxMask){

uint32_t nBitsCheck =0;

#ifdef __XMK__

	std::vector <uint8_t> CompareBitStream;
	std::vector <uint8_t> MaskBitStream;

	Fei3GlobalCfg compCfg;
	for(size_t i=0;i<Fei3GlobalCfg::nWordsGlobal;i++)compCfg.setWord(i, 0);

	//Control words needed for the monleak scan
	compCfg.writeRegGlobal(Fei3::GlobReg::MonLeakADC, 0x3000);//Enable Comp and enableADC
	compCfg.writeRegGlobal(Fei3::GlobReg::EnableLeakMeas, 0x1);//Enable Leak Measurement
	compCfg.writeRegGlobal(Fei3::GlobReg::Select_DO, 0xF);//Needed for the readback

	nBitsCheck = createCompareBitStream (CompareBitStream, MaskBitStream, compCfg );

	for(uint8_t rxCh = 0 ; rxCh < 32; ++rxCh){
	  if( !(rxMask & (1 << rxCh)) )continue;
	  std::cout<<"NBits check "<<nBitsCheck<<std::endl;
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareLoad(rxCh, CompareBitStream, MaskBitStream);
	  IblBoc::PixCompareEnable(rxCh);
	  IblBoc::PixSetTrailerLength(rxCh, 264);//This trailer lenght seems to work do not touch this value
	  int bocTrailerLength = IblBoc::PixGetTrailerLength(rxCh);
	  std::cout<<"Trailer length "<<(int)bocTrailerLength<<std::endl;
	}

#endif

return nBitsCheck;

}

void Fei3MonLeak::getMonLeakADC(uint32_t rxMask, uint16_t pixelIndex, int nSteps, int steps[MAX_STEPS_MON_LEAK], uint32_t nBitsCheck, uint32_t &skipRxMask, std::map < uint8_t, std::vector < uint32_t > > &MonADC, bool useBOCComp){

#ifdef __XMK__

//std::cout<<"Skip Rx mask 0x"<<std::hex<<skipRxMask<<std::dec<<std::endl;

uint32_t bitsReceived, bitErrors;

Fei3ModProxy Fei3Proxy( BarrelConnectivity::getModMaskFromRxMask(rxMask) );

uint32_t bitMask[Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];

	  //Generate bit mask for the mask stepping
	  int iPix=0;
	   for (unsigned i = 0; i < Fei3PixelCfg::nDC; i++){
	        for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
	                if(pixelIndex/32 == iPix){
	                bitMask[i][j] = ((1<<pixelIndex%32));
	                if(pixelIndex%100==0)std::cout<<"Scanning Pixel "<<pixelIndex<<" DC "<<(int)i <<" nWord "<<(int)j <<" mask "<<std::hex<<bitMask[i][j]<<std::dec<<std::endl;
	                } else{
	                bitMask[i][j] = 0x0;
	           	}
	        iPix++;
	        }
	   }
	 //Set Hit bus
	 Fei3Proxy.strobePixelLatches(16, Fei3::PixLatch::HitBus, bitMask);
	 int ADC[32][16];

	  for(int j=0;j<32;j++)
	  for(int k=0;k<Fei3ModCfg::nFE;k++) ADC[j][k] = steps[0];//Start value for the ADC register for the binary search

	Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
	int maxModules = RodResourceManagerPIX::getInstance().getNModuleConfigs();

	  for(uint8_t s=0;s<nSteps;s++){//Binary search steps

	     for (int iFE = 0; iFE < Fei3ModCfg::nFE; iFE++)
	         for(uint8_t iMod = 0 ; iMod < maxModules; ++iMod){
	           if (fes[iMod].revision ==0)continue;
	           uint8_t rxCh = fes[iMod].getRxCh();
	           if( !(rxMask & (1 << rxCh)) )continue;
	           uint16_t feMask = fes[iMod].maskEnableFEScan | fes[iMod].maskEnableFEConfig;
	           if( (skipRxMask & (1 << rxCh)) ){ADC[rxCh][iFE]=0xFFFF;continue;}//Skip bad Rx, bad opto?
	           if( !(feMask & (1 << iFE)) ){ADC[rxCh][iFE]=0xFFFF;continue;}//Skip disabled FE
	           
	           fes[iMod].writeGlobalReg(iFE, Fei3::GlobReg::MonLeakADC, ((ADC[rxCh][iFE]<<1)&0x7FE) | 0x1000);
	       	   fes[iMod].sendGlobalConfig(iFE);
	       

	       if(!useBOCComp){
	       Fei3GlobalCfg readbackCfg;
	          readbackCfg = fes[iMod].getGlobalConfig(iFE);
	          if (readbackCfg.readRegGlobal(Fei3::GlobReg::MonLeakADC) & 0x2000)//Comparator fires
	            ADC[rxCh][iFE] += steps[s+1];
	          else
	            ADC[rxCh] [iFE] -= steps[s+1];
	         } else {
	           //Clear counters and enable comparator
	           //IblBoc::PixCompareDisable(rxCh);
		   IblBoc::PixCompareClearCounters(rxCh);
		   IblBoc::PixCompareEnable(rxCh);

	           fes[iMod].readGlobalConfig(iFE);//Issue readback

	           IblBoc::PixCompareDisable(rxCh);
		   IblBoc::PixCompareGetCounters(rxCh, bitsReceived, bitErrors);//Get bits received and errors
	         //if(bitsReceived != nBitsCheck)
	         //std::cout<<"WARNING! For rxCh "<<(int)rxCh<<" FE "<<iFE<<" ERROR I got "<<(int)bitsReceived<<" bits received while I was expecting "<<(int)nBitsCheck<<std::endl;

	         if (bitErrors == 0 )//Comparator fires
	         ADC[rxCh][iFE] += steps[s+1];
	         else if ( bitErrors == 8 )//Comparator didn't fire
	         ADC[rxCh] [iFE] -= steps[s+1];
	         else//Bad opto?
	         std::cout<<"For rxCh "<<(int)rxCh<<" FE "<<iFE <<" I have "<<(int)bitErrors<<" bit ERRORS can only be 8 or 0!!!"<<std::endl;
	        
	        if(bitsReceived==0 || !(bitErrors == 0  || bitErrors == 8) ){//Check for bad opto
	         std::cout<<"Bad RX found "<<(int)rxCh<<" will be skipped!!"<<std::endl;
	         ADC[rxCh] [iFE] = 0xFFFF;//Set to max uint16_t
	         skipRxMask |= (1<<rxCh);
	         std::cout<<"Skip Rx mask 0x"<<std::hex<<skipRxMask<<std::dec<<std::endl;
	         }
	        }
	      }
	     }

	    for(uint8_t rxCh = 0 ; rxCh < 32; ++rxCh){
	     if( !(rxMask & (1 << rxCh)) )continue;
	     std::vector<uint32_t> MON_LEAK;
	       for (int iFE = 0; iFE < Fei3ModCfg::nFE; iFE++) {
	       MonADC[rxCh].push_back(ADC[rxCh] [iFE]);
	       }
	     }

#endif


}

void Fei3MonLeak::cleanUp( uint32_t rxMask,  bool useBOCComp ){

#ifdef __XMK__
      if(useBOCComp){
	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	  if (rxMask & (1<<rxCh)){
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareClearCounters(rxCh);
	  IblBoc::PixSetTrailerLength(rxCh, 22);//Set to default value
	  std::cout<<"RxCh: "<<(int)rxCh<<" Frame counts "<<(int)IblBoc::GetFrameCount(rxCh)<<std::endl;
	  }
	}
	RodResourceManagerIBL::getIblBocLink()->disableRxChannels(rxMask);

#endif

}

#ifdef __XMK__
uint32_t Fei3MonLeak::createCompareBitStream(std::vector<uint8_t> &CompareBitStream, std::vector<uint8_t> &MaskBitStream, Fei3GlobalCfg compCfg){

uint32_t nBitsCheck=0;

  Boclib::Bitstream val;
  Boclib::Bitstream mask;

  val.AppendVar(0x1D, 5);
  mask.AppendVar(0x1F, 5);//HEADER not MASKED

  val.AppendVar(0x00, 5);
  mask.AppendVar(0x1F, 5);//Zeros not MASKED

  for(int i=0;i<30 ;i++){
      val.Append8(0x00);
      mask.Append8(0xFF);
  }

    //Generate BitSteam from a given configuration
    for (unsigned i = 231; i >0; i--) {
        if ( (compCfg.globcfg[i/32] >>  i%32) & 0x1 ){
    	  val.Append8(0xFF);
          mask.Append8(0xFF);
    	  } else{
    	  val.Append8(0x00);
          mask.Append8(0x00);
    	  }
    }

  val.AppendVar(0x00,14);
  mask.AppendVar(0x00,14);
  
  for(int i=0;i<24 ;i++){
      val.Append8(0x00);
      mask.Append8(0x00);
  }

  for(int i=0;i<32 ;i++){
      val.Append8(0x00);
      mask.Append8(0x00);
  }

  CompareBitStream = val.GetData();
  MaskBitStream = mask.GetData();

  std::cout<<"Compare BS dump"<<std::endl;
    for (size_t i=0; i<CompareBitStream.size();++i){
	std::cout<<std::hex << std::setw(2) << std::setfill('0') << (int)CompareBitStream[i] << std::dec;
    	if((i+1)%4==0 && i!=0)std::cout<<"\t";
        if((i+1)%32==0 && i!=0)std::cout<<std::endl;
    }
  std::cout<<std::endl;

  std::cout<<"Mask BS dump"<<std::endl;
    for (size_t i=0; i<MaskBitStream.size();++i){
	std::cout<<std::hex << std::setw(2) << std::setfill('0') << (int)MaskBitStream[i] << std::dec;
    	if((i+1)%4==0 && i!=0)std::cout<<"\t";
        if((i+1)%32==0 && i!=0)std::cout<<std::endl;
	for(size_t j=0; j<8;++j)if(MaskBitStream[i] & (1<<j))nBitsCheck++;
    }
  std::cout<<std::endl;

return nBitsCheck;

}
#endif // _XMK_

void Fei3MonLeak::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset,  m_rxMask);
	Serializer<uint16_t>::serialize(out, offset,  m_nPixPerFE);
	Serializer<uint8_t>::serialize(out, offset,  m_nSteps);
}

void Fei3MonLeak::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
	m_nPixPerFE = Serializer<uint16_t>::deserialize(in, offset);
	m_nSteps = Serializer<uint8_t>::deserialize(in, offset);
}

uint32_t Fei3MonLeak::serializedSize() const
{
	return ( sizeof(uint32_t)+sizeof(uint16_t) + sizeof(uint8_t) );
}

void Fei3MonLeakResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::serialize(out,offset,MON_LEAK);
}

void Fei3MonLeakResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
        MON_LEAK = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::deserialize(in,offset);

}

uint32_t Fei3MonLeakResults::serializedSize() const
{
	std::size_t sizeMap = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::size(MON_LEAK);
	
	return sizeMap;
}

