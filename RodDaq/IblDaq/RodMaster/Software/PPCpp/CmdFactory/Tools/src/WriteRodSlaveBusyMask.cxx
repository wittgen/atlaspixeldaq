#include "WriteRodSlaveBusyMask.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void WriteRodSlaveBusyMask::dump() {

  std::cout<<"I'm about to write the busy mask on the ROD slave"<<std::endl;
}

WriteRodSlaveBusyMask::WriteRodSlaveBusyMask(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
  maskValue = 0x00000000;
  forceValue = 0x00000000;
  targetIsSlaveB = false;
}

// inside execute() the actual meat of the command happens on the ppc
void WriteRodSlaveBusyMask::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);


  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

  // write the busy mask register
  if (!targetIsSlaveB)
	std::cout << "\nGoing to write the busy control register on Slave A" << std::endl << std::endl;
  else
  	std::cout << "\nGoing to write the busy control register on Slave B" << std::endl << std::endl;

  if (!targetIsSlaveB){
  	IblRod::SlaveA::BusyMaskReg::write(maskValue);
        IblRod::SlaveA::BusyForceReg::write(forceValue);
  	std::cout<<"\n\t set slave A busy mask to "<<(unsigned int)maskValue<<" and force to "<<(unsigned int)forceValue<<std::endl<<std::endl;
	std::cout<<"Reading back: " << HEX( IblRod::SlaveA::BusyMaskReg::read() ) << " and " << HEX( IblRod::SlaveA::BusyForceReg::read() ) << std::endl;
  	}
  else {
  	IblRod::SlaveB::BusyMaskReg::write(maskValue);
        IblRod::SlaveB::BusyForceReg::write(forceValue);
  	std::cout<<"\n\t set slave B busy mask to "<<(unsigned int)maskValue<<" and force to "<<(unsigned int)forceValue<<std::endl<<std::endl;
	std::cout<<"Reading back: " << HEX( IblRod::SlaveB::BusyMaskReg::read() ) << " and " << HEX( IblRod::SlaveB::BusyForceReg::read() ) << std::endl;
  	}

  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void WriteRodSlaveBusyMask::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
	Serializer<uint32_t>::serialize(out,offset,maskValue);
	Serializer<uint32_t>::serialize(out,offset,forceValue);
	Serializer<bool>::serialize(out,offset,targetIsSlaveB);	
}

// Deseralize the data members of the command
// In this case, only one boolean
void WriteRodSlaveBusyMask::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
	maskValue = Serializer<uint32_t>::deserialize(in,offset);
	forceValue = Serializer<uint32_t>::deserialize(in,offset);
	targetIsSlaveB = Serializer<bool>::deserialize(in,offset);	
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t WriteRodSlaveBusyMask::serializedSize() const
{
	return (2*sizeof(uint32_t)+2*sizeof(bool));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void WriteRodSlaveBusyResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyForceValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyCurrentStatusValue);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void WriteRodSlaveBusyResult::deserialize(const uint8_t *in) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  slaveBusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyForceValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t WriteRodSlaveBusyResult::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (3*sizeof(uint32_t)/*+ sizeof(uint8_t)*/);
}
