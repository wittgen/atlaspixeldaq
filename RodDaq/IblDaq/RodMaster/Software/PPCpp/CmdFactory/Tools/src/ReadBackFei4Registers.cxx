#include <ReadBackFei4Registers.h>
#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblConnectivity.h"
#include "IblBocLink.h"
#include "IblBoc.h"
#include "Fei4Proxy.h"
#endif


ReadBackFei4Registers::ReadBackFei4Registers() {
  //default 
  setResultPtr(); //necessary to setup where the result class is
  m_clear = false;
  m_global  = true;
  m_modMask = 0x1;
  m_DC      = 0x0;
  m_CP      = 0x0;   // suggest: keep it at 0 
  rb_mode   =  RB_PixelRegisters;
  delay     = 0;
  ShiftRegLeft = 0;

}

void ReadBackFei4Registers::execute() {
  std::cout<<__PRETTY_FUNCTION__<<std::endl;
  
  #ifdef __XMK__
  
  if (RodResMgr::isFei3()) {
    std::cout<<"ERROR::This can run only on FEi4"<<std::endl;
    return;
  }
  
  //Getting instance of BocLink
  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
    
  //Setting Master register for changing serial port on the fly
  //uint32_t RodControlRegister = IblRod::Master::RrifCtrlReg::read();
  //uint32_t currentSerial0 = IblRod::Master::FeCmdMask0LsbReg::read();
  //uint32_t currentSerial1 = IblRod::Master::FeCmdMask1LsbReg::read();

  //Get the current enable mask
  uint32_t enabledMask =  (IblRod::SlaveA::FmtLinkEnable::read() & 0xFFFF) | ((IblRod::SlaveB::FmtLinkEnable::read() & 0xFFFF) << 16);
  std::cout<<std::hex<<"Enabled Mask 0x"<<enabledMask<<" Readback mask 0x"<<m_modMask<<std::dec<<std::endl;
  //uint32_t enabledTxMask = IblConnectivity::getTxMaskFromRxMask(enabledMask);
  
  //Get the read Tx back mask
  //uint32_t readbackTxMask = IblConnectivity::getTxMaskFromRxMask(m_modMask);

  //This version only can run on disabled modules.
  if ((enabledMask & m_modMask)) {
      xil_printf("This Version of the Readback can only run on previously disabled modules!\n");
      return;
    }
  
  //For the North or 0 (only first 8 bits)
  //IblRod::Master::FeCmdMask0LsbReg::write(((enabledTxMask) & (~readbackTxMask)) & 0xff );
  //For the South or 1 (most significative 8 bits)
  //IblRod::Master::FeCmdMask1LsbReg::write((((enabledTxMask) & (~readbackTxMask)) >> 16) & 0xff );
  //To set a mask we need to set 1 the bits 20 and the bits 2
  //IblRod::Master::RrifCtrlReg::write(RodControlRegister | 0x100004);
  //Unset Bit 20 and Bit 2
  //IblRod::Master::RrifCtrlReg::write(RodControlRegister & ~0x100004);
  
  //Enable the Tx/Rx Channels for readback
 
  iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(m_modMask));
  iblBocLink->enableRxChannels(m_modMask);
  
  //I'm going to run on all the modules I want to for the readback
  Fei4Proxy fes = Fei4Proxy(m_modMask);
  //This is to re-setup the run mode at the end
  Fei4Proxy fes_restore = Fei4Proxy(enabledMask);
  
  fes.setRunMode(false);
  
  std::map<uint32_t, Fei4PixelCfg> PixCfgMap;
  
  
  //uint32_t ShiftRegBitStream[DoubleColumnBit::nWords];
  
  
  if (!m_global) {
    
    ShiftRegLeft = (int)m_DC;
    std::cout<<"checking Shift Register"<<std::endl;
    std::cout<<"rb_mode "<<(int)rb_mode<<std::endl;
    std::cout<<"delay"<<(int)delay<<std::endl;
    std::cout<<"shift reg left "<<ShiftRegLeft<<std::endl;
    
    
    
    switch (rb_mode) 
      {
      case RB_ReadShiftRegister:
	std::cout<<"READ SHIFT REGISTER"<<std::endl;
	fes.readBackFullShiftRegister(result.PixCfgMap,delay,ShiftRegLeft);
	break;
      case RB_ClearAndReadBackSR:
	std::cout<<"Mode not implemented yet"<<std::endl;
	//fes.clearFullShiftRegister();
	break;
      case RB_WriteCustomPattern:
	std::cout<<"Mode not implemented yet"<<std::endl;
	//fes.writeCustomToShiftRegister(m_DC,m_CP);
	//fes.readBackShiftRegister(m_DC,m_CP);
	break;
      case RB_ClearSR:
	fes.clearFullShiftRegister();
	break;
      case RB_PutOnesInSR:
	std::cout<<"Push Ones in SR"<<std::endl;
	fes.loadOnesInSR(true);
	break;
      case RB_PixelRegisters:
	std::cout<<"Read Back Pixel Configuration"<<std::endl;
	fes.rdBackPixelCfg(result.PixCfgMap,ShiftRegLeft);	
	break;
      default:
	std::cout<<"Mode unknown"<<std::endl;
	break;
      }
  }
  
  else
    {
      std::cout<<"checking Global Config"<<std::endl;
      //ampsOn is ignored in the Fei4::checkGlobalConfig
      bool ampsOn   = false;
      bool sendBack = true;
      fes.checkGlobalConfig(ampsOn,sendBack,result.GRCfgMap);
    }
  
  //result.PixCfgMap = PixCfgMap;

    
  //for (std::map<uint32_t,std::vector<uint32_t> >::iterator it = result.GRCfgMap.begin(); it!=result.GRCfgMap.end(); ++it)
  //{
  //  for (std::vector<uint32_t>::iterator it2 = (it->second).begin(); it2!=(it->second).end();++it2)
  //	std::cout<<std::hex<<"0x"<<it->first<<" 0x"<<(*it2)<<std::dec<<std::endl;
  //}
  
  //Clean-up
  //Disable all Rx/Tx channels
  iblBocLink->disableRxChannels(m_modMask);
  iblBocLink->disableTxChannels(IblConnectivity::getTxMaskFromRxMask(m_modMask));
  //Enable only what was enabled before
  
  //Redundant
  //iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(enabledMask));
  //iblBocLink->enableRxChannels(enabledMask);
  
  //Restore the Serial port as it was before 
  //IblRod::Master::FeCmdMask0LsbReg::write(currentSerial0);
  //IblRod::Master::FeCmdMask1LsbReg::write(currentSerial1);
  //To set a mask we need to set 1 the bits 20 and the bits 2
  //IblRod::Master::RrifCtrlReg::write(RodControlRegister | 0x100004);
  //Unset Bit 20 and Bit 2
  //IblRod::Master::RrifCtrlReg::write(RodControlRegister & ~0x100004);
  
  //Restore the run mode for the FEs that were enabled before
  //fes_restore.setRunMode(true);
  
#endif

}


void ReadBackFei4Registers::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out,offset,m_modMask);
  Serializer<bool>::serialize(out,offset,m_clear);
  Serializer<bool>::serialize(out,offset,m_global);
  //useless to serialize uint8
  Serializer<uint8_t>::serialize(out,offset,m_DC);
  Serializer<uint8_t>::serialize(out,offset,m_CP);
  Serializer<ReadBackMode>::serialize(out,offset,rb_mode);
  Serializer<uint32_t>::serialize(out,offset,delay);
  //Serializer<int>::serialize(out,offset,ShiftRegLeft);
}

void ReadBackFei4Registers::deserialize(const uint8_t *in) {
  uint32_t offset =0;
  m_modMask = Serializer<uint32_t>::deserialize(in,offset);
  m_clear   = Serializer<bool>::deserialize(in,offset);
  m_global  = Serializer<bool>::deserialize(in,offset);
  //useless to deserialize uint8
  m_DC  = Serializer<uint8_t>::deserialize(in,offset);
  m_CP  = Serializer<uint8_t>::deserialize(in,offset);
  rb_mode   = Serializer<ReadBackMode>::deserialize(in,offset);
  delay     = Serializer<uint32_t>::deserialize(in,offset);
  //ShiftRegLeft = Serializer<int>::deserialize(in,offset);
  }

uint32_t ReadBackFei4Registers::serializedSize() const {
  return (sizeof(uint32_t) + 2*sizeof(bool) + 2*sizeof(uint8_t) + sizeof(ReadBackMode) + sizeof(uint32_t));
}


void ReadBackFei4RegistersResult::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<std::map<uint32_t,Fei4PixelCfg> >::serialize(out,offset,PixCfgMap);
  Serializer<std::map<uint32_t,std::vector<uint32_t > > >::serialize(out,offset,GRCfgMap);
  
}

void ReadBackFei4RegistersResult::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  PixCfgMap = Serializer<std::map<uint32_t,Fei4PixelCfg> >::deserialize(in,offset);
  GRCfgMap  = Serializer<std::map<uint32_t,std::vector<uint32_t > > >::deserialize(in,offset);
  
}

uint32_t ReadBackFei4RegistersResult::serializedSize() const {
  
  std::size_t sizeMap = Serializer<std::map<uint32_t,Fei4PixelCfg> >::size(PixCfgMap);
  std::size_t sizeGRMap = Serializer<std::map<uint32_t,std::vector<uint32_t > > >::size(GRCfgMap);
  return (sizeMap + sizeGRMap);
}
