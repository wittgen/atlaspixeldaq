#include "ReadRodSlaveBusyStatus.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#endif
#include "RodMasterRegisters.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void ReadRodSlaveBusyStatus::dump() {

  std::cout<<"I'm about to output the current"<<
    "value of registers on the ROD"<<std::endl;

  std::cout<<"slaveBusy Mask: "<<HEX(result.slaveBusyMaskValue)<<std::endl;
  std::cout<<"slaveBusy Force: "<<HEX(result.slaveBusyForceValue)<<std::endl;
  std::cout<<"slaveBusy Status: "<<HEX(result.slaveBusyCurrentStatusValue)<<std::endl;

  std::cout<<"slaveBusyOutHistValue: "<<HEX(result.slaveBusyOutHistValue)<<std::endl;
  
  std::cout<<"slaveBusySlink0LffHistValue: "<<HEX(result.slaveBusySlink0LffHistValue)<<std::endl;
  std::cout<<"slaveBusySlink0DownHistValue: "<<HEX(result.slaveBusySlink0DownHistValue)<<std::endl;
  std::cout<<"slaveBusySlink1LffHistValue: "<<HEX(result.slaveBusySlink1LffHistValue)<<std::endl;
  std::cout<<"slaveBusySlink1DownHistValue: "<<HEX(result.slaveBusySlink1DownHistValue)<<std::endl;
  std::cout<<"slaveBusyEfb1Stts0HistValue: "<<HEX(result.slaveBusyEfb1Stts0HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb1Stts1HistValue: "<<HEX(result.slaveBusyEfb1Stts1HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb2Stts0HistValue: "<<HEX(result.slaveBusyEfb2Stts0HistValue)<<std::endl;
  std::cout<<"slaveBusyEfb2Stts1HistValue: "<<HEX(result.slaveBusyEfb2Stts1HistValue)<<std::endl;
  std::cout<<"slaveBusyRouter0HistValue: "<<HEX(result.slaveBusyRouter0HistValue)<<std::endl;
  std::cout<<"slaveBusyRouter1HistValue: "<<HEX(result.slaveBusyRouter1HistValue)<<std::endl;

  std::cout<<"slaveBusyHistfull0HistValue: "<<HEX(result.slaveBusyHistfull0HistValue)<<std::endl;
  std::cout<<"slaveBusyHistfull1HistValue: "<<HEX(result.slaveBusyHistfull1HistValue)<<std::endl;
  std::cout<<"slaveBusyHistoverrun0HistValue: "<<HEX(result.slaveBusyHistoverrun0HistValue)<<std::endl;
  std::cout<<"slaveBusyHistoverrun1HistValue: "<<HEX(result.slaveBusyHistoverrun1HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt0ff0HistValue: "<<HEX(result.slaveBusyFmt0ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff1HistValue: "<<HEX(result.slaveBusyFmt0ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff2HistValue: "<<HEX(result.slaveBusyFmt0ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt0ff3HistValue: "<<HEX(result.slaveBusyFmt0ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt1ff0HistValue: "<<HEX(result.slaveBusyFmt1ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff1HistValue: "<<HEX(result.slaveBusyFmt1ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff2HistValue: "<<HEX(result.slaveBusyFmt1ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt1ff3HistValue: "<<HEX(result.slaveBusyFmt1ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt2ff0HistValue: "<<HEX(result.slaveBusyFmt2ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff1HistValue: "<<HEX(result.slaveBusyFmt2ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff2HistValue: "<<HEX(result.slaveBusyFmt2ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt2ff3HistValue: "<<HEX(result.slaveBusyFmt2ff3HistValue)<<std::endl;

  std::cout<<"slaveBusyFmt3ff0HistValue: "<<HEX(result.slaveBusyFmt3ff0HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff1HistValue: "<<HEX(result.slaveBusyFmt3ff1HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff2HistValue: "<<HEX(result.slaveBusyFmt3ff2HistValue)<<std::endl;
  std::cout<<"slaveBusyFmt3ff3HistValue: "<<HEX(result.slaveBusyFmt3ff3HistValue)<<std::endl;

}

ReadRodSlaveBusyStatus::ReadRodSlaveBusyStatus(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

uint32_t ReadRodSlaveBusyStatus::getBusyOutput() {
  // WARNING: not meaningfull if the result hasn't been populated!
  return (result.slaveBusyCurrentStatusValue & (~result.slaveBusyMaskValue)) | result.slaveBusyForceValue;
}

// inside execute() the actual meat of the command happens on the ppc
void ReadRodSlaveBusyStatus::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  unsigned value;

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);


  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::RegBase::read() ) << std::endl;
//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::CtlReg::read() ) << std::endl;
//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::StatReg::read() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::RegBase::read() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::CtlReg::read() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::StatReg::read() ) << std::endl;

//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::RegBase::addr() ) << std::endl;
//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::CtlReg::addr() ) << std::endl;
//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::StatReg::addr() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::RegBase::addr() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::CtlReg::addr() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::StatReg::addr() ) << std::endl;

//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::BusyMaskReg::addr() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::BusyMaskReg::addr() ) << std::endl;
//	std::cout << "Slave A values: " << HEX( IblRod::SlaveA::BusyForceReg::addr() ) << std::endl;
//	std::cout << "Slave B values: " << HEX( IblRod::SlaveB::BusyForceReg::addr() ) << std::endl;


//	std::cout << "Read direct address from slave: " << std::endl;
//	std::cout << "SlaveA address 0x0114: " << IblRod::SlaveA::Ram::read(0x0114) << std::endl;
//	std::cout << "SlaveB address 0x0114: " << IblRod::SlaveB::Ram::read(0x0114) << std::endl;

	if (!targetIsSlaveB){
		  // read the busy mask and force register for crosscheck
		  if(dumpOnPPC) std::cout << "Mask on slave A is: " << HEX( IblRod::SlaveA::BusyMaskReg::read() ) << std::endl;
		  value = IblRod::SlaveA::BusyMaskReg::read();
		  result.slaveBusyMaskValue=value;
		  if(dumpOnPPC) std::cout << "Force on slave A is: " << HEX( IblRod::SlaveA::BusyForceReg::read() ) << std::endl;
		  value = IblRod::SlaveA::BusyForceReg::read();
		  result.slaveBusyForceValue=value;
	
		  // read the busy current status ro register
		  value = IblRod::SlaveA::BusyStatusReg::read();
		  result.slaveBusyCurrentStatusValue=value;

		  // read the histograms
		  value = IblRod::SlaveA::BusyOutHist::read();
		  result.slaveBusyOutHistValue=value;
		  value = IblRod::SlaveA::BusySlink0LffHist::read();
		  result.slaveBusySlink0LffHistValue=value;
		  value = IblRod::SlaveA::BusySlink0DownHist::read();
		  result.slaveBusySlink0DownHistValue=value;
		  value = IblRod::SlaveA::BusySlink1LffHist::read();
		  result.slaveBusySlink1LffHistValue=value;
		  value = IblRod::SlaveA::BusySlink1DownHist::read();
		  result.slaveBusySlink1DownHistValue=value;
		  value = IblRod::SlaveA::BusyEfb1Stts0Hist::read();
		  result.slaveBusyEfb1Stts0HistValue=value;
		  value = IblRod::SlaveA::BusyEfb1Stts1Hist::read();
		  result.slaveBusyEfb1Stts1HistValue=value;
		  value = IblRod::SlaveA::BusyEfb2Stts0Hist::read();
		  result.slaveBusyEfb2Stts0HistValue=value;
		  value = IblRod::SlaveA::BusyEfb2Stts1Hist::read();
		  result.slaveBusyEfb2Stts1HistValue=value;
		  value = IblRod::SlaveA::BusyRouter0Hist::read();
		  result.slaveBusyRouter0HistValue=value;
		  value = IblRod::SlaveA::BusyRouter1Hist::read();
		  result.slaveBusyRouter1HistValue=value;

		  value = IblRod::SlaveA::BusyHistfull0Hist::read();
		  result.slaveBusyHistfull0HistValue=value;
		  value = IblRod::SlaveA::BusyHistfull1Hist::read();
		  result.slaveBusyHistfull1HistValue=value;
		  value = IblRod::SlaveA::BusyHistoverrun0Hist::read();
		  result.slaveBusyHistoverrun0HistValue=value;
		  value = IblRod::SlaveA::BusyHistoverrun1Hist::read();
		  result.slaveBusyHistoverrun1HistValue=value;

		  value = IblRod::SlaveA::BusyFmt0Ff0Hist::read();
		  result.slaveBusyFmt0ff0HistValue=value;
		  value = IblRod::SlaveA::BusyFmt0Ff1Hist::read();
		  result.slaveBusyFmt0ff1HistValue=value;
		  value = IblRod::SlaveA::BusyFmt0Ff2Hist::read();
		  result.slaveBusyFmt0ff2HistValue=value;
		  value = IblRod::SlaveA::BusyFmt0Ff3Hist::read();
		  result.slaveBusyFmt0ff3HistValue=value;

		  value = IblRod::SlaveA::BusyFmt1Ff0Hist::read();
		  result.slaveBusyFmt1ff0HistValue=value;
		  value = IblRod::SlaveA::BusyFmt1Ff1Hist::read();
		  result.slaveBusyFmt1ff1HistValue=value;
		  value = IblRod::SlaveA::BusyFmt1Ff2Hist::read();
		  result.slaveBusyFmt1ff2HistValue=value;
		  value = IblRod::SlaveA::BusyFmt1Ff3Hist::read();
		  result.slaveBusyFmt1ff3HistValue=value;

		  value = IblRod::SlaveA::BusyFmt2Ff0Hist::read();
		  result.slaveBusyFmt2ff0HistValue=value;
		  value = IblRod::SlaveA::BusyFmt2Ff1Hist::read();
		  result.slaveBusyFmt2ff1HistValue=value;
		  value = IblRod::SlaveA::BusyFmt2Ff2Hist::read();
		  result.slaveBusyFmt2ff2HistValue=value;
		  value = IblRod::SlaveA::BusyFmt2Ff3Hist::read();
		  result.slaveBusyFmt2ff3HistValue=value;

		  value = IblRod::SlaveA::BusyFmt3Ff0Hist::read();
		  result.slaveBusyFmt3ff0HistValue=value;
		  value = IblRod::SlaveA::BusyFmt3Ff1Hist::read();
		  result.slaveBusyFmt3ff1HistValue=value;
		  value = IblRod::SlaveA::BusyFmt3Ff2Hist::read();
		  result.slaveBusyFmt3ff2HistValue=value;
		  value = IblRod::SlaveA::BusyFmt3Ff3Hist::read();
		  result.slaveBusyFmt3ff3HistValue=value;

	}
	else{
		  // read the busy mask register for crosscheck
		  if(dumpOnPPC) std::cout << "Mask on slave B is: " << HEX( IblRod::SlaveB::BusyMaskReg::read() ) << std::endl;
		  value = IblRod::SlaveB::BusyMaskReg::read();
		  result.slaveBusyMaskValue=value;
		  if(dumpOnPPC) std::cout << "Force on slave B is: " << HEX( IblRod::SlaveB::BusyForceReg::read() ) << std::endl;
		  value = IblRod::SlaveB::BusyForceReg::read();
		  result.slaveBusyForceValue=value;
	
		  // read the busy current status ro register
		  value = IblRod::SlaveB::BusyStatusReg::read();
		  result.slaveBusyCurrentStatusValue=value;

		  // read the histograms
		  value = IblRod::SlaveB::BusyOutHist::read();
		  result.slaveBusyOutHistValue=value;
		  value = IblRod::SlaveB::BusySlink0LffHist::read();
		  result.slaveBusySlink0LffHistValue=value;
		  value = IblRod::SlaveB::BusySlink0DownHist::read();
		  result.slaveBusySlink0DownHistValue=value;
		  value = IblRod::SlaveB::BusySlink1LffHist::read();
		  result.slaveBusySlink1LffHistValue=value;
		  value = IblRod::SlaveB::BusySlink1DownHist::read();
		  result.slaveBusySlink1DownHistValue=value;
		  value = IblRod::SlaveB::BusyEfb1Stts0Hist::read();
		  result.slaveBusyEfb1Stts0HistValue=value;
		  value = IblRod::SlaveB::BusyEfb1Stts1Hist::read();
		  result.slaveBusyEfb1Stts1HistValue=value;
		  value = IblRod::SlaveB::BusyEfb2Stts0Hist::read();
		  result.slaveBusyEfb2Stts0HistValue=value;
		  value = IblRod::SlaveB::BusyEfb2Stts1Hist::read();
		  result.slaveBusyEfb2Stts1HistValue=value;
		  value = IblRod::SlaveB::BusyRouter0Hist::read();
		  result.slaveBusyRouter0HistValue=value;
		  value = IblRod::SlaveB::BusyRouter1Hist::read();
		  result.slaveBusyRouter1HistValue=value;

		  value = IblRod::SlaveB::BusyHistfull0Hist::read();
		  result.slaveBusyHistfull0HistValue=value;
		  value = IblRod::SlaveB::BusyHistfull1Hist::read();
		  result.slaveBusyHistfull1HistValue=value;
		  value = IblRod::SlaveB::BusyHistoverrun0Hist::read();
		  result.slaveBusyHistoverrun0HistValue=value;
		  value = IblRod::SlaveB::BusyHistoverrun1Hist::read();
		  result.slaveBusyHistoverrun1HistValue=value;

		  value = IblRod::SlaveB::BusyFmt0Ff0Hist::read();
		  result.slaveBusyFmt0ff0HistValue=value;
		  value = IblRod::SlaveB::BusyFmt0Ff1Hist::read();
		  result.slaveBusyFmt0ff1HistValue=value;
		  value = IblRod::SlaveB::BusyFmt0Ff2Hist::read();
		  result.slaveBusyFmt0ff2HistValue=value;
		  value = IblRod::SlaveB::BusyFmt0Ff3Hist::read();
		  result.slaveBusyFmt0ff3HistValue=value;

		  value = IblRod::SlaveB::BusyFmt1Ff0Hist::read();
		  result.slaveBusyFmt1ff0HistValue=value;
		  value = IblRod::SlaveB::BusyFmt1Ff1Hist::read();
		  result.slaveBusyFmt1ff1HistValue=value;
		  value = IblRod::SlaveB::BusyFmt1Ff2Hist::read();
		  result.slaveBusyFmt1ff2HistValue=value;
		  value = IblRod::SlaveB::BusyFmt1Ff3Hist::read();
		  result.slaveBusyFmt1ff3HistValue=value;

		  value = IblRod::SlaveB::BusyFmt2Ff0Hist::read();
		  result.slaveBusyFmt2ff0HistValue=value;
		  value = IblRod::SlaveB::BusyFmt2Ff1Hist::read();
		  result.slaveBusyFmt2ff1HistValue=value;
		  value = IblRod::SlaveB::BusyFmt2Ff2Hist::read();
		  result.slaveBusyFmt2ff2HistValue=value;
		  value = IblRod::SlaveB::BusyFmt2Ff3Hist::read();
		  result.slaveBusyFmt2ff3HistValue=value;

		  value = IblRod::SlaveB::BusyFmt3Ff0Hist::read();
		  result.slaveBusyFmt3ff0HistValue=value;
		  value = IblRod::SlaveB::BusyFmt3Ff1Hist::read();
		  result.slaveBusyFmt3ff1HistValue=value;
		  value = IblRod::SlaveB::BusyFmt3Ff2Hist::read();
		  result.slaveBusyFmt3ff2HistValue=value;
		  value = IblRod::SlaveB::BusyFmt3Ff3Hist::read();
		  result.slaveBusyFmt3ff3HistValue=value;

	}

  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void ReadRodSlaveBusyStatus::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
	Serializer<bool>::serialize(out,offset,targetIsSlaveB);
}

// Deseralize the data members of the command
// In this case, only one boolean
void ReadRodSlaveBusyStatus::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
	targetIsSlaveB = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t ReadRodSlaveBusyStatus::serializedSize() const
{
	return 2*sizeof(bool);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void ReadSlaveBusyStatusResult::serialize(uint8_t *out) const {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyForceValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyCurrentStatusValue);
	  
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyOutHistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusySlink0LffHistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusySlink0DownHistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusySlink1LffHistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusySlink1DownHistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyEfb1Stts0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyEfb1Stts1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyEfb2Stts0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyEfb2Stts1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyRouter0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyRouter1HistValue);

	  Serializer<uint32_t>::serialize(out, offset, slaveBusyHistfull0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyHistfull1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyHistoverrun0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyHistoverrun1HistValue);

	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt0ff0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt0ff1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt0ff2HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt0ff3HistValue);

	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt1ff0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt1ff1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt1ff2HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt1ff3HistValue);

	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt2ff0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt2ff1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt2ff2HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt2ff3HistValue);

	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt3ff0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt3ff1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt3ff2HistValue);
	  Serializer<uint32_t>::serialize(out, offset, slaveBusyFmt3ff3HistValue);

}


// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void ReadSlaveBusyStatusResult::deserialize(const uint8_t *in) {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  slaveBusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyForceValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);

	  slaveBusyOutHistValue = Serializer<uint32_t>::deserialize(in, offset);	  
	  slaveBusySlink0LffHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusySlink0DownHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusySlink1LffHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusySlink1DownHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyEfb1Stts0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyEfb1Stts1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyEfb2Stts0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyEfb2Stts1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyRouter0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyRouter1HistValue = Serializer<uint32_t>::deserialize(in, offset);

	  slaveBusyHistfull0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyHistfull1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyHistoverrun0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyHistoverrun1HistValue = Serializer<uint32_t>::deserialize(in, offset); 

	  slaveBusyFmt0ff0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt0ff1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt0ff2HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt0ff3HistValue = Serializer<uint32_t>::deserialize(in, offset);

	  slaveBusyFmt1ff0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt1ff1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt1ff2HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt1ff3HistValue = Serializer<uint32_t>::deserialize(in, offset);

	  slaveBusyFmt2ff0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt2ff1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt2ff2HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt2ff3HistValue = Serializer<uint32_t>::deserialize(in, offset);

	  slaveBusyFmt3ff0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt3ff1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt3ff2HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  slaveBusyFmt3ff3HistValue = Serializer<uint32_t>::deserialize(in, offset);

}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t ReadSlaveBusyStatusResult::serializedSize() const {
		//std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (34*sizeof(uint32_t));
}
