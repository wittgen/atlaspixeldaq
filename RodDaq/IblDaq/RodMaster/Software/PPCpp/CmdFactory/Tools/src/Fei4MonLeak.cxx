#include "Fei4MonLeak.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "Fei4Cmd.h"
#include "Fei4Proxy.h"
#include <iomanip>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <functional>
#endif

Fei4MonLeak::Fei4MonLeak() {

	setResultPtr();
	m_rxMask =0xFFFFFFFF;
	m_nPixPerFE =1;

}

void Fei4MonLeak::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei4()) return;

  uint32_t newRxMask = initMonLeak(m_rxMask);

  uint32_t maxPixPerFE = 26880;

   if(m_nPixPerFE > maxPixPerFE  ){
   m_nPixPerFE = maxPixPerFE;
   std::cout<<"You requested "<<(int)m_nPixPerFE<<" Pixel per FE but maximum number of Pixels per FE is "<<(int)maxPixPerFE<<" setting to this value "<<std::endl;
   }

  std::map< uint8_t, std::vector <uint32_t > > MonADC;
  getMonLeakADC(newRxMask, 0, 2 ,MonADC);//First reading is somehow wrong

  
  for(int index=0;index<m_nPixPerFE; index++){
   MonADC.clear();
   getMonLeakADC(newRxMask, index, 1 ,MonADC);

    for (std::map<uint8_t, std::vector <uint32_t > >::iterator it = MonADC.begin(); it!=MonADC.end(); ++it)result.MON_ILEAK[it->first].push_back(it->second);
  }
  
  cleanUp(newRxMask);

#endif

}

uint32_t Fei4MonLeak::initMonLeak(uint32_t rxMask){

uint32_t newRxMask=0;

#ifdef __XMK__

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->enableRxChannels(rxMask);
  newRxMask = iblBocLink->getEnChMask();

  Fei4Proxy fei4 = Fei4Proxy(newRxMask);

  fei4.setRunMode(false);
  //fei4.sendGlobalConfigAmpsOn();
  //fei4.sendPixelConfig();
 /* 
  Mask dc_mask_high;dc_mask_high.set(MASK_FULL);//All 1
  Mask dc_mask_low;dc_mask_low.set(MASK_CLEAR);//All 0

  for(unsigned iDC=0;iDC<40;iDC++){
      fei4.strobePixelLatches(dc_mask_low, 0x1E00, iDC); //set FDAC to 0
      fei4.strobePixelLatches(dc_mask_high, 0x3E, iDC); //set TDAC to 1
  }

  fei4.writeRegister(&Fei4::PrmpVbpf,1);//set IF to 1 
*/
#endif

return newRxMask;

}

void Fei4MonLeak::getMonLeakADC(uint32_t rxMask, uint16_t pixelIndex, uint32_t nIter, std::map < uint8_t, std::vector < uint32_t > > &MonADC){

#ifdef __XMK__
 Fei4Proxy fei4 = Fei4Proxy(rxMask);
 Mask dc_mask;
 uint32_t bitMask [DoubleColumnBit::nWords];
 int iPix=0;
 fei4.writeRegister(&Fei4::CP,0x0); // set DC mode to one DC  0x24 -25 DC?
 fei4.writeRegister(&Fei4::GADCSel, 7);// GADCSelect to MonLeak
 fei4.writeRegister(&Fei4::ILR,0); //ILR=1 for 10kOhm, ILR=0 for 90kOhm ILeak Hitbus 

 for(unsigned iDC=0;iDC<40;iDC++){
   for(unsigned j=0;j<DoubleColumnBit::nWords;j++){
        if(pixelIndex/32 == iPix){
	  bitMask[j] = ((1<<pixelIndex%32));
	   if(pixelIndex%100 == 0)std::cout<<"Scanning Pixel "<<pixelIndex<<" DC "<<(int)iDC <<" nWord "<<(int)j <<" mask "<<std::hex<<bitMask[j]<<std::dec<<std::endl;
	   } else{
	    bitMask[j] = 0x0;
	   }
   iPix++;
    }
   dc_mask.set(bitMask);
   fei4.strobePixelLatches(dc_mask, 0x100, iDC); //write pixel registers
 }

  //Copy from Latches to SR
  fei4.globalPulse(10);

  Fei4 *fes = RodResourceManagerIBL::getCalibModuleConfigs();

 for(uint32_t i=0;i<nIter;i++){
   for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++){
       uint8_t rxCh = fes[i].getRxCh();
        if ( !(rxMask & (1<<rxCh) ) )continue;
         fes[i].writeRegister(&Fei4::ADC,1);
         //fes[i].clearFullShiftRegister();
         fes[i].globalPulse(63);
         uint16_t val = fes[i].getRegisterValueFromFE(40);
         if(val == 0xdead)MonADC[rxCh].push_back(0xFFFF);
         else MonADC[rxCh].push_back(val >> 4);
         fes[i].writeRegister(&Fei4::ADC,0);
      }
 }

#endif

}

void Fei4MonLeak::cleanUp( uint32_t rxMask){

#ifdef __XMK__  
  Fei4Proxy fei4 = Fei4Proxy(rxMask);
  //fei4.sendGlobalConfigAmpsOff();
  //fei4.sendPixelConfig();

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->disableRxChannels(rxMask);

#endif

}

void Fei4MonLeak::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset,  m_rxMask);
	Serializer<uint16_t>::serialize(out, offset,  m_nPixPerFE);
}

void Fei4MonLeak::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
	m_nPixPerFE = Serializer<uint16_t>::deserialize(in, offset);
}

uint32_t Fei4MonLeak::serializedSize() const
{
	return ( sizeof(uint32_t)+sizeof(uint16_t) );
}

void Fei4MonLeakResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::serialize(out,offset,MON_ILEAK);
}

void Fei4MonLeakResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
        MON_ILEAK = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::deserialize(in,offset);

}

uint32_t Fei4MonLeakResults::serializedSize() const
{
	std::size_t sizeMap = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::size(MON_ILEAK);
	return sizeMap;
}

