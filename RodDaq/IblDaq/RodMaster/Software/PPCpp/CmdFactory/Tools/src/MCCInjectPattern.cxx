#include "MCCInjectPattern.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "Serializer.h"
#include "BitUtils.h"
#endif

MCCInjectPattern::MCCInjectPattern() {
  setResultPtr();
  m_verbosity = 0;
  m_pattern = 0;
  m_nPattern = 0;
  m_rxMask = 0;
}

void MCCInjectPattern::execute() {
  std::cout << "in function: " << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__
  
  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

  int8_t fibreNr = IblBoc::getMasterFibreFromRxCh(19);
  IblBoc::PixSetRxThreshold(fibreNr, 30000);
  IblBoc::PixSetRxRGS(fibreNr%12, 65500);
  IblBoc::SetRxSampleDelay( fibreNr, 4, 10);

  std::cout << "Sending test patterns" << std::endl;
  for (uint8_t rxCh = 0; rxCh < 32; rxCh++) {
    if ( (m_rxMask & (1 << rxCh)) == 0 ) continue;
    std::cout << "rxCh=" << static_cast<int>(rxCh) << std::endl;
    fes[0].setTxCh(rxCh);
    fes[0].setRxCh(rxCh);
    fes[0].mccGlobalResetMCC();
    fes[0].writeMCCRegister(CSR, 0x0040); // write to CSR to enable play back
    fes[0].addPattern(m_pattern, m_nPattern); // write m_pattern m_nPattern times to all receivers
    std::vector<uint32_t> data; data.push_back(0x87800000);
    fes[0].mccWrReceiver(data); // write EOE to all receivers
    fes[0].mccEnDataTake(); // enable data taking
    fes[0].sendLV1(); // trigger and start event builder
  }

  // Warning, this will remove entries from the RX FIFO on the BOC
  if( m_verbosity > 10 ) {

    for (uint8_t rxCh = 0; rxCh < 32; rxCh++) {
      if ( (m_rxMask & (1 << rxCh)) == 0 ) continue;
      IblBocRxFifo rxFifo;
      rxFifo.setRxCh(rxCh);

      std::vector<uint8_t> rxData;
      rxData.reserve(100);

      for(size_t k = 0 ; k < 100 ; ++k )
        rxData.push_back( rxFifo.read() );

      std::vector<uint32_t> rxData32(25);
      for(size_t k = 0 ; k < 100 ; ++k ) {
        uint8_t shift = 24 - ((k%4)*8);
        rxData32[k/4] |= (rxData[k] << shift);
      }

      dumpData( &rxData32[0], 25 );
    }
  }

#endif 
}

void MCCInjectPattern::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_verbosity);
  Serializer<uint32_t>::serialize(out, offset, m_pattern);
  Serializer<uint32_t>::serialize(out, offset, m_nPattern);
  Serializer<uint32_t>::serialize(out, offset, m_rxMask);
}

void MCCInjectPattern::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_verbosity = Serializer<uint32_t>::deserialize(in, offset);
  m_pattern = Serializer<uint32_t>::deserialize(in, offset);
  m_nPattern = Serializer<uint32_t>::deserialize(in, offset);
  m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t MCCInjectPattern::serializedSize() const {
  return (4 * sizeof(uint32_t));
}
