#include "DataTakingConfig.h"
#include "Fei4Proxy.h"
#include "DataTakingTools.h"
#include "IblConnectivity.h"
#include "BarrelConnectivity.h"
#include "DumpRodRegisters.h"
#include "Fei3ModProxy.h"
#ifdef __XMK__
#include "IblBoc.h"
#include "IblRodSlave.h"
#include "RodResourceManager.h"
#endif

DataTakingConfig::DataTakingConfig() {
  setResultPtr();
  channels = 0xFFFFFFFF;
  rodID = 0x1400000;
  nHitsPerTrigger = 5;
  fmtReadoutTimeoutLimit = 0x801;
  frameCount = 16;
  trigLatency = 255 - 8 - frameCount;
  bcidOffset = 0;
  slinkMode = SlinkIgnoreFTK;
  bocFeEmu = false;
  rodFeEmu = false;
  initRealFE = true;
  rolEmu = false;
  physicsMode = true;
  doECRReset = false;
  feSynchAtECR = false;
  fmtHeaderTrailerLimit = 0x801;
  fmtRodBusyLimit = 0x3c1;
  efbMaskValue = 0x0;
  fmtDataOverflowLimit = 0x800;
  fmtTrailerTimeoutLimit = 0xf50;
  vetoAtECR=0x1fa4;     //8100 ==> 810 microS
  busyAtECR=0x2328;     //9000 ==> 900 microS
  resetAfterECR=0x1f40; //8000 ==> 800 microS
  pixSpeed = IblBoc::Speed_40;
  fmtDataOverflowLimit = 0x801;
}

void  DataTakingConfig::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  
#ifdef __XMK__
	
  printOut();
  
  // clear some registers
  initialize();

  // set subdetector id
  setRodID(rodID);
	
  // set serial port for sending commands to FE
  SerialPort* sPort = RodResourceManagerIBL::getSerialPort();
  if( RodResMgr::isFei4() )
    sPort->setTxMask(IblConnectivity::getTxMaskFromRxMask(channels));
  else
    sPort->setTxMask(BarrelConnectivity::getTxMaskFromRxMask(channels));

  initBocEmu(bocFeEmu, channels, frameCount, pixSpeed, nHitsPerTrigger);
  if(!bocFeEmu)initFrontEnd(channels, frameCount, trigLatency, pixSpeed);

  std::cout << "New enabled channel mask = 0x" << std::hex << (int)channels << std::dec << std::endl;

  // initialize slaves with their IDs
  initSlaves();

  // set bcid offset 
  DataTakingTools::setBcidOffset(bcidOffset);

  // enable slinks
  DataTakingTools::sLinkEnable(slinkMode);

  // set Read Out Link into appropriate mode
  setRoLMode(rolEmu);
	
  // set rrif control register and spr reg to physics or calibration mode
  setPhysicsMode(physicsMode);
	
  // set fmt readout timeout limit
  setFmtReadoutTimeoutLimit(fmtReadoutTimeoutLimit);

  // set fmt header trailer limit
  setFmtHeaderTrailerLimit(fmtHeaderTrailerLimit);

  // set fmt rod busy limit
  setFmtRodBusyLimit(fmtRodBusyLimit);

  // set ROD FmtDataOverFlow Limit ==> cut onevent size
  DataTakingTools::setFmtDataOverflowLimit(fmtDataOverflowLimit);

  // set ROD FmtTrailerTimeoutLimit ==> time waiting for a trailer...
  DataTakingTools::setFmtTrailerTimeoutLimit(fmtTrailerTimeoutLimit);

  // set ROD Veto time @ ECR; register with 100 ns granularity
  DataTakingTools::setRODVetoTimeAtECR(vetoAtECR);

  // set ROD Busy time @ ECR; register with 100 ns granularity
  DataTakingTools::setRODBusyTimeAtECR(busyAtECR);

  // set ROD Reset time after ECR; register with 100 ns granularity
  DataTakingTools::setRODResetTimeAfterECR(resetAfterECR);

  // set ROD frame count
  setRODFrameCount(frameCount);
	
  // set hit disc config register for true tot
  if(RodResMgr::isFei4())DataTakingTools::writeHitDiscConfig(channels);

  // reset ECR counter
  if (doECRReset) DataTakingTools::setECRCounter(0);

  if( RodResMgr::isFei3() ) {
    
    //Set ROD Slave speed on both slaves
    IblRodSlave::setFmtFei3Reg(pixSpeed);

    //Set BMF speed on both bridges
    IblBoc::PixSetSpeed(pixSpeed);

    //Activate FESync upon request
    uint32_t sprCtrl = IblRod::Master::SprCtrlReg::read();
    if(feSynchAtECR) IblRod::Master::SprCtrlReg::write( sprCtrl | 0x400000 );//Activate FESync
    else IblRod::Master::SprCtrlReg::write( sprCtrl & 0xFFBFFFFF );//De-activate FESync
    
  }

  // enable BOC-->ROD formatter links (set the Link Enable registers on the ROD side)
  DataTakingTools::enableFmtLink(channels);

  // dump register values!
  std::cout<<std::endl;
  std::cout<<" ===================================================================="<<std::endl;
  std::cout<<" After configuration, the ROD and BOC registers are read below: "<<std::endl;
  std::cout<<" ===================================================================="<<std::endl;
  DumpRodRegisters::dump();
	
#endif
}

void DataTakingConfig::initialize() {
  
  IblRod::Master::RrifCtrlReg::write(0x80000000);
  std::cout << "SlaveA and SlaveB setting FmtLinkEnable to 0" << std::endl;
  IblRod::SlaveA::FmtLinkEnable::write(0x00000000);
  IblRod::SlaveB::FmtLinkEnable::write(0x00000000);
    
}

void DataTakingConfig::setRodID(uint32_t rodID) {

  uint8_t detId = (rodID & 0xFF0000) >> 16;
  if( detId != 0x14 && detId != 0x15 && detId != 0x11 )
    std::cout<<"WARNING!!! You have sent a subdetector ID that is not 0x14 (IBL), 0x15 (DBM), 0x11 (L1/L2). You sent ID: 0x"<<std::hex<<(int)(rodID)<<std::dec<<std::endl;

  uint32_t id = (rodID & 0x00FFFFFF);
  if ( (detId == 0x14 || detId == 0x15) && ((rodID & 0xF) != (0x0)) ) {
    std::cout<<"WARNING!!! You have sent a ROD ID that does not end in 0x0. You sent ID: 0x"<<std::hex<<(int)(rodID)<<std::dec<<std::endl;
    id = (rodID & 0x00FFFFF0);
  }
  
  //  printf("Setting ROD / Source / Subdetector ID to 0x014%04h",(int)id);
  std::cout<<"Setting ROD / Source / Subdetector ID to: 0x"<<std::hex<<id<<std::dec<<std::endl;
  
  std::cout<<std::endl;
  IblRod::SlaveA::SourceId::write(id);
  IblRod::SlaveB::SourceId::write(id);
  std::cout<<std::endl;
}

void DataTakingConfig::initRodFeEmu(uint32_t hits, uint8_t frameCount, IblBoc::PixSpeed speed) {
#ifdef __XMK__

  // turn on the global emulator
  DataTakingTools::setRodEmu(true, frameCount, speed);

  for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
    std::cout << "Configuring ROD FE emulator for Slave " << (int)slaveId << std::endl;
    for(uint8_t ch = 0 ; ch < 16 ; ++ch) {
      std::cout<<" for slave: "<<(int)slaveId<<" initializing ch: "<<(int)ch<<" with "<<(int)hits<<" per trigger"<<std::endl;
      // Enabling channel
      IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x23);
      IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x1);

    }
    
    if( RodResMgr::isFei4() ) {
    	std::cout << "ROD Emulator not available on IBL" << std::endl;
    } else {
      // Setting hit count per trigger
      IblRodSlave::MCCEmu::setHitCount(slaveId, hits);
    }
  }
#endif
}

void DataTakingConfig::initSlaves() {
  IblRod::SlaveA::SlaveId::write(0x0);
  IblRod::SlaveB::SlaveId::write(0x1);
}

void DataTakingConfig::setRoLMode(bool emu) {
  if (emu) IblRod::Master::DebugModeCnfgReg::write( 0x42000000 ); // ROL test mode active / used by DAvide also in calib mode 
  else IblRod::Master::DebugModeCnfgReg::write( 0x02000000 ); // ROL test mode NOT active - for FE Emulator in the BOC or Data coming from the Fe	
}

void DataTakingConfig::setPhysicsMode(bool physics) {
  // Todo: move appropriately
#define RRIF_CTL_TIM_TRIG  0x88001527 //physics run
//#define RRIF_CTL_TIM_TRIG  0x88101527 //physics run...setting used by Gabriele for L2...why it is different?
#define RRIF_CTL_PPC_TRIG  0x80041425 //calibration run
  
  // trigger source
  if (physics) IblRod::Master::RrifCtrlReg::write( RRIF_CTL_TIM_TRIG );//physics run
  else IblRod::Master::RrifCtrlReg::write( RRIF_CTL_PPC_TRIG );//calibration run
  
  // spare register controls different things in different bits
  // bit 11 control activation of LV1s 
  if (physics) {
    IblRod::Master::SprCtrlReg::write( 0x0FF ); // reset trigger counter
    IblRod::Master::SprCtrlReg::write( 0x8FF );
	IblRod::SlaveA::CalibrationMode::write(0x0);
	IblRod::SlaveB::CalibrationMode::write(0x0);
  }
  else {
	IblRod::Master::SprCtrlReg::write( 0x0FF );
	IblRod::SlaveA::CalibrationMode::write(0x1);
	IblRod::SlaveB::CalibrationMode::write(0x1);
  }

}

void DataTakingConfig::setFmtReadoutTimeoutLimit(uint32_t limit) {
  IblRod::SlaveA::FmtReadoutTimeoutLimit::write(limit);
  IblRod::SlaveB::FmtReadoutTimeoutLimit::write(limit);
}

void DataTakingConfig::setFmtHeaderTrailerLimit(uint32_t limit) {
  IblRod::SlaveA::FmtHeaderTrailerLimit::write(limit);
  IblRod::SlaveB::FmtHeaderTrailerLimit::write(limit);
}

void DataTakingConfig::setFmtRodBusyLimit(uint32_t limit) {
  IblRod::SlaveA::FmtRodBusyLimit::write(limit);
  IblRod::SlaveB::FmtRodBusyLimit::write(limit);
}

void DataTakingConfig::setFmtDataOverflowLimit(uint32_t limit) {
  IblRod::SlaveA::FmtDataOverflowLimit::write(limit);
  IblRod::SlaveB::FmtDataOverflowLimit::write(limit);
}

void DataTakingConfig::setRODFrameCount(uint32_t count) {
  IblRod::SlaveA::TriggerCountNumber::write(count);
  IblRod::SlaveB::TriggerCountNumber::write(count);
}

void DataTakingConfig::initBocEmu(bool bocEmu, uint32_t &channels, uint8_t frameCount, IblBoc::PixSpeed speed, uint32_t hits, EmuUtils::OCC_MODE occMode){

#ifdef __XMK__
  DataTakingTools::setBocEmu(bocEmu, channels, frameCount, speed, hits,occMode);

  if(bocEmu){
    IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink();
      if(RodResMgr::isFei4()){
        iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(channels));//Needed to mute the TX
        Fei4Proxy fei4Proxy = Fei4Proxy(0xFFFFFFFF);//Needed in IBL emulator TX is already muted
        fei4Proxy.sendGlobalConfigAmpsOff(false);
        fei4Proxy.writeRegister(&Fei4::Trig_Count, frameCount);
        fei4Proxy.setRunMode(true);
      }
   
      if( !iblBocLink->enableChannels(channels) ) {
        std::cout<<"WARNING: could not enable all the desired channels, New enabled channel mask = 0x"<<std::hex<<(int)channels<<std::dec<<std::endl;
     }
  }

#endif

}

void DataTakingConfig::initFrontEnd(uint32_t &channels, uint8_t frameCount, uint8_t trigLatency, IblBoc::PixSpeed speed) {
#ifdef __XMK__
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 

    if( !iblBocLink->enableChannels(channels) ) {
      std::cout<<"WARNING: could not enable all the desired channels. Will continue data taking anyway"<<std::endl;
      channels = iblBocLink->getEnChMask();
      std::cout<<"New enabled channel mask = 0x"<<std::hex<<(int)channels<<std::dec<<std::endl;
    }
    
  if( RodResMgr::isFei4()  ) { //Always init even in emulator
    Fei4Proxy fei4 = Fei4Proxy(channels);
    fei4.setRunMode(false);
    fei4.writeRegister(&Fei4::Trig_Count, frameCount);
    fei4.writeRegister(&Fei4::Trig_Lat, trigLatency);
    fei4.setRunMode(true);
  } else {
    // Using proxy because it updates the local values of the module registers (needed for reconfiguration)
    uint32_t modMask = BarrelConnectivity::getModMaskFromRxMask(channels);
    Fei3ModProxy fei3Mod = Fei3ModProxy(modMask);
    fei3Mod.writeMCCRegister(LV1, (frameCount-1)<<8);
    PixLib::EnumMccBandwidth::MccBandwidth mccBandwidth = IblBoc::PixGetMccBandwidthFromPixSpeed(speed);
    fei3Mod.writeMCCSpeed((uint8_t)mccBandwidth );
    fei3Mod.writeGlobalReg(Fei3::GlobReg::Latency, trigLatency);//Set latency
    fei3Mod.sendGlobalConfig();//Needed to propagate the latency
    fei3Mod.mccEnDataTake();
  }

#endif
}

void DataTakingConfig::printOut() {
  std::cout<<std::endl;
  std::cout<<"===================================================================="<<std::endl;
  std::cout<<"You are requesting data taking configuration with the following parameters:"<<std::endl;
  std::cout<<"===================================================================="<<std::endl;
  std::cout<<std::endl;
  std::cout<<"The channels to enable are: 0x"<<std::hex<<(int)channels<<std::dec<<std::endl;
  std::cout<<"The ROD id is: 0x"<<std::hex<<(int)rodID<<std::dec<<std::endl;
  std::cout<<"The fmt Readout Timeout Limit is: 0x"<<std::hex<<(int)fmtReadoutTimeoutLimit<<std::dec<<std::endl;
  std::cout<<"The number of frames per trigger is: "<<(int)frameCount<<std::endl;
  std::cout<<"The trigger latency is: "<<(int)trigLatency<<std::endl;
  std::cout<<"The BCID offset is: "<<(int)bcidOffset<<std::endl;
  std::cout<<"The slink enable mode is: "<<(int)slinkMode<<std::endl;
  std::cout<< ((bocFeEmu) ? "Use the BOC FE emulator" : "Do not use the BOC FE emulator") << std::endl;
  std::cout<< ((initRealFE) ? "Initialize the FE" : "Do not initialize the FE") << std::endl;
  std::cout<< ((rolEmu) ? "Use the ROL emulator" : "Do not use the ROL emulator") << std::endl;
  std::cout<< ((physicsMode) ? "Set the ROD in physics mode" : "Set the ROD in calibration mode") << std::endl;
  std::cout<< ((doECRReset) ? "Do an ECR reset" : "Don't do an ECR reset") << std::endl;
  std::cout<<std::endl;
  std::cout<<"===================================================================="<<std::endl;
  std::cout<<std::endl;
}
