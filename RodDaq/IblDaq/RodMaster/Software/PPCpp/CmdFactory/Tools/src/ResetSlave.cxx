#include "ResetSlave.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

ResetSlave::ResetSlave(){ 
  setResultPtr(); 
  resetHisto = true;
  resetNetwork = true;
}

void ResetSlave::execute() {
#ifdef __XMK__
  IblRodSlave s;
  int i = 0;
  int j = 2;
  if (whichSlave == 1) i = 1;
  if (whichSlave == 0) j = 1;

  for ( ; i<j; i++) {
    std::cout<<"resetting slave "<<(int)i<<std::endl;
    if (resetHisto) s.resetHisto(i);
    if (resetNetwork) s.resetNetwork(i);
  }

#endif
}

