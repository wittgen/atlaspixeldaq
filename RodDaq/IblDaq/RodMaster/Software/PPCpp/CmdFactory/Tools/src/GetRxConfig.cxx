/*
 * * F. Meloni <federico.meloni@cern.ch>
 * Set Rx Threshold and Delay (and Gain)
 */

#include <stdlib.h>
#include <iomanip>
#include "GetRxConfig.h"
#include "IblBoc.h"

void GetRxConfig::execute() {
#ifdef __XMK__
	
	//Determine fibre and plugin ID, given the Rx channel
        uint8_t fibre = IblBoc::getMasterFibreFromRxCh(rxCh);
        uint8_t pluginId = (fibre / 12);
	std::cout << "Fibre: " << (int)fibre << " PluginId: " << (int)pluginId  << std::endl;
		
	uint8_t coarseDelay_test = 0;
	uint8_t fineDelay_test = 0;
	
	IblBoc::GetRxSampleDelay(fibre, coarseDelay_test, fineDelay_test);
	std::cout << "Register readback - Coarse: " << (int)coarseDelay_test << " Fine: " << (int)fineDelay_test  << std::endl;	  		
	result.coarseDelay = coarseDelay_test;
	result.fineDelay = fineDelay_test;	

	//Not implemented yet
	result.rxThreshold = IblBoc::GetRxThreshold(fibre);
	result.rxGain = IblBoc::GetRxRGS(pluginId);
	std::cout << "Register readback - Threshold: " << (int)result.rxThreshold << " Gain: " << (int)result.rxGain  << std::endl;	  		
	std::cout << "All done!" << std::endl;	
#endif
}

void GetRxConfig::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<int32_t>::serialize(out, offset, rxCh);
}

void GetRxConfig::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<int32_t>::deserialize(in, offset, rxCh);
}

uint32_t GetRxConfig::serializedSize() const
{
	return sizeof(uint32_t);
}

void GetRxConfigResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint8_t>::serialize(out, offset, coarseDelay);
	Serializer<uint8_t>::serialize(out, offset, fineDelay);	
	Serializer<uint16_t>::serialize(out, offset, rxThreshold);
	Serializer<uint16_t>::serialize(out, offset, rxGain);	
}

void GetRxConfigResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<uint8_t>::deserialize(in, offset, coarseDelay);
	Serializer<uint8_t>::deserialize(in, offset, fineDelay);
	Serializer<uint16_t>::deserialize(in, offset, rxThreshold);
	Serializer<uint16_t>::deserialize(in, offset, rxGain);	
}

uint32_t GetRxConfigResults::serializedSize() const
{
	return 2 * sizeof(uint8_t) + 2 * sizeof(uint16_t) ;
}
