#include "GetGADCOutput.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "Fei4Cmd.h"
#include "Fei4Proxy.h"
#include <iomanip>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <functional>
#endif

GetGADCOutput::GetGADCOutput() {

	setResultPtr();
	m_rxMask =0xFFFFFFFF;
	m_GADCSel = TEMP;
	m_GADCVref = -1;

}

void GetGADCOutput::execute() {

#ifdef __XMK__

if(!RodResMgr::isFei4()) return;

  std::cout<<"Selected GADC to measure "<<(int)m_GADCSel<<" --> "<<GADCSelectNames[m_GADCSel]<<std::endl;
  
  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->enableRxChannels(m_rxMask);
  uint32_t rxMask = iblBocLink->getEnChMask();
  
  Fei4Proxy fei4 = Fei4Proxy(rxMask);
  
  fei4.setRunMode(false);

    if(m_GADCVref>=0 && m_GADCVref <256){
      fei4.writeRegister(&Fei4::GADCVref,m_GADCVref);
      std::cout<<"Using GADCVref "<<(int)m_GADCVref<<std::endl;
    } else {
      std::cout<<"GADCVref "<<(int)m_GADCVref<<" out of range, using value on DB"<<std::endl;
    }

  std::map< uint8_t, std::vector <uint32_t > > gADC = getGADC (rxMask, m_GADCSel );

    for (std::map<uint8_t, std::vector <uint32_t > >::iterator it = gADC.begin(); it!=gADC.end(); ++it)result.GADC[it->first].push_back(it->second);

  iblBocLink->disableRxChannels(rxMask);
  
#endif

}

std::map < uint8_t, std::vector < uint32_t > > GetGADCOutput::getGADC(uint32_t rxMask, GADCSelect select){

std::map<uint8_t, std::vector <uint32_t > >GADC_VAL;

#ifdef __XMK__

Fei4Proxy fei4 = Fei4Proxy(rxMask);

//fei4.globalPulse(10);
fei4.writeRegister(&Fei4::GADCSel, (int)select);

  switch(select){
    case (TEMP):
     fei4.writeRegister(&Fei4::TempSensIbias,75);
     fei4.writeRegister(&Fei4::TMD,0);
     fei4.writeRegister(&Fei4::TM0,0); 
     fei4.writeRegister(&Fei4::TM1,1); //change to 2 and 3 for calibration, TM0 bit 0
    break;
    default:
    std::cout<<"No action for GADCSelect "<<GADCSelectNames[select]<<std::endl;
  }

  Fei4 *fes = RodResourceManagerIBL::getCalibModuleConfigs();

  if(select == DAC_PULSER){
    for ( int vcal=0;vcal<1024;vcal++ ){
      for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++){
       uint8_t rxCh = fes[i].getRxCh();
        if ( !(rxMask & (1<<rxCh) ) )continue;
         fes[i].writeRegister(&Fei4::PlsrDAC,vcal);
         fes[i].writeRegister(&Fei4::ADC,1);
         fes[i].clearFullShiftRegister();
         fes[i].globalPulse(63);
         uint16_t val = fes[i].getRegisterValueFromFE(40);
         if(val == 0xdead)GADC_VAL[rxCh].push_back(0xFFFF);
         else GADC_VAL[rxCh].push_back(val >> 4);
         //std::cout<<"Rx "<<(int)rxCh<<" DAC "<<vcal<<" GADC "<<(int)(val >> 4)<<" "<<val<<std::endl;
         fes[i].writeRegister(&Fei4::ADC,0);
      }
    }
  } else if(select ==ANALOG_VOL){
     for ( int vrefAn=0;vrefAn<255;vrefAn++ ){
      for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++){
       uint8_t rxCh = fes[i].getRxCh();
        if ( !(rxMask & (1<<rxCh) ) )continue;
         fes[i].writeRegister(&Fei4::VrefAnTune,vrefAn);
         fes[i].writeRegister(&Fei4::ADC,1);
         //fes[i].clearFullShiftRegister();
         fes[i].globalPulse(63);
         uint16_t val = fes[i].getRegisterValueFromFE(40);
         if(val == 0xdead)GADC_VAL[rxCh].push_back(0xFFFF);
         else GADC_VAL[rxCh].push_back(val >> 4);
         //std::cout<<"Rx "<<(int)rxCh<<" DAC "<<vrefAn<<" GADC "<<(int)(val >> 4)<<" "<<val<<std::endl;
         fes[i].writeRegister(&Fei4::ADC,0);
      }
    }
  } else if(select == TEMP){
  
  for ( uint32_t TM1=2;TM1<4;TM1++ ){
      for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++){
       uint8_t rxCh = fes[i].getRxCh();
        if ( !(rxMask & (1<<rxCh) ) )continue;
         fei4.writeRegister(&Fei4::TM1,TM1); //change to 2 and 3 for calibration, TM0 bit 0
         fes[i].writeRegister(&Fei4::ADC,1);
         //fes[i].clearFullShiftRegister();
         fes[i].globalPulse(63);
         uint16_t val = fes[i].getRegisterValueFromFE(40);
         if(val == 0xdead)GADC_VAL[rxCh].push_back(0xFFFF);
         else GADC_VAL[rxCh].push_back(val >> 4);
         //std::cout<<"Rx "<<(int)rxCh<<" DAC "<<vrefAn<<" GADC "<<(int)(val >> 4)<<" "<<val<<std::endl;
         fes[i].writeRegister(&Fei4::ADC,0);
      }
    }
  } else {//Add more else if needed
      for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++){
       uint8_t rxCh = fes[i].getRxCh();
        if ( !(rxMask & (1<<rxCh) ) )continue;
         fes[i].writeRegister(&Fei4::ADC,1);
         //fes[i].clearFullShiftRegister();
         fes[i].globalPulse(63);
         uint16_t val = fes[i].getRegisterValueFromFE(40);
         if(val == 0xdead)GADC_VAL[rxCh].push_back(0xFFFF);
         else GADC_VAL[rxCh].push_back(val >> 4);
         //std::cout<<"Rx "<<(int)rxCh<<" GADC "<<(int)(val >> 4)<<" "<<val<<std::endl;
         fes[i].writeRegister(&Fei4::ADC,0);
      }
  }

#endif

return GADC_VAL;

}

void GetGADCOutput::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset,  m_rxMask);
	Serializer<uint32_t>::serialize(out, offset,  m_GADCVref);
	Serializer<GADCSelect>::serialize(out, offset,  m_GADCSel);
}

void GetGADCOutput::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_rxMask = Serializer<uint32_t>::deserialize(in, offset);
	m_GADCVref = Serializer<uint32_t>::deserialize(in, offset);
	m_GADCSel = Serializer<GADCSelect>::deserialize(in, offset);
}

uint32_t GetGADCOutput::serializedSize() const
{
	return ( 2*sizeof(uint32_t)+sizeof(GADCSelect) );
}

void GetGADCOutputResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::serialize(out,offset,GADC);
}

void GetGADCOutputResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
        GADC = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::deserialize(in,offset);
}

uint32_t GetGADCOutputResults::serializedSize() const
{
	std::size_t sizeMap = Serializer<std::map <uint8_t, std::vector<std::vector <uint32_t > > > >::size(GADC);
	return sizeMap;
}

