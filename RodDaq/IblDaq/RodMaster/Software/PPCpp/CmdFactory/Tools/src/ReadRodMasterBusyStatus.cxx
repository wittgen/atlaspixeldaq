#include "ReadRodMasterBusyStatus.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void ReadRodMasterBusyStatus::dump() {

  std::cout<<"I'm about to output the current"<<
    "value of registers on the ROD"<<std::endl;

  std::cout<<"masterBusy Mask: "<<HEX(result.masterBusyMaskValue)<<std::endl;
  std::cout<<"masterBusy Status: "<<HEX(result.masterBusyCurrentStatusValue)<<std::endl;

  std::cout<<"masterBusyOutHistValue: "<<HEX(result.masterBusyOutHistValue)<<std::endl;
  std::cout<<"masterBusyRodBusyNinP0HistValue: "<<HEX(result.masterBusyRodBusyNinP0HistValue)<<std::endl;
  std::cout<<"masterBusyRodBusyNinP1HistValue: "<<HEX(result.masterBusyRodBusyNinP1HistValue)<<std::endl;
  std::cout<<"masterBusyEfbHeaderPauseHistValue: "<<HEX(result.masterBusyEfbHeaderPauseHistValue)<<std::endl;
  std::cout<<"masterBusyFormatterMB0HistValue: "<<HEX(result.masterBusyFormatterMB0HistValue)<<std::endl;
  std::cout<<"masterBusyFormatterMB1HistValue: "<<HEX(result.masterBusyFormatterMB1HistValue)<<std::endl;
  std::cout<<"masterBusyRolTestPauseHistValue: "<<HEX(result.masterBusyRolTestPauseHistValue)<<std::endl;
}

ReadRodMasterBusyStatus::ReadRodMasterBusyStatus(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

uint32_t ReadRodMasterBusyStatus::getBusyOutput() {
  // WARNING: not meaningfull if the result hasn't been populated!
  return (result.masterBusyCurrentStatusValue & (~result.masterBusyMaskValue & 0xFF)) | ((result.masterBusyMaskValue >> 8) & 0xFF);
}

// inside execute() the actual meat of the command happens on the ppc
void ReadRodMasterBusyStatus::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  unsigned value;

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);


  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

  // read the busy mask register for crosscheck
  value = IblRod::MasterBusyMask::read();
  result.masterBusyMaskValue=value;

  // read the busy current status ro register
  value = IblRod::MasterBusyCurrentStatus::read();
  result.masterBusyCurrentStatusValue=value;

  // read the histograms
  value = IblRod::MasterBusyOutHist::read();
  result.masterBusyOutHistValue=value;
  value = IblRod::MasterBusyRodBusyNinP0Hist::read();
  result.masterBusyRodBusyNinP0HistValue=value;
  value = IblRod::MasterBusyRodBusyNinP1Hist::read();
  result.masterBusyRodBusyNinP1HistValue=value;
  value = IblRod::MasterBusyEfbHeaderPauseHist::read();
  result.masterBusyEfbHeaderPauseHistValue=value;
  value = IblRod::MasterBusyFormatterMB0Hist::read();
  result.masterBusyFormatterMB0HistValue=value;
  value = IblRod::MasterBusyFormatterMB1Hist::read();
  result.masterBusyFormatterMB1HistValue=value;
  value = IblRod::MasterBusyRolTestPauseHist::read();
  result.masterBusyRolTestPauseHistValue=value;


  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void ReadRodMasterBusyStatus::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
}

// Deseralize the data members of the command
// In this case, only one boolean
void ReadRodMasterBusyStatus::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t ReadRodMasterBusyStatus::serializedSize() const
{
	return sizeof(bool);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void ReadMasterBusyStatusResult::serialize(uint8_t *out) const {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, masterBusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyCurrentStatusValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyOutHistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyRodBusyNinP0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyRodBusyNinP1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyEfbHeaderPauseHistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyFormatterMB0HistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyFormatterMB1HistValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyRolTestPauseHistValue);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void ReadMasterBusyStatusResult::deserialize(const uint8_t *in) {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  masterBusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyOutHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyRodBusyNinP0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyRodBusyNinP1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyEfbHeaderPauseHistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyFormatterMB0HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyFormatterMB1HistValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyRolTestPauseHistValue = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t ReadMasterBusyStatusResult::serializedSize() const {
		//std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (9*sizeof(uint32_t));
}
