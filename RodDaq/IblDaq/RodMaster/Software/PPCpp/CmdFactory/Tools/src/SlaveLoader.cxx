/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>,
 * Date: 2014-VIII-19
 */

#include "SlaveLoader.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void SlaveLoader::dump() {

}

SlaveLoader::SlaveLoader(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void SlaveLoader::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__

  //  IblRodSlave::init();

  std::cout << "Received slave SW binary of size " << slaveSW_bin_size  << " bytes." << std::endl;
  std::cout << "Loading image to both slaves!" << std::endl;

  for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) IblRodSlave::setId(slaveId); // Set the IDs first for logging purposes!

  for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
	  std::cout << "Loading slave " << (int)slaveId << " software." << std::endl;
	  IblRodSlave::loadSlave(slaveId, (uint32_t*)&slaveSW_data[0], (slaveSW_bin_size+3)/4); 
	  std::cout << "Starting slave " << (int)slaveId << "." << std::endl;
	  IblRodSlave::initSlave(slaveId);
	  IblRodSlave::setId(slaveId);
	  IblRodSlave::startSlave(slaveId);
	  // Don't uncomment the lines below: initSlave already waits until the slave booted!
	  // std::cout << "Waiting for slave " << (int)slaveId << " to boot!" << std::endl;
	  // IblRodSlave::waitSlaveBoot(slaveId);
  }

  std::cout << "Done loading!" << std::endl;

  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void SlaveLoader::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out,offset,slaveSW_bin_size);
	for(uint32_t i = 0 ; i < slaveSW_bin_size ; ++i)
		Serializer<uint8_t>::serialize(out,offset,slaveSW_data[i]);
}

// Deseralize the data members of the command
// In this case, only one boolean
void SlaveLoader::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	slaveSW_bin_size = Serializer<uint32_t>::deserialize(in, offset);
	for(uint32_t i = 0 ; i < slaveSW_bin_size ; ++i)
		slaveSW_data[i] = Serializer<uint8_t>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t SlaveLoader::serializedSize() const
{
	return sizeof(bool) + sizeof(uint32_t) + sizeof(slaveSW_data);
}

// Seralize the data members of the command result
// These are the values of the registers we have read
void SlaveLoaderResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, status);

}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void SlaveLoaderResult::deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  status = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t SlaveLoaderResult::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(uint32_t);
}
