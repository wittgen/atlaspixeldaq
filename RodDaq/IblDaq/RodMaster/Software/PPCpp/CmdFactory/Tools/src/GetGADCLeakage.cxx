#include "GetGADCLeakage.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "Fei4Cmd.h"
#include "IblConnectivity.h"
#include "IblThreshScan.h"
#include "IblBocLink.h"
#include "Fei4Proxy.h"
#include "Fei4GlobalCfg.h"
#include "IblDigScan.h"
#include "IblScan.h"
#include "ReadModuleConfig.h"
#include "ProduceData.h"
#include <chrono>
#include <thread>
#include <iomanip>

#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <vector>
#include <numeric>
#include <functional>

#endif

std::map<uint32_t,std::string> gadcSelectorMapz;
std::map<uint32_t,std::vector<uint32_t> > gadcValuesMapz;

GetGADCLeakage::GetGADCLeakage()
{
  setResultPtr(); 
  Channels = 0xFFFFFFFF; // channels to test
  cpMode =0;
  nDC=1;
  UserMask = 672;
  IterInput = 10;
}

void  GetGADCLeakage::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__ 
#ifdef __XMK__

  std::cout<<"cpMode = "<<cpMode<<", nDC = "<<nDC<<", UserMask = "<<UserMask<<", and IterInput = "<<IterInput<<std::endl;

  int rx;

  IblBocLink *iblBocLink = RodResourceManagerIBL::getIblBocLink();
  iblBocLink->enableRxChannels(Channels);
  uint32_t EnabledChannels = iblBocLink->getEnChMask();

  for (int i=0; i<32; i++){
    rx=i;

    Fei4 fei4 = RodResourceManagerIBL::getCalibModuleConfig(rx);
 
    if (EnabledChannels & (1<<i)) {

      fei4.setRunMode(false);
      //fei4.sendPixelConfig(true); // send only fdac and tdac pixel regs individually

      fei4.writeRegister(&Fei4::GADCVref,100);
      fei4.writeRegister(&Fei4::BufVgOpAmp,160);

      Mask my_mask;
      // clear pixel monleak regs.... just in case! 
      my_mask.set(MASK_CLEAR);                                                                                                                                    
      // only write front end / strobe pixel latches one DC at a time                    
      fei4.writeRegister(&Fei4::CP,0x0); // set DC mode to one DC                                                                                                   
      for (int iDC = 0; iDC < 40; iDC++) // all 40 DC                                                                                                     
	fei4.strobePixelLatches(my_mask, 0x100, iDC); // write pixel latches    
      //my_mask.set(MASK_672); 
      if (UserMask == 1) my_mask.set(MASK_FULL);
      else if (UserMask == 4) my_mask.set(MASK_QUARTER);
      else if (UserMask == 8) my_mask.set(MASK_EIGHTH);
      else if (UserMask == 16) my_mask.set(MASK_16);
      else if (UserMask == 32) my_mask.set(MASK_32);
      else if (UserMask == 672) my_mask.set(MASK_672);
      else {
	my_mask.set(MASK_672); 
	std::cout<<"You chose an invalid mask! Now applying default mask 672."<<std::endl;
      }

      fei4.writeRegister(&Fei4::ILR,1); //ILR=1 for 10kOhm, ILR=0 for 90kOhm ILeak Hitbus
      fei4.writeRegister(&Fei4::CP,cpMode); // set DC mode to one DC  0x24 -25 DC?
      fei4.writeRegister(&Fei4::GADCSel, 7);

      for (uint32_t iDC = 1; iDC <= nDC; iDC++) 
	{
	  //enable shift register before pixel register
	  fei4.strobePixelLatches(my_mask, 0x100, iDC); //write pixel registers 
	}

      //Copy from Latches to SR
      fei4.globalPulse(10);   

      std::cout << "--" << nDC <<std:: endl;
        std::cout << "--" << cpMode <<std:: endl;

      std::this_thread::sleep_for(std::chrono::microseconds(100));

      fei4.writeRegister(&Fei4::ADC,1);
      std::cout << "--------------------" << std:: endl;
      std::cout << "Start to read GADC"<< std::endl;

      vector<uint32_t> gadcValues;
      uint32_t GadcSum; 
      uint32_t GadcSqSum; 
      uint32_t GadcMean;
      uint32_t GadcStdDev;

      //std::vector<uint32_t>& gadcValues = gadcValuesMapz[7];
      
      for( uint32_t i = 1 ; i <= IterInput ; ++i ) 
	{
	  fei4.globalPulse(63);// Pulse for LOAD Latches FIRST (Clock cycles lenght of the pulse)
     std::this_thread::sleep_for(std::chrono::microseconds(100));
	  int GadcOutput=fei4.getRegisterValueFromFE(40); //Read GR40
	  gadcValues.push_back(GadcOutput >> 4);
	}
      GadcSum = (int)(std::accumulate( gadcValues.begin(), gadcValues.end(), 0.0 ) + 0.5);
      GadcSqSum = (int)(std::inner_product( gadcValues.begin(), gadcValues.end(), gadcValues.begin(), 0.0 ) + 0.5);
      GadcMean = GadcSum/gadcValues.size();
      GadcStdDev = (int)(std::sqrt( GadcSqSum/gadcValues.size() - GadcMean*GadcMean ) + 0.5); // Warning: precision lost // Todo: address

      std::cout.setf(std::ios::left);
      std::cout << "\n" << GadcMean << " +/- " << GadcStdDev << std::endl; 
      gadcValues.clear();

      fei4.writeRegister(&Fei4::ADC,0);

      //Move back the Pixel Strobes to 0
      fei4.writeRegister(&Fei4::Pixel_latch_strobe,0x0);


#endif //hier war endif

      //Results and output on PPC

      result.resultarrayMean[rx]=GadcMean;
      result.resultarrayStd[rx]=GadcStdDev;

      std::cout << "Additional read-out info :"<< std::endl;
      //std::cout << "Channels: " << Channels << std::endl;
      std::cout << "Number of iterations of GadcOutput: " << IterInput << std::endl;
      std::cout << "Output of Rx " << rx << std::endl;

    }
 
    else { 
      std::cout<<"THIS CHANNEL Rx " << rx <<  " IS NOT INCLUDED (either you didn't request it or it is disabled)"<<std::endl;
      uint32_t GadcMean=9999;
      uint32_t GadcStdDev=9999;

      result.resultarrayMean[rx]=GadcMean;
      result.resultarrayStd[rx]=GadcStdDev;
    }
  }
  return ;
#endif

}

