/*
 * * F. Meloni <federico.meloni@cern.ch>
 * * P. Morettini <Paolo.Morettini@ge.infn.it>
 * Configure BOC
 */

#include <iomanip>
#include <bitset>
#include "BocSendConfig.h"
#include "RodResourceManager.h"

BocSendConfig::BocSendConfig(){

action = Dump;
setResultPtr();
}

void BocSendConfig::execute() {

switch(action) {
  case Dump :
    dump();
    break;
  case SetRxThresholdDelay :
    setRxThresholdDelay( );
    break;
  case SetRxFibreMapping :
    setRxFibreMapping( );
    break;
  case SetTxCoarseDelay :
    setTxCoarseDelay( );
    break;
  case SetIBLPhases :
    setIBLPhases( );
    break;
  default:
    std::cout<<"You didn't choose a sensible action. Please try again. You sent action type: "<<(int)action<<std::endl;
}

}

void BocSendConfig::setRxThresholdDelay( ){

#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

  if (nRx <= 32) {
    for (uint32_t il=0; il<nRx; il++) {
      if (rxCh[il] >= 0 && rxCh[il] <= 31) {
	//Determine fibre and plugin ID, given the Rx channel
	uint8_t fibre = IblBoc::getFibreFromRxCh(rxCh[il]);
        uint8_t pluginId = (fibre / 12);
	//std::cout << "RX # " << rxCh[il] << " Fibre: " << (int)fibre << " PluginId: " << (int)pluginId  << std::endl;
	/* Commented out by Chris on 25-05-2016 */ 
	//Setting the RX gain (one setting is shared by all the channels)
	IblBoc::PixSetRxRGS(pluginId, gain[il]);
		
	//Setting the RX threshold
        uint32_t ithr = (uint32_t)(threshold[il]*250 + 2500.5);
	//std::cout << "Threshold: "<<threshold[il]<<" --> " << ithr << std::endl;
	IblBoc::PixSetRxThreshold(fibre, ithr); //Using broadcast as the ch-DAC mapping is currently bugged
	  
	//Setting the fibre delay
	uint32_t iDelay = (uint32_t)(delay[il]*1000 + 0.5);	
	uint32_t coarseDelay = iDelay*8/25000;
	uint32_t fineDelay = (iDelay - coarseDelay*3125)/35;
	//std::cout << "Delay Coarse: " << (int)coarseDelay << " Fine: " << (int)fineDelay  << std::endl;
	IblBoc::SetRxSampleDelay(fibre, coarseDelay, fineDelay);
	/* */
	//uint8_t coarseDelay_test = 0;
	//uint8_t fineDelay_test = 0;
	
	//IblBoc::GetRxSampleDelay(fibre, coarseDelay_test, fineDelay_test);
	//std::cout << "Register readback - Coarse: " << (int)coarseDelay_test << " Fine: " << (int)fineDelay_test  << std::endl;	  
      }
    }
  }
#endif

}

void BocSendConfig::setIBLPhases( ){

#ifdef __XMK__

if(!RodResMgr::isFei4()) return;

  if (nRx <= 32) {
    for (uint32_t il=0; il<nRx; il++) {
       if (rxCh[il] >= 0 && rxCh[il] <= 31) {
	//Determine fibre and plugin ID, given the Rx channel
	uint8_t fibre = IblBoc::getFibreFromRxCh(rxCh[il]);
	//std::cout<<"RxCh "<<(int)rxCh[il]<<" Phase "<<(int)phases[il]<<std::endl;
	if(phases[il]>3)continue;
        IblBoc::CDRManPhase(fibre,phases[il]);
      }
    }
  }
#endif

}

void BocSendConfig::setTxCoarseDelay( ){

#ifdef __XMK__

  if (nTx <= 32) {
    for (uint32_t il=0; il<nTx; il++) {
      if (txCh[il] >= 0 && txCh[il] <= 31) {
	//Setting TX coarse delay
	uint32_t coarseDelay = txCoarseDelay[il];

	IblBoc::PixSetTxCoarseDelay(txCh[il],coarseDelay);
	//uint32_t coarseDelayReadBack = IblBoc::PixGetTxCoarseDelay(txCh[il]);
	//std::cout << "TX# " << txCh[il]<< " Coarse Delay "<<coarseDelay<< " Readback: " << coarseDelayReadBack << std::endl;
      }
    }
  }
#endif

}

void BocSendConfig::setRxFibreMapping( ){

#ifdef __XMK__

if(!RodResMgr::isFei3()) return;

    for (uint32_t il=0; il<nRx; il++) {
      if (rxCh[il] >= 0 && rxCh[il] <= 31) {
        //Set Master and Slave RX-Fiber mapping
	IblBoc::setMasterAndSlaveFibre(rxCh[il], masterFibre[il], slaveFibre[il]);
	//std::cout<<"Setting to rx "<<rxCh[il]<<" Master Fibre "<< masterFibre[il]<< " Slave Fibre "<< slaveFibre[il]<<std::endl;
      }
    }
#endif

}

void BocSendConfig::dump( ){

std::cout<<__PRETTY_FUNCTION__<<std::endl;

std::cout<<" Dumping RX configuration: "<<std::endl;
for (uint32_t il=0; il<nRx; il++) {
   std::cout<<" RX Index: "<<il<<std::endl;
   std::cout<<" rxCh: "<<rxCh[il]<<std::endl;
   std::cout<<" Master Fibre: "<<masterFibre[il]<<std::endl;
   std::cout<<" Slave Fibre: "<<slaveFibre[il]<<std::endl;
   std::cout<<" Threshold: "<<threshold[il]<<std::endl;
   std::cout<<" Delay: "<<delay[il]<<std::endl;
   std::cout<<" Gain: "<<gain[il]<<std::endl;
   std::cout<<" Phase (IBL): "<<phases[il]<<std::endl;

}

std::cout<<std::endl;

}


void BocSendConfig::serialize(uint8_t *out) const
{
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, nTx);
  Serializer<uint32_t>::serialize(out, offset, nRx);
  Serializer<BocSendConfigActionType>::serialize(out, offset, action);
  for (uint32_t il=0; il<32; il++) {
    Serializer<uint32_t>::serialize(out, offset, txCh[il]);
    Serializer<uint32_t>::serialize(out, offset, rxCh[il]);
    Serializer<uint32_t>::serialize(out, offset, masterFibre[il]);
    Serializer<uint32_t>::serialize(out, offset, slaveFibre[il]);
    Serializer<float>::serialize(out, offset, threshold[il]);
    Serializer<float>::serialize(out, offset, delay[il]);
    Serializer<uint32_t>::serialize(out, offset, txCoarseDelay[il]);
    Serializer<uint32_t>::serialize(out, offset, gain[il]);
    Serializer<uint8_t>::serialize(out, offset, phases[il]);
  }
}

void BocSendConfig::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  Serializer<uint32_t>::deserialize(in, offset, nTx);
  Serializer<uint32_t>::deserialize(in, offset, nRx);
  Serializer<BocSendConfigActionType>::deserialize(in, offset, action);
  for (uint32_t il=0; il<32; il++) {
    Serializer<uint32_t>::deserialize(in, offset, txCh[il]);
    Serializer<uint32_t>::deserialize(in, offset, rxCh[il]);
    Serializer<uint32_t>::deserialize(in, offset, masterFibre[il]);
    Serializer<uint32_t>::deserialize(in, offset, slaveFibre[il]);
    Serializer<float>::deserialize(in, offset, threshold[il]);
    Serializer<float>::deserialize(in, offset, delay[il]);
    Serializer<uint32_t>::deserialize(in, offset, txCoarseDelay[il]);
    Serializer<uint32_t>::deserialize(in, offset, gain[il]);
    Serializer<uint8_t>::deserialize(in, offset, phases[il]);
  }
}

uint32_t BocSendConfig::serializedSize() const
{
  return (2 + 32*6) * sizeof(uint32_t) + 2 * 32 * sizeof(float) + 32 * sizeof(uint8_t) +sizeof(BocSendConfigActionType);
}
