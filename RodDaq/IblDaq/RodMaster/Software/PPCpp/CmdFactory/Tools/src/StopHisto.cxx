#include "StopHisto.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

void StopHisto::execute() {
#ifdef __XMK__
  IblRodSlave s;
  
  bool success = s.stopHisto(whichSlave);
  std::cout<< (success ? "    stopped slave histogrammer " : " !! failed to stop slave histogrammer ") << whichSlave << std::endl;
 
#endif
}

void StopHisto::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        
}
void StopHisto::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<uint32_t>::deserialize(in, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        
}
uint32_t StopHisto::serializedSize() const
{
        return sizeof(uint32_t);
}


