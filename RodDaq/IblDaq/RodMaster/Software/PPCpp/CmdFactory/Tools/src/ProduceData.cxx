#include "ProduceData.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "RodResourceManager.h"
#include "Fei4Proxy.h"
#include "IblConnectivity.h"
#include "IblBocLink.h"
#include "IblRodSlave.h"
#include "DumpRodRegisters.h"
#include <chrono>
#include <thread>
#include <iomanip>
#endif

ProduceData::ProduceData() {
  setResultPtr();
  sleep = 100; // sleep in usecs bewteen triggers
  nTriggers = 1000000; // number of triggers to execute
  channels = 0xFFFFFFFF; // channels to send data
  trigCount = 8; // number of frames per trigger
  trigDelay = 50; // delay between injection and trigger
  trigLatency = 195; // trigger latency
  maskSteps = 8; // Number of mask steps in mask (options are 1, 2, 4, 8, 16, 32, 672) 
  dcMode = 0; // Select Double Column Mode, 0x0=AddrDC, 0x3 = all, 0x1=every 4 col,0x2 = every 8 col 
  rawToT = 5; // ToT for digital injection
  noise = false; // instead of digital injection only pick up noies hits
  debug = false; // print out onto kermit?
  debugRx = 18; // if debugging, which rx
}

void  ProduceData::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__

  //set up channel mask                                                                                                                                                                        
  SerialPort* sPort = RodResourceManagerIBL::getSerialPort();   
  sPort->setTxMask(IblConnectivity::getTxMaskFromRxMask(channels)); 
  Fei4 fe; fe.setTxLink(sPort); fe.setChipID(8);                       

  while (1) {
    fe.calTrigger(trigDelay);
    std::this_thread::sleep_for(std::chrono::microseconds(sleep));
  }

  return; // tmp for testing calpulse issue

  // configure the modules and enable the boc rx/tx links
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  if( !iblBocLink->enableChannels(channels) ) {
    std::cout<<"WARNING: could not enable all the desired channels. Will continue anyway"<<std::endl;
    channels = iblBocLink->getEnChMask();
  }
  std::cout<<std::endl;
  std::cout<<"**********************************"<<std::endl;
  std::cout<<"Enabled channel mask = 0x"<<std::hex<<channels<<std::dec<<std::endl;


  // Formatter link enable
  IblRodSlave s; s.enableRxLink(channels);

  //set up channel mask
  // tmp lj

  /* SerialPort* sPort = RodResourceManagerIBL::getSerialPort(); 
  sPort->setTxMask(IblConnectivity::getTxMaskFromRxMask(channels));
  Fei4 fe; fe.setTxLink(sPort); fe.setChipID(8); */
  Fei4Proxy feConfigs = Fei4Proxy(channels); 
 
  // set number of frames per trigger on ROD
  IblRod::SlaveA::TriggerCountNumber::write(trigCount);
  IblRod::SlaveB::TriggerCountNumber::write(trigCount);

  // set time out for channels with no data
  IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0xFFF);
  IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0xFFF);

  std::cout<<"About to initialize front end"<<std::endl;

  // initialize front end and setup mask steps (1 DC only)
  std::cout<<"number of frames per trigger = "<<(int)trigCount<<
    ", trigger delay = "<<(int)trigDelay<<", and latency = "<<(int)trigLatency<<std::endl;
  feConfigs.setRunMode(false);
  feConfigs.writeRegister(&Fei4::SRK, 0x0);
  feConfigs.writeRegister(&Fei4::Trig_Count, trigCount);
  feConfigs.writeRegister(&Fei4::PlsrDelay, trigDelay);
  feConfigs.writeRegister(&Fei4::Trig_Lat, trigLatency);
  feConfigs.writeRegister(&Fei4::CalPulseDelay, 0);
  feConfigs.writeRegister(&Fei4::CMDcnt12,rawToT-1);
  //turn on digital injection                                                                                                                                        
  if (!noise) {
    std::cout<<"initializing digital injection "<<std::endl;
    std::cout<<"raw tot = "<<(int)rawToT<<std::endl;
    if (dcMode == 0) std::cout<<"number of hits to expect = "<<int(672/(maskSteps))<<std::endl;
    else if (dcMode == 1) std::cout<<"number of hits to expect = "<<int(672/(maskSteps)*10)<<std::endl;
    else if (dcMode == 2) std::cout<<"number of hits to expect = "<<int(672/(maskSteps)*5)<<std::endl;
    else if (dcMode == 3) std::cout<<"number of hits to expect = "<<int(672/(maskSteps)*40)<<std::endl;
    feConfigs.writeRegister(&Fei4::DHS, 1);
    feConfigs.writeRegister(&Fei4::Vthin_Coarse,255);
    feConfigs.writeRegister(&Fei4::Vthin_Fine,255);
  }
  else { // init a config that will pick up some noise
    std::cout<<"initializing config to pick up some noise "<<std::endl;
    feConfigs.writeRegister(&Fei4::DHS, 0);
    feConfigs.writeRegister(&Fei4::PrmpVbpf,100);
    feConfigs.writeRegister(&Fei4::Amp2Vbpff,50);
    feConfigs.writeRegister(&Fei4::DisVbn,26);
    feConfigs.writeRegister(&Fei4::GADCCompBias,100);
    feConfigs.writeRegister(&Fei4::PlsrIDACRamp,180);
    feConfigs.writeRegister(&Fei4::Vthin_Coarse,1);
    feConfigs.writeRegister(&Fei4::Vthin_Fine,46);
  }
  
  Mask mask;
  // clear pixel capicator regs.... just in case!                                                                                                                    
  mask.set(MASK_CLEAR);
  feConfigs.writeRegister(&Fei4::CP,0x3); // set DC mode to all DC                                                                                                      
  feConfigs.strobePixelLatches(mask, 0xc1, 0); // write capicitor pixel latches                                                                                       

  
  if (!noise) {
    if (maskSteps == 2) mask.set(MASK_HALF);
    else if (maskSteps == 4) mask.set(MASK_QUARTER);
    else if (maskSteps == 8) mask.set(MASK_EIGHTH);
    else if (maskSteps == 16) mask.set(MASK_16);
    else if (maskSteps == 32) mask.set(MASK_32);
    else if (maskSteps == 672) mask.set(MASK_672);
  }
  else {
    mask.set(MASK_FULL);
  }

  feConfigs.writeRegister(&Fei4::CP,0x3); // set DC mode to all DC                                                                                                      
  feConfigs.strobePixelLatches(mask, 0x1, 0); // write pixel latches with 1 for output enable                                                                         
  feConfigs.writeRegister(&Fei4::CP,dcMode); // return to desired DC mode                                                                                     
  feConfigs.writeRegister(&Fei4::Colpr_Addr, 0);
  
  feConfigs.setRunMode(true);
  fe.setRunMode(true);

  std::cout<<std::endl;  
  std::cout<<"**********************************"<<std::endl;
  std::cout<<"Right before data taking, ROD register dump"<<std::endl;
  std::cout<<std::endl;  
  DumpRodRegisters cmd;
  cmd.dump();
  std::cout<<"**********************************"<<std::endl;

  std::cout<<std::endl;
  std::cout<<"About to issue "<<(int)nTriggers<<" triggers with a "<<(int)sleep<<" usec sleep after each one"<<std::endl;

  Fei4 * feReadBack = &(RodResourceManagerIBL::getCalibModuleConfig(debugRx));

  if (noise) { // trigger only, no injection
    if (sleep > 0) {
      for(uint32_t iTrig = 0 ; iTrig < nTriggers ; ++iTrig) {
	fe.trigger();
	if (debug) debugRead(trigCount, feReadBack, debugRx);
    std::this_thread::sleep_for(std::chrono::microseconds(sleep));
	
      }
    }
    else {
      for(uint32_t iTrig = 0 ; iTrig < nTriggers ; ++iTrig) {
	fe.trigger();
      }
    }
  } 
  else { // inject and trigger
    if (sleep > 0) {
      for(uint32_t iTrig = 0 ; iTrig < nTriggers ; ++iTrig) {
	fe.calTrigger(trigDelay);
    std::this_thread::sleep_for(std::chrono::microseconds(sleep));	
	if (debug) debugRead(trigCount, feReadBack, debugRx);
      } 
    }
    else {
      for(uint32_t iTrig = 0 ; iTrig < nTriggers ; ++iTrig) {
	fe.calTrigger(trigDelay);
      } 
    }
  }

  std::cout<<"finished triggering!"<<std::endl;

#endif
}

void ProduceData::debugRead(uint8_t trigCnt, Fei4* fe, uint8_t rx) {
#ifdef __XMK__

  std::vector<int> answer;
  for (uint8_t iTrigCount = 0 ; iTrigCount < trigCnt ; ++iTrigCount) {
    answer = fe->readFrame();
    std::cout<<"**********************************"<<std::endl;
    std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo channel "<<(int)rx<<std::endl;
    std::cout<<"**********************************"<<std::endl;
    unsigned int answerSize = answer.size();
    for (unsigned int i = 0; i < answerSize; i++) {
      if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<std::setw(2)<<answer[i];
      else if (i%3 == 1) std::cout<<std::hex<<std::setw(2)<<answer[i];
      else std::cout<<std::hex<<std::setw(2)<<answer[i]<<std::dec<<std::endl;
    }
    std::cout<<"**********************************"<<std::endl;
  }
#endif
}
