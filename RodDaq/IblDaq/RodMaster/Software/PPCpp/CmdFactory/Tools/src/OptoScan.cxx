/*
 * JuanAn Garcia
 * Date: Nov-2018
 *
 * Generic class to run an OptoScan
 *
 */
#include <unistd.h>
#include <bitset>
#include "OptoScan.h"
#include "HwRegsInCpp.h"
#include "SerialPort.h"
#include "RodResourceManager.h"
#include "BarrelConnectivity.h"

OptoScan::OptoScan() {

	setResultPtr();
	m_gain = 0;
	m_rxMask = 0xFFFFFFFF;
	m_nTriggers = 10;
	m_speed = 0;
	m_bitPattern = 0xBFE;
	m_nBitPattern = 11;
	m_maxDelay = 25;//ns
	m_maxThr = 250;
	m_nThr = 25;
	m_nDelay = 75;
}

void OptoScan::execute() {
#ifdef __XMK__
		
	std::cout << "Running OptoScan with these parameters" << std::endl;
	std::cout << "Gain: " << m_gain << std::endl;
	std::cout << "Rx Mask: "<<std::hex << m_rxMask <<std::dec<< std::endl;
	std::cout << "Number of Triggers: " << m_nTriggers << std::endl;
	std::cout << "Speed: " << m_speed << std::endl; // 0 -> 40Mbit, 1-> 80Mbit
	std::cout << "Bit Pattern: " << HEX(m_bitPattern) << std::endl;
	std::cout << std::endl;

	PixLib::PixScanBase::MccBandwidth MCCSpeed = PixLib::PixScanBase::SINGLE_40;
	switch(m_speed){
	  case 0:
	    MCCSpeed =  PixLib::PixScanBase::SINGLE_40;
	  break;
	  case 1:
	    MCCSpeed =  PixLib::PixScanBase::SINGLE_80;
	  break;
	  default:
	  std::cout<<"Speed can be only 0 (40 Mbps) or 1 (80 Mbps), while you provide "<< m_speed <<" setting to 40 Mbps by default"<<std::endl;
	}
	
	uint32_t nBitsCheck = 0;
	uint32_t extendedRxMask = initOpto(m_rxMask,MCCSpeed,m_gain,m_bitPattern,m_nBitPattern, nBitsCheck);

	float delayMin = 0;
	float delayMax = 1000* m_maxDelay;
	float delayDelta = 1000;
	if(m_nDelay>0)delayDelta=(delayMax - delayMin)/m_nDelay;
	
	float thrMin = 2500;
	float thrMax = m_maxThr*250 + 2500;
	float thrDelta = 250;
	if(m_nThr>0) thrDelta = (thrMax - thrMin)/m_nThr;

	SerialPort* sPort =NULL;
	sPort = RodResourceManagerPIX::getSerialPort();
	uint32_t txMask = BarrelConnectivity::getTxMaskFromRxMask(m_rxMask);
	sPort->setTxMask(txMask);
	Fei3Mod Fei3;
	Fei3.setTxLink(sPort);

	for (uint32_t nDel = 0; nDel < m_nDelay; nDel++ ) {
	  std::cout<<"Delay step "<<nDel<<"/"<<m_nDelay<<std::endl;
	  uint32_t iDelay = (uint32_t)(delayMin + nDel*delayDelta + delayDelta/2.);
	  uint32_t coarseD = iDelay*8/25000;
	  uint32_t fineD = (iDelay - coarseD*3125)/35;
	  Fei3.writeMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay
  	  setDelay(extendedRxMask,coarseD, fineD);
  	  Fei3.mccGlobalResetMCC();
	  Fei3.writeMCCRegister(CSR, 0x0040 | (int8_t)MCCSpeed );//setMCCspeed
	  std::map< uint8_t, std::vector< uint32_t > > thresResult;

	  	for (uint32_t nThr = 0; nThr < m_nThr; nThr++) {
		  uint32_t iThr = (uint32_t)(thrMin + nThr*thrDelta + thrDelta/2.);
		  setThreshold(extendedRxMask, iThr);

		  std::map<uint8_t, uint32_t> err = getOptoErrors(extendedRxMask, m_nTriggers, nBitsCheck, m_bitPattern,m_nBitPattern, Fei3);
		  for (std::map<uint8_t, uint32_t >::iterator it = err.begin(); it!=err.end(); ++it)thresResult[it->first].push_back(it->second);
		}
	  for (std::map<uint8_t, std::vector< uint32_t> >::iterator it2 = thresResult.begin(); it2!=thresResult.end(); ++it2)result.errors[it2->first].push_back(it2->second);
	}

    cleanUp(extendedRxMask, Fei3);

#endif
}

#ifdef __XMK__

uint32_t OptoScan::initOpto(uint32_t rxMask, PixLib::PixScanBase::MccBandwidth MCCSpeed, uint32_t gain, uint32_t bitPattern, uint32_t nBitPattern, uint32_t &nBitsCheck){
   uint32_t extendedRxMask=0x0;//taking into account AltLink
   Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
   uint32_t modMask = BarrelConnectivity::getModMaskFromRxMask(rxMask);
   std::cout<<"Module Mask 0x"<<std::hex<<modMask<<std::dec<<std::endl;

   IblBoc::PixSetSpeed( MCCSpeed );

        for(uint8_t iMod = 0 ; iMod < 32 ; ++iMod) {
	    //Only channels belonging to my mask!!!
	    if (fes[iMod].revision ==0)continue;
	    if (!(modMask & (1<<iMod)) )continue;
              uint8_t rxCh = fes[iMod].getRxCh();
	      uint8_t fibre = IblBoc::getMasterFibreFromRxCh(rxCh);
	      extendedRxMask |= (1<<rxCh);
	      IblBoc::PixSetRxRGS(fibre/12, gain);
	      //Alternative channel
	      if(IblBoc::getSlaveFibreFromRxCh(rxCh)%24 == 0 )continue;//To check if second link is there
	      std::cout<<"Second fibre for rx "<<(int)rxCh<<" Slave fibre "<<(int)IblBoc::getSlaveFibreFromRxCh(rxCh)<<std::endl;
	      rxCh = BarrelConnectivity::getAltRxLink(rxCh);
	      fibre = IblBoc::getMasterFibreFromRxCh(rxCh);
	      IblBoc::PixSetRxRGS(fibre/12, gain);
	      extendedRxMask |= 1<<rxCh;
	  }

	std::cout<<"Extended RxMask: "<<std::hex<<extendedRxMask<<std::dec<<std::endl;

	RodResourceManagerIBL::getIblBocLink()->enableTxChannels( BarrelConnectivity::getTxMaskFromRxMask(rxMask) );
	RodResourceManagerIBL::getIblBocLink()->enableRxChannels(extendedRxMask);

	//create bitStream to compare
	std::vector <uint8_t> CompareBitStream;
	std::vector <uint8_t> MaskBitStream;
	nBitsCheck = createCompareBitStream (bitPattern, nBitPattern, CompareBitStream, MaskBitStream);

	std::cout<<"Compare stream size: "<<nBitsCheck<<" bits"<<std::endl;

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	  if (extendedRxMask & (1<<rxCh)){
	  IblBoc::PixCompareDisable(rxCh);
	  IblBoc::PixCompareLoad(rxCh, CompareBitStream, MaskBitStream);
	  IblBoc::PixCompareEnable(rxCh);
	}

return extendedRxMask;
}

void OptoScan::setThreshold(uint32_t rxMask, uint16_t threshold){

//Setting the RX threshold	
   for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	if (rxMask & (1<<rxCh))IblBoc::PixSetRxThreshold(IblBoc::getMasterFibreFromRxCh(rxCh), threshold);

}

void OptoScan::setDelay(uint32_t rxMask, uint8_t coarseD, uint8_t fineD){

for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	    if(rxMask & (1<<rxCh))IblBoc::SetRxSampleDelay(IblBoc::getMasterFibreFromRxCh(rxCh), (uint8_t)coarseD, (uint8_t)fineD);

}

void OptoScan::setDelaySlaveFibre(uint32_t rxMask, uint8_t coarseD, uint8_t fineD){

for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	    if(rxMask & (1<<rxCh))IblBoc::SetRxSampleDelay(IblBoc::getSlaveFibreFromRxCh(rxCh), (uint8_t)coarseD, (uint8_t)fineD);

}

std::map<uint8_t, uint32_t> OptoScan::getOptoErrors(uint32_t rxMask, uint32_t nTriggers, uint32_t nBitsCheck ,uint32_t bitPattern, uint32_t nBitPattern, Fei3Mod &sPort){

std::map<uint8_t, uint32_t> optoErrors;


  std::vector<uint32_t> endOfEvent;
  endOfEvent.push_back( 0x87800000);

  for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
    if (rxMask & (1<<rxCh)){
      IblBoc::PixCompareDisable(rxCh);
      IblBoc::PixCompareClearCounters(rxCh);
      IblBoc::PixCompareEnable(rxCh);
    }

     for (size_t iTrig = 0; iTrig < nTriggers; iTrig++) {

	sPort.addPattern(bitPattern, nBitPattern);  // write pattern n times to all receivers
	sPort.mccWrReceiver(endOfEvent); // write EOE to all receivers
	sPort.mccEnDataTake(); // enable data taking

	sPort.sendLV1(); // trigger and start event builder
	usleep(100);

       }//Trigger loop

uint32_t err, bitsReceived, bitErrors;

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
		 if (rxMask & (1<<rxCh)){
		  err =0;
	          IblBoc::PixCompareDisable(rxCh);
	          IblBoc::PixCompareGetCounters(rxCh, bitsReceived, bitErrors);
	    	 if(bitErrors > nBitsCheck*nTriggers ) err = nBitsCheck*nTriggers;
	    	 else {
	    	 if( bitsReceived < (nBitsCheck*nTriggers) )err += (nBitsCheck*nTriggers) - bitsReceived;
	    	err += bitErrors;
	    	}
	    optoErrors[rxCh]= err;
	}


return optoErrors;

}

void OptoScan::cleanUp(uint32_t rxMask, Fei3Mod &sPort){

	sPort.writeMCCRegister(CSR, 0x020);
	sPort.writeMCCRegister(CSR, 0x0);
	sPort.mccGlobalResetMCC();

	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
		if (rxMask & (1<<rxCh)){
		IblBoc::PixCompareDisable(rxCh);
		IblBoc::PixCompareClearCounters(rxCh);
		}

    RodResourceManagerIBL::getIblBocLink()->disableRxChannels(rxMask);

}

uint32_t OptoScan::createCompareBitStream (uint32_t pattern, uint8_t nPatterns ,std::vector<uint8_t> &CompareBitStream, std::vector<uint8_t> &MaskBitStream){
uint32_t nBitsCheck=0;

  Boclib::Bitstream val;
  Boclib::Bitstream mask;
  
    val.AppendVar(0x1D, 5);   mask.AppendVar(0x00, 5);     // header MASKED
    val.Append8(0x1);         mask.Append8(0x0);       // L1ID  MASKED
    val.AppendVar(0x1, 1);    mask.AppendVar(0x1,1);	// sync NOT MASKED
    val.Append8(0x29);        mask.Append8(0x00);        // BCID  MASKED

    // FE data NOT MASKED
    for(int i = 0; i < 16; i++)
    {
        val.AppendVar(0x1E0 | i, 9);   mask.AppendVar(0xFFF, 9);    // sync + FE#

        for(uint8_t j = 0; j < nPatterns; j++)
        {
            val.AppendVar(0x1, 1);    		mask.AppendVar(0x1, 1);    // sync
            val.AppendVar(pattern, 21);     mask.AppendVar(0xFFFFFFFF, 21);
        }
    }

    // trailer NOT MASKED
    val.AppendVar(0x1, 1);     		mask.AppendVar(0x1, 1);       // sync
    val.AppendVar(0x000000, 22);    	mask.AppendVar(0xFFFF00, 22); // trailer

CompareBitStream = val.GetData();
MaskBitStream = mask.GetData();


    for (size_t i=0; i<MaskBitStream.size();++i)
	for(size_t j=0; j<8;++j)
	    if(MaskBitStream[i] & (1<<j))nBitsCheck++;

return nBitsCheck;

}

//WIP, not properly implemented and obsolete
uint32_t OptoScan::getErrorsFromRxFIFO(uint8_t rxCh, uint32_t pattern){

  uint32_t bad_msg =0;

  //Read the data
  uint8_t start_frame = 0x0;
  std::vector< std::bitset<8> > rxData;

  size_t k = 0;
    while (!(IblBoc::read( rxCh / 16, rxCh % 16, IblBoc::RxStatus ) & 0x20)) {
     uint8_t msg = IblBoc::read( rxCh / 16, rxCh % 16, IblBoc::DataLow );
     std::bitset<8> msg_bit(msg);
     rxData.push_back(msg_bit);
     if (k == 0) start_frame = msg;
     k++;
    }

  //Looking for the MCC header, if it's not received correctly
  if ( rxData.size() > 5 && (start_frame & 0xF8) == 0xe8 ) {

	//Switch to Marius pattern matching asap
	std::bitset< 12 > pattern_to_match(pattern); //0b101111111110
	int8_t pos = 6;
	size_t k = 5;
	size_t sensors = 0;
	while (k < rxData.size()) {
	    for (int8_t z = pattern_to_match.size() - 1; z >= 0; z--) {
	      if (pattern_to_match[z] != rxData.at(k)[pos]) bad_msg++;
	      pos--;

	        if (pos < 0) {
	        pos = 7;
	        k++;
		if (k > rxData.size() - 1) break;
	        }
	    }

	  sensors++;
	  if (sensors > 15) break;
	  pos -= 19;
	    if (pos < 0) {
		while (pos < 0) {
		k++;
		pos += 8;
		}
	    } else {
		k++;
	    }
	}
  } else {
     bad_msg = 16 * 12;
  }

return bad_msg;
}


#endif

void OptoScan::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset, m_gain);
	Serializer<uint32_t>::serialize(out, offset, m_rxMask);
	Serializer<uint32_t>::serialize(out, offset, m_nTriggers);
	Serializer<uint32_t>::serialize(out, offset, m_speed);
	Serializer<uint32_t>::serialize(out, offset, m_bitPattern);
	Serializer<float>::serialize(out, offset, m_maxDelay);
	Serializer<float>::serialize(out, offset, m_maxThr);
	Serializer<uint32_t>::serialize(out, offset, m_nThr);
	Serializer<uint32_t>::serialize(out, offset, m_nDelay);	
}

void OptoScan::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<uint32_t>::deserialize(in, offset, m_gain);
	Serializer<uint32_t>::deserialize(in, offset, m_rxMask);
	Serializer<uint32_t>::deserialize(in, offset, m_nTriggers);
	Serializer<uint32_t>::deserialize(in, offset, m_speed);
	Serializer<uint32_t>::deserialize(in, offset, m_bitPattern);
	Serializer<float>::deserialize(in, offset, m_maxDelay);
	Serializer<float>::deserialize(in, offset, m_maxThr);
	Serializer<uint32_t>::deserialize(in, offset, m_nThr);
	Serializer<uint32_t>::deserialize(in, offset, m_nDelay);
}

uint32_t OptoScan::serializedSize() const
{
	return (7* sizeof(uint32_t) + 2*sizeof(float));
}

void OptoScanResults::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer< std::map< uint8_t, std::vector< std::vector<uint32_t> > > >::serialize(out, offset, errors); 

}

void OptoScanResults::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	errors = Serializer< std::map<uint8_t, std::vector< std::vector<uint32_t> > > >::deserialize(in, offset); 

}

uint32_t OptoScanResults::serializedSize() const
{

	uint32_t sizeMap =  Serializer< std::map<uint8_t, std::vector< std::vector<uint32_t> > > >::size(errors);
	return sizeMap;

}

