#include "HalfClockReturn.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "Serializer.h"
#endif

HalfClockReturn::HalfClockReturn() {
  setResultPtr();
  m_modMask = 0xFFFFFFFF;
  result.regs.clear();
}

void HalfClockReturn::execute() {
  std::cout << "in function: " << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__
  
  result.nMod = 0;

  RodResourceManagerPIX &rm = RodResourceManagerPIX::getInstance();
  int maxModules = rm.getNModuleConfigs();

//  IblBocLink *iblBocLink = RodResourceManagerPIX::getIblBocLink();
//  iblBocLink->enableChannels(m_modMask, false);

  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

	IblBoc::write(0, 0, IblBoc::TxControl, 0x01);
	IblBoc::write(0, 0, IblBoc::RxControl, 0x0F);

  for (int i = 0; i < maxModules; i++) {
    if (m_modMask & (1 << i)) {
      std::cout << "Setting half clock for module " << i << std::endl;
      result.nMod++;
      for (int ix = 16; ix < 32; ix++) {
      fes[i].setTxCh(ix);
      fes[i].setRxCh(20);
      std::cout << "LASER: " << fes[i].getTxCh() << " " << fes[i].getRxCh() << std::endl;
      //fes[i].mccGlobalResetMCC();
      fes[i].writeMCCRegister(CSR, 0x20); // half clock
      //fes[i].writeMCCRegister(2, 0xFFFF);
      //fes[i].mccRdRegister(2);
      //uint8_t dh = IblBoc::read(0, 0, IblBoc::DataHigh) & 0xFF;
      //uint8_t dl = IblBoc::read(0, 0, IblBoc::DataLow) & 0xFF;
      //result.regs.push_back((dh << 8) + dl);
      result.regs.push_back(0);
      }
      break;
      //std::vector<int> reg = fes[i].readFrame();
      //result.regs.push_back(((reg[0] & 0xFF) << 8) + (reg[1] & 0xFF));
    }
  }

#endif 
}

void HalfClockReturn::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_modMask);
}

void HalfClockReturn::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_modMask = Serializer<uint32_t>::deserialize(in, offset);
}

uint32_t HalfClockReturn::serializedSize() const {
  return (sizeof(uint32_t));
}

void HalfClockReturnValue::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint8_t>::serialize(out, offset, nMod);
  for (int i = 0; i < nMod; i++) Serializer<uint16_t>::serialize(out, offset, regs[i]);
}

void HalfClockReturnValue::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  nMod = Serializer<uint8_t>::deserialize(in, offset);
  regs.resize(nMod);
  for (int i = 0; i < nMod; i++) {
    uint16_t reg;
    Serializer<uint16_t>::deserialize(in, offset, reg);
    regs[i] = reg;
  }
}

uint32_t HalfClockReturnValue::serializedSize() const {
  return (sizeof(uint8_t) + nMod * sizeof(uint16_t));
}
