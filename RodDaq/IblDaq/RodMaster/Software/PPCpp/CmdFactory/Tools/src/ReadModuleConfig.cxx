#include "ReadModuleConfig.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "Serializer.h"
#endif

ReadModuleConfig::ReadModuleConfig() { //todo parameter list
  setResultPtr(); 
  m_ConfigType = Physics;
  m_feMask = 0xFFFFFFFF;
  m_readFrontEnd = false;
  m_isFei3 = false;
  result.isFei3 = false;
  result.moduleConfigsFei3.clear();
  result.moduleConfigsFei4.clear();
}

void ReadModuleConfig::execute() {
  bool verbose = true;//todo move to be an option
  if (verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

#ifdef __XMK__

  result.nMod = 0;
  
  if (!m_isFei3) {
  result.isFei3 = false;
  // get set of configs from the resource manager, and then send back the ones we want
  RodResourceManagerIBL &rm = RodResourceManagerIBL::getInstance();

  int maxModules = rm.getNModuleConfigs();


  Fei4 *fes; //pointer to Fei4s

  if (verbose){
    std::cout<<"config Type to read is: "<<(int)m_ConfigType<<std::endl;
    std::cout<<"FE mask to read is: 0x"<< std::hex << (int)m_feMask << std::dec << std::endl;
    std::cout<<"read FE directly?: "<<(int)m_readFrontEnd<<std::endl;
  }

  if (m_ConfigType == Physics) fes = rm.getPhysicsModuleConfigs(); //fill fei4 array
  else if (m_ConfigType == Calib) fes = rm.getCalibModuleConfigs();
  else { std::cout<<" ERROR: did not specify Physics nor Calib config type"<<
		  "; will return without doing anything"<<std::endl;
    return;
  }

  //read the configs!
  if(m_readFrontEnd==false){
    for (int i = 0; i < maxModules; i++) {
      if (m_feMask & (1<<i)) {
	result.nMod++;
	if (verbose) std::cout<<"About to read config #: "<<int(i)<<std::endl;
	result.moduleConfigsFei4.push_back((Fei4ExtCfg)fes[i]);
      }
    }
  }
  else{
    std::cout << "We are reading directly from the FE" << std::endl;

    IblBocLink * boclink = RodResourceManagerIBL::getIblBocLink();
    boclink->enableRxChannels(m_feMask);

    for (int l = 0; l < maxModules; l++) {
      if (m_feMask & (1<<l)) {
	result.nMod++;

	Fei4ExtCfg extCfg;
	uint16_t translated_cfg[Fei4Cfg::numberOfRegisters];
	Fei4* fe =  RodResourceManagerIBL::getInstance().getCalibModuleConfigs();
	extCfg = fe[l]; // gets the tx/rx etc from ExtCfg

	for(unsigned i = 0; i < Fei4Cfg::numberOfRegisters ; i++){
	  if (verbose) std::cout << "Sending Cmd to rdRegister "  << i << std::endl;

	  fes[l].rdRegister(i);

	  std::vector<int> current_frame = fes[l].readFrame();
	  translated_cfg[i] = (0x100)*(current_frame[current_frame.size()-2]) + current_frame[current_frame.size() - 1];

	} 

	extCfg.setCfg(translated_cfg);

	if (verbose) std::cout<<"About to read config #: "<<int(l)<<std::endl;

	std::cout << "result.nMod is" << (int)result.nMod	<< std::endl;

	result.moduleConfigsFei4.push_back(extCfg);
      }//fe mask
    }//module loop
  }//read directly from front end else

     if (verbose){
       std::cout << "Going to dump the configs" << std::endl;
       for(size_t j = 0; j < result.moduleConfigsFei4.size(); j++){
	 result.moduleConfigsFei4[j].dump();
       }
    
       std::cout << "Just dumped the configs" << std::endl;
     }
  }
  else {
    result.isFei3 = true;
    RodResourceManagerPIX &rm = RodResourceManagerPIX::getInstance();

    int maxModules = rm.getNModuleConfigs();

    Fei3Mod *fes;

    if (m_ConfigType == Physics) fes = rm.getPhysicsModuleConfigs(); //fill fei4 array
    else if (m_ConfigType == Calib) fes = rm.getCalibModuleConfigs();
    else { std::cout<<" ERROR: did not specify Physics nor Calib config type"<<"; will return without doing anything"<<std::endl; return;}

    //read the configs!
    for (int i = 0; i < maxModules; i++) {
      if (m_feMask & (1<<i)) {
	result.nMod++;
	if (verbose) std::cout<<"About to read config #: "<<int(i)<<std::endl;
	result.moduleConfigsFei3.push_back((Fei3ModCfg)fes[i]);
      }
    }
  }
#else
  //Todo: emulator
#endif
}

void ReadModuleConfig::serialize(uint8_t *out) const
{
  uint32_t offset = 0;
  Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
  Serializer<uint32_t>::serialize(out, offset, m_feMask);
  Serializer<bool>::serialize(out, offset, m_readFrontEnd);
  Serializer<bool>::serialize(out, offset, m_isFei3);
}

void ReadModuleConfig::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
  m_feMask = Serializer<uint32_t>::deserialize(in, offset);
  m_readFrontEnd = Serializer<bool>::deserialize(in, offset);
  m_isFei3 = Serializer<bool>::deserialize(in, offset);
}

uint32_t ReadModuleConfig::serializedSize() const
{
  return (sizeof(ConfigType)+sizeof(uint32_t)+sizeof(bool)+sizeof(bool));
}

void RedModuleConfig::serialize(uint8_t *out) const {
   std::cout<<"in function: "<<__PRETTY_FUNCTION__<< std::endl;
   uint32_t offset = 0;
   Serializer<uint8_t>::serialize(out, offset, nMod);
   Serializer<bool>::serialize(out, offset, isFei3);
   std::cout << "moduleConfigs size is "  << ((isFei3) ? moduleConfigsFei3.size() : moduleConfigsFei4.size()) << std::endl;

   for (int i = 0; i < nMod; i++) {
     std::cout << "Serializing moduleConfig " << i <<std::endl; 
     if (isFei3) {
       Serializer<Fei3ModCfg>::serialize(out,offset,moduleConfigsFei3[i]);
     }
     else {
       Serializer<Fei4ExtCfg>::serialize(out,offset,moduleConfigsFei4[i]);
     }
   }
}
 
 void RedModuleConfig::deserialize(const uint8_t *in) {
   std::cout<<"in function: "<<__PRETTY_FUNCTION__<< std::endl;
   
   uint32_t offset = 0;
   nMod = Serializer<uint8_t>::deserialize(in,offset);
   isFei3 = Serializer<bool>::deserialize(in, offset);
   moduleConfigsFei3.resize(nMod);
   moduleConfigsFei4.resize(nMod);
   std::cout << "moduleConfigs size is "  << ((isFei3) ? moduleConfigsFei3.size() : moduleConfigsFei4.size()) << std::endl;

   for (int i = 0; i < nMod; i++) {
     if (isFei3) {
       Fei3ModCfg module;
       std::cout << "Deserializing moduleConfig " << i <<std::endl; 
       Serializer<Fei3ModCfg>::deserialize(in,offset, module);
       moduleConfigsFei3[i]=module;
     }
     else {
       Fei4ExtCfg module;
       std::cout << "Deserializing moduleConfig " << i <<std::endl; 
       Serializer<Fei4ExtCfg>::deserialize(in,offset, module);
       moduleConfigsFei4[i]=module;
     }
  }
}

uint32_t RedModuleConfig::serializedSize() const {
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<", nMod = "<< (int) nMod <<std::endl;
  if (isFei3) {
    Fei3ModCfg dummy;
    return (sizeof(uint8_t) + sizeof(bool) + nMod*(dummy.serializedSize()));
  }
  else {
    Fei4ExtCfg dummy;
    return (sizeof(uint8_t) + sizeof(bool) + nMod*(dummy.serializedSize()));
  }
  return 0;
}
