/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 28-August-2015
 *
 * Sets up slave to be ready for histogramming
 *
 */
// #define LL_DEBUG

#include "StartHisto.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

#include "HistCfgSerializer.h"

void StartHisto::execute() {
#ifdef __XMK__
  IblRodSlave s;
  
  bool success = s.startHisto(&histConfig_in, whichSlave); //  Send histConfig to slave
  std::cout<< (success ? "    started slave histogrammer " : " !! failed to start slave histogrammer ") << whichSlave << std::endl;

  histConfig_out = s.getHistCfg(whichSlave); //  Send request for histConfig to slave and pick it up when sent back

#ifdef LL_DEBUG //  Display what was sent back by the slave on the PPC UART
  for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
    std::cout << "Slave " << whichSlave << ", Unit " << slvUnit << " histogrammer configuration" << std::endl;

    std::cout << "nChips =           " << histConfig_out.cfg[slvUnit].nChips << std::endl;
    std::cout << "chipSel =          " << histConfig_out.cfg[slvUnit].chipSel << std::endl;
    std::cout << "maskStep =         " << histConfig_out.cfg[slvUnit].maskStep << std::endl;
    std::cout << "type =             " << histConfig_out.cfg[slvUnit].type << std::endl;
    std::cout << "mStepOdd =         " << histConfig_out.cfg[slvUnit].mStepOdd << std::endl;
    std::cout << "mStepEven =        " << histConfig_out.cfg[slvUnit].mStepEven << std::endl;
    std::cout << "expectedOccValue = " << histConfig_out.cfg[slvUnit].expectedOccValue << std::endl;
    std::cout << "enable =           " << histConfig_out.cfg[slvUnit].enable << std::endl;
    std::cout << "addrRange =        " << histConfig_out.cfg[slvUnit].addrRange << std::endl;
    std::cout << "scanId =           " << histConfig_out.cfg[slvUnit].scanId << std::endl;
    std::cout << "binId =            " << histConfig_out.cfg[slvUnit].binId << std::endl;
    std::cout << "maskId =           " << histConfig_out.cfg[slvUnit].maskId << std::endl;
  }
#endif

//  Fill values obtained from the slave into the result object to be sent back to host
  for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
    result.histConfig.cfg[slvUnit].nChips           = histConfig_out.cfg[slvUnit].nChips;
    result.histConfig.cfg[slvUnit].enable           = histConfig_out.cfg[slvUnit].enable;
    result.histConfig.cfg[slvUnit].type             = histConfig_out.cfg[slvUnit].type;
    result.histConfig.cfg[slvUnit].maskStep         = histConfig_out.cfg[slvUnit].maskStep;
    result.histConfig.cfg[slvUnit].mStepEven        = histConfig_out.cfg[slvUnit].mStepEven;
    result.histConfig.cfg[slvUnit].mStepOdd         = histConfig_out.cfg[slvUnit].mStepOdd;
    result.histConfig.cfg[slvUnit].chipSel          = histConfig_out.cfg[slvUnit].chipSel;
    result.histConfig.cfg[slvUnit].expectedOccValue = histConfig_out.cfg[slvUnit].expectedOccValue;
    result.histConfig.cfg[slvUnit].addrRange        = histConfig_out.cfg[slvUnit].addrRange;
    result.histConfig.cfg[slvUnit].scanId           = histConfig_out.cfg[slvUnit].scanId;
    result.histConfig.cfg[slvUnit].binId            = histConfig_out.cfg[slvUnit].binId;
    result.histConfig.cfg[slvUnit].maskId            = histConfig_out.cfg[slvUnit].maskId;
  }

 
#endif
}

void StartHisto::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig_in);
}
void StartHisto::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<uint32_t>::deserialize(in, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig_in);
}
uint32_t StartHisto::serializedSize() const
{
        return sizeof(uint32_t) + Serializer<IblSlvHistCfg>::size(histConfig_in);
}

void HistCfgCheck::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig); //  To forward histConfig to host

}
void HistCfgCheck::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig); //  To forward histConfig to host
}
uint32_t HistCfgCheck::serializedSize() const
{
        return Serializer<IblSlvHistCfg>::size(histConfig);
}
