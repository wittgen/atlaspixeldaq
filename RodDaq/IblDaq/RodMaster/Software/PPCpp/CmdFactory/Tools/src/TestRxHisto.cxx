/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 13-November-2015
 *
 * PPC Dummy Scan Driver
 *
 */

//  NOTE:  Although a channel mask exists as a parameter to be passed to MCCTestScan and Fei3TestScan,
//         it will be ignored until the Rx channel is no longer hard there (currently with value 19).

#include "TestRxHisto.h"
#include "HistCfgSerializer.h"
#include "RodMasterRegisters.h"
#include "MCCTestScan.h"
#include "Fei3TestScan.h"

void TestRxHisto::execute() {
#ifdef __XMK__

    int frameCount = 1;
#define RRIF_CTL_PPC_TRIG 0x80041425
#define RRIF_CTL_INITIAL  0x80000000

  IblRod::Master::RrifCtrlReg::write(RRIF_CTL_INITIAL);

  // enable calibration mode
  IblRod::Master::RrifCtrlReg::write( RRIF_CTL_PPC_TRIG);
  IblRod::Master::SprCtrlReg::write(0x0FF);
  IblRod::Master::DebugModeCnfgReg::write( 0x02000000 );

  // set number of frames per trigger on ROD
  IblRod::SlaveA::TriggerCountNumber::write(frameCount);
  IblRod::SlaveB::TriggerCountNumber::write(frameCount);

  // set time out for channels with no data
  IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0x9C40); //  20,000 bunch crossings
  IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0x9C40);
//   IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0x4E20); //  10,000 bunch crossings
//   IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0x4E20);

  // set limit of number of header + hit + trailer per module per trigger (how many data-records to process per frame) to 1680
  IblRod::SlaveA::FmtHeaderTrailerLimit::write(0x690);
  IblRod::SlaveB::FmtHeaderTrailerLimit::write(0x690);

  // set limit of number of trailer per module per trigger
  IblRod::SlaveA::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);
  IblRod::SlaveB::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);

  // set limit of how full the fifo is before asserting busy to 1680
  IblRod::SlaveA::FmtRodBusyLimit::write(0x690);
  IblRod::SlaveB::FmtRodBusyLimit::write(0x690);

  // put slaves into calibration mode (1 = don't send fragments over the slink "calibration mode")
  IblRod::SlaveA::CalibrationMode::write(0x1);
  IblRod::SlaveB::CalibrationMode::write(0x1);
//   IblRod::SlaveA::CalibrationMode::write(0x0); //  Use for some low-level chipscope debugging only
//   IblRod::SlaveB::CalibrationMode::write(0x0);

  IblRod::SlaveA::FmtLinkEnable::write(modMask & 0xFFFF);
  IblRod::SlaveB::FmtLinkEnable::write((modMask >> 16) & 0xFFFF);

  IblRodSlave s;

  int firstUnit = 0;
  int stopUnit = 2;
  if (histoUnit == 1) firstUnit = 1;
  else if (histoUnit == 0) stopUnit = 1;

  int nChips = histConfig_in.cfg[firstUnit].nChips ;
  uint32_t stepSize = (1 << histConfig_in.cfg[firstUnit].maskStep);

  for (uint32_t iMask = 0; iMask < stepSize; iMask++) { //  Note:  Mask steps are also treated as "bins" in some routines.
    for(int slvUnit = firstUnit; slvUnit < stopUnit; slvUnit++) {
      histConfig_in.cfg[slvUnit].enable = 1;
//       if(filter && nChips <= 8 && stepSize > 0) { //  Set up for Double-column shift mask stepping for FE-I4 only.
      if(filter && stepSize > 0) { //  Set up for Double-column shift mask stepping.
        histConfig_in.cfg[slvUnit].mStepOdd = iMask + 1;
        histConfig_in.cfg[slvUnit].mStepEven = stepSize - iMask;
      }
    } //  slvUnit

    bool success = s.startHisto(&histConfig_in, whichSlave); //  Send histConfig to slave
    std::cout<< (success ? "    started histogrammer(s) for slave " : " !! failed to start histogrammer(s) for slave ") << whichSlave << std::endl;

    if(success) {
      int doNothing = 0;
//       if(mode != 2 && iMask != 30 && iMask != 31) doNothing = 1; //  Just do actual MCC injection for last two mask steps.
//       if(mode != 2 && nMasks != 0 && iMask != nMasks) doNothing = 1; //  Just do actual MCC injection for nMasks mask steps.
      if(mode != 2 && nMasks != 0 && iMask >= nMasks) doNothing = 1; //  Just do actual MCC injection for first nMasks mask steps.
      if(!doNothing) {
        histConfig_out = s.getHistCfg(whichSlave); //  Send request for histConfig to slave and pick it up when sent back

        for(int slvUnit = firstUnit; slvUnit < stopUnit; slvUnit++) {
          std::cout << std::endl;
          if(mode == 2)
            std::cout << "Slave " << whichSlave << ", Unit " << slvUnit << " set up for Rx dummy scan with the following histogrammer configuration" << std::endl;
          else std::cout << "Slave " << whichSlave << ", Unit " << slvUnit << " set up for Rx test scan with the following histogrammer configuration" << std::endl;
          std::cout << "nChips =           " << histConfig_out.cfg[slvUnit].nChips << std::endl;
          std::cout << "enable =           " << histConfig_out.cfg[slvUnit].enable << std::endl;
          std::cout << "type =             " << histConfig_out.cfg[slvUnit].type << std::endl;
          std::cout << "maskStep =         " << histConfig_out.cfg[slvUnit].maskStep << std::endl;
          std::cout << "mStepOdd =         " << histConfig_out.cfg[slvUnit].mStepOdd << std::endl;
          std::cout << "mStepEven =        " << histConfig_out.cfg[slvUnit].mStepEven << std::endl;
          std::cout << "chipSel =          " << histConfig_out.cfg[slvUnit].chipSel << std::endl;
          std::cout << "expectedOccValue = " << histConfig_out.cfg[slvUnit].expectedOccValue << std::endl;
          std::cout << "addrRange =        " << histConfig_out.cfg[slvUnit].addrRange << std::endl;
          std::cout << "scanId =           " << histConfig_out.cfg[slvUnit].scanId << std::endl;
          std::cout << "binId =            " << histConfig_out.cfg[slvUnit].binId << std::endl;
          if(mode == 2) {
            int usms; //  = unit scan mask shift
            if(nChips > 8) usms = (128 - nChips) / 16;
            else usms = 8 - nChips;
            uint32_t unitMask = (0x000000FF >> usms); //  For the first nChips chips of unit 0 on slave 0;
            s.testRxBypass(&histConfig_in, iMask, unitMask, whichSlave, slvUnit);
          } //  mode = 2
        } //  slvUnit

        if(mode != 2) {
          // Generate scan injection patterns and send them to front ends
          std::cout << "Sending " << nTrig << " trigger(s) to Front End(s) with" << (!filter ? "" : "out") << " histogrammer filtering" << std::endl;
          if(mode == 1) {
            MCCTestScan mccTS;
            mccTS.histConfig_in.cfg[0].maskStep = histConfig_in.cfg[0].maskStep;
            mccTS.histConfig_in.cfg[0].mStepOdd = histConfig_in.cfg[0].mStepOdd;
            mccTS.histConfig_in.cfg[0].mStepEven = histConfig_in.cfg[0].mStepEven;
            mccTS.setModMask(modMask);
            mccTS.setNTrig(nTrig);
            mccTS.iMask = iMask;
            mccTS.histoFilter = filter;
            mccTS.execute();
          } else {
            Fei3TestScan Fei3TS;
            Fei3TS.histConfig_in.cfg[0].maskStep = histConfig_in.cfg[0].maskStep;
            Fei3TS.histConfig_in.cfg[0].mStepOdd = histConfig_in.cfg[0].mStepOdd;
            Fei3TS.histConfig_in.cfg[0].mStepEven = histConfig_in.cfg[0].mStepEven;
            Fei3TS.setModMask(modMask);
            Fei3TS.setNTrig(nTrig);
            Fei3TS.iMask = iMask;
            Fei3TS.histoFilter = filter;
            Fei3TS.execute();
          }
        } //  mode = 0
      } //  not doNothing
      s.stopHisto(whichSlave); //  Tell the slave it can start sending results to FitServer
      std::cout<< "    stopped histogrammer(s) for slave " << whichSlave << std::endl << std::endl;
    } else {
     std::cout << "* TestRxHisto will not be executed *" << std::endl;
    }
  } //  iMask

#endif
}

void TestRxHisto::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<uint32_t>::serialize(out, offset, histoUnit);
        Serializer<uint32_t>::serialize(out, offset, modMask);
        Serializer<uint32_t>::serialize(out, offset, nTrig);
        Serializer<uint32_t>::serialize(out, offset, mode);
        Serializer<uint32_t>::serialize(out, offset, filter);
        Serializer<uint32_t>::serialize(out, offset, nMasks);
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig_in);
}
void TestRxHisto::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<uint32_t>::deserialize(in, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<uint32_t>::deserialize(in, offset, histoUnit);
        Serializer<uint32_t>::deserialize(in, offset, modMask);
        Serializer<uint32_t>::deserialize(in, offset, nTrig);
        Serializer<uint32_t>::deserialize(in, offset, mode);
        Serializer<uint32_t>::deserialize(in, offset, filter);
        Serializer<uint32_t>::deserialize(in, offset, nMasks);
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig_in);
}
uint32_t TestRxHisto::serializedSize() const
{
        return 7*sizeof(uint32_t) + Serializer<IblSlvHistCfg>::size(histConfig_in);
}
