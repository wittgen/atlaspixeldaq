/*
 * Author: Nick Dreyer
 * Date: 2016-I-6
 * Description: this command can be used to test some Fei3 digital scan functionality
 */

//  Developement Note:  Currently this is only set up for a single Rx channel, i.e. 19, in spite of
//                      the feeble attempt to use a Channel mask by passing it in as a parameter.
//                      Much work is still needed to fully make use of this channel mask, if ever needed.
//                      The code remains obviously a work-in-progress in that regard, and whoever
//                      takes on the excercise of generalizing the use of Rx chanels will want to
//                      eliminate lots of useless redundancy left over from the initial developement.
//                      Basically whatever was started needs to be completely canned and redone,
//                      and I, ND, do not have it in me to do that now.
//
//                      I'm now merely resurecting Fei3TestScan
//                      to do no more than what was possible before other developments broke it.

#include <unistd.h>
#include "Fei3TestScan.h"

#include "HistCfgSerializer.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBocLink.h"
#include "BitUtils.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void Fei3TestScan::showTriggerData( const std::vector<uint8_t>& rxData ) {
    size_t cntr = 0;
    size_t mccHeaderPos = 0;
    size_t nTrig = 1;
    for(size_t k = 0 ; k < rxData.size() ; ++k) {
      bool isMccHeader = false;
      uint8_t skipBit = 0;
      if( rxData[k] == 0xe8 || rxData[k] == 0x74 || rxData[k] == 0x3a || rxData[k] == 0x1d ) {
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (rxData[k] & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      uint8_t firstWord = (rxData[k] << 4) + ((rxData[k+1] & 0xF0) >> 4);
      if( firstWord == 0xe8 || firstWord == 0x74 || firstWord == 0x3a || firstWord == 0x1d ) {
        skipBit += 4;
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (firstWord & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      if( isMccHeader ) {
        cntr = 0;
      } else skipBit = 0;

      for(size_t b = skipBit ; b < 8 ; ++b) {
        std::cout << (( rxData[k] & (0x1 << (7-b) )) ? "1" : "0" );
        if( (++cntr)%4 == 0 ) std::cout << " ";
      }
    }
}

Fei3TestScan::Fei3TestScan() {
  setResultPtr();
  m_modMask = 0xFFFFFFFF;
  result.data.clear();
}


void Fei3TestScan::execute() {
  std::cout << "in function: " << __PRETTY_FUNCTION__ <<std::endl;

#ifdef __XMK__

  result.nMod = 0;

  Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();

  IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;
  uint8_t mccRxSpeed = rxSpeed + (rxSpeed ? 1 : 0);
  IblBoc::PixSetSpeed( rxSpeed );
  std::cout << "MCC speed: " << HEX(mccRxSpeed) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

  uint8_t rxCh = 19; //  Just do this one for now.  It seems to but most reliable in SR1 setups.

  for (int iRxCh = 0 ; iRxCh < 32; iRxCh++) {
std::cout << iRxCh << ", rxCh = " << static_cast<int>(rxCh) << std::endl;
    if(iRxCh != rxCh) continue;
std::cout << "We made it !!" << std::endl;

    int iTxCh = iRxCh;
    std::cout << "Processing rxCh=" << iRxCh << " with txCh=" << iTxCh << std::endl;

    IblBoc::PixSpeed rxSpeed = IblBoc::Speed_40;
    uint8_t mccRxSpeed = rxSpeed + (rxSpeed ? 1 : 0);
    IblBoc::PixSetSpeed( rxSpeed );
    std::cout << "MCC speed: " << HEX(mccRxSpeed) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

    int8_t fibreNr = IblBoc::getMasterFibreFromRxCh(iRxCh);
    IblBoc::PixSetRxThreshold(fibreNr, 30000);
    IblBoc::PixSetRxRGS(fibreNr, 65500);
    // Drain BOC RX FIFO
    IblBocRxFifo rxFifo;
    rxFifo.setRxCh(iRxCh);
    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );

    // Enabling the RX and TX channels
    IblBoc::write(iTxCh/16, iTxCh%16, IblBoc::TxControl, 0x01);

    Fei3Mod &fe = fes[iRxCh];

    std::cout << "Processing rxCh=" << iRxCh << " with txCh=" << iTxCh << std::endl;
    fe.setTxCh(iTxCh);
    fe.setRxCh(iRxCh);

    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x00);

    IblBoc::PixSetTrailerLength(iRxCh, 22);

    // Sending half-clock before setting the fine delay, to allow it to kick in
    std::cout << "Setting RxDelay...";
    fe.writeMCCRegister(CSR, 0x0020);
    IblBoc::SetRxSampleDelay( fibreNr, 4, 10);
    //fe.writeMCCRegister(CSR, 0x0040 | mccRxSpeed ); // write to CSR to enable play back
    fe.writeMCCRegister(CSR, 0x0000 | mccRxSpeed ); // write to CSR to DISABLE play back
    std::cout << "Done" << std::endl;

    for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );
    IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x07);
    std::cout << "rxCh=" << static_cast<uint32_t>(iRxCh) << "; RxControl=" << HEX( IblBoc::read(iRxCh/16, iRxCh%16, IblBoc::RxControl) ) << std::endl;

    std::vector<Fei3HitData> hd;

    // Complete reset of the MCC and all FEs
    fe.mccGlobalResetMCC();
    fe.mccGlobalResetFE(10); // Hard FE reset


    uint16_t nConsecutiveTrig = 1;
    uint8_t trigDelay = 247;
    uint8_t theLatency = 248;

    uint8_t tot = iMask + 1;
    uint16_t pulseDuration = tot + 1;

#define NC 16
    int FEs[NC] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15};

    uint16_t fe_mask = 0x0;
    for(size_t  iFE = 0; iFE < NC; iFE++) fe_mask |= 1 << FEs[iFE];

    //uint8_t bcid = 0;

    const uint16_t nDC = COLSIZE/2;
    uint32_t bitstream[nDC][Fei3PixelCfg::nWordsPerDC] = {{ 0 }};

    uint8_t stepSize = (1 << histConfig_in.cfg[0].maskStep); //  Overkill perhaps, but for now histConfig_in just used to transfer in maskStep, wnd only in "unit" 0.
    uint32_t filterTestShiftLimit = 1; //  Must be at least 1 to do anything (1 = NO filter shift test)
    for(uint32_t filterTestShift = 0; filterTestShift < filterTestShiftLimit; filterTestShift++){
std::cout << "iMask = " << iMask << ", stepSize = " << (uint32_t)stepSize << ", ROWSIZE = " << ROWSIZE << std::endl;
      for(uint16_t iDC = 0; iDC < nDC; iDC++) {
        for(uint8_t iStep = 0; iStep < ROWSIZE/((uint32_t)stepSize); iStep++) {
          for(uint8_t dCol = 0; dCol < 2; ++dCol) {
            uint8_t iStepRow = iStep*stepSize;
            uint8_t row = iStepRow + iMask;
std::cout << "iStep = " << (uint32_t)iStep << std::endl;
            std::cout << "Sending " << m_nTrig << " trigger(s) per pixel to 'Mask Step row' " << (uint32_t)row << " with" << (!histoFilter ? "out" : "") << " histogrammer filtering" << std::endl;
              int mStepEven = stepSize - iMask - 1 - filterTestShift;
              int mStepOdd = iMask + filterTestShift;
              if(histoFilter) {
                 mStepEven = (histConfig_in.cfg[0].mStepEven - 1) - filterTestShift;
                 mStepOdd = ((histConfig_in.cfg[0].mStepOdd - 1) + filterTestShift)%32;
              }
              if (mStepEven < 0) mStepEven = 32 + mStepEven % 32;
              mStepEven %= 32;
              (dCol == 1) ? row = iStepRow + (uint8_t)mStepEven :
                            row = iStepRow + (uint8_t)mStepOdd;
              uint16_t col = 2*iDC + dCol;
std::cout << "col = " << (uint32_t)col << ", istepRow = " << (uint32_t)iStepRow << ", mstepEven = " << mStepEven << ", mstepOdd = " << mStepOdd << std::endl;

              uint32_t posInDC = (dCol ?  (319-row) : row);
              if(iDC == 0)bitstream[iDC][posInDC/32] = 0x1 << (posInDC%32); //  Only doing one DC at a time with same bit-pattern in all
//               break;
          } // dCol
        } // row
#if 1
        std::cout << "SR content" << std::endl;
        std::cout << "DC=" << iDC << std::endl;
        dumpData(bitstream[0], Fei3PixelCfg::nWordsPerDC);
//         for (int k = 0; k < nDC ; k++) {
//           std::cout << "DC=" << k << std::endl;
//           dumpData(bitstream[k], Fei3PixelCfg::nWordsPerDC);
//         }
#endif

        uint32_t cnt = 1;
        do {

          for(uint8_t  iFE = 0; iFE < NC; iFE++) {

            fe.setDefaultGlobalConfig(iFE);

            fe.writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 8);
            fe.writeGlobalReg(iFE, Fei3::GlobReg::EnableDigInj, 1);
            fe.writeGlobalReg(iFE, Fei3::GlobReg::VCal, 0);
            fe.writeGlobalReg(iFE, Fei3::GlobReg::Latency, theLatency);
            //fe.enableDC(iDC);
            fe.setAllPixelShiftRegisters(iFE, false);
            fe.writeGlobalReg(iFE, Fei3GlobalCfg::colEnReg(iDC), true);
            fe.writeGlobalReg(iFE, Fei3::GlobReg::CEU_Clock, 1);


            fe.sendGlobalConfig(iFE);
            //fe.sendPixelConfig(iFE);

            //fe.clearSR(); // In memory
            //fe.enablePixel(row); // In memory
            fe.strobePixelLatches(iFE, Fei3::PixLatch::Select, 1, bitstream); //  Sets FEs to receive signal
            fe.strobePixelLatches(iFE, Fei3::PixLatch::Enable, 1, bitstream); //  Sets FEs to return data
//             fe.strobePixelLatches(iFE, Fei3::PixLatch::Preamp, 1, bitstream); //  Turn FE Preamp on (only for analog injection) - causes problem for digital injection !!!

          } //iiFE
// std::cout << "Dump 1:" << std::endl; fe.dump();
          uint8_t calDelay = 0;
          uint8_t calDelayRange = 5;
          //calDelay = calDel;
          uint32_t regVal = (1 << 10) | ((calDelayRange&0xF) << 6) | ((calDelay&0x3F));
          std::cout << "calDelay=" << static_cast<uint32_t>(calDelay) << ", calDelayRange=" << static_cast<uint32_t>(calDelayRange) << std::endl;
          fe.writeMCCRegister(CAL,regVal);


          //fe.dumpGlobalConfigHR(iFE);

          fe.writeMCCRegister(LV1,(nConsecutiveTrig-1)<<8);
          fe.writeMCCRegister(CNT, (uint16_t)(pulseDuration));
          fe.writeMCCRegister(FEEN, fe_mask);
// std::cout << "Dump 2:" << std::endl; fe.dump();

#if 0
          // addHit(uint16_t fe_mask, uint8_t bcid, uint8_t row, uint8_t col, uint8_t tot)
          fe.addHit(0x0001, 0, 120, 6, 72);
          fe.addEOE(0x0001, 0);

          fe.addHit(0x0100, 0, 100, 5, 75);
          fe.addEOE(0x0100, 0);

#endif

          // Disabling FIFO and clearing it
          IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x01);
          for(int k = 0 ; k < 8192 ; ++k) IblBoc::read( iRxCh/16, iRxCh%16, IblBoc::DataLow );
          IblBoc::write(iRxCh/16, iRxCh%16, IblBoc::RxControl, 0x07);

          IblBoc::PixSetTrailerLength(rxCh, 22);
//           fe.dumpMccRegisters(); //  This destroys communication to histogrammer !!!!!

          fe.mccEnDataTake(); // enable data taking
// std::cout << "Dump 3:" << std::endl; fe.dump();

#if 0
          for(int j = 19 ; j < 20 ; j++) {
            rxFifo.setRxCh(j);
            std::cout << j;
            for(size_t k = 0 ; k < 10 ; ++k )
              //std::cout << "\t" << HEX( IblBoc::read(j/16, j%16, IblBoc::DataLow) & 0xFF );
              std::cout << "\t" << HEX( rxFifo.read() );
            std::cout << std::endl;
          }
#endif

#if TEST_MCC_REGISTER_READ_BACK
          for( size_t k = 0 ; k < 10 ; ++k ) {
            fe.mccRdRegister(0x0);
            for(size_t k = 0 ; k < 10 ; ++k )
              std::cout << "\t" << HEX( rxFifo.read() );
            std::cout << std::endl;
          }
#endif

          usleep(100000);

          if(!m_nTrig) continue;
          fe.sendCALandLVL1(trigDelay);
          std::cout << cnt << " --- ";
#if 0
          for(size_t k = 0 ; k < 100 ; ++k )
            std::cout << "\t" << HEX( IblBoc::read( irxCh/16, irxCh%16, IblBoc::DataLow ) );
            //std::cout << "\t" << HEX( rxFifo.read() );
          std::cout << std::endl;
#else
          std::vector<Fei3HitData> hd = fe.readHitData();
          Fei3ModData::decodeRawHitData( hd );
#endif
        } while (cnt++ < m_nTrig); //  Trigger

        usleep(100000);
#if 0
        std::vector<uint8_t> tmpRxData;
        tmpRxData.reserve(nConsecutiveTrig*8);
        for(size_t k = 0 ; k < tmpRxData.capacity() ; ++k) tmpRxData.push_back( IblBoc::read( irxCh/16, irxCh%16, IblBoc::DataLow ) );
        std::cout << "tmpRxData.size()=" << tmpRxData.size() << std::endl;
        dumpData( tmpRxData );
        showTriggerData(tmpRxData);
#endif
//         break;
      } // iDC
//       break;
    } //filterTestShift

  } // iRxCh

  for (int i = 0 ; i < 32; i++) {
    IblBoc::write(i/16, i%16, IblBoc::RxControl, 0x00);
  }

  result.data.push_back(0);

#endif

}

void Fei3TestScan::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint32_t>::serialize(out, offset, m_modMask);
  Serializer<uint32_t>::serialize(out, offset, m_nTrig);
  Serializer<uint32_t>::serialize(out, offset, iMask);
  Serializer<uint32_t>::serialize(out, offset, histoFilter);
  Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig_in);
}

void Fei3TestScan::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  m_modMask = Serializer<uint32_t>::deserialize(in, offset);
  m_nTrig = Serializer<uint32_t>::deserialize(in, offset);
  iMask = Serializer<uint32_t>::deserialize(in, offset);
  histoFilter = Serializer<uint32_t>::deserialize(in, offset);
  Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig_in);
}

uint32_t Fei3TestScan::serializedSize() const {
  return (sizeof(uint32_t)*4) + Serializer<IblSlvHistCfg>::size(histConfig_in);
}

void Fei3TestScanValue::serialize(uint8_t *out) const {
  uint32_t offset = 0;
  Serializer<uint8_t>::serialize(out, offset, nMod);
  for (int i = 0; i < nMod; i++) Serializer<uint8_t>::serialize(out, offset, data[i]);
}

void Fei3TestScanValue::deserialize(const uint8_t *in) {
  uint32_t offset = 0;
  nMod = Serializer<uint8_t>::deserialize(in, offset);
  data.resize(nMod);
  for (int i = 0; i < nMod; i++) {
    uint8_t reg;
    Serializer<uint8_t>::deserialize(in, offset, reg);
    data[i] = reg;
  }
}

uint32_t Fei3TestScanValue::serializedSize() const {
  return (sizeof(uint8_t) + nMod * sizeof(uint8_t));
}
