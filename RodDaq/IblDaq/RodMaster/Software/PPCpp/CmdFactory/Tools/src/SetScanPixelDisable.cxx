#include "SetScanPixelDisable.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "Fei4.h"
#include <iomanip>
#endif

SetScanPixelDisable::SetScanPixelDisable() {
  setResultPtr();
  rx = 0;
  dc = 0;
  pixel = 0;
  clear = false;
  clearAdd = false;
}

void  SetScanPixelDisable::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
  
  RodResourceManagerIBL &rm = RodResourceManagerIBL::getInstance();

  std::cout<<"For RX: "<<(int)rx<<"disabling DC: "<<(int)dc<<" and pixel: "<<(int)pixel<<" with clear: "<<(int)clear<<" and clearAdd: "<<(int)clearAdd<<std::endl;

  Fei4 &f = rm.getCalibModuleConfig(rx);

  if (clear) {
    f.clearScanDisablePixel();
    return;
  }
  if (clearAdd) f.clearScanDisablePixel();
  f.addScanDisablePixel(dc,pixel); 

#endif
}

