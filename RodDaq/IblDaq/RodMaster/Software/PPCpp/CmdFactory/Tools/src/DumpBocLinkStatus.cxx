/*
 * Authors: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-X-29
 */

#include "DumpBocLinkStatus.h"

#include "IblBoc.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void DumpBocLinkStatus::execute() {
#ifdef __XMK__
	for(int slaveId = 0 ; slaveId < 2 ; ++slaveId) {
		std::cout << "SlaveId = " << slaveId << std::endl;
		for(int tx_ch = 0 ; tx_ch < 8 ; ++tx_ch) {
			uint8_t readValue = IblBoc::read(slaveId, tx_ch, IblBoc::TxStatus);
			std::cout << "  +tx_ch = " << tx_ch << " ; value = " << HEX(readValue) << std::endl;
		}
		for(int rx_ch = 0 ; rx_ch < 16 ; ++rx_ch) {
			uint8_t readValue = IblBoc::read(slaveId, rx_ch, IblBoc::RxStatus);
			std::cout << "  +rx_ch = " << rx_ch << " ; value = " << HEX(readValue) << std::endl;
		}
	}


	std::cout << "LASER?" << std::endl;
	std::cout << std::hex << (int)IblBoc::read(0x7) << std::dec << std::endl;
	volatile unsigned long* bocReg = (volatile unsigned long*)BOC_REG_BASE;
	std::cout << std::hex << (int)bocReg[0x7] << std::dec << std::endl;
	
#else
	std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

void DumpBocLinkStatus::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	(void)offset;
}

void DumpBocLinkStatus::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	(void)offset;
}

uint32_t DumpBocLinkStatus::serializedSize() const
{
	return  0;
}

void BocLinkStatus::serialize(uint8_t *out) const {
	uint32_t offset = 0;
	(void)offset;
}

void BocLinkStatus::deserialize(const uint8_t *in) {
	uint32_t offset = 0;
	(void)offset;
}

uint32_t BocLinkStatus::serializedSize() const {
	return 0;
}
