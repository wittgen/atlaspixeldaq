#include "GetServiceRecords.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "Fei4.h"
#include <iomanip>
#endif

GetServiceRecords::GetServiceRecords() {
  setResultPtr();
}

void  GetServiceRecords::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
  
  for (int i = 0; i < 32; i++) {
    
    Fei4 * fe = &(RodResourceManagerIBL::getCalibModuleConfig(i));
    std::copy((*fe).m_serviceRecord, (*fe).m_serviceRecord+32, result.srValues[i]);
  }
  
#endif
}

