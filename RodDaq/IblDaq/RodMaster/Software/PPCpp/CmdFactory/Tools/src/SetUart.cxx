/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "SetUart.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

SetUart::SetUart(){ 
  setResultPtr(); 
  uart = 0;  // default set uart to 0
}

void SetUart::execute() {
#ifdef __XMK__
  IblRodSlave s;
  s.setUart(uart);
#endif
}

void SetUart::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint8_t>::serialize(out,offset,uart);
}
void SetUart::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	uart = Serializer<uint8_t>::deserialize(in,offset);
}
uint32_t SetUart::serializedSize() const
{
	return sizeof(uint8_t);
}
