/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 11-September-2015
 *
 * List slave histogrammer configuration
 *
 */
#define LL_DEBUG

#include "CheckHistCfg.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

#include "HistCfgSerializer.h"

void CheckHistCfg::execute() {
#ifdef __XMK__
  IblRodSlave s;
  
  histConfig = s.getHistCfg(whichSlave); //  Send request for histConfig to slave and pick it up when sent back

#ifdef LL_DEBUG //  Display what was sent back by the slave on the PPC UART
  for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
    std::cout << "Slave " << whichSlave << ", Unit " << slvUnit << " histogrammer configuration" << std::endl;
  
    std::cout << "nChips =           " << histConfig.cfg[slvUnit].nChips << std::endl;
    std::cout << "enable =           " << histConfig.cfg[slvUnit].enable << std::endl;
    std::cout << "type =             " << histConfig.cfg[slvUnit].type << std::endl;
    std::cout << "maskStep =         " << histConfig.cfg[slvUnit].maskStep << std::endl;
    std::cout << "mStepEven =        " << histConfig.cfg[slvUnit].mStepEven << std::endl;
    std::cout << "mStepOdd =         " << histConfig.cfg[slvUnit].mStepOdd << std::endl;
    std::cout << "chipSel =          " << histConfig.cfg[slvUnit].chipSel << std::endl;
    std::cout << "expectedOccValue = " << histConfig.cfg[slvUnit].expectedOccValue << std::endl;
    std::cout << "addrRange =        " << histConfig.cfg[slvUnit].addrRange << std::endl;
    std::cout << "scanId =           " << histConfig.cfg[slvUnit].scanId << std::endl;
    std::cout << "binId =            " << histConfig.cfg[slvUnit].binId << std::endl;
  }
#endif
 
//  Fill values obtained from the slave into the result object to be sent back to host
  for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) { 
    result.histConfig.cfg[slvUnit].nChips           = histConfig.cfg[slvUnit].nChips;
    result.histConfig.cfg[slvUnit].enable           = histConfig.cfg[slvUnit].enable;
    result.histConfig.cfg[slvUnit].type             = histConfig.cfg[slvUnit].type;
    result.histConfig.cfg[slvUnit].maskStep         = histConfig.cfg[slvUnit].maskStep;
    result.histConfig.cfg[slvUnit].mStepEven        = histConfig.cfg[slvUnit].mStepEven;
    result.histConfig.cfg[slvUnit].mStepOdd         = histConfig.cfg[slvUnit].mStepOdd;
    result.histConfig.cfg[slvUnit].chipSel          = histConfig.cfg[slvUnit].chipSel;
    result.histConfig.cfg[slvUnit].expectedOccValue = histConfig.cfg[slvUnit].expectedOccValue;
    result.histConfig.cfg[slvUnit].addrRange        = histConfig.cfg[slvUnit].addrRange;
    result.histConfig.cfg[slvUnit].scanId           = histConfig.cfg[slvUnit].scanId;
    result.histConfig.cfg[slvUnit].binId            = histConfig.cfg[slvUnit].binId;
  }

#endif
}

void CheckHistCfg::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
}
void CheckHistCfg::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<uint32_t>::deserialize(in, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
}
uint32_t CheckHistCfg::serializedSize() const
{
        return Serializer<uint32_t>::size(whichSlave);
}

void CheckHistResult::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig); //  To forward histConfig to host
	
}
void CheckHistResult::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig); //  To forward histConfig to host
}
uint32_t CheckHistResult::serializedSize() const
{
        return Serializer<IblSlvHistCfg>::size(histConfig);
}
