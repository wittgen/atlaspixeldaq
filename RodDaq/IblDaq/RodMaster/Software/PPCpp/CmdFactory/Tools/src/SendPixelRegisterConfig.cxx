#include "SendPixelRegisterConfig.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#endif


SendPixelRegisterConfig::SendPixelRegisterConfig():
  m_feMask (0x00010000) //todo remove PIXELLAB default
  //m_ConfigType (Physics)
{
  setResultPtr();
}


void SendPixelRegisterConfig::execute() {
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  result.success = false;
  // bool verbose = true;

#ifdef __XMK__
  /*
  RodResourceManagerIBL &rm = RodResourceManagerIBL::getInstance();

  // don't let user request more configs than stored by RodResourceManager
 int maxModules = rm.getNModuleConfigs();


  Fei4 * fes;            

  if (verbose) std::cout<<"config Type to send is: "<<m_ConfigType<<std::endl;
  if (verbose) std::cout<<"# of modules to configure is: "<<int(maxModules)<<std::endl;

  //get (pixel) cfg stored on the rod
  if (m_ConfigType == Physics) fes = rm.getPhysicsModuleConfigs();
  else if (m_ConfigType == Calib) fes = rm.getCalibModuleConfigs();
  else { std::cerr<<" ERROR: did not specify Physics nor Calib config type"<<
		  "; will return without doing anything"<<std::endl;
    return;
  }



  int usedFes = 0;
  const int numberOfDoubleColumns = Fei4PixelCfg::numberOfDoubleColumns;
 

  for (uint8_t i = 0; i < maxModules; i++) {
    if (m_feMask & (1<<i)) {
      if (verbose){
	std::cout<<"About to write config #: "<<int(i)<<std::endl;
	(fes[i]).setVerbose();
      }
      int dc = 0;
      fes[i].setRunMode(false);

      fes[i].writeRegister(&Fei4::CP, 0x00);// write only to selected double column
      fes[i].writeRegister(&Fei4::Colpr_Addr, dc); //select the zeroth dc 
      //todo just do all the doublecolumns at once

      //todo hardcoded 13 
      for(unsigned j = 0; j < 13 ; j++){ //loop over each doublebitcolumn
	std::cout << "Looping on the DoubleColumnBit" << j <<   " in a double column" << std::endl; 
	fes[i].wrFrontEnd(fes[i].getNthDoubleColumnBit(j , dc).getDoubleColumnBit() ) ;//todo awkward function names

	fes[i].writeRegister(&Fei4::Pixel_latch_strobe, (0x1) << j ); //fill every latch from configuration ()
	fes[i].writeRegister(&Fei4::LEN, 0x1); //turn on the latch enable bit

	fes[i].globalPulse(10);//pulse to load latches (should be configured here??)

	fes[i].writeRegister(&Fei4::LEN, 0x0); //turn off the latch enable bit
	fes[i].writeRegister(&Fei4::Pixel_latch_strobe,0x0);//empty the pixel latch strobe
      
      }
      //      fes[i].wrFrontEnd(fes[i].getOutputEnable(dc).getDoubleColumnBit());






      //      fes[i].setPixelCfg(m_pixelCfg); 
      //     for(uint8_t dc=0; dc < numberOfDoubleColumns ; dc++){//loop doublecolumns
	//fill the pixel configs of fes[i] with the pixel configs received from host
	//	fes[i].setTDAC(dc , m_pixelCfg.getTDAC(dc)); 
	
	//}

      if (verbose) {
	std::cout << "dumping config for double column 0" << std::endl;
	fes[i].dumpPix(0); //0 is for 0th double column
	std::cout << "dumping config for double column 1" << std::endl;
	fes[i].dumpPix(1); //0 is for 0th double column

      }
      usedFes++;
    }
  }

  

  */

#endif

  result.success = true;
}

void SendPixelRegisterConfig::serialize(uint8_t *out) const{
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  (void)offset;
  //  Serializer<uint32_t>::serialize(out,offset,m_feMask);
  //  Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
  //  Serializer<SerializableFei4PixelCfg>::serialize(out,offset,m_pixelCfg);
}

void SendPixelRegisterConfig::deserialize(const uint8_t *in){
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  (void)offset;
  //  Serializer<uint32_t>::deserialize(in, offset,m_feMask);
  //Serializer<ConfigType>::deserialize(in, offset,m_ConfigType);
  //Serializer<SerializableFei4PixelCfg>::deserialize(in,offset,m_pixelCfg);
}

uint32_t SendPixelRegisterConfig::serializedSize() const{
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  return(sizeof(uint32_t) + sizeof(ConfigType));
  //  return(sizeof(uint32_t) + sizeof(ConfigType)  + m_pixelCfg.serializedSize());
}

void SentPixelRegisterConfig::serialize(uint8_t *out) const {
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  Serializer<bool>::serialize(out, offset, success);
}

void SentPixelRegisterConfig::deserialize(const uint8_t *in) {
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  uint32_t offset = 0;
  success = Serializer<bool>::deserialize(in, offset);
}

uint32_t SentPixelRegisterConfig::serializedSize() const {
  std::cout <<__PRETTY_FUNCTION__<<std::endl;
  return sizeof(bool);
}
