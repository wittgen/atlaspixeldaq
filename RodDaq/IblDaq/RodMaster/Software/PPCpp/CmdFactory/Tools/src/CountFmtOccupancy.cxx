/*
 * L. Jeanty <laura.jeanty@cern.ch>
 * Date: 2013-Sep-30
 */

#include "CountFmtOccupancy.h"
#include "RodMasterRegisters.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec


void formatterOccupancyConverter(unsigned long lsb, unsigned long msb, int*occupancies)
{

  unsigned long long bigdummy;

  bigdummy=static_cast<unsigned long long>(lsb)+(static_cast<unsigned long long>(msb)*4294967296ll);
  occupancies[0]=(int)(bigdummy&0x3FF);
  occupancies[1]=(int)((bigdummy>>10)&0x3FF);
  occupancies[2]=(int)((bigdummy>>20)&0x3FF);
  occupancies[3]=(int)((bigdummy>>30)&0x3FF);
}

void CountFmtOccupancy::dump() {

  std::cout<<" ===================================================================="<<std::endl;
  std::cout<<std::endl;

  std::cout << "SlaveA::Fmt0OccupancyRegMSB @ " << HEX( IblRod::SlaveA::Fmt0OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveA::Fmt0OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveA::Fmt1OccupancyReg @ " << HEX( IblRod::SlaveA::Fmt1OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt1OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveA::Fmt1OccupancyRegMSB @ " << HEX( IblRod::SlaveA::Fmt1OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveA::Fmt1OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveA::Fmt2OccupancyReg @ " << HEX( IblRod::SlaveA::Fmt2OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt2OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveA::Fmt2OccupancyRegMSB @ " << HEX( IblRod::SlaveA::Fmt2OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveA::Fmt2OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveA::Fmt3OccupancyReg @ " << HEX( IblRod::SlaveA::Fmt3OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt3OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveA::Fmt3OccupancyRegMSB @ " << HEX( IblRod::SlaveA::Fmt3OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveA::Fmt3OccupancyReg::read(1) ) << std::endl;


  std::cout << "SlaveB::Fmt0OccupancyReg @ " << HEX( IblRod::SlaveB::Fmt0OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt0OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveB::Fmt0OccupancyRegMSB @ " << HEX( IblRod::SlaveB::Fmt0OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveB::Fmt0OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveB::Fmt1OccupancyReg @ " << HEX( IblRod::SlaveB::Fmt1OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt1OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveB::Fmt1OccupancyRegMSB @ " << HEX( IblRod::SlaveB::Fmt1OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveB::Fmt1OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveB::Fmt2OccupancyReg @ " << HEX( IblRod::SlaveB::Fmt2OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt2OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveB::Fmt2OccupancyRegMSB @ " << HEX( IblRod::SlaveB::Fmt2OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveB::Fmt2OccupancyReg::read(1) ) << std::endl;
  std::cout << "SlaveB::Fmt3OccupancyReg @ " << HEX( IblRod::SlaveB::Fmt3OccupancyReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt3OccupancyReg::read() ) << std::endl;
  std::cout << "SlaveB::Fmt3OccupancyRegMSB @ " << HEX( IblRod::SlaveB::Fmt3OccupancyReg::at(1) ) << ": " << HEX( IblRod::SlaveB::Fmt3OccupancyReg::read(1) ) << std::endl;

  std::cout<<" ===================================================================="<<std::endl;
  std::cout<<std::endl;
}

CountFmtOccupancy::CountFmtOccupancy(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void CountFmtOccupancy::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__

  unsigned long occ;
  unsigned long occMSB;

  int link[4];

  occ = IblRod::SlaveA::Fmt0OccupancyReg::read();
  occMSB = IblRod::SlaveA::Fmt0OccupancyReg::read(1);
  result.slvA_Fmt0Occupancy = occ;
  result.slvA_Fmt0OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvA_Fmt0Occupancy_Link0 = link[0];
  result.slvA_Fmt0Occupancy_Link1 = link[1];
  result.slvA_Fmt0Occupancy_Link2 = link[2];
  result.slvA_Fmt0Occupancy_Link3 = link[3];
  occ = IblRod::SlaveA::Fmt1OccupancyReg::read();
  occMSB = IblRod::SlaveA::Fmt1OccupancyReg::read(1);
  result.slvA_Fmt1Occupancy = occ;
  result.slvA_Fmt1OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvA_Fmt1Occupancy_Link0 = link[0];
  result.slvA_Fmt1Occupancy_Link1 = link[1];
  result.slvA_Fmt1Occupancy_Link2 = link[2];
  result.slvA_Fmt1Occupancy_Link3 = link[3];
  occ = IblRod::SlaveA::Fmt2OccupancyReg::read();
  occMSB = IblRod::SlaveA::Fmt2OccupancyReg::read(1);
  result.slvA_Fmt2Occupancy = occ;
  result.slvA_Fmt2OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvA_Fmt2Occupancy_Link0 = link[0];
  result.slvA_Fmt2Occupancy_Link1 = link[1];
  result.slvA_Fmt2Occupancy_Link2 = link[2];
  result.slvA_Fmt2Occupancy_Link3 = link[3];
  occ = IblRod::SlaveA::Fmt3OccupancyReg::read();
  occMSB = IblRod::SlaveA::Fmt3OccupancyReg::read(1);
  result.slvA_Fmt3Occupancy = occ;
  result.slvA_Fmt3OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvA_Fmt3Occupancy_Link0 = link[0];
  result.slvA_Fmt3Occupancy_Link1 = link[1];
  result.slvA_Fmt3Occupancy_Link2 = link[2];
  result.slvA_Fmt3Occupancy_Link3 = link[3];

  occ = IblRod::SlaveB::Fmt0OccupancyReg::read();
  occMSB = IblRod::SlaveB::Fmt0OccupancyReg::read(1);
  result.slvB_Fmt0Occupancy = occ;
  result.slvB_Fmt0OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvB_Fmt0Occupancy_Link0 = link[0];
  result.slvB_Fmt0Occupancy_Link1 = link[1];
  result.slvB_Fmt0Occupancy_Link2 = link[2];
  result.slvB_Fmt0Occupancy_Link3 = link[3];
  occ = IblRod::SlaveB::Fmt1OccupancyReg::read();
  occMSB = IblRod::SlaveB::Fmt1OccupancyReg::read(1);
  result.slvB_Fmt1Occupancy = occ;
  result.slvB_Fmt1OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvB_Fmt1Occupancy_Link0 = link[0];
  result.slvB_Fmt1Occupancy_Link1 = link[1];
  result.slvB_Fmt1Occupancy_Link2 = link[2];
  result.slvB_Fmt1Occupancy_Link3 = link[3];
  occ = IblRod::SlaveB::Fmt2OccupancyReg::read();
  occMSB = IblRod::SlaveB::Fmt2OccupancyReg::read(1);
  result.slvB_Fmt2Occupancy = occ;
  result.slvB_Fmt2OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvB_Fmt2Occupancy_Link0 = link[0];
  result.slvB_Fmt2Occupancy_Link1 = link[1];
  result.slvB_Fmt2Occupancy_Link2 = link[2];
  result.slvB_Fmt2Occupancy_Link3 = link[3];
  occ = IblRod::SlaveB::Fmt3OccupancyReg::read();
  occMSB = IblRod::SlaveB::Fmt3OccupancyReg::read(1);
  result.slvB_Fmt3Occupancy = occ;
  result.slvB_Fmt3OccupancyMSB = occMSB;
  formatterOccupancyConverter(occ, occMSB, link);
  result.slvB_Fmt3Occupancy_Link0 = link[0];
  result.slvB_Fmt3Occupancy_Link1 = link[1];
  result.slvB_Fmt3Occupancy_Link2 = link[2];
  result.slvB_Fmt3Occupancy_Link3 = link[3];

  if(dumpOnPPC) dump();

#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}



//  LocalWords:  endl
