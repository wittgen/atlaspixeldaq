#include "ResetRodBusyHistograms.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void ResetRodBusyHistograms::dump() {

  std::cout<<"I'm about to write the busy mask on the ROD master"<<std::endl;
}

ResetRodBusyHistograms::ResetRodBusyHistograms(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
}

// inside execute() the actual meat of the command happens on the ppc
void ResetRodBusyHistograms::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__
  int value;

  // read the current control register value from the master
  value = IblRod::MasterBusyMask::read();
  std::cout<<"Old master control register state: "<<value<<std::endl;
  
  // set master reset bit:
  int valueReset =  value + 0x00010000;
  IblRod::MasterBusyMask::write(valueReset);
    std::cout<<"Writing to reset: "<<valueReset<<std::endl;
  
  // write back old control register setting to master
  IblRod::MasterBusyMask::write(value);
  std::cout<<"Setting master control registers back to: "<<value<<std::endl;
  
  
  
     
  // read the current control register value from slave A
  value = 0;
  value = IblRod::SlaveA::BusyReg::read();
  std::cout<<"Old slave A control register state: "<<value<<std::endl;
  
  // set master reset bit:
  valueReset =  value + 0x00000001;
  IblRod::SlaveA::BusyReg::write(valueReset);
  std::cout<<"Writing to reset: "<<valueReset<<std::endl;
  
  // write back old control register setting to master
  IblRod::SlaveA::BusyReg::write(value);
  std::cout<<"Setting slave A control registers back to: "<<value<<std::endl;
  
  
  
  
  // read the current control register value from slave B
  value = 0;
  value = IblRod::SlaveB::BusyReg::read();
  std::cout<<"Old slave B control register state: "<<value<<std::endl;
  
  // set master reset bit:
  valueReset =  value + 0x00000001;

  IblRod::SlaveB::BusyReg::write(valueReset);
  std::cout<<"Writing to reset: "<<valueReset<<std::endl;
  
  // write back old control register setting to master
  IblRod::SlaveB::BusyReg::write(value);
  std::cout<<"Setting slave B control registers back to: "<<value<<std::endl;
  
  
  
  
  
 
  
  
  result.resetSuccessful = true;  
  
  // read the master histograms for crosscheck
  value = 0x0000ffff & IblRod::MasterBusyOutHist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyOutHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyRodBusyNinP0Hist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyRodBusyNinP0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyRodBusyNinP1Hist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyRodBusyNinP1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyEfbHeaderPauseHist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyEfbHeaderPauseHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyFormatterMB0Hist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyFormatterMB0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyFormatterMB1Hist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyFormatterMB1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::MasterBusyRolTestPauseHist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of MasterBusyRolTestPauseHist not successful!"<<std::endl;
    }






  // read the slave A histograms for crosscheck
  value = 0x0000ffff & IblRod::SlaveA::BusySlink0LffHist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusySlink0LffHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusySlink0DownHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusySlink0DownHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusySlink1LffHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusySlink1LffHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusySlink1DownHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusySlink1DownHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyEfb1Stts0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyEfb1Stts0Hist; not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyEfb1Stts1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyEfb1Stts1Hist; not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyEfb2Stts0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyEfb2Stts0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyEfb2Stts1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyEfb2Stts1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyRouter0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyRouter0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyRouter1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyRouter1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyHistfull0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyHistfull0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyHistfull1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyHistfull1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyHistoverrun0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyHistoverrun0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyHistoverrun1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyHistoverrun1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyFmt0Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt0Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt0Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt0Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt0Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyFmt1Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt1Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt1Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt1Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt1Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyFmt2Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt2Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt2Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt2Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt2Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyFmt3Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt3Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt3Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt3Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveA::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyFmt3Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveA::BusyOutHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveA BusyOutHist not successful!"<<std::endl;
    }              
 


// read the slave B histograms for crosscheck
  value = 0x0000ffff & IblRod::SlaveB::BusySlink0LffHist::read();
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusySlink0LffHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusySlink0DownHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusySlink0DownHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusySlink1LffHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusySlink1LffHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusySlink1DownHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusySlink1DownHist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyEfb1Stts0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyEfb1Stts0Hist; not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyEfb1Stts1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyEfb1Stts1Hist; not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyEfb2Stts0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyEfb2Stts0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyEfb2Stts1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyEfb2Stts1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyRouter0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyRouter0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyRouter1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyRouter1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyHistfull0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyHistfull0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyHistfull1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyHistfull1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyHistoverrun0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyHistoverrun0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyHistoverrun1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyHistoverrun1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyFmt0Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt0Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt0Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt0Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt0Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt0Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyFmt1Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt1Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt1Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt1Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt1Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt1Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyFmt2Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt2Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt2Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt2Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt2Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt2Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyFmt3Ff0Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt3Ff0Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt3Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt3Ff1Hist not successful!"<<std::endl;
    }
  value = 0x0000ffff & IblRod::SlaveB::BusyFmt3Ff1Hist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyFmt3Ff1Hist not successful!"<<std::endl;
    }

  value = 0x0000ffff & IblRod::SlaveB::BusyOutHist::read();  
  if (value != 0){
    result.resetSuccessful = false;
    std::cout<<"Reset of SlaveB BusyOutHist not successful!"<<std::endl;
    }


  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void ResetRodBusyHistograms::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
}

// Deseralize the data members of the command
// In this case, only one boolean
void ResetRodBusyHistograms::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t ResetRodBusyHistograms::serializedSize() const
{
	return (sizeof(bool));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void ResetRodBusyHistogramsResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<bool>::serialize(out, offset, resetSuccessful);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void ResetRodBusyHistogramsResult::deserialize(const uint8_t *in) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  resetSuccessful = Serializer<bool>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t ResetRodBusyHistogramsResult::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (sizeof(bool));
}
