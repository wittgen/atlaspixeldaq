#include "TestMapping.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Fei4.h"
#include "IblConnectivity.h"
#include "IblBocLink.h"
#include <iomanip>
#endif

TestMapping::TestMapping() {
  channels = 0xFFFFFFFF; // channels to test
  configType = Physics;
}

void  TestMapping::execute() {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__
  
  // configure the modules and enable the boc rx/tx links
  IblBocLink * iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  if( !iblBocLink->enableChannels(channels)) {
    std::cout<<"WARNING: could not enable all the desired channels. Will continue anyway"<<std::endl;
    channels = iblBocLink->getEnChMask();
  }
  std::cout<<std::endl;
  std::cout<<"**********************************"<<std::endl;
  std::cout<<"Enabled channel mask = 0x"<<std::hex<<channels<<std::dec<<std::endl;

  RodResourceManagerIBL &rm = RodResourceManagerIBL::getInstance();
  Fei4 *fes = NULL;

  if (configType == Physics) fes = rm.getPhysicsModuleConfigs();
  else if (configType == Calib) fes = rm.getCalibModuleConfigs();

  if(!fes) { std::cout << "ERROR: fes is NULL. Cannot proceed" << std::endl; return; }

  int maxModules = rm.getNModuleConfigs();

  for (int i = 0; i < maxModules; i++) {
    if (channels & (1<<i)) {
      std::cout<<std::endl;
      std::cout<<"About to read SN for FE with Rx: "<<(int)i<<std::endl;
      fes[i].readSerialNumber();
    }
  }
  
#endif
}

