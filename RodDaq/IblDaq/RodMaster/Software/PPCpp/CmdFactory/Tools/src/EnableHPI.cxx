#include <iomanip>
#include "EnableHPI.h"
#include "RodMasterRegisters.h"

void EnableHPI::execute() {
#ifdef __XMK__


  uint32_t value;
  if (enableHPIbit)
    {
      
      std::cout<<"Enabling HPI bit"<<std::endl;
	  value = IblRod::EpcLocalCtrl::read();
	  value|=0x40000000;
	  IblRod::EpcLocalCtrl::write(value);
      result.PPCctrlRegValue = IblRod::EpcLocalCtrl::read();
	  std::cout<<"PPC_CTRL_REG = 0x"<< std::hex << value << std::endl;

    }
  else 
    {
      std::cout<<"Disabling HPI bit"<<std::endl;
	  value = IblRod::EpcLocalCtrl::read();
	  value&=0xBFFFFFFF;
	  IblRod::EpcLocalCtrl::write(value);
      result.PPCctrlRegValue = IblRod::EpcLocalCtrl::read();
	  std::cout<<"PPC_CTRL_REG = 0x"<< std::hex << value << std::endl;
    }
  
  
#endif
}

void EnableHPI::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out, offset, enableHPIbit);
}

void EnableHPI::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<bool>::deserialize(in, offset, enableHPIbit);
}

uint32_t EnableHPI::serializedSize() const
{
	return sizeof(uint8_t);
}

void GetEnableHPIResult::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<uint32_t>::serialize(out, offset, PPCctrlRegValue);	
}

void GetEnableHPIResult::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<uint32_t>::deserialize(in, offset, PPCctrlRegValue);	
}

uint32_t GetEnableHPIResult::serializedSize() const
{
  return sizeof(uint32_t);
}
