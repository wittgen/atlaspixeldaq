#include "GetDebugHists.h"

#ifdef __XMK__
#include "ScanBoss.h"
#endif

GetDebugHists::GetDebugHists(){ 
  setResultPtr(); // necessary step to tell command where the result class is
}

// inside execute() the actual meat of the command happens on the ppc
void GetDebugHists::execute() {
#ifdef __XMK__
  std::cout << "ScanBoss has " << ScanBoss::getDebugHists().size() << " debug histograms." << std::endl;
  result.hists = ScanBoss::getDebugHists();
  std::cout << "And there are " << result.hists.size() << " debug histograms sotred in result." << std::endl;
#endif
}

void DebugHists::serialize(uint8_t *out) const {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  uint32_t offset = 0;
  std::cout << "And there are " << hists.size() << " available for serialization." << std::endl;
  Serializer<std::vector<Fei4Hist> >::serialize(out, offset, hists);
}

void DebugHists::deserialize(const uint8_t *in) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  uint32_t offset = 0;
  Serializer<std::vector<Fei4Hist> >::deserialize(in, offset, hists);
}

uint32_t DebugHists::serializedSize() const {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  return Serializer<std::vector<Fei4Hist> >::size(hists);
}
