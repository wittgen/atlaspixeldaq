#include "BmfFeEmuConfig.h"

#include "RodResourceManager.h"

#include <iterator>


BmfFeEmuConfig::BmfFeEmuConfig() {
	// Zero is the default value. This value will be IGNORED when running execute!
	hitsPerTrigger = 0;
	channels = 0; // Zero means picking the internal value of the enabled channels
}

void BmfFeEmuConfig::execute() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#ifdef __XMK__

	if(!channels) {
		// There is a BocLink defined in RodResourceManager
		channels =  RodResourceManagerIBL::getIblBocLink()->getEnChMask();
	}

	std::cout << "Setting BOC FE emulator occupancy to " << static_cast<int>(hitsPerTrigger) << " for the channel mask 0x" << std::hex << channels << std::dec << std::endl;
	for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
		for(uint8_t ch = 0 ; ch < 16 ; ++ch) {
			if (channels & (1<<(slaveId*16+ch))) {
				// Setting hit count per trigger
				if(hitsPerTrigger) IblBoc::IBLFeEmu::setHitCount(slaveId, ch, hitsPerTrigger);
			}
		}
	}

	return;

#endif

#if 0
	// Keeping this lecacy code but commenting it out
	uint32_t nTrig = 3;
	hitsPerTrigger = 1;
	std::cout << __PRETTY_FUNCTION__ << " configuration: " << nTrig << " triggers with " << (int)hitsPerTrigger << " hits each." << std::endl;

	uint32_t nFE = RodResourceManagerIBL::getNModuleConfigs();

#if 1
	for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
		std::cout << "Configuring BMF FE emulator for Slave " << (int)slaveId << std::endl;
		for(uint8_t ch = 0 ; ch < 16 ; ++ch) {
			std::cout << "--- Preparing ch " << (int)ch << std::endl;
			// Enabling channel
			IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x7);
			IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x1);

			// Initializing FE emulator, keeping whatever status was set before
			IblBoc::FeEmu::init(slaveId,ch);
			
			// Setting hit count per trigger
			IblBoc::FeEmu::setHitCount(slaveId, ch, hitsPerTrigger);
		}
		std::cout << "Done!" << std::endl;
	}


	for(uint32_t i = 0 ; i < nFE ; ++i) {
		Fei4& fe = RodResourceManagerIBL::getCalibModuleConfig(i);
		uint8_t slaveId = i/16; uint8_t ch = i%16;
		uint8_t rxControl = IblBoc::read(slaveId, ch, IblBoc::RxControl);
		// Skipping non-enabled channels
		if((rxControl & 0x1) == 0x0) continue;
		std::cout << "Triggering FE #" << i << std::endl;

		fe.setChipID(6+(ch%2));
		fe.setRunMode(true);
		for(uint32_t t = 0 ; t < nTrig ; ++t) fe.trigger();
#ifdef __XMK__
		usleep(5000);
#endif
		while(1) {
			const std::vector<int>& frame = fe.readFrame();
			if(!frame.size()) break;
			std::cout << "Frame received: " << std::hex;
			std::copy(frame.begin(), frame.end(), std::ostream_iterator<int>(std::cout," "));
			std::cout << std::dec << std::endl;
		}
	}

#else

	std::cout << "Testing manual hit injection" << std::endl;
	//std::vector<uint8_t> sampleFifo;
	//uint8_t array[] = { 0xe9, 0x87, 0x09, 0xef, 0x38, 0x07, 0x5a, 0x59, 0x5f, 0x45, 0x23, 0x5f };
	//sampleFifo.assign(array, array+sizeof(array));

	
	for(uint8_t slaveId = 0 ; slaveId < 2 ; ++slaveId) {
		std::cout << "Configuring BMF FE emulator for Slave " << (int)slaveId << std::endl;
		for(uint8_t ch = 0 ; ch < 16 ; ++ch) {
			std::cout << "--- Preparing ch " << (int)ch << std::endl;

			// Enabling channel
			IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x7);
			IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x1);

			// Initializing FE emulator, keeping whatever status was set before
			IblBoc::FeEmu::init(slaveId,ch);
			
			// Moving to manual hit injection
			uint8_t feEmuControl = IblBoc::read(slaveId, ch, IblBoc::FeEmuControl);
			feEmuControl &= ~0x04;
			IblBoc::write(slaveId, ch, IblBoc::FeEmuControl, feEmuControl);
			feEmuControl = IblBoc::read(slaveId, ch, IblBoc::FeEmuControl);
			std::cout << "FeEmuControl: 0x" << std::hex << (int)feEmuControl << std::dec << std::endl;

			// Preparing manual hit injection
			uint16_t L1ID = 0x00AA; uint16_t BCID = 0xBB;
			IblBoc::FeEmu::injectStartOfFrame(slaveId, ch);
			IblBoc::FeEmu::injectFrameHeader(slaveId, ch, L1ID, BCID);
			IblBoc::FeEmu::injectHit(slaveId, ch, 1, 1, 0xB, 0xB);
			IblBoc::FeEmu::injectHit(slaveId, ch, 2, 2, 0xB, 0xB);
			IblBoc::FeEmu::injectEndOfFrame(slaveId, ch);
			IblBoc::FeEmu::armEmu(slaveId, ch);
		}
		std::cout << "Done!" << std::endl;
	}

	for(uint32_t i = 0 ; i < nFE ; ++i) {
		Fei4& fe = RodResourceManagerIBL::getCalibModuleConfig(i);
		uint8_t slaveId = i/16; uint8_t ch = i%16;
		uint8_t rxControl = IblBoc::read(slaveId, ch, IblBoc::RxControl);
		// Skipping non-enabled channels
		if((rxControl & 0x1) == 0x0) continue;
		std::cout << "Triggering FE #" << i << std::endl;

		fe.setChipID(6+(ch%2));
		fe.setRunMode(true);
		fe.trigger();
#ifdef __XMK__
		usleep(50000);
#endif
		while(1) {
			const std::vector<int>& frame = fe.readFrame();
			if(!frame.size()) break;
			std::cout << "Frame received: " << std::hex;
			std::copy(frame.begin(), frame.end(), std::ostream_iterator<int>(std::cout," "));
			std::cout << std::dec << std::endl;
		}
	}
#endif

#endif

}


void BmfFeEmuConfig::serialize(uint8_t *out) const {
	uint32_t offset = 0;
	Serializer<uint8_t>::serialize(out,offset,hitsPerTrigger);
	Serializer<uint32_t>::serialize(out,offset,channels);
}

void BmfFeEmuConfig::deserialize(const uint8_t *in) { 
	uint32_t offset = 0;
	hitsPerTrigger = Serializer<uint8_t>::deserialize(in,offset);
	channels = Serializer<uint32_t>::deserialize(in,offset);
}

uint32_t BmfFeEmuConfig::serializedSize() const { 
	return sizeof(uint8_t) + sizeof(uint32_t); 
}
