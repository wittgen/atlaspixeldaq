/*
 * * F. Meloni <federico.meloni@cern.ch>
 * Set Rx Threshold and Delay (and Gain)
 */
#include <stdlib.h>
#include <unistd.h>
#include <iomanip>
#include "SetRxDelayThreshold.h"
#include "IblBoc.h"
#include "RodResourceManager.h"
void SetRxDelayThreshold::execute() {
#ifdef __XMK__
	
	std::cout << "Config RX with these parameters" << std::endl;
	std::cout << "Delay Bin Number (1--63): " << delay << std::endl;
	std::cout << "Threshold: " << threshold << std::endl;
	std::cout << "Gain: " << gain << std::endl;
	std::cout << std::endl;
	
	//Determine fibre and plugin ID, given the Rx channel
        uint8_t fibre = IblBoc::getMasterFibreFromRxCh(rxCh);
        uint8_t pluginId = (fibre / 12);
	std::cout << "Fibre: " << (int)fibre << " PluginId: " << (int)pluginId  << std::endl;
	
	//Setting the RX gain (one setting is shared by all the channels)
	IblBoc::PixSetRxRGS(pluginId, gain);
		
	//Getting 
	Fei3Mod *fes = RodResourceManagerPIX::getInstance().getCalibModuleConfigs();
	fes[0].setRxCh(rxCh);
	fes[0].setTxCh(txCh);

	//Enabling Tx on BOC
	IblBoc::write(txCh/16, txCh%16, IblBoc::TxControl, 0x01);
	IblBoc::write(rxCh/16, rxCh%16, IblBoc::RxControl, 0x3);
	
	//Setting the RX threshold	
	IblBoc::PixSetRxThreshold(fibre, threshold); //Using broadcast as the ch-DAC mapping is currently bugged
	  
	//halfclock return to remove the busy from the IODELAY     
	fes[0].writeMCCRegister(CSR, 0x020);
	
	delay--; //conversion to DAC setting (0--62)	
	uint32_t fineDelay = (delay%9)*10;
	uint32_t coarseDelay = (delay/9)%8;
	
	std::cout << "Delay Coarse: " << (int)coarseDelay << " Fine: " << (int)fineDelay  << std::endl;
		
	//Setting the fibre delay
	IblBoc::SetRxSampleDelay(fibre, coarseDelay, fineDelay);
	usleep(5000);		
	
	uint8_t coarseDelay_test = 0;
	uint8_t fineDelay_test = 0;
	
	IblBoc::GetRxSampleDelay(fibre, coarseDelay_test, fineDelay_test);
	std::cout << "Register readback - Coarse: " << (int)coarseDelay_test << " Fine: " << (int)fineDelay_test  << std::endl;	  

	fes[0].mccGlobalResetMCC();
		
	std::cout << "All done!" << std::endl;
#endif
}

void SetRxDelayThreshold::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<int32_t>::serialize(out, offset, txCh);
	Serializer<int32_t>::serialize(out, offset, rxCh);
	Serializer<uint32_t>::serialize(out, offset, threshold);
	Serializer<uint32_t>::serialize(out, offset, delay);
	Serializer<uint32_t>::serialize(out, offset, gain);
}

void SetRxDelayThreshold::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	Serializer<int32_t>::deserialize(in, offset, txCh);
	Serializer<int32_t>::deserialize(in, offset, rxCh);
	Serializer<uint32_t>::deserialize(in, offset, threshold);
	Serializer<uint32_t>::deserialize(in, offset, delay);
	Serializer<uint32_t>::deserialize(in, offset, gain);
}

uint32_t SetRxDelayThreshold::serializedSize() const
{
	return 3 * sizeof(uint32_t) + 2 * sizeof(int32_t);
}
