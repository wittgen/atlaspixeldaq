/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 13-November-2015
 *
 * PPC Dummy Scan Driver
 *
 */

#define DEBUG_ND

#include "DummyScan.h"
#include "HistCfgSerializer.h"

void DummyScan::execute() {
#ifdef __XMK__

//  Note:  This just does one histoUnit at a time (for now it's unit 0 on either slave, which MUST have been setup and enabled BEFORE calling this function)).

//  Determine injection type from inj_Type variable
    if(inj_Type == 0)  injectionType = (TestHistoStatus)HISTO_TEST_SETUP_NOHISTO; //  INTERNAL injection test without touching histogrammer
    if(inj_Type == 1)  injectionType = (TestHistoStatus)HISTO_TEST_SETUP;         //  INTERNAL injection test using histogrammer (but NOT slave formatter)
    if(inj_Type == 2)  injectionType = (TestHistoStatus)HISTO_TEST_SETUP_EXT;     //  EXTERNAL injection test involving BOTH histogrammer AND slave formatter

//  Threshold Scan stuff:
  uint32_t bins = 21; //  Make sure this is the same as in slaveEmu.cfg if running FitServer in emulator mode.
  int binDelta = (int)(2.0 * (double)RNGTHR / ((double)(bins-1)));

  IblRodSlave s;
  uint32_t stepSize = (1 << histConfig_in.cfg[histoUnit].maskStep);

  int nChips = histConfig_in.cfg[histoUnit].nChips ;
  int usms; //  = unit scan mask shift
  if(nChips > 8) usms = (128 - nChips) / 16;
  else usms = 8 - nChips;
  uint32_t unitMask = (0x000000FF >> usms); //  For the first nChips chips of unit 0 of either slave;

// std::cout << "BbBbBbB " << inj_Type << " " << injectionType << " " << scanType << std::endl;
//  Looping
  if(scanType != THR_SCURVE_FIT) bins=1; //  This ensures that, for now, more than 1 "real" bin is only used in Threshold Scans
  for (uint32_t iMask = 0; iMask < stepSize; iMask++) { //  Note:  Mask steps are also treated as "bins" in some routines.
    for (uint32_t iPar = 0, x = EXPTHR - RNGTHR; iPar < bins; iPar++, x += binDelta) { //  > 1 loop for Threshold only (see above)
      if(nChips <= 8) {  //  Eventually, this needs be handled the same way as Fei4 in Fei3 firmware !!
        histConfig_in.cfg[histoUnit].mStepOdd = iMask + 1;
        histConfig_in.cfg[histoUnit].mStepEven = stepSize - iMask;
      }
      histConfig_in.cfg[histoUnit].binId = iPar;
      // std::cout << "AaAaAaA " << histConfig_in.cfg[histoUnit].mStepEven << " " << histConfig_in.cfg[histoUnit].mStepOdd << std::endl;
      bool success = s.startHisto(&histConfig_in, whichSlave); //  Send histConfig to slave
      std::cout<< (success ? "    started histogrammer for slave " : " !! failed to start histogrammer for slave ") << whichSlave << std::endl;
      s.fillDummyHisto(&histConfig_in, iMask, iPar, unitMask, bins, whichSlave, histoUnit, (enum DummyScanType)scanType, (enum TestHistoStatus)injectionType, multiHit);

      histConfig_out = s.getHistCfg(whichSlave); //  Send request for histConfig to slave and pick it up when sent back

#ifdef DEBUG_ND //  Display what was sent back by the slave on the PPC UART
      for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
        if(histConfig_out.cfg[slvUnit].enable) {
          std::cout << std::endl;
          std::cout << "Slave " << whichSlave << ", Unit " << slvUnit << " Scan just completed with the following histogrammer configuration" << std::endl;

          std::cout << "nChips =           " << histConfig_out.cfg[slvUnit].nChips << std::endl;
          std::cout << "chipSel =          " << histConfig_out.cfg[slvUnit].chipSel << std::endl;
          std::cout << "maskStep =         " << histConfig_out.cfg[slvUnit].maskStep << std::endl;
          std::cout << "type =             " << histConfig_out.cfg[slvUnit].type << std::endl;
          std::cout << "mStepOdd =         " << histConfig_out.cfg[slvUnit].mStepOdd << std::endl;
          std::cout << "mStepEven =        " << histConfig_out.cfg[slvUnit].mStepEven << std::endl;
          std::cout << "expectedOccValue = " << histConfig_out.cfg[slvUnit].expectedOccValue << std::endl;
          std::cout << "enable =           " << histConfig_out.cfg[slvUnit].enable << std::endl;
          std::cout << "addrRange =        " << histConfig_out.cfg[slvUnit].addrRange << std::endl;
          std::cout << "scanId =           " << histConfig_out.cfg[slvUnit].scanId << std::endl;
          std::cout << "binId =            " << histConfig_out.cfg[slvUnit].binId << std::endl;
        }
      }
      std::cout << std::endl;
#endif

// //  Fill values obtained from the slave into the result object to be sent back to host
//       for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
//         result.histConfig.cfg[slvUnit].nChips           = histConfig_out.cfg[slvUnit].nChips;
//         result.histConfig.cfg[slvUnit].enable           = histConfig_out.cfg[slvUnit].enable;
//         result.histConfig.cfg[slvUnit].type             = histConfig_out.cfg[slvUnit].type;
//         result.histConfig.cfg[slvUnit].maskStep         = histConfig_out.cfg[slvUnit].maskStep;
//         result.histConfig.cfg[slvUnit].mStepEven        = histConfig_out.cfg[slvUnit].mStepEven;
//         result.histConfig.cfg[slvUnit].mStepOdd         = histConfig_out.cfg[slvUnit].mStepOdd;
//         result.histConfig.cfg[slvUnit].chipSel          = histConfig_out.cfg[slvUnit].chipSel;
//         result.histConfig.cfg[slvUnit].expectedOccValue = histConfig_out.cfg[slvUnit].expectedOccValue;
//         result.histConfig.cfg[slvUnit].addrRange        = histConfig_out.cfg[slvUnit].addrRange;
//         result.histConfig.cfg[slvUnit].scanId           = histConfig_out.cfg[slvUnit].scanId;
//         result.histConfig.cfg[slvUnit].binId            = histConfig_out.cfg[slvUnit].binId;
//       }
    }
  }
#endif
}

void DummyScan::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<uint32_t>::serialize(out, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<uint32_t>::serialize(out, offset, histoUnit);
        Serializer<uint32_t>::serialize(out, offset, inj_Type);
        Serializer<uint32_t>::serialize(out, offset, multiHit);
        Serializer<uint32_t>::serialize(out, offset, scanType);
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig_in);
}
void DummyScan::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<uint32_t>::deserialize(in, offset, whichSlave); //  Note:  This is only way to get slave id to PPC instance of CheckHistCfg
        Serializer<uint32_t>::deserialize(in, offset, histoUnit);
        Serializer<uint32_t>::deserialize(in, offset, inj_Type);
        Serializer<uint32_t>::deserialize(in, offset, multiHit);
        Serializer<uint32_t>::deserialize(in, offset, scanType);
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig_in);
}
uint32_t DummyScan::serializedSize() const
{
        return 5*sizeof(uint32_t) + Serializer<IblSlvHistCfg>::size(histConfig_in);
}

void DummyHistCfgCheck::serialize(uint8_t *out) const
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::serialize(out, offset, histConfig); //  To forward histConfig to host

}
void DummyHistCfgCheck::deserialize(const uint8_t *in)
{
        uint32_t offset = 0;
        Serializer<IblSlvHistCfg>::deserialize(in, offset, histConfig); //  To forward histConfig to host
}
uint32_t DummyHistCfgCheck::serializedSize() const
{
        return Serializer<IblSlvHistCfg>::size(histConfig);
}
