#include "WriteRodMasterBusyMask.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

void WriteRodMasterBusyMask::dump() {

  std::cout<<"I'm about to write the busy mask on the ROD master"<<std::endl;
}

WriteRodMasterBusyMask::WriteRodMasterBusyMask(){ 
  setResultPtr(); // necessary step to tell command where the result class is
  dumpOnPPC = true; // default to print info to kermit logger as well as return to host
  maskValue = 0x00000000;
}

// inside execute() the actual meat of the command happens on the ppc
void WriteRodMasterBusyMask::execute() {

  // put stuff that should only happen on the PPC (not on the host nor on the emulator) 
  // inside of this ifdef

#ifdef __XMK__

  // if you wanted to write to a register here, you would use:
  //  IblRod::EpcBase::write(0x01);


  // to change the uart, use:
  // IblRodSlave s;
  // s.setUart(0); // for PPC
  // s.setUart(1); // for slave 0
  // or, use the program ./bin/setUart from HostCommandPattern

  // write the busy mask register

	
  IblRod::MasterBusyMask::write(maskValue);
  std::cout<<"\n\t set busy mask to "<<HEX(maskValue)<<std::endl<<std::endl;



  if(dumpOnPPC) dump();
#else
  std::cout<<"nothing to do, in emulator mode"<<std::endl;
#endif
}

// Seralize the data members of the command
// In this case, only one boolean
void WriteRodMasterBusyMask::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,dumpOnPPC);
	Serializer<uint32_t>::serialize(out,offset,maskValue);
}

// Deseralize the data members of the command
// In this case, only one boolean
void WriteRodMasterBusyMask::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	dumpOnPPC = Serializer<bool>::deserialize(in,offset);
	maskValue = Serializer<uint32_t>::deserialize(in,offset);
}

// Define the size of the serialized command
// In this case, the size of a boolean
uint32_t WriteRodMasterBusyMask::serializedSize() const
{
	return (sizeof(uint32_t)+sizeof(bool));
}

// Seralize the data members of the command result
// These are the values of the registers we have read
// The order must be the same as the order in RegisterResult::deserialize()
void WriteRodMasterBusyResult::serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  Serializer<uint32_t>::serialize(out, offset, masterBusyMaskValue);
	  Serializer<uint32_t>::serialize(out, offset, masterBusyCurrentStatusValue);
}

// Deeralize the data members of the command result
// These are the values of the registers we have read
// These must be deserialized in the same order as they were serialized
void WriteRodMasterBusyResult::deserialize(const uint8_t *in) {
  std::cout << __PRETTY_FUNCTION__ << std::endl;
	  uint32_t offset = 0;
	  masterBusyMaskValue = Serializer<uint32_t>::deserialize(in, offset);
	  masterBusyCurrentStatusValue = Serializer<uint32_t>::deserialize(in, offset);
}

// Define the size of the serialized command result
// In this case, the size of all the register values we are shipping back
// If this does not match the size of the objects you have serialized,
// you may run into trouble!!

uint32_t WriteRodMasterBusyResult::serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return (2*sizeof(uint32_t)+ sizeof(uint8_t));
}
