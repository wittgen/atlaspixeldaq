/*
 * L. Jeanty <laura.jeanty@cern.ch>
 */

#include "SetSlaveVerbose.h"

#ifdef __XMK__
#include "IblRodSlave.h"
#endif

SetSlaveVerbose::SetSlaveVerbose(){ 
  setResultPtr(); 
  verbose= true;  
  whichSlave = 1;
}

void SetSlaveVerbose::execute() {
#ifdef __XMK__
  IblRodSlave s;
  int i = 0;
  int j = 2;
  if (whichSlave ==1) i = 1;
  else if (whichSlave == 0) j = 1;

  for ( ; i<j; i++) {
    bool success = s.setVerbose(i,verbose);
    std::cout<<"for slave "<<(int)i<<":"<<std::endl;
    if (verbose) std::cout<< (success ? "    set slave verbose" : "failed to set slave verbose") <<std::endl;
    else std::cout<< (success ? "    set slave not verbose" : "failed to set slave not verbose") <<std::endl;
  }
#endif
}

void SetSlaveVerbose::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<bool>::serialize(out,offset,verbose);
	Serializer<uint8_t>::serialize(out,offset, whichSlave);
}
void SetSlaveVerbose::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	verbose = Serializer<bool>::deserialize(in,offset);
	whichSlave = Serializer<uint8_t>::deserialize(in,offset);
}
uint32_t SetSlaveVerbose::serializedSize() const
{
  return (sizeof(bool)+sizeof(uint8_t));
}
