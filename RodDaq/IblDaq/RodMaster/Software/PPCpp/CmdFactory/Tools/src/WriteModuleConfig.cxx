#include "WriteModuleConfig.h"
#ifdef __XMK__
#include "RodResourceManager.h"
#include "Serializer.h"
#endif

WriteModuleConfig::WriteModuleConfig() { 
  setResultPtr(); 
  m_ConfigType = Physics;
}

void WriteModuleConfig::execute() {

#ifdef __XMK__

  // get current set of configs from resource manager, and then change them
  RodResourceManagerIBL &rm = RodResourceManagerIBL::getInstance();
  Fei4 *fes = (m_ConfigType == Physics) ? rm.getPhysicsModuleConfigs() : rm.getCalibModuleConfigs();

  //copy the configs!
 for (uint8_t i = 0; i < moduleConfigs.size(); i++) {
   uint8_t rxCh = moduleConfigs[i].getRxCh();
   if ( 0 < rxCh < rm.getNModuleConfigs()) {
     (fes[rxCh]).copyConfig((moduleConfigs[i]));
   #ifdef LL_DEBUG
   std::cout<<"Now FE "<<(int)rxCh<<" has the following config: "<<std::endl;
   fes[rxCh].dump();
   #endif
   }
   else std::cerr<<"WARNING: you have tried to set a module with invalid rx: "<<(int)rxCh
		 <<" and therefore no config was saved on the ppc"<<std::endl;
 }

 // Now that we have shipped the configurations tot he ROD, we can update the connectivity
 RodResourceManagerIBL::updateConnectivity(fes);
#endif
}

void WriteModuleConfig::serialize(uint8_t *out) const
{
	uint32_t offset = 0;
	Serializer<ConfigType>::serialize(out, offset, m_ConfigType);
	uint8_t nFe =  moduleConfigs.size();
	Serializer<uint8_t>::serialize(out,offset,nFe); 
	for (int i = 0; i < nFe; i++) {
	  Serializer<Fei4ExtCfg>::serialize(out,offset,moduleConfigs[i]);
	}
}

void WriteModuleConfig::deserialize(const uint8_t *in)
{
	uint32_t offset = 0;
	m_ConfigType = Serializer<ConfigType>::deserialize(in, offset);
	uint8_t nFe = Serializer<uint8_t>::deserialize(in, offset);
	moduleConfigs.reserve(nFe);
	for (int i = 0; i < nFe; i++) {
	  Fei4ExtCfg module;
	    Serializer<Fei4ExtCfg>::deserialize(in,offset, module);
	    moduleConfigs.push_back(module);
	}
}

uint32_t WriteModuleConfig::serializedSize() const
{
  Fei4ExtCfg dummy;
  return (sizeof(uint8_t) + sizeof(ConfigType)+ moduleConfigs.size()*(dummy.serializedSize()));
}


