/*
 * Author: Pierfrancescp Butti <pierfrancesco.butti@cern.ch>
 *         Karolos Potamianos <karolos.potamianos@cern.ch>
 * Description: contains common commands by PF to handle UDP publishing in PPC
 */

#ifndef __UDP_SOCKETS_H__
#define __UDP_SOCKETS_H__

#ifdef __XMK__

#undef LWIP_POSIX_SOCKETS_IO_NAMES
#define LWIP_POSIX_SOCKETS_IO_NAMES 0
#include <sys/socket.h>
#include <arpa/inet.h>

#include <stdint.h>

#define STAT_UDP_BUFLEN 1024

class UDPSocket {
	public:
		void add8(uint8_t val);
		void add16(uint16_t val);
		void add32(uint32_t val);
		int sendBuffer();
		bool initializeClient(char* ipAddress, uint16_t port);
		void setBufferLen(uint32_t buflen) {len = buflen;}
		uint8_t* getBuffer() { return buffer; } // Note: size isn't propagated. Warning!
	private:
		int socketId;
		struct sockaddr_in server;
		/* UDP packet BUFFER */
		uint8_t buffer[STAT_UDP_BUFLEN]; // UDP packet BUFFER
		uint32_t len;
		uint32_t num;
		void clear() { len=0; }
};
#endif

#endif // __UDP_SOCKETS_H__
