#ifndef IPBUS_H_
#define IPBUS_H_

#define IPBUS_PORT		50000U

extern void ipbus_server_thread(void *);
extern void ipbus_process_thread(int sd);
extern void ipbus_generate_response(int sd, unsigned char *req, int req_len);
extern void ipbus_app_header();

#endif /* IPBUS_H_ */
