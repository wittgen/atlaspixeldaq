#ifndef HW_REGS_IN_CPP_H
#define HW_REGS_IN_CPP_H

/*
 * Author: K. Potamianos <karolos.potamianos@cenr.ch>
 * Date: 2013-VI-27
 * Description: This file provides a policy-safe description of HW registers in C++
 * 	which works both on HW (fixed registers) and SW (emulated registers).
 * 	SW mode is the default. HW mode can be enabled by defining __XMK__
 * Usage:
 *	* SW case: 
 *	struct IblRod { static uint32_t rEpcCtrl; typedef reg_t<uint32_t, EpcLocalCtrl, 0xFFFFFFFF, 0, rw_t> EpcLocalCtrl; }
 *	* HW case:
 *	struct IblRod { typedef reg_t<uint32_t, PPC_CTL_REG, 0xFFFFFFFF, 0, rw_t> EpcLocalCtrl; }
 * 	* Common usage:
 *	IblRod::EpcLocalCtrl::write(10);
 *	IblRod::EpcLocalCtrl::read();
 */

#include <stdexcept>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

# if __cplusplus >= 201103L || defined(__GXX_EXPERIMENTAL_CXX0X__)
#include <cstdio>
#include <memory>
//using std::shared_ptr;
#else
#include <tr1/memory>
using std::tr1::shared_ptr;
#endif

//#define VERBOSE_RW true
#ifndef VERBOSE_RW
#define VERBOSE_RW false
#endif

#ifdef __XMK__
template < typename RegType, RegType address, RegType mask, unsigned bit_offset, class policy, RegType (*mapping)(RegType) >
#else
template < typename RegType, RegType& address, RegType mask, unsigned bit_offset, class policy,  uint32_t (*mapping)(uint32_t)>
#endif
struct reg_t
{
	static void write(RegType value)
	{
#ifdef __XMK__
		policy::write( reinterpret_cast<volatile RegType *>(mapping(address)), mask, bit_offset, value );
#else
		policy::write( reinterpret_cast<volatile RegType *>(&address), mask, bit_offset, value );
#endif
	}

	static void write(unsigned word, RegType value)
	{
#ifdef __XMK__
        policy::write( reinterpret_cast<volatile RegType *>(mapping(address))+word, mask, bit_offset, value );
#else
        policy::write( reinterpret_cast<volatile RegType *>(&address)+word, mask, bit_offset, value );
#endif
	}

	static unsigned read()
	{
#ifdef __XMK__
      return policy::read( reinterpret_cast<volatile RegType *>(mapping(address)), mask, bit_offset );
#else
		return policy::read( reinterpret_cast<volatile RegType *>(&address), mask, bit_offset );
#endif
	}

	static unsigned read(unsigned word)
	{
#ifdef __XMK__
      return policy::read( reinterpret_cast<volatile RegType *>(mapping(address))+word, mask, bit_offset );
#else
		return policy::read( reinterpret_cast<volatile RegType *>(&address)+word, mask, bit_offset );
#endif
	}

	static void reset() {
		return reset(mask);
	}

	static void reset(unsigned m)
	{
#ifdef __XMK__
		return policy::resetBits( reinterpret_cast<volatile RegType *>(mapping(address)), m, bit_offset );
#else
		return policy::resetBits( reinterpret_cast<volatile RegType *>(&address), m, bit_offset );
#endif
	}

	static void set() {
		return set(mask);
	}

	static void set(unsigned m)
	{
#ifdef __XMK__
		return policy::setBits( reinterpret_cast<volatile RegType *>(mapping(address)), m, bit_offset );
#else
		return policy::setBits( reinterpret_cast<volatile RegType *>(&address), m, bit_offset );
#endif
	}

	static RegType addr()
	{
        return mapping(address);
	}

	static RegType* addrPtr()
	{
        return reinterpret_cast<RegType *>(mapping(address));
	}

	static RegType* addrPtr(RegType pos)
	{
        return &reinterpret_cast<RegType *>(mapping(address))[pos];

	}

	static unsigned at(unsigned pos)
	{
        return reinterpret_cast<RegType *>(mapping(address))[pos];
	}

};

#if 0
#ifdef __XMK__
template < RegType address, RegType mask, unsigned bit_offset, class policy >
#else
template < RegType& address, RegType mask, unsigned bit_offset, class policy >
#endif
static operator&  (RegType value) {
	RegType oldValue = reg_t::read();
	oldValue &= value;
	reg_t::write(oldValue);
}
#endif

struct addr_t
{
	// This is just meant to store an address
	// Should not accept read or write
};

template <typename RegType>
struct wo_t
{
	static void write( volatile RegType *reg, RegType mask, unsigned bit_offset, RegType value )
	{
	  if(VERBOSE_RW) printf("%s: addr=0x%p, value=0x%lx\n", __PRETTY_FUNCTION__, (void*)reg, (long unsigned int)value);
		*reg = (value & mask) << bit_offset;
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}

	static void resetBits( volatile RegType *reg, RegType mask, unsigned bit_offset ) {
		if(VERBOSE_RW) printf("%s: addr=0x%p, mask=0x%x\n", __PRETTY_FUNCTION__,  (void*)reg, mask);
		*reg &= ~(mask << bit_offset);
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}

	static void setBits( volatile RegType *reg, RegType mask, unsigned bit_offset ) {
		if(VERBOSE_RW) printf("%s: addr=0x%p, mask=0x%x\n", __PRETTY_FUNCTION__,  (void*)reg, mask);
		*reg &= ~(mask << bit_offset);
		*reg |= (mask << bit_offset);
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}
};

template <typename RegType>
struct ro_t {
	static RegType read( volatile RegType* reg, unsigned  mask, unsigned bit_offset )
	{
	if(VERBOSE_RW) printf("%s: addr=0x%p, value=0x%lx\n", __PRETTY_FUNCTION__,  (void*)reg, (long unsigned int) ((*reg >> bit_offset) & mask));
        return (*reg >> bit_offset) & mask;
	}

private:
	// Obviously, one cannot write to a read-only register
	// TODO: Provided clearer warning
	static RegType write( volatile RegType* reg, unsigned  mask, unsigned bit_offset, RegType value );
};


template <typename RegType>
struct rw_t : public ro_t<RegType>
{
	static void write( volatile RegType* reg, RegType  mask, unsigned bit_offset, RegType value )
	{
		if(VERBOSE_RW) printf("%s: addr=0x%p, value=0x%lx\n", __PRETTY_FUNCTION__,  (void*)reg, (long unsigned int)value);
		*reg = (*reg & ~(mask << bit_offset)) | ((value & mask) << bit_offset);
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}

	static void resetBits( volatile RegType *reg, RegType mask, unsigned bit_offset ) {
		if(VERBOSE_RW) printf("%s: addr=0x%p, mask=0x%lx\n", __PRETTY_FUNCTION__,  (void*)reg, (long unsigned int)mask);
		*reg &= ~(mask << bit_offset);
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}

	static void setBits( volatile RegType *reg, RegType mask, unsigned bit_offset ) {
		if(VERBOSE_RW) printf("%s: addr=0x%p, mask=0x%lx\n", __PRETTY_FUNCTION__,  (void*)reg,(long unsigned int) mask);
		*reg |= (mask << bit_offset);
		if(VERBOSE_RW) printf("%s: result=0x%lx\n", __PRETTY_FUNCTION__, (long unsigned int)*reg);
	}
};

#endif // __HW_REGS_IN_CPP_H__
