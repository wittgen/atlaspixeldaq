/*
 * Author: Pierfrancescp Butti <pierfrancesco.butti@cern.ch>
 *         Karolos Potamianos <karolos.potamianos@cern.ch>
 * Description: contains common commands by PF to handle UDP publishing in PPC
 */

#include "CppCompatibility.h" // Keep this header first!

#include "UDPSocket.h"

uint8_t buffer[STAT_UDP_BUFLEN]; // UDP packet BUFFER

#ifdef __XMK__


#undef LWIP_POSIX_SOCKETS_IO_NAMES
#define LWIP_POSIX_SOCKETS_IO_NAMES 0
#include <sys/socket.h>
#include <arpa/inet.h>

#include <cstring>

void UDPSocket::add8(uint8_t val) {
	buffer[len]=val;len++;
}

void UDPSocket::add16(uint16_t val) {
	uint16_t v=htons(val);
	memcpy((void *)&buffer[len],(void*)&v,2);len+=2;
}

void UDPSocket::add32(uint32_t val) {
	uint32_t v=htonl(val);
	memcpy((void*)&buffer[len],(void*)&v,4);len+=4;
}

int UDPSocket::sendBuffer() {
	int res = sendto(socketId,buffer,len,0,(const struct sockaddr *) &server, sizeof(struct sockaddr_in));
	return res;
}

bool UDPSocket::initializeClient(char* ipAddress,uint16_t port) {
	socketId = socket(AF_INET,SOCK_DGRAM,0);
	std::cout<<"Created Socket "<<socketId<<std::endl;

	if (socketId < 0)
		return false;

	server.sin_family = AF_INET;
	server.sin_port = htons(port);
	server.sin_addr.s_addr = inet_addr(ipAddress);

	
	if (connect(socketId, (struct sockaddr *) &server, sizeof(server)) < 0) {
	  std::cout<<"Could not connect to socket"<<std::endl;
	  return false;
	} else {
	  std::cout<<"Connected to the server..."<<ipAddress<<":"<<port<<std::endl;
	}
	return true;
}

#endif
