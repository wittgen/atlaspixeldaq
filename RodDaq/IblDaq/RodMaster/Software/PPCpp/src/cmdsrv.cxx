/*
 *      Author: K. Potamianos <karolos.potamianos@cern.ch>
 *      Date: 2013-VI-20s
 */

#include "RodResourceManager.h"

#include "Server.h"

#include "IblRodSlave.h"
#include "IblRodVersionInfo.h"

#include <cstdio>
#include <cstring>


static unsigned cmdsrv_port = CMD_SRV_IP_PORT;

void print_cmdsrv_app_header()
{
        xil_printf("Command Server listening on port %6d\n", cmdsrv_port);
}

void cmdsrv_application_thread(void *)
{
#if 1
  // This part of the code is very time-consuming if the slaves have no MB
  // Therefore, we disable them in the case where no slave is detected

	// Initializing network on both slaves
	// NO connection to the FitFarm

    if( RodResMgr::slaveIdle(0) || RodResMgr::slaveAlive(0) ) IblRodSlave::setDefaultNetConfig(0);
  else xil_printf("Skipping network initialization on slave A because it is not idle or even alive.\n");
	if( RodResMgr::slaveIdle(1) || RodResMgr::slaveAlive(1) ) IblRodSlave::setDefaultNetConfig(1);
  else xil_printf("Skipping network initialization on slave B because it is not idle or even alive.\n");
#else
	IblRodSlave::setDefaultNetConfig(0);
	IblRodSlave::setDefaultNetConfig(1);
#endif
	IblRodVersionInfo::printHashes();
	if (!IblRodVersionInfo::checkConsistency()) {
	    xil_printf("WARNING: Git hashes are inconsistent!\n");
	}

	Server server;
	server.run();
}
