
#define __MAIN__
#include "RodMasterRegisters.h" // Keep this header first!
#include "IblBocLink.h" // Should be before anything else, since it uses read and write
#include "RodResourceManager.h" // Should be at the top too

#include "IblBoc.h"


#include "xmk.h"
#include "os_config.h"
#include "sys/ksched.h"
#include "sys/init.h"
#include "config/config_param.h"
#include "stdio.h"
#include "xparameters.h"
#include "platform.h"
#include "platform_config.h"
#include <pthread.h>
#include <sys/types.h>

#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/init.h"
#include "netif/xadapter.h"

#include "platform_config.h"
#include "config_apps.h"
#include "dispatch.h"
#include "xvcd.h"

#include "xparameters.h"

#include "SerialPort.h"


void* master_thread(void *);
int main_thread(void *);
void network_thread(void *p);

/* Functions */
int main()
{
    enable_caches();

    init_platform();

    /* Initialize xilkernel */
    xilkernel_init();

    /* initialize lwIP before calling sys_thread_new */
    lwip_init();

#ifdef VERY_LOW_LEVEL_DEBUGGING
    // Infinite loop to allow the debugger to catch up
    static bool wait = true; while(wait) ;
#endif

    /* Create the main thread */
    // Have to cast by hand because C++ doesn't allow it.
    // Todo: fix this!!!
    xmk_add_static_thread((void* (*)(void*)) (main_thread), 1);

    /* Start the kernel */
    xilkernel_start();

    /* Never reached */
    cleanup_platform();

    return 0;
}


void
print_ip(char *msg, struct ip_addr *ip)
{
    print(msg);
    xil_printf("%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip),
            ip4_addr3(ip), ip4_addr4(ip));
}

void
print_ip_settings(struct ip_addr *ip, struct ip_addr *mask, struct ip_addr *gw)
{

    print_ip("Board IP: ", ip);
    print_ip("Netmask : ", mask);
    print_ip("Gateway : ", gw);
}

struct netif server_netif;

#ifndef ROD_SLOT
#define ROD_SLOT 8
#endif

/* the mac address of the board. this should be unique per board */
unsigned char mac_ethernet_address[6] = { 0x00, 0x0a, 0x35, 0x42, 0x51, ROD_SLOT };
struct ip_addr ipaddr, netmask, gw;


void network_thread(void *p)
{
    struct netif *netif;

    netif = &server_netif;

// Note: in SR1/PIT these are overwritten later ...
#ifdef SR1
    /* initliaze IP addresses to be used */
    IP4_ADDR(&ipaddr,  192, 168,   2, ROD_SLOT*10);
    IP4_ADDR(&netmask, 255, 255,   0,  0);
    IP4_ADDR(&gw,      192, 168,   0,100);
#elif defined PIT
if( RodResMgr::isFei3() ) {
    IP4_ADDR(&ipaddr,   10, 145,   97, 200+ROD_SLOT);
    IP4_ADDR(&netmask, 255, 255,  255,  0);
    IP4_ADDR(&gw,       10, 145,   97,  1);
} else {
    IP4_ADDR(&ipaddr,   10, 145,   89, 200+ROD_SLOT);
    IP4_ADDR(&netmask, 255, 255,  255,  0);
    IP4_ADDR(&gw,       10, 145,   89,  1);
}
#else
    IP4_ADDR(&ipaddr,  192, 168,   1, ROD_SLOT*10);
    IP4_ADDR(&netmask, 255, 255,   0,  0);
    IP4_ADDR(&gw,      192, 168,   0,  1);
#endif

    if( RodResMgr::isFei3() ) {
      mac_ethernet_address[3] = 0x79;
      mac_ethernet_address[4] = 0x11;
    }

    uint8_t vmeSlot = RodResourceManagerIBL::getVmeSlot();
    if(vmeSlot) {
	std::cout << "Obtaining MAC and IP addresses from VME slot (slot=" << (int)vmeSlot << ")" << std::endl;
	ipaddr.addr &= ~0xFF;
	ipaddr.addr |= 200 + vmeSlot;
	mac_ethernet_address[5] = vmeSlot;
    }

// In the PIT and SR1, use the same net ID as the BOC
#define KP_GET_IP_BASE_FROM_BOC
#ifdef KP_GET_IP_BASE_FROM_BOC
    uint8_t bocIpAddr[4];
    IblBoc::getIpAddress(bocIpAddr);
    ipaddr.addr &= ~0xFF00;
    ipaddr.addr |= bocIpAddr[2] << 8;
    gw.addr &= ~0xFF00;
    gw.addr |= bocIpAddr[2] << 8;
    if( bocIpAddr[2] == 97 ) // L1
      mac_ethernet_address[4] = 0x11;
    else if( bocIpAddr[2] == 98 ) // L2
      mac_ethernet_address[4] = 0x12;
    else if( bocIpAddr[2] == 99 ) // L3
      mac_ethernet_address[4] = 0x13;
    else if( bocIpAddr[2] == 100 ) // L4
      mac_ethernet_address[4] = 0x14;
    else if( bocIpAddr[2] == 104 ) { // B-Layer
      mac_ethernet_address[4] = (bocIpAddr[3] > 130) ? 0xb3 : 0xb2;
      ipaddr.addr &= ~0xFF;
      ipaddr.addr |= bocIpAddr[3] + 100;
    } else if( bocIpAddr[2] == 105 ) { // Disk
      mac_ethernet_address[4] = (bocIpAddr[3] > 130) ? 0xd2 : 0xd1;
      ipaddr.addr &= ~0xFF;
      ipaddr.addr |= bocIpAddr[3] + 100;
    } else {
      
     std::cout<<"WARNING!!!!!!"<<std::dec<<std::endl;
     std::cout<<"The ip address is not supported, setting field 4 of MAC to "<<std::hex<<mac_ethernet_address[4]<<std::dec<<std::endl;
    }

#endif // KP_GET_IP_BASE_FROM_BOC

    /* print out IP settings of the board */
    print("\n\r\n\r");
    print("-----lwIP Socket Mode Demo Application ------\n\r");
    print_ip_settings(&ipaddr, &netmask, &gw);
    print("MAC address: "); for(int i = 0 ; i < 6 ; ++i) xil_printf("%02x", mac_ethernet_address[i]);
    print("\n\r\n\r");


    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(netif, &ipaddr, &netmask, &gw, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
        xil_printf("Error adding N/W interface\n\r");
        return;
    }
    netif_set_default(netif);

    /* specify that the network if is up */
    netif_set_up(netif);

	xil_printf("Starting xemacif_input_thread. THREAD_STACKSIZE: %d %d\n", THREAD_STACKSIZE, DEFAULT_THREAD_PRIO);

    /* start packet receive thread - required for lwIP operation */
    sys_thread_new("network", (void(*)(void*))xemacif_input_thread, netif,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);

    /* start XVCD thread */
    sys_thread_new("xvcd", (void(*)(void*))xvcd_server_thread, 0, THREAD_STACKSIZE, 1);

    /* print all application headers */
    print_headers();

    /* now we can start application threads */
    launch_app_threads();

    return;
}


#include <sleep.h>

int
main_thread(void * p)
{
	// Todo: adapt to multiple serial ports
	RodResourceManagerIBL::getInstance().getSerialPort()->init();
	RodResourceManagerPIX::getInstance().getSerialPort()->init();

#ifdef SLEEP_TIMING_TESTS
	for(int i = 0 ; i < 10 ; ++i) {
		std::cout << "Sleeping for 0 microseconds..." << std::endl;
		usleep(0);
		std::cout << "Done!" << std::endl;
	}

	for(int i = 0 ; i < 10 ; ++i) {
		std::cout << "Sleeping for 1 millisecond..." << std::endl;
		usleep(1000);
		std::cout << "Done!" << std::endl;
	}

	for(int i = 0 ; i < 10 ; ++i) {
		std::cout << "Sleeping for 10 milliseconds..." << std::endl;
		usleep(10000);
		std::cout << "Done!" << std::endl;
	}

	for(int i = 0 ; i < 10 ; ++i) {
		std::cout << "Sleeping for 100 milliseconds..." << std::endl;
		usleep(100000);
		std::cout << "Done!" << std::endl;
	}
#endif

	/* any thread using lwIP should be created using sys_thread_new */
	sys_thread_new("NW_THREAD", network_thread, NULL,
		       THREAD_STACKSIZE,
		       DEFAULT_THREAD_PRIO);


	// creates a link from the BOC to the modules
	// comment out if no modules for now
	//		IblBocLink iblBocLink(&sPort);
	//		iblBocLink.enableChannels(0x01); // should be the correct mask for PixelLab testing
	// right now, enabling the channels from the main causes the ppc to freeze when getting a command
	
    return 0;
}
