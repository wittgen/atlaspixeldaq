// XVCD implementation taken from
// http://debugmo.de/2012/02/xvcd-the-xilinx-virtual-cable-daemon/

#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include <stdio.h>
#include <string.h>
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwipopts.h"
#include "xvcd.h"
#include "xgpio.h"
#include "xspi.h"
#include <vector>

static XGpio xvcd_gpio;
static int jtag_state;
static int verbose=1;
unsigned char u[4];
XSpi m_Spi;
XSpi_Config *m_ConfigPtr;
unsigned int m_StatusReg, m_ControlReg;
std::vector<unsigned int> m_spiData;


enum num_states
  {
    test_logic_reset=0, 
    run_test_idle=1,
    select_dr_scan=2, 
    capture_dr=3, 
    shift_dr=4,
    exit1_dr=5, pause_dr=6, exit2_dr=7, update_dr=8,
    select_ir_scan=9, capture_ir=10, shift_ir=11,
    exit1_ir=12, pause_ir=13, exit2_ir=14, update_ir=15,
    num_states=16
 };

static void gpio_set(int i, int val)
{
  int tmp = XGpio_DiscreteRead(&xvcd_gpio, 1);

  if(val)
    {
      tmp |= (1 << i);
    }
  else
    {
      tmp &= ~(1 << i);
    }

  XGpio_DiscreteWrite(&xvcd_gpio, 1, tmp);
}

static inline int gpio_get(int i)
{
  if(XGpio_DiscreteRead(&xvcd_gpio, 1) & (1 << i))
    return 1;
  else
    return 0;
}

static int jtag_step(int state, int tms)
{
  static const int next_state[16][2] =
    {

      {run_test_idle, test_logic_reset},
      
      {run_test_idle, select_dr_scan},

      {capture_dr, select_ir_scan},
      {shift_dr, exit1_dr},
      {shift_dr, exit1_dr},
      {pause_dr, update_dr},
      {pause_dr, exit2_dr},
      {shift_dr, update_dr},
      {run_test_idle, select_dr_scan},

      {capture_ir, test_logic_reset},
      {shift_ir, exit1_ir},
      {shift_ir, exit1_ir},
      {pause_ir, update_ir},
      {pause_ir, exit2_ir},
      {shift_ir, update_ir},
      {run_test_idle, select_dr_scan}

    };

  return next_state[state][tms];
}

static int fsread(int sd, void *target, int len)
{
   unsigned char *t = (unsigned char* )target;
   while (len)
    {
      int r = (read(sd, t, len));
      if (r <= 0)
	return r;
      t += r;
      len -= r;
    }
  return 1;
}

static int sread(int sd, void *target, int len)
{
  
   unsigned char *t = (unsigned char* )target;
   while (len)
    {
      int r = read(sd, t, len);
      if (r <= 0)
        return r;

      u[0]=t[0];
      u[1]=t[1];
      u[2]=t[2];
      u[3]=t[3];
      t[0]=u[3];
      t[1]=u[2];
      t[2]=u[1];
      t[3]=u[0];

      t += r;
      len -= r;
    }
  return 1;
}

static int sread2(int sd, void *target, int len)
{
  
   unsigned char *t = (unsigned char* )target;
   while (len)
    {
      int r = read(sd, t, len);
      if (r <= 0)
	return r;
      t += r;
      len -= r;
    }
  return 1;
}

int xvcd_server_thread(void)
{
	// xilinx_xvc host=xxx.xxx.x.xxx:2542 disableversioncheck=true

  int sock, new_sd;
  struct sockaddr_in address, remote;
  socklen_t size;

  verbose=0;

  /* initialize GPIO */
  XGpio_Initialize(&xvcd_gpio, XVCD_GPIO);
  XGpio_SetDataDirection(&xvcd_gpio, 1, (1 << XVCD_TDO));// make only TDO an input
  XGpio_DiscreteWrite(&xvcd_gpio, 1, 0x00);// set all to low

  /* create a TCP socket */
  if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    return -1;
  }

  /* set TCP_NODELAY */
  int on = 1;
  setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof( on )); 

  /* bind to port 80 at any interface */
  address.sin_family = AF_INET;
  address.sin_port = htons(XVCD_PORT);
  address.sin_addr.s_addr = INADDR_ANY;
  if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0) {
    return -1;
  }

  /* listen for incoming connections */
  lwip_listen(sock, 0);
  xil_printf("XVCD: Listening to incoming connections on TCP port %d.\r\n", XVCD_PORT);

  /* accept all incoming connections */
  size = sizeof(remote);
  while (1) {
    new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
    xil_printf("XVCD: Accepted connection.\r\n");

    /* switch over to virtual cable */
    xil_printf("XVCD: Giving JTAG control to the virtual cable.\n\r");
  

  //  Sending via SPI JTAG chain configuration
  m_ConfigPtr = XSpi_LookupConfig(XPAR_SPI_1_DEVICE_ID);
  if(NULL == m_ConfigPtr) {
  	xil_printf("ERROR: XST device not found\r\n");
	return 1;
  }
  XStatus m_Status;
  m_Status = XSpi_CfgInitialize(&m_Spi, m_ConfigPtr, m_ConfigPtr->BaseAddress);
  if(XST_SUCCESS != m_Status) {
	 xil_printf("ERROR: XST failure\r\n");
	return 1;
  }

  XSpi_Reset(&m_Spi);

  m_ControlReg = XSpi_GetControlReg(&m_Spi);

  xil_printf("SPI control register initial value: 0x%x\r\n",m_ControlReg);
  XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_MASTER_MODE_MASK | XSP_MANUAL_SSELECT_OPTION | XSP_CR_TRANS_INHIBIT_MASK);

  /*
   * We do not need interrupts for this loopback test.
   */
  XSpi_IntrGlobalDisable(&m_Spi);

  // reset ss
  XSpi_SetSlaveSelectReg(&m_Spi, ~0);

  // wait for tx ready
  do {
  	m_StatusReg = XSpi_GetStatusReg(&m_Spi);
  } while ((m_StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);

  /*
  * Start the transfer by not inhibiting the transmitter and
  * enabling the device.
  */

  m_ControlReg = XSpi_GetControlReg(&m_Spi) & (~XSP_CR_TRANS_INHIBIT_MASK);
  XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_ENABLE_MASK);

  m_spiData.clear();

  XSpi_SetSlaveSelectReg(&m_Spi, ~1);
  XSpi_WriteReg(m_Spi.BaseAddr, XSP_DTR_OFFSET, 0x1);
  do {
	m_StatusReg = XSpi_GetStatusReg(&m_Spi);
  } while ((m_StatusReg & XSP_SR_RX_FULL_MASK) == 0);
  XSpi_SetSlaveSelectReg(&m_Spi, ~0);
  xil_printf("Enabling XVC\r\n");
  sleep(1000);    


    /* spawn a separate handler for each request */
    sys_thread_new("xvcd", (void(*)(void*))xvcd_process_thread, (void*)new_sd,
		   THREAD_STACKSIZE,
		   1);
  }

  return 0;
}

int xvcd_process_thread(int sd)
{
  int i;
  int seen_tlr = 0;
  int ret = 0;
  gpio_set(XVCD_TDI, 1);

  do
    {
      char cmd[16];
      unsigned char buffer[2048], result[1024];

      //      xil_printf("Packet received from Remote ChipScope ");
      if (fsread(sd, cmd, 6) != 1)
      {
        ret = 1;
        break;
      }      

      if (memcmp(cmd, "shift:", 6))
      {
        cmd[6] = 0;
        xil_printf("XVCD: invalid cmd '%s'\r\n", cmd);
        ret = 1;
        break;
      }

      int len;
      if (sread(sd, &len, 4) != 1)
      {
        xil_printf("XVCD: reading length failed\r\n");
        ret = 1;
        break;
      }
          
      int nr_bytes = (len + 7) / 8;

      if (nr_bytes * 2 > (int)sizeof(buffer))
	{
	  xil_printf("XVCD: buffer size exceeded\r\n");
	  ret = 1;
	  break;
	}

      if (sread2(sd, buffer, nr_bytes * 2) != 1)
      {
        xil_printf("XVCD: reading data failed\r\n");
        ret = 1;
        break;
      }

      memset(result, 0, nr_bytes);

      if (verbose)
      {
        xil_printf("XVCD: #");
        for (i = 0; i < nr_bytes * 2; ++i)
          xil_printf("%02x ", buffer[i]);
        xil_printf("\n");
    }


      //
      // Only allow exiting if the state is rti and the IR
      // has the default value (IDCODE) by going through test_logic_reset.
      // As soon as going through capture_dr or capture_ir no exit is
      // allowed as this will change DR/IR.
      //

      seen_tlr = (seen_tlr || jtag_state == test_logic_reset) && (jtag_state != capture_dr) && (jtag_state != capture_ir);
      
      //
      // Due to a weird bug(??) xilinx impacts goes through another "capture_ir"/"capture_dr" cycle after
      // reading IR/DR which unfortunately sets IR to the read-out IR value.
      // Just ignore these transactions.
      //
      
      if ((jtag_state == exit1_ir && len == 5 && buffer[0] == 0x17) || (jtag_state == exit1_dr && len == 4 && buffer[0] == 0x0b))
      	{
        if (verbose)
            xil_printf("XVCD: ignoring bogus jtag state movement in jtag_state %d\r\n", jtag_state);
      	}
      else {


	    for (i = 0; i < len; ++i)
	    {
	    //
	    // Do the actual cycle.
	    //
	    
	    int tms = !!(buffer[i/8] & (1<<(i&7)));
	    int tdi = !!(buffer[nr_bytes + i/8] & (1<<(i&7)));
	    int tdo;   
             
	     tdo = gpio_get(XVCD_TDO);
      	     result[i / 8] |= tdo << (i&7);
	    
	    gpio_set(XVCD_TMS, tms);
	    gpio_set(XVCD_TDI, tdi);
	    gpio_set(XVCD_TCK, 1);
	    gpio_set(XVCD_TCK, 0);
	    //
	    // Track the state.
	    //
	    
	    jtag_state = jtag_step(jtag_state, tms);
	  }

      }


      if (verbose)
	{
	  xil_printf("XVCD: sending response %d bytes.\r\n", nr_bytes);
	  xil_printf("XVCD: #");
	  for (i = 0; i < nr_bytes; ++i)
	    xil_printf("%02x ", result[i]);
	  xil_printf("\n");
	} 
      if (write(sd, result, nr_bytes) != nr_bytes)
	{
	  xil_printf("XVCD: error write");
	  ret = 1;
	  break;
	}


    } while(1); //while (!(seen_tlr && jtag_state == run_test_idle));


  XSpi_Reset(&m_Spi);
  m_ControlReg = XSpi_GetControlReg(&m_Spi);
  xil_printf("SPI control register initial value: 0x%x\r\n",m_ControlReg);
  XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_MASTER_MODE_MASK | XSP_MANUAL_SSELECT_OPTION | XSP_CR_TRANS_INHIBIT_MASK);
  XSpi_IntrGlobalDisable(&m_Spi);
  XSpi_SetSlaveSelectReg(&m_Spi, ~0);
  do {
  	m_StatusReg = XSpi_GetStatusReg(&m_Spi);
  } while ((m_StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);
  m_ControlReg = XSpi_GetControlReg(&m_Spi) & (~XSP_CR_TRANS_INHIBIT_MASK);
  XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_ENABLE_MASK);
  m_spiData.clear();
  XSpi_SetSlaveSelectReg(&m_Spi, ~1);
  XSpi_WriteReg(m_Spi.BaseAddr, XSP_DTR_OFFSET, 0x00);
  do {
	m_StatusReg = XSpi_GetStatusReg(&m_Spi);
  } while ((m_StatusReg & XSP_SR_RX_FULL_MASK) == 0);

  XSpi_SetSlaveSelectReg(&m_Spi, ~0);
  xil_printf("Disabling XVC\r\n");  
  sleep(1000);

  xil_printf("XVCD: exiting...\r\n");
  close(sd);

  return ret;
}
