#include "SysStatus.h"

#include <cassert>
#include <cstdio>
#include <cstring>

void sysMon_application_thread(void *)
{
  SysStatus *sysStatus = SysStatus::getInstance();
  assert(sysStatus);
}
