/*
 * Copyright (c) 2009 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */


#include "cmdsrv.h"
#include "rxperf.h"
#include "txperf.h"
#include "echo.h"
#include "ipbus.h"
#include "cmdQS.h"
#include "sysMon.h"
#include "cmdQSN.h"
#include "cmdLP.h"
#include "cmdRecoECR.h"

#include "config_apps.h"
#include "dispatch.h"

#include <thread>
#include <vector>
#include "CppCompatibility.h"
#include <memory>
#include <IblBoc.h>
#include <RodResourceManager.h>


void 
print_headers()
{
    xil_printf("\n\r");
    xil_printf("%20s %6s %s\n\r", "Server", "Port", "Connect With..");
    xil_printf("%20s %6s %s\n\r", "--------------------", "------", "--------------------");

    if (INCLUDE_ECHO_SERVER)
        print_echo_app_header();

    if (INCLUDE_RXPERF_SERVER)
        print_rxperf_app_header();

    if (INCLUDE_TXPERF_CLIENT)
        print_txperf_app_header();

    if (INCLUDE_COMMAND_SERVER)
    	print_cmdsrv_app_header();

    if (INCLUDE_BOC_IPBUS)
        ipbus_app_header();
    
    if (INCLUDE_QUICKSTATUS)
     {
       print_cmdQS_app_header();
      }


    if (INCLUDE_LOGPUBLISHER)
          print_cmdLP_app_header();

    if (INCLUDE_RECOECR)
          print_cmdRecoECR_app_header();


#if 0
    if (INCLUDE_TFTP_SERVER)
        print_tftp_app_header();

    if (INCLUDE_WEB_SERVER)
        print_web_app_header();
#endif

    xil_printf("\n\r");
}


void
launch_app_threads()
{
    std::vector<std::thread> threads;

    // dummy parameter
    void * xnull = nullptr;

#if 0
    /* start webserver thread */
    if (INCLUDE_WEB_SERVER)
        sys_thread_new("httpd", web_application_thread, 0,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);
#endif


    /* start echo server thread */
    if (INCLUDE_ECHO_SERVER){
        threads.push_back( std::thread(echo_application_thread, xnull) );
    }

#if 0
    /* start tftp server thread */
    if (INCLUDE_TFTP_SERVER)
        sys_thread_new("tftpd", tftpserver_application_thread, 0,
            THREAD_STACKSIZE,
            DEFAULT_THREAD_PRIO);
#endif

    uint8_t bocIpAddr[4];
    IblBoc::getIpAddress(bocIpAddr);
    std::cout << "BOC IP " << (int)bocIpAddr[0] << " " << (int)bocIpAddr[1] << " " <<  (int)bocIpAddr[2] << " " << (int)bocIpAddr[3] << std::endl;


    /*
    if (INCLUDE_RXPERF_SERVER)
        threads.push_back(  std::thread(rx_application_thread, xnull) );

    if (INCLUDE_TXPERF_CLIENT)
        threads.push_back( std::thread(tx_application_thread, xnull) );
        */

    if (INCLUDE_COMMAND_SERVER)
        threads.push_back( std::thread(cmdsrv_application_thread, xnull ) );

    /*

    if(  INCLUDE_SYSSTATUS ) 
        threads.push_back( std::thread(sysMon_application_thread, xnull) );
    */

    if (INCLUDE_QUICKSTATUS)
        threads.push_back( std::thread(cmdQS_application_thread, xnull) );
	
    if (INCLUDE_BOC_IPBUS)
        threads.push_back( std::thread(ipbus_server_thread, xnull) );

    if(INCLUDE_LOGPUBLISHER)
        threads.push_back( std::thread(cmdLP_application_thread, xnull) );

    if (INCLUDE_RECOECR)
        threads.push_back( std::thread(cmdRecoECR_application_thread, xnull) );


    for(auto &t: threads) t.join();
    
}


