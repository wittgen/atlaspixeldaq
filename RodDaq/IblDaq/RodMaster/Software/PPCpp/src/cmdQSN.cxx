#include "QSNetwork.h"

#include <cassert>
#include <cstdio>
#include <cstring>


void print_cmdQSN_app_header()
{
        xil_printf("QuickStatus network thread up and running...\n");
}

void cmdQSN_application_thread(void *)
{
	// QS instance already starts running in the getInstance() function
	QSNetwork *qsn = QSNetwork::getInstance();
	assert(qsn);
}
