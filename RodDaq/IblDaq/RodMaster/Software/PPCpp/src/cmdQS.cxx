/*
 *      Author: K. Potamianos <karolos.potamianos@cern.ch>
 *      Date: 2013-VI-20s
 */

#include "QuickStatus.h"

#include "IblRodSlave.h"

#include <cassert>
#include <cstdio>
#include <cstring>
#include "CppCompatibility.h"


void print_cmdQS_app_header()
{
        xil_printf("QuickStatus daemon up and running...\n");
}

void cmdQS_application_thread(void *)
{
  // QS instance already starts running in the getInstance() function
  QuickStatus *quickStatus = QuickStatus::getInstance();
  assert(quickStatus);
}
