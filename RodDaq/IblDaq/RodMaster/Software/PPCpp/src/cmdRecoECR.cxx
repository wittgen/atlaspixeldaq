#include "RecoAtECRFei4.h"
#include "RecoAtECRFei3.h"

#include "RodResourceManager.h"

#include <cassert>
#include <iostream>


void print_cmdRecoECR_app_header() {
   std::cout << "Reconfig at ECR daemon up and running...\n";
}

void cmdRecoECR_application_thread(void*) {
  
    RecoAtECR* recoAtECR = RodResMgr::isFei4() ? RecoAtECRFei4::getInstance() : RecoAtECRFei3::getInstance();
    printf("RecoAtECR instance: 0x%08x \n", (unsigned int)recoAtECR);
    assert(recoAtECR);
}


