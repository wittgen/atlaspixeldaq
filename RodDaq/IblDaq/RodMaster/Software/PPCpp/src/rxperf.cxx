/*
 * Copyright (c) 2007 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <stdio.h>
#include <string.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <unistd.h>

#include "CppCompatibility.h"
#include <thread>


static unsigned rxperf_port = 5001;	/* iperf default port */

void print_rxperf_app_header()
{
        xil_printf("%20s %6d %s\n\r", "rxperf server",
                        rxperf_port,
                        "$ iperf -c <board ip> -i 5 -t 100");
}

void rx_application_thread(void *)
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	socklen_t size;

	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0)
		return;

	address.sin_family = AF_INET;
	address.sin_port = htons(rxperf_port);
	address.sin_addr.s_addr = INADDR_ANY;

	if (bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0)
		return;

	listen(sock, 5);

	size = sizeof(remote);
	new_sd = accept(sock, (struct sockaddr *)&remote, (socklen_t*)&size);

	while (1) {
                char recv_buf[1500];
		/* keep reading data */
		if (read(new_sd, recv_buf, 1500) <= 0)
			break;
	}

	xil_printf("Connection closed. RXPERF exiting.\n\r");

        close(new_sd);
}
