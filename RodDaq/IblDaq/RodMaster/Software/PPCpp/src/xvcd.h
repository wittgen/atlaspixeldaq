#ifndef XVCD_H__
#define XVCD_H__

#include "xparameters.h"

#define XVCD_PORT 2542
#define THREAD_STACKSIZE 2 * 1024 * 1024
#define XVCD_GPIO XPAR_XPS_GPIO_XVC_DEVICE_ID
#define XVCD_TCK 3
#define XVCD_TMS 2
#define XVCD_TDI 1
#define XVCD_TDO 0

extern int xvcd_server_thread(void);
extern int xvcd_process_thread(int sd);

#endif
