
#define __MAIN__
#include "RodMasterRegisters.h" // Keep this header first!
#include "IblBocLink.h" // Should be before anything else, since it uses read and write
#include "RodResourceManager.h" // Should be at the top too
//#include "signal_handler.h"
#include "IblBoc.h"
#include "config_apps.h"
#include "dispatch.h"
#include "SerialPort.h"
#include "NetUtils.h"
#include "stdio.h"

extern void version_print_info();

int main()
{
    printf("Starting ROD DAQ software\n");

	// Todo: adapt to multiple serial ports
    //OKUPDATE
    RodResourceManagerIBL::getInstance().getSerialPort()->init();
    RodResourceManagerPIX::getInstance().getSerialPort()->init();

    version_print_info();
    // OKUPDATE
    /* start XVCD thread */
//    sys_thread_new("xvcd", (void(*)(void*))xvcd_server_thread, 0, THREAD_STACKSIZE, 1);

    print_network_configuration();

    /* print all application headers */
    print_headers();

    // handle ctrl+c signals, etc.
  //  signal_handler::register_signals();

    /* now we can start application threads */
    launch_app_threads();


     // OKUPDATE - what is it? 
	// creates a link from the BOC to the modules
	// comment out if no modules for now
	//		IblBocLink iblBocLink(&sPort);
	//		iblBocLink.enableChannels(0x01); // should be the correct mask for PixelLab testing
	// right now, enabling the channels from the main causes the ppc to freeze when getting a command
	
    return 0;
}
