#include "cmdLP.h"
#include "LogPublisher.h"

#include <cassert>

#define _unused(x) ((void)x)	// prevent warnings about unused variable

void print_cmdLP_app_header()
{
        xil_printf("LogPublisher daemon up and running...\n");
}

void cmdLP_application_thread(void *)
{
  // LP instance already starts running in the getInstance() function
  LogPublisher* logPublisher = LogPublisher::getInstance();

  assert(logPublisher);
  _unused(logPublisher);
}
