/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-V-26
 */

#ifndef __ENUM_UTILS_H__
#define __ENUM_UTILS_H__

#include "scanEnums.h"

#include <iostream>

std::ostream& operator<< (std::ostream& os, const ScanState& s);

#endif // __ENUM_UTILS_H__
