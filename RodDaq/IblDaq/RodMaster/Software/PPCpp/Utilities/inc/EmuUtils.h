
#ifndef __EMU_UTILS_H__
#define __EMU_UTILS_H__

#include <vector>
#include <stdint.h>

namespace EmuUtils {

  enum class OCC_MODE {IBL_PP,IBL_HI,BLAYER_PP,BLAYER_HI, LANDAU, OCC_FIXED};

  double landau_pdf(const double x, const double xi, const double x0);
  void getLandauPdf(const double mean, const double sigma, std::vector<double> &pdf);
  void appendLandauPdf(const double mean, const double sigma, std::vector<double> &pdf);
  void getLUTFromPdf(const std::vector<double> &pdf, std::vector<uint8_t> &lut);
  void getLUTFromLandauPdf(const double mean, const double sigma , std::vector<uint8_t> &lut);
  void getLUTHitOccFixed (const uint8_t nHits , std::vector<uint8_t> &lut);
  void getLUTFromMode (EmuUtils::OCC_MODE occ, std::vector<uint8_t> &lut, const double nHits =0, const double sigma=0);

}

#endif // __EMU_UTILS_H__
