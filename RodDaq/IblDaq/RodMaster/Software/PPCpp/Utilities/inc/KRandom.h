

class KRandom {
public:
  KRandom();
  double getFromGaussian(double mu = 0., double sigma = 1.);
private:
  double m_z0, m_z1;
  bool m_generate;
};
