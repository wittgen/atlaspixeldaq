#ifndef VERSIONINFO
#define VERSIONINFO

#include <cstdio>
#include <string>

static constexpr const char * sGitHash = GIT_HASH;
static constexpr const char * sGitDate = GIT_DATE;
static constexpr const char * sGitWorkdirStatus = GIT_WORKDIR;
static constexpr const char * sBuildDate = BUILD_DATE;
static constexpr const char * sBuildHost = BUILD_HOST;
static constexpr const char * sBuildUser = BUILD_USER;
static constexpr const char * sCrossCompilerVersion = CROSSCOMPILER_VERSION; 

void version_print_info() {
    printf("-------------------------\n");
    printf("---- git information ----\n");
    printf("git hash: %s\n", sGitHash);
    printf("git date: %s\n", sGitDate);
    printf("git work dir status: %s\n", sGitWorkdirStatus);
    printf("--- build information ---\n");
    printf("build date: %s\n", sBuildDate);
    printf("build host: %s\n", sBuildHost);
    printf("build user: %s\n", sBuildUser);
    printf("----- cross compiler ----\n");
    printf("compiler version: %s\n", sCrossCompilerVersion);
    printf("-------------------------\n\n");
}

inline std::string version_git_hash_string() {
    return std::string(sGitHash);
}

inline std::string version_git_date_string() {
    return std::string(sGitDate);
}

inline std::string version_git_workdir_status_string() {
    return std::string(sGitWorkdirStatus);
}
#endif
