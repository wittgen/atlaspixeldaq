#ifndef __FEI3_HELPERS_H__
#define __FEI3_HELPERS_H__

#include <vector>
#ifndef __XMK__
#include <cstdint>
#include <cstdlib>
#endif

#include <bitset>

class Fei3Helpers {
public:
  static void showPixelData( const std::vector<uint8_t>& rxData, size_t nDC );
  static void decodePixelData( const std::vector<uint8_t>& rxData, size_t nDC, std::vector< std::bitset<320> >& dcBitSetVec );
  static void showTriggerData( const std::vector<uint8_t>& rxData );
};

#endif //__FEI3_HELPERS_H__
