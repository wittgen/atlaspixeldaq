/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-II-4
 * Description: utilities for binary manipulations that could be common to multiple functions
 */

#ifndef __BIT_UTILS_H__
#define __BIT_UTILS_H__

#include <vector>
#include <stdint.h>

void dumpData(const uint32_t*, uint32_t);
void dumpData(uint32_t);
void dumpData(const std::vector<uint32_t>& data);
void dumpData(const std::vector<uint8_t>& data);
uint32_t lowbias32(uint32_t x);
uint32_t rotl32b(uint32_t x, uint32_t n);
uint32_t float_bit_cast(const float &src);

#endif // __BIT_UTILS_H__
