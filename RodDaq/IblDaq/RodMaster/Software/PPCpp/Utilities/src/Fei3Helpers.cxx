#include "Fei3Helpers.h"

#include "Fei3PixelCfg.h"


void Fei3Helpers::showPixelData( const std::vector<uint8_t>& rxData, size_t nDC ) {
  static std::vector< std::bitset<320> > dcBitSetVec;

  size_t origBitSetVecSize = dcBitSetVec.size();
  Fei3Helpers::decodePixelData( rxData, nDC, dcBitSetVec );
  for( size_t i = origBitSetVecSize ; i < dcBitSetVec.size() ; ++i)
    std::cout << "DC=" << i << ": " << dcBitSetVec[i] << std::endl;

  if( dcBitSetVec.size() == nDC * Fei3::nPixLatches ) {
    std::map<size_t, std::vector<uint16_t> > tdacVal;
    std::map<size_t, std::vector<uint16_t> > fdacVal;
    std::cout << "=== TDAC/FDAC Summary ===" << std::endl;
    for(size_t iDC = 0 ; iDC < nDC ; ++iDC) {
      std::cout << "iDC == " << iDC << ": ";
      tdacVal[iDC].resize(320);
      fdacVal[iDC].resize(320);
      // Put TDACs together: bits 3, 4, 5, 6, 7, 8, 9
      for(size_t b = 0 ; b < 320 ; ++b) {
        tdacVal[iDC][b] = 0;
        for(size_t bP = 3 ; bP < 10 ; ++bP)
          tdacVal[iDC][b] |= dcBitSetVec[iDC+bP*nDC][b] << (bP-3);
        std::cout << tdacVal[iDC][b] << "/";
      }
      std::cout << std::endl;

      // Put FDACs together: bits 10, 11, 12
      for(size_t b = 0 ; b < 320 ; ++b) {
        fdacVal[iDC][b] = 0;
        for(size_t bP = 10 ; bP < 13 ; ++bP)
          fdacVal[iDC][b] |= dcBitSetVec[iDC+bP*nDC][b] << (bP-10);
        std::cout << fdacVal[iDC][b] << " ";
      }
      std::cout << std::endl;
    }
    dcBitSetVec.clear();
  }
}


void Fei3Helpers::decodePixelData( const std::vector<uint8_t>& rxData, size_t nDC, std::vector< std::bitset<320> >& dcBitSetVec ) {

#ifdef __XMK__
#ifdef __LL_DEBUG__
    dumpData(rxData);
#endif

    size_t k = 32;
    for(size_t dc = 0 ; dc < nDC ; ++dc) {
      std::bitset<320> dcContent;
      uint16_t dcBitPos = 0;
      for( ; k < rxData.capacity() && dcBitPos < 320 ; ++k) {
        if( (rxData[k] & 0xF) == 0xF && (rxData[k+1] & 0xF0) == 0xF0 )
          dcContent.set(dcBitPos, 1);
        dcBitPos++;
      }

      dcBitSetVec.push_back( dcContent );
    }
#endif

    return;
}

void Fei3Helpers::showTriggerData( const std::vector<uint8_t>& rxData ) {
    size_t cntr = 0;
    size_t mccHeaderPos = 0;
    size_t nTrig = 1;
    for(size_t k = 0 ; k < rxData.size() ; ++k) {
      bool isMccHeader = false;
      uint8_t skipBit = 0;
      if( rxData[k] == 0xe8 || rxData[k] == 0x74 || rxData[k] == 0x3a || rxData[k] == 0x1d ) {
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (rxData[k] & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      uint8_t firstWord = (rxData[k] << 4) + ((rxData[k+1] & 0xF0) >> 4);
      if( firstWord == 0xe8 || firstWord == 0x74 || firstWord == 0x3a || firstWord == 0x1d ) {
        skipBit += 4;
        for(size_t b = 0 ; b < 8 ; ++b) {
          if( (firstWord & (0x1 << (7-b))) == 0x00 ) skipBit++;
          else break;
        }
        if( !k || k > (mccHeaderPos + 3) ) {
          std::cout << std::endl << "[" << ((nTrig>9)?"":" ") << nTrig << "]: ";
          isMccHeader = true;
          nTrig++;
          mccHeaderPos = k;
        }
      }

      if( isMccHeader ) {
        cntr = 0;
      } else skipBit = 0;

      for(size_t b = skipBit ; b < 8 ; ++b) {
        std::cout << (( rxData[k] & (0x1 << (7-b) )) ? "1" : "0" );
        if( (++cntr)%4 == 0 ) std::cout << " ";
      }
    }
}
