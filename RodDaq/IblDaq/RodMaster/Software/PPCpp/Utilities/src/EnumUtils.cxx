/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-V-26
 * Description: this file contains code to display enum values properly as strings.
 * 	It can be improved, esp. using meta-programming, but for now we proceed using a simple method.
 */

#include "EnumUtils.h"

// Todo: make more generic!
#define ENUM_CASE_OSTREAM(os, name) case name: os << #name ; break

std::ostream& operator<< (std::ostream& os, const ScanState& s) {
	switch(s) {
		ENUM_CASE_OSTREAM( os, SCAN_IDLE );
		ENUM_CASE_OSTREAM( os, SCAN_INIT );
		ENUM_CASE_OSTREAM( os, SCAN_ENTER_TRIGGER_LOOP );
		ENUM_CASE_OSTREAM( os, SCAN_TRIGGER_LOOP );
		ENUM_CASE_OSTREAM( os, TRIGGER_LOOP_COMPLETE );
		ENUM_CASE_OSTREAM( os, SCAN_IN_PROGRESS );
		ENUM_CASE_OSTREAM( os, SCAN_END_OF_LOOP_ACTION );
		ENUM_CASE_OSTREAM( os, SCAN_COMPLETE );
		ENUM_CASE_OSTREAM( os, SCAN_TERMINATED );
		ENUM_CASE_OSTREAM( os, SCAN_NUM_STATES );
		default: os << "Unkown enum value. Integer value is " << (int)s << std::endl;
	}
	return os;
}
