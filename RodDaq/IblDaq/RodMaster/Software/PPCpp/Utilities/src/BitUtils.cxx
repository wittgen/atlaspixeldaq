/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-II-4
 * Description: utilities for binary manipulations that could be common to multiple functions
 */

#include "BitUtils.h"
#include <iostream>
#include <cstring>
#include <cassert>

void dumpData(const uint32_t* data, uint32_t nWords) {
	uint32_t i;
	int32_t n;
	char buffer[200];
	for (i=0;i<nWords;i++){
	  	snprintf(buffer,200,"Word[%u]: 0x%08x ",i,data[i]);
		std::cout << buffer;
		for(n = 31 ; n >= 0 ; --n) {
			snprintf(buffer, 200, "%s", (((data[i])>>n)&0x1)?"1":"0");
			std::cout << buffer;
			if(!(n%4)) std::cout << " ";
		}
		std::cout << std::endl;
        }
}

void dumpData(uint32_t data) {
	dumpData(&data,1);
}

void dumpData(const std::vector<uint32_t>& data) {
  dumpData( data.data(), data.size() );
}

void dumpData(const std::vector<uint8_t>& data) {

  std::vector<uint32_t> data32((data.size()+3)/4);
  for(size_t k = 0 ; k < data.size() ; ++k ) {
    uint8_t shift = 24 - ((k%4)*8);
    data32[k/4] |= (data[k] << shift);
  }
  dumpData(data32);

}

//This function "scrambles" a uint32_t, a change in a single bit will randomly
//change 1/2 (on average) of the other bits while remaining being fast if the
//CPU has built-in integer multiplication
uint32_t lowbias32(uint32_t x) {
    x ^= x >> 16;
    x *= UINT32_C(0x7feb352d);
    x ^= x >> 15;
    x *= UINT32_C(0x846ca68b);
    x ^= x >> 16;
    return x;
}

uint32_t rotl32b(uint32_t x, uint32_t n) {
    assert(n<32);
    if(!n) return x;
    return (x<<n) | (x>>(32-n));
}

uint32_t float_bit_cast(const float &src) {
    uint32_t dst;
    std::memcpy(&dst, &src, sizeof(uint32_t));
    return dst;
}

