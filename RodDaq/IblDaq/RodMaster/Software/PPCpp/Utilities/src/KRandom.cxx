/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2010-IV-20
 * Description: Lightweight random number generator (for now only Gaussian)
 */

#include "KRandom.h"

#include <limits>
#include <cstdlib>
#include <cmath>

KRandom::KRandom(): m_generate(true) {

}

double KRandom::getFromGaussian(double mu, double sigma) {
  const double epsilon = std::numeric_limits<double>::min();
  const double tau = 2.0*3.14159265358979323846;

  m_generate = !m_generate;

  if (!m_generate)
      return m_z1 * sigma + mu;

  double u1, u2;
  do
  {
      u1 = rand() * (1.0 / RAND_MAX);
      u2 = rand() * (1.0 / RAND_MAX);
  }
  while ( u1 <= epsilon );

  m_z0 = sqrt(-2.0 * log(u1)) * cos(tau * u2);
  m_z1 = sqrt(-2.0 * log(u1)) * sin(tau * u2);

  return m_z0 * sigma + mu;
}

