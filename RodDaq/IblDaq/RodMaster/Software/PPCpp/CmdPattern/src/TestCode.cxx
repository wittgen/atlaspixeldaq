#include "CppCompatibility.h"

#include "TestCode.h"

// Todo: think return type through
void TestCode::execute()
{	
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << "This is a function of type " << TypeName(*this) << " with execution ID of " << m_ExecId << std::endl;
	// Modify the result member as appropriate
	std::cout << "Result address = " << &result << std::endl;
	std::cout << "Pointer to result: " << m_ResultPtr << std::endl;

	result.value1 = 10;
	result.value2 = 200;
	std::cout << "value1 = " << result.value1 << std::endl;
	std::cout << "value2 = " << result.value2 << std::endl;
}

/// Inhertited functions for TestCode
void TestCode::serialize(uint8_t *out) const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

void TestCode::deserialize(const uint8_t *in)
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

uint32_t TestCode::serializedSize() const
{
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	return 0;
}


