#include "CppCompatibility.h" // Keep this header first!

#include "Server.h"

using namespace std;

void Server::run() {

    cout << "Starting server (waits for connections from a client)..." << endl;

    this->link.setIpAddr("127.0.0.1");
    this->link.setIpPort(CMD_SRV_IP_PORT);

    if (!this->link.initializeServer()) {
        return;
    }

    this->link.acceptConnections();

    // Should never be reached

    cout << endl << "Shutting down...";
    this->link.closeLink();
    cout << "Done." << endl;

}
