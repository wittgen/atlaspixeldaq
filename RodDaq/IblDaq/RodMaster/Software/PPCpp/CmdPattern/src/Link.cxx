#include "CppCompatibility.h" // Keep this header first!

#include "Link.h"
#include <cstring>

#include <iomanip>

#ifdef __XMK__
//#include "lwip/inet.h"
//#include "arch/sys_arch.h"
//#include "lwipopts.h"
#include "config_apps.h"

 #define LL_DEBUG 0


#else

#include <cmath>
#endif

void hexdump(uint8_t* buffer, uint32_t n) {
#if LL_DEBUG
  //	for(uint32_t i = 0 ; i < n ; ++i)
  //		std::cout <<i<<" : "<<std::hex << (int) buffer[i] << std::dec << std::endl; // tmp lj, too much output
#endif
}

CmdMonitor Link::cmdMonitor;

/*
 * This class abstracts a link over a TCP/IP connection. Only one client can
 * connect to the server!
 */

Link::Link()
{
	buffer = new uint8_t[CMD_MAX_SIZE];
	CommandManager::lsCommands();
}

// Todo: cleanup socket IDs
Link::Link(int connSockId): socketId(connSockId), newSocketAfterConn(connSockId)
{
	buffer = new uint8_t[CMD_MAX_SIZE];
}

Link::~Link() {
	delete[] buffer;
}

bool Link::initializeClient() {
    this->socketId = socket(AF_INET, SOCK_STREAM, 0);

    if (this->socketId == -1) {
        error("Could not create socket for client.");
        return false;
    }
    
    
#ifdef __XMK__
    
    if(!linkAddr || !linkPort)
      xil_printf("No IP or PORT specified. Cannot initialize client!\n");
    
#endif

    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(linkPort);
    server.sin_addr.s_addr = linkAddr;

    if (connect(this->socketId, (struct sockaddr *) &server, sizeof (server)) < 0) {
        error("Client could not connect to server.");
        return false;
    }

    return true;
}

bool Link::initializeServer() {
    this->socketId = socket(AF_INET, SOCK_STREAM, 0);

    if (this->socketId == -1) {
        error("Could not create socket for server.");
        error(strerror(this->socketId));
        return false;
    }

    // set socket reusable, otherwise port might not be free right after stopping daq code
    int option = 1;
    setsockopt(this-> socketId, SOL_SOCKET, SO_REUSEADDR, &option, sizeof(option));

    
#ifdef __XMK__
    if(!linkAddr || !linkPort)
    	xil_printf("No IP or PORT specified. Cannot initialize server!\n");
#endif
    struct sockaddr_in server;
    server.sin_family = AF_INET;
    server.sin_port = htons(linkPort);
    server.sin_addr.s_addr = INADDR_ANY;

    int err = bind(this->socketId, (struct sockaddr *) &server, sizeof (server));
    if (err < 0) {
        error("Could not bind address and port to server.");
        error(strerror(err));
        return false;
    }

    err = listen(this->socketId, 1);
    if (err < 0) {
        error("Server could not listen to connections.");
        error(strerror(err));
        return false;
    }

    return true;

}

void Link::acceptConnections() {


#if LL_DEBUG
    size_t cnt = 0;
#endif
    while(1) {
#if LL_DEBUG
    	std::cout << __PRETTY_FUNCTION__ << ": Loop Counter=" << ++cnt << std::endl;
#endif
    	this->newSocketAfterConn =
    			accept(this->socketId, NULL, NULL);
	
#if LL_DEBUG
#ifdef __XMK__
    	xil_printf("Accepted connection: socket ID %d\n", this->newSocketAfterConn);
#endif 
#endif
    	if (this->newSocketAfterConn < 0) {
    		error("Could not accept the connection from client.");
    		error(strerror(this->newSocketAfterConn));
    		return;
    	}

#if LL_DEBUG
    	std::cout << "[sockId=" << this->newSocketAfterConn << "] " <<  __PRETTY_FUNCTION__ << std::endl;
#endif
	
#ifdef __XMK__
	
	//for (size_t i = 0; i<SYS_THREAD_MAX; ++i)
	// {
	//  xil_printf("thread present %d\n",lwip_thread[i].tid);
	    
	//}

	
	
	      


      std::thread th(&Link::processConnection, this, (void*)this->newSocketAfterConn);
      th.detach();

#endif
    }

}


void Link::waitUntilDone() {
	// This commands waits until all commands have been returned
}

// Sends the command to the remote system
// Note: this function returns immediately if the command has no result member!
bool Link::sendCommand(const Command * cmd) {
#if LL_DEBUG
    std::cout <<"In function: " << __PRETTY_FUNCTION__ << std::endl;
#endif
    uint32_t dataSize = cmd->serializedSize();
    if(dataSize > CMD_MAX_SIZE) {
	    error("Command is too large for serialization buffer!");
	    std::cerr << "Size of command is: " << dataSize << " bytes and max size is: " << CMD_MAX_SIZE << " bytes" << std::endl;
	    return false;
    }

    //const Command* rPtr = cmd->getResult();
    //(void)rPtr;
    //if(rPtr && rPtr->serializedSize() > CMD_MAX_SIZE) {
    //	error("Result is too large for serialization buffer!");
    //	std::cerr << "Size of result is: " << rPtr->serializedSize() << " bytes and max size is: " << CMD_MAX_SIZE << " bytes" << std::endl;
    //	return false;
    //}

#if LL_DEBUG
    std::cout << TypeName(*cmd) << " has serialized length of " << dataSize << std::endl;
#endif

    uint32_t offset = 0;
    Serializer<uint32_t>::serialize(buffer,offset,offset); // Placeholder for the value of the message length
    Serializer<uint16_t>::serialize(buffer,offset,cmd->getTypeId());
    Serializer<uint16_t>::serialize(buffer,offset,cmd->getExecId());
    dataSize += offset;
    // Todo: check for a mismatch in sizes
    cmd->serialize(buffer+offset);
    uint32_t dummyOffset(0);
    Serializer<uint32_t>::serialize(buffer,dummyOffset,dataSize); // Updating the value we had before

#if LL_DEBUG
    std::cout << "Execution ID = " << cmd->getExecId() << std::endl;
#endif

    // Reusing the variable
    offset = 0;
    int n1;
    uint32_t sendSize = dataSize;
#ifdef __XMK__
    sendSize = 14000; // used to be TCP_SND_BUF/2
#endif

    do {
	n1 = send(this->socketId, buffer+offset, ((dataSize-offset)<sendSize) ? (dataSize-offset) : sendSize, 0);
	if(n1<0) break; // Error display done below
	offset += n1;
    } while(offset<dataSize);

    if( n1 < 0 ) {
	    error("Send command failed.");
	    return false;
    } else if(!CommandManager::isSly(cmd->getTypeId())) {
#if LL_DEBUG
      std::string action = "sent command";
#ifndef __XMK__ //  timeStamp() does not work on PPC
      action = timeStamp() + ":  " + action;
#endif // __XMK__
      log(action, " with ExcId = " + to_string((int)cmd->getExecId()));
#else //  To keep things running smoothly and fast in normal operation, We don't print timestamp or ExecId.
      log("sent command", cmd->toString());
#endif // LL_DEBUG
    }

    hexdump(buffer, dataSize);

#ifdef __XMK__
    // Supposedly the PPC doesn't need to wait for a response from the host, since it doesn't ask the host to do things
    // Todo: do this in a cleaner way!
    return true;
#endif
    // Reading response if applicable
    // Todo: use proper recursion for ping-pong
    if(const Command* rPtr = cmd->getResult()) {
	if(!rPtr){ std::cerr << "ERROR: cannot retrieve result. Null pointer returned" << std::endl; return false; }
#if LL_DEBUG
#ifndef __XMK__
        std::cout << timeStamp() << ":  Retrieving result for command with ExcId = " << cmd->getExecId() << std::endl;
#endif
#endif

	int n2;
	uint32_t tmpBufOffset = 0;
	uint32_t msgLength = 0xFFFFFFFF;

#ifndef __XMK__
  struct timeval tv;
  std::string cmdStr = cmd->toString();
  tv.tv_sec=15;
  tv.tv_usec=0;
  if(cmdStr == "StartScan" ||
     cmdStr == "StartScanL12" ||
     cmdStr == "OptoScan" ||
     cmdStr == "Fei3MonLeak" ||
     cmdStr == "Fei4MonLeak" ||
     cmdStr == "GetGADCOutput" ||
     cmdStr == "TuneGADCVref") {
    tv.tv_sec = 0;
  }
  setsockopt(this->socketId, SOL_SOCKET, SO_RCVTIMEO,&tv,sizeof(struct timeval));

  uint32_t cnt = 1;
  uint32_t nTries = 10;
  double waitTimeSec = (double)tv.tv_sec + round((double)tv.tv_usec/100000.0)/10.0;
	do {
#if LL_DEBUG
        std::cout << timeStamp() << ":  Now waiting " << waitTimeSec << " seconds for result from command with ExcId = "
                  << cmd->getExecId() << " before complaining" << std::endl;
#endif
#endif

	while((n2  = read(this->socketId, buffer+tmpBufOffset, CMD_MAX_SIZE-tmpBufOffset)) > 0 ) {
		// Initialization after first segment has arrived
		// Check against 0xFFFFFFFF avoids running multiple times to get the same result
		if(msgLength == 0xFFFFFFFF) msgLength = CommandManager::getMsgLength(buffer);
		tmpBufOffset += n2;
		if(tmpBufOffset == msgLength) break; // To do: find proper break condidion (what if something goes wrong?)
	}

#ifndef __XMK__
  if( n2 == -1 && errno == ECONNRESET ) {
    std::cout << "Connection reset by peer! errno==104" << std::endl;
    return false;
  } else if( n2 == -1 && errno == EAGAIN ) {
    std::cout << "[" << std::setw(2) << cnt << "] Timeout of " << waitTimeSec << " seconds reached from command with ExcId = " 
                  << cmd->getExecId() << "!" << std::endl;
    if(cnt++ >= nTries) {
      std::cout << "nTries=" << nTries << " attempts reached at " << timeStamp() << ". Aborting." << std::endl;
      return false;
    }
  }

  } while(n2 < 0);
#endif

#if LL_DEBUG
#ifndef __XMK__
	std::cout << timeStamp() << ":  Command with ExcId = " << cmd->getExecId() << ", read " << tmpBufOffset << " bytes!" << std::endl;
#endif
#endif

	if(n2<0) {
#ifdef __XMK__
	  xil_printf("%s: error reading from socket %d, closing socket (n2=%d, errno=%d)\n", __FUNCTION__, this->socketId, n2, errno);
#endif
	  close(this->socketId);
    		return false;
    	}
	
    	// We know it comes from the same command. Todo: Need to generalize for out-of-order execution!
	hexdump(buffer,tmpBufOffset);

	if( tmpBufOffset <= sizeof(CommandManager::CommandHeader) ) return false;

    	// I'm a lazy hacker! Don't do this at home :)
    	const_cast<Command*>(rPtr)->deserialize(buffer+sizeof(CommandManager::CommandHeader));
    	const_cast<Command*>(rPtr)->setExecId(CommandManager::getExecId(buffer));
#if LL_DEBUG
    	std::cout << "Got response from execution ID = " << rPtr->getExecId() << std::endl;
#endif
    }

    return true;
}

shared_ptr<Command> Link::getCommand() {
  

  shared_ptr<Command> cmd = CommandManager::getCommand(this->buffer+bufOffset);
    uint16_t type_id = CommandManager::getTypeId((const uint8_t*)this->buffer); // Explicit cast required
    std::stringstream ss;
    if(cmd) {
	    bufOffset += sizeof(CommandManager::CommandHeader) + cmd->serializedSize();
	    ss << cmd->toString() << " with execution ID " << cmd->getExecId();
    }
    else {
        std::cout << "Link.cxx: unknown ID " << type_id << std::endl;
        ss << "Link.cxx unknown ID " << type_id << " for command " << cmd->toString();
    }
    
    if(!CommandManager::isSly(type_id)) 
      log("received command", ss.str());

    return cmd; // Do not proceed with displaying content if command is stealth
    
}

void Link::processConnection(void *p) {
#ifdef __XMK__
	int n;
	int sd = (int)p;
	Link conn(sd);

#if LL_DEBUG
	std::cout << "[sID=" << sd << "] " <<  __PRETTY_FUNCTION__ << std::endl;
#endif

	// This bufOffset variable is modified internally in Link. We're just borrowing it.
	conn.bufOffset = 0;
	uint32_t msgLength = 0xFFFFFFFF;
	
	while((n  = read(sd, conn.buffer+conn.bufOffset, CMD_MAX_SIZE-conn.bufOffset)) > 0 ) {
		// Initialization after first segment has arrived
		// Check against 0xFFFFFFFF avoids running multiple times to get the same result
		if(msgLength == 0xFFFFFFFF) msgLength = CommandManager::getMsgLength(conn.buffer);
		conn.bufOffset += n;
        if(n==0) break;
		if(conn.bufOffset == msgLength) break; // To do: find proper break condidion (what if something goes wrong?)
	}

#if LL_DEBUG
	std::cout << "[sID=" << sd << "] Read " << conn.bufOffset << " bytes!" << std::endl;
	std::cout << "[sID=" << sd << "] Msg Len " << msgLength << " bytes!" << std::endl;
#endif

	if (n < 0) {
#ifdef __XMK__
		xil_printf("%s: error reading from socket %d, closing socket\n\r", __FUNCTION__, sd);
#endif
		close(sd);
		return;
	}
	// Connection closed
	// else if(!n) return;


	if(conn.bufOffset!=msgLength) {
#ifdef __XMK__
	  xil_printf("receive error: serial command stream length mismatch!\n");
#endif
		return;
	}

	// Now returning the bufOffset for use by Link::getCommand
	conn.bufOffset = 0;
	
	
	while(conn.bufOffset<msgLength) { // Checking whether command has been 'processed'. Todo: cleanup (scheme was modified)
		shared_ptr<Command> cmd = conn.getCommand();
		if(!cmd) {
			// Todo: probably make static!
			conn.error("unrecognized command");
			// Temporary. Todo: fix better
			conn.bufOffset = msgLength;
			break;
		}


		

#if LL_DEBUG
		std::cout << "Processing command " << cmd->toString() << " with execution ID " << cmd->getExecId() << "!" << std::endl;
#endif

        // register command to monitor
        cmdMonitor.insert( cmd->getTypeId(), cmd-> getExecId(), std::this_thread::get_id(), sd);

		cmd->execute();

		if( const Command* rPtr = cmd->getResult() ) {
			// Returning the proper execution ID for bookkeeping.
			// Dirty but easier :)
			// Todo: make result automatically inherit parent execution id
#if LL_DEBUG
			std::cout << "Pointer to result: " << rPtr << std::endl;
			std::cout << "Setting execution ID for the returned command" << std::endl;
#endif
			if(!rPtr){ std::cerr << "ERROR: cannot retrive result. Null pointer returned" << std::endl; return; }
			const_cast<Command*>(rPtr)->setExecId(cmd->getExecId());
#if LL_DEBUG
			std::cout << "Trying to send command return value..." << std::endl;
#endif
			// Senqding back the result type
			conn.sendCommand(rPtr);
		}

        // remove command when finished
        cmdMonitor.erase( cmd->getTypeId(), cmd-> getExecId());

#if LL_DEBUG
		std::cout << "Done processing command " << cmd->toString() << " with execution ID " << cmd->getExecId() << "!" << std::endl;
#endif
	}

#if LL_DEBUG
	std::cout << "Closing LwIP socket with ID " << sd << std::endl;
#endif

	close(sd);
#endif
#if LL_DEBUG
	std::cout << "end of process connections" << std::endl;
#endif
}

#ifndef __XMK__
std::string Link::timeStamp() { //  Does not work on PPC
  time_t raw_time;
  struct tm * timeinfo;
  char timebuffer[80];

  time (&raw_time);
  timeinfo = localtime(&raw_time);

  strftime(timebuffer,80,"%Y-%m-%d %H:%M:%S",timeinfo);
  return std::string(timebuffer);
}
#endif

template < typename T > std::string Link::to_string( const T& n )
{
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
}
