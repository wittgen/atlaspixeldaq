#include "CmdMonitor.h"
#include <iostream>
#include <iomanip>

CmdMonitor::CmdMonitor()
{

}

void CmdMonitor::insert(uint16_t type_id, uint16_t exec_id,
                        std::thread::id thread_id, int sd)
{

    std::lock_guard<std::mutex> lock(mutex);
    _map[type_id].insert(std::make_pair(exec_id,std::make_pair(thread_id, sd)));
}


void CmdMonitor::erase(uint16_t type_id, uint16_t exec_id)
{
    std::lock_guard<std::mutex> lock(mutex);
   _map[type_id].erase(exec_id);
}


void CmdMonitor::cloneMap(CmdMonitor::ThreadMap &newMap) {

    std::lock_guard<std::mutex> lock(mutex);
    newMap = _map;
}


void CmdMonitor::print()
{



  std::cout.fill(' ');
  std::cout << "--------------------------" << std::endl;
  std::cout << std::setw(8) << "TypeID" << std::setw(8) << "ExecID" << std::setw(8) << "Socket" << std::endl;

  ThreadMap m;
  cloneMap(m);

  for(ThreadMap::iterator outer = m.begin(); outer != m.end(); ++outer){
    const uint16_t & typeId = outer->first;

    ThreadMapInner &innerMap = outer->second;
    for(ThreadMapInner::iterator inner = innerMap.begin(); inner != innerMap.end(); ++inner){
      const uint16_t & execId = inner->first;
      int & sd = inner->second.second;
      // Print each, including this command.
      std::cout << std::setw(8) << typeId << std::setw(8) << execId << std::setw(8) << sd << std::endl;
    }
  }
  std::cout << "--------------------------" << std::endl;
}
