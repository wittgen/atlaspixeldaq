/*
Steve Alkire <alkire@cern.ch> 2014.02.18
*/
#include "EasySerializable.h"

void EasySerializable::serialize(uint8_t *out) const {
  m_ActionSwitch=doSerialize;
  m_out = out;
  m_offset = 0;
  serial();
}

void EasySerializable::deserialize(const uint8_t *in){
  m_in = in;
  m_offset = 0;
  serial();
}

uint32_t EasySerializable::serializedSize() const {
  m_ActionSwitch=doSerializedSize;
  m_SerializedSize=0;
  serial();
  return m_SerializedSize;
}
