#include "TestCommand.h"

void TestCommand::execute()
{
	std::cout << "Hello" << std::endl;
}

/// Inhertited functions for TestCommand
void TestCommand::serialize(uint8_t *out) const
{
	
}

void TestCommand::deserialize(const uint8_t *in)
{

}

uint32_t TestCommand::serializedSize() const
{
	return 0;
}
