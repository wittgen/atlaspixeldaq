#include "ServerInterface.h"

/*
 * The purpose of the class is to abstract the details, but for performance
 * the Link class could be used directly and the client could create the 
 * commands by itself...
 */

uint16_t ServerInterface::s_ExecId = 0;

ServerInterface::ServerInterface() {
    srand(time(NULL));
    ServerInterface::s_ExecId = rand() % 10000;
}

bool ServerInterface::connectToServer(const char* ip, uint16_t port) {
	std::cout << "Connecting to server at " << ip << ":" << port << std::endl;
    this->link.setIpAddr(ip);
    this->link.setIpPort(port);
    return this->link.initializeClient();
}

void ServerInterface::disconnectFromServer() {
    // Waiting until we received all replies
    // Todo: provide proper mechanisms for addressing failed commands
    this->link.waitUntilDone();
    this->link.closeLink();
}
