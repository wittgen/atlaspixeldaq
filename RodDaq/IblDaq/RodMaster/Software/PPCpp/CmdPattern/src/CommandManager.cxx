#include "CppCompatibility.h" // Keep this header first!

#include "CommandManager.h"
#include "Serializer.h"

unsigned CommandManager::s_CmdIdCounter = 1;
CommandManager::CommandMap CommandManager::m_CommandMap;
CommandManager::StealthMap CommandManager::m_StealthMap;
CommandManager::CommandBuilderMap CommandManager::m_CommandBuilderMap;

#include "CommandRegistrar.ixx"
// Instantiation needs to be here in order to avoid problems with the static initialization order
CommandRegistrar __commandRegistrar__;

CommandManager::CommandManager() {
}

// Tuples would make this staightforward and without manual intervention
CommandManager::CommandHeader CommandManager::getCommandHeader(const uint8_t *serialData) {
	CommandHeader cmdHdr;
	uint32_t offset = 0;
	cmdHdr.msgLength = Serializer<uint32_t>::deserialize(serialData,offset);
	cmdHdr.cmdTypeId = Serializer<uint16_t>::deserialize(serialData,offset);
	cmdHdr.cmdExecId = Serializer<uint16_t>::deserialize(serialData,offset);
	return cmdHdr;
}

uint32_t CommandManager::getMsgLength(const uint8_t *serialData) {
	uint32_t offset = 0;
	return Serializer<uint32_t>::deserialize(serialData,offset);
}

uint16_t CommandManager::getTypeId(const uint8_t *serialData) {
	uint32_t offset = sizeof(uint32_t); // First uint16_t is message length
	return Serializer<uint16_t>::deserialize(serialData,offset);
}

uint16_t CommandManager::getExecId(const uint8_t *serialData) {
	uint32_t offset = sizeof(uint32_t)+sizeof(uint16_t); // After message length and type id
	return Serializer<uint16_t>::deserialize(serialData,offset);
}

shared_ptr<Command> CommandManager::getCommand(const uint8_t *serialData)
{

	CommandManager::CommandHeader cmdHdr = CommandManager::getCommandHeader(serialData);
	uint16_t cmd_type_id = cmdHdr.cmdTypeId;
	// Todo: Address case where the command is unrecognized, or too much data for the command!
	// For now, it returns an empty shared_pointer
	// Creates new instance of the command
	// Todo: recover existing command from list of _sent_ commands

	// If the command doesn't exist, an empty shared_ptr<Command> will be created, defaulting to false
	shared_ptr<Command> cmd = CommandManager::m_CommandMap[cmd_type_id];
	//shared_ptr<Command> cmd = CommandManager::m_CommandBuilderMap[cmd_type_id]();

	if(!cmd) return cmd;
	// We have a known command
	cmd = cmd->getCopy();
	// Should ExecId really be part of the command ? Could be useful.
	cmd->setExecId( cmdHdr.cmdExecId );
	cmd->deserialize(serialData+sizeof(CommandHeader));
	return cmd;
}
