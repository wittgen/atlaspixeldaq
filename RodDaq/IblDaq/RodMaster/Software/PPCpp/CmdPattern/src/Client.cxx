#include <iostream>
#include "Client.h"

#include "DumpRodRegisters.h"

using namespace std;

void Client::run() {
  //todo maybe just get rid of this version and make abstract?
	cout << "Starting client...";

	if (!server.connectToServer(m_srvIpAddr, m_srvIpPort)) {
		return;
	}
	cout << "Connected to server." << endl;

//	DumpRodRegisters dumpRodRegs;
	//	server.sendCommand(dumpRodRegs);

	RodCommand cmd;
	for(int i = 0 ; i < 1 ; ++i) {
		cmd.setExecId(i+1);
		server.sendCommand(cmd);
		std::cout << "Result: " << cmd.result.value1 << std::endl;
		std::cout<<"giving you 2 results! woohoo."<<std::endl;
		std::cout << "Result: " << cmd.result.value2 << std::endl;
	}

	//testing Fei4Cfg
	
	/*
	Fei4Cfg fc;
	fc.SME.write(1);
	fc.dump();
	*/
	//end testing Fei4Cfg

	cout << "Disconnecting from server...";
	server.disconnectFromServer();
	cout << "Done." << endl;
}
