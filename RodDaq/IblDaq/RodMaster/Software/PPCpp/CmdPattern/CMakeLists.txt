project(CmdPattern)

add_library(${PROJECT_NAME} STATIC
    src/CleanTypes.cxx
    src/Client.cxx
    src/CmdMonitor.cxx
    src/CommandManager.cxx
    src/EasySerializable.cxx
    src/Link.cxx
    src/Server.cxx
    src/ServerInterface.cxx
    src/TestCode.cxx
    src/TestCommand.cxx

)

target_include_directories(${PROJECT_NAME}
    PUBLIC inc
)

target_link_libraries(${PROJECT_NAME}
    PRIVATE Tools
    PRIVATE TestCommands
    PRIVATE FrontEnd
    PRIVATE Scan
    PRIVATE UnitTests
    PRIVATE QuickStatus
    PRIVATE LogPublisher
    PRIVATE ReconfigAtECR
    )
