#ifndef __TEST_CODE_H__
#define __TEST_CODE_H__

#include "Serializer.h"
#include "Command.h"
#include "stdint.h"

#include <iostream>

struct TestCodeResult: public CommandCRTP<TestCodeResult > {
	uint32_t value1;
	uint16_t value2;

	/// Inhertited functions
	void serialize(uint8_t *out) const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		Serializer<uint32_t>::serialize(out, offset, value1);
		Serializer<uint16_t>::serialize(out, offset, value2);
	}
	void deserialize(const uint8_t *in) {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		uint32_t offset = 0;
		value1 = Serializer<uint32_t>::deserialize(in,offset);
		value2 = Serializer<uint16_t>::deserialize(in,offset);
	}
	uint32_t serializedSize() const {
		std::cout << __PRETTY_FUNCTION__ << std::endl;
		return sizeof(uint32_t)+sizeof(uint16_t);
	}

	TestCodeResult() {}
	virtual ~TestCodeResult() {}
};

class TestCode: public CommandCRTP<TestCode > {
public:
  InstantiateResult(TestCodeResult)

	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;


	TestCode() { setResultPtr(); }
	virtual ~TestCode() {}

	protected:

	private:
};

#endif // __TEST_CODE_H__

