/* 
 * File:   Link.h
 * 
 * This class represents the link between the client and the server. The
 * implementation assumes that there is only one server and only one client for
 * simplicity.
 */

#ifndef __LINK_H__
#define	__LINK_H__

#include "CppCompatibility.h" // Keep this header first!
//#include "RodCommand.h"
#include "Serializer.h"
#include "CommandManager.h"
#include "CmdMonitor.h"

#include <arpa/inet.h>

#include <string>
#include <map>
#include <utility>
#include <stdint.h>
#include <unistd.h>
#include <pthread.h>
#include <mutex>

#define CMD_MAX_SIZE 5 * 1024 * 1024 + sizeof(CommandManager::CommandHeader) // 3E6 x command header size should do it
#define CMD_SRV_IP_PORT 10763


class Link {
public:
	/**
	 * Sets the link IP address
	 *
	 */
	void setIpAddr(uint32_t ip) { linkAddr = ip; }
	void setIpAddr(const char* ip) { linkAddr = inet_addr(ip); }

	/**
	 * Sets the link port
	 */
	void setIpPort(uint16_t p) { linkPort = p; }

    /**
     * Initializes the link on the client side.
     *
     * @return <code>true</code> iff the initialization succeeded, 
     * <code>false</code> otherwise.
     */
    bool initializeClient();

    /**
     * Initializes the link on the server side.
     *
     * @return <code>true</code> iff the initialization succeeded, 
     * <code>false</code> otherwise.
     */
    bool initializeServer();

    /**
     * Main loop accepting connections and processing them via
     * Link::processConnection(void*)
     */

    void acceptConnections();

    /**
     * Closes the link. Each side has to call this method.
     */
    void closeLink() {
        std::cout << "Closing link" << this-> socketId << std::endl;
        if(close(this->socketId) < 0) {
            std::cout << "Closing socket failed\n";
        }

    }

    /**
     * Waits until all commands have received answers
     * This is mostly useful for test commands
     */

    void waitUntilDone();

    /**
     * Sends a command over the link.
     * @param cmd Command to send.
     * @return <code>true</code> iff the command was sent, 
     * <code>false</code> otherwise.
     */
    bool sendCommand(const Command* cmd);

    /**
     * Receives a command that was send over the link.
     * 
     * @return The original command sent over the link.
     */
    shared_ptr<Command> getCommand();

    /**
     * Processes the command in a separate thread.
     *
     */
    void processConnection(void *p);

    /**
     * Default constructor
     */
    Link();

    /**
     * Destructor
     */
    ~Link();
  
    //! Keep track of running commands
    static CmdMonitor cmdMonitor;

private:

    /**
     * Private constructor for establishing links
     */

    Link(int connSockId);

    /**
     * Writes an error message to standard output.
     * 
     * @param msg Message to write.
     */
    void error(std::string msg) {
        std::cerr << ">>> ERROR: " << msg << std::endl;
    };

    /**
     * Writes a log message to standard output.
     * 
     * @param msg Message to write.
     */
    void log(std::string action, std::string msg) {
    	std::cout << "> " << action << ": " << msg << std::endl;
    };
    std::string timeStamp();
    template < typename T > std::string to_string( const T& n );

    uint16_t linkPort; // TCP port
    uint32_t linkAddr; // IP address

    int socketId; // Main socket descriptor
    int newSocketAfterConn; // New socket descriptor for serving the client on the server
    uint8_t* buffer; // Buffer where the serialized command is stored.
    uint32_t bufOffset; // Offset withing buffer, for handling multiple commands in one frame

};

#endif //__LINK_H__

