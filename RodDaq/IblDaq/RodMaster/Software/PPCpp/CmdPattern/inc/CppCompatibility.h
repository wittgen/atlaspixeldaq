#ifndef __CPP_COMPATIBILITY_H__
#define __CPP_COMPATIBILITY_H__

// Xilinx messes up with things using macros!!!
// One needs to add some standard headers before anything from Xilinx
#include <sstream>
#include <iostream>

#define xil_printf  printf


#include <memory>
#include <type_traits>
using std::shared_ptr;
using std::weak_ptr;
using std::dynamic_pointer_cast;




template < typename _CastTo, typename _CastFrom >
  _CastTo union_cast( _CastFrom _value ){
  union Union{ _CastFrom _from; _CastTo _to; } unionCast;
  unionCast._from = _value;
  return unionCast._to;
}


#endif //__CPP_COMPATIBILITY_H__
