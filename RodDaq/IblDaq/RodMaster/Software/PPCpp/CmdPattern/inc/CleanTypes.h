#ifndef __CLEAN_TYPES_H__
#define __CLEAN_TYPES_H__

#include <string>
#include <typeinfo>

const std::string demangle(const char* name);

template <typename T>
std::string TypeName(T& x) {
	return demangle(typeid(x).name());
}

#endif //__CLEAN_TYPES_H__
