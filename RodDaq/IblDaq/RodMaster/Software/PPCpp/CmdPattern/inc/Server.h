/* 
 * File:   Server.h
 * 
 * Represents a server that executes commands sent from a client.
 */

#ifndef SERVER_H
#define	SERVER_H

#include "Link.h"

class Server {
public:
    /**
     * Runs the server.
     */
    void run();

private:
    /**
     * Since the server does not call any method of the client, a interface to
     * the clients methods is not needed (see Client.h for comparison). The
     * server interacts directly with the client through a link [GOF::Facade].
     */
    Link link;
};

#endif	/* SERVER_H */

