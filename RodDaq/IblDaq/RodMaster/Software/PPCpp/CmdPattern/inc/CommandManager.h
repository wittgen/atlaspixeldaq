#ifndef __COMMAND_MANAGER_H__
#define __COMMAND_MANAGER_H__

#include "CppCompatibility.h" // Keep this header first!

#include "Command.h"
#include "CommandTraits.h"
#include <map>

class CommandManager {
public:

	struct CommandHeader {
		uint32_t msgLength;
		uint16_t cmdTypeId;
		uint16_t cmdExecId;
	};

	typedef std::map< uint16_t, shared_ptr<Command> > CommandMap;
	typedef std::map< uint16_t, bool > StealthMap;
	typedef std::map< uint16_t, shared_ptr<Command> (*)() > CommandBuilderMap;

	CommandManager();

	// These two template structurs are needed because C++ doesn't allow default template arguments prior to C++11
	template <class T, typename = void>
	struct ResultRegistrar {
		static void doIt(bool isSly = false) { }
	};

	template <typename T>
	struct ResultRegistrar<T, typename std::enable_if<has_result_type<T>::value>::type> {
		static void doIt(bool isSly = false) { CommandManager::registerResult<typename T::ResultType>(isSly); }
	};


	static shared_ptr<Command> getCommand(const uint8_t *serialData);
	static CommandHeader getCommandHeader(const uint8_t *serialData);
	static uint32_t getMsgLength(const uint8_t *serialData);
	static uint16_t getTypeId(const uint8_t *serialData);
	static uint16_t getExecId(const uint8_t *serialData);

	static bool isSly( uint16_t typeId ) {
		return m_StealthMap[typeId];
	}

	/// Template members

	// Todo: check how to group in one type of command (with pointer to result)
	template <class T>
	static void registerResult(bool isSly = false) {
		uint16_t type_id = TypeId<T>::getTypeId();
		if(type_id) return;
		TypeId<T>::setTypeId( s_CmdIdCounter++ );
		type_id = TypeId<T>::getTypeId();
		shared_ptr<Command> sPtr = shared_ptr<Command>( new T() );
		m_CommandMap[type_id] = sPtr;
		m_StealthMap[type_id] = isSly;
		m_CommandBuilderMap[type_id] = &CommandManager::buildCommand<T>;
#ifdef LL_DEBUG
		std::cout << "Registering response " << sPtr->toString() << " with ID " << type_id << std::endl;
#endif
	}

	template <class T>
	static uint16_t getTypeId(const T&) { return TypeId<T>::getTypeId(); }

	template <class T>
	static shared_ptr<T> getCopy(const T& master) {
		// TypeId will be the same from the copy constructor
	  return std::make_shared<T>(master);
	}

	template <class T>
	static shared_ptr<Command> getInstance() {
	  return std::shared_ptr<Command>( new T() );
	}

	template <class T>
	static shared_ptr<Command> buildCommand() {
		return shared_ptr<Command>( new T() );
	}

	template <class T>
	static void registerCommand(bool isSly = false) {
		uint16_t type_id = TypeId<T>::getTypeId();
		if(type_id) return;
		else {
			TypeId<T>::setTypeId( s_CmdIdCounter++ );
			type_id = TypeId<T>::getTypeId();
			shared_ptr<Command> sPtr = shared_ptr<Command>( new T() );
#ifdef LL_DEBUG
			std::cout << "Registering command " << sPtr->toString() << " with ID " << type_id << std::endl;
#endif
			m_CommandMap[type_id] = sPtr;
			m_StealthMap[type_id] = isSly;
			m_CommandBuilderMap[type_id] = &CommandManager::buildCommand<T>;

			// Register replies when defined!
			ResultRegistrar<T>::doIt(isSly);

		}
	}

	static void lsCommands() {
#ifdef LL_DEBUG
		std::map< uint16_t, shared_ptr<Command> >::iterator it_cmdMap = m_CommandMap.begin();
		std::cout << "Dumping list of known commands:" << std::endl;
		for( ; it_cmdMap != m_CommandMap.end() ; ++it_cmdMap )
		  std::cout << "+ Command with ID=" << it_cmdMap->first << " is named " << it_cmdMap->second->toString() << std::endl;
#endif
	}

        static const CommandMap& getCommandMap() {return m_CommandMap;}
private:
	static unsigned s_CmdIdCounter;
	static CommandMap m_CommandMap;
	static StealthMap m_StealthMap;
	static CommandBuilderMap m_CommandBuilderMap;
};

#endif //__COMMAND_MANAGER_H__
