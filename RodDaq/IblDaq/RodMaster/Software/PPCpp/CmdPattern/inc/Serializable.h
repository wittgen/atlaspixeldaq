#ifndef __SERIALIZABLE_H__
#define __SERIALIZABLE_H__

#include <stdint.h>

/*
 * This
 */

#define __ALLOW_NON_PURE_VIRTUAL__

class Serializable {
public:
	// Using default implementations to facilitate tests
#ifdef __ALLOW_NON_PURE_VIRTUAL__
	virtual void serialize(uint8_t *out) const {};
	virtual void deserialize(const uint8_t *in) {};
	virtual uint32_t serializedSize() const { return 0; }
#else
	virtual void serialize(uint8_t *out) const = 0;
	virtual void deserialize(const uint8_t *in) = 0;
	virtual uint32_t serializedSize() const = 0;
#endif

	Serializable() {};
	virtual ~Serializable() {};
};

#endif // __SERIALIZABLE_H__
