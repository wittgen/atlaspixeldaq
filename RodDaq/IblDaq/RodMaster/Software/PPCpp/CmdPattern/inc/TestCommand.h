#ifndef __TEST_COMMAND_H__
#define __TEST_COMMAND_H__

#include "Command.h"

class TestCommand: public Command {
public:
	virtual void execute();

	/// Inhertited functions
	virtual void serialize(uint8_t *out) const;
	virtual void deserialize(const uint8_t *in);
	virtual uint32_t serializedSize() const;
};

#endif //__TEST_COMMAND_H__
