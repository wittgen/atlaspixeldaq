/* 
 * File:   Client.h
 *
 * Represents a client that sends commands to a server.
 */

#ifndef CLIENT_H
#define	CLIENT_H

#include "ServerInterface.h"


#define MAX_CMD 10

class Client {
public:
    /**
     * Runs the client. MAX_CMD random commands are sent to the server. 
     */

    void run();

    //    accessor functions
    const char* srvIpAddr() {return m_srvIpAddr;};
    uint16_t srvIpPort() {return m_srvIpPort;};

    /**
     * Constructor
     */
    Client(const char* srvIpAddr = "127.0.0.1", uint16_t port = CMD_SRV_IP_PORT): m_srvIpAddr(srvIpAddr), m_srvIpPort(port) {
    }

protected:
    ServerInterface server;

private:
    /**
     * The client uses this interface to send commands to the server. That is,
     * all low level details of the commands and the communication are hidden
     * from the client. [GOF::Facade]
     */
    const char* m_srvIpAddr;
    uint16_t m_srvIpPort;
};

#endif	/* CLIENT_H */

