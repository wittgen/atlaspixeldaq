#ifndef CMDMONITOR_H
#define CMDMONITOR_H

#include <stdint.h>
#include <thread>
#include <map>
#include <mutex>


class CmdMonitor
{
public:

    typedef std::map<uint16_t, std::pair<std::thread::id, int> > ThreadMapInner;
    typedef std::map<uint16_t, ThreadMapInner> ThreadMap;

    CmdMonitor();
    //! thread safe insert
    void insert(uint16_t type_id, uint16_t exec_id, std::thread::id thread_id, int sd);

    //! thread safe erase
    void erase(uint16_t type_id, uint16_t exec_id);

    //! thread safe copy
    void cloneMap(ThreadMap & newMap);

    //! thread safe print of ThreadMap
    void print();


private:

    ThreadMap _map;
    std::mutex mutex;
};

#endif // CMDMONITOR_H
