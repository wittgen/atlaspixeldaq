//
// Author: K. Potamianos <karolos.potamianos@cern.ch>
// Description: Simple serialization class to deal with inetegers
// Updates:
// 	K.Potamianos <karolos.potamianos@cern.ch> 23-XI-2103
//		Adding serialization for enum types using partial specialization
//      B.Axen <bradley.axen@cern.ch> 2014-06-10
//              Adding serialization for some std library containers
//              Added a size function to each serializer for convenience

#ifndef __SERIALIZER_H__
#define __SERIALIZER_H__

#include "CppCompatibility.h" // Keep this header first!
#include "Serializable.h"
#include <stdio.h>
#include <stdint.h>
#include <map>
#include <vector>
#include <array>
#include <string>
#include <utility>
#include <algorithm>
#include <numeric>


using std::enable_if;
using std::is_enum;
using std::is_base_of;
#define assert_typesize(x) static_assert(x, "Type too big for serialize implementation.")


template <typename T, typename = void>
class Serializer {
public:
	static void serialize(uint8_t* data, uint32_t& offset, const T& value) {
      assert_typesize(sizeof(T)<=4); // Implemented up to 32 bit type
		data[offset++] = value & 0xFF;
		if(sizeof(T)>1) data[offset++] = (value >>  8) & 0xFF;
		if(sizeof(T)>2) data[offset++] = (value >> 16) & 0xFF;
		if(sizeof(T)>3) data[offset++] = (value >> 24) & 0xFF;
	}

	static T deserialize(const uint8_t* data, uint32_t& offset) {
		// That's OK for small types, but larger types need another construct
		T v; deserialize(data, offset, v); return v;
	}
	
	static void deserialize(const uint8_t* data, uint32_t& offset, T& value) {
      assert_typesize(sizeof(T)<=4); // Implemented up to 32 bit type
		// Larger types need this method, to avoid copying too much data around
		value = data[offset++];
		if(sizeof(T)>1) value |= (data[offset++] <<  8);
		if(sizeof(T)>2) value |= (data[offset++] << 16);
		if(sizeof(T)>3) value |= (data[offset++] << 24);
	}
        static std::size_t size(const T& value = 0) {
	  return sizeof(value);
	}
private:
	Serializer();
};

// Functor for Serializer::serialize to bind data and offset. 
template<typename T>
class SerializeFunctor {
 public:
  SerializeFunctor(uint8_t* data, uint32_t& offset): m_data(data), m_offset(offset) {}
  void operator() (const T& value) {
    Serializer<T>::serialize(m_data, m_offset, value);
  }
 private:
  uint8_t* m_data;
  uint32_t& m_offset;
};

// Accumulation function for size
template<typename T>
std::size_t sum_size(std::size_t lhs, const T& rhs)
{
  return lhs + Serializer<T>::size(rhs);
}

/* Okay Karolos, this is temporary...stop crying! */
//partial specialization for pointers
template<typename T>
class Serializer<T*>{
 public:
  static void serialize(uint8_t* data, uint32_t& offset,  T* const & value) {
    uint32_t valueInt = union_cast<uint32_t>(value);
    data[offset++] = valueInt & 0xFF;
    data[offset++] = (valueInt >>  8) & 0xFF;
    data[offset++] = (valueInt >> 16) & 0xFF;
    data[offset++] = (valueInt >> 24) & 0xFF;
  }

  static T* deserialize(const uint8_t* data, uint32_t& offset) {
    T* v; deserialize(data, offset, v); return v;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, T*& value) {
    uint32_t valueInt = 0;
    valueInt = data[offset++];
    valueInt |= (data[offset++] <<  8);
    valueInt |= (data[offset++] << 16);
    valueInt |= (data[offset++] << 24);
    value = union_cast< T* >(valueInt);
  }
  static std::size_t size(T* const & value = 0) {
    return sizeof(value);
  }
 private:
  Serializer();
};


//specialization for float
template< >
class Serializer<float>{
 public:
  static void serialize(uint8_t* data, uint32_t& offset, const float& value) {
    uint32_t valueInt = union_cast<uint32_t>(value);
    data[offset++] = valueInt & 0xFF;
    data[offset++] = (valueInt >>  8) & 0xFF;
    data[offset++] = (valueInt >> 16) & 0xFF;
    data[offset++] = (valueInt >> 24) & 0xFF;
  }

  static float deserialize(const uint8_t* data, uint32_t& offset) {
    float v; deserialize(data, offset, v); return v;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, float& value) {
    uint32_t valueInt = 0;
    valueInt = data[offset++];
    valueInt |= (data[offset++] <<  8);
    valueInt |= (data[offset++] << 16);
    valueInt |= (data[offset++] << 24);
    value = union_cast< float >(valueInt);
  }
  static std::size_t size(const float& value = 0.0) {
    return sizeof(value);
  }
 private:
  Serializer();
};

//specialization for char
template< >
class Serializer<char>{
 public:
  static void serialize(uint8_t* data, uint32_t& offset, const char& value) {
    data[offset++] = value;
  }
  
  static char deserialize(const uint8_t* data, uint32_t& offset) {
    char v; deserialize(data, offset, v); return v;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, char& value) {
    value = data[offset++];
  }
  static std::size_t size(const char& value = 0) {
    return sizeof(value);
  }
 private:
  Serializer();
};

// Specialization for enum types
template <typename T>
class Serializer<T, typename enable_if<is_enum<T>::value>::type> {
public:
	typedef uint32_t EnumIntType;
	static void serialize(uint8_t* data, uint32_t& offset, const T& value) {
		return Serializer<EnumIntType>::serialize(data, offset, static_cast<EnumIntType>(value));
	}

	static T deserialize(const uint8_t* data, uint32_t& offset) {
		return static_cast<T>( Serializer<EnumIntType>::deserialize(data, offset) );
	}

	static void deserialize(const uint8_t* data, uint32_t& offset, T& value) {
		value = static_cast<T>( Serializer<EnumIntType>::deserialize(data, offset) );
	}
        static std::size_t size(const T& value) {
	  return sizeof(value);
	}

private:
	Serializer();		
};

// Specialization for classes deriving from Serializable
template <typename T>
class Serializer<T, typename enable_if<is_base_of<Serializable,T>::value>::type> {
public:
	static void serialize(uint8_t* data, uint32_t& offset, const T& obj) {
		obj.serialize(data+offset); offset += obj.serializedSize();
	}

	static T deserialize(const uint8_t* data, uint32_t& offset) {
		// CAUTION: There is a significant overhead invovled when deserializing 'heavy' objects
		// Solution: use C++11 and move semantics, or pass object by reference // Todo: apply this
		// Thus better use the next function
		T obj; deserialize(data, offset, obj); return obj;
	}

	static void deserialize(const uint8_t* data, uint32_t& offset, T& obj) {
		obj.deserialize(data+offset); offset +=  obj.serializedSize();
	}
        static std::size_t size(const T& obj) {
	  return obj.size();
	}
};

// Specialization for strings
template < >
class Serializer<std::string> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const std::string& obj) {
    Serializer<uint16_t>::serialize(data, offset, obj.size());
    std::for_each(obj.begin(), obj.end(), SerializeFunctor<char>(data, offset));
  }

  static std::string deserialize(const uint8_t* data, uint32_t& offset) {
    std::string obj;
    deserialize(data, offset, obj); 
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, std::string& obj) {
    uint16_t len = Serializer<uint16_t>::deserialize(data, offset);
    for(uint16_t i = 0; i < len; i++){
      obj.push_back(Serializer<char>::deserialize(data, offset));
    }
  }
  static std::size_t size(const std::string& obj) {
    return sizeof(uint16_t) + obj.size()*Serializer<char>::size();
  }
};

// Specialization for pairs
template <typename T1, typename T2>
class Serializer<std::pair<T1, T2> >{
public:
  static void serialize(uint8_t* data, uint32_t& offset, const std::pair<T1, T2>& obj) {
    Serializer<T1>::serialize(data, offset, obj.first);
    Serializer<T2>::serialize(data, offset, obj.second);
  }

  static std::pair<T1, T2> deserialize(const uint8_t* data, uint32_t& offset) {
    std::pair<T1, T2> obj;
    deserialize(data, offset, obj); 
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, std::pair<T1, T2>& obj) {
    obj.first = Serializer<T1>::deserialize(data, offset);
    obj.second = Serializer<T2>::deserialize(data, offset);
  }
  static std::size_t size(const std::pair<T1, T2>& obj) {
    return Serializer<T1>::size(obj.first) + Serializer<T2>::size(obj.second);
  }
};

// Specialization for vectors
template <typename T>
class Serializer<std::vector<T> > {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const std::vector<T>& obj) {
    Serializer<uint32_t>::serialize(data, offset, obj.size());
    std::for_each(obj.begin(), obj.end(), SerializeFunctor<T>(data, offset));
  }

  static std::vector<T> deserialize(const uint8_t* data, uint32_t& offset) {
    std::vector<T> obj;
    deserialize(data, offset, obj); 
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, std::vector<T>& obj) {
    uint32_t len = Serializer<uint32_t>::deserialize(data, offset);
    for(uint32_t i = 0; i < len; i++){
      obj.push_back(Serializer<T>::deserialize(data, offset));
    }
  }
  static std::size_t size(const std::vector<T>& obj) {
    return sizeof(uint32_t) + std::accumulate(obj.begin(), obj.end(), 0, sum_size<T>);
  }
};

// Specialization for maps
template <typename Key, typename T>
class Serializer<std::map<Key, T> > {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const std::map<Key, T>& obj) {
    Serializer<uint32_t>::serialize(data, offset, obj.size());
    std::for_each(obj.begin(), obj.end(), SerializeFunctor<std::pair<Key, T> >(data, offset));
  }

  static std::map<Key, T> deserialize(const uint8_t* data, uint32_t& offset) {
    std::map<Key, T> obj;
    deserialize(data, offset, obj); 
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, std::map<Key, T>& obj) {
    uint32_t len = Serializer<uint32_t>::deserialize(data, offset);
    for(uint32_t i = 0; i < len; i++){
      obj.insert(Serializer<std::pair<Key, T> >::deserialize(data, offset));
    }
  }
  static std::size_t size(const std::map<Key, T>& obj) {
    return sizeof(uint32_t) + std::accumulate(obj.begin(), obj.end(), 0, sum_size<std::pair<Key, T> >);
  }
};

// Specialization for arrays
template <typename T, size_t Size>
class Serializer<std::array<T, Size>> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const std::array<T, Size>& obj) {
    Serializer<uint32_t>::serialize(data, offset, obj.size());
    std::for_each(obj.begin(), obj.end(), SerializeFunctor<T>(data, offset));
  }

  static std::array<T, Size> deserialize(const uint8_t* data, uint32_t& offset) {
    std::array<T, Size> obj;
    deserialize(data, offset, obj);
    return obj;
  }
  
  static void deserialize(const uint8_t* data, uint32_t& offset, std::array<T, Size>& obj) {
    uint32_t len = Serializer<uint32_t>::deserialize(data, offset);
    for(uint32_t i = 0; i < len; i++){
      obj.at(i) = Serializer<T>::deserialize(data, offset);
    }
  }
  static std::size_t size(const std::array<T, Size>& obj) {
    return sizeof(uint32_t) + std::accumulate(obj.begin(), obj.end(), 0, sum_size<T>);
  }
};


#endif //__SERIALIZER_H__
