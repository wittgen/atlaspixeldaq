#ifndef __COMMAND_H__
#define __COMMAND_H__

#include "CppCompatibility.h" // Keep this header first!
#include "CommandTraits.h"

#include "Serializable.h"
#include "CleanTypes.h"


/*
 * Template class to provide one type ID per class.
 */

template <typename T_ID>
class TypeId {
public:
	static uint16_t getTypeId() { return TypeId<T_ID>::s_TypeId; }
	static void setTypeId(const uint16_t t_id) { TypeId<T_ID>::s_TypeId = t_id; }
protected:
	static uint16_t s_TypeId;	//! Global command id: unique to each command. Set by the command manager.
};

template <typename T_ID>
uint16_t TypeId<T_ID>::s_TypeId = 0;

class Command;

class CommandInterface {
public:
	virtual std::string toString() const = 0;
	virtual uint16_t getExecId() const = 0;
	virtual void setExecId(const uint16_t) = 0;
	virtual uint16_t getTypeId() const = 0;
	virtual void execute() = 0;
	virtual const Command* getResult() const = 0;
	virtual shared_ptr<Command> getCopy() const = 0;

	// Todo: Get this out of user's realm
	virtual void setResultPtr() { }

	virtual ~CommandInterface() {}
};

class Command: public CommandInterface, virtual public Serializable {
public:
	std::string toString() const { return TypeName(*this); }

	uint16_t getExecId() const { return m_ExecId; }
	void setExecId(const uint16_t e_id) { m_ExecId = e_id; }

	virtual void execute() {};
	const Command* getResult() const {
		return this->m_ResultPtr;
	}

	Command():m_ResultPtr(NULL){}
	virtual ~Command() {};

protected:
	uint16_t m_ExecId;			//! Run-time command id: specific to a given command instance.
	const Command *m_ResultPtr;	//! Pointer to the result of a function, which must also be a command
private: // A base class should probably not have private members, unless necessary

	friend class CommandManager;
};

template <typename D>
class CommandCRTP: public Command {
public:
	// Deal with providing class type ID
	uint16_t getTypeId() const { return TypeId<D>::getTypeId(); }
	virtual shared_ptr<Command> getCopy() const {
		shared_ptr<Command> cmd = shared_ptr<Command>( new D(static_cast<D const&>(*this)) );
		// Updates the result pointer
		// Todo: make it better
		cmd->setResultPtr();
		return cmd;
	}
};



#define InstantiateResult(Type) \
	Type result; \
	typedef Type ResultType; \
	virtual void setResultPtr() { this->m_ResultPtr = &result; }

/*
 * This is an example on how to instantiate a command and its corresponding result.
 */

class MyResult: public CommandCRTP<MyResult > {
public:
	virtual ~MyResult() {}
};


class MyCommand: public CommandCRTP<MyCommand > {
//class MyCommand: public CommandCRTP<MyCommand> public CommandBase {
 public:
  InstantiateResult(MyResult)
    
  void serialize(uint8_t *out) const { }
  void deserialize(const uint8_t *in) { }
  uint32_t serializedSize() const { return 0; }
  MyCommand() { setResultPtr(); }
  virtual ~MyCommand() {}
 protected:
 private:
};


#endif // __COMMAND_H__

