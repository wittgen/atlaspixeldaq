/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-X-20
 *
 * Description: This is not your regular include file. It's only meant to be included _once_ in every app using the framework!
 * Todo: Ensure that a warning is shown when included more than once? 
 * Updates:
 */ 

#ifndef __COMMAND_REGISTRAR_H__
#define __COMMAND_REGISTRAR_H__

#include "CommandManager.h"
#include "RodCommand.h"
#include "VeryLargeRodCommand.h"
#include "TemplateRodCommand.h"
#include "DumpRodRegisters.h"
#include "TestRodRegisters.h"
#include "TestRodBusy.h"
#include "WriteRodMasterBusyMask.h"
#include "ReadRodMasterBusyStatus.h"
#include "ResetRodBusyHistograms.h"
#include "IblDigScan.h"
#include "DumpBocLinkStatus.h"
#include "WriteModuleConfig.h"
#include "SendModuleConfig.h"
#include "ReadBackFei4Registers.h"
#include "SendConfig.h"
#include "ReadModuleConfig.h"
#include "EnableChannels.h"
#include "PixScanBaseToRod.h"
#include "IblThreshScan.h"
#include "TestResultSize.h"
#include "StartScan.h"
#include "TestBenchCommand.h"
#include "GetStatus.h"
#include "TestSlave.h"
#include "WriteConfig.h"
#include "SetUart.h"
#include "ConfigScan.h"
#include "BmfFeEmuConfig.h"
#include "IblDataTakingWithEmulator.h"
#include "WriteRodSlaveBusyMask.h"
#include "ReadRodSlaveBusyStatus.h"
#include "DigitalInjectionTest.h"
#include "WriteRodRegisters.h"
#include "ReadConfig.h"
#include "KillCommand.h"
#include "PrintCommands.h"
#include "ListCommands.h"
#include "SetSlaveVerbose.h"
#include "GetDebugHists.h"
#include "ProduceData.h"
#include "ResetSlave.h"
#include "IblDataTaking.h"
#include "IblDataTakingWithFEEmulator.h"
#include "IblDataTakingWithRoLEmulator.h"
#include "SlaveLoader.h"
#include "ReadUartOutput.h"
#include "TestMapping.h"
#include "SingleWrite.h"
#include "SingleRead.h"
#include "DataTakingConfig.h"
#include "DataTakingTools.h"
#include "TestRegister.h"
#include "RegisterReadBack.h"
#include "SetNetwork.h"
#include "ReconfigModules.h"
#include "IblRodStatus.h"
#include "CountOccupancy.h"
#include "GetVersionInfo.h"
#include "SetScanPixelDisable.h"
#include "AbortScan.h"
#include "GetServiceRecords.h"
#include "GetGADCOutput.h"
#include "GetGADCLeakage.h"
#include "CountFmtOccupancy.h"
#include "QSTalkTask.h"
#include "ReadBocCounters.h"
#include "QSTestHelper.h"
#include "EnableHPI.h"

// Barrel ROD specific code (+ development tools)
#include "OptoScan.h"
#include "DummyOptoTune.h"
#include "DummyScan.h"
#include "TestRxHisto.h"

#include "StartHisto.h"
#include "StopHisto.h"
#include "CheckHistCfg.h"

#include "HalfClockReturn.h"
#include "MCCTest.h"
#include "MCCTestScan.h"
#include "MCCInjectPattern.h"
#include "Fei3Test.h"
#include "SetRxDelayThreshold.h"
#include "StartScanL12.h"
#include "Fei3Readback.h"
#include "Fei3TestScan.h"
#include "GetDSPResults.h"
#include "GetRxConfig.h"
#include "CleanDSPResults.h"
#include "SetDSPScan.h"
#include "BocSendConfig.h"
#include "MCCConnScan.h"
#include "LogPublisherCmd.h"
#include "RecoAtECR_manager.h"
#include "ReadRodVetoCounters.h"
#include "DelayScan.h"
#include "VerifyOptoTune.h"
#include "Fei3MonLeak.h"
#include "Fei4MonLeak.h"
#include "IblOptoScan.h"
#include "TuneGADCVref.h"
#include "GetFEConfigHash.h"
#include "GetFei3CfgRod.h"
#include "GetFei4CfgRod.h"
#include "WriteRegister.h"
#include "IBLChannelMapping.h"

struct CommandRegistrar {
	CommandRegistrar() {
		CommandManager::registerCommand<RodCommand>();
		CommandManager::registerCommand<NoWaitRodCommand>();
		CommandManager::registerCommand<DumpRodRegisters>(true); //stealth mode
		CommandManager::registerCommand<TestRodRegisters>();
		CommandManager::registerCommand<TestRodBusy>();
		CommandManager::registerCommand<ReadRodMasterBusyStatus>(true); //stealth mode
		CommandManager::registerCommand<WriteRodMasterBusyMask>();
		CommandManager::registerCommand<ResetRodBusyHistograms>();
		CommandManager::registerCommand<WriteRodRegisters>();
		CommandManager::registerCommand<IblDigScan>();
		CommandManager::registerCommand<DumpBocLinkStatus>();
		CommandManager::registerCommand<WriteModuleConfig>();
		CommandManager::registerCommand<SendModuleConfig>();
		CommandManager::registerCommand<ReadModuleConfig>();
		CommandManager::registerCommand<EnableChannels>();
		CommandManager::registerCommand<PixScanBaseToRod>();
		CommandManager::registerCommand<IblThreshScan>();
		CommandManager::registerCommand<TestResultSize>();
		CommandManager::registerCommand<StartScan>();
		//CommandManager::registerCommand<WritePixelRegisterConfig>();
		//		CommandManager::registerCommand<SendPixelRegisterConfig>();
		CommandManager::registerCommand<TestBenchCommand>();
		CommandManager::registerCommand<GetStatus>(true); // stealth mode	
		CommandManager::registerCommand<TestSlave>();
		CommandManager::registerCommand<TemplateRodCommand<int> >();
		//CommandManager::registerCommand<Set<std::string,Stage::getHowDoIFeel> >();
		//CommandManager::registerCommand<Get<std::string,Stage::getHowDoIFeel> >();
		//		CommandManager::registerCommand<WriteConfig<Fei4PixelCfg> >();
		//	CommandManager::registerCommand<WriteConfig<Fei4ExtCfg> >();
		//CommandManager::registerCommand<WriteConfig<Fei4GlobalCfg> >();
		CommandManager::registerCommand<WriteConfig<Fei4Cfg> >();
		CommandManager::registerCommand<SendConfig>();
		CommandManager::registerCommand<SetUart>();
		CommandManager::registerCommand<ConfigScan>();
		CommandManager::registerCommand<DigitalInjectionTest>();
		//CommandManager::registerCommand<IterSet<DoubleColumnBit,Stage::testFei4PixelCfg_f, CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> > >();
		//CommandManager::registerCommand<IterGet<DoubleColumnBit,Stage::testFei4PixelCfg_f, CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> > >();
		//CommandManager::registerCommand<IterSet<DoubleColumnBit,Stage::testIter_f,CircularIterator<DoubleColumnBit,5> > >();
		//CommandManager::registerCommand<IterGet<DoubleColumnBit,Stage::testIter_f,CircularIterator<DoubleColumnBit,5> > >();
		CommandManager::registerCommand<BmfFeEmuConfig>();
		CommandManager::registerCommand<IblDataTakingWithEmulator>();
		CommandManager::registerCommand<ReadConfig>();
		CommandManager::registerCommand<WriteRodSlaveBusyMask>(true); //stealth mode
		CommandManager::registerCommand<ReadRodSlaveBusyStatus>(true); //stealth mode
		CommandManager::registerCommand<KillCommand>();
		CommandManager::registerCommand<KillAllCommands>();
		CommandManager::registerCommand<PrintCommands>();
		CommandManager::registerCommand<ListCommands>();
		CommandManager::registerCommand<SetSlaveVerbose>();
		CommandManager::registerCommand<VeryLargeRodCommand>();
		CommandManager::registerCommand<GetDebugHists>();
		CommandManager::registerCommand<ProduceData>();
		CommandManager::registerCommand<ResetSlave>();
		CommandManager::registerCommand<IblDataTaking>();
		CommandManager::registerCommand<IblDataTakingWithFEEmulator>();
		CommandManager::registerCommand<IblDataTakingWithRoLEmulator>();
		CommandManager::registerCommand<SlaveLoader>();
		CommandManager::registerCommand<ReadUartOutput>();
		CommandManager::registerCommand<TestMapping>();
		CommandManager::registerCommand<SingleWrite>();
		CommandManager::registerCommand<SingleRead>();
		CommandManager::registerCommand<DataTakingConfig>();
		CommandManager::registerCommand<DataTakingTools>();
		CommandManager::registerCommand<TestRegister>();
		CommandManager::registerCommand<RegisterReadBack>();
		CommandManager::registerCommand<SetNetwork>();
		CommandManager::registerCommand<ReconfigModules>();
		CommandManager::registerCommand<IblRodStatus>(true);
		CommandManager::registerCommand<CountOccupancy>(true);
		CommandManager::registerCommand<GetVersionInfo>();
		CommandManager::registerCommand<SetScanPixelDisable>();
		CommandManager::registerCommand<AbortScan>();
		CommandManager::registerCommand<GetServiceRecords>();
		CommandManager::registerCommand<GetGADCOutput>();
		CommandManager::registerCommand<GetGADCLeakage>();
		CommandManager::registerCommand<CountFmtOccupancy>(true);
		CommandManager::registerCommand<QSTalkTask>();
		CommandManager::registerCommand<ReadBocCounters>(true); // stealth mode
		CommandManager::registerCommand<QSTestHelper>();

// Barrel ROD specific code (+ development tools)

		CommandManager::registerCommand<OptoScan>();
		CommandManager::registerCommand<DummyOptoTune>();
		CommandManager::registerCommand<DummyScan>();
		CommandManager::registerCommand<TestRxHisto>();

		CommandManager::registerCommand<StartHisto>();   //  Nick's test template for making sense of Host Command usage
		CommandManager::registerCommand<StopHisto>();   
		CommandManager::registerCommand<CheckHistCfg>(); //  Another of Nick's trial runs
		
		CommandManager::registerCommand<EnableHPI>(); //Enable HPI register

		CommandManager::registerCommand<WriteConfig<Fei3ModCfg> >();
		CommandManager::registerCommand<HalfClockReturn>();
		CommandManager::registerCommand<MCCTest>();
		CommandManager::registerCommand<MCCTestScan>();
		CommandManager::registerCommand<MCCInjectPattern>();
		CommandManager::registerCommand<Fei3Test>();
		CommandManager::registerCommand<SetRxDelayThreshold>();
		CommandManager::registerCommand<StartScanL12>();
		CommandManager::registerCommand<Fei3Readback>();
                CommandManager::registerCommand<Fei3TestScan>();
                CommandManager::registerCommand<GetDSPResults>(); 
                CommandManager::registerCommand<GetRxConfig>();
                CommandManager::registerCommand<CleanDSPResults>();
                CommandManager::registerCommand<SetDSPScan>();
                CommandManager::registerCommand<BocSendConfig>();
		CommandManager::registerCommand<ReadBackFei4Registers>();
		CommandManager::registerCommand<MCCConnScan>();
      		CommandManager::registerCommand<LogPublisherCmd>();	
     		CommandManager::registerCommand<RecoAtECR_manager>();
		CommandManager::registerCommand<ReadRodVetoCounters>();
		CommandManager::registerCommand<DelayScan>();
		CommandManager::registerCommand<VerifyOptoTune>();
		CommandManager::registerCommand<Fei3MonLeak>();
		CommandManager::registerCommand<Fei4MonLeak>();
		CommandManager::registerCommand<IblOptoScan>();
		CommandManager::registerCommand<TuneGADCVref>();
		CommandManager::registerCommand<GetFEConfigHash>();
		CommandManager::registerCommand<GetFei3CfgRod>();
		CommandManager::registerCommand<GetFei4CfgRod>();
		CommandManager::registerCommand<WriteRegisterFEI3<Fei3::GlobReg>>();
		CommandManager::registerCommand<WriteRegisterFEI3<Fei3::PixReg>>();
		CommandManager::registerCommand<WriteRegisterFEI4<Fei4::GlobReg>>();
		CommandManager::registerCommand<WriteRegisterFEI4<Fei4::PixReg>>();
		CommandManager::registerCommand<IBLChannelMapping>();
	}
};

#endif // __COMMAND_REGISTRAR_H__
