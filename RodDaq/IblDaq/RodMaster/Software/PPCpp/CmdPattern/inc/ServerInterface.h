/* 
 * File:   ServerInterface.h
 * 
 * [GOF::Facade] Abstracts all the details of communications with the server and
 * command handling to the client.
 */

#ifndef SERVERINTERFACE_H
#define	SERVERINTERFACE_H
#include "Link.h"

#include <stdint.h>
#include <iostream>

class ServerInterface {
public:
    /** 
     *  Constructor, randomizes s_ExecId
     *
     */
    ServerInterface();

    /**
     * Connect a client with the server.
     * @return <code>true</code> iff the connection was successful, 
     * <code>false</code> otherwise.
     */
    bool connectToServer(const char* ip = "127.0.0.1", uint16_t port = CMD_SRV_IP_PORT);
    
    /**
     * Disconnect the client from the server.
     */
    void disconnectFromServer();

    // Server commands ---------------------------------------------------------
    template <typename T>
    bool sendCommand(T& cmd) {
            ServerInterface::s_ExecId++;
	    cmd.setExecId(ServerInterface::s_ExecId);
	    return this->link.sendCommand(&cmd);
    }
    // -------------------------------------------------------------------------
private:
    Link link;
    static uint16_t s_ExecId;
};

#endif	/* SERVERINTERFACE_H */

