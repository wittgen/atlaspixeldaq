/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-VII-10
 */

#include "stdint.h"

#ifndef __COMMAND_TRAITS_H__
#define __COMMAND_TRAITS_H__

/*
 * Traits structure to check for the presence of typedef ResultType in a class/struct
 * has_result_type<Command>::value is either true of false
 */

template <typename T>
struct has_result_type {
	typedef uint8_t yes[1];
	typedef uint8_t no[2];

	template <typename TT>
	static yes& test(typename TT::ResultType*);

	template <typename TT>
	static no& test(...);

	static const bool value = sizeof(test<T>(0)) == sizeof(yes);

};

#endif // __COMMAND_TRAITS_H__
