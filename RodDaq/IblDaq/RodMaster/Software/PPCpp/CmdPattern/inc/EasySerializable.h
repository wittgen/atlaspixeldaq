/*
Steve Alkire <alkire@cern.ch>
2014.02.18
Use to make serializing a class simpler and more error proof.

The only way to use this class: 
1) Derive your class from EasySerializable
2) In your h file: virtual void serial(); virtual void serial() const;
3) in your cxx file prep your class members you want to serialize like so--yes the syntax is correct ;) : 
SERIAL(MyClass,
  prep(m_MyMember);
  prep(m_MyMemberArray,LENGTH_OF_ARRAY);
  etc.
)
*/
#ifndef __EASYSERIALIZABLE_H__
#define __EASYSERIALIZABLE_H__

#include "Serializer.h"

#include <iostream>       
#include <vector>
#include <string>

#define SERIAL(yourClass,prep) void yourClass::serial(){prep} void yourClass::serial() const{prep}
#define SERIAL_H(prep) virtual void serial(){prep} virtual void serial() const{prep}
 
class EasySerializable : virtual public Serializable {
 private:
  enum ActionSwitch{doSerializedSize , doSerialize};
  mutable ActionSwitch m_ActionSwitch;
  mutable uint32_t m_SerializedSize;
  mutable uint8_t *m_out;
  const uint8_t *m_in;
  mutable uint32_t m_offset;
 public:
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  uint32_t serializedSize() const;
  virtual ~EasySerializable(){}
  EasySerializable(){}
  virtual void serial() = 0;
  virtual void serial() const = 0;
  
  //regular variables
  template<typename T> void prep(T& datum,typename enable_if<!is_base_of<Serializable,T>::value>::type* = 0){
    datum = Serializer<T>::deserialize(m_in,m_offset); 
  }
  template<typename T> void prep(const T& datum, typename enable_if<!is_base_of<Serializable,T>::value>::type* = 0) const{
    switch(m_ActionSwitch){
    case doSerializedSize:
      m_SerializedSize+=sizeof(datum);
	break;
    case doSerialize:
      Serializer<T>::serialize(m_out,m_offset,datum);
      break;
    }
  }
  
  //Classes--working
  template<typename T> void prep(T& datum, typename enable_if<is_base_of<Serializable,T>::value>::type* = 0){
      T localDatum;
      Serializer<T>::deserialize(m_in,m_offset,localDatum);
      datum = localDatum;
  }
  template<typename T> void prep(const T& datum, typename enable_if<is_base_of<Serializable,T>::value>::type* = 0) const{
    switch(m_ActionSwitch){
    case doSerializedSize:
      m_SerializedSize+=datum.serializedSize();
      break;
    case doSerialize:
      Serializer<T>::serialize(m_out,m_offset,datum);
      break;
    }
  }
    
  //arrays
  template<typename T> void prep(T* data, unsigned arraySize){
    for(unsigned i = 0; i < arraySize; i++){
      prep(data[i]);
    }
  }
  template<typename T> void prep(const T* data, unsigned arraySize) const{
    for(unsigned i = 0; i < arraySize; i++){
      prep(data[i]);
    }
  }
  //2D-arrays                                                                                            
  template<typename T, int _INNER> void prep(T (*data)[_INNER], int outer){
    for(int i = 0; i < outer; ++i){
      for(int j = 0; j < _INNER; ++j){
        prep(data[i][j]);
      }
    }
  }
  template<typename T, int _INNER> void prep(const T (*data)[_INNER], int outer) const{
    for(int i = 0; i < outer; ++i){
      for(int j = 0; j < _INNER; ++j){
        prep(data[i][j]);
      }
    }
  } 
  //vectors
  template<typename T> void prep(std::vector<T>& data){
    uint8_t length = Serializer<uint8_t>::deserialize(m_in,m_offset);
    data.reserve(length);
    for (unsigned i =0; i < length; i++){
      T val; 
      prep(val);
      data.push_back(val);
    }    
  }
  template<typename T> void prep(const std::vector<T>& data) const{
    uint8_t length = data.size();
    switch(m_ActionSwitch){
    case doSerializedSize:
      m_SerializedSize+=sizeof(uint8_t);
      break;
    case doSerialize:
      Serializer<uint8_t>::serialize(m_out,m_offset,data.size()); 
      break;
    }
    for(uint8_t i  = 0; i < length; i++){
      prep(data.at(i));
    }
      
  }

  //strings
  void prep(std::string& datum){
    uint8_t length = Serializer<uint8_t>::deserialize(m_in,m_offset);
    datum="";
    for(uint8_t i = 0; i < length; i++){
      datum+=Serializer<char>::deserialize(m_in,m_offset);
    }
  }
  void prep(const std::string& datum) const{
    switch(m_ActionSwitch){
    case doSerializedSize:
      m_SerializedSize+=sizeof(uint8_t);
      m_SerializedSize+=datum.length()*sizeof(char);
      break;
    case doSerialize:
      uint8_t length = datum.length();
      Serializer<uint8_t>::serialize(m_out,m_offset,length);
      for(uint8_t i = 0; i < length; i++){
	Serializer<char>::serialize(m_out,m_offset,datum.at(i));
      }
      break;
    }
  }  
};

#endif //__EASYSERIALIZABLE_H__
