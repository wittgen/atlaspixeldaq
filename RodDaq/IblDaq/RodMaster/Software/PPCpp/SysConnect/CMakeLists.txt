project(SysConnect)

add_library(${PROJECT_NAME} STATIC
    src/IblBoc.cxx
    src/IblBocLink.cxx
    src/IblBocRxFifo.cxx
    src/IblBocTxFifo.cxx
    src/IblBocTxFifoL12.cxx
    src/IblRodInmemFifo.cxx
    src/IblRodSerialPort.cxx
    src/IblRodSlave.cxx
    src/IblRodVersionInfo.cxx
    #    src/PrmSpiDev.cxx
    src/RodMasterRegisters.cxx
    # src/MemoryMapHandler.cxx
    src/NetUtils.cxx
    src/MMapHandler.cxx
)

target_include_directories(${PROJECT_NAME}
    PUBLIC inc
)

target_compile_definitions(${PROJECT_NAME} PRIVATE
	GIT_HASH=${PPC_GIT_HASH} GIT_DATE=${PPC_GIT_DATE} GIT_WORKDIR=${PPC_GIT_WORKDIR} BUILD_DATE=${PPC_BUILD_DATE} BUILD_HOST=${PPC_BUILD_HOST} BUILD_USER=${PPC_BUILD_USER} CROSSCOMPILER_VERSION=${PPC_CROSSCOMPILER_VERSION}
    )


target_link_libraries(${PROJECT_NAME}
    PUBLIC Utilities
    PUBLIC PPCpp
    PUBLIC FrontEnd
    PUBLIC CmdPattern
    PUBLIC DspInterface
    PUBLIC RodMaster_FWSW_INTERFACE
    PUBLIC RodSlave_FWSW_INTERFACE
    )
