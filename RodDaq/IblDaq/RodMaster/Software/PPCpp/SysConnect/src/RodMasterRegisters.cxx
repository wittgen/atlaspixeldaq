/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch
 * Date: 2013-VI-01
 * Description:
 */

#include "CppCompatibility.h" // Keep this header first!
#include "RodMasterRegisters.h" // Keep this header first!

#include "RodSlaveRegistersFromMaster.ixx"
#include "RodMasterRegistersFromMaster.ixx"

#ifndef __XMK__ // For emulator
#define xil_printf printf

// Dummy values
// Todo: sort this out.
#define SLV_VERSION_REG 0

uint32_t IblRod::CE0_BASE;
uint32_t IblRod::COMMON_REG_BASE;
uint32_t IblRod::BOC_REG_BASE;
uint32_t IblRod::PPC_CTL_REG;
uint32_t IblRod::PPC_STAT_REG;
uint32_t IblRod::PPC_SPORT_REG;
uint32_t IblRod::CTRL_REGS;
uint32_t IblRod::PPC_DESIGN_REG;

uint32_t IblRod::PPC_WDOG_REG;
uint32_t IblRod::GEO_ADDR_REG;
uint32_t IblRod::PPC_BUSY_MASK_REG;
uint32_t IblRod::PPC_BUSY_MASTER_STATUS_REG;
uint32_t IblRod::PPC_BUSY_MASTER_OUT_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_BUSYNINP0_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_BUSYNINP1_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_FORMATTER0_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_FORMATTER1_HIST_REG;
uint32_t IblRod::PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG;
uint32_t IblRod::PPC_UART_S6A_STATUS_REG;
uint32_t IblRod::PPC_UART_S6A_DATA_REG;
uint32_t IblRod::PPC_UART_S6B_STATUS_REG;
uint32_t IblRod::PPC_UART_S6B_DATA_REG;
uint32_t IblRod::PPC_UART_MASTER_STATUS_REG;
uint32_t IblRod::PPC_UART_MASTER_DATA_REG;

#endif


void RodMaster::initEPC() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;

   // memory map register zones
   MMapHandler::mmap_init();

	IblRod::EpcLocalCtrl::write(1);

	xil_printf("EPC base from offset 0x%x\r\n",IblRod::EpcBase::addr());
//	xil_printf("EPC size 0x%x bytes\r\n",EPC_WORD_SIZE * 4);
	xil_printf("Local registers from offset 0x%x\r\n",IblRod::EpcLocal::addr());
	xil_printf("Local ctl registers from offset 0x%x\r\n",IblRod::EpcLocalCtrl::addr());
	xil_printf("Local stat registers from offset 0x%x\r\n",IblRod::EpcLocalStat::addr());
	xil_printf("Local spmask registers from offset 0x%x\r\n",IblRod::EpcLocalSpMask::addr());

	xil_printf("IblRod::EpcBoc 0x%x\r\n", IblRod::EpcBoc::addr());
	// design id
	// TODO: read out git hash instead of design ID
	xil_printf("Design ID: 0x%x, expected: 0x%x\r\n",IblRod::DesignRegPPC::read(),0xdeadbeef); // Todo: implement replacement to HDL_FPGA_VERSION
	// test ctl status register
	xil_printf("Status register: 0x%x\r\n",IblRod::EpcLocalStat::read());
	int testValue = 0x5115c66c; // Todo: check why for some reason 0x5aa5c66c seems to hang output (but not operation)
	IblRod::EpcLocalCtrl::write(testValue);
	xil_printf("Clt register (0x%x): 0x%x\r\n",testValue,IblRod::EpcLocalCtrl::read());
	IblRod::EpcLocalCtrl::write(0);
	xil_printf("Clt register (0): 0x%x\r\n",IblRod::EpcLocalCtrl::read());
	// access dsp register area
	//IblRod::EpcLocalCtrl::set(1 << PPC_CTL_MASTER_BIT);
	IblRod::EpcLocalCtrlMasterBit::write(1);
	xil_printf("Acquiring DSP register area\r\n");
	IblRod::EpcLocalCtrlHpiEnable::write(1);
	xil_printf("Enabling HPI memory access\n");

	xil_printf("Clt register (0): 0x%x\r\n",IblRod::EpcLocalCtrl::read());
	//*************************************************************************************
	//*************************************************************************************
	// test controller version
   xil_printf("Controller version (at 0x%p): 0x%x\r\n",(void*)IblRod::DspRegBase::addrPtr(3),IblRod::DspRegBase::at(3));
	std::cout << "SlaveA Fmt0StatusReg @ " << HEX( IblRod::SlaveA::Fmt0StatusReg::addr() ) << ": " << HEX( IblRod::SlaveA::Fmt0StatusReg::read() ) << std::endl;
	std::cout << "SlaveB Fmt0StatusReg @ " << HEX( IblRod::SlaveB::Fmt0StatusReg::addr() ) << ": " << HEX( IblRod::SlaveB::Fmt0StatusReg::read() ) << std::endl;

#if 0
	epcSlvARam[0] = 0;
	epcSlvBRam[0] = 0;

	// some ram location
	for (i=1;i<10;i++){
		epcSlvARam[i] = 0x5aa56cc0 +i;
	}
	for (i=1;i<10;i++){
		epcSlvBRam[i] = 0xc66ca550 +i;
	}
	for (i=1;i<10;i++){
		xil_printf("Formatter0 ram (at 0x%x): 0x%x\r\n",&epcSlvARam[i],epcSlvARam[i]);
		xil_printf("Formatter1 ram (at 0x%x): 0x%x\r\n",&epcSlvBRam[i],epcSlvBRam[i]);
	}
#endif

}

void RodMaster::setFeCmdMask(uint8_t slaveId, uint16_t mask) {
	IblRod::EpcLocal::write( 0xC + (slaveId<<1), mask );
	std::cout << "FE command mask for slave " << (int)slaveId << " set to " << HEX(IblRod::EpcLocal::read( 0xC + (slaveId<<1) )) << std::endl;
}

void RodMaster::setFeCmdMask(uint32_t mask) {
	for(int slaveId = 0 ; slaveId < 2 ; ++slaveId)
		RodMaster::setFeCmdMask(slaveId, (mask & (0xFFFF<<(slaveId*16))) >> (slaveId*16) );
}


