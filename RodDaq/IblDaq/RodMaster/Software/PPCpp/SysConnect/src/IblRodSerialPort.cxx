/*
 * Author: K. Potmianos <karolos.potamianos@cern.ch>
 * Date: 2013-VI-10
 * Author: M.Bindi
 * Fundamental change in the Register settings that allow to change the serial port immediately or at the next trigger
 */

#include "RodResourceManager.h"

#define BOLOGNA_SPORT


#include <iterator>

#ifndef __XMK__

#define PPC_CTL_SPLOCLINK_BIT 31

uint32_t IblRodSerialPort::XPAR_MCBSP_ROD_0_BASEADDR_1;
uint32_t IblRodSerialPort::XPAR_MCBSP_ROD_0_BASEADDR_2;
uint32_t IblRodSerialPort::XPAR_MCBSP_ROD_0_BASEADDR_4;

#endif


void IblRodSerialPort::init() {
#ifdef BOLOGNA_SPORT
	IblRod::EpcLocalCtrl::reset(1 << PPC_CTL_SPLOCLINK_BIT);              //Bologna Sport
	xil_printf("Serial port interface: Bologna\n");
#else
	IblRod::EpcLocalCtrl::set(1 << PPC_CTL_SPLOCLINK_BIT);               //Andreas Sport
	xil_printf("Serial port interface: Mannheim\n");
#endif
	xil_printf("Clt register (0): 0x%x\r\n",IblRod::EpcLocalCtrl::read());
	//xil_printf("Control register (0): 0x%x\r\n",IblRod::EpcLocalCtrl::read());

	// Todo: have in .hxx file 
	const int boRstnBit = 16; // Attention - EDK peripheral: bit numbers must count down from 31

	Ctrl::write(0); // Reset
	TxCtrl::write(0); // Disable output
	Ctrl::write( 1 << (31 - boRstnBit) ); // Remove reset
}

int IblRodSerialPort::ready() {
	return 0;
}

/*
 * Sets the TxMask.
 * Note: bits 31-16 refer to slave 1, and 15-0 to slave 0
 */ 

void IblRodSerialPort::setTxMask(uint32_t ch_mask) {
	// Hard-coded information!
#ifdef LL_DEBUG
  std::cout<<"SERIAL PORT: setting the chnannel mask to: 0x"<<std::hex<<(int)ch_mask<<std::dec<<std::endl;
#endif

  uint32_t RodControlRegister = IblRod::Master::RrifCtrlReg::read();
  uint32_t currentSerial0 = IblRod::Master::FeCmdMask0LsbReg::read();
  uint32_t currentSerial1 = IblRod::Master::FeCmdMask1LsbReg::read();

  //if(verbose) std::cout << __PRETTY_FUNCTION__ << ": received TX mask 0x" << std::hex << ch_mask << std::dec << std::endl;
  // The mask contains 1 bit for every ROD (and BOC) Tx channel
  // However, channels 0 and 8, 1 and 9, ... 7 and 15, ... 16 and 24, ... 23 and 31 are coupled together
  // For the IBL mode, it makes no difference since there is no TX plugin installed
  // However, in the L1/L2 mode, we have to take this coupling into account, making sure to OR the channels
  if( RodResMgr::isFei3() )
    ch_mask = ((ch_mask & 0xFF000000)>>8) | (ch_mask & 0x00FF0000) | ((ch_mask & 0xFF00)>>8) | (ch_mask & 0xFF);
  //if(verbose) std::cout << __PRETTY_FUNCTION__ << ": applying TX mask 0x" << std::hex << ch_mask << std::dec << std::endl;
  std::cout<< " Modifying the actual SerialPortMask in the North: 0x" << std::hex << currentSerial0 << " using the new Global TxMask: 0x" <<ch_mask<< std::dec << std::endl;
  std::cout<< " Modifying the actual SerialPortMask in the South: 0x" << std::hex << currentSerial1 << " using the new Global TxMask: 0x" <<ch_mask<< std::dec << std::endl;
    
  //For the North or 0 (only bits [0-7]  of ch_mask)
    IblRod::Master::FeCmdMask0LsbReg::write(ch_mask & 0xff);
  //For the South or 1 (only bits [16-23] of ch_mask)
    IblRod::Master::FeCmdMask1LsbReg::write((ch_mask >> 16) & 0xff );
  //To set a mask immediately we need to set and unset the bits #20 and the bits #2;
    IblRod::Master::RrifCtrlReg::write(RodControlRegister | 0x100004);
    IblRod::Master::RrifCtrlReg::write(RodControlRegister & ~0x100004);
  //To set the mask so that it gets picked-up at the next trigger, we need to set and unset only bits #2...only a test now.
  //  IblRod::Master::RrifCtrlReg::write(RodControlRegister | 0x4);
  //  IblRod::Master::RrifCtrlReg::write(RodControlRegister & ~0x4);
    std::cout<< " Modified SerialPort in the North to : 0x" << std::hex <<  IblRod::Master::FeCmdMask0LsbReg::read()<< std::dec << std::endl;
    std::cout<< " Modified SerilaPort in the South to : 0x" << std::hex <<  IblRod::Master::FeCmdMask1LsbReg::read() << std::dec << std::endl;

  m_txMask = ch_mask;
}

uint32_t IblRodSerialPort::getTxMask() {
  return m_txMask;
}

uint32_t IblRodSerialPort::readsPort() {

   //MB Read Back the register values stored in the ROD Master FPGA and pass a TxMask equivalent for the entire ROD

#ifdef LL_DEBUG
  std::cout<<"SERIAL PORT: reading back the ROD Master Registers! "<<std::endl;
#endif

  uint32_t currentSerial0 = IblRod::Master::FeCmdMask0LsbReg::read();
  uint32_t currentSerial1 = IblRod::Master::FeCmdMask1LsbReg::read();
  uint32_t sPortMask=0x0;

  sPortMask=(currentSerial0 & 0xFF) + ((currentSerial1 & 0xFF)<<16);

  std::cout << __PRETTY_FUNCTION__ << "Read back the sPort Registers in the ROD Master 0x" << std::hex << sPortMask << std::dec << std::endl;

  return sPortMask;
}
/*
 * Sets the TxCh (using TxMask, which is more general)
 * Note: ch 31-16 are on slave 1 (south bridge) and ch 15-0 on slave 0 (north bridge)
 */

void IblRodSerialPort::setTxCh(uint8_t ch) {
	this->setTxMask(1<<ch);
}

void IblRodSerialPort::write(uint8_t size, uint32_t* values) {
	for(uint32_t i = 0 ; i < size ; Data::write(*values++), ++i);
}
/*
void IblRodSerialPort::write(uint8_t size, ...) {
	va_list vl; va_start(vl,size);
	std::vector<uint32_t> values(size);
	for(uint8_t i = 0 ; i < size ; ++i) {
		values[i] = va_arg(vl,uint32_t);
	}
	va_end(vl);
	write(size, &values[0]);
}
*/
void IblRodSerialPort::sendStream() {
	// Todo: get from hxx !!
	const int boEnaTxBit = 0; // Attention - EDK peripheral: bit numbers must count down from 31
        const int boNotFullBit = 17;
        (void)boNotFullBit;
        const int boNotEmptyBit = 18;

	TxCtrl::write( 1 << (31 - boEnaTxBit) );  // enable output ; data are now shifted out
	while( ( (Ctrl::read() >> (31 - boNotEmptyBit)) & 0x1 ) == 1 );
	TxCtrl::write(0); // disable output
}

void IblRodSerialPort::sendTestConfig() {
	std::cout << __PRETTY_FUNCTION__ << "\n";
	Fei4GlobalCfg::sendTestConfig(this);
}


IblRodSerialPort::IblRodSerialPort() { std::cout << __PRETTY_FUNCTION__ << std::endl; m_txMask = 0; }

