/*
 * Author: L. Kaplan <lkaplan@cern.ch>
 * Date: 2016-II-11
 * Description: Extension of IblBoxTxFifo to include 5MHz operation
 */

#include "IblBocTxFifoL12.h"
#include "IblBoc.h"

IblBocTxFifoL12::IblBocTxFifoL12() : IblBocTxFifo() {
}

IblBocTxFifoL12::~IblBocTxFifoL12() {
}

void IblBocTxFifoL12::set5MHzMode(bool on) {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
	uint8_t txCtrl = IblBoc::read( m_txCh/16, m_txCh%16, IblBoc::TxControl2 );
	if(on) txCtrl |= 0x10;
	else txCtrl &= ~(0x10);
	IblBoc::write( m_txCh/16, m_txCh%16, IblBoc::TxControl2, txCtrl );
}

void IblBocTxFifoL12::write5MHz(uint32_t data) {
#ifdef LL_DEBUG
    dumpData(data);
#endif
    for(int i = 24 ; i >= 0 ; i -= 8) {
        IblBoc::write( m_txCh/16, m_txCh%16, IblBoc::FifoControl, 0xFF&(data>>i) );
    }
}
