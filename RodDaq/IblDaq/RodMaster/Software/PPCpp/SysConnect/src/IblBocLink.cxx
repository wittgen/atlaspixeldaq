#include "IblBocLink.h"
#include "IblBoc.h"

#include "RodResourceManager.h"

#include "IblConnectivity.h"
#include "BarrelConnectivity.h"

#include <chrono>
#include <thread>

#ifdef __XMK__
#include <unistd.h>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

#ifndef VERBOSE_RW
#define VERBOSE_RW false
#endif

// WARNING: a segmentation violation is possible until setTxLink is used !!!
IblBocLink::IblBocLink(): m_TxLink(nullptr) { 
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
}

void IblBocLink::setTxLink(TxLink* txLink) {
	m_TxLink = txLink;
}

bool IblBocLink::enableTxChannel(uint8_t slaveId, uint8_t ch) {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
	IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x1);

	// mute the tx output to the real FE if using the boc FE emulator
	if (RodResourceManagerIBL::bocEmu()) IblBoc::set(slaveId, ch, IblBoc::TxControl2, 0x1);
	else IblBoc::reset(slaveId, ch, IblBoc::TxControl2, 0x1);
	
	return IblBoc::read(slaveId, ch, IblBoc::TxControl) == 0x1;
}

bool IblBocLink::enableRxChannel(uint8_t slaveId, uint8_t ch) {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif

  // MW: check if we have a Pixel RX
  if(IblBoc::IsPixelRx(slaveId)) {

    IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x0);
    IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x1);

    // TODO: check if channel is working (using MCC readback)
    // Enabling ROD output, but NOT the MON FIFO
    IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x3);
    // for now return always true
    return true;
  } else {
  	uint8_t ChannelSelectMask = RodResourceManagerIBL::bocEmu() ? 0x20 : 0x00;

#ifdef LL_DEBUG
  	std::cout << "ChannelSelectMask (" << (int)slaveId << ", " << (int)ch << ") :" << HEX(ChannelSelectMask) << std::endl;
#endif
  	// toggle enable of RxChannel to reset
  	IblBoc::write(slaveId, ch, IblBoc::RxControl, ChannelSelectMask | 0x0);
  	IblBoc::write(slaveId, ch, IblBoc::RxControl, ChannelSelectMask | 0x1);
  
  	int rxStatus = IblBoc::read(slaveId, ch, IblBoc::RxStatus);
  #ifdef LL_DEBUG
  	std::cout << "RxChannel (" << (int)slaveId << ", " << (int)ch << ") status: " << HEX(rxStatus) << std::endl;
  #endif
  	int try_count = 0;
  	int max_tries = 2;
  
  	while( (rxStatus & 0x4F) != 0x41 ) {
  		std::cout << "RxChannel (" << (int)slaveId << ", " << (int)ch << ") not ready! Status: " << HEX(rxStatus) << std::endl;
  		if(++try_count>max_tries) {
  			IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x0);
  			return false;
  		}
  		rxStatus = IblBoc::read(slaveId, ch, IblBoc::RxStatus);
  	}

    // Todo: do NOT enable MON FIFO for RX channel because it slows down BocLib tools
  	IblBoc::write(slaveId, ch, IblBoc::RxControl, ChannelSelectMask | 0x7);
  	return IblBoc::read(slaveId, ch, IblBoc::RxControl) == (ChannelSelectMask | 0x7);
  }
}

// return true if channel is enabled and in good state
// else return false
bool IblBocLink::checkRxChannel(uint8_t slaveId, uint8_t ch) {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif

  // MW: check if we have a L1/L2 RX
  if(IblBoc::IsPixelRx(slaveId)) {
    bool enabled = (IblBoc::read(slaveId, ch, IblBoc::RxControl)) & 0x1;
    return enabled;
  } else {
  	bool enabled = (IblBoc::read(slaveId, ch, IblBoc::RxControl)) & 0x1;

  	int rxStatus = IblBoc::read(slaveId, ch, IblBoc::RxStatus);
  	if (enabled && ((rxStatus & 0x4F) == 0x41)) return true;
  	else return false;
  }
}

void IblBocLink::disableChannels(uint32_t rx_mask) {
  this->disableRxChannels(rx_mask);
  uint32_t tx_mask=0;
  if(RodResMgr::isFei4())tx_mask = IblConnectivity::getTxMaskFromRxMask(rx_mask);
  else tx_mask =BarrelConnectivity::getTxMaskFromRxMask(rx_mask);
  this->disableTxChannels(tx_mask);  
}

void IblBocLink::disableRxChannels(uint32_t rx_mask) {
  for(uint8_t i = 0 ; i < 32 ; ++i)
    if( rx_mask & (1<<i) )
      this->disableRxChannel(i/16, i%16);
}

void IblBocLink::disableTxChannels(uint32_t tx_mask) {
  for(uint8_t i = 0 ; i < 32 ; ++i)
    if( tx_mask & (1<<i) )
      this->disableTxChannel(i/16, i%16);
}

void IblBocLink::disableRxChannel(uint8_t slaveId, uint8_t ch) {
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
  if(IblBoc::IsPixelRx(slaveId)) {
    IblBoc::write(slaveId, ch, IblBoc::RxControl, 0x0);
  } else {
    // MW: save channel selection bits (important when using the emulator)
    uint8_t ChannelSelectMask = IblBoc::read(slaveId, ch, IblBoc::RxControl) & 0xE0;
#ifdef LL_DEBUG
    std::cout << "ChannelSelectMask (" << (int)slaveId << ", " << (int)ch << ") :" << HEX(ChannelSelectMask) << std::endl;
#endif
    IblBoc::write(slaveId, ch, IblBoc::RxControl, ChannelSelectMask | 0x0);

#ifdef LL_DEBUG
    int rxStatus = IblBoc::read(slaveId, ch, IblBoc::RxStatus);
    std::cout << "RxChannel (" << (int)slaveId << ", " << (int)ch << ") status: " << HEX(rxStatus) << std::endl;
#endif
  }
}

void IblBocLink::disableTxChannel(uint8_t slaveId, uint8_t ch) {
#ifdef LL_DEBUG
  std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
  IblBoc::write(slaveId, ch, IblBoc::TxControl, 0x0);
}

// Counting is done w.r.t. to RX path
bool IblBocLink::enableChannels(uint32_t rx_mask) {
  // Whatever the real mappling, there will always be up to 32 modules per ROD/BOC

  uint32_t tx_mask=0;
  if(RodResMgr::isFei4())tx_mask = IblConnectivity::getTxMaskFromRxMask(rx_mask);
  else tx_mask = BarrelConnectivity::getTxMaskFromRxMask(rx_mask);

#ifdef LL_DEBUG
  std::cout << "TxMask: " << HEX(tx_mask) << std::endl;
#endif

  this->enableTxChannels(tx_mask);
    
  std::this_thread::sleep_for(std::chrono::microseconds(100));
  return this->enableRxChannels(rx_mask);
 }


bool IblBocLink::enableRxChannels(uint32_t rx_mask) {

 uint32_t rxMask = rx_mask;

  for(uint8_t i = 0 ; i < 32 ; ++i)
    if( rx_mask & (1<<i) )
      if( ! this->enableRxChannel(i/16, i%16) ) {
	std::cout << "ERROR: Couldn't enable RX "<<(int)i<<" channel as desired!" << std::endl;
	rxMask &= ~(1<<i);
      }
  // Setting m_enChMask to the channels actually enabled
  // Mask of enabled channels is availabe using getEnChMask()
  std::cout << "INFO: Input Rx channel mask: " << HEX(rx_mask) << ", successfully enabled mask: " << HEX(rxMask) << std::endl;
  setEnChMask( rxMask );
  return rxMask == rx_mask;
}

bool IblBocLink::enableTxChannels(uint32_t tx_mask) {
#ifdef __XMK__
  if (!(RodResourceManagerIBL::bocEmu()))
    xil_printf("Enabling Real FE\n");
  else
    xil_printf("Enabling BOC FE emulator");
#endif
  
  uint32_t chMask = tx_mask;
  for(uint8_t i = 0 ; i < 32 ; ++i)
    if( tx_mask & (1<<i) )
      if( ! this->enableTxChannel(i/16, i%16) ) {
	std::cerr << "ERROR: Couldn't enable TX channel " <<i<< std::endl;
	chMask &= ~(1<<i);
      }
  
  std::cout << "INFO: Input Tx channel mask: " << HEX(tx_mask) << ", successfully enabled mask: " << HEX(chMask) << std::endl;
  setTxEnChMask( chMask );
  return chMask == tx_mask;
}

bool IblBocLink::resamplePhase(uint32_t ch_mask) {
  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
    if (ch_mask & (1<<i)) {
      IblBoc::write(i/16,i%16, IblBoc::ExtraControl, 0x20);
      IblBoc::write(i/16,i%16, IblBoc::ExtraControl, 0x00);
    }
  }
  return true;
}


bool IblBocLink::ECRBusyTxChannels(uint32_t ch_mask) {
  
  for (uint8_t i =0; i<32; ++i) {
    if (ch_mask & (1<<i)) {
      if (IblBoc::ECRBusy(i/16,i%16))
	return true;
    }
  }
  return false;
}

void IblBocLink::ECRVetoTxChannels(uint32_t ch_mask,bool veto) {
  for (uint8_t i = 0; i < 32; ++i) {
    if (ch_mask & (1<<i)){
     int timeout =0;
      while (IblBoc::ECRBusy(i / 16, i % 16)){//wait for the ECR busy to be finished on this Tx channel
        std::this_thread::sleep_for(std::chrono::microseconds(100));
	    if(timeout++>10){
	      std::cout << "Timeout for ECR busy Tx: "<<(int)i<<std::endl;
	      break;
	    }
 	}
      IblBoc::ECRSetVeto(i / 16, i % 16, veto);
    }
  }
}
