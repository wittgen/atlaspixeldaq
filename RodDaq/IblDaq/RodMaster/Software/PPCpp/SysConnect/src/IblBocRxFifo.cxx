
#include "IblBocRxFifo.h"
#include "IblBoc.h"

#include <iostream>
#define HEX(x) std::hex << static_cast<int>(x) << std::dec

IblBocRxFifo::IblBocRxFifo() {
	//this->reset();
}

IblBocRxFifo::IblBocRxFifo(uint8_t rxCh) {
	setRxCh(rxCh);
}

IblBocRxFifo::~IblBocRxFifo() {
	//IblBoc::write(m_rxCh/16, m_rxCh%16, IblBoc::RxControl, 0x0);
}

//Warning: hard-coded assumption for slaveId
//Todo: make nicer, and decode data high
uint32_t IblBocRxFifo::read() {
  //std::cout << __PRETTY_FUNCTION__ << std::endl;
	//For temporary debug purposes
	//return 0xDEADBEEF;
	/*
	std::cout << "=== RxControl=" << HEX(IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::RxControl)) << std::endl;
	std::cout << "=== RxStatus=" << HEX(IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::RxStatus)) << std::endl;
	std::cout << "--- m_rxCh=" << (int)m_rxCh << std::endl;
	*/
	uint32_t value = (IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::DataHigh) & 0xFF) << 8;
	// Reading the IblBoc::DataLow will pop() from the FIFO
	value |= (IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::DataLow) & 0xFF);
	//std::cout << "+++ read value: " << HEX(value) << std::endl;
	return value;
}

// Todo: implement all useful functionality
void IblBocRxFifo::reset() {
  // Reading back RxControl register value
  uint16_t rxControl = IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::RxControl);
  IblBoc::write(m_rxCh/16, m_rxCh%16, IblBoc::RxControl, rxControl & (~0x1));

	// Draining FIFO
	while( (IblBoc::read(m_rxCh/16, m_rxCh%16, IblBoc::RxStatus) & 0x20) != 0x20 )
		this->read();
	std::cout << __PRETTY_FUNCTION__ << std::endl;

  IblBoc::write(m_rxCh/16, m_rxCh%16, IblBoc::RxControl, rxControl);
}

