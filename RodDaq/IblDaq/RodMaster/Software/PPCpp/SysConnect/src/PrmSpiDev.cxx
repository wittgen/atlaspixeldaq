/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-V-5
 * Description: This class provides SPI access to the PRM.
 * Notes: this class is a mix between custom and generic code. Todo: calrify the situation
 */

#ifdef __XMK__

#include "PrmSpiDev.h"

#include "sleep.h"

#include <iostream>

// Todo: remove hard-coded information
PrmSpiDev::PrmSpiDev(uint16_t deviceId, uint32_t wordCount): m_DeviceId(deviceId), m_WordCount(wordCount) {
	if(m_WordCount!=6) std::cout << "Setting word count to" << (m_WordCount = 6) << std::endl;
	m_spiData.reserve(m_WordCount);
}

void PrmSpiDev::init() {

	std::cout << "Initializing SPI device " << m_DeviceId << ", providing " << m_WordCount << " words of data!" << std::endl;

	m_ConfigPtr = XSpi_LookupConfig(m_DeviceId);
	if(NULL == m_ConfigPtr) {
		std::cerr << "ERROR: XST device not found" << std::endl;
		return;
	}

	m_Status = XSpi_CfgInitialize(&m_Spi, m_ConfigPtr, m_ConfigPtr->BaseAddress);
	if(XST_SUCCESS != m_Status) {
		std::cerr << "EROR: XST failure" << std::endl;
		return;
	}

	XSpi_Reset(&m_Spi);

	/*
	   As described earlier, SCK must be stable before the assertion of slave select. Therefore, when manual slave select
	   mode is utilized, the SPI master must be enabled first (SPICR bit(24) = 1) to assert SCK to the idle state prior to
	   asserting slave select.
	   Note that the master transfer inhibit (SPICR bit(23)) can be utilized to inhibit master transactions until the slave
	   select is asserted manually and all data registers of FIFOs are initialized as desired. This can be utilized before the
	   first transaction and after any transaction that is allowed to complete.
	   When the above rules are followed, the timing is the same as presented for the automatic slave select assertion
	   mode with the exception that assertion of slave select signal and the number of elements transferred is controlled by
	   the user.
	 */

	m_ControlReg = XSpi_GetControlReg(&m_Spi);
	xil_printf("SPI control register initial value: 0x%x\r\n",m_ControlReg);
	XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_MASTER_MODE_MASK | XSP_MANUAL_SSELECT_OPTION | XSP_CR_TRANS_INHIBIT_MASK);

	/*
	 * We do not need interrupts for this loopback test.
	 */
	XSpi_IntrGlobalDisable(&m_Spi);

	// reset ss
	XSpi_SetSlaveSelectReg(&m_Spi, ~0);

	// wait for tx ready
	do {
		m_StatusReg = XSpi_GetStatusReg(&m_Spi);
	} while ((m_StatusReg & XSP_SR_TX_EMPTY_MASK) == 0);

	/*
	 * Start the transfer by not inhibiting the transmitter and
	 * enabling the device.
	 */

	m_ControlReg = XSpi_GetControlReg(&m_Spi) & (~XSP_CR_TRANS_INHIBIT_MASK);
	XSpi_SetControlReg(&m_Spi, m_ControlReg | XSP_CR_ENABLE_MASK);

	m_spiData.clear();

	// set ss
	XSpi_SetSlaveSelectReg(&m_Spi, ~1);

	// write word
	XSpi_WriteReg(m_Spi.BaseAddr, XSP_DTR_OFFSET, 0 );

	// wait for rx ready
	do {
		m_StatusReg = XSpi_GetStatusReg(&m_Spi);
	} while ((m_StatusReg & XSP_SR_RX_FULL_MASK) == 0);

	// read byte
	uint32_t spiRxWord = XSpi_ReadReg(m_Spi.BaseAddr, XSP_DRR_OFFSET);

	if (0x5 != (spiRxWord >> 28)) {
		std::cerr << "ERROR: SPI read error. 0x" << std::hex << spiRxWord << std::dec << std::endl;
	} else {
		std::cout << "PRM word is 0x" << std::hex << spiRxWord << std::dec << std::endl;
	}
	m_spiData.push_back(spiRxWord);

	std::cout << "PRM git hash: " << std::hex;
	// Hash is 5 words but we're skipping the first which is the PRM word
	for (uint8_t i = 0; i < 5; i++){
		// write word
		// warning: the new spi mode at PRM will control the jtag mapping
		// if bit 3 in the last byte is set, jtag will go to the XVC mapping
		// and become unavailable for IMPACT/XMD
		// XSpi_WriteReg(m_Spi.BaseAddr, XSP_DTR_OFFSET, 0x08 );
		XSpi_WriteReg(m_Spi.BaseAddr, XSP_DTR_OFFSET, 0x00 );

		// wait for rx ready
		do {
			m_StatusReg = XSpi_GetStatusReg(&m_Spi);
		} while ((m_StatusReg & XSP_SR_RX_FULL_MASK) == 0);

		// read byte
		uint32_t spiRxWord = XSpi_ReadReg(m_Spi.BaseAddr, XSP_DRR_OFFSET);
		m_spiData.push_back(spiRxWord);

		std::cout << spiRxWord;
	}
	std::cout << std::dec << std::endl;

	// reset ss
	XSpi_SetSlaveSelectReg(&m_Spi, ~0);
}


uint8_t PrmSpiDev::getVmeSlot() {
	if(m_spiData.size() != m_WordCount) init();
	if(0x5 != (m_spiData[0]>>28)) return 0;
	return ~m_spiData[0] & 0x1F;
}

#endif
