#include "NetUtils.h"
#include <string>
#include <net/if.h>
#include <sys/ioctl.h>
#include <cstring>
#include <vector>
#include <arpa/inet.h>


//! Tokenize the given string str with given delimiter. If no delimiter is given whitespace is used.
//local function
void tokenize(const std::string& str, std::vector<std::string>& tokens, const std::string& delimiters = " ")
{
    tokens.clear();
    // Skip delimiters at beginning.
    std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    // Find first "non-delimiter".
    std::string::size_type pos     = str.find_first_of(delimiters, lastPos);

    while (std::string::npos != pos || std::string::npos != lastPos)
    {
	    // Found a token, add it to the vector.
	    tokens.push_back(str.substr(lastPos, pos - lastPos));
	    // Skip delimiters.  Note the "not_of"
	    lastPos = str.find_first_not_of(delimiters, pos);
	    // Find next "non-delimiter"
	    pos = str.find_first_of(delimiters, lastPos);
    }
}


//! Print master network configuration
void print_network_configuration()
{

    unsigned char mac[6];
    uint32_t ipaddr, netmask, gw;

    getNetInfo(mac, ipaddr, netmask, gw);

    // to simplify printout
    uint8_t* ip8 = reinterpret_cast<uint8_t*>(&ipaddr);
    uint8_t* nm8 = reinterpret_cast<uint8_t*>(&netmask);
    uint8_t* gw8 = reinterpret_cast<uint8_t*>(&gw);

    printf("Master IP: %i.%i.%i.%i\n\n", ip8[0], ip8[1], ip8[2], ip8[3]);
    printf("Master Mask: %i.%i.%i.%i\n\n", nm8[0], nm8[1], nm8[2], nm8[3]);
    printf("Master Gateway: %i.%i.%i.%i\n\n", gw8[0], gw8[1], gw8[2], gw8[3]);
    printf("Master Mac: %.2X:%.2X:%.2X:%.2X:%.2X:%.2X\n\n" , mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}


//! return 0 if interface and gateway found
bool getInterfaceName(std::string& interface, std::string& gateway)
{
	FILE *f = nullptr;
	char line[100];
	f = fopen("/proc/net/route" , "r");
	bool retval = true;

	while(fgets(line , 100 , f))
	{
		// tokenize the line
		std::vector<std::string> tokens;
		tokenize(line, tokens, " \t");

		if(tokens.size()>2) {
			// line containing the following string is the default interface
			if(tokens[1] == "00000000") {
				interface = tokens[0];
				gateway = tokens[2];
				retval = false;
				break;
			}
		}
	}

	fclose(f);
	return retval;
}


void getNetInfo(unsigned char *mac, uint32_t &ipaddr, uint32_t & netmask, uint32_t & gw)
{

    std::string interface, gateway;
    if(getInterfaceName(interface, gateway) != 0){
        printf("Could not get interface\n");
        exit(EXIT_FAILURE);
    }

    // recast string to uint32_t representation
    gw = std::stoul(gateway.c_str(), nullptr, 16);



    int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);
    if (sock == -1) { /* handle error*/ }

    // This block of code gets the 1) list of interfaces 2) finds which interfaces is not a loopback by system calls.
    // Essentially, this gets to eth0. It is an alternative to reading the /proc/net/route in getInterfaceName above
    // and portable across platforms, contrary to getInterfaceName. It was not clear how to find gateway using
    // using system call, so we stick to getInterfaName and the system call mechanism below is commente dout.
    /*
    struct ifreq ifr;
    struct ifconf ifc;

    ifc.ifc_len = sizeof(buf);
    ifc.ifc_buf = buf;
    // get list of interfaces
    if (ioctl(sock, SIOCGIFCONF, &ifc) == -1) { printf("Cannot ioctl SIOCGIFCONF\n") ; exit(EXIT_FAILURE);}

    struct ifreq* it = ifc.ifc_req;
    const struct ifreq* const end = it + (ifc.ifc_len / sizeof(struct ifreq));

    for (; it != end; ++it) {
        strcpy(ifr.ifr_name, it->ifr_name);
        if (ioctl(sock, SIOCGIFFLAGS, &ifr) == 0) {
            if (! (ifr.ifr_flags & IFF_LOOPBACK)) { // don't count loopback
                break;
            }
        }
        else {
            printf("ioctl SIOCGIFHWADDR failed %s\n"); exit(EXIT_FAILURE); }
    }
    */

    // the interface is known, define the if request
    struct ifreq ifr;
    strcpy(ifr.ifr_name, interface.c_str());


    // get mac
    if(ioctl(sock, SIOCGIFHWADDR, &ifr) ==0) {
        memcpy(mac, ifr.ifr_hwaddr.sa_data, 6);
        //printf("Mac : %.2X:%.2X:%.2X:%.2X:%.2X:%.2X\n" , mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    }

    // get ip
    if(ioctl(sock, SIOCGIFADDR, &ifr)==0){
        struct sockaddr_in *  addr = (struct sockaddr_in *)&ifr.ifr_addr;
        ipaddr = addr->sin_addr.s_addr;

        //char ip_address[INET_ADDRSTRLEN];
        //inet_ntop(AF_INET, &(addr->sin_addr), ip_address, sizeof(ip_address));
        //printf("IP: %s\n", ip_address);
    }

    // get netmas
    if(ioctl(sock, SIOCGIFNETMASK, &ifr)==0){
        struct sockaddr_in *  addr = (struct sockaddr_in *)&ifr.ifr_netmask;
        netmask = addr->sin_addr.s_addr;

        //char nm_address [INET_ADDRSTRLEN];
        //inet_ntop(AF_INET, &(addr->sin_addr), nm_address, sizeof(nm_address));
        //printf("NETMASK: %s\n", nm_address);
    }
}

