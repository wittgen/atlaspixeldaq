/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-XI-10
 * Updates:	2014-II-4 -- K. Potamianos -- expanding functionality
 * 		2014-V-15 -- K. Potamianos -- returning only values for enabled channels
 * Comments: 	2014-II-4 -- K. Potamianos -- todo: cleanup design to separate slaves
 * 		2014-V-15 -- K. Potamianos -- todo: cleanup 'catch all' channel number (32)
 */
#include "IblRodInmemFifo.h"

IblRodInmemFifo::IblRodInmemFifo() {
	m_readAllChannels = true; // Enabling 'catch all' mode by default
	// Set Control register if needed
	this->reset();
}

// WARNING: if a channel is set, ALL values read that do not match its modulo 4 will be discarded!!!
uint32_t IblRodInmemFifo::read() {
	uint32_t value(0xdeaddead);
#ifdef __XMK__
	value = localRead();
#endif
	if(m_readAllChannels) return value; // Return any value read in 'catch all' mode
	while( ! (value & 0x800) ) { // Loop until the FIFO is empty
		if( (value & 0x300) == ((uint32_t)(m_rxCh%4) << 8) ) return value; // We have a value from the channel we've set
		value = localRead();
	}
	return value;
}

void IblRodInmemFifo::reset() {
#ifdef __XMK__

	// Todo: ask for 0x41 to be put in rodMaster.hxx
  if(m_rxCh>15) IblRod::SlaveB::InmemFifoRst::write(1);
	else IblRod::SlaveA::InmemFifoRst::write(1);
	std::cout << "Done resetting INMEM FIFO" << std::endl;
#endif
}

void IblRodInmemFifo::setRxCh(uint8_t rx_ch) {
	// Gatherer selector should match the channel
	// Gatherer A(00) serves ch. 0-3, B(01) ch. 4-7, C(10) ch. 8-11, D(11) ch. 12-15
  std::cout<<"in function: "<<__PRETTY_FUNCTION__<<" setting rx ch to : "<<(int)rx_ch<<std::endl;

	m_readAllChannels = false;
	if(rx_ch == 32) { RxLink::setRxCh(rx_ch); m_readAllChannels = true; return; }

	if(rx_ch>15) {
		if( IblRod::SlaveB::InmemFifoChSel::read() != (uint32_t)(rx_ch-16)/4 ) {
			std::cerr << "ERROR: Cannot change channel because GathererSelector is locked to a different gatherer" << std::endl;
			std::cerr << "\t Please call IblRodInmemFifo::selectGatherer(uint8_t) to ajust the gatherer ID" << std::endl;
		} else RxLink::setRxCh(rx_ch);
	} else {
		if( IblRod::SlaveA::InmemFifoChSel::read() != (rx_ch/4) ) {
			std::cerr << "ERROR: Cannot change channel because GathererSelector is locked to a different gatherer" << std::endl;
			std::cerr << "\t Please call IblRodInmemFifo::selectGatherer(uint8_t) to ajust the gatherer ID" << std::endl;
		} else RxLink::setRxCh(rx_ch);
	}
}

void IblRodInmemFifo::selectGathererFromRxCh(uint8_t rx_ch) {
	uint16_t gathererId; // U16 to avoid char cout issue
	gathererId = (rx_ch/4)%4;
	// This should in principle never occur
	if(gathererId>3) std::cout << "WARNING: Ignoring all but last 2 bits of gatherer ID provided" << std::endl;
	gathererId %= 4;
	std::cout << "Setting gathererId for INMEM FIFO to " << gathererId << std::endl;
	if(m_rxCh>15) IblRod::SlaveB::InmemFifoChSel::write(gathererId);
	else IblRod::SlaveA::InmemFifoChSel::write(gathererId);
}
