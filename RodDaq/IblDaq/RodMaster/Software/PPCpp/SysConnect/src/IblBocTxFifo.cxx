/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-II-7
 * Description: TX link going though the BMFs
 */

#include "IblBocTxFifo.h"
#include "IblBoc.h"

#include <iostream>

#define HEX(x) std::hex << "0x" << (int)(x) << std::dec

#include <chrono>
#include <thread>


IblBocTxFifo::IblBocTxFifo() {

}

IblBocTxFifo::~IblBocTxFifo() {

}

void IblBocTxFifo::write(uint32_t data) {
#ifdef LL_DEBUG
  dumpData(data);
#endif
  for(int i = 24 ; i >= 0 ; i -= 8) {
		// Todo: cleanup and avoid computing /16 and %16 every time
		IblBoc::write( m_txCh/16, m_txCh%16, IblBoc::FifoData, 0xFF&(data>>i) );
}
}

void IblBocTxFifo::send() {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << std::endl;
#endif
	uint8_t txCtrl = IblBoc::read( m_txCh/16, m_txCh%16, IblBoc::TxControl );
	// Todo: use bit operators on IblBoc!
	// Setting TxFromFifo and InSel bits // Hard-coded!!! Todo: use bitfield or alternative (C++11)
	txCtrl |= 0x44; IblBoc::write( m_txCh/16, m_txCh%16, IblBoc::TxControl, txCtrl );
	//	usleep(1000);
    while (!(IblBoc::read(m_txCh/16,  m_txCh%16, IblBoc::TxStatus) & 0x1)) {
        std::this_thread::sleep_for(std::chrono::microseconds(10));
    }
	       
	// Disabling TxFromFifo and InSel bit // Hard-coded!!!
	txCtrl &= ~0x44;	IblBoc::write( m_txCh/16, m_txCh%16, IblBoc::TxControl, txCtrl );
}

void IblBocTxFifo::setTxMask(uint32_t ch_mask) {
#ifdef LL_DEBUG
	std::cout << __PRETTY_FUNCTION__ << HEX(ch_mask) << std::endl;
#endif
	// Keeping the lowest channel set
	int effMask = ch_mask;
	effMask &= -effMask;
	// Checking whether bit is 2^N, or 0
	if( ch_mask != (uint32_t) effMask )
		std::cerr << "WARNING: Mask " << HEX(ch_mask) << " enables more than one channel; using the first." << std::endl;
	m_txCh = __builtin_ctzl(effMask);
#ifdef LL_DEBUG
	std::cout<<"from mask: 0x"<<std::hex<<(int)ch_mask<<" calculated ch: "<<std::dec<<(int)m_txCh<<" in function: "<<__PRETTY_FUNCTION__<<std::dec<<std::endl;
#endif
}

void IblBocTxFifo::setTxCh(uint8_t tx_ch) {
	m_txCh = tx_ch;
}

uint32_t IblBocTxFifo::getTxMask() { 
  return (1<< m_txCh);
}
