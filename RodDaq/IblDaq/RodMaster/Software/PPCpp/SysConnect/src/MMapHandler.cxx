#include "MMapHandler.h"
#ifdef __XMK__
#include "rodMaster.hxx"
#include "rodMasterReg.hxx"

#include "rodSlaveReg.hxx"
#endif
#include <fcntl.h>

#include <cstdlib>
#include <cstdio>
#include <unistd.h>
#include <sys/mman.h>
#include <cstring>
#include <cerrno>
#include <cassert>
#include <functional>

#define FATAL do { fprintf(stderr, "MemmoryMapHandler at line %d, file %s (%d) [%s]\n", \
    __LINE__, __FILE__, errno, strerror(errno)); exit(1); } while(0)


// dummy instance. Destructor clears mmap if it exists.
MMapHandler _dummy_instance;

mmap_info MMapHandler::mmap_info_ppc;
mmap_info MMapHandler::mmap_info_boc;
mmap_info MMapHandler::mmap_info_common;
mmap_info MMapHandler::mmap_info_slaveA;
mmap_info MMapHandler::mmap_info_slaveB;
mmap_info MMapHandler::mmap_info_sp;
bool MMapHandler::inited;

void memory_map(mmap_info &minfo, const uint32_t target, const uint32_t length, int & fd)
{

    const long sz = sysconf(_SC_PAGESIZE);
    const long MAP_MASK = sz-1;

    minfo.target = target;
    minfo.pa_offset = target & ~MAP_MASK;
    minfo.tot_length =  length + target - minfo.pa_offset;
    minfo.addr = (intptr_t)mmap(0, minfo.tot_length, PROT_READ | PROT_WRITE, MAP_SHARED, fd, minfo.pa_offset);

    if(minfo.addr ==  -1) FATAL;
}


void memory_unmap(mmap_info &minfo)
{

    if(minfo.addr != -1) {
        if (munmap((void*)minfo.addr, minfo.tot_length ) == -1) FATAL;
    } else {
        printf("memory_unmap called, but addr is null");
        FATAL;
    }
}


void MMapHandler::mmap_init() {


#ifdef __XMK__

    // protection for duplicate mmap
    if(inited) {
        printf("MMapHandler already initialized\n");
        return;
    }


    int fd;
    off_t target;


    if((fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1) FATAL;
    printf("/dev/mem opened.\n");
    fflush(stdout);


    const long sz = sysconf(_SC_PAGESIZE);
    const long MAP_MASK = sz-1;

    target = PPC_DESIGN_REG;
    memory_map(mmap_info_ppc, target, sz, fd);
    printf("Memory mapped at address %p with for ppc.\n", mmap_info_ppc.addr);

    target = BOC_REG_BASE;
    memory_map(mmap_info_boc, target, 64*sz, fd);
    printf("Memory mapped at address %p with for boc.\n", mmap_info_boc.addr);

    target = COMMON_REG_BASE;
    memory_map(mmap_info_common, target, sz, fd);
    printf("Memory mapped at address %p with for common.\n", mmap_info_common.addr);

    target = SLV_A_REG_BASE;
    memory_map(mmap_info_slaveA, target, 33*sz, fd);
    printf("Memory mapped at address %p with for slaveA.\n", mmap_info_slaveA.addr);

    target = SLV_B_REG_BASE;
    memory_map(mmap_info_slaveB, target, 33*sz, fd);
    printf("Memory mapped at address %p with for slaveB.\n", mmap_info_slaveB.addr);

#ifdef __XMK__
    target =  XPAR_MCBSP_ROD_0_BASEADDR;
#else
    target =  XPAR_MCBSP_ROD_0_BASEADDR_1
#endif
    memory_map(mmap_info_sp, target, sz, fd);
    printf("Memory mapped at address %p for sp.\n", mmap_info_sp.addr);

    inited = true;

#endif


}

#ifdef __XMK__
// mapping function (see man mmap for details)
inline static uint32_t mmap_function(uint32_t reg, const mmap_info & minfo)
{
    assert(minfo.addr != -1);  // TODO remove for production?
    return minfo.addr + reg - minfo.pa_offset;
}
#endif

uint32_t MMapHandler::mmap_ppc(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_ppc);
    return fce(reg);
#else
    return reg;
#endif
}


uint32_t MMapHandler::mmap_boc(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_boc);
    return fce(reg);
#else
    return reg;
#endif
}


uint32_t MMapHandler::mmap_common(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_common);
    return fce(reg);
#else
    return reg;
#endif
}


uint32_t MMapHandler::mmap_slaveA(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_slaveA);
    return fce(reg);
#else
    return reg;
#endif
}




uint32_t MMapHandler::mmap_slaveB(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_slaveB);
    return fce(reg);
#else
    return reg;
#endif
}



uint32_t MMapHandler::mmap_sp(uint32_t reg)
{
#ifdef __XMK__
    using namespace std::placeholders;
    auto fce = std::bind(mmap_function, _1, mmap_info_sp);
    return fce(reg);
#else
    return reg;
#endif
}


void MMapHandler::mmap_clear()
{
    if(!inited)  return;

    printf("MMapHandler: clearing nmap\n");

    memory_unmap(mmap_info_ppc);
    memory_unmap(mmap_info_boc);
    memory_unmap(mmap_info_common);
    memory_unmap(mmap_info_slaveA);
    memory_unmap(mmap_info_slaveB);
    memory_unmap(mmap_info_sp);
}
