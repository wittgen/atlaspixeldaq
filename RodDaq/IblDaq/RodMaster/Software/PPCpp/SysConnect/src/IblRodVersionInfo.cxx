#ifdef __PPC__

#include <cstdlib> // strtoul
#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <stdint.h> // uint typedef

#include "HwRegsInCpp.h"
#include "RodMasterRegisters.h"

#include "IblRodVersionInfo.h"
#include "IblRodSlave.h"

#include "RodResourceManager.h"
#include "VersionInfo.h"


void IblRodVersionInfo::printHashes() {
    printHash(getSlaveHashSw(0), "Slave 0 SW hash");
    printHash(getSlaveHashFw(0), "Slave 0 FW hash");
    printHash(getSlaveHashSw(1), "Slave 1 SW hash");
    printHash(getSlaveHashFw(1), "Slave 1 FW hash");
    printHash(getMasterHashSw(), "Master SW hash");
    printHash(getMasterHashFw(), "Master FW hash");
}

void IblRodVersionInfo::printHash(GitHash_t const& hash, std::string const& desc) {
    std::ios::fmtflags f(std::cout.flags());
    std::cout << desc << ": " << std::hex << std::setw(8) << std::setfill('0');
    for (int i = 0; i < c_hashWords; i++) {
        std::cout << hash.word[i];
    }
    std::cout << std::dec << std::endl;
    std::cout.flags(f);
}

bool IblRodVersionInfo::checkConsistency() {
    std::vector<GitHash_t> hashes;
    
    hashes.push_back(getSlaveHashSw(0));
    hashes.push_back(getSlaveHashFw(0));
    hashes.push_back(getSlaveHashSw(1));
    hashes.push_back(getSlaveHashFw(1));
    hashes.push_back(getMasterHashFw());

    GitHash_t masterSw = getMasterHashSw();
    
    for (int i = 0; i < c_hashWords; i++) {
	if (!isHashIdentical(hashes[i], masterSw)) {
	    return false;
	}
    }
    return true;
}

IblRodVersionInfo::GitHash_t IblRodVersionInfo::getSlaveHashSw(int slaveId) {
    IblRodSlave slv;
    GitHash_t gitHash;

    if(!(RodResMgr::slaveIdle(slaveId) || RodResMgr::slaveAlive(slaveId))) {
      std::cout << "Cannot get slave SW hash because the slave is not idle or even alive." << std::endl;
      return gitHash;
    }

    IblSlvHashes hashes = slv.getHashes(slaveId);
    for (int i = 0; i < c_hashWords; i++) {
	gitHash.word[i] = hashes.softwareHash[i]; 
    }
    return gitHash;
}

IblRodVersionInfo::GitHash_t IblRodVersionInfo::getSlaveHashFw(int slaveId) {
    IblRodSlave slv;
    GitHash_t gitHash;
    IblSlvHashes hashes = slv.getHashes(slaveId);
    for (int i = 0; i < c_hashWords; i++) {
	gitHash.word[i] = hashes.firmwareHash[i]; 
    }
    return gitHash;
}

IblRodVersionInfo::GitHash_t IblRodVersionInfo::getMasterHashSw() {
    GitHash_t gitHash;
    auto master_sw_string = version_git_hash_string();
    gitHash.word[0] = strtoul(master_sw_string.substr(0,8).c_str(), nullptr, 16);
    gitHash.word[1] = strtoul(master_sw_string.substr(8,8).c_str(), nullptr, 16);
    gitHash.word[2] = strtoul(master_sw_string.substr(16,8).c_str(), nullptr, 16);
    gitHash.word[3] = strtoul(master_sw_string.substr(24,8).c_str(), nullptr, 16);
    gitHash.word[4] = strtoul(master_sw_string.substr(32,8).c_str(), nullptr, 16);
/*
    gitHash.word[0] = strtoul(TOSTRING(GIT_HASH_1), NULL, 16);
    gitHash.word[1] = strtoul(TOSTRING(GIT_HASH_2), NULL, 16);
    gitHash.word[2] = strtoul(TOSTRING(GIT_HASH_3), NULL, 16);
    gitHash.word[3] = strtoul(TOSTRING(GIT_HASH_4), NULL, 16);
    gitHash.word[4] = strtoul(TOSTRING(GIT_HASH_5), NULL, 16);
*/
    return gitHash;
}

IblRodVersionInfo::GitHash_t IblRodVersionInfo::getMasterHashFw() {
    GitHash_t hash;

    /* First write to register to reset, then read out the hash byte-wise. */
    IblRod::GitIdReg::write(0);

    for (int i = 0; i < c_hashWords; i++) {
        uint32_t partialHash = 0;
        for (int j = 0; j < 4; j++) {
            partialHash <<= 8;
	    partialHash |= IblRod::GitIdReg::read();
        }
        hash.word[i] = partialHash;
    }
    return hash;
}

bool IblRodVersionInfo::isHashIdentical(GitHash_t const& a, GitHash_t const& b) {
    for (int i = 0; i < c_hashWords; i++) {
	if (a.word[i] != b.word[i]) {
	    return false;
	}
    }
    return true;
}   

#endif // PPC

