// This piece of code needs significant cleanup and consistency
// Use a common position of the slaveId parameter (ideally the first)
// All functions should be static (to be changed in the header)

#include "RodResourceManager.h"

#include "IblRodSlave.h"
#include "NetUtils.h"
#include <chrono>
#include <thread>
#include <arpa/inet.h>
#include <cstring>

#define ROW_BITS 9
#define COL_BITS 7
#define ROW_SHIFT 0
#define COL_SHIFT (ROW_BITS + ROW_SHIFT)
#define CHIP_SHIFT (COL_BITS + COL_SHIFT)
#define TOT_SHIFT (CHIP_BITS + CHIP_SHIFT)

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// #define DEBUG_ND

bool IblRodSlave::setId(unsigned id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_ID_SET;
  slvCmd.addr = 0;
  slvCmd.count = 1;
  slvCmd.data = id;

  return(sendCommand(id, &(slvCmd.cmd), nWords));
}

bool IblRodSlave::setVerbose(unsigned id, unsigned on){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_VERBOSE;
  slvCmd.addr = 0;
  slvCmd.count = 1;
  slvCmd.data = on;

  return(sendCommand(id, &(slvCmd.cmd), nWords));
}

void IblRodSlave::resetNetwork(uint8_t id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_CLEAR_CB;
  slvCmd.addr = 0;
  slvCmd.count = 1;

  sendCommand(id, &(slvCmd.cmd), nWords);
}

void IblRodSlave::resetHisto(uint8_t id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_ABORT;
  slvCmd.addr = 0;
  slvCmd.count = 1;

  sendCommand(id, &(slvCmd.cmd), nWords);
}

//val = 0 MASTER, 1 SLAVE0, 2 SLAVE1
void IblRodSlave::setUart(unsigned val){
  switch(val){
  case 0:
    IblRod::EpcLocalCtrlUartA::write(0);
    IblRod::EpcLocalCtrlUartB::write(0);
    break;
  case 1:
    IblRod::EpcLocalCtrlUartA::write(1);
    IblRod::EpcLocalCtrlUartB::write(0);
    break;
  case 2:
    IblRod::EpcLocalCtrlUartA::write(0);
    IblRod::EpcLocalCtrlUartB::write(1);
    break;
  }
}

IblRodSlave::~IblRodSlave(){}

IblRodSlave::IblRodSlave(){}

bool IblRodSlave::getHisto(unsigned id, unsigned dc, uint32_t* buffer){
  unsigned nWords = sizeof(IblSlvHistCfg)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_READ;
  slvCmd.addr = 0;
  slvCmd.count = 6*80;
  slvCmd.data = 0;//could be 1 for south slave
  return(sendCommand( id, &(slvCmd.cmd), nWords, buffer, slvCmd.count));
}

bool IblRodSlave::getOccHisto(unsigned id, uint32_t* buffer, uint32_t relAdd, uint16_t count, unsigned histogrammer) {
  unsigned nWords = sizeof(IblSlvHistCfg)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_READ;
  slvCmd.addr = relAdd;
  if (count >= 512) {
    std::cout << "Warning: The selected buffer size is too big and set to 6*80 instead." << std::endl;
    slvCmd.count = 6*80;
  }
  else {
    slvCmd.count = count;
  }
  slvCmd.data = histogrammer; // 0 or 1 for histogrammer unit?
  
  return sendCommand(id, &(slvCmd.cmd), nWords, buffer, slvCmd.count);
}

bool IblRodSlave::testInj(unsigned id, uint32_t addr, uint32_t data) {
  unsigned nWords = sizeof(IblSlvHistCfg)>>2;
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_TEST;
  slvCmd.addr = addr; // need to be 0 or 1
  slvCmd.count = 1;
  slvCmd.data = data; // will then loops over data[0] ... data[count-1/]

  std::vector<uint32_t> buffer(slvCmd.count);
  return sendCommand(id, &(slvCmd.cmd), nWords, buffer.data(), slvCmd.count);
}

bool IblRodSlave::injectHitInHistoUnit(unsigned id, uint32_t addr, unsigned row, unsigned col, unsigned tot, unsigned chip) {
  uint32_t data = 0;

  data |= (row << ROW_SHIFT);
  data |= (col << COL_SHIFT);
  data |= (tot << TOT_SHIFT);
  data |= (chip << CHIP_SHIFT);
  return testInj(id, addr, data);
}


//type: SLV_STAT_TYPE_STD   0x01    // default status
//      SLV_STAT_TYPE_NET   0x02    // network status
//      SLV_STAT_TYPE_FIT   0x03    // status of connection to fit server
//      SLV_STAT_TYPE_HIST  0x04    // hisotgrammer status
iblSlvStat IblRodSlave::getStatus(unsigned type, unsigned id){
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_STAT;
  slvCmd.addr = 0;
  slvCmd.count = 1;
  slvCmd.data = type;
  
  iblSlvStat statIn;
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  unsigned nWordsStat = sizeof(iblSlvStat)>>2;

  sendCommand( id, &(slvCmd.cmd), nWords, (uint32_t*)&statIn, nWordsStat);
  return(statIn);
}

void IblRodSlave::write(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id){
  //std::cout << __PRETTY_FUNCTION__ << ": bufferLength=" << bufferLength << std::endl;
  for(int k = 0; k < bufferLength; k++){
	if(bufferLength>1023 && (k%1024*10)==0) std::cout << __PRETTY_FUNCTION__ << " loop: k=" << k << std::endl;
    id ? IblRod::SlaveB::Ram::write(k+addr, buffer[k]) : IblRod::SlaveA::Ram::write(k+addr, buffer[k]);
  }
}

void IblRodSlave::write(uint32_t addr, uint32_t value, unsigned id){
    id ? IblRod::SlaveB::Ram::write(addr, value) : IblRod::SlaveA::Ram::write(addr, value);
}

void IblRodSlave::checkRead(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id){
  uint32_t cmd;
  
  read(SLV_CMD_LOCATION, (uint32_t*)&cmd, sizeof(cmd)>>2, id);
  for(int i = 0 ; i < 5 ; i++){
    if(SLV_CMD_ACK != cmd){
      std::cout << "No ACK in " << i << " seconds with CMD: " << cmd << std::endl;
      read(SLV_CMD_LOCATION, (uint32_t*)&cmd, sizeof(cmd)>>2, id);
    }
  }
  read(addr, buffer, bufferLength, id);
}

void IblRodSlave::read(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id){
  for(int k = 0; k < bufferLength; k++){
    id ? buffer[k] = IblRod::SlaveB::Ram::read(k+addr) : buffer[k] = IblRod::SlaveA::Ram::read(k+addr);
  }
}

bool IblRodSlave::sendCommand(unsigned id, uint32_t *buffer, unsigned nWords, uint32_t *returnBuffer, unsigned nReturnWords){
 if(nWords>1) write((SLV_CMD_LOCATION + sizeof(uint32_t))>>2, &(buffer[1]), nWords - 1, id);
 write(SLV_CMD_LOCATION, &(buffer[0]), 1, id);

 uint32_t cmdAck = SLV_CMD_ALIVE;

 // to-do: make sure this is large enough
 int maxTries = 500000;
 if( RodResMgr::isFei3() )
   maxTries = 50000;

 int tries = 0;
 bool success = true;
 do {
    read(SLV_CMD_LOCATION, &cmdAck, 1, id);
    // to-do: optimize timing here
      std::this_thread::sleep_for(std::chrono::microseconds(100));

     tries++;
        if (tries > maxTries) {
        success = false;
        break;
      }
    //    std::cout<<"cmdAck = 0x"<<std::hex<<cmdAck<<std::endl;
 } while(cmdAck != SLV_CMD_ACK && cmdAck != SLV_CMD_IDLE); // The default bootloader acknowlegdes commands with SLV_CMD_IDLE.

 if (!success) std::cout<<" failed to get acknowledge after " << (int) maxTries/1E4 << " secs from slave "<<(int)id<<", cmdAck = "<<(int)cmdAck<<std::endl;
 else write(SLV_CMD_LOCATION, SLV_CMD_IDLE, id);

  if(nReturnWords && returnBuffer){

    // 3 is the difference IblSlvRdWr::cmd and IblSlvRdWr::data
    read(3, returnBuffer, nReturnWords, id);
  }
  return success;
}

bool IblRodSlave::setNetCfg(IblSlvNetCfg *cfg, unsigned id){
  unsigned nWords = sizeof(IblSlvNetCfg)>>2;
  cfg->cmd = SLV_CMD_NET_CFG_SET;
  if (sendCommand( id, (uint32_t*)(cfg), nWords)) {
    // Check if connection succeeds, add time-out as protection
    int maxTries = 100;
    int tries = 0;
    bool success = true;
    uint32_t statusType = SLV_STAT_TYPE_NET;
    // Requesting a different status when connecting to a fit server
    if( cfg->targetIpA || cfg->targetIpB ) statusType = SLV_STAT_TYPE_FIT;
    while(getStatus(statusType, id).status != SLV_STAT_BUSY){
      if( cfg->targetIpA || cfg->targetIpB ) std::this_thread::sleep_for(std::chrono::milliseconds(10));
      else std::this_thread::sleep_for(std::chrono::milliseconds(50));
 // Waiting longer for initial network setup
      tries++;
      if (tries > maxTries) {
	success = false;
	break;
      }
    }
    return success;
  }
  return false;
}

bool IblRodSlave::startHisto(IblSlvHistCfg *cfg, unsigned id){
  
  unsigned nWords = sizeof(IblSlvHistCfg)>>2;
  cfg->cmd = SLV_CMD_HIST_CFG_SET;
  return(sendCommand( id, (uint32_t*)(cfg), nWords));
}

IblSlvHistCfg IblRodSlave::getHistCfg(unsigned id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  unsigned nWordsHist = sizeof(IblSlvHistCfg)>>2;
  IblSlvRdWr slvCmd;
  IblSlvHistCfg cfg;
  slvCmd.cmd = SLV_CMD_HIST_CFG_GET;
  sendCommand( id, &(slvCmd.cmd), nWords, (uint32_t*)&cfg, nWordsHist);
  return(cfg);
}

IblSlvHashes IblRodSlave::getHashes(unsigned id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  unsigned nWordsHashes = sizeof(IblSlvHashes)>>2;

  IblSlvRdWr slvCmd;
  IblSlvHashes hashes;

  slvCmd.cmd = SLV_CMD_GET_HASHES;
  sendCommand( id, &(slvCmd.cmd), nWords, (uint32_t*)&hashes, nWordsHashes);
  return(hashes);
}

IblSlvNetCfg IblRodSlave::getNetCfg(unsigned id){
  unsigned nWords = sizeof(IblSlvRdWr)>>2;
  unsigned nWordsNet = sizeof(IblSlvNetCfg)>>2;

  IblSlvRdWr slvCmd;
  IblSlvNetCfg cfg;

  slvCmd.cmd = SLV_CMD_NET_CFG_GET;
  sendCommand( id, &(slvCmd.cmd), nWords, (uint32_t*)&cfg, nWordsNet);
  return(cfg);
}

bool IblRodSlave::stopHisto(unsigned id){
  IblSlvHistTerm slvCmd;
  unsigned nWords = sizeof(IblSlvHistTerm)>>2;
  slvCmd.cmd = SLV_CMD_HIST_CMPLT;
  return(sendCommand( id, &(slvCmd.cmd), nWords));
}

void IblRodSlave::testHisto(){
  std::cout << "yeah!" << std::endl;
  //int ntrigger=1;
  int slave = 0;

#if 0
  IblSlvNetCfg netCfgOut;
  unsigned char mac[8] = {0,0, 0x00, 0x0a, 0x35, 0x00, 0x01, 0x03};
  unsigned char lIp[4] = {192, 168, 1, 151};
  unsigned char maskIp[4] = {255,255, 0,0};
  unsigned char gwIp[4] = {192, 168, 0, 1};
  unsigned char tIpA[4] = {192, 168, 1, 100};
  unsigned char tIpB[4] = {192, 168, 1, 101};
  uint32_t tPA = 5002;
  uint32_t tPB = 5003;
  memcpy(&netCfgOut.localMac,mac,sizeof(mac));
  memcpy(&netCfgOut.localIp,lIp,sizeof(lIp));
  memcpy(&netCfgOut.maskIp,maskIp,sizeof(maskIp));
  memcpy(&netCfgOut.gwIp,gwIp,sizeof(gwIp));
  memcpy(&netCfgOut.targetIpA,tIpA,sizeof(tIpA));
  netCfgOut.targetPortA = tPA;
#endif
  IblSlvHistCfg histCfgIn;
  /*IblSlvHistCfg histCfgOut;
  histCfgOut.cfg[0].nChips = 8; // Fei4 only for now
  histCfgOut.cfg[0].enable = 1;     //enable histo0 unit
  histCfgOut.cfg[0].type = SHORT_TOT;
  histCfgOut.cfg[0].maskStep = 0;   //0 for single chip(or 1 if divide row by 2)
  histCfgOut.cfg[0].chipSel = 0;    //0 for single chip(1 for all chips)
  histCfgOut.cfg[0].addrRange = 0;  //single chip <32K( full range < 1M)
  histCfgOut.cfg[0].scanId = 0x1234;//remote use only
  histCfgOut.cfg[0].binId = 1;
  histCfgOut.cfg[0].expectedOccValue = ntrigger;
*/
  setVerbose(slave,1);
  setUart(slave+1);
  setId(slave);
  std::cout << "----startHisto" << std::endl;
  
  /////////////////////////////////////////////////
  histCfgIn = getHistCfg(slave);
  //////////////////////////////////////////////////
  std::cout << "----gethistcfg" << std::endl;

  for(unsigned i = 0; i < 2; i++){
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].nChips--" << HEX(histCfgIn.cfg[i].nChips) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].enable--" << HEX(histCfgIn.cfg[i].enable) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].type--" << HEX(histCfgIn.cfg[i].type) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].maskStep--" << HEX(histCfgIn.cfg[i].maskStep) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].chipSel--" << HEX(histCfgIn.cfg[i].chipSel) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].addrRange--" << HEX(histCfgIn.cfg[i].addrRange) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].scanId--" << HEX(histCfgIn.cfg[i].scanId) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].binId--" << HEX(histCfgIn.cfg[i].binId) << std::endl;
    std::cout << "--testHisto histCfgIn.cfg["<<i<<"].expectedOccValue--" << HEX(histCfgIn.cfg[i].expectedOccValue) << std::endl;
  }

//   IblSlvHistTerm histTermOut;
//   //unsigned char tIp[4] = {192,168,1,200};
//   histTermOut.targetPort = tP;
//   histTermOut.pcol = 1;
//   memcpy(&histTermOut.targetIp,tIp,sizeof(lIp));
//   //////////////////////////////////////////////
//   sleep(1000);
//   stopHisto(&histTermOut, slave);
//   //////////////////////////////////////////////
//   std::cout << "----stop histo" << std::endl;

  /////////////////
  uint32_t histoOutput[6*80] = {};

  std::this_thread::sleep_for(std::chrono::seconds(1));

  getHisto(slave, 0, histoOutput);

  for(int i = 0; i < 6*80; i++){
    std::cout << "--getHisto-data--" << i << "--- " << histoOutput[i] << std::endl;
  }
  std::cout << "----get histo" << std::endl;
  ////////

  
  ///////////////////
  iblSlvStat stat;
  std::this_thread::sleep_for(std::chrono::seconds(1));
  stat = getStatus(SLV_STAT_TYPE_STD, slave);
  std::cout << "----getStatus STD" << std::endl;
  std::cout << "--statSTD.progType--" << HEX(stat.progType) << std::endl;
  std::cout << "--statSTD.progVer--" << HEX(stat.progVer) << std::endl;
  std::cout << "--statSTD.status--" << HEX(stat.status) << std::endl;

  std::this_thread::sleep_for(std::chrono::seconds(1));
  stat = getStatus(SLV_STAT_TYPE_FIT, slave);
  std::cout << "----getStatus NET" << std::endl;
  std::cout << "--statNET.progType--" << HEX(stat.progType) << std::endl;
  std::cout << "--statNET.progVer--" << HEX(stat.progVer) << std::endl;
  std::cout << "--statNET.status--" << HEX(stat.status) << std::endl;
  
  std::this_thread::sleep_for(std::chrono::seconds(1));
  stat = getStatus(SLV_STAT_TYPE_HIST, slave);
  std::cout << "----getStatus HIST" << std::endl;
  std::cout << "--statHIST.progType--" << HEX(stat.progType) << std::endl;
  std::cout << "--statHIST.progVer--" << HEX(stat.progVer) << std::endl;
  std::cout << "--statHIST.status--" << HEX(stat.status) << std::endl;
  //////////////////////////
}

void IblRodSlave::enableRxLink(uint8_t slave, uint16_t chMask) {
  slave ? IblRod::SlaveB::FmtLinkEnable::write(chMask) : IblRod::SlaveA::FmtLinkEnable::write(chMask);
}

void IblRodSlave::enableRxLink(uint32_t chMask) {

  IblRod::SlaveA::FmtLinkEnable::write(chMask&0x0000FFFF);
  IblRod::SlaveB::FmtLinkEnable::write((chMask&0xFFFF0000)>>16);
#if LL_DEBUG
    std::cout<<"after enabling Rx links, FmtLinkEnable = 0x"<<std::hex<<IblRod::SlaveA::FmtLinkEnable::read()<<std::dec<<std::endl;
    std::cout<<"at address: 0x"<<std::hex<<(int)IblRod::SlaveA::FmtLinkEnable::addr()<<std::dec<<std::endl;
    std::cout<<"after enabling Rx links, FmtLinkeEnable = 0x"<<std::hex<<IblRod::SlaveB::FmtLinkEnable::read()<<std::dec<<std::endl;
    std::cout<<"at address: 0x"<<std::hex<<(int)IblRod::SlaveB::FmtLinkEnable::addr()<<std::dec<<std::endl;
#endif
}




// The purpose of this command is to provide the IblSlvNetCfg structure that allows the slave to set its IP address.
// This structure does NOT yet contain any information to allow the salve to connect to the FitFarm !
// The FitFarm IP and port has to be added by the user

void IblRodSlave::getDefaultNetConfig(uint8_t slaveId, IblSlvNetCfg &netCfgOut) {


    unsigned char mac_ethernet_address[6];
    uint32_t ipaddr, netmask, gw;
    getNetInfo(mac_ethernet_address, ipaddr, netmask, gw); // OKUPDATE rework - getDefaultNetConfig called twice and so this too


#ifdef __XMK__
  // Network parameters for RodSlave
  unsigned char mac[8] = {0, 0, 0x00, 0x0a, 0x35, 0x42, 0x52, 0x00};
  for(int i = 0 ; i < 4 ; ++i) mac[i+2] = mac_ethernet_address[i];
#if defined(PIT)
  mac[6] = mac_ethernet_address[4];
  mac[7] = mac_ethernet_address[5] + (slaveId + 1)*0x30;
#else
  mac[6] = mac_ethernet_address[4] + slaveId + 1;
  mac[7] = mac_ethernet_address[5];
#endif

  uint8_t ip4 = ipaddr&0xFF;

#if defined(PIT)
  uint32_t ibl_rod_slave_subnet_class_b = 145;
  uint32_t ibl_rod_slave_subnet = 89; // IBL crate

  if( RodResMgr::isFei3() ) {
    unsigned char ibl_rod_net_id = (ipaddr&0xFF00)>>8;
    ibl_rod_slave_subnet_class_b = 153;
    ibl_rod_slave_subnet = 38; // L1 crate
    // Updating with the data from the ROD IP
    // For L1 and L2: keep as before
    ibl_rod_slave_subnet = ibl_rod_net_id - 59;
    // Overwrite for Bx and Dx crates
    if( ibl_rod_net_id == 104 || ibl_rod_net_id == 105 )
      ibl_rod_slave_subnet = ibl_rod_net_id - 61;
  }
  if(RodResMgr::isFei4()) ip4 +=30;

  std::cout << "ip address " << ipaddr << " " << netmask <<  std::endl;
  uint32_t lIp[4] = { (ipaddr&0xFF000000)>>24, ibl_rod_slave_subnet_class_b, ibl_rod_slave_subnet&0xFF, ip4 - (1-(uint32_t)slaveId%2)*100};
  uint32_t maskIp[4] = {(netmask&0xFF000000)>>24, (netmask&0xFF0000)>>16, (netmask&0xFF00)>>8, (netmask&0xFF)};
  uint32_t gwIp[4] = {(gw&0xFF000000)>>24, ibl_rod_slave_subnet_class_b, (ibl_rod_slave_subnet&0xFF), gw&0xFF};
#else
  uint32_t lIp[4] = {(ipaddr&0xFF000000)>>24, (ipaddr&0xFF0000)>>16, (ipaddr&0xFF00)>>8, (ipaddr&0xFF) + slaveId+1};
  uint32_t maskIp[4] = {(netmask&0xFF000000)>>24, (netmask&0xFF0000)>>16, (netmask&0xFF00)>>8, netmask&0xFF};
  uint32_t gwIp[4] = {(gw&0xFF000000)>>24, (gw&0xFF0000)>>16, (gw&0xFF00)>>8, gw&0xFF};
#endif

  std::cout << "Setting slave network configuration for slave " << (int)slaveId << std::endl;
  char buffer[100];
  snprintf(buffer, 100, "MAC: %02x-%02x-%02x-%02x-%02x-%02x\n", mac[2], mac[3], mac[4], mac[5], mac[6], mac[7]); std::cout << buffer << std::endl;
  snprintf(buffer, 100, "IP: %d.%d.%d.%d\n", lIp[0], lIp[1], lIp[2], lIp[3]); std::cout << buffer << std::endl;
  snprintf(buffer, 100, "Mask: %d.%d.%d.%d\n", maskIp[0], maskIp[1], maskIp[2], maskIp[3]); std::cout << buffer << std::endl;
  snprintf(buffer, 100, "Gateway: %d.%d.%d.%d\n", gwIp[0], gwIp[1], gwIp[2], gwIp[3]); std::cout << buffer << std::endl;

  IblRodSlave::createMacAddress(mac[2], mac[3], mac[4], mac[5], mac[6], mac[7], netCfgOut.localMac);
  IblRodSlave::createIp4Address(lIp[0], lIp[1], lIp[2], lIp[3], &netCfgOut.localIp);
  IblRodSlave::createIp4Address(maskIp[0], maskIp[1], maskIp[2], maskIp[3], &netCfgOut.maskIp);
  IblRodSlave::createIp4Address(gwIp[0], gwIp[1], gwIp[2], gwIp[3], &netCfgOut.gwIp);

 // Seeting FitFarm IP to 0.0.0.0 ; slave won't attempt to connect
  uint32_t tmpFitIp = { 0 };
  tmpFitIp = htonl(tmpFitIp);
  uint32_t *ptr = & tmpFitIp;
  memcpy(&netCfgOut.targetIpA,ptr,sizeof(tmpFitIp));
  memcpy(&netCfgOut.targetIpB,ptr,sizeof(tmpFitIp));
#endif


}

void IblRodSlave::setDefaultNetConfig(uint8_t slaveId) {
#ifdef __XMK__
  IblSlvNetCfg netCfgOut;
  IblRodSlave::getDefaultNetConfig(slaveId, netCfgOut);
  IblRodSlave s; // There's no need for an instance if things are done properly
  s.setNetCfg(&netCfgOut, slaveId);
#endif
}

/* Creates an IP4 address as expected by the slave 
 * (MSB corresponds to first address octet). */
void IblRodSlave::createIp4Address(
  uint32_t ip1,
  uint32_t ip2,
  uint32_t ip3,
  uint32_t ip4,
  uint32_t* destination){

  uint32_t ip;
  ip = ip1 << 24 |
       ip2 << 16 |
       ip3 << 8  |
       ip4;

  *destination = ip;
}

/* Creates MAC address as expected by the slave. */
void IblRodSlave::createMacAddress(
  unsigned char mac1,
  unsigned char mac2,
  unsigned char mac3,
  unsigned char mac4,
  unsigned char mac5,
  unsigned char mac6,
  uint32_t* destination){

  destination[0] = mac1 << 8 | 
                   mac2;
  destination[1] = mac3 << 24 |
                   mac4 << 16 |
                   mac5 << 8 |
                   mac6;
}

// code inspired by primitive functionality in DSP code:
// RodDaq/IblDaq/RodMaster/Software/System/iblSlave.c
// RodDaq/IblDaq/RodMaster/Software/Common/PIX/iblPrimFxns.c
//
void IblRodSlave::initSlave(uint8_t id){ 
#ifdef __XMK__ 
  id ? IblRod::SlaveB::CtlReg::write(1 << SLV_CTL_RESET_BIT) : IblRod::SlaveA::CtlReg::write(1 << SLV_CTL_RESET_BIT); 
  // write boot command while reset asserted
  uint32_t cmd = SLV_CMD_BOOT;
  write(SLV_CMD_LOCATION, &cmd, sizeof(cmd), id);
  // release reset
  id ? IblRod::SlaveB::CtlReg::write(0) : IblRod::SlaveA::CtlReg::write(0); 
  waitSlaveBoot(id);
#endif
}
void IblRodSlave::waitSlaveBoot(uint8_t id){ 
#ifdef __XMK__ 
  uint32_t cmd = SLV_CMD_BOOT;
  while (SLV_CMD_ALIVE != cmd){
    read(SLV_CMD_LOCATION, &cmd, sizeof(cmd), id);
  }
  // clear command location
  cmd = SLV_CMD_IDLE;
  write(SLV_CMD_LOCATION, &cmd, sizeof(cmd), id);
#endif
}
void IblRodSlave::startSlave(uint8_t id){ 
#ifdef __XMK__ 
  IblSlvRdWr slvCmd;
  slvCmd.cmd = SLV_CMD_START;
  slvCmd.addr = 0;
  slvCmd.count = 0;
  slvCmd.data = 0xB007B007;
  sendCommand(id, &(slvCmd.cmd), 4);
#endif
}
void IblRodSlave::loadSlave(uint8_t id, uint32_t *data, uint32_t blkSize){ 
  std::cout << __PRETTY_FUNCTION__ << ": id=" << (int)id << ", data=" << (void*)data << ", blkSize=" << blkSize << std::endl;
#ifdef LL_DEBUG
  char strStr[100];
#endif
  for(uint32_t i = 0  ; i < blkSize ; i += 4) {
#ifdef LL_DEBUG
    snprintf(strStr, sizeof(strStr), "%07lx %04lx %04lx %04lx %04lx %04lx %04lx %04lx %04lx\n", i*4,
	(data[i]&0xFFFF0000)>>16, data[i]&0xFFFF,
	(data[i+1]&0xFFFF0000)>>16, data[i+1]&0xFFFF,
	(data[i+2]&0xFFFF0000)>>16, data[i+2]&0xFFFF,
	(data[i+3]&0xFFFF0000)>>16, data[i+3]&0xFFFF);
	//std::cout << strStr;
	//std::cout << std::hex << data[i] << "\t" << data[i+1] << "\t" << data[i+2] << "\t" << data[i+3] << std::dec << std::endl;
#endif
	for(int j = 0 ; j < 4 ; ++j) {
		uint8_t *ptr = (uint8_t*)&data[i+j];
		uint8_t swp;
		swp = ptr[0]; ptr[0] = ptr[3]; ptr[3] = swp;
		swp = ptr[1]; ptr[1] = ptr[2]; ptr[2] = swp;
	}
	//std::cout << std::hex << data[i] << "\t" << data[i+1] << "\t" << data[i+2] << "\t" << data[i+3] << std::dec << std::endl;

  }
#ifdef __XMK__ 

  const uint32_t maxBlkSize = 253; // Remember: DPR is 2kB
  uint32_t cmdBuffer[maxBlkSize+3];
  uint32_t offset = 0;
  while( offset < blkSize ) {
/*
    std::cout << "Data: " << "\t";
    for(int i = 0 ; i < 4 ; ++i) std::cout << HEX( data[offset+i] ) << "\t";
    std::cout << std::endl;
*/
    uint32_t bSize =  blkSize - offset  > maxBlkSize ? maxBlkSize : blkSize - offset;
    uint32_t *cmdB = cmdBuffer;
    IblSlvRdWr* slvCmd = reinterpret_cast<IblSlvRdWr*>(cmdB);
    slvCmd->cmd = SLV_CMD_WRITE;
    slvCmd->addr = offset;
    slvCmd->count = bSize;
    memcpy(cmdBuffer + 3, data + offset, bSize);
/*
    std::cout << "DataBuf: " << "\t";
    for(int i = 0 ; i < 8 ; ++i) std::cout << HEX( cmdBuffer[i] ) << "\t";
    std::cout << std::endl;
*/
    sendCommand(id, (uint32_t*)cmdBuffer, bSize + 3);

    offset += bSize;
  }

#endif
}

void IblRodSlave::fillDummyHisto(IblSlvHistCfg* pHistConfig, uint32_t iMask, uint32_t iPar, uint32_t unitMask, uint32_t nPar, uint32_t whichSlave, uint32_t histoUnit,
                                                             enum DummyScanType scanType, enum TestHistoStatus injectionType, uint32_t multiHit) {
#ifdef __XMK__

#ifdef __DS_FEI3__
  usingFei3 = true;
#endif

  int iMod, iChip;
  int iStep, iStepRow, row, col, tot;

//   int pixelsPerChip = COLSIZE * ROWSIZE;

  int x;
  int charge;
  int injections, hits;

//  Variables needed for Threshold Scan
  int expThr = EXPTHR;
  int varThr = 40;

  int rngThr = RNGTHR;
  int binDelta = (int)(2.0 * (double)RNGTHR / ((double)(nPar-1)));

  int expNoise = 160;
  int varNoise = 10;

  KRandom ndThreshold;
  KRandom ndNoise;
  KRandom ndNoiseSigma;
  ndThreshold.getFromGaussian((double)expThr, (double)varThr); //  First call is never "random", but just returns expected value.
  ndNoise.getFromGaussian((double)expNoise, (double)varNoise); //  First call is never "random", but just returns expected value.
//

  std::cout << "Working on Mask Step " << iMask << " in Bin " << iPar << std::endl;

//  Setup Slave command for running testHistogramming().
  IblRodSlave::LargeIblSlvRdWr_t slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_TEST;
  slvCmd.addr = histoUnit;
  uint32_t nWords = 1 + (sizeof(IblSlvRdWr)>>2); //  1 more for testHistogramming status flag

// Get parameters from histConfig that are needed here for the histogram to be generated.
  int chipsToScan = pHistConfig->cfg[histoUnit].chipSel == 1 ? pHistConfig->cfg[histoUnit].nChips : 1;
  int stepSize = (1 << pHistConfig->cfg[histoUnit].maskStep);

//  Get histogrammer ready to handle test (as oppsed to "real") data
  slvCmd.count = 1;
  switch(injectionType) {
    case(HISTO_TEST_SETUP): slvCmd.data[0] = HISTO_TEST_SETUP; break;
    case(HISTO_TEST_SETUP_NOHISTO): slvCmd.data[0] = HISTO_TEST_SETUP_NOHISTO; break;
    case(HISTO_TEST_SETUP_EXT): slvCmd.data[0] = HISTO_TEST_SETUP_EXT; break;
    default: std::cout << "Did not recognize this injectionType: " << injectionType << std::endl;
  }
  sendCommand(whichSlave, &(slvCmd.cmd), nWords);
  setVerbose(whichSlave, 0);

  slvCmd.count = 2; //  Send one fake hit at a time to histogrammer test register (count=2 for preceeding status flag)
  switch(injectionType) {
    case(HISTO_TEST_SETUP): slvCmd.data[0] = HISTO_TEST_INPROGRESS; break;
    case(HISTO_TEST_SETUP_NOHISTO): slvCmd.data[0] = HISTO_TEST_INPROGRESS_NOHISTO; break;
    case(HISTO_TEST_SETUP_EXT): slvCmd.data[0] = HISTO_TEST_INPROGRESS_EXT; break;
    default: std::cout << "Did not recognize this injectionType: " << injectionType << std::endl;
  }

  int scanProcessType = scanType;
  if(scanProcessType == CHECKERBOARD || scanProcessType == PIXELADDRESS) scanProcessType = CHECKERBOARD;
  switch(scanProcessType) {
    case(CHECKERBOARD):
//  Generate Occupacy pattern to be reproduced in FitServer Histograms

//       for (iChip = 22; iChip < 23; ++iChip) { //  Hard Coded here to imitate initial tests using MCCTestScan.
      for (iChip = 0; iChip < chipsToScan; ++iChip) {
        iMod = iChip;
        if(usingFei3) iMod /= 16;
        if(0x1 & (unitMask >> iMod)) {
          std::cout << "Starting dummy occupancy Scan for Chip " << iChip << std::endl;
          #ifdef DEBUG_ND
            std::cout << "XxXxXxX injectionType = " << injectionType << ", stepSize = " << stepSize << ", scanType = " << scanType
                      << ", scanProcessType = " << scanProcessType << ", unitMask = " << HEX(unitMask) << std::endl;
          #endif
          for(iStep = 0; iStep < ROWSIZE/stepSize; iStep++) {
            #ifdef DEBUG_ND
              std::cout << "YyYyYyY iStep = " << iStep << std::endl;
            #endif
            iStepRow = iStep*stepSize;
            row = iStepRow + iMask + 1; //  Note:  Row and column numbers start at 1, whereas chip numbers start at 0 !!
            for(col = 1; col <= COLSIZE; col++) {

              int mStepEven = pHistConfig->cfg[histoUnit].mStepEven;
              int mStepOdd = pHistConfig->cfg[histoUnit].mStepOdd;
              if(usingFei3) mStepEven = stepSize - iMask;
              if(usingFei3) mStepOdd = iMask + 1;
              if(mStepEven > 0) { //  This is the "flag" that doubble-column processing is in effect
                (col%2 == 0) ? row = iStepRow + mStepEven :
                               row = iStepRow + mStepOdd; //  Always same as iStepRow + iMask + 1 ?
              }

              injections = 50 * ((row+col)%2 == 0); //  The "checkerboard" pattern
              #ifdef DEBUG_ND
                std::cout << "col = " << col << ", row = " << row << ", injections = " << injections << ", scanType = " << scanType
                          << ", PIXELADDRESS = " << PIXELADDRESS << std::endl;
              #endif
              if(scanType == PIXELADDRESS) {
//                 injections = iChip*100000 + row*100 + col; // ONLY use in no-histo mode !!!
//                 injections = ((row < 11) ? row : 1);
//                 injections *= ((row+col)%2 == 0); //  Superimpose "checkerboard" pattern - This is a managable address check in histo mode
                injections = iMask + 1; //  Nice way to highlight double-column mask-shifting
              }
              #ifdef DEBUG_ND
                std::cout << "injections = " << injections << std::endl;
              #endif
              tot = 1+(row-1)%16;
              if(injections > 0) {
                slvCmd.data[1] = 0;
                if(injectionType == HISTO_TEST_SETUP_NOHISTO)
                  slvCmd.data[1] |= ((1+((row-1)/stepSize)) << ROW_SHIFT); //  Note:  Row numbers of masked memory location are NOT the actual chip row numbers
                else slvCmd.data[1] |= (row << ROW_SHIFT);
                slvCmd.data[1] |= (col << COL_SHIFT);
                slvCmd.data[1] |= (iChip << CHIP_SHIFT);
                slvCmd.data[1] |= (tot << TOT_SHIFT);
                if(multiHit) {
                  slvCmd.data[1] |= 0x80000000;
                  slvCmd.data[2] = injections;
                  sendCommand(whichSlave, &(slvCmd.cmd), nWords+1);
                } else {
                  for(int i = 0; i < injections; i++) {
                    sendCommand(whichSlave, &(slvCmd.cmd), nWords);
                  }
                }
                #ifdef DEBUG_ND
                  if(iChip == 1)std::cout << "ZzZzZzZ iMask = " << iMask << ", iChip = " << iChip << ", col = " << col << ", row= " << row << ", data[1] = "
                            << HEX(slvCmd.data[1]) << ", injections, = " << injections << ", tot = " << tot << ", multihit = " << multiHit << std::endl;
                #endif
              }
            }
          }
        }
      }
      break;

    case(THR_SCURVE_FIT):
//  Generate Fake Threshold Scan Results

      injections = 25;
      for (iChip = 0; iChip < chipsToScan; ++iChip) {
        iMod = iChip;
        if(usingFei3) iMod /= 16;
        if(0x1 & (unitMask >> iMod)) {
          std::cout << "Starting dummy threshold Scan for Chip " << iChip << std::endl;
          #ifdef DEBUG_ND
            std::cout << "XxXxXxX injectionType = " << injectionType << ", stepSize = " << stepSize << ", scanType = " << scanType
                      << ", scanProcessType = " << scanProcessType << ", unitMask = " << HEX(unitMask) << std::endl;
          #endif
          for(iStep = 0; iStep < ROWSIZE/stepSize; iStep++) {
            #ifdef DEBUG_ND
              std::cout << "YyYyYyY iStep = " << iStep << std::endl;
            #endif
            iStepRow = iStep*stepSize;
            row = iStepRow + iMask + 1; //  Note:  Row and column numbers start at 1, whereas chip numbers start at 0 !!
            for(col = 1; col <= COLSIZE; col++) {

              int mStepEven = pHistConfig->cfg[histoUnit].mStepEven;
              int mStepOdd = pHistConfig->cfg[histoUnit].mStepOdd;
              if(usingFei3) mStepEven = stepSize - iMask;
              if(usingFei3) mStepOdd = iMask + 1;
              if(mStepEven > 0) { //  This is the "flag" that doubble-column processing is in effect
                (col%2 == 0) ? row = iStepRow + mStepEven :
                               row = iStepRow + mStepOdd; //  Always same as iStepRow + iMask + 1 ?
              }

              tot = 0;
              slvCmd.data[1] = 0;
              if(injectionType == HISTO_TEST_SETUP_NOHISTO)
                slvCmd.data[1] |= ((1+((row-1)/stepSize)) << ROW_SHIFT); //  Note:  Row numbers of masked memory location are NOT the actual chip row numbers
              else slvCmd.data[1] |= (row << ROW_SHIFT);
              slvCmd.data[1] |= (col << COL_SHIFT);
              slvCmd.data[1] |= (iChip << CHIP_SHIFT);
              slvCmd.data[1] |= (tot << TOT_SHIFT);

              int pixelThreshold = 0;
              int pixelNoise = 0;
              /* We only accept threshold values above a certain charge value and noise values greater than 0 */
              while (pixelThreshold < rngThr) {
                pixelThreshold = (int) ndThreshold.getFromGaussian((double)expThr, (double)varThr); //  Distribution around expected Threshold
              }
              while (pixelNoise <= 0) {
                pixelNoise = (int) ndNoise.getFromGaussian((double)expNoise, (double)varNoise); //  Distribution around expected Threshold Noise
              }

              //  Uncomment for useful debugging output of a 15-bin scan:
              // if(6 < iPar && iPar < 9)std::cout << "Histogram with Threshold: " << pixelThreshold << " Noise: " << pixelNoise << std::endl;

              hits = 0;
              x = expThr - rngThr + iPar*binDelta;
              for (int i = 0; i < injections; i++) {
                charge = x + (int) ndNoiseSigma.getFromGaussian(0.0, (double)pixelNoise); //  Distribution and variate for electronic noise variation
                //  Uncomment for useful debugging output of a 15-bin scan:
                // if(6 < iPar && iPar < 9)std::cout << "charge = " << charge << " Threshold = " << pixelThreshold << std::endl;
                if (charge > pixelThreshold) {
                  hits++;
                  if(!multiHit) sendCommand(whichSlave, &(slvCmd.cmd), nWords); //  One "hit" at a time is of course much slower
                }
              }

              if(col == 1 && (iStep < 3 || (ROWSIZE/stepSize - iStep) < 4)) std::cout << "                                  Chip/Row "
                                                                                      << iChip << "/" << row <<  " Hits = " << hits << std::endl;
              if(col == 1 && (iStep == 3)) std::cout                                  << "                                           . . ." << std::endl << std::endl;
              if(col == 1 && ((ROWSIZE/stepSize - iStep) == 4)) std::cout             << "                                           . . ." << std::endl;

              if(multiHit && hits > 0) {
                slvCmd.data[1] |= 0x80000000;
                slvCmd.data[2] = hits;
                sendCommand(whichSlave, &(slvCmd.cmd), nWords + 1);
              }
            }
          }
        }
      }

      break;

    default:
      xil_printf("\r\n ***** Unsupported DummyScan type: %d\r\n       Nothing Done.\r\n\r\n", scanType);
      return;
  } //  scanType

//
//  Tell histogrammer to tranfer results to FitServer and return itself to the state it originally was in
//
  setVerbose(whichSlave, 1);
  slvCmd.count = 1;
  switch(injectionType) {
    case(HISTO_TEST_SETUP): slvCmd.data[0] = HISTO_TEST_FINISH; break;
    case(HISTO_TEST_SETUP_NOHISTO): slvCmd.data[0] = HISTO_TEST_FINISH_NOHISTO; break; //  Different because of no call to stopHistogramming()
    case(HISTO_TEST_SETUP_EXT): slvCmd.data[0] = HISTO_TEST_FINISH; break;
    default: std::cout << "Did not recognize this injectionType: " << injectionType << std::endl;
  }
  sendCommand(whichSlave, &(slvCmd.cmd), nWords);

#endif
}

void IblRodSlave::testRxBypass(IblSlvHistCfg* pHistConfig, uint32_t iMask, uint32_t unitMask, uint32_t whichSlave, uint32_t histoUnit) {
#ifdef __XMK__

#ifdef __DS_FEI3__
  usingFei3 = true;
#endif

  int iMod, iChip, iStep, iStepRow, row, col, tot, injections;

//  Setup Slave command for running testHistogramming().
  IblRodSlave::LargeIblSlvRdWr_t slvCmd;
  slvCmd.cmd = SLV_CMD_HIST_TEST;
  slvCmd.addr = histoUnit;
  uint32_t nWords = 1 + (sizeof(IblSlvRdWr)>>2); //  1 more for testHistogramming status flag

// Get parameters from histConfig that are needed here for the histogram to be generated.
  int chipsToScan = pHistConfig->cfg[histoUnit].chipSel == 1 ? pHistConfig->cfg[histoUnit].nChips : 1;
  int stepSize = (1 << pHistConfig->cfg[histoUnit].maskStep);

//  Get histogrammer ready to handle test (as oppsed to "real") data
  slvCmd.count = 1;
  slvCmd.data[0] = HISTO_TEST_SETUP;
  sendCommand(whichSlave, &(slvCmd.cmd), nWords);
  setVerbose(whichSlave, 0);

  slvCmd.count = 2; //  Send one fake hit at a time to histogrammer test register (count=2 for preceeding status flag)
  slvCmd.data[0] = HISTO_TEST_INPROGRESS;

  for (iChip = 0; iChip < chipsToScan; ++iChip) {
    iMod = iChip;
    if(usingFei3) iMod /= 16;
    if(0x1 & (unitMask >> iMod)) {
      std::cout << "Starting dummy Rx-occupancy Scan Mask Step " << iMask << " for Chip " << iChip << " and unitmask = " << HEX(unitMask) << std::endl;
      #ifdef DEBUG_ND
        std::cout << "XxXxXxX stepSize = " << stepSize << ", unitMask = " << HEX(unitMask) << std::endl;
      #endif
      for(iStep = 0; iStep < ROWSIZE/stepSize; iStep++) {
        #ifdef DEBUG_ND
          std::cout << "YyYyYyY iStep = " << iStep << std::endl;
        #endif
        iStepRow = iStep*stepSize;
        row = iStepRow + iMask + 1; //  Note:  Row and column numbers start at 1, whereas chip numbers start at 0 !!
        for(col = 1; col <= COLSIZE; col++) {

          int mStepEven = pHistConfig->cfg[histoUnit].mStepEven;
          int mStepOdd = pHistConfig->cfg[histoUnit].mStepOdd;
          if(usingFei3) mStepEven = stepSize - iMask;
          if(usingFei3) mStepOdd = iMask + 1;
          if(mStepEven > 0) { //  This is the "flag" that doubble-column processing is in effect
            (col%2 == 0) ? row = iStepRow + mStepEven :
                           row = iStepRow + mStepOdd; //  Always same as iStepRow + iMask + 1 ?
          }

//  Determine if this is a pixel that should get an injection, and if so, send it the command to so so here.
          tot = row;
          // injections = 1; //  5' 30'' on an entire ROD with 7 MCCs per histoUnit
          injections = (row + col)%2;
          if(injections > 0) {
            slvCmd.data[1] = 0;
            slvCmd.data[1] |= (row << ROW_SHIFT);
            slvCmd.data[1] |= (col << COL_SHIFT);
            slvCmd.data[1] |= (iChip << CHIP_SHIFT);
            slvCmd.data[1] |= (tot << TOT_SHIFT);
            slvCmd.data[1] |= 0x80000000;
            #ifdef DEBUG_ND
              std::cout << iMask << " " << iChip << " " << col << " " << row << " "
                        << HEX(slvCmd.data[1]) << " " << injections << " " << tot << std::endl;
            #endif
            slvCmd.data[2] = injections;
            sendCommand(whichSlave, &(slvCmd.cmd), nWords+1);
          }
        }
      }
    }
  }

//
//  Tell histogrammer to tranfer results to FitServer and return itself to the state it originally was in
//
  setVerbose(whichSlave, 1);

#endif
}

//Initialize the front end emulator for Layer 1/layer2 PIX
void IblRodSlave::MCCEmu::config(uint8_t slaveId, uint32_t Config1, uint32_t Config2)
{
    std::cout << "DEBUG: Configuring the ROD PixEmu; Config1: " << std::hex << Config1 << "       Config2: " << Config2 << std::dec << std::endl;

		if(slaveId == 0){
			IblRod::SlaveA::PixelConfig1Reg::write(Config1);		
			std::cout << "DEBUG: read PixelConfig1Reg: " << std::hex << IblRod::SlaveA::PixelConfig1Reg::read() << std::dec << std::endl;
		}
		else{
			IblRod::SlaveB::PixelConfig1Reg::write(Config1);
			std::cout << "DEBUG: read PixelConfig1Reg: " << std::hex << IblRod::SlaveB::PixelConfig1Reg::read() << std::dec << std::endl;
		}

		if(slaveId == 0){
			IblRod::SlaveA::PixelConfig2Reg::write(Config2);		
			std::cout << "DEBUG: read PixelConfig2Reg: " << std::hex << IblRod::SlaveA::PixelConfig2Reg::read() << std::dec << std::endl;
		}
		else{
			IblRod::SlaveB::PixelConfig2Reg::write(Config2);
			std::cout << "DEBUG: read PixelConfig2Reg: " << std::hex << IblRod::SlaveB::PixelConfig2Reg::read() << std::dec << std::endl;
		}

}

void IblRodSlave::MCCEmu::init(uint8_t slaveId, uint8_t frameCount, uint8_t speed) {

		std::cout << "Configuring ROD MCC emulator..." << std::endl;		
		//Tells the formatter to take the input from the simulator and not from the BOC
		uint8_t SimEnable = 0x0;
		if(slaveId == 0){
			SimEnable = IblRod::SlaveA::PixelSimReg::read();
			SimEnable |= 0x1;
			IblRod::SlaveA::PixelSimReg::write(SimEnable);
		}
		else{
			SimEnable = IblRod::SlaveB::PixelSimReg::read();
			SimEnable |= 0x1;
			IblRod::SlaveB::PixelSimReg::write(SimEnable);
		}

    uint32_t Config1 = 0x01 /*| (speed << 1)*/;	//default 1x40
    Config1 |= speed;
    Config1 |= ((frameCount-1) <<4);
    uint32_t Config2 = 0x01000000;		//default 1 hits per trigger/L1A
    MCCEmu::config(slaveId, Config1, Config2);
}

void IblRodSlave::MCCEmu::reset(uint8_t slaveId) {
		std::cout << "Resetting ROD MCC emulator..." << std::endl;		
		//Tells the formatter to take the input from the simulator and not from the BOC
		uint8_t SimEnable = 0x0;
		if(slaveId == 0){
			SimEnable = IblRod::SlaveA::PixelSimReg::read();
			SimEnable &= 0xFE;
			IblRod::SlaveA::PixelSimReg::write(SimEnable);
		}
		else{
			SimEnable = IblRod::SlaveB::PixelSimReg::read();
			SimEnable &= 0xFE;
			IblRod::SlaveB::PixelSimReg::write(SimEnable);
		}
		
		MCCEmu::config(slaveId, 0, 0);
}


void IblRodSlave::MCCEmu::setHitCount(uint8_t slaveId, uint8_t c) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": slaveId=" << (int)slaveId << ", count=" << c << std::endl;
	uint32_t counterValue = ((c << 24) & 0xFF000000);
	if(slaveId == 0){
		IblRod::SlaveA::PixelConfig2Reg::write(counterValue);		
		std::cout << "DEBUG: read PixelConfig2Reg: " << std::hex << IblRod::SlaveA::PixelConfig2Reg::read() << std::dec << std::endl;
	}
	else{
		IblRod::SlaveB::PixelConfig2Reg::write(counterValue);
		std::cout << "DEBUG: read PixelConfig2Reg: " << std::hex << IblRod::SlaveB::PixelConfig2Reg::read() << std::dec << std::endl;
	}
}

void IblRodSlave::setFmtFei3Reg(IblBoc::PixSpeed pixSpeed, uint8_t slaveMask) {
#ifdef __XMK__
  uint8_t fmtFei3RegValue = 0x00; // Default for SINGLE_40
  switch(pixSpeed) {
    case IblBoc::Speed_40:
      fmtFei3RegValue = 0x00;
      break;
    case IblBoc::Speed_40_Alt:
      std::cout << "WARNING: IblBoc::Speed_40_Alt is not supported. Falling back to IblBoc::Speed_40" << std::endl;
      fmtFei3RegValue = 0x00;
      break;
    case IblBoc::Speed_80:
      fmtFei3RegValue = 0x01;
      break;
    case IblBoc::Speed_160:
      fmtFei3RegValue = 0x42; // bit 0/1 drive the read out speed; bit 6/7 drive the inversion of the 4 lines in
      break;
    default:
      std::cout << "ERROR: this mode is not supported" << std::endl;
  }

  std::cout << "fmtFei3RegValue=" << (int)fmtFei3RegValue << std::endl;
  if( slaveMask & 0x1) {
    IblRod::SlaveA::FmtFei3RegS1::write(fmtFei3RegValue);
    //IblRod::SlaveA::FmtFei3RegS2::write(fmtFei3RegValue);
  }
  if( slaveMask & 0x2) {
    IblRod::SlaveB::FmtFei3RegS1::write(fmtFei3RegValue);
    //IblRod::SlaveB::FmtFei3RegS2::write(fmtFei3RegValue);
  }
#endif
}

