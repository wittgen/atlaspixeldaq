#include "IblBoc.h"
#include "RodMasterRegisters.h"

#include <chrono>
#include <thread>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

// Todo: add flags for verbose __PRETTY_FUNCTION__
#ifndef VERBOSE_RW
#define VERBOSE_RW false
#endif

IblBoc::IblBoc() {
	std::cout << __PRETTY_FUNCTION__ << std::endl;
}

bool IblBoc::write(uint32_t regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": regNum=" << HEX(regNum) << ", value=" << HEX(value) << std::endl;
	IblRod::EpcBoc::write(regNum, value);
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": regNum=" << HEX(regNum) << ", written=" << HEX(IblBoc::read(regNum)) << std::endl;

	// Slower, but safer: checks that we wrote the value fine.
	// TODO: Remove in production
	//if((regNum & 0x5) == IblBoc::DataHigh || (regNum & 0x5) == IblBoc::DataLow) return true;
	//return IblBoc::read(regNum) == value;
	// Default value: assume it went OK
	return true;
}

bool IblBoc::writeOnly(uint32_t regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": regNum=" << HEX(regNum) << ", value=" << HEX(value) << std::endl;
	IblRod::EpcBocWriteOnly::write(regNum, value);
	//Do not read
	return true;
}


uint8_t IblBoc::read(uint32_t regNum) {
    return IblRod::EpcBoc::read(regNum);
}

const uint16_t IblBoc::getRegister(BocFpga bocFPGA, BocRegType regType, uint8_t ch, uint8_t regNum) {
	uint16_t regAddr = regNum;
	if(bocFPGA == Bcf) return regAddr;
	regAddr |= ((2-bocFPGA)<<14);
	if(regType == FeEmu) {
		regAddr |= 0x80;
		regAddr |= ((ch & 0xF)<<3);
	} else { 
		regAddr |= (regType << 10);
		regAddr |= ((ch & 0x1F)<<5);
	}
	return regAddr;
}

const uint16_t IblBoc::getRegister(BcfReg const regNum) {
	return getRegister( Bcf, GR, 0, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, const BmfReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, GR, 0, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, Tx, ch, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, Rx, ch, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, Rx, ch, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, FeEmu, ch, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, FeEmu, ch, regNum );
}

const uint16_t IblBoc::getRegister(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum) {
	return getRegister( slaveId?BmfSouth:BmfNorth, Rx, ch, regNum );
}

void IblBoc::write(const BcfReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(regNum), value );
}

void IblBoc::write(uint8_t slaveId, const BmfReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::writeOnly(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum, uint8_t value) {
	IblBoc::writeOnly( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum, uint8_t value) {
    if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::writeOnly(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum, uint8_t value) {
	IblBoc::writeOnly( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::writeOnly(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum, uint8_t value) {
	IblBoc::writeOnly( IblBoc::getRegister(slaveId, ch, regNum), value );
}

void IblBoc::write(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum, uint8_t value) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": " << HEX(IblBoc::getRegister(slaveId, ch, regNum)) << ", " << HEX(value) << std::endl;
	IblBoc::write( IblBoc::getRegister(slaveId, ch, regNum), value );
}

uint8_t IblBoc::read(BcfReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, const BmfReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  if(regNum == RxStatus) IblBoc::write( slaveId, ch, regNum, 0xFF );
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

uint8_t IblBoc::read(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum) {
  if (VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << std::endl;
  return IblBoc::read( IblBoc::getRegister(slaveId, ch, regNum) );
}

bool IblBoc::laserOn(bool doIt) {
        std::cout << __PRETTY_FUNCTION__ << ": Enabling laser!" << std::endl;
	// Todo: less cryptic
  	if(doIt) IblBoc::write(0x7, 0xab);
	else IblBoc::write(0x7,0x00);
	return IblBoc::read(0x7) == (doIt?0xab:0x00);
}

void IblBoc::dumpFwInfo() {
	std::cout << std::string(80,'-') << std::endl;
	std::cout << __PRETTY_FUNCTION__ << std::endl;

	const char* chipName[] = { "BCF", "BMF North", "BMF South" };
	for(int i = 0 ; i < 3 ; ++i) {
		int addrBase = (i ? (0x8000 >>(i-1)) : 0);
		std::cout << chipName << " FW: " << HEX(IblBoc::read(addrBase+0)) << ". Built on ";
		std::cout << static_cast<int>(IblBoc::read(addrBase+3)) << "/" << static_cast<int>(IblBoc::read(addrBase+2)) << "/" << static_cast<int>(IblBoc::read(addrBase+1)) <<  " @ ";
		std::cout << static_cast<int>(IblBoc::read(addrBase+4)) << ":" << static_cast<int>(IblBoc::read(addrBase+5)) << ":" << static_cast<int>(IblBoc::read(addrBase+6)) <<  std::endl;
	}

	std::cout << std::string(80,'-') << std::endl;

	std::cout << "Reading BOC registers" << std::endl;
	for(int i = 0 ; i < 7 ; ++i) std::cout << i << ": " << HEX(IblBoc::read(i)) << std::endl;
	std::cout << "Bmf::North" << std::endl;
	for(int i = 0 ; i < 7 ; ++i) std::cout << i << ": " << HEX(IblBoc::read(0x8000+i)) << std::endl;
	std::cout << "Bmf::South" << std::endl;
	for(int i = 0 ; i < 7 ; ++i) std::cout << i << ": " << HEX(IblBoc::read(0x4000+i)) << std::endl;
	std::cout << "Done reading BOC registers" << std::endl;
}


const uint8_t IblBoc::GetVersion(uint8_t slaveId){
  return IblBoc::read(slaveId, IblBoc::BmfFirmware);
}

const bool IblBoc::isNewEmulator(uint8_t slaveId){
  return IblBoc::GetVersion(slaveId) >= 0x70;
}

void IblBoc::UnlockSecureRegBmf(uint8_t slaveId){

	if((IblBoc::GetFeatures(slaveId) & 0x8) != 0 ) {//Only protected in BOC FW from June 2018 with new features
         IblBoc::write(slaveId, IblBoc::BmfSecureReg, 0x42);
         std::cout<<"Unlocking BMF sec reg"<<std::endl;
        }

}

void IblBoc::sLinkReset(uint8_t slaveId, uint8_t link, bool holaReset, bool mgtReset) {
	if(link>2){ std::cerr << "Warning: SLink link ID set to non-physical value. Using link=1." << std::endl; link=1; }
	std::cout << "Resetting SLINK" << (int)link << " for slave " << (int)slaveId << std::endl;


	BmfReg sLinkCtrlReg = link ? SLink1Ctrl : SLink0Ctrl;
	BmfReg sLinkStatReg = link ? SLink1Stat : SLink0Stat;


        uint16_t sLinkCtrlRegValue ;
        //uint16_t sLinkStatRegValue ;

	sLinkCtrlRegValue=IblBoc::read(slaveId, sLinkCtrlReg);
	//sLinkStatRegValue=
            (void)IblBoc::read(slaveId, sLinkStatReg);

	std::cout << "SLINK" << (int)link << " Ctrl for slave " << (int)slaveId << ": " << HEX( IblBoc::read(slaveId, sLinkCtrlReg) ) << std::endl;
	std::cout << "SLINK" << (int)link << " Status for slave " << (int)slaveId << ": " << HEX( IblBoc::read(slaveId, sLinkStatReg) ) << std::endl;
	std::cout << "RESETTING THE SLINK" << (int)link << " for slave " << (int)slaveId <<" with holareset= " <<holaReset<<" and mgtreset= "<<mgtReset<<std::endl;

        sLinkCtrlRegValue &= ~(0x1 << IntUResetN);//Active Low IntUReset bit
        if(holaReset) sLinkCtrlRegValue |= (0x1 << HolaReset);
	if(mgtReset) sLinkCtrlRegValue |= (0x1 << MgtReset);

        IblBoc::UnlockSecureRegBmf(slaveId);
	IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue);

	if(mgtReset) { sLinkCtrlRegValue &= ~(0x1 << MgtReset); IblBoc::UnlockSecureRegBmf(slaveId); IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue); std::this_thread::sleep_for(std::chrono::milliseconds(1)); }
	if(holaReset) { sLinkCtrlRegValue &= ~(0x1 << HolaReset);  IblBoc::UnlockSecureRegBmf(slaveId); IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue); std::this_thread::sleep_for(std::chrono::milliseconds(1)); }
	sLinkCtrlRegValue |= (0x1 << IntUResetN);  IblBoc::UnlockSecureRegBmf(slaveId);IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue);
std::this_thread::sleep_for(std::chrono::milliseconds(1));

	std::cout << "SLINK" << (int)link << " Ctrl for slave " << (int)slaveId << " after reset " << HEX( IblBoc::read(slaveId, sLinkCtrlReg) ) << std::endl;
	std::cout << "SLINK" << (int)link << " status for slave " << (int)slaveId << " after reset: " << HEX( IblBoc::read(slaveId, sLinkStatReg) ) << std::endl;

}

void IblBoc::sLinkEnable(uint8_t slaveId, uint8_t link, SLinkEnableMode mode) {
	if(link>2){ std::cerr << "Warning: SLink link ID set to non-physical value. Using link=1." << std::endl; link=1; }
	std::cout << "Enabling SLINK" << (int)link << " for slave " << (int)slaveId << " with mode " << mode << std::endl;

	BmfReg sLinkCtrlReg = link ? SLink1Ctrl : SLink0Ctrl;
	BmfReg sLinkStatReg = link ? SLink1Stat : SLink0Stat;

        uint16_t sLinkCtrlRegValue;
        uint16_t sLinkStatRegValue;

	// use external ROD SLINK clock
	sLinkCtrlRegValue=IblBoc::read(slaveId, sLinkCtrlReg);
	std::cout << "Switching both SLINKs to external ROD clock.\n";
	sLinkCtrlRegValue |= 0x1 << ClkSel;
	IblBoc::UnlockSecureRegBmf(slaveId);
	IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue);

	// disable test mode
	sLinkCtrlRegValue=IblBoc::read(slaveId, sLinkCtrlReg);
	std::cout << "Disabling SLINK test generator...\n" << std::flush;
	sLinkCtrlRegValue &= ~( (0x1 << TestSel) | (0x1 << TestEna) );
	IblBoc::UnlockSecureRegBmf(slaveId);
	IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue);

	// resetting SLINK (incl. MGT and HOLA if necessary)
        bool hardresetmode=false;
	std::cout << "Resetting both SLINKs... + also removing HOLA and MGT reset bit the first time after FPGA reset/power cycle " << std::flush;
        if ((sLinkCtrlRegValue & 0x70) != 0x10) hardresetmode=true;
	else hardresetmode=false;

	IblBoc::sLinkReset(slaveId, link, hardresetmode, hardresetmode);

	sLinkCtrlRegValue=IblBoc::read(slaveId, sLinkCtrlReg);
	sLinkStatRegValue=IblBoc::read(slaveId, sLinkStatReg);
	std::cout << "SLINK" << (int)link << " Ctrl for slave " << (int)slaveId << " after reset and before SlinkMode: " << HEX( sLinkCtrlRegValue) << std::endl;
	std::cout << "SLINK" << (int)link << " Status for slave " << (int)slaveId << " after reset and before SLinkMode: " << HEX( sLinkStatRegValue) << std::endl;

	// enable or not FTK holas wiht XOff and LDown
	if (mode == SlinkIgnoreFTK) {
	  std::cout<<" Enabling IBL/Pixel Hola only ==> FTK link ignored"<<std::endl;
	  sLinkCtrlRegValue &= ~(0x1 <<  FTKXOffEna);
	  sLinkCtrlRegValue &= ~(0x1 << FTKLDownEna);
	}
	else if (mode ==  SlinkEnableFTKXOff) {
	  std::cout<<" Enabling FTK XOff and disabling FTK LDown"<<std::endl;
	  sLinkCtrlRegValue |=  (0x1 <<  FTKXOffEna);
	  sLinkCtrlRegValue &= ~(0x1 << FTKLDownEna);
	}
	else if (mode == SlinkEnableFTKLDown) {
	  std::cout<<" Enabling FTK LDown and disabling FTK XOff"<<std::endl;
	  sLinkCtrlRegValue &= ~(0x1 <<  FTKXOffEna);
	  sLinkCtrlRegValue |=  (0x1 << FTKLDownEna);
	} 
	else if (mode == SlinkEnableFTKBoth){
	  std::cout<<" Enabling both FTK LDown and FTK XOff"<<std::endl;
	  sLinkCtrlRegValue |=  (0x1 <<  FTKXOffEna);
	  sLinkCtrlRegValue |=  (0x1 << FTKLDownEna);
	}

	std::cout << "SLINK" << (int)link << " Ctrl for slave " << (int)slaveId << "after setting SlinkMode: " << HEX(sLinkCtrlRegValue) << std::endl;
	IblBoc::UnlockSecureRegBmf(slaveId);
	IblBoc::write(slaveId, sLinkCtrlReg, sLinkCtrlRegValue);
	std::cout << "SLINK" << (int)link << " Status for slave " << (int)slaveId << "after setting SlinkMode: " << HEX( IblBoc::read(slaveId, sLinkStatReg) ) << std::endl;

	// finishing
	std::cout << "The SLINK is now enabled. The ROD can now send fragments to the ROBIN.\n";
}

void IblBoc::IBLFeEmu::init(uint8_t slaveId, uint8_t ch) {
	// Changing INPUT_SELECT (bit 5) part without touching other bits.
	uint16_t rxControl = IblBoc::read(slaveId, ch, RxControl);
	IblBoc::write(slaveId, ch, RxControl, (rxControl | 0x20) );

       if(!IblBoc::isNewEmulator(slaveId)) {
	// Resetting the emulator
	IblBoc::write(slaveId, ch, FeEmuControl, 0x0);

	std::this_thread::sleep_for(std::chrono::milliseconds(1));

	// Enabling the emulator with automatic hit injection
	IblBoc::write(slaveId, ch, FeEmuControl, 0x5);
          uint8_t feEmuControl = IblBoc::read(slaveId, ch, FeEmuControl);
	  feEmuControl &= 0x0F;
	  feEmuControl |= ((6+(ch%2)) << 4);
	  IblBoc::write(slaveId, ch, FeEmuControl, feEmuControl);

          feEmuControl = IblBoc::read(slaveId, ch, FeEmuControl);
        } else{
          uint8_t tmp = IblBoc::read(slaveId, ch, FeEmuControlLUT);
          // Resetting the emulator
          IblBoc::write(slaveId, ch, FeEmuControlLUT, tmp & 0xFE );
          std::this_thread::sleep_for(std::chrono::milliseconds(1));
          // Enabling the emulator
          IblBoc::write(slaveId, ch, FeEmuControlLUT, tmp|0x1 );
        }

}

void IblBoc::IBLFeEmu::reset(uint8_t slaveId, uint8_t ch) {
	// Changing INPUT_SELECT part without touching other bits
	uint16_t rxControl = IblBoc::read(slaveId, ch, RxControl);
	IblBoc::write(slaveId, ch, RxControl, (rxControl & 0x1F) );

	// Resetting the emulator
	if(!IblBoc::isNewEmulator(slaveId)) {
	  IblBoc::write(slaveId, ch, FeEmuControl, 0x0);
	} else {
	  uint8_t tmp = IblBoc::read(slaveId, ch, FeEmuControl);
          IblBoc::write(slaveId, ch, FeEmuControlLUT, tmp & 0xFE );
	}
}

void IblBoc::IBLFeEmu::setHitCount(uint8_t slaveId, uint8_t ch, uint8_t c) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": slaveId=" << (int)slaveId << ", ch=" << (int)ch << ", count=" << c << std::endl;
	IblBoc::write(slaveId, ch, HitCountPerTrigger, c);
}

//New FE-I4 emulator method July 2020
// ToT and extra frames
uint8_t IblBoc::IBLFeEmu::GetExtraFrames(uint8_t slaveId, uint8_t ch){
   return IblBoc::read(slaveId, ch, ExtraFrames);
}

void IblBoc::IBLFeEmu::SetExtraFrames(uint8_t slaveId, uint8_t ch,uint8_t Value){
  IblBoc::write(slaveId, ch, ExtraFrames, Value);
}

uint8_t IblBoc::IBLFeEmu::GetTotValue(uint8_t slaveId, uint8_t ch){
   return IblBoc::read(slaveId, ch, TotValue) >> 4;
}

void IblBoc::IBLFeEmu::SetTotValue(uint8_t slaveId, uint8_t ch, uint8_t Value){

  uint8_t tmp = IblBoc::read(slaveId, ch, TotValue);
  tmp &= 0xF;
  tmp |= (Value & 0xF) << 4;

  IblBoc::write(slaveId, ch, TotValue, tmp);

}
uint8_t IblBoc::IBLFeEmu::GetTot2Value(uint8_t slaveId, uint8_t ch){
   return (IblBoc::read(slaveId, ch, TotValue) & 0xF );
}

void IblBoc::IBLFeEmu::SetTot2Value(uint8_t slaveId, uint8_t ch,uint8_t Value){

  uint8_t tmp = IblBoc::read(slaveId, ch, TotValue);
  tmp &= 0xF0;
  tmp |= (Value & 0xF);

  IblBoc::write(slaveId, ch, TotValue, tmp);
}

// Hit-Count LUT loading
void IblBoc::IBLFeEmu::WriteLUT(uint8_t slaveId, uint8_t ch, const std::vector <uint8_t> & data){

  if(!IblBoc::isNewEmulator(slaveId)) {
    std::cerr << "LUT reading/writing only works with firmware versions starting from 0x70" << std::endl;
    return;
  }

  // reset address register
  uint8_t tmp = IblBoc::read(slaveId, ch, FeEmuControlLUT);
  tmp |= 0x2;
  IblBoc::write(slaveId, ch, FeEmuControlLUT,tmp);

  for(size_t i =0;i< data.size();i++){
    IblBoc::writeOnly(slaveId, ch,IBLLutData, data[i]);
    if(i>=2048)break;
  }

}


void IblBoc::IBLFeEmu::ReadLUT(uint8_t slaveId, uint8_t ch, std::vector <uint8_t> & data){

  if(!IblBoc::isNewEmulator(slaveId)) {
    std::cerr << "LUT reading/writing only works with firmware versions starting from 0x70" << std::endl;
    return;
  }

  data.clear();
  // reset address register
  uint8_t tmp = IblBoc::read(slaveId, ch, FeEmuControlLUT);
  tmp |= 0x2;
  IblBoc::write(slaveId, ch, FeEmuControlLUT,tmp);

  for(size_t i =0;i< 2048;i++){
    data.push_back(IblBoc::read(slaveId, ch,IBLLutData));
  }

}


//Initialize the front end emulator for Layer 1/layer2 PIX
void IblBoc::PixFeEmu::MCCEmuConfig(uint8_t slaveId, uint8_t ch, uint32_t Config1, uint32_t Config2)
{

    std::cout << "DEBUG: Configuring the PixEmu; Config1: " << std::hex << Config1 << "       Config2: " << Config2 << std::dec << std::endl;
    uint8_t Config1_0 = ((Config1 >> 0) & 0xFF);
    uint8_t Config1_1 = ((Config1 >> 8) & 0xFF);
    uint8_t Config1_2 = ((Config1 >> 16) & 0xFF);
    uint8_t Config1_3 = ((Config1 >> 24) & 0xFF);

    //std::cout << "DEBUG: Configuring the PixEmu; Config1_0: " << std::hex << (uint32_t)Config1_0 << " Config1_1: " << (uint32_t)Config1_1 << " Config1_2: " << (uint32_t)Config1_2 << " Config1_3: " << (uint32_t)Config1_3 << std::dec << std::endl;

    IblBoc::write(slaveId, ch, PixEmuConfig1_0, Config1_0);	//16 = 0x10 it's the register Config1
    IblBoc::write(slaveId, ch, PixEmuConfig1_1, Config1_1);
    IblBoc::write(slaveId, ch, PixEmuConfig1_2, Config1_2);
    IblBoc::write(slaveId, ch, PixEmuConfig1_3, Config1_3);
    
    uint16_t DebugRead = IblBoc::read(slaveId, ch, PixEmuConfig1_0);
    //DebugRead = IblRod::EpcBoc::read( slaveId | ch << 5 | 0x10 );
    std::cout << "DEBUG: read register 0x10: " << std::hex << (uint32_t)DebugRead << std::dec << std::endl;


    uint8_t Config2_0 = ((Config2 >> 0) & 0xFF);
    uint8_t Config2_1 = ((Config2 >> 8) & 0xFF);
    uint8_t Config2_2 = ((Config2 >> 16) & 0xFF);
    uint8_t Config2_3 = ((Config2 >> 24) & 0xFF);

    IblBoc::write(slaveId, ch, PixEmuConfig2_0, Config2_0);	//20 = 0x14 it's the register Config2
    IblBoc::write(slaveId, ch, PixEmuConfig2_1, Config2_1);
    IblBoc::write(slaveId, ch, PixEmuConfig2_2, Config2_2);
    IblBoc::write(slaveId, ch, PixEmuConfig2_3, Config2_3);

    DebugRead = IblBoc::read(slaveId, ch, PixEmuConfig2_3);
    std::cout << "DEBUG: read register 0x17: " << std::hex << (uint32_t)DebugRead << std::dec << std::endl;

}

void IblBoc::PixFeEmu::init(uint8_t slaveId, uint8_t ch, uint8_t frameCount, uint8_t speed) {
    // enable emulator
    // TODO: need to figure out all the bit-meanings
    /*Config1 ([3,0]):
    [0]	0/1 	disable/enable PixSim
    [1,2]	speed	00 = 1x40, 10 = 2x40, 11 = 4x40
    [3]	multiple
    Config2	(8 MSBs)
    # of hits*/

    // Changing INPUT_SELECT (bit 5) part without touching other bits.
    //uint16_t rxControl = IblBoc::read(slaveId, ch, RxControl);
    //IblBoc::write(slaveId, ch, RxControl, rxControl | 0x20 );

    std::cout << "Configuring MCC emulator..." << std::endl;
    uint32_t Config1 = 0x01 /*| (speed << 1)*/;	//default 1x40
    Config1 |= speed;
    Config1 |= ((frameCount-1) <<4);
    if(IblBoc::isNewEmulator(slaveId))Config1 |= 0x100;
    uint32_t Config2 = 0x01000000;		//default 1 hits per trigger/L1A
    MCCEmuConfig(slaveId, ch, Config1, Config2);
}

void IblBoc::PixFeEmu::reset(uint8_t slaveId, uint8_t ch) {
	// Changing INPUT_SELECT part without touching other bits
	//uint16_t rxControl = IblBoc::read(slaveId, ch, RxControl);
	//IblBoc::write(slaveId, ch, RxControl, (rxControl & 0x1F) );

	// Resetting the emulator
	MCCEmuConfig(slaveId, ch, 0x0, 0x0);
}

void IblBoc::PixFeEmu::setHitCount(uint8_t slaveId, uint8_t ch, uint8_t c) {
	if(VERBOSE_RW) std::cout << __PRETTY_FUNCTION__ << ": slaveId=" << (int)slaveId << ", ch=" << (int)ch << ", count=" << c << std::endl;
	BmfPixFeEmuReg pixHitCount = PixEmuConfig2_3;
	IblBoc::write(slaveId, ch, pixHitCount, c);
}

// Hit-Count LUT loading
void IblBoc::PixFeEmu::WriteLUT(uint8_t slaveId, uint8_t ch, const std::vector <uint8_t> & data){

  if(!IblBoc::isNewEmulator(slaveId)) {
    std::cerr << "LUT reading/writing only works with firmware versions starting from 0x70" << std::endl;
    return;
  }

  // reset address register
  uint8_t tmp = IblBoc::read(slaveId, ch, RxControl);
  tmp |= 0x20;
  IblBoc::write(slaveId, ch, RxControl,tmp);

  for(size_t i =0;i< data.size();i++){
    IblBoc::writeOnly(slaveId, ch,LutData, data[i]);
    if(i>=2048)break;
  }

}

void IblBoc::PixFeEmu::ReadLUT(uint8_t slaveId, uint8_t ch, std::vector <uint8_t> & data){

  if(!IblBoc::isNewEmulator(slaveId)) {
    std::cerr << "LUT reading/writing only works with firmware versions starting from 0x70" << std::endl;
    return;
  }

  data.clear();
  // reset address register
  uint8_t tmp = IblBoc::read(slaveId, ch, RxControl);
  tmp |= 0x20;
  IblBoc::write(slaveId, ch, RxControl,tmp);

  for(size_t i =0;i< 2048;i++){
    data.push_back(IblBoc::read(slaveId, ch,LutData));
  }

}


uint32_t IblBoc::GetFrameCount(uint32_t rx_ch)
{
  uint8_t slaveId = rx_ch/16;
  uint8_t ch = rx_ch%16;

  uint32_t value = 0;
  if(IblBoc::IsPixelRx(slaveId)) {
    IblBoc::write(slaveId, ch, FrameCounter, 0x00);
    for(std::size_t offset = 0 ; offset <= 24 ; offset += 8)
      value |= ((0xff & IblBoc::read(slaveId, ch, FrameCounter)) << offset);
  } else { // IBL
    value |= ((0xff & IblBoc::read(slaveId, ch, FrameCnt0)) << 24);
    value |= ((0xff & IblBoc::read(slaveId, ch, FrameCnt1)) << 16);
    value |= ((0xff & IblBoc::read(slaveId, ch, FrameCnt2)) << 8);
    value |= ((0xff & IblBoc::read(slaveId, ch, FrameCnt3)));
  }

  return value;

}

void IblBoc::PixSetTxCoarseDelay(int8_t txCh, uint32_t coarseDelay){

   if(coarseDelay != PixGetTxCoarseDelay(txCh)){
   IblBoc::UnlockSecureRegBmf(txCh/16);
   IblBoc::write(txCh/16, txCh%16, IblBoc::CoarseDelay, coarseDelay);
   }
}

uint32_t IblBoc::PixGetTxCoarseDelay(int8_t txCh){

return IblBoc::read(txCh/16, txCh%16, IblBoc::CoarseDelay);

}


uint32_t IblBoc::GetCounter(uint32_t rx_ch, CounterId id)
{
    // each RX channel has an error count register at the offset 0x1D
    // if one want to read a counter, one has to write the counter to that register
    // possible are:
    // 0x0 lock loss counter
    // 0x1 sync loss counter
    // 0x2 decoding error counter
    // 0x3 disparity error counter
    // 0x4 frame error counter
    // after writing, one read back four times
    // this gives the counter value (32bit) from MSB to LSB

  uint8_t slaveId = rx_ch/16;
  uint8_t ch = rx_ch%16;

  IblBoc::write(slaveId, ch, ErrHistoCount, id);

  uint32_t counter = 0;

  counter |= static_cast<uint32_t>(IblBoc::read(slaveId, ch, ErrHistoCount) & 0xFF) << 24;
  counter |= static_cast<uint32_t>(IblBoc::read(slaveId, ch, ErrHistoCount) & 0xFF) << 16;
  counter |= static_cast<uint32_t>(IblBoc::read(slaveId, ch, ErrHistoCount) & 0xFF) << 8;
  counter |= static_cast<uint32_t>(IblBoc::read(slaveId, ch, ErrHistoCount) & 0xFF);

  return counter;
}


void IblBoc::ResetCounter(CounterId id)
{
  for (size_t i=0; i<32; i++) {
    uint8_t slaveId = i/16;
    uint8_t ch = i%16;

    IblBoc::write(slaveId, ch, ErrHistoCtrl, (0x08 | id));
    IblBoc::write(slaveId, ch, ErrHistoCtrl, 0x0);
  }
}

void IblBoc::ResetAllCounters() {
  for (size_t i=0; i<32; i++) {
    uint8_t slaveId = i/16;
    uint8_t ch = i%16;

    IblBoc::write(slaveId, ch, ErrHistoCtrl, 0x10);
    IblBoc::write(slaveId, ch, ErrHistoCtrl, 0x0);
  }
}

/*
 * This function will set the threshold on the custon RX plugin used for the L1/L2 readout
 * Arguments:
 *        fibre: a BOC global fibre number (0...47) not the RX channel!!
 *               a value < 0 will broadcast the threshold to all inputs
 *        threshold: the DAC value to be set
 */
void IblBoc::PixSetRxThreshold(int8_t fibre, uint16_t threshold)
{
	if(fibre < 0) {
		// this will be a broadcast to all plugins on the BOC
		for(int8_t i = 0; i < 48; i++) {
			IblBoc::PixSetRxThreshold(i, threshold);
		}
	} else {
		// just set threshold for a single channel
		uint8_t pluginId = (fibre / 12);		// each plugin has 12 channels
		uint8_t slaveId = pluginId / 2;			// we have 2 plugins per slave
		uint8_t channelId = (fibre % 12);		// get plugin internal channel

		// plugin internal DAC mapping
    	const unsigned int DACMapping[] = {
            (0x2 << 2) | 0x2,       // DAC 2 (I2C 0x41), Channel C
            (0x2 << 2) | 0x3,       // DAC 2 (I2C 0x41), Channel D
            (0x2 << 2) | 0x1,       // DAC 2 (I2C 0x41), Channel B
            (0x2 << 2) | 0x0,       // DAC 2 (I2C 0x41), Channel A
            (0x1 << 2) | 0x2,       // DAC 1 (I2C 0x40), Channel C
            (0x1 << 2) | 0x3,       // DAC 1 (I2C 0x40), Channel D
            (0x1 << 2) | 0x1,       // DAC 1 (I2C 0x40), Channel B
            (0x1 << 2) | 0x0,       // DAC 1 (I2C 0x40), Channel A
            (0x0 << 2) | 0x2,       // DAC 0 (I2C 0x32), Channel C
            (0x0 << 2) | 0x3,       // DAC 0 (I2C 0x32), Channel D
            (0x0 << 2) | 0x1,       // DAC 0 (I2C 0x32), Channel B
            (0x0 << 2) | 0x0        // DAC 0 (I2C 0x32), Channel A
    	};

    	// get DAC channel
    	uint8_t DACChannel = DACMapping[channelId];

    	// set threshold
    	if(pluginId % 2) {
    		// this is plugin 1 on the slave
    		IblBoc::write(slaveId, RxPlugin1DAC_DataL, threshold & 0xFF);
    		IblBoc::write(slaveId, RxPlugin1DAC_DataH, (threshold >> 8) & 0xFF);

    		// start transfer
    		IblBoc::write(slaveId, RxPlugin1DAC_Csr, (DACChannel << 4) | 0x1);

    		// wait until transfer is finished (or timed out)
    		uint32_t timeout = 100000;
    		while(IblBoc::read(slaveId, RxPlugin1DAC_Csr) & 0x1)
    		{
    			timeout--;
    			if(timeout == 0) {
    				std::cout << "Timeout when setting threshold=" << threshold << " on slaveId=" << slaveId;
    				std::cout << ", pluginId=" << pluginId << ", channelId=" << channelId << std::endl;
    				break;
    			}
    		}
    	} else {
    		// this is plugin 0 on the slave
    		IblBoc::write(slaveId, RxPlugin0DAC_DataL, threshold & 0xFF);
    		IblBoc::write(slaveId, RxPlugin0DAC_DataH, (threshold >> 8) & 0xFF);

    		// start transfer
    		IblBoc::write(slaveId, RxPlugin0DAC_Csr, (DACChannel << 4) | 0x1);

    		// wait until transfer is finished (or timed out)
    		uint32_t timeout = 100000;
    		while(IblBoc::read(slaveId, RxPlugin0DAC_Csr) & 0x1)
    		{
    			timeout--;
    			if(timeout == 0) {
    				std::cout << "Timeout when setting threshold=" << threshold << " on slaveId=" << slaveId;
    				std::cout << ", pluginId=" << pluginId << ", channelId=" << channelId << std::endl;
    				break;
    			}
    		}
    	}
	}
}

/*
 * This function retrieves the fibre threshold
 * Arguments:
 *        fibre: a BOC global fibre number (0...47) not the RX channel!!
 */

uint16_t IblBoc::GetRxThreshold(uint8_t Fibre){
    
    uint8_t pluginId = (Fibre / 12);		// each plugin has 12 channels
    uint8_t slaveId = pluginId / 2;			// we have 2 plugins per slave

    // get DAC and Channel
    	const unsigned int DACMapping[] = {
            (0x2 << 2) | 0x2,       // DAC 2 (I2C 0x41), Channel C
            (0x2 << 2) | 0x3,       // DAC 2 (I2C 0x41), Channel D
            (0x2 << 2) | 0x1,       // DAC 2 (I2C 0x41), Channel B
            (0x2 << 2) | 0x0,       // DAC 2 (I2C 0x41), Channel A
            (0x1 << 2) | 0x2,       // DAC 1 (I2C 0x40), Channel C
            (0x1 << 2) | 0x3,       // DAC 1 (I2C 0x40), Channel D
            (0x1 << 2) | 0x1,       // DAC 1 (I2C 0x40), Channel B
            (0x1 << 2) | 0x0,       // DAC 1 (I2C 0x40), Channel A
            (0x0 << 2) | 0x2,       // DAC 0 (I2C 0x32), Channel C
            (0x0 << 2) | 0x3,       // DAC 0 (I2C 0x32), Channel D
            (0x0 << 2) | 0x1,       // DAC 0 (I2C 0x32), Channel B
            (0x0 << 2) | 0x0        // DAC 0 (I2C 0x32), Channel A
    	};

    // get DAC channel
    uint8_t DACChSel = DACMapping[Fibre%12];

    // get value
    if(pluginId % 2)
    {
        uint16_t val = 0;

        // select DAC    	
	IblBoc::write(slaveId, RxPlugin1DAC_Csr, (DACChSel << 4));
							
        // read back the value
        val = IblBoc::read(slaveId, RxPlugin1DAC_DataL);
        val |= (uint16_t) IblBoc::read(slaveId, RxPlugin1DAC_DataH) << 8;

        // return it
        return val;
    }
    else
    {
        uint16_t val = 0;

        // select DAC    	
	IblBoc::write(slaveId, RxPlugin0DAC_Csr, (DACChSel << 4));

        // read back the value
        val = IblBoc::read(slaveId, RxPlugin0DAC_DataL);
        val |= (uint16_t) IblBoc::read(slaveId, RxPlugin0DAC_DataH) << 8;

        // return it
        return val;
    }


}

/*
 * This function configure the gain switch DAC
 * Arguments:
 *        pluginId: a BOC global plugin number (0...3)
 *                  a value < 0 will broadcast the RGS value to all inputs
 *        value: the DAC value to be set
 */
void IblBoc::PixSetRxRGS(int8_t pluginId, uint16_t value)
{
	if(pluginId < 0) {
		// broadcast to all plugins
		for(int8_t i = 0; i < 4; i++) {
			IblBoc::PixSetRxRGS(i, value);
		}
	} else {
		// just set a single plugin
		uint8_t slaveId = pluginId / 2;			// we have 2 plugins per slave

		// set threshold
		if(pluginId % 2) {
			// this is plugin 1 on the slave
			IblBoc::write(slaveId, RxPlugin1DAC_DataL, value & 0xFF);
			IblBoc::write(slaveId, RxPlugin1DAC_DataH, (value >> 8) & 0xFF);

			// start transfer
			IblBoc::write(slaveId, RxPlugin1DAC_Csr, 0xC1);

			// wait until transfer is finished (or timed out)
			uint32_t timeout = 100000;
			while(IblBoc::read(slaveId, RxPlugin1DAC_Csr) & 0x1)
			{
				timeout--;
				if(timeout == 0) {
					std::cout << "Timeout when setting RGS value=" << value << " on slaveId=" << slaveId << ", pluginId=" << pluginId << std::endl;
					break;
				}
			}
		} else {
			// this is plugin 0 on the slave
			IblBoc::write(slaveId, RxPlugin0DAC_DataL, value & 0xFF);
			IblBoc::write(slaveId, RxPlugin0DAC_DataH, (value >> 8) & 0xFF);

			// start transfer
			IblBoc::write(slaveId, RxPlugin0DAC_Csr, 0xC1);

			// wait until transfer is finished (or timed out)
			uint32_t timeout = 100000;
			while(IblBoc::read(slaveId, RxPlugin0DAC_Csr) & 0x1)
			{
				timeout--;
				if(timeout == 0) {
					std::cout << "Timeout when setting RGS value=" << value << " on slaveId=" << slaveId << ", pluginId=" << pluginId << std::endl;
					break;
				}
			}
		}
	}
}

/*
 * This function retrieves the plugin gain
 * Arguments: 
 *        pluginId: a BOC global plugin number (0...3)
 */

uint16_t IblBoc::GetRxRGS(uint8_t Plugin){
    
    uint8_t slaveId = Plugin / 2;			// we have 2 plugins per slave
    
    if(Plugin % 2) {
        uint16_t val = 0;

        // select RGS DAC
	IblBoc::write(slaveId, RxPlugin1DAC_Csr, 0xC0);

        // read back the value
        val = IblBoc::read(slaveId, RxPlugin1DAC_DataL);
        val |= (uint16_t) IblBoc::read(slaveId, RxPlugin1DAC_DataH) << 8;

        // return it
        return val;
    }
    else
    {
        uint16_t val = 0;

        // select RGS DAC
	IblBoc::write(slaveId, RxPlugin0DAC_Csr, 0xC0);

        // read back the value
        val = IblBoc::read(slaveId, RxPlugin0DAC_DataL);
        val |= (uint16_t) IblBoc::read(slaveId, RxPlugin0DAC_DataH) << 8;

        // return it
        return val;
    }

}


/*
 * This function configure RX sampling point for the pixel firmware
 * Arguments:
 *        fibre: a BOC global fibre number (0...47) not the RX channel!!
 *               a value < 0 will broadcast the threshold to all inputs
 *        sampling: select the sampling point (0...7)
 *        delay: IODELAY2 value (will add delay * 35 ps to the signal, to be confirmed)
 */
void IblBoc::SetRxSampleDelay(int8_t fibre, uint8_t sampling, uint8_t delay)
{
	if(fibre < 0) {
		// this will be a broadcast to all channels on the BOC
		for(int8_t i = 0; i < 48; i++) {
			IblBoc::SetRxSampleDelay(i, sampling, delay);
		}
	} else {
		// this will only change a single fibre

		// get slaveId
		uint8_t slaveId = fibre / 24;
		fibre = fibre % 24;

		// check if the slave is a Pixel slave
		if(IblBoc::IsPixelRx(slaveId) == true) {
			// write sampling point and delay
			IblBoc::write(IblBoc::getRegister(slaveId, CdrBase) + 2 * fibre, sampling & 0x7);
			IblBoc::write(IblBoc::getRegister(slaveId, CdrBase) + 2 * fibre + 1, delay);

            // check if delay was accepted
#ifdef __XMK__
      std::this_thread::sleep_for(std::chrono::milliseconds(1));
#endif
            if(!IblBoc::RxDelayOK(slaveId * 24 + fibre)) {
               // std::cout << "WARNING: fine delay on fibre " << (int) fibre << " is busy or stuck. Delay may not be changed!" << std::endl;
            }
		} else {
			std::cout << "You attempted to set RxSampleDelay on a IBL card. Skipping." << std::endl;
		}
	}
}

/*
 * check if delay setting is OK
 * Arguments:
 *        fibre: a BOC global fibre number (0...47) not the RX channel!!
 * Returns: true if delay is OK
 */
bool IblBoc::RxDelayOK(int8_t fibre)
{
    // range check
    if((fibre < 0) || (fibre > 47)) {
        std::cout << "Fibre " << fibre << " is out of range! Returning false in RxDelayOK." << std::endl;
        return false;
    }

    // get slaveId and fibre on that slave
    uint8_t slaveId = fibre / 24;
    fibre = fibre % 24;

    // check if it is an IBL card
    if(!IblBoc::IsPixelRx(slaveId)) {
        std::cout << "You are running RxDelayOK on an IBL card. Returning false." << std::endl;
        return false;
    }

    // check delay OK
    if((IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2 * fibre) & 0x30) != 0x20) {
        return false;
    } else {
        return true;
    }
}

/*
 * This function returns the RX sampling point for the pixel firmware
 * Arguments:
 *        fibre: a BOC global fibre number (0...47) not the RX channel!!
 *               a value < 0 will broadcast the threshold to all inputs
 *        sampling: select the sampling point (0...7)
 *        delay: IODELAY2 value (will add delay * 35 ps to the signal, to be confirmed)
 */
void IblBoc::GetRxSampleDelay(int8_t fibre, uint8_t& sampling, uint8_t& delay)
{
		// get slaveId
		uint8_t slaveId = fibre / 24;
		fibre = fibre % 24;

		// check if the slave is a Pixel slave
		if(IblBoc::IsPixelRx(slaveId) == true) {
			// write sampling point and delay
			sampling = IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2 * fibre) & 0x7;
			delay = IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2 * fibre + 1);
		} else {
			std::cout << "You attempted to set RxSampleDelay on a IBL card. Skipping." << std::endl;
		}
}


void IblBoc::CDRManPhase(int fibre, uint8_t phase)
{
// range check
    if((fibre < 0) || (fibre > 47)) {
        std::cout << "Fibre " << fibre << " is out of range! Returning false in RxDelayOK." << std::endl;
        return;
    }

    // get slaveId and fibre on that slave
    uint8_t slaveId = fibre / 24;
    fibre = fibre % 24;

    // check for IBL firmware
    if(!IblBoc::IsIblRx(slaveId)){
        std::cout << "The BMF firmware is not an IBL firmware. Stopping CDRManPhase." << std::endl;
        return;
    }

    // select phase and override
    uint8_t tmp = IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre);
    IblBoc::write(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre, (tmp & ~0x1C) | 0x10 | ((phase & 0x3) << 2));
 
}

uint8_t IblBoc::CDRGetPhase(int fibre)
{
    // range check
    if((fibre < 0) || (fibre > 47)) {
        std::cout << "Fibre " << fibre << " is out of range! Returning false in RxDelayOK." << std::endl;
        return 0;
    }

    // get slaveId and fibre on that slave
    uint8_t slaveId = fibre / 24;
    fibre = fibre % 24;

    // check for IBL firmware
    if(!IblBoc::IsIblRx(slaveId))
    {
        std::cout << "The BMF firmware is not an IBL firmware. Stopping CDRGetPhase." << std::endl;
        return 0;
    }

    return (IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre) >> 2) & 0x3;
}


void IblBoc::CDRAutoPhase(int fibre)
{
// range check
    if((fibre < 0) || (fibre > 47)) {
        std::cout << "Fibre " << fibre << " is out of range!" << std::endl;
        return;
    }

    // get slaveId and fibre on that slave
    uint8_t slaveId = fibre / 24;
    fibre = fibre % 24;

    // check for IBL firmware
    if(!IblBoc::IsIblRx(slaveId)){
        std::cout << "The BMF firmware is not an IBL firmware. Stopping CDRAutoPhase." << std::endl;
        return;
    }

    // Set phases to auto
    uint8_t tmp = IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre);
    IblBoc::write(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre, tmp & ~0x10 );
}

void IblBoc::CDRManPhase(int fibre)
{
// range check
    if((fibre < 0) || (fibre > 47)) {
        std::cout << "Fibre " << fibre << " is out of range!" << std::endl;
        return;
    }

    // get slaveId and fibre on that slave
    uint8_t slaveId = fibre / 24;
    fibre = fibre % 24;

    // check for IBL firmware
    if(!IblBoc::IsIblRx(slaveId)){
        std::cout << "The BMF firmware is not an IBL firmware. Stopping CDRAutoPhase." << std::endl;
        return;
    }

    //Set  phases to Manual, note that the phase is not set!!!
    uint8_t tmp = IblBoc::read(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre);
    IblBoc::write(IblBoc::getRegister(slaveId, CdrBase) + 2*fibre, tmp | 0x10 );
}

/*
 * Get the feature register from each slave
 * Argument:
 *		slaveId: which of the two slaves?
 * Return:
 *		binary value for the feature register, currently (13.10.2015) the
 *		mapping of the bits is following:
 *			0: always 1
 *			1: 1 if the firmware has an RX mux (usually)
 *			2: 1 if it is using a non-standard IBL RX (was for testing with Bern)
 *			3: 1 if the IBL CDR is the RCE version (usually)
 *			4: 1 if it is a Pixel RX, 0 if it is an IBL RX
 *			15:5: currently unused spares
 */
uint16_t IblBoc::GetFeatures(uint8_t slaveId)
{
	uint16_t ret = 0;

	ret |= IblBoc::read(slaveId, FeatureLow);
	ret |= (uint16_t) IblBoc::read(slaveId, FeatureLow) << 8;

	return ret;
}

/*
 * Check if slave is Pixel
 *
 * Argument:
 *		slaveId: which of the two slaves?
 * Return:
 *		true if PixelRX
 */
bool IblBoc::IsPixelRx(uint8_t slaveId)
{
	if(IblBoc::GetFeatures(slaveId) & 0x10)
		return true;
	else
		return false;
}

/*
 * Check if slave is IBL
 *
 * Argument:
 *		slaveId: which of the two slaves?
 * Return:
 *		true if IBLRx
 */
bool IblBoc::IsIblRx(uint8_t slaveId)
{
	if(IblBoc::GetFeatures(slaveId) & 0x10)
		return false;
	else
		return true;
}

/*
 * set speed for L1/L2 operation
 */
void IblBoc::PixSetSpeed(PixSpeed speed)
{
	IblBoc::PixSetSpeed(0, speed);
	IblBoc::PixSetSpeed(1, speed);
}

/*
 * set speed for L1/L2 operation for a given slaveId
 */
void IblBoc::PixSetSpeed(uint8_t slaveId, PixSpeed speed)
{
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to set Speed on an IBL card. Skipping." << std::endl;
	} else {
		IblBoc::write(slaveId, Speed, (uint8_t) speed);
	}
}

/*
 * set speed for L12 operation based on the MCC bandwidth enum
 * slaveMask = 0 or 1 will do a single slave, any number above that will do both
 */

void IblBoc::PixSetSpeed(PixLib::EnumMccBandwidth::MccBandwidth mccBandwidth, uint8_t slaveMask) {
  IblBoc::PixSpeed rxSpeed = PixSpeedFromMccBandwidth(mccBandwidth);
  if( slaveMask & 0x1 ) IblBoc::PixSetSpeed(0, rxSpeed );
  if( slaveMask & 0x2 ) IblBoc::PixSetSpeed(1, rxSpeed );
}


IblBoc::PixSpeed IblBoc::PixSpeedFromMccBandwidth(PixLib::EnumMccBandwidth::MccBandwidth mccBandwidth) {
  // MCC Bandwidth bits: SINGLE_40 (00), DOUBLE_40 (01), SINGLE_80 (10), DOUBLE_80 (11)
  // BOC Speed settings: Speed_40 (00 or 11), Speed_80 (01), Speed_160 (10)
  IblBoc::PixSpeed speed = IblBoc::Speed_40;
  switch (mccBandwidth) {
  	case PixLib::EnumMccBandwidth::SINGLE_40:
  	  speed = IblBoc::Speed_40;
  	  break;
  	case PixLib::EnumMccBandwidth::DOUBLE_40:
  	  speed = IblBoc::Speed_40_Alt;
  	  break;
  	case PixLib::EnumMccBandwidth::SINGLE_80:
  	  speed = IblBoc::Speed_80;
  	  break;
  	case PixLib::EnumMccBandwidth::DOUBLE_80:
  	  speed = IblBoc::Speed_160;
  	  break;
  	default:
  	  std::cout<<"Speed "<<(int)mccBandwidth<<" not found, falling back to 40 Mbps"<<std::endl;
  	break;
  }
  return speed;
}


PixLib::EnumMccBandwidth::MccBandwidth IblBoc::PixGetMccBandwidthFromPixSpeed(IblBoc::PixSpeed speed){
// MCC Bandwidth bits: SINGLE_40 (00), DOUBLE_40 (01), SINGLE_80 (10), DOUBLE_80 (11)
  // BOC Speed settings: Speed_40 (00 or 11), Speed_80 (01), Speed_160 (10)
  switch (speed) {
    case IblBoc::Speed_40:
      return PixLib::EnumMccBandwidth::SINGLE_40;
    break;
    case IblBoc::Speed_40_Alt:
      return PixLib::EnumMccBandwidth::DOUBLE_40;
    break;
    case IblBoc::Speed_80:
      return PixLib::EnumMccBandwidth::SINGLE_80;
    break;
    case IblBoc::Speed_160:
      return PixLib::EnumMccBandwidth::DOUBLE_80;
    break;
    default:
      std::cout<<"Speed "<<(int)speed<<" not found, falling back to 40 Mbps"<<std::endl;
      return PixLib::EnumMccBandwidth::SINGLE_40;
    break;
  }

}


/*
 * get the speed setting for L1/L2 operation from a slaveId
 */
IblBoc::PixSpeed IblBoc::PixGetSpeed(uint8_t slaveId)
{
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to get speed from an IBL card. Returning dummy value." << std::endl;
		return IblBoc::Speed_40;		// dummy value (it's IBL)
	} else {
		return (IblBoc::PixSpeed) IblBoc::read(slaveId, IblBoc::Speed);
	}
}

/*
 * Returns the fibre number (2...45) based on the RX channel number
 * Rx1 has fibres 0..11 and Rx2 has fibres 12..23
 * The BMF North hosts fibres from 0..23 and BMF South from 24..47
 *
 */

uint8_t IblBoc::getFibreFromRxCh(uint8_t rxCh) {
	uint8_t fibreNr = 2 + rxCh%8;	//2 = OFFSET spare channels
	fibreNr += 12 * (rxCh/8);
	return fibreNr;
}


//JuanAn: Why not ask directly to the BOC?
uint8_t IblBoc::getMasterFibreFromRxCh(uint8_t rxCh) {
	
	uint8_t fibreNr = IblBoc::read(rxCh/16, rxCh%16, IblBoc::FibreMaster) + 24 * (rxCh/16);

	return fibreNr;
}

uint8_t IblBoc::getSlaveFibreFromRxCh(uint8_t rxCh) {
	uint8_t fibreNr = IblBoc::read(rxCh/16, rxCh%16, IblBoc::FibreSlave) + 24 * (rxCh/16);

	return fibreNr;
}

//JuanAn set master fiber
void IblBoc::setMasterFibre(uint8_t rxCh, uint8_t fibreNr){

	IblBoc::write(rxCh/16, rxCh%16, IblBoc::FibreMaster, fibreNr%24);

}

//JuanAn set slave fiber
void IblBoc::setSlaveFibre(uint8_t rxCh, uint8_t fibreNr){

	IblBoc::write(rxCh/16, rxCh%16, IblBoc::FibreSlave, fibreNr%24);

}

void IblBoc::setMasterAndSlaveFibre(uint8_t rxCh, uint8_t masterFibre, uint8_t slaveFibre){
   IblBoc::setMasterFibre(rxCh, masterFibre);
   IblBoc::setSlaveFibre(rxCh, slaveFibre);
}

/*
*	enable the multiplexing on the BOC.
*	One Rx channel is multiplexed to all the RX channels in the same FPGA.
*	The Fibre is input number
*/

bool IblBoc::EnableFibreMux(uint8_t slaveId, uint8_t fibreNr, uint32_t channels) {

  if(fibreNr/24 != slaveId) {
  	
    std::cout << "ERROR: The fibre you pointed: " << fibreNr << " doesn't belong to the slaveId: " << slaveId << std::endl;
    std::cout << "ERROR: Multiplexer not enabled. No BMF registers have been touched" << std::endl;
    return false;

  }else {

    for (int i = 0 ; i < 16; i++) 
    	if (channels & (1<<(slaveId*16+i))) {
      	IblBoc::write(slaveId, i, IblBoc::FibreMaster, fibreNr%24);
      	std::cout << "WARNING: Enabling the MUX for slave "<< (int)slaveId << " in rx channel"<< i <<" with fibre number " << (int)fibreNr%24 << std::endl;
      	uint8_t rxControl = IblBoc::read(slaveId, i%16, IblBoc::RxControl);
      	rxControl |= 0x01; // enables the Rx channel on the BOC (this does not forwad the data to the ROD...this should be done in different functions)
      	rxControl &= 0xEF; // set the input of the RX channel to the SNAP12 fibers (this is usually the default value)
      	IblBoc::write(slaveId, i, IblBoc::RxControl, rxControl);
    }
    return true;

  }
}

/*
 * enable the compare unit for L1/L2
 */
void IblBoc::PixCompareEnable(uint8_t rxCh) {
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to enable the compare unit on an IBL card. Skipping." << std::endl;
		return;
	}

	// make sure monitor RAW mode is enabled
	uint8_t tmp = IblBoc::read(slaveId, ch, RxControl);
	tmp |= 0x08;
	IblBoc::write(slaveId, ch, RxControl, tmp);

	// enable compare unit
	tmp = IblBoc::read(slaveId, ch, CompareControl);
	tmp |= 0x1;
	IblBoc::write(slaveId, ch, CompareControl, tmp);
}

/*
 * disable compare unit
 */
void IblBoc::PixCompareDisable(uint8_t rxCh)
{
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to disable the compare unit on an IBL card. Skipping." << std::endl;
		return;
	}

  	// disable
	uint8_t tmp = IblBoc::read(slaveId, ch, CompareControl);
	tmp &= ~0x1;
	IblBoc::write(slaveId, ch, CompareControl, tmp);
}

void IblBoc::PixCompareGetCounters(uint8_t rxCh, uint32_t &nBits, uint32_t &nErrors)
{
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to read the compare unit counters on an IBL card. Skipping." << std::endl;
		nBits = 0; nErrors = 0;
		return;
	}

  	// dummy write to reset counter readout
  	IblBoc::write(slaveId, ch, CompareCounters, 0x00);

  	// readout values
  	nBits = (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 0;
    nBits |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 8;
    nBits |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 16;
    nBits |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 24;
    nErrors = (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 0;
    nErrors |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 8;
    nErrors |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 16;
    nErrors |= (uint32_t) IblBoc::read(slaveId, ch, CompareCounters) << 24;
}

void IblBoc::PixCompareLoad(uint8_t rxCh, std::vector<uint8_t> &Pattern, uint8_t Mask)
{
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to load pattern to compare unit on an IBL card. Skipping." << std::endl;
		return;
	}

  	// reset write pointer
	uint8_t tmp = IblBoc::read(slaveId, ch, CompareControl);
	IblBoc::write(slaveId, ch, CompareControl, tmp | 0x4);
	IblBoc::write(slaveId, ch, CompareControl, tmp & ~0x4);

	// set mask
	IblBoc::write(slaveId, ch, CompareMask, Mask);

	// write the pattern
	for(size_t i = 0; i < Pattern.size(); i++)
	{
		IblBoc::write(slaveId, ch, ComparePattern, Pattern[i]);
	}
}

void IblBoc::PixCompareLoad(uint8_t rxCh, std::vector<uint8_t> &Pattern, std::vector<uint8_t> &Mask)
{
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to load pattern to compare unit on an IBL card. Skipping." << std::endl;
		return;
	}

	// check size of Mask and pattern
	if(Pattern.size() != Mask.size()) {
		std::cout << "Pattern size and Mask size do not match. Skipping." << std::endl;
		return;
	}

  	// reset write pointer
	uint8_t tmp = IblBoc::read(slaveId, ch, CompareControl);
	IblBoc::write(slaveId, ch, CompareControl, tmp | 0x4);
	IblBoc::write(slaveId, ch, CompareControl, tmp & ~0x4);

	// write the pattern
	for(size_t i = 0; i < Pattern.size(); i++)
	{
		IblBoc::write(slaveId, ch, CompareMask, Mask[i]);
		IblBoc::write(slaveId, ch, ComparePattern, Pattern[i]);
	}
}

void IblBoc::PixCompareClearCounters(uint8_t rxCh)
{
	// get slave and channel numbers
	uint8_t slaveId = rxCh/16;
  	uint8_t ch = rxCh%16;

	// check for IBL card
	if(!IblBoc::IsPixelRx(slaveId)) {
		std::cout << "You attempted to disable the compare unit on an IBL card. Skipping." << std::endl;
		return;
	}

	uint8_t tmp = IblBoc::read(slaveId, ch, CompareControl);
	IblBoc::write(slaveId, ch, CompareControl, tmp | 0x02);//disable
	IblBoc::write(slaveId, ch, CompareControl, tmp & ~0x02);
}

void IblBoc::PixSetTrailerLength(uint8_t rxCh, uint16_t trailerLength) {

  uint8_t slaveId = rxCh/16;
  uint8_t ch = rxCh%16;
  IblBoc::write(slaveId, ch, TrailerLengthLow, trailerLength & 0xFF);
  IblBoc::write(slaveId, ch, TrailerLengthHigh, trailerLength >> 8);

}

uint16_t IblBoc::PixGetTrailerLength(uint8_t rxCh) {

  uint8_t slaveId = rxCh/16;
  uint8_t ch = rxCh%16;
  return (IblBoc::read(slaveId, ch, TrailerLengthHigh) << 8) | IblBoc::read(slaveId, ch, TrailerLengthLow);

}

uint8_t IblBoc::runPrimitive(BcfPrimitive p, uint8_t arg[4], uint8_t ret[4]) {

  uint8_t to = 255;
  while( (--to) && IblBoc::read(PrimProcOp) ) std::this_thread::sleep_for(std::chrono::microseconds(100));
  if(to == 0) { std::cout << __PRETTY_FUNCTION__ << ": timeout!" << std::endl; return 1; }

  BcfReg primArg[] = { PrimProcArg0, PrimProcArg1, PrimProcArg2, PrimProcArg3 };
  for(size_t i = 0 ; i < 4 ; ++i) {
    IblBoc::write(42, 0x42);
    IblBoc::write(primArg[i], arg[i]);
  }
  IblBoc::write(42, 0x42);
  IblBoc::write(PrimProcOp, p);

  to = 255;
  while( (--to) && IblBoc::read(PrimProcOp) )  std::this_thread::sleep_for(std::chrono::microseconds(100));
  if(to == 0) { std::cout << __PRETTY_FUNCTION__ << ": timeout!" << std::endl; return 1; }

  BcfReg primRet[] = { PrimProcRet0, PrimProcRet1, PrimProcRet2, PrimProcRet3 };
  for(size_t i = 0 ; i < 4 ; ++i) ret[i] = IblBoc::read(primRet[i]);

  std::cout << __PRETTY_FUNCTION__ << ": ";
  for(size_t i = 0 ; i < 4 ; ++i) std::cout << static_cast<uint32_t>(ret[i]) << "\t";
  std::cout << std::endl;

  return 0;
}

void IblBoc::getIpAddress(uint8_t ipAddr[4]) {
  uint8_t ipArg[4];
  IblBoc::runPrimitive(PrimGetIp, ipArg, ipAddr);
}

void IblBoc::ResetSlinkCounters(uint8_t slaveId)
{
	IblBoc::write(slaveId, IblBoc::SlinkCounters, 0xAB);
}

void IblBoc::ResetSlinkCounters()
{
	// reset both slave slink counters
	IblBoc::ResetSlinkCounters(0);
	IblBoc::ResetSlinkCounters(1);
}

// Note: need to provide the proper offset in SLinkCounter (see blBoc::GetSlinkCounters(SLinkInfo& sLinkInfo) below)
void IblBoc::GetSlinkCounters(uint8_t slaveId, SLinkCounter sLinkCounter[]) {
	// dummy write to activate counter readout
	IblBoc::write(slaveId, IblBoc::SlinkCounters, 0x00);

	// read out the counters (shifted out from LSB -> MSB for each counter)
	sLinkCounter[0].Hola[0].Lff    = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[0].Hola[0].Lff   |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[0].Hola[0].LDown  = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[0].Hola[0].LDown |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[0].Hola[1].Lff    = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[0].Hola[1].Lff   |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[0].Hola[1].LDown  = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[0].Hola[1].LDown |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[1].Hola[0].Lff    = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[1].Hola[0].Lff   |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[1].Hola[0].LDown  = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[1].Hola[0].LDown |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[1].Hola[1].Lff    = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[1].Hola[1].Lff   |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
	sLinkCounter[1].Hola[1].LDown  = ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 0);
	sLinkCounter[1].Hola[1].LDown |= ((uint16_t) IblBoc::read(slaveId, IblBoc::SlinkCounters) << 8);
}

void IblBoc::GetSlinkCounters(SLinkInfo& sLinkInfo) {
	IblBoc::GetSlinkCounters(0, &sLinkInfo.sLinkCounters[0]);
	IblBoc::GetSlinkCounters(1, &sLinkInfo.sLinkCounters[2]);
}

// Note: to be devided by 65535. to get fraction
uint16_t IblBoc::GetLinkOccMean(uint32_t rxCh) {
  uint8_t slaveId = rxCh/16;
  uint8_t ch = rxCh%16;
  uint16_t linkOccMean = ((uint16_t) IblBoc::read(slaveId, ch, IblBoc::LinkOccMeanLow) << 0);
  linkOccMean |= ((uint16_t) IblBoc::read(slaveId, ch, IblBoc::LinkOccMeanHigh) << 8);
  return linkOccMean;
}

// Note: to be devided by 65535. to get fraction
uint16_t IblBoc::GetLinkOccLive(uint32_t rxCh) {
  uint8_t slaveId = rxCh/16;
  uint8_t ch = rxCh%16;
  uint16_t linkOccLive = ((uint16_t) IblBoc::read(slaveId, ch, IblBoc::LinkOccLiveLow) << 0);
  linkOccLive |= ((uint16_t) IblBoc::read(slaveId, ch, IblBoc::LinkOccLiveHigh) << 8);
  return linkOccLive;
}

//New ECR functions
void IblBoc::ECRWriteBitStream(uint8_t slaveId, uint8_t ch,const std::vector <uint8_t>&  bitstream){

    // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return;
    }

    // reset bitStream data address
    IblBoc::write(slaveId, ch, IblBoc::ECRAddressLow, 0x0);
    IblBoc::write(slaveId, ch, IblBoc::ECRAddressHigh, 0x0);

    //Write data
    for(size_t i=0;i<bitstream.size(); i++)
        IblBoc::writeOnly(slaveId, ch, IblBoc::ECRData, bitstream[i]);

    // update bitstream length
    uint8_t tmp = IblBoc::read(slaveId,ch,IblBoc::ECRControl);
    IblBoc::write(slaveId, ch,IblBoc::ECRControl, tmp | 0x04);

}

void IblBoc::ECRReadBitStream(uint8_t slaveId, uint8_t ch, std::vector<uint8_t> &bitstream){

  // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
    }

   uint8_t tmp = IblBoc::read(slaveId, ch,IblBoc::ECRControl);
   IblBoc::write(slaveId, ch,IblBoc::ECRControl, tmp | 0x08);
   uint16_t bitstream_len = (IblBoc::read(slaveId, ch,IblBoc::ECRAddressLow) | (IblBoc::read(slaveId, ch,IblBoc::ECRAddressHigh) << 8));
   
    // reset bitStream data address
    IblBoc::write(slaveId, ch, IblBoc::ECRAddressLow, 0x0);
    IblBoc::write(slaveId, ch, IblBoc::ECRAddressHigh, 0x0);

    // read the data
    for(uint16_t i=0;i<bitstream_len; i++)
      bitstream.push_back(IblBoc::read(slaveId, ch, IblBoc::ECRData));
}

void IblBoc::ECREnable(uint8_t slaveId, uint8_t ch, bool enExtTrigger, bool enManTrigger)
{

    // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return;
    }
    uint8_t tmp = IblBoc::read(slaveId, ch,IblBoc::ECRControl) & 0x20;

    if(enExtTrigger)    tmp |= 0x01;
    if(enManTrigger)    tmp |= 0x02;

    IblBoc::write(slaveId, ch, IblBoc::ECRControl, tmp);
}


void IblBoc::ECRDisable(uint8_t slaveId, uint8_t ch)
{
    // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return;
    }

    //uint8_t tmp = IblBoc::read(slaveId, ch,IblBoc::ECRControl);
    //IblBoc::write(slaveId, ch, IblBoc::ECRControl,tmp & 0x20);
    IblBoc::write(slaveId, ch, IblBoc::ECRControl, 0x0);
}

bool IblBoc::ECRIsEnabled(uint8_t slaveId, uint8_t ch)
{

// ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return false;
    }

        return ((IblBoc::read(slaveId, ch, IblBoc::ECRControl) & 0x03) != 0);
}

void IblBoc::ECRSetVeto(uint8_t slaveId, uint8_t ch, bool veto){
// ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return;
    }

 uint8_t tmp = IblBoc::read(slaveId, ch,IblBoc::ECRControl);
    if(veto) tmp &= ~0x20;
    else tmp |= 0x20;

    IblBoc::write(slaveId, ch,IblBoc::ECRControl, tmp);

}

bool IblBoc::ECRisVetoActive(uint8_t slaveId, uint8_t ch){
// ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return false;
    }

 return !(IblBoc::read(slaveId, ch,IblBoc::ECRControl) & 0x20);

}

bool IblBoc::ECRHasConfigured(uint8_t slaveId, uint8_t ch){
// ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return false;
    }

 return IblBoc::read(slaveId, ch,IblBoc::ECRStatus) & 0x4;

}


void IblBoc::ECRManTrigger(uint8_t slaveId, uint8_t ch)
{

// ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return;
    }

    uint8_t tmp = IblBoc::read(slaveId, ch, IblBoc::ECRControl);
    IblBoc::write(slaveId, ch, IblBoc::ECRControl, tmp | 0x80);
}

bool IblBoc::ECRBusy(uint8_t slaveId, uint8_t ch)
{
    // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return false;
    }

    return IblBoc::read(slaveId, ch,  IblBoc::ECRStatus) & 0x1;
}

bool IblBoc::ECRCollision(uint8_t slaveId, uint8_t ch)
{

    // ECR works only for IBL
    if((IblBoc::GetFeatures(slaveId) & 0x8) == 0) {
        std::cout << "ECRWriteGRegs only works on IBL card with BitStream Configuration" << std::endl;
        return false;
    }

   return IblBoc::read(slaveId, ch, IblBoc::ECRStatus) & 0x2;
}


time_t IblBoc::getBuildDateBmf(uint8_t slaveId){


unsigned int year,month,day, hour, minute,seconds;

    year = (unsigned int) IblBoc::read(slaveId, IblBoc::BmfBuildYear) + 2000;
    month = (unsigned int)IblBoc::read(slaveId, IblBoc::BmfBuildMonth);
    day = (unsigned int) IblBoc::read(slaveId, IblBoc::BmfBuildDay);
    hour = (unsigned int) IblBoc::read(slaveId, IblBoc::BmfBuildHour);
    minute = (unsigned int) IblBoc::read(slaveId, IblBoc::BmfBuildMinute);
    seconds = (unsigned int) IblBoc::read(slaveId, IblBoc::BmfBuildSecond);

struct tm timeinfo;

timeinfo.tm_year   = year - 1900;
timeinfo.tm_mon    = month - 1;    //months since January - [0,11]
timeinfo.tm_mday   = day;          //day of the month - [1,31] 
timeinfo.tm_hour   = hour;         //hours since midnight - [0,23]
timeinfo.tm_min    = minute;          //minutes after the hour - [0,59]
timeinfo.tm_sec    = seconds;          //seconds after the minute - [0,59]

time_t date = mktime ( &timeinfo );

//std::cout<<"Build date BMF: "<<(unsigned int)slaveId<<" "<<day<<"-"<<month<<"-"<<year<<" "<<" "<<hour<<":"<<minute<<":"<<seconds<<" TimeStamp: "<<(uint32_t)date<<std::endl;

return date;

}

time_t IblBoc::getBuildDateBcf(){

unsigned int year,month,day, hour, minute,seconds;

    year = (unsigned int) IblBoc::read(IblBoc::BcfBuildYear) + 2000;
    month =(unsigned int)IblBoc::read(IblBoc::BcfBuildMonth);
    day = (unsigned int) IblBoc::read(IblBoc::BcfBuildDay);
    hour = (unsigned int) IblBoc::read(IblBoc::BcfBuildHour);
    minute = (unsigned int) IblBoc::read(IblBoc::BcfBuildMinute);
    seconds = (unsigned int) IblBoc::read(IblBoc::BcfBuildSecond);

struct tm timeinfo;

timeinfo.tm_year   = year - 1900;
timeinfo.tm_mon    = month - 1;    //months since January - [0,11]
timeinfo.tm_mday   = day;          //day of the month - [1,31]
timeinfo.tm_hour   = hour;         //hours since midnight - [0,23]
timeinfo.tm_min    = minute;          //minutes after the hour - [0,59]
timeinfo.tm_sec    = seconds;          //seconds after the minute - [0,59]

time_t date = mktime ( &timeinfo );

//std::cout<<"Build date BCF:"<<day<<"-"<<month<<"-"<<year<<" "<<" "<<hour<<":"<<minute<<":"<<seconds<<" TimeStamp: "<<(uint32_t)date<<std::endl;

return date;

}


void IblBoc::EnableTxMonitoring(uint8_t slaveId, uint8_t ch){

    uint8_t tmp = IblBoc::read(slaveId, ch, IblBoc::TxControl2);
    tmp |= 0x4;
    IblBoc::write(slaveId, ch, IblBoc::TxControl2, tmp);

}

void IblBoc::DisableTxMonitoring(uint8_t slaveId, uint8_t ch){

    uint8_t tmp = IblBoc::read(slaveId, ch, IblBoc::TxControl2);
    tmp &= ~0xC;
    IblBoc::write(slaveId, ch, IblBoc::TxControl2, tmp);

}


void IblBoc::getTxMonitoringCounters(uint8_t slaveId, uint8_t ch, uint32_t monCnt [7]){

    uint32_t tmp;

   for(int reg =0; reg<7;reg++){
    IblBoc::write(slaveId, ch, IblBoc::MonCnt, reg & 0x7);

    // read out (MSB to LSB)
    tmp = (uint32_t) IblBoc::read(slaveId, ch, IblBoc::MonCnt) << 24;
    tmp |= (uint32_t) IblBoc::read(slaveId, ch, IblBoc::MonCnt) << 16;
    tmp |= (uint32_t) IblBoc::read(slaveId, ch, IblBoc::MonCnt) << 8;
    tmp |= (uint32_t) IblBoc::read(slaveId, ch, IblBoc::MonCnt) << 0;

    monCnt [reg]= tmp;

    }


}



