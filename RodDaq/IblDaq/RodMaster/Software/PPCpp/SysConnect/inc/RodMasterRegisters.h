/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch
 * Date: 2013-VI-01
 * Updates: 
 *    6-April-2014
 *    L Jeanty <laura.jeanty@cern.ch>
 *    Updated to use generated .hxx files from firmware 
 */

#ifndef __ROD_MASTER_H__
#define __ROD_MASTER_H__

#include "HwRegsInCpp.h" // Keep this line first
#include <functional>
#include "MMapHandler.h"

#include <stdint.h>
#include <cassert>
#include <iostream>

#ifdef __XMK__
#include "rodMaster.hxx"
#include "rodMasterReg.hxx"

#include "rodSlaveReg.hxx"
#undef print

#define CE0_BASE XPAR_XPS_EPC_0_PRH0_BASEADDR
#define CTRL_REGS COMMON_REG_BASE


// Todo: (hard) enable reg_t on u32_t offsets from a given base
#define GEO_ADDR_REG COMMON_REG_BASE + PRM_GEO_ADDR_REG * sizeof(uint32_t)
#endif

#include "RodSlaveRegistersFromMaster.hxx" // Needs to be after all hxx includes
#include "RodMasterRegistersFromMaster.hxx" // Needs to be after all hxx includes

struct IblRod: public RodMasterRegistersFromMaster, public RodSlaveRegistersFromMaster {
private:
#ifndef __XMK__ // For emulator
// Redefining bits for UART in emulator
// Cannot take this from rodMaster.hxx because it causes other conflicts
// Todo (if ever?): include rodMaster.hxx in emulator and fix address issues
#define PPC_CTL_UARTA_BIT 26
#define PPC_CTL_UARTB_BIT 27
#define PPC_CTL_MASTER_BIT 31
#define PPC_CTL_HPIENABLE_BIT 30

	static uint32_t CE0_BASE;
    static uint32_t COMMON_REG_BASE;
    static uint32_t BOC_REG_BASE;
	static uint32_t PPC_CTL_REG;

	static uint32_t PPC_STAT_REG;
	static uint32_t PPC_SPORT_REG;
	static uint32_t CTRL_REGS;
	static uint32_t PPC_DESIGN_REG;

	static uint32_t PPC_WDOG_REG;

        static uint32_t PPC_BUSY_MASK_REG;
	static uint32_t PPC_BUSY_MASTER_STATUS_REG;
	static uint32_t PPC_BUSY_MASTER_OUT_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_BUSYNINP0_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_BUSYNINP1_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_FORMATTER0_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_FORMATTER1_HIST_REG;
	static uint32_t PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG;
	static uint32_t PPC_UART_S6A_STATUS_REG;
	static uint32_t PPC_UART_S6A_DATA_REG;
	static uint32_t PPC_UART_S6B_STATUS_REG;
	static uint32_t PPC_UART_S6B_DATA_REG;
	static uint32_t PPC_UART_MASTER_STATUS_REG;
	static uint32_t PPC_UART_MASTER_DATA_REG;

	static uint32_t SLV_A_FIFO_CH_SEL;
	static uint32_t SLV_B_FIFO_CH_SEL;
	static uint32_t INMEM_FIFO_RST;
	
	static uint32_t MASTER_GITID_REG;
	static uint32_t GEO_ADDR_REG;


	// Access to slave registers
	static uint32_t SLV_A_REG_BASE; // Offset for all SLV_A_XXX (in reality), but we instantiate one variable per register (for now)
	static uint32_t SLV_B_REG_BASE; // Offset for all SLV_B_XXX (in reality), but we instantiate one variable per register (for now)
#endif

public:


  typedef reg_t<uint32_t, CE0_BASE, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_slaveA > EpcBase; // iblRegisters.h
  typedef reg_t<uint32_t, COMMON_REG_BASE, 0xFFFFFFFF, 0, rw_t<uint32_t>,MMapHandler::mmap_common > EpcLocal; // rodMaster.hxx
  // Although this register is 8 bits, it's in a 32-bit namespace on the PPC
  typedef reg_t<uint32_t, BOC_REG_BASE, 0xFF, 0, rw_t<uint32_t>, MMapHandler::mmap_boc > EpcBoc; // rodMaster.hxx
  typedef reg_t<uint32_t, BOC_REG_BASE, 0xFF, 0, wo_t<uint32_t>, MMapHandler::mmap_boc > EpcBocWriteOnly; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_CTL_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalCtrl; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_CTL_REG, 0x00000001, PPC_CTL_UARTA_BIT, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalCtrlUartA;
  typedef reg_t<uint32_t, PPC_CTL_REG, 0x00000001, PPC_CTL_UARTB_BIT, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalCtrlUartB;
  typedef reg_t<uint32_t, PPC_CTL_REG, 0x00000001, PPC_CTL_MASTER_BIT, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalCtrlMasterBit;
  typedef reg_t<uint32_t, PPC_CTL_REG, 0x00000001, PPC_CTL_HPIENABLE_BIT, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalCtrlHpiEnable;

  typedef reg_t<uint32_t, PPC_STAT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalStat; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_SPORT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > EpcLocalSpMask; // rodMaster.hxx
  typedef reg_t<uint32_t, CTRL_REGS, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DspRegBase; // iblRegisters.h
  typedef reg_t<uint32_t, PPC_DESIGN_REG, 0xFFFFFFFF, 0, ro_t<uint32_t>, MMapHandler::mmap_ppc > DesignRegPPC; // rodMaster.hxx

  typedef reg_t<uint32_t, PPC_WDOG_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > WatchDog; // rodMaster.hxx
  typedef reg_t<uint32_t, GEO_ADDR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > PrmGeoAddr;
  typedef reg_t<uint32_t, PPC_BUSY_MASK_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyMask; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_STATUS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyCurrentStatus; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_OUT_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyOutHist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_BUSYNINP0_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyRodBusyNinP0Hist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_BUSYNINP1_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyRodBusyNinP1Hist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyEfbHeaderPauseHist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_FORMATTER0_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyFormatterMB0Hist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_FORMATTER1_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyFormatterMB1Hist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > MasterBusyRolTestPauseHist; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_BUSY_MASTER_OUT_HIST_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcBusyMasterOutHistReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_S6A_STATUS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartS6AStatusReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_S6A_DATA_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartS6ADataReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_S6B_STATUS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartS6BStatusReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_S6B_DATA_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartS6BDataReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_MASTER_STATUS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartMasterStatusReg; // rodMaster.hxx
  typedef reg_t<uint32_t, PPC_UART_MASTER_DATA_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > PpcUartMasterDataReg; // rodMaster.hxx
  typedef reg_t<uint32_t, MASTER_GITID_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_ppc > GitIdReg; // rodMaster.hxx
};

// Not using a namespace because otherwise the items would be instantiated multiple times
struct RodMaster {
public:
	static void initEPC();
	static void setFeCmdMask(uint8_t slaveId, uint16_t mask);
	static void setFeCmdMask(uint32_t);
};

#endif //__ROD_MASTER_H__
