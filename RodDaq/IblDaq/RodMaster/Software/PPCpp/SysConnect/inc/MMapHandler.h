#ifndef __MEMORYMAPHANDLER__
#define __MEMORYMAPHANDLER__

#include <cstdint>

struct mmap_info {
    intptr_t addr;
    uint32_t target;
    uint32_t pa_offset;
    uint32_t tot_length;
};


struct MMapHandler {

public:

    MMapHandler() {}
    ~MMapHandler() { mmap_clear();}
    static void mmap_init();
    static void mmap_clear();
    static uint32_t mmap_ppc(uint32_t reg);
    static uint32_t mmap_boc(uint32_t reg);
    static uint32_t mmap_common(uint32_t reg);
    static uint32_t mmap_slaveA(uint32_t reg);
    static uint32_t mmap_slaveB(uint32_t reg);
    static uint32_t mmap_sp(uint32_t reg);

private:
    static mmap_info mmap_info_ppc;
    static mmap_info mmap_info_boc;
    static mmap_info mmap_info_common;
    static mmap_info mmap_info_slaveA;
    static mmap_info mmap_info_slaveB;
    static mmap_info mmap_info_sp;
    static bool inited;
};

#endif
