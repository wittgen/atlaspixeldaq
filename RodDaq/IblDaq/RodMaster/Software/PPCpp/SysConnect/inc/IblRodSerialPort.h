/*
 * Author: K. Potmianos <karolos.potamianos@cern.ch>
 * Date: 2013-VI-10
 */

#ifndef __IBL_ROD_SERIAL_PORT_H__
#define __IBL_ROD_SERIAL_PORT_H__

#include "HwRegsInCpp.h" // Keep this line first
#include "RodMasterRegisters.h"
#include "TxLink.h"
#include "SerialPort.h"
#include "MMapHandler.h"

#include <cstdarg>

#undef write

//Todo: use templates when multiple serial ports template <uint32_t N>
struct IblRodSerialPort: virtual public SerialPort {
	// Todo: configure appropriately



#ifndef __XMK__
	//static uint32_t XPAR_MCBSP_ROD_0_BASEADDR[5];
	static uint32_t XPAR_MCBSP_ROD_0_BASEADDR_1;
	static uint32_t XPAR_MCBSP_ROD_0_BASEADDR_2;
	static uint32_t XPAR_MCBSP_ROD_0_BASEADDR_4;


	//This doesn't work: cannot appear in a constant-expression. To be fixed with C++11
        //typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR[1], 0xFFFFFFFF, 0, wo_t<uint32_t> > DataOne;
        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR_1, 0xFFFFFFFF, 0, wo_t<uint32_t>, MMapHandler::mmap_sp > Data;
        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR_2, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_sp > Ctrl;
        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR_4, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_sp > TxCtrl;
// Todo: fix HwRegsInCpp appropriately
#else

        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR+4, 0xFFFFFFFF, 0, wo_t<uint32_t>, MMapHandler::mmap_sp > Data;
        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR+8, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_sp > Ctrl;
        typedef reg_t <uint32_t, XPAR_MCBSP_ROD_0_BASEADDR+16, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_sp > TxCtrl;
#endif


	void init();
	int ready();

	void setTxMask(uint32_t m);
	void setTxCh(uint8_t ch);
        uint32_t getTxMask();
        uint32_t readsPort();

	void write(uint8_t size, uint32_t* values);
	//void write(uint8_t size, ...);
	void sendStream();

	// Implementing function from TxLink
	// Todo: move to implementation
	void write(uint32_t data) {
		this->write(1, &data);
	}

	void send() {
		this->sendStream();
	}
	
	void sendTestConfig();


	IblRodSerialPort();

  private:
  uint32_t m_txMask;
};

#endif
