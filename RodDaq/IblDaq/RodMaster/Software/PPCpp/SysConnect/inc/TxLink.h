#ifndef __TX_LINK__
#define __TX_LINK__

#undef write
#undef send

#include <stdint.h>

class TxLink {
public:
	virtual void write(uint32_t data) = 0; //! Function to write to the link's FIFO
	virtual void send() = 0; //! Function to flush the FIFO and serialize its contents

	// To do: cleanup inheritance
	virtual void setTxMask(uint32_t ch_mask) { }
	virtual void setTxCh(uint8_t tx_ch) { }
	virtual uint32_t getTxMask() {return 0;}
	virtual ~TxLink() { }
};

class DummyTxLink: public TxLink {
public:
	void write(uint32_t data) { }
	void send() { }
	uint32_t getTxMask() { return 0; };
	~DummyTxLink() { }
};

#endif // __TX_LINK__
