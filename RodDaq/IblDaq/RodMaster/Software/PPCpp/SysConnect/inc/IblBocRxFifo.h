#ifndef __IBL_BOC_RX_FIFO__
#define __IBL_BOC_RX_FIFO__

#include "RxLink.h"

#include <stdint.h>

class IblBocRxFifo: virtual public RxLink {

public:
	IblBocRxFifo();
	IblBocRxFifo(uint8_t rxCh);
	~IblBocRxFifo();

	uint32_t read();
	void reset();

};

#endif // __IBL_BOC_RX_FIFO__

