/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-V-5
 * Description: This class provides SPI access to the PRM.
 * Notes: this class is a mix between custom and generic code. Todo: calrify the situation
 */

#ifndef __XMK__
#define __PRM_SPI_DEV__
#endif

#ifndef __PRM_SPI_DEV__
#define __PRM_SPI_DEV__

#include "xspi.h"
#include "rodMaster.hxx"
#define PRM_SPI_DEVICE XPAR_SPI_1_DEVICE_ID

#include <vector>

class PrmSpiDev {
        XSpi m_Spi;
        XStatus m_Status;
        XSpi_Config *m_ConfigPtr;
        uint32_t m_StatusReg, m_ControlReg;

	uint16_t m_DeviceId;
	uint32_t m_WordCount;
	std::vector<uint32_t> m_spiData;

public:
	// Todo: remove hard-coded information
        PrmSpiDev(uint16_t deviceId, uint32_t wordCount);

        void init();

	uint8_t getVmeSlot();
};

#endif // __PRM_SPI_DEV__

