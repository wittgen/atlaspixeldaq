#ifndef __IBL_BOC_LINK_H__
#define __IBL_BOC_LINK_H__

#include <stdint.h>

class TxLink;

class IblBocLink {
public:
	IblBocLink();
	void setTxLink(TxLink *txLink);
	bool enableChannels(uint32_t rx_mask);
	bool enableTxChannels(uint32_t tx_mask);
	bool enableRxChannels(uint32_t rx_mask);
	bool enableTxChannel(uint8_t slaveId, uint8_t ch);
	bool enableRxChannel(uint8_t slaveId, uint8_t ch);

	bool checkRxChannel(uint8_t slaveId, uint8_t ch);

	void disableChannels(uint32_t rx_mask);
	void disableRxChannel(uint8_t slaveId, uint8_t ch);
	void disableRxChannels(uint32_t rx_mask);
	void disableTxChannel(uint8_t slaveId, uint8_t ch);
	void disableTxChannels(uint32_t tx_mask);
	
	void ECRVetoTxChannels(uint32_t tx_mask,bool veto);
	bool ECRBusyTxChannels(uint32_t tx_mask);
	
	bool resamplePhase(uint32_t rx_mask);
	
	/// Returns the map of active channels
	void setEnChMask(uint32_t m) { m_enChMask = m; }
	uint32_t getEnChMask() { return m_enChMask; }
	void setTxEnChMask(uint32_t m) { m_enTxChMask = m; }
	uint32_t getTxEnChMask() { return m_enTxChMask; }
	
private:

	TxLink *m_TxLink;
	uint32_t m_enChMask;
	uint32_t m_enTxChMask;
};

#endif //__IBL_BOC_LINK_H__
