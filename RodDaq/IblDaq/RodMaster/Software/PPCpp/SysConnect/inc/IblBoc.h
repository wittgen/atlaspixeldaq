/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2013-X-25
 * Remarks: could eventually import from BocLib instead of cherry-picking
 */

#ifndef __IBL_BOC_H__
#define __IBL_BOC_H__

#include "iblEnums.h"
#include "PixEnumBase.h"
#include <stdint.h>
#include <vector>

class IblBoc {
public:

	//Todo: template<uint8_t _sId, uint8_t _rxCh>
	class IBLFeEmu {
	public:
		static void init(uint8_t slaveId, uint8_t ch);
		static void reset(uint8_t slaveId, uint8_t ch);
		static void setHitCount(uint8_t slaveId, uint8_t ch, uint8_t c = 0xFF);

		//New FE-I4 emulator method July 2020
		// ToT and extra frames
                static uint8_t GetExtraFrames(uint8_t slaveId, uint8_t ch);
                static void SetExtraFrames(uint8_t slaveId, uint8_t ch,uint8_t Value);
                static uint8_t GetTotValue(uint8_t slaveId, uint8_t ch);
                static void SetTotValue(uint8_t slaveId, uint8_t ch, uint8_t Value);
                static uint8_t GetTot2Value(uint8_t slaveId, uint8_t ch);
                static void SetTot2Value(uint8_t slaveId, uint8_t ch,uint8_t Value);

                // Hit-Count LUT loading
                static void WriteLUT(uint8_t slaveId, uint8_t ch, const std::vector <uint8_t> & data);
                static void ReadLUT(uint8_t slaveId, uint8_t ch, std::vector <uint8_t> & data);
	};


	//Pixel front end emulator
	class PixFeEmu {
	public:
		static void init(uint8_t slaveId, uint8_t ch, uint8_t frameCount, uint8_t speed);
		static void reset(uint8_t slaveId, uint8_t ch);
		static void setHitCount(uint8_t slaveId, uint8_t ch, uint8_t c = 0xFF);
		static void MCCEmuConfig(uint8_t slaveId, uint8_t ch, uint32_t Config1, uint32_t Config2);
		// Hit-Count LUT loading
                static void WriteLUT(uint8_t slaveId, uint8_t ch, const std::vector <uint8_t> & data);
                static void ReadLUT(uint8_t slaveId, uint8_t ch, std::vector <uint8_t> & data);
	};

	IblBoc();
	
	enum BocFpga { BmfNorth  = 0, BmfSouth  = 1, Bcf = 2 };
	enum BocRegType { GR = 0, Rx = 2, Tx = 3, FeEmu = 4 };

	enum SLinkCtrlBit { TestSel, TestEna, FTKXOffEna, FTKLDownEna, IntUResetN /* Active Low! */, HolaReset, MgtReset, ClkSel /* External is 1 */ };

	enum BcfReg { BcfFirmware, BcfBuildYear, BcfBuildMonth, BcfBuildDay, BcfBuildHour, BcfBuildMinute, BcfBuildSecond, LaserEnable, JtagOverride,
			SerialNumber0, SerialNumber1, SerialNumber2, SerialNumber3, SerialNumber4, SerialNumber5, SerialNumberStatus,
			PrimProcOp, PrimProcArg0, PrimProcArg1, PrimProcArg2, PrimProcArg3, PrimProcRet0, PrimProcRet1, PrimProcRet2, PrimProcRet3,
			VmeClkCsr = 0x020, VmeFreq0, VmeFreq1, VmeFreq2, VmeFreq3, BocRev, DcsHandshake, RebootReg, BcfState, MGTFreqSel, SecureReg,
			InterlockLossCnt, Spare, PllCtrl = 0x030, PllStatus, PllData0, PllData1, PllData2, PllData3, PllIdelay0, PllIdelay1,
			ProgNorthCrc0, ProgNorthCrc1, ProgNorthCrc2, ProgNorthCrc3, ProgSouthCrc0, ProgSouthCrc1, ProgSouthCrc2, ProgSouthCrc3,
			ProgNorthCtrl, ProgNorthStatus, ProgNorthDCNT, ProgNorthData, ProgNorthFladdr0, ProgNorthFladdr1, ProgNorthFladdr2,
			ProgSouthCtrl = 0x048, ProgSouthStatus, ProgSouthDCNT, ProgSouthData, ProgSouthFladdr0, ProgSouthFladdr1, ProgSouthFladdr2,
			BcfGitHash0 = 0x050, BcfGitHash1, BcfGitHash2, BcfGitHash3, BcfGitHash4, BcfGitHash5, BcfGitHash6, BcfGitHash7, BcfGitHash8, BcfGitHash9,
			BcfGitHash10, BcfGitHash11, BcfGitHash12, BcfGitHash13, BcfGitHash14, BcfGitHash15, BcfGitHash16, BcfGitHash17, BcfGitHash18, BcfGitHash19 };

	enum BcfPrimitive { PrimGetIp = 0x11 };

	enum BmfReg { BmfFirmware, BmfBuildYear, BmfBuildMonth, BmfBuildDay, BmfBuildHour, BmfBuildMinute, BmfBuildSecond, RxDebug = 0xA, SlinkCounters = 0xB,
			SLink0Ctrl = 0x10, SLink0Stat, SLink1Ctrl, SLink1Stat, FeatureLow = 0x14, FeatureHigh, RxPlugin0DAC_Csr = 0x16, RxPlugin0DAC_DataL, RxPlugin0DAC_DataH,
			RxPlugin1DAC_Csr, RxPlugin1DAC_DataL, RxPlugin1DAC_DataH, IcapCsr = 0x1C, IcapDataLow, IcapDataHigh, Speed = 0x1F,
			BmfGitHash0 = 0x20, BmfGitHash1, BmfGitHash2, BmfGitHash3, BmfGitHash4, BmfGitHash5, BmfGitHash6, BmfGitHash7, BmfGitHash8, BmfGitHash9,
			BmfGitHash10, BmfGitHash11, BmfGitHash12, BmfGitHash13, BmfGitHash14, BmfGitHash15, BmfGitHash16, BmfGitHash17, BmfGitHash18, BmfGitHash19,
			RxMuxSel0 = 0x38, RxMuxSel1, RxMuxSel2, RxMuxSel3, RxMuxSel4, RxMuxSel5, RxMuxSel6, RxMuxSel7, BmfSecureReg=0x4D, CdrBase = 0x50 };
	// Note: by default assuming IblRxChannel, see BmfPixRxReg for PixRxChannel (in Boc2 nomenclature), which provides overrides.
	//       Most common features are available from BmfRxReg.
	enum BmfTxReg { TxControl, TxStatus, FifoControl, FifoData, TxControl2, CoarseDelay, MonCnt, ECRControl = 0xA, ECRStatus = 0xB, ECRAddressLow  = 0xC, ECRAddressHigh = 0xD, ECRData  = 0xE };

	enum BmfRxReg { RxControl, RxStatus, DataHigh, DataLow, FrameCnt0, FrameCnt1, FrameCnt2, FrameCnt3, FrameErrCnt0, FrameErrCnt1, FrameErrCnt2, FrameErrCnt3, ExtraControl =0xF, LinkOccMeanLow = 0x18, LinkOccMeanHigh = 0x19, LinkOccLiveLow = 0x1A, LinkOccLiveHigh = 0x1B, ErrHistoCtrl = 0x1C, ErrHistoCount = 0x1D, RodSaverThresh = 0x1E};
	enum BmfPixRxReg {  FibreMaster = 0x4, FibreSlave = 0x5, TrailerLengthLow = 0x6, TrailerLengthHigh = 0x7, CompareControl = 0x8, ComparePattern = 0x9, CompareMask = 0xA, CompareCounters = 0xB, FrameCounter = 0xC, LutData =0xD, XCSel = 0xE, Control2 = 0xF, EmuConfig1 = 0x10, EmuConfig2 = 0x14 };

	enum BmfFeEmuReg { FeEmuControl, FeEmuStatus, L1ID_LSB, L1ID_MSB, BCID_LSB, BCID_MSB, ManualHitInjection, HitCountPerTrigger };

	enum BmfFeEmuRegLut { FeEmuControlLUT= 0x0, FeEmuStatusLUT= 0x1, ExtraFrames =0x2, TotValue= 0x3, IBLLutData= 0x4 };

	enum CounterId { LockLoss = 0x0, SyncLoss = 0x1, DecoError = 0x2, DisparityError = 0x3, FrameError = 0x4};

	enum PixSpeed { Speed_40 = 0x0, Speed_80 = 0x1, Speed_160 = 0x82, Speed_40_Alt = 0x3, Speed_160_Alt = 0x2};

	enum BmfPixFeEmuReg { PixEmuConfig1_0 = 0x10, PixEmuConfig1_1 = 0x11, PixEmuConfig1_2 = 0x12, PixEmuConfig1_3 = 0x13, PixEmuConfig2_0 = 0x14, PixEmuConfig2_1 = 0x15, PixEmuConfig2_2 = 0x16, PixEmuConfig2_3 = 0x17 };

// Todo: set function visibility
	static bool write(uint32_t regNum, uint8_t value);
	static bool writeOnly(uint32_t regNum, uint8_t value);
	static uint8_t read(uint32_t regNum);
	static const uint16_t getRegister(BocFpga bocFPGA, BocRegType regType, uint8_t ch, uint8_t regNum);

	static const uint16_t getRegister(const BcfReg regNum);
	static void write(BcfReg regNum, uint8_t value);
	static uint8_t read(BcfReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, const BmfReg regNum);
	static void write(uint8_t slaveId, const BmfReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, BmfReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum, uint8_t value);
	static void writeOnly(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfTxReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfRxReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum, uint8_t value);
	static void writeOnly(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfPixRxReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfFeEmuReg regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum, uint8_t value);
	static void writeOnly(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfFeEmuRegLut regNum);

	static const uint16_t getRegister(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum);
	static void write(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum, uint8_t value);
	static uint8_t read(uint8_t slaveId, uint8_t ch, const BmfPixFeEmuReg regNum);


	template <typename T>
	static void set(uint8_t slaveId, uint8_t ch, T regNum, uint8_t mask) {
		uint8_t regValue = IblBoc::read(slaveId, ch, regNum);
		regValue |= mask;
		IblBoc::write(slaveId, ch, regNum, regValue);
	}

	template <typename T>
	static void reset(uint8_t slaveId, uint8_t ch, T regNum, uint8_t mask) {
		uint8_t regValue = IblBoc::read(slaveId, ch, regNum);
		regValue &= ~mask;
		IblBoc::write(slaveId, ch, regNum, regValue);
	}

	static bool laserOn(bool doIt = true);
	static void dumpFwInfo();

	static const uint8_t GetVersion(uint8_t slaveId);

	static const bool isNewEmulator(uint8_t slaveId);

	static void UnlockSecureRegBmf(uint8_t slaveId);

	static void sLinkReset(uint8_t slaveId, uint8_t link, bool holaReset, bool mgtReset);
	static void sLinkEnable(uint8_t slaveId, uint8_t link, SLinkEnableMode mode = SlinkIgnoreFTK);

	static void FeDataInjection( uint8_t slaveId, uint8_t ch, const std::vector<uint8_t>& feData );

	static void ResetSlinkCounters(uint8_t slaveId);
	static void ResetSlinkCounters();

	struct HolaCounter {
		uint16_t Lff;
		uint16_t LDown;
	};

	struct SLinkCounter {
		HolaCounter Hola[2];
	};

	struct SLinkInfo {
		SLinkCounter sLinkCounters[4];
	};

	static void GetSlinkCounters(uint8_t slaveId, SLinkCounter sLinkCounter[]);
	static void GetSlinkCounters(SLinkInfo& sLinkInfo);

	//Note: output to be divided by std::numeric_limits<uint16_t>::max() == 65535. to get fraction
	static uint16_t GetLinkOccMean(uint32_t rxCh);
	static uint16_t GetLinkOccLive(uint32_t rxCh);

	static uint32_t GetFrameCount(uint32_t rx_ch);
	static uint32_t GetCounter(uint32_t rx_ch, CounterId id);
	static void ResetCounter(CounterId id);
	static void ResetAllCounters();	

	//new functions for TX control for pixel
	static void PixSetTxCoarseDelay(int8_t txCh, uint32_t coarseDelay);
	static uint32_t PixGetTxCoarseDelay(int8_t txCh);

	// new functions for RX control for pixel
	static void PixSetRxThreshold(int8_t fibre, uint16_t threshold);
	static void PixSetRxRGS(int8_t pluginId, uint16_t value);
	static void SetRxSampleDelay(int8_t fibre, uint8_t sampling, uint8_t delay = 0);

	static void GetRxSampleDelay(int8_t fibre, uint8_t& sampling, uint8_t& delay);	
	static bool RxDelayOK(int8_t fibre);

	static void CDRManPhase(int fibre, uint8_t phase);
	static uint8_t CDRGetPhase(int fibre);
	static void CDRAutoPhase(int fibre);
	static void CDRManPhase(int fibre);

	static uint16_t GetRxThreshold(uint8_t Fibre);
	static uint16_t GetRxRGS(uint8_t Plugin);

	// identify Pixel/IBL
	static uint16_t GetFeatures(uint8_t slaveId);
	static bool IsPixelRx(uint8_t slaveId);
	static bool IsIblRx(uint8_t slaveId);

	// set speed for BMF in L1/L2 operation
	static void PixSetSpeed(PixSpeed speed);
	static void PixSetSpeed(uint8_t slaveId, PixSpeed speed);
	static void PixSetSpeed( PixLib::EnumMccBandwidth::MccBandwidth mccBandwidth, uint8_t slaveMask = 0x3 );
	static PixSpeed PixSpeedFromMccBandwidth(PixLib::EnumMccBandwidth::MccBandwidth mccBandwidth);
	static PixSpeed PixGetSpeed(uint8_t slaveId);
	static PixLib::EnumMccBandwidth::MccBandwidth PixGetMccBandwidthFromPixSpeed(PixSpeed speed);

	// Returns the fibre number based on the RX channel number
	static uint8_t getFibreFromRxCh(uint8_t rxCh);
	static uint8_t getMasterFibreFromRxCh(uint8_t rxCh);
	static uint8_t getSlaveFibreFromRxCh(uint8_t rxCh);
	static bool EnableFibreMux(uint8_t slaveId, uint8_t fibreNr, uint32_t channels);

	//Sets master and slave fibers in the slave
	static void setMasterFibre(uint8_t rxCh, uint8_t fibreNr);
	static void setSlaveFibre(uint8_t rxCh, uint8_t fibreNr);
	static void setMasterAndSlaveFibre(uint8_t rxCh, uint8_t masterFibre, uint8_t slaveFibre);

	// pix compare unit functions
	static void PixCompareEnable(uint8_t rxCh);
	static void PixCompareDisable(uint8_t rxCh);
	static void PixCompareGetCounters(uint8_t rxCh, uint32_t &nBits, uint32_t &nErrors);
	static void PixCompareLoad(uint8_t rxCh, std::vector<uint8_t> &Pattern, uint8_t Mask = 0xFF);
	static void PixCompareLoad(uint8_t rxCh, std::vector<uint8_t> &Pattern, std::vector<uint8_t> &Mask);
	static void PixCompareClearCounters(uint8_t rxCh);
	static void PixSetTrailerLength(uint8_t rxCh, uint16_t trailerLength);
	static uint16_t PixGetTrailerLength(uint8_t rxCh);

	// Executes primitives processed by the BCF MB
	static uint8_t runPrimitive(BcfPrimitive p, uint8_t arg[4], uint8_t ret[4]);

	// Get IP address
	static void getIpAddress(uint8_t ipAddr[4]);

	//Date and version check
	static time_t getBuildDateBmf(uint8_t slaveId);
	static time_t getBuildDateBcf();

	//TxMonitoring  specific
        static void EnableTxMonitoring(uint8_t slaveId, uint8_t ch);
        static void DisableTxMonitoring(uint8_t slaveId, uint8_t ch);
        static void getTxMonitoringCounters(uint8_t slaveId, uint8_t ch, uint32_t monCnt [7]);
	
	// ECR
        static void ECRWriteBitStream(uint8_t slaveId, uint8_t ch, const std::vector <uint8_t>& bitstream);
        static void ECRReadBitStream(uint8_t slaveId, uint8_t ch,std::vector<uint8_t>& bitstream);
        static void ECREnable(uint8_t slaveId, uint8_t ch, bool enExtTrigger = true, bool enManTrigger = false);
        static void ECRDisable(uint8_t slaveId, uint8_t ch);
        static bool ECRBusy(uint8_t slaveId, uint8_t ch);
        static bool ECRCollision(uint8_t slaveId, uint8_t ch);
        static bool ECRIsEnabled(uint8_t slaveId, uint8_t ch);
        static void ECRManTrigger(uint8_t slaveId, uint8_t ch);
        static void ECRSetVeto(uint8_t slaveId, uint8_t ch, bool veto);
        static bool ECRisVetoActive(uint8_t slaveId, uint8_t ch);
        static bool ECRHasConfigured(uint8_t slaveId, uint8_t ch);

protected:

private:

};

#endif //__IBL_BOC_H__
