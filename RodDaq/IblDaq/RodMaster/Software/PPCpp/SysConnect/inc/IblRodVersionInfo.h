/*
 * Author: M. Kretz <moritz.kretz@cern.ch>
 * Date: 2015-V-08
 * Provide hardware and software versioning info for ROD master and slaves.
 * Not safe to use in multiple threads!
 */
#ifndef __IBL_ROD_VERSION_INFO_H__
#define __IBL_ROD_VERSION_INFO_H__

#include <stdint.h>

/* Macros to handle git hashes. */
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

class IblRodVersionInfo {
public:
    /** How many 32-bit words make up a 160 bit git hash. */
    static const int c_hashWords = 5;

    /** Struct containing git hash for easier handling. */
    typedef struct GitHash_t {uint32_t word[c_hashWords];} GitHash_t;

    /** Get git hash of slave software.
     * @param slaveId The slave ID, either 0 or 1. */
    static GitHash_t getSlaveHashSw(int slaveId);

    /** Get git hash of slave software.
     * @param slaveId The slave ID, either 0 or 1. */
    static GitHash_t getSlaveHashFw(int slaveId);

    /** Get git hash of master software. */
    static GitHash_t getMasterHashSw();

    /** Get git hash of master firmware. */
    static GitHash_t getMasterHashFw();

    /** Checks if all hashes correspond to the master SW hash.
     * @returns true if all hashes are consistent, false otherwise. */
    static bool checkConsistency();

    /** Prints out git hash and description.
     * @param hash The hash to be printed.
     * @param desc Description for this hash. */
    static void printHash(GitHash_t const& hash, std::string const& desc);

    /** Prints out the git hashes of ROD master and slaves. */
    static void printHashes();

private:
    /** Predicate that compares two hashes for identity.
     * @returns true if hashes are identical. */
    static bool isHashIdentical(GitHash_t const& a, GitHash_t const& b);
};

#endif // __IBL_ROD_VERSION_INFO_H__

