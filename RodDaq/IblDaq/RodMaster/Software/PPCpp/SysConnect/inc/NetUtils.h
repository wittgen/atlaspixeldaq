#ifndef SOCKETUTILS_H
#define SOCKETUTILS_H

#include <iosfwd>
#include <stdint.h>

void print_network_configuration();

//! get ron master network info
void getNetInfo(unsigned char *mac, uint32_t &ipaddr, uint32_t & netmask, uint32_t & gw);

#endif // SOCKETUTILS_H
