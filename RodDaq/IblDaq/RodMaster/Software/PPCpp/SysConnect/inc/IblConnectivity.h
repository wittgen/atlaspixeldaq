/* 
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-III-27
 * Description: this header defines 'hard-coded' parameters for the low-level layout.
 *	These parameters are provided for initialization and are overridden when running from PixLib.
 * 	Use ifdef flags to adjust to labs.
 */

#ifndef __IBL_CONNECTIVITY_H__
#define __IBL_CONNECTIVITY_H__

#include "RodResourceManager.h"
#include <iostream>

// Hard-coded DBM connectivity -- not used: just for reference
// Higher level tool available via RodResourceManagerIBL::updateConnectivity(Fei4*)
// which uses the Rx and Tx information stored in the Fei4ExtCfg class
/*
static uint8_t DbmTxMask[][2] = { // {Rx,Tx}
	// A-side
	{12, 4},{13, 5},{14, 6},	// M3 - TDBM-05
	{ 8, 4},{ 9, 5},{10, 6},	// M4 - TDBM-04
	{ 4, 1},{ 5, 1},{ 6 ,2},	// M1 - TSBM-01
	{ 0, 1},{ 1, 1},{ 2 ,2},	// M2 - TDBM-03
	// C-side
	{20,16},{21,19},{22,18},	// M2 - TSBM-02
	{16,16},{17,19},{18,18},	// M1 - TDBM-01
	{24,20},{25,21},{26,22},	// M4 - TDBM-06
	{28,20},{29,21},{30,22},	// M3 - TDBM-07 - as currently in FSM
};
 */

// I could have used a namespace but we might have this object templated in the future
struct IblConnectivity {
  
  /// This function provides a tx_mask with a SINGLE bit set, corresponding to the rxCh
  // This uses the module configs to determine the tx
  static uint32_t getTxMaskFromRxCh(uint8_t rxCh) {
    uint32_t txMask(0);
    for (uint32_t i = 0; i< RodResourceManagerIBL::getNModuleConfigs(); i++) {
      if (rxCh == i) {
	txMask |= (1<<((RodResourceManagerIBL::getCalibModuleConfig(i)).getTxCh()));
      }
    }
    return txMask;
  }

  /// This function computes the tx_mask that needs to be set in order to talk to the FEs enabled in rx_mask
  /// Note: this doesn't take the chipID into account. A TX channel is enabled as soon as one of the corresponding RX channels is enabled.
  /// This scheme doesn't require modifications for 'enabling channels' since it is assumed that one wants to broadcast to 'enable' links.
  static uint32_t getTxMaskFromRxMask(uint32_t rx_mask) {
    uint32_t tx_mask(0);
    for (uint32_t  i = 0; i< RodResourceManagerIBL::getNModuleConfigs(); i++) {
      if (rx_mask & (1<<i)) tx_mask |= IblConnectivity::getTxMaskFromRxCh(i);
    }
    return tx_mask;
  }
  
  static uint8_t getTxChFromRxCh(uint8_t rxCh) {
    uint32_t mask = getTxMaskFromRxCh(rxCh);
    uint8_t out = 0;
    while (mask >>= 1) out++;
    return out;
  }
  
};	


#endif // __IBL_CONNECTIVITY_H__
