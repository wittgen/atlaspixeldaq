#ifndef __IBL_ROD_INMEM_FIFO__
#define __IBL_ROD_INMEM_FIFO__

#include "RxLink.h"

#include <stdint.h>
#include "RodMasterRegisters.h"

class IblRodInmemFifo: public RxLink {
public:
	IblRodInmemFifo();
	~IblRodInmemFifo() { }

	uint32_t read();

	void reset();

	void setRxCh(uint8_t rx_ch);
	void selectGathererFromRxCh(uint8_t rx_ch);

private:
	bool m_readAllChannels;
	uint32_t localRead() {return m_rxCh>15 ? (IblRod::SlaveB::InmemFifoData::read()) :
	    (IblRod::SlaveA::InmemFifoData::read()); };

};

#endif // __IBL_ROD_INMEM_FIFO__

