// This file was generated automatically by scripts/updateMasterRegisters.sh at Mon Apr  6 19:58:06 EDT 2020
#ifndef __ROD_MASTER_REGISTERS_FROM_MASTER_HXX__
#define __ROD_MASTER_REGISTERS_FROM_MASTER_HXX__
#include "HwRegsInCpp.h"
#include <stdint.h>
#include "MMapHandler.h"

#define REGISTER(type, addr, mask, off, policy) typedef reg_t<type, addr, mask, off, policy<type> >

#ifdef __XMK__

#define ROD_MASTER_MDSP_GPREG COMMON_REG_BASE + MDSP_GPREG * sizeof(uint32_t)
#define ROD_MASTER_DSP_STTS_REG COMMON_REG_BASE + DSP_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_TEST_STTS_REG COMMON_REG_BASE + TEST_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_ROD_REV_CODE_REV_REG COMMON_REG_BASE + ROD_REV_CODE_REV_REG * sizeof(uint32_t)
#define ROD_MASTER_RRIF_CTRL_REG COMMON_REG_BASE + RRIF_CTRL_REG * sizeof(uint32_t)
#define ROD_MASTER_SPR_CTRL_REG COMMON_REG_BASE + SPR_CTRL_REG * sizeof(uint32_t)
#define ROD_MASTER_ROD_MODE_REG COMMON_REG_BASE + ROD_MODE_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_MASK_SEL_REG COMMON_REG_BASE + FE_CMD_MASK_SEL_REG * sizeof(uint32_t)
#define ROD_MASTER_RRIF_STTS_REG COMMON_REG_BASE + RRIF_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_SPR_STTS_REG COMMON_REG_BASE + SPR_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_EMPTY_REG0 COMMON_REG_BASE + EMPTY_REG0 * sizeof(uint32_t)
#define ROD_MASTER_EMPTY_REG1 COMMON_REG_BASE + EMPTY_REG1 * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_MASK0_LSB_REG COMMON_REG_BASE + FE_CMD_MASK0_LSB_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_MASK0_MSB_REG COMMON_REG_BASE + FE_CMD_MASK0_MSB_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_MASK1_LSB_REG COMMON_REG_BASE + FE_CMD_MASK1_LSB_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_MASK1_MSB_REG COMMON_REG_BASE + FE_CMD_MASK1_MSB_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_STRB_DLY_REG COMMON_REG_BASE + CAL_STRB_DLY_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_CMD_REG COMMON_REG_BASE + CAL_CMD_REG * sizeof(uint32_t)
#define ROD_MASTER_ECR_CNT_I_REG COMMON_REG_BASE + ECR_CNT_I_REG * sizeof(uint32_t)
#define ROD_MASTER_ECR_CNT_IN_REG COMMON_REG_BASE + ECR_CNT_IN_REG * sizeof(uint32_t)
#define ROD_MASTER_MB_FIFO_STTS_REG COMMON_REG_BASE + MB_FIFO_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_DYN_MASK_STTS_LSB_REG COMMON_REG_BASE + DYN_MASK_STTS_LSB_REG * sizeof(uint32_t)
#define ROD_MASTER_DYN_MASK_STTS_MSB_REG COMMON_REG_BASE + DYN_MASK_STTS_MSB_REG * sizeof(uint32_t)
#define ROD_MASTER_CNFG_RDBACK_CNT_REG COMMON_REG_BASE + CNFG_RDBACK_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_DEBUG_MODE_CNFG_REG COMMON_REG_BASE + DEBUG_MODE_CNFG_REG * sizeof(uint32_t)
#define ROD_MASTER_DEBUG_MODE_STTS_REG COMMON_REG_BASE + DEBUG_MODE_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_INT_TIM_FIFO_DATA_REG COMMON_REG_BASE + INT_TIM_FIFO_DATA_REG * sizeof(uint32_t)
#define ROD_MASTER_INT_TO_SLV_DSPS_REG COMMON_REG_BASE + INT_TO_SLV_DSPS_REG * sizeof(uint32_t)
#define ROD_MASTER_SLV_DSP_INT_REG COMMON_REG_BASE + SLV_DSP_INT_REG * sizeof(uint32_t)
#define ROD_MASTER_TEST_IRQ_REG COMMON_REG_BASE + TEST_IRQ_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_OCC_RES_REG COMMON_REG_BASE + FE_OCC_RES_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_OCC_NUM_L1_REG0 COMMON_REG_BASE + FE_OCC_NUM_L1_REG0 * sizeof(uint32_t)
#define ROD_MASTER_FE_OCC_NUM_L1_REG1 COMMON_REG_BASE + FE_OCC_NUM_L1_REG1 * sizeof(uint32_t)
#define ROD_MASTER_FE_OCC_NUM_L1_REG2 COMMON_REG_BASE + FE_OCC_NUM_L1_REG2 * sizeof(uint32_t)
#define ROD_MASTER_FE_OCC_NUM_L1_REG3 COMMON_REG_BASE + FE_OCC_NUM_L1_REG3 * sizeof(uint32_t)
#define ROD_MASTER_INT_SCAN_REG0 COMMON_REG_BASE + INT_SCAN_REG0 * sizeof(uint32_t)
#define ROD_MASTER_INT_SCAN_REG1 COMMON_REG_BASE + INT_SCAN_REG1 * sizeof(uint32_t)
#define ROD_MASTER_INT_SCAN_REG2 COMMON_REG_BASE + INT_SCAN_REG2 * sizeof(uint32_t)
#define ROD_MASTER_INT_SCAN_REG3 COMMON_REG_BASE + INT_SCAN_REG3 * sizeof(uint32_t)
#define ROD_MASTER_CAL_EVNT_TYPE0_REG COMMON_REG_BASE + CAL_EVNT_TYPE0_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_EVNT_TYPE1_REG COMMON_REG_BASE + CAL_EVNT_TYPE1_REG * sizeof(uint32_t)
#define ROD_MASTER_SCAN_DONE_STTS_REG COMMON_REG_BASE + SCAN_DONE_STTS_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_L1_ID0_REG COMMON_REG_BASE + CAL_L1_ID0_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_L1_ID1_REG COMMON_REG_BASE + CAL_L1_ID1_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_BCID0_REG COMMON_REG_BASE + CAL_BCID0_REG * sizeof(uint32_t)
#define ROD_MASTER_DSP_LEDS_REG COMMON_REG_BASE + DSP_LEDS_REG * sizeof(uint32_t)
#define ROD_MASTER_PRM_GEO_ADDR_REG COMMON_REG_BASE + PRM_GEO_ADDR_REG * sizeof(uint32_t)
#define ROD_MASTER_CAL_PULSE_COUNT0 COMMON_REG_BASE + CAL_PULSE_COUNT0 * sizeof(uint32_t)
#define ROD_MASTER_CAL_PULSE_COUNT1 COMMON_REG_BASE + CAL_PULSE_COUNT1 * sizeof(uint32_t)
#define ROD_MASTER_DBGPPC440 COMMON_REG_BASE + DBGPPC440 * sizeof(uint32_t)
#define ROD_MASTER_EFB_MASK_REG COMMON_REG_BASE + EFB_MASK_REG * sizeof(uint32_t)
#define ROD_MASTER_EFB_MASK_REG_ECR COMMON_REG_BASE + EFB_MASK_REG_ECR * sizeof(uint32_t)
#define ROD_MASTER_BUSYTIME_AFTER_ECR_REG COMMON_REG_BASE + BUSYTIME_AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_VETO_AFTER_ECR_REG COMMON_REG_BASE + VETO_AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_RESET_AFTER_ECR_REG COMMON_REG_BASE + RESET_AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_ECR_Vetoed__AFTER_ECR_REG COMMON_REG_BASE + ECR_Vetoed__AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_BCR_Vetoed__AFTER_ECR_REG COMMON_REG_BASE + BCR_Vetoed__AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_L1A_Vetoed_AFTER_ECR_REG COMMON_REG_BASE + L1A_Vetoed_AFTER_ECR_REG * sizeof(uint32_t)
#define ROD_MASTER_ECR_Dropped_REG COMMON_REG_BASE + ECR_Dropped_REG * sizeof(uint32_t)
#define ROD_MASTER_BCR_Dropped_REG COMMON_REG_BASE + BCR_Dropped_REG * sizeof(uint32_t)
#define ROD_MASTER_L1A_Dropped_REG COMMON_REG_BASE + L1A_Dropped_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_Pulse_Sent_A_REG COMMON_REG_BASE + FE_CMD_Pulse_Sent_A_REG * sizeof(uint32_t)
#define ROD_MASTER_FE_CMD_Pulse_Sent_B_REG COMMON_REG_BASE + FE_CMD_Pulse_Sent_B_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_CNT_READEN_REG COMMON_REG_BASE + BUSY_CNT_READEN_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_CLK_CNT_REG COMMON_REG_BASE + BUSY_CLK_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_TOT_CNT_REG COMMON_REG_BASE + BUSY_TOT_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_SLV0_CNT_REG COMMON_REG_BASE + BUSY_SLV0_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_SLV1_CNT_REG COMMON_REG_BASE + BUSY_SLV1_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_EFB_HEADERPAUSE_CNT_REG COMMON_REG_BASE + BUSY_EFB_HEADERPAUSE_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_FMT_MB_FEF_0_CNT_REG COMMON_REG_BASE + BUSY_FMT_MB_FEF_0_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_FMT_MB_FEF_1_CNT_REG COMMON_REG_BASE + BUSY_FMT_MB_FEF_1_CNT_REG * sizeof(uint32_t)
#define ROD_MASTER_BUSY_ROL_PAUSE_CNT_REG COMMON_REG_BASE + BUSY_ROL_PAUSE_CNT_REG * sizeof(uint32_t)

#endif

struct RodMasterRegistersFromMaster {

	struct Master {
	protected:
#ifndef __XMK__
		static uint32_t ROD_MASTER_MDSP_GPREG;
		static uint32_t ROD_MASTER_DSP_STTS_REG;
		static uint32_t ROD_MASTER_TEST_STTS_REG;
		static uint32_t ROD_MASTER_ROD_REV_CODE_REV_REG;
		static uint32_t ROD_MASTER_RRIF_CTRL_REG;
		static uint32_t ROD_MASTER_SPR_CTRL_REG;
		static uint32_t ROD_MASTER_ROD_MODE_REG;
		static uint32_t ROD_MASTER_FE_CMD_MASK_SEL_REG;
		static uint32_t ROD_MASTER_RRIF_STTS_REG;
		static uint32_t ROD_MASTER_SPR_STTS_REG;
		static uint32_t ROD_MASTER_EMPTY_REG0;
		static uint32_t ROD_MASTER_EMPTY_REG1;
		static uint32_t ROD_MASTER_FE_CMD_MASK0_LSB_REG;
		static uint32_t ROD_MASTER_FE_CMD_MASK0_MSB_REG;
		static uint32_t ROD_MASTER_FE_CMD_MASK1_LSB_REG;
		static uint32_t ROD_MASTER_FE_CMD_MASK1_MSB_REG;
		static uint32_t ROD_MASTER_CAL_STRB_DLY_REG;
		static uint32_t ROD_MASTER_CAL_CMD_REG;
		static uint32_t ROD_MASTER_ECR_CNT_I_REG;
		static uint32_t ROD_MASTER_ECR_CNT_IN_REG;
		static uint32_t ROD_MASTER_MB_FIFO_STTS_REG;
		static uint32_t ROD_MASTER_DYN_MASK_STTS_LSB_REG;
		static uint32_t ROD_MASTER_DYN_MASK_STTS_MSB_REG;
		static uint32_t ROD_MASTER_CNFG_RDBACK_CNT_REG;
		static uint32_t ROD_MASTER_DEBUG_MODE_CNFG_REG;
		static uint32_t ROD_MASTER_DEBUG_MODE_STTS_REG;
		static uint32_t ROD_MASTER_INT_TIM_FIFO_DATA_REG;
		static uint32_t ROD_MASTER_INT_TO_SLV_DSPS_REG;
		static uint32_t ROD_MASTER_SLV_DSP_INT_REG;
		static uint32_t ROD_MASTER_TEST_IRQ_REG;
		static uint32_t ROD_MASTER_FE_OCC_RES_REG;
		static uint32_t ROD_MASTER_FE_OCC_NUM_L1_REG0;
		static uint32_t ROD_MASTER_FE_OCC_NUM_L1_REG1;
		static uint32_t ROD_MASTER_FE_OCC_NUM_L1_REG2;
		static uint32_t ROD_MASTER_FE_OCC_NUM_L1_REG3;
		static uint32_t ROD_MASTER_INT_SCAN_REG0;
		static uint32_t ROD_MASTER_INT_SCAN_REG1;
		static uint32_t ROD_MASTER_INT_SCAN_REG2;
		static uint32_t ROD_MASTER_INT_SCAN_REG3;
		static uint32_t ROD_MASTER_CAL_EVNT_TYPE0_REG;
		static uint32_t ROD_MASTER_CAL_EVNT_TYPE1_REG;
		static uint32_t ROD_MASTER_SCAN_DONE_STTS_REG;
		static uint32_t ROD_MASTER_CAL_L1_ID0_REG;
		static uint32_t ROD_MASTER_CAL_L1_ID1_REG;
		static uint32_t ROD_MASTER_CAL_BCID0_REG;
		static uint32_t ROD_MASTER_DSP_LEDS_REG;
		static uint32_t ROD_MASTER_PRM_GEO_ADDR_REG;
		static uint32_t ROD_MASTER_CAL_PULSE_COUNT0;
		static uint32_t ROD_MASTER_CAL_PULSE_COUNT1;
		static uint32_t ROD_MASTER_DBGPPC440;
		static uint32_t ROD_MASTER_EFB_MASK_REG;
		static uint32_t ROD_MASTER_EFB_MASK_REG_ECR;
		static uint32_t ROD_MASTER_BUSYTIME_AFTER_ECR_REG;
		static uint32_t ROD_MASTER_VETO_AFTER_ECR_REG;
		static uint32_t ROD_MASTER_RESET_AFTER_ECR_REG;
		static uint32_t ROD_MASTER_ECR_Vetoed__AFTER_ECR_REG;
		static uint32_t ROD_MASTER_BCR_Vetoed__AFTER_ECR_REG;
		static uint32_t ROD_MASTER_L1A_Vetoed_AFTER_ECR_REG;
		static uint32_t ROD_MASTER_ECR_Dropped_REG;
		static uint32_t ROD_MASTER_BCR_Dropped_REG;
		static uint32_t ROD_MASTER_L1A_Dropped_REG;
		static uint32_t ROD_MASTER_FE_CMD_Pulse_Sent_A_REG;
		static uint32_t ROD_MASTER_FE_CMD_Pulse_Sent_B_REG;
		static uint32_t ROD_MASTER_BUSY_CNT_READEN_REG;
		static uint32_t ROD_MASTER_BUSY_CLK_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_TOT_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_SLV0_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_SLV1_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_EFB_HEADERPAUSE_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_FMT_MB_FEF_0_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_FMT_MB_FEF_1_CNT_REG;
		static uint32_t ROD_MASTER_BUSY_ROL_PAUSE_CNT_REG;
#endif
	public:
		typedef reg_t<uint32_t, ROD_MASTER_MDSP_GPREG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > MdspGpreg;
		typedef reg_t<uint32_t, ROD_MASTER_DSP_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DspSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_TEST_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > TestSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_ROD_REV_CODE_REV_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > RodRevCodeRevReg;
		typedef reg_t<uint32_t, ROD_MASTER_RRIF_CTRL_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > RrifCtrlReg;
		typedef reg_t<uint32_t, ROD_MASTER_SPR_CTRL_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > SprCtrlReg;
		typedef reg_t<uint32_t, ROD_MASTER_ROD_MODE_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > RodModeReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_MASK_SEL_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdMaskSelReg;
		typedef reg_t<uint32_t, ROD_MASTER_RRIF_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > RrifSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_SPR_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > SprSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_EMPTY_REG0, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EmptyReg0;
		typedef reg_t<uint32_t, ROD_MASTER_EMPTY_REG1, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EmptyReg1;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_MASK0_LSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdMask0LsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_MASK0_MSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdMask0MsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_MASK1_LSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdMask1LsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_MASK1_MSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdMask1MsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_STRB_DLY_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalStrbDlyReg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_CMD_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalCmdReg;
		typedef reg_t<uint32_t, ROD_MASTER_ECR_CNT_I_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EcrCntIReg;
		typedef reg_t<uint32_t, ROD_MASTER_ECR_CNT_IN_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EcrCntInReg;
		typedef reg_t<uint32_t, ROD_MASTER_MB_FIFO_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > MbFifoSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_DYN_MASK_STTS_LSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DynMaskSttsLsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_DYN_MASK_STTS_MSB_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DynMaskSttsMsbReg;
		typedef reg_t<uint32_t, ROD_MASTER_CNFG_RDBACK_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CnfgRdbackCntReg;
		typedef reg_t<uint32_t, ROD_MASTER_DEBUG_MODE_CNFG_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DebugModeCnfgReg;
		typedef reg_t<uint32_t, ROD_MASTER_DEBUG_MODE_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DebugModeSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_INT_TIM_FIFO_DATA_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntTimFifoDataReg;
		typedef reg_t<uint32_t, ROD_MASTER_INT_TO_SLV_DSPS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntToSlvDspsReg;
		typedef reg_t<uint32_t, ROD_MASTER_SLV_DSP_INT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DspIntReg;
		typedef reg_t<uint32_t, ROD_MASTER_TEST_IRQ_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > TestIrqReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_OCC_RES_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeOccResReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_OCC_NUM_L1_REG0, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeOccNumL1Reg0;
		typedef reg_t<uint32_t, ROD_MASTER_FE_OCC_NUM_L1_REG1, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeOccNumL1Reg1;
		typedef reg_t<uint32_t, ROD_MASTER_FE_OCC_NUM_L1_REG2, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeOccNumL1Reg2;
		typedef reg_t<uint32_t, ROD_MASTER_FE_OCC_NUM_L1_REG3, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeOccNumL1Reg3;
		typedef reg_t<uint32_t, ROD_MASTER_INT_SCAN_REG0, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntScanReg0;
		typedef reg_t<uint32_t, ROD_MASTER_INT_SCAN_REG1, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntScanReg1;
		typedef reg_t<uint32_t, ROD_MASTER_INT_SCAN_REG2, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntScanReg2;
		typedef reg_t<uint32_t, ROD_MASTER_INT_SCAN_REG3, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > IntScanReg3;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_EVNT_TYPE0_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalEvntType0Reg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_EVNT_TYPE1_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalEvntType1Reg;
		typedef reg_t<uint32_t, ROD_MASTER_SCAN_DONE_STTS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > ScanDoneSttsReg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_L1_ID0_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalL1Id0Reg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_L1_ID1_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalL1Id1Reg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_BCID0_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalBcid0Reg;
		typedef reg_t<uint32_t, ROD_MASTER_DSP_LEDS_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > DspLedsReg;
		typedef reg_t<uint32_t, ROD_MASTER_PRM_GEO_ADDR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > PrmGeoAddrReg;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_PULSE_COUNT0, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalPulseCount0;
		typedef reg_t<uint32_t, ROD_MASTER_CAL_PULSE_COUNT1, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > CalPulseCount1;
		typedef reg_t<uint32_t, ROD_MASTER_DBGPPC440, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > Dbgppc440;
		typedef reg_t<uint32_t, ROD_MASTER_EFB_MASK_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EfbMaskReg;
		typedef reg_t<uint32_t, ROD_MASTER_EFB_MASK_REG_ECR, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EfbMaskRegEcr;
		typedef reg_t<uint32_t, ROD_MASTER_BUSYTIME_AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusytimeAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_VETO_AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > VetoAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_RESET_AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > ResetAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_ECR_Vetoed__AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EcrVetoedAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_BCR_Vetoed__AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BcrVetoedAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_L1A_Vetoed_AFTER_ECR_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > L1AVetoedAfterEcrReg;
		typedef reg_t<uint32_t, ROD_MASTER_ECR_Dropped_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > EcrDroppedReg;
		typedef reg_t<uint32_t, ROD_MASTER_BCR_Dropped_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BcrDroppedReg;
		typedef reg_t<uint32_t, ROD_MASTER_L1A_Dropped_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > L1ADroppedReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_Pulse_Sent_A_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdPulseSentAReg;
		typedef reg_t<uint32_t, ROD_MASTER_FE_CMD_Pulse_Sent_B_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > FeCmdPulseSentBReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_CNT_READEN_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyCntReadenReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_CLK_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyClkCntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_TOT_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyTotCntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_SLV0_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusySlv0CntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_SLV1_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusySlv1CntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_EFB_HEADERPAUSE_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyEfbHeaderpauseCntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_FMT_MB_FEF_0_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyFmtMbFef0CntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_FMT_MB_FEF_1_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyFmtMbFef1CntReg;
		typedef reg_t<uint32_t, ROD_MASTER_BUSY_ROL_PAUSE_CNT_REG, 0xFFFFFFFF, 0, rw_t<uint32_t>, MMapHandler::mmap_common > BusyRolPauseCntReg;
	};

};
#endif
