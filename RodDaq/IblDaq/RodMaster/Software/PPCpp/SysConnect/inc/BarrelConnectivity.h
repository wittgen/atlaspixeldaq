/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2016-III-21
 * Description: this header defines 'hard-coded' parameters for the low-level layout.
 *	These parameters are provided for initialization and are overridden when running from PixLib.
 * 	Use ifdef flags to adjust to labs.
 */

#ifndef __BARREL_CONNECTIVITY_H__
#define __BARREL_CONNECTIVITY_H__

#include "RodResourceManager.h"
#include <iostream>

// I could have used a namespace but we might have this object templated in the future
struct BarrelConnectivity {

  /// This function provides a tx_mask with a SINGLE bit set, corresponding to the rxCh
  // This uses the module configs to determine the tx
  static uint32_t getTxMaskFromRxCh(uint8_t rxCh, bool skipInactive = true) {
    uint32_t tx_mask = 0x0;
    Fei3Mod * const cfgArray = RodResourceManagerPIX::getCalibModuleConfigs();
    const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();

    Fei3Mod *modPtr = cfgArray;
    while (modPtr != (cfgArray+nCfg)) {
      if (skipInactive && modPtr->revision == 0) ;
      else
        if (rxCh == modPtr->getRxCh())
          tx_mask |= (0x1 << modPtr->getTxCh());
      modPtr++;
    }

    return tx_mask;
  }

  /// This function computes the tx_mask that needs to be set in order to talk to the FEs enabled in rx_mask
  /// Note: this doesn't take the chipID into account. A TX channel is enabled as soon as one of the corresponding RX channels is enabled.
  /// This scheme doesn't require modifications for 'enabling channels' since it is assumed that one wants to broadcast to 'enable' links.
  static uint32_t getTxMaskFromRxMask(uint32_t rx_mask, bool skipInactive = true) {
    uint32_t tx_mask = 0x0;
    Fei3Mod * const cfgArray = RodResourceManagerPIX::getCalibModuleConfigs();
    const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();

    Fei3Mod *modPtr = cfgArray;
    while (modPtr != (cfgArray+nCfg)) {
      if (skipInactive && modPtr->revision == 0) ;
      else
        if (rx_mask & (0x1 << modPtr->getRxCh()))
          tx_mask |= (0x1 << modPtr->getTxCh());
      modPtr++;
    }

    return tx_mask;
  }

  static uint32_t getRxMaskFromModMask(uint32_t mod_mask, bool skipInactive = true) {
    uint32_t rx_mask = 0x0,i=0;
    Fei3Mod * const cfgArray = RodResourceManagerPIX::getCalibModuleConfigs();
    const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();

    Fei3Mod *modPtr = cfgArray;
    while (modPtr != (cfgArray+nCfg)) {
      if (skipInactive && modPtr->revision == 0) ;
      else
        if ( mod_mask & (0x1 << i))
          rx_mask |= (0x1 << modPtr->getRxCh());
      modPtr++;
      i++;
    }

    return rx_mask;
  }

  static uint32_t getModMaskFromRxMask(uint32_t rx_mask, bool skipInactive = true) {
    uint32_t mod_mask = 0x0;
    Fei3Mod * const cfgArray = RodResourceManagerPIX::getCalibModuleConfigs();
    for (uint32_t i = 0; i < RodResourceManagerPIX::getNModuleConfigs(); i++) {
      if (skipInactive && cfgArray[i].revision == 0) ;
      else
        if( (0x1 << cfgArray[i].getRxCh() & rx_mask ) )
          mod_mask |= 0x1 << i;
    }
    return mod_mask;
  }

  static uint8_t getTxChFromRxCh(uint8_t rxCh) {
    uint32_t mask = getTxMaskFromRxCh(rxCh);
    uint8_t out = 0;
    while (mask >>= 1) out++;
    return out;
  }

  // Returns the rxCh of the other link for a given module
  // Note: assumes Layer-1 connectivity
  static uint8_t getAltRxLink(uint8_t rxCh) {
    rxCh = rxCh % 32;
    if( (rxCh/8) % 2 ) return rxCh - 8;
    return rxCh + 8;
  }

};


#endif // __BARREL_CONNECTIVITY_H__
