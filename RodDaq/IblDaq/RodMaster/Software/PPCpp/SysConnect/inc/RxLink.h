#ifndef __RX_LINK__
#define __RX_LINK__

#include <stdint.h>

class RxLink {
protected:
	uint8_t m_rxCh;
public:
	virtual uint32_t read() = 0;
	uint8_t read8() {return (0xFF&this->read());};
	void setRxCh(uint8_t rx_ch) { m_rxCh = rx_ch; }
	uint8_t getRxCh() { return m_rxCh; };
	virtual ~RxLink() { }
};

class DummyRxLink: public RxLink {
public:
	uint32_t read() { return 0x00DEAD00; }
	~DummyRxLink() { }
};

#endif // __RX_LINK__
