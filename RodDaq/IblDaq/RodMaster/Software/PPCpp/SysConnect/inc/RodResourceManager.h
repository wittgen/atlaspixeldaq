// Author: K. Potamianos <karolos.potamianos@cern.ch>
// Date: 2013-XI-15
// Description: This file describes a _global_ object that stores objects that need 
// 		to be accessed globally and need to be management. Module configurations are
// 		a good example, so are the multiple serial serial streams available.

#ifndef __ROD_RESOURCE_MANAGER__
#define __ROD_RESOURCE_MANAGER__

#ifdef __XMK__
//#include "PrmSpiDev.h" OKREMOVE
#endif

#include "Fei3Mod.h"
#include "Fei4.h"
#include "IblBoc.h"
#include "FeLink.h"
#include "IblRodSerialPort.h"
#include "IblBocRxFifo.h"
#include "IblBocTxFifo.h"
#include "IblBocLink.h"
#include "MMapHandler.h"

typedef IblRodSerialPort FeTxBcType;
typedef IblBocTxFifoL12 FeTxType;
typedef IblBocRxFifo FeRxType;

#include <stdint.h>
#include <iostream>
#include <iomanip>

// Todo: include serial port in pattern
template <uint32_t N_MOD, typename _FeMod>
class RodResourceManager {
public:
  // To be used as either RodResourceManagerPIX::FeModType or RodResourceManagerIBL::FeModType
  // A more generic way is to use RodResMgr::FeModType
  typedef _FeMod FeModType;
  static uint32_t getCodeVersion() {
    if( IblRod::SlaveA::CodeVersion::read() != IblRod::SlaveB::CodeVersion::read() )
      std::cout << "ERROR: Your slaves don't have the same FIRMWARE CODE !!!" << std::endl;
    return IblRod::SlaveA::CodeVersion::read(); // Need to pick one of the 2
  }
  static bool isFei4() { return !isFei3() ; } // return getCodeVersion() == 0xfe14; isn't working on the OLD IBL FW
  static bool isFei3() { return getCodeVersion() == 0xfe13; }
  static bool slaveIdle(uint8_t slaveId) {
    return 0xBEEF == (slaveId ? IblRod::SlaveA::Ram::read(0) : IblRod::SlaveB::Ram::read(0)); // Checking for SLV_CMD_IDLE
  }
  static bool slaveAlive(uint8_t slaveId) {
    return 0xC0FFEE == (slaveId ? IblRod::SlaveA::Ram::read(0) : IblRod::SlaveB::Ram::read(0)); // Checking for SLV_CMD_ALIVE
  }
  // Caution: taking modulo to avoid exceptions or introducing overhrad.
  // Todo: make this safer!
  static uint32_t getNModuleConfigs() { return N_MOD; }
  // Wraps around to make sure we don't access other objects
  static _FeMod& getPhysicsModuleConfig(uint32_t m) { return getInstance().physModCfg[m%N_MOD]; }
  static _FeMod& getCalibModuleConfig(uint32_t m) { return getInstance().calibModCfg[m%N_MOD]; }
  static _FeMod* getPhysicsModuleConfigs() { return getInstance().physModCfg; }
  static _FeMod* getCalibModuleConfigs() { return getInstance().calibModCfg; }
  // For now returns the serial port.
  // Todo: configure to return any _available_ serial port and implement release mechanisms
  // Alternative: use 'task' scheduler which manages the serial ports. Viability tbc.
  static SerialPort* getSerialPort() { return &getInstance().sPort; }
  // Singletons to provide /dev/null links
  static SerialPort* getDevNullSerialPort() { static SerialPort nullSPort; return &nullSPort; }
  static FeLink* getDevNullFeLink() { static FeLinkImpl<SerialPort,DummyRxLink> nullFeLink; return &nullFeLink; }
  // This link uses the IblRodSerial port instance as its TxLink a way to broadcast ; the RxLink is dummy
  // WARNING: this link is a 2nd instance of the IblRodSerialPort which is NOT necessarily in the same state as the one returned by getRodSerialPort()
  static FeLink* getBroadcastFeLink() { static FeLinkImpl<IblRodSerialPort,DummyRxLink> bcFeLink; return &bcFeLink; }
  static FeLink* getFeLink() { return getInstance().feLinkImpl; }
  static IblBocLink* getIblBocLink() { return &getInstance().iblBocLink; }
  static uint8_t getVmeSlot() { return getInstance().vmeSlot ? getInstance().vmeSlot : (~IblRod::PrmGeoAddr::read()&0x1F); }

  static bool bocEmu() { return getInstance().m_bocEmu; }
  static void setBocEmu(bool emu) { getInstance().m_bocEmu = emu; }

  static uint32_t getRxMask(_FeMod (&modCfg)[N_MOD]) {
	uint32_t rxMask = 0;
	for(uint32_t rxCh = 0 ; rxCh < N_MOD ; ++rxCh) {
		uint8_t modRxCh = modCfg[rxCh].getRxCh();
		// Todo: use 'enabled' bit for given FE
		if(modCfg.isEnabled()) rxMask |= (1<<modRxCh); // Assumes channels are provided 1-32 ...
	}

	return rxMask;
  }

  // Todo: maybe move this somewhere else, inside a per-config-group structure, or in _FeMod
  uint32_t getRxMaskFromPhys() { return getRxMask(physModCfg); }
  uint32_t getRxMaskFromCalib() { return getRxMask(calibModCfg); }

  bool enableChannelsFromPhys() { return iblBocLink.enableChannels( getRxMaskFromPhys() ); }
  bool enableChannelsFromCalib() { return iblBocLink.enableChannels( getRxMaskFromPhys() ); }

	// Sets FeLink Tx masks based on contents of Fei4 array
	static void updateConnectivity(_FeMod* cfgArray) { getInstance().updateConnectivityFromCfg(cfgArray); }

  /// This function provides a tx_mask with a SINGLE bit set, corresponding to the rxCh
  // This is the default version of the tx_mask, before you have sent module configs
  static uint32_t getDefaultTxMaskFromRxCh(uint8_t rxCh) {
    uint32_t txMask = 1 << ((rxCh%16)/2); // Two 'RX' served by 1 'TX'
    if(rxCh>15) txMask <<= 16; // Shifting because second slave is TxMask bits 16-31
    return txMask;
  }

  /// This function computes the tx_mask that needs to be set in order to talk to the FEs enabled in rx_mask
  /// Note: this doesn't take the chipID into account. A TX channel is enabled as soon as one of the corresponding RX channels is enabled.
  /// This scheme doesn't require modifications for 'enabling channels' since it is assumed that one wants to broadcast to 'enable' links.
  // This is the default version of the tx_mask, before you have sent module configs
  static uint32_t getDefaultTxMaskFromRxMask(uint32_t rx_mask) {
    return 0x0;
  }

private:
  // Cannot use std::array until C++11
  _FeMod physModCfg[N_MOD];
  _FeMod calibModCfg[N_MOD];

  // Todo: use shared_ptr
  FeLink* feLinkImpl;		// Multipurpose link
  FeLinkImpl<IblBocTxFifoL12,IblBocRxFifo> feLink[N_MOD];	// One link for each channel
  IblBocLink iblBocLink;

  IblRodSerialPort sPort;	// Actual serial port for IBL ROD // To do: update when more lines are present

  uint8_t vmeSlot = 22; // OKREMOVE assign it out of range

  bool m_bocEmu = false; // use the boc FE emulator?


// Singleton pattern
public:
  static RodResourceManager<N_MOD,_FeMod>& getInstance() { static RodResourceManager<N_MOD,_FeMod> instance; return instance; }
private:
  // All these need to remain private if we are to maintain the singleton
  RodResourceManager<N_MOD,_FeMod>() { 

  	std::cout << __PRETTY_FUNCTION__ << std::endl; 
	// Must be the first thing done in the RodResourceManager!
	RodMaster::initEPC();


   // OKREMOVE
//#ifdef __XMK__
//	PrmSpiDev spiDev(PRM_SPI_DEVICE,6);
//	spiDev.init();
//	vmeSlot = spiDev.getVmeSlot();
//#endif

	std::cout << "SLOT in VME crate from SPI: (OKREMOVE NOT WORKING)" << (uint32_t) vmeSlot << std::endl;
	//uint32_t tmpVal = IblRod::PrmGeoAddr::read();
	//while(tmpVal == 0x0){ std::cout << "IblRod::PrmGeoAddr::read(): " << tmpVal << std::endl; usleep(500000); }
	std::cout << "SLOT in VME crate from IblRod::PrmGeoAddr::read(): " << std::hex << "0x" << IblRod::PrmGeoAddr::read() << std::dec << std::endl;

    feLinkImpl = new FeLinkImpl<FeTxType, FeRxType>();
	iblBocLink.setTxLink( &sPort ); // Setting IblRodSerialPort as default for IblBocLink (allows broadcasting)
	for(uint8_t rxCh = 0 ; rxCh <  N_MOD ; ++rxCh) {
		// Warning: hard-coded for IBL! // To do: make generic
		// Currently implementing on FeTxType item _per channel_ // To do: revise model
		// Default initialization for IBL. To do: get from connectivity, i.e. FeCfg
		// Info: updateConnectivity can be used to update the connectivity based on the information from the Fei4[] physics configuration
		feLink[rxCh].setRxCh( rxCh );
#ifdef LL_DEBUG
		std::cout<<"setting Rx channel "<<(int)rxCh<<" in the Boc"<<std::endl;
#endif

		uint32_t txMask = getDefaultTxMaskFromRxCh(rxCh);
		feLink[rxCh].setTxMask( txMask );

		// Mapping the links to the FEs
		physModCfg[rxCh].setFeLink( &feLink[rxCh] );
		calibModCfg[rxCh].setFeLink( &feLink[rxCh] );
#ifdef LL_DEBUG
		std::cout<<"setting Tx mask to : 0x"<<std::hex<<(int)txMask<<std::dec<<std::endl;
#endif
	}
	m_bocEmu = false;
  }

  ~RodResourceManager<N_MOD,_FeMod>() { 
  	std::cout << __PRETTY_FUNCTION__ << std::endl; 
	delete feLinkImpl;
  }

  RodResourceManager<N_MOD,_FeMod>(RodResourceManager<N_MOD,_FeMod> const&);
  void operator=(RodResourceManager<N_MOD,_FeMod> const&);

	// Sets FeLink Tx masks based on contents of Fei4 in Physics array
	void updateConnectivityFromCfg(_FeMod* cfgArray) {
	  //std::cout << __PRETTY_FUNCTION__ << std::endl;
		for(uint8_t rxCh = 0 ; rxCh <  N_MOD ; ++rxCh) {
				uint32_t txMask = (1 << cfgArray[rxCh].getTxCh());
				feLink[rxCh].setTxMask( txMask );
				IblBocTxFifoL12 *btf;
				if( RodResourceManager<N_MOD,_FeMod>::isFei3() )
					if( (btf = dynamic_cast<IblBocTxFifoL12*>( &feLink[rxCh] )) ) btf->set5MHzMode();
				//std::cout << std::setfill('0') << std::setw(8) << (uint32_t)rxCh << "/0x" << std::hex << cfgArray[rxCh].getTxCh() << std::dec << std::endl;
				//std::cout << "Assigning mask 0x" << std::hex << txMask << std::dec << " (rx=" << cfgArray[rxCh].getTxCh() << ") to module with rxCh=" << (uint32_t)rxCh << std::endl;
		}
		//std::cout << "Connectivity updated from config array!" << std::endl;
	}

};

typedef RodResourceManager<32, Fei4> RodResourceManagerIBL;
typedef RodResourceManager<32, Fei3Mod> RodResourceManagerPIX; // To follow PixModuleGroup

#ifndef L12_CPP_SW
typedef RodResourceManagerIBL RodResMgr;
#else
typedef RodResourceManagerPIX RodResMgr;
#endif

#ifdef KAROLOS_TESTING
// RodResMgr::isFei4()

template <>
static bool RodResourceManager<32, Fei4>::isFei4() { return true; }
template <>
static bool RodResourceManager<32, Fei4>::isFei3() { return false; }

template <>
static bool RodResourceManager<26, Fei3Mod>::isFei4() { return false; }
template <>
static bool RodResourceManager<26, Fei3Mod>::isFei3() { return true; }
#endif

#endif // __ROD_RESOURCE_MANAGER__

