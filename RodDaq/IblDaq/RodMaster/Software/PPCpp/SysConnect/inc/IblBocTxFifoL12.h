/*
 * Author: L. Kaplan <lkaplan@cern.ch>
 * Date: 2016-II-11
 * Description: Extension of IblBoxTxFifo to include 5MHz operation
 */

#ifndef __IBL_BOC_TX_FIFO_L12__
#define __IBL_BOC_TX_FIFO_L12__

#include "IblBocTxFifo.h"

class IblBocTxFifoL12 : public IblBocTxFifo {
    public:
        IblBocTxFifoL12();
        ~IblBocTxFifoL12();

        void set5MHzMode(bool on = true);
        void write5MHz(uint32_t data);
};

#endif // __IBL_BOC_TX_FIFO_L12__
