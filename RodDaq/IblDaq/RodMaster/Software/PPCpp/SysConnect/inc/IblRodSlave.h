#ifndef __IBL_ROD_SLAVE_H__
#define __IBL_ROD_SLAVE_H__

//Still in progress. Steve, 2013.02.17

#include <stdint.h>
#include "iblSlaveCmds.h"
#include "IblBoc.h"
#include <string>

#ifdef __XMK__
//  Stuff just for dummyHistoFill function:

#define __DS_FEI3__

#include "KRandom.h"

#endif // __XMK__ (dummyHistoFill stuff)

#ifdef __DS_FEI3__ //  TODO:  At least create L12 versions of thses and switch based on nChips in a function (as in slave setupGeometry)
#define CHIP_BITS 7
#define ROWSIZE 160
#define COLSIZE 18
#define TOT_BITS 8
#else
#define CHIP_BITS 3
#define ROWSIZE 336
#define COLSIZE 80
#define TOT_BITS 4
#endif // __DS_FEI3__

// Threshold Scan drivers
#define EXPTHR 4000
#define RNGTHR 1000
//

#define MAX_SLVCMD_DATA 470 //  Not sure what the memory limit of RAM for slave command is, but 4700 does not seem to work, while 470 does

using std::string;

class IblRodSlave{
 protected:
 public:

  bool usingFei3;

  uint32_t *epcSlvRegs[2];
  uint32_t *epcSlvRam[2];

  struct LargeIblSlvRdWr_t {
    uint32_t cmd;
    uint32_t addr;
    uint32_t count;
    uint32_t data[MAX_SLVCMD_DATA];
  }; //  Same as IblSlvRdWr, but with memory allocated for more than one word of data

  static void write(uint32_t addr, uint32_t value, unsigned id);
  static void write(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id);
  static void read(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id);
  static void checkRead(uint32_t addr, uint32_t* buffer, int bufferLength, unsigned id);
 public:
  IblRodSlave();
  ~IblRodSlave();

  static bool sendCommand(unsigned id, uint32_t *buffer, unsigned nWords, uint32_t *returnBuffer = NULL, unsigned nReturnWords = 0);
  static bool setId(unsigned id);
  //template<typename T, typename R> void sendCommand(unsigned id, T

  static int check(unsigned id);

  //void sendData(uint32_t* data, unsigned nWords, unsigned id); 
  static bool getHisto(unsigned id, unsigned dc, uint32_t* buffer);
  static bool getOccHisto(unsigned id, uint32_t* buffer, uint32_t relAdd, uint16_t count, unsigned histogrammer);
  static bool testInj(unsigned id, uint32_t addr, uint32_t data);
  static bool injectHitInHistoUnit(unsigned id, uint32_t addr, unsigned row, unsigned col, unsigned tot, unsigned chip);
  iblSlvStat getStatus(unsigned type, unsigned id);
  static bool setVerbose(unsigned id, unsigned on);
  static void setUart(unsigned val);
  void startSlave();
  bool setId();
  bool startHisto(IblSlvHistCfg *cfg, unsigned id);
  IblSlvHistCfg getHistCfg(unsigned id);
  bool setNetCfg(IblSlvNetCfg *cfg, unsigned id);
  IblSlvNetCfg getNetCfg(unsigned id);
  IblSlvHashes getHashes(unsigned id);
  void writeDevice();
  bool stopHisto(unsigned id);

  void resetNetwork(uint8_t slave);
  void resetHisto(uint8_t slave);

  static void enableRxLink(uint8_t slave, uint16_t chMask);
  static void enableRxLink(uint32_t chMask = 0xFFFFFFFF);

  static void getDefaultNetConfig(uint8_t slaveid, IblSlvNetCfg& netCfg);
  static void setDefaultNetConfig(uint8_t slaveid);

  void testHisto();
  void testNetCfg();

  void disableAllRxChannels();

  static void createIp4Address(uint32_t ip1, uint32_t ip2,
    uint32_t ip3, uint32_t ip4, uint32_t *destination);

  static void createMacAddress(unsigned char mac1, unsigned char mac2,
                               unsigned char mac3, unsigned char mac4,
                               unsigned char mac5, unsigned char mac6,
                               uint32_t* destination);

  static void initSlave(uint8_t id);
  static void waitSlaveBoot(uint8_t id);
  static void startSlave(uint8_t id);
  static void loadSlave(uint8_t id, uint32_t *data, uint32_t blkSize);
  void fillDummyHisto(IblSlvHistCfg* pHistConfig, uint32_t iMask, uint32_t iPar, uint32_t nPar, uint32_t unitMask, uint32_t whichSlave, uint32_t histoUnit,
                                                  enum DummyScanType scanType, enum TestHistoStatus injectionType, uint32_t multiHit);
  void testRxHisto(IblSlvHistCfg* pHistConfig, uint32_t iMask, uint32_t unitMask, uint32_t whichSlave, uint32_t histoUnit,
                                                uint32_t modMask, uint32_t nTrig, uint32_t mode, uint32_t donothing);

  void testRxBypass(IblSlvHistCfg* pHistConfig, uint32_t iMask, uint32_t unitMask, uint32_t whichSlave, uint32_t histoUnit);

	//ROD Emulator
	struct MCCEmu {
	  static void config(uint8_t slaveId, uint32_t Config1, uint32_t Config2);
	  static void init(uint8_t slaveId, uint8_t frameCount, uint8_t speed);
	  static void reset(uint8_t slaveId);
	  static void setHitCount(uint8_t slaveId, uint8_t c = 0xFF);
	};

  static void setFmtFei3Reg(IblBoc::PixSpeed pixSpeed, uint8_t slaveMask = 0x3);

};


#endif //__IBL_ROD_SLAVE_H__
