/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-II-7
 * Description: TX link going though the BMFs
 */

#ifndef __IBL_BOC_TX_FIFO__
#define __IBL_BOC_TX_FIFO__

#include "TxLink.h"

#include <stdint.h>

class IblBocTxFifo: virtual public TxLink {
protected:
	uint8_t m_txCh;
public:
	IblBocTxFifo();
	~IblBocTxFifo();

        void write(uint32_t data);
        void send();

        void setTxMask(uint32_t ch_mask);
        void setTxCh(uint8_t tx_ch);
	uint32_t getTxMask();
	
};

#endif // __IBL_BOC_TX_FIFO__

