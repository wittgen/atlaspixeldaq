/*
 * Author: K. Potmianos <karolos.potamianos@cern.ch>
 * Date: 2014-I-23
 * Description: class to handle '/dev/null' serial port
 */

#ifndef __SERIAL_PORT_H__
#define __SERIAL_PORT_H__

#include "TxLink.h"

class SerialPort: public TxLink {
public:
        virtual void init() { }
        virtual int ready() { return 0; }

        virtual void setTxMask(uint32_t m) { }
        virtual void setTxCh(uint8_t ch) { } 
	virtual uint32_t getTxMask() {return 0;}
	virtual uint32_t readsPort() {return 0;}

        virtual void write(uint8_t size, uint32_t* values) { } 
        virtual void write(uint8_t size, ...) { }
        virtual void sendStream() { } 

        virtual void write(uint32_t data) { }

        virtual void send() { }

        virtual void sendTestConfig() { } 
};

#endif // __SERIAL_PORT_H__
