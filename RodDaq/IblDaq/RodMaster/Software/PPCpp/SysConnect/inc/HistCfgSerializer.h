/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * Date: 13-September-2015
 *
 * Serializer for slave histogrammer configuration structure
 *
 */

template <>
class Serializer<IblSlvHistCfg> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const IblSlvHistCfg &obj) {
    Serializer<uint32_t>::serialize(data, offset, obj.cmd);
    for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].nChips);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].enable);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].type);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].maskStep);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].mStepEven);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].mStepOdd);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].chipSel);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].expectedOccValue);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].addrRange);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].scanId);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].binId);
      Serializer<uint32_t>::serialize(data, offset, obj.cfg[slvUnit].maskId);
    }
  }

  static IblSlvHistCfg deserialize(const uint8_t* data, uint32_t& offset) {
    IblSlvHistCfg obj;
    deserialize(data, offset, obj);
    return obj;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, IblSlvHistCfg &obj) {
    obj.cmd = Serializer<uint32_t>::deserialize(data, offset);
    for(std::size_t slvUnit = 0 ; slvUnit < 2 ; ++slvUnit) {
      obj.cfg[slvUnit].nChips = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].enable = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].type = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].maskStep = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].mStepEven = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].mStepOdd = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].chipSel = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].expectedOccValue = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].addrRange = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].scanId = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].binId = Serializer<uint32_t>::deserialize(data, offset);
      obj.cfg[slvUnit].maskId = Serializer<uint32_t>::deserialize(data, offset);
    }
  }

  static std::size_t size(const IblSlvHistCfg &obj) {
    return sizeof(obj);
  }
};
