/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Serializer for IblBoc::SLinkInfo
 */

#include "IblBoc.h"


template <>
class Serializer<IblBoc::SLinkInfo> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const IblBoc::SLinkInfo &obj) {
    for(std::size_t sLinkId = 0 ; sLinkId < 4 ; ++sLinkId) {
      for(std::size_t holaId = 0 ; holaId < 2 ; ++holaId) {
        Serializer<uint16_t>::serialize(data, offset, obj.sLinkCounters[sLinkId].Hola[holaId].Lff);
        Serializer<uint16_t>::serialize(data, offset, obj.sLinkCounters[sLinkId].Hola[holaId].LDown);
      }
    }
  }

  static IblBoc::SLinkInfo deserialize(const uint8_t* data, uint32_t& offset) {
    IblBoc::SLinkInfo obj;
    deserialize(data, offset, obj);
    return obj;
  }

  static void deserialize(const uint8_t* data, uint32_t& offset, IblBoc::SLinkInfo &obj) {
    for(std::size_t sLinkId = 0 ; sLinkId < 4 ; ++sLinkId) {
      for(std::size_t holaId = 0 ; holaId < 2 ; ++holaId) {
        Serializer<uint16_t>::deserialize(data, offset, obj.sLinkCounters[sLinkId].Hola[holaId].Lff);
        Serializer<uint16_t>::deserialize(data, offset, obj.sLinkCounters[sLinkId].Hola[holaId].LDown);
      }
    }
  }

  static std::size_t size(const IblBoc::SLinkInfo &obj) {
    return sizeof(obj);
  }
};
