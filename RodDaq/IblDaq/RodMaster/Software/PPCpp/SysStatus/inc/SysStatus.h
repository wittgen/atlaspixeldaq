#ifndef SYSTATUS_H
#define SYSSTATS_H

#include "CppCompatibility.h"
#include <string>
#include "Serializer.h"


//For some obscure reason this has to be before the lwip declarations
#ifdef __XMK__
#endif

#ifdef __XMK__
#include <sys/stats.h>
#include <sys/process.h>
#include "lwip/sys.h"
#include "lwip/opt.h"
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/stats.h"
#endif

#define STAT_UDP_BUFLEN 1024


class SysStatus{
 

 public:
  bool initializeClient();
  static SysStatus* getInstance();
  void run();
  

 private:
  
  void add8(uint8_t val);
  void add16(uint16_t val);
  void add32(uint32_t val);
  void clear() {len=0;};
  uint8_t buffer[STAT_UDP_BUFLEN]; // UDP packet BUFFER
  void netstat(struct stats_proto net);
  uint32_t len;
  int socketId;
  
#ifdef __XMK__
  struct sockaddr_in server;
#endif

};


#endif
