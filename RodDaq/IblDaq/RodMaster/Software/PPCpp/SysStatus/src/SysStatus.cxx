#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <iostream>
#include <iomanip>
#include <time.h>
#include "SysStatus.h"
#include <pthread.h>
#include "malloc.h"


#ifdef __XMK__

extern "C" {void CHECK_STD_INIT(void*) {};} // fix missing macros definition in libc
extern "C" pthread_info_t* pthread_get_info (pthread_t thr);

// ugly copy of definition of private LWIP structures

struct thread_start_param {
  struct sys_thread *thread;
  void (*function)(void*);
  void *arg;
};


struct sys_thread {
  pthread_t tid;
  struct thread_start_param tp;
};


extern "C" char sys_thread_name[SYS_THREAD_MAX][12];
extern "C" struct sys_thread lwip_thread[SYS_THREAD_MAX];
extern "C" process_struct ptable[MAX_PROCESS_CONTEXTS];
extern sem_info_t sem_heap[MAX_SEM];

#endif

bool SysStatus::initializeClient() {
#ifdef __XMK__
  this->socketId = socket(AF_INET,SOCK_DGRAM,0);
  std::cout<<"Created Socket "<<this->socketId<<std::endl;

  if (this->socketId < 0)
    return false;

  
  server.sin_family = AF_INET;
  server.sin_port = htons(11111);
  server.sin_addr.s_addr = inet_addr("192.168.1.141");
  
  // I don't think we need to connect here.
  if (connect(this->socketId, (struct sockaddr *) &server, sizeof(server)) < 0)
    {
      std::cout<<"Could not connect to socket"<<std::endl;
      return false;
    }
  else 
    std::cout<<"Connected to the server"<<std::endl;
  
  return true;
#endif
}

SysStatus* SysStatus::getInstance() {
  static SysStatus *instance = 0;
  if (!instance) {
    instance = new SysStatus();
    instance->run();
  }
  
  //if (!instance)
  //std::cerr<<"ERROR: couldn't create GetStatus2 instance!!!" << std::endl;
  return instance;
}

void SysStatus::add8(uint8_t val) {
#ifdef __XMK__
  buffer[len]=val;len++;
#endif
}
void SysStatus::add16(uint16_t val) {
#ifdef __XMK__
  uint16_t v=htons(val);
  memcpy((void *)&buffer[len],(void*)&v,2);len+=2;
#endif
}
void SysStatus::add32(uint32_t val) {
#ifdef __XMK__
  uint32_t v=htonl(val);
  memcpy((void*)&buffer[len],(void*)&v,4);len+=4;
#endif
}


#ifdef __XMK__
void SysStatus::netstat(struct stats_proto net)
{
  add32(net.xmit);             /* Transmitted packets. */
  add32(net.recv);             /* Received packets. */
  add32(net.fw);               /* Forwarded packets. */
  add32(net.drop);             /* Dropped packets. */
  add32(net.chkerr);           /* Checksum error. */
  add32(net.lenerr);           /* Invalid length error. */
  add32(net.memerr);           /* Out of memory error. */
  add32(net.rterr);            /* Routing error. */
  add32(net.proterr);          /* Protocol error. */
  add32(net.opterr);           /* Error in options. */
  add32(net.err);              /* Misc error. */
  add32(net.cachehit);
}
#endif

void SysStatus::run() {
  
  initializeClient();
  #ifdef __XMK__
  kstats_t kstats;
  const char unknown_task[12]="UNKNOWN\0\0\0\0";
  p_stat pstats[SYS_THREAD_MAX];
  uint32_t i, j,l;

  xil_printf("Starting SysStatus daemon...\n");

  uint32_t num=0; //sequence number
  sleep(2000); //Give it the time to get up..
 
  while (1) {
    kstats.pstat_count= SYS_THREAD_MAX;
    kstats.pstats= pstats;
    int result=sys_get_kernel_stats(&kstats);
    clear(); //new UDP packet
    add32(0xcafeefac); // header
    add32(num); //sequence number
    uint32_t nproc=kstats.pstat_count; // number of processes;
    add32(kstats.kernel_ticks); // kernel ticks
    add32(result);
    add32(SCHED_HISTORY_SIZ); // scheduler history length 
    for(i=0;i<SCHED_HISTORY_SIZ;i++) add32(kstats.sched_history[i]); //scheduler history
    if(nproc>=SYS_THREAD_MAX) nproc=0;
    add32(nproc);// number of processes
    uint32_t stackaddr=0;
    uint32_t stacksize=0;
    uint32_t stackptr=0;
    for(i=0;i<nproc;i++) {
      for (j = 0; j < SYS_THREAD_MAX; j++) {
	pthread_info_t *thread_info;
	thread_info = pthread_get_info ( lwip_thread[j].tid);
	if(thread_info)  {
	  pid_t tpid=thread_info->parent->pid;
	  if(  thread_info) {
	    if( tpid==kstats.pstats[i].pid) {
	      stackaddr=(uint32_t)thread_info->thread_attr.stackaddr;
	      stacksize=thread_info->thread_attr.stacksize;
	      stackptr=thread_info->parent->pcontext.regs[CTX_INDEX_GPR(1)];

	    //	if ( lwip_thread[j].tid == ptable[kstats.pstats[i].pid].thread->tid ) {
	    
	    for(l=0;l<12;l++) add8(sys_thread_name[j][l]);
	    break;
	    }
	  }	
	}
      }
      if(j==SYS_THREAD_MAX) { 
	for(l=0;l<12;l++) add8(unknown_task[l]); // thread name not found
      }
      add32(kstats.pstats[i].pid); //PID
      add8(kstats.pstats[i].state); // process state
      add32(kstats.pstats[i].aticks); // ticks 
      add8(kstats.pstats[i].priority);
      add32(stackaddr); // stack info
      add32(stacksize);
      add32(stackptr);

    }

    add32(3); //number of netstat blocks
    netstat(lwip_stats.link);
    netstat(lwip_stats.tcp);
    netstat(lwip_stats.udp);
    //    leak=new uint32_t[512];
    //leak++;
    struct mallinfo mi=mallinfo();
    add32(mi.arena);
    add32(mi.uordblks);
    add32(mi.fordblks);
    add32(mi.keepcost);

    uint32_t nsem = 0;
    for (int i=0; i<MAX_SEM;i++) {
      if(sem_heap[i].sem_id != -1 ) 
	nsem++;
    }
    
    add32(nsem);
    
    
    int res = sendto(this->socketId, buffer, len,0, (const struct sockaddr *) &server, sizeof(struct sockaddr_in));

    if (res<0)
       xil_printf("send error");
    ++num;
    sleep(10000);
  }

#endif

  
}





