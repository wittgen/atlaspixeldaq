/* 
 * File:   RecoAtECRFei3.h
 * 
 * Represents the Reconfiguration at ECR class.
 */

#ifndef RECO_AT_ECR_FEI3_H
#define	RECO_AT_ECR_FEI3_H

#include "RecoAtECR.h"

class Fei3Mod;


class RecoAtECRFei3 : public RecoAtECR {

public:
    
#ifdef __XMK__

    static RecoAtECR* getInstance();

    virtual void PrepareBitStreams(uint32_t modMask);
    virtual void SingleLoad(uint32_t modMask);
    void addGlobalConfig(Boclib::Bitstream& bitstream, int iFE, Fei3Mod* fePtr);
    void addMccConfig(Boclib::Bitstream& bitstream,  Fei3Mod* fePtr);
    void addMccWrRegister(Boclib::Bitstream& bitstream, uint32_t address, uint32_t value);
    void addMccWrFrontEnd(Boclib::Bitstream& bitstream, const std::vector<uint32_t> &data);
    void addMccGlobalReset(Boclib::Bitstream& bitstream);
    void addFEGlobalReset(Boclib::Bitstream& bitstream, uint32_t syncW);
    void addMccEnableDataTake(Boclib::Bitstream& bitstream);
     void addECR(Boclib::Bitstream& bitstream);

    protected:
    RecoAtECRFei3(); // Prevent construction
    RecoAtECRFei3(const RecoAtECRFei3&); // Prevent construction by copying
    RecoAtECRFei3& operator=(const RecoAtECRFei3&); // Prevent assignment
    ~RecoAtECRFei3(); // Prevent unwanted destruction

#endif
    
    
};

#endif	/* RECO_AT_ECR_H */

