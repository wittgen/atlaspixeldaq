/* 
 * File:   RecoAtECR.h
 * 
 * Represents the Reconfiguration at ECR class.
 */

#ifndef RECO_AT_ECR_H
#define	RECO_AT_ECR_H

#include "CppCompatibility.h" // Keep this header first!
#include "EasySerializable.h"


#include <vector>
#include <list>
#include "IblBoc.h"
#include "Bitstream.h"
#include <thread>
#include <mutex>
#include <condition_variable>


class RecoAtECR {
public:

     enum RecoAtECR_mode {
    RECO_OFF = 0, RECO_ON_IDLE, RECO_ON, GLOB_RECO, PIX_RECO, GLOB_PIX_RECO, CUSTOM_BS, ALLOW_RECO, FULL_CONFIG,READ_BACK_GLOB};

      class RecoConfig : public EasySerializable {
    public:
    
      RecoConfig(){init();}


      //Common
      uint32_t mask;
      int verbosityLevel;
      bool customBS;
      uint32_t sleepDelay;
      bool debug;
      bool allowReconfig;
      bool readBackCheck;
      //FE-I4 specific
      bool glbRegCfg;
      bool pixRegCfg;
      uint32_t pixmask;
      uint16_t latchesMask;
      int latchesRotation;
      bool doHitBus;
      //FE-I3 specific
      bool MccCfg;
      bool fei3GlobCfg;
      bool softReset;

     void init(){
       mask = 0xffffffff;
       verbosityLevel = 0;
       customBS  = false;
       sleepDelay=10; 
       debug  = false;
       allowReconfig = false;
       readBackCheck = false;
       //FE-I4 specific
       glbRegCfg = true;
       pixRegCfg = false;
       pixmask = 0x0; 
       latchesMask = 0x0;
       latchesRotation = 3;
       doHitBus    = true;
       //FE-I3 specific
       MccCfg = false;
       fei3GlobCfg = false;
       softReset = true;
       
     }
     
     void PrintCfg(){
       printf("######### COMMON ######\n");
       printf("mask \t\t0x%.8X \n", (unsigned int)mask);
       printf("verbosityLevel \t%d \n", verbosityLevel);
       printf("debug \t\t%d \n", debug);
       printf("readBackCheck \t%d \n", readBackCheck);
       printf("customBS \t\t%d \n", customBS);
       printf("allowReconfig \t%d \n", allowReconfig);
       printf("sleepDelay \t%d \n", (int)sleepDelay);
       printf("pixRegCfg \t%d \n", pixRegCfg );
       printf("######### FE-I4 specific ######\n");
       printf("glbRegCfg \t%d \n", glbRegCfg);
       printf("pixmask \t\t0x%.8X \n", (unsigned int)pixmask);
       printf("latchesRotation \t%d \n", latchesRotation);
       printf("latchesMask \t0x%.8X \n", latchesMask);
       printf("#########FE-I3 specific ######\n");
       printf("MccCfg \t%d \n", MccCfg );
       printf("fei3GlobCfg \t%d \n", fei3GlobCfg );
       printf("softReset \t%d \n", softReset );
       
     }
 
   SERIAL_H(prep(mask); prep(verbosityLevel); prep(customBS); prep(sleepDelay); prep(debug); prep(allowReconfig); prep(readBackCheck); prep(glbRegCfg); prep(pixRegCfg); prep(pixmask); prep(latchesMask); prep(latchesRotation); prep(doHitBus);prep(MccCfg); prep(fei3GlobCfg); prep(softReset); )
  
  };


    void init();



    void setRecoAtECRConfig( RecoConfig recoConf ) {
      m_recoConfig = recoConf;      
    }

    bool getHitBusFlag() {
      return m_recoConfig.doHitBus;
    }
    
    void setHitBusFlag(bool doHitBus){
      m_recoConfig.doHitBus = doHitBus;
    }
    
    int getRecoAtECRMode() {
      return m_recoAtECRMode;
    }

    void setRecoAtECRMode(RecoAtECR_mode mode) {
      m_recoAtECRMode = mode;
      cv.notify_one(); // wake-up thread if sleeping
    }

    void setPixRegCfg(bool PixRegCfg) {
      m_recoConfig.pixRegCfg = PixRegCfg;
    }

    void setGlbRegCfg(bool GlbRegCfg) {
      m_recoConfig.glbRegCfg = GlbRegCfg;
    }

   void setAllowReconfig(bool AllowReconfig) {
     m_recoConfig.allowReconfig = AllowReconfig;
   } 

    void setReadBackCheck(bool readBackCheck) {
      m_recoConfig.readBackCheck = readBackCheck;
    }
    
    bool getReadBackCheck() {
      return m_recoConfig.readBackCheck;
    }
    
    void resetMask() {
      m_recoConfig.mask = 0;
    }

    void setMask(uint32_t newMask) {
      m_recoConfig.mask = newMask;
    }

    uint32_t getMask() {
      return m_recoConfig.mask;
    }
    
    void setSleepDelay(uint32_t delay) {
      m_recoConfig.sleepDelay = delay;
    }
    uint32_t getSleepDelay() {
      return m_recoConfig.sleepDelay;
    }

    /**
     * Runs the server.
     */

    void run();

    
    //OKUPDATE Very bad style c++ mutexing
    std::mutex mtx_thread;			// wake/sleep the thread
    static std::mutex mtx_cfg;			    // synchronize config sending
    std::condition_variable cv;


#ifdef __XMK__
    static RecoAtECR* instance;

    void setVerbosityLevel(int level) {
      m_recoConfig.verbosityLevel = level;
    }
    
    int getVerbosityLevel() {
      return m_recoConfig.verbosityLevel;
    }

    void setCustomBS(bool customBS){
      m_recoConfig.customBS = customBS;
    }

    void setLatchesMask(uint16_t latchesMask){
      m_recoConfig.latchesMask = latchesMask;
    }

    //Wait for ECRBusy, Tx stream has been sent?
    void waitForECRBusy(const int txCh);

    void PrepareAndLoad();
    virtual void PrepareBitStreams(uint32_t modMask )=0;
    virtual void SingleLoad(uint32_t modMask)=0;
    void resetFIFOs(bool all);
    void LoadFIFOs();
    bool checkForECRArrived(bool wait);
    void PrintCfg();

#endif
    
    std::map<uint8_t, Boclib::Bitstream> m_ECRBitstreamMap;

    RecoConfig m_recoConfig;
    
    RecoAtECR_mode m_recoAtECRMode;

    Boclib::Bitstream m_clear_bitstream;
    
    bool m_loadBitStream;
    bool m_FifoLoaded;
    uint32_t controlEcrCounter;
    uint32_t currentEcrCounter;
    
    protected:
    
    RecoAtECR();
    virtual~RecoAtECR();

};

#endif	/* RECO_AT_ECR_H */

