/* 
 * File:   RecoAtECRFei4.h
 * 
 * Represents the Reconfiguration at ECR class.
 */

#ifndef RECO_AT_ECR_FEI4_H
#define	RECO_AT_ECR_FEI4_H

#include "RecoAtECR.h"

class Fei4;


class RecoAtECRFei4 : public RecoAtECR {

public:
    
    
#ifdef __XMK__

    static RecoAtECR* getInstance();

    uint8_t getLatchFromMask(uint16_t mask);
    uint32_t writeRegisterHeader(unsigned i, uint8_t chipID) ;
    uint32_t globalPulse(uint32_t chipID, uint32_t width);
    void addSRTo1(Boclib::Bitstream& bitstream, Fei4* fePtr, uint32_t width = 10);
    void PulseLatchTo1(Boclib::Bitstream& bitstream, Fei4* fePtr, uint32_t pixelStrobeValue, int dc);
    void addRunMode(Boclib::Bitstream& bitstream, Fei4* fePtr, bool mode);
    void addStrobePixelLatches(Boclib::Bitstream& bitstream, Fei4* fePtr);
    void addGlobalConfiguration(int currentTx, Fei4* fePtr);
    void addPixelConfiguration(int currentTx, int configCounter, uint32_t DC, Fei4* fePtr); 
    void writeDoubleColumnCfg(Boclib::Bitstream & bitstream, Fei4* fePtr, uint8_t latch, uint32_t pixelStrobeValue, uint8_t dc);
    void addAddrValue(Boclib::Bitstream &bitstream, const std::pair<uint32_t, uint16_t> addr_value);
    virtual void PrepareBitStreams(uint32_t modMask);
    virtual void SingleLoad(uint32_t modMask);

    uint8_t HitBus_Latch;
    uint8_t m_index;
    uint32_t DC_counter;
    
    protected:
    RecoAtECRFei4();// Prevent construction
    RecoAtECRFei4(const RecoAtECRFei4&); // Prevent construction by copying
    RecoAtECRFei4& operator=(const RecoAtECRFei4&); // Prevent assignment
    ~RecoAtECRFei4(); // Prevent unwanted destruction


#endif
    
    
};

#endif	/* RECO_AT_ECR_H */

