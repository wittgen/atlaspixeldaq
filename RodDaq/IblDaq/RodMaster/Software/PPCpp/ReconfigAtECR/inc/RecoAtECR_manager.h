/*
 * File : RecoAtECR_manager.h
 * 
 * Configuration of the Reconfiguration at ECR thread
 */

#ifndef RECO_AT_ECR_MANAGER_H
#define RECO_AT_ECR_MANAGER_H

#include "CppCompatibility.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#include "IblBoc.h"
#include "Bitstream.h"
#endif

#include "Command.h"
#include  "Serializer.h"
#include "GeneralResults.h"
#include "RecoAtECR.h"

class RecoAtECR;

class RecoAtECR_manager: public CommandCRTP<RecoAtECR_manager> {
 public:
  InstantiateResult(EmptyResult);
  
  void dump();
  RecoAtECR_manager();
  virtual void execute();
  
  virtual void serialize(uint8_t *out) const;
  virtual void deserialize(const uint8_t *in);
  virtual uint32_t serializedSize() const;
  
  virtual ~RecoAtECR_manager() {}

  
  //Instance to the RecoAtECR
  RecoAtECR* p_recoAtECR;

  RecoAtECR::RecoAtECR_mode mode;
  RecoAtECR::RecoConfig m_recoConfig;

};

#endif /* RECO_AT_ECR_MANAGER_H */

