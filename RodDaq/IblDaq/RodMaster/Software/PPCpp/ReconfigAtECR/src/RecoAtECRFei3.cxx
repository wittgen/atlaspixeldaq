#include "RecoAtECRFei3.h"

#include "time.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodResourceManager.h"
#include "DataTakingTools.h"
#endif

#include "RodMasterRegisters.h"
#include <algorithm>
#include <iterator>
#include <iomanip>

#include <unistd.h>
//#include "primXface_common.h"

#ifdef __XMK__

RecoAtECR* RecoAtECRFei3::getInstance() {
    if(instance == 0) {
        instance = new RecoAtECRFei3();
        instance->run();
    }
    if(!instance)
        std::cout << "ERROR: couldn't create RecoAtECR instance!!!" << std::endl;

    printf("RecoAtECR instance: 0x%08x \n", (unsigned int)instance);
    return instance;
}

RecoAtECRFei3::RecoAtECRFei3():RecoAtECR(){

}

RecoAtECRFei3::~RecoAtECRFei3(){


}


void RecoAtECRFei3::PrepareBitStreams(uint32_t modMask) {

  //should be redundant - better be sure
  if (RodResMgr::isFei4()) {
    xil_printf("\n Skipping Reconfig at ECR for IBL RODs.. \n");
    return;
  }
  
  if (m_loadBitStream) {

    uint32_t iFE = controlEcrCounter % 16;

    std::unique_lock<std::mutex> lk(mtx_cfg);

    for (uint32_t mod = 0; mod< RodResourceManagerPIX::getNModuleConfigs(); mod++){

      if (m_recoAtECRMode < RECO_ON )break;

      if (!(modMask & (1<<mod )) )continue;
      Fei3Mod &fe = RodResourceManagerPIX::getCalibModuleConfig(mod);
      if(fe.revision ==0)continue;

      int currentTx = fe.getTxCh();

        if (m_recoConfig.verbosityLevel>0)
	  std::cout<<"Tx line:"<<currentTx<<" FE "<<(int)iFE <<std::endl;

        if (m_recoConfig.customBS) {
	  xil_printf("Appending custom stuff to the BitStream \n");
	  m_ECRBitstreamMap[currentTx].Append32(0xF0F0F0F1);
	}

      uint8_t sync = m_recoConfig.softReset ? 4 : 0;
      addFEGlobalReset(m_ECRBitstreamMap[currentTx],sync);

        if (m_recoConfig.MccCfg) {
          addMccGlobalReset(m_ECRBitstreamMap[currentTx]);
	  addMccConfig(m_ECRBitstreamMap[currentTx],&fe);
	}

	if (m_recoConfig.fei3GlobCfg )
	  if(fe.maskEnableFEConfig & (0x1 << iFE) )
	    addGlobalConfig(m_ECRBitstreamMap[currentTx], iFE,&fe);

	addMccEnableDataTake(m_ECRBitstreamMap[currentTx]);

	  //Append ECR, this is needed because if not we go desync! Check FE-I3 manual
	  if(m_recoConfig.MccCfg || m_recoConfig.fei3GlobCfg)
	    addECR(m_ECRBitstreamMap[currentTx]);
    }
  }

}

//Only load MCC config, do we want this?
void RecoAtECRFei3::SingleLoad(uint32_t modMask) {

  //Start with empty FIFOs and clear map
  resetFIFOs(true);

  if(modMask == 0x0){std::cout<<"Mask is 0. Just resetting the FIFOs and shutting down ECR"<<std::endl;return;}
  if(!m_recoConfig.glbRegCfg) {std::cout<<"Global configuration is disabled. Just resetting the FIFOs and shutting down ECR"<<std::endl;return;}

  //should be redundant - better be sure
  if (RodResMgr::isFei4()) {
    std::cout<<"\n Skipping Reconfig at ECR for IBL RODs.. \n"<<std::endl;
    return;
  }
  
  for (uint32_t mod = 0; mod< RodResourceManagerPIX::getNModuleConfigs(); mod++){

     //std::cout<<"Mod mask "<<std::hex<<modMask<<" "<<(1<<mod )<<std::dec<<std::endl;
     if (!(modMask & (1<<mod )) )continue;
     Fei3Mod &fe = RodResourceManagerPIX::getCalibModuleConfig(mod);
     if(fe.revision ==0)continue;

       int currentTx = fe.getTxCh();

       if (m_recoConfig.verbosityLevel>0)
	 std::cout<<"SingleLoad Tx line:"<<currentTx<<std::endl;

       uint8_t sync = m_recoConfig.softReset ? 4 : 0;
       addFEGlobalReset(m_ECRBitstreamMap[currentTx],sync);
         if(m_recoConfig.MccCfg){
           addMccGlobalReset(m_ECRBitstreamMap[currentTx]);
           addMccConfig(m_ECRBitstreamMap[currentTx],&fe);
         }
       addMccEnableDataTake(m_ECRBitstreamMap[currentTx]);

         //Append ECR, this is needed because if not we go desync! Check FE-I3 manual
         if(m_recoConfig.MccCfg)
           addECR(m_ECRBitstreamMap[currentTx]);
       
   }

  LoadFIFOs();
  //clear the map
  m_ECRBitstreamMap.clear();
 
}

void RecoAtECRFei3::addGlobalConfig(Boclib::Bitstream& bitstream, int iFE, Fei3Mod* fePtr){

// compute parity
    fePtr->writeGlobalReg(iFE, Fei3::GlobReg::GlobalParity, 0); // reset parity
    fePtr->writeGlobalReg(iFE, Fei3::GlobReg::HitBusScaler, 0); // reset read-only HitBusScaler
    uint16_t curMonLeakADC = fePtr->m_chipCfgs[iFE].readRegGlobal(Fei3::GlobReg::MonLeakADC);
    curMonLeakADC &= 0x1FFF;
    fePtr->writeGlobalReg(iFE, Fei3::GlobReg::MonLeakADC, curMonLeakADC); // reset read-only MonLeakADC(13)
    int nOnes = 0;
    for (unsigned iword = 0; iword < Fei3GlobalCfg::nWordsGlobal; iword++) nOnes += __builtin_popcount(fePtr->m_chipCfgs[iFE].globcfg[iword]); // count ones
    fePtr->writeGlobalReg(iFE, Fei3::GlobReg::GlobalParity, (nOnes % 2 == 0) ? 0 : 1); // set parity

    addMccWrRegister(bitstream, CNT, (231 << 3) | 0x4); // write to CNT the length of the data
    std::vector<uint32_t> clockGlobalData;
    Fei3Cmd::clockGlobal(iFE, clockGlobalData, fePtr->m_chipCfgs[iFE].globcfg);

    addMccWrFrontEnd(bitstream, clockGlobalData); // clock the global config to the strobes
    addMccWrRegister(bitstream, CNT, 0x4); // write to CNT the length of the WriteGlobal command
    std::vector<uint32_t> writeGlobalData;
    Fei3Cmd::writeGlobal(iFE, writeGlobalData);
    addMccWrFrontEnd(bitstream, writeGlobalData); // write the global config
    std::vector<uint32_t> empty;
    empty.push_back(0x0);
    addMccWrFrontEnd(bitstream,empty);

}


void RecoAtECRFei3::addMccConfig(Boclib::Bitstream& bitstream,  Fei3Mod* fePtr){

    addMccWrRegister(bitstream, CSR,   fePtr->mccRegisters[CSR]);
    addMccWrRegister(bitstream, LV1,   fePtr->mccRegisters[LV1]);
    addMccWrRegister(bitstream, FEEN,  fePtr->mccRegisters[FEEN]);
    addMccWrRegister(bitstream, WFE,   fePtr->mccRegisters[WFE]);
    addMccWrRegister(bitstream, WMCC,  fePtr->mccRegisters[WMCC]);
    addMccWrRegister(bitstream, CNT,   fePtr->mccRegisters[CNT]);
    addMccWrRegister(bitstream, CAL,   fePtr->mccRegisters[CAL]);
    addMccWrRegister(bitstream, PEF,   fePtr->mccRegisters[PEF]);
    addMccWrRegister(bitstream, SBSR,  fePtr->mccRegisters[SBSR]);
    addMccWrRegister(bitstream, WBITD, fePtr->mccRegisters[WBITD]);
    addMccWrRegister(bitstream, WRECD, fePtr->mccRegisters[WRECD]);

}

void RecoAtECRFei3::addMccGlobalReset(Boclib::Bitstream& bitstream){

  bitstream.Append32((0x16B << 8) + (0x9 << 4));

}

// syncW < 4 SYNC RESET;  4 <= syncW < 8 SOFT RESET; syncW >= 8 HARD RESET 
void RecoAtECRFei3::addFEGlobalReset(Boclib::Bitstream& bitstream, uint32_t syncW){

  bitstream.Append32((0x16B << 12) + (0xA << 8) + (0x0 << 4) + (syncW & 0xF));

}

void RecoAtECRFei3::addMccEnableDataTake(Boclib::Bitstream& bitstream){

  bitstream.Append32((0x16B << 8) + (0x8 << 4));

}

void RecoAtECRFei3::addECR(Boclib::Bitstream& bitstream){

  bitstream.Append32(0x162);

}



void RecoAtECRFei3::addMccWrRegister(Boclib::Bitstream& bitstream, uint32_t address, uint32_t value){

  bitstream.Append32(0x1);
  bitstream.Append32((0x6B << 24) + (0x0 << 20) + ((address & 0xF) << 16) + (value & 0xFFFF));

}


void RecoAtECRFei3::addMccWrFrontEnd(Boclib::Bitstream& bitstream, const std::vector<uint32_t> &data){

  bitstream.Append32((0x16B << 8) + (0x4 << 4));

  std::vector<uint32_t> data5MHz;
  data5MHz.reserve(data.size() * 8);
  //Data has to be converted into 5MHz
  Fei3ModCmd::convert40MHzTo5MHz(data, data5MHz);
  for (size_t i = 0; i < data5MHz.size(); i++)bitstream.Append32(data5MHz[i]);

}

#endif
