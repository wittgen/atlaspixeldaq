
#include "RecoAtECRFei4.h"
#include "Client.h"

#include "time.h"

#ifdef __XMK__
#include "HwRegsInCpp.h"
#include "RodResourceManager.h"
#include "DataTakingTools.h"
#endif

#include "RodMasterRegisters.h"
#include <algorithm>
#include <iterator>
#include <iomanip>
#include "Fei4Proxy.h"

#include <unistd.h>
//#include "primXface_common.h"

#ifdef __XMK__
static const size_t Nlatches = 10;
static const uint8_t latches [Nlatches] = {0, 1, 2, 3, 4, 5, 9, 10, 11, 12 };

RecoAtECR* RecoAtECRFei4::getInstance() {
    if(instance == 0) {
        instance = new RecoAtECRFei4();
        instance->run();
    }
    if(!instance)
        std::cout << "ERROR: couldn't create RecoAtECR instance!!!" << std::endl;
    printf("RecoAtECR instance: 0x%08x \n", (unsigned int)instance);
    return instance;
}

RecoAtECRFei4::RecoAtECRFei4():RecoAtECR(){

   DC_counter = 0;
   HitBus_Latch =  8;
   m_index =0;
}

RecoAtECRFei4::~RecoAtECRFei4(){


}

void RecoAtECRFei4::PrepareBitStreams(uint32_t modMask) {

  //should be redundant - better be sure
  if (RodResMgr::isFei3()) {
    xil_printf("\n Skipping Reconfig at ECR for Pixel RODs.. \n");
    return;
  }

  //xil_printf("Reco At ECR Mode: %d \n", m_recoAtECRMode);

  if (m_loadBitStream) {

    if (m_recoConfig.debug)
      xil_printf("Loop Counter %d \n", (int) DC_counter);

    //uint32_t DC = DC_counter % 40;
    //Get which DC index 0-39 depending on the ECR
    uint32_t DC = controlEcrCounter % 40;
    
    //These two variables are needed to shape the Bitstream writing:
    // The 1st is to skip writing RunMode(False) twice
    // The 2nd is to skip writing RunMode(True ) twice
    int configCounter = 0;

    std::unique_lock<std::mutex> lk(mtx_cfg);

   //Set run mode to false
   for (uint32_t mod = 0; mod< RodResourceManagerIBL::getNModuleConfigs(); mod++){

     if ( !(modMask & (1<<mod)) )continue;
     Fei4 &fe = RodResourceManagerIBL::getCalibModuleConfig(mod);
     if (fe.getRevision() ==0 )continue;

     addRunMode(m_ECRBitstreamMap[fe.getTxCh()],&fe,false);
   }

   //LoadConfig
   for (uint32_t mod = 0; mod< RodResourceManagerIBL::getNModuleConfigs(); mod++){

      //In order to interrupt in the case requested externally
      if (m_recoAtECRMode < RECO_ON )break;

      if ( !(modMask & (1<<mod)) )continue;
     
      Fei4 &fe = RodResourceManagerIBL::getCalibModuleConfig(mod);

      if (fe.getRevision() ==0 )continue;

	if (m_recoConfig.verbosityLevel>0) { 
	  std::cout<<"Tx line:"<<(int) fe.getTxCh()<<std::endl;
	  std::cout<<"Chip ID:"<<(int) fe.getChipID()<<std::endl;
	}

	int currentTx = fe.getTxCh();

	if (m_recoConfig.customBS) {
	  xil_printf("Appending custom stuff to the BitStream \n");
	  m_ECRBitstreamMap[currentTx].Append32(0xF0F0F0F1);
	}

	if (m_recoConfig.glbRegCfg) {
	  addGlobalConfiguration(currentTx, &fe);
	}

	if (m_recoConfig.pixRegCfg && (((m_recoConfig.pixmask & modMask) & (1<<mod) ))) {
	  addPixelConfiguration(currentTx, configCounter, DC, &fe);
	}

	configCounter++;

	if (configCounter==2)  {
	  //closing the bitstream
	  //m_ECRBitstreamMap[currentTx].Append8(0x00); //=>shouldn't be necessary with latest Marius BOC FW

	  //Set both Rxs at DC=0
	  if (m_recoConfig.allowReconfig) {
	    std::cout<<"Restoring DCs to 0 in broadcast"<<std::endl;
	    m_ECRBitstreamMap[currentTx].Append32(0x005A0800+((8<<6)&0x3C0)+(22&0x3F));
	    m_ECRBitstreamMap[currentTx].Append32(0x00000000);
	  }
	  if (m_recoConfig.verbosityLevel>0)
	    std::cout<<"Written GR::bitstream lenght="<<(int) (m_ECRBitstreamMap[currentTx].GetData()).size() << " for Tx "<<(int) currentTx<<std::endl;
	  configCounter=0;
	}

    } //Loop on the FEs 

   //Set run mode to true
   for (uint32_t mod = 0; mod< RodResourceManagerIBL::getNModuleConfigs(); mod++){
     if ( !(modMask & (1<<mod)) )continue;
     Fei4 &fe = RodResourceManagerIBL::getCalibModuleConfig(mod);
     if (fe.getRevision() ==0 )continue;
       addRunMode(m_ECRBitstreamMap[fe.getTxCh()],&fe,true);
   }

    DC_counter++;
  }//LoadtheFIFOs
}

#endif

#ifdef __XMK__

void RecoAtECRFei4::addAddrValue(Boclib::Bitstream &bitstream, const std::pair<uint32_t, uint16_t > addr_value) {
  bitstream.Append32(addr_value.first);
  bitstream.Append16(addr_value.second);
  if (m_recoConfig.verbosityLevel>1) {
    std::cout<<"addAddrValue::bitstream lenght="<<(bitstream.GetData()).size()<<std::endl;
  }
  
}

uint32_t RecoAtECRFei4::writeRegisterHeader(unsigned i, uint8_t chipID) {
  //std::cout<<"Write Register Header (addr="<<(int)i<<") 0x"<<std::hex<<(int)word<<std::dec<<std::endl;
  uint32_t word = 0x005A0800|i|(chipID<<6);
  return word;
}

void RecoAtECRFei4::addRunMode(Boclib::Bitstream &bitstream, Fei4* fePtr, bool mode) {
  //when configuring the chip, need to set Vthreshold high first
  //then set Vthreshold back to waht it was previously before running
  //Vthreshold = register 20
  
  uint32_t modeBits = mode ? 0x38 : 0x07;
  
  
  if (mode == false) {
    //Set to config mode
    bitstream.Append32(0x005A2800+(((fePtr->getChipID())<<6)&0x3C0)+modeBits);
    
    //Set Threshold high - the two methods are the same.
    //bitstream.Append32(0x005A0800+((8<<6)&0x3C0)+(20&0x3F));  
    bitstream.Append32(writeRegisterHeader(20,fePtr->getChipID()));
    bitstream.Append16(0xFFFF);
    
    //if (m_recoConfig.verbosityLevel>1) {
    //std::cout<<"RunMode(False)::bitstream lenght="<<(int)(bitstream.GetData()).size()<<std::endl;
    //}
    
  }
  else{
    //Set the thresholds back if preAmps on else leave them high.
    
    if (fePtr->isPreAmpsOn() || 15 == fePtr->getRxCh() || 16 == fePtr->getRxCh())
      {
	//Access the thresholds and set them back.
	bitstream.Append32(writeRegisterHeader(20,fePtr->getChipID()));
	bitstream.Append16(fePtr->getGlobCfgRegValue(20));
      }
    //Set back to run mode
    bitstream.Append32(0x005A2800+((fePtr->getChipID()<<6)&0x3C0)+modeBits);
    if (m_recoConfig.verbosityLevel>1) {
      std::cout<<"RunMode(True)::bitstream lenght="<<(bitstream.GetData()).size()<<std::endl;
    }
  }
}

uint8_t RecoAtECRFei4::getLatchFromMask(uint16_t mask) {
  uint8_t latchValue = 0;
  for (;mask;mask>>=1) {
    if (mask!=0)
      latchValue+=1;
  }
  return latchValue;
}

void RecoAtECRFei4::writeDoubleColumnCfg(Boclib::Bitstream &bitstream, Fei4* fePtr, uint8_t latch, uint32_t pixelStrobeValue, uint8_t dc) {
  
  if (m_recoConfig.verbosityLevel > 0)
    std::cout<<"writeDoubleColumnCfg::latch="<<(int) latch<<" pixelStrobeValue="<<(int) pixelStrobeValue<<" dc="<<(int) dc<<std::endl;
  
  DoubleColumnBit dcb = fePtr->dcb(dc,latch);
  

  // set both FEs to double column 40 - protection for the WriteFrEnd command -  check Fei4Proxy.h
  bitstream.Append32(0x005A0800+((8<<6)&0x3C0)+(22&0x3F));
  bitstream.Append32(0x00140000);
  

  //Select double column - for the correct FE   (internally has a check on the fePtr chip ID)
  if (m_recoConfig.verbosityLevel>1) std::cout<<"SelectDoubleColumn"<<std::endl;
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::Colpr_Addr,dc));
  
  
  //Prepare front-end -- Broadcast->Check Fei4::strobePixelLatches
  bitstream.Append32(0x005A1000+((8<<6)&0x3C0));
  
  for (unsigned i = 0; i <  DoubleColumnBit::nWords;++i) {
    bitstream.Append32(Fei4Cmd::reverse(dcb.getDoubleColumnBit()[i]));
  }
  if (m_recoConfig.verbosityLevel > 1) std::cout<<"writeFrontEnd::bitstream lenght="<<(int)(bitstream.GetData()).size()<<std::endl;
 
  
  //This is added customly in the prepFrontEnd
  bitstream.Append32(0x0);
  
  if (m_recoConfig.verbosityLevel > 1) std::cout<<"Adding 32bit word::bitstream lenght"<<(int)(bitstream.GetData()).size()<<std::endl;
  
  //Select which latch to strobe
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::Pixel_latch_strobe,0x1FFF & pixelStrobeValue));
  //Strobe it
  addStrobePixelLatches(bitstream,fePtr);
  
  if (m_recoConfig.verbosityLevel > 1) std::cout<<"StrobeLatches+addStrobeLatches+StrobeLatches::bitstream lenght"<<(int)(bitstream.GetData()).size()<<std::endl;

}

uint32_t RecoAtECRFei4::globalPulse( uint32_t chipID,  uint32_t width) {
  uint32_t word = 0x005A2400+((chipID<<6)&0x3C0)+(width&0x3F);
  return word;
  
}

void RecoAtECRFei4::PulseLatchTo1(Boclib::Bitstream& bitstream, Fei4* fePtr, uint32_t pixelStrobeValue, int dc) {
  
  if (m_recoConfig.verbosityLevel>1)
    xil_printf("PulseLatchTo1::pixelStrobeValue=%d dc=%d", (int) pixelStrobeValue, (int) dc);
  
  //Decide if to pulse all the dcs or only partial:
  if (dc == -1)
    addAddrValue(bitstream,fePtr->returnRegister(&Fei4::CP,0x3));
  else 
    {
      addAddrValue(bitstream,fePtr->returnRegister(&Fei4::Colpr_Addr,dc));
    }
  //add 1 to the Shift Register
  addSRTo1(bitstream,fePtr);
  //Select which latch to strobe
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::Pixel_latch_strobe,0x1FFF & pixelStrobeValue));
  //Strobe it
  addStrobePixelLatches(bitstream,fePtr);
  
  //In the case I wanted to pulse all the columns return to single DC selection right after.
  if (dc==-1)
    addAddrValue(bitstream,fePtr->returnRegister(&Fei4::CP,0x0));
  
}

void RecoAtECRFei4::addSRTo1(Boclib::Bitstream& bitstream, Fei4* fePtr, uint32_t width) {
  
  
  //SR in parallel mode
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S0, 0x1));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S1, 0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::HLD,0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::SRK,0x1));
  
  //Add the global pulse - not in broadcast for the moment.
  bitstream.Append32(globalPulse(fePtr->getChipID(), width));
  
  //Add some time for pulse to propagate
  bitstream.Append16(0x0);
  
  //Return with the SR to normal mode
    
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S0, 0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S1, 0x0)); //redundant
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::HLD,0x0)); //redundant
  //Restore the SRK value to the original one
  //SRK is the bit 1 of Reg 27 (This register is usually set to 0x8000 -> SRK goes back to 0)
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::SRK,0x0));
}


void RecoAtECRFei4::addPixelConfiguration(int currentTx, int configCounter, uint32_t DC, Fei4* fePtr) {
  if (m_recoConfig.debug || m_recoConfig.verbosityLevel > 0 )
    xil_printf("Reconfiguring single pixel Latches \n");
  
  addAddrValue(m_ECRBitstreamMap[currentTx],fePtr->returnRegister(&Fei4::CP,0x0));
  
  uint8_t latch = 0; 	    
  if (!m_recoConfig.latchesRotation) {
    for (uint16_t currentLatchMask = 0x1; currentLatchMask <= m_recoConfig.latchesMask; currentLatchMask<<=1) {
      if (m_recoConfig.latchesMask & currentLatchMask) {
	writeDoubleColumnCfg(m_ECRBitstreamMap[currentTx],fePtr,latch,0x1<<latch,DC);
      }
      latch+=1;
    }
  }
  
  else { 
    int delaySize = m_recoConfig.sleepDelay;
    //xil_printf("m_recoConfig.latchesRotation %d \n", m_latchesRotation);  
    for (uint8_t ilatch=m_index; ilatch<m_index+m_recoConfig.latchesRotation; ilatch++) { 
      if (m_recoConfig.verbosityLevel > 0)
	xil_printf("TX %d m_index %d latchindex %d latchToBeCFG %d DC %d \n",currentTx, m_index,ilatch%Nlatches,latches[ilatch % Nlatches], DC);  
      writeDoubleColumnCfg(m_ECRBitstreamMap[currentTx],fePtr,latches[ilatch % Nlatches],0x1<<latches[ilatch % Nlatches],DC);
      //insert the delay
      if (ilatch==m_index+m_recoConfig.latchesRotation - 2) {
	//xil_printf("Delay\n");  
	for (int idelay=0; idelay<delaySize;idelay++) {
	  m_ECRBitstreamMap[currentTx].Append32(0x0);}
      }
    }
    
    if (DC==39 && configCounter == 1)
      m_index = (m_index+m_recoConfig.latchesRotation) % Nlatches;
    
  }//LatchesRotation
  
  if (m_recoConfig.doHitBus)//Pulse the HitBus latches 
    PulseLatchTo1(m_ECRBitstreamMap[currentTx], fePtr, 0x1<<HitBus_Latch,DC);
  
  //Reset ColPair address to 0
  addAddrValue(m_ECRBitstreamMap[currentTx],fePtr->returnRegister(&Fei4::Colpr_Addr,0x0));
}

void RecoAtECRFei4::SingleLoad(uint32_t modMask) { 

  //Start with empty FIFOs and clear map
  resetFIFOs(true);
  

  if(modMask== 0x0){std::cout<<"Mask is 0. Just resetting the FIFOs and shutting down ECR"<<std::endl;return;}
  if(!m_recoConfig.glbRegCfg) {std::cout<<"Global configuration is disabled. Just resetting the FIFOs and shutting down ECR"<<std::endl;return;}

  //should be redundant - better be sure
  if (RodResMgr::isFei3()) {
    xil_printf("\n Skipping Reconfig at ECR for Pixel RODs.. \n");
    return;
  }
  
    for (uint32_t mod = 0; mod< RodResourceManagerIBL::getNModuleConfigs(); mod++){
      if ( !(modMask & (1<<mod)) )continue;
      Fei4 &fe = RodResourceManagerIBL::getCalibModuleConfig(mod);
      if (fe.getRevision() ==0 )continue;
      int currentTx = fe.getTxCh();
      //Set the FE in Configuration Mode
      addRunMode(m_ECRBitstreamMap[currentTx],&fe,false);
      addGlobalConfiguration(currentTx, &fe);
      addRunMode(m_ECRBitstreamMap[currentTx],&fe,true);
    }

   LoadFIFOs();
   //clear the map
   m_ECRBitstreamMap.clear();

}

void RecoAtECRFei4::addGlobalConfiguration(int currentTx, Fei4* fePtr) {
  if (m_recoConfig.debug || m_recoConfig.verbosityLevel >0)
    xil_printf("Setting up global reconfiguration \n");
  //BitStream for global configuration
  // - TODO - 
  // Remove hardcoding of the global register number
  for (unsigned int i=0; i<Fei4Cfg::numberOfRegisters; i++) {
    
    //Write the address
    m_ECRBitstreamMap[currentTx].Append32(writeRegisterHeader(i,fePtr->getChipID()));
    m_ECRBitstreamMap[currentTx].Append16(fePtr->correctedReg(i,fePtr->isPreAmpsOn()));
  }
}


void RecoAtECRFei4::addStrobePixelLatches(Boclib::Bitstream& bitstream, Fei4* fePtr) {
  
  //Strobe Pixel Latches
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::SRK,0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S0, 0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S1, 0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::HLD,0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::LEN,0x1));
  // Add pulse for this Fe
  uint32_t width = 10; //TODO: make it configurable
  bitstream.Append32(0x005A2400+(((fePtr->getChipID())<<6)&0x3C0)+(width&0x3F));
  //bitstream.Append32(globalPulse(fePtr->getChipID(),width); -- this should be used --
  
  //Add some time for pulse to propagate
  bitstream.Append16(0x0);
  //Return register 13 to 0 to prevent accidental latching from SEU
  //This could be achieved by just sending the GR value directly;
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::LEN,0x0));
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S0,0x0)); //redundant
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::S1,0x0)); //redundant
  addAddrValue(bitstream,fePtr->returnRegister(&Fei4::Pixel_latch_strobe,0x0));
  
}

#endif
