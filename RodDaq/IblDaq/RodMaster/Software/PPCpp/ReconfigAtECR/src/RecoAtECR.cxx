
#include "RecoAtECR.h"

#ifdef __XMK__
#include "RodResourceManager.h"
#endif
#include <chrono>

#include <unistd.h>


#ifdef __XMK__
std::mutex RecoAtECR::mtx_cfg;
RecoAtECR* RecoAtECR::instance = 0;
#endif

RecoAtECR::RecoAtECR() {
    init();
}

RecoAtECR::~RecoAtECR(){


}

void RecoAtECR::init(){
#ifdef __XMK__
  
  m_recoAtECRMode = RECO_OFF;
  
  m_recoConfig.init();
  
  m_loadBitStream=true;
  m_FifoLoaded=false;

  controlEcrCounter = -1;

  m_clear_bitstream.Append8(0x0);

  resetFIFOs(true);

#endif 
}

void RecoAtECR::run() {

  using std::chrono::milliseconds;

#ifdef __XMK__

  xil_printf("Starting RecoAtECR daemon...\n");

  //Wait before debug printing to avoid messed up UART log

  std::this_thread::sleep_for(milliseconds(200));

  std::unique_lock<std::mutex> lk(mtx_thread);

  while(1) {

      if (m_recoConfig.verbosityLevel > 0)
      xil_printf("Reco At ECR Mode: %d \n", m_recoAtECRMode);

      if (m_recoConfig.debug) std::this_thread::sleep_for( milliseconds(m_recoConfig.sleepDelay*15) );


      // condition functon for condition variable cv
      auto cnd_fce = [this]() {
          return (m_recoAtECRMode >=  RECO_ON) ? 1 : 0;
      };
      cv.wait(lk,cnd_fce);

      PrepareBitStreams(m_recoConfig.mask);
      LoadFIFOs();
      checkForECRArrived(true);
      resetFIFOs(true);

  } //end of loop

    // Should never be reached

  xil_printf("\n Reco At ECR loop closed. Exiting...\n");

#endif
}

#ifdef __XMK__

void RecoAtECR::PrintCfg() {
    m_recoConfig.PrintCfg();
}

void RecoAtECR::LoadFIFOs() {

  if (m_FifoLoaded)
    return;

  for (std::map<uint8_t, Boclib::Bitstream>::iterator it = m_ECRBitstreamMap.begin(); it != m_ECRBitstreamMap.end(); ++it) {

    //In order to interrupt in the case requested externally
    if (m_recoAtECRMode < RECO_ON_IDLE )
      break;

    bool correctTxWriting = true;
    int slv = (it->first) / 16;
    int  ch = (it->first) % 16;

    if (m_recoConfig.debug || m_recoConfig.verbosityLevel > 0)
      std::cout<<"Checking for ECR busy.."<<std::endl;

    waitForECRBusy(it->first);

    if (m_recoConfig.debug || m_recoConfig.verbosityLevel>0)
      std::cout<<"Writing the BitStream with size "<< (it->second).Size() <<std::endl;

    //Disable The ECR
    IblBoc::ECREnable(slv, ch, false, false);

    //Write the BitStream
    IblBoc::ECRWriteBitStream(slv, ch, (it->second).GetData());

    if (m_recoConfig.readBackCheck) {

      if (m_recoConfig.debug || m_recoConfig.verbosityLevel>0) 
	std::cout<<"Checking the stream back"<<std::endl;

      std::vector<uint8_t> read_back_bitstream;
      IblBoc::ECRReadBitStream(slv, ch, read_back_bitstream);

      if (m_recoConfig.verbosityLevel > 2) {
       std::cout<<"Dumping bit stream Tx "<<(int)it->first;
       if (RodResMgr::isFei3())std::cout<<" FE "<<(int)(controlEcrCounter % 16)<<std::endl;
       else std::cout<<" DC "<<(int)(controlEcrCounter % 40)<<std::endl;
	for (unsigned int j = 0; j < read_back_bitstream.size(); ++j ) {
	std::cout<<std::hex << std::setw(2) << std::setfill('0') << (int)read_back_bitstream.at(j) << std::dec;
    	if((j+1)%4==0 && j!=0)std::cout<<"\t";
        if((j+1)%32==0 && j!=0)std::cout<<std::endl;
	}
       std::cout<<std::endl;
      }
      if (((it->second).GetData() != read_back_bitstream)) {
	std::cout<<"WARNING::the bitstream written in the ECR Tx Fifo "<<(int)(it->first)<<" doesn't correspond to the one to be sent!"<<std::endl;
	correctTxWriting = false;
      }
      else {
	if (m_recoConfig.debug || m_recoConfig.verbosityLevel > 0)
	  std::cout<<"The bitstream has been correctly written in the FIFOs"<<std::endl;
      }
    }

    if (correctTxWriting) {
      if (m_recoConfig.debug || m_recoConfig.verbosityLevel > 0)
	std::cout<<"Enabling ECR for Tx "<<(int) it->first <<std::endl;
      IblBoc::ECREnable(slv, ch, true, false);
    }

    /*
      if (m_allowReconfig) {
      xil_printf("Forcing Veto removal \n");
      IblBoc::ECRSetVeto(slv, ch, false);
      }
      else {
      xil_printf("Not forcing veto removal \n");
      }
    */

  }//loop over the map

  //Only load FIFOs once
  m_loadBitStream = false;
  m_FifoLoaded    = true;
}


void RecoAtECR::resetFIFOs(bool allFIFOs) {

  if (allFIFOs) {
    for (uint8_t Tx = 0; Tx < 31; Tx++) {
      waitForECRBusy(Tx);
      IblBoc::ECREnable(Tx / 16, Tx % 16, false,false);
      IblBoc::ECRWriteBitStream(Tx / 16, Tx % 16,m_clear_bitstream.GetData()); 
    }
  } else {

    for (std::map<uint8_t, Boclib::Bitstream>::iterator it = m_ECRBitstreamMap.begin(); it != m_ECRBitstreamMap.end(); ++it) {
      int slv = (it->first) / 16;
      int  ch = (it->first) % 16;

      waitForECRBusy(it->first);
      //Disable the config at ECR
      IblBoc::ECREnable(slv, ch, false, false);
      //Clear the fifos
      IblBoc::ECRWriteBitStream(slv, ch, m_clear_bitstream.GetData());
    }
  }
  m_ECRBitstreamMap.clear();
  m_loadBitStream = true;
  m_FifoLoaded    = false;

}


bool RecoAtECR::checkForECRArrived(bool wait) {

  bool ERCArrived = false;
    if (wait)
      while (IblRod::Master::EcrCntInReg::read() == controlEcrCounter && m_recoAtECRMode > RECO_ON_IDLE)
          std::this_thread::sleep_for(std::chrono::milliseconds(1));

    ERCArrived = IblRod::Master::EcrCntInReg::read() != controlEcrCounter;

      if (ERCArrived && m_recoAtECRMode > RECO_ON_IDLE) {
	  currentEcrCounter = IblRod::Master::EcrCntInReg::read();
	  controlEcrCounter=currentEcrCounter; 
	  //The ECR arrived. Prepare to load the bit stream for next loop
	  //m_loadBitStream = true;
      }

  return ERCArrived;
}

void RecoAtECR::waitForECRBusy(const int txCh){
  const int slv = txCh / 16;
  const int  ch = txCh % 16;
  int timeout =0;
    while (IblBoc::ECRBusy(slv, ch) && ++timeout<10){//wait for the ECR busy to be finished on this Tx channel
      std::this_thread::sleep_for(std::chrono::milliseconds(1));//Max 10 ms
    }

}


#endif

