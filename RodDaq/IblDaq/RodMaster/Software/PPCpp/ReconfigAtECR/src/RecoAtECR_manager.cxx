#include "RecoAtECR_manager.h"
#include "RecoAtECRFei4.h"
#include "RecoAtECRFei3.h"


RecoAtECR_manager::RecoAtECR_manager(){
  m_recoConfig.init();
  mode  = RecoAtECR::RECO_OFF;
}

void RecoAtECR_manager::execute() {
  #ifdef __XMK__
  std::cout<<"mode \t"<<(int)mode<<std::endl;
  m_recoConfig.PrintCfg();

  xil_printf("RecoMan locking RecoAtECR thread\n");
  //OKUPDATE Bad style c++ locking
  std::lock_guard<std::mutex>(RecoAtECR::mtx_cfg);


    if(RodResMgr::isFei4())
      p_recoAtECR = RecoAtECRFei4::getInstance();
    else 
    p_recoAtECR = RecoAtECRFei3::getInstance();
  
  switch (mode) { 
    case RecoAtECR::RECO_OFF:
      std::cout<<"Turning off thread"<<std::endl;
      p_recoAtECR->resetFIFOs(true);
      p_recoAtECR->setRecoAtECRMode(RecoAtECR::RECO_OFF);
      break;
    case RecoAtECR::RECO_ON_IDLE:
      std::cout<<"Fill The FIFOs with Global Configuration Only"<<std::endl;
      p_recoAtECR->setRecoAtECRMode(RecoAtECR::RECO_ON_IDLE);

       m_recoConfig.glbRegCfg = true;
       m_recoConfig.pixRegCfg = false;
       m_recoConfig.customBS  = false;
       m_recoConfig.sleepDelay=100; 
       m_recoConfig.debug  = false;
       m_recoConfig.allowReconfig = false;
       m_recoConfig.pixmask = 0x0; 
       m_recoConfig.latchesMask = 0x0;
       m_recoConfig.latchesRotation = 0;
       m_recoConfig.readBackCheck = true;
       m_recoConfig.doHitBus    = true;

      p_recoAtECR->setRecoAtECRConfig(m_recoConfig);
      p_recoAtECR->PrintCfg();
      p_recoAtECR->SingleLoad(m_recoConfig.mask);
      break;
    case RecoAtECR::RECO_ON:
      std::cout<<"Turning on thread"<<std::endl;
      p_recoAtECR->setRecoAtECRMode(RecoAtECR::RECO_ON);
      break;
    case RecoAtECR::GLOB_RECO:
      p_recoAtECR->setGlbRegCfg(m_recoConfig.glbRegCfg);
      break;
    case RecoAtECR::PIX_RECO:
      p_recoAtECR->setPixRegCfg(m_recoConfig.pixRegCfg);
      break;
    case RecoAtECR::CUSTOM_BS:
      p_recoAtECR->setCustomBS(m_recoConfig.customBS);
      break;
    case RecoAtECR::ALLOW_RECO:
      p_recoAtECR->setAllowReconfig(m_recoConfig.allowReconfig);
      break;
    case RecoAtECR::FULL_CONFIG:
      std::cout<<"Configuring the thread"<<std::endl;
      std::cout<<" m_latchesRotation "<<(int)m_recoConfig.latchesRotation<<std::endl;
    
      p_recoAtECR->setRecoAtECRConfig(m_recoConfig);
    
      break;
    default:
      std::cout<<"Reconfiguration mode "<<(int) mode<<" not found"<<std::endl;
      break;
  }

   xil_printf("RecoMan unlocking RecoAtECR thread\n");

#endif

}

void RecoAtECR_manager::serialize(uint8_t* out) const 
{
  uint32_t offset = 0;
  Serializer<RecoAtECR::RecoAtECR_mode>::serialize(out,offset,mode);
  Serializer<RecoAtECR::RecoConfig>::serialize(out,offset,m_recoConfig);
}

void RecoAtECR_manager::deserialize(const uint8_t *in)
{
  uint32_t offset = 0;
  mode           = Serializer<RecoAtECR::RecoAtECR_mode>::deserialize(in,offset);
  m_recoConfig   = Serializer<RecoAtECR::RecoConfig>::deserialize(in,offset);
}

uint32_t RecoAtECR_manager::serializedSize() const 
{
  return (sizeof(RecoAtECR::RecoAtECR_mode)+ m_recoConfig.serializedSize());
}

