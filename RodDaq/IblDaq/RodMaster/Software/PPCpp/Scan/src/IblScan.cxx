///////////////////////////  define-setion for Nick's Dummy Scans  ///////////////////////////
//  When we firgure out (decide?) how, these should be moved out of compile-time paramters

// #define __HISTO_TEST__ //  Comment out for regular operation !!!!

#ifdef __HISTO_TEST__

#include "iblSlaveCmds.h"

#define SCANTYPE PIXELADDRESS             //  CHECKERBOARD, PIXELADDRESS or THR_SCURVE_FIT (DummyScanType enum 0, 1 or 2 - see iblSlaveCmds.h)
                                          //  Choose calibration console "DIGITAL_TEST" to launch the first two, "THRESHOLD_SCAN" for the thrid.

#define INJ_TYPE HISTO_TEST_SETUP         //  Choices are:
                                          //  HISTO_TEST_SETUP         = INTERNAL injection test using histogrammer (but NOT slave formatter)
                                          //  HISTO_TEST_SETUP_NOHISTO = INTERNAL injection test without touching histogrammer
                                          //  HISTO_TEST_SETUP_EXT     = EXTERNAL injection test involving BOTH histogrammer AND slave formatter

#define MULTIHIT 1                        //  1 = Add total number of injections at end of slave command to speed things up.

#endif // __HISTO_TEST__
//
//////////////////////////////////////////////////////////////////////////////////////////////

#include "IblScan.h"
#ifdef __XMK__
#include "ScanBoss.h"
#include "Fei4MonLeak.h"
#include "RodResourceManager.h"
#include "DumpRodRegisters.h"
#include "IblBoc.h"
#include "IblBocLink.h"
#include "IblConnectivity.h"
#include "DataTakingTools.h"
#include <algorithm>
#include "IblOptoScan.h"
#include <iomanip>
#include <arpa/inet.h>
#else
#include <unistd.h>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

Scan::Scan() { 
  m_useSlave0 = false;
  m_useSlave1 = false;
  // temporary or debug parameters
  debugWhichChips = false; // add +1 ToT to each chip for unique identification 
  debugWhichMaskStep = false; // add +1 ToT for each mask step
  lowDebug = false; // low level debug, print out register values + pause before each trigger
  aborting=false;
  m_dspScanType = SetDSPScan::NONE;

}

bool Scan::initScan() {

#ifdef __XMK__
  // get pixscanbase from scan boss
  m_scan = &(ScanBoss::getPixScanBase());

  status.state = SCAN_INIT;
  status.errorState = PPCERROR_NONE;
  status.mask = 0;
  status.par = 0;
  status.par1 = 0; //  Not used at all (yet?)
  status.par2 = 0; //  Not used at all (yet?)
  status.dc = 0;
  status.trig = 0;

#endif // __XMK__

#ifdef __HISTO_TEST__
  std::cout << std::endl << "***** IblScan running in dummy mode with the settings listed below *****" << std::endl
            << std::endl << "           SCANTYPE = " << SCANTYPE << std::endl
                         << "           INJ_TYPE = " << INJ_TYPE << std::endl
                         << "           MULTIHIT = " << MULTIHIT << std::endl << std::endl;
  m_channelsConsole = m_channels;
#endif // __HISTO_TEST__
  fillScanParameters();

#ifdef __XMK__
  //In order to restore the global registers after a scan it is cached in the
  //m_globalConfigStore. In order to save memory, only the Fei4GlobalCfg are stored
  //TBi TODO: move to std::array and new GlobalRegister enums in future version
  m_globalConfigStore.clear();
  uint32_t nMod = RodResourceManagerIBL::getNModuleConfigs();
  for (size_t modIx = 0; modIx < nMod; ++modIx){
      Fei4GlobalCfg & cfg = RodResourceManagerIBL::getCalibModuleConfig(modIx);
      m_globalConfigStore.push_back( cfg );
  }
#endif

#ifndef __HISTO_TEST__
  initializeFrontEnd();
#endif // no __HISTO_TEST__

  m_dspResults.clear();

  return initializeROD();

}

void Scan::runScan() {
#ifdef __XMK__
  // For now, loop order and number is hard-coded

    if (m_dspScanType != SetDSPScan::NONE) {
      if(m_dspScanType == SetDSPScan::BOC_THR_DEL_DSP)runOptoScan();
      if(m_dspScanType == SetDSPScan::LEAK_SCAN_DSP)runMonLeakScan();
      return;
    }

  status.state = SCAN_IN_PROGRESS;

  // Mask stage loop
  int32_t iMask;
  for (iMask = 0; iMask < m_maskStepsToDo; ++iMask) {
    status.mask = iMask;
    setupMaskIteration(iMask);

    // parameter loop
     bool slave_problem = false;
    for (int32_t iPar = 0; iPar < m_nParameterSteps; ++iPar) {

      status.par = iPar;
      setupParameterIteration(iPar);
      if(!startHistogrammer(iPar, iMask)) {slave_problem = true; break;};

#ifdef __HISTO_TEST__
// //  Get Configuration that must have been prevoiusly set
//       IblSlvHistCfg histConfig[2];
      IblRodSlave s;
//       for(uint32_t whichSlave=0; whichSlave<2; whichSlave++) histConfig[whichSlave] = s.getHistCfg(whichSlave);

// Checck all four units and only use if covered by the m_channels mask.
      IblSlvHistCfg* pHistConfig[2] = {&m_histCfgOut0, &m_histCfgOut1};
      int usms = 0; //  = unit scan mask shift
      for(uint32_t whichSlave=0; whichSlave<2; whichSlave++) {
        if(m_useSlave0 && whichSlave == 0 || m_useSlave1 && whichSlave == 1) {
          uint32_t useUnit[2] = {pHistConfig[whichSlave]->cfg[0].enable, pHistConfig[whichSlave]->cfg[1].enable};
          for(uint32_t histoUnit=0; histoUnit<2; histoUnit++) {
            if(useUnit[histoUnit]){
              pHistConfig[whichSlave]->cfg[histoUnit].enable = 1;
              pHistConfig[whichSlave]->cfg[1 - histoUnit].enable = 0;
              bool success = m_slave.startHisto(pHistConfig[whichSlave],whichSlave);
              if (!success) {
                status.errorState = SLAVE_NOT_RESPONDING;
                std::cout<<" Slave 0 did not respond to start histo command"<<std::endl;
              }
              if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer " << histoUnit << std::endl;
              uint32_t unitMask = (0x000000FF & (m_channelsConsole >> 8*usms));
              std::cout << " *** Calling fillDummyHisto for Slave " << whichSlave << ", Unit "
                        << histoUnit << ", unitMask = " << HEX(unitMask)
                        << ", iMask = " << iMask << ", iPar = " << iPar << " ***" << std::endl;
              s.fillDummyHisto(pHistConfig[whichSlave], iMask, iPar, unitMask, m_nParameterSteps, whichSlave, histoUnit, SCANTYPE, INJ_TYPE, MULTIHIT);
              pHistConfig[whichSlave]->cfg[histoUnit].enable = useUnit[histoUnit];
              pHistConfig[whichSlave]->cfg[1 - histoUnit].enable = useUnit[1 - histoUnit];
            }
            if(aborting)break;
            usms++;
          }
        } else usms +=2;
        if(aborting)break;
      }
#else

      // double column loop
      for (int8_t iDC = 0; iDC < m_nDCLoops; ++iDC) {
        status.dc = iDC;
	if (ScanBoss::verbose) std::cout<<std::dec<<"++++++ Starting DC loop: "<<
	  (int)iDC+1<<" out of "<<(int)m_nDCLoops<<" total loops"<<", mask loop = "<<(int)iMask+1<<", and par loop = "<<(int)iPar+1<<std::endl;
	
	setupDoubleColumnIteration(iDC);	
	if (lowDebug) registerDump();
	
	//  std::cout<<" about to start triggering "<<std::endl;
	if (ScanBoss::verbose) std::cout << std::dec << "++++++++ Starting to send " << (int)m_nTriggers << " triggers" << std::endl;
	for (int32_t iTrig = 0; iTrig < m_nTriggers; ++iTrig) {
          status.trig = iTrig;
	  if (m_noiseScan) triggerOnly();
	  else { 
	    pulseFrontEnd();
	    checkReadyForNextTrigger();
	  }
	  debugReadBack(iMask);
	} // trigger loop
	if (ScanBoss::verbose) std::cout<<std::dec<<"++++++++ Done sending triggers "<<std::endl;

	//  std::cout<<" done with trigger loop"<<std::endl;
        if(aborting)break;
      } // double column loop
      getParameterFeedback(iPar);
      usleep(2000); // tmp lj to-do: is this necessary?
      if(!stopHistogrammer(iMask)) {slave_problem = true; break;};

#endif // no __HISTO_TEST__

      if(aborting)break;
    } // parameter loop 
    if (aborting || slave_problem)break;
  } // mask stage loop

  if(iMask == m_maskStepsToDo)status.state = SCAN_COMPLETE; //  Normal termination.  Otherwise aborting or slave_problem.
#endif
}

void Scan::endScan() {
#ifndef __HISTO_TEST__
  processResult();
#endif
  if (aborting  && m_dspScanType != SetDSPScan::NONE) cleanDSPResults();
  resetSystem();
  std::cout << " ** Scan cleanup complete." << std::endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Scan Internal Functions
//////////////////////////////////////////////////////////////////////////////////////////////
void Scan::runOptoScan() {
#ifdef __XMK__
  status.state = SCAN_IN_PROGRESS;

  m_iblBocLink->enableChannels(m_channels);
  
  IblOptoScan::getOptoErrors(m_channels,m_nTriggers,IblConnectivity::getTxMaskFromRxMask(m_channels),m_dspResults);

  m_iblBocLink->disableRxChannels(m_channels);//reset the system.

	if(aborting) {
	  std::cout << "Optoscan aborted" << std::endl;
	} else {
	  status.state = SCAN_COMPLETE;
	  std::cout << "Optoscan done" << std::endl;
	}

#endif // _XMK_

}

void Scan::runMonLeakScan() {
#ifdef __XMK__
status.state = SCAN_IN_PROGRESS;


  uint16_t maxPixPerFE = 26880;
  //No mask step iterationin monLeak scan, we just get the number of Pixels to scan from the maskStage mode
  uint16_t nPixPerFE =  maxPixPerFE*m_maskStepsToDo/m_maskSteps;

   if(nPixPerFE > maxPixPerFE  ){
   nPixPerFE = maxPixPerFE;
   std::cout<<"You requested "<<(int)nPixPerFE<<" Pixel per FE but maximum number of Pixels per FE is "<<(int)maxPixPerFE<<" setting to this value "<<std::endl;
   }

   std::cout<<"Number of Pixels per FE "<<(int)nPixPerFE<<std::endl;

  uint32_t newRxMask = Fei4MonLeak::initMonLeak(m_channels);

 std::map< uint8_t, std::vector <uint32_t > > MonLeak;
 Fei4MonLeak::getMonLeakADC(newRxMask, 0, 1, MonLeak);//Skip first wrong reading

  for(uint16_t index=0;index<nPixPerFE; index++){
    MonLeak.clear();
    if (aborting)break;
    Fei4MonLeak::getMonLeakADC(newRxMask, index,1,MonLeak);

    for (std::map<uint8_t, std::vector <uint32_t > >::iterator it = MonLeak.begin(); it!=MonLeak.end(); ++it)m_dspResults[it->first].push_back(it->second);
  }

	if(aborting) {
	  std::cout << "MonLeak scan aborted" << std::endl;
	} else {
	  status.state = SCAN_COMPLETE;
	  std::cout << "MonLeak scan done" << std::endl;
	}

#endif // _XMK_

}


////////////////////////////////////////////
// Initialization
////////////////////////////////////////////

void Scan::fillScanParameters() {
#ifdef __XMK__
  // fill internal variables from PixScanBase

  // mask stage and double column loop variables
  m_loopDC = setupMaskSteps(m_scan->getMaskStageTotalSteps(),m_maskSteps,m_modeDC);
  m_maskStepsToDo = m_scan->getMaskStageSteps();
  m_maskStageMode = m_scan->getMaskStageMode();
  m_pixelLatchStrobeValue = decodeMaskStageMode(m_maskStageMode);
  
  m_nDCLoops = 1;
  if (m_loopDC && (m_modeDC == 0)) m_nDCLoops = 40;
  else if (m_loopDC && m_modeDC == 1) m_nDCLoops = 4;
  else if (m_loopDC && m_modeDC == 2) m_nDCLoops = 8;  

  // trigger variables
  m_nTriggers = m_scan->getRepetitions();
  m_frameCount = m_scan->getConsecutiveLvl1Trig();
  m_trigDelay = m_scan->getStrobeLVL1Delay();
  m_trigDelayOverride = m_scan->getStrobeLVL1DelayOverride();
  m_trigLatency = m_scan->getLVL1Latency();
  m_noiseScan = m_scan->getSelfTrigger(); // this is cheating; to-do, add a new variable?

  // calibration pulse variables
  m_strobeDuration = m_scan->getStrobeDuration();
  m_strobeDelay = m_scan->getStrobeMCCDelay(); 
  m_strobeDelayRange = m_scan->getStrobeMCCDelayRange();
  m_feVCal = m_scan->getFeVCal();
  m_hitDisConfig= m_scan->getTotMin();//Reusing ToT min variable
  if(m_hitDisConfig >2){
  std::cout<<"ERROR HitDisConfig is only in the range [0-2] while I got "<<(int)m_hitDisConfig<<" Falling back to zero"<<std::endl;
  m_hitDisConfig =0;
  }

  // type of scan
  m_doDigitalInjection = m_scan->getDigitalInjection();
  m_setToTHistoMode = false; // default is occupancy mode
  // set histogrammer to LONG_TOT if any TOT histogram is desired
  if (m_scan->getHistogramFilled(PixLib::EnumHistogramType::TOT_MEAN) || m_scan->getHistogramFilled(PixLib::EnumHistogramType::TOT_SIGMA)) 
	  m_setToTHistoMode = true;
  
  // To-do: update for variable parameter step size scanning
  // To-do: allow for more than 1 parameter scan, or to change parameter loop position
  // Only do a parameter loop on PPC if dspProcessing flag is set to true
  m_doParamLoop = (m_scan->getLoopActive(0) && m_scan->getDspProcessing(0));
  m_doParam1Loop = (m_scan->getLoopActive(1) && m_scan->getDspProcessing(1));
  m_doParam2Loop = (m_scan->getLoopActive(2) && m_scan->getDspProcessing(2));
  m_nParameterSteps = 1; 
  m_parameterMin = 0;
  m_parameterMax = 1;
  m_nParameter1Steps = 1;
  m_parameter1Min = 0;
  m_parameter1Max = 1; 
  m_nParameter2Steps = 1;
  m_parameter2Min = 0;
  m_parameter2Max = 1;
  if (m_doParamLoop) {
    m_nParameterSteps = m_scan->getLoopVarNSteps(0);
    m_parameterMin = m_scan->getLoopVarMin(0);
    m_parameterMax = m_scan->getLoopVarMax(0);
  }
  if (m_doParam1Loop) {
    m_nParameter1Steps = m_scan->getLoopVarNSteps(1);
    m_parameter1Min = m_scan->getLoopVarMin(1);
    m_parameter1Max = m_scan->getLoopVarMax(1);
  }
  if (m_doParam2Loop) {
    m_nParameter2Steps = m_scan->getLoopVarNSteps(2);
    m_parameter2Min = m_scan->getLoopVarMin(2);
    m_parameter2Max = m_scan->getLoopVarMax(2);
  }
  m_scanParam = m_scan->getLoopParam(0); 
  m_scanParam1 = m_scan->getLoopParam(1);
  m_scanParam2 = m_scan->getLoopParam(2);

  // system parameters
  m_restoreModConfig = m_scan->getRestoreModuleConfig();

  // sleep longer if not doing a DC because you'll get more data per frame
  if (m_noiseScan) m_sleepDelay = m_scan->getSuperGroupTrigDelay(); // this is cheating; to-do, add a new variable?
  // safe for b-field 
  m_sleepDelay = 300; // us

  printOutScanParams();

#endif
}

#ifdef __XMK__
bool Scan::setupMaskSteps(PixLib::EnumMaskSteps::MaskStageSteps requestedMaskSteps, uint32_t &maskSteps, uint8_t &modeDC) { 
  bool loopDC = false;
  modeDC = 0; // default DC mode is 0 (specific DC)

  // for now, DC looping is ENFORCED regardless of what the user selects
  // this is an extra protection until we determine what the max occupancy
  // per front end is (w.r.t. wire bond oscilliations)
  // safe for b-field 
  switch(requestedMaskSteps) {
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_1_DC:
      maskSteps = 1;
      loopDC = true;
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_2_DC:
      maskSteps = 2;
      loopDC = true;
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_4_DC:
      loopDC = true;
      maskSteps = 4;
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_8_DC:
      loopDC = true;
      maskSteps = 8;
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_8_4DC:
      loopDC = true;
      maskSteps = 8;
      modeDC = 1; // every 4 DC
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_8_8DC:
      loopDC = true;
      maskSteps = 8;
      modeDC = 2; // every 8 DC
      break;
    default :
      std::cout <<"AN UNSUPPORTED MASK STEP PROCEDURE HAS BEEN CHOSEN BY PIXSCANBASE"<<std::endl;
      std::cout <<"Check the MaskStageTotalSteps variable in PixScan"<<std::endl;
      std::cout <<"It was set to: "<<requestedMaskSteps<<", which is not supported"<<std::endl;
      std::cout <<"Will set the mask steps to STEPS_8_DC "<<std::endl;
      loopDC = true;
      maskSteps = 8;
  }
  return loopDC;
}
#endif

bool Scan::initializeROD() {
  bool success = true;
#ifdef __XMK__
  std::cout << __PRETTY_FUNCTION__ << std::endl;
  std::cout<<std::endl; std::cout<<"** Initializing the ROD **"<<std::endl; std::cout<<std::endl;
  // to-do: move where approprite!!!
#define RRIF_CTL_PPC_TRIG 0x80041425
#define RRIF_CTL_INITIAL  0x80000000

  // initialization of master ctrl reg
  IblRod::Master::RrifCtrlReg::write(RRIF_CTL_INITIAL);

  if (m_dspScanType != SetDSPScan::NONE) return success;
  // slave Ids
  IblRod::SlaveA::SlaveId::write(0);
  IblRod::SlaveB::SlaveId::write(1);
  m_useSlave0 = false;
  m_useSlave1 = false;

  // configure slaves and histogramming if using
  if ((m_channelsConsole & 0x0000FFFF) > 0) {
    m_useSlave0 = true;
    bool successS0 =  m_slave.setId(0);
    if (!successS0) status.errorState = SLAVE_NOT_RESPONDING;
    else successS0 = configureHistogrammer(0, m_histCfgOut0, m_netCfgOut0);
    success &= successS0;
  }
  if ((m_channelsConsole & 0xFFFF0000) > 0) {
    m_useSlave1 = true;
    bool successS1 =  m_slave.setId(1);
    if (!successS1) status.errorState = SLAVE_NOT_RESPONDING;
    else successS1 = configureHistogrammer(1, m_histCfgOut1, m_netCfgOut1);
    success &= successS1;
  }

#ifndef __HISTO_TEST_
  // enable link between BOC and ROD
  DataTakingTools::enableFmtLink(m_channels);

  // enable calibration mode
  IblRod::Master::RrifCtrlReg::write( RRIF_CTL_PPC_TRIG);
  IblRod::Master::SprCtrlReg::write(0x0FF);
  IblRod::Master::DebugModeCnfgReg::write( 0x02000000 );

  // set number of frames per trigger on ROD
  IblRod::SlaveA::TriggerCountNumber::write(m_frameCount);
  IblRod::SlaveB::TriggerCountNumber::write(m_frameCount);

  // set time out for channels with no data
  IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0xFFF);
  IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0xFFF);

  // set limit of number of header + hit + trailer per module per trigger (how many data-records to process per frame) to 1680 (to be tuneed)          
  IblRod::SlaveA::FmtHeaderTrailerLimit::write(0x7FF);
  IblRod::SlaveB::FmtHeaderTrailerLimit::write(0x7FF);

  //Overflow limit
  IblRod::SlaveA::FmtDataOverflowLimit::write(0x7FF);
  IblRod::SlaveB::FmtDataOverflowLimit::write(0x7FF);

  IblRod::SlaveA::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);
  IblRod::SlaveB::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);

  //put slaves into calibration mode (don't send over the slink)
  IblRod::SlaveA::CalibrationMode::write(0x1);
  IblRod::SlaveB::CalibrationMode::write(0x1);

  if (ScanBoss::readBackInmem) {
    fifo.selectGathererFromRxCh(ScanBoss::rxCh); 
    fifo.setRxCh(ScanBoss::rxCh); 
    fifo.reset();
  }

#endif // no __HISTO_TEST__

#endif
  return success;
}

#ifdef __XMK__
bool Scan::configureHistogrammer(uint8_t slaveID, IblSlvHistCfg & histCfgOut, IblSlvNetCfg & netCfgOut) {
  std::cout<<"** Configuring the histogrammers and connecting to the fit server** "<<std::endl; std::cout<<std::endl;

  IblRodSlave::getDefaultNetConfig(slaveID, netCfgOut);

  uint32_t tPA = slaveID ? m_fitPorts[2] : m_fitPorts[0];
  uint32_t tPB = slaveID ? m_fitPorts[3] : m_fitPorts[1];

  std::cout<<std::dec<<"Fit farm target port A, B = "<<(int)tPA<<", "<<(int)tPB<<std::endl; 

  netCfgOut.targetPortA = htonl(tPA);
  netCfgOut.targetPortB = htonl(tPB);

  char buffer[100];

  // fit server target ip from scan config
  if (slaveID) {
    IblRodSlave::createIp4Address(m_fitIps[2][0], m_fitIps[2][1], m_fitIps[2][2], m_fitIps[2][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(m_fitIps[3][0], m_fitIps[3][1], m_fitIps[3][2], m_fitIps[3][3], &netCfgOut.targetIpB);
    snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", m_fitIps[2][0], m_fitIps[2][1], m_fitIps[2][2], m_fitIps[2][3]); std::cout << buffer << std::endl;
    snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", m_fitIps[3][0], m_fitIps[3][1], m_fitIps[3][2], m_fitIps[3][3]); std::cout << buffer << std::endl;
  } else {
    IblRodSlave::createIp4Address(m_fitIps[0][0], m_fitIps[0][1], m_fitIps[0][2], m_fitIps[0][3], &netCfgOut.targetIpA);
    IblRodSlave::createIp4Address(m_fitIps[1][0], m_fitIps[1][1], m_fitIps[1][2], m_fitIps[1][3], &netCfgOut.targetIpB);
     snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", m_fitIps[0][0], m_fitIps[0][1], m_fitIps[0][2], m_fitIps[0][3]); std::cout << buffer << std::endl;
     snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", m_fitIps[1][0], m_fitIps[1][1], m_fitIps[1][2], m_fitIps[1][3]); std::cout << buffer << std::endl;
  }


  bool success = true;
  success = m_slave.setNetCfg(&netCfgOut,slaveID);
  if (success) std::cout<<"** Connected to the fit server ** "<<std::endl;

  if (!success) {
    status.errorState = NO_CONN_TO_FITSERVER;
    std::cout<<"***ERROR***"<<std::endl;
    std::cout<<"ERROR: NO CONNECTION TO FIT SERVER; CONTINUTING ANYWAY"<<std::endl;
  }

  for (int i = 0; i<2; i++) {
    histCfgOut.cfg[i].nChips = 8;
    histCfgOut.cfg[i].enable = 0;    
     m_setToTHistoMode ? histCfgOut.cfg[i].type = LONG_TOT : histCfgOut.cfg[i].type = ONLINE_OCCUPANCY;
  if (m_maskSteps ==1) histCfgOut.cfg[i].maskStep = 0;  
  else if (m_maskSteps ==2) histCfgOut.cfg[i].maskStep = 1;
  else if (m_maskSteps ==4) histCfgOut.cfg[i].maskStep = 2;
  else if (m_maskSteps ==8) histCfgOut.cfg[i].maskStep = 3;
  else histCfgOut.cfg[i].maskStep = 0; // for now, all mask steps ther than 2,4 and 8 use the full histogrammer space

  // always doing multi-chip case now so that Rx channels don't get messed up in the FitServer
  histCfgOut.cfg[i].chipSel = 1;
  histCfgOut.cfg[i].addrRange = 1; // tmp lj: or 0?  
  histCfgOut.cfg[i].scanId = m_scanId;
  histCfgOut.cfg[i].binId = 0; 
  histCfgOut.cfg[i].expectedOccValue = m_nTriggers;
  histCfgOut.cfg[i].mStepEven = 0; // default disable; will set in start histo function 
  histCfgOut.cfg[i].mStepOdd = 0; // default disable; will set in start histo function 
  }
  if (slaveID == 0) {
    if ((m_channelsConsole & 0x000000FF) > 0) histCfgOut.cfg[0].enable = 1;
    if ((m_channelsConsole & 0x0000FF00) > 0) histCfgOut.cfg[1].enable = 1;
  }
  else if (slaveID == 1) {
    if ((m_channelsConsole & 0x00FF0000) > 0) histCfgOut.cfg[0].enable = 1;
    if ((m_channelsConsole & 0xFF000000) > 0) histCfgOut.cfg[1].enable = 1;
  }
  return success;
}
#endif

void Scan::initializeFrontEnd() {
#ifdef __XMK__
  std::cout<<"** Initializing the front ends **"<<std::endl; std::cout<<std::endl;

  // disable Rx before enabling
  m_iblBocLink = RodResourceManagerIBL::getIblBocLink(); 
  m_iblBocLink->disableRxChannels(0xFFFFFFFF); 

  m_channelsConsole = m_channels;

  std::cout<<"Enabling Tx Monitoringcounters "<<std::endl;
  uint32_t maskTx = IblConnectivity::getTxMaskFromRxMask(m_channels);
  for(uint8_t txCh=0;txCh<32;txCh++)
    if (maskTx & (1<<txCh)){
    IblBoc::DisableTxMonitoring(txCh/16, txCh%16);
    IblBoc::EnableTxMonitoring(txCh/16, txCh%16);
  }
  
  //m_iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(m_channels));
  SerialPort* sPort = RodResourceManagerIBL::getSerialPort();
  sPort->setTxMask(IblConnectivity::getTxMaskFromRxMask(m_channels));
  m_fei4Sport.setTxLink(sPort);

  if (m_dspScanType != SetDSPScan::NONE)return;

  // Setup fei4Proxy with the channel map
  // fei4Proxy will forward all commands to all channels (in the map) simultaneously
  // for register reads and writes, it will loop through each fei4 seperately
  // from the set of fei4s held by RodResourceManager
  //Proxy set to 0xFFFFFFFF in emu (configuration not sent since)

  if(m_scan->getUseEmulator()){
    DataTakingTools::setBocEmu(true, m_channels, m_frameCount, IblBoc::Speed_40, m_scan->getNHitsEmu());
    m_fei4 = Fei4Proxy(0xFFFFFFFF);
    m_iblBocLink->enableTxChannels(IblConnectivity::getTxMaskFromRxMask(m_channels));
  } else {
    DataTakingTools::setBocEmu(false, 0xFFFFFFFF);
    m_fei4 = Fei4Proxy(m_channels);
  }

  // Send config to the modules  
  // pre-amps OFF for digital scan
  // pre-amps ON for all other scans

  if( !m_iblBocLink->enableChannels(m_channels) ) {
    std::cout<<"WARNING: could not enable all the desired channels. Will continue scan anyway"<<std::endl;
    m_channels = m_iblBocLink->getEnChMask();
    std::cout<<"New (enabled) channel mask = 0x"<<std::hex<<m_channels<<std::dec<<std::endl;
    if (m_channels == 0x0) status.errorState = NO_ENABLED_CHANNELS;
    m_fei4.setFeMask(m_channels); // update channel mask if not all enabled
  }

  if (m_doDigitalInjection) m_fei4.sendGlobalConfigAmpsOff();
  else m_fei4.sendGlobalConfigAmpsOn();

  m_fei4.setRunMode(false);
  m_fei4.setVerbose(false); 
  Fei4Data::setRawDataDump(false); 
  Fei4Data::setVerboseSR(false); // read back Service records 

    if (!m_doDigitalInjection) { 
      std::cout<<"about to send pixel cfgs"<<std::endl;
      m_fei4.sendPixelConfig(true); // send only fdac and tdac pixel regs individually
      std::cout<<"sent pixel cfgs"<<std::endl;
  }

  // clear pixel capicator regs.... just in case! // this seems to be necessary.... why?
    m_mask.set(MASK_CLEAR);
    // safe for b-field 
    // only write front end / strobe pixel latches one DC at a time
    m_fei4.writeRegister(&Fei4::CP,0x0); // set DC mode to one DC
       for (int iDC = 0; iDC < 40; iDC++) // hard-coded! 40!
         m_fei4.strobePixelLatches(m_mask, 0xc1, iDC); // write capicitor pixel latches
    
  // write specific registers that are set in scan parameters and should be set before starting scan
  //To-do: ensure protection for variable ranges (trig delay, trig count, trig lat...)
  
  m_fei4.writeRegister(&Fei4::Trig_Count, m_frameCount); 
  m_fei4.writeRegister(&Fei4::Trig_Lat, m_trigLatency);
  m_doDigitalInjection ? m_fei4.writeRegister(&Fei4::DHS, 1) : m_fei4.writeRegister(&Fei4::DHS, 0);

  if (m_scan->getOverrideStrobeMCCDelay()) m_fei4.writeRegister(&Fei4::PlsrDelay, m_strobeDelay);
  if (m_scan->getOverrideStrobeMCCDelayRange()) m_fei4.writeRegister(&Fei4::PlsrIDACRamp, m_strobeDelayRange);
  
  
  // sets width of calibration pulse
  if (m_strobeDuration > 0) m_fei4.writeRegister(&Fei4::CalPulseWidth, m_strobeDuration); 
  
// send different cal pulse widths (ToT) to debug which chip is which
  if (debugWhichChips) {
    int CalPulse = m_strobeDuration - 1;
    for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
      if (m_channels & (1<<i)) {
	if (CalPulse > 0xF) CalPulse = 0xF; // protection against too large ToT
	Fei4Proxy::writeRegister(&Fei4::CalPulseWidth, (CalPulse), (m_channels & (1<<i))); 
	std::cout<<"Debug ToT: about to set true ToT of "<<(int)CalPulse<<" in rx ch mask: 0x"<<std::hex<<(int)(m_channels & (1<<i))<<std::dec<<std::endl;
	CalPulse++;; // FE increase in ToT
      }
    }
  }

  // Only write the PlsrDAC register if a value within range is set
  if (m_feVCal > 0 && m_feVCal < 1024)
    m_fei4.writeRegister(&Fei4::PlsrDAC,m_feVCal);

   DataTakingTools::setHitDiscCnfg(m_channels,m_hitDisConfig);
   DataTakingTools::trickHitDiscConfigCalib();

  return;

#endif 
}

#ifdef __XMK__
uint32_t Scan::decodeMaskStageMode(PixLib::EnumMaskStageMode::MaskStageMode mode) {
  uint32_t regBits = 0x0;
  
  switch(mode) {
  case SCAN_FEI4_ENA_NOCAP :
    regBits = regBits | 0x1;
    break;
  case SCAN_FEI4_ENA_SCAP :
    regBits = regBits | 0x81;
    break;
  case SCAN_FEI4_ENA_LCAP :
    regBits = regBits | 0x41;
    break;
  case SCAN_FEI4_ENA_BCAP :
    regBits = regBits | 0xc1;
    break;
  case SCAN_FEI4_ENA_HITBUS :
    regBits = regBits | 0x101;
    break;
  case SCAN_FEI4_XTALK:
    regBits = regBits | 0xc0;//Both capacitors for the XTALK scan
    break;
  case SCAN_FEI4_MONLEAK :
    regBits = regBits | 0x100; // or 0x101? Doesn't explicitly include enable...
    break;
  default:  
    std::cout <<"AN UNSUPPORTED MASK STEP MODE HAS BEEN CHOSEN BY PIXSCANBASE"<<std::endl;
    std::cout <<"Check the MaskStageMode variable in PixScan"<<std::endl;
    std::cout <<"It was set to: "<<(int)mode<<", which is not supported"<<std::endl;
    std::cout <<"Will set the mask stage modes to SCAN_FEI4_ENA_NOCAP"<<std::endl;
    std::cout <<"So you are basically getting a digital scan; no analog injection will work"<<std::endl;
    regBits = regBits | 0x1;
  }
  return regBits;
}
#endif

void Scan::printOutScanParams() {
#ifdef __XMK__
  std::cout<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<" Initialized scan with the following parameters: "<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<std::endl;

  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<" Channels: 0x"<<std::hex<<(int)m_channels<<std::dec<<std::endl;
  std::cout<<" Scan id: "<<std::dec<<(int)m_scanId<<std::endl;
  std::cout<<" Fit server port 0: "<<std::dec<<(int)m_fitPorts[0]<<std::endl;
  std::cout << " DSP SCAN TYPE: " << std::dec << (int)m_dspScanType << std::endl;
  std::cout<<std::endl;

  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<" mask step options: "<<std::endl;
  std::cout<<std::dec<<std::endl;
  std::cout<<" loop over DC (true or false) = "<<(int)m_loopDC<<std::endl;
  std::cout<<" number of DC loops: "<<(int)m_nDCLoops<<std::endl;
  std::cout<<" dc mode = "<<(int)m_modeDC<<std::endl;
  std::cout<<" total mask steps: "<<(int)m_maskSteps<<std::endl;
  std::cout<<" mask steps to do out of total: "<<(int)m_maskStepsToDo<<std::endl;
  std::cout<<" maskStageMode: "<<(int)m_maskStageMode<<std::endl;
  std::cout<<" value to write to pixel latch strobe general reg: "<<(int)m_pixelLatchStrobeValue<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;

  std::cout<<" trigger options: "<<std::endl;
  std::cout<<std::dec<<std::endl;
  std::cout<<" # ntriggers: "<<(int)m_nTriggers<<std::endl;
  std::cout<<" # frames per trigger: "<<(int)m_frameCount<<std::endl;
  std::cout<<" trigger delay between pulse and trigger: "<<(int)m_trigDelay<<std::endl;
  std::cout<<" override trigger delay from config? "<<(int)m_trigDelayOverride<<std::endl;
  std::cout<<" trigger latency: "<<(int)m_trigLatency<<std::endl;
  std::cout<<" noise scan? = no pulse, trigger only: "<<(bool)m_noiseScan<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;

  std::cout<<" pulser options: "<<std::endl;
  std::cout<<std::dec<<std::endl;
  std::cout<<" strobe width: "<<(int)m_strobeDuration<<std::endl;
  std::cout<<" strobe delay: "<<(int)m_strobeDelay<<std::endl;
  std::cout<<" strobe delay override : "<<m_scan->getOverrideStrobeMCCDelay()<<std::endl;
  std::cout << " strobe delay range : " << (int)m_strobeDelayRange << std::endl;
  std::cout << " strobe delay range override: " << m_scan->getOverrideStrobeMCCDelayRange() << std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;

  std::cout<<" scan parameter 0 options: "<<std::endl;
  std::cout<<std::endl;
  std::cout<<" do a parameter loop scan? : "<<(int)m_doParamLoop<<std::endl;
  std::cout<<" # parameter steps: "<<(int)m_nParameterSteps<<std::endl;
  std::cout<<" parameter min: "<<(int)m_parameterMin<<std::endl;
  std::cout<<" parameter max: "<<(int)m_parameterMax<<std::endl;
  std::cout<<" parameter over which to scan (enum): "<<(int)m_scanParam<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
    
  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << "+  !! The following not used on ROD at all now !!  +" << std::endl;
  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << " scan parameter 1 options: " << std::endl;
  std::cout << std::endl;
  std::cout << " do parameter loop 1 scan? : " << (int)m_doParam1Loop << std::endl;
  std::cout << " # parameter steps: " << (int)m_nParameter1Steps << std::endl;
  std::cout << " parameter min: " << (int)m_parameter1Min << std::endl;
  std::cout << " parameter max: " << (int)m_parameter1Max << std::endl;
  std::cout << " parameter over which to scan (enum): " << (int)m_scanParam1 << std::endl;
  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << "+  !! The following not used on ROD at all now !!  +" << std::endl;
  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
  std::cout << " scan parameter 2 options: " << std::endl;
  std::cout << std::endl;
  std::cout << " do parameter loop 2 scan? : " << (int)m_doParam2Loop << std::endl;
  std::cout << " # parameter steps: " << (int)m_nParameter2Steps << std::endl;
  std::cout << " parameter min: " << (int)m_parameter2Min << std::endl;
  std::cout << " parameter max: " << (int)m_parameter2Max << std::endl;
  std::cout << " parameter over which to scan (enum): " << (int)m_scanParam2 << std::endl;
  std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

  std::cout<<" general scan params: "<<std::endl;
  std::cout<<std::endl;
  std::cout<<" HitDisConfig "<<(int)m_hitDisConfig<<std::endl;
  std::cout<<" do digital injection? "<<(int)m_doDigitalInjection<<std::endl;
  std::cout<<" restore module config to original ? "<<(int)m_restoreModConfig<<std::endl;
  std::cout<<" use BOC emulator ? "<<m_scan->getUseEmulator()<<std::endl;
  std::cout<<" height of analog injection: "<<(int)m_feVCal<<std::endl;
  std::cout<<" set histogrammer to LONG_TOT mode? "<<(int)m_setToTHistoMode<<std::endl;
  std::cout<<" sleep after trigger = "<<(int)m_sleepDelay<<" (us)"<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
    
  std::cout<<" debug params: "<<std::endl;
  std::cout<<" verbose? : "<<(int)ScanBoss::verbose<<std::endl;
  std::cout<<" debug read back from boc fifo onto kermit? : "<<(int)ScanBoss::readBackBoc<<std::endl;

  std::cout<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<"++++++++++++++++++++++++++++++++++++++++++++++++++++"<<std::endl;
  std::cout<<std::endl;
#endif
}

////////////////////////////////////////////
// Running
////////////////////////////////////////////

void Scan::setupMaskIteration(int32_t iMask, bool clear) {
  if (!clear) std::cout<<"++ Starting mask stage loop: "<<
    (int)iMask+1<<" out of "<<(int)m_maskStepsToDo<<" total loops"<<std::endl;

#ifdef __XMK__
  if (ScanBoss::verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  //Define or shift pixel mask
  if (iMask == 0) {
    if (m_maskSteps ==1) m_mask.set(MASK_FULL);
    else if (m_maskSteps == 2) m_mask.set(MASK_HALF);
    else if (m_maskSteps == 4) m_mask.set(MASK_QUARTER);
    else if (m_maskSteps == 8) m_mask.set(MASK_EIGHTH);
    else if (m_maskSteps == 16) m_mask.set(MASK_16);
    else if (m_maskSteps == 32) m_mask.set(MASK_32);
    else if (m_maskSteps == 672) m_mask.set(MASK_672);

    m_disableMask = m_mask;

      if(m_maskStageMode == PixLib::EnumMaskStageMode::FEI4_XTALK) {
        if (m_maskSteps ==1) m_mask.set(MASK_FULL);
        else if (m_maskSteps == 2) m_mask.setXTALK(MASK_HALF);
        else if (m_maskSteps == 4) m_mask.setXTALK(MASK_QUARTER);
        else if (m_maskSteps == 8) m_mask.setXTALK(MASK_EIGHTH);
        else if (m_maskSteps == 16) m_mask.setXTALK(MASK_16);
        else if (m_maskSteps == 32) m_mask.setXTALK(MASK_32);
        else if (m_maskSteps == 672) m_mask.setXTALK(MASK_672);
        std::cout<<"Setting mask step for XTALK scan"<<std::endl;
      }
  }
  else {
    m_mask.shift();
    if(m_maskStageMode == PixLib::EnumMaskStageMode::FEI4_XTALK)m_disableMask.shift();
  }

  // if done with scan, clear the loaded pixel latches
  if (clear){
  m_mask.set(MASK_CLEAR);
  m_disableMask.set(MASK_CLEAR);
  }

  if(m_scan->getUseEmulator())return;
  
  m_fei4.setRunMode(false);

  // safe for b-field 
  // only write front end / strobe pixel latches one DC at a time
  m_fei4.writeRegister(&Fei4::CP,0x0); // set DC mode to one DC
  for (int iDC =0; iDC<40; iDC++){ // hard-coded! nDC = 40! 
    m_fei4.strobePixelLatches(m_mask, m_pixelLatchStrobeValue, iDC); // write pixel latches
    if(m_maskStageMode == PixLib::EnumMaskStageMode::FEI4_XTALK)m_fei4.strobePixelLatches(m_disableMask, 0x1, iDC);//Disable Pixels not belonging to the readout mask, note that for standard scans both enable and injected mask matches
    }

  m_fei4.writeRegister(&Fei4::Colpr_Addr, 40); // write non-valid DC to all FE

  for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
    if (m_channels & (1<<i)) {
      Fei4 &f = RodResourceManagerIBL::getCalibModuleConfig(i);
      if (f.scanDisable.size() != 0) {
      Mask disable;
	if(m_maskStageMode != PixLib::EnumMaskStageMode::XTALK) disable = m_mask;
	else disable = m_disableMask;

	for (size_t iDisable = 0; iDisable < f.scanDisable.size(); iDisable++) { // don't enable pixels which we explicitly disable
	  //	  std::cout<<"for fe ch: "<<(int)i<<" have DC: "<<(int)f.scanDisable[iDisable].first<<" and pixel: "<< (int)f.scanDisable[iDisable].second<<" to disable"<<std::endl;
	  disable.setPixel(f.scanDisable[iDisable].second, 0);
	  f.strobePixelLatches(disable, m_pixelLatchStrobeValue, f.scanDisable[iDisable].first);
	}
      }
    }
  }
  
  if (m_loopDC) m_fei4.writeRegister(&Fei4::CP,m_modeDC); // set DC mode

  // increase ToT if debugging which mask step
  if (debugWhichMaskStep && !clear) {
    if (m_strobeDuration > 0 && m_strobeDuration < 0xFF) m_fei4.writeRegister(&Fei4::CalPulseWidth, (0x00FF & m_strobeDuration)-1);
    else if (m_strobeDuration >= 0xFF) m_fei4.writeRegister(&Fei4::CalPulseWidth, 0xFF);
    // send different cal pulse widths (ToT) to debug which chip is which                                                                                                  
  if (debugWhichChips) {
    int CalPulse = m_strobeDuration - 1;
    for (uint32_t i = 0; i < RodResourceManagerIBL::getNModuleConfigs(); i++) {
      if (m_channels & (1<<i)) {
        if (CalPulse > 0xF) CalPulse = 0xF; // protection against too large ToT                                                                                        
	Fei4Proxy::writeRegister(&Fei4::CalPulseWidth, (CalPulse), (m_channels & (1<<i)));
	std::cout<<"Debug ToT: about to set true ToT of "<<(int)CalPulse<<" in rx ch mask: 0x"<<std::hex<<(int)(m_channels & (1<<i))<<std::dec<<std::endl;
        CalPulse++;; // FE increase in ToT                                                                                                                               
      }
    }
  }
  m_strobeDuration++;
  }

  std::cout<<"done with setupmaskiteration"<<std::endl;
#endif
}

bool Scan::startHistogrammer(int32_t iPar, int32_t iMask) {

  bool success = true;
#ifdef __XMK__
  //  std::cout<<"about to start histgorammer "<<std::endl;
  bool doHistoMS = true; // do mask stepping in histogrammer ?

  // only start histogrammer each mask step for those mask steps supported by histogrammer
  // otherwise only start histogrammer at beginning of scan
  if (!(m_maskSteps == 2 || m_maskSteps == 4 || m_maskSteps== 8)) {
    doHistoMS = false;
    if (!iMask == 0) return success;
  }

  if (ScanBoss::verbose) std::cout << __PRETTY_FUNCTION__ << ": iPar=" << iPar << ", iMask=" << iMask << std::endl;
  if (m_useSlave0) {
    for (int i = 0; i < 2; i++) {
      m_histCfgOut0.cfg[i].binId = iPar;
      m_histCfgOut0.cfg[i].maskId = iMask;
      if (doHistoMS) { m_histCfgOut0.cfg[i].mStepOdd = iMask+1;
	      m_histCfgOut0.cfg[i].mStepEven = m_maskSteps-iMask; }//set mask step position (different for even and odd columns)
      if (ScanBoss::verbose) std::cout<<"at mask step position: "<<(int)iMask<<", setting mStepOdd to: "<<(int)(m_histCfgOut0.cfg[0].mStepOdd)<<" and mStepEven to: "<<(int)(m_histCfgOut0.cfg[0].mStepEven)<<std::endl; 
    }
#ifndef __HISTO_TEST__
    if (!m_slave.startHisto(&m_histCfgOut0,0)) {
      status.errorState = SLAVE_NOT_RESPONDING;
      std::cout<<" Slave 0 did not respond to start histo command"<<std::endl;
      success = false;
    }
    if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer 0" << std::endl;
#endif // no __HISTO_TEST__
  }
  if (m_useSlave1)  {
    for (int i = 0; i < 2; i++) {
      m_histCfgOut1.cfg[i].binId = iPar;
      m_histCfgOut1.cfg[i].maskId = iMask;
      if (doHistoMS) { m_histCfgOut1.cfg[i].mStepOdd = iMask+1;
	m_histCfgOut1.cfg[i].mStepEven = m_maskSteps-iMask; } //set mask step position (different for even and odd columns)
      if (ScanBoss::verbose) std::cout<<"at mask step position: "<<(int)iMask<<", setting mStepOdd to: "<<(int)(m_histCfgOut1.cfg[0].mStepOdd)<<" and mStepEven to: "<<(int)(m_histCfgOut1.cfg[0].mStepEven)<<std::endl; 
    }
#ifndef __HISTO_TEST__
    if (!m_slave.startHisto(&m_histCfgOut1,1)) {
      status.errorState = SLAVE_NOT_RESPONDING;
      std::cout<<" Slave 1 did not respond to start histo command"<<std::endl;
      success = false;
    }
    if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer 1" << std::endl;
#endif // no __HISTO_TEST__
  }
  //  std::cout<<"done with start histgorammer "<<std::endl;
#endif
  return success;
}

void Scan::setupDoubleColumnIteration(int8_t iDC) {
#ifdef __XMK__
  //  std::cout<<"about to setupDC "<<std::endl;
  if (m_loopDC) {
    m_fei4.setRunMode(false);
    m_fei4.writeRegister(&Fei4::Colpr_Addr, iDC);
  }	      
  m_fei4.setRunMode(true);
  //  std::cout<<"done setting up DC "<<std::endl;
#endif
}

void Scan::setupParameterIteration(int32_t iPar) {
#ifdef __XMK__
  if (!m_doParamLoop) return;

  std::cout<<"++++ Starting parameter loop: "<<std::dec<<
    (int)iPar+1<<" out of "<<(int)m_nParameterSteps<<" total loops"<<std::endl;
  
  if (ScanBoss::verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  uint32_t paramValue = (uint32_t)(m_parameterMin + ((m_parameterMax-m_parameterMin)/(m_nParameterSteps-1))*iPar);
  float paramValueFloat = (m_parameterMin + ((m_parameterMax-m_parameterMin)/(m_nParameterSteps-1))*(float)iPar);
  int capType = 0;

  if (ScanBoss::verbose) std::cout<<std::dec<<"setting parameter " << m_scanParam << " value to: "<<(int)paramValue<<std::endl;
  m_fei4.setRunMode(false);

  uint32_t pixRegMaskFull =0x0,pixRegMaskClear =0x0;
  const Mask mask_full(MASK_FULL);
  const Mask mask_clear(MASK_CLEAR);

  switch (m_scanParam) {
  case SCAN_VCAL :
    m_fei4.writeRegister(&Fei4::PlsrDAC,paramValue);
    break;
  case SCAN_L1ALATENCY :
    m_fei4.writeRegister(&Fei4::Trig_Lat,paramValue);
    break;
  case SCAN_TRIGGER_DELAY :
    m_trigDelay = paramValue;
    break;
  case SCAN_STROBE_DELAY :
    m_fei4.writeRegister(&Fei4::PlsrDelay,(0x003F & paramValue));
    break;
  case CHARGE :
    if (ScanBoss::verbose) std::cout<<"Going to do a CHARGE scan; using charge value: = "<<paramValueFloat<<" to calculate vcal; stay tuned"<<std::endl;
    if (m_pixelLatchStrobeValue & 0xc0) capType = 2; // Both capacaitors, i.e. highest charge
    else if (m_pixelLatchStrobeValue & 0x40) capType = 1; // Only CHigh, i.e. medium charge
    // Else defaults to zero, i.e. low charge (when only CLow OR -- ATTENTION -- NO capacitor enabled)
    std::cout<<"m_pixelLatchStroveValue = 0x"<<std::hex<<(int)m_pixelLatchStrobeValue<<" and capType is: "<<capType<<std::endl; // tmp Lj debugging
    m_fei4.writeVcalFromCharge(paramValueFloat,capType);
    break;
  case PixLib::PixScanBase::TDACS :
    {
     std::array<int, 5> TDACreg = {5,4,3,2,1};//TDAC registers 1 MSB
       for (int bit = 0; bit < TDACreg.size(); bit++){
         if( paramValue & (1<<bit) ) pixRegMaskFull |= 1<< TDACreg[bit];
         else pixRegMaskClear |= 1<< TDACreg[bit];
       }
     //std::cout<<"Mask full "<<std::hex<<pixRegMaskFull<<" Mask clear "<<pixRegMaskClear<<std::dec<<std::endl;
     m_fei4.writeRegister(&Fei4::CP,0); // set DC mode to one DC
       for (int iDC = 0; iDC < 40; iDC++){ // hard-coded! 40!
         if(pixRegMaskFull)m_fei4.strobePixelLatches(mask_full, pixRegMaskFull, iDC);
         if(pixRegMaskClear)m_fei4.strobePixelLatches(mask_clear, pixRegMaskClear, iDC);
       }
      if (m_loopDC) m_fei4.writeRegister(&Fei4::CP,m_modeDC); // Restore DC mode
    }
    break;
  case PixLib::PixScanBase::FDACS :
    {
     std::array<int, 4> FDACreg = {9,10,11,12};//FDAC registers 12 MSB
       for (int bit = 0; bit < FDACreg.size(); bit++){
         if( paramValue & (1<<bit) ) pixRegMaskFull |= 1<< FDACreg[bit];
         else pixRegMaskClear |= 1<< FDACreg[bit];
       }
       //std::cout<<"Mask full "<<std::hex<<pixRegMaskFull<<" Mask clear "<<pixRegMaskClear<<std::dec<<std::endl;
       m_fei4.writeRegister(&Fei4::CP,0); // set DC mode to one DC
       for (int iDC = 0; iDC < 40; iDC++){ // hard-coded! 40!
         if(pixRegMaskFull)m_fei4.strobePixelLatches(mask_full, pixRegMaskFull, iDC);
         if(pixRegMaskClear)m_fei4.strobePixelLatches(mask_clear, pixRegMaskClear, iDC);
       }
     if (m_loopDC) m_fei4.writeRegister(&Fei4::CP,m_modeDC); //Restore DC mode
    }
    break;
  case PixLib::PixScanBase::GDAC :
      m_fei4.writeRegister(&Fei4::Vthin_Fine, paramValue & 0xFF);
      //m_fei4.writeRegister(&Fei4::Vthin_Coarse, (paramValue & 0x7F00)>>7);
    break;
  case PixLib::PixScanBase::IF :
      m_fei4.writeRegister(&Fei4::PrmpVbpf, paramValue & 0xFF);
    break;
  default :
    std::cout<<"WARNING: You chose a scan parameter (enum): "<<(int)m_scanParam<<" that is not supported by the PPC code yet."
	     <<"Sorry. You won't get any scanning action."<<std::endl;
  }
#endif
}

void Scan::pulseFrontEnd() {
#ifdef __XMK__
  m_fei4Sport.calTrigger(m_trigDelay);
#endif
}

void Scan::triggerOnly() {
#ifdef __XMK__
  m_fei4Sport.trigger();
  usleep(m_sleepDelay);
#endif
}

void Scan::checkReadyForNextTrigger() {
  // not yet implemented at all
  usleep(m_sleepDelay);
}
void Scan::debugReadBack(int32_t iMask) { 
#ifdef __XMK__

  if(ScanBoss::readSR){
    Fei4Data::setVerboseSR(true); 
    Fei4 f =  RodResourceManagerIBL::getCalibModuleConfig(ScanBoss::rxCh);
    for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount)
      f.readFrame();    
  }
  
  if (ScanBoss::readBackInmem){
    uint32_t fifoByte = 0xc0ffee;
    for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
      fifoByte = (fifo.read()&0xFF);
      for (int i = 0; i < 1000; i++) {
	fifoByte = (fifo.read()&0xFF);
	if (i%3 == 0) std::cout<<std::dec<<"read back frame : "<<(int)iTrigCount<<" from inmem fifo ch "<<
	  (int)ScanBoss::rxCh<<": Word "<<int(i/3)<<" = 0x"<<std::hex<<fifoByte;
	else if (i%3 == 1) std::cout<<std::hex<<fifoByte;
	else std::cout<<std::hex<<fifoByte<<std::dec<<std::endl;

	if ((fifoByte == 0xBC) || (fifoByte == 0x00)) break;
	if (fifoByte == 0x800) break;
      }
      std::cout<<std::endl;
    }
    std::cout<<"**********************************"<<std::dec<<std::endl;
  }

  // Loop over all debug hists and fill for each trigger.
  for(auto &hist: ScanBoss::getDebugHists()) {
    Fei4 & f = RodResourceManagerIBL::getCalibModuleConfig(hist.rxCh);
    for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
      Fei4Data::fillPixDataHist(f.readFrame(), f.getValue(&Fei4Cfg::HD), hist);
    }
  }

  if (!ScanBoss::readBackBoc) return;
  // if you want to read out from the inmem fifo or bob fifo
  // for debug purpose, put that code here
  std::vector<int> answer;
  Fei4 & f =  RodResourceManagerIBL::getCalibModuleConfig(ScanBoss::rxCh);
  for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
    answer = f.readFrame();
    // Original print out version
    if (ScanBoss::readBackBoc) {
      std::cout<<"**********************************"<<std::endl;
      std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo channel "<<(int)ScanBoss::rxCh<<std::endl;
      std::cout<<"**********************************"<<std::endl;
      unsigned int answerSize = answer.size();
      for (unsigned int i = 0; i < answerSize; i++) {
	if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<std::setw(2)<<answer[i];
	else if (i%3 == 1) std::cout<<std::hex<<std::setw(2)<<answer[i];
	else std::cout<<std::hex<<std::setw(2)<<answer[i]<<std::dec<<std::endl;
      }
      //      std::cout<<"**********************************"<<std::endl;
      //      Fei4Data::printPixData(answer, f.->getValue(&Fei4Cfg::HD)); // tmp, LJ! for debugging
      //      std::cout<<"**********************************"<<std::endl;
    } // printOut
  }
#endif
}

void Scan::getParameterFeedback(int32_t iPar) {
  // anything you want to do before starting next parameter loop
}

bool Scan::stopHistogrammer(int32_t iMask) {
  bool success = true;
#ifdef __XMK__
  //  std::cout<<"about to stop histogrammer "<<std::endl;
  if (ScanBoss::verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;
  // only stop histogrammer each mask step for those mask steps supported by histogrammer
  // otherwise only stop histogrammer at end of scan
  if (!(m_maskSteps == 2 || m_maskSteps == 4 || m_maskSteps== 8) && !(iMask == (m_maskStepsToDo-1))) return success;

  if (m_useSlave0) success =  m_slave.stopHisto(0);
  if (m_useSlave1) success &= m_slave.stopHisto(1);
  if (!success) {
    status.errorState = SLAVE_NOT_RESPONDING;
    std::cout<<" Slave did not respond to stop histo command"<<std::endl;
  }
  if (success && ScanBoss::verbose) std::cout << "----stopped histogrammer" << std::endl;
  //  std::cout<<"stopped histogrammer "<<std::endl;
#endif
  return success;
}

////////////////////////////////////////////
// Data processing and resetting of system
////////////////////////////////////////////

void Scan::processResult() { 
#ifdef __XMK__
  // dump end of scan debug

  // expected number of frames = total number of triggers * frame count
  uint32_t expected = m_nTriggers * m_nDCLoops * m_maskStepsToDo * m_nParameterSteps * m_frameCount;
  
 for (uint32_t i = 0; i<RodResourceManagerIBL::getNModuleConfigs(); i++) 
   if (m_channels & (1<<i)) {
     uint32_t received = (int)IblBoc::GetFrameCount(i);
      std::cout<<"Rx "<<(int)i<<" received: "<<(int)received
	       <<" start of frames when I expected "<<(int)expected<< ((received == expected) ? " Correct " : " Not correct ") <<std::endl;
   }
   
   //reading BOC Tx counters
  std::cout<<" Reading BOC Tx monitoring counters "<<std::endl;
  std::cout << "TxCh |  Trigger   | BCR        | ECR        | CAL        | Slow       | Corrupt" << std::endl;
	std::cout << "===+============+============+============+============+============+============+============" << std::endl;
	std::array <uint32_t,7> monCnt;
	uint32_t txMask = IblConnectivity::getTxMaskFromRxMask(m_channels);
	for(uint8_t txCh=0;txCh<32;txCh++)
	  if (txMask & (1<<txCh)){
	  IblBoc::getTxMonitoringCounters(txCh/16, txCh%16, monCnt.data());
	   std::cout << std::setfill(' ') << std::setw(4) << (int)txCh << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[0] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[2] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[1] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[3] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[4] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[5] << std::endl;
           IblBoc::DisableTxMonitoring(txCh/16, txCh%16);
	  }
  // Process debug output?
#endif
}

void Scan::resetSystem() {
#ifdef __XMK__

  status.state = SCAN_TERMINATED;

  // Reset the pixel latches, by writing a final mask stage mode in clear mode
  if(m_dspScanType == SetDSPScan::NONE) setupMaskIteration(0, true);
    
#ifndef __HISTO_TEST_

  m_iblBocLink->disableChannels(0xFFFFFFFF); 
  RodResourceManagerIBL::getSerialPort()->setTxMask(0x0);//reset Serial Port

  DataTakingTools::setBocEmu(false, 0xFFFFFFFF);//Disable emulator

  // Turn off the BOC->ROD link to prevent junk from filling the slaves while not running
  DataTakingTools::disableFmtLink(0xFFFFFFFF);

#else
        status.state = SCAN_TERMINATED;
#endif // no __HISTO_TEST__

  resetGlobalRegister(&Fei4::Colpr_Addr, m_globalConfigStore);
  resetGlobalRegister(&Fei4::CP, m_globalConfigStore);
  resetGlobalRegister(&Fei4::Trig_Count, m_globalConfigStore);
  resetGlobalRegister(&Fei4::Trig_Lat, m_globalConfigStore);
  resetGlobalRegister(&Fei4::DHS, m_globalConfigStore);
  resetGlobalRegister(&Fei4::PlsrDelay, m_globalConfigStore);
  resetGlobalRegister(&Fei4::PlsrIDACRamp, m_globalConfigStore);
  resetGlobalRegister(&Fei4::CalPulseWidth, m_globalConfigStore);

  m_dspScanType = SetDSPScan::NONE;
  // To-do: reset rest of ROD
#endif // __XMK__
}

void Scan::registerDump() {
#ifdef __XMK__
  std::cout<<"********************"<<std::endl;
  std::cout<<"Register debug, after setting up DC and about to trigger: "<<std::endl;
  std::cout<<"********************"<<std::endl;

  DumpRodRegisters cmd;
  cmd.dump();

  std::cout<<"******about to sleep for 10 seconds"<<std::endl;
  usleep(10000000); 
#endif
}

void Scan::abortScan(uint32_t scanId) {
  if(scanId != m_scanId) std::cout << " ** Warning scan ID to be aborted(" << scanId << ") and Scan ID being aborted(" << m_scanId << ") do not match !!" << std::endl;
  aborting = true;
}


std::vector< std::vector< uint32_t > > Scan::getDSPResults(uint8_t rxCh) {

	return m_dspResults[rxCh];

}

void Scan::cleanDSPResults() {

	m_dspResults.clear();

}

void Scan::cleanDSPResults(uint8_t rxCh) {

	m_dspResults[rxCh].clear();
}

#ifdef __XMK__
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
  void Scan::resetGlobalRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, std::vector<Fei4GlobalCfg>& cfgs) {
  uint32_t nMod = RodResourceManagerPIX::getNModuleConfigs();
  for (size_t modIx = 0; modIx < nMod; ++modIx){
    T resetToValue = cfgs[modIx].getValueEndian(ref);
    Fei4GlobalCfg & currentCfg = RodResourceManagerIBL::getCalibModuleConfig(modIx);
    currentCfg.writeField(ref, resetToValue);
  }
}
#endif
