/*
Steve Alkire <alkire@cern.ch>, 2014.02.11
*/
#include <iostream>
#include "ScanBoss.h"
#include "ResetSlave.h"


Scan ScanBoss::m_scan;
ScanL12 ScanBoss::m_scanL12;
PixLib::PixScanBase ScanBoss::scanBase;
std::string ScanBoss::howDoIFeel = "NOT SET";
//CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> ScanBoss::iterFei4PixelCfg(&ScanBoss::testFei4PixelCfg.dcb[0][0]);
unsigned ScanBoss::Offset = 1;
DoubleColumnBit ScanBoss::testIterSB[5];
//Testa<DoubleColumnBit,Fei4PixelCfg::n_Bit,Fei4PixelCfg::n_DC> ScanBoss::testim(ScanBoss::testFei4PixelCfg.dcb[0]);
bool 	ScanBoss::verbose       = false;
bool 	ScanBoss::readBackBoc   = false;
bool 	ScanBoss::readBackInmem = false;
bool 	ScanBoss::readSR = false;
uint8_t ScanBoss::rxCh          = 0;
uint8_t ScanBoss::debugDCLoops          = 0;
uint32_t ScanBoss::debugChannels          = 0;

IblRodScanStatus ScanBoss::scanResult;

std::vector<Fei4Hist> ScanBoss::m_debugHists;

PixLib::PixScanBase& ScanBoss::getPixScanBase(){
  return scanBase;
}

void ScanBoss::setPixScanBase(PixLib::PixScanBase sb){
  scanBase = sb;
  return;
}

#ifdef __DO_IBL_GET_STATUS__
void ScanBoss::startScan(uint32_t channels, uint32_t scanId, uint32_t fitPorts[4], uint8_t fitIps[4][4]){
#else
IblRodScanStatus ScanBoss::startScan(uint32_t channels, uint32_t scanId, uint32_t fitPorts[4], uint8_t fitIps[4][4]){
#endif

  m_scan.setChannels(channels);
  m_scan.setScanId(scanId);
  m_scan.setFitServerPorts(fitPorts);
  m_scan.setFitServerIps(fitIps);

  for(auto &hist: m_debugHists) {
    hist.clear();
  }

  std::cout<<"** About to init scan: **"<<std::endl;
  m_scan.aborting = false;
  if(m_scan.initScan()) {
    if(m_scan.aborting) std::cout << "**** Scan aborted during initScan()!" << std::endl;
    else {
      std::cout<<"** About to run scan: ** "<<std::endl;
      m_scan.runScan();
      if(m_scan.aborting) std::cout << "**** Scan aborted during runScan()!" << std::endl;
      else {
        std::cout<<"** About to end scan: ** "<<std::endl;
        m_scan.endScan();
        if(m_scan.aborting) std::cout << "**** Scan aborted during EndScan()!" << std::endl;
        else {
          std::cout<<"** Woah, I finished the scan **"<<std::endl;
        }
      }
    }
    if(m_scan.aborting) {
      if(m_scan.getStatus().state != SCAN_TERMINATED)m_scan.endScan(); //  Note:  status.state == SCAN_TERMINATED means endScan already run!
      ResetSlave cmd;
      cmd.resetHisto = true;
      cmd.resetNetwork = true;
      cmd.execute();
    }
    m_scan.aborting = false;
  } else {
    std::cout << "**** No Scan run because init scan failed.  ROD software probably needs to be restarted." << std::endl;
    m_scan.endScan();
  }
#ifdef __DO_IBL_GET_STATUS__
  return;
#else
  scanResult = m_scan.getStatus();
  return scanResult;
#endif

}

void ScanBoss::startRegisterScan(uint32_t channels){
  std::cout << "Starting register scan." << std::endl;
  //m_registerScan.runScan();
  std::cout << "Done with register scan." << std::endl;
}
#ifdef __DO_GET_STATUS__
void ScanBoss::startScanL12(uint32_t channels, uint32_t scanId, uint32_t fitPorts[4], uint8_t fitIps[4][4]){
#else
IblRodScanStatus ScanBoss::startScanL12(uint32_t channels, uint32_t scanId, uint32_t fitPorts[4], uint8_t fitIps[4][4]){
#endif

  m_scanL12.setChannels(channels);
  m_scanL12.setScanId(scanId);
  m_scanL12.setFitServerPorts(fitPorts);
  m_scanL12.setFitServerIps(fitIps);

//   BOOST_FOREACH(Fei4Hist& hist, m_debugHists) {
//     hist.clear();
//   }

  std::cout<<"** About to init scan: **"<<std::endl;
  m_scanL12.aborting = false;
  if(m_scanL12.initScan()) {
    if(m_scanL12.aborting) std::cout << "**** Scan aborted during initScan()!" << std::endl;
    else {
      std::cout<<"** About to run scan: ** "<<std::endl;
      m_scanL12.runScan();
      if(m_scanL12.aborting) std::cout << "**** Scan aborted during runScan()!" << std::endl;
      else {
        std::cout<<"** About to end scan: ** "<<std::endl;
        m_scanL12.endScan();
        if(m_scanL12.aborting) std::cout << "**** Scan aborted during EndScan()!" << std::endl;
        else {
          std::cout<<"** Woah, I finished the scan **"<<std::endl;
        }
      }
    }
    if(m_scanL12.aborting) {
      if(m_scanL12.getStatus().state != SCAN_TERMINATED)m_scanL12.endScan(); //  Note:  status.state == SCAN_TERMINATED means endScan already run!
      ResetSlave cmd;
      cmd.resetHisto = true;
      cmd.resetNetwork = true;
      cmd.execute();
    }
    m_scanL12.aborting = false;
  } else {
    std::cout << "**** No Scan run because init scan failed.  ROD software probably needs to be restarted." << std::endl;
    m_scanL12.endScan();
  }
#ifdef __DO_GET_STATUS__
  return;
#else
  scanResult = m_scanL12.getStatus();
  return scanResult;
#endif

}

void ScanBoss::addDebugHist(const Fei4Data::DataType& data, const Fei4Data::FIFOType& fifo, uint8_t rxChArg){
  ScanBoss::m_debugHists.push_back(Fei4Hist(data, fifo, rxChArg));
  std::cout << "Added a new debug histogram with data: " << data << " fifo: " << fifo << " and rxChArg: " << (int)rxChArg << std::endl;
  std::cout << "There are now " << ScanBoss::m_debugHists.size() << " debug histograms enabled." << std::endl;
}

void ScanBoss::clearDebugHists(){
  ScanBoss::m_debugHists.clear();
}
