/*
 * * N. Dreyer <dreyern@u.washington.edu>
 * * F. Meloni <federico.meloni@cern.ch>
 * Date: 25-March-2016
 *
 * L1-L2 Scan Driver called from BarrelRodPixController.cxx via ScanBoss (equivalent functionality as IblScan.cxx).
 *
 */

// #define __SINGLE_MASK_STEP__ //  Comment out for regular operation !!!!

// #define __MCC_TEST__ //  Comment out for regular operation !!!!
#define __USE_FEI3__ 1 //  In __MCC_TEST__ mode only:  1 uses Fei3TestScan, 0 uses MCCTestScan.

///////////////////////////  define-setion for Nick's Dummy Scans  ///////////////////////////
//  When we figure out (decide?) how, these should be moved out of compile-time paramters

// #define __HISTO_TEST__ //  Comment out for regular operation !!!!

#ifdef __HISTO_TEST__

#include "iblSlaveCmds.h"

#define SCANTYPE PIXELADDRESS             //  CHECKERBOARD, PIXELADDRESS or THR_SCURVE_FIT (DummyScanType enum 0, 1 or 2 - see iblSlaveCmds.h)
//  Choose calibration console "DIGITAL_TEST" to launch the first two, "THRESHOLD_SCAN" for the thrid.

#define INJ_TYPE HISTO_TEST_SETUP         //  Choices are:
//  HISTO_TEST_SETUP         = INTERNAL injection test using histogrammer (but NOT slave formatter)
//  HISTO_TEST_SETUP_NOHISTO = INTERNAL injection test without touching histogrammer
//  HISTO_TEST_SETUP_EXT     = EXTERNAL injection test involving BOTH histogrammer AND slave formatter

#define MULTIHIT 1                        //  1 = Add total number of injections at end of slave command to speed things up.

#endif // __HISTO_TEST__
//
//////////////////////////////////////////////////////////////////////////////////////////////

#define __HISTO_FILTERING__ //  This MUST be set for normal operation (commenting out disables histogrammer mask step filtering)

#include "IblScanL12.h"

#ifdef __XMK__
#include "ScanBoss.h"
#include "RodResourceManager.h"
#include "DumpRodRegisters.h"
#include "IblBoc.h"
#include "IblBocLink.h"
#include "BarrelConnectivity.h"
#include "DataTakingTools.h"
#include "Fei3MonLeak.h"
#include "OptoScan.h"
#include "DataTakingTools.h"
#include <arpa/inet.h>

#if __USE_FEI3__
#include "Fei3TestScan.h"
#else
#include "MCCTestScan.h"
#endif

#include <algorithm>
#include <iomanip>
#include <bitset>
#else
#include <unistd.h>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

ScanL12::ScanL12() {
	m_useSlave0 = false;
	m_useSlave1 = false;
	// temporary or debug parameters
	debugWhichChips = false; // add +1 ToT to each chip for unique identification
	debugWhichMaskStep = false; // add +1 ToT for each mask step
	lowDebug = false; // low level debug, print out register values + pause before each trigger
	aborting = false;
	m_dspScanType = SetDSPScan::NONE;
}

bool ScanL12::initScan() {

#ifdef __XMK__
	// get pixscanbase from scan boss
	m_scan = &(ScanBoss::getPixScanBase());

	status.state = SCAN_INIT;
	status.errorState = PPCERROR_NONE;
	status.mask = 0;
	status.par = 0;
	status.par1 = 0; //  Only used in optoscan for now
	status.par2 = 0; //  Not used at all (yet?)
	status.dc = 0;
	status.trig = 0;

#endif // __XMK__

#ifdef __HISTO_TEST__
	std::cout << std::endl << "***** L12IblScan running in dummy mode with the settings listed below *****" << std::endl
	          << std::endl << "           SCANTYPE = " << SCANTYPE << std::endl
	          << "           INJ_TYPE = " << INJ_TYPE << std::endl
	          << "           MULTIHIT = " << MULTIHIT << std::endl << std::endl;
	m_channelsConsole = m_channels;
#endif // __HISTO_TEST__
	fillScanParameters();
#ifndef __HISTO_TEST__
	initializeFrontEnd();
#endif // no __HISTO_TEST__
	m_dspResults.clear();

	return initializeROD();

}

void ScanL12::runScan() {
#ifdef __XMK__

	if (m_dspScanType != SetDSPScan::NONE) {
          
          if(m_dspScanType == SetDSPScan::BOC_THR_DEL_DSP)runOptoScan();
          if(m_dspScanType == SetDSPScan::LEAK_SCAN_DSP)runMonLeakScan();

	} else {
		// For now, loop order and number is hard-coded

		status.state = SCAN_IN_PROGRESS;

		// Mask stage loop
		int32_t iMask;
		for (iMask = 0; iMask < m_maskStepsToDo; ++iMask) {
			status.mask = iMask;
			setupMaskIteration(iMask);

			bool slave_problem = false;
			for (int32_t iPar = 0; iPar < m_nParameterSteps; ++iPar) {
				status.par = iPar;
      				setupParameterIteration(iPar);
				if(!startHistogrammer(iPar, iMask)) {slave_problem = true; break;};
      				if (ScanBoss::verbose) std::cout << "RodResourceManagerPIX::getSerialPort()->getTxMask(m_channels) = " << HEX(RodResourceManagerPIX::getSerialPort()->getTxMask()) << std::endl;

#ifdef __HISTO_TEST__
// //  Get Configuration that must have been prevoiusly set
//       IblSlvHistCfg histConfig[2];
				IblRodSlave s;
//       for(uint32_t whichSlave=0; whichSlave<2; whichSlave++) histConfig[whichSlave] = s.getHistCfg(whichSlave);

// Checck all four units and only use if covered by the m_channels mask.
				IblSlvHistCfg* pHistConfig[2] = {&m_histCfgOut0, &m_histCfgOut1};
				int usms = 0; //  = unit scan mask shift
				for (uint32_t whichSlave = 0; whichSlave < 2; whichSlave++) {
					if (m_useSlave0 && whichSlave == 0 || m_useSlave1 && whichSlave == 1) {
						uint32_t useUnit[2] = {pHistConfig[whichSlave]->cfg[0].enable, pHistConfig[whichSlave]->cfg[1].enable};
						for (uint32_t histoUnit = 0; histoUnit < 2; histoUnit++) {
							if (useUnit[histoUnit]) {
								pHistConfig[whichSlave]->cfg[histoUnit].enable = 1;
								pHistConfig[whichSlave]->cfg[1 - histoUnit].enable = 0;
								bool success = m_slave.startHisto(pHistConfig[whichSlave], whichSlave);
								if (!success) {
									status.errorState = SLAVE_NOT_RESPONDING;
									std::cout << " Slave 0 did not respond to start histo command" << std::endl;
								}
								if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer " << histoUnit << std::endl;
								uint32_t unitMask = (0x000000FF & (m_channelsConsole >> 8 * usms));
								std::cout << " *** Calling fillDummyHisto for Slave " << whichSlave << ", Unit "
								          << histoUnit << ", unitMask = " << HEX(unitMask)
								          << ", iMask = " << iMask << ", iPar = " << iPar << " ***" << std::endl;
								s.fillDummyHisto(pHistConfig[whichSlave], iMask, iPar, unitMask, m_nParameterSteps, whichSlave, histoUnit, SCANTYPE, INJ_TYPE, MULTIHIT);
								pHistConfig[whichSlave]->cfg[histoUnit].enable = useUnit[histoUnit];
								pHistConfig[whichSlave]->cfg[1 - histoUnit].enable = useUnit[1 - histoUnit];
							}
							if (aborting)break;
							usms++;
						}
					} else usms += 2;
					if (aborting)break;
				}
#else

      // FE loop
      int nFEgroup = 1; //  Only set nFEgroup to anything but 1 for testing purposes AND you know what you are doing !
      if(m_loopDC) nFEgroup = 1; //  FE loop disabled this way if DC looping selected
      int FEgroupSize = 16/nFEgroup;
      uint32_t FEgroupMaskBits = (0x1 << FEgroupSize) - 1;
      for (int iFEgroup = 0; iFEgroup < nFEgroup; iFEgroup++){
//       for (int iFEgroup = 0; iFEgroup < 1; iFEgroup++){
//       for (int iFEgroup = 1; iFEgroup < nFEgroup; iFEgroup++){
        FEEN_Mask =  FEgroupMaskBits << iFEgroup * FEgroupSize;
        //Soft reset of the modules
	m_fei3Sport.mccGlobalResetFE(0x4);
	//Send ECR to clean the ROD FIFOs
	m_fei3Sport.sendECR();
	std::this_thread::sleep_for(std::chrono::milliseconds(1));
      // double column loop
      for (int8_t iDC = 0; iDC < m_nDCLoops; ++iDC) {
        status.dc = iDC;
#ifdef __SINGLE_MASK_STEP__
if(iMask > 0) break;
#endif
	if (ScanBoss::verbose) std::cout<<std::dec<<"++++++ Starting DC loop: "<<
	  (int)iDC+1<<" out of "<<(int)m_nDCLoops<<" total loops"<<", mask loop = "<<(int)iMask+1<<", and par loop = "<<(int)iPar+1<<std::endl;
	
	setupDoubleColumnIteration(iDC);

	if (lowDebug) registerDump();

	//  std::cout<<" about to start triggering "<<std::endl;
	if (ScanBoss::verbose) std::cout << std::dec << "++++++++ Starting to send " << (int)m_nTriggers << " triggers" << std::endl;
	for (int32_t iTrig = 0; iTrig < m_nTriggers; ++iTrig) {
          status.trig = iTrig;
	  if (m_noiseScan) triggerOnly();
	  else {
	    pulseFrontEnd(iMask);
	    checkReadyForNextTrigger();
	  }
	  debugReadBack(iMask);
#ifdef __MCC_TEST__
	  break;
#endif
					} // trigger loop
					if (ScanBoss::verbose) std::cout << std::dec << "++++++++ Done sending triggers " << std::endl;

	//  std::cout<<" done with trigger loop"<<std::endl;
	if(aborting)break;
#ifdef __MCC_TEST__
	break;
#endif
      } // double column loop
      } // FE loop
      getParameterFeedback(iPar);

				// Not needed for L12 usleep(2000); // tmp lj to-do: is this necessary?
				if(!stopHistogrammer(iMask)) {slave_problem = true; break;};
#endif // no __HISTO_TEST__

				if (aborting)break;
			} // parameter loop
			if (aborting || slave_problem)break;
		} // mask stage loop
		if(iMask == m_maskStepsToDo)status.state = SCAN_COMPLETE; //  Normal termination.  Otherwise aborting or slave_problem.
	}
#endif
}

void ScanL12::endScan() {
#ifndef __HISTO_TEST__
	processResult();
#endif	
	if (aborting  && m_dspScanType != SetDSPScan::NONE) cleanDSPResults();	

        resetSystem();

	std::cout << " ** Scan cleanup complete." << std::endl;
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Scan Internal Functions
//////////////////////////////////////////////////////////////////////////////////////////////
void ScanL12::runOptoScan() {
#ifdef __XMK__
	status.state = SCAN_IN_PROGRESS;

	uint32_t gain = 0;
	uint32_t nTriggers = m_nTriggers;
	uint32_t bitPattern = 0xBFE;//TODO make configurable
	uint32_t nPatterns = 11; //number of patterns Max of 11!
	PixLib::PixScanBase::MccBandwidth mccBandwidth = m_scan->getMccBandwidth();
	if( mccBandwidth == PixLib::PixScanBase::DOUBLE_40 ) mccBandwidth = PixLib::PixScanBase::DOUBLE_80;
	IblBoc::PixSetSpeed( mccBandwidth ); // Setting speed for BOTH BMFs

	std::cout << "Gain: " << gain << std::endl;
	std::cout << "Number of Triggers: " << m_nTriggers << std::endl;
	std::cout << "Speed: " << (int)mccBandwidth << std::endl; // 0 -> 40Mbit, 1-> 80Mbit
	std::cout << "Number of patterns: " << nPatterns << std::endl;
	std::cout << std::endl;

	uint32_t delaySteps = 0;
	float delayMin = 0;
	float delayMax = 0;
        float delayDelta = 0;
	uint32_t thrSteps = 0;
	float thrMin = 0;
	float thrMax = 0;
        float thrDelta = 0;

	//Loop over all the scan parameters to get THR and DELAY
	for(int sP =0;sP<3;sP++){
	 if( !m_scan->getLoopActive(sP) )continue;
	  switch (m_scan->getLoopParam(sP)){
	    case PixLib::PixScanBase::BOC_RX_DELAY:
	      delaySteps = m_scan->getLoopVarNSteps(sP);
	      delayMin = 1000* m_scan->getLoopVarMin(sP);
	      delayMax = 1000* m_scan->getLoopVarMax(sP);
	      delayDelta = (delayMax - delayMin)/delaySteps;
	    break;
	    case PixLib::PixScanBase::BOC_RX_THR:
	      thrSteps = m_scan->getLoopVarNSteps(sP);
	      thrMin = m_scan->getLoopVarMin(sP)*250 + 2500;
	      thrMax = m_scan->getLoopVarMax(sP)*250 + 2500;
	      thrDelta = (thrMax - thrMin)/thrSteps;
	    break;
	    default :
	    std::cout<<"WARNING: You chose a scan parameter (enum): "<<(int)m_scanParam<<" that is not supported by the opto-scan "<<std::endl;
	  }
	}

	std::cout<<"RxMask 0x"<<std::hex<<m_channels<<std::dec<<std::endl;
	std::cout << "Delay steps : " << delaySteps << " " << delayMin  << " " << delayMax << std::endl;
	std::cout << "Threshold steps : " << thrSteps << " " << thrMin  << " " << thrMax << std::endl;

	uint32_t nBitsCheck = 0;
	uint32_t extendedRxMask = OptoScan::initOpto(m_channels,mccBandwidth,gain,bitPattern,nPatterns, nBitsCheck);

	std::cout<<"Extended RxMask: "<<std::hex<<extendedRxMask<<std::dec<<std::endl;
	std::cout<<"Compare stream size: "<<nBitsCheck<<" bits"<<std::endl;

	//Delay loop
	for (uint32_t nDel = 0; nDel < delaySteps; nDel++ ) {
	  uint32_t iDelay = (uint32_t)(delayMin + nDel*delayDelta + delayDelta/2.);
	  status.par = nDel;
          xil_printf("Scan PAR-1 %d / %d\n", status.par,delaySteps);
	  uint32_t coarseD = iDelay*8/25000;
	  uint32_t fineD = (iDelay - coarseD*3125)/35;

	  m_fei3Sport.sendMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay
  	  OptoScan::setDelay(extendedRxMask,coarseD, fineD);
  	  m_fei3Sport.mccGlobalResetMCC();
	  m_fei3Sport.sendMCCRegister(CSR, 0x0040 | (int8_t)mccBandwidth );//setMCCspeed
	  std::map< uint8_t, std::vector< uint32_t > > thresResult;

		//Threshold loop
		for (uint32_t nThr = 0; nThr < thrSteps; nThr++) {
		  uint32_t iThr = (uint32_t)(thrMin + nThr*thrDelta + thrDelta/2.);
		  status.par1 = nThr;
		  OptoScan::setThreshold(extendedRxMask, iThr);

		  std::map<uint8_t, uint32_t> err = OptoScan::getOptoErrors(extendedRxMask, nTriggers, nBitsCheck, bitPattern, nPatterns , m_fei3Sport);
		  for (std::map<uint8_t, uint32_t >::iterator it = err.begin(); it!=err.end(); ++it)thresResult[it->first].push_back(it->second);
		  }//threshold loop
	    for (std::map<uint8_t, std::vector< uint32_t> >::iterator it2 = thresResult.begin(); it2!=thresResult.end(); ++it2)m_dspResults[it2->first].push_back(it2->second);

	    if (aborting)break;

	  } //coarse delay loop

    OptoScan::cleanUp(extendedRxMask, m_fei3Sport);

	if(aborting) {
	  std::cout << "Optoscan aborted" << std::endl;
	} else {
	  status.state = SCAN_COMPLETE;
	  std::cout << "Optoscan done" << std::endl;
	}
//printOptoResults();

#endif // _XMK_
}

void ScanL12::runMonLeakScan() {
#ifdef __XMK__

  status.state = SCAN_IN_PROGRESS;

  uint16_t maxPixPerFE = Fei3PixelCfg::nDC*Fei3PixelCfg::nWordsPerDC*32;
  //No mask step iterationin monLeak scan, we just get the number of Pixels to scan from the maskStage mode
  uint16_t nPixPerFE =  maxPixPerFE*m_maskStepsToDo/m_maskSteps;

   if(nPixPerFE > maxPixPerFE  ){
   nPixPerFE = maxPixPerFE;
   std::cout<<"You requested "<<(int)nPixPerFE<<" Pixel per FE but maximum number of Pixels per FE is "<<(int)maxPixPerFE<<" setting to this value "<<std::endl;
   }

   std::cout<<"Number of Pixels per FE "<<(int)nPixPerFE<<std::endl;

   int steps [MAX_STEPS_MON_LEAK] = {512,256,128,64,32,16,8,4,2,1};//Default steps for the binary search
   int nSteps = 9;//Default

   int IFSet = 1;
   int ITrimIFSet = 1;
/*
   //Loop over all the scan parameters to get IF, ITrimIF and steps for the binary search (NO_PAR)
   for(int sP =0;sP<3;sP++){
	if( !m_scan->getLoopActive(sP) )continue;
	  switch (m_scan->getLoopParam(sP)){
	    case PixLib::PixScanBase::IF :
	      IFSet = (int)m_scan->getLoopVarMin(sP);
	    break;
	    case PixLib::PixScanBase::TRIMF :
	      ITrimIFSet = (int)m_scan->getLoopVarMin(sP);
	    break;
	    case PixLib::PixScanBase::NO_PAR ://Binary search steps
	      nSteps = 0;
	        for(size_t i=0;i<m_scan->getLoopVarValues(sP).size();i++){
	          if(nSteps >= MAX_STEPS_LEAK_SCAN - 1)break;
	          if(i==0){
	           ADCStart = (int)m_scan->getLoopVarValues(sP).at(i);
	          } else{
	           step[nSteps] = (int)m_scan->getLoopVarValues(sP).at(i);
	           nSteps++;
	          }
	        }
	    break;
	    default :
	    std::cout<<"WARNING: You chose a scan parameter (enum): "<<(int)m_scanParam<<" that is not supported by the leak scan "<<std::endl;
	}
    }
*/
  std::cout<<"IF "<<IFSet<<" IFTrim "<<ITrimIFSet<<std::endl;

  for(int i=0;i<nSteps;i++){
  std::cout<<"Step "<<i<<" value "<< steps[i]<<std::endl; 
  }

  uint32_t skipRxMask =0;
  
  uint32_t newRxMask = Fei3MonLeak::initMonLeak(m_channels);
  uint32_t nBitsCheck = Fei3MonLeak::loadCompareBits(newRxMask);

  std::map< uint8_t, std::vector <uint32_t > > MonLeak;

  for(uint16_t index=0;index<nPixPerFE; index++){
    MonLeak.clear();
    if (aborting)break;
      Fei3MonLeak::getMonLeakADC(newRxMask, index, nSteps, steps, nBitsCheck, skipRxMask, MonLeak);

    for (std::map<uint8_t, std::vector <uint32_t > >::iterator it = MonLeak.begin(); it!=MonLeak.end(); ++it)m_dspResults[it->first].push_back(it->second);
  }

  Fei3MonLeak::cleanUp(m_channels);

	if(aborting) {
	  std::cout << "MonLeak scan aborted" << std::endl;
	} else {
	  status.state = SCAN_COMPLETE;
	  std::cout << "MonLeak scan done" << std::endl;
	}

#endif // _XMK_
}


////////////////////////////////////////////
// Initialization
////////////////////////////////////////////

void ScanL12::fillScanParameters() {
#ifdef __XMK__

  // fill internal variables from PixScanBase

  // mask stage and double column loop variables
  PixLib::EnumMaskSteps::MaskStageSteps requestedMaskSteps = m_scan->getMaskStageTotalSteps();
  m_loopDC = setupMaskSteps(requestedMaskSteps, m_maskSteps,m_modeDC);
  m_maskStepsToDo = m_scan->getMaskStageSteps(); if(m_maskStepsToDo < 1) m_maskStepsToDo = 1;
  m_maskStageMode = m_scan->getMaskStageMode();

  m_pixelLatchStrobeValue = decodeMaskStageMode(m_maskStageMode);

  m_nDCLoops = 1;
  if (m_loopDC && (m_modeDC == 0)) m_nDCLoops = 9;
  else if (m_loopDC && m_modeDC == 1) m_nDCLoops = 2;
  else if (m_loopDC && m_modeDC == 2) m_nDCLoops = 4;

  // trigger variables
  m_nTriggers = m_scan->getRepetitions();
  m_frameCount = m_scan->getConsecutiveLvl1Trig();
  m_trigDelay = m_scan->getStrobeLVL1Delay();
  m_trigDelayOverride = m_scan->getStrobeLVL1DelayOverride();
  m_trigLatency = m_scan->getLVL1Latency();
  m_noiseScan = m_scan->getSelfTrigger(); // this is cheating; to-do, add a new variable?


  // FE/MCC variables - PF added to maintain the same logic of the other assignments
  m_ReadMode         = m_scan->getTotThrMode();
  m_TotMin           = m_scan->getTotMin();
  m_TotDHThr         = m_scan->getTotDHThr();
  m_TotTimeStampMode = m_scan->getTotTimeStampMode();
  m_ColumnROFreq     = m_scan->getColumnROFreq();

  // calibration pulse variables
  m_strobeDuration = m_scan->getStrobeDuration();
  m_strobeDelay = m_scan->getStrobeMCCDelay(); 
  m_strobeDelayRange = m_scan->getStrobeMCCDelayRange();
  m_feVCal = m_scan->getFeVCal();
  m_feCapType = (m_scan->getChargeInjCapHigh() ? 1 : 0);

  // type of scan
  m_doDigitalInjection = m_scan->getDigitalInjection();
  m_setToTHistoMode = false; // default is occupancy mode
  // set histogrammer to LONG_TOT if any TOT histogram is desired
  if (m_scan->getHistogramFilled(PixLib::EnumHistogramType::TOT_MEAN) || m_scan->getHistogramFilled(PixLib::EnumHistogramType::TOT_SIGMA))
	  m_setToTHistoMode = true;
  
  // To-do: update for variable parameter step size scanning
  // To-do: allow for more than 1 parameter scan, or to change parameter loop position
  // Only do a parameter loop on PPC if dspProcessing flag is set to true
  m_doParamLoop = (m_scan->getLoopActive(0) && m_scan->getDspProcessing(0));
  m_doParam1Loop = (m_scan->getLoopActive(1) && m_scan->getDspProcessing(1));
  m_doParam2Loop = (m_scan->getLoopActive(2) && m_scan->getDspProcessing(2));
  m_nParameterSteps = 1;
  m_parameterMin = 0;
  m_parameterMax = 1;
  m_nParameter1Steps = 1;
  m_parameter1Min = 0;
  m_parameter1Max = 1;
  m_nParameter2Steps = 1;
  m_parameter2Min = 0;
  m_parameter2Max = 1;
  if (m_doParamLoop) {
    m_nParameterSteps = m_scan->getLoopVarNSteps(0);
    m_parameterMin = m_scan->getLoopVarMin(0);
    m_parameterMax = m_scan->getLoopVarMax(0);
  }
  if (m_doParam1Loop) {
    m_nParameter1Steps = m_scan->getLoopVarNSteps(1);
    m_parameter1Min = m_scan->getLoopVarMin(1);
    m_parameter1Max = m_scan->getLoopVarMax(1);
  }
  if (m_doParam2Loop) {
    m_nParameter2Steps = m_scan->getLoopVarNSteps(2);
    m_parameter2Min = m_scan->getLoopVarMin(2);
    m_parameter2Max = m_scan->getLoopVarMax(2);
  }
  m_scanParam = m_scan->getLoopParam(0);
  m_scanParam1 = m_scan->getLoopParam(1);
  m_scanParam2 = m_scan->getLoopParam(2);

  // system parameters
  m_restoreModConfig = m_scan->getRestoreModuleConfig();

  // sleep longer if not doing a DC because you'll get more data per frame
  if (m_noiseScan) m_sleepDelay = m_scan->getSuperGroupTrigDelay(); // this is cheating; to-do, add a new variable?

  
  // PF: This is needed to wait until the trigger is back before sending a new one. Lower values cause occupancy losses. 
  // Possible to optimise?
  m_sleepDelay = 1800; // us

  printOutScanParams();

#endif
}

#ifdef __XMK__
bool ScanL12::setupMaskSteps(PixLib::EnumMaskSteps::MaskStageSteps requestedMaskSteps, uint32_t &maskSteps, uint8_t &modeDC) { 
  bool loopDC = false;
  modeDC = 0; // default DC mode is 0 (specific DC), TBi: is theis even implemented in Fei3?
  switch(requestedMaskSteps) {
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_32: //This is the tested and default case
      loopDC = false;
      maskSteps = 32;
      break;
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_64: //Presumably this works, still need intense testing
      loopDC = false;
      maskSteps = 64;
      break;
/*  //We have to check and see if these cases are really implemented, in case they are, we also need to put them in console
    case PixLib::EnumMaskSteps::MaskStageSteps::STEPS_32_DC:
      loopDC = true;
      maskSteps = 32;
      break;
    case SCAN_STEPS_64_DC:
      loopDC = true;
      maskSteps = 32;
      break;
*/
  default :
    std::cout <<"AN UNSUPPORTED MASK STEP PROCEDURE HAS BEEN CHOSEN BY PIXSCANBASE"<<std::endl;
    std::cout <<"Check the MaskStageTotalSteps variable in PixScan"<<std::endl;
    std::cout <<"It was set to: "<<requestedMaskSteps<<", which is not supported"<<std::endl;
    std::cout <<"Will set the mask steps to SCAN_STEPS_32 "<<std::endl;
    loopDC = false;
    maskSteps = 32;
  }
  return loopDC;
}
#endif

bool ScanL12::initializeROD() {
	bool success = true;
#ifdef __XMK__
	std::cout << __PRETTY_FUNCTION__ << std::endl;
	std::cout << std::endl; std::cout << "** Initializing the ROD **" << std::endl; std::cout << std::endl;
	// to-do: move where approprite!!!
#define RRIF_CTL_PPC_TRIG 0x80041425
#define RRIF_CTL_INITIAL  0x80000000

	// initialization of master ctrl reg
	IblRod::Master::RrifCtrlReg::write(RRIF_CTL_INITIAL);

	if (m_dspScanType == SetDSPScan::NONE) {

		// slave Ids
		IblRod::SlaveA::SlaveId::write(0);
		IblRod::SlaveB::SlaveId::write(1);
		m_useSlave0 = false;
		m_useSlave1 = false;

		// configure slaves and histogramming if using
		if ((m_channelsConsole & 0x0000FFFF) > 0) {
			m_useSlave0 = true;
			bool successS0 =  m_slave.setId(0);
			if (!successS0) status.errorState = SLAVE_NOT_RESPONDING;
			else successS0 = configureHistogrammer(0, m_histCfgOut0, m_netCfgOut0);
			success &= successS0;
		}
		if ((m_channelsConsole & 0xFFFF0000) > 0) {
			m_useSlave1 = true;
			bool successS1 = m_slave.setId(1);
			if (!successS1) status.errorState = SLAVE_NOT_RESPONDING;
			else successS1 = configureHistogrammer(1, m_histCfgOut1, m_netCfgOut1);
			success &=successS1;
		}

#ifndef __HISTO_TEST__

  // enable link between BOC and ROD
  m_slave.enableRxLink(m_channels);

		// enable calibration mode
		IblRod::Master::RrifCtrlReg::write( RRIF_CTL_PPC_TRIG);
		IblRod::Master::SprCtrlReg::write(0x0FF);
		IblRod::Master::DebugModeCnfgReg::write( 0x02000000 );

		// set number of frames per trigger on ROD
		IblRod::SlaveA::TriggerCountNumber::write(m_frameCount);
		IblRod::SlaveB::TriggerCountNumber::write(m_frameCount);

		// set time out for channels with no data
		IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0x9C40); //  20,000 bunch crossings
		IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0x9C40);
// 		IblRod::SlaveA::FmtReadoutTimeoutLimit::write(0x4E20); //  10,000 bunch crossings
// 		IblRod::SlaveB::FmtReadoutTimeoutLimit::write(0x4E20);

		// set limit of number of header + hit + trailer per module per trigger (how many data-records to process per frame) to 1680
		IblRod::SlaveA::FmtHeaderTrailerLimit::write(0x690);
		IblRod::SlaveB::FmtHeaderTrailerLimit::write(0x690);

		// set limit of number of trailer per module per trigger
		IblRod::SlaveA::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);
		IblRod::SlaveB::FmtTrailerTimeoutLimit::write(0xFFFFFFFF);

		// set limit of how full the fifo is before asserting busy to 1680
		IblRod::SlaveA::FmtRodBusyLimit::write(0x690);
		IblRod::SlaveB::FmtRodBusyLimit::write(0x690);

		// set the maximum number of hits to be processed
		IblRod::SlaveA::FmtDataOverflowLimit::write(0x7FF);
		IblRod::SlaveB::FmtDataOverflowLimit::write(0x7FF);

		if (ScanBoss::readBackInmem) {
			fifo.selectGathererFromRxCh(ScanBoss::rxCh);
			fifo.setRxCh(ScanBoss::rxCh);
			fifo.reset();
		}

		// put slaves into calibration mode (1 = don't send fragments over the slink "calibration mode")
		IblRod::SlaveA::CalibrationMode::write(0x1);
		IblRod::SlaveB::CalibrationMode::write(0x1);
// 		IblRod::SlaveA::CalibrationMode::write(0x0); //  Use for some low-level chipscope debugging only
// 		IblRod::SlaveB::CalibrationMode::write(0x0);

		DataTakingTools::enableFmtLink(m_channels);

#endif // no __HISTO_TEST__
	}
#endif // __XMK__
	return success;
}

#ifdef __XMK__
bool ScanL12::configureHistogrammer(uint8_t slaveID, IblSlvHistCfg & histCfgOut, IblSlvNetCfg & netCfgOut) {
	std::cout << "** Configuring the histogrammers and connecting to the fit server** " << std::endl; std::cout << std::endl;

	IblRodSlave::getDefaultNetConfig(slaveID, netCfgOut);

	uint32_t tPA = slaveID ? m_fitPorts[2] : m_fitPorts[0];
	uint32_t tPB = slaveID ? m_fitPorts[3] : m_fitPorts[1];

	std::cout << std::dec << "Fit farm target port A, B = " << (int)tPA << ", " << (int)tPB << std::endl;

	netCfgOut.targetPortA = htonl(tPA);
	netCfgOut.targetPortB = htonl(tPB);

	char buffer[100];

	// fit server target ip from scan config
	if (slaveID) {
		IblRodSlave::createIp4Address(m_fitIps[2][0], m_fitIps[2][1], m_fitIps[2][2], m_fitIps[2][3], &netCfgOut.targetIpA);
		IblRodSlave::createIp4Address(m_fitIps[3][0], m_fitIps[3][1], m_fitIps[3][2], m_fitIps[3][3], &netCfgOut.targetIpB);
		snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", m_fitIps[2][0], m_fitIps[2][1], m_fitIps[2][2], m_fitIps[2][3]); std::cout << buffer << std::endl;
		snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", m_fitIps[3][0], m_fitIps[3][1], m_fitIps[3][2], m_fitIps[3][3]); std::cout << buffer << std::endl;
	} else {
		IblRodSlave::createIp4Address(m_fitIps[0][0], m_fitIps[0][1], m_fitIps[0][2], m_fitIps[0][3], &netCfgOut.targetIpA);
		IblRodSlave::createIp4Address(m_fitIps[1][0], m_fitIps[1][1], m_fitIps[1][2], m_fitIps[1][3], &netCfgOut.targetIpB);
		snprintf(buffer, 100, "Target IP H0: %d.%d.%d.%d\n", m_fitIps[0][0], m_fitIps[0][1], m_fitIps[0][2], m_fitIps[0][3]); std::cout << buffer << std::endl;
		snprintf(buffer, 100, "Target IP H1: %d.%d.%d.%d\n", m_fitIps[1][0], m_fitIps[1][1], m_fitIps[1][2], m_fitIps[1][3]); std::cout << buffer << std::endl;
	}


	bool success = true;
	success = m_slave.setNetCfg(&netCfgOut, slaveID);
	if (success) std::cout << "** Connected to the fit server ** " << std::endl;

	if (!success) {
		status.errorState = NO_CONN_TO_FITSERVER;
		std::cout << "***ERROR***" << std::endl;
		std::cout << "ERROR: NO CONNECTION TO FIT SERVER; CONTINUTING ANYWAY" << std::endl;
	}

	for (int i = 0; i < 2; i++) {
		histCfgOut.cfg[i].nChips = 128;
		histCfgOut.cfg[i].enable = 0;
		m_setToTHistoMode ? histCfgOut.cfg[i].type = LONG_TOT : histCfgOut.cfg[i].type = ONLINE_OCCUPANCY;
		if (m_maskSteps == 1) histCfgOut.cfg[i].maskStep = 0;
		else if (m_maskSteps == 2) histCfgOut.cfg[i].maskStep = 1;
		else if (m_maskSteps == 4) histCfgOut.cfg[i].maskStep = 2;
		else if (m_maskSteps == 8) histCfgOut.cfg[i].maskStep = 3;
		else if (m_maskSteps == 16) histCfgOut.cfg[i].maskStep = 4;
		else if (m_maskSteps == 32) histCfgOut.cfg[i].maskStep = 5;
		else if (m_maskSteps == 64) histCfgOut.cfg[i].maskStep = 6; //  This is/must be reset to 5 by slave SW until FW can handle 64 mask steps.
		else histCfgOut.cfg[i].maskStep = 0; // for now, all mask steps other than 2,4 and 8 use the full histogrammer space

		// always doing multi-chip case now so that Rx channels don't get messed up in the FitServer
		histCfgOut.cfg[i].chipSel = 1;
		histCfgOut.cfg[i].addrRange = 1; // tmp lj: or 0?
		histCfgOut.cfg[i].scanId = m_scanId;
		histCfgOut.cfg[i].binId = 0;
		histCfgOut.cfg[i].expectedOccValue = m_nTriggers;
		histCfgOut.cfg[i].mStepEven = 0; // default disable; will set in start histo function
		histCfgOut.cfg[i].mStepOdd = 0; // default disable; will set in start histo function
	}
	if (slaveID == 0) {
		if ((m_channelsConsole & 0x000000FF) > 0) histCfgOut.cfg[0].enable = 1;
		if ((m_channelsConsole & 0x0000FF00) > 0) histCfgOut.cfg[1].enable = 1;
	}
	else if (slaveID == 1) {
		if ((m_channelsConsole & 0x00FF0000) > 0) histCfgOut.cfg[0].enable = 1;
		if ((m_channelsConsole & 0xFF000000) > 0) histCfgOut.cfg[1].enable = 1;
	}
        return success;
}
#endif

void ScanL12::initializeFrontEnd() {
#ifdef __XMK__
  std::cout<<"** Initializing the front ends **"<<std::endl; std::cout<<std::endl;

  // disable Rx before enabling
  m_iblBocLink = RodResourceManagerPIX::getIblBocLink(); 
  m_iblBocLink->disableRxChannels(0xFFFFFFFF); 

  m_channelsConsole = m_channels;

  //Enabling TX 
#ifndef __HISTO_TEST__
  std::cout<<"Enabling Tx Monitoringcounters "<<std::endl;
  uint32_t maskTx = BarrelConnectivity::getTxMaskFromRxMask(m_channels);
  for(uint8_t txCh=0;txCh<32;txCh++)
    if (maskTx & (1<<txCh)){
    IblBoc::DisableTxMonitoring(txCh/16, txCh%16);
    IblBoc::EnableTxMonitoring(txCh/16, txCh%16);
  }
#endif

  // The serial port shouldn't be altered here ... Todo: fix me.
  // Warning: all modules are not accessible from the serial port until a new BOC FW.
  uint32_t modMask = BarrelConnectivity::getModMaskFromRxMask(m_channels);
  uint32_t txMask = BarrelConnectivity::getTxMaskFromRxMask(m_channels);
  std::cout << "modMask = " << HEX(modMask) << ", txMask = " << HEX(txMask) << std::endl;
  RodResourceManagerPIX::getSerialPort()->setTxMask(txMask);
  m_fei3Sport.setTxLink(RodResourceManagerPIX::getSerialPort());
  m_fei3Sport.set5MHzMode(false);//Needed?
  m_fei3Mod = Fei3ModProxy(modMask);


  //In order to restore the global registers after a scan it is cached in the
  //m_globalConfigStore. In order to save memory, only the Fei3GlobalCfg are stored
  //TBi TODO: move to std::array and new GlobalRegister enums in future version
  m_globalConfigStore.clear();
  m_MCCConfigStore.clear();
  uint32_t nMod = RodResourceManagerPIX::getNModuleConfigs();
  for (size_t modIx = 0; modIx < nMod; ++modIx){
      m_globalConfigStore.push_back( std::vector<Fei3GlobalCfg>() );
      Fei3Mod & modCfg = RodResourceManagerPIX::getCalibModuleConfig(modIx);
      std::array<uint16_t,11> mccCfg;
      std::copy(std::begin(modCfg.mccRegisters), std::end(modCfg.mccRegisters), std::begin(mccCfg));
      m_MCCConfigStore.push_back( mccCfg);
      for(size_t feIx = 0; feIx < Fei3ModCfg::nFE; ++feIx) {
          m_globalConfigStore[modIx].push_back( modCfg.m_chipCfgs[feIx] );
      }
  }

  if(m_scan->getUseEmulator())DataTakingTools::setBocEmu(true, m_channels, m_frameCount, IblBoc::PixSpeedFromMccBandwidth( m_scan->getMccBandwidth() ), m_scan->getNHitsEmu());
  else DataTakingTools::setBocEmu(false, 0xFFFFFFFF);

  m_iblBocLink->enableTxChannels(txMask);
  
  if (m_dspScanType != SetDSPScan::NONE)return;

  // Using local copy of Fei3Mod with the serial port as a TxLink
  // This is to send the same pattern to ALL modules enabled within the Proxy's mod_mask
  // WARNING - ACHTUNG - ATTENTION - The m_dummyFei3ModForBroadcast stores NO valid configuration
  // It should NOT be used for anything else but sending "common" pixel register bits
  std::cout << HEX( RodResourceManagerPIX::getSerialPort()->getTxMask() ) << std::endl;
  // Synchronizing the TxMasks between the RodSerial port and the BroadcastFeLink
  // Note: this is for reference only because only SetTxMask will actually modify the IblRod registers
  //RodResourceManagerPIX::getBroadcastFeLink()->getTxLink()->setTxMask( RodResourceManagerPIX::getSerialPort()->getTxMask() );
  //m_dummyFei3ModForBroadcast.setFeLink( RodResourceManagerPIX::getBroadcastFeLink() );
  //m_dummyFei3ModForBroadcast.set5MHzMode(false); // The ROD doesn't support 5MHz

  bool calib = true;
  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();

  if (ScanBoss::verbose) {
    for(size_t i = 0 ; i < 26 ; ++i) {
      std::cout << "Config #" << i << std::endl;
      cfgArray[i].dump();
    }
  }


  if( !m_iblBocLink->enableRxChannels(m_channels) ) {
    std::cout<<"WARNING: could not enable all the desired channels. Will continue scan anyway"<<std::endl;
    m_channels = m_iblBocLink->getEnChMask();
    std::cout<<"New (enabled) channel mask = 0x"<<std::hex<<m_channels<<std::dec<<std::endl;
    if (m_channels == 0x0) status.errorState = NO_ENABLED_CHANNELS;
    m_fei3Mod.setFeMask(m_channels); // update channel mask if not all enabled
  }

  m_fei3Mod.sendMCCRegister(CSR, 0x020);//Set half clock return to remove IO delay
  m_fei3Mod.mccGlobalResetMCC();
  m_fei3Mod.mccGlobalResetFE(4); //Soft FE reset

  // WARNING - ACHTUNG - ATTENTION -- Adjusting BOC setting (but tightly coupled to MCC operation)
  PixLib::PixScanBase::MccBandwidth mccBandwidth = m_scan->getMccBandwidth();
  // When selecting DOUBLE_40, which is not supported with the L12 readout, we fall back to DOUBLE_80
  if( mccBandwidth == PixLib::PixScanBase::DOUBLE_40 ) mccBandwidth = PixLib::PixScanBase::SINGLE_80;
  IblBoc::PixSetSpeed( mccBandwidth ); // Setting speed for BOTH BMFs
  IblBoc::PixSpeed rxSpeed = IblBoc::PixSpeedFromMccBandwidth( mccBandwidth );
  IblRodSlave::setFmtFei3Reg(rxSpeed); // Setting the speed for the ROD FMT
  m_fei3Mod.sendMCCRegister(CSR, (int16_t)(mccBandwidth) & 0x3 );//Only 2 last bits are usied for the MCC speed, others set to 0
  m_fei3Mod.sendMCCRegister(WRECD,0x0);//Disable Receiver disable 
  m_fei3Mod.sendMCCRegister(WBITD,0x0);//Disable MCC and FE warnings
  std::cout << "MCC speed: " << HEX(mccBandwidth) << "; BOC RX speed: " << HEX(rxSpeed) << std::endl;

///////////// Fei3 functionality unknown //////////////////////
//   m_fei3Mod.setRunMode(false);
//   m_fei3Mod.setVerbose(false);
//   Fei3ModData::setRawDataDump(false);
//   Fei3ModData::setVerboseSR(false); // read back Service records
///////////////////////////////////////////////////////////////

//   m_fei3Mod.setPreAmpsOn( !m_doDigitalInjection ); // Pre-amps should be OFF for DIGITAL_SCAN
//   m_fei3Mod.sendPixelConfig(true); // send only fdac and tdac pixel regs individually

  // clear pixel capicator regs.... just in case! // this seems to be necessary.... why?
///////////// Fei3 m_pixReadoutEnableMask might eventualy be replaced by m_mask /////////////
//     m_mask.set(MASK_CLEAR);
////////////////////////////////////////////////////////////////////////////////

  // Todo: make usage of masks uniform (e.g. use codeing similar to Fei4 m_mask above
  for (uint8_t k = 0; k < Fei3PixelCfg::nDC ; k++) {
    for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
      m_pixReadoutEnableMask[k][j] = 0x00000000;
      m_pixWriteSelectMask[k][j] =  m_pixReadoutEnableMask[k][j];
    }
  }

//  ????????????    for (int iDC = 0; iDC < Fei3PixelCfg::nDC; iDC++) //  ????????????
//  ????????????      m_fei3Mod.strobePixelLatchesDC_wBC(iDC, Fei3::PixLatch::Enable, m_pixReadoutEnableMask); // Enable bit //  ????????????

	// write specific registers that are set in scan parameters and should be set before starting scan
	//To-do: ensure protection for variable ranges (trig delay, trig count, trig lat...)

  m_fei3Mod.sendMCCRegister(LV1, (m_frameCount - 1) << 8);

  // Fetching scan options from the configuration
  // Todo: make sure all the options are propagated
  
  //MCC/FE tab options
  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::ReadMode,  m_ReadMode);
  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::THRMIN,    m_TotMin);
  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::THRDUB,    m_TotDHThr);
  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::EOC_MUX,   m_TotTimeStampMode);
  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::CEU_Clock, m_ColumnROFreq-1);

  m_fei3Mod.writeGlobalReg(Fei3::GlobReg::Latency, m_trigLatency);
  m_doDigitalInjection ? m_fei3Mod.writeGlobalReg(Fei3::GlobReg::EnableDigInj, 1) : m_fei3Mod.writeGlobalReg(Fei3::GlobReg::EnableDigInj, 0);

  m_fei3Mod.setCapType(m_feCapType);

  // Only write the PlsrDAC register if a value within range is set
  if (m_feVCal > 0 && m_feVCal < 1024) {
    m_fei3Mod.writeGlobalReg(Fei3::GlobReg::VCal, m_feVCal);
  }

  m_fei3Mod.CALset(m_strobeDelayRange,m_scan->getOverrideStrobeMCCDelayRange(), m_strobeDelay,m_scan->getOverrideStrobeMCCDelay());

  // Send config to the modules
  // pre-amps OFF for digital scan
  // pre-amps ON for all other scans
  // Needed because the global register has been altered
  m_fei3Mod.setAllPixelShiftRegisters(true);
  m_fei3Mod.sendGlobalConfig();

  m_fei3Mod.setPreAmpsOn( !m_doDigitalInjection ); // Pre-amps should be OFF for DIGITAL_SCAN
  m_fei3Mod.sendPixelConfig(true); // send only fdac and tdac pixel regs individually
  // m_fei3Mod.sendMCCRegister(FEEN, 0x00FF); //  For testing only !! (to be done properly with m_fei3Mod.disableFEs in subsequent commit)

  // sets width of calibration pulse
  if (m_strobeDuration > 0) // Warning: make sure that CNT is not overwritten afterwards (or move code)
     m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);  // ToCheck: -1 in IBL

// send different cal pulse widths (ToT) to debug which chip is which
	if (debugWhichChips) {
		int CalPulse = m_strobeDuration - 1;
		for (uint32_t i = 0; i < RodResourceManagerPIX::getNModuleConfigs(); i++) {
			if (m_channels & (1 << i)) {
				if (CalPulse > 0xF) CalPulse = 0xF; // protection against too large ToT
				// Todo: pick right module according to mask
				m_fei3Mod.sendMCCRegister(CNT, CalPulse);
				std::cout << "Debug ToT: about to set true ToT of " << (int)CalPulse << " in rx ch mask: 0x" << std::hex << (int)(m_channels & (1 << i)) << std::dec << std::endl;
				CalPulse++;; // FE increase in ToT
			}
		}
	}

  return;

	std::cout << __PRETTY_FUNCTION__ << " completed" << std::endl;
#endif
}

#ifdef __XMK__
uint32_t ScanL12::decodeMaskStageMode(PixLib::EnumMaskStageMode::MaskStageMode mode) {
  uint32_t regBits = 0x0;
  
//   switch(mode) {
//   case SCAN_FEI4_ENA_NOCAP :
//     regBits = regBits | 0x1;
//     break;
//   case SCAN_FEI4_ENA_SCAP :
//     regBits = regBits | 0x81;
//     break;
//   case SCAN_FEI4_ENA_LCAP :
//     regBits = regBits | 0x41;
//     break;
//   case SCAN_FEI4_ENA_BCAP :
//     regBits = regBits | 0xc1;
//     break;
//   case SCAN_FEI4_ENA_HITBUS :
//     regBits = regBits | 0x101;
//     break;
//   case SCAN_FEI4_MONLEAK :
//     regBits = regBits | 0x100; // or 0x101? Doesn't explicitly include enable...
//     break;
//   default:
//     if(!m_doParamLoop) break; //  The way code is written now, the value returned by this function is only used if m_doParamLoop == true !!!
//     std::cout <<"AN UNSUPPORTED MASK STEP MODE HAS BEEN CHOSEN BY PIXSCANBASE"<<std::endl;
//     std::cout <<"Check the MaskStageMode variable in PixScan"<<std::endl;
//     std::cout <<"It was set to: "<<(int)mode<<", which is not supported"<<std::endl;
//     std::cout <<"Will set the mask stage modes to SCAN_FEI4_ENA_NOCAP"<<std::endl;
//     std::cout <<"So you are basically getting a digital scan; no analog injection will work"<<std::endl;
//     regBits = regBits | 0x1;
//   }
  return regBits;
}
#endif

void ScanL12::printOutScanParams() {
#ifdef __XMK__
	std::cout << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << " Initialized scan with the following parameters: " << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << " Channels: 0x" << std::hex << (int)m_channels << std::dec << std::endl;
	std::cout << " Scan id: " << std::dec << (int)m_scanId << std::endl;
	std::cout << " Fit server port 0: " << std::dec << (int)m_fitPorts[0] << std::endl;
	std::cout << " DSP SCAN TYPE: " << std::dec << (int)m_dspScanType << std::endl;
	std::cout << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << " mask step options: " << std::endl;
	std::cout << std::dec << std::endl;
	std::cout << " loop over DC (true or false) = " << (int)m_loopDC << std::endl;
	std::cout << " number of DC loops: " << (int)m_nDCLoops << std::endl;
	std::cout << " dc mode = " << (int)m_modeDC << std::endl;
	std::cout << " total mask steps: " << (int)m_maskSteps << std::endl;
	std::cout << " mask steps to do out of total: " << (int)m_maskStepsToDo << std::endl;
	std::cout << " maskStageMode: " << (int)m_maskStageMode << std::endl;
// 	std::cout << " value to write to pixel latch strobe general reg: " << (int)m_pixelLatchStrobeValue << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << " trigger options: " << std::endl;
	std::cout << std::dec << std::endl;
	std::cout << " # ntriggers: " << (int)m_nTriggers << std::endl;
	std::cout << " # frames per trigger: " << (int)m_frameCount << std::endl;
	std::cout << " trigger delay between pulse and trigger: " << (int)m_trigDelay << std::endl;
	std::cout << " override trigger delay from config? " << (int)m_trigDelayOverride << std::endl;
	std::cout << " trigger latency: " << (int)m_trigLatency << std::endl;
	std::cout << " noise scan? = no pulse, trigger only: " << (bool)m_noiseScan << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << " pulser options: " << std::endl;
	std::cout << std::dec << std::endl;
	std::cout << " strobe width: " << (int)m_strobeDuration << std::endl;
	std::cout << " strobe delay: " << (int)m_strobeDelay << std::endl;
	std::cout << " strobe delay override: " << m_scan->getOverrideStrobeMCCDelay() << std::endl;
	std::cout << " strobe delay range : " << (int)m_strobeDelayRange << std::endl;
	std::cout << " strobe delay range override: " << m_scan->getOverrideStrobeMCCDelayRange() << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << " FE/MMC options: "<< std::endl;
	std::cout << " Read Mode: " << (int) m_ReadMode << std::endl;
	std::cout << " TotMin: " << (int) m_TotMin << std::endl;
	std::cout << " Tot Doubling hit Threshold: " << (int) m_TotDHThr << std::endl;
	std::cout << " Tot TimeStamp Mode;" << (int) m_TotTimeStampMode << std::endl;
	std::cout << " Column Readout Freq (-1):" << (int) m_ColumnROFreq-1 << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;                    
	
	std::cout << " scan parameter 0 options: " << std::endl;
	std::cout << std::endl;
	std::cout << " do parameter loop 0 scan? : " << (int)m_doParamLoop << std::endl;
	std::cout << " # parameter steps: " << (int)m_nParameterSteps << std::endl;
	std::cout << " parameter min: " << (int)m_parameterMin << std::endl;
	std::cout << " parameter max: " << (int)m_parameterMax << std::endl;
	std::cout << " parameter over which to scan (enum): " << (int)m_scanParam << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
        std::cout << "+  !! The following only used by Opto Scan now !!  +" << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << " scan parameter 1 options: " << std::endl;
	std::cout << std::endl;
	std::cout << " do parameter loop 1 scan? : " << (int)m_doParam1Loop << std::endl;
	std::cout << " # parameter steps: " << (int)m_nParameter1Steps << std::endl;
	std::cout << " parameter min: " << (int)m_parameter1Min << std::endl;
	std::cout << " parameter max: " << (int)m_parameter1Max << std::endl;
	std::cout << " parameter over which to scan (enum): " << (int)m_scanParam1 << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
        std::cout << "+  !! The following not used on ROD at all now !!  +" << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << " scan parameter 2 options: " << std::endl;
	std::cout << std::endl;
	std::cout << " do parameter loop 2 scan? : " << (int)m_doParam2Loop << std::endl;
	std::cout << " # parameter steps: " << (int)m_nParameter2Steps << std::endl;
	std::cout << " parameter min: " << (int)m_parameter2Min << std::endl;
	std::cout << " parameter max: " << (int)m_parameter2Max << std::endl;
	std::cout << " parameter over which to scan (enum): " << (int)m_scanParam2 << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << " general scan params: " << std::endl;
	std::cout << std::endl;
	std::cout << " do digital injection? " << (int)m_doDigitalInjection << std::endl;
	std::cout << " restore module config to original ? " << (int)m_restoreModConfig << std::endl;
	std::cout << " use BOC emulator ? " << m_scan->getUseEmulator() << std::endl;
	std::cout << " height of analog injection: " << (int)m_feVCal << std::endl;
	std::cout << " capacitor type (0 = low, 1 = high): " << (int)m_feCapType << std::endl;
	std::cout << " set histogrammer to LONG_TOT mode? " << (int)m_setToTHistoMode << std::endl;
	std::cout << " sleep after trigger = " << (int)m_sleepDelay << " (us)" << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;

	std::cout << " debug params: " << std::endl;
	std::cout << " verbose? : " << (int)ScanBoss::verbose << std::endl;
	std::cout << " debug read back from boc fifo onto kermit? : " << (int)ScanBoss::readBackBoc << std::endl;

	std::cout << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << "++++++++++++++++++++++++++++++++++++++++++++++++++++" << std::endl;
	std::cout << std::endl;
#endif
}

////////////////////////////////////////////
// Running
////////////////////////////////////////////

void ScanL12::setupMaskIteration(int32_t iMask, bool clear) {
  if(!clear) std::cout << "++ Starting mask stage loop: " << (int)iMask + 1 << " out of "
                       << (int)m_maskStepsToDo << " total loops" << std::endl;

#ifdef __XMK__
  if (ScanBoss::verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

  if(!clear) {
  //Define or shift pixel mask
///////////// Fei3 m_pixReadoutEnableMask might eventualy be replaced by m_mask /////////////
//   if (iMask == 0) {
//     if (m_maskSteps ==1) m_mask.set(MASK_FULL);
//     else if (m_maskSteps == 2) m_mask.set(MASK_HALF);
//     else if (m_maskSteps == 4) m_mask.set(MASK_QUARTER);
//     else if (m_maskSteps == 8) m_mask.set(MASK_EIGHTH);
//     else if (m_maskSteps == 16) m_mask.set(MASK_16);
//     else if (m_maskSteps == 32) m_mask.set(MASK_32);
//     else if (m_maskSteps == 672) m_mask.set(MASK_672);
//   }
//   else m_mask.shift();
////////////////////////////////////////////////////////////////////////////////

  // Todo: make usage of masks uniform (e.g. use codeing similar to Fei4 m_mask above
    if(m_loopDC) {
      if (iMask%32 == 0) {
        // Hard-coded for MASK_32 !!! // Todo: fix me
        for (unsigned int j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
          if((iMask == 0 && j%2 == 0) || m_maskSteps < 33)m_pixReadoutEnableMask[0][j] = 0x00000001;
          if (iMask == 32) {
            for (unsigned int k = 0; k < Fei3PixelCfg::nDC ; k++) {
              m_pixReadoutEnableMask[k][j] = 0x00000000;
              m_pixWriteSelectMask[k][j]=m_pixReadoutEnableMask[k][j];
            }
            if(j%2 == 1) {
              m_pixReadoutEnableMask[0][j] = 0x00000001;
              m_pixWriteSelectMask[0][j]=m_pixReadoutEnableMask[0][j];
            }
          }
        }

      } else { //  Put the mask of the last DC (shifted by one) into the first, then clear out the former.
          for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
            m_pixReadoutEnableMask[0][j] = m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j] << 1;
            m_pixWriteSelectMask[0][j] = m_pixReadoutEnableMask[0][j];
            m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j] = 0x00000000;
            m_pixWriteSelectMask[Fei3PixelCfg::nDC - 1][j] = m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j];
          }
      }
    } else {
      if (iMask == 0) {
        // Hard-coded for MASK_32 !!! // Todo: fix me
        for (unsigned int k = 0; k < Fei3PixelCfg::nDC ; k++) {
          for (unsigned int j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
            if(j%2 == 0 || m_maskSteps != 64) {
              m_pixReadoutEnableMask[k][j] = 0x00000001;
              m_pixWriteSelectMask[k][j]=m_pixReadoutEnableMask[k][j];
              if(m_maskStageMode == PixLib::EnumMaskStageMode::XTALK)m_pixWriteSelectMask[k][j] = 0x80000002;
            }
          }
        }
        m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Select, m_pixWriteSelectMask);
        m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Enable, m_pixReadoutEnableMask);
         if(m_maskStageMode == PixLib::EnumMaskStageMode::XTALK)m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Preamp, m_pixReadoutEnableMask);//Kill all preamp except the ones that are readout
        // Needs to be reset because strobing the pixel SR sets CNT to other values
        m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);
      } else if (m_maskStageMode == PixLib::EnumMaskStageMode::XTALK){
        
          int s=31;
          for (uint8_t k = 0; k < Fei3PixelCfg::nDC ; k++) {
            for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++){
             //if(j% != 0)continue;
             m_pixWriteSelectMask[k][j] = (m_pixWriteSelectMask[k][j]<<1) | (m_pixWriteSelectMask[k][j]>>( (-1)& s ));
             m_pixReadoutEnableMask[k][j] <<=1;
            	  if(k==0&&j==0)std::cout<<std::hex<<"Write enable 0x"<<m_pixReadoutEnableMask[k][j]<<" Write Select 0x"<<m_pixWriteSelectMask[k][j]<<std::dec<<std::endl;
             }
           }                                                                                            //  scan, injects one row
             m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Select, m_pixWriteSelectMask);
             m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Enable, m_pixReadoutEnableMask);//XTALK scan only enable preamp in all the modules belonging to the readout enable mask
             m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Preamp, m_pixReadoutEnableMask);
             // Needs to be reset because strobing the pixel SR sets CNT to other values
             m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);
        
        } else {
          // During a scan typically one would want to have regMask = WriteMask | WriteSelect
          m_fei3Mod.moveMask(16, WriteMask | WriteSelect);
          
          // Needs to be reset because moveMask sets CNT to other values
          m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);

          // This mode cannot possibly work because the ROD FIFO doesn't support 5MHz operation
          // Todo: update ROD FW to have the 5MHz feature OR to have a large enough FIFO to use the SW 5MHz mode
          // m_dummyFei3ModForBroadcast.strobePixelLatches(16, Fei3::PixLatch::Select, Fei3PixelCfg::nDC, m_pixWriteSelectMask);
          // m_dummyFei3ModForBroadcast.strobePixelLatches(16, Fei3::PixLatch::Enable, Fei3PixelCfg::nDC, m_pixReadoutEnableMask);
      } // iMask != 0
    } // !m_loopDC

  // if done with scan, clear the loaded pixel latches
///////////// Fei3 m_pixReadoutEnableMask might eventualy be replaced by m_mask /////////////
//   if (clear) m_mask.set(MASK_CLEAR);
/////////////////////////////////////////////////////////////////////////////////

  // Todo: make usage of masks uniform (e.g. use codeing similar to Fei4 m_mask above
  } else {
    for (uint8_t k = 0; k < Fei3PixelCfg::nDC ; k++) {
      for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
        m_pixReadoutEnableMask[k][j] = 0x00000000;
        m_pixWriteSelectMask[k][j]=m_pixReadoutEnableMask[k][j];
      }
    }
  }

  // Ensure to write 1 DC at a time
  // ToCheck

  // Todo: use proxy instead of m_fei3Mod

  // Enable the necessary pixels
//  ????????????  for (int iDC = 0; iDC < Fei3PixelCfg::nDC; iDC++) {
//  ????????????    std::cout << iDC << ": -- strobePixelLatchesDC" << std::endl;
//  ????????????    // Todo: put necessary if statements when necessary (e.g. cross-talk scan)
//  ????????????    m_fei3Mod.strobePixelLatchesDC_wBC(Fei3::PixLatch::Enable, m_pixReadoutEnableMask); // Enable bit (active enables pixel)
//  ????????????    // Using NULL to use what is already strobed in the shift register
//  ????????????    m_fei3Mod.strobePixelLatchesDC_wBC(Fei3::PixLatch::Select, NULL); // Select bit (active enables injection)
//  ????????????    m_fei3Mod.strobePixelLatchesDC_wBC(Fei3::PixLatch::Preamp, NULL); // Preamp (Kill) bit (active enables the pre-amplifier)
//  ????????????  }

#if 0 // Todo: implement this Fei4 code for Fei3 (using m_pixReadoutEnableMask instead of m_mask?)
  for (int i = 0; i < RodResourceManagerPIX::getNModuleConfigs(); i++) {
    if (m_channels & (1<<i)) {
      Fei3Mod &f = RodResourceManagerPIX::getCalibModuleConfig(i);
      if (f.scanDisable.size() != 0) {
        Mask disable = m_mask;
        for (int iDisable = 0; iDisable < f.scanDisable.size(); iDisable++) { // don't enable pixels which we explicitly disable
          //	  std::cout<<"for fe ch: "<<(int)i<<" have DC: "<<(int)f.scanDisable[iDisable].first<<" and pixel: "<< (int)f.scanDisable[iDisable].second<<" to disable"<<std::endl;
          disable.setPixel(f.scanDisable[iDisable].second, 0);
          f.strobePixelLatches(disable, m_pixelLatchStrobeValue, f.scanDisable[iDisable].first);
        }
      }
    }
  }
#endif

	// WARNING: setting the calibration pulse length is done using the CNT register which is also used for other purposes.
	// One should ensure that the CNT register is not overwritten after this and until the trigger is sent

	// increase ToT if debugging which mask step
	if (debugWhichMaskStep && !clear) {
		if (m_strobeDuration > 0)
			m_fei3Mod.sendMCCRegister(CNT, m_strobeDuration); // ToCheck: -1 in IBL

		// send different cal pulse widths (ToT) to debug which chip is which
		if (debugWhichChips) {
			int CalPulse = m_strobeDuration - 1;
			for (uint32_t i = 0; i < RodResourceManagerPIX::getNModuleConfigs(); i++) {
				if (m_channels & (1 << i)) {
					if (CalPulse > 0xF) CalPulse = 0xF; // protection against too large ToT
					// Todo: pick right module according to mask
					m_fei3Mod.sendMCCRegister(CNT, CalPulse);
					std::cout << "Debug ToT: about to set true ToT of " << (int)CalPulse << " in rx ch mask: 0x" << std::hex << (int)(m_channels & (1 << i)) << std::dec << std::endl;
					CalPulse++;; // FE increase in ToT
				}
			}
		}
		m_strobeDuration++;
	}

///////////// Fails with MCCTestScan run //////////////////////
//   // re-sample the clock phase
//
// //  if (iMask == 0) {
//     for (int slaveId = 0; slaveId < 2; slaveId++) {
//       for (int ch = 0; ch < RodResourceManagerPIX::getNModuleConfigs(); ch++) {
// 	IblBoc::write(slaveId,ch, IblBoc::ExtraControl, 0x20);
// 	IblBoc::write(slaveId,ch, IblBoc::ExtraControl, 0x00);
//       }
//     }
//     usleep(100);
//  }
///////////////////////////////////////////////////////////////

	if(!clear) std::cout << "done with setupmaskiteration" << std::endl;
#endif
}

bool ScanL12::startHistogrammer(int32_t iPar, int32_t iMask) {
	std::cout << __PRETTY_FUNCTION__ << ": iPar=" << iPar << ", iMask=" << iMask << std::endl;

        bool success = true;
#ifdef __XMK__
	//  std::cout<<"about to start histgorammer "<<std::endl;
	bool doHistoMS = true; // do mask stepping in histogrammer ?

	// only start histogrammer each mask step for those mask steps supported by histogrammer
	// otherwise only start histogrammer at beginning of scan
	if (!(m_maskSteps == 32 || m_maskSteps == 64)) {
		doHistoMS = false;
		if (!iMask == 0) return success;
	}


  uint32_t iMaskforslaves = iMask%32; //  Slaves only handle 32 mask steps
  uint32_t maskStepsforslaves = (m_maskSteps - 1)%32 + 1;
  if (m_useSlave0) {
    for (int i = 0; i < 2; i++) {
      m_histCfgOut0.cfg[i].binId = iPar;
      m_histCfgOut0.cfg[i].maskId = iMask; //  Not actually needed in slave other than returning the value to FitServer as consistency check.
#ifdef __HISTO_FILTERING__
      if (doHistoMS) { m_histCfgOut0.cfg[i].mStepOdd = iMaskforslaves+1;
                       m_histCfgOut0.cfg[i].mStepEven = maskStepsforslaves-iMaskforslaves; } //set mask step position (different for even and odd columns)
#endif
      if (ScanBoss::verbose) {
        std::cout << std::endl << "Histgramming unit " << i << " of slave 0 to be configured as follows:" << std::endl << std::endl;
        std::cout << "  nChips =           " << m_histCfgOut0.cfg[i].nChips << std::endl;
        std::cout << "  enable =           " << m_histCfgOut0.cfg[i].enable << std::endl;
        std::cout << "  type =             " << m_histCfgOut0.cfg[i].type << std::endl;
        std::cout << "  maskStep =         " << m_histCfgOut0.cfg[i].maskStep << std::endl;
        std::cout << "  mStepEven =        " << m_histCfgOut0.cfg[i].mStepEven << std::endl;
        std::cout << "  mStepOdd =         " << m_histCfgOut0.cfg[i].mStepOdd << std::endl;
        std::cout << "  chipSel =          " << m_histCfgOut0.cfg[i].chipSel << std::endl;
        std::cout << "  expectedOccValue = " << m_histCfgOut0.cfg[i].expectedOccValue << std::endl;
        std::cout << "  addrRange =        " << m_histCfgOut0.cfg[i].addrRange << std::endl;
        std::cout << "  scanId =           " << m_histCfgOut0.cfg[i].scanId << std::endl;
        std::cout << "  binId =            " << m_histCfgOut0.cfg[i].binId << std::endl;
        std::cout << "  maskId =           " << m_histCfgOut0.cfg[i].maskId << std::endl;
        std::cout << std::endl;
      }
    }
#ifndef __HISTO_TEST__
		if (!m_slave.startHisto(&m_histCfgOut0, 0)) {
			status.errorState = SLAVE_NOT_RESPONDING;
			std::cout << " Slave 0 did not respond to start histo command" << std::endl;
			success=false;
		}
		if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer 0" << std::endl;
#endif // no __HISTO_TEST__
  }
  if (m_useSlave1)  {
    for (int i = 0; i < 2; i++) {
      m_histCfgOut1.cfg[i].binId = iPar;
      m_histCfgOut1.cfg[i].maskId = iMask; //  Not actually needed in slave other than returning the value to FitServer as consistency check.
#ifdef __HISTO_FILTERING__
      if (doHistoMS) { m_histCfgOut1.cfg[i].mStepOdd = iMaskforslaves+1;
                       m_histCfgOut1.cfg[i].mStepEven = maskStepsforslaves-iMaskforslaves; } //set mask step position (different for even and odd columns)
#endif
      if (ScanBoss::verbose) {
        std::cout << std::endl << "Histgramming unit " << i << " of slave 1 to be configured as follows:" << std::endl << std::endl;
        std::cout << "  nChips =           " << m_histCfgOut1.cfg[i].nChips << std::endl;
        std::cout << "  enable =           " << m_histCfgOut1.cfg[i].enable << std::endl;
        std::cout << "  type =             " << m_histCfgOut1.cfg[i].type << std::endl;
        std::cout << "  maskStep =         " << m_histCfgOut1.cfg[i].maskStep << std::endl;
        std::cout << "  mStepEven =        " << m_histCfgOut1.cfg[i].mStepEven << std::endl;
        std::cout << "  mStepOdd =         " << m_histCfgOut1.cfg[i].mStepOdd << std::endl;
        std::cout << "  chipSel =          " << m_histCfgOut1.cfg[i].chipSel << std::endl;
        std::cout << "  expectedOccValue = " << m_histCfgOut1.cfg[i].expectedOccValue << std::endl;
        std::cout << "  addrRange =        " << m_histCfgOut1.cfg[i].addrRange << std::endl;
        std::cout << "  scanId =           " << m_histCfgOut1.cfg[i].scanId << std::endl;
        std::cout << "  binId =            " << m_histCfgOut1.cfg[i].binId << std::endl;
        std::cout << "  maskId =           " << m_histCfgOut1.cfg[i].maskId << std::endl;
        std::cout << std::endl;
      }
    }
#ifndef __HISTO_TEST__
		if (!m_slave.startHisto(&m_histCfgOut1, 1)) {
			status.errorState = SLAVE_NOT_RESPONDING;
			std::cout << " Slave 1 did not respond to start histo command" << std::endl;
			success = false;
		}
		if (ScanBoss::verbose && success)  std::cout << "----started Histogrammer 1" << std::endl;
#endif // no __HISTO_TEST__
	}
	//  std::cout<<"done with start histgorammer "<<std::endl;
#endif
	return success;
}

///////////// Fei3 functionality unknown //////////////////////
void ScanL12::setupDoubleColumnIteration(int8_t iDC) {
#ifdef __XMK__
  //  std::cout<<"about to setupDC "<<std::endl;

  if(m_loopDC) {
    if(iDC > 0) { //  Move to next DC
      for(unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
        m_pixReadoutEnableMask[iDC][j] = m_pixReadoutEnableMask[iDC - 1][j];
        m_pixReadoutEnableMask[iDC - 1][j] = 0;
        m_pixWriteSelectMask[iDC][j]=m_pixReadoutEnableMask[iDC][j];
        m_pixWriteSelectMask[iDC - 1][j]=m_pixReadoutEnableMask[iDC - 1][j];
      }
    }
    m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Select, m_pixWriteSelectMask);
    m_fei3Mod.strobePixelLatches(16, Fei3::PixLatch::Enable, m_pixReadoutEnableMask);
    // Needs to be reset because strobing the pixel SR sets CNT to other values
    m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);
  } // m_loopDC

  // Using the proxy again (could keep using m_dummyFei3ModForBroadcast and it would be the same
  // Needs to be reset because strobing the pixel SR sets CNT to other values
  if(FEEN_Mask != 0xFFFF) { //  Regular operation (i.e. nFEgroup = 1) will never go here
    std::cout << "Filtering  MCC Register FEEN with " << HEX(FEEN_Mask) << std::endl;
    m_fei3Mod.disableFEs(FEEN_Mask);
  }
  m_fei3Mod.mccEnDataTake();
  //  std::cout<<"done setting up DC "<<std::endl;
#endif
}
///////////////////////////////////////////////////////////////

void ScanL12::setupParameterIteration(int32_t iPar) {
#ifdef __XMK__
	if (!m_doParamLoop) return;

	std::cout << "++++ Starting parameter loop: " << std::dec <<
	          (int)iPar + 1 << " out of " << (int)m_nParameterSteps << " total loops" << std::endl;

  if(iPar > 0) { //  Put the mask of the last DC (without shifting it) into the first, then clear out the former.
    for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
      m_pixReadoutEnableMask[0][j] = m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j];
      m_pixWriteSelectMask[0][j] = m_pixReadoutEnableMask[0][j];
      m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j] = 0x00000000;
      m_pixWriteSelectMask[Fei3PixelCfg::nDC - 1][j] = m_pixReadoutEnableMask[Fei3PixelCfg::nDC - 1][j];
    }
  }
  
  if (ScanBoss::verbose) std::cout<<"in function: "<<__PRETTY_FUNCTION__<<std::endl;

	uint32_t paramValue;
	float paramValueFloat;
	if(m_nParameterSteps > 1)paramValue = (uint32_t)(m_parameterMin + ((m_parameterMax - m_parameterMin) / (m_nParameterSteps - 1)) * iPar);
	else paramValue = (uint32_t)m_parameterMin;
	if(m_nParameterSteps > 1)paramValueFloat = (m_parameterMin + ((m_parameterMax - m_parameterMin) / (m_nParameterSteps - 1)) * (float)iPar);
	else paramValueFloat = m_parameterMin;

///////////// Fei3 functionality unknown //////////////////////
  if (ScanBoss::verbose) std::cout<<std::dec<<"setting parameter value to: "<<(int)paramValue<<std::endl;

  switch (m_scanParam) {
  case PixLib::PixScanBase::VCAL :
    if (ScanBoss::verbose) std::cout << "Going to do a VCAL scan at Vcal value: = "<< paramValue <<" with capicator type = "
                                     << (m_feCapType ? "High" : "Low") << std::endl;
    m_fei3Mod.setCapType(m_feCapType);
    m_fei3Mod.writeGlobalReg(Fei3::GlobReg::VCal, paramValue);
    break;
  case PixLib::PixScanBase::LATENCY :
    m_fei3Mod.writeGlobalReg(Fei3::GlobReg::Latency, paramValue);
    break;
  case PixLib::PixScanBase::TRIGGER_DELAY :
    m_trigDelay = paramValue;
    break;
  case PixLib::PixScanBase::STROBE_DELAY :
      m_fei3Mod.CALset(m_strobeDelayRange,false, paramValue,true);
    break;
  case PixLib::PixScanBase::CHARGE :
    if (ScanBoss::verbose) std::cout<<"Going to do a CHARGE scan using charge value: = "<<paramValueFloat<<" with capicator type = "
                                    <<(m_feCapType ? "High" : "Low") << " to calculate vcal; stay tuned"<<std::endl;
    m_fei3Mod.writeVcalFromCharge(paramValueFloat, m_feCapType);
    break;
  case PixLib::PixScanBase::TDACS :
    m_fei3Mod.setAllPixelShiftRegisters(true);
    m_fei3Mod.sendGlobalConfig();
      for (int reg = static_cast<int>(Fei3::PixLatch::TDAC0); reg <= static_cast<int>(Fei3::PixLatch::TDAC6); reg++){
        uint32_t bitStream [Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];
        int bit = reg - static_cast<int>(Fei3::PixLatch::TDAC0);
        //std::cout<<reg<<" "<<paramValue<<" "<<bit<<" "<< (int)(paramValue &  (1<<bit)) <<std::endl;
        //Build TDAC values from paramValue
          for (unsigned int k = 0; k < Fei3PixelCfg::nDC ; k++) {
            for (unsigned int j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
              if( paramValue & (1<<bit) ) bitStream[k][j]=0xFFFFFFFF;
              else bitStream[k][j]=0x0;
            }
          }
        m_fei3Mod.strobePixelLatches(16, static_cast<Fei3::PixLatch>(reg), bitStream);
      }
    m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);
    break;
  case PixLib::PixScanBase::FDACS :
    m_fei3Mod.setAllPixelShiftRegisters(true);
    m_fei3Mod.sendGlobalConfig();
      for (int reg = static_cast<int>(Fei3::PixLatch::FDAC0); reg <= static_cast<int>(Fei3::PixLatch::FDAC2); reg++){
        uint32_t bitStream [Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];
        int bit = reg - static_cast<int>(Fei3::PixLatch::FDAC0);
        //std::cout<<reg<<" "<<paramValue<<" "<<bit<<" "<< (int)(paramValue &  (1<<bit)) <<std::endl;
        //Build FDAC values from paramValue
          for (unsigned int k = 0; k < Fei3PixelCfg::nDC ; k++) {
            for (unsigned int j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
              if( paramValue & (1<<bit) ) bitStream[k][j]=0xFFFFFFFF;
              else bitStream[k][j]=0x0;
            }
          }
        m_fei3Mod.strobePixelLatches(16, static_cast<Fei3::PixLatch>(reg), bitStream);
      }
    m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);
    break;
    case PixLib::PixScanBase::GDAC :
      m_fei3Mod.writeGlobalReg(Fei3::GlobReg::GlobalTDAC, paramValue & 0x1F);
    break;
    case PixLib::PixScanBase::IF :
      m_fei3Mod.writeGlobalReg(Fei3::GlobReg::IF, paramValue & 0xFF);
    break;
  default :
    std::cout<<"WARNING: You chose a scan parameter (enum): "<<(int)m_scanParam<<" that is not supported by the PPC code yet."
             <<"Sorry. You won't get any scanning action."<<std::endl;
  }
  m_fei3Mod.sendGlobalConfig();

  // sets width of calibration pulse
  if (m_strobeDuration > 0) // Warning: make sure that CNT is not overwritten afterwards (or move code)
      m_fei3Mod.sendMCCRegister(CNT,m_strobeDuration);  // ToCheck: -1 in IBL

///////////////////////////////////////////////////////////////
#endif
}

void ScanL12::pulseFrontEnd(int32_t iMask) {
#ifdef __XMK__
#ifdef __MCC_TEST__
#ifdef __SINGLE_MASK_STEP__
if(iMask > 0) return;
#endif
#if __USE_FEI3__
    Fei3TestScan fei3TS;
#else
    MCCTestScan fei3TS;
#endif
//     fei3TS.m_feMask = 0x40; //  Chip 6 hard coded for now.
// //     fei3TS.m_nTriggers = m_nTriggers;
//     fei3TS.m_nTriggers = 1;
//     mccTS.setNTrig(m_nTriggers);
    fei3TS.setNTrig(1);
    fei3TS.iMask = iMask;
    fei3TS.setModMask(m_channelsConsole);
#ifdef __HISTO_FILTERING__
    fei3TS.histoFilter = 1;
#else
    fei3TS.histoFilter = 0;
#endif
//     fei3TS.m_opMode = Fei3TestScan::MccInjection;
if (ScanBoss::verbose) std::cout << "m_nTriggers: " << m_nTriggers << ", iMask: " << fei3TS.iMask << ", m_useSlave1: " << (uint32_t)m_useSlave1 << ", m_histCfgOut1.cfg[0].enable: " << m_histCfgOut1.cfg[0].enable << ", m_histCfgOut0.cfg[1].enable: " << m_histCfgOut0.cfg[1].enable << std::endl;
    if(m_useSlave0){
      if(m_histCfgOut0.cfg[0].enable) {
        fei3TS.histConfig_in.cfg[0].maskStep = m_histCfgOut0.cfg[0].maskStep;
        fei3TS.histConfig_in.cfg[0].mStepOdd = m_histCfgOut0.cfg[0].mStepOdd;
        fei3TS.histConfig_in.cfg[0].mStepEven = m_histCfgOut0.cfg[0].mStepEven;
        fei3TS.execute();
      }
      if(m_histCfgOut0.cfg[1].enable) {
        fei3TS.histConfig_in.cfg[0].maskStep = m_histCfgOut0.cfg[1].maskStep;
        fei3TS.histConfig_in.cfg[0].mStepOdd = m_histCfgOut0.cfg[1].mStepOdd;
        fei3TS.histConfig_in.cfg[0].mStepEven = m_histCfgOut0.cfg[1].mStepEven;
        fei3TS.execute();
      }
    }
    if(m_useSlave1){
      if(m_histCfgOut1.cfg[0].enable) {
        fei3TS.histConfig_in.cfg[0].maskStep = m_histCfgOut1.cfg[0].maskStep;
        fei3TS.histConfig_in.cfg[0].mStepOdd = m_histCfgOut1.cfg[0].mStepOdd;
        fei3TS.histConfig_in.cfg[0].mStepEven = m_histCfgOut1.cfg[0].mStepEven;
        fei3TS.execute();
      }
      if(m_histCfgOut1.cfg[1].enable) {
        fei3TS.histConfig_in.cfg[0].maskStep = m_histCfgOut1.cfg[1].maskStep;
        fei3TS.histConfig_in.cfg[0].mStepOdd = m_histCfgOut1.cfg[1].mStepOdd;
        fei3TS.histConfig_in.cfg[0].mStepEven = m_histCfgOut1.cfg[1].mStepEven;
        fei3TS.execute();
      }
    }
#else // no __MCC_TEST__
      m_fei3Sport.sendCALandLVL1(m_trigDelay);
#endif
#endif // __XMK__
}

void ScanL12::triggerOnly() {
#ifdef __XMK__
	m_fei3Sport.sendLV1();
	usleep(m_sleepDelay);
#endif
}

void ScanL12::checkReadyForNextTrigger() {
  // not yet implemented at all  -- to do 
  usleep(m_sleepDelay);  //wait before sending next trigger
}
void ScanL12::debugReadBack(int32_t iMask) {
#ifdef __XMK__

///////////// Fei3 functionality unknown //////////////////////
//   if(ScanBoss::readSR){
//     Fei3ModData::setVerboseSR(true);
//     Fei3Mod f =  RodResourceManagerPIX::getCalibModuleConfig(ScanBoss::rxCh);
//     for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount)
//       f.readFrame();
//   }
//
//   if (ScanBoss::readBackInmem){
//     uint32_t fifoByte = 0xc0ffee;
//     for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
//       fifoByte = (fifo.read()&0xFF);
//       for (int i = 0; i < 1000; i++) {
// 	fifoByte = (fifo.read()&0xFF);
// 	if (i%3 == 0) std::cout<<std::dec<<"read back frame : "<<(int)iTrigCount<<" from inmem fifo ch "<<
// 	  (int)ScanBoss::rxCh<<": Word "<<int(i/3)<<" = 0x"<<std::hex<<fifoByte;
// 	else if (i%3 == 1) std::cout<<std::hex<<fifoByte;
// 	else std::cout<<std::hex<<fifoByte<<std::dec<<std::endl;
//
// 	if ((fifoByte == 0xBC) || (fifoByte == 0x00)) break;
// 	if (fifoByte == 0x800) break;
//       }
//       std::cout<<std::endl;
//     }
//     std::cout<<"**********************************"<<std::dec<<std::endl;
//   }
//   Fei3Mod f;
//   // Loop over all debug hists and fill for each trigger.
//   BOOST_FOREACH(Fei4Hist& hist, ScanBoss::getDebugHists()) { //  Need to build Fei3 equivalent of Fei4Hist !!!
//     f = RodResourceManagerPIX::getCalibModuleConfig(hist.rxCh);
//     for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
//       Fei3ModData::fillPixDataHist(f.readFrame(), f.getValue(&Fei3ModCfg::HD), hist); //  Fei3ModCfg or Fei3Cfg ???
//     }
//   }
//
//   if (!ScanBoss::readBackBoc) return;
//   // if you want to read out from the inmem fifo or bob fifo
//   // for debug purpose, put that code here
//   std::vector<int> answer;
//   f =  RodResourceManagerPIX::getCalibModuleConfig(ScanBoss::rxCh);
//   for (uint8_t iTrigCount = 0 ; iTrigCount < m_frameCount ; ++iTrigCount) {
//     answer = f.readFrame();
//     // Original print out version
//     if (ScanBoss::readBackBoc) {
//       std::cout<<"**********************************"<<std::endl;
//       std::cout<<" READ FRAME "<<int(iTrigCount)<<" from BOC fifo channel "<<(int)ScanBoss::rxCh<<std::endl;
//       std::cout<<"**********************************"<<std::endl;
//       unsigned int answerSize = answer.size();
//       for (unsigned int i = 0; i < answerSize; i++) {
// 	if (i%3 == 0) std::cout<<"Word: "<<int(i/3)<<" = 0x"<<std::hex<<std::setw(2)<<answer[i];
// 	else if (i%3 == 1) std::cout<<std::hex<<std::setw(2)<<answer[i];
// 	else std::cout<<std::hex<<std::setw(2)<<answer[i]<<std::dec<<std::endl;
//       }
//       //      std::cout<<"**********************************"<<std::endl;
//       //      Fei3ModData::printPixData(answer, f.->getValue(&Fei3ModCfg::HD)); // tmp, LJ! for debugging
//       //      std::cout<<"**********************************"<<std::endl;
//     } // printOut
//   }
///////////////////////////////////////////////////////////////

#endif
}

void ScanL12::getParameterFeedback(int32_t iPar) {
	// anything you want to do before starting next parameter loop
}

bool ScanL12::stopHistogrammer(int32_t iMask) {
	bool success = true;
#ifdef __XMK__
	//  std::cout<<"about to stop histogrammer "<<std::endl;
	if (ScanBoss::verbose) std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;
	// only stop histogrammer each mask step for those mask steps supported by histogrammer
	// otherwise only stop histogrammer at end of scan
	if (!(m_maskSteps == 32 || m_maskSteps == 64) && !(iMask == (m_maskStepsToDo - 1))) return success;

	if (m_useSlave0) success =  m_slave.stopHisto(0);
	if (m_useSlave1) success &= m_slave.stopHisto(1);
	if (!success) {
		status.errorState = SLAVE_NOT_RESPONDING;
		std::cout << " One of the slaves did not respond to stop histo command" << std::endl;
	}
	if (success && ScanBoss::verbose) std::cout << "----stopped histogrammer(s)" << std::endl;
	//  std::cout<<"stopped histogrammer "<<std::endl;
#endif
	return success;
}

////////////////////////////////////////////
// Data processing and resetting of system
////////////////////////////////////////////

void ScanL12::processResult() {
#ifdef __XMK__

	//expected number of frames = total number of triggers * frame count
	uint32_t expected = 0;
	if(m_dspScanType != SetDSPScan::NONE) expected = m_nTriggers * m_nParameterSteps * m_nParameter1Steps;
	else expected =  m_nTriggers * m_nDCLoops * m_maskStepsToDo * m_nParameterSteps * m_frameCount;

	//dump Frames received
	for(uint8_t rxCh = 0 ; rxCh < 32 ; ++rxCh)
	   if (m_channels & (1<<rxCh)){
			uint32_t received = (int)IblBoc::GetFrameCount(rxCh);
			std::cout << "Rx " << (int)rxCh << " received: " << (int)received
			          << " start of frames when I expected " << (int)expected << ((received == expected) ? " Correct " : " Not correct ") << std::endl;
		}
	//if(m_dspScanType != SetDSPScan::NONE)std::cout<<"Frame counters not reliable during DSP-scan"<<std::endl;

	//reading BOC Tx counters
	std::cout<<" Reading BOC Tx monitoring counters "<<std::endl;
	std::cout << "TxCh |  Trigger   | BCR        | ECR        | CAL        | SYNC       | Slow       | Corrupt" << std::endl;
	std::cout << "===+============+============+============+============+============+============+============" << std::endl;
	std::array <uint32_t,7> monCnt;
	uint32_t txMask = BarrelConnectivity::getTxMaskFromRxMask(m_channels);
	for(uint8_t txCh=0;txCh<32;txCh++)
	  if (txMask & (1<<txCh)){
	  IblBoc::getTxMonitoringCounters(txCh/16, txCh%16, monCnt.data());
	   std::cout << std::setfill(' ') << std::setw(4) << (int)txCh << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[0] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[2] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[1] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[3] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[6] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[4] << " | ";
           std::cout << std::setfill(' ') << std::setw(10) << monCnt[5] << std::endl;
           IblBoc::DisableTxMonitoring(txCh/16, txCh%16);
	  }

	// Process debug output?
#endif
}

void ScanL12::resetSystem() {
#ifdef __XMK__

	status.state = SCAN_TERMINATED;
	// Reset the pixel latches, by writing a final mask stage mode in clear mode
	if(m_dspScanType != SetDSPScan::NONE)setupMaskIteration(0, true);
	//After performing scan with digital injection, we're restoring the configuration to 0 after scan
	//reset global registers to the one in the original config after the scan which were changed during the scan
	if( m_doDigitalInjection) resetGlobalRegister(Fei3::GlobReg::EnableDigInj, m_globalConfigStore);
	resetGlobalRegister(Fei3::GlobReg::ReadMode, m_globalConfigStore);
	resetGlobalRegister(Fei3::GlobReg::THRMIN, m_globalConfigStore);
	resetGlobalRegister(Fei3::GlobReg::THRDUB, m_globalConfigStore);
	resetGlobalRegister(Fei3::GlobReg::EOC_MUX, m_globalConfigStore);
	resetGlobalRegister(Fei3::GlobReg::CEU_Clock, m_globalConfigStore);
	resetMCCRegister(CAL,m_MCCConfigStore);

#ifndef __HISTO_TEST__
	// Turn off the BOC->ROD link to prevent junk from filling the slaves while not running
	m_slave.enableRxLink(0);

	// To-do: dump # of received boc frames here
	// Disable all Boc Rx
	DataTakingTools::setBocEmu(false, 0xFFFFFFFF);//Disable emulator
	m_iblBocLink->disableRxChannels(0xFFFFFFFF);
	RodResourceManagerPIX::getSerialPort()->setTxMask(0x0);//Disable serial port
#else
	status.state = SCAN_TERMINATED;
#endif // no __HISTO_TEST__

m_dspScanType = SetDSPScan::NONE;
#endif // __XMK__
	// To-do: reset rest of ROD
}

void ScanL12::registerDump() {
#ifdef __XMK__
	std::cout << "********************" << std::endl;
	std::cout << "Register debug, after setting up DC and about to trigger: " << std::endl;
	std::cout << "********************" << std::endl;

	DumpRodRegisters cmd;
	cmd.dump();

	std::cout << "******about to sleep for 10 seconds" << std::endl;
	usleep(10000000);
#endif
}

void ScanL12::abortScan(uint32_t scanId) {
	if (scanId != m_scanId) std::cout << " ** Warning scan ID to be aborted(" << scanId << ") and Scan ID being aborted(" << m_scanId << ") do not match !!" << std::endl;
	aborting = true;
}

std::vector< std::vector< uint32_t > > ScanL12::getDSPResults(uint8_t rxCh) {

	return m_dspResults[rxCh];

}

void ScanL12::cleanDSPResults() {

	m_dspResults.clear();

}

void ScanL12::cleanDSPResults(uint8_t rxCh) {

	m_dspResults[rxCh].clear();
}


void ScanL12::fakeOptoResults() {
	//This method fills the optoErrors vector with a dummy result, to check propagation in the system

	int max_y = 7;
	int max_x = 7;

	for(int rxCh=0;rxCh<32;rxCh++)
	for (int y = 0; y < max_y; y++) {
		std::vector< uint32_t > results_thresh;
		for (int i = 0; i < max_x; i++) {
			if (i < 3 || i > max_x - 3 || y < 3 || y > max_y - 3) results_thresh.push_back(1);
			else results_thresh.push_back(0);
		}
		m_dspResults[rxCh].push_back(results_thresh);
	}
}

void ScanL12::printOptoResults() {
	//DEBUG cout of opto info
	for(std::map< uint8_t, std::vector< std::vector< uint32_t > > >::iterator it = m_dspResults.begin(); it!=m_dspResults.end(); ++it){
		std::cout << " -- Rx channel " << (int)it->first << std::endl;
		for (size_t iThr = 0; iThr < it->second.size(); iThr++) {
			for (size_t iDelay = 0; iDelay < it->second[iThr].size(); iDelay++) {
				std::cout << (int)it->second[iThr][iDelay] << " ";
			}
			std::cout << std::endl;
		}
		std::cout << std::endl;
	}
}

#ifdef __XMK__
void ScanL12::resetGlobalRegister(Fei3::GlobReg reg, std::vector<std::vector<Fei3GlobalCfg>>& cfgs) {
  uint32_t nMod = RodResourceManagerPIX::getNModuleConfigs();
  for (size_t modIx = 0; modIx < nMod; ++modIx){
      Fei3Mod & modCfg = RodResourceManagerPIX::getCalibModuleConfig(modIx);
      for(size_t feIx = 0; feIx < Fei3ModCfg::nFE; ++feIx) {
          Fei3GlobalCfg & resMgrCfg = modCfg.m_chipCfgs[feIx];
          Fei3GlobalCfg & bkgCfg = cfgs.at(modIx).at(feIx);
          resMgrCfg.writeRegGlobal(reg, bkgCfg.readRegGlobal(reg));
      }
  }
}

void ScanL12::resetMCCRegister(MCCRegisters reg, std::vector<std::array <uint16_t,11>>& cfgs){
  uint32_t nMod = RodResourceManagerPIX::getNModuleConfigs();
  for (size_t modIx = 0; modIx < nMod; ++modIx){
      Fei3Mod & modCfg = RodResourceManagerPIX::getCalibModuleConfig(modIx);
      modCfg.mccRegisters[reg] = cfgs.at(modIx).at(reg);
  }

}
#endif
