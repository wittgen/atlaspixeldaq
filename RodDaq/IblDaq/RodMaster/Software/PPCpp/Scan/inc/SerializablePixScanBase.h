/*
  Steve Alkire <alkire@cern.ch>
  2014.02.17
*/
#ifndef __SERIALIZABLE_PSB_H__
#define __SERIALIZABLE_PSB_H__

#include "PixScanBase.h"
#include "EasySerializable.h"

class SerializablePixScanBase : public PixLib::PixScanBase, public EasySerializable {

 public:

  SerializablePixScanBase(){};
  SerializablePixScanBase(const PixScanBase& psb): PixScanBase(psb) {
      std::cout << __PRETTY_FUNCTION__ << std::endl;
  }
  virtual ~SerializablePixScanBase() {};


  virtual void serial();
  virtual void serial() const;
};
#endif //__SERIALIZABLE_PSB_H__
