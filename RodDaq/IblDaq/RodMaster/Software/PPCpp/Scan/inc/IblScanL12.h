
/*
 * Author: L. Jeanty <laura.jeanty@cern.ch>
 * Date: 11-Feb-2014
 * Description: Scan for IBL
 * Uses PixScanBase to set parameters
 */

#ifndef __IBL_SCAN_L12_H__
#define __IBL_SCAN_L12_H__

//#define __DO_GET_STATUS__

#include "IblRodScanStatus.h"

#ifdef __XMK__
#include "Fei3Mod.h"
#include "Fei3ModProxy.h"
#include "Fei3PixelCfg.h"
#include "IblRodSlave.h"
#include "PixScanBase.h"
#include "IblRodInmemFifo.h"
#include "IblBocLink.h"
#include "DataTakingTools.h"
#endif
#include "SetDSPScan.h"
#include <vector>

class ScanL12 {

 public:

  ScanL12();
  virtual ~ScanL12() {;};
  
  bool initScan();
  void runScan();
  void endScan();
  void setChannels(uint32_t ch) {m_channels = ch;};
  void setScanId(uint32_t id) {m_scanId = id;};
  void setFitServerPorts(uint32_t ports[4]) { std::copy(ports, ports+4, m_fitPorts); };
  void setFitServerIps(uint8_t ips[4][4]) { std::copy(ips[0], ips[0]+16, (m_fitIps)[0]); };
  void abortScan( uint32_t scanId);

  std::vector< std::vector< uint32_t > > getDSPResults(uint8_t rxCh);

  void setDSPScan(SetDSPScan::ScanTypeDSP sType ){m_dspScanType = sType;}

  void cleanDSPResults( ); 
  void cleanDSPResults(uint8_t rxCh ); 

  IblRodScanStatus getStatus(){ return status; };
  
  bool doSleep; // for slowing down scan for testing purposes
  bool aborting;
 protected:

 private:
  
  struct optoSettings{
	uint8_t modId;
	uint8_t rxCh;
	uint8_t txCh;
	uint8_t fibre;
	uint8_t pluginId;
	uint8_t coarseDelay;
	uint8_t fineDelay;
	uint16_t threshold;
	bool isAltLink;
	};

  // Internal functions
  void runOptoScan();  
  void runMonLeakScan();
  void fakeOptoResults();
  void printOptoResults();

  // initialize functions
  void fillScanParameters();
  void printOutScanParams();
  // interprets enum of # of mask steps for both total and number to run over
  // returns true if you should loop over double columns as well
#ifdef __XMK__
  bool setupMaskSteps(PixLib::EnumMaskSteps::MaskStageSteps requestedMaskSteps, uint32_t &maskSteps, uint8_t &dcMode);
  uint32_t decodeMaskStageMode(PixLib::EnumMaskStageMode::MaskStageMode maskStageMode);
#endif
  bool initializeROD();
#ifdef __XMK__
  bool configureHistogrammer(uint8_t slaveID, IblSlvHistCfg& histCfgOut, IblSlvNetCfg& netCfgOut);
#endif
  void initializeFrontEnd(); 

  // scan functions
  void setupMaskIteration(int32_t iMask, bool clear = false);
  bool startHistogrammer(int32_t iPar, int32_t iMask);
  void setupDoubleColumnIteration(int8_t iDC);
  void setupParameterIteration(int32_t iPar);
  void pulseFrontEnd(int32_t iMask);
  void triggerOnly();
  void checkReadyForNextTrigger();
  void debugReadBack(int32_t iMask);
  void getParameterFeedback(int32_t iPar);
  bool stopHistogrammer(int32_t iMask);

  void registerDump();
  
  void processResult();
  void resetSystem();

#ifdef __XMK__
  void resetGlobalRegister(Fei3::GlobReg reg, std::vector<std::vector<Fei3GlobalCfg>>& cfgs);
  void resetMCCRegister(MCCRegisters reg, std::vector<std::array <uint16_t,11>>& cfgs);
#endif

#ifdef __XMK__
  // not premenant; for debug
  IblRodInmemFifo fifo;
#endif 

  // Scan parameters
#ifdef __XMK__
  PixLib::PixScanBase * m_scan;
#endif

  // Links to front ends for sending configs and commands
#ifdef __XMK__
  Fei3ModProxy m_fei3Mod; // Abstract front end for broadcasting commands
  Fei3Mod m_dummyFei3ModForBroadcast; // Only used for broadcasting pixel-setting commmands, for which the Fei3ModProxy would loop over all modules
  Fei3Mod m_fei3Sport;//Serial port to speed up commands
  IblBocLink * m_iblBocLink; // link to the boc for enabling/disabling channels
#endif

  // Pixel Mask
#ifdef __XMK__
  Mask m_mask;
  uint32_t m_pixReadoutEnableMask[Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];
  uint32_t m_pixWriteSelectMask[Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC];
#endif

  // Histogrammer related classes
#ifdef __XMK__
  IblRodSlave m_slave;
  IblSlvHistCfg m_histCfgOut0, m_histCfgOut1; // one for each slave (can enable different histogrammering units on each)
  IblSlvNetCfg m_netCfgOut0, m_netCfgOut1;
#endif

  // necessary parameters set per each specific scan 
  uint32_t m_channels;
#ifdef __XMK__
  uint32_t m_channelsConsole; // needed to directly get the number of channels enabled in the Console (rather than updating it according to BocLink)
#endif
  uint32_t m_scanId;
  uint32_t m_fitPorts[4];
  uint8_t m_fitIps[4][4];

  // Derived variables or other parameters, not all permenant
  bool m_useSlave0, m_useSlave1;
  bool debugWhichChips;
  bool debugWhichMaskStep;
  uint32_t m_sleepDelay;
  bool lowDebug;
  int32_t m_maskStepsToDo; // number of mask steps (only 1, 2, 4, 8 supported) to loop over
#ifdef __XMK__
  // Parameters needed from PixScanBase; direct or derived
  bool m_loopDC; // loop over double columns if true
  uint8_t m_modeDC; // dc mode (0,1,2, or 3)
  int8_t m_nDCLoops; // number of double column loops 
  uint32_t m_maskSteps; // total number of mask steps (only 1, 2, 4, 8 supported)
  int32_t m_nTriggers; // number of triggers per parameter value
  uint8_t m_frameCount; // number of frames to read out per trigger
  uint8_t m_trigDelay; // delay between cal pulse and trigger
  uint8_t m_trigDelayOverride; // whether or not to use the trig delay set in the calib config
  bool m_restoreModConfig; // restore module configuration at end of scan to original config (note sure: physics or calib?)
  uint8_t m_ReadMode; //2-bit control field; 0:send in the usual way,1:apply THRMIN to TOT value,2:activates TOT doubling,3:both of the previous operations
  uint8_t m_TotMin;   //Cut value on the minimum TOT value	
  uint8_t m_TotDHThr; //Threshold for the hit doubling
  uint8_t m_TotTimeStampMode; //Control which data the CEU passes on to the EOC buffer for storage in the TOT field of the hit data.
  uint8_t m_ColumnROFreq; // Controls the speed of operation of the column pair readout.

  PixLib::EnumMaskStageMode::MaskStageMode m_maskStageMode; // "Mask staging option i.e. which ctrl bits are staged"
  PixLib::EnumScanParam::ScanParam m_scanParam; // parameter to scan over in zeroth loop
  PixLib::EnumScanParam::ScanParam m_scanParam1; // parameter to scan over in first loop  
  PixLib::EnumScanParam::ScanParam m_scanParam2; // parameter to scan over in second loop
  bool m_doDigitalInjection; // do digital injection?
  uint8_t m_trigLatency; // 8-bit trigger latency as programmed on FEs
  uint8_t m_strobeDuration; // width of calibration pulse
  uint8_t m_strobeDelay; // delay between detection of command for strobe and strobe issue
  uint8_t m_strobeDelayRange; // Sstrobe delay range
  uint32_t m_pixelLatchStrobeValue; // the value to write to the pixel latch strobe register to set the 13 pixel latches
  uint32_t m_feVCal; // height of analog injection
  uint32_t m_feCapType; // Capicator type (0 = low, 1 = high)
  bool m_doParamLoop; // a loop over parameter 0 is active
  bool m_doParam1Loop; // a loop over parameter 1 is active
  bool m_doParam2Loop; // a loop over parameter 2 is active
  int32_t m_nParameterSteps; // parameter scan variables // for now, only 1 parameter loop at a time is supported
  int32_t m_nParameter1Steps; // Not (yet?) used
  int32_t m_nParameter2Steps; // Not (yet?) used
  float m_parameterMin;
  float m_parameterMax;
  float m_parameter1Min;
  float m_parameter1Max;
  float m_parameter2Min;
  float m_parameter2Max;
  bool m_setToTHistoMode; // set the histogrammer to ToT mode? may be moved to enum if we use both SHORT and LONG TOT; for now just using LONG
  bool m_noiseScan;
  bool usingFei3;
  int32_t m_bocMode; // readout speed
  uint32_t FEEN_Mask; // for looping through FE trigger broadcast blocks
  std::vector<std::vector<Fei3GlobalCfg>> m_globalConfigStore;
  std::vector<std::array <uint16_t,11>> m_MCCConfigStore;
#endif
  // DSP scan variables
  SetDSPScan::ScanTypeDSP m_dspScanType;
  std::map<uint8_t, std::vector<std::vector<uint32_t>>> m_dspResults;  	
 
  IblRodScanStatus status;
};

#endif // __IBL_SCAN_L12_H__
