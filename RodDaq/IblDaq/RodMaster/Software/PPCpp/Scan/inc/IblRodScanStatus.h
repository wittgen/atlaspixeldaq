/*
Steve Alkire <alkire@cern.ch>, 2014.03.10
Special Status implementation for a scan on the Fei4.
*/
#ifndef __IRS_STATUS_H__
#define __IRS_STATUS_H__

#include<stdint.h>
#include<iostream>
#include "scanEnums.h"

enum PPCErrorType{PPCERROR_NONE, NO_CONN_TO_FITSERVER, SLAVE_NOT_RESPONDING, NO_ENABLED_CHANNELS};
  
class IblRodScanStatus {
 public:
  IblRodScanStatus(){
    //Maybe it is nice if you set defaults to something in particular  so you know if things have started
    state = SCAN_IDLE;
    errorState = PPCERROR_NONE;
    mask = 0;
    par = 0;
    par1 = 0;
    par2 = 0;
    dc = 0;
    trig = 0;
}
  virtual ~IblRodScanStatus(){}

  
  ScanState state;
  PPCErrorType errorState;
  int32_t mask;
  int32_t par;
  int32_t par1;
  int32_t par2;
  int8_t dc;
  int32_t trig;

  void serial();
  void serial() const;
};
#endif //__IRS_STATUS_H__   
