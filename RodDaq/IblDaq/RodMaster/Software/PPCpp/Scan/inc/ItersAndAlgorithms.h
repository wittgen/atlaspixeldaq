#ifndef __ITERS_ALGORITHMS_H__
#define __ITERS_ALGORITHMS_H__
/*
  Steve Alkire <alkire@cern.ch> 2014.03.19
  Iterators and algorithms.
*/
#include <iostream>
#include "EasySerializable.h"

//use with an unsigned type T--the range is your own responsibility
template<typename T> bool maskInterpret(const T& mask, unsigned position){
  return (bool) (0x1 & (mask>>position));
}

template<typename T> 
class Iterator{
  virtual Iterator<T>& operator++()=0;
  virtual operator T*()=0;
};

template<typename T> 
class Search{
  T* m_object;
  virtual Search<T>& operator++()=0;
  virtual bool done() = 0;
  virtual bool compare(T* a, T* b) = 0;
};

template<typename T, unsigned Range>
  class CircularIterator:public Iterator<T>, public EasySerializable{
 private:
  T* m_object;
  T* m_lower;
 public: 
  operator T*(){
    return m_object;
  }
  CircularIterator(){}
  CircularIterator(T* object){
    m_object = object;
    m_lower = object;
  }
  CircularIterator(const CircularIterator<T,Range> &rhs){
    m_object = rhs.m_object;
    m_lower = rhs.m_lower;
  }
  
  CircularIterator<T,Range>& operator++(){
    m_object = (++m_object - m_lower)%Range + m_lower;
    return *this;
  }
  //T& operator--(){return (--m_object - m_lower)%Range + m_lower;}
  SERIAL_H(prep(m_object); prep(m_lower);)
};

//Note: Assumes Y is contiguous memory. X doesn't have to be.
template<typename T, unsigned XRange, unsigned YRange, bool XOutsideLoop = true>
class CircularIterator2D:public EasySerializable{
 private:
  unsigned m_YOffset;
  T** m_currentX;
  T** m_lower;//I would love to const this but then I can't serialize this class

 public:
  operator T*(){
    return *m_currentX+m_YOffset;
  }
  CircularIterator2D():m_YOffset(0){}
  CircularIterator2D(T* object):m_currentX(&object),m_lower(&object),m_YOffset(0){}
  //CircularIterator2D(T** object):m_currentX(object),m_lower(object),m_YOffset(0){}
  CircularIterator2D(const CircularIterator2D<T,XRange,YRange,XOutsideLoop> &rhs):m_currentX(rhs.m_currentX),m_lower(rhs.m_lower),m_YOffset(rhs.m_YOffset){}
 
  CircularIterator2D<T,XRange,YRange,XOutsideLoop>& operator++(){
    if( XOutsideLoop ){
      if(++m_YOffset >= YRange){
	m_YOffset = 0;
	(++m_currentX - m_lower)%XRange + m_lower;
      }
    }else{
      if(++m_currentX >= m_lower+XRange){
	m_currentX = m_lower;
	++m_YOffset%YRange;
      }
    }
    return *this;
  }
  void printTest(){std::cout<<"hi"<<std::endl;}//remove

  SERIAL_H(prep(m_YOffset); prep(m_currentX); prep(m_lower);)
};

//use to iterate on another device along with this one
//The iterator is on type T
template<typename T, typename _Iterator>
class OffsetIterator: public EasySerializable{
 private:
  T* m_lower;
  _Iterator m_home;
  uint32_t m_offset;
 public:
  OffsetIterator():m_offset(0){}
  OffsetIterator(T* object):m_lower(object),m_offset(0){
    _Iterator home( object );
    m_home = home;
  }
  //use on away
  T* getAwayPointer(T* base){return base + m_offset;}
  //don't use on away--I could protect these with XMK but then we assume that this is used from host
  operator T*(){
    return (T*)m_home;
  }  
  //don't use on away
  OffsetIterator<T,_Iterator>& operator++(){
    ++m_home;
    m_offset = (T*)m_home - m_lower;
  }
  SERIAL_H( prep(m_offset); )
};

template<typename _Iterator>
class SharedIterator:public EasySerializable{
 private:
  _Iterator m_home;
  _Iterator m_away;
  bool m_awayIsSet;
 public:
  SharedIterator(): m_awayIsSet(false){}
  SharedIterator(_Iterator home): m_home(home),m_awayIsSet(false){}
  void setAwayIterator(_Iterator away){if(!m_awayIsSet){m_away = away; m_awayIsSet = true;}}
  bool awayIsSet(){return m_awayIsSet;}
  SharedIterator<_Iterator>& operator++(){
    ++m_home;
    if(m_awayIsSet) ++m_away;
  }
  _Iterator& home(){return m_home;}//careful, if you increment these individually, syncing of home/away will change.
  _Iterator& away(){return m_away;}//...lets just call this a feature for now ;)
  SERIAL_H(prep(m_home); prep(m_away); prep(m_awayIsSet);)
};
/*
template<typename T, unsigned Range>
  class LinearSearch{
  
};

template<typename T, unsigned Range>
class BinomialSearch{
  
};
*/

#endif //__ITERS_ALGORITHMS_H__

