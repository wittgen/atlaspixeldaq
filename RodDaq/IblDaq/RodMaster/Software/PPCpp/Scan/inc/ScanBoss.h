/*
 * Steve Alkire <alkire@cern.ch>, 2014.02.11
 *
 * B. Axen <bradley.axen@cern.ch>
 * Updated 10 July 2014:
 * Added more debugging tools.
 *
 * Modifying static debug flags is not thread safe. I could make 
 * it thread safe if we ever expect to threads to modify the
 * values at near the same time, but that seems very unlikely.
 */
#ifndef __SCANBOSS_H__
#define __SCANBOSS_H__

#include <stdint.h>
#include <iostream>
#include <string>
#include "PixScanBase.h"
#include "IblScan.h"
#include "IblScanL12.h"
#include "Fei4PixelCfg.h"
#include "Fei4Data.h"
#include "ItersAndAlgorithms.h"

class ScanBoss{
 public:

  ScanBoss(){}
  virtual ~ScanBoss(){}

  static IblRodScanStatus scanResult;

  static PixLib::PixScanBase& getPixScanBase();
  static void setPixScanBase(PixLib::PixScanBase sb);
#ifdef __DO_IBL_GET_STATUS__
  static void startScan(uint32_t channels, uint32_t scanId, uint32_t fitsPort[4], uint8_t fitIps[4][4]);
#else
  static IblRodScanStatus startScan(uint32_t channels, uint32_t scanId, uint32_t fitsPort[4], uint8_t fitIps[4][4]);
#endif
#ifdef __DO_GET_STATUS__
  static void startScanL12(uint32_t channels, uint32_t scanId, uint32_t fitsPort[4], uint8_t fitIps[4][4]);
#else
  static IblRodScanStatus startScanL12(uint32_t channels, uint32_t scanId, uint32_t fitsPort[4], uint8_t fitIps[4][4]);
#endif
  static Scan& getScan(){return m_scan;};
  static ScanL12& getScanL12(){return m_scanL12;};

  // Text based debugging.
  static bool verbose;
  static bool readBackBoc;
  static bool readBackInmem;
  static bool readSR;
  static uint8_t rxCh;
  static uint8_t debugDCLoops;
  static uint32_t debugChannels;

  // Histogram debugging.
  static void addDebugHist(const Fei4Data::DataType& data, const Fei4Data::FIFOType& fifo, uint8_t rxCh);
  static std::vector<Fei4Hist>& getDebugHists() {return ScanBoss::m_debugHists;}
  static void clearDebugHists();

  static void startRegisterScan(uint32_t channels = 0xFFFFFFFF);
  static std::string howDoIFeel;
  static CircularIterator2D<DoubleColumnBit,Fei4PixelCfg::n_DC,Fei4PixelCfg::n_Bit> iterFei4PixelCfg;
  static unsigned Offset;
  static DoubleColumnBit testIterSB[5];
  //static CircularIterator<DoubleColumnBit,5> iterTestIter;

 private:
  static PixLib::PixScanBase scanBase;
  //static bool scanCanBeStarted;
  static Scan m_scan;
  static ScanL12 m_scanL12;
  //static RegisterScan m_registerScan;
  static std::vector<Fei4Hist> m_debugHists;
};

#endif //__SCANBOSS_H__   
