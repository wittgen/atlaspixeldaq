/*
Steve Alkire <alkire@cern.ch> 2014.03.10
Base class status classes. 

Usage:
class StatusType : public Status{
public:
 void serial();
 void serial() const;
};

class MyClass : public Spy<StatusType>


...in your StatusType cxx:
SERIAL(
 prep(statusMember);
 ...
)

...in your scan spy on your variables:
type_t i; spy(i, status.member );
unspy(status.member, doneValue);

To do: Provide this functionality without using map!
*/
#ifndef __STATUS_H__
#define __STATUS_H__

#include <stdint.h>
#include <iostream>
#include "EasySerializable.h"
#include "Spy.h"

class Status:public EasySerializable{
 private:
  std::map<void*,void*>* spyMap;
 public:
  enum StatusSwitch_t {_EasySerializable,_StatusAction};
  StatusSwitch_t prepSwitch;
  Status(){prepSwitch = _EasySerializable;}
  virtual ~Status(){}

  void setSpy(std::map<void*,void*>* sm){
    spyMap = sm;
  }
  
  //overloading EasySerializable prep. Writes value spied to statusMember.
  template<typename T> void prep(T& datum){
    if(prepSwitch == _StatusAction){ 
#ifdef __XMK__
      std::map<void*,void*>::iterator it = spyMap->find((void*)(&datum));
      if(it!=spyMap->end()) datum = *((T*)it->second);
#endif
    }else
      EasySerializable::prep(datum);
  }
  
  //overloading EasySerializable prep. Writes values spied to statusMember.
  template<typename T> void prep(T* data, unsigned arraySize){
    if(prepSwitch == _StatusAction){
#ifdef __XMK__
      std::map<void*,void*>::iterator it = spyMap->find((void*)data);	
      if(it!=spyMap->end()){
	T* source = it->second;
	for(unsigned i = 0; i < arraySize; i++)
	  data[i] = source[i];
      }
#endif
    }else
      EasySerializable::prep(data);
  } 
  
  template<typename T> void prep(const T& datum) const{
    EasySerializable::prep(datum);
  }
  
  template<typename T> void prep(const T* data, unsigned arraySize) const{
    EasySerializable::prep(data);
  }   
};

#endif //__STATUS_H__   
