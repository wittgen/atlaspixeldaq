/*
Steve Alkire <alkire@cern.ch> 2014.03.10 
Maps pointers to values you wish to spy on to a status memeber 
 for the purpose of peeking the status of a task.

Usage: 
class StatusType : public Status 
class MyClass : public Spy<StatusType> 
...in your scan spy on your variables:
type_t i; spy(i, status.member );
...i varies...
...send commands to fetch status...
...i is done varying...
unspy(status.member, doneValue);

Thats it.

To do: Provide this functionality without using map!
*/
#ifndef __SPY_H__
#define __SPY_H__

#include <map>

template<class _Status>
class Spy{
 private:
  std::map<void*,void*> spyMap;
 protected:
  _Status status;
 public:
  Spy(){
    status.setSpy(&spyMap);
  }

  template<typename T> T spy(const T& datum, const T& statusMember){
    spyMap.insert(std::pair<void*,void*>((void*)&statusMember,(void*)&datum));
    return datum;
  }
  
  template<typename T> void unspy(T& statusMember){
    std::map<void*,void*>::iterator it = spyMap.find((void*)&statusMember);
    if (it != spyMap.end())
      spyMap.erase(it);   
  }
  
  template<typename T, typename U> void unspy(T& statusMember, U doneValue){
    statusMember = (T) doneValue;
    unspy(statusMember);
  }

  template<typename T> T* find(T& statusMember){
    return (T*) spyMap.find((void*)(&statusMember))->second;
  }
  
  _Status& getStatus(){
    status.prepSwitch = status._StatusAction;
    status.serial();
    status.prepSwitch = status._EasySerializable;
    return(status);
  }  
};

#endif //__SPY_H__
