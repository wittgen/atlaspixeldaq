/*  
 * Fei3PixelCfg
 * Pixel registers for one Fei3
 * By: Laser Kaplan
 */

#ifndef __FEI3PIXELCFG_H__
#define __FEI3PIXELCFG_H__

#include <stdint.h>
#include <cmath>
#include <iostream>
#include "EasySerializable.h"
#include "Fei3.h"


class Fei3PixelCfg : virtual public EasySerializable {
    public:
        Fei3PixelCfg();
        Fei3PixelCfg(const Fei3PixelCfg &rhs);
        Fei3PixelCfg & operator=(const Fei3PixelCfg &rhs);
        virtual ~Fei3PixelCfg() {}
        
        constexpr static unsigned nDC = 9;
        constexpr static unsigned nWordsPerDC = 10;

        void writeBitPixel(Fei3::PixLatch reg, int row, int col, int val);
        void writeTDACPixel(int row, int col, int val);
        void writeFDACPixel(int row, int col, int val);
        int readBitPixel(Fei3::PixLatch reg, int row, int col);
        int readTDACPixel(int row, int col);
        int readFDACPixel(int row, int col);
        int getWordFromRowCol(int row, int col);
        int getBitFromRowCol(int row, int col);
        int getWordNumber(Fei3::PixLatch reg, int dc, int word);
        void setWord(Fei3::PixLatch reg, int dc, int word, uint32_t val);

	uint32_t getPixelRegHash() const;

        uint32_t pixcfg[Fei3::nPixLatches * nDC * nWordsPerDC];
        static uint32_t nullPixelSR[nDC][nWordsPerDC];

	static uint32_t highPixelSR[nDC][nWordsPerDC];

        const std::size_t size() const {return Fei3::nPixLatches * nDC * nWordsPerDC * sizeof(uint32_t);}

        SERIAL_H(prep(pixcfg, Fei3::nPixLatches * nDC * nWordsPerDC);)
};

class Fei3Mask {
    public:
        enum Fei3MaskType {FEI3_MASK_FULL, FEI3_MASK_32, FEI3_MASK_CLEAR};
        Fei3Mask() {set(FEI3_MASK_CLEAR);}
        Fei3Mask(Fei3MaskType inmask) {set(inmask);}

        void set(Fei3MaskType inmask);
        void shift();
        void shiftBack();

        uint32_t mask[1][Fei3PixelCfg::nWordsPerDC];
};

#endif
