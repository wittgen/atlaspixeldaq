/*
 * Fei3ModCfg
 * MCC configuration + 16 Fei3Cfgs
 * By: Laser Kaplan
 */

#ifndef __FEI3MODCFG_H__
#define __FEI3MODCFG_H__

#include "Fei3Cfg.h"

enum MCCRegisters {CSR = 0, LV1, FEEN, WFE, WMCC, CNT, CAL, PEF, SBSR, WBITD, WRECD};
enum MCCCSR {Single40 = 0x0, Double40 = 0x1, Signle80 = 0x2, Double80 = 0x3, HalfClockEn = 0x20, PlaybackEn = 0x40, DisableBuilder = 0x80};

class Fei3ModCfg : public Fei3Cfg {
    public:
        static const uint8_t nFE = 16;

        Fei3ModCfg();
        Fei3ModCfg(const Fei3ModCfg &rhs);
        Fei3ModCfg & operator=(const Fei3ModCfg &rhs);
        virtual ~Fei3ModCfg() {}

        void dump() const;
        uint32_t getHash() const;
        uint32_t getMCCHash() const;
        std::vector< std::vector<uint32_t> > getFEHashes() const;

        Fei3Cfg m_chipCfgs[nFE];
        // Rx & Tx channels
        uint8_t m_rxCh;
        uint8_t m_txCh;
        // MCC registers
        uint16_t mccRegisters[11];
        // Other values
        uint16_t maskEnableFEConfig;
        uint16_t maskEnableFEScan;
        uint16_t maskEnableFEDacs;
        uint8_t feFlavor;
        uint8_t mccFlavor;
        char idStr[128];
        char tag[128];
        uint32_t revision;
        uint8_t active;
        uint8_t moduleIdx;
        uint8_t groupId;

        SERIAL_H(prep(m_chipCfgs, nFE); prep(m_rxCh); prep(m_txCh); prep(mccRegisters, 11);
                 prep(maskEnableFEConfig); prep(maskEnableFEScan); prep(maskEnableFEDacs);
                 prep(feFlavor); prep(mccFlavor); prep(idStr, 128); prep(tag, 128); prep(revision);
                 prep(active); prep(moduleIdx); prep(groupId);
		)
};

#endif
