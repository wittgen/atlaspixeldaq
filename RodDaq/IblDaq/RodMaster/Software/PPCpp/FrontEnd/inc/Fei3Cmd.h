/*
 * Fei3Cmd
 * Fei3 commands as described in the Fei3 documentation
 * This class is used for getting data streams for the MCC to send
 * Methods return arrays of uint32_t words to write
 * By: Laser Kaplan
 */

#ifndef __FEI3CMD_H__
#define __FEI3CMD_H__

#include "TxLink.h"
#include "Fei3PixelCfg.h"
#include "Fei3GlobalCfg.h"

enum CommandRegisters {
    ReadPixel   = 0x800000,
    WriteKill   = 0x400000,
    WriteFDAC2  = 0x200000,
    WriteFDAC1  = 0x100000,
    WriteFDAC0  = 0x080000,
    WriteTDAC6  = 0x040000,
    WriteTDAC5  = 0x020000,
    WriteTDAC4  = 0x010000,
    WriteTDAC3  = 0x008000,
    WriteTDAC2  = 0x004000,
    WriteTDAC1  = 0x002000,
    WriteTDAC0  = 0x001000,
    WriteMask   = 0x000800,
    WriteSelect = 0x000400,
    WriteEnable = 0x000200,
    ClockPixel  = 0x000100,
    ReadGlobal  = 0x000080,
    WriteGlobal = 0x000060,
    ClockGlobal = 0x000010,
    SoftReset   = 0x00000B,
    RefReset    = 0x000002
};

class Fei3Cmd {
    public:
        Fei3Cmd();
        ~Fei3Cmd(){}
  
        static void nullCmd(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void readPixel(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeKill(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeFDAC2(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeFDAC1(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeFDAC0(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC6(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC5(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC4(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC3(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC2(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC1(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeTDAC0(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeMask(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeSelect(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeEnable(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void clockPixel(int chipID, std::vector<uint32_t>& data, uint32_t bitstream[][Fei3PixelCfg::nWordsPerDC], unsigned int nbits, bool bcast = false);
        static void readGlobal(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void writeGlobal(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void clockGlobal(int chipID, std::vector<uint32_t>& data, uint32_t bitstream[], bool bcast = false);
        static void softReset(int chipID, std::vector<uint32_t>& data, bool bcast = false);
        static void refReset(int chipID, std::vector<uint32_t>& data, bool bcast = false);

        static void genericCommand(int chipID, uint32_t command, std::vector<uint32_t>& data, bool bcast = false);

        static std::vector<uint32_t> readPixel(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; readPixel(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeKill(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeKill(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeFDAC2(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeFDAC2(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeFDAC1(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeFDAC1(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeFDAC0(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeFDAC0(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC6(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC6(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC5(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC5(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC4(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC4(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC3(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC3(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC2(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC2(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC1(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC1(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeTDAC0(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeTDAC0(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeMask(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeMask(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeSelect(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeSelect(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeEnable(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeEnable(chipID, data, bcast); return data; }
//  Not sure what this was supposed to do, but as is it will do nothing but hang in infinte recursion.  Ouch !!!
//         static std::vector<uint32_t> clockPixel(int chipID, uint32_t bitstream[][Fei3PixelCfg::nWordsPerDC], unsigned int nbits, bool bcast = false)
//         { std::vector<uint32_t> data; clockPixel(chipID, bitstream, nbits, bcast); return data; }
        static std::vector<uint32_t> readGlobal(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; readGlobal(chipID, data, bcast); return data; }
        static std::vector<uint32_t> writeGlobal(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; writeGlobal(chipID, data, bcast); return data; }
        static std::vector<uint32_t> clockGlobal(int chipID, uint32_t bitstream[], bool bcast = false)
        { std::vector<uint32_t> data; clockGlobal(chipID, data, bitstream, bcast); return data; }
        static std::vector<uint32_t> softReset(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; softReset(chipID, data, bcast); return data; }
        static std::vector<uint32_t> refReset(int chipID, bool bcast = false)
        { std::vector<uint32_t> data; refReset(chipID, data, bcast); return data; }

        static std::vector<uint32_t> genericCommand(int chipID, uint32_t command, bool bcast = false)
        { std::vector<uint32_t> data; genericCommand(chipID, command, data, bcast); return data; }

        inline uint32_t reverse(uint32_t x) {
            x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
            x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
            x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
            x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
            return((x >> 16) | (x << 16));
        }
};

#endif
