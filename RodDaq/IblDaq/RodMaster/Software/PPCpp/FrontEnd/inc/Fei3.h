#ifndef __FEI3_H__
#define __FEI3_H__

#ifndef __XMK__
#include "ConstexprMap.h"
using std::string_view_literals::operator""sv;
#endif

namespace Fei3 {
        enum struct PixLatch : int { HitBus = 0, Select, Enable,
                                   TDAC0, TDAC1, TDAC2, TDAC3, TDAC4, TDAC5, TDAC6,
                                   FDAC0, FDAC1, FDAC2, Preamp, NPIXELLATCHES
                                 };

        enum struct PixReg : int  { HitBus = 0, Select, Enable, TDAC, FDAC, Preamp, NPIXELREGS };

        enum struct GlobReg : int { GlobalParity = 0, Latency, SelfTriggerDelay, SelfTriggerWidth, EnableSelfTrigger, EnableHitParity, Select_DO, Select_MonHit, TSI_TSC_Enable, SelectDataPhase, EnableEOEParity, HitBusScaler, MonLeakADC, ARegTrim, EnableARegMeas, ARegMeas, EnableAReg, EnableLVDSReferenceMeas, DRegTrim, EnableDRegMeas, DRegMeas, CapMeasure, EnableCapTest, EnableAnalogOut, TestPixelMUX, EnableVCalMeas, EnableLeakMeas, EnableBufferBoost, EnableCol8, TestDAC_IVDD2, IVDD2, ID, TestDAC_ID, EnableCol7, TestDAC_IP2, IP2, IP, TestDAC_IP, EnableCol6, TestDAC_ITrimTh, ITrimTh, IF, TestDAC_IF, EnableCol5, TestDAC_ITrimIf, ITrimIf, VCal, TestDAC_VCal, EnableCol4, HighInjCapSel, EnableExtInj, TestAnalogRef, EOC_MUX, CEU_Clock, EnableDigInj, EnableCol3, TestDAC_ITH1, ITH1, ITH2, TestDAC_ITH2, EnableCol2, TestDAC_IL, IL, IL2, TestDAC_IL2, EnableCol1, THRMIN, THRDUB, ReadMode, EnableCol0, HitBusEnable, GlobalTDAC, EnableTune, EnableBiasCompensation, EnableIPMonitor, NGLOBALREGISTERS
};

        static int constexpr nPixLatches = static_cast<int>(PixLatch::NPIXELLATCHES);
        static int constexpr nPixRegs = static_cast<int>(PixReg::NPIXELREGS);
        static int constexpr nGlobRegs = static_cast<int>(GlobReg::NGLOBALREGISTERS);
        static int constexpr nPixCol = 18;
        static int constexpr nPixRow = 160;

#ifndef __XMK__
  /* This array holds the global registers we store in the configuration object under a given name (key) 
   * and the enum they refer to (value). */
  //We're not interfacing the following 18 registers from the config:
  //GlobalParity, SelfTriggerDelay, EnableHitParity, SelectDataPhase, EnableEOEParity, HitBusScaler,
  //ARegTrim, EnableARegMeas, ARegMeas, EnableAReg, EnableLVDSReferenceMeas, DRegTrim, EnableDRegMeas, DRegMeas,
  //HitBusEnable, EnableTune, EnableBiasCompensation, EnableIPMonitor
  static constexpr std::array<std::pair<std::string_view, GlobReg>, nGlobRegs-18> globRegNames {
   {{"LATENCY"sv, Fei3::GlobReg::Latency},
    {"DAC_IVDD2"sv, Fei3::GlobReg::IVDD2},
    {"DAC_IP2"sv, Fei3::GlobReg::IP2},
    {"DAC_ID"sv, Fei3::GlobReg::ID},
    {"DAC_IP"sv, Fei3::GlobReg::IP},
    {"DAC_ITRIMTH"sv, Fei3::GlobReg::ITrimTh},
    {"DAC_IF"sv, Fei3::GlobReg::IF},
    {"DAC_ITH1"sv, Fei3::GlobReg::ITH1},
    {"DAC_ITH2"sv, Fei3::GlobReg::ITH2},
    {"DAC_IL"sv, Fei3::GlobReg::IL},
    {"DAC_IL2"sv, Fei3::GlobReg::IL2},
    {"DAC_ITRIMIF"sv, Fei3::GlobReg::ITrimIf},
    //addConfig("DAC_SPARE"sv, },
    {"THRESH_TOT_MINIMUM"sv, Fei3::GlobReg::THRMIN},
    {"THRESH_TOT_DOUBLE"sv, Fei3::GlobReg::THRDUB},
    {"CAP_MEASURE"sv, Fei3::GlobReg::CapMeasure},
    {"GLOBAL_DAC"sv, Fei3::GlobReg::GlobalTDAC},
    {"MUX_TEST_PIXEL"sv, Fei3::GlobReg::TestPixelMUX},
    //addConfig("DAC_MON_LEAK_ADC"sv, },
    {"DAC_VCAL"sv, Fei3::GlobReg::VCal},
    {"WIDTH_SELF_TRIGGER"sv, Fei3::GlobReg::SelfTriggerWidth},
    {"MUX_DO"sv, Fei3::GlobReg::Select_DO},
    {"MUX_MON_HIT"sv, Fei3::GlobReg::Select_MonHit},
    {"MUX_EOC"sv, Fei3::GlobReg::EOC_MUX},
    {"FREQUENCY_CEU"sv, Fei3::GlobReg::CEU_Clock},
    {"MODE_TOT_THRESH"sv, Fei3::GlobReg::ReadMode},
    {"ENABLE_TIMESTAMP"sv, Fei3::GlobReg::TSI_TSC_Enable},
    {"ENABLE_SELF_TRIGGER"sv, Fei3::GlobReg::EnableSelfTrigger},
    //addConfig("SPARE"sv, },
    {"MON_MON_LEAK_ADC"sv, Fei3::GlobReg::MonLeakADC},
    //addConfig("MON_ADC_REF"sv,},
    //addConfig("ENABLE_MON_LEAK"sv, },
    //addConfig("STATUS_MON_LEAK"sv, },
    {"ENABLE_CAP_TEST"sv, Fei3::GlobReg::EnableCapTest},
    {"ENABLE_BUFFER"sv, Fei3::GlobReg::EnableAnalogOut},
    {"ENABLE_VCAL_MEASURE"sv, Fei3::GlobReg::EnableVCalMeas},
    {"ENABLE_LEAK_MEASURE"sv, Fei3::GlobReg::EnableLeakMeas},
    {"ENABLE_BUFFER_BOOST"sv, Fei3::GlobReg::EnableBufferBoost},
    {"ENABLE_CP8"sv, Fei3::GlobReg::EnableCol8},
    {"MON_IVDD2"sv, Fei3::GlobReg::TestDAC_IVDD2},
    {"MON_ID"sv, Fei3::GlobReg::TestDAC_ID},
    {"ENABLE_CP7"sv, Fei3::GlobReg::EnableCol7},
    {"MON_IP2"sv, Fei3::GlobReg::TestDAC_IP2},
    {"MON_IP"sv, Fei3::GlobReg::TestDAC_IP},
    {"ENABLE_CP6"sv, Fei3::GlobReg::EnableCol6},
    {"MON_ITRIMTH"sv, Fei3::GlobReg::TestDAC_ITrimTh},
    {"MON_IF"sv, Fei3::GlobReg::TestDAC_IF},
    {"ENABLE_CP5"sv, Fei3::GlobReg::EnableCol5},
    {"MON_ITRIMIF"sv, Fei3::GlobReg::TestDAC_ITrimIf},
    {"MON_VCAL"sv, Fei3::GlobReg::TestDAC_VCal},
    {"ENABLE_CP4"sv, Fei3::GlobReg::EnableCol4},
    {"ENABLE_CINJ_HIGH"sv, Fei3::GlobReg::HighInjCapSel},
    {"ENABLE_EXTERNAL"sv, Fei3::GlobReg::EnableExtInj},
    {"ENABLE_TEST_ANALOG_REF"sv, Fei3::GlobReg::TestAnalogRef},
    {"ENABLE_DIGITAL"sv, Fei3::GlobReg::EnableDigInj},
    {"ENABLE_CP3"sv, Fei3::GlobReg::EnableCol3},
    {"MON_ITH1"sv, Fei3::GlobReg::TestDAC_ITH1},
    {"MON_ITH2"sv, Fei3::GlobReg::TestDAC_ITH2},
    {"ENABLE_CP2"sv, Fei3::GlobReg::EnableCol2},
    {"MON_IL"sv, Fei3::GlobReg::TestDAC_IL},
    {"MON_IL2"sv, Fei3::GlobReg::TestDAC_IL2},
    {"ENABLE_CP1"sv, Fei3::GlobReg::EnableCol1},
    {"ENABLE_CP0"sv, Fei3::GlobReg::EnableCol0}}
    //addConfig("MON_SPARE"sv,},
  };
  //Check that the entire array is filled during compilation
  static_assert(!(globRegNames[globRegNames.size()-1].first.empty()));

  /* This array holds the pixel registers we store in the configuration object under a given name (key) 
   * and the enum they refer to (value). */ 
  static constexpr std::array<std::pair<std::string_view, PixReg>, nPixRegs> pixRegNames {
   {{"HITBUS"sv, Fei3::PixReg::HitBus},
    {"SELECT"sv, Fei3::PixReg::Select},
    {"PREAMP"sv, Fei3::PixReg::Preamp},
    {"ENABLE"sv, Fei3::PixReg::Enable},
    {"TDAC"sv, Fei3::PixReg::TDAC},
    {"FDAC"sv, Fei3::PixReg::FDAC}}
  };
  static_assert(!(pixRegNames[pixRegNames.size()-1].first.empty()));

  /* From a register name (like it appears in the database) get the corresponding global register 
   * Throws a std::range_error if not found */
  static inline auto globreg_name_lookup(const std::string_view sv) {
    static constexpr auto map =
		ConstexprMap<std::string_view, GlobReg, globRegNames.size()>{{globRegNames}};
    return map.at(sv);
  }

  /* From a register name (like it appears in the database) get the corresponding pixel register 
   * Throws a std::range_error if not found */
  static inline auto pixreg_name_lookup(const std::string_view sv) {
    static constexpr auto map = 
		ConstexprMap<std::string_view, PixReg, pixRegNames.size()>{{pixRegNames}};
    return map.at(sv);
  }

  /* Holds the size (in bits) as the value of all the global registers (key)*/
  static constexpr std::array<std::pair<GlobReg, std::size_t>, nGlobRegs> globRegSizes {
   {{/*"LATENCY",*/ Fei3::GlobReg::Latency, 8},
    {/*"DAC_IVDD2",*/ Fei3::GlobReg::IVDD2, 8},
    {/*"DAC_IP2",*/ Fei3::GlobReg::IP2, 8},
    {/*"DAC_ID",*/ Fei3::GlobReg::ID, 8},
    {/*"DAC_IP",*/ Fei3::GlobReg::IP, 8},
    {/*"DAC_ITRIMTH",*/ Fei3::GlobReg::ITrimTh, 8},
    {/*"DAC_IF",*/ Fei3::GlobReg::IF, 8},
    {/*"DAC_ITH1",*/ Fei3::GlobReg::ITH1, 8},
    {/*"DAC_ITH2",*/ Fei3::GlobReg::ITH2, 8},
    {/*"DAC_IL",*/ Fei3::GlobReg::IL, 8},
    {/*"DAC_IL2",*/ Fei3::GlobReg::IL2, 8},
    {/*"DAC_ITRIMIF",*/ Fei3::GlobReg::ITrimIf, 8},
    //addConfig("DAC_SPARE",*/ },
    {/*"THRESH_TOT_MINIMUM",*/ Fei3::GlobReg::THRMIN, 8},
    {/*"THRESH_TOT_DOUBLE",*/ Fei3::GlobReg::THRDUB, 8},
    {/*"CAP_MEASURE",*/ Fei3::GlobReg::CapMeasure, 6},
    {/*"GLOBAL_DAC",*/ Fei3::GlobReg::GlobalTDAC, 5},
    {/*"MUX_TEST_PIXEL",*/ Fei3::GlobReg::TestPixelMUX, 2},
    //addConfig("DAC_MON_LEAK_ADC",*/ },
    {/*"DAC_VCAL",*/ Fei3::GlobReg::VCal, 10},
    {/*"WIDTH_SELF_TRIGGER",*/ Fei3::GlobReg::SelfTriggerWidth, 4},
    {/*"MUX_DO",*/ Fei3::GlobReg::Select_DO, 4},
    {/*"MUX_MON_HIT",*/ Fei3::GlobReg::Select_MonHit, 4},
    {/*"MUX_EOC",*/ Fei3::GlobReg::EOC_MUX, 2},
    {/*"FREQUENCY_CEU",*/ Fei3::GlobReg::CEU_Clock, 2},
    {/*"MODE_TOT_THRESH",*/ Fei3::GlobReg::ReadMode, 2},
    {/*"ENABLE_TIMESTAMP",*/ Fei3::GlobReg::TSI_TSC_Enable, 1},
    {/*"ENABLE_SELF_TRIGGER",*/ Fei3::GlobReg::EnableSelfTrigger, 1},
    //addConfig("SPARE",*/ },
    {/*"MON_MON_LEAK_ADC",*/ Fei3::GlobReg::MonLeakADC, 1},
    //addConfig("MON_ADC_REF",*/},
    //addConfig("ENABLE_MON_LEAK",*/ },
    //addConfig("STATUS_MON_LEAK",*/ },
    {/*"ENABLE_CAP_TEST",*/ Fei3::GlobReg::EnableCapTest, 1},
    {/*"ENABLE_BUFFER",*/ Fei3::GlobReg::EnableAnalogOut, 1},
    {/*"ENABLE_VCAL_MEASURE",*/ Fei3::GlobReg::EnableVCalMeas, 1},
    {/*"ENABLE_LEAK_MEASURE",*/ Fei3::GlobReg::EnableLeakMeas, 1},
    {/*"ENABLE_BUFFER_BOOST",*/ Fei3::GlobReg::EnableBufferBoost, 1},
    {/*"ENABLE_CP8",*/ Fei3::GlobReg::EnableCol8, 1},
    {/*"MON_IVDD2",*/ Fei3::GlobReg::TestDAC_IVDD2, 1},
    {/*"MON_ID",*/ Fei3::GlobReg::TestDAC_ID, 1},
    {/*"ENABLE_CP7",*/ Fei3::GlobReg::EnableCol7, 1},
    {/*"MON_IP2",*/ Fei3::GlobReg::TestDAC_IP2, 1},
    {/*"MON_IP",*/ Fei3::GlobReg::TestDAC_IP, 1},
    {/*"ENABLE_CP6",*/ Fei3::GlobReg::EnableCol6, 1},
    {/*"MON_ITRIMTH",*/ Fei3::GlobReg::TestDAC_ITrimTh, 1},
    {/*"MON_IF",*/ Fei3::GlobReg::TestDAC_IF, 1},
    {/*"ENABLE_CP5",*/ Fei3::GlobReg::EnableCol5, 1},
    {/*"MON_ITRIMIF",*/ Fei3::GlobReg::TestDAC_ITrimIf, 1},
    {/*"MON_VCAL",*/ Fei3::GlobReg::TestDAC_VCal, 1},
    {/*"ENABLE_CP4",*/ Fei3::GlobReg::EnableCol4, 1},
    {/*"ENABLE_CINJ_HIGH",*/ Fei3::GlobReg::HighInjCapSel, 1},
    {/*"ENABLE_EXTERNAL",*/ Fei3::GlobReg::EnableExtInj, 1},
    {/*"ENABLE_TEST_ANALOG_REF",*/ Fei3::GlobReg::TestAnalogRef, 1},
    {/*"ENABLE_DIGITAL",*/ Fei3::GlobReg::EnableDigInj, 1},
    {/*"ENABLE_CP3",*/ Fei3::GlobReg::EnableCol3, 1},
    {/*"MON_ITH1",*/ Fei3::GlobReg::TestDAC_ITH1, 1},
    {/*"MON_ITH2",*/ Fei3::GlobReg::TestDAC_ITH2, 1},
    {/*"ENABLE_CP2",*/ Fei3::GlobReg::EnableCol2, 1},
    {/*"MON_IL",*/ Fei3::GlobReg::TestDAC_IL, 1},
    {/*"MON_IL2",*/ Fei3::GlobReg::TestDAC_IL2, 1},
    {/*"ENABLE_CP1",*/ Fei3::GlobReg::EnableCol1, 1},
    {/*"ENABLE_CP0",*/ Fei3::GlobReg::EnableCol0, 1},
    //addConfig("MON_SPARE",*/},
    //The additional 18 we don't really use within the host, but knowing their size
    //doesn't hurt
    {Fei3::GlobReg::GlobalParity, 1},
    {Fei3::GlobReg::SelfTriggerDelay, 4},
    {Fei3::GlobReg::EnableHitParity, 1},
    {Fei3::GlobReg::SelectDataPhase, 1},
    {Fei3::GlobReg::EnableEOEParity, 1},
    {Fei3::GlobReg::HitBusScaler, 8},
    {Fei3::GlobReg::ARegTrim, 2},
    {Fei3::GlobReg::EnableARegMeas, 1},
    {Fei3::GlobReg::ARegMeas, 2},
    {Fei3::GlobReg::EnableAReg, 1},
    {Fei3::GlobReg::EnableLVDSReferenceMeas, 1},
    {Fei3::GlobReg::DRegTrim, 2},
    {Fei3::GlobReg::EnableDRegMeas, 1},
    {Fei3::GlobReg::DRegMeas, 2},
    {Fei3::GlobReg::HitBusEnable, 1},
    {Fei3::GlobReg::EnableTune, 1},
    {Fei3::GlobReg::EnableBiasCompensation, 1},
    {Fei3::GlobReg::EnableIPMonitor, 1}}
  };

  /* Holds the size (in bits) as the value of all the pixel registers (key)*/
  static constexpr std::array<std::pair<PixReg, std::size_t>, nPixRegs> pixRegSizes {
   {{/*"HITBUS",*/ Fei3::PixReg::HitBus, 1},
    {/*"SELECT",*/ Fei3::PixReg::Select, 1},
    {/*"PREAMP",*/ Fei3::PixReg::Preamp, 1},
    {/*"ENABLE",*/ Fei3::PixReg::Enable, 1},
    {/*"TDAC",*/ Fei3::PixReg::TDAC, 8},
    {/*"FDAC",*/ Fei3::PixReg::FDAC, 3}}
  };

  /* From a global register (the enum) get the corresponding size of the register (in bits)
   * Throws a std::range_error if not found */
  static inline auto globreg_size_lookup(const GlobReg reg) {
    static constexpr auto map =
		ConstexprMap<GlobReg, std::size_t, globRegSizes.size()>{{globRegSizes}};
    return map.at(reg);
  }

  /* From a pixel register (the enum) get the corresponding size of the register (in bits)
   * Throws a std::range_error if not found */
  static inline auto pixreg_size_lookup(const PixReg reg) {
    static constexpr auto map = 
		ConstexprMap<PixReg, std::size_t, pixRegSizes.size()>{{pixRegSizes}};
    return map.at(reg);
  }
#endif

}
#endif
