/*
Steve Alkire <alkire@cern.ch> 2013.11.05

Classes:
Fei4PixelCfg
.DoubleColumnBitField
..DoubleColumnBit
.Mask: DoubleColumnBit

usage:
e.g: Fei4PixelCfg fe;
     Mask myMask(MASK_QUARTER);
     DoubleColumnBit myDoubleColumn = (DoubleColumnBit)myMask;
     myDoubleColumn.setPixel(234,1);
     *outputEnable[23] = myDoubleColumn;
     *TDAC[25].dc[3] = myDoubleColumn;
     etc..

*/
#ifndef __FEI4PIXELCFG_H__
#define __FEI4PIXELCFG_H__

#include<stdint.h>
#include <cmath>
#include<iostream>
#include "EasySerializable.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

//Holds for an entire double column one of the 13 pixel configuration bits, stored as array of 21*uint32_t.
//Contains the usual get/set stuff with various overloading.
//Translating to and from the row/column description of bit position to the position in the array is done
//with rowColumnToArrayBit, etc..
//To apply a mask to a bit configuration use the & operator with a Mask which is child of this class.

class DoubleColumnBit : public EasySerializable{
 public:
  const static unsigned nWords = 21;
 protected:
  uint32_t store[nWords];
 public:
  DoubleColumnBit();
  DoubleColumnBit(const DoubleColumnBit& rhs);
  DoubleColumnBit operator^(const DoubleColumnBit& rhs);

  void dump() const{for(unsigned i = 0; i < nWords; ++i) std::cout << HEX(store[i]) << std::endl;}

  void set(const uint32_t& word);//todo rename set -> setWord?
  void set(const uint32_t allWords[nWords]);
  void set(const DoubleColumnBit& dcb);
  void setPixel(const unsigned& n, const unsigned& val);//todo should val really be unsigned? bool instead? 
  void setPixel(const unsigned& row,const unsigned& column, const unsigned& val);
  unsigned getPixel(const unsigned& n) const;
  unsigned getPixel(const unsigned& row, const unsigned& column) const;
  uint32_t getWord(const unsigned& word) const {return store[word];}
  DoubleColumnBit operator&(const DoubleColumnBit& dc) const; //used for applying a mask
  uint32_t* const getDoubleColumnBit();//todo change name, maybe get raw dcb or something?
  uint32_t const * const getStore () const { return store; }

  bool operator==(const DoubleColumnBit & rhs){
    bool areEqual = true;
    for(unsigned i_word = 0; i_word < nWords; ++i_word){
      areEqual = areEqual & ((*this).getWord(i_word) == rhs.getWord(i_word) );
    }
    return areEqual;
  }

  static unsigned rowColumnToArrayBit(const unsigned& r, const unsigned& c);
  static unsigned arrayBitToRow(const unsigned& n); //To-do
  static unsigned arrayBitToColumn(const unsigned& n); //To-do
  SERIAL_H(prep(store,nWords);)
};

//Just a DoubleColumnBit with some added constructors and preset masks for convenience.
//To apply to given doubleColumnBit config just use & operator
//Also, use the shift/shiftBack to increment the the mask positions.
enum MaskType{MASK_FULL, MASK_HALF, MASK_QUARTER, MASK_EIGHTH, MASK_16, MASK_32, MASK_672, MASK_26880,MASK_CLEAR};
class Mask:public DoubleColumnBit{
 public:
  using DoubleColumnBit::set;
  void set(const MaskType& mask);
  void setXTALK(const MaskType& mask);
  Mask(){set(0xFFFFFFFF);}
  explicit Mask(const uint32_t& maskCode){set(maskCode);}
  explicit Mask(const uint32_t wholeMaskCode[nWords]){set(wholeMaskCode);}
  explicit Mask(const MaskType& mask){set(mask);}
  void shift();
  void shiftBack();
};

//For the multibit fields in the 13 pixel config bits this class associates N DoubleColumnBits.
//The shift* functions are equivalent to <</>> operators for the bit field.
//twice/half are used for shifting like shift* but takes into account whether MSB is on left or right
//getDoubleColumn bit returns doubleCoulumnBit for a single bit.
template<unsigned N, bool msbRight>
class DoubleColumnBitField{
 public:
  DoubleColumnBit* dcb;
   
  DoubleColumnBit operator[](const unsigned& i) const{return dcb[i];}
  DoubleColumnBit& operator[](const unsigned& i) {return dcb[i];}
 
  void dump(const unsigned& b) const{(dcb+b)->dump();}

  DoubleColumnBitField(){}//todo if you shouldn't make a standalone dcbf, do we even want to offer this constructor?
  void setPointer(DoubleColumnBit* dcrhs){dcb = dcrhs;} //use with care!
  DoubleColumnBitField(DoubleColumnBit* dcrhs){setPointer(dcrhs);} 
  void shiftLeft(const unsigned& n){
    for(unsigned i = N-1; i > 0; --i)   dcb[i].setPixel(n, dcb[i-1].getPixel(n));
    dcb[0].setPixel(n,(unsigned)0);
  }
  void shiftRight(const unsigned& n){
    for(unsigned i = 1; i < N; ++i)  dcb[i-1].setPixel(n, dcb[i].getPixel(n));
    dcb[N-1].setPixel(n,(unsigned)0);
  }
  void twice(const unsigned& n){//this checks if msb is 1. If so, we set to the maximum value
    if(msbRight){
      if(dcb[N-1].getPixel(n)==0){  shiftRight(n); }
      else{
	for(unsigned i = 0; i < N-1; ++i) dcb[i].setPixel(n,1); //set all bits to one
	std::cout << "You have tried to double above the maximum value.  Setting to the maximum value of 2^N-1="<< std::hex<< pow(2,N) -1 << std::dec << std::endl;  
       }//todo maybe more standardized 
    }else{
      if(dcb[0].getPixel(n)==0){shiftLeft(n);}
      else{
	for(unsigned i = 0; i < N-1; ++i) dcb[i].setPixel(n,1); //set all bits to one
	std::cout << "You have tried to double above the maximum value.  Setting to the maximum value of 2^N-1="<< std::hex<< pow(2,N) -1  << std::dec << std::endl;  
      }
    }
  }
  void half(const unsigned& n){msbRight ? shiftLeft(n) : shiftRight(n);} 
  void twice(const unsigned& row, const unsigned& column){twice(DoubleColumnBit::rowColumnToArrayBit(row,column));}
  void half(const unsigned& row, const unsigned& column){half(DoubleColumnBit::rowColumnToArrayBit(row,column));}
  void setPixel(const unsigned& n, unsigned val){for(unsigned i = 0; i < N; i++){dcb[msbRight ? (N-i-1): i].setPixel(n,val); val>>=1;}}
  void setPixel(const unsigned& row, const unsigned& column, const unsigned& val){setPixel(DoubleColumnBit::rowColumnToArrayBit(row,column), val);}
  unsigned getPixel(const unsigned& n) const{
    unsigned out = 0;
    if(msbRight) for(unsigned i = 0; i < N; ++i){ out<<=1; out = out|dcb[i].getPixel(n);}
    else for(unsigned i = N; i > 0; --i){ out<<=1; out = out|dcb[i-1].getPixel(n);}
    return out;
  }
  unsigned getPixel(const unsigned& row, const unsigned& column) const{return getPixel(DoubleColumnBit::rowColumnToArrayBit(row,column));}

  void set(const unsigned& b, const uint32_t& word){dcb[b].set(word);}
  void set(const unsigned& b, const uint32_t allWords[DoubleColumnBit::nWords]){dcb[b].set(allWords);}
  void set(const DoubleColumnBitField<N , msbRight>& rhs){for (unsigned i = 0; i < N ; i++) dcb[i] = rhs.dcb[i];}
};

//Holds the 13bits/pixel configuration for an entire chip.
class Fei4PixelCfg: virtual public EasySerializable{
 public:
   const static unsigned n_DC = 40;
  //  const static unsigned n_DC = 1;
  const static unsigned n_Bit = 13;
 private:
  DoubleColumnBit m_dcb[n_DC*n_Bit];//n_bit has to be on the inside or BitFields are wrong   
  DoubleColumnBitField<5, true> m_TDAC[n_DC];//doing it this way for consistency of access to bit and bit field
  DoubleColumnBitField<4, false> m_FDAC[n_DC];
  DoubleColumnBitField<13, false> m_all[n_DC];
 public:  
  inline DoubleColumnBit& dcb(const unsigned& dc,const unsigned& cfgBit)       {return m_dcb[dc*n_Bit + cfgBit];}
  inline const DoubleColumnBit&  dcb(const unsigned& dc,const unsigned& cfgBit) const {return m_dcb[dc*n_Bit + cfgBit];}
  DoubleColumnBitField<13, false>& all(const unsigned& dc){return m_all[dc];} //MSB left

  DoubleColumnBit& outputEnable(const unsigned& dc)    {return m_dcb[dc*n_Bit      ];}
  DoubleColumnBitField<5, true>& TDAC(const unsigned& dc){return m_TDAC[dc];} //MSB right
  DoubleColumnBit& largeCapacitor(const unsigned& dc)  {return m_dcb[dc*n_Bit+6    ];}
  DoubleColumnBit& smallCapacitor(const unsigned& dc)  {return m_dcb[dc*n_Bit+7    ];}
  DoubleColumnBit& hitBusOut(const unsigned& dc)       {return m_dcb[dc*n_Bit+8    ];}
  DoubleColumnBitField<4, false>& FDAC(const unsigned& dc){return m_FDAC[dc];} //MSB left

  Fei4PixelCfg();
  Fei4PixelCfg(const Fei4PixelCfg& rhs);
  Fei4PixelCfg& operator=( const Fei4PixelCfg& rhs);
  bool          operator==(const Fei4PixelCfg& rhs);
  Fei4PixelCfg  operator^( const Fei4PixelCfg& rhs);

  void setPointers();
  void dumpPixel(int dc);
  const std::size_t size() const {return DoubleColumnBit::nWords * sizeof(uint32_t) * n_Bit * n_DC;}

  uint32_t getPixelRegHash() const {
    uint32_t hash = 0;
    size_t totalDCregisters = n_DC*n_Bit;
    for(size_t i = 0; i < totalDCregisters; i++){
        for(size_t j = 0; j < DoubleColumnBit::nWords; j++) {
            hash ^= m_dcb[i].getStore()[j];
        }
    }
    return hash;
  }

  SERIAL_H(prep(m_dcb , n_Bit* n_DC) ; )
};

class TestFei4PixelCfg{
 public:
  static void test();
};

#endif //__FEI4PIXELCFG_H__
