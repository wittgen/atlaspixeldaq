/*
 * Author: B. Axen <bradley.axen@cern.ch>
 * Date: 3-July-2014
 * 
 * Static utility class to handle reading/recording
 * data from the Fei4. 
 */
#ifndef __FEI4_DATA_H__
#define __FEI4_DATA_H__

#include <vector>
#include <map>
#include <string>

#ifndef __XMK__
#include <cstdint>
#endif

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

class RxLink;
class Fei4Hist;

class Fei4Data {
 public:
  // Types used by external classes.
  enum DataType {OCC, TOT};
  enum FIFOType {BOC, INMEM};

  // Utility
  static const std::string& serviceRecordString(int i) {return m_serviceRecordMap.find(i)->second;}
  static uint8_t decodeToT(uint8_t tot, uint8_t hitdisccnf);
  static void printServiceRecord(uint8_t byte1, uint8_t byte2);
  // Returns a vector<int> from the data frame returned by an RxLink 
  static std::vector<int> readFrame(RxLink& link, bool raw = false, bool shiftRegisterReadBack = false);
  // fills an array of 4 column x 336 pixel ToTs starting with the startCol provided
  static void fillPixDataArray(const std::vector<int>& frame, int HitDiscCnf,  uint8_t tot[4][336], int startCol = 0);
  // fills an array of 80 column x 336 pixel with occupancy
  static void fillPixDataArray(const std::vector<int>& frame, int HitDiscCnf,  uint16_t occ[80][336]);
  // fills a Fei4Data::Hist with pixel data
  static void fillPixDataHist(const std::vector<int>& frame, int HitDiscCnf, Fei4Hist& hist);
  // Prints pixel data directly
  static void printPixData(const std::vector<int>& frame, int HitDiscCnf);

  // the following two versions are included for backwards compatibility with older debugging tests
  // returns the number of pixel hits in the frame
  static uint32_t decodePixData(const std::vector<int>& frame);

  static inline bool verboseSR() {return m_verboseSR;}

  static void setVerbose(bool flag) {m_verbose = flag;}
  static void setVerboseSR(bool flag) {m_verboseSR = flag;}
  static void setRawDataDump(bool flag) {m_rawDataDump = flag;}
  static void setMaxRawDataDump(unsigned int max) {m_maxRawDataDump = max;}
  static void setMaxFrameSize(unsigned int max) {m_maxFrameSize = max;}
  static void setMaxStartFrameCount(unsigned int max) {m_maxStartFrameCount = max;}

  static bool m_error;
  static const std::map<int, std::string> m_serviceRecordMap;

 private:  

  static bool m_verbose;
  static bool m_verboseSR;
  static bool m_rawDataDump;
  static unsigned int m_maxStartFrameCount;
  static unsigned int m_maxRawDataDump;
  static unsigned int m_maxFrameSize;

  static bool readDataRecord(RxLink& link, std::vector<int>& rawData, std::vector<int>& answer);
  static bool readPixelSRData(RxLink& link, std::vector<int>& rawData, std::vector<int>& answer);
  
  static void dumpRawData(RxLink& link, std::vector<int> rawData, int frameCount, bool goodFrame);
};  

class Fei4Hist {
 public:
  static const unsigned ncol;
  static const unsigned nrow;
  Fei4Hist(const Fei4Data::DataType d = Fei4Data::TOT, const Fei4Data::FIFOType f = Fei4Data::BOC, const uint8_t rx = 16);
  Fei4Data::DataType data;
  Fei4Data::FIFOType fifo;
  uint8_t rxCh;
  void clear();
  const uint16_t operator () (uint32_t col,uint32_t row) const {
      return m_internal[col+nrow*row];
  }
  uint16_t & operator () (uint32_t col,uint32_t row) {
    return  m_internal[col+nrow*row];
  }

 private:
  std::vector<uint16_t>  m_internal;
};
#endif // __FEI4_DATA_H__
