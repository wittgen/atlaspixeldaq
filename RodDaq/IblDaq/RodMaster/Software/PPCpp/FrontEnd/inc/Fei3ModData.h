/*
 * Fei3ModData
 * Decode data coming back from MCC
 * By: Laser Kaplan
 */

#ifndef __FEI3MODDATA_H__
#define __FEI3MODDATA_H__

#include <vector>
#include <iostream>

#ifndef __XMK__
#include <cstdint>
#endif

class RxLink;

struct Fei3HitData {
    uint32_t l1id    : 8;
    uint32_t bcid    : 8;
    uint32_t fe      : 4;
    uint32_t row     : 8;
    uint32_t col     : 5;
    uint32_t tot     : 8;
    uint32_t mccflag : 8;
    uint32_t feflag  : 8;
};

class Fei3ModData {
    public:
        static std::vector<Fei3HitData> readHitData(RxLink &link, bool raw = false);
        static void readRemainingRawHitData(RxLink &link, std::vector<uint8_t> &rawData);
        static void decodeRawHitData(const std::vector<Fei3HitData>& hd, std::ostream& os = std::cout);
};

#endif
