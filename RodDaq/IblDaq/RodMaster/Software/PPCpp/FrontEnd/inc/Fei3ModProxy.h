/*
 * Fei3ModProxy
 * Proxy class for receiving info from RodResourceManager
 * Inspired from Fei4Proxy
 * By: Laser Kaplan
 *     K. Potamianos <karolos.potamianos@cern.ch>
 * Update: Nick Dreyer 2016-III-12 - Add new functions
 */

#ifndef _FEI3MODPROXY_H_
#define _FEI3MODPROXY_H_

#include "Fei3ModCmd.h"
#include "Fei3ModCfg.h"
#include "RodResourceManager.h"

class Fei3ModProxy : public Fei3ModCmd {
    public:

        // Default constructor sets front end mask to all channels for iterating through FE operations
        // Default constructor doesn't change the Tx mask of the serial port
        // The serial port may already be configured to all enabled channels
        // If you want to change the serial port mask, access directly the serial port
        Fei3ModProxy(uint32_t modMask = 0xFFFFFFFF): m_modMask(modMask) {
            // Todo: make it clear that the setting of the serial port TX mask is done outside this code
            // OR bring it inside this construtor, making it clear that the serial port is being modified
            // An alternative is to use a copy of IblRodSerialPort but this requires locking to avoid 2 serial ports messing with the FIFO in the ROD
            // IMPORTANT NOTE: it is possible for the modMask and the IblRodSerialPort::m_TxMask effectively acting on different sets of modules
            Fei3ModCmd::setTxLink(RodResourceManagerPIX::getSerialPort());
        }
	~Fei3ModProxy(){}

        static void setPreAmpsOn(uint32_t mod_mask, bool ampsOn = true, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->setPreAmpsOn(ampsOn);
                current_mask <<= 1;
                modPtr++;
            }
        }

        // Set default configuration for Fei3Mod objects
        static void setDefaultGlobalConfig(uint32_t mod_mask, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->setDefaultGlobalConfigProxy(calib);
                current_mask <<= 1;
                modPtr++;
            }
        }

        // Send global register to Fei3Mod objects
        static void sendGlobalConfig(uint32_t mod_mask, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->sendGlobalConfigProxy(calib);
                current_mask <<= 1;
                modPtr++;
            } 
        }

        static void strobePixelLatches(uint32_t mod_mask, int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->strobePixelLatchesProxy(iFE, pixReg, bitstream);
                current_mask <<= 1;
                modPtr++;
            }
        }

        static void strobePixelLatchesDC(uint32_t mod_mask, Fei3::PixLatch pixReg, uint32_t bitstream[1][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->strobePixelLatchesDCProxy(calib, pixReg, bitstream);
                current_mask <<= 1;
                modPtr++;
            }
        }

        static void strobePixelLatchesDC_wBC(uint32_t mod_mask,  uint8_t iDC, Fei3::PixLatch pixReg, uint32_t bitstream[1][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->strobePixelLatchesDC_wBCProxy(iDC, calib, pixReg, bitstream);
                current_mask <<= 1;
                modPtr++;
            }
        }

	static void moveMask(uint32_t mod_mask, uint16_t iFE, uint32_t regMask, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	      if( mod_mask & current_mask ){
		modPtr->moveMask(iFE,regMask);
	      }
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

        // Use this command to set the vcal value directory from the desired charge
        // By default, loops over CalibCfg
        static void writeVcalFromCharge(float charge, int capType, uint32_t mod_mask, bool calib = true) {
          Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
          Fei3Mod *modPtr = cfgArray;
          const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
          uint32_t current_mask = 0x1;
          while( modPtr != (cfgArray+nCfg) ) {
            if( mod_mask & current_mask )
              modPtr->writeVcalFromChargeProxy(calib, charge, capType);
            current_mask <<= 1;
            modPtr++;
          }
        }

        // Use this command to set Capicitor Type
        // By default, loops over CalibCfg
        static void setCapType(int capType, uint32_t mod_mask, bool calib = true) {
          Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
          Fei3Mod *modPtr = cfgArray;
          const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
          uint32_t current_mask = 0x1;
          while( modPtr != (cfgArray+nCfg) ) {
            if( mod_mask & current_mask )
              modPtr->setCapTypeProxy(calib, capType);
            current_mask <<= 1;
            modPtr++;
          }
        }

        // Send pixel register for Fei3Mod objects
        static void sendPixelConfig(uint32_t mod_mask, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->sendPixelConfigProxy(calib);
                current_mask <<= 1;
                modPtr++;
            }
        }

        // Write global register value for Fei3Mod objects
        static void writeGlobalReg(Fei3::GlobReg reg, uint16_t val, uint32_t mod_mask, bool calib = true ) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->writeGlobalRegProxy(calib, reg, val);
                current_mask <<= 1;
                modPtr++;
            }
        }

        // Enable or disable all double columns for Fei3Mod objects
        static void setAllPixelShiftRegisters(uint32_t mod_mask, bool val, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->setAllPixelShiftRegistersProxy(val, calib);
                current_mask <<= 1;
                modPtr++;
            }
        }
        
        static void addPattern(uint32_t mod_mask, uint32_t pattern, uint32_t nPattern, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->addPattern(pattern, nPattern);
                current_mask <<= 1;
                modPtr++;
            }
        }

        // Write value to MCC Register for Fei3Mod objects
        static void CALset(uint8_t delayRange, bool overrideRange, uint8_t delay, bool overrideDelay, uint32_t mod_mask, bool calib = true) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (mod_mask & current_mask) modPtr->CALset(delayRange, overrideRange, delay, overrideDelay);
                current_mask <<= 1;
                modPtr++;
            }
        }

        static void sendMCCRegister(uint32_t mod_mask, MCCRegisters reg, uint16_t val, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	    if( mod_mask & current_mask ) modPtr->sendMCCRegister(reg, val);
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

	static void writeMCCRegister(uint32_t mod_mask, MCCRegisters reg, uint16_t val, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	    if( mod_mask & current_mask ) modPtr->writeMCCRegister(reg, val);
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

	static void writeMCCSpeed(uint32_t mod_mask,uint8_t speed, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	    uint16_t mccCSR = modPtr->mccRegisters[CSR] & 0xFFFC;
	    mccCSR |= (speed & 0x3);
	    if( mod_mask & current_mask )modPtr->writeMCCRegister(CSR, mccCSR);
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

	// Do what writeMCCRegister probably really should be doing for the FEEN register?
	static void disableFEs(uint32_t mod_mask, uint16_t FEEN_Mask, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	    if( mod_mask & current_mask ) modPtr->writeMCCRegister(FEEN, FEEN_Mask & (calib ? modPtr->maskEnableFEScan : modPtr->maskEnableFEConfig));
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

	static void resetModules(uint32_t mod_mask, uint32_t syncW, bool calib = true) {
	  Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
	  Fei3Mod *modPtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( modPtr != (cfgArray+nCfg) ) {
	      if( mod_mask & current_mask ){
		modPtr->mccGlobalResetFE(syncW);
		modPtr->mccGlobalResetMCC();
	      }
	    current_mask <<= 1;
	    modPtr++;
	  }
	}

/*        
        // Write pixel register value for Fei3Mod objects (using the fe_mask)
        static void writePixelReg(Fei3::PixLatch bit, uint32_t val[Fei3PixelCfg::nDC][DoubleColumnBitFei3::nWordsPixel], uint32_t fe_mask, bool calib = true ) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (fe_mask & current_mask) modPtr->writePixelRegProxy(calib, bit, val);
                current_mask <<= 1;
                modPtr++;
            } 
        }
        
        // Write pixel register value for Fei3Mod objects (using the fe_mask)
        static void writePixelReg(Fei3::PixLatch bit, int dc, uint32_t val[DoubleColumnBitFei3::nWordsPixel], uint32_t fe_mask, bool calib = true ) {
            Fei3Mod * const cfgArray = calib ? RodResourceManagerPIX::getCalibModuleConfigs() : RodResourceManagerPIX::getPhysicsModuleConfigs();
            Fei3Mod *modPtr = cfgArray;
            const uint8_t nCfg = RodResourceManagerPIX::getNModuleConfigs();
            uint32_t current_mask = 0x1;
            while (modPtr != (cfgArray+nCfg)) {
                if (fe_mask & current_mask) modPtr->writePixelRegProxy(calib, bit, dc, val);
                current_mask <<= 1;
                modPtr++;
            } 
        }
*/
        // The set of functions below calls the static functions above, but using the internal mask (m_modMask).

        void setPreAmpsOn(bool ampsOn = true, bool calib = true) {
            Fei3ModProxy::setPreAmpsOn(m_modMask, ampsOn, calib);
        }

        void sendGlobalConfig(bool calib = true) {
            Fei3ModProxy::sendGlobalConfig(m_modMask, calib);
        }

        void strobePixelLatches(int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3ModProxy::strobePixelLatches(m_modMask, iFE, pixReg, bitstream, calib);
        }

        void strobePixelLatchesDC(Fei3::PixLatch pixReg, uint32_t bitstream[1][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3ModProxy::strobePixelLatchesDC(m_modMask, pixReg, bitstream, calib);
        }

        void strobePixelLatchesDC_wBC(uint8_t iDC, Fei3::PixLatch pixReg, uint32_t bitstream[1][Fei3PixelCfg::nWordsPerDC], bool calib = true) {
            Fei3ModProxy::strobePixelLatchesDC_wBC(m_modMask, iDC, pixReg, bitstream, calib);
        }

	void moveMask (uint16_t iFE, uint32_t regMask, bool calib = true){
	    Fei3ModProxy::moveMask(m_modMask,iFE,regMask,calib);
	}

        void writeVcalFromCharge(float charge, int capType, bool calib = true) {
          Fei3ModProxy::writeVcalFromCharge(charge, capType, m_modMask, calib);
        }

        void setCapType(int capType, bool calib = true) {
          Fei3ModProxy::setCapType(capType, m_modMask, calib);
        }

        void sendPixelConfig(bool calib = true) {
            Fei3ModProxy::sendPixelConfig(m_modMask, calib);
        }

        void writeGlobalReg(Fei3::GlobReg reg, uint16_t val, bool calib = true ) {
            Fei3ModProxy::writeGlobalReg(reg, val, m_modMask, calib);
        }

        void setAllPixelShiftRegisters(bool val, bool calib = true) {
             Fei3ModProxy::setAllPixelShiftRegisters(m_modMask, val, calib);
        }

        void addpattern(bool val, bool calib = true) {
            Fei3ModProxy::setAllPixelShiftRegisters(m_modMask, val,  calib);
        }

        void CALset(uint8_t delayRange, bool overrideRange, uint8_t delay, bool overrideDelay, bool calib = true) {
            Fei3ModProxy::CALset(delayRange, overrideRange, delay, overrideDelay, m_modMask, calib);
        }

	//Write MCC register value on PPC configuration and send
	void writeMCCRegister(MCCRegisters reg, uint16_t val, bool calib = true) {
	  writeMCCRegister(m_modMask, reg, val, calib);
	}

	//Send doesn't write MCC value in the PPC config
	void sendMCCRegister(MCCRegisters reg, uint16_t val, bool calib = true) {
	  sendMCCRegister(m_modMask, reg, val, calib);
	}


	void writeMCCSpeed(uint8_t speed, bool calib = true) {
	  writeMCCSpeed(m_modMask, speed, calib);
	}

	void disableFEs(uint16_t FEEN_Mask, bool calib = true) {
	  disableFEs(m_modMask, FEEN_Mask, calib);
	}

	void resetModules(uint32_t syncW, bool calib = true) {
	  resetModules(m_modMask, syncW, calib);
	}

/*
        void writePixelReg(Fei3::PixLatch bit, uint32_t val[Fei3PixelCfg::nDC][DoubleColumnBitFei3::nWordsPixel], bool calib = true ) {
            Fei3ModProxy::writePixelReg(bit, val, m_modMask, calib);
        }

        void writePixelReg(Fei3::PixLatch bit, int dc, uint32_t val[DoubleColumnBitFei3::nWordsPixel], bool calib = true ) {
            Fei3ModProxy::writePixelReg(bit, dc, val, m_modMask, calib);
        }
*/
        // Getters and setters
        void setFeMask(uint32_t feM) {m_modMask = feM;};
        uint32_t getFeMask() {return m_modMask;};

    private:
        uint32_t m_modMask; ///< FE mask used for operations
};

#endif
