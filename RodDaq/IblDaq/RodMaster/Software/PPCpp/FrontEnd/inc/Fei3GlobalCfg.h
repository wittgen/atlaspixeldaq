/* 
 * Fei3GlobalCfg
 * Global registers for Fei3
 * By: Laser Kaplan
 */

#ifndef __FEI3GLOBALCFG_H__
#define __FEI3GLOBALCFG_H__

#include "EasySerializable.h"
#include <stdint.h>
#include <iostream>
#include <math.h>
#include "Fei3.h"

namespace Fei3GlobalMaps {
    const static int posMap[Fei3::nGlobRegs] = {0, 1, 9, 13, 17, 18, 19, 23, 27, 28, 29, 30, 38, 52, 54, 55, 57, 58, 59, 61, 62, 64, 70, 71, 72, 74, 75, 76, 77, 78, 79, 87, 95, 96, 97, 98, 106, 114, 115, 116, 117, 125, 133, 134, 135, 136, 144, 154, 155, 156, 157, 158, 159, 161, 163, 164, 165, 166, 174, 182, 183, 184, 185, 193, 201, 202, 203, 211, 219, 221, 222, 223, 228, 229, 230};
    const static int lenMap[Fei3::nGlobRegs] = {1, 8, 4, 4, 1, 1, 4, 4, 1, 1, 1, 8, 14, 2, 1, 2, 1, 1, 2, 1, 2, 6, 1, 1, 2, 1, 1, 1, 1, 1, 8, 8, 1, 1, 1, 8, 8, 1, 1, 1, 8, 8, 1, 1, 1, 8, 10, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 8, 8, 1, 1, 1, 8, 8, 1, 1, 8, 8, 2, 1, 1, 5, 1, 1, 1};
    const static std::string globRegNames[Fei3::nGlobRegs] = { "GlobalParity", "Latency", "SelfTriggerDelay", "SelfTriggerWidth", "EnableSelfTrigger", "EnableHitParity", "Select_DO", "Select_MonHit", "TSI_TSC_Enable", "SelectDataPhase", "EnableEOEParity", "HitBusScaler", "MonLeakADC", "ARegTrim", "EnableARegMeas", "ARegMeas", "EnableAReg", "EnableLVDSReferenceMeas", "DRegTrim", "EnableDRegMeas", "DRegMeas", "CapMeasure", "EnableCapTest", "EnableAnalogOut", "TestPixelMUX", "EnableVCalMeas", "EnableLeakMeas", "EnableBufferBoost", "EnableCol8", "TestDAC_IVDD2", "IVDD2", "ID", "TestDAC_ID", "EnableCol7", "TestDAC_IP2", "IP2", "IP", "TestDAC_IP", "EnableCol6", "TestDAC_ITrimTh", "ITrimTh", "IF", "TestDAC_IF", "EnableCol5", "TestDAC_ITrimIf", "ITrimIf", "VCal", "TestDAC_VCal", "EnableCol4", "HighInjCapSel", "EnableExtInj", "TestAnalogRef", "EOC_MUX", "CEU_Clock", "EnableDigInj", "EnableCol3", "TestDAC_ITH1", "ITH1", "ITH2", "TestDAC_ITH2", "EnableCol2", "TestDAC_IL", "IL", "IL2", "TestDAC_IL2", "EnableCol1", "THRMIN", "THRDUB", "ReadMode", "EnableCol0", "HitBusEnable", "GlobalTDAC", "EnableTune", "EnableBiasCompensation", "EnableIPMonitor" };

}


class Fei3GlobalCfg : virtual public EasySerializable {
    public:

        Fei3GlobalCfg();
        Fei3GlobalCfg(const Fei3GlobalCfg &rhs);
        Fei3GlobalCfg & operator=(const Fei3GlobalCfg &rhs);
        virtual ~Fei3GlobalCfg() {}

        const static unsigned nWordsGlobal = 8;

        // 18 rows of 160 pixels, accessible via 9 double columns of 320 pixels
        static const Fei3::GlobReg colEnReg(uint8_t idx) {
          static const Fei3::GlobReg colEnReg[] = { Fei3::GlobReg::EnableCol0, Fei3::GlobReg::EnableCol1,
                                                    Fei3::GlobReg::EnableCol2, Fei3::GlobReg::EnableCol3,
                                                    Fei3::GlobReg::EnableCol4, Fei3::GlobReg::EnableCol5,
                                                    Fei3::GlobReg::EnableCol6, Fei3::GlobReg::EnableCol7,
                                                    Fei3::GlobReg::EnableCol8 };
          return colEnReg[idx];
        }

        void writeRegGlobal(Fei3::GlobReg reg, uint32_t val);
        void dumpFei3GlobalCfg();

        void setWord(size_t index, uint32_t value ){
        if (index<nWordsGlobal)globcfg[index] = value;
        }

        bool operator==(const Fei3GlobalCfg &comp) const;

        uint32_t readRegGlobal(Fei3::GlobReg reg) const;
        uint32_t getGlobalRegHash() const;

        uint32_t globcfg[nWordsGlobal];

        const std::size_t size() const {return nWordsGlobal * sizeof(uint32_t);}

        SERIAL_H(prep(globcfg,nWordsGlobal);)
};
#endif
