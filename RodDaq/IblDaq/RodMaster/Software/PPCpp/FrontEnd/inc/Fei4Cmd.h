/*
 * Author: L. Jeanty <laura.jeanty@cern.ch>
 * Date: 15-Oct-2013
 * Description: Class of slow and fast commands for the Fei4 front end
 *              Fei4Cmd needs a Tx Link to send its commands to the front end
 *              Implemented from the Fei4 Integrated Circuit Guide
 *              Fei4 and FeGroup inherit from this class 
 */

#ifndef __FEI4Cmd_H__
#define __FEI4Cmd_H__

#include "SerialPort.h"
#include "FeLink.h"

class Fei4Cmd {

 public:

  Fei4Cmd();
  Fei4Cmd(TxLink * link);
  ~Fei4Cmd() {;}
 
  // default constructor links TxLink 
  void setTxLink(TxLink * link) { m_Txlink = link; }
  TxLink* getTxLink(){ return m_Txlink; }

  void setVerbose(bool v = true){verbose = v;}

  // Commands that can be directly issued to the FEI4 chips:

  // FEI4 fast cmds
  void trigger();
  void bcr();
  void ecr();
  void cal();

  // FEI4 slow cmds
  // These take chip ID as a parameter
  // overwritten in FE and FEgroup to send chipID
  void wrRegister(int chipID, int address, int value);
  void rdRegister(int chipID, int address);
  void wrFrontEnd(int chipID, uint32_t bitstream[21]);
  void runMode(int chipID, bool mode);
  void globalReset(int chipID);
  void globalPulse(int chipID, int width);

  // these prep commands write to the txlink but do not send
  void prepFrontEnd(int chipID, uint32_t bitstream[21]);
  void prepRegister(int chipID, int address, int value);
  void prepPulse(int chipID, int width);
  // send command sends the data stored in the txlink
  void send();

  // FEI4 combined commands
  void calTrigger(int delay);

  static inline uint8_t reverse8(uint8_t x) {
    return (Fei4Cmd::reverse(x)>>24);
  }
  static inline uint32_t reverse(uint32_t x) {
    x = (((x & 0xaaaaaaaa) >> 1) | ((x & 0x55555555) << 1));
    x = (((x & 0xcccccccc) >> 2) | ((x & 0x33333333) << 2));
    x = (((x & 0xf0f0f0f0) >> 4) | ((x & 0x0f0f0f0f) << 4));
    x = (((x & 0xff00ff00) >> 8) | ((x & 0x00ff00ff) << 8));
    return((x >> 16) | (x << 16));
  }
  
 protected:
  bool verbose;

 private:
  TxLink * m_Txlink; // write functionality
};

#endif
