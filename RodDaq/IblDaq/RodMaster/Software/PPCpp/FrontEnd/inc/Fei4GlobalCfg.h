#ifndef __FEI4GLOBALCFG_H__
#define __FEI4GLOBALCFG_H__
/*
  Steve Alkire <alkire@cern.ch> 2013.11.04

  Field: Interface for a named FEI4 global register field. 
  Fei4GlobalCfg: Complete global configuration for FEI4 with interface. 
*/
#include "EasySerializable.h"
#include <stdint.h>
#include <iostream>

//The interface to a specific set of configuration bits in a configuration array.
//Used as a map for the registers in the global configuration of the Fei4 (see FEI4_B manual pg. 118)
//template params: T: array type (use uint), mOffset: word in array , bOffset: LSB of the field in given word, 
//mask: length of field in bits, msbRight: most significant bit is on the right, so flipped.
template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight = false>
class Field{
 private:
  T* m_loc;
 public:
  Field(){}
  void setField(T* loc){m_loc = loc;}

  int value() const{ 
	 unsigned maskBits = (1<<mask)-1;
	 return ((m_loc[mOffset]&(maskBits<<bOffset))>>bOffset);
	 // To do: enable after fixing every calling function!
	 //return msbRight ? reverse((m_loc[mOffset]&(maskBits<<bOffset))>>bOffset) : ((m_loc[mOffset]&(maskBits<<bOffset))>>bOffset);
  }

   int valueEndian() const{ 
	 unsigned maskBits = (1<<mask)-1;
	 auto val = (m_loc[mOffset]&(maskBits<<bOffset))>>bOffset;
	 if(!msbRight) return val;
	 unsigned result = 0;
	 for(size_t bitix = 0; bitix < mask; bitix++){
		 bool is_set = ((1u << bitix) & val);
		 if(is_set) result = (result | (1u << (mask-bitix-1)));
         }
	 return (int)result;
  }

  //Places bits in configuration array in memory--Note: does not send to FE. See Fei4 for that.
  //Automatically reverses bits accordingly if the field has MSB on the right. 
  void write(const T& cfgBits){
    unsigned maskBits = (1<<mask)-1;
    m_loc[mOffset]=(m_loc[mOffset]&(~(maskBits<<bOffset)))|(((msbRight?reverse(cfgBits,mask):cfgBits)&maskBits)<<bOffset);
  }
 
  //Performs the bit-reversing functionality for fields with MSB on the right.
  unsigned reverse(unsigned x, unsigned n){
    if(n == 1){
      return(x);
    }else{
      return((reverse((((1<<unsigned((n+1)/2))-1)&x),((n+1)/2))<<(n/2))|reverse(x>>((n+1)/2),n/2));
    }
  }
};

class TxLink;
//Configuration map for global registers in FEI4: FEI4_B v2.3 manual page 118
class Fei4GlobalCfg: virtual public EasySerializable{
 public:
  static const unsigned numberOfRegisters = 36;
 protected:
  uint16_t cfg[numberOfRegisters];
  // Member fucntions to allow comparing field entries
  // The generic version returns false by default (uncomparable types)
  // The second version below is an overload for Field<> types that are comparable
  template <typename A, typename B>
  bool isSameField( A lhs, B rhs ) {
    return false;
  }
  // This function returns true only when pointers to Field<> members that are of compatible type point to the same location
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
  bool isSameField( Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*lhs, Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*rhs) {
    return lhs == rhs;
  }
 public: 
  static const uint16_t defaultCfg[numberOfRegisters]; // Static object common to _all_ Fei4GlobalCfg instances and _all_ objects derived from it
  static void sendTestConfig(TxLink*, uint8_t chipID = 8);//todo is this still used?
  Fei4GlobalCfg();
  Fei4GlobalCfg(const Fei4GlobalCfg& rhs);
  Fei4GlobalCfg& operator=(const Fei4GlobalCfg& rhs);
  bool operator==( const Fei4GlobalCfg& rhs);

  uint16_t rdGlobalRegister(unsigned reg) const {return cfg[reg];}
  void setTestingCfg();
  uint32_t getGlobalRegHash() const;
 
  void dump() const;
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    void writeField(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, const T& cfgBits){
    // Hard-coded safeguard against writing Vthin_Fine < 64 when Vthin_Coarse is zero
    // safe for b-field
    if( (this->Vthin_Coarse.value() == 0 && (isSameField(ref, &Fei4GlobalCfg::Vthin_Fine) && cfgBits < 64))) {
      std::cout << "WARNING: Ignoring instruction to set FE threshold below (Vthin_Coarse, Vthin_Fine) = (0,64)." << std::endl;
      (this->*ref).write(64);
      return;
    }
    if (((this->Vthin_Fine.value() & 0x3) == 0) && (isSameField(ref, &Fei4GlobalCfg::Vthin_Coarse) && cfgBits == 0)) { 
      std::cout << "WARNING: Ignoring instruction to set FE threshold below (Vthin_Coarse, Vthin_Fine) = (0,64)." << std::endl;
      return;
    }
    (this->*ref).write(cfgBits);
  }
  
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    int getValue(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref) const {
    return ((this->*ref).value());
  }

  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    int getValueEndian(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref) const {
    return ((this->*ref).valueEndian());
  }

  void setCfg(uint16_t thecfg[numberOfRegisters]);

  void setFields();
  //FEI4 global register map: FEI4_B v2.3 manual page 118
  //1
  Field<uint16_t, 1, 0x8, 1> SME;
  Field<uint16_t, 1, 0x0, 8, true> EventLimit;
  //2
  Field<uint16_t, 2, 0xC, 4> Trig_Count;
  Field<uint16_t, 2, 0xB, 1> CAE;
  //3
  Field<uint16_t, 3, 0x0, 16> ErrorMask_0;
  //4
  Field<uint16_t, 4, 0x0, 16> ErrorMask_1;
  //5
  Field<uint16_t, 5, 0x8, 8, true> PrmpVbp_R;
  Field<uint16_t, 5, 0x0, 8,true> BufVgOpAmp;
  Field<uint16_t, 5, 0x0, 8,true> GADCVref;
  //6
  Field<uint16_t, 6, 0x0, 8, true> PrmpVbp;
  //7
  Field<uint16_t, 7, 0x8, 8, true> TDACVbp;
  Field<uint16_t, 7, 0x0, 8, true> DisVbn;
  //8
  Field<uint16_t, 8, 0x8, 8, true> Amp2Vbn;
  Field<uint16_t, 8, 0x0, 8, true> Amp2VbpFol;
  //9
  Field<uint16_t, 9, 0x0, 8, true> Amp2Vbp;
  //10
  Field<uint16_t, 10, 0x8, 8, true> FDACVbn;
  Field<uint16_t, 10, 0x0, 8, true> Amp2Vbpff;
  //11
  Field<uint16_t, 11, 0x8, 8, true> PrmpVbnFol;
  Field<uint16_t, 11, 0x0, 8, true> PrmpVbp_L;
  //12
  Field<uint16_t, 12, 0x8, 8, true> PrmpVbpf;
  Field<uint16_t, 12, 0x0, 8, true> PrmpVbnLCC;
  //13
  Field<uint16_t, 13, 0xF, 1> S1;
  Field<uint16_t, 13, 0xE, 1> S0;
  Field<uint16_t, 13, 0xE, 2> S;
  Field<uint16_t, 13, 0x1, 13, true> Pixel_latch_strobe;
  //14
  Field<uint16_t, 14, 0x8, 8, true> LVDSDrvIref;
  Field<uint16_t, 14, 0x0, 8, true> GADCCompBias;
  //15
  Field<uint16_t, 15, 0x8, 8, true> PllIbias;
  Field<uint16_t, 15, 0x0, 8, true> LVDSDrvVos;
  //16
  Field<uint16_t, 16, 0x8, 8, true> TempSensIbias;
  Field<uint16_t, 16, 0x0, 8, true> PllIcp;
  //17
  Field<uint16_t, 17, 0x0, 8, true> PlsrIDACRamp;
  //18
  Field<uint16_t, 18, 0x8, 8, true> VrefDigTune;
  Field<uint16_t, 18, 0x0, 8, true> PlsrVgOpAmp;
  //19
  Field<uint16_t, 19, 0x8, 8, true> PlsrDACbias;
  Field<uint16_t, 19, 0x0, 8, true> VrefAnTune;
  //20
  Field<uint16_t, 20, 0x8, 8,true> Vthin_Coarse;
  Field<uint16_t, 20, 0x0, 8, true> Vthin_Fine;
  //21
  Field<uint16_t, 21, 0xC, 1> HLD;
  Field<uint16_t, 21, 0xB, 1> DJO;
  Field<uint16_t, 21, 0xA, 1> DHS;
  Field<uint16_t, 21, 0x0, 10, true> PlsrDAC;
  //22
  Field<uint16_t, 22, 0x9, 1> CP0;
  Field<uint16_t, 22, 0x8, 1> CP1;
  Field<uint16_t, 22, 0x8, 2, true> CP;
  Field<uint16_t, 22, 0x2, 6, true> Colpr_Addr;
  //23
  Field<uint16_t, 23, 0x0, 16> DisableColCnfg0;
  //24
  Field<uint16_t, 24, 0x0, 16> DisableColCnfg1;
  //25
  Field<uint16_t, 25, 0x8, 8> Trig_Lat;
  Field<uint16_t, 25, 0x0, 8> DisableColCnfg2;
  //26
  Field<uint16_t, 26, 0x3, 13> CMDcnt12;
  Field<uint16_t, 26, 0x3, 8> CalPulseWidth;
  Field<uint16_t, 26, 0xB, 5> CalPulseDelay;
  Field<uint16_t, 26, 0x2, 1> STC;
  Field<uint16_t, 26, 0x1, 1> HD1;
  Field<uint16_t, 26, 0x0, 1> HD0;
  Field<uint16_t, 26, 0x0, 2> HD;
  //27
  Field<uint16_t, 27, 0xF, 1> PLL;
  Field<uint16_t, 27, 0xE, 1> EFS;
  Field<uint16_t, 27, 0xD, 1> STP;
  Field<uint16_t, 27, 0xC, 1> RER;
  Field<uint16_t, 27, 0xA, 1> ADC;
  Field<uint16_t, 27, 0x9, 1> SRR;
  Field<uint16_t, 27, 0x5, 1> HOR;
  Field<uint16_t, 27, 0x4, 1> CAL;
  Field<uint16_t, 27, 0x3, 1> SRC;
  Field<uint16_t, 27, 0x2, 1> LEN;
  Field<uint16_t, 27, 0x1, 1> SRK;
  Field<uint16_t, 27, 0x0, 1> M13;
  //28
  Field<uint16_t, 28, 0xF, 1> LV0;
  Field<uint16_t, 28, 0x9, 1> EN_40M;
  Field<uint16_t, 28, 0x8, 1> EN_80M;
  Field<uint16_t, 28, 0x7, 1> c10;
  Field<uint16_t, 28, 0x6, 1> c11;
  Field<uint16_t, 28, 0x5, 1> c12;
  Field<uint16_t, 28, 0x5, 3> clk1;
  Field<uint16_t, 28, 0x4, 1> c00;
  Field<uint16_t, 28, 0x3, 1> c01;
  Field<uint16_t, 28, 0x2, 1> c02;
  Field<uint16_t, 28, 0x2, 3> clk0;
  Field<uint16_t, 28, 0x1, 1> EN_160;
  Field<uint16_t, 28, 0x0, 1> EN_320;
  //29
  Field<uint16_t, 29, 0xD, 1> N8b;
  Field<uint16_t, 29, 0xC, 1> c2o;
  Field<uint16_t, 29, 0x4, 8> EmptyRecordCnfg;
  Field<uint16_t, 29, 0x2, 1> LVE;
  Field<uint16_t, 29, 0x1, 1> LV3;
  Field<uint16_t, 29, 0x0, 1> LV1;
  //30
  Field<uint16_t, 30, 0xF, 1> TM0;
  Field<uint16_t, 30, 0xE, 1> TM1;
  Field<uint16_t, 30, 0xE, 2> TM;
  Field<uint16_t, 30, 0xD, 1> TMD;
  Field<uint16_t, 30, 0xC, 1> ILR;
  //31
  Field<uint16_t, 31, 0xD, 3> PlsrRiseUpTau;
  Field<uint16_t, 31, 0xC, 1> PPW;
  Field<uint16_t, 31, 0x6, 6, true> PlsrDelay;
  Field<uint16_t, 31, 0x5, 1> XDC;
  Field<uint16_t, 31, 0x4, 1> XAC;
  Field<uint16_t, 31, 0x0, 3> GADCSel;
  //32
  Field<uint16_t, 32, 0x0, 16> SELB0;
  //33
  Field<uint16_t, 33, 0x0, 16> SELB1;
  //34
  Field<uint16_t, 34, 0x8, 8> SELB2;
  Field<uint16_t, 34, 0x4, 1> b7E;
  //35
  Field<uint16_t, 35, 0x0, 16> EFUSE;

  const std::size_t size() const {return numberOfRegisters * sizeof(uint16_t);}

  SERIAL_H(prep(cfg,numberOfRegisters);)
};

#endif //__FEI4GLOBALCFG_H__

