/*
 * Fei3Mod
 * A single MCC object that talks to 16 FEs
 * By: Laser Kaplan
 */

#ifndef __FEI3MOD_H__
#define __FEI3MOD_H__

#include "Fei3ModCfg.h"
#include "Fei3ModCmd.h"
#include "Fei3ModData.h"
#include "FeLink.h"

class Fei3Mod : public Fei3ModCfg, public Fei3ModCmd {
    public:
        Fei3Mod();
        ~Fei3Mod();

        // rx and tx
        void setRxCh(uint32_t ch) {if (m_link) m_link->getRxLink()->setRxCh(ch); m_rxCh = ch;}
        void setTxCh(uint32_t ch) {if (m_link) m_link->getTxLink()->setTxCh(ch); m_txCh = ch;}
        uint32_t getRxCh() const {return m_rxCh;}
        uint32_t getTxCh() const {return m_txCh;}

        void setFeLink(FeLink *feLink) {
            m_link = feLink; 
            if (feLink) {
                Fei3ModCmd::setTxLink(feLink->getTxLink()); 
                Fei3ModCmd::set5MHzMode();
                m_rxCh = feLink->getRxCh();
            }
        }

        // high level MCC commands
        void sendMCCConfig();
        void readMCCRegister(MCCRegisters reg);
        void writeMCCRegister(MCCRegisters reg, uint16_t val, bool doSend=true);
        //WARNING, sendMCCRegister doesn't write the configuration in the PPC
        void sendMCCRegister(MCCRegisters reg, uint16_t val, bool doSend=true);
        void CALset(uint8_t delayRange, bool overrideRange, uint8_t delay, bool overrideDelay);//Send cal value but do not store in the PPC
        void addPattern(uint32_t pattern, uint32_t nPattern);
        void addHit(uint16_t fe_mask, uint8_t bcid, uint8_t row, uint8_t col, uint8_t tot);
        void addEOEWarning(uint16_t fe_mask, uint8_t bcid, uint8_t feflag, uint8_t parity, uint8_t l1id);
        void addEOE(uint16_t fe_mask, uint8_t bcid);

        // high level commands per FE
        void sendGlobalConfig(int iFE);
        void strobePixelLatchesDC(int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[1][nWordsPerDC]);
        void strobePixelLatchesDC(int iFE, Fei3::PixLatch pixReg, uint8_t iDC, uint32_t bitstream[1][nWordsPerDC]);
        void strobePixelLatches(int iFE, Fei3::PixLatch pixReg, uint8_t nDC, uint32_t bitstream[][nWordsPerDC]);
        void writeVcalFromCharge(int iFE, float charge, int capType);
        void setCapType(int iFE, int capType);
        void moveMask(int iFE, uint32_t pixRegMask);
        void sendPixelConfig(int iFE);
        void readGlobalConfig(int iFE);
	template<int nDCs>
        void readPixelConfigBit(int iFE, Fei3::PixLatch reg);
        void writeGlobalReg(int iFE, Fei3::GlobReg reg, uint16_t val) {m_chipCfgs[iFE].writeRegGlobal(reg, val);}
        void writePixelReg(int iFE, Fei3::PixLatch bit, int row, int col, int val) {m_chipCfgs[iFE].writeBitPixel(bit, row, col, val);}
        void writePixelTDAC(int iFE, int row, int col, int val) {m_chipCfgs[iFE].writeTDACPixel(row, col, val);}
        void writePixelFDAC(int iFE, int row, int col, int val) {m_chipCfgs[iFE].writeFDACPixel(row, col, val);}
        bool checkConfig(uint8_t iFE, bool doPR=false);

        //Read-back methods
        Fei3GlobalCfg getGlobalConfig(uint8_t iFE);
        Fei3PixelCfg getPixelConfig(uint8_t iFE);

        // proxy level commands
        void setDefaultGlobalConfigProxy(bool calib);
        void sendGlobalConfigProxy(bool calib);
        void strobePixelLatchesProxy(int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[][nWordsPerDC]);
        void strobePixelLatchesDCProxy(bool calib, Fei3::PixLatch pixReg, uint32_t bitstream[1][nWordsPerDC]);
        void strobePixelLatchesDC_wBCProxy(uint8_t iDC, bool calib, Fei3::PixLatch pixReg, uint32_t bitstream[][nWordsPerDC]);
        void writeVcalFromChargeProxy(bool calib, float charge, int capType);
        void setCapTypeProxy(bool calib, int capType);
        void sendPixelConfigProxy(bool calib);
        void writeGlobalRegProxy(bool calib, Fei3::GlobReg reg, uint16_t val);
        void writePixelRegProxy(bool calib, Fei3::PixLatch bit, int row, int col, int val);
        void writePixelTDACProxy(bool calib, int row, int col, int val);
        void writePixelFDACProxy(bool calib, int row, int col, int val);

        // helper functions, not for general use
        void setAllPixelShiftRegisters(int iFE, bool val);
        void setAllPixelShiftRegistersProxy(bool val, bool calib);
        std::vector<uint32_t> decideWriteCommand(int iFE, Fei3::PixLatch bit);
        
        // decode data from MCC
        std::vector<Fei3HitData> readHitData(bool raw = false) {return Fei3ModData::readHitData(*m_link, raw);}

        void setPreAmpsOn(bool ampsOn = true) { m_preAmpsOn = ampsOn; }
        void maskPixelEna (bool doMask = false){m_maskPixelEna = doMask;}
        void setDefaultGlobalConfig(uint8_t iFE);
        void dumpGlobalConfig(uint8_t iFE);
        void dumpGlobalConfigHR(uint8_t iFE);
        void dumpMccRegisters(bool readLocal);
        void debugConfigReadback(uint8_t iFE, const std::vector<uint8_t> &rxData, bool isGlobal, Fei3::PixLatch pixReg = Fei3::PixLatch::HitBus);
        uint32_t getVcalFromCharge(int iFE, float charge, int capType);
	/* Return the status of pre-amp */
	bool isPreAmpsOn(){return m_preAmpsOn;}
    private:
        FeLink *m_link;
        uint32_t m_verbosity;
        bool m_preAmpsOn;
        bool m_maskPixelEna;
};

#endif
