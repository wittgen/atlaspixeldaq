/*
 * Fei3ModCmd
 * MCC commands as described in the MCC documentation
 * This class sends commands to the MCC through the TxLink
 * Interacts with Fei3Cmd to get Fei3 command for mccWrFrontEnd
 * By: Laser Kaplan
 */

#ifndef __FEI3MODCMD_H__
#define __FEI3MODCMD_H__

#include "TxLink.h"
#include "IblBocTxFifoL12.h"
#include "Fei3Cmd.h"
#include "CleanTypes.h"

class Fei3ModCmd : public Fei3Cmd {
    public:
        Fei3ModCmd();
        Fei3ModCmd(TxLink *link);
        ~Fei3ModCmd() {}

        void setTxLink(TxLink *link) {m_TxLink = link;}
        void set5MHzMode(bool on = true) {
            IblBocTxFifoL12 *btf = dynamic_cast<IblBocTxFifoL12*>(m_TxLink);
            if (btf) {
                btf->set5MHzMode(on);
            }
            m_useFw5MHzMode = on;
        }
        TxLink* getTxLink() {return m_TxLink;}

        // trigger
        void sendLV1();

        // fast commands
        void sendBCR();
        void sendECR();
        void sendCAL();
        void sendSYNC();
        void sendCALandLVL1(uint8_t delay, uint32_t triggers = 1, bool doECR = false, bool doBCR = false);

        // slow commands
        void mccWrRegister(uint32_t address, uint32_t data, bool doSend = true);
        void mccRdRegister(uint32_t address);
        void mccWrFifo(uint32_t data, bool doSend = true);
        void mccRdFifo(uint32_t address);
        void mccWrFrontEnd(const std::vector<uint32_t> &data, bool doSend = true);
        void mccRdFrontEnd(const std::vector<uint32_t> &data, bool doSend = true);
        void mccWrReceiver(const std::vector<uint32_t> &data, bool doSend = true);
        void mccEnDataTake();
        void mccGlobalResetMCC();
        void mccGlobalResetFE(uint32_t syncW);

        static void convert40MHzTo5MHz(const std::vector<uint32_t> &dataIn, std::vector<uint32_t> &dataOut) {
            uint32_t word = 0;
            for (size_t i = 0; i < dataIn.size() * 32; i++) {
                int bit = (dataIn[i / 32] >> (31 - (i % 32))) & 0x1;
                word |= ((bit) ? 0xFF : 0x00) << ((3 - (i % 4)) * 8);
                if (i % 4 == 3) {
                    dataOut.push_back(word);
                    word = 0;
                }
            }
        }

    private:
        TxLink *m_TxLink;
        uint32_t m_verbosity;
        bool m_useFw5MHzMode;
};

#endif
