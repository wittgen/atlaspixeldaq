#ifndef __FEI4CFG_H__
#define __FEI4CFG_H__

//  Fei4Cfg: Combines Fei4GlobalCfg, Fei4ExtCfg, and Fei4PixelCfg.

#include "Fei4PixelCfg.h"
#include "Fei4ExtCfg.h"

class Fei4Cfg: public Fei4ExtCfg, public Fei4PixelCfg{
 public:
  Fei4Cfg(): Fei4ExtCfg(), Fei4PixelCfg(){}
  Fei4Cfg(const Fei4Cfg& rhs): Fei4ExtCfg(*(Fei4ExtCfg*)&rhs), Fei4PixelCfg(*(Fei4PixelCfg*)&rhs){}
  Fei4Cfg& operator=(const Fei4Cfg& rhs){
    Fei4ExtCfg::operator=(rhs);
    Fei4PixelCfg::operator=(rhs);
    return *this;
  }
  bool operator==( const Fei4Cfg& rhs){
    return (Fei4ExtCfg::operator==( rhs ) & 
	    Fei4PixelCfg::operator==( rhs ) ); 
  }

  uint32_t getHash() const {
    uint32_t hash = getPixelRegHash();
    hash ^= getGlobalRegHash();
    hash ^= getExtRegHash();
    return hash;
  }

  std::vector<uint32_t> getFEHashes() const {
     std::vector<uint32_t> result;
     result.resize(3);
     result[0] = getPixelRegHash();
     result[1] = getGlobalRegHash();
     result[2] = getExtRegHash();
     return result;
  }

  virtual ~Fei4Cfg(){}
  SERIAL_H(Fei4ExtCfg::serial()  ;
	   Fei4PixelCfg::serial();)
};
#endif //__FEI4CFG_H__
