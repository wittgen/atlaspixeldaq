
#ifndef __FE_LINK_H__
#define __FE_LINK_H__

#include "TxLink.h"
#include "RxLink.h"
#include <iostream>

class FeLink: virtual public TxLink, virtual public RxLink {
public:
	virtual TxLink* getTxLink() = 0;
	virtual RxLink* getRxLink() = 0;
	virtual ~FeLink() { };
};

template <typename _TxLinkT, typename _RxLinkT>
class FeLinkImpl: public _TxLinkT, public _RxLinkT, public FeLink {
public:

  void write(uint32_t data) { _TxLinkT::write(data); }
  uint32_t read() { return _RxLinkT::read(); }
    void send() { return _TxLinkT::send(); }
    TxLink *getTxLink() { return static_cast<_TxLinkT*>(this); }
    RxLink *getRxLink() { return static_cast<_RxLinkT*>(this); }
    
  };
  
#endif // __FE_LINK_H__
