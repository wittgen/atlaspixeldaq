#ifndef _FEI4PROXY_H_
#define _FEI4PROXY_H_
/*
 * Author: K. Potamianos <karolos.potamianos@cern.ch>
 * Date: 2014-II-18
 * Description: Command proxy to easily forward commands to Fei4
 * The purpose of the Fei4Proxy is to be the single point of entry towards FE operations.
 * The internal m_feMask is used to set the mask for the IblRodSerialPort and also to decide
 * over which FE to loop when forwarding a command to it.
 * The 'broadcast' operations are provided through Fei4Cmd, while FE specific operations are 
 * passed to Fei4 objets. The m_feMask decides which Fei4 objets are looped over.
 * It is also possible to pass whether the 'Physics' or 'Calib' set of Fei4 objects (see RodRessourceManager).
 */

// To-do lj: Replace commands that should be broadcast during a scan with single Fei4
// object which has serial port as Tx Link
#include <unistd.h>
#include "Fei4Cmd.h"
#include "RodResourceManager.h"

class Fei4Proxy: public Fei4Cmd {
public:
	

  // Default constructor sets front end mask to all channels for iterating through FE operations
  // Default constructor doesn't change the Tx mask of the serial port
  // The serial port may already be configured to all enabled channels
  // If you want to change the serial port mask, access directly the serial port
  
  Fei4Proxy( uint32_t feMask = 0xFFFFFFFF ): m_feMask(feMask) {
    this->Fei4Cmd::setTxLink( RodResourceManagerIBL::getSerialPort() );
  }
  
    	// Use this command to set a given register to a set of Fei4 objects (using the fe_mask)
	// Usage: Fei4Proxy::writeField( &Fei4::SME, 0x1 )
	// By default, loops over CalibCfg
	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	static void writeField(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits, uint32_t fe_mask, bool calib = true ) {
		Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
		Fei4 *fePtr = cfgArray;
		const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
		uint32_t current_mask = 0x1;
		while( fePtr != (cfgArray+nCfg) ) {
			if( fe_mask & current_mask )
				fePtr->writeField( ref, cfgBits );
			current_mask <<= 1;
			fePtr++;
		} 
			
	}
	
	// Todo: use C++11 and variadic templates to get rid of code duplication

	// Use this command to set a given register to a set of Fei4 objects (using the fe_mask)
	// Usage: Fei4Proxy::writeRegister( &Fei4::SME, 0x1 )
	// By default, loops over CalibCfg
	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	static void writeRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits, uint32_t fe_mask, bool calib = true ) {
		Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
		Fei4 *fePtr = cfgArray;
		const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
		uint32_t current_mask = 0x1;
		while( fePtr != (cfgArray+nCfg) ) {
		  if( fe_mask & current_mask )
		    fePtr->writeRegister( ref, cfgBits );
		  current_mask <<= 1;
		  fePtr++;
		}
	}

	// Use this command to set a given register to a set of Fei4 objects (using the fe_mask)
	// Usage: Fei4Proxy::sendRegister( &Fei4::SME, 0x1 )
	// By default, loops over CalibCfg
	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	static void sendRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint32_t fe_mask, bool calib = true ) {
		Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
		Fei4 *fePtr = cfgArray;
		const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
		uint32_t current_mask = 0x1;
		while( fePtr != (cfgArray+nCfg) ) {
			if( fe_mask & current_mask )
				fePtr->sendRegister( ref );
			current_mask <<= 1;
			fePtr++;
		}
	}

	// Use this command to send the global config of a collection of front ends
	// By default, loops over CalibCfg
	static void sendGlobalConfigAmpsOn( uint32_t fe_mask, bool clearSR = true, uint32_t sleepDelay = 0, bool calib = true ) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  #ifdef __XMK__
	  xil_printf("INFO: Sending Global Config with pre-amps ON\n");
	  #endif
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->sendGlobalConfigAmpsOn(clearSR);
	    current_mask <<= 1;
	    fePtr++;
	    usleep(sleepDelay); // to prevent current transients
	  }
	}
	// Use this command to send the global config of a collection of front ends
	// By default, loops over CalibCfg
	static void sendGlobalConfigAmpsOff( uint32_t fe_mask, bool clearSR = true, uint32_t sleepDelay = 0, bool calib = true ) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  #ifdef __XMK__
	  xil_printf("INFO: Sending Global Config with pre-amps OFF\n");
	  #endif
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->sendGlobalConfigAmpsOff(clearSR);
	    current_mask <<= 1;
	    fePtr++;
	    usleep(sleepDelay); // to prevent current transients
	  }
	}

	static uint32_t checkGlobalConfig( uint32_t fe_mask, bool ampsOn, bool sendBack, std::map<uint32_t,std::vector<uint32_t> > & readBackGlobal, bool calib = true) {
	  uint32_t success = 0;
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      if (fePtr->checkGlobalConfig(ampsOn,sendBack,readBackGlobal[fe_mask & current_mask])) success |= current_mask;
	    current_mask <<= 1;
	    fePtr++;
	  }
	  return success;
	}

	static void clearFullShiftRegister(uint32_t fe_mask, bool calib=true){
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->clearFullShiftRegister();
	    current_mask <<= 1;
	    fePtr++;
	  }
	}


	static void readBackFullShiftRegister(uint32_t fe_mask, std::map<uint32_t,Fei4PixelCfg>& ShiftRegisterMap, uint32_t delay, int ShiftRegLeft, bool calib=true){

	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask ) {
	      ShiftRegisterMap[current_mask] = fePtr->rdbackPixelConfig(false,delay,ShiftRegLeft);
	    }
	    current_mask <<= 1;
	    fePtr++;
	  }
	}
	
	static void loadOnesInSR(uint32_t fe_mask, bool calib=true)
	{
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->loadOnesInSR();
	    
	    current_mask <<= 1;
	    fePtr++;
	  }
	}
	
	static void readBackPixelCfg(uint32_t fe_mask, std::map<uint32_t, Fei4PixelCfg>& PixCfgMap, int ShiftRegLeft, bool calib=true){
	  
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;

	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      PixCfgMap[current_mask] = (fePtr->rdbackPixelConfig(true, 0, ShiftRegLeft));
	    current_mask <<= 1;
	    fePtr++;
	  }

	}


	// write a reg by address and value
        static void writeRegister( uint32_t fe_mask, uint16_t address, uint16_t value, bool calib = true ) {
          Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
          Fei4 *fePtr = cfgArray;
          const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
          uint32_t current_mask = 0x1;
          while( fePtr != (cfgArray+nCfg) ) {
            if( fe_mask & current_mask )
              fePtr->writeRegister(address, value);
            current_mask <<= 1;
            fePtr++;
          }
        }

	// send a reg by address
        static void sendRegister( uint16_t address, uint32_t fe_mask, bool calib = true ) {
          Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
          Fei4 *fePtr = cfgArray;
          const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
          uint32_t current_mask = 0x1;
          while( fePtr != (cfgArray+nCfg) ) {
            if( fe_mask & current_mask )
              fePtr->sendRegister(address);
            current_mask <<= 1;
            fePtr++;
          }
        }

	// Use this command to set a collection of front ends into run mode
	// By default, loops over CalibCfg
	// Calls the Fei4 specific setRunMode to handle the delicate threshold register sending / setting high for safety
	void setRunMode(bool mode, uint32_t fe_mask, bool calib = true ) {
	  
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->setRunMode(mode);
	    current_mask <<= 1;
	    fePtr++;
	  }
	}

	void globalPulse(int width) {
	  this->Fei4Cmd::globalPulse(8, width);
	}

	void globalReset() {
	  this->Fei4Cmd::globalReset(8);
	}

	 void wrFrontEnd(uint32_t bitstream[21]) {
	  this->Fei4Cmd::wrFrontEnd(8, bitstream);
	}

	static void strobePixelLatches(uint32_t fe_mask, bool calib = true) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->strobePixelLatches();
	    current_mask <<= 1;
	    fePtr++;
	  }
	}
	static void strobePixelLatches(Mask mask, uint32_t pixelStrobeValue, uint8_t dc, uint32_t fe_mask, bool calib = true) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->strobePixelLatches(mask, pixelStrobeValue, dc);
	    current_mask <<= 1;
	    fePtr++;
	  }
	}

	//WARNING DO NOT USE AFTER GREEN LIGHT FROM EXPERTS, MAY NOT BE SAFE FOR B-FIELD!!!
	static void broadcastPixelLatches(Mask mask, uint32_t pixelStrobeValue, uint32_t fe_mask, bool calib = true) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->broadcastPixelLatches(mask, pixelStrobeValue);
	    current_mask <<= 1;
	    fePtr++;
	  }
	}

	// Use this command to set the vcal value directory from the desired charge
	// By default, loops over CalibCfg
	static void writeVcalFromCharge(float charge, int capType, uint32_t fe_mask, bool calib = true) {
	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->writeVcalFromCharge(charge,capType);
	    current_mask <<= 1;
	    fePtr++;
	  }
	}

	static void readBackSR(uint32_t fe_mask) {
	  Fei4 * const cfgArray = RodResourceManagerIBL::getCalibModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    if( fe_mask & current_mask )
	      fePtr->readBackSR();
	    current_mask <<= 1;
	    fePtr++;
	  }
	}

	// The set of functions below calls the static functions above, but using the internal mask (m_feMask).

	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	void writeField(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits, bool calib = true ) {
		Fei4Proxy::writeField( ref, cfgBits, m_feMask, calib );
	}

	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	void writeRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits, bool calib = true ) {
		Fei4Proxy::writeRegister( ref, cfgBits, m_feMask, calib );
	}

	template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
	void sendRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, bool calib = true ) {
		Fei4Proxy::sendRegister( ref, m_feMask, calib );
	}

        void sendRegister(uint16_t reg, bool calib = true) {
	  Fei4Proxy::sendRegister(reg, m_feMask, calib);
        }

	void setRunMode(bool mode = false, bool calib = true) {
	  Fei4Proxy::setRunMode(mode, m_feMask, calib); 
	}

	void sendGlobalConfigAmpsOn(bool clearSR = true, uint32_t sleepDelay = 0, bool calib = true) {
	  Fei4Proxy::sendGlobalConfigAmpsOn(m_feMask, clearSR, sleepDelay, calib); 
	}
	void sendGlobalConfigAmpsOff(bool clearSR = true, uint32_t sleepDelay = 0, bool calib = true) {
	  Fei4Proxy::sendGlobalConfigAmpsOff(m_feMask, clearSR, sleepDelay, calib); 
	}
	
	//PF::JP patch 28/09/2018. ampsOn is ignored. Todo: remove passing of ampsOn.
	uint32_t checkGlobalConfig(bool ampsOn, bool sendBack, std::map<uint32_t, std::vector<uint32_t> > & readBackGlobal, bool calib = true) {
	  return Fei4Proxy::checkGlobalConfig(m_feMask, ampsOn, sendBack, readBackGlobal,calib);
	}
	
	void clearFullShiftRegister(bool calib = true) {
	  Fei4Proxy::clearFullShiftRegister(m_feMask,calib);
	}

	void loadOnesInSR(bool calib = true) {
	  Fei4Proxy::loadOnesInSR(m_feMask, calib);
	}
	

	void readBackFullShiftRegister(std::map<uint32_t, Fei4PixelCfg>& ShiftRegisterMap, uint32_t delay, int ShiftRegLeft, bool calib = true) {
	  Fei4Proxy::readBackFullShiftRegister(m_feMask,ShiftRegisterMap,delay,ShiftRegLeft, calib);
   }

	void rdBackPixelCfg(std::map<uint32_t, Fei4PixelCfg>& PixCfgMap, int ShiftRegLeft, bool calib = true) {
	  Fei4Proxy::readBackPixelCfg(m_feMask,PixCfgMap,ShiftRegLeft, calib);
	}

	void writeRegister(uint16_t address, uint16_t value, bool calib = true) {
	  Fei4Proxy::writeRegister(m_feMask, address, value, calib); 
	}

	void sendPixelConfig(bool normalScan = false, bool calib = true) {

	  Fei4 * const cfgArray = calib ? RodResourceManagerIBL::getCalibModuleConfigs() : RodResourceManagerIBL::getPhysicsModuleConfigs();
	  Fei4 *fePtr = cfgArray;
	  const uint8_t nCfg = RodResourceManagerIBL::getNModuleConfigs();
	  uint32_t current_mask = 0x1;
	  while( fePtr != (cfgArray+nCfg) ) {
	    // protection against the fact that the FE will not ignore the 672 bits
	    // following the wrFrontEnd command even if it is not issued to its chip id
	    // always broadcast wrFrontEnd, but set other chips to a non-valid DC
	    this->Fei4Cmd::getTxLink()->write(0x005A0800+((8<<6)&0x3C0)+(22&0x3F));
	    this->Fei4Cmd::getTxLink()->write(0x00140000);
	    this->Fei4Cmd::getTxLink()->send();

	    if( m_feMask & current_mask ) {	      
	      fePtr->sendPixelConfig(normalScan);
	    }
	    current_mask <<= 1;
	    fePtr++;
	  }
	  this->sendRegister(&Fei4::Colpr_Addr);
	}

	void strobePixelLatches() {
	  Fei4Proxy::strobePixelLatches(m_feMask);
	}
	void strobePixelLatches(Mask mask, uint32_t pixelStrobeValue, uint8_t dc) {
	  Fei4Proxy::strobePixelLatches(mask, pixelStrobeValue, dc, m_feMask);
	}

	//WARNING DO NOT USE AFTER GREEN LIGHT FROM EXPERTS, MAY NOT BE SAFE FOR B-FIELD!!!
	void broadcastPixelLatches(Mask mask, uint32_t pixelStrobeValue) {
	  Fei4Proxy::broadcastPixelLatches(mask, pixelStrobeValue, m_feMask);
	}
	void writeVcalFromCharge(float charge, int capType, bool calib = true) {
	  Fei4Proxy::writeVcalFromCharge(charge, capType, m_feMask, calib);
	}

	void readBackSR() {
	  Fei4Proxy::readBackSR(m_feMask);
	}

	// Getters and setters
	void setFeMask( uint32_t feM ) { m_feMask = feM; };
	uint32_t getFeMask() { return m_feMask; };
	
 private:
	uint32_t m_feMask; ///< FE mask used for operations
};
#endif //_FEI4PROXY_H_
