/*
  Fei4ExtCfg: Adds link info, IDs, and revision to the FE configuration. 
  Edit: Steve Alkire <alkire@cern.ch> 2014.03.27
*/
#ifndef __FEI4EXTCFG_H__
#define __FEI4EXTCFG_H__

#include "Fei4GlobalCfg.h"

class Fei4ExtCfg: public Fei4GlobalCfg {
 protected:
  uint8_t m_chipID;
  uint8_t m_moduleID;
  uint8_t m_rxCh;
  uint8_t m_txCh;
  uint16_t m_revision;
  bool m_enabled;
  float m_vcal0;
  float m_vcal1;
  float m_cinjLow;
  float m_cinjMed;
  float m_cinjHigh;
  int m_serialNumber;

 public:
  Fei4ExtCfg();
  Fei4ExtCfg(const Fei4ExtCfg& rhs);
  //todo maybe should be friends?
  Fei4ExtCfg& operator=(const Fei4ExtCfg& rhs);
  bool operator==( const Fei4ExtCfg& rhs);
  

  void dump() const;
  
  void setRevision(uint16_t r){m_revision = r;}
  uint16_t getRevision() const{return m_revision;}

  void setChipID(uint8_t id){m_chipID = id;}
  int getChipID() const{return m_chipID;}
  void setModuleID(uint8_t id){m_moduleID = id;}
  int getModuleID() const{return m_moduleID;} 

  void setRxCh(uint8_t c){m_rxCh = c;}
  int getRxCh() const{return m_rxCh;}
  void setTxCh(uint8_t c){m_txCh = c;}
  int getTxCh() const{return m_txCh;}
  bool getEnabled() const{return m_enabled;}
  //todo remove one of these?
  bool isEnabled() const{return m_enabled;}
  void setEnabled(bool yn = true) {m_enabled = yn;}

  void setVcal0(float vcal){m_vcal0 = vcal;}
  void setVcal1(float vcal){m_vcal1 = vcal;}
  void setCinjLow(float c){m_cinjLow = c;}
  void setCinjMed(float c){m_cinjMed = c;}
  void setCinjHigh(float c){m_cinjHigh = c;}
  float getVcal0() const{return m_vcal0;}
  float getVcal1() const{return m_vcal1;}
  float getCinjLow() const{return m_cinjLow;}
  float getCinjMed() const{return m_cinjMed;}
  float getCinjHigh() const{return m_cinjHigh;}

  int getSerialNumber() const { return m_serialNumber; }
  uint32_t getExtRegHash() const;

  SERIAL_H(Fei4GlobalCfg::serial(); prep(m_chipID); prep(m_moduleID); prep(m_rxCh); prep(m_txCh); 
	   prep(m_revision); prep(m_enabled); prep(m_vcal0); prep(m_vcal1);
	   prep(m_cinjLow); prep(m_cinjMed); prep(m_cinjHigh); prep(m_serialNumber);)
};

#endif //__FEI4EXTCFG_H__
