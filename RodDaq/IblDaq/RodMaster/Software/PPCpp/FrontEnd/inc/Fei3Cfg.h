/*
 * Fei3Cfg
 * Combines one Global, Pixel, and Ext cfg per Fei3
 * By: Laser Kaplan
 */

#ifndef __FEI3CFG_H__
#define __FEI3CFG_H__

#include "Fei3PixelCfg.h"
#include "Fei3ExtCfg.h"

class Fei3Cfg : public Fei3ExtCfg, public Fei3PixelCfg {
    public:
        Fei3Cfg() : Fei3ExtCfg(), Fei3PixelCfg() {}
        Fei3Cfg(const Fei3Cfg &rhs) : Fei3ExtCfg(*(Fei3ExtCfg*)&rhs), Fei3PixelCfg(*(Fei3PixelCfg*)&rhs) {}
        Fei3Cfg & operator=(const Fei3Cfg &rhs) {
            Fei3ExtCfg::operator=(rhs);
            Fei3PixelCfg::operator=(rhs);
            return *this;
        }
        virtual ~Fei3Cfg() {}

        uint32_t getHash() const {
            uint32_t hash = getPixelRegHash();
            hash ^= getGlobalRegHash();
            hash ^= getExtRegHash();
            return hash;
        }

        SERIAL_H(Fei3ExtCfg::serial(); Fei3PixelCfg::serial();)
};

#endif
