/*
 * Fei3ExtCfg
 * Various extra values that define the Fei3
 * By: Laser Kaplan
 */

#ifndef __FEI3EXTCFG_H__
#define __FEI3EXTCFG_H__

#include "Fei3GlobalCfg.h"

class Fei3ExtCfg : public Fei3GlobalCfg {
    protected:
        // PixelFECommand
        uint32_t m_address;
        // PixelFECalib
        float m_cinjLow;
        float m_cinjHigh;
        float m_vcalCoeff0;
        float m_vcalCoeff1;
        float m_vcalCoeff2;
        float m_vcalCoeff3;

    public:
        Fei3ExtCfg();
        Fei3ExtCfg(const Fei3ExtCfg &rhs);
        Fei3ExtCfg & operator=(const Fei3ExtCfg &rhs);
        virtual ~Fei3ExtCfg() {}

        void dump() const;

        void setAddress(uint32_t address){m_address = address;}
        int getAddress() {return m_address;}

        void setCinjLow(float c){m_cinjLow = c;}
        float getCinjLow() {return m_cinjLow;}
        void setCinjHigh(float c){m_cinjHigh = c;}
        float getCinjHigh() {return m_cinjHigh;}
        void setVcalCoeff0(float vcalCoeff0){m_vcalCoeff0 = vcalCoeff0;}
        float getVcalCoeff0() {return m_vcalCoeff0;}
        void setVcalCoeff1(float vcalCoeff1){m_vcalCoeff1 = vcalCoeff1;}
        float getVcalCoeff1() {return m_vcalCoeff1;}
        void setVcalCoeff2(float vcalCoeff2){m_vcalCoeff2 = vcalCoeff2;}
        float getVcalCoeff2() {return m_vcalCoeff2;}
        void setVcalCoeff3(float vcalCoeff3){m_vcalCoeff3 = vcalCoeff3;}
        float getVcalCoeff3() {return m_vcalCoeff3;}

        uint32_t getExtRegHash() const;

        SERIAL_H(Fei3GlobalCfg::serial();
                 prep(m_address); prep(m_cinjLow); prep(m_cinjHigh);
                 prep(m_vcalCoeff0); prep(m_vcalCoeff1); prep(m_vcalCoeff2); prep(m_vcalCoeff3);
                )
};

#endif
