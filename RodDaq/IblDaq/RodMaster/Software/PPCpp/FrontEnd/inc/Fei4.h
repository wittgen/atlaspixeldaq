/* L. Jeanty <laura.jeanty@cern.ch>
 * Date: 15-Oct-2013
 * Description: Front end class for Fei4 chips. Inherits from the Fei4Cfg and 
 *              Fei4Cmds, and contains additional information.
 *           
 */
#ifndef __FEI4_H__
#define __FEI4_H__

#include "Fei4Cfg.h"
#include "Fei4Cmd.h"
#include "Fei4Data.h"
#include "FeLink.h"


#include <vector>
#include <string>
#include <array>
#include <map>

#include "Bitstream.h"

#ifndef __XMK__
#include <string_view>
#include "ConstexprMap.h"
using std::string_view_literals::operator""sv;
#endif

class Fei4 : public Fei4Cfg, public Fei4Cmd {

private:
  
  // send actual threshold, private to prevent accidental sending of threshold 
  // threshold should be kept high when doing all configuration actions
  void sendThreshold( ){ 
    writeRegisterHeader(20);
    m_link->write((cfg[20])<<16);
    m_link->send();
  }
  
  FeLink * m_link;
  bool m_runMode;
 
  
  std::vector< int > m_serviceRecordCount;
  
  void writeRegisterHeader(unsigned i);
  
  
  void sendGlobalConfig(bool ampsOn, bool clearSR = true); // sendGlobalConfig that does the work
                                      // private so user has to explicitly specify preamps ON or OFF in function name

  void finishConfig(bool clearSR = true); // cleaning up the global config after sending register values
  

  uint8_t m_prmpvbp_standby; // standby prmpvbp value
  uint8_t m_amp2vbn_standby; // standby amp2vbn value
  uint8_t m_disvbn_standby; // standby disvbn value
  bool m_standby_3DBM; // enable outermost 3D modules outermost DC for beam monitoring?

  bool m_preAmpsOn; // were the preAmps set OFF or ON in the last action?
  bool m_checkSR; // read back service records after configuring?

 public:
  

  Fei4();
  Fei4(FeLink * felink);
  //Fei4(const Fei4& rhs);
  ~Fei4();

  void setFeLink(FeLink * feLink) { m_link = feLink; if(feLink) { Fei4Cmd::setTxLink(feLink->getTxLink()); m_rxCh = feLink->getRxCh();} }

  
  uint16_t correctedReg(int i, bool ampsOn); // corrected value for special warm start / threshold registers
  void clearFullShiftRegister();
  void loadOnesInSR();
  void readBackShiftRegister(uint32_t bitstream[],uint8_t regDC = 0x0, uint8_t regCP = 0x0,bool reverseBits = true, bool complementToOne=false);
  void DecodeToBitStream (uint32_t bitstream[],const std::vector<int> &frame, bool reverseBits, bool complementToOne);
  
  void setVerbose(bool v = true){verbose = v;};
  void copyConfig(const Fei4ExtCfg & cfgToCopy);//To-do remove
  void setRxCh(uint32_t ch) {if (m_link) m_link->getRxLink()->setRxCh(ch); m_rxCh = ch;};
  void setTxCh(uint32_t ch) {if (m_link) m_link->getTxLink()->setTxCh(ch); m_txCh = ch;};

  uint32_t getRxCh() const {return m_rxCh;}
  uint32_t getTxCh() const {return m_txCh;}

  // Send module configurations
  // Note:: does NOT set register 20 (threshold), which is set when going into runMode
  void sendGlobalConfigAmpsOn(bool clearSR = true); // full config sent including pre-amps ON
  void sendGlobalConfigAmpsOff(bool clearSR = true); // send config with low or off pre-amps for non-stable beam conditions

  void reconfig(bool sendPixelCfg);

  void sendPixelConfig(bool normalScan = false); // if doing a normal scan, don't send all pixels in config

  
  bool checkGlobalConfig(bool ampsOn, bool sendBack, std::vector<uint32_t>& readBackGlobal); // read back the global config, default is that pre-amps are off
 
  //Send to FE the entire bits of global register reg
  //Steve <alkire@cern.ch>
  void sendRegister(unsigned reg){
    writeRegisterHeader(reg);
    m_link->write(correctedReg(reg,m_preAmpsOn)<<16);
    m_link->send();
  }
  void prepRegister(unsigned reg){
    writeRegisterHeader(reg);
    m_link->write(correctedReg(reg,m_preAmpsOn)<<16);
  }

  //Return header bits for the write register command
  uint32_t returnRegisterHeader(unsigned i) {
    return (0x005A0800|i|(m_chipID<<6));
  }
  
  
  std::pair<uint32_t,uint16_t> returnReg(unsigned reg) {
    std::pair<uint32_t,uint16_t> pair(returnRegisterHeader(reg),correctedReg((int)reg,m_preAmpsOn));
    return pair;
  }
  
  //Send to FE the entire bits of global register identified by containing field Fei4GlobalCfg::*ref
  //Use: fe.sendRegister(&Fei4::SME)
  //Steve <alkire@cern.ch>
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    void sendRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref){
    unsigned reg = mOffset;
    sendRegister(reg);
  }
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    void prepRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref){
    unsigned reg = mOffset;
    prepRegister(reg);
  }
  
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    std::pair<uint32_t,uint16_t> returnReg(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref) {
    unsigned reg = mOffset;
    return returnReg(reg);
  }

  
  //Write val to entire bits and send to FE the entire bits of global register reg
  //Steve <alkire@cern.ch>
  void writeRegister(unsigned reg,uint16_t val){
    // Hard-coded safeguard against writing Vthin_Fine < 64 when Vthin_Coarse is zero
    // safe for b-field
    if (reg == 20 && (Fei4Cmd::reverse8((0x00FF&val)) < 64)) {
      std::cout << "WARNING: Ignoring instruction to set FE threshold below (Vthin_Coarse, Vthin_Fine) = (0,64)." << std::endl;      
      return;
    } 
    cfg[reg] = val;
    sendRegister(reg);
  }

  //Write cfgBits to the Field Fei4GlobalCfg::*ref only--Note:not entire register 
  //and send to FE the entire bits of global register identified bycontaining field Fei4GlobalCfg::*ref
  //Use: fe.writeRegister(&Fei4::SME,0x1)
  //Steve <alkire@cern.ch>
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    void writeRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits){
    writeField(ref, cfgBits);
    sendRegister(ref);
  }
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    void prepRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits){
    writeField(ref, cfgBits);
    prepRegister(ref);
  }
  template<typename T, unsigned mOffset, unsigned bOffset, unsigned mask, bool msbRight>
    std::pair<uint32_t,uint16_t> returnRegister(Field<T, mOffset, bOffset, mask, msbRight> Fei4GlobalCfg::*ref, uint16_t cfgBits) {
    //I'm not convinced this is the best thing..
    writeField(ref,cfgBits);
    return returnReg(ref);
  }



  void printServiceRecordErrors();

  //TODO:remove hardcoding and think about a better thing to return
  uint16_t getGlobCfgRegValue(int i) const {
    if (i>31){
      std::cout<<"ERROR::getGlobCfgRegValue with address >31. Returning 0x0"<<std::endl;
      return 0x0;
    }
    return cfg[i];
  }
  
  // run mode = true: run mode
  // run mode = false: config mode
  bool getRunMode() {return m_runMode;};
  void setRunMode(bool mode = true);
  

  // inherit command functions from Fei4Cmd and use the chipID of the FEI4
  void wrRegister(int address, int value) {Fei4Cmd::wrRegister(m_chipID, address, value);};
  void rdRegister(int address){Fei4Cmd::rdRegister(m_chipID, address);};
  void wrFrontEnd(uint32_t bitstream[21]) {Fei4Cmd::wrFrontEnd(m_chipID, bitstream);};
  void runMode(bool mode) {Fei4Cmd::runMode(m_chipID, mode);};
  void globalReset() {Fei4Cmd::globalReset(m_chipID);};
  void globalPulse(int width) {Fei4Cmd::globalPulse(m_chipID, width);};
  void prepFrontEnd(uint32_t bitstream[21]) {Fei4Cmd::prepFrontEnd(m_chipID, bitstream);};
  void prepPulse(int width) {Fei4Cmd::prepPulse(m_chipID, width);};

  //todo should we create a blank one and fill it insteaad? probably not but idk
  //todo is this where these should be?
  //todo maybe should return all of the fes
  static Fei4ExtCfg rdExtConfig(uint32_t iFe);
  static Fei4PixelCfg rdPixelConfig(uint32_t iFe);
  //this function reads the full pixel configuration by default. 
  //If readFullPixelConfig=false it reads back the shift register 13 times (writing 0s every time)
  Fei4PixelCfg rdbackPixelConfig(bool readFullPixelConfig = true,uint32_t delay = 0, int ShiftRegLeft = 0);
  static Fei4Cfg rdFullConfig(uint32_t iFe);
  
  // the sequence of register writes and global pulse to latch the pixel registers 
  // set in the pixel_strobe_latches global register with the value currently in the shift register
  // with no arguments, just the strobing action happens
  void strobePixelLatches();
  // if you give it a mask, a pixel latch strobe GR value, and a double column address,
  // this function will do the complete set of operations for latching the pixel registers to that DC
  // only the lowest 13 bits of the pixelStrobe Value are used
  void strobePixelLatches(DoubleColumnBit dcb, uint32_t pixelStrobeValue, uint8_t dc, bool send = true);
  void strobePixelLatches(Mask mask, uint32_t pixelStrobeValue, uint8_t dc, bool send = true);

  //WARNING DO NOT USE AFTER GREEN LIGHT FROM EXPERTS, MAY NOT BE SAFE FOR B-FIELD!!!
  void broadcastPixelLatches(Mask mask, uint32_t pixelStrobeValue, bool send=true);

  void decodePixData(std::vector<int>& frame, uint8_t occ[80][336], uint8_t tot[80][336], uint8_t tot2[80][336]);
  // returns a vector of 4 column x 336 pixel ToTs starting with the startCol provided
  void decodePixData(std::vector<int>& frame, uint8_t tot[4][336], int startCol = 0);
  // returns the number of pixel hits in the frame
  uint32_t decodePixData(std::vector<int>& frame);
  // for debug, print outs the hit information for each hit (col, row, tot)
  void decodePixData(std::vector<int>& frame, bool printOut);
  int decodeToT(int tot, int hitdisccnf);
  int decodeToT(int tot);
  void decodeServiceRecord(uint8_t byte1, uint8_t byte2);
  void rawDataDump();
  void readDataRecord();
  uint32_t getVcalFromCharge(float charge, int capType);
  void writeVcalFromCharge(float charge, int capType);

  void readSerialNumber();
  uint16_t getRegisterValueFromFE(uint8_t gRegAddr);

  void setAmp2VbnStandby(uint8_t value) {m_amp2vbn_standby = value;};
  void setPrmpVbpStandby(uint8_t value) {m_prmpvbp_standby = value;};
  void setDisVbnStandby(uint8_t value) {m_disvbn_standby = value;};
  void setStandby3DBM(uint8_t value) {m_standby_3DBM = value;};

  void addScanDisablePixel(uint8_t dc, uint32_t pixel) {scanDisable.push_back(std::make_pair(dc, pixel));};
  void clearScanDisablePixel() {scanDisable.clear();};
  
  void saveServiceRecord(uint8_t byte1, uint8_t byte2);
  void readBackSR();

  int m_serviceRecord[32];

  // Fei4 version of readFrame for backwards compatibility.
  std::vector<int> readFrame(bool raw = false,bool shiftRegisterReadBack = false){return Fei4Data::readFrame(*m_link, raw, shiftRegisterReadBack);}

  std::vector< std::pair< uint8_t, uint32_t > > scanDisable; // pixels to explictly disable during calibration < DC, pixel within DC (0-671) >
  
  /* Return the status of pre-amp */
  bool isPreAmpsOn() const {return m_preAmpsOn;} 

  enum struct GlobReg : int { SME = 0, EventLimit, Trig_Count, CAE, ErrorMask_0, ErrorMask_1, PrmpVbp_R, BufVgOpAmp, GADCVref, PrmpVbp,
                       TDACVbp, DisVbn, Amp2Vbn, Amp2VbpFol, Amp2Vbp, FDACVbn, Amp2Vbpff, PrmpVbnFol, PrmpVbp_L, PrmpVbpf,
                       PrmpVbnLCC, S1, S0, /*S,*/ Pixel_latch_strobe, LVDSDrvIref, GADCCompBias, PllIbias, LVDSDrvVos, TempSensIbias,
                       PllIcp, PlsrIDACRamp, VrefDigTune, PlsrVgOpAmp, PlsrDACbias, VrefAnTune, Vthin_Coarse, Vthin_Fine,
                       HLD, DJO, DHS, PlsrDAC, CP0, CP1, /*CP,*/ Colpr_Addr, DisableColCnfg0, DisableColCnfg1, Trig_Lat, DisableColCnfg2,
                       CMDcnt12, /*CalPulseWidth, CalPulseDelay,*/ STC, HD1, HD0, PLL, EFS, STP, RER, ADC, SRR, HOR, CAL, SRC, LEN,
                       SRK, M13, LV0, EN_40M, EN_80M, c10, c11, c12, /*clk1,*/ c00, c01, c02, /*clk0,*/ EN_160, EN_320, N8b, c2o, EmptyRecordCnfg,
                       LVE, LV3, LV1, TM0, TM1, /*TM,*/ TMD, ILR, PlsrRiseUpTau, PPW, PlsrDelay, XDC, XAC, GADCSel, SELB0, SELB1, SELB2, b7E,
                       EFUSE, NGLOBALREGS };

  bool writeRegGlobal(GlobReg reg, int val) {
    switch(reg) {
      case GlobReg::SME:
        SME.write(val);
        break;
      case GlobReg::EventLimit:
        EventLimit.write(val);
        break;
      case GlobReg::Trig_Count:
        Trig_Count.write(val);
        break;
      case GlobReg::CAE:
        CAE.write(val);
        break;
      case GlobReg::ErrorMask_0:
        ErrorMask_0.write(val);
        break;
      case GlobReg::ErrorMask_1:
        ErrorMask_1.write(val);
        break;
      case GlobReg::PrmpVbp_R:
        PrmpVbp_R.write(val);
        break;
      case GlobReg::BufVgOpAmp:
        BufVgOpAmp.write(val);
        break;
      case GlobReg::GADCVref:
        GADCVref.write(val);
        break;
      case GlobReg::PrmpVbp:
        PrmpVbp.write(val);
        break;
      case GlobReg::TDACVbp:
        TDACVbp.write(val);
        break;
      case GlobReg::DisVbn:
        DisVbn.write(val);
        break;
      case GlobReg::Amp2Vbn:
        Amp2Vbn.write(val);
        break;
      case GlobReg::Amp2VbpFol:
        Amp2VbpFol.write(val);
        break;
      case GlobReg::Amp2Vbp:
        Amp2VbpFol.write(val);
        break;
      case GlobReg::FDACVbn:
        FDACVbn.write(val);
        break;
      case GlobReg::Amp2Vbpff:
        Amp2Vbpff.write(val);
        break;
      case GlobReg::PrmpVbnFol:
        PrmpVbnFol.write(val);
        break;
      case GlobReg::PrmpVbp_L:
        PrmpVbp_L.write(val);
        break;
      case GlobReg::PrmpVbpf:
        PrmpVbpf.write(val);
        break;
      case GlobReg::PrmpVbnLCC:
        PrmpVbnLCC.write(val);
        break;
      case GlobReg::S1:
        S1.write(val);
        break;
      case GlobReg::S0: /*S*/
        S0.write(val);
        break;
      case GlobReg::Pixel_latch_strobe:
        Pixel_latch_strobe.write(val);
        break;
      case GlobReg::LVDSDrvIref:
        LVDSDrvIref.write(val);
        break;
      case GlobReg::GADCCompBias:
        GADCCompBias.write(val);
        break;
      case GlobReg::PllIbias:
        PllIbias.write(val);
        break;
      case GlobReg::LVDSDrvVos:
        LVDSDrvVos.write(val);
        break;
      case GlobReg::TempSensIbias:
        TempSensIbias.write(val);
        break;
      case GlobReg::PllIcp:
        PllIcp.write(val);
        break;
      case GlobReg::PlsrIDACRamp:
        PlsrIDACRamp.write(val);
        break;
      case GlobReg::VrefDigTune:
        VrefDigTune.write(val);
        break;
      case GlobReg::PlsrVgOpAmp:
        PlsrVgOpAmp.write(val);
        break;
      case GlobReg::PlsrDACbias:
        PlsrDACbias.write(val);
        break;
      case GlobReg::VrefAnTune:
        VrefAnTune.write(val);
        break;
      case GlobReg::Vthin_Coarse:
        Vthin_Coarse.write(val);
        break;
      case GlobReg::Vthin_Fine:
        Vthin_Fine.write(val);
        break;
      case GlobReg::HLD:
        HLD.write(val);
        break;
      case GlobReg::DJO:
        DJO.write(val);
        break;
      case GlobReg::DHS:
        DHS.write(val);
        break;
      case GlobReg::PlsrDAC:
        PlsrDAC.write(val);
        break;
      case GlobReg::CP0:
        CP0.write(val);
        break;
      case GlobReg::CP1: /*CP,*/
        CP1.write(val);
        break;
      case GlobReg::Colpr_Addr:
        Colpr_Addr.write(val);
        break;
      case GlobReg::DisableColCnfg0:
        DisableColCnfg0.write(val);
        break;
      case GlobReg::DisableColCnfg1:
        DisableColCnfg1.write(val);
        break;
      case GlobReg::Trig_Lat:
        Trig_Lat.write(val);
        break;
      case GlobReg::DisableColCnfg2:
        DisableColCnfg2.write(val);
        break;
      case GlobReg::CMDcnt12: /*CalPulseWidth, CalPulseDelay,*/
        CMDcnt12.write(val);
        break;
      case GlobReg::STC:
        STC.write(val);
        break;
      case GlobReg::HD1:
        HD1.write(val);
        break;
      case GlobReg::HD0:
        HD0.write(val);
        break;
      case GlobReg::PLL:
        PLL.write(val);
        break;
      case GlobReg::EFS:
        EFS.write(val);
        break;
      case GlobReg::STP:
        STP.write(val);
        break;
      case GlobReg::RER:
        RER.write(val);
        break;
      case GlobReg::ADC:
        ADC.write(val);
        break;
      case GlobReg::SRR:
        SRR.write(val);
        break;
      case GlobReg::HOR:
        HOR.write(val);
        break;
      case GlobReg::CAL:
        CAL.write(val);
        break;
      case GlobReg::SRC:
        SRC.write(val);
        break;
      case GlobReg::LEN:
        LEN.write(val);
        break;
      case GlobReg::SRK:
        SRK.write(val);
        break;
      case GlobReg::M13:
        M13.write(val);
        break;
      case GlobReg::LV0:
        LV0.write(val);
        break;
      case GlobReg::EN_40M:
        EN_40M.write(val);
        break;
      case GlobReg::EN_80M:
        EN_80M.write(val);
        break;
      case GlobReg::c10:
        c10.write(val);
        break;
      case GlobReg::c11:
        c11.write(val);
        break;
      case GlobReg::c12: /*clk1,*/ 
        c12.write(val);
        break;
      case GlobReg::c00:
        c00.write(val);
        break;
      case GlobReg::c01:
        c01.write(val);
        break;
      case GlobReg::c02: /*clk0,*/ 
        c02.write(val);
        break;
      case GlobReg::EN_160:
        EN_160.write(val);
        break;
      case GlobReg::EN_320:
        EN_320.write(val);
        break;
      case GlobReg::N8b:
        N8b.write(val);
        break;
      case GlobReg::c2o:
        c2o.write(val);
        break;
      case GlobReg::EmptyRecordCnfg:
        EmptyRecordCnfg.write(val);
        break;
      case GlobReg::LVE:
        LVE.write(val);
        break;
      case GlobReg::LV3:
        LV3.write(val);
        break;
      case GlobReg::LV1:
        LV1.write(val);
        break;
      case GlobReg::TM0:
        TM0.write(val);
        break;
      case GlobReg::TM1: /*TM,*/
        TM1.write(val);
        break;
      case GlobReg::TMD:
        TMD.write(val);
        break;
      case GlobReg::ILR:
        ILR.write(val);
        break;
      case GlobReg::PlsrRiseUpTau:
        PlsrRiseUpTau.write(val);
        break;
      case GlobReg::PPW:
        PPW.write(val);
        break;
      case GlobReg::PlsrDelay:
        PlsrDelay.write(val);
        break;
      case GlobReg::XDC:
        XDC.write(val);
        break;
      case GlobReg::XAC:
        XAC.write(val);
        break;
      case GlobReg::GADCSel:
        GADCSel.write(val);
        break;
      case GlobReg::SELB0:
        SELB0.write(val);
        break;
      case GlobReg::SELB1:
        SELB1.write(val);
        break;
      case GlobReg::SELB2:
        SELB2.write(val);
        break;
      case GlobReg::b7E:
        b7E.write(val);
        break;
      case GlobReg::EFUSE:
        EFUSE.write(val);
        break;
      default:
        return false;
    }
    return true;
  };

  enum struct PixReg : int { Enable = 0, TDAC, LargeCap, SmallCap, HitBus, FDAC, NPIXELREGS };
  enum struct MergedGlobReg : int { CP = 0, CLK0, CLK1, HD, TM, NMERGEDGLOBREGS };

  static int constexpr nPixRegs = static_cast<int>(PixReg::NPIXELREGS);
  static int constexpr nGlobRegs = static_cast<int>(GlobReg::NGLOBALREGS);
  static int constexpr nMergedGlobRegs = static_cast<int>(MergedGlobReg::NMERGEDGLOBREGS);
  static int constexpr nPixCol = 80;
  static int constexpr nPixRow = 336;

#ifndef __XMK__

  /* This array holds the global registers we store in the configuration object under a given name (key) 
   * and the enum they refer to (value). Some FeI4 registers (CP, HD, TM, CLK0, and CLK1) are not addressed
   * individually, but via the merged register - these are in a seperate map. */ 
  //Decreased size of 12 for 2 times (CP, HD, TM) and 3 times (CLK0, CLK1) registers which we only
  //address via the merged registers and hence they don't have a name in the config
  //Additionally, BufVgOpAmp and GADCVref are the same register and M13 is derived from CMDcnt12, so these
  //two register are not stored, hence in total 14 registers are not addressed by this table
  static constexpr std::array<std::pair<std::string_view, GlobReg>, nGlobRegs-14> globRegNames {
   {{"TrigCnt"sv, Fei4::GlobReg::Trig_Count},
    {"Conf_AddrEnable"sv, Fei4::GlobReg::CAE},
    //{"Reg2Spare"sv, Fei4::GlobReg::},
    {"ErrMask0"sv, Fei4::GlobReg::ErrorMask_0},
    {"ErrMask1"sv, Fei4::GlobReg::ErrorMask_1},
    {"PrmpVbpRight"sv, Fei4::GlobReg::PrmpVbp_R},
    {"BufVgOpAmp"sv, Fei4::GlobReg::BufVgOpAmp},
    //{"Reg6Spare"sv, Fei4::GlobReg},
    {"PrmpVbp"sv, Fei4::GlobReg::PrmpVbp},
    {"TdacVbp"sv, Fei4::GlobReg::TDACVbp},
    {"DisVbn"sv, Fei4::GlobReg::DisVbn},
    {"Amp2Vbn"sv, Fei4::GlobReg::Amp2Vbn},
    {"Amp2VbpFol"sv, Fei4::GlobReg::Amp2VbpFol},
    //{"Reg9Spare"sv, Fei4::GlobReg::},
    {"Amp2Vbp"sv, Fei4::GlobReg::Amp2Vbp},
    {"FdacVbn"sv, Fei4::GlobReg::FDACVbn},
    {"Amp2Vbpf"sv, Fei4::GlobReg::Amp2Vbpff},
    {"PrmpVbnFol"sv, Fei4::GlobReg::PrmpVbnFol},
    {"PrmpVbpLeft"sv, Fei4::GlobReg::PrmpVbp_L},
    {"PrmpVbpf"sv, Fei4::GlobReg::PrmpVbpf},
    {"PrmpVbnLcc"sv, Fei4::GlobReg::PrmpVbnLCC},
    //{"Reg13Spare"sv, Fei4::GlobReg::},
    {"PxStrobes"sv, Fei4::GlobReg::Pixel_latch_strobe},
    {"S0"sv, Fei4::GlobReg::S0},
    {"S1"sv, Fei4::GlobReg::S1},
    {"LVDSDrvIref"sv, Fei4::GlobReg::LVDSDrvIref},
    {"GADCOpAmp"sv, Fei4::GlobReg::GADCCompBias},
    {"PllIbias"sv, Fei4::GlobReg::PllIbias},
    {"LVDSDrvVos"sv, Fei4::GlobReg::LVDSDrvVos},
    {"TempSensBias"sv, Fei4::GlobReg::TempSensIbias},
    {"PllIcp"sv, Fei4::GlobReg::PllIcp},
    //{"Reg17Spare"sv, Fei4::GlobReg::},
    {"PlsrIdacRamp"sv, Fei4::GlobReg::PlsrIDACRamp},
    {"VrefDigTune"sv, Fei4::GlobReg::VrefDigTune},
    {"PlsrVgOPamp"sv, Fei4::GlobReg::PlsrVgOpAmp},
    {"PlsrDacBias"sv, Fei4::GlobReg::PlsrDACbias},
    {"VrefAnTune"sv, Fei4::GlobReg::VrefAnTune},
    {"Vthin_AltCoarse"sv, Fei4::GlobReg::Vthin_Coarse},
    {"Vthin_AltFine"sv, Fei4::GlobReg::Vthin_Fine},
    {"PlsrDAC"sv, Fei4::GlobReg::PlsrDAC},
    {"DIGHITIN_Sel"sv, Fei4::GlobReg::DHS},
    {"DINJ_Override"sv, Fei4::GlobReg::DJO},
    {"HITLD_In"sv, Fei4::GlobReg::HLD},
    //{"Reg21Spare"sv, Fei4::GlobReg::},
    //{"Reg22Spare2"sv, Fei4::GlobReg::},
    {"Colpr_Addr"sv, Fei4::GlobReg::Colpr_Addr},
    //{"Reg22Spare1"sv, Fei4::GlobReg::},
    {"DisableColumnCnfg0"sv, Fei4::GlobReg::DisableColCnfg0},
    {"DisableColumnCnfg1"sv, Fei4::GlobReg::DisableColCnfg1},
    {"DisableColumnCnfg2"sv, Fei4::GlobReg::DisableColCnfg2},
    {"TrigLat"sv, Fei4::GlobReg::Trig_Lat},
    {"CMDcnt"sv, Fei4::GlobReg::CMDcnt12},
    {"StopModeCnfg"sv, Fei4::GlobReg::STC},
    {"EN_PLL"sv, Fei4::GlobReg::PLL},
    {"Efuse_sense"sv, Fei4::GlobReg::EFS},
    {"Stop_Clk"sv, Fei4::GlobReg::STP},
    {"ReadErrorReq"sv, Fei4::GlobReg::RER},
    //{"Reg27Spare1"sv, Fei4::GlobReg::},
    {"GADC_Enable"sv, Fei4::GlobReg::ADC},
    {"ShiftReadBack"sv, Fei4::GlobReg::SRR},
    //{"Reg27Spare2"sv, Fei4::GlobReg::},
    {"GateHitOr"sv, Fei4::GlobReg::HOR},
    {"CalEn"sv, Fei4::GlobReg::CAL},
    {"SR_clr"sv, Fei4::GlobReg::SRC},
    {"Latch_en"sv, Fei4::GlobReg::LEN},
    {"SR_Clock"sv, Fei4::GlobReg::SRK},
    {"LVDSDrvSet06"sv, Fei4::GlobReg::LV0},
    //{"Reg28Spare"sv, Fei4::GlobReg::},
    {"EN40M"sv, Fei4::GlobReg::EN_40M},
    {"EN80M"sv, Fei4::GlobReg::EN_80M},
    {"EN160M"sv, Fei4::GlobReg::EN_160},
    {"EN320M"sv, Fei4::GlobReg::EN_320},
    //{"Reg29Spare1"sv, Fei4::GlobReg::},
    {"no8b10b"sv, Fei4::GlobReg::N8b},
    {"Clk2OutCnfg"sv, Fei4::GlobReg::c2o},
    {"EmptyRecord"sv, Fei4::GlobReg::EmptyRecordCnfg},
    //{"Reg29Spare2"sv, Fei4::GlobReg::},
    {"LVDSDrvEn"sv, Fei4::GlobReg::LVE},
    {"LVDSDrvSet30"sv, Fei4::GlobReg::LV3},
    {"LVDSDrvSet12"sv, Fei4::GlobReg::LV1},
    {"TempSensDisable"sv, Fei4::GlobReg::TMD},
    {"IleakRange"sv, Fei4::GlobReg::ILR},
    //{"Reg30Spare"sv, Fei4::GlobReg::},
    {"PlsrRiseUpTau"sv, Fei4::GlobReg::PlsrRiseUpTau},
    {"PlsrPwr"sv, Fei4::GlobReg::PPW},
    {"PlsrDelay"sv, Fei4::GlobReg::PlsrDelay},
    {"ExtDigCalSW"sv, Fei4::GlobReg::XDC},
    {"ExtAnaCalSW"sv, Fei4::GlobReg::XAC},
    //{"Reg31Spare"sv, Fei4::GlobReg::},
    {"GADCSel"sv, Fei4::GlobReg::GADCSel},
    {"SELB0"sv, Fei4::GlobReg::SELB0},
    {"SELB1"sv, Fei4::GlobReg::SELB1},
    {"SELB2"sv, Fei4::GlobReg::SELB2},
    //{"Reg34Spare1"sv, Fei4::GlobReg::},
    {"PrmpVbpMsnEn"sv, Fei4::GlobReg::b7E},
    {"Chip_SN"sv, Fei4::GlobReg::EFUSE},
    //{"Reg1Spare"sv, Fei4::GlobReg::},
    {"SmallHitErase"sv, Fei4::GlobReg::SME},
    {"Eventlimit"sv, Fei4::GlobReg::EventLimit}}
  };
  //Check that the entire array is filled during compilation
  static_assert(!(globRegNames[globRegNames.size()-1].first.empty()));

  /* The five registers we only address via their combined register */
  static constexpr std::array<std::pair<std::string_view, MergedGlobReg>, nMergedGlobRegs> mergedGlobRegNames {
   {{"Colpr_Mode"sv, Fei4::MergedGlobReg::CP},
    {"HitDiscCnfg"sv, Fei4::MergedGlobReg::HD},
    {"CLK1"sv, Fei4::MergedGlobReg::CLK1},
    {"CLK0"sv, Fei4::MergedGlobReg::CLK0},
    {"TempSensDiodeSel"sv, Fei4::MergedGlobReg::TM}}
  };
  static_assert(!(mergedGlobRegNames[mergedGlobRegNames.size()-1].first.empty()));

  /* The pixel registers, both the latches (single bit) and the trim registers (TDAC, FDAC) 
   * Note that there is a mix of naming, the HitBus register is sometimes also called ILEAK*/
  static constexpr std::array<std::pair<std::string_view, PixReg>, nPixRegs> pixRegNames {
   {{"ENABLE"sv, Fei4::PixReg::Enable},
    {"CAP1"sv, Fei4::PixReg::LargeCap},
    {"CAP0"sv, Fei4::PixReg::SmallCap},
    {"ILEAK"sv, Fei4::PixReg::HitBus},
    {"TDAC"sv, Fei4::PixReg::TDAC},
    {"FDAC"sv, Fei4::PixReg::FDAC}}
  };
  static_assert(!(pixRegNames[pixRegNames.size()-1].first.empty()));

  /* From a register name (like it appears in the database) get the corresponding global register 
   * Throws a std::range_error if not found */
  static inline auto globreg_name_lookup(const std::string_view sv) {
    static constexpr auto map =
		ConstexprMap<std::string_view, GlobReg, globRegNames.size()>{{globRegNames}};
    return map.at(sv);
  }

  /* From a register name (like it appears in the database) get the corresponding merged global register
   * Throws a std::range_error if not found */
  static inline auto mergedGlobreg_name_lookup(const std::string_view sv) {
    static constexpr auto map = 
		ConstexprMap<std::string_view, MergedGlobReg, mergedGlobRegNames.size()>{{mergedGlobRegNames}};
    return map.at(sv);
  }

  /* From a register name (like it appears in the database) get the corresponding pixel register
   * Throws a std::range_error if not found */
  static inline auto pixreg_name_lookup(const std::string_view sv) {
    static constexpr auto map = 
		ConstexprMap<std::string_view, PixReg, pixRegNames.size()>{{pixRegNames}};
    return map.at(sv);
  }

  /* Holds the size (in bits) as the value of all the global registers (key)*/
  static constexpr std::array<std::pair<GlobReg, std::size_t>, nGlobRegs> globRegSizes {
   {{/*"TrigCnt",*/ Fei4::GlobReg::Trig_Count, 4},
    {/*"Conf_AddrEnable",*/ Fei4::GlobReg::CAE, 1},
    //{/*"Reg2Spare",*/ Fei4::GlobReg::},
    {/*"ErrMask0",*/ Fei4::GlobReg::ErrorMask_0, 16},
    {/*"ErrMask1",*/ Fei4::GlobReg::ErrorMask_1, 16},
    {/*"PrmpVbpRight",*/ Fei4::GlobReg::PrmpVbp_R, 8},
    {/*"BufVgOpAmp",*/ Fei4::GlobReg::BufVgOpAmp, 8},
    //{/*"Reg6Spare",*/ Fei4::GlobReg},
    {/*"PrmpVbp",*/ Fei4::GlobReg::PrmpVbp, 8},
    {/*"TdacVbp",*/ Fei4::GlobReg::TDACVbp, 8},
    {/*"DisVbn",*/ Fei4::GlobReg::DisVbn, 8},
    {/*"Amp2Vbn",*/ Fei4::GlobReg::Amp2Vbn, 8},
    {/*"Amp2VbpFol",*/ Fei4::GlobReg::Amp2VbpFol, 8},
    //{/*"Reg9Spare",*/ Fei4::GlobReg::},
    {/*"Amp2Vbp",*/ Fei4::GlobReg::Amp2Vbp, 8},
    {/*"FdacVbn",*/ Fei4::GlobReg::FDACVbn, 8},
    {/*"Amp2Vbpf",*/ Fei4::GlobReg::Amp2Vbpff, 8},
    {/*"PrmpVbnFol",*/ Fei4::GlobReg::PrmpVbnFol, 8},
    {/*"PrmpVbpLeft",*/ Fei4::GlobReg::PrmpVbp_L, 8},
    {/*"PrmpVbpf",*/ Fei4::GlobReg::PrmpVbpf, 8},
    {/*"PrmpVbnLcc",*/ Fei4::GlobReg::PrmpVbnLCC, 8},
    //{/*"Reg13Spare",*/ Fei4::GlobReg::},
    {/*"PxStrobes",*/ Fei4::GlobReg::Pixel_latch_strobe, 13},
    {/*"S0",*/ Fei4::GlobReg::S0, 1},
    {/*"S1",*/ Fei4::GlobReg::S1, 1},
    {/*"LVDSDrvIref",*/ Fei4::GlobReg::LVDSDrvIref, 8},
    {/*"GADCOpAmp",*/ Fei4::GlobReg::GADCCompBias, 8},
    {/*"PllIbias",*/ Fei4::GlobReg::PllIbias, 8},
    {/*"LVDSDrvVos",*/ Fei4::GlobReg::LVDSDrvVos, 8},
    {/*"TempSensBias",*/ Fei4::GlobReg::TempSensIbias, 8},
    {/*"PllIcp",*/ Fei4::GlobReg::PllIcp, 8},
    //{/*"Reg17Spare",*/ Fei4::GlobReg::},
    {/*"PlsrIdacRamp",*/ Fei4::GlobReg::PlsrIDACRamp, 8},
    {/*"VrefDigTune",*/ Fei4::GlobReg::VrefDigTune, 8},
    {/*"PlsrVgOPamp",*/ Fei4::GlobReg::PlsrVgOpAmp, 8},
    {/*"PlsrDacBias",*/ Fei4::GlobReg::PlsrDACbias, 8},
    {/*"VrefAnTune",*/ Fei4::GlobReg::VrefAnTune, 8},
    {/*"Vthin_AltCoarse",*/ Fei4::GlobReg::Vthin_Coarse, 8},
    {/*"Vthin_AltFine",*/ Fei4::GlobReg::Vthin_Fine, 8},
    {/*"PlsrDAC",*/ Fei4::GlobReg::PlsrDAC, 10},
    {/*"DIGHITIN_Sel",*/ Fei4::GlobReg::DHS, 1},
    {/*"DINJ_Override",*/ Fei4::GlobReg::DJO, 1},
    {/*"HITLD_In",*/ Fei4::GlobReg::HLD, 1},
    //{/*"Reg21Spare",*/ Fei4::GlobReg::},
    //{/*"Reg22Spare2",*/ Fei4::GlobReg::},
    {/*"Colpr_Addr",*/ Fei4::GlobReg::Colpr_Addr, 6},
    //{/*"Reg22Spare1",*/ Fei4::GlobReg::},
    {/*"DisableColumnCnfg0",*/ Fei4::GlobReg::DisableColCnfg0, 16},
    {/*"DisableColumnCnfg1",*/ Fei4::GlobReg::DisableColCnfg1, 16},
    {/*"DisableColumnCnfg2",*/ Fei4::GlobReg::DisableColCnfg2, 8},
    {/*"TrigLat",*/ Fei4::GlobReg::Trig_Lat, 8},
    {/*"CMDcnt",*/ Fei4::GlobReg::CMDcnt12, 13},
    {/*"StopModeCnfg",*/ Fei4::GlobReg::STC, 1},
    {/*"EN_PLL",*/ Fei4::GlobReg::PLL, 1},
    {/*"Efuse_sense",*/ Fei4::GlobReg::EFS, 1},
    {/*"Stop_Clk",*/ Fei4::GlobReg::STP, 1},
    {/*"ReadErrorReq",*/ Fei4::GlobReg::RER, 1},
    //{/*"Reg27Spare1",*/ Fei4::GlobReg::},
    {/*"GADC_Enable",*/ Fei4::GlobReg::ADC, 1},
    {/*"ShiftReadBack",*/ Fei4::GlobReg::SRR, 1},
    //{/*"Reg27Spare2",*/ Fei4::GlobReg::},
    {/*"GateHitOr",*/ Fei4::GlobReg::HOR, 1},
    {/*"CalEn",*/ Fei4::GlobReg::CAL, 1},
    {/*"SR_clr",*/ Fei4::GlobReg::SRC, 1},
    {/*"Latch_en",*/ Fei4::GlobReg::LEN, 1},
    {/*"SR_Clock",*/ Fei4::GlobReg::SRK, 1},
    {/*"LVDSDrvSet06",*/ Fei4::GlobReg::LV0, 1},
    //{/*"Reg28Spare",*/ Fei4::GlobReg::},
    {/*"EN40M",*/ Fei4::GlobReg::EN_40M, 1},
    {/*"EN80M",*/ Fei4::GlobReg::EN_80M, 1},
    {/*"EN160M",*/ Fei4::GlobReg::EN_160, 1},
    {/*"EN320M",*/ Fei4::GlobReg::EN_320, 1},
    //{/*"Reg29Spare1",*/ Fei4::GlobReg::},
    {/*"no8b10b",*/ Fei4::GlobReg::N8b, 1},
    {/*"Clk2OutCnfg",*/ Fei4::GlobReg::c2o, 1},
    {/*"EmptyRecord",*/ Fei4::GlobReg::EmptyRecordCnfg, 8},
    //{/*"Reg29Spare2",*/ Fei4::GlobReg::},
    {/*"LVDSDrvEn",*/ Fei4::GlobReg::LVE, 1},
    {/*"LVDSDrvSet30",*/ Fei4::GlobReg::LV3, 1},
    {/*"LVDSDrvSet12",*/ Fei4::GlobReg::LV1, 1},
    {/*"TempSensDisable",*/ Fei4::GlobReg::TMD, 1},
    {/*"IleakRange",*/ Fei4::GlobReg::ILR, 1},
    //{/*"Reg30Spare",*/ Fei4::GlobReg::},
    {/*"PlsrRiseUpTau",*/ Fei4::GlobReg::PlsrRiseUpTau, 3},
    {/*"PlsrPwr",*/ Fei4::GlobReg::PPW, 1},
    {/*"PlsrDelay",*/ Fei4::GlobReg::PlsrDelay, 6},
    {/*"ExtDigCalSW",*/ Fei4::GlobReg::XDC, 1},
    {/*"ExtAnaCalSW",*/ Fei4::GlobReg::XAC, 1},
    //{/*"Reg31Spare",*/ Fei4::GlobReg::},
    {/*"GADCSel",*/ Fei4::GlobReg::GADCSel, 3},
    {/*"SELB0",*/ Fei4::GlobReg::SELB0, 16},
    {/*"SELB1",*/ Fei4::GlobReg::SELB1, 16},
    {/*"SELB2",*/ Fei4::GlobReg::SELB2, 8},
    //{/*"Reg34Spare1",*/ Fei4::GlobReg::},
    {/*"PrmpVbpMsnEn",*/ Fei4::GlobReg::b7E, 1},
    {/*"Chip_SN",*/ Fei4::GlobReg::EFUSE, 16},
    //{/*"Reg1Spare",*/ Fei4::GlobReg::},
    {/*"SmallHitErase",*/ Fei4::GlobReg::SME, 1},
    {/*"Eventlimit",*/ Fei4::GlobReg::EventLimit, 8},
    //Let's add the twelve other regs we typically don't directly address
    //but there's no harm in adding them here
    {Fei4::GlobReg::CP0, 1},
    {Fei4::GlobReg::CP1, 1},
    {Fei4::GlobReg::HD0, 1},
    {Fei4::GlobReg::HD1, 1},
    {Fei4::GlobReg::TM0, 1},
    {Fei4::GlobReg::TM1, 1},
    {Fei4::GlobReg::c00, 1},
    {Fei4::GlobReg::c01, 1},
    {Fei4::GlobReg::c01, 1},
    {Fei4::GlobReg::c10, 1},
    {Fei4::GlobReg::c11, 1},
    {Fei4::GlobReg::c12, 1},
    //And the other two
    {Fei4::GlobReg::GADCVref, 8},
    {Fei4::GlobReg::M13, 1},
   }
  };

  /* Holds the size (in bits) as the value of all the merged global registers (key)
   * The size of the merged registers equals the sum of the individual, not merged registers */
  static constexpr std::array<std::pair<MergedGlobReg, std::size_t>, nMergedGlobRegs> mergedGlobRegSizes {
   {{/*"Colpr_Mode",*/ Fei4::MergedGlobReg::CP, 2},
    {/*"HitDiscCnfg",*/ Fei4::MergedGlobReg::HD, 2},
    {/*"CLK1",*/ Fei4::MergedGlobReg::CLK1, 3},
    {/*"CLK0",*/ Fei4::MergedGlobReg::CLK0, 3},
    {/*"TempSensDiodeSel",*/ Fei4::MergedGlobReg::TM, 2}}
  };

  /* Holds the size (in bits) as the value of all the pixel registers (key)*/
  static constexpr std::array<std::pair<PixReg, std::size_t>, nPixRegs> pixRegSizes {
   {{/*"ENABLE",*/ Fei4::PixReg::Enable, 1},
    {/*"CAP1",*/ Fei4::PixReg::LargeCap, 1},
    {/*"CAP0",*/ Fei4::PixReg::SmallCap, 1},
    {/*"ILEAK",*/ Fei4::PixReg::HitBus, 1},
    {/*"TDAC",*/ Fei4::PixReg::TDAC, 5},
    {/*"FDAC",*/ Fei4::PixReg::FDAC, 4}}
  };

  /* From a global register (the enum) get the corresponding size of the register (in bits)
   * Throws a std::range_error if not found */
  static inline auto globreg_size_lookup(const GlobReg reg) {
    static constexpr auto map =
		ConstexprMap<GlobReg, std::size_t, globRegSizes.size()>{{globRegSizes}};
    return map.at(reg);
  }

  /* From a merged global register (the enum) get the corresponding size of the register (in bits)
   * Throws a std::range_error if not found */
  static inline auto mergedGlobreg_size_lookup(const MergedGlobReg reg) {
    static constexpr auto map = 
		ConstexprMap<MergedGlobReg, std::size_t, mergedGlobRegSizes.size()>{{mergedGlobRegSizes}};
    return map.at(reg);
  }

  /* From a pixel register (the enum) get the corresponding size of the register (in bits)
   * Throws a std::range_error if not found */
  static inline auto pixreg_size_lookup(const PixReg reg) {
    static constexpr auto map = 
		ConstexprMap<PixReg, std::size_t, pixRegSizes.size()>{{pixRegSizes}};
    return map.at(reg);
  }
#endif


};

#endif
