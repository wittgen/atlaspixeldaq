#include "Fei3GlobalCfg.h"
#include "BitUtils.h"

Fei3GlobalCfg::Fei3GlobalCfg() {
}

Fei3GlobalCfg::Fei3GlobalCfg(const Fei3GlobalCfg &rhs) {
    std::copy(rhs.globcfg, rhs.globcfg + nWordsGlobal, globcfg);
}

Fei3GlobalCfg & Fei3GlobalCfg::operator=(const Fei3GlobalCfg &rhs) {
    std::copy(rhs.globcfg, rhs.globcfg + nWordsGlobal, globcfg);
    return *this;
}

void Fei3GlobalCfg::dumpFei3GlobalCfg(){
for(size_t reg = 0 ; reg < Fei3::nGlobRegs ; ++reg)
    std::cout << Fei3GlobalMaps::globRegNames[reg] << " with pos " << Fei3GlobalMaps::posMap[reg]
              << ": " << this->readRegGlobal( static_cast<Fei3::GlobReg>(reg) ) << std::endl;
}

void Fei3GlobalCfg::writeRegGlobal(Fei3::GlobReg reg, uint32_t val) {
    if (static_cast<int>(reg) >= Fei3::nGlobRegs) return;

    int regIx = static_cast<int>(reg);
    for (int i = 0; i < Fei3GlobalMaps::lenMap[regIx]; i++) {
        int word = (int) ((Fei3GlobalMaps::posMap[regIx] + i) / 32);
        int bit = (Fei3GlobalMaps::posMap[regIx] + i) % 32;
        uint32_t resetmask = ~(0x1 << bit);
        uint32_t mask = ((val >> i) & 0x1) << bit;
        globcfg[word] &= resetmask;
        globcfg[word] |= mask;
    }
}

uint32_t Fei3GlobalCfg::readRegGlobal(Fei3::GlobReg reg) const{

    int regIx = static_cast<int>(reg);
    if (regIx >= Fei3::nGlobRegs) return 0;
    uint32_t retVal = 0;

    for (int i = 0; i < Fei3GlobalMaps::lenMap[regIx]; i++) {
        int word = (int) ((Fei3GlobalMaps::posMap[regIx] + i) / 32);
        int bit = (Fei3GlobalMaps::posMap[regIx] + i) % 32;
        int val = (globcfg[word] >> bit) & 0x1;
        retVal |= val << i;
    }
    return retVal;
}

bool Fei3GlobalCfg::operator==(const Fei3GlobalCfg &comp) const{
    for(size_t reg = 0 ; reg < Fei3::nGlobRegs ; ++reg){
        Fei3::GlobReg regT = static_cast<Fei3::GlobReg>(reg);
        if(regT == Fei3::GlobReg::HitBusScaler || regT ==  Fei3::GlobReg::GlobalParity || regT == Fei3::GlobReg::Select_DO)continue;//Skip HitBusScaler and GlobalParity
        uint32_t regVal = readRegGlobal(regT);
        if (regT == Fei3::GlobReg::MonLeakADC) {//Skip reg address 51 ADC comparator status
            if((regVal & 0x1FFF) != (comp.readRegGlobal(regT) & 0x1FFF) ){
                std::cout << "Mistmatch on register "<< Fei3GlobalMaps::globRegNames[reg] << " I got 0x" << std::hex << comp.readRegGlobal(regT)
                          << " while I was expecting 0x" << regVal << std::endl;
                return false;
            }
        } else {
            if(regVal != comp.readRegGlobal(regT) ){
                std::cout << "Mistmatch on register " << Fei3GlobalMaps::globRegNames[reg] << " I got 0x" << std::hex << comp.readRegGlobal(regT)
                          << " while I was expecting 0x" << regVal << std::endl;
                return false;
            }
        }
    }
    return true;
}

uint32_t Fei3GlobalCfg::getGlobalRegHash() const {
    uint32_t hash = 0x0;
    for(size_t reg = 0; reg < Fei3::nGlobRegs; ++reg){
        //We skip certain registers which are allowed to change and must not impact the hash
        if( reg == static_cast<int>(Fei3::GlobReg::GlobalParity) ||
            reg == static_cast<int>(Fei3::GlobReg::Latency) ||
            reg == static_cast<int>(Fei3::GlobReg::VCal) ) continue;
        if( reg == static_cast<int>(Fei3::GlobReg::MonLeakADC)) {
            //ADC comperatus status register is excluded
            hash ^= lowbias32(static_cast<uint32_t>(readRegGlobal(static_cast<Fei3::GlobReg>(reg)) & 0x1FFF));
        } else {
            hash ^= lowbias32(static_cast<uint32_t>(readRegGlobal(static_cast<Fei3::GlobReg>(reg))));
        }
    }
    return hash;
}
