
#include "Fei4Data.h"
#include "RxLink.h"

#include <iostream>
#include <iomanip>
#include <chrono>
#include <thread>


bool Fei4Data::m_verbose = false;
bool Fei4Data::m_verboseSR = false;
bool Fei4Data::m_rawDataDump = false;
unsigned int  Fei4Data::m_maxRawDataDump = 30;
unsigned int  Fei4Data::m_maxStartFrameCount = 10;
bool Fei4Data::m_error = false;
unsigned int Fei4Data::m_maxFrameSize = 300;

const unsigned Fei4Hist::ncol = 80;
const unsigned Fei4Hist::nrow = 336;

const std::map<int, std::string> Fei4Data::m_serviceRecordMap = { 
  {31, "EFUSE_errorflag"}, 
  {30, "AddrErr"}, 
  {29, "CmdSeu"}, 
  {28, "BitFlip"}, 
  {27, "CmdErr"}, 
  {26, "AddrErr"}, 
  {25, "WrRegDataErr"}, 
  {24, "Triple redundant mismatch"}, 
  {23, "Reference clock phase faster than PLL"}, 
  {22, "PLL generated clock phase faster than reference"}, 
  {21, "RA2b reset bar pulsed"}, 
  {20, "not used "}, 
  {19, "not used "}, 
  {18, "not used "}, 
  {17, "not used "}, 
  {16, "Truncated event flag and counter"}, 
  {15, "Skipped trigger counter"}, 
  {14, "3 MSBs of bunch counter and 7 MSBs of L1A counter"}, 
  {13, "not used "}, 
  {12, "not used "}, 
  {11, "not used "}, 
  {10, "HitOr"}, 
  {9, "Fifo_Full"}, 
  {8, "ReadOut_Processor_Error"}, 
  {7, "L1_Trig_Id_Error"}, 
  {6, "L1_Register_Error"}, 
  {5, "L1 Request CounterError"}, 
  {4, "L1_in Counter Error"}, 
  {3, "Hamming Error in word 2"}, 
  {2, "Hamming Error in word 1"}, 
  {1, "Hamming Error in word 0"}, 
  {0, "BCID counter error"}
};

void Fei4Data::printServiceRecord(uint8_t byte1, uint8_t byte2) {
  uint32_t code = ((0xFC & byte1)>>2);
  uint32_t value = (((0x03 & byte1)<<8) + ((0xFF & byte2)));
  // hard-coded number of service records in map
  // Odd syntax for serviceRecordMap is for const correctness. 
  // Can change to m_serviceRecordMap.at(code) in C++11
  if (code >= 0 && code < 32) {
    std::cout<<"SR "<<(int)code<<": "<< m_serviceRecordMap.find(code)->second <<" with value: "<<(int)value<<std::endl;
  }
  return;
}

uint8_t Fei4Data::decodeToT(uint8_t ToT, uint8_t HitDiscCnf) {
  if (HitDiscCnf <= 2) {
    if (ToT <= 13) 
      return ToT + HitDiscCnf + 1;
    else if (ToT == 14)
      return HitDiscCnf;
  }
  return 0;
}


bool Fei4Data::readPixelSRData(RxLink& link, std::vector<int>& rawData, std::vector<int>& answer){

  
  

  int counter = 0;
  int sameCounter = 0;
  int byte = rawData.back();
  int lastByte = 0x0;
  bool haveSR = false;
  unsigned int frameSize = 0;
  
  std::vector<int> serviceRecord;

  // for end of frame. We need to skip eventual IDLE signals in between. This is due to bc->fc sequence and close properly on bc->bc sequence. 
  // make sure count is correct
  
  while((frameSize < m_maxFrameSize)) {  //bytes. I will never read more than 300 bytes anyway
    //read the raw data
    rawData.push_back(link.read());
    //make a byte out of it
    byte = rawData.back() & 0xFF;
    counter++;
    frameSize++;
    
    
    if (byte==0xBC && counter%3 == 0) { // possible interleaved IDLE signal or possible and of frame.
            
      rawData.push_back(link.read());
      byte = rawData.back() & 0xFF;
      if (byte==0xBC) //end of the frame
	return true;
      else
	counter--; //to realign with the 24-bit word. Potential risk...
    }
      else {  //normal byte word 
	
	
	
	if (lastByte == byte) sameCounter++;
	else sameCounter = 0;
	lastByte = byte;    
	if (sameCounter > 50) {
	  if (m_verbose) std::cout<<"Found the same character, "<<lastByte<<" more than 10 times in a row, aborting."<<std::endl;
	  return false;
	}
    
	// store 3 bytes of SR
	if (counter%3 == 0 && byte == 0xEF) haveSR = true;
	else if (counter%3 ==0) haveSR = false;
	if (haveSR) serviceRecord.push_back(byte);
	
	answer.push_back(byte);
    
      }// If you don't have BC at the right location
    
  } // too much to read. 
  
  if (m_verboseSR) {
    for (unsigned int i = 0; i<serviceRecord.size()/3; i++)
      if (serviceRecord.size() >= (3*i+2)) printServiceRecord(serviceRecord[3*i+1], serviceRecord[3*i+2]);
  }
  return true;
}


//The IDLES are filtered away by the FW, that's why I don't see them here.
//It can be that a large DataRecord is interleaved by IDLE signals. You would see something like bc->(3c)->fc
bool Fei4Data::readDataRecord(RxLink& link, std::vector<int>& rawData, std::vector<int>& answer){
  int counter = 0;
  int sameCounter = 0;
  int byte = rawData.back();
  int lastByte = 0x0;
  bool haveSR = false;
  unsigned int frameSize = 0;
  
  std::vector<int> serviceRecord;

  // for end of frame
  // make sure count is correct
  while(!(byte==0xBC && counter%3 == 0) && (frameSize < m_maxFrameSize)) {  //byte
    if (m_verbose) std::cout<<"frame size (byte) = "<<(int)frameSize<<" and maxFrameSize (bytes) = "<<(int)m_maxFrameSize<<std::endl;
      rawData.push_back(link.read());
    byte = rawData.back() & 0xFF;
    counter++;

    if (lastByte == byte) sameCounter++;
    else sameCounter = 0;
    lastByte = byte;    
    if (sameCounter > 10) {
      if (m_verbose) std::cout<<"Found the same character, "<<lastByte<<" more than 10 times in a row, aborting."<<std::endl;
      return false;
    }

    // store 3 bytes of SR
    if (counter%3 == 0 && byte == 0xEF) haveSR = true;
    else if (counter%3 ==0) haveSR = false;
    if (haveSR) serviceRecord.push_back(byte);
    
    // don't push back the final 0xBC
    if (!(byte==0xBC && counter%3 == 0)) answer.push_back(byte);

    frameSize++;
  } // while you don't have a BC at the right location

  if (m_verboseSR) {
    for (unsigned int i = 0; i<serviceRecord.size()/3; i++)
      if (serviceRecord.size() >= (3*i+2)) printServiceRecord(serviceRecord[3*i+1], serviceRecord[3*i+2]);
  }
  return true;
}

void Fei4Data::dumpRawData(RxLink& link, std::vector<int> rawData, int frameCount, bool goodFrame) {
  // if it's a good frame, don't read out extra data
  if (!goodFrame) {
    while (rawData.size() < m_maxRawDataDump) {
      rawData.push_back(link.read());
    }
  }

  if (rawData.size() == 0) {
    std::cout<<"no raw data to dump, sorry"<<std::endl;
    return;
  }

  std::cout<<"********** RAW DATA FROM FRAME: "<<frameCount<<" has "<<rawData.size()<<" bytes: *************"<<std::endl;
  for (unsigned int i = 0; i<rawData.size(); i++) {
    if (i%3 == 0) std::cout<<"0x";
    std::cout<<std::hex<<rawData[i];
    if (i%3 == 2) std::cout<<std::endl;
  }
  std::cout<<std::endl;
  std::cout<<"**************************************************"<<std::endl;
  return;
}

struct printTOT {
  // Prints out the row, col, and tot.                                                           
  void operator()(int col, int row, int tot) { std::cout<<std::dec<<" hit in column: "<<(int)col\
							<<", row: "<<(int)row<<", and tot: "<<(int)tot<<std::endl;
    if (col > 80 || row > 336) std::cout<<"WARNING! HAVE INVLAID HIT WITH col: "<<(int)col<<", r\
ow: "<<(int)row<<", and tot: "<<(int)tot<<std::endl; }
};

struct lookForError {
  void operator()(int col, int row, int tot) {
    if (col > 80 || row > 336) {
      std::cout<<"found error!"<<std::endl;
      Fei4Data::m_error = true;
    }
      return;
  }
};

// Using this template and functors to avoid copying/pasting
// the loop structure. class F implements operator() and stores
// any necessary output.
template<class F>
void loopPixData(const std::vector<int>& frame, int HitDiscCnf, F func){
  if (frame.size() < 3 || frame[0] != 0xe9) {
    return;
  }

  int col, row, tot;

  for (unsigned int i=3; i<frame.size(); i+=3) {
    if ( frame[i] == 0xef) {
      if (Fei4Data::verboseSR()) Fei4Data::printServiceRecord(frame[i+1], frame[i+2]);
      continue;
    }
    
    col = ((0xFE & frame[i]) >> 1); 
    row = ((0x01 & frame[i]) << 8); 

     if (frame.size() > i) row += (0xFF & frame[i+1]);
    else{
      std::cout <<"WARNING!!! Data word not expected size"<<std::endl;
      return;
    }
    if (frame.size() > i+1) tot = Fei4Data::decodeToT((0xF0 & frame[i+2]) >> 4, HitDiscCnf); 
    else{
      std::cout <<"WARNING!!! Data word not expected size"<<std::endl;
      return;
    }

    // tmp lj
    if (col > 80 || row > 336) {
      std::cout<<"LJ Warning! Have hit: 0x"<<std::hex<<(int)frame[i]<<(int)frame[i+1]<<(int)frame[i+2]<<std::dec<<std::endl;
      std::cout<<"dumping frame: "<<std::endl;
      for (unsigned int j=3; j<frame.size(); j+=3) {
	std::cout<<"0x"<<std::hex<<std::setw(2)<<frame[j]<<std::setw(2)<<frame[j+1]<<std::setw(2)<<frame[j+2]<<std::dec<<std::endl; 
      std::this_thread::sleep_for(std::chrono::microseconds(10));
    }
      std::cout<<std::endl;
    }

    //tot2 = decodeToT(0x0F & frame[i+2], HitDiscCnf);
    func(col, row, tot);
  }
  return;
}

std::vector<int> Fei4Data::readFrame(RxLink& link, bool raw,bool shiftRegisterReadBack) {
  static int frameCounter = 0;
  std::vector<int> rawData;

  //m_verbose = true;
#ifdef LL_DEBUG
  std::cout<<"Entering function: "<<__PRETTY_FUNCTION__<<std::endl;
  std::cout<<"Attempting to read frame: "<<frameCounter+1<<std::endl;
  std::cout<<"Reading from channel: "<<(int)link.getRxCh()<<std::endl;
#endif

  std::vector<int> answer;
  uint8_t byte = 0;
  bool goodFrame = false;
  
  frameCounter++;

  // Look for a start of fram character, up until m_maxStartFrameCount attempts
  do {
    if (m_verbose) std::cout<<"byte: "<< rawData.size() << "has value: " << HEX(int(byte)) << std::endl;    
    rawData.push_back(link.read());
    byte = rawData.back() & 0xFF;
  } while(rawData.size() < m_maxStartFrameCount && byte != 0xFC);

  if (rawData.size() == m_maxStartFrameCount) {
    if (m_verbose) std::cout<<"WARNING: Did not find a start of frame 0xFC after "<<m_maxStartFrameCount
			  <<" bytes, returning empty frame"<<std::endl;
    if (m_rawDataDump) dumpRawData(link, rawData, frameCounter, goodFrame);
    return answer;
  }

  // once you find a start of frame character, see what type of data you are reading
  rawData.push_back(link.read());
  byte = rawData.back() & 0xFF;

  answer.push_back(byte);

  if (byte == 0xE9 || //Data Header
      byte == 0xEA || //Address Record
      byte == 0xEC || //Value Record
      byte == 0xEF || //Service Record
      byte == 0x3C){   //Idle State (K28.1 comma)
    if (!shiftRegisterReadBack)
      goodFrame = readDataRecord(link, rawData, answer);
    else
      goodFrame = readPixelSRData(link, rawData, answer);
  }
  else if (m_verbose) {
    std::cout<<"Potentially a corrupt frame; did not find a valid record following the start of frame; returning empty frame"<<std::endl;
  }

  if (m_rawDataDump) {
    m_error = false;
    loopPixData(answer,0,lookForError());
  if (m_error) dumpRawData(link, rawData, frameCounter, goodFrame);
  }

  if (raw) return rawData;
  else return answer;
}

// Legacy function needed by IblScan.cxx for debugPrintNHits
uint32_t Fei4Data::decodePixData(const std::vector<int>& frame) {
  if (frame.size() < 3 || frame[0] != 0xe9) {
    return 0;
  }

  return ((frame.size()-3)/3);
}

void Fei4Data::printPixData(const std::vector<int>& frame, int HitDiscCnf) {
  loopPixData(frame, HitDiscCnf, printTOT());
}

struct fillArray {
  // Fills an array of size [nCol][336] starting at startCol with summed TOT
  fillArray(int nCol, int startCol, uint8_t array[][336]) : m_nCol(nCol), m_startCol(startCol), m_array(array){}
  void operator()(int col, int row, int tot) {
    if ((col <= (2*m_startCol+m_nCol)) && (col > 2*m_startCol)) m_array[(col-(m_startCol*2)-1)][row-1] += tot;
  }
private:
  int m_nCol;
  int m_startCol;
  uint8_t (*m_array)[336];
};

struct fillOccArray {
  // Fills an array of size [nCol][336] with summed OCC
  fillOccArray(uint16_t array[][336]) : m_array(array) {} 
  void operator()(int col, int row, int tot) {
    if (col > 80 || col < 0 || row > 336 || row < 0) return;
    m_array[(col-1)][(row-1)] ++;
  }
private:
  uint16_t (*m_array)[336];
};

void Fei4Data::fillPixDataArray(const std::vector<int>& frame, int HitDiscCnf, uint8_t tot[4][336], int startCol) {
  loopPixData(frame, HitDiscCnf, fillArray(4, startCol, tot));
}

void Fei4Data::fillPixDataArray(const std::vector<int>& frame, int HitDiscCnf, uint16_t occ[80][336]) {
  loopPixData(frame, HitDiscCnf, fillOccArray(occ));
}


struct fillHist {
  // Fills an Hist with occupancy or summed TOT. 
  fillHist(Fei4Hist& hist) : m_hist(hist) {}
  void operator()(unsigned col, unsigned row, int tot) {
    if (col <= 0 || col > Fei4Hist::ncol || row <= 0 || row > Fei4Hist::nrow) {
      std::cout << "*** WARNING *** Requested to fill invalid col: " << col << " or row: " << row << " with tot:" << tot << std::endl;
      return;
    }
    switch (m_hist.data) {
    case Fei4Data::OCC:
      m_hist(col-1,row-1) += 1;
      break;
    case Fei4Data::TOT:
      m_hist(col-1,row-1) += tot;
      break;
    }
  }
 private:
  Fei4Hist& m_hist;
};

void Fei4Data::fillPixDataHist(const std::vector<int>& frame, int HitDiscCnf, Fei4Hist& hist){
  loopPixData(frame, HitDiscCnf, fillHist(hist));
}

Fei4Hist::Fei4Hist(const Fei4Data::DataType d, const Fei4Data::FIFOType f, const uint8_t rx) : data(d), fifo(f), rxCh(rx), m_internal(std::vector<uint16_t>(nrow*ncol,0)) {
}

void Fei4Hist::clear() {
std::fill(m_internal.begin(), m_internal.end(), 0);
}

