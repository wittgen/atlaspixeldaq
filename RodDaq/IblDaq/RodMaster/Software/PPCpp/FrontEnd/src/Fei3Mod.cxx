#include "Fei3Mod.h"
#include "IblBocRxFifo.h"
#include "IblRodInmemFifo.h"

#include "RodResourceManager.h"
#include <bitset>


#ifdef __XMK__
#include "BitUtils.h"
#endif

Fei3Mod::Fei3Mod() : Fei3ModCfg(), Fei3ModCmd(), m_link(nullptr), m_verbosity(0) {
    setFeLink(RodResourceManagerPIX::getDevNullFeLink());
//#define LL_DEBUG
#ifdef LL_DEBUG
    m_verbosity = 11;
#endif
m_maskPixelEna=false;

}

Fei3Mod::~Fei3Mod() {
}

void Fei3Mod::sendMCCConfig() {
    Fei3ModCmd::mccGlobalResetMCC();
    writeMCCRegister(CSR,   mccRegisters[CSR],   false);
    writeMCCRegister(LV1,   mccRegisters[LV1],   false);
    writeMCCRegister(FEEN,  mccRegisters[FEEN],  false);
    writeMCCRegister(WFE,   mccRegisters[WFE],   false);
    writeMCCRegister(WMCC,  mccRegisters[WMCC],  false);
    writeMCCRegister(CNT,   mccRegisters[CNT],   false);
    writeMCCRegister(CAL,   mccRegisters[CAL],   false);
    writeMCCRegister(PEF,   mccRegisters[PEF],   false);
    writeMCCRegister(SBSR,  mccRegisters[SBSR],  false);
    writeMCCRegister(WBITD, mccRegisters[WBITD], false);
    writeMCCRegister(WRECD, mccRegisters[WRECD], false);
    getTxLink()->send();
}

void Fei3Mod::readMCCRegister(MCCRegisters reg) {
    mccRdRegister(reg);

    std::vector<uint8_t> rxData;
    rxData.reserve(8);
    if( m_link ) {
      for(size_t k = 0 ; k < rxData.capacity() ; ++k)
        rxData.push_back( m_link->read8() );
    }

    uint32_t rxDataWord = 0x0;
    for(size_t k = 0 ; k < rxData.size() ; ++k) {
      if( rxData[k] == 0x00 ) continue;
      rxDataWord = rxData[k] << 24 | rxData[k+1] << 16 | rxData[k+2] << 8 | rxData[k+3];
      break;
    }

    mccRegisters[reg] = ((rxDataWord >> 11) & 0xFFFF);
}

void Fei3Mod::writeMCCRegister(MCCRegisters reg, uint16_t val, bool doSend) {
    mccRegisters[reg] = val;
    Fei3ModCmd::mccWrRegister(reg, val, doSend);
}

void Fei3Mod::sendMCCRegister(MCCRegisters reg, uint16_t val, bool doSend) {
    Fei3ModCmd::mccWrRegister(reg, val, doSend);
}

void Fei3Mod::CALset(uint8_t delayRange, bool overrideRange, uint8_t delay, bool overrideDelay) {
  // strange use of strobe delay range to communicate whether or not to use scan strobe dealy or module config strobe delay
  uint32_t regVal = (1 << 10 ) | (mccRegisters[CAL] & 0x3ff); //  Make local copy from array value.

  if(overrideRange){
    regVal &= 0x43f;
    regVal |= ((delayRange & 0xf) << 6);
  }
  if(overrideDelay){
    regVal &= 0x7c0;
    regVal |= delay & 0x3f;
  }
  writeMCCRegister(CAL,regVal);//Send MCC config
  
  //std::cout<<"CAL: 0x"<<std::hex<<regVal <<std::dec<<" range "<<(int)((regVal & 0x3c0) >> 6)<<" and delay "<<(int)(regVal & 0x3f)<<std::endl;
}

void Fei3Mod::addPattern(uint32_t pattern, uint32_t nPattern) {
    uint32_t word = 0;
    word = 1 << 25;
    word |= (pattern & 0x1FFFFF);
    word <<= 6;
    writeMCCRegister(FEEN, 0xFFFF); // enable all FEs
    writeMCCRegister(CNT, 0x0004); // write to CNT to accept 32 bits
    std::vector<uint32_t> data; data.push_back(word);
    for (uint32_t i = 0; i < nPattern; i++) {
        Fei3ModCmd::mccWrReceiver(data); // write word to enabled receiver
    }
    //writeMCCRegister(FEEN, 0x0000); // disable all FEs
}

void Fei3Mod::addHit(uint16_t fe_mask, uint8_t bcid, uint8_t row, uint8_t col, uint8_t tot) {
    if (row >= 160 || col >= 18) return;
    uint32_t word = 0;
    word |= 0x1 << 25; // header
    word |= ((bcid >> 4) & 0xF) << 21; // BCID [7:4]
    word |= (row & 0xFF) << 13; // row
    word |= (col & 0x1F) << 8; // column
    word |= tot & 0xFF; // tot
    word <<= 6; // set to the MSB
    writeMCCRegister(FEEN, fe_mask); // enable FE
    writeMCCRegister(CNT, 0x0004); // write to CNT to accept 32 bits
    std::vector<uint32_t> data; data.push_back(word);
    Fei3ModCmd::mccWrReceiver(data); // write word to enabled receiver
}

void Fei3Mod::addEOEWarning(uint16_t fe_mask, uint8_t bcid, uint8_t feflag, uint8_t parity, uint8_t l1id) {
    if (parity >= 2 || l1id >= 16) return;
    uint32_t word = 0;
    word |= 0x1 << 25; // header
    word |= (bcid & 0xF) << 21; // BCID [3:0]
    word |= 0xD << 17; // 1110 for EOEWarning
    word |= (feflag & 0xF) << 13; // FE-Flag [3:0]
    word |= (parity & 0x1) << 12; // Parity Error
    word |= (l1id & 0xF) << 8; // LV1ID
    word |= ((bcid >> 4) & 0xF) << 4; // BCID [7:4]
    word |= ((feflag >> 4) & 0xF); // FE-Flag [7:4]
    word <<= 6; // set to the MSB
    std::vector<uint32_t> data; data.push_back(word);
    Fei3ModCmd::mccWrReceiver(data); // write word to enabled receiver
}

void Fei3Mod::addEOE(uint16_t fe_mask, uint8_t bcid) {
    uint32_t word = 0;
    word |= 0x1 << 12; // header
    word |= (bcid & 0xF) << 8; // BCID [3:0]
    word |= 0xF << 4; // 1111 for EOE
    word |= 0x0; // FE-Flag [3:0] = 0 for a good EOE
    word <<= 19; // set to the MSB
    std::vector<uint32_t> data; data.push_back(word);
    Fei3ModCmd::mccWrReceiver(data); // write word to enabled receiver
}

void Fei3Mod::sendGlobalConfig(int iFE) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << std::endl;

    // compute parity
    writeGlobalReg(iFE, Fei3::GlobReg::GlobalParity, 0); // reset parity
    writeGlobalReg(iFE, Fei3::GlobReg::HitBusScaler, 0); // reset read-only HitBusScaler
    uint16_t curMonLeakADC = m_chipCfgs[iFE].readRegGlobal(Fei3::GlobReg::MonLeakADC);
    curMonLeakADC &= 0x1FFF;
    writeGlobalReg(iFE, Fei3::GlobReg::MonLeakADC, curMonLeakADC); // reset read-only MonLeakADC(13)
    int nOnes = 0;
    for (unsigned iword = 0; iword < Fei3GlobalCfg::nWordsGlobal; iword++) nOnes += __builtin_popcount(m_chipCfgs[iFE].globcfg[iword]); // count ones
    writeGlobalReg(iFE, Fei3::GlobReg::GlobalParity, (nOnes % 2 == 0) ? 0 : 1); // set parity

    writeMCCRegister(CNT, (231 << 3) | 0x4, false); // write to CNT the length of the data
    std::vector<uint32_t> clockGlobalData;
    Fei3Cmd::clockGlobal(iFE, clockGlobalData, m_chipCfgs[iFE].globcfg);
#ifdef __XMK__
#ifdef LL_DEBUG
std::cout << "ClockData" << std::endl;
dumpData(clockGlobalData);
std::cout << "ClockData" << std::endl;
#endif
#endif
    Fei3ModCmd::mccWrFrontEnd(clockGlobalData, false); // clock the global config to the strobes
    writeMCCRegister(CNT, 0x4, false); // write to CNT the length of the WriteGlobal command
    std::vector<uint32_t> writeGlobalData;
    Fei3Cmd::writeGlobal(iFE, writeGlobalData);
#ifdef __XMK__
#ifdef LL_DEBUG
std::cout << "WriteGlobal" << std::endl;
dumpData(writeGlobalData);
std::cout << "WriteGlobal" << std::endl;
#endif
#endif
    Fei3ModCmd::mccWrFrontEnd(writeGlobalData, false); // write the global config
    getTxLink()->send();
}

// Todo: clean this up by using, e.g. vectors
// Write the SAME bitstream to each DC
void Fei3Mod::strobePixelLatchesDC(int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[1][nWordsPerDC]) {
    for(uint8_t iDC = 0 ; iDC < Fei3PixelCfg::nDC ; ++iDC)
      strobePixelLatchesDC(iFE, pixReg, iDC, bitstream);
}

void Fei3Mod::strobePixelLatchesDC(int iFE, Fei3::PixLatch pixReg, uint8_t iDC, uint32_t bitstream[1][nWordsPerDC]) {
    setAllPixelShiftRegisters(iFE, false); //  These calls done more effectively outside this function ?
    writeGlobalReg(iFE, Fei3GlobalCfg::colEnReg(iDC), true); //  These calls done more effectively outside this function ?
    sendGlobalConfig(iFE); // send the global config with these DCs enabled //  These calls done more effectively outside this function ?
    strobePixelLatches(iFE, pixReg, 1, bitstream);
}

// This function receivs an array of nDCs times 320 bit (i.e. nDCs double columns) in 10 32-bit words
// WARNING - ACHTUNG - ATTENTION - This function should NEVER call anything requiring a stored configuration item
// This is because it can be used by a dummy Fei3Mod that stores no real configuration but is used to broadcast common pixel settings
// This is used for loading the Select, Enable, and Preamp masks
void Fei3Mod::strobePixelLatches(int iFE, Fei3::PixLatch pixReg, uint8_t nDCs, uint32_t bitstream[][nWordsPerDC]) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << ": iFE=" << iFE << std::endl;

    if( bitstream ) {
      // Write the contents of the shift register from the bitstream array
      std::vector<uint32_t> clockPixelData;
      Fei3Cmd::clockPixel(iFE, clockPixelData, bitstream, nDCs, iFE >= 16); // Prepare the content of the shift register
      writeMCCRegister(CNT, ((320 * nDCs) << 3) | 0x4); // write to CNT the length of the data
      Fei3ModCmd::mccWrFrontEnd(clockPixelData, false);
    }

    // Send actual command to FE
    writeMCCRegister(CNT, 0x4, false );
    Fei3ModCmd::mccWrFrontEnd(decideWriteCommand(iFE, pixReg), false);
    std::vector<uint32_t> nullCmdVec;
    Fei3Cmd::nullCmd(iFE, nullCmdVec, iFE >= 16);
    Fei3ModCmd::mccWrFrontEnd(nullCmdVec, false);
    // Instruct the BOC TX FIFO to be serialized out
    getTxLink()->send();
}

// Note: the regMask is in absolute pixel register terms, i.e. 0x1 << pixReg+9, e.g. WriteEnable = 0x000200
// During a scan typically one would want to have regMask = WriteMask | WriteSelect
void Fei3Mod::moveMask(int iFE, uint32_t regMask) {
    bool bcast = false;
    if (iFE >= 16) bcast = true;

    for (int reg = 0; reg < Fei3::nPixLatches; reg++) {
      uint32_t curPixRegMask = 0x200 << reg;
      if( (regMask & curPixRegMask) == 0 ) continue; // Skipping pixels not in the mask

      // prepare pixel SR by sending only ReadPixel
      writeMCCRegister(CNT, 4);
      std::vector<uint32_t> onlyReadPixelData;
      Fei3Cmd::genericCommand(iFE, ReadPixel, onlyReadPixelData, bcast);
      Fei3ModCmd::mccWrFrontEnd(onlyReadPixelData);

      // read the mask
      writeMCCRegister(CNT, (2 << 3) | 4);
      std::vector<uint32_t> readPixelData;
      Fei3Cmd::genericCommand(iFE, ReadPixel | curPixRegMask | ClockPixel, readPixelData, bcast);
      readPixelData.push_back(0);
      Fei3ModCmd::mccWrFrontEnd(readPixelData);

      // disable read pixel by sending null command
      writeMCCRegister(CNT, 4);
      std::vector<uint32_t> nullCmdVec;
      Fei3Cmd::nullCmd(iFE, nullCmdVec, bcast);
      Fei3ModCmd::mccWrFrontEnd(nullCmdVec, false);

      // shift mask by 1 in pixel register
      writeMCCRegister(CNT, (1 << 3) | 0x4);
      std::vector<uint32_t> clockPixelData;
      Fei3Cmd::genericCommand(iFE, ClockPixel, clockPixelData, bcast);
      clockPixelData.push_back(0);
      Fei3ModCmd::mccWrFrontEnd(clockPixelData);

      // write contents of pixel register to latches
      writeMCCRegister(CNT, 0x4);
      Fei3ModCmd::mccWrFrontEnd(decideWriteCommand(iFE, static_cast<Fei3::PixLatch>(reg)));

    }
}

void Fei3Mod::sendPixelConfig(int iFE) {

if(m_maskPixelEna && iFE==0)std::cout<<" Masking Pixels in Stand-by"<<std::endl;

    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << ": iFE=" << iFE << std::endl;
    writeMCCRegister(FEEN, 0x1 << iFE); // enable front end to send config to

    for (int reg = 0; reg < Fei3::nPixLatches; reg++) {
#if 0
        if( reg == Fei3::GlobReg::Preamp && !m_preAmpsOn )
            std::cout << reg << " -- Not enabling pre-amps" << std::endl;
        else std::cout << reg << " -- " << std::endl;
#endif

#define DO_NINE_DC_IN_ONE_GO
#ifdef DO_NINE_DC_IN_ONE_GO
        if (reg == 0) { // only need to do this once
            setAllPixelShiftRegisters(iFE, true); // Enable all DCs on the pixel for writing
            sendGlobalConfig(iFE); // send the global config with these DCs enabled
        }
        uint32_t bitstream[nDC][nWordsPerDC]; // get config for given register
        for (uint8_t i = 0; i < nDC; i++) {
            for (unsigned j = 0; j < nWordsPerDC; j++) {
                int word = Fei3PixelCfg::getWordNumber(static_cast<Fei3::PixLatch>(reg), i, j);
                if( reg == static_cast<int>(Fei3::PixLatch::Preamp) && !m_preAmpsOn )
                  bitstream[i][j] = Fei3PixelCfg::nullPixelSR[i][j];
                else if( reg == static_cast<int>(Fei3::PixLatch::Enable) && !m_preAmpsOn && m_maskPixelEna )
                  bitstream[i][j] = Fei3PixelCfg::nullPixelSR[i][j];
                else
                  bitstream[i][j] = m_chipCfgs[iFE].pixcfg[word];
            }
        }
        strobePixelLatches(iFE, static_cast<Fei3::PixLatch>(reg), nDC, bitstream);

#else

        for (int i = 0; i < nDC; i++) {
#if 0
          std::cout << "iDC=" << i << std::endl;
#endif
          uint32_t bitstream[1][nWordsPerDC]; // get config for given register
          setAllPixelShiftRegisters(iFE, false); // Disable all DCs on the pixel for writing
          writeGlobalReg(iFE, Fei3GlobalCfg::colEnReg(i), true);
          sendGlobalConfig(iFE); // send the global config with these DCs enabled

          for (int j = 0; j < nWordsPerDC; j++) {
            int word = Fei3PixelCfg::getWordNumber(static_cast<Fei3::PixLatch>(reg), i, j);
            if( reg == Fei3::GlobReg::Preamp && !m_preAmpsOn )
              bitstream[0][j] = Fei3PixelCfg::nullPixelSR[i][j];
            else if( reg == Fei3::GlobReg::Enable && !m_preAmpsOn && m_maskPixelEna )
               bitstream[0][j] = Fei3PixelCfg::nullPixelSR[i][j];
            else
              bitstream[0][j] = m_chipCfgs[iFE].pixcfg[word];
          }
          strobePixelLatches(iFE, static_cast<Fei3::PixLatch>(reg), 1, bitstream);
#if 0
          dumpData(bitstream)
#endif
        }

        setAllPixelShiftRegisters(iFE, true); // Enable all DCs on the pixel for writing
        sendGlobalConfig(iFE); // send the global config with these DCs enabled
#endif
    }
    writeMCCRegister(FEEN, maskEnableFEConfig); // Enable all FEs enabled for this module in the config
}

void Fei3Mod::readGlobalConfig(int iFE) {
    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << ": iFE=" << iFE << std::endl;
    uint32_t emptycfg[Fei3GlobalCfg::nWordsGlobal]; for (unsigned i = 0; i < Fei3GlobalCfg::nWordsGlobal; i++) emptycfg[i] = 0; // empty config for clocking
    std::vector<uint32_t> clockGlobalEmpty;
    Fei3Cmd::clockGlobal(iFE, clockGlobalEmpty, emptycfg);
    writeMCCRegister(CNT, (231 << 3) | 0x4); // write to CNT the length of the data
    Fei3ModCmd::mccWrFrontEnd(clockGlobalEmpty); // clock the empty config to clear the strobed
    writeMCCRegister(CNT, 0x4); // write to CNT the length of the ReadGlobal command
    std::vector<uint32_t> readGlobalData;
    Fei3Cmd::readGlobal(iFE, readGlobalData);
    Fei3ModCmd::mccWrFrontEnd(readGlobalData); // read the global registers to the strobes
    writeMCCRegister(FEEN, 0x1 << iFE); // enable front end to read config from
    writeMCCRegister(CNT, (231 << 3) | 0x4); // write to CNT the length of the data
    Fei3ModCmd::mccRdFrontEnd(clockGlobalEmpty); // clock the global config in the strobes back to us by refilling with an empty config
}

template<int nDCs>
void Fei3Mod::readPixelConfigBit(int iFE, Fei3::PixLatch reg) {
    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << ": iFE=" << iFE << " nDC=" << nDCs << std::endl;

    if (nDCs < 1 || nDCs > 9) return;

    // command to send
    uint32_t cmdToSend = 0x1 << ((int) reg + 9);

    writeMCCRegister(FEEN, 0x1 << iFE);

    // empty config for clocking
    uint32_t emptycfg[nDCs][Fei3PixelCfg::nWordsPerDC];
    for (int iDC = 0; iDC < nDCs; iDC++) {
        for (unsigned iWord = 0; iWord < Fei3PixelCfg::nWordsPerDC; iWord++) {
            emptycfg[iDC][iWord] = 0;
        }
    }

    // clear pixel shift register
    writeMCCRegister(CNT, ((320 * nDCs) << 3) | 0x4); // write to CNT the length of the data
    std::vector<uint32_t> clockPixelEmpty;
    Fei3Cmd::clockPixel(iFE, clockPixelEmpty, emptycfg, nDCs);
    Fei3ModCmd::mccWrFrontEnd(clockPixelEmpty);

    // prepare pixel SR by sending only ReadPixel
    writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> onlyReadPixelData;
    Fei3Cmd::genericCommand(iFE, ReadPixel | ClockPixel, onlyReadPixelData);
    Fei3ModCmd::mccWrFrontEnd(onlyReadPixelData);

    // read the mask
    writeMCCRegister(CNT, (2 << 3) | 0x4);
    std::vector<uint32_t> readPixelData;
    Fei3Cmd::genericCommand(iFE, ReadPixel | cmdToSend | ClockPixel, readPixelData);
    readPixelData.push_back(0);
    Fei3ModCmd::mccWrFrontEnd(readPixelData);
    
    //Somehow a clock Pixel is needed here, if not for FE>0 the latches are not properly clocked to the shift register
    writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> clockPixelData;
    Fei3Cmd::genericCommand(iFE, ClockPixel, clockPixelData);
    clockPixelData.push_back(0);
    Fei3ModCmd::mccWrFrontEnd(clockPixelData);

    // disable read pixel by sending null command
    writeMCCRegister(CNT, 0x4);
    std::vector<uint32_t> nullData;
    nullData.push_back(0);
    Fei3ModCmd::mccWrFrontEnd(nullData);

    // read data from SR
    writeMCCRegister(CNT, ((320 * nDCs) << 3) | 0x4);
    Fei3ModCmd::mccRdFrontEnd(clockPixelEmpty);

}

uint32_t Fei3Mod::getVcalFromCharge(int iFE, float charge, int capType) {

  float cinj = ((capType ==1) ? this->m_chipCfgs[iFE].getCinjHigh() : this->m_chipCfgs[iFE].getCinjLow());

  uint32_t vcal = (uint32_t)((((0.160218*charge)/cinj) - this->m_chipCfgs[iFE].getVcalCoeff0()) / this->m_chipCfgs[iFE].getVcalCoeff1());

  return vcal;
}

void Fei3Mod::writeVcalFromCharge(int iFE, float charge, int capType) {
  uint32_t vcal = this->getVcalFromCharge(iFE, charge,capType);
// std::cout << "FE " << iFE << " got vcal = " << vcal << ", from charge = " << charge << std::endl;
  if (vcal > 1023) {
    std::cout<<"WARNING! Calculated a vcal greater than 1023. Setting to 1023 (max value.)"<<std::endl;
    vcal = 1023;
  }
  writeGlobalReg(iFE, Fei3::GlobReg::HighInjCapSel, capType);
  writeGlobalReg(iFE, Fei3::GlobReg::VCal, vcal);
}

void Fei3Mod::setCapType(int iFE, int capType) {
  writeGlobalReg(iFE, Fei3::GlobReg::HighInjCapSel, capType);
}

void Fei3Mod::setDefaultGlobalConfigProxy(bool calib) {
    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << std::endl;
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) setDefaultGlobalConfig(i);
    }
}

void Fei3Mod::sendGlobalConfigProxy(bool calib) {
    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << std::endl;
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) sendGlobalConfig(i);
    }
}

void Fei3Mod::strobePixelLatchesProxy(int iFE, Fei3::PixLatch pixReg, uint32_t bitstream[][nWordsPerDC]) {
        strobePixelLatches(iFE, pixReg, Fei3PixelCfg::nDC, bitstream);
}

void Fei3Mod::strobePixelLatchesDCProxy(bool calib, Fei3::PixLatch pixReg, uint32_t bitstream[][nWordsPerDC]) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) strobePixelLatchesDC(i, pixReg, bitstream);
    }
}

void Fei3Mod::strobePixelLatchesDC_wBCProxy(uint8_t iDC, bool calib, Fei3::PixLatch pixReg, uint32_t bitstream[1][nWordsPerDC]) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) strobePixelLatchesDC(i, pixReg, iDC, bitstream);
    }
}

void Fei3Mod::sendPixelConfigProxy(bool calib) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) sendPixelConfig(i);
    }
}

void Fei3Mod::writeGlobalRegProxy(bool calib, Fei3::GlobReg reg, uint16_t val) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) writeGlobalReg(i, reg, val);
    }
}

void Fei3Mod::writePixelRegProxy(bool calib, Fei3::PixLatch bit, int row, int col, int val) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) writePixelReg(i, bit, row, col, val);
    }
}

void Fei3Mod::writePixelTDACProxy(bool calib, int row, int col, int val) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) writePixelTDAC(i, row, col, val);
    }
}

void Fei3Mod::writePixelFDACProxy(bool calib, int row, int col, int val) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) writePixelFDAC(i, row, col, val);
    }
}

void Fei3Mod::setAllPixelShiftRegisters(int iFE, bool val) {
    for(uint8_t iDC = 0 ; iDC < Fei3PixelCfg::nDC ; ++iDC)
      writeGlobalReg(iFE, Fei3GlobalCfg::colEnReg(iDC), val);
}

void Fei3Mod::setAllPixelShiftRegistersProxy(bool val, bool calib) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) setAllPixelShiftRegisters(i, val);
    }
}

void Fei3Mod::writeVcalFromChargeProxy(bool calib, float charge, int capType) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) writeVcalFromCharge(i, charge, capType);
    }
}

void Fei3Mod::setCapTypeProxy(bool calib, int capType) {
    uint16_t mask = (calib) ? maskEnableFEScan : maskEnableFEConfig;
    for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) {
        if ((0x1 << i) & mask) setCapType(i, capType);
    }
}

std::vector<uint32_t> Fei3Mod::decideWriteCommand(int iFE, Fei3::PixLatch bit) {
    std::vector<uint32_t> result;

    bool bcast = false;
    if( iFE >= 16 ) {
      bcast = true;
    }

    switch (bit) {
        case Fei3::PixLatch::HitBus:
            result = Fei3Cmd::writeEnable(iFE, bcast);
            break;
        case Fei3::PixLatch::Select:
            result = Fei3Cmd::writeSelect(iFE, bcast);
            break;
        case Fei3::PixLatch::Enable:
            result = Fei3Cmd::writeMask(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC0:
            result = Fei3Cmd::writeTDAC0(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC1:
            result = Fei3Cmd::writeTDAC1(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC2:
            result = Fei3Cmd::writeTDAC2(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC3:
            result = Fei3Cmd::writeTDAC3(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC4:
            result = Fei3Cmd::writeTDAC4(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC5:
            result = Fei3Cmd::writeTDAC5(iFE, bcast);
            break;
        case Fei3::PixLatch::TDAC6:
            result = Fei3Cmd::writeTDAC6(iFE, bcast);
            break;
        case Fei3::PixLatch::FDAC0:
            result = Fei3Cmd::writeFDAC0(iFE, bcast);
            break;
        case Fei3::PixLatch::FDAC1:
            result = Fei3Cmd::writeFDAC1(iFE, bcast);
            break;
        case Fei3::PixLatch::FDAC2:
            result = Fei3Cmd::writeFDAC2(iFE, bcast);
            break;
        case Fei3::PixLatch::Preamp:
            result = Fei3Cmd::writeKill(iFE, bcast);
            break;
        default:
            break;
    }
    return result;
}

void Fei3Mod::setDefaultGlobalConfig(uint8_t iFE) {
  if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__  << ": iFE=" << static_cast<uint32_t>(iFE) << std::endl;
  m_chipCfgs[iFE].globcfg[0] = 0x08c001f0;
  m_chipCfgs[iFE].globcfg[1] = 0x08100000;
  m_chipCfgs[iFE].globcfg[2] = 0x20200000;
  m_chipCfgs[iFE].globcfg[3] = 0x68010100;
  m_chipCfgs[iFE].globcfg[4] = 0x08001003;
  m_chipCfgs[iFE].globcfg[5] = 0x8000000c;
  m_chipCfgs[iFE].globcfg[6] = 0x01000080;
  m_chipCfgs[iFE].globcfg[7] = 0x00000020;
}

void Fei3Mod::dumpGlobalConfig(uint8_t iFE) {
  if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__  << ": iFE=" << static_cast<uint32_t>(iFE) << std::endl;
#ifdef __XMK__
  for(size_t i = 7 ; i != 0xFFFFFFFF ; --i) {
    //dumpData(m_chipCfgs[iFE].globcfg[i]);
    uint32_t dWord = ((m_chipCfgs[iFE].globcfg[i] & 0x7F) << 25);
    if(i) dWord |= (m_chipCfgs[iFE].globcfg[i-1] >> 7);
    dumpData(dWord);
  }
#endif
}

void Fei3Mod::dumpGlobalConfigHR(uint8_t iFE) {
   m_chipCfgs[iFE].dumpFei3GlobalCfg();

}

void Fei3Mod::dumpMccRegisters(bool readLocal) {
  std::string mccRegName[11]={"CSR", "LV1", "FEEN", "WFE", "WMCC", "CNT", "CAL", "PEF", "SBSR", "WBITD", "WRECD"};
  for(size_t mccReg = 0; mccReg < 11; ++mccReg) {
    if(!readLocal) readMCCRegister((MCCRegisters)mccReg);
    std::cout << "Register #" << mccReg << " " << mccRegName[mccReg] <<" value: " << mccRegisters[mccReg] << "(" << HEX( mccRegisters[mccReg] ) << ")" << std::endl;
  }
}

bool Fei3Mod::checkConfig(uint8_t iFE, bool doPR) {
    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << " iFE: " << (int) iFE << std::endl;

    Fei3GlobalCfg readBackCfg = getGlobalConfig(iFE);

    bool passGR = (readBackCfg == m_chipCfgs[iFE]);

    if (doPR) {
      std::cout<<"Readback check not performed for Pixel registers"<<std::endl;
    }

    return passGR;
}

Fei3GlobalCfg Fei3Mod::getGlobalConfig(uint8_t iFE) {

    if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << " iFE: " << (int) iFE << std::endl;

    // keep a copy of cfg to restore to
    Fei3Cfg originalCfg = m_chipCfgs[iFE];

    //Set DO to GR
    writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 15);
    sendGlobalConfig(iFE);

    // enlarge trailer length to make sure we can get all the data
    IblBoc::PixSetTrailerLength(m_rxCh, 264);

    // make an IblBocRxFifo
    IblBocRxFifo rxFifo;
    rxFifo.setRxCh(m_rxCh);

    // clear the current fifo contents
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x01);
    for (int k = 0 ; k < 8192 ; ++k) rxFifo.read();
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x17);//Enable Rx Fifo monitoring and set Rx speed to 40Mbps

    // Compare Global Register
    if(m_verbosity>10) std::cout << "GR test" << std::endl;

    // Read GR
    readGlobalConfig(iFE);

    // Read from RxFifo
    std::vector<uint8_t> rxData;
    rxData.reserve(400);
    for(size_t k = 0 ; k < rxData.capacity() ; ++k) rxData.push_back(rxFifo.read());

    // Make words from what is read back
    uint32_t gr[Fei3GlobalCfg::nWordsGlobal] = {0};
    uint32_t curw = 0;
    size_t cnt = 231;
      for(size_t j = 32 ; j < rxData.capacity() ; j++) {
        if (cnt % 32 == 0) {gr[(cnt / 32) % 8] = curw; curw = 0;}
        if (cnt == 0) break;
        uint8_t v = ((rxData[j] & 0x3F) << 2) + ((rxData[j+1] & 0xC0) >> 6);
        if (__builtin_popcount(v) >= 4) curw |= 1 << ((cnt - 1) % 32);
        --cnt;
      }

    if(m_verbosity>10)debugConfigReadback(iFE, rxData, true);

    Fei3GlobalCfg readBackCfg;
    //std::cout<<"######Reg HEX dump#######"<<std::endl;
    for(size_t i=0;i<Fei3GlobalCfg::nWordsGlobal;i++){
    readBackCfg.setWord(i, gr[i]);
    //std::cout<<std::hex << std::setw(8) << std::setfill('0') << gr[i] <<std::dec<<std::endl;
    }
    //std::cout<<std::endl;

    //readBackCfg.dumpFei3GlobalCfg();

    // clear other crap in here
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x01);
    for (int k = 0 ; k < 8192 ; ++k) rxFifo.read();
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x03);

    // restore trailer length
    IblBoc::PixSetTrailerLength(m_rxCh, 22);//Default trailer length

    // restore original config
    m_chipCfgs[iFE] = originalCfg;

  return readBackCfg;
}


Fei3PixelCfg Fei3Mod::getPixelConfig(uint8_t iFE) {

if(m_verbosity>10) std::cout << __PRETTY_FUNCTION__ << " iFE: " << (int) iFE << std::endl;

  // keep a copy of cfg to restore to
  Fei3Cfg originalCfg = m_chipCfgs[iFE];

  IblBoc::PixSetTrailerLength(m_rxCh, 3200);

  //Set DO to GR
  writeGlobalReg(iFE, Fei3::GlobReg::Select_DO, 11);

  // make an IblBocRxFifo
  IblBocRxFifo rxFifo;
  rxFifo.setRxCh(m_rxCh);

  Fei3PixelCfg readBackCfg;

  // clear the current fifo contents
  IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x01);
  for (int k = 0 ; k < 8192 ; ++k) rxFifo.read();
  IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x17);//Enable Rx Fifo monitoring and set Rx speed to 40Mbps

        setAllPixelShiftRegisters(iFE, true);
        sendGlobalConfig(iFE);

        for(uint8_t iReg = 0 ; iReg < Fei3::nPixLatches; ++iReg) {
          Fei3::PixLatch pixReg = static_cast<Fei3::PixLatch>(iReg);
          readPixelConfigBit<Fei3PixelCfg::nDC>(iFE, pixReg);
          std::cout << ", iReg=" << static_cast<uint32_t>(iReg) << std::endl;
          // Get data from RX FIFO
          std::vector<uint8_t> rxData;
          rxData.reserve(3200);
            for(size_t k = 0 ; k < rxData.capacity() ; ++k) {
                rxData.push_back(rxFifo.read());
            }

         if(m_verbosity>10)debugConfigReadback(iFE, rxData, false,pixReg);

         uint32_t gr[Fei3PixelCfg::nDC][Fei3PixelCfg::nWordsPerDC]={{0}};

         int start = 32;
         for(int iDC=nDC-1;iDC>=0;iDC--){
           size_t cnt = 0;
           for(size_t j = start ; j < rxData.capacity() ; j++) {
             if (cnt >= 320) break;
             uint8_t val = ((rxData[j] & 0x0F) << 4) + ((rxData[j+1] & 0xF0) >> 4);
               if( val == 0xFF )gr[iDC][(9 - cnt/32)] |= 1 << (31 - (cnt) % 32);
               else if (val == 0) gr[iDC][(9 - cnt/32)] &= ~(1 << (31 - (cnt) % 32));
               else std::cout<<"Warning I got "<<std::hex<<val<<" while I was expecting 0xFF or 0x0"<<std::endl;
             cnt++;
           }
          start+=320;
         }

          for(size_t iDC=0;iDC<Fei3PixelCfg::nDC;iDC++){
            for(size_t i=0;i<Fei3PixelCfg::nWordsPerDC;i++){
              readBackCfg.setWord(pixReg,iDC,i,gr[iDC][i]);
              //std::cout<<std::hex<<(int)gr[iDC][i]<<std::dec<<std::endl;
            }
          }

          // Disabling monitoring FIFO and clearing it
          IblBoc::write(m_rxCh/16, m_rxCh%16, IblBoc::RxControl, 0x01);
          for(int k = 0 ; k < 8192 ; ++k) rxFifo.read(); //IblBoc::read( m_rxCh/16, m_rxCh%16, IblBoc::DataLow );
          IblBoc::write(m_rxCh/16, m_rxCh%16, IblBoc::RxControl, 0x17);
        }

    // clear other crap in here
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x01);
    for (int k = 0 ; k < 8192 ; ++k) rxFifo.read();
    IblBoc::write(m_rxCh / 16, m_rxCh % 16, IblBoc::RxControl, 0x03);

    // restore trailer length
    IblBoc::PixSetTrailerLength(m_rxCh, 22);//Default trailer length

  // restore original config
  m_chipCfgs[iFE] = originalCfg;

  return readBackCfg;

}


void Fei3Mod::debugConfigReadback(uint8_t iFE, const std::vector<uint8_t> & rxData, bool isGlobal, Fei3::PixLatch pixReg){

  std::cout<<"######GR ReadBack dump#######"<<std::endl;
    for(size_t j = 0 ; j < rxData.size() ; ++j){
      std::cout << std::bitset<8>(rxData[j])<<" ";
      if((j+1)%16==0 && j!=0)std::cout<<std::endl;
    }
  std::cout<<std::endl;


  //Build bitStream from  current confiuration
  Boclib::Bitstream val;

  //Header
  val.AppendVar(0x1D, 5);
  val.AppendVar(0x00, 5);

  //Generate expected BitSteam readback from a given configuration
  if(isGlobal){//Global
   for(int i=0;i<30 ;i++)val.Append8(0x00);
    for (unsigned i = 231; i > 0; i--) {
      if ( (m_chipCfgs[iFE].globcfg[i/32] >>  i%32) & 0x1 ){
        val.Append8(0xFF);
      }else{
        val.Append8(0x0);
      }
    }
  } else {//Pixel
     val.AppendVar(0x00, 2);
      for(size_t i=0;i<31 ;i++)val.Append8(0x00);

         for(int iDC=nDC-1;iDC>=0;iDC--){
           for (size_t i = 320; i > 0; i--) {
             uint32_t word = Fei3PixelCfg::getWordNumber(pixReg,iDC,(i-1)/32);
             uint32_t value =m_chipCfgs[iFE].pixcfg[word];
             if ( (value >>  (i-1)%32) & 0x1 ){
    	       val.Append8(0xFF);
     	     } else{
    	       val.Append8(0x0);
    	     }
           }
         } 
  }

  val.AppendVar(0x00,14);


  std::cout<<"######Built BS#######"<<std::endl;
  std::vector<uint8_t>CompareBitStream = val.GetData();
    for (size_t i=0; i<CompareBitStream.size();++i){
      std::cout << std::bitset<8>(CompareBitStream[i])<<" ";
      if((i+1)%16==0 && i!=0)std::cout<<std::endl;
    }
  std::cout<<std::endl;

  std::cout<<"######Compare BS#######"<<std::endl;
  size_t size =CompareBitStream.size();
  if(rxData.size() < size)size = rxData.size();

    for (size_t i=0; i<CompareBitStream.size();++i){
      
      if(CompareBitStream[i] != rxData[i] ){
	std::bitset<8> rB =rxData[i];
	std::bitset<8> bS =CompareBitStream[i];
	  for(int j=7;j>=0;j--){
	    if(rB[j]==bS[j])std::cout<<"x";
	    else std::cout<<rB[j];
	  }
	std::cout<<" ";
      } else {
        std::cout << "xxxxxxxx"<<" ";
      }
      if((i+1)%16==0 && i!=0)std::cout<<std::endl;
    }
  std::cout<<std::endl;

}


