#include "Fei3Cmd.h"

const unsigned Fei3PixelCfg::nWordsPerDC;
const unsigned Fei3GlobalCfg::nWordsGlobal;

Fei3Cmd::Fei3Cmd() {
}

void Fei3Cmd::nullCmd(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000000 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::readPixel(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x800001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeKill(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x400001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeFDAC2(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x200001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeFDAC1(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x100001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeFDAC0(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x080001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC6(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x040001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC5(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x020001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC4(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x010001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC3(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x008001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC2(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x004001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC1(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x002001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeTDAC0(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x001001 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeMask(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000801 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeSelect(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000401 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeEnable(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000201 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::clockPixel(int chipID, std::vector<uint32_t>& data, uint32_t bitstream[][Fei3PixelCfg::nWordsPerDC], unsigned int ndcs, bool bcast) {
    //std::cout << __PRETTY_FUNCTION__ << std::endl;
    //std::cout << "Allocating " << ndcs * Fei3PixelCfg::nWordsPerDC + 1 << " elements." << std::endl;
    data.reserve(ndcs * Fei3PixelCfg::nWordsPerDC + 1);
    data.push_back((0x000101 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
    for (unsigned int i = 0; i < ndcs; i++) {
        for (unsigned j = 0; j < Fei3PixelCfg::nWordsPerDC; j++) {
            data.push_back(bitstream[ndcs - i - 1][Fei3PixelCfg::nWordsPerDC - j - 1]);
        }
    }
}

void Fei3Cmd::readGlobal(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000081 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::writeGlobal(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000060 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::clockGlobal(int chipID, std::vector<uint32_t>& data, uint32_t bitstream[Fei3GlobalCfg::nWordsGlobal], bool bcast) {
    //bool hasRightSize = (data.size() == Fei3GlobalCfg::nWordsGlobal + 1);
    data.reserve(Fei3GlobalCfg::nWordsGlobal + 1);
    data.push_back((0x000011 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
    for (unsigned i = 0; i < Fei3GlobalCfg::nWordsGlobal; i++) {
        uint32_t word = 0;
        word |= (bitstream[Fei3GlobalCfg::nWordsGlobal - i - 1] & 0x7F) << 25;
        if (i != Fei3GlobalCfg::nWordsGlobal - 1) {
            word |= (bitstream[Fei3GlobalCfg::nWordsGlobal - i - 2] & 0xFFFFFF80) >> 7;
        }
        data.push_back(word);
    }
}

void Fei3Cmd::softReset(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x00000C << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::refReset(int chipID, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    data.push_back((0x000003 << 5) + (((int) bcast) << 4) + (chipID & 0xF));
}

void Fei3Cmd::genericCommand(int chipID, uint32_t command, std::vector<uint32_t>& data, bool bcast) {
    data.reserve(1);
    command &= 0xFFFFFF;
    command <<= 5; // chipID + bcast
    if (__builtin_popcount(command) % 2) command |= 0x20; // compute parity
    command |= (bcast << 4); // bcast
    command |= (chipID & 0xF); // chipID
    data.push_back(command);
}
