#include <iostream>
#include "Fei3PixelCfg.h"

uint32_t Fei3PixelCfg::nullPixelSR[nDC][nWordsPerDC] = {{ 0 }};
uint32_t Fei3PixelCfg::highPixelSR[nDC][nWordsPerDC] = {{ 1 }};

void Fei3Mask::set(Fei3MaskType inmask) {
    uint32_t maskCode = 0;

    switch (inmask) {
        case FEI3_MASK_FULL :
            maskCode = 0xFFFFFFFF;
            break;
        case FEI3_MASK_32 :
            maskCode = 0x1;
            break;
        case FEI3_MASK_CLEAR :
            maskCode = 0;
            break;
    }

    for (unsigned i = 0; i < Fei3PixelCfg::nWordsPerDC; i++) mask[0][i] = maskCode;
}

void Fei3Mask::shift() {
    uint32_t lastBitPrev = (mask[0][Fei3PixelCfg::nWordsPerDC - 1] >> 31) & 0x1;
    uint32_t lastBitCur = 0;
    for (unsigned i = 0; i < Fei3PixelCfg::nWordsPerDC; i++) {
        lastBitCur = (mask[0][i] >> 31) & 0x1;
        mask[0][i] &= 0x7FFFFFFF;
        mask[0][i] <<= 1;
        mask[0][i] |= lastBitPrev;
        lastBitPrev = lastBitCur;
    }
}

void Fei3Mask::shiftBack() {
    uint32_t lastBitPrev = mask[0][0] & 0x1;
    uint32_t lastBitCur = 0;
    for (unsigned i = 0; i < Fei3PixelCfg::nWordsPerDC; i++) {
        lastBitCur = mask[0][Fei3PixelCfg::nWordsPerDC - i - 1] & 0x1;
        mask[0][Fei3PixelCfg::nWordsPerDC - i - 1] &= 0xFFFFFFFE;
        mask[0][Fei3PixelCfg::nWordsPerDC - i - 1] >>= 1;
        mask[0][Fei3PixelCfg::nWordsPerDC - i - 1] |= (lastBitPrev << 31);
        lastBitPrev = lastBitCur;
    }
}

Fei3PixelCfg::Fei3PixelCfg(){
}

Fei3PixelCfg::Fei3PixelCfg(const Fei3PixelCfg &rhs) {
    std::copy(rhs.pixcfg, rhs.pixcfg + (Fei3::nPixLatches * nDC * nWordsPerDC), pixcfg);
}

Fei3PixelCfg & Fei3PixelCfg::operator=(const Fei3PixelCfg &rhs) {
    std::copy(rhs.pixcfg, rhs.pixcfg + (Fei3::nPixLatches * nDC * nWordsPerDC), pixcfg);
    return *this;
}

void Fei3PixelCfg::writeBitPixel(Fei3::PixLatch reg, int row, int col, int val) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);
    int wordToWrite = getWordNumber(reg, dc, word);
    uint32_t resetmask = ~(0x1 << bit);
    uint32_t mask = (val & 0x1) << bit;
    pixcfg[wordToWrite] &= resetmask;
    pixcfg[wordToWrite] |= mask;
}

void Fei3PixelCfg::writeTDACPixel(int row, int col, int val) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);

    int ibit = 0;
    for (int reg = static_cast<int>(Fei3::PixLatch::TDAC0); reg <= static_cast<int>(Fei3::PixLatch::TDAC6); reg++) {
        int wordToWrite = getWordNumber(static_cast<Fei3::PixLatch>(reg), dc, word);
        uint32_t resetmask = ~(0x1 << bit);
        uint32_t mask = ((val >> ibit) & 0x1) << bit;
        pixcfg[wordToWrite] &= resetmask;
        pixcfg[wordToWrite] |= mask;
        ibit++;
    }
}

void Fei3PixelCfg::writeFDACPixel(int row, int col, int val) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);

    int ibit = 0;
    for (int reg = static_cast<int>(Fei3::PixLatch::FDAC0); reg <= static_cast<int>(Fei3::PixLatch::FDAC2); reg++) {
        int wordToWrite = getWordNumber(static_cast<Fei3::PixLatch>(reg), dc, word);
        uint32_t resetmask = ~(0x1 << bit);
        uint32_t mask = ((val >> ibit) & 0x1) << bit;
        pixcfg[wordToWrite] &= resetmask;
        pixcfg[wordToWrite] |= mask;
        ibit++;
    }
}

int Fei3PixelCfg::readBitPixel(Fei3::PixLatch reg, int row, int col) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return 0;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);
    int wordToRead = getWordNumber(static_cast<Fei3::PixLatch>(reg), dc, word);
    return ((pixcfg[wordToRead] >> bit) & 0x1);
}

int Fei3PixelCfg::readTDACPixel(int row, int col) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return 0;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);

    int result = 0;

    int ibit = 0;
    for (int reg = static_cast<int>(Fei3::PixLatch::TDAC0); reg <= static_cast<int>(Fei3::PixLatch::TDAC6); reg++) {
        int wordToRead = getWordNumber(static_cast<Fei3::PixLatch>(reg), dc, word);
        result |= (((pixcfg[wordToRead] >> bit) & 0x1) << ibit);
        ibit++;
    }

    return result;    
}

int Fei3PixelCfg::readFDACPixel(int row, int col) {
    if (row < 0 || row > 159 || col < 0 || col > 17) return 0;
    int dc = (int) (col/2);
    int word = getWordFromRowCol(row, col);
    int bit = getBitFromRowCol(row, col);

    int result = 0;

    int ibit = 0;
    for (int reg = static_cast<int>(Fei3::PixLatch::FDAC0); reg <= static_cast<int>(Fei3::PixLatch::FDAC2); reg++) {
        int wordToRead = getWordNumber(static_cast<Fei3::PixLatch>(reg), dc, word);
        result |= (((pixcfg[wordToRead] >> bit) & 0x1) << ibit);
        ibit++;
    }

    return result;    
}

int Fei3PixelCfg::getWordFromRowCol(int row, int col) {
    return (col % 2 == 0) ? (int) (row/32) : ((int) ((159 - row)/32)) + 5;
}

int Fei3PixelCfg::getBitFromRowCol(int row, int col) {
    return (col % 2 == 0) ? row % 32 : (159 - row) % 32;
}

int Fei3PixelCfg::getWordNumber(Fei3::PixLatch reg, int dc, int word) {
    return ((static_cast<int>(reg) * nDC * nWordsPerDC) + (dc * nWordsPerDC) + word);
}

void Fei3PixelCfg::setWord(Fei3::PixLatch reg, int dc, int word, uint32_t val){
   int pos = getWordNumber(reg, dc, word);
   pixcfg[pos] = val;
}

uint32_t Fei3PixelCfg::getPixelRegHash() const {
    uint32_t hash = 0;
    size_t totalRegisters = Fei3::nPixLatches*nDC*nWordsPerDC;
    for(size_t i = 0; i < totalRegisters; i++){
            hash ^= pixcfg[i];
    }
    return hash;
}
