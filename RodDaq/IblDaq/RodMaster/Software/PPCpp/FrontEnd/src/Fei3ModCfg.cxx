#include "Fei3ModCfg.h"
#include "BitUtils.h"

Fei3ModCfg::Fei3ModCfg() {
}

Fei3ModCfg::Fei3ModCfg(const Fei3ModCfg &rhs) {
    std::copy(rhs.m_chipCfgs, rhs.m_chipCfgs + Fei3ModCfg::nFE, m_chipCfgs);
    m_rxCh = rhs.m_rxCh;
    m_txCh = rhs.m_txCh;
    std::copy(rhs.mccRegisters, rhs.mccRegisters + 11, mccRegisters);
    maskEnableFEConfig = rhs.maskEnableFEConfig;
    maskEnableFEScan = rhs.maskEnableFEScan;
    maskEnableFEDacs = rhs.maskEnableFEDacs;
    feFlavor = rhs.feFlavor;
    mccFlavor = rhs.mccFlavor;
    std::copy(rhs.idStr, rhs.idStr + 32, idStr);
    std::copy(rhs.tag, rhs.tag + 32, tag);
    revision = rhs.revision;
    active = rhs.active;
    moduleIdx = rhs.moduleIdx;
    groupId = rhs.groupId;
}

Fei3ModCfg & Fei3ModCfg::operator=(const Fei3ModCfg &rhs) {
    std::copy(rhs.m_chipCfgs, rhs.m_chipCfgs + Fei3ModCfg::nFE, m_chipCfgs);
    m_rxCh = rhs.m_rxCh;
    m_txCh = rhs.m_txCh;
    std::copy(rhs.mccRegisters, rhs.mccRegisters + 11, mccRegisters);
    maskEnableFEConfig = rhs.maskEnableFEConfig;
    maskEnableFEScan = rhs.maskEnableFEScan;
    maskEnableFEDacs = rhs.maskEnableFEDacs;
    feFlavor = rhs.feFlavor;
    mccFlavor = rhs.mccFlavor;
    std::copy(rhs.idStr, rhs.idStr + 32, idStr);
    std::copy(rhs.tag, rhs.tag + 32, tag);
    revision = rhs.revision;
    active = rhs.active;
    moduleIdx = rhs.moduleIdx;
    groupId = rhs.groupId;
    return *this;
}

void Fei3ModCfg::dump() const {
    std::cout << __PRETTY_FUNCTION__ <<std::endl;
    std::cout << "rxCh = " << (int) m_rxCh << std::endl;
    std::cout << "txCh = " << (int) m_txCh << std::endl;
    std::cout << "mccRegisters[CSR] = "   << mccRegisters[CSR] << std::endl;
    std::cout << "mccRegisters[LV1] = "   << mccRegisters[LV1] << std::endl;
    std::cout << "mccRegisters[FEEN] = "  << mccRegisters[FEEN] << std::endl;
    std::cout << "mccRegisters[WFE] = "   << mccRegisters[WFE] << std::endl;
    std::cout << "mccRegisters[WMCC] = "  << mccRegisters[WMCC] << std::endl;
    std::cout << "mccRegisters[CNT] = "   << mccRegisters[CNT] << std::endl;
    std::cout << "mccRegisters[CAL] = "   << mccRegisters[CAL] << std::endl;
    std::cout << "mccRegisters[PEF] = "   << mccRegisters[PEF] << std::endl;
    std::cout << "mccRegisters[SBSR] = "  << mccRegisters[SBSR] << std::endl;
    std::cout << "mccRegisters[WBITD] = " << mccRegisters[WBITD] << std::endl;
    std::cout << "mccRegisters[WRECD] = " << mccRegisters[WRECD] << std::endl;
    std::cout << "maskEnableFEConfig = " << maskEnableFEConfig << std::endl;
    std::cout << "maskEnableFEScan = " << maskEnableFEScan << std::endl;
    std::cout << "maskEnableFEDacs = " << maskEnableFEDacs << std::endl;
    std::cout << "feFlavor = " << (int) feFlavor << std::endl;
    std::cout << "mccFlavor = " << (int) mccFlavor << std::endl;
    std::cout << "revision = " <<  revision << std::endl;
    std::cout << "active = " << (int) active << std::endl;
    std::cout << "moduleIdx = " << (int) moduleIdx << std::endl;
    std::cout << "groupId = " << (int) groupId << std::endl;
    //for (uint8_t i = 0; i < Fei3ModCfg::nFE; i++) m_chipCfgs[i].Fei3ExtCfg::dump();
}

uint32_t Fei3ModCfg::getHash() const {
    uint32_t hash = getMCCHash();
    std::vector< std::vector<uint32_t> > FEHashes = getFEHashes();
    for(size_t ix = 0; ix < nFE; ++ix){
        hash ^= rotl32b(FEHashes[ix][0], 2*ix);
        hash ^= rotl32b(FEHashes[ix][1], 2*ix);
        hash ^= rotl32b(FEHashes[ix][2], 2*ix);
    }
   return hash;
}

uint32_t Fei3ModCfg::getMCCHash() const {
    uint32_t hash = 0x0;

    // Rx & Tx channels (8bit)
    hash ^= m_rxCh << 24;
    hash ^= m_txCh << 16;

    // MCC registers (11 times 16 bit) - we only keep a few
    hash ^= mccRegisters[WBITD];
    hash ^= mccRegisters[WRECD] << 16;

    // Other values
    //16bit
    hash ^= maskEnableFEConfig;
    hash ^= maskEnableFEScan << 16;
    hash ^= maskEnableFEDacs;
    //8bit
    hash ^= feFlavor << 24;
    hash ^= mccFlavor << 16;
    //char[128]
    for(size_t ix = 0; ix < 128; ++ix){
        hash ^= static_cast<uint32_t>(idStr[ix]);
        hash ^= static_cast<uint32_t>(tag[ix]);
    }
    //32bit
    hash ^= revision;
    //8bit
    hash ^= active << 24;
    hash ^= moduleIdx << 16;
    hash ^= groupId << 8;

    return hash;
}

std::vector< std::vector<uint32_t> > Fei3ModCfg::getFEHashes() const {
    std::vector< std::vector< uint32_t> > result;
    result.resize(nFE);
    for(size_t ix = 0; ix < nFE; ++ix){
        result[ix].resize(3);
    }
    //FEI3 configs
    for(size_t ix = 0; ix < nFE; ++ix){
        result[ix][0] = m_chipCfgs[ix].getPixelRegHash();
        result[ix][1] = m_chipCfgs[ix].getGlobalRegHash();
        result[ix][2] = m_chipCfgs[ix].getExtRegHash();
    }
    return result;
}
