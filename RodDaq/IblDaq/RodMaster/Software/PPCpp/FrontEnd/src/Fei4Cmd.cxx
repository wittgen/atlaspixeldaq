#include "Fei4Cmd.h"

// default constructor links TxLink to dummy global TxLink in PPCglobal namespace
// won't do much, but won't crash
// to be useful, need to use the SetLink function to set the TxLink to where you
// want to send comands

#include "RodResourceManager.h"

Fei4Cmd::Fei4Cmd() {
  verbose = false;
  setTxLink( RodResourceManagerIBL::getDevNullSerialPort() );
}

Fei4Cmd::Fei4Cmd(TxLink * link) {
  verbose = false;
  m_Txlink = link;
}

// send lvl1 trigger to Fei4
void Fei4Cmd::trigger(){
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
  m_Txlink->write(0x0001D000);
  m_Txlink->send();
}

// bunch counter reset
void Fei4Cmd::bcr(){
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
  m_Txlink->write(0x00161000);
  m_Txlink->send();
}

// event counter reset
void Fei4Cmd::ecr(){
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
  m_Txlink->write(0x00162000);
  m_Txlink->send();
}

//calibration pulse
void Fei4Cmd::cal() {
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
  m_Txlink->write(0x00164000);
  m_Txlink->send();
}

// Combined calibration pulse and trigger, needed for calibration scans
// real delay is given by = delay + 5 (length of lv1d signal)
// this function takes the delay in bits
// and writes the number of 0s in the delay into 32 bit words
// before sending the trigger

void Fei4Cmd::calTrigger(int delay) {
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
  m_Txlink->write(0x00000000);
  m_Txlink->write(0x00001640);
  for (int i = 0; i<delay/32; i++){ // how many 32-bit words in my delay
      m_Txlink->write(0x00000000);
    }
  m_Txlink->write(0x1D000000>>delay%32); // extra bits in my delay
  m_Txlink->send();
}

// Write a whole global register with a given value
void Fei4Cmd::wrRegister(int chipID, int address, int value) {
  // see page 75 of Fei4 Manual
  prepRegister(chipID, address, value);
  m_Txlink->send();
}

void Fei4Cmd::prepRegister(int chipID, int address, int value) {
  m_Txlink->write(0x005A0800+((chipID<<6)&0x3C0)+(address&0x3F));
  m_Txlink->write((value<<16)&0xFFFF0000);
}

// Read a whole global register
// Just sends the command, does not receive the data
void Fei4Cmd::rdRegister(int chipID, int address) {
  m_Txlink->write(0x005A0400+((chipID<<6)&0x3C0)+(address&0x3F));
  m_Txlink->send();
}

// Write the Pixel Shift register
// 672 bits of data = 21 * 32 bit
void Fei4Cmd::wrFrontEnd(int chipID, uint32_t bitstream[DoubleColumnBit::nWords]){
  prepFrontEnd(chipID, bitstream);
  m_Txlink->send();
}

void Fei4Cmd::prepFrontEnd(int chipID, uint32_t bitstream[DoubleColumnBit::nWords]){
  m_Txlink->write(0x005A1000+((chipID<<6)&0x3C0));
  
  //Flipping the order in order to send bit 671-0, and not bit 31-0, 63-21, etc.
  //todo check which way we should be doing this
  //	for(int i = 20 ; i >= 0 ; --i) {
  for(unsigned i = 0; i < DoubleColumnBit::nWords; ++i){
    m_Txlink->write(reverse(bitstream[i]));	
    }
  m_Txlink->write(0x00000000);
}

// Set FEI4 into run mode or conf mode
void Fei4Cmd::runMode(int chipID, bool mode) {
  uint32_t modeBits = mode ? 0x38 : 0x07;

  m_Txlink->write(0x005A2800+((chipID<<6)&0x3C0)+modeBits);
  m_Txlink->send();
}

// Reset FEI4 into intial state
void Fei4Cmd::globalReset(int chipID) {
  m_Txlink->write(0x005A2000+((chipID<<6)&0x3C0));
  m_Txlink->send();
}

// Send a global pulse with given length
// Used to activate global pulse regs (#27)
void Fei4Cmd::globalPulse(int chipID, int width) {
  prepPulse(chipID, width);
  m_Txlink->send();
}

void Fei4Cmd::prepPulse(int chipID, int width) {
  m_Txlink->write(0x005A2400+((chipID<<6)&0x3C0)+(width&0x3F));
}

void Fei4Cmd::send() {
  m_Txlink->send(); 
}
