/*
Steve Alkire <alkire@cern.ch> 2013.11.05
*/
#include <iostream>
#include "Fei4PixelCfg.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

const unsigned DoubleColumnBit::nWords;
//////////////////////TestFei4PixelCfg////////////////////
//todo remove the test function
void TestFei4PixelCfg::test(){
  Fei4PixelCfg fe;
  Mask m(MASK_HALF);
  std::cout<<"m "<<std::endl;
  DoubleColumnBit d = (DoubleColumnBit)m;
  std::cout<<"d "<<std::endl;
  d.dump();
  DoubleColumnBit d2;
  d2.set(0xABCD);
  std::cout<<"d2 "<<std::endl;
  fe.dcb(0,0) = d2;
  fe.dcb(0,3) = d2;
  fe.dcb(0,4) = d;
  std::cout<<"TDAC dump "<<std::endl;
  for(unsigned i = 0; i < 5; ++i){
    std::cout<<"i = "<<i<<std::endl;
    fe.TDAC(0).dump(i);
  }
  fe.outputEnable(0) = d2;
  std::cout<<"outputEnable "<<std::endl;
  fe.outputEnable(0).dump();
  std::cout<<"FDAC "<<std::endl;
  
  //You can do stuff like this..
  DoubleColumnBitField<4, false> FDACtest(&fe.dcb(0,9));  
  //  FDACtest.set(0xABCD,0);FDACtest.set(0xBCDA,1);FDACtest.set(0xCDAB,2);FDACtest.set(0xDABC,3);
  /*BUT DONT DO ANYTHING LIKE THIS!!!!!!!!!!!!!!!!!!!!!!!!
  *fe.FDAC[0] = FDACnew;
  */
  //you can do this:
  std::cout << "FDAC TEST" << std::endl;
  for(unsigned i = 0; i < 4; ++i){
    std::cout<<"i = "<<i<<std::endl;
    fe.FDAC(0).dump(i);
  } 

  //Testing doublecolumnbitfield
  
  fe.TDAC(23).setPixel(1,0xC);
  std::cout << fe.TDAC(23).getPixel(1) << std::endl;
  
  fe.TDAC(23).setPixel(2,2,0xA);
  std::cout << fe.TDAC(23).getPixel(2,2) << std::endl;
  
  std::cout << "TDAC DUMP" << std::endl;
  fe.TDAC(23)[0].dump();
  fe.TDAC(23)[1].dump();
  fe.TDAC(23)[2].dump();
  fe.TDAC(23)[3].dump();
  fe.TDAC(23)[4].dump();
  fe.TDAC(23)[0].setPixel(2,0x1);
  /*
  std::cout << "TDAC DUMP" << std::endl;
  fe.TDAC(23)[0].dump();
  fe.TDAC(23)[1].dump();
  fe.TDAC(23)[2].dump();
  fe.TDAC(23)[3].dump();
  fe.TDAC(23)[4].dump();
  std::cout << "TDAC DUMP" << std::endl;
  fe.TDAC(23)[0].dump();
  fe.TDAC(23)[1].dump();
  fe.TDAC(23)[2].dump();
  fe.TDAC(23)[3].dump();
  fe.TDAC(23)[4].dump();
  std::cout << "TDAC DUMP" << std::endl;
  fe.TDAC(23)[0].dump();
  fe.TDAC(23)[1].dump();
  fe.TDAC(23)[2].dump();
  fe.TDAC(23)[3].dump();
  fe.TDAC(23)[4].dump();

  */

}



///////////////////////Fei4PixelCfg///////////////////////
// void Fei4PixelCfg::dumpPixel(int dc){//todo fix this? or remove
//   //  for(unsigned i = 0 ; i < n_DC; ++i){
//     std::cout << "DC: " << dc << std::endl;
//     all[dc].dump();
  
// }

Fei4PixelCfg Fei4PixelCfg::operator^(const Fei4PixelCfg& rhs){
  Fei4PixelCfg fe;
  for(unsigned i = 0; i < n_DC*n_Bit; ++i){
    fe.m_dcb[i] = m_dcb[i] ^ rhs.m_dcb[i];
  }
  return fe;
}
Fei4PixelCfg::Fei4PixelCfg(){
  setPointers();
}

Fei4PixelCfg::Fei4PixelCfg(const Fei4PixelCfg& rhs){
  setPointers();
  std::copy(rhs.m_dcb,rhs.m_dcb+n_DC*n_Bit,m_dcb);
}

Fei4PixelCfg& Fei4PixelCfg::operator=(const Fei4PixelCfg& rhs){
  //  std::copy(rhs.m_dcb,rhs.m_dcb+n_DC*n_Bit,m_dcb);
  for( unsigned j_bit = 0; j_bit < n_Bit ; ++j_bit){
    for( unsigned i_dc = 0; i_dc < n_DC; ++i_dc){
      ((*this).dcb(i_dc, j_bit)).set(rhs.dcb(i_dc , j_bit));
    }
  }
  //  setPointers();
  return *this;
}

bool Fei4PixelCfg::operator==(const Fei4PixelCfg& rhs){
  bool areEqual = true;
  for( unsigned j_bit = 0; j_bit < n_Bit ; ++j_bit){
  //  for( unsigned j_bit = 0; j_bit < 9 ; ++j_bit){//todo readd FDAC!!!!!
    for( unsigned i_dc = 0; i_dc < n_DC; ++i_dc){
      areEqual = areEqual & 
	( (*this).dcb(i_dc, j_bit) == rhs.dcb(i_dc , j_bit) );
      //      std::cout << "areEqual? : " << areEqual << " at (bit,dc)=("  << j_bit << "," << i_dc << ")" << std::endl;
    }
  }
  return areEqual;
}

//Todo add copy constructor to DoubleColumnBitField
void Fei4PixelCfg::setPointers(){
  for(unsigned i_DC = 0; i_DC < n_DC; ++i_DC){
    DoubleColumnBitField<5, true> TDACtemp(&dcb(i_DC,1)); m_TDAC[i_DC] = TDACtemp;
    DoubleColumnBitField<4, false> FDACtemp(&dcb(i_DC,9)); m_FDAC[i_DC] = FDACtemp;
    DoubleColumnBitField<13, false> alltemp(&dcb(i_DC,0)); m_all[i_DC] = alltemp;
  }
}
////////////////DOUBLECOLUMNBIT//////////////
DoubleColumnBit DoubleColumnBit::operator^(const DoubleColumnBit& rhs){
  DoubleColumnBit b;
  for(unsigned i = 0; i < nWords; ++i){
    b.store[i] = store[i] ^ rhs.store[i];
  }
  return b;
}

void DoubleColumnBit::set(const uint32_t& word){
  for(unsigned i = 0; i < nWords; ++i){
    store[i] = word;
  }
}

void DoubleColumnBit::set(const uint32_t allWords[nWords]){
  for(unsigned i = 0; i < nWords; ++i){
    store[i] = allWords[i];
  }
}

void DoubleColumnBit::set(const DoubleColumnBit& dcb){
    for(unsigned i = 0; i < nWords; ++i){
      store[i] = dcb.store[i];
    }
}

//converts row, column pixel position to position of bit in uint32 array fed to double column
unsigned DoubleColumnBit::rowColumnToArrayBit(const unsigned& r, const unsigned& c){
  return(336 + (c%2)*(r-1) - ((c+1)%2)*r);
}

unsigned DoubleColumnBit::arrayBitToRow(const unsigned& n){
  //TODO
  return 0;
}

unsigned DoubleColumnBit::arrayBitToColumn(const unsigned& n){
  //TODO
  return 0;
}

void DoubleColumnBit::setPixel(const unsigned& n, const unsigned& val){
  unsigned w = n/32;
  unsigned b = n%32;
  unsigned mask = 1<<(b);
  store[w] = (store[w]&(~mask))|(mask&(val<<(b)));
}

void DoubleColumnBit::setPixel(const unsigned& row, const unsigned& column, const unsigned& val){
  unsigned n = rowColumnToArrayBit(row,column);
  setPixel(n,val);
}

unsigned DoubleColumnBit::getPixel(const unsigned& n) const{
  unsigned w = n/32;
  unsigned b = n%32;
  unsigned mask = 1<<(b);
  return((store[w]&(mask))>>(b));
}

unsigned DoubleColumnBit::getPixel(const unsigned& row, const unsigned& column) const{return(getPixel(rowColumnToArrayBit(row,column)));}

DoubleColumnBit DoubleColumnBit::operator&(const DoubleColumnBit& dc) const{
  DoubleColumnBit dcOut;
  for(unsigned i = 0; i < nWords; ++i){
    dcOut.store[i] = store[i]&dc.store[i];
  }
  return(dcOut);
}
DoubleColumnBit::DoubleColumnBit(){
  for(unsigned i = 0; i < nWords; ++i){
    store[i] = 0;
  }
}
DoubleColumnBit::DoubleColumnBit(const DoubleColumnBit& rhs){
  std::copy(rhs.store,rhs.store+nWords,store);
}

uint32_t* const DoubleColumnBit::getDoubleColumnBit(){
    return(&store[0]);
}
///////////////////MASK////////////////
//todo Note: can make more efficient
void Mask::shift(){
  unsigned cycBit = getPixel(671); 
  for(unsigned i = 671 ; i > 0 ; --i){
    setPixel(i, getPixel(i-1));
  }
  setPixel(0, cycBit);
}

//todo Note: can make more efficient 
void Mask::shiftBack(){
  unsigned cycBit = getPixel(0);
  for(unsigned i = 1 ; i <672 ; ++i){
    setPixel(i-1,getPixel(i));
  }
  setPixel(671, cycBit);
}

void Mask::set(const MaskType& mask){
  //todo use dcb
  uint32_t maskCode;
  // uint32_t wholeMaskCode[nWords] = {0};
  bool useWhole = false;
  
  switch(mask){
  case MASK_FULL:
    maskCode = 0xFFFFFFFF;
    break;
  case MASK_HALF:
    maskCode = 0x55555555;
    break;
  case MASK_QUARTER:
    maskCode = 0x11111111;
    break;
  case MASK_EIGHTH:
    maskCode = 0x01010101;
    break;
  case MASK_16:
    maskCode = 0x00010001;
    break;
  case MASK_32 :
    maskCode = 0x00000001;
    break;
  case MASK_672 :
    //     wholeMaskCode[nWords-1] = 0x00000001;//set the last pixel
    useWhole = true;
    break;
  case MASK_26880://Single Pixel
    uint32_t bits[DoubleColumnBit::nWords];
    for(unsigned i=1;i<DoubleColumnBit::nWords;i++)bits[i]=0;
    bits[0]=0x00000001;
    set(bits);
    break;
  break;
  case MASK_CLEAR:
    maskCode = 0x0;
    break;
  }
  useWhole ? setPixel(1,1,1) : set(maskCode);
}


void Mask::setXTALK(const MaskType& mask){

  //todo use dcb
  uint32_t maskCode;
  // uint32_t wholeMaskCode[nWords] = {0};
  bool useWhole = false;
  
  switch(mask){
  case MASK_FULL:
    maskCode = 0x0;
    break;
  case MASK_HALF:
    maskCode = 0x0;
    break;
  case MASK_QUARTER:
    maskCode = 0xaaaaaaaa;
    break;
  case MASK_EIGHTH:
    maskCode = 0x82828282;
    break;
  case MASK_16:
    maskCode = 0x80028002;
    break;
  case MASK_32 :
    maskCode = 0x80000002;
    break;
  case MASK_672 :
    //     wholeMaskCode[nWords-1] = 0x00000001;//set the last pixel
    useWhole = false;
    break;
    case MASK_26880://Single Pixel
    uint32_t bits[DoubleColumnBit::nWords];
    for(unsigned i=1;i<DoubleColumnBit::nWords;i++)bits[i]=0;
    bits[0]=0x80000002;
    set(bits);
    break;
  case MASK_CLEAR:
    maskCode = 0x0;
    break;
  }
  useWhole ? setPixel(1,1,1) : set(maskCode);

}



