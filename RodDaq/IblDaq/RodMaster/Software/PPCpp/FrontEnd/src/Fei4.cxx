#include "Fei4.h"
#include "IblRodInmemFifo.h"
#include <bitset>

#include "RodResourceManager.h"

#include <chrono>
#include <thread>

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

//Fei4::Fei4(FeLink * felink) : Fei4Cfg(), Fei4Cmd(felink->getTxLink()) {
// Fei4();
// m_link = felink;
//}

Fei4::Fei4() : Fei4Cfg(), Fei4Cmd(), m_link(nullptr) {
#ifdef LL_DEBUG
  static uint16_t feCounter(0);
  std::cout << __PRETTY_FUNCTION__ << " iteration #" << ++feCounter << std::endl;
#endif

  // Todo: manage verbosity as part of iostream
   verbose = false;

 // Initialization to be performed outside of this class because the class is not aware of the FE's location
 // initialize links for writing and reading to FE
   setFeLink( RodResourceManagerIBL::getDevNullFeLink() ); 

   std::copy(Fei4GlobalCfg::defaultCfg,Fei4GlobalCfg::defaultCfg+Fei4GlobalCfg::numberOfRegisters,cfg);

   // set defaults for the values of the registers set during beam standby
   // hard-coded, but can be overwritten with setAmp2VbnStandy and setPrmpVbpStandby and setDisVbnStandby functions
   m_prmpvbp_standby = 43;
   m_amp2vbn_standby = 0;
   m_disvbn_standby = 40; // not used for now
   m_preAmpsOn = false; // by default preAmps off
   m_standby_3DBM = true; // by default enable outermost 3DBM for beam monitioring when in standby
   m_checkSR = true; // by default read back SR when configuring
   std::fill(m_serviceRecord,m_serviceRecord+32, -1);

#ifdef LL_DEBUG
   std::cout << __PRETTY_FUNCTION__ << " terminated" << std::endl;
#endif
}

Fei4::~Fei4() {
}

void Fei4::reconfig(bool sendPixelCfg) {
  if (m_preAmpsOn) sendGlobalConfigAmpsOn();
  else sendGlobalConfigAmpsOff();
  if(sendPixelCfg) sendPixelConfig();
}

void Fei4::sendGlobalConfig(bool ampsOn, bool clearSR) {

  this->setRunMode(false);
  for(unsigned int i = 0; i < numberOfRegisters; i++) {
    writeRegisterHeader(i);
    m_link->write(correctedReg(i,ampsOn)<<16);//MB this technically add sixteen 0s (16 BC) at the end of each WriteRegister 
  }
  m_link->send();
  this->finishConfig(clearSR);
}

void Fei4::sendGlobalConfigAmpsOn(bool clearSR){
  //xil_printf("INFO: Sending Global Config with pre-amps ON\n");
  m_preAmpsOn = true;
  this->sendGlobalConfig(true, clearSR);
}

void Fei4::sendGlobalConfigAmpsOff(bool clearSR) {
  //xil_printf("INFO: Sending Global Config with pre-amps OFF\n");
  m_preAmpsOn = false;
  this->sendGlobalConfig(false, clearSR);
}

void Fei4::finishConfig(bool clearSR) {

   // fix GR read back for certain picky FEs
   // loop over DCs rather than sending to all because draws less current
   // safe for b-field 
   Mask mask;  
   mask.set(MASK_CLEAR);
   this->writeRegister(&Fei4::CP,0x0); // set DC mode to only addressed DC
   for (unsigned i = 0; i < n_DC; i++) {
     this->writeRegister(&Fei4::Colpr_Addr, i); 
     this->wrFrontEnd(mask.getDoubleColumnBit()); 
   }

   if (clearSR) { // if reading back SR, don't want to do this clearing now. First enable channel, then read them back.
     // otherwise, have to clear service records after powerup
     // write GR 27 to 0x9000 and then back to normal value to get rid of 30 bit pattern on a few FEs
     m_link->write(0x005A0800|27|(m_chipID<<6));  
     m_link->write(0x9000<<16);  
     m_link->send();  
     
     this->globalPulse(10); 
     
     m_link->write(0x005A0800|27|(m_chipID<<6)); 
     m_link->write(cfg[27]<<16);
     m_link->send();   
   }
}

void Fei4::loadOnesInSR() { 
  
#ifdef __XMK__
  xil_printf("loadOnesInSR\n");
#endif

  for (unsigned int i_dc = 0; i_dc < n_DC; i_dc++)	
    {
      this->prepRegister(&Fei4::CP,0x0); //set DC mode to only addressed DC
      this->prepRegister(&Fei4::Colpr_Addr,i_dc);
      this->prepRegister(&Fei4::S0,0x1);
      this->prepRegister(&Fei4::S1,0x0);
      this->prepRegister(&Fei4::HLD,0x0);
      this->prepRegister(&Fei4::SRK,0x1);
      
      this->prepPulse(10);
      m_link->write(0x0000); // add some time for pulse to propogate
            
      
      this->prepRegister(&Fei4::S0,0x0);
      this->prepRegister(&Fei4::S1,0x0);
      this->prepRegister(&Fei4::HLD,0x0);
      this->prepRegister(&Fei4::SRK,0x0);
      
      m_link->send();
    }

  //Return to column 0
  this->prepRegister(&Fei4::Colpr_Addr,0x0);
}

void Fei4::clearFullShiftRegister() { 
  
  //Clear the shift register (SR)
  //Select DC for operations
  //Set S0=S1=HitLd=0
  //Set SRClr=1
  //Pulse SR clear line
  
  //Clear the shift register
  
  for (unsigned int i_dc = 0; i_dc < n_DC; i_dc++)	
    {
      
      this->writeRegister(&Fei4::SRR,0x0);
      this->writeRegister(&Fei4::CP,0x0); //set DC mode to only addressed DC
      this->writeRegister(&Fei4::Colpr_Addr,i_dc);
      this->writeRegister(&Fei4::S1,0x0);
      this->writeRegister(&Fei4::S0,0x0);
      this->writeRegister(&Fei4::HLD,0x0);
      this->writeRegister(&Fei4::SRC,0x1);
      this->globalPulse(1);
      
      this->writeRegister(&Fei4::SRC,0x0); 
    }
  
}



void Fei4::readBackShiftRegister(uint32_t bitstream[], uint8_t regDC,uint8_t regCP,bool reverseBits, bool complementToOne) { 
  
  //Mask mask;
  //mask.set(MASK_CLEAR);
  //bit-stream for the readback is all to 0
  uint32_t test_bitstream[DoubleColumnBit::nWords]; //21
  for (unsigned int iword = 0 ; iword<DoubleColumnBit::nWords; iword++)
    test_bitstream[iword] = 0;
  
  this->prepRegister(&Fei4::CP,0x0);
  //Put both FEs to double column 40
  //m_link->write(0x005A0800+((8<<6)&0x3C0)+(22&0x3F));
  //m_link->write(0x00140000);

  
  //Set dc for the correct FE
  this->prepRegister(&Fei4::Colpr_Addr,regDC);
  this->prepRegister(&Fei4::SRR,0x0);
  this->prepRegister(&Fei4::S1,0x0);
  this->prepRegister(&Fei4::S0,0x0);
  this->prepRegister(&Fei4::HLD,0x0);
      
  this->prepRegister(&Fei4::SRR,0x1);
  //Fei4Cmd::prepFrontEnd(8,mask.getDoubleColumnBit());
  this->prepFrontEnd(test_bitstream);
  
  //Set dc for both FE to 0
  //m_link->write(0x005A0800+((8<<6)&0x3C0)+(22&0x3F));
  //m_link->write(0x00000000);
  
  this->prepRegister(&Fei4::SRR,0x0);
  m_link->send();
  
  //std::vector<int> current_frame =  this->readFrame(false,false);  //standard read back function
  std::vector<int> current_frame = this->readFrame(false,true); //new read back function
  
#ifdef LL_DEBUG
  
  //debug
  for (unsigned int i =0; i<current_frame.size();i++)
    {
      std::cout<<"Entry:"<<i<<" "<<current_frame[i]<<"-->"<<std::hex<<current_frame[i]<<std::dec<<"-->"<<std::bitset<32>(current_frame[i])<<std::endl;
      if (current_frame[i]==0xbc)
  	std::cout<<"---------------------"<<std::endl;
    }
#endif
    
  DecodeToBitStream(bitstream, current_frame,reverseBits,complementToOne);
  
#ifdef LL_DEBUG
  for (unsigned int iword = 0; iword<DoubleColumnBit::nWords; ++iword)
    std::cout<<"double column "<<(int)regDC<<" bitstream["<<iword<<"]=0x"<<std::hex<<bitstream[iword]<<std::dec<<std::endl;
#endif
  
}


//This function translates the read back frame to a an array of 21 32-bit words. 
void Fei4::DecodeToBitStream (uint32_t bitstream[],const std::vector<int>& frame, bool reverseBits, bool complementToOne)
{
  
  //The frame is made of a sequence of Address Record (AR), Value Record (VR), AR, VR, ...
  //Each AR(VR) is made of 3 8-bit words.
  //When reading back the shift register I expect a frame of 252 elements
  
  if (frame.size() != 252) {

    #ifdef __XMK__
    xil_printf("DecodeToBitStream::Shift Register readback failed.. Returning empty bitstream \n");
    xil_printf("frame size = %d", (int) frame.size());
    #endif

    return;
  }
  
  uint32_t word = 0;
  uint32_t word_count = 0;
  uint16_t bit_count = 0 ;

  //Start from the first VR header.
  for (unsigned int iframe = 3; iframe < frame.size()-2; iframe=iframe+6) {
    
    if ((iframe % 3 == 0) && frame[iframe] == 0xEC)
      {
	
	//protection
	if (word_count > 20) {
	  #ifdef __XMK__
	  xil_printf("ERROR: Fei4::DecodeToBitStream::there is something wrong in this algorithm");
	  #endif
	  break;
	}
	  
	  word += (((frame[iframe+1] & 0XFF)<< 8) + (frame[iframe+2] & 0XFF));
	bit_count++;
	
	if (bit_count % 2 != 0)
	  word = word << 16;
	else {

	  if (complementToOne) word = ~word;
	  if (reverseBits)     word = reverse(word);
	    
	  bitstream[word_count] = word;
	  word_count++;
	  word=0;
	  	  
	}//filling the 32-bit word
	
      }//this should be always realised
    else {
      std::cout<<"Expected value record header 0xec, found:"<<std::hex<<frame[iframe]<<std::dec<<std::endl;
      break;
    }
    
    
  }//end of decoding
    


}

  
void Fei4::readBackSR() {
 // write GR 27 to 0x9000 and then back to normal value to get rid of 30 bit pattern on a few FEs
   m_link->write(0x005A0800|27|(m_chipID<<6));  
   m_link->write(0x9000<<16);  
   m_link->send();  

   this->globalPulse(10); 
   
   std::this_thread::sleep_for(std::chrono::milliseconds(10));

   
   std::vector<int> current_frame = this->readFrame();
   
   std::cout<<"For RX: "<<(int)this->getRxCh()<<" reading SR:"<<std::endl;
   for (size_t i = 2; i<current_frame.size(); i = i+3) {
     //     printf("raw data = 0x:  %2x %2x %2x",(int)current_frame[i-2],(int)current_frame[i-1],(int)current_frame[i]);
     Fei4Data::printServiceRecord(current_frame[i-1], current_frame[i]);
     this->saveServiceRecord(current_frame[i-1], current_frame[i]);
   }
   std::cout<<std::dec<<std::endl;
   m_link->write(0x005A0800|27|(m_chipID<<6)); 
   m_link->write(cfg[27]<<16);
   m_link->send();   
}


void Fei4::sendPixelConfig(bool normalScan){
  this->setRunMode(false);
  this->writeRegister(&Fei4::CP,0x0); // set DC mode to only addressed DC

  for(int j_bit = n_Bit-1; j_bit>=0; --j_bit){
    if (normalScan) if ((j_bit < 9 && j_bit > 5) || (j_bit == 0)) continue; // only send fdac and tdac for normal scan
      for(unsigned i_dc = 0; i_dc < n_DC; ++i_dc){
	//since we store each dcb separately, we have to loop over the values
	if (((i_dc%4 == 0) && (i_dc != 0)) || (i_dc == n_DC-1)) {
	  this->strobePixelLatches(this->dcb(i_dc , j_bit), 0x1 << j_bit, i_dc, true); // only send every 4th DC or last
	}
	else this->strobePixelLatches(this->dcb(i_dc , j_bit), 0x1 << j_bit, i_dc, false); 
      }
  }
}

  //todo is this where these should be?
Fei4ExtCfg Fei4::rdExtConfig(uint32_t iFe){

  Fei4ExtCfg extCfg;
#ifdef __XMK__  
  uint16_t translated_cfg[Fei4Cfg::numberOfRegisters];  
  Fei4* fe =  RodResourceManagerIBL::getInstance().getCalibModuleConfigs();
  extCfg = fe[iFe]; // gets the tx/rx etc from ExtCfg

  //read information from the FE
  for(unsigned i = 0; i < Fei4Cfg::numberOfRegisters ; i++){                                                                                         
#ifdef LL_DEBUG
    std::cout << "Sending Cmd to rdRegister "  << i << std::endl;                                                                  
#endif
    //todo physics config version too?


    fe[iFe].rdRegister(i);                                                                                                                       
    
    //todo do we have a better way to do this yet?
  std::vector<int> current_frame = fe[iFe].readFrame();                                                                                        
    translated_cfg[i] = (0x100)*(current_frame[current_frame.size()-2]) + current_frame[current_frame.size() - 1];
  }

  extCfg.setCfg(translated_cfg);
#else 
  std::cout << __PRETTY_FUNCTION__ <<  "doesn't do anything on the host, returning default config" << std::endl;

#endif

    return extCfg;
}

Fei4PixelCfg Fei4::rdbackPixelConfig(bool readFullPixelConfig, uint32_t delay, int ShiftRegLeft){

  uint32_t readbackbitstream[DoubleColumnBit::nWords];
  Fei4PixelCfg PixelCfg;
  
  for (unsigned int i_dc  = 0; i_dc < n_DC; ++i_dc)
    {
      for(int j_bit = n_Bit-1; j_bit>=0; --j_bit){
	//Only read for 1 bit
	if (!readFullPixelConfig) { //read only the shift register
	  //The shift register of each DC will be read 13 times.
	  //clearFullShiftRegister();
	  if (delay>0) {
         std::this_thread::sleep_for(std::chrono::microseconds(delay));
	  }
	  readBackShiftRegister(readbackbitstream,i_dc,0,true,false);
	  PixelCfg.dcb(i_dc,j_bit).set(readbackbitstream);
	  break;
	} // read only shift register
	
	else { //read all configuration
	  
	  //Make sure the SRR is at 0, Select the  DC
	  this->writeRegister(&Fei4::SRR,0x0);
	  this->writeRegister(&Fei4::CP,0x0);
	  this->writeRegister(&Fei4::Colpr_Addr,i_dc);
	  
	  //Put the ShiftRegister in parallelMode
	  this->writeRegister(&Fei4::S0,0x1);
	  this->writeRegister(&Fei4::S1,0x1);
	  this->writeRegister(&Fei4::HLD,0x0);
	  
	  //latches path: [0] OutputEnable; [1:5] TDAC 1=MSB; [6] Large Inj cap; [7] Small inj cap
	  //[8] Imon and Hitbusout
	  //[9:12] FDAC [12]=MSB
	  //SR: Enable digital injection
	  // See  Table 26, pag 85 FEi4 manual 
	  //Select the latch to copy  (let's copy the ENABLE only for the moment)
	  //Register 13
	  //F   E   D   C   B   A   9   8   7   6   5   4   3   2   1   0
	  //S1  S0  Pixel latch strobe 0 --------Pixel latch strobe 12  -
	  
	  //To set the ENABLE we need to pass 1, which is equivalent of doing 0x1 << 0. 
	  //Decided to use this to maintain the same logic of sendPixelCfg
	  
	  
	  
	  
	  this->writeRegister(&Fei4::Pixel_latch_strobe,(0x1FFF & (0x1 << j_bit)));
	  
	  //Set SR_Clk to 1
	  this->writeRegister(&Fei4::SRK,0x1);
	  
	  
	  this->globalPulse(10);
	  //add some time for pulse to propagate needed?
	  //m_link->write(0x0000);
	  
	  //m_link->send();
	  
	  //Reset GR 13 to 0
	  this->writeRegister(&Fei4::S0,0x0);
	  this->writeRegister(&Fei4::S1,0x0);
	  this->writeRegister(&Fei4::Pixel_latch_strobe,0);
	  
	  //Set SR_Clk to 0
	  this->writeRegister(&Fei4::SRK,0x0);

	  
	  readBackShiftRegister(readbackbitstream,i_dc,0,true,true);
	  PixelCfg.dcb(i_dc,j_bit).set(readbackbitstream);
	  
	}//read configuration
	
      }//latch
    }//double column
  
  //std::cout<<"Checking the configs"<<std::endl;
  bool cfgEqual = true;
  if (readFullPixelConfig) {
    for( unsigned j_bit = 0; j_bit < n_Bit ; ++j_bit){
      for( unsigned i_dc = 0; i_dc < n_DC; ++i_dc){
	if (!((*this).dcb(i_dc, j_bit) == PixelCfg.dcb(i_dc , j_bit)))  {
	  cfgEqual=false; //do something with this?
	  //std::cout<<(int)i_dc<<" "<<(int) j_bit<<" is different"<<std::endl;
	  //for (unsigned int iword = 0; iword<DoubleColumnBit::nWords;iword++) {
	  //std::cout<<"expected["<<iword<<"]=0x"<<std::hex<<(*this).dcb(i_dc, j_bit).getDoubleColumnBit()[iword]<<std::dec<<" seen["<<std::dec<<iword<<"]=0x"<<std::hex<<PixelCfg.dcb(i_dc , j_bit).getDoubleColumnBit()[iword]<<std::dec<<std::endl;
	  //}
	}//double column bit loop
      }//latch loop
    }//double column loop
    if (cfgEqual) std::cout<<"Pixel Cfgs Rx ch "<<(int)this->getRxCh()<<" match"<<std::endl;
    else std::cout<<"Pixel Cfgs Rx ch "<<(int) this->getRxCh()<<"does not match"<<std::endl;
  }
  
  Mask mask;
  //xil_printf("Shift Reg Left: %d \n", ShiftRegLeft);
  if (ShiftRegLeft==1) {
    mask.set(MASK_FULL);
    this->writeRegister(&Fei4::CP,0x0); // set DC mode to only addressed DC
    for (unsigned i = 0; i < n_DC; i++) {
      this->writeRegister(&Fei4::Colpr_Addr, i); 
      this->wrFrontEnd(mask.getDoubleColumnBit()); 
    }
  }
  else if (ShiftRegLeft==2) {
    this->writeRegister(&Fei4::CP,0x0); // set DC mode to only addressed DC
    for (unsigned i = 0; i < n_DC; i++) {
      this->prepRegister(&Fei4::Colpr_Addr, i); 
      Fei4Cmd::prepFrontEnd(m_chipID, (*this).dcb(i,0).getDoubleColumnBit());   
      m_link->send();
    }
  }
  else  {
      mask.set(MASK_CLEAR);
      this->writeRegister(&Fei4::CP,0x0); // set DC mode to only addressed DC
      for (unsigned i = 0; i < n_DC; i++) {
	this->writeRegister(&Fei4::Colpr_Addr, i); 
	this->wrFrontEnd(mask.getDoubleColumnBit()); 
      }
    }
  return PixelCfg;
}

Fei4PixelCfg  Fei4::rdPixelConfig(uint32_t iFe){
  Fei4PixelCfg pixCfg;
#ifdef __XMK__
  Fei4* fe =  RodResourceManagerIBL::getInstance().getCalibModuleConfigs();

  fe->writeRegister(&Fei4::SRR,0x1); //Turn on the shift register read!
  //todo is this right?
  for(unsigned j_bit = 0; j_bit < n_Bit; ++j_bit){
    for(unsigned i_dc = 0; i_dc < n_DC; ++i_dc){
      fe->strobePixelLatches(fe->dcb(i_dc , j_bit), 0x1 << j_bit, i_dc);
	//since we store each dcb separately, we have to over the values
	//todo readFrame here
      }
  }
  fe->writeRegister(&Fei4::SRR,0x0);//shut off the shift register read back

  //todo
#else 
  std::cout << __PRETTY_FUNCTION__ <<  "doesn't do anything on the host" << std::endl;
#endif
  return pixCfg;
}

Fei4Cfg Fei4::rdFullConfig(uint32_t iFe){//todo extCfg also!
  Fei4Cfg fei4Cfg;
#ifdef __XMK__
  Fei4::rdPixelConfig(iFe);
  Fei4::rdExtConfig(iFe);
#else 
  std::cout << __PRETTY_FUNCTION__ <<  "doesn't do anything on the host" << std::endl;
#endif
  return fei4Cfg;
}

bool Fei4::checkGlobalConfig(bool ampsOn, bool sendBack, std::vector<uint32_t> & readBackGlobal) {
  
  readBackGlobal.clear();
  //PF::JP => The follow line is a patch to make it always consistent with the FE status
  //No need to pass ampsOn.
  ampsOn = m_preAmpsOn;

#ifdef __XMK__
  if (verbose)
    xil_printf("For FE: %d m standby 3DBM = %d \n", (int)this->getRxCh(), (int)m_standby_3DBM);
#endif

  uint16_t regValue;
  bool success = true;

  // don't check the global config if using the boc emulator
  if (RodResourceManagerIBL::bocEmu()) return true;

  for(unsigned int i = 1; i < (Fei4GlobalCfg::numberOfRegisters-1 ); i++) { // don't check register 0 or 35
    
    regValue = this->getRegisterValueFromFE(i);
    
#ifdef __XMK__
    if (verbose) 
      xil_printf("Register: %d  Value: 0x%04x /n",(int)i, regValue);

    if (sendBack)
      {
	uint32_t value = (correctedReg(i,ampsOn)<<16);
	readBackGlobal.push_back(value+regValue);
      }
    
#endif
    if (regValue == 0xdead) {
#ifdef __XMK__
      xil_printf("WARNING: could not read register %d from FE: %d\n",(int)i, (int)(this->getRxCh()));
#endif
      
      success = false;
      continue;
    }
    
    if (regValue != correctedReg(i, ampsOn)) {
      //std::cout<<"WARNING: Rx "<<(int)(this->getRxCh())<<" For GR: "<<(int)i<<" read back 0x:"<<std::hex<<(int)regValue<<
      //" when I expected: 0x"<<std::hex<<(int)correctedReg(i, ampsOn)<<", retrying"<<std::dec<<std::endl;
      std::this_thread::sleep_for(std::chrono::microseconds(100));
      regValue = this->getRegisterValueFromFE(i);
      if (regValue != correctedReg(i, ampsOn)) {
	#ifdef __XMK__
	xil_printf("WARNING: Rx %d For GR: %d 0x:%04x when I expected : 0x%04x \n", (int)(this->getRxCh()), (int)i, (int)regValue,(int)correctedReg(i, ampsOn));
#endif
	//std::cout<<"WARNING: Rx "<<(int)(this->getRxCh())<<" For GR: "<<(int)i<<" read back 0x:"<<std::hex<<(int)regValue<<
	//" when I expected: 0x"<<std::hex<<(int)correctedReg(i, ampsOn)<<" second time"<<std::dec<<std::endl;
	success = false;
      }
    }
  }

  if (success) {
#ifdef __XMK__
    xil_printf("INFO: Rx %d Global Config checked-out OK \n", (int)(this->getRxCh()));
  #endif
  }  else {
#ifdef __XMK__
    xil_printf("ERROR: Rx %d Global Config did not check-out OK \n", (int)(this->getRxCh()));
#endif
    success = false;
  }
  return success;
}


uint16_t Fei4::correctedReg(int i, bool ampsOn) {

  if (i == 20) return 0xFFFF; // threshold high while doing all configure / slow command operations
  if (ampsOn) return cfg[i]; // default config values are for pre-amps ON
  
  // Temporarily (?) enable only the outermost DC in the outermost 3D modules (Rx 15 and 16)
  // to serve as a beam monitor
  // keep the preamps at nominal for these modules to see actual hits

  if (16 == this->getRxCh() && m_standby_3DBM) {
    if (i == 23) return 0xFFFE;
    if (i == 24) return 0xFFFF;
    if (i == 25) return ((cfg[25]&0xFF00)|(0x00FF));
  } 
  else if (15 == this->getRxCh() && m_standby_3DBM) {
    if (i == 23) return 0xFFFF;
    if (i == 24) return 0xFFFF;
    if (i == 25) return ((cfg[25]&0xFF00)|(0x007F));
  } 
  else {
    // write specific values for prmbvbp, prmpvbp_L, prmpvbp_R, amp2vbn, and disvbn for warm startup
    // cumbersome in order to keep config value for other registers in same field (see FEI4 register map)
    
    if (i == 5) return ((cfg[5]&0x00FF)|((Fei4Cmd::reverse8(m_prmpvbp_standby))<<8)); // PrmpVbp_R
    if (i == 6) return ((cfg[6]&0xFF00)|(Fei4Cmd::reverse8(m_prmpvbp_standby))); // PrmpVpb 
    if (i == 8) return ((cfg[8]&0x00FF)|((Fei4Cmd::reverse8(m_amp2vbn_standby))<<8)); // Amp2Vbn
    if (i == 11) return ((cfg[11]&0xFF00)|(Fei4Cmd::reverse8(m_prmpvbp_standby))); // PrmpVbp_L
    
  // for now, don't set these registers to special values in standby
  // but keep them here just in case
  //  if (i == 7) return ((cfg[7]&0xFF00)|(Fei4Cmd::reverse8(m_disvbn_standby))); // DisVbn
  //  if (i == 23) return (0xFFFF); // disableColCnfg0
  //  if (i == 24) return (0xFFFF); // disableColCnfg1
  //  if (i == 25) return ((cfg[25]&0xFF00)|(0x00FF)); // disableColCnfg2

  }

  return cfg[i]; // for all other regs, even in standby, send default value
}


void Fei4::strobePixelLatches() {
  // set the sequence of registers to enable shifting
  this->prepRegister(&Fei4::SRK,0x0); 
  this->prepRegister(&Fei4::S0,0x0);
  this->prepRegister(&Fei4::S1,0x0);
  this->prepRegister(&Fei4::HLD,0x0);
  this->prepRegister(&Fei4::LEN,0x1);

  // pulse
  this->prepPulse(10);  

  // return regsiters to 0 to prevent accidental latching from SEU
  m_link->write(0x0000); // add some time for pulse to propogate
  this->prepRegister(&Fei4::LEN,0x0);
  this->prepRegister(&Fei4::S0,0x0);
  this->prepRegister(&Fei4::S1,0x0);

}

void Fei4::strobePixelLatches(Mask mask, uint32_t pixelStrobeValue, uint8_t dc, bool send) {
  
  // set double column
  this->prepRegister(&Fei4::Colpr_Addr, dc);
  // write mask into shift register
  // this mask will determine what values are latched into the pixel latches selected by the Pixel_latch_strobe GR
  Fei4Cmd::prepFrontEnd(8,mask.getDoubleColumnBit());
  //  for (int i = 0; i < 21; ++i) {
  //    if (verbose) std::cout<<std::hex<<"pixelMask["<<i<<"]: "<<(int)mask.getDoubleColumnBit()[i]<<std::dec<<std::endl; 
  //  }
  // which pixel registers to latch
  this->prepRegister(&Fei4::Pixel_latch_strobe,(0x1FFF & pixelStrobeValue)); 
    // strobe the pixel latches which have been set in Pixel_latch_strobe register
  this->strobePixelLatches();
  // return to 0 for safety
  this->prepRegister(&Fei4::Pixel_latch_strobe,0x0);
  if (send)  this->send();
} 


//todo maybe offer a version without pixelStrobeValue or default it to 0x1fff i.e. all 13 pixel bits
void Fei4::strobePixelLatches(DoubleColumnBit dcb, uint32_t pixelStrobeValue, uint8_t dc, bool send) {

  // set double column
  this->prepRegister(&Fei4::Colpr_Addr, dc);
  // write dcb into shift register
  // this dcb will determine what values are latched into the pixel latches selected by the Pixel_latch_strobe GR
  Fei4Cmd::prepFrontEnd(8,dcb.getDoubleColumnBit());
#ifdef LL_DEBUG
  std::cout<<"PixelStrobe Value: "<<std::dec<< (int) pixelStrobeValue<<std::endl;
  for (unsigned int i = 0; i < DoubleColumnBit::nWords; ++i) {//nWords=21
    std::cout<<std::hex<<"pixelMask["<<i<<"]: "<<(int)dcb.getDoubleColumnBit()[i]<<std::dec<<std::endl; 
  }
#endif
  // which pixel registers to latch
  this->prepRegister(&Fei4::Pixel_latch_strobe,(0x1FFF & pixelStrobeValue)); 
    // strobe the pixel latches which have been set in Pixel_latch_strobe register
  this->strobePixelLatches();
  // return to 0 for safety
  this->prepRegister(&Fei4::Pixel_latch_strobe,0x0);
  if (send)  this->send();
}

//Set same mask to all the Pixels
//WARNING DO NOT USE AFTER GREEN LIGHT FROM EXPERTS, MAY NOT BE SAFE FOR B-FIELD!!!
void Fei4::broadcastPixelLatches(Mask mask, uint32_t pixelStrobeValue, bool send) {

  std::cout<<__PRETTY_FUNCTION__<<" SKIPPING, VERIFY THAT IS SAFE FOR B-FIELD!!! "<<std::endl;
  return;

  uint8_t cpMode = getValue(&Fei4::CP);
  this->prepRegister(&Fei4::CP,0x3); // set DC mode to one DCs

  // this mask will determine what values are latched into the pixel latches selected by the Pixel_latch_strobe GR
  Fei4Cmd::prepFrontEnd(8,mask.getDoubleColumnBit());

  // which pixel registers to latch
  this->prepRegister(&Fei4::Pixel_latch_strobe,(0x1FFF & pixelStrobeValue)); 
    // strobe the pixel latches which have been set in Pixel_latch_strobe register
  this->strobePixelLatches();
  // return to 0 for safety
  this->prepRegister(&Fei4::Pixel_latch_strobe,0x0);
  this->prepRegister(&Fei4::CP,cpMode); // Restore DC mode
  if (send)  this->send();

}

//Header bits for the write register command
void Fei4::writeRegisterHeader(unsigned i){
  m_link->write(0x005A0800|i|(m_chipID<<6)); // MB this technically add nine 0s (9 BC) in fron of each WriteRegister
}

void Fei4::printServiceRecordErrors() {
  if (verbose) std::cout <<" hello, I'm in function: "<<__PRETTY_FUNCTION__<<std::endl;
	std::cout << "Service record count:\n";
	for (size_t i=0; i < m_serviceRecordCount.size(); ++i) {
		if (m_serviceRecordCount[i] > 0)
		  std::cout << std::setw(24) << Fei4Data::serviceRecordString(i) << ":\t" << m_serviceRecordCount[i] << "\n";
	}
}

void Fei4::setRunMode(bool mode) {
  // when configuring the chip, need to set Vthreshold high first
  // then set Vthreshold back to what it was previously before running
  // Vthreshold = register 20

  if (mode == false) {
  //if going into config mode, set Vthreshold high manually
    this->runMode(false);
    m_link->write(0x005A0800+((m_chipID<<6)&0x3C0)+(20&0x3F));
    m_link->write(0xFFFF0000);
    m_link->send();
  }
  else {
    // if going into run mode with pre-amps ON, send the configuration value of Vthreshold
    // otherwise, if preAmps are OFF, keep threshold high
    // temporary (?) modification for 3D: send nominal threshold for outermost 3D modules

    if (m_preAmpsOn || (15 == this->getRxCh() || 16 == this->getRxCh())) this->sendThreshold();
    this->runMode(true);
  }

  m_runMode = mode;
}

//Fei4::Fei4(const Fei4& rhs): Fei4Cfg(*(Fei4Cfg*)&rhs){}

//Todo replace usage with copy constructor
// special copy function in case you want to exlcude some members...
void Fei4::copyConfig(const Fei4ExtCfg & cfgToCopy) {

  // Todo: copy pixel registers too?
  // Copy global FE registers in config

  //If you just want to copy Fei4PixelCfg jsut do this instead of special functions:
  //todo check if I (Russell) have solved this problem with = operators

  // Hard-coded safeguard against writing Vthin_Fine < 64 when Vthin_Coarse is zero
  // safe for b-field                                                                
  
  *((Fei4GlobalCfg*)this) = *((Fei4GlobalCfg*)&cfgToCopy);

  if ((cfgToCopy.Vthin_Coarse.value() == 0) && ((Fei4Cmd::reverse8(cfgToCopy.Vthin_Fine.value())) < 64)) { 
    std::cout << "WARNING: Ignoring instruction to set FE threshold below (Vthin_Coarse, Vthin_Fine) = (0,64)." << std::endl;
    std::cout << "WARNING: Setting Vthin_fine to 64" << std::endl;
    this->writeRegister(&Fei4::Vthin_Fine, 64);
  }


  // Setting the fei4extcfg member variables
  m_rxCh = cfgToCopy.getRxCh();
  m_txCh = cfgToCopy.getTxCh();
 
  // Setting the link channels
  if (m_link) {
    m_link->setRxCh(m_rxCh);
    m_link->setTxCh(m_txCh); 
  }
  m_enabled = cfgToCopy.getEnabled();
  m_chipID = cfgToCopy.getChipID();
  m_moduleID = cfgToCopy.getModuleID();
  m_revision = cfgToCopy.getRevision();
  m_vcal0 = cfgToCopy.getVcal0();
  m_vcal1 = cfgToCopy.getVcal1();
  m_cinjLow = cfgToCopy.getCinjLow();
  m_cinjMed = cfgToCopy.getCinjMed();
  m_cinjHigh = cfgToCopy.getCinjHigh();
  m_serialNumber = cfgToCopy.getSerialNumber();
}

uint32_t Fei4::getVcalFromCharge(float charge, int capType) {
 
  float cinj = ((capType ==2) ? this->getCinjHigh() : ((capType ==1) ? this->getCinjMed() : this->getCinjLow()));

  uint32_t vcal = (uint32_t)((((0.160218*charge)/cinj) - this->getVcal0()) / this->getVcal1());

  //std::cout<<" for cinj = "<<cinj<<" and charge "<<charge<<" calculated a vcal of : "<<(int)vcal<<std::endl;
  return vcal;
}

void Fei4::writeVcalFromCharge(float charge, int capType) {
  uint32_t vcal = this->getVcalFromCharge(charge,capType);
  if (vcal > 1023) {
    //std::cout<<"WARNING! Calculated a vcal greater than 1023. Setting to 1023 (max value.)"<<std::endl;
    vcal = 1023;
  }
   this->writeRegister(&Fei4::PlsrDAC,vcal);
}

uint16_t Fei4::getRegisterValueFromFE(uint8_t gRegAddr) {
#ifdef __XMK__
	uint16_t gRegValue = 0;
	this->rdRegister(gRegAddr);
   std::this_thread::sleep_for(std::chrono::microseconds(100));
	std::vector<int> current_frame = this->readFrame();

	if (current_frame.size() < 3) {
	  //std::cout<<"no frame received for register: "<<(int)gRegAddr<<std::endl;
	  //std::cout<<"retrying to read frame"<<std::endl;
     std::this_thread::sleep_for(std::chrono::microseconds(100));
	  current_frame = this->readFrame();
	  if (current_frame.size() < 3) {
#ifdef __XMK__
	    xil_printf("no frame received for register: %d \n", (int)gRegAddr);
#endif
	    //std::cout<<"no frame received for register: "<<(int)gRegAddr<<" second time"<<std::endl;
	    return 0xdead;
	  }
	}
	
	/*
	if ((((uint16_t)current_frame[1]<<8)+current_frame[2]) != gRegAddr) {
	  std::cout<<"my check = "<<(int)(((uint16_t)current_frame[1]<<8)+current_frame[2])<<std::endl;
	  std::cout<<"For register: "<<(int)gRegAddr<<" reading: 0x:";
	  for (size_t i = 0; i<current_frame.size(); i++)
	    std::cout<<std::hex<<(int)current_frame[i];
	  std::cout<<std::dec<<std::endl;
	}
	*/
	
	gRegValue = (current_frame[current_frame.size()-2]<<8) + current_frame[current_frame.size() - 1];
	
	return gRegValue;
#endif
 return 0; //to suppress warnings about no return value when __XMK__ is not defined
}

void Fei4::readSerialNumber() {
  // Todo: make compatible with current scheme of things!
  this->setRunMode(false);

  this->writeRegister(&Fei4::EFS,1);
  this->globalPulse(10);
  this->writeRegister(&Fei4::EFS,0);

  m_serialNumber = this->getRegisterValueFromFE(35);

  if (m_serialNumber == 0xdead) {
    std::cout<<"Could not read the SN for chip with Rx: "<<(int)m_rxCh<<std::endl;
    return;
  }

  std::cout<<"----"<<std::endl;
  std::cout<<"For Fe with Rx: "<<(int)m_rxCh<<" I got SN from the chip: "<<(int)m_serialNumber<<std::endl;
  std::cout<<"----"<<std::endl;
  if (cfg[35] != m_serialNumber) {
    std::cout<<"ERROR, SN from chip does not match SN in config! "<<std::endl;
    std::cout<<"For Fe with Rx: "<<(int)m_rxCh<<" I got SN from the chip: "<<(int)m_serialNumber<<std::endl;
    std::cout<<"and SN in config is: "<<(int)cfg[35]<<std::endl;
  }
  std::cout<<"----"<<std::endl;
}

 void Fei4::saveServiceRecord(uint8_t byte1, uint8_t byte2) {
   uint32_t code = ((0xFC & byte1)>>2);
   uint32_t value = (((0x03 & byte1)<<8) + ((0xFF & byte2)));
   m_serviceRecord[code] = value;
}
