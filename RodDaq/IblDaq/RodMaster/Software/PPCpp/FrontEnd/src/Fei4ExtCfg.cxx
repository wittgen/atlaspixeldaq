#include "Fei4ExtCfg.h"
#include "BitUtils.h"

Fei4ExtCfg::Fei4ExtCfg(): Fei4GlobalCfg(), m_chipID(8), m_moduleID(0), m_rxCh(16), m_txCh(16), m_revision(0),
                          m_enabled(true), m_vcal0(0.0), m_vcal1(0.0), m_cinjLow(0.0), m_cinjMed(0.0), m_cinjHigh(0.0),
                          m_serialNumber(0) {//todo change rx,tx defaults
#ifdef LL_DEBUG
  std::cout << "Setting default hard-coded broadcast value of chip id 8" << std::endl;
#endif
}

Fei4ExtCfg::Fei4ExtCfg(const Fei4ExtCfg& rhs): Fei4GlobalCfg(*(Fei4GlobalCfg*)&rhs){
  m_chipID = rhs.m_chipID;
  m_moduleID = rhs.m_moduleID;
  m_rxCh = rhs.m_rxCh;
  m_txCh = rhs.m_txCh;
  m_revision = rhs.m_revision;
  m_enabled = rhs.m_enabled;
  m_vcal0 = rhs.m_vcal0;
  m_vcal1 = rhs.m_vcal1;
  m_cinjLow = rhs.m_cinjLow;
  m_cinjMed = rhs.m_cinjMed;
  m_cinjHigh = rhs.m_cinjHigh;
  m_serialNumber = rhs.m_serialNumber;
}

Fei4ExtCfg& Fei4ExtCfg::operator=(const Fei4ExtCfg& rhs){
  Fei4GlobalCfg::operator=(rhs);
  m_chipID = rhs.m_chipID;
  m_moduleID = rhs.m_moduleID;
  m_rxCh = rhs.m_rxCh;
  m_txCh = rhs.m_txCh;
  m_revision = rhs.m_revision;
  m_enabled = rhs.m_enabled;
  m_vcal0 = rhs.m_vcal0;
  m_vcal1 = rhs.m_vcal1;
  m_cinjLow = rhs.m_cinjLow;
  m_cinjMed = rhs.m_cinjMed;
  m_cinjHigh = rhs.m_cinjHigh;
  m_serialNumber = rhs.m_serialNumber;
  return *this;
}

bool Fei4ExtCfg::operator==(const Fei4ExtCfg& rhs){
  return (  Fei4GlobalCfg::operator==(rhs) &
	  ( (*this).m_chipID   ==   rhs.m_chipID)   &
	  ( (*this).m_moduleID ==   rhs.m_moduleID) &
	  ( (*this).m_rxCh     ==   rhs.m_rxCh)     &
	  ( (*this).m_txCh     ==   rhs.m_txCh)     &
	  ( (*this).m_revision ==   rhs.m_revision) &
	  ( (*this).m_enabled  ==   rhs.m_enabled)
	    );
}

void Fei4ExtCfg::dump() const{
  Fei4GlobalCfg::dump();
  std::cout << "m_chipID: " << (int) m_chipID << std::endl; 
  std::cout << "m_moduleID: " << (int) m_moduleID << std::endl; 
  std::cout << "m_rxCh: " << (int) m_rxCh << std::endl; 
  std::cout << "m_txCh: " << (int) m_txCh << std::endl; 
  std::cout << "m_revision: " << (int) m_revision << std::endl; 
  std::cout << "m_enabled: " <<  (int) m_enabled << std::endl; 
  std::cout << "m_vcal0: " <<  m_vcal0 << std::endl; 
  std::cout << "m_vcal1: " <<  m_vcal1 << std::endl; 
  std::cout << "m_cinjLow: " << m_cinjLow << std::endl; 
  std::cout << "m_cinjMed: " << m_cinjMed << std::endl; 
  std::cout << "m_cinjHigh: " << m_cinjHigh << std::endl; 
  std::cout << "m_serialNumber: " << m_serialNumber << std::endl; 
}

uint32_t Fei4ExtCfg::getExtRegHash() const {
  uint32_t hash = 0x0;
  //all 8bit values
  hash ^= m_chipID << 24;
  hash ^= m_moduleID << 16;
  hash ^= m_rxCh << 8;
  hash ^= m_txCh;
  //16bit values
  hash ^= m_revision;
  //bool
  hash ^= static_cast<uint32_t>(m_enabled);
  //floats
  hash ^= float_bit_cast(m_vcal0);
  hash ^= float_bit_cast(m_vcal1);
  hash ^= float_bit_cast(m_cinjLow);
  hash ^= float_bit_cast(m_cinjMed);
  hash ^= float_bit_cast(m_cinjHigh);
  //hash ^= m_serialNumber; //The serial number is read from the module and can change
  return hash;
}
