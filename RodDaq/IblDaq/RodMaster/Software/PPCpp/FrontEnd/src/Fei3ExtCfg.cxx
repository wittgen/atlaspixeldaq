#include "Fei3ExtCfg.h"
#include "BitUtils.h"

Fei3ExtCfg::Fei3ExtCfg(): Fei3GlobalCfg(), m_address(0), m_cinjLow(0.0), m_cinjHigh(0.0),
                          m_vcalCoeff0(0.0), m_vcalCoeff1(0.0), m_vcalCoeff2(0.0), m_vcalCoeff3(0.0) {
}

Fei3ExtCfg::Fei3ExtCfg(const Fei3ExtCfg &rhs) : Fei3GlobalCfg(*(Fei3GlobalCfg*)&rhs) {
    m_address = rhs.m_address;
    m_cinjLow = rhs.m_cinjLow;
    m_cinjHigh = rhs.m_cinjHigh;
    m_vcalCoeff0 = rhs.m_vcalCoeff0;
    m_vcalCoeff1 = rhs.m_vcalCoeff1;
    m_vcalCoeff2 = rhs.m_vcalCoeff2;
    m_vcalCoeff3 = rhs.m_vcalCoeff3;
}

Fei3ExtCfg & Fei3ExtCfg::operator=(const Fei3ExtCfg &rhs) {
    Fei3GlobalCfg::operator=(rhs);
    m_address = rhs.m_address;
    m_cinjLow = rhs.m_cinjLow;
    m_cinjHigh = rhs.m_cinjHigh;
    m_vcalCoeff0 = rhs.m_vcalCoeff0;
    m_vcalCoeff1 = rhs.m_vcalCoeff1;
    m_vcalCoeff2 = rhs.m_vcalCoeff2;
    m_vcalCoeff3 = rhs.m_vcalCoeff3;
    return *this;
}

void Fei3ExtCfg::dump() const{
    std::cout << "m_address: " <<  m_address << std::endl; 
    std::cout << "m_cinjLow: " << m_cinjLow << std::endl; 
    std::cout << "m_cinjHigh: " << m_cinjHigh << std::endl; 
    std::cout << "m_vcalCoeff0: " <<  m_vcalCoeff0 << std::endl; 
    std::cout << "m_vcalCoeff1: " <<  m_vcalCoeff1 << std::endl; 
    std::cout << "m_vcalCoeff2: " <<  m_vcalCoeff2 << std::endl; 
    std::cout << "m_vcalCoeff3: " <<  m_vcalCoeff3 << std::endl; 
}

uint32_t Fei3ExtCfg::getExtRegHash() const {
    uint32_t hash = 0x0;
    // PixelFECommand
    hash ^= m_address; //32bit
    // PixelFECalib
    hash ^= float_bit_cast(m_cinjLow );
    hash ^= float_bit_cast(m_cinjHigh);
    hash ^= float_bit_cast(m_vcalCoeff0);
    hash ^= float_bit_cast(m_vcalCoeff1);
    hash ^= float_bit_cast(m_vcalCoeff2);
    hash ^= float_bit_cast(m_vcalCoeff3);
    //hash ^= m_cpEnableMask;
    return hash;
}
