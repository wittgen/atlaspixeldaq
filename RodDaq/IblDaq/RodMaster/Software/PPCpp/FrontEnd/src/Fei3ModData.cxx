#include "Fei3ModData.h"
#include "RxLink.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

std::vector<Fei3HitData> Fei3ModData::readHitData(RxLink &link, bool raw) {
    std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;

    std::vector<uint8_t> rawData;
    rawData.reserve(32);
    std::vector<Fei3HitData> answer;
    answer.reserve(10);

    uint8_t byte = 0;

    do {
        rawData.push_back(link.read());
        byte = rawData.back() & 0xFF;
        std::cout << "current byte: " << HEX(byte) << std::endl;
    } while (!((byte >> 3) == 0x1D) && rawData.size() < 10);

    if (rawData.size() == 10 && !((byte >> 3) == 0x1D)) {
        std::cout << "no header found, returning empty result" << std::endl;
        return answer;
    }

    std::cout << "found header 11101" << std::endl; // congrats!
    
    int k = rawData.size() - 1; // k is set to location of data header

    readRemainingRawHitData(link, rawData); // read remaining raw data from link

std::cout << "/////" << std::endl;
for (size_t l = 0; l < rawData.size(); l++) std::cout << "\t" << std::hex << (int) rawData[l] << std::dec;
std::cout << std::endl;
std::cout << "/////" << std::endl;

    Fei3HitData hd; // hit object struct
    hd.l1id = 0; hd.bcid = 0; hd.fe = 0; hd.row = 0; hd.col = 0; hd.tot = 0; hd.mccflag = 0; hd.feflag = 0;

    // read l1id
    uint8_t l1id = 0;
    l1id |= (byte & 0x7) << 5;
    k++; byte = rawData[k] & 0xFF;
    l1id |= (byte >> 3) & 0x1F;
    hd.l1id = l1id;

    // check sync bit
    if (!((byte >> 2) & 0x1)) {
        std::cout << "missed sync bit, returning empty result" << std::endl;
        return answer;
    }

    // read bcid
    uint8_t bcid = 0;
    bcid |= (byte & 0x3) << 6;
    k++; byte = rawData[k] & 0xFF;
    bcid |= (byte >> 2) & 0x3F;
    hd.bcid = bcid;
    
    // check sync bit
    if (!((byte >> 1) & 0x1)) {
        std::cout << "missed sync bit, returning empty result" << std::endl;
        return answer;
    }

    // loop over remaining data
    int offset = 1; // current offset after reading l1id & bcid
    uint8_t lick = 0, mask = 0;
    while (k < (int)(rawData.size() - 1)) {
        lick = mask = 0; // reset lick & mask
        for (int imask = 0; imask < offset; imask++) mask |= (0x1 << imask); // set mask based on offset
        lick |= (byte & mask) << (8 - offset); // set high lick bits
        k++; byte = rawData[k] & 0xFF;
        lick |= (byte & ~mask) >> offset; // set low lick bits
        if ((lick >> 3) == 0x1F) { // read mcc flag & fe flag
            uint8_t mccflag = 0, feflag = 0;
            offset = (offset + 3) % 8; // reset offset
            mask = 0; // reset mask
            for (int imask = 0; imask < offset; imask++) mask |= (0x1 << imask); // set mask based on offset
            mccflag |= (byte & mask) << (8 - offset);
            k++; byte = rawData[k] & 0xFF;
            mccflag |= (byte & ~mask) >> offset;
            feflag |= (byte & mask) << (8 - offset);
            k++; byte = rawData[k] & 0xFF;
            feflag |= (byte & ~mask) >> offset;
            hd.mccflag = mccflag;
            hd.feflag = feflag;
            answer.push_back(hd);
            offset = (offset == 0) ? 7 : offset - 1; // skip sync bit
        }
        else if ((lick >> 4) == 0xE) { // read new fe number
            hd.fe = lick & 0xF; // fe number only 4 LSBs
            offset = (offset == 0) ? 7 : offset - 1; // skip sync bit
        }
        else { // read hit
            uint8_t row = 0, col = 0, tot = 0;
            row = lick;
            if (offset >= 4) { // all of col is contained in current byte
                col |= (byte >> (offset - 4)) & 0x1F;
            }
            else {
                col |= (byte & mask) << (5 - offset);
                k++; byte = rawData[k] & 0xFF;
                col |= (byte & ~mask) >> (offset + 3);
            }
            offset = (offset + 3) % 8; // reset offset
            mask = 0; // reset mask
            for (int imask = 0; imask < offset; imask++) mask |= (0x1 << imask); // set mask based on offset
            tot |= (byte & mask) << (8 - offset);
            k++; byte = rawData[k] & 0xFF;
            tot |= (byte & ~mask) >> offset;
            hd.row = row;
            hd.col = col;
            hd.tot = tot;
            answer.push_back(hd);
            offset = (offset == 0) ? 7 : offset - 1; // skip sync bit
        }
    } 

    return answer;
}

void Fei3ModData::readRemainingRawHitData(RxLink &link, std::vector<uint8_t> &rawData) {
    int k = rawData.size() - 1;
    do {
        rawData.push_back(link.read());
        k++;
    } while (!(k >= 3 && (rawData[k] == 0 && rawData[k - 1] == 0 && rawData[k - 2] == 0)));
    while (rawData.back() == 0) rawData.pop_back();
}

void Fei3ModData::decodeRawHitData(const std::vector<Fei3HitData>& hd, std::ostream& os) {
    for (size_t k = 0; k < hd.size(); k++) {
        os << "LV1ID:\t" << hd[k].l1id;
        os << "\tBCID:\t" << hd[k].bcid;
        os << "\tFE:\t" << hd[k].fe;
        os << "\tRow:\t" << hd[k].row;
        os << "\tCol:\t" << hd[k].col;
        os << "\tToT:\t" << hd[k].tot;
        os << "\tMCC-Flag:\t" << hd[k].mccflag;
        os << "\tFE-Flag:\t" << hd[k].feflag;
        os << std::endl;
    }
}

