/*
  Steve Alkire <alkire@cern.ch> 2013.11.04
*/
#include <iostream>
#include "Fei4GlobalCfg.h"
#include "TxLink.h"
#include "BitUtils.h"

#define HEX(x) std::hex << "0x" << static_cast<int>(x) << std::dec

const unsigned Fei4GlobalCfg::numberOfRegisters;

const uint16_t Fei4GlobalCfg::defaultCfg[Fei4GlobalCfg::numberOfRegisters] = {
	0x0000, // 0x00 this one is important too!!
	0x0000, // 0x01
	0x1800, // 0x02
	0x0000, // 0x03
	0x0000, // 0x04
	0xD4FF, // 0x05
	0x7CD4, // 0x06
	0xFF58, // 0x07
	0xF258, // 0x08
	0x00AA, // 0x09
	0x4CB0, // 0x0A
	0x56D4, // 0x0B //*
	0x2800, // 0x0C
	0x0000, // 0x0D
	0xABB7, // 0x0E //* LJ changed
	0x1A96, // 0x0F
	0x0038, // 0x10
	0x00AB, // 0x11
	0x00FF, // 0x12
	0x6000, // 0x13 //*
	0xFFFF, // 0x14 //*
	0x05A4, // 0x15
	0x0000, // 0x16
	0x0000, // 0x17
	0x0000, // 0x18
	0xD200, // 0x19
	0x0002, // 0x1A
	0x8000, // 0x1B
	0x8206, // 0x1C
	0x0007, // 0x1D
	0x0000, // 0x1E
	0xF400, // 0x1F
	0x0000, // 0x20
	0x0000, // 0x21
	0x0000, // 0x22
	0x0000  // 0x23
};

Fei4GlobalCfg::Fei4GlobalCfg(){
  setFields();
  for(unsigned i  = 0; i < numberOfRegisters; ++i){
    cfg[i] = defaultCfg[i];
  }
}

Fei4GlobalCfg::Fei4GlobalCfg(const Fei4GlobalCfg& rhs){
  setFields();
  std::copy(rhs.cfg,rhs.cfg+numberOfRegisters,cfg);
}

Fei4GlobalCfg& Fei4GlobalCfg::operator=(const Fei4GlobalCfg& rhs){
    std::copy(rhs.cfg,rhs.cfg+numberOfRegisters,cfg);
    return *this;
}

bool Fei4GlobalCfg::operator==( const Fei4GlobalCfg& rhs){
  bool areEqual = true;
  for(unsigned i = 0 ; i < Fei4GlobalCfg::numberOfRegisters; i++){
    areEqual = areEqual & ( (*this).cfg[i] == rhs.cfg[i]);
  }
  return areEqual;
}



void Fei4GlobalCfg::setCfg(uint16_t thecfg[numberOfRegisters]){//right now just to return the correct cfg
  for(unsigned int i = 0 ; i < numberOfRegisters; i++){    
    cfg[i] = thecfg[i];
  }
}


void Fei4GlobalCfg::dump() const{
  for(unsigned int i = 0; i < numberOfRegisters; i++){
    std::cout << "field "<<i<<" has value: "<<HEX(cfg[i]) << std::endl;
  }

  std::cout << "SME: " << SME.valueEndian() << '\n';
  std::cout << "EventLimit: " << EventLimit.valueEndian() << '\n';
  std::cout << "Trig_Count: " << Trig_Count.valueEndian() << '\n';
  std::cout << "CAE: " << CAE.valueEndian() << '\n';
  std::cout << "ErrorMask_0: " << ErrorMask_0.valueEndian() << '\n';
  std::cout << "ErrorMask_1: " << ErrorMask_1.valueEndian() << '\n';
  std::cout << "PrmpVbp_R: " << PrmpVbp_R.valueEndian() << '\n';
  std::cout << "BufVgOpAmp: " << BufVgOpAmp.valueEndian() << '\n';
  std::cout << "GADCVref: " << GADCVref.valueEndian() << '\n';
  std::cout << "PrmpVbp: " << PrmpVbp.valueEndian() << '\n';
  std::cout << "TDACVbp: " << TDACVbp.valueEndian() << '\n';
  std::cout << "DisVbn: " << DisVbn.valueEndian() << '\n';
  std::cout << "Amp2Vbn: " << Amp2Vbn.valueEndian() << '\n';
  std::cout << "Amp2VbpFol: " << Amp2VbpFol.valueEndian() << '\n';
  std::cout << "Amp2Vbp: " << Amp2Vbp.valueEndian() << '\n';
  std::cout << "FDACVbn: " << FDACVbn.valueEndian() << '\n';
  std::cout << "Amp2Vbpff: " << Amp2Vbpff.valueEndian() << '\n';
  std::cout << "PrmpVbnFol: " << PrmpVbnFol.valueEndian() << '\n';
  std::cout << "PrmpVbp_L: " << PrmpVbp_L.valueEndian() << '\n';
  std::cout << "PrmpVbpf: " << PrmpVbpf.valueEndian() << '\n';
  std::cout << "PrmpVbnLCC: " << PrmpVbnLCC.valueEndian() << '\n';
  std::cout << "S1: " << S1.valueEndian() << '\n';
  std::cout << "S0: " << S0.valueEndian() << '\n';
  std::cout << "S (merged): " << S.valueEndian() << '\n';
  std::cout << "Pixel_latch_strobe: " << Pixel_latch_strobe.valueEndian() << '\n';
  std::cout << "LVDSDrvIref: " << LVDSDrvIref.valueEndian() << '\n';
  std::cout << "GADCCompBias: " << GADCCompBias.valueEndian() << '\n';
  std::cout << "PllIbias: " << PllIbias.valueEndian() << '\n';
  std::cout << "LVDSDrvVos: " << LVDSDrvVos.valueEndian() << '\n';
  std::cout << "TempSensIbias: " << TempSensIbias.valueEndian() << '\n';
  std::cout << "PllIcp: " << PllIcp.valueEndian() << '\n';
  std::cout << "PlsrIDACRamp: " << PlsrIDACRamp.valueEndian() << '\n';
  std::cout << "VrefDigTune: " << VrefDigTune.valueEndian() << '\n';
  std::cout << "PlsrVgOpAmp: " << PlsrVgOpAmp.valueEndian() << '\n';
  std::cout << "PlsrDACbias: " << PlsrDACbias.valueEndian() << '\n';
  std::cout << "VrefAnTune: " << VrefAnTune.valueEndian() << '\n';
  std::cout << "Vthin_Coarse: " << Vthin_Coarse.valueEndian() << '\n';
  std::cout << "Vthin_Fine: " << Vthin_Fine.valueEndian() << '\n';
  std::cout << "HLD: " << HLD.valueEndian() << '\n';
  std::cout << "DJO: " << DJO.valueEndian() << '\n';
  std::cout << "DHS: " << DHS.valueEndian() << '\n';
  std::cout << "PlsrDAC: " << PlsrDAC.valueEndian() << '\n';
  std::cout << "CP0: " << CP0.valueEndian() << '\n';
  std::cout << "CP1: " << CP1.valueEndian() << '\n';
  std::cout << "CP (merged): " << CP.valueEndian() << '\n';
  std::cout << "Colpr_Addr: " << Colpr_Addr.valueEndian() << '\n';
  std::cout << "DisableColCnfg0: " << DisableColCnfg0.valueEndian() << '\n';
  std::cout << "DisableColCnfg1: " << DisableColCnfg1.valueEndian() << '\n';
  std::cout << "Trig_Lat: " << Trig_Lat.valueEndian() << '\n';
  std::cout << "DisableColCnfg2: " << DisableColCnfg2.valueEndian() << '\n';
  std::cout << "CMDcnt12: " << CMDcnt12.valueEndian() << '\n';
  std::cout << "CalPulseWidth: " << CalPulseWidth.valueEndian() << '\n';
  std::cout << "CalPulseDelay: " << CalPulseDelay.valueEndian() << '\n';
  std::cout << "STC: " << STC.valueEndian() << '\n';
  std::cout << "HD1: " << HD1.valueEndian() << '\n';
  std::cout << "HD0: " << HD0.valueEndian() << '\n';
  std::cout << "HD (merged): " << HD.valueEndian() << '\n';
  std::cout << "PLL: " << PLL.valueEndian() << '\n';
  std::cout << "EFS: " << EFS.valueEndian() << '\n';
  std::cout << "STP: " << STP.valueEndian() << '\n';
  std::cout << "RER: " << RER.valueEndian() << '\n';
  std::cout << "ADC: " << ADC.valueEndian() << '\n';
  std::cout << "SRR: " << SRR.valueEndian() << '\n';
  std::cout << "HOR: " << HOR.valueEndian() << '\n';
  std::cout << "CAL: " << CAL.valueEndian() << '\n';
  std::cout << "SRC: " << SRC.valueEndian() << '\n';
  std::cout << "LEN: " << LEN.valueEndian() << '\n';
  std::cout << "SRK: " << SRK.valueEndian() << '\n';
  std::cout << "M13: " << M13.valueEndian() << '\n';
  std::cout << "LV0: " << LV0.valueEndian() << '\n';
  std::cout << "EN_40M: " << EN_40M.valueEndian() << '\n';
  std::cout << "EN_80M: " << EN_80M.valueEndian() << '\n';
  std::cout << "c10: " << c10.valueEndian() << '\n';
  std::cout << "c11: " << c11.valueEndian() << '\n';
  std::cout << "c12: " << c12.valueEndian() << '\n';
  std::cout << "clk1 (merged): " << clk1.valueEndian() << '\n';
  std::cout << "c00: " << c00.valueEndian() << '\n';
  std::cout << "c01: " << c01.valueEndian() << '\n';
  std::cout << "c02: " << c02.valueEndian() << '\n';
  std::cout << "clk0 (merged): " << clk0.valueEndian() << '\n';
  std::cout << "EN_160: " << EN_160.valueEndian() << '\n';
  std::cout << "EN_320: " << EN_320.valueEndian() << '\n';
  std::cout << "N8b: " << N8b.valueEndian() << '\n';
  std::cout << "c2o: " << c2o.valueEndian() << '\n';
  std::cout << "EmptyRecordCnfg: " << EmptyRecordCnfg.valueEndian() << '\n';
  std::cout << "LVE: " << LVE.valueEndian() << '\n';
  std::cout << "LV3: " << LV3.valueEndian() << '\n';
  std::cout << "LV1: " << LV1.valueEndian() << '\n';
  std::cout << "TM0: " << TM0.valueEndian() << '\n';
  std::cout << "TM1: " << TM1.valueEndian() << '\n';
  std::cout << "TM (merged): " << TM.valueEndian() << '\n';
  std::cout << "TMD: " << TMD.valueEndian() << '\n';
  std::cout << "ILR: " << ILR.valueEndian() << '\n';
  std::cout << "PlsrRiseUpTau: " << PlsrRiseUpTau.valueEndian() << '\n';
  std::cout << "PPW: " << PPW.valueEndian() << '\n';
  std::cout << "PlsrDelay: " << PlsrDelay.valueEndian() << '\n';
  std::cout << "XAC: " << XDC.valueEndian() << '\n';
  std::cout << "GADCSel: " << GADCSel.valueEndian() << '\n';
  std::cout << "SELB0: " << SELB0.valueEndian() << '\n';
  std::cout << "SELB1: " << SELB1.valueEndian() << '\n';
  std::cout << "SELB2: " << SELB2.valueEndian() << '\n';
  std::cout << "b7E: " << b7E.valueEndian() << '\n';
  std::cout << "EFUSE: " << EFUSE.valueEndian() << '\n';
}

void Fei4GlobalCfg::setFields(){
  //1
  SME.setField(cfg);
  EventLimit.setField(cfg);
  //2
  Trig_Count.setField(cfg);
  CAE.setField(cfg);
  //3
  ErrorMask_0.setField(cfg);
  //4
  ErrorMask_1.setField(cfg);
  //5
  PrmpVbp_R.setField(cfg);
  BufVgOpAmp.setField(cfg);
  GADCVref.setField(cfg);
  //6
  PrmpVbp.setField(cfg);
  //7
  TDACVbp.setField(cfg);
  DisVbn.setField(cfg);
  //8
  Amp2Vbn.setField(cfg);
  Amp2VbpFol.setField(cfg);
  //9
  Amp2Vbp.setField(cfg);
  //10
  FDACVbn.setField(cfg);
  Amp2Vbpff.setField(cfg);
  //11
  PrmpVbnFol.setField(cfg);
  PrmpVbp_L.setField(cfg);
  //12
  PrmpVbpf.setField(cfg);
  PrmpVbnLCC.setField(cfg);
  //13
  S1.setField(cfg);
  S0.setField(cfg);
  S.setField(cfg);
  Pixel_latch_strobe.setField(cfg);
  //14
  LVDSDrvIref.setField(cfg);
  GADCCompBias.setField(cfg);
  //15
  PllIbias.setField(cfg);
  LVDSDrvVos.setField(cfg);
  //16
  TempSensIbias.setField(cfg);
  PllIcp.setField(cfg);
  //17
  PlsrIDACRamp.setField(cfg);
  //18
  VrefDigTune.setField(cfg);
  PlsrVgOpAmp.setField(cfg);
  //19
  PlsrDACbias.setField(cfg);
  VrefAnTune.setField(cfg);
  //20
  Vthin_Coarse.setField(cfg);
  Vthin_Fine.setField(cfg);
  //21
  HLD.setField(cfg);
  DJO.setField(cfg);
  DHS.setField(cfg);
  PlsrDAC.setField(cfg);
  //22
  CP0.setField(cfg);
  CP1.setField(cfg);
  CP.setField(cfg);
  Colpr_Addr.setField(cfg);
  //23
  DisableColCnfg0.setField(cfg);
  //24
  DisableColCnfg1.setField(cfg);
  //25
  Trig_Lat.setField(cfg);
  DisableColCnfg2.setField(cfg);
  //26
  CMDcnt12.setField(cfg);
  CalPulseWidth.setField(cfg);
  CalPulseDelay.setField(cfg);
  STC.setField(cfg);
  HD1.setField(cfg);
  HD0.setField(cfg);
  HD.setField(cfg);
  //27
  PLL.setField(cfg);
  EFS.setField(cfg);
  STP.setField(cfg);
  RER.setField(cfg);
  ADC.setField(cfg);
  SRR.setField(cfg);
  HOR.setField(cfg);
  CAL.setField(cfg);
  SRC.setField(cfg);
  LEN.setField(cfg);
  SRK.setField(cfg);
  M13.setField(cfg);
  //28
  LV0.setField(cfg);
  EN_40M.setField(cfg);
  EN_80M.setField(cfg);
  c10.setField(cfg);
  c11.setField(cfg);
  c12.setField(cfg);
  clk1.setField(cfg);
  c00.setField(cfg);
  c01.setField(cfg);
  c02.setField(cfg);
  clk0.setField(cfg);
  EN_160.setField(cfg);
  EN_320.setField(cfg);
  //29
  N8b.setField(cfg);
  c2o.setField(cfg);
  EmptyRecordCnfg.setField(cfg);
  LVE.setField(cfg);
  LV3.setField(cfg);
  LV1.setField(cfg);
  //30
  TM0.setField(cfg);
  TM1.setField(cfg);
  TMD.setField(cfg);
  TM.setField(cfg);
  ILR.setField(cfg);
  //31
  PlsrRiseUpTau.setField(cfg);
  PPW.setField(cfg);
  PlsrDelay.setField(cfg);
  XDC.setField(cfg);
  XAC.setField(cfg);
  GADCSel.setField(cfg);
  //32
  SELB0.setField(cfg);
  //33
  SELB1.setField(cfg);
  //34
  SELB2.setField(cfg);
  b7E.setField(cfg);
  //35
  EFUSE.setField(cfg);  
}

void Fei4GlobalCfg::setTestingCfg() {//todo maybe remove
  //just increase each register by one for testing
  for(unsigned i =0; i < numberOfRegisters; i++){
    cfg[i] = defaultCfg[i]+1;
  }
}

void Fei4GlobalCfg::sendTestConfig(TxLink *txLink, uint8_t chipID) {

	//Todo: switch to Fei4 with constructor to use this link
	//Fei4 fe; // Has defaut config too
	//fe.send();
	//return;
	
	// All this is low level on purpose

	// Put chip in configuration mode
	txLink->write(0x005A2807|(chipID<<6)); txLink->write(0x00000000);
	dumpData(0x005A2807|(chipID<<6)); dumpData(0x00000000);

	// Sending GR#20 first to avoid oscillations
	txLink->write(0x005A0814|(chipID<<6)); txLink->write(Fei4GlobalCfg::defaultCfg[20]<<16);
	dumpData(0x005A0814|(chipID<<6)); dumpData(Fei4GlobalCfg::defaultCfg[20]<<16);


	for(uint8_t i = 0 ; i < Fei4GlobalCfg::numberOfRegisters ; ++i) {
		txLink->write((0x005A0800|(chipID<<6))|i); txLink->write(Fei4GlobalCfg::defaultCfg[i]<<16);
		dumpData((0x005A0A00|(chipID<<6))|i); dumpData(Fei4GlobalCfg::defaultCfg[i]<<16);
	}
	txLink->send();
}

uint32_t Fei4GlobalCfg::getGlobalRegHash() const {
  uint32_t hash = 0x0;
  //Since we're only dealing with 16bit numbers, but want a useful 32bit hash, we're
  //shifting every second value - if you comment out some values, that's not a problem
  //as long as you're not removing all the shifted or unshifted ones!
  //1
  hash ^= lowbias32(static_cast<uint32_t>(SME.value()) | (static_cast<uint32_t>(EventLimit.value()) << 16));
  //2
  //hash ^= Trig_Count.value(); skip because it's updated whenever data taking/clibration is run
  hash ^= lowbias32(static_cast<uint32_t>(CAE.value()));
  //3 & 4
  hash ^= lowbias32(static_cast<uint32_t>(ErrorMask_0.value()) | (static_cast<uint32_t>(ErrorMask_1.value()) << 16));
  //5
  hash ^= lowbias32(static_cast<uint32_t>(PrmpVbp_R.value()) | (static_cast<uint32_t>(BufVgOpAmp.value()) << 16));
  //hash ^= GADCVref.value(); (same registers as before, mapping both to one)
  //6
  hash ^= lowbias32(static_cast<uint32_t>(PrmpVbp.value()));
  //7
  hash ^= lowbias32(static_cast<uint32_t>(TDACVbp.value()) | (static_cast<uint32_t>(DisVbn.value()) << 16));
  //8
  hash ^= lowbias32(static_cast<uint32_t>(Amp2Vbn.value()) | (static_cast<uint32_t>(Amp2VbpFol.value()) << 16));
  //9
  hash ^= lowbias32(static_cast<uint32_t>(Amp2Vbp.value()));
  //10
  hash ^= lowbias32((static_cast<uint32_t>(FDACVbn.value()) << 16) | static_cast<uint32_t>(Amp2Vbpff.value()));
  //11
  hash ^= lowbias32((static_cast<uint32_t>(PrmpVbnFol.value()) << 16) | static_cast<uint32_t>(PrmpVbp_L.value()));
  //12
  hash ^= lowbias32((static_cast<uint32_t>(PrmpVbpf.value()) << 16) | PrmpVbnLCC.value());
  //13
  hash ^= lowbias32((static_cast<uint32_t>(S1.value()) << 16) | static_cast<uint32_t>(S0.value()));
  //hash ^= static_cast<uint32_t>(S.value()) << 16; combination of S1 and S0
  //hash ^= Pixel_latch_strobe.value(); skip as it dynamically changes when configuration is sent
  //14
  hash ^= lowbias32(static_cast<uint32_t>((LVDSDrvIref.value()) << 16) | static_cast<uint32_t>(GADCCompBias.value()));
  //15
  hash ^= lowbias32(static_cast<uint32_t>((PllIbias.value()) << 16) | static_cast<uint32_t>(LVDSDrvVos.value()));
  //16
  hash ^= lowbias32(static_cast<uint32_t>((TempSensIbias.value()) << 16) | static_cast<uint32_t>(PllIcp.value()));
  //17
  hash ^= lowbias32(static_cast<uint32_t>(PlsrIDACRamp.value()));
  //18
  hash ^= lowbias32(static_cast<uint32_t>(VrefDigTune.value()) | (static_cast<uint32_t>(PlsrVgOpAmp.value()) << 16));
  //19
  hash ^= lowbias32(static_cast<uint32_t>(PlsrDACbias.value()) | (static_cast<uint32_t>(VrefAnTune.value()) << 16));
  //20
  hash ^= lowbias32(static_cast<uint32_t>(Vthin_Coarse.value()) | (static_cast<uint32_t>(Vthin_Fine.value()) << 16));
  //21
  hash ^= lowbias32(static_cast<uint32_t>(HLD.value()) | (static_cast<uint32_t>(DJO.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(DHS.value()));
  //hash ^= static_cast<uint32_t>(PlsrDAC.value()) << 16; skip as changed during scans
  //22
  hash ^= lowbias32(static_cast<uint32_t>(CP0.value()) | (static_cast<uint32_t>(CP1.value()) << 16));
  //hash ^= CP.value(); combined CP0 and CP1
  //hash ^= static_cast<uint32_t>(Colpr_Addr.value()) << 16; skip as it changes with configuration
  //23 & 24
  hash ^= lowbias32(static_cast<uint32_t>(DisableColCnfg0.value()) | (static_cast<uint32_t>(DisableColCnfg1.value()) << 16));
  //25
  //hash ^= Trig_Lat.value();
  hash ^= lowbias32(static_cast<uint32_t>(DisableColCnfg2.value()));
  //26 & first 27 (PLL)
  hash ^= lowbias32(static_cast<uint32_t>(CMDcnt12.value()) | (static_cast<uint32_t>(CalPulseWidth.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(CalPulseDelay.value()) | (static_cast<uint32_t>(STC.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(HD1.value()) | (static_cast<uint32_t>(HD0.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(HD.value()) | (static_cast<uint32_t>(PLL.value()) << 16));
  //remaining 27 & first 28 (LV0)
  hash ^= lowbias32(static_cast<uint32_t>(EFS.value()) | (static_cast<uint32_t>(STP.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(RER.value()) | (static_cast<uint32_t>(ADC.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(SRR.value()) | (static_cast<uint32_t>(HOR.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(CAL.value()) | (static_cast<uint32_t>(SRC.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(LEN.value()) | (static_cast<uint32_t>(SRK.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(M13.value()) | (static_cast<uint32_t>(LV0.value()) << 16));
  //remaining 28
  hash ^= lowbias32(static_cast<uint32_t>(EN_40M.value()) | (static_cast<uint32_t>(EN_80M.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(c10.value()) | (static_cast<uint32_t>(c11.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(c12.value()) | (static_cast<uint32_t>(clk1.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(c00.value()) | (static_cast<uint32_t>(c01.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(c02.value()) | (static_cast<uint32_t>(clk0.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(EN_160.value()) | (static_cast<uint32_t>(EN_320.value()) << 16));
  //29
  hash ^= lowbias32(static_cast<uint32_t>(N8b.value()) | (static_cast<uint32_t>(c2o.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(EmptyRecordCnfg.value()) | (static_cast<uint32_t>(LVE.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(LV3.value()) | (static_cast<uint32_t>(LV1.value()) << 16));
  //30 & first 31 (PlsrRiseUpTau)
  hash ^= lowbias32(static_cast<uint32_t>(TM0.value()) | (static_cast<uint32_t>(TM1.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(TM.value()) | (static_cast<uint32_t>(TMD.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(ILR.value()) | (static_cast<uint32_t>(PlsrRiseUpTau.value()) << 16));
  //remaining 31 & 32 (SELB0) //skipping PlsrDelay as is a free configuration register
  hash ^= lowbias32(static_cast<uint32_t>(PPW.value()) /*| (static_cast<uint32_t>(PlsrDelay.value()) << 16)*/);
  hash ^= lowbias32(static_cast<uint32_t>(XDC.value()) | (static_cast<uint32_t>(XAC.value()) << 16));
  hash ^= lowbias32(static_cast<uint32_t>(GADCSel.value()) | (static_cast<uint32_t>(SELB0.value()) << 16));
  //33 & first 34
  hash ^= lowbias32(static_cast<uint32_t>(SELB1.value()) | (static_cast<uint32_t>(SELB2.value()) << 16));
  //second 34 and 35
  hash ^= lowbias32(static_cast<uint32_t>(b7E.value()) | (static_cast<uint32_t>(EFUSE.value()) << 16));
  return hash;
  }
