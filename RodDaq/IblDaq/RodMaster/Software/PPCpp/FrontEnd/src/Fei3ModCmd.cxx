#include "Fei3ModCmd.h"
#include <iomanip>

#include "SerialPort.h"
#include "RodResourceManager.h"

Fei3ModCmd::Fei3ModCmd() {
    setTxLink(RodResourceManagerIBL::getDevNullSerialPort());
    m_verbosity = 0;
    m_useFw5MHzMode = false;
}

Fei3ModCmd::Fei3ModCmd(TxLink *link) {
    m_TxLink = link;
    m_verbosity = 0;
    m_useFw5MHzMode = false;
}

void Fei3ModCmd::sendLV1() {
    m_TxLink->write(0x0001D000);
    m_TxLink->send();
}

void Fei3ModCmd::sendBCR() {
    m_TxLink->write(0x161);
    m_TxLink->send();
}

void Fei3ModCmd::sendECR() {
    m_TxLink->write(0x162);
    m_TxLink->send();
}

void Fei3ModCmd::sendCAL() {
    m_TxLink->write(0x164);
    m_TxLink->send();
}

void Fei3ModCmd::sendSYNC() {
    m_TxLink->write(0x168);
    m_TxLink->send();
}

void Fei3ModCmd::sendCALandLVL1(uint8_t delay, uint32_t triggers, bool doECR, bool doBCR) {
  if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << ": delay=" << (int)delay << ", triggers=" << triggers
                        << ", doECR=" << std::boolalpha << doECR << ", doBCR=" << doBCR << std::endl;
	for(uint32_t i=0;i<triggers;++i){
	  uint16_t nBits = delay; // Number of bits to write, will be decremented in this function
	  nBits -= 5; // delay is from end of CAL to end of L1
	  
	  //Add 32-bit 0 for the PPC serial port
	  m_TxLink->write(0x00000000);
	  // Preparing header
	  if(nBits > 16) {
      if(doECR) m_TxLink->write(0x00000162); // Send ECR
      if(doBCR) m_TxLink->write(0x00000161); // Send BCR
	    m_TxLink->write(0x01640000);
	    nBits -= 16;
	  } else {
	    uint32_t bitHelper = 0xe8000000 >> nBits;
	    m_TxLink->write(0x01640000|((bitHelper&0xFFFF0000)>>16));
	    m_TxLink->write(bitHelper<<16);
	    nBits = 0;
	  }

	  // Fill in chunks of 32-bit words
	  while( nBits > 32 ) { m_TxLink->write(0x00000000); nBits -= 32; }

	  // Fill in any remaining bits
	  if( nBits > 0 ) {
	    uint32_t bitHelper = 0xe8000000 >> nBits;
	    m_TxLink->write(bitHelper);
	    bitHelper = 0xe8000000 << (32-nBits);
	    m_TxLink->write(bitHelper);
	  }

	  // Sending the stream to the MCC
	  m_TxLink->send();
	}
}

void Fei3ModCmd::mccWrRegister(uint32_t address, uint32_t data, bool doSend) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << "addr=" << address << ", data=" << data << std::endl;
#ifdef LL_DEBUG
    std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "will send MCC this command: " << std::hex << (0x1) << ((0x6B << 24) + (0x0 << 20) + ((address & 0xF) << 16) + (data & 0xFFFF)) << std::endl;
#endif
    m_TxLink->write(0x0);
    m_TxLink->write(0x1);
    m_TxLink->write((0x6B << 24) + (0x0 << 20) + ((address & 0xF) << 16) + (data & 0xFFFF));
    m_TxLink->write(0x0);
    if(doSend) m_TxLink->send();
}

void Fei3ModCmd::mccRdRegister(uint32_t address) {
#ifdef LL_DEBUG
    std::cout << "in function: " << __PRETTY_FUNCTION__ << std::endl;
    std::cout << "will send MCC this command: " << std::hex << ((0x16B << 8) + (0x1 << 4) + (address & 0xF)) << std::endl;
#endif
    m_TxLink->write((0x16B << 8) + (0x1 << 4) + (address & 0xF));
    m_TxLink->send();
}

void Fei3ModCmd::mccWrFifo(uint32_t data, bool doSend) {
    m_TxLink->write((0x16B << 3) + (0x1));
    m_TxLink->write(/*(0x16B << 35) + (0x2 << 31) +*/ (0x0 << 27) + (data & 0x7FFFFFF));
    if(doSend) m_TxLink->send();
}

void Fei3ModCmd::mccRdFifo(uint32_t address) {
    m_TxLink->write((0x16B << 8) + (0x3 << 4) + (address & 0xF));
    m_TxLink->send();
}

void Fei3ModCmd::mccWrFrontEnd(const std::vector<uint32_t> &data, bool doSend) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << std::endl;
    m_TxLink->write((0x16B << 8) + (0x4 << 4));

    if( m_useFw5MHzMode ) {
      IblBocTxFifoL12 *btf = dynamic_cast<IblBocTxFifoL12*>(m_TxLink);
      if (!btf) return;
      for (size_t i = 0; i < data.size(); i++) {
        btf->write5MHz(data[i]);
      }
    } else {
      std::vector<uint32_t> data5MHz;
      data5MHz.reserve(data.size() * 8);
      convert40MHzTo5MHz(data, data5MHz);
      for (size_t i = 0; i < data5MHz.size(); i++) {
        m_TxLink->write(data5MHz[i]);
      }
    }
    if(doSend) m_TxLink->send();
}

void Fei3ModCmd::mccRdFrontEnd(const std::vector<uint32_t> &data, bool doSend) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << std::endl;
    m_TxLink->write((0x16B << 8) + (0x5 << 4));

    if( m_useFw5MHzMode ) {
      IblBocTxFifoL12 *btf = dynamic_cast<IblBocTxFifoL12*>(m_TxLink);
      if (!btf) return;
      for (size_t i = 0; i < data.size(); i++) {
        btf->write5MHz(data[i]);
      }
    } else {
      std::vector<uint32_t> data5MHz;
      data5MHz.reserve(data.size() * 8);
      convert40MHzTo5MHz(data, data5MHz);
      for (size_t i = 0; i < data5MHz.size(); i++) {
        m_TxLink->write(data5MHz[i]);
      }
    }

    if(doSend) m_TxLink->send();
}

void Fei3ModCmd::mccWrReceiver(const std::vector<uint32_t> &data, bool doSend) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << std::endl;
    m_TxLink->write(0x0);
    m_TxLink->write((0x16B << 8) + (0x6 << 4));
    for (size_t i = 0; i < data.size(); i++) {
        m_TxLink->write(data[i]);
    }
    m_TxLink->write(0x0);
    if(doSend) m_TxLink->send();
}

void Fei3ModCmd::mccEnDataTake() {
    m_TxLink->write((0x16B << 8) + (0x8 << 4));
    m_TxLink->send();
}

void Fei3ModCmd::mccGlobalResetMCC() {
    m_TxLink->write((0x16B << 8) + (0x9 << 4));
    m_TxLink->send();
}

void Fei3ModCmd::mccGlobalResetFE(uint32_t syncW) {
    if(m_verbosity > 10) std::cout << __PRETTY_FUNCTION__ << ": syncW=" << syncW << std::endl;
    m_TxLink->write((0x16B << 12) + (0xA << 8) + (0x0 << 4) + (syncW & 0xF));
    m_TxLink->send();
}
