#include "LogPublisher.h"
#include "RodMasterRegisters.h"
#include "IblRodSlave.h"
#include "LogPublisherCmd.h"
#include "LPPacket.h"
#include <thread>

// debugging
const bool DEBUGGING = 1;   // log additional information to debug system


std::string LogPublisher::modeToString(int mode) const
{
    switch(mode){
    case MODE_OFF: return "MODE_OFF";
    case MODE_ON:  return "MODE_ON";
    case MODE_KILL:  return "MODE_KILL";
    }
    return "UNKNOWN_MODE";
}

LogPublisher::LogPublisher():
        UDPClient() {

    pthread_mutex_init(&m_rwMutex, NULL);
    init();
}


void LogPublisher::init() {

    m_mode 		= MODE_OFF;
    m_sleep 		= 1000; 	// in ms
    m_time_standby_max = 200; // in sec (2 min on HOST ~ 1 min on PPC)
    m_log_switch = std::bitset<3>(std::string("100")); // only master by default
    m_slaveVerbose = 0;
    m_verbosity = 0;
    time(&time_last_packet_in);
}


LogPublisher* LogPublisher::getInstance() {

    static LogPublisher *instance = 0;
    if(!instance) {
        instance = new LogPublisher();
        instance-> run();
    }
    if(!instance)
    {
        std::cerr << "ERROR: couldn't create LogPublisher instance!" << std::endl;
    }

    return instance;
}


void LogPublisher::setMode(int mode) {
    pthread_mutex_lock(&m_rwMutex);
    m_mode = mode;
    pthread_mutex_unlock(&m_rwMutex);
    cv.notify_one();
}

int LogPublisher::getMode() {
    pthread_mutex_lock(&m_rwMutex);
    int mode = m_mode;
    pthread_mutex_unlock(&m_rwMutex);
    return mode;
}

void LogPublisher::setTimeMax(int x) {
    pthread_mutex_lock(&m_rwMutex);
    m_time_standby_max = x;
    pthread_mutex_unlock(&m_rwMutex);
}

int LogPublisher::getTimeMax() {
    pthread_mutex_lock(&m_rwMutex);
    int x = m_time_standby_max;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}

void LogPublisher::setLogSwitch(const std::bitset<3> &x) {
    pthread_mutex_lock(&m_rwMutex);
    m_log_switch = x;
    pthread_mutex_unlock(&m_rwMutex);
}

std::bitset<3> LogPublisher::getLogSwitch() {
    pthread_mutex_lock(&m_rwMutex);
    std::bitset<3> x = m_log_switch;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}


void LogPublisher::setSlaveA(bool x) {

    pthread_mutex_lock(&m_rwMutex);
    m_log_switch[1] = x;
    pthread_mutex_unlock(&m_rwMutex);
    IblRod::EpcLocalCtrlUartA::write(x);
}


void LogPublisher::setSlaveB(bool x) {

    pthread_mutex_lock(&m_rwMutex);
    m_log_switch[0] = x;
    pthread_mutex_unlock(&m_rwMutex);
    IblRod::EpcLocalCtrlUartB::write(x);
}


void LogPublisher::setSlaveVerbose(int x) {

    IblRodSlave::setVerbose(0, x); // Slave A
    IblRodSlave::setVerbose(1, x); // Slave B
    pthread_mutex_lock(&m_rwMutex);
    m_slaveVerbose = x;
    pthread_mutex_unlock(&m_rwMutex);
}

int LogPublisher::getSlaveVerbose() {

    pthread_mutex_lock(&m_rwMutex);
    int x = m_slaveVerbose;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}


void LogPublisher::setSleep(int x) {
    pthread_mutex_lock(&m_rwMutex);
    m_sleep = x;
    pthread_mutex_unlock(&m_rwMutex);
}

int LogPublisher::getSleep() {
    pthread_mutex_lock(&m_rwMutex);
    int x = m_sleep;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}

void LogPublisher::setVerbosity(int x) {
    pthread_mutex_lock(&m_rwMutex);
    m_verbosity = x;
    pthread_mutex_unlock(&m_rwMutex);
}

int LogPublisher::getVerbosity() {
    pthread_mutex_lock(&m_rwMutex);
    int x = m_verbosity;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}


void LogPublisher::setTimeLastPacketIn() {
    pthread_mutex_lock(&m_rwMutex);
    time(&time_last_packet_in);
    pthread_mutex_unlock(&m_rwMutex);
}

time_t LogPublisher::getTimeLastPacketIn() {
    pthread_mutex_lock(&m_rwMutex);
    int x = time_last_packet_in;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}


void LogPublisher::setConfig(const LPConfig & config) {

    setTimeLastPacketIn();

    if(m_verbosity>9) std::cout << "setConfig input: " << config.print() << "\n";

    if(config.mode		 	!=LPNoValue()) setMode	 (config.mode);
    if(config.receiver_ip	!=LPNoValue()) setIpAddress(config.receiver_ip.c_str());
    if(config.receiver_port	!=LPNoValue()) setPort	 (config.receiver_port);
    if(config.time_standby_max!=LPNoValue()) setTimeMax(config.time_standby_max);
    if(config.log_switch	!=LPNoValue())  setLogSwitch(config.log_switch);
    if(config.slave_verbose	!=LPNoValue())  setSlaveVerbose(config.slave_verbose);
    if(config.sleep			!=LPNoValue())  setSleep(config.sleep);
    if(config.verbosity		!=LPNoValue())  setVerbosity(config.verbosity);
}


void LogPublisher::getConfig(LPConfig &config) {

    config.mode		 	 = getMode();
    config.receiver_ip   = getIpAddressStr();
    config.receiver_port  = getPort	 ();
    config.time_standby_max = getTimeMax();
    config.log_switch 	 = getLogSwitch().to_ulong();
    config.slave_verbose = getSlaveVerbose();
    config.sleep 		    = getSleep();
    config.verbosity 	 = getVerbosity();


    if(m_verbosity>9) std::cout << "getConfig: " << config.print() << "\n";
}


bool LogPublisher::timeout() {

    time_t now;
    time(&now);
    if(difftime(now, getTimeLastPacketIn())>240) return true;
    else return false;
}


void LogPublisher::run() {

#ifdef __XMK__
    xil_printf("Starting LogPublisher daemon...\n");
#endif

    int tick = 0;

    time_t time_last_packet_out;
    time(&time_last_packet_out);

    int res = -1; 	// result for send_data
    //int sem_value;  // semafor value


    shared_ptr<LPPacket> packet = std::make_shared<LPPacket>();
    uint32_t maxMsgSize = packet->bodyMaxDynamicSize();

    shared_ptr<LPPacket> packetDBG = std::make_shared<LPPacket>();
    std::ostringstream str;

    std::unique_lock<std::mutex> lk(cv_m);

    int  mode;

    // actual work
    while(true) {

// disable stopping thread if no handshake from host        
//        if(timeout()){
//            std::cout << "Host handshake timeout " << std::endl;
//            setMode(MODE_OFF);
//        }


        // loop only if certain condition met
        cv.wait(lk,
              [this]() { return getMode() >= MODE_OFF; }
        );

        mode = getMode();

        if(mode == MODE_KILL) return;
        if(mode >= MODE_ON) {

            // check that client is configured. If not, dont read UART, logs would be lost
            if(!hasValidServerInfo()) {
#ifdef __XMK__
	      xil_printf("Invalid Server Info\n");
#endif
                setMode(MODE_OFF); continue;
            }

            // TODO the following 3 ifs should be abstracted by one function

            // Logging for master
            if(m_log_switch[2]) {

                uint32_t size = IblRod::PpcUartMasterStatusReg::read();
                if(size) {

                    packet-> body.clear();
                    for(uint32_t i = 0; i < std::min(size, maxMsgSize); i++) {
                        uint32_t value = IblRod::PpcUartMasterDataReg::read();
                        packet-> body +=  (char)(value&0xFF);
                    }

                    packet-> header.log_type = LOG_MASTER;
                    packet-> serialize();

                    if(DEBUGGING) {
                        packetDBG-> body.clear();

                        str.str(std::string()); 
                        str << "LogPublisher dbg: send " << packet-> serializedSize() << "B" << std::endl;
                        packetDBG-> body += str.str();
                        packetDBG-> header.log_type = LOG_MASTER;
                        packetDBG-> serialize();

                        // serialize
                        res = send_data(packetDBG-> buffer, packetDBG-> serializedSize());
                  
                        if(res >=0) time(&time_last_packet_out); 		// update time of last good packet
                    }



                    res = send_data(packet->buffer, packet-> serializedSize());
                    if(res >=0) time(&time_last_packet_out); 		// update time of last good packet

                }

            }


            if(m_log_switch[1]) {

                uint32_t size = IblRod::PpcUartS6AStatusReg::read();
                if(size) {

                    packet-> body.clear();
                    for(uint32_t i = 0; i < std::min(size, maxMsgSize); i++) {
                        uint32_t value = IblRod::PpcUartS6ADataReg::read();
                        packet-> body +=  (char)(value&0xFF);
                    }

                    packet-> header.log_type = LOG_SLAVEA;
                    packet-> serialize();
                    res = send_data(packet->buffer, packet-> serializedSize());
                    if(res >=0) time(&time_last_packet_out); 		// update time of last good packet
                }
            }

            if(m_log_switch[0]) {

                uint32_t size = IblRod::PpcUartS6BStatusReg::read();
                if(size) {

                    packet-> body.clear();
                    for(uint32_t i = 0; i < std::min(size, maxMsgSize); i++) {
                        uint32_t value = IblRod::PpcUartS6BDataReg::read();
                        packet-> body +=  (char)(value&0xFF);
                    }

                    packet-> header.log_type = LOG_SLAVEB;
                    packet-> serialize();
                    res = send_data(packet->buffer, packet-> serializedSize());
                    if(res >=0) time(&time_last_packet_out); 		// update time of last good packet
                }
            }


            // Send PING packet if no logs for more than m_time_standby_max seconds
            if(time(NULL)-time_last_packet_out > m_time_standby_max) {
                if(m_verbosity>9) std::cout << "PPC sends PING packet" << std::endl;

                packet-> header.log_type = LOG_PING;
                packet->body.clear();
                packet-> serialize();
                res = send_data(packet->buffer, packet-> serializedSize());
                if(res >=0) time(&time_last_packet_out); 		// update time of last good packet
            }


           // std::cout << "LogPublisher running. Tick = " << tick << std::endl;
        }

        std::this_thread::sleep_for(std::chrono::milliseconds(1));
        tick++;

    }

}
