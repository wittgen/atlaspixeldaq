#include "LPPacket.h"


// Specialization for LPHeader
void Serializer<LPHeader>::serialize(uint8_t* data, uint32_t& offset, const LPHeader& obj) {
    Serializer<uint8_t>::serialize(data, offset, obj.log_type);
}


LPHeader Serializer<LPHeader>::deserialize(const uint8_t* data, uint32_t& offset) {
    LPHeader obj;
    deserialize(data, offset, obj);
    return obj;
}


void Serializer<LPHeader>::deserialize(const uint8_t* data, uint32_t& offset, LPHeader& obj) {
    Serializer<uint8_t>::deserialize(data, offset, obj.log_type);
}


std::size_t Serializer<LPHeader>::size(const LPHeader& obj) {

    return sizeof(obj.log_type);
}


//-------------------------------------------------------------------------------------
LPHeader::LPHeader() :
   log_type(0) {

   }

LPHeader::LPHeader(const LPHeader & other) {
    (*this) = other;
}
