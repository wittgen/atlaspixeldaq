#include "UDPClient.h"
#include <string.h>
#include "Packet.h"


UDPClient::UDPClient(uint16_t _port_client):m_port_client(_port_client){
   pthread_mutex_init(&m_rwMutex, NULL);

   m_ipAddress 	= 0xFFFFFF; // user is expected to set the ip and port of the receiver
   m_port 		= 65535;


   if(!initializeSocket())
       std::cerr << "ERROR: couldn't initialize LogPublisher socket!" << std::endl;
}


bool UDPClient::initializeSocket() {

    pthread_mutex_lock(&m_rwMutex);

  // Communication via UDP
  socketId = socket(AF_INET,SOCK_DGRAM,0);

  if (socketId<0) {
    std::cerr << "ERROR LogReceiver: socket creation failed \n";
    return false;
  }

#ifdef __XMK__
  bzero(&server, sizeof(server));
#endif


  client.sin_family = AF_INET;
  client.sin_addr.s_addr=INADDR_ANY;
  client.sin_port=htons(m_port_client);   // automatic port assignment if PORT=0?

  //bind the socket

  if (bind(socketId, (struct sockaddr*) &client, sizeof(client))<0) {
      std::cerr << "Error UDPClient: socket bind failed\n";
      return false;
  }

  // debug, check the port

  struct sockaddr_in addr_tmp;
  uint32_t len = sizeof(addr_tmp);

  if(getsockname(socketId, (struct sockaddr *)&addr_tmp, &len) <0 ) {
      std::cerr << "Error LogReceiver: cannot retrieve sockname\n";
      return false;
  }

  std::cout << "Socket associated with IP: " << addr_tmp.sin_addr.s_addr
            << " and port: " <<  ntohs(addr_tmp.sin_port) << "\n";

  pthread_mutex_unlock(&m_rwMutex);

  return true;

}


/// sends data. Returns -1 if error, number of bytes if success
int UDPClient::send_data(uint8_t *sbuf,  uint32_t sendsize) {

    if(sendsize>1200) std::cout << "Packet size: " << sendsize << ". Problems can appear. Reduce size to 1200." << std::endl;

    uint32_t n = 0;

#ifdef __XMK__
    bzero(&server, sizeof(server));
#else
    memset(&server, 0, sizeof(server));
#endif
    server.sin_family 	   = AF_INET;
    server.sin_addr.s_addr = getIpAddress();
    server.sin_port		   = htons(getPort());

    n = sendto(socketId, sbuf, sendsize, 0, (struct sockaddr *)&server, sizeof(server));
    if(n<0) return -1;

    //std::cout << "Send packet with " << n << " bytes" << std::endl;

    return n;
}

bool UDPClient::hasValidServerInfo() {

    return  (getIpAddress() != 0xFFFFFF) && (getPort() != 65535);
}


void UDPClient::setIpAddress(uint32_t x) {
    pthread_mutex_lock(&m_rwMutex);
    m_ipAddress = x;
    pthread_mutex_unlock(&m_rwMutex);
}

void UDPClient::setIpAddress(const char* ip) { setIpAddress(inet_addr(ip)); }

uint32_t UDPClient::getIpAddress() {
    pthread_mutex_lock(&m_rwMutex);
    uint32_t x = m_ipAddress;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}

std::string UDPClient::getIpAddressStr() {
    struct sockaddr_in adr;
    adr.sin_addr.s_addr = getIpAddress();
    struct in_addr* act = &(adr.sin_addr); // explicity dereference type-punned pointer to keep compiler happy
    return inet_ntoa(*act);
}

void UDPClient::setPort(uint16_t x) {
    pthread_mutex_lock(&m_rwMutex);
    m_port = x;
    pthread_mutex_unlock(&m_rwMutex);
}

uint16_t UDPClient::getPort() {
    pthread_mutex_lock(&m_rwMutex);
    uint16_t x = m_port;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}

void UDPClient::setPortClient(uint16_t x) {
    pthread_mutex_lock(&m_rwMutex);
    m_port_client = x;
    pthread_mutex_unlock(&m_rwMutex);
}

uint16_t UDPClient::getPortClient() {
    pthread_mutex_lock(&m_rwMutex);
    uint16_t x = m_port_client;
    pthread_mutex_unlock(&m_rwMutex);
    return x;
}

