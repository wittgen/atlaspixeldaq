#ifndef LPPACKET_H
#define LPPACKET_H

#include "Packet.h"
#include "Serializer.h"

class LPHeader {

public:
    LPHeader();
    LPHeader(const LPHeader &);
    inline LPHeader & operator=(const LPHeader & right) {
        this->log_type = right.log_type;
        return *this;
    }

    uint8_t log_type;

};

//---------------------------------------------------------------------------

typedef Packet<LPHeader, std::string> LPPacket;


template < >
class Serializer<LPHeader> {
public:
  static void serialize(uint8_t* data, uint32_t& offset, const LPHeader& obj);
  static LPHeader deserialize(const uint8_t* data, uint32_t& offset);
  static void deserialize(const uint8_t* data, uint32_t& offset, LPHeader& obj);
  static std::size_t size(const LPHeader& obj);
};




#endif // LPPACKET_H
