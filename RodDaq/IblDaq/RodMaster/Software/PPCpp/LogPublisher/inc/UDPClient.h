#ifndef UDPCLIENT_H
#define UDPCLIENT_H

#include "CppCompatibility.h"

#include <arpa/inet.h>
#include <netinet/in.h>

#include <pthread.h>
#include <iosfwd>




class UDPClient {

public:

    UDPClient(uint16_t _port_client = 0);   // 0 = automatic port assignment

    bool initializeSocket();

    void setIpAddress(uint32_t x);
    void setIpAddress(const char* ip);
    uint32_t getIpAddress();
    std::string getIpAddressStr();

    void setPort(uint16_t x);
    uint16_t getPort();

    void setPortClient(uint16_t x);
    uint16_t getPortClient();


    void getSocketID(char *);

    bool hasValidServerInfo();	/// Check if user assigned meaningful host IP and Port where to send logs

   int send_data(uint8_t * sbuf, uint32_t sendsize);
   int send_data(std::string sbuf, uint32_t msg_end);
   int send_data(std::stringstream &ss);

protected:
   struct sockaddr_in server;   // socket of the server on the Host size
   struct sockaddr_in client; 	// socket of the PPC to send data
   int socketId;



private:

    // networking information of the server, where packets are sent
    uint32_t m_ipAddress;
    uint16_t m_port;

    // information on this client
    uint16_t m_port_client;


   pthread_mutex_t m_rwMutex;
};

#endif // UDPCLIENT_H
