#ifndef PACKET_H
#define PACKET_H

#include "CppCompatibility.h"

#include <string>
//#include <memory>
#include "Serializer.h"


// Structure to hold received packets

#define MAX_UDPPACKET_SIZE 1200

template<class Header, class Body>
class Packet : Serializable {

public:

 //   typedef std::shared_ptr<Packet> shared_ptr;

    Packet() : buffer_n(0){ }
  //MW: this can't work, gcc disagrees with the standard
  //can't initliaze an C++ array from a std::strring
  //it's never used anyway
  //Packet(std::string _data):
  //     buffer(_data) {
  //  }
    Packet(const Header &_header, const Body & _body):
            header(_header),
            body(_body) { }

    Header get_header() {
        uint32_t offset = 0;
        Serializer<Header>::deserialize(buffer, offset, header);
        return header;
    }

    virtual void serialize() {
        return serialize(buffer);
    }

    virtual void serialize(uint8_t *out) const {

        // check size
        if(serializedSize() > MAX_UDPPACKET_SIZE) {
            std::cerr << "Enlarge Packet buffer ["  << MAX_UDPPACKET_SIZE
                      << "]. Attemted to serialize " << serializedSize() << " bytes."
                      << std::endl;
        }

        uint32_t offset = 0;
        Serializer<Header>	::serialize(out, offset, header);
        Serializer<Body>	::serialize(out, offset, body);
    }

    void deserialize() {
        deserialize(buffer);
    }

    void deserialize(const uint8_t *in) {

        // clear objects (important for variable lenght types such as vectors)
        header = Header();
        body = Body();

        uint32_t offset = 0;
        Serializer<Header>::deserialize(in, offset, header);
        Serializer<Body>  ::deserialize(in, offset, body);
    }

    virtual uint32_t serializedSize() const {
        return Serializer<Header>::size(header)+Serializer<Body>::size(body);
    }

    // Note: call once, might be memory inneficient method for big structres. Needs to instantiate (empty) header and object structures
    /// Maximum allowed size for body object
    virtual uint32_t bodyMaxDynamicSize() const {
        int bsize = MAX_UDPPACKET_SIZE -  Serializer<Header>::size(Header()) - Serializer<Body>::size(Body());
        return (bsize < 0) ? 0 : bsize;
    }

    bool fits_buffer() const {
        return (serializedSize() <= MAX_UDPPACKET_SIZE) ? true : false;
    }


    uint8_t buffer[MAX_UDPPACKET_SIZE];
    uint32_t buffer_n;

    Header header;
    Body   body;
};



#endif // PACKET_H
