#ifndef LOGPUBLISHER_H
#define LOGPUBLISHER_H

#include "pthread.h"
#include "stdint.h"
#include "iosfwd"
#include <time.h>
#include <bitset>
#include <mutex>
#include <condition_variable>



#include "UDPClient.h"

struct LPConfig;


class LogPublisher : public UDPClient {

 public:
    enum Mode { MODE_OFF, MODE_ON, MODE_KILL=1000 };
    enum LogType { LOG_MASTER, LOG_SLAVEA, LOG_SLAVEB, LOG_PING, LOG_N };

    LogPublisher();

    void init();
    void run();
    static LogPublisher* getInstance();

    void setMode(int mode);
    void setTimeMax(int);
    void setLogSwitch(const std::bitset<3> &);
    void setSlaveVerbose(int x);	/// Enable Verbose mode for (both) Slaves
    void setSleep(int sleep);
    void setVerbosity(int verbosity);
    void setTimeLastPacketIn();

    int  getMode();
    int  getTimeMax();
    std::bitset<3> getLogSwitch() ;
    int  getSlaveVerbose();			/// Get Verbosity for Slaves
    int  getSleep();
    int  getVerbosity();
    time_t getTimeLastPacketIn();

    void setSlaveA(bool x); 		/// Enable/Disable filling UART for Slave A
    void setSlaveB(bool x); 		/// Enable/Disable filling UART for Slave B

    void setConfig(const LPConfig & config);	/// Set configuration
    void getConfig(LPConfig & config);  		/// Read configuration


 protected:
    std::string modeToString(int) const;  /// Make state string

 private:
    bool timeout();				 /// Timeout if no active handshake with Host

    int m_mode;  		 	 /// \sa State of LogPublisher LogPublisher::Mode
    int m_sleep; 				 /// Cycle in ms
    int m_verbosity;			 /// Message verbosity
    int m_time_standby_max;		 /// Send ping to Host if no log during this time (sec)
    std::bitset<3> m_log_switch; /// Configuration of the logs. Only Master(100), Only Slave A(010), Only Slave B(001). Combination allowed.
    int m_slaveVerbose;			 /// Verbosity of slave logs

    time_t time_last_packet_in;  /// time of last incomming setup

    pthread_mutex_t m_rwMutex;

    std::mutex cv_m;			// wake/sleep the thread
    std::condition_variable cv;
};

#endif // LOGPUBLISHER_H


/*! \class LogPublisher
 * Application running on PPC exportaing UART of Master or Slaves (optional)
 */

/*! \enum LogPublisher::Mode
  * Modes of the LogPublisher state
  */
