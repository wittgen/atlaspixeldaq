// IBL ROD rodMaster address and bit definitions
#ifndef ROD_MASTER_H
#define ROD_MASTER_H
// This is a generated file, do not edit!
// #define PPC_VHDL_COMPILE_DATE "2015-05-07T14:38:48+0200"
// Design parameters
// The following register addresses sit on top of the EPC area, defined in xparameters.h
#ifndef XPAR_XPS_EPC_0_PRH0_BASEADDR
#include "xparameters.h"
#endif
// Number of EPC address bits is an alternative way to identify the address range
#define PPC_EPC_ADDRESS_BITS 25
// Design identification
#define PPC_HDL_FPGA_HAS_GIT_HASH 1
#define PPC_HDL_FPGA_GIT_HASH "09C9D6CC6B1F5E5D71FFF995AD832BA2C041E3DE"
#define PPC_DESIGN_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000000)
#define MASTER_GITID_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000070)
// PPC control register
#define PPC_CTL_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000004)
// PPC watchdog. enabled with first write access if enable masks set. tomeout approx 0.3s
#define PPC_WDOG_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000014)
#define PPC_WDOG_ENABLE_MASK0 (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000018)
#define PPC_WDOG_ENABLE_MASK1 (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x100001C)
#define PPC_WDOG_ENABLE_VAL0 0x12345678
#define PPC_WDOG_ENABLE_VAL1 0xDEADFACE
#define PPC_WDOG_TIMEOUT 0x1000000  // 25 ns units
// Master: PPC is master if 1, else DSP is master
#define PPC_CTL_MASTER_BIT 31
// HPI enable: HPI port is activated if 1
#define PPC_CTL_HPIENABLE_BIT 30
// Serial port local link: local link implementation selected if 1, else EDK peripheral version (Bologna)
#define PPC_CTL_SPLOCLINK_BIT 29
// Invert serial port clock if 1
#define PPC_CTL_SPCLKINV_BIT 28
#define PPC_CTL_UARTA_BIT 27
#define PPC_CTL_UARTB_BIT 26
// PPC status register
#define PPC_STAT_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000008)
// PPC rod busy registers
#define PPC_BUSY_MASK_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000010)
#define PPC_BUSY_MASTER_STATUS_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000020)
#define PPC_BUSY_MASTER_BUSYNINP0_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000024)
#define PPC_BUSY_MASTER_BUSYNINP1_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000028)
#define PPC_BUSY_MASTER_FORMATTER0_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x100002C)
#define PPC_BUSY_MASTER_FORMATTER1_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000040)
#define PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000044)
#define PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000048)
#define PPC_BUSY_MASTER_OUT_HIST_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x100004C)
// PPC UART registers
#define PPC_UART_S6A_STATUS_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000050)
#define PPC_UART_S6A_DATA_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000054)
#define PPC_UART_S6B_STATUS_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000058)
#define PPC_UART_S6B_DATA_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x100005C)
#define PPC_UART_MASTER_STATUS_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000060)
#define PPC_UART_MASTER_DATA_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000064)
#define PPC_TIMER64_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x1000068)
// Simulation mode if 1
#define PPC_STAT_SIMULATION_BIT 31
// Serial port mask. Bit 0..7 enable output from local link serial port to SP6A, Bits 8..15 to SP6B. Output goes to XC signals
#define PPC_SPORT_REG (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x100000C)
// Common PPC+DSP registers
// Base of common (PPC+DSP) control registers, aka RCF registers
// TODO: check if addressing scheme / location has changed
#define COMMON_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x80000)
// Base of common (PPC+DSP) slave-A registers, aka formatter 0
#define SLV_A_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x0)
// Base of common (PPC+DSP) slave-B registers, aka formatter 4
#define SLV_B_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x40000)
// Base of common (PPC+DSP) BOC registers
#define BOC_REG_BASE (XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x200000)
#endif //ROD_MASTER_H
