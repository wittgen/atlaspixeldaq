PIXELDAQ_ROOT=$(shell echo $(CURDIR) | sed 's/RodDaq.*//g')
IBLDAQ_BASEDIR = $(PIXELDAQ_ROOT)
RODMASTERFW_BASEDIR = $(IBLDAQ_BASEDIR)/RodDaq/IblDaq/RodMaster/Firmware

include $(dir $(RODMASTERFW_BASEDIR))/xilconf.mk

# use bash
SHELL := /bin/bash -O extglob

# save time for certain targets
TIME = /usr/bin/time --output=$@.time
#TIME = 

# multithreading settings for XPS synthesis, MAP, and PAR
# MAP currently (14.2) supports only off or 2; PAR off or 2, 3, 4; XPS yes/no
MAP_MT = 2
PAR_MT = 4
XPS_MT = yes

SYNTHESIS_OPT = 
TRANSLATE_OPT =	-dd _ngo -nt timestamp
MAP_OPT = -w -logic_opt on -ol high -t 1 -register_duplication on -r 4 -global_opt off -mt $(MAP_MT) -cm speed -ir off -pr b -lc off -power off
PAR_OPT = -ol high -xe c -mt $(PAR_MT)
TIMING_OPT = -v 3 -s 1 -n 3 -fastpaths

# directory structure as seen from toplevel Makefile
CORES_DIR = cores
SYSTEM_DIR = system
SOURCE_DIR = src
BUILD_DIR = build
LOGS_DIR = logs
SOFTWARE_DIR = sw

# logfiles that are copied to $(LOGS_DIR)
LOGFILES = $(CORES_DIR)/coregen_all.log \
		$(SYSTEM_DIR)/platgen.log \
		$(BUILD_DIR)/rodMaster.syr \
		$(BUILD_DIR)/rodMaster.bld \
		$(BUILD_DIR)/rodMaster_map.mrp \
		$(BUILD_DIR)/rodMaster.par \
		$(BUILD_DIR)/rodMaster.pad \
		$(BUILD_DIR)/rodMaster.twr \
		$(BUILD_DIR)/rodMaster.bgn 
	
# FPGA settings
MASTER_FPGA_FAMILY = virtex5
MASTER_FPGA_DEVICE = xc5vfx70t
MASTER_FPGA_SPEED = -1
MASTER_FPGA_PACKAGE = ff1136

# Toolchain settings
INTSTYLE = xflow


#########################
# DO NOT EDIT BELOW!!!  #
#########################

MASTER_FPGA_PART = $(MASTER_FPGA_DEVICE)$(MASTER_FPGA_SPEED)-$(MASTER_FPGA_PACKAGE)

# processor architecture
ARCH = $(shell uname -p)

# check for 32/64-bit architecture
# and source settings.sh
ifeq ($(ARCH), x86_64)
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin64
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin64
else
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin
endif

# command line tools
XST = $(ISE_BIN_PATH)/xst
COREGEN = $(ISE_BIN_PATH)/coregen
NGDBUILD = $(ISE_BIN_PATH)/ngdbuild
MAP = $(ISE_BIN_PATH)/map
PAR = $(ISE_BIN_PATH)/par
BITGEN = $(ISE_BIN_PATH)/bitgen
IMPACT = $(ISE_BIN_PATH)/impact
PROMGEN = $(ISE_BIN_PATH)/promgen
PLATGEN = $(EDK_BIN_PATH)/platgen
XPS = $(EDK_BIN_PATH)/xps
DATA2MEM = $(ISE_BIN_PATH)/data2mem
TRCE = $(ISE_BIN_PATH)/trce
