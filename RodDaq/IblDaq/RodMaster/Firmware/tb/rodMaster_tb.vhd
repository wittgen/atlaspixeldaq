--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:31:21 07/16/2012
-- Design Name:   
-- Module Name:   /home/kugel/daten/work/projekte/atlas/iblBoc/vhdl/rod/RODcode/trunk/rodMasterPpc/rodMaster_tb.vhd
-- Project Name:  rodMaster
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rodMaster
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY rodMaster_tb IS
END rodMaster_tb;
 
ARCHITECTURE behavior OF rodMaster_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rodMaster
	generic (simulation : boolean := false);
    PORT(
         fpga_0_RS232_RX_pin : IN  std_logic;
         fpga_0_RS232_TX_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : IN  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : OUT  std_logic_vector(7 downto 0);
         fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : IN  std_logic_vector(7 downto 0);
         fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : IN  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : IN  std_logic;
         fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : IN  std_logic;
         fpga_0_Hard_Ethernet_MAC_MDC_0_pin : OUT  std_logic;
         fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : INOUT  std_logic;
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ : INOUT  std_logic_vector(63 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS : INOUT  std_logic_vector(7 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N : INOUT  std_logic_vector(7 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A : OUT  std_logic_vector(13 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA : OUT  std_logic_vector(2 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N : OUT  std_logic;
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N : OUT  std_logic;
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N : OUT  std_logic;
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N : OUT  std_logic_vector(1 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE : OUT  std_logic_vector(1 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT : OUT  std_logic;
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM : OUT  std_logic_vector(7 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK : OUT  std_logic_vector(1 downto 0);
         fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N : OUT  std_logic_vector(1 downto 0);
         fpga_0_LEDS_GPIO_IO_O_pin : OUT  std_logic_vector(7 downto 0);
         xc_a : OUT  std_logic_vector(7 downto 0);
         xc_b : OUT  std_logic_vector(7 downto 0);
         master_reset : IN  std_logic;
         reset_s6a : OUT  std_logic;
         reset_s6b : OUT  std_logic;
         controller_bus_data_inout : INOUT  std_logic_vector(31 downto 0);
         controller_bus_adr_in : IN  std_logic_vector(19 downto 0);
         controller_bus_be_n_in : IN  std_logic_vector(3 downto 0);
         controller_bus_ce_n_in : IN  std_logic;
         controller_bus_oe_n_in : IN  std_logic;
         controller_bus_re_n_in : IN  std_logic;
         controller_bus_rnw_in : IN  std_logic;
         controller_bus_ack_out : OUT  std_logic;
         controller_tinp_out : OUT  std_logic_vector(1 downto 0);
         controller_tout_in : IN  std_logic_vector(1 downto 0);
         controller_dx_in : IN  std_logic_vector(1 downto 0);
         controller_clkx_out : OUT  std_logic_vector(1 downto 0);
         controller_fsx_out : OUT  std_logic_vector(1 downto 0);
         controller_dr_out : OUT  std_logic_vector(1 downto 0);
         controller_clkr_in : IN  std_logic_vector(1 downto 0);
         controller_fsr_in : IN  std_logic_vector(1 downto 0);
         controller_ext_int_n_out : OUT  std_logic_vector(4 downto 0);
         ext_iacknum_in : IN  std_logic_vector(4 downto 0);
         mdsp_int_n_in : IN  std_logic;
         clock_to_fei4A : OUT  std_logic;
         serial_to_fei4A : OUT  std_logic;
         clock_to_fei4B : OUT  std_logic;
         serial_to_fei4B : OUT  std_logic;
			uartFromS6a : in std_logic;
			uartToS6a : out std_logic;
			uartFromS6b : in std_logic;
			uartToS6b : out std_logic;
			flash_si : out std_logic;
			flash_cs_n : out std_logic;
			flash_sck : out std_logic;
			flash_so : in std_logic;
			flash_reset_n : out std_logic;
			flash_wp_n : out std_logic;
			prm_ctrl_out : OUT std_logic_vector(19 downto 0); 
			prm_data_io : INOUT std_logic_vector(31 downto 0); 
			prm_strobe : IN std_logic_vector(1 downto 0); 
			ctrl_from_prm : IN std_logic_vector(14 downto 0);
			ddr2_scl : INOUT std_logic;
			ddr2_sda : INOUT std_logic;
         rod_bus_data : INOUT  std_logic_vector(15 downto 0);
         rod_bus_addr : OUT  std_logic_vector(15 downto 0);
         rod_bus_ce1_s6 : OUT  std_logic;
         rod_bus_ce2_s6 : OUT  std_logic;
         rod_bus_rnw : OUT  std_logic;
         form_hwob : OUT  std_logic;
         form_ds : OUT  std_logic;
         ttc_n : IN  std_logic_vector(7 downto 0);
         tim_clock_ok : IN  std_logic;
         v5_busy_out : OUT  std_logic;
         bocbus_strb_out : OUT  std_logic;
         bocbus_rodoe_out : OUT  std_logic;
         bocbus_write_out : OUT  std_logic;
         bocbus_busy_in : IN  std_logic;
         clk40_p : IN  std_logic;
         clk40_n : IN  std_logic;
         clk100_p : IN  std_logic;
         clk100_n : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal fpga_0_RS232_RX_pin : std_logic := '0';
   signal fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : std_logic := '0';
   signal fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : std_logic_vector(7 downto 0) := (others => '0');
   signal fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : std_logic := '0';
   signal fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : std_logic := '0';
   signal fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : std_logic := '0';
   signal master_reset : std_logic := '0';
   signal controller_bus_adr_in : std_logic_vector(19 downto 0) := (others => '0');
   signal controller_bus_be_n_in : std_logic_vector(3 downto 0) := (others => '0');
   signal controller_bus_ce_n_in : std_logic := '0';
   signal controller_bus_oe_n_in : std_logic := '0';
   signal controller_bus_re_n_in : std_logic := '0';
   signal controller_bus_rnw_in : std_logic := '0';
   signal controller_tout_in : std_logic_vector(1 downto 0) := (others => '0');
   signal controller_dx_in : std_logic_vector(1 downto 0) := (others => '0');
   signal controller_clkr_in : std_logic_vector(1 downto 0) := (others => '0');
   signal controller_fsr_in : std_logic_vector(1 downto 0) := (others => '0');
   signal ext_iacknum_in : std_logic_vector(4 downto 0) := (others => '0');
   signal mdsp_int_n_in : std_logic := '0';
   signal ttc_n : std_logic_vector(7 downto 0) := (others => '0');
   signal tim_clock_ok : std_logic := '0';
   signal bocbus_busy_in : std_logic := '0';
   signal clk40_p : std_logic := '0';
   signal clk40_n : std_logic := '0';
   signal clk100_p : std_logic := '0';
   signal clk100_n : std_logic := '0';

	--BiDirs
   signal fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ : std_logic_vector(63 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS : std_logic_vector(7 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N : std_logic_vector(7 downto 0);
   signal controller_bus_data_inout : std_logic_vector(31 downto 0);
   signal rod_bus_data : std_logic_vector(15 downto 0);

 	--Outputs
   signal fpga_0_RS232_TX_pin : std_logic;
   signal fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : std_logic;
   signal fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : std_logic_vector(7 downto 0);
   signal fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : std_logic;
   signal fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : std_logic;
   signal fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : std_logic;
   signal fpga_0_Hard_Ethernet_MAC_MDC_0_pin : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A : std_logic_vector(13 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA : std_logic_vector(2 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N : std_logic_vector(1 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE : std_logic_vector(1 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT : std_logic;
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM : std_logic_vector(7 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK : std_logic_vector(1 downto 0);
   signal fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N : std_logic_vector(1 downto 0);
   signal fpga_0_LEDS_GPIO_IO_O_pin : std_logic_vector(7 downto 0);
   signal sport0data : std_logic_vector(7 downto 0);
   signal sport1data : std_logic_vector(7 downto 0);
   signal reset_s6a : std_logic;
   signal reset_s6b : std_logic;
   signal controller_bus_ack_out : std_logic;
   signal controller_tinp_out : std_logic_vector(1 downto 0);
   signal controller_clkx_out : std_logic_vector(1 downto 0);
   signal controller_fsx_out : std_logic_vector(1 downto 0);
   signal controller_dr_out : std_logic_vector(1 downto 0);
   signal controller_ext_int_n_out : std_logic_vector(4 downto 0);
   signal clock_to_fei4A : std_logic;
   signal serial_to_fei4A : std_logic;
   signal clock_to_fei4B : std_logic;
   signal serial_to_fei4B : std_logic;
   signal rod_bus_addr : std_logic_vector(15 downto 0);
   signal rod_bus_ce1_s6 : std_logic;
   signal rod_bus_ce2_s6 : std_logic;
   signal rod_bus_rnw : std_logic;
   signal form_hwob : std_logic;
   signal form_ds : std_logic;
   signal v5_busy_out : std_logic;
   signal bocbus_strb_out : std_logic;
   signal bocbus_rodoe_out : std_logic;
   signal bocbus_write_out : std_logic;


   signal flash_si :  std_logic;
   signal flash_cs_n :  std_logic;
   signal flash_sck :  std_logic;
   signal flash_so :  std_logic;
   signal flash_reset_n :  std_logic;
   signal flash_wp_n :  std_logic;

   signal prm_ctrl_out :  std_logic_vector(19 downto 0); 
   signal prm_data_io :  std_logic_vector(31 downto 0); 
   signal prm_strobe :  std_logic_vector(1 downto 0); 
   signal ctrl_from_prm :  std_logic_vector(14 downto 0);
	------------------------------------------------------
   signal ddr2_scl :  std_logic;
   signal ddr2_sda :  std_logic;

   signal	uartFromS6a : std_logic := '0';
   signal	uartToS6a : std_logic;
   signal	uartFromS6b : std_logic;
   signal	uartToS6b : std_logic := '0';
	------------------------------------------------------

component bocEmu
	port
	(
		-- clocking and reset
		clk : in std_logic := '0';
		reset : in std_logic := '0';
		
		-- rod bus slave
		sb_strobe_n : in std_logic := '1';
		sb_write_n : in std_logic := '0';
		sb_busy : out std_logic := '0';
		sb_addr : in std_logic_vector(15 downto 0);
		sb_data : inout std_logic_vector(7 downto 0) := (others => 'Z')
		
	);
end component;


   -- Clock period definitions
   -- Clock period definitions
   constant clk100_p_period : time := 10 ns;
   constant clk40_p_period : time := 25 ns;

BEGIN

	-- boc emu
	theBoc: bocEmu
	port map(
		clk => clk40_p,
		reset => master_reset,
		
		sb_strobe_n => bocbus_strb_out,
		sb_write_n => rod_bus_rnw, -- bocbus_write_out,
		sb_busy => bocbus_busy_in,
		sb_addr => rod_bus_addr(15 downto 0),
		sb_data => rod_bus_data(7 downto 0)
	);
	 
	
	-- Instantiate the Unit Under Test (UUT)
   uut: rodMaster 
		generic map (simulation => true)
		PORT MAP (
          fpga_0_RS232_RX_pin => fpga_0_RS232_RX_pin,
          fpga_0_RS232_TX_pin => fpga_0_RS232_TX_pin,
          fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin => fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin,
          fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin,
          fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin,
          fpga_0_Hard_Ethernet_MAC_MDC_0_pin => fpga_0_Hard_Ethernet_MAC_MDC_0_pin,
          fpga_0_Hard_Ethernet_MAC_MDIO_0_pin => fpga_0_Hard_Ethernet_MAC_MDIO_0_pin,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK,
          fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N,
          fpga_0_LEDS_GPIO_IO_O_pin => fpga_0_LEDS_GPIO_IO_O_pin,
          xc_a => sport0data,
          xc_b => sport1data,
          master_reset => master_reset,
          reset_s6a => reset_s6a,
          reset_s6b => reset_s6b,
          controller_bus_data_inout => controller_bus_data_inout,
          controller_bus_adr_in => controller_bus_adr_in,
          controller_bus_be_n_in => controller_bus_be_n_in,
          controller_bus_ce_n_in => controller_bus_ce_n_in,
          controller_bus_oe_n_in => controller_bus_oe_n_in,
          controller_bus_re_n_in => controller_bus_re_n_in,
          controller_bus_rnw_in => controller_bus_rnw_in,
          controller_bus_ack_out => controller_bus_ack_out,
          controller_tinp_out => controller_tinp_out,
          controller_tout_in => controller_tout_in,
          controller_dx_in => controller_dx_in,
          controller_clkx_out => controller_clkx_out,
          controller_fsx_out => controller_fsx_out,
          controller_dr_out => controller_dr_out,
          controller_clkr_in => controller_clkr_in,
          controller_fsr_in => controller_fsr_in,
          controller_ext_int_n_out => controller_ext_int_n_out,
          ext_iacknum_in => ext_iacknum_in,
          mdsp_int_n_in => mdsp_int_n_in,
          clock_to_fei4A => clock_to_fei4A,
          serial_to_fei4A => serial_to_fei4A,
          clock_to_fei4B => clock_to_fei4B,
          serial_to_fei4B => serial_to_fei4B,
			 uartFromS6a =>uartFromS6a,
			 uartToS6a =>uartToS6a,
			 uartFromS6b =>uartFromS6b,
			 uartToS6b =>uartToS6b,
			flash_si => flash_si,
			flash_cs_n => flash_cs_n,
			flash_sck => flash_sck,
			flash_so => flash_so,
			flash_reset_n => flash_reset_n,
			flash_wp_n => flash_wp_n,
			prm_ctrl_out => prm_ctrl_out,
			prm_data_io => prm_data_io, 
			prm_strobe => prm_strobe, 		
			ctrl_from_prm => ctrl_from_prm,
			ddr2_sda => ddr2_sda,
			ddr2_scl => ddr2_scl,
          rod_bus_data => rod_bus_data,
          rod_bus_addr => rod_bus_addr,
          rod_bus_ce1_s6 => rod_bus_ce1_s6,
          rod_bus_ce2_s6 => rod_bus_ce2_s6,
          rod_bus_rnw => rod_bus_rnw,
          form_hwob => form_hwob,
          form_ds => form_ds,
          ttc_n => ttc_n,
          tim_clock_ok => tim_clock_ok,
          v5_busy_out => v5_busy_out,
          bocbus_strb_out => bocbus_strb_out,
          bocbus_rodoe_out => bocbus_rodoe_out,
          bocbus_write_out => bocbus_write_out,
          bocbus_busy_in => bocbus_busy_in,
          clk40_p => clk40_p,
          clk40_n => clk40_n,
          clk100_p => clk100_p,
          clk100_n => clk100_n
        );

   -- Clock process definitions
   -- Clock process definitions
   clk100_p_process :process
   begin
		clk100_p <= '0';
		wait for clk100_p_period/2;
		clk100_p <= '1';
		wait for clk100_p_period/2;
   end process;
	
	clk100_n <= not clk100_p;

   clk40_p_process :process
   begin
		clk40_p <= '0';
		wait for clk40_p_period/2;
		clk40_p <= '1';
		wait for clk40_p_period/2;
   end process;
	
	clk40_n <= not clk40_p;
 

   -- Stimulus process
   stim_proc: process
   begin		
		master_reset <= '1';
      -- hold reset state for 100 ns.
      wait for 100 ns;	
		master_reset <= '0';

      wait for clk100_p_period*20;

      -- insert stimulus here 

      wait;
   end process;


END;
