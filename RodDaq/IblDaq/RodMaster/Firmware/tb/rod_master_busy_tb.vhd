--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:09:52 03/28/2014
-- Design Name:   
-- Module Name:   E:/IblDaq/Bing/ibl_rodMaster/rod_master_busy_tb.vhd
-- Project Name:  ibl_rodMaster
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rod_master_busy
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY rod_master_busy_tb IS
END rod_master_busy_tb;
 
ARCHITECTURE behavior OF rod_master_busy_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rod_master_busy
    PORT(
         clk40in : IN  std_logic;
         rod_busy_n_in_p : IN  std_logic_vector(1 downto 0);
         efb_header_pause_i : IN  std_logic;
         formatter_mb_fef_n_in_p : IN  std_logic_vector(1 downto 0);
         spare_ctrl_reg_i : IN  std_logic;
         rol_test_pause_i : IN  std_logic;
			reset : IN  std_logic;
			reset_hists : IN  std_logic;
			busy_mask : IN std_logic_vector (7 downto 0);
         rod_busy_n_out_p : OUT  std_logic;
         current_busy_status : OUT  std_logic_vector(7 downto 0);
         rod_busy_n_in_p_0_hist : OUT  std_logic_vector(15 downto 0);
         rod_busy_n_in_p_1_hist : OUT  std_logic_vector(15 downto 0);
         formatter_mb_fef_n_in_p_0_hist : OUT  std_logic_vector(15 downto 0);
         formatter_mb_fef_n_in_p_1_hist : OUT  std_logic_vector(15 downto 0);
         efb_header_pause_i_hist : OUT  std_logic_vector(15 downto 0);
         --spare_ctrl_reg_i_hist : OUT  std_logic_vector(15 downto 0);
         rol_test_pause_i_hist : OUT  std_logic_vector(15 downto 0);
         rod_busy_n_out_p_hist : OUT  std_logic_vector(15 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk40in : std_logic := '0';
   signal rod_busy_n_in_p : std_logic_vector(1 downto 0) := (others => '0');
   signal efb_header_pause_i : std_logic := '0';
   signal formatter_mb_fef_n_in_p : std_logic_vector(1 downto 0) := (others => '0');
   signal spare_ctrl_reg_i : std_logic := '0';
   signal rol_test_pause_i : std_logic := '0';
   signal reset : std_logic := '0';
	signal reset_hists : std_logic := '0';
	signal busy_mask : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
   signal rod_busy_n_out_p : std_logic;
   signal current_busy_status : std_logic_vector(7 downto 0);
   signal rod_busy_n_in_p_0_hist : std_logic_vector(15 downto 0);
   signal rod_busy_n_in_p_1_hist : std_logic_vector(15 downto 0);
   signal formatter_mb_fef_n_in_p_0_hist : std_logic_vector(15 downto 0);
   signal formatter_mb_fef_n_in_p_1_hist : std_logic_vector(15 downto 0);
   signal efb_header_pause_i_0_hist : std_logic_vector(15 downto 0);
   --signal spare_ctrl_reg_i_hist : std_logic_vector(15 downto 0);
   signal rol_test_pause_i_hist : std_logic_vector(15 downto 0);
   signal rod_busy_n_out_p_hist : std_logic_vector(15 downto 0);

   -- Clock period definitions
   constant clk40in_period : time := 25 ns;
    
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: rod_master_busy PORT MAP (
          clk40in => clk40in,
          rod_busy_n_in_p => rod_busy_n_in_p,
          efb_header_pause_i => efb_header_pause_i,
          formatter_mb_fef_n_in_p => formatter_mb_fef_n_in_p,
          spare_ctrl_reg_i => spare_ctrl_reg_i,
          rol_test_pause_i => rol_test_pause_i,
          rod_busy_n_out_p => rod_busy_n_out_p,
          reset => reset,
			 reset_hists => reset_hists,
			 busy_mask => busy_mask,
          current_busy_status => current_busy_status,
          rod_busy_n_in_p_0_hist => rod_busy_n_in_p_0_hist,
          rod_busy_n_in_p_1_hist => rod_busy_n_in_p_1_hist,
          formatter_mb_fef_n_in_p_0_hist => formatter_mb_fef_n_in_p_0_hist,
          formatter_mb_fef_n_in_p_1_hist => formatter_mb_fef_n_in_p_1_hist,
          efb_header_pause_i_hist => efb_header_pause_i_0_hist,
--          spare_ctrl_reg_i_hist => spare_ctrl_reg_i_hist,
          rol_test_pause_i_hist => rol_test_pause_i_hist,
          rod_busy_n_out_p_hist => rod_busy_n_out_p_hist
        );

   -- Clock process definitions
   clk40in_process :process
   begin
		clk40in <= '0';
		wait for clk40in_period/2;
		clk40in <= '1';
		wait for clk40in_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns
		reset <= '1';
		reset_hists <= '0';
		rod_busy_n_in_p(0) <= '1';
		rod_busy_n_in_p(1) <= '1';
      efb_header_pause_i <= '0';
      formatter_mb_fef_n_in_p(0) <= '1';
		formatter_mb_fef_n_in_p(1) <= '1';
      spare_ctrl_reg_i <= '0';
      rol_test_pause_i <= '0';
      reset <= '1';
		busy_mask <= "00000000";
		-- insert stimulus here 
      wait for 100 ns;	
		reset <= '0';
		busy_mask(2) <= '1';
		busy_mask(1) <= '1';
		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(1) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(1) <= '1';

      wait for 100 ns;	
		busy_mask(2) <= '0';
		busy_mask(1) <= '0';
		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(1) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(1) <= '1';

      wait for 100 ns;	
		busy_mask(3) <= '1';	
		
		wait for 432 ns;
		efb_header_pause_i <= '1';
		wait for 100 ns;
		efb_header_pause_i <= '0';
	
		wait for 100 ns;	
		busy_mask(3) <= '0';	
	
		wait for 432 ns;
		efb_header_pause_i <= '1';
		wait for 100 ns;
		efb_header_pause_i <= '0';
	
		wait for 100 ns;	
		busy_mask(4) <= '1';	

		wait for 432 ns;
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(0) <= '1';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(0) <= '1';

		wait for 100 ns;	
		busy_mask(4) <= '0';	

		wait for 432 ns;
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(0) <= '1';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(0) <= '1';

		
		wait for 100 ns;	
		busy_mask(5) <= '1';	
		
		wait for 432 ns;
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;		
		spare_ctrl_reg_i <= '0';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 100 ns;	
		busy_mask(5) <= '0';	
		
		wait for 432 ns;
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;		
		spare_ctrl_reg_i <= '0';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 100 ns;	
		busy_mask(6) <= '1';	
		
		wait for 432 ns;
		rol_test_pause_i <= '1';
		wait for 100 ns;
		rol_test_pause_i <= '0';
		
		wait for 100 ns;	
		busy_mask(6) <= '0';	
		
		wait for 432 ns;
		rol_test_pause_i <= '1';
		wait for 100 ns;
		rol_test_pause_i <= '0';

		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';		





      wait for 1000 ns;	
		reset_hists	<= '1';
		wait for 100 ns;
		reset_hists <= '0';
		
		wait for 100 ns;
		busy_mask(2) <= '1';
		busy_mask(1) <= '1';
		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(1) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(1) <= '1';

      wait for 100 ns;	
		busy_mask(2) <= '0';
		busy_mask(1) <= '0';
		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(1) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(1) <= '1';

      wait for 100 ns;	
		busy_mask(3) <= '1';	
		
		wait for 432 ns;
		efb_header_pause_i <= '1';
		wait for 100 ns;
		efb_header_pause_i <= '0';
	
		wait for 100 ns;	
		busy_mask(3) <= '0';	
	
		wait for 432 ns;
		efb_header_pause_i <= '1';
		wait for 100 ns;
		efb_header_pause_i <= '0';
	
		wait for 100 ns;	
		busy_mask(4) <= '1';	

		wait for 432 ns;
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(0) <= '1';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(0) <= '1';

		wait for 100 ns;	
		busy_mask(4) <= '0';	

		wait for 432 ns;
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(0) <= '1';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(0) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(0) <= '1';

		
		wait for 100 ns;	
		busy_mask(5) <= '1';	
		
		wait for 432 ns;
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;		
		spare_ctrl_reg_i <= '0';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 100 ns;	
		busy_mask(5) <= '0';	
		
		wait for 432 ns;
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		wait for 100 ns;		
		spare_ctrl_reg_i <= '0';
		
		wait for 200 ns;
		spare_ctrl_reg_i <= '1';
		formatter_mb_fef_n_in_p(1) <= '0';
		wait for 100 ns;
		spare_ctrl_reg_i <= '0';
		formatter_mb_fef_n_in_p(1) <= '1';

		wait for 100 ns;	
		busy_mask(6) <= '1';	
		
		wait for 432 ns;
		rol_test_pause_i <= '1';
		wait for 100 ns;
		rol_test_pause_i <= '0';
		
		wait for 100 ns;	
		busy_mask(6) <= '0';	
		
		wait for 432 ns;
		rol_test_pause_i <= '1';
		wait for 100 ns;
		rol_test_pause_i <= '0';

		
		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';

		wait for 432 ns;
		rod_busy_n_in_p(0) <= '0';
		wait for 100 ns;
		rod_busy_n_in_p(0) <= '1';		





















		
		
		wait for 1432 ns;
		reset <= '1';
		wait for 100 ns;
		reset <= '0';		
		
		
		
		
      wait for clk40in_period*10;

      

      wait;
   end process;

END;
