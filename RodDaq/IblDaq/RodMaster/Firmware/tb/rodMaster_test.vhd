-------------------------------------------------------------------------------
-- rodMaster.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity rodMaster is
  generic (simulation : boolean := false);
  port (
    fpga_0_RS232_RX_pin : in std_logic;
    fpga_0_RS232_TX_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : out std_logic_vector(7 downto 0);
    fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : in std_logic_vector(7 downto 0);
    fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_MDC_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : inout std_logic;
    fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin : out std_logic;
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ : inout std_logic_vector(63 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS : inout std_logic_vector(7 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N : inout std_logic_vector(7 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A : out std_logic_vector(12 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA : out std_logic_vector(1 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N : out std_logic;
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N : out std_logic;
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N : out std_logic;
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N : out std_logic_vector(1 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT : out std_logic;
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE : out std_logic_vector(1 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM : out std_logic_vector(7 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK : out std_logic_vector(1 downto 0);
	fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N : out std_logic_vector(1 downto 0);
    fpga_0_LEDS_GPIO_IO_O_pin : out std_logic_vector(7 downto 0);
	 ---
	busAd : INOUT  std_logic_vector(31 downto 0);
	busAds : OUT  std_logic;
	busCsA : OUT  std_logic;
	busCsB : OUT  std_logic;
	busCsBoc : OUT  std_logic;
	busRnw : OUT  std_logic;
	--busAack : INOUT  std_logic;
	busAack : IN  std_logic;
	busDackA : IN  std_logic;
	busDackB : IN  std_logic;
	busDackBoc : IN  std_logic;
	-- serial port
	sport0data: out std_logic;
	sport0clk: out std_logic;
	--
	clk100_p: in std_logic;
	clk100_n: in std_logic
  );
end rodMaster;

architecture STRUCTURE of rodMaster is

  component v5gmac125 is
    port (
      fpga_0_RS232_RX_pin : in std_logic;
      fpga_0_RS232_TX_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : in std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : out std_logic_vector(7 downto 0);
      fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : in std_logic_vector(7 downto 0);
      fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : in std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : in std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : in std_logic;
      fpga_0_Hard_Ethernet_MAC_MDC_0_pin : out std_logic;
      fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : inout std_logic;
      fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin : out std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ_pin : inout std_logic_vector(63 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_pin : inout std_logic_vector(7 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N_pin : inout std_logic_vector(7 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A_pin : out std_logic_vector(12 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA_pin : out std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N_pin : out std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N_pin : out std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N_pin : out std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N_pin : out std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT_pin : out std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE_pin : out std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM_pin : out std_logic_vector(7 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_pin : out std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N_pin : out std_logic_vector(1 downto 0);
      fpga_0_clk_1_sys_clk_pin : in std_logic;
      fpga_0_rst_1_sys_rst_pin : in std_logic;
      cpuclk : out std_logic;
      cpurst : out std_logic;
      clk40aux : out std_logic;
		xps_ll_fifo_0_llink_rst_pin : out std_logic;
      xps_ll_fifo_0_tx_llink_src_rdy_n_pin : out std_logic;
      xps_ll_fifo_0_tx_llink_dest_rdy_n_pin : in std_logic;
      xps_ll_fifo_0_tx_llink_din_pin : out std_logic_vector(31 downto 0);
      xps_ll_fifo_0_tx_llink_eop_n_pin : out std_logic;
      xps_ll_fifo_0_tx_llink_sop_n_pin : out std_logic;
      xps_epc_0_PRH_CS_n_pin : out std_logic;
      xps_epc_0_PRH_Addr_pin : out std_logic_vector(24 downto 0);
      xps_epc_0_PRH_RNW_pin : out std_logic;
      xps_epc_0_PRH_Rdy_pin : in std_logic;
      xps_epc_0_PRH_Data_I_pin : in std_logic_vector(31 downto 0);
      xps_epc_0_PRH_Data_O_pin : out std_logic_vector(31 downto 0)
    );
  end component;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of v5gmac125 : component is "user_black_box";

  constant gndBits: std_logic_vector(31 downto 0):= (others => '0');


  signal fpga_0_clk_1_sys_clk_pin : std_logic;
  signal fpga_0_rst_1_sys_rst_pin : std_logic := '0';

  signal cpuclk :  std_logic;
  signal cpurst :  std_logic;
  signal sportReset: std_logic;

  signal clk40aux :  std_logic;
  signal xps_ll_fifo_0_tx_llink_src_rdy_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_dest_rdy_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_din_pin :  std_logic_vector(31 downto 0);
  signal xps_ll_fifo_0_tx_llink_eop_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_sop_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_packet :  std_logic;
  
  signal xps_epc_0_PRH_CS_n_pin :  std_logic;
  signal xps_epc_0_PRH_Addr_pin :  std_logic_vector(24 downto 0);
  signal xps_epc_0_PRH_RNW_pin :  std_logic;
  signal xps_epc_0_PRH_Rdy_pin :  std_logic;
  signal xps_epc_0_PRH_Data_I_pin :  std_logic_vector(31 downto 0);
  signal xps_epc_0_PRH_Data_O_pin :  std_logic_vector(31 downto 0);

  signal ctlReg: std_logic_vector(31 downto 0);
  signal statReg: std_logic_vector(31 downto 0);
  signal ledReg: std_logic_vector(31 downto 0);
  
  -- use 2 address bits to decode internal and external registers
  constant locSlv:  std_logic_vector(1 downto 0) := "00";	-- local registers
  constant sp6Aslv: std_logic_vector(1 downto 0) := "01";	-- fpga a
  constant sp6Bslv: std_logic_vector(1 downto 0) := "10";	-- fpga b
  constant bocSlv:  std_logic_vector(1 downto 0) := "11";	-- boc

  -- and the remaining bits as register addresses
  constant ctlAddr: std_logic_vector(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) := gndBits(xps_epc_0_PRH_Addr_pin'length - 3 downto 8) & X"00";
  constant statAddr: std_logic_vector(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) := gndBits(xps_epc_0_PRH_Addr_pin'length - 3 downto 8) & X"04";
  constant ledAddr: std_logic_vector(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) := gndBits(xps_epc_0_PRH_Addr_pin'length - 3 downto 8) & X"08";

	----------------------------------
 COMPONENT asyncBusMasterMx
 PORT(
		localClk : IN  std_logic;
		rst : IN  std_logic;
		busAd : INOUT  std_logic_vector(31 downto 0);
		busAds : OUT  std_logic;
		busCs : OUT  std_logic;
		busRnw : OUT  std_logic;
		busAack : IN  std_logic;
		busDack : IN  std_logic;
		addr : IN  std_logic_vector(31 downto 0);
		dout : OUT  std_logic_vector(31 downto 0);
		din : IN  std_logic_vector(31 downto 0);
		rdy : OUT  std_logic;
		cs_n : IN  std_logic;
		rnw : IN  std_logic
	  );
 END COMPONENT;

   signal addr : std_logic_vector(31 downto 0) := (others => '0');
   signal din : std_logic_vector(31 downto 0) := (others => '0');
   signal cs_n : std_logic := '1';
   signal rnw : std_logic := '0';

   signal dout : std_logic_vector(31 downto 0);
   signal rdy : std_logic;

	signal mstData: std_logic_vector(31 downto 0);
	signal mstRdy, locRdy: std_logic;
  
  signal dack: std_logic;
  signal slvSelect: std_logic;
  signal mstSelect_n: std_logic;
  signal locSelect: std_logic;
  

	----------------------------
	COMPONENT sportFifo
	  PORT (
		 rst : IN STD_LOGIC;
		 wr_clk : IN STD_LOGIC;
		 rd_clk : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC
	  );
	END COMPONENT;
  
	signal wrSport, rdSport, sportFull, sportEmpty: std_logic;
   signal sportData, sportReg : std_logic_vector(31 downto 0);
	signal sportBits: integer range 0 to 32;


begin

  clkBuf: ibufgds port map (i => clk100_p, ib => clk100_n, o => fpga_0_clk_1_sys_clk_pin);

  v5gmac125_i : v5gmac125
    port map (
      fpga_0_RS232_RX_pin => fpga_0_RS232_RX_pin,
      fpga_0_RS232_TX_pin => fpga_0_RS232_TX_pin,
      fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin => fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin,
      fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin,
      fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin,
      fpga_0_Hard_Ethernet_MAC_MDC_0_pin => fpga_0_Hard_Ethernet_MAC_MDC_0_pin,
      fpga_0_Hard_Ethernet_MAC_MDIO_0_pin => fpga_0_Hard_Ethernet_MAC_MDIO_0_pin,
      fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin => fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK,
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N,
      fpga_0_clk_1_sys_clk_pin => fpga_0_clk_1_sys_clk_pin,
      fpga_0_rst_1_sys_rst_pin => fpga_0_rst_1_sys_rst_pin,
      cpuclk => cpuclk,
      cpurst => cpurst,
		xps_ll_fifo_0_llink_rst_pin => sportReset,
      clk40aux => clk40aux,
      xps_ll_fifo_0_tx_llink_src_rdy_n_pin => xps_ll_fifo_0_tx_llink_src_rdy_n_pin,
      xps_ll_fifo_0_tx_llink_dest_rdy_n_pin => xps_ll_fifo_0_tx_llink_dest_rdy_n_pin,
      xps_ll_fifo_0_tx_llink_din_pin => xps_ll_fifo_0_tx_llink_din_pin,
      xps_ll_fifo_0_tx_llink_eop_n_pin => xps_ll_fifo_0_tx_llink_eop_n_pin,
      xps_ll_fifo_0_tx_llink_sop_n_pin => xps_ll_fifo_0_tx_llink_sop_n_pin,
      xps_epc_0_PRH_CS_n_pin => xps_epc_0_PRH_CS_n_pin,
      xps_epc_0_PRH_Addr_pin => xps_epc_0_PRH_Addr_pin,
      xps_epc_0_PRH_RNW_pin => xps_epc_0_PRH_RNW_pin,
      xps_epc_0_PRH_Rdy_pin => xps_epc_0_PRH_Rdy_pin,
      xps_epc_0_PRH_Data_I_pin => xps_epc_0_PRH_Data_I_pin,
      xps_epc_0_PRH_Data_O_pin => xps_epc_0_PRH_Data_O_pin
    );


	ctlWrProc: process
	begin
		wait until rising_edge(cpuclk);
		if locSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) = ctlAddr then
			ctlReg <= xps_epc_0_PRH_Data_O_pin;
		end if;
	end process;

	-- leds
	ledWrProc: process
	begin
		wait until rising_edge(cpuclk);
		if locSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) = ledAddr then
			ledReg <= xps_epc_0_PRH_Data_O_pin;
		end if;
	end process;
	
	fpga_0_LEDS_GPIO_IO_O_pin <= ledReg( 7 downto 0);
	
	
	-- read mux
	epcRdBlock: block
	begin
		xps_epc_0_PRH_Data_I_pin <= ctlReg when locSelect = '1' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) = ctlAddr 
			else statReg when locSelect = '1' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) = statAddr
			else ledReg when locSelect = '1' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0) = ledAddr
			else mstData when locSelect = '0'
			else (others => '0');
	end block;
	
	-- ready
	xps_epc_0_PRH_Rdy_pin <= '1' when locSelect = '1' else mstRdy;
	

	-- peripherals and external rod bus
   busMaster: asyncBusMasterMx PORT MAP (
	 localClk => cpuclk,
	 rst => cpurst,
	 busAd => busAd,
	 busAds => busAds,
	 busCs => slvSelect, 
	 busRnw => busRnw,
	 busAack => busAack,
	 busDack => dack, 
	 addr => addr,
	 dout => mstData,
	 din => xps_epc_0_PRH_Data_O_pin,
	 rdy => mstRdy, 
	 cs_n => mstSelect_n, 
	 rnw => xps_epc_0_PRH_RNW_pin
  );

	--busAack <= 'Z' when slvSelect = '1' or dack = '1' else '0'; -- drive aack low if bus inactive
	
	addr <= gndBits(31 downto xps_epc_0_PRH_Addr_pin'length) & "00" & xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 3 downto 0); -- leave top 2 address bits for range selection
	mstSelect_n <= '1' when (xps_epc_0_PRH_CS_n_pin = '1') or 
		xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) = locSlv else '0';

	locSelect <= '1' when (xps_epc_0_PRH_CS_n_pin = '0') and 
		xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) = locSlv else '0';
	
	busCsA <= '1' when slvSelect = '1' and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) = sp6Aslv else '0'; 
	busCsB <= '1' when slvSelect = '1'  and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) = sp6Bslv else '0'; 
	busCsBoc <= '1' when slvSelect = '1'  and xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) = bocSlv else '0'; 

	with xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1 downto xps_epc_0_PRH_Addr_pin'length - 2) select
		dack <= busDackA when sp6Aslv,
				  busDackB when sp6Bslv,
				  busDackBoc when bocSlv,
				  '0' when others;
	
	-- status register
	simStat: if simulation generate
	begin
		statReg(31) <= '1';
	end generate;
	
	impStat: if not simulation generate
	begin
		statReg(31) <= '0';
	end generate;
	
	
	-- serial port fifo
	-- from MSP code:
	--/* define the Transmit Control Register values */ 
	--/* no bit reversal, xmit length = 32bits, no companding, msb first */

	xps_ll_fifo_0_tx_llink_dest_rdy_n_pin <= sportFull;
	wrSport <= '1' when xps_ll_fifo_0_tx_packet = '1' and xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' 
		else '1' when xps_ll_fifo_0_tx_llink_sop_n_pin = '0' and xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' 
		else '0';
	theSport : sportFifo
  PORT MAP (
    rst => sportReset,
    wr_clk => cpuclk,
    rd_clk => clk40aux,
    din => xps_ll_fifo_0_tx_llink_din_pin,
    wr_en => wrSport,
    rd_en => rdSport,
    dout => sportData,
    full => sportFull,
    empty => sportEmpty
  );
  -- trace packet state: valid data only between sop and eop
  sportPac: process(sportReset, cpuclk)
  begin
	if sportReset = '1' then
		xps_ll_fifo_0_tx_packet <= '0';
	elsif rising_edge(cpuclk) then
		if xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_sop_n_pin = '0' then
			xps_ll_fifo_0_tx_packet <= '1';
		elsif xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_eop_n_pin = '0' then
			xps_ll_fifo_0_tx_packet <= '0';
		end if;
	end if;
  end process;

  --sportClkBuf: oddr2 port map (c0 => clk40aux, c1 => clk40aux_n, d0 => '1', d1 => '0', CE => '1', R => '0', S => '0', q => sport0clk);
  sportClkBuf: oddr 
		generic map(DDR_CLK_EDGE => "OPPOSITE_EDGE", INIT => '0', SRTYPE => "SYNC")
		port map (c => clk40aux, ce => '1', d1 => '1', d2 => '0', R => '0', S => '0', q => sport0clk);

  sportproc:process(sportReset, clk40aux)
  begin
	if sportReset = '1' then
		sportBits <= 0;
	elsif rising_edge(clk40aux) then
		if sportBits = 0 then
			sport0Data <= '0';
			-- read fifo
			if sportEmpty = '0' then
				sportReg <= sportData;
				rdSport <= '1';
				sportBits <= 32;
			end if;
		else
			if sportBits > 1 then
				sport0Data <= sportReg(sportBits - 1);
				sportBits <= sportBits - 1;
				rdSport <= '0';
			elsif sportEmpty = '0' then -- reload
				sport0Data <= sportReg(sportBits - 1);
				sportReg <= sportData;
				rdSport <= '1';
				sportBits <= 32;
			else
				sportBits <= 0;
				sport0Data <= '0';
				rdSport <= '0';
			end if;
		end if;
	end if;
  end process;
	
end architecture STRUCTURE;

