library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.math_real.all;
use ieee.numeric_std.all;
use ieee.STD_LOGIC_TEXTIO.ALL;
use Std.TextIO.ALL;


entity TTCgenerator is 
port (
    TTCbusout: out std_logic_vector(7 downto 0);    --:="11111111";
    rst_n_out :  out std_logic; -- asynchronous global reset 
    clk40_out :   out std_logic;
    clk40_neg :   out std_logic;
    L1Ap:   out std_logic;
    DCI:    out std_logic
   

);
end TTCgenerator;
 
architecture BEH of TTCgenerator is



constant triggertype : std_logic_vector := "0011000111"; -- numero a caso 8 + 2

type stato is ( a, b, c );
signal counterdel: integer := 0;  
signal current_state, next_state : stato;
signal clkin : std_logic := '0';
signal bx_cnt: std_logic_vector(11 downto 0):="000000000000";
signal L1_cnt: std_logic_vector(23 downto 0):="000000000000000000000000";
signal Ev_cnt: std_logic_vector(23 downto 0):="000000000000000000000000";
signal dummy_sID:std_logic_vector(0 to 36):="0000000000000000000000000000000000000";
signal dummy_sTT:std_logic_vector(0 to 10):="00000000000";
signal TTCbus: std_logic_vector(7 downto 0):="11111111";
signal bc_clk : std_logic :='1';
signal sl_clk : std_logic :='1';
signal num1: integer;
signal L1A: std_logic := '1' ;
signal L1A_rit: std_logic := '1' ;
signal orbit: std_logic := '0';
signal counter: integer :=1;
signal counterL1: integer :=0;  
signal clkL1As: std_logic:='1';  


---------------------------------------------------------
signal triggermode: std_logic_vector (1 downto 0):= "00";

signal BCntStr: std_logic :='0'; -- Validating signals
signal EvCntHStr: std_logic :='0';
signal EvCntLStr: std_logic :='0';

signal BCntRes: std_logic :='0'; --Bunch reset
signal L1Asomma: std_logic := '0';

signal Evcounter: integer :=0;


begin


TTCbusout(7)<=TTCbus(7);
TTCbusout(6)<=TTCbus(6);
TTCbusout(5)<=TTCbus(5);
TTCbusout(4)<=TTCbus(4);
TTCbusout(3)<=TTCbus(3);
TTCbusout(2)<=TTCbus(2);
TTCbusout(1)<=TTCbus(1);
TTCbusout(0)<=TTCbus(0);



L1Acontrol: process
VARIABLE timetest : REAL :=  0.00004;
VARIABLE interval3 : time;

begin

interval3 :=timetest * 1 sec;
  loop
      clkL1As <= not(clkL1As);
      wait for interval3;
      clkL1As <= not(clkL1As);
      wait for interval3;
  end loop;
  
--if rising_edge(clkL1As) then
 -- L1Asomma <= not(L1Asomma);
--end if;  
end process;



EventCount: process (L1A)

begin
if rising_edge(L1A)  then
  Evcounter <= Evcounter + 1;
  end if;
  
Ev_cnt <=conv_std_logic_vector(Evcounter,24);
end process;




L1ACount: process (clkL1As,L1A)

--begin
--if rising_edge(clkL1As)  then
--  counterL1 <= 0;
--  L1_cnt <=conv_std_logic_vector(counterL1,24);
--else if rising_edge(L1A) then
--  counterL1 <= counterL1 + 1;
--  L1_cnt <=conv_std_logic_vector(counterL1,24);
--  end if;
--end if;


begin
if rising_edge(L1A) and counterL1 = 16777216 then
  counterL1 <= 0;
else if rising_edge(L1A) then
  counterL1 <= counterL1 + 1;
  end if;
end if;

L1_cnt <=conv_std_logic_vector(counterL1,24);
end process;


L1Accept: process

VARIABLE PI : REAL := 6.28;

VARIABLE T1 : REAL :=  1.0/1.0; 
VARIABLE T2 : REAL :=  1.0/100.0;
VARIABLE T3 : REAL :=  1.0/1000.0;
VARIABLE T4 : REAL :=  1.0/10000.0;
VARIABLE T5 : REAL :=  1.0/100000.0;
VARIABLE buf: line;
VARIABLE Z1 : REAL :=  1.0/2147483398.0;

VARIABLE L : real := EXP (-10.0);
VARIABLE p : real := 1.0;
VARIABLE rand3: real;
VARIABLE s3 : positive;
VARIABLE s4 : positive;
VARIABLE k : real:= 0.0;
VARIABLE interval2 : time;
file my_output : TEXT;-- open APPEND_MODE is file_output; 
variable my_out_line : LINE; 
variable fstatus: file_open_status;

begin
LOOP 
p := 1.0;
k := 0.0;
while ( p > L ) LOOP
  UNIFORM(s3, s4, rand3);
  p := p*rand3;
  k := k+1.0;
END LOOP;
k := k-1.0;

interval2 := (k*(T5/10.0))*1 sec;
if L1_cnt < 16777215 then   --era 7
  WAIT FOR 25 ns;
  L1A <= '1';
  TTCbus(0)<='1';     --L1A active low
  WAIT FOR interval2;
  L1A <= '0'; 
  TTCbus(0)<='0';
else
  L1A <= '1';
  TTCbus(0)<='1';
  WAIT FOR 25 ns;
  L1A <= '1';
  TTCbus(0)<='1';
  WAIT FOR interval2;
end if;
END LOOP;




end process;






BC_CLKproc: process
VARIABLE time_bx : real := 0.000000025 ; --1.0/40078000; --sec
VARIABLE reset_bx: std_logic := '0';
VARIABLE interval: time :=(time_bx) *1 sec;

begin
  
loop
WAIT FOR 12.5 ns;
bc_clk <= NOT (bc_clk);
clk40_out<='1';
--TTCbus(2)<= (bc_clk);
WAIT FOR 12.5 ns;
bc_clk <= NOT (bc_clk);
clk40_out<='0';
--TTCbus(2)<= (bc_clk);
end loop;



TTCbus(7)<='1';  
TTCbus(6)<='1';  TTCbus(3)<='1'; 


end process;


BXcount: process (bc_clk, orbit)

VARIABLE reset_bx: std_logic := '0';

begin
if rising_edge(orbit) then
  counter <= 0;
else if rising_edge(bc_clk) then
  counter <= counter + 1;
  end if;
end if;

bx_cnt <=conv_std_logic_vector(counter,12);

end process;



ECR: process
Begin
LOOP

  TTCbus(1)<='1';
  WAIT FOR 200 ns;  

  TTCbus(1)<='0';
  
  WAIT FOR 25 ns;
  TTCbus(1)<='1';
  WAIT FOR 100 sec;
     
END LOOP;


end process;

ORBITA: process

VARIABLE time_orb : real := +0.0000920875;  --1.0/11223.0; --sec
VARIABLE interval: time :=time_orb *1 sec;

Begin
LOOP
  orbit <='1';
  TTCbus(2)<='0';
  
  WAIT FOR 5 ns;
  orbit <= '0'; 
  TTCbus(2)<='1';
  WAIT FOR 7.5 ns;  
  orbit <= '0';
  TTCbus(2)<='1';
  WAIT FOR interval;
     
END LOOP;


end process;


slow_clk: process
begin
  loop
    WAIT FOR 0.5 us;
    sl_clk <= NOT (sl_clk);

    WAIT FOR 0.5 us;
    sl_clk <= NOT (sl_clk);
  end loop;
end process;


L1AOut1: process(L1A)

begin
       L1Ap<= not (L1A);


end process;


L1ARit1: process(bc_clk,L1A)

--VARIABLE rand4: real;
--VARIABLE s5 : positive;
--VARIABLE s6 : positive;
--VARIABLE interval3 : time;


begin
  
--if (L1A = '0') then
  --current_state <= b;
--elsif 
if rising_edge(bc_clk) then 
  current_state <= next_state;
end if;

case current_state is
when a =>
      counterdel <= 0 ;
      L1A_rit<= '1';
      if (L1A ='0')  then   
    next_state <= b; 
    else
      next_state <= a;
        end if;
when b =>
     if (counterdel = 142 ) then
      next_state <= c;
     else   
     counterdel <= counterdel + 1;
     next_state <= b;
     end if;
   
   L1A_rit<= '1';
when c =>
       L1A_rit<= '0';
       next_state <= a;
end case;       

--UNIFORM(s5, s6, rand4);

--interval3 := rand4 * 0.000007 sec;

--if falling_edge(L1A) then
  
--  WAIT FOR interval3;
--  L1A_rit <= '0';
--  WAIT FOR 25 ns;
--  L1A_rit <= '1'; 
--end if;


end process;


SerialID: process (L1A_rit,bc_clk)


begin
  
if falling_edge(L1A_rit) then 

dummy_sID(0)<='1';
dummy_sID(1)<= L1_cnt(0);
dummy_sID(2)<= L1_cnt(1);
dummy_sID(3)<= L1_cnt(2);
dummy_sID(4)<= L1_cnt(3);
dummy_sID(5)<= L1_cnt(4);
dummy_sID(6)<= L1_cnt(5);
dummy_sID(7)<= L1_cnt(6);
dummy_sID(8)<= L1_cnt(7);
dummy_sID(9)<= L1_cnt(8);
dummy_sID(10)<= L1_cnt(9);
dummy_sID(11)<= L1_cnt(10);
dummy_sID(12)<= L1_cnt(11);
dummy_sID(13)<= L1_cnt(12);
dummy_sID(14)<= L1_cnt(13);
dummy_sID(15)<= L1_cnt(14);
dummy_sID(16)<= L1_cnt(15);
dummy_sID(17)<= L1_cnt(16);
dummy_sID(18)<= L1_cnt(17);
dummy_sID(19)<= L1_cnt(18);
dummy_sID(20)<= L1_cnt(19);
dummy_sID(21)<= L1_cnt(20);
dummy_sID(22)<= L1_cnt(21);
dummy_sID(23)<= L1_cnt(22);
dummy_sID(24)<= L1_cnt(23);

dummy_sID(25)<= bx_cnt(0);
dummy_sID(26)<= bx_cnt(1);
dummy_sID(27)<= bx_cnt(2);
dummy_sID(28)<= bx_cnt(3);
dummy_sID(29)<= bx_cnt(4);
dummy_sID(30)<= bx_cnt(5);
dummy_sID(31)<= bx_cnt(6);
dummy_sID(32)<= bx_cnt(7);
dummy_sID(33)<= bx_cnt(8);
dummy_sID(34)<= bx_cnt(9);
dummy_sID(35)<= bx_cnt(10);
dummy_sID(36)<= bx_cnt(11);

--


--TTCbus(4)<='0' after 40000 ns, '1' after 40025 ns;
--else if falling_edge (bc_clk) and L1A = '1' then
  --TTCbus(4)<='1';
else if rising_edge(bc_clk) then
    dummy_sID <= dummy_sID(1 to 36) & '0';
    end if;

end if;

--end if;
--WAIT 7 us;
TTCbus(4)<=NOT(dummy_sID(0));


end process;






SerialTT: process (L1A_rit,bc_clk)

begin
  
if ( falling_edge(bc_clk) or rising_edge(bc_clk) ) and L1A_rit = '0' then 

dummy_sTT(0)<='1';
dummy_sTT(1)<= triggertype(0);
dummy_sTT(2)<= triggertype(1);
dummy_sTT(3)<= triggertype(2);
dummy_sTT(4)<= triggertype(3);
dummy_sTT(5)<= triggertype(4);
dummy_sTT(6)<= triggertype(5);
dummy_sTT(7)<= triggertype(6);
dummy_sTT(8)<= triggertype(7);
dummy_sTT(9)<= triggertype(8);
dummy_sTT(10)<= triggertype(9);

else if rising_edge(bc_clk) then
    dummy_sTT <= dummy_sTT(1 to 10) & '0';
    end if;

end if;

TTCbus(5)<=NOT(dummy_sTT(0));


end process;

signal_1 : process
begin
loop  
  rst_n_out<='0';
  wait for 120 ns;
  rst_n_out<='1';
  wait for 120000000 ns;
end loop;

end process;
  
signal_2 : process 
begin
loop
 -- clk40_out<='1';
  clk40_neg<='0';
  wait for 12.5 ns;
 -- clk40_out<='0';
  clk40_neg<='1';
  wait for 12.5 ns;
end loop;

end process;



DCItrig: process

begin
DCI <= '0';
wait on L1A;
   
    wait for 300 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
     
    DCI<= '0';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
   
   
    DCI<= '0';
    wait for 225 ns;
   
    wait for 100 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
     
    DCI<= '0';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
----- Runmode ---------
    
    DCI<= '0';
    wait for 425 ns;
    
    ---SLOW ----
    wait for 100 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
     
     
    DCI<= '1';
    wait for 25 ns; 
    DCI<= '0'; 
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    -------------------- F3
    DCI<= '1';
    wait for 25 ns; 
    DCI<= '0'; 
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns; 
   --------------------- F4
   
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns; 
    DCI<= '1'; 
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
   
    -------------------- F5
    DCI<= '1';
    wait for 25 ns; 
    DCI<= '1'; 
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns; 
    DCI<= '0'; 
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
   
   
    DCI<= '0';
    wait for 2225 ns;
   




----- Trigger Cmd ------ Prima settare il runmode
   
    DCI<= '0';
    wait for 1725 ns;
    
    
    wait for 100 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
     
   
    DCI<= '0';
    wait for 225 ns;
    
    
    
    ----- Second Trigger 
    DCI<= '0';
    wait for 1725 ns;
    
    
    wait for 100 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
    DCI<= '0';
    wait for 25 ns;
    DCI<= '1';
    wait for 25 ns;
     
   
    DCI<= '0';
    wait for 2025 ns;
--    
--    
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    
--    --Chip ID
--    
--    
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--
--
--    --Field 5
--
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--
--    
--    
--    
--    
--    wait for 100 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns; 
--    DCI<='0';
--    
--    
--    --BCR
--    wait for 100 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '1';
--    wait for 25 ns;
--    DCI<= '0';
--    
--    wait for 25 ns; 
--    DCI<='0';
--    wait for 25 ns; 
--    DCI<='0';
--    wait for 25 ns; 
--    DCI<='0';
--    wait for 25 ns; 
--    DCI<='1';
--    
--    
end process;


end BEH;
