-- SetupBus to Wishbone connector
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bocEmu is
	port
	(
		-- clocking and reset
		clk : in std_logic := '0';
		reset : in std_logic := '0';
		
		-- rod bus slave
		sb_strobe_n : in std_logic := '1';
		sb_write_n : in std_logic := '1';
		sb_busy : out std_logic := '0';
		sb_addr : in std_logic_vector(15 downto 0);
		sb_data : inout std_logic_vector(7 downto 0) := (others => 'Z')
		
	);
end bocEmu;

architecture Behavioral of bocEmu is
	signal sb_strobe_delay : std_logic_vector(7 downto 0);
	signal sb_strobe_sr : std_logic_vector(2 downto 0) := "111";
	signal sb_strobe_falling : std_logic := '0';
	
	signal sb_data_in : std_logic_vector(7 downto 0);
	signal sb_data_out : std_logic_vector(7 downto 0);
	
	type WB_states is (st_idle, st_read, st_readwait, st_write, st_writewait);
	signal Z_wb : WB_states := st_idle;
	signal wb_do_read : std_logic := '0';
	signal wb_do_write : std_logic := '0';
	signal wb_finished : std_logic := '0';

	signal wb_timeout : integer range 0 to 15 := 15;
	
	type SB_states is (st_idle, st_wbwait, st_wbwait2, st_acknowledge);
	signal Z_sb : SB_states := st_idle;
	
	-- emulator ram
	TYPE mem IS ARRAY(0 TO 2**16 - 1) OF std_logic_vector(7 DOWNTO 0);
	SIGNAL ram_block : mem;
	
begin

-- register setup bus strobe signal for metastability reasons
process begin
	wait until rising_edge(clk);
	-- delay
	sb_strobe_delay(0) <= sb_strobe_n;
	sb_strobe_delay(7 downto 1) <= sb_strobe_delay(6 downto 0);
	
	if reset = '1' then
		sb_strobe_sr <= "111";
	else
		sb_strobe_sr <= sb_strobe_sr(1 downto 0) & sb_strobe_delay(7); --sb_strobe_n;
	end if;
end process;

-- get falling edge of strobe
sb_strobe_falling <= '1' when sb_strobe_sr(2 downto 1) = "10" else '0';

-- setup bus data IOB
sb_data <= sb_data_out when (sb_strobe_n = '0') and (sb_write_n = '1') else (others => 'Z');	-- tristate when write cycle from ROD
sb_data_in <= sb_data;

-- setup bus slave state machine
process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		wb_do_write <= '0';
		wb_do_read <= '0';
		sb_busy <= '0';
		
		Z_sb <= st_idle;
	else
		case Z_sb is
			when st_idle =>
				sb_busy <= '0';
				if sb_strobe_falling = '1' and sb_write_n = '0' then
					-- start a wishbone write cycle
					wb_do_write <= '1';
					wb_do_read <= '0';
					Z_sb <= st_wbwait;
				elsif sb_strobe_falling = '1' and sb_write_n = '1' then
					-- start a wishbone read cycle
					wb_do_read <= '1';
					wb_do_write <= '0';
					Z_sb <= st_wbwait;
				else
					-- do nothing
					wb_do_read <= '0';
					wb_do_write <= '0';
				end if;
				
			when st_wbwait =>
				-- wait
				wb_do_read <= '0';
				wb_do_write <= '0';
				sb_busy <= '0';
				Z_sb <= st_wbwait2;
				
			when st_wbwait2 =>
				Z_sb <= st_acknowledge;
				
			when st_acknowledge =>
				-- acknowledge data on setup bus
				sb_busy <= '1';
				
				if sb_strobe_sr(2) = '1' then
					Z_sb <= st_idle;
				end if;
		end case;
	end if;
end process;

ramProc: process
	begin
		wait until rising_edge(clk);
		if wb_do_write = '1' then
			ram_block(to_integer(unsigned(sb_addr))) <= sb_data_in;
		end if;
	end process;

   sb_data_out <= ram_block(to_integer(unsigned(sb_addr))); -- when wb_do_read = '1' else (others => 'Z');
	

end Behavioral;

