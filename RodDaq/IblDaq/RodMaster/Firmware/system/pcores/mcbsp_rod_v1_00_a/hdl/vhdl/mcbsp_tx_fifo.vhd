--------------------------------------------------------------------------------
-- Copyright (c) 1995-2011 Xilinx, Inc.  All rights reserved.
--------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version: O.61xd
--  \   \         Application: netgen
--  /   /         Filename: mcbsp_tx_fifo.vhd
-- /___/   /\     Timestamp: Mon Feb 20 15:58:36 2012
-- \   \  /  \ 
--  \___\/\___\
--             
-- Command	: -w -sim -ofmt vhdl C:/Users/travaglini/Documents/lavoro/Atlas/IBL_ROD/RodProto1/firmware_devel/13.2/ppc_globalnoddr/system/pcores/mcbsp_rod_v1_00_a/devl/projnav/ipcore_dir/tmp/_cg/mcbsp_tx_fifo.ngc C:/Users/travaglini/Documents/lavoro/Atlas/IBL_ROD/RodProto1/firmware_devel/13.2/ppc_globalnoddr/system/pcores/mcbsp_rod_v1_00_a/devl/projnav/ipcore_dir/tmp/_cg/mcbsp_tx_fifo.vhd 
-- Device	: 5vfx70tff1136-1
-- Input file	: C:/Users/travaglini/Documents/lavoro/Atlas/IBL_ROD/RodProto1/firmware_devel/13.2/ppc_globalnoddr/system/pcores/mcbsp_rod_v1_00_a/devl/projnav/ipcore_dir/tmp/_cg/mcbsp_tx_fifo.ngc
-- Output file	: C:/Users/travaglini/Documents/lavoro/Atlas/IBL_ROD/RodProto1/firmware_devel/13.2/ppc_globalnoddr/system/pcores/mcbsp_rod_v1_00_a/devl/projnav/ipcore_dir/tmp/_cg/mcbsp_tx_fifo.vhd
-- # of Entities	: 1
-- Design Name	: mcbsp_tx_fifo
-- Xilinx	: C:\Xilinx\13.2bis\ISE_DS\ISE\
--             
-- Purpose:    
--     This VHDL netlist is a verification model and uses simulation 
--     primitives which may not represent the true implementation of the 
--     device, however the netlist is functionally correct and should not 
--     be modified. This file cannot be synthesized and should only be used 
--     with supported simulation tools.
--             
-- Reference:  
--     Command Line Tools User Guide, Chapter 23
--     Synthesis and Simulation Design Guide, Chapter 6
--             
--------------------------------------------------------------------------------


-- synthesis translate_off
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use UNISIM.VPKG.ALL;

entity mcbsp_tx_fifo is
  port (
    rd_en : in STD_LOGIC := 'X'; 
    rst : in STD_LOGIC := 'X'; 
    empty : out STD_LOGIC; 
    wr_en : in STD_LOGIC := 'X'; 
    rd_clk : in STD_LOGIC := 'X'; 
    full : out STD_LOGIC; 
    wr_clk : in STD_LOGIC := 'X'; 
    prog_full : out STD_LOGIC; 
    dout : out STD_LOGIC_VECTOR ( 31 downto 0 ); 
    din : in STD_LOGIC_VECTOR ( 31 downto 0 ) 
  );
end mcbsp_tx_fifo;

architecture STRUCTURE of mcbsp_tx_fifo is
  signal N0 : STD_LOGIC; 
  signal N2 : STD_LOGIC; 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0_3 : STD_LOGIC; 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_reg_10 : STD_LOGIC; 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_reg_16 : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_ALMOSTEMPTY_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDERR_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRERR_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_3_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_2_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_1_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_0_UNCONNECTED : STD_LOGIC; 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_8_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_7_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_6_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_5_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_4_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_3_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_2_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_1_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_0_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_8_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_7_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_6_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_5_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_4_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_3_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_2_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_1_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_0_UNCONNECTED : STD_LOGIC;
 
  signal NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0_Q15_UNCONNECTED : STD_LOGIC; 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rd_rst_i : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_power_on_rd_rst : STD_LOGIC_VECTOR ( 0 downto 0 ); 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb : STD_LOGIC_VECTOR ( 4 downto 0 ); 
  signal U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb : STD_LOGIC_VECTOR ( 4 downto 0 ); 
begin
  XST_GND : GND
    port map (
      G => N0
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_reg_10,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(4)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(4),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(3)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(3),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(2)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(2),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(1)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(1),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(0)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_reg : FDPE
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      CE => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(0),
      D => N0,
      PRE => rst,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_reg_16
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb_4 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_reg_16,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(4)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb_3 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(4),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(3)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb_2 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(3),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(2)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb_1 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(2),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(1)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb_0 : FD
    generic map(
      INIT => '0'
    )
    port map (
      C => wr_clk,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(1),
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_wr_rst_fb(0)
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_reg : FDPE
    generic map(
      INIT => '0'
    )
    port map (
      C => rd_clk,
      CE => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_fb(0),
      D => N0,
      PRE => rst,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_reg_10
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18 : FIFO18_36
    generic map(
      ALMOST_FULL_OFFSET => X"004",
      SIM_MODE => "SAFE",
      DO_REG => 1,
      EN_SYN => FALSE,
      FIRST_WORD_FALL_THROUGH => FALSE,
      ALMOST_EMPTY_OFFSET => X"005"
    )
    port map (
      RDEN => rd_en,
      WREN => wr_en,
      RST => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rd_rst_i(0),
      RDCLK => rd_clk,
      WRCLK => wr_clk,
      ALMOSTEMPTY => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_ALMOSTEMPTY_UNCONNECTED,
      ALMOSTFULL => prog_full,
      EMPTY => empty,
      FULL => full,
      RDERR => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDERR_UNCONNECTED,
      WRERR => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRERR_UNCONNECTED,
      DI(31) => din(31),
      DI(30) => din(30),
      DI(29) => din(29),
      DI(28) => din(28),
      DI(27) => din(27),
      DI(26) => din(26),
      DI(25) => din(25),
      DI(24) => din(24),
      DI(23) => din(23),
      DI(22) => din(22),
      DI(21) => din(21),
      DI(20) => din(20),
      DI(19) => din(19),
      DI(18) => din(18),
      DI(17) => din(17),
      DI(16) => din(16),
      DI(15) => din(15),
      DI(14) => din(14),
      DI(13) => din(13),
      DI(12) => din(12),
      DI(11) => din(11),
      DI(10) => din(10),
      DI(9) => din(9),
      DI(8) => din(8),
      DI(7) => din(7),
      DI(6) => din(6),
      DI(5) => din(5),
      DI(4) => din(4),
      DI(3) => din(3),
      DI(2) => din(2),
      DI(1) => din(1),
      DI(0) => din(0),
      DIP(3) => N0,
      DIP(2) => N0,
      DIP(1) => N0,
      DIP(0) => N0,
      DO(31) => dout(31),
      DO(30) => dout(30),
      DO(29) => dout(29),
      DO(28) => dout(28),
      DO(27) => dout(27),
      DO(26) => dout(26),
      DO(25) => dout(25),
      DO(24) => dout(24),
      DO(23) => dout(23),
      DO(22) => dout(22),
      DO(21) => dout(21),
      DO(20) => dout(20),
      DO(19) => dout(19),
      DO(18) => dout(18),
      DO(17) => dout(17),
      DO(16) => dout(16),
      DO(15) => dout(15),
      DO(14) => dout(14),
      DO(13) => dout(13),
      DO(12) => dout(12),
      DO(11) => dout(11),
      DO(10) => dout(10),
      DO(9) => dout(9),
      DO(8) => dout(8),
      DO(7) => dout(7),
      DO(6) => dout(6),
      DO(5) => dout(5),
      DO(4) => dout(4),
      DO(3) => dout(3),
      DO(2) => dout(2),
      DO(1) => dout(1),
      DO(0) => dout(0),
      DOP(3) => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_3_UNCONNECTED,
      DOP(2) => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_2_UNCONNECTED,
      DOP(1) => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_1_UNCONNECTED,
      DOP(0) => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_DOP_0_UNCONNECTED,
      RDCOUNT(8) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_8_UNCONNECTED,
      RDCOUNT(7) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_7_UNCONNECTED,
      RDCOUNT(6) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_6_UNCONNECTED,
      RDCOUNT(5) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_5_UNCONNECTED,
      RDCOUNT(4) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_4_UNCONNECTED,
      RDCOUNT(3) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_3_UNCONNECTED,
      RDCOUNT(2) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_2_UNCONNECTED,
      RDCOUNT(1) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_1_UNCONNECTED,
      RDCOUNT(0) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_RDCOUNT_0_UNCONNECTED,
      WRCOUNT(8) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_8_UNCONNECTED,
      WRCOUNT(7) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_7_UNCONNECTED,
      WRCOUNT(6) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_6_UNCONNECTED,
      WRCOUNT(5) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_5_UNCONNECTED,
      WRCOUNT(4) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_4_UNCONNECTED,
      WRCOUNT(3) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_3_UNCONNECTED,
      WRCOUNT(2) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_2_UNCONNECTED,
      WRCOUNT(1) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_1_UNCONNECTED,
      WRCOUNT(0) => 
NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_v5_fifo_fblk_gextw_1_inst_extd_gonep_inst_prim_gw36_sngfifo18_WRCOUNT_0_UNCONNECTED
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_RD_RST_I_1_1 : LUT2
    generic map(
      INIT => X"E"
    )
    port map (
      I0 => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_rd_rst_reg_10,
      I1 => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_power_on_rd_rst(0),
      O => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rd_rst_i(0)
    );
  XST_VCC : VCC
    port map (
      P => N2
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0 : SRLC16E
    generic map(
      INIT => X"001F"
    )
    port map (
      A0 => N0,
      A1 => N0,
      A2 => N2,
      A3 => N0,
      CE => N2,
      CLK => rd_clk,
      D => N0,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0_3,
      Q15 => NLW_U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0_Q15_UNCONNECTED
    );
  U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_power_on_rd_rst_0 : FDE
    generic map(
      INIT => '1'
    )
    port map (
      C => rd_clk,
      CE => N2,
      D => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_Mshreg_power_on_rd_rst_0_3,
      Q => U0_xst_fifo_generator_gconvfifo_rf_gbiv5_bi_rstbt_power_on_rd_rst(0)
    );

end STRUCTURE;

-- synthesis translate_on
