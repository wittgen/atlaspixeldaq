library ieee;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.gitIdPack.all;

entity gitId is
port (
  reset: in std_logic;
  read: in std_logic;
  id: out std_logic_vector(7 downto 0)
);
end entity;

architecture arch of gitId is 

signal addr: integer range 0 to gitIdLength;

begin

  process(reset, read)
  begin
    if reset = '1' then 
      addr <= 0;
    elsif falling_edge(read) then 
      if addr < gitIdLength - 1 then
	addr <= addr + 1;
      end if;
    end if;
  end process;

  id <= gitHash(addr);
  
end arch;

