--------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Oklahoma
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------
--                                                         
--------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--
--------------------------------------------------------------------------
--
--  The controller bus is mapped directly to the fifo from low order bit
--   to high order bit- eg: data_bus(0 to 15) -> fifo_in(0 to 15)
--   with the link numbers are in ascending order as well.
--
--  The controller should at this point examine the
--   int_fifo_ef_out
--   int_fifo_ff_out
--   modebits_fifo_word_count to see if there is room for another "mode".
--
--  It is assumed that the modebit words will be written into the LUTs by
--  the controller in the following manner:
--
--
--    Readout Modebits LUT 32Words x 16bits
--     The following 12 bit words will be placed in the lower 12 bits of
--      the 16bit LUT Words. No zero padding of bits [15 downto 12] is
--      required, as they are ignored.
--
--    Default Modebits:
--     Address modebits
--    ------------------------------------
--     0x80    modebit0 links[11 to  0]
--     0x81    modebit1 links[11 to  0]
--     0x82    modebit0 links[23 to 12]
--     0x83    modebit1 links[23 to 12]
--     0x84    modebit0 links[35 to 24]
--     0x85    modebit1 links[35 to 24]
--     0x86    modebit0 links[47 to 36]
--     0x87    modebit1 links[47 to 36]
--     0x88    modebit0 links[59 to 48]
--     0x89    modebit1 links[59 to 48]
--     0x8A    modebit0 links[71 to 60]
--     0x8B    modebit1 links[71 to 60]
--     0x8C    modebit0 links[83 to 72]
--     0x8D    modebit1 links[83 to 72]
--     0x8E    modebit0 links[95 to 84]
--     0x8F    modebit1 links[95 to 84]
--
--    Correction Modebits:
--     Address modebits
--    ------------------------------------
--     0x90    modebit0 links[11 to  0]
--     0x91    modebit1 links[11 to  0]
--     0x92    modebit0 links[23 to 12]
--     0x93    modebit1 links[23 to 12]
--     0x94    modebit0 links[35 to 24]
--     0x95    modebit1 links[35 to 24]
--     0x96    modebit0 links[47 to 36]
--     0x97    modebit1 links[47 to 36]
--     0x98    modebit0 links[59 to 48]
--     0x99    modebit1 links[59 to 48]
--     0x9A    modebit0 links[71 to 60]
--     0x9B    modebit1 links[71 to 60]
--     0x9C    modebit0 links[83 to 72]
--     0x9D    modebit1 links[83 to 72]
--     0x9E    modebit0 links[95 to 84]
--     0x9F    modebit1 links[95 to 84]
--
--
--  Readout databus size is adjusted in the mux, where the appropriate
--  12 bits from the fifos are muxed to the modebits_out bus when appropriate.
--
--
--
--                     BankA
--                                      +------------------------------+
--                       clk----+       | 256x16bit registered fifo1   |     +
--                              |       | modebits for links[47 to  0] | 16  |\
--                             +--------|R                            R|-----| +------+
--                             |+------->                              |     |/       |
--                             ||  +----|wen                        ren|     +|       |
--                             ||  |    +------------------------------+      +---+   |
--                             ||  |                                              |   |
--                             ||  |                                              |   +--modebits(0 to 11)
--                     BankB   ||  |                                              |   |
--                             ||  |    +------------------------------+          |   |
--                             ||  |    | 256x16bit registered fifo2   |      +   |   |
--                             ||  |    | modebits for links[95 to 48] | 16   |\  |   |
--                             +--------|R                            R|------| +-----+
--                             |+------->                              |      |/  |
--                             |   |+---|wen                        ren|      +|  |
--                             |   ||   +------------------------------+       +-+|
--                             |   ||                                            ||
--                             |   |+-------------------------------+            ||
--                             |   +-------------------------------+|            ||
--                             |                                   ||            ||
--                             +----------------------+            ||            ||
--                                                    |            ||            ||
--                       +-------------------+        |            ||            ||
--                       | LUT 32wordx16bit  |        |            ||            ||
-- data_in(0 to 15)------|                   |--------+            ||            ||
-- address_in(0 to 5)----|                   |                     ||            ||
-- we--------------------|                   |--------+            ||            ||
--                       +-------------------+        |            ||            ||
--                                                    |            ||            ||
--                       +-------------------------+  |            ||            ||
--                       | writein_state_machine   |  |            ||            ||
-- L1A-------------------|      LUT readout address|--+            ||            ||
-- control(0 to 1)-------|                      we0|---------------+|            ||
-- apply_correction------|                      we1|----------------+            ||
--                       +-------------------------+                             ||
--                                                                               ||
--                                                                               ||
--                                                +------------------------+     ||
--                        clk --------------------> readout_state_machine0 |     ||
--                                                |                        |------+
--                     modebits_fifo_ef(0)--------|                        |---modebits_fifo_we[0 to 3]
--                     modebits_fifo_ff(0)--------|                        |     |            |
--                     control(0 to 1)------------|                        |     |            |
--                                         +------|token_in       token_out|-+   |            |
--                                         |  +---|                        | |   |            |
--                                         |  |   +------------------------+ |   |            |
--                                         |  |                              |   |            |
--                                         |  |   +--------------------+     |   |            |
--                                         |  +---|incr            decr|----------------------+
--                                         |      | BankA              |-------modebits_eventcount(7 downto 0)
--                        clk --------------------> modebits_eventcount|     |   |
--                                         |      +--------------------+     |   |
--                                         |    +----------------------------+   |
--                                         |    |                                |
--                                         |    |                                |
--                                         +---------------------------------+   |
--                                              |                            |   |
--                                              | +------------------------+ |   |
--                        clk --------------------> readout_state_machine1 | |   |
--                                              | |                        |-----+
--                     modebits_fifo_ef(0)--------|                        |---modebits_fifo_we[4 to 7]
--                     modebits_fifo_ff(0)--------|                        | |                |
--                     control(0 to 1)------------|                        | |                |
--                                              +-|token_in       token_out|-+                |
--                                            +---|                        |                  |
--                                            |   +------------------------+                  |
--                                            |                                               |
--                                            |   +--------------------+                      |
--                                            +---|incr            decr|----------------------+
--                                                | BankB              |-------modebits_eventcount(7 downto 0)
--                        clk --------------------> modebits_eventcount|
--                                                +--------------------+
--
--
--
--------------------------------------------------------------------------
-- Author: Sriram.S
-- 
--    6 March 2000 - MLN first version : Requirements and functionality
--                   understood and suficiently documented to proceed 
--                   with coding.
--    Friday 18 August 2000 - MLN second version: Kevin Einsweiler requested
--                            that we completely remove the controller dsp
--                            from the datapath. So now we have a LUT which
--                            get loaded with a default and corrective modebit
--                            set, that gets pushed out to the formatters 
--                            when a L1A is received.
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations


--------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------

entity formatter_readoutmodebits_encoder is
  port (
    clk                      : in  std_logic;  
    rst                      : in  std_logic;  
    control_in               : in  std_logic;  -- 0 : flush all, 1 : enable transfer

    int_lut_adr_in           : in  unsigned(7 downto 0);
    int_lut_bdata_in         : in  std_logic_vector(15 downto 0);  --
    int_lut_bdata_out        : out std_logic_vector(15 downto 0);  --
    ce_n_in                  : in  std_logic;
    rnw_in                   : in  std_logic;
    ack_out                  : out std_logic;
    apply_default_mode_in    : in  std_logic;
 -- interrupt to the master dsp to signify that a DEFAULT trigger has occurred
 --  and that default dynamic mask and default readoutmodebits will be written
 --  to the efb and formatter (respectively) on this trigger. 
 
    apply_corrective_mode_in : in  std_logic;
 -- interrupt to the master dsp to signify that a CORRECTIVE trigger has occurred
 -- and that corrective dynamic mask and corrective readoutmodebits will be written
 -- to the efb and formatter (respectively) on this AND ONLY this trigger. 
 -- This signal is used to clear the new_mask_ready_in set/reset flip-flop 
 
    modebits_fifo_ff_n_in    : in  std_logic_vector( 1 downto 0);  --
    modebits_out             : out std_logic_vector(11 downto 0);  --
    modebits_fifo_we_n_out   : out std_logic_vector( 7 downto 0);  --
    int_fifo_ef_out          : out std_logic_vector( 1 downto 0);  --
    int_fifo_ff_out          : out std_logic_vector( 1 downto 0);
    mb0_fifo_occ_count       : out std_logic_vector( 9 downto 0);
    mb1_fifo_occ_count       : out std_logic_vector( 9 downto 0);
    modebits_sent_out        : out std_logic;
    fe_cmd_mask_sel_in       : in  std_logic_vector( 2 downto 0)
    );
end formatter_readoutmodebits_encoder;

architecture rtl of formatter_readoutmodebits_encoder is

--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type readout_states is (
  reset,
  idle,
  switch_mux0,
  readout_mode00, -- mode, links (11 :  0) Bank A 
  readout_mode01, -- mode, links (23 : 12) Bank A
  readout_mode02,
  readout_mode03,
  pdafbc, -- propagate data and feedback control (4 Clks)
          -- one clk out of the controller fpga,
          -- one into the fifo memory,
          -- one out of the fifo memory,
          -- one into the controller fpga
  change_mode1, 
  switch_mux1,
  readout_mode10, -- mode, links (11 :  0) Bank B 
  readout_mode11, -- mode, links (23 : 12) Bank B
  readout_mode12,
  readout_mode13,
  change_mode0, 
  xfer_done,
  xfer_done_d1,
  xfer_done_d2
  );

type lut_writein_states is (
  reset,
  idle,
  set_default_lut_baseaddress,
  set_corrective_lut_baseaddress,
  check_fifo_status,
  check_fifo_status_do,
  write_fmb_word,
  write_fmb_word_do,
  target_fifo_full_wait,
  done_xfer
  );

type ack_states is (
  reset,
  wait_0_clk,
  wait_1_clk,
  wait_2_clk,
  wait_3_clk,
  acknowledge
  );

signal ro_pres_state             : readout_states;
signal wi_lut_pres_state         : lut_writein_states;
signal acknowledge_cycle_state   : ack_states;


--Bing
signal lut_wr_addr_i       : std_logic_vector(7 downto 0);
signal lut_rd_addr_i       : std_logic_vector(7 downto 0); 
signal gnd_i               : std_logic;
signal gnd_v_i             : std_logic_vector(15 downto 0);


signal ce_n_in_dly1_i            : std_logic; 
signal wi_lut_fine_statecount_i  : unsigned(4 downto 0);
signal wi_lut_fine_state_load_i  : std_logic;
signal wi_lut_fine_state_cntup_i : std_logic;
signal lut_baseaddress_i         : std_logic_vector(7 downto 0);
signal lut_outcoming_data_i      : std_logic_vector(15 downto 0);
signal lut_write_en_i            : std_logic;
signal lut_readout_adr_i         : unsigned(7 downto 0); --
signal flush_fifo_i              : std_logic;  
signal fifo_wren_i               : std_logic_vector(1 downto 0);  -- one clk version of the incoming write enable strobe
signal fifo_rden_i               : std_logic_vector(1 downto 0);
signal mbfifo_we_n_i             : std_logic_vector(7 downto 0);
signal fifo_ef_i                 : std_logic_vector(1 downto 0);  -- 
signal fifo_ff_i                 : std_logic_vector(1 downto 0);  --
signal mode0_out_i               : std_logic_vector(15 downto 0);
signal mode1_out_i               : std_logic_vector(15 downto 0);
signal token0                    : unsigned(3 downto 0);
signal token1                    : unsigned(3 downto 0);
signal wait_count_i              : unsigned(1 downto 0);
signal complete_lut_written_i    : std_logic;
signal fifo_select_i             : std_logic;
signal modebits_fifo_ff_in_i     : std_logic_vector(1 downto 0);
signal outputmux_control_i       : std_logic;
signal mb_count_i                : unsigned (4 downto 0);

-------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

-- instantiate all of the fifos and hook them up.
component fifo_1024x16_master
  port (
    clk             : in    std_logic;
    rst             : in    std_logic;
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    empty_flag_out  : out   std_logic;
    full_flag_out   : out   std_logic; --
    occupancy_count : out   std_logic_vector(9 downto 0)
    );
end component;

--component dpram_256x16
--  port (
--    clk              : in  std_logic;
--    rst              : in  std_logic;
--    dprm_writein_bus : in  std_logic_vector(15 downto 0);  -- changed data type
--    dprm_readiin_bus : out std_logic_vector(15 downto 0);  --
--    writein_adr      : in  unsigned(7 downto 0);
--    readout_adr      : in  unsigned(7 downto 0);
--    write_strobe     : in  std_logic;
--    read_strobe      : in  std_logic;
--    dprm_readout_bus : out std_logic_vector(15 downto 0)
--    );
--end component;

COMPONENT dpram_true_256x16
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
begin

flush_fifo_i <= NOT control_in;
-- instantiate all of the fifos and hook them up.

-- Bank A FIFO :  Stores mode bit words for formatters (0..3) since one
-- bank may be ahead of the other in event processing/readout
bank0_fifo : fifo_1024x16_master
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(0),
    rden_in         => fifo_rden_i(0),
    data_in         => lut_outcoming_data_i(15 downto 0),
    data_out        => mode0_out_i(15 downto 0),
    empty_flag_out  => fifo_ef_i(0),
    full_flag_out   => fifo_ff_i(0),
    occupancy_count => mb0_fifo_occ_count
    );

-- Bank B FIFO :  Stores mode bit words for formatters (4..7)
bank1_fifo : fifo_1024x16_master
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(1),
    rden_in         => fifo_rden_i(1),
    data_in         => lut_outcoming_data_i(15 downto 0),
    data_out        => mode1_out_i(15 downto 0),
    empty_flag_out  => fifo_ef_i(1),
    full_flag_out   => fifo_ff_i(1),
    occupancy_count => mb1_fifo_occ_count
    );

--instantiate the lut and hook it up.
-- purpose:  Mode Bit Look Up Table 
--lut : dpram_256x16
--  port map (
--    clk              => clk,
--    rst              => rst,
--    dprm_writein_bus => int_lut_bdata_in,
--    dprm_readiin_bus => int_lut_bdata_out,
--    writein_adr      => int_lut_adr_in,
--    readout_adr      => lut_readout_adr_i,
--    write_strobe     => lut_write_en_i,
--    read_strobe      => rst,  -- Not Connected
--    dprm_readout_bus => lut_outcoming_data_i
--    );

--Bing
lut_wr_addr_i <= std_logic_vector(int_lut_adr_in);
lut_rd_addr_i <= std_logic_vector(lut_readout_adr_i); 
gnd_i <= '0';
gnd_v_i <= (others => '0');

lut : dpram_true_256x16
  PORT MAP (
    clka => clk,--
    rsta => rst,--
    wea(0) => lut_write_en_i,--
    addra => lut_wr_addr_i,
    dina => int_lut_bdata_in,--
    douta => int_lut_bdata_out,--
    clkb => clk,--
    rstb => rst,--
    web(0) => gnd_i,--
    addrb => lut_rd_addr_i,
    dinb => gnd_v_i,--
    doutb => lut_outcoming_data_i-- 
  );	 

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------

lut_readout_adr_i(7) <= lut_baseaddress_i(7); 
lut_readout_adr_i(6) <= lut_baseaddress_i(6); 
lut_readout_adr_i(5) <= lut_baseaddress_i(5); 
lut_readout_adr_i(4) <= lut_baseaddress_i(4); 

lut_readout_adr_i(3) <= lut_baseaddress_i(3) OR wi_lut_fine_statecount_i(3); 
lut_readout_adr_i(2) <= lut_baseaddress_i(2) OR wi_lut_fine_statecount_i(2); 
lut_readout_adr_i(1) <= lut_baseaddress_i(1) OR wi_lut_fine_statecount_i(1); 
lut_readout_adr_i(0) <= lut_baseaddress_i(0) OR wi_lut_fine_statecount_i(0); 
      
fifo_select_i <= std_logic (lut_readout_adr_i(3));

int_fifo_ff_out <= fifo_ff_i;
int_fifo_ef_out <= fifo_ef_i;

signal_register : process(
   clk,
	rst
	)
begin  -- process mode_inputs
  if (rst = '1') then
    modebits_fifo_ff_in_i  <= (others => '0');
    modebits_fifo_we_n_out <= (others => '1');
  elsif (clk = '1' and clk'event) then
    modebits_fifo_ff_in_i  <= NOT modebits_fifo_ff_n_in;
    modebits_fifo_we_n_out <= mbfifo_we_n_i;  -- delay the WE by 1 clk
  end if;
end process signal_register;


-- State Machine that Writes ModeBits into the Que FIFO
wr_lut_to_fifo : process (
  clk, 
  rst,
  apply_default_mode_in,
  apply_corrective_mode_in,
  fifo_ef_i, fifo_ff_i,
  wi_lut_fine_statecount_i,
  wi_lut_pres_state
  )
begin
  if (rst = '1') then
    fifo_wren_i               <= "00";
    wi_lut_fine_state_load_i  <= '1';
    wi_lut_fine_state_cntup_i <= '0';
    lut_baseaddress_i         <= (others => '0');
    wi_lut_pres_state         <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '1') then
      case wi_lut_pres_state is
        when reset =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '1';
          wi_lut_fine_state_cntup_i <= '0';
          lut_baseaddress_i         <= (others => '0');
          wi_lut_pres_state         <= idle; 

        when idle =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '1';
          wi_lut_fine_state_cntup_i <= '0';
    -- set the base address of the LUT

          if (apply_corrective_mode_in = '1') then
            wi_lut_pres_state <= set_corrective_lut_baseaddress;
          elsif (apply_default_mode_in = '1') then
            wi_lut_pres_state <= set_default_lut_baseaddress;
          end if;
   
        when set_default_lut_baseaddress =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '1';
          wi_lut_fine_state_cntup_i <= '0';
          lut_baseaddress_i         <= fe_cmd_mask_sel_in & "00000";
--        lut_baseaddress_i         <= "10000000";
       -- 
         wi_lut_pres_state <= check_fifo_status;

        when set_corrective_lut_baseaddress =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '1';
          wi_lut_fine_state_cntup_i <= '0';
          lut_baseaddress_i         <= fe_cmd_mask_sel_in & "10000";
--        lut_baseaddress_i         <= "10010000";     
       -- 
          wi_lut_pres_state <= check_fifo_status;

        when check_fifo_status =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '0';
       -- start a writein sequence when the fifo has room
          wi_lut_pres_state         <= check_fifo_status_do;

        when check_fifo_status_do =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '0';
        -- 
         if (wi_lut_fine_statecount_i(4) = '1') then
           wi_lut_pres_state <= done_xfer;
         elsif (fifo_select_i = '0') then
           if (fifo_ff_i(0) = '1') then
             wi_lut_pres_state <= target_fifo_full_wait;
           else
             wi_lut_pres_state <= write_fmb_word;
           end if; 
         elsif (fifo_select_i = '1') then
           if (fifo_ff_i(1) = '1') then
             wi_lut_pres_state <= target_fifo_full_wait;
           else
             wi_lut_pres_state <= write_fmb_word;
           end if; 
         end if;
     
        when write_fmb_word =>
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '1';  -- increment ReadOut Addr now

          case lut_readout_adr_i(4 downto 3) is
            when "00"    => fifo_wren_i <= "01";
            when "01"    => fifo_wren_i <= "10";
            when "10"    => fifo_wren_i <= "01";
            when "11"    => fifo_wren_i <= "10";
            when others  => fifo_wren_i <= "00";
          end case; 
          -- 
          wi_lut_pres_state <= check_fifo_status;

        when target_fifo_full_wait =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '0';
        -- 
          if (   (fifo_select_i = '0' AND fifo_ff_i(0) = '0')         -- fifo0 not full
              OR (fifo_select_i = '1' AND fifo_ff_i(1) = '0') ) then  -- fifo1 not full
            wi_lut_pres_state <= write_fmb_word;
          end if;

        when done_xfer =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '0';

          wi_lut_pres_state <= idle;
    
        when others =>
          fifo_wren_i               <= "00";
          wi_lut_fine_state_load_i  <= '0';
          wi_lut_fine_state_cntup_i <= '0';
        --
          wi_lut_pres_state <= reset;
           
      end case;
    else
      fifo_wren_i               <= "00";
      wi_lut_fine_state_load_i  <= '1';
      wi_lut_fine_state_cntup_i <= '0';
      lut_baseaddress_i         <= (others => '0');
      wi_lut_pres_state         <= reset;
    end if; 
  end if;  
end process wr_lut_to_fifo;


-- lut_writein fine state counter
wi_lut_fine_statecounter : process (
  clk, 
  rst,
  wi_lut_fine_statecount_i,
  wi_lut_fine_state_load_i,
  wi_lut_fine_state_cntup_i
  )
begin
  if (rst = '1') then
    wi_lut_fine_statecount_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (wi_lut_fine_state_load_i = '1') then
      wi_lut_fine_statecount_i  <= (others => '0');
    elsif ((wi_lut_fine_statecount_i < 16) AND (wi_lut_fine_state_cntup_i = '1')) then
      wi_lut_fine_statecount_i <= wi_lut_fine_statecount_i + 1;
    end if;
  end if; 
end process wi_lut_fine_statecounter;

mb_updn_count: process (
  clk, 
  rst, 
  wi_lut_pres_state, 
  ro_pres_state
  )
begin  -- 
  if (rst = '1') then
    mb_count_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (wi_lut_pres_state = reset OR ro_pres_state = reset) then
      mb_count_i <= (others => '0');
    elsif (wi_lut_pres_state = done_xfer AND ro_pres_state = xfer_done) then
      mb_count_i <= mb_count_i;
    elsif (wi_lut_pres_state = done_xfer) then
      mb_count_i <= mb_count_i + 1;
    elsif (ro_pres_state = xfer_done) then
      mb_count_i <= mb_count_i - 1;
    end if;
  end if;
end process mb_updn_count;

mask_write: process (
  clk, 
  rst, 
  mb_count_i
  )
begin
  if (rst = '1') then
    complete_lut_written_i <= '0';
  elsif (clk'event and clk = '1') then
    if (mb_count_i = 0) then
      complete_lut_written_i <= '0';
    else
      complete_lut_written_i <= '1';
    end if;
  end if;
end process mask_write;

-- Read out State Machine
-- For the RO State Machine to operate, the assumuption is made that a transfer
-- can only be started if there is enough room in the Formatter ModeBit FIFOs
-- for all of the Mode Bits.  The ReadOut Engine can transfer to the banks
-- of Formatters in any order, but the transfer can not complete if one of the
-- destination FIFOs is Full.

ro_state_logic : process (
  clk, 
  rst, 
  ro_pres_state
  )
begin
  if (rst = '1') then
    modebits_sent_out <= '0';
    fifo_rden_i       <= "00";
    mbfifo_we_n_i     <= (others => '1');
    token0            <= (others => '0');
    token1            <= (others => '0');
    wait_count_i      <= (others => '0');
   -- clear everything
    ro_pres_state <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '0') then  -- Soft Reset
      modebits_sent_out <= '0';
      fifo_rden_i       <= "00";
      mbfifo_we_n_i     <= (others => '1');
      token0            <= (others => '0');
      token1            <= (others => '0');
      wait_count_i      <= (others => '0');
   -- clear everything
      ro_pres_state <= reset;
    elsif (control_in = '1') then
      case ro_pres_state is
        when reset =>
          modebits_sent_out <= '0';
          fifo_rden_i       <= "00";
          mbfifo_we_n_i     <= (others => '1');
          token0            <= (others => '0');
          token1            <= (others => '0');
          wait_count_i      <= (others => '0');
      -- clear everything
          ro_pres_state <= idle; 
        when idle =>
          modebits_sent_out <= '0';
          fifo_rden_i       <= "00";
          mbfifo_we_n_i     <= (others => '1');
          token0            <= (others => '0');
          token1            <= (others => '0');
          wait_count_i      <= (others => '0');
      --
-- If an event has begun, and the LUT has been written into the Internal
-- FIFO, check the Formatter Mode Bit FIFOs and start the transfer
          if (complete_lut_written_i = '1') then
            if (modebits_fifo_ff_in_i(0) = '0' AND fifo_ef_i(0) = '0') then
              ro_pres_state <= switch_mux0;
            elsif (modebits_fifo_ff_in_i = "01" AND fifo_ef_i(1) = '0') then
              ro_pres_state <= switch_mux1;
            end if;
          end if;
      
        when switch_mux0 =>
          ro_pres_state <= readout_mode00;
 
        when readout_mode00 =>
          fifo_rden_i   <= "01";
          mbfifo_we_n_i <= "11111110";
          --
          ro_pres_state <= pdafbc;
          token0 <= token0 + 1;
 
        when pdafbc =>
          fifo_rden_i   <= "00";
          mbfifo_we_n_i <= (others => '1');
          --
          if (std_logic_vector(wait_count_i) = "01") then
            if    (std_logic_vector(token0) = "1000" AND std_logic_vector(token1) = "1000") then
              ro_pres_state <= xfer_done;   
            elsif (std_logic_vector(token0) = "1000" AND std_logic_vector(token1) < "1000") then
              ro_pres_state <= change_mode1;
            elsif (outputmux_control_i = '0') then
              if (std_logic_vector(token0) < "1000" ) then
                ro_pres_state <= change_mode0;
              end if;
            elsif (outputmux_control_i = '1') then
              if (std_logic_vector(token1) < "1000" ) then
                ro_pres_state <= change_mode1;
              end if;
            end if;  
          else
            wait_count_i <= wait_count_i + 1;
          end if;
 
        when readout_mode01 =>
          fifo_rden_i   <= "01";
          mbfifo_we_n_i <= "11111101";
          token0 <= token0 + 1;
          --
          ro_pres_state <=  pdafbc;

        when readout_mode02 =>
          fifo_rden_i   <= "01";
          mbfifo_we_n_i <= "11111011";
          token0 <= token0 + 1;
          --
          ro_pres_state <=  pdafbc;

        when readout_mode03 =>
          fifo_rden_i   <= "01";
          mbfifo_we_n_i <= "11110111";
          token0 <= token0 + 1;
          --
          ro_pres_state <=  pdafbc;

        when switch_mux1 =>
          if (fifo_ef_i(1) = '0') then  --
            ro_pres_state <= readout_mode10;
          end if;
          
        when readout_mode10 =>
          fifo_rden_i   <= "10";
          mbfifo_we_n_i <= "11101111";
          token1 <= token1 + 1;
          --
          ro_pres_state <=  pdafbc;
 
        when readout_mode11 =>
          fifo_rden_i   <= "10";
          mbfifo_we_n_i <= "11011111";
          token1 <= token1 + 1;
          --
          ro_pres_state <=  pdafbc;

        when readout_mode12 =>
          fifo_rden_i   <= "10";
          mbfifo_we_n_i <= "10111111";
          token1 <= token1 + 1;
          --
          ro_pres_state <=  pdafbc;

        when readout_mode13 =>
          fifo_rden_i   <= "10";
          mbfifo_we_n_i <= "01111111";
          token1 <= token1 + 1;
          --
          ro_pres_state <=  pdafbc;

        when change_mode1 =>
          case token1 is
            when "0000" => ro_pres_state <= switch_mux1;
            when "0001" => ro_pres_state <= readout_mode10;
            when "0010" => ro_pres_state <= readout_mode11;
            when "0011" => ro_pres_state <= readout_mode11;
            when "0100" => ro_pres_state <= readout_mode12;
            when "0101" => ro_pres_state <= readout_mode12;
            when "0110" => ro_pres_state <= readout_mode13;
            when "0111" => ro_pres_state <= readout_mode13;
            when "1000" =>
              if (std_logic_vector(token0)  = "1000") then
                ro_pres_state <= xfer_done;
              else
                ro_pres_state <= change_mode0;
              end if;
            when others => -- NOP
          end case;
          wait_count_i <= (others => '0');

        when change_mode0 =>     
          case token0 is
            when "0000" => ro_pres_state <= switch_mux0;
            when "0001" => ro_pres_state <= readout_mode00;
            when "0010" => ro_pres_state <= readout_mode01;
            when "0011" => ro_pres_state <= readout_mode01;
            when "0100" => ro_pres_state <= readout_mode02;
            when "0101" => ro_pres_state <= readout_mode02;
            when "0110" => ro_pres_state <= readout_mode03;
            when "0111" => ro_pres_state <= readout_mode03;
            when "1000" =>
              if (std_logic_vector(token1)  = "1000") then
                ro_pres_state <= xfer_done;
              else
                ro_pres_state <= change_mode1;
              end if;
            when others => -- NOP
          end case;
          wait_count_i <= (others => '0');

        when xfer_done =>
          modebits_sent_out <= '1';
          token1            <= (others => '0');
          token0            <= (others => '0');
          wait_count_i      <= (others => '0');
         --
          ro_pres_state <= xfer_done_d1;

        when xfer_done_d1 =>
          modebits_sent_out <= '0';
          ro_pres_state     <= xfer_done_d2;

        when xfer_done_d2 =>
          ro_pres_state <= idle;

        when others =>
          ro_pres_state <= idle;
      end case;
    else
      fifo_rden_i   <= "00";
      mbfifo_we_n_i <= (others => '1');
         --
      ro_pres_state <= idle;
    end if; 
  end if;  
end process ro_state_logic;


output_mux : process (
  outputmux_control_i, 
  mode0_out_i, 
  mode1_out_i
  )
begin
  if (outputmux_control_i = '0') then
    modebits_out(11 downto 0) <= mode0_out_i(11 downto 0);
  elsif (outputmux_control_i = '1') then
    modebits_out(11 downto 0) <= mode1_out_i(11 downto 0);
  end if;
end process output_mux;


mux_ctrl : process (
  clk,
  rst,  
  ro_pres_state
  )
begin
  if (rst = '1') then
    outputmux_control_i <= '0';
  elsif (clk'event and clk = '1') then
    if (ro_pres_state = switch_mux0) then
      outputmux_control_i <= '0';
    elsif (ro_pres_state = switch_mux1) then
      outputmux_control_i <= '1';
    end if;
  end if;
end process mux_ctrl;


reg_transfer_acknowledge_generator : process (
  clk, 
  rst, 
  ce_n_in
  )
begin
  if (rst = '1') then
    ack_out                 <= '0';
    ce_n_in_dly1_i          <= '1';
    acknowledge_cycle_state <= reset;
    lut_write_en_i          <= '0';

  elsif (clk'event and clk = '1') then
    ce_n_in_dly1_i <= ce_n_in; -- en_n_in negative edge delay detection register
    case acknowledge_cycle_state is
      when reset =>
        ack_out        <= '0';
        lut_write_en_i <=  NOT ce_n_in AND NOT rnw_in;
      --
        if (ce_n_in = '0' AND ce_n_in_dly1_i = '1') then
          acknowledge_cycle_state <= wait_0_clk;
        end if;

      when wait_0_clk =>
        ack_out        <= '0';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= wait_1_clk;

      when wait_1_clk =>
        ack_out        <= '0';
        lut_write_en_i <='0';
      --
        acknowledge_cycle_state <= wait_2_clk;

      when wait_2_clk =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= wait_3_clk;

      when wait_3_clk =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= acknowledge;
  --
      when acknowledge =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --
        if (ce_n_in = '1') then
          acknowledge_cycle_state <= reset;
        end if;
    end case;
  end if;
end process reg_transfer_acknowledge_generator;

end rtl;

