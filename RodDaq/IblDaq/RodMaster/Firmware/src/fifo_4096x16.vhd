--------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------
--
-- Filename: 2048x16fifo.vhd                  
-- Description:
--  Input registered and Output registered fifo.     
--
--------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- History:
--    23 February 2000 - MLN first version : requirements/functionality finally
--                       understood and sufficiently documented to proceed 
--                       with coding.
--    13 March 2000 - MLN : 1536 word dynamic mask fifo not required.
--                         Must update to a 512 word fifo.
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------
entity fifo_4096x16 is
  port(
    clk             : in    std_logic; -- clk40 input
    rst             : in    std_logic; -- negative logic asynchronous global reset
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    empty_flag_out  : out   std_logic; --
    full_flag_out   : out   std_logic; --
    occupancy_count : out   std_logic_vector(11 downto 0)
    );
end fifo_4096x16;

architecture rtl of fifo_4096x16 is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

signal vcc_i    : std_logic;
signal gnd_i    : std_logic;

signal ingoing_data_i    : std_logic_vector(15 downto 0);
signal outcoming_data_i  : std_logic_vector(15 downto 0);
signal write_en_i        : std_logic;
signal writein_adr_i     : unsigned(11 downto 0);
signal readout_adr_i     : unsigned(11 downto 0);
signal occupancy_count_i : unsigned(11 downto 0);

-- Bing
signal ram_wr_addr : std_logic_vector(11 downto 0);
signal ram_rd_addr : std_logic_vector(11 downto 0);
--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

--component dpram_4096x1
--  port(
--    clk              : in  std_logic; -- clk40 input
--    rst              : in  std_logic; -- asynchronous global reset
--    dprm_writein_bus : in  std_logic_vector(0 downto  0); -- data from the DataRouterManager 
--    writein_adr      : in  unsigned(11 downto  0); -- 
--    readout_adr      : in  unsigned(11 downto  0); --
--    write_strobe     : in  std_logic; --
--    dprm_readout_bus : out std_logic_vector(0 downto  0)  -- data to the output tbuf
--    );
--end component;

COMPONENT dpram_4096x16
  PORT (
    clka : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 downto 0);
    addra : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    addrb : IN STD_LOGIC_VECTOR(11 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
begin

vcc_i    <= '1';
gnd_i    <= '0';

occupancy_count <= std_logic_vector(occupancy_count_i);

-- Bing
ram_wr_addr <= std_logic_vector(writein_adr_i);
ram_rd_addr <= std_logic_vector(readout_adr_i);

--MB : for i in 0 to 15 generate
--    mb_dpr : dpram_4096x1
--      port map (
--        clk              => clk,
--        rst              => rst,
--        dprm_writein_bus => ingoing_data_i(i downto i),
--        writein_adr      => writein_adr_i,
--        readout_adr      => readout_adr_i,
--        write_strobe     => wren_in,
--        dprm_readout_bus => outcoming_data_i(i downto i)
--        );
--end generate;

M0 : dpram_4096x16
  PORT MAP (
    clka => clk,
    wea(0) => wren_in,
    addra => ram_wr_addr,
    dina => ingoing_data_i,
    clkb => clk,
    rstb => rst,
    addrb => ram_rd_addr,
    doutb => outcoming_data_i
  );

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------

-- input  address counter
input_counter : process (
  clk,
  rst,  
  flush_fifo_in
  )
begin
  if (rst = '1') then
    writein_adr_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (flush_fifo_in = '1') then
      writein_adr_i  <= (others => '0'); 
    elsif (write_en_i = '1' AND occupancy_count_i < 4095) then
      writein_adr_i <= writein_adr_i + 1;
    end if;
  end if;
end process input_counter;

-- output  address counter
output_counter : process (
  clk,
  rst,
  flush_fifo_in
  )
begin
  if (rst = '1') then
    readout_adr_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (flush_fifo_in = '1') then
      readout_adr_i  <= (others => '0'); 
    elsif (rden_in = '1' AND occupancy_count_i > 0) then
      readout_adr_i <= readout_adr_i + 1;
    end if;
  end if;
end process output_counter;


-- fifo 16bit word occupancy counter
occupancy_counter : process (
  clk,
  rst,
  flush_fifo_in
  )
begin
  if (rst = '1') then
    occupancy_count_i  <= (others => '0');
    empty_flag_out <= '1';
    full_flag_out  <= '0';
  elsif (clk'event and clk = '1') then

    if (occupancy_count_i = 4095) then 
      full_flag_out <= '1';
    else
      full_flag_out <= '0'; 
    end if;

    if (occupancy_count_i = 0   ) then 
      empty_flag_out <= '1';
    else
      empty_flag_out <= '0';
    end if;
    
    if (flush_fifo_in = '1') then
      occupancy_count_i  <= (others => '0');
    else
      occupancy_count_i <= writein_adr_i - readout_adr_i;
    end if;
  end if;
end process occupancy_counter;


-- delay wren to ram block by 1 clk
-- delay rden to ram block by 1 clk
-- read/write enable delay register
enable_delay : process (
  clk,
  rst,
  wren_in
  )
begin
  if (rst = '1') then
    write_en_i <= '0';
  elsif (clk'event and clk = '1') then
    write_en_i <= wren_in;
  end if;
end process enable_delay;


-- input  register enabled by delayed wren
input_register : process (
  clk,
  rst,
  data_in
  )
begin
  if (rst = '1') then
    ingoing_data_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    ingoing_data_i <= data_in;
  end if;
end process input_register;

-- memory output   enabled by delayed rden
output_register : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    data_out <= (others => '0');
  elsif (clk'event and clk = '1') then
    data_out <= outcoming_data_i;
  end if;
end process output_register;

end rtl;
