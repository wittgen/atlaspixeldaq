library ieee;
use IEEE.STD_LOGIC_1164.ALL;

package gitIdPack is
-- git hash is 40 byte
constant gitIdLength: integer := 20;
type gitHashType is array (0 to gitIdLength - 1) of std_logic_vector(7 downto 0);

-- the constant definition must be in the following format:
---constant gitHash: gitHashType := (
--X"4b",
-- ... total of 20 values like this. last one not terminated with ,
--X"c6",
-- X"df"
--);
--end gitIdPack;  
--
-- Todo: copy this template to a new file named gitIdPkg.vhd
-- append the hash values in the format above
-- append closing bracket and "end gitIdPack" statements  
-- done

constant gitHash: gitHashType := (
