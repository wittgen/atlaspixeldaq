-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: front_end_trigger_counter.vhd
-- Description: 
--
-------------------------------------------------------------------------------
-- Structure: 
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author:         John M. Joseph
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen 
-- History:
--    Wednesday 1 Marcj 2000 - First version
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
-------------------------------------------------------------------------------
--library UNISIM;
--use unisim.all;
-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
--
entity fe_trigger_counter is  
  port(
    clk                   : in  std_logic;
    clear_counter_in      : in  std_logic; -- originally 2 bits, MSB for SCT
--    rod_type_in           : in  std_logic;
    num_l1_value_in       : in  unsigned(3 downto 0); --
    fe_command_pulse_in   : in  std_logic; -- L1A trigger pulse
    fmt_writes_trailer_in : in  std_logic; -- originally 2 bits, MSB for SCT
    occupancy_count_out   : out unsigned(3 downto 0) -- oiginally 8 bits, 4 MSBs for SCT
    );
end fe_trigger_counter; 

architecture rtl of fe_trigger_counter is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------
signal count0_i : unsigned(3 downto 0); -- 
-- signal count1_i : unsigned(3 downto 0); -- for SCT, don't need this
signal countn_i : unsigned(3 downto 0);
signal dec_count0_i : std_logic; -- decrement count

begin

-- occupancy_count_out <= count1_i & count0_i; -- concatenate, do we need the 4 MSBs?
occupancy_count_out <= count0_i; --

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
feocc_counter0 : process (
  clk,
  clear_counter_in, 
  fmt_writes_trailer_in,
  fe_command_pulse_in, 
  num_l1_value_in
  )
begin
  if (clear_counter_in = '1') then
    count0_i <= (others => '0');
    countn_i <= (others => '0');
    dec_count0_i <= '0';
  elsif (clk'event and clk = '1') then
-- EVEN Link Counter
    if    (fe_command_pulse_in = '1' AND fmt_writes_trailer_in = '0') then
      count0_i <= count0_i + 1; 
    elsif (fe_command_pulse_in = '0' AND dec_count0_i = '1') then
      count0_i <= count0_i - 1; 
    end if;

    dec_count0_i <= '0';
	 
    if (fmt_writes_trailer_in = '1' AND countn_i = num_l1_value_in) then
      dec_count0_i <= '1';
      countn_i <= (others => '0');
    elsif (fmt_writes_trailer_in = '1') then
      countn_i <= countn_i + 1;
    end if;
	 
  end if; 
end process feocc_counter0;

--feocc_counter1 : process (
--  clk_in,
--  clear_counter_in, 
--  fmt_writes_trailer_in,
--  fe_command_pulse_in
--  )
--begin
--  if (clear_counter_in(1) = '1') then
--    count1_i <= (others => '0');
--  elsif (clk_in'event and clk_in = '1') then
---- ODD Link Counter
--    if (rod_type_in = '0') then
--      if (fe_command_pulse_in = '1' AND fmt_writes_trailer_in(1) = '0') then
--        count1_i <= count1_i + 1; 
--      elsif (fe_command_pulse_in = '0' AND fmt_writes_trailer_in(1) = '1') then
--        count1_i <= count1_i - 1; 
--      end if;
--    else
--      count1_i <= (others => '0');
--    end if;
--  end if; 
--end process feocc_counter1;

end rtl; -- code for front_end_trigger_counter
