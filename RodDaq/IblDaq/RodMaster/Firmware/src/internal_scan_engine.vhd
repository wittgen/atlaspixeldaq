-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: internal_scan_engine.vhd
-- Description:
--
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: John M. Joseph
-- Board Engineer: John M. Joseph
-- History:
--    14 February 2005 - JMJ first version
--
-------------------------------------------------------------------------------
-- Input Definitions
--   NEvent      : Number of command sequences to issue in this mask stage
--   TrigDelay0  : Delay between CAL and L1 cmd in 25ns increment
--   TrigDelay1  : Delay between CAL and L1 cmd in 25ns increment
--   PreCalDelay : Delay between first L1 and second L1 cmd in 25ns increment
--   Interval    : Delay Channels for crosstalk scan default value is 5
--   TrigMode    : Trigger Sequence and Algorithm selection 32 bits
--     Modes to control scanning:
--       Trigger Algorithm Select Bits (15:0)
--         VAL  SCAN DESCRIPTION
--         0x1  Self Trigger up to 4 groups
--         0x2  Group synchronous scan CAL/TD0/L1A
--         0x3  Fixed interval scan CAL/TD0/L1A 
--         0x4  Fully synchronous scan CAL/TD0/L1A
--         0x5  Crosstalk Scan CAL/CAL
--         0x6  Crosstalk Scan CAL/NOCAL
--              Only Group 0 is allowed for Crosstalk testing
--
--    TrigAlgorithm = decide between different algorithms for issuing 
--                    sequences to different module groups, and for 
--                    issuing successive sequences.
--
--      * delayed-mode => issue command sequences to a series of groups. Each
--                        subsequent group in the list has the command sequence
--                        issued n microsecs after the previous group.
--
--      * next trigger mode => Issue the next command sequence based on one of 
--                             three different algorithms:        
--
--          i) fixed interval    => Issue next sequence after a fixed delay,
--                                  independent of the status of returned data
--
--         ii) group synchronous => Issue the next sequence for a given module
--                                  group as soon as all of the FE occupancy
--                                  counters for the modules in that group go
--                                  to zero (meaning all expected trailers 
--                                  have been seen)
--
--        iii) fully synchronous => Issue the next sequence only when all
--                                  modules for all active groups have their 
--                                  FE occupancy counters returned to zero.
--
--      TrigSequence = decide what sequence of commands to issue for one event:
--        * Self-trigger => Issue single TRIG command only
--        * Normal       => Issue CAL/TrigDelay1/TRIG sequence
--        * Dual         => Issue CAL/TrigDelay1/TRIG/TrigDelay2/TRIG seq.
--
--   Triggers will be issued to groups in the following order:
--      TRIG0 FECMD0_OUT ==> Group 0
--      TRIG1 FECMD0_OUT ==> Group 1
--      TRIG2 FECMD0_OUT ==> Group 2
--      TRIG3 FECMD0_OUT ==> Group 3
--
-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity int_scan_engine is
  port (
    clk               : in  std_logic; 
    rst               : in  std_logic;
    enable_in         : in  std_logic; --
    clear_nevent_in   : in  std_logic; --
    new_mask_ready_in : in  std_logic; -- a new mask from the controller is ready a
    rod_tt_wr_in      : in  std_logic;
    num_events_in     : in  std_logic_vector(15 downto 0); -- 
    precal_delay_in   : in  std_logic_vector( 7 downto 0);
    trig_delay0_in    : in  std_logic_vector(15 downto 0); --
    trig_delay1_in    : in  std_logic_vector(15 downto 0); --
    evt_interval_in   : in  std_logic_vector(31 downto 0); --
    trig_mode_in      : in  std_logic_vector(31 downto 0); --
    act_groups_in     : in  std_logic_vector( 3 downto 0); --
    feocc_zero_in     : in  std_logic_vector( 3 downto 0); --
    xoff_in           : in  std_logic;
    mask_en_out       : out std_logic_vector( 2 downto 0); --
    new_mask_out      : out std_logic;
    mask_dxi_out      : out std_logic;
    fecmd0_out        : out std_logic;
    fecmd1_out        : out std_logic;
    nevent_out        : out std_logic_vector(15 downto 0);
    nevent_done_out   : out std_logic
    );
end int_scan_engine;

architecture rtl of int_scan_engine is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type scan_engine_modes is (
  idle,
  scan,
  done
  );
signal scan_mode : scan_engine_modes;               

type scan_engine_states is (
  idle,
  set_mask,
  mask_setup,
  mask_ready,
  send_l1a0,
  send_l1a1,
  send_cal0,
  send_cal1,
  send_nocal,
  send_xtalk,
  check_next_group,
  wait_precal_td,
  wait_td0,
  wait_td1,
  wait_interval,
  wait_for_data,
  wait_for_groups,
  latch_l1id,
  done
  );
signal scan0_state : scan_engine_states;               
signal scan1_state : scan_engine_states;               

signal cmd0_bit_count : unsigned( 3 downto 0);
signal cmd1_bit_count : unsigned( 3 downto 0);
signal trig_delay0    : unsigned(15 downto 0); --
signal trig_delay1    : unsigned(15 downto 0); --
signal precal_delay   : unsigned(15 downto 0); --
signal evt_interval   : unsigned(31 downto 0); --
signal group_en_i     : unsigned( 2 downto 0); --
signal group_select   : natural range 0 to 3;
signal state0_done_i  : std_logic; --
signal state1_done_i  : std_logic; --

type array4of16bitunsigned is array (0 to 3) of unsigned(15 downto 0);
signal nevent_count  : array4of16bitunsigned;
signal nevent_done_i : std_logic; --

signal set_mask_i   : std_logic; --
signal new_mask_i   : std_logic; --
signal last_group_i : std_logic; --

begin
-------------------------------------------------------------------------------
-- SIGNAL ASSIGNMENTS
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Scan Engine Logic
-------------------------------------------------------------------------------
scan_engine : process (
  clk,
  rst
  )						    
begin
  if (rst = '1') then
    scan_mode  <= idle;
    scan0_state <= idle;
    scan1_state <= idle;
    mask_dxi_out <= '0';
    evt_interval   <= (others => '0');
    nevent_done_out <= '0';
    for j in 0 to 3 loop
      nevent_count(j) <= (others => '0');
    end loop;
    nevent_out <= (others => '0');
  elsif (clk'event and clk = '1') then
    mask_dxi_out <= enable_in;
    case scan_mode is
      when idle =>
        evt_interval <= (others => '0');
        if (clear_nevent_in = '1') then
          for j in 0 to 3 loop
            nevent_count(j) <= (others => '0');
          end loop;
        end if;
        if (trig_mode_in(3 downto 0) /= "0000" AND enable_in = '1' AND act_groups_in /= "0000") then
          scan_mode  <= scan;
        else
          scan_mode  <= idle;
        end if;
        scan0_state <= idle;
        scan1_state <= idle;
      when scan =>
        if (enable_in = '0') then
          scan_mode <= idle;
        end if;
        if (enable_in = '1' AND xoff_in = '0') then
          if (evt_interval = unsigned(evt_interval_in)) then
            evt_interval <= (others => '0');
          else
            evt_interval <= evt_interval + 1 ;
          end if;
        end if;
        case scan0_state is
          when idle =>
            if (enable_in = '1' AND xoff_in = '0') then
              scan0_state <= set_mask;
            end if;
          when send_cal0 =>
            if (state0_done_i = '1') then
              scan0_state <= wait_td0;
            end if;
          when wait_td0 =>
            if (state0_done_i = '1') then
              scan0_state <= send_l1a0;
            end if;
          when send_l1a0 =>
            if (state0_done_i = '1') then
              scan0_state <= check_next_group;
            end if;
          when check_next_group => 
            if (rod_tt_wr_in = '1') then
              nevent_count(group_select) <= nevent_count(group_select) + 1 ;  -- inc event count 
              if (trig_mode_in(3 downto 0) = "0001") then
                if (act_groups_in(group_select) = '1' AND feocc_zero_in(group_select) = '1') then
                  scan0_state <= wait_for_data;
                end if;
              elsif (trig_mode_in(3 downto 0) = "0010") then
                if (act_groups_in(group_select) = '1' AND feocc_zero_in(group_select) = '1') then
                  scan0_state <= wait_for_data;
                end if;
              elsif (trig_mode_in(3 downto 0) = "0011") then
                if (group_select < 3) then
                  if (act_groups_in(group_select+1) = '1') then
                    scan0_state <= set_mask;
                  elsif (act_groups_in(group_select+1) = '0') then
                    scan0_state <= wait_interval;
                  end if;
                else
                  scan0_state <= wait_interval;
                end if;
              elsif (trig_mode_in(3 downto 2) = "01") then  -- Code 5&6 Crosstalk
                scan0_state <= wait_for_groups;
              end if;
            end if;
          when set_mask =>
            if (xoff_in = '0') then
              scan0_state <= mask_setup;
            end if;
          when mask_setup =>                           
            if (state0_done_i = '1') then
              scan0_state <= mask_ready;
            end if;
          when mask_ready =>
            if (new_mask_ready_in = '1') then
              if (trig_mode_in(3 downto 0) = "0001") then
                scan0_state <= send_l1a0;
              else
                scan0_state <= send_cal0;
              end if;
              if (trig_mode_in(3 downto 2) = "01") then
                scan1_state <= wait_precal_td;
              end if;
            end if;
          when wait_interval =>
            if (set_mask_i = '1') then
              scan0_state <= latch_l1id;
            elsif (state0_done_i = '1') then
              scan0_state <= done;
            end if; 
          when wait_for_groups =>
            if (set_mask_i = '1') then
              scan0_state <= latch_l1id;
            elsif (state0_done_i = '1') then
              scan0_state <= done;
            end if; 
          when wait_for_data =>
            if (set_mask_i = '1') then
              scan0_state <= latch_l1id;
            elsif (state0_done_i = '1') then
              scan0_state <= done;
            end if; 
          when latch_l1id =>
            nevent_out <= std_logic_vector(nevent_count(group_select));
            scan0_state <= set_mask;
          when done => 
            if (enable_in = '0') then
              scan0_state <= idle;
            end if;
          when others =>
        end case;

--  if trig_mode_in(3 downto 0) = "0101" OR "0110"
        case scan1_state is
          when wait_precal_td =>
            if (state1_done_i = '1') then
              if (trig_mode_in(3 downto 0) = "0101") then 
                scan1_state <= send_cal1;
              elsif (trig_mode_in(3 downto 0) = "0110") then
                scan1_state <= send_nocal;
              end if;
            end if;
          when send_cal1 =>
            if (state1_done_i = '1') then
              scan1_state <= wait_td1;
            end if;
          when send_nocal =>
            if (state1_done_i = '1') then
              scan1_state <= wait_td1;
            end if;
          when wait_td1 =>
            if (state1_done_i = '1') then
              scan1_state <= send_l1a1;
            end if;
          when send_l1a1 =>
            if (state1_done_i = '1') then
              scan1_state <= done;
            end if;
          when done => scan1_state <= idle;
          when others =>
        end case;
      when done =>
        scan_mode  <= idle;
        scan0_state <= idle;
      when others =>
        scan_mode  <= idle;
        scan0_state <= idle;
    end case;
    if (nevent_done_i = '1' AND enable_in = '1') then
      nevent_done_out <= '1';
    elsif (enable_in = '0') then
      nevent_done_out <= '0';
    end if;  
  end if;  
end process scan_engine;

scan0_states : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    new_mask_out <= '0';
    new_mask_i   <= '0';
    mask_en_out <= (others => '0');
    cmd0_bit_count <= (others => '0');                
    state0_done_i <= '0';                
    nevent_done_i <= '0'; 
    set_mask_i <= '0';
    last_group_i <= '0';
    group_select <=  0 ;
    group_en_i <= (others => '0');
    trig_delay0 <= (others => '0');
    fecmd0_out <= '0';
  elsif (clk'event and clk = '1') then
    state0_done_i <= '0';                
    set_mask_i <= '0';
    last_group_i <= '0';
    new_mask_i   <= '0';
    new_mask_out <= new_mask_i;
    nevent_done_i <= '0'; 
    case scan0_state is
      when idle =>
        cmd0_bit_count <= (others => '0');
        state0_done_i <= '0';                
        nevent_done_i <= '0'; 
        group_en_i <= (others => '0');
        group_select <=  0 ;
        set_mask_i <= '0';
        last_group_i <= '0';
      when check_next_group => 
        if (rod_tt_wr_in = '1') then
          if (trig_mode_in(3 downto 0) = "0011" OR trig_mode_in(3 downto 0) = "0100") then
            if (group_select < 3) then
              if (act_groups_in(group_select+1) = '1') then
                set_mask_i <= '1';
                group_select <= group_select + 1 ;
                group_en_i <= group_en_i + 1;
              end if;
            else
            end if;
          end if;
        end if;
      when set_mask =>
        cmd0_bit_count <= (others => '0');
        state0_done_i <= '0';                
        nevent_done_i <= '0'; 
        mask_en_out <= std_logic_vector(group_en_i);
        new_mask_i <= '1';
        last_group_i <= '0';
      when mask_setup =>                           
        new_mask_i <= '0';
        if (cmd0_bit_count = 4) then
          state0_done_i <= '0';                
          cmd0_bit_count <= (others => '0');
        elsif (cmd0_bit_count = 3) then
          state0_done_i <= '1';                
          cmd0_bit_count <= cmd0_bit_count + '1';
        else
          cmd0_bit_count <= cmd0_bit_count + '1';
        end if;
      when send_l1a0 =>  -- L1A Command
        state0_done_i <= '0';                
--        if (rod_type_in = '1') then --  l1accept: "11101" 
          cmd0_bit_count <= cmd0_bit_count + '1';
          case cmd0_bit_count is
            when "0000" => fecmd0_out <= '1';                 
            when "0001" => fecmd0_out <= '1';                 
            when "0010" => fecmd0_out <= '1';                 
            when "0011" => fecmd0_out <= '0';                 
            when "0100" => fecmd0_out <= '1';                 
            when "0101" => fecmd0_out <= '0';
                           state0_done_i <= '1';                
            when others => fecmd0_out <= '0';
                           state0_done_i <= '0';                
          end case;
--        else
--          fecmd0_out <= '0';                 
--          state0_done_i <= '1';                
--        end if;
      when send_cal0 =>  -- CAL Command
        state0_done_i <= '0';                
--        if (rod_type_in = '1') then    -- "1011001000"
          cmd0_bit_count <= cmd0_bit_count + '1';
          case cmd0_bit_count is
            when "0001" => fecmd0_out <= '1';
            when "0010" => fecmd0_out <= '0';
            when "0011" => fecmd0_out <= '1';
            when "0100" => fecmd0_out <= '1';
            when "0101" => fecmd0_out <= '0';
            when "0110" => fecmd0_out <= '0';
            when "0111" => fecmd0_out <= '1';
            when "1000" => fecmd0_out <= '0';
            when "1001" => fecmd0_out <= '0';
            when "1010" => fecmd0_out <= '0';
                           state0_done_i <= '1';
            when others => fecmd0_out <= '0';
                           state0_done_i <= '0';
          end case;
--        else
--          fecmd0_out <= '0';                 
--          state0_done_i <= '1';                
--        end if;
      when wait_td0 => 
        state0_done_i <= '0';                
        cmd0_bit_count <= (others => '0');
        trig_delay0 <= trig_delay0 - 1 ;
        if (trig_delay0 = unsigned(trig_delay0_in)) then
          trig_delay0 <= (others => '0');
          state0_done_i <= '1';                
        end if;
      when wait_interval => 
        state0_done_i <= '0';                
        cmd0_bit_count <= (others => '0');
        if (evt_interval = unsigned(evt_interval_in)) then
          if ((nevent_count(0) = unsigned(num_events_in) OR act_groups_in(0) = '0') AND
              (nevent_count(1) = unsigned(num_events_in) OR act_groups_in(1) = '0') AND
              (nevent_count(2) = unsigned(num_events_in) OR act_groups_in(2) = '0') AND
              (nevent_count(3) = unsigned(num_events_in) OR act_groups_in(3) = '0')) then
            state0_done_i <= '1';                
          else 
            set_mask_i <= '1';
            group_select <=  0 ;
            group_en_i <= "000";
          end if;
        end if;
      when wait_for_data =>
        if (act_groups_in(0) = '1' AND feocc_zero_in(0) = '0' AND nevent_count(0) /= unsigned(num_events_in)) then
          set_mask_i <= '1';
          group_select <= 0 ;
          group_en_i <= "000";
        elsif (act_groups_in(1) = '1' AND feocc_zero_in(1) = '0' AND nevent_count(1) /= unsigned(num_events_in)) then
          set_mask_i <= '1';
          group_select <= 1 ;
          group_en_i <= "001";
        elsif (act_groups_in(2) = '1' AND feocc_zero_in(2) = '0' AND nevent_count(2) /= unsigned(num_events_in)) then
          set_mask_i <= '1';
          group_select <= 2 ;
          group_en_i <= "010";
        elsif (act_groups_in(3) = '1' AND feocc_zero_in(3) = '0' AND nevent_count(3) /= unsigned(num_events_in)) then
          set_mask_i <= '1';
          group_select <= 3 ;
          group_en_i <= "011";
        elsif ((nevent_count(0) = unsigned(num_events_in) OR act_groups_in(0) = '0') AND
               (nevent_count(1) = unsigned(num_events_in) OR act_groups_in(1) = '0') AND
               (nevent_count(2) = unsigned(num_events_in) OR act_groups_in(2) = '0') AND
               (nevent_count(3) = unsigned(num_events_in) OR act_groups_in(3) = '0')) then
          state0_done_i <= '1';
        end if;
      when wait_for_groups =>
        if (feocc_zero_in = "0000") then
          if (trig_mode_in(3 downto 0) = "0101" OR trig_mode_in(3 downto 0) = "0110") then
            if (nevent_count(0) = unsigned(num_events_in)) then -- Group 0 only
              state0_done_i <= '1';
            else 
              set_mask_i <= '1';
              group_select <=  0 ;
              group_en_i <= "000";
            end if;
          else
            if ((nevent_count(0) = unsigned(num_events_in) OR act_groups_in(0) = '0') AND
                (nevent_count(1) = unsigned(num_events_in) OR act_groups_in(1) = '0') AND
                (nevent_count(2) = unsigned(num_events_in) OR act_groups_in(2) = '0') AND
                (nevent_count(3) = unsigned(num_events_in) OR act_groups_in(3) = '0')) then
              state0_done_i <= '1';
            else 
              set_mask_i <= '1';
              group_select <=  0 ;
              group_en_i <= "000";
            end if;
          end if;
        end if;
      when done =>
        state0_done_i <= '0';                
        nevent_done_i <= '1'; 
        set_mask_i    <= '0';
        cmd0_bit_count <= (others => '0');
      when others =>
    end case;
  end if;  
end process scan0_states;

scan1_states : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    state1_done_i <= '0';                
    cmd1_bit_count <= (others => '0');
    trig_delay1 <= (others => '0');
    precal_delay <= (others => '0');
    fecmd1_out <= '0';
  elsif (clk'event and clk = '1') then
    state1_done_i <= '0';                
    case scan1_state is
      when idle =>
        cmd1_bit_count <= (others => '0');
        state1_done_i <= '0';                
      when send_l1a1 =>  -- L1A Command
        state1_done_i <= '0';                
--        if (rod_type_in = '1') then --  l1accept: "11101" 
          cmd1_bit_count <= cmd1_bit_count + '1';
          case cmd1_bit_count is
            when "0000" => fecmd1_out <= '1';                 
            when "0001" => fecmd1_out <= '1';                 
            when "0010" => fecmd1_out <= '1';                 
            when "0011" => fecmd1_out <= '0';                 
            when "0100" => fecmd1_out <= '1';                 
            when "0101" => fecmd1_out <= '0';
                           state1_done_i <= '1';                
            when others => fecmd1_out <= '0';
                           state1_done_i <= '0';                
          end case;
--        else
--          fecmd1_out <= '0';                 
--          state1_done_i <= '1';                
--        end if;
      when send_cal1 =>  -- CAL Command
        state1_done_i <= '0';                
--        if (rod_type_in = '1') then    -- "1011001000"
          cmd1_bit_count <= cmd1_bit_count + '1';
          case cmd1_bit_count is
            when "0001" => fecmd1_out <= '1';
            when "0010" => fecmd1_out <= '0';
            when "0011" => fecmd1_out <= '1';
            when "0100" => fecmd1_out <= '1';
            when "0101" => fecmd1_out <= '0';
            when "0110" => fecmd1_out <= '0';
            when "0111" => fecmd1_out <= '1';
            when "1000" => fecmd1_out <= '0';
            when "1001" => fecmd1_out <= '0';
            when "1010" => fecmd1_out <= '0';
                           state1_done_i <= '1';
            when others => fecmd1_out <= '0';
                           state1_done_i <= '0';
          end case;
--        else
--          fecmd1_out <= '0';                 
--          state1_done_i <= '1';                
--        end if;
      when wait_td1 => 
        state1_done_i <= '0';                
        cmd1_bit_count <= (others => '0');
        trig_delay1 <= trig_delay1 + 1 ;
        if (trig_delay1 = unsigned(trig_delay1_in)) then
          trig_delay1 <= (others => '0');
          state1_done_i <= '1';                
        end if;
      when send_nocal =>  -- NOCAL Command
        state1_done_i <= '0';                
--        if (rod_type_in = '1') then    -- "0000000000"
          cmd1_bit_count <= cmd1_bit_count + '1';
          case cmd1_bit_count is
            when "1010" => fecmd1_out <= '0';
                           state1_done_i <= '1';
            when others => fecmd1_out <= '0';
                           state1_done_i <= '0';
          end case;
--        else
--          fecmd1_out <= '0';                 
--          state1_done_i <= '1';                
--        end if;
      when wait_precal_td => 
        state1_done_i <= '0';                
        cmd1_bit_count <= (others => '0');
        precal_delay <= precal_delay + 1 ;
        if (precal_delay = unsigned(precal_delay_in)) then
          precal_delay <= (others => '0');
          state1_done_i <= '1';                
        end if;
      when done =>
        state1_done_i <= '0';                
        cmd1_bit_count <= (others => '0');
      when others =>
    end case;
  end if;  
end process scan1_states;

end rtl;
