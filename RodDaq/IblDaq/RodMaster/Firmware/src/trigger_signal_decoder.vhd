-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: trigger_signal_decoder.vbd
-- Description:
--  Implements the serialL1ID and TriggerType serial decoders.
--  These are just simple no error detection shift registers -
--   that is: there is no examination for consistency with local L1/BC 
--   counters, proper size, or any sort of bit error or crc checking.
--
-- Serial input data streams are all positive logic.
-- Below listing is from the "TIM web-page: Timinf of TIM Output Signals"
--        LSB first
-- Serial ID: leading zero(s) & 1 & 24bits L1ID & 12bits BCID & trailing_zero(s)
--                                     
-- Serial TT: leading zero(s) & 1 & 8+2bits trigger type & trailing_zero(s)
--    
--
-- Add a 256x32 Block Ram to store event type counters 
-- Start Address 0x00404C00
-- Event Type == ADDR 
-- include a state machine that histograms the Event type when it arrives from
-- the TIM
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    23 February 2000 - MLN first version : requirements/functionality finally
--                       understood and sufficiently documented to proceed 
--                       with coding.
--
--    21 March    2002 - JMJ Modified State Machines to fix bug that prevented
--                       proper decoding if the serial data arrived with 1 clk 
--                       between packet.
--
--    20 August   2013 - SPC Modified code (slightly) for use in IBL ROD
--
-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity trigger_signal_decoder is
  port (
    clk                  : in    std_logic; --
    rst                  : in    std_logic; -- asynchronous global reset
    enable_decoding_in   : in    std_logic; -- 
    serial_id_in         : in    std_logic; --
    serial_tt_in         : in    std_logic; --
    l1id_bits_out        : out   std_logic_vector(23 downto 0); --
    bcid_bits_out        : out   std_logic_vector(11 downto 0); --
    ttype_bits_out       : out   std_logic_vector( 9 downto 0); --
    new_l1id_valid_out   : out   std_logic; --
    new_ttype_valid_out  : out   std_logic  --
    );
end trigger_signal_decoder;

architecture rtl of trigger_signal_decoder is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type id_states is (reset, idle, load_id, id_valid, wait_for_next_id);
type tt_states is (reset, idle, load_tt, tt_valid, wait_for_next_tt);

signal pres_id_state : id_states;
signal pres_tt_state : tt_states;

signal serial_id_bits_i   : std_logic_vector(36 downto 0);
signal l1bc_id_data_reg_i : std_logic_vector(35 downto 0);

signal serial_tt_bits_i : std_logic_vector(10 downto 0);                 
signal ttype_data_reg_i : std_logic_vector( 9 downto 0);                 

signal latch_id_data_i : std_logic;
signal latch_tt_data_i : std_logic;

signal new_l1id_valid_out_i  : std_logic;
signal new_ttype_valid_out_i : std_logic;


-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

begin

-------------------------------------------------------------------------------
-- Event ID Serial Decode Logic
-------------------------------------------------------------------------------

-- State machine next state determination logic 
id_state_logic : process (
  clk, 
  rst, 
  pres_id_state
  )
variable serial_id_bit_count_i  : integer range 0 to 36;
begin
  if (rst = '1') then
    new_l1id_valid_out_i  <= '0'; 
    new_l1id_valid_out    <= '0'; 
    serial_id_bit_count_i := 0;
    pres_id_state <= reset;
  elsif (clk'event and clk = '1') then
    new_l1id_valid_out <= new_l1id_valid_out_i; -- delay outputs by 1 clk
    
    if (enable_decoding_in = '0') then
      new_l1id_valid_out_i  <= '0'; 
      serial_id_bit_count_i := 0;
      pres_id_state         <= reset;
    elsif (enable_decoding_in = '1') then
      case pres_id_state is
        when reset =>
          new_l1id_valid_out_i  <= '0'; 
          latch_id_data_i       <= '0';
          serial_id_bit_count_i :=  0 ;
          pres_id_state         <= idle; 

        when idle =>
          new_l1id_valid_out_i  <= '0'; 
          latch_id_data_i       <= '0';
          serial_id_bit_count_i :=  0 ;
          if (serial_id_bits_i(1) = '1') then  -- The Bit Location of the Wake-Up Bit
            pres_id_state        <= load_id; 
          end if; 

        when load_id =>
          new_l1id_valid_out_i <= '0'; 
          latch_id_data_i      <= '1';
          pres_id_state        <= id_valid;

        when id_valid =>
          new_l1id_valid_out_i <= '1';
          latch_id_data_i      <= '0';
          pres_id_state        <= wait_for_next_id;

        when wait_for_next_id =>
          new_l1id_valid_out_i <= '0'; 
          latch_id_data_i      <= '0';
          serial_id_bit_count_i := serial_id_bit_count_i + 1;
--        if (serial_id_bit_count_i = 34) then
          if (serial_id_bit_count_i = 35) then
            pres_id_state  <= idle;
          end if;

        when others =>
          pres_id_state <= reset;
      end case;
    else
      new_l1id_valid_out_i  <= '0'; 
      latch_id_data_i       <= '0';
      serial_id_bit_count_i := 0;
    --   
      pres_id_state <= reset;
    end if; 
  end if;  
end process id_state_logic;


--  bits come out the low order side of the shifter, and 0's get shifted into
--  the other side
id_shifter : process (
  clk, 
  rst, 
  enable_decoding_in
  )
begin
  if (rst = '1') then
    serial_id_bits_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (enable_decoding_in = '0') then
      serial_id_bits_i <= (others => '0');
    else
      serial_id_bits_i(36 downto 0) <= serial_id_in & serial_id_bits_i(36 downto 1);
    end if;
  end if;
end process id_shifter;

id_register : process (
  clk, 
  rst, 
  enable_decoding_in
  )
begin
  if (rst = '1') then
    l1bc_id_data_reg_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (enable_decoding_in = '0') then
      l1bc_id_data_reg_i <= (others => '0');
    elsif (latch_id_data_i = '1') then
      l1bc_id_data_reg_i(35 downto 0) <= serial_id_bits_i(35 downto 0);
    end if;
  end if;
end process id_register;

-- Connect Data to Outputs
l1id_bits_out(23 downto 0) <= l1bc_id_data_reg_i(23 downto  0);
bcid_bits_out(11 downto 0) <= l1bc_id_data_reg_i(35 downto 24);

-------------------------------------------------------------------------------
-- Trigger Type Serial Decode Logic
-------------------------------------------------------------------------------

-- State machine next state determination logic 
tt_state_logic : process (
  clk, 
  rst, 
  pres_tt_state
  )
variable serial_tt_bit_count_i : integer range 0 to 10;      
begin
  if (rst = '1') then
    new_ttype_valid_out_i <= '0';
    new_ttype_valid_out   <= '0';
    latch_tt_data_i       <= '0';
    serial_tt_bit_count_i :=  0 ;
    pres_tt_state <= reset;
  elsif (clk'event and clk = '1') then
    new_ttype_valid_out <= new_ttype_valid_out_i;
    
    if (enable_decoding_in = '0') then
      new_ttype_valid_out_i <= '0';
      latch_tt_data_i       <= '0';
      -- 
      pres_tt_state <= reset;
    elsif (enable_decoding_in = '1') then
      case pres_tt_state is
        when reset =>
          new_ttype_valid_out_i <= '0';
          latch_tt_data_i       <= '0';
          serial_tt_bit_count_i :=  0 ;
         --   
          pres_tt_state <= idle; 

        when idle =>
          new_ttype_valid_out_i <= '0';
          latch_tt_data_i       <= '0';
          serial_tt_bit_count_i :=  0 ;
         --
          if (serial_tt_bits_i(1) = '1') then  -- The Bit Location of the Wake-Up Bit
            pres_tt_state        <= load_tt; 
          end if; 

        when load_tt =>
          latch_tt_data_i       <= '1';
          new_ttype_valid_out_i <= '0';
         --
          pres_tt_state        <= tt_valid;

        when tt_valid =>
          new_ttype_valid_out_i <= '1';
          latch_tt_data_i       <= '0';
          pres_tt_state         <= wait_for_next_tt; 
          
        when wait_for_next_tt => 
          new_ttype_valid_out_i <= '0';
          latch_tt_data_i       <= '0';
          serial_tt_bit_count_i := serial_tt_bit_count_i +1;
--        if (serial_tt_bit_count_i = 8) then
          if (serial_tt_bit_count_i = 9) then
            pres_tt_state <= idle; 
          end if;

        when others =>
          pres_tt_state <= reset;
      end case;
    else
      new_ttype_valid_out_i <= '0';
      latch_tt_data_i       <= '0';
      serial_tt_bit_count_i :=  0 ;
     --   
      pres_tt_state <= reset; 
    end if;
  end if;
end process tt_state_logic;

--  bits come out the low order side of the shifter, and 0's get shifted into
--  the other side
tt_shifter : process (
  clk, 
  rst, 
  enable_decoding_in
  )
begin
  if (rst = '1') then
    serial_tt_bits_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (enable_decoding_in = '0') then
      serial_tt_bits_i <= (others => '0');
    else
      serial_tt_bits_i(10 downto 0) <= serial_tt_in & serial_tt_bits_i(10 downto 1);
    end if;
  end if;
end process tt_shifter;

tt_register : process (
  clk, 
  rst, 
  enable_decoding_in
  )
begin
  if (rst = '1') then
    ttype_data_reg_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (enable_decoding_in = '0') then
      ttype_data_reg_i <= (others => '0');
    elsif (latch_tt_data_i = '1') then
      ttype_data_reg_i(9 downto 0) <= serial_tt_bits_i(9 downto 0);
    end if;
  end if;
end process tt_register;

-- Connect Data to Outputs
ttype_bits_out(9 downto 0) <= ttype_data_reg_i(9 downto 0);

end rtl; -- end code for trigger_signal_decoder
