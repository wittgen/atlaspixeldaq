-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: address_decoder_busbridge.vhd
-- Description:
--  Decode the dsp emi ea[21..2] or the rodbus[19..0] and enable leads 
--  into strobes for the various rod board and controller fpga devices,
--  as well as bridge the data bus to the appropriate resource.
--  
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- History:
--   20 Mar 2000 - MLN first version : Requirements and functionality
--                   understood and suficiently documented to proceed 
--                   with coding.
--   20 Mar 2001 - JMJ Set up ack logic to handle all 8 formatters
--   15 Feb 2002 - JMJ Changed the DSP HPI I/O to handle 32 bit transfers
--   12 Jul 2012 - AKU Formatter access extended via setup_fhw_rw_cycle
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
--------------------------------------------------------------------------
-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on

-- get rod bus addresses
use work.rodBusPackage.all;

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity address_decoder_busbridge is
  port (
    rst_n_in           : in    std_logic; -- asynchronous global reset
    clk_in             : in    std_logic; -- rodclk40 input

    dspbus_ea_in       : in    std_logic_vector(21 downto 2); --
    dspbus_ed_in       : in    std_logic_vector(31 downto 0); --
    dspbus_ed_out      : out   std_logic_vector(31 downto 0); --
    dspbus_en_n_in     : in    std_logic; -- MDSP CE_N
    dspbus_be_n_in     : in    std_logic_vector( 3 downto 0); -- MDSP Bytes Enables
    dspbus_rnw_in      : in    std_logic; -- Write Strobe from the MDSP
    dspbus_re_n_in     : in    std_logic; -- Read Enable from the MDSP
    dspbus_oe_n_in     : in    std_logic; -- Output Enable from the MDSP
    dspbus_ack_out     : out   std_logic; --

    rodbus_a_out       : out   std_logic_vector(19 downto 0); --
    rodbus_d_in        : in    std_logic_vector(31 downto 0); --
    rodbus_d_out       : out   std_logic_vector(31 downto 0); --
    rodbus_en_n_out    : out   std_logic_vector(10 downto 0); -- strobe/enable from the addr
    rodbus_en_n_itr    : out   std_logic_vector( 6 downto 0); -- strobe/enable from the addr
    rodbus_rnw_out     : out   std_logic; --

    formA_ack_n_in     : in    std_logic; --
    formB_ack_n_in     : in    std_logic; --
    efb_ack_in         : in    std_logic; --
    router_ack_in      : in    std_logic; --

    bocbus_busy_in     : in    std_logic;
    bocbus_strb_out    : out   std_logic;
    bocbus_rodoe_out   : out   std_logic;
    bocbus_write_out   : out   std_logic;

    regbus_a_out       : out   std_logic_vector(11 downto 0); -- rrif global registers bus
    regbus_d_in        : in    std_logic_vector(31 downto 0); --
    regbus_d_out       : out   std_logic_vector(31 downto 0); --
    regbus_rnw_out     : out   std_logic; --
    regbus_ack_in      : in    std_logic; --

    fcmbus_d_in        : in    std_logic_vector(31 downto 0);

    focbus_a_out       : out   std_logic_vector(11 downto 0); -- front end occupancy counter
    focbus_d_in        : in    std_logic_vector(31 downto 0); --
    focbus_rnw_out     : out   std_logic; --
    focbus_ack_in      : in    std_logic; --

    fmebus_a_out       : out   std_logic_vector( 7 downto 0); -- formatter modebitsmask enco
    fmebus_d_in        : in    std_logic_vector(15 downto 0); --
    fmebus_d_out       : out   std_logic_vector(15 downto 0); --
    fmebus_rnw_out     : out   std_logic; --
    fmebus_ack_in      : in    std_logic; --

    edebus_a_out       : out   std_logic_vector( 7 downto 0); --
    edebus_d_in        : in    std_logic_vector(15 downto 0); --
    edebus_d_out       : out   std_logic_vector(15 downto 0); -- 
    edebus_rnw_out     : out   std_logic; --
    edebus_ack_in      : in    std_logic; --
 
    dfibus_a_out       : out   std_logic_vector( 6 downto 0); -- debugmem fifo interface bus
    dfibus_rnw_out     : out   std_logic;
    dfibus_ack_in      : in    std_logic;
  
    fmt_hwob_out       : out   std_logic;
    fmt_ds_out         : out   std_logic;
    efb_hwob_out       : out   std_logic;

    extbus_ack_out     : out   std_logic; --
    intbus_ack_out     : out   std_logic
    );
end address_decoder_busbridge; 

architecture rtl of address_decoder_busbridge is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

--component icon
--  PORT (
--    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--end component;
--
--component ila
--  PORT (
--    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--    CLK : IN STD_LOGIC;
--    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--    TRIG1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--    TRIG2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0));
--end component;
--
--signal control:		std_logic_vector(35 downto 0);
--signal trig0:			std_logic_vector(31 downto 0);
--signal trig1:			std_logic_vector(31 downto 0);
--signal trig2:			std_logic_vector(31 downto 0);

signal dspbus_ack_int: std_logic;

signal vcc_i            : std_logic;

signal intbus_en_n_i    : std_logic_vector(15 downto 0);
signal extfifo_access_i : std_logic;
signal extfmt_access_i  : std_logic;
signal extefb_access_i  : std_logic;
signal extrtr_access_i  : std_logic;
signal extboc_access_i  : std_logic;

signal dspbus_rnw_in_i  : std_logic;
signal dspbus_en_n_in_i : std_logic;
signal dspbus_oe_n_in_i : std_logic;
signal dspbus_re_n_in_i : std_logic;
signal dspbus_be_n_in_i : std_logic_vector(3 downto 0);
signal dspbus_be_n_i    : std_logic;

signal rodbus_rnw_clr_i : std_logic;
signal rodbus_rnw_pre_i : std_logic;
signal rodbus_rw_en_i   : std_logic;
signal bocbus_en_i      : std_logic;

type boc_cycle_states is (
  idle, 
  boc_access, 
  setup0, 
  setup1, 
  strobe, 
  busy
  );
signal boc_cycle_state  : boc_cycle_states;



type rodbus_states is (
  idle,
  rb_access,
  setup_fhw_rw_cycle,
  extend_fhw_cycle,
  fhw_rw_cycle,
  latch_fhw,
  setup_shw_rw_cycle,
  shw_rw_cycle,
  latch_shw,
  done
  );
signal rodbus_access : rodbus_states;

signal rb_hwil_i    : std_logic;
signal rb_wr_data_i : std_logic_vector(31 downto 0);
signal rb_rd_data_i : std_logic_vector(31 downto 0);
signal fmt_en_i     : std_logic;
signal efb_en_i     : std_logic;

signal ack_fmt_i   : std_logic;
signal ack_formA_i : std_logic;
signal ack_formB_i : std_logic;
signal ack_efb_i   : std_logic;
signal ack_rtr_i   : std_logic;
signal ack_fifos_i : std_logic;
signal ack_boc_i   : std_logic;

signal bocbus_strb_int:		std_logic;
signal bocbus_rodoe_int: 	std_logic;
signal bocbus_write_int: 	std_logic;
signal bocbus_busy_sr :		std_logic_vector(2 downto 0) := "000";

signal rodbus_a_int:			std_logic_vector(19 downto 0);
signal rodbus_en_n_int:		std_logic_vector(10 downto 0);

signal regbus_d_out_int:	std_logic_vector(31 downto 0);
signal regbus_a_out_int:	std_logic_vector(11 downto 0);			
signal regbus_rnw_out_int:	std_logic;

-------------------------------------------------------------------------------
-- COMPONENT DECLARATION
-------------------------------------------------------------------------------

component FDCP
  port (
    Q   : out STD_LOGIC;
    D   : in  STD_LOGIC;
    C   : in  STD_LOGIC;
    CLR : in  STD_LOGIC;
    PRE : in  STD_LOGIC
    );
end component;

attribute box_type : string;
attribute box_type of FDCP: component is "user_black_box";

-------------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
-------------------------------------------------------------------------------

begin

--icon_instance : icon
--  port map (
--    CONTROL0 => control);
--	 
--ila_instance : ila
--  port map (
--    CONTROL => control,
--    CLK => clk_in,		-- 40 MHz
--    TRIG0 => trig0,
--    TRIG1 => trig1,
--    TRIG2 => trig2);
--	 
--trig0						<= regbus_d_in;
--trig1						<= regbus_d_out_int;
--trig2(11 downto 0)	<= regbus_a_out_int;
--trig2(12)				<= regbus_rnw_out_int;
--trig2(13)				<= regbus_ack_in;
--trig2(14)				<= intbus_en_n_i(4);

regbus_d_out			<= regbus_d_out_int;
regbus_a_out			<= regbus_a_out_int;
regbus_rnw_out			<= regbus_rnw_out_int;

--trig0(0)					<= bocbus_strb_int;
--trig0(1)					<= bocbus_rodoe_int;
--trig0(2)					<= bocbus_write_int;
--trig0(3)					<= ack_boc_i;
--trig0(4)					<= bocbus_busy_in;
--trig0(5)					<= bocbus_en_i;
--trig0(6)					<= rodbus_en_n_int(0);
--trig0(7)					<= rodbus_en_n_int(1);
--trig0(8)					<= rodbus_en_n_int(2);
--
--trig1(19 downto 0)	<= rodbus_a_int;
--
--trig2						<= rodbus_d_in;

bocbus_strb_out		<= bocbus_strb_int;
bocbus_rodoe_out		<= bocbus_rodoe_int;
bocbus_write_out		<= bocbus_write_int;

rodbus_a_out			<= rodbus_a_int;
rodbus_en_n_out		<= rodbus_en_n_int;


-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
-- Chip Enable Encoder
--   Combinatorial logic state machine used to enable the selected chip or
--   function on the ROD Board
chip_enable_encoder : process (
  rst_n_in, 
  dspbus_en_n_in, 
  dspbus_ea_in,
  dspbus_rnw_in,
  bocbus_busy_sr,
  bocbus_busy_in  
  )
--subtype select_map is std_logic_vector(9 downto 0);
subtype select_map is std_logic_vector(2 downto 0);
subtype select_loc is std_logic_vector(1 downto 0);
subtype select_int is std_logic_vector(2 downto 0);
subtype select_mem is std_logic_vector(1 downto 0);
begin
  if (rst_n_in = '0') then
    intbus_en_n_i <= (others => '1');
    extfifo_access_i <= '0';
    extfmt_access_i  <= '0';
    extefb_access_i  <= '0';
    extrtr_access_i  <= '0';
    extboc_access_i  <= '0';
  else
    if (dspbus_en_n_in = '0') then
      intbus_en_n_i <= (others => '1');
      extfifo_access_i <= '0';
      extfmt_access_i  <= '0';
      extefb_access_i  <= '0';
      extrtr_access_i  <= '0';
      extboc_access_i  <= '0';
		
		if dspbus_ea_in(BOC_SEL_SHIFT) = BOC_SEL_ADDR then					-- BOC registers
			intbus_en_n_i(11) <= bocbus_busy_sr(2); 
			extfifo_access_i  <= '0';
			extfmt_access_i   <= '0';
			extefb_access_i   <= '0';
			extrtr_access_i   <= '0';
			extboc_access_i   <= NOT bocbus_busy_sr(2);
		else														-- ROD registers
			case select_map(dspbus_ea_in(20 downto 18)) is
			  when SLV_NORTH_ADDR => 						-- slave north
				 intbus_en_n_i(0) <= '0';
				 extfifo_access_i <= '0';
				 extfmt_access_i  <= '1';
				 extefb_access_i  <= '0';
				 extrtr_access_i  <= '0';
				 extboc_access_i  <= '0';
			  when SLV_SOUTH_ADDR => 						-- slave south
				 intbus_en_n_i(1) <= '0';
				 extfifo_access_i <= '0';
				 extfmt_access_i  <= '1';
				 extefb_access_i  <= '0';
				 extrtr_access_i  <= '0';
				 extboc_access_i  <= '0';
			  when MASTER_INTERNAL_REG =>						-- master internal registers
				 intbus_en_n_i(4) <= '0';
			  when others =>                                                        
				 intbus_en_n_i <= (others => '1');
				 extfifo_access_i <= '0';
				 extfmt_access_i  <= '0';
				 extefb_access_i  <= '0';
				 extrtr_access_i  <= '0';
				 extboc_access_i  <= '0';
			end case;
		end if;
-- DAV commented (27/02/2014)
--		if dspbus_ea_in(21 downto 18) = BOC_SEL_ADDR then
--			intbus_en_n_i(11) <= bocbus_busy_sr(2); 
--			extfifo_access_i  <= '0';
--			extfmt_access_i   <= '0';
--			extefb_access_i   <= '0';
--			extrtr_access_i   <= '0';
--			extboc_access_i   <= NOT bocbus_busy_sr(2);
--
--		elsif dspbus_ea_in(21 downto 18) = ROD_SEL_ADDR then
--
--			case select_map(dspbus_ea_in(17 downto 12)) is
--			  when FMT_A_ADDR =>  -- Formatter Bank A Registers
--				 intbus_en_n_i(0) <= '0';
--				 extfifo_access_i <= '0';
--				 extfmt_access_i  <= '1';
--				 extefb_access_i  <= '0';
--				 extrtr_access_i  <= '0';
--				 extboc_access_i  <= '0';
--
--			  when FMT_B_ADDR =>  -- Formatter Bank B Registers
--				 intbus_en_n_i(1) <= '0';
--				 extfifo_access_i <= '0';
--				 extfmt_access_i  <= '1';
--				 extefb_access_i  <= '0';
--				 extrtr_access_i  <= '0';
--				 extboc_access_i  <= '0';
--
--			  when EFB_ADDR =>  -- EFB and Router Registers
--				 case select_loc(dspbus_ea_in(11 downto 10)) is
--					when "00"   => intbus_en_n_i(2) <= '0'; -- EFB
--										extefb_access_i  <= '1';
--					when "01"   => intbus_en_n_i(3) <= '0'; -- Router
--										extrtr_access_i  <= '1';
--					when others => intbus_en_n_i    <= (others => '1');
--				 end case;
--				 extfifo_access_i <= '0';
--				 extfmt_access_i  <= '0';
--				 extboc_access_i  <= '0';
--
--			  when INTERN_ADDR =>  -- ROD Controller Internal Registers
--				 if (dspbus_ea_in(11) = INT_RCF_OFFS(11)) then
--					case select_int(dspbus_ea_in(10 downto 8)) is
--	--              when "100" => intbus_en_n_i(4) <= '0'; -- Internal Registers
--					  when INT_RCF_OFFS(10 downto 8) => intbus_en_n_i(4) <= '0'; -- Internal Registers
--					  when INT_OCC_OFFS(10 downto 8) => intbus_en_n_i(5) <= '0'; -- FE Occupancy Counters
--					  when INT_LUT_OFFS(10 downto 8) => intbus_en_n_i(6) <= '0'; -- FE Command Mask LUT
--					  when INT_MSK_OFFS(10 downto 8) => intbus_en_n_i(7) <= '0'; -- EFB DynmicMask Encoder
--					  when others => intbus_en_n_i   <= (others => '1');
--					end case;
--				 else
--					intbus_en_n_i(8) <= '0'; -- Formatter ModeBits Encoder
--				 end if;
--				 extfifo_access_i <= '0';
--				 extfmt_access_i  <= '0';
--				 extefb_access_i  <= '0';
--				 extrtr_access_i  <= '0';
--				 extboc_access_i  <= '0';
--
--			  when EXTFIFO_ADDR =>  -- All external ROD Fifos
--				 case select_mem(dspbus_ea_in(8 downto 7)) is
--					when "00"   => intbus_en_n_i( 9) <= '0'; -- Inmem FIFOs
--										extfifo_access_i  <= '1';
--					when "10"   => intbus_en_n_i(10) <= '0'; -- Eventmem FIFOs
--										extfifo_access_i  <= '1';
--					when others => intbus_en_n_i     <= (others => '1');
--										extfifo_access_i  <= '0';
--				 end case;
--				 extfmt_access_i  <= '0';
--				 extefb_access_i  <= '0';
--				 extrtr_access_i  <= '0';
--				 extboc_access_i  <= '0';
--
--			  when others =>                                                        
--				 intbus_en_n_i <= (others => '1');
--				 extfifo_access_i <= '0';
--				 extfmt_access_i  <= '0';
--				 extefb_access_i  <= '0';
--				 extrtr_access_i  <= '0';
--				 extboc_access_i  <= '0';
--			end case;
--
--		else
--			intbus_en_n_i <= (others => '1');
--			extfifo_access_i <= '0';
--			extfmt_access_i  <= '0';
--			extefb_access_i  <= '0';
--			extrtr_access_i  <= '0';
--			extboc_access_i  <= '0';
--		end if;
			
    else
      intbus_en_n_i <= (others => '1');
      extfifo_access_i <= '0';
      extfmt_access_i  <= '0';
      extefb_access_i  <= '0';
      extrtr_access_i  <= '0';
      extboc_access_i  <= '0';
    end if;
  end if;
end process chip_enable_encoder; 

-------------------------------------------------------------------------------
--  Combinatorial Logic to Decode External RODBus CE Signals
--  (These signals are registered in this block)

--  This signal guarantees a falling and rising edge for each cycle  
--rodbus_rw_en_i <= (NOT dspbus_rnw_in_i OR NOT dspbus_oe_n_in_i);
rodbus_rw_en_i <= (NOT dspbus_rnw_in_i OR NOT dspbus_re_n_in_i);

--  BOC Bus Enable Signal
bocbus_en_i <= (NOT intbus_en_n_i(11) AND rodbus_rw_en_i);

--  External CEn Signals
rodbus_en_n_int(0) <= NOT (NOT intbus_en_n_i(11) AND rodbus_rw_en_i); -- BOC
-- rodbus_en_n_int(1) <= NOT (NOT intbus_en_n_i( 0) AND rodbus_rw_en_i); -- FormA		-- DAV commented NOT on 28/02/2014
-- rodbus_en_n_int(2) <= NOT (NOT intbus_en_n_i( 1) AND rodbus_rw_en_i); -- FormB		-- DAV commented NOT on 28/02/2014
-- rodbus_en_n_int(3) <= NOT (NOT intbus_en_n_i( 2) AND rodbus_rw_en_i); -- EFB			-- DAV commented NOT on 28/02/2014
-- rodbus_en_n_int(4) <= NOT (NOT intbus_en_n_i( 3) AND rodbus_rw_en_i); -- Router		-- DAV commented NOT on 28/02/2014
rodbus_en_n_int(1) <= (NOT intbus_en_n_i( 0) AND rodbus_rw_en_i); -- FormA
rodbus_en_n_int(2) <= (NOT intbus_en_n_i( 1) AND rodbus_rw_en_i); -- FormB
rodbus_en_n_int(3) <= (NOT intbus_en_n_i( 2) AND rodbus_rw_en_i); -- EFB
rodbus_en_n_int(4) <= (NOT intbus_en_n_i( 3) AND rodbus_rw_en_i); -- Router

rodbus_en_n_int(5) <= NOT (NOT intbus_en_n_i( 9) AND rodbus_rw_en_i); -- Inmem
rodbus_en_n_int(6) <= NOT (NOT intbus_en_n_i(10) AND rodbus_rw_en_i); -- Eventmem

--  Internal CEn Signals
rodbus_en_n_itr(0) <= NOT (NOT intbus_en_n_i( 4) AND rodbus_rw_en_i); -- Controller Internal Registers
rodbus_en_n_itr(1) <= NOT (NOT intbus_en_n_i( 5) AND rodbus_rw_en_i); -- FE Occupancy Counters
rodbus_en_n_itr(2) <= NOT (NOT intbus_en_n_i( 6) AND rodbus_rw_en_i); -- FE Command Mask LUT
rodbus_en_n_itr(3) <= NOT (NOT intbus_en_n_i( 7) AND rodbus_rw_en_i); -- EFB DynamicMask Encoder
rodbus_en_n_itr(4) <= NOT (NOT intbus_en_n_i( 8) AND rodbus_rw_en_i); -- Formatter Readout ModeBits Encoder
rodbus_en_n_itr(5) <= NOT (NOT intbus_en_n_i( 9) AND rodbus_rw_en_i); -- Inmem Control
rodbus_en_n_itr(6) <= NOT (NOT intbus_en_n_i(10) AND rodbus_rw_en_i); -- Eventmem Control


--  MDSP Byte Enable Signal
dspbus_be_n_i <= dspbus_be_n_in_i(3) AND
                 dspbus_be_n_in_i(2) AND
                 dspbus_be_n_in_i(1) AND
                 dspbus_be_n_in_i(0);

-- Formatter Enable Signal
fmt_en_i <= rodbus_rw_en_i AND (NOT intbus_en_n_i(0) OR NOT intbus_en_n_i(1));
-- EFB Enable Signal
efb_en_i <=  rodbus_rw_en_i AND NOT intbus_en_n_i(2);

-------------------------------------------------------------------------------
-- Data Mux
data_mux_logic : process (
  clk_in, 
  rst_n_in, 
  dspbus_en_n_in, 
  dspbus_ed_in,
  dspbus_rnw_in, 
  intbus_en_n_i,
  extfmt_access_i, 
  rodbus_d_in,
  regbus_d_in,
  edebus_d_in, 
  focbus_d_in,
  fcmbus_d_in,
  fmebus_d_in,
  rb_rd_data_i,
  rb_wr_data_i,
  rb_hwil_i
  )
begin
  if (rst_n_in = '0') then
    dspbus_ed_out <= (others => '0');                     
    rodbus_d_out  <= (others => '0');
  elsif (clk_in'event AND clk_in = '1') then
    dspbus_ed_out <= (others => '0');                     
    rodbus_d_out  <= (others => '0');                     
    if (dspbus_en_n_in = '0' AND dspbus_rnw_in = '1') then
      if (extfmt_access_i = '1' OR extefb_access_i = '1') then  -- Formatters & EFB
        dspbus_ed_out <= rb_rd_data_i;
      elsif (intbus_en_n_i(3) = '0') then  --  Router
        dspbus_ed_out <= "0000000000000000" & rodbus_d_in(15 downto 0);     
      elsif (intbus_en_n_i(4) = '0') then   -- ROD Controller Register Block
        dspbus_ed_out <= regbus_d_in;     
      elsif (intbus_en_n_i(5) = '0') then   -- FE Occupancy Counters
        dspbus_ed_out <= focbus_d_in;     
      elsif (intbus_en_n_i(6) = '0') then   -- FE Commmand Mask LUT
        dspbus_ed_out <= fcmbus_d_in;     
      elsif (intbus_en_n_i(7) = '0') then   -- EFB DynamicMask Encoder
        dspbus_ed_out(15 downto 0) <= edebus_d_in;	  
      elsif (intbus_en_n_i(8) = '0') then   -- Formatter ModeBits Encoder
        dspbus_ed_out(15 downto 0) <= fmebus_d_in;     
      elsif (intbus_en_n_i(11) = '0') then  --  BOC
        dspbus_ed_out <= "000000000000000000000000" & rodbus_d_in(7 downto 0);     
      else  -- Diagnostic FIFOs, 
        dspbus_ed_out <= rodbus_d_in;     
      end if;  
    elsif (dspbus_en_n_in = '0' AND dspbus_rnw_in = '0') then
      if (extfmt_access_i = '1' OR extefb_access_i = '1') then    -- Formatters & EFB
        if (rb_hwil_i = '0') then
          rodbus_d_out(15 downto 0) <= rb_wr_data_i(15 downto  0);
        else
          rodbus_d_out(15 downto 0) <= rb_wr_data_i(31 downto 16);
        end if;
      else  -- Diagnostic FIFOs, EFB, Router, BOC
        rodbus_d_out <= dspbus_ed_in;
      end if;  
   -- ROD Controller Register Block, FE Occ Counters, FE Commmand Mask LUT
      regbus_d_out_int <= dspbus_ed_in;
      edebus_d_out <= dspbus_ed_in(15 downto 0); -- EFB DynamicMask Encoder
      fmebus_d_out <= dspbus_ed_in(15 downto 0); -- Formatter ModeBits Encoder
    end if;
  end if;
end process data_mux_logic;

-------------------------------------------------------------------------------
-- Address Mux
-- Logic to map the correct address out to the ROD bus, and control the data
address_mux_logic : process (
  clk_in, 
  rst_n_in
  )
begin
  if (rst_n_in = '0') then
    rodbus_a_int <= (others => '0');
  elsif (clk_in'event AND clk_in = '1') then
      rodbus_a_int <= dspbus_ea_in(21 downto 2);
  end if;
end process address_mux_logic;

-------------------------------------------------------------------------------
--  At reset the rodbus_rnw_pre_i signal is high presetting the FF output 
--  to '1'.  After the reset cycle, preset is low allowing the FF to 
--  operate in it's normal mode.
--
-- The idea here is to get the address, output enable, rnw, etc. to the slave 
--  dsp's BEFORE the chip enable gets there by a controlled, consistent
--  (integer nunber of rod_clk) amount of time.
--

vcc_i <= '1';
rodbus_rnw_clr_i <= NOT (dspbus_en_n_in OR dspbus_rnw_in);  

rodbus_rnw_out_FF_logic : FDCP
  port map (
    Q   => rodbus_rnw_out,
    D   => vcc_i,
    C   => clk_in,
    CLR => rodbus_rnw_clr_i, 
    PRE => rodbus_rnw_pre_i
    );


-------------------------------------------------------------------------------
-- RodBus 32 bit Read/Write Interface
--   Synchronous logic state machine used to control the communication with the
--   the Formatters and EFB

rodbus_rw_interface : process (
  rst_n_in, 
  clk_in
  )
begin
  if (rst_n_in = '0') then
    fmt_ds_out    <= '0';
    fmt_hwob_out  <= '0';  -- Formatter Half Word Order Indicator
    efb_hwob_out  <= '0';
    rb_hwil_i     <= '0';
    ack_fmt_i     <= '0';
    rodbus_access <= idle;
  elsif (clk_in'event and clk_in = '1') then
    case rodbus_access is
      when idle =>
        fmt_ds_out   <= '0';
        fmt_hwob_out <= '0';  -- Formatter Half Word Order Indicator
        efb_hwob_out <= '0';
        ack_fmt_i    <= '0';
        rb_hwil_i    <= '0';
        if ((extfmt_access_i = '1' AND fmt_en_i = '1') OR
            (extefb_access_i = '1' AND efb_en_i = '1')) then
          rodbus_access <= rb_access;
        end if;

-- Set up for first half word access to the Formatters
     when rb_access =>  
        efb_hwob_out <= '0';  -- EFB Half Word Order Indicator
        fmt_hwob_out <= '0';  -- Formatter Half Word Order Indicator
        fmt_ds_out   <= '0';  -- Formatter Data Strobe
        ack_fmt_i    <= '0';
        rb_hwil_i    <= '0';

        if (dspbus_be_n_i = '0') then  -- Latch Write Data into Register
          rb_wr_data_i  <= dspbus_ed_in;
          rodbus_access <= setup_fhw_rw_cycle;
        end if;

      when setup_fhw_rw_cycle =>
          rodbus_access <= extend_fhw_cycle;  -- extend always
-- AKU        if (extefb_access_i = '1') then
-- AKU          rodbus_access <= extend_fhw_cycle;
-- AKU        else        
-- AKU          rodbus_access <= fhw_rw_cycle;
-- AKU        end if;

      when extend_fhw_cycle =>
          rodbus_access <= fhw_rw_cycle;
          rb_rd_data_i(15 downto 0) <= rodbus_d_in(15 downto 0); -- AKU this is redundant?

      when fhw_rw_cycle =>
        if (dspbus_rnw_in = '1') then
          rb_rd_data_i(15 downto 0) <= rodbus_d_in(15 downto 0);
          fmt_hwob_out <= '1';  -- Formatter Half Word Order Indicator
          rb_hwil_i    <= '1';
        else
          fmt_ds_out <= fmt_en_i;  --'1';  -- Formatter Data Strobe
        end if;
        efb_hwob_out <= '1';  -- EFB Half Word Order Indicator
        rodbus_access <= setup_shw_rw_cycle;

      when setup_shw_rw_cycle =>
        fmt_hwob_out <= '1';  -- Formatter Half Word Order Indicator
        fmt_ds_out   <= '0';  -- Formatter Data Strobe
        rb_hwil_i    <= '1';
        rodbus_access <= latch_shw;

      when latch_shw =>
        rodbus_access <= shw_rw_cycle;

      when shw_rw_cycle =>
        if (dspbus_rnw_in = '1') then
          rb_rd_data_i(31 downto 16) <= rodbus_d_in(15 downto 0);
        else
          fmt_ds_out <= fmt_en_i;  --'1';  -- Formatter Data Strobe
        end if;
        rodbus_access <= done;

      when done =>
        fmt_ds_out <= '0';  -- Formatter Data Strobe
        ack_fmt_i  <= '1';

        if (fmt_en_i = '0' AND efb_en_i = '0') then
          rodbus_access <= idle;
        end if;

      when others => rodbus_access <= idle;
    end case;
  end if;
end process rodbus_rw_interface;

-------------------------------------------------------------------------------
-- BOC Bus Read/Write Interface
--   Synchronous logic state machine used to control the communication with the
--   Back of Crate Card
boc_rw_interface : process (
  clk_in, 
  rst_n_in, 
  bocbus_busy_in, 
  dspbus_rnw_in
  )
begin
  if (rst_n_in = '0') then
    bocbus_strb_int  <= '1';
    bocbus_rodoe_int <= '1';
    bocbus_write_int <= '0';
    ack_boc_i        <= '0';
	bocbus_busy_sr		 <= "000";
    boc_cycle_state  <= idle;
  elsif (clk_in'event AND clk_in = '1') then
	-- shift in busy signal from BOC
	bocbus_busy_sr <= bocbus_busy_sr(1 downto 0) & bocbus_busy_in;

    case boc_cycle_state is
-- Wait for BOC Address match.
      when idle =>
        bocbus_strb_int  <= '1';
        bocbus_rodoe_int <= '1';
        ack_boc_i        <= '0';
        if (bocbus_en_i = '1') then
          boc_cycle_state <= boc_access;
        end if;
-- Test MDSP AWE_N and set the BOC Write Direction control signal if required.
        if (dspbus_rnw_in = '0') then
          bocbus_write_int <= '0'; -- AKU inverted to comply with IBL
		  else
			 bocbus_write_int <= '1';
        end if;

-- Start BOC RW Cycle. Set RODOE to enable the BOC data buffer chip, set up the
-- Address and Data and test bocbus_busy_in to see if the BOC is busy.
      when boc_access =>
        bocbus_rodoe_int <= '0';
        boc_cycle_state  <= setup0;

      when setup0 =>
        if (bocbus_busy_sr(2) = '0') then
          boc_cycle_state <= strobe;
        end if;

-- Send the start strobe to the BOC, when bocbus_busy_in transitions to '1' 
-- move to the next state.
      when strobe =>
        bocbus_strb_int  <= '0';
        if (bocbus_busy_sr(2) = '1') then
          boc_cycle_state <= busy;
        end if;

-- Clear STRB and RODOE to hold the valid data on the ROD Bus and the DSP EMIF.
-- When the cycle ends, go to idle
      when busy =>
        bocbus_strb_int  <= '1';
        bocbus_rodoe_int <= '1';
        ack_boc_i        <= '1';
        if (bocbus_en_i = '0') then
          boc_cycle_state <= idle;
        end if;

      when others =>
        bocbus_strb_int  <= '1';
        bocbus_rodoe_int <= '1';
        bocbus_write_int <= '0';
        ack_boc_i        <= '1';
        boc_cycle_state  <= idle;
    end case;
  end if;
end process boc_rw_interface;


-------------------------------------------------------------------------------
-- Logic to Register the signals into and out of this block
signal_register : process (
  clk_in, 
  rst_n_in
  )
begin
  if (rst_n_in = '0') then
    rodbus_rnw_pre_i <= '1';
    dspbus_rnw_in_i  <= '1';
    dspbus_en_n_in_i <= '1';
    dspbus_re_n_in_i <= '1';
    dspbus_be_n_in_i <= (others => '1');
    dspbus_oe_n_in_i <= '1';
    regbus_rnw_out_int   <= '1';
    focbus_rnw_out   <= '1';
    fmebus_rnw_out   <= '1';
    edebus_rnw_out   <= '1';
    dfibus_rnw_out   <= '1';
    ack_formA_i      <= '0';
    ack_formB_i      <= '0';
    ack_efb_i        <= '0';
    ack_rtr_i        <= '0';
    ack_fifos_i      <= '0';
  elsif (clk_in'event AND clk_in = '1') then
    rodbus_rnw_pre_i <= '0';
    dspbus_rnw_in_i  <= dspbus_rnw_in;
    dspbus_en_n_in_i <= dspbus_en_n_in;
    dspbus_re_n_in_i <= dspbus_re_n_in;
    dspbus_oe_n_in_i <= dspbus_oe_n_in;
    dspbus_be_n_in_i <= dspbus_be_n_in;
    regbus_rnw_out_int   <=  dspbus_rnw_in;
    focbus_rnw_out   <=  dspbus_rnw_in;
    fmebus_rnw_out   <=  dspbus_rnw_in;
    edebus_rnw_out   <=  dspbus_rnw_in;
    dfibus_rnw_out   <=  dspbus_rnw_in;

    regbus_a_out_int(11 downto 0) <= dspbus_ea_in(13 downto 2);
    focbus_a_out(11 downto 0) <= dspbus_ea_in(13 downto 2);
    fmebus_a_out(7 downto 0) <= dspbus_ea_in(9 downto 2);
    edebus_a_out(7 downto 0) <= dspbus_ea_in(9 downto 2);
    dfibus_a_out(6 downto 0) <= dspbus_ea_in(8 downto 2);

    ack_formA_i <= NOT formA_ack_n_in;
    ack_formB_i <= NOT formB_ack_n_in;
    ack_efb_i   <=     efb_ack_in;
    ack_rtr_i   <=     router_ack_in;
    ack_fifos_i <=     dfibus_ack_in;
  end if;
end process signal_register;

-------------------------------------------------------------------------------
--  Combinatorial Logic to Decode all of the Acknowledge Signals
--  (These signals are registered in this block)

ARDY_out: process(extfmt_access_i, extfifo_access_i, 
						ack_fmt_i, ack_fifos_i, extefb_access_i, ack_efb_i, 
						extrtr_access_i, ack_rtr_i, extboc_access_i, ack_boc_i)
begin
  if(extfifo_access_i = '1') then
    dspbus_ack_int <= ack_fifos_i;
  elsif(extfmt_access_i = '1') then
    dspbus_ack_int <= ack_fmt_i;
  elsif(extefb_access_i = '1') then
    dspbus_ack_int <= ack_efb_i;
  elsif(extrtr_access_i = '1') then
    dspbus_ack_int <= ack_rtr_i;
  elsif(extboc_access_i = '1') then
    dspbus_ack_int <= ack_boc_i;
  else           
    dspbus_ack_int <= '1';
  end if;
end process;    

dspbus_ack_out	<= dspbus_ack_int;
    
extbus_ack_out <= ack_formA_i OR
                  ack_formB_i OR
                  ack_efb_i   OR
                  ack_rtr_i   OR
                  ack_fifos_i OR
                  ack_boc_i;

intbus_ack_out <= regbus_ack_in OR
                  focbus_ack_in OR
                  fmebus_ack_in OR
                  edebus_ack_in;

end rtl; -- address_decoder_busbridge


