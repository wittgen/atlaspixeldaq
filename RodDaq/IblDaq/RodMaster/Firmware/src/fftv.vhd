-- 
-- Fixed-Frequency-Trigger Veto
-- 
-- Matthew Warren, HEP, University College London
--
-- June 2006
--
-- Incorporates 'Emergency Action'
--

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


entity fftv_main is
  generic(
  -- ROD firmware devs: Do not modify these generics - they work as is
    ROD_TRIGGER_MODE  : std_logic := '1';
    COUNT_WIDTH       : integer := 24; -- Considering veto's last for 1ms, this allows
    ID_WIDTH          : integer := 8;  -- best balance of ID's counted vs time/1ms counted (255/419)
    PERIOD_MIN        : integer := 85;
    PERIOD_MAX        : integer := 2666;
    PERIOD_OVERFLOW   : integer := 3076;
    PERIOD_DELTAT     : integer := 40;
    VETO_DURATION     : integer := 40000
    );

  port( 
    rst                   : in  std_logic;
    clk40                 : in  std_logic;
    fftv_match_lvl_i      : in  std_logic_vector (3 downto 0);
    fftv_match_lvl_o      : out std_logic_vector (3 downto 0);
    fftv_count_o          : out std_logic_vector ((COUNT_WIDTH-1) downto 0);
    fftv_count_clr_i      : in  std_logic;
    fftv_count_rollover_o : out std_logic;
    fftv_id_o             : out std_logic_vector ((ID_WIDTH-1) downto 0);
    fftv_id_clr_i         : in  std_logic;
    fftv_id_rollover_o    : out std_logic;
    fftv_emergency_o      : out std_logic;
    fftv_emergency_clr_i  : in  std_logic;
    trigger0_i            : in  std_logic;
    trigger0_o            : out std_logic;
    trigger1_i            : in  std_logic;
    trigger1_o            : out std_logic;
    fftv_busy_o           : out std_logic;
    fftv_disable_i        : in  std_logic
    );

-- Declarations

end fftv_main ;

-- hds interface_end

architecture rtl of fftv_main is

	signal clk 						: std_logic;
	signal p_buffer  				: std_logic_vector(15 downto 0);
	signal p_count  				: std_logic_vector(15 downto 0);

	signal p_count_min			: std_logic_vector(15 downto 0);
	signal p_count_max			: std_logic_vector(15 downto 0);
	signal p_count_oflow			: std_logic_vector(15 downto 0);
	signal p_under					: std_logic;
	signal p_top					: std_logic;
	signal p_over					: std_logic;
	signal p_oflow				   : std_logic;
	
	signal p_diff					: std_logic_vector(15 downto 0);
	signal p_deltat				: std_logic_vector(15 downto 0);
	signal p_deltat_max   	   : std_logic_vector(15 downto 0);
	
	signal p_match					: std_logic;
	signal p_match_count 		: std_logic_vector( 3 downto 0);
	signal p_match_count_max	: std_logic_vector( 3 downto 0);
	signal p_match_count_zero	: std_logic;
	
	signal trig						: std_logic;
	signal goodtrig				: std_logic;
	signal goodtrig_q				: std_logic;
	
	signal deltat_ok				: std_logic;
	signal veto_set				: std_logic;
	signal veto_set_q				: std_logic;
	signal veto						: std_logic;
	signal veto_q					: std_logic;
	signal veto_falling			: std_logic;
	signal veto_en					: std_logic;
	signal vetolen_count			: std_logic_vector(15 downto 0);
	signal vetolen_count_max	: std_logic_vector(15 downto 0);

	signal veto_count				: std_logic_vector((COUNT_WIDTH-1) downto 0);
	signal wr_reg_veto_count	: std_logic;
		
	signal fv_id					: std_logic_vector((ID_WIDTH-1) downto 0);
	signal rgw_fv_id				: std_logic;
	signal emergency				: std_logic;
	signal emergency_set			: std_logic;
	
	signal trig_q0					: std_logic_vector(2 downto 0);
	signal trig_q1					: std_logic_vector(2 downto 0);
	signal rod_trig_decoded 	: std_logic;

	

-------------------------------------------------------------------------------
--	MAIN CODE
-------------------------------------------------------------------------------

begin

	clk <= clk40;
		
	fftv_busy_o	     <= veto;
	fftv_emergency_o <= emergency or fftv_emergency_clr_i; -- _clr added to ensure it is not used to allow trigs by holding asserted
	
	fftv_count_o <= veto_count;
	fftv_id_o    <= fv_id;

   p_count_max       <= conv_std_logic_vector(PERIOD_MIN,	16);
   p_count_min       <= conv_std_logic_vector(PERIOD_MAX,	16);
   p_count_oflow  	 <= conv_std_logic_vector(PERIOD_OVERFLOW, 16);
   p_deltat_max   	 <= conv_std_logic_vector(PERIOD_DELTAT, 16);
   vetolen_count_max <= conv_std_logic_vector(VETO_DURATION, 16);
   
   
   -- Ensures we only get useful match values
   p_match_count_max <= "0010" when (fftv_match_lvl_i = "0010") else	-- 2
                        "0011" when (fftv_match_lvl_i = "0011") else	-- 3
                        "0100" when (fftv_match_lvl_i = "0100") else	-- 4
                        "0101" when (fftv_match_lvl_i = "0101") else	-- 5
                        "0110" when (fftv_match_lvl_i = "0110") else	-- 6 
								        "0111" when (fftv_match_lvl_i = "0111") else	-- 7
								        "1000" when (fftv_match_lvl_i = "1000") else	-- 8
								        "1001" when (fftv_match_lvl_i = "1001") else  -- 9
								        "1010" when (fftv_match_lvl_i = "1010") else  --10
								        "1010";                                       --10!
								
 	fftv_match_lvl_o <= p_match_count_max; -- for readback of actual value

	veto_en <= not(fftv_disable_i); -- or not(ALLOW_FFTV_DISABLE);

	rod_trig_decoder : process (clk)
	begin

		if	rising_edge(clk) then
			if	(rst = '1') or (veto_en = '0') then
				trig_q0 <= "000";
				trig_q1 <= "000";
				rod_trig_decoded <= '0';
								
			else
				trig_q0 <= trig_q0(1 downto 0) & trigger0_i;
				trig_q1 <= trig_q1(1 downto 0) & trigger1_i;
				
				--default
				rod_trig_decoded <= '0';
				
				if ((trigger0_i = '0') and (trig_q0 = "011")) OR
				   ((trigger1_i = '0') and (trig_q1 = "011")) then
					rod_trig_decoded <= '1';
				
				end if;
			end if;	
		end if;
	end process;
	
	
	trig 		 <= rod_trig_decoded 	when ROD_TRIGGER_MODE = '1' else (trigger0_i OR trigger1_i);
	trigger0_o <= trigger0_i and not(emergency or fftv_emergency_clr_i);
  trigger1_o <= trigger1_i and not(emergency or fftv_emergency_clr_i);


	-- Period Counter and flags
	------------------------------------------------------------
   p_under <= '1' when ((p_count < p_count_min) and (p_over = '0')) else '0';
   p_top   <= '1' when ((p_count = p_count_max) and (p_over = '0')) else '0';

   goodtrig <= trig and not(p_under);
   	
	period_counter : process (clk)
	begin

		if	rising_edge(clk) then
			if	(rst = '1') or (veto_en = '0') then
				p_count <= (others => '0');
				p_over  <= '0';
				p_oflow <= '0';
				
			else
				--defaults
				p_oflow <= '0';
			
				if (goodtrig = '1') then
					p_over <= '0';
					p_count <= (others => '0');
					
				elsif (p_top = '1') then
					p_over <= '1';
					p_count <= p_count + '1';					
					
				elsif (p_count = p_count_oflow) then
					p_oflow <= '1';
					p_count <= (others => '0');
					
				else
					p_count <= p_count + '1';					
	
				end if;
			end if;	
		end if;
	end process;


	-- Previous Period Buffer
	------------------------------------------------------------
	period_buffer : process (clk)
	begin

		if	rising_edge(clk) then                  
			if	(rst = '1') or (veto_en = '0') or (veto_falling = '1') then  --*** added to ensure we get a proper match after a veto
				p_buffer <= (others => '0');

			elsif (goodtrig = '1') then
				p_buffer <= p_count;
				
			end if;
		end if;
	end process;


	-- Period Match Count
	-----------------------------------------------------------
	-- lots of combinatorial, let's hope it fits nicely!

	
	
	p_deltat	<= (p_buffer - p_count) when (p_buffer > p_count) 
			 else (p_count - p_buffer);
	
	p_match	<= '1' 		when (p_deltat <= p_deltat_max) 	else '0';
	
	p_match_count_zero <= '1' when (p_match_count = 0) else '0';

	deltat_ok <= p_match and goodtrig;
		
	period_match_counter : process (clk)
	begin
		if	rising_edge(clk) then

			if	(rst = '1') or (veto_en = '0') then
				p_match_count <= (others => '0');

			elsif (goodtrig = '1') then
			
				if (p_match = '1') and (p_oflow = '0') then
  					p_match_count <= p_match_count + '1';
				
				elsif (p_match_count_zero = '0') then
					p_match_count <= p_match_count - '1';
				
				end if;
				
			-- decriments match_count everytime period counter overflows!
			elsif (p_oflow = '1') and (p_match_count_zero = '0') then
				p_match_count <= p_match_count - '1';
						
			end if;
		end if;
	end process;
	


	-- Generater Veto
	------------------------------------------------------------------
	veto_set_gen : process (clk)
	begin
		if	rising_edge(clk) then
			if	(rst = '1')  or (veto_en = '0') then
				goodtrig_q 		<= '0';
				veto_set   		<= '0';
				veto_set_q		<= '0';
				emergency_set	<= '0';
				
			else
				goodtrig_q 		<= goodtrig;
				veto_set 		<= '0';
				veto_set_q		<= veto_set;
				emergency_set	<= '0';
				
				if  (goodtrig_q = '1') then
					if (p_match_count = p_match_count_max) then
						veto_set <= '1';

					end if;
					
					if (p_match_count > p_match_count_max) then
						emergency_set <= '1';

					end if;

				end if;
			end if;
		end if;
	end process;
	
	

	-- Generater long veto signal
	--------------------------------------------------------
	veto_signal : process (clk)
	begin

		if	rising_edge(clk) then
			veto_falling <= '0';
		
			if	(rst = '1') or (veto_en = '0') then
				veto 				<= '0';
				veto_q 			<= '0';
				vetolen_count 	<= (others => '0');

			else
			
				veto_q <= veto;
							
				if (veto_set = '1') then
					veto <= '1';
					veto_falling <= '0';
					vetolen_count <= (others => '0');

				elsif (vetolen_count = vetolen_count_max ) then
					veto <= '0';
					veto_falling <= '1';
					vetolen_count <= (others => '0');

				elsif (veto = '1') then
				   veto <= '1';
					veto_falling <= '0';
					vetolen_count <= vetolen_count + '1';

				else
					veto <= '0';
					veto_falling <= '0';
					vetolen_count <= (others => '0');
				
				end if;
			end if;
		end if;
	end process;
	
	-- Emergency
	----------------------------------------------------------
	
	emergency_gen : process (clk)
	begin
		if	rising_edge(clk) then
			if	(rst = '1') then
				emergency <= '0';

			else
				if (fftv_emergency_clr_i = '1') and (veto = '0') then -- ensure it's not reset for duration of at least 1 veto
					emergency <= '0';
					
				elsif (emergency_set = '1') or (emergency = '1') then
					emergency <= '1';
				
				end if;
			end if;
		end if;
	end process;

	
	-- FFVeto Clocks Counter
	----------------------------------------------------------
	
	veto_counter : process (clk)
	begin

		if	rising_edge(clk) then
			if	(rst = '1') or (fftv_count_clr_i = '1') then
				veto_count <= (others => '0');
				fftv_count_rollover_o <= '0';
				
			else
			
				if (veto = '1') then 
					veto_count <= veto_count + '1';
				end if;
							
				if (veto_q = '1') and (veto_count = 0) then
					fftv_count_rollover_o <= '1';
				
				end if;			
			end if;
		end if;
	end process;
	
		
	-- FV ID Counter
	----------------------------------------------------------
		
	fv_id_counter : process (clk)
	begin

		if	rising_edge(clk) then
			if	(rst = '1') or (fftv_id_clr_i = '1') then
				fv_id <= (others => '0');
				fftv_id_rollover_o <= '0';
				
			else
				if (veto_set = '1') then 
					fv_id <= fv_id + '1';
					
				end if;
				
				if (veto_set_q = '1') and (fv_id = 0) then
					fftv_id_rollover_o <= '1';
				
				end if;			
			end if;
		end if;
	end process;

end rtl;

-------------------------------------------------------------------------------
