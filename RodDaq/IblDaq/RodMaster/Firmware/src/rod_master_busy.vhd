----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Malte Backhaus
-- 
-- Create Date:    11:47:58 03/28/2014 
-- Design Name: 
-- Module Name:    rod_master_busy - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM; 
--use UNISIM.VComponents.all;

entity rod_master_busy is
    Port ( clk40in : in  STD_LOGIC;
           rod_busy_n_in_p : in  STD_LOGIC_VECTOR (1 downto 0);
           efb_header_pause_i : in  STD_LOGIC;
           formatter_mb_fef_n_in_p : in  STD_LOGIC_VECTOR (1 downto 0);
           spare_ctrl_reg_i : in  STD_LOGIC; 
           rol_test_pause_i : in  STD_LOGIC;
	   reset : in  STD_LOGIC;
	   reset_hists : in  STD_LOGIC;
     	   busy_mask : in STD_LOGIC_VECTOR (7 downto 0);
	   force_input : in STD_LOGIC_VECTOR (7 downto 0);
           rod_busy_n_out_p : out  STD_LOGIC;
           current_busy_status : out  STD_LOGIC_VECTOR (7 downto 0);
           rod_busy_n_in_p_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rod_busy_n_in_p_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           formatter_mb_fef_n_in_p_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           formatter_mb_fef_n_in_p_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           efb_header_pause_i_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rol_test_pause_i_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rod_busy_n_out_p_hist : out  STD_LOGIC_VECTOR (15 downto 0));
end rod_master_busy;

architecture Behavioral of rod_master_busy is

signal rod_busy_n_out_p_i : STD_LOGIC;

signal rod_busy_n_out_p_hist_i : unsigned (15 DOWNTO 0);
signal rod_busy_n_in_p_0_hist_i : unsigned (15 DOWNTO 0);
signal rod_busy_n_in_p_1_hist_i : unsigned (15 DOWNTO 0);
signal formatter_mb_fef_n_in_p_0_hist_i : unsigned (15 DOWNTO 0);
signal formatter_mb_fef_n_in_p_1_hist_i : unsigned (15 DOWNTO 0);
signal efb_header_pause_i_hist_i : unsigned (15 DOWNTO 0);
signal rol_test_pause_i_hist_i : unsigned (15 DOWNTO 0);

signal rod_busy_in_p_masked: STD_LOGIC_VECTOR (1 downto 0);
signal efb_header_pause_i_masked: STD_LOGIC;
signal formatter_mb_fef_in_p_masked: STD_LOGIC_VECTOR (1 downto 0);
signal rol_test_pause_i_masked: STD_LOGIC;

signal rod_busy_in_p: STD_LOGIC_VECTOR (1 downto 0);
signal formatter_mb_fef_in_p: STD_LOGIC_VECTOR (1 downto 0);

signal force_rod_busy_masked: STD_LOGIC;
signal rod_busy_n_out_p_A: STD_LOGIC;
signal rod_busy_n_out_p_B: STD_LOGIC;
signal rod_busy_in_p_masked_0_A: STD_LOGIC;
signal rod_busy_in_p_masked_0_B: STD_LOGIC;
signal rod_busy_in_p_masked_1_A: STD_LOGIC;
signal rod_busy_in_p_masked_1_B: STD_LOGIC;
signal efb_header_pause_i_masked_A: STD_LOGIC;
signal efb_header_pause_i_masked_B: STD_LOGIC;
signal formatter_mb_fef_in_p_masked_0_A: STD_LOGIC;
signal formatter_mb_fef_in_p_masked_0_B: STD_LOGIC;
signal formatter_mb_fef_in_p_masked_1_A: STD_LOGIC;
signal formatter_mb_fef_in_p_masked_1_B: STD_LOGIC;
signal rol_test_pause_i_masked_A: STD_LOGIC;
signal rol_test_pause_i_masked_B: STD_LOGIC;





begin

-- convert active low inputs to active high
rod_busy_in_p(0) 		<= NOT rod_busy_n_in_p(0);
rod_busy_in_p(1) 		<= NOT rod_busy_n_in_p(1);
formatter_mb_fef_in_p(0)	<= NOT formatter_mb_fef_n_in_p(0);
formatter_mb_fef_in_p(1)	<= NOT formatter_mb_fef_n_in_p(1);

-- actual busy logic here. All signals ORed below
force_rod_busy_masked <= force_input(0) AND NOT busy_mask(0); 

rod_busy_in_p_masked(0) <= (rod_busy_in_p(0) AND NOT busy_mask(1)) OR force_input(1); 

rod_busy_in_p_masked(1) <= (rod_busy_in_p(1) AND NOT busy_mask(2)) OR force_input(2); 

efb_header_pause_i_masked <= (efb_header_pause_i AND NOT busy_mask(3)) OR force_input(3); 

formatter_mb_fef_in_p_masked(0) <= (
	((formatter_mb_fef_in_p(0) AND NOT busy_mask(4)) OR force_input(4))
	 AND 
	((spare_ctrl_reg_i AND NOT busy_mask(7)) OR force_input(7))
); 

formatter_mb_fef_in_p_masked(1) <= (
	((formatter_mb_fef_in_p(1) AND NOT busy_mask(5)) OR force_input(5))
	 AND 
	((spare_ctrl_reg_i AND NOT busy_mask(7)) OR force_input(7))
);

rol_test_pause_i_masked <= (rol_test_pause_i AND NOT busy_mask(6)) OR force_input(6);



	check_rising_edge0: process (clk40in, reset)
	begin
		if(reset = '1') then
			rod_busy_n_out_p_A <= '0';
			rod_busy_n_out_p_B <= '0';	
		elsif rising_edge (clk40in) then
			rod_busy_n_out_p_A <= rod_busy_n_out_p_i;
			rod_busy_n_out_p_B <= rod_busy_n_out_p_A;
		end if;	
	end process check_rising_edge0;
	
	check_rising_edge1: process (clk40in, reset)
	begin
		if(reset = '1') then
			rod_busy_in_p_masked_0_A <= '0';
			rod_busy_in_p_masked_0_B <= '0';
		elsif rising_edge (clk40in) then
			rod_busy_in_p_masked_0_A <= rod_busy_in_p_masked(0);
			rod_busy_in_p_masked_0_B <= rod_busy_in_p_masked_0_A;
		end if;	
	end process check_rising_edge1;

	check_rising_edge2: process (clk40in, reset)
	begin
		if(reset = '1') then
			rod_busy_in_p_masked_1_A <= '0';
			rod_busy_in_p_masked_1_B <= '0';
		elsif rising_edge (clk40in) then
			rod_busy_in_p_masked_1_A <= rod_busy_in_p_masked(1);
			rod_busy_in_p_masked_1_B <= rod_busy_in_p_masked_1_A;
		end if;	
	end process check_rising_edge2;
	
	check_rising_edge3: process (clk40in, reset)
	begin
		if(reset = '1') then
			efb_header_pause_i_masked_A <= '0';
			efb_header_pause_i_masked_B <= '0';
		elsif rising_edge (clk40in) then
			efb_header_pause_i_masked_A <= efb_header_pause_i_masked;
			efb_header_pause_i_masked_B <= efb_header_pause_i_masked_A;
		end if;
	end process check_rising_edge3;
	
	check_rising_edge4: process (clk40in, reset)
	begin
		if(reset = '1') then
			formatter_mb_fef_in_p_masked_0_A <= '0';
			formatter_mb_fef_in_p_masked_0_B <= '0';
		elsif rising_edge (clk40in) then
			formatter_mb_fef_in_p_masked_0_A <= formatter_mb_fef_in_p_masked(0);
			formatter_mb_fef_in_p_masked_0_B <= formatter_mb_fef_in_p_masked_0_A;
		end if;
	end process check_rising_edge4;
	
	check_rising_edge5: process (clk40in, reset)
	begin
		if(reset = '1') then
			formatter_mb_fef_in_p_masked_1_A <= '0';
			formatter_mb_fef_in_p_masked_1_B <= '0';
		elsif rising_edge (clk40in) then
			formatter_mb_fef_in_p_masked_1_A <= formatter_mb_fef_in_p_masked(1);
			formatter_mb_fef_in_p_masked_1_B <= formatter_mb_fef_in_p_masked_1_A;
		end if;	
	end process check_rising_edge5;
	
	check_rising_edge6: process (clk40in, reset)
	begin
		if(reset = '1') then
			rol_test_pause_i_masked_A <= '0';
			rol_test_pause_i_masked_B <= '0';
		elsif rising_edge (clk40in) then
			rol_test_pause_i_masked_A <= rol_test_pause_i_masked;
			rol_test_pause_i_masked_B <= rol_test_pause_i_masked_A;
		end if;	
	end process check_rising_edge6;
	

  busy_out_process: process(reset, reset_hists, force_rod_busy_masked, rod_busy_in_p_masked, efb_header_pause_i_masked, 
  formatter_mb_fef_in_p_masked, rol_test_pause_i_masked)
  begin
    if(reset = '1') then
      rod_busy_n_out_p    <= '0';
		rod_busy_n_out_p_i  <= '0';
		current_busy_status <= "00000000";
		
    else	 
		
      rod_busy_n_out_p <= NOT(
			  force_rod_busy_masked OR -- force busy functionality (set via mask(0))
			  rod_busy_in_p_masked(1) OR -- input from slave
                          rod_busy_in_p_masked(0) OR -- input from slave
                          efb_header_pause_i_masked OR  
                          formatter_mb_fef_in_p_masked(0) OR
			  formatter_mb_fef_in_p_masked(1) OR
                          rol_test_pause_i_masked -- Required when TIM issues RoL triggers 
			  );
								  
      rod_busy_n_out_p_i <= NOT(
			    force_rod_busy_masked OR -- force busy functionality (set via mask(0))
			    rod_busy_in_p_masked(1) OR -- input from slave
                            rod_busy_in_p_masked(0) OR -- input from slave
                            efb_header_pause_i_masked OR  
                            formatter_mb_fef_in_p_masked(0) OR
			    formatter_mb_fef_in_p_masked(1) OR
			    rol_test_pause_i_masked -- Required when TIM issues RoL triggers
									 );
								  
		current_busy_status(0) <= rod_busy_n_out_p_i;
		current_busy_status(1) <= rod_busy_in_p(0);-- OR force_input(1);
		current_busy_status(2) <= rod_busy_in_p(1);-- OR force_input(2);
		current_busy_status(3) <= efb_header_pause_i;-- OR force_input(3);
		current_busy_status(4) <= formatter_mb_fef_in_p(0);-- OR force_input(4);
		current_busy_status(5) <= formatter_mb_fef_in_p(1);-- OR force_input(5);
		current_busy_status(6) <= rol_test_pause_i;-- OR force_input(6);
		current_busy_status(7) <= spare_ctrl_reg_i;-- OR force_input(7);
										  
	
	end if;
end process busy_out_process;
	 

-- histogramming of busy sources
busy_hist_process: process(clk40in, reset, reset_hists)
  begin
    if(reset = '1' OR reset_hists = '1') then
      rod_busy_n_in_p_0_hist_i <= "0000000000000000";
		rod_busy_n_in_p_1_hist_i <= "0000000000000000";
		formatter_mb_fef_n_in_p_0_hist_i <= "0000000000000000";
		formatter_mb_fef_n_in_p_1_hist_i <= "0000000000000000";
		efb_header_pause_i_hist_i <= "0000000000000000";
		rol_test_pause_i_hist_i <= "0000000000000000";
		rod_busy_n_out_p_hist_i <= "0000000000000000";
	
	elsif rising_edge (clk40in) then 
			if ((rod_busy_n_out_p_A = '0') AND (rod_busy_n_out_p_B = '1')) then	
				rod_busy_n_out_p_hist_i <= rod_busy_n_out_p_hist_i + "0000000000000001";
			end if;
			
			if ((rod_busy_in_p_masked_0_A = '1') AND (rod_busy_in_p_masked_0_B = '0')) then	
				rod_busy_n_in_p_0_hist_i <= rod_busy_n_in_p_0_hist_i + "0000000000000001";	
			end if;	
		
			if ((rod_busy_in_p_masked_1_A = '1') AND (rod_busy_in_p_masked_1_B = '0')) then
				rod_busy_n_in_p_1_hist_i <= rod_busy_n_in_p_1_hist_i + "0000000000000001";
			end if;	
  
			if  ((efb_header_pause_i_masked_A = '1') AND (efb_header_pause_i_masked_B = '0')) then
				efb_header_pause_i_hist_i <= efb_header_pause_i_hist_i + "0000000000000001";
			end if;	
		
			if  ((formatter_mb_fef_in_p_masked_0_A = '1') AND (formatter_mb_fef_in_p_masked_0_B = '0')) then
				formatter_mb_fef_n_in_p_0_hist_i <= formatter_mb_fef_n_in_p_0_hist_i + "0000000000000001";
			end if;	

			if  ((formatter_mb_fef_in_p_masked_1_A = '1') AND (formatter_mb_fef_in_p_masked_1_B = '0')) then
				formatter_mb_fef_n_in_p_1_hist_i <= formatter_mb_fef_n_in_p_1_hist_i + "0000000000000001";		
			end if;	

			if  ((rol_test_pause_i_masked_A = '1') AND (rol_test_pause_i_masked_B = '0')) then
				rol_test_pause_i_hist_i <= rol_test_pause_i_hist_i + "0000000000000001";			
			end if;
  end if;
end process busy_hist_process;	
	
busy_hists_out_process: process(reset, reset_hists, rod_busy_n_in_p_0_hist_i, rod_busy_n_in_p_1_hist_i, 
formatter_mb_fef_n_in_p_0_hist_i, formatter_mb_fef_n_in_p_1_hist_i, efb_header_pause_i_hist_i,
rol_test_pause_i_hist_i, rod_busy_n_out_p_hist_i)
  
 begin	
	 if((reset_hists = '1') OR (reset = '1')) then
		rod_busy_n_in_p_0_hist <= "0000000000000000";
		rod_busy_n_in_p_1_hist <= "0000000000000000";
		formatter_mb_fef_n_in_p_0_hist <= "0000000000000000";
		formatter_mb_fef_n_in_p_1_hist <= "0000000000000000";
		efb_header_pause_i_hist <= "0000000000000000";
		rol_test_pause_i_hist <= "0000000000000000";
		rod_busy_n_out_p_hist <= "0000000000000000";
		
    else	 						  
		rod_busy_n_in_p_0_hist <= std_logic_vector(rod_busy_n_in_p_0_hist_i);
		rod_busy_n_in_p_1_hist <= std_logic_vector(rod_busy_n_in_p_1_hist_i);
		formatter_mb_fef_n_in_p_0_hist <= std_logic_vector(formatter_mb_fef_n_in_p_0_hist_i);
		formatter_mb_fef_n_in_p_1_hist <= std_logic_vector(formatter_mb_fef_n_in_p_1_hist_i);
		efb_header_pause_i_hist <= std_logic_vector(efb_header_pause_i_hist_i);
		rol_test_pause_i_hist <= std_logic_vector(rol_test_pause_i_hist_i);
		rod_busy_n_out_p_hist <= std_logic_vector(rod_busy_n_out_p_hist_i);
		
	end if;
end process busy_hists_out_process;	
	
end Behavioral;

