-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: front_end_occupancy_counters.vhd
-- Description: Track the number of triggers in the front end modules.
--
-------------------------------------------------------------------------------
-- Structure: 
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author:         Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    Wednesday 1 March 2000 - First version
--    14 Feb 2002 - JMJ Changed the mapping of the counters to pack 7 links
--                      per address location.
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
-------------------------------------------------------------------------------
-- Notes
-- FE Occupancy Counter Mapping
--  SCT Map  
--    Links from 1 module must be in the same group
--  FE Cmd  0 to 23 == FE Links  0 to 47
--  FE Cmd 24 to 47 == FE Links 48 to 95
--
-- Pixel Map
--  FE Cmd 0 to 47 == FE Links 0 to 47
--
-- IBL Map
--  FE Cmd 0 to 31 == FE Links 0 to 31
--
-- Group Zero Out = fe_occupancy_zero_i(j) AND group_mask_i(j)
-- j == highest link value
-- Group Enable

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
-------------------------------------------------------------------------------
-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on
-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
--
entity front_end_occupancy_counters is
  port (
    clk                           : in    std_logic;
    rst                           : in    std_logic; -- asyncronous global reset
    trailerflags_readout_en_in    : in    std_logic; -- enable trailer flags readout/counter
    trailerflags_in               : in    std_logic_vector(  3 downto 0); -- 4 FE/formatter in IBL, 12 FE/formatter in Pixel
    clear_fe_occupancy_counter_in : in    std_logic_vector( 31 downto 0); -- individual counter reset, 96 bits for Pixel
    num_l1_value_in               : in    std_logic_vector(103 downto 0); -- num_accept is 4 bits for each FE, total 32 FE modules , needs to be 128 bits wide fix
    fe_l1_pulse_in                : in    std_logic_vector( 31 downto 0); -- 48 bits for Pixel
    fe_occupancy_gl_zero_out      : out   std_logic; -- global zero out
--    group_zero_out                : out   std_logic_vector(  3 downto 0); --
--    group_en_out                  : out   std_logic_vector(  3 downto 0); --
    show_trailer_flags_out        : out   std_logic_vector(  7 downto 0); -- eight formatters in IBL
    focbus_a_in                   : in    std_logic_vector( 11 downto 0);
    focbus_d_in                   : in    std_logic_vector( 31 downto 0);
    focbus_d_out                  : out   std_logic_vector( 31 downto 0);
    focbus_rnw_in                 : in    std_logic;
    focbus_en_in                  : in    std_logic;
    focbus_ack_out                : out   std_logic
    );
end front_end_occupancy_counters;

architecture rtl of front_end_occupancy_counters is
--------------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------------
type trailer_flag_states is (
  reset, 
  idle, 
  readout_fmt0, 
  readout_fmt1, 
  readout_fmt2,
  readout_fmt3, 
  readout_fmt4, 
  readout_fmt5, 
  readout_fmt6, 
  readout_fmt7
  );
signal pres_state : trailer_flag_states;

type ack_states is (
  reset, 
  wait_0_clk, 
  wait_1_clk, 
  acknowledge
  );
signal acknowledge_cycle_state : ack_states;

signal trailer_reg0_i : std_logic_vector(3 downto 0);
signal trailer_reg1_i : std_logic_vector(3 downto 0);
signal trailer_reg2_i : std_logic_vector(3 downto 0);
signal trailer_reg3_i : std_logic_vector(3 downto 0);
signal trailer_reg4_i : std_logic_vector(3 downto 0);
signal trailer_reg5_i : std_logic_vector(3 downto 0);
signal trailer_reg6_i : std_logic_vector(3 downto 0);
signal trailer_reg7_i : std_logic_vector(3 downto 0);

signal fmt_writes_trailer_i : std_logic_vector(31 downto 0); --
signal clr_fe_occ_count_i   : std_logic_vector(31 downto 0); --

signal trlr_reg_en_i  : std_logic_vector(7 downto 0);
--signal trlr_reg_clr_i : std_logic_vector(7 downto 0);

type array32of4bitunsigned is array (0 to 31) of unsigned(3 downto 0);
signal fe_occupancy_count_i : array32of4bitunsigned; --
signal fe_occupancy_zero_i : std_logic_vector(31 downto 0);
signal num_l1_value_in_i : unsigned(127 downto 0); --




-- for internal scan engine, not needed yet
--type array8of6bitunsigned is array (0 to 7) of unsigned(5 downto 0);
--signal group_mask_i : array8of6bitunsigned;
--signal group_en_i : std_logic_vector(3 downto 0);
--type array16ofinteger is array (0 to 15) of integer range 0 to 63;
--signal grp_link_pointer : array16ofinteger;

--------------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------------

component fe_trigger_counter
  port (
    clk                   : in  std_logic;
    clear_counter_in      : in  std_logic; -- 
--    rod_type_in           : in  std_logic;
    num_l1_value_in       : in  unsigned(3 downto 0); --
    fe_command_pulse_in   : in  std_logic; --
    fmt_writes_trailer_in : in  std_logic; --
    occupancy_count_out   : out unsigned(3 downto 0)
   );
end component;

--component fe_trigger_counter_sct
--  port(
--    clk_in                : in  std_logic; -- clk40 input
--    clear_counter_in      : in  std_logic_vector(1 downto 0); --
--    fe_command_pulse_in   : in  std_logic; --
--    fmt_writes_trailer_in : in  std_logic_vector(1 downto 0); --
--    occupancy_count_out   : out unsigned(7 downto 0)
--    );
--end component;

begin
--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
-- FE Counter Blocks
gen_feocc_blocks:
  for i in 0 to 31 generate
	 feocc_counter : fe_trigger_counter
	   port map (
		  clk                   => clk,
		  clear_counter_in      => clr_fe_occ_count_i(i),
		  num_l1_value_in       => num_l1_value_in_i((4*i+3) downto (4*i)),
		  fe_command_pulse_in   => fe_l1_pulse_in(i),
		  fmt_writes_trailer_in => fmt_writes_trailer_i(i),
		  occupancy_count_out   => fe_occupancy_count_i(i)
	   );	
  end generate;

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------

num_l1_value_in_i(103 downto 0) <= unsigned(num_l1_value_in); -- need fix

--------------------------------------------------------------------------------
-- Formatter writes trailer pulse counter/decoder and control state machine 
--------------------------------------------------------------------------------
counter0 : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    trailer_reg0_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(0) = '0') then -- THIS WAS A BUG IN PIXEL!?!?!?
      trailer_reg0_i <= (others => '0');
    else
      trailer_reg0_i <= trailerflags_in;
    end if;
  end if; 
end process counter0;

counter1 : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    trailer_reg1_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(1) = '0') then 
      trailer_reg1_i <= (others => '0');
    else
      trailer_reg1_i <= trailerflags_in;
    end if;
  end if; 
end process counter1;

counter2 : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    trailer_reg2_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(2) = '0') then
      trailer_reg2_i <= (others => '0');
    else
      trailer_reg2_i <= trailerflags_in;
    end if;
  end if; 
end process counter2;

counter3 : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    trailer_reg3_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(3) = '0') then 
      trailer_reg3_i <= (others => '0');
    else
      trailer_reg3_i <= trailerflags_in;
    end if;
  end if; 
end process counter3;

counter4 : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    trailer_reg4_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(4) = '0') then
      trailer_reg4_i <= (others => '0');
    else
      trailer_reg4_i <= trailerflags_in;
    end if;
  end if; 
end process counter4;

counter5 : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    trailer_reg5_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(5) = '0') then 
      trailer_reg5_i <= (others => '0');
    else
      trailer_reg5_i <= trailerflags_in;
    end if;
  end if; 
end process counter5;

counter6 : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    trailer_reg6_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(6) = '0') then 
      trailer_reg6_i <= (others => '0');
    else
      trailer_reg6_i <= trailerflags_in;
    end if;
  end if; 
end process counter6;

counter7 : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    trailer_reg7_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (trlr_reg_en_i(7) = '0') then 
      trailer_reg7_i <= (others => '0');
    else
      trailer_reg7_i <= trailerflags_in;
    end if;
  end if; 
end process counter7;

fmt_writes_trailer_mux : process (
  trailer_reg0_i,
  trailer_reg1_i,
  trailer_reg2_i,
  trailer_reg3_i,
  trailer_reg4_i,
  trailer_reg5_i,
  trailer_reg6_i,
  trailer_reg7_i
  )
begin
  for j in 0 to 3 loop
	 fmt_writes_trailer_i(j   ) <= trailer_reg0_i(j);
	 fmt_writes_trailer_i(j+ 4) <= trailer_reg1_i(j);
	 fmt_writes_trailer_i(j+ 8) <= trailer_reg2_i(j);
	 fmt_writes_trailer_i(j+12) <= trailer_reg3_i(j);
	 fmt_writes_trailer_i(j+16) <= trailer_reg4_i(j);
	 fmt_writes_trailer_i(j+20) <= trailer_reg5_i(j);
	 fmt_writes_trailer_i(j+24) <= trailer_reg6_i(j);
    fmt_writes_trailer_i(j+28) <= trailer_reg7_i(j);
  end loop;
end process fmt_writes_trailer_mux;

clr_fe_occ_count_i <= clear_fe_occupancy_counter_in;

-- formatter writes trailer pulse counter control state machine 
--
-- Remember the show_trailer_flags signals must go out the RRIfpga into 
--  the FORMATTERfpgas, which in turn register the control signal and 
--  then tri-state and assert the output register onto the trailerflags_in bus.
--  The results for that fpga return onlyANDexactly 4or5(must check this)
--  clocks later. So we must time the latching and reset signals on the
--  trapping registers to trap the incoming flag data, then hold and 
--  display it to the occupancy counters for ONLY ONE CLOCK cycle. 
--  Any more than that, and the count will be decreased by that much more.
--
--  +--------------------+                    +------------------------+
--  | FormatterN         |                    | RRIfpga                |
--  |                    |                    |                        |
--  |  shw_trlr_flgs_in R|<-------------------|R<------state_machine   |
--  |         |          |                    |               | | |    |
--  |         v          |                    |               V | |    |
--  |   state_machine    |       +----------->|R-+->trlr_regN+0 V |    |
--  |         |          |       |            |  +--->trlr_regN+1 V    |
--  |         v          |       |            |  +----->trlr_regN+2    |
--  |     trlr_flgs_out R|-------+            |          .             |
--  |                    |       |            |          .             |
--  +--------------------+       |            |          .             |
--                               |            |          .             |
--                               .            |                        |
--                               .            +------------------------+
--                               .
--
--------------------------------------------------------------------------------
-- state machine next state determination logic
--------------------------------------------------------------------------------
state_logic : process (
  clk, 
  rst, 
  trailerflags_readout_en_in, 
  pres_state
  )
begin
  if (rst = '1') then
    trlr_reg_en_i          <= (others => '0');
--    trlr_reg_clr_i         <= (others => '1');
    show_trailer_flags_out <= (others => '0');
    pres_state             <= reset;
  elsif (clk'event and clk = '1') then
    case pres_state is
      when reset    =>
--        trlr_reg_clr_i         <= (others => '1');
        trlr_reg_en_i          <= (others => '0');
        show_trailer_flags_out <= (others => '0');
        pres_state <= idle;
   -- choose which command bitstream will be sent
      when idle     =>
--        trlr_reg_clr_i         <= (others => '1');
        trlr_reg_en_i          <= (others => '0');
        show_trailer_flags_out <= (others => '0');
        if (trailerflags_readout_en_in = '1') then
          pres_state <= readout_fmt0;
        end if;
      when readout_fmt0 =>
--        trlr_reg_clr_i         <= "01000000";
        trlr_reg_en_i          <= "10000000";
        show_trailer_flags_out <= "00000001"; -- send request to fmt0
        if (trailerflags_readout_en_in = '1') then
          pres_state <= readout_fmt1;
        else
          pres_state <= idle;
        end if;
      when readout_fmt1 =>
--        trlr_reg_clr_i         <= "10000000";
        trlr_reg_en_i          <= "00000001";
        show_trailer_flags_out <= "00000010";
        pres_state             <= readout_fmt2;
      when readout_fmt2 =>
--        trlr_reg_clr_i         <= "00000001";
        trlr_reg_en_i          <= "00000010";
        show_trailer_flags_out <= "00000100";
        pres_state             <= readout_fmt3;
      when readout_fmt3 =>
--        trlr_reg_clr_i         <= "00000010";
        trlr_reg_en_i          <= "00000100";
        show_trailer_flags_out <= "00001000";
        pres_state             <= readout_fmt4;
      when readout_fmt4 =>
--        trlr_reg_clr_i         <= "00000100";
        trlr_reg_en_i          <= "00001000";
        show_trailer_flags_out <= "00010000";
        pres_state             <= readout_fmt5;
      when readout_fmt5 =>
--        trlr_reg_clr_i         <= "00001000";
        trlr_reg_en_i          <= "00010000";
        show_trailer_flags_out <= "00100000";
        pres_state             <= readout_fmt6;
      when readout_fmt6 =>
--        trlr_reg_clr_i         <= "00010000";
        trlr_reg_en_i          <= "00100000";
        show_trailer_flags_out <= "01000000";
        pres_state             <= readout_fmt7;
      when readout_fmt7 =>
--        trlr_reg_clr_i         <= "00100000";
        trlr_reg_en_i          <= "01000000";
        show_trailer_flags_out <= "10000000";
        pres_state             <= readout_fmt0;
      when others   =>
--        trlr_reg_clr_i         <= (others => '1');
        trlr_reg_en_i          <= (others => '0');
        show_trailer_flags_out <= (others => '0');
        pres_state             <= idle;
    end case;
  end if;
end process state_logic;

--------------------------------------------------------------------------------
-- Process to allow read access to counters
--------------------------------------------------------------------------------
read_counters : process (
  clk, 
  rst, 
  focbus_a_in,
  focbus_en_in, 
  focbus_rnw_in
  )
begin
  if (rst = '1') then
    focbus_d_out <= (others => '0');
--    group_en_i   <= (others => '0');
--    for j in 0 to 7 loop
--      group_mask_i(j) <= (others => '0');
--    end loop;
  elsif (clk'event AND clk = '1') then
    if (focbus_en_in = '1' AND focbus_a_in(7 downto 5) = "010") then  -- consult p 54 section 6.11 in PixelRod Appendix A
      if (focbus_rnw_in = '1') then
        case focbus_a_in(4 downto 0) is
          when "00000" =>  -- PPC Address 0x00404500
            focbus_d_out <= std_logic_vector(fe_occupancy_count_i( 7)) &
                            std_logic_vector(fe_occupancy_count_i( 6)) &
                            std_logic_vector(fe_occupancy_count_i( 5)) &
                            std_logic_vector(fe_occupancy_count_i( 4)) &
									 std_logic_vector(fe_occupancy_count_i( 3)) &
                            std_logic_vector(fe_occupancy_count_i( 2)) &
                            std_logic_vector(fe_occupancy_count_i( 1)) &
                            std_logic_vector(fe_occupancy_count_i( 0));
          when "00001" =>  -- PPC Address 0x00404504
            focbus_d_out <= std_logic_vector(fe_occupancy_count_i(15)) &
                            std_logic_vector(fe_occupancy_count_i(14)) &
                            std_logic_vector(fe_occupancy_count_i(13)) &
                            std_logic_vector(fe_occupancy_count_i(12)) &
									 std_logic_vector(fe_occupancy_count_i(11)) &
                            std_logic_vector(fe_occupancy_count_i(10)) &
                            std_logic_vector(fe_occupancy_count_i( 9)) &
                            std_logic_vector(fe_occupancy_count_i( 8));
          when "00010" =>  -- PPC Address 0x00404508
            focbus_d_out <= std_logic_vector(fe_occupancy_count_i(23)) &
                            std_logic_vector(fe_occupancy_count_i(22)) &
                            std_logic_vector(fe_occupancy_count_i(21)) &
                            std_logic_vector(fe_occupancy_count_i(20)) &
                            std_logic_vector(fe_occupancy_count_i(19)) &
                            std_logic_vector(fe_occupancy_count_i(18)) &
                            std_logic_vector(fe_occupancy_count_i(17)) &
                            std_logic_vector(fe_occupancy_count_i(16));
          when "00011" =>  -- PPC Address 0x0040450c
            focbus_d_out <= std_logic_vector(fe_occupancy_count_i(31)) &
                            std_logic_vector(fe_occupancy_count_i(30)) &
                            std_logic_vector(fe_occupancy_count_i(29)) &
                            std_logic_vector(fe_occupancy_count_i(28)) &
                            std_logic_vector(fe_occupancy_count_i(27)) &
                            std_logic_vector(fe_occupancy_count_i(26)) &
                            std_logic_vector(fe_occupancy_count_i(25)) &
                            std_logic_vector(fe_occupancy_count_i(24));

          -- the following three cases are for internal scan engine, not needed yet
--          when "01100" => 
--            focbus_d_out( 7 downto  0) <= "00" & std_logic_vector(group_mask_i(0));
--            focbus_d_out(15 downto  8) <= "00" & std_logic_vector(group_mask_i(1));
--            focbus_d_out(23 downto 16) <= "00" & std_logic_vector(group_mask_i(2));
--            focbus_d_out(31 downto 24) <= "00" & std_logic_vector(group_mask_i(3));
--          when "01101" =>
--            focbus_d_out( 7 downto  0) <= "00" & std_logic_vector(group_mask_i(4));
--            focbus_d_out(15 downto  8) <= "00" & std_logic_vector(group_mask_i(5));
--            focbus_d_out(23 downto 16) <= "00" & std_logic_vector(group_mask_i(6));
--            focbus_d_out(31 downto 24) <= "00" & std_logic_vector(group_mask_i(7));
--          when "10000" =>
--			   focbus_d_out(3 downto 0) <= group_en_i;
				
          when others => focbus_d_out <= (others => '0');
			 
        end case;
--      elsif (focbus_rnw_in = '0' AND acknowledge_cycle_state = wait_1_clk) then
--        case focbus_a_in(4 downto 0) is
--          when "01100" => 
--            group_mask_i(0) <= unsigned(focbus_d_in( 5 downto  0));
--            group_mask_i(1) <= unsigned(focbus_d_in(13 downto  8));
--            group_mask_i(2) <= unsigned(focbus_d_in(21 downto 16));
--            group_mask_i(3) <= unsigned(focbus_d_in(29 downto 24));
--          when "01101" =>
--            group_mask_i(4) <= unsigned(focbus_d_in( 5 downto  0));
--            group_mask_i(5) <= unsigned(focbus_d_in(13 downto  8));
--            group_mask_i(6) <= unsigned(focbus_d_in(21 downto 16));
--            group_mask_i(7) <= unsigned(focbus_d_in(29 downto 24));
--          when "10000" =>
--            group_en_i <= focbus_d_in(3 downto 0);
--          when others  => 
--        end case;
      end if;
    end if;
  end if;
end process read_counters;

-- for internal scan engine, not needed for now
--group_en_out <= group_en_i;

--------------------------------------------------------------------------------
-- Process to generate bus transfer ack
--------------------------------------------------------------------------------
foc_transfer_ack : process (
  clk, 
  rst, 
  focbus_en_in
  )
begin
  if (rst = '1') then
    focbus_ack_out          <= '0';
    acknowledge_cycle_state <= reset;
  elsif (clk'event and clk = '1') then
    case acknowledge_cycle_state is
      when reset =>
        focbus_ack_out <= '0';
      --
        if (focbus_en_in = '1') then
          acknowledge_cycle_state <= wait_0_clk;
        end if;

      when wait_0_clk =>
        focbus_ack_out <= '0';
      --
        acknowledge_cycle_state <= wait_1_clk;

      when wait_1_clk =>
        focbus_ack_out <= '1';
      --
        acknowledge_cycle_state <= acknowledge;

      when acknowledge =>
        focbus_ack_out <= '1';
      --
        if (focbus_en_in = '0') then
          acknowledge_cycle_state <= reset;
        end if;
    end case;
  end if;
end process foc_transfer_ack;

--------------------------------------------------------------------------------
-- Single Counter zero signal
--
--   FE Occ Zero Indicator    |  40 Mhz | 80 MHz  | 160 MHz |
--       |--------------------+---------+---------+---------|             
--       |  feocc_zero_in( 0) | MOD 00  | MOD 00  | MOD 00  | 
--       |  feocc_zero_in( 1) | MOD 01  | MOD 01  |         |
--       |  feocc_zero_in( 2) | MOD 02  |         |         |
--       |  feocc_zero_in( 3) | MOD 03  |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in( 6) | MOD 10  | MOD 10  | MOD 10  | 
--       |  feocc_zero_in( 7) | MOD 11  | MOD 11  |         |
--       |  feocc_zero_in( 8) | MOD 12  |         |         |
--       |  feocc_zero_in( 9) |         |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(12) | MOD 20  | MOD 20  | MOD 20  |
--       |  feocc_zero_in(13) | MOD 21  | MOD 21  |         |
--       |  feocc_zero_in(14) | MOD 22  |         |         |
--       |  feocc_zero_in(15) | MOD 23  |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(18) | MOD 30  | MOD 30  | MOD 30  |
--       |  feocc_zero_in(19) | MOD 31  | MOD 31  |         |
--       |  feocc_zero_in(20) |         |         |         |
--       |  feocc_zero_in(21) |         |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(24) | MOD 40  | MOD 40  | MOD 40  |
--       |  feocc_zero_in(25) | MOD 41  | MOD 41  |         |
--       |  feocc_zero_in(26) | MOD 42  |         |         |
--       |  feocc_zero_in(27) | MOD 43  |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(30) | MOD 50  | MOD 50  | MOD 50  |
--       |  feocc_zero_in(31) | MOD 51  | MOD 51  |         |
--       |  feocc_zero_in(32) | MOD 52  |         |         |
--       |  feocc_zero_in(33) |         |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(36) | MOD 60  | MOD 60  | MOD 60  |
--       |  feocc_zero_in(37) | MOD 61  | MOD 61  |         |
--       |  feocc_zero_in(38) | MOD 62  |         |         |
--       |  feocc_zero_in(39) | MOD 63  |         |         |
--       |--------------------+---------+---------+---------|
--       |  feocc_zero_in(42) | MOD 70  | MOD 70  | MOD 70  |
--       |  feocc_zero_in(43) | MOD 71  | MOD 71  |         |
--       |  feocc_zero_in(44) |         |         |         |
--       |  feocc_zero_in(45) |         |         |         |
--       |--------------------+---------+---------+---------|             
--
-- SCT/Pixel shared counters
--   Link01 j=0,6,12,18,24,30,36,42
--   Link23 j=1,7,13,19,25,31,37,43
--   Link45 j=2,8,14,20,26,32,38,44
--   Link67 j=3,9,15,21,27,33,39,45
-- SCT Only
--   Link89 j=4,10,16,22,28,34,40,46
--   LinkAB j=5,11,17,23,29,35,41,47
--
--------------------------------------------------------------------------------
fe_occ_zero : process (
  clk,
  fe_occupancy_count_i,
  fe_occupancy_zero_i
  )
begin
  if (clk'event and clk = '1') then -- can we combine the two loops for clean up??
    for j in 0 to 30 loop
      fe_occupancy_gl_zero_out <= fe_occupancy_zero_i(j) OR fe_occupancy_zero_i(j+1); -- adds routing inefficiency?
    end loop;

    for j in 0 to 31 loop
      if (fe_occupancy_count_i(j) = 0) then
        fe_occupancy_zero_i(j) <= '1';
      else
        fe_occupancy_zero_i(j) <= '0';
      end if;     
    end loop;
  end if;
end process;

-- the following two processes are for internal scan engine, not needed for now

--group_zero_logic : process (
--  clk, 
--  rst
--  )
--begin
--  if (rst = '1') then
--    group_zero_out <= (others => '0');
--  elsif (clk'event and clk = '1') then
--    for j in 0 to 3 loop
--      group_zero_out(j) <= group_en_i(j) AND (
--                          (fe_occupancy_zero_i(grp_link_pointer(2*j+1))) OR
--                          (fe_occupancy_zero_i(grp_link_pointer(2*j  ))) );
--    end loop;
--  end if;
--end process;
--
--group_mask_conv : process (group_mask_i)
--begin
--  for i in 0 to 7 loop
--    grp_link_pointer(i) <= CONV_INTEGER(group_mask_i(i));
--  end loop;
--end process;

end rtl; -- code for front_end_occupancy_counters

