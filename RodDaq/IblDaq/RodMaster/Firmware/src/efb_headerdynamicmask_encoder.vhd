-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: efb_headerdynamicmask_encoder.vhd 
-- Description:
--  Event fragment builder dynamic mask look up table is addressed as a memory 
--  with the read/write_en selecting/strobing the data into/outof each address. 
--
--  L1 triggers are issued to the rrif by the TIM causing the look up table 
--  to be "written into" the local event header fifo. 
--  We see in the below diagram and code, there are signals:
--   apply_default_mask, apply_correction_mask
--
--  When a L1 trigger occurs, it is qualified and translated by the 
--   front_end_command_processor.vhd into the following signals:
--
--  fe_pulse_out : out   std_logic; 
--   A signal which indicates a trigger has occurred and the default 
--   readout_modebits and eventheader_dynamicmask should be written
--   to local fifos for transmission to the Formatters and EFB
--   respectively. 
--
--  fe_cmd_mask_updated_out : out   std_logic;
--   After a new mask ready signal has been set in the control register,
--   the VERY NEXT trigger loads the NEW COMMAND mask into local operation
--   mask register in this block, and duplicates fe_pulse_out onto this
--   line FOR ONLY THIS TRIGGER.  This signal is used to clear the
--   new_mask_ready_in SR flip-flop set in the front_end_command_processor 
--   control_register. This is required so that that new command masks may
--   be written by the control dsp without them being erroneously used by
--   the command command processor.  In addition, it indicates that
--   corrective readout_modebits and eventheader_dynamicmask should be 
--   written to local fifos for transmission to the Formatters and EFB
--   respectively. 
-- 
--  Since these signals can happen VERY quickly (5 pulses in 25ns) they
--  they must be accumulated and issued to the appropriate destinations 
--  with appropriate amount of latency. 
--  DSP requirements:
--   Interrupt service routine time for default/normal l1 interrupt
--   Interrupt service routine time for corrective l1 interrupt
--
--  formatter_modebits_encoder.vhd requirements:
--   Enough time to copy the appropriate modebits into the local data
--   transfer fifo.
--  efb_headerdynamicmask_encoder.vhd requirements:
--   Enough time to copy the appropriate header and dynamicmask into the
--   local data transfer fifo.
--  fe_command_pulse_formatter_accumulator.vhd and decodes the fe_pulse_out
--  and fe_cmd_mask_updated_out into the following signals:  
--   fe_cmdpulse_formattersAB_out(1 downto 0): Signals the master_formatter
--     to expect another event from the detector
--   apply_default_mask    <--> dsp_interrupt(0) -- :
--   apply_correction_mask <--> dsp_interrupt(1)
-- The "apply..." are just simply equivalent signals to the interrupts.
--
--  When apply_default_mask is pulsed(commanded), this block copies
--  the default dynamic mask into the dynamic mask fifo. When the 
--  apply_correction_mask is pulsed(commanded), it copies the corrective
--  mask into the dynamic mask fifo.
--
--  
--  When enabled by the control register, readout of the local event header
--  fifo occurs until EITHER efb header/dynamic mask fifo is full or the local 
--  dynamic mask fifo is empty or full.
-- 
--  The following state machines are all 1 stage glitch free MOORE MACHINES.
--   as per the LBL template  "vhdl/sm1_sample.vhd" code.
--
--  The controller bus is mapped directly to the fifo from low order bit
--  to high order bit- eg: data_bus(0 to 15) -> fifo_in(0  to 15)
--
--  It is assumed that the dynamic mask words written into the LUT by the 
--  controller will be arranged in the following manner:
--       
--  The controller should examine the 
--   int_fifo_ef_out
--   int_fifo_ff_out
--   modebits_eventcount(6 downto 0)
--  flags to monitor "pending" system triggers in the efb of the ROD.
--
--    Default dynamicmask bits LookUpTable memory allocation
--     Address maskbits
--    ------------------------------------
--     0xC0    ROD specific event type[15 to  0]                   16bits,RW
--     0xC1    DynamicMask for   links[ 7 to  0] 2bits per link    16bits,RW
--     0xC2    DynamicMask for   links[15 to  8] 2bits per link    16bits,RW
--     0xC3    DynamicMask for   links[23 to 16] 2bits per link    16bits,RW
--     0xC4    DynamicMask for   links[31 to 24] 2bits per link    16bits,RW
--     0xC5    DynamicMask for   links[39 to 32] 2bits per link    16bits,RW (unused for IBL)
--     0xC6    DynamicMask for   links[47 to 40] 2bits per link    16bits,RW (unused for IBL)
--     0xC7    DynamicMask for   links[55 to 48] 2bits per link    16bits,RW (unused for IBL)
--     0xC8    DynamicMask for   links[63 to 56] 2bits per link    16bits,RW (unused for IBL)
--     0xC9    DynamicMask for   links[71 to 64] 2bits per link    16bits,RW (unused for IBL)
--     0xCA    DynamicMask for   links[79 to 72] 2bits per link    16bits,RW (unused for IBL)
--     0xCB    DynamicMask for   links[87 to 80] 2bits per link    16bits,RW (unused for IBL)
--     0xCC    DynamicMask for   links[95 to 88] 2bits per link    16bits,RW (unused for IBL)
--
--    Corrective dynamicmask bits
--     Address maskbits
--    ------------------------------------
--     0xD0    ROD specific event type[15 to  0]                   16bits,RW
--     0xD1    DynamicMask for   links[ 7 to  0] 2bits per link    16bits,RW
--     0xD2    DynamicMask for   links[15 to  8] 2bits per link    16bits,RW
--     0xD3    DynamicMask for   links[23 to 16] 2bits per link    16bits,RW
--     0xD4    DynamicMask for   links[31 to 24] 2bits per link    16bits,RW
--     0xD5    DynamicMask for   links[39 to 32] 2bits per link    16bits,RW (unused for IBL)
--     0xD6    DynamicMask for   links[47 to 40] 2bits per link    16bits,RW (unused for IBL)
--     0xD7    DynamicMask for   links[55 to 48] 2bits per link    16bits,RW (unused for IBL)
--     0xD8    DynamicMask for   links[63 to 56] 2bits per link    16bits,RW (unused for IBL)
--     0xD9    DynamicMask for   links[71 to 64] 2bits per link    16bits,RW (unused for IBL)
--     0xDA    DynamicMask for   links[79 to 72] 2bits per link    16bits,RW (unused for IBL)
--     0xDB    DynamicMask for   links[87 to 80] 2bits per link    16bits,RW (unused for IBL)
--     0xDC    DynamicMask for   links[95 to 88] 2bits per link    16bits,RW (unused for IBL)
--
--    Correction Trigger FIFO 
--     Address trapped maskbits 
--    ------------------------------------
--     0xE0    Trapped CorrectiveTrigger Header and DynamicMask    16bits,RW
--          Copied from the efb_hdm_e->efb datastream as directed by the
--          Corrective Trigger CaptureFIFO(fe_command_pulse_accumulator.vhd)
--
--
--  The "writein_state_machine" and "readout_state_machine" assumes that the LUT
--   contains valid data when it is enabled.  The control_in bit enables both
--   the writein AND readout state machines.
--    control_in : in std_logic;
--            0:flush all, 1:enable transfer
--
--  When the new_l1id_valid_in and new_ttype_valid_in signals go true, write the 
--  event header into the event_header_fifo with the following format:
--               
--  1. [15 to 0] : L1ID[15:0]                                              (eh_fifo)
--  2. [15 to 0] : ECRID[7:0] & L1ID[23:16]                                (eh_fifo)
--  3. [15 to 0] : BCID[11:0] & 0 & RoL Test block & BocClkErr & TimClkErr (eh_fifo)
--  4. [15 to 0] : RodEvtType[5:0] & TimTrigType[1:0] & AtlasTrigType[7:0] (tt_fifo)
--
--
--                                     +-------------------+                               
--                           +---------|incr           decr|-----+   
--                           |         |                   |-------event_mask_count(4 downto 0)
--                           |  clk ---> mask_count        |     |
--                           |         +-------------------+     |
--                           |                                   |
--                           |                                   |
--                       +---------------------------+           |                
--                       | LUT_writein_state_machine |           |                
-- L1A-------------------|        LUT readout address|----+      |                    
-- control(0 to 1)-------|                         re|---+|      |                     
-- apply_default_mask----|                         we|--+||      |                   
-- apply_correction_mask-|                           |  |||      |                   
--               clk----->                           |  |||      |                   
--                       +---------------------------+  |||      |                   
--                   +-------------+                    |||      |
--                   |         LUT |                    |||      |
-- adr_in(0 to 6)----|writein_adr  |                    |||      |
-- data_in(0 to 15)--|portA        |                    |||      |
-- we----------------|we         re|---------------------+|      |
--                   |  readout_adr|----------------------+      |                    
--                   |        portB|-+                  |        |                    
--                   +-------------+ |+-----------------+        |
--                                   ||                          |
--                                   || +------------------------------+            
--                                   || | 512x16bit registered fifo0   |            
--                                   || | dynamicmask bits             | 16         
--                                   +--|R                            R|-------+  +      
--   clk ----------------------------+-->                              |       |  |\
--                                   |+-|wen                        ren|----+  +--| | 16
--                                   |  +------------------------------+    |     | |----event_headermask(15 downto 0)
--                                   |  | 256x16bit registered fifo1   |    |  +--| |
--                                   |  | eventheader bits             | 16 |  |  |/
--                                 +----|R                            R|-------+  +|
--                                 | +-->                              |    |      |
--                                 |  +-|wen                        ren|---+|      |
--                                 |  | +------------------------------+   ||      |
--                                 |  |                          |         ||      |
--                                 |  +-------------+            |         ||      |
--                                 +---------------+|            |         ||      |
--                                                 ||            |         ||      |
--                         +-------------------+   ||            |         ||      |
--    clk -----------------> event_header_     |   ||            |         ||      |
--                         | writein_          |   ||            |         ||      |
-- l1id_bits(23 downto 0)--| state_machine     |---+|            |         ||      |
-- bcid_bits(11 downto 0)--|                   |----+            |         ||      |
-- ttype_bits(9 downto 0)--|                   |                 |         ||      |
-- new_l1id_valid_out------|                   |                 |         ||      |
-- new_bcid_valid_out------|                   |                 |         ||      |
-- new_ttype_valid_out-----|                   |---+             |         ||      |
--         |       clk----->                   |   |             |         ||      |
--         |               +-------------------+   |             |         ||      |
--         |                                       |             |         ||      |
--         |                           +-----------------------+ |         ||      |
--         |                           | readout_control_      |-+         ||      |
--         |---------------------------| count_control_        |-----------+|      |
--         |                           | state_machine         |------------+      |
-- event_mask_count(4 downto 0) -------|                       |-------------------+
-- event_headermask_fifo_ef_n(0 to 1)--|                       |---event_headermask_fifo_we_n        
-- event_headermask_fifo_ff_n(0 to 1)--|                       |                          
-- control(0 to 1)---------------------|                       |---+                      
--    clk ------------------------+---->                       |   |     
--         |                      |    +-----------------------+   |                               
--         |                      |                                |                          
--         |                      |                                |                              
--         |                      |                                |                          
--         |                      |    +-------------------+       |                       
--         +---------------------------|incr           decr|-------+   
--                                |    |                   |-------event_header_count(4 downto 0)
--                                +----> header_count      |   
--                                     +-------------------+   
--
--
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--   14 March 2000 - MLN first version : Requirements and functionality
--                   understood and suficiently documented to proceed 
--                   with coding.
--
--   27 May   2002 - JMJ: Added ECR_ID to Header, and Muxed in the Calibaration
--                        L1ID, BCID, Event ID, and Trigger Type data.
--                        Simplified state machines as required.
--
--   30 August 2013 - SPC Modified code for use in IBL ROD, changed the number of mask words per trigger
--                    See line 1286 and line 1592
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
--
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
--
-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity efb_headerdynamicmask_encoder is
  port (
    clk        : in  std_logic;
    rst        : in  std_logic;
    control_in : in  std_logic;  -- 1:enable transfer, -- 0:flush all
    rnw_in     : in  std_logic;
    ce_n_in    : in  std_logic;
    ack_out    : out std_logic;

    apply_default_mask_in : in  std_logic;
-- Interrupt to the PPC to signify that a DEFAULT trigger has occurred
-- and that default dynamic mask and default readoutmodebits will be written
-- to the efb and formatter (respectively) on this trigger. 
    
    apply_corrective_mask_in : in  std_logic;
-- Interrupt to the PPC to signify that a CORRECTIVE trigger has occurred
-- and that corrective dynamic mask and corrective readoutmodebits will be
-- written to the efb and formatter (respectively) on this AND ONLY this trigger.
-- This signal is used to clear the new_mask_ready_in set/reset flip-flop in the
-- front_end_command_processor control_register. If this signal is detected, the
-- contents of the Corrective Dynamic Mask LUT locations are transferred to, and
-- overwrite the Default Dynamic Mask LUT values.

    record_headerdynamicmask_in    : in  std_logic;  -- 
    read_trig_indictor_que_out     : out std_logic;  --
    l1id_bits_in                   : in  std_logic_vector(23 downto 0); --
    bcid_bits_in                   : in  std_logic_vector(11 downto 0); --
    ttype_bits_in                  : in  std_logic_vector( 9 downto 0); --
    new_l1id_valid_in              : in  std_logic;                     --
    new_ttype_valid_in             : in  std_logic;                     --
    int_lut_bdata_in               : in  std_logic_vector(15 downto 0); --
    int_lut_bdata_out              : out std_logic_vector(15 downto 0); --
    int_lut_adr_in                 : in  unsigned(7 downto 0);          --
-- Signals from EFB to indicate FIFO Occupancy,  EF is not used
    event_headermask_fifo_ef_n_in  : in  std_logic;                     -- empty flag
    event_headermask_fifo_ff_n_in  : in  std_logic;                     -- full flag
    event_headermask_out           : out std_logic_vector(15 downto 0); --
    event_lbid_count_out           : out std_logic_vector( 7 downto 0); -- l1 bc id pair count?
    event_trgt_count_out           : out std_logic_vector( 7 downto 0); -- trigger tpye
    event_mask_count_out           : out std_logic_vector( 7 downto 0); -- 
    event_header_count_out         : out std_logic_vector( 7 downto 0); --
-- Signal to EFB that Enables Writes
    event_headermask_fifo_we_n_out : out std_logic;                     --
--  Internal FIFO Occupancy Flags: EF=Empty Flag, FF=Full Flag
--     0 : l1id & bcid bits  (FIFO 256x16)
--     1 : trigger type bits (FIFO 256x16)
--     2 : l1id,bcid & Ttype bits (FIFO 1024x16)
--     3 : Mask Bits (FIFO 2048x16)
--     4 : Corrective trigger dynamicmask storage (FIFO 256x16)
    int_fifo_ef_out                : out std_logic_vector( 4 downto 0);
    int_fifo_ff_out                : out std_logic_vector( 4 downto 0);
-- Count of ECR commands that have been issued
    ecrid_in                       : in  unsigned( 7 downto 0);
    int_cal_mode_en_in             : in  std_logic; -- 1:CalMode 0:Normal
    cal_l1a0_in                    : in  std_logic; -- L1 Triggers from SP0
    cal_l1a1_in                    : in  std_logic; -- L1 Triggers from SP1
-- Calibration or Test Trigger Type: [9:8] TIM, [7:0] ATLAS
    cal_tt0_in                     : in  std_logic_vector( 9 downto 0); -- SP0
    cal_tt1_in                     : in  std_logic_vector( 9 downto 0); -- SP1
-- Calibration or Test L1 ID
    cal_l1id0_in                   : in  std_logic_vector(23 downto 0); -- SP0
    cal_l1id1_in                   : in  std_logic_vector(23 downto 0); -- SP1
-- Calibration or Test BC ID
    cal_bcid0_in                   : in  std_logic_vector(11 downto 0); -- SP0
    cal_bcid1_in                   : in  std_logic_vector(11 downto 0); -- SP1
    pause_triggers_out             : out std_logic;
    event_type_in                  : in  std_logic_vector( 2 downto 0);
    load_event_type_in             : in  std_logic;
    mask_dxi_in                    : in  std_logic;
    nevent_in                      : in  std_logic_vector(15 downto 0);
    boc_ok_in                      : in  std_logic; -- boc clock error
    tim_ok_in                      : in  std_logic; -- tim clock error
    rol_test_in                    : in  std_logic;
    rol_test_count_in              : in  std_logic_vector( 5 downto 0);
    rod_tt_wr_out                  : out std_logic;
    event_l1id_out                 : out std_logic_vector(23 downto 0)
    );
end efb_headerdynamicmask_encoder;

architecture rtl of efb_headerdynamicmask_encoder is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type eh_writein_states is (
  reset,                   -- event header writein state  
  idle,                    -- the event header gets copied from the 
  write_hdr_word0,         -- trigger_signal_decoder outputs
  setup_word1,
  write_hdr_word1,
  setup_word2,
  write_hdr_word2,
  increment_header_count
  );

type tt_writein_states is (
  reset,                   -- trigger type writein state
  idle,                    -- the event triggertype gets copied from the 
  write_ttype_word,        -- trigger_signal_decoder outputs
  increment_ttype_count
  );

type lut_writein_states is (
  reset,
  idle, 
  setup_rod_tt,
  write_rod_tt,
  write_dynamic_mask,
  target_fifo_full_wait,
  wr_data_to_fifo,
  increment_dm_address,
  increment_mask_count
  );

type lbtrosm_states is (
  reset,
  idle,
  lbt_write_L1ID, 
  readout_ECRID_L1ID,
  lbt_write_ECRID_L1ID,
  readout_BCID,
  lbt_write_BCID,
  lbt_write_TT,
  readout_L1ID_TT,
  decrement_event_lbid_tt_count,  
  increment_event_header_count,
  lbtrosm_done
  );

type dfmrosm_states is (
  reset,                   -- dynamicmask and header readout state machine 
  idle, 
  load_header_wordcount,   -- load finestatecounter with "3"
  wait_a_cycle0,
  target_fifo_full_wait0,
  wr_efb_id_data, -- write the first header word into the efb 
                  --(it's already sitting at the head of the fifo)
                  -- propagate data and feedback control
                  -- one clk out of the rcf_fpga, one into the fifo_memory
                  -- one out of the fifo_memory, one into the rcf_fpga
  decrement_header_count,
  load_dynamicmask_wordcount, -- load finestatecounter with "13"
  wait_a_cycle1,
  target_fifo_full_wait1,
  wr_efb_dm_data,        -- same as above, but for the dynamic mask
  decrement_mask_count,
  read_trig_indictor_que
  );

type ack_states is (
  reset,
  wait_0_clk,
  wait_1_clk,
  wait_2_clk,
  wait_3_clk,
  acknowledge
  );

signal ce_n_in_dly1_i          : std_logic;
signal acknowledge_cycle_state : ack_states;
 -- number of l1/bcid's packets in this block's internal fifos
signal lbid_cnt_i    : unsigned(7 downto 0);
signal lbid_cnt_up_i : std_logic;
signal lbid_cnt_dn_i : std_logic;
 -- number of trigger type packets in this block's internal fifos
signal trgt_cnt_i    : unsigned(7 downto 0);
signal trgt_cnt_up_i : std_logic;
signal trgt_cnt_dn_i : std_logic;
 -- number of cached headers in this block's internal fifos
signal hedr_cnt_i    : unsigned(7 downto 0);
signal hedr_cnt_up_i : std_logic;
signal hedr_cnt_dn_i : std_logic;
 -- number of cached masks in this block's internal fifos
signal mask_cnt_i    : unsigned(7 downto 0);
signal mask_cnt_up_i : std_logic;
signal mask_cnt_dn_i : std_logic;

signal lbtrosm_pres_state         : lbtrosm_states;
signal dfmrosm_pres_state         : dfmrosm_states;
signal dfmrosm_loadval_i          : unsigned(4 downto 0);
signal dfmrosm_fine_statecount_i  : unsigned(4 downto 0);
signal dfmrosm_fine_state_load_i  : std_logic;
signal dfmrosm_fine_state_cntdn_i : std_logic;

signal wi_lut_pres_state        : lut_writein_states;
signal wi_lut_fine_statecount_i : unsigned(3 downto 0);
signal wi_lut_fine_state_load_i : std_logic;

signal wr_eventheader_state : eh_writein_states;
signal wr_trigtype_state    : tt_writein_states;

signal lut_baseaddress_i   : std_logic_vector( 3 downto 0);
signal lut_porta_datain_i  : std_logic_vector(15 downto 0);  -- changed
signal lut_porta_dataout_i : std_logic_vector(15 downto 0);  --
signal lut_out_data_i      : std_logic_vector(15 downto 0);
signal lut_wen_i           : std_logic;
signal lut_write_en_i      : std_logic;
signal lut_writein_adr_i   : unsigned(7 downto 0);
signal lut_readout_adr_i   : unsigned(7 downto 0);
signal mux_select_i        : std_logic;

--Bing
signal lut_wr_addr_i       : std_logic_vector(7 downto 0);
signal lut_rd_addr_i       : std_logic_vector(7 downto 0); 
signal gnd_i               : std_logic;
signal gnd_v_i             : std_logic_vector(15 downto 0);

signal ctsf_write_en_i : std_logic;
signal ctsf_read_en_i  : std_logic;
signal ctsf_dataout_i  : std_logic_vector(15 downto 0);

signal flush_fifo_i         : std_logic;
signal fifo_wren_i          : std_logic_vector(4 downto 0);
signal fifo_rden_i          : std_logic_vector(1 downto 0);
signal header_fifo_ren_i    : std_logic;  --
signal dnmask_fifo_ren_i    : std_logic;  --
signal efb_hdm_fifo_wen_n_i : std_logic;
signal read_trgr_indctr_q_i : std_logic;

signal eheader_i      : std_logic_vector(15 downto 0);
signal event_ttype_i  : std_logic_vector(15 downto 0);
signal modein_i       : std_logic_vector(15 downto 0);
signal modett_i       : std_logic_vector(15 downto 0);
signal rodtt_i        : std_logic_vector(15 downto 0);
signal ttfifo_i       : std_logic_vector(15 downto 0);
signal rod_ttfifo_i   : std_logic_vector(15 downto 0);
signal dynmkmsk_i     : std_logic_vector(15 downto 0);
signal evnthedr_i     : std_logic_vector(15 downto 0);
signal evnt_hedrmsk_i : std_logic_vector(15 downto 0);

signal complete_mask_written_i : std_logic;
signal new_id_valid_in_i       : std_logic; --
signal new_ttype_valid_in_i    : std_logic; --
signal dfmw_count_i            : unsigned(2 downto 0);

signal cal_sp0_i : std_logic;
signal cal_sp1_i : std_logic;
signal fifo_ef_i : std_logic_vector(4 downto 0);  -- 
signal fifo_ff_i : std_logic_vector(4 downto 0);  --

signal rod_tt_full_i : std_logic;

--signal l1bc_fifo_occ_i   : std_logic_vector( 7 downto 0);
--signal tt_fifo_occ_i     : std_logic_vector( 7 downto 0);
--signal rod_tt_fifo_occ_i : std_logic_vector( 7 downto 0);
--signal evth_fifo_occ_i   : std_logic_vector( 9 downto 0);
--signal mask_fifo_occ_i   : std_logic_vector(10 downto 0);

type set_event_type_states is (
  idle,
  load_def_evt_type,
  load_cor_evt_type,
  done
  );
signal set_event_type : set_event_type_states;

--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

-- declare all of the fifos components
component fifo_256x16_master
  port (
    clk             : in    std_logic; 
    rst             : in    std_logic; 
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    full_flag_out   : out   std_logic; --
    empty_flag_out  : out   std_logic;
    occupancy_count : out   std_logic_vector(7 downto 0)
    );
end component;

component fifo_1024x16_master
  port(
    clk             : in    std_logic;
    rst             : in    std_logic;
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    empty_flag_out  : out   std_logic; --
    full_flag_out   : out   std_logic;  --
    occupancy_count : out   std_logic_vector(9 downto 0)
    );
end component;

component fifo_2048x16
  port(
    clk             : in    std_logic; 
    rst             : in    std_logic; 
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    empty_flag_out  : out   std_logic; --
    full_flag_out   : out   std_logic; --
    occupancy_count : out   std_logic_vector(10 downto 0)
    );
end component;

component fifo_4096x16
  port(
    clk             : in    std_logic;
    rst             : in    std_logic;
    flush_fifo_in   : in    std_logic; --
    wren_in         : in    std_logic; -- enable input register
    rden_in         : in    std_logic; -- enable output register
    data_in         : in    std_logic_vector(15 downto 0); --
    data_out        : out   std_logic_vector(15 downto 0); --
    empty_flag_out  : out   std_logic; --
    full_flag_out   : out   std_logic; --
    occupancy_count : out   std_logic_vector(11 downto 0)
    );
end component;

--component dpram_256x16
--  port (
--    clk              : in  std_logic;
--    rst              : in  std_logic;
--    dprm_writein_bus : in  std_logic_vector(15 downto 0);  -- changed data type
--    dprm_readiin_bus : out std_logic_vector(15 downto 0);  --
--    writein_adr      : in  unsigned(7 downto 0);
--    readout_adr      : in  unsigned(7 downto 0);
--    write_strobe     : in  std_logic;
--    read_strobe      : in  std_logic;
--    dprm_readout_bus : out std_logic_vector(15 downto 0)
--    );
--end component;

COMPONENT dpram_true_256x16
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    clkb : IN STD_LOGIC;
    rstb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(15 DOWNTO 0)
  );
END COMPONENT;

--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
begin
--
-- wire up internal signals to the outside world
--
event_lbid_count_out   <= std_logic_vector(lbid_cnt_i);
event_trgt_count_out   <= std_logic_vector(trgt_cnt_i);
event_mask_count_out   <= std_logic_vector(mask_cnt_i);
event_header_count_out <= std_logic_vector(hedr_cnt_i);
--
read_trig_indictor_que_out     <= read_trgr_indctr_q_i;
event_headermask_out           <= evnt_hedrmsk_i;
event_headermask_fifo_we_n_out <= efb_hdm_fifo_wen_n_i;

-- internal fifo status to the controller 
int_fifo_ef_out <=  fifo_ef_i;
int_fifo_ff_out <=  fifo_ff_i;
-- 
flush_fifo_i <= NOT control_in;
--
--------------------------------------------------------------------------
-- instantiate the fifos and hook them up.
-- l1id & bcid bits from the trigger signal decoder get cached here
--  until a ttype arrives and is written into the trigger type fifo 
l1id_bcid_fifo : fifo_1024x16_master   -- was ==> fifo_256x16
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(0),
    rden_in         => fifo_rden_i(0),
    data_in         => modein_i(15 downto 0),
    data_out        => eheader_i(15 downto 0),
    full_flag_out   => fifo_ff_i(0),
    empty_flag_out  => fifo_ef_i(0),
    occupancy_count => open --l1bc_fifo_occ_i
    );

-- trigger type bits from the trigger signal decoder get cached here
--  until a l1/bcid header arrives and is written into the l1id & bcid fifo
tt_fifo : fifo_256x16_master  -- 1 word per trigger
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(1),
    rden_in         => fifo_rden_i(1),
    data_in         => modett_i(15 downto 0),
    data_out        => ttfifo_i(15 downto 0), 
    full_flag_out   => fifo_ff_i(1),
    empty_flag_out  => fifo_ef_i(1),
    occupancy_count => open --tt_fifo_occ_i
    );

rod_tt_fifo : fifo_256x16_master  -- 1 word per trigger
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(4),
    rden_in         => fifo_rden_i(1),
    data_in         => rodtt_i(15 downto 0),
    data_out        => rod_ttfifo_i(15 downto 0), 
    full_flag_out   => rod_tt_full_i,
    empty_flag_out  => open,
    occupancy_count => open --rod_tt_fifo_occ_i
    );

-- l1id,bcid & ttype bits  get read out of the above fifos and 'mixed' to 
--  make a real event header
evth_fifo : fifo_1024x16_master  -- 4 words per trigger
   port map(
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(2),
    rden_in         => header_fifo_ren_i,
    data_in         => event_ttype_i,
    data_out        => evnthedr_i,
    full_flag_out   => fifo_ff_i(2),
    empty_flag_out  => fifo_ef_i(2),
    occupancy_count => open --evth_fifo_occ_i
    );

-- Mask Bits get stored here every time we receive an l1 trigger
--  in preparation for the header/dynamic mask to be forwarded to the efb
mask_fifo : fifo_4096x16    -- was: 2048x16   -- 12 words per trigger, must be changed to 4 words>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
  port map(
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => fifo_wren_i(3),
    rden_in         => dnmask_fifo_ren_i,
    data_in         => lut_out_data_i,
    data_out        => dynmkmsk_i,
    full_flag_out   => fifo_ff_i(3),
    empty_flag_out  => fifo_ef_i(3),
    occupancy_count => open --mask_fifo_occ_i
    );

-- Readout the default/corrective look up table (0x00-0x1F):RW or the 
-- correction_trigger_fifo (0xE0):R only 
dataout_lut_mux : process (
  clk, 
  rst, 
  ce_n_in, 
  ce_n_in_dly1_i,
  int_lut_adr_in, 
  lut_porta_dataout_i, 
  ctsf_dataout_i
  )
begin  
  if (rst = '1') then
    int_lut_bdata_out <= (others => '0'); 
    ctsf_read_en_i <= '0'; -- disable the ren on the corrective fifo
  elsif (clk'event and clk = '1') then
-- When address is"0xE0" point to efb_hdm_fifo output
    if (std_logic_vector(int_lut_adr_in) = "11100000") then
      int_lut_bdata_out <= ctsf_dataout_i; 
-- Strobe the ren on the corrective fifo at the end of a read cycle address
--  to advance the ReadOut Address pointer
      ctsf_read_en_i <= NOT ce_n_in_dly1_i AND ce_n_in;
-- All other addresses point to LUT
    else 
      int_lut_bdata_out <= lut_porta_dataout_i; 
      ctsf_read_en_i <= '0';
    end if;
  end if;
end process dataout_lut_mux;
  
-- instantiate the lut and hook it up.
-- this can be cleaned up later
lut_readout_adr_i(7)  <= lut_baseaddress_i(3); 
lut_readout_adr_i(6)  <= lut_baseaddress_i(2); 
lut_readout_adr_i(5)  <= lut_baseaddress_i(1); 
lut_readout_adr_i(4)  <= lut_baseaddress_i(0); 
lut_readout_adr_i(3)  <= wi_lut_fine_statecount_i(3); 
lut_readout_adr_i(2)  <= wi_lut_fine_statecount_i(2); 
lut_readout_adr_i(1)  <= wi_lut_fine_statecount_i(1); 
lut_readout_adr_i(0)  <= wi_lut_fine_statecount_i(0); 
--

dm_lut_mux : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    lut_porta_datain_i <= (others => '0');
    lut_writein_adr_i  <= (others => '0');
    lut_wen_i <= '0';
    set_event_type <= idle;
  elsif (clk'event and clk = '1') then
    case set_event_type is 
      when idle => 
        lut_porta_datain_i <= int_lut_bdata_in;
        lut_writein_adr_i  <= int_lut_adr_in;
        lut_wen_i <= lut_write_en_i AND NOT ce_n_in;
        if (load_event_type_in = '1') then
          set_event_type <= load_def_evt_type;
        end if;
		  
      when load_def_evt_type => 
        lut_porta_datain_i <= "0000000000000" & event_type_in;
        lut_writein_adr_i  <= "11000000"; -- 0xC0 default LUT address
        lut_wen_i <= '1';
        set_event_type <= load_cor_evt_type;
		  
      when load_cor_evt_type => 
        lut_porta_datain_i <= "0000000000001" & event_type_in;
        lut_writein_adr_i  <= "11010000"; -- 0xD0 corrective LUT address
        lut_wen_i <= '1';
        set_event_type <= done;
		  
      when others => -- done
        lut_wen_i <= '0';
        set_event_type <= idle;
		  
    end case;
  end if;
end process;

-- default and corrective dynamic mask look up table gets written here by the
-- MDSP/controller for pasting onto the event headers from the TIM for
-- transmitting to the efb.
--dm_lut : dpram_256x16 
--  port map(
--    clk              => clk,
--    rst              => rst,
--    dprm_writein_bus => lut_porta_datain_i,
--    dprm_readiin_bus => lut_porta_dataout_i,
--    writein_adr      => lut_writein_adr_i,
--    readout_adr      => lut_readout_adr_i,
--    write_strobe     => lut_wen_i,
--    read_strobe      => rst,  -- Not Connected (what does this mean??)
--    dprm_readout_bus => lut_out_data_i
--    );

--Bing
lut_wr_addr_i <= std_logic_vector(lut_writein_adr_i);
lut_rd_addr_i <= std_logic_vector(lut_readout_adr_i); 
gnd_i <= '0';
gnd_v_i <= (others => '0');
	 
dm_lut : dpram_true_256x16
  PORT MAP (
    clka => clk,--
    rsta => rst,--
    wea(0) => lut_wen_i,--
    addra => lut_wr_addr_i,
    dina => lut_porta_datain_i,--
    douta => lut_porta_dataout_i,--
    clkb => clk,--
    rstb => rst,--
    web(0) => gnd_i,--
    addrb => lut_rd_addr_i,
    dinb => gnd_v_i,--
    doutb => lut_out_data_i--
  );	 
	 
--
-- Corrective trigger dynamicmask storage fifo
ctsf_write_en_i <= (NOT efb_hdm_fifo_wen_n_i) AND record_headerdynamicmask_in;
--  Whenever a corrective mask gets written to the efb, we make a copy of it here 
--  so we know to which trigger/event the correction got applied and the command
--  masks got updated
ctsf : fifo_256x16_master
  port map (
    clk             => clk,
    rst             => rst,
    flush_fifo_in   => flush_fifo_i,
    wren_in         => ctsf_write_en_i,
    rden_in         => ctsf_read_en_i,
    data_in         => evnt_hedrmsk_i,
    data_out        => ctsf_dataout_i,
    full_flag_out   => fifo_ff_i(4),
    empty_flag_out  => fifo_ef_i(4),
    occupancy_count => open
    );

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------
-- Register Signals in and out of this block

signal_register : process (
  clk,
  rst
  )
begin  -- process input_signals
  if (rst = '1') then
    new_id_valid_in_i  <= '0';
    new_ttype_valid_in_i <= '0';
  elsif (clk = '1' AND clk'event) then
    new_id_valid_in_i    <= new_l1id_valid_in;
    new_ttype_valid_in_i <= new_ttype_valid_in;
  end if;
end process signal_register;

-------------------------------------------------------------------------------
-- EVID FIFO Occupancy Test: If almost full raise ROD Busy and stop Triggers
pause_triggers : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    pause_triggers_out <= '0';
  elsif (clk = '1' AND clk'event) then
    if (lbid_cnt_i > 249 OR     -- (168 triggers: can hold 256 max)
        trgt_cnt_i > 249 OR     -- (168 triggers: can hold 256 max)
        mask_cnt_i > 249 OR     -- (168 triggers: can hold 256 max)
        hedr_cnt_i > 249) then  -- (168 triggers: can hold 256 max)
      pause_triggers_out <= '1';
    else
      pause_triggers_out <= '0';
    end if;
  end if;
end process pause_triggers;

-------------------------------------------------------------------------------
-- Event header (l1/bcid information) fifo write-in state machine
--  For each trigger write to 16 bit wide FIFO:
--    Word 1: L1ID[15:0]
--    Word 2: ECRID & L1ID[23:16]
--    Word 3: BCID & '0' & rol_test_in & boc_ok_in & tim_ok_in;
 
wr_eventheader_to_fifo : process (
  clk, 
  rst, 
  cal_l1a0_in, 
  cal_l1a1_in,
  fifo_ef_i, 
  fifo_ff_i, 
  new_id_valid_in_i,
  wr_eventheader_state, 
  int_cal_mode_en_in
  )
begin
  if (rst = '1') then
    fifo_wren_i(0) <= '0';
    modein_i       <= (others => '0');
    lbid_cnt_up_i  <= '0';
    cal_sp0_i      <= '0';
    cal_sp1_i      <= '0';
    wr_eventheader_state <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '1') then
      case wr_eventheader_state is
        when reset =>
          fifo_wren_i(0) <= '0';
          modein_i       <= (others => '0');
          lbid_cnt_up_i  <= '0';
          cal_sp0_i      <= '0';
          cal_sp1_i      <= '0';
          wr_eventheader_state <= idle;

        when idle =>
         -- start a readout sequence when header information is available
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
          if (int_cal_mode_en_in = '1') then -- calibration mode trigger from PPC
            if (cal_l1a0_in = '1') then
              cal_sp0_i <= '1';
              if (mask_dxi_in = '1') then
                modein_i <= nevent_in(15 downto 0);
              else
                modein_i <= cal_l1id0_in(15 downto 0);
              end if;
              wr_eventheader_state <= write_hdr_word0;
            elsif (cal_l1a1_in = '1') then
              cal_sp1_i <= '1';
              if (mask_dxi_in = '1') then
                modein_i <= nevent_in(15 downto 0);
              else
                modein_i  <= cal_l1id1_in(15 downto 0);
              end if;
              wr_eventheader_state <= write_hdr_word0;
            end if;
          elsif (new_id_valid_in_i = '1') then -- real trigger available from TIM
            cal_sp0_i <= '0';
            cal_sp1_i <= '0';
            modein_i  <= l1id_bits_in(15 downto 0);
            wr_eventheader_state <= write_hdr_word0;
          end if;

        when write_hdr_word0 =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';

          if (fifo_ff_i(0) = '0') then
            fifo_wren_i(0) <= '1'; -- schreiben sie!!
            wr_eventheader_state <= setup_word1;
          end if;

        when setup_word1 =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
 -- Set up next word
          modein_i(15 downto 8) <= std_logic_vector(ecrid_in);
          if (cal_sp0_i = '1') then
            if (mask_dxi_in = '1') then
              modein_i(7 downto 0) <= (others => '0');
            else
              modein_i(7 downto 0) <= cal_l1id0_in(23 downto 16);
            end if;
          elsif (cal_sp1_i = '1') then
            if (mask_dxi_in = '1') then
              modein_i(7 downto 0) <= (others => '0');
            else
              modein_i(7 downto 0) <= cal_l1id1_in(23 downto 16);
            end if;
          else -- real TIM trigger
            modein_i(7 downto 0) <= l1id_bits_in(23 downto 16);
          end if;
          wr_eventheader_state <= write_hdr_word1;

        when write_hdr_word1 => 
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
         -- 
          if (fifo_ff_i(0) = '0') then
            fifo_wren_i(0) <= '1';
            wr_eventheader_state <= setup_word2;
          end if;

        when setup_word2 =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
          modein_i(3 downto 0) <= '0' & rol_test_in & NOT boc_ok_in & NOT tim_ok_in;
--  Set up Next Word
          if (cal_sp0_i = '1') then
            modein_i(15 downto 4) <= cal_bcid0_in;
          elsif (cal_sp1_i = '1') then
            modein_i(15 downto 4) <= cal_bcid1_in;
          else
            modein_i(15 downto 4) <= bcid_bits_in;
          end if;

          wr_eventheader_state <= write_hdr_word2;

        when write_hdr_word2 =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
         -- 
          if (fifo_ff_i(0) = '0') then
            fifo_wren_i(0) <= '1';
            wr_eventheader_state <= increment_header_count;
          end if;

        when increment_header_count =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '1';
          cal_sp0_i      <= '0';
          cal_sp1_i      <= '0';
         -- 
          wr_eventheader_state <= idle;

        when others =>
          fifo_wren_i(0) <= '0';
          lbid_cnt_up_i  <= '0';
          --
          wr_eventheader_state <= reset;
      end case;
    else
      fifo_wren_i(0) <= '0';
      modein_i       <= (others => '0');
      lbid_cnt_up_i  <= '0';
      cal_sp0_i      <= '0';
      cal_sp1_i      <= '0';
      --
      wr_eventheader_state <= reset;
    end if; 
  end if;  
end process wr_eventheader_to_fifo;

-------------------------------------------------------------------------------
-- Trigger Type Writein_state machine
--  For each trigger write to 16 bit wide FIFO:
--    Word 1: ATLAS/TIM Trigger Type

wr_trigtype_to_fifo : process (
  clk, 
  rst, 
  cal_l1a0_in, 
  cal_l1a1_in,
  new_ttype_valid_in_i, 
  fifo_ef_i, 
  fifo_ff_i,
  wr_trigtype_state
  )
begin
  if (rst = '1') then
    trgt_cnt_up_i  <= '0';
    fifo_wren_i(1) <= '0';
    modett_i       <= (others => '0'); 
    wr_trigtype_state <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '1') then
      case wr_trigtype_state is
        when reset =>
          trgt_cnt_up_i  <= '0';
          fifo_wren_i(1) <= '0';
          modett_i       <= (others => '0'); 
          -- 
          wr_trigtype_state <= idle;

        when idle =>
-- start a readout sequence when ttype information is available
          fifo_wren_i(1) <= '0';
          trgt_cnt_up_i  <= '0';
          if (int_cal_mode_en_in = '1') then
            if (cal_l1a0_in = '1') then
              modett_i <= "000000" & cal_tt0_in(9 downto 0);
              wr_trigtype_state <= write_ttype_word;
            elsif (cal_l1a1_in = '1') then
              modett_i <= "000000" & cal_tt1_in(9 downto 0);
              wr_trigtype_state <= write_ttype_word;
            end if;
          elsif (new_ttype_valid_in_i = '1') then          
            modett_i <= "000000" & ttype_bits_in(9 downto 0);
            wr_trigtype_state <= write_ttype_word;
          end if;

        when write_ttype_word =>
          fifo_wren_i(1) <= '0';
          trgt_cnt_up_i  <= '0';
          if (fifo_ff_i(1) = '0') then
            wr_trigtype_state <= increment_ttype_count;
            fifo_wren_i(1) <= '1';
          end if;

        when increment_ttype_count =>
          fifo_wren_i(1) <= '0';
          trgt_cnt_up_i  <= '1';
          -- 
          wr_trigtype_state <= idle;

        when others =>
          trgt_cnt_up_i  <= '0';
          fifo_wren_i(1) <= '0';
          modett_i       <= (others => '0'); 
          --
          wr_trigtype_state <= reset;
      end case;
    else
      trgt_cnt_up_i  <= '0';
      fifo_wren_i(1) <= '0';
      modett_i       <= (others => '0'); 
      --
      wr_trigtype_state <= reset;
    end if; 
  end if;  
end process wr_trigtype_to_fifo;

--------------------------------------------------------------------------------
-- Dynamic mask fifo write in state machine
--  Copy the appropriate dynamic mask into the dynamic mask readout fifo
--  when an l1 trigger is received.
-- rod type fifo writein
wr_lut_to_fifo : process (
  clk, 
  rst, 
  wi_lut_pres_state,
  apply_default_mask_in, 
  apply_corrective_mask_in,
  fifo_ef_i, 
  fifo_ff_i
  )
begin
  if (rst = '1') then
    wi_lut_fine_state_load_i <= '0';
    lut_baseaddress_i        <= "1100";
    fifo_wren_i(4 downto 3)  <= (others => '0');
    mask_cnt_up_i            <= '0';
    rod_tt_wr_out            <= '0';
    -- 
    wi_lut_pres_state   <= reset;
  elsif (clk'event and clk = '1') then
    rod_tt_wr_out <= '0';
    if (control_in = '1') then
     case wi_lut_pres_state is
       when reset =>
         wi_lut_fine_state_load_i <= '1';
         lut_baseaddress_i        <= "1100";
         fifo_wren_i(4 downto 3)  <= (others => '0');
         mask_cnt_up_i            <= '0';
         wi_lut_pres_state        <= idle; 

      when idle =>
        wi_lut_fine_state_load_i  <= '1';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i             <= '0';
  -- start a writein sequence when the fifo has room
        if (apply_default_mask_in = '1') then
          lut_baseaddress_i <= "1100"; -- 0xCx
--        wi_lut_pres_state <= write_dynamic_mask;
          wi_lut_pres_state <= setup_rod_tt;
        elsif (apply_corrective_mask_in = '1') then
          lut_baseaddress_i <= "1101"; -- 0xDx
--        wi_lut_pres_state <= write_dynamic_mask;
          wi_lut_pres_state <= setup_rod_tt;
        end if; 

      when setup_rod_tt =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i            <= '0';
        if (rol_test_in = '1') then  -- modified in v1Bf 01/30/06
          rodtt_i <= "0000000000" & rol_test_count_in;
        else
          rodtt_i <= lut_out_data_i;
        end if;

        if (rod_tt_full_i = '0') then
          wi_lut_pres_state <= write_rod_tt;   
        end if;

      when write_rod_tt =>
        fifo_wren_i(4 downto 3) <= "10";
        mask_cnt_up_i           <= '0';
        rod_tt_wr_out           <= '1';
       --
        wi_lut_pres_state <= write_dynamic_mask;

      when write_dynamic_mask =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        fifo_wren_i(3)           <= '0'; -- why here?
        mask_cnt_up_i            <= '0';
        --
        wi_lut_pres_state <= target_fifo_full_wait;

      when target_fifo_full_wait =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i            <= '0';
        --
        if (fifo_ff_i(3) = '1') then
          wi_lut_pres_state <= target_fifo_full_wait;   
        elsif (complete_mask_written_i = '1') then
          wi_lut_pres_state <= increment_mask_count;
        else
          wi_lut_pres_state <= wr_data_to_fifo;
        end if;

      when wr_data_to_fifo =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= "01";
        mask_cnt_up_i            <= '0';
        --
        wi_lut_pres_state <= increment_dm_address;   

      when increment_dm_address =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i            <= '0';
        --
        wi_lut_pres_state <= target_fifo_full_wait;   

      when increment_mask_count =>
        wi_lut_fine_state_load_i <= '0';
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i            <= '1';
        --
        wi_lut_pres_state <= idle; 

      when others =>
        wi_lut_fine_state_load_i <= '0';
        lut_baseaddress_i        <= "1100"; -- 0xCx default address base
        fifo_wren_i(4 downto 3)  <= (others => '0');
        mask_cnt_up_i            <= '0';
        --
        wi_lut_pres_state <= idle;
     end case;
   else -- flush
     wi_lut_fine_state_load_i <= '0';
     lut_baseaddress_i        <= "1100"; -- 0xCx default address base
     fifo_wren_i(4 downto 3)  <= (others => '0');
     mask_cnt_up_i            <= '0';
     -- 
     wi_lut_pres_state <= reset;
    end if;
  end if;
end process wr_lut_to_fifo;


-- lut_writein fine state counter
wi_lut_fine_statecounter : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    wi_lut_fine_statecount_i <= (others => '0');
--  wi_lut_fine_statecount_i <= "0001";
  elsif (clk'event and clk = '1') then
    if (wi_lut_fine_state_load_i = '1') then 
      wi_lut_fine_statecount_i <= (others => '0');
--    wi_lut_fine_statecount_i <= "0001";
    elsif (fifo_wren_i(3) = '1' OR fifo_wren_i(4) = '1') then -- wren for word 3 (4th word) and dynamic mask words
      wi_lut_fine_statecount_i <= wi_lut_fine_statecount_i + 1; 
    end if;
  end if; 
end process wi_lut_fine_statecounter;

wi_lut_statusflags : process (
--  clk, originally existed
  rst, 
  wi_lut_fine_statecount_i
  )
begin
  if (rst = '1') then
    complete_mask_written_i <= '0';
  elsif (wi_lut_fine_statecount_i = 5) then -- was 13, to account for word 3 (4th word with rod_tt)
    complete_mask_written_i <= '1';
  else
    complete_mask_written_i <= '0';
  end if;
end process wi_lut_statusflags;

-------------------------------------------------------------------------------
-- lbtrosm -> (L1ID/BCID/TT Read Out State Machine)
-- tim header accumulator:  Copies/splices the header information from the
-- l1id/bcid and ttype fifos into the tim header fifo
--
lbtrosm : process (
  clk, 
  rst, 
  lbtrosm_pres_state
  )
variable read_delay : unsigned(2 downto 0);
begin
  if (rst = '1') then
    fifo_rden_i(1) <= '0'; -- tt and rod_tt fifos (shared read enable) for 1 word 
    fifo_rden_i(0) <= '0'; -- l1 bc tt fifo read enable (3 words)
    fifo_wren_i(2) <= '0'; -- to event header fifo for first 4 words 
    lbid_cnt_dn_i  <= '0';
    trgt_cnt_dn_i  <= '0';
    hedr_cnt_up_i  <= '0';
    event_ttype_i  <= (others => '0');
    read_delay     := (others => '0');
    lbtrosm_pres_state <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '1') then
      case lbtrosm_pres_state is
        when reset =>
          fifo_rden_i(1) <= '0';
          fifo_rden_i(0) <= '0';
          fifo_wren_i(2) <= '0';
          lbid_cnt_dn_i  <= '0';
          trgt_cnt_dn_i  <= '0';
          hedr_cnt_up_i  <= '0';
          event_ttype_i  <= (others => '0');
          read_delay     := (others => '0');

          lbtrosm_pres_state <= idle; 

        when idle =>
          fifo_rden_i(1) <= '0';
          fifo_rden_i(0) <= '0';
          fifo_wren_i(2) <= '0';
          lbid_cnt_dn_i  <= '0';
          trgt_cnt_dn_i  <= '0';
          hedr_cnt_up_i  <= '0';
          event_ttype_i  <= eheader_i;  -- L1ID(15:0)
          read_delay     := (others => '0');
          if (lbid_cnt_i > 0 AND trgt_cnt_i > 0) then 
            lbtrosm_pres_state <= lbt_write_L1ID;
          end if;

        when lbt_write_L1ID =>
          event_ttype_i <= eheader_i;  -- L1ID(15:0)
          if (fifo_ff_i(2) = '0') then
            fifo_wren_i(2) <= '1';     -- WR L1ID(15:0) to FIFO
            lbtrosm_pres_state <= readout_ECRID_L1ID;
          end if;

        when readout_ECRID_L1ID =>
          fifo_wren_i(2) <= '0';
          event_ttype_i <= eheader_i;  -- ECR_ID & L1ID(23:16)
          if (read_delay = 0) then
            fifo_rden_i(1 downto 0) <= "01";
            read_delay := read_delay + 1;
          elsif (read_delay = 4) then
            lbtrosm_pres_state <= lbt_write_ECRID_L1ID;
          else
            fifo_rden_i(1 downto 0) <= "00";
            read_delay := read_delay + 1;
          end if;

        when lbt_write_ECRID_L1ID =>
          read_delay := (others => '0');
          event_ttype_i <= eheader_i;  -- ECR_ID & L1ID(23:16)
          if (fifo_ff_i(2) = '0') then
            fifo_wren_i(2) <= '1';     -- WR ECR_ID & L1ID(23:16)
            lbtrosm_pres_state <= readout_BCID;
          end if;

        when readout_BCID => -- this read delay can be adjusted
          fifo_wren_i(2) <= '0';
          event_ttype_i <= eheader_i;  -- BCID & "00" & TIM & BOC
          if (read_delay = 0) then
            fifo_rden_i(1 downto 0) <= "01";
            read_delay := read_delay + 1;
          elsif (read_delay = 4) then
            lbtrosm_pres_state <= lbt_write_BCID;
          else
            fifo_rden_i(1 downto 0) <= "00"; -- single read cycle
            read_delay := read_delay + 1;
          end if;

        when lbt_write_BCID =>
          read_delay := (others => '0');
          event_ttype_i <= eheader_i;  -- BCID & "00" & TIM & BOC  , don't need this get rid of during ibl clean up?
          if (fifo_ff_i(2) = '0') then
            fifo_wren_i(2) <= '1';  -- WR BCID & "00" & TIM & BOC
            lbtrosm_pres_state  <= lbt_write_TT;
          end if;

        when lbt_write_TT =>
          fifo_wren_i(2) <= '0';
          if (read_delay = 2) then
            event_ttype_i <= rod_ttfifo_i(5 downto 0) & ttfifo_i(9 downto 0); -- ROD/TIM/ATLAS Trigger Type
            read_delay := read_delay + 1;
          elsif (read_delay = 4) then
            if (fifo_ff_i(2) = '0') then
              fifo_wren_i(2) <= '1';  -- WR Atlas/TIM/ROD Trigger Type
              lbtrosm_pres_state  <= readout_L1ID_TT;
            end if;
          else
            read_delay := read_delay + 1;
          end if;

        when readout_L1ID_TT =>
          read_delay := (others => '0');
          fifo_wren_i(2) <= '0';
          fifo_rden_i(1 downto 0) <= "11";
          lbtrosm_pres_state <= decrement_event_lbid_tt_count;

        when decrement_event_lbid_tt_count =>
          fifo_rden_i(1 downto 0) <= "00";
          lbid_cnt_dn_i <= '1';
          trgt_cnt_dn_i <= '1';
          hedr_cnt_up_i <= '0';

          lbtrosm_pres_state <= increment_event_header_count;

        when increment_event_header_count =>
          lbid_cnt_dn_i <= '0';
          trgt_cnt_dn_i <= '0';
          hedr_cnt_up_i <= '1';

          lbtrosm_pres_state <= lbtrosm_done; 

        when lbtrosm_done =>
          lbid_cnt_dn_i <= '0';
          trgt_cnt_dn_i <= '0';
          hedr_cnt_up_i <= '0';

          if (read_delay = 2) then
            lbtrosm_pres_state <= idle; 
          else
            read_delay := read_delay + 1;
          end if;

        when others =>
          fifo_rden_i(1 downto 0)    <= "00";
          fifo_wren_i(2)             <= '0';
          lbid_cnt_dn_i              <= '0';
          trgt_cnt_dn_i              <= '0';
          hedr_cnt_up_i              <= '0';

          lbtrosm_pres_state <= idle;
      end case;
    else -- real trigger from TIM
      fifo_rden_i(1 downto 0) <= "00";
      fifo_wren_i(2)          <= '0';
      lbid_cnt_dn_i           <= '0';
      trgt_cnt_dn_i           <= '0';
      hedr_cnt_up_i           <= '0';
      event_ttype_i           <= (others => '0');
      lbtrosm_pres_state      <= reset;
    end if; 
  end if;  
end process lbtrosm;

-------------------------------------------------------------------------------
-- dfmrosm -> (Dynamic Mask Read Out State Machine)
-- tim header + dynamic mask readout to the efb out the efb_header_dynamic_mask 
--  port --> data readout state machine
dfmrosm : process (
  clk, 
  rst,
  event_headermask_fifo_ff_n_in,
  dfmrosm_pres_state
  )
begin
  if (rst = '1') then
    dfmrosm_loadval_i          <= (others => '0');
    dfmrosm_fine_state_load_i  <= '0';
    dfmrosm_fine_state_cntdn_i <= '0';
    header_fifo_ren_i          <= '0';
    dnmask_fifo_ren_i          <= '0';
    efb_hdm_fifo_wen_n_i       <= '1';
    hedr_cnt_dn_i              <= '0';
    mask_cnt_dn_i              <= '0';
    read_trgr_indctr_q_i       <= '0';
    mux_select_i               <= '1';
    dfmw_count_i               <= (others => '0');
    event_l1id_out             <= (others => '1');
    dfmrosm_pres_state <= reset;
  elsif (clk'event and clk = '1') then
    if (control_in = '1') then 
      case dfmrosm_pres_state is
        when reset =>
          dfmrosm_loadval_i          <= (others => '0');
          dfmrosm_fine_state_load_i  <= '0';
          dfmrosm_fine_state_cntdn_i <= '0';
          header_fifo_ren_i          <= '0';
          dnmask_fifo_ren_i          <= '0';
          efb_hdm_fifo_wen_n_i       <= '1';
          hedr_cnt_dn_i              <= '0';
          mask_cnt_dn_i              <= '0';
          read_trgr_indctr_q_i       <= '0';
          mux_select_i               <= '1';
          dfmw_count_i               <= (others => '0');

          dfmrosm_pres_state <= idle; 

        when idle =>
          dfmrosm_loadval_i          <= "00100";
          dfmrosm_fine_state_load_i  <= '0';
          dfmrosm_fine_state_cntdn_i <= '0';
          header_fifo_ren_i          <= '0';
          dnmask_fifo_ren_i          <= '0';
          efb_hdm_fifo_wen_n_i       <= '1';
          hedr_cnt_dn_i              <= '0';
          mask_cnt_dn_i              <= '0';
          read_trgr_indctr_q_i       <= '0';
          mux_select_i               <= '1';
          dfmw_count_i               <= (others => '0');
-- start a readout sequence when there are at least 1header and 1mask
          if (hedr_cnt_i > 0 AND mask_cnt_i > 0) then 
            dfmrosm_pres_state <= load_header_wordcount;
          end if;

        when load_header_wordcount =>
          dfmrosm_loadval_i          <= "00100"; -- 4 header words
          dfmrosm_fine_state_load_i  <= '1';
          dfmrosm_fine_state_cntdn_i <= '0';
          header_fifo_ren_i          <= '0';
          dnmask_fifo_ren_i          <= '0';
          efb_hdm_fifo_wen_n_i       <= '1';
          hedr_cnt_dn_i              <= '0';
          mask_cnt_dn_i              <= '0';
          read_trgr_indctr_q_i       <= '0';
          mux_select_i               <= '1';

          dfmrosm_pres_state <= wait_a_cycle0;

        when wait_a_cycle0 =>
          dfmrosm_fine_state_load_i <= '0';
          dfmrosm_pres_state <= target_fifo_full_wait0;

        when target_fifo_full_wait0 =>
          dfmrosm_loadval_i          <= (others => '0'); -- already loaded so clean up
          dfmrosm_fine_state_cntdn_i <= '0';
          header_fifo_ren_i          <= '0';
          dnmask_fifo_ren_i          <= '0';
          efb_hdm_fifo_wen_n_i       <= '1';
          hedr_cnt_dn_i              <= '0';
          mask_cnt_dn_i              <= '0';
          read_trgr_indctr_q_i       <= '0';
          mux_select_i               <= '1';

          if (dfmrosm_fine_statecount_i = 0)  then -- done writing to the fifo, decrement count
            dfmrosm_pres_state <= decrement_header_count;
          elsif (event_headermask_fifo_ff_n_in = '1') then -- if count is still more than zero, need to keep reading out
            dfmrosm_pres_state <= wr_efb_id_data;
          end if;

        when wr_efb_id_data =>
          if (std_logic_vector(dfmw_count_i) = "010") then
            dfmrosm_pres_state <= target_fifo_full_wait0;
            dfmw_count_i       <= (others => '0');
          elsif (std_logic_vector(dfmw_count_i) = "000") then
            efb_hdm_fifo_wen_n_i       <= '0'; -- write to efb the headers and dynamic masks
            header_fifo_ren_i          <= '1'; -- read out next header
            dfmrosm_fine_state_cntdn_i <= '1';
            dfmw_count_i               <= dfmw_count_i + 1;
          else
            efb_hdm_fifo_wen_n_i       <= '1';
            header_fifo_ren_i          <= '0';
            dfmrosm_fine_state_cntdn_i <= '0';
            dfmw_count_i               <= dfmw_count_i + 1;
          end if;
          if (dfmrosm_fine_statecount_i = dfmrosm_loadval_i) then
            event_l1id_out(15 downto 0) <= evnthedr_i;
          end if;
          if (dfmrosm_fine_statecount_i = dfmrosm_loadval_i - 1) then
            event_l1id_out(23 downto 16) <= evnthedr_i(7 downto 0);
          end if;

        when decrement_header_count =>
          dfmrosm_loadval_i           <= (others => '0');
          dfmrosm_fine_state_load_i   <= '0';
          dfmrosm_fine_state_cntdn_i  <= '0';
          header_fifo_ren_i           <= '0';
          dnmask_fifo_ren_i           <= '0';
          efb_hdm_fifo_wen_n_i        <= '1';
          hedr_cnt_dn_i               <= '1';
          mask_cnt_dn_i               <= '0';
          read_trgr_indctr_q_i        <= '0';
          mux_select_i                <= '1';

          dfmrosm_pres_state <= load_dynamicmask_wordcount;

        when load_dynamicmask_wordcount =>
--        dfmrosm_loadval_i          <= "01100"; for 12 mask words in Pixel
          dfmrosm_loadval_i          <= "00100"; -- now for 4 mask words
          dfmrosm_fine_state_load_i  <= '1';
          dfmrosm_fine_state_cntdn_i <= '0';
          header_fifo_ren_i          <= '0';
          dnmask_fifo_ren_i          <= '0';
          efb_hdm_fifo_wen_n_i       <= '1';
          hedr_cnt_dn_i              <= '0';
          mask_cnt_dn_i              <= '0';
          read_trgr_indctr_q_i       <= '0';
          mux_select_i               <= '0';

          dfmrosm_pres_state <= wait_a_cycle1;

        when wait_a_cycle1 =>
          dfmrosm_fine_state_load_i <= '0';
          dfmrosm_pres_state <= target_fifo_full_wait1;

        when target_fifo_full_wait1 =>
          dfmrosm_loadval_i           <= (others => '0');
          dfmrosm_fine_state_cntdn_i  <= '0';
          header_fifo_ren_i           <= '0';
          dnmask_fifo_ren_i           <= '0';
          efb_hdm_fifo_wen_n_i        <= '1';
          hedr_cnt_dn_i               <= '0';
          mask_cnt_dn_i               <= '0';
          read_trgr_indctr_q_i        <= '0';
          mux_select_i                <= '0';

          if (dfmrosm_fine_statecount_i = 0)  then
            dfmrosm_pres_state <= decrement_mask_count;
          elsif (event_headermask_fifo_ff_n_in = '1') then 
            dfmrosm_pres_state <= wr_efb_dm_data;
          end if;

        when wr_efb_dm_data =>
          if (std_logic_vector(dfmw_count_i) = "010") then -- not just one but two sets of mask
            dfmrosm_pres_state <= target_fifo_full_wait1;
            dfmw_count_i       <= (others => '0');
          elsif (std_logic_vector(dfmw_count_i) = "000") then
            efb_hdm_fifo_wen_n_i       <= '0';
            dnmask_fifo_ren_i          <= '1';
            dfmrosm_fine_state_cntdn_i <= '1';
            dfmw_count_i               <= dfmw_count_i + 1;
          else
            efb_hdm_fifo_wen_n_i       <= '1';
            dnmask_fifo_ren_i          <= '0';
            dfmrosm_fine_state_cntdn_i <= '0';
            dfmw_count_i               <= dfmw_count_i + 1;
          end if;

        when decrement_mask_count =>
          dfmrosm_loadval_i           <= (others => '0');
          dfmrosm_fine_state_load_i   <= '0';
          dfmrosm_fine_state_cntdn_i  <= '0';
          header_fifo_ren_i           <= '0';
          dnmask_fifo_ren_i           <= '0';
          efb_hdm_fifo_wen_n_i        <= '1';
          hedr_cnt_dn_i               <= '0';
          mask_cnt_dn_i               <= '1';
          read_trgr_indctr_q_i        <= '0';
          mux_select_i                <= '0';

          dfmrosm_pres_state <= read_trig_indictor_que;

        when read_trig_indictor_que =>
          dfmrosm_loadval_i           <= (others => '0');
          dfmrosm_fine_state_load_i   <= '0';
          dfmrosm_fine_state_cntdn_i  <= '0';
          header_fifo_ren_i           <= '0';
          dnmask_fifo_ren_i           <= '0';
          efb_hdm_fifo_wen_n_i        <= '1';
          hedr_cnt_dn_i               <= '0';
          mask_cnt_dn_i               <= '0';
          read_trgr_indctr_q_i        <= '1';
          mux_select_i                <= '0';

          dfmrosm_pres_state <= reset;
-- this will tell us if this is a corrective trigger and needs to be stored into the correctived_trigger_fifo (how?)

        when others =>
          dfmrosm_loadval_i           <= (others => '0');
          dfmrosm_fine_state_load_i   <= '0';
          dfmrosm_fine_state_cntdn_i  <= '0';
          header_fifo_ren_i           <= '0';
          dnmask_fifo_ren_i           <= '0';
          efb_hdm_fifo_wen_n_i        <= '1';
          hedr_cnt_dn_i               <= '0';
          mask_cnt_dn_i               <= '0';
          read_trgr_indctr_q_i        <= '0';
          mux_select_i                <= '0';

          dfmrosm_pres_state <= reset;
      end case;
    else
      dfmrosm_loadval_i          <= (others => '0');
      dfmrosm_fine_state_load_i  <= '0';
      dfmrosm_fine_state_cntdn_i <= '0';
      header_fifo_ren_i          <= '0';
      dnmask_fifo_ren_i          <= '0';
      efb_hdm_fifo_wen_n_i       <= '1';
      hedr_cnt_dn_i              <= '0';
      mask_cnt_dn_i              <= '0';
      read_trgr_indctr_q_i       <= '0';
      mux_select_i               <= '1';
      dfmw_count_i               <= (others => '0');

      dfmrosm_pres_state <= reset;
    end if; 
  end if;  
end process dfmrosm;

-------------------------------------------------------------------------------
-- Keep track of the number of id, trigger type, header, and mask packets
--  in each of the internal fifos and output them to the register block
--  so this can be monitored by the master dsp.
-------------------------------------------------------------------------------
--
-- lbid_count_i : count the number of cached event headers 
-- 7bit l1id/bcid packet counter
lbid_counter : process (
  clk,
  rst,
  control_in
  )
begin
  if (rst = '1') then
    lbid_cnt_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if    (control_in = '0') then
      lbid_cnt_i <= (others => '0');
    elsif (lbid_cnt_up_i = '1' AND lbid_cnt_dn_i = '1')then
      lbid_cnt_i <= lbid_cnt_i;
    elsif (lbid_cnt_up_i = '1' AND lbid_cnt_i < 255) then
      lbid_cnt_i <= lbid_cnt_i + 1;
    elsif (lbid_cnt_dn_i = '1' AND lbid_cnt_i > 0 ) then
      lbid_cnt_i <= lbid_cnt_i - 1;
    end if;
  end if;
end process lbid_counter;

-------------------------------------------------------------------------------
-- trgt_count_i : count the number of cached event headers 
-- 7bit trigger type packet counter
trgt_counter : process (
  clk,
  rst,
  control_in
  )
begin
  if (rst = '1') then
    trgt_cnt_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if    (control_in = '0') then
      trgt_cnt_i <= (others => '0');
    elsif (trgt_cnt_up_i = '1' AND trgt_cnt_dn_i = '1') then
      trgt_cnt_i <= trgt_cnt_i;
    elsif (trgt_cnt_up_i = '1' AND trgt_cnt_i < 255) then
      trgt_cnt_i <= trgt_cnt_i + 1;
    elsif (trgt_cnt_dn_i = '1' AND trgt_cnt_i > 0 ) then
      trgt_cnt_i <= trgt_cnt_i - 1;
    end if;
  end if;
end process trgt_counter;

-------------------------------------------------------------------------------
-- hedr_count_i : count the number of cached event headers 
-- 7bit header packet counter
hedr_counter : process (
  clk,
  rst, 
  control_in
  )
begin
  if (rst = '1') then
    hedr_cnt_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if    (control_in = '0') then
      hedr_cnt_i <= (others => '0');
    elsif (hedr_cnt_up_i = '1' AND hedr_cnt_dn_i = '1') then
      hedr_cnt_i <= hedr_cnt_i;
    elsif (hedr_cnt_up_i = '1' AND hedr_cnt_i < 255) then
      hedr_cnt_i <= hedr_cnt_i + 1;
    elsif (hedr_cnt_dn_i = '1' AND hedr_cnt_i > 0 ) then
      hedr_cnt_i <= hedr_cnt_i - 1;
    end if;
  end if;
end process hedr_counter;

-------------------------------------------------------------------------------
-- trgt_count_i : count the number of cached event headers 
-- 7bit header packet counter
mask_counter : process ( 
  clk,
  rst,
  control_in
  )
begin
  if (rst = '1') then
    mask_cnt_i  <= (others => '0');
  elsif (clk'event and clk = '1') then
    if    (control_in = '0') then
      mask_cnt_i <= (others => '0');
    elsif (mask_cnt_up_i = '1' AND mask_cnt_dn_i = '1') then
      mask_cnt_i <= mask_cnt_i;
    elsif (mask_cnt_up_i = '1' AND mask_cnt_i < 255) then
      mask_cnt_i <= mask_cnt_i + 1;
    elsif (mask_cnt_dn_i = '1' AND mask_cnt_i > 0 ) then
      mask_cnt_i <= mask_cnt_i - 1;
    end if;
  end if;
end process mask_counter;

-------------------------------------------------------------------------------
-- EventHeader and DynamicMask readout statemachine "hdmrosm" fine state counter
fine_statecounter1 : process (
  clk, 
  rst, 
  dfmrosm_loadval_i,
  dfmrosm_fine_state_load_i,
  dfmrosm_fine_state_cntdn_i
  )
begin
  if (rst = '1') then
    dfmrosm_fine_statecount_i <= (others => '1');
  elsif (clk'event and clk = '1') then
    if (dfmrosm_fine_state_load_i  = '1') then
      dfmrosm_fine_statecount_i <= dfmrosm_loadval_i;
    elsif (dfmrosm_fine_state_cntdn_i = '1') then
      dfmrosm_fine_statecount_i <= dfmrosm_fine_statecount_i - "00001"; 
    end if;
  end if; 
end process fine_statecounter1;

-------------------------------------------------------------------------------
-- combine the EventHeader then the DynamicMask out to the EFB
-- 3 words of header and 13 words of dynamicmask
mux1 : process (
  mux_select_i, 
  dynmkmsk_i, 
  evnthedr_i
  )
begin
  case (mux_select_i) is
    when '0'    => evnt_hedrmsk_i(15 downto 0) <= dynmkmsk_i(15 downto 0);
    when others => evnt_hedrmsk_i(15 downto 0) <= evnthedr_i(15 downto 0);
  end case;
end process mux1;


-------------------------------------------------------------------------------
-- This generates the register/lut/fifo transfer acknowledge to the dsp
--  and generates the internal we strobes to the dynamicmask lut.
reg_transfer_acknowledge_generator : process (
  clk,
  rst, 
  ce_n_in
  )
begin
  if (rst = '1') then
    ack_out        <= '0';
    ce_n_in_dly1_i <= '1';
    lut_write_en_i <= '0';
      --
    acknowledge_cycle_state <= reset;

  elsif (clk'event and clk = '1') then
    ce_n_in_dly1_i <= ce_n_in; -- en_n_in negative edge delay detection reg

    case acknowledge_cycle_state is
      when reset =>
        ack_out        <= '0';
        lut_write_en_i <= NOT ce_n_in AND NOT rnw_in;
      --
        if (ce_n_in = '0' AND ce_n_in_dly1_i = '1') then
          acknowledge_cycle_state <= wait_0_clk;
        end if;

      when wait_0_clk =>
        ack_out        <= '0';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= wait_1_clk;

      when wait_1_clk =>
        ack_out        <= '0';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= wait_2_clk;

      when wait_2_clk =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= wait_3_clk;

      when wait_3_clk =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --
        acknowledge_cycle_state <= acknowledge;

      when acknowledge =>
        ack_out        <= '1';
        lut_write_en_i <= '0';
      --

        if (ce_n_in = '1') then
          acknowledge_cycle_state <= reset;
        end if;
    end case;
  end if;
end process reg_transfer_acknowledge_generator;


end rtl; -- end code for efb_headerdynamicmask_encoder

