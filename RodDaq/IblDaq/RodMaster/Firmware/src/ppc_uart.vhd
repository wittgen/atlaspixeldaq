---------------------------------------------------------------------------------------------------
--
-- Title       : ppc_uart
-- Design      : robin_v4
-- Author      : Erich Krause, M. Mueller
-- Company     : Uni-Mannheim, LS Inf. V
--
---------------------------------------------------------------------------------------------------
--
-- File        : ppc_uart.vhd
-- Generated   : Tue Nov 16 11:07:29 2004
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.20
--
---------------------------------------------------------------------------------------------------
--
-- Description : This module implements an UART to allow a simple communtion with the PPC over
--               its serial interface. The client side is 8bit oriented.
--
---------------------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
-- synthesis translate_off
library unisim;
use unisim.all;
-- synthesis translate_on

entity ppc_uart is
  port(
    -- system interface
    clk: in std_logic; -- system clock @ 66MHz
    reset: in std_logic; -- asynchronous reset, active high
    -- serial interface
    serial_data_in: in std_logic; -- serial data input from the ppc
    serial_data_out: out std_logic; -- serial data out to the ppc
    -- client write interface
    write_data: in std_logic_vector(7 downto 0); -- byte to transmit
    write_en: in std_logic; -- write enable flag, active high
    write_fifo_wr_count: out std_logic_vector(4 downto 0); -- the amount of words in the write fifo = fill level
    write_fifo_full: out std_logic; -- full flag of the write fifo
    -- client read interface
    read_data: out std_logic_vector(7 downto 0); -- byte to read
    read_en: in std_logic; -- read enable flag, active high
    read_fifo_empty: out std_logic; -- empty flag of the read fifo
    read_fifo_rd_count: out std_logic_vector(14 downto 0) -- the fill level of the read fifo = amount of word in the read fifo
    );
end ppc_uart;

architecture ppc_uart_arch of ppc_uart is
  
  -------------------------------------------------------------------------------------------
  -- Components
  -------------------------------------------------------------------------------------------
  
  attribute syn_black_box: boolean;   
  -- Component declaration of the "srlc16e(srlc16e_v)" unit
  -- File name contains "srlc16e" entity: ./src/unisim.vhd
  component srlc16e
    -- synthesis translate_off
    generic(
      INIT: bit_vector := X"0000"
      );
    -- synthesis translate_on
    port(
      D : in std_ulogic;
      CE : in std_ulogic;
      CLK : in std_ulogic;
      A0 : in std_ulogic;
      A1 : in std_ulogic;
      A2 : in std_ulogic;
      A3 : in std_ulogic;
      Q : out std_ulogic;
      Q15 : out std_ulogic);
  end component;
  -- synthesis translate_off
  for all: srlc16e use entity unisim.srlc16e(srlc16e_v);
  -- synthesis translate_on
  attribute syn_black_box of srlc16e: component is true;
  
  -- Component declaration of the "srl16e(srl16e_v)" unit
  -- File name contains "srl16e" entity: ./src/unisim.vhd
  component srl16e
    -- synthesis translate_off
    generic(
      INIT: bit_vector := X"0000"
      );
    -- synthesis translate_on
    port(
      D : in std_ulogic;
      CE : in std_ulogic;
      CLK : in std_ulogic;
      A0 : in std_ulogic;
      A1 : in std_ulogic;
      A2 : in std_ulogic;
      A3 : in std_ulogic;
      Q : out std_ulogic);
  end component;
  -- synthesis translate_off
  for all: srl16e use entity unisim.srl16e(srl16e_v);
  -- synthesis translate_on
  attribute syn_black_box of srl16e: component is true;     
    
  -- Init values for the srl 's
  -- used for P&R
  attribute INIT: string;
  attribute INIT of srlc16e: component is "0000000000000001";
  attribute INIT of srl16e: component is "0000000000000000";        
    
  component ppc_uart_write_fifo -- 8 bit x 16 words, distributed memory based
    port (
      clk: in std_logic;
      srst: in std_logic;
      din: in std_logic_vector(7 downto 0);
      wr_en: in std_logic;
      rd_en: in std_logic;
      dout: out std_logic_vector(7 downto 0);
      full: out std_logic;
      empty: out std_logic;
      data_count: out std_logic_vector(4 downto 0)
      );
  end component;
  attribute syn_black_box of ppc_uart_write_fifo: component is true;
    
  component ppc_uart_read_fifo -- 8 bit x 2047 words, built memory based, uses 1 BRAM
    port (
      clk: in std_logic;
      srst: in std_logic;
      din: in std_logic_vector(7 downto 0);
      wr_en: in std_logic;
      rd_en: in std_logic;
      dout: out std_logic_vector(7 downto 0);
      full: out std_logic;
      empty: out std_logic;
      data_count: out std_logic_vector(14 downto 0)
      );
  end component;
  attribute syn_black_box of ppc_uart_read_fifo: component is true;
    
  -------------------------------------------------------------------------------------------
  -- Constants
  -------------------------------------------------------------------------------------------

  -- Stage divider calculation: 
  -- Output clock = Input clock / ( 2 * (STAGE_DIVIDER + 2))
  --constant STAGE_DIVIDER :std_logic_vector(4 downto 0) := "1" & X"0";  
  constant STAGE_DIVIDER :std_logic_vector(4 downto 0) := "01001";  
  constant RECEIVE_PHASE_SHIFT : integer := 4;

  
  -------------------------------------------------------------------------------------------
  -- Signals
  -------------------------------------------------------------------------------------------  

  --------------------- The sending part ----------------------
  
  -- The transmit FSM
  type transmit_fsm_state_type is (REQUEST_DATA, WAIT_FOR_SHIFT, SHIFT_DATA, GET_DATA);
  signal transmit_fsm_state, transmit_fsm_next_state: transmit_fsm_state_type;
  
  -- The uart send clock multiplied with 16.
  signal uart_send_clk_mul_16  : std_logic := '0';  
  
  -- The rising edge signal which indicates that a new bit can be sent
  signal uart_send_clk_rising_edge : std_logic := '0';  
      
   
  -- Write fifo signals. The write fifo contains the data to be sent to the PPC
  signal write_fifo_read_en: std_logic;
  signal write_fifo_empty: std_logic;
  signal write_fifo_read_data: std_logic_vector(7 downto 0);
  
  -- Other transmit signals
  signal last_bit_transmitted: std_logic := '0';
  signal transmit_bit_counter: std_logic_vector(3 downto 0) := (others => '0');
  signal transmit_register: std_logic_vector(9 downto 0) := (others => '0');
  
  -- Signals for clock generation
  -- First divider stage
  signal divider_stage_srl_output            : std_logic := '0';
  signal divider_stage_srl0_output           : std_logic := '0';
  signal divider_stage_srl1_output           : std_logic := '0';
  signal divider_stage_srl_carry             : std_logic := '0';
  signal divider_stage_srl_feedback          : std_logic := '0';  
  signal divider_stage_stage_1_active        : std_logic := '0';
  
  -- Second divider stage for the send clock. Divides by 16
  signal snd_divider_stage_16_srl_output         : std_logic;
  signal snd_divider_stage_16_srl_feedback       : std_logic;  
  signal snd_divider_stage_16_srl_feedback_delayed : std_logic;

  
  --------------------- The receiving part ----------------------
  
  -- The FSM
  type receive_fsm_state_type is (IDLE, WAIT_FOR_SHIFT, SHIFT_IN);
  signal receive_fsm_state, receive_fsm_next_state: receive_fsm_state_type;

  -- The read fifo signals. This fifo gets the data which is received from the PPC
  signal read_data_int: std_logic_vector(read_data'length - 1 downto 0);
  signal read_fifo_empty_int: std_logic;
  signal read_fifo_rd_count_int: std_logic_vector(read_fifo_rd_count'length - 1 downto 0);  
  signal read_fifo_write_data: std_logic_vector(7 downto 0) := (others => '0');
  signal read_fifo_write_en: std_logic;
  signal read_fifo_write_en_delayed : std_logic;
  signal read_fifo_write_en_sync    : std_logic;  
  signal read_fifo_full: std_logic;
    
  signal serial_data_in_reg: std_logic_vector(3 downto 0) := (others => '0');
  signal receive_clk_counter: std_logic_vector(5 downto 0) := (others => '0');
  signal receive_clk_rising_edge: std_logic := '0';
  signal last_bit_received: std_logic := '0';
  signal received_bit_counter: std_logic_vector(3 downto 0) := (others => '0');
  
  signal serial_data_in_inverted : std_logic;
  signal serial_data_out_inverted : std_logic;
  
begin
  
  serial_data_in_inverted <= NOT serial_data_in;
  serial_data_out <= NOT serial_data_out_inverted;
  
  --------------------- Sender clock generation ----------------------
  
  -- The clock generation for the uart sending is using a two stage clock divider.
  -- Both stages use the srl shift registers. The first divider stage uses two 
  -- concatenated 16 bit srl's. The SRL rotate 1 bit and generate a pulse which
  -- toggle the clock with each round-trip. This means one round trip is half
  -- a clock cycle. The divider can be adjusted by connecting the feedback signal
  -- a number of bits earlier or later to the beginning. This reduces or enlarges
  -- the roundtrip by a number of bits. 
  -- The second divider stage divides the clock by 16. Since only a pulse is required
  -- to shift a bit no real clock is generated. The pulse is generated with
  -- each shift-register round-trip.

  -- First Divider stage
  divider_stage : block
    
  begin
    
    -- The two concatenated shift register primitives
    divider_stage_0 : srlc16e
    -- synthesis translate_off    
    generic map(
      INIT => X"0001"
      )
    -- synthesis translate_on
    port map(
      D   => divider_stage_srl_feedback,
      CE  => '1',
      CLK => clk,
      A0  => STAGE_DIVIDER(0),
      A1  => STAGE_DIVIDER(1),
      A2  => STAGE_DIVIDER(2),
      A3  => STAGE_DIVIDER(3),
      Q   => divider_stage_srl0_output,  -- Take the signal already here if the divider is smaller the 16!
      Q15 => divider_stage_srl_carry   -- Carry the signal to the next stage
    );
        
    divider_stage_1: srl16e
    -- synthesis translate_off
    generic map(
      INIT => X"0000"
      )
    -- synthesis translate_on
    port map(
      D => divider_stage_srl_carry,
      CE => '1', 
      CLK => clk,
      A0 => STAGE_DIVIDER(0),
      A1 => STAGE_DIVIDER(1),
      A2 => STAGE_DIVIDER(2),
      A3 => STAGE_DIVIDER(3),
      Q => divider_stage_srl1_output
    );	            
      
    -- Concatenating the two dividers requires may result in the problem that for small dividers
    -- the output of the first shift register has to be used. This is done by this multiplexer.
    -- (see Xilinx Virtex II user guide!)
    divider_stage_stage_1_active <= STAGE_DIVIDER(4);
      
    divider_stage_srl_output <= divider_stage_srl1_output when divider_stage_stage_1_active = '1' else
                  divider_stage_srl0_output;
      	
    -- the last stage of the shift register is using a DFF
    shift_register_last_stage_proc: process(clk)
    begin
      if rising_edge(clk) then
        if reset = '1' then
          divider_stage_srl_feedback <= '0';
        end if;        
        divider_stage_srl_feedback <= divider_stage_srl_output after 1 ns;
      end if;
    end process;
    
    -- No toggle the clock with each round-trip.
    clk_divider_proc: process(clk)
    begin
      if rising_edge(clk) then
        -- divide by 64
        if divider_stage_srl_feedback = '1' then
          uart_send_clk_mul_16 <= not uart_send_clk_mul_16 after 1 ns;		
        end if;										  
      end if;
    end process;  	
    
  end block;
  

  -- This is the second divider stage which generates a send pulse each 16th clock
  -- The pulse is sync. to the clk which is used for the fifos and the FSMs
  sender_divider_stage_16: block 
  begin

    -- This srl shifts one bit for 15 clock cycles
    divider_stage: srlc16e
    -- synthesis translate_off
    generic map(
      INIT => X"0001"
      )
    -- synthesis translate_on
    port map(
      D => snd_divider_stage_16_srl_feedback,
      CE => '1', 
      CLK => uart_send_clk_mul_16,
      A0 => '0',
      A1 => '1',
      A2 => '1',
      A3 => '1',
      Q => snd_divider_stage_16_srl_output,
      Q15 => open
      );	  
  	
    -- the last stage of the shift register is using a DFF
    -- This makes the feedback signal raise after 16 clocks
    shift_register_last_stage_proc: process(uart_send_clk_mul_16)
    begin
      if rising_edge(uart_send_clk_mul_16) then
        snd_divider_stage_16_srl_feedback <= snd_divider_stage_16_srl_output after 1 ns;
      end if;
    end process;           
    
    
    -- generate a send pulse in sync with clk
    clk_divider_proc: process(clk, reset)
    begin
      if reset = '1' then
        uart_send_clk_rising_edge <= '0' after 1 ns;
      elsif rising_edge(clk) then      
        -- divide by 64
        if snd_divider_stage_16_srl_feedback = '1' and snd_divider_stage_16_srl_feedback_delayed = '0'then
          uart_send_clk_rising_edge <= '1' after 1 ns;
        else
          uart_send_clk_rising_edge <= '0' after 1 ns;
        end if;										  
        snd_divider_stage_16_srl_feedback_delayed <= snd_divider_stage_16_srl_feedback after 1 ns;
      end if;
    end process;  	

  end block;  
  
  --------------------------------------------------------------------
  
  
  --------------------- Transmission ---------------------------------
  
  -- The write fifo
  -- contains the data to be transmitted
  write_fifo: ppc_uart_write_fifo -- 8 bit x 16 words, distributed memory based
  port map(
    -- system interface
    clk => clk,
    srst => reset,
    -- write interface
    din => write_data, 
    wr_en => write_en,
    full => write_fifo_full,
    data_count => write_fifo_wr_count,
    -- read interface
    rd_en => write_fifo_read_en,
    dout => write_fifo_read_data,
    empty => write_fifo_empty
    );
  

  -- The transmit engine
  -- transmit fsm: combinational part. Determines how to travel through the FSM
  transmit_fsm_comb_proc: process(transmit_fsm_state, write_fifo_empty, uart_send_clk_rising_edge,
    last_bit_transmitted)
  begin
    case transmit_fsm_state is
      when REQUEST_DATA =>
      if write_fifo_empty = '0' then
        -- the fifo contains a new byte to transmit
        transmit_fsm_next_state <= GET_DATA;
      else
        -- the fifo is empty, wait for the arrival of a new byte
        transmit_fsm_next_state <= REQUEST_DATA;
      end if;
      
      when GET_DATA =>
      -- initialize the shift register
      transmit_fsm_next_state <= WAIT_FOR_SHIFT;
      
      when WAIT_FOR_SHIFT =>
      if last_bit_transmitted = '1' then
        -- the last bit was transmitted
        transmit_fsm_next_state <= REQUEST_DATA;
      elsif uart_send_clk_rising_edge = '1' then
        -- tramsmit the next bit at the rising edge of the clock divided by 64
        transmit_fsm_next_state <= SHIFT_DATA;
      else
        -- idle
        transmit_fsm_next_state <= WAIT_FOR_SHIFT;
      end if;
      
      when SHIFT_DATA =>
      -- create a pulse to shift and leave the state
      transmit_fsm_next_state <= WAIT_FOR_SHIFT;
      
      when others =>
      transmit_fsm_next_state <= REQUEST_DATA;
      
    end case;
  end process;
  
  -- transmit fsm: sequential part. Clocks the FSM
  transmit_fsm_seq_proc: process(clk, reset)
  begin
    if reset = '1' then
      transmit_fsm_state <= REQUEST_DATA after 1 ns;
    elsif rising_edge(clk) then
      transmit_fsm_state <= transmit_fsm_next_state after 1 ns;
    end if;
  end process;
  
  -- Here a pulse is generated which reads the fifo
  write_fifo_read_en <= '1' when transmit_fsm_state = REQUEST_DATA else '0';
  
  -- serializer.
  -- Generates the start bit, data bit and stop bit upon the state of the
  -- transmission FSM.
  transmit_serializer_proc: process(clk, reset)
  begin
    if reset = '1' then
      serial_data_out_inverted <= '0' after 1ns;
      last_bit_transmitted <= '0' after 1 ns;
      transmit_bit_counter <= (others => '0') after 1 ns;
      transmit_register <= (others => '0') after 1 ns;
    elsif rising_edge(clk) then
      -- IOB register
      if transmit_fsm_state = SHIFT_DATA then
        serial_data_out_inverted <= transmit_register(0) after 1 ns;
      end if;
      
      if transmit_fsm_state = GET_DATA then
        -- initialize the register: STOP_BIT & data_byte & START_BIT
        transmit_register <= '0' & (NOT write_fifo_read_data) & '1' after 1 ns;
      elsif transmit_fsm_state = SHIFT_DATA then
        -- shift out the LSB
        transmit_register <= '0' & transmit_register(9 downto 1) after 1 ns;
      end if;
      
      if transmit_fsm_state = SHIFT_DATA then
        -- count with every shift
        transmit_bit_counter <= transmit_bit_counter + '1' after 1 ns;
      elsif transmit_fsm_state = WAIT_FOR_SHIFT and last_bit_transmitted = '1' then
        -- reset the counter when the last bit transmittion detected by the fsm
        transmit_bit_counter <= (others => '0') after 1 ns;
      end if;
      
      if transmit_bit_counter = CONV_STD_LOGIC_VECTOR(9, 4) and transmit_fsm_state = SHIFT_DATA then
        last_bit_transmitted <= '1' after 1 ns;
      elsif transmit_fsm_state = WAIT_FOR_SHIFT then
        last_bit_transmitted <= '0' after 1 ns;
      end if;
    end if;
  end process;
  
  --------------------------------------------------------------------
  
  
  --------------------- Receive part ---------------------------------

  -- receive clock generation
  -- The clock is dervied from the send clock multiplied by 16. This is generated
  -- in the first part of this architecture. To sync with the clock of the incoming
  -- serial data, the pase is shifted when the start bit is detected in a 
  -- way that the signal is sampeled in the middle.
  receive_clk_generator_proc: process(uart_send_clk_mul_16, reset)
  begin
    if reset = '1' then
      receive_clk_counter <= CONV_STD_LOGIC_VECTOR(15 + RECEIVE_PHASE_SHIFT, 6) after 1 ns;
      receive_clk_rising_edge <= '0' after 1 ns;
    elsif rising_edge(uart_send_clk_mul_16) then      
      if receive_fsm_state = IDLE then
        -- start bit detected, start counting the clock cycles for the division
        -- The phase shift enshures that we smaple in the middle of the signal
        receive_clk_counter <= CONV_STD_LOGIC_VECTOR(15 + RECEIVE_PHASE_SHIFT, 6) after 1 ns;
      elsif receive_clk_counter = "000000" then
        -- If no data is received load the counter with 15.
        receive_clk_counter <= CONV_STD_LOGIC_VECTOR(15, 6) after 1 ns;        
      else
        -- count during receiving
        receive_clk_counter <= receive_clk_counter - '1' after 1 ns;
      end if;
      
      if receive_clk_counter = "000000" then
        -- create a pulse for the 16th clock edge
        receive_clk_rising_edge <= '1' after 1 ns;
      else
        receive_clk_rising_edge <= '0' after 1 ns;
      end if;
    end if;
  end process;
  
  
  -- synthronize register for the incoming serial data
  -- sync to the system clock
  receive_data_synchronizer_proc: process(uart_send_clk_mul_16)
  begin
    if rising_edge(uart_send_clk_mul_16) then
      serial_data_in_reg <= serial_data_in_reg(2 downto 0) & serial_data_in_inverted after 1 ns;
    end if;
  end process;
  
  -- receive fsm: combinational part
  receive_fsm_comb_proc: process(receive_fsm_state, serial_data_in_reg(2 downto 1), receive_clk_rising_edge,
    last_bit_received)
  begin
    case receive_fsm_state is
      when IDLE =>
      if serial_data_in_reg(2) = '0' and serial_data_in_reg(1) = '1' then
        -- a start bit is detected, begin the deserializing process
        receive_fsm_next_state <= WAIT_FOR_SHIFT;
      else
        -- wait for the start bit
        receive_fsm_next_state <= IDLE;
      end if;
      
      when WAIT_FOR_SHIFT =>
      if last_bit_received = '1' and receive_clk_rising_edge = '1' then
        -- last data bit was received, leave the state with the stop bit
        receive_fsm_next_state <= IDLE;
      elsif receive_clk_rising_edge = '1' then
        -- shift in 1 bit
        receive_fsm_next_state <= SHIFT_IN;
      else
        -- wait for the next bit to receive
        receive_fsm_next_state <= WAIT_FOR_SHIFT;
      end if;
      
      when SHIFT_IN =>
      -- store the received bit and wait for the next one to arrive
      receive_fsm_next_state <= WAIT_FOR_SHIFT;
      
      when others =>
      receive_fsm_next_state <= IDLE;
    end case;
  end process;
  
  -- receive fsm: sequential part
  receive_fsm_seq_proc: process(uart_send_clk_mul_16, reset)
  begin
    if reset = '1' then
      receive_fsm_state <= IDLE after 1 ns;
    elsif rising_edge(uart_send_clk_mul_16) then
      receive_fsm_state <= receive_fsm_next_state after 1 ns;
    end if;
  end process;
  
  -- receiver management
  receiver_manager_proc: process(uart_send_clk_mul_16)
  begin
    if rising_edge(uart_send_clk_mul_16) then
      if received_bit_counter = CONV_STD_LOGIC_VECTOR(8, 4) then        
        last_bit_received <= '1' after 1 ns;
      else
        last_bit_received <= '0' after 1 ns;
      end if;
      
      if receive_fsm_state = IDLE then
        received_bit_counter <= (others => '0') after 1 ns;
      elsif receive_fsm_state = SHIFT_IN then
        -- count with every received data bit
        received_bit_counter <= received_bit_counter + '1' after 1 ns;
      end if;
      
      -- receive shift register
      if receive_fsm_state = SHIFT_IN then
        -- the LSB arrives first, so shift accordingly
        read_fifo_write_data <= (NOT serial_data_in_reg(3)) & read_fifo_write_data(7 downto 1) after 1 ns;
      end if;
    end if;
  end process;  
  
  -- Generate a write pulse to the read fifo when a complete
  -- data byte has been received
  receive_fsm_output_decoder_proc: process(uart_send_clk_mul_16, reset)
  begin
    if reset = '1' then
      read_fifo_write_en <= '0' after 1 ns;
    elsif rising_edge(uart_send_clk_mul_16) then
      if receive_fsm_state = WAIT_FOR_SHIFT and read_fifo_full = '0'
        and last_bit_received = '1' and receive_clk_rising_edge = '1' then
        -- Write the data byte from the receive shift register into the receive fifo
        -- In case the fifo is full, the received byte is discarded.
        read_fifo_write_en <= '1' after 1ns;
      else
        read_fifo_write_en <= '0' after 1ns;
      end if;
    end if;
  end process;    
  
  -- Synchronise the read_fifo_write_en to clk to supply it to the read fifo.
  read_fifo_write_en_sync_process : process (clk, reset)
  begin
    if reset = '1' then
      read_fifo_write_en_sync <= '0' after 1 ns;
    elsif rising_edge(clk) then
      if read_fifo_write_en = '1' and read_fifo_write_en_delayed = '0' then
        read_fifo_write_en_sync <= '1' after 1 ns;
      else
        read_fifo_write_en_sync <= '0' after 1 ns;
      end if;      
      read_fifo_write_en_delayed <= read_fifo_write_en after 1 ns;
    end if;  
  end process;
      
  read_fifo: ppc_uart_read_fifo -- 8 bit x 2048 words, built memory based, uses 1 BRAM
  port map(
    -- system interface
    clk => clk,
    srst => reset,
    -- write interface
    din => read_fifo_write_data,
    wr_en => read_fifo_write_en_sync,
    full => read_fifo_full,
    -- read interface
    rd_en => read_en,
    dout => read_data_int,
    empty => read_fifo_empty_int,
    data_count => read_fifo_rd_count_int
    );
  read_data <= read_data_int after 1 ns;
  read_fifo_empty <= read_fifo_empty_int after 1 ns;
  read_fifo_rd_count <= read_fifo_rd_count_int after 1 ns;
   
end ppc_uart_arch;
