------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
------------------------------------------------------------------------------
--
-- Filename: fe_command_pulse_counter.vhd
-- Description:
--  Accumulates the L1triggers pulses and fe_cmd_mask_updated pulses 
--  from the front_end_command_processor and issues controller interrupts
--  and formatter_bankAB L1 trigger pulses after the receiving a signal to
--  indicate that the RRIF has sent the EFB Dynamic Mask to the EFB.
--  That is, it receives and counts the L1 triggers as well as accumulates
--  them into a fifo along with the front_end_command_processor pulses.
--  The fifo'd pulses are used to select the normal controller trigger 
--  interrupt or the alternate "mode mask has been updated" interrupt. 
--  The mode/mask interrupt is pulsed only once whenever a mode mask is
--  updated. 
--
--  Since triggers can arrive at a rate of 5 in 25 40MHz clks, and the 
--  controller cannot handle interrupts of that rapidity, it is necessary 
--  to buffer and issued these to the controller when its interrupt 
--  handler can deal with them. This is also the case with the 
--  efb_headerdynamicmask_encoder - in the best case when the rcf internal
--  dynamic_mask_fifo is not full, it takes: 
--  (13words * 2wait states/word) + 4 miscellaneous setup steps = 30 clocks!
--  BEFORE ANOTHER DYNAMIC MASK CAN BE WRITTEN. If there are additional waits
--  because the rcf internal dynamic_mask_fifo is full or not large enough
--  dynamic masks will be dropped and an error condition will occur!
--
--  In the best case: when the efb internal
--  writein fifo is not full, and so no waiting, it will take: 
--  (16 words * 4wait states per word) + 4 miscellaneaous clocks = 68clocks
--  to completely write in a dynamic mask to the efb from the rrif.
--
--  The counter is now used to allow time for the L1 Trigger accumulator
--  logic to operate correctly
--
--
--                         +----------------------+     
-- fe_cmdmask_updated------| 2048x2bit fifo       |------------+
-- fe_cmd_pulse---+------+-|                      |            |
--                |      +-|wen                ren|-----------+| 
--             +--|-------->                      |           || 
--             |  |        +----------------------+           ||
--             |  |                           +------------------fe_cmdpulse_count[4:0]
--             |  |                           |               ||
--             |  |    +--------------------+ |  +-------+    ||
--             |  |    | 5bit counter       |-+--| count |-+  ||
--             |  +----|inc                 |    |  > 0  | |  ||
--             +------->                 dec|-+  |       | |  ||
--             |       +--------------------+ |  +-------+ |  ||
--             |                              |            |  ||
--             |                              |            |  ||
--             |                              |            |  ||
--    clk -----+                              |            |  ||
--             |              +-----------------------+    |  ||
--             +--------------> readout_state_machine |    |  ||
--                            |                       |----+  ||
--                            |                       |       ||
--                            |                       |       ||                      
-- control(0 to 2)------------|                       |-------+|
--       efb_ready_in   ------|                       |--------+
--                        +---|                       |---fe_cmdpulse_formattersAB
--                        |   |                       |---controller_int_out[1:0]     
--                        |   |                       |                               
--                        |   |                       |-----------+                   
--                        |   +-----------------------+           |            
--                        |                                       |                  
--                        |                                       |                  
--                        |   +-------------------+               |               
--                        |   | delay counter     |               |                 
--                        |   |                   |   +-------+   |                
--                        |   |              count|---| count |---+                
--                        +---|countdown          |   |  = 0  |                    
--     fe_cmdpulse_delay_in---|preloadvalue       |   |       |                    
--                            |                   |   +-------+                         
--    clk --------------------> occupancy_count   |   
--                            +-------------------+   
--
--
------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
------------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    6 March 2000 - MLN first version : Requirements and functionality
--                   understood and suficiently documented to proceed 
--                   with coding.
--
--   20 Sept 2001  - JMJ : 
--                 Reordered State Machine:
--                   Post First Controller Interrupt immediately then wait to
--                   post FE Command Pulse.  Added "Done" state, and loop the
--                   state machine from "Done" to "Reset" as the state Reset
--                   does really reset the block, it just insures that the
--                   outputs to the rest of the RRIF are held in safe values.
--
--                 Changed Order of Trigger Counter:
--                   Previous Order would never get to the elseif test of a 
--                   L1 Trigger arriving at the exact time that a FE Command 
--                   Pulse is posted.
--
--                 Removed the Delay Counter:
--                   Removed the Delay Counter and added a pulse from the EFB
--                   dynamic mask encoder to indicate when the mask has been
--                   written to the EFB.  The FE Command pulse will be sent to
--                   Formatters after this signal has been received.
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
------------------------------------------------------------------------------
-- LIBRARY INCLUDES
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

------------------------------------------------------------------------------
-- PORT DECLARATION
------------------------------------------------------------------------------
entity fe_command_pulse_counter is
  port (
    clk                       : in  std_logic; -- clk40 input
    rst                       : in  std_logic; --  global reset

  -- 2: load counter, 1: enable transfer, 0: flush all
    control_in                : in  std_logic_vector(1 downto 0); 
    fe_cmdpulse_in            : in  std_logic; --
    corrective_fe_cmdpulse_in : in  std_logic; --
    fe_cmdpulse_count_out     : out std_logic_vector(7 downto 0); --
    fe_cmdpulse_AB_out        : out std_logic_vector(1 downto 0); --
    controller_int_out        : out std_logic_vector(1 downto 0); --
    command_cycle_done_out    : out std_logic; --
    store_current_trigger_out : out std_logic; --
    current_trigger_out       : out std_logic_vector(1 downto 0); --

  -- Pulse to indicate that the EFB DM has been sent to the EFB.
    dynamic_mask_sent_in      : in  std_logic;
    modebits_sent_in          : in  std_logic;
    efb_ready_in              : in  std_logic;
    fe_trigger_occupancy_out  : out std_logic_vector(10 downto 0);
    modebits_error_out        : out std_logic
    );
end fe_command_pulse_counter; 

architecture rtl of fe_command_pulse_counter is
------------------------------------------------------------------------------
-- SIGNAL DECLARATION
------------------------------------------------------------------------------

type states is (
  reset, 
  idle, 
  readout_trigger, 
  issue_controller_interrupt,
  wait_for_efbdm, 
  done, 
  done_delay
  );
signal pres_state : states;

type fe_cmd_pulse_states is (
  reset, 
  idle, 
  ready, 
  send_fe_pulse, 
  done
  );
signal fe_cmd_pulse_state : fe_cmd_pulse_states;

signal fe_cmdpulse_fmtrAB_i : std_logic_vector(1 downto 0);
signal fe_cmdpulse_count_i  : unsigned(7 downto 0);
signal fe_cmd_cycle_done_i  : std_logic;

signal trigger_inputs_i  : std_logic_vector(1 downto 0);
signal fifoed_triggers_i : std_logic_vector(1 downto 0);
signal trig_fifo_ren_i   : std_logic;
signal controller_int_i  : std_logic;

signal fifo_full_i : std_logic; -- empty flag
signal fifo_empty_i : std_logic; -- full flag

signal store_current_trigger_i : std_logic;

signal fmb_sent_i : std_logic;
signal mb_error_i : std_logic;

signal fe_trigger_counter : unsigned(10 downto 0);

------------------------------------------------------------------------------
-- COMPONENT DECLARATION
------------------------------------------------------------------------------

COMPONENT fifo_2048x2
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
  );
END COMPONENT;

------------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
------------------------------------------------------------------------------

  component icon
    PORT (
      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
  end component;

  component ila
    PORT (
      CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
      CLK : IN STD_LOGIC;
      TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TRIG1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      TRIG2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TRIG3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		TRIG4 : IN STD_LOGIC_VECTOR(35 DOWNTO 0)
		);
  end component;
  
    -- chipscope signals  
  signal control:					std_logic_vector(35 downto 0);
  signal trig0:					std_logic_vector(31 downto 0);
  signal trig1:					std_logic_vector(31 downto 0);
  signal trig2:					std_logic_vector(31 downto 0);
  signal trig3:					std_logic_vector(31 downto 0);
  signal trig4:					std_logic_vector(35 downto 0);
  signal debug_state:			std_logic_vector(2 downto 0);

begin

 withCs: if false generate
 begin
  icon_instance : icon
    port map (
      CONTROL0 => control);
  
  ila_instance : ila
    port map (
      CONTROL => control,
      CLK => clk,		-- 40 MHz
      TRIG0 => trig0,
      TRIG1 => trig1,
      TRIG2 => trig2,
		TRIG3 => trig3,
		TRIG4 => trig4);
  end generate;
  
  
  trig0(0)					<= controller_int_i;
  trig0(1)					<= fifoed_triggers_i(1);
  trig0(2)					<= fe_cmdpulse_in;
  trig0(3)					<= trig_fifo_ren_i;
  trig0(4)					<= fifo_full_i;
  trig0(5)					<= fifo_empty_i;  
  trig0(6)					<= efb_ready_in;
  trig0(7)					<= fmb_sent_i;
  trig0(8)					<= mb_error_i;
  trig0(11 downto 9)		<= debug_state;
  
  

trigger_inputs_i(0) <= fe_cmdpulse_in;
trigger_inputs_i(1) <= corrective_fe_cmdpulse_in;

fe_trigger_occupancy_out <= std_logic_vector(fe_trigger_counter);

trig_fifo : fifo_2048x2  --2048x2 FIFO
  port map (
    clk             => clk,
    rst             => rst,
    wr_en           => fe_cmdpulse_in,
    rd_en           => trig_fifo_ren_i,
    din             => trigger_inputs_i,
    dout            => fifoed_triggers_i,
    full            => fifo_full_i,
    empty           => fifo_empty_i,
    data_count      => open  -- fe_trigger_occupancy_out
    );

------------------------------------------------------------------------------
-- PROCESS DECLARATION
------------------------------------------------------------------------------

command_gen : process (
  clk,
  rst,
  control_in
  )
variable done_counter : unsigned(1 downto 0);
begin
  if (rst = '1') then
    controller_int_i        <= '0';
    fe_cmd_cycle_done_i     <= '0';
    trig_fifo_ren_i         <= '0';
    store_current_trigger_i <= '0';
    mb_error_i              <= '0';
    pres_state              <= reset;
    done_counter            := (others => '0');
	 debug_state				 <= "000";
  elsif (clk'event AND clk = '1') then
    if (control_in(0) = '1') then
      case pres_state is
        when reset =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          pres_state              <= idle; 
          done_counter            := (others => '0');
			 debug_state				 <= "000";

        when idle =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          done_counter            := (others => '0');
  -- start a trigger forwarding sequence when there are triggers pending
          if (fe_cmdpulse_count_i > 0) then
            pres_state <= readout_trigger;
          end if; 
			 debug_state				 <= "001";

        when readout_trigger =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '1';
          store_current_trigger_i <= '0';
          pres_state              <= issue_controller_interrupt;
			 debug_state				 <= "010";

  -- Send Trigger to MDSP, ModeBits and Dynamic Mask Encoders.
        when issue_controller_interrupt =>
          controller_int_i        <= '1';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '1';
          pres_state              <= wait_for_efbdm;
			 debug_state				 <= "011";

        when wait_for_efbdm =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          if (efb_ready_in = '1') then
            pres_state <= done;
            if (fmb_sent_i = '0') then 
              mb_error_i <= '1';
            end if;
          end if; 
			 debug_state				 <= "100";

        when done =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '1';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          pres_state              <= done_delay;
			 debug_state				 <= "101";

        when done_delay =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          done_counter            := done_counter + 1;
          if (done_counter = 2) then
            pres_state <= idle;
          end if;
			 debug_state				 <= "110";

        when others =>
          controller_int_i        <= '0';
          fe_cmd_cycle_done_i     <= '0';
          trig_fifo_ren_i         <= '0';
          store_current_trigger_i <= '0';
          done_counter            := (others => '0');
          pres_state              <= reset;
			 debug_state				 <= "111";
      end case;
    else
      controller_int_i        <= '0';
      fe_cmd_cycle_done_i     <= '0';
      trig_fifo_ren_i         <= '0';
      store_current_trigger_i <= '0';
      pres_state              <= reset;
    end if; 
  end if;  
end process command_gen;

-- Count the L1A triggers for the interrupt issuer and formatter banks
trigger_counter : process (
  clk, 
  rst, 
  control_in,
  fe_cmdpulse_in, 
  fe_cmd_cycle_done_i
  )
begin
  if (rst = '1' ) then
    fe_cmdpulse_count_i <= (others => '0');
    fe_trigger_counter  <= (others => '0');
  elsif (clk'event AND clk = '1') then
    if (control_in(0) = '0') then
      fe_cmdpulse_count_i <= (others => '0');
    elsif (control_in(1) = '1') then
      fe_cmdpulse_count_i <= (others => '1');
    elsif (fe_cmdpulse_in = '1' AND fe_cmd_cycle_done_i = '1') then
      fe_cmdpulse_count_i <= fe_cmdpulse_count_i;
    elsif (fe_cmdpulse_in = '1' AND fe_cmd_cycle_done_i = '0') then
      fe_cmdpulse_count_i <= fe_cmdpulse_count_i + 1;
    elsif (fe_cmdpulse_in = '0' AND fe_cmd_cycle_done_i = '1') then
      fe_cmdpulse_count_i <= fe_cmdpulse_count_i - 1;
    end if;

    if (control_in(0) = '0') then
      fe_trigger_counter  <= (others => '0');
    elsif (fe_cmdpulse_in = '1') then
      fe_trigger_counter <= fe_trigger_counter + '1';
    end if;
  end if;
end process trigger_counter;


-- When there are triggers: 
--   Based on the fe_cmd_mask_updated signal, issue the appropriate interrupt
--   to the controller, wait for the dynamic mask to be sent, then issue a L1
--   command pulse to the formatter banks.  Check for more triggers and repeat
--   the process.

formatter_fe_cmd_pulse_gen : process (
  clk, 
  rst, 
  dynamic_mask_sent_in
  )
variable wait_count : unsigned(1 downto 0);
begin
  if (rst = '1' ) then
    fe_cmdpulse_fmtrAB_i <= "00";
    wait_count := (others => '0');
    fe_cmd_pulse_state <= reset;
  elsif (clk'event AND clk = '1') then
    if (control_in(0) = '1') then
      case fe_cmd_pulse_state is
        when reset =>
          fe_cmdpulse_fmtrAB_i <= "00";
          wait_count := (others => '0');
          fe_cmd_pulse_state <= idle;

        when idle  =>
          fe_cmdpulse_fmtrAB_i <= "00";
          wait_count := (others => '0');
          if (dynamic_mask_sent_in = '1') then
            fe_cmd_pulse_state <= ready;
          end if;

        when ready =>
          fe_cmdpulse_fmtrAB_i <= "00";
          if (efb_ready_in = '1') then
            fe_cmd_pulse_state <= send_fe_pulse;
          end if;

        when send_fe_pulse =>
          if (wait_count = 3) then
            fe_cmdpulse_fmtrAB_i <= "11";
            fe_cmd_pulse_state <= done;
          end if;
          wait_count := wait_count + 1 ;

        when done  =>
          fe_cmdpulse_fmtrAB_i <= "00";
          wait_count := (others => '0');
          fe_cmd_pulse_state <= idle;

        when others =>
          fe_cmdpulse_fmtrAB_i <= "00";
          fe_cmd_pulse_state <= reset;
      end case;
    else
      fe_cmdpulse_fmtrAB_i <= "00";
      fe_cmd_pulse_state <= reset;
    end if;
  end if;
end process formatter_fe_cmd_pulse_gen;

fmb_sent : process (
  clk, 
  rst, 
  modebits_sent_in, 
  fe_cmd_cycle_done_i
  )
begin
  if (rst = '1' ) then
    fmb_sent_i <= '0';
  elsif (clk'event AND clk = '1') then
    if (modebits_sent_in = '1') then
      fmb_sent_i <= '1';
    elsif (fe_cmd_cycle_done_i = '1') then
      fmb_sent_i <= '0';
    end if;
  end if;
end process fmb_sent;

-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------
command_cycle_done_out <= fe_cmd_cycle_done_i;
fe_cmdpulse_AB_out     <= fe_cmdpulse_fmtrAB_i;

controller_int_out(0) <= (controller_int_i AND (NOT fifoed_triggers_i(1)));
controller_int_out(1) <= (controller_int_i AND (    fifoed_triggers_i(1)));

fe_cmdpulse_count_out     <= std_logic_vector (fe_cmdpulse_count_i);
store_current_trigger_out <= store_current_trigger_i;
current_trigger_out       <= fifoed_triggers_i;

modebits_error_out        <= mb_error_i;

end rtl; -- end code for fe_command_pulse_counter
