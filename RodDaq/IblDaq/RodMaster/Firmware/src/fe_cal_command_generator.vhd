--------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--------------------------------------------------------------------------
--
-- Filename: fe_medium_command_generator.vhd
-- Description:
--  Implements the front end calibration bit stream. 
--  Some pre-determined cal-bitstream is placed into the 
--  RRIFPGA_cal_command_register. On the cal_strobe, this gets copied into
--  the command shift register for transmission to the front end modules.
--
--  As of the UK - Cambridge workshop 27 January 2000:
--  Since the relaxation of the cal_pulse to cal output bitstream latency 
--  from 3clocks to "a more reasonable number", 
--  an alternative lower gate utilization scheme is implemented here.
--  In addition, the number of clks from the start of the cal-bitstream to
--  the l1_accept bit stream is configurable from 0-255. One must take
--  care not to set the number below the length of the cal stream, as
--  they will collide "programmer be-ware". 
--  
--
--------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    23 February 2000 - MLN first version : requirements/functionality finally
--                       understood and sufficiently documented to proceed 
--                       with coding.
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------
entity fe_cal_command_generator is
  port(
    clk                  : in  std_logic;
    rst                  : in  std_logic; -- asynchronous global reset
    enable_in            : in  std_logic;
--    rod_type_in          : in  std_logic; -- ROD Type 0=>SCT 1 =>Pixel
    calibrate_in         : in  std_logic; -- calibrate command pulse, 1 clk wide
    calibrate_command_in : in  std_logic_vector(26 downto 0); -- calibrate command pulse, 1 clk wide
    cal_trig_delay_in    : in  unsigned(7 downto 0); -- clks from when the calpulse to l1trig pulse sent + 1clk
    command_bits_out     : out std_logic; -- cal command bits out                
    send_l1a_pulse_out   : out std_logic  -- l1a pulse to force the calibration 
    );
end fe_cal_command_generator; 

architecture rtl of fe_cal_command_generator is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  idle,
  shift_cal_bits,
  issue_cal_cmd,
  wait_for_trigger_delay
  );
signal pres_state       : states;

signal sr_output_i      : std_logic;
signal sr_reg_i         : std_logic_vector(26 downto 0);
signal sr_en_i          : std_logic;

signal ibl_cal_cmd_i     : std_logic; 
signal ibl_cal_bit_count : unsigned(3 downto 0);
signal trigger_delay_count : unsigned(8 downto 0); 

--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
begin

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
send_l1a_pulse_out <= '0';
command_bits_out   <= enable_in AND (sr_output_i OR ibl_cal_cmd_i);

-- State machine next state determination logic 
state_logic : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    sr_en_i <= '0';
    ibl_cal_bit_count <= (others => '0');
	 ibl_cal_cmd_i	<= '0';
    pres_state <= idle;
  elsif (clk'event and clk = '1') then
    case pres_state is
 -- start a cal sequence when directed
      when idle =>
        sr_en_i <= '0';
        ibl_cal_bit_count   <= (others => '0');
        trigger_delay_count <= '0' & cal_trig_delay_in;
		  ibl_cal_cmd_i	<= '0';
        if (calibrate_in = '1') then
          pres_state <= issue_cal_cmd;
        end if; 

      when shift_cal_bits =>
        sr_en_i <= '1';
        trigger_delay_count <= trigger_delay_count - '1';
        pres_state  <= wait_for_trigger_delay;

      when issue_cal_cmd =>
        sr_en_i             <= '0';
        ibl_cal_bit_count   <= ibl_cal_bit_count + '1';
        trigger_delay_count <= trigger_delay_count - '1';
          
        case ibl_cal_bit_count is
          when "0001" => ibl_cal_cmd_i <= '1';
          when "0010" => ibl_cal_cmd_i <= '0';
          when "0011" => ibl_cal_cmd_i <= '1';
          when "0100" => ibl_cal_cmd_i <= '1';
          when "0101" => ibl_cal_cmd_i <= '0';
          when "0110" => ibl_cal_cmd_i <= '0';
          when "0111" => ibl_cal_cmd_i <= '1';
          when "1000" => ibl_cal_cmd_i <= '0';
          when "1001" => ibl_cal_cmd_i <= '0';
                         pres_state    <= wait_for_trigger_delay;
          when others => ibl_cal_cmd_i <= '0';
        end case;
        
      when wait_for_trigger_delay =>
        if (trigger_delay_count(8) = '1') then
          pres_state <= idle;
        else
          trigger_delay_count <= trigger_delay_count - '1';
        end if;

      when others =>
        sr_en_i <= '0';
        pres_state <= idle;
    end case;
  end if;
end process state_logic;

-- calibration command shift register
--  bits come out in register MSB order first
shifter : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    sr_reg_i    <= (others => '0');
    sr_output_i <= '0'; 
  elsif (clk'event and clk = '1') then
    if (sr_en_i = '0') then
      sr_reg_i(26 downto 0) <= calibrate_command_in(26 downto 0);
    else
      sr_output_i <= sr_reg_i(26); 
      sr_reg_i(26 downto 0) <= sr_reg_i(25 downto 0) & '0'; 
    end if; 
  end if;  
end process shifter;

end rtl; -- end code for fe_medium_command_generator
