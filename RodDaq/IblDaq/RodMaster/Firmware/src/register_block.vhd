-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: register_block.vhd
-- Description:
--   Register Block used inthe RCF FPGA.  See Users Manual Appendix A for
--   detaled definitions of each register
--------------------------------------------------------------------------
-- Structure:
--
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: John M. Joseph
-- Board Engineer: John M. Joseph
-- History:
--          12 Feb 2002 : JMJ Map Update to clean up code
--------------------------------------------------------------------------
--
--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations

use work.rodMasterRegPack.all;

--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------

entity register_block is
  port (
    clk_in                    : in  std_logic; -- clk40 input
    rst_n_in                  : in  std_logic; -- global reset

	 -- AKU
	 dspLeds							: out std_logic_vector(7 downto 0);
	 -- 
    regbus_en_n_in            : in  std_logic;
    regbus_rnw_in             : in  std_logic;
    regbus_a_in               : in  std_logic_vector(11 downto 0);
    regbus_d_in               : in  std_logic_vector(31 downto 0);
    regbus_d_out              : out std_logic_vector(31 downto 0);
    regbus_ack_out            : out std_logic;   

    fcmbus_en_n_in            : in  std_logic;
    fcmbus_d_out              : out std_logic_vector(31 downto 0);

    rrif_ctrl_reg_out         : out std_logic_vector(31 downto 0);
    spare_ctrl_reg_out        : out std_logic_vector(31 downto 0);
    rrif_stts_reg_in          : in  std_logic_vector(31 downto 0);
    spare_stts_reg_in         : in  std_logic_vector(31 downto 0);
    dsp_stts_reg_in           : in  std_logic_vector(31 downto 0);
    test_stts_reg_in          : in  std_logic_vector(31 downto 0);

    fecmd_link_mask0_out      : out std_logic_vector(47 downto 0);
    fecmd_link_mask1_out      : out std_logic_vector(47 downto 0);

    calstrobe_delay_reg_out   : out std_logic_vector( 7 downto 0);
    cal_command_reg_out       : out std_logic_vector(26 downto 0);

    modebits_fifo_stts_reg_in : in  std_logic_vector(23 downto 0);
    dynmmask_fifo_stts_reg_in : in  std_logic_vector(41 downto 0);
    inmem_wordcount_reg_out   : out std_logic_vector(31 downto 0);
    dbgmem_wordcount_reg_out  : out std_logic_vector(31 downto 0);
    cnfg_rdback_count_reg_out : out std_logic_vector(31 downto 0);
    ide_debugmem_stts_reg_in  : in  std_logic_vector( 7 downto 0);
    ide_debugmem_cnfg_reg_out : out std_logic_vector(17 downto 0);
    debug_fifo_stts_in        : in  std_logic_vector( 7 downto 0);
    rol_test_block_reg_out    : out std_logic_vector( 3 downto 0);  
    rol_test_block_count_out  : out std_logic_vector( 5 downto 0);  
	 rol_test_out					: out std_logic;

    int_tim_fifo_outdata_out  : out std_logic_vector( 7 downto 0);
    int_tim_fifo_re_in        : in  std_logic;
    int_tim_fifo_rt_in        : in  std_logic;

    slave_dsp_ints_out        : out std_logic_vector( 3 downto 0);
    slave_dsp_ints_in         : in  std_logic_vector( 7 downto 0);
    sdsp_inttohost_signal_out : out std_logic;
     -- or'd interrupt of the sdsp's to one of the RCDSP's ints

    feocc_reset_reg_out       : out std_logic_vector( 31 downto 0);
    feocc_num_l1_reg_out      : out std_logic_vector(103 downto 0);

    cal_event_type0_out       : out std_logic_vector( 9 downto 0);
    cal_event_type1_out       : out std_logic_vector( 9 downto 0);
    cal_l1id0_in              : in  std_logic_vector(23 downto 0);
    cal_l1id1_in              : in  std_logic_vector(23 downto 0);
	 calpulse_count0_in			: in  std_logic_vector(23 downto 0);
	 calpulse_count1_in			: in  std_logic_vector(23 downto 0);
    static_l1id0_out          : out std_logic_vector(23 downto 0);
    static_l1id1_out          : out std_logic_vector(23 downto 0);
    cal_bcid0_in              : in  std_logic_vector(11 downto 0);
    cal_bcid1_in              : in  std_logic_vector(11 downto 0);
    static_bcid0_out          : out std_logic_vector(11 downto 0);
    static_bcid1_out          : out std_logic_vector(11 downto 0);
    rod_mode_out              : out std_logic_vector( 3 downto 0);
    test_mux_out              : out std_logic_vector( 3 downto 0);
    fe_cmd_mask_sel_out       : out std_logic_vector( 2 downto 0);
    fe_cmd_mask_updated_in    : in  std_logic;
    corrective_mask_sent_in   : in  std_logic;
    send_int_test_out         : out std_logic;
    test_irq_out              : out std_logic_vector( 2 downto 0);
    ecr_count_in              : in  std_logic_vector( 7 downto 0);
    ecr_count_out             : out std_logic_vector( 7 downto 0);
    nevent_out                : out std_logic_vector(15 downto 0);  -- NEvent
    precal_delay_out          : out std_logic_vector( 7 downto 0);  -- Pre CAL Delay
    trig_delay0_out           : out std_logic_vector(15 downto 0);  -- TrigDelay1
    trig_delay1_out           : out std_logic_vector(15 downto 0);  -- TrigDelay2
    interval_out              : out std_logic_vector(31 downto 0);  -- Interval
    trig_mode_out             : out std_logic_vector(31 downto 0);  -- TrigMode
    mask_en_in                : in  std_logic_vector( 2 downto 0);  --
    new_mask_in               : in  std_logic;
    load_event_type_out       : out std_logic;
    int_nevent_done_in        : in  std_logic;
	 prm_ga							: in std_logic_vector(4 downto 0);
    l1id_bits_in              : in  std_logic_vector(23 downto 0)
    );   
end register_block;

architecture rtl of register_block is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  reset,
  wait_0_clk,
  wait_1_clk,
  acknowledge
  );
signal ackn_cycle_state : states;               

type mask_fetch_states is (
  idle,
  fetch_mask_data,
  done
  );
signal mask_fetch : mask_fetch_states;               

signal rst_i                   : std_logic;
signal rrif_ctrl_reg_i         : std_logic_vector(31 downto 0);
signal spare_ctrl_reg_i        : std_logic_vector(31 downto 0);
signal rod_mode_i              : std_logic_vector( 3 downto 0);
signal test_mux_i              : std_logic_vector( 3 downto 0);
signal fecmd_link_mask0_i      : std_logic_vector(47 downto 0);
signal fecmd_link_mask1_i      : std_logic_vector(47 downto 0);
signal calstrobe_delay_reg_i   : std_logic_vector( 7 downto 0);
signal cal_command_reg_i       : std_logic_vector(26 downto 0);
signal inmem_wordcount_reg_i   : std_logic_vector(31 downto 0);
signal dbgmem_wordcount_reg_i  : std_logic_vector(31 downto 0);
signal cnfg_rdback_count_reg_i : std_logic_vector(31 downto 0);
signal ide_debugmem_cnfg_reg_i : std_logic_vector(17 downto 0);
signal rol_test_block_reg_i    : std_logic_vector( 3 downto 0);
signal rol_test_block_count_i  : std_logic_vector( 5 downto 0); 
signal rol_test_i					 : std_logic; 
signal sdsp_inttohost_signal_i : std_logic;
signal feocc_reset_i           : std_logic_vector( 31 downto 0);
signal feocc_num_l1_i          : std_logic_vector(103 downto 0);
signal ints_to_sdsps_i         : std_logic_vector(  3 downto 0);
signal cal_event_type0_i       : std_logic_vector(  9 downto 0);
signal cal_event_type1_i       : std_logic_vector(  9 downto 0);
signal static_bcid0_i          : std_logic_vector( 11 downto 0);
signal static_bcid1_i          : std_logic_vector( 11 downto 0);
signal static_l1id0_i          : std_logic_vector( 23 downto 0);
signal static_l1id1_i          : std_logic_vector( 23 downto 0);
signal fe_cmd_mask_count_i     : unsigned( 2 downto 0);
signal fe_cmd_mask_addr_i      : std_logic_vector( 7 downto 0);
signal fe_cmd_mask_data_i      : std_logic_vector(31 downto 0);
signal command_mask_select_i   : std_logic_vector( 2 downto 0);
signal command_mask_ready_i    : std_logic;
signal fetch_fe_mask_i         : std_logic;     
 
signal lut_wr_addr_i           : std_logic_vector(7 downto 0);
signal lut_wr_i                : std_logic;

signal int_tim_fifo_we_i      : std_logic; --
signal int_tim_fifo_wen_i     : std_logic; --
signal int_tim_fifo_re_i      : std_logic; --
signal int_tim_fifo_ren_i     : std_logic; --
signal int_tim_fifo_rt_i      : std_logic; --
signal int_tim_fifo_en_i      : std_logic; --
signal int_tim_fifo_ef_i      : std_logic; --
signal int_tim_fifo_ff_i      : std_logic; --
signal int_tim_fifo_rst_i     : std_logic; --
signal int_tim_fifo_indata_i  : std_logic_vector( 7 downto 0); --
signal int_tim_fifo_outdata_i : std_logic_vector( 7 downto 0); --
signal int_tim_occ_count_i    : std_logic_vector(11 downto 0); --

signal wr_strb_i        : std_logic; --
signal rd_strb_i        : std_logic; --
signal regbus_en_n_in_i : std_logic; -- signal used to detect edges 
                                     -- in regbus_en_n_in
signal send_int_test_i : std_logic;
signal test_irq_i      : std_logic_vector( 2 downto 0);
signal ecr_count_i     : std_logic_vector( 7 downto 0);

signal nevent_i      : std_logic_vector(15 downto 0);  -- NEvent
signal precal_td_i   : std_logic_vector( 7 downto 0);  -- Pre CAL Delay
signal trig_delay0_i : std_logic_vector(15 downto 0);  -- TrigDelay1
signal trig_delay1_i : std_logic_vector(15 downto 0);  -- TrigDelay2
signal interval_i    : std_logic_vector(31 downto 0);  -- Interval
signal trig_mode_i   : std_logic_vector(31 downto 0);  -- TrigMode
signal mdsp_gpreg_i  : std_logic_vector(31 downto 0);

type rb_access_states is (
  idle,
  write,
  read,
  done
  );
signal rb_access : rb_access_states;

signal fake_bus_dpram  : std_logic_vector(31 downto 0);
signal read_zero		  : std_logic_vector(0 downto 0);

signal dspLeds_i: std_logic_vector(7 downto 0);
--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

COMPONENT dpram_256x32
  PORT (
    clka : IN STD_LOGIC;
    rsta : IN STD_LOGIC;
    wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb : IN STD_LOGIC;
    web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

attribute box_type: string;
attribute box_type of dpram_256x32: component is "user_black_box";

begin
--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
                               
int_tim_fifo_re_i  <= int_tim_fifo_re_in OR 
                     (int_tim_fifo_ren_i AND int_tim_fifo_en_i);
int_tim_fifo_we_i  <= int_tim_fifo_wen_i AND int_tim_fifo_en_i;     
int_tim_fifo_rt_i  <= int_tim_fifo_rt_in OR spare_ctrl_reg_i(8);
int_tim_fifo_rst_i <= spare_ctrl_reg_i(6);

-- actually it is a 256x32
dpram_256x32_instance: dpram_256x32
  PORT MAP (
    clka => clk_in,
    rsta => rst_i,
    wea(0) => lut_wr_i,
    addra => lut_wr_addr_i,
    dina => regbus_d_in,
    douta => fcmbus_d_out,
    clkb => clk_in,
    web => read_zero,
    addrb => fe_cmd_mask_addr_i,
    dinb => fake_bus_dpram,
    doutb => fe_cmd_mask_data_i
  );
  
  fake_bus_dpram	<= (others => '0');
  read_zero(0)			<= '0';

  rst_i         <= NOT rst_n_in;
  lut_wr_addr_i <= regbus_a_in(7 downto 0);
  lut_wr_i      <= NOT fcmbus_en_n_in AND NOT regbus_rnw_in;

-------------------------------------------------------------------------------
-- Output Signal Connections
-------------------------------------------------------------------------------

  spare_ctrl_reg_out <= spare_ctrl_reg_i;
--  spare_ctrl_reg_out( 7 downto  0) <= spare_ctrl_reg_i( 7 downto 0);
--  spare_ctrl_reg_out(12 downto  8) <= spare_ctrl_reg_i(12 downto 8);
--  spare_ctrl_reg_out(31 downto 10) <= spare_ctrl_reg_i(31 downto 10);

  rrif_ctrl_reg_out <= rrif_ctrl_reg_i;
  rod_mode_out      <= rod_mode_i;
  test_mux_out      <= test_mux_i;

  fecmd_link_mask0_out <= fecmd_link_mask0_i;
  fecmd_link_mask1_out <= fecmd_link_mask1_i;
  fe_cmd_mask_sel_out  <= command_mask_select_i;
  feocc_reset_reg_out  <= feocc_reset_i;
  feocc_num_l1_reg_out <= feocc_num_l1_i;

  calstrobe_delay_reg_out <= calstrobe_delay_reg_i;      
  cal_command_reg_out     <= cal_command_reg_i;

  cal_event_type0_out <= cal_event_type0_i;
  cal_event_type1_out <= cal_event_type1_i;
  static_l1id0_out    <= static_l1id0_i;
  static_l1id1_out    <= static_l1id1_i;
  static_bcid0_out    <= static_bcid0_i;
  static_bcid1_out    <= static_bcid1_i;
  nevent_out          <= nevent_i;
  precal_delay_out    <= precal_td_i;
  trig_delay0_out     <= trig_delay0_i;
  trig_delay1_out     <= trig_delay1_i;
  interval_out        <= interval_i;
  trig_mode_out       <= trig_mode_i;

  inmem_wordcount_reg_out   <= inmem_wordcount_reg_i;
  dbgmem_wordcount_reg_out  <= dbgmem_wordcount_reg_i;
  cnfg_rdback_count_reg_out <= cnfg_rdback_count_reg_i;
  ide_debugmem_cnfg_reg_out <= ide_debugmem_cnfg_reg_i;
  rol_test_block_reg_out    <= rol_test_block_reg_i;
  rol_test_block_count_out  <= rol_test_block_count_i;
  rol_test_out					 <= rol_test_i;

  slave_dsp_ints_out <= ints_to_sdsps_i;
  ecr_count_out      <= ecr_count_i;
    
  sdsp_inttohost_signal_out <= (    slave_dsp_ints_in(0) AND
                                    slave_dsp_ints_in(1) AND
                                    slave_dsp_ints_in(2) AND
                                    slave_dsp_ints_in(3) AND
                                NOT slave_dsp_ints_in(4) AND
                                NOT slave_dsp_ints_in(5) AND
                                NOT slave_dsp_ints_in(6) AND
                                NOT slave_dsp_ints_in(7)     );
  test_irq_out <= test_irq_i;

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
--  The output of the TIM FIFO is used to drive the FE Command blocks, and the
--  ROD EVID and MB block in stand alone debug mode.  The output must be masked
--  to these blocks when the system is in data-taking mode.
tim_fifo_output_mux : process (
  rst_n_in, 
  clk_in
  )
begin                      
  if (rst_n_in = '0') then
    int_tim_fifo_outdata_out <= (others => '0');
  elsif (clk_in'event and clk_in = '1') then
    if (int_tim_fifo_re_in = '1') then
      int_tim_fifo_outdata_out <= int_tim_fifo_outdata_i;
    else
      int_tim_fifo_outdata_out <= (others => '0');
    end if;
  end if;
end process tim_fifo_output_mux;

-------------------------------------------------------------------------------

write_registers : process (
  clk_in, 
  rst_n_in, 
  regbus_a_in, 
  regbus_d_in,
  regbus_en_n_in, 
  regbus_rnw_in
  )
begin
  if (rst_n_in = '0') then
 	 dspLeds_i	<= (others => '0');

    rrif_ctrl_reg_i         <= (others => '0');
    -- Startup with all functions in reset
    spare_ctrl_reg_i        <= X"000000FF"; 
    -- Startup with all functions in reset
    rod_mode_i              <= (others => '0');
    test_mux_i              <= "0000";
    fecmd_link_mask0_i      <= (others => '0'); -- All Cmd Links Masked
    fecmd_link_mask1_i      <= (others => '0'); -- All Cmd Links Masked
--  calstrobe_delay_reg_i   <= "10011100"; -- Default to 156 clocks (27 + 132 - 3) = 156
    calstrobe_delay_reg_i   <= X"40"; -- Default to 64 clocks
    cal_command_reg_i       <= "101011100001100111111110000"; -- issue Cal Stobe to all Chips
	 rol_test_block_count_i	 <= "000010";-----------------------------------------------------------------------------------------
	 rol_test_i					 <= '0';
    inmem_wordcount_reg_i   <= (others => '0');
    dbgmem_wordcount_reg_i  <= (others => '0');
    cnfg_rdback_count_reg_i <= (others => '0');
    ide_debugmem_cnfg_reg_i <= (others => '0');
    rol_test_block_reg_i    <= (others => '0');
    feocc_reset_i           <= (others => '1');
    feocc_num_l1_i          <= (others => '0');
    cal_event_type0_i       <= (others => '0');
    cal_event_type1_i       <= (others => '0');
    int_tim_fifo_indata_i   <= (others => '0');
    int_tim_fifo_ren_i      <= '0'; 
    int_tim_fifo_wen_i      <= '0';
    command_mask_ready_i    <= '0';
    command_mask_select_i   <= (others => '0');
    send_int_test_i         <= '0';
    send_int_test_out       <= '0';
--  test_irq_out            <= "100";
    test_irq_i              <= "100";
    load_event_type_out     <= '0';
    mdsp_gpreg_i            <= (others => '0');
    nevent_i                <= (others => '0');
    precal_td_i             <= X"05";
    trig_delay0_i           <= X"00F0";
    trig_delay1_i           <= X"00F0";
    interval_i              <= (others => '0');
    trig_mode_i             <= (others => '0');
  elsif (clk_in'event AND clk_in = '1') then
    case rb_access is
      when idle => 
    -- Enable Formatter MB and EFB Dynamic Mask FIFOs after reset
        spare_ctrl_reg_i(2 downto 0) <= (others => '0');
    -- Enable the Corrective Trigger FIFO after reset
        spare_ctrl_reg_i(7) <= '0';
    -- Set the RRIF_CMND_1 Register to support the RoL Test selected and mask all FE outputs
--        if (rol_test_block_reg_i(0) = '1') then
--          fecmd_link_mask0_i <= (others => '0'); -- All Cmd Links Masked
--          fecmd_link_mask1_i <= (others => '0'); -- All Cmd Links Masked
--          if (rol_test_block_reg_i(2) = '1') then -- TIM Control
--            rrif_ctrl_reg_i <= X"00001523"; 
--          else  -- RCF Control (SP Trigger Controller)      
--            rrif_ctrl_reg_i <= X"00141421"; 
--          end if;
--        end if;

        if (regbus_en_n_in = '0' AND regbus_en_n_in_i = '1') then 
          if (regbus_rnw_in = '1') then
            rb_access <= read;
          else
            rb_access <= write;
          end if;
        end if;

      when read => 
        if (regbus_a_in(7 downto 0) = IntTimFifoDataReg) then
          int_tim_fifo_ren_i <= '1';
        end if;
        rb_access <= done;
        
      when write => 
        case regbus_a_in(7 downto 0) is
          when MdspGpreg => mdsp_gpreg_i <= regbus_d_in;
  --      when DspSttsReg => dsp_stts_reg_in       (read only)
  --      when TestSttsReg => test_stts_reg_in      (read only)
  --      when RodRevCodeVerReg => NOT USED              (read only)
          when RrifCtrlReg => rrif_ctrl_reg_i(27 downto  0) <= regbus_d_in(27 downto  0);
                        rrif_ctrl_reg_i(28)           <= regbus_d_in(28) AND spare_stts_reg_in(31);  -- Allow in Pixel ROD only
                        rrif_ctrl_reg_i(31 downto 29) <= regbus_d_in(31 downto 29);
          when SprCtrlReg => spare_ctrl_reg_i      <= regbus_d_in;
          when RodModeReg => rod_mode_i            <= regbus_d_in( 3 downto  0);
                        test_mux_i            <= regbus_d_in(19 downto 16);
          when FeCmdMaskSelReg => command_mask_select_i <= regbus_d_in( 2 downto  0);
                        command_mask_ready_i  <= '0';
                        fetch_fe_mask_i       <= '1';
  --      when RrifSttsReg => rrif_stts_reg_in       (read only)
  --      when SpareSttsReg => spare_stts_reg_in      (read only)
          when FeCmdMask0LsbReg => fecmd_link_mask0_i(31 downto  0) <= regbus_d_in;
                        command_mask_ready_i             <= '0';
          when FeCmdMask0MsbReg => fecmd_link_mask0_i(47 downto 32) <= regbus_d_in(15 downto 0);
                        command_mask_ready_i             <= '0';
          when FeCmdMask1LsbReg => fecmd_link_mask1_i(31 downto  0) <= regbus_d_in;
                        command_mask_ready_i             <= '0';
          when FeCmdMask1MsbReg => fecmd_link_mask1_i(47 downto 32) <= regbus_d_in(15 downto 0);
                        command_mask_ready_i             <= '0';
          when CalStrbDlyReg => calstrobe_delay_reg_i            <= regbus_d_in( 7 downto 0);
          when CalCommandReg => cal_command_reg_i                <= regbus_d_in(26 downto 0);
          when EcrCountIReg => ecr_count_i                      <= regbus_d_in( 7 downto 0); 
  --      when MbFifoSttsReg => modebits_fifo_stts_reg_in   (read only)
  --      when DynMaskSttsLsbReg => dynmmask_fifo_stts_reg_in   (read only)
  --      when DynMaskSttsMsbReg => dynmmask_wc_stts_reg_in     (read only)
          when InMemWordCntReg => inmem_wordcount_reg_i   <= regbus_d_in;
          when DbgMemWordCntReg => dbgmem_wordcount_reg_i  <= regbus_d_in;
          when CnfgRdbackCntReg => cnfg_rdback_count_reg_i <= regbus_d_in;
          when DebugModeCnfgReg => ide_debugmem_cnfg_reg_i <= regbus_d_in(17 downto  0);
                        rol_test_block_reg_i    <= regbus_d_in(23 downto 20);
                        rol_test_block_count_i  <= regbus_d_in(29 downto 24);
								rol_test_i					<= regbus_d_in(30);
  --      when DebugModeSttsgReg => ide_debugmem_stts_reg_in    (read only)
          when IntTimFifoDataReg => int_tim_fifo_indata_i    <= regbus_d_in( 7 downto  0);
                        int_tim_fifo_wen_i       <= '1'; 
          when IntToSlvDspsReg => ints_to_sdsps_i <= regbus_d_in( 3 downto 0);
  --      when SlvDspIntReg => slave_dsp_ints_in     (read only)
          when TestIrqReg => send_int_test_i <= regbus_d_in(0);
                        test_irq_i      <= regbus_d_in(6 downto 4);
          when FeOccResReg => feocc_reset_i(31 downto  0) <= regbus_d_in;
          when FeOccNumL1Reg0 => feocc_num_l1_i( 31 downto  0) <= regbus_d_in;
          when FeOccNumL1Reg1 => feocc_num_l1_i( 63 downto 32) <= regbus_d_in;
          when FeOccNumL1Reg2 => feocc_num_l1_i( 95 downto 64) <= regbus_d_in;
          when FeOccNumL1Reg3 => feocc_num_l1_i(103 downto 96) <= regbus_d_in(7 downto  0);
          when IntScanReg0 => nevent_i      <= regbus_d_in(15 downto  0);  -- NEvent
                        precal_td_i   <= regbus_d_in(23 downto 16);  -- PreCAL Delay
          when IntScanReg1 => trig_delay0_i <= regbus_d_in(15 downto  0);  -- TrigDelay1
                        trig_delay1_i <= regbus_d_in(31 downto 16);  -- TrigDelay2
          when IntScanReg2 => interval_i        <= regbus_d_in;  -- Interval
          when IntScanReg3 => trig_mode_i       <= regbus_d_in;  -- TrigMode
          when CalEvntType0Reg => cal_event_type0_i <= regbus_d_in( 9 downto  0);
          when CalEvntType1Reg => cal_event_type1_i <= regbus_d_in( 9 downto  0);
--        when ScanDoneSttsReg => Scan Done     (Read Only) 
          when CalL1id0Reg => static_l1id0_i <= regbus_d_in(23 downto  0);
          when CalL1id1Reg => static_l1id1_i <= regbus_d_in(23 downto  0);
          when CalBcid0Reg => static_bcid1_i <= regbus_d_in(27 downto 16);
                        static_bcid0_i <= regbus_d_in(11 downto  0);
          when DspLedsReg => dspLeds_i <= regbus_d_in( 7 downto  0);
          when others =>  -- NOP
        end case;
        rb_access <= done;

      when done => 
        if (ackn_cycle_state = reset) then
          rb_access <= idle;
          int_tim_fifo_ren_i <= '0'; 
          int_tim_fifo_wen_i <= '0'; 
          spare_ctrl_reg_i( 8) <= '0';  -- Clear TIM FIFO Re-Tx bits
          spare_ctrl_reg_i( 9) <= '0';  -- Clear InMem FIFO Re-Tx bits
          spare_ctrl_reg_i(12) <= '0';  -- Clear ECR mID Reset
        end if;
      when others => 
    end case;

    if (fe_cmd_mask_updated_in = '1') then
      rrif_ctrl_reg_i(2)   <= '0';
      command_mask_ready_i <= '1';
    end if;
    if (corrective_mask_sent_in = '1') then
      rrif_ctrl_reg_i(23) <= '0';
    end if;

    if (new_mask_in = '1') then
      command_mask_select_i <= mask_en_in;
      fetch_fe_mask_i <= '1';
    end if;

    case mask_fetch is
      when idle =>
        fe_cmd_mask_addr_i  <= (others => '0');
        fe_cmd_mask_count_i <= (others => '0');
        load_event_type_out <= '0';
        if (fetch_fe_mask_i = '1') then
          mask_fetch          <= fetch_mask_data;
          fe_cmd_mask_addr_i  <= "100" & command_mask_select_i & "00"; 
          load_event_type_out <= rrif_ctrl_reg_i(29); -- Auto load of ROD Event type if int scan mode enabled
        end if;
      when fetch_mask_data =>
        case fe_cmd_mask_count_i(2 downto 0) is
          when "001"  => fecmd_link_mask0_i(31 downto  0) <= fe_cmd_mask_data_i;
          when "010"  => fecmd_link_mask0_i(47 downto 32) <= fe_cmd_mask_data_i(15 downto  0);
          when "011"  => fecmd_link_mask1_i(31 downto  0) <= fe_cmd_mask_data_i;
          when "100"  => fecmd_link_mask1_i(47 downto 32) <= fe_cmd_mask_data_i(15 downto  0);
                         mask_fetch          <= done;
                         fetch_fe_mask_i     <= '0';
                         rrif_ctrl_reg_i(2)  <= '1';
          when others => null;
        end case;
        fe_cmd_mask_addr_i  <= fe_cmd_mask_addr_i + 1;
        fe_cmd_mask_count_i <= fe_cmd_mask_count_i + 1;
      when done =>
--      load_event_type_out <= rrif_ctrl_reg_i(29); -- Auto load of ROD Event type if int scan mode enabled
        mask_fetch <= idle;
    end case;
     
    if (send_int_test_i = '1') then
      send_int_test_out <= '1';
      send_int_test_i   <= '0';
    else
      send_int_test_out <= '0';
    end if;

  end if;
end process write_registers;

	dspLeds <= dspLeds_i;


-- depending on how fast register routing paths are in the fpga
-- more or less waiting may be required.
-- this provides for an open loop acknowledge system, which is fine
-- for internal registers, since their internal routing is well defined.
-- Must make this ce edge sesetive, otherwise will initial multiple times.

reg_transfer_acknowledge_generator : process (
  rst_n_in, 
  clk_in, 
  regbus_en_n_in
  )
begin                      
  if (rst_n_in = '0') then
    regbus_ack_out    <= '0';
    regbus_en_n_in_i  <= '1';
    int_tim_fifo_en_i <= '0';
    ackn_cycle_state  <= reset;

  elsif (clk_in'event and clk_in = '1') then
    regbus_en_n_in_i <= regbus_en_n_in; -- en_n_in negative edge delay detection register
  
    case ackn_cycle_state is
      when reset =>
        regbus_ack_out    <= '0';
        int_tim_fifo_en_i <= '0';
      --    
        if (regbus_en_n_in = '0' AND regbus_en_n_in_i = '1') then 
          ackn_cycle_state <= wait_0_clk;
        end if;
        
      when wait_0_clk =>   -- Latch Read Data from TIM Fifo
        regbus_ack_out    <= '0';
        ackn_cycle_state  <= wait_1_clk;

      when wait_1_clk =>   -- Write Data to TIM Fifo
        regbus_ack_out    <= '0';
        int_tim_fifo_en_i <= int_tim_fifo_wen_i OR int_tim_fifo_ren_i;
        ackn_cycle_state  <= acknowledge;
  
      when acknowledge =>
        regbus_ack_out    <= '1';
        int_tim_fifo_en_i <= '0';
      --
        if (regbus_en_n_in = '1') then
          ackn_cycle_state <= reset;
        end if;
    end case;
  end if;  
end process reg_transfer_acknowledge_generator;     

regbus_readout : process (
  rst_n_in, 
  clk_in
  )
constant code_version : std_logic_vector(7 downto 0) := X"01"; -- v3.7;
constant rod_revision : std_logic_vector(7 downto 0) := X"10";  -- revF;
begin      
  if (rst_n_in = '0') then
  elsif (clk_in'event and clk_in = '1') then
  -- Always clear the regbus_out bus so as left over garbage won't be 
  -- asserted onto the bus turnaround time required to transition into
  -- the "read/decoder" stage of this block.
    regbus_d_out <= (others => '0');
    case regbus_a_in(7 downto 0) is
      when MdspGpreg => regbus_d_out <= mdsp_gpreg_i;
      when DspSttsReg => regbus_d_out <= dsp_stts_reg_in;
      when TestSttsReg => regbus_d_out <= test_stts_reg_in;

  -- Code Version and ROD Revision #
      when RodRevCodeVerReg => regbus_d_out(15 downto 0) <= rod_revision & code_version;

  -- Main & Spare Control Registers
      when RrifCtrlReg => regbus_d_out <= rrif_ctrl_reg_i;
      when SprCtrlReg => regbus_d_out <= spare_ctrl_reg_i;

  -- ROD Mode Register
      when RodModeReg => regbus_d_out( 3 downto  0) <= rod_mode_i;
                    regbus_d_out(19 downto 16) <= test_mux_i;

  -- FE Command Mask Select Register
      when FeCmdMaskSelReg => regbus_d_out(2 downto 0) <= command_mask_select_i;

  -- Main & Spare Status Registers
      when RrifSttsReg => regbus_d_out <= command_mask_ready_i &
                                    rrif_stts_reg_in(30 downto 0);
      when SpareSttsReg => regbus_d_out <= spare_stts_reg_in;
      when EmptyReg0 => regbus_d_out(31 downto 12) <= (others => '0'); -- AKU: void
     --  HRDY Time Out Count  AKU: void
      when EmptyReg1 => regbus_d_out   <= (others => '0');

  -- FE Command Link Mask Registers
      when FeCmdMask0LsbReg => regbus_d_out              <= fecmd_link_mask0_i(31 downto  0);
      when FeCmdMask0MsbReg => regbus_d_out(15 downto 0) <= fecmd_link_mask0_i(47 downto 32);
      when FeCmdMask1LsbReg => regbus_d_out              <= fecmd_link_mask1_i(31 downto  0);
      when FeCmdMask1MsbReg => regbus_d_out(15 downto 0) <= fecmd_link_mask1_i(47 downto 32);
        
  -- Calstrobe Registers       
      when CalStrbDlyReg => regbus_d_out(7 downto 0) <= calstrobe_delay_reg_i;
     -- time to from the start of the cal command to the start of the cal l1 trigger pulse + 1clock
      
  -- Calibrate Command Bit Stream
      when CalCommandReg => regbus_d_out(26 downto 0) <= cal_command_reg_i;

  -- ECR Count Readback Register
      when EcrCountIReg => regbus_d_out(7 downto 0) <= ecr_count_i;
      when EcrCountInReg => regbus_d_out(7 downto 0) <= ecr_count_in;

  -- Formatter Mode Bits, and EFB Dynamic Mask Registers
      when MbFifoSttsReg => 
        regbus_d_out(27 downto 16) <= modebits_fifo_stts_reg_in(23 downto 14) &
                                      modebits_fifo_stts_reg_in(3) &
                                      modebits_fifo_stts_reg_in(1);
          -- bits [25..18]: internalB_fifo_occupancy,        
          -- bits      17: internalB_fifo_ff,        
          --           16: internalB_fifo_ef        
        regbus_d_out(11 downto 0) <= modebits_fifo_stts_reg_in(13 downto 4) &
                                     modebits_fifo_stts_reg_in(2) &
                                     modebits_fifo_stts_reg_in(0);
          -- bits [9..2]: internalA_fifo_occupancy, 
          -- bits      1: internalA_fifo_ff,        
          --           0: internalA_fifo_ef,       
                                            
      when DynMaskSttsLsbReg =>
        regbus_d_out( 4 downto 0) <= dynmmask_fifo_stts_reg_in(4 downto 0);
        regbus_d_out(12 downto 8) <= dynmmask_fifo_stts_reg_in(9 downto 5);
      when DynMaskSttsMsbReg =>
        regbus_d_out( 7 downto  0) <= dynmmask_fifo_stts_reg_in(17 downto 10);
        regbus_d_out(15 downto  8) <= dynmmask_fifo_stts_reg_in(25 downto 18);
        regbus_d_out(23 downto 16) <= dynmmask_fifo_stts_reg_in(33 downto 26);
        regbus_d_out(31 downto 24) <= dynmmask_fifo_stts_reg_in(41 downto 34);

  -- TestBench Debug Word_Counts_Register
      when InMemWordCntReg => regbus_d_out <= inmem_wordcount_reg_i;
      when DbgMemWordCntReg => regbus_d_out <= dbgmem_wordcount_reg_i;
      when CnfgRdbackCntReg => regbus_d_out <= cnfg_rdback_count_reg_i;

  -- DebugMode_Configuration_Register[31..0] :
      when DebugModeCnfgReg => 
        regbus_d_out(17 downto  0) <= ide_debugmem_cnfg_reg_i;
        regbus_d_out(19 downto 18) <= "00";
        regbus_d_out(23 downto 20) <= rol_test_block_reg_i;
        regbus_d_out(29 downto 24) <= rol_test_block_count_i;
		  regbus_d_out(30)			  <= rol_test_i;

  -- DebugMode_Status_Register[31..0]
      when DebugModeSttsgReg =>
        regbus_d_out(31 downto 30) <= (others => '0');
        regbus_d_out(29 downto 18) <= int_tim_occ_count_i;
        regbus_d_out(17)           <= int_tim_fifo_ff_i;
        regbus_d_out(16)           <= int_tim_fifo_ef_i;
        regbus_d_out(15 downto  8) <= debug_fifo_stts_in;
        regbus_d_out( 7 downto  0) <= ide_debugmem_stts_reg_in;   -- 

  -- Internal Tim Fifo
      when IntTimFifoDataReg => regbus_d_out(7 downto 0) <= int_tim_fifo_outdata_i;
--                  int_tim_fifo_ren_i       <= '1'; 

  -- The master dsp interrupts the slave dsp's hpi-interrupt edge mode should be selected 
  -- in the slave dsp interrupt configuration register.
      when IntToSlvDspsReg => regbus_d_out( 3 downto 0) <= ints_to_sdsps_i;

  -- The master dsp gets an OR'd interrupt from all of the slave dsp on one of it main
  -- interrup lines.  It can then utilizes this register to determine which slave issued
  -- the interrupt.  SET THIS TO LATCH!!!!!!!!!!
      when SlvDspIntReg => regbus_d_out(7 downto 0) <= slave_dsp_ints_in;
      when TestIrqReg => regbus_d_out(6 downto 4) <= test_irq_i;
                    regbus_d_out(0)          <= send_int_test_i;

  -- Front end occupancy counters reset command pulse 
      when FeOccResReg => regbus_d_out <= feocc_reset_i(31 downto  0);

  -- Adder value for Pixel FE Occ Counters
      when FeOccNumL1Reg0 => regbus_d_out <= feocc_num_l1_i( 31 downto  0);
      when FeOccNumL1Reg1 => regbus_d_out <= feocc_num_l1_i( 63 downto 32);
      when FeOccNumL1Reg2 => regbus_d_out <= feocc_num_l1_i( 95 downto 64);
      when FeOccNumL1Reg3 => regbus_d_out(31 downto 8) <= (others => '0');
		                regbus_d_out(7 downto  0) <= feocc_num_l1_i(103 downto 96);
  --RCF Internal Scan Register
      when IntScanReg0 => regbus_d_out(15 downto  0) <= nevent_i;      -- NEvent
                    regbus_d_out(23 downto 16) <= precal_td_i;   -- PreCAL Delay
      when IntScanReg1 => regbus_d_out(15 downto  0) <= trig_delay0_i; -- TrigDelay1
                    regbus_d_out(31 downto 16) <= trig_delay1_i; -- TrigDelay2
      when IntScanReg2 => regbus_d_out <= interval_i;   -- Interval
      when IntScanReg3 => regbus_d_out <= trig_mode_i;  -- TrigMode

  -- Calibration Event Type Register 0
      when CalEvntType0Reg => regbus_d_out( 9 downto  0) <= cal_event_type0_i;

  -- Calibration Event Type Register 1
      when CalEvntType1Reg => regbus_d_out( 9 downto  0) <= cal_event_type1_i;

  -- Scan Done Status Register
      when ScanDoneSttsReg => regbus_d_out(0) <= int_nevent_done_in;
--        when "00110111" => regbus_d_out <=

  -- Calibration L1ID and BCID Status
      when CalL1id0Reg => regbus_d_out(23 downto  0) <= cal_l1id0_in;
      when CalL1id1Reg => regbus_d_out(23 downto  0) <= cal_l1id1_in;
      when CalBcid0Reg => regbus_d_out(27 downto 16) <= cal_bcid1_in;
                    regbus_d_out(11 downto  0) <= cal_bcid0_in;
      when L1IdBitsReg => regbus_d_out(23 downto  0) <= l1id_bits_in;

	-- LEDs
      when DspLedsReg => regbus_d_out(7 downto  0) <= dspLeds_i; -- unused27 
		when PrmGeoAddrReg => regbus_d_out<= X"000000"&"000"&prm_ga;
		when CalPulseCount0Reg => regbus_d_out(23 downto  0) <= calpulse_count0_in;
		when CalPulseCount1Reg => regbus_d_out(23 downto  0) <= calpulse_count1_in;
      when others => regbus_d_out <= (others => '0');
    end case;
  end if;  
end process;
    
end rtl; -- code of register_block
