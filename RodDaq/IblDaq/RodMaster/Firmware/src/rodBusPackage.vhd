--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package rodBusPackage is


-- Rodbus address map. uses dspbus_ea_in (21 downto 12)

-- select between BOC and ROD 
constant BOC_SEL_SHIFT: integer := 21;
constant ROD_SEL_ADDR: std_logic := '0'; -- local ROD resources
constant BOC_SEL_ADDR: std_logic := '1'; -- BOC resources

-- Locally on ROD select further with bits 20 .. 18
constant RODBUS_ADDR_SHIFT: integer := 18;
constant SLV_NORTH_ADDR: std_logic_vector(20 downto 18) := "000";
constant SLV_SOUTH_ADDR: std_logic_vector(20 downto 18) := "001";
constant MASTER_INTERNAL_REG: std_logic_vector(20 downto 18) := "010";

-- extending RCF:
-- if (dspbus_ea_in(11) = '0') then
--	case select_int(dspbus_ea_in(10 downto 8)) is
--	  when "100" => intbus_en_n_i(4) <= '0'; -- Internal Registers
--	  when "101" => intbus_en_n_i(5) <= '0'; -- FE Occupancy Counters
--	  when "110" => intbus_en_n_i(6) <= '0'; -- FE Command Mask LUT
--	  when "111" => intbus_en_n_i(7) <= '0'; -- EFB DynmicMask Encoder
--	  when others => intbus_en_n_i   <= (others => '1');
--	end case;
-- elsif (dspbus_ea_in(11) = '1') then
--	intbus_en_n_i(8) <= '0'; -- Formatter ModeBits Encoder
constant INT_OFFS_SHIFT: integer := 0;
constant INT_RCF_OFFS: std_logic_vector(11 downto 8) := "0000";
constant INT_OCC_OFFS: std_logic_vector(11 downto 8) := "0101";
constant INT_LUT_OFFS: std_logic_vector(11 downto 8) := "0110";
constant INT_MSK_OFFS: std_logic_vector(11 downto 8) := "0111";
constant INT_MOD_OFFS: std_logic_vector(11 downto 8) := "1000";

--	when "0000000110" =>  -- All external ROD Fifos
constant EXTFIFO_ADDR: std_logic_vector(17 downto 12) := "000110";
--	when "0001000000" =>  -- BOC. This setting allows 64k registers on the BOC


--

end rodBusPackage;

