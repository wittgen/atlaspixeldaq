----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:39:37 08/26/2013 
-- Design Name: 
-- Module Name:    fe_command_processor - RTL 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

entity fe_command_processor is
  port(
    clk                       : in  std_logic;
    rst                       : in  std_logic;
	 rrif_control_register_in  : in  std_logic_vector(23 downto  0);
    tim_signals_in            : in  std_logic_vector( 3 downto  0);
    rrif_status_register_out  : out std_logic_vector(29 downto 22);
    spare_status_register_out : out std_logic_vector(18 downto  8);
    corrective_fe_pulse_out   : out std_logic;
    calibrate_command_in      : in  std_logic_vector(26 downto  0);
    cal_trig_delay_in         : in  std_logic_vector( 7 downto  0);
    fe_command_mask0_in       : in  std_logic_vector(15 downto  0);
    fe_command_mask1_in       : in  std_logic_vector(15 downto  0); 
    long_command_stream_in    : in  std_logic_vector( 1 downto  0); 
    long_command_pulse_in     : in  std_logic_vector( 1 downto  0); 
    command_stream_out        : out std_logic_vector(15 downto  0); 
    pulse_stream_out          : out std_logic_vector(15 downto  0); 
    fe_cmd_mask_updated_out   : out std_logic;
    fmt_fe_cmd_out            : out std_logic;
    fe_cmdpulse_AB_out        : out std_logic_vector( 1 downto  0);
    controller_int_out        : out std_logic_vector( 1 downto  0); 
    command_cycle_done_out    : out std_logic;
    store_current_trigger_out : out std_logic;
    current_trigger_out       : out std_logic_vector( 1 downto  0);
    dynamic_mask_sent_in      : in  std_logic;
    modebits_sent_in          : in  std_logic;
    efb_ready_in              : in  std_logic;
    modebits_error_out        : out std_logic
  );
end fe_command_processor;

architecture rtl of fe_command_processor is

------------------------------------------------------------------------------
-- SIGNAL DECLARATION
------------------------------------------------------------------------------

signal fe_pulse_i             : std_logic; --
signal corrective_fe_pulse_i  : std_logic; --

------------------------------------------------------------------------------
-- COMPONENT DECLARATION
------------------------------------------------------------------------------
component fe_command_generators
  port(
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    enable_in                 : in  std_logic;
    cnfg_cal_new_mask_load_in : in  std_logic;
    l1accept_in               : in  std_logic;
    bcr_in                    : in  std_logic;
    ecr_in                    : in  std_logic;
    calibrate_in              : in  std_logic;
    calibrate_command_in      : in  std_logic_vector(26 downto 0);
    cal_trig_delay_in         : in  std_logic_vector( 7 downto 0);
    fast_not_long_commands_in : in  std_logic;
    new_mask_ready_in         : in  std_logic;
    corrective_mask_ready_in  : in  std_logic;
    fe_command_mask0_in       : in  std_logic_vector(15 downto 0);
    fe_command_mask1_in       : in  std_logic_vector(15 downto 0); 
    long_command_stream_in    : in  std_logic_vector( 1 downto 0); 
    long_command_pulse_in     : in  std_logic_vector( 1 downto 0);
    command_stream_out        : out std_logic_vector(15 downto 0);
    pulse_stream_out          : out std_logic_vector(15 downto 0);
    fe_pulse_out              : out std_logic;
    corrective_fe_pulse_out   : out std_logic; 
    fe_cmd_mask_updated_out   : out std_logic;
    fmt_fe_cmd_out            : out std_logic;
    fmt_fe_cmd_mask_n_in      : in  std_logic
    );
end component;

component fe_command_pulse_counter
  port(
    clk                       : in  std_logic;
    rst                       : in  std_logic;
    control_in                : in  std_logic_vector(1 downto 0); 
    fe_cmdpulse_in            : in  std_logic;
    corrective_fe_cmdpulse_in : in  std_logic;
    fe_cmdpulse_count_out     : out std_logic_vector(7 downto 0);
    fe_cmdpulse_AB_out        : out std_logic_vector(1 downto 0);
    controller_int_out        : out std_logic_vector(1 downto 0);
    command_cycle_done_out    : out std_logic;
    store_current_trigger_out : out std_logic;
    current_trigger_out       : out std_logic_vector(1 downto 0);
    dynamic_mask_sent_in      : in  std_logic;
    modebits_sent_in          : in  std_logic;
    efb_ready_in              : in  std_logic;
    fe_trigger_occupancy_out  : out std_logic_vector(10 downto 0);
    modebits_error_out        : out std_logic
    );
end component;

begin
------------------------------------------------------------------------------
-- COMPONENT ASSIGNMENT
------------------------------------------------------------------------------
fe_command_generators_instance: fe_command_generators
  port map (
    clk                       => clk,
	 rst                       => rst,
    enable_in                 => rrif_control_register_in(0), --
    cnfg_cal_new_mask_load_in => rrif_control_register_in(20), --
    l1accept_in               => tim_signals_in(0), --
    bcr_in                    => tim_signals_in(2), --
    ecr_in                    => tim_signals_in(1), --
    calibrate_in              => tim_signals_in(3), --
    calibrate_command_in      => calibrate_command_in, --
    cal_trig_delay_in         => cal_trig_delay_in, --
    fast_not_long_commands_in => rrif_control_register_in(1), --
    new_mask_ready_in         => rrif_control_register_in(2), --
    corrective_mask_ready_in  => rrif_control_register_in(23), --
    fe_command_mask0_in       => fe_command_mask0_in, --
    fe_command_mask1_in       => fe_command_mask1_in, --
    long_command_stream_in    => long_command_stream_in, --
    long_command_pulse_in     => long_command_pulse_in, --
    command_stream_out        => command_stream_out, --
    pulse_stream_out          => pulse_stream_out, --
    fe_pulse_out              => fe_pulse_i, --
    corrective_fe_pulse_out   => corrective_fe_pulse_i, --
    fe_cmd_mask_updated_out   => fe_cmd_mask_updated_out, --
    fmt_fe_cmd_out            => fmt_fe_cmd_out, --
    fmt_fe_cmd_mask_n_in      => rrif_control_register_in(18) --
    );
	
fe_command_pulse_counter_instance: fe_command_pulse_counter
  port map (
    clk                       => clk,
	 rst                       => rst,
    control_in                => rrif_control_register_in(6 downto 5),  --
    fe_cmdpulse_in            => fe_pulse_i, --
    corrective_fe_cmdpulse_in => corrective_fe_pulse_i, --
    fe_cmdpulse_count_out     => rrif_status_register_out(29 downto 22), --
    fe_cmdpulse_AB_out        => fe_cmdpulse_AB_out, --
    controller_int_out        => controller_int_out, --
    command_cycle_done_out    => command_cycle_done_out, --
    store_current_trigger_out => store_current_trigger_out, --
    current_trigger_out       => current_trigger_out, --
    dynamic_mask_sent_in      => dynamic_mask_sent_in, --
    modebits_sent_in          => modebits_sent_in, --
    efb_ready_in              => efb_ready_in, --
    fe_trigger_occupancy_out  => spare_status_register_out(18 downto 8), --
    modebits_error_out        => modebits_error_out --
    );

------------------------------------------------------------------------------
-- PROCESS
------------------------------------------------------------------------------


end rtl;

