--------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------
--
-- Filename: testbench_interface.vhd               
-- Description: Control the rod datapath for the debugmodes described and 
--              enumerated in /home/edafiles/atlas/rod/Rod_RRIF_DebugModes.txt
-------------------------------------------------------------------------------
-- Test Modes:
--
-- 'normal datatpath' runtime debugging modes
--  when "100000" => Inmem->Formatters->EFB->Router->SDSP
--    Note :  we use the L1 trigger to start the inmem fifos to allow the the
--            test to simulate real data into the ROD
--            inmem_a_word_count - Plays all of the FIFOs
--
--  when "100100" => Inmem->Formatters->EFB->Router->SDSP with retransmit
--    Note :  we use the L1 trigger to start the inmem fifos to allow the the
--            test to simulate real data into the ROD
--            inmem_a_word_count  - Plays internal TIM FIFO
--            inmem_b_word_count  - Retransmit Count
--
--  Retransmit Timing
--                    __________________
--   RT _____________/    15ns min      \_________________________________
--         ____________________________________________________
--   REN_N                              |    15ns min         |\___________
--
--------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- History:
--    1 February 2001 - MLN first version began
--   23 March    2001 - MLN first version completed
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------
entity testbench_interface is
  port (
    clk                          : in  std_logic;   
    rst                          : in  std_logic;   
    control_in                   : in  std_logic_vector( 1 downto 0); --
--  control_in:  Bit 1=>run/stop, Bit 0 >> 1=>enable, 0=>clear
    debugmode_cnfg_register_in   : in  std_logic_vector(17 downto 0); -- 
    debugmode_stts_register_out  : out std_logic_vector( 7 downto 0); -- 
    inmem_wordcount_register_in  : in  std_logic_vector(31 downto 0); -- 
    dbgmem_wordcount_register_in : in  std_logic_vector(31 downto 0); -- 
    inmem_ctrl_out               : out std_logic_vector(11 downto 0); -- 
    int_tim_fifo_re_out          : out std_logic; --
    int_tim_fifo_rt_out          : out std_logic; --
    l1_trigger_in                : in  std_logic;   
    pause_trigger_rt             : in  std_logic    
    );
end testbench_interface;

-------------------------------------------------------------------------------
-- 
-- TestBench Interface Signal Definitions
-- Please resolve any changes with the signals in top.vhd / vice-versa.
--
-- inmem_ctrl_out( 0) == detector_to_form_sel_n
-- inmem_ctrl_out( 1) == detector_to_form_sel_n
-- inmem_ctrl_out( 2) == detector_to_fifo_a_sel_n
-- inmem_ctrl_out( 3) == detector_to_fifo_b_sel_n
-- inmem_ctrl_out( 4) == detector_to_form_sel_n (fifo_a_to_detector_sel_n)
-- inmem_ctrl_out( 5) == detector_to_form_sel_n (fifo_b_to_detector_sel_n)
-- inmem_ctrl_out( 6) == inmem_fifo_a_wen_n
-- inmem_ctrl_out( 7) == inmem_fifo_a_ren_n
-- inmem_ctrl_out( 8) == inmem_fifo_b_wen_n
-- inmem_ctrl_out( 9) == inmem_fifo_b_ren_n
-- inmem_ctrl_out(10) == inmem_a_rt
-- inmem_ctrl_out(11) == inmem_b_rt
--

architecture rtl of testbench_interface is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

-- Test Fixture Mode ## _ state number
type states_action is (
  idle,     -- others case overall inactivity state
  idle_m00, --
  tfm00_s0,
  tfm00_s1,
  tfm00_s2,
  tfm00_s3,
  tfm00_s4,
  tfm00_s5,
  tfm00_s6,
  tfm00_ack
  );
signal actncontrol_state : states_action; 

signal modecontrol_i     : std_logic_vector(5 downto 0);
signal trigger_dly1_i    : std_logic; 
signal int_tim_fifo_re_i : std_logic; --
signal iwc_a_count_i     : unsigned(16 downto 0);
signal iwc_a_encount_i   : std_logic;
signal iwc_a_encount_ii  : std_logic;
signal iwc_a_done_i      : std_logic;
signal iwc_b_count_i     : unsigned(16 downto 0);
signal iwc_b_encount_i   : std_logic;
signal iwc_b_encount_ii  : std_logic;
signal iwc_b_done_i      : std_logic;
-- 4 different "levels of done-ness" can be encoded here.
signal op_done0_i        : std_logic;  -- operation done/completed0 
signal op_done1_i        : std_logic;  -- operation done/completed1

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
begin

int_tim_fifo_re_out <= int_tim_fifo_re_i;

debugmode_stts_register_out(0) <= iwc_a_done_i;  -- word  counter
debugmode_stts_register_out(1) <= iwc_b_done_i;  -- word  counter
debugmode_stts_register_out(2) <= '0';  -- word  counter
debugmode_stts_register_out(3) <= '0';  -- word  counter
debugmode_stts_register_out(6) <= op_done0_i;  -- operation done/completed0
debugmode_stts_register_out(7) <= op_done1_i;  -- operation done/completed1

iwc_a_encount_ii <=  iwc_a_encount_i OR debugmode_cnfg_register_in( 0);
iwc_b_encount_ii <=  iwc_b_encount_i OR debugmode_cnfg_register_in( 2);
--
-- The configuration register chooses a mode of operation, in which signals are
-- issued based on triggers/instructions from the CPU.  The mode chosen by the
-- debugmode_config_mode_register selects the type of testbench operation to be
-- performed.  No control signals will be created or processed unless this is
-- properly configured and enabled.  The TB IO Control Signal Mux in the RRIF
-- Control Register is used to select signals from this block.  The status bits
-- going back indicate when the operation is finished.  The system will idle in
-- a done state until the "run" bit is cleared.  The done bit will then be
-- cleared, and the system will return to the idle state for the selected mode.
--
modecontrol_i <= debugmode_cnfg_register_in(17 downto 12);

dataflow_controller : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    iwc_a_encount_i   <= '0';
    iwc_b_encount_i   <= '0';
    int_tim_fifo_re_i   <= '0';
    int_tim_fifo_rt_out <= '0';  -- Clear TIM Re-Transmit Signal
    inmem_ctrl_out(11 downto 10)  <= (others => '0');  -- inmem re-transmit signal must be held low
    inmem_ctrl_out( 9 downto  0)  <= (others => '1');
    actncontrol_state <= idle; 
  elsif (clk'event and clk = '1') then
    trigger_dly1_i <= control_in(1);
    if (control_in(0) = '1') then -- testfixture operation enabled
      if (modecontrol_i = "100000") then
-- 'normal datatpath' runtime debugging modes
-- Note :  we have to use the L1 trigger to start the inmem fifos to 
--         allow the the test to simulate real data into the ROD
--  Required Register : inmem_a_word_count. Plays all of the FIFOs
         case actncontrol_state is
          when idle_m00 =>    -- sit there waiting for a trigger (positive edge) from control_reg_in(2)
            inmem_ctrl_out(11 downto 10)    <= (others => '0');  -- inmem rt
            inmem_ctrl_out( 9 downto  0)    <= (others => '1');
            int_tim_fifo_re_i <= '0';
            iwc_a_encount_i   <= '0';
            op_done0_i        <= '0';
            op_done1_i        <= '0';
          --
            if (control_in(1) = '1' AND trigger_dly1_i = '0') then
              actncontrol_state <= tfm00_s0;
            end if;

          when tfm00_s0 =>  -- Start playing the TIM Fifo
            inmem_ctrl_out(7) <= '1'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '1'; -- inmem_fifo_b_ren_n
            int_tim_fifo_re_i <= '1';
            iwc_a_encount_i   <= '0';
            op_done0_i        <= '0';
            op_done1_i        <= '0';
          --
            actncontrol_state <= tfm00_s1;

          when tfm00_s1 =>  -- wait for the L1 trigger before playing the inmem FIFOs. 
            inmem_ctrl_out(7) <= '1'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '1'; -- inmem_fifo_b_ren_n
            int_tim_fifo_re_i <= '1';
            iwc_a_encount_i   <= '1';
            op_done0_i        <= '0';
            op_done1_i        <= '1';
          --
            if (l1_trigger_in = '1') then
              actncontrol_state <= tfm00_s2;
            end if;

          when tfm00_s2 =>    -- start playing the inmem FIFOs
            inmem_ctrl_out(7) <= '0'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '0'; -- inmem_fifo_b_ren_n
            int_tim_fifo_re_i <= '1';
            iwc_a_encount_i   <= '1';
            op_done0_i        <= '1';
            op_done1_i        <= '0';
          --
            if (iwc_a_done_i = '1') then
              actncontrol_state <= tfm00_ack;
            end if;

          when tfm00_ack =>    -- wait in the done state until the controller confirms to exit the operation
            inmem_ctrl_out(7) <= '1'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '1'; -- inmem_fifo_b_ren_n
            int_tim_fifo_re_i <= '0';
            iwc_a_encount_i   <= '0';
            op_done0_i        <= '1';
            op_done1_i        <= '1';
          --
            if (control_in(1) = '0') then
              actncontrol_state <= idle_m00;
            end if;

          when others =>
            actncontrol_state <= idle_m00;
         end case; -- signaling state machine mode "100000"

      elsif (modecontrol_i = "100100") then
--Inmem->Formatters->EFB->Router->SDSP with retransmit
--    Note :  we use the L1 trigger to start the inmem fifos to allow the the
--            test to simulate real data into the ROD
--            inmem_a_word_count  - Plays all FIFOs
--            inmem_b_word_count  - Retransmit Count
        case actncontrol_state is
          when idle_m00 =>    -- sit there waiting for a trigger (positive edge) from control_reg_in(2)
            inmem_ctrl_out(11 downto 10) <= (others => '0');  -- inmem rt
            inmem_ctrl_out( 9 downto  0) <= (others => '1');
            int_tim_fifo_re_i <= '0';  -- Hold off internal TIM FIFO
            iwc_a_encount_i   <= '0';  -- Hold off counters
            iwc_b_encount_i   <= '0';
            op_done0_i        <= '0';  -- Clear OpDone Status Bits
            op_done1_i        <= '0';
          --
            if (control_in(1) = '1' AND trigger_dly1_i = '0') then
              actncontrol_state <= tfm00_s0;
            end if;

          when tfm00_s0 =>  -- Start playing the TIM Fifo
            int_tim_fifo_re_i <= '1';
            iwc_b_encount_i   <= '1';  -- Enable Transmit Counter
            op_done0_i        <= '1';
            op_done1_i        <= '0';
          --
            actncontrol_state <= tfm00_s1;

          when tfm00_s1 =>  -- wait for the L1 trigger before playing the inmem FIFOs. 
            iwc_a_encount_i <= '1';  -- Enable FIFO Counter
            iwc_b_encount_i <= '0';  -- Disable Transmit Counter
          --
            if (l1_trigger_in = '1') then
              actncontrol_state <= tfm00_s2;
            end if;

          when tfm00_s2 =>    -- start playing the inmem FIFOs
            inmem_ctrl_out(7) <= '0'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '0'; -- inmem_fifo_b_ren_n
            op_done0_i        <= '0';
            op_done1_i        <= '1';
          --
            if (iwc_a_done_i = '1') then
              actncontrol_state <= tfm00_s3;
            end if;

          when tfm00_s3 =>  -- Stop playing all FIFOs
            int_tim_fifo_re_i <= '0';
            inmem_ctrl_out(7) <= '1'; -- inmem_fifo_a_ren_n
            inmem_ctrl_out(9) <= '1'; -- inmem_fifo_b_ren_n
          --
            iwc_a_encount_i   <= '0';  -- Disable FIFO Counter

            if (iwc_b_done_i = '1') then
              actncontrol_state <= tfm00_ack;
            else
              actncontrol_state <= tfm00_s4;
            end if;

          when tfm00_s4 =>  -- Send Retransmit Command
            int_tim_fifo_rt_out <= '1';  -- Set TIM Re-Transmit Signal
            inmem_ctrl_out(11 downto 10) <= (others => '1');  -- Set Inmem Re-Transmit Signal
          --
            actncontrol_state <= tfm00_s5;

          when tfm00_s5 =>  -- Return to State 0
            int_tim_fifo_rt_out <= '0';  -- Clear TIM Re-Transmit Signal
            inmem_ctrl_out(11 downto 10) <= (others => '0');  -- Clear Inmem re-transmit signal
          --
            if (pause_trigger_rt = '0') then
              actncontrol_state <= tfm00_s0;
            end if;

          when tfm00_ack =>    -- wait in the done state until the controller confirms to exit the operation
            int_tim_fifo_re_i <= '0';
            iwc_a_encount_i   <= '0';
            op_done0_i        <= '1';
            op_done1_i        <= '1';
          --
            if (control_in(1) = '0') then
              actncontrol_state <= idle_m00;
            end if;

          when others =>
            actncontrol_state <= idle_m00;
         end case;
       else  -- The dsp has put us into an undefined testbench mode.
         actncontrol_state <= idle; 
         iwc_a_encount_i   <= '0';
         iwc_b_encount_i   <= '0';
         int_tim_fifo_re_i   <= '0';
         int_tim_fifo_rt_out <= '0';  -- Clear TIM Re-Transmit Signal
         inmem_ctrl_out(11 downto 10) <= (others => '0');  -- inmem rt
         inmem_ctrl_out( 9 downto  0) <= (others => '1');
      end if; -- mode selection
    else
      iwc_a_encount_i     <= '0';
      iwc_b_encount_i     <= '0';
      int_tim_fifo_re_i   <= '0';
      int_tim_fifo_rt_out <= '0';  -- Clear TIM Re-Transmit Signal
      inmem_ctrl_out(11 downto 10) <= (others => '0');  -- inmem rt
      inmem_ctrl_out( 9 downto  0) <= (others => '1');
    end if;
  end if;
end process dataflow_controller;

-------------------------------------------------------------------------------
-- Counters
inmem_a_word_counter : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    iwc_a_count_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (iwc_a_encount_ii = '1') then
      iwc_a_count_i <= iwc_a_count_i - 1;
    else
      iwc_a_count_i <= '0' & unsigned(inmem_wordcount_register_in(15 downto 0));
    end if;
  end if;
end process;

inmem_a_word_done : process (  
  clk,
  rst
  )
begin
  if (rst = '1') then
    iwc_a_done_i <= '0'; 
  elsif (clk'event and clk = '1') then
    if (iwc_a_count_i(16) = '1') then
      iwc_a_done_i <= '1'; 
    else
      iwc_a_done_i <= '0'; 
    end if;
  end if;
end process; 

inmem_b_word_counter : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    iwc_b_count_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (iwc_b_encount_ii = '1') then
      iwc_b_count_i <= iwc_b_count_i - 1;
    elsif (iwc_b_encount_ii = '0' AND actncontrol_state <= idle_m00) then
      iwc_b_count_i <= '0' & unsigned(inmem_wordcount_register_in(31 downto 16)); 
    end if;
  end if;
end process; 

inmem_b_word_done : process (
  clk,
  rst
  )
begin
  if (rst = '1') then
    iwc_b_done_i <= '0';  
  elsif (clk'event and clk = '1') then
    if (iwc_b_count_i(16) = '1') then
      iwc_b_done_i <= '1';  
    else
      iwc_b_done_i <= '0';  
    end if;
  end if;
end process; 

end rtl; -- end code for testbench_interface
