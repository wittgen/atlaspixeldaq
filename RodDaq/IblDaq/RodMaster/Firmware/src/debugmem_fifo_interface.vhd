-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
--
-- Filename: debugmem_fifo_interface.vhd           
-- Description: interface the rrif internal bus to the boc.
--  Re-register data going to the wider databus and data coming from
--  a wider databus. 
-- Reading/Writing a wider bus involves a bit of handshaking:
--
-- reading:
--  a read to the read_strobe location must be received to clock new data into
--  the onboard_temp_registers.  Readout from the temporary registers can then
--  commence as if accessing normal rrif internal registers.
--       - dspbus_ed(15 downto 0) <= somemem_reg(47 downto 32)
--       - dspbus_ed(31 downto 0) <= somemem_reg(31 downto  0)
--
-- writing:
--       - dspbus_ed(15 downto 0) => somemem_reg(47 downto 32)
--       - dspbus_ed(31 downto 0) => somemem_reg(31 downto  0)
--  a write to the write_strobe location must be received to clock new data
--  from the onboard_temp_write_registers into the fifo.
-- 
--
--  The following state machines are all 1 stage glitch free MOORE MACHINES.
--   as per the LBL template  "vhdl/sm1_sample.vhd" code.
--
--
-- Note: 
-- The normal data path MUST BE DISABLED OR THIS BLOCK WILL CAUSE BUS CONTENTION!!!
--    That is - the rrif_control_register signals that enable this block MUST also 
--        exclude the normal data path from working.
--
--
-- normal_input_data_path                                        +
--                  |                                            |\
--                  +--------------------------------------------| +--+-- normal_output_data_path
--                  |                                            |/   |
--                  |                                            +|   |
--                  |  +                      fifo_output_mux_control |
--                  |  |\         +-------------+                +|   |
--                  +--| +---+----| debug fifo  |-----+          |\   |
--                     |/    |  +-|we         re|-+   +----------| +--+
--                     +|    |  | +-------------+ |   |          |/  
--                      |    |  |+----------------+   |          + 
--                      |    |  ||                    |            
--    fifo_input_mux_control |  ||                    |            
--                      |    |  ||                    |          + 
--                      |    |  ||                    |  +---+   |\
--                      |    |  ||                    +--|d q|---| +---+
--                     +|    |  ||               clk40 -->   |   |/    |
--              +---+  |\    |  ||                    +--|en |   +|    |
--    +---------|d q|--| +---+  ||                    |  +---+    |    |
--    | clk40 -->   |  |/       ||                    |           +--+ |
--    |       +-|en |  +        ||                    |              | | 
--    |       | +---+           ||                    +------------+ | |
--    |       |                 ||                                 | | |
--    |       +--------------------------------------------------+ | | |
--    +----------------------------------------------------+-----------+
--                              ||                         |     | | |
--                              ||                         |     | | |
--                  ...................                    |     | | |
-- dspbus_d_inout --. rrif bus bridge .-- rodbus_d_inout --+     | | |
--                  ...................                          | | |
--                              ||                               | | |
--                              ||                               | | |
--                      +-------------------------+              | | |            
--                      | xfer_state_machine      |- in_stbh_n --+ | |
--                      |                         |- in_stbl_n --+ | |          
--                      |                         |                | |
-- rnw -----------------|                         |- out_stbh_n ---+ |
-- adr -----------------|                         |- out_stbl_n ---+ |
-- inmem_en_n ----------|                         |                  |
-- debugmem_en_n -------|                         |- oe_h_n ---------+
-- eventmem_en_n -------|                         |- oe_l_n ---------+
--                      |                         |
--                      |                         |    
-- clk40 --------------->                         |----- ack
--                      +-------------------------+          
--                                                           
-- The signal names have been kept general, as there are 3 such state machines
--  in this block of code 1/ea for inmem, debugmem, eventmem.
-- The eventmem is normally under the control of the efb and so does not have
--  quite the same buffering and control configuration. The control signals
--  are sent directly into the efb, and the efb muxes/switches/combines them
--  with the normal fifo control signals to obtain the desired results.
-- The rnw/adr/n_en_n combine to become the respective read write strobes.  
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- History:
--   21 March 2000 - MLN first version :
--   30 March 2000 - MLN control of the writein and readout are complete 
--   31 March 2000 - MLN need to add signals for the play and record modes
--                    which means add play and record (both = loopback) 
--                    signals to the control registers for each debugmemory 
--                    bank that enable a play/loop counter and word counter 
--                    for the fifo_we and fifo_re signals.
--
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity debugmem_fifo_interface is
  port(
    clk               : in  std_logic; 
    rst               : in  std_logic;
    control_in        : in  std_logic; -- 1=enable, 0=idle
    address_in        : in  std_logic_vector(6 downto 0); --
    rnw_in            : in  std_logic; --      
    inmem_en_n_in     : in  std_logic; --      
    eventmem_en_n_in  : in  std_logic; -- 
    inmem_ctrl_out    : out std_logic_vector(15 downto 0); --
    eventmem_ctrl_out : out std_logic_vector(10 downto 0); --
    ack_out           : out std_logic  --
    );
end debugmem_fifo_interface; 

architecture rtl of debugmem_fifo_interface is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  idle,
  inmem_read, 
  inmem_write, 
  evtmem_read, 
  evtmem_write, 
  acknowledge
  );
signal ackn_state : states;

signal rstrb_n_i    : std_logic;
signal rdata_en_n_i : std_logic;
signal wstrb_n_i    : std_logic;
signal wdata_en_n_i : std_logic;

signal inmem_en_i   : std_logic;
signal evtmem_en_i  : std_logic;

signal strobe_count : unsigned(1 downto 0);

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
begin

-------------------------------------------------------------------------------
-- Input Memory Control Signal Definitions
--
-- inmem_ctrl_out( 0) :   inmem_to_fifo_a_sel_n
-- inmem_ctrl_out( 1) :   inmem_to_fifo_a_ldstrb
-- inmem_ctrl_out( 2) :   inmem_to_fifo_a_udstrb
-- inmem_ctrl_out( 3) :   inmem_to_fifo_b_sel_n
-- inmem_ctrl_out( 4) :   inmem_to_fifo_b_ldstrb
-- inmem_ctrl_out( 5) :   inmem_to_fifo_b_udstrb
-- inmem_ctrl_out( 6) :   fifo_a_to_inmem_ldsel_n
-- inmem_ctrl_out( 7) :   fifo_a_to_inmem_udsel_n
-- inmem_ctrl_out( 8) :   fifo_a_to_inmem_strb
-- inmem_ctrl_out( 9) :   fifo_b_to_inmem_ldsel_n 
-- inmem_ctrl_out(10) :   fifo_b_to_inmem_udsel_n
-- inmem_ctrl_out(11) :   fifo_b_to_inmem_strb
-- inmem_ctrl_out(12) :   inmem_fifo_a_we_n 
-- inmem_ctrl_out(13) :   inmem_fifo_a_re_n
-- inmem_ctrl_out(14) :   inmem_fifo_b_we_n
-- inmem_ctrl_out(15) :   inmem_fifo_b_re_n
--
--  Read operation:                       --  Write operation:
--  data_en_n:   ---  ---                 --  data_en_n:   --  ----
--                  __                    --                 __
--
--  strb_n:      --- ----                 --  strb_n:      --- ----
--                  _                     --                  _    
--
inmem_strobe_select : process (
  clk,
  rst,
  control_in,
  address_in, 
  rnw_in, 
  inmem_en_n_in,
  rdata_en_n_i, 
  rstrb_n_i,
  wdata_en_n_i, 
  wstrb_n_i
  )
begin
  if (rst = '1') then
    inmem_ctrl_out(15 downto 0) <= (others => '1');
  else
    inmem_ctrl_out(15 downto 0) <= (others => '1');
    if (control_in = '1' AND inmem_en_n_in = '0') then
      if (rnw_in = '1') then
        inmem_ctrl_out(15 downto 0) <= (others => '1');
        if    (address_in = "0001000") then 
          inmem_ctrl_out( 6) <= inmem_en_n_in; -- fifo_a_to_inmem_ldsel_n 
        elsif (address_in = "0001001") then 
          inmem_ctrl_out( 7) <= inmem_en_n_in; -- fifo_a_to_inmem_udsel_n    
        elsif (address_in = "0001010") then 
          inmem_ctrl_out( 9) <= inmem_en_n_in; -- fifo_b_to_inmem_ldsel_n 
        elsif (address_in = "0001011") then 
          inmem_ctrl_out(10) <= inmem_en_n_in; -- fifo_b_to_inmem_udsel_n   
        elsif (address_in = "0010000") then 
          inmem_ctrl_out(13) <= rstrb_n_i;     -- inmem_fifo_a_re_n        
          inmem_ctrl_out( 8) <= rdata_en_n_i;  -- fifo_a_to_inmem_strb
        elsif (address_in = "0010010") then 
          inmem_ctrl_out(15) <= rstrb_n_i;     -- inmem_fifo_b_re_n      
          inmem_ctrl_out(11) <= rdata_en_n_i;  -- fifo_b_to_inmem_strb
        else
          inmem_ctrl_out(15 downto 0) <= (others => '1');
        end if;
      elsif (rnw_in = '0') then
        inmem_ctrl_out(15 downto 0) <= (others => '1');
        if    (address_in = "0000000") then 
          inmem_ctrl_out( 1) <= wstrb_n_i;     -- inmem_to_fifo_a_ldstrb 
        elsif (address_in = "0000001") then 
          inmem_ctrl_out( 2) <= wstrb_n_i;     -- inmem_to_fifo_a_udstrb   
        elsif (address_in = "0000010") then 
          inmem_ctrl_out( 4) <= wstrb_n_i;     -- inmem_to_fifo_b_ldstrb    
        elsif (address_in = "0000011") then 
          inmem_ctrl_out( 5) <= wstrb_n_i;     -- inmem_to_fifo_b_udstrb   
        elsif (address_in = "0010000") then  
          inmem_ctrl_out(12) <= wstrb_n_i;     -- inmem_fifo_a_we_n
          inmem_ctrl_out( 0) <= wdata_en_n_i;  -- inmem_to_fifo_a_sel_n  
        elsif (address_in = "0010010") then 
          inmem_ctrl_out(14) <= wstrb_n_i;     -- inmem_fifo_b_we_n
          inmem_ctrl_out( 3) <= wdata_en_n_i;  -- inmem_to_fifo_b_sel_n    
        else
          inmem_ctrl_out(15 downto 0) <= (others => '1');
        end if;
      end if;
    end if;
  end if;
end process inmem_strobe_select;


-------------------------------------------------------------------------------
-- eventmem_ctrl_out( 0) 8 : efb_to_eventmem_strb
-- eventmem_ctrl_out( 1) 9 : efb_to_eventmem_ldsel_n
-- eventmem_ctrl_out( 2) 10: efb_to_eventmem_udsel_n
-- eventmem_ctrl_out( 3) 12: efb_a_re_n
-- eventmem_ctrl_out( 4) 13: efb_a_oe_n
-- eventmem_ctrl_out( 5) 14: efb_a_en_n
-- eventmem_ctrl_out( 6) 16: efb_b_re_n
-- eventmem_ctrl_out( 7) 17: efb_b_oe_n
-- eventmem_ctrl_out( 8) 18: efb_b_en_n
-- eventmem_ctrl_out( 9) 20: efb_htf_re_n
-- eventmem_ctrl_out(10) 22: efb_htf_en_n
--
-- Note: The fifo ren and oen are piped through the EFB fpga and delayed by 
--  one clock cycle, and so must be sent 1 clock earlier 
--  Read operation:
--  data_en_n:   --  ----
--                 __
--  strb_n:      -- -----
--                 _    
--
evtmem_strobe_select : process (
  clk,
  rst,
  control_in,
  address_in, 
  rnw_in, 
  eventmem_en_n_in, 
  rdata_en_n_i, 
  rstrb_n_i
  )
begin
  if (rst = '1') then
    eventmem_ctrl_out(10 downto 0) <= (others => '1');
  elsif (clk'event and clk = '1') then
    eventmem_ctrl_out(10 downto 0) <= (others => '1');
    if (control_in = '1' AND eventmem_en_n_in = '0') then
      eventmem_ctrl_out(10 downto 0) <= (others => '1');
      if (rnw_in = '1') then
        eventmem_ctrl_out(10 downto 0) <= (others => '1');
        if    (address_in = "1001000") then 
          eventmem_ctrl_out( 1) <= eventmem_en_n_in; -- eventmem_readreg_a0_loc  
        elsif (address_in = "1001001") then 
          eventmem_ctrl_out( 2) <= eventmem_en_n_in; -- eventmem_readreg_a1_loc 
        elsif (address_in = "1001010") then 
          eventmem_ctrl_out( 1) <= eventmem_en_n_in; -- eventmem_readreg_b0_loc  
        elsif (address_in = "1001011") then 
          eventmem_ctrl_out( 2) <= eventmem_en_n_in; -- eventmem_readreg_b1_loc 
        elsif (address_in = "1001100") then 
          eventmem_ctrl_out( 1) <= eventmem_en_n_in; -- eventmem_readreg_c0_loc   
        elsif (address_in = "1010000") then
          eventmem_ctrl_out( 3) <= rstrb_n_i; -- eventmem_rw_strba_loc    
          eventmem_ctrl_out( 0) <= rdata_en_n_i;
          eventmem_ctrl_out( 4) <= rdata_en_n_i;
          eventmem_ctrl_out( 5) <= rdata_en_n_i;
        elsif (address_in = "1010010") then 
          eventmem_ctrl_out( 6) <= rstrb_n_i; -- eventmem_rw_strbb_loc    
          eventmem_ctrl_out( 0) <= rdata_en_n_i;
          eventmem_ctrl_out( 7) <= rdata_en_n_i;
          eventmem_ctrl_out( 8) <= rdata_en_n_i;
        elsif (address_in = "1010100") then 
          eventmem_ctrl_out( 9) <= rstrb_n_i; -- eventmem_rw_strbc_loc     
          eventmem_ctrl_out( 0) <= rdata_en_n_i;
          eventmem_ctrl_out(10) <= rdata_en_n_i;
        else
          eventmem_ctrl_out(10 downto 0) <= (others => '1');
        end if;
      end if;
    end if;
  end if;
end process evtmem_strobe_select;

-------------------------------------------------------------------------------
-- depending on how fast register routing paths are in the fpga
-- more or less waiting may be required.
-- this provides for an open loop acknowledge system, which is fine
-- for internal and external registers, since their routing is well defined.

rw_ackn_strobe_gen : process (
  clk,
  rst,
  inmem_en_n_in,
  eventmem_en_n_in
  )
begin
  if (rst = '1') then
    ack_out      <= '1';
    rstrb_n_i    <= '1';
    wstrb_n_i    <= '1';
    rdata_en_n_i <= '1';
    wdata_en_n_i <= '1';
    strobe_count <= (others => '0');
    ackn_state   <= idle;  
  elsif (clk'event and clk = '1') then
    inmem_en_i   <= NOT inmem_en_n_in;
    evtmem_en_i  <= NOT eventmem_en_n_in;
    if (control_in = '1') then
      case ackn_state is
        when idle =>
          ack_out      <= '0';
          rstrb_n_i    <= '1';
          wstrb_n_i    <= '1';
          rdata_en_n_i <= '1';
          wdata_en_n_i <= '1';
          strobe_count <= (others => '0');
      --
          if    (inmem_en_i = '1' AND rnw_in = '1') then
            ackn_state <= inmem_read;
          elsif (inmem_en_i = '1' AND rnw_in = '0') then 
            ackn_state <= inmem_write;
          elsif (evtmem_en_i  = '1' AND rnw_in = '1') then 
            ackn_state <= evtmem_read;
          elsif (evtmem_en_i  = '1' AND rnw_in = '0') then 
            ackn_state <= evtmem_write;
          end if;

        when inmem_read =>
          case strobe_count is
            when "00" => 
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '0';
            when "01" => 
              rstrb_n_i    <= '0';
              rdata_en_n_i <= '0';
              ack_out      <= '0';
            when "10" => 
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '0';
              ack_out      <= '0';
            when "11" =>
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
            when others =>
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
          end case;
          strobe_count <= strobe_count + '1';
  
        when inmem_write =>
          case strobe_count is
            when "00" => 
              wstrb_n_i    <= '1';
              wdata_en_n_i <= '0';
              ack_out      <= '0';
            when "01" => 
              wstrb_n_i    <= '0';
              wdata_en_n_i <= '0';
              ack_out      <= '0';
            when "10" => 
              wstrb_n_i    <= '1';
              wdata_en_n_i <= '1';
              ack_out      <= '1';
            when "11" =>
              wstrb_n_i    <= '1';
              wdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
            when others =>
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
          end case;
          strobe_count <= strobe_count + '1';

        when evtmem_read =>
          case strobe_count is
            when "00" => 
              rstrb_n_i    <= '0';
              rdata_en_n_i <= '0';
              ack_out      <= '0';
            when "01" => 
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '0';
              ack_out      <= '0';
            when "10" => 
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '0';
              ack_out      <= '1';
            when "11" =>
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
            when others =>
              rstrb_n_i    <= '1';
              rdata_en_n_i <= '1';
              ack_out      <= '1';
              ackn_state   <= acknowledge;
          end case;
          strobe_count <= strobe_count + '1';
  
        when evtmem_write =>
          ack_out      <= '1';
          ackn_state   <= acknowledge;
  
        when acknowledge =>
          ack_out      <= '1';
          rstrb_n_i    <= '1';
          wstrb_n_i    <= '1';
          rdata_en_n_i <= '1';
          wdata_en_n_i <= '1';
          strobe_count <= (others => '0');
          if (inmem_en_i = '0' AND evtmem_en_i  = '0') then
            ackn_state   <= idle; 
          end if;  
      end case;
    else 
      ack_out      <= '1';
      rstrb_n_i    <= '1';
      wstrb_n_i    <= '1';
      rdata_en_n_i <= '1';
      wdata_en_n_i <= '1';
      strobe_count <= (others => '0');
      ackn_state   <= idle; 
    end if;
  end if;
end process rw_ackn_strobe_gen;

end rtl; -- end code for debugmem_fifo_interface
