----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:39:37 08/26/2013 
-- Design Name: 
-- Module Name:    fe_command_processor - RTL 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

entity event_id_trigger_processor is
	port (
		clk            			  : in  	std_logic; --                           
		rst            			  : in  	std_logic; --                           
		rrif_control_register_in  : in  std_logic_vector(22 downto  8); --       
		tim_signals_in            : in  std_logic_vector( 5 downto  4); --       
--		SP_TRIG_CMD_DETECTOR SIGNALS 
		cal_serial0_in 			  : in  	std_logic; --                           
		cal_serial1_in 			  : in  	std_logic; --                           
		l1_accept0_out 		  	  : out 	std_logic; -- L1 Trig Detected          
		l1_accept1_out 		  	  : out 	std_logic; --                           
		l1id0_reg_in   		  	  : in  	std_logic_vector(23 downto 0); --       
		l1id1_reg_in   		  	  : in  	std_logic_vector(23 downto 0); --       
		l1id0_out      			  : out 	std_logic_vector(23 downto 0); --       
		l1id1_out      			  : out 	std_logic_vector(23 downto 0); --   
		calpulse_count0_out		  : out  std_logic_vector(23 downto 0);
		calpulse_count1_out		  : out  std_logic_vector(23 downto 0);
		bcid0_reg_in   			  : in  	std_logic_vector(11 downto 0); --       
		bcid1_reg_in   			  : in  	std_logic_vector(11 downto 0); --       
		bcid0_out      			  : out 	std_logic_vector(11 downto 0); --       
		bcid1_out      			  : out 	std_logic_vector(11 downto 0); --       
--		TRIGGER SIGNAL DECODER SIGNALS 
		l1id_bits_out        	  : out   std_logic_vector(23 downto 0); --      
		bcid_bits_out         	  : out   std_logic_vector(11 downto 0); --      
		ttype_bits_out            : out   std_logic_vector( 9 downto 0); --      
		new_l1id_valid_out  	     : out   std_logic; --                          
		new_ttype_valid_out       : out   std_logic  --                          
		);
end event_id_trigger_processor;

architecture rtl of event_id_trigger_processor is

------------------------------------------------------------------------------
-- SIGNAL DECLARATION
------------------------------------------------------------------------------


------------------------------------------------------------------------------
-- COMPONENT DECLARATION
------------------------------------------------------------------------------
component sp_trig_cmd_detector is
  port (
    clk            : in  std_logic; -- 
    rst            : in  std_logic; -- asynchronous global reset
    enable_in      : in  std_logic; -- 
    l1id_select_in : in  std_logic; -- 
    bcid_select_in : in  std_logic; -- 
    cal_serial0_in : in  std_logic; -- SerPort0
    cal_serial1_in : in  std_logic; -- SerPort1
    l1_accept0_out : out std_logic; -- L1 Trig Detected on SerPort0
    l1_accept1_out : out std_logic; -- L1 Trig Detected on SerPort1
    l1id0_reg_in   : in  std_logic_vector(23 downto 0); --
    l1id1_reg_in   : in  std_logic_vector(23 downto 0); --
    l1id0_out      : out std_logic_vector(23 downto 0); --
    l1id1_out      : out std_logic_vector(23 downto 0); --
	 calpulse_count0_out	: out std_logic_vector(23 downto 0);
	 calpulse_count1_out	: out std_logic_vector(23 downto 0);
    bcid0_reg_in   : in  std_logic_vector(11 downto 0); --
    bcid1_reg_in   : in  std_logic_vector(11 downto 0); --
    bcid0_out      : out std_logic_vector(11 downto 0); --
    bcid1_out      : out std_logic_vector(11 downto 0)  --
    );
end component;

component trigger_signal_decoder is
  port (
    clk                  : in    std_logic; --
    rst                  : in    std_logic; -- asynchronous global reset
    enable_decoding_in   : in    std_logic; -- 
    serial_id_in         : in    std_logic; --
    serial_tt_in         : in    std_logic; --
    l1id_bits_out        : out   std_logic_vector(23 downto 0); --
    bcid_bits_out        : out   std_logic_vector(11 downto 0); --
    ttype_bits_out       : out   std_logic_vector( 9 downto 0); --
    new_l1id_valid_out   : out   std_logic; --
    new_ttype_valid_out  : out   std_logic  --
    );
end component;

begin
------------------------------------------------------------------------------
-- COMPONENT ASSIGNMENT
------------------------------------------------------------------------------
sp_trig_cmd_detector_instance : sp_trig_cmd_detector
  port map (
    clk            => clk, --
    rst            => rst, --
    enable_in      => rrif_control_register_in(18), --
    l1id_select_in => rrif_control_register_in(22), --
    bcid_select_in => rrif_control_register_in(21), --
    cal_serial0_in => cal_serial0_in, --
    cal_serial1_in => cal_serial1_in, --
    l1_accept0_out => l1_accept0_out, --
    l1_accept1_out => l1_accept1_out, --
    l1id0_reg_in   => l1id0_reg_in, --
    l1id1_reg_in   => l1id1_reg_in, --
    l1id0_out      => l1id0_out, --
    l1id1_out      => l1id1_out, --
	 calpulse_count0_out	=> calpulse_count0_out,
	 calpulse_count1_out => calpulse_count1_out,
    bcid0_reg_in   => bcid0_reg_in, --
    bcid1_reg_in   => bcid1_reg_in, --
    bcid0_out      => bcid0_out, --
    bcid1_out      => bcid1_out --
    );
    
trigger_signal_decoder_instance : trigger_signal_decoder
  port map (
    clk                 => clk, --
    rst                 => rst, --
    enable_decoding_in  => rrif_control_register_in(8), --
    serial_id_in        => tim_signals_in (4), --
    serial_tt_in        => tim_signals_in (5), --
    l1id_bits_out       => l1id_bits_out, --      
    bcid_bits_out       => bcid_bits_out, --      
    ttype_bits_out      => ttype_bits_out, --
    new_l1id_valid_out  => new_l1id_valid_out, --
    new_ttype_valid_out => new_ttype_valid_out --
    );
	 
end rtl;
