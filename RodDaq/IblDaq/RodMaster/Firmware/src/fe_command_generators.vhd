--------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------
-- Filename: front_end_command_processor.vhd
-- Description: Glue the short and medium command blocks together with 
--               mask logic and registers to form the Front End Trigger
--               Command Module.
--
--------------------------------------------------------------------------
-- Structure:
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--
--------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    23 February 2000 - MLN first version : requirements/functionality finally
--                       understood and sufficiently documented to proceed
--                       with coding.
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
--------------------------------------------------------------------------
--library UNISIM;
--use unisim.all;
--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------
--
entity fe_command_generators is
  port(
    clk                       : in  std_logic;
    rst                       : in  std_logic; -- asyncronous global reset
    enable_in                 : in  std_logic;
--    rod_type_in               : in  std_logic; -- ROD Type 0=>SCT 1 =>Pixel
    cnfg_cal_new_mask_load_in : in  std_logic; -- 1=Enable Load, 0=Disable Load
    l1accept_in               : in  std_logic; -- l1trigger accept pulse, 1 clk wide.
    bcr_in                    : in  std_logic; -- bunch counter reset
    ecr_in                    : in  std_logic; -- event counter reset
    calibrate_in              : in  std_logic; -- calibrate command pulse, 1 clk wide
    calibrate_command_in      : in  std_logic_vector(26 downto 0); -- calibrate command pulse, 1 clk wide
    cal_trig_delay_in         : in  std_logic_vector( 7 downto 0); -- clks from when the calpulse to l1trig pulse sent + 1clk
    fast_not_long_commands_in : in  std_logic;
    new_mask_ready_in         : in  std_logic; -- a new FE Command mask from the controller is ready and should be loaded on the next trigger
    corrective_mask_ready_in  : in  std_logic; -- a new Corrective mask from the controller is ready and should be loaded on the next trigger
    fe_command_mask0_in       : in  std_logic_vector(15 downto 0); -- 48 bits for Pixel
    fe_command_mask1_in       : in  std_logic_vector(15 downto 0); -- 
    long_command_stream_in    : in  std_logic_vector( 1 downto 0); -- the dsp_sspi_data_out[1..0] signal 
    long_command_pulse_in     : in  std_logic_vector( 1 downto 0); -- input from the Cal_trigger Source Block
    command_stream_out        : out std_logic_vector(15 downto 0); -- command_stream bits out
    pulse_stream_out          : out std_logic_vector(15 downto 0); -- command_pulse bits out
    fe_pulse_out              : out std_logic; --  interrupt to the PPC to signify that a trigger has occurred
                                               --  and it should expect event header information AND process the next
                                               --  set of readout_mode_bits, event_dynamic_mask - then place into the 
                                               --  respective fifos.
    corrective_fe_pulse_out   : out std_logic; -- 
    fe_cmd_mask_updated_out   : out std_logic; -- after a new mask ready signal has been received, the VERY NEXT trigger
                                               --  loads the new mask into local mask register in this block, and duplicates 
                                               --  fe_pulse_out onto this line FOR ONLY THIS TRIGGER. This signal is used
                                               --  to clear the new_mask_ready_in sr flip-flop set  in the 
                                               --  front_end_command_processor control_register.
    fmt_fe_cmd_out            : out std_logic;
    fmt_fe_cmd_mask_n_in      : in  std_logic
    );
end fe_command_generators; 


architecture rtl of fe_command_generators is

--  component icon
--    PORT (
--      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--  end component;
----  attribute box_type of icon: component is "user_black_box";
--
--  component ila
--    PORT (
--      CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--      CLK : IN STD_LOGIC;
--      TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--      TRIG1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--      TRIG2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--		TRIG3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--		TRIG4 : IN STD_LOGIC_VECTOR(35 DOWNTO 0)
--		);
--  end component;
--  attribute box_type of ila: component is "user_black_box";

--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

signal l1a_in_i             : std_logic;
signal bcr_in_i             : std_logic;
signal ecr_in_i             : std_logic;
signal cal_cmdbits_buf_ai   : std_logic;
signal cal_cmdbits_buf_bi   : std_logic_vector(15 downto 0); -- 48 bits for Pixel
signal short_cmdbits_buf_ai : std_logic_vector(15 downto 0);
signal fast_cmdbits_buf_i   : std_logic_vector(15 downto 0);
signal cmd_stream_fanout0_i : std_logic_vector(15 downto 0);
signal cmd_stream_fanout1_i : std_logic_vector(15 downto 0);
signal cmd_pulse_fanout0_i  : std_logic_vector(15 downto 0);
signal cmd_pulse_fanout1_i  : std_logic_vector(15 downto 0);
signal cmd_pulse_i          : std_logic;
signal cal_trig_pulse_i     : std_logic;
signal cmd_stream_masked0_i : std_logic_vector(15 downto 0);
signal cmd_stream_masked1_i : std_logic_vector(15 downto 0);
signal cmd_pulse_masked0_i  : std_logic_vector(15 downto 0);
signal cmd_pulse_masked1_i  : std_logic_vector(15 downto 0);
signal command_i            : std_logic_vector(15 downto 0);
signal pulse_i              : std_logic_vector(15 downto 0);
signal l1_trgrd_mask0_i     : std_logic_vector(15 downto 0);
signal l1_trgrd_mask1_i     : std_logic_vector(15 downto 0);
signal cal_trig_delay_i     : unsigned(7 downto 0); 
signal long_cmd_stream_i    : std_logic_vector(1 downto 0);
signal long_cmd_pulse_i     : std_logic_vector(1 downto 0);
signal cmd_proc_busy_i      : std_logic;

--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

component fe_short_command_generator 
  port(
    clk               : in  std_logic;
    rst               : in  std_logic;
    l1accept_in       : in  std_logic; -- l1trigger accept pulse, 1 clk wide.
    bcr_in            : in  std_logic; -- bunch counter reset
    ecr_in            : in  std_logic; -- event counter reset
    command_bits_out  : out std_logic_vector(15 downto 0);  -- 48 bits for Pixel
    cmd_proc_busy_out : out std_logic
    );
end component;

component fe_cal_command_generator
  port(
    clk                  : in  std_logic;
    rst                  : in  std_logic;
    enable_in            : in  std_logic; -- 1= enabled, 0=disabled                   
--    rod_type_in          : in  std_logic; -- ROD Type 0=>SCT 1 =>Pixel
    calibrate_in         : in  std_logic; -- calibrate command pulse, 1 clk wide
    calibrate_command_in : in  std_logic_vector(26 downto 0); -- calibrate command pulse, 1 clk wide
    cal_trig_delay_in    : in  unsigned(7 downto 0); -- clks from when the calpulse to l1trig pulse sent + 1clk
    command_bits_out     : out std_logic; -- cal command bits out
    send_l1a_pulse_out   : out std_logic  -- l1a pulse to force the calibration
    );
end component;

--signal control:					std_logic_vector(35 downto 0);
--  signal trig0:					std_logic_vector(31 downto 0);
--  signal trig1:					std_logic_vector(31 downto 0);
--  signal trig2:					std_logic_vector(31 downto 0);
--  signal trig3:					std_logic_vector(31 downto 0);
--  signal trig4:					std_logic_vector(35 downto 0);

--------------------------------------------------------------------------
-- PAD DECLARATIONS
--------------------------------------------------------------------------

begin

--  icon_instance : icon
--    port map (
--      CONTROL0 => control);
--  
--  ila_instance : ila
--    port map (
--      CONTROL => control,
--      CLK => clk,		-- 40 MHz
--      TRIG0 => trig0,
--      TRIG1 => trig1,
--      TRIG2 => trig2,
--		TRIG3 => trig3,
--		TRIG4 => trig4);
--  
--  trig0(15 downto 0)		<= cmd_stream_masked0_i;
--  trig0(31 downto 16)	<= cmd_stream_masked1_i;
--  
--  trig1(15 downto 0)		<= l1_trgrd_mask0_i;
--  trig1(31 downto 16)	<= l1_trgrd_mask1_i;
--  
--  trig2(0)					<= l1accept_in;
--  trig2(1)					<= fast_not_long_commands_in;
--  
--  trig3(15 downto 0)		<= fast_cmdbits_buf_i;
  
  
--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------

-- Calibration Commands
cal_trig_delay_i <= unsigned (cal_trig_delay_in); 

fe_cal_command_generator_instance : fe_cal_command_generator
  port map(
    clk                  => clk,
    rst                  => rst,
    enable_in            => enable_in,
--    rod_type_in          => rod_type_in,
    calibrate_in         => calibrate_in,
    calibrate_command_in => calibrate_command_in,
    cal_trig_delay_in    => cal_trig_delay_i,
    command_bits_out     => cal_cmdbits_buf_ai,
    send_l1a_pulse_out   => cal_trig_pulse_i
    );

-- Fast Commands
l1a_in_i <= enable_in AND (l1accept_in OR cal_trig_pulse_i);
bcr_in_i <= enable_in AND bcr_in;
ecr_in_i <= enable_in AND ecr_in;

fe_short_command_generator_instance : fe_short_command_generator 
  port map(
    clk               => clk,
    rst               => rst,
--    rod_type_in       => rod_type_in,
    l1accept_in       => l1a_in_i,
    bcr_in            => bcr_in_i,
    ecr_in            => ecr_in_i,
    command_bits_out  => short_cmdbits_buf_ai,
    cmd_proc_busy_out => cmd_proc_busy_i
    );

--cmd_bits_combiner 
fast_cmdbits_buf_i <= short_cmdbits_buf_ai OR cal_cmdbits_buf_bi; 

-- Enable Command Streams if Fe Command Processor is On
long_cmd_stream_i(0) <= long_command_stream_in(0) AND enable_in;
long_cmd_stream_i(1) <= long_command_stream_in(1) AND enable_in;

-- Enable Pulse Streams if Fe Command Processor is On
long_cmd_pulse_i(0) <= long_command_pulse_in(0) AND enable_in;
long_cmd_pulse_i(1) <= long_command_pulse_in(1) AND enable_in;

-- Choose TIM or MDSP SP0 pulse source 
cmd_pulse_i <= (    fast_not_long_commands_in AND NOT cmd_proc_busy_i AND l1a_in_i) OR
               (NOT fast_not_long_commands_in AND long_cmd_pulse_i(0));
                          
--cmd_mask_logic0 : 
cmd_mask_logic0 :
  for j in 0 to 15 generate
    cmd_stream_masked0_i(j) <=  l1_trgrd_mask0_i(j) AND 
                               ((    fast_not_long_commands_in AND fast_cmdbits_buf_i(j)) OR
                                (NOT fast_not_long_commands_in AND cmd_stream_fanout0_i(j)));
  end generate;

--cmd_mask_logic1 : 
cmd_mask_logic1 :
  for j in 0 to 15 generate
    cmd_stream_masked1_i(j) <=  l1_trgrd_mask1_i(j) AND 
                               ((    fast_not_long_commands_in AND fast_cmdbits_buf_i(j)) OR
                                (NOT fast_not_long_commands_in AND cmd_stream_fanout1_i(j)));
  end generate;
  
--cmd_stream_masked1_i <= l1_trgrd_mask1_i AND cmd_stream_fanout1_i; -- COMMENTED 12/12/2013 DAV

--cmd_or_logic : 
command_i <= cmd_stream_masked1_i(7 downto 0) & cmd_stream_masked0_i(7 downto 0); 
--command_i <= cmd_stream_masked0_i OR cmd_stream_masked1_i; -- COMMENTED 12/12/2013 DAV

--pulse_mask_logic0 :
cmd_pulse_masked0_i <= l1_trgrd_mask0_i AND cmd_pulse_fanout0_i;

--pulse_mask_logic1 : 
cmd_pulse_masked1_i <= l1_trgrd_mask1_i AND cmd_pulse_fanout1_i;

--pulse_or_logic : 
pulse_i <= cmd_pulse_masked0_i OR cmd_pulse_masked1_i; 

-- FMT FE Command out (TIM OR SP0 OR SP1 for the source)
fmt_fe_cmd_out <=  short_cmdbits_buf_ai(0) OR 
                  (fmt_fe_cmd_mask_n_in AND long_cmd_stream_i(0)) OR  -- DSP SP0
                  (fmt_fe_cmd_mask_n_in AND long_cmd_stream_i(1));    -- DSP SP1

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
signal_fanout : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    cal_cmdbits_buf_bi(15 downto 0)   <= (others => '0');
    cmd_stream_fanout0_i(15 downto 0) <= (others => '0');
    cmd_stream_fanout1_i(15 downto 0) <= (others => '0');
    cmd_pulse_fanout0_i(15 downto 0)  <= (others => '0');
    cmd_pulse_fanout1_i(15 downto 0)  <= (others => '0');
  elsif (clk'event and clk = '1') then
    cal_cmdbits_buf_bi(15 downto 0)   <= (others => cal_cmdbits_buf_ai);
-- fanout the command bit stream coming in from the MDSP SP0
    cmd_stream_fanout0_i(15 downto 0) <= (others => long_cmd_stream_i(0));
-- fanout the command bit stream coming in from the MDSP SP1
    cmd_stream_fanout1_i(15 downto 0) <= (others => long_cmd_stream_i(1));
-- fanout the pulse bit stream coming in from the TIM or MDSP SP0
    cmd_pulse_fanout0_i(15 downto 0)  <= (others => cmd_pulse_i);
-- fanout the pulse bit stream coming in from the MDSP SP1
    cmd_pulse_fanout1_i(15 downto 0)  <= (others => long_cmd_pulse_i(1));
  end if; 
end process signal_fanout;
  

-- command and pulse bitstream output register
state_register : process (
  clk, 
  rst, 
  command_i, 
  pulse_i
  )
begin
  if (rst = '1') then
    command_stream_out <= (others => '0');
    pulse_stream_out   <= (others => '0');
  elsif (clk'event and clk = '1') then
    command_stream_out <= command_i(15 downto 0);  
    pulse_stream_out   <= pulse_i(15 downto 0);
  end if; 
end process state_register;

-- Register the FE Command Pulse before sending it out.
fe_pulse_output : process (
  clk,
  rst,  
  long_cmd_pulse_i, 
  cmd_pulse_i
  )
begin
  if (rst = '1') then
    fe_pulse_out <= '0';
  elsif (clk'event AND clk = '1') then
    fe_pulse_out <= cmd_pulse_i OR long_cmd_pulse_i(1);
  end if; 
end process fe_pulse_output;


-- l1trigger synchronized internal mask and fe_command_mask_updated_out pulse registers 
-- NOTE: must make sure that the timing of fe_pulse_out and fe_cmd_mask_updated_out 
--  are coincident, as the fe_pulse_out will be used to count the number of pending 
--  triggers, AND also be used to strobe itself and the fe_cmd_mask_updated_out
--  signal into a "new_mask_loaded" fifo. This is determines which trigger interrupt
--  is sent to the controller, differentiating mask load triggers from non-mask loading
--  triggers.
--  A new command mask is loaded if the "NewMaskReady" bit is set, and an L1 trigger is
--  detected, or if the "CnfgCalNewMaskLoad" and the "NewMaskReady" bits are set.
--  the signal fe_cmd_mask_updated_out is returned to the register block to clear the
--  the "NewMaskReady" bit.
mask_register : process (
  clk, 
  rst, 
  l1accept_in,
  new_mask_ready_in, 
  cnfg_cal_new_mask_load_in,
  corrective_mask_ready_in,
  fe_command_mask0_in, 
  fe_command_mask1_in
  )
begin
  if (rst = '1') then
    fe_cmd_mask_updated_out <= '0';
    l1_trgrd_mask0_i <= (others => '0'); -- loads the command masks with ALL fast control channels 
    l1_trgrd_mask1_i <= (others => '0'); -- MASKED for the reset case
  elsif (clk'event AND clk = '1') then
    if ((l1accept_in = '1' OR cnfg_cal_new_mask_load_in = '1') AND new_mask_ready_in = '1') then
      fe_cmd_mask_updated_out <= '1';
      l1_trgrd_mask0_i <= fe_command_mask0_in;
      l1_trgrd_mask1_i <= fe_command_mask1_in;
    else
      fe_cmd_mask_updated_out <= '0';
    end if;

    corrective_fe_pulse_out <= (l1accept_in AND corrective_mask_ready_in) OR long_cmd_pulse_i(1);
-- If a corrective mask and trigger are to be sent, the "CorrectiveMaskReady" bit must be set. 
  end if;
end process mask_register;

end rtl; -- code for front_end_command_processor 
