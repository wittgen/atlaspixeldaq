--------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--------------------------------------------------------------------------
--
-- Filename: fe_short_command_generator.vhd
-- Description:
--  In as compact a form as possible, implements the 
-- SCT Command bit streams to front end modules.
--  l1accept:  "110" 
--  bcr:       "1010010" 
--  ecr:       "1010100"
-- Pixel Command bit streams to front end modules.
--  l1accept:  "11101" 
--  bcr:       "101100001" 
--  ecr:       "101100010"
--  cal:       "101100100"
--  sync:      "101101000"
--
--------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: Mark L. Nagel
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    23 February 2000 - MLN first version : requirements/functionality finally
--                       understood and sufficiently documented to proceed 
--                       with coding.
--     6 January 2003 - JMJ Added Pixel Commands
--
--    20 August 2013 - SPC Modified code for use in IBL ROD
--
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------
entity fe_short_command_generator is
  port (
    clk               : in  std_logic;
    rst               : in  std_logic; -- asynchronous global reset
--    rod_type_in       : in  std_logic; -- ROD Type 0=>SCT 1 =>Pixel
    l1accept_in       : in  std_logic; -- l1trigger accept pulse, 1 clk wide. 
    bcr_in            : in  std_logic; -- bunch counter reset                 
    ecr_in            : in  std_logic; -- event counter reset 
    command_bits_out  : out std_logic_vector(15 downto 0);  -- 48 bits for Pixel
    cmd_proc_busy_out : out std_logic
    );
end fe_short_command_generator;

architecture rtl of fe_short_command_generator is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  reset, 
  idle,
  l1a, 
  bcr, 
  ecr
  );
signal cmd_state : states;

signal cmd_bit_count : unsigned(2 downto 0);

begin
--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
-- IBL Command bit streams to front end modules.
--  l1accept:  "11101" 
--  bcr:       "101100001" 
--  ecr:       "101100010"
--  cal:       "101100100"
--  sync:      "101101000"

fe_cmd_out : process (
  clk, 
  rst, 
--  rod_type_in,
  l1accept_in, 
  bcr_in, 
  ecr_in
  )
begin
  if (rst = '1') then
    cmd_proc_busy_out <= '0';   
    command_bits_out  <= (others => '0'); 
    cmd_bit_count     <= (others => '0');
    cmd_state         <= reset;
  elsif (clk'event and clk = '1') then
    case cmd_state is
      when reset =>
        cmd_proc_busy_out <= '0';   
        command_bits_out  <= (others => '0');                 
        cmd_bit_count     <= (others => '0');                
        cmd_state         <= idle; 

-- Choose which command bitstream will be sent
      when idle =>
        cmd_proc_busy_out <= '0';   
        command_bits_out  <= (others => '0');                 
        cmd_bit_count     <= (others => '0');                
        if (l1accept_in = '1') then 
          cmd_proc_busy_out <= '1';  
          command_bits_out  <= (others => '1'); -- preset the first command bit to 1               
          cmd_state         <= l1a; 
        elsif (bcr_in = '1')      then
          cmd_proc_busy_out <= '1';   
          command_bits_out  <= (others => '1');                 
          cmd_state         <= bcr;
        elsif (ecr_in = '1')      then
          cmd_proc_busy_out <= '1';   
          command_bits_out  <= (others => '1');                 
          cmd_state         <= ecr;
        end if; 

      when l1a =>
        cmd_bit_count <= cmd_bit_count + '1';  
          case cmd_bit_count is  -- l1accept: "11101"
--                      => command_bits_out  <= (others => '1'); -- preset in idle stage                
            when "000"  => command_bits_out  <= (others => '1');                 
            when "001"  => command_bits_out  <= (others => '1');                 
            when "010"  => command_bits_out  <= (others => '0');                 
            when "011"  => command_bits_out  <= (others => '1');                 
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
            when others => command_bits_out  <= (others => '0');
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
          end case;

      when bcr =>
        cmd_bit_count <= cmd_bit_count + '1';
          case cmd_bit_count is  -- bcr: "101100001" 
--                      => command_bits_out  <= (others => '1'); -- preset in idle stage                
            when "000"  => command_bits_out  <= (others => '0');                 
            when "001"  => command_bits_out  <= (others => '1');                 
            when "010"  => command_bits_out  <= (others => '1');                 
            when "011"  => command_bits_out  <= (others => '0');                 
            when "100"  => command_bits_out  <= (others => '0');                 
            when "101"  => command_bits_out  <= (others => '0');                 
            when "110"  => command_bits_out  <= (others => '0');                 
            when "111"  => command_bits_out  <= (others => '1');                 
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
            when others => command_bits_out  <= (others => '0');
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
          end case;

      when ecr =>
        cmd_bit_count <= cmd_bit_count + '1';
          case cmd_bit_count is  -- ecr: "101100010"
--                      => command_bits_out  <= (others => '1'); -- preset in idle stage                
            when "000"  => command_bits_out  <= (others => '0');                 
            when "001"  => command_bits_out  <= (others => '1');                 
            when "010"  => command_bits_out  <= (others => '1');                 
            when "011"  => command_bits_out  <= (others => '0');                 
            when "100"  => command_bits_out  <= (others => '0');                 
            when "101"  => command_bits_out  <= (others => '0');                 
            when "110"  => command_bits_out  <= (others => '1');                 
            when "111"  => command_bits_out  <= (others => '0');                 
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
            when others => command_bits_out  <= (others => '0');
                           cmd_proc_busy_out <= '0';   
                           cmd_state         <= idle; 
          end case;

      when others =>
        cmd_proc_busy_out <= '0';   
        command_bits_out  <= (others => '0');                 
        cmd_bit_count     <= (others => '0');                
        cmd_state         <= idle;
    end case;
  end if;
end process fe_cmd_out;

end rtl; -- end code for fe_short_command_generator
