--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
-- Author: Davide Falchieri

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_unsigned.all;

package rodMasterRegPack is

	-- definitions for the address base
	constant addr_width: integer := 8;
	
	-- ROD Master global registers
	constant MdspGpreg			: std_logic_vector(addr_width - 1 downto 0) := X"00";	-- RW/32
	constant DspSttsReg			: std_logic_vector(addr_width - 1 downto 0) := X"01";	-- R/32
	constant TestSttsReg		: std_logic_vector(addr_width - 1 downto 0) := X"02";	-- R/32
	constant RodRevCodeVerReg	: std_logic_vector(addr_width - 1 downto 0) := X"03";	-- R/16
	constant RrifCtrlReg		: std_logic_vector(addr_width - 1 downto 0) := X"04";	-- RW/32
	constant SprCtrlReg			: std_logic_vector(addr_width - 1 downto 0) := X"05";	-- RW/32
	constant RodModeReg			: std_logic_vector(addr_width - 1 downto 0) := X"06";	-- RW/20	
	constant FeCmdMaskSelReg	: std_logic_vector(addr_width - 1 downto 0) := X"07";	-- RW/3
	constant RrifSttsReg		: std_logic_vector(addr_width - 1 downto 0) := X"08";	-- R/32
	constant SpareSttsReg		: std_logic_vector(addr_width - 1 downto 0) := X"09";	-- R/32
	constant EmptyReg0			: std_logic_vector(addr_width - 1 downto 0) := X"0A";	-- R/32
	constant EmptyReg1			: std_logic_vector(addr_width - 1 downto 0) := X"0B";	-- R/32
	constant FeCmdMask0LsbReg  	: std_logic_vector(addr_width - 1 downto 0) := X"0C";	-- RW/32
	constant FeCmdMask0MsbReg	: std_logic_vector(addr_width - 1 downto 0) := X"0D";	-- RW/16
	constant FeCmdMask1LsbReg	: std_logic_vector(addr_width - 1 downto 0) := X"0E";	-- RW/32
	constant FeCmdMask1MsbReg	: std_logic_vector(addr_width - 1 downto 0) := X"0F";	-- RW/16
	constant CalStrbDlyReg		: std_logic_vector(addr_width - 1 downto 0) := X"10";	-- RW/8
	constant CalCommandReg		: std_logic_vector(addr_width - 1 downto 0) := X"11";	-- RW/27
	constant EcrCountIReg		: std_logic_vector(addr_width - 1 downto 0) := X"12";	-- RW/8
	constant EcrCountInReg		: std_logic_vector(addr_width - 1 downto 0) := X"13";	-- R/8
	constant MbFifoSttsReg		: std_logic_vector(addr_width - 1 downto 0) := X"14";	-- R/24
	constant DynMaskSttsLsbReg	: std_logic_vector(addr_width - 1 downto 0) := X"16";	-- R/10
	constant DynMaskSttsMsbReg	: std_logic_vector(addr_width - 1 downto 0) := X"17";	-- R/32
	constant InMemWordCntReg	: std_logic_vector(addr_width - 1 downto 0) := X"18";	-- RW/32
	constant DbgMemWordCntReg	: std_logic_vector(addr_width - 1 downto 0) := X"19";	-- RW/32
	constant CnfgRdbackCntReg	: std_logic_vector(addr_width - 1 downto 0) := X"1A";	-- RW/32
	constant DebugModeCnfgReg	: std_logic_vector(addr_width - 1 downto 0) := X"1C";	-- RW/32
	constant DebugModeSttsgReg	: std_logic_vector(addr_width - 1 downto 0) := X"1D";	-- R/32
	constant IntTimFifoDataReg	: std_logic_vector(addr_width - 1 downto 0) := X"20";	-- RW/8
	constant IntToSlvDspsReg	: std_logic_vector(addr_width - 1 downto 0) := X"24";	-- RW/4
	constant SlvDspIntReg		: std_logic_vector(addr_width - 1 downto 0) := X"25";	-- R/8
	constant TestIrqReg			: std_logic_vector(addr_width - 1 downto 0) := X"26";	-- RW/7
	constant FeOccResReg		: std_logic_vector(addr_width - 1 downto 0) := X"28";	-- RW/32
	constant FeOccNumL1Reg0		: std_logic_vector(addr_width - 1 downto 0) := X"2C";	-- RW/32
	constant FeOccNumL1Reg1		: std_logic_vector(addr_width - 1 downto 0) := X"2D";	-- RW/32
	constant FeOccNumL1Reg2		: std_logic_vector(addr_width - 1 downto 0) := X"2E";	-- RW/32
	constant FeOccNumL1Reg3		: std_logic_vector(addr_width - 1 downto 0) := X"2F";	-- RW/32
	constant IntScanReg0		: std_logic_vector(addr_width - 1 downto 0) := X"30";	-- RW/32
	constant IntScanReg1		: std_logic_vector(addr_width - 1 downto 0) := X"31";	-- RW/32
	constant IntScanReg2		: std_logic_vector(addr_width - 1 downto 0) := X"32";	-- RW/32
	constant IntScanReg3		: std_logic_vector(addr_width - 1 downto 0) := X"33";	-- RW/32
	constant CalEvntType0Reg	: std_logic_vector(addr_width - 1 downto 0) := X"34";	-- RW/10
	constant CalEvntType1Reg	: std_logic_vector(addr_width - 1 downto 0) := X"35";	-- RW/10
	constant ScanDoneSttsReg	: std_logic_vector(addr_width - 1 downto 0) := X"36";	-- R/32
	constant CalL1id0Reg		: std_logic_vector(addr_width - 1 downto 0) := X"38";	-- RW/24
	constant CalL1id1Reg		: std_logic_vector(addr_width - 1 downto 0) := X"39";	-- RW/24
	constant CalBcid0Reg		: std_logic_vector(addr_width - 1 downto 0) := X"3A";	-- RW/28
	constant L1IdBitsReg		: std_logic_vector(addr_width - 1 downto 0) := X"3B";	-- R/24
	constant DspLedsReg			: std_logic_vector(addr_width - 1 downto 0) := X"3F";	-- RW/8
	constant PrmGeoAddrReg		: std_logic_vector(addr_width - 1 downto 0) := X"40";	-- R/14
	constant CalPulseCount0Reg : std_logic_vector(addr_width - 1 downto 0) := X"41"; --R/24
	constant CalPulseCount1Reg : std_logic_vector(addr_width - 1 downto 0) := X"42"; --R/24
	
end rodMasterRegPack;

package body rodMasterRegPack is
 
end rodMasterRegPack;