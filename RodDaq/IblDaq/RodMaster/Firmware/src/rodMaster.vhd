-------------------------------------------------------------------------------
-- rodMaster.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

use std.textio.all;
use work.txt_util.all;
-- get rod bus addresses
use work.rodBusPackage.all;

use work.rodMasterRegPack.all;
use work.datePack.all; 
use work.gitIdPack.all;

entity rodMaster is
  generic (simulation : boolean := false);
  port (
    fpga_0_RS232_RX_pin : in std_logic;
    fpga_0_RS232_TX_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : out std_logic_vector(7 downto 0);
    fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : in std_logic_vector(7 downto 0);
    fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : in std_logic;
    fpga_0_Hard_Ethernet_MAC_MDC_0_pin : out std_logic;
    fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : inout std_logic;
    -- fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin : out std_logic; -- not used on board
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ : inout std_logic_vector(63 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS : inout std_logic_vector(7 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N : inout std_logic_vector(7 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A : out std_logic_vector(13 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA : out std_logic_vector(2 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N : out std_logic;
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N : out std_logic;
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N : out std_logic;
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N : out std_logic_vector(1 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE : out std_logic_vector(1 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT : out std_logic;
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM : out std_logic_vector(7 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK : out std_logic_vector(1 downto 0);
    fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N : out std_logic_vector(1 downto 0);
	 ddr2_scl : INOUT std_logic;
    ddr2_sda : INOUT std_logic;
    fpga_0_LEDS_GPIO_IO_O_pin : out std_logic_vector(7 downto 0);
    -- serial port
    xc_a: out std_logic_vector(7 downto 0);
    xc_b: out std_logic_vector(7 downto 0);
    --
    -----------------------------------------------------------------------------------------
    -----------------------------------------------------------------------------------------
    master_reset: in std_logic;
    reset_s6a:		out std_logic;
    reset_s6b:		out std_logic;							
    reset_processor_core_SEL_s6a :		out std_logic;	
	 reset_processor_core_SEL_s6b :		out std_logic;
    -----------------------------------------------------------------------------------------
    -- MDSP interface
    -- controller dsp External Memory Interface -- 61
    controller_bus_data_inout : inout std_logic_vector(31 downto 0); -- dsp data lines
    controller_bus_adr_in     : in    std_logic_vector(19 downto 0); -- dsp address lines 
    controller_bus_be_n_in    : in    std_logic_vector( 3 downto 0); -- dsp byte eanbles 
    controller_bus_ce_n_in    : in    std_logic; -- dsp chip enable
    controller_bus_oe_n_in    : in    std_logic; -- dsp output enable
    controller_bus_re_n_in    : in    std_logic; -- dsp read enable
    controller_bus_rnw_in     : in    std_logic; -- dsp write enable
    controller_bus_ack_out    : out   std_logic; -- dsp acknowledge input (ARDY)

    --	 controller dsp Timer Counter Unit Inteface -- 4
    controller_tinp_out : out   std_logic_vector(1 downto 0); -- dsp timer1,0 input
    controller_tout_in  : in    std_logic_vector(1 downto 0); -- dsp timer1,0 output 

    -- controller dsp Syncronous Serial Port Interface  -- 12
    controller_dx_in    : in    std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit serial data 
    controller_clkx_out : out   std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit clock 
    controller_fsx_out  : out   std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit frame syncroniztion
    controller_dr_out   : out   std_logic_vector(1 downto 0); -- dsp sspi1,0 receive serial data (Used for Reset Commands)
    controller_clkr_in  : in    std_logic_vector(1 downto 0); -- dsp sspi1,0 receive clock        
    controller_fsr_in   : in    std_logic_vector(1 downto 0); -- dsp sspi1,0 receive frame syncroniztion

    -- controller dsp Interrupt Inputs -- 10
    controller_ext_int_n_out : out   std_logic_vector(4 downto 0); -- mdsp external interrupt
    ext_iacknum_in           : in    std_logic_vector(4 downto 0); -- mdsp external interrupt ack sigs  

    -- master dsp Interrupt Input  -- 1
    mdsp_int_n_in       : in    std_logic; -- /HINT for the Master DSP
    -----------------------------------------------------------------------------------------
    -- ROD bus
    rod_bus_data		 	 : inout std_logic_vector(15 downto 0); 
    rod_bus_addr       	 : out   std_logic_vector(15 downto 0);  -- was 17
    rod_bus_ce1_s6			 : out   std_logic; -- external component/module enables
    rod_bus_ce2_s6			 : out   std_logic; -- external component/module enables
    rod_bus_rnw       	 : out   std_logic; 
    form_hwob		     	 : out   std_logic; 							 -- Formatter Half Word Order
    form_ds	         	 : out   std_logic; 							 -- Formatter Data Strobe
    
    tim_clock_ok			 : in		std_logic;
	 boc_clock_ok			 : in		std_logic;
    
    bocbus_strb_out    : out   std_logic;
    bocbus_rodoe_out   : out   std_logic;
    bocbus_write_out   : out   std_logic;
    bocbus_busy_in     : in    std_logic;
    ------------------------------------------------------
    flash_si : out std_logic;
    flash_cs_n : out std_logic;
    flash_sck : out std_logic;
    flash_so : in std_logic;
    flash_reset_n : out std_logic;
    flash_wp_n : out std_logic;
    ------------------------------------------------------
	 -- prm control in and out mapping: see PRM code
	 
    prm_ctrl_out : OUT std_logic_vector(19 downto 0); 
    prm_data_io : INOUT std_logic_vector(31 downto 0); 
    prm_strobe : IN std_logic_vector(1 downto 0); 
    ctrl_from_prm : IN std_logic_vector(14 downto 0);
	 reset_commands_in: IN std_logic_vector(1 downto 0);
	 reset_commands_out: OUT std_logic_vector(1 downto 0);
    ------------------------------------------------
    formatter_writes_trailer_in : in    std_logic_vector(11 downto 0); -- signal to readout formatter/fifoFPGA ic trailer flags
    show_trailer_flagsA	        : out   std_logic; 
	 show_trailer_flagsB	        : out   std_logic; 
    header_trailer_limit_inA    : in    std_logic; 
	 header_trailer_limit_inB	  : in	 std_logic;

-- TIM interface -- 8
    serial_eventtype_n_in       : in    std_logic; --
    serial_l1id_n_in            : in    std_logic; --
    cal_strobe_n_in             : in    std_logic; --
    l1_accept_n_in              : in    std_logic; --
    bunch_counter_reset_n_in    : in    std_logic; --
    event_counter_reset_n_in    : in    std_logic; --
    ttc6_n_in                   : in    std_logic; --
    ttc7_n_in                   : in    std_logic; --

-- formatters mode bits interface "mb" -- 22
	 form_mb_fifo_ffA					: in	  std_logic;
	 form_mb_fifo_ffB					: in	  std_logic;
    form_mb_fifo_efA             : in    std_logic;
	 form_mb_fifo_efB             : in    std_logic;
    formatter_mb_data_out        : out   std_logic_vector(11 downto 0); -- mode bit information to be loaded to the formatter mode bit fifos
    form_mode_bits_weA           : out   std_logic; 
	 form_mode_bits_weB				: out   std_logic;
    form_mode_bits_rst_nA        : out   std_logic;
	 form_mode_bits_rst_nB        : out   std_logic;
    fe_cmdpulse_form_outA        : out   std_logic; 
	 fe_cmdpulse_form_outB			: out	  std_logic;
                                                                    
-- formatter rod busy ios -- 3
    rod_busy_n_in                : in    std_logic_vector(1 downto 0); 
    rod_busy_n_out               : out   std_logic;
                                                    
-- event fragment builder event dynamic mask interface "edm" -- 30
    efb_edm_fifo_empty_error_in_A : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos full flag
    efb_edm_fff_n_in_A            : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos full flag
    efb_edm_fef_n_in_A            : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos empty flag
    
    efb_edm_fifo_empty_error_in_B : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos full flag
    efb_edm_fff_n_in_B            : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos full flag
    efb_edm_fef_n_in_B            : in    std_logic_vector( 1 downto 0); -- event dynamic mask fifos empty flag
    
    efb_edm_data_out            : out   std_logic_vector(15 downto 0); -- event dynamic mask information to be loaded to the formatter dynamic mask fifos 
    efb_edm_we_n_out            : out   std_logic; -- event dynamic mask fifo write enable 
    efb_edm_fifo_rst_n_out      : out   std_logic; -- event dynamic mask fifo write enable 
    
    ------------------------------------------------------
    uartFromS6a : in std_logic;
    uartToS6a : out std_logic;
    uartFromS6b : in std_logic;
    uartToS6b : out std_logic;
    ------------------------------------------------------
    clk40_p: in std_logic;
    clk40_n: in std_logic;
    clk100_p: in std_logic;
    clk100_n: in std_logic;
	 tim_enable: out std_logic
    );
end rodMaster;

architecture STRUCTURE of rodMaster is

attribute box_type: string;
  
--  component icon
--    PORT (
--      CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
--  end component;
--  attribute box_type of icon: component is "user_black_box";
--
--  component ila
--    PORT (
--      CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--      CLK : IN STD_LOGIC;
--      TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--      TRIG1 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--      TRIG2 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--		TRIG3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--		TRIG4 : IN STD_LOGIC_VECTOR(35 DOWNTO 0)
--		);
--  end component;
--  attribute box_type of ila: component is "user_black_box";
  
  component v5gmac125 is
    PORT(
      fpga_0_RS232_RX_pin : IN std_logic;
      fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin : IN std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin : IN std_logic_vector(7 downto 0);
      fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin : IN std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin : IN std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin : IN std_logic;
      fpga_0_clk_1_sys_clk_pin : IN std_logic;
      fpga_0_rst_1_sys_rst_pin : IN std_logic;
      xps_epc_0_PRH_Rdy_pin : IN std_logic;
      xps_epc_0_PRH_Data_I_pin : IN std_logic_vector(0 to 31);
      xps_ll_fifo_0_tx_llink_dest_rdy_n_pin : IN std_logic;
      extirq_0_extIrqReq_pin : IN std_logic;
      xps_epc_0_PRH_Clk_pin : IN std_logic;
      flash_so : IN std_logic;
      hpiport_0_PRM_strobe_pin : IN std_logic;
      hpiport_0_PRM_command_pin : IN std_logic_vector(3 downto 0);
      hpiport_0_PRM_data_bus_in_pin : IN std_logic_vector(31 downto 0);
      mcbsp_rod_0_Clkx_pin : IN std_logic;    
      fpga_0_Hard_Ethernet_MAC_MDIO_0_pin : INOUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ_pin : INOUT std_logic_vector(63 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_pin : INOUT std_logic_vector(7 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N_pin : INOUT std_logic_vector(7 downto 0);
      xps_iic_0_Sda : INOUT std_logic;
      xps_iic_0_Scl : INOUT std_logic;      
      fpga_0_RS232_TX_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin : OUT std_logic_vector(7 downto 0);
      fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_MDC_0_pin : OUT std_logic;
      fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin : OUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A_pin : OUT std_logic_vector(13 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA_pin : OUT std_logic_vector(2 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N_pin : OUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N_pin : OUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N_pin : OUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N_pin : OUT std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT_pin : OUT std_logic;
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE_pin : OUT std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM_pin : OUT std_logic_vector(7 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_pin : OUT std_logic_vector(1 downto 0);
      fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N_pin : OUT std_logic_vector(1 downto 0);
      cpurst : OUT std_logic;
      xps_epc_0_PRH_CS_n_pin : OUT std_logic;
      xps_epc_0_PRH_Addr_pin : OUT std_logic_vector(0 to 24);
      xps_epc_0_PRH_Data_O_pin : OUT std_logic_vector(0 to 31);
      cpuclk : OUT std_logic;
      xps_ll_fifo_0_tx_llink_src_rdy_n_pin : OUT std_logic;
      xps_ll_fifo_0_tx_llink_din_pin : OUT std_logic_vector(31 downto 0);
      xps_ll_fifo_0_tx_llink_eop_n_pin : OUT std_logic;
      xps_ll_fifo_0_tx_llink_sop_n_pin : OUT std_logic;
      xps_ll_fifo_0_llink_rst_pin : OUT std_logic;
      extirq_0_extIrqAck_pin : OUT std_logic;
      flash_si : OUT std_logic;
      flash_ss : OUT std_logic_vector(0 to 0);
      flash_sck : OUT std_logic;
		-- spi for prm 
		xps_spi_1_SCK_O_pin : OUT std_logic;
		xps_spi_1_MOSI_O_pin : OUT std_logic;
		xps_spi_1_SS_O_pin : OUT std_logic_vector(0 to 0);
		xps_spi_1_MISO_I_pin : IN std_logic;    
		--
		-- gpio pins for xilinx virtual cable jtag
		xps_gpio_xvc_GPIO_IO_I_pin : IN std_logic_vector(0 to 3);
		xps_gpio_xvc_GPIO_IO_O_pin : OUT std_logic_vector(0 to 3);
		-- 
      hpiport_0_PRM_data_bus_out_pin : OUT std_logic_vector(31 downto 0);
      hpiport_0_PRM_busy_pin : OUT std_logic;
      hpiport_0_PRM_done_pin : OUT std_logic;
      hpiport_0_PRM_data_valid_pin : OUT std_logic;
      mcbsp_rod_0_Dx_O_pin : OUT std_logic;
      xps_epc_0_PRH_Wr_n_pin : OUT std_logic;
      xps_epc_0_PRH_RNW_pin : OUT std_logic
      );
  end component;
  attribute BOX_TYPE of v5gmac125 : component is "user_black_box";


	signal spi_1_ss : std_logic_vector(0 to 0);
	signal spi1_sck : std_logic;
	signal spi1_mosi : std_logic;

	signal xps_gpio_xvc_GPIO_IO_I_pin : std_logic_vector(0 to 3);
	signal xps_gpio_xvc_GPIO_IO_O_pin : std_logic_vector(0 to 3);


  component sportFifo
    PORT (
      rst : IN STD_LOGIC;
      wr_clk : IN STD_LOGIC;
      rd_clk : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
      );
  end component;
  attribute box_type of sportFifo: component is "user_black_box";

  component address_decoder_busbridge
    port (
      rst_n_in           : in    std_logic; -- negative logic asynchronous global reset
      clk_in             : in    std_logic; -- rodclk40 input
      dspbus_ea_in       : in    std_logic_vector(21 downto 2); --
      dspbus_ed_in       : in    std_logic_vector(31 downto 0); --
      dspbus_ed_out      : out   std_logic_vector(31 downto 0); --
      dspbus_en_n_in     : in    std_logic; -- MDSP CE_N
      dspbus_be_n_in     : in    std_logic_vector( 3 downto 0); -- MDSP Bytes Enables
      dspbus_rnw_in      : in    std_logic; -- Write Strobe from the MDSP
      dspbus_re_n_in     : in    std_logic; -- Read Enable from the MDSP
      dspbus_oe_n_in     : in    std_logic; -- Output Enable from the MDSP
      dspbus_ack_out     : out   std_logic; --
      rodbus_a_out       : out   std_logic_vector(19 downto 0); --
      rodbus_d_in        : in    std_logic_vector(31 downto 0); --
      rodbus_d_out       : out   std_logic_vector(31 downto 0); --
      rodbus_en_n_out    : out   std_logic_vector(10 downto 0); -- strobe/enable from the addr
      rodbus_en_n_itr    : out   std_logic_vector( 6 downto 0); -- strobe/enable from the addr
      rodbus_rnw_out     : out   std_logic; --
      formA_ack_n_in     : in    std_logic; --
      formB_ack_n_in     : in    std_logic; --
      efb_ack_in         : in    std_logic; --
      router_ack_in      : in    std_logic; --
      bocbus_busy_in     : in    std_logic;
      bocbus_strb_out    : out   std_logic;
      bocbus_rodoe_out   : out   std_logic;
      bocbus_write_out   : out   std_logic;
      regbus_a_out       : out   std_logic_vector(11 downto 0); -- rrif global registers bus
      regbus_d_in        : in    std_logic_vector(31 downto 0); --
      regbus_d_out       : out   std_logic_vector(31 downto 0); --
      regbus_rnw_out     : out   std_logic; --
      regbus_ack_in      : in    std_logic; --
      fcmbus_d_in        : in    std_logic_vector(31 downto 0);
      focbus_a_out       : out   std_logic_vector(11 downto 0); -- front end occupancy counter
      focbus_d_in        : in    std_logic_vector(31 downto 0); --
      focbus_rnw_out     : out   std_logic; --
      focbus_ack_in      : in    std_logic; --
      fmebus_a_out       : out   std_logic_vector( 7 downto 0); -- Formatter ModeBit Encoder
      fmebus_d_in        : in    std_logic_vector(15 downto 0); --
      fmebus_d_out       : out   std_logic_vector(15 downto 0); --
      fmebus_rnw_out     : out   std_logic; --
      fmebus_ack_in      : in    std_logic; --
      edebus_a_out       : out   std_logic_vector( 7 downto 0); -- efb Dynamic Mask Encoder
      edebus_d_in        : in    std_logic_vector(15 downto 0); --
      edebus_d_out       : out   std_logic_vector(15 downto 0); -- 
      edebus_rnw_out     : out   std_logic; --
      edebus_ack_in      : in    std_logic; --
      dfibus_a_out       : out   std_logic_vector( 6 downto 0); -- Debugmem Fifo Interface bus
      dfibus_rnw_out     : out   std_logic;
      dfibus_ack_in      : in    std_logic;
      fmt_hwob_out       : out   std_logic;
      fmt_ds_out         : out   std_logic;
      efb_hwob_out       : out   std_logic;
      extbus_ack_out     : out   std_logic; --
      intbus_ack_out     : out   std_logic
      );
  end component;

  component register_block
    port (
      clk_in                    : in  std_logic; -- clk40 input
      rst_n_in                  : in  std_logic; -- global reset
      dspLeds							: out std_logic_vector(7 downto 0);
      regbus_en_n_in            : in  std_logic;
      regbus_rnw_in             : in  std_logic;
      regbus_a_in               : in  std_logic_vector(11 downto 0);
      regbus_d_in               : in  std_logic_vector(31 downto 0);
      regbus_d_out              : out std_logic_vector(31 downto 0);
      regbus_ack_out            : out std_logic;   
      fcmbus_en_n_in            : in  std_logic;
      fcmbus_d_out              : out std_logic_vector(31 downto 0);
      rrif_ctrl_reg_out         : out std_logic_vector(31 downto 0);
      spare_ctrl_reg_out        : out std_logic_vector(31 downto 0);
      rrif_stts_reg_in          : in  std_logic_vector(31 downto 0);
      spare_stts_reg_in         : in  std_logic_vector(31 downto 0);
      dsp_stts_reg_in           : in  std_logic_vector(31 downto 0);
      test_stts_reg_in          : in  std_logic_vector(31 downto 0);
      fecmd_link_mask0_out      : out std_logic_vector(47 downto 0);
      fecmd_link_mask1_out      : out std_logic_vector(47 downto 0);
      calstrobe_delay_reg_out   : out std_logic_vector( 7 downto 0);
      cal_command_reg_out       : out std_logic_vector(26 downto 0);
      modebits_fifo_stts_reg_in : in  std_logic_vector(23 downto 0);
      dynmmask_fifo_stts_reg_in : in  std_logic_vector(41 downto 0);
      inmem_wordcount_reg_out   : out std_logic_vector(31 downto 0);
      dbgmem_wordcount_reg_out  : out std_logic_vector(31 downto 0);
      cnfg_rdback_count_reg_out : out std_logic_vector(31 downto 0);
      ide_debugmem_stts_reg_in  : in  std_logic_vector( 7 downto 0);
      ide_debugmem_cnfg_reg_out : out std_logic_vector(17 downto 0);
      debug_fifo_stts_in        : in  std_logic_vector( 7 downto 0);
      rol_test_block_reg_out    : out std_logic_vector( 3 downto 0);  
      rol_test_block_count_out  : out std_logic_vector( 5 downto 0);  
		rol_test_out				  : out std_logic;
      int_tim_fifo_outdata_out  : out std_logic_vector( 7 downto 0);
      int_tim_fifo_re_in        : in  std_logic;
      int_tim_fifo_rt_in        : in  std_logic;
      slave_dsp_ints_out        : out std_logic_vector( 3 downto 0);
      slave_dsp_ints_in         : in  std_logic_vector( 7 downto 0);
      sdsp_inttohost_signal_out : out std_logic;
      feocc_reset_reg_out       : out std_logic_vector( 31 downto 0);
      feocc_num_l1_reg_out      : out std_logic_vector(103 downto 0);
      cal_event_type0_out       : out std_logic_vector( 9 downto 0);
      cal_event_type1_out       : out std_logic_vector( 9 downto 0);
      cal_l1id0_in              : in  std_logic_vector(23 downto 0);
      cal_l1id1_in              : in  std_logic_vector(23 downto 0);
		calpulse_count0_in		  : in  std_logic_vector(23 downto 0);
		calpulse_count1_in		  : in  std_logic_vector(23 downto 0);
      static_l1id0_out          : out std_logic_vector(23 downto 0);
      static_l1id1_out          : out std_logic_vector(23 downto 0);
      cal_bcid0_in              : in  std_logic_vector(11 downto 0);
      cal_bcid1_in              : in  std_logic_vector(11 downto 0);
      static_bcid0_out          : out std_logic_vector(11 downto 0);
      static_bcid1_out          : out std_logic_vector(11 downto 0);
      rod_mode_out              : out std_logic_vector( 3 downto 0);
      test_mux_out              : out std_logic_vector( 3 downto 0);
      fe_cmd_mask_sel_out       : out std_logic_vector( 2 downto 0);
      fe_cmd_mask_updated_in    : in  std_logic;
      corrective_mask_sent_in   : in  std_logic;
      send_int_test_out         : out std_logic;
      test_irq_out              : out std_logic_vector( 2 downto 0);
      ecr_count_in              : in  std_logic_vector( 7 downto 0);
      ecr_count_out             : out std_logic_vector( 7 downto 0);
      nevent_out                : out std_logic_vector(15 downto 0);  -- NEvent
      precal_delay_out          : out std_logic_vector( 7 downto 0);  -- Pre CAL Delay
      trig_delay0_out           : out std_logic_vector(15 downto 0);  -- TrigDelay1
      trig_delay1_out           : out std_logic_vector(15 downto 0);  -- TrigDelay2
      interval_out              : out std_logic_vector(31 downto 0);  -- Interval
      trig_mode_out             : out std_logic_vector(31 downto 0);  -- TrigMode
      mask_en_in                : in  std_logic_vector( 2 downto 0);  --
      new_mask_in               : in  std_logic;
      load_event_type_out       : out std_logic;
      int_nevent_done_in        : in  std_logic;
		prm_ga							: in std_logic_vector(4 downto 0);
      l1id_bits_in              : in  std_logic_vector(23 downto 0)
      );
  end component;

  component debugmem_fifo_interface
    port (
      rst               : in  std_logic; -- negative logic asynchronous global rese
      clk               : in  std_logic; -- clk40 input
      control_in        : in  std_logic; -- 1: enable, 0: idle
      address_in        : in  std_logic_vector( 6 downto 0); --
      rnw_in            : in  std_logic; --
      inmem_en_n_in     : in  std_logic; --
      eventmem_en_n_in  : in  std_logic; --
      inmem_ctrl_out    : out std_logic_vector(15 downto 0); --
      eventmem_ctrl_out : out std_logic_vector(10 downto 0); --
      ack_out           : out std_logic  --
      );
  end component; 

  component testbench_interface
    port (
      rst                          : in  std_logic; -- global reset
      clk                          : in  std_logic; -- clk40 input
      control_in                   : in  std_logic_vector( 1 downto 0); --
--  control_in:  2=>run/stop, 1=>enable, 0=>clear
      debugmode_cnfg_register_in   : in  std_logic_vector(17 downto 0); --
      debugmode_stts_register_out  : out std_logic_vector( 7 downto 0); -- 
      inmem_wordcount_register_in  : in  std_logic_vector(31 downto 0); -- 
      dbgmem_wordcount_register_in : in  std_logic_vector(31 downto 0); -- 
      inmem_ctrl_out               : out std_logic_vector(11 downto 0); --
      int_tim_fifo_re_out          : out std_logic; --
      int_tim_fifo_rt_out          : out std_logic; --
      l1_trigger_in                : in  std_logic;
      pause_trigger_rt             : in  std_logic
      );
  end component;
  
  component event_id_trigger_processor is
	port (
		clk            			  : in  	std_logic; --                           
		rst            			  : in  	std_logic; --                           
		rrif_control_register_in  : in  std_logic_vector(22 downto  8); --       
		tim_signals_in            : in  std_logic_vector( 5 downto  4); --        
		cal_serial0_in 			  : in  	std_logic; --                           
		cal_serial1_in 			  : in  	std_logic; --                           
		l1_accept0_out 		  	  : out 	std_logic; --        
		l1_accept1_out 		  	  : out 	std_logic; --                           
		l1id0_reg_in   		  	  : in  	std_logic_vector(23 downto 0); --       
		l1id1_reg_in   		  	  : in  	std_logic_vector(23 downto 0); --       
		l1id0_out      			  : out 	std_logic_vector(23 downto 0); --       
		l1id1_out      			  : out 	std_logic_vector(23 downto 0); --   
		calpulse_count0_out		  : out  std_logic_vector(23 downto 0);
		calpulse_count1_out		  : out  std_logic_vector(23 downto 0);
		bcid0_reg_in   			  : in  	std_logic_vector(11 downto 0); --       
		bcid1_reg_in   			  : in  	std_logic_vector(11 downto 0); --       
		bcid0_out      			  : out 	std_logic_vector(11 downto 0); --       
		bcid1_out      			  : out 	std_logic_vector(11 downto 0); --       
		l1id_bits_out        	  : out   std_logic_vector(23 downto 0); --      
		bcid_bits_out         	  : out   std_logic_vector(11 downto 0); --      
		ttype_bits_out            : out   std_logic_vector( 9 downto 0); --      
		new_l1id_valid_out  	     : out   std_logic; --                          
		new_ttype_valid_out       : out   std_logic  --                          
		);
end component;

  component efb_headerdynamicmask_encoder
    port(
      clk	                   : in  std_logic;  -- clk40 input
      rst                      : in  std_logic;  -- negative logic global reset
      control_in               : in  std_logic;  -- 1:enable transfer, 0:flush all
      rnw_in                   : in  std_logic;
      ce_n_in                  : in  std_logic;
      ack_out                  : out std_logic;
      apply_default_mask_in    : in  std_logic;  -- interrupt to the MDSP to 
-- indicate that a DEFAULT trigger has occurred and that the default dynamic 
-- mask and default readoutmodebits will be written to the efb and formatter 
-- (respectively) on this trigger.
      apply_corrective_mask_in : in  std_logic;  -- interrupt to the master dsp
-- to indicate that a CORRECTIVE trigger has occurred and that the corrective 
-- dynamic mask and corrective readoutmodebits will be written to the efb and
-- formatter (respectively) on this AND ONLY this trigger.  This signal is used
-- to clear the new_mask_ready_in set/reset flip-flop in the 
-- front_end_command_processor control_register.
      record_headerdynamicmask_in    : in  std_logic;  --
      read_trig_indictor_que_out     : out std_logic;  --
      l1id_bits_in                   : in  std_logic_vector(23 downto 0);  --
      bcid_bits_in                   : in  std_logic_vector(11 downto 0);  --
      ttype_bits_in                  : in  std_logic_vector( 9 downto 0);  --
      new_l1id_valid_in              : in  std_logic;                      --
      new_ttype_valid_in             : in  std_logic;                      --
      int_lut_bdata_in               : in  std_logic_vector(15 downto 0);  --
      int_lut_bdata_out              : out std_logic_vector(15 downto 0);  --
      int_lut_adr_in                 : in  unsigned(7 downto 0);           --
      event_headermask_fifo_ef_n_in  : in  std_logic;                      --
      event_headermask_fifo_ff_n_in  : in  std_logic;                      --
      event_headermask_out           : out std_logic_vector(15 downto 0);  --
      event_lbid_count_out           : out std_logic_vector( 7 downto 0);
      event_trgt_count_out           : out std_logic_vector( 7 downto 0);
      event_mask_count_out           : out std_logic_vector( 7 downto 0);  --
      event_header_count_out         : out std_logic_vector( 7 downto 0);  --
      event_headermask_fifo_we_n_out : out std_logic;                      --
      int_fifo_ef_out                : out std_logic_vector( 4 downto 0);  -- 
      int_fifo_ff_out                : out std_logic_vector( 4 downto 0);  -- 
      ecrid_in                       : in  unsigned( 7 downto 0);
      int_cal_mode_en_in             : in  std_logic; -- 1:CalMode 0:Normal
      cal_l1a0_in                    : in  std_logic;
      cal_l1a1_in                    : in  std_logic;
      cal_tt0_in                     : in  std_logic_vector( 9 downto 0);  --
      cal_tt1_in                     : in  std_logic_vector( 9 downto 0);  --
      cal_l1id0_in                   : in  std_logic_vector(23 downto 0);
      cal_l1id1_in                   : in  std_logic_vector(23 downto 0);
      cal_bcid0_in                   : in  std_logic_vector(11 downto 0);
      cal_bcid1_in                   : in  std_logic_vector(11 downto 0);
      pause_triggers_out             : out std_logic;
      event_type_in                  : in  std_logic_vector( 2 downto 0);
      load_event_type_in             : in  std_logic;
      mask_dxi_in                    : in  std_logic;
      nevent_in                      : in  std_logic_vector(15 downto 0);
      boc_ok_in                      : in  std_logic;
      tim_ok_in                      : in  std_logic;
      rol_test_in                    : in  std_logic;
      rol_test_count_in              : in  std_logic_vector(5 downto 0);
      rod_tt_wr_out                  : out std_logic;
      event_l1id_out                 : out std_logic_vector(23 downto 0)
      );
  end component;

	signal rod_tt_wr_i : std_logic;

  component formatter_readoutmodebits_encoder
    port(
      clk                      : in  std_logic;  -- clk40 input
      rst                      : in  std_logic;  -- negative logic global reset
      control_in               : in  std_logic;  -- 0:flush all, 1:enable
      int_lut_adr_in           : in  unsigned(7 downto 0);
      int_lut_bdata_in         : in  std_logic_vector(15 downto 0);  --
      int_lut_bdata_out        : out std_logic_vector(15 downto 0);  --
      ce_n_in                  : in  std_logic;
      rnw_in                   : in  std_logic;
      ack_out                  : out std_logic;
-- interrupt to the MDSP to indicate that a DEFAULT trigger has occurred
      apply_default_mode_in    : in  std_logic;
-- interrupt to the MDSP to indicate that a CORRECTIVE trigger has occurred
      apply_corrective_mode_in : in  std_logic;
      modebits_fifo_ff_n_in    : in  std_logic_vector( 1 downto 0);  --
      modebits_out             : out std_logic_vector(11 downto 0);  --
      modebits_fifo_we_n_out   : out std_logic_vector( 7 downto 0);  --
      int_fifo_ef_out          : out std_logic_vector( 1 downto 0);  --
      int_fifo_ff_out          : out std_logic_vector( 1 downto 0);
      mb0_fifo_occ_count       : out std_logic_vector( 9 downto 0);
      mb1_fifo_occ_count       : out std_logic_vector( 9 downto 0);
      modebits_sent_out        : out std_logic;
      fe_cmd_mask_sel_in       : in  std_logic_vector( 2 downto 0)
      );
  end component;

  component fe_command_processor
  port(
    clk                       : in  std_logic;
    rst                       : in  std_logic;
	 rrif_control_register_in  : in  std_logic_vector(23 downto  0);
    tim_signals_in            : in  std_logic_vector( 3 downto  0);
    rrif_status_register_out  : out std_logic_vector(29 downto 22);
    spare_status_register_out : out std_logic_vector(18 downto  8);
    corrective_fe_pulse_out   : out std_logic;
    calibrate_command_in      : in  std_logic_vector(26 downto  0);
    cal_trig_delay_in         : in  std_logic_vector( 7 downto  0);
    fe_command_mask0_in       : in  std_logic_vector(15 downto  0);
    fe_command_mask1_in       : in  std_logic_vector(15 downto  0); 
    long_command_stream_in    : in  std_logic_vector( 1 downto  0); 
    long_command_pulse_in     : in  std_logic_vector( 1 downto  0); 
    command_stream_out        : out std_logic_vector(15 downto  0); 
    pulse_stream_out          : out std_logic_vector(15 downto  0); 
    fe_cmd_mask_updated_out   : out std_logic;
    fmt_fe_cmd_out            : out std_logic;
    fe_cmdpulse_AB_out        : out std_logic_vector( 1 downto  0);
    controller_int_out        : out std_logic_vector( 1 downto  0); 
    command_cycle_done_out    : out std_logic;
    store_current_trigger_out : out std_logic;
    current_trigger_out       : out std_logic_vector( 1 downto  0);
    dynamic_mask_sent_in      : in  std_logic;
    modebits_sent_in          : in  std_logic;
    efb_ready_in              : in  std_logic;
    modebits_error_out        : out std_logic
  );
  end component; 

signal store_trig2ti_que_i : std_logic;
signal trigger_descriptor_i : std_logic_vector( 1 downto  0);
signal read_trigger_que_i : std_logic;
signal modebits_sent_i : std_logic;


  component front_end_occupancy_counters
    port(
      clk                           : in    std_logic; -- clk40 input
      rst                           : in    std_logic; -- asyncronous global reset
      trailerflags_readout_en_in    : in    std_logic; -- enable trailer flags readout/counter
      trailerflags_in               : in    std_logic_vector( 3 downto 0); --
      clear_fe_occupancy_counter_in : in    std_logic_vector( 31 downto 0); --
      num_l1_value_in               : in    std_logic_vector(103 downto 0); --  value 0-15 for Links 0-31 Pixel only
      fe_l1_pulse_in                : in    std_logic_vector( 31 downto 0); --
      fe_occupancy_gl_zero_out      : out   std_logic; --
--      group_zero_out                : out   std_logic_vector(  3 downto 0); --
--      group_en_out                  : out   std_logic_vector(  3 downto 0); --
      show_trailer_flags_out        : out   std_logic_vector(  7 downto 0); --
      focbus_a_in                   : in    std_logic_vector( 11 downto 0); -- front end occupancy counters bus
      focbus_d_in                   : in    std_logic_vector( 31 downto 0); --
      focbus_d_out                  : out   std_logic_vector( 31 downto 0); --
      focbus_rnw_in                 : in    std_logic; --
      focbus_en_in                  : in    std_logic; --
      focbus_ack_out                : out   std_logic  --
      );
  end component; 

  component fifo_2048x2
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(1 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(1 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
  );
  end component;

  component int_scan_engine
    port (
      clk               : in  std_logic; -- clk40 input
      rst               : in  std_logic; -- asynchronous global reset
      enable_in         : in  std_logic; -- 
      clear_nevent_in   : in  std_logic; --
      new_mask_ready_in : in  std_logic; -- 
      rod_tt_wr_in      : in  std_logic;
      num_events_in     : in  std_logic_vector(15 downto 0); -- 
      precal_delay_in   : in  std_logic_vector( 7 downto 0);
      trig_delay0_in    : in  std_logic_vector(15 downto 0); --
      trig_delay1_in    : in  std_logic_vector(15 downto 0); --
      evt_interval_in   : in  std_logic_vector(31 downto 0); --
      trig_mode_in      : in  std_logic_vector(31 downto 0); --
      act_groups_in     : in  std_logic_vector( 3 downto 0); --
      feocc_zero_in     : in  std_logic_vector( 3 downto 0); --
      xoff_in           : in  std_logic;
      mask_en_out       : out std_logic_vector( 2 downto 0); --
      new_mask_out      : out std_logic;
      mask_dxi_out      : out std_logic;
      fecmd0_out        : out std_logic;
      fecmd1_out        : out std_logic;
      nevent_out        : out std_logic_vector(15 downto 0);
      nevent_done_out   : out std_logic
      );
  end component;
        
  -- rod master busy now encapsulated in dedicated entity
  component rod_master_busy
    Port ( clk40in : in  STD_LOGIC;
           rod_busy_n_in_p : in  STD_LOGIC_VECTOR (1 downto 0);
           efb_header_pause_i : in  STD_LOGIC;
           formatter_mb_fef_n_in_p : in  STD_LOGIC_VECTOR (1 downto 0);
           spare_ctrl_reg_i : in  STD_LOGIC; 
           rol_test_pause_i : in  STD_LOGIC;
			  reset : in  STD_LOGIC;
			  reset_hists : in  STD_LOGIC;
			  busy_mask : in STD_LOGIC_VECTOR (7 downto 0);
           force_input : in STD_LOGIC_VECTOR (7 downto 0);
			  rod_busy_n_out_p : out  STD_LOGIC;
           current_busy_status : out  STD_LOGIC_VECTOR (7 downto 0);
           rod_busy_n_in_p_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rod_busy_n_in_p_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           formatter_mb_fef_n_in_p_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           formatter_mb_fef_n_in_p_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           efb_header_pause_i_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           --spare_ctrl_reg_i_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rol_test_pause_i_hist : out  STD_LOGIC_VECTOR (15 downto 0);
           rod_busy_n_out_p_hist : out  STD_LOGIC_VECTOR (15 downto 0)
			  );
  end component;	  

	signal rod_busy_mask_register: std_logic_vector(31 downto 0) := (others => '1');
	signal rod_busy_n_in_p_0_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal rod_busy_n_in_p_1_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal formatter_mb_fef_n_in_p_0_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal formatter_mb_fef_n_in_p_1_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal efb_header_pause_i_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal rol_test_pause_i_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal rod_busy_n_out_p_hist_register: std_logic_vector(31 downto 0) := (others => '1');
	signal rod_busy_master_status_register: std_logic_vector(31 downto 0) := (others => '1');
	
	-- UART+FIFO for VME_TTY implementation (2 slaves + master)
  component ppc_uart
    port(
		-- system interface
		clk: in std_logic; -- system clock @ 66MHz
		reset: in std_logic; -- asynchronous reset, active high
		-- serial interface
		serial_data_in: in std_logic; -- serial data input from the ppc
		serial_data_out: out std_logic; -- serial data out to the ppc
		-- client write interface
		write_data: in std_logic_vector(7 downto 0):= X"00"; -- byte to transmit
		write_en: in std_logic := '0'; -- write enable flag, active high
		write_fifo_wr_count: out std_logic_vector(4 downto 0); -- the amount of words in the write fifo = fill level
		write_fifo_full: out std_logic; -- full flag of the write fifo
		-- client read interface
		read_data: out std_logic_vector(7 downto 0); -- byte to read
		read_en: in std_logic; -- read enable flag, active high
		read_fifo_empty: out std_logic; -- empty flag of the read fifo
		read_fifo_rd_count: out std_logic_vector(14 downto 0) -- the fill level of the read fifo = amount of word in the read fifo
		);
  end component;
  signal ppc_uart_s6a_status: std_logic_vector(31 downto 0) := (others => '0');
  signal ppc_uart_s6a_data: std_logic_vector(31 downto 0) := (others => '0');
  signal RdEn_s6a: std_logic;
  signal RdEnable_s6a: std_logic;
  
  signal ppc_uart_s6b_status: std_logic_vector(31 downto 0) := (others => '0');
  signal ppc_uart_s6b_data: std_logic_vector(31 downto 0) := (others => '0');
  signal RdEn_s6b: std_logic;
  signal RdEnable_s6b: std_logic;
  
  signal ppc_uart_master_status: std_logic_vector(31 downto 0) := (others => '0');
  signal ppc_uart_master_data: std_logic_vector(31 downto 0) := (others => '0');
  signal RdEn_master: std_logic;
  signal RdEnable_master: std_logic;
  signal RdEnMaster_r0,RdEnMaster_r1: std_logic;
  signal RdEnS6a_r0,RdEnS6a_r1: std_logic;
  signal RdEnS6b_r0,RdEnS6b_r1: std_logic;
--  -- chipscope signals  
--  signal control:					std_logic_vector(35 downto 0);
--  signal trig0:					std_logic_vector(31 downto 0);
--  signal trig1:					std_logic_vector(31 downto 0);
--  signal trig2:					std_logic_vector(31 downto 0);
--  signal trig3:					std_logic_vector(31 downto 0);
--  signal trig4:					std_logic_vector(35 downto 0);

  file mappingFile: 				text;
  signal dumpIt: 					integer := 1;
  file mappingFile2: 			text;
  signal dumpIt2: 				integer := 1;
  constant designVersion: 		std_logic_vector(15 downto 0) := X"0002";
  constant designRevision: 	std_logic_vector(15 downto 0) := X"0007";
  constant designId: 			std_logic_vector(31 downto 0) := designVersion & designRevision;
  constant gndBits: std_logic_vector(31 downto 0):= (others => '0');

  signal flash_ss : std_logic_vector(0 downto 0);

  signal fpga_0_clk_1_sys_clk_pin : std_logic;
  signal fpga_0_rst_1_sys_rst_pin : std_logic := '0';

  signal cpuclk :  std_logic;
  signal cpurst, rst0 :  std_logic;
  signal sportReset: std_logic;

  signal clk40in :  std_logic;
  
  signal xps_ll_fifo_0_tx_llink_src_rdy_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_dest_rdy_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_din_pin :  std_logic_vector(31 downto 0);
  signal xps_ll_fifo_0_tx_llink_eop_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_llink_sop_n_pin :  std_logic;
  signal xps_ll_fifo_0_tx_packet :  std_logic;
  
  signal xps_epc_0_PRH_CS_n_pin :  std_logic;
  signal xps_epc_0_PRH_Addr_pin :  std_logic_vector(24 downto 0);
  signal xps_epc_0_PRH_RNW_pin :  std_logic;
  signal xps_epc_0_PRH_Rdy_pin :  std_logic;
  signal xps_epc_0_PRH_Data_I_pin :  std_logic_vector(31 downto 0);
  signal xps_epc_0_PRH_Data_O_pin :  std_logic_vector(31 downto 0);

  signal ctlReg: std_logic_vector(31 downto 0) := (others => '0');
  constant ctlMasterBit: integer := 31; -- select PPC is master with bit = 1
  constant ctlHpiBit: integer := 30; -- enable HPI port with bit = 1
  constant ctlLlSportBit: integer := 29; -- select local link sport with bit = 1
  constant ctlSportInvertBit: integer := 28; -- invert sport clock with bit = 1
  constant ctlSlaveAUartBit: integer := 27; -- select slave a uart output (priority of slaveb)
  constant ctlSlaveBUartBit: integer := 26; -- select slave a uart output (priority of slaveb)

  signal uartFromMaster: std_logic;  
  
  signal statReg: std_logic_vector(31 downto 0);
  constant statSimBit :integer := 31;
  
  -- use MSB address select PPC (1) or DSP (0) range
  
  -- and the remaining bits as register addresses
  constant ppcRegBits: integer := 8;
  constant designIdAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"00";
  constant ctlAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"04";
  constant statAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"08";
  constant spMaskAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"0c";
  constant busyMaskAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"10";
  constant wdAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"14";
  constant wdMaskAddr0: std_logic_vector(ppcRegBits - 1 downto 0) := X"18";
  constant wdMaskAddr1: std_logic_vector(ppcRegBits - 1 downto 0) := X"1c";
  constant busyMasterStatusAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"20";
  constant busyBusyNinP0HistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"24";
  constant busyBusyNinP1HistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"28";
  constant busyFormatter0HistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"2C";
  constant busyFormatter1HistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"40";
  constant busyHeaderPauseHistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"44";
  constant busyRolTestPauseHistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"48";
  constant busyOutHistAddr: std_logic_vector(ppcRegBits - 1 downto 0) := X"4C";
  constant wdMaskVal0: std_logic_vector(31 downto 0) := X"12345678";
  constant wdMaskVal1: std_logic_vector(31 downto 0) := X"deadface";
  constant ppcUartS6aStatus: std_logic_vector(ppcRegBits - 1 downto 0) := X"50";
  constant ppcUartS6aData: std_logic_vector(ppcRegBits - 1 downto 0) := X"54";
  constant ppcUartS6bStatus: std_logic_vector(ppcRegBits - 1 downto 0) := X"58";
  constant ppcUartS6bData: std_logic_vector(ppcRegBits - 1 downto 0) := X"5C";
  constant ppcUartMasterStatus: std_logic_vector(ppcRegBits - 1 downto 0) := X"60";
  constant ppcUartMasterData: std_logic_vector(ppcRegBits - 1 downto 0) := X"64";
  
  
  signal wdMask0, wdMask1: std_logic_vector(31 downto 0);

  -- interrupts sigs
  signal irqReq, irqAck: std_logic;

  -- dsp bus
  signal controller_bus_data_in_p       : std_logic_vector(31 downto 0); -- dsp data lines
  signal controller_bus_data_out_p      : std_logic_vector(31 downto 0); -- dsp data lines
  signal controller_bus_adr_in_p        : std_logic_vector(19 downto 0); -- dsp address lines
  signal controller_bus_be_n_in_p       : std_logic_vector( 3 downto 0); -- dsp be_n
  signal controller_bus_ce_n_in_p       : std_logic; -- dsp chip enable
  signal controller_bus_oe_n_in_p       : std_logic; -- dsp output enable
  signal controller_bus_re_n_in_p       : std_logic; -- dsp read enable
  signal controller_bus_rnw_in_p        : std_logic; -- dsp write enable
  signal controller_bus_tbuf_driver_i   : std_logic; -- dsp bus buffer enable  
  signal controller_bus_ack_out_p       : std_logic; -- dsp acknowledge input (ARDY)
  signal controller_bus_ack_out_i       : std_logic; -- dsp acknowledge internal

  -- int,ext regs and bus

  signal dspDout :  std_logic_vector(31 downto 0); -- dsp data lines
  signal dspDin  :  std_logic_vector(31 downto 0); -- dsp data lines
  signal dspAddr :  std_logic_vector(19 downto 0); -- dsp address lines 
  signal dspBe_n :  std_logic_vector( 3 downto 0); -- dsp byte eanbles 
  signal dspCe_n :  std_logic; -- dsp chip enable
  signal dspOe_n :  std_logic; -- dsp output enable
  signal dspRd_n :  std_logic; -- dsp read enable
  signal dspRnw  :  std_logic; -- dsp write enable
  signal dspAck  :  std_logic; -- dsp acknowledge input (ARDY)

  signal mstData: std_logic_vector(31 downto 0);
  signal mstRdy, locRdy: std_logic;
 
  signal ppcIsController: std_logic := '0';
  signal ppcSelect: std_logic;
  
  signal mstSelect_n: std_logic;
  
  -- hpi port
  signal		hpiport_0_PRM_data_bus_in_pin : std_logic_vector(31 downto 0);
  signal		hpiport_0_PRM_command_pin :  std_logic_vector(3 downto 0);
  signal		hpiport_0_PRM_strobe_pin : std_logic;    		
  signal		hpiport_0_PRM_data_bus_out_pin :  std_logic_vector(31 downto 0);
  signal		hpiport_0_PRM_data_valid_pin :  std_logic;
  signal		hpiport_0_PRM_busy_pin :  std_logic;
  signal		hpiport_0_PRM_done_pin :  std_logic;

  -- mcb serial port
  signal mcbsp_rod_0_Dx_O : std_logic;

  signal sport0Data: std_logic_vector(7 downto 0);
  signal sport1Data: std_logic_vector(7 downto 0);
  
  signal controller_tinp_out_p          : std_logic_vector(1 downto 0); -- dsp timer1,0 input
  signal controller_tout_in_p           : std_logic_vector(1 downto 0); -- dsp timer1,0 output 
  signal controller_dr_out_p            : std_logic_vector(1 downto 0); -- dsp sspi1,0 receive serial data 
  signal controller_dx_in_p             : std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit serial data 
  signal controller_dx_i                : std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit serial data 
  signal controller_clkr_in_p           : std_logic_vector(1 downto 0); -- dsp sspi1,0 receive clock        
  signal controller_clkx_out_p          : std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit clock 
  signal controller_fsr_in_p            : std_logic_vector(1 downto 0); -- dsp sspi1,0 receive frame syncroniztion
  signal controller_fsx_out_p           : std_logic_vector(1 downto 0); -- dsp sspi1,0 transmit frame syncroniztion
  signal controller_ext_int_n_out_p     : std_logic_vector(4 downto 0); -- dsp external interrupt

  signal ext_iacknum_in_p               : std_logic_vector(4 downto 0); -- mdsp external interrupt ack sigs  
  signal mdsp_int_n_in_p                : std_logic; -- this signal is new from the change in vme bus bridge chips
  signal vme_bus_int_n_out_p            : std_logic; -- vme interrupt output 
  signal vme_irq_out_p                  : std_logic_vector(2 downto 0); --

  signal rod_bus_data_in_p              : std_logic_vector(31 downto 0); -- dsp data lines
  signal rod_bus_data_out_p             : std_logic_vector(31 downto 0); -- dsp data lines
  signal rod_bus_adr_out_p              : std_logic_vector(19 downto 0); -- dsp address lines 
  signal rod_bus_ce_n_out_p             : std_logic_vector(10 downto 0); -- external component/module enables
  signal rod_bus_rnw_out_p              : std_logic; -- dsp read/now write signal

  signal fmt_hwob_p                     : std_logic;
  signal fmt_ds_p                       : std_logic;

  signal formA_ack_n_in_p               : std_logic; -- Formatter A acknowledge (active low)
  signal formB_ack_n_in_p               : std_logic; -- Formatter B acknowledge (active low)
  signal efb_ack_in_p                   : std_logic; -- EFB acknowledge (active high)
  signal router_ack_in_p                : std_logic; -- Router acknowledge (active high)
  signal dsp_detect_i                   : std_logic_vector( 3 downto 0);

  signal reset_commands_in_p            : std_logic_vector(1 downto 0); -- 
  signal reset_commands_out_p           : std_logic_vector(1 downto 0); --

  signal formatter_writes_trailer_in_p  : std_logic_vector(3 downto 0); -- signal to readout formatter/fifoFPGA ic trailer flags 
  signal show_trailer_flags_out_p       : std_logic_vector( 7 downto 0); -- signal to readout formatter/fifoFPGA ic trailer flags 
  signal header_trailer_limit_in_p      : std_logic_vector( 1 downto 0);

  signal formatter_mb_fef_n_in_p        : std_logic_vector(1 downto 0); -- mode bit fifos empty flag
  signal formatter_mb_data_out_p        : std_logic_vector(11 downto 0); -- 
  signal formatter_mb_we_n_out_p        : std_logic_vector(7 downto 0); -- mode bit fifo write enable
  signal fe_cmdpulse_formattersAB_out_i : std_logic_vector(1 downto 0); -- l1trigger pulse to formatter master accumulators

  signal rod_busy_n_in_p                : std_logic_vector(1 downto 0); -- ROD Formatters Error
  signal rod_busy_n_out_p               : std_logic;

  signal efb_edm_fef_n_in_p             : std_logic; 	-- event dynamic mask fifos empty flag
  signal efb_edm_data_out_p             : std_logic_vector(15 downto 0); --
  signal efb_edm_we_n_out_p             : std_logic; -- event dynamic mask fifo write enable 
  signal efb_edm_fifo_rst_n_out_p       : std_logic; -- event dynamic mask fifo write enable 
  signal efb_edm_fff_n_in_p				 : std_logic;

  signal fe_command_link_out_p          : std_logic_vector(15 downto 0); -- front end command link interface output leads
  signal fe_command_link_dir_out_p      : std_logic;
  signal fe_command_link_en_out_p       : std_logic;

  signal inmem_status_in_p              : std_logic_vector(3 downto 0); -- inmem status signals
  signal eventmem_status_in_p           : std_logic_vector(3 downto 0); -- eventmem status signals

  signal inmem_ctrl_out_i               : std_logic_vector(24 downto 0); -- inmem control signals
  signal inmem_ctrl_out_p               : std_logic_vector(24 downto 0); -- inmem control signals
  signal eventmem_ctrl_out_p            : std_logic_vector(10 downto 0); -- eventmem control signals

  signal bocbus_strb_out_p              : std_logic;
  signal bocbus_rodoe_out_p             : std_logic;
  signal bocbus_busy_in_p               : std_logic;  -- of these signals
  signal bocbus_write_out_p             : std_logic;

  signal boc_clock_ok_p                 : std_logic;
  signal tim_clock_ok_p                 : std_logic;
  signal slink_ldown_n_p                : std_logic;
  signal slink_lff_n_p                  : std_logic;
  signal slink_we_n_p                   : std_logic;

  signal spare_i                        : std_logic_vector(31 downto 0);
  signal spare_o                        : std_logic_vector(31 downto 0);
------------------------------------------------------------------------------
	
  signal wrSport, rdSport, sportFull, sportEmpty: std_logic;
  signal sportData, sportReg : std_logic_vector(31 downto 0);
  signal sportDataBit: std_logic;
  signal sportMask : std_logic_vector(31 downto 0);
  signal sportBits: integer range 0 to 32;

  signal fsx0_count    					  : unsigned(4 downto 0);
  signal fsx1_count    					  : unsigned(4 downto 0);
  signal fsx_en_n_obuft 					  : std_logic;

  signal apply_dynmask_mb_i 				  : std_logic_vector(1 downto 0);
  signal fe_cmd_mask_sel_i  				  : std_logic_vector(2 downto 0);

  signal reset_n								  : std_logic;
  signal rodbus_en_n_itr_i         	  : std_logic_vector(6 downto 0);

  signal rrif_ctrl_reg_i            : std_logic_vector(31 downto 0);
  signal rrif_stts_reg_i            : std_logic_vector(31 downto 0);
  signal spare_stts_reg_i           : std_logic_vector(31 downto 0);

  signal edebus_a_i                 : std_logic_vector(7 downto 0); -- EFB dynamic mask encoder bus
  signal edebus_a_ii                : unsigned(7 downto 0);
  signal edebus_din_i               : std_logic_vector(15 downto 0);
  signal edebus_dout_i              : std_logic_vector(15 downto 0);
  signal edebus_rnw_i               : std_logic;
  signal edebus_ack_i               : std_logic;
  signal edebus_en_n_i              : std_logic;
  signal dfibus_a_i                 : std_logic_vector(6 downto 0); -- Debgumem Fifo Inteface bus
  signal dfibus_rnw_i               : std_logic;
  signal dfibus_inmem_en_n_i        : std_logic;
  signal dfibus_eventmem_en_n_i     : std_logic;
  signal dfibus_ack_i               : std_logic;
  signal fcmbus_en_n_i              : std_logic;
  signal fcmbus_dout_i              : std_logic_vector(31 downto 0);
  signal regbus_en_n_i              : std_logic;
  signal regbus_rnw_i               : std_logic;
  signal regbus_a_i                 : std_logic_vector(11 downto 0);
  signal regbus_d_in_i              : std_logic_vector(31 downto 0);
  signal regbus_d_out_i             : std_logic_vector(31 downto 0);
  signal regbus_ack_i               : std_logic;
  signal focbus_a_i                 : std_logic_vector(11 downto 0); -- Frontend Occupancy Counter bus
  signal focbus_din_i               : std_logic_vector(31 downto 0);
  signal focbus_dout_i              : std_logic_vector(31 downto 0); --
  signal focbus_en_i                : std_logic;
  signal focbus_rnw_i               : std_logic;
  signal focbus_ack_i               : std_logic;
  signal frebus_a_i                 : std_logic_vector(7 downto 0); -- Formatter Readoutmodebits Encoder bus
  signal frebus_a_ii                : unsigned(7 downto 0);
  signal frebus_din_i               : std_logic_vector(15 downto 0);
  signal frebus_dout_i              : std_logic_vector(15 downto 0);
  signal frebus_en_n_i              : std_logic;
  signal frebus_rnw_i               : std_logic;
  signal frebus_ack_i               : std_logic;
  signal extbus_ack_i				    : std_logic; --
  signal intbus_ack_i 				    : std_logic; 
  signal spare_ctrl_reg_i           : std_logic_vector(31 downto 0);
  signal fe_command_mask0_i         : std_logic_vector(47 downto 0); 
  signal fe_command_mask1_i         : std_logic_vector(47 downto 0); 
  signal calstrobe_delay_reg_i      : std_logic_vector( 7 downto 0);
  signal cal_command_reg_i          : std_logic_vector(26 downto 0);
  signal modebits_fifo_stts_reg_i   : std_logic_vector(23 downto 0);
  signal dynmmask_fifo_stts_reg_i   : std_logic_vector(41 downto 0);
  signal inmem_wordcount_i          : std_logic_vector(31 downto 0);
  signal dbgmem_wordcount_i         : std_logic_vector(31 downto 0);
  signal cnfg_rdback_count_i        : std_logic_vector(31 downto 0);
  signal ide_debugmem_stts_reg_i    : std_logic_vector( 7 downto 0);
  signal ide_debugmem_cnfg_reg_i    : std_logic_vector(17 downto 0);
  signal rol_test_block_i           : std_logic_vector( 3 downto 0);
  signal rol_test_block_count_i     : std_logic_vector( 5 downto 0);
  signal int_tim_fifo_outdata_i     : std_logic_vector(7 downto 0);
  signal int_tim_fifo_re_i          : std_logic; 
  signal int_tim_fifo_rt_i          : std_logic; 
  signal slave_dsp_ints_in_i        : std_logic_vector(7 downto 0);
  signal slv_int_to_mdsp_i			 : std_logic;
  signal feocc_reset_i              : std_logic_vector(31 downto 0);
  signal feocc_num_l1_i             : std_logic_vector(103 downto 0);
  signal cal_event_type0_i          : std_logic_vector( 9 downto 0);
  signal cal_event_type1_i          : std_logic_vector( 9 downto 0);
  signal cal_l1a0_i                 : std_logic;
  signal cal_l1a1_i                 : std_logic;
  signal cal_l1a_i                  : std_logic_vector( 1 downto 0);
  signal cal_l1id0_i                : std_logic_vector(23 downto 0);
  signal cal_l1id1_i                : std_logic_vector(23 downto 0);
  signal cal_l1id0_m                : std_logic_vector(23 downto 0);
  signal cal_l1id1_m                : std_logic_vector(23 downto 0);
  signal static_l1id0_i             : std_logic_vector(23 downto 0);
  signal static_l1id1_i             : std_logic_vector(23 downto 0);
  signal cal_bcid0_i                : std_logic_vector(11 downto 0);
  signal cal_bcid1_i                : std_logic_vector(11 downto 0);
  signal static_bcid0_i             : std_logic_vector(11 downto 0);
  signal static_bcid1_i             : std_logic_vector(11 downto 0);
  signal calpulse_count0				: std_logic_vector(23 downto 0);
  signal calpulse_count1				: std_logic_vector(23 downto 0);
  signal fe_cmd_mask_updated_i      : std_logic;
  signal corrective_fe_pulse_i      : std_logic;
  signal send_int_test_i 				 : std_logic;
  signal test_irq_i      				 : std_logic_vector(2 downto 0);
  signal ecr_id                     : unsigned(7 downto 0);
  signal ecr_count_i                : std_logic_vector(7 downto 0);
  signal nevent_i       				 : std_logic_vector(15 downto 0);  -- NEvent
  signal nevent_o       				 : std_logic_vector(15 downto 0);  -- NEvent
  signal precal_delay_i 				 : std_logic_vector( 7 downto 0);  -- PreCal Delay
  signal trig_delay0_i  				 : std_logic_vector(15 downto 0);  -- TrigDelay1
  signal trig_delay1_i  				 : std_logic_vector(15 downto 0);  -- TrigDelay2
  signal interval_i     				 : std_logic_vector(31 downto 0);  -- Interval
  signal trig_mode_i    				 : std_logic_vector(31 downto 0);  -- TrigMode
  signal int_mask_en_i     			 : std_logic_vector(2 downto 0);
  signal int_new_mask_i    			 : std_logic;
  signal int_load_event_i  			 : std_logic;
  signal int_nevent_done_i 			 : std_logic;
  signal event_l1id   					 : std_logic_vector(23 downto 0);
  signal dsp_stts_reg_i             : std_logic_vector(31 downto 0);
  signal test_stts_reg_i            : std_logic_vector(31 downto 0);
  signal debug_fifo_stts_i          : std_logic_vector( 7 downto 0);

  signal l1_accept_i					 : std_logic;
  signal event_counter_reset_i		 : std_logic;
  signal bunch_counter_reset_i		 : std_logic;
  signal cal_strobe_i					 : std_logic;
  signal serial_l1id_i					 : std_logic;
  signal serial_eventtype_i			 : std_logic;
  signal ttc6_i							 : std_logic;
  signal ttc7_i							 : std_logic;
  signal emif_re_i						 : std_logic;
  signal dsp_timer1						 : std_logic;
  signal slink_lff_i					 : std_logic;
  signal slink_ldown_i					 : std_logic;
  signal slink_we_i						 : std_logic;
  signal pause_triggers_i				 : std_logic;
  signal rol_test_pause_i				 : std_logic;
  signal mask_dxi_i    				 : std_logic;
  signal event_count1 					 : unsigned(23 downto 0);
  signal event_count2 					 : unsigned(23 downto 0);
  signal tim_signal_ii              : std_logic_vector(5 downto 0);
  signal ck100							 : std_logic;
  signal counter_busy					 : integer range 0 to 2000000;

  signal mstRd_n: std_logic;
  signal mstOe_n: std_logic; 

  signal efb_header_pause_i : std_logic;
  signal rol_trigs_on_i   : std_logic;
  signal sweeper_event_on_rod_i : std_logic;
  signal efb_rod_busy_i         : std_logic;
  signal formatter_mb_fff_n_in: std_logic_vector(1 downto 0);

  type rol_test_states is (
    idle,
    setup_delay,
    send_l1a,
    wait_for_cycle
    );
  signal rol_test         : rol_test_states;
  signal rol_led_n_i      : std_logic;
  signal rol_test_i       : std_logic;
  signal pin_hold_reg_i             : std_logic_vector(31 downto 0);
  signal cmd_cycle_done_i   : std_logic;
  type cnfg_rdbk_states is (
    reset, 
    idle, 
    start_delay, 
    wr_to_fifo,
    set_done_flag, 
    return_to_idle
    );
  signal cnfg_rdbk_state : cnfg_rdbk_states;
  signal wr_cnfg_data_inmemA_i : std_logic;
  signal wr_cnfg_data_inmemB_i : std_logic;
  signal rdbk_done_i           : std_logic;
  signal new_ttype_valid_i          : std_logic;

  signal ttype_bits_i               : std_logic_vector( 9 downto 0);
  signal trap_done_i            : std_logic;
  signal rcf_hrdy_n_p	       : std_logic;
  signal slave_dsp_ints_out_i       : std_logic_vector(3 downto 0);

  type vme_int_states is (
    idle,
    issue_int,
    clear_int,
    done
    );
  signal vme_int_state : vme_int_states;

  signal rol_l1a_i     : std_logic;
  signal cmd_bit_count : unsigned(2 downto 0);
  signal rol_test_block0_ii         : std_logic;

  signal gnd_i : std_logic;
  signal vcc_i : std_logic;

  signal fmt_fe_cmd_i : std_logic;

  signal inmem_ctrl_i               : std_logic_vector(15 downto 0);

  signal eventmem_ctrl_i            : std_logic_vector(10 downto 0);
  signal tb_inmem_ctrl_out_i        : std_logic_vector(11 downto 0);


  signal l1id_bits_i                : std_logic_vector(23 downto 0);
  signal bcid_bits_i                : std_logic_vector(11 downto 0);

  signal new_l1id_valid_i           : std_logic;
  signal trap_indicator_i           : std_logic_vector(1 downto 0);

	------------------
	-- watchdog. runs from 40MHz
	-- disabled after reset
	-- enabled with first write access
	-- generates reset upon timeout
	function wdTimeout return std_logic_vector is
	begin
		if simulation then 
			return X"00000100";-- 256 * 40MHz ~ 6us
		else
			return X"01000000";-- 16M * 40MHz ~ 0.3s
		end if;
	end wdTimeout;
	
	signal wdMainCnt: std_logic_vector(31 downto 0);
	signal wdRstCnt: std_logic_vector(7 downto 0); -- ensure min reset duration
	constant wdMaxRstCnt: std_logic_vector(7 downto 0) := (others => '1');
	signal wdReset: std_logic := '0';
	signal wdEnable, wdEnable_r0, wdEnable_r1: std_logic;
	signal master_reset_int: std_logic := '0';
	type wdStates is (wdIdle, wdActive, WdDone);
	signal wdState: wdStates;
	signal wdTrig: std_logic := '0';
	signal wdogTiggerCnt: std_logic_vector(31 downto 0);
	
	-- PRM interfacce bits: copy from PRM code
	-- ----------- prm control out ----------
	-- hpi cmd out is 4 bits
	constant ctl_from_prm_hpi_cmd_lo_bit: integer := 0;
	constant ctl_from_prm_hpi_cmd_hi_bit: integer := 3;
	-- spi bits
	constant ctl_from_prm_spi_miso_bit: integer := 4; -- needs to be swapped with in 4 if spi-sck used as clock
	--xvc bits
	constant ctl_from_prm_xvc_tdo_bit: integer := 5;

	-- ----------- prm control in ----------
	-- hpi cmd in is 3 bits, but bit 0 is unused
	constant ctl_to_prm_hpi_valid_bit: integer := 0; -- unused
	constant ctl_to_prm_hpi_done_bit: integer := 1;
	constant ctl_to_prm_hpi_busy_bit: integer := 2;
	-- spi bits
	constant ctl_to_prm_spi_ss_bit: integer := 3;
	constant ctl_to_prm_spi_sck_bit: integer := 4;   -- needs to be swapped with out 4 if used as clock
	constant ctl_to_prm_spi_mosi_bit: integer := 5; 
	--watchdog bit
	constant ctl_to_prm_wdog_bit: integer := 6;
	--xvc bits
	constant ctl_to_prm_xvc_tck_bit: integer := 7;
	constant ctl_to_prm_xvc_tms_bit: integer := 8;
	constant ctl_to_prm_xvc_tdi_bit: integer := 9;

	constant ctl_to_prm_last_bit: integer := ctl_to_prm_xvc_tdi_bit;
	
	
	-- the GIT hash key
	COMPONENT gitId
	PORT(
		reset : IN std_logic;
		read : IN std_logic;          
		id : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	signal gitIdReset : std_logic;
	signal gitIdRead: std_logic;
	signal gitIdByte: std_logic_vector(7 downto 0);

  signal feocc_zero_i   : std_logic_vector( 3 downto 0); --

  signal act_group_i    : std_logic_vector( 3 downto 0); --
  signal cal_serial0_i  : std_logic; -- SerPort0
  signal cal_serial1_i  : std_logic; -- SerPort1
  signal cal_serial0_o  : std_logic; -- SerPort0
  signal cal_serial1_o  : std_logic; -- SerPort1


  signal int_scan_fecmd0_i : std_logic;
  signal int_scan_fecmd1_i : std_logic;
  signal fe_pulse_i                 : std_logic;
  signal fe_l1_pulse_i              : std_logic_vector(31 downto 0);
  
  -- UART debug
  	component uart_ila
	  PORT (
		 CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
		 CLK : IN STD_LOGIC;
		 TRIG0 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		 TRIG1 : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
		 TRIG2 : IN STD_LOGIC_VECTOR(23 DOWNTO 0)
		 );
	end component;
       signal uart_cs_CONTROL : STD_LOGIC_VECTOR(35 DOWNTO 0);
       signal uart_cs_trig_master : STD_LOGIC_VECTOR(23 DOWNTO 0);
       signal uart_cs_trig_s6a : STD_LOGIC_VECTOR(23 DOWNTO 0);
       signal uart_cs_trig_s6b : STD_LOGIC_VECTOR(23 DOWNTO 0);
       constant uartDebug: boolean := false;

  -- PRM spi debugging 
	component ila8bit
	  PORT (
		 CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
		 CLK : IN STD_LOGIC;
		 TRIG0 : IN STD_LOGIC_VECTOR(7 DOWNTO 0));

	end component;
	component icon
	  PORT (
		 CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
	end component;
	signal cs_CONTROL0 : STD_LOGIC_VECTOR(35 DOWNTO 0);
	signal cs_trig0 : STD_LOGIC_VECTOR(7 DOWNTO 0);
	constant spiDebug: boolean := false; --true;

begin

  gnd_i 										<= '0';
  vcc_i 										<= '1';
  tim_enable								<= rrif_ctrl_reg_i(27);

  efb_edm_fef_n_in_p 					<= efb_edm_fef_n_in_A(0) OR efb_edm_fef_n_in_B(0);
  efb_edm_fff_n_in_p						<= efb_edm_fff_n_in_A(0) OR efb_edm_fff_n_in_B(0);
  
  form_mode_bits_weA						<= formatter_mb_we_n_out_p(0);
  form_mode_bits_weB						<= formatter_mb_we_n_out_p(1);
  fe_cmdpulse_form_outA					<= fe_cmdpulse_formattersAB_out_i(0);
  fe_cmdpulse_form_outB					<= fe_cmdpulse_formattersAB_out_i(1);
  show_trailer_flagsA					<= show_trailer_flags_out_p(0);
  show_trailer_flagsB					<= show_trailer_flags_out_p(1);
  rod_busy_n_out							<= rod_busy_n_out_p;
  reset_commands_in_p					<= reset_commands_in;
  formatter_mb_fef_n_in_p				<= form_mb_fifo_efA & form_mb_fifo_efB;
  mdsp_int_n_in_p							<= mdsp_int_n_in;
  slave_dsp_ints_in_i					<= (others => '0');
  tim_clock_ok_p							<= tim_clock_ok;
  boc_clock_ok_p							<= boc_clock_ok;
  header_trailer_limit_in_p(0)		<= header_trailer_limit_inA;
  header_trailer_limit_in_p(1)		<= header_trailer_limit_inB;
  rod_busy_n_in_p							<= rod_busy_n_in;
  
  tim_signal_ii(0) <= l1_accept_i           OR int_tim_fifo_outdata_i(5);
  tim_signal_ii(1) <= event_counter_reset_i OR int_tim_fifo_outdata_i(4);
  tim_signal_ii(2) <= bunch_counter_reset_i OR int_tim_fifo_outdata_i(3);
  tim_signal_ii(3) <= cal_strobe_i          OR int_tim_fifo_outdata_i(2);
  tim_signal_ii(4) <= serial_l1id_i         OR int_tim_fifo_outdata_i(1);
  tim_signal_ii(5) <= serial_eventtype_i    OR int_tim_fifo_outdata_i(0);

-- External Reset for the FIFOs in the Formatters and the EFB
  efb_edm_fifo_rst_n_out_p <= NOT spare_ctrl_reg_i(2);

-- Send command streams to the Formatters for the Simulator
-- Redirected in V35f to allow TX of FE commands to Formatters
  form_mode_bits_rst_nA <= fmt_fe_cmd_i;
  form_mode_bits_rst_nB <= fmt_fe_cmd_i;

-- ROD Bus peripheral select Signals
  regbus_en_n_i          <= rodbus_en_n_itr_i(0); -- Controller Internal Registers
  focbus_en_i            <= NOT rodbus_en_n_itr_i(1); -- FE Occupancy Counters
  fcmbus_en_n_i          <= rodbus_en_n_itr_i(2); -- FE Command Mask LUT
  edebus_en_n_i          <= rodbus_en_n_itr_i(3); -- EFB DynamicMask Encoder
  frebus_en_n_i          <= rodbus_en_n_itr_i(4); -- Formatter Readout ModeBits Encoder
  dfibus_inmem_en_n_i    <= rodbus_en_n_itr_i(5); -- Inmem Control
  dfibus_eventmem_en_n_i <= rodbus_en_n_itr_i(6); -- Eventmem Control

  controller_clkx_out_p(0) <= clk40in;
  controller_clkx_out_p(1) <= clk40in;
  fsx_en_n_obuft 				<= NOT spare_ctrl_reg_i(18);

  fe_command_link_dir_out_p <= '0'; --NOT rrif_ctrl_reg_i(0); 
  fe_command_link_en_out_p  <= '0'; --NOT rrif_ctrl_reg_i(0);
 
  efb_edm_we_n_out					<= efb_edm_we_n_out_p;
  efb_edm_data_out					<= efb_edm_data_out_p;
  efb_edm_fifo_rst_n_out			<= efb_edm_fifo_rst_n_out_p;
  
  reset_commands_out					<= reset_commands_out_p;
  
  formatter_mb_fff_n_in				<= form_mb_fifo_ffA & form_mb_fifo_ffB;
  
  clkBuf: ibufgds port map (i => clk100_p, ib => clk100_n, o => fpga_0_clk_1_sys_clk_pin);
  clkBuf40: ibufgds port map (i => clk40_p, ib => clk40_n, o => clk40in);

  
--  icon_instance : icon
--    port map (
--      CONTROL0 => control);
--  
--  ila_instance : ila
--    port map (
--      CONTROL => control,
--      CLK => clk40in,		-- 40 MHz
--      TRIG0 => trig0,
--      TRIG1 => trig1,
--      TRIG2 => trig2,
--		TRIG3 => trig3,
--		TRIG4 => trig4);
--  
--  trig0(0)					<= form_mb_fifo_ffA;
--  trig0(1)					<= form_mb_fifo_ffB;
--  trig0(2)					<= form_mb_fifo_efA;
--  trig0(3)					<= form_mb_fifo_efB;
--  trig0(5 downto 4)		<= efb_edm_fifo_empty_error_in_A;
--  trig0(7 downto 6)		<= efb_edm_fff_n_in_A;
--  trig0(9 downto 8)		<= efb_edm_fef_n_in_A;
--  trig0(11 downto 10)	<= efb_edm_fifo_empty_error_in_B;
--  trig0(13 downto 12)	<= efb_edm_fff_n_in_B;
--  trig0(15 downto 14)	<= efb_edm_fef_n_in_B;
--  
--  trig1(1 downto 0)		<= apply_dynmask_mb_i;
--  
--  trig2(15 downto 0)		<= efb_edm_data_out_p;
--  trig2(16)					<= efb_edm_we_n_out_p;
--  
--  trig3(15 downto 0)		<= fe_command_link_out_p;
  
	 
  signal_register_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
      debug_fifo_stts_i     <= (others => '0');
      dsp_detect_i          <= (others => '0');
      l1_accept_i           <= '0';
      event_counter_reset_i <= '0';
      bunch_counter_reset_i <= '0';
      cal_strobe_i          <= '0';
      serial_l1id_i         <= '0';
      serial_eventtype_i    <= '0';
      ttc6_i                <= '0';
      ttc7_i                <= '0';
      emif_re_i             <= '0';
      dsp_timer1            <= '0';
      slink_lff_i           <= '0';
      slink_ldown_i         <= '0';
      slink_we_i            <= '0';
      pause_triggers_i      <= '0';
      rol_test_pause_i      <= '0';
--      rod_busy_n_out_p      <= '1';
    elsif(rising_edge(clk40in)) then

-- this rod busy logic is now in dedicated component "rod_master_busy"
-- rod_busy_in is Low True
--		rod_busy_n_out_p	<= '1';
--      rod_busy_n_out_p  <=      rod_busy_n_in_p(1) AND 
--                                rod_busy_n_in_p(0) AND 
--                                NOT  efb_header_pause_i AND 
--                                NOT (NOT formatter_mb_fef_n_in_p(0) AND spare_ctrl_reg_i(10)) AND -- Formatter A pause from EFB
--                                NOT (NOT formatter_mb_fef_n_in_p(1) AND spare_ctrl_reg_i(10)) AND -- Formatter B pause from EFB
--                                NOT rol_test_pause_i; -- Required when TIM issues RoL triggers

      rol_test_pause_i <= (rol_test_block_i(0) OR rol_test_block_i(2)) AND 
                          (slink_lff_i OR rrif_stts_reg_i(16)); -- {rrif_stts_reg_i(16)==EFB Event ID FIFO Full}

      pause_triggers_i  <= NOT rod_busy_n_in_p(1) OR
                           NOT rod_busy_n_in_p(0) OR
                           NOT formatter_mb_fef_n_in_p(0) OR  --Formatter A pause from EFB
                           NOT formatter_mb_fef_n_in_p(1) OR  --Formatter B pause from EFB
                           efb_header_pause_i OR 
                           rol_test_pause_i;

      debug_fifo_stts_i <= rol_trigs_on_i &
                           "000" &
                           NOT inmem_status_in_p(3) &
                           NOT inmem_status_in_p(2) &
                           NOT inmem_status_in_p(1) &
                           NOT inmem_status_in_p(0) ;

---- The TIM signals are active LOW!
      l1_accept_i           <= NOT l1_accept_n_in;
      event_counter_reset_i <= NOT event_counter_reset_n_in;
      bunch_counter_reset_i <= NOT bunch_counter_reset_n_in;
      cal_strobe_i          <= NOT cal_strobe_n_in;
      serial_l1id_i         <= NOT serial_l1id_n_in;
      serial_eventtype_i    <= NOT serial_eventtype_n_in;
      ttc6_i                <= NOT ttc6_n_in;
      ttc7_i                <= NOT ttc7_n_in;
    end if;
  end process signal_register_process;

  sweeper_event_detect_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
      sweeper_event_on_rod_i <= '0';
      efb_rod_busy_i <= '0';
    elsif(rising_edge(clk40in)) then
      efb_rod_busy_i <= spare_i(25);
      -- Sweeper Event detected by the RCF
      if ((new_ttype_valid_i = '1' AND ttype_bits_i(7 downto 0) = "00000111") OR
          -- EFB handshake to set sweeper event status on the rising edge
          (spare_i(25) = '1' AND efb_rod_busy_i = '0')) then
        sweeper_event_on_rod_i <= '1';
        -- EFB handshake to clear sweeper event status on the falling edge
        --  elsif (spare_i(25) = '0' AND efb_rod_busy_i = '1') then
      elsif (efb_rod_busy_i = '1') then
        sweeper_event_on_rod_i <= '0';
      end if;
    end if;
  end process sweeper_event_detect_process;

  rrif_stts_reg_bitmap_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
    elsif(rising_edge(clk40in)) then
      rrif_stts_reg_i( 0) <= tim_clock_ok_p; 
      rrif_stts_reg_i( 1) <= boc_clock_ok_p;
      rrif_stts_reg_i( 2) <= bocbus_busy_in_p;
--  rrif_stts_reg_i( 3) <= rdbk_done_i OR trap_done_i;
--  rrif_stts_reg_i( 4) == Cal Test Ready
--  rrif_stts_reg_i( 5) == FE Trigger FIFO empty flag, connected to PortMap
--  rrif_stts_reg_i( 6) == FE Trigger FIFO full flag, connected to PortMap
      rrif_stts_reg_i( 7) <= NOT formatter_mb_fef_n_in_p(0);
      rrif_stts_reg_i( 8) <= NOT form_mb_fifo_ffA;
      rrif_stts_reg_i( 9) <= NOT formatter_mb_fef_n_in_p(1);
      rrif_stts_reg_i(10) <= NOT form_mb_fifo_ffB;
      rrif_stts_reg_i(11) <= NOT header_trailer_limit_in_p(0);
      rrif_stts_reg_i(12) <= NOT header_trailer_limit_in_p(1);
      rrif_stts_reg_i(13) <= NOT rod_busy_n_in_p(0);
      rrif_stts_reg_i(14) <= NOT rod_busy_n_in_p(1);
      rrif_stts_reg_i(15) <= NOT efb_edm_fef_n_in_p;
      rrif_stts_reg_i(16) <= NOT efb_edm_fff_n_in_p;
      rrif_stts_reg_i(17) <= efb_edm_fifo_empty_error_in_A(0);
      rrif_stts_reg_i(18) <= NOT eventmem_status_in_p(1); --Evtmem FIFO A EF
      rrif_stts_reg_i(19) <= NOT eventmem_status_in_p(0); --Evtmem FIFO A FF
      rrif_stts_reg_i(20) <= NOT eventmem_status_in_p(3); --Evtmem FIFO B EF
      rrif_stts_reg_i(21) <= NOT eventmem_status_in_p(2); --Evtmem FIFO B FF
--  rrif_stts_reg_i(22:29) == FE cmdpulse_count_out
--  rrif_stts_reg_i(30)    == FE Occupancy Count All Zero
--  rrif_stts_reg_i(31)    == Command Mask Ready 
    end if ;
  end process rrif_stts_reg_bitmap_process; 
  
  rrif_stts_reg_i( 3) <= rdbk_done_i OR trap_done_i;

  spare_stts_reg_bitmap_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
    elsif(rising_edge(clk40in)) then
--  spare_stts_reg_i(18 downto  8) == FE Trigger Occupancy
      spare_stts_reg_i(23 downto 19) <= ext_iacknum_in_p;
      spare_stts_reg_i(31)           <= '1';      -- 0=> SCT 1=>Pixel
    end if ;
  end process spare_stts_reg_bitmap_process;
  
  spare_stts_reg_i(0)  <= l1_accept_i;
  spare_stts_reg_i(1)  <= event_counter_reset_i;
  spare_stts_reg_i(2)  <= bunch_counter_reset_i;
  spare_stts_reg_i(3)  <= cal_strobe_i;
  spare_stts_reg_i(4)  <= serial_l1id_i;
  spare_stts_reg_i(5)  <= serial_eventtype_i;
  spare_stts_reg_i(6)  <= ttc6_i;
  spare_stts_reg_i(7)  <= ttc7_i;
  spare_stts_reg_i(25) <= slink_lff_i;
  spare_stts_reg_i(26) <= slink_ldown_i;
  spare_stts_reg_i(30 downto 27) <= dsp_detect_i;

  dsp_stts_reg_bitmap_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
    elsif(rising_edge(clk40in)) then
      dsp_stts_reg_i( 5) <= mdsp_int_n_in_p;
      dsp_stts_reg_i(10) <= controller_bus_rnw_in_p AND controller_bus_ce_n_in_p;
      dsp_stts_reg_i(31 downto 16) <= hpiport_0_PRM_data_bus_in_pin(15 downto 0);
    end if ;
  end process dsp_stts_reg_bitmap_process;
  
  dsp_stts_reg_i( 4) <= emif_re_i;
  dsp_stts_reg_i(11) <= dsp_timer1;

  rcf_hrdy_n_reg_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
      rcf_hrdy_n_p <= '1';
    elsif(rising_edge(clk40in)) then
      rcf_hrdy_n_p <= controller_bus_ack_out_p;
    end if ;
  end process rcf_hrdy_n_reg_process;
  
  test_stts_reg_i <= spare_i;

  pin_hold_reg_bitmap_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
    elsif(rising_edge(clk40in)) then
      pin_hold_reg_i( 0) <= NOT rod_busy_n_out_p;
      pin_hold_reg_i( 1) <= NOT rod_busy_n_in_p(0); -- ROD Busy from Formatters
      pin_hold_reg_i( 2) <= NOT rod_busy_n_in_p(1); -- ROD Busy from Formatters
      pin_hold_reg_i( 5) <= NOT formatter_mb_fef_n_in_p(0);  -- Formatter A pause from EFB
      pin_hold_reg_i( 6) <= NOT formatter_mb_fef_n_in_p(1);  -- Formatter B pause from EFB
      pin_hold_reg_i( 9) <= NOT header_trailer_limit_in_p(0);
      pin_hold_reg_i(10) <= NOT header_trailer_limit_in_p(1);
      pin_hold_reg_i(28) <= formA_ack_n_in_p;
      pin_hold_reg_i(29) <= formB_ack_n_in_p;
    end if ;
  end process pin_hold_reg_bitmap_process;

  pin_hold_reg_i( 3) <= efb_header_pause_i;     -- ROD Busy from Event ID FIFOs
  pin_hold_reg_i( 4) <= rol_test_pause_i;       -- Required when TIM issues RoL triggers
  pin_hold_reg_i( 7) <= pause_triggers_i;
  pin_hold_reg_i( 8) <= sweeper_event_on_rod_i;
  pin_hold_reg_i(11) <= '0';
  pin_hold_reg_i(23 downto 12) <= pin_hold_reg_i(11 downto 0);
  pin_hold_reg_i(27 downto 24) <= (others => '0');
--  pin_hold_reg_i(30) == modebits_error
--  pin_hold_reg_i(31) == sdsp_address_error

------------------------------------------------------------------------------
  ecrid_counter_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
      ecr_id <= (others => '0');
    elsif(rising_edge(clk40in)) then
      if (spare_ctrl_reg_i(12) = '1') then
        ecr_id <= UNSIGNED(ecr_count_i);
      elsif (tim_signal_ii(1) = '1') then
        ecr_id <= ecr_id + 1;
      end if;
    end if;
  end process ecrid_counter_process;

------------------------------------------------------------------------------
-- Process to drive the DSP bus when required
-- '0' : tbuf active  --  '1' : tbuf high Z
-- only drive the tbuf outputs when the OE is active! 
  controller_bus_tbuf_driver_process: process(master_reset, clk40in)
  begin
    if(master_reset = '1') then
      controller_bus_tbuf_driver_i <= '1';
    elsif(rising_edge(clk40in)) then
      controller_bus_tbuf_driver_i <= controller_bus_oe_n_in_p OR
                                      controller_bus_ce_n_in_p; 
    end if;
  end process controller_bus_tbuf_driver_process;
  
------------------------------------------------------------------------------
  dsp_emif_ARDY_driver_process: process(master_reset, controller_bus_ack_out_i, controller_bus_ce_n_in_p)
  begin
    if(master_reset = '1') then
      controller_bus_ack_out_p <= '1';
    elsif (controller_bus_ce_n_in_p = '0') then
      controller_bus_ack_out_p <= controller_bus_ack_out_i;
    else
      controller_bus_ack_out_p <= '1';
    end if;
  end process dsp_emif_ARDY_driver_process; 
  
--------------------------------------------------------------------------------
  reset_command_io_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      controller_dr_out_p  <= (others => '0');
      reset_commands_out_p <= (others => '0');
    elsif(rising_edge(clk40in)) then
      controller_dr_out_p  <= reset_commands_in_p;
      -- MDSP to PRM Reset functions
      reset_commands_out_p(0) <= controller_fsr_in_p(1);
      -- Command to trigger ROD Busy histogram capture in the PRM    
      reset_commands_out_p(1) <= cal_l1a0_i OR cal_l1a1_i OR tim_signal_ii(0);
    end if;
  end process reset_command_io_process;

-------------------------------------------------------------------------------
-- Clock and Frame Sync for SP0
-- fsx0_count : unsigned(4 downto 0);
  msdsp_fsx_out_sp0_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      controller_fsx_out_p(0) <= '0';
      fsx0_count <= (others => '0');
    elsif(rising_edge(clk40in)) then
      if (fsx0_count = 0) then
        controller_fsx_out_p(0) <= '1';
      else
        controller_fsx_out_p(0) <= '0';
      end if;
      fsx0_count <= fsx0_count + 1;
    end if;
  end process msdsp_fsx_out_sp0_process;

-- Clock and Frame Sync for SP1
-- fsx1_count : unsigned(4 downto 0);
  msdsp_fsx_out_sp1_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      controller_fsx_out_p(1) <= '0';
      fsx1_count <= (others => '0');
    elsif(rising_edge(clk40in)) then
      -- Adjustable offset to fsx0
      if (fsx1_count = unsigned(spare_ctrl_reg_i(17 downto 13))) then
        controller_fsx_out_p(1) <= '1';
      else
        controller_fsx_out_p(1) <= '0';
      end if;
      fsx1_count <= fsx1_count + 1;
    end if;
  end process msdsp_fsx_out_sp1_process;

--------------------------------------------------------------------------------
  dsp_dx_mask_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      controller_dx_i <= (others => '0');
    elsif(rising_edge(clk40in)) then
      controller_dx_i(0) <= controller_dx_in_p(0) AND NOT spare_ctrl_reg_i(19);
      controller_dx_i(1) <= controller_dx_in_p(1) AND NOT spare_ctrl_reg_i(20);
    end if;
  end process dsp_dx_mask_process;

--------------------------------------------------------------------------------

  interrupts_enable_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      controller_tinp_out_p         <= (others => '0');
      controller_ext_int_n_out_p    <= (others => '0');
    elsif(rising_edge(clk40in)) then
      controller_tinp_out_p(0)      <=                              spare_ctrl_reg_i(25);
      controller_tinp_out_p(1)      <=                              spare_ctrl_reg_i(25);
      controller_ext_int_n_out_p(0) <= apply_dynmask_mb_i(0)    AND spare_ctrl_reg_i(26);
-- interrupt to the master dsp to signify that a DEFAULT trigger has occurred
      controller_ext_int_n_out_p(1) <= apply_dynmask_mb_i(1)    AND spare_ctrl_reg_i(27);
-- interrupt to the master dsp to signify that a CORRECTIVE trigger has occurred
      controller_ext_int_n_out_p(2) <=                              spare_ctrl_reg_i(28);
      controller_ext_int_n_out_p(3) <=                              spare_ctrl_reg_i(29);
      controller_ext_int_n_out_p(4) <= slv_int_to_mdsp_i        AND spare_ctrl_reg_i(30);
    end if;
  end process interrupts_enable_process;

--------------------------------------------------------------------------------
--  This is the first cut of the RCF Interrupt handler, and needs improvements
--------------------------------------------------------------------------------
  vme_irq_vector_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      vme_bus_int_n_out_p <= '1';
      vme_irq_out_p       <= (others => '0');
    elsif(rising_edge(clk40in)) then
      if (spare_ctrl_reg_i(31) = '1') then
        case vme_int_state is
          when idle =>
            vme_bus_int_n_out_p <= '1';
            vme_irq_out_p <= (others => '0');
            if (send_int_test_i = '1') then
              vme_irq_out_p <= test_irq_i;
              vme_int_state <= issue_int;
            end if;
          when issue_int => 
            vme_bus_int_n_out_p <= '0';
            if (spare_i(5) = '1') then
              vme_int_state <= clear_int;
            end if;
          when clear_int  =>
            if (spare_i(5) = '0') then
              vme_bus_int_n_out_p <= '1';
              vme_int_state <= done;
            end if;
          when done =>
            vme_int_state <= idle;
            vme_bus_int_n_out_p <= '1';
            vme_irq_out_p <= (others => '0');
          when others     =>
            vme_bus_int_n_out_p <= '1';
            vme_int_state <= idle;
        end case;
      else
        vme_bus_int_n_out_p <= '1';
        vme_irq_out_p <= (others => '0');
      end if;
    end if;
  end process vme_irq_vector_process;

------------------------------------------------------------------------------
--
--  rol_test_l1a_source_process: process(clk40in, master_reset)
--    variable wait_count : unsigned(10 downto 0);
--    variable trig_count : unsigned(23 downto 0);
--  begin
--    if(master_reset = '1') then
--      rol_l1a_i <= '0';
--      rol_test <= idle;
--      rol_led_n_i <= '1';
--      rol_test_i <= '0';
--      rol_trigs_on_i <= '0';
--      cmd_bit_count <= (others => '0');
--      wait_count := (others => '0');
--      trig_count := (others => '0');
--    elsif(rising_edge(clk40in)) then
--      rol_test_block0_ii <= rol_test_block_i(0);
--      case rol_test is
--        when idle =>
--          rol_l1a_i <= '0';
--          rol_test_i <= rol_test_block_i(2);  -- Enable ROL Test Block for TIM Triggers
--          cmd_bit_count <= (others => '0');
--          wait_count := (others => '0');
--          trig_count := (others => '0');
--          if (
--            (rol_test_block_i = "0001") OR   -- Send Multiple triggers
--            (rol_test_block_i = "0011" AND rol_test_block0_ii  = '0') OR -- Send 1 trigger
--            (rol_test_block_i = "1001" AND rol_test_block0_ii  = '0')    -- Send n triggers
--            ) then
--            rol_led_n_i <= '0';
--            rol_trigs_on_i <= '1';
--            rol_test <= setup_delay;
--          end if;
--          -- Delay is required to allow proper configuration and to keep the trigger 
--          -- rate close to 100kHz
--          -- rrif_stts_reg_i(16) == NOT efb_edm_fff_n_in_p
--          -- slink_lff_i == Xoff from the Slink
--          -- slink_ldown_i == LSlink Down
--        when setup_delay =>
--          rol_test_i <= '1';
--          rol_led_n_i <= '0';
--          if (pause_triggers_i = '0') then
--            wait_count := wait_count + 1 ;
--            if (wait_count = 220) then
--              rol_test <= send_l1a;
--              wait_count := (others => '0');
--            end if;
--          end if;
--        when send_l1a =>
--          if (spare_stts_reg_i(31) = '0') then --  l1accept: "110"  SCT
--            case cmd_bit_count is
--              when "000"  => rol_l1a_i <= '1';                 
--              when "001"  => rol_l1a_i <= '1';                 
--              when "010"  => rol_l1a_i <= '0';                 
--              when others => rol_l1a_i <= '0';
--                             cmd_bit_count <= (others => '0');
--                             rol_test <= wait_for_cycle;
--            end case;
--            cmd_bit_count <= cmd_bit_count + '1';
--          elsif (spare_stts_reg_i(31) = '1') then --  l1accept: "11101"  Pixel
--            case cmd_bit_count is
--              when "000"  => rol_l1a_i <= '1';                 
--              when "001"  => rol_l1a_i <= '1';                 
--              when "010"  => rol_l1a_i <= '1';                 
--              when "011"  => rol_l1a_i <= '0';                 
--              when "100"  => rol_l1a_i <= '1';                 
--              when "101"  => rol_l1a_i <= '0';
--              when others => rol_l1a_i <= '0';
--                             cmd_bit_count <= (others => '0');
--                             rol_test <= wait_for_cycle;
--            end case;
--            cmd_bit_count <= cmd_bit_count + '1';
--          end if;
--        when wait_for_cycle =>
--          if (cmd_cycle_done_i = '1') then
--            rol_led_n_i <= '1';
--            if (rol_test_block_i(3) = '1') then
--              if (trig_count = unsigned(dbgmem_wordcount_i(23 downto 0))) then
--                rol_trigs_on_i <= '0';
--                rol_test <= idle;
--              else
--                cmd_bit_count <= (others => '0');
--                trig_count := trig_count + 1 ;
--                wait_count := (others => '0');
--                rol_test <= setup_delay;
--              end if;  
--            else
--              rol_trigs_on_i <= '0';
--              rol_test <= idle;
--            end if; 
--          end if; 
--        when others =>
--          rol_l1a_i <= '0';
--          rol_test <= idle;
--          rol_led_n_i <= '1';
--          cmd_bit_count <= (others => '0');
--      end case;
--    end if;
--  end process rol_test_l1a_source_process;

-- DAV: hacking this process to avoid RoL test signals generation !!!
rol_test_l1a_source_process: process(clk40in, master_reset)
  begin
    if(master_reset = '1') then
      rol_l1a_i <= '0';
      rol_test <= idle;
      rol_led_n_i <= '1';
--      rol_test_i <= '0';
      rol_trigs_on_i <= '0';
      cmd_bit_count <= (others => '0');
    elsif(rising_edge(clk40in)) then
		rol_l1a_i <= '0';
      rol_test <= idle;
      rol_led_n_i <= '1';
--      rol_test_i <= '1';---------------------------------------------------------------------------------------------
      rol_trigs_on_i <= '0';
      cmd_bit_count <= (others => '0');
    end if;
  end process rol_test_l1a_source_process;

--  -- begin header generator ----------
	dump_vars:process(dumpIt)
   variable fline: line; -- needs a variable
   variable instructions: integer := 0;
	variable gid: std_logic_vector(7 downto 0);
	begin
		  if dumpit = 1 then
        file_open(mappingFile,"../inc/rodMaster.hxx",WRITE_MODE);
        write(fline,string'("// IBL ROD rodMaster address and bit definitions"));
        writeline(mappingFile,fline);

        write(fline,string'("#ifndef ROD_MASTER_H"));
        writeline(mappingFile,fline);
        write(fline,string'("#define ROD_MASTER_H"));
        writeline(mappingFile,fline);

        write(fline,string'("// This is a generated file, do not edit!"));
        writeline(mappingFile,fline);

        write(fline,string'("#define PPC_VHDL_COMPILE_DATE "));
        write(fline,"""" & compileDate & """");
        writeline(mappingFile,fline);

        write(fline,string'("// Design parameters"));
        writeline(mappingFile,fline);
		  

        write(fline,string'("// The following register addresses sit on top of the EPC area, defined in xparameters.h"));
        writeline(mappingFile,fline);
		  -- include xilinx parameters, if not defined
        write(fline,string'("#ifndef XPAR_XPS_EPC_0_PRH0_BASEADDR"));
        writeline(mappingFile,fline);
        write(fline,string'("#include ""xparameters.h"""));
        writeline(mappingFile,fline);
        write(fline,string'("#endif"));
        writeline(mappingFile,fline);
		  
        write(fline,string'("// Number of EPC address bits is an alternative way to identify the address range"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_EPC_ADDRESS_BITS "));
        write(fline,xps_epc_0_PRH_Addr_pin'length);
        writeline(mappingFile,fline);
        write(fline,string'("// Design identification"));
        writeline(mappingFile,fline);
		  -- just say we're using the hash
        write(fline,string'("#define PPC_HDL_FPGA_HAS_GIT_HASH 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_HDL_FPGA_GIT_HASH """));
		  for i in 0 to gitIdLength - 1 loop
				gid := gitHash(i);
				write(fline,hstr(gid));
			end loop;	
        write(fline,string'(""""));
        writeline(mappingFile,fline);

        write(fline,string'("#define PPC_DESIGN_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(designIdAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// PPC control register"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ctlAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// PPC watchdog. enabled with first write access if enable masks set. tomeout approx 0.3s"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(wdAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_ENABLE_MASK0 "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(wdMaskAddr0)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_ENABLE_MASK1 "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(wdMaskAddr1)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_ENABLE_VAL0 "));
        write(fline,"0x" & str(conv_integer(unsigned(wdMaskVal0)),16));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_ENABLE_VAL1 "));
        write(fline,"0x" & str(conv_integer(unsigned(wdMaskVal1)),16));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_WDOG_TIMEOUT "));
        write(fline,"0x" & str(conv_integer(wdTimeout),16));
        write(fline,string'("  // 25 ns units"));
        writeline(mappingFile,fline);
		  
        write(fline,string'("// Master: PPC is master if 1, else DSP is master"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_MASTER_BIT "));
        write(fline,ctlMasterBit);
        writeline(mappingFile,fline);
        write(fline,string'("// HPI enable: HPI port is activated if 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_HPIENABLE_BIT "));
        write(fline,ctlHpiBit);
        writeline(mappingFile,fline);
        write(fline,string'("// Serial port local link: local link implementation selected if 1, else EDK peripheral version (Bologna)"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_SPLOCLINK_BIT "));
        write(fline,ctlLlSportBit);
        writeline(mappingFile,fline);
        write(fline,string'("// Invert serial port clock if 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_SPCLKINV_BIT "));
        write(fline,ctlSportInvertBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_UARTA_BIT "));
        write(fline,ctlSlaveAUartBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_CTL_UARTB_BIT "));
        write(fline,ctlSlaveBUartBit);
        writeline(mappingFile,fline);

        write(fline,string'("// PPC status register"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_STAT_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(statAddr)),16)&")");
        writeline(mappingFile,fline);

        write(fline,string'("// PPC rod busy registers"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASK_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyMaskAddr)),16)&")");
        writeline(mappingFile,fline);
	write(fline,string'("#define PPC_BUSY_MASTER_STATUS_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyMasterStatusAddr)),16)&")");
        writeline(mappingFile,fline);
	write(fline,string'("#define PPC_BUSY_MASTER_BUSYNINP0_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyBusyNinP0HistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_BUSYNINP1_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyBusyNinP1HistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_FORMATTER0_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyFormatter0HistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_FORMATTER1_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyFormatter1HistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyHeaderPauseHistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyRolTestPauseHistAddr)),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_BUSY_MASTER_OUT_HIST_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyOutHistAddr)),16)&")");
	writeline(mappingFile,fline);

	--------****************----------------------
	write(fline,string'("// PPC UART registers"));
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_S6A_STATUS_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartS6aStatus)),16)&")");
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_S6A_DATA_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartS6aData)),16)&")");
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_S6B_STATUS_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartS6bStatus)),16)&")");
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_S6B_DATA_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartS6bData)),16)&")");
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_MASTER_STATUS_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartMasterStatus)),16)&")");
	writeline(mappingFile,fline);
	write(fline,string'("#define PPC_UART_MASTER_DATA_REG "));
	write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(ppcUartMasterData)),16)&")");
	writeline(mappingFile,fline);
	--------****************----------------------
		  
        write(fline,string'("// Simulation mode if 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_STAT_SIMULATION_BIT "));
        write(fline,statSimBit);
        writeline(mappingFile,fline);
		  
        write(fline,string'("// Serial port mask. Bit 0..7 enable output from local link serial port to SP6A, Bits 8..15 to SP6B. Output goes to XC signals"));
        writeline(mappingFile,fline);
        write(fline,string'("#define PPC_SPORT_REG "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(spMaskAddr)),16)&")");
        writeline(mappingFile,fline);

        write(fline,string'("// Common PPC+DSP registers"));
        writeline(mappingFile,fline);
        write(fline,string'("// Base of common (PPC+DSP) control registers, aka RCF registers"));
        writeline(mappingFile,fline);
        write(fline,string'("// TODO: check if addressing scheme / location has changed"));
        writeline(mappingFile,fline);
        write(fline,string'("#define COMMON_REG_BASE "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & 
        str(conv_integer(ROD_SEL_ADDR) * 2**BOC_SEL_SHIFT + conv_integer(MASTER_INTERNAL_REG) * 2**RODBUS_ADDR_SHIFT + conv_integer(INT_RCF_OFFS) * 2**INT_OFFS_SHIFT ,16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Base of common (PPC+DSP) slave-A registers, aka formatter 0"));
        writeline(mappingFile,fline);
        write(fline,string'("#define SLV_A_REG_BASE "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str(conv_integer(ROD_SEL_ADDR) * 2**BOC_SEL_SHIFT + conv_integer(SLV_NORTH_ADDR) * 2**RODBUS_ADDR_SHIFT ,16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Base of common (PPC+DSP) slave-B registers, aka formatter 4"));
        writeline(mappingFile,fline);
        write(fline,string'("#define SLV_B_REG_BASE "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str(conv_integer(ROD_SEL_ADDR) * 2**BOC_SEL_SHIFT + conv_integer(SLV_SOUTH_ADDR) * 2**RODBUS_ADDR_SHIFT ,16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Base of common (PPC+DSP) BOC registers"));
        writeline(mappingFile,fline);
        write(fline,string'("#define BOC_REG_BASE "));
        write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str(conv_integer(BOC_SEL_ADDR) * 2**BOC_SEL_SHIFT ,16)&")");
        writeline(mappingFile,fline);
		  
		----------------------------------------------------------------------
        -- writeline(mappingFile,fline);

        write(fline,string'("#endif //ROD_MASTER_H"));
        writeline(mappingFile,fline);
        file_close(mappingFile);

		  dumpIt <= 0;
		  end if;
	end process;
	
	--  -- begin header generator ----------
	dump_vars2:process(dumpIt2)
   variable fline: line;
	begin
		  if dumpit2 = 1 then
			  file_open(mappingFile2,"../inc/rodMasterReg.hxx",WRITE_MODE);
			  write(fline,string'("// IBL ROD rodMaster firmware registers"));
			  writeline(mappingFile2,fline);

			  write(fline,string'("#ifndef ROD_MASTER_REG_H"));
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define ROD_MASTER_REG_H"));
			  writeline(mappingFile2,fline);

			  write(fline,string'("// This is a generated file, do not edit!"));
			  writeline(mappingFile2,fline);

			  write(fline,string'("#define PPC_VHDL_COMPILE_DATE "));
			  write(fline,"""" & compileDate & """");
			  writeline(mappingFile2,fline);
			  
			  -- Slave Global Variable Registers
			  write(fline,string'("// Master Global Register Definitions"));
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define MDSP_GPREG "));        
			  write(fline,"(0x" & str(conv_integer(MdspGpreg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DSP_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(DspSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define TEST_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(TestSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define ROD_REV_CODE_REV_REG "));        
			  write(fline,"(0x" & str(conv_integer(RodRevCodeVerReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define RRIF_CTRL_REG "));        
			  write(fline,"(0x" & str(conv_integer(RrifCtrlReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define SPR_CTRL_REG "));        
			  write(fline,"(0x" & str(conv_integer(SprCtrlReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define ROD_MODE_REG "));        
			  write(fline,"(0x" & str(conv_integer(RodModeReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_CMD_MASK_SEL_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeCmdMaskSelReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define RRIF_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(RrifSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define SPR_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(SpareSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define EMPTY_REG0 "));        
			  write(fline,"(0x" & str(conv_integer(EmptyReg0),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define EMPTY_REG1 "));        
			  write(fline,"(0x" & str(conv_integer(EmptyReg1),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_CMD_MASK0_LSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeCmdMask0LsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_CMD_MASK0_MSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeCmdMask0MsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_CMD_MASK1_LSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeCmdMask1LsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_CMD_MASK1_MSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeCmdMask1MsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_STRB_DLY_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalStrbDlyReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_CMD_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalCommandReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define ECR_CNT_I_REG "));        
			  write(fline,"(0x" & str(conv_integer(EcrCountIReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define ECR_CNT_IN_REG "));        
			  write(fline,"(0x" & str(conv_integer(EcrCountInReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define MB_FIFO_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(MbFifoSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DYN_MASK_STTS_LSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(DynMaskSttsLsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DYN_MASK_STTS_MSB_REG "));        
			  write(fline,"(0x" & str(conv_integer(DynMaskSttsMsbReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INMEM_WORD_CNT_REG "));        
			  write(fline,"(0x" & str(conv_integer(InMemWordCntReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DBG_MEM_WORD_CNT_REG "));        
			  write(fline,"(0x" & str(conv_integer(DbgMemWordCntReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CNFG_RDBACK_CNT_REG "));        
			  write(fline,"(0x" & str(conv_integer(CnfgRdbackCntReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DEBUG_MODE_CNFG_REG "));        
			  write(fline,"(0x" & str(conv_integer(DebugModeCnfgReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DEBUG_MODE_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(DebugModeSttsgReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_TIM_FIFO_DATA_REG "));        
			  write(fline,"(0x" & str(conv_integer(IntTimFifoDataReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_TO_SLV_DSPS_REG "));        
			  write(fline,"(0x" & str(conv_integer(IntToSlvDspsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define SLV_DSP_INT_REG "));        
			  write(fline,"(0x" & str(conv_integer(SlvDspIntReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define TEST_IRQ_REG "));        
			  write(fline,"(0x" & str(conv_integer(TestIrqReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_OCC_RES_REG "));        
			  write(fline,"(0x" & str(conv_integer(FeOccResReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_OCC_NUM_L1_REG0 "));        
			  write(fline,"(0x" & str(conv_integer(FeOccNumL1Reg0),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_OCC_NUM_L1_REG1 "));        
			  write(fline,"(0x" & str(conv_integer(FeOccNumL1Reg1),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_OCC_NUM_L1_REG2 "));        
			  write(fline,"(0x" & str(conv_integer(FeOccNumL1Reg2),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define FE_OCC_NUM_L1_REG3 "));        
			  write(fline,"(0x" & str(conv_integer(FeOccNumL1Reg3),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_SCAN_REG0 "));        
			  write(fline,"(0x" & str(conv_integer(IntScanReg0),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_SCAN_REG1 "));        
			  write(fline,"(0x" & str(conv_integer(IntScanReg1),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_SCAN_REG2 "));        
			  write(fline,"(0x" & str(conv_integer(IntScanReg2),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define INT_SCAN_REG3 "));        
			  write(fline,"(0x" & str(conv_integer(IntScanReg3),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_EVNT_TYPE0_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalEvntType0Reg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_EVNT_TYPE1_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalEvntType1Reg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define SCAN_DONE_STTS_REG "));        
			  write(fline,"(0x" & str(conv_integer(ScanDoneSttsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_L1_ID0_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalL1id0Reg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_L1_ID1_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalL1id1Reg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_BCID0_REG "));        
			  write(fline,"(0x" & str(conv_integer(CalBcid0Reg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define L1ID_BITS_REG "));        
			  write(fline,"(0x" & str(conv_integer(L1IdBitsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define DSP_LEDS_REG "));        
			  write(fline,"(0x" & str(conv_integer(DspLedsReg),16)&")");
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define PRM_GEO_ADDR_REG "));        
			  write(fline,"(0x" & str(conv_integer(PrmGeoAddrReg),16)&")");		--
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_PULSE_COUNT0 "));        
			  write(fline,"(0x" & str(conv_integer(CalPulseCount0Reg),16)&")");		--
			  writeline(mappingFile2,fline);
			  write(fline,string'("#define CAL_PULSE_COUNT1 "));        
			  write(fline,"(0x" & str(conv_integer(CalPulseCount1Reg),16)&")");		--
			  writeline(mappingFile2,fline);
			  
--			  write(fline,string'("// PPC rod busy registers"));
--        		  writeline(mappingFile2,fline);
--        		  write(fline,string'("#define PPC_BUSY_MASK_REG "));
--        		  write(fline,"(0x" & str(conv_integer(busyMaskAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyMaskAddr)),16)&")");
--        		  writeline(mappingFile2,fline);
--			  write(fline,string'("#define PPC_BUSY_MASTER_STATUS_REG "));
--     			  write(fline,"(0x" & str(conv_integer(busyMasterStatusAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyMasterStatusAddr)),16)&")");
--    			  writeline(mappingFile2,fline);		
--			  write(fline,string'("#define PPC_BUSY_MASTER_BUSYNINP0_HIST_REG "));
--     			  write(fline,"(0x" & str(conv_integer(busyBusyNinP0HistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyBusyNinP0HistAddr)),16)&")");
--     			  writeline(mappingFile2,fline);
--     			  write(fline,string'("#define PPC_BUSY_MASTER_BUSYNINP1_HIST_REG "));
--     			  write(fline,"(0x" & str(conv_integer(busyBusyNinP1HistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyBusyNinP1HistAddr)),16)&")");
--    			  writeline(mappingFile2,fline);
--    			  write(fline,string'("#define PPC_BUSY_MASTER_FORMATTER0_HIST_REG "));
--    			  write(fline,"(0x" & str(conv_integer(busyFormatter0HistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyFormatter0HistAddr)),16)&")");
--			  writeline(mappingFile2,fline);
--    			  write(fline,string'("#define PPC_BUSY_MASTER_FORMATTER1_HIST_REG "));
--    			  write(fline,"(0x" & str(conv_integer(busyFormatter1HistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyFormatter1HistAddr)),16)&")");
--    			  writeline(mappingFile2,fline);
--    			  write(fline,string'("#define PPC_BUSY_MASTER_HEADERPAUSE_HIST_REG "));
--    			  write(fline,"(0x" & str(conv_integer(busyHeaderPauseHistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyHeaderPauseHistAddr)),16)&")");
--			  writeline(mappingFile2,fline);
--    			  write(fline,string'("#define PPC_BUSY_MASTER_ROLTESTPAUSE_HIST_REG "));
--    			  write(fline,"(0x" & str(conv_integer(busyRolTestPauseHistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyRolTestPauseHistAddr)),16)&")");
--    			  writeline(mappingFile2,fline);
--    			  write(fline,string'("#define PPC_BUSY_MASTER_OUT_HIST_REG "));
--    			  write(fline,"(0x" & str(conv_integer(busyOutHistAddr),16)&")");		--write(fline,"(XPAR_XPS_EPC_0_PRH0_BASEADDR + 0x" & str((2**(xps_epc_0_PRH_Addr_pin'length - 1) + conv_integer(busyOutHistAddr)),16)&")");
--    			  writeline(mappingFile2,fline);
			  
			  
			  
			  

			  write(fline,string'("#endif //ROD_MASTER_REG_H"));
			  writeline(mappingFile2,fline);				
			  file_close(mappingFile2);
			  
			  dumpIt2 <= 0;
			end if;
	end process;

  -- end header generator ------------

	-- 20140214 AKU: the reset now inludes a watchdoc reset

	-- wdog trigger counter
	process(clk40in, master_reset, wdTrig)
	begin
		if master_reset = '1' then 
			wdogTiggerCnt <= (others => '0');
		elsif rising_edge(clk40in) then
			if wdTrig = '1' then
				wdogTiggerCnt <= wdogTiggerCnt + 1;
			end if;
		end if;
	end process;

	-- wd enable synchronizer
	process(clk40in, wdEnable)
	begin
		if wdEnable = '1' then 
			wdEnable_r0 <= '1';
			wdEnable_r1 <= '0';
		elsif rising_edge(clk40in) then
			wdEnable_r1 <= wdEnable_r0;
			if wdEnable_r1 = '1' then
				wdEnable_r1 <= '0';
				wdEnable_r0 <= '0';
			end if;
		end if;
	end process;
	-- watchdog state machine 
	process(clk40in, master_reset, wdEnable_r1)
	begin
		if master_reset = '1' then 
			wdState <= wdIdle;
			wdReset <= '0';
			wdTrig <= '0';
		elsif rising_edge(clk40in) then
		
			case wdState is 
				when wdIdle =>
					wdReset <= '0';
					wdTrig <= '0';
					-- move to active only when masks sets
					if wdEnable_r1 = '1' and (wdMask0 = wdMaskVal0) and (wdMask1 = wdMaskVal1) then 
						wdMainCnt <= (others => '0');
						wdState <= wdActive;
					end if;
					
				when wdActive => 
					wdReset <= '0';
					wdTrig <= '0';
					if wdEnable_r1 = '1' then 
						wdMainCnt <= (others => '0');
					elsif wdMainCnt < wdTimeout then
						wdMainCnt <= wdMainCnt + 1;
					else
						wdRstCnt <= (others => '0');
						wdState <= wdDone;
					end if;
					
				when wdDone =>
					wdReset <= '1';
					wdTrig <= '0';
					if wdRstCnt < wdMaxRstCnt then
						wdRstCnt <= wdRstCnt + 1;
					else
						wdState <= wdIdle;
						wdTrig <= '1';
					end if;
				
			end case;

		end if;
	end process;
	
	-- combine external and wd reset
	master_reset_int <= master_reset or wdReset;
	
	-- push wdReset to PROM
	prm_ctrl_out(ctl_to_prm_wdog_bit) <= wdReset;

  -- reset block
  rstProc: process
  begin
	wait until rising_edge(clk40in);
	rst0 <= master_reset_int;
	fpga_0_rst_1_sys_rst_pin <= rst0 and ctrl_from_prm(6);
  end process;

  
  -- slave serial ports
  -- copy data from host
  uartToS6a <= fpga_0_RS232_RX_pin;
  uartToS6b <= fpga_0_RS232_RX_pin;
  fpga_0_RS232_TX_pin <= uartFromS6a when ctlReg(ctlSlaveAUartBit) = '1' else 
                         uartFromS6B when ctlReg(ctlSlaveBUartBit) = '1' else uartFromMaster;
  
--------------------------------------------------------------------------
-- COMPONENT INSTATIATION
--------------------------------------------------------------------------

	event_id_trigger_processor_instance: event_id_trigger_processor
	port map(
		clk            			  => clk40in,        
		rst            			  => master_reset,     
		rrif_control_register_in  => rrif_ctrl_reg_i(22 downto 8), 
		tim_signals_in            => tim_signal_ii(5 downto 4),
		cal_serial0_in 			  => cal_serial0_i,     
		cal_serial1_in 			  => cal_serial1_i,   
		l1_accept0_out 		  	  => cal_l1a0_i,
		l1_accept1_out 		  	  => cal_l1a1_i,
		l1id0_reg_in   		  	  => static_l1id0_i,
		l1id1_reg_in   		  	  => static_l1id1_i,
		l1id0_out      			  => cal_l1id0_i,
		l1id1_out      			  => cal_l1id1_i,
		calpulse_count0_out		  => calpulse_count0,
		calpulse_count1_out		  => calpulse_count1,
		bcid0_reg_in   			  => static_bcid0_i,
		bcid1_reg_in   			  => static_bcid1_i,
		bcid0_out      			  => cal_bcid0_i,
		bcid1_out      			  => cal_bcid1_i,
		l1id_bits_out        	  => l1id_bits_i,
		bcid_bits_out         	  => bcid_bits_i,
		ttype_bits_out            => ttype_bits_i,
		new_l1id_valid_out  	     => new_l1id_valid_i,
		new_ttype_valid_out       => new_ttype_valid_i
	);
	
  cal_l1a_i <= cal_l1a1_i & cal_l1a0_i;
--  cal_serial0_i <= (controller_dx_i(0) AND NOT mask_dxi_i) OR 
--                   (rol_l1a_i          AND NOT rol_test_block_i(2)) OR
--                   (int_scan_fecmd0_i  AND     mask_dxi_i);
--  cal_serial1_i <= (controller_dx_i(1) AND NOT mask_dxi_i) OR 
--                   (int_scan_fecmd1_i  AND     mask_dxi_i);

  cal_serial0_i <= (sport0Data(0)      AND NOT mask_dxi_i) OR 
                   (rol_l1a_i          AND NOT rol_test_block_i(2)) OR
                   (int_scan_fecmd0_i  AND     mask_dxi_i);
  cal_serial1_i <= (sport1Data(0)      AND NOT mask_dxi_i) OR 
                   (int_scan_fecmd1_i  AND     mask_dxi_i);
						 
  fe_command_processor_instance: fe_command_processor
	 port map (
    clk                       => clk40in,
    rst                       => master_reset,
	 rrif_control_register_in  => rrif_ctrl_reg_i(23 downto 0), -- input
    tim_signals_in            => tim_signal_ii(3 downto 0), -- input
	 rrif_status_register_out  => rrif_stts_reg_i(29 downto 22), -- input
	 spare_status_register_out => spare_stts_reg_i(18 downto 8), -- input
    corrective_fe_pulse_out   => corrective_fe_pulse_i, -- output
    calibrate_command_in      => cal_command_reg_i, --
    cal_trig_delay_in         => calstrobe_delay_reg_i, --
    fe_command_mask0_in       => fe_command_mask0_i(15 downto 0), -- temperarily trunctated >>>>>>>>>>>>>>>>>>>>
    fe_command_mask1_in       => fe_command_mask1_i(15 downto 0), -- temperarily trunctated >>>>>>>>>>>>>>>>>>>>
    long_command_stream_in    => controller_dx_i, --
    long_command_pulse_in     => cal_l1a_i, --
    command_stream_out        => fe_command_link_out_p, 
    pulse_stream_out          => fe_l1_pulse_i(15 downto 0), -- temperarily trunctated >>>>>>>>>>>>>>>>>>>> should actually be 32 bits fix
    fe_cmd_mask_updated_out   => fe_cmd_mask_updated_i, --
    fmt_fe_cmd_out            => fmt_fe_cmd_i, --
    fe_cmdpulse_AB_out        => fe_cmdpulse_formattersAB_out_i, --
	 controller_int_out        => apply_dynmask_mb_i(1 downto 0), --
    command_cycle_done_out    => cmd_cycle_done_i, --
    store_current_trigger_out => store_trig2ti_que_i, --
    current_trigger_out       => trigger_descriptor_i, --
    dynamic_mask_sent_in      => read_trigger_que_i, --
    modebits_sent_in          => modebits_sent_i, --
    efb_ready_in              => efb_edm_fef_n_in_p,
    modebits_error_out        => pin_hold_reg_i(30) --
    );

  
  formatter_readoutmodebits_encoder_instance: formatter_readoutmodebits_encoder
    port map(
      clk                      => clk40in,
      rst                      => master_reset,
      control_in               => rrif_ctrl_reg_i(10),
      int_lut_adr_in           => frebus_a_ii,
      int_lut_bdata_in         => frebus_dout_i,
      int_lut_bdata_out        => frebus_din_i,
      ce_n_in                  => frebus_en_n_i,
      rnw_in                   => frebus_rnw_i,
      ack_out                  => frebus_ack_i,
      apply_default_mode_in    => apply_dynmask_mb_i(0),
      apply_corrective_mode_in => apply_dynmask_mb_i(1),
      modebits_fifo_ff_n_in    => formatter_mb_fff_n_in,
      modebits_out             => formatter_mb_data_out(11 downto 0),
      modebits_fifo_we_n_out   => formatter_mb_we_n_out_p(7 downto 0),
      int_fifo_ef_out          => modebits_fifo_stts_reg_i(1 downto 0),
      int_fifo_ff_out          => modebits_fifo_stts_reg_i(3 downto 2),
      mb0_fifo_occ_count       => modebits_fifo_stts_reg_i(13 downto  4),
      mb1_fifo_occ_count       => modebits_fifo_stts_reg_i(23 downto 14),
      modebits_sent_out        => modebits_sent_i,
      fe_cmd_mask_sel_in       => fe_cmd_mask_sel_i
      );
  frebus_a_ii <= unsigned(frebus_a_i);
  
   efb_headerdynamicmask_encoder_instance : efb_headerdynamicmask_encoder
    port map  (
      clk                            => clk40in,
      rst	                         => master_reset,
      control_in                     => rrif_ctrl_reg_i(12),
      rnw_in                         => edebus_rnw_i,
      ce_n_in                        => edebus_en_n_i,
      ack_out                        => edebus_ack_i,
      apply_default_mask_in          => apply_dynmask_mb_i(0),
      apply_corrective_mask_in       => apply_dynmask_mb_i(1),
      record_headerdynamicmask_in    => trap_indicator_i(1),
      read_trig_indictor_que_out     => read_trigger_que_i,
      l1id_bits_in                   => l1id_bits_i,
      bcid_bits_in                   => bcid_bits_i,
      ttype_bits_in                  => ttype_bits_i,
      new_l1id_valid_in              => new_l1id_valid_i,
      new_ttype_valid_in             => new_ttype_valid_i,
      int_lut_bdata_in               => edebus_dout_i,
      int_lut_bdata_out              => edebus_din_i,
      int_lut_adr_in                 => edebus_a_ii,	
		event_headermask_fifo_ef_n_in  => efb_edm_fef_n_in_p,
		event_headermask_fifo_ff_n_in  => efb_edm_fff_n_in_p,
      event_headermask_out           => efb_edm_data_out_p,
      event_lbid_count_out           => dynmmask_fifo_stts_reg_i(17 downto 10),
      event_trgt_count_out           => dynmmask_fifo_stts_reg_i(25 downto 18),
      event_mask_count_out           => dynmmask_fifo_stts_reg_i(33 downto 26),
      event_header_count_out         => dynmmask_fifo_stts_reg_i(41 downto 34),
      event_headermask_fifo_we_n_out => efb_edm_we_n_out_p,
      int_fifo_ef_out                => dynmmask_fifo_stts_reg_i(4 downto 0),
      int_fifo_ff_out                => dynmmask_fifo_stts_reg_i(9 downto 5),
      ecrid_in                       => ecr_id,
      int_cal_mode_en_in             => rrif_ctrl_reg_i(18),
      cal_l1a0_in                    => cal_l1a0_i,
      cal_l1a1_in                    => cal_l1a1_i,
      cal_tt0_in                     => cal_event_type0_i,
      cal_tt1_in                     => cal_event_type1_i,
      cal_l1id0_in                   => cal_l1id0_m,   --cal_l1id0_i,  -- nevent_o
      cal_l1id1_in                   => cal_l1id1_m,   --cal_l1id1_i,  -- nevent_o
      cal_bcid0_in                   => cal_bcid0_i,
      cal_bcid1_in                   => cal_bcid1_i,
      pause_triggers_out             => efb_header_pause_i,
      event_type_in                  => fe_cmd_mask_sel_i,
      load_event_type_in             => int_load_event_i,
      mask_dxi_in                    => mask_dxi_i,
      nevent_in                      => nevent_o,
      boc_ok_in                      => boc_clock_ok_p,
      tim_ok_in                      => tim_clock_ok_p,
      rol_test_in                    => rol_test_i,
      rol_test_count_in              => rol_test_block_count_i,
      rod_tt_wr_out                  => rod_tt_wr_i,
      event_l1id_out                 => event_l1id
      );
  edebus_a_ii <= unsigned(edebus_a_i);
	
  debugmem_fifo_interface_instance : debugmem_fifo_interface
    port map (
      rst               => master_reset,
      clk               => clk40in,
      control_in        => rrif_ctrl_reg_i(25),
      address_in        => dfibus_a_i(6 downto 0),
      rnw_in            => dfibus_rnw_i,
      inmem_en_n_in     => dfibus_inmem_en_n_i,
      eventmem_en_n_in  => dfibus_eventmem_en_n_i,
      inmem_ctrl_out    => inmem_ctrl_i(15 downto 0),
      eventmem_ctrl_out => eventmem_ctrl_i(10 downto 0),
      ack_out           => dfibus_ack_i
      );

  fifo_2048x2_instance : fifo_2048x2  -- FE Trigger FIFO
    port map (
      clk                       => clk40in,
      rst                       => master_reset,
      wr_en                     => store_trig2ti_que_i,
      rd_en                     => read_trigger_que_i,
      din                       => trigger_descriptor_i,
      dout                      => trap_indicator_i,
      full                      => rrif_stts_reg_i(6),
      empty                     => rrif_stts_reg_i(5),
      data_count                => open
      );

  front_end_occupancy_counters_instance : front_end_occupancy_counters
    port map  (
      clk                           => clk40in,
      rst                           => master_reset,
      trailerflags_readout_en_in    => rrif_ctrl_reg_i(3),
      trailerflags_in               => formatter_writes_trailer_in_p,
      clear_fe_occupancy_counter_in => feocc_reset_i,
      num_l1_value_in               => feocc_num_l1_i,
      fe_l1_pulse_in                => fe_l1_pulse_i,
      fe_occupancy_gl_zero_out      => rrif_stts_reg_i(30),
--      group_zero_out                => feocc_zero_i,
--      group_en_out                  => act_group_i,
      show_trailer_flags_out        => show_trailer_flags_out_p,
      focbus_a_in                   => focbus_a_i,
      focbus_d_in                   => regbus_d_out_i,
      focbus_d_out                  => focbus_din_i,
      focbus_rnw_in                 => focbus_rnw_i,
      focbus_en_in                  => focbus_en_i,
      focbus_ack_out                => focbus_ack_i
      );

  testbench_interface_instance : testbench_interface
    port map(
      rst                          => master_reset,
      clk                          => clk40in,
      control_in                   => rrif_ctrl_reg_i(17 downto 16),
      debugmode_cnfg_register_in   => ide_debugmem_cnfg_reg_i,
      debugmode_stts_register_out  => ide_debugmem_stts_reg_i,
      inmem_wordcount_register_in  => inmem_wordcount_i,
      dbgmem_wordcount_register_in => dbgmem_wordcount_i,
      inmem_ctrl_out               => tb_inmem_ctrl_out_i,
      int_tim_fifo_re_out          => int_tim_fifo_re_i,
      int_tim_fifo_rt_out          => int_tim_fifo_rt_i,
      l1_trigger_in                => tim_signal_ii(0),
      pause_trigger_rt             => pause_triggers_i
      );

  int_scan_engine_instance : int_scan_engine
    port map(
      clk               => clk40in,
      rst               => master_reset,
      enable_in         => rrif_ctrl_reg_i(29),
      clear_nevent_in   => rrif_ctrl_reg_i(30),
      new_mask_ready_in => rrif_ctrl_reg_i(2),
      rod_tt_wr_in      => rod_tt_wr_i,
      num_events_in     => nevent_i,
      precal_delay_in   => precal_delay_i,
      trig_delay0_in    => trig_delay0_i,
      trig_delay1_in    => trig_delay1_i,
      evt_interval_in   => interval_i,
      act_groups_in     => act_group_i,
      trig_mode_in      => trig_mode_i,
      feocc_zero_in     => feocc_zero_i,
      xoff_in           => pause_triggers_i,
      mask_en_out       => int_mask_en_i,     -- Select Mask LUT
      new_mask_out      => int_new_mask_i,    -- WR new mask to Register
      mask_dxi_out      => mask_dxi_i,
      fecmd0_out        => int_scan_fecmd0_i,
      fecmd1_out        => int_scan_fecmd1_i,
      nevent_out        => nevent_o,    -- L1ID
      nevent_done_out   => int_nevent_done_i
      );

  v5gmac125_i : v5gmac125
    port map (
              fpga_0_RS232_RX_pin => fpga_0_RS232_RX_pin,
              fpga_0_RS232_TX_pin => uartFromMaster, --fpga_0_RS232_TX_pin,
              
              fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin => fpga_0_Hard_Ethernet_MAC_TemacPhy_RST_n_pin,
              fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_MII_TX_CLK_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TXD_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_EN_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_ER_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_TX_CLK_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RXD_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_DV_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_ER_0_pin,
              fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin => fpga_0_Hard_Ethernet_MAC_GMII_RX_CLK_0_pin,
              fpga_0_Hard_Ethernet_MAC_MDC_0_pin => fpga_0_Hard_Ethernet_MAC_MDC_0_pin,
              fpga_0_Hard_Ethernet_MAC_MDIO_0_pin => fpga_0_Hard_Ethernet_MAC_MDIO_0_pin,
              fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin => open, -- fpga_0_Hard_Ethernet_MAC_PHY_MII_INT_pin,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQ,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DQS_N,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_A,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_BA,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_RAS_N,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CAS_N,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_WE_N,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CS_N,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_ODT,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CKE,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_DM,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK,
              fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N_pin => fpga_0_DDR2_SDRAM_W1D32M72R8A_5A_DDR2_CK_N,
              fpga_0_clk_1_sys_clk_pin => fpga_0_clk_1_sys_clk_pin,
              fpga_0_rst_1_sys_rst_pin => fpga_0_rst_1_sys_rst_pin,
              cpurst => cpurst,
              xps_epc_0_PRH_CS_n_pin => xps_epc_0_PRH_CS_n_pin,
              xps_epc_0_PRH_Addr_pin => xps_epc_0_PRH_Addr_pin,
              xps_epc_0_PRH_Rdy_pin => xps_epc_0_PRH_Rdy_pin,     
              xps_epc_0_PRH_Data_I_pin => xps_epc_0_PRH_Data_I_pin,
              xps_epc_0_PRH_Data_O_pin => xps_epc_0_PRH_Data_O_pin,
              cpuclk => cpuclk,
              xps_ll_fifo_0_tx_llink_src_rdy_n_pin => xps_ll_fifo_0_tx_llink_src_rdy_n_pin,
              xps_ll_fifo_0_tx_llink_dest_rdy_n_pin => xps_ll_fifo_0_tx_llink_dest_rdy_n_pin,
              xps_ll_fifo_0_tx_llink_din_pin => xps_ll_fifo_0_tx_llink_din_pin,
              xps_ll_fifo_0_tx_llink_eop_n_pin => xps_ll_fifo_0_tx_llink_eop_n_pin,
              xps_ll_fifo_0_tx_llink_sop_n_pin => xps_ll_fifo_0_tx_llink_sop_n_pin,
              xps_ll_fifo_0_llink_rst_pin => sportReset,
              extirq_0_extIrqAck_pin => irqAck,
              extirq_0_extIrqReq_pin => irqReq,
              xps_epc_0_PRH_Clk_pin => clk40in,
              flash_si => flash_si,
              flash_ss => flash_ss,
              flash_sck => flash_sck,
              flash_so => flash_so,
              hpiport_0_PRM_data_bus_out_pin => hpiport_0_PRM_data_bus_out_pin,
              hpiport_0_PRM_busy_pin => hpiport_0_PRM_busy_pin,
              hpiport_0_PRM_strobe_pin => hpiport_0_PRM_strobe_pin, 
              hpiport_0_PRM_command_pin => hpiport_0_PRM_command_pin, 
              hpiport_0_PRM_done_pin => hpiport_0_PRM_done_pin,
              hpiport_0_PRM_data_valid_pin => hpiport_0_PRM_data_valid_pin,
              hpiport_0_PRM_data_bus_in_pin => hpiport_0_PRM_data_bus_in_pin,
              xps_iic_0_Sda => ddr2_sda,
              xps_iic_0_Scl => ddr2_scl,
				  --
				xps_spi_1_SCK_O_pin => spi1_sck, 
				xps_spi_1_MOSI_O_pin => spi1_mosi, 
				xps_spi_1_SS_O_pin => spi_1_ss,
				xps_spi_1_MISO_I_pin => ctrl_from_prm(ctl_from_prm_spi_miso_bit),
				  --
				xps_gpio_xvc_GPIO_IO_O_pin => xps_gpio_xvc_GPIO_IO_O_pin,
				xps_gpio_xvc_GPIO_IO_I_pin => xps_gpio_xvc_GPIO_IO_I_pin,
				  --
              mcbsp_rod_0_Clkx_pin => clk40in,
              mcbsp_rod_0_Dx_O_pin => mcbsp_rod_0_Dx_O,
              xps_epc_0_PRH_RNW_pin => xps_epc_0_PRH_RNW_pin,
              xps_epc_0_PRH_Wr_n_pin=>open	   
              );

		prm_ctrl_out(ctl_to_prm_spi_sck_bit) <= spi1_sck;
		prm_ctrl_out(ctl_to_prm_spi_mosi_bit) <= spi1_mosi;
		prm_ctrl_out(ctl_to_prm_spi_ss_bit) <= spi_1_ss(0); 

		-- xvc cable mappings like on boc
		prm_ctrl_out(ctl_to_prm_xvc_tck_bit) <= xps_gpio_xvc_GPIO_IO_O_pin(0);
		prm_ctrl_out(ctl_to_prm_xvc_tms_bit) <= xps_gpio_xvc_GPIO_IO_O_pin(1);
		prm_ctrl_out(ctl_to_prm_xvc_tdi_bit) <= xps_gpio_xvc_GPIO_IO_O_pin(2);
		
		xps_gpio_xvc_GPIO_IO_I_pin(0) <= xps_gpio_xvc_GPIO_IO_O_pin(0); -- loopback
		xps_gpio_xvc_GPIO_IO_I_pin(1) <= xps_gpio_xvc_GPIO_IO_O_pin(1); -- loopback
		xps_gpio_xvc_GPIO_IO_I_pin(2) <= xps_gpio_xvc_GPIO_IO_O_pin(2); -- loopback
		xps_gpio_xvc_GPIO_IO_I_pin(3) <= ctrl_from_prm(ctl_from_prm_xvc_tdo_bit);
		--
	
		withSpiCs: if spiDebug generate
		begin	
			spi_icon : icon
			  port map (
				 CONTROL0 => cs_CONTROL0);
			spi_ila: ila8bit
			  port map (
				 CONTROL => cs_CONTROL0,
				 CLK => cpuclk,
				 TRIG0 => cs_TRIG0);
		end generate;
		
		cs_trig0 <= (0 => spi_1_ss(0), 1 => spi1_sck, 2 => spi1_mosi, 3 => ctrl_from_prm(ctl_from_prm_spi_miso_bit), others => '0');

--		withUartCs: if uartDebug generate
--		begin
--			uart_icon: icon
--				port map	(
--					CONTROL0=> uart_cs_CONTROL);
--			uart_ila: uart_ila
--				port map (
--					CONTROL => uart_cs_CONTROL,
--					CLK => clk40in,
--					TRIG0 => uart_cs_trig_master,
--					TRIG1 => uart_cs_trig_s6a,
--					TRIG2 => uart_cs_trig_s6b
--					);
--		end generate;
	
uart_cs_trig_master(0) <= RdEnable_master;
uart_cs_trig_master(8 downto 1) <= ppc_uart_master_data(7 downto 0);
uart_cs_trig_master(23 downto 9) <= ppc_uart_master_status(14 downto 0);
	
uart_cs_trig_s6a(0) <= RdEnable_s6a;
uart_cs_trig_s6a(8 downto 1) <= ppc_uart_s6a_data(7 downto 0);
uart_cs_trig_s6a(23 downto 9) <= ppc_uart_s6a_status(14 downto 0);

uart_cs_trig_s6b(0) <= RdEnable_s6b;
uart_cs_trig_s6b(8 downto 1) <= ppc_uart_s6b_data(7 downto 0);
uart_cs_trig_s6b(23 downto 9) <= ppc_uart_s6b_status(14 downto 0);		

    address_decoder_busbridge_instance : address_decoder_busbridge
    port map (
      rst_n_in           => reset_n,
      clk_in             => clk40in, -- ck,
      dspbus_ea_in       => dspAddr, -- addr(21 downto 2), -- controller_bus_adr_in_p(19 downto 0),
      dspbus_ed_in       => dspDin,  -- xps_epc_0_PRH_Data_O_pin, -- controller_bus_data_in_p(31 downto 0),
      dspbus_ed_out      => dspDout, --controller_bus_data_out_p(31 downto 0),
      dspbus_en_n_in     => dspCe_n, -- mstSelect_n, -- controller_bus_ce_n_in_p,
      dspbus_be_n_in     => dspBe_n, -- "0000", -- controller_bus_be_n_in_p,
      dspbus_rnw_in      => dspRnw, -- xps_epc_0_PRH_RNW_pin, -- controller_bus_rnw_in_p,
      dspbus_re_n_in     => dspRd_n, -- mstRd_n, -- controller_bus_re_n_in_p,
      dspbus_oe_n_in     => dspOe_n, -- mstOe_n, -- controller_bus_oe_n_in_p,
      dspbus_ack_out     => dspAck, -- mstRdy, -- controller_bus_ack_out_i,
      rodbus_a_out       => rod_bus_adr_out_p(19 downto 0),
      rodbus_d_in        => rod_bus_data_in_p(31 downto 0),
      rodbus_d_out       => rod_bus_data_out_p(31 downto 0),
      rodbus_en_n_out    => rod_bus_ce_n_out_p(10 downto 0),
      rodbus_en_n_itr    => rodbus_en_n_itr_i(6 downto 0),
      rodbus_rnw_out     => rod_bus_rnw_out_p,
      formA_ack_n_in     => formA_ack_n_in_p,
      formB_ack_n_in     => formB_ack_n_in_p,
      efb_ack_in         => efb_ack_in_p,
      router_ack_in      => router_ack_in_p,
      bocbus_busy_in     => bocbus_busy_in, -- _p,
      bocbus_strb_out    => bocbus_strb_out, -- _p,
      bocbus_rodoe_out   => bocbus_rodoe_out, -- _p,
      bocbus_write_out   => bocbus_write_out, -- _p,
      regbus_a_out       => regbus_a_i(11 downto 0),
      regbus_d_in        => regbus_d_in_i(31 downto 0),
      regbus_d_out       => regbus_d_out_i(31 downto 0),
      regbus_rnw_out     => regbus_rnw_i, 
      regbus_ack_in      => regbus_ack_i,
      fcmbus_d_in        => fcmbus_dout_i,
      focbus_a_out       => focbus_a_i(11 downto 0),
      focbus_d_in        => focbus_din_i,
      focbus_rnw_out     => focbus_rnw_i,
      focbus_ack_in      => focbus_ack_i,
      fmebus_a_out       => frebus_a_i,
      fmebus_d_in        => frebus_din_i(15 downto 0),
      fmebus_d_out       => frebus_dout_i(15 downto 0),
      fmebus_rnw_out     => frebus_rnw_i,
      fmebus_ack_in      => frebus_ack_i,
      edebus_a_out       => edebus_a_i,
      edebus_d_in        => edebus_din_i(15 downto 0),
      edebus_d_out       => edebus_dout_i(15 downto 0),
      edebus_rnw_out     => edebus_rnw_i,
      edebus_ack_in      => edebus_ack_i,
      dfibus_a_out       => dfibus_a_i,
      dfibus_rnw_out     => dfibus_rnw_i,
      dfibus_ack_in      => dfibus_ack_i,
      fmt_hwob_out       => fmt_hwob_p,
      fmt_ds_out         => fmt_ds_p,
      efb_hwob_out       => spare_o(22),
      extbus_ack_out     => extbus_ack_i,
      intbus_ack_out     => intbus_ack_i
      );

  register_block_instance: register_block
    port map (
      clk_in                     => clk40in, -- ck,
      rst_n_in                   => reset_n,
      dspLeds 						 => fpga_0_LEDS_GPIO_IO_O_pin,
      regbus_en_n_in             => regbus_en_n_i,
      regbus_rnw_in              => regbus_rnw_i,
      regbus_a_in                => regbus_a_i(11 downto 0),
      regbus_d_in                => regbus_d_out_i,
      regbus_d_out               => regbus_d_in_i,
      regbus_ack_out             => regbus_ack_i,
      fcmbus_en_n_in             => fcmbus_en_n_i,
      fcmbus_d_out               => fcmbus_dout_i,
      rrif_ctrl_reg_out          => rrif_ctrl_reg_i,
      spare_ctrl_reg_out         => spare_ctrl_reg_i,
      rrif_stts_reg_in           => rrif_stts_reg_i,
      spare_stts_reg_in          => spare_stts_reg_i,
      dsp_stts_reg_in            => dsp_stts_reg_i,
      test_stts_reg_in           => test_stts_reg_i,
      fecmd_link_mask0_out       => fe_command_mask0_i,
      fecmd_link_mask1_out       => fe_command_mask1_i,
      calstrobe_delay_reg_out    => calstrobe_delay_reg_i,
      cal_command_reg_out        => cal_command_reg_i,
      modebits_fifo_stts_reg_in  => modebits_fifo_stts_reg_i,
      dynmmask_fifo_stts_reg_in  => dynmmask_fifo_stts_reg_i,
      inmem_wordcount_reg_out    => inmem_wordcount_i,
      dbgmem_wordcount_reg_out   => dbgmem_wordcount_i,
      cnfg_rdback_count_reg_out  => cnfg_rdback_count_i,
      ide_debugmem_stts_reg_in   => ide_debugmem_stts_reg_i,
      ide_debugmem_cnfg_reg_out  => ide_debugmem_cnfg_reg_i,
      debug_fifo_stts_in         => debug_fifo_stts_i,
      rol_test_block_reg_out     => rol_test_block_i,
      rol_test_block_count_out   => rol_test_block_count_i,  
		rol_test_out					=> rol_test_i,
      int_tim_fifo_outdata_out   => int_tim_fifo_outdata_i,
      int_tim_fifo_re_in         => int_tim_fifo_re_i,
      int_tim_fifo_rt_in         => int_tim_fifo_rt_i,
      slave_dsp_ints_out         => open,
      slave_dsp_ints_in          => slave_dsp_ints_in_i,
      sdsp_inttohost_signal_out  => slv_int_to_mdsp_i,
      feocc_reset_reg_out        => feocc_reset_i,
      feocc_num_l1_reg_out       => feocc_num_l1_i,
      cal_event_type0_out        => cal_event_type0_i,
      cal_event_type1_out        => cal_event_type1_i,
      cal_l1id0_in               => cal_l1id0_m,   --cal_l1id0_i,
      cal_l1id1_in               => cal_l1id1_m,   --cal_l1id1_i,
		calpulse_count0_in			=> calpulse_count0,
		calpulse_count1_in			=> calpulse_count1,
      static_l1id0_out           => static_l1id0_i,
      static_l1id1_out           => static_l1id1_i,
      cal_bcid0_in               => cal_bcid0_i,
      cal_bcid1_in               => cal_bcid1_i,
      static_bcid0_out           => static_bcid0_i,
      static_bcid1_out           => static_bcid1_i,
      rod_mode_out               => open,
      test_mux_out               => open,      
      fe_cmd_mask_sel_out        => fe_cmd_mask_sel_i,
      fe_cmd_mask_updated_in     => fe_cmd_mask_updated_i,
      corrective_mask_sent_in    => corrective_fe_pulse_i,
      send_int_test_out          => send_int_test_i,
      test_irq_out               => test_irq_i,
      ecr_count_in               => std_logic_vector(ecr_id),
      ecr_count_out              => ecr_count_i,
      nevent_out                 => nevent_i,
      precal_delay_out           => precal_delay_i,
      trig_delay0_out            => trig_delay0_i,
      trig_delay1_out            => trig_delay1_i,
      interval_out               => interval_i,
      trig_mode_out              => trig_mode_i,
      mask_en_in                 => int_mask_en_i,  -- Select Mask LUT
      new_mask_in                => int_new_mask_i, -- WR new mask to Register
      load_event_type_out        => int_load_event_i,
      int_nevent_done_in         => int_nevent_done_i,
		prm_ga							=> ctrl_from_prm(12 downto 8), -- no longer needed once SPI functional
      l1id_bits_in               => event_l1id
      );
		
		
		
		
	rod_master_busy_instance: rod_master_busy
		port map ( 
			clk40in 				=> clk40in,
         		rod_busy_n_in_p 			=> rod_busy_n_in_p,
         		efb_header_pause_i			=> efb_header_pause_i,
         		formatter_mb_fef_n_in_p			=> formatter_mb_fef_n_in_p,
         		spare_ctrl_reg_i			=> spare_ctrl_reg_i(10),
         		rol_test_pause_i			=> rol_test_pause_i,
	 		reset					=> master_reset, -- is this the major rod master reset?
	 		reset_hists				=> rod_busy_mask_register(16),
	 		busy_mask				=> rod_busy_mask_register(7 downto 0), 
         		force_input				=> rod_busy_mask_register(15 downto 8),
	 		rod_busy_n_out_p			=> rod_busy_n_out_p,
         		current_busy_status			=> rod_busy_master_status_register(7 downto 0),
         		rod_busy_n_in_p_0_hist			=> rod_busy_n_in_p_0_hist_register(15 downto 0),
         		rod_busy_n_in_p_1_hist			=> rod_busy_n_in_p_1_hist_register(15 downto 0),
         		formatter_mb_fef_n_in_p_0_hist		=> formatter_mb_fef_n_in_p_0_hist_register(15 downto 0),
         		formatter_mb_fef_n_in_p_1_hist		=> formatter_mb_fef_n_in_p_1_hist_register(15 downto 0),
         		efb_header_pause_i_hist			=> efb_header_pause_i_hist_register(15 downto 0),
         		rol_test_pause_i_hist			=> rol_test_pause_i_hist_register(15 downto 0),
         		rod_busy_n_out_p_hist			=> rod_busy_n_out_p_hist_register(15 downto 0)
			); 
			 
	uart_s6A_instance: ppc_uart
		port map ( 
				clk						=>	clk40in,
				reset						=>	master_reset,
				serial_data_in			=>	uartFromS6a,
--				serial_data_out		=>,
				write_data				=>OPEN,
				write_en					=>OPEN,
--				write_fifo_wr_count	=>,
--				write_fifo_full		=>,
				read_data				=>	ppc_uart_s6a_data (7 downto 0),
				read_en					=>	RdEnable_s6a,
--				read_fifo_empty		=>,
				read_fifo_rd_count	=>	ppc_uart_s6a_status(14 downto 0)
		);
		
	uart_s6B_instance: ppc_uart
		port map ( 
				clk						=>	clk40in,
				reset						=>	master_reset,
				serial_data_in			=>	uartFromS6b,
--   			serial_data_out		=>,
				write_data				=>OPEN,
				write_en					=>OPEN,
--				write_fifo_wr_count	=>,
--				write_fifo_full		=>,
				read_data				=>	ppc_uart_s6b_data (7 downto 0),
				read_en					=>	RdEnable_s6b,
--				read_fifo_empty		=>,
				read_fifo_rd_count	=>	ppc_uart_s6b_status(14 downto 0)
		);
	uart_master_instance: ppc_uart
		port map ( 
				clk						=>	clk40in,
				reset						=>	master_reset,
				serial_data_in			=>	uartFromMaster,
--				serial_data_out		=>,
				write_data				=>OPEN,
				write_en					=>OPEN,
--				write_fifo_wr_count	=>,
--				write_fifo_full		=>,
				read_data				=>	ppc_uart_master_data (7 downto 0),
				read_en					=>	RdEnable_master,
--				read_fifo_empty		=>,
				read_fifo_rd_count	=>	ppc_uart_master_status(14 downto 0)
		);

--rod_busy_n_out_p <= '1';
		
	-- git id

	theGitId: gitId PORT MAP(
		reset => gitIdReset,
		read => gitIdRead,
		id => gitIdByte
	);

		process
		begin
			wait until rising_edge(cpuclk);
			-- write signal => reset
			if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and 
						xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = designIdAddr then 
					gitIdReset <= '1';
			else 
					gitIdReset <= '0';
			end if;
			-- read signal
			if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '1' and 
						xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = designIdAddr then 
					gitIdRead <= '1';
			else 
					gitIdRead <= '0';
			end if;
		end process;
  -- status register
  simStat: if simulation generate
  begin
    statReg(statSimBit) <= '1';
  end generate;
  
  impStat: if not simulation generate
  begin
    statReg(statSimBit) <= '0';
  end generate;
  
  -- select master interface (= DSP registers) with lower range
  mstSelect_n <= '1' when (xps_epc_0_PRH_CS_n_pin = '1') or 
                 xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1) = '1' else '0';

  -- select PPC special registers with upper range
  ppcSelect <= '1' when (xps_epc_0_PRH_CS_n_pin = '0') and 
               xps_epc_0_PRH_Addr_pin(xps_epc_0_PRH_Addr_pin'length - 1) = '1' else '0';
  

  ctlWrProc_process: process
  begin
    wait until rising_edge(clk40in);
    if fpga_0_rst_1_sys_rst_pin = '1' then
      ctlReg <= (others => '0');
    elsif ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ctlAddr then
      ctlReg <= xps_epc_0_PRH_Data_O_pin;
    end if;
  end process ctlWrProc_process;
  
  ppcIsController <= ctlReg(ctlMasterBit);
  
  -- serial port mask. disable with mask bit = 1
  spMaskWrProc_process: process
  begin
    wait until rising_edge(clk40in);
    if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = spMaskAddr then
      sportMask <= xps_epc_0_PRH_Data_O_pin;
    end if;
  end process spMaskWrProc_process; 

  -- busy mask
  bsyMaskWrProc_process: process
  begin
    wait until rising_edge(clk40in);
	 if master_reset_int = '1' then
      rod_busy_mask_register <= (others => '1');
    elsif ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyMaskAddr then
      rod_busy_mask_register <= xps_epc_0_PRH_Data_O_pin;
    end if;
  end process;

	-- wdog enable mask
	wdWrMaskProc: process
	begin
		wait until rising_edge(clk40in);
		if master_reset_int = '1' then
			wdMask0 <= (others => '0');
			wdMask1 <= (others => '0');
		else
			if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = wdMaskAddr0 then
				wdMask0 <= xps_epc_0_PRH_Data_O_pin;
			elsif ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = wdMaskAddr1 then
				wdMask1 <= xps_epc_0_PRH_Data_O_pin;
			end if;
		end if;
	end process;

	wdWrProc: process
	begin
		wait until rising_edge(clk40in);
		if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '0' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = wdAddr then
			wdEnable <= '1';
		else
			wdEnable <= '0';
		end if;
	end process;
	
	--Read Enable signal generation for PPC-UART
	UartRdEnProc: process
	begin
		wait until rising_edge(clk40in);

		if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6aData then
			RdEn_s6a 	<= '1';
		else
			RdEn_s6a 	<= '0';
		end if;
		if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6bData then
			RdEn_s6b 	<= '1';
		else
			RdEn_s6b 	<= '0';
		end if;
		if ppcSelect = '1' and xps_epc_0_PRH_RNW_pin = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartMasterData then
			RdEn_master <= '1';
		else
			RdEn_master <= '0';
		end if;
	end process;

-- Process for generating exactly one cycle ReadEnable for each uart
  S6aUartRdEnProc: process
  begin
		wait until rising_edge(clk40in);
		RdEnS6a_r0<=RdEn_s6a;
		RdEnS6a_r1<=RdEnS6a_r0;
		if RdEnS6a_r1='0' and RdEnS6a_r0='1' then
			RdEnable_s6a <='1';
		else
			RdEnable_s6a <='0';
		end if;
	end process;
	
  S6bUartRdEnProc: process
  begin
		wait until rising_edge(clk40in);
		RdEnS6b_r0<=RdEn_s6b;
		RdEnS6b_r1<=RdEnS6b_r0;
		if RdEnS6b_r1='0' and RdEnS6b_r0='1' then
			RdEnable_s6b <='1';
		else
			RdEnable_s6b <='0';
		end if;
	end process;
	  
 MasterUartRdEnProc: process

  begin
		wait until rising_edge(clk40in);
		RdEnMaster_r0<=RdEn_master;
		RdEnMaster_r1<=RdEnMaster_r0;
		if RdEnMaster_r1='0' and RdEnMaster_r0='1' then
			RdEnable_master <='1';
		else
			RdEnable_master <='0';
		end if;
	end process;

  -- read mux
  epcRdBlock: block
  begin
    xps_epc_0_PRH_Data_I_pin <= ctlReg when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ctlAddr 
                                else designId when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = designIdAddr
                                else statReg when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = statAddr
                                else sportMask when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = spMaskAddr
                                else rod_busy_mask_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyMaskAddr
				else rod_busy_master_status_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyMasterStatusAddr
				else rod_busy_n_in_p_0_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyBusyNinP0HistAddr				
				else rod_busy_n_in_p_1_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyBusyNinP1HistAddr
				else formatter_mb_fef_n_in_p_0_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyFormatter0HistAddr
				else formatter_mb_fef_n_in_p_1_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyFormatter1HistAddr
				else efb_header_pause_i_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyHeaderPauseHistAddr
				else rol_test_pause_i_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyRolTestPauseHistAddr
				else rod_busy_n_out_p_hist_register when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = busyOutHistAddr
				else wdogTiggerCnt when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = wdAddr
				else ppc_uart_s6a_status 		when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6aStatus
				else ppc_uart_s6a_data 			when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6aData
				else ppc_uart_s6b_status 		when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6bStatus
				else ppc_uart_s6b_data 			when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartS6bData
				else ppc_uart_Master_status 	when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartMasterStatus
				else ppc_uart_Master_data	 	when ppcSelect = '1' and xps_epc_0_PRH_Addr_pin(ppcRegBits - 1 downto 0) = ppcUartMasterData
                                else mstData when ppcSelect = '0'
				
                                else (others => '0');
  end block;
  
	epc_ready_process: process(clk40in)
	begin
		if(rising_edge(clk40in)) then
			if(ppcSelect = '1') then
				xps_epc_0_PRH_Rdy_pin <= 	'1';
			elsif(mstRdy = '1' and xps_epc_0_PRH_CS_n_pin = '0') then
				xps_epc_0_PRH_Rdy_pin <= 	'1';
			else
				xps_epc_0_PRH_Rdy_pin <= 	'0';
			end if;
		end if;
	end process epc_ready_process;
  
  -- spi flash
  flash_cs_n <= flash_ss(0);
  flash_reset_n <= reset_n; -- ctlReg(ctlFlashRstBit);
  flash_wp_n <= '1'; -- ctlReg(ctlFlashWpBit);
  
  -- serial port fifo
  -- from MSP code:
  --/* define the Transmit Control Register values */ 
  --/* no bit reversal, xmit length = 32bits, no companding, msb first */

  -- Note: this runs with cpuClk!
  xps_ll_fifo_0_tx_llink_dest_rdy_n_pin <= sportFull;
  wrSport <= '1' when xps_ll_fifo_0_tx_packet = '1' and xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' 
             else '1' when xps_ll_fifo_0_tx_llink_sop_n_pin = '0' and xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' 
             else '0';
				 
  theSport : sportFifo
    PORT MAP (
      rst => sportReset,
      wr_clk => cpuClk,
      rd_clk => clk40in,
      din => xps_ll_fifo_0_tx_llink_din_pin,
      wr_en => wrSport,
      rd_en => rdSport,
      dout => sportData,
      full => sportFull,
      empty => sportEmpty
      );
		
  -- trace packet state: valid data only between sop and eop
  sportPac_process1: process(sportReset, cpuClk)
  begin
    if sportReset = '1' then
      xps_ll_fifo_0_tx_packet <= '0';
    elsif rising_edge(cpuClk) then
      if xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_sop_n_pin = '0' then
        xps_ll_fifo_0_tx_packet <= '1';
      elsif xps_ll_fifo_0_tx_llink_dest_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_src_rdy_n_pin = '0' and xps_ll_fifo_0_tx_llink_eop_n_pin = '0' then
        xps_ll_fifo_0_tx_packet <= '0';
      end if;
    end if;
  end process sportPac_process1;

  sportPac_process2:process(sportReset, clk40in)
  begin
    if sportReset = '1' then
      sportBits <= 0;
    elsif rising_edge(clk40in) then
      if sportBits = 0 then
        sportDataBit <= '0';
        -- read fifo
        if sportEmpty = '0' then
          sportReg <= sportData;
          rdSport <= '1';
          sportBits <= 32;
        end if;
      else
        if sportBits > 1 then
          sportDataBit <= sportReg(sportBits - 1);
          sportBits <= sportBits - 1;
          rdSport <= '0';
        elsif sportEmpty = '0' then -- reload
          sportDataBit <= sportReg(sportBits - 1);
          sportReg <= sportData;
          rdSport <= '1';
          sportBits <= 32;
        else
          sportBits <= 0;
          sportDataBit <= '0';
          rdSport <= '0';
        end if;
      end if;
    end if;
  end process sportPac_process2;

  -- interrupts not used yet
  irqReq <= '0';

  -- ------------------------------------------
  -- ------------ old stuff -------------------

  -- reset and serial port already remapped
  reset_s6a			<= ctrl_from_prm(13);--master_reset; -- ctlReg(ctlSlvARstBit);
  reset_s6b			<= ctrl_from_prm(14);--master_reset; -- ctlReg(ctlSlvBRstBit);

  reset_processor_core_SEL_s6a			<= ctrl_from_prm(6);--master_reset; -- ctlReg(ctlSlvARstBit);
  reset_processor_core_SEL_s6b			<= ctrl_from_prm(6);--master_reset; -- ctlReg(ctlSlvBRstBit);

  
  controller_clkx_out_p(0) <= clk40in;
  controller_clkx_out_p(1) <= clk40in;
  
  serial_port_output_gen: for i in 0 to 7 generate
  begin
    process(sportDataBit, mcbsp_rod_0_Dx_O, controller_dx_in_p(0), ppcIsController, ctlReg(ctlLlSportBit))	
    begin
      if(ppcIsController = '1' and ctlReg(ctlLlSportBit) = '1') then
        sport0Data(i)	<= sportDataBit;
      elsif(ppcIsController = '1' and ctlReg(ctlLlSportBit) = '0') then
        sport0Data(i)	<= mcbsp_rod_0_Dx_O;
      else
        sport0Data(i)	<= controller_dx_in_p(0);
      end if;
      if(ppcIsController = '1' and ctlReg(ctlLlSportBit) = '1') then
        sport1Data(i)	<= sportDataBit;
      elsif(ppcIsController = '1' and ctlReg(ctlLlSportBit) = '0') then
        sport1Data(i)	<= mcbsp_rod_0_Dx_O;
      else
        sport1Data(i)	<= controller_dx_in_p(0);
      end if;
    end process;
  end generate;
  
 serial_out_process: process(rrif_ctrl_reg_i, sport0Data, sport1Data,
           fe_command_mask0_i, fe_command_mask1_i,
           fe_command_link_out_p)
 begin
  
  if(rrif_ctrl_reg_i(8) = '0') then
    xc_a <= sport0Data AND fe_command_mask0_i(7 downto 0);
    xc_b <= sport1Data AND fe_command_mask1_i(7 downto 0);
  else
    xc_a  <= fe_command_link_out_p(7 downto 0);
    xc_b <= fe_command_link_out_p(15 downto 8);
  end if;
 end process serial_out_process;

  
  rodbus_data_block : for i in 0 to 15 generate
    rodbus_data_iobuf : IOBUF
      port map (
        I  => rod_bus_data_out_p(i),
        O  => rod_bus_data_in_p(i),
        IO => rod_bus_data(i),
        T  => rod_bus_rnw_out_p
        );
  end generate;

-- full 16 bit address size
  rodbus_addr_block :  for i in 0 to 15 generate
    rodbus_adr_obuf : OBUF
      port map (I => rod_bus_adr_out_p(i), O => rod_bus_addr(i));
  end generate;

  rodbus_cen_S6A_obuf : OBUF
    port map (I => rod_bus_ce_n_out_p(1), O => rod_bus_ce1_s6);		-- CE to S6A

  rodbus_cen_S6B_obuf : OBUF
    port map (I => rod_bus_ce_n_out_p(2), O => rod_bus_ce2_s6);		-- CE to S6B

  rodbus_rnw_obuf : OBUF  
    port map (I => rod_bus_rnw_out_p, O => rod_bus_rnw);

  fmt_hwob_obuf : OBUF
    port map (I => fmt_hwob_p, O => form_hwob);

  fmt_ds_obuf : OBUF
    port map (I => fmt_ds_p, O => form_ds); 
 
  
  	reset_n <= not cpuRst;
	mstRd_n <= mstSelect_n or not xps_epc_0_PRH_RNW_pin;
	mstOe_n <= mstRd_n;

	controlSwitch: block
	begin
		controller_bus_data_out_p <= dspDout;
		mstData 	<= dspDout;
		controller_bus_ack_out_i <= dspAck;
		mstRdy	<= dspAck;

		dspDin 	<= xps_epc_0_PRH_Data_O_pin when ppcIsController = '1' else controller_bus_data_in_p;
		dspAddr 	<= xps_epc_0_PRH_Addr_pin(21 downto 2) when ppcIsController = '1' else controller_bus_adr_in_p(19 downto 0);
		dspBe_n 	<= "0000" when ppcIsController = '1' else controller_bus_be_n_in_p;
		dspCe_n 	<= mstSelect_n when ppcIsController = '1' else controller_bus_ce_n_in_p;
		dspRnw 	<= xps_epc_0_PRH_RNW_pin when ppcIsController = '1' else controller_bus_rnw_in_p;
		dspRd_n 	<= mstRd_n when ppcIsController = '1' else controller_bus_re_n_in_p;
		dspOe_n 	<= mstOe_n when ppcIsController = '1' else controller_bus_oe_n_in_p;
	end block; 

  cal_l1id_mux_process: process(clk40in, reset_n)
  begin
    if(reset_n = '0') then
      cal_l1id0_m <= (others => '0');
      cal_l1id1_m <= (others => '0');
    elsif(rising_edge(clk40in)) then
      if(mask_dxi_i = '1') then
        cal_l1id0_m <= "00000000" & nevent_o;
        cal_l1id1_m <= "00000000" & nevent_o;
      elsif(spare_ctrl_reg_i(11) = '1') then
        cal_l1id0_m <= std_logic_vector(event_count1);
        cal_l1id1_m <= std_logic_vector(event_count2);
      else
        cal_l1id0_m <= cal_l1id0_i;
        cal_l1id1_m <= cal_l1id1_i;
      end if;
    end if;
  end process cal_l1id_mux_process;

  diag_evt_count_process: process(clk40in, reset_n)
  begin
    if(reset_n = '0') then
      event_count1 <= (others => '0');
      event_count2 <= (others => '0');
    elsif(rising_edge(clk40in)) then
      if(spare_ctrl_reg_i(11) = '0') then
        event_count1 <= (others => '0');
        event_count2 <= (others => '0');
      else
        if(tim_signal_ii(0) = '1') then
          event_count1 <= event_count1 + '1';
        end if;
        if(tim_signal_ii(0) = '1' AND rod_busy_n_out_p = '0') then
          event_count2 <= event_count2 + '1';
        end if;
      end if;
    end if;
  end process diag_evt_count_process;

dsp_addr_block :
  for i in 0 to 19 generate dsp_addr_ibufs : IBUF
    port map (
      I => controller_bus_adr_in(i),
      O => controller_bus_adr_in_p(i)
      );
  end generate;

dsp_be_n_block :
  for i in 0 to 3 generate dsp_be_n_ibuf : IBUF 
    port map (
      I => controller_bus_be_n_in(i),
      O => controller_bus_be_n_in_p(i)
      );
  end generate;

dsp_ce_n_ibuf : IBUF
  port map (I => controller_bus_ce_n_in, O => controller_bus_ce_n_in_p);
  
dsp_oe_n_ibuf : IBUF
  port map (I => controller_bus_oe_n_in, O => controller_bus_oe_n_in_p);
  
dsp_re_n_ibuf : IBUF
  port map (I => controller_bus_re_n_in, O => controller_bus_re_n_in_p);
  
dsp_we_n_ibuf : IBUF
  port map (I => controller_bus_rnw_in, O => controller_bus_rnw_in_p);
  
dsp_ARDY_obuf : OBUF
  port map (I => controller_bus_ack_out_p, O => controller_bus_ack_out);

dsp_McSP_block : for i in 0 to 1 generate
  dsp_tinp_obuf : OBUF
    port map (I => controller_tinp_out_p(i), O => controller_tinp_out(i));

  dsp_tout_ibuf : IBUF
    port map (I => controller_tout_in(i), O => controller_tout_in_p(i));

  dsp_dr_obuf : OBUF
    port map (I => controller_dr_out_p(i), O => controller_dr_out(i));

  dsp_dx_ibuf : IBUF
    port map (I => controller_dx_in(i), O => controller_dx_in_p(i));

  dsp_clkr_ibuf : IBUF
    port map (I => controller_clkr_in(i), O => controller_clkr_in_p(i));

  dsp_clkx_obuft : OBUFT
    port map (
      I => controller_clkx_out_p(i),
      O => controller_clkx_out(i),
      T => fsx_en_n_obuft
      );
		
  dsp_fsr_ibuf : IBUF
    port map (I => controller_fsr_in(i), O => controller_fsr_in_p(i));

  dsp_fsx_obuft : OBUFT
    port map (
      I => controller_fsx_out_p(i), 
      O => controller_fsx_out(i),
      T => fsx_en_n_obuft
      );
end generate;

dsp_ext_int_block :  for i in 0 to  4 generate
  dsp_ext_int_obuf : OBUF
    port map (
      I => controller_ext_int_n_out_p(i),
      O => controller_ext_int_n_out(i)
      );
end generate;
	 
dsp_data_block :
  for i in 0 to 31 generate dsp_data_iobufs : IOBUF
    port map (
      I  => controller_bus_data_out_p(i),
      O  => controller_bus_data_in_p(i),
      IO => controller_bus_data_inout(i),
      T  => controller_bus_tbuf_driver_i
      );
  end generate;

--------------------------------------------------------------------------------
-- ppc hpi port emu
--------------------------------------------------------------------------------

  prm_ctrl_out(ctl_to_prm_hpi_valid_bit) <= 	'0'; 
  prm_ctrl_out(ctl_to_prm_hpi_done_bit) <=	hpiport_0_PRM_done_pin ;
  prm_ctrl_out(ctl_to_prm_hpi_busy_bit) <=	hpiport_0_PRM_busy_pin ;
  prm_ctrl_out(19 downto ctl_to_prm_last_bit + 1) <= (others => '0');

  -- gate prm strobe and command with hpi enable bit
  hpiport_0_PRM_command_pin <= ctrl_from_prm(ctl_from_prm_hpi_cmd_hi_bit downto ctl_from_prm_hpi_cmd_lo_bit) when ctlReg(ctlHpiBit) = '1' else (others => '0');
  hpiport_0_PRM_strobe_pin <= prm_strobe(0) and ctlReg(ctlHpiBit);
  
  hpiport_0_PRM_data_bus_in_pin <= prm_data_io;
  prm_data_io <= hpiport_0_PRM_data_bus_out_pin when hpiport_0_PRM_data_valid_pin = '1' and ctlReg(ctlHpiBit) = '1' 
                 else (others => 'Z');

end architecture STRUCTURE;

