-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: cal_trigger_signal_decoder.vbd
-- Description:
--  Counts L1 Triggers issued by the Master DSP during calibration and special
--  testing modes
-- SCT Command bit streams to front end modules.
--  l1accept:  "110" 
--  bcr:       "1010010" 
--  ecr:       "1010100"
--  cal:       "1010111"
-- Pixel Command bit streams to front end modules.
--  l1accept:  "11101" 
--  bcr:       "101100001" 
--  ecr:       "101100010"
--  cal:       "101100100"

--
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author: John M. Joseph
-- Board Engineer: John M. Joseph
-- IBL Upgrade: Shaw-Pin (Bing) Chen
-- History:
--    15 May 2002 - JMJ first version 
--    20 Mar 2003 - Added Pixel Command Detection
--    20 Aug 2013 - SPC Modified code for use in IBL ROD
-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all;
-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity sp_trig_cmd_detector is
  port (
    clk            		: in  std_logic; -- 
    rst            		: in  std_logic; -- asynchronous global reset
    enable_in      		: in  std_logic; -- 
--    rod_type_in    : in  std_logic; -- 0=>SCT, 1=>Pixel
    l1id_select_in 		: in  std_logic; -- 
    bcid_select_in 		: in  std_logic; -- 
    cal_serial0_in 		: in  std_logic; -- SerPort0
    cal_serial1_in 		: in  std_logic; -- SerPort1
    l1_accept0_out 		: out std_logic; -- L1 Trig Detected on SerPort0
    l1_accept1_out 		: out std_logic; -- L1 Trig Detected on SerPort1
    l1id0_reg_in   		: in  std_logic_vector(23 downto 0); --
    l1id1_reg_in   		: in  std_logic_vector(23 downto 0); --
    l1id0_out      		: out std_logic_vector(23 downto 0); --
    l1id1_out      		: out std_logic_vector(23 downto 0); --
	 calpulse_count0_out	: out std_logic_vector(23 downto 0);
	 calpulse_count1_out	: out std_logic_vector(23 downto 0);
    bcid0_reg_in   		: in  std_logic_vector(11 downto 0); --
    bcid1_reg_in   		: in  std_logic_vector(11 downto 0); --
    bcid0_out      		: out std_logic_vector(11 downto 0); --
    bcid1_out      		: out std_logic_vector(11 downto 0)  --
    );
end sp_trig_cmd_detector;

architecture rtl of sp_trig_cmd_detector is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type cal_states is (
  reset, 
  idle, 
  l1a, 
  fast_slow_cmd, 
  ecr, 
  bcr, 
  cal
  );
signal sp0_command_type_state : cal_states;
signal sp1_command_type_state : cal_states;

signal inc_l1id0_i  : std_logic;
signal send_l1id0_i : std_logic;
signal inc_l1id1_i  : std_logic;
signal send_l1id1_i : std_logic;
signal clr_l1id0_i  : std_logic;
signal clr_l1id1_i  : std_logic;
signal clr_bcid0_i  : std_logic;
signal clr_bcid1_i  : std_logic;
signal calpulse_count0: std_logic_vector(23 downto 0);
signal calpulse_count1: std_logic_vector(23 downto 0);

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

begin

calpulse_count0_out <= calpulse_count0;
calpulse_count1_out <= calpulse_count1;

-------------------------------------------------------------------------------
-- Serial Data Decoders
-------------------------------------------------------------------------------

serial_data_decode : process (
  clk, 
  rst, 
  cal_serial0_in
  )
variable serdata0 : std_logic_vector(8 downto 0);
variable serdata1 : std_logic_vector(8 downto 0);
variable sp0_cal_count : unsigned(5 downto 0);
variable sp1_cal_count : unsigned(5 downto 0);
begin
  if(rst = '1') then
    inc_l1id0_i 				<= '0';
    inc_l1id1_i 				<= '0';
    clr_bcid0_i 				<= '1';
    clr_l1id0_i 				<= '1';
    clr_bcid1_i 				<= '1';
    clr_l1id1_i 				<= '1';
    serdata0 					:= (others => '0');
    serdata1 					:= (others => '0');
    sp0_command_type_state <= reset;
    sp0_cal_count 			:= (others => '0');
    sp1_command_type_state <= reset;
    sp1_cal_count 			:= (others => '0');
	 calpulse_count0 			<= (others => '0');
	 calpulse_count1 			<= (others => '0');
  elsif (clk'event and clk = '1') then
-- cal_serial0 Decoder
    if (enable_in = '1') then
      serdata0(8 downto 1) := serdata0(7 downto 0); -- serial in parallel out register
      serdata0(0) := cal_serial0_in;
    else
      serdata0 := (others => '0');
    end if;
    
    case sp0_command_type_state is
      when reset =>
        clr_bcid0_i <= '1';
        clr_l1id0_i <= '1';
        inc_l1id0_i <= '0';
        sp0_cal_count := (others => '0');
        sp0_command_type_state <= idle;

      when idle  =>
        clr_bcid0_i <= '0';
        clr_l1id0_i <= '0';
        inc_l1id0_i <= '0';
        sp0_cal_count := (others => '0');
        if (serdata0(4 downto 0) = "11101") then -- IBL, Same as Pixel
          sp0_command_type_state <= l1a;
        elsif (serdata0(4 downto 0) = "10110") then
          sp0_command_type_state <= fast_slow_cmd;
        end if;

      when l1a =>
        inc_l1id0_i <= '1';
        sp0_command_type_state <= idle;

      when fast_slow_cmd =>
        sp0_cal_count := sp0_cal_count + 1;
        if (sp0_cal_count = 4) then
          if (serdata0 = "101100010") then -- event counter reset command
            sp0_command_type_state <= ecr;
          elsif (serdata0 = "101100001") then -- bunch counter reset command
            sp0_command_type_state <= bcr;
          elsif (serdata0 = "101100100") then -- calibration command
            sp0_command_type_state <= cal;
            sp0_cal_count := (others => '0');
          else -- garbage/unknown command
            sp0_command_type_state <= idle;
          end if;
        end if;
  
      when ecr =>
        clr_l1id0_i <= '1';
        clr_bcid0_i <= '1';
        sp0_command_type_state <= idle;
  
      when bcr =>
        clr_bcid0_i <= '1';
        sp0_command_type_state <= idle;

      when cal =>
        sp0_cal_count := sp0_cal_count + 1;
        if (sp0_cal_count = 1) then		-- was 20d: DAV (29 Jul 2014)
          sp0_command_type_state <= idle;
        end if;
		  calpulse_count0 <= calpulse_count0 + 1;
        
      when others =>
        clr_bcid0_i <= '1';
        clr_l1id0_i <= '1';
        inc_l1id0_i <= '0';
        sp0_command_type_state <= idle;
    end case;

-- Cal_serial1 Decoder
    if (enable_in = '1') then
      serdata1(8 downto 1) := serdata1(7 downto 0);
      serdata1(0) := cal_serial1_in;
    else
      serdata1 := (others => '0');
    end if;
    
    case sp1_command_type_state is
      when reset =>
        clr_bcid1_i <= '1';
        clr_l1id1_i <= '1';
        inc_l1id1_i <= '0';
        sp1_cal_count := (others => '0');
        sp1_command_type_state <= idle;

      when idle  =>
        clr_bcid1_i <= '0';
        clr_l1id1_i <= '0';
        inc_l1id1_i <= '0';
        sp1_cal_count := (others => '0');
        if (serdata1(4 downto 0) = "11101") then
          sp1_command_type_state <= l1a;
        elsif (serdata1(4 downto 0) = "10110") then
          sp1_command_type_state <= fast_slow_cmd;
        end if;

      when l1a =>
        inc_l1id1_i <= '1';
        sp1_command_type_state <= idle;

      when fast_slow_cmd =>
        sp1_cal_count := sp1_cal_count + 1;
        if(sp1_cal_count = 4) then
          if (serdata1 = "101100010") then
            sp1_command_type_state <= ecr;
          elsif(serdata1 = "101100001") then
            sp1_command_type_state <= bcr;
          elsif(serdata1 = "101100100") then
            sp1_command_type_state <= cal;
            sp1_cal_count := (others => '0');
          else
            sp1_command_type_state <= idle;
          end if;
        end if;
  
      when ecr =>
        clr_l1id1_i <= '1';
        clr_bcid1_i <= '1';
        sp1_command_type_state <= idle;
  
      when bcr =>
        clr_bcid1_i <= '1';
        sp1_command_type_state <= idle;

      when cal =>
        sp1_cal_count := sp1_cal_count + 1;
        if (sp1_cal_count = 1) then			-- was 20d: DAV (29 Jul 2014)
          sp1_command_type_state <= idle;
        end if;
		  calpulse_count1 <= calpulse_count1 + 1;
        
      when others =>
        clr_bcid1_i <= '1';
        clr_l1id1_i <= '1';
        inc_l1id1_i <= '0';
        sp1_command_type_state <= idle;
    end case;

  end if;  
end process serial_data_decode;
    
-------------------------------------------------------------------------------
-- Counters
-------------------------------------------------------------------------------
l1id0_counter : process (
  clk, 
  rst
  )
variable l1id0_count : unsigned(23 downto 0);
begin
  if (rst = '1') then
    l1id0_out   <= (others => '1'); -- so next iteration will start the counter at 0 
    l1id0_count := (others => '1');
    l1_accept0_out <= '0';
  elsif (clk'event and clk = '1') then
    if (clr_l1id0_i = '1' OR enable_in = '0') then
      l1id0_count := (others => '1');
    elsif (l1id_select_in = '1') then
      l1id0_count := unsigned(l1id0_reg_in);
    elsif (inc_l1id0_i = '1' AND l1id_select_in = '0') then
      l1id0_count := l1id0_count + 1;
    end if;
    l1id0_out      <= std_logic_vector(l1id0_count);
    send_l1id0_i   <= inc_l1id0_i; -- send out L1ID whenever there is a new one
    l1_accept0_out <= send_l1id0_i; -- delay one clock cycle
  end if;  
end process l1id0_counter;
-------------------------------------------------------------------------------
l1id1_counter : process (
  clk, 
  rst
  )
variable l1id1_count : unsigned(23 downto 0);
begin
  if (rst = '1') then
    l1id1_out    <= (others => '1');
    l1id1_count  := (others => '1');
    l1_accept1_out <= '0';
  elsif (clk'event and clk = '1') then
    if (clr_l1id1_i = '1' OR enable_in = '0') then
      l1id1_count := (others => '1');
    elsif (l1id_select_in = '1') then
      l1id1_count := unsigned(l1id1_reg_in);
    elsif (inc_l1id1_i = '1' AND l1id_select_in = '0') then
      l1id1_count := l1id1_count + 1;
    end if;
    l1id1_out      <= std_logic_vector(l1id1_count);
    send_l1id1_i   <= inc_l1id1_i;
    l1_accept1_out <= send_l1id1_i;
  end if;  
end process l1id1_counter;
-------------------------------------------------------------------------------
bcid0_counter : process (
  clk, 
  rst
  )
variable bcid0_count : unsigned(11 downto 0);
begin
  if (rst = '1') then
    bcid0_out   <= (others => '1');
    bcid0_count := (others => '0');
  elsif (clk'event and clk = '1') then
    if (clr_bcid0_i = '1' OR enable_in = '0') then
      bcid0_count := "000000000001";
    else
      bcid0_count := bcid0_count + 1;
    end if;
     
    if (inc_l1id0_i = '1' AND bcid_select_in = '0') then
      bcid0_out <= std_logic_vector(bcid0_count);
    elsif (bcid_select_in = '1') then
      bcid0_out <= bcid0_reg_in;
    end if;  
  end if;  
end process bcid0_counter;
-------------------------------------------------------------------------------
bcid1_counter : process (
  clk, 
  rst
  )
variable bcid1_count : unsigned(11 downto 0);
begin
  if (rst = '1') then
    bcid1_out   <= (others => '1');
    bcid1_count := (others => '0');
  elsif (clk'event and clk = '1') then
    if (clr_bcid1_i = '1' OR enable_in = '0') then
      bcid1_count := "000000000001";
    else
      bcid1_count := bcid1_count + 1;
    end if;

    if (inc_l1id1_i = '1' AND bcid_select_in = '0') then
      bcid1_out <= std_logic_vector(bcid1_count);
    elsif (bcid_select_in = '1') then
      bcid1_out <= bcid1_reg_in;
    end if;  
  end if;  
end process bcid1_counter;


end rtl; -- end code for trigger_signal_decoder
