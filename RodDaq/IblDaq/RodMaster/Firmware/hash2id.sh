#!/bin/bash
# purpose: generate gitIdPkg file with current git hash

#set destination file name. might be propagated from Makefile later on
export IDFILE=src/gitIdPkg.vhd
#copy template to destination file
cp src/gitIdPkg_template.txt $IDFILE
#prompt current hash. should do a pull first ...
echo "Current git hash: `git rev-parse HEAD`"

#now add hash to package file
# git hash has 40 bytes. start with 1
i=1
while [ $i -lt 39 ]
do 
  let j=$i+1
  echo "X\"`git rev-parse HEAD | cut -b $i,$j`\"," >> $IDFILE
  let i=$i+2
done  
# last byte
echo "X\"`git rev-parse HEAD | cut -b 39,40`\"" >> $IDFILE
echo ");" >> $IDFILE
echo "-- GIT HASH: `git rev-parse HEAD`" >>  $IDFILE
echo "end gitIdPack;" >> $IDFILE
#if desired copy to console for check
#cat $IDFILE
unset IDFILE

