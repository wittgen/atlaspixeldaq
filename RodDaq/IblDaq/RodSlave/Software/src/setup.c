/*
 * Filename:   setup.c
 * Author:     Kugel
 *
 * Summary:
 *   This file contains routines used during the setup phase of the
 *   embedded system. Vectors for interrupt and exception handling are
 *   set, the EPC is initialized, and DMA buffer pointers are set.
 */

#include "histClient.h"
#include "rodSlave.hxx"
#include "rodHisto.hxx"
#include "rodHistoL12_addendum.hxx"

void setupAddresses() {
	// setup vectors, except reset (reset should still point to the bootloader code)
	((uint32_t*)IRQ_VECTOR)[0] = 0xb0000000 + (((uint32_t)&_interrupt_handler) >> 16);
	((uint32_t*)IRQ_VECTOR)[1] = 0xb8080000 + (((uint32_t)&_interrupt_handler) & 0xffff);
	((uint32_t*)EXCEPT_VECTOR)[0] = 0xb0000000 + (((uint32_t)&_exception_handler) >> 16);
	((uint32_t*)EXCEPT_VECTOR)[1] = 0xb8080000 + (((uint32_t)&_exception_handler) & 0xffff);
	((uint32_t*)HW_EXCEPT_VECTOR)[0] = 0xb0000000 + (((uint32_t)&_hw_exception_handler) >> 16);
	((uint32_t*)HW_EXCEPT_VECTOR)[1] = 0xb8080000 + (((uint32_t)&_hw_exception_handler) & 0xffff);

	// init EPC
	epcIdReg = (volatile uint32_t *)MB_DESIGN_REG;
	epcCtlReg = (volatile uint32_t *)MB_CTL_REG;
	epcStatReg = (volatile uint32_t *)MB_STAT_REG;
	busRam = (volatile uint32_t *)MB_EPC_RAM_BASE;

	// command loop stuff
	verbose = 0;
	slaveId = -1;

	// get address of command and status
	cmd = (IblSlvRdWr*)&busRam[SLV_CMD_LOCATION];
	cmdStat = (iblSlvStat*)&cmd->data;

	// indicate we're ready
	cmd->cmd = SLV_CMD_ALIVE;

	// get simulation status
	simulation = (0 != (*epcStatReg & (1 << MB_STAT_SIMULATION_BIT)));
}

void setupGeometry(int unit) {

  nChips = histConfig.cfg[unit].nChips;
  isFei3 = (nChips > 8); //  How this will ultimately be set is still up in the air.  Could end up staying like this with
                         //  nChips ultimately set in the currently unused bits 17-24 in MB_HISTx_CTL_REG, for x = 0, 1.
  if (isFei3) { //  Fei3 case

    chipsPerMod = 16;
    maxChips = 128; //  Remove once (if ever) slave firmware uses nChips (as it should).

    row_size = ROW_SIZE_L12;
    col_size = COL_SIZE_L12;
    chip_bits = CHIP_BITS_L12;
    tot_bits = TOT_BITS_L12; //  Apparently not used
    tot_shift = TOT_SHIFT_L12;
    tot_result_bits = TOT_RESULT_BITS_L12;
    totsqr_result_bits = TOTSQR_RESULT_BITS_L12;
    twoword_tot_result_shift = TWOWORD_TOT_RESULT_SHIFT_L12;
    twoword_totsqr_result_shift = TWOWORD_TOTSQR_RESULT_SHIFT_L12;
    rowmaskctl_bits = ROWMASKCTL_BITS_L12; //  Apparently not used
    opmodectl_shift = OPMODECTL_SHIFT_L12;
    mstepindex_shift_even = MSTEPINDEX_SHIFT_EVEN_L12;

  }else{ //  Fei4 case

    chipsPerMod = 2;
    maxChips = 8; //  Remove once (if ever) slave firmware uses nChips (as it should).

    // What follows are lower case versions of former #define variables,
    // necessary because of seperate firmware bit files for FE-I3 and FE-I4

    row_size = ROW_SIZE;
    col_size = COL_SIZE;
    chip_bits = CHIP_BITS;
    tot_bits = TOT_BITS; //  Apparently not used
    tot_shift = TOT_SHIFT;
    tot_result_bits = TOT_RESULT_BITS;
    totsqr_result_bits = TOTSQR_RESULT_BITS;
    twoword_tot_result_shift = TWOWORD_TOT_RESULT_SHIFT;
    twoword_totsqr_result_shift = TWOWORD_TOTSQR_RESULT_SHIFT;
    rowmaskctl_bits = ROWMASKCTL_BITS; //  Apparently not used
    opmodectl_shift = OPMODECTL_SHIFT;
    mstepindex_shift_even = MSTEPINDEX_SHIFT_EVEN;

  }

  opmode_online_occupancy = (0 << opmodectl_shift);
  opmode_offline_occupancy = (1 << opmodectl_shift);
  opmode_short_tot = (2 << opmodectl_shift);
  opmode_long_tot = (3 << opmodectl_shift);

  pixelsPerChip = row_size * col_size;
//  Maximum number of pixels to be handled at a time by one histoUnit (of which there are 2 per slave)
  maxPossiblePixels=((1 << chip_bits) * pixelsPerChip);
}
