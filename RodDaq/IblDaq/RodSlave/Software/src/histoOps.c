 /*
 * Filename:   histoOps.c
 * Author:     Kugel
 *
 * Summary:
 *   Functions to feed test data to the histogrammer and move histogram
 *   data from internal histogrammer memory (BRAM/SSRAM) to the external
 *   slave RAM for further processing (e.g. transferring it via network).
 */

#include <string.h>
#include <math.h>

/* Xilinx Header Files */
#include "xparameters.h"
#include "xbasic_types.h"
#include "xtmrctr.h"
#include "xaxidma.h"

#include "rodSlave.hxx"
#include "rodHisto.hxx"
#include "rodHistoL12_addendum.hxx"
#include "histClient.h"
#include "memoryManagement.h"

// Memory size for writes to interfere with DMA transfers during readout
#define MEMORY_SIZE 4*1024*1024

#define EXTRACT_FROM_BITS_IN_WD(WD, shift, bits) ((WD & ((1<<(bits + shift)) - 1) & (~((1<<shift) - 1))) >> shift)

static XAxiDma axiDma0;
static XAxiDma axiDma1;

#ifndef HISTO_TEST
//  Variables that need to remain unchanged over multiple testHistogramming() calls must be static:
static circular_buffer_t* cb_th[2];
static cb_item_t* item_th[2];
static int pixelsPerHisto;
static IblSlvDmaTransfer dma;
static IblSlvDmaTransfer *pDma;

static volatile uint8_t* cb_item_data;
static volatile uint32_t* cb_item_data32;
#endif

int checkMaskStepSettings(IblSlvHistUnitCfg* pUnitCfg);
void rdDmaStatus(int l);
void printHistoReadout(IblSlvHistUnitCfg* pUnitCfg, int dmaSize, volatile uint32_t *memory, int i);

/* Cross-checks mask step settings for inconsistencies. */
int checkMaskStepSettings(IblSlvHistUnitCfg* pUnitCfg) {
uint32_t mStepEven = pUnitCfg->mStepEven;
uint32_t mStepOdd = pUnitCfg->mStepOdd;
uint32_t maskStep = pUnitCfg->maskStep;

int invalid = 0;

/* Out of range checks */
if ((isFei3 == 0 && (mStepEven < 0 || mStepEven > 16 || mStepOdd < 0 || mStepOdd > 16)) ||
    (isFei3 == 1 && (mStepEven < 0 || mStepEven > 64 || mStepOdd < 0 || mStepOdd > 64))) {
  verbmsg("ERROR: mask stepping invalid because of wrong isFei3/mStepEven/mStepOdd settings");
  invalid = 1;
}

if ((maskStep != ROWMASKNONE && maskStep != ROWMASK2 && maskStep != ROWMASK4 && maskStep != ROWMASK8 && maskStep != ROWMASK16 && maskStep != ROWMASK32) ||
    (maskStep != ROWMASKNONE && maskStep != ROWMASK32 && maskStep != ROWMASK64 && isFei3 == 1)) {
  
    invalid = 1;
}

/* Logical checks */
switch (maskStep) {
    case ROWMASKNONE:
        if (mStepEven > 0 || mStepOdd > 0) invalid = 1;
	
        break;

    case ROWMASK2:
        if (mStepEven > 2 || mStepOdd > 2) invalid = 1;
        break;

    case ROWMASK4:
        if (mStepEven > 4 || mStepOdd > 4) invalid = 1;
        break;

    case ROWMASK8:
        if (mStepEven > 8 || mStepOdd > 8) invalid = 1;
        break;

    case ROWMASK16: //  Cannot be used until implemented in firmware
        if (mStepEven > 16 || mStepOdd > 16) invalid = 1;
        break;

    case ROWMASK32: //  Only for FE-I3 use
        if (mStepEven > 32 || mStepOdd > 32) invalid = 1;
        break;

    case ROWMASK64: //  Only for FE-I3 use
        if (mStepEven > 64 || mStepOdd > 64) invalid = 1;
        break;
}

/* Error message */
if (invalid) {
    verbmsg("ERROR: mask stepping settings are invalid");
    verbmsg("ERROR: maskStep: %d", maskStep);
    verbmsg("ERROR: mStepEven: %d", mStepEven);
    verbmsg("ERROR: mStepOdd: %d", mStepOdd);
}

return invalid;
}

void histoSetMode(int histo, int mode)
{
  // the upper two bits are read only. Mask them for writing to avoid simulation confusion
  // also the other bits that are not referring to histo mode must bit mask
  int base = (histo == 0) ? MB_CTL_HIST_MODE0_BASE : MB_CTL_HIST_MODE1_BASE;
  *epcCtlReg =  (*epcCtlReg & (  ~(((1<<MB_HISTO_MODE_BITS)-1) << base) ) ) | (mode << base);
                                            //  =2              =2 or 4

}

void testHistogramming() {
#ifndef HISTO_TEST //  Not needed since this is an alterative to running histoTest.
	uint32_t i, j, count, injections, histoUnit, stepSize;
        uint32_t testinWd, chip, row, col, tot, pixel, fast, maxMissingTriggers, missingTriggers, totWdOffset;
	uint32_t *addr, *data;
	uint32_t muxCtlBit;
	enum TestHistoStatus status;
	char mode[11];
	int unitCfgOffset;

        histoUnit = cmd->addr;
	if (0 == histoUnit) {
		addr = (uint32_t*)MB_HIST0_TEST_REG;
		muxCtlBit = MB_CTL_HIST_MUX0_BIT;
	} else {
		addr = (uint32_t*)MB_HIST1_TEST_REG;
		muxCtlBit = MB_CTL_HIST_MUX1_BIT;
	}

        IblSlvHistUnitCfg* pUnitCfg = &histConfig.cfg[histoUnit];
        stepSize = (1 << pUnitCfg->maskStep);
        status = (enum TestHistoStatus)cmd->data;
	count = cmd->count - 1; //  -1 since first "data" word is testHistogramming status flag!
	data = ((uint32_t *)(&(cmd->data))) + 1;

	if(status == HISTO_TEST_SETUP || status == HISTO_TEST_SETUP_NOHISTO || status == HISTO_TEST_SETUP_EXT || status == HISTO_TEST_CMPLT) {

		cmd->cmd = SLV_CMD_BUSY;

		switch(status) {
		  case(HISTO_TEST_SETUP):
		    strcpy(mode,"HISTO");
		    break;
		  case(HISTO_TEST_SETUP_NOHISTO):
		    strcpy(mode,"NOHISTO");
		    break;
		  case(HISTO_TEST_SETUP_EXT):
		    strcpy(mode,"EXTERNAL");
		    break;
		  case(HISTO_TEST_CMPLT):
		    strcpy(mode,"CMPLT");
		    break;
		  default:;
		}
		verbmsg("TestHistogramming in %s mode: unit=%d, status=%d, count=%d", mode, histoUnit, status, count);

		if(status == HISTO_TEST_SETUP_EXT) {
		  *epcCtlReg |= (1 << muxCtlBit);
		  verbmsg("Set control register: 0x%x", *epcCtlReg);
		  verbmsg("Ext-FwSetup: Slave control register after enabling MUX: 0x%x", *epcCtlReg);
		  verbmsg("Ext-FwSetup: Slave status register after enabling MUX: 0x%x", *epcStatReg);
		} else {
		  *epcCtlReg &= ~(1 << muxCtlBit);
		  verbmsg("Set control register: 0x%x", *epcCtlReg);
		  verbmsg("Int-FwSetup: Slave control register after disabling MUX: 0x%x", *epcCtlReg);
		  verbmsg("Int-FwSetup: Slave status register after disabling MUX: 0x%x", *epcStatReg);
		}

		if(status == HISTO_TEST_SETUP_NOHISTO) {

		  //  Setup circular buffers to receive test data (just for NOHISTO mode)
		  cb_th[histoUnit] = &histogram_circular_buffers[histoUnit];
		  pixelsPerHisto = getChipDataSize(histoUnit);
		  unitCfgOffset = 4 + 4*((pixelsPerHisto- 1)/4); //  Config struct must be appended on word boundary!
		  item_th[histoUnit] = cbGetWriteItem(cb_th[histoUnit], unitCfgOffset + sizeof(IblSlvHistUnitCfg));
		  if (item_th[histoUnit] == NULL) {
		    verbmsg("ERROR: Non-FwSetup: Circular buffer of unit %d is full!", histoUnit);
		  }
		  cb_item_data = item_th[histoUnit]->data;
		  cb_item_data32 = (uint32_t*)cb_item_data;

		  //  Clear histogram_buffer
		  for (i = 0; i < pixelsPerHisto; ++i) {
		    cb_item_data[i] = 0;
		  }
		  dma.size[histoUnit] = pixelsPerHisto;
		  dma.address[histoUnit] = (u32) cb_item_data;
		  //  Ensure that nothing is done with the other histoUnit:
		  dma.size[1-histoUnit] = 0;
		  dma.address[1-histoUnit] = (u32) NULL;
		}

		// running
		histState = SLV_STAT_BUSY;

		cmd->cmd = SLV_CMD_IDLE;
	}

	for (i=0; i < count; i++) { //  Doesn't do anything if count = 0 here (i.e. sent in as 1, as NEEDS to be done in "SETUP" and "FINISH" modes)
          testinWd = data[i];
	  fast = ((testinWd & 0x80000000) >> 31);
          testinWd &= 0x7FFFFFFF;
	  injections = 1; //  Only one inection at a time unless in fast (multihit) mode.
	  if(fast){
            injections = data[i + 1];  //  Inject number of times given by values in data-word following pixel "address".
	    verbmsg("Fast Injection = %d", injections);
          }
	  if(status == HISTO_TEST_INPROGRESS_NOHISTO) {
//  Compute pixel # from chip, column, row and pixels-per-chip information
	    row = EXTRACT_FROM_BITS_IN_WD(testinWd, ROW_SHIFT, ROW_BITS);
	    col = EXTRACT_FROM_BITS_IN_WD(testinWd, COL_SHIFT, COL_BITS);
	    chip = EXTRACT_FROM_BITS_IN_WD(testinWd, CHIP_SHIFT, chip_bits);
	    tot = EXTRACT_FROM_BITS_IN_WD(testinWd, tot_shift, tot_bits);
	    pixel = pixelsPerChip*chip/stepSize + col_size*(row-1) + col-1; // Chip numbering starts at 0, but row and columns at 1 !!
	    verbmsg("data = 0x%x Pixel: %d, Chip: %d, Col: %d, Row: %d, ToT: %d, buffer size: %d", testinWd, pixel, chip, col, row, tot, item_th[histoUnit]->size);
	    if(pixel >= pixelsPerHisto) {
	      xil_printf("\r\n **** ERROR:  Pixel # exceeds maximum possible value of %d for this histogram:\r\n",pixelsPerHisto);
	      xil_printf("              data = 0x%x Pixel #: %d, Chip: %d, Col: %d, Row: %d, ToT: %d\r\n\r\n", testinWd, pixel, chip, col, row, tot);
	    }
	    switch(pUnitCfg->type) {
	      case ONLINE_OCCUPANCY:
	        cb_item_data32[pixel] += injections; //  Fill memory faster using values in data-word following pixel "address".
	        break;
	      case OFFLINE_OCCUPANCY:
	        cb_item_data32[pixel/4] += (injections << 8*(pixel%4));
	        break;
	      case SHORT_TOT:
	        maxMissingTriggers = (1 << MISSING_TRIGGERS_RESULT_BITS) - 1;
	        missingTriggers = 0;
	        if(injections > pUnitCfg->expectedOccValue) missingTriggers = maxMissingTriggers; //  This is what Fei4 firmware actually does, but shouldn't it be 0 ???
	        else if(injections < pUnitCfg->expectedOccValue) missingTriggers = pUnitCfg->expectedOccValue - injections;
	        if(missingTriggers > maxMissingTriggers) missingTriggers = maxMissingTriggers;
	        cb_item_data32[pixel] = (missingTriggers << ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT);
	        cb_item_data32[pixel] += ((injections*tot) << ONEWORD_TOT_RESULT_SHIFT);
	        cb_item_data32[pixel] += ((injections*tot*tot) << ONEWORD_TOTSQR_RESULT_SHIFT);
	        break;
	      case LONG_TOT:
	        totWdOffset = 1;
	        if(isFei3)totWdOffset = 0;
	        cb_item_data32[2*pixel] += injections;
	        cb_item_data32[2*pixel+totWdOffset] += ((injections*tot) << twoword_tot_result_shift);
	        cb_item_data32[2*pixel+1] += ((injections*tot*tot) << twoword_totsqr_result_shift);
	        break;
	    default:
	      verbmsg("ERROR: Unsupported non-firmware histo test readout mode %d", pUnitCfg->type);
	    }
	  } else { //  Here "low-level" injections are done one at a time

	    if(status == HISTO_TEST_INPROGRESS_EXT) {

            } else { //  HISTO_TEST_INPROGRESS or HISTO_TEST_CMPLT

              // row = EXTRACT_FROM_BITS_IN_WD(testinWd, ROW_SHIFT, ROW_BITS);
              // col = EXTRACT_FROM_BITS_IN_WD(testinWd, COL_SHIFT, COL_BITS);
              // chip = EXTRACT_FROM_BITS_IN_WD(testinWd, CHIP_SHIFT, chip_bits);
              // tot = EXTRACT_FROM_BITS_IN_WD(testinWd, tot_shift, tot_bits);
              // pixel = pixelsPerChip*chip/stepSize + col_size*(row-1) + col-1; // Chip numbering starts at 0, but row and columns at 1 !!
              // if(injections > 1)xil_printf("i = %d, testinWd = 0x%x, injections = %d, Pixel: %d, Chip: %d, Col: %d, Row: %d\r\n", \\
              //                               i, testinWd, injections, pixel, chip, col, row);
              // if((chip < 21 && row > 156) || (chip > 89 && row < 6))xil_printf("i = %d, fast = %d, testinWd = 0x%x, \\
              //                                                                   injections = %d ,Pixel: %d, Chip: %d, Col: %d, Row: %d\r\n", \\
              //                                                                   i, fast, testinWd, injections, pixel, chip, col, row);
	      for(j=0; j < injections; j++) {
	        *addr = testinWd;
	        verbmsg("test write 0x%x to 0x%x", testinWd, addr);
	      }
	    }
	  }
	  if(fast)i++;
	}
	if(status == HISTO_TEST_INPROGRESS || status == HISTO_TEST_INPROGRESS_NOHISTO ) cmd->cmd = SLV_CMD_IDLE; //  Needed?  Useful?  Harmful?

	if(status == HISTO_TEST_FINISH || status == HISTO_TEST_CMPLT) cmd->cmd = SLV_CMD_HIST_CMPLT;

	if(status == HISTO_TEST_FINISH_NOHISTO) {

		cmd->cmd = SLV_CMD_BUSY;

		verbmsg("Non-FwStop: Moving histogrammer %d into wait state",histoUnit);
		histoSetMode(histoUnit,MB_CTL_HIST_MODE_WAIT);
		verbmsg("Non-FwStop: Slave Control after wait  0x%x", *epcCtlReg);
		verbmsg("Non-FwStop: Slave Status  after wait  0x%x\n", *epcStatReg);

/* Invalidate the cache of the RAM region where histograms were moved by DMA */
		microblaze_invalidate_dcache_range((unsigned int) cb_item_data, (unsigned int) (dma.size[histoUnit]));
//  Append configuration information
		dma.size[histoUnit] = 4 + 4*((dma.size[histoUnit] - 1)/4); //  Must append on word boundary!
		*((IblSlvHistUnitCfg*) (((char*) cb_item_data) + dma.size[histoUnit])) = *pUnitCfg;
//  Put histogram_buffers into cb buffers
		cbPush(cb_th[histoUnit], item_th[histoUnit]);

#if 0 //debug on slave console, writes out "non-empty" memory
                        verbmsg("HISTO_TEST: %d",i);
                        printHistoReadout(pUnitCfg, dma.size[histoUnit], cb_item_data32, histoUnit);
#endif

		histoSetMode(histoUnit,MB_CTL_HIST_MODE_IDLE);

		histState = SLV_STAT_DATA;

		verbmsg("Non-Firmware Histo complete");
		dma.address[0] -= XPAR_MCB_DDR2_S0_AXI_BASEADDR;
		dma.address[1] -= XPAR_MCB_DDR2_S0_AXI_BASEADDR;
		dma.address[0] /= sizeof(long);
		dma.address[1] /= sizeof(long);

		// copy values. NB: we cannot use memcpy as the DPR is not byte accessible
		pDma = (IblSlvDmaTransfer*)&cmd->data;
		for (i=0; i < sizeof(IblSlvDmaTransfer)/sizeof(uint32_t); i++) {
		  ((uint32_t*)pDma)[i] = ((uint32_t*)&dma)[i];
		}
		cmd->count = sizeof(IblSlvDmaTransfer)/sizeof(long);

		cmd->cmd = SLV_CMD_ACK;
	}
#endif //  testHistogramming()
}

void startHistogramming() {

	int i;
	volatile uint32_t *histo0CtlReg = (volatile uint32_t *)MB_HIST0_CTL_REG;
	volatile uint32_t *histo1CtlReg = (volatile uint32_t *)MB_HIST1_CTL_REG;
	IblSlvHistUnitCfg *pUnitCfg;
	volatile uint32_t *pHistCtl;

	// reset potentially pending dma
	XAxiDma_Reset(&axiDma0);
	XAxiDma_Reset(&axiDma1);

	verbmsg("StartHisto: Slave control register: 0x%x", *epcCtlReg);
	verbmsg("StartHisto: Slave status register: 0x%x", *epcStatReg);

	// set configuration
	for (i = 0; i < 2; i++) {
		pUnitCfg = &histConfig.cfg[i];
		pHistCtl = (uint32_t*)(i == 0 ? histo0CtlReg : histo1CtlReg);
		
		if (pUnitCfg->enable == 1) {
			setupGeometry(i);
#ifndef HISTO_TEST
			verbmsg("\r\n*************************\r\n* Running in %s mode *\r\n*************************\r\n\r\n", isFei3 ? "FE-I3" : "FE-I4");
			verbmsg("StartHisto: Histogrammer %d enabled with binId = %d", i, pUnitCfg->binId);
#endif
			*pHistCtl = 0;

			// chipSel
			if (pUnitCfg->chipSel == 1) {
				BIT_SET(*pHistCtl, CHIPCTL_SHIFT);
			}

			// mStepEven and mStepOdd
                        checkMaskStepSettings(pUnitCfg);
                        uint32_t mStepEven = (pUnitCfg->mStepEven << mstepindex_shift_even);
                        uint32_t mStepOdd = (pUnitCfg->mStepOdd << MSTEPINDEX_SHIFT_ODD);
                        BITMASK_SET(*pHistCtl, mStepEven);
                        BITMASK_SET(*pHistCtl, mStepOdd);

			// maskStep
			switch (pUnitCfg->maskStep) {
				case ROWMASKNONE: 
					BITMASK_SET(*pHistCtl, ROWMASK_NONE);
					break;
       				case ROWMASK2:
					BITMASK_SET(*pHistCtl, ROWMASK_2);
					break;
				case ROWMASK4:
					BITMASK_SET(*pHistCtl, ROWMASK_4);
					break;
				case ROWMASK8:
					BITMASK_SET(*pHistCtl, ROWMASK_8);
					break;
				case ROWMASK16: //  Cannot be used until implemented in firmware
					BITMASK_SET(*pHistCtl, ROWMASK_16);
					break;
				case ROWMASK32: //  Only for FE-I3 use
					BITMASK_SET(*pHistCtl, ROWMASK_32);
				case ROWMASK64: //  Only for FE-I3 use (Firmware cannot handle 64 mask steps, so filtering done in software)
					BITMASK_SET(*pHistCtl, ROWMASK_32);
					break;
			}

			// HistType
			switch (pUnitCfg->type) {
				case ONLINE_OCCUPANCY:
					BITMASK_SET(*pHistCtl, opmode_online_occupancy);
					break;
				case OFFLINE_OCCUPANCY:
					BITMASK_SET(*pHistCtl, opmode_offline_occupancy);
					break;
				case SHORT_TOT:
					BITMASK_SET(*pHistCtl, opmode_short_tot);

					// set the expected occupancy value that is needed for SHORT_TOT scan
					uint32_t expectedOccupancy = pUnitCfg->expectedOccValue;
					if (expectedOccupancy > 0 && expectedOccupancy < (1 << EXPECTEDOCCCTL_BITS))
					{
       						BITMASK_SET(*pHistCtl, (expectedOccupancy << EXPECTEDOCCCTL_SHIFT));
					} else {
						verbmsg("StartHisto: Expected occupancy value %d is out of range. Must be at least 1 and maxmimum allowed value is %d", \
						expectedOccupancy, (1 << EXPECTEDOCCCTL_BITS) - 1);
       					}
					break;
				case LONG_TOT:
                                        BITMASK_SET(*pHistCtl, opmode_long_tot);
                                        break;
                                default:
                                        verbmsg("ERROR: Unsupported readout mode %d", pUnitCfg->type);
			}

                        // wait for histogrammers to move to SAMPLE state
                        if (0 == i) {
                            histoSetMode(i,MB_CTL_HIST_MODE_SAMPLE);
                            while (0 == (*epcStatReg & (1 << MB_STAT_HIST_RFD0_BIT))) {
                                verbmsg("StartHisto0: Slave control register: 0x%x", *epcCtlReg);
                                verbmsg("StartHisto0: Slave status register: 0x%x", *epcStatReg);
		                      verbmsg("StartHisto0: Wait for RFD0");
                            }
                        }
			else {
                            histoSetMode(i,MB_CTL_HIST_MODE_SAMPLE);
                            while (0 == (*epcStatReg & (1 << MB_STAT_HIST_RFD1_BIT))) {
                                verbmsg("StartHisto1: Slave control register: 0x%x", *epcCtlReg);
                                verbmsg("StartHisto1: Slave status register: 0x%x", *epcStatReg);
		                      verbmsg("StartHisto1: Wait for RFD1");
                            }
                        }

			verbmsg("StartHisto: Histoctlreg after Sample mode: 0x%x", *pHistCtl);
		        verbmsg("StartHisto: Slave control register after Sample mode: 0x%x", *epcCtlReg);
			verbmsg("StartHisto: Slave status register after Sample mode: 0x%x", *epcStatReg);
		}//enabled histo
	}//loop

	if(usingMUX) {
		// enable data input from boc
		if (1 == histConfig.cfg[0].enable) {
			*epcCtlReg |= (1 << MB_CTL_HIST_MUX0_BIT);
			verbmsg("Setting MB_CTRL_HIST_MUX0_BIT to enable");
		}
		if (1 == histConfig.cfg[1].enable) {
			*epcCtlReg |= (1 << MB_CTL_HIST_MUX1_BIT);
			verbmsg("Setting MB_CTRL_HIST_MUX1_BIT to enable");
		}

		verbmsg("StartHisto: Slave control register after enabling MUX: 0x%x", *epcCtlReg);
		verbmsg("StartHisto: Slave status register after enabling MUX: 0x%x", *epcStatReg);
	}

	// running
	histState = SLV_STAT_BUSY;
}

IblSlvDmaTransfer stopHistogramming() {
	int rc;
#ifndef HISTO_TEST
	circular_buffer_t* cb[2];
	cb_item_t* item[2];
#endif
	IblSlvDmaTransfer dma;
	u32 *dmaTarget[2] = {NULL, NULL};
	int dmaSize[2] = {0, 0};
	XAxiDma *axiDma;
	unsigned int dmaId;
	int i;
	volatile uint32_t *histo0CtlReg = (volatile uint32_t *)MB_HIST0_CTL_REG;
	volatile uint32_t *histo1CtlReg = (volatile uint32_t *)MB_HIST1_CTL_REG;
	IblSlvHistUnitCfg *pUnitCfg;
	volatile uint32_t *pHistCtl;
	int unitCfgOffset;


        if ((&histConfig.cfg[0])->enable == 1) {
		verbmsg("StopHisto0: Moving histogrammer 0 into wait state");
		histoSetMode(0,MB_CTL_HIST_MODE_WAIT);
		verbmsg("StopHisto0: Slave Control after wait  0x%x", *epcCtlReg);
		verbmsg("StopHisto0: Slave Status  after wait  0x%x\n", *epcStatReg);
        }

	if ((&histConfig.cfg[1])->enable == 1) {
		verbmsg("StopHisto1: Moving histogrammer 1 into wait state");
		histoSetMode(1,MB_CTL_HIST_MODE_WAIT);
		verbmsg("StopHisto1: Slave Control is 0x%x", *epcCtlReg);
		verbmsg("StopHisto1: Slave Status is 0x%x\n", *epcStatReg);
        }


	for (i = 0; i < 2; i++) {
		pHistCtl = (uint32_t*)(i == 0 ? histo0CtlReg : histo1CtlReg);
		pUnitCfg = &histConfig.cfg[i];
		if (1 == pUnitCfg->enable) {
			verbmsg("StopHisto: Histogrammer %d enabled to read out", i);
                        verbmsg("StopHisto: Histoctlreg at start of stopHistogramming = 0x%x", *pHistCtl);
			if (0 == i) {
				axiDma = &axiDma0;
				dmaId = XPAR_HISTO0_DMA_DEVICE_ID;                
                        }
			else {
				axiDma = &axiDma1;
				dmaId = XPAR_HISTO1_DMA_DEVICE_ID;
			}
			dmaSize[i] = getChipDataSize(i);
			verbmsg("StopHisto: DMA size is 0x%x bytes. buffer size is 0x%x bytes", dmaSize[i], maxPossiblePixels*4*2);

#ifdef HISTO_TEST
			dmaTarget[i] = (u32*) histogram_buffers[i];
#else
			// set buffer pointer
			cb[i] = &histogram_circular_buffers[i];
			unitCfgOffset = 4 + 4*((dmaSize[i] - 1)/4); //  Config struct must be appended on word boundary!
			item[i] = cbGetWriteItem(cb[i], unitCfgOffset + sizeof(*pUnitCfg));

			if (item[i] == NULL) {
				verbmsg("ERROR: StopHisto: Circular buffer of unit %d is full!", i);
				continue;
			}
			dmaTarget[i] = (u32*) item[i]->data;
#endif

			verbmsg("StopHisto: Current DMA buffer at 0x%x", dmaTarget[i]);

			// // clear buffer, TODO: necessary, performance?
			// for (size_t j = 0; j < dmaSize[i]/4; ++i) { //  Why does this not work ???
			//   dmaTarget[i][j] = 0;
			// }

			// init dma
			XAxiDma_Reset(axiDma);
			while(!XAxiDma_ResetIsDone(axiDma));
			verbmsg("StopHisto: DMA reset successful");

			rc = XAxiDma_CfgInitialize(axiDma, XAxiDma_LookupConfig(dmaId));
			verbmsg("StopHisto: DMA CfgInitialize returned %d", rc);

			// TODO: this still returns >0, relevant?
			rc = XAxiDma_Busy(axiDma, XAXIDMA_DEVICE_TO_DMA );
			verbmsg("StopHisto: DMA busy status: %d", rc);

			// start transfer
			verbmsg("StopHisto: Start DMA. Status: 0x%x", *epcStatReg);
			rc = XAxiDma_SimpleTransfer(axiDma, (u32)dmaTarget[i], dmaSize[i], XAXIDMA_DEVICE_TO_DMA );
			verbmsg("StopHisto: DMA transfer returned %d",rc);

			verbmsg("StopHisto: Transfer started. Status: 0x%x", *epcStatReg);

			// set readout mode and keep
			verbmsg("StopHisto: Slave Control before the RO is 0x%x", *epcCtlReg);
			verbmsg("StopHisto: Slave Status  before the RO is 0x%x", *epcStatReg);
			histoSetMode(i,MB_CTL_HIST_MODE_READOUT);
			verbmsg("StopHisto: Slave Control after RO is 0x%x", *epcCtlReg);
			verbmsg("StopHisto: Slave Status after RO: 0x%x", *epcStatReg);
		}
	}//loop

	for (i = 0; i < 2; i++) {
		pUnitCfg = &histConfig.cfg[i];
		if (1 == pUnitCfg->enable) {
			if (0 == i) {
				axiDma = &axiDma0;
				dmaId = XPAR_HISTO0_DMA_DEVICE_ID;
			}
			else {
				axiDma = &axiDma1;
				dmaId = XPAR_HISTO1_DMA_DEVICE_ID;
			}

			int statRegROCBit;
			if (0 == i) {
				statRegROCBit = MB_STAT_HIST_ROC0_BIT;
			}
			else if (1 == i) {
				statRegROCBit = MB_STAT_HIST_ROC1_BIT;
			}

			while (0 == (*epcStatReg & (1 << statRegROCBit))) {
				verbmsg("StopHisto %d: Waiting. Control is 0x%x", i, *epcCtlReg);
				verbmsg("StopHisto %d: Waiting. Status is 0x%x", i, *epcStatReg);
			}

			verbmsg("StopHisto %d: Readout complete. Status: 0x%x", i, *epcStatReg);

			// check how many bytes read
			rc = XAxiDma_ReadReg(axiDma->RegBase + (XAXIDMA_RX_OFFSET * XAXIDMA_DEVICE_TO_DMA), XAXIDMA_BUFFLEN_OFFSET);
			verbmsg("StopHisto: DMA transfersize: 0x%x", rc);
			if (rc != dmaSize[i]) {
				xil_printf("Warning: transfer sizes did not match: 0x%x instead of 0x%x bytes\r\n", rc, dmaSize[i]);
			}

			rc = XAxiDma_Busy(axiDma, XAXIDMA_DEVICE_TO_DMA );
			verbmsg("StopHisto: DMA busy status: %d", rc);

			/* Invalidate the cache of the RAM region where histograms were moved by DMA */
			microblaze_invalidate_dcache_range((unsigned int) dmaTarget[i], (unsigned int) (dmaSize[i]));

			// Append configuration information
			dmaSize[i] = 4 + 4*((dmaSize[i] - 1)/4); //  Must append on word boundary!
			*((IblSlvHistUnitCfg*) (((char*) dmaTarget[i]) + dmaSize[i])) = *pUnitCfg;

#ifndef HISTO_TEST
			// Finish writing circular buffer item
			cbPush(cb[i], item[i]);

#endif

#if 0 //debug on slave console, writes out "non-empty" memory
			verbmsg("HISTO: %d",i);
			printHistoReadout(pUnitCfg, dmaSize[i], dmaTarget[i], i);
#endif

			verbmsg("StopHisto: Histoctlreg before IDLE = 0x%x", *pHistCtl);
			verbmsg("StopHisto: Slave control register before IDLE: 0x%x", *epcCtlReg);
			verbmsg("StopHisto: Slave status register before IDLE: 0x%x", *epcStatReg);
			verbmsg("StopHisto: Moving histogrammer %d into idle state after the readout", i);
			histoSetMode(i,MB_CTL_HIST_MODE_IDLE);
			verbmsg("StopHisto: Slave control register after IDLE: 0x%x", *epcCtlReg);
			verbmsg("StopHisto: Slave status register after IDLE : 0x%x", *epcStatReg);

		} // enabled histo

		/* set details of DMA transfer */
		dma.address[i] = (u32) dmaTarget[i];
		dma.size[i] = dmaSize[i];

	} // loop over the histogram

	histState = SLV_STAT_DATA;
        return dma;
}

void abortHistogramming() {
    verbmsg("Resetting both histogramming units.");

    histoSetMode(0, MB_CTL_HIST_MODE_IDLE);                        
    histoSetMode(1, MB_CTL_HIST_MODE_IDLE);                        

    histState = SLV_STAT_OK;
}

void rdDmaStatus(int l) {
	unsigned int rc;
	
    // check dma status
    // TODO: change for dual dma
	rc = XAxiDma_ReadReg(axiDma0.RegBase + (XAXIDMA_RX_OFFSET * XAXIDMA_DEVICE_TO_DMA), XAXIDMA_SR_OFFSET);
	// dummy indicative write to stat reg
	*epcStatReg = rc;
	
    verbmsg("Dma status (line %d): 0x%x", l, rc);
	verbmsg("Local status (line %d): 0x%x", l, *epcStatReg);
}


void printHistoReadout(IblSlvHistUnitCfg* pUnitCfg, int dmaSize, volatile uint32_t *memory, int i) {
    int j = 0;
    uint32_t emptyData = 0x0;
    uint32_t data;

    if (pUnitCfg->type == SHORT_TOT) {
        emptyData = 0xF;
    }
    verbmsg("Histogrammer: Printing only values that are different from 0x%x", emptyData);
    switch (pUnitCfg->type) {
        case ONLINE_OCCUPANCY:
            for (j = 0; j < dmaSize/4; j++) {
                data = memory[j];
                if (data != emptyData && data > 0 && data < 256) {
                  verbmsg("value Histo %d 0x%x: \t occ %d \t at %d", i, data, data, j);
                }
            }
            break;
        case OFFLINE_OCCUPANCY:
            for (j = 0; j < dmaSize/4; j++) {
                data = memory[j];
                if (data != emptyData) {
                    verbmsg("value 0x%x: \t occ %d, \t occ %d, \t occ %d, \t occ %d \t at %d", data, 
                        (data >> 3*OCC_RESULT_BITS) & ((1 << OCC_RESULT_BITS) - 1), 
                        (data >> 2*OCC_RESULT_BITS) & ((1 << OCC_RESULT_BITS) - 1), 
                        (data >> 1*OCC_RESULT_BITS) & ((1 << OCC_RESULT_BITS) - 1), 
                        (data >> 0*OCC_RESULT_BITS) & ((1 << OCC_RESULT_BITS) - 1), j);
                }
            }
            break;
        case SHORT_TOT:
            for (j = 0; j < dmaSize/4; j++) {
                data = memory[j];
                if (data != emptyData) {
                    verbmsg("value 0x%x: \t missing triggers %d, \t tot %d, \t tot² %d \t at %d", data, 
                        (data >> ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT) & ((1 << (MISSING_TRIGGERS_RESULT_BITS)) - 1),
                        (data >> ONEWORD_TOT_RESULT_SHIFT) & ((1 << (tot_result_bits)) - 1),
                        (data >> ONEWORD_TOTSQR_RESULT_SHIFT) & ((1 << (totsqr_result_bits)) - 1), j);
                }
            }
            break;
        case LONG_TOT:
            for (j = 0; j < dmaSize/4; j++) {
                data = memory[j];
                if (data != emptyData) {
                    if (j % 2 == 0) {
                        // first partial data word
                        verbmsg("word 1 value 0x%x: \t occ %d, \t tot %d \t at %d", data,
                        (data >> TWOWORD_OCC_RESULT_SHIFT) & ((1 << OCC_RESULT_BITS) - 1),
                        (data >> twoword_tot_result_shift) & ((1 << (tot_result_bits)) - 1), j);
                    }
                    else {
                        // second partial word
                        verbmsg("word 2 value 0x%x: \t tot² %d \t at %d", data,
                        (data >> twoword_totsqr_result_shift) & ((1 << (totsqr_result_bits)) - 1), j);
                    }
                }
            }
            break;
    }   
}

