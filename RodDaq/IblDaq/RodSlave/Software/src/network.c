/*
 * Filename:    network.c
 * Date:        February 2014
 * Author:      Lohse (h.lohse@stud.uni-heidelberg.de)
 *
 * Summary:     See header file
 */

#include "network.h"
#include "memoryManagement.h"
#include "platform.h"
#include "platform_config.h"
#include "histClient.h"
#include "xparameters.h"
#include <string.h>

network_t network;

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

// Helper function declarations

static bool isIdentical(const IblSlvNetCfg* configuration);
static void useConfiguration(const IblSlvNetCfg* configuration);
static void setUpInterface();
static void updateInterface();

static void openTcpConnection(connection_t* connection);
static err_t closeTcpConnection(connection_t* connection);

static err_t connectedCallback(void* argument, struct tcp_pcb* pcb, err_t error);
static err_t sentCallback(void* argument, struct tcp_pcb* pcb, u16_t length);
static err_t receiveCallback(void* argument, struct tcp_pcb* pcb, struct pbuf* p, err_t err);
static void errorCallback(void* argument, err_t error);

static void copyMac(unsigned char* destination, const uint32_t* source);
static void copyIp4(struct ip_addr* destination, const uint32_t source);
static void printMac(const char* message, const unsigned char* mac);
static void printIp(const char* message, const struct ip_addr* ip);

static unsigned char ip4_octet1(uint32_t ip4);
static unsigned char ip4_octet2(uint32_t ip4);
static unsigned char ip4_octet3(uint32_t ip4);
static unsigned char ip4_octet4(uint32_t ip4);
// ---

void networkInitialize(struct netif* interface)
{
    memset(&network, 0, sizeof(network));
    network.interface = interface;
}

void networkReset()
{
    int i;
    for (i = 0; i < NETWORK_NUM_CONNECTIONS; ++i) {
        if (network.connections[i].connected) {
            closeTcpConnection(&network.connections[i]);
        }

        // Implies networkDeactivateConnection(&network.connections[i]);
        memset(&network.connections[i], 0, sizeof(network.connections[i]));
    }
    
    network.connection_attempts = 0;
}

bool networkIsActiveConnection(connection_t* connection)
{
    return connection->host_ip.addr != 0;
}


void networkDeactivateConnection(connection_t* connection)
{
    connection->host_ip.addr = 0;

    if (connection->connected) {
        closeTcpConnection(connection);
    }
}

void networkApply(const IblSlvNetCfg* configuration)
{
    if (network.enabled) {
        if (!isIdentical(configuration) || !networkIsConnected()) {
            networkReset();
            useConfiguration(configuration);
            updateInterface();
        }
    }
    else {
        useConfiguration(configuration);
        setUpInterface();
    }

    network.connection_attempts = 0;
}

void setUpInterface()
{
    if (!xemac_add(network.interface,
                   &network.local_ip,
                   &network.subnet_mask,
                   &network.gateway_ip,
                   network.mac_address,
                   PLATFORM_EMAC_BASEADDR))
    {
		verbmsg("NET: Error adding N/W interface");
		return;
	}
	
    netif_set_default(network.interface);
    
    platform_enable_interrupts();
    netif_set_up(network.interface);

    network.enabled = true;
}

void updateInterface()
{
    int i;
    
    platform_disable_interrupts();
    netif_set_down(network.interface);
    
    netif_set_addr(network.interface,
                   &network.local_ip,
                   &network.subnet_mask,
                   &network.gateway_ip);
    
    for (i = 0; i < 6; ++i) {
        network.interface->hwaddr[i] = network.mac_address[i];
    }

    platform_enable_interrupts();
    netif_set_up(network.interface);
}

bool networkIsActive()
{
    int i;

    if (!network.enabled) {
        return false;
    }

    for (i = 0; i < NETWORK_NUM_CONNECTIONS; ++i) {
        if (networkIsActiveConnection(&network.connections[i])) {
            return true;
        }
    }

    return false;
}

bool networkIsConnected()
{
    int i;
    int inactive_connections = 0;

    if (!network.enabled) {
        return false;
    }

    for (i = 0; i < NETWORK_NUM_CONNECTIONS; ++i) {
        if (networkIsActiveConnection(&network.connections[i])) {
            if (!network.connections[i].connected) {
                return false;
            }
        }
        else {
            inactive_connections++;
        }
    }

    // Must at least have 1 active connection anyway
    return inactive_connections != NETWORK_NUM_CONNECTIONS;
}

void networkTryConnect()
{
    int i;

    network.connection_attempts++;
    
    if (!network.enabled) {
        return;
    }
        
    for (i = 0; i < NETWORK_NUM_CONNECTIONS; i++) {
        connection_t* connection = &network.connections[i];

        if (networkIsActiveConnection(connection) && !connection->connected) {
            verbmsg("NET: Waiting for connection.");
            
            openTcpConnection(connection);

            while (connection->waiting_for_connection) {
                xemacif_input(network.interface);
                checkTcpTimers();
            }

            if (!connection->connected) {
                continue;
            }

            verbmsg("NET: Waiting for buffer space.");
            
            while (connection->connected
            &&  tcp_sndbuf(connection->tcp) == 0)
            {
                xemacif_input(network.interface);
                checkTcpTimers();
            }
        }
    }
}

void openTcpConnection(connection_t* connection)
{
    err_t error;
    connection->waiting_for_connection = true;

    connection->tcp = tcp_new();
    if (!connection->tcp) {
		verbmsg("NET: histo: Error creating PCB");
        connection->waiting_for_connection = false;
        return;
    }

	printIp("NET: connecting to fitfarm server at ",&connection->host_ip);
   
    tcp_arg(connection->tcp, connection);
	tcp_sent(connection->tcp, sentCallback);
    tcp_recv(connection->tcp, receiveCallback);
    tcp_err(connection->tcp, errorCallback);

    error = tcp_connect(
        connection->tcp,
        &connection->host_ip,
        connection->host_port,
        connectedCallback);

	if (error != ERR_OK) {
		verbmsg("NET: Error %d enqueueing SYN (tcp_connect)\r\n", error);
        connection->waiting_for_connection = false;
        return;
	}
}

err_t connectedCallback(void* argument, struct tcp_pcb* pcb, err_t error)
{
    connection_t* connection = (connection_t*) argument;

    if (error == ERR_OK) {
        verbmsg("NET: histo: Connected to fit server");
        connection->connected = true;
    }
    else {
        verbmsg("NET: histo: Could not connect to fit server");
    }
    
    connection->waiting_for_connection = false;

	return error;
}

err_t closeTcpConnection(connection_t* connection)
{
    verbmsg("NET: Attempting to close TCP connection.");
    
    connection->connected = false;
    err_t error;
    
    tcp_recv(connection->tcp, NULL);
    error = tcp_close(connection->tcp);

    if (error != ERR_OK) {
        verbmsg("NET: Close failed with error %d; aborting connection.", error);
        tcp_abort(connection->tcp);
        return ERR_ABRT;
    }
    else {
        verbmsg("NET: Closed connection successfully.");
        return ERR_OK;
    }
}

u16_t networkWrite(connection_t* connection, void* data, const u16_t length, const bool copy)
{
    u16_t available_buffer;
    u16_t write_length;
    err_t error;

    if (!connection->connected) {
        verbmsg("NET: Cannot write to TCP send buffer; no connection!");
        return 0;
    }

    available_buffer = tcp_sndbuf(connection->tcp);
    write_length = MIN(available_buffer, length);

    error = tcp_write(connection->tcp, data, write_length, copy);

    if (error == ERR_MEM) {
        return 0;
    }
    else if (error != ERR_OK) {
        verbmsg("NET: Error %d on tcp_write!", error);
        return 0;
    }

    return write_length;
}

err_t sentCallback(void* argument, struct tcp_pcb* pcb, u16_t length)
{
    connection_t* connection = (connection_t*) argument;

    if (network.sent_callback != NULL) {
        network.sent_callback(connection->sent_args, length);
    }
    else {
        verbmsg("NET: Must specify network.sent_callback!");
    }

    return ERR_OK;
}

// Handles host closing connection, discards other data received
err_t receiveCallback(void* argument, struct tcp_pcb* pcb, struct pbuf* p, err_t err)
{
    connection_t* connection = (connection_t*) argument;

    if (p == NULL) {
        verbmsg("NET: Host closed our connection.");
        connection->connected = false;
        connection->waiting_for_connection = false;
       
        if (closeTcpConnection(connection) != ERR_OK) {
            return ERR_ABRT;
        }
        else {
            return ERR_OK;
        }
    }
    else {
        verbmsg("NET: Received data from host; discarding.");
        pbuf_free(p);
        return ERR_OK;
    }
}

/*  Is called in case of error:
 *  - If host isn't listening, called with ERR_RST
 *  - If no cable is plugged in during tcp_open, called with ERR_ABRT
 */
void errorCallback(void* argument, err_t error)
{
    connection_t* connection = (connection_t*) argument;

    if (connection->connected) {
        closeTcpConnection(connection);
    }

    connection->connected = false;
    connection->waiting_for_connection = false;

    verbmsg("NET: Error %d occured on used connection!", error);
}

bool isIdentical(const IblSlvNetCfg* configuration)
{
    return memcmp(&network.configuration, configuration, sizeof(*configuration)) == 0;
}

void useConfiguration(const IblSlvNetCfg* configuration)
{
    network.configuration = *configuration;

    copyMac(network.mac_address, configuration->localMac);

    IP4_ADDR(
        &network.local_ip, 
        ip4_octet1(configuration->localIp),
        ip4_octet2(configuration->localIp),
        ip4_octet3(configuration->localIp),
        ip4_octet4(configuration->localIp));

    IP4_ADDR(
        &network.gateway_ip, 
        ip4_octet1(configuration->gwIp),
        ip4_octet2(configuration->gwIp),
        ip4_octet3(configuration->gwIp),
        ip4_octet4(configuration->gwIp));

    IP4_ADDR(
        &network.subnet_mask, 
        ip4_octet1(configuration->maskIp),
        ip4_octet2(configuration->maskIp),
        ip4_octet3(configuration->maskIp),
        ip4_octet4(configuration->maskIp));

    IP4_ADDR(
        &network.connections[0].host_ip, 
        ip4_octet1(configuration->targetIpA),
        ip4_octet2(configuration->targetIpA),
        ip4_octet3(configuration->targetIpA),
        ip4_octet4(configuration->targetIpA));
    network.connections[0].host_port = (u16_t) configuration->targetPortA;
    
    IP4_ADDR(
        &network.connections[1].host_ip, 
        ip4_octet1(configuration->targetIpB),
        ip4_octet2(configuration->targetIpB),
        ip4_octet3(configuration->targetIpB),
        ip4_octet4(configuration->targetIpB));
    network.connections[1].host_port = (u16_t) configuration->targetPortB;
}

void copyMac(unsigned char* destination, const uint32_t* source)
{
    destination[0] = (source[0] >> 8) & 0xff;
    destination[1] = (source[0] >> 0) & 0xff;
    destination[2] = (source[1] >> 24) & 0xff;
    destination[3] = (source[1] >> 16) & 0xff;
    destination[4] = (source[1] >> 8) & 0xff;
    destination[5] = (source[1] >> 0) & 0xff;
}

void copyIp4(struct ip_addr* destination, const uint32_t source)
{
    destination->addr = source;
}

void networkPrint()
{
    printMac("Board MAC:       ", network.mac_address);
	printIp("Board IP :       ", &network.local_ip);
	printIp("Gateway  :       ", &network.gateway_ip);
	printIp("Netmask  :       ", &network.subnet_mask);
    verbmsg("Interface %s\r\n", (network.enabled ? "up" : "down"));
    
    printIp("Host IP A:       ", &network.connections[0].host_ip);
    verbmsg("Host Port A:     %d\r\n", network.connections[0].host_port);
    verbmsg("TCP to A %s\r\n", (network.connections[0].connected ? "connected" : "closed"));
    
    printIp("Host IP B:       ", &network.connections[1].host_ip);
    verbmsg("Host Port B:     %d\r\n", network.connections[1].host_port);
    verbmsg("TCP to B %s\r\n", (network.connections[1].connected ? "connected" : "closed"));
}

void printMac(const char* message, const unsigned char* mac)
{
    verbmsg("%s%02x-%02x-%02x-%02x-%02x-%02x\r\n",
            message,
            mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
}

void printIp(const char* message, const struct ip_addr* ip)
{
	verbmsg("%s%d.%d.%d.%d\r\n",
            message,
            ip4_addr1(ip), ip4_addr2(ip), ip4_addr3(ip), ip4_addr4(ip));
}


/* These functions return IP4 octets from an uint32_t as expected by the slave. */
unsigned char ip4_octet1(uint32_t ip4) {
    return (ip4 >> 24 & 0xff);
}

unsigned char ip4_octet2(uint32_t ip4) {
    return (ip4 >> 16 & 0xff);
}

unsigned char ip4_octet3(uint32_t ip4) {
    return (ip4 >> 8 & 0xff);
}

unsigned char ip4_octet4(uint32_t ip4) {
    return (ip4 & 0xff);
}


void handleNetworkError()
{ 
    cbClear(&histogram_circular_buffers[0]);
    cbClear(&histogram_circular_buffers[1]);

    networkReset();
    initializeHistogramTransfer();
}


