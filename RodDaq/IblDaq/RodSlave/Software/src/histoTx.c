/*
 * Filename:   histoTx.c
 * Author:     Kugel
 *
 * Summary:
 *      See header.
 */

#include "histoTx.h"
#include "histClient.h"
#include "rodHisto.hxx"
#include "network.h"
#include "memoryManagement.h"
#include <string.h>

#define MIN(x, y) (((x) < (y)) ? (x) : (y))

histo_tx_t histo_txs[2];

static void popSentItems(histo_tx_t* histo_tx);
static void transmit(histo_tx_t* histo_tx);
static void continueItem(histo_tx_t* histo_tx, const uint32_t unbuffered_bytes);
static cb_item_t* nextItem(histo_tx_t* histo_tx);
static void transmitNextItemHeader(histo_tx_t* histo_tx);
static void transmitItemData(histo_tx_t* histo_tx);
static void setBufferingItemsHeaderCommand(histo_tx_t* histo_tx);
static void sent(void* sent_args, const u16_t length);

void histoTxSetup()
{
    network.sent_callback = sent;
}

void histoTxInitialize(histo_tx_t* histo_tx,
                       connection_t* connection,
                       circular_buffer_t* buffer)
{
    memset(histo_tx, 0, sizeof(*histo_tx));

    histo_tx->connection = connection;
    histo_tx->buffer     = buffer;

    histo_tx->connection->sent_args = histo_tx;
}

void histoTxTransmit(histo_tx_t* histo_tx)
{
    if (!cbIsEmpty(histo_tx->buffer)) {
        popSentItems(histo_tx);

        if (networkIsActive(histo_tx->connection)) {
            transmit(histo_tx);
        }
    }
}

void popSentItems(histo_tx_t* histo_tx)
{
    while (true) {
        const cb_item_t* oldest_item = cbGetReadItem(histo_tx->buffer);
        const uint32_t bytes_to_send =
              oldest_item->size
            - sizeof(IblSlvHistUnitCfg)
            + sizeof(histo_tx->header_command);

        if (histo_tx->sent_bytes >= bytes_to_send) {
            histo_tx->sent_bytes -= bytes_to_send;
            cbPop(histo_tx->buffer);

            if (cbIsEmpty(histo_tx->buffer)) {
                histo_tx->buffering_item = NULL;
                break;
            }
        }
        else {
            break;
        }
    }
}

void transmit(histo_tx_t* histo_tx)
{
    const uint32_t unbuffered_bytes = histo_tx->data_size - histo_tx->buffered_bytes;

    if (unbuffered_bytes > 0) {
        continueItem(histo_tx, unbuffered_bytes);
    }
    else {
        if (histo_tx->is_sending_header_command) {
            transmitItemData(histo_tx);
        }
        else {
            transmitNextItemHeader(histo_tx);
        }
    }
}

void continueItem(histo_tx_t* histo_tx, const uint32_t unbuffered_bytes)
{
    void* data = histo_tx->data + histo_tx->buffered_bytes;
    const u16_t bytes_to_buffer = MIN(unbuffered_bytes, 0xFFFF);
    const bool copy = histo_tx->is_sending_header_command;

    histo_tx->buffered_bytes += networkWrite(
        histo_tx->connection,
        data,
        bytes_to_buffer,
        copy);
}

cb_item_t* nextItem(histo_tx_t* histo_tx)
{
    if (histo_tx->buffering_item == NULL) {
        return cbGetReadItem(histo_tx->buffer);
    }
    else {
        return cbGetReadItemBehind(histo_tx->buffer, histo_tx->buffering_item);
    }
}

void transmitNextItemHeader(histo_tx_t* histo_tx)
{
    cb_item_t* next_item = nextItem(histo_tx);

    if (next_item == NULL) {
        return;
    }
    else {
        histo_tx->buffering_item = next_item;
    }

    setBufferingItemsHeaderCommand(histo_tx);

    histo_tx->data = (uint8_t*) &histo_tx->header_command;
    histo_tx->data_size = sizeof(histo_tx->header_command);
    histo_tx->buffered_bytes = 0;

    histo_tx->is_sending_header_command = true;
}

void transmitItemData(histo_tx_t* histo_tx)
{
    histo_tx->data = (uint8_t*) histo_tx->buffering_item->data;
    histo_tx->data_size = histo_tx->buffering_item->size
        - sizeof(IblSlvHistUnitCfg);
    histo_tx->buffered_bytes = 0;
    
    histo_tx->is_sending_header_command = false;
}

void setBufferingItemsHeaderCommand(histo_tx_t* histo_tx)
{
    // Read config at back, after data
    const uint32_t cfg_off = histo_tx->buffering_item->size
        - sizeof(IblSlvHistUnitCfg);
    const uint32_t scanId = ((IblSlvHistUnitCfg*)
        (((char*) histo_tx->buffering_item->data) + cfg_off))->scanId;
    const uint32_t binId = ((IblSlvHistUnitCfg*)
        (((char*) histo_tx->buffering_item->data) + cfg_off))->binId;
    const uint32_t maskId = ((IblSlvHistUnitCfg*)
        (((char*) histo_tx->buffering_item->data) + cfg_off))->maskId;
    verbmsg("histoTx binId = %d, histo_tx->buffering_item->size = %d, cfg_off = %d",binId, histo_tx->buffering_item->size, cfg_off);

    histo_tx->header_command.magic       = htonl(SLVNET_MAGIC);
    histo_tx->header_command.command     = htonl(SLVNET_HIST_DATA_CMD);
    histo_tx->header_command.bins        = htonl(binId);
    histo_tx->header_command.scanId      = htonl(scanId);
    histo_tx->header_command.maskId      = htonl(maskId);
    histo_tx->header_command.payloadSize = htonl(cfg_off);
}

void sent(void* sent_args, const u16_t length)
{
    histo_tx_t* histo_tx = (histo_tx_t*) sent_args;
    histo_tx->sent_bytes += (uint32_t) length;
}

