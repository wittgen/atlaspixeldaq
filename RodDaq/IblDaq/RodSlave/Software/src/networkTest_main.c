/*
 * Filename:   histClient_main.c
 * Author:     Kugel
 *
 * Summary:
 *   This file contains the main loop of the histoClient program
 *   running on a MicroBlaze CPU implemented on the ROD slave FPGAs.
 *   It sets up the environment of the embedded system.
 *   The main loop processes commands received by the ROD master and
 *   takes care of networking connectivity.
 */

#include <stdio.h>
#include <string.h>

#include "xparameters.h"
#include "xintc_l.h"
#include "netif/xadapter.h"

#include "platform.h"
#include "platform_config.h"
void disable_caches(); // not declared via platform.h

#define MAIN
#include "histClient.h"
#include "rodSlave.hxx"
#include "rodHisto.hxx"
#include "network.h"
#include "histoTx.h"
#include "memoryManagement.h"
#include "lwip/init.h"

void printWelcomeMsg() {
    int i = 0;

    xil_printf("\r\n");
    xil_printf("\r\n");
    xil_printf("---- IBL ROD Slave Software ----\r\n");

    /* Print the slave firmware and software git hashes. */
    IblSlvHashes gitHashes;
    fillHashes(&gitHashes);
    xil_printf("FW hash: ");
    for (i = 0; i < 5; i++) {
        xil_printf("%x", gitHashes.firmwareHash[i]);
    }
    xil_printf("\r\n");

    xil_printf("SW hash: ");
    for (i = 0; i < 5; i++) {
        xil_printf("%x", gitHashes.softwareHash[i]);
    }
    xil_printf("\r\n");

    xil_printf("--------\r\n");
}

void setIp(
    char* message,
    uint32_t* ip,
    unsigned char ip1,
    unsigned char ip2,
    unsigned char ip3,
    unsigned char ip4)
{
    uint32_t new_ip = (ip1 << 24) + (ip2 << 16) + (ip3 << 8) + ip4;
    *ip = new_ip;

    verbmsg("SLV_CMD_NET_CFG_SET %s IP: %d.%d.%d.%d",
            message, ip1, ip2, ip3, ip4);
}

void setLocalMac(
    IblSlvNetCfg* command,
    unsigned char mac1,
    unsigned char mac2,
    unsigned char mac3,
    unsigned char mac4,
    unsigned char mac5,
    unsigned char mac6)
{
    uint32_t mac_upper = (mac1 << 8) + mac2;
    uint32_t mac_lower = (mac3 << 24) + (mac4 << 16) + (mac5 << 8) + mac6;

    command->localMac[0] = mac_upper;
    command->localMac[1] = mac_lower;

    verbmsg("SLV_CMD_NET_CFG_SET MAC: %02x-%02x-%02x-%02x-%02x-%02x",
            mac1, mac2, mac3, mac4, mac5, mac6);
}
            
void prepareCmdNetCfgSet(
    unsigned char local_ip3,
    unsigned char target_ip3,
    unsigned char mac)
{
    verbmsg("Preparing SLV_CMD_NET_CFG_SET");
    
    IblSlvNetCfg* command = (IblSlvNetCfg*) cmd;
    command->cmd = SLV_CMD_NET_CFG_SET;
    
    setLocalMac(command, mac, mac, mac, mac, mac, mac);
    setIp("local",   &command->localIp,   192, 168, 1, local_ip3);
    setIp("target",  &command->targetIpA, 192, 168, 1, target_ip3);
    setIp("target",  &command->targetIpB, 192, 168, 1, target_ip3);
    setIp("gateway", &command->gwIp,      192, 168, 0, 1);
    setIp("netmask", &command->maskIp,    255, 255, 0, 0);
    
    command->targetPortA = 5001;
    command->targetPortB = 5002;
    verbmsg("SLV_CMD_NET_CFG_SET port: %d", command->targetPortA);
    verbmsg("SLV_CMD_NET_CFG_SET port: %d", command->targetPortB);
}

void prepareCmdHistCfgSet()
{
    IblSlvHistCfg* command = (IblSlvHistCfg*) cmd;
    command->cmd = SLV_CMD_HIST_CFG_SET;

    command->cfg[0].nChips = 8;
    command->cfg[0].enable = 1;
    command->cfg[0].type = LONG_TOT;
    command->cfg[0].maskStep = ROWMASKNONE;
    command->cfg[0].chipSel = 1;
    command->cfg[0].expectedOccValue = 6;
    command->cfg[0].addrRange = 0;
    command->cfg[0].scanId = 0xDEADBEEF;
    command->cfg[0].binId = 1;

    command->cfg[1].enable = 8;
    command->cfg[1].type =  LONG_TOT;
    command->cfg[1].maskStep = ROWMASKNONE;
    command->cfg[1].chipSel = 1;
    command->cfg[1].expectedOccValue = 101;
    command->cfg[1].addrRange = 0;
    command->cfg[1].scanId = 0xDEADBEEF;
    command->cfg[1].binId = 1;
}

int main() {
	int loop = 0;
	int i = 0;
	// setup vectors and epc. do before init_platform
	XIntc_MasterDisable(XPAR_INTC_0_BASEADDR);
	disable_caches();
	setupAddresses();

	init_platform();
	// Added to enable the DATA read out coming from the BOC;
	// otherwise the ROD expects the FEI4 connected locally

	//*epcCtlReg |= (1 << CTL_BOC_ENABLE_BIT);
	BIT_SET(*epcCtlReg, MB_CTL_BOC_ENABLE_BIT);
	
	// default verbose
	verbose = 1;
    
    printWelcomeMsg();

    // Initialize DMA-aligned circular buffers
    setupHistoBuffer(0);
    setupHistoBuffer(1);
    
    // Initialize network
    lwip_init();
    memset(&netConfig, 0, sizeof(IblSlvNetCfg));
    networkInitialize(&server_netif);
    initializeHistogramTransfer();

	// init hist state
	histState = SLV_STAT_OK; 	// idle
	memset(&histConfig, 0, sizeof(IblSlvHistCfg));

	if (!simulation) print("ROD Slave histogram client\r\n");
    prepareCmdHistCfgSet();
    cmdLoop();

    prepareCmdNetCfgSet(11, 9, 0xA);
    cmdLoop();

	/* receive and process packets */
	while (1) {
		cmdLoop();
        
        if (networkIsConnected()) {
            xemacif_input(network.interface);
            transferHistogramData();
        }
       
        if (++i > 1000000)
        {
            i = 0;

            verbmsg("Slave %d command loop %d, A connected = %d, B connected = %d",
                slaveId, loop++, network.connections[0].connected, network.connections[1].connected);
            
            if (!networkIsConnected()) {
                if (loop % 10 != 0) {
                    networkTryConnect();
                }
                else {
                    handleNetworkError();
                }
            }
        }

		checkTcpTimers();
	}

	/* never reached */
	cleanup_platform();

	return 0;
}
