/*
 * Filename:   cmdLoop.c
 * Author:     Kugel
 *
 * Summary:
 *   The loop processes commands that were sent by the ROD master.
 * TODO: check if memcpy substitute is safe
 */

#include <string.h>
#include <unistd.h>

#include "histClient.h"
#include "rodSlave.hxx"
#include "network.h"
#include "memoryManagement.h"
#include "histoTx.h"

#include "lwip/tcp.h" // for timer stuff

void checkTcpTimers() {
	if (fastTimerFlag == 1) {
		tcp_fasttmr();
		fastTimerFlag = 0;
	}
	if (slowTimerFlag == 1) {
		tcp_slowtmr();
		slowTimerFlag = 0;
	}
}

void cmdLoop() {
	IblSlvHistCfg *pHistCfg;
	IblSlvNetCfg *pNetCfg;
        IblSlvDmaTransfer *pDma;

	uint32_t statType;
	uint32_t crcVal;
	void (*startAddr)();
	int i, count, histNum;
	uint32_t *addr, *data;

	// command loop
	switch (cmd->cmd) {
		case SLV_CMD_IDLE:
		case SLV_CMD_BUSY:
		case SLV_CMD_ACK:
		case SLV_CMD_ALIVE:
			break;

		case SLV_CMD_VERBOSE:
			verbose  = cmd->data?1:0;
			xil_printf("Verbose set to %d\r\n",verbose);
			cmd->cmd = SLV_CMD_IDLE;
			break;

		case SLV_CMD_CRC:
			verbmsg("Comupting crc");
			addr = (uint32_t*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
			crc32((char *)addr, cmd->data*sizeof(uint32_t), &crcVal);
			verbmsg("Crc from address 0x%x, 0x%x words: 0x%x",addr, cmd->data, crcVal);
			data = (uint32_t*)&cmd->data;
			data[0] = ENDIAN_FLAG;
			data[1] = crcVal;
			cmd->count = 2;
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_STAT:
			verbmsg("Status request");
			statType =  cmd->data; // get status type
			cmdStat = (iblSlvStat*)&cmd->data;
			cmdStat->progType = SLV_TYPE_PROG;
			cmdStat->progVer = 1;
			switch (statType) {
				case SLV_STAT_TYPE_STD:
					cmdStat->status = SLV_STAT_OK;
					break;
				case SLV_STAT_TYPE_NET:
					cmdStat->status = network.enabled ? SLV_STAT_BUSY : SLV_STAT_OK;
					break;
				case SLV_STAT_TYPE_FIT:
					cmdStat->status = networkIsConnected() ? SLV_STAT_BUSY : SLV_STAT_OK;
					break;
				case SLV_STAT_TYPE_HIST:
					cmdStat->status = histState;
					break;
				default:
					cmdStat->status = SLV_STAT_ERROR;
					break;
			}
			cmd->count = sizeof(iblSlvStat)/sizeof(long);
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_WRITE:
			verbmsg("Write");
			addr = (uint32_t*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
			count = cmd->count;
			data = (uint32_t*)&cmd->data;
			for (i=0; i<count; i++) {
				addr[i] = data[i];
				verbmsg("Write 0x%x at 0x%x",addr[i],&addr[i]);
			}
			cmd->cmd = SLV_CMD_IDLE;
			break;

		case SLV_CMD_READ:
			verbmsg("Read\r\n",slaveId);
			addr = (uint32_t*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
			count = cmd->count;
			data = (uint32_t*)&cmd->data;
			for (i=0; i<count; i++) {
				data[i] = addr[i];
				verbmsg("Read 0x%x at 0x%x",addr[i],&addr[i]);
			}
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_START:
			verbmsg("Start");
			addr = (uint32_t*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
			startAddr = (void (*)())addr;
			xil_printf("[%d:] Branching to address 0x%x\r\n",slaveId,startAddr);
			cmd->cmd = SLV_CMD_IDLE;
			(*startAddr)(); // jump
			break;

		case SLV_CMD_NET_CFG_SET:
			verbmsg("NetCfg set");
			cmd->cmd = SLV_CMD_BUSY;
            
			pNetCfg = (IblSlvNetCfg*) cmd;
			// copy values. NB: No memcpy as the DPR is not byte accessible
			for (i=0; i < sizeof(IblSlvNetCfg)/sizeof(uint32_t); i++) {
				((uint32_t*)&netConfig)[i] = ((uint32_t*)pNetCfg)[i];
			}
				
			networkApply(pNetCfg);
			networkTryConnect();
			//networkDeactivate(&network.connections[1]);

			cmd->cmd = SLV_CMD_IDLE;
			networkPrint();
			break;

		case SLV_CMD_NET_CFG_GET:
			verbmsg("NetCfg get");
			pNetCfg = (IblSlvNetCfg*)&cmd->data;	// map to netconfig structure
			// copy values. NB: we cannot use memcpy as the DPR is not byte accessible
			for (i=0; i < sizeof(IblSlvNetCfg)/sizeof(uint32_t); i++) {
				((uint32_t*)pNetCfg)[i] = ((uint32_t*)&netConfig)[i];
			}
				//  Hard-coded TMPFIX !!! 
				((uint32_t*)&netConfig)[0] = ((uint32_t*)pNetCfg)[0];
				((uint32_t*)&netConfig)[7] = ((uint32_t*)pNetCfg)[7];
			cmd->count = sizeof(IblSlvNetCfg)/sizeof(long);
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_HIST_CFG_SET:
			verbmsg("HistoCfg set");
			pHistCfg = (IblSlvHistCfg*)cmd;
			// copy values. NB: we cannot use memcpy as the DPR is not byte accessible
			for (i=0; i < sizeof(IblSlvHistCfg)/sizeof(uint32_t); i++) {
				((uint32_t*)&histConfig)[i] = ((uint32_t*)pHistCfg)[i];
			verbmsg("HistoCfg set histConfig[%d] is 0x%x",i,((uint32_t*)&histConfig)[i]);
			}
			cmd->cmd = SLV_CMD_BUSY;
			startHistogramming();
			cmd->cmd = SLV_CMD_IDLE;
			break;

		case SLV_CMD_HIST_CFG_GET:
			verbmsg("HistoCfg get");
			pHistCfg = (IblSlvHistCfg*)&cmd->data;
			// copy values. NB: we cannot use memcpy as the DPR is not byte accessible
			for (i=0; i < sizeof(IblSlvHistCfg)/sizeof(uint32_t); i++) {
				((uint32_t*)pHistCfg)[i] = ((uint32_t*)&histConfig)[i];
			}
			cmd->count = sizeof(IblSlvHistCfg)/sizeof(long);
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_HIST_CMPLT:
			verbmsg("Histo complete");
			cmd->cmd = SLV_CMD_BUSY;

			/* read out the histogrammers and transfer data via network if needed */
			IblSlvDmaTransfer dma = stopHistogramming();

                        /* adjust for base address offset word size */
                        dma.address[0] -= XPAR_MCB_DDR2_S0_AXI_BASEADDR;
                        dma.address[1] -= XPAR_MCB_DDR2_S0_AXI_BASEADDR;
                        dma.address[0] /= sizeof(long);
                        dma.address[1] /= sizeof(long);
			
                        // copy values. NB: we cannot use memcpy as the DPR is not byte accessible
			pDma = (IblSlvDmaTransfer*)&cmd->data;
			for (i=0; i < sizeof(IblSlvDmaTransfer)/sizeof(uint32_t); i++) {
				((uint32_t*)pDma)[i] = ((uint32_t*)&dma)[i];
			}
			cmd->count = sizeof(IblSlvDmaTransfer)/sizeof(long);

			cmd->cmd = SLV_CMD_ACK;
			break;

                case SLV_CMD_HIST_ABORT:
                        verbmsg("Histo abort");
                        cmd->cmd = SLV_CMD_BUSY;
                        abortHistogramming();
                        cmd->cmd = SLV_CMD_IDLE;
                        break;

		case SLV_CMD_HIST_READ: // input data selects buffer (0,1)
			verbmsg("HistoRead get");
			count = cmd->count;
			histNum = cmd->data;
			addr = (uint32_t*)histogram_buffers[histNum] + cmd->addr;
			data = (uint32_t*)&cmd->data;
			for (i=0; i<count; i++) {
				data[i] = addr[i];
				//verbmsg("HistoRead 0x%x at 0x%x",addr[i],&addr[i]);
				//ignore this output as it gives too much output
			}
			cmd->cmd = SLV_CMD_ACK;
			break;

		case SLV_CMD_HIST_TEST:
			testHistogramming(cmd);
			break;

		case SLV_CMD_CLEAR_CB:
			verbmsg("clearing CB and resetting network");
                        handleNetworkError();
			cmd->cmd = SLV_CMD_IDLE;
                        break;

		case SLV_CMD_ID_SET:
			verbmsg("set id %d", cmd->data);
			slaveId = cmd->data;
			cmd->cmd = SLV_CMD_IDLE;
			break;

		case SLV_CMD_ID_GET:
			verbmsg("get id");
			cmd->data = slaveId;
			cmd->count = 1;
			cmd->cmd = SLV_CMD_ACK;
			break;

                case SLV_CMD_GET_HASHES:
                        verbmsg("get hashes");

                        IblSlvHashes hashes;
                        IblSlvHashes *pHashes = (IblSlvHashes*)&cmd->data;
                        /* Get hash values for firmware and software and fill in struct. */
                        fillHashes(&hashes);
			// copy values. NB: we cannot use memcpy as the DPR is not byte accessible
			for (i=0; i < sizeof(IblSlvHashes)/sizeof(uint32_t); i++) {
				((uint32_t*)pHashes)[i] = ((uint32_t*)&hashes)[i];
			}
			cmd->count = sizeof(IblSlvHashes)/sizeof(long);
                        cmd->cmd = SLV_CMD_ACK;
                        break;
		default:
			xil_printf("[%d:] Invalid command 0x%x\r\n",slaveId,cmd->cmd);
			cmd->cmd = SLV_CMD_IDLE;
			break;
	}
}


void fillHashes(IblSlvHashes *pHashes) {
    int i = 0;
    int j = 0;

    /* Write to register first to reset. */
    *epcIdReg = 0;

    for (i = 0; i < 5; i++) {
        /* firmware hash */
        uint32_t partialHash = 0;
        for (j = 0; j < 4; j++) {
            partialHash <<= 8;
            partialHash |= *epcIdReg;
        }
        pHashes->firmwareHash[i] = partialHash;

    }
    /* software hash */
    pHashes->softwareHash[0] = strtoul(TOSTRING(GIT_HASH_1), NULL, 16);
    pHashes->softwareHash[1] = strtoul(TOSTRING(GIT_HASH_2), NULL, 16);
    pHashes->softwareHash[2] = strtoul(TOSTRING(GIT_HASH_3), NULL, 16);
    pHashes->softwareHash[3] = strtoul(TOSTRING(GIT_HASH_4), NULL, 16);
    pHashes->softwareHash[4] = strtoul(TOSTRING(GIT_HASH_5), NULL, 16);
}

