/*
 * Filename:   histClient_main.c
 * Author:     Kugel
 *
 * Summary:
 *   This file contains the main loop of the histoClient program
 *   running on a MicroBlaze CPU implemented on the ROD slave FPGAs.
 *   It sets up the environment of the embedded system.
 *   The main loop processes commands received by the ROD master and
 *   takes care of networking connectivity.
 */

#include <stdio.h>
#include <string.h>

#include "xparameters.h"
#include "xintc_l.h"
#include "netif/xadapter.h"

#include "platform.h"
#include "platform_config.h"
void disable_caches(); // not declared via platform.h

#define MAIN
#include "histClient.h"
#include "rodSlave.hxx"
#include "rodHisto.hxx"
#include "network.h"
#include "histoTx.h"
#include "memoryManagement.h"
#include "lwip/init.h"

void printWelcomeMsg() {
    int i = 0;

    xil_printf("\r\n");
    xil_printf("\r\n");
    xil_printf("---- IBL ROD Slave Software ----\r\n");

    /* Print the slave firmware and software git hashes. */
    IblSlvHashes gitHashes;
    fillHashes(&gitHashes);
    xil_printf("FW hash: ");
    for (i = 0; i < 5; i++) {
        xil_printf("%x", gitHashes.firmwareHash[i]);
    }
    xil_printf("\r\n");

    xil_printf("SW hash: ");
    for (i = 0; i < 5; i++) {
        xil_printf("%x", gitHashes.softwareHash[i]);
    }
    xil_printf("\r\n");
    xil_printf("Current command register contains 0x%08x\r\n", cmd->cmd);

    xil_printf("--------\r\n");
}


int main() {

	usingMUX = 1;

	int loop = 0;
	int i = 0;
	const int max_connection_attempts = 10;
	// setup vectors and epc. do before init_platform
	XIntc_MasterDisable(XPAR_INTC_0_BASEADDR);
	disable_caches();
	setupAddresses();

	init_platform();
	// Added to enable the DATA read out coming from the BOC;
	// otherwise the ROD expects FE connected locally

	//*epcCtlReg |= (1 << CTL_BOC_ENABLE_BIT);
	
	// default verbose
	verbose = 0;
    
	printWelcomeMsg();

	// Initialize DMA-aligned circular buffers
	setupHistoBuffer(0);
	setupHistoBuffer(1);
    
	// Initialize network
	lwip_init();
	memset(&netConfig, 0, sizeof(IblSlvNetCfg));
	networkInitialize(&server_netif);
	initializeHistogramTransfer();

	// init hist state
	histState = SLV_STAT_OK; 	// idle
	memset(&histConfig, 0, sizeof(IblSlvHistCfg));

	if (!simulation) print("ROD Slave histogram client\r\n");

	/* receive and process packets */
	while (1)
	{
		cmdLoop();
        
		if (network.enabled) {
			xemacif_input(network.interface);
			if( networkIsConnected() ) transferHistogramData();
		}

		if (++i > 1000000)
		{
       			i = 0;
            
			if (!networkIsConnected()) {
				if (networkIsActive()
					&&  network.connection_attempts < max_connection_attempts)
				{
					networkTryConnect();
				}
				else {
					handleNetworkError();
				}
			}

			verbmsg("Slave %d command loop %d, A connected = %d, B connected = %d",
			slaveId, loop++, network.connections[0].connected, network.connections[1].connected);
		}

		checkTcpTimers();
	}

	/* never reached */
	cleanup_platform();

	return 0;
}
