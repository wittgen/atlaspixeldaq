#include "memoryManagement.h"
#include "histClient.h"
#include "xparameters.h"

volatile uint32_t* histogram_buffers[2];

#ifdef HISTO_TEST

// for BRAM-only version just use the DRAM without malloc'ing...
// align the DMA buffer later!

static volatile uint32_t* histogram_buffer_a =
    (volatile uint32_t*) XPAR_MCB_DDR2_S0_AXI_BASEADDR;

static volatile uint32_t* histogram_buffer_b =
    ((volatile uint32_t*) XPAR_MCB_DDR2_S0_AXI_BASEADDR) + 2*MAX_PIXELS + DMA_RAM_ALIGNMENT_BYTES/sizeof(uint32_t);

#else

// buffer size is 2 * MAX_PIXELS as LONG_TOT readout mode uses 2 32 bit words.
static volatile uint32_t histogram_buffer_a[RAM_BUFFER_SIZE_WORDS] __attribute__ ((section("__DMA")));
static volatile uint32_t histogram_buffer_b[RAM_BUFFER_SIZE_WORDS] __attribute__ ((section("__DMA")));

#endif // HISTO_TEST

#ifndef HISTO_TEST
#include "histoTx.h"

circular_buffer_t histogram_circular_buffers[2];

void generateFakedHistogramData()
{
    circular_buffer_t* circular_buffer;
    cb_item_t* item;
    volatile uint8_t* data;
    uint32_t unit, size, i;

    for (unit = 0; unit <= 1; ++unit) {
        size = getChipDataSize(i);
        circular_buffer = &histogram_circular_buffers[unit];
        item = cbGetWriteItem(circular_buffer, size);

        if (item == NULL) {
            verbmsg("ERROR: Histogram circular buffer of unit %d is full!",
                    unit);
            continue;
        }
        data = item->data;

        for (i = 0; i < size; ++i) {
            *(data++) = 48 + (i % 10);
        }

        cbPush(circular_buffer, item);

        verbmsg("Faked histogram data for unit %d.", unit);
    }
}

void initializeHistogramTransfer()
{
    histoTxSetup();
    histoTxInitialize(&histo_txs[0],
                      &network.connections[0],
                      &histogram_circular_buffers[0]);
    histoTxInitialize(&histo_txs[1],
                      &network.connections[1],
                      &histogram_circular_buffers[1]);
}

void transferHistogramData()
{
    histoTxTransmit(&histo_txs[0]);
    histoTxTransmit(&histo_txs[1]);
}
#endif // no HISTO_TEST

void setupHistoBuffer(const uint32_t histogram_unit)
{
    verbmsg("Initializing DMA circular buffer for histogrammer %d:",
            histogram_unit);
    
    volatile uint32_t* ram_buffer = histogram_unit == 0 ?
        histogram_buffer_a :
        histogram_buffer_b;
    histogram_buffers[histogram_unit] = ram_buffer;

#ifdef HISTO_TEST

    // Align memory
    histogram_buffers[histogram_unit] = (volatile uint32_t*)
        ((((uint32_t) ram_buffer) + DMA_RAM_ALIGNMENT_BYTES) & ~(DMA_RAM_ALIGNMENT_BYTES - 1));

#else
    
    // Alignment done in circular buffers
    circular_buffer_t* circular_buffer = &histogram_circular_buffers[histogram_unit];

    cbInitialize(circular_buffer,
                 ram_buffer,
                 RAM_BUFFER_SIZE_MB * 1024 * 1024,
                 DMA_RAM_ALIGNMENT_BYTES);

#endif // HISTO_TEST
}


uint32_t getChipDataSize(const uint32_t histogram_unit)
{
    IblSlvHistUnitCfg* config = &histConfig.cfg[histogram_unit];
    uint32_t dataSize = pixelsPerChip;

    // Adjusting transfer size depending on mode of operation.
    if (config->chipSel == 1) dataSize *= maxChips;
    switch (config->type) {
        case SHORT_TOT:        dataSize *= 4; break;
        case ONLINE_OCCUPANCY: dataSize *= 4; break;
        case LONG_TOT:         dataSize *= 8; break;
        default: break;
    }

    switch (config->maskStep) {
        case ROWMASK2: dataSize /= 2; break;
        case ROWMASK4: dataSize /= 4; break;
        case ROWMASK8: dataSize /= 8; break;
        case ROWMASK16: dataSize /= 16; break; //  Cannot be used until implemented in firmware
        case ROWMASK32: dataSize /= 32; break; //  Only for FE-I3 use
        case ROWMASK64: dataSize /= 32; break; //  Only for FE-I3 use (64 mask steps handled in 2 seperate 32 mask steps)
        default: break;
    }

    return dataSize;
}

