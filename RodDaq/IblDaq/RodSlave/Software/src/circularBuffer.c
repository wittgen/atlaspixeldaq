/*
 * Filename:    circularBuffer.c
 * Date:        February 2014
 * Author:      Lohse (h.lohse@stud.uni-heidelberg.de)
 *
 * Summary see header
 */

#include "circularBuffer.h"
#include "histClient.h"
#include <stdlib.h>

static const uint32_t header_size = sizeof(cb_item_header_t)
                                  + sizeof(cb_item_t);

static uint32_t aligned(const circular_buffer_t* cb, const uint32_t bytes)
{
    const uint32_t offset = bytes % cb->alignment_bytes;
    if (offset == 0) {
        return bytes;
    }
    else {
        const uint32_t missing = cb->alignment_bytes - offset;
        return bytes + missing;
    }
}

// Ensure first byte is start of alignment block
static void* bufferStartAligned(const circular_buffer_t* cb)
{
    return (void*) aligned(cb, (uint32_t) cb->buffer_start);
}

// Ensure last byte is end of buffer's last alignment block
static void* bufferEndAligned(const circular_buffer_t* cb,
                       const uint32_t buffer_size_bytes)
{
    const uint32_t start_diff = (uint32_t) cb->buffer_start_aligned
                              - (uint32_t) cb->buffer_start;
    uint32_t size = buffer_size_bytes - start_diff;
    size = (size / cb->alignment_bytes) * cb->alignment_bytes;

    return (void*) (((uint8_t*) cb->buffer_start_aligned) + size);
}

void cbInitialize(
    circular_buffer_t* cb,
    volatile void* buffer,
    const uint32_t buffer_size_bytes,
    const uint32_t alignment_bytes)
{
    cb->buffer_start = buffer;
    cb->buffer_end = ((uint8_t*) cb->buffer_start) + buffer_size_bytes;
    cb->alignment_bytes = alignment_bytes;

    cb->buffer_start_aligned = bufferStartAligned(cb);
    cb->buffer_end_aligned = bufferEndAligned(cb, buffer_size_bytes);

    cb->head = NULL;
    cb->tail = NULL;
    cb->unused_memory = cb->buffer_start_aligned;

    verbmsg("Circular buffer 0x%x:", cb);
    verbmsg("  Located at 0x%x and %ld bytes long.",
            cb->buffer_start,
            buffer_size_bytes);
    verbmsg("  Aligned with %ld bytes to 0x%x.",
            cb->alignment_bytes,
            cb->buffer_start_aligned);
}

cb_item_t* cbGetReadItem(const circular_buffer_t* cb)
{
    return cbIsEmpty(cb) ? NULL : cb->head->item;
}

void cbPop(circular_buffer_t* cb)
{
    if (cbIsEmpty(cb)) {
        return;
    }

    cb->head = cb->head->next;

    if (cbIsEmpty(cb)) {
        cbClear(cb);
    }
}

cb_item_t* cbGetReadItemBehind(const circular_buffer_t* cb,
                               const cb_item_t* item)
{
    const cb_item_header_t* header = (cb_item_header_t*)
        (((uint8_t*) item) + sizeof(*item));
    
    if (cbIsEmpty(cb) || header->next == NULL) {
        return NULL;
    }
    else {
        return header->next->item;
    }
}

static int fitsEmpty(const circular_buffer_t* cb, const uint32_t item_size)
{
    const uint32_t buffer_size = (uint32_t) cb->buffer_end_aligned
                               - (uint32_t) cb->buffer_start_aligned;
    return item_size < (buffer_size - header_size);
}

static int fitsBack(const circular_buffer_t* cb, const uint32_t item_size)
{
    const uint32_t space_back = (uint32_t) cb->buffer_end_aligned
                              - (uint32_t) cb->unused_memory;
    return item_size < (space_back - header_size);
}

static int fitsFront(const circular_buffer_t* cb, const uint32_t item_size)
{
    const uint32_t space_front = (uint32_t) cb->head->item->data
                               - (uint32_t) cb->buffer_start_aligned;
    return item_size < (space_front - header_size);
}

static int fitsSpace(const circular_buffer_t* cb, const uint32_t item_size)
{
    const uint32_t space = (uint32_t) cb->head->item->data
                         - (uint32_t) cb->unused_memory;
    return item_size < (space - header_size);
}

static int isTailAtBack(const circular_buffer_t* cb)
{
    return (uint32_t) cb->unused_memory > (uint32_t) cb->head;
}

static int isHeadAtBack(const circular_buffer_t* cb)
{
    return (uint32_t) cb->head > (uint32_t) cb->unused_memory;
}

static cb_item_t* createItem(volatile void* data, const uint32_t size)
{
    // Put item information behind data space
    cb_item_t* item = (cb_item_t*) (((uint8_t*) data) + size);
    item->data = data;
    item->size = size;
    return item;
}

// Search for sufficient memory space in buffer for item_size bytes,
// keep circular arrangement.
cb_item_t* cbGetWriteItem(const circular_buffer_t* cb,
                          const uint32_t item_size)
{
    // Check if empty or has enough space if wrapped around buffer borders
    if ((cbIsEmpty(cb) && fitsEmpty(cb, item_size))
    ||  (isHeadAtBack(cb) && fitsSpace(cb, item_size)))
    {
        return createItem(cb->unused_memory, item_size);
    }
    // Check if enough space at back/front is available
    else if (isTailAtBack(cb)) {
        if (fitsBack(cb, item_size)) {
            return createItem(cb->unused_memory, item_size);
        }
        // Front may have enough space if items have been popped.
        else if (fitsFront(cb, item_size)) {
            return createItem(cb->buffer_start_aligned, item_size);
        }
    }
    
    return NULL;
}

static void appendItem(circular_buffer_t* cb, cb_item_header_t* item_header)
{
    if (cbIsEmpty(cb)) {
        cb->head = item_header;
        cb->tail = cb->head;
    }
    else {
        cb->tail->next = item_header;
        cb->tail = cb->tail->next;
    }
}

static void advanceUnusedMemory(circular_buffer_t* cb)
{
    cb->unused_memory = ((uint8_t*) cb->tail) + sizeof(cb_item_header_t);
    cb->unused_memory = (volatile void*)
        aligned(cb, (uint32_t) cb->unused_memory);
    
    const uint32_t remaining = (uint32_t) cb->buffer_end_aligned
                             - (uint32_t) cb->unused_memory;

    if (remaining <= header_size) {
        cb->unused_memory = cb->buffer_start_aligned;
    }
}

void cbPush(circular_buffer_t* cb, cb_item_t* item)
{
    // Put item header after item information
    cb_item_header_t* item_header = (cb_item_header_t*)
        (((uint8_t*) item) + sizeof(*item));
    item_header->item = item;
    item_header->next = NULL;
    
    appendItem(cb, item_header);
    advanceUnusedMemory(cb);
}

void cbClear(circular_buffer_t* cb)
{
    cb->head = NULL;
    cb->tail = NULL;
    cb->unused_memory = cb->buffer_start_aligned;
}

int cbIsEmpty(const circular_buffer_t* cb)
{
    return cb->head == NULL;
}

