/////////////////////////////////////////////////////////////////////////////////
//
// Copyright (c) 2009 Xilinx, Inc. All Rights Reserved.
//
// You may copy and modify these files for your own internal use solely with
// Xilinx programmable logic devices and  Xilinx EDK system or create IP
// modules solely for Xilinx programmable logic devices and Xilinx EDK system.
// No rights are granted to distribute any files unless they are distributed in
// Xilinx programmable logic devices.
//
/////////////////////////////////////////////////////////////////////////////////

/*
 *      Simple SREC Bootloader
 *      This simple bootloader is provided with Xilinx EDK for you to easily re-use in your
 *      own software project. It is capable of booting an SREC format image file 
 *      (Mototorola S-record format), given the location of the image in memory.
 *      In particular, this bootloader is designed for images stored in non-volatile flash
 *      memory that is addressable from the processor. 
 *
 *      Please modify the define "FLASH_IMAGE_BASEADDR" in the blconfig.h header file 
 *      to point to the memory location from which the bootloader has to pick up the 
 *      flash image from.
 *
 *      You can include these sources in your software application project in XPS and 
 *      build the project for the processor for which you want the bootload to happen.
 *      You can also subsequently modify these sources to adapt the bootloader for any
 *      specific scenario that you might require it for.
 *
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "portab.h"
#include "errors.h"
#include "xil_cache.h"
#include "xparameters.h"

#define MAIN // only here
#include "rodSlave.hxx"

#include "iblSlaveCmds.h"

/* bootable program to be prepared like this:
mb-objdump -S bootTest.elf >bootTest.lst
mb-objcopy  -O binary -R .vectors.reset -R .vectors.sw_exception -R .vectors.interrupt -R .vectors.hw_exception bootTest.elf bootTest.bin
bin2c -c -n slvProg -t int bootTest.bin > bootTest.hxx
cp bootTest.hxx ../../../../rodMasterPpc/cpu/rodMaster
*/


/* Declarations */
extern void init_stdout();

#ifdef __cplusplus
extern "C" {
#endif

extern void outbyte(char c); 

#ifdef __cplusplus
}
#endif


#ifdef VERBOSE
static int8_t *errors[] = { 
    "",
    "Error while copying executable image into RAM",
    "Error while reading an SREC line from flash",
    "SREC line is corrupted",
    "SREC has invalid checksum."
};
#endif


void crc32(char *p, int nr, uint32_t *cval);

static int incarnation = 1;

volatile unsigned long *epcCtlReg  =  (volatile unsigned long *)MB_CTL_REG;
volatile unsigned long *epcStatReg = (volatile unsigned long *)MB_STAT_REG;
volatile unsigned long * busRam = (volatile unsigned long *)MB_EPC_RAM_BASE;

int simulation;
int slaveId;

// vectors
extern void *_start1;				// offset 0x00
#define START_VECTOR 0
extern void *_exception_handler;	// offset 0x08
#define EXCEPT_VECTOR 8
//extern unsigned long _interrupt_handler;	// offset 0x10
extern void *_interrupt_handler;	// offset 0x10
#define IRQ_VECTOR 0x10
extern void *_hw_exception_handler; // offset 0x20
#define HW_EXCEPT_VECTOR 0x20

int main()
{
    int i;
    int count;
    int verbose = 0;
    verbose = 1;
    void (*startAddr)();
    volatile IblSlvRdWr *cmd;
    iblSlvStat *stat;
    uint32_t statType;
    unsigned long *addr, *data;
    uint32_t crcVal;
    uint32_t cmd_loop_cnt = 0;

    // disable chaches
    Xil_DCacheDisable();
    Xil_ICacheDisable();
	//setup vectors, except reset
    ((unsigned long*)IRQ_VECTOR)[0] = 0xb0000000 + (((unsigned long)&_interrupt_handler) >> 16);
    ((unsigned long*)IRQ_VECTOR)[1] = 0xb8080000 + (((unsigned long)&_interrupt_handler) & 0xffff);
    ((unsigned long*)EXCEPT_VECTOR)[0] = 0xb0000000 + (((unsigned long)&_exception_handler) >> 16);
    ((unsigned long*)EXCEPT_VECTOR)[1] = 0xb8080000 + (((unsigned long)&_exception_handler) & 0xffff);
    ((unsigned long*)HW_EXCEPT_VECTOR)[0] = 0xb0000000 + (((unsigned long)&_hw_exception_handler) >> 16);
    ((unsigned long*)HW_EXCEPT_VECTOR)[1] = 0xb8080000 + (((unsigned long)&_hw_exception_handler) & 0xffff);

    init_stdout();

    volatile unsigned long *epcIdReg  =  (volatile unsigned long *)MB_DESIGN_REG;
    epcCtlReg  =  (volatile unsigned long *)MB_CTL_REG;
    epcStatReg = (volatile unsigned long *)MB_STAT_REG;
    busRam = (volatile unsigned long *)MB_EPC_RAM_BASE;
    // get address of command and status
    cmd = (IblSlvRdWr*)&busRam[SLV_CMD_LOCATION];
    stat = (iblSlvStat*)&cmd->data;

    // get simulation status
    simulation = (0 != (*epcStatReg & (1 << MB_STAT_SIMULATION_BIT)));
    slaveId = -1;

    // on first run clear command memory
    if (1 == incarnation) {
    	cmd->cmd = SLV_CMD_IDLE;
    }

    xil_printf("\r\n Slave Loader: Binary bootloader, incarnation: %d\r\n",incarnation++);

    xil_printf("\r\n Slave Loader: Design ID: 0x%x\r\n",*epcIdReg);

    // start monitoring ram
    xil_printf("\r\n Slave Laoder: Entering boot loop\r\n");

    while (1){
	if (verbose) if( ++cmd_loop_cnt % 1000000 == 0 ) xil_printf("[%d:] Command loop: %d -- 0x%x \r\n", slaveId, cmd_loop_cnt/1000000, cmd->cmd);
        // command loop
        switch (cmd->cmd){
        case SLV_CMD_IDLE:
        case SLV_CMD_BUSY:
        case SLV_CMD_ACK:
        case SLV_CMD_ALIVE:
            continue;

        case SLV_CMD_BOOT:
		   // set active status
           if (verbose) xil_printf("[%d:] Slave Loader: Booting: indicate run mode\r\n",slaveId);
 		   cmd->cmd = SLV_CMD_ALIVE;
 		   break;
        case SLV_CMD_VERBOSE:
            verbose  = cmd->data?1:0;
            if (verbose) xil_printf("[%d:] Slave Loader: Verbose set to %d\r\n",slaveId, verbose);
            cmd->cmd = SLV_CMD_IDLE;
            break;
        case SLV_CMD_CRC:
            if (verbose) xil_printf("[%d:] Comupting crc\r\n",slaveId);
            addr = (unsigned long*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
        	crc32((char *)addr, cmd->data*sizeof(uint32_t), &crcVal);
            if (verbose) xil_printf("[%d:] Crc from address 0x%x, 0x%x words: 0x%x\r\n",slaveId, addr, cmd->data, crcVal);
            data = (unsigned long*)&cmd->data;
            data[0] = ENDIAN_FLAG;
            data[1] = crcVal;
            cmd->count = 2;
            cmd->cmd = SLV_CMD_ACK;
        	break;
        case SLV_CMD_STAT:
            if (verbose) xil_printf("[%d:] Slave Loader: Status request\r\n",slaveId);
            statType =  cmd->data; // get status type
            stat = (iblSlvStat*)&cmd->data;
            stat->progType = SLV_TYPE_LOADER;
            stat->progVer = 1;
            switch (statType){
            case SLV_STAT_TYPE_STD:
                stat->status = SLV_STAT_OK;
            	break;
            default:
                stat->status = SLV_STAT_ERROR;
            	break;
            }
            cmd->count = sizeof(iblSlvStat)/sizeof(long);
            cmd->cmd = SLV_CMD_ACK;
            break;
        case SLV_CMD_WRITE:
            addr = (unsigned long*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
            count = cmd->count;
            data = (unsigned long*)&cmd->data;
	    if (verbose) xil_printf("[%d:] Writing %d words starting at 0x%x\r\n", slaveId, cmd->count, addr);
            for (i=0;i<count;i++){
                addr[i] = data[i];
                //if (verbose) xil_printf("[%d:] Write 0x%x at 0x%x\r\n",slaveId,  addr[i],&addr[i]);
            }
            cmd->cmd = SLV_CMD_IDLE;
            break;
        case SLV_CMD_READ:
            addr = (unsigned long*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
            count = cmd->count;
            data = (unsigned long*)&cmd->data;
            for (i=0;i<count;i++){
                data[i] = addr[i];
                if (verbose) xil_printf("[%d:] Read 0x%x at 0x%x\r\n",slaveId, addr[i],&addr[i]);
            }
            cmd->cmd = SLV_CMD_ACK;
            break;
        case SLV_CMD_START:
            xil_printf("[%d:] Starting from address 0x%x", slaveId, cmd->addr);
            addr = (unsigned long*)XPAR_MCB_DDR2_S0_AXI_BASEADDR + cmd->addr/sizeof(long);
            startAddr = (void (*)())addr;
	    if (verbose) for(i=0;i<4;i++) xil_printf("[%d:] Header content %d 0x%x\r\n", slaveId, i, ((uint32_t*)cmd)[i]);
            // if (verbose) xil_printf("Branching to address 0x%x\r\n",startAddr);
            xil_printf("[%d:] Slave Loader: Branching to address 0x%x\r\n",slaveId, startAddr);
            for (i = 0; i < 100000;i++);
            cmd->cmd = SLV_CMD_IDLE;
            (*startAddr)(); // jump
            break;
        case SLV_CMD_ID_SET:
	    xil_printf("Slave Loader: detected set id %d\n", cmd->data);
        	slaveId = cmd->data;
            cmd->cmd = SLV_CMD_IDLE;
            break;

        case SLV_CMD_ID_GET:
        	cmd->data = slaveId;
        	cmd->count = 1;
            cmd->cmd = SLV_CMD_ACK;
            break;

        default:
        	xil_printf("[%d:] Slave Loader: invalid command 0x%x\r\n",slaveId, cmd->cmd);
            cmd->cmd = SLV_CMD_IDLE;
            break;
        }
    }
    return 0;
}

