#ifndef HISTO_TEST_DATA_H
#define HISTO_TEST_DATA_H

#define TEST_DATA_OFFSET 4  // >= 2

struct test_data_t;
struct test_args_t;

typedef int (*is_hit_pixel_f)(unsigned long masked_row, unsigned long column);
typedef void (*next_test_data_f)(struct test_data_t* data);

typedef struct test_data_t {
    unsigned long row;
    unsigned long column;
    unsigned long tot;
    
    unsigned long row_mask;
    unsigned long row_offset;
    unsigned long row_filter;

    next_test_data_f generator;
    is_hit_pixel_f   is_hit;
    char* pattern;
} test_data_t;

typedef void (*init_test_data_f)(test_data_t* data, struct test_args_t* args);

void initTestDataComplete(test_data_t* data, struct test_args_t* args);
void initTestDataOffset(test_data_t* data, struct test_args_t* args);
void initTestDataLinear(test_data_t* data, struct test_args_t* args);
void initTestDataFilteredOnly(test_data_t* data, struct test_args_t* args);
void initTestDataNoUnfiltered(test_data_t* data, struct test_args_t* args);

void nextTestDataNoUnfiltered(struct test_data_t* data);

void nextTestData(test_data_t* data);
void resetTestData(test_data_t* data);

int isHitPixel(test_data_t* data, unsigned long masked_row, unsigned long column);

void getTotSums(test_data_t* data,
                unsigned long* sum_tot,
                unsigned long* sum_tot_sqr,
                unsigned long masked_row,
                unsigned long column);

#endif // HISTO_TEST_DATA_H

