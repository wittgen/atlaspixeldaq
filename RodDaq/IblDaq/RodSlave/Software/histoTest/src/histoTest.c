/*
 * Filename:   histoTest.c
 * Author:     Kugel, Kretz
 * Modified:   Dreyer, Nick
 *
 * Summary:
 * Standalone test program without network support to test the histogrammer.
 * Modification by Nick Dreyer to allow for L1/L2 Upgrade
 *
 *
 *
 *
 */

#define N_CHIPS 128

#define USING_MUX 0

#include "histoTest.h"
#include <stdio.h>
#include <string.h>
#define MAIN
#include "histClient.h"
#include "rodHisto.hxx"
#include "xuartlite_l.h"

volatile unsigned long* histo_0_test_reg = (volatile unsigned long*) MB_HIST0_TEST_REG;
volatile unsigned long* histo_1_test_reg = (volatile unsigned long*) MB_HIST1_TEST_REG;

static const uint32_t modi[] = {
//     OFFLINE_OCCUPANCY,
    ONLINE_OCCUPANCY,
//     SHORT_TOT, //  DO NOT use for FE-I3
    LONG_TOT
};

static const uint32_t chips[] = {
//     0,
    1
};

static const uint32_t row_mask_modes[] = {
//     ROWMASKNONE,
//     ROWMASK2,
//     ROWMASK4,
//     ROWMASK8,
//     ROWMASK16 //  DO NOT USE until implemented in firmware
    ROWMASK32 //  ONLY one to use for FE-I3 - not even  ROWMASKNONE works yet
};

static const init_test_data_f unfiltered_pattern[] = {
    initTestDataComplete,
    initTestDataOffset,
    initTestDataLinear
};

static const init_test_data_f filtered_pattern[] = {
//     initTestDataFilteredOnly, // Is this any different than initTestDataComplete unfiltered_pattern ?
//     initTestDataNoUnfiltered
};

//  These must correspond to the functions above for possible eventual replacement of their convoluted use as flags
static char* unfiltered_pattern_s[] = {
    "Complete     ",
    "Offset       ",
    "Linear       "
};

static char* filtered_pattern_s[] = {
//     "Only Filtered", // Is this any different than "Complete" ?
//     "No Unfiltered"
};

static const uint32_t occ_values[] = {
//     1,
//     2,
//     4,
//     15,
//     16,
    255
};

static const int num_modi = sizeof(modi)/sizeof(modi[0]);
static const int num_chips = sizeof(chips)/sizeof(chips[0]);
static const int num_row_mask_modes = sizeof(row_mask_modes)/sizeof(row_mask_modes[0]);
static const int num_unfiltered_pattern = sizeof(unfiltered_pattern)/sizeof(unfiltered_pattern[0]);
static const int num_filtered_pattern = sizeof(filtered_pattern)/sizeof(filtered_pattern[0]);
static const int num_occ_values = sizeof(occ_values)/sizeof(occ_values[0]);

static void adjustExpectedOccToFilter(test_args_t* args)
{
    if (args->test_data_pattern == initTestDataFilteredOnly) {
        //args->occupancy = 1;
        //histConfig.cfg[0].expectedOccValue = 1;
        //histConfig.cfg[1].expectedOccValue = 1;
    }
    else if (args->test_data_pattern == initTestDataNoUnfiltered) {
        args->occupancy = 0;
        histConfig.cfg[0].expectedOccValue = 0;
        histConfig.cfg[1].expectedOccValue = 0;
    }
}

int test(test_args_t* args)
{
    test_data_t test_data[2];
    int is_correct;
   
    useConfiguration(0, args);
    useConfiguration(1, args);

    adjustExpectedOccToFilter(args);

    args->test_data_pattern(&test_data[0], args);
    args->test_data_pattern(&test_data[1], args);
    
    setUpHistogramming();

    writeTestData(0, args, &test_data[0]);
    writeTestData(1, args, &test_data[1]);
   
    tearDownHistogramming();

    is_correct  = isCorrect(0, &test_data[0]);
    is_correct &= isCorrect(1, &test_data[1]);

    if (is_correct) {
//         showFailedResult("    PASSED ", args);
        return 1;
    }
    else {
        showFailedResult("    FAILED ", args);
        return 0;
    }
}

void useConfiguration(const int unit, test_args_t* args)
{
    IblSlvHistUnitCfg* config = &histConfig.cfg[unit];
    
    config->nChips           = N_CHIPS;
    config->enable           = 1;
    config->type             = args->mode;
    config->maskStep         = args->row_mask_mode;
    config->mStepEven        = args->row_filter;
    config->mStepOdd         = args->row_filter;
    config->chipSel          = args->use_all_chips;
    config->expectedOccValue = args->occupancy;
    config->addrRange        = 0;
    config->scanId           = 0xDEADBEEF;
    config->binId            = 1;
}

void writeTestData(const int unit, test_args_t* args, test_data_t* data)
{
    // const unsigned long conn_chip = args->use_all_chips ? (nChips - 1 - ((isFei3 == 0) ? 0 : (unit * chipsPerMod))) : 0;
    const unsigned long conn_chip = args->use_all_chips ? (nChips - 1) : 0;
    volatile unsigned long* histo_test_reg = unit == 0 ?
        histo_0_test_reg :
        histo_1_test_reg;
    unsigned long value;
    int i, c;

    int rl = row_size + 1;
    int cl = col_size + 1;
    if(isFei3) {
      rl -= 1;
      cl -= 1;
    }
    for (i = 0; i < args->occupancy; ++i) {
        for (c = 0; c <= conn_chip; ++c) {
            resetTestData(data);
        
            while (data->row < rl && data->column < cl) {
                value = 0;
                value |= (data->row << ROW_SHIFT);
                value |= (data->column << COL_SHIFT);
                value |= (data->tot << tot_shift);
                value |= (c << CHIP_SHIFT);

// if (data->column == 1 && data->row < 10 && unit == 0) verbmsg("%d %d %d %d %d 0x%8.8x", unit, data->tot, c, data->column, data->row, value);
// if (data->column == 1 && data->row < 10) verbmsg("%d %d %d %d %d 0x%8.8x", unit, data->tot, c, data->column, data->row, value);
// if (data->row < 1) verbmsg(""); //  Why is this dummy output statement needed to properly start the program ???
// verbmsg("%d %d %d %d %d 0x%8.8x", unit, data->tot, c, data->column, data->row, value);

		if(usingMUX) { //  Call function here to handle sending data to slave formatter


                } else {
                  *histo_test_reg = value;
		}
                nextTestData(data);
            }
        }
    }
}

void showFailedResult(char* message, test_args_t* args)
{
    xil_printf("\r\n%s| %s | %s |            %2d |   %3d | %s | %3d |%s | %s | ",
        message, hms[args->mode], rms[args->row_mask_mode],
        (args->row_filter == 0 ? args->row_offset : args->row_filter),
        (args->use_all_chips ? nChips : 1),
        args->pattern, args->occupancy,"     ","       ");
}

void setUpHistogramming()
{
    verbose = 0;
    startHistogramming();
    
    // histogrammers now receives data from test input
    BIT_CLEAR(*epcCtlReg, MB_CTL_HIST_MUX0_BIT); 
    BIT_CLEAR(*epcCtlReg, MB_CTL_HIST_MUX1_BIT); 
    
    verbose = 1;
}

void tearDownHistogramming()
{
    verbose = 0;
    stopHistogramming();
    verbose = 1;
}

// copy inbyte and outbyte from the bsp "standalone" tree to be able to switch between uart and mdm stdio
// the selection is done via the base address, either
// XPAR_UARTLITE_0_BASEADDR (mdm)
// or
// XPAR_UARTLITE_1_BASEADDR (rs232)
static int mdmUart = 0;

void outbyte(char c) {
	 XUartLite_SendByte(
        mdmUart ? XPAR_UARTLITE_0_BASEADDR : XPAR_UARTLITE_1_BASEADDR,
        c);
}

char inbyte(void) {
	 return XUartLite_RecvByte(
        mdmUart ? XPAR_UARTLITE_0_BASEADDR : XPAR_UARTLITE_1_BASEADDR);
}

void showHeader()
{
    int i;
    *epcIdReg = 0;
    
    xil_printf("\n\r\n\r");
    verbmsg("ROD Slave histogram test running on firmware with git hash");
    
    xil_printf("***** ");
    for (i = 0; i < 20; ++i) {
        xil_printf("%02x", *epcIdReg);
    }
    xil_printf("\r\n\r\n");
}

int main()
{
    test_args_t test_args;
    int m, s, c, o, f, r, p;
    int tests = 0, passed_tests = 0;

	// setup vectors and epc. do before init_platform
	XIntc_MasterDisable(XPAR_INTC_0_BASEADDR);
	disable_caches();
	setupAddresses();
	setupGeometry();
	init_platform();
	setupHistoBuffer(0);
	setupHistoBuffer(1);
	verbose = 1;

	usingMUX = USING_MUX;

	hms[0] = "ONLINE_OCCUPANCY ";
	hms[1] = "OFFLINE_OCCUPANCY";
	hms[2] = "SHORT_TOT        ";
	hms[3] = "LONG_TOT         ";

	rms[0] = "ROWMASKNONE";
	rms[1] = "ROWMASK2   ";
	rms[2] = "ROWMASK4   ";
	rms[3] = "ROWMASK8   ";
	rms[4] = "ROWMASK16  ";
	rms[5] = "ROWMASK32  ";

	// init hist state
	histState = SLV_STAT_OK; 	// idle
	memset(&histConfig, 0, sizeof(IblSlvHistCfg));

	if (!simulation) {
        showHeader();    
    }

    xil_printf("   Result  | Mode              | Mask Step   | Offset/Filter | Chips | Pattern       |Occ  |  ToT |   ToT²  |\r\n===\r\n");
  
    for (m = 0; m < num_modi; ++m) {
        test_args.mode = modi[m];

        for (s = 0; s < num_row_mask_modes; ++s) {
            test_args.row_mask_mode = row_mask_modes[s];
    
            for (c = 0; c < num_chips; ++c) {
                test_args.use_all_chips = chips[c];
                        
                for (o = 0; o < num_occ_values; ++o) {
                    test_args.occupancy  = occ_values[o];
                    
                    test_args.row_filter = 0;
                    for (r = 0; r < (1 << test_args.row_mask_mode); ++r) {
                        test_args.row_offset = r;

                        for (p = 0; p < num_unfiltered_pattern; ++p) {
                            test_args.test_data_pattern = unfiltered_pattern[p];
                            test_args.pattern=unfiltered_pattern_s[p];
                            
                            passed_tests += test(&test_args);
                            tests++;

                            if (tests % 10 == 0) {
                                xil_printf("Finished %d tests.\r\n", tests);
                            }
                        }
                    }

                    if (test_args.row_mask_mode == ROWMASKNONE) {
                        continue;
                    }

                    test_args.row_offset = 0;
                    for (f = 1; f <= (1 << test_args.row_mask_mode); ++f) {
                        test_args.row_filter = f;

                        for (p = 0; p < num_filtered_pattern; ++p) {
                            test_args.test_data_pattern = filtered_pattern[p];
                            test_args.pattern=filtered_pattern_s[p];
                            
                            passed_tests += test(&test_args);
                            tests++;

                            if (tests % 10 == 0) {
                                xil_printf("Finished %d tests.\r\n", tests);
                            }
                        }
                    }
                }
            }
        }
    }

    xil_printf("\r\n===\r\n");
    if (passed_tests == tests) {
        verbmsg("\tAll %d tests PASSED!", tests);
    }
    else {
        verbmsg("\tFAILED %d of %d tests! See errors above.",
            (tests - passed_tests), tests);
    }

    cleanup_platform();
    while (1);
    return 0;
}
