#ifndef HISTO_TEST_VERIFICATION_H
#define HISTO_TEST_VERIFICATION_H

#include "histoTestData.h"
#include "iblSlaveCmds.h"

#define CANCEL_TEST_ON_FIRST_ERROR 1

typedef struct {
    uint32_t unit;
    IblSlvHistUnitCfg* config;
    test_data_t* data;
    volatile unsigned long* value;
    int masked_row;
    int column;
} is_value_correct_args_t;

int isCorrect(const uint32_t unit, test_data_t* data);
void showLastGoodResult(char* message, is_value_correct_args_t* args);

#endif // HISTO_TEST_VERIFICATION_H

