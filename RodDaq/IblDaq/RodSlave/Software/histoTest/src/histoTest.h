#ifndef HISTO_TEST_H
#define HISTO_TEST_H

#include "histoTestData.h"
#include "histoTestVerification.h"
#include "memoryManagement.h"
#include "xintc_l.h"
#include "rodSlave.hxx"

// not declared via platform.h
extern void disable_caches(); 
extern void init_platform();
extern void cleanup_platform();

extern volatile unsigned long* histo_0_test_reg;
extern volatile unsigned long* histo_1_test_reg;

typedef struct test_args_t {
    init_test_data_f test_data_pattern;
    char* pattern;
    uint32_t mode;
    uint32_t use_all_chips;
    uint32_t row_mask_mode;
    uint32_t row_offset;
    uint32_t row_filter;
    uint32_t occupancy;
} test_args_t;

int test(test_args_t* args);

void writeTestData(const int unit, test_args_t*, test_data_t* data);
void useConfiguration(const int unit, test_args_t* args);
void showFailedResult(char* message, test_args_t* args);

void setUpHistogramming();
void tearDownHistogramming();

void showHeader();
int main();

#endif // HISTO_TEST_H

