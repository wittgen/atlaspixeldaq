#include "histoTestData.h"
#include "histoTest.h"
#include "rodHisto.hxx"
#include "histClient.h"
#include <stdlib.h>

static unsigned long getTot(unsigned long row, unsigned long row_mask)
{
    unsigned long rv = (row - 1) % row_mask + 1;
    if(isFei3) rv = row % row_mask + 1;
    return rv;
}

static void incrementColumn(struct test_data_t* data, unsigned long offset)
{
    data->column += offset;
    
    if(!isFei3) {
      if (data->column > col_size) {
          data->column = 1;
          data->row += data->row_mask;
      }
    } else {
      if (data->column > col_size - 1) {
          data->column = 0;
          data->row += data->row_mask;
      }
    }
}

static void nextTestDataColumn(struct test_data_t* data)
{
    incrementColumn(data, 1);
}

static void nextTestDataOffset(struct test_data_t* data)
{
    incrementColumn(data, TEST_DATA_OFFSET);
}

static void nextTestDataLinear(struct test_data_t* data)
{
    data->row    += data->row_mask;
    data->column += 1;
}

void nextTestDataNoUnfiltered(struct test_data_t* data)
{
    data->column++;

    if(!isFei3) {
      if (data->column > col_size) {
          data->column = 1;
          data->row++;

          if (((data->row - 1) % data->row_mask) == (data->row_filter - 1)) {
              data->row++;
          }
      }
    } else {
      if (data->column > col_size - 1) {
          data->column = 0;
          data->row++;

          if ((data->row % data->row_mask) == (data->row_filter - 1)) {
              data->row++;
          }
      }
    }
}

static int isHitPixelAlways(unsigned long masked_row, unsigned long column)
{
    return 1;
}

static int isHitPixelOffset(unsigned long masked_row, unsigned long column)
{
    return column % TEST_DATA_OFFSET == 1;
}

static int isHitPixelLinear(unsigned long masked_row, unsigned long column)
{
    return masked_row == column;
}

void resetTestData(test_data_t* data)
{
    if (data->row_filter > 0) {
        if (data->generator == nextTestDataNoUnfiltered) {
            data->row = data->row_filter == 1 ? 2 : 1;
        }
        else {
            data->row = data->row_filter;
        }
    }
    else {
        data->row = data->row_offset + 1;
        if(isFei3)data->row = data->row_offset;
    }
    
    data->column = 1;
    if(isFei3)data->column = 0;
    data->tot = getTot(data->row, data->row_mask);
}

static void initTestData(test_data_t* data,
                         next_test_data_f generator,
                         is_hit_pixel_f is_hit,
                         struct test_args_t* args)
{
    data->generator  = generator;
    data->is_hit     = is_hit;
    data->pattern    = args->pattern;
    data->row_mask   = (1 << args->row_mask_mode);
    data->row_offset = args->row_offset;
    data->row_filter = args->row_filter;

    resetTestData(data);
}

void initTestDataComplete(test_data_t* data, struct test_args_t* args)
{
    initTestData(data, nextTestDataColumn, isHitPixelAlways, args);
}

void initTestDataOffset(test_data_t* data, struct test_args_t* args)
{
    initTestData(data, nextTestDataOffset, isHitPixelOffset, args);
}

void initTestDataLinear(test_data_t* data, struct test_args_t* args)
{
    initTestData(data, nextTestDataLinear, isHitPixelLinear, args);
}

void initTestDataFilteredOnly(test_data_t* data, struct test_args_t* args)
{
    initTestData(data, nextTestDataColumn, isHitPixelAlways, args);
}

void initTestDataNoUnfiltered(test_data_t* data, struct test_args_t* args)
{
    initTestData(data, nextTestDataNoUnfiltered, isHitPixelAlways, args);
}

void nextTestData(test_data_t* data)
{
    data->generator(data);
    data->tot = getTot(data->row, data->row_mask);
}

int isHitPixel(test_data_t* data, unsigned long masked_row, unsigned long column)
{
    return data->is_hit(masked_row, column);
}

void getTotSums(test_data_t* data,
                unsigned long* sum_tot,
                unsigned long* sum_tot_sqr,
                unsigned long masked_row,
                unsigned long column)
{
    unsigned long offset = data->row_filter > 0 ?
                           (data->row_filter - 1) :
                           data->row_offset;
    unsigned long row = (masked_row - 1) * data->row_mask + offset + 1;
    if(isFei3) row = masked_row * data->row_mask + offset;
    unsigned long tot = getTot(row, data->row_mask);

    *sum_tot = tot;
    *sum_tot_sqr = tot * tot;
}
