#include "histoTestVerification.h"
#include "histClient.h"
#include "rodHisto.hxx"
#include "xintc_l.h"
#include "memoryManagement.h"

#define TOT(v, s, b) ((v >> s) & ((1 << b) - 1))

#define SHORT_TOT_MISSING_OCC(v) \
                             TOT(v, ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT, \
                                    MISSING_TRIGGERS_RESULT_BITS)
#define SHORT_TOT_SUM(v)     TOT(v, ONEWORD_TOT_RESULT_SHIFT, tot_result_bits)
#define SHORT_TOT_SUM_SQR(v) TOT(v, ONEWORD_TOTSQR_RESULT_SHIFT, TOTSQR_RESULT_BITS)

#define LONG_TOT_OCC(v)      TOT(v, TWOWORD_OCC_RESULT_SHIFT, OCC_RESULT_BITS)
#define LONG_TOT_SUM(v)      TOT(v, twoword_tot_result_shift, tot_result_bits)
#define LONG_TOT_SUM_SQR(v)  TOT(v, twoword_totsqr_result_shift, totsqr_result_bits)

#define MIN(a, b) (((a) < (b)) ? (a) : (b))

typedef int (*is_value_correct_f)(const is_value_correct_args_t* args, int overflow, int end_of_masked_step);

static int isOnlineOccupancyCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step);
static int isOfflineOccupancyCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step);
static int isShortTotCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step);
static int isLongTotCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step);

int isCorrect(const uint32_t unit, test_data_t* data)
{
    int is_correct = 1;
    
    is_value_correct_args_t args;
    args.unit = unit;
    args.config = &histConfig.cfg[unit];
    args.data = data;
    args.value = histogram_buffers[unit];
    
    const int max_chip = args.config->chipSel ? maxChips : 1;
    //  Change maxChips to nChips once (if ever) slave firmware uses nChips (as it should).

    int chip;

    is_value_correct_f is_value_correct;
        int words_per_value = 1,
            values_per_word = 1;

    switch (args.config->type) {
        case ONLINE_OCCUPANCY:
            is_value_correct = isOnlineOccupancyCorrect;
            break;
        case OFFLINE_OCCUPANCY:
            is_value_correct = isOfflineOccupancyCorrect;
            values_per_word = 4;
            break;
        case SHORT_TOT:
            is_value_correct = isShortTotCorrect;
            if(isFei3 == 1) words_per_value = 2;
            break;
        case LONG_TOT:
            is_value_correct = isLongTotCorrect;
            words_per_value = 2;
            break;
        default:
            verbmsg("ERROR: Invalid mode of unit %d: %d (must be 0..3)",
                unit, args.config->type);
            return 0;
    }
    
    
    int overflow = 0;
    for (chip = 0; chip < max_chip; ++chip) {
        for (args.masked_row = 1;
             args.masked_row <= row_size / data->row_mask;
             ++args.masked_row)
        {
            args.column = overflow + 1;

            do {
// verbmsg("XxXxXxX Unit:  %d, Chip  %d, Masked Row:  %d, Column:  %d, packet 0:  0x%8.8x, packet 1:  0x%8.8x", unit, chip, args.masked_row, args.column, *(args.value), (*(args.value+1)));
// if(chip < nChips && *(args.value) == 0)verbmsg("XxXxXxX Unit:  %d, Chip  %d, Masked Row:  %d, Column:  %d, Value:  %d, in hex:  0x%8.8x, Next in hex:  0x%8.8x", unit, chip, args.masked_row, args.column, *(args.value), *(args.value), *(args.value+1));
                 overflow = 0;
                 if(args.column + values_per_word - 1 > (int)col_size) overflow = args.column + values_per_word - 1 - (int)col_size;
                 int end_of_masked_step = 0;
                 if(args.masked_row == row_size / data->row_mask) end_of_masked_step=1;
// verbmsg("YyYyYyY overflow: %d, end_of_masked_step: %d", overflow, end_of_masked_step);
                 if(chip < nChips) {
                   if (!is_value_correct(&args, overflow, end_of_masked_step)) {
                       if (CANCEL_TEST_ON_FIRST_ERROR) {
                           return 0;
                       }
                       is_correct = 0;
                   }
                 }
                 
                 args.value += words_per_value;
                 args.column += values_per_word;
            } while(args.column < (int)col_size + 1);
        }
    }

//     if(is_correct)showLastGoodResult(" LAST GOOD ", &args);
    if(is_correct)showLastGoodResult("    PASSED ", &args);
    return is_correct;
}

static void showFail(char* message,
                     const unsigned long given,
                     const unsigned long expected,
                     const unsigned long unit,
                     const unsigned long masked_row,
                     const unsigned long column)
{
    xil_printf("\r\n[%d:]%s is %d (expected %d) @ unit %d, masked_row %d, column %d",
        slaveId, message, given, expected, unit, masked_row, column);
}

static int isOccupancyCorrect(const unsigned long occupancy,
                              const is_value_correct_args_t* args,
                              const int masked_row,
                              const int column)
{
    const unsigned long expected = args->config->expectedOccValue;

    if (isHitPixel(args->data, masked_row, column)) {
        if (occupancy != expected) {
// xil_printf("occupancy: %d\n", occupancy);
            showFail("Occupancy",
                occupancy, expected, args->unit, masked_row, column);
            return 0;
        }
    }
    else if (occupancy != 0) {
        showFail("Occupancy",
            occupancy, 0, args->unit, masked_row, column);
        return 0;
    }

    return 1;
}

int isOnlineOccupancyCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step)
{
    return isOccupancyCorrect(*args->value, args, args->masked_row, args->column);
}

int isOfflineOccupancyCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step)
{
    unsigned long occupancy;
    int i;

    for (i = 0; i < 4; ++i) {
        occupancy = (*args->value >> i * OCC_RESULT_BITS)
                    & ((1 << OCC_RESULT_BITS) - 1);

        if(i + overflow < 4 ) {
            if (!isOccupancyCorrect(occupancy, args, args->masked_row, args->column + i)) {
                return 0;
            }
        } else {
          if(end_of_masked_step == 0) {
            if (!isOccupancyCorrect(occupancy, args, args->masked_row + 1, i + overflow - 3)) {
                return 0;
            }
          }
        }
    }

    return 1;
}

static int compare(const is_value_correct_args_t* args,
                   char* message,
                   const unsigned long given,
                   const unsigned long expected)
{
    if (given != expected) {
        showFail(message,
            given, expected, args->unit, args->masked_row, args->column);
        return 0;
    }
    return 1;
}

static void getExpectedTotSums(const is_value_correct_args_t* args,
                               unsigned long* exp_sum_tot,
                               unsigned long* exp_sum_tot_sqr)
{
    getTotSums(args->data,
               exp_sum_tot,
               exp_sum_tot_sqr,
               args->masked_row,
               args->column);
    
    *exp_sum_tot *= args->config->expectedOccValue;
    *exp_sum_tot_sqr *= args->config->expectedOccValue;
}

int isShortTotCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step)
{
    unsigned long values = *args->value;
    unsigned long given_missing_occ, given_sum_tot, given_sum_tot_sqr;
    unsigned long exp_missing_occ, exp_sum_tot, exp_sum_tot_sqr;
    int is_correct;

    if (isHitPixel(args->data, args->masked_row, args->column)) {
        if(isFei3 == 0){
          given_missing_occ = SHORT_TOT_MISSING_OCC(values);
          given_sum_tot     = SHORT_TOT_SUM(values);
          given_sum_tot_sqr = SHORT_TOT_SUM_SQR(values);
        }else{
          given_missing_occ = SHORT_TOT_MISSING_OCC(values);
          given_sum_tot     = values >> (8 * sizeof(unsigned long) - tot_result_bits);
          given_sum_tot_sqr = *(args->value + 1);
        }
        
        exp_missing_occ = 0;
        getExpectedTotSums(args, &exp_sum_tot, &exp_sum_tot_sqr);
        
        is_correct  = compare(args, "Missing OCC", given_missing_occ, exp_missing_occ);
        is_correct &= compare(args, "Sum ToT",     given_sum_tot,     exp_sum_tot);
        is_correct &= compare(args, "Sum ToT²",    given_sum_tot_sqr, exp_sum_tot_sqr);

        if (!is_correct) {
            return 0;
        }
    }
    else {
        const unsigned long empty = MIN(0xF, args->config->expectedOccValue);
        
        if (!compare(args, "Empty value", values, empty)) {
            return 0;
        }
    }

    return 1;
}

int isLongTotCorrect(const is_value_correct_args_t* args, int overflow, int end_of_masked_step)
{
    unsigned long value_occ = *args->value,
                  value_tot = *(args->value + 1);
    unsigned long given_occ, given_sum_tot, given_sum_tot_sqr;
    unsigned long exp_occ, exp_sum_tot, exp_sum_tot_sqr;
    int is_correct;

    if (isHitPixel(args->data, args->masked_row, args->column)) {
        if(isFei3 == 0){
          given_occ         = LONG_TOT_OCC(value_occ);
          given_sum_tot     = LONG_TOT_SUM(value_tot);
          given_sum_tot_sqr = LONG_TOT_SUM_SQR(value_tot);
        }else{
          given_occ = LONG_TOT_OCC(value_occ);
          given_sum_tot     = value_occ >> (8 * sizeof(unsigned long) - tot_result_bits);
          given_sum_tot_sqr = *(args->value + 1);
        }
        
        exp_occ = args->config->expectedOccValue;
        getExpectedTotSums(args, &exp_sum_tot, &exp_sum_tot_sqr);

        is_correct  = compare(args, "OCC",      given_occ,         exp_occ);
        is_correct &= compare(args, "Sum ToT",  given_sum_tot,     exp_sum_tot);
        is_correct &= compare(args, "Sum ToT²", given_sum_tot_sqr, exp_sum_tot_sqr);

        if (!is_correct) {
            return 0;
        }
    }
    else {
        is_correct  = compare(args, "Empty OCC",    value_occ, 0);
        is_correct &= compare(args, "Sum ToT/ToT²", value_tot, 0);

        if (!is_correct) {
            return 0;
        }
    }

    return 1;
}

void showLastGoodResult(char* message, is_value_correct_args_t* args)
{
    uint32_t mode;
    uint32_t row_mask_mode;
    unsigned long exp_sum_tot=0, exp_sum_tot_sqr=0;
    mode = args->config->type;
    row_mask_mode = args->config->maskStep;
    if(mode == SHORT_TOT || mode == LONG_TOT){
      getExpectedTotSums(args, &exp_sum_tot, &exp_sum_tot_sqr);
    }
    xil_printf("\r\n%s| %s | %s |            %2d |   %3d | %s | %3d |%5ld | %7ld | ",
        message, hms[mode], rms[row_mask_mode],
        (args->data->row_filter == 0 ? args->data->row_offset : args->data->row_filter),
        (args->config->chipSel ? nChips : 1), args->data->pattern,
        args->config->expectedOccValue, exp_sum_tot, exp_sum_tot_sqr);
}
