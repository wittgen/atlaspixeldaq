/*
 * Filename:    circularBuffer.h
 * Date:        February 2014
 * Author:      Lohse (h.lohse@stud.uni-heidelberg.de)
 *
 * Summary:
 *      Implements circular buffer of arbitrary sized items.
 *      Aligns item data memory to specified alignment bytes.
 *      Item information and header are put behind read-/writeable item data.
 *      Reading or writing item memory does not imply pop or push semantics.
 *      Pop and push have to be handled manually.
 */

/*      
 *      Example buffer usage
 * - Each symbol represents 4 bytes:
 *   D (user data), I (cb_item_t), H (cb_item_header_t), . (unused/alignment)
 * - Pointers below:
 *   H (head), T (tail), U (unused memory) and --| (next)
 * - Alignment: 16 bytes
 * - Buffer size: 128 bytes, aligned at byte 0
 * - 3 items pushed with 4, 12 and 8 bytes of data
 *
 *      DIIHH...DDDIIHH.DDIIHH..........
 *         H---------|------T   U
 */

#ifndef CIRCULAR_BUFFER_H
#define CIRCULAR_BUFFER_H

#include <stdint.h>

struct cb_item_header_t;

typedef struct {
    volatile void* data;
    uint32_t size;
} cb_item_t;

typedef struct cb_item_header_t {
    cb_item_t* item;
    struct cb_item_header_t* next;
} cb_item_header_t;

typedef struct {
    volatile void* buffer_start;
    volatile void* buffer_end;
    
    volatile void* buffer_start_aligned;
    volatile void* buffer_end_aligned;
    uint32_t alignment_bytes;

    cb_item_header_t* head;
    cb_item_header_t* tail;
    volatile void*    unused_memory;
} circular_buffer_t;

// Uses given buffer and aligns it using given alignment information.
void cbInitialize(
    circular_buffer_t* cb,
    volatile void* buffer,
    const uint32_t buffer_size_bytes,
    const uint32_t alignment_bytes);

// Get front item to read data from. NULL if empty.
cb_item_t* cbGetReadItem(const circular_buffer_t* cb);
// Pop front item. Does nothing if empty.
void cbPop(circular_buffer_t* cb);

// Get new back item to write data of item_size bytes to. NULL if too large.
// NOTE: Push item before requesting another to prevent corruption.
cb_item_t* cbGetWriteItem(const circular_buffer_t* cb,
                          const uint32_t item_size);
// Push item returned by cbGetWriteSlot.
// MUST use item returned by cbGetWriteItem.
void cbPush(circular_buffer_t* cb, cb_item_t* item);

// Get item to read data from behind given item. NULL if none available.
cb_item_t* cbGetReadItemBehind(const circular_buffer_t* cb,
                               const cb_item_t* item);

// Empties buffer, but keeps buffer and alignment.
void cbClear(circular_buffer_t* cb);

// Check if circular buffer is empty
int cbIsEmpty(const circular_buffer_t* cb);

#endif

