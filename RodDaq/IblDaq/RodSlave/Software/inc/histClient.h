/*
 * Filename:   histclient.h
 * Author:     Kugel
 *             Nick Dreyer (L1/L2 Upgrade)
 *
 * Summary:
 *   Header file for the histClient IBL ROD slave software
 */

#ifndef HISTCLIENT_H_
#define HISTCLIENT_H_

#ifdef MAIN
#define GLOBAL
#else
#define GLOBAL extern
#endif

#include "xil_printf.h"
#include <stdint.h>

#ifndef HISTO_TEST
#include "netif/xadapter.h"
#endif

#include "iblSlaveCmds.h"

// Pixel geometry parameters and maximums per histoUnit (Note:  2 units per slave)
GLOBAL uint32_t isFei3, row_size, col_size, pixelsPerChip, chipsPerMod, nChips, maxChips;

// replacements for #define variables to avoid having to separate histClient elf files (histTest still needs separate ones).
GLOBAL uint32_t chip_bits, tot_bits, tot_shift,
                opmode_online_occupancy, opmode_offline_occupancy, opmode_short_tot, opmode_long_tot,
                tot_result_bits, totsqr_result_bits,
                twoword_tot_result_shift, twoword_totsqr_result_shift,
                twoword_totsqr_result_shift,
                rowmaskctl_bits, opmodectl_shift, mstepindex_shift_even,
                maxPossiblePixels;

GLOBAL char* hms[4];
GLOBAL char* rms[6];

// vectors
extern void *_exception_handler;	// offset 0x08
#define EXCEPT_VECTOR 8
extern void *_interrupt_handler;	// offset 0x10
#define IRQ_VECTOR 0x10
extern void *_hw_exception_handler; // offset 0x20
#define HW_EXCEPT_VECTOR 0x20

// do not use initialisation of statics or globals!!!

GLOBAL volatile uint32_t *epcCtlReg;
GLOBAL volatile uint32_t *svdCtlReg; //  Used to restore epcCtlReg after testHistogramming is done
GLOBAL volatile uint32_t *epcStatReg;
GLOBAL volatile uint32_t *busRam;
GLOBAL volatile uint32_t *epcIdReg;

GLOBAL volatile IblSlvRdWr *cmd;
GLOBAL volatile iblSlvStat *cmdStat;

GLOBAL int verbose;
GLOBAL int simulation;
GLOBAL int slaveId;

GLOBAL int usingMUX;

// hist state is one of: idle, active (during histogramming), data available (after histogramming)
// encoding is done via: define SLV_STAT_OK, SLV_STAT_BUSY, SLV_STAT_DATA
GLOBAL int histState;

GLOBAL IblSlvHistCfg histConfig;

#ifndef HISTO_TEST
GLOBAL IblSlvNetCfg netConfig;
GLOBAL struct netif server_netif;

GLOBAL int slowTimerFlag;
GLOBAL int fastTimerFlag;
#endif

#define MAX_PIXELS 368640 //  Just always use the largest value, i.e. ((1 << 7) * 2880)
                          //                                     from ((1 << CHIP_BITS_L12) * pixelsPerChip)

// test single front end
#define ONEFE_ROWCTL 0
#define	ONEFE_COLCTL 0

#define BIT_SET(a,b) ((a) |= (1<<(b)))
#define BIT_CLEAR(a,b) ((a) &= ~(1<<(b)))
#define BIT_FLIP(a,b) ((a) ^= (1<<(b)))
#define BIT_CHECK(a,b) ((a) & (1<<(b)))

#define BITMASK_SET(x,y) ((x) |= (y))
#define BITMASK_CLEAR(x,y) ((x) &= (~(y)))
#define BITMASK_FLIP(x,y) ((x) ^= (y))
#define BITMASK_CHECK(x,y) ((x) & (y))

// Define for simple verbose output.
// Uses the "##" gcc/msvc extension to account for calls with only one argument. */
#define verbmsg(msg, ...) if (verbose) xil_printf("[%d:] " msg "\r\n", slaveId, ##__VA_ARGS__)

/* Set git hash values if not defined by makefile. */
#if !defined(GIT_HASH_1) || !defined(GIT_HASH_2) || !defined(GIT_HASH_3) || !defined(GIT_HASH_4) || !defined(GIT_HASH_5)
#define GIT_HASH_1 0000000
#define GIT_HASH_2 0000000
#define GIT_HASH_3 0000000
#define GIT_HASH_4 0000000
#define GIT_HASH_5 0000000
#endif

/* Macros to handle git hashes. */
#define STRINGIFY(x) #x
#define TOSTRING(x) STRINGIFY(x)

// --------- functions --------
void cmdLoop();
void setupAddresses();
void setupGeometry();
void crc32(char *p, int nr, uint32_t *cval);
void startHistogramming();
IblSlvDmaTransfer stopHistogramming();
void abortHistogramming();
// void testHistogramming(IblSlvRdWr *cmd);
void testHistogramming();
void fillHashes(IblSlvHashes *pHashes);

#ifndef HISTO_TEST
void checkTcpTimers();

int start_applications();
int transfer_data();
#endif

#endif /* HISTCLIENT_H_ */
