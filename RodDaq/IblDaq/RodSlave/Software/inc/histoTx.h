/*
 * Filename:    histoTx.h
 * Date:        February 2014
 * Author:      Lohse (h.lohse@stud.uni-heidelberg.de)
 *
 * Summary:
 *      Provides histo_tx variable and associated functions to handle sending
 *      of circular buffered histogram data.
 *      After specifying connection and buffer, sending can be started.
 *      Call continue function until histo_tx is ready again.
 */

#ifndef HISTO_TX_H
#define HISTO_TX_H

#include "iblSlaveNetCmds.h"
#include "circularBuffer.h"
#include "network.h"
#include <stdint.h>
#include <stdbool.h>

typedef struct {
    connection_t*      connection;
    circular_buffer_t* buffer;

    cb_item_t* buffering_item;
    RodSlvTcpCmd header_command;
    uint32_t sent_bytes;
    
    uint8_t* data;
    uint32_t data_size;
    uint32_t buffered_bytes;

    bool is_sending_header_command;
} histo_tx_t;

extern histo_tx_t histo_txs[2];

// Call once first
void histoTxSetup();

// Initialize each histo_tx with a connection via which to send buffer data
void histoTxInitialize(histo_tx_t* histo_tx,
                       connection_t* connection,
                       circular_buffer_t* buffer);

// Continue buffering of circular buffer items on connection
void histoTxTransmit(histo_tx_t* histo_tx);

#endif

