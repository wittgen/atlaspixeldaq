/*
 * Filename:   memoryManagement.h
 * Date:       March 2014
 * Author:     Lohse (h.lohse@student.ziti.uni-heidelberg.de)
 *
 * Summary:
 *      Information shared by net and no net configurations.
 */

#ifndef MEMORY_MANAGEMENT_H_
#define MEMORY_MANAGEMENT_H_

#include <stdint.h>

#define RAM_BUFFER_SIZE_MB 100
#define RAM_BUFFER_SIZE_WORDS (RAM_BUFFER_SIZE_MB * 1024 * 1024 / 4)
#define DMA_RAM_ALIGNMENT_BYTES 256

extern volatile uint32_t* histogram_buffers[2];

#ifndef HISTO_TEST
#include "circularBuffer.h"

extern circular_buffer_t histogram_circular_buffers[2];

void generateFakedHistogramData();
void initializeHistogramTransfer();
void transferHistogramData();
#endif

/* Initializes circular buffer for one histogrammer
 * histo_unit: 0 or 1, identifies the histogrammer */
void setupHistoBuffer(const uint32_t histogram_unit);

uint32_t getChipDataSize(const uint32_t histogram_unit);

#endif // MEMORY_MANAGEMENT_H_

