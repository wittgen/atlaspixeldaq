/*
 * Filename:    network.h
 * Date:        February 2014
 * Author:      Lohse (h.lohse@stud.uni-heidelberg.de)
 *
 * Summary:
 *      Offers network variable as network_t struct, encapsulating:
 *      - Network interface and configuration in use
 *      - TCP connections with host information
 *      Offers functions operating on network variable to:
 *      - Use configuration to set up network interface, open TCP connections
 *      - Write data to a TCP connection's send buffer
 *      - Callback for sent data (sent_callback)
 *
 * Usage example:
 *      networkInitialize(netif);
 *      networkApply(configuration);
 *      networkDeactivate(connection); // If e.g. just one connection desired
 *      networkConnect();
 *      while (!network.failed && networkIsConnected()) {
 *          networkWrite(active_connection, data + offset, length);
 *          xemacif_input/checkTcpTimers etc.
 *      }
 */

#ifndef NETWORK_H
#define NETWORK_H

#include "iblSlaveCmds.h"
#include "netif/xadapter.h"
#include "lwip/tcp.h"
#include <stdint.h>
#include <stdbool.h>

#define NETWORK_NUM_CONNECTIONS 2

typedef struct {
    struct tcp_pcb* tcp;
    struct ip_addr  host_ip;
    u16_t           host_port;
    bool            waiting_for_connection;
    bool            connected;
    void*           sent_args;
} connection_t;

typedef struct {
    struct netif*  interface;
    IblSlvNetCfg   configuration;

    unsigned char  mac_address[6];
    struct ip_addr local_ip;
    struct ip_addr gateway_ip;
    struct ip_addr subnet_mask;
    bool           enabled;

    // Can be specified to be called after sent data is ACK'd.
    // Gets passed sending connection's sent_args.
    void (*sent_callback)(void* sent_args, const u16_t length);

    connection_t connections[NETWORK_NUM_CONNECTIONS];

    int connection_attempts;
} network_t;

extern network_t network;

// Zero-initialize network parameters, use given interface
void networkInitialize(struct netif* interface);

// Reset e.g. in case of error
void networkReset();

// Define network parameters, set up interface; does not open TCP connections
// If given configuration is different from the one in use,
// close TCP connections
void networkApply(const IblSlvNetCfg* configuration);

// Check if connection is meant to be used or deactivate it.
// Deactivation is implied by a target host IP address of 0.0.0.0
bool networkIsActiveConnection(connection_t* connection);
void networkDeactivateConnection(connection_t* connection);

// Returns whether there is at least 1 active connection
bool networkIsActive();

// Try to establish a TCP connection of active connections
void networkTryConnect();

// Returns whether all active connections have established a TCP connection
// Always false if all connections inactive; check networkIsActive
bool networkIsConnected();

// Write data to TCP connection's send buffer, return length of buffered data
// NO COPY! Ensure data is available until sent
u16_t networkWrite(connection_t* connection, void* data, const u16_t length, const bool copy);

// Show configuration and state
void networkPrint();

// Clears ringbuffers and resets network
void handleNetworkError();

#endif

