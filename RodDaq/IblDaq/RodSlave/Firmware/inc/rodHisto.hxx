// IBL ROD Spartan-6 histogrammer registers and bit definitions
// This is a generated file, do not edit!
#ifndef ROD_HISTO_H
#define ROD_HISTO_H

#define ROW_SIZE 336
#define COL_SIZE 80
#define CHIP_SIZE (COL_SIZE * ROW_SIZE)

// input format definitions for the histogramming unit
// for debugging purposes only
#define ROW_BITS 9
#define COL_BITS 7
#define CHIP_BITS 3
#define TOT_BITS 4
#define ROW_SHIFT 0
#define COL_SHIFT 9
#define CHIP_SHIFT 16
#define TOT_SHIFT 19

// output format definitions for the histogramming unit
#define OCC_RESULT_BITS 8
#define OCC_ONLINE_RESULT_BITS 24
#define MISSING_TRIGGERS_RESULT_BITS 4
#define TOT_RESULT_BITS 12
#define TOTSQR_RESULT_BITS 16
#define ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT 0
#define ONEWORD_TOT_RESULT_SHIFT (ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT + MISSING_TRIGGERS_RESULT_BITS)
#define ONEWORD_TOTSQR_RESULT_SHIFT (ONEWORD_TOT_RESULT_SHIFT + TOT_RESULT_BITS)
#define TWOWORD_OCC_RESULT_SHIFT 0
#define TWOWORD_TOT_RESULT_SHIFT 0
#define TWOWORD_TOTSQR_RESULT_SHIFT 16

// histogrammer control register
#define CHIPCTL_BITS 1
#define ROWMASKCTL_BITS 2
#define MSTEPINDEXCTL_BITS 4
#define OPMODECTL_BITS 2
#define EXPECTEDOCCCTL_BITS 8
#define CHIPCTL_SHIFT 0
#define ROWMASKCTL_SHIFT 1
#define OPMODECTL_SHIFT 3
#define MSTEPINDEX_SHIFT_ODD 8
#define MSTEPINDEX_SHIFT_EVEN 12
#define EXPECTEDOCCCTL_SHIFT 24

#define ONEFE_CHIPCTL (0 << CHIPCTL_SHIFT)
#define ALLFE_CHIPCTL (1 << CHIPCTL_SHIFT)
#define ROWMASK_NONE (0 << ROWMASKCTL_SHIFT)
#define ROWMASK_2 (1 << ROWMASKCTL_SHIFT)
#define ROWMASK_4 (2 << ROWMASKCTL_SHIFT)
#define ROWMASK_8 (3 << ROWMASKCTL_SHIFT)

#define OPMODE_ONLINE_OCCUPANCY (0 << OPMODECTL_SHIFT)
#define OPMODE_OFFLINE_OCCUPANCY (1 << OPMODECTL_SHIFT)
#define OPMODE_SHORT_TOT (2 << OPMODECTL_SHIFT)
#define OPMODE_LONG_TOT (3 << OPMODECTL_SHIFT)
// use MSTEPINDEX_IGNORE to force acception of ALL pixels
#define MSTEPINDEX_IGNORE 0
// use MSTEPINDEX_STEP1..8 to accept only pixels of a certain step pattern
#define MSTEPINDEX_STEP1 1
#define MSTEPINDEX_STEP2 2
#define MSTEPINDEX_STEP3 3
#define MSTEPINDEX_STEP4 4
#define MSTEPINDEX_STEP5 5
#define MSTEPINDEX_STEP6 6
#define MSTEPINDEX_STEP7 7
#define MSTEPINDEX_STEP8 8

#endif //ROD_HISTO_H
