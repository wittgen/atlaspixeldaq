// IBL ROD Spartan-6 histogrammer registers and bit definitions
// This is a generated file, do not edit!
#ifndef ROD_HISTOL12_ADDENDUM_H
#define ROD_HISTOL12_ADDENDUM_H

#define ROW_SIZE_L12 160
#define COL_SIZE_L12 18

// input format definitions for the histogramming unit
#define CHIP_BITS_L12 7
#define TOT_BITS_L12 8
#define TOT_SHIFT_L12 23

// output format definitions for the histogramming unit
#define TOT_RESULT_BITS_L12 16
#define TOTSQR_RESULT_BITS_L12 24
#define TWOWORD_TOT_RESULT_SHIFT_L12 16
#define TWOWORD_TOTSQR_RESULT_SHIFT_L12 0

// histogrammer control register
#define MSTEPINDEX_SHIFT_EVEN_L12 14

#define ROWMASKCTL_BITS_L12 3
#define OPMODECTL_SHIFT_L12 4

//  Only used for setting MB_HISTx_CTL_REG in histoOps.c.  Not use in any other software.
#define ROWMASK_16 (5 << ROWMASKCTL_SHIFT) //  Very unusual switch for quirky firmware.
#define ROWMASK_32 (4 << ROWMASKCTL_SHIFT) //   "      "      "     "    "       "

#endif //ROD_HISTOL12_ADDENDUM_H
