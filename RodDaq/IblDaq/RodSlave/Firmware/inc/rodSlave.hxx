// IBL ROD spartan-6 formatter slave address and bit definitions
#ifndef ROD_SLAVE_H
#define ROD_SLAVE_H
// This is a generated file, do not edit!

#define MB_VHDL_COMPILE_DATE "2015-12-08T13:15:39+0100"
// make size of ram available
#define MB_SLV_RAM_WORDS 512
// Design parameters
// The following register addresses sit on top of the EPC area, defined in xparameters.h
#ifndef XPAR_AXI_EPC_0_PRH0_BASEADDR
#include "xparameters.h"
#endif
// Number of EPC address bits is an alternative way to identify the address range
#define MB_EPC_ADDRESS_BITS 22
// EPC address range for registers
#define MB_EPC_REG_BASE (XPAR_AXI_EPC_0_PRH0_BASEADDR)
// EPC address range for registers
#define MB_EPC_RAM_BASE (XPAR_AXI_EPC_0_PRH0_BASEADDR + 0x10000)
// Design identification
#define MB_DESIGN_REG (MB_EPC_REG_BASE + 0x0)
// Design identification
#define MB_HDL_FPGA_HAS_GIT_HASH 1
#define MB_HDL_FPGA_GIT_HASH "FF264747221E3C9C73E5AA8A9FDBCAC048FB463D"
// Control register
#define MB_CTL_REG (MB_EPC_REG_BASE + 0x4)
// Status register
#define MB_STAT_REG (MB_EPC_REG_BASE + 0x8)
// Status bits
#define MB_STAT_SIMULATION_BIT 31
#define MB_STAT_EXT_HISTRAM_BIT 30
#define MB_STAT_DMA0_RDY_BIT 29
#define MB_STAT_DMA1_RDY_BIT 28
#define MB_STAT_HIST_RFR0_BIT 27
#define MB_STAT_HIST_RFR1_BIT 26
#define MB_STAT_HIST_RFD0_BIT 25
#define MB_STAT_HIST_RFD1_BIT 24
#define MB_STAT_HIST_ROC0_BIT 23
#define MB_STAT_HIST_ROC1_BIT 22
#define MB_STAT_LAST_BIT 22
// BOC test input address
#define MB_BOC_TEST_REG (MB_EPC_REG_BASE + 0xC)
// BOC serial transmit to master address
#define MB_TX_REG (MB_EPC_REG_BASE + 0x10)
// Histogrammer 0 test input address
#define MB_HIST0_TEST_REG (MB_EPC_REG_BASE + 0x18)
// Histogrammer 1 test input address
#define MB_HIST1_TEST_REG (MB_EPC_REG_BASE + 0x1C)
// Histogrammer 0 control address
#define MB_HIST0_CTL_REG (MB_EPC_REG_BASE + 0x20)
// Histogrammer 1 control address
#define MB_HIST1_CTL_REG (MB_EPC_REG_BASE + 0x24)
// Timer value address (two read cycles for 64bit value)
#define MB_TIMER64_REG (MB_EPC_REG_BASE + 0x28)

// Control register bits
// Input control bits
// CTL_BOC_ENABLE_BIT: 0: input from local FE-I4. 1: BOC input
#define MB_CTL_BOC_ENABLE_BIT 0
// CTL_BOC_TEST_BIT: Select test input for boc. Requires BOC_ENABLE to be 1. Writes to BOC_TEST_ADDR supply data
#define MB_CTL_BOC_TEST_BIT 1
// Histogrammer bits
// CTL_HIST_MODE uses multiple bits with sperate offsets for histo 0 and 1
#define MB_HISTO_MODE_BITS 2 // number of bits to set mode: 
#define MB_CTL_HIST_MODE0_BASE 2 // bit shift for histo 0 mode
#define MB_CTL_HIST_MODE1_BASE 4 // bit shift for histo 1 mode
// CTL_HIST_MODE definitions
// Insert this value at the base bit position of the histogrammer
#define MB_CTL_HIST_MODE_IDLE 0 // Idle is wait state prior to sampling
#define MB_CTL_HIST_MODE_SAMPLE 1 // Sampling
#define MB_CTL_HIST_MODE_WAIT 2 // Wait is wait state prior to readout
#define MB_CTL_HIST_MODE_READOUT 3 // Readout transfer
// CTL_HIST_MUX: 0: test input via write to MB_HISTx_TEST_REG. 1: EFB input
#define MB_CTL_HIST_MUX0_BIT 6
#define MB_CTL_HIST_MUX1_BIT 7
#define MB_CTL_MASTER_MODE_BIT 30 // read-only
#define MB_CTL_MASTER_RESET_BIT 31 // read-only
#endif //ROD_SLAVE_H
