----------------------------------------------------------------------------
-- UW EE/PHYSICS- WASHINGTON
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    bc_l1_check.vhd
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen on 06.11.2013
-- Description:
--    Event Fragment Builder
--    IBLROD/FEI4 Version
--    Checks that the BCID and L1 IDs are correct. Flags any errors in the BC
--    or L1 ID. Data from the event fifo keeps track of an L1 offset value for
--    each link. This offset allows the EFB to compensate for missing or extra
--    triggers on a given link. 
--
--    BCID and L1ID data are "trapped" here to allow the user to view diagnostic data of the current state of
--    event synchronization during data taking mode. This trapping mechanism does not interfere with data
--    processing.
--
--    Beginning of event and end of event signals are added for determining first word in and last word out flags 
--
--    Checks the Hit data to make sure that the FE Modules are sending data 
--    from the chips in ascending order
--
--    INPUT DATA FORMAT: data_in[34:0] (from formatter FIFO that muxes 4 FE links)
--      data_in[31:0]  event data (4 types of words)
--        - header      => [ 001 | p | xx | link#(2) | 0(4) | s(4) | FEI4B flag | L1ID(5) | BCID(10) ]  
--        - hit         => [ 100 | xx(3) | link#(2) | ToT(8) | col(7) | row(9) ]   
--        - error_flags => [ 0001 | xx | link#(2) | x | SR code(6) | xx(7) | SR count(10) ]
--        - trailer     => [ 010 | xx(3) | link#(2) | errors found(24) ]
--      data_in[32]    timeout error         (set in Formatter)
--      data_in[33]    condensed mode        (set in Formatter)
--      data_in[34]    Link masked by PPC    (set in Formatter)
--      * p-preamble error(?) s-# of skipped triggers in FE
--
--    OUTPUT DATA FORMAT: data_out[42:0]
--      data_out[31:0] detector data
--      data_out[35:32] link address
--           - [34:32] link# (0-3, 4-7) -- this gets encoded from incoming data (2b) and gatherer ID (1b)
--           - [35]    efb engine#(0-1) -- probably can take out later, unused
--      data_out[36] timeout error
--      data_out[37] condensed mode
--      data_out[38] L1ID error                      (header only)        
--      data_out[39] BCID error                      (header only)
--      data_out[40] Link masked by PPC                  
--      data_out[41] end of event fragment           (header only, offset from previous word to next word header)
--      data_out[42] Non sequential chip error
--
--    EVENT ID FIFO INPUT DATA FORMAT: ev_fifo_data[26:0]
--      ev_fifo_data[9:0] BCID(10)  
--      ev_fifo_data[14:10] L1ID(5)  
--      ev_fifo_data[18:15] ROD Trigger Type(4) 
--      ev_fifo_data[26:19] Dynamic Mask #(8) supports 4 links (represented by 2 bits each)
--           - L1 Offset Link 0 or 4 <= ev_fifo_data (20 downto 19)
--           - L1 Offset Link 1 or 5 <= ev_fifo_data (22 downto 21)
--           - L1 Offset Link 2 or 6 <= ev_fifo_data (24 downto 23)   
--           - L1 Offset Link 3 or 7 <= ev_fifo_data (26 downto 25)  
--
--    Dynamic Sync Bits Definition
--      Link Offset = "00" <= No Change to L1ID
--      Link Offset = "01" <= Increment L1ID
--      Link Offset = "10" <= Decrement L1ID
--
--    Versions
--    Before 07.31.2013 -- too lazy didn't record :(
--    v. 3.4 07.31.2103
--      got rid of form_enabled, added beginning of event and end of event signals from formatter
--
----------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY bc_l1_check IS
  port (
      clk                 : in  STD_LOGIC ;
      rst                 : in  STD_LOGIC ;
      
		-- from the gatherer/formatter
		data_in             : in  STD_LOGIC_VECTOR(34 DOWNTO 0) ;
      data_valid_in       : in  STD_LOGIC ;
		fmt_beg_of_event    : in  STD_LOGIC ;
		fmt_end_of_event    : in  STD_LOGIC ;

		-- to the error_detect
      data_out            : out STD_LOGIC_VECTOR(42 DOWNTO 0) ;
      data_valid_out      : out STD_LOGIC ;
		
      -- from the ev_data_decoder
      ev_fifo_ren         : out STD_LOGIC ;
      ev_fifo_empty       : in  STD_LOGIC ;
      ev_fifo_empty_error : out STD_LOGIC ;
      ev_fifo_data        : in  STD_LOGIC_VECTOR(34 DOWNTO 0) ;
		
		-- Passed from or specified in top module
      efb_engine_id       : in  STD_LOGIC_VECTOR(1 DOWNTO 0); -- this is an new addition
      gatherer_id         : in  STD_LOGIC ; -- gatherer = formatter sub block
		
      bcid_offset         : in  STD_LOGIC_VECTOR( 9 DOWNTO 0);  -- not used, can throw away, offset determined in ev_data_decode and present in ev_fifo_data
      hitdisccnfg         : in  STD_LOGIC_VECTOR( 7 DOWNTO 0);

      grp_evt_count_out   : out STD_LOGIC_VECTOR(31 downto 0) ;
      grp_evt_enable_in   : in  STD_LOGIC ;
      l1_id_fifo_data     : out STD_LOGIC_VECTOR(19 downto 0) ;  -- goes into the efb_interface_bus (top)
      l1_id_fifo_addr     : in  STD_LOGIC_VECTOR( 3 DOWNTO 0) ;
      enable_l1_trap_in   : in  STD_LOGIC ; 
		
		-- goes to the efb_interface_bus (top)
      l1_trap_full_out    : out STD_LOGIC ;
      latched_l1id        : out STD_LOGIC_VECTOR(12 DOWNTO 0) ;
      latched_bcid        : out STD_LOGIC_VECTOR( 9 DOWNTO 0)
      );
END bc_l1_check ;

architecture rtl of bc_l1_check is

----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------

-- signal buses for registered data inputs
  signal data_in_d1             : STD_LOGIC_VECTOR(34 DOWNTO 0);  
  signal data_in_d2             : STD_LOGIC_VECTOR(34 DOWNTO 0);  
  signal data_in_d3             : STD_LOGIC_VECTOR(34 DOWNTO 0);  
  signal data_valid_in_d1       : STD_LOGIC;
  signal data_valid_in_d2       : STD_LOGIC;
  signal data_valid_in_d3       : STD_LOGIC;
  signal fmt_beg_of_event_d1    : STD_LOGIC;
  signal fmt_beg_of_event_d2    : STD_LOGIC;
  signal fmt_beg_of_event_d3    : STD_LOGIC;
  signal fmt_end_of_event_d1    : STD_LOGIC;
  signal fmt_end_of_event_d2    : STD_LOGIC;
  signal fmt_end_of_event_d3    : STD_LOGIC;

  signal last_word_out          : STD_LOGIC; -- indicates the end of the input data packet
  signal first_word_in          : STD_LOGIC; -- indicates the start of the input data packet
  signal next_to_last_word_out  : STD_LOGIC; -- preemptive signal to the end of data packet
  signal ev_fifo_ren_i          : STD_LOGIC;  
  signal ev_fifo_ren_dly1       : STD_LOGIC;  
  signal get_event_flag         : STD_LOGIC;
  signal processing_event       : STD_LOGIC;

  type   offset_array_type is ARRAY (0 to 3) of UNSIGNED (3 DOWNTO 0); -- modified
  signal l1_offset              : offset_array_type;
  signal l1_id                  : UNSIGNED (4 DOWNTO 0);  
  signal bc_id                  : STD_LOGIC_VECTOR (9 DOWNTO 0);  
  signal grp_id                 : STD_LOGIC_VECTOR (3 DOWNTO 0);  
  signal new_ev_data_flag       : STD_LOGIC ;
  signal expected_l1            : UNSIGNED (4 DOWNTO 0);
  signal header_l1id            : UNSIGNED (12 DOWNTO 0);
  signal expected_bc            : STD_LOGIC_VECTOR (9 DOWNTO 0);
  signal frame_offset           : UNSIGNED (3 downto 0);
  signal mark_link              : STD_LOGIC_VECTOR(2 DOWNTO 0);
  signal num_accept_bc          : UNSIGNED (9 DOWNTO 0);
  signal header_bcid            : UNSIGNED (9 DOWNTO 0);

  signal link_number            : STD_LOGIC_VECTOR (2 DOWNTO 0);  -- modified: 5 downto 0 => 2 downto 0
  signal grp_evt_count_i        : UNSIGNED (31 DOWNTO 0);

-- BRAM is changed to have 20-bit width and 16 words (need 8 but 16 is the minimual depth allowed) deep.
-- For now we use distributed memory due to the lack of BRAM resources.
  signal read_addr  : STD_LOGIC_VECTOR (3 DOWNTO 0);
  signal write_addr : STD_LOGIC_VECTOR (3 DOWNTO 0);
  signal read_data  : STD_LOGIC_VECTOR (19 DOWNTO 0);
  signal write_data : STD_LOGIC_VECTOR (19 DOWNTO 0);
  signal write_en   : STD_LOGIC;
  
  type state_typedef is (
    idle, 
    trap_event,
    hold_for_readout
    );
  signal trap_event_id_state : state_typedef;

-------------------------------------------------------------------------------
-- COMPONENT DECLARATION
-------------------------------------------------------------------------------

COMPONENT dist_ram_16x20
  PORT (
    a : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    d : IN STD_LOGIC_VECTOR(19 DOWNTO 0);
    dpra : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
    clk : IN STD_LOGIC;
    we : IN STD_LOGIC;
    dpo : OUT STD_LOGIC_VECTOR(19 DOWNTO 0)
  );
END COMPONENT;

BEGIN 

-------------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
-------------------------------------------------------------------------------

U1 : dist_ram_16x20
  PORT MAP (
    a => write_addr,
    d => write_data,
    dpra => read_addr,
    clk => clk,
    we => write_en, -- modified from pwr, this is write enable
    dpo => read_data
  );

l1_id_fifo_data <= read_data;  -- top output 16 bits
read_addr       <= l1_id_fifo_addr;  -- top input 8 bits (4 bit address)

-- edited this part for block RAM event trap. Will need to modify according to change in link numbers
-- traps BCID and L1ID for the incoming event data. Operates on single events only 
-- modified (double check)
bram_data : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    write_data <= (others => '0');
    write_addr <= (others => '0');
    write_en <= '0';
    trap_event_id_state <= idle;
    l1_trap_full_out <= '0';
    latched_l1id <= (others => '0'); -- not done in jmj code
    latched_bcid <= (others => '0'); -- not done in jmj code
  elsif (clk'event and clk = '1') then
    write_data(19 downto 10) <= data_in_d3(9 downto 0);  -- module/chip BCID, the original code was SCT version!
    write_data( 9 downto  5) <= std_logic_vector(expected_l1(4 downto 0));  -- expected L1ID
    write_data( 4 downto  0) <= data_in_d3(14 downto 10);  -- module/chip L1ID
	case trap_event_id_state is   
		when idle => 
        l1_trap_full_out <= '0';
        write_en <= '0';
        if (enable_l1_trap_in = '1' AND first_word_in = '1') then
          trap_event_id_state <= trap_event;
          latched_l1id <= "00000000" & std_logic_vector(l1_id);
          latched_bcid <= std_logic_vector(bc_id);
        end if;      
      when trap_event =>
        write_en <= '0';
        if (data_valid_in_d3 = '1') then
          if (data_in_d3(31 downto 29) = "001") then  -- write to BRAM only if word is a header 
            write_en <= '1';
          end if;
        end if;
        if (last_word_out = '1') then
          l1_trap_full_out <= '1';
          trap_event_id_state <= hold_for_readout;
        end if;
      when others =>  -- hold_for_read_out & else
        write_en <= '0';
        if (enable_l1_trap_in = '0') then -- single sequence trap
          trap_event_id_state <= idle;
        end if;
    end case;      

    -- modified, change in link number format, only 3 bits now, also change BRAM config to 16x20
	 write_addr <= '0' & link_number;
--	 case link_number(2 downto 0) is
-- 	when X"0" => write_addr <= link_number;
--		when X"1" => write_addr <= link_number;
--  	when X"2" => write_addr <= link_number;
--		when X"3" => write_addr <= link_number;
--		when X"4" => write_addr <= link_number;
--		when X"5" => write_addr <= link_number;
--		when X"6" => write_addr <= link_number;
--		when X"7" => write_addr <= link_number;
--		when others => write_addr <= X"F";
--  end case;
		
  end if;
end process;

-------------------------------------------------------------------------------
-- 
-------------------------------------------------------------------------------

-- clean up this thing
--form_enabled_d0 <= (form_enabled(3)) & 
--                   (form_enabled(2)) &
--                   (form_enabled(1)) &
--                   (form_enabled(0));

-- This process puts each input in 3 stage registers.
-- Useful for determining first_word_in, last_word_out, next_to_last_word_out
-- kept original
register_data : process ( 
  clk, 
  rst
  )
begin
  if (rst = '1') then
    data_in_d1       <= (others => '0');
    data_in_d2       <= (others => '0');
    data_in_d3       <= (others => '0');
    data_valid_in_d1 <= '0';
    data_valid_in_d2 <= '0';
    data_valid_in_d3 <= '0';
--    form_enabled_d1  <= (others => '0');
--    form_enabled_d2  <= (others => '0');
--    form_enabled_d3  <= (others => '0');
    fmt_beg_of_event_d1 <= '0' ;
    fmt_beg_of_event_d2 <= '0' ;
    fmt_beg_of_event_d3 <= '0' ;
    fmt_end_of_event_d1 <= '0' ;
    fmt_end_of_event_d2 <= '0' ;
    fmt_end_of_event_d3 <= '0' ;
    data_valid_out      <= '0';
  elsif (clk'event and clk = '1') then
    -- register inputs
    data_in_d1 <= data_in;
    data_in_d2 <= data_in_d1;
    data_in_d3 <= data_in_d2;
    
    data_valid_in_d1 <= data_valid_in;
    data_valid_in_d2 <= data_valid_in_d1;
    data_valid_in_d3 <= data_valid_in_d2;

--    form_enabled_d1  <= form_enabled_d0;
--    form_enabled_d2  <= form_enabled_d1;
--    form_enabled_d3  <= form_enabled_d2;
    
	 fmt_beg_of_event_d1 <= fmt_beg_of_event;
    fmt_beg_of_event_d2 <= fmt_beg_of_event_d1;
	 fmt_beg_of_event_d3 <= fmt_beg_of_event_d2;
	 fmt_end_of_event_d1 <= fmt_end_of_event;
    fmt_end_of_event_d2 <= fmt_end_of_event_d1;
	 fmt_end_of_event_d3 <= fmt_end_of_event_d2;

    -- register outputs
    data_valid_out <= data_valid_in_d3;
--    data_valid_out <= ((data_valid_in_d3 AND form_enabled_d3(0)) OR
--                       (data_valid_in_d3 AND form_enabled_d3(1)) OR
--                       (data_valid_in_d3 AND form_enabled_d3(2)) OR
--                       (data_valid_in_d3 AND form_enabled_d3(3)));


  end if;
end process register_data;


-- kept original
detect_event_start_end: process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    ev_fifo_empty_error <= '0';
    get_event_flag      <= '1';
    processing_event    <= '0';
  elsif (clk'event and clk = '1') then
    -- event fifo read enable
    if (next_to_last_word_out = '1') then
      get_event_flag   <= '1';
      processing_event <= '0';
    elsif (ev_fifo_ren_i = '1') then
      get_event_flag   <= '0';
      processing_event <= '1';
    end if;
    -- check if data from new event has arrived but event fifo is empty
    -- this is a fatal sync error that cannot be recovered, reset needed
    if (first_word_in = '1' AND processing_event = '0') then
      ev_fifo_empty_error <= '1';
    end if;
  end if;
end process detect_event_start_end;

ev_fifo_ren_i <= (NOT ev_fifo_empty) and get_event_flag;
ev_fifo_ren   <= ev_fifo_ren_i;


--first_word_in         <= (form_enabled_d2(0) AND NOT form_enabled_d3(0));
--next_to_last_word_out <= (form_enabled_d1(3) AND NOT form_enabled_d0(3));
--last_word_out         <= (form_enabled_d3(3) AND NOT form_enabled_d2(3));

first_word_in         <= (fmt_beg_of_event_d2 AND NOT fmt_beg_of_event_d3);
next_to_last_word_out <= (fmt_end_of_event_d1 AND NOT fmt_end_of_event);  -- may be changed
last_word_out         <= (fmt_end_of_event_d3 AND NOT fmt_end_of_event_d2);


-- ev_fifo_data[26:0] = dynmask(8) & rodTriggerType(4) & L1ID(5) & BCID(10)
-- modified (need to double check)
get_l1_bc_id : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    new_ev_data_flag <= '0';
    ev_fifo_ren_dly1 <= '0';
    l1_id  <= (others => '0');
    bc_id  <= (others => '0');
    grp_id <= (others => '0');
    for i in 0 to 3 loop   -- changed from 0-47 to 0-3, dyna mask for 4 links
      l1_offset(i) <= (others => '0');
    end loop;
  elsif (clk'event and clk = '1') then
    ev_fifo_ren_dly1 <= ev_fifo_ren_i;    -- add 1 clock of delay
    new_ev_data_flag <= ev_fifo_ren_dly1;
    if (new_ev_data_flag = '1') then
      l1_id  <= unsigned(ev_fifo_data(14 downto 10));
      bc_id  <= ev_fifo_data(9 downto 0);
      grp_id <= ev_fifo_data(26 downto 23); -- does this still exist? Is this ROD Trigger Type?
      for i in 0 to 3 loop  -- changed loop from 0 to 47 to 0 to 3, dyna mask for 4 links 
        if (ev_fifo_data((28+(i*2)) downto (27+(i*2))) = "01") then -- increment
          l1_offset(i) <= l1_offset(i) + 1;  -- individual offset (check)
        elsif (ev_fifo_data((28+(i*2)) downto (27+(i*2))) = "10") then -- decrement
          l1_offset(i) <= l1_offset(i) - 1;
        end if;
      end loop;
    end if;
  end if;
end process get_l1_bc_id;

-- modified (need to double check)
compute_expected_l1 : process (
  link_number, 
  l1_id, 
  l1_offset
  )
begin
  case link_number is
    when "000"  => expected_l1 <= l1_id + l1_offset(0);-- + 1 ;  -- the +1 offset per ATC-TD-EC-0002, still needed?
    when "001"  => expected_l1 <= l1_id + l1_offset(1);-- + 1 ; 
    when "010"  => expected_l1 <= l1_id + l1_offset(2);-- + 1 ;
    when "011"  => expected_l1 <= l1_id + l1_offset(3);-- + 1 ;
    when "100"  => expected_l1 <= l1_id + l1_offset(0);-- + 1 ;
    when "101"  => expected_l1 <= l1_id + l1_offset(1);-- + 1 ;
    when "110"  => expected_l1 <= l1_id + l1_offset(2);-- + 1 ;
    when "111"  => expected_l1 <= l1_id + l1_offset(3);-- + 1 ;
    when others => expected_l1 <= l1_id;
  end case;
end process compute_expected_l1;
  
-- expected_bc <= bc_id + bcid_offset;
expected_bc <= bc_id;  -- The offset is added for in the ev_data_decode block

link_number(2) <= gatherer_id;

---- modified (complete)
--decode_link_number: process (
--  form_enabled_d3,
--  data_in_d3
--  )
--begin
----  link_number(2) <= gatherer_id;
--  if (form_enabled_d3 = "1000") then  -- one hot decoding
--    link_number(1 downto 0) <= "11";
--  elsif (form_enabled_d3 = "0100") then
--    link_number(1 downto 0) <= "10";
--  elsif (form_enabled_d3 = "0010") then
--    link_number(1 downto 0) <= "01";
--  else
--    link_number(1 downto 0) <= "00";
--  end if;
--end process decode_link_number;

decode_link_number: process (  -- with the new change in FE error word format...
  data_in_d3
  )
begin
  link_number(1 downto 0) <= data_in_d3(25 downto 24);
end process decode_link_number;  

-- Output data formatting, flag L1 & BCID errors, checks ascending chip order
-- Got rid of ROD type
-- modified (double check)
check_id_tags : process (
  clk, 
  rst
  )
begin
   if (rst = '1') then
     data_out      <= (others => '0');
     num_accept_bc <= (others => '0');
     frame_offset  <= (others => '0');
     mark_link     <= (others => '0');
   elsif (clk'event and clk = '1') then

		 data_out(25 downto  0) <= data_in_d3(25 downto  0);
		 data_out(28 downto 26) <= efb_engine_id & gatherer_id;
		 data_out(31 downto 29) <= data_in_d3(31 downto 29);
       data_out(34 downto 32) <= link_number;
       data_out(35)           <= efb_engine_id(0);  -- double check this, do we need this later on?
       data_out(37 downto 36) <= data_in_d3(33 downto 32); -- modified, pass down TO error flag (32) and condensed mode (33)
       data_out(40)           <= data_in_d3(34);  -- pass on masked by PPC flag      
	
       -- Latch L1ID and BCID from Header
       if(data_in_d3(31 downto 29) = "001" AND data_in_d3(32) = '0') then--Header with no TO Error
         header_l1id <= unsigned(data_in_d3(22 downto 10));
         header_bcid <= unsigned(data_in_d3( 9 downto  0));
         if(mark_link(2) = '1' AND mark_link(1 downto 0) = data_in_d3(25 downto 24)) then
            frame_offset <= frame_offset + 1;
            mark_link(2) <= '0';
         else
            frame_offset <= (others => '0');
         end if;
       end if;
       --Once you get a trailer then decode it and check L1ID and BCID
       --Skipped events and flags set in trailer
       if(data_in_d3(31 downto 29) = "010" AND data_in_d3(32) ='0') then
          if(header_l1id(3 downto 0) /= expected_l1(3 downto 0)) then
             data_out(19) <= '1'; --L1ID Error in Trailer
             data_out(38) <= '1'; --L1ID Error in Data Word
          else
             data_out(19) <= '0';
             data_out(38) <= '0';
          end if;
                             --Expected BCID + Skipped Events + frame_offset
          if(header_bcid /= (unsigned(expected_bc) + unsigned(data_in_d3(14 downto 5)) + frame_offset)) then
             data_out(18) <= '1'; --BCID Error in Trailer
             data_out(39) <= '1'; --BCID Error in Data Word
          else
             data_out(18) <= '0';
             data_out(39) <= '0';
          end if;
      else
         data_out(38) <= '0';
         data_out(39) <= '0';
      end if;

      --Find hit, convert "true" ToT
      if(data_in_d3(31 downto 29) = "100") then
         if(hitdisccnfg((2*(conv_integer(data_in_d3(25 downto 24))) + 1) downto (2*(conv_integer(data_in_d3(25 downto 24))))) = "00") then
           if(data_in_d3(23 downto 20) > "1101") then --First ToT
              data_out(23 downto 20) <= "0000"; --No Hit
           else
              data_out(23 downto 20) <= unsigned(data_in_d3(23 downto 20)) + 1;
           end if;
           if(data_in_d3(19 downto 16) > "1101") then --Second ToT
              data_out(19 downto 16) <= "1111";	-- DAV
--				  data_out(19 downto 16) <= "0000";
           else
              data_out(19 downto 16) <= unsigned(data_in_d3(19 downto 16)) + 1;
           end if;

         elsif(hitdisccnfg((2*(conv_integer(data_in_d3(25 downto 24))) + 1) downto (2*(conv_integer(data_in_d3(25 downto 24))))) = "01") then
           if(data_in_d3(23 downto 20) = "1110") then --E goes to 1
              data_out(23 downto 20) <= "0001";
           elsif(data_in_d3(23 downto 20) = "1111") then -- F = no hit
              data_out(23 downto 20) <= "0000";
           else
              data_out(23 downto 20) <= unsigned(data_in_d3(23 downto 20)) + 2;
           end if;
           if(data_in_d3(19 downto 16) = "1110") then --Second ToT
              data_out(19 downto 16) <= "0001";
           elsif(data_in_d3(19 downto 16) = "1111") then 
--              data_out(19 downto 16) <= "0000";
					 data_out(19 downto 16) <= "1111";	-- DAV
           else
              data_out(19 downto 16) <= unsigned(data_in_d3(19 downto 16)) + 2;
           end if;

         elsif(hitdisccnfg((2*(conv_integer(data_in_d3(25 downto 24))) + 1) downto (2*(conv_integer(data_in_d3(25 downto 24))))) = "10") then
           if(data_in_d3(23 downto 20) = "1111") then --First ToT
              data_out(23 downto 20) <= "0000";
           elsif(data_in_d3(23 downto 20) = "1101") then --D -> F
              data_out(23 downto 20) <= "1111";
           else
              data_out(23 downto 20) <= unsigned(data_in_d3(23 downto 20)) + 3; --E +3 = 1
           end if;
           if(data_in_d3(19 downto 16) = "1111") then --Second ToT
--              data_out(19 downto 16) <= "0000";
					data_out(19 downto 16) <= "1111";	-- DAV
           elsif(data_in_d3(19 downto 16) = "1101") then
              data_out(19 downto 16) <= "1111";
           else
              data_out(19 downto 16) <= unsigned(data_in_d3(19 downto 16)) + 3;
           end if;

         else --HitDiscCnfg = "11" not allowed
           data_out(23 downto 20) <= data_in_d3(23 downto 20);
           data_out(19 downto 16) <= data_in_d3(19 downto 16);
         end if;
      end if;

		-- Check for non-sequential (should be ascending) Chip Numbers in the Data
		-- modified (complete)
		if (data_in_d3(31) = '1' AND  -- pres Data word
			 data_in_d2(31) = '1' AND  -- next data word
			 data_in_d3(25 downto 24) > data_in_d2(25 downto 24))then -- need to only check the 4 links
		  data_out(42) <= '1'; -- flag if present data link# is greater than next
		else
		  data_out(42) <= '0';
		end if;

      data_out(41) <= last_word_out;  -- flag the end of event fragment

   end if;
end process check_id_tags;


-- should be kept original (double check)
group_event_count : process (
  clk, 
  rst
  )
begin
  if (rst = '1') then
    grp_evt_count_i <= (others => '0');
  elsif (clk'event and clk = '1') then
    if (grp_evt_enable_in = '0') then 
      grp_evt_count_i <= (others => '0');
    else
      if (last_word_out = '1') then  -- perform at the end of event
        case grp_id is
          when "0000" => grp_evt_count_i( 3 downto  0) <= grp_evt_count_i( 3 downto  0) + 1;
          when "0001" => grp_evt_count_i( 7 downto  4) <= grp_evt_count_i( 7 downto  4) + 1;
          when "0010" => grp_evt_count_i(11 downto  8) <= grp_evt_count_i(11 downto  8) + 1;
          when "0011" => grp_evt_count_i(15 downto 12) <= grp_evt_count_i(15 downto 12) + 1;
          when "0100" => grp_evt_count_i(19 downto 16) <= grp_evt_count_i(19 downto 16) + 1;
          when "0101" => grp_evt_count_i(23 downto 20) <= grp_evt_count_i(23 downto 20) + 1;
          when "0110" => grp_evt_count_i(27 downto 24) <= grp_evt_count_i(27 downto 24) + 1;
          when "0111" => grp_evt_count_i(31 downto 28) <= grp_evt_count_i(31 downto 28) + 1;
          when others => null;
        end case;
      end if;
    end if;
  end if;
end process group_event_count;

grp_evt_count_out <= STD_LOGIC_VECTOR(grp_evt_count_i);

END rtl;
