----------------------------------------------------------------------------
-- UW EE/PHYSICS- WASHINGTON
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    ev_data_decode.vhd
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen 06.2013
-- Description:
--    ROD Dual Gatherer
--    IBLROD/FEI4 Version
--    Reads Event FIFO data from the external FIFO and writes the 8 bits
--    BCID and 4 bit L1 ID data into the Event ID FIFOs of each gatherer.
--    The full L1, BC ID, and Trig type info are sent to the logic that
--    generates the event fragment header/trailer.
--
--    A full event of data must be read out over 8 words from the
--    external FIFO. Since the
--    -- The 4K FIFO can buffer the data from 240 Events
--
--  INPUT EVENT DATA from PPC - 8 words
--    word 0  : L1 ID[15:0]                                ev_data_sh_reg(15: 0)
--    word 1  : ECR ID[7:0] & L1 ID[23:16]                 ev_data_sh_reg(31:16)
--    word 2  : BC ID[11:0] & x & RoL & BOC OK & TIM OK    ev_data_sh_reg(47:32)  -- RoL is readout link test
--    word 3  : TT - ROD[5:0] & TIM[1:0] & ATLAS[7:0]      ev_data_sh_reg(63:48)
--    word 4  : Dynamic Mask for Links [ 7: 0]             ev_data_sh_reg(79:64)
--    word 5  : Dynamic Mask for Links [ 15: 8]            ev_data_sh_reg(95:80)
--    word 6  : Dynamic Mask for Links [ 23: 16]           ev_data_sh_reg(111:96)
--    word 7  : Dynamic Mask for Links [ 31: 24]           ev_data_sh_reg(127:112)
--
--  header_ev_id_data(63 DOWNTO 0) <= ev_data_sh_reg (63 DOWNTO 0); --Header ID Info
--
--  ev_id_data1(18 DOWNTO 0) <=   ev_data_sh_reg ( 61 DOWNTO 58)   -- ROD Trigger Type (5:0) take the 4 lsbs 
--                              & ev_data_sh_reg (  4 DOWNTO  0)   -- L1ID(4:0)
--                              & bcid_comp_value(  9 DOWNTO  0);  --& ev_data_sh_reg( 45 DOWNTO 36);    -- BCID
--
--  ev_id_data2(18 DOWNTO 0) <=   ev_data_sh_reg ( 61 DOWNTO 58)  -- ROD Trigger Type (5:0) take the 4 lsbs
--                              & ev_data_sh_reg (  4 DOWNTO  0)  -- L1ID(4:0)
--                             & bcid_comp_value(  9 DOWNTO  0); --& ev_data_sh_reg( 45 DOWNTO  36);   -- BCID
--
--  ev_id_data1(26 DOWNTO 19) <=  ev_data_sh_reg ( 71 DOWNTO 64 )   WHEN mask_sel = "00" ELSE
--                                ev_data_sh_reg ( 87 DOWNTO 80 )   WHEN mask_sel = "01" ELSE
--                                ev_data_sh_reg ( 103 DOWNTO 96 )  WHEN mask_sel = "10" ELSE
--                                ev_data_sh_reg ( 119 DOWNTO 112 ) WHEN mask_sel = "11" ELSE
--                                "00000000" ;
--
--  ev_id_data2(26 DOWNTO 19) <=  ev_data_sh_reg ( 79 DOWNTO 72 )   WHEN mask_sel = "00" ELSE
--                                ev_data_sh_reg ( 95 DOWNTO 88 )   WHEN mask_sel = "01" ELSE
--                                ev_data_sh_reg ( 111 DOWNTO 104 ) WHEN mask_sel = "10" ELSE
--                                ev_data_sh_reg ( 127 DOWNTO 120 ) WHEN mask_sel = "11" ELSE
--                                "00000000" ;
--
--   Important info in old ROD UM p29.
-- 
----------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY ev_data_decode IS
  port (
    clk80                 : in  STD_LOGIC ;
	 clk40                 : in  STD_LOGIC ;
    rst                   : in  STD_LOGIC ;
    ev_data_in            : in  STD_LOGIC_VECTOR(15 DOWNTO 0) ;
    ev_data_wen_n         : in  STD_LOGIC ;
	 fmt_link_enabled		  : in  std_logic_vector( 7 downto 0) ;
    ev_data_almost_full_n : out STD_LOGIC ;
    ev_data_empty         : out STD_LOGIC ;
    header_ev_id_data     : out STD_LOGIC_VECTOR(63 DOWNTO 0) ; -- first 4 words concatenated as header
    header_ev_id_wen      : out STD_LOGIC ;
	 header_ev_id_wen_long : out STD_LOGIC ;
    ev_id_data1           : out STD_LOGIC_VECTOR(34 DOWNTO 0) ;
    ev_id_wen1            : out STD_LOGIC ;
    ev_id_data2           : out STD_LOGIC_VECTOR(34 DOWNTO 0) ;
    ev_id_wen2            : out STD_LOGIC ;      
    ev_fifo1_af           : in  STD_LOGIC ; -- header_ev_id_almost_full
    ev_fifo2_af           : in  STD_LOGIC ; -- ev_id_almost_full1,
    ev_fifo3_af           : in  STD_LOGIC ; -- ev_id_almost_full2,
    bcid_offset           : in  STD_LOGIC_VECTOR( 9 DOWNTO 0) ;  -- this will need to change
    bcid_rollover_select  : in  STD_LOGIC;
	 -- new addition, used for distinguishing dynamic mask bit selection for EFBs on different slave 
	 mask_sel              : in  STD_LOGIC_VECTOR(1 DOWNTO 0);
	 -- HW DEBUG
	 fifocount_out         : out STD_LOGIC_VECTOR( 9 DOWNTO 0)
  );
END ev_data_decode ;

architecture rtl of ev_data_decode is

----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------

  type ev_id_state_typedef IS (
    idle, 
    read,
    check_space, 
    write_data
    );
  signal ev_id_state         : EV_ID_STATE_TYPEDEF ;

  signal ev_data_empty_i     : STD_LOGIC ;
  signal repeat_counter      : UNSIGNED(2 DOWNTO 0) ;
  signal ev_data_sh_reg      : STD_LOGIC_VECTOR(127 DOWNTO 0) ;
  signal ev_data_in_d1       : STD_LOGIC_VECTOR(15 DOWNTO 0) ;
  signal ev_data_out         : STD_LOGIC_VECTOR(15 DOWNTO 0) ;
  signal ev_data_ren         : STD_LOGIC ;
  signal fifocount_out_i     : STD_LOGIC_VECTOR( 9 DOWNTO 0) ;
  signal ev_data_available   : STD_LOGIC ;
  signal ev_data_almost_full : STD_LOGIC ;
  signal ev_data_wen         : STD_LOGIC ;
  signal bcid_value          : STD_LOGIC_VECTOR(13 DOWNTO 0) ; -- changed...double check this
  signal bcid_comp_value     : STD_LOGIC_VECTOR(13 DOWNTO 0) ; -- changed...
  signal bcid_offset_i       : STD_LOGIC_VECTOR(13 DOWNTO 0) ; -- changed...
  
----------------------------------------------------------------------------
--COMPONENT DECLARATION
----------------------------------------------------------------------------
COMPONENT fifo_1024x16
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_data_count : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)
  );
END COMPONENT;

BEGIN  --  Main Body of VHDL code

----------------------------------------------------------------------------
--COMPONENT INSTANTIATION
----------------------------------------------------------------------------

-- HW DEBUG
fifocount_out <= fifocount_out_i;
	
event_fifo_comp : fifo_1024x16
  PORT MAP (
    rst => rst,
    wr_clk => clk40,
    rd_clk => clk80,
    din => ev_data_in_d1,
    wr_en => ev_data_wen,
    rd_en => ev_data_ren,
    dout => ev_data_out,
    full => open,
    empty => ev_data_empty_i,
    wr_data_count => fifocount_out_i
  );			
			
-- modified (completed, check with fifo size)
check_for_event : process (fifocount_out_i) 
begin
  if (fifocount_out_i > "0000000111") then  -- at least one event (8 words) in the FIFO, is it true only 8 words for one event?
    ev_data_available <= '1';
  else
    ev_data_available <= '0';
  end if;
end process check_for_event;

-- modified (completed)
register_data : process (clk40, rst)
begin
  if (rst = '1') then
    ev_data_wen <= '0';
    ev_data_almost_full <= '0';
    ev_data_almost_full_n <= '1';
    ev_data_in_d1 <= (others => '0');
    ev_data_empty <= '1';
  elsif (clk40'event AND clk40 = '1') then
    if (fifocount_out_i > "1110000000") then  -- 112 events at 8 words/event = 896 words/almost full
      ev_data_almost_full <= '1';
    else
      ev_data_almost_full <= '0';
    end if;
    ev_data_almost_full_n <= NOT ev_data_almost_full;
    ev_data_in_d1 <= ev_data_in;
    ev_data_wen <= NOT ev_data_wen_n;
    ev_data_empty <= ev_data_empty_i;
  end if;
end process register_data;

-- modified (in progress, bcid)
read_ev_data : process (clk80, rst)  -- states are (idle, read, check_space, write_data)
begin
  if (rst = '1') then
    ev_id_state <= idle;
    ev_data_ren <= '0';
    header_ev_id_wen <= '0';
	 header_ev_id_wen_long <= '0';
    ev_id_wen1 <= '0';
    ev_id_wen2 <= '0';
    repeat_counter <= (others => '0');
    ev_data_sh_reg <= (others => '0');
    bcid_value <= (others => '1');
  elsif (clk80'event AND clk80 = '1') then
    case ev_id_state is
	 
      when idle =>
        ev_data_ren <= '0';
        header_ev_id_wen <= '0';
		  header_ev_id_wen_long <= '0';
        ev_id_wen1 <= '0';
        ev_id_wen2 <= '0';
        repeat_counter <= (others => '0');
        if (ev_data_available = '1') then
          ev_data_ren   <= '1';
          ev_id_state   <= read;
        end if;
		  
      when read =>
		  -- after all the words are read out through corresponding clock cycle, the sh_reg data 
		  -- would be in the form of word7 & word6 & ... & word 0
        ev_data_sh_reg(111 downto 0) <= ev_data_out & ev_data_sh_reg(111 downto 16);
        if (repeat_counter = 3) then  -- word 2
          bcid_value <= "00" & ev_data_out(15 downto 4);  -- double check offset, zero padding...
        end if;
        if (repeat_counter = 7) then  -- done registering all (but one) the event data words (counter rolls over to repeat)
          ev_id_state <= check_space;
          ev_data_ren <= '0';
			 -- repeat_counter <= (others => '0');  -- I think this hard reset is necessary
        end if;
        repeat_counter <= repeat_counter + 1;
		  
      when check_space =>
		  -- get the last word in the sh_reg
        ev_data_sh_reg(127 downto 0) <= ev_data_out & ev_data_sh_reg(111 downto 0);
        -- when none of the output fifos (to bc_l1_check) is almost full, write data to the output fifos 		  
        if (ev_fifo1_af = '0' AND ev_fifo2_af = '0' AND ev_fifo3_af = '0') then
          ev_id_state <= write_data;
          header_ev_id_wen <= '1';
			 header_ev_id_wen_long <= '1';
			 if(fmt_link_enabled(3 downto 0) = "0000") then
				ev_id_wen1  <= '0';
			 else
				ev_id_wen1  <= '1';
			 end if;
			 if(fmt_link_enabled(7 downto 4) = "0000") then
				ev_id_wen2  <= '0';
			 else
				ev_id_wen2  <= '1';
			 end if;
        end if;
		  
      when write_data =>
        header_ev_id_wen <= '0';
		  header_ev_id_wen_long <= '1'; -- an extra clock cycle of 80 MHz to make a 40 MHz pulse
        ev_id_wen1  <= '0';
        ev_id_wen2  <= '0';
        ev_id_state <= idle;
		  
    end case;
  end if;
end process read_ev_data;

-- modified (completed, but need to check offset calculations)
calc_bcid_comp_value : process (clk80, rst)
begin
  if (rst = '1') then
    bcid_comp_value <= (others => '0');
    bcid_offset_i <= (others => '0');
  elsif (clk80'event AND clk80 = '1') then
    if (bcid_offset(9) = '1') then  -- modified
      bcid_offset_i <= X"F" & bcid_offset;
    else
      bcid_offset_i <= X"0" & bcid_offset;
    end if;
    if (bcid_rollover_select = '1' AND (bcid_value + bcid_offset_i > "00110111101011")) then -- this value needs to change
      bcid_comp_value <= bcid_value + bcid_offset_i - "00110111101100"; -- this value needs to change
    else
      bcid_comp_value <= bcid_value + bcid_offset_i;
    end if;
  end if;
end process calc_bcid_comp_value;

-- see p13 in old ROD user manual
-- Keep original 
header_ev_id_data(63 DOWNTO 0) <= ev_data_sh_reg (63 DOWNTO 0); --Header ID Info, kept original


-- What do we do with BCID? do we use the computed value or registered PPC input value?
-- Use hard coded mask_sel to choose dynamic mask bits in ev_data_sh_reg
ev_id_data1(34 DOWNTO 27) <=  ev_data_sh_reg ( 71 DOWNTO 64 )   WHEN mask_sel = "00" ELSE
                              ev_data_sh_reg ( 87 DOWNTO 80 )   WHEN mask_sel = "01" ELSE
                              ev_data_sh_reg ( 103 DOWNTO 96 )  WHEN mask_sel = "10" ELSE
                              ev_data_sh_reg ( 119 DOWNTO 112 ) WHEN mask_sel = "11" ELSE
                              "00000000" ;

ev_id_data2(34 DOWNTO 27) <=  ev_data_sh_reg ( 79 DOWNTO 72 )   WHEN mask_sel = "00" ELSE
                              ev_data_sh_reg ( 95 DOWNTO 88 )   WHEN mask_sel = "01" ELSE
                              ev_data_sh_reg ( 111 DOWNTO 104 ) WHEN mask_sel = "10" ELSE
                              ev_data_sh_reg ( 127 DOWNTO 120 ) WHEN mask_sel = "11" ELSE
                              "00000000" ;

ev_id_data1(26 DOWNTO 0) <=   ev_data_sh_reg ( 61 DOWNTO 58)   -- ROD Trigger Type (5:0) take the 4 lsbs 
                            & ev_data_sh_reg ( 12 DOWNTO  0)   -- L1ID(12:0)  13 Bits
                            & bcid_comp_value(  9 DOWNTO  0);  -- BCID(9:0)

ev_id_data2(26 DOWNTO 0) <=   ev_data_sh_reg ( 61 DOWNTO 58)  -- ROD Trigger Type (5:0) take the 4 lsbs
                            & ev_data_sh_reg ( 12 DOWNTO  0)  -- L1ID(12:0)  13 Bits
                            & bcid_comp_value(  9 DOWNTO  0); -- BCID(9:0)

END rtl;
