----------------------------------------------------------------------------
-- LBL - WISCONSIN
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    Event Fragment Builder: efb_top.vhd
-- Description:
--    ROD Dual Gatherer
--    IBL Version
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen
--
-- Notes:

-- To do list:
    
------------------------------------------------------------------------------


library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY efb_top IS
  generic (use_bram: boolean := true) ; 
  port (
	 clk80                  : in  STD_LOGIC;
 	 clk40                  : in  STD_LOGIC;
    rst                    : in  STD_LOGIC;
    efb_id                 : in  STD_LOGIC_VECTOR(1 DOWNTO 0); --- new addition to bc_l1_check and ev_data_decode
	 
-- Formatter FIFO Controller Signals
    data_in1               : in  STD_LOGIC_VECTOR(34 DOWNTO 0);
    data_in2               : in  STD_LOGIC_VECTOR(34 DOWNTO 0);
	 fmt_link_enabled	      : in  STD_LOGIC_VECTOR( 7 DOWNTO 0);
	 fmt_data_valid_in1     : in  STD_LOGIC;
	 fmt_data_valid_in2     : in  STD_LOGIC;
    fmt_beg_of_event1      : in  STD_LOGIC;      
    fmt_beg_of_event2      : in  STD_LOGIC;
    fmt_end_of_event1      : in  STD_LOGIC;      
    fmt_end_of_event2      : in  STD_LOGIC;
    fifo1_pause            : out STD_LOGIC;--- probably not needed
    fifo2_pause            : out STD_LOGIC;--- probably not needed
    ev_data_valid          : out STD_LOGIC;
 
-- Register Interface Signals
    ---- Interfacing with the efb err_detect_new blocks----
    err_mask_wen1            : in  STD_LOGIC;
    err_mask_din1            : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_dout1           : out STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_sel1            : in  STD_LOGIC_VECTOR( 2 DOWNTO 0);      
    err_mask_wen2            : in  STD_LOGIC;
    err_mask_din2            : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_dout2           : out STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_sel2            : in  STD_LOGIC_VECTOR( 2 DOWNTO 0);
	 ---- Information going into efb gen_fragment block----
    source_id                : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    format_version           : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    run_number               : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    hitdisccnfg              : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
--    send_empty_events        : out STD_LOGIC;
    ---- output evtmem fifos signals----
    out_fifo_rst             : in  STD_LOGIC;	 
    out_fifo1_empty_n        : out STD_LOGIC;
    out_fifo1_aempty_n       : out STD_LOGIC;
    out_fifo1_full_n         : out STD_LOGIC;
    out_fifo1_afull_n        : out STD_LOGIC;
    out_fifo1_play_done      : out STD_LOGIC; -- out_mem1_done  -- not used right now...
    out_fifo2_empty_n        : out STD_LOGIC;
    out_fifo2_aempty_n       : out STD_LOGIC;
    out_fifo2_full_n         : out STD_LOGIC;
    out_fifo2_afull_n        : out STD_LOGIC;
    out_fifo2_play_done      : out STD_LOGIC;
    efb_misc_stts_reg        : out STD_LOGIC_VECTOR(30 DOWNTO 0);
    ev_header_data           : out STD_LOGIC_VECTOR(15 DOWNTO 0);
    ev_fifo_data1            : out STD_LOGIC_VECTOR(34 DOWNTO 0);-- decoded event id data
    ev_fifo_data2            : out STD_LOGIC_VECTOR(34 DOWNTO 0);
    mask_bcid_error          : in  STD_LOGIC;
    mask_l1id_error          : in  STD_LOGIC;
    bcid_offset              : in  STD_LOGIC_VECTOR( 9 DOWNTO 0);
    grp_evt_enable           : in  STD_LOGIC;
    event_count              : out STD_LOGIC_VECTOR(31 downto 0);
--    bandwidth_status_in      : out STD_LOGIC_VECTOR(31 downto 0);
--    spare_pins_in            : out STD_LOGIC_VECTOR(23 DOWNTO 0);
--    dll_locked_in            : out STD_LOGIC;
    mask_boc_clock_err       : in  STD_LOGIC;
    mask_tim_clock_err       : in  STD_LOGIC;
--    sweeper_event_in         : out STD_LOGIC;
--    sync_event_error_in      : out STD_LOGIC;
 
    l1id_fifo_data           : out STD_LOGIC_VECTOR(39 downto 0);
    l1id_fifo_addr           : in  STD_LOGIC_VECTOR( 3 downto 0);
    enable_l1_trap           : in  STD_LOGIC;
    l1_trap_full_in          : out STD_LOGIC;
    latched_l1id0            : out STD_LOGIC_VECTOR(12 DOWNTO 0);
    latched_l1id1            : out STD_LOGIC_VECTOR(12 DOWNTO 0);
    latched_bcid0            : out STD_LOGIC_VECTOR(9  DOWNTO 0);
    latched_bcid1            : out STD_LOGIC_VECTOR(9  DOWNTO 0);
    tim_bcid_in_rol          : in  STD_LOGIC;
    bcid_rollover_select     : in  STD_LOGIC;
	 
-- Event Data L1/BCID/Trig Type Data
    ev_data_from_ppc       : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
    ev_data_wen_n          : in  STD_LOGIC;
    ev_data_rst_n          : in  STD_LOGIC;-- jmj 20000626
    ev_data_almost_full_n  : out STD_LOGIC;
    ev_data_ready          : out STD_LOGIC; -- ev_data_empty, ready for more
    ev_id_fifo_empty_error : out STD_LOGIC;
    gatherer_halt_output   : in  STD_LOGIC; -- need to have some logic involving histo_rfd and slink  -- or completely speerate signal
	 -- HW DEBUG (event header word FIFO count)
	 fifocount_out          : out STD_LOGIC_VECTOR( 9 DOWNTO 0);
-- Output to Router (Slink and Histogrammer)
	 k_word						: out std_logic;
    data_valid_out         : out STD_LOGIC;
    out_data_to_router     : out STD_LOGIC_VECTOR(31 DOWNTO 0);
	 data_chip_id           : out STD_LOGIC_VECTOR( 2 DOWNTO 0) -- for the histogrammer
	 
    );
END efb_top ;

architecture rtl of efb_top is

----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------

  signal halt_output                 : STD_LOGIC;
  signal fifo1_pause_i               : STD_LOGIC;
  signal fifo2_pause_i               : STD_LOGIC;
  
  signal ev_data_almost_full_n_i_out : STD_LOGIC;-- >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

  signal ev_id_data_in1              : STD_LOGIC_VECTOR(34 DOWNTO 0);
  signal ev_id_data_out1             : STD_LOGIC_VECTOR(34 DOWNTO 0); 
  signal ev_id_ren1                  : STD_LOGIC;
  signal ev_id_wen1                  : STD_LOGIC;
  signal ev_id_full1                 : STD_LOGIC;
  signal ev_id_empty1                : STD_LOGIC;
  signal ev_id_fifo_empty_error1     : STD_LOGIC;
  signal ev_id_almost_full1          : STD_LOGIC;



  signal ev_data_almost_full_n_i     : STD_LOGIC;

  signal ev_data_empty_i             : STD_LOGIC; -- careful with this one, might have a bug

  signal ev_id_data_in2              : STD_LOGIC_VECTOR(34 DOWNTO 0);   
  signal ev_id_data_out2             : STD_LOGIC_VECTOR(34 DOWNTO 0); 
  signal ev_id_ren2                  : STD_LOGIC;
  signal ev_id_wen2                  : STD_LOGIC;
  signal ev_id_full2                 : STD_LOGIC;
  signal ev_id_empty2                : STD_LOGIC;
  signal ev_id_fifo_empty_error2     : STD_LOGIC;
  signal ev_id_almost_full2          : STD_LOGIC;

  signal header_ev_id_data_in        : STD_LOGIC_VECTOR(63 DOWNTO 0); 
  signal header_ev_id_data_out       : STD_LOGIC_VECTOR(63 DOWNTO 0); 
  signal header_ev_id_ren            : STD_LOGIC;
  signal header_ev_id_wen            : STD_LOGIC;
  signal header_ev_id_wen_long_i     : STD_LOGIC;
  signal header_ev_id_full           : STD_LOGIC;
  signal header_ev_id_empty          : STD_LOGIC;
  signal header_ev_id_almost_full    : STD_LOGIC;
  signal header_ev_id_data_stts_reg  : STD_LOGIC_VECTOR(15 DOWNTO 0); 

  signal id_check1_data_out          : STD_LOGIC_VECTOR(42 DOWNTO 0); 
  signal id_check1_data_valid_out    : STD_LOGIC;
    
  signal id_check2_data_out          : STD_LOGIC_VECTOR(42 DOWNTO 0); 
  signal id_check2_data_valid_out    : STD_LOGIC;


  signal err_det1_data_out           : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal err_det1_data_valid_out     : STD_LOGIC;

  signal err_sumry_fifo1_wen         : STD_LOGIC;
  signal err_sumry_fifo1_data_in     : STD_LOGIC_VECTOR(47 DOWNTO 0);
  signal err_sumry_fifo1_data_out    : STD_LOGIC_VECTOR(47 DOWNTO 0);
  signal err_sumry_fifo1_ren         : STD_LOGIC;
  signal err_sumry_fifo1_full        : STD_LOGIC;
  signal err_sumry_fifo1_empty       : STD_LOGIC;
  signal err_sumry_fifo1_almost_full : STD_LOGIC;
   
          
  signal err_det2_data_out           : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal err_det2_data_valid_out     : STD_LOGIC;   
  signal err_sumry_fifo2_wen         : STD_LOGIC;
  signal err_sumry_fifo2_data_in     : STD_LOGIC_VECTOR(47 DOWNTO 0);
  signal err_sumry_fifo2_data_out    : STD_LOGIC_VECTOR(47 DOWNTO 0);
  signal err_sumry_fifo2_ren         : STD_LOGIC;
  signal err_sumry_fifo2_full        : STD_LOGIC;
  signal err_sumry_fifo2_empty       : STD_LOGIC;
  signal err_sumry_fifo2_almost_full : STD_LOGIC;

  signal count_fifo1_data_in         : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal count_fifo1_data_out        : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal count_fifo1_ren             : STD_LOGIC;
  signal count_fifo1_wen             : STD_LOGIC;
  signal count_fifo1_full            : STD_LOGIC;
  signal count_fifo1_empty           : STD_LOGIC;
  signal count_fifo1_almost_full     : STD_LOGIC;
  
  signal count_fifo2_data_in         : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal count_fifo2_data_out        : STD_LOGIC_VECTOR(15 DOWNTO 0);
  signal count_fifo2_ren             : STD_LOGIC;
  signal count_fifo2_wen             : STD_LOGIC;
  signal count_fifo2_full            : STD_LOGIC;
  signal count_fifo2_empty           : STD_LOGIC;
  signal count_fifo2_almost_full     : STD_LOGIC;

  signal out_mem1_play               : STD_LOGIC;
  signal out_mem1_num_words          : STD_LOGIC_VECTOR(13 DOWNTO 0);
  signal out_mem1_done               : STD_LOGIC;
  signal out_mem1_data_valid         : STD_LOGIC;
  signal out_mem2_play               : STD_LOGIC;
  signal out_mem2_num_words          : STD_LOGIC_VECTOR(13 DOWNTO 0);
  signal out_mem2_done               : STD_LOGIC;
  signal out_mem2_data_valid         : STD_LOGIC;
  signal ev_header_data_valid_out    : STD_LOGIC;
  signal ev_header_data_valid_out_i  : STD_LOGIC;
  signal ev_header_data_valid_out_d1 : STD_LOGIC;

  signal out_mem1_play_i             : STD_LOGIC;
  signal out_mem1_num_words_i        : STD_LOGIC_VECTOR(13 DOWNTO 0);
  signal out_mem2_play_i             : STD_LOGIC;
  signal out_mem2_num_words_i        : STD_LOGIC_VECTOR(13 DOWNTO 0);

  signal out_fifo1_oe_n_p1           : STD_LOGIC;
  signal out_fifo2_oe_n_p1           : STD_LOGIC;  


--  signal efb_flush_data              : STD_LOGIC ;
--  signal send_empty_events           : STD_LOGIC ; 
  signal rst_or_flush                : STD_LOGIC; 


  signal efb_bus_data_oe_n           : STD_LOGIC;
  signal ev_data_flush               : STD_LOGIC;
--  signal ev_data_empty_o             : STD_LOGIC ;


-------- internal signals for 4K word output fifos --------
  signal out_fifo1_rst               : STD_LOGIC;
  signal out_fifo2_rst               : STD_LOGIC;

  signal out_fifo1_ren               : STD_LOGIC;
  signal out_fifo2_ren               : STD_LOGIC;

  signal out_fifo1_wen               : STD_LOGIC;
  signal out_fifo2_wen               : STD_LOGIC;

  signal out_fifo1_oe_n              : STD_LOGIC;
  signal out_fifo2_oe_n              : STD_LOGIC;

  signal out_fifo1_data              : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal out_fifo2_data              : STD_LOGIC_VECTOR(41 DOWNTO 0);

  signal out_fifo1_empty             : STD_LOGIC;
  signal out_fifo1_aempty            : STD_LOGIC;
  signal out_fifo1_full              : STD_LOGIC;
  signal out_fifo1_afull             : STD_LOGIC;
  signal out_fifo2_empty             : STD_LOGIC;
  signal out_fifo2_aempty            : STD_LOGIC;
  signal out_fifo2_full              : STD_LOGIC;
  signal out_fifo2_afull             : STD_LOGIC;


  signal out_fifo1_data_to_router    : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal out_fifo2_data_to_router    : STD_LOGIC_VECTOR(41 DOWNTO 0);

  signal out_fifo1_data_i            : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal out_fifo1_wen_n_i           : STD_LOGIC;
  signal out_fifo1_ren_n_i           : STD_LOGIC;
  signal out_fifo2_data_i            : STD_LOGIC_VECTOR(41 DOWNTO 0);
  signal out_fifo2_wen_n_i           : STD_LOGIC;
  signal out_fifo2_ren_n_i           : STD_LOGIC;


  signal ev_header_data_i            : STD_LOGIC_VECTOR(31 DOWNTO 0);

  
  signal efb_misc_stts_reg_i         : STD_LOGIC_VECTOR(30 DOWNTO 0);

--  signal efb_ready_i        : STD_LOGIC ;

  signal link_in_mux_i               : STD_LOGIC_VECTOR(3 downto 0);  

  signal event_count_i               : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal grp_evt_count1              : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal grp_evt_count2              : STD_LOGIC_VECTOR(31 DOWNTO 0);

--  signal sweeper_event_i      : STD_LOGIC;
  signal hold_next_event_i           : STD_LOGIC;

  signal l1id_diag_data              : STD_LOGIC_VECTOR(39 downto 0);
--  signal enable_l1_trap : STD_LOGIC;
  signal l1_trap_full1               : STD_LOGIC;
  signal l1_trap_full2               : STD_LOGIC;
  signal l1_trap_full                : STD_LOGIC;
  
  
  signal data_valid_out_i				 : STD_LOGIC;
  signal kword_to_router_i				 : STD_LOGIC;
  signal data_chip_id_i					 : STD_LOGIC_VECTOR( 2 downto 0);
  signal out_data_to_router_i			 : STD_LOGIC_VECTOR(31 downto 0);
  
  signal ev_header_kword_out_i		 : STD_LOGIC;
  
--------------------------------------------------------------------------
-- XILINX PARTS SPECIFIC COMPONENTS/SIGNALS
--------------------------------------------------------------------------

COMPONENT fifo_1024x35 -- BRAM FIFO
  PORT (
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(34 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(34 DOWNTO 0);
    full        : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT fifo_256x64
  PORT (
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(63 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(63 DOWNTO 0);
    full        : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    data_count  : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
  );
END COMPONENT;


COMPONENT fifo_256x48 -- distributed FIFO
  PORT (
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(47 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(47 DOWNTO 0);
    full        : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC
    );
END COMPONENT;

COMPONENT fifo_256x16 -- distributed FIFO
  PORT (
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full        : OUT STD_LOGIC;
    almost_full : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC
  );
END COMPONENT;

component bc_l1_check ------ modified
  port (
    clk                 : in  STD_LOGIC;
    rst                 : in  STD_LOGIC;
    data_in             : in  STD_LOGIC_VECTOR(34 DOWNTO 0);
	 data_valid_in       : in  STD_LOGIC;

    fmt_beg_of_event    : in  STD_LOGIC;
    fmt_end_of_event    : in  STD_LOGIC;	 
    data_out            : out STD_LOGIC_VECTOR(42 DOWNTO 0);
    data_valid_out      : out STD_LOGIC;
    ev_fifo_ren         : out STD_LOGIC;
    ev_fifo_empty       : in  STD_LOGIC;
    ev_fifo_empty_error : out STD_LOGIC;
    ev_fifo_data        : in  STD_LOGIC_VECTOR(34 DOWNTO 0);
	 efb_engine_id       : in  STD_LOGIC_VECTOR(1 DOWNTO 0);  -- check the component assignment
    gatherer_id         : in  STD_LOGIC;
    bcid_offset         : in  STD_LOGIC_VECTOR( 9 DOWNTO 0);
    hitdisccnfg         : in  STD_LOGIC_VECTOR( 7 DOWNTO 0);
    grp_evt_count_out   : out STD_LOGIC_VECTOR(31 downto 0);
    grp_evt_enable_in   : in  STD_LOGIC;
    l1_id_fifo_data     : out STD_LOGIC_VECTOR(19 downto 0);
    l1_id_fifo_addr     : in  STD_LOGIC_VECTOR( 3 DOWNTO 0);
    enable_l1_trap_in   : in  STD_LOGIC;
    l1_trap_full_out    : out STD_LOGIC;
    latched_l1id        : out STD_LOGIC_VECTOR(12 DOWNTO 0);
    latched_bcid        : out STD_LOGIC_VECTOR( 9 DOWNTO 0)
    );
END component;

component ev_data_decode ------ modified
  port (
    clk80                 : in  STD_LOGIC;
    clk40                 : in  STD_LOGIC;
    rst                   : in  STD_LOGIC;
    ev_data_in            : in  STD_LOGIC_VECTOR(15 DOWNTO 0);
    ev_data_wen_n         : in  STD_LOGIC;
	 fmt_link_enabled		  : in  std_logic_vector( 7 downto 0);
    ev_data_almost_full_n : out STD_LOGIC;
    ev_data_empty         : out STD_LOGIC;
    header_ev_id_data     : out STD_LOGIC_VECTOR(63 DOWNTO 0);
    header_ev_id_wen      : out STD_LOGIC;
	 header_ev_id_wen_long : out STD_LOGIC;
    ev_id_data1           : out STD_LOGIC_VECTOR(34 DOWNTO 0);
    ev_id_wen1            : out STD_LOGIC;
    ev_id_data2           : out STD_LOGIC_VECTOR(34 DOWNTO 0);
    ev_id_wen2            : out STD_LOGIC;      
    ev_fifo1_af           : in  STD_LOGIC;
    ev_fifo2_af           : in  STD_LOGIC;
    ev_fifo3_af           : in  STD_LOGIC;
    bcid_offset           : in  STD_LOGIC_VECTOR( 9 DOWNTO 0);
    bcid_rollover_select  : in  STD_LOGIC;
	 mask_sel              : in  STD_LOGIC_VECTOR( 1 DOWNTO 0);
	 fifocount_out         : out STD_LOGIC_VECTOR( 9 DOWNTO 0)
    );
END component ;

component err_detect_new      -----modified
  port (
    clk40, clk80        : in  STD_LOGIC;
    rst                 : in  STD_LOGIC;
    data_in             : in  STD_LOGIC_VECTOR(42 DOWNTO 0);
    data_valid_in       : in  STD_LOGIC;
    data_out            : out STD_LOGIC_VECTOR(41 DOWNTO 0);
    data_valid_out      : out STD_LOGIC;
    err_sumry_fifo_wen  : out STD_LOGIC;
    err_sumry_fifo_data : out STD_LOGIC_VECTOR(47 DOWNTO 0);
    err_mask_wen        : in  STD_LOGIC;
    err_mask_din        : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_dout       : out STD_LOGIC_VECTOR(31 DOWNTO 0);
    err_mask_sel        : in  STD_LOGIC_VECTOR( 2 DOWNTO 0);
    mask_bcid_error     : in  STD_LOGIC;
    mask_l1id_error     : in  STD_LOGIC 

    );
END component;

component format_data    ----- modified
  port (
    clk                 : in  STD_LOGIC;
    rst                 : in  STD_LOGIC;
    data_in             : in  STD_LOGIC_VECTOR(41 DOWNTO 0);
    data_valid_in       : in  STD_LOGIC;
    data_out            : out STD_LOGIC_VECTOR(41 DOWNTO 0);
    data_valid_out_n    : out STD_LOGIC;
    count_fifo_wen      : out STD_LOGIC;
    count_fifo_data     : out STD_LOGIC_VECTOR(13 DOWNTO 0)

    );
END component;

component gen_fragment   ---- no modification
  port (
    clk                   : in  STD_LOGIC;
    rst                   : in  STD_LOGIC;
    efb_engine_id         : in  STD_LOGIC_VECTOR(1 DOWNTO 0);
--    efb_flush_data        : out STD_LOGIC ; -- SPC
--    send_empty_events     : in  STD_LOGIC ;
    header_ev_id_empty    : in  STD_LOGIC;
    header_ev_id_data     : in  STD_LOGIC_VECTOR(63 DOWNTO 0);
    header_ev_id_ren      : out STD_LOGIC;
    err_sumry_fifo1_empty : in  STD_LOGIC;
    err_sumry_fifo1_data  : in  STD_LOGIC_VECTOR(47 DOWNTO 0);
    err_sumry_fifo1_ren   : out STD_LOGIC;
    err_sumry_fifo2_empty : in  STD_LOGIC;
    err_sumry_fifo2_data  : in  STD_LOGIC_VECTOR(47 DOWNTO 0);
    err_sumry_fifo2_ren   : out STD_LOGIC;
    count_fifo1_empty     : in  STD_LOGIC;
    count_fifo1_data      : in  STD_LOGIC_VECTOR(13 DOWNTO 0);
    count_fifo1_ren       : out STD_LOGIC;
    count_fifo2_empty     : in  STD_LOGIC;
    count_fifo2_data      : in  STD_LOGIC_VECTOR(13 DOWNTO 0);
    count_fifo2_ren       : out STD_LOGIC;
    halt_output           : in  STD_LOGIC;
    out_mem1_play         : out STD_LOGIC;
    out_mem1_num_words    : out STD_LOGIC_VECTOR(13 DOWNTO 0);
    out_mem1_done         : in  STD_LOGIC;
    out_mem2_play         : out STD_LOGIC;
    out_mem2_num_words    : out STD_LOGIC_VECTOR(13 DOWNTO 0);
    out_mem2_done         : in  STD_LOGIC;
    ev_header_data_out    : out STD_LOGIC_VECTOR(31 DOWNTO 0);
    ev_header_dv_out_p1   : out STD_LOGIC;
	 ev_header_kword_out   : out STD_LOGIC;
    source_id             : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    format_version        : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
    run_number            : in  STD_LOGIC_VECTOR(31 DOWNTO 0);
	 fmt_link_enabled		  : in  STD_LOGIC_VECTOR( 7 downto 0);
    mask_boc_err_in       : in  STD_LOGIC;
    mask_tim_err_in       : in  STD_LOGIC;
--    sweeper_event_out     : out STD_LOGIC ;
    hold_next_event_in    : in  STD_LOGIC;
    tim_bcid_in_rol_in    : in  STD_LOGIC 
    );
END component ;

-- actually having a depth of 1K words
COMPONENT bram_fifo_4kwords
  PORT (
    clk          : IN STD_LOGIC;
    rst          : IN STD_LOGIC;
    din          : IN STD_LOGIC_VECTOR(41 DOWNTO 0);
    wr_en        : IN STD_LOGIC;
    rd_en        : IN STD_LOGIC;
    dout         : OUT STD_LOGIC_VECTOR(41 DOWNTO 0);
    full         : OUT STD_LOGIC;
    almost_full  : OUT STD_LOGIC;
    empty        : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT dist_fifo_4kwords
  PORT (
    clk          : IN STD_LOGIC;
    rst          : IN STD_LOGIC;
    din          : IN STD_LOGIC_VECTOR(41 DOWNTO 0);
    wr_en        : IN STD_LOGIC;
    rd_en        : IN STD_LOGIC;
    dout         : OUT STD_LOGIC_VECTOR(41 DOWNTO 0);
    full         : OUT STD_LOGIC;
    almost_full  : OUT STD_LOGIC;
    empty        : OUT STD_LOGIC;
    almost_empty : OUT STD_LOGIC
  );
END COMPONENT;

component outmem -------- no modification
  port (
    clk                   : in  STD_LOGIC;
    rst                   : in  STD_LOGIC;
    xoff                  : in  STD_LOGIC;
    out_mem_play          : in  STD_LOGIC;
    out_mem_num_words     : in  STD_LOGIC_VECTOR(13 DOWNTO 0);
    out_mem_done          : out STD_LOGIC;
    out_mem_data_valid_p1 : out STD_LOGIC;
    out_fifo_ren_n        : out STD_LOGIC;
    out_fifo_oe_n_p1      : out STD_LOGIC 
    );  
END component;

-- HW DEBUG
SIGNAL write_header_data_count_i : STD_LOGIC_VECTOR(7 downto 0);

BEGIN  --  Main Body of VHDL code

----------------------------------------------------------------------------
--COENT INSTANTIATION
------------------------------------------------------------------------

--decoev_id1 : fifo_256x27 --No, data size is 35 now
--  PORT MAP (
--    clk => clk80,
--    rst => rst_or_flush,
--    din => ev_id_data_in1, -- should be renamed as decoded_ev_id_data1
--    wr_en => ev_id_wen1, -- decoded_ev_id_wen1
--    rd_en => ev_id_ren1, -- decoded_ev_id_ren1
--    dout => ev_id_data_out1, -- decoded_ev_id_data_out1
--    full => ev_id_full1, -- decoded_ev_id_full1
--    almost_full => ev_id_almost_full1, -- decoded_ev_id_almost_full1
--    empty => ev_id_empty1); -- decoded_ev_id_empty1
--			
--decoded_ev_id2 : fifo_256x27
--  PORT MAP (
--    clk => clk80,
--    rst => rst_or_flush,
--    din => ev_id_data_in2, -- should be renamed as decoded_ev_id_data2
--    wr_en => ev_id_wen2, -- decoded_ev_id_wen2
--    rd_en => ev_id_ren2, -- decoded_ev_id_ren2
--    dout => ev_id_data_out2, -- decoded_ev_id_data_out2
--    full => ev_id_full2, -- decoded_ev_id_full2
--    almost_full => ev_id_almost_full2, -- decoded_ev_id_almost_full2
--    empty => ev_id_empty2); -- decoded_ev_id_empty2

decoded_ev_id1 : fifo_1024x35 
  PORT MAP (
    clk => clk80,
    rst => rst_or_flush,
    din => ev_id_data_in1, -- should be renamed as decoded_ev_id_data1
    wr_en => ev_id_wen1, -- decoded_ev_id_wen1
    rd_en => ev_id_ren1, -- decoded_ev_id_ren1
    dout => ev_id_data_out1, -- decoded_ev_id_data_out1
    full => ev_id_full1, -- decoded_ev_id_full1
    almost_full => ev_id_almost_full1, -- decoded_ev_id_almost_full1
    empty => ev_id_empty1); -- decoded_ev_id_empty1
			
decoded_ev_id2 : fifo_1024x35
  PORT MAP (
    clk => clk80,
    rst => rst_or_flush,
    din => ev_id_data_in2, -- should be renamed as decoded_ev_id_data2
    wr_en => ev_id_wen2, -- decoded_ev_id_wen2
    rd_en => ev_id_ren2, -- decoded_ev_id_ren2
    dout => ev_id_data_out2, -- decoded_ev_id_data_out2
    full => ev_id_full2, -- decoded_ev_id_full2
    almost_full => ev_id_almost_full2, -- decoded_ev_id_almost_full2
    empty => ev_id_empty2); -- decoded_ev_id_empty2

--header_ev_id : fifo_256x64
--  PORT MAP (
--    clk => clk80,
--	 rst => rst,
--	 din => header_ev_id_data_in,
--	 wr_en => header_ev_id_wen,
--	 rd_en => header_ev_id_ren,
--	 dout => header_ev_id_data_out,
--	 full => header_ev_id_full,
--	 almost_full => header_ev_id_almost_full,
--	 empty => header_ev_id_empty);

header_ev_id : fifo_256x64
  PORT MAP (
    clk => clk80,
	 rst => rst_or_flush,
	 din => header_ev_id_data_in,
	 wr_en => header_ev_id_wen,
	 rd_en => header_ev_id_ren,
	 dout => header_ev_id_data_out,
	 full => header_ev_id_full,
	 almost_full => header_ev_id_almost_full,
	 empty => header_ev_id_empty,
	 data_count => write_header_data_count_i -- HW DEBUG
  );
	 
-- HW DEBUG
fifocount_out <= "00" & write_header_data_count_i;	 
	 
event_data_decode: ev_data_decode
  port map (
    clk80                 => clk80,
    clk40                 => clk40,
	 rst                   => rst_or_flush, -- SPC 03/23/2014-- ev_data_flush,
    ev_data_in            => ev_data_from_ppc,---
    ev_data_wen_n         => ev_data_wen_n,
	 fmt_link_enabled	     => fmt_link_enabled,
    ev_data_almost_full_n => ev_data_almost_full_n_i, --output
    ev_data_empty         => ev_data_empty_i,
    header_ev_id_data     => header_ev_id_data_in,
    header_ev_id_wen      => header_ev_id_wen,
	 header_ev_id_wen_long => header_ev_id_wen_long_i,
    ev_id_data1           => ev_id_data_in1,
    ev_id_wen1            => ev_id_wen1,
    ev_id_data2           => ev_id_data_in2,
    ev_id_wen2            => ev_id_wen2,      
    ev_fifo1_af           => header_ev_id_almost_full,
    ev_fifo2_af           => ev_id_almost_full1,
    ev_fifo3_af           => ev_id_almost_full2,
    bcid_offset           => bcid_offset,
    bcid_rollover_select  => bcid_rollover_select,
	 mask_sel              => efb_id,
	 fifocount_out         => open--fifocount_out
    );

bc_l1_check1: bc_l1_check  -- need to put in some kind of efb_engine_id see notes in notebook
  port map (
    clk                 => clk80,---
    rst                 => rst_or_flush,
    data_in             => data_in1,---
    data_valid_in       => fmt_data_valid_in1,
    fmt_beg_of_event    => fmt_beg_of_event1,
    fmt_end_of_event    => fmt_end_of_event1,	 
    data_out            => id_check1_data_out,
    data_valid_out      => id_check1_data_valid_out,
    ev_fifo_ren         => ev_id_ren1,
    ev_fifo_empty       => ev_id_empty1,
    ev_fifo_empty_error => ev_id_fifo_empty_error1,
    ev_fifo_data        => ev_id_data_out1,
	 efb_engine_id       => efb_id,
    gatherer_id         => '0',
    bcid_offset         => bcid_offset,
    hitdisccnfg         => hitdisccnfg(7 downto 0),
    grp_evt_count_out   => grp_evt_count1,
    grp_evt_enable_in   => grp_evt_enable,
    l1_id_fifo_data     => l1id_diag_data(19 downto 0),
    l1_id_fifo_addr     => l1id_fifo_addr,
    enable_l1_trap_in   => enable_l1_trap,
    l1_trap_full_out    => l1_trap_full1,
    latched_l1id        => latched_l1id0,
    latched_bcid        => latched_bcid0
    );

bc_l1_check2: bc_l1_check
  port map (
    clk                 => clk80,---
    rst                 => rst_or_flush,
    data_in             => data_in2,---
    data_valid_in       => fmt_data_valid_in2,---
    fmt_beg_of_event    => fmt_beg_of_event2,
    fmt_end_of_event    => fmt_end_of_event2,
    data_out            => id_check2_data_out,
    data_valid_out      => id_check2_data_valid_out,
    ev_fifo_ren         => ev_id_ren2,
    ev_fifo_empty       => ev_id_empty2,
    ev_fifo_empty_error => ev_id_fifo_empty_error2,
    ev_fifo_data        => ev_id_data_out2,
	 efb_engine_id       => efb_id,
    gatherer_id         => '1',
    bcid_offset         => bcid_offset,
    hitdisccnfg         => hitdisccnfg(15 downto 8), 
    grp_evt_count_out   => grp_evt_count2,
    grp_evt_enable_in   => grp_evt_enable,
    l1_id_fifo_data     => l1id_diag_data(39 downto 20),
    l1_id_fifo_addr     => l1id_fifo_addr,
    enable_l1_trap_in   => enable_l1_trap,
    l1_trap_full_out    => l1_trap_full2,
    latched_l1id        => latched_l1id1,
    latched_bcid        => latched_bcid1
    );

l1_trap_full <= l1_trap_full1 AND l1_trap_full2;

err_sumry_word_fifo1 : fifo_256x48
  PORT MAP (
	 clk => clk80,
	 rst => rst_or_flush,
	 din => err_sumry_fifo1_data_in,
 	 wr_en => err_sumry_fifo1_wen,
	 rd_en => err_sumry_fifo1_ren,
	 dout => err_sumry_fifo1_data_out,
	 full => err_sumry_fifo1_full,
	 almost_full => err_sumry_fifo1_almost_full,
	 empty => err_sumry_fifo1_empty);

err_sumry_word_fifo2 : fifo_256x48
  PORT MAP (
	 clk => clk80,
	 rst => rst_or_flush,
	 din => err_sumry_fifo2_data_in,
 	 wr_en => err_sumry_fifo2_wen,
	 rd_en => err_sumry_fifo2_ren,
	 dout => err_sumry_fifo2_data_out,
	 full => err_sumry_fifo2_full,
	 almost_full => err_sumry_fifo2_almost_full,
	 empty => err_sumry_fifo2_empty);

err_detect1: err_detect_new
  port map(
    clk40               => clk40,
	 clk80					=> clk80,
    rst                 => rst_or_flush,
    data_in             => id_check1_data_out,
    data_valid_in       => id_check1_data_valid_out,
    data_out            => err_det1_data_out,
    data_valid_out      => err_det1_data_valid_out,
    err_sumry_fifo_wen  => err_sumry_fifo1_wen,
    err_sumry_fifo_data => err_sumry_fifo1_data_in(47 downto 0),
    err_mask_wen        => err_mask_wen1,
    err_mask_din        => err_mask_din1,
    err_mask_dout       => err_mask_dout1,
    err_mask_sel        => err_mask_sel1,
    mask_bcid_error     => mask_bcid_error,
    mask_l1id_error     => mask_l1id_error
    );

err_detect2: err_detect_new
  port map( 
    clk40               => clk40,
	 clk80					=> clk80,
    rst                 => rst_or_flush,
    data_in             => id_check2_data_out,
    data_valid_in       => id_check2_data_valid_out,
    data_out            => err_det2_data_out,
    data_valid_out      => err_det2_data_valid_out,
    err_sumry_fifo_wen  => err_sumry_fifo2_wen,
    err_sumry_fifo_data => err_sumry_fifo2_data_in(47 downto 0),
    err_mask_wen        => err_mask_wen2,
    err_mask_din        => err_mask_din2,
    err_mask_dout       => err_mask_dout2,
    err_mask_sel        => err_mask_sel2,
    mask_bcid_error     => mask_bcid_error,
    mask_l1id_error     => mask_l1id_error
    );

format_data1 : format_data
  port map (
    clk              => clk80,
    rst              => rst_or_flush,
    data_in          => err_det1_data_out,
    data_valid_in    => err_det1_data_valid_out,
    data_out         => out_fifo1_data_i,
    data_valid_out_n => out_fifo1_wen_n_i,     
    count_fifo_wen   => count_fifo1_wen,
    count_fifo_data  => count_fifo1_data_in(13 downto 0)
    );

format_data2 : format_data
  port map (
    clk              => clk80,
    rst              => rst_or_flush,
    data_in          => err_det2_data_out,
    data_valid_in    => err_det2_data_valid_out,
    data_out         => out_fifo2_data_i,
    data_valid_out_n => out_fifo2_wen_n_i,
    count_fifo_wen   => count_fifo2_wen,
    count_fifo_data  => count_fifo2_data_in(13 downto 0)
    );

ev_out_word_count_fifo1 : fifo_256x16
  PORT MAP (
	 clk => clk80,
	 rst => rst_or_flush,
	 din => count_fifo1_data_in,
    wr_en => count_fifo1_wen,
    rd_en => count_fifo1_ren,
	 dout => count_fifo1_data_out,
	 full => count_fifo1_full,
	 almost_full => count_fifo1_almost_full,
	 empty => count_fifo1_empty);

ev_out_word_count_fifo2 : fifo_256x16
  PORT MAP (
	 clk => clk80,
	 rst => rst_or_flush,
	 din => count_fifo2_data_in,
    wr_en => count_fifo2_wen,
    rd_en => count_fifo2_ren,
	 dout => count_fifo2_data_out,
	 full => count_fifo2_full,
	 almost_full => count_fifo2_almost_full,
	 empty => count_fifo2_empty);
	 
event_fragment : gen_fragment
  port map (
    clk                   => clk80,
    rst                   => rst,
	 efb_engine_id		  => efb_id,
--    efb_flush_data        => efb_flush_data, -- SPC
--    send_empty_events     => send_empty_events,
    header_ev_id_empty    => header_ev_id_empty,
    header_ev_id_data     => header_ev_id_data_out,
    header_ev_id_ren      => header_ev_id_ren,
    err_sumry_fifo1_empty => err_sumry_fifo1_empty,
    err_sumry_fifo1_data  => err_sumry_fifo1_data_out,
    err_sumry_fifo1_ren   => err_sumry_fifo1_ren,
    err_sumry_fifo2_empty => err_sumry_fifo2_empty,
    err_sumry_fifo2_data  => err_sumry_fifo2_data_out,
    err_sumry_fifo2_ren   => err_sumry_fifo2_ren,
    count_fifo1_empty     => count_fifo1_empty,
    count_fifo1_data      => count_fifo1_data_out(13 downto 0),
    count_fifo1_ren       => count_fifo1_ren,
    count_fifo2_empty     => count_fifo2_empty,
    count_fifo2_data      => count_fifo2_data_out(13 downto 0),
    count_fifo2_ren       => count_fifo2_ren,
    halt_output           => halt_output,
    out_mem1_play         => out_mem1_play,
    out_mem1_num_words    => out_mem1_num_words,
    out_mem1_done         => out_mem1_done,
    out_mem2_play         => out_mem2_play,
    out_mem2_num_words    => out_mem2_num_words,
    out_mem2_done         => out_mem2_done,
    ev_header_data_out    => ev_header_data_i,
    ev_header_dv_out_p1   => ev_header_data_valid_out,
	 ev_header_kword_out   => ev_header_kword_out_i,
    source_id             => source_id,
    format_version        => format_version,
    run_number            => run_number,
	 fmt_link_enabled      => fmt_link_enabled,		 
    mask_boc_err_in       => mask_boc_clock_err,
    mask_tim_err_in       => mask_tim_clock_err,
--    sweeper_event_out     => sweeper_event_i,
    hold_next_event_in    => hold_next_event_i,
    tim_bcid_in_rol_in    => tim_bcid_in_rol
--    hv_flag_set_in        => hv_flag_set_i
    );


distributed_outmem : if NOT use_bram GENERATE

	efb_event_out_fifo1_d : dist_fifo_4kwords
	  PORT MAP (
		 clk => clk80,
		 rst => out_fifo1_rst,
		 din => out_fifo1_data,
		 wr_en => out_fifo1_wen,
		 rd_en => out_fifo1_ren,
		 dout => out_fifo1_data_to_router,
		 full => out_fifo1_full,
		 almost_full => out_fifo1_afull,
		 empty => out_fifo1_empty,
		 almost_empty => out_fifo1_aempty
	  );
	  
	efb_event_out_fifo2_d : dist_fifo_4kwords
	  PORT MAP (
		 clk => clk80,
		 rst => out_fifo2_rst,
		 din => out_fifo2_data,
		 wr_en => out_fifo2_wen,
		 rd_en => out_fifo2_ren,
		 dout => out_fifo2_data_to_router,
		 full => out_fifo2_full,
		 almost_full => out_fifo2_afull,
		 empty => out_fifo2_empty,
		 almost_empty => out_fifo2_aempty
	  );

end generate;


bram_outmem : if use_bram GENERATE

	efb_event_out_fifo1_b : bram_fifo_4kwords
	  PORT MAP (
		 clk => clk80,
		 rst => out_fifo1_rst,
		 din => out_fifo1_data,
		 wr_en => out_fifo1_wen,
		 rd_en => out_fifo1_ren,
		 dout => out_fifo1_data_to_router,
		 full => out_fifo1_full,
		 almost_full => out_fifo1_afull,
		 empty => out_fifo1_empty,
		 almost_empty => out_fifo1_aempty
	  );
	  
	efb_event_out_fifo2_b : bram_fifo_4kwords
	  PORT MAP (
		 clk => clk80,
		 rst => out_fifo2_rst,
		 din => out_fifo2_data,
		 wr_en => out_fifo2_wen,
		 rd_en => out_fifo2_ren,
		 dout => out_fifo2_data_to_router,
		 full => out_fifo2_full,
		 almost_full => out_fifo2_afull,
		 empty => out_fifo2_empty,
		 almost_empty => out_fifo2_aempty
	  );

end generate;


outmem1_ctrl : outmem 
  port map (
    clk                   => clk80,
    rst                   => rst_or_flush,
    xoff                  => halt_output,
    out_mem_play          => out_mem1_play_i,
    out_mem_num_words     => out_mem1_num_words_i,
    out_mem_done          => out_mem1_done,
    out_mem_data_valid_p1 => out_mem1_data_valid,
    out_fifo_ren_n        => out_fifo1_ren_n_i,
    out_fifo_oe_n_p1      => out_fifo1_oe_n_p1
    );   

outmem2_ctrl : outmem 
  port map (
    clk                   => clk80,
    rst                   => rst_or_flush,
    xoff                  => halt_output,
    out_mem_play          => out_mem2_play_i,
    out_mem_num_words     => out_mem2_num_words_i,
    out_mem_done          => out_mem2_done,
    out_mem_data_valid_p1 => out_mem2_data_valid,
    out_fifo_ren_n        => out_fifo2_ren_n_i,
    out_fifo_oe_n_p1      => out_fifo2_oe_n_p1
    ); 


-- Signal routing for the top level register
    out_fifo1_play_done      <= out_mem1_done;
	 out_fifo2_play_done      <= out_mem2_done;
    efb_misc_stts_reg        <= efb_misc_stts_reg_i;
    ev_fifo_data1            <= ev_id_data_out1(34 downto 0);
    ev_fifo_data2            <= ev_id_data_out2(34 downto 0);
    ev_header_data           <= header_ev_id_data_stts_reg;
    event_count              <= event_count_i;
    l1id_fifo_data           <= l1id_diag_data;
    l1_trap_full_in          <= l1_trap_full; 


--eventmem_status_reg_i <= eventmem_fifo_cntrl_i;

header_ev_id_data_stts_reg <= header_ev_id_data_out( 4 downto  0) & '0' & -- modified, L1ID
                              header_ev_id_data_out(45 downto 36); -- modified, BCID

-- this will need some study and modifications
efb_misc_stts_reg_i <=   count_fifo2_full              -- (30)
                       & count_fifo1_full              -- (29)
                       & err_sumry_fifo2_full          -- (28)
                       & err_sumry_fifo1_full          -- (27)
                       & ev_data_empty_i               -- (26)
                       & ev_id_full2                   -- (25)
                       & ev_id_full1                   -- (24)
                       & header_ev_id_full             -- (23)
                       & ev_data_almost_full_n_i       -- (22)
                       & header_ev_id_almost_full      -- (21)
                       & ev_id_almost_full2            -- (20)
                       & ev_id_almost_full1            -- (19)
                       & header_ev_id_data_out(34)     -- (18)
                       & "0000" --form_enabled_n2      -- (17:14) -- should be taken out
                       & "0000" --form_enabled_n1      -- (13:10) -- should be taken out
                       & gatherer_halt_output          -- (9)
                       & fifo2_pause_i                 -- (8)
                       & fifo1_pause_i                 -- (7)
                       & header_ev_id_empty            -- (6)
                       & ev_id_empty2                  -- (5)
                       & ev_id_empty1                  -- (4)
                       & ev_id_fifo_empty_error2       -- (3)
                       & ev_id_fifo_empty_error1       -- (2)
                       & err_sumry_fifo2_almost_full   -- (1)
                       & err_sumry_fifo1_almost_full;  -- (0)

--------------------------------------------------------------------------

rst_or_flush  <= rst; --OR out_fifo_rst; -- SPC
--ht_fifo_rst_i <= rst OR efb_flush_data OR out_fifo_rst;
ev_data_flush <= rst OR NOT ev_data_rst_n;

-- modified new addition
fifo1_pause <= fifo1_pause_i;
fifo2_pause <= fifo2_pause_i;

--out_fifo1_rst <= (rst OR efb_flush_data OR out_fifo_rst);
--out_fifo2_rst <= (rst OR efb_flush_data OR out_fifo_rst);
-- SPC
out_fifo1_rst <= rst; --OR out_fifo_rst);
out_fifo2_rst <= rst; --OR out_fifo_rst);

 -- this is all from the 2 evtmem output and the fragment generator
data_valid_out_i <= ev_header_data_valid_out_i OR---
                    out_mem1_data_valid OR---
                    out_mem2_data_valid;---

-- final output of the efb
output_data_mux: process (
  ev_header_data_valid_out_i,
  out_mem1_data_valid,
  out_mem2_data_valid,
  ev_header_data_i,
  ev_header_kword_out_i,
  out_fifo1_data_to_router,
  out_fifo2_data_to_router
  )
begin
	if (ev_header_data_valid_out_i = '1') then
	  out_data_to_router_i(31 downto 0) <= ev_header_data_i(31 downto 0);
	  data_chip_id_i (2 DOWNTO 0) <= (others => '0');
	  kword_to_router_i <= ev_header_kword_out_i;
	elsif (out_mem1_data_valid = '1') then
	  out_data_to_router_i(31 downto 0) <= out_fifo1_data_to_router(31 downto 0);
	  data_chip_id_i (2 DOWNTO 0) <= out_fifo1_data_to_router(34 downto 32);
	  kword_to_router_i <= '0';
	elsif (out_mem2_data_valid = '1') then
	  out_data_to_router_i(31 downto 0) <= out_fifo2_data_to_router(31 downto 0);
	  data_chip_id_i (2 DOWNTO 0) <= out_fifo2_data_to_router(34 downto 32);
	  kword_to_router_i <= '0';
	else
	  out_data_to_router_i(31 downto 0) <= (others => '0');
	  data_chip_id_i (2 DOWNTO 0) <= (others => '0');
	  kword_to_router_i <= '0';
	end if;
end process output_data_mux;

k_word_process: process(clk80, rst)
begin
	if(rst = '1') then
		data_valid_out			<= '0';
		out_data_to_router	<= (others => '0');
		k_word					<= '0';
	elsif(clk80'event and clk80 = '1') then
		data_valid_out			<= data_valid_out_i;
      k_word 					<= kword_to_router_i;
		out_data_to_router 	<= out_data_to_router_i;	
		data_chip_id			<= data_chip_id_i;
	end if;
end process k_word_process;

ev_data_almost_full_n <= ev_data_almost_full_n_i_out; -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>


--- these can all later be simplified...
out_fifo1_empty_n   <= NOT out_fifo1_empty;---
out_fifo1_aempty_n  <= NOT out_fifo1_aempty;---
out_fifo1_full_n    <= NOT out_fifo1_full;---
out_fifo1_afull_n   <= NOT out_fifo1_afull;---

out_fifo2_empty_n   <= NOT out_fifo2_empty;---
out_fifo2_aempty_n  <= NOT out_fifo2_aempty;---
out_fifo2_full_n    <= NOT out_fifo2_full;---
out_fifo2_afull_n   <= NOT out_fifo2_afull;---

mux_outmem1_control_sigs: process (
  out_mem1_play, 
  out_mem1_num_words,
  out_fifo1_ren_n_i,
  out_fifo1_oe_n_p1,
  rst,
  clk80
  )
begin
  if (rst = '1') then
    out_mem1_play_i       <= '0';
    out_mem1_num_words_i  <= (others => '0');
    out_fifo1_ren     <= '1';
    out_fifo1_oe_n      <= '1';
  else
        out_mem1_play_i       <= out_mem1_play;
        out_mem1_num_words_i  <= out_mem1_num_words;
        out_fifo1_ren     <= NOT out_fifo1_ren_n_i;
        out_fifo1_oe_n      <= out_fifo1_oe_n_p1;  --out_fifo1_ren_n_i;
  end if;
end process mux_outmem1_control_sigs;


mux_outmem2_control_sigs: process (
  out_mem2_play,
  out_mem2_num_words,
  out_fifo2_ren_n_i,
  out_fifo2_oe_n_p1,
  rst, 
  clk80
  )
begin
  if (rst = '1') then
    out_mem2_play_i       <= '0';
    out_mem2_num_words_i  <= (others => '0');
    out_fifo2_ren         <= '1';
    out_fifo2_oe_n        <= '1';
  else
        out_mem2_play_i       <= out_mem2_play;
        out_mem2_num_words_i  <= out_mem2_num_words;
        out_fifo2_ren         <= NOT out_fifo2_ren_n_i;
        out_fifo2_oe_n        <= out_fifo2_oe_n_p1;  --out_fifo2_ren_n_i;
--    end case;
  end if;
end process mux_outmem2_control_sigs;


mux_htf_control_sigs: process (
  ev_header_data_valid_out_d1,
  ev_header_data_i,
  rst, 
  clk80
  )
begin
  if (rst = '1') then
--    ev_header_data           <= (others => '0');
    ev_header_data_valid_out_i <= '0';
  else
--    ev_header_data           <= ev_header_data_i;
    ev_header_data_valid_out_i <= ev_header_data_valid_out_d1;
  end if;
end process mux_htf_control_sigs;

register_data : process (
  clk80, 
  rst
  )
--  subtype select_mode is STD_LOGIC_VECTOR(2 downto 0);
  begin -- register
    if (rst = '1') then
      halt_output                  <= '0';     
      fifo1_pause_i                <= '0';
      fifo2_pause_i                <= '0';
      ev_id_fifo_empty_error       <= '0';
      ev_data_almost_full_n_i_out  <= '1'; -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
      ev_header_data_valid_out_d1  <= '0';
      out_fifo1_data               <= (others => '0');
      out_fifo1_wen                <= '1';
      out_fifo2_data               <= (others => '0');
      out_fifo2_wen                <= '1';
--      ht_fifo_we_i      <= '0';
      ev_data_ready                <= '0';
      hold_next_event_i            <= '0';
    elsif (clk80'event and clk80 = '1') then
	 
      halt_output   <= gatherer_halt_output;     

      fifo1_pause_i <=  out_fifo1_afull    OR 
                        err_sumry_fifo1_almost_full OR
                        count_fifo1_almost_full;

      fifo2_pause_i <=  out_fifo2_afull    OR 
                        err_sumry_fifo2_almost_full OR
                        count_fifo2_almost_full;

      ev_id_fifo_empty_error <= ev_id_fifo_empty_error1 OR
                                ev_id_fifo_empty_error2;

      ev_data_almost_full_n_i_out <= ev_data_almost_full_n_i AND -->>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
                                 NOT (halt_output AND header_ev_id_data_out(34));  --- readout link test and halt output

      ev_header_data_valid_out_d1 <= ev_header_data_valid_out;

      out_fifo1_data  <= out_fifo1_data_i;
      out_fifo1_wen   <= NOT out_fifo1_wen_n_i; -- OR out_fifo_diag_mode;
      out_fifo2_data  <= out_fifo2_data_i; 
      out_fifo2_wen   <= NOT out_fifo2_wen_n_i; -- OR out_fifo_diag_mode;

      ev_data_ready <= header_ev_id_wen_long_i;
    end if;
  end process register_data;
  
grp_evt_count_mux : process ( -- don't touch this part
  clk80, 
  rst
  )
begin -- register
  if (rst = '1') then
    event_count_i <= (others => '0');
  elsif (clk80'event and clk80 = '1') then
    if (grp_evt_enable = '1') then
      if (grp_evt_count1(3 downto 0) = grp_evt_count2(3 downto 0)) then
        event_count_i(3 downto 0) <= grp_evt_count1(3 downto 0);
      end if;
      if (grp_evt_count1(7 downto 4) = grp_evt_count2(7 downto 4)) then
        event_count_i(7 downto 4) <= grp_evt_count1(7 downto 4);
      end if;
      if (grp_evt_count1(11 downto 8) = grp_evt_count2(11 downto 8)) then
        event_count_i(11 downto 8) <= grp_evt_count1(11 downto 8);
      end if;
      if (grp_evt_count1(15 downto 12) = grp_evt_count2(15 downto 12)) then
        event_count_i(15 downto 12) <= grp_evt_count1(15 downto 12);
      end if;
      if (grp_evt_count1(19 downto 16) = grp_evt_count2(19 downto 16)) then
        event_count_i(19 downto 16) <= grp_evt_count1(19 downto 16);
      end if;
      if (grp_evt_count1(23 downto 20) = grp_evt_count2(23 downto 20)) then
        event_count_i(23 downto 20) <= grp_evt_count1(23 downto 20);
      end if;
      if (grp_evt_count1(27 downto 24) = grp_evt_count2(27 downto 24)) then
        event_count_i(27 downto 24) <= grp_evt_count1(27 downto 24);
      end if;
      if (grp_evt_count1(31 downto 28) = grp_evt_count2(31 downto 28)) then
        event_count_i(31 downto 28) <= grp_evt_count1(31 downto 28);
      end if;
    else
      event_count_i <= (others => '0');
    end if;
  end if;
end process grp_evt_count_mux;

END rtl;
