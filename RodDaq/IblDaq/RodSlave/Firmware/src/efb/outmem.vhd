----------------------------------------------------------------------------
-- LBL - WISCONSIN
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    outmem.vhd
-- By John Joseph
-- Description:
--    ROD Dual Gatherer
--    IBLROD/FEI4 Version
--    Writes a block of data out to the data exchange
--
-- Notes:
--    The read enable signal must become active 1 clk cycle before the
--    output enable and data valid signals for the event mem FIFO to 
--    output data correctly.  An example of a 2 word read cycle is shown 
--    in the figure below:
--
--               ____      ____      ____      ____      ____      ____
--   RCLK       |    |    |    |    |    |    |    |    |    |    |    |    
--          ____|    |____|    |____|    |____|    |____|    |____|    |__
--          _________________________                      _______________
--                                   |                    |
--   REN_N                           |____________________|
--          ___________________________________                      _____
--                                             |                    |
--   OE_N                                      |____________________|
--                                              ____________________
--   DATA_VALID                                |                    |
--          ___________________________________|                    |_____
--
--
----------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY outmem IS
  port (
    clk                   : in  STD_LOGIC ;
    rst                   : in  STD_LOGIC ;
    xoff                  : in  STD_LOGIC ;
    out_mem_play          : in  STD_LOGIC ;
    out_mem_num_words     : in  STD_LOGIC_VECTOR(13 DOWNTO 0) ;  -- the count of words in output FIFO
    out_mem_done          : out STD_LOGIC ;
    out_mem_data_valid_p1 : out STD_LOGIC ;
    out_fifo_ren_n        : out STD_LOGIC ;
    out_fifo_oe_n_p1      : out STD_LOGIC 
    );  
END outmem ;

architecture rtl of outmem is

----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------
  signal xoff_d1               : STD_LOGIC ;
  signal out_fifo_ren_n_p1     : STD_LOGIC ;
  signal out_fifo_oe_n_p2      : STD_LOGIC ;
  signal out_fifo_oe_n_p3      : STD_LOGIC ;

  signal count                 : UNSIGNED (13 downto 0);
  signal play_zero_words       : STD_LOGIC ;
  signal play_one_word         : STD_LOGIC ;
  signal play_done             : STD_LOGIC ;
  signal count_done            : STD_LOGIC ;
  signal play_flag             : STD_LOGIC ; 
  signal out_mem_play_d1       : STD_LOGIC ;
  signal out_mem_play_d2       : STD_LOGIC ;
  signal out_mem_done_p1       : STD_LOGIC ; 
  signal out_mem_data_valid_p2 : STD_LOGIC ;
  signal out_mem_data_valid_p3 : STD_LOGIC ;
  
----------------------------------------------------------------------------
--COMPONENT DECLARATION
----------------------------------------------------------------------------

BEGIN  --  Main Body of VHDL code

----------------------------------------------------------------------------
--COMPONENT INSTANTIATION
----------------------------------------------------------------------------


register_data : process (clk, rst)
begin -- register_data
  if (rst = '1') then
    out_mem_play_d1       <= '0';
    out_mem_play_d2       <= '0';
    xoff_d1               <= '0';
    out_fifo_ren_n        <= '1';
    out_fifo_oe_n_p1      <= '1';
    out_fifo_oe_n_p2      <= '1';
    out_mem_data_valid_p1 <= '0';
    out_mem_data_valid_p2 <= '0';
    out_mem_done          <= '0';
  elsif (clk'event and clk = '1') then
    out_mem_play_d1       <= out_mem_play;
    out_mem_play_d2       <= out_mem_play_d1;                    
    xoff_d1               <= xoff;
    out_fifo_ren_n        <= out_fifo_ren_n_p1;
    out_fifo_oe_n_p1      <= out_fifo_oe_n_p2;
    out_fifo_oe_n_p2      <= out_fifo_oe_n_p3;
    out_mem_data_valid_p1 <= out_mem_data_valid_p2;
    out_mem_data_valid_p2 <= out_mem_data_valid_p3;
    out_mem_done          <= out_mem_done_p1;
  end if;
end process register_data;

burst_data : process (clk, rst)
begin -- burst_data
  if (rst = '1') then
    play_flag             <= '0';
    count                 <= "00000000000001";
    play_zero_words       <= '1';
    play_one_word         <= '0';
    play_done             <= '0';
    count_done            <= '1';
    out_mem_done_p1       <= '0';
    out_fifo_ren_n_p1     <= '1';
    out_fifo_oe_n_p3      <= '1';
    out_mem_data_valid_p3 <= '0';
  elsif (clk'event AND clk = '1') then
    out_fifo_ren_n_p1     <= '1';
    out_fifo_oe_n_p3      <= '1';
    out_mem_data_valid_p3 <= '0';

    if (out_mem_play_d1 = '1' AND out_mem_play_d2 = '0') then
      count     <= "00000000000001";
      play_flag <= '1';
      play_done <= '0';
    end if;

    if (std_logic_vector(out_mem_num_words) = "00000000000000") then
      play_zero_words <= '1';
      play_one_word   <= '0';
    elsif (std_logic_vector(out_mem_num_words) = "00000000000001") then   -- JMJ
      play_zero_words <= '0';
      play_one_word   <= '1';
    else
      play_zero_words <= '0';
      play_one_word   <= '0';
    end if;

    if ((std_logic_vector(count) = out_mem_num_words) and xoff_d1 = '0') then
      count_done <= '1';
    else
      count_done <= '0';
    end if;

    if (play_flag = '1') then
      if (count_done = '0' AND play_zero_words = '0' AND play_one_word = '0') then
        if (xoff_d1 = '1') then  -- Backpressure from the S-Link
          out_fifo_ren_n_p1     <= '1';
          out_fifo_oe_n_p3      <= '1';
          out_mem_data_valid_p3 <= '0';
        else
          out_fifo_ren_n_p1     <= '0';
          out_fifo_oe_n_p3      <= '0';
          out_mem_data_valid_p3 <= '1';
          count <= count + 1;
        end if;  
      elsif (play_one_word = '1' AND play_done = '0') then  -- JMJ
        if (xoff_d1 = '0') then
          out_fifo_ren_n_p1     <= '0';
          out_fifo_oe_n_p3      <= '0';
          out_mem_data_valid_p3 <= '1';
          play_done             <= '1';
        end if;  
      else
        count           <= "00000000000000";     
        out_mem_done_p1 <= '1';
        play_flag       <= '0';
      end if;
    else
      out_mem_done_p1 <= '0';
    end if;  
  end if;
end process burst_data;

END rtl;
