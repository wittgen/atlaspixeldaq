--------------------------------------------------------------------------------
-- Company:  University of Washington
-- Engineer:  Bing (Shaw-Pin) Chen
--
-- Create Date:   11:17:34 06/24/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/CERN/efb_bing/ibl_efb/ev_data_decode_tb.vhd
-- Project Name:  ibl_efb
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ev_data_decode
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY ev_data_decode_tb IS
END ev_data_decode_tb;
 
ARCHITECTURE behavior OF ev_data_decode_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT ev_data_decode
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         ev_data_in : IN  std_logic_vector(15 downto 0);
         ev_data_wen_n : IN  std_logic;
         ev_data_almost_full_n : OUT  std_logic;
         ev_data_empty : OUT  std_logic;
         header_ev_id_data : OUT  std_logic_vector(63 downto 0);
         header_ev_id_wen : OUT  std_logic;
         ev_id_data1 : OUT  std_logic_vector(26 downto 0);
         ev_id_wen1 : OUT  std_logic;
         ev_id_data2 : OUT  std_logic_vector(26 downto 0);
         ev_id_wen2 : OUT  std_logic;
         ev_fifo1_af : IN  std_logic;
         ev_fifo2_af : IN  std_logic;
         ev_fifo3_af : IN  std_logic;
         bcid_offset : IN  std_logic_vector(9 downto 0);
         bcid_rollover_select : IN  std_logic;
         mask_sel : IN  std_logic_vector(1 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal ev_data_in : std_logic_vector(15 downto 0) := (others => '0');
   signal ev_data_wen_n : std_logic := '0';
   signal ev_fifo1_af : std_logic := '0';
   signal ev_fifo2_af : std_logic := '0';
   signal ev_fifo3_af : std_logic := '0';
   signal bcid_offset : std_logic_vector(9 downto 0) := (others => '0');
   signal bcid_rollover_select : std_logic := '0';
   signal mask_sel : std_logic_vector(1 downto 0) := (others => '0');

 	--Outputs
   signal ev_data_almost_full_n : std_logic;
   signal ev_data_empty : std_logic;
   signal header_ev_id_data : std_logic_vector(63 downto 0);
   signal header_ev_id_wen : std_logic;
   signal ev_id_data1 : std_logic_vector(26 downto 0);
   signal ev_id_wen1 : std_logic;
   signal ev_id_data2 : std_logic_vector(26 downto 0);
   signal ev_id_wen2 : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ev_data_decode PORT MAP (
          clk => clk,
          rst => rst,
          ev_data_in => ev_data_in,
          ev_data_wen_n => ev_data_wen_n,
          ev_data_almost_full_n => ev_data_almost_full_n,
          ev_data_empty => ev_data_empty,
          header_ev_id_data => header_ev_id_data,
          header_ev_id_wen => header_ev_id_wen,
          ev_id_data1 => ev_id_data1,
          ev_id_wen1 => ev_id_wen1,
          ev_id_data2 => ev_id_data2,
          ev_id_wen2 => ev_id_wen2,
          ev_fifo1_af => ev_fifo1_af,
          ev_fifo2_af => ev_fifo2_af,
          ev_fifo3_af => ev_fifo3_af,
          bcid_offset => bcid_offset,
          bcid_rollover_select => bcid_rollover_select,
          mask_sel => mask_sel
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 25 ns.
      wait for 25 ns;	
      rst <= '1'; wait for 10 ns;
      rst <= '0'; wait for 10 ns;
		mask_sel <= "11";
		ev_fifo1_af <= '0';
		ev_fifo2_af <= '0';
		ev_fifo3_af <= '0'; 
		bcid_rollover_select <= '0';
		bcid_offset <= "0111101011"; wait for 10 ns;
		
		-- First test, assuming everything is perfect everything should get passed along 
		-- just fine and BCID offset adds correctly (test passed)
		ev_data_wen_n <= '0';
		ev_data_in <= X"000A"; wait for 10 ns;
		ev_data_in <= X"1111"; wait for 10 ns;
		ev_data_in <= X"FFFC"; wait for 10 ns;
		ev_data_in <= X"3333"; wait for 10 ns;
		ev_data_in <= X"4444"; wait for 10 ns;
		ev_data_in <= X"5555"; wait for 10 ns;
		ev_data_in <= X"6666"; wait for 10 ns;
		ev_data_in <= X"7654"; wait for 200 ns;

		-- now turn off write enable to the internal fifo, should not be able to write 
		-- to the output fifo, so ev_id_fifo and header fifo wen disabled (test passed)
		ev_data_wen_n <= '1';
		ev_data_in <= X"000A"; wait for 10 ns;
		ev_data_in <= X"1111"; wait for 10 ns;
		ev_data_in <= X"FFFC"; wait for 10 ns;
		ev_data_in <= X"3333"; wait for 10 ns;
		ev_data_in <= X"4444"; wait for 10 ns;
		ev_data_in <= X"5555"; wait for 10 ns;
		ev_data_in <= X"6666"; wait for 10 ns;
		ev_data_in <= X"7654"; wait for 200 ns;		
    
		-- now to assume one of the output fifos is full, result should be same as test above.
		-- (test passed)
		ev_fifo1_af <= '1';
		ev_data_wen_n <= '0';
		ev_data_in <= X"000A"; wait for 10 ns;
		ev_data_in <= X"1111"; wait for 10 ns;
		ev_data_in <= X"FFFC"; wait for 10 ns;
		ev_data_in <= X"3333"; wait for 10 ns;
		ev_data_in <= X"4444"; wait for 10 ns;
		ev_data_in <= X"5555"; wait for 10 ns;
		ev_data_in <= X"6666"; wait for 10 ns;
		ev_data_in <= X"7654"; wait for 200 ns;	

      wait;
   end process;

END;
