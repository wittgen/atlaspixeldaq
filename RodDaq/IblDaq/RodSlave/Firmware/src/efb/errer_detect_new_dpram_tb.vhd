--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:15:03 11/08/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/ibl_rod_fw/ibl_rodSlave/errer_detect_new_dpram_tb.vhd
-- Project Name:  ibl_rodSlave
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: err_detect_new
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY errer_detect_new_dpram_tb IS
END errer_detect_new_dpram_tb;
 
ARCHITECTURE behavior OF errer_detect_new_dpram_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT err_detect_new
    PORT(
         clk40 : IN  std_logic;
         clk80 : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic_vector(42 downto 0);
         data_valid_in : IN  std_logic;
         data_out : OUT  std_logic_vector(41 downto 0);
         data_valid_out : OUT  std_logic;
         err_sumry_fifo_wen : OUT  std_logic;
         err_sumry_fifo_data : OUT  std_logic_vector(47 downto 0);
         err_mask_wen : IN  std_logic;
         err_mask_din : IN  std_logic_vector(31 downto 0);
         err_mask_dout : OUT  std_logic_vector(31 downto 0);
         err_mask_sel : IN  std_logic_vector(2 downto 0);
         mask_bcid_error : IN  std_logic;
         mask_l1id_error : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk40 : std_logic := '0';
   signal clk80 : std_logic := '0';
   signal rst : std_logic := '0';
   signal data_in : std_logic_vector(42 downto 0) := (others => '0');
   signal data_valid_in : std_logic := '0';
   signal err_mask_wen : std_logic := '0';
   signal err_mask_din : std_logic_vector(31 downto 0) := (others => '0');
   signal err_mask_sel : std_logic_vector(2 downto 0) := (others => '0');
   signal mask_bcid_error : std_logic := '0';
   signal mask_l1id_error : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(41 downto 0);
   signal data_valid_out : std_logic;
   signal err_sumry_fifo_wen : std_logic;
   signal err_sumry_fifo_data : std_logic_vector(47 downto 0);
   signal err_mask_dout : std_logic_vector(31 downto 0);

   -- Clock period definitions
   constant clk40_period : time := 20 ns;
   constant clk80_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: err_detect_new PORT MAP (
          clk40 => clk40,
          clk80 => clk80,
          rst => rst,
          data_in => data_in,
          data_valid_in => data_valid_in,
          data_out => data_out,
          data_valid_out => data_valid_out,
          err_sumry_fifo_wen => err_sumry_fifo_wen,
          err_sumry_fifo_data => err_sumry_fifo_data,
          err_mask_wen => err_mask_wen,
          err_mask_din => err_mask_din,
          err_mask_dout => err_mask_dout,
          err_mask_sel => err_mask_sel,
          mask_bcid_error => mask_bcid_error,
          mask_l1id_error => mask_l1id_error
        );

   -- Clock process definitions
   clk40_process :process
   begin
		clk40 <= '0';
		wait for clk40_period/2;
		clk40 <= '1';
		wait for clk40_period/2;
   end process;
 
   clk80_process :process
   begin
		clk80 <= '0';
		wait for clk80_period/2;
		clk80 <= '1';
		wait for clk80_period/2;
   end process;
 
   rst_process: process
	begin
		rst <= '1'; wait for 40 ns;
		rst <= '0'; wait;
	end process;

	write_error_mask: process
	begin
		wait for 120 ns;
		err_mask_wen <= '1';
		err_mask_sel <= "000";
      err_mask_din <= X"11111111";
      wait for 20 ns;
		err_mask_sel <= "001";
      err_mask_din <= X"22222222";
      wait for 20 ns;
		err_mask_sel <= "010";
      err_mask_din <= X"33333333";
		wait for 20 ns;
		err_mask_sel <= "011";
      err_mask_din <= X"44444444";
		wait for 20 ns;
		err_mask_sel <= "100";
      err_mask_din <= X"55555555";
		wait for 20 ns;
		err_mask_sel <= "101";
      err_mask_din <= X"66666666";
		wait for 20 ns;
		err_mask_sel <= "110";
      err_mask_din <= X"77777777";
		wait for 20 ns;
		err_mask_sel <= "111";
      err_mask_din <= X"FFFFFFFF";
		wait for 20 ns;
		err_mask_wen <= '0';
		wait for 80 ns;
		
		err_mask_sel <= "000";
      err_mask_din <= X"13243442";
      wait for 20 ns;
		err_mask_sel <= "001";
      err_mask_din <= X"35254243";
      wait for 20 ns;
		err_mask_sel <= "010";
      err_mask_din <= X"25464323";
		wait for 20 ns;
		err_mask_sel <= "011";
      err_mask_din <= X"f4444444";
		wait for 20 ns;
		err_mask_sel <= "100";
      err_mask_din <= X"f4444444";
		wait for 20 ns;
		err_mask_sel <= "101";
      err_mask_din <= X"f4444444";
		wait for 20 ns;
		err_mask_sel <= "110";
      err_mask_din <= X"f4444444";
		wait for 20 ns;
		err_mask_sel <= "111";
      err_mask_din <= X"f4444444";
		
		wait;
		
	end process;
	
--	overwrite_error_mask: process
--	begin
--	
--	
--	end process;
	
	
	read_error_mask_internal: process
	begin	
		wait for 400 ns;
		data_in(34 downto 32) <= "111";     wait for 10 ns;
		data_in(34 downto 32) <= "110";     wait for 10 ns;
		data_in(34 downto 32) <= "101";     wait for 10 ns;
		data_in(34 downto 32) <= "100";     wait for 10 ns;
		data_in(34 downto 32) <= "011";     wait for 10 ns;
		data_in(34 downto 32) <= "010";     wait for 10 ns;
		data_in(34 downto 32) <= "001";     wait for 10 ns;
		data_in(34 downto 32) <= "000";     wait for 10 ns;		
		data_in(34 downto 32) <= "011";     wait for 10 ns;
		data_in(34 downto 32) <= "010";     wait for 10 ns;
		data_in(34 downto 32) <= "001";     wait for 10 ns;
		data_in(34 downto 32) <= "000";     wait for 10 ns;		
	
		wait;
	
	end process;
	
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	
      
		
		
		
		
		
		
      wait for clk40_period*10;

      -- insert stimulus here 

      wait;
   end process;

END;
