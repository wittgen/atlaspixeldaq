----------------------------------------------------------------------------
-- UW EE/PHYSICS - WASHINGTON
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    err_detect.vhd
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen 07.2013
-- Description:
--    ROD Dual Gatherer
--    IBLROD/FEI4 Version
--    Scans for errors in the data stream, contains maskable error
--    counters, generates error summary word for gen_fragment
--
----------------------------------------------------------------------------
--  History
--    07-06-2005 : Organized Error Map to match Format 3.0 Document
--
----------------------------------------------------------------------------
--
--  Error Flags used in the Generic Field of the first status element
--  Bit 0 = 0x00000001 BC ID Error (PPC event ID to EFB check)
--  Bit 1 = 0x00000002 L1 ID Error (PPC event ID to EFB check)
--  Bit 2 = 0x00000004 FE Readout Module Timeout (checked in Formatter)
--  Bit 3 = 0x00000008 Data may be incorrect -> see Bits[31:14]
--  Bit 4 = 0x00000010 Internal Buffer Overflow -> See Bits[15:14] -- not used now SPC 3/19/2014

--  From the 24-bit data trailer error field (only 8 bits used for now), these errors are detected and flagged in Formatter
--  Bit 5 =  0x00000020 Trailer Error (checked in Formatter)
--  Bit 6 =  0x00000040 Header/Trailer Error (checked in Formatter)
--  Bit 7 =  0x00000080 
--  Bit 8 =  0x00000100 
--  Bit 9 =  0x00000200 
--  Bit 10 = 0x00000400 
--  Bit 11 = 0x00000800 
--  Bit 12 = 0x00001000

--  Bit 13 = 0x00002000 Link masked by PPC (from Formatter), omit if not needed
--  Bit 14 = 0x00004000 Reserved for ROD BUSY error
--  Bit 15 = 0x00008000 Reserved for ROD BUSY error
--  Bit 16 = 0x00010000 Non-Sequential Chip Error (checked in EFB)    --- this may be taken out, may not be needed
--  Bit 17 = 0x00020000 Header bit error (preamble error in header)
--  Bit 18 = 0x00040000 Sync error (getting raw giberish data)
--  Bit 19 = 0x00080000 Invalid pixel row (> 335) OR column (> 79) (Checked in Formatter)
--  Bit 20 = 0x00100000 There are skipped events (monitored in Formatter in the trailer)
--
--  FE-I4 Service Record Error
--  Bit 21 = 0x00200000 FE-I4 ==> bit 0 = BCID counter error (requires Bunch Counter reset)
--  Bit 22 = 0x00400000 FE-I4 ==> bit 1, 2, 3 = Hamming code word error (initially masked)
--  Bit 23 = 0x00800000 FE-I4 ==> bit 4, 5, 6, 7 = L1-related error (requires L1 Counter reset)
--  Bit 24 = 0x01000000 FE-I4 ==> bit 8 = read out processor error
--  Bit 25 = 0x02000000 FE-I4 ==> bit 24 = TRM(triple redundant mismatch) in config memory (requires reconfiguration)
--  Bit 26 = 0x04000000 FE-I4 ==> bit 25 = write register data error
--  Bit 27 = 0x08000000 FE-I4 ==> bit 26 = address error
--  Bit 28 = 0x10000000 FE-I4 ==> bit 27 = other cmd decoder error
--  Bit 29 = 0x20000000 FE-I4 ==> bit 29 = TRM SEU(single-event upset) detected in cmd decoder (initially masked)
--  Bit 30 = 0x40000000 FE-I4 ==> bit 30 = data bus address error
--  Bit 31 = 0x80000000 FE-I4 ==> bit 31 = TRM in EFUSE
--
-- err_sumry_fifo_data(14: 0) <= err_sumry_count(14:0)
-- err_sumry_fifo_data(47:16) <= err_sumry_flags(31:0)
--
-- data output gets rid of nonseq chip error and put that into error summary word
----------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;

-- pragma translate_off
library UNISIM;
use unisim.all;
-- pragma translate_on

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY err_detect_new IS
  port (
    clk40, clk80        : in  STD_LOGIC ;
    rst                 : in  STD_LOGIC ;
    data_in             : in  STD_LOGIC_VECTOR(42 DOWNTO 0) ;
    data_valid_in       : in  STD_LOGIC ;  -- when bcl1 check data is ready for error processing 
    data_out            : out STD_LOGIC_VECTOR(41 DOWNTO 0) ; -- not include non-sequential chip error bit
    data_valid_out      : out STD_LOGIC ;
    err_sumry_fifo_wen  : out STD_LOGIC ;
    err_sumry_fifo_data : out STD_LOGIC_VECTOR(47 DOWNTO 0) ;
    err_mask_wen        : in  STD_LOGIC ;
    err_mask_din        : in  STD_LOGIC_VECTOR(31 DOWNTO 0) ;
    err_mask_dout       : out STD_LOGIC_VECTOR(31 DOWNTO 0) ;
    err_mask_sel        : in  STD_LOGIC_VECTOR( 2 DOWNTO 0) ; -- modified from 5 downto 0 
    mask_bcid_error     : in  STD_LOGIC ;
    mask_l1id_error     : in  STD_LOGIC 
    );
END err_detect_new ;

architecture rtl of err_detect_new is
  
----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------
signal mem_rdout : STD_LOGIC_VECTOR(31 downto 0);
signal mem_wdout : STD_LOGIC_VECTOR(31 downto 0);
signal mem_raddr : STD_LOGIC_VECTOR( 2 downto 0);
signal mem_waddr : STD_LOGIC_VECTOR( 2 downto 0);
signal mem_wdin  : STD_LOGIC_VECTOR(31 downto 0);
signal mem_wren  : STD_LOGIC;

signal err_sumry_flags : STD_LOGIC_VECTOR(31 downto 0);
signal err_sumry_count : UNSIGNED(14 downto 0);
signal inc_err_count0  : STD_LOGIC;
-- signal inc_err_count1  : STD_LOGIC; -- an extra counter when the 16-bit SCT format is used, not needed in IBLROD
-- signal trailer_err_sumry_count : UNSIGNED(3 downto 0);  -- new addition for IBL
signal trailer_err_sumry_count : integer;
signal link_num        : STD_LOGIC_VECTOR(2 downto 0);

signal err_mask_dout_i      : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal err_sumry_mask       : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal err_sumry_word       : STD_LOGIC_VECTOR(31 DOWNTO 0);
signal last_word            : STD_LOGIC;
signal last_word_d1         : STD_LOGIC;
signal err_sumry_word_valid : STD_LOGIC;

----------------------------------------------------------------------------
--COMPONENT DECLARATION
----------------------------------------------------------------------------

COMPONENT emask_bram_32x8
  PORT (
    clka  : IN  STD_LOGIC;
    rsta  : IN  STD_LOGIC;
    wea   : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    addra : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
    dina  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    clkb  : IN  STD_LOGIC;
    web   : IN  STD_LOGIC_VECTOR(0 DOWNTO 0);
    addrb : IN  STD_LOGIC_VECTOR(2 DOWNTO 0);
    dinb  : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
  );
END COMPONENT;

BEGIN  --  Main Body of VHDL code

----------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
----------------------------------------------------------------------------
error_mask_storage : emask_bram_32x8
  PORT MAP (
    clka   => clk40,
    rsta   => rst,
    wea(0) => mem_wren,
    addra  => mem_waddr,
    dina   => mem_wdin,
    douta  => mem_wdout,
    clkb   => clk80,
    web(0) => '0',
    addrb  => mem_raddr,
    dinb   => X"00000000",
    doutb  => mem_rdout
  );  
 
-- need to change all these
mem_raddr <= link_num(2 downto 0);     -- modified
mem_waddr <= err_mask_sel(2 downto 0); -- modified

mem_wdin        <= err_mask_din;
mem_wren        <= err_mask_wen;
err_mask_dout   <= mem_wdout;
err_mask_dout_i <= mem_rdout(31 downto 0); -- why need to specify vector element?

-- Begin main body of error processing logic
link_num <= data_in (34 downto 32); -- modified (not include efb)

-- modified (completed)
register_data : process (clk80, rst)
begin -- register
    if (rst = '1') then
      data_out             <= (others => '0');
      data_valid_out       <= '0';
      last_word            <= '0';
      last_word_d1         <= '0';
      err_sumry_word_valid <= '0';
      err_sumry_mask       <= (others => '0');
    elsif (clk80'event and clk80 = '1') then
      data_out             <= data_in(41 downto 0);  -- not include non-sequential error flag
      data_valid_out       <= data_valid_in;
      last_word            <= data_in(41); -- end of event flag
      last_word_d1         <= last_word; -- delay one clock cycle
      err_sumry_word_valid <= data_valid_in;
      err_sumry_mask       <= err_mask_dout_i;
    end if;
end process register_data;

-- modified (yet to complete)
-- check each error flag and account for different masks. 
-- Produces the error summary word that goes to gen_fragment.vhd block
decode_errors : process (clk80, rst)
begin -- register
  if (rst = '1') then
    err_sumry_word <= (others => '0');
    inc_err_count0 <= '0';
	 trailer_err_sumry_count <= 0;
  elsif (clk80'event and clk80 = '1') then
    err_sumry_word <= (others => '0');
    inc_err_count0 <= '0'; -- resets next cycle unless there is an error count
    trailer_err_sumry_count <= 0;
    -- the next few statements, if errors exist in all, is only counted as a single error?
    -- Bit[0] : BC ID error (modification complete) --header only
    if(data_valid_in = '1') then
       -- Bit[13] : reserved for ROD BUSY error, this is supposed to be detected in header
	    if (data_in(40) = '1') then --header only
         err_sumry_word(13) <= '1';
         inc_err_count0 <= '1';
       end if;
       -- Bit[16] : non-sequential chip error (new IBL addition)
       if (data_in(42) = '1') then --header only
         err_sumry_word(16) <= '1';
         inc_err_count0 <= '1';
       end if;
       -- Not yet specified in IBL. Use only if we have Raw Data mode at the formatter output
       -- Bit[18] : check sync error
       if (data_in(31 downto 29) = "011") then 
         err_sumry_word(18) <= '1'; -- gets only raw data
         inc_err_count0 <= '1';
       end if;

     if (data_in(36) = '0') then --check for timeout error
       case data_in(31 downto 29) is
          --Look for trailer
          when "010" =>
              if (data_in(39) = '1' AND mask_bcid_error = '0') then
                 err_sumry_word(0) <= '1';
                 inc_err_count0 <= '1';
               end if;
               -- Bit[1] : L1 ID error (modification complete) --header only
               if (data_in(38) = '1' AND mask_l1id_error = '0') then
                 err_sumry_word(1) <= '1';
                 inc_err_count0 <= '1';
               end if;
               -- Bit[3] : Data may be incorrect -> see Bits[31:14]
               -- Bit[4] : Internal Buffer Overflow -> See Bits[15:14]
               -- Bit[5:12] : Trailer errors detected and flagged from the Formatter
	            -- add the error summary words from the trailer and add the count of error flags
               trailer_err_sumry_count <= conv_integer(unsigned(data_in(23 downto 23)))+ --Timeout Error bit
	                                       conv_integer(unsigned(data_in(20 downto 20)))+ --Header Error
	                                       conv_integer(unsigned(data_in(19 downto 19)))+ --L1ID Error
	                                       conv_integer(unsigned(data_in(18 downto 18)))+ --BCID Error
	                                       conv_integer(unsigned(data_in(17 downto 17)))+ --Trailer Error
	                                       conv_integer(unsigned(data_in(16 downto 16)))+ --HT Limit Error
	                                       conv_integer(unsigned(data_in(15 downto 15))); --Row/Col Error
               -- Bit[14] : reserved for ROD BUSY error, this is supposed to be detected in header
               -- Bit[15] : reserved for ROD BUSY error, this is supposed to be detected in header
	            -- should be combined with previous error check bit[5:12] if statement later...
               -- check almost full error, data truncated
               --  err_sumry_word(14) <= '0';--SPC 3/19/2014 data_in(27);  -- needs to be changed/clarified
               -- check overflow
               --  err_sumry_word(15) <= '0';--SPC 3/19/2014 data_in(26);  -- needs to be changed/clarified
               --  inc_err_count0 <= '0';--SPC 3/19/2014 data_in(27) OR data_in(26);

               -- Bit[17] : check header bit error (preamble error) (modification complete)
               if (data_in(20) = '1') then
                 err_sumry_word(17) <= '1';
                 inc_err_count0 <= '1';
               end if;
               --Trailer Error
               if (data_in(17) = '1') then
                 err_sumry_word(5) <= '1';
                 inc_err_count0 <= '1';
               end if;
               --Header/Trailer Limit Error
               if (data_in(16) = '1') then
                 err_sumry_word(6) <= '1';
                 inc_err_count0 <= '1';
               end if;
               -- Bit[19] : invalid row or column (checked in formatter)
               if (data_in(15) = '1') then
                 err_sumry_word(19) <= '1';
                 inc_err_count0 <= '1';
               end if;
               -- Bit[20] : check for skipped events (modification completed)
               if (data_in(14 downto 5) /= "0000000000") then
                  err_sumry_word(20) <= '1'; -- header and non-zero skipped event count
                  inc_err_count0 <= '1';
               end if;
            -- Bit[2] : timeout error (modification complete) (don't need to check for empty events data?)

	      -- Bits[21:31] FE-I4 Service Record Error Flags:
         when "000" => 
	       -- not sure if we want to count the error counter values, will overflow if too many errors
	       case data_in(22 downto 17) is 
		   	when "000000" =>  -- bit 0 BC counter Error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (21) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "000001" =>  -- bit 1 Hamming code error in word 0 
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (22) <= '1';  -- shared hamming code error word
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "000010" =>  -- bit 2 Hamming code error in word 1
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (22) <= '1';  -- shared hamming code error word
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "000011" =>  -- bit 3 Hamming code error in word 2
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (22) <= '1';  -- shared hamming code error word
		   			inc_err_count0 <= '1';
		   		end if;
            when "000100" =>  -- bit 4 L1_in counter error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (23) <= '1';  -- shared L1 counter error word
		   			inc_err_count0 <= '1';
		   		end if;
            when "000101" =>  -- bit 5 L1 request error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (23) <= '1';  -- shared L1 counter error word
		   			inc_err_count0 <= '1';
		   		end if;
            when "000110" =>  -- bit 6 L1 register error 
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (23) <= '1';  -- shared L1 counter error word
		   			inc_err_count0 <= '1';
		   		end if;
            when "000111" =>  -- bit 7 L1 trigger ID error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (23) <= '1';  -- shared L1 counter error word
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "001000" =>  -- bit 8 readout processor error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (24) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011000" =>  -- bit 24 Tripple redundant mismatch erro in config memory
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (25) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011001" =>  -- bit 25 write register data error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (26) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011010" =>  -- bit 26 address error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (27) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011011" =>  -- bit 27 other cmd decoder error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (28) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011101" =>  -- bit 29 TRM SEU detected in cmd decoder
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (29) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011110" =>  -- bit 30 data bus address error
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (30) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;
		   	when "011111" =>  -- bit 31 TRM in EFUSE
		   		if (data_in(9 downto 0) /= "0000000000") then
		   			err_sumry_word (31) <= '1';
		   			inc_err_count0 <= '1';
		   		end if;	
		   		
		   	when others => 
		   	  inc_err_count0 <= '0';
		    end case;
        when others =>
           inc_err_count0 <= '0';
       end case;
     else
        err_sumry_word(2) <= '1'; --Timeout Error
        inc_err_count0 <= '1';
     end if;
    end if;
  end if;
end process decode_errors;

process_errors : process (clk80, rst)
begin -- register
  if (rst = '1') then
    err_sumry_flags <= (others => '0');
    err_sumry_count <= (others => '0');
--	 trailer_err_sumry_count <= 0; -- new addition
    err_sumry_fifo_wen <= '0';
  elsif (clk80'event and clk80 = '1') then
    err_sumry_fifo_wen <= last_word; -- so it makes sense it's at a header not trailer
    if (last_word_d1 = '1') then
      err_sumry_flags <= (others => '0');
      err_sumry_count <= (others => '0');
--		trailer_err_sumry_count <= 0;  -- new addition, reset to 0 at last word
    elsif (err_sumry_word_valid = '1') then
	 
      for i in 0 to 2 loop  -- needs to be changed
        err_sumry_flags(i) <= err_sumry_flags(i) OR (err_sumry_word(i)  AND NOT err_sumry_mask(i));
      end loop;
      err_sumry_flags(3) <= err_sumry_flags(3) OR (((err_sumry_word(14) AND NOT err_sumry_mask(14))  OR -- this will need to be modified depending on the trailer error type
		                                              (err_sumry_word(15) AND NOT err_sumry_mask(15))  OR
		                                              (err_sumry_word(16) AND NOT err_sumry_mask(16))  OR
																	 (err_sumry_word(17) AND NOT err_sumry_mask(17))  OR
																	 (err_sumry_word(18) AND NOT err_sumry_mask(18))  OR
                                                    (err_sumry_word(19) AND NOT err_sumry_mask(19))  OR
                                                    (err_sumry_word(20) AND NOT err_sumry_mask(20))  OR
                                                    (err_sumry_word(21) AND NOT err_sumry_mask(21))  OR
                                                    (err_sumry_word(22) AND NOT err_sumry_mask(22))  OR
                                                    (err_sumry_word(23) AND NOT err_sumry_mask(23))  OR
                                                    (err_sumry_word(24) AND NOT err_sumry_mask(24))  OR
                                                    (err_sumry_word(25) AND NOT err_sumry_mask(25))  OR
                                                    (err_sumry_word(26) AND NOT err_sumry_mask(26))  OR
                                                    (err_sumry_word(27) AND NOT err_sumry_mask(27))  OR
                                                    (err_sumry_word(28) AND NOT err_sumry_mask(28))  OR
                                                    (err_sumry_word(29) AND NOT err_sumry_mask(29))  OR
                                                    (err_sumry_word(30) AND NOT err_sumry_mask(30))  OR
                                                    (err_sumry_word(31) AND NOT err_sumry_mask(31))) AND
                                                     NOT err_sumry_mask(3));
      err_sumry_flags(4) <= err_sumry_flags(4) OR (((err_sumry_word(14) AND NOT err_sumry_mask(14))  OR  -- this will need to be modified depending on the trailer error type
                                                    (err_sumry_word(15) AND NOT err_sumry_mask(15))) AND 
                                                     NOT err_sumry_mask(4));
      for i in 14 to 31 loop
        err_sumry_flags(i) <= err_sumry_flags(i) OR (err_sumry_word(i) AND NOT err_sumry_mask(i));
      end loop;
		
      if (((err_sumry_word( 2 downto  0) AND NOT err_sumry_mask( 2 downto  0)) /= "000") OR -- there are some data errors, increment count
          ((err_sumry_word(31 downto 14) AND NOT err_sumry_mask(31 downto 14)) /= "000000000000000000")) then 
        if (std_logic_vector(err_sumry_count) < "111111111111111") then
		    -- modified to add trailer error count
          err_sumry_count <= err_sumry_count + inc_err_count0 + trailer_err_sumry_count; -- + inc_err_count1;
        end if;
      end if;
    end if;
  end if;
end process process_errors;

err_sumry_fifo_data <= err_sumry_flags & '0' & std_logic_vector(err_sumry_count);

END rtl;
