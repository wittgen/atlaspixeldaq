--------------------------------------------------------------------------------
-- Company:  University of Washington
-- Engineer:  Bing (Shaw-Pin) Chen
--
-- Create Date:   14:08:37 06/21/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/cern_stuff_2/davide/SPARTAN6/efb_Bing_sch/format_data_tb.vhd
-- Project Name:  efb_Bing_sch
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: format_data
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY format_data_tb IS
END format_data_tb;
 
ARCHITECTURE behavior OF format_data_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT format_data
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic_vector(41 downto 0);
         data_valid_in : IN  std_logic;
         data_out : OUT  std_logic_vector(41 downto 0);
         data_valid_out_n : OUT  std_logic;
         count_fifo_wen : OUT  std_logic;
         count_fifo_data : OUT  std_logic_vector(13 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal data_in : std_logic_vector(41 downto 0) := (others => '0');
   signal data_valid_in : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(41 downto 0);
   signal data_valid_out_n : std_logic;
   signal count_fifo_wen : std_logic;
   signal count_fifo_data : std_logic_vector(13 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant header : std_logic_vector(2 downto 0) := "001";
   constant hit : std_logic_vector(2 downto 0) := "100";
   constant error_flag : std_logic_vector(2 downto 0) := "000";
   constant trailer : std_logic_vector(2 downto 0) := "010";
	constant link0 : std_logic_vector(1 downto 0) := "00";
	constant link1 : std_logic_vector(1 downto 0) := "01";
	constant link2 : std_logic_vector(1 downto 0) := "10";
	constant link3 : std_logic_vector(1 downto 0) := "11";
	
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: format_data PORT MAP (
          clk => clk,
          rst => rst,
          data_in => data_in,
          data_valid_in => data_valid_in,
          data_out => data_out,
          data_valid_out_n => data_valid_out_n,
          count_fifo_wen => count_fifo_wen,
          count_fifo_data => count_fifo_data
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 25 ns;	
		report "Test begins!";
      rst <= '1'; wait for 10 ns;
      rst <= '0'; wait for 10 ns;
      data_valid_in <= '1'; -- wait for 10 ns;
--        - header      => [ 001 | p | xx | link#(2) | 0(4) | s(4) | FEI4B flag | L1ID(5) | BCID(10) ]  
--        - hit         => [ 100 | xx(3) | link#(2) | ToT(8) | col(7) | row(9) ]   
--        - error_flags => [ 0001 | link#(2) | xx(3) | SR code(6) | xx(7) | SR count(10) ]
--        - trailer     => [ 010 | xx(3) | link#(2) | errors found(24) ]
--      data_out[35:32] link address
--                      - [34:32] link# (0-3, 4-7)  -- this gets encoded from incoming data (2b) and gatherer ID (1b)
--                      - [35]    efb engine#(0-1)
--      data_out[36] timeout error
--      data_out[37] condensed mode
--      data_out[38] L1ID error
--      data_out[39] BCID error
--      data_out[40] Link masked by PPC
--      data_out[41] end of event fragment
	   data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
     	data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1110001110"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1010101010"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1111000011"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1101100100"; wait for 10 ns;
		data_in <= "0000000000" & error_flag & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		data_in <= "1000000000" & trailer & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		
		data_valid_in <= '0'; wait for 10 ns;
		
		data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1101100100"; wait for 10 ns;
		data_in <= "0000000000" & error_flag & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		data_in <= "1000000000" & trailer & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		
      data_valid_in <= '1'; wait for 10 ns;		
		
		data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110010"; wait for 10 ns;
     	data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1110001000"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1010101111"; wait for 10 ns;
		data_in <= "1000000000" & hit & "000" & link0 & "000000000" & "10101" & "1111000011"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1101100110"; wait for 10 ns;
		data_in <= "0000000000" & error_flag & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		data_in <= "1000000000" & trailer & "000" & link0 & "000000000" & "10101" & "1100110011"; wait for 10 ns;
		
		rst <= '1'; wait for 40 ns;
		data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110010"; wait for 10 ns;
     	data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1110001000"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1010101111"; wait for 10 ns;
		data_in <= "1000000000" & hit & "000" & link0 & "000000000" & "10101" & "1111000011"; wait for 10 ns;
		rst <= '0'; wait for 40 ns;
		
		data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110010"; wait for 10 ns;
     	data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1110001000"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1010101111"; wait for 10 ns;
		data_in <= "1000000000" & hit & "000" & link0 & "000000000" & "10101" & "1111000011"; wait for 10 ns;
		
		data_in <= "0000000000" & header & "000" & link0 & "000000000" & "10101" & "1100110010"; wait for 10 ns;
     	data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1110001000"; wait for 10 ns;
		data_in <= "0000000000" & hit & "000" & link0 & "000000000" & "10101" & "1010101111"; wait for 10 ns;
		data_in <= "1000000000" & hit & "000" & link0 & "000000000" & "10101" & "1111000011"; wait for 10 ns;	

      wait;
   end process;

END;
