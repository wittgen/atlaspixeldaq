----------------------------------------------------------------------------
-- University of Washington, Seattle, USA
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    gen_fragment.vhd
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen
-- 
-- 		03/03/2014 - modification of load fifo signals and FSM 
--
-- Description:
--    ROD Dual Gatherer
--    IBLROD/FEI4 Version
--
-- err_sumry_fifo_data(14: 0) <= err_sumry_count(14:0)
-- err_sumry_fifo_data(39:16) <= err_sumry_flags(23:0)
--
-- Event ID Data (from ev_data_decode)
--  header_ev_id_data(63:58) ==> ROD TT
--  header_ev_id_data(57:56) ==> TIM TT
--  header_ev_id_data(55:48) ==> ATLAS TT
--  header_ev_id_data(47:36) ==> BCID
--  header_ev_id_data(35)    ==> NOT USED *** 1b ***
--  header_ev_id_data(34)    ==> Sending RoL Test Block
--  header_ev_id_data(33)    ==> BOC CLOCK ERROR
--  header_ev_id_data(32)    ==> TIM CLOCK ERROR
--  header_ev_id_data(31:24) ==> ECRID
--  header_ev_id_data(23: 0) ==> L1ID
--
-------------------------------------------------------------------------------
--
--  Event Fragment Format == ROD Specific
--  Header
--  Slink UCTRL WORD       - 0xB0F00000
--  Start of Header Marker - 0xEE1234EE
--  Header Size            - 0x00000009
--  Format Version Number  - 0xMMMMmmmm M(MajorVerNum) m(MinorVerNum)
--  Source Identifier      - 0x00DDMMMM D(SubDetectorID) M(ModuleID) 
--  Run Number             - 0xRRSSSSSS R(Run Type) S(SequenceInRun)
--  Extended L1ID          - 0xEELLLLLL E(ECRID) L(L1ID)
--  BCID                   - 0x00000BBB B(BCID)
--  ATLAS Trigger Type     - 0x000000AA A(AtlasEventType)
--  Detector Event Type    - 0x000R000T R(RodEventType) T(TimEventType)
--
--   ****  DATA ELEMENTS  ****
--
--   Trailer
--  Status Element0       - 0xSSSSGGGG S(SpecificErrFlags) G(GenericErrFlags) 
--  Status Element1       - 0xUUUUCCCC U(UncountedErrFlags) C(ErrFlagCount)
--  # of Status Elements  - 0x00000002
--  # of Data Elements    - 0x0000NNNN
--  Status Block Position - 0x00000001
--  Slink UCTRL Word      - 0xE0F00000
--  
--  Format
--    Status Element0: See err_detect.vhd 
--    Status Element1:
--      Bits[31:18] - NOT USED
--      Bit 17      - TIM Clock OK (Not counted for each link)
--      Bit 16      - BOC Clock OK (Not counted for each link)
--      Bits[15: 0] - Link Error Count
--
------------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;

-------------------------------------------------------------------------------
--PORT DECLARATION
-------------------------------------------------------------------------------

ENTITY gen_fragment IS
  port (
      clk                   : in  STD_LOGIC ;
      rst                   : in  STD_LOGIC ;
      
	  efb_engine_id			: in  STD_LOGIC_VECTOR(1 DOWNTO 0);
      efb_flush_data        : out STD_LOGIC ; -- not used
--      send_empty_events     : in  STD_LOGIC ;
      header_ev_id_empty    : in  STD_LOGIC ;
      header_ev_id_data     : in  STD_LOGIC_VECTOR(63 DOWNTO 0) ;
      header_ev_id_ren      : out STD_LOGIC ;

      err_sumry_fifo1_empty : in  STD_LOGIC ;
      err_sumry_fifo1_data  : in  STD_LOGIC_VECTOR(47 DOWNTO 0) ; 
      err_sumry_fifo1_ren   : out STD_LOGIC ;

      err_sumry_fifo2_empty : in  STD_LOGIC ;
      err_sumry_fifo2_data  : in  STD_LOGIC_VECTOR(47 DOWNTO 0) ; 
      err_sumry_fifo2_ren   : out STD_LOGIC ;

      count_fifo1_empty     : in  STD_LOGIC ;
      count_fifo1_data      : in  STD_LOGIC_VECTOR(13 DOWNTO 0) ;
      count_fifo1_ren       : out STD_LOGIC ;

      count_fifo2_empty     : in  STD_LOGIC ;
      count_fifo2_data      : in  STD_LOGIC_VECTOR(13 DOWNTO 0) ;
      count_fifo2_ren       : out STD_LOGIC ;
      
      halt_output           : in  STD_LOGIC ;  -- gatherer_halt_output (back pressure from S-Link)

      out_mem1_play         : out STD_LOGIC ;
      out_mem1_num_words    : out STD_LOGIC_VECTOR(13 DOWNTO 0) ;
      out_mem1_done         : in  STD_LOGIC ;
      
      out_mem2_play         : out STD_LOGIC ;
      out_mem2_num_words    : out STD_LOGIC_VECTOR(13 DOWNTO 0) ;
      out_mem2_done         : in  STD_LOGIC ;

      ev_header_data_out    : out STD_LOGIC_VECTOR(31 DOWNTO 0) ;  -- where does this go?
      ev_header_dv_out_p1   : out STD_LOGIC ;
		ev_header_kword_out	 : out STD_LOGIC ; -- Marius Wensing, indicating BOF or EOF

      source_id             : in  STD_LOGIC_VECTOR(31 DOWNTO 0) ;
      format_version        : in  STD_LOGIC_VECTOR(31 DOWNTO 0) ;
      run_number            : in  STD_LOGIC_VECTOR(31 DOWNTO 0) ;
		fmt_link_enabled		 : in  std_logic_vector( 7 downto 0) ;
      mask_boc_err_in       : in  STD_LOGIC ;
      mask_tim_err_in       : in  STD_LOGIC ;
--      sweeper_event_out     : out STD_LOGIC ;  -- commented out for now
--      sync_event_error_out  : out STD_LOGIC ;  -- entirely unused should be taken out 
      hold_next_event_in    : in  STD_LOGIC ;
      tim_bcid_in_rol_in    : in  STD_LOGIC
--      hv_flag_set_in        : in  STD_LOGIC_VECTOR(3 DOWNTO 0)
      );
END gen_fragment ;

architecture rtl of gen_fragment is

-------------------------------------------------------------------------------
--SIGNAL DECLARATION
-------------------------------------------------------------------------------

  signal start_frag_xmit          : STD_LOGIC; 
  signal xoff_d1                  : STD_LOGIC ; -- halt_output -- set to 0 for now need to change
  signal ev_header_data_out_p1    : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal data_valid_out_p2        : STD_LOGIC ;
  signal data_kword_out_p2        : STD_LOGIC ;   -- MWE: kword indicator
  signal header_ev_id_data_d1     : STD_LOGIC_VECTOR(63 DOWNTO 0) ;
  signal header_ev_id_ren_i       : STD_LOGIC ;
--signal load_fifo_data           : STD_LOGIC ;
	signal load_err_sumry_fifo1_i		: STD_LOGIC ; -- SPC
	signal load_err_sumry_fifo2_i		: STD_LOGIC ; -- SPC
	signal load_count_fifo1_i			: STD_LOGIC ; -- SPC
	signal load_count_fifo2_i			: STD_LOGIC ; -- SPC

  type state_typedef IS (
    idle, 
    wait_for_ev_id_data,
    test_event_type,
    send_event_fragment,
    header0, 
    header1, 
    header2, 
    header3, 
    header4, 
    header5, 
    header6, 
    header7, 
    header8, 
    header9, 
    out_mem1, 
    out_mem2, 
    rol_test_data, 
    trailer0, 
    trailer1, 
    trailer2, 
    trailer3, 
    trailer4, 
    trailer5  
    );
  signal gen_fragment_state       : state_typedef;

  signal num_data_words           : UNSIGNED (14 DOWNTO 0); -- combine two 14-bit output fifo count to get a 15-bit count
  signal num_err_words            : UNSIGNED (15 DOWNTO 0);
  signal err_sumry_word           : STD_LOGIC_VECTOR(31 DOWNTO 0);
  signal err_sumry_fifo1_empty_d1 : STD_LOGIC;
  signal err_sumry_fifo2_empty_d1 : STD_LOGIC;
  signal header_ev_id_empty_d1    : STD_LOGIC;
  signal count_fifo1_empty_d1     : STD_LOGIC;
  signal count_fifo2_empty_d1     : STD_LOGIC;
  signal error_in_event           : STD_LOGIC;
  signal rol_block_count_i        : UNSIGNED(6 DOWNTO 0);

-------------------------------------------------------------------------------
--COMPONENT DECLARATION
-------------------------------------------------------------------------------

BEGIN  --  Main Body of VHDL code

-------------------------------------------------------------------------------
--COMPONENT INSTANTIATION
-------------------------------------------------------------------------------

	register_data : process (clk, rst)
	begin -- register_data
		if (rst = '1') then
			err_sumry_fifo1_empty_d1 <= '1';
			err_sumry_fifo2_empty_d1 <= '1';
			header_ev_id_empty_d1    <= '1';
			count_fifo1_empty_d1     <= '1';
			count_fifo2_empty_d1     <= '1';
			ev_header_dv_out_p1      <= '0';
			xoff_d1                  <= '0';
			ev_header_data_out       <= (others => '0');
			header_ev_id_data_d1     <= (others => '0');
		elsif (clk'event and clk = '1') then
			err_sumry_fifo1_empty_d1 <= err_sumry_fifo1_empty;
			err_sumry_fifo2_empty_d1 <= err_sumry_fifo2_empty;
			header_ev_id_empty_d1    <= header_ev_id_empty;
			count_fifo1_empty_d1     <= count_fifo1_empty;
			count_fifo2_empty_d1     <= count_fifo2_empty;
			ev_header_dv_out_p1      <= data_valid_out_p2;
			ev_header_kword_out      <= data_kword_out_p2;    -- MWE: kword indicator
			xoff_d1                  <= halt_output;
			ev_header_data_out       <= ev_header_data_out_p1;
			header_ev_id_data_d1     <= header_ev_id_data;
		end if;
	end process register_data;

	register_control_signals : process (clk, rst)
	begin -- register data for control signals
		if (rst = '1') then
			header_ev_id_ren    <= '0';
			err_sumry_fifo1_ren <= '0';
			err_sumry_fifo2_ren <= '0';
			count_fifo1_ren     <= '0';
			count_fifo2_ren     <= '0';
			num_err_words       <= (others => '0');
			err_sumry_word      <= (others => '0');
			num_data_words      <= (others => '0');
		elsif (clk'event and clk = '1') then
			header_ev_id_ren    <= header_ev_id_ren_i;
			err_sumry_fifo1_ren <= load_err_sumry_fifo1_i;
			err_sumry_fifo2_ren <= load_err_sumry_fifo2_i;   
			count_fifo1_ren     <= load_count_fifo1_i;
			count_fifo2_ren     <= load_count_fifo2_i;
	--    num_err_words       <= unsigned('0' & err_sumry_fifo1_data (14 downto 0)) +
	--                           unsigned('0' & err_sumry_fifo2_data (14 downto 0)); -- pad one 0 to prevent addition overflow
	-- SPC 
			if(fmt_link_enabled(7 downto 4) = X"0" AND fmt_link_enabled(3 downto 0) /= X"0")then
				num_err_words       <= unsigned('0' & err_sumry_fifo1_data(14 downto 0));
				num_data_words <= unsigned('0' & count_fifo1_data(13 downto 0));
			elsif(fmt_link_enabled(7 downto 4) /= X"0" AND fmt_link_enabled(3 downto 0) = X"0")then
				num_err_words       <= unsigned('0' & err_sumry_fifo2_data(14 downto 0));
				num_data_words <= unsigned('0' & count_fifo2_data(13 downto 0));
			else
				num_err_words		<= unsigned('0' & err_sumry_fifo1_data(14 downto 0)) +
															unsigned('0' & err_sumry_fifo2_data(14 downto 0));
				num_data_words 	<= unsigned('0' & count_fifo1_data(13 downto 0)) +
															unsigned('0' & count_fifo2_data(13 downto 0));
			end if;
			for i in 0 to 31 loop
	-- err_sumry_fifo_data(47:16) <= err_sumry_flags(31:0)
			   err_sumry_word(i) <= err_sumry_fifo1_data(i+16) OR err_sumry_fifo2_data(i+16);
			end loop;
	--    num_data_words <= unsigned('0' & count_fifo1_data(13 downto 0)) +
	--                      unsigned('0' & count_fifo2_data(13 downto 0)); -- pad one 0 to prevent adddition overflow lost


		end if;    
	end process register_control_signals;

	out_mem1_num_words <= count_fifo1_data(13 downto 0);
	out_mem2_num_words <= count_fifo2_data(13 downto 0);

	gen_ev_fragment : process (clk, rst)
	variable wait_count : unsigned(2 downto 0);
	variable word_count : natural range 0 to 32;
	variable rol_block_size : unsigned(11 downto 0);
	begin -- gen_ev_fragment
		if (rst = '1') then
			gen_fragment_state <= idle;
			start_frag_xmit <= '0'; 
			header_ev_id_ren_i <= '0';
			data_valid_out_p2 <= '0';
			data_kword_out_p2 <= '0';   -- MWE: kword indicator
			ev_header_data_out_p1 <= (others => '0');
			out_mem1_play <= '0';
			out_mem2_play <= '0';
	--    load_fifo_data <= '0';
			load_err_sumry_fifo1_i <= '0'; -- SPC
			load_err_sumry_fifo2_i <= '0'; -- SPC
			load_count_fifo1_i <= '0'; -- SPC
			load_count_fifo2_i <= '0'; -- SPC
			efb_flush_data <= '0';
	--  error_sumry_out <= (others => '0');
	--    sweeper_event_out <= '0';
	--    sync_event_error_out <= '0';
			error_in_event <= '0';
			wait_count := (others => '0');
			rol_block_count_i <= (others => '0');
			rol_block_size := (others => '0');
		elsif (clk'event AND clk = '1') then
			header_ev_id_ren_i <= '0';
			out_mem1_play <= '0';
			out_mem2_play <= '0';     
	--    load_fifo_data <= '0';
			load_err_sumry_fifo1_i <= '0'; -- SPC
			load_err_sumry_fifo2_i <= '0'; -- SPC
			load_count_fifo1_i <= '0'; -- SPC
			load_count_fifo2_i <= '0'; -- SPC
			start_frag_xmit <= NOT header_ev_id_empty_d1;
			efb_flush_data <= '0';
			data_kword_out_p2 <= '0';   -- MWE: by default no kword
	--    sweeper_event_out <= '0';
	--    sync_event_error_out <= '0';

			case gen_fragment_state is
				when idle => -- SPC
					if(start_frag_xmit = '1' AND hold_next_event_in = '0') then -- currently hold next event is always 0
						if(fmt_link_enabled(7 downto 4) = X"0" 	AND 
							  fmt_link_enabled(3 downto 0) /= X"0" AND
							  err_sumry_fifo1_empty_d1 = '0' 		AND 
							  count_fifo1_empty_d1 = '0') then
							load_err_sumry_fifo1_i <= '1';
							load_count_fifo1_i <= '1';
							gen_fragment_state <= wait_for_ev_id_data;
							header_ev_id_ren_i <= '1';
						elsif(fmt_link_enabled(7 downto 4) /= X"0" 	AND 
									fmt_link_enabled(3 downto 0) = X"0" 	AND
									err_sumry_fifo2_empty_d1 = '0' 				AND 
									count_fifo2_empty_d1 = '0')then
							load_err_sumry_fifo2_i <= '1';
							load_count_fifo2_i <= '1';
							gen_fragment_state <= wait_for_ev_id_data;
							header_ev_id_ren_i <= '1';							
						elsif(err_sumry_fifo1_empty_d1 = '0' AND 
									err_sumry_fifo2_empty_d1 = '0' AND
									count_fifo1_empty_d1 = '0'     AND
									count_fifo2_empty_d1 = '0')then
							load_err_sumry_fifo1_i <= '1';
							load_err_sumry_fifo2_i <= '1';
							load_count_fifo1_i <= '1';
							load_count_fifo2_i <= '1';
							gen_fragment_state <= wait_for_ev_id_data;
							header_ev_id_ren_i <= '1';							
						end if;
						-- Bug Fix SPC 03/18/2104
						--gen_fragment_state <= wait_for_ev_id_data;
						--header_ev_id_ren_i <= '1';     -- cleared on next clk, back to default
					else
						data_valid_out_p2 <= '0';
					end if;          
					wait_count := (others => '0');
					rol_block_count_i <= (others => '0');
					rol_block_size := (others => '0');

				when wait_for_ev_id_data =>
					if (wait_count = 2) then--if (wait_count = 2) then
						gen_fragment_state <= test_event_type;
					else
						wait_count := wait_count + 1; -- wait 2 clock cycles then test_event_type. 
					end if;
						
				when test_event_type => 
	--        if (header_ev_id_data_d1(55 downto 48) = "00000111") then  --ATLAS event type
	--          sweeper_event_out <= '1';
	----        elsif (header_ev_id_data_d1(55 downto 51) = "01110") then
	----          if (header_ev_id_data_d1(50 downto 48) /= header_ev_id_data(2 downto 0)) then
	----            sync_event_error_out <= '1';
	----          end if;
	--        end if;
					if (num_err_words > 0) then
						error_in_event <= '1';
					else
						error_in_event <= '0';
					end if;		    
					gen_fragment_state <= send_event_fragment;

				when send_event_fragment =>
					if (xoff_d1 = '1') then  -- back pressure applied
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header0;
					end if;

				when header0 => -- Beginning of fragment , 0xB0F00000
					wait_count := (others => '0');
					ev_header_data_out_p1 <=  X"B0F" & X"00000";-- "00"                      &  -- "B0F"
																		--header_ev_id_data_d1(34)           &  -- ROL Test Block Enable
																		--error_in_event                     &  -- ERR Detect
																		--header_ev_id_data_d1(63 downto 58) &  -- ROD TT
																		--header_ev_id_data_d1(57 downto 56) &  -- TIM TT
																		--header_ev_id_data_d1(55 downto 48);   -- Atlas TT
				 data_kword_out_p2 <= '1';   -- BOF is a kword
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1'; -- send vaild
						gen_fragment_state <= header1;
					end if;

				when header1 => -- Start of Header Marker, 0xee1234ee
					ev_header_data_out_p1 <= X"EE1234EE";
	--      ev_header_data_out_p1 <= err_sumry_word;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header2;
					end if;        

				when header2 => -- Header Size, 9 words
					ev_header_data_out_p1 <= X"00000009"; 
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header3;
					end if;

				when header3 => -- Format Version Num
					ev_header_data_out_p1 <= format_version;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header4;
					end if;        

				when header4 => -- Source ID
					ev_header_data_out_p1(31 downto 2) 	<= source_id(31 downto 2);
					ev_header_data_out_p1(1 downto 0)	<= efb_engine_id;	-- == slave_id & efb_id
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header5;
					end if;          

				when header5 => -- Run Number
					ev_header_data_out_p1 <= run_number;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header6;
					end if;          

	--  header_ev_id_data(31:24) ==> ECRID
	--  header_ev_id_data(23: 0) ==> L1ID
				when header6 => -- L1 ID
					ev_header_data_out_p1 <= header_ev_id_data_d1(31 downto 0);

					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header7;
					end if;              

	--  header_ev_id_data(47:36) ==> BCID
				when header7 => -- BC ID
						-- readout link test trigger h_e_id(34)
					if (header_ev_id_data_d1(34) = '1' AND tim_bcid_in_rol_in = '0') then
						ev_header_data_out_p1 <= X"0000BC1D"; -- 0xBC1D
					else
						ev_header_data_out_p1 <= X"00000" &
																		 header_ev_id_data_d1 (47 downto 36);
					end if;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header8;
					end if;         

	--  header_ev_id_data(55:48) ==> ATLAS TT
				when header8 => -- ATLAS Specific Trig Type (TTC)
					ev_header_data_out_p1 <= X"000000" &
																	 header_ev_id_data_d1 (55 downto 48);
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= header9;
					end if;          

	--  header_ev_id_data(63:58) ==> ROD TT
	--  header_ev_id_data(57:56) ==> TIM TT
				when header9 => -- Detector Specific Event Type
					ev_header_data_out_p1 <= "0000000000" &
																	 header_ev_id_data_d1 (63 downto 58) &
																	 "00000000000000" &
																	 header_ev_id_data_d1 (57 downto 56);
					data_valid_out_p2 <= '0';

					if (xoff_d1 = '0') then
						if (header_ev_id_data_d1(34) = '1') then
							data_valid_out_p2 <= '1';
							gen_fragment_state <= rol_test_data;
						else
							if(fmt_link_enabled(3 downto 0) = X"0")then -- SPC
								gen_fragment_state <= out_mem2;
							else
								gen_fragment_state <= out_mem1;
							end if;


						end if;
					end if;

				when out_mem1 => -- Send Data from EFB # 1
					out_mem1_play <= '1';
					if(out_mem1_done = '1' OR fmt_link_enabled(3 downto 0) = "0000") then -- plays the out_mem1 until done
						if(fmt_link_enabled(7 downto 4) = "0000") then
							gen_fragment_state <= trailer0;
						else
							gen_fragment_state <= out_mem2;
						end if;
						out_mem1_play <= '0';
					end if;

				when out_mem2 => -- Send Data from EFB # 2
				 out_mem2_play <= '1';
				 if(out_mem2_done = '1' OR fmt_link_enabled(7 downto 4) = "0000") then -- plays the output_mem2 until done
						gen_fragment_state <= trailer0;
						out_mem2_play <= '0';
						data_valid_out_p2 <= '1'; -- set data valid out signal for trailer0					
				 end if;

	--      when out_mem1 => -- Send Data from EFB # 1
	--			-- DAV
	--        if(out_mem1_done = '1' OR fmt_link_enabled(3 downto 0) = "0000") then -- plays the out_mem1 until done
	--					gen_fragment_state <= out_mem2;
	--					out_mem1_play <= '0';
	--				else
	--					out_mem1_play <= '1';
	--        end if;
	--
	--      when out_mem2 => -- Send Data from EFB # 2
	--			 -- DAV
	--        if(out_mem2_done = '1' OR fmt_link_enabled(7 downto 4) = "0000") then -- plays the output_mem2 until done
	--					gen_fragment_state <= trailer0;
	--					data_valid_out_p2 <= '1'; -- set data valid out signal for trailer0
	--					out_mem2_play <= '0';
	--				else
	--					out_mem2_play <= '1';
	--        end if;

				when rol_test_data =>
					for i in 0 to 31 loop
						ev_header_data_out_p1(i) <= '0';  -- 32 consecutive no data (of zeros) output
					end loop;  
					ev_header_data_out_p1(word_count) <= '1';
					if (xoff_d1 = '1') then -- back pressure from S-Link
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						rol_block_size := rol_block_size + 1 ;
						if (word_count < 31) then
							word_count := word_count + 1 ;
						elsif (word_count = 31) then
							if (header_ev_id_data_d1 (63 downto 58) = std_logic_vector(rol_block_count_i(5 downto 0))) then -- ROD TT
								gen_fragment_state <= trailer0;
							else
								rol_block_count_i <= rol_block_count_i + 1 ;
							end if;
							word_count := 0;
						end if;
					end if;           

				when trailer0 => -- Error Status Word
					ev_header_data_out_p1 <= err_sumry_word;
	--      error_sumry_out <= err_sumry_word;  -- Output to global error counters
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= trailer1;
					end if;           

				when trailer1 => -- Error Count Word
					if (header_ev_id_data_d1(34) = '1') then
						ev_header_data_out_p1 <= X"00040000"; -- indicates ROL Test Block
					else
						ev_header_data_out_p1 <=  "00000000"        -- [31:24]
	--                                  & hv_flag_set_in    -- [23:20]
																		& "0000"
																		& "00"              -- [19:18] 
																		& (header_ev_id_data_d1(33) AND NOT mask_boc_err_in) -- (17)
																		& (header_ev_id_data_d1(32) AND NOT mask_tim_err_in) -- (16)
																		& std_logic_vector(num_err_words);
					end if;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= trailer2;
					end if;
				
				when trailer2 => -- Number of status words  = 2
					ev_header_data_out_p1 <= X"00000002";
	--      error_sumry_out <= (others => '0');
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= trailer3;
					end if;
				
				when trailer3 => -- Number of data words
					if (header_ev_id_data_d1(34) = '1') then
						ev_header_data_out_p1 <= "00000000000000000000" & std_logic_vector(rol_block_size);
					else
						ev_header_data_out_p1 <= "00000000000000000" & std_logic_vector(num_data_words);
					end if;
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= trailer4;
					end if;
				
				when trailer4 => -- status block position
					ev_header_data_out_p1 <= X"00000001";
					if (xoff_d1 = '1') then
						data_valid_out_p2 <= '0';
					else
						data_valid_out_p2 <= '1';
						gen_fragment_state <= trailer5;
					end if;
				
				when trailer5 =>  -- end of fragment
					ev_header_data_out_p1 <= X"E0F00000";
					data_valid_out_p2 <= '0';
				data_kword_out_p2 <= '1';   -- EOF is a kword
					gen_fragment_state <= idle;
				
				when others => gen_fragment_state <= idle;
			
			end case;
		end if;
	end process gen_ev_fragment;

END rtl;

