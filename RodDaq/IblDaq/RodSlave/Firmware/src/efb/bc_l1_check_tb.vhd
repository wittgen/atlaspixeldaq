--------------------------------------------------------------------------------
-- Company:  University of Washington
-- Engineer:  Bing (Shaw-Pin) Chen
--
-- Create Date:   13:38:25 06/26/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/CERN/efb_bing/ibl_efb/bc_l1_check_tb.vhd
-- Project Name:  ibl_efb
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bc_l1_check
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bc_l1_check_tb IS
END bc_l1_check_tb;
 
ARCHITECTURE behavior OF bc_l1_check_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT bc_l1_check
    PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         data_in : IN  std_logic_vector(34 downto 0);
         data_valid_in_n : IN  std_logic_vector(3 downto 0);
         form_enabled_n : IN  std_logic_vector(3 downto 0);
         data_out : OUT  std_logic_vector(42 downto 0);
         data_valid_out : OUT  std_logic;
         ev_fifo_ren : OUT  std_logic;
         ev_fifo_empty : IN  std_logic;
         ev_fifo_empty_error : OUT  std_logic;
         ev_fifo_data : IN  std_logic_vector(26 downto 0);
         efb_engine_id : IN  std_logic;
         gatherer_id : IN  std_logic;
         bcid_offset : IN  std_logic_vector(9 downto 0);
         grp_evt_count_out : OUT  std_logic_vector(31 downto 0);
         grp_evt_enable_in : IN  std_logic;
         l1_id_fifo_data : OUT  std_logic_vector(19 downto 0);
         l1_id_fifo_addr : IN  std_logic_vector(3 downto 0);
         enable_l1_trap_in : IN  std_logic;
         l1_trap_full_out : OUT  std_logic;
         latched_l1id : OUT  std_logic_vector(4 downto 0);
         latched_bcid : OUT  std_logic_vector(9 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal data_in : std_logic_vector(34 downto 0) := (others => '0');
   signal data_valid_in_n : std_logic_vector(3 downto 0) := (others => '0');
   signal form_enabled_n : std_logic_vector(3 downto 0) := (others => '0');
   signal ev_fifo_empty : std_logic := '0';
   signal ev_fifo_data : std_logic_vector(26 downto 0) := (others => '0');
   signal efb_engine_id : std_logic := '0';
   signal gatherer_id : std_logic := '0';
   signal bcid_offset : std_logic_vector(9 downto 0) := (others => '0');
   signal grp_evt_enable_in : std_logic := '0';
   signal l1_id_fifo_addr : std_logic_vector(3 downto 0) := (others => '0');
   signal enable_l1_trap_in : std_logic := '0';

 	--Outputs
   signal data_out : std_logic_vector(42 downto 0);
   signal data_valid_out : std_logic;
   signal ev_fifo_ren : std_logic;
   signal ev_fifo_empty_error : std_logic;
   signal grp_evt_count_out : std_logic_vector(31 downto 0);
   signal l1_id_fifo_data : std_logic_vector(19 downto 0);
   signal l1_trap_full_out : std_logic;
   signal latched_l1id : std_logic_vector(4 downto 0);
   signal latched_bcid : std_logic_vector(9 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: bc_l1_check PORT MAP (
          clk => clk,
          rst => rst,
          data_in => data_in,
          data_valid_in_n => data_valid_in_n,
          form_enabled_n => form_enabled_n,
          data_out => data_out,
          data_valid_out => data_valid_out,
          ev_fifo_ren => ev_fifo_ren,
          ev_fifo_empty => ev_fifo_empty,
          ev_fifo_empty_error => ev_fifo_empty_error,
          ev_fifo_data => ev_fifo_data,
          efb_engine_id => efb_engine_id,
          gatherer_id => gatherer_id,
          bcid_offset => bcid_offset,
          grp_evt_count_out => grp_evt_count_out,
          grp_evt_enable_in => grp_evt_enable_in,
          l1_id_fifo_data => l1_id_fifo_data,
          l1_id_fifo_addr => l1_id_fifo_addr,
          enable_l1_trap_in => enable_l1_trap_in,
          l1_trap_full_out => l1_trap_full_out,
          latched_l1id => latched_l1id,
          latched_bcid => latched_bcid
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 25 ns.
		form_enabled_n <= "1111";
      wait for 25 ns;	
      
		efb_engine_id <= '0';
		gatherer_id <= '1';
		bcid_offset <= "1100110011";
		grp_evt_enable_in <= '0'; -- group event counter inactive
		enable_l1_trap_in <= '0'; -- the trap is inactive
		l1_id_fifo_addr <= X"C"; -- just random read for now, test for real later
		ev_fifo_empty <= '0';
		
		-- event fifo data can be constant as data words come in
--			ev_fifo_data1[9:0] BCID(10)
--			ev_fifo_data1[14:10] L1ID(5)
--			ev_fifo_data1[18:15] ROD Trigger Type(4)
--			ev_fifo_data2[26:19] Dynamic Mask # (8) supports links 4 to 7 with 2-bits each
--			- L1 Offset Link 0/4 <= ev_fifo_data (20 downto 19)
--			- L1 Offset Link 1/5 <= ev_fifo_data (22 downto 21)
--			- L1 Offset Link 2/6 <= ev_fifo_data (24 downto 23)
--			- L1 Offset Link 3/7 <= ev_fifo_data (26 downto 25)
      -- no link offset, neglect trigger type
		ev_fifo_data <= "00000000" & "0000" & "11001" & "1110011100";
		
		-- for this test all data valid
      data_valid_in_n <= X"0";      
		
		rst <= '1'; wait for 10 ns;
      rst <= '0'; wait for 10 ns;
		
		-- Formatter will sends data in following order when at least one link is valid,
      -- Only at most one form_enabled can be on at any given time.
		-- note: test data doesn't use accurate lunk# in the data_in
		form_enabled_n <= "1110"; -- 0001
      data_in <= "000" & X"20001234"; wait for 10 ns; -- header
		data_in <= "000" & X"80aa0011"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
		
		form_enabled_n <= "1101";
      data_in <= "000" & X"20005678"; wait for 10 ns; -- header
		data_in <= "000" & X"80bb2233"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer

		form_enabled_n <= "1011";
      data_in <= "000" & X"20009abc"; wait for 10 ns; -- header
		data_in <= "000" & X"80cc4455"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
 
		form_enabled_n <= "0111";
      data_in <= "000" & X"2000def0"; wait for 10 ns; -- header
		data_in <= "000" & X"80dd6677"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer

      data_in <= "000" & X"200" & "00100011100010011111"; wait for 10 ns; -- header
		data_in <= "000" & X"80ee8899"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
		form_enabled_n <= "1101";
      data_in <= "000" & X"200" & "00100011110010011111"; wait for 10 ns; -- header
		data_in <= "000" & X"80ff1122"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
		form_enabled_n <= "1011";		
      data_in <= "000" & X"200" & "00100011100010111111"; wait for 10 ns; -- header
		data_in <= "000" & X"80113344"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
		form_enabled_n <= "0111";		
      data_in <= "000" & X"200" & "00100010110101101110"; wait for 10 ns; -- header
		data_in <= "000" & X"80113344"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer

      -- try out the l1id resync and bcid check
		ev_fifo_data <= "01010101" & "0000" & "01100" & "0010011101";  -- expected_li <= "01110"
		
		form_enabled_n <= X"F"; wait for 50 ns;
		form_enabled_n <= "1110";
		
		-- all headers include 2 skiped events
		
		-- no bc l1 errors (passed)
      data_in <= "000" & X"200" & "00100011100010011111"; wait for 10 ns; -- header
		data_in <= "000" & X"80ee8899"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer

      -- l1 error (passed)
		form_enabled_n <= "1101";
      data_in <= "000" & X"200" & "00100011110010011111"; wait for 10 ns; -- header
		data_in <= "000" & X"80ff1122"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
      
		-- bc error (passed)
		form_enabled_n <= "1011";		
      data_in <= "000" & X"200" & "00100011100010111111"; wait for 10 ns; -- header
		data_in <= "000" & X"80113344"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer
      
		-- bc and l1 errors (passed)
		form_enabled_n <= "0111";		
      data_in <= "000" & X"200" & "00100010110101101110"; wait for 10 ns; -- header
		data_in <= "000" & X"80113344"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer

		-- bc errors, see end of event (passed)
		form_enabled_n <= "1110"; 
      data_in <= "000" & X"200" & "00100011100010011001"; wait for 10 ns; -- header
		data_in <= "000" & X"80ee8899"; wait for 10 ns; -- hit
		data_in <= "000" & X"1000f125"; wait for 10 ns; -- error flags
		data_in <= "000" & X"4023f2cd"; wait for 10 ns; -- trailer





      wait;
   end process;

END;
