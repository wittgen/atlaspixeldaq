----------------------------------------------------------------------------
-- UW EE/PHYSICS- WASHINGTON
-- ATLAS ROD ELECTRONICS
----------------------------------------------------------------------------
-- Filename:
--    format_data.vhd
-- Originally by John Joseph
-- Modified by Bing (Shaw-Pin) Chen on 06.19.2013
-- Description:
--    ROD Dual Gatherer
--    IBL Version
--    Performs final data formatting, removes events with no data,
--    and counts how many words are in the output memories for the
--    given event fragment. Simply passes on the incoming data and 
--    give a count of data in fifo (connected to the output)
--
--
--    INPUT DATA FORMAT: data_in[41:0]
--      data_out[31:0] event data
--      data_out[35:32] link address
--      data_out[36] timeout error 
--      data_out[37] condensed mode 
--      data_out[38] L1ID error     
--      data_out[39] BCID error      
--      data_out[40] Link masked by PPC
--      data_out[41] end of event fragment
-- 
--    OUTPUT DATA FORMAT: data_out[41:0]
--      data_out[31:0] event data
--      data_out[35:32] link address        (later to be present in header word only) 
--      data_out[36] timeout error          (later to be present in header word only) 
--      data_out[37] condensed mode         (later to be present in header word only)
--      data_out[38] L1ID error             (present in header word only)
--      data_out[39] BCID error             (present in header word only)
--      data_out[40] Link masked by PPC     (later to be present in header word only)
--      data_out[41] end of event fragment  (later to be present in header word only)
--
--
----------------------------------------------------------------------------

library IEEE ;
use IEEE.STD_LOGIC_1164.all ;
use IEEE.std_logic_arith.all;

----------------------------------------------------------------------------
--PORT DECLARATION
----------------------------------------------------------------------------

ENTITY format_data IS
  port (
    clk              : in  STD_LOGIC ;
    rst              : in  STD_LOGIC ;
    data_in          : in  STD_LOGIC_VECTOR(41 DOWNTO 0) ;
    data_valid_in    : in  STD_LOGIC ;
    data_out         : out STD_LOGIC_VECTOR(41 DOWNTO 0) ;
    data_valid_out_n : out STD_LOGIC ;
    count_fifo_wen   : out STD_LOGIC ;
    count_fifo_data  : out STD_LOGIC_VECTOR(13 DOWNTO 0) -- need to change size later
    -- rod_type         : in  STD_LOGIC
    );
END format_data ;

architecture rtl of format_data is

----------------------------------------------------------------------------
--SIGNAL DECLARATION
----------------------------------------------------------------------------

signal data_in_d1              : STD_LOGIC_VECTOR(41 DOWNTO 0); 
signal data_valid_in_d1        : STD_LOGIC ;
signal last_word               : STD_LOGIC ;
-- signal lsw_valid               : STD_LOGIC ;
-- signal msw_valid               : STD_LOGIC ;
-- signal extra_data              : STD_LOGIC_VECTOR(15 downto 0);
-- signal extra_halfword          : STD_LOGIC ;
signal data_out_p2             : STD_LOGIC_VECTOR(41 downto 0);
signal data_valid_out_p2       : STD_LOGIC ;
signal last_word_flush         : STD_LOGIC ;
signal data_out_p1             : STD_LOGIC_VECTOR(41 downto 0);
signal data_valid_out_p1       : STD_LOGIC ;
signal count_fifo_wen_i        : STD_LOGIC ;
signal word_count              : UNSIGNED (13 DOWNTO 0); -- need to be changed later

BEGIN  --  Main Body of VHDL code

-- modified (completed)
register_data : process (clk, rst)
  begin
    if (rst = '1') then
      data_in_d1 <= (others => '0');
      data_valid_in_d1 <= '0';
      last_word <= '0';
    elsif (clk'event and clk = '1') then
      data_in_d1 <= data_in;
      data_valid_in_d1 <= data_valid_in;
      last_word <= data_in(41); -- end of event flag
    end if;
  end process register_data;

-- the below process is ommitted since it only applies to SCT
-- mark_valid_halfwords : process (clk, rst)

-- modified (completed)
repack_data : process (clk, rst)
  begin
    if (rst = '1') then
     -- extra_halfword     <= '0';
     -- extra_data         <= (others => '0');
      data_out_p2        <= (others => '0');
      data_valid_out_p2  <= '0';
      last_word_flush    <= '0';
    elsif (clk'event and clk = '1') then
      -- data_valid_out_p2  <= '0';
      last_word_flush    <= last_word;
      data_out_p2       <= data_in_d1;
      data_valid_out_p2 <= data_valid_in_d1;
    end if;
  end process repack_data;
  
-- modified (completed)
send_out_and_count : process (clk, rst)
  begin
    if (rst = '1') then
      data_out_p1       <= (others => '0');
      data_out          <= (others => '0');
      data_valid_out_p1 <= '0';
      data_valid_out_n  <= '1';
      count_fifo_wen_i  <= '0';
      word_count        <= (others => '0');
    elsif (clk'event and clk = '1') then
      data_out_p1       <= data_out_p2;
      data_out          <= data_out_p1;  -- one clock delay
      data_valid_out_p1 <= data_valid_out_p2;
      data_valid_out_n  <= NOT data_valid_out_p1;  -- one clock delay
      count_fifo_wen_i  <= last_word_flush;
		-- counts the number of words in each event data and "flushes" (resets to 1) when 
		-- reaches the end of a word
		-- in testbench, the very first data packet is counted one extra. 
      if (count_fifo_wen_i = '1' AND data_valid_out_p2 = '1') then
        word_count <= "00000000000001"; -- resets for the next data packet
      elsif (count_fifo_wen_i = '1' AND data_valid_out_p2 = '0') then
        word_count <= "00000000000000"; -- no valid data means no words
      elsif (count_fifo_wen_i = '0' AND data_valid_out_p2 = '1') then
        word_count <= word_count + 1;
      end if;        
    end if;
  end process send_out_and_count;
    
count_fifo_wen <= count_fifo_wen_i;
count_fifo_data <= std_logic_vector (word_count);

END rtl;
