--------------------------------------------------------------------------------
-- Company: University of Washington, Seattle
-- Engineer: Shaw-Pin (Bing) Chen
--
-- Create Date:   20:34:34 11/11/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/ibl_rod_fw/ibl_rodSlave/ibl_slv_register/ibl_slv_register_tb.vhd
-- Project Name:  ibl_slv_register
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: ibl_slv_register
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.std_logic_arith.ALL;
use IEEE.std_logic_unsigned.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

ENTITY ibl_slv_register_tb IS
END ibl_slv_register_tb;
 
ARCHITECTURE behavior OF ibl_slv_register_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
	COMPONENT ibl_slv_register
		PORT(
         clk : IN  std_logic;
         rst : IN  std_logic;
         rod_bus_strb_in : IN  std_logic;
         rod_bus_addr : IN  std_logic_vector(15 downto 0);
         rod_bus_rnw : IN  std_logic;
         rod_bus_hwsb_in : IN  std_logic;
         rod_bus_ds_in : IN  std_logic;
         rod_bus_data_in : IN  std_logic_vector(15 downto 0);
         rod_bus_data_out : OUT  std_logic_vector(15 downto 0);
         rod_bus_ack_out : OUT  std_logic;
         slave_id : OUT  std_logic;
         fmt_link_enabled : OUT  std_logic_vector(15 downto 0);
         fmt_timeout_limit : OUT  unsigned(31 downto 0);
         fmt_data_overflow_limit : OUT  unsigned(15 downto 0);
         fmt_header_trailer_limit : OUT  unsigned(31 downto 0);
         fmt_rod_busy_limit : OUT  unsigned(31 downto 0);
         fmt_header_trailer_error : IN  std_logic_vector(15 downto 0);
         fmt_time_out_error : IN  std_logic_vector(15 downto 0);
         fmt_data_overflow_error : IN  std_logic_vector(15 downto 0);
         fmt_ctrl_reg : OUT  std_logic_vector(31 downto 0);
         fmt_stts_reg : IN  std_logic_vector(31 downto 0);
         inmem_reset : OUT  std_logic;
         inmem_select : OUT  std_logic_vector(1 downto 0);
         inmem_rden : OUT  std_logic;
         inmem_data : IN  std_logic_vector(11 downto 0);
         err_mask_wen1 : OUT  std_logic;
         err_mask_din1 : OUT  std_logic_vector(31 downto 0);
         err_mask_dout1 : IN  std_logic_vector(31 downto 0);
         err_mask_sel1 : OUT  std_logic_vector(2 downto 0);
         err_mask_wen2 : OUT  std_logic;
         err_mask_din2 : OUT  std_logic_vector(31 downto 0);
         err_mask_dout2 : IN  std_logic_vector(31 downto 0);
         err_mask_sel2 : OUT  std_logic_vector(2 downto 0);
         err_mask_wen3 : OUT  std_logic;
         err_mask_din3 : OUT  std_logic_vector(31 downto 0);
         err_mask_dout3 : IN  std_logic_vector(31 downto 0);
         err_mask_sel3 : OUT  std_logic_vector(2 downto 0);
         err_mask_wen4 : OUT  std_logic;
         err_mask_din4 : OUT  std_logic_vector(31 downto 0);
         err_mask_dout4 : IN  std_logic_vector(31 downto 0);
         err_mask_sel4 : OUT  std_logic_vector(2 downto 0);
         source_id : OUT  std_logic_vector(31 downto 0);
         format_version : OUT  std_logic_vector(31 downto 0);
         run_number : OUT  std_logic_vector(31 downto 0);
         efb1_out_fifo_rst : OUT  std_logic;
         efb1_out_fifo1_empty_n : IN  std_logic;
         efb1_out_fifo1_aempty_n : IN  std_logic;
         efb1_out_fifo1_full_n : IN  std_logic;
         efb1_out_fifo1_afull_n : IN  std_logic;
         efb1_out_fifo1_play_done : IN  std_logic;
         efb1_out_fifo2_empty_n : IN  std_logic;
         efb1_out_fifo2_aempty_n : IN  std_logic;
         efb1_out_fifo2_full_n : IN  std_logic;
         efb1_out_fifo2_afull_n : IN  std_logic;
         efb1_out_fifo2_play_done : IN  std_logic;
         efb2_out_fifo_rst : OUT  std_logic;
         efb2_out_fifo1_empty_n : IN  std_logic;
         efb2_out_fifo1_aempty_n : IN  std_logic;
         efb2_out_fifo1_full_n : IN  std_logic;
         efb2_out_fifo1_afull_n : IN  std_logic;
         efb2_out_fifo1_play_done : IN  std_logic;
         efb2_out_fifo2_empty_n : IN  std_logic;
         efb2_out_fifo2_aempty_n : IN  std_logic;
         efb2_out_fifo2_full_n : IN  std_logic;
         efb2_out_fifo2_afull_n : IN  std_logic;
         efb2_out_fifo2_play_done : IN  std_logic;
         efb1_misc_stts_reg : IN  std_logic_vector(30 downto 0);
         efb2_misc_stts_reg : IN  std_logic_vector(30 downto 0);
         efb1_ev_header_data : IN  std_logic_vector(15 downto 0);
         efb2_ev_header_data : IN  std_logic_vector(15 downto 0);
         efb1_ev_fifo_data1 : IN  std_logic_vector(26 downto 0);
         efb1_ev_fifo_data2 : IN  std_logic_vector(26 downto 0);
         efb2_ev_fifo_data1 : IN  std_logic_vector(26 downto 0);
         efb2_ev_fifo_data2 : IN  std_logic_vector(26 downto 0);
         efb_mask_bcid_error : OUT  std_logic;
         efb_mask_l1id_error : OUT  std_logic;
         efb_bcid_offset : OUT  std_logic_vector(9 downto 0);
         efb_grp_evt_enable : OUT  std_logic_vector(1 downto 0);
         efb1_event_count : IN  std_logic_vector(31 downto 0);
         efb2_event_count : IN  std_logic_vector(31 downto 0);
         mask_boc_clock_err : OUT  std_logic;
         mask_tim_clock_err : OUT  std_logic;
         efb1_l1id_fifo_data : IN  std_logic_vector(39 downto 0);
         efb1_l1id_fifo_addr : OUT  std_logic_vector(3 downto 0);
         efb2_l1id_fifo_data : IN  std_logic_vector(39 downto 0);
         efb2_l1id_fifo_addr : OUT  std_logic_vector(3 downto 0);
         efb_enable_l1_trap : OUT  std_logic_vector(1 downto 0);
         efb_l1_trap_full_in : IN  std_logic_vector(1 downto 0);
         efb1_latched_l1id0 : IN  std_logic_vector(4 downto 0);
         efb1_latched_l1id1 : IN  std_logic_vector(4 downto 0);
         efb1_latched_bcid0 : IN  std_logic_vector(9 downto 0);
         efb1_latched_bcid1 : IN  std_logic_vector(9 downto 0);
         efb2_latched_l1id0 : IN  std_logic_vector(4 downto 0);
         efb2_latched_l1id1 : IN  std_logic_vector(4 downto 0);
         efb2_latched_bcid0 : IN  std_logic_vector(9 downto 0);
         efb2_latched_bcid1 : IN  std_logic_vector(9 downto 0);
         tim_bcid_in_rol : OUT  std_logic;
         bcid_rollover_select : OUT  std_logic
      );
   END COMPONENT;
    	 
   --Inputs
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal rod_bus_strb_in : std_logic := '0';
   signal rod_bus_addr : std_logic_vector(15 downto 0) := (others => '0');
   signal rod_bus_rnw : std_logic := '0';
   signal rod_bus_hwsb_in : std_logic := '0';
   signal rod_bus_ds_in : std_logic := '0';
   signal rod_bus_data_in : std_logic_vector(15 downto 0) := (others => '0');
   signal fmt_header_trailer_error : std_logic_vector(15 downto 0) := (others => '0');
   signal fmt_time_out_error : std_logic_vector(15 downto 0) := (others => '0');
   signal fmt_data_overflow_error : std_logic_vector(15 downto 0) := (others => '0');
   signal fmt_stts_reg : std_logic_vector(31 downto 0) := x"1234abcd";-- (others => '0');
   signal inmem_data : std_logic_vector(11 downto 0) := (others => '0');
   signal err_mask_dout1 : std_logic_vector(31 downto 0) := (others => '0');
   signal err_mask_dout2 : std_logic_vector(31 downto 0) := (others => '0');
   signal err_mask_dout3 : std_logic_vector(31 downto 0) := (others => '0');
   signal err_mask_dout4 : std_logic_vector(31 downto 0) := (others => '0');
   signal efb1_out_fifo1_empty_n : std_logic := '0';
   signal efb1_out_fifo1_aempty_n : std_logic := '0';
   signal efb1_out_fifo1_full_n : std_logic := '0';
   signal efb1_out_fifo1_afull_n : std_logic := '0';
   signal efb1_out_fifo1_play_done : std_logic := '0';
   signal efb1_out_fifo2_empty_n : std_logic := '0';
   signal efb1_out_fifo2_aempty_n : std_logic := '0';
   signal efb1_out_fifo2_full_n : std_logic := '0';
   signal efb1_out_fifo2_afull_n : std_logic := '0';
   signal efb1_out_fifo2_play_done : std_logic := '0';
   signal efb2_out_fifo1_empty_n : std_logic := '0';
   signal efb2_out_fifo1_aempty_n : std_logic := '0';
   signal efb2_out_fifo1_full_n : std_logic := '0';
   signal efb2_out_fifo1_afull_n : std_logic := '0';
   signal efb2_out_fifo1_play_done : std_logic := '0';
   signal efb2_out_fifo2_empty_n : std_logic := '0';
   signal efb2_out_fifo2_aempty_n : std_logic := '0';
   signal efb2_out_fifo2_full_n : std_logic := '0';
   signal efb2_out_fifo2_afull_n : std_logic := '0';
   signal efb2_out_fifo2_play_done : std_logic := '0';
   signal efb1_misc_stts_reg : std_logic_vector(30 downto 0) := (others => '0');
   signal efb2_misc_stts_reg : std_logic_vector(30 downto 0) := (others => '0');
   signal efb1_ev_header_data : std_logic_vector(15 downto 0) := (others => '0');
   signal efb2_ev_header_data : std_logic_vector(15 downto 0) := (others => '0');
   signal efb1_ev_fifo_data1 : std_logic_vector(26 downto 0) := (others => '0');
   signal efb1_ev_fifo_data2 : std_logic_vector(26 downto 0) := (others => '0');
   signal efb2_ev_fifo_data1 : std_logic_vector(26 downto 0) := (others => '0');
   signal efb2_ev_fifo_data2 : std_logic_vector(26 downto 0) := (others => '0');
   signal efb1_event_count : std_logic_vector(31 downto 0) := (others => '0');
   signal efb2_event_count : std_logic_vector(31 downto 0) := (others => '0');
   signal efb1_l1id_fifo_data : std_logic_vector(39 downto 0) := (others => '0');
   signal efb2_l1id_fifo_data : std_logic_vector(39 downto 0) := (others => '0');
   signal efb_l1_trap_full_in : std_logic_vector(1 downto 0) := (others => '0');
   signal efb1_latched_l1id0 : std_logic_vector(4 downto 0) := (others => '0');
   signal efb1_latched_l1id1 : std_logic_vector(4 downto 0) := (others => '0');
   signal efb1_latched_bcid0 : std_logic_vector(9 downto 0) := (others => '0');
   signal efb1_latched_bcid1 : std_logic_vector(9 downto 0) := (others => '0');
   signal efb2_latched_l1id0 : std_logic_vector(4 downto 0) := (others => '0');
   signal efb2_latched_l1id1 : std_logic_vector(4 downto 0) := (others => '0');
   signal efb2_latched_bcid0 : std_logic_vector(9 downto 0) := (others => '0');
   signal efb2_latched_bcid1 : std_logic_vector(9 downto 0) := (others => '0');

 	--Outputs
   signal rod_bus_data_out : std_logic_vector(15 downto 0);
   signal rod_bus_ack_out : std_logic;
   signal slave_id : std_logic;
   signal fmt_link_enabled : std_logic_vector(15 downto 0);
   signal fmt_timeout_limit : unsigned(31 downto 0);
   signal fmt_data_overflow_limit : unsigned(15 downto 0);
   signal fmt_header_trailer_limit : unsigned(31 downto 0);
   signal fmt_rod_busy_limit : unsigned(31 downto 0);
   signal fmt_ctrl_reg : std_logic_vector(31 downto 0);
   signal inmem_reset : std_logic;
   signal inmem_select : std_logic_vector(1 downto 0);
   signal inmem_rden : std_logic;
   signal err_mask_wen1 : std_logic;
   signal err_mask_din1 : std_logic_vector(31 downto 0);
   signal err_mask_sel1 : std_logic_vector(2 downto 0);
   signal err_mask_wen2 : std_logic;
   signal err_mask_din2 : std_logic_vector(31 downto 0);
   signal err_mask_sel2 : std_logic_vector(2 downto 0);
   signal err_mask_wen3 : std_logic;
   signal err_mask_din3 : std_logic_vector(31 downto 0);
   signal err_mask_sel3 : std_logic_vector(2 downto 0);
   signal err_mask_wen4 : std_logic;
   signal err_mask_din4 : std_logic_vector(31 downto 0);
   signal err_mask_sel4 : std_logic_vector(2 downto 0);
   signal source_id : std_logic_vector(31 downto 0);
   signal format_version : std_logic_vector(31 downto 0);
   signal run_number : std_logic_vector(31 downto 0);
   signal efb1_out_fifo_rst : std_logic;
   signal efb2_out_fifo_rst : std_logic;
   signal efb_mask_bcid_error : std_logic;
   signal efb_mask_l1id_error : std_logic;
   signal efb_bcid_offset : std_logic_vector(9 downto 0);
   signal efb_grp_evt_enable : std_logic_vector(1 downto 0);
   signal mask_boc_clock_err : std_logic;
   signal mask_tim_clock_err : std_logic;
   signal efb1_l1id_fifo_addr : std_logic_vector(3 downto 0);
   signal efb2_l1id_fifo_addr : std_logic_vector(3 downto 0);
   signal efb_enable_l1_trap : std_logic_vector(1 downto 0);
   signal tim_bcid_in_rol : std_logic;
   signal bcid_rollover_select : std_logic;
	
   -- Clock period definitions
   constant clk_period : time := 10 ns;
	
 	procedure read_bus(
		address : in std_logic_vector(15 downto 0);
		signal rod_bus_strb_in : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb_in : out  std_logic;
		signal rod_bus_ds_in: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_strb_in <= '1';
		rod_bus_rnw <= '1'; rod_bus_hwsb_in <= '0';
		rod_bus_ds_in <= '0';
	   wait for clk_period*2; rod_bus_ds_in <= '1'; wait for clk_period; rod_bus_ds_in <= '0';
	   rod_bus_hwsb_in <= '1'; wait for clk_period*2;
      rod_bus_ds_in <= '1'; wait for clk_period; rod_bus_ds_in <= '0';		
		wait for clk_period*3;
		rod_bus_hwsb_in <= '0';
		rod_bus_addr <= (others => '0');
      wait for clk_period; rod_bus_strb_in <= '0';	wait for clk_period*2;	
	end read_bus;

   procedure write_bus(
         address : in std_logic_vector(15 downto 0);
			data_in : in std_logic_vector(31 downto 0);
			signal rod_bus_data_in : out std_logic_vector(15 downto 0);
			signal rod_bus_strb_in : out std_logic;
         signal rod_bus_addr : out  std_logic_vector(15 downto 0);
         signal rod_bus_rnw  : out  std_logic;
         signal rod_bus_hwsb_in : out  std_logic;
			signal rod_bus_ds_in: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_strb_in <= '1';
		rod_bus_rnw <= '0'; rod_bus_hwsb_in <= '0';
		rod_bus_data_in <= data_in(15 downto 0);
	   wait for clk_period*3; rod_bus_ds_in <= '1'; wait for clk_period; rod_bus_ds_in <= '0';
	   rod_bus_hwsb_in <= '1'; rod_bus_data_in <= data_in(31 downto 16);
		wait for clk_period*2; rod_bus_ds_in <= '1'; wait for clk_period; rod_bus_ds_in <= '0';
		wait for clk_period*3;
		rod_bus_hwsb_in <= '0';
		rod_bus_addr <= (others => '0');
		rod_bus_rnw <= '1';
      wait for clk_period; 
		rod_bus_strb_in <= '0';	
		wait for clk_period*2;			
	end write_bus;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: ibl_slv_register PORT MAP (
          clk => clk,
          rst => rst,
          rod_bus_strb_in => rod_bus_strb_in,
          rod_bus_addr => rod_bus_addr,
          rod_bus_rnw => rod_bus_rnw,
          rod_bus_hwsb_in => rod_bus_hwsb_in,
          rod_bus_ds_in => rod_bus_ds_in,
          rod_bus_data_in => rod_bus_data_in,
          rod_bus_data_out => rod_bus_data_out,
          rod_bus_ack_out => rod_bus_ack_out,
          slave_id => slave_id,
          fmt_link_enabled => fmt_link_enabled,
          fmt_timeout_limit => fmt_timeout_limit,
          fmt_data_overflow_limit => fmt_data_overflow_limit,
          fmt_header_trailer_limit => fmt_header_trailer_limit,
          fmt_rod_busy_limit => fmt_rod_busy_limit,
          fmt_header_trailer_error => fmt_header_trailer_error,
          fmt_time_out_error => fmt_time_out_error,
          fmt_data_overflow_error => fmt_data_overflow_error,
          fmt_ctrl_reg => fmt_ctrl_reg,
          fmt_stts_reg => fmt_stts_reg,
          inmem_reset => inmem_reset,
          inmem_select => inmem_select,
          inmem_rden => inmem_rden,
          inmem_data => inmem_data,
          err_mask_wen1 => err_mask_wen1,
          err_mask_din1 => err_mask_din1,
          err_mask_dout1 => err_mask_dout1,
          err_mask_sel1 => err_mask_sel1,
          err_mask_wen2 => err_mask_wen2,
          err_mask_din2 => err_mask_din2,
          err_mask_dout2 => err_mask_dout2,
          err_mask_sel2 => err_mask_sel2,
          err_mask_wen3 => err_mask_wen3,
          err_mask_din3 => err_mask_din3,
          err_mask_dout3 => err_mask_dout3,
          err_mask_sel3 => err_mask_sel3,
          err_mask_wen4 => err_mask_wen4,
          err_mask_din4 => err_mask_din4,
          err_mask_dout4 => err_mask_dout4,
          err_mask_sel4 => err_mask_sel4,
          source_id => source_id,
          format_version => format_version,
          run_number => run_number,
          efb1_out_fifo_rst => efb1_out_fifo_rst,
          efb1_out_fifo1_empty_n => efb1_out_fifo1_empty_n,
          efb1_out_fifo1_aempty_n => efb1_out_fifo1_aempty_n,
          efb1_out_fifo1_full_n => efb1_out_fifo1_full_n,
          efb1_out_fifo1_afull_n => efb1_out_fifo1_afull_n,
          efb1_out_fifo1_play_done => efb1_out_fifo1_play_done,
          efb1_out_fifo2_empty_n => efb1_out_fifo2_empty_n,
          efb1_out_fifo2_aempty_n => efb1_out_fifo2_aempty_n,
          efb1_out_fifo2_full_n => efb1_out_fifo2_full_n,
          efb1_out_fifo2_afull_n => efb1_out_fifo2_afull_n,
          efb1_out_fifo2_play_done => efb1_out_fifo2_play_done,
          efb2_out_fifo_rst => efb2_out_fifo_rst,
          efb2_out_fifo1_empty_n => efb2_out_fifo1_empty_n,
          efb2_out_fifo1_aempty_n => efb2_out_fifo1_aempty_n,
          efb2_out_fifo1_full_n => efb2_out_fifo1_full_n,
          efb2_out_fifo1_afull_n => efb2_out_fifo1_afull_n,
          efb2_out_fifo1_play_done => efb2_out_fifo1_play_done,
          efb2_out_fifo2_empty_n => efb2_out_fifo2_empty_n,
          efb2_out_fifo2_aempty_n => efb2_out_fifo2_aempty_n,
          efb2_out_fifo2_full_n => efb2_out_fifo2_full_n,
          efb2_out_fifo2_afull_n => efb2_out_fifo2_afull_n,
          efb2_out_fifo2_play_done => efb2_out_fifo2_play_done,
          efb1_misc_stts_reg => efb1_misc_stts_reg,
          efb2_misc_stts_reg => efb2_misc_stts_reg,
          efb1_ev_header_data => efb1_ev_header_data,
          efb2_ev_header_data => efb2_ev_header_data,
          efb1_ev_fifo_data1 => efb1_ev_fifo_data1,
          efb1_ev_fifo_data2 => efb1_ev_fifo_data2,
          efb2_ev_fifo_data1 => efb2_ev_fifo_data1,
          efb2_ev_fifo_data2 => efb2_ev_fifo_data2,
          efb_mask_bcid_error => efb_mask_bcid_error,
          efb_mask_l1id_error => efb_mask_l1id_error,
          efb_bcid_offset => efb_bcid_offset,
          efb_grp_evt_enable => efb_grp_evt_enable,
          efb1_event_count => efb1_event_count,
          efb2_event_count => efb2_event_count,
          mask_boc_clock_err => mask_boc_clock_err,
          mask_tim_clock_err => mask_tim_clock_err,
          efb1_l1id_fifo_data => efb1_l1id_fifo_data,
          efb1_l1id_fifo_addr => efb1_l1id_fifo_addr,
          efb2_l1id_fifo_data => efb2_l1id_fifo_data,
          efb2_l1id_fifo_addr => efb2_l1id_fifo_addr,
          efb_enable_l1_trap => efb_enable_l1_trap,
          efb_l1_trap_full_in => efb_l1_trap_full_in,
          efb1_latched_l1id0 => efb1_latched_l1id0,
          efb1_latched_l1id1 => efb1_latched_l1id1,
          efb1_latched_bcid0 => efb1_latched_bcid0,
          efb1_latched_bcid1 => efb1_latched_bcid1,
          efb2_latched_l1id0 => efb2_latched_l1id0,
          efb2_latched_l1id1 => efb2_latched_l1id1,
          efb2_latched_bcid0 => efb2_latched_bcid0,
          efb2_latched_bcid1 => efb2_latched_bcid1,
          tim_bcid_in_rol => tim_bcid_in_rol,
          bcid_rollover_select => bcid_rollover_select
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rst <= '1';
      wait for 100 ns;	
		rst <= '0';
      wait for 45 ns;

      -- insert stimulus here 
      read_fmt_stts_reg : read_bus(X"0022", rod_bus_strb_in, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 
      assert (rod_bus_data_out /= X"1234abcd")
         report "This is incorrect!"		
         severity NOTE;

      read_fmt_ctrl_reg0 : read_bus(X"0020", rod_bus_strb_in, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 
			
      write_fmt_ctrl_reg : write_bus(X"0020", X"20131122", rod_bus_data_in, 
			rod_bus_strb_in, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 

      wait for 50 ns;

      read_fmt_ctrl_reg1 : read_bus(X"0020", rod_bus_strb_in, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 
      assert (rod_bus_data_out = X"20131122")
         report "This is correct!"		
         severity NOTE;			
			
		wait for 100 ns;	
			
      write_error_mask_14 : write_bus(X"2038", X"EE01FF34", rod_bus_data_in, 
			rod_bus_strb_in, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 		

      read_error_mask_14 : read_bus(X"2038", rod_bus_strb_in, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in);

      write_slave_id_1 : write_bus(X"0005", X"00000001", rod_bus_data_in, 
			rod_bus_strb_in, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 	

      write_error_mask_15 : write_bus(X"213C", X"EFBCABCD", rod_bus_data_in, 
			rod_bus_strb_in, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in); 

      read_error_mask_14_2 : read_bus(X"2038", rod_bus_strb_in, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb_in, rod_bus_ds_in);
     
      wait;
   end process;

END;
