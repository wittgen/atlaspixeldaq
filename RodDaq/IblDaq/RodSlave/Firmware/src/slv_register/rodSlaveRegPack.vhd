--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
-- Author: Shaw-Pin (Bing) Chen 

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.numeric_std.all;
use IEEE.STD_LOGIC_unsigned.all;

package rodSlaveRegPack is

	-- definitions for the address base
	constant addr_width: integer := 16;
	-- we use 4k (12 bits) register ranges for various functions
	constant range_bits: integer := 4; -- topmost bits for decoding the memory
	
	-- block memory
	constant ram_word_size: integer := 9; -- actually is 10 and would be supported by new decoding. 9 for legacy
	constant ram_range: std_logic_vector(range_bits - 1 downto 0) := "0001";

	-- the following ranges are not fully used for decoding yet, but should be in the future
	constant fmt_range: std_logic_vector(range_bits - 1 downto 0) := "0000"; -- formatter 
	constant efb_range: std_logic_vector(range_bits - 1 downto 0) := "0010"; -- efb 
	constant erm_range: std_logic_vector(range_bits - 1 downto 0) := "0011"; -- error masks
	

	-- AKU: I believe the following register relate to the old formatter and efb registers. 
	-- We put them into range 0 and 2 respectively
	-- ROD Slave global registers
	constant fmtLinkEnable	: std_logic_vector(addr_width - 1 downto 0) := X"0000";	-- RW/16
	constant slaveID			: std_logic_vector(addr_width - 1 downto 0) := X"0005";	-- RW/1
	constant calibMode		: std_logic_vector(addr_width - 1 downto 0) := X"0006";	-- RW/1
   constant fmtTrigCntReg  : std_logic_vector(addr_width - 1 downto 0) := X"0007";  --RW/4
   constant HitDiscCnfgReg : std_logic_vector(addr_width - 1 downto 0) := X"0008";  --RW/32 
	
	constant formatVersion	: std_logic_vector(addr_width - 1 downto 0) := X"2200";	-- R/32, W/lower16
	constant sourceID			: std_logic_vector(addr_width - 1 downto 0) := X"2204";	-- R/32, W/lower23
	constant runNumber		: std_logic_vector(addr_width - 1 downto 0) := X"2208";	-- R/32
	constant codeVersion		: std_logic_vector(addr_width - 1 downto 0) := X"221C";	-- R/16	
	

	-- ROD Slave INMEM FIFO Registers
	constant inmemFifoChSel	: std_logic_vector(addr_width - 1 downto 0) := X"0040";	-- RW/2
	constant inmemFifoRst	: std_logic_vector(addr_width - 1 downto 0) := X"0042";	-- W/1
	constant inmemFifoData	: std_logic_vector(addr_width - 1 downto 0) := X"0044";	-- R/12

	-- ROD Slave Formatter Registers
	constant fmtReadoutTimeOutLimit	: std_logic_vector(addr_width - 1 downto 0) := X"0010";	-- RW/32
	constant fmtDataOverflowLimit		: std_logic_vector(addr_width - 1 downto 0) := X"0014";	-- RW/16
	constant fmtHeaderTrailerLimit	: std_logic_vector(addr_width - 1 downto 0) := X"0018";	-- RW/32
	constant fmtRodBusyLimit			: std_logic_vector(addr_width - 1 downto 0) := X"001C";	-- RW/32
   constant fmtTrailerTimeOutLimit	: std_logic_vector(addr_width - 1 downto 0) := X"001F";	-- RW/32

	constant fmtCtrlReg  	       : std_logic_vector(addr_width - 1 downto 0) := X"0020";	-- RW/32
	constant fmt0SttsReg 	       : std_logic_vector(addr_width - 1 downto 0) := X"0022";	-- R/32
	constant fmt1SttsReg 	       : std_logic_vector(addr_width - 1 downto 0) := X"0023";	-- R/32
	constant fmt2SttsReg 	       : std_logic_vector(addr_width - 1 downto 0) := X"0024";	-- R/32
	constant fmt3SttsReg	          : std_logic_vector(addr_width - 1 downto 0) := X"0025";	-- R/32
   constant fmt0OccupancyReg	    : std_logic_vector(addr_width - 1 downto 0) := X"0027";	-- R/40
   constant fmt1OccupancyReg	    : std_logic_vector(addr_width - 1 downto 0) := X"0029";	-- R/40
   constant fmt2OccupancyReg	    : std_logic_vector(addr_width - 1 downto 0) := X"002B";	-- R/40
   constant fmt3OccupancyReg	    : std_logic_vector(addr_width - 1 downto 0) := X"002D";	-- R/40
   constant Formatter0FramesReg   : std_logic_vector(addr_width - 1 downto 0) := x"0030"; -- R/16 
   constant Formatter1FramesReg   : std_logic_vector(addr_width - 1 downto 0) := x"0034"; -- R/16 
   constant Formatter2FramesReg   : std_logic_vector(addr_width - 1 downto 0) := x"0038"; -- R/16 
   constant Formatter3FramesReg   : std_logic_vector(addr_width - 1 downto 0) := x"003C"; -- R/16 
	
	constant fmtReadoutTimeoutErr	: std_logic_vector(addr_width - 1 downto 0) := X"0070";	-- R/16
	constant fmtDataOverflowErr	: std_logic_vector(addr_width - 1 downto 0) := X"0074";	-- R/16
	constant fmtHeaderTrailerErr	: std_logic_vector(addr_width - 1 downto 0) := X"0078";	-- R/16
	constant fmtRodBusyErr			: std_logic_vector(addr_width - 1 downto 0) := X"007A";	-- R/16
	
	-- slave control register, formerly part of formatter registers
	constant slvCtrlReg			: std_logic_vector(addr_width - 1 downto 0) := X"0100";	-- R/32
	constant slvStatReg			: std_logic_vector(addr_width - 1 downto 0) := X"0104";	-- R/32
	
	-- busy register, new
	constant slvBsyReg			      : std_logic_vector(addr_width - 1 downto 0) := X"0108";	-- R/32
	constant slvBsyStatusReg		   : std_logic_vector(addr_width - 1 downto 0) := X"010C";	-- R/32
   constant slvBsyMaskReg           : std_logic_vector(addr_width - 1 downto 0) := X"0110";	-- R/32
   constant slvBsyForceReg          : std_logic_vector(addr_width - 1 downto 0) := X"0114";	-- R/32
	constant slvBsyEfb1Stts0Hist		: std_logic_vector(addr_width - 1 downto 0) := X"0120";	-- R/32
	constant slvBsyEfb1Stts1Hist		: std_logic_vector(addr_width - 1 downto 0) := X"0124";	-- R/32
	constant slvBsyEfb2Stts0Hist		: std_logic_vector(addr_width - 1 downto 0) := X"0128";	-- R/32
	constant slvBsyEfb2Stts1Hist		: std_logic_vector(addr_width - 1 downto 0) := X"012C";	-- R/32
	constant slvBsyRouter0Hist		   : std_logic_vector(addr_width - 1 downto 0) := X"0130";	-- R/32
	constant slvBsyRouter1Hist		   : std_logic_vector(addr_width - 1 downto 0) := X"0134";	-- R/32
	constant slvBsyHistFull0Hist		: std_logic_vector(addr_width - 1 downto 0) := X"0138";	-- R/32
	constant slvBsyHistFull1Hist		: std_logic_vector(addr_width - 1 downto 0) := X"013C";	-- R/32
	constant slvBsyHistOverrun0Hist	: std_logic_vector(addr_width - 1 downto 0) := X"0140";	-- R/32
	constant slvBsyHistOverrun1Hist	: std_logic_vector(addr_width - 1 downto 0) := X"0144";	-- R/32
	constant slvBsyOutHist			   : std_logic_vector(addr_width - 1 downto 0) := X"0148";  -- R/32
   constant slvBsyFmt0FF0Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0150";  -- R/32
   constant slvBsyFmt0FF1Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0154";  -- R/32
   constant slvBsyFmt0FF2Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0158";  -- R/32
   constant slvBsyFmt0FF3Hist       : std_logic_vector(addr_width - 1 downto 0) := X"015C";  -- R/32
   constant slvBsyFmt1FF0Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0160";  -- R/32
   constant slvBsyFmt1FF1Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0164";  -- R/32
   constant slvBsyFmt1FF2Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0168";  -- R/32
   constant slvBsyFmt1FF3Hist       : std_logic_vector(addr_width - 1 downto 0) := X"016C";  -- R/32
   constant slvBsyFmt2FF0Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0170";  -- R/32
   constant slvBsyFmt2FF1Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0174";  -- R/32
   constant slvBsyFmt2FF2Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0178";  -- R/32
   constant slvBsyFmt2FF3Hist       : std_logic_vector(addr_width - 1 downto 0) := X"017C";  -- R/32
   constant slvBsyFmt3FF0Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0180";  -- R/32
   constant slvBsyFmt3FF1Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0184";  -- R/32
   constant slvBsyFmt3FF2Hist       : std_logic_vector(addr_width - 1 downto 0) := X"0188";  -- R/32
   constant slvBsyFmt3FF3Hist       : std_logic_vector(addr_width - 1 downto 0) := X"018C";  -- R/32
   constant slvBsySlink0LffHist		: std_logic_vector(addr_width - 1 downto 0) := X"0190";	-- R/32
	constant slvBsySlink0LdownHist	: std_logic_vector(addr_width - 1 downto 0) := X"0194";	-- R/32
	constant slvBsySlink1LffHist		: std_logic_vector(addr_width - 1 downto 0) := X"0198";	-- R/32
	constant slvBsySlink1LdownHist	: std_logic_vector(addr_width - 1 downto 0) := X"019C";	-- R/32

	-- ROD Slave 0 EFB Error Mask Registers
	-- constant linkErrMaskBase		: std_logic_vector(addr_width - 1 downto 0) := X"3000";	-- R/16
	constant linkErrMaskBase      : std_logic_vector(addr_width - 1 downto 0) := erm_range & X"000";
	constant linkErrGroupBits		: integer := 2;	-- we have 4 (2 bits) groups of error mask register
	constant linkErrGroupShift		: integer := 5;	-- starting at address bit 5
	constant linkSelBits			   : integer := 2;	-- we have 4 links per group
	constant linkSelShift			: integer := 2;	-- starting at address bit 2
	constant ermGroup0				: std_logic_vector(linkErrGroupBits - 1 downto 0) := std_logic_vector(to_unsigned(0,linkErrGroupBits));
	constant ermGroup1				: std_logic_vector(linkErrGroupBits - 1 downto 0) := std_logic_vector(to_unsigned(1,linkErrGroupBits));
	constant ermGroup2				: std_logic_vector(linkErrGroupBits - 1 downto 0) := std_logic_vector(to_unsigned(2,linkErrGroupBits));
	constant ermGroup3				: std_logic_vector(linkErrGroupBits - 1 downto 0) := std_logic_vector(to_unsigned(3,linkErrGroupBits));
	
	-- ROD Slave 0 EFB Error Mask Registers
--	constant link0ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2000";	-- RW/32
--	constant link1ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2004";	-- RW/32
--	constant link2ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2008";	-- RW/32
--	constant link3ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"200C";	-- RW/32
--	constant link4ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2010";	-- RW/32
--	constant link5ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2014";	-- RW/32
--	constant link6ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2018";	-- RW/32
--	constant link7ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"201C";	-- RW/32
--	constant link8ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2020";	-- RW/32
--	constant link9ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2024";	-- RW/32
--	constant link10ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2028";	-- RW/32
--	constant link11ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"202C";	-- RW/32
--	constant link12ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2030";	-- RW/32
--	constant link13ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2034";	-- RW/32
--	constant link14ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2038";	-- RW/32
--	constant link15ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"203C";	-- RW/32
--	

--	-- ROD Slave 1 EFB Error Mask  Registers		
--	constant link16ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2100";	-- RW/32
--	constant link17ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2104";	-- RW/32
--	constant link18ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2108";	-- RW/32
--	constant link19ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"210C";	-- RW/32
--	constant link20ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2110";	-- RW/32
--	constant link21ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2114";	-- RW/32
--	constant link22ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2118";	-- RW/32
--	constant link23ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"211C";	-- RW/32
--	constant link24ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2120";	-- RW/32
--	constant link25ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2124";	-- RW/32
--	constant link26ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2128";	-- RW/32
--	constant link27ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"212C";	-- RW/32
--	constant link28ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2130";	-- RW/32
--	constant link29ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2134";	-- RW/32
--	constant link30ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"2138";	-- RW/32
--	constant link31ErrMask	: std_logic_vector(addr_width - 1 downto 0) := X"213C";	-- RW/32	

	-- ROD Slave EFB General Registers
	constant efb1CtrlReg	: std_logic_vector(addr_width - 1 downto 0) := X"2210";	-- RW/32
	constant efb2CtrlReg	: std_logic_vector(addr_width - 1 downto 0) := X"2212";	-- RW/32
	constant efb1SttsReg	: std_logic_vector(addr_width - 1 downto 0) := X"2218";	-- R/12
	constant efb2SttsReg	: std_logic_vector(addr_width - 1 downto 0) := X"221A";	-- R/12
	
	constant efb1EvtHeader	: std_logic_vector(addr_width - 1 downto 0) := X"2220";	-- R/16
	constant efb2EvtHeader	: std_logic_vector(addr_width - 1 downto 0) := X"2222";	-- R/16	
	constant efb1EvtCount	: std_logic_vector(addr_width - 1 downto 0) := X"2230";	-- R/32
	constant efb2EvtCount	: std_logic_vector(addr_width - 1 downto 0) := X"2232";	-- R/32	

	constant efb1OutFifoRst	: std_logic_vector(addr_width - 1 downto 0) := X"2248";	-- W/1
	constant efb2OutFifoRst	: std_logic_vector(addr_width - 1 downto 0) := X"224A";	-- W/1	

	constant efb1OutFifoSttsReg: std_logic_vector(addr_width - 1 downto 0) := X"224C";	-- R/24
	constant efb2OutFifoSttsReg: std_logic_vector(addr_width - 1 downto 0) := X"224E";	-- R/24	
	
	constant efb1L1BcTrappedVal: std_logic_vector(addr_width - 1 downto 0) := X"227C";	-- R/32
	constant efb2L1BcTrappedVal: std_logic_vector(addr_width - 1 downto 0) := X"227D";	-- R/32
	
	constant efb1MiscSttsReg	: std_logic_vector(addr_width - 1 downto 0) := X"227E";	-- R/32
	constant efb2MiscSttsReg	: std_logic_vector(addr_width - 1 downto 0) := X"227F";	-- R/32

	-- Range 1  is the memory block
	
	
	-- ROD Slave BUSY Registers
	-- register range 3
	

	
	-- ROD Slave Router Registers
	-- register range 4
   --Runtime Error Registers
   constant Link0ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4000";	-- R/32
   constant Link1ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4004";	-- R/32	
   constant Link2ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4008";	-- R/32  
   constant Link3ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"400C";	-- R/32
   constant Link4ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4010";	-- R/32
   constant Link5ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4014";	-- R/32	
   constant Link6ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4018";	-- R/32  
   constant Link7ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"401C";	-- R/32
   constant Link8ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4020";	-- R/32
   constant Link9ErrorsReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4024";	-- R/32	
   constant Link10ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"4028";	-- R/32  
   constant Link11ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"402C";	-- R/32
   constant Link12ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"4030";	-- R/32
   constant Link13ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"4034";	-- R/32	
   constant Link14ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"4038";	-- R/32  
   constant Link15ErrorsReg	: std_logic_vector(addr_width - 1 downto 0) := X"403C";	-- R/32
   constant HeaderErrorReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4040";	-- R/32
   constant TrailerErrorReg	: std_logic_vector(addr_width - 1 downto 0) := X"4044";	-- R/32	
   constant RDTimeoutErrorReg	: std_logic_vector(addr_width - 1 downto 0) := X"4048";	-- R/32  
   constant HTLimitErrorReg	: std_logic_vector(addr_width - 1 downto 0) := X"404C";	-- R/32
   constant RowColErrorReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4050";	-- R/32
   constant L1IDErrorReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4054";	-- R/32	
   constant BCIDErrorReg	   : std_logic_vector(addr_width - 1 downto 0) := X"4058";	-- R/32
	
	-- ROD Slave Histogrammer Registers	
	-- register range 5
	

	-- ROD Slave Error Status Registers
	-- register range 6
	
	

end rodSlaveRegPack;

package body rodSlaveRegPack is




 
end rodSlaveRegPack;
