----------------------------------------------------------------------------------
-- Company: University of Washington, Seattle
-- Engineer: Shaw-Pin (Bing) Chen
-- 
-- Create Date:    13:01:52 11/01/2013 
-- Design Name: 
-- Module Name:    ibl_slv_register - rtl
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--       line 420 look for 'Z'
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_unsigned.ALL;

use work.rodSlaveRegPack.all;

entity ibl_slv_register is
	generic(simulation : boolean := false);
	port (
		clk	                        : in  STD_LOGIC;                         
		rst                           : in  STD_LOGIC;                       
		-- ROD Bus Control and Data Signals		
		rod_bus_strb_in		         : in  STD_LOGIC; -- chip-enable        
		rod_bus_addr       	         : in  STD_LOGIC_VECTOR(15 downto 0);   
		rod_bus_rnw        	         : in  STD_LOGIC;                       
		rod_bus_hwsb_in    	         : in  STD_LOGIC;                       
		rod_bus_ds_in      	         : in  STD_LOGIC;                       
		rod_bus_data_in    	         : in  STD_LOGIC_VECTOR(15 downto 0);   
		rod_bus_data_out   	         : out STD_LOGIC_VECTOR(15 downto 0);   
		rod_bus_ack_out    	         : out STD_LOGIC;
		slave_id           	         : out STD_LOGIC;
		-- MicroBlaze signals
		busCtlReg				         : out STD_LOGIC_VECTOR(31 downto 0);
		busStatReg				         : in  STD_LOGIC_VECTOR(31 downto 0);
		-- busy signals
		busy_reg_out			         : out STD_LOGIC_VECTOR(31 downto 0);
      busy_mask_reg                 : out STD_LOGIC_VECTOR(28 downto 0);
      force_input_reg               : out STD_LOGIC_VECTOR(28 downto 0);
		busy_status_reg			      : in STD_LOGIC_VECTOR(28 downto 0);
		busy_Slink0LffHist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Slink0LdownHist_reg		: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Slink1LffHist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Slink1LdownHist_reg		: in STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt0FF0_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt0FF1_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt0FF2_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt0FF3_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt1FF0_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt1FF1_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt1FF2_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt1FF3_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt2FF0_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt2FF1_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt2FF2_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt2FF3_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt3FF0_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt3FF1_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt3FF2_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
      busy_Fmt3FF3_Hist_reg         : in  STD_LOGIC_VECTOR(31 downto 0);
		busy_Efb1Stts0Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Efb1Stts1Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Efb2Stts0Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Efb2Stts1Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_Router0Hist_reg			   : in STD_LOGIC_VECTOR(31 downto 0);
		busy_Router1Hist_reg			   : in STD_LOGIC_VECTOR(31 downto 0);
		busy_HistFull0Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_HistFull1Hist_reg			: in STD_LOGIC_VECTOR(31 downto 0);
		busy_HistOverrun0Hist_reg		: in STD_LOGIC_VECTOR(31 downto 0);
		busy_HistOverrun1Hist_reg		: in STD_LOGIC_VECTOR(31 downto 0);
		busy_OutHist_reg			      : in STD_LOGIC_VECTOR(31 downto 0);
      hitdisccnfg                   : out STD_LOGIC_VECTOR(31 downto 0);
		-- Formatter Signals
		fmt_link_enabled					: out STD_LOGIC_VECTOR(15 downto 0);
      fmt_trig_cnt                  : out STD_LOGIC_VECTOR( 3 downto 0);
		fmt_timeout_limit			 		: out STD_LOGIC_VECTOR(31 downto 0);
		fmt_data_overflow_limit  	   : out STD_LOGIC_VECTOR(15 downto 0);
		fmt_header_trailer_limit 	   : out STD_LOGIC_VECTOR(31 downto 0);        
		fmt_rod_busy_limit	 	 		: out STD_LOGIC_VECTOR( 9 downto 0);      
      fmt_trailer_limit			 		: out STD_LOGIC_VECTOR(31 downto 0);
		fmt_header_trailer_error 	   : in  STD_LOGIC_VECTOR(15 downto 0); 
		fmt_time_out_error       	   : in  STD_LOGIC_VECTOR(15 downto 0); 
		fmt_data_overflow_error  	   : in  STD_LOGIC_VECTOR(15 downto 0); 
		fmt_ctrl_reg   		    		: out STD_LOGIC_VECTOR(31 downto 0);  
		fmt0_stts_reg 				 		: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt1_stts_reg 				 		: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt2_stts_reg 				 		: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt3_stts_reg 				 		: in  STD_LOGIC_VECTOR(31 downto 0);
      fmt0Occupancy                 : in  STD_LOGIC_VECTOR(39 downto 0);
      fmt1Occupancy                 : in  STD_LOGIC_VECTOR(39 downto 0);
      fmt2Occupancy                 : in  STD_LOGIC_VECTOR(39 downto 0);
      fmt3Occupancy                 : in  STD_LOGIC_VECTOR(39 downto 0);
      formatter0_frames             : in  STD_LOGIC_VECTOR(15 downto 0);
      formatter1_frames             : in  STD_LOGIC_VECTOR(15 downto 0);
      formatter2_frames             : in  STD_LOGIC_VECTOR(15 downto 0);
      formatter3_frames             : in  STD_LOGIC_VECTOR(15 downto 0);
		-- link occupancy counts need to be implemented
		-- Inmem Fifo Signals
		inmem_reset                   : out STD_LOGIC;
		inmem_select		            : out STD_LOGIC_VECTOR( 1 downto 0);
		inmem_rden			            : out STD_LOGIC;
		inmem_data			            : in  STD_LOGIC_VECTOR(11 downto 0);
		-- Interfacing with the efb err_detect_new blocks
		err_mask_wen1   		         : out STD_LOGIC;
		err_mask_din1   		         : out STD_LOGIC_VECTOR(31 downto 0);
		err_mask_dout1  		         : in  STD_LOGIC_VECTOR(31 downto 0);
		err_mask_sel1   		         : out STD_LOGIC_VECTOR( 2 downto 0);     
		err_mask_wen2   		         : out STD_LOGIC;
		err_mask_din2   		         : out STD_LOGIC_VECTOR(31 downto 0);
		err_mask_dout2  		         : in  STD_LOGIC_VECTOR(31 downto 0);
		err_mask_sel2   		         : out STD_LOGIC_VECTOR( 2 downto 0);
		err_mask_wen3   		         : out STD_LOGIC;
		err_mask_din3   		         : out STD_LOGIC_VECTOR(31 downto 0);
		err_mask_dout3  		         : in  STD_LOGIC_VECTOR(31 downto 0);
		err_mask_sel3   		         : out STD_LOGIC_VECTOR( 2 downto 0);     
		err_mask_wen4   		         : out STD_LOGIC;
		err_mask_din4   		         : out STD_LOGIC_VECTOR(31 downto 0);
		err_mask_dout4  		         : in  STD_LOGIC_VECTOR(31 downto 0);
		err_mask_sel4   		         : out STD_LOGIC_VECTOR( 2 downto 0);
		-- Information going into efb gen_fragment block
		source_id           		      : out STD_LOGIC_VECTOR(31 downto 0); 
		format_version         	      : out STD_LOGIC_VECTOR(31 downto 0); 
		run_number						   : out STD_LOGIC_VECTOR(31 downto 0); 
		-- output evtmem fifos and EFB signals
		efb1_out_fifo_rst       	   : out STD_LOGIC;	 
		efb1_out_fifo1_empty_n   	   : in  STD_LOGIC;
		efb1_out_fifo1_aempty_n  	   : in  STD_LOGIC;
		efb1_out_fifo1_full_n    	   : in  STD_LOGIC;
		efb1_out_fifo1_afull_n   	   : in  STD_LOGIC;
		efb1_out_fifo1_play_done 	   : in  STD_LOGIC;
		efb1_out_fifo2_empty_n   	   : in  STD_LOGIC;
		efb1_out_fifo2_aempty_n  	   : in  STD_LOGIC;
		efb1_out_fifo2_full_n    	   : in  STD_LOGIC;
		efb1_out_fifo2_afull_n   	   : in  STD_LOGIC;
      efb1_out_fifo2_play_done      : in  STD_LOGIC;
		efb2_out_fifo_rst        	   : out STD_LOGIC;	 
		efb2_out_fifo1_empty_n   	   : in  STD_LOGIC;
		efb2_out_fifo1_aempty_n  	   : in  STD_LOGIC;
		efb2_out_fifo1_full_n    	   : in  STD_LOGIC;
		efb2_out_fifo1_afull_n   	   : in  STD_LOGIC;
		efb2_out_fifo1_play_done 	   : in  STD_LOGIC;
		efb2_out_fifo2_empty_n   	   : in  STD_LOGIC;
		efb2_out_fifo2_aempty_n  	   : in  STD_LOGIC;
		efb2_out_fifo2_full_n    	   : in  STD_LOGIC;
		efb2_out_fifo2_afull_n   	   : in  STD_LOGIC;
      efb2_out_fifo2_play_done      : in  STD_LOGIC;
		efb1_misc_stts_reg       	   : in  STD_LOGIC_VECTOR(30 downto 0);			
		efb2_misc_stts_reg       	   : in  STD_LOGIC_VECTOR(30 downto 0);  
		efb1_ev_header_data      	   : in  STD_LOGIC_VECTOR(15 downto 0);
		efb2_ev_header_data      	   : in  STD_LOGIC_VECTOR(15 downto 0);
		efb1_ev_fifo_data1       	   : in  STD_LOGIC_VECTOR(34 downto 0);
		efb1_ev_fifo_data2       	   : in  STD_LOGIC_VECTOR(34 downto 0);
		efb2_ev_fifo_data1       	   : in  STD_LOGIC_VECTOR(34 downto 0);
		efb2_ev_fifo_data2       	   : in  STD_LOGIC_VECTOR(34 downto 0);
		efb_mask_bcid_error      	   : out STD_LOGIC;
		efb_mask_l1id_error      	   : out STD_LOGIC;
		efb_bcid_offset          	   : out STD_LOGIC_VECTOR( 9 downto 0);
		efb_grp_evt_enable       	   : out STD_LOGIC_VECTOR( 1 downto 0);
		efb1_event_count         	   : in  STD_LOGIC_VECTOR(31 downto 0);
		efb2_event_count         	   : in  STD_LOGIC_VECTOR(31 downto 0);
		mask_boc_clock_err       	   : out STD_LOGIC;
		mask_tim_clock_err       	   : out STD_LOGIC;
		efb1_l1id_fifo_data		 	   : in  STD_LOGIC_VECTOR(39 downto 0);
		efb1_l1id_fifo_addr		 	   : out STD_LOGIC_VECTOR( 3 downto 0);
		efb2_l1id_fifo_data	    	   : in  STD_LOGIC_VECTOR(39 downto 0);
		efb2_l1id_fifo_addr		 	   : out STD_LOGIC_VECTOR( 3 downto 0);
		efb_enable_l1_trap       	   : out STD_LOGIC_VECTOR( 1 downto 0);
		efb_l1_trap_full_in      	   : in  STD_LOGIC_VECTOR( 1 downto 0);
		efb1_latched_l1id0       	   : in  STD_LOGIC_VECTOR(12 downto 0);
		efb1_latched_l1id1       	   : in  STD_LOGIC_VECTOR(12 downto 0);
		efb1_latched_bcid0       	   : in  STD_LOGIC_VECTOR( 9 downto 0);
		efb1_latched_bcid1       	   : in  STD_LOGIC_VECTOR( 9 downto 0);
		efb2_latched_l1id0       	   : in  STD_LOGIC_VECTOR(12 downto 0);
		efb2_latched_l1id1       	   : in  STD_LOGIC_VECTOR(12 downto 0);
		efb2_latched_bcid0       	   : in  STD_LOGIC_VECTOR( 9 downto 0);
		efb2_latched_bcid1       	   : in  STD_LOGIC_VECTOR( 9 downto 0);						
		tim_bcid_in_rol          	   : out STD_LOGIC;
		bcid_rollover_select     	   : out STD_LOGIC;
		calib_mode		 	            : out STD_LOGIC;
      --Error Registers
      link0_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link1_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link2_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link3_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link4_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link5_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link6_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link7_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link8_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link9_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      link10_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0);
      link11_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
      link12_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
      link13_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
      link14_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
      link15_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
      header_err_reg             : in  STD_LOGIC_VECTOR(31 downto 0);     
      trailer_err_reg            : in  STD_LOGIC_VECTOR(31 downto 0); 
      rd_timeout_err_reg         : in  STD_LOGIC_VECTOR(31 downto 0); 
      ht_limit_err_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
      row_col_err_reg            : in  STD_LOGIC_VECTOR(31 downto 0); 
      l1_id_err_reg              : in  STD_LOGIC_VECTOR(31 downto 0); 
      bc_id_err_reg              : in  STD_LOGIC_VECTOR(31 downto 0)
	);   
end ibl_slv_register;

architecture rtl of ibl_slv_register is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------
	signal slave_id_i        : STD_LOGIC;                          
	signal calib_mode_i		 : STD_LOGIC;
   signal hitdisccnfg_i     : STD_LOGIC_VECTOR(31 downto 0);
	signal link_enabled_i    : STD_LOGIC_VECTOR(15 downto 0);       
   signal trig_cnt_i        : STD_LOGIC_VECTOR( 3 downto 0);
	signal fmt_ctrl_reg_i    : STD_LOGIC_VECTOR(31 downto 0);      
	signal timeout_limit_i   : STD_LOGIC_VECTOR(31 downto 0);      
   signal trailer_limit_i   : STD_LOGIC_VECTOR(31 downto 0);
	signal overflow_limit_i  : STD_LOGIC_VECTOR(15 downto 0);      
	signal ht_limit_i        : STD_LOGIC_VECTOR(31 downto 0);      
	signal rod_busy_limit_i  : STD_LOGIC_VECTOR( 9 downto 0);      
	
	type states is (
	  idle, 
	  acknowledge, 
	  wait_cycle -- this state never reached...
	  );
	signal acknowledge_cycle_state : states;

	signal simu_config_1_int 	: STD_LOGIC_VECTOR(31 downto 0);
	signal simu_config_2_int 	: STD_LOGIC_VECTOR(31 downto 0);

	signal readattivo					: STD_LOGIC;
	signal readattivo_old			: STD_LOGIC;
	signal first_check 				: STD_LOGIC;
	
   signal inmem_select_i			: STD_LOGIC_VECTOR(1 downto 0);

	-- EFB Register Signals
	SIGNAL format_version_i		: STD_LOGIC_VECTOR(31 downto 0);       
	SIGNAL source_id_i			: STD_LOGIC_VECTOR(31 downto 0);         
	SIGNAL run_number_i			: STD_LOGIC_VECTOR(31 downto 0);              
	
	SIGNAL err_mask_clr_cntr  : UNSIGNED( 2 downto 0);
	SIGNAL err_mask_clr_flag  : STD_LOGIC;
	
	SIGNAL l1id_fifo_data_dummy_i   : STD_LOGIC_VECTOR(39 downto 0); 
	SIGNAL bcid_offset_i            : STD_LOGIC_VECTOR( 9 downto 0);
	SIGNAL mask_bcid_error_i        : STD_LOGIC;
	SIGNAL mask_l1id_error_i        : STD_LOGIC;
	SIGNAL grp_evt_enable_i         : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL mask_boc_clk_err_i		  : STD_LOGIC;
	SIGNAL mask_tim_clk_err_i 		  : STD_LOGIC;
	SIGNAL enable_l1_trap_i     	  : STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL tim_bcid_in_rol_i    	  : STD_LOGIC;
	SIGNAL bcid_rollover_sel_i  	  : STD_LOGIC;

	signal slv_ctlReg_i      : std_logic_vector(31 downto 0);
	signal busy_reg_i        : std_logic_vector(31 downto 0);
   signal busy_mask_reg_i   : std_logic_vector(28 downto 0);
   signal force_input_reg_i : std_logic_vector(28 downto 0);
	
	-- initialisation default values
	function getLinkEnableDefault(simulation: boolean) return STD_LOGIC_VECTOR is 
	begin
		if simulation then
			return X"0000";
		else
			return X"0000";
		end if;
	end getLinkEnableDefault;

--	function getConfigModeDefault(simulation: boolean) return STD_LOGIC_VECTOR is 
--	begin
--		if simulation then
--			return X"000";
--		else
--			return X"00f";
--		end if;
--	end getConfigModeDefault;
--
--	function getNumAcceptsDefault(simulation: boolean) return STD_LOGIC_VECTOR is 
--	begin
--		if simulation then
--			return X"0000";
--		else
--			return X"7777";
--		end if;
--	end getNumAcceptsDefault;

	function getTimeoutLimitDefault(simulation: boolean) return STD_LOGIC_VECTOR is 
	begin
		if simulation then
			return X"00000080";
		else
			return X"00000800";
		end if;
	end getTimeoutLimitDefault;

	function getSimuConfig1Default(simulation: boolean) return STD_LOGIC_VECTOR is
	begin
		if simulation then
			return X"00000071";
		else
			return X"00000000";
		end if;
	end getSimuConfig1Default;

	function getSimuConfig2Default(simulation: boolean) return STD_LOGIC_VECTOR is
	begin
		if simulation then
			return X"04000000";
		else
			return X"00000000";
		end if;
	end getSimuConfig2Default;

	function getOverflowLimitDefault(simulation: boolean) return STD_LOGIC_VECTOR is 
	begin
		if simulation then
			return X"0100";
		else
			return X"0800";
		end if;
	end getOverflowLimitDefault;


begin

	busCtlReg 	      <= slv_ctlReg_i;

	busy_reg_out	   <= busy_reg_i;
   busy_mask_reg     <= busy_mask_reg_i;
   force_input_reg   <= force_input_reg_i;
	
	slave_id	         <= slave_id_i;                                     
	calib_mode 	      <= calib_mode_i;
	fmt_ctrl_reg 	   <= fmt_ctrl_reg_i;                                 
	format_version 	<= format_version_i;                               
	source_id      	<= source_id_i;                                    
	run_number     	<= run_number_i;                                   
	inmem_select   	<= inmem_select_i;                                 
 
   hitdisccnfg                <= hitdisccnfg_i;
	fmt_link_enabled      		<= link_enabled_i;
   fmt_trig_cnt               <= trig_cnt_i;
	fmt_timeout_limit				<= timeout_limit_i;              
	fmt_data_overflow_limit		<= overflow_limit_i;           
	fmt_header_trailer_limit 	<= ht_limit_i;                 
	fmt_rod_busy_limit	      <= rod_busy_limit_i;           
   fmt_trailer_limit				<= trailer_limit_i;

   efb_mask_bcid_error		<= mask_bcid_error_i;                      
	efb_mask_l1id_error 	   <= mask_l1id_error_i;                      
   efb_bcid_offset 			<= bcid_offset_i;                            
	efb_grp_evt_enable		<= grp_evt_enable_i;                       
	mask_boc_clock_err		<= mask_boc_clk_err_i;                         
	mask_tim_clock_err 		<=	mask_tim_clk_err_i;                        
	efb_enable_l1_trap 		<= enable_l1_trap_i;
	tim_bcid_in_rol 			<= tim_bcid_in_rol_i;
	bcid_rollover_select 	<= bcid_rollover_sel_i;

-- Error mask selection from rod bus address. we need to supply 3 bits but use only 2 for selection
	err_mask_sel1 		<= '0' & rod_bus_addr(linkSelShift +  linkSelBits - 1 downto linkSelShift);   
	err_mask_sel2 		<= '0' & rod_bus_addr(linkSelShift +  linkSelBits - 1 downto linkSelShift);   
	err_mask_sel3 		<= '0' & rod_bus_addr(linkSelShift +  linkSelBits - 1 downto linkSelShift);   
	err_mask_sel4 		<= '0' & rod_bus_addr(linkSelShift +  linkSelBits - 1 downto linkSelShift);   
	
---------------------------------------------------------
-- Process to Enable Read/Write to Router Registers
---------------------------------------------------------
	rw_registers : process (
		clk, 
		rst,
		rod_bus_addr,
		rod_bus_rnw,
		rod_bus_strb_in
	)
	constant code_version : STD_LOGIC_VECTOR(7 downto 0) := X"1B";   -- vA1			
	constant xper_version : STD_LOGIC_VECTOR(3 downto 0) := X"1" ;   -- v1
	constant board_rev    : STD_LOGIC_VECTOR(4 downto 0) := "00010"; -- Rev2
	begin
		if (rst = '1') then
			rod_bus_data_out   <= (others => '0');
-- ******* Implementation / slmulation dependent values *******
         hitdisccnfg_i      <= x"00000000";
			link_enabled_i     <= getLinkEnableDefault(simulation);               
         trig_cnt_i         <= "0001";
--			config_mode_i      <= getConfigModeDefault(simulation);
--			num_accepts_i      <= getNumAcceptsDefault(simulation);
			timeout_limit_i    <= getTimeoutLimitDefault(simulation);             
         trailer_limit_i    <= X"00000050"; --Set to 1us by default on reset
         rod_busy_limit_i   <= "1111111111"; --Set to max by default on reset
			simu_config_1_int  <= getSimuConfig1Default(simulation);              
			simu_config_2_int  <= getSimuConfig2Default(simulation);            
			overflow_limit_i   <= getOverflowLimitDefault(simulation);            
-- ************************************************************
			ht_limit_i         <= X"00000348"; --840 (Max hits calibration)                               
			rod_busy_limit_i   <= (others => '1'); -- fIFO full 
         slave_id_i         <= '0';
			calib_mode_i		 <= '0';
			inmem_reset			 <= '1';
			inmem_select_i		 <= "00";
         busy_mask_reg_i    <= (others=>'0');
         force_input_reg_i  <= (others=>'0');

			fmt_ctrl_reg_i <= X"1234FF0F"; -- NOT USED FOR NOW
			
			-- Reset for EFB Signals
			format_version_i	<= X"03010000";       
			source_id_i				<= (others => '0');   
			run_number_i			<= (others => '0');   
			-- err_mask_sel is taken directly from addres lines
--			err_mask_sel1 		<= (others => '0');   
--			err_mask_sel2 		<= (others => '0');   
--			err_mask_sel3 		<= (others => '0');   
--			err_mask_sel4 		<= (others => '0');   
			err_mask_din1 		<= (others => '0');   
			err_mask_din2 		<= (others => '0');   
			err_mask_din3 		<= (others => '0');   
			err_mask_din4 		<= (others => '0');   
			err_mask_wen1 		<= '0';               
			err_mask_wen2 		<= '0';               
			err_mask_wen3 		<= '0';               
			err_mask_wen4 		<= '0';				        
			err_mask_clr_cntr <= "000";
			err_mask_clr_flag <= '1';
			
			mask_bcid_error_i 	<= '0';
			mask_l1id_error_i 	<= '0';
			bcid_offset_i 			<= (others => '0');
			efb1_out_fifo_rst		<= '1'; 
			efb2_out_fifo_rst		<= '1';

			grp_evt_enable_i 		<= "11";
			enable_l1_trap_i 		<= "00";
			tim_bcid_in_rol_i 	<= '0';
			bcid_rollover_sel_i  <= '0';
			mask_boc_clk_err_i 	<= '0';
		   mask_tim_clk_err_i 	<= '0';

-- Send Empty Events Control Register
-- Address 0x84 (RW) = Bit     0 : 0 (send empty events)
--                     Bit     1 : Mask BCID Error
--                     Bit     2 : Group Event Counter Enable
--                     Bit     3 : Mask L1ID Error
--                     Bit   4-7 : 0 (link input test select)
--                     Bit     8 : Mask TIM Clock Error
--                     Bit     9 : Mask BOC Clock Error
--                     Bit    10 : 0 (mask sweeper error)
--                     Bit    11 : Enable L1 and BC ID Trap
--                     Bit    12 : TIM BCID Readout Link Enable
--                     Bit    13 : BCID Rollover Select
--                     Bit 14-15 : 0
--                     Bit 16-25 : BCID Offset

		elsif (clk'event AND clk = '1') then
			rod_bus_data_out 	<= (others => '0');
			inmem_rden		   	<= '0';
			inmem_reset 			<= '0';
                        efb1_out_fifo_rst	<= '0';
			efb2_out_fifo_rst	<= '0'; 
			err_mask_wen1 <= '0';
			err_mask_wen2 <= '0';
			err_mask_wen3 <= '0';
			err_mask_wen4 <= '0';
			
			source_id_i(23 downto 16) <= X"14";  
			
			-- Register Read
			if (rod_bus_rnw = '1' AND rod_bus_strb_in = '1') then
				-- Reading Error Masks
				-- AKU: error masks use 2 bits from rodbus addr(3 downto 2). 
				-- AKU: however, for some HW constraints we need to splly 3 bit to the error masks
				--	AKU: we loxk the upper one to 0
				-- AKU: the slave id isn't needed here, as the slaves are selected individually by the master
				-- these are located in the erm (error mask) range
				if (rod_bus_addr(addr_width - 1 downto addr_width - range_bits) = erm_range) then 
					if (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup0) then
						if (rod_bus_hwsb_in = '0') then
							rod_bus_data_out <= err_mask_dout1(15 downto 0);
						elsif (rod_bus_hwsb_in = '1') then
							rod_bus_data_out <= err_mask_dout1(31 downto 16);
						end if;		
					elsif (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup1) then
						if (rod_bus_hwsb_in = '0') then
							rod_bus_data_out <= err_mask_dout2(15 downto 0);
						elsif (rod_bus_hwsb_in = '1') then
							rod_bus_data_out <= err_mask_dout2(31 downto 16);
						end if;							
					elsif (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup2) then
						if (rod_bus_hwsb_in = '0') then
							rod_bus_data_out <= err_mask_dout3(15 downto 0);
						elsif (rod_bus_hwsb_in = '1') then
							rod_bus_data_out <= err_mask_dout3(31 downto 16);
						end if;							
					else --rod_bus_addr(5 downto 4) = "11"
						if (rod_bus_hwsb_in = '0') then
							rod_bus_data_out <= err_mask_dout4(15 downto 0);
						elsif (rod_bus_hwsb_in = '1') then
							rod_bus_data_out <= err_mask_dout4(31 downto 16);
						end if;		
					end if;
					
				else
				
					case rod_bus_addr is
				
						when fmtLinkEnable =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link_enabled_i;
							end if;

                  when fmtTrigCntReg=>
                      if (rod_bus_hwsb_in = '0') then
                         rod_bus_data_out(3 downto 0) <= trig_cnt_i;
                      end if;

						when HitDiscCnfgReg =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= hitdisccnfg_i(15 downto 0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= hitdisccnfg_i(31 downto 16);
							end if;

						when slaveID =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out(0) <= slave_id_i;
							end if;

						when calibMode =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out(0) <= calib_mode_i;
							end if;
							
						when fmtReadoutTimeOutLimit =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= timeout_limit_i(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= timeout_limit_i(31 downto 16);
							end if;

						when fmtTrailerTimeOutLimit =>                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= trailer_limit_i(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= trailer_limit_i(31 downto 16);
							end if;

						when fmtDataOverflowLimit =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= overflow_limit_i(15 downto  0);
							end if;						

						when fmtHeaderTrailerLimit =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= ht_limit_i(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= ht_limit_i(31 downto 16);
							end if;

						when fmtRodBusyLimit =>                                     
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "000000" & rod_busy_limit_i;
							end if;

						when fmtCtrlReg =>                                            
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt_ctrl_reg_i(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt_ctrl_reg_i(31 downto 16);
							end if;

						when fmt0SttsReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt0_stts_reg(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt0_stts_reg(31 downto 16);
							end if;						
						
						when fmt1SttsReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt1_stts_reg(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt1_stts_reg(31 downto 16);
							end if;						

						when fmt2SttsReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt2_stts_reg(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt2_stts_reg(31 downto 16);
							end if;						

						when fmt3SttsReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt3_stts_reg(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt3_stts_reg(31 downto 16);
							end if;						
                  --Only bottom 32 bits of occupancy 
                  when fmt0OccupancyReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt0Occupancy(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt0Occupancy(31 downto 16);
							end if;	

                  when fmt1OccupancyReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt1Occupancy(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt1Occupancy(31 downto 16);
							end if;	

                  when fmt2OccupancyReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt2Occupancy(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt2Occupancy(31 downto 16);
							end if;	

                  when fmt3OccupancyReg =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt3Occupancy(15 downto  0);
							elsif (rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= fmt3Occupancy(31 downto 16);
							end if;	

                  when Formatter0FramesReg =>
                     if (rod_bus_hwsb_in = '0') then
                        rod_bus_data_out <= formatter0_frames;
                     end if;

                  when Formatter1FramesReg =>
                     if (rod_bus_hwsb_in = '0') then
                        rod_bus_data_out <= formatter1_frames;
                     end if;

                  when Formatter2FramesReg =>
                     if (rod_bus_hwsb_in = '0') then
                        rod_bus_data_out <= formatter2_frames;
                     end if;

                  when Formatter3FramesReg =>
                     if (rod_bus_hwsb_in = '0') then
                        rod_bus_data_out <= formatter3_frames;
                     end if;

						when inmemFifoChSel =>                    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out(1 downto 0)<= inmem_select_i;
							end if;

						when inmemFifoData => 
							rod_bus_data_out <= "0000" & inmem_data;
							if(first_check = '1') then
								inmem_rden		  <= '1';
							else 
								inmem_rden		  <= '0';
							end if;


					
--							if(rod_bus_hwsb_in = '0') then
--								rod_bus_data_out <= "0000" & inmem_data;
--								if(first_check = '1') then
--									inmem_rden		  <= '1';
--								else 
--									inmem_rden		  <= '0';
--								end if;
--							else
--								inmem_rden		  <= '0';
--							end if;

						when fmtReadoutTimeoutErr => -- need to implement    
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt_time_out_error;   
							end if;

						when fmtDataOverflowErr => -- need to implement       
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt_data_overflow_error;
							end if;

						when fmtHeaderTrailerErr => -- need to implement  
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= fmt_header_trailer_error;
							end if;

						when fmtRodBusyErr => -- need to implement  
							if (rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= X"ABCD";
							end if;
					
						when formatVersion =>       
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= format_version_i(15 downto 0);
							else
								rod_bus_data_out <= format_version_i(31 downto 16);
							end if;
				
						when sourceID =>           
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= source_id_i(15 downto 0);
							else
								rod_bus_data_out <= source_id_i(31 downto 16);
							end if;
										
						when runNumber =>         
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= run_number_i(15 downto 0);
							else
								rod_bus_data_out <= run_number_i(31 downto 16);
							end if;
					
						when efb1CtrlReg => -- Read EFB1 Control Register
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "00"
														& bcid_rollover_sel_i
														& tim_bcid_in_rol_i
														& enable_l1_trap_i(0)
														& '0'
														& mask_boc_clk_err_i
														& mask_tim_clk_err_i
														& "0000"
														& mask_l1id_error_i
														& grp_evt_enable_i(0)
														& mask_bcid_error_i
														& '0';							
							else
								rod_bus_data_out <= "000000" & bcid_offset_i;
							end if;
			
						when efb2CtrlReg => -- Read EFB2 Control Register
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "00"
														& bcid_rollover_sel_i
														& tim_bcid_in_rol_i
														& enable_l1_trap_i(1)
														& '0'
														& mask_boc_clk_err_i
														& mask_tim_clk_err_i
														& "0000"
														& mask_l1id_error_i
														& grp_evt_enable_i(1)
														& mask_bcid_error_i
														& '0';							
							else
								rod_bus_data_out <= "000000" & bcid_offset_i;
							end if;

						when efb1SttsReg => -- Read EFB1 Run-Time Status
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "0000"
														& '0' -- need to replace
														& '0' -- need to replace
														& '0' -- need to replace
														& '0' -- need to implement halt from router
														& efb1_misc_stts_reg(8)
														& efb1_misc_stts_reg(3) -- a bit redundant since we have 227E
														& efb1_misc_stts_reg(1)
														& efb1_out_fifo2_afull_n
														& efb1_misc_stts_reg(7)
														& efb1_misc_stts_reg(2)
														& efb1_misc_stts_reg(0)
														& efb1_out_fifo1_afull_n;
							end if;		

						when efb2SttsReg => -- Read EFB2 Run-Time Status                             
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "0000"
														& '0' -- need to replace
														& '0' -- need to replace
														& '0' -- need to replace
														& '0' -- need to implement halt from router
														& efb2_misc_stts_reg(8)
														& efb2_misc_stts_reg(3) -- a bit redundant since we have 227E
														& efb2_misc_stts_reg(1)
														& efb2_out_fifo2_afull_n
														& efb2_misc_stts_reg(7)
														& efb2_misc_stts_reg(2)
														& efb2_misc_stts_reg(0)
														& efb2_out_fifo1_afull_n;
							end if;		

						when codeVersion => -- code version & board version  
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= "00" & board_rev & '0' & code_version;
							end if;

						when efb1EvtHeader => -- EFB1 event_header data     
							if(rod_bus_hwsb_in = '0') then -- can be taken out
								rod_bus_data_out <= efb1_ev_header_data;
							end if;
							
						when efb2EvtHeader => -- EFB2 event_header data   
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb2_ev_header_data;
							end if;						

						when efb1EvtCount =>  -- EFB1 group event count                                                                            
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb1_event_count(15 downto  0);
							else
								rod_bus_data_out <= efb1_event_count(31 downto 16);							
							end if;

						when efb2EvtCount =>  -- EFB2 group event count                                                                            
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb2_event_count(15 downto  0);
							else
								rod_bus_data_out <= efb2_event_count(31 downto 16);		
							end if;
							
						when efb1L1BcTrappedVal =>  -- EFB1 L1ID and BCID Trapped Value                                                                 
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb1_latched_bcid0 & '0' & efb1_latched_l1id0(4 downto 0);
							else
								rod_bus_data_out <= efb1_latched_bcid1 & '0' & efb1_latched_l1id1(4 downto 0);
							end if;

						when efb2L1BcTrappedVal =>  -- EFB2 L1ID and BCID Trapped Value                                                                  
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb2_latched_bcid0 & '0' & efb2_latched_l1id0(4 downto 0);
							else
								rod_bus_data_out <= efb2_latched_bcid1 & '0' & efb2_latched_l1id1(4 downto 0);
							end if;						
							
						when efb1MiscSttsReg =>                                                                                                     
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb1_misc_stts_reg(15 downto 0);
							else
								rod_bus_data_out <= '0' & efb1_misc_stts_reg(30 downto 16);
							end if;
				
						when efb2MiscSttsReg =>                                                                  
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= efb2_misc_stts_reg(15 downto 0);
							else
								rod_bus_data_out <= '0' & efb2_misc_stts_reg(30 downto 16);
							end if;

						when slvCtrlReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= slv_ctlReg_i(15 downto 0);
							else
								rod_bus_data_out <= slv_ctlReg_i(31 downto 16);
							end if;

						when slvBsyReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_reg_i(15 downto 0);
                     elsif(rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= busy_reg_i(31 downto 16);
							end if;

						when slvBsyMaskReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_mask_reg_i(15 downto 0);
                     elsif(rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= "000" & busy_mask_reg_i(28 downto 16);
							end if;

						when slvBsyForceReg => 				
							if(rod_bus_hwsb_in = '0') then
                        rod_bus_data_out <= force_input_reg_i(15 downto 0);
                     elsif(rod_bus_hwsb_in = '1') then
                        rod_bus_data_out <= "000" & force_input_reg_i(28 downto 16);
							end if;

						when slvBsyStatusReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_status_reg(15 downto 0);
							else
								rod_bus_data_out <= "000" & busy_status_reg(28 downto 16);
							end if;		
							
						when slvBsySlink0LffHist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Slink0LffHist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Slink0LffHist_reg(31 downto 16);
							end if;	
							
						when slvBsySlink0LdownHist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Slink0LdownHist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Slink0LdownHist_reg(31 downto 16);
							end if;		
							
						when slvBsySlink1LffHist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Slink1LffHist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Slink1LffHist_reg(31 downto 16);
							end if;	
							
						when slvBsySlink1LdownHist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Slink1LdownHist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Slink1LdownHist_reg(31 downto 16);
							end if;	
							
						when slvBsyEfb1Stts0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Efb1Stts0Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Efb1Stts0Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyEfb1Stts1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Efb1Stts1Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Efb1Stts1Hist_reg(31 downto 16);
							end if;							
							
						when slvBsyEfb2Stts0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Efb2Stts0Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Efb2Stts0Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyEfb2Stts1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Efb2Stts1Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Efb2Stts1Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyRouter0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Router0Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Router0Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyRouter1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Router1Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Router1Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyHistFull0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_HistFull0Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_HistFull0Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyHistFull1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_HistFull1Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_HistFull1Hist_reg(31 downto 16);
							end if;		
							
						when slvBsyHistOverrun0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_HistOverrun0Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_HistOverrun0Hist_reg(31 downto 16);
							end if;	
							
						when slvBsyHistOverrun1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_HistOverrun1Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_HistOverrun1Hist_reg(31 downto 16);
							end if;		
							
						when slvBsyOutHist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_OutHist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_OutHist_reg(31 downto 16);
							end if;	

						when slvBsyFmt0FF0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt0FF0_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt0FF0_Hist_reg(31 downto 16);
							end if;	

						when slvBsyFmt0FF1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt0FF1_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt0FF1_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt0FF2Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt0FF2_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt0FF2_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt0FF3Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt0FF3_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt0FF3_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt1FF0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt1FF0_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt1FF0_Hist_reg(31 downto 16);
							end if;	

						when slvBsyFmt1FF1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt1FF1_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt1FF1_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt1FF2Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt1FF2_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt1FF2_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt1FF3Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt1FF3_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt1FF3_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt2FF0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt2FF0_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt2FF0_Hist_reg(31 downto 16);
							end if;	

						when slvBsyFmt2FF1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt2FF1_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt2FF1_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt2FF2Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt2FF2_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt2FF2_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt2FF3Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt2FF3_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt2FF3_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt3FF0Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt3FF0_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt3FF0_Hist_reg(31 downto 16);
							end if;	

						when slvBsyFmt3FF1Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt3FF1_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt3FF1_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt3FF2Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt3FF2_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt3FF2_Hist_reg(31 downto 16);
							end if;

						when slvBsyFmt3FF3Hist => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busy_Fmt3FF3_Hist_reg(15 downto 0);
							else
								rod_bus_data_out <= busy_Fmt3FF3_Hist_reg(31 downto 16);
							end if;

						when slvStatReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= busStatReg(15 downto 0);
							else
								rod_bus_data_out <= busStatReg(31 downto 16);
							end if;
----------------------------------------------------------------------------------------							
						when Link0ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link0_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link0_errors_reg(31 downto 16);
							end if;
							
						when Link1ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link1_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link1_errors_reg(31 downto 16);
							end if;
						
						when Link2ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link2_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link2_errors_reg(31 downto 16);
							end if;

						when Link3ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link3_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link3_errors_reg(31 downto 16);
							end if;
							
						when Link4ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link4_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link4_errors_reg(31 downto 16);
							end if;
						
						when Link5ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link5_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link5_errors_reg(31 downto 16);
							end if;
						
						when Link6ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link6_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link6_errors_reg(31 downto 16);
							end if;
						
						when Link7ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link7_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link7_errors_reg(31 downto 16);
							end if;
						
						when Link8ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link8_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link8_errors_reg(31 downto 16);
							end if;						
						
						when Link9ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link9_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link9_errors_reg(31 downto 16);
							end if;		

						when Link10ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link10_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link10_errors_reg(31 downto 16);
							end if;		

						when Link11ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link11_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link11_errors_reg(31 downto 16);
							end if;	

						when Link12ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link12_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link12_errors_reg(31 downto 16);
							end if;	

						when Link13ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link13_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link13_errors_reg(31 downto 16);
							end if;	

						when Link14ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link14_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link14_errors_reg(31 downto 16);
							end if;	

						when Link15ErrorsReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= link15_errors_reg(15 downto 0);
							else
								rod_bus_data_out <= link15_errors_reg(31 downto 16);
							end if;	

						when HeaderErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= header_err_reg(15 downto 0);
							else
								rod_bus_data_out <= header_err_reg(31 downto 16);
							end if;		

						when TrailerErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= trailer_err_reg(15 downto 0);
							else
								rod_bus_data_out <= trailer_err_reg(31 downto 16);
							end if;

						when RDTimeoutErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= rd_timeout_err_reg(15 downto 0);
							else
								rod_bus_data_out <= rd_timeout_err_reg(31 downto 16);
							end if;

						when HTLimitErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= ht_limit_err_reg(15 downto 0);
							else
								rod_bus_data_out <= ht_limit_err_reg(31 downto 16);
							end if;	

						when RowColErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= row_col_err_reg(15 downto 0);
							else
								rod_bus_data_out <= row_col_err_reg(31 downto 16);
							end if;		

						when L1IDErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= l1_id_err_reg(15 downto 0);
							else
								rod_bus_data_out <= l1_id_err_reg(31 downto 16);
							end if;		

						when BCIDErrorReg => 				
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= bc_id_err_reg(15 downto 0);
							else
								rod_bus_data_out <= bc_id_err_reg(31 downto 16);
							end if;								
--------------------------------------------------------------------------------------------------------------
							
										
						when X"EEEE" =>                                                                  
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= simu_config_1_int(15 downto  0);
							elsif(rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= simu_config_1_int(31 downto 16);
							end if; 
				 
						when X"FFFF" =>                                                                  
							if(rod_bus_hwsb_in = '0') then
								rod_bus_data_out <= simu_config_2_int(15 downto  0);
							elsif(rod_bus_hwsb_in = '1') then
								rod_bus_data_out <= simu_config_2_int(31 downto 16);
							end if;				  
									
						when others => rod_bus_data_out <= (others => '0');
							  
					end case;
		
				end if;
			
		--Write registers
			elsif (rod_bus_rnw = '0' AND rod_bus_strb_in = '1') then
				inmem_reset <= '0';
			   
				-- Writing Error Masks: for decoding description see above (reading)
				if (rod_bus_addr(addr_width - 1 downto addr_width - range_bits) = erm_range) then 
					if (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup0) then
						if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
							err_mask_din1(15 downto  0) <= rod_bus_data_in;
						elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
							err_mask_din1(31 downto 16) <= rod_bus_data_in;
							err_mask_wen1 <= '1';
						end if;		
					elsif (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup1) then
						if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
							err_mask_din2(15 downto  0) <= rod_bus_data_in;
						elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
							err_mask_din2(31 downto 16) <= rod_bus_data_in;
							err_mask_wen2 <= '1';
						end if;							
					elsif (rod_bus_addr(linkErrGroupShift + linkErrGroupBits - 1 downto linkErrGroupShift) = ermGroup2) then
						if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
							err_mask_din3(15 downto  0) <= rod_bus_data_in;
						elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
							err_mask_din3(31 downto 16) <= rod_bus_data_in;
							err_mask_wen3 <= '1';
						end if;							
					else --rod_bus_addr(5 downto 4) = "11"
						if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
							err_mask_din4(15 downto  0) <= rod_bus_data_in;
						elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
							err_mask_din4(31 downto 16) <= rod_bus_data_in;
							err_mask_wen4 <= '1';
						end if;		
					end if;
				else	
						
					case rod_bus_addr is
					
						when fmtLinkEnable =>   
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								link_enabled_i <= rod_bus_data_in;
						end if;

                  when fmtTrigCntReg =>
                     if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
                        trig_cnt_i <= rod_bus_data_in(3 downto 0);
                     end if;

                  when HitDiscCnfgReg =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								hitdisccnfg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								hitdisccnfg_i(31 downto 16) <= rod_bus_data_in;
							end if;

						when slaveID => 
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								slave_id_i <= rod_bus_data_in(0);
						end if;
						
						when calibMode =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								calib_mode_i <= rod_bus_data_in(0);
						end if;

                  when fmtReadoutTimeOutLimit =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								timeout_limit_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								timeout_limit_i(31 downto 16) <= rod_bus_data_in;
							end if;

                  when fmtTrailerTimeOutLimit =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								trailer_limit_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								trailer_limit_i(31 downto 16) <= rod_bus_data_in;
							end if;
					 
						when fmtDataOverflowLimit =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								overflow_limit_i <= rod_bus_data_in;
							end if;
						
						when fmtHeaderTrailerLimit =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								ht_limit_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								ht_limit_i(31 downto 16) <= rod_bus_data_in;	
							end if;
					 
						when fmtRodBusyLimit =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								rod_busy_limit_i <= rod_bus_data_in(9 downto 0);
							end if;
							
						when fmtCtrlReg =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								fmt_ctrl_reg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								fmt_ctrl_reg_i(31 downto 16) <= rod_bus_data_in;	
							end if;
						
						when inmemFifoChSel =>
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								inmem_select_i <= rod_bus_data_in(1 downto 0);
							end if;
					 
						when inmemFifoRst =>
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								inmem_reset <= rod_bus_data_in(0);
							end if;
						
						when formatVersion =>
							if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								format_version_i(15 downto 0) <= rod_bus_data_in;
							end if;
					
						when sourceID =>
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								source_id_i(15 downto  0) <= rod_bus_data_in;  -- Module ID  (from Host)
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								--source_id_i(19 downto 16) <= rod_bus_data_in( 3 downto 0); -- SubDetector ID fixed
								source_id_i(31 downto 24) <= rod_bus_data_in(15 downto 8); -- User field
							end if;
					
						when runNumber =>
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								run_number_i(15 downto 0)  <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								run_number_i(31 downto 16) <= rod_bus_data_in;
							end if;

						when efb1CtrlReg => 
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								bcid_rollover_sel_i	<= rod_bus_data_in(13);
								tim_bcid_in_rol_i		<= rod_bus_data_in(12);
								enable_l1_trap_i(0)	<= rod_bus_data_in(11);
								mask_boc_clk_err_i	<= rod_bus_data_in(9);
								mask_tim_clk_err_i	<= rod_bus_data_in(8);
								mask_l1id_error_i  	<= rod_bus_data_in(3);
								grp_evt_enable_i(0) <= rod_bus_data_in(2);
								mask_bcid_error_i  	<= rod_bus_data_in(1);		
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
									bcid_offset_i			<= rod_bus_data_in(9 downto 0);
							end if;
						
						when efb2CtrlReg => 				-- temperarily redundant here	
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								bcid_rollover_sel_i	<= rod_bus_data_in(13);
								tim_bcid_in_rol_i		<= rod_bus_data_in(12);
								enable_l1_trap_i(1)	<= rod_bus_data_in(11);
								mask_boc_clk_err_i	<= rod_bus_data_in(9);
								mask_tim_clk_err_i	<= rod_bus_data_in(8);
								mask_l1id_error_i  	<= rod_bus_data_in(3);
								grp_evt_enable_i(1)  <= rod_bus_data_in(2);
								mask_bcid_error_i  	<= rod_bus_data_in(1);		
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
									bcid_offset_i			<= rod_bus_data_in(9 downto 0);
							end if;					 

						when slvCtrlReg => 				
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								slv_ctlReg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								slv_ctlReg_i(31 downto  16) <= rod_bus_data_in;
							end if;					 

						when slvBsyReg => 				
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								busy_reg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								busy_reg_i(31 downto  16) <= rod_bus_data_in;
							end if;	

						when slvBsyMaskReg => 				
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
								busy_mask_reg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
								busy_mask_reg_i(28 downto  16) <= rod_bus_data_in(12 downto 0);
							end if;

						when slvBsyForceReg => 				
							if(rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
                        force_input_reg_i(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
                        force_input_reg_i(28 downto  16) <= rod_bus_data_in(12 downto 0);
							end if;	



							
								 -- Flush and reset EFB output FIFOs					  
						when efb1OutFifoRst => -- donb't need data strobe
							efb1_out_fifo_rst	<= '1';
								 
						when efb2OutFifoRst =>
							efb2_out_fifo_rst <= '1';
						
						when X"EEEE" => -- simulator configuration reg one
							if (rod_bus_hwsb_in = '0') then
								simu_config_1_int(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1') then
								simu_config_1_int(31 downto 16) <= rod_bus_data_in;
							end if;
						
						when X"FFFF" => -- simulator configuration reg two      
							if (rod_bus_hwsb_in = '0') then
								simu_config_2_int(15 downto  0) <= rod_bus_data_in;
							elsif (rod_bus_hwsb_in = '1') then
								simu_config_2_int(31 downto 16) <= rod_bus_data_in;
							end if;	  
							
						when others =>
					
					end case;
				
				end if;

			end if;                                

	  end if;
	end process rw_registers;

	readattivo <= '1' when (rod_bus_rnw = '1' AND rod_bus_strb_in = '1') else '0';
	first_check <= '1' when (readattivo = '1' and readattivo_old = '0') else '0';
	
	process(clk, rst)
	begin
		if(rst = '1') then
			readattivo_old <= '0';
		elsif(clk'event and clk = '1') then
			readattivo_old <= readattivo;
		end if;
	end process;

-------------------------------------------------------------------------------
-- Logic to generate Access Acknowledge
-------------------------------------------------------------------------------
	access_acknowledge : process (
		clk, 
		rst, 
		rod_bus_strb_in
		)
	begin                      
		if (rst = '1') then
			rod_bus_ack_out 	<= '0';
	--    mb_fifo_rd      <= '0';
			acknowledge_cycle_state <= idle;
		elsif (clk'event and clk = '1') then
			case acknowledge_cycle_state is
				when idle => 
					rod_bus_ack_out <= '0';
					if (rod_bus_strb_in = '1') then
	--          mb_fifo_rd   <= en_mb_fifo_rd_i;
						acknowledge_cycle_state <= acknowledge;
					end if;
				 
				when acknowledge => 
					rod_bus_ack_out <= '1';
	--        mb_fifo_rd      <= '0';
					if (rod_bus_strb_in = '0') then 
						acknowledge_cycle_state <= idle;
					end if;

				when others =>
					rod_bus_ack_out <= '0';
	--        mb_fifo_rd      <= '0';
					acknowledge_cycle_state <= idle;
			end case;
		end if;  
	end process access_acknowledge;     

end rtl;

