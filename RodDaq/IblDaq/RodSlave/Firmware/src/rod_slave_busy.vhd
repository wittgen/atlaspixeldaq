----------------------------------------------------------------------------------
----------------------------------------------------------------------------------
-- Company: CERN
-- Engineer: Malte Backhaus
-- 
-- Create Date:    11:47:58 03/28/2014 
-- Design Name: 
-- Module Name:    rod_slave_busy - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.numeric_std.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM; 
--use UNISIM.VComponents.all;

entity rod_slave_busy is
    Port ( clk40in                     : in  STD_LOGIC;
           slink0_lff_n                : in  STD_LOGIC;
           slink0_ldown_n              : in  STD_LOGIC;
           slink1_lff_n                : in  STD_LOGIC;
           slink1_ldown_n              : in  STD_LOGIC; 
           efb1_misc_stts_reg_i        : in  STD_LOGIC_VECTOR ( 1 downto 0);
	        efb2_misc_stts_reg_i        : in  STD_LOGIC_VECTOR ( 1 downto 0);
           fmt0_ff                     : in  STD_LOGIC_VECTOR ( 3 downto 0);
           fmt1_ff                     : in  STD_LOGIC_VECTOR ( 3 downto 0); 
           fmt2_ff                     : in  STD_LOGIC_VECTOR ( 3 downto 0);
           fmt3_ff                     : in  STD_LOGIC_VECTOR ( 3 downto 0);
	        router0_busy_i              : in  STD_LOGIC; 
	        router1_busy_i              : in  STD_LOGIC;
	        histoFull0                  : in  STD_LOGIC;
	        histoFull1                  : in  STD_LOGIC;
	        reset                       : in  STD_LOGIC;
	        reset_hists                 : in  STD_LOGIC;
	        histoOverrun0               : in  STD_LOGIC;
	        histoOverrun1               : in  STD_LOGIC;
	        busy_mask                   : in  STD_LOGIC_VECTOR (28 downto 0);
	        force_input                 : in  STD_LOGIC_VECTOR (28 downto 0);
           rod_busy_n_out_p            : out STD_LOGIC;
           current_busy_status         : out STD_LOGIC_VECTOR (28 downto 0);
           slink0_lff_hist             : out STD_LOGIC_VECTOR (15 downto 0);
           slink0_ldown_hist           : out STD_LOGIC_VECTOR (15 downto 0);
           slink1_lff_hist             : out STD_LOGIC_VECTOR (15 downto 0);
           slink1_ldown_hist           : out STD_LOGIC_VECTOR (15 downto 0);
           fmt0_ff_0_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt0_ff_1_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt0_ff_2_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt0_ff_3_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt1_ff_0_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt1_ff_1_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt1_ff_2_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt1_ff_3_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt2_ff_0_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt2_ff_1_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt2_ff_2_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt2_ff_3_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt3_ff_0_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt3_ff_1_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt3_ff_2_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           fmt3_ff_3_hist              : out STD_LOGIC_VECTOR (15 downto 0);
           efb1_misc_stts_reg_i_0_hist : out STD_LOGIC_VECTOR (15 downto 0);
	        efb1_misc_stts_reg_i_1_hist : out STD_LOGIC_VECTOR (15 downto 0);
           efb2_misc_stts_reg_i_0_hist : out STD_LOGIC_VECTOR (15 downto 0);
	        efb2_misc_stts_reg_i_1_hist : out STD_LOGIC_VECTOR (15 downto 0);
           router0_busy_i_hist         : out STD_LOGIC_VECTOR (15 downto 0);
	        router1_busy_i_hist         : out STD_LOGIC_VECTOR (15 downto 0);
	        histoFull0_hist             : out STD_LOGIC_VECTOR (15 downto 0);
	        histoFull1_hist             : out STD_LOGIC_VECTOR (15 downto 0);
	        rod_busy_n_out_p_hist       : out STD_LOGIC_VECTOR (15 downto 0);
	        histoOverrun0_hist          : out STD_LOGIC_VECTOR (15 downto 0);
	        histoOverrun1_hist          : out STD_LOGIC_VECTOR (15 downto 0)
	   );
end rod_slave_busy;



architecture Behavioral of rod_slave_busy is

signal rod_busy_n_out_p_i            : STD_LOGIC;

signal slink0_lff_hist_i             : unsigned (15 DOWNTO 0);
signal slink0_ldown_hist_i           : unsigned (15 DOWNTO 0);
signal slink1_lff_hist_i             : unsigned (15 DOWNTO 0);
signal slink1_ldown_hist_i           : unsigned (15 DOWNTO 0);
signal efb1_misc_stts_reg_i_0_hist_i : unsigned (15 DOWNTO 0);
signal efb1_misc_stts_reg_i_1_hist_i : unsigned (15 DOWNTO 0);
signal efb2_misc_stts_reg_i_0_hist_i : unsigned (15 DOWNTO 0);
signal efb2_misc_stts_reg_i_1_hist_i : unsigned (15 DOWNTO 0);
signal router0_busy_i_hist_i         : unsigned (15 DOWNTO 0);
signal router1_busy_i_hist_i         : unsigned (15 DOWNTO 0);
signal histoFull0_hist_i             : unsigned (15 DOWNTO 0);
signal histoFull1_hist_i             : unsigned (15 DOWNTO 0);
signal rod_busy_n_out_p_hist_i       : unsigned (15 DOWNTO 0);
signal histoOverrun0_hist_i          : unsigned (15 DOWNTO 0);
signal histoOverrun1_hist_i          : unsigned (15 DOWNTO 0);
signal fmt0_ff_0_hist_i              : unsigned (15 downto 0);
signal fmt0_ff_1_hist_i              : unsigned (15 downto 0);
signal fmt0_ff_2_hist_i              : unsigned (15 downto 0);
signal fmt0_ff_3_hist_i              : unsigned (15 downto 0);
signal fmt1_ff_0_hist_i              : unsigned (15 downto 0);
signal fmt1_ff_1_hist_i              : unsigned (15 downto 0);
signal fmt1_ff_2_hist_i              : unsigned (15 downto 0);
signal fmt1_ff_3_hist_i              : unsigned (15 downto 0);
signal fmt2_ff_0_hist_i              : unsigned (15 downto 0);
signal fmt2_ff_1_hist_i              : unsigned (15 downto 0);
signal fmt2_ff_2_hist_i              : unsigned (15 downto 0);
signal fmt2_ff_3_hist_i              : unsigned (15 downto 0);
signal fmt3_ff_0_hist_i              : unsigned (15 downto 0);
signal fmt3_ff_1_hist_i              : unsigned (15 downto 0);
signal fmt3_ff_2_hist_i              : unsigned (15 downto 0);
signal fmt3_ff_3_hist_i              : unsigned (15 downto 0);

signal slink0_lff       : STD_LOGIC;
signal slink0_ldown     : STD_LOGIC;
signal slink1_lff       : STD_LOGIC;
signal slink1_ldown     : STD_LOGIC;

signal force_rod_busy_masked         : STD_LOGIC;
signal slink0_lff_masked             : STD_LOGIC;
signal slink0_ldown_masked           : STD_LOGIC;
signal slink1_lff_masked             : STD_LOGIC;
signal slink1_ldown_masked           : STD_LOGIC;
signal efb1_misc_stts_reg_i_masked   : STD_LOGIC_VECTOR (1 downto 0);
signal efb2_misc_stts_reg_i_masked   : STD_LOGIC_VECTOR (1 downto 0);
signal router0_busy_i_masked         : STD_LOGIC;
signal router1_busy_i_masked         : STD_LOGIC;
signal histoFull0_masked             : STD_LOGIC;
signal histoFull1_masked             : STD_LOGIC;
signal fmt0_ff_masked                : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt1_ff_masked                : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt2_ff_masked                : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt3_ff_masked                : STD_LOGIC_VECTOR( 3 downto 0);

--For Histogramming
signal rod_busy_n_out_p_A            : STD_LOGIC;
signal rod_busy_n_out_p_B            : STD_LOGIC;
signal slink0_lff_masked_A           : STD_LOGIC;
signal slink0_ldown_masked_A         : STD_LOGIC;
signal slink1_lff_masked_A           : STD_LOGIC;
signal slink1_ldown_masked_A         : STD_LOGIC;
signal efb1_misc_stts_reg_i_masked_A : STD_LOGIC_VECTOR (1 downto 0);
signal efb2_misc_stts_reg_i_masked_A : STD_LOGIC_VECTOR (1 downto 0);
signal router0_busy_i_masked_A       : STD_LOGIC;
signal router1_busy_i_masked_A       : STD_LOGIC;
signal histoFull0_masked_A           : STD_LOGIC;
signal histoFull1_masked_A           : STD_LOGIC;
signal slink0_lff_masked_B           : STD_LOGIC;
signal slink0_ldown_masked_B         : STD_LOGIC;
signal slink1_lff_masked_B           : STD_LOGIC;
signal slink1_ldown_masked_B         : STD_LOGIC;
signal efb1_misc_stts_reg_i_masked_B : STD_LOGIC_VECTOR (1 downto 0);
signal efb2_misc_stts_reg_i_masked_B : STD_LOGIC_VECTOR (1 downto 0);
signal router0_busy_i_masked_B       : STD_LOGIC;
signal router1_busy_i_masked_B       : STD_LOGIC;
signal histoFull0_masked_B           : STD_LOGIC;
signal histoFull1_masked_B           : STD_LOGIC;
signal fmt0_ff_masked_A              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt1_ff_masked_A              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt2_ff_masked_A              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt3_ff_masked_A              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt0_ff_masked_B              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt1_ff_masked_B              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt2_ff_masked_B              : STD_LOGIC_VECTOR( 3 downto 0);
signal fmt3_ff_masked_B              : STD_LOGIC_VECTOR( 3 downto 0);

begin

-- go to active high logic, if needed
slink0_lff     <= NOT slink0_lff_n;
slink0_ldown   <= NOT slink0_ldown_n;
slink1_lff     <= NOT slink1_lff_n;
slink1_ldown   <= NOT slink1_ldown_n;

-- actual busy logic here. All signals ORed below
force_rod_busy_masked          <= force_input(0) AND NOT busy_mask(0); 
slink0_lff_masked              <= (slink0_lff AND NOT busy_mask(1)) OR force_input(1);
slink0_ldown_masked            <= (slink0_ldown AND NOT busy_mask(2)) OR force_input(2);
slink1_lff_masked              <= (slink1_lff AND NOT busy_mask(3)) OR force_input(3);
slink1_ldown_masked            <= (slink1_ldown AND NOT busy_mask(4)) OR force_input(4);
efb1_misc_stts_reg_i_masked(0) <= (efb1_misc_stts_reg_i(0) AND NOT busy_mask(5)) OR force_input(5);
efb1_misc_stts_reg_i_masked(1) <= (efb1_misc_stts_reg_i(1) AND NOT busy_mask(6)) OR force_input(6);
efb2_misc_stts_reg_i_masked(0) <= (efb2_misc_stts_reg_i(0) AND NOT busy_mask(7)) OR force_input(7);
efb2_misc_stts_reg_i_masked(1) <= (efb2_misc_stts_reg_i(1) AND NOT busy_mask(8)) OR force_input(8);
router0_busy_i_masked          <= (router0_busy_i AND NOT busy_mask(9)) OR force_input(9);
router1_busy_i_masked          <= (router1_busy_i AND NOT busy_mask(10)) OR force_input(10);
histoFull0_masked              <= (histoFull0 AND NOT busy_mask(11)) OR force_input(11);
histoFull1_masked              <= (histoFull1 AND NOT busy_mask(12)) OR force_input(12);
fmt0_ff_masked                 <= (fmt0_ff AND NOT busy_mask(16 downto 13)) OR force_input(16 downto 13);
fmt1_ff_masked                 <= (fmt1_ff AND NOT busy_mask(20 downto 17)) OR force_input(20 downto 17);
fmt2_ff_masked                 <= (fmt2_ff AND NOT busy_mask(24 downto 21)) OR force_input(24 downto 21);
fmt3_ff_masked                 <= (fmt3_ff AND NOT busy_mask(28 downto 25)) OR force_input(28 downto 25);

   --Double Flop b/c they come from 80MHz datapath
	check_rising_edge0: process (clk40in, reset)
	begin
		if(reset = '1') then
			rod_busy_n_out_p_A <= '0';
			rod_busy_n_out_p_B <= '0';	
		elsif rising_edge (clk40in) then
			rod_busy_n_out_p_A <= rod_busy_n_out_p_i;
			rod_busy_n_out_p_B <= rod_busy_n_out_p_A;
		end if;	
	end process check_rising_edge0;
	
	check_rising_edge1: process (clk40in, reset)
	begin
		if(reset = '1') then
			slink0_lff_masked_A <= '0';
			slink0_lff_masked_B <= '0';
		elsif rising_edge (clk40in) then
			slink0_lff_masked_A <= slink0_lff_masked;
			slink0_lff_masked_B <= slink0_lff_masked_A;
		end if;	
	end process check_rising_edge1;

	check_rising_edge2: process (clk40in, reset)
	begin
		if(reset = '1') then
			slink0_ldown_masked_A <= '0';
			slink0_ldown_masked_B <= '0';
		elsif rising_edge (clk40in) then
			slink0_ldown_masked_A <= slink0_ldown_masked;
			slink0_ldown_masked_B <= slink0_ldown_masked_A;
		end if;	
	end process check_rising_edge2;
	
	check_rising_edge3: process (clk40in, reset)
	begin
		if(reset = '1') then
			slink1_lff_masked_A <= '0';
			slink1_lff_masked_B <= '0';
		elsif rising_edge (clk40in) then
			slink1_lff_masked_A <= slink1_lff_masked;
			slink1_lff_masked_B <= slink1_lff_masked_A;
		end if;
	end process check_rising_edge3;
	
	check_rising_edge4: process (clk40in, reset)
	begin
		if(reset = '1') then
			slink1_ldown_masked_A <= '0';
			slink1_ldown_masked_B <= '0';
		elsif rising_edge (clk40in) then
			slink1_ldown_masked_A <= slink1_ldown_masked;
			slink1_ldown_masked_B <= slink1_ldown_masked_A;
		end if;
	end process check_rising_edge4;
	
	check_rising_edge5: process (clk40in, reset)
	begin
		if(reset = '1') then
			efb1_misc_stts_reg_i_masked_A(0) <= '0';
			efb1_misc_stts_reg_i_masked_B(0) <= '0';
		elsif rising_edge (clk40in) then
			efb1_misc_stts_reg_i_masked_A(0) <= efb1_misc_stts_reg_i_masked(0);
			efb1_misc_stts_reg_i_masked_B(0) <= efb1_misc_stts_reg_i_masked_A(0);
		end if;	
	end process check_rising_edge5;
	
	check_rising_edge6: process (clk40in, reset)
	begin
		if(reset = '1') then
			efb1_misc_stts_reg_i_masked_A(1) <= '0';
			efb1_misc_stts_reg_i_masked_B(1) <= '0';
		elsif rising_edge (clk40in) then
			efb1_misc_stts_reg_i_masked_A(1) <= efb1_misc_stts_reg_i_masked(1);
			efb1_misc_stts_reg_i_masked_B(1) <= efb1_misc_stts_reg_i_masked_A(1);
		end if;	
	end process check_rising_edge6;	
	
	check_rising_edge7: process (clk40in, reset)
	begin
		if(reset = '1') then
			efb2_misc_stts_reg_i_masked_A(0) <= '0';
			efb2_misc_stts_reg_i_masked_B(0) <= '0';
		elsif rising_edge (clk40in) then
			efb2_misc_stts_reg_i_masked_A(0) <= efb2_misc_stts_reg_i_masked(0);
			efb2_misc_stts_reg_i_masked_B(0) <= efb2_misc_stts_reg_i_masked_A(0);
		end if;	
	end process check_rising_edge7;
	
	check_rising_edge8: process (clk40in, reset)
	begin
		if(reset = '1') then
			efb2_misc_stts_reg_i_masked_A(1) <= '0';
			efb2_misc_stts_reg_i_masked_B(1) <= '0';
		elsif rising_edge (clk40in) then
			efb2_misc_stts_reg_i_masked_A(1) <= efb2_misc_stts_reg_i_masked(1);
			efb2_misc_stts_reg_i_masked_B(1) <= efb2_misc_stts_reg_i_masked_A(1);
		end if;	
	end process check_rising_edge8;
	
	check_rising_edge9: process (clk40in, reset)
	begin
		if(reset = '1') then
			router0_busy_i_masked_A <= '0';
			router0_busy_i_masked_B <= '0';
		elsif rising_edge (clk40in) then
			router0_busy_i_masked_A <= router0_busy_i_masked;
			router0_busy_i_masked_B <= router0_busy_i_masked_A;
		end if;	
	end process check_rising_edge9;

	check_rising_edge10: process (clk40in, reset)
	begin
		if(reset = '1') then
			router1_busy_i_masked_A <= '0';
			router1_busy_i_masked_B <= '0';
		elsif rising_edge (clk40in) then
			router1_busy_i_masked_A <= router1_busy_i_masked;
			router1_busy_i_masked_B <= router1_busy_i_masked_A;
		end if;	
	end process check_rising_edge10;

	check_rising_edge11: process (clk40in, reset)
	begin
		if(reset = '1') then
			histoFull0_masked_A <= '0';
			histoFull0_masked_B <= '0';
		elsif rising_edge (clk40in) then
			histoFull0_masked_A <= histoFull0_masked;
			histoFull0_masked_B <= histoFull0_masked_A;
		end if;	
	end process check_rising_edge11;
	
	check_rising_edge12: process (clk40in, reset)
	begin
		if(reset = '1') then
			histoFull1_masked_A <= '0';
			histoFull1_masked_B <= '0';
		elsif rising_edge (clk40in) then
			histoFull1_masked_A <= histoFull1_masked;
			histoFull1_masked_B <= histoFull1_masked_A;
		end if;	
	end process check_rising_edge12;

   check_rising_edge13: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt0_ff_masked_A(0) <= '0';
         fmt0_ff_masked_B(0) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt0_ff_masked_A(0) <= fmt0_ff_masked(0);
         fmt0_ff_masked_B(0) <= fmt0_ff_masked_A(0);
      end if;
   end process check_rising_edge13;
	
   check_rising_edge14: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt0_ff_masked_A(1) <= '0';
         fmt0_ff_masked_B(1) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt0_ff_masked_A(1) <= fmt0_ff_masked(1);
         fmt0_ff_masked_B(1) <= fmt0_ff_masked_A(1);
      end if;
   end process check_rising_edge14;
	
   check_rising_edge15: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt0_ff_masked_A(2) <= '0';
         fmt0_ff_masked_B(2) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt0_ff_masked_A(2) <= fmt0_ff_masked(2);
         fmt0_ff_masked_B(2) <= fmt0_ff_masked_A(2);
      end if;
   end process check_rising_edge15;

   check_rising_edge16: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt0_ff_masked_A(3) <= '0';
         fmt0_ff_masked_B(3) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt0_ff_masked_A(3) <= fmt0_ff_masked(3);
         fmt0_ff_masked_B(3) <= fmt0_ff_masked_A(3);
      end if;
   end process check_rising_edge16;

   check_rising_edge17: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt1_ff_masked_A(0) <= '0';
         fmt1_ff_masked_B(0) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt1_ff_masked_A(0) <= fmt1_ff_masked(0);
         fmt1_ff_masked_B(0) <= fmt1_ff_masked_A(0);
      end if;
   end process check_rising_edge17;

   check_rising_edge18: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt1_ff_masked_A(1) <= '0';
         fmt1_ff_masked_B(1) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt1_ff_masked_A(1) <= fmt1_ff_masked(1);
         fmt1_ff_masked_B(1) <= fmt1_ff_masked_A(1);
      end if;
   end process check_rising_edge18;

   check_rising_edge19: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt1_ff_masked_A(2) <= '0';
         fmt1_ff_masked_B(2) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt1_ff_masked_A(2) <= fmt1_ff_masked(2);
         fmt1_ff_masked_B(2) <= fmt1_ff_masked_A(2);
      end if;
   end process check_rising_edge19;

   check_rising_edge20: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt1_ff_masked_A(3) <= '0';
         fmt1_ff_masked_B(3) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt1_ff_masked_A(3) <= fmt1_ff_masked(3);
         fmt1_ff_masked_B(3) <= fmt1_ff_masked_A(3);
      end if;
   end process check_rising_edge20;

   check_rising_edge21: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt2_ff_masked_A(0) <= '0';
         fmt2_ff_masked_B(0) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt2_ff_masked_A(0) <= fmt2_ff_masked(0);
         fmt2_ff_masked_B(0) <= fmt2_ff_masked_A(0);
      end if;
   end process check_rising_edge21;

   check_rising_edge28: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt2_ff_masked_A(1) <= '0';
         fmt2_ff_masked_B(1) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt2_ff_masked_A(1) <= fmt2_ff_masked(1);
         fmt2_ff_masked_B(1) <= fmt2_ff_masked_A(1);
      end if;
   end process check_rising_edge28;

   check_rising_edge22: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt2_ff_masked_A(2) <= '0';
         fmt2_ff_masked_B(2) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt2_ff_masked_A(2) <= fmt2_ff_masked(2);
         fmt2_ff_masked_B(2) <= fmt2_ff_masked_A(2);
      end if;
   end process check_rising_edge22;

   check_rising_edge23: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt2_ff_masked_A(3) <= '0';
         fmt2_ff_masked_B(3) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt2_ff_masked_A(3) <= fmt2_ff_masked(3);
         fmt2_ff_masked_B(3) <= fmt2_ff_masked_A(3);
      end if;
   end process check_rising_edge23;

   check_rising_edge24: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt3_ff_masked_A(0) <= '0';
         fmt3_ff_masked_B(0) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt3_ff_masked_A(0) <= fmt3_ff_masked(0);
         fmt3_ff_masked_B(0) <= fmt3_ff_masked_A(0);
      end if;
   end process check_rising_edge24;

   check_rising_edge25: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt3_ff_masked_A(1) <= '0';
         fmt3_ff_masked_B(1) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt3_ff_masked_A(1) <= fmt3_ff_masked(1);
         fmt3_ff_masked_B(1) <= fmt3_ff_masked_A(1);
      end if;
   end process check_rising_edge25;

   check_rising_edge26: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt3_ff_masked_A(2) <= '0';
         fmt3_ff_masked_B(2) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt3_ff_masked_A(2) <= fmt3_ff_masked(2);
         fmt3_ff_masked_B(2) <= fmt3_ff_masked_A(2);
      end if;
   end process check_rising_edge26;

   check_rising_edge27: process(clk40in, reset)
   begin
      if(reset = '1') then
         fmt3_ff_masked_A(3) <= '0';
         fmt3_ff_masked_B(3) <= '0';
      elsif (rising_edge(clk40in)) then
         fmt3_ff_masked_A(3) <= fmt3_ff_masked(3);
         fmt3_ff_masked_B(3) <= fmt3_ff_masked_A(3);
      end if;
   end process check_rising_edge27;

  busy_out_process: process(reset, reset_hists, force_rod_busy_masked, slink0_lff_masked, 
  slink0_ldown_masked, slink1_lff_masked, slink1_ldown_masked, efb1_misc_stts_reg_i_masked,
  efb2_misc_stts_reg_i_masked, router0_busy_i_masked, router1_busy_i_masked, histoFull0_masked, histoFull1_masked,
  fmt0_ff_masked,fmt1_ff_masked,fmt2_ff_masked,fmt3_ff_masked)
  begin
    if(reset = '1') then
      rod_busy_n_out_p    <= '0';
		rod_busy_n_out_p_i  <= '0';

		current_busy_status <= "00000000000000000000000000000";
		
    else	 
		
      rod_busy_n_out_p <= NOT(
			  force_rod_busy_masked OR -- force busy functionality
			  slink0_lff_masked     OR 
           slink0_ldown_masked   OR 
           slink1_lff_masked     OR 
			  slink1_ldown_masked   OR								  
           efb1_misc_stts_reg_i_masked(0) OR
			  efb1_misc_stts_reg_i_masked(1) OR
			  efb2_misc_stts_reg_i_masked(0) OR
			  efb2_misc_stts_reg_i_masked(1) OR
           router0_busy_i_masked OR
			  router1_busy_i_masked OR
			  histoFull0_masked     OR
			  histoFull1_masked     OR
           fmt0_ff_masked(0)     OR
           fmt0_ff_masked(1)     OR
           fmt0_ff_masked(2)     OR
           fmt0_ff_masked(3)     OR
           fmt1_ff_masked(0)     OR
           fmt1_ff_masked(1)     OR
           fmt1_ff_masked(2)     OR
           fmt1_ff_masked(3)     OR
           fmt2_ff_masked(0)     OR
           fmt2_ff_masked(1)     OR
           fmt2_ff_masked(2)     OR
           fmt2_ff_masked(3)     OR
           fmt3_ff_masked(0)     OR
           fmt3_ff_masked(1)     OR
           fmt3_ff_masked(2)     OR
           fmt3_ff_masked(3)
			  );

					
      rod_busy_n_out_p_i <= NOT(
			 force_rod_busy_masked OR -- force busy functionality (set via mask(0))
			 slink0_lff_masked     OR 
          slink0_ldown_masked   OR 
          slink1_lff_masked     OR 
			 slink1_ldown_masked   OR								  
          efb1_misc_stts_reg_i_masked(0) OR
			 efb1_misc_stts_reg_i_masked(1) OR
			 efb2_misc_stts_reg_i_masked(0) OR
			 efb2_misc_stts_reg_i_masked(1) OR
          router0_busy_i_masked OR
			 router1_busy_i_masked OR
			 histoFull0_masked     OR
			 histoFull1_masked     OR
          fmt0_ff_masked(0)     OR
          fmt0_ff_masked(1)     OR
          fmt0_ff_masked(2)     OR
          fmt0_ff_masked(3)     OR
          fmt1_ff_masked(0)     OR
          fmt1_ff_masked(1)     OR
          fmt1_ff_masked(2)     OR
          fmt1_ff_masked(3)     OR
          fmt2_ff_masked(0)     OR
          fmt2_ff_masked(1)     OR
          fmt2_ff_masked(2)     OR
          fmt2_ff_masked(3)     OR
          fmt3_ff_masked(0)     OR
          fmt3_ff_masked(1)     OR
          fmt3_ff_masked(2)     OR
          fmt3_ff_masked(3) 
			 );
								  
		current_busy_status(0)            <= rod_busy_n_out_p_i;
		current_busy_status(1)            <= slink0_lff;
		current_busy_status(2)            <= slink0_ldown;
		current_busy_status(3)            <= slink1_lff;
		current_busy_status(4)            <= slink1_ldown;
		current_busy_status(5)            <= efb1_misc_stts_reg_i(0);
		current_busy_status(6)            <= efb1_misc_stts_reg_i(1);
		current_busy_status(7)            <= efb2_misc_stts_reg_i(0);
		current_busy_status(8)            <= efb2_misc_stts_reg_i(1);
		current_busy_status(9)            <= router0_busy_i;
		current_busy_status(10)           <= router1_busy_i;
		current_busy_status(11)           <= histoFull0;
		current_busy_status(12)           <= histoFull1;
      current_busy_status(16 downto 13) <= fmt0_ff;
      current_busy_status(20 downto 17) <= fmt1_ff;
      current_busy_status(24 downto 21) <= fmt2_ff;
      current_busy_status(28 downto 25) <= fmt3_ff;
	
	end if;
end process busy_out_process;
	 

-- histogramming of busy sources
busy_hist_process: process(clk40in, reset, reset_hists)
  begin
    if(reset = '1' OR reset_hists = '1') then
      slink0_lff_hist_i             <= "0000000000000000";
		slink0_ldown_hist_i           <= "0000000000000000";
		slink1_lff_hist_i             <= "0000000000000000";
		slink1_ldown_hist_i           <= "0000000000000000";
		efb1_misc_stts_reg_i_0_hist_i <= "0000000000000000";
		efb1_misc_stts_reg_i_1_hist_i <= "0000000000000000";
		efb2_misc_stts_reg_i_0_hist_i <= "0000000000000000";
		efb2_misc_stts_reg_i_1_hist_i <= "0000000000000000";
		router0_busy_i_hist_i         <= "0000000000000000";
		router1_busy_i_hist_i         <= "0000000000000000";
		histoFull0_hist_i             <= "0000000000000000";
		histoFull1_hist_i             <= "0000000000000000";
		rod_busy_n_out_p_hist_i       <= "0000000000000000";
		histoOverrun0_hist_i          <= "0000000000000000";
		histoOverrun1_hist_i          <= "0000000000000000";
      fmt0_ff_0_hist_i              <= "0000000000000000"; 
      fmt0_ff_1_hist_i              <= "0000000000000000"; 
      fmt0_ff_2_hist_i              <= "0000000000000000"; 
      fmt0_ff_3_hist_i              <= "0000000000000000"; 
      fmt1_ff_0_hist_i              <= "0000000000000000"; 
      fmt1_ff_1_hist_i              <= "0000000000000000"; 
      fmt1_ff_2_hist_i              <= "0000000000000000"; 
      fmt1_ff_3_hist_i              <= "0000000000000000"; 
      fmt2_ff_0_hist_i              <= "0000000000000000"; 
      fmt2_ff_1_hist_i              <= "0000000000000000"; 
      fmt2_ff_2_hist_i              <= "0000000000000000"; 
      fmt2_ff_3_hist_i              <= "0000000000000000"; 
      fmt3_ff_0_hist_i              <= "0000000000000000"; 
      fmt3_ff_1_hist_i              <= "0000000000000000"; 
      fmt3_ff_2_hist_i              <= "0000000000000000"; 
      fmt3_ff_3_hist_i              <= "0000000000000000"; 
	elsif rising_edge (clk40in) then 
	
			if ((rod_busy_n_out_p_A = '0') AND (rod_busy_n_out_p_B = '1')) then	
				rod_busy_n_out_p_hist_i <= rod_busy_n_out_p_hist_i + "0000000000000001";
			end if;
			
			if ((slink0_lff_masked_A = '1') AND (slink0_lff_masked_B = '0')) then	
				slink0_lff_hist_i <= slink0_lff_hist_i + "0000000000000001";	
			end if;	
			
			if ((slink0_ldown_masked_A = '1') AND (slink0_ldown_masked_B = '0')) then	
				slink0_ldown_hist_i <= slink0_ldown_hist_i + "0000000000000001";	
			end if;
			
			if ((slink1_lff_masked_A = '1') AND (slink1_lff_masked_B = '0')) then	
				slink1_lff_hist_i <= slink1_lff_hist_i + "0000000000000001";	
			end if;
			
			if ((slink1_ldown_masked_A = '1') AND (slink1_ldown_masked_B = '0')) then	
				slink1_ldown_hist_i <= slink1_ldown_hist_i + "0000000000000001";	
			end if;

         if ((fmt0_ff_masked_A(0) = '1') AND (fmt0_ff_masked_B(0) = '0')) then	
				fmt0_ff_0_hist_i <= fmt0_ff_0_hist_i + "0000000000000001";	
			end if;
         if ((fmt0_ff_masked_A(1) = '1') AND (fmt0_ff_masked_B(1) = '0')) then	
				fmt0_ff_1_hist_i <= fmt0_ff_1_hist_i + "0000000000000001";	
			end if;
         if ((fmt0_ff_masked_A(2) = '1') AND (fmt0_ff_masked_B(2) = '0')) then	
				fmt0_ff_2_hist_i <= fmt0_ff_2_hist_i + "0000000000000001";	
			end if;
         if ((fmt0_ff_masked_A(3) = '1') AND (fmt0_ff_masked_B(3) = '0')) then	
				fmt0_ff_3_hist_i <= fmt0_ff_3_hist_i + "0000000000000001";	
			end if;

	      if ((fmt1_ff_masked_A(0) = '1') AND (fmt1_ff_masked_B(0) = '0')) then	
				fmt1_ff_0_hist_i <= fmt1_ff_0_hist_i + "0000000000000001";	
			end if;
         if ((fmt1_ff_masked_A(1) = '1') AND (fmt1_ff_masked_B(1) = '0')) then	
				fmt1_ff_1_hist_i <= fmt1_ff_1_hist_i + "0000000000000001";	
			end if;
         if ((fmt1_ff_masked_A(2) = '1') AND (fmt1_ff_masked_B(2) = '0')) then	
				fmt1_ff_2_hist_i <= fmt1_ff_2_hist_i + "0000000000000001";	
			end if;
         if ((fmt1_ff_masked_A(3) = '1') AND (fmt1_ff_masked_B(3) = '0')) then	
				fmt1_ff_3_hist_i <= fmt1_ff_3_hist_i + "0000000000000001";	
			end if;

	      if ((fmt2_ff_masked_A(0) = '1') AND (fmt2_ff_masked_B(0) = '0')) then	
				fmt2_ff_0_hist_i <= fmt2_ff_0_hist_i + "0000000000000001";	
			end if;
         if ((fmt2_ff_masked_A(1) = '1') AND (fmt2_ff_masked_B(1) = '0')) then	
				fmt2_ff_1_hist_i <= fmt2_ff_1_hist_i + "0000000000000001";	
			end if;
         if ((fmt2_ff_masked_A(2) = '1') AND (fmt2_ff_masked_B(2) = '0')) then	
				fmt2_ff_2_hist_i <= fmt2_ff_2_hist_i + "0000000000000001";	
			end if;
         if ((fmt2_ff_masked_A(3) = '1') AND (fmt2_ff_masked_B(3) = '0')) then	
				fmt2_ff_3_hist_i <= fmt2_ff_3_hist_i + "0000000000000001";	
			end if;

	      if ((fmt3_ff_masked_A(0) = '1') AND (fmt3_ff_masked_B(0) = '0')) then	
				fmt3_ff_0_hist_i <= fmt3_ff_0_hist_i + "0000000000000001";	
			end if;
         if ((fmt3_ff_masked_A(1) = '1') AND (fmt3_ff_masked_B(1) = '0')) then	
				fmt3_ff_1_hist_i <= fmt3_ff_1_hist_i + "0000000000000001";	
			end if;
         if ((fmt3_ff_masked_A(2) = '1') AND (fmt3_ff_masked_B(2) = '0')) then	
				fmt3_ff_2_hist_i <= fmt3_ff_2_hist_i + "0000000000000001";	
			end if;
         if ((fmt3_ff_masked_A(3) = '1') AND (fmt3_ff_masked_B(3) = '0')) then	
				fmt3_ff_3_hist_i <= fmt3_ff_3_hist_i + "0000000000000001";	
			end if;
		
			if ((efb1_misc_stts_reg_i_masked_A(0) = '1') AND (efb1_misc_stts_reg_i_masked_B(0) = '0')) then
				efb1_misc_stts_reg_i_0_hist_i <= efb1_misc_stts_reg_i_0_hist_i + "0000000000000001";
			end if;

			if ((efb1_misc_stts_reg_i_masked_A(1) = '1') AND (efb1_misc_stts_reg_i_masked_B(1) = '0')) then
				efb1_misc_stts_reg_i_1_hist_i <= efb1_misc_stts_reg_i_1_hist_i + "0000000000000001";
			end if;
  
			if ((efb2_misc_stts_reg_i_masked_A(0) = '1') AND (efb2_misc_stts_reg_i_masked_B(0) = '0')) then
				efb2_misc_stts_reg_i_0_hist_i <= efb2_misc_stts_reg_i_0_hist_i + "0000000000000001";
			end if;

			if ((efb2_misc_stts_reg_i_masked_A(1) = '1') AND (efb2_misc_stts_reg_i_masked_B(1) = '0')) then
				efb2_misc_stts_reg_i_1_hist_i <= efb2_misc_stts_reg_i_1_hist_i + "0000000000000001";
			end if;  
  
			if  ((router0_busy_i_masked_A = '1') AND (router0_busy_i_masked_B = '0')) then
				router0_busy_i_hist_i <= router0_busy_i_hist_i + "0000000000000001";
			end if;	
		
			if  ((router1_busy_i_masked_A = '1') AND (router1_busy_i_masked_B = '0')) then
				router1_busy_i_hist_i <= router1_busy_i_hist_i + "0000000000000001";
			end if;

			if  ((histoFull0_masked_A = '1') AND (histoFull0_masked_B = '0')) then
				histoFull0_hist_i <= histoFull0_hist_i + "0000000000000001";
			end if;	

			if  ((histoFull1_masked_A = '1') AND (histoFull1_masked_B = '0')) then
				histoFull1_hist_i <= histoFull1_hist_i + "0000000000000001";
			end if;	
			
			if  (histoOverrun0 = '1') then
				histoOverrun0_hist_i <= histoOverrun0_hist_i + "0000000000000001";
			end if;	
			
			if  (histoOverrun1 = '1') then
				histoOverrun1_hist_i <= histoOverrun1_hist_i + "0000000000000001";
			end if;
  end if;
end process busy_hist_process;	

	
busy_hists_out_process: process(
	reset, 
	reset_hists, 
	slink0_lff_hist_i, 
	slink0_ldown_hist_i,
	slink1_lff_hist_i, 
	slink1_ldown_hist_i, 
	efb1_misc_stts_reg_i_0_hist_i, 
	efb1_misc_stts_reg_i_1_hist_i, 
	efb2_misc_stts_reg_i_0_hist_i,
	efb2_misc_stts_reg_i_1_hist_i,
	router0_busy_i_hist_i,
	router1_busy_i_hist_i,
	histoFull0_hist_i,
	histoFull1_hist_i,
	rod_busy_n_out_p_hist_i,
	histoOverrun0_hist_i,
	histoOverrun1_hist_i,
   fmt0_ff_0_hist_i,
   fmt0_ff_1_hist_i,
   fmt0_ff_2_hist_i,
   fmt0_ff_3_hist_i,
   fmt1_ff_0_hist_i,
   fmt1_ff_1_hist_i,
   fmt1_ff_2_hist_i,
   fmt1_ff_3_hist_i,
   fmt2_ff_0_hist_i,
   fmt2_ff_1_hist_i,
   fmt2_ff_2_hist_i,
   fmt2_ff_3_hist_i,
   fmt3_ff_0_hist_i,
   fmt3_ff_1_hist_i,
   fmt3_ff_2_hist_i,
   fmt3_ff_3_hist_i
	)
  
 begin	
	 if((reset_hists = '1') OR (reset = '1')) then
	   slink0_lff_hist             <= "0000000000000000";
		slink0_ldown_hist           <= "0000000000000000";
		slink1_lff_hist             <= "0000000000000000";
		slink1_ldown_hist           <= "0000000000000000";
		efb1_misc_stts_reg_i_0_hist <= "0000000000000000";
		efb1_misc_stts_reg_i_1_hist <= "0000000000000000";
		efb2_misc_stts_reg_i_0_hist <= "0000000000000000";
		efb2_misc_stts_reg_i_1_hist <= "0000000000000000";
		router0_busy_i_hist         <= "0000000000000000";
		router1_busy_i_hist         <= "0000000000000000";
		histoFull0_hist             <= "0000000000000000";
		histoFull1_hist             <= "0000000000000000";
		rod_busy_n_out_p_hist       <= "0000000000000000";
		histoOverrun0_hist          <= "0000000000000000";
		histoOverrun1_hist          <= "0000000000000000";
      fmt0_ff_0_hist              <= "0000000000000000"; 
      fmt0_ff_1_hist              <= "0000000000000000"; 
      fmt0_ff_2_hist              <= "0000000000000000"; 
      fmt0_ff_3_hist              <= "0000000000000000"; 
      fmt1_ff_0_hist              <= "0000000000000000"; 
      fmt1_ff_1_hist              <= "0000000000000000"; 
      fmt1_ff_2_hist              <= "0000000000000000"; 
      fmt1_ff_3_hist              <= "0000000000000000"; 
      fmt2_ff_0_hist              <= "0000000000000000"; 
      fmt2_ff_1_hist              <= "0000000000000000"; 
      fmt2_ff_2_hist              <= "0000000000000000"; 
      fmt2_ff_3_hist              <= "0000000000000000"; 
      fmt3_ff_0_hist              <= "0000000000000000"; 
      fmt3_ff_1_hist              <= "0000000000000000"; 
      fmt3_ff_2_hist              <= "0000000000000000"; 
      fmt3_ff_3_hist              <= "0000000000000000"; 
				
    else	 						  
		slink0_lff_hist             <= std_logic_vector(slink0_lff_hist_i);
		slink0_ldown_hist           <= std_logic_vector(slink0_ldown_hist_i);
		slink1_lff_hist             <= std_logic_vector(slink1_lff_hist_i);
		slink1_ldown_hist           <= std_logic_vector(slink1_ldown_hist_i);
		efb1_misc_stts_reg_i_0_hist <= std_logic_vector(efb1_misc_stts_reg_i_0_hist_i);
		efb1_misc_stts_reg_i_1_hist <= std_logic_vector(efb1_misc_stts_reg_i_1_hist_i);
		efb2_misc_stts_reg_i_0_hist <= std_logic_vector(efb2_misc_stts_reg_i_0_hist_i);
		efb2_misc_stts_reg_i_1_hist <= std_logic_vector(efb2_misc_stts_reg_i_1_hist_i);
		router0_busy_i_hist         <= std_logic_vector(router0_busy_i_hist_i);
		router1_busy_i_hist         <= std_logic_vector(router1_busy_i_hist_i);
		histoFull0_hist             <= std_logic_vector(histoFull0_hist_i);
		histoFull1_hist             <= std_logic_vector(histoFull1_hist_i);
		rod_busy_n_out_p_hist       <= std_logic_vector(rod_busy_n_out_p_hist_i);
		histoOverrun0_hist          <= std_logic_vector(histoOverrun0_hist_i);
		histoOverrun1_hist          <= std_logic_vector(histoOverrun1_hist_i);
      fmt0_ff_0_hist              <= std_logic_vector(fmt0_ff_0_hist_i);  
      fmt0_ff_1_hist              <= std_logic_vector(fmt0_ff_1_hist_i);   
      fmt0_ff_2_hist              <= std_logic_vector(fmt0_ff_2_hist_i);   
      fmt0_ff_3_hist              <= std_logic_vector(fmt0_ff_3_hist_i);   
      fmt1_ff_0_hist              <= std_logic_vector(fmt1_ff_0_hist_i);   
      fmt1_ff_1_hist              <= std_logic_vector(fmt1_ff_1_hist_i);   
      fmt1_ff_2_hist              <= std_logic_vector(fmt1_ff_2_hist_i);   
      fmt1_ff_3_hist              <= std_logic_vector(fmt1_ff_3_hist_i);   
      fmt2_ff_0_hist              <= std_logic_vector(fmt2_ff_0_hist_i);   
      fmt2_ff_1_hist              <= std_logic_vector(fmt2_ff_1_hist_i);   
      fmt2_ff_2_hist              <= std_logic_vector(fmt2_ff_2_hist_i);   
      fmt2_ff_3_hist              <= std_logic_vector(fmt2_ff_3_hist_i);   
      fmt3_ff_0_hist              <= std_logic_vector(fmt3_ff_0_hist_i);   
      fmt3_ff_1_hist              <= std_logic_vector(fmt3_ff_1_hist_i);   
      fmt3_ff_2_hist              <= std_logic_vector(fmt3_ff_2_hist_i);   
      fmt3_ff_3_hist              <= std_logic_vector(fmt3_ff_3_hist_i);   
	end if;                            
end process busy_hists_out_process;	
	
end Behavioral;

