--------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- SiROD ReadOutDriver Electronics
--------------------------------------------------------------------------
-- Filename: ram_block.vhd
-- Description: block memory parallel to register block

--------------------------------------------------------------------------
-- Structure:
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author: A. Kugel, based upon code from Johm M. Joseph
-- History: 2012-07-08
--------------------------------------------------------------------------
-- Notes:
--------------------------------------------------------------------------
--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------
entity ram_block is
  port (
	clk40                  : in  std_logic; -- clk40 input
	rst                    : in  std_logic; -- async global reset
	rod_bus_strb_in        : in  std_logic; --
	rod_bus_rnw_in         : in  std_logic; --
	rod_bus_hwsb_in        : in  std_logic; --
	rod_bus_ds_in          : in  std_logic; --
	rod_bus_addr_in        : in  std_logic_vector(9 downto 0); --
	rod_bus_data_in        : in  std_logic_vector(15 downto 0); --
	rod_bus_data_out       : out std_logic_vector(15 downto 0); --
	rod_bus_ack_out        : out std_logic;	
	cpuClk : in std_logic;
	cpuWr : in std_logic;
	cpuAddr : IN  std_logic_vector(9 downto 0);
	cpuDout : IN  std_logic_vector(31 downto 0);
	cpuDin : out  std_logic_vector(31 downto 0);
	ctlReg : out  std_logic_vector(31 downto 0)
    );   
end ram_block;

architecture rtl of ram_block is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  idle, 
  acknowledge, 
  wait_cycle
  );
  
signal acknowledge_cycle_state : states;


------------------------------------------------------------------------------
-- COMPONENT DECLARATION
-------------------------------------------------------------------------------
	COMPONENT dpr1kx32
	  PORT (
		 clka : IN STD_LOGIC;
		 wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		 addra : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 dina : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 douta : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
		 clkb : IN STD_LOGIC;
		 web : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
		 addrb : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 dinb : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
		 doutb : OUT STD_LOGIC_VECTOR(31 DOWNTO 0)
	  );
	end component;

   signal ramAddrA : std_logic_vector(9 downto 0);
   signal ramDinA, ramDoutA : std_logic_vector(31 downto 0);
   signal ramDinB, ramDoutB : std_logic_vector(31 downto 0) := (others => '0');
   signal ramWrA  : std_logic_vector(0 downto 0);
   signal ramRdA  : std_logic;
   signal doWriteA  : std_logic;
   signal ramRdB : std_logic := '0';
   signal ramWrB : std_logic_vector(0 downto 0) := (others => '0');
	

begin

  ramWrB(0) <= cpuWr;
  
  theRam : dpr1kx32
  PORT MAP (
    clka => clk40,
    wea => ramWrA,
    addra => ramAddrA,
    dina => ramDinA,
    douta => ramDoutA,
    clkb => cpuClk, --clk40_in,
    web => ramWrB,
    addrb => cpuAddr, --ramAddrB,
    dinb => cpuDout, -- ramDinB,
    doutb => cpuDin --ramDoutB
  );


-------------------------------------------------------------------------------
-- Process to Enable Read/Write to Router Registers
-------------------------------------------------------------------------------
wr_ram : process (
  clk40, 
  rst,
  rod_bus_addr_in,
  rod_bus_rnw_in,
  rod_bus_strb_in
  )
  
begin
	if (rst = '1') then
		doWriteA <= '0';
		ramAddrA	<= (others => '1');
	elsif (clk40'event AND clk40 = '1') then
		if rod_bus_strb_in = '1' then
			-- capture address while strb active. input addresses are word address
			ramAddrA <= rod_bus_addr_in(9 downto 0);
			--Write ram
			if rod_bus_rnw_in = '0'then
				if (rod_bus_hwsb_in = '0' AND rod_bus_ds_in = '1') then
					ramDinA(15 downto  0) <= rod_bus_data_in;
					doWriteA <= '0';
				elsif (rod_bus_hwsb_in = '1' AND rod_bus_ds_in = '1') then
					ramDinA(31 downto 16) <= rod_bus_data_in;
					doWriteA <= '1';
				else
					doWriteA <= '0';
				end if;
				ramWrA(0) <= doWriteA; -- delayed by 1 cycle to have data captured already
			end if;
		end if;
	end if;
  
end process;

	--Read ram
rd_ram:block
begin
	rod_bus_data_out <= ramDoutA(15 downto  0) when rod_bus_hwsb_in = '0' else ramDoutA(31 downto 16);
end block;

--------------------------------------------------------------------------------------
--------------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Logic to generate Access Acknowledge
-------------------------------------------------------------------------------
access_acknowledge : process (
  rst, 
  clk40, 
  rod_bus_strb_in
  )
begin                      
  if (rst = '1') then
    rod_bus_ack_out <= '0';
    acknowledge_cycle_state <= idle;
  elsif (clk40'event and clk40 = '1') then
    case acknowledge_cycle_state is
      when idle => 
        rod_bus_ack_out <= '0';
        if (rod_bus_strb_in = '1') then 
          acknowledge_cycle_state <= acknowledge;
        end if;
       
      when acknowledge => 
        rod_bus_ack_out <= '1';
        if (rod_bus_strb_in = '0') then 
          acknowledge_cycle_state <= idle;
        end if;

      when others =>
        rod_bus_ack_out <= '0';
        acknowledge_cycle_state <= idle;
    end case;
  end if;  
end process access_acknowledge;     

end rtl; -- code of ram_block