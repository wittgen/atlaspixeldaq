library ieee;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.gitIdPack.all;

entity gitId_tb is
end entity;

architecture arch of gitId_tb is 

component gitId
port (
  reset: in std_logic;
  read: in std_logic;
  id: out std_logic_vector(7 downto 0)
);
end component;

signal reset: std_logic;
signal read: std_logic;
signal id: std_logic_vector(7 downto 0);

begin

  hash: gitId port map (reset, read ,id);

  process
  begin
    reset <= '1';
    read <= '0';
    wait for 10 ns;
    reset <= '0';
    wait for 10 ns;
    
    for i in 0 to gitIdLength - 1 loop
      read <= '1';
      wait for 5 ns;
      read <= '0';
      wait for 5 ns;
    end loop;

    wait;
    end process;
  
end arch;

