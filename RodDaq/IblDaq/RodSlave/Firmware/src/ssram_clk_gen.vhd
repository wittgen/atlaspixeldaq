----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:54:44 09/19/2013 
-- Design Name: 
-- Module Name:    ssram_clk_gen - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;
use ieee.numeric_std.all;

library unisim;
use unisim.vcomponents.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ssram_clk_gen is
port
 (-- Clock in ports
  CLK_IN           : in     std_logic;
  CLK_SRAM_IN1      : in     std_logic;
  CLK_SRAM_IN2      : in     std_logic;  
  -- Clock out ports
  CLK_SRAM_OUT1    : out    std_logic;
  CLK_SRAM_OUT2    : out    std_logic;
  --Clock internal
  clk1_100M         : out    std_logic;
  clk2_100M         : out    std_logic  
 );
 end ssram_clk_gen;

architecture Behavioral of ssram_clk_gen is

  signal clkin             : std_logic;
  signal clk100_1			: std_logic;
  signal clk100_2			: std_logic;     
  signal clk100_bufg1			: std_logic;
  signal clk100_bufg2			: std_logic;
  signal clk100_fb1			: std_logic;
  signal clk100_fb2			: std_logic;  
  

  signal clkfb1             : std_logic;
  signal clkfb_in1          : std_logic;
  signal clkfb2             : std_logic;
  signal clkfb_in2          : std_logic;
  signal clkfb0				 : std_logic;
  signal clkfbout0          : std_logic;
  signal clkfboutbufg       : std_logic;
  
  signal clk0              : std_logic;
  signal clk1              : std_logic;
  signal clk1_buf          : std_logic;
  signal clk1_buf_n        : std_logic;
  signal clk2              : std_logic;
  signal clk2_buf          : std_logic;
  signal clk2_buf_n        : std_logic;

	signal clk0_input_bufio : std_logic;  
	signal clk1_input_bufio : std_logic;
	signal clk2_input_bufio : std_logic;

begin

  	 clk1_ext_bufio2 : BUFIO2
   generic map (
      DIVIDE => 1,           -- DIVCLK divider (1-8)
      DIVIDE_BYPASS => TRUE, -- Bypass the divider circuitry (TRUE/FALSE)
      I_INVERT => FALSE,     -- Invert clock (TRUE/FALSE)
      USE_DOUBLER => FALSE   -- Use doubler circuitry (TRUE/FALSE)
   )
   port map (
      DIVCLK => clk1_input_bufio,             -- 1-bit output: Divided clock output
      IOCLK => open,               -- 1-bit output: I/O output clock
      SERDESSTROBE => open, -- 1-bit output: Output SERDES strobe (connect to ISERDES2/OSERDES2)
      I => CLK_IN                        -- 1-bit input: Clock input (connect to IBUFG)
   );
	
 
  -- input buffer for feedback clock
  clkfb1_ibuf: IBUF
  port map
   (O  => clkfb_in1,
    I  => CLK_SRAM_IN1);

  -- bufio2fb instantiation
  clkfb1_bufio2fb: BUFIO2FB
  generic map
   (DIVIDE_BYPASS => TRUE)
  port map
   (O  => clkfb1,
    I  => clkfb_in1);

  pll1_base_inst : PLL_BASE
  generic map
   (BANDWIDTH            => "HIGH",
    CLK_FEEDBACK         => "CLKFBOUT", 
    COMPENSATION         => "EXTERNAL",
    DIVCLK_DIVIDE        => 1,
    CLKFBOUT_MULT        => 5, -- with CLKFBOUT feedback
    CLKFBOUT_PHASE       => 0.000, --default is 0
    --CLKFBOUT_PHASE       => -16.000, -- 36 is 1 ns at 100MHz
	 -- CLKFBOUT_PHASE shift FBout vs CLK0 etc
	 -- has no effect, if FBout is used to drive external logic
    CLKOUT0_DIVIDE       => 5,
    CLKOUT0_PHASE        => 0.000,
    CLKOUT0_DUTY_CYCLE   => 0.500,
    CLKIN_PERIOD         => 10.0,
    REF_JITTER           => 0.099)
  port map
    -- Output clocks
   (CLKFBOUT            => clk1, -- clkfbout,
    CLKOUT0             => open,
    CLKOUT1             => open,
    CLKOUT2             => open,
    CLKOUT3             => open,
    CLKOUT4             => open,
    CLKOUT5             => open,
    LOCKED              => open,
    RST                 => '0',
    -- Input clock control
    CLKFBIN             => clkfb1,
    CLKIN               => clk1_input_bufio);


  -- buffer for feedback clock output
  clkfbout1_bufg : BUFG
  port map
   (O  => clk1_buf,
    I  => clk1);

  -- Locally invert clkfb_bufg_out for use in ODDR2
  clk1_buf_n <= not clk1_buf;

  -- Forward the feedback clock off-chip
  clkfbout_oddr1 : ODDR2
  port map
   (Q  => CLK_SRAM_OUT1,
    C0 => clk1_buf,
    C1 => clk1_buf_n,
    CE => '1',
    D0 => '1',
    D1 => '0',
    R  => '0',
    S  => '0');




 	 clk2_ext_bufio2 : BUFIO2
   generic map (
      DIVIDE => 1,           -- DIVCLK divider (1-8)
      DIVIDE_BYPASS => TRUE, -- Bypass the divider circuitry (TRUE/FALSE)
      I_INVERT => FALSE,     -- Invert clock (TRUE/FALSE)
      USE_DOUBLER => FALSE   -- Use doubler circuitry (TRUE/FALSE)
   )
   port map (
      DIVCLK => clk2_input_bufio,             -- 1-bit output: Divided clock output
      IOCLK => open,               -- 1-bit output: I/O output clock
      SERDESSTROBE => open, -- 1-bit output: Output SERDES strobe (connect to ISERDES2/OSERDES2)
      I => CLK_IN                        -- 1-bit input: Clock input (connect to IBUFG)
   );
	
 
  -- input buffer for feedback clock
  clkfb2_ibuf: IBUF
  port map
   (O  => clkfb_in2,
    I  => CLK_SRAM_IN2);

  -- bufio2fb instantiation
  clkfb2_bufio2fb: BUFIO2FB
  generic map
   (DIVIDE_BYPASS => TRUE)
  port map
   (O  => clkfb2,
    I  => clkfb_in2);

  pll2_base_inst : PLL_BASE
  generic map
   (BANDWIDTH            => "HIGH",
    CLK_FEEDBACK         => "CLKFBOUT", -- CLKFBOUTbasically, this is not true, because we use a feedback derived from
														-- clkout. But we need this setting to adjust via clkfb phase shift
    COMPENSATION         => "EXTERNAL",  -- because we run from an internally generated clock
    DIVCLK_DIVIDE        => 1,
    CLKFBOUT_MULT        => 5, -- with CLKFBOUT feedback
    CLKFBOUT_PHASE       => 0.000, --default is 0
    --CLKFBOUT_PHASE       => -16.000, -- 36 is 1 ns at 100MHz
	 -- CLKFBOUT_PHASE shift FBout vs CLK0 etc
	 -- has no effect, if FBout is used to drive external logic
    CLKOUT0_DIVIDE       => 5,
    CLKOUT0_PHASE        => 0.000,
    CLKOUT0_DUTY_CYCLE   => 0.500,
    CLKIN_PERIOD         => 10.0,
    REF_JITTER           => 0.099)
  port map
    -- Output clocks
   (CLKFBOUT            => clk2, -- clkfbout,
    CLKOUT0             => open,
    CLKOUT1             => open,
    CLKOUT2             => open,
    CLKOUT3             => open,
    CLKOUT4             => open,
    CLKOUT5             => open,
    LOCKED              => open,
    RST                 => '0',
    -- Input clock control
    CLKFBIN             => clkfb2,
    CLKIN               => clk2_input_bufio);



  -- Output buffering
  -------------------------------------
  -- buffer for feedback clock output
  clkfbout2_bufg : BUFG
  port map
   (O  => clk2_buf,
    I  => clk2);

  -- Locally invert clkfb_bufg_out for use in ODDR2
  clk2_buf_n <= not clk2_buf;

  -- Forward the feedback clock off-chip
  clkfbout_oddr2 : ODDR2
  port map
   (Q  => CLK_SRAM_OUT2,
    C0 => clk2_buf,
    C1 => clk2_buf_n,
    CE => '1',
    D0 => '1',
    D1 => '0',
    R  => '0',
    S  => '0');



  -- Clocking primitive
  --------------------------------------
  
  -- Instantiation of the DCM primitive
  --    * Unused inputs are tied off
  --    * Unused outputs are labeled unused
  dcm1_sp_inst: DCM_SP
  generic map
   (CLKDV_DIVIDE          => 2.000,
    CLKFX_DIVIDE          => 1,
    CLKFX_MULTIPLY        => 2,
    CLKIN_DIVIDE_BY_2     => FALSE,
    CLKIN_PERIOD          => 10.0,
    CLKOUT_PHASE_SHIFT    => "NONE",
    CLK_FEEDBACK          => "1X",
    DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
    PHASE_SHIFT           => 0,
    STARTUP_WAIT          => FALSE)
  port map
   -- Input clock
   (CLKIN                 => clk1_input_bufio,
    CLKFB                 => clk100_fb1,
    -- Output clocks
    CLK0                  => clk100_1,
    CLK90                 => open,
    CLK180                => open,
    CLK270                => open,
    CLK2X                 => open,
    CLK2X180              => open,
    CLKFX                 => open,
    CLKFX180              => open,
    CLKDV                 => open,
   -- Ports for dynamic phase shift
    PSCLK                 => '0',
    PSEN                  => '0',
    PSINCDEC              => '0',
    PSDONE                => open,
   -- Other control and status signals
    LOCKED                => open,
    STATUS                => open,
    RST                   => '0',
   -- Unused pin, tie low
    DSSEN                 => '0');

clk100_fb1 <= clk100_bufg1;

  clk100M_bufg1 : BUFG
  port map
   (O  => clk100_bufg1,
    I  => clk100_1);
	 
	 
  dcm2_sp_inst: DCM_SP
  generic map
   (CLKDV_DIVIDE          => 2.000,
    CLKFX_DIVIDE          => 1,
    CLKFX_MULTIPLY        => 2,
    CLKIN_DIVIDE_BY_2     => FALSE,
    CLKIN_PERIOD          => 10.0,
    CLKOUT_PHASE_SHIFT    => "NONE",
    CLK_FEEDBACK          => "1X",
    DESKEW_ADJUST         => "SYSTEM_SYNCHRONOUS",
    PHASE_SHIFT           => 0,
    STARTUP_WAIT          => FALSE)
  port map
   -- Input clock
   (CLKIN                 => clk2_input_bufio,
    CLKFB                 => clk100_fb2,
    -- Output clocks
    CLK0                  => clk100_2,
    CLK90                 => open,
    CLK180                => open,
    CLK270                => open,
    CLK2X                 => open,
    CLK2X180              => open,
    CLKFX                 => open,
    CLKFX180              => open,
    CLKDV                 => open,
   -- Ports for dynamic phase shift
    PSCLK                 => '0',
    PSEN                  => '0',
    PSINCDEC              => '0',
    PSDONE                => open,
   -- Other control and status signals
    LOCKED                => open,
    STATUS                => open,
    RST                   => '0',
   -- Unused pin, tie low
    DSSEN                 => '0');

clk100_fb2 <= clk100_bufg2;

  clk100M_bufg2 : BUFG
  port map
   (O  => clk100_bufg2,
    I  => clk100_2);
	 
	 
	 
	 clk1_100M <= clk100_bufg1;
	 clk2_100M <= clk100_bufg2;	 
end Behavioral;

