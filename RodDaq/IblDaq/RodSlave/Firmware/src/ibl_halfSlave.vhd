----------------------------------------------------------------------------------
-- Company: University of Washington, Seattle
-- Engineer: Shaw-Pin (Bing) Chen
-- 
-- Create Date:    15:07:27 10/22/2013 
-- Design Name: 
-- Module Name:    ibl_halfSlave - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
-- 
--    EFB halt output connects directly to Formatter halt output
--    pause EFB fifo 1 & 2 outputs left open
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.all;
use IEEE.STD_LOGIC_unsigned.all;
use IEEE.STD_LOGIC_textio.all;

--library ibl_formatter;
--library ibl_efb;
--library ibl_router;
--
--use ibl_formatter.all;
--use ibl_efb.all;
--use ibl_router.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ibl_halfSlave is
	port(
		clk40   				    	: in  STD_LOGIC;
		clk80   				    	: in  STD_LOGIC;
		clk100                  : in  STD_LOGIC;
		reset                   : in  STD_LOGIC;                                
		efb_id						: in  STD_LOGIC_VECTOR(1 downto 0);            
--		hold_output             : in  STD_LOGIC;  -- implemented as EFB to Formatter internal signal
		-- Signals From BOC
		data2rod1 					: in  STD_LOGIC_VECTOR(7 downto 0); 
		addr2rod1 					: in  STD_LOGIC_VECTOR(1 downto 0); 
		valid2rod1 					: in  STD_LOGIC;
		ctl2rod1 					: in  STD_LOGIC;
		data2rod2 					: in  STD_LOGIC_VECTOR(7 downto 0); 
		addr2rod2 					: in  STD_LOGIC_VECTOR(1 downto 0); 
		valid2rod2 					: in  STD_LOGIC;
		ctl2rod2 					: in  STD_LOGIC;
		fmt_fifo_busy           : out STD_LOGIC_VECTOR(7 downto 0); 
      fe_cmdpulse             : in  STD_LOGIC;
		-- Mode Bits from ROD Master
		mode_bits_in            : in  STD_LOGIC_VECTOR(11 downto 0);
		modebits_fifo_wen_n_in  : in  STD_LOGIC;
		modebits_fifo_rst_n_in  : in  STD_LOGIC;
		modebits_fifo_ef_n_out  : out STD_LOGIC; 
		modebits_fifo_ff_n_out  : out STD_LOGIC;
		-- Event Data from ROD Master
		ev_data_from_ppc       	: in  STD_LOGIC_VECTOR(15 downto 0);
		ev_data_wen_n          	: in  STD_LOGIC;
		ev_data_rst_n          	: in  STD_LOGIC;     
		ev_data_almost_full_n  	: out STD_LOGIC;
		ev_data_ready          	: out STD_LOGIC; -- ev_data_empty, ready for more
		-- HW DEBUG
		fifocount_out           : out STD_LOGIC_VECTOR( 9 DOWNTO 0);
		ev_id_fifo_empty_error 	: out STD_LOGIC;
		-- Register Interface Signals
		fmt0_stts_reg				: out STD_LOGIC_VECTOR(31 downto 0);
		fmt1_stts_reg				: out STD_LOGIC_VECTOR(31 downto 0);
      fmt0Occupancy           : out STD_LOGIC_VECTOR(39 downto 0);
      fmt1Occupancy           : out STD_LOGIC_VECTOR(39 downto 0);
      fmt0_1_ht_error         : out STD_LOGIC_VECTOR( 7 downto 0);
      fmt0_1_timeout_error    : out STD_LOGIC_VECTOR( 7 downto 0);
		fmt_link_enabled			: in  STD_LOGIC_VECTOR( 7 downto 0);
      fmt_trig_cnt            : in  STD_LOGIC_VECTOR( 3 downto 0);
      hitdisccnfg             : in  STD_LOGIC_VECTOR(15 downto 0);
		fmt_timeout_limit	  		: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt_data_overflow_limit	: in  STD_LOGIC_VECTOR(15 downto 0);
		fmt_header_trailer_limit: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt_rod_busy_limit		: in  STD_LOGIC_VECTOR( 9 downto 0);
      fmt_trailer_limit	  		: in  STD_LOGIC_VECTOR(31 downto 0);
      
      formatter0_frames       : out STD_LOGIC_VECTOR(15 downto 0);
      formatter1_frames       : out STD_LOGIC_VECTOR(15 downto 0);

		efb_err_mask_wen1       : in  STD_LOGIC;
		efb_err_mask_din1			: in  STD_LOGIC_VECTOR(31 downto 0);
		efb_err_mask_dout1		: out STD_LOGIC_VECTOR(31 downto 0);       
		efb_err_mask_sel1			: in  STD_LOGIC_VECTOR( 2 downto 0);             
		efb_err_mask_wen2			: in  STD_LOGIC;       
		efb_err_mask_din2			: in  STD_LOGIC_VECTOR(31 downto 0);       
		efb_err_mask_dout2		: out STD_LOGIC_VECTOR(31 downto 0);       
		efb_err_mask_sel2			: in  STD_LOGIC_VECTOR( 2 downto 0);
		efb_source_id           : in  STD_LOGIC_VECTOR(31 downto 0);
		efb_format_version		: in  STD_LOGIC_VECTOR(31 downto 0);
		efb_run_number          : in  STD_LOGIC_VECTOR(31 downto 0);
		efb_out_fifo_rst        : in  STD_LOGIC;	 
		efb_out_fifo1_empty_n   : out STD_LOGIC;
		efb_out_fifo1_aempty_n  : out STD_LOGIC;
		efb_out_fifo1_full_n    : out STD_LOGIC;
		efb_out_fifo1_afull_n   : out STD_LOGIC;
		efb_out_fifo1_play_done : out STD_LOGIC;
		efb_out_fifo2_empty_n   : out STD_LOGIC;
		efb_out_fifo2_aempty_n  : out STD_LOGIC;
		efb_out_fifo2_full_n    : out STD_LOGIC;
		efb_out_fifo2_afull_n   : out STD_LOGIC;
		efb_out_fifo2_play_done : out STD_LOGIC;
		efb_misc_stts_reg       : out STD_LOGIC_VECTOR(30 downto 0);
		efb_ev_header_data      : out STD_LOGIC_VECTOR(15 downto 0);
		efb_ev_fifo_data1       : out STD_LOGIC_VECTOR(34 downto 0);
		efb_ev_fifo_data2       : out STD_LOGIC_VECTOR(34 downto 0);
		efb_mask_bcid_error     : in  STD_LOGIC;
		efb_mask_l1id_error     : in  STD_LOGIC;
		efb_bcid_offset         : in  STD_LOGIC_VECTOR( 9 downto 0);
		efb_grp_evt_enable      : in  STD_LOGIC;
		efb_event_count         : out STD_LOGIC_VECTOR(31 downto 0);
		efb_mask_boc_clock_err  : in  STD_LOGIC;
		efb_mask_tim_clock_err  : in  STD_LOGIC;
		efb_l1id_fifo_data      : out STD_LOGIC_VECTOR(39 downto 0);
		efb_l1id_fifo_addr      : in  STD_LOGIC_VECTOR( 3 downto 0);
		efb_enable_l1_trap      : in  STD_LOGIC;
		efb_l1_trap_full_in     : out STD_LOGIC;
		efb_latched_l1id0       : out STD_LOGIC_VECTOR(12 downto 0);
		efb_latched_l1id1       : out STD_LOGIC_VECTOR(12 downto 0);
		efb_latched_bcid0       : out STD_LOGIC_VECTOR( 9 downto 0);
		efb_latched_bcid1       : out STD_LOGIC_VECTOR( 9 downto 0);
		efb_tim_bcid_in_rol     : in  STD_LOGIC;
		efb_bcid_rollover_sel   : in  STD_LOGIC;
		router_busy					: out STD_LOGIC;
		-- S-Link Interface
		calib_mode					: in  STD_LOGIC;
		slink_utest					: out STD_LOGIC;
		slink_ureset         	: out STD_LOGIC; 
		slink_uctrl           	: out STD_LOGIC; 
		slink_lff             	: in  STD_LOGIC; 
		slink_ldown           	: in  STD_LOGIC; 
		slink_data_out         	: out STD_LOGIC_VECTOR(31 downto 0);
		slink_data_wen        	: out STD_LOGIC;		
		-- Data to Histogrammer
		histo_rfd					: in  STD_LOGIC;
		chip                    : out STD_LOGIC_VECTOR(2 downto 0);
		hitValid                : out STD_LOGIC;
		row         				: out STD_LOGIC_VECTOR(8 downto 0);
		col				         : out STD_LOGIC_VECTOR(6 downto 0);
		totVal   			      : out STD_LOGIC_VECTOR(3 downto 0);
      --histo fifo
      histoFull               : out STD_LOGIC;
      histoOverrun            : out STD_LOGIC;
		-- HW DEBUGGING for ChipScope
		fmt0_valid_to_efb   		: out STD_LOGIC;  
		fmt0_data_to_efb			: out STD_LOGIC_VECTOR(34 downto 0);      
		fmt0_boe_to_efb			: out STD_LOGIC;                 
		fmt0_eoe_to_efb			: out STD_LOGIC;
      link0_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link1_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link2_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link3_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link4_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link5_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link6_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      link7_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      header_err_reg          : out STD_LOGIC_VECTOR(31 downto 0);     
      trailer_err_reg         : out STD_LOGIC_VECTOR(31 downto 0); 
      rd_timeout_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
      ht_limit_err_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      row_col_err_reg         : out STD_LOGIC_VECTOR(31 downto 0); 
      l1_id_err_reg           : out STD_LOGIC_VECTOR(31 downto 0); 
      bc_id_err_reg           : out STD_LOGIC_VECTOR(31 downto 0)
	);
end ibl_halfSlave;

architecture Behavioral of ibl_halfSlave is
--------------------------------------------
--             COMPONENTS 
--------------------------------------------
   component formatter_top is
      port(
		  clk40   						   : in  STD_LOGIC;-- 40 MHz
		  clk80   						   : in  STD_LOGIC;-- 80 MHz
		  rst   						      : in  STD_LOGIC;
		  hold_output					   : in  STD_LOGIC;
		  fmt_stts_reg				      : out STD_LOGIC_VECTOR(31 downto 0);
		  -- inputs from BOC
		  we_from_boc				      : in  STD_LOGIC;
		  data_from_boc 			      : in  STD_LOGIC_VECTOR(7 downto 0);
		  addr_from_boc			      : in  STD_LOGIC_VECTOR(1 downto 0);
		  cntrl_from_boc		 	      : in  STD_LOGIC;
		  -- inputs from register_block
		  fmt_link_enabled		      : in  STD_LOGIC_VECTOR(3 downto 0);
        fmt_trig_cnt                : in  STD_LOGIC_VECTOR(3 downto 0);
		  fmt_readout_timeout	   	: in  STD_LOGIC_VECTOR(31 downto 0);
		  fmt_data_overflow_limit		: in  STD_LOGIC_VECTOR(15 downto 0);
		  fmt_header_trailer_limit	   : in  STD_LOGIC_VECTOR(31 downto 0);
		  fmt_rod_busy_limit	   	   : in  STD_LOGIC_VECTOR( 9 downto 0);
        fmt_trailer_timeout	   	: in  STD_LOGIC_VECTOR(31 downto 0);
        fmt_occupancy_out           : out STD_LOGIC_VECTOR(39 downto 0);
        fmt_ht_error                : out STD_LOGIC_VECTOR( 3 downto 0);
        fmt_timeout_error           : out STD_LOGIC_VECTOR( 3 downto 0);
		  -- outputs to EFB
		  valid_to_efb					   : out STD_LOGIC;
		  data_to_efb		  				: out STD_LOGIC_VECTOR(34 downto 0);
		  boe					  			   : out STD_LOGIC; -- beginning of event
		  eoe					  			   : out STD_LOGIC; -- end of event
		  fifo_busy         			   : out STD_LOGIC_VECTOR(3 downto 0);
        
        formatter_frames            : out STD_LOGIC_VECTOR(15 downto 0);
 
        fe_cmdpulse                 : in  STD_LOGIC;
		  -- mode bits
		  mode_bits_in                : in  STD_LOGIC_VECTOR(11 downto 0); -- 12 wire time multiplexed mode bits
		  modebits_fifo_wen_n_in      : in  STD_LOGIC; -- modebit fifos writein strobe: strobe 0 - zero bits, strobe 1 - one bits
		  modebits_fifo_rst_n_in      : in  STD_LOGIC; -- modebit fifos reset strobe
		  modebits_fifo_ef_n_out      : out STD_LOGIC; -- at least one of the readout mode fifos is empty
		  modebits_fifo_ff_n_out      : out STD_LOGIC 	-- at least one of the readout mode fifos is full
      );
   end component;

	component efb_top is
		generic (use_bram: boolean := true); 
		port (
			clk80                   : in  STD_LOGIC;
			clk40                   : in  STD_LOGIC;
			rst                    	: in  STD_LOGIC;
			efb_id                 	: in  STD_LOGIC_VECTOR(1 downto 0);
			-- Formatter FIFO Controller Signals
			data_in1               	: in  STD_LOGIC_VECTOR(34 downto 0);
			data_in2               	: in  STD_LOGIC_VECTOR(34 downto 0);
			fmt_link_enabled	      : in  STD_LOGIC_VECTOR( 7 downto 0);
			fmt_data_valid_in1     	: in  STD_LOGIC;
			fmt_data_valid_in2     	: in  STD_LOGIC;
			fmt_beg_of_event1      	: in  STD_LOGIC;      
			fmt_beg_of_event2      	: in  STD_LOGIC;
			fmt_end_of_event1      	: in  STD_LOGIC;      
			fmt_end_of_event2      	: in  STD_LOGIC;
			fifo1_pause           	: out STD_LOGIC;
			fifo2_pause				   : out STD_LOGIC;
			-- Register Interface Signals
			-- Interfacing with the efb err_detect_new blocks
			err_mask_wen1           : in  STD_LOGIC;
			err_mask_din1           : in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout1          : out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel1           : in  STD_LOGIC_VECTOR( 2 downto 0);      
			err_mask_wen2           : in  STD_LOGIC;
			err_mask_din2           : in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout2          : out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel2           : in  STD_LOGIC_VECTOR( 2 downto 0);
			-- Information going into efb gen_fragment block
			source_id               : in  STD_LOGIC_VECTOR(31 downto 0);
			format_version          : in  STD_LOGIC_VECTOR(31 downto 0);
			run_number              : in  STD_LOGIC_VECTOR(31 downto 0);
         hitdisccnfg             : in  STD_LOGIC_VECTOR(15 downto 0);
			-- output evtmem fifos signals
			out_fifo_rst            : in  STD_LOGIC;	 
			out_fifo1_empty_n       : out STD_LOGIC;
			out_fifo1_aempty_n      : out STD_LOGIC;
			out_fifo1_full_n        : out STD_LOGIC;
			out_fifo1_afull_n       : out STD_LOGIC;
			out_fifo1_play_done     : out STD_LOGIC;
			out_fifo2_empty_n       : out STD_LOGIC;
			out_fifo2_aempty_n      : out STD_LOGIC;
			out_fifo2_full_n        : out STD_LOGIC;
			out_fifo2_afull_n       : out STD_LOGIC;
			out_fifo2_play_done     : out STD_LOGIC;
			efb_misc_stts_reg       : out STD_LOGIC_VECTOR(30 downto 0);
			ev_header_data          : out STD_LOGIC_VECTOR(15 downto 0);
			ev_fifo_data1           : out STD_LOGIC_VECTOR(34 downto 0);
			ev_fifo_data2           : out STD_LOGIC_VECTOR(34 downto 0);
			mask_bcid_error         : in  STD_LOGIC;
			mask_l1id_error         : in  STD_LOGIC;
			bcid_offset             : in  STD_LOGIC_VECTOR( 9 downto 0);
			grp_evt_enable          : in  STD_LOGIC;
			event_count             : out STD_LOGIC_VECTOR(31 downto 0);
			mask_boc_clock_err      : in  STD_LOGIC;
			mask_tim_clock_err      : in  STD_LOGIC;
			l1id_fifo_data          : out STD_LOGIC_VECTOR(39 downto 0);
			l1id_fifo_addr          : in  STD_LOGIC_VECTOR( 3 downto 0);
			enable_l1_trap          : in  STD_LOGIC;
			l1_trap_full_in         : out STD_LOGIC;
			latched_l1id0           : out STD_LOGIC_VECTOR(12 downto 0);
			latched_l1id1           : out STD_LOGIC_VECTOR(12 downto 0);
			latched_bcid0           : out STD_LOGIC_VECTOR(9 downto 0);
			latched_bcid1           : out STD_LOGIC_VECTOR(9 downto 0);
			tim_bcid_in_rol         : in  STD_LOGIC;
			bcid_rollover_select    : in  STD_LOGIC;
			-- Event Data L1/BCID/Trig Type Data
			ev_data_from_ppc       	: in  STD_LOGIC_VECTOR(15 downto 0);
			ev_data_wen_n           : in  STD_LOGIC;
			ev_data_rst_n           : in  STD_LOGIC;
			ev_data_almost_full_n   : out STD_LOGIC;
			ev_data_ready           : out STD_LOGIC;
			-- HW DEBUG
			fifocount_out           : out STD_LOGIC_VECTOR( 9 DOWNTO 0);
			ev_id_fifo_empty_error  : out STD_LOGIC;
			gatherer_halt_output    : in  STD_LOGIC;
			-- Output to Router (Slink and Histogrammer)
         k_word						: out STD_LOGIC;			
			data_valid_out          : out STD_LOGIC;
			out_data_to_router      : out STD_LOGIC_VECTOR(31 downto 0);
			data_chip_id            : out STD_LOGIC_VECTOR( 2 downto 0)
		);
	end component;

	component router_top is
		Port ( 
			clk40							: in  STD_LOGIC;
			clk80							: in  STD_LOGIC;
			clk100						: in  STD_LOGIC;
			reset             : in  STD_LOGIC;
			calib_mode				:	in  STD_LOGIC;
			router_busy				: out STD_LOGIC;
			--  Signals From a Single EFB Engine
			chip_id           : in  STD_LOGIC_VECTOR ( 2 downto 0);
			efb_data_in       : in  STD_LOGIC_VECTOR (31 downto 0);
			efb_data_valid    : in  STD_LOGIC;
			k_word						: in  STD_LOGIC;
			--  Slink Interface Signals
			slink_utest       : out STD_LOGIC; 
			slink_ureset      : out STD_LOGIC; 
			slink_uctrl       : out STD_LOGIC; 
			slink_lff         : in  STD_LOGIC; 
			slink_ldown       : in  STD_LOGIC; 
			data_to_slink     : out STD_LOGIC_VECTOR (31 downto 0);
			slink_data_wen    : out STD_LOGIC;
			--  Histogrammer Interface Signals	 
         histoFull         : out STD_LOGIC;
         histoOverrun      : out STD_LOGIC;
			histo_rfd					: in  STD_LOGIC;
			hitValid          : out STD_LOGIC;
			chip              : out STD_LOGIC_VECTOR (2 downto 0);
			row         			: out STD_LOGIC_VECTOR (8 downto 0);
			col				        : out STD_LOGIC_VECTOR (6 downto 0);
			totVal   			    : out STD_LOGIC_VECTOR (3 downto 0);
         --Error Registers
         link0_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link1_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link2_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link3_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link4_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link5_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link6_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         link7_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         header_err_reg       : out STD_LOGIC_VECTOR(31 downto 0);     
         trailer_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
         rd_timeout_err_reg   : out STD_LOGIC_VECTOR(31 downto 0); 
         ht_limit_err_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
         row_col_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
         l1_id_err_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         bc_id_err_reg        : out STD_LOGIC_VECTOR(31 downto 0)
		);
	end component;

--------------------------------------------
--                SIGNALS 
--------------------------------------------
  -- Formatter Signals
   signal modebits_fifo_ef_n_out_i			  : STD_LOGIC_VECTOR(1 downto 0);   
	signal modebits_fifo_ff_n_out_i			  : STD_LOGIC_VECTOR(1 downto 0);   
	signal fmt_fifo_busy_i                   : STD_LOGIC_VECTOR(7 downto 0);   
                               
  -- Formatters to EFB Signals
	signal valid_to_efb1_i, valid_to_efb2_i : STD_LOGIC;                      
	signal data_to_efb1_i, data_to_efb2_i	 : STD_LOGIC_VECTOR(34 downto 0);  
   signal boe1_i, boe2_i, eoe1_i, eoe2_i   : STD_LOGIC;                      

	-- EFB to Formatter Signals
--	signal fmt_halt_output_i			:	STD_LOGIC;
	signal fmt_halt_output_0i			:	STD_LOGIC;
	signal fmt_halt_output_1i			:	STD_LOGIC;

  -- EFB to Router Signals
	signal router_busy_i          : STD_LOGIC;
	signal k_word_i               : STD_LOGIC;                                
   signal valid_to_router_i      : STD_LOGIC;                                
   signal data_to_router_i       : STD_LOGIC_VECTOR(31 downto 0);            
	signal data_chip_id_i         : STD_LOGIC_VECTOR( 2 downto 0);            

begin
  
	-- HW DEBUGGING FOR CHIPSCOPE
		fmt0_valid_to_efb <=	valid_to_efb1_i;
		fmt0_data_to_efb	<=	data_to_efb1_i;	
		fmt0_boe_to_efb	<=	boe1_i;	
		fmt0_eoe_to_efb	<= eoe1_i;		
  
	-- SPC change 2/5 temp assignment
--	fmt_halt_output_i <= '0';
	router_busy	<= router_busy_i; -- SPC 3/18 can direct route later

	modebits_fifo_ef_n_out	<= modebits_fifo_ef_n_out_i(1) OR modebits_fifo_ef_n_out_i(0);
	modebits_fifo_ff_n_out	<= modebits_fifo_ff_n_out_i(1) OR modebits_fifo_ff_n_out_i(0);
	fmt_fifo_busy <= fmt_fifo_busy_i;
	
	formatter_instance0: formatter_top
		port map(     
			clk80   							=> clk80,                       
			clk40   							=> clk40,                       
			rst   							=> reset,                       
			hold_output						=> fmt_halt_output_0i,
			fmt_stts_reg					=> fmt0_stts_reg,			
			we_from_boc						=> valid2rod1,                  
			data_from_boc 					=> data2rod1,                   
			addr_from_boc					=> addr2rod1,                   
			cntrl_from_boc					=> ctl2rod1,                    
			fmt_link_enabled				=> fmt_link_enabled(3 downto 0),
         fmt_trig_cnt               => fmt_trig_cnt,
			fmt_readout_timeout			=> fmt_timeout_limit,           
			fmt_data_overflow_limit    => fmt_data_overflow_limit,     
			fmt_header_trailer_limit   => fmt_header_trailer_limit,    
			fmt_rod_busy_limit			=> fmt_rod_busy_limit,                    
         fmt_trailer_timeout			=> fmt_trailer_limit,
         fmt_occupancy_out          => fmt0Occupancy,
         fmt_ht_error               => fmt0_1_ht_error(3 downto 0),
         fmt_timeout_error          => fmt0_1_timeout_error(3 downto 0),
			valid_to_efb   				=> valid_to_efb1_i,             
			data_to_efb						=> data_to_efb1_i,              
			boe								=> boe1_i,                      
			eoe								=> eoe1_i,                      
			fifo_busy						=> fmt_fifo_busy_i(3 downto 0), 
         formatter_frames           => formatter0_frames,
         fe_cmdpulse                => fe_cmdpulse,
			mode_bits_in               => mode_bits_in,                
			modebits_fifo_wen_n_in     => modebits_fifo_wen_n_in,      
			modebits_fifo_rst_n_in     => modebits_fifo_rst_n_in,      
			modebits_fifo_ef_n_out     => modebits_fifo_ef_n_out_i(0), 
			modebits_fifo_ff_n_out     => modebits_fifo_ff_n_out_i(0)  
		);

	formatter_instance1: formatter_top
		port map(
			clk80								=> clk80,                            
			clk40   							=> clk40,                            
			rst   							=> reset,                            
			hold_output						=> fmt_halt_output_1i,
			fmt_stts_reg					=> fmt1_stts_reg,
			we_from_boc						=> valid2rod2,                       
			data_from_boc 					=> data2rod2,                        
			addr_from_boc					=> addr2rod2,                        
			cntrl_from_boc					=> ctl2rod2,                         
			fmt_link_enabled				=> fmt_link_enabled(7 downto 4),
         fmt_trig_cnt               => fmt_trig_cnt,
			fmt_readout_timeout			=> fmt_timeout_limit,             
			fmt_data_overflow_limit    => fmt_data_overflow_limit,       
			fmt_header_trailer_limit   => fmt_header_trailer_limit,      
			fmt_rod_busy_limit			=> fmt_rod_busy_limit,
         fmt_trailer_timeout			=> fmt_trailer_limit,
         fmt_occupancy_out          => fmt1Occupancy,
         fmt_ht_error               => fmt0_1_ht_error(7 downto 4),
         fmt_timeout_error          => fmt0_1_timeout_error(7 downto 4),
			valid_to_efb   				=> valid_to_efb2_i,                      
			data_to_efb						=> data_to_efb2_i,                       
			boe								=> boe2_i,                               
			eoe								=> eoe2_i,                               
			fifo_busy						=> fmt_fifo_busy_i(7 downto 4),          
         formatter_frames           => formatter1_frames,
         fe_cmdpulse                => fe_cmdpulse,
			mode_bits_in               => mode_bits_in,                  
			modebits_fifo_wen_n_in     => modebits_fifo_wen_n_in,        
			modebits_fifo_rst_n_in     => modebits_fifo_rst_n_in,        
			modebits_fifo_ef_n_out     => modebits_fifo_ef_n_out_i(1),   
			modebits_fifo_ff_n_out     => modebits_fifo_ff_n_out_i(1)    
		);

  efb_instance: efb_top
		port map(
			clk80			   			=> clk80,                 
			clk40			   			=> clk40,                 
			rst   						=> reset,                 
			efb_id						=> efb_id,                
			data_in1		   			=> data_to_efb1_i,      
			data_in2		   			=> data_to_efb2_i,      
			fmt_link_enabled			=> fmt_link_enabled,
			fmt_data_valid_in1		=> valid_to_efb1_i,   
			fmt_data_valid_in2		=> valid_to_efb2_i,   
			fmt_beg_of_event1			=> boe1_i,            
			fmt_beg_of_event2			=> boe2_i,            
			fmt_end_of_event1			=> eoe1_i,            
			fmt_end_of_event2			=> eoe2_i,            
			fifo1_pause			 	  	=> fmt_halt_output_0i,               
			fifo2_pause					=> fmt_halt_output_1i,                
			err_mask_wen1           => efb_err_mask_wen1,         
			err_mask_din1				=> efb_err_mask_din1,         
			err_mask_dout1				=> efb_err_mask_dout1,        
			err_mask_sel1				=> efb_err_mask_sel1,            
			err_mask_wen2				=> efb_err_mask_wen2,         
			err_mask_din2				=> efb_err_mask_din2,         
			err_mask_dout2				=> efb_err_mask_dout2,        
			err_mask_sel2				=> efb_err_mask_sel2,         
			source_id             	=> efb_source_id,             
			format_version		    	=> efb_format_version,        
			run_number            	=> efb_run_number,            
         hitdisccnfg             => hitdisccnfg,
			out_fifo_rst          	=> efb_out_fifo_rst,          
			out_fifo1_empty_n       => efb_out_fifo1_empty_n,     
			out_fifo1_aempty_n      => efb_out_fifo1_aempty_n,    
			out_fifo1_full_n        => efb_out_fifo1_full_n,      
			out_fifo1_afull_n       => efb_out_fifo1_afull_n,     
			out_fifo1_play_done     => efb_out_fifo1_play_done,   
			out_fifo2_empty_n       => efb_out_fifo2_empty_n,     
			out_fifo2_aempty_n      => efb_out_fifo2_aempty_n,    
			out_fifo2_full_n        => efb_out_fifo2_full_n,      
			out_fifo2_afull_n       => efb_out_fifo2_afull_n,     
			out_fifo2_play_done     => efb_out_fifo2_play_done,	
			efb_misc_stts_reg			=> efb_misc_stts_reg,       
			ev_header_data          => efb_ev_header_data,      
			ev_fifo_data1           => efb_ev_fifo_data1,       
			ev_fifo_data2           => efb_ev_fifo_data2,       
			mask_bcid_error         => efb_mask_bcid_error,     
			mask_l1id_error         => efb_mask_l1id_error,     
			bcid_offset             => efb_bcid_offset,         
			grp_evt_enable          => efb_grp_evt_enable,      
			event_count             => efb_event_count,         
			mask_boc_clock_err      => efb_mask_boc_clock_err,  
			mask_tim_clock_err      => efb_mask_tim_clock_err,  
			l1id_fifo_data          => efb_l1id_fifo_data,      
			l1id_fifo_addr          => efb_l1id_fifo_addr,      
			enable_l1_trap          => efb_enable_l1_trap,      
			l1_trap_full_in         => efb_l1_trap_full_in,     
			latched_l1id0           => efb_latched_l1id0,       
			latched_l1id1           => efb_latched_l1id1,         
			latched_bcid0           => efb_latched_bcid0,         
			latched_bcid1           => efb_latched_bcid1,         
			tim_bcid_in_rol         => efb_tim_bcid_in_rol,       
			bcid_rollover_select    => efb_bcid_rollover_sel,     
			ev_data_from_ppc 	   	=> ev_data_from_ppc,          
			ev_data_wen_n    	   	=> ev_data_wen_n,             
			ev_data_rst_n    		   => ev_data_rst_n,             
			ev_data_almost_full_n   => ev_data_almost_full_n,     
			ev_data_ready           => ev_data_ready,     
      	fifocount_out           => fifocount_out,	
			ev_id_fifo_empty_error  => ev_id_fifo_empty_error,    
--			gatherer_halt_output    => fmt_halt_output_i,         
			gatherer_halt_output    => router_busy_i,         
			k_word						=> k_word_i,                  
			data_valid_out          => valid_to_router_i,         
			out_data_to_router	   => data_to_router_i,          
			data_chip_id		      => data_chip_id_i             
		);

  router_instance: router_top
		port map( 
			clk40				    => clk40,				               
			clk80				    => clk80,
			clk100             => clk100,                      
			reset              => reset,                      
			calib_mode			 => calib_mode,
			router_busy			 => router_busy_i,--_i,
			chip_id            => data_chip_id_i,             
			efb_data_in        => data_to_router_i,           
			efb_data_valid     => valid_to_router_i,          
			k_word				 => k_word_i,										
			slink_utest        => slink_utest,                
			slink_ureset       => slink_ureset,               
			slink_uctrl        => slink_uctrl,                
			slink_lff          => slink_lff,                  
			slink_ldown        => slink_ldown,                
			data_to_slink      => slink_data_out,             
			slink_data_wen     => slink_data_wen,             
         histoFull          => histoFull,
         histoOverrun       => histoOverrun, --single cycle only
			histo_rfd	     	 => histo_rfd,
			hitValid           => hitValid,                   
			chip               => chip,                       
			row         	 	 => row,         	             
			col				    => col,				                 
			totVal   		    => totVal,
         link0_errors_reg   => link0_errors_reg,     
         link1_errors_reg   => link1_errors_reg,
         link2_errors_reg   => link2_errors_reg,
         link3_errors_reg   => link3_errors_reg,
         link4_errors_reg   => link4_errors_reg,
         link5_errors_reg   => link5_errors_reg,
         link6_errors_reg   => link6_errors_reg,
         link7_errors_reg   => link7_errors_reg,
         header_err_reg     => header_err_reg,
         trailer_err_reg    => trailer_err_reg,
         rd_timeout_err_reg => rd_timeout_err_reg,
         ht_limit_err_reg   => ht_limit_err_reg ,
         row_col_err_reg    => row_col_err_reg,
         l1_id_err_reg      => l1_id_err_reg,
         bc_id_err_reg      => bc_id_err_reg
		);

end Behavioral;
