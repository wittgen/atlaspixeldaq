-------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- ReadOutDriver Electronics
-------------------------------------------------------------------------------
--
-- Filename: trigger_counter_token_generator.vhd
-- Description:
--
-------------------------------------------------------------------------------
-- Structure: 
--  Multiple state machines in a flat topology/hierarchy which communicate
--   with single wire signals.  
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author:         Mark L. Nagel
-- Board Engineer: John Joseph 
-- History:
--    Friday 19 May 2000: 1st version
--    Modified : 09 Dec 2002  - JMJ
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------

entity tctg is  -- Trigger Counter/Token Generator
  port(
    clk                 : in  std_logic;
    rst                 : in  std_logic;
    roc_ready_in        : in  std_logic;
    l1_trigger_pulse_in : in  std_logic;
    token_out           : out std_logic;
    trigger_count_out   : out std_logic_vector(9 downto 0)
    );
end tctg;

architecture rtl of tctg is

-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type states is (
  reset, 
  idle, 
  issue_token, 
  wait_for_token
  );
signal pres_state : states;

signal trigger_count_i       : unsigned(8 downto 0);
signal token_out_i           : std_logic;
signal l1_trigger_pulse_i    : std_logic;
signal trigger_count_error_i : std_logic;
signal l1_trigger_pulse_ii	  : std_logic;
signal trigger_decr			  : std_logic;

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
begin
-------------------------------------------------------------------------------
-- Signal Connections
-------------------------------------------------------------------------------

trigger_count_out <= trigger_count_error_i & std_logic_vector(trigger_count_i);
token_out         <= token_out_i;

-------------------------------------------------------------------------------
-- Input Signal Register
-------------------------------------------------------------------------------
input_register_process : process(rst, clk)
begin
  if(rst = '1') then
    l1_trigger_pulse_i 		<= '0';
	 l1_trigger_pulse_ii 	<= '0';
  elsif (rising_edge(clk)) then
    l1_trigger_pulse_i 		<= l1_trigger_pulse_in;
	 l1_trigger_pulse_ii		<= l1_trigger_pulse_i;
  end if;
end process input_register_process;

-------------------------------------------------------------------------------
-- Trigger Counter
-------------------------------------------------------------------------------
trigger_counter_process : process(rst, clk)
begin
  if(rst = '1') then
		trigger_count_i 			<= (others => '0');
		trigger_count_error_i 	<= '0';
  elsif(rising_edge(clk)) then
		if(l1_trigger_pulse_i = '1' AND l1_trigger_pulse_ii = '1') then
			trigger_count_i 	<= trigger_count_i + 1;
			trigger_decr		<= '0';
--		elsif(l1_trigger_pulse_i = '0' AND trigger_count_i > 0 AND token_out_i = '1') then
		elsif(l1_trigger_pulse_i = '0' AND trigger_count_i > 0 AND token_out_i = '1' AND trigger_decr = '0') then -- DAV (28/11/2014)
			trigger_count_i 	<= trigger_count_i - 1;
			trigger_decr		<= '1';
		else
			trigger_decr		<= '0';
		end if;
		if(trigger_count_i = 511 AND l1_trigger_pulse_i = '1') then
			trigger_count_error_i <= '1';
		end if;
	end if;
end process trigger_counter_process;

-------------------------------------------------------------------------------
-- Token Generator
-------------------------------------------------------------------------------
token_generator_process : process(rst, clk)
begin
  if(rst = '1') then
		token_out_i <= '0';
		pres_state  <= reset; 
  elsif(rising_edge(clk)) then
      case pres_state is
        when reset =>
          token_out_i <= '0';
          pres_state  <= idle; 
  
        when idle =>
          token_out_i <= '0';
          if(trigger_count_i > 0 AND roc_ready_in = '1') then
            pres_state <= issue_token;
          end if; 
  
        when issue_token =>
          token_out_i <= '1';  		-- issue the token
          pres_state  <= wait_for_token;
  
        when wait_for_token =>
			 if(trigger_decr = '1') then
					token_out_i <= '0';  -- clear the token
					pres_state  <= reset;
			 else
					token_out_i <= '1';  
					pres_state  <= wait_for_token;
			 end if;
      end case;
  end if;
end process token_generator_process;

end rtl; -- end code for tctg
