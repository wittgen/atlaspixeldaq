-----------------------------------------------------------------------------

--Test Bench for Formatter FIFO Runtime Errors

-----------------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_LOGIC_1164.ALL;
USE IEEE.STD_LOGIC_ARITH.ALL;
USE IEEE.STD_LOGIC_UNSIGNED.ALL;

ENTITY formatter_fifo_tb is
END formatter_fifo_tb;

architecture behav of formatter_top_tb is

   --Componenet Declaration
   component formatter_fifo 
    port (
      clk: 				 in  std_logic;
      rst: 				 in  std_logic;
      din: 				 in  std_logic_vector(31 downto 0);
      wr_en: 			 in  std_logic;
      rd_en: 			 in  std_logic;
      trigger_in:     in  std_logic;
      rod_busy_limit: in  std_logic_vector( 9 downto 0);
      dout: 			 out std_logic_vector(31 downto 0);
      full: 			 out std_logic;
      wr_ack:         out std_logic;
      empty: 			 out std_logic;
      valid: 			 out std_logic;
      link_busy:      out std_logic;
      frames_in:      out std_logic_vector( 3 downto 0);
      link_occu_out:  out std_logic_vector( 9 downto 0)
		);
   end component;

   --Input Signals
   signal clk80          : std_logic := '0';
   signal rst            : std_logic := '0';
   signal din            : std_logic_vector(31 downto 0) := (others => '0');
   signal wr_en          : std_logic := '0';
   signal rd_en          : std_logic := '0';
   signal trigger_in     : std_logic := '0';
   signal rod_busy_limit : std_logic_vector( 9 downto 0) := x"ff" & "11";

   --Output Signals
   signal dout          : std_logic_vector(31 downto 0);
   signal full          : std_logic;
   signal wr_ack        : std_logic;
   signal empty         : std_logic;
   signal valid         : std_logic;
   signal link_busy     : std_logic;
   signal frames_in     : std_logic_vector( 3 downto 0);
   signal link_occu_out : std_logic_vector( 9 downto 0);

   --Clock period
   constant clk80_period : time := 12.5 ns;

begin

   --Instantiate Unit Under Test
   uut: formatter_fifo
      port map(
         clk            => clk80,
         rst            => rst,
         din            => din,
         wr_en          => wr_en,
         rd_en          => rd_en,
         trigger_in     => trigger_in,
         rod_busy_limit => rod_busy_limit,
         dout           => dout,
         full           => full,
         wr_ack         => wr_ack,
         empty          => empty,
         valid          => valid,
         link_busy      => link_busy,
         frames_in      => frames_in,
         link_occu_out  => link_occu_out
      );
   
   --80 MHz clock process
   clk80_process : process
   begin
      clk80 <= '1';
      wait for clk80_period/2;
      clk80 <= '0';
      wait for clk80_period/2;
   end process;

   --Reset Process
   reset_process : process
   begin
      rst <= '1';
      wait for 115 ns;
      rst <= '0';
		wait;
   end process;

   --Control Process
   control_process : process
   begin
      wait for 150 ns; 
      for I in 0 to 2 loop
      --write to FIFO
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; --wait for 0 ns;
      --write to FIFO
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
       --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
       --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      end loop;
      wait for 12.5 ns;
      trigger_in <= '1'; wait for 25 ns; trigger_in <= '0'; wait for 25 ns;
      for X in 0 to 2 loop
      --write to FIFO
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; --wait for 0 ns;
      --write to FIFO
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
       --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
       --write to FIFO                              
      wr_en <= '1'; wait for 12.5 ns; wr_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      --read from FIFO                             
      rd_en <= '1'; wait for 12.5 ns; rd_en <= '0'; wait for 12.5 ns;
      end loop;
      wait for 12.5 ns;
      trigger_in <= '1'; wait for 25 ns; trigger_in <= '0'; wait for 25 ns;
      wait; --forever                              
   end process;                                   

   --Data Process
   data_process : process
   begin
      wait for 150 ns; 
      for I in 0 to 2 loop
      --write to FIFO
      din <= x"20000019"; wait for 12.5 ns; --A Header
      --write to FIFO
      din <= x"80000025"; wait for 25 ns;
      --write to FIFO
      din <= x"80000055"; wait for 25 ns;
      --write to FIFO
      din <= x"80000079"; wait for 25 ns;
      --write to FIFO
      din <= x"40000047"; wait for 25 ns; --A Trailer
      --read from FIFO
      wait for 25 ns; --Make Sure it matches
      --read from FIFO
      wait for 25 ns;
      --write to FIFO
      din <= x"20025000"; wait for 25 ns;
      --write to FIFO
      din <= x"800ff047"; wait for 25 ns;
      --write to FIFO
      din <= x"40045047"; wait for 25 ns;
      --read from FIFO
      wait for 25 ns;
      --read from FIFO
      wait for 25 ns;
      end loop;
      wait for 62.5 ns; --Trigger time
      for X in 0 to 2 loop
      --write to FIFO
      din <= x"20000019"; wait for 12.5 ns; --A Header
      --write to FIFO
      din <= x"80000025"; wait for 25 ns;
      --write to FIFO
      din <= x"80000055"; wait for 25 ns;
      --write to FIFO
      din <= x"80000079"; wait for 25 ns;
      --write to FIFO
      din <= x"40000047"; wait for 25 ns; --A Trailer
      --read from FIFO
      wait for 25 ns; --Make Sure it matches
      --read from FIFO
      wait for 25 ns;
      --write to FIFO
      din <= x"20025000"; wait for 25 ns;
      --write to FIFO
      din <= x"800ff047"; wait for 25 ns;
      --write to FIFO
      din <= x"40045047"; wait for 25 ns;
      --read from FIFO
      wait for 25 ns;
      --read from FIFO
      wait for 25 ns;
      end loop;
      wait for 62.5 ns; --Trigger time
      wait; --forever
  end process;

end behav;
