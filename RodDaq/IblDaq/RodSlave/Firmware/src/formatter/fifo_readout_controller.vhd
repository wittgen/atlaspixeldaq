------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2003
-- ReadOutDriver Electronics
------------------------------------------------------------------------------
-- Filename: fifo_readout_controller.vhd
-- Description:
--  Fifo ReadOut Controller
--    This logic block is used to control the read out of all 12 link fifos
--    in the SCT Formatter.
--
--  Inputs:
--    From Globals
--      Clock and Reset..(2)
--
--    From the Dual Link Formatters
--      Link FIFO Data.........(32 x 6)
--      FIFO Empty Status......( 2 x 6)
--      Trailer Detect Status..(12 x 6)
--
--    From the Mode Bits Decoder
--      Dynamic ModeBits......(24)
--      Mode Bits DataReady...( 1)
--      Mode Bits FIFO Empty..( 1)
--
--    From the Register Block
--      Link Enable..........................(12)
--      Condensed Mode Bit...................(12)
--      Token to FIFO Readout Timeout Limit..( 8) 
--      Data Overflow Limit..................( 9)
--
--    From the Trigger Counter / Token Generator
--      Token In..(1)
--
--    From the EFB
--      Hold Output..(1)
--
--  Outputs:
--    To the Dual Link Formatters
--      FIFO Read Enable..(12)
--      FIFO Flush........(12)
--
--    To the Mode Bits Decoder
--      MB FIFO Read Enable..(1)
--
--    To the Register Block
--      Time Out Error Out.......(12)
--      Data Overflow Error Out..(12)
--
--    To the Trigger Counter / Token Generator
--      Token Out..(1)
--
--    To the EFB
--      Formatter FIFO Data Out..(32)
--      Active Link Number.......( 4)
--      Time Out Error Out.......( 1)
--      Condensed Mode Bit.......( 1)
--      Data Valid...............( 1)
--      Chip Has Token...........( 1)
--
--
-- Read Out Mode Definitions:
--  +----+----+------------------------------------------------------------+
--  |Mode| MB |                  Description                               |
--  +----+----+------------------------------------------------------------+
--  | M0 |[00]| Play 1 event from the Link FIFO, normal data flow from the |
--  |    |    | Formatter to the EFB.                                      |
--  +----+----+------------------------------------------------------------+
--  | M1 |[01]| Mask this link for 1 trigger.  This action dumps one event |
--  |    |    | then increments to the next link FIFO.                     |
--  +----+----+------------------------------------------------------------+
--  | M2 |[10]| Skip this link for 1 trigger.  This action does not change |
--  |    |    | any of the counters or pointers used by the Link FIFO.     |
--  +----+----+------------------------------------------------------------+
--  | M3 |[11]| Dump (play data, suppress Data Valid) 1st event in FIFO,   |
--  |    |    | play 2nd event to the EFB.                                 |
--  +----+----+------------------------------------------------------------+
--
--  Readout States:
--    Idle (IDLE) : 
--      Wait for Mode Bits from ROD Controller FPGA or ModeBits FIFO (512x24).
--      Mode bits for all links are loaded at the same time.  This simplifies
--      and minimizes FPGA utilization, and does not affect the performance
--      of the Formatter because the data is always played out in link order
--      for each event.
--
--    Fetch Mode Bits (FMB) :
--      Send a 1 Clock pulse Read Enable Pulse to the Mode Bits FIFO to fetch
--      Read Out Mode bits for the current Trigger
--
--    Wait For Token (WFT) :
--      After Mode Bits are loaded, Formatter is ready for the token to arrive.
--      In this state, the link enable signal is checked to determine the
--      active links in the chip, the occupancy status of each Link FIFO is 
--      checked to see if any data has been decoded, and the Mode Bits are
--      monitored to determine the proper action to perform.  While waiting
--      for the token, the link counter can be incremented to the first
--      active link (enable_link_in).  When the token arrives, the state 
--      machine will branch to a different state depending on the value
--      of the Mode Bits and the Link Enable Signal.
--
--    Read Out Link FIFO (ROLF) :
--      Begin readout of a link FIFO.  If a trailer is present when first
--      entering this state, the FIFO has a Header/Trailer Word only for 
--      this trigger, and can begin readout of the next link on the next 
--      clock.  
--
--    Read Out Link FIFO / Set Data Valid (ROLF_SDV) :
--
--    Wait for Data (WFD) :
--      If readout is true for a specific link and the link fifo is empty,
--      wait in this state until the empty flag is clear, go to ROLF
--
--    Dump One Event (DOE) :
--      Read one event from the Link FIFO, no Data Valid signal
--
--    Next Link (NLINK) :
--      Increment to the next active link
--
--    Done (DONE):
--      Pass the Token to the next Formatter and return to Idle
--
--    Pause Output (PAUSE):
--      If hold_output_in = '1' then stop data output and hold token.  This 
--      state condition is tested outside of the main case statement.
--
------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
------------------------------------------------------------------------------
-- Author: John Joseph
-- Board Engineer: 
-- History:
------------------------------------------------------------------------------
-- LIBRARY INCLUDES
------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity fifo_readout_controller is
  port(
-- Inputs:    
    clk                    : in  std_logic;
    rst                    : in  std_logic;
    enable_link_in         : in  std_logic_vector( 3 downto 0);
    hold_output_in         : in  std_logic;
    fifo_timeout_limit_in  : in  std_logic_vector(31 downto 0);
    data_overflow_limit_in : in  std_logic_vector(15 downto 0);
    fmt_trig_cnt           : in  std_logic_vector( 3 downto 0);
    link0_fifo_data_in     : in  std_logic_vector(31 downto 0);
    link1_fifo_data_in     : in  std_logic_vector(31 downto 0);
    link2_fifo_data_in     : in  std_logic_vector(31 downto 0);
    link3_fifo_data_in     : in  std_logic_vector(31 downto 0);
    data_valid_in          : in  std_logic_vector( 3 downto 0);
    link_fifo_empty_in     : in  std_logic_vector( 3 downto 0);
    link_fifo_full_in      : in  std_logic_vector( 3 downto 0);
    mb_ready_in            : in  std_logic;
    mb_fifo_empty_in       : in  std_logic;
    mode_bits_in           : in  std_logic_vector(23 downto 0); 
    mark_header            : in  std_logic_vector( 3 downto 0);
-- Outputs:    
    formatter_fifo_data_out : out std_logic_vector(31 downto 0);
--    link_num_has_token_out  : out std_logic_vector( 3 downto 0);
    timeout_error_out       : out std_logic;
    data_valid_out          : out std_logic;
    eoe                     : out std_logic;
    boe                     : out std_logic;
--    chip_has_token_out      : out std_logic;
    timeout_error_status    : out std_logic_vector( 3 downto 0);
    data_overflow_status    : out std_logic_vector( 3 downto 0);
    mb_ren_out              : out std_logic;
    fifo_ren_out            : out std_logic_vector( 3 downto 0);
    fifo_pause_out          : out std_logic_vector( 3 downto 0);
    readout_state_out       : out std_logic_vector( 3 downto 0);
    roc_ready_out           : out std_logic;
-- Additional Inputs
    token_in                : in  std_logic;
    na_count_in             : in  std_logic_vector(15 downto 0) -- Number of accepts in Event Data per link
    );
end fifo_readout_controller;

architecture rtl of fifo_readout_controller is
--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------

type states is (
  reset,       --                                        
  idle,        -- wait for mode                          
  fmb,         -- fetch current Mode Bits                
  wmb,         -- wait 1 clk for current Mode Bits                
  lmb,         -- latch current Mode Bits                
  wft,         -- wait for token 
  wfd,         -- wait for data 
  rolf,        -- read out link fifo                     
  doe,         -- dump one event from the link fifo
  roe,         -- read one event from the link fifo
  nlnk,        -- start readout of next link
  trto,        -- token to readout timeout error
  trtotr,      -- send to error trailer
  dovr,        -- data overflow condition error
  done         -- done with link event, pass token
  );
signal readout_state : states;

type   array5_of_unsigned4 is array (0 to 4) of unsigned(3 downto 0); --
signal na_counter_i : array5_of_unsigned4;

type   array5_of_stdlv32 is array (0 to 4) of std_logic_vector(31 downto 0); --
signal fifo_data_temp_i : array5_of_stdlv32;

signal to_count                : unsigned(31 downto 0);
signal to_error_i              : std_logic;
signal send_to_trailer_i       : std_logic;
signal en_error_i              : std_logic;
signal clr_to_counter_i        : std_logic;
signal dovr_count              : unsigned(15 downto 0);
signal dovr_error_i            : std_logic;
signal clr_dovr_counter_i      : std_logic;
signal inc_dovr_counter_i      : std_logic;
signal link_num                : natural RANGE 0 to 4;
signal link_count              : unsigned(2 downto 0);
signal output_link_count       : unsigned(2 downto 0);
signal clr_link_counter_i      : std_logic;
signal mask_data_valid_i       : std_logic;
signal mask_dv_n               : std_logic;
--signal chip_has_token_i   : std_logic;
signal data_overflow_i         : std_logic;
signal fifo_ren_i              : std_logic_vector(3 downto 0);
signal to_status_i             : std_logic_vector(3 downto 0);
signal dovr_status_i           : std_logic_vector(3 downto 0);
signal link_mb_i               : std_logic_vector(1 downto 0);

signal eoe_word_i              : std_logic_vector(4 downto 0);
signal boe_word_i              : std_logic_vector(4 downto 0);
signal data_valid_i            : std_logic_vector(4 downto 0);
signal trailer_count           : std_logic_vector(3 downto 0);
signal data_valid_int          : std_logic;
signal formatter_fifo_data_int : std_logic_vector(31 downto 0);
--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

begin

--------------------------------------------------------------------------
-- SIGNALS
--------------------------------------------------------------------------

fifo_ren_out         <= fifo_ren_i;
timeout_error_status <= to_status_i;
data_overflow_status <= dovr_status_i;

na_counter_i(0) <= unsigned(na_count_in( 3 downto  0));
na_counter_i(1) <= unsigned(na_count_in( 7 downto  4));
na_counter_i(2) <= unsigned(na_count_in(11 downto  8));
na_counter_i(3) <= unsigned(na_count_in(15 downto 12));
na_counter_i(4) <= (others => '0');

fifo_data_temp_i(0) <= link0_fifo_data_in;
fifo_data_temp_i(1) <= link1_fifo_data_in;
fifo_data_temp_i(2) <= link2_fifo_data_in;
fifo_data_temp_i(3) <= link3_fifo_data_in;
fifo_data_temp_i(4) <= (others => '0');

data_valid_i <= '0' & data_valid_in;
--data_valid_i	<= '0' & (data_valid_in AND fifo_ren_i); for FWFT FIFOs

-------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
-- State machine next state determination logic 
ro_state_logic : process (clk,rst)
begin
  if (rst = '1') then
--    chip_has_token_i   <= '0';
    data_overflow_i    <= '0';
    mb_ren_out         <= '0';
    fifo_ren_i         <= (others =>'0');
    fifo_pause_out     <= (others =>'0');
    link_mb_i          <= (others =>'0');
    link_count         <= (others =>'0');
    link_num           <=  0 ;
    clr_to_counter_i   <= '1';
    clr_dovr_counter_i <= '1';
    clr_link_counter_i <= '1';
    output_link_count  <= (others =>'1');
    readout_state      <= reset;
    mask_data_valid_i  <= '0';
    readout_state_out  <= (others =>'0');
    roc_ready_out      <= '0';
    en_error_i         <= '0';
    trailer_count      <= (others =>'0');
  elsif (rising_edge(clk)) then
    if (clr_link_counter_i = '1') then
      link_num <=  0 ;
    end if;      
    link_count <= CONV_UNSIGNED(link_num,3);
    if (enable_link_in = "0000") then
      readout_state      <= reset;
      data_overflow_i    <= '0';
      mb_ren_out         <= mb_ready_in;
      fifo_ren_i         <= (others =>'0');
      clr_to_counter_i   <= '1';
      clr_dovr_counter_i <= '1';
      clr_link_counter_i <= '1';
      output_link_count  <= (others =>'1');
      roc_ready_out      <= '1';
      en_error_i         <= '0';
      readout_state_out  <= "0001";
    else
      output_link_count <= link_count;
      en_error_i        <= '1';
      if (hold_output_in = '1') then
        fifo_pause_out <= (others => '1');
      else
        fifo_pause_out <= (others => '0');
      end if;

      case readout_state is
        when reset =>
--          chip_has_token_i   <= '0';
          data_overflow_i    <= '0';
          mb_ren_out         <= '0';
          fifo_ren_i         <= (others =>'0');
          clr_to_counter_i   <= '1';
          clr_dovr_counter_i <= '1';
          clr_link_counter_i <= '1';
          mask_data_valid_i  <= '0';
          roc_ready_out      <= '0';
          readout_state_out  <= "0001";
          readout_state      <= idle;

        when idle =>
--          chip_has_token_i   <= '0';
          data_overflow_i    <= '0';
          mb_ren_out         <= '0';
          fifo_ren_i         <= (others =>'0');
          clr_to_counter_i   <= '1';
          clr_dovr_counter_i <= '1';
          clr_link_counter_i <= '0';
          mask_data_valid_i  <= '0';
          roc_ready_out      <= '0';
          readout_state_out  <= "0010";

          if (mb_ready_in = '1' OR mb_fifo_empty_in = '0') then
            mb_ren_out    <= '1';
            readout_state <= fmb;
          end if;

        when fmb =>  -- Fetch Current Mode Bits
          mb_ren_out        <= '0';
          readout_state_out <= "0011";
          readout_state     <= wmb;
            
        when wmb =>  -- Wait 1 clk for ModeBits to arrive
          mb_ren_out    <= '0';
          readout_state <= lmb;

        when lmb =>  -- Latch Current Mode Bits
          readout_state <= wft;
          link_mb_i     <= mode_bits_in(2*(link_num)+1) & mode_bits_in(2*link_num);

        when wft =>  -- Wait for the Token to arrive
          roc_ready_out  <= '1';
          if(token_in = '1') then
            roc_ready_out   <= '0';
            if (enable_link_in(link_num) = '1') then
              case link_mb_i is
--              when "01"   =>  *** NOT IMPLEMENTED
--                readout_state        <= doe;
--                fifo_ren_i(link_num) <= NOT link_fifo_empty_in(link_num);
                when "10"   =>   -- skip link
                  readout_state <= nlnk;
                  link_num  <= link_num + 1 ;
                  link_mb_i <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
                when "11"   =>  -- Dump 1 EVT, Play 1 EVT
                  readout_state        <= doe;
                  fifo_ren_i(link_num) <= '1';
                when others =>
                  if (link_fifo_empty_in(link_num) = '1') then
                    readout_state <= wfd;
                    fifo_ren_i(link_num) <= '0';
                  else   -- Link ModeBits == "00"
                    readout_state <= rolf;
                    if (hold_output_in = '1') then 
                      fifo_pause_out <= (others => '1');
                      fifo_ren_i(link_num) <= '0';
                    else
                      fifo_pause_out <= (others => '0');
                      fifo_ren_i(link_num) <= '1';
                    end if;
                  end if;
               end case;
            else
              readout_state <= nlnk;
              link_num      <= link_num + 1 ;
              link_mb_i     <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
            end if;
          end if;
          readout_state_out <= "0100";

        when wfd =>
          clr_to_counter_i <= '0';
--          if (to_error_i = '1') then
--            readout_state <= trto;
--          elsif (link_fifo_empty_in(link_num) = '0' OR na_counter_i(link_num) /= 0) then
            readout_state <= rolf;
            fifo_ren_i(link_num) <= '1';
--            clr_to_counter_i <= '1';
--          end if;

        when rolf =>
            clr_to_counter_i <= hold_output_in OR NOT link_fifo_empty_in(link_num) OR mark_header(link_num);

          if(fifo_data_temp_i(link_num)(31 downto 29) = "010" AND data_valid_i(link_num) = '1') then 
            fifo_pause_out         <= (others => '0');
            link_mb_i              <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
            fifo_ren_i(link_num)   <= '0';
            trailer_count          <= trailer_count + 1;
            if(trailer_count = fmt_trig_cnt - 1) then
               readout_state <= nlnk;
               link_num      <= link_num + 1;
            else
               readout_state <= rolf;
            end if;
          elsif (to_error_i = '1') then
            clr_to_counter_i     <= '1';
            fifo_ren_i(link_num) <= '0';
            readout_state        <= trto;
          elsif (dovr_error_i = '1') then
            fifo_ren_i(link_num) <= '1';
            readout_state        <= dovr;
          else
             if(link_fifo_empty_in(link_num) = '0' AND hold_output_in = '0') then	-- DAV (20Oct2014)
                fifo_ren_i(link_num) <= NOT(fifo_ren_i(link_num));
             else
                fifo_ren_i(link_num) <= '0';
             end if;
             readout_state <= rolf; 
          end if;
 
          if (link_mb_i = "01") then
            mask_data_valid_i <= '1';
          else
            mask_data_valid_i <= '0';
          end if;
          readout_state_out <= "0101";

        when doe =>
          fifo_ren_i(link_num) <= '1';
          clr_to_counter_i     <= '1';
          mask_data_valid_i    <= '1';
          --if (eoe_word_i(link_num) = '1') then
          if (fifo_data_temp_i(link_num)(31 downto 29) = "010" AND data_valid_i(link_num) = '1') then
            readout_state        <= roe;
            fifo_ren_i(link_num) <= '0';
          end if;
          readout_state_out <= "0111";
          
        when roe =>
          readout_state        <= rolf;
          clr_to_counter_i     <= '1';
          clr_dovr_counter_i   <= '1';
          if (hold_output_in = '1') then
            fifo_ren_i(link_num) <= '0';
            fifo_pause_out       <= (others => '1');
          else
            fifo_ren_i(link_num) <= '1';
            fifo_pause_out       <= (others => '0');
          end if;

        when nlnk =>
          fifo_ren_i(link_num-1) <= '0';
          clr_dovr_counter_i     <= '1';
          clr_to_counter_i       <= '1';
          trailer_count          <= (others => '0');
          
		    if(link_num > 3) then
            readout_state <= done;
		    elsif (enable_link_in(link_num) = '1') then
            case link_mb_i is
--            when "01"   =>  *** NOT IMPLEMENTED
--              readout_state        <= doe;
--              fifo_ren_i(link_num) <= NOT link_fifo_empty_in(link_num);
              when "10"   =>   -- skip link
                readout_state <= nlnk;
                link_num      <= link_num + 1 ;
                link_mb_i     <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
              when "11"   =>  -- Dump 1 EVT, Play 1 EVT
                readout_state        <= doe;
                fifo_ren_i(link_num) <= NOT link_fifo_empty_in(link_num);
              when others =>  -- Link ModeBits == "00"
                if (link_fifo_empty_in(link_num) = '1') then
                   readout_state <= wfd;
                   fifo_ren_i(link_num) <= '0';
                else
                  readout_state <= rolf;
                  fifo_ren_i(link_num) <= NOT link_fifo_empty_in(link_num);
                end if;
            end case;
          else
            link_num  <= link_num + 1 ;
            link_mb_i <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
          end if;
          readout_state_out <= "1000";

        when trto =>  -- Should we flush the Link FIFO here?
          clr_to_counter_i <= '1';
          readout_state    <= trtotr;
          readout_state_out <= "1001";

        when trtotr =>  -- Should we flush the Link FIFO here?
          readout_state <= nlnk;
          link_num      <= link_num + 1 ;
          link_mb_i     <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
          readout_state_out <= "1001";
          
        when dovr => -- Should we dynamically mask this FIFO from this state?
          clr_dovr_counter_i   <= '1';
          readout_state <= nlnk;
          link_num      <= link_num + 1 ;
          link_mb_i     <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
          readout_state_out <= "1010";

        when done =>
          fifo_ren_i         <= (others => '0');
          readout_state      <= idle;
          clr_to_counter_i   <= '1';
          clr_dovr_counter_i <= '1';
          clr_link_counter_i <= '1';
          trailer_count      <= (others => '0');
          readout_state_out  <= "1100";

        when others =>
          if (link_num < 3) then
            readout_state <= nlnk;
            link_num      <= link_num + 1 ;
            link_mb_i     <= mode_bits_in((2*(link_num+1))+1) & mode_bits_in(2*(link_num+1));
          else
            readout_state <= done;
          end if;            
          readout_state_out <= "1111";
      end case;
    end if;
  end if;
end process ro_state_logic;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
output_data_select : process(rst, clk)
begin
  if(rst = '1') then
    formatter_fifo_data_int <= (others => '0');
    data_valid_int          <= '0';
    inc_dovr_counter_i      <= '0';
	 boe_word_i					 <= (others => '0');
	 eoe_word_i					 <= (others => '0');
	 send_to_trailer_i		 <= '0';
  elsif(rising_edge(clk)) then
    mask_dv_n 					<= NOT mask_data_valid_i;
    if(to_error_i = '1' AND en_error_i = '1') then
      formatter_fifo_data_int <= X"2" & '0' & std_logic_vector(link_count) & X"000bad";
      data_valid_int          <= to_error_i; 	-- so 1
      inc_dovr_counter_i      <= '0';
		boe_word_i(link_num)		<= '1'; 
		eoe_word_i(link_num)		<= '0';
		send_to_trailer_i			<= '1';
    elsif(send_to_trailer_i = '1') then
      formatter_fifo_data_int <= X"4" & '0' & std_logic_vector(link_count) & X"800bad"; -- timeout flag set
      data_valid_int 			<= '1';
		boe_word_i(link_num)		<= '0'; 
		eoe_word_i(link_num)		<= '1'; 
		send_to_trailer_i			<= '0';
    elsif(dovr_error_i = '1' AND en_error_i = '1') then
      formatter_fifo_data_int <= X"44000000";
      data_valid_int          <= dovr_error_i;
      inc_dovr_counter_i      <= '0';
		boe_word_i(link_num)		<= '0'; 
		eoe_word_i(link_num)		<= '1'; 
		send_to_trailer_i			<= '0';
    else		
      if(fifo_data_temp_i(link_num)(31 downto 29) = "001" AND data_valid_i(link_num) = '1') then		-- HEADER !
	     formatter_fifo_data_int 				<= fifo_data_temp_i(link_num);
        data_valid_int 							<= '1' AND mask_dv_n;
		  boe_word_i(link_num)					<= '1'; eoe_word_i(link_num)				<= '0';
		elsif(fifo_data_temp_i(link_num)(31 downto 29) = "010" AND data_valid_i(link_num) = '1') then	 -- TRAILER !
	     formatter_fifo_data_int 				<= fifo_data_temp_i(link_num);
        data_valid_int 							<= '1' AND mask_dv_n;
		  boe_word_i(link_num)					<= '0'; eoe_word_i(link_num)				<= '1';
      else           
        formatter_fifo_data_int 				<= fifo_data_temp_i(link_num);
        data_valid_int 							<= data_valid_i(link_num) AND mask_dv_n;
		  boe_word_i								<= (others => '0');	
		  eoe_word_i								<= (others => '0');
      end if;
      inc_dovr_counter_i 		<= data_valid_i(link_num) OR eoe_word_i(link_num);
		send_to_trailer_i			<= '0';
    end if;
  end if;
end process output_data_select;

	process(clk, rst)
	begin
		if(rst = '1') then
			formatter_fifo_data_out	<= (others => '0');
			data_valid_out				<= '0';
			boe							<= '0';
			eoe							<= '0';
		elsif(rising_edge(clk)) then
			if(trailer_count = "0000") then
				if(enable_link_in(0) = '1') then
					boe		<= boe_word_i(0);
				elsif(enable_link_in(1 downto 0) = "10") then
					boe		<= boe_word_i(1);
				elsif(enable_link_in(2 downto 0) = "100") then
					boe		<= boe_word_i(2);
				elsif(enable_link_in(3 downto 0) = "1000") then		
					boe		<= boe_word_i(3);
				end if;
			end if;
			if(trailer_count = fmt_trig_cnt or readout_state = trtotr) then
				if(enable_link_in(3) = '1') then
					eoe		<= eoe_word_i(3);
				elsif(enable_link_in(3 downto 2) = "01") then
					eoe		<= eoe_word_i(2);
				elsif(enable_link_in(3 downto 1) = "001") then
					eoe		<= eoe_word_i(1);
				elsif(enable_link_in(3 downto 0) = "0001") then		
					eoe		<= eoe_word_i(0);
				end if;	
			else
				eoe	<= '0';
			end if;
			formatter_fifo_data_out	<= formatter_fifo_data_int;
			data_valid_out				<= data_valid_int;
		end if;
	end process;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
output_control_reg : process (
  rst,
  clk
  )
begin
  if (rst = '1') then
--    chip_has_token_out     <= '0';
    timeout_error_out      <= '0';
--    link_num_has_token_out <= (others => '0');
  elsif (clk'event AND clk = '1') then
--    chip_has_token_out     <= chip_has_token_i;
    timeout_error_out      <= to_error_i;
--    link_num_has_token_out <= '0' & std_logic_vector(output_link_count);
  end if;
end process output_control_reg;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
error_status : process (
  rst,
  clk
  )
begin
  if (rst = '1') then
    to_status_i   <= (others => '0');
    dovr_status_i <= (others => '0');
  elsif (clk'event AND clk = '1') then
    case output_link_count is 
      when "000" => 
        to_status_i   <= "000" & to_error_i;
        dovr_status_i <= "000" & dovr_error_i;
      when "001" =>
        to_status_i   <= "00" & to_error_i   & '0';
        dovr_status_i <= "00" & dovr_error_i & '0';
      when "010" =>
        to_status_i   <= "0" & to_error_i   & "00";
        dovr_status_i <= "0" & dovr_error_i & "00";
      when "011" =>
        to_status_i   <= to_error_i   & "000";
        dovr_status_i <= dovr_error_i & "000";
      when others =>
        to_status_i   <= (others => '0');
        dovr_status_i <= (others => '0');
    end case;
  end if;
end process error_status;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
timeout_counter : process(clk, rst)
begin
  if(rst = '1') then
    to_count   <= (others => '0');
    to_error_i <= '0';
  elsif(rising_edge(clk)) then
    if(clr_to_counter_i = '1') then
      to_count <= (others => '0');
    else
      to_count <= to_count + 1;			
    end if;
    
    if (to_count = unsigned(fifo_timeout_limit_in) AND en_error_i = '1') then
      to_error_i <= '1'; 
    else
      to_error_i <= '0';
    end if;
  end if;
end process timeout_counter;
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
data_overflow_counter : process(clk, rst)
begin
  if(rst = '1') then
    dovr_count   <= (others => '0');
    dovr_error_i <= '0';
  elsif(rising_edge(clk)) then
    if(clr_dovr_counter_i = '1') then
      dovr_count <= (others => '0');
    elsif(inc_dovr_counter_i = '1') then
      dovr_count <= dovr_count + 1;
    end if;
    
    if(dovr_count = unsigned(data_overflow_limit_in) AND en_error_i = '1') then
--    if (dovr_count = data_overflow_limit_in) then
      dovr_error_i <= '1';
    else
      dovr_error_i <= '0';
    end if;
  end if;
end process data_overflow_counter;

end rtl; -- end code for fifo_readout_controller 
