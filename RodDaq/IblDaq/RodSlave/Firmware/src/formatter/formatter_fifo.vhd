----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:09:03 06/11/2014 
-- Design Name: 
-- Module Name:    formatter_fifo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity formatter_fifo is
  port (
    clk: 				in  std_logic;
    rst: 				in  std_logic;
    din: 				in  std_logic_vector(31 downto 0);
    wr_en: 				in  std_logic;
    rd_en: 				in  std_logic;
    trigger_in:      in  std_logic;
    rod_busy_limit:  in  std_logic_vector( 9 downto 0);
    dout: 				out std_logic_vector(31 downto 0);
    full: 				out std_logic;
    empty: 				out std_logic;
    valid: 				out std_logic;
    link_busy:       out std_logic;
    frames_in:       out std_logic_vector( 3 downto 0);
    link_occu_out:   out std_logic_vector( 9 downto 0)
		);
end formatter_fifo;

architecture Behavioral of formatter_fifo is

COMPONENT fifo_1024x32
  PORT (
    clk         : IN  STD_LOGIC;
    rst         : IN  STD_LOGIC;
    din         : IN  STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en       : IN  STD_LOGIC;
    rd_en       : IN  STD_LOGIC;
    dout        : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
    full        : OUT STD_LOGIC;
    wr_ack      : OUT STD_LOGIC;
    empty       : OUT STD_LOGIC;
    valid       : OUT STD_LOGIC;
    data_count  : OUT STD_LOGIC_VECTOR( 9 downto 0)
  );
END COMPONENT;

signal fifo_data_out:			std_logic_vector(31 downto 0);
signal valid_fifo:				std_logic;
signal link_occu_out_i:       std_logic_vector( 9 downto 0);
signal frame_count:           unsigned        ( 3 downto 0);
signal wr_ack, wr_attempt:    std_logic;

begin

dout 		     <= fifo_data_out;
valid		     <= valid_fifo;
link_occu_out <= link_occu_out_i;
frames_in     <= std_logic_vector(frame_count);

fifo_1024x32_instance : fifo_1024x32
  PORT MAP (
    clk        => clk,
    rst        => rst,
    din        => din,
    wr_en      => wr_en,
    rd_en      => rd_en,
    dout       => fifo_data_out,
    full       => full,
    wr_ack     => wr_ack,
    empty      => empty,
    valid      => valid_fifo,
    data_count => link_occu_out_i
  );

--frame_counter: process(clk, rst)
--begin
--   if(rst = '1') then
--      frame_count <= x"0";
--   elsif(rising_edge(clk)) then
--      if(trigger_in = '1') then
--         frame_count  <= x"0";
--      elsif( din(31 downto 29) = "010" and wr_en = '1') then
--         wr_attempt   <= '1';
--      elsif(wr_attempt = '1' and wr_ack = '1') then --Have to wait 1 clock for wr_ack anyway
--         frame_count  <= frame_count + 1;
--         wr_attempt   <= '0';
--      end if;
--   end if;
--end process frame_counter;

frame_counter: process(clk, rst)
begin
   if(rst = '1') then
      frame_count 	<= x"0";
      wr_attempt   	<= '0';
   elsif(rising_edge(clk)) then
      if(trigger_in = '1') then
         frame_count  <= x"0";
         wr_attempt   <= '0';
      elsif(fifo_data_out(31 downto 29) = "010" and valid_fifo = '1') then			-- trailer read out
         wr_attempt   <= '1';
      elsif(wr_attempt = '1') then 
         frame_count  <= frame_count + 1;
         wr_attempt   <= '0';
      end if;
   end if;
end process frame_counter;

busy_limit : process(clk, rst)
begin
   if(rst = '1') then
       link_busy       <= '0';
   elsif(rising_edge(clk)) then
       --Check occupancy against busy limit
       if(link_occu_out_i >= rod_busy_limit) then
          link_busy <= '1';
       else
          link_busy <= '0';
       end if;
   end if;
end process busy_limit;

end Behavioral;

