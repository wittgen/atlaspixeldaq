----------------------------------------------------------------------------------
-- Company: INFN (Bologna), University of Washington (Seattle, WA)
-- Engineer: Davide Falchieri, Shaw-Pin (Bing) Chen
-- 
-- Create Date:    21:05:32 10/09/2013 
-- Design Name: 
-- Module Name:    formatter_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Convert fifos, rearrange code
--
-- Additional Comments: 
-- some signals are over-lengthed. needs to reduce lengths.
-- need to add the register block
-- occupancy counter?
-- take out chip and link number has token out signals, they're not used...

--  need to implement the following:
	 -- fmt_exp_mode_en	      	: in STD_LOGIC_VECTOR(3 downto 0);
	 -- fmt_header_trailer_limit	: in STD_LOGIC_VECTOR(31 downto 0);
	 -- fmt_rod_busy_limit		   : in STD_LOGIC_VECTOR(31 downto 0);
	 -- fmt_pxl_link_l1a_cnt		: in STD_LOGIC_VECTOR(31 downto 0);
	 -- readout_state_i
	 -- roc_ready_i
	 
-- check the following:
    -- time_out_error_i
	 -- na_count_i is not determined anywhere. Set to all 0 for now.
	 -- fmt_type_i not needed?
	 -- fmt_number_i not needed?
	 -- decoder_wr_trailer_i
	 
-- took out
	 -- fmt_type_i
	 -- fmt_number_i
	 
-- line 470 BUGGGG
	 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.all; -- needed for +/- operations
use IEEE.STD_LOGIC_unsigned.all; -- needed for +/- operations

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity formatter_top is
  port (
		clk40   						   : in  STD_LOGIC;-- 40 MHz
		clk80   						   : in  STD_LOGIC;-- 80 MHz
		rst   						   : in  STD_LOGIC;
		hold_output					   : in  STD_LOGIC;
		fmt_stts_reg				   : out STD_LOGIC_VECTOR(31 downto 0);
		-- inputs from BOC
		we_from_boc				      : in  STD_LOGIC;
		data_from_boc 			      : in  STD_LOGIC_VECTOR(7 downto 0);
		addr_from_boc			      : in  STD_LOGIC_VECTOR(1 downto 0);
		cntrl_from_boc		 	      : in  STD_LOGIC;
		-- inputs from register_block
		fmt_link_enabled		      : in  STD_LOGIC_VECTOR(3 downto 0);
      fmt_trig_cnt               : in  STD_LOGIC_VECTOR(3 downto 0);
		fmt_readout_timeout	   	: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt_data_overflow_limit		: in  STD_LOGIC_VECTOR(15 downto 0);
		fmt_header_trailer_limit	: in  STD_LOGIC_VECTOR(31 downto 0);
		fmt_rod_busy_limit	   	: in  STD_LOGIC_VECTOR( 9 downto 0);
      fmt_trailer_timeout	   	: in  STD_LOGIC_VECTOR(31 downto 0);
      fmt_occupancy_out          : out STD_LOGIC_VECTOR(39 downto 0);
      fmt_ht_error               : out STD_LOGIC_VECTOR( 3 downto 0);
      fmt_timeout_error          : out STD_LOGIC_VECTOR( 3 downto 0);          
		-- outputs to EFB
		valid_to_efb					: out STD_LOGIC;
		data_to_efb		  				: out STD_LOGIC_VECTOR(34 downto 0);
		boe					  			: out STD_LOGIC; -- beginning of event
		eoe					  			: out STD_LOGIC; -- end of event
		fifo_busy         			: out STD_LOGIC_VECTOR(3 downto 0);

      formatter_frames           : out STD_LOGIC_VECTOR(15 downto 0);
 
      fe_cmdpulse                : in  STD_LOGIC;
      --mode bits
		mode_bits_in               : in  STD_LOGIC_VECTOR(11 downto 0); -- 12 wire time multiplexed mode bits
		modebits_fifo_wen_n_in     : in  STD_LOGIC; -- modebit fifos writein strobe: strobe 0 - zero bits, strobe 1 - one bits
		modebits_fifo_rst_n_in     : in  STD_LOGIC; -- modebit fifos reset strobe
		modebits_fifo_ef_n_out     : out STD_LOGIC; -- at least one of the readout mode fifos is empty
		modebits_fifo_ff_n_out     : out STD_LOGIC 	-- at least one of the readout mode fifos is full
  );
end formatter_top;

architecture Behavioral of formatter_top is
--------------------------------------------------------------------------
--                       SIGNAL DECLARATIONS
--------------------------------------------------------------------------
	signal data_from_boc_i		: STD_LOGIC_VECTOR(7 downto 0);
	signal reg_enable				: STD_LOGIC_VECTOR(3 downto 0);
	signal cntrl_from_boc_i		: STD_LOGIC;
	signal link_valid_i			: STD_LOGIC_VECTOR(3 downto 0);
	signal fifo_valid				: STD_LOGIC_VECTOR(3 downto 0);

	signal link_data_0     		: STD_LOGIC_VECTOR(31 downto 0);
	signal link_data_1     		: STD_LOGIC_VECTOR(31 downto 0);
	signal link_data_2     		: STD_LOGIC_VECTOR(31 downto 0);
	signal link_data_3     		: STD_LOGIC_VECTOR(31 downto 0);

	signal hold_output_in_i     : STD_LOGIC;
	signal time_out_error_out_i : STD_LOGIC;
	signal mb_fifo_ef_i         : STD_LOGIC;

	type   fifo_data_array_type is array (0 to 3) of STD_LOGIC_VECTOR(31 downto 0); 
	signal link_fifo_data_i  		: fifo_data_array_type;  

	--signal header_trailer_error_i : STD_LOGIC_VECTOR(11 downto 0);
	--signal rod_busy_error_i       : STD_LOGIC_VECTOR(11 downto 0);
	signal data_overflow_error_i  : STD_LOGIC_VECTOR(3 downto 0);

	signal timeout_limit_i        : STD_LOGIC_VECTOR(31 downto 0);
   signal trailer_limit_i        : STD_LOGIC_VECTOR(31 downto 0);
	signal overflow_limit_i       : STD_LOGIC_VECTOR(15 downto 0);
	--signal header_trailer_limit_i : unsigned(15 downto 0);
	--signal rod_busy_limit_i       : unsigned( 9 downto 0);

	signal fifo_ren_i             : STD_LOGIC_VECTOR(3 downto 0); -- changed from length of 12
	signal link_fifo_ef_i         : STD_LOGIC_VECTOR(3 downto 0);
	signal link_fifo_ff_i         : STD_LOGIC_VECTOR(3 downto 0);
   signal link_busy_i            : STD_LOGIC_VECTOR(3 downto 0);
   signal mark_header_i          : STD_LOGIC_VECTOR(3 downto 0);

	signal roc_mb_ren_i           : STD_LOGIC;
	--signal rb_mb_ren_i            : STD_LOGIC;
	signal mb_fifo_ren_i          : STD_LOGIC;
	signal mb_ready_i             : STD_LOGIC;
	signal link_mb_i              : STD_LOGIC_VECTOR(23 downto 0);

	-- may be useful to check FIFO controller internal state
	signal readout_state_i        : STD_LOGIC_VECTOR( 3 downto 0);
	signal roc_ready_i            : STD_LOGIC;

	signal na_count_i             : STD_LOGIC_VECTOR(15 downto 0) := x"0000"; -- for now...
	signal fifo_pause_i           : STD_LOGIC_VECTOR( 3 downto 0);
   signal boe_word_i             : STD_LOGIC_VECTOR( 3 downto 0);

	signal modebits_fifo_ef_n_out_p   : STD_LOGIC;
	signal formatter_fifo_data_out    : STD_LOGIC_VECTOR(31 downto 0);
   signal token_i                    : STD_LOGIC;
   signal trigger_count              : STD_LOGIC_VECTOR( 9 downto 0);
   signal eoe_word_i                 : STD_LOGIC_VECTOR( 3 downto 0);
   signal trig_cnt_i                 : STD_LOGIC_VECTOR( 3 downto 0);
--------------------------------------------------------------------------
--                    COMPONENT DECLARATIONS
--------------------------------------------------------------------------
	component link_encoder
		port ( 
			clk					  : in  STD_LOGIC;
			rst					  : in  STD_LOGIC;
			register_enable	  : in  STD_LOGIC;
			control_sig			  : in  STD_LOGIC;
			data_in				  : in  STD_LOGIC_VECTOR( 7 downto 0);
			channel_number	     : in  STD_LOGIC_VECTOR( 1 downto 0);
         trailer_timeout     : in  STD_LOGIC_VECTOR(31 downto 0);
         header_trailer_limit: in  STD_LOGIC_VECTOR(31 downto 0);
			link_data			  : out STD_LOGIC_VECTOR(31 downto 0);
         mark_header_out     : out STD_LOGIC;
         link_ht_error       : out STD_LOGIC;
			link_valid			  : out STD_LOGIC
     	);
	end component link_encoder;

   COMPONENT formatter_fifo 
		PORT (
			clk 		     : IN  STD_LOGIC;
			rst 		     : IN  STD_LOGIC;
			din 		     : IN  STD_LOGIC_VECTOR(31 downto 0);
			wr_en 	     : IN  STD_LOGIC;
			rd_en 	     : IN  STD_LOGIC;
         trigger_in    : IN  STD_LOGIC;
         rod_busy_limit: IN  STD_LOGIC_VECTOR( 9 downto 0);
			dout 		     : OUT STD_LOGIC_VECTOR(31 downto 0);
			full 		     : OUT STD_LOGIC;
			empty 	     : OUT STD_LOGIC;
			valid 	     : OUT STD_LOGIC;
         link_busy     : OUT STD_LOGIC;
         frames_in     : OUT STD_LOGIC_VECTOR( 3 downto 0);
         link_occu_out : OUT STD_LOGIC_VECTOR( 9 downto 0)
		);
	END COMPONENT;

	component mb_data_decode
		port(
			clk40            : in  STD_LOGIC; -- clk40 input
			clk80            : in  STD_LOGIC; -- clk40 input
			rst              : in  STD_LOGIC; -- asynchronous global reset
			mode_bits_in     : in  STD_LOGIC_VECTOR(11 downto 0);
			mb_fifo_wen_n_in : in  STD_LOGIC; -- mb fifos wr strobe: active low
			mb_fifo_ren_in   : in  STD_LOGIC; -- mb fifos rd strobe: active high
			mb_fifo_rst_in   : in  STD_LOGIC; -- modebit fifos reset strobe
			mode_bits_out    : out STD_LOGIC_VECTOR(23 downto 0);
			mb_ready_out     : out STD_LOGIC; 
			mb_fifo_ef_n_out : out STD_LOGIC; -- readout mode fifo is empty
			mb_fifo_ff_n_out : out STD_LOGIC  -- readout mode fifo is full 
		);
	end component; 

	component fifo_readout_controller
		port(
			clk	                  : in  STD_LOGIC;
			rst		               : in  STD_LOGIC;
			enable_link_in          : in  STD_LOGIC_VECTOR(3 downto 0);
			hold_output_in          : in  STD_LOGIC;
			fifo_timeout_limit_in   : in  STD_LOGIC_VECTOR(31 downto 0); 
			data_overflow_limit_in  : in  STD_LOGIC_VECTOR(15 downto 0); 
         fmt_trig_cnt            : in  STD_LOGIC_VECTOR( 3 downto 0);
			link0_fifo_data_in      : in  STD_LOGIC_VECTOR(31 downto 0);
			link1_fifo_data_in      : in  STD_LOGIC_VECTOR(31 downto 0);
			link2_fifo_data_in      : in  STD_LOGIC_VECTOR(31 downto 0);
			link3_fifo_data_in      : in  STD_LOGIC_VECTOR(31 downto 0);
			data_valid_in           : in  STD_LOGIC_VECTOR( 3 downto 0);
			link_fifo_empty_in      : in  STD_LOGIC_VECTOR( 3 downto 0);
			link_fifo_full_in       : in  STD_LOGIC_VECTOR( 3 downto 0);
			mb_ready_in             : in  STD_LOGIC;
			mb_fifo_empty_in        : in  STD_LOGIC;
			mode_bits_in            : in  STD_LOGIC_VECTOR(23 downto 0); 
         mark_header             : in  STD_LOGIC_VECTOR( 3 downto 0);
			formatter_fifo_data_out : out STD_LOGIC_VECTOR(31 downto 0);
			timeout_error_out       : out STD_LOGIC;
			data_valid_out          : out STD_LOGIC;
         boe                     : out STD_LOGIC;
         eoe                     : out STD_LOGIC;
			timeout_error_status    : out STD_LOGIC_VECTOR( 3 downto 0);
			data_overflow_status    : out STD_LOGIC_VECTOR( 3 downto 0);
			mb_ren_out              : out STD_LOGIC;
			fifo_ren_out            : out STD_LOGIC_VECTOR( 3 downto 0);
			fifo_pause_out          : out STD_LOGIC_VECTOR( 3 downto 0);
			readout_state_out       : out STD_LOGIC_VECTOR( 3 downto 0);
			roc_ready_out           : out STD_LOGIC;
         token_in                : in  STD_LOGIC;
			na_count_in             : in  STD_LOGIC_VECTOR(15 downto 0) -- Number of accepts in Event Data per link
		);
	end component;

	component tctg  -- Trigger Counter/Token Generator
		port
		(
		 clk                 : in  std_logic;
		 rst                 : in  std_logic;
		 roc_ready_in        : in  std_logic;
		 l1_trigger_pulse_in : in  std_logic;
		 token_out           : out std_logic;
		 trigger_count_out   : out std_logic_vector(9 downto 0)
		 );
   end component;

begin

	-- reserved for debugging
	fmt_stts_reg <= X"FF1234FF";

	mb_fifo_ef_i	<= NOT(modebits_fifo_ef_n_out_p);
	modebits_fifo_ef_n_out	<= modebits_fifo_ef_n_out_p;
	data_to_efb	<= '0' & '0' & time_out_error_out_i & formatter_fifo_data_out;
	-- second MSB was condensed mode bit but it does not exist for Pixel/IBL
	
	fifo_busy	<= link_busy_i OR link_fifo_ff_i;

	link_encoder0 : link_encoder
		port map(
			clk	 	     		   => clk80,
			rst         		   => rst,
			data_in  			   => data_from_boc_i,
			register_enable	   => reg_enable(0),
			control_sig			   => cntrl_from_boc_i,
			channel_number	      => "00",
         trailer_timeout      => std_logic_vector(trailer_limit_i),
         header_trailer_limit => fmt_header_trailer_limit,
			link_data			   => link_data_0,
         mark_header_out      => mark_header_i(0),
         link_ht_error        => fmt_ht_error(0),
			link_valid			   => link_valid_i(0)
		);

	link_encoder1 : link_encoder
    port map(
			clk	 	     		   => clk80,
			rst         		   => rst,
			data_in  			   => data_from_boc_i,
			register_enable	   => reg_enable(1),
			control_sig			   => cntrl_from_boc_i,
			channel_number	      => "01",
         trailer_timeout      => std_logic_vector(trailer_limit_i),
         header_trailer_limit => fmt_header_trailer_limit,
			link_data			   => link_data_1,
         mark_header_out      => mark_header_i(1),
         link_ht_error        => fmt_ht_error(1),
			link_valid			   => link_valid_i(1)
		);

	link_encoder2 : link_encoder
    port map(
			clk	 	     		   => clk80,
			rst         		   => rst,
			data_in  			   => data_from_boc_i,
			register_enable	   => reg_enable(2),
			control_sig			   => cntrl_from_boc_i,
			channel_number	      => "10",
         trailer_timeout      => std_logic_vector(trailer_limit_i),
         header_trailer_limit => fmt_header_trailer_limit,
			link_data			   => link_data_2,
         mark_header_out      => mark_header_i(2),
         link_ht_error        => fmt_ht_error(2),
			link_valid			   => link_valid_i(2)
		);

	link_encoder3 : link_encoder
    port map(
			clk	 	     	      => clk80,
			rst         		   => rst,
			data_in  			   => data_from_boc_i,
			register_enable	   => reg_enable(3),
			control_sig			   => cntrl_from_boc_i,
			channel_number	      => "11",
         trailer_timeout      => std_logic_vector(trailer_limit_i),
         header_trailer_limit => fmt_header_trailer_limit,
			link_data			   => link_data_3,
         mark_header_out      => mark_header_i(3),
         link_ht_error        => fmt_ht_error(3),
			link_valid			   => link_valid_i(3)
		);

   data_fifo0 : formatter_fifo 
		port map(
			clk 		     => clk80,
			rst 		     => rst,
			din 		     => link_data_0,
			wr_en 	     => link_valid_i(0),
			rd_en 	     => fifo_ren_i(0),
         trigger_in    => fe_cmdpulse,
         rod_busy_limit=> std_logic_vector(fmt_rod_busy_limit),
			dout 		     => link_fifo_data_i(0),
			full 		     => link_fifo_ff_i(0),
			empty 	     => link_fifo_ef_i(0),
			valid 	     => fifo_valid(0),
         link_busy     => link_busy_i(0),
         frames_in     => formatter_frames(3 downto 0),
         link_occu_out => fmt_occupancy_out(9 downto 0)
		);

   data_fifo1 : formatter_fifo
		port map(
			clk 		     => clk80,
			rst 		     => rst,
			din 		     => link_data_1,
			wr_en 	     => link_valid_i(1),
			rd_en 	     => fifo_ren_i(1),
         trigger_in    => fe_cmdpulse,
         rod_busy_limit=> std_logic_vector(fmt_rod_busy_limit), 
			dout 		     => link_fifo_data_i(1),
			full 		     => link_fifo_ff_i(1),
			empty 	     => link_fifo_ef_i(1),
			valid 	     => fifo_valid(1),
         link_busy     => link_busy_i(1),
         frames_in     => formatter_frames(7 downto 4),
         link_occu_out => fmt_occupancy_out(19 downto 10)
		);

   data_fifo2 : formatter_fifo 
		port map(
			clk 		     => clk80,
			rst 		     => rst,
			din 		     => link_data_2,
			wr_en 	     => link_valid_i(2),
			rd_en 	     => fifo_ren_i(2),
         trigger_in    => fe_cmdpulse,
         rod_busy_limit=> std_logic_vector(fmt_rod_busy_limit),
			dout 		     => link_fifo_data_i(2),
			full 		     => link_fifo_ff_i(2),
			empty 	     => link_fifo_ef_i(2),
			valid 	     => fifo_valid(2),
         link_busy     => link_busy_i(2),
         frames_in     => formatter_frames(11 downto 8),
         link_occu_out => fmt_occupancy_out(29 downto 20) 
		);

   data_fifo3 : formatter_fifo
		port map(
			clk 		     => clk80,
			rst 		     => rst,
			din 		     => link_data_3,
			wr_en 	     => link_valid_i(3),
			rd_en 	     => fifo_ren_i(3),
         trigger_in    => fe_cmdpulse,
         rod_busy_limit=> std_logic_vector(fmt_rod_busy_limit),
			dout 		     => link_fifo_data_i(3),
			full 		     => link_fifo_ff_i(3),
			empty 	     => link_fifo_ef_i(3),
			valid 	     => fifo_valid(3),
         link_busy     => link_busy_i(3),
         frames_in     => formatter_frames(15 downto 12),
         link_occu_out => fmt_occupancy_out(39 downto 30)
		);

	mb_data_decode_instance : mb_data_decode
		port map (
			clk40                  => clk40,
			clk80						  => clk80,
			rst                    => rst,
			mode_bits_in           => mode_bits_in,
			mb_fifo_wen_n_in       => modebits_fifo_wen_n_in,
			mb_fifo_ren_in         => mb_fifo_ren_i,
			mb_fifo_rst_in         => rst,  		--	modebits_fifo_rst_n_in,
			mode_bits_out          => link_mb_i,		-- 	to register_block and fifo_readout_controller
			mb_ready_out           => mb_ready_i,
			mb_fifo_ef_n_out       => modebits_fifo_ef_n_out_p,
			mb_fifo_ff_n_out       => modebits_fifo_ff_n_out
    );

--mb_fifo_ren_i      <= roc_mb_ren_i OR rb_mb_ren_i;	-- from register_block or from fifo_readout_controller
  mb_fifo_ren_i      <= roc_mb_ren_i;				-- ONLY from fifo_readout_controller

	fifo_readout_controller_instance : fifo_readout_controller
		port map(
			clk                     => clk80,
			rst                     => rst,
			enable_link_in          => fmt_link_enabled,
			hold_output_in          => hold_output_in_i,
			fifo_timeout_limit_in   => timeout_limit_i,
			data_overflow_limit_in  => overflow_limit_i,
         fmt_trig_cnt            => trig_cnt_i,
			link0_fifo_data_in      => link_fifo_data_i(0),
			link1_fifo_data_in      => link_fifo_data_i(1),
			link2_fifo_data_in      => link_fifo_data_i(2),
			link3_fifo_data_in      => link_fifo_data_i(3),
			data_valid_in           => fifo_valid,
			link_fifo_empty_in      => link_fifo_ef_i(3 downto 0),
			link_fifo_full_in       => link_fifo_ff_i(3 downto 0),
			mb_ready_in             => '1',--=> mb_ready_i,
			mb_fifo_empty_in        => '0',--=> mb_fifo_ef_i,
			mode_bits_in            => X"000000",--=> link_mb_i,
         mark_header             => mark_header_i,
			formatter_fifo_data_out => formatter_fifo_data_out,
			timeout_error_out       => time_out_error_out_i,
			data_valid_out          => valid_to_efb,
         boe                     => boe,
         eoe                     => eoe,
			timeout_error_status    => fmt_timeout_error(3 downto 0),
			data_overflow_status    => data_overflow_error_i(3 downto 0),
			mb_ren_out              => roc_mb_ren_i,
			fifo_ren_out            => fifo_ren_i(3 downto 0),
			fifo_pause_out          => fifo_pause_i,
			readout_state_out       => readout_state_i,
			roc_ready_out           => roc_ready_i,
         token_in                => token_i,
			na_count_in             => na_count_i
		); 

	tctg_instance: tctg		
		port map
		(
		 clk                 => clk80,
		 rst                 => rst,
		 roc_ready_in        => roc_ready_i,
		 l1_trigger_pulse_in => fe_cmdpulse,
		 token_out           => token_i,
		 trigger_count_out   => trigger_count
		 );

	fill_in_buffers_process: process(clk80, rst)
	begin 
		if(rst = '1') then
			reg_enable 	<= "0000";
			data_from_boc_i <= (others => '0');
		elsif(rising_edge(clk80)) then
			if(we_from_boc = '1') then
				if(addr_from_boc = "00")then
					reg_enable <= "0001" AND fmt_link_enabled;
				elsif(addr_from_boc = "01")then
					reg_enable <= "0010" AND fmt_link_enabled;
				elsif(addr_from_boc = "10")then
					reg_enable <= "0100" AND fmt_link_enabled;
				else			-- addr_from_boc = "11"
					reg_enable <= "1000" AND fmt_link_enabled;
				end if;
				data_from_boc_i 	<= data_from_boc;
				cntrl_from_boc_i 	<= cntrl_from_boc;
			else 
				reg_enable <= "0000";
			end if;
		end if;
	end process fill_in_buffers_process;

	process(clk40, rst)
	begin
		if(rst = '1') then
			hold_output_in_i	<= '0';
			timeout_limit_i	<= (others => '0');
			overflow_limit_i	<= (others => '0');
         trailer_limit_i	<= (others => '0');
		elsif(rising_edge(clk40)) then
			hold_output_in_i	<= hold_output;
			timeout_limit_i	<= fmt_readout_timeout;
         trailer_limit_i	<= fmt_trailer_timeout;
			overflow_limit_i	<= fmt_data_overflow_limit;
         trig_cnt_i        <= fmt_trig_cnt;
		end if;
	end process;
	
end Behavioral;
