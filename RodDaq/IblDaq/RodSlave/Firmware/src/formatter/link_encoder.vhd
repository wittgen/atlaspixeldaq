----------------------------------------------------------------------------------
-- Company:  University of Washington, Seattle
-- Engineer:  Joe Mayer
-- 
-- Create Date:    14:32:03 02/18/2011 
-- Design Name: 
-- Module Name:    header_finder - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
-- TODO: Improve sequencing to even better with FEI4 format
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_arith.all; -- needed for +/- operations
use IEEE.STD_LOGIC_unsigned.all; -- needed for unsigned 
-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
entity link_encoder is
	port ( 
		clk                 : in  STD_LOGIC;
		rst			   	  : in  STD_LOGIC;
		register_enable	  : in  STD_LOGIC;
		control_sig			  : in  STD_LOGIC;
		data_in				  : in  STD_LOGIC_VECTOR( 7 downto 0);
		channel_number	     : in  STD_LOGIC_VECTOR( 1 downto 0);
      trailer_timeout	  : in  STD_LOGIC_VECTOR(31 downto 0);
      header_trailer_limit: in  STD_LOGIC_VECTOR(31 downto 0);
		link_data			  : out STD_LOGIC_VECTOR(31 downto 0);
      mark_header_out     : out STD_LOGIC;
      link_ht_error       : out STD_LOGIC;
		link_valid			  : out STD_LOGIC
  );
end link_encoder;

architecture behavioral of link_encoder is
   --States
   constant wfd              : STD_LOGIC_VECTOR(1 downto 0) := "00"; --Wait for Data
   constant header           : STD_LOGIC_VECTOR(1 downto 0) := "01"; --Header state
   constant hit              : STD_LOGIC_VECTOR(1 downto 0) := "11"; --Hit state
   constant trailer          : STD_LOGIC_VECTOR(1 downto 0) := "10"; --Trailer state

	signal register_enable1   : STD_LOGIC;
	signal counter            : integer range 0 to 100;
	signal data_reg		     : STD_LOGIC_VECTOR(23 downto 0) := (others => '0');
	signal skipped			     : STD_LOGIC_VECTOR( 9 downto 0) := "0000000000";
	signal flag_errors		  : STD_LOGIC_VECTOR( 8 downto 0) := "000000000";
	signal lvinfo			     : STD_LOGIC_VECTOR( 4 downto 0);
   signal l1id_msbs          : STD_LOGIC_VECTOR( 7 downto 0) := X"00";
   signal mark_header        : STD_LOGIC; --Mark you have seen header
   signal trailer_counter    : UNSIGNED(31 downto 0) := x"00000000";
   signal ht_counter         : UNSIGNED(31 downto 0) := x"00000000";
   signal record_count       : UNSIGNED(31 downto 0) := x"00000000"; --Waiting on record flood decision
--Also waiting on other Record decisions
--signal dh_found:				STD_LOGIC;

begin

 link_ht_error   <= flag_errors(1); --send to fmtHeaderTrailerErr
 mark_header_out <= mark_header;

  -- SPC bug fix: find_header_proc changed if state condition order
 find_header_proc: process(clk, rst)
 begin
 	if(rst = '1')then
 		counter 				<= 0;
 		data_reg 			<= (others => '0');	
 		register_enable1 	<= '0';
 	elsif (rising_edge(clk)) then
 		if(register_enable = '1')then
 			if(control_sig = '1')then
 				counter <= 0;
 			elsif(counter = 3)then
 				counter <= 1;
 			else
 				counter <= counter + 1;
 			end if;
 			data_reg <= data_reg(15 downto 0) & data_in;
 			register_enable1 <= '1';
 		else
 			register_enable1 <= '0';
 		end if;	
 	end if;
 end process find_header_proc;
 
 output_proc: process(rst, clk)
 variable data_type : std_logic_vector(1 downto 0) := "00";
 begin
   if(rst = '1') then
      link_valid 	    <= '0';
      mark_header     <= '0';
      trailer_counter <= x"00000000";
      ht_counter      <= x"00000000";
      record_count    <= x"00000000";
      link_data	    <= (others => '0');
      lvinfo          <= (others => '0');
      l1id_msbs       <= (others => '0');
      skipped         <= (others => '0');
      flag_errors     <= (others => '0');
      data_type       := wfd;
   elsif(rising_edge(clk)) then
      data_type := wfd; --Variable default value
      --Control
      if(register_enable1 = '1' AND counter = 3) then --Valid Data
         if(mark_header = '0') then
            data_type := header;
         elsif(mark_header = '1') then
            data_type := hit;
         else
            data_type := wfd;
         end if;
      --Checks for single bit flip in trailer
      elsif(((register_enable = '1' AND (data_in = X"BC" OR data_in = X"9C" OR
              data_in = X"AC" OR data_in = X"B4" OR data_in = X"B9" OR data_in = X"BE"
              OR data_in = X"BD") AND control_sig = '1') OR flag_errors(2) = '1') 
              AND mark_header = '1') then --Trailer Condition
         data_type := trailer;
      else
         data_type := wfd;
      end if;
      --Data Processing
      case data_type is

         when header  =>
            trailer_counter <= (others => '0'); --reset on valid data
            case data_reg(23 downto 16) is
               when x"E9" => --Vaild Header
                  link_data 	 <= "001" & "000" & channel_number & data_reg(15) & l1id_msbs & data_reg(14 downto 0); 
 					   lvinfo		 <= data_reg(4 downto 0);
                  link_valid   <= '1';
                  mark_header  <= '1';
               when "11101010" =>			-- Address Record AR found (0xEA)
 				   	link_valid	 <= '0';    -- don't want it
                  record_count <= record_count + 1;
 				   when "11101100" =>			-- Value Record VR found (0xEC)
 				   	link_valid	 <= '0';    -- don't want it
                  record_count <= record_count + 1;
 				   when "11101111" =>			-- Service Record SR found (0xEF)
 				   	link_valid	 <= '1';
                  record_count <= record_count + 1;
                  link_data	 <= "000" & "000" & channel_number & '0' & data_reg(15 downto 10) & "0000000" & data_reg(9 downto 0);
				      if(data_reg(15 downto 10) = "001110") then		-- service record code = 14 (7 MSBs of L1A counter)
				      	l1id_msbs <= '0' & data_reg(6 downto 0);
				      elsif(data_reg(15 downto 10) = "001111") then 	-- service record code = 15 (skipped trigger counter)
				      	skipped	 <= data_reg(9 downto 0);
				      end if;
               when others     => --Header Error
                  --Add xBAD to LSB to represent header Error, should have a flag in trailer
                  link_data      <= "001" & "000" & channel_number & X"000BAD";
                  --lvinfo not updated
                  flag_errors(5) <= '1';
                  link_valid     <= '1';
                  mark_header    <= '1';
               end case;

         when hit     =>
            trailer_counter <= (others => '0'); --reset on valid data
            case data_reg(23 downto 16) is
               when X"E9"      => --Extra/Late Header
                  link_valid   <= '0';
               when "11101010" =>			-- Address Record AR found (0xEA)
 				   	link_valid	 <= '0';    -- don't want it
                  record_count <= record_count + 1;
 				   when "11101100" =>			-- Value Record VR found (0xEC)
 				   	link_valid	 <= '0';    -- don't want it
                  record_count <= record_count + 1;
 				   when "11101111" =>			-- Service Record SR found (0xEF)
 				   	link_valid	 <= '1';
                  record_count <= record_count + 1;
                  link_data	 <= "000" & "000" & channel_number & '0' & data_reg(15 downto 10) & "0000000" & data_reg(9 downto 0);
				      if(data_reg(15 downto 10) = "001110") then		-- service record code = 14 (7 MSBs of L1A counter)
				      	l1id_msbs <= '0' & data_reg(6 downto 0);
				      elsif(data_reg(15 downto 10) = "001111") then 	-- service record code = 15 (skipped trigger counter)
				      	skipped	 <= data_reg(9 downto 0);
				      end if;
               when others     => --Actual Hits
                  if(flag_errors(1) = '1') then --HT limit reached
                     link_valid  <= '0'; --Drop incoming data, wait for trailer
                     --Col > 80 ; Row > 336
                  elsif(data_reg(23 downto 17) > "1010000" OR data_reg(16 downto 8) > "101010000") then
                     flag_errors(0) <= '1'; --Row/Col Error Flag
                     link_valid     <= '0'; --Drop it.
                  else --Valid Hit
                     if(ht_counter >= unsigned(header_trailer_limit)) then --HT Counter
                        flag_errors(1) <= '1';
                        link_valid     <= '0';
                     else
                        link_data  <= "100" & "000" & channel_number & data_reg(7 downto 0) & data_reg(23 downto 8);
 					         link_valid <= '1';
                        ht_counter <= ht_counter + 1;
                     end if;
                  end if;
            end case;

         when trailer =>
            link_data       <= "010" & "000" & channel_number & flag_errors & skipped & lvinfo;-- trailer
 			   link_valid 	    <= '1';
 			   lvinfo		    <= (others => '0');
            trailer_counter <= (others => '0');
            ht_counter      <= (others => '0');
            flag_errors     <= (others => '0');
            mark_header     <= '0'; --Reset after trailer written, look for next header
         
         when wfd     =>
            link_valid <= '0';
            --Trailer timeout
            if(mark_header = '1' AND trailer_counter >= unsigned(trailer_timeout-1)) then
               flag_errors(2) <= '1';
            else
               trailer_counter <= trailer_counter + 1;
            end if;

         when others => --Trouble state, reset
            link_valid 	    <= '0';
            mark_header     <= '0';
            trailer_counter <= x"00000000";
            ht_counter      <= x"00000000";
            record_count    <= x"00000000";
            link_data	    <= (others => '0');
            lvinfo          <= (others => '0');
            l1id_msbs       <= (others => '0');
            skipped         <= (others => '0');
            flag_errors     <= (others => '0');
      end case;
   end if;
 end process output_proc;

end behavioral;
