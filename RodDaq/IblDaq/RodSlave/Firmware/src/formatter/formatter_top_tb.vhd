--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   09:16:07 10/21/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/ibl_rod_fw/ibl_rodSlave/ibl_formatter/formatter_top_tb.vhd
-- Project Name:  ibl_formatter
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: formatter_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all; -- needed for +/- operations
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY formatter_top_tb IS
END formatter_top_tb;
 
ARCHITECTURE behavior OF formatter_top_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT formatter_top
    PORT(
         clk40 : IN  std_logic;
         clk80 : IN  std_logic;
         rst : IN  std_logic;
         hold_output : IN  std_logic;
         we_from_boc : IN  std_logic;
         data_from_boc : IN  std_logic_vector(7 downto 0);
         addr_from_boc : IN  std_logic_vector(1 downto 0);
         cntrl_from_boc : IN  std_logic;
         fmt_link_enabled : IN  std_logic_vector(3 downto 0);
         fmt_readout_timeout : IN  unsigned(31 downto 0);
         fmt_data_overflow_limit : IN  unsigned(15 downto 0);
         valid_to_efb : OUT  std_logic;
         data_to_efb : OUT  std_logic_vector(34 downto 0);
         boe : OUT  std_logic;
         eoe : OUT  std_logic;
         fifo_full : OUT  std_logic_vector(3 downto 0);
         mode_bits_in : IN  std_logic_vector(11 downto 0);
         modebits_fifo_wen_n_in : IN  std_logic;
         modebits_fifo_rst_n_in : IN  std_logic;
         modebits_fifo_ef_n_out : OUT  std_logic;
         modebits_fifo_ff_n_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal clk40 : std_logic := '0';
   signal clk80 : std_logic := '0';
   signal rst : std_logic := '0';
   signal hold_output : std_logic := '0';
   signal we_from_boc : std_logic := '0';
   signal data_from_boc : std_logic_vector(7 downto 0) := X"3C";
   signal addr_from_boc : std_logic_vector(1 downto 0) := "10"; -- chip 0
   signal cntrl_from_boc : std_logic := '0';
   signal fmt_link_enabled : std_logic_vector(3 downto 0) := "0000"; -- enabled for just chip 0
   signal fmt_readout_timeout : unsigned(31 downto 0) := x"0000FFFF";
   signal fmt_data_overflow_limit : unsigned(15 downto 0) := x"FFFF";
   signal mode_bits_in : std_logic_vector(11 downto 0) := (others => '0');
   signal modebits_fifo_wen_n_in : std_logic := '1';
   signal modebits_fifo_rst_n_in : std_logic := '0';

 	--Outputs
   signal valid_to_efb : std_logic;
   signal data_to_efb : std_logic_vector(34 downto 0);
   signal boe : std_logic;
   signal eoe : std_logic;
   signal fifo_full : std_logic_vector(3 downto 0);
   signal modebits_fifo_ef_n_out : std_logic;
   signal modebits_fifo_ff_n_out : std_logic;

   -- Clock period definitions
   constant clk40_period : time := 20 ns;
   constant clk80_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: formatter_top PORT MAP (
          clk40 => clk40,
          clk80 => clk80,
          rst => rst,
          hold_output => hold_output,
          we_from_boc => we_from_boc,
          data_from_boc => data_from_boc,
          addr_from_boc => addr_from_boc,
          cntrl_from_boc => cntrl_from_boc,
          fmt_link_enabled => fmt_link_enabled,
          fmt_readout_timeout => fmt_readout_timeout,
          fmt_data_overflow_limit => fmt_data_overflow_limit,
          valid_to_efb => valid_to_efb,
          data_to_efb => data_to_efb,
          boe => boe,
          eoe => eoe,
          fifo_full => fifo_full,
          mode_bits_in => mode_bits_in,
          modebits_fifo_wen_n_in => modebits_fifo_wen_n_in,
          modebits_fifo_rst_n_in => modebits_fifo_rst_n_in,
          modebits_fifo_ef_n_out => modebits_fifo_ef_n_out,
          modebits_fifo_ff_n_out => modebits_fifo_ff_n_out
        );

   -- Clock process definitions
   clk40_process :process
   begin
		clk40 <= '1';
		wait for clk40_period/2;
		clk40 <= '0';
		wait for clk40_period/2;
   end process;
 
   link_enable :process
   begin
		wait for 200 ns;
		fmt_link_enabled <= "0100";
		wait;
   end process; 
 
   clk80_process :process
   begin
		clk80 <= '1';
		wait for clk80_period/2;
		clk80 <= '0';
		wait for clk80_period/2;
   end process;
 
   reset_process :process
   begin
		rst <= '1';
		modebits_fifo_rst_n_in <= '0';
		wait for 65 ns;
		rst <= '0';
		modebits_fifo_rst_n_in <= '1';
		wait;
   end process;

   boc_to_fmt_we :process
   begin
	   we_from_boc <= '1';
		wait for 10 ns;
		we_from_boc <= '0';
		wait for 40 ns;
	end process;
	
	mode_bits_we :process
	begin
		wait for 105 ns;
		modebits_fifo_wen_n_in <= '0';
		wait for 30 ns;
		modebits_fifo_wen_n_in <= '1';
		wait for 30 ns;
		modebits_fifo_wen_n_in <= '0';
		wait for 30 ns;
		modebits_fifo_wen_n_in <= '1';
		wait;
	end process;
	
	boc_ctrl_in :process
	begin
	   cntrl_from_boc <= '1'; wait for 250 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 190 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns; 
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns; 
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait for 40 ns; cntrl_from_boc <= '0'; wait for 10 ns;
		cntrl_from_boc <= '1'; wait;
	end process;
	
   boc_data: process
   begin		
		wait for 150 ns;
		-- first header word is E9. EOE by BOC is marked as X"BC" (fmt use this to generate trailer)
      data_from_boc <= X"3C"; wait for 50 ns; data_from_boc <= X"FC"; wait for 50 ns;
      data_from_boc <= X"E9"; wait for 50 ns; data_from_boc <= X"6B"; wait for 50 ns;
      data_from_boc <= X"F8"; wait for 50 ns; data_from_boc <= X"BC"; wait for 50 ns; -- empty event only with header
      data_from_boc <= X"3C"; wait for 50 ns; data_from_boc <= X"FC"; wait for 50 ns;
      data_from_boc <= X"E9"; wait for 50 ns; data_from_boc <= X"8C"; wait for 50 ns;
      data_from_boc <= X"5F"; wait for 50 ns; data_from_boc <= X"22"; wait for 50 ns;
      data_from_boc <= X"DE"; wait for 50 ns; data_from_boc <= X"5F"; wait for 50 ns;
      data_from_boc <= X"2C"; wait for 50 ns; data_from_boc <= X"80"; wait for 50 ns;
      data_from_boc <= X"DF"; wait for 50 ns; data_from_boc <= X"9A"; wait for 50 ns;
      data_from_boc <= X"92"; wait for 50 ns; data_from_boc <= X"2F"; wait for 50 ns;
      data_from_boc <= X"7F"; wait for 50 ns; data_from_boc <= X"2B"; wait for 50 ns;
      data_from_boc <= X"DF"; wait for 50 ns; data_from_boc <= X"66"; wait for 50 ns;
      data_from_boc <= X"FE"; wait for 50 ns; data_from_boc <= X"5F"; wait for 50 ns;      
		data_from_boc <= X"62"; wait for 50 ns; data_from_boc <= X"5F"; wait for 50 ns;
      data_from_boc <= X"4F"; wait for 50 ns; data_from_boc <= X"BC"; wait for 50 ns;
      data_from_boc <= X"3C";
      wait;
   end process;

END;
