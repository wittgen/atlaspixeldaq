-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: mb_data_decoder.vhd
-- Description:
--
-------------------------------------------------------------------------------
-- Structure: 
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
-------------------------------------------------------------------------------
-- Author:         John Joseph
-- Board Engineer: 
-- History:
--  v13e : 01/28/2004 attached mb_fifo_ren_in directly to the FIFO block
--
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.all;  -- needed for logic operations
use IEEE.STD_LOGIC_arith.all; -- needed for +/- operations

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------
entity mb_data_decode is
  port(
    clk40            : in  STD_LOGIC; -- clk40 input
		clk80						 : in  STD_LOGIC; -- clk80 input	
    rst              : in  STD_LOGIC; -- asynchronous global reset
    mode_bits_in     : in  STD_LOGIC_VECTOR(11 downto 0);
    mb_fifo_wen_n_in : in  STD_LOGIC; -- mb fifos wr strobe: active low
    mb_fifo_ren_in   : in  STD_LOGIC; -- mb fifos rd strobe: active high
    mb_fifo_rst_in   : in  STD_LOGIC; -- modebit fifos reset strobe
    mode_bits_out    : out STD_LOGIC_VECTOR(23 downto 0);
    mb_ready_out     : out STD_LOGIC; 
    mb_fifo_ef_n_out : out STD_LOGIC; -- readout mode fifo is empty
    mb_fifo_ff_n_out : out STD_LOGIC  -- readout mode fifo is full 
    );
end mb_data_decode; 

architecture rtl of mb_data_decode is

-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

	signal mode_bits_i   : STD_LOGIC_VECTOR(11 downto 0);
	signal link_mb_i     : STD_LOGIC_VECTOR(23 downto 0);
	signal mb_fifo_wen_i : STD_LOGIC;
	signal mb_data_set_i : STD_LOGIC;
	signal mb_fifo_rst_i : STD_LOGIC;

	signal wr_mb_to_fifo_i : STD_LOGIC;
	signal mb_fifo_wen_dly : STD_LOGIC;

	signal fifo_ef_i : STD_LOGIC;
	signal fifo_ff_i : STD_LOGIC;

--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

	COMPONENT fifo_512x24
		PORT (
			rst : IN STD_LOGIC;
			wr_clk : IN STD_LOGIC;
			rd_clk : IN STD_LOGIC;
			din : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
			wr_en : IN STD_LOGIC;
			rd_en : IN STD_LOGIC;
			dout : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
			full : OUT STD_LOGIC;
			empty : OUT STD_LOGIC
		);
	END COMPONENT;

begin

--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------

	U1 : fifo_512x24
		PORT MAP (
			rst			=> rst,
			wr_clk 	=> clk40,
			rd_clk 	=> clk80,
			din 		=> link_mb_i,
			wr_en 	=> mb_fifo_wen_dly,
			rd_en 	=> mb_fifo_ren_in,
			dout 		=> mode_bits_out,
			full 		=> fifo_ff_i,
			empty 	=> fifo_ef_i
		);

--	U1 : fifo_512x24
--		PORT MAP (
--			clk 	=> clk,
--			rst 	=> rst,
--			din 	=> link_mb_i,
--			wr_en => mb_fifo_wen_dly,
--			rd_en => mb_fifo_ren_in,
--			dout 	=> mode_bits_out,
--			full 	=> fifo_ff_i,
--			empty => fifo_ef_i
--		);

--------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------
	signal_register : process (
		clk80,
		rst
		)
	begin
		if (rst = '1') then
			mode_bits_i   <= (others => '0');
			mb_fifo_wen_i    <= '0';
			mb_fifo_rst_i    <= '1';
			mb_fifo_ef_n_out <= '0';
			mb_fifo_ff_n_out <= '1';
		elsif (clk80'event AND clk80 = '1') then
			mode_bits_i      <=     mode_bits_in;
			mb_fifo_wen_i    <= NOT mb_fifo_wen_n_in;
			mb_fifo_rst_i    <= mb_fifo_rst_in;
			mb_fifo_ef_n_out <= NOT fifo_ef_i;
			mb_fifo_ff_n_out <= NOT fifo_ff_i;
		end if;
	end process;    

	mb_decode : process (
		rst,
		clk80,
		mode_bits_i, 
		mb_fifo_wen_i, 
		mb_fifo_rst_i
		)
	begin
		if (rst = '1') then
			link_mb_i(23 downto 0) <= (others => '0');
			mb_data_set_i   <= '0';
			wr_mb_to_fifo_i <= '0';
		elsif (clk80'event AND clk80 = '1') then
			wr_mb_to_fifo_i <= '0';
			if (mb_fifo_rst_i = '1') then
				link_mb_i(23 downto 0) <= (others => '0');
				mb_data_set_i   <= '0';
			elsif (mb_fifo_wen_i = '1') then
				if (mb_data_set_i = '0') then
					for j in 0 to 11 loop
						link_mb_i(2*j) <= mode_bits_i(j);
					end loop;
					mb_data_set_i <= NOT mb_data_set_i;
				elsif (mb_data_set_i = '1') then
					for j in 0 to 11 loop
						link_mb_i((2*j)+1) <= mode_bits_i(j);
					end loop;
					mb_data_set_i <= NOT mb_data_set_i;
					wr_mb_to_fifo_i <= '1';
				end if;
			end if;
		end if;    
	end process mb_decode;

	mb_strobe : process (
		clk40,
		rst,
		wr_mb_to_fifo_i
		)
	begin
		if (rst = '1') then
			mb_fifo_wen_dly <= '0';
			mb_ready_out    <= '0'; 
		elsif (clk40'event AND clk40 = '1') then
			mb_fifo_wen_dly <= wr_mb_to_fifo_i;
			mb_ready_out    <= mb_fifo_wen_dly;
		end if;
	end process mb_strobe;

end rtl;