----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:07:10 06/01/2012 
-- Design Name: 
-- Module Name:    histoUnit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - Added header export functionality
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_arith.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use std.textio.all;
use work.txt_util.all;

use work.rodHistoPack.all;

entity histoUnit is
	 generic (simulation : boolean := false; useExtRam: boolean := false);
    PORT(
			control : in  STD_LOGIC_VECTOR (31 downto 0);
         hitEnable : IN  std_logic;
			row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
			col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
			chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
			totVal : IN  std_logic_vector(totWidth - 1 downto 0);
         rfd : OUT  std_logic;
         mode : IN  std_logic_vector(readoutModeBits - 1 downto 0);
         rfr : IN  std_logic;
         roc : OUT  std_logic;
         histValid : OUT  std_logic;
         histo : OUT  std_logic_vector(31 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic;
         ramClk : IN  std_logic;
         ramAddr : OUT  std_logic_vector(addrBits - 1 downto 0);
         ramData : INOUT  std_logic_vector(histDataBits - 1 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
        );
end histoUnit;

architecture Behavioral of histoUnit is

file mappingFile: text;
signal dumpIt: integer := 1;

    COMPONENT rodHisto
	 generic (simulation : boolean := false; useExtRam: boolean := false);
    PORT(
		    control : in  STD_LOGIC_VECTOR (31 downto 0);
         hitValid : IN  std_logic;
         row : IN  std_logic_vector(8 downto 0);
         col : IN  std_logic_vector(6 downto 0);
         chip : IN  std_logic_vector(2 downto 0);
         totVal : IN  std_logic_vector(3 downto 0);
         rfd : OUT  std_logic;
         mode : IN  std_logic_vector(readoutModeBits - 1 downto 0);
         rfr : IN  std_logic;
         roc : OUT  std_logic;
         histValid : OUT  std_logic;
         histo : OUT  std_logic_vector(31 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic;
         ramClk : IN  std_logic;
         ramAddr : OUT  std_logic_vector(addrBits - 1 downto 0);
         ramData : INOUT  std_logic_vector(histDataBits - 1 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
        );
    END COMPONENT;

begin

   histogrammer: rodHisto 
		generic map(simulation => simulation, useExtRam => useExtRam)
		PORT MAP (
			 control => control,
          hitValid => hitEnable,
          row => row,
          col => col,
          chip => chip,
          totVal => totVal,
          rfd => rfd,
          mode => mode,
          rfr => rfr,
          roc => roc,
          histValid => histValid,
          histo => histo,
          rst => rst,
          clk => clk,
          ramClk => ramClk,
          ramAddr => ramAddr,
          ramData => ramData,
          ramCke_n => ramCke_n,
          ramCs_n => ramCs_n,
          ramBw_n => ramBw_n,
          ramWe_n => ramWe_n,
          ramOe_n => ramOe_n,
          ramAdv => ramAdv,
          ramLbo => ramLbo,
          ramZz => ramZz
        );
		  

  -- begin header generator ----------
	dump_vars : process(dumpIt)
   variable fline: line;
	begin
		  if dumpit = 1 then
        file_open(mappingFile,"../sw/rodHisto.hxx",WRITE_MODE);

        write(fline,string'(""));
		  write(fline,string'("// IBL ROD Spartan-6 histogrammer registers and bit definitions"));
        writeline(mappingFile,fline);
		  
        write(fline,string'("// This is a generated file, do not edit!"));
        writeline(mappingFile,fline);	  

        write(fline,string'("#ifndef ROD_HISTO_H"));
        writeline(mappingFile,fline);
        write(fline,string'("#define ROD_HISTO_H"));
        writeline(mappingFile,fline);

        write(fline,string'(""));
        writeline(mappingFile,fline);
		  
	-- TODO MK: FEI4 specs are certainly defined in some other header...
	write(fline,string'("#define ROW_SIZE "));
	write(fline, rowSize);
	writeline(mappingFile,fline);

	write(fline,string'("#define COL_SIZE "));
	write(fline, colSize);
	writeline(mappingFile,fline);		  

	write(fline,string'("#define CHIP_SIZE "));
	write(fline,string'("(COL_SIZE * ROW_SIZE)"));
	writeline(mappingFile,fline);
	  
	write(fline,string'(""));
	writeline(mappingFile,fline);		  
	write(fline,string'("// input format definitions for the histogramming unit"));
	writeline(mappingFile,fline);
	write(fline,string'("// for debugging purposes only"));
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define ROW_BITS "));
	write(fline, rowBits);
	writeline(mappingFile,fline);

	write(fline,string'("#define COL_BITS "));
	write(fline, colBits);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define CHIP_BITS "));
	write(fline, chipBits);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TOT_BITS "));
	write(fline, totBits);
	writeline(mappingFile,fline);
	  
	  
	write(fline,string'("#define ROW_SHIFT "));
	write(fline, rowShift);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define COL_SHIFT "));
	write(fline, colShift);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define CHIP_SHIFT "));
	write(fline, chipShift);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TOT_SHIFT "));
	write(fline, totShift);
	writeline(mappingFile,fline);

	  
	write(fline,string'(""));
	writeline(mappingFile,fline);		  
	write(fline,string'("// output format definitions for the histogramming unit"));
	writeline(mappingFile,fline);
	  
	  
	write(fline,string'("#define OCC_RESULT_BITS "));
	write(fline, occBits);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define OCC_ONLINE_RESULT_BITS "));
	write(fline, occOnlineBits);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define MISSING_TRIGGERS_RESULT_BITS "));
	write(fline, occMissingWidth);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TOT_RESULT_BITS "));
	write(fline, totRamWidth);
	writeline(mappingFile,fline);

	write(fline,string'("#define TOTSQR_RESULT_BITS "));
	write(fline, totSqrRamWidth);
	writeline(mappingFile,fline);		  

        -- the following is hardcoded and needs to change together with the splitHistoWord definition in histoTop.vhd

	write(fline,string'("#define ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT "));
	write(fline, occOffs);
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define ONEWORD_TOT_RESULT_SHIFT "));
	write(fline,string'("(ONEWORD_MISSING_TRIGGERS_RESULT_SHIFT + MISSING_TRIGGERS_RESULT_BITS)"));
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define ONEWORD_TOTSQR_RESULT_SHIFT "));
	write(fline,string'("(ONEWORD_TOT_RESULT_SHIFT + TOT_RESULT_BITS)"));
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TWOWORD_OCC_RESULT_SHIFT 0"));
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TWOWORD_TOT_RESULT_SHIFT 0"));
	writeline(mappingFile,fline);
	  
	write(fline,string'("#define TWOWORD_TOTSQR_RESULT_SHIFT 16"));
	writeline(mappingFile,fline);
	  

	write(fline,string'(""));
        writeline(mappingFile,fline);
	write(fline,string'("// histogrammer control register"));
        writeline(mappingFile,fline);

		  
  	write(fline,string'("#define CHIPCTL_BITS "));
        write(fline, chipCtlBits);
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ROWMASKCTL_BITS "));
        write(fline, rowMaskCtlBits);
        writeline(mappingFile,fline);		  
		  
	  
	-- new histogrammer needs to know mask step index, if mask stepping used
	write(fline,string'("#define MSTEPINDEXCTL_BITS "));
		write(fline, mstepIndexCtlBits);
		writeline(mappingFile,fline);

	write(fline,string'("#define OPMODECTL_BITS "));
        write(fline, opModeCtlBits);
        writeline(mappingFile,fline);		  
		  
	write(fline,string'("#define EXPECTEDOCCCTL_BITS "));
        write(fline, expectedOccCtlBits);
        writeline(mappingFile,fline);		  
		  
	write(fline,string'("#define CHIPCTL_SHIFT "));
        write(fline, chipCtlShift);
        writeline(mappingFile,fline);

	write(fline,string'("#define ROWMASKCTL_SHIFT "));
        write(fline, rowMaskCtlShift);
        writeline(mappingFile,fline);

	write(fline,string'("#define OPMODECTL_SHIFT "));
        write(fline, opModeCtlShift);
        writeline(mappingFile,fline);

	-- new histogrammer needs to know mask step index, if mask stepping used
	write(fline,string'("#define MSTEPINDEX_SHIFT_ODD "));
		write(fline, mstepIndexCtlShiftOdd);
		writeline(mappingFile,fline);

	write(fline,string'("#define MSTEPINDEX_SHIFT_EVEN "));
		write(fline, mstepIndexCtlShiftEven);
		writeline(mappingFile,fline);

	write(fline,string'("#define EXPECTEDOCCCTL_SHIFT "));
        write(fline, expectedOccCtlShift);
        writeline(mappingFile,fline);

	write(fline,string'(""));
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ONEFE_CHIPCTL "));
        write(fline, "(" & str(conv_integer(chipCtlNull)) & " << CHIPCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ALLFE_CHIPCTL "));
        write(fline, "(" & str(conv_integer(chipCtlOne)) & " << CHIPCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ROWMASK_NONE "));
        write(fline, "(" & str(conv_integer(rowMaskNone)) & " << ROWMASKCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ROWMASK_2 "));
        write(fline, "(" & str(conv_integer(rowMask2)) & " << ROWMASKCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ROWMASK_4 "));
        write(fline, "(" & str(conv_integer(rowMask4)) & " << ROWMASKCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define ROWMASK_8 "));
        write(fline, "(" & str(conv_integer(rowMask8)) & " << ROWMASKCTL_SHIFT)");
        writeline(mappingFile,fline);
		  
        write(fline,string'(""));
        writeline(mappingFile,fline);
	
	write(fline,string'("#define OPMODE_ONLINE_OCCUPANCY "));
        write(fline, "(" & str(conv_integer(opOnlineOccupancy)) & " << OPMODECTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define OPMODE_OFFLINE_OCCUPANCY "));
        write(fline, "(" & str(conv_integer(opOfflineOccupancy)) & " << OPMODECTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define OPMODE_SHORT_TOT "));
        write(fline, "(" & str(conv_integer(opShortTot)) & " << OPMODECTL_SHIFT)");
        writeline(mappingFile,fline);
		  
	write(fline,string'("#define OPMODE_LONG_TOT "));
        write(fline, "(" & str(conv_integer(opLongTot)) & " << OPMODECTL_SHIFT)");
        writeline(mappingFile,fline);


	-- new histogrammer needs to know mask step index, if mask stepping used
	write(fline,string'("// use MSTEPINDEX_IGNORE to force acception of ALL pixels"));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_IGNORE "));
		write(fline, str(conv_integer(mstepIndexCtlIgnore))); 
		writeline(mappingFile,fline);

	write(fline,string'("// use MSTEPINDEX_STEP1..8 to accept only pixels of a certain step pattern"));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP1 "));
		write(fline, str(conv_integer(mstepIndexCtlStep1)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP2 "));
		write(fline, str(conv_integer(mstepIndexCtlStep2)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP3 "));
		write(fline, str(conv_integer(mstepIndexCtlStep3)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP4 "));
		write(fline, str(conv_integer(mstepIndexCtlStep4)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP5 "));
		write(fline, str(conv_integer(mstepIndexCtlStep5)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP6 "));
		write(fline, str(conv_integer(mstepIndexCtlStep6)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP7 "));
		write(fline, str(conv_integer(mstepIndexCtlStep7)));
		writeline(mappingFile,fline);
	write(fline,string'("#define MSTEPINDEX_STEP8 "));
		write(fline, str(conv_integer(mstepIndexCtlStep8)));
		writeline(mappingFile,fline);
		  

        write(fline,string'(""));
        writeline(mappingFile,fline);
	
        write(fline,string'("#endif //ROD_HISTO_H"));
        writeline(mappingFile,fline);
        file_close(mappingFile);

	dumpIt <= 0;
	end if;
	end process;		  


end Behavioral;

