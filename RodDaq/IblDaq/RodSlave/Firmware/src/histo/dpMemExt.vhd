----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:31:57 02/25/2010 
-- Design Name: 
-- Module Name:    dpMem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.rodHistoPack.all;

entity dpMemExt is
	Generic (useExtRam: boolean := false);
    Port ( clk : in  STD_LOGIC;
			  -- external memory ports
			  ramClk: in std_logic; -- doubled clock
			  ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
			  ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
			  ramCke_n: out std_logic; -- low for normal operation
			  ramCs_n: out std_logic;
			  ramBw_n: out std_logic_vector(3 downto 0); -- combine with we_n
			  ramWe_n: out std_logic;
			  ramOe_n: out std_logic;
			  ramAdv : out std_logic; -- low for normal operation (direct address mode)
			  ramLbo: out std_logic; -- mode input. low for normal operation (linear burst)
			  ramZz: out std_logic;  -- low for operation
			  -- histogrammer ports
           rdAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrData : in  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           wrEn : in  STD_LOGIC;
           rdData : out  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           rdEn : in  STD_LOGIC;
           rdAck : out  STD_LOGIC);
end dpMemExt;

architecture Behavioral of dpMemExt is

COMPONENT dpMem
PORT(
	clk : IN  std_logic;
	rdAddr : IN  std_logic_vector(addrBits - 1 downto 0);
	wrAddr : IN  std_logic_vector(addrBits - 1 downto 0);
	wrData : IN  std_logic_vector(histDataBits - 1 downto 0);
	wrEn : IN  std_logic;
	rdData : OUT  std_logic_vector(histDataBits - 1 downto 0);
	rdEn : IN  std_logic;
	rdAck : OUT  std_logic
  );
END COMPONENT;


	COMPONENT ssram_if
	PORT(
			  clk : in  STD_LOGIC;
			  -- external memory ports
			  ramClk: in std_logic; 
			  ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
			  ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
			  ramCke_n: out std_logic; -- low for normal operation
			  ramCs_n: out std_logic;
			  ramBw_n: out std_logic_vector(3 downto 0); -- combine with we_n
			  ramWe_n: out std_logic;
			  ramOe_n: out std_logic;
			  ramAdv : out std_logic; -- low for normal operation (direct address mode)
			  ramLbo: out std_logic; -- mode input. low for normal operation (linear burst)
			  ramZz: out std_logic;  -- low for operation
			  -- histogrammer ports
           rdAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrData : in  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           wrEn : in  STD_LOGIC;
           rdData : out  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           rdEn : in  STD_LOGIC;
           rdAck : out  STD_LOGIC
		);
	END COMPONENT;
	
	
component pipeline_sig
	Generic (stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC;
           dout : out  STD_LOGIC
			  );
end component;


signal writeRam: std_logic_vector(0 downto 0);

signal toggle, phase: std_logic := '0';
signal ramCs, ramWe, ramOe: std_logic;
signal ramData_i: STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
signal ramData_r: STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
signal ramWe_r: std_logic;

begin

--internal logic cannot be used --hujun
	internal: if not useExtRam generate
   intMem: dpMem 
		PORT MAP (
          clk => clk,
          rdAddr => rdAddr,
          wrAddr => wrAddr,
          wrData => wrData,
          wrEn => wrEn,
          rdData => rdData,
          rdEn => rdEn,
          rdAck => rdAck
        );
	-- fix external ram signals
	  ramAddr <= (others => '0');
	  ramData <= (others => 'Z');
	  ramCke_n <= '1';
	  ramCs_n <= '1';
	  ramBw_n <= (others => '1');
	  ramWe_n <= '1';
	  ramOe_n <= '1';
	  ramAdv <= '0';
	  ramLbo <= '0';
	  ramZz <= '1';
	end generate;

	external: if useExtRam generate
	extMem: ssram_if
   Port map( clk => clk,
			  -- external memory ports
			  ramClk => ramClk,
			  ramAddr => ramAddr,
			  ramData => ramData,
			  ramCke_n => ramCke_n,
			  ramCs_n => ramCs_n,
			  ramBw_n => ramBw_n,
			  ramWe_n => ramWe_n,
			  ramOe_n => ramOe_n,
			  ramAdv  => ramAdv,
			  ramLbo => ramLbo,
			  ramZz => ramZz,
			  -- histogrammer ports
          rdAddr => rdAddr,
          wrAddr => wrAddr,
          wrData => wrData,
          wrEn => wrEn,
          rdData => rdData,
          rdEn => rdEn,
          rdAck => rdAck
 			  );
	end generate;		
	
end Behavioral;
