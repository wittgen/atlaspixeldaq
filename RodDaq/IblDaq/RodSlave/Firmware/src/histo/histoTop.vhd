----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    19:17:36 02/24/2010 
-- Design Name: 
-- Module Name:    histoTop - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

use work.rodHistoPack.all;

entity histoTop is
	 generic (simulation : boolean := false; useExtRam: boolean := false);
    Port (
		  control : in  STD_LOGIC_VECTOR (31 downto 0);
		  clk : in  STD_LOGIC;
		  rst : in  STD_LOGIC;
		  enable : in  STD_LOGIC;
		  pixAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		  totVal : in  STD_LOGIC_VECTOR (totWidth - 1 downto 0);
		  mode : in  STD_LOGIC_VECTOR(readoutModeBits - 1 downto 0); -- sample, wait, readout
		  addrOut : out  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		  histo: out std_logic_vector(dmaWidth - 1 downto 0);
		  rfr : in  STD_LOGIC;	-- ready for readout
		  rfd : out  STD_LOGIC; -- ready for data
		  roc : out  STD_LOGIC; -- read outcompleted
		  valid: out std_logic; -- readout valid
		  -- external memory ports
		  ramClk: in std_logic; -- doubled clock
		  ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		  ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
		  ramCke_n: out std_logic; -- low for normal operation
		  ramCs_n: out std_logic;
		  ramBw_n: out std_logic_vector(3 downto 0); -- combine with we_n
		  ramWe_n: out std_logic;
		  ramOe_n: out std_logic;
		  ramAdv : out std_logic; -- low for normal operation (direct address mode)
		  ramLbo: out std_logic; -- mode input. low for normal operation (linear burst)
		  ramZz: out std_logic  -- low for operation
		  );
end histoTop;

architecture Behavioral of histoTop is

type histoStateType is (RESET, INIT, SAMPLE, PREPARE, READOUT_WAIT, READOUT, CLEAN, IDLE);
signal histoState: histoStateType;

component occSel
port(
	rst : IN  std_logic;
	clk : IN  std_logic;
	valid : IN  std_logic;
	datain : IN  std_logic_vector(occWidth - 1 downto 0);
	dataout : OUT  std_logic_vector(dmaWidth - 1 downto 0);
	complete : OUT  std_logic
  );
end component;
	 
component histoDmaFifo
  Port (
    wr_clk : IN STD_LOGIC;
	 rd_clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(dmaWidth downto 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(dmaWidth downto 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
	 rd_data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0);
    wr_data_count : OUT STD_LOGIC_VECTOR(10 DOWNTO 0)
  );
end component;

component pipeline_sig
	Generic (stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC;
           dout : out  STD_LOGIC
			  );
end component;


component pipeline_vec
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0);
           dout : out  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0)
			  );
end component;

component pipeline_uns
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  unsigned (pipeWidth - 1 downto 0);
           dout : out  unsigned (pipeWidth - 1 downto 0)
			  );
end component;

component dpMemExt
	Generic (useExtRam: boolean := false);
    Port ( 
		clk : in  STD_LOGIC;
	  -- external memory ports
	  ramClk: in std_logic; -- doubled clock
	  ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
	  ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
	  ramCke_n: out std_logic; -- low for normal operation
	  ramCs_n: out std_logic;
	  ramBw_n: out std_logic_vector(3 downto 0); -- combine with we_n
	  ramWe_n: out std_logic;
	  ramOe_n: out std_logic;
	  ramAdv : out std_logic; -- low for normal operation (direct address mode)
	  ramLbo: out std_logic; -- mode input. low for normal operation (linear burst)
	  ramZz: out std_logic;  -- low for operation
	  --
		rdAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		wrAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		wrData : in  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
		wrEn : in  STD_LOGIC;
		rdData : out  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
      rdEn : in  STD_LOGIC;
		rdAck : out  STD_LOGIC
		);
end component;

component Adder24b
	port (
	a: IN std_logic_VECTOR(23 downto 0);
	b: IN std_logic_VECTOR(23 downto 0);
	clk: IN std_logic;
	ce: IN std_logic;
	sclr: IN std_logic;
	s: OUT std_logic_VECTOR(23 downto 0));
end component;

component mult16
	port (
	clk: IN std_logic;
	a: IN std_logic_VECTOR(15 downto 0);
	b: IN std_logic_VECTOR(15 downto 0);
	sclr: IN std_logic;
	p: OUT std_logic_VECTOR(31 downto 0));
end component;

 component icon
  PORT (
		 CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
 end component;
	
 component ila
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TRIG1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TRIG2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	 TRIG3 : IN STD_LOGIC_VECTOR(47 DOWNTO 0)
	 );
 end component;
 	signal CONTROL0: STD_LOGIC_VECTOR(35 DOWNTO 0);
	signal TRIG0: STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal TRIG1: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG2: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG3: STD_LOGIC_VECTOR(47 DOWNTO 0);
	
	
	
signal ramWr, ramWrHist: std_logic;
signal ramRd: std_logic;

signal thrNew: std_logic_vector(23 downto 0);
signal thrOld: std_logic_vector(23 downto 0);

signal tot: std_logic_vector(23 downto 0);
signal totD: std_logic_vector(23 downto 0);
signal totNew: std_logic_vector(23 downto 0);
signal totOld: std_logic_vector(23 downto 0);

signal totM: std_logic_vector(15 downto 0);
signal totSqr: std_logic_vector(31 downto 0);
signal totSqrD: std_logic_vector(23 downto 0);
signal totSqrNew: std_logic_vector(23 downto 0);
signal totSqrOld: std_logic_vector(23 downto 0);

signal readAddr: std_logic_vector(addrBits - 1 downto 0);
signal writeAddr, writeAddrHist: std_logic_vector(addrBits - 1 downto 0);

signal ramDataIn: std_logic_vector(histDataBits - 1 downto 0);
signal ramDataInHist: std_logic_vector(histDataBits - 1 downto 0);
signal ramDataOut: std_logic_vector(histDataBits - 1 downto 0);
signal ramDataOutPipe: std_logic_vector(histDataBits - 1 downto 0);

signal ramCleanAddr: std_logic_vector(addrBits - 1 downto 0);
signal ramClean: std_logic;

signal ramValid: std_logic;
signal ramValidPipe: std_logic;

signal fifoWrEn: std_logic;
signal fifoRdEn: std_logic;
signal fifoFull: std_logic;
signal fifoEmpty: std_logic;
signal fifoData: std_logic_vector(dmaWidth - 1 downto 0);

signal readcount:     std_logic_vector(10 downto 0);
signal writecount:    std_logic_vector(10 downto 0);

signal ramValidPipe1: std_logic;

-- output signals from the occupancy component
signal occDataValid: std_logic;
signal occData: std_logic_vector((4 * occWidth) - 1 downto 0);

constant thrInc: std_logic_vector(23 downto 0) := x"000001";
constant clkEn: std_logic := '1';

-- need to compute maxaddr here
constant maxAddr: integer := (getChipSize(simulation) * getNumOfChips(simulation)) - 1;
constant maxlocalAddr: integer := maxAddr + 3;

signal localAddr: integer range 0 to maxlocalAddr;
signal hiAddr: integer range 0 to maxlocalAddr := maxlocalAddr;
signal wait2cyc: std_logic;--hujun

--signal localAddr: integer range 0 to maxAddr;
--signal hiAddr: integer range 0 to maxAddr := maxAddr;

 constant ramDoutDelay: integer := 1; -- can be 0 for small rams
--constant ramDoutDelay: integer := 2; -- larger for zbt

constant wrDelay: integer := 2 + ramDoutDelay;
--constant wrDelay: integer := 1 + ramDoutDelay; --3 by hujun
--constant totDelay: integer := wrDelay - 1;
constant totDelay: integer := wrDelay + 2;--5 by hujun
constant totSqrDelay: integer := totDelay - 1;
constant cleanDelay: integer := 3;

signal fifoHistoPipe1 : std_logic_vector(histDataBits downto 0);
signal roc1: std_logic;
signal rocpipe: std_logic;--hujun
signal expectedTriggers: integer range 0 to (2**occWidth - 1);
signal missingTriggers, missingTriggersPipe: std_logic_vector(occMissingWidth - 1 downto 0);

-- select occupancy mode 8 bit vs. 24 bit
signal muxOccModeSel: std_logic_vector(occSizeBits - 1 downto 0);
signal histoOpMode: std_logic_vector(opModeCtlShift + opModeCtlBits - 1 downto opModeCtlShift);
signal readoutType: std_logic;

-- we need to delay the readout flag to correctly align with dmafifo write enable
signal twoWordReadoutFlag: std_logic;
signal twoWordReadoutFlagD: std_logic;
constant twoWordDelay: integer := 6; 

signal splitHistoWord: std_logic_vector(dmaWidth - 1 downto 0);

-- external memory
--signal ramClk : std_logic;
--signal ramData : std_logic_vector(7 downto 0);
--signal ramAddr : std_logic_vector(7 downto 0);
--signal ramCke_n : std_logic;
--signal ramCs_n : std_logic;
--signal ramBw_n : std_logic_vector(3 downto 0);
--signal ramWe_n : std_logic;
--signal ramOe_n : std_logic;
--signal ramAdv : std_logic;
--signal ramLbo : std_logic;
--signal ramZz : std_logic;
--signal rdData : std_logic_vector(7 downto 0);
--signal rdAck : std_logic;
signal trig_addrout : STD_LOGIC_VECTOR (addrBits - 1 downto 0);
signal trig_addrin : STD_LOGIC_VECTOR (addrBits - 1 downto 0);
signal trig_en : std_logic;
signal histo_r: std_logic_vector(dmaWidth - 1 downto 0);
signal histo_dly: std_logic_vector(dmaWidth - 1 downto 0);
signal valid_dly:std_logic;

begin

-- control states	
addrCnt: process
	begin
		wait until rising_edge(ramclk);
		-- AKU: keep address reset also in readout wait
		if histoState = RESET or histoState = SAMPLE or histoState = PREPARE or  histoState = READOUT_WAIT then
			localAddr <= 0;
		elsif localAddr = hiAddr + 2 then
								localAddr <= 0;
		elsif localAddr < hiAddr + 2 then 
         -- TODO MK: too many nested if-levels?
			if histoState = INIT or histoState = CLEAN then
					localAddr <= localAddr + 1;
			elsif rfr = '1' then 
					 if readoutType = readoutTwoWords then
						  if localAddr <= hiAddr then
								if twoWordReadOutFlag = '1' then
										localAddr <= localAddr + 1;
								else
										localAddr <= localAddr;
								end if;
							else
								localAddr <= localAddr + 1;
							end if;
					 else
								localAddr <= localAddr + 1;
					end if;
			end if;
		end if;
	end process;

----------------------------------------------------------------------------------
-- The main state machine of the histogrammer. It uses the following states:
-- RESET: 
--	Initial state after slave FPGA reset. Set default values of some registers.
-- INIT: 
--	Initialize the histogrammer memory (write zeroes)
-- IDLE: 
--	Accept settings from the histogrammer control register (mask stepping, etc.)
--	Wait for mode bit to go to modeSample and then move to SAMPLE.
-- SAMPLE: 
--	Accept incoming hit data and do the necessary calculations for ToT/ToT².
--	Stop sampling when mode bit changes to modeReadout.
--	Flow is: read old values for occupancy/sum(ToT)/sum(ToT²) from RAM, add the
--	calculated values and write back the new values to the RAM.
-- PREPARE: 
--      Transition state to clear pipeline.
-- READOUT_WAIT:
--      Wait state before actual readout, as the two histogrammers cannot be read
--      out in parallel.
-- READOUT: 
--      The memory is read out according to the histogrammer configuation,
--	taking into account mask stepping and mode of operation.
--	Together with the last data word the ROC signal is asserted (this happens in
--	the CLEAN state).
-- CLEAN: Transition state to finish cleaning the RAM and readout.
----------------------------------------------------------------------------------
histoStates: process(ramclk, rst)
	begin
		if rst = '1' then 
			histoState <= RESET;
			hiAddr <= maxAddr;
			twoWordReadoutFlag <= '0';

		elsif rising_edge(ramclk) then
		
			case histoState is 
			when RESET =>
				histoState <= INIT;
                                readoutType <= readoutOneWord;
                                muxOccModeSel <= occSizeShort;
                                histoOpMode <= opShortToT;
                                expectedTriggers <= 0;
				
			when INIT => 
--				if localAddr = maxAddr then
				if localAddr = hiAddr + 2 then  --delay 2 cycle --hujun					
					histoState <= IDLE;
				end if;
				
			when IDLE =>
				-- get settings from control register
			  case control(opModeCtlShift + opModeCtlBits - 1 downto opModeCtlShift) is
					when opOnlineOccupancy =>
						 muxOccModeSel <= occSizeLong;
						 readoutType <= readoutOneWord;

					when opOfflineOccupancy =>
						 muxOccModeSel <= occSizeShort;
						 readoutType <= readoutOneWord;

					when opShortToT =>
						 muxOccModeSel <= occSizeShort;
						 readoutType <= readoutOneWord;

					when opLongTot =>
						 muxOccModeSel <= occSizeShort;
						 readoutType <= readoutTwoWords;

					when others => 
						 muxOccModeSel <= occSizeShort;
						 readoutType <= readoutOneWord;
			  end case;
			  
			  histoOpMode <= control(opModeCtlShift + opModeCtlBits - 1 downto opModeCtlShift);

			  expectedTriggers <= conv_integer(unsigned(control(expectedOccCtlShift + expectedOccCtlBits - 1 downto expectedOccCtlShift))); 
			  hiAddr <= getHiAddr(control);

				if mode = modeSample then
					histoState <= SAMPLE;
				end if;

			when SAMPLE =>
				if mode = modeReadoutWait then
					histoState <= PREPARE;
					twoWordReadoutFlag <= '0';
				elsif mode = modeIdle then
					histoState <= RESET; -- reset the histogrammer 
				end if;

			when PREPARE =>
				if ramValidPipe = '0' then
					histoState <= READOUT_WAIT;
				end if;
				
                        when READOUT_WAIT =>
				if mode = modeSample then
					histoState <= RESET; -- reset the histogrammer when mode change comes during wait phase
				elsif mode = modeIdle then
					histoState <= RESET; -- reset the histogrammer 
                                elsif mode = modeReadOut then
                                        histoState <= READOUT;
                                end if;

			when READOUT =>
				if mode = modeSample then
					histoState <= RESET; -- reset the histogrammer when mode change comes during readout phase
				elsif mode = modeIdle then
					histoState <= RESET; -- reset the histogrammer 
				elsif localAddr = hiAddr + 2 then
					-- AKU 20140221: transition to clean made immediate in both modes according to Riccardo's suggestion
					histoState <= CLEAN;
					twoWordReadoutFlag <= not(twoWordReadoutFlag);							
					-- previous code
--					if readoutType = readoutTwoWords then
--						if twoWordReadoutFlag = '1' then
--							histoState <= CLEAN;
--						else
--							twoWordReadoutFlag <= not(twoWordReadoutFlag);							
--						end if;
--					else
--						histoState <= CLEAN;
--					end if;
					-- end previous
				elsif (rfr = '1') then 
							twoWordReadoutFlag <= not(twoWordReadoutFlag);
				end if;

			when CLEAN =>
				-- TODO: do we want to clean until hiAddr or maxAddr?
				-- would need to modify the scheme for this (not just change the value below!)
				
				if localAddr = hiAddr + 2 then --by hujun
						twoWordReadoutFlag <= not(twoWordReadoutFlag);
						histoState <= IDLE;
				elsif (rfr = '1') then 
						twoWordReadoutFlag <= not(twoWordReadoutFlag);
				end if;
				
			end case;
		end if;
	end process;

-- control outputs
	rfd <= '1' when histoState = SAMPLE else '1' when histoState = PREPARE else '0';
--	roc1 <= '1' when histoState = IDLE else '0';
	roc1 <= '1' when histoState = CLEAN else '0';
	addrOut <= readAddr;		
	--hujun
	wait2cyc <= '1' when localAddr > hiAddr else '0';
	
-- multiplex control and histogram signals to memory
	with histoState select
		readAddr 	<= pixAddr when SAMPLE, conv_std_logic_vector(localAddr, addrBits) when others;
	with histoState select
		writeAddr 	<= writeAddrHist when SAMPLE,
		--ramCleanADDR when READOUT, ramCleanADDR when CLEAN, --by hujun
				conv_std_logic_vector(localAddr, addrBits) when others;
	with histoState select
		-- ramRd		<= enable when SAMPLE, '1' when READOUT, '0' when others;
		ramRd		<= enable when SAMPLE, rfr  and not wait2cyc when READOUT, '0' when others;
	with histoState select
		--ramWr	<= ramWrHist when SAMPLE, '1' when INIT, ramClean when READOUT, ramClean when CLEAN, '0' when others;
		ramWr	<= ramWrHist when SAMPLE, not wait2cyc  when INIT,  not wait2cyc  when CLEAN, '0' when others;
	with histoState select
		ramDataIn	<= ramDataInHist when SAMPLE ,ramDataInHist when PREPARE, (others => '0')  when others;
	with histoState select
		ramValidPipe1 <= ramValidPipe when READOUT, ramValidPipe when CLEAN, '0' when others; -- keep valid until end

	-- signals for adder and multiplier
	tot <= gndBits(tot'length - 1 downto totWidth) & totVal;
	totM <= gndBits(totM'length - 1 downto totWidth) & totVal;

-- delay address 
addrPipe: pipeline_vec 
	generic map(pipeWidth => addrBits, stages => wrDelay)
	port map (clk => ramclk, din => readAddr, dout => writeAddrHist);

-- delay enable 
enablePipe: pipeline_sig
	generic map(stages => wrDelay)
	port map (clk => ramclk, din => enable, dout => ramWrHist);

-- delay tot 
totPipe: pipeline_vec
	generic map(pipeWidth => 24, stages => totDelay)
	port map (clk => ramclk, din => tot, dout => totD);

-- delay totSqr
totSqrPipe: pipeline_vec
	generic map(pipeWidth => 24, stages => totSqrDelay)
	port map (clk => ramclk, din => totSqr(23 downto 0), dout => totSqrD);

-- pipe twoWordFlag
twoWordPipe: pipeline_sig
	generic map(stages => twoWordDelay)
	port map (clk => ramclk, din => twoWordReadoutFlag, dout => twoWordReadoutFlagD);

-- delayed write and data during readout
cleanWr: pipeline_sig
	generic map(stages => cleanDelay)
	port map (clk => ramclk, din => ramRd, dout => ramClean);
	
cleanAddr: pipeline_vec
	generic map(pipeWidth => addrBits, stages => cleanDelay)
	port map (clk => ramclk, din => readAddr, dout => ramCleanAddr);

rocp: pipeline_sig
	generic map(stages => cleanDelay)
	port map (clk => ramclk, din => roc1, dout => rocpipe);




-- delay thr by 1  not used any more
--thrPipe: pipeline_vec
--	generic map(pipeWidth => occBits, stages => 1)
--	port map (clk => clk, din => thrNew(occBits - 1 downto 0), dout => thrNew_d(occBits - 1 downto 0));

-- map ram input
	process (totSqrNew, totNew, thrNew, muxOccModeSel)
	begin
	  if muxOccModeSel = occSizeShort then
			ramDataInHist <= totSqrNew(totSqrRamWidth - 1 downto 0) & totNew(totRamWidth - 1 downto 0) & thrNew(occBits - 1 downto 0);
	  else
		 ramDataInHist(occOnlineBits - 1 downto 0) <= thrNew(occOnlineBits - 1 downto 0);
		 ramDataInHist(ramDataInHist'length - 1 downto occOnlineBits) <= (others => '0');
	  end if;
	end process;

-- pipeline ram output
ramDoutPipe: pipeline_vec
	generic map(pipeWidth => histDataBits, stages => ramDoutDelay)
	port map (clk => ramclk, din => ramDataOut, dout => ramDataOutPipe);

ramValPipe: pipeline_sig
	generic map(stages => ramDoutDelay)
	port map (clk => ramclk, din => ramValid, dout => ramValidPipe);

	totOld <= gndBits(totOld'length - 1 downto totRamWidth) & ramDataOutPipe(totRamOffs + totRamWidth - 1 downto totRamOffs);
	totSqrOld <= gndBits(totSqrOld'length - 1 downto totSqrRamWidth) & ramDataOutPipe(totSqrRamOffs + totSqrRamWidth - 1 downto totSqrRamOffs);

-- thr is always output, size depends on operation mode
	with muxOccModeSel select
            thrOld <= gndBits(thrOld'length - 1 downto occBits) & ramDataOutPipe(occBits - 1 downto 0) when occSizeShort,
            ramDataOutPipe(occOnlineBits - 1 downto 0) when others;

valid <= (not fifoEmpty) and rfr; --TODO: correct or only not fifoEmpty?
fifoRdEn <= (not fifoEmpty) and rfr;

-- split 36 bit wide histogram data into two 32 bit words
	with twoWordReadoutFlagD select
		splitHistoWord <= gndBits(splitHistoWord'length - 1 downto occBits) & fifoHistoPipe1(occBits - 1 + 1 downto 0 + 1) when '0',
		fifoHistoPipe1(totSqrRamOffs + totSqrRamWidth - 1 + 1 downto totSqrRamOffs + 1) & gndBits((16 - totRamWidth) - 1 downto 0) & fifoHistoPipe1(totRamOffs + totRamWidth - 1 + 1 downto totRamOffs + 1) when others;
		
-- calculate difference between counted and expected triggers
procOccDifference: process(ramclk, rst)
variable occDifference: integer range 0 to (2**occWidth - 1);
begin
    if rst = '1' then 
        missingTriggers <= (others => '0');
    elsif rising_edge(ramclk) then
        occDifference := expectedTriggers - conv_integer(unsigned(ramDataOut(occWidth + occOffs - 1 downto occOffs)));
        if occDifference > (2**occMissingWidth - 1) then
            missingTriggers <= std_logic_vector(conv_unsigned(2**occMissingWidth - 1, missingTriggers'length));
        else
            missingTriggers <= std_logic_vector(conv_unsigned(occDifference, missingTriggers'length));
        end if;
    end if;
end process;


-- delay the missingTriggers signal
missingTriggersPipeline: pipeline_vec
	generic map(pipeWidth => missingTriggers'length, stages => ramDoutDelay)
	port map(clk => ramclk, din => missingTriggers, dout => missingTriggersPipe);

-- delay the valid/histo/fifoWrEn signals by one cycle to correctly align the roc/last signal
fifoHistoPipe: pipeline_vec
	generic map(pipeWidth => (histDataBits + 1), stages => 1)
	port map (
			clk => ramclk, 
			din(histDataBits downto 1) => ramDataOutPipe, 
			din(0) => ramValidPipe1,
			dout => fifoHistoPipe1);

-- FIFO that holds the aligned data and ROC signals for DMA transfer
dmaFifo: histoDmaFifo
  port map (
    wr_clk => ramclk,
	 rd_clk => clk,
    rst => rst,
    din(dmaWidth downto 1) => fifoData,
    din(0) => rocpipe,--hujun
    wr_en => fifoWrEn,
    rd_en => fifoRdEn,
    dout(dmaWidth downto 1) => histo_r,
    dout(0) => roc,
    full => fifoFull,
    empty => fifoEmpty,
	 rd_data_count => readcount,
    wr_data_count => writecount
	 );
	 
histo <= histo_r;

occSelect: occSel
port map (
	rst => rst,
	clk => ramclk,
	valid => fifoHistoPipe1(0),
	datain => fifoHistoPipe1(occWidth downto 1),
	dataout => occData,
	complete => occDataValid);

-- mux that provides four sources for the final output to the readout fifo
readoutMux: process (histoOpMode, occData, occDataValid, fifoHistoPipe1, splitHistoWord, missingTriggersPipe)
begin
	case histoOpMode is 
		when opOnlineOccupancy =>
		fifoWrEn <= fifoHistoPipe1(0);
		fifoData <= fifoHistoPipe1(dmaWidth downto 1);

		when opOfflineOccupancy =>
		fifoWrEn <= occDataValid;
		fifoData <= occData;

		when opShortTot =>
		fifoWrEn <= fifoHistoPipe1(0);
		fifoData <= fifoHistoPipe1(totSqrRamOffs + totSqrRamWidth + 1 - 1 downto totRamOffs + 1) & missingTriggersPipe; -- TODO MK: is missing triggers correctly aligned?

		when opLongTot =>
		fifoWrEn <= fifoHistoPipe1(0); -- TODO MK: do we need a separate valid signal?
		fifoData <= splitHistoWord;
		
		-- should not be reached
		when others =>
		null;
	end case;
end process;

-- instantiate the memory and adders/multiplicator
pixMem : dpMemExt
		generic map(useExtRam => useExtRam)
		port map (
			clk => ramclk,
          ramClk => ramClk,
          ramAddr => ramAddr,
          ramData => ramData,
          ramCke_n => ramCke_n,
          ramCs_n => ramCs_n,
          ramBw_n => ramBw_n,
          ramWe_n => ramWe_n,
          ramOe_n => ramOe_n,
          ramAdv => ramAdv,
          ramLbo => ramLbo,
          ramZz => ramZz,
			wrEn => ramWr,
			wrAddr => writeAddr,
			wrData => ramDataIn,
			rdAddr => readAddr,
			rdData => ramDataOut,
			rdEn => ramRd,
			rdAck => ramValid
		);

thrAdder : Adder24b
		port map (
			a => thrOld,
			b => thrInc,
			clk => ramclk,
			ce => clkEn,
			sclr => rst,
			s => thrNew);

totAdder : Adder24b
		port map (
			a => totOld,
			b => totD,
			clk => ramclk,
			ce => clkEn,
			sclr => rst,
			s => totNew);

totSqrAdder : Adder24b
		port map (
			a => totSqrOld,
			b => totSqrD,
			clk => ramclk,
			ce => clkEn,
			sclr => rst,
			s => totSqrNew);

totMult : mult16
		port map (
			clk => ramclk,
			a => totM,
			b => totM,
			sclr => rst,
			p => TotSqr);

--ramRdcount: process
--	begin
--		wait until rising_edge(clk); 
--			if(rst='1') then
--				trig_addrout <=(others=>'0');
--			else 
--				if((histoState = READOUT) and (RamRd = '1')) then
--					trig_addrout <= trig_addrout + '1';
--				end if;
--			end if;
--	end process;	
--
--
--	
--trig_addin: process
--	begin
--		wait until rising_edge(ramclk); 
--			if(rst='1') then
--				trig_addrin <=(others=>'0');
--			else 
--				if(fifoWrEn = '1') then
--					trig_addrin <= trig_addrin + '1';
--				end if;
--			end if;
--	end process;	
--	
--	addr_icon : icon
--	  port map (
--		 CONTROL0 => CONTROL0);
--
--	addr_ila : ila
--	  port map (
--		 CONTROL => CONTROL0,
--		 CLK => clk,
--		 TRIG0 => trig0,
--		 TRIG1 => trig1,
--		 TRIG2 => trig2,
--		 TRIG3 => trig3
--		 );
----
----   
--		trig0(15 downto 0)  <= ramDataOut(15 downto 0);
--		trig0(31 downto 16) <= fifoData(15 downto 0);
--		trig1(0)  			  <= rfr;
--		trig1(1)				  <= fifoEmpty;
--		trig1(2)  			  <= wait2cyc;
--		trig1(15 downto 14) <= readAddr(17 downto 16);
--		trig2(15 downto 0)  <= readAddr(15 downto 0);
--      trig3(17 downto 0)  <= trig_addrout;
--      trig3(18) 			  <= rocpipe;
--      trig3(36 downto 19) <= trig_addrin;	
end Behavioral;

