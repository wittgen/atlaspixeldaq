--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 


library IEEE;
use IEEE.STD_LOGIC_1164.all;

--use work.simulation.all; 	-- get number of chips for simulation vs implementation

package rodHistoPack is

-----------------------------------

-- function prototypes
function getHiAddr(control: std_logic_vector) return integer;
function getNumOfChips(simulation: boolean) return integer;
function getChipSize(simulation: boolean) return integer;

-- definitions for the mode
constant readoutModeBits: integer := 2;
constant modeIdle: std_logic_vector(readoutModeBits - 1 downto 0) := "00";
constant modeSample: std_logic_vector(readoutModeBits - 1 downto 0) := "01";
constant modeReadoutWait: std_logic_vector(readoutModeBits - 1 downto 0) := "10";
constant modeReadout: std_logic_vector(readoutModeBits - 1 downto 0) := "11";

constant readoutOneWord: std_logic := '0';
constant readoutTwoWords: std_logic := '1';

-- we need to delay the ToT signal in order for the address converter to finish its job
constant addrConverterDelay: integer := 5;

---------------------------------
-- definitions for input data format and address conversion
-- target values are 8 FE-I4 per histogramming unit.
-- the maximum hit rate in this case is approx 80MHz with two-hit FE-I4 data format
-- we compute a pixel number from row and col values in order to save memory space
-- Per chip: 336*80 = 26880, Per 8 chips: 215040 => 256kB = 18 bit

-- input format 
-- Bit 08 .. 00 	: row number
-- Bit 15 .. 09 	: col number
-- Bit 18 .. 16 	: chip number
-- Bit 22 .. 19 	: ToT value

constant rowBits: integer := 9;
constant colBits: integer := 7;
constant chipBits: integer := 3;
constant totBits: integer := 4;

constant rowShift: integer := 0;
constant colShift: integer := rowBits;
constant chipShift: integer := colShift + colBits;
constant totShift: integer := chipShift + chipBits;
---------------------------------

---------------------------------
-- definitions for the control register
-- Bit 00               : select one or all chips
-- Bit 02 .. 01         : full, half, quarter, or eigth row. also selects mask stepping size mask: 000 = full. 001 = step 2, 011 = step 4, 111 = step 8
-- Bit 04 .. 03         : mode of operation: ONLINE/OFFLINE_OCCUPANCY, SHORT/LONG_TOT
-- Bit 10 .. 08			: mask stepping step index: 0 to 1/3/7 (don't care if no stepping)
-- Bit 31 .. 24         : expected occupancy value (used to calculate differences)

constant chipCtlBits: integer := 1; -- select 0 or given number
constant chipCtlNull: std_logic_vector(chipCtlBits - 1 downto 0) := "0"; --- only one chip
constant chipCtlOne: std_logic_vector(chipCtlBits - 1 downto 0) := "1"; --- all 8 chips

constant rowMaskCtlBits: integer := 2; -- row mask setting (read out every, every other, every 4th... row)
constant rowMaskNone: std_logic_vector(rowMaskCtlBits - 1 downto 0) := "00";
constant rowMask2: std_logic_vector(rowMaskCtlBits - 1 downto 0) := "01";
constant rowMask4: std_logic_vector(rowMaskCtlBits - 1 downto 0) := "10";
constant rowMask8: std_logic_vector(rowMaskCtlBits - 1 downto 0) := "11";

--type opMode_t is (ONLINE_OCCUPANCY, OFFLINE_OCCUPANCY, SHORT_TOT, LONG_TOT); -- TODO MK: use custom types to clean up code
constant opModeCtlBits: integer := 2;
constant opOnlineOccupancy: std_logic_vector(opModeCtlBits - 1 downto 0) := "00";
constant opOfflineOccupancy: std_logic_vector(opModeCtlBits - 1 downto 0) := "01";
constant opShortTot: std_logic_vector(opModeCtlBits - 1 downto 0) := "10";
constant opLongTot: std_logic_vector(opModeCtlBits - 1 downto 0) := "11";

constant scanTypeBits: integer := 1; -- read out ToT/ToT² values or discard them
constant scanTypeOcc: std_logic_vector(scanTypeBits - 1 downto 0) := "0"; -- occupancy only
constant scanTypeTot: std_logic_vector(scanTypeBits - 1 downto 0) := "1"; -- ToT/ToT² and occupancy

constant occSizeBits: integer := 1; -- 8/24 bit occupancy
constant occSizeShort: std_logic_vector(occSizeBits - 1 downto 0) := "0";
constant occSizeLong: std_logic_vector(occSizeBits - 1 downto 0) := "1";

constant expectedOccCtlBits: integer := 8;

constant chipCtlShift: integer := 0;
constant rowMaskCtlShift: integer := chipCtlShift + chipCtlBits;
constant opModeCtlShift: integer := rowMaskCtlShift + rowMaskCtlBits;
constant expectedOccCtlShift: integer := 24;

constant mstepIndexCtlBits: integer := 3 + 1; -- 3 bits index selection plus 1 index enable bit
constant mstepIndexCtlShiftOdd: integer := 8;
constant mstepIndexCtlShiftEven: integer := mstepIndexCtlShiftOdd + mstepIndexCtlBits;
constant mstepIndexCtlIgnore: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0000";
constant mstepIndexCtlStep1: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0001";
constant mstepIndexCtlStep2: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0010";
constant mstepIndexCtlStep3: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0011";
constant mstepIndexCtlStep4: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0100";
constant mstepIndexCtlStep5: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0101";
constant mstepIndexCtlStep6: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0110";
constant mstepIndexCtlStep7: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "0111";
constant mstepIndexCtlStep8: std_logic_vector(mstepIndexCtlBits - 1 downto 0) := "1000";

---------------------------------
constant rowSize: integer := 336;
constant colSize: integer := 80;
constant chipSize: integer := colSize * rowSize;
constant addrBits: integer := 18; -- don't use rowBits + colBits + chipBits !
constant numOfChips: integer := 8;

-- maximum number of pixels per single FEI4, FEI4s served by one SP6FMT, and one ROD
constant pixelsPerFE: integer := chipSize;
constant pixelsPerFPGA: integer := pixelsPerFE * numOfChips;

---------------------------------
-- Definitions for internal processing and output
-- For the ROD we target 32 bit ram width to not complicate the output to the newtwork.

-- Internally we use 36 bit wide memory, which is then either condensed into one 32 bit
-- word or expanded into two words during the readout phase
--
-- Bit 07 .. 00 	: occupancy value
-- Bit 19 .. 08 	: ToT sum
-- Bit 35 .. 20 	: ToT² sum

-- or for online histogramming
--
-- Bit 23 .. 00         : occupancy value

----
-- Final output depends upon configuration - 4 options:

-- 1. Online histogramming: occupancy values only, 24 bit accuracy, packed in one data word.
-- Mode: ONLINE_OCCUPANCY

-- Bit 23 .. 00         : occupancy value

-- 2. Threshold scan: occupancy values only, 8 bit accuracy, 4 pixels packed in one data word.
-- Mode: OFFLINE_OCCUPANCY

-- Bit 07 .. 00 	: occupancy value a
-- Bit 15 .. 08 	: occupancy value b
-- Bit 23 .. 15 	: occupancy value c
-- Bit 31 .. 24 	: occupancy value d

-- 3. Readout that contains sum of ToT/ToT² values and the difference to the expected occupancy
-- as defined in the histo control register.
-- Mode: SHORT_TOT

-- Bit 03 .. 00         : occupancy difference to value set in histogrammer control register
--                      : values 0x0..0xE: number of missing hits, 0xF: 15 or more missing hits.
-- Bit 15 .. 04         : sum of ToT values
-- Bit 31 .. 16         : sum of ToT² values

-- 4. Readout that contains all the gathered data. Splitting in two words, aligned on 16 bit boundary.
-- Mode: LONG_TOT

-- Word 1 --
-- Bit 07 .. 00         : occupancy value
-- Word 2 --
-- Bit 11 .. 00         : sum of ToT values
-- Bit 31 .. 16         : sum of ToT² values

constant occWidth: integer := 8; -- 8 bits for 255 iterations on occupancy scan
constant occOnlineBits: integer := 24; -- 24 bits for a maximum of 16M hits per pixel
constant occBits: integer := occWidth; -- TODO MK: unify naming: bits/width and shift/offs
constant occMissingWidth: integer := 4; -- size of missing triggers for SHORT_TOT readout

constant totIterBits: integer := 8; -- 8 bits for 256 iterations
constant totWidth: integer := 4;
constant totRamWidth: integer := totWidth + totIterBits;
constant totSqrRamWidth: integer := 2 * totWidth + totIterBits;
constant histDataBits: integer := occBits + totRamWidth + totSqrRamWidth; -- this is the internal size of histogrammer memory

-- ram offsets
constant occOffs: integer := 0;
constant occOnlineOffs: integer := 0;
constant totRamOffs: integer := occOffs + occBits;
constant totSqrRamOffs: integer := totRamOffs + totRamWidth;

constant useBocIoSync: boolean := true;
constant use40MhzExt: boolean := true; -- false;

constant ramWidth: integer := 36; -- width of the internal histogrammer memory (BRAM/SSRAM) is 36 bits
constant dmaWidth: integer := 32; -- width of the target memory for histogrammer readout (DDR2) is 32 bits


constant gndBits: std_logic_vector(histDataBits - 1 downto 0) := (others => '0');


end rodHistoPack;
-----------------------------------


package body rodHistoPack is

-- Returns the upper memory limit used during reading out of the histogrammer depending on the configuration.
function getHiAddr(control:std_logic_vector) return integer is
begin
	case (control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) & control(chipCtlShift + chipCtlBits - 1 downto chipCtlShift)) is
		-- only one FE
		when (rowMask8 & chipCtlNull) => return ((pixelsPerFE / 8) - 1);
		when (rowMask4 & chipCtlNull) => return ((pixelsPerFE / 4) - 1);
		when (rowMask2 & chipCtlNull) => return ((pixelsPerFE / 2) - 1);
		when (rowMaskNone & chipCtlNull) => return ((pixelsPerFE) - 1);
		-- all FEs
		when (rowMask8 & chipCtlOne) => return ((pixelsPerFPGA / 8) - 1);
		when (rowMask4 & chipCtlOne) => return ((pixelsPerFPGA / 4) - 1);
		when (rowMask2 & chipCtlOne) => return ((pixelsPerFPGA / 2) - 1);
		when (rowMaskNone & chipCtlOne) => return ((pixelsPerFPGA) - 1);
		-- should never happen
		when others => return (pixelsPerFPGA - 1);
	end case;
end getHiAddr;


-- These functions were originally used to get a smaller "chip" to speed up simulation.
function getNumOfChips(simulation:boolean) return integer is
begin
	if simulation then 
		--return 1;
		return 2 ** chipBits;
	else
		return 2 ** chipBits;
	end if;
end getNumOfChips;


function getChipSize(simulation:boolean) return integer is
begin
	if simulation then 
		--return rowSize * colSize;	
		return 4 * colSize;
	else
		return rowSize * colSize;
	end if;

end getChipSize;

end rodHistoPack;
