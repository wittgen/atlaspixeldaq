----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    18:45:24 06/14/2010 
-- Design Name: 
-- Module Name:    addrConverter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
-- The raw pixel position (row, col, chip) is converted into an effective address
-- With mask stepping, the value value is modified in order to reduce the address space
-- To date, 1 mask step is used (factor 2 reduction) for odd (mask = 1)/even (mask = 0) rows

-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Revision 0.02 - 20120626 - compensate row/col numbering starting at 1 (instead 0)
--   	Two default address mappings are supported for the moment
--		1/8 each of 8 chips and mask stepping = 2, 10 double columns (1st, 2nd, 3rd, 4th quarter)
--			Row bit 0 = 0 => all rows, 1 => divide row by 2
--			Col bits 2..0 = 000 => all columns, 001 => 1st quarter, 011 => 2nd, 101 => 3rd, 111 => 4th
--		One full chip: select chip via control bits 
--			Chip bit 0 = 0 => chip number 0, 1 => chip number as given
-- Revision 0.03 - 20130104 Bugfixes
-- Revision 0.04 - 20130524 major changes regarding the allowed readout modes: one or all chips; all rows or only a 
-- 		fraction of 2, 4, 8.
--	
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.rodHistoPack.all;

entity addrConverter is
	 generic (simulation : boolean := false);
    Port ( 
			  control : in  STD_LOGIC_VECTOR (31 downto 0);
			  valid : in  STD_LOGIC;
           hit : out  STD_LOGIC;
           addr : out  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
           col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
           chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
           rst : in  STD_LOGIC;
           clk : in  STD_LOGIC
			  );
end addrConverter;

architecture Behavioral of addrConverter is

signal tCol0, tCol:  integer range 0 to 2**colBits - 1;

signal tRow0, nRow:  integer range 0 to 2**rowBits - 1;
signal tRow: integer range 0 to 2**(colBits+rowBits) - 1;

signal chipSize: integer range 0 to 2**(colBits+rowBits) - 1;

signal tChip0: integer range 0 to 2**chipBits - 1;
signal tChip, tChip1: integer range 0 to 2**addrBits - 1; -- already large value
signal tAddr, tAddr0: integer range 0 to 2**addrBits - 1; -- (colBits + rowBits + chipBits);

signal tHit0, tHit1, tHit2, tHit3: std_logic;


-- mask stepping

signal tRowMaskOdd:  	STD_LOGIC_VECTOR (rowBits - 1 downto 0);
signal tRowOffsetOdd:  	STD_LOGIC_VECTOR (mstepIndexCtlBits - 1 downto 0);
signal tRowMaskEven:  	STD_LOGIC_VECTOR (rowBits - 1 downto 0);
signal tRowOffsetEven: 	STD_LOGIC_VECTOR (mstepIndexCtlBits - 1 downto 0);

signal hitIsOdd: std_logic;
signal noMaskIndex: std_logic;

constant mstepCheckBits: integer := 3;
signal tRow_mslice0, tRow_mslice:  STD_LOGIC_VECTOR (mstepCheckBits - 1 downto 0);


begin

	-- map control values
	mapControl: process
	begin
		wait until rising_edge(clk);
                case (control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift)) is
                        when rowMask8 => nRow <= (rowSize / 8);
                        when rowMask4 => nRow <= (rowSize / 4);
                        when rowMask2 => nRow <= (rowSize / 2);
                        when rowMaskNone => nRow <= rowSize;
                        when others => nRow <= rowSize;
                end case;	
	end process;

	conversion:process(clk,rst)
	begin
		if rst = '1' then
			hit <= '0';
			tHit0 <= '0';
			tHit1 <= '0';
			tHit2 <= '0';
			tAddr <= 0;
			addr <= (others => '0');
		elsif rising_edge(clk) then
			-- static control
			if (control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) = mstepIndexCtlIgnore) and 
				(control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) = mstepIndexCtlIgnore) then
				noMaskIndex <= '1';
			else
				noMaskIndex <= '0';
			end if;
			
			-- first stage
			-- subtract 1 from row and col so that they start at 0
			tCol0 <= conv_integer(unsigned(col - '1'));
			hitIsOdd <= col(0); -- save odd/even info
			tRow0 <= conv_integer(unsigned(row - '1'));
			
			-- compute mask step based difference. 
			tRowMaskOdd <= std_logic_vector(conv_unsigned(conv_integer(unsigned(row)) - 
								conv_integer(unsigned(control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd))),rowBits));

			tRowMaskEven <= std_logic_vector(conv_unsigned(conv_integer(unsigned(row)) - 
								conv_integer(unsigned(control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven))),rowBits));

			-- chip can be 0 or from input
			if control(chipCtlBits + chipCtlShift - 1 downto chipCtlShift) = chipCtlNull then
				tChip0 <= 0;
			else
				tChip0 <= conv_integer(unsigned(chip));
			end if;
			-- pipeline valid
			tHit0 <= valid;

			-- chip size
			chipSize <= colSize * nRow;
		
			-- second stage
			tCol <= tCol0;
			
			case (control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift)) is
				when rowMask8 => tRow <= (tRow0 / 8) * colSize;
				when rowMask4 => tRow <= (tRow0 / 4) * colSize;
				when rowMask2 => tRow <= (tRow0 / 2) * colSize;
				when rowMaskNone => tRow <= tRow0 * colSize;
				when others => tRow <= tRow0 * colSize;
			end case;	

			tChip1 <= tChip0 * chipSize; -- chip pixel offset
			
			tHit1 <= tHit0;

			-- mask step odd/even selection
			if hitIsOdd = '1' then -- odd
				tRow_mslice0 <= tRowMaskOdd(mstepCheckBits - 1 downto 0);
			else
				tRow_mslice0 <= tRowMaskEven(mstepCheckBits - 1 downto 0);
			end if;
			
			-- third stage
			tChip <= tChip1;
			tAddr0 <= tCol + tRow;
			tHit2<= tHit1;

			-- mask stepping bit masking. NO masking is handled by index
			case (control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift)) is
				when rowMask2 => tRow_mslice <= tRow_mslice0 AND "001";
				when rowMask4 => tRow_mslice <= tRow_mslice0 AND "011";
				when others => tRow_mslice <= tRow_mslice0;
			end case;	
			
			-- fourth stage
			tAddr <= tAddr0 + tChip;

			-- includes mask stepping evaluation
			-- force hit if index masking not enabled
			if (tRow_mslice = "000") or (noMaskIndex = '1') then
				tHit3 <= tHit2;
			else
				tHit3 <= '0';
			end if;
	
			-- fifth stage
			addr <= std_logic_vector(conv_unsigned(tAddr,addrBits));
			hit <= tHit3;	

		end if;
	end process;

end Behavioral;
