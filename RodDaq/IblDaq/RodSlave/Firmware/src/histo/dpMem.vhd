----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:31:57 02/25/2010 
-- Design Name: 
-- Module Name:    dpMem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

use work.rodHistoPack.all;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity dpMem is
    Port ( clk : in  STD_LOGIC;
           rdAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrData : in  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           wrEn : in  STD_LOGIC;
           rdData : out  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           rdEn : in  STD_LOGIC;
           rdAck : out  STD_LOGIC);
end dpMem;

architecture Behavioral of dpMem is

component dpr32kx36sp6
	port (
	clka: IN std_logic;
	wea: IN std_logic_VECTOR(0 downto 0);
	addra: IN std_logic_VECTOR(14 downto 0);
	dina: IN std_logic_VECTOR(35 downto 0);
	clkb: IN std_logic;
	addrb: IN std_logic_VECTOR(14 downto 0);
	doutb: OUT std_logic_VECTOR(35 downto 0));
end component;

component pipeline 
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0);
           dout : out  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0)
			  );
end component;

component pipeline_sig 
	Generic (stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC;
           dout : out  STD_LOGIC
			  );
end component;

signal writeRam: std_logic_vector(0 downto 0);

begin

	writeRam(0) <= wrEn;

	theRam: dpr32kx36sp6
		port map (
			clka => clk,
			wea => writeRam,
			addra => wrAddr(14 downto 0),
			dina => wrData,
			clkb => clk,
			addrb => rdAddr(14 downto 0),
			doutb => rdData);

  
	readDelay: pipeline_sig
		generic map (stages => 1)	-- delay 2 is too slow
		port map (
			clk => clk,
			din => rdEn,
			dout => rdAck
			);

end Behavioral;

