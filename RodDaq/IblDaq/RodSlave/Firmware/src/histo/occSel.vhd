----------------------------------------------------------------------------------
-- In order to read out only occupancy values instead of the
-- complete dataset with sum of ToT/ToT² values, we combine
-- 4 occupancy pixel values (7 bit each) into to one 32-bit
-- word that can then be fed into the DMA readout FIFO. 
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

use work.rodHistoPack.all;
--use IEEE.NUMERIC_STD.ALL;

entity occSel is
    Port ( rst : in  STD_LOGIC;
           clk : in  STD_LOGIC;
           valid : in  STD_LOGIC;
           datain : in  STD_LOGIC_VECTOR(occWidth - 1 downto 0);
           dataout : out  STD_LOGIC_VECTOR((4 * occWidth) - 1 downto 0);
           complete : out  STD_LOGIC);
end occSel;

architecture Behavioral of occSel is

type occSelStateType is (RESET, D1, D2, D3, D4);
signal currentState, nextState: occSelStateType;

signal complete1: std_logic;
signal occRegister : STD_LOGIC_VECTOR((4 * occWidth) - 1 downto 0);

begin

dataout <= occRegister;

seqProcess: process (clk, rst)
begin
	if rst = '1' then
		currentState <= RESET;
		occRegister <= (others => '0');
		complete <= '0';
	elsif rising_edge(clk) then
		-- save occ values
		case currentState is
		when D1 =>
			occRegister((1 * occWidth) - 1 downto (0 * occWidth)) <= datain;
		when D2 =>
			occRegister((2 * occWidth) - 1 downto (1 * occWidth)) <= datain;
		when D3 =>
			occRegister((3 * occWidth) - 1 downto (2 * occWidth)) <= datain;
		when D4 =>
			occRegister((4 * occWidth) - 1 downto (3 * occWidth)) <= datain;
		when others =>
			occRegister <= occRegister;
		end case;
		
		-- delay output of complete signal
		complete <= complete1;
		
		-- advance states
		currentState <= nextState;
	end if;
end process;

combProcess: process (currentState, valid)
begin
	complete1 <= '0';
	
	case currentState is
	when RESET =>
		nextState <= D1;
		
	when D1 =>
		if valid = '1' then
			nextState <= D2;
		else
			nextState <= currentState;
		end if;
		
	when D2 =>
		if valid = '1' then
			nextState <= D3;
		else
			nextState <= currentState;
		end if;

	when D3 =>
		if valid = '1' then
			nextState <= D4;
		else
			nextState <= currentState;
		end if;

	when D4 =>
		if valid = '1' then
			nextState <= D1;
			complete1 <= '1';
		else
			nextState <= currentState;
		end if;
		
	when others =>	-- never reached
		null;
	end case;

end process;

end Behavioral;
