----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:38:40 02/25/2010 
-- Design Name: 
-- Module Name:    pipeline - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_vec is
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0);
           dout : out  STD_LOGIC_VECTOR (pipeWidth -1 downto 0)
			  );
end pipeline_vec;

architecture Behavioral of pipeline_vec is

   type pipeType is array  (natural range <>) of std_logic_vector(pipeWidth -1 downto 0);
--	signal pipes: std_logic_vector(width -1 downto 0)(stages - 1 downto 0);
	signal pipes: pipeType(stages - 1 downto 0);
	
begin

bypass:	if stages = 0 generate
	dout <= din;
	end generate;
	
delayed: if stages > 0 generate
	-- initial stage
		dProc: process
			begin
				wait until rising_edge(clk);
				pipes(0) <= din;
		end process;
	-- final stage directly mapped
		dout <= pipes(stages - 1); 
	-- intermediate stages
		addDelay: if stages > 1 generate
			pipeLoop: for i in 1 to stages - 1 generate
				dProc2: process
				begin
					wait until rising_edge(clk);
					pipes(i) <= pipes(i - 1);
				end process;
			end generate;
		end generate;
	end generate;

end Behavioral;

------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

---- Uncomment the following library declaration if instantiating
---- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity pipeline_uns is
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  unsigned (pipeWidth - 1 downto 0);
           dout : out  unsigned (pipeWidth -1 downto 0)
			  );
end pipeline_uns;

architecture Behavioral of pipeline_uns is

   type pipeType is array  (natural range <>) of unsigned(pipeWidth -1 downto 0);
--	signal pipes: std_logic_vector(width -1 downto 0)(stages - 1 downto 0);
	signal pipes: pipeType(stages - 1 downto 0);
	
begin

bypass:	if stages = 0 generate
	dout <= din;
	end generate;
	
delayed: if stages > 0 generate
	-- initial stage
		dProc: process
			begin
				wait until rising_edge(clk);
				pipes(0) <= din;
		end process;
	-- final stage directly mapped
		dout <= pipes(stages - 1); 
	-- intermediate stages
		addDelay: if stages > 1 generate
			pipeLoop: for i in 1 to stages - 1 generate
				dProc2: process
				begin
					wait until rising_edge(clk);
					pipes(i) <= pipes(i - 1);
				end process;
			end generate;
		end generate;
	end generate;

end Behavioral;

------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity pipeline_sig is
	Generic (stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC;
           dout : out  STD_LOGIC
			  );
end pipeline_sig;

architecture Behavioral_sig of pipeline_sig is

   type pipeType is array  (natural range <>) of std_logic;
--	signal pipes: std_logic_vector(width -1 downto 0)(stages - 1 downto 0);
	signal pipes: pipeType(stages - 1 downto 0);
	
begin

bypass:	if stages = 0 generate
	dout <= din;
	end generate;
	
delayed: if stages > 0 generate
	-- initial stage
		dProc: process
			begin
				wait until rising_edge(clk);
				pipes(0) <= din;
		end process;
	-- final stage directly mapped
		dout <= pipes(stages - 1); 
	-- intermediate stages
		addDelay: if stages > 1 generate
			pipeLoop: for i in 1 to stages - 1 generate
				dProc2: process
				begin
					wait until rising_edge(clk);
					pipes(i) <= pipes(i - 1);
				end process;
			end generate;
		end generate;
	end generate;

end Behavioral_sig;

