----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:50:30 05/11/2012 
-- Design Name: 
-- Module Name:    rodHisto - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

use work.rodHistoPack.all;

entity rodHisto is 
	 generic (simulation : boolean := false; useExtRam: boolean := false);
	port (
		-- pixel side ----
		hitValid : in  STD_LOGIC;
		row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
		col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
		chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
      totVal : IN  std_logic_vector(totWidth - 1 downto 0);
      rfd : OUT  std_logic;
		-- network side ----
      mode : IN  std_logic_vector (readoutModeBits - 1 downto 0);
		rfr : in  STD_LOGIC;	-- ready for readout
		roc : OUT  std_logic;
		histValid : OUT  std_logic;
		histo: out std_logic_vector(dmaWidth - 1 downto 0);
		-- ext ram --------
		ramClk : IN  std_logic;
		ramAddr : OUT  std_logic_vector(addrBits - 1 downto 0);
		ramData : INOUT  std_logic_vector(histDataBits - 1 downto 0);
		ramCke_n : OUT  std_logic;
		ramCs_n : OUT  std_logic;
		ramBw_n : OUT  std_logic_vector(3 downto 0);
		ramWe_n : OUT  std_logic;
		ramOe_n : OUT  std_logic;
		ramAdv : OUT  std_logic;
		ramLbo : OUT  std_logic;
		ramZz : OUT  std_logic;
		-- common ---------
		control : in  STD_LOGIC_VECTOR (31 downto 0);
		rst : in  STD_LOGIC;
		clk : in  STD_LOGIC
	);
end rodHisto;

architecture Behavioral of rodHisto is

   COMPONENT histoTop
	 generic (simulation : boolean := false; useExtRam: boolean := false);
   PORT(
		   control : in  STD_LOGIC_VECTOR (31 downto 0);
         clk : IN  std_logic;
         rst : IN  std_logic;
         enable : IN  std_logic;
         pixAddr : IN  std_logic_vector(addrBits - 1 downto 0);
         totVal : IN  std_logic_vector(totWidth - 1 downto 0);
         mode : IN  std_logic_vector(readoutModeBits - 1 downto 0);
         addrOut : OUT  std_logic_vector(addrBits - 1 downto 0);
			histo: out std_logic_vector(dmaWidth - 1 downto 0);
		   rfr : in  STD_LOGIC;	-- ready for readout
         rfd : OUT  std_logic;
         roc : OUT  std_logic;
         valid : OUT  std_logic;
         ramClk : IN  std_logic;
		   ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		   ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
--         ramAddr : OUT  std_logic_vector(17 downto 0);
--         ramData : INOUT  std_logic_vector(31 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
        );
    END COMPONENT;
	 



   --Inputs
   signal enable : std_logic := '0';
   signal pixAddr : std_logic_vector(addrBits - 1 downto 0);
   signal totVal_d0 : std_logic_vector(totWidth - 1  downto 0);

 	--Outputs
   signal addrOut : std_logic_vector(addrBits - 1 downto 0);
   signal valid : std_logic;
	
attribute keep:string;
attribute keep of pixAddr :signal is "true";
attribute keep of totVal_d0 :signal is "true";	


component addrConverter
	 generic (simulation : boolean := false);
    Port ( 
	   control : in  STD_LOGIC_VECTOR (31 downto 0);
		valid : in  STD_LOGIC;
		hit : out  STD_LOGIC;
		addr : out  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
		row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
		col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
		chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
		rst : in  STD_LOGIC;
		clk : in  STD_LOGIC
			  );
end component;
 
component pipeline_vec
	Generic (pipeWidth: integer := 8; stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0);
           dout : out  STD_LOGIC_VECTOR (pipeWidth - 1 downto 0)
			  );
end component;


signal hitpipe: std_logic;
signal fifoRdEn: std_logic;
signal fifoFull: std_logic;
signal fifoEmpty: std_logic;
signal fifoDatain: std_logic_vector(addrBits + totWidth - 1 downto 0); --22bits

signal toggle: std_logic := '0';

component addrcvtFifo
  Port (
    wr_clk : IN STD_LOGIC;
	 rd_clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(addrBits + totWidth - 1 downto 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(addrBits + totWidth - 1 downto 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
end component;

-- component icon
--  PORT (
--		 CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
-- end component;
--	
-- component ila
--  PORT (
--    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
--    CLK : IN STD_LOGIC;
--    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
--    TRIG1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--    TRIG2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
--	 TRIG3 : IN STD_LOGIC_VECTOR(47 DOWNTO 0)
--	 );
-- end component;

	signal CONTROL0: STD_LOGIC_VECTOR(35 DOWNTO 0);
	signal TRIG0: STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal TRIG1: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG2: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG3: STD_LOGIC_VECTOR(47 DOWNTO 0);
	
begin

   histUnit: histoTop 
		generic map(simulation => simulation, useExtRam => useExtRam)
		PORT MAP (
			 control => control,
          clk => clk,
          rst => rst,
          enable => enable,
          pixAddr => pixAddr,
          totVal => totVal_d0,
          mode => mode,
          addrOut => addrOut,
          histo => histo,
          rfr => rfr,
          rfd => rfd,
          roc => roc,
          valid => HistValid,
          ramClk => ramClk,
          ramAddr => ramAddr,
          ramData => ramData,
          ramCke_n => ramCke_n,
          ramCs_n => ramCs_n,
          ramBw_n => ramBw_n,
          ramWe_n => ramWe_n,
          ramOe_n => ramOe_n,
          ramAdv => ramAdv,
          ramLbo => ramLbo,
          ramZz => ramZz
        );

	-- we need to delay for 5 cycles the tot input to compensate the address converter latency
	totPipe : pipeline_vec
		generic map(pipeWidth => totWidth, stages => addrConverterDelay)
		port map (clk => clk, din => totVal, dout => fifoDatain(totwidth - 1 downto 0));

   converter: addrConverter 
		generic map(simulation => simulation)
		PORT MAP (
			 control => control,
          valid => hitValid,
          hit => hitpipe,
          addr => fifoDatain(addrBits + totWidth - 1 downto totwidth),
          row => row,
          col => col,
          chip => chip,
          rst => rst,
          clk => clk
        );

  addrFifo: addrcvtFifo
	port map (
    wr_clk => clk,
	 rd_clk => ramclk,
    rst => rst,
    din => fifoDatain,
    wr_en => hitpipe,
    rd_en => enable,
    dout(addrBits + totWidth - 1 downto totwidth) => pixAddr,
	 dout(totwidth - 1 downto 0) => totVal_d0,
    full => fifoFull,
    empty => fifoEmpty);
	 
--enable <= (not fifoEmpty) and toggle;

	toggleProc: process
	begin
		wait until rising_edge(ramclk);
		toggle <= not toggle;
		enable <= (not fifoEmpty) and toggle;
	end process;

--	histo_icon : icon
--	  port map (
--		 CONTROL0 => CONTROL0);
--
--	histo_ila : ila
--	  port map (
--		 CONTROL => CONTROL0,
--		 CLK => ramclk,
--		 TRIG0 => trig0,
--		 TRIG1 => trig1,
--		 TRIG2 => trig2,
--		 TRIG3 => trig3
--		 );
--
--    trig0(3 downto 0)   <= totVal_d0;
--    trig0(21 downto 4)  <= pixAddr;
--    trig0(22) 				<= fifoFull;
--    trig0(23) 				<= fifoEmpty;
--    trig0(23 downto 20) <= totVal0_r;
--    trig0(24)           <= rfd0;
--    trig0(25)           <= histValid0;
--    trig0(26)           <= rfr0;
--    trig0(27)           <= roc0;
--    trig0(29 downto 28) <= rxdata_boc2rod(9 downto 8);
--    trig0(30)           <= rxdata_boc2rod(10);
--    trig0(31)           <= rxdata_boc2rod(11);
--                                                                                                                
--    trig1(0)            <= effectiveCtlReg(ctlBocEnableBit);
--    trig1(1)            <= effectiveCtlReg(ctlBocTestBit);
--    trig1(12 downto 2)  <= testBocData;
--    trig1(13)           <= testBocEnable;
--    trig1(15 downto 14) <= inmemSelect;
--                                                
--    trig2(7 downto 0)   <= xc_in;
--    trig2(8)            <= bocHitEnable0;
--    trig2(9)            <= bocHitEnable1;
--   
--    trig3(0)            <= hitEnable1_r;
--    trig3(9 downto 1)   <= row1_r;
--    trig3(16 downto 10) <= col1_r;
--    trig3(19 downto 17) <= chip1_r;
--    trig3(23 downto 20) <= totVal1_r;
--    trig3(24)           <= rfd1;
--    trig3(25)           <= histValid1;
--    trig3(26)           <= rfr1;
--    trig3(27)           <= roc1;
--    trig3(28)           <= effectiveCtlReg(ctlHistMode0Bit);
--    trig3(29)           <= effectiveCtlReg(ctlHistMode1Bit);
--    trig3(30)           <= effectiveCtlReg(ctlHistMux0Bit);
--    trig3(31)           <= effectiveCtlReg(ctlHistMux1Bit);
--    trig3(39 downto 32) <= rxdata_boc2rod(7 downto 0);

end Behavioral;

