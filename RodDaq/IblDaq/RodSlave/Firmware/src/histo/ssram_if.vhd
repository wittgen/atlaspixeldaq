----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:20:04 08/07/2013 
-- Design Name: 
-- Module Name:    ssram_if - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
use work.rodHistoPack.all;

entity ssram_if is
    Port ( clk : in  STD_LOGIC;
			  -- external memory ports
			  ramClk: in std_logic;
			  ramAddr: out STD_LOGIC_VECTOR (addrBits - 1 downto 0);
			  ramData: inout STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
			  ramCke_n: out std_logic; -- low for normal operation
			  ramCs_n: out std_logic;
			  ramBw_n: out std_logic_vector(3 downto 0); -- combine with we_n
			  ramWe_n: out std_logic;
			  ramOe_n: out std_logic;
			  ramAdv : out std_logic; -- low for normal operation (direct address mode)
			  ramLbo: out std_logic; -- mode input. low for normal operation (linear burst)
			  ramZz: out std_logic;  -- low for operation
			  -- histogrammer ports
           rdAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrAddr : in  STD_LOGIC_VECTOR (addrBits - 1 downto 0);
           wrData : in  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           wrEn : in  STD_LOGIC;
           rdData : out  STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
           rdEn : in  STD_LOGIC;
           rdAck : out  STD_LOGIC);
end ssram_if;

architecture Behavioral of ssram_if is

component pipeline_sig
	Generic (stages: integer := 0);
    Port ( 
			  clk : in  STD_LOGIC;
           din : in  STD_LOGIC;
           dout : out  STD_LOGIC
			  );
end component;

 component icon
  PORT (
		 CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
 end component;
	
 component ila
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TRIG1 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    TRIG2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	 TRIG3 : IN STD_LOGIC_VECTOR(47 DOWNTO 0)
	 );
 end component;
 	signal CONTROL0: STD_LOGIC_VECTOR(35 DOWNTO 0);
	signal TRIG0: STD_LOGIC_VECTOR(31 DOWNTO 0);
	signal TRIG1: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG2: STD_LOGIC_VECTOR(15 DOWNTO 0);
	signal TRIG3: STD_LOGIC_VECTOR(47 DOWNTO 0);
constant ssramDelay: integer := 2; -- data always 2 clk cycles later than address --by Hujun 

signal ramCs, ramWe, ramOe: std_logic;
signal wrData_r: STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
signal ramdataWe_r: STD_LOGIC_VECTOR (histDataBits - 1 downto 0);
signal ramOe_pipe: std_logic;
signal ramWe_pipe: std_logic;
signal ramWe_r :std_logic;
signal ramaddr_r: STD_LOGIC_VECTOR (addrBits - 1 downto 0);
signal ramBw_r: std_logic_vector(3 downto 0);
signal rdData_int: STD_LOGIC_VECTOR (histDataBits - 1 downto 0);

--attribute equivalent_register_removal: string;   
--attribute equivalent_register_removal of ramWe_r : signal is "no";
attribute keep:string;
attribute keep of ramdataWe_r :signal is "true";
attribute keep of ramWe_r :signal is "true";
--
--attribute equivalent_register_removal of wrData_r : signal is "no";
attribute keep of wrData_r :signal is "true";
attribute keep of ramBw_r :signal is "true";

begin
	ramProc: process
	begin
		wait until rising_edge(ramClk); 
			ramCs <= '1';
			ramOe <= '1';		
		if wrEn = '1' then 
			ramaddr_r <= wrAddr;
			ramWe <= '1';
		elsif  rdEn = '1'		then					
			ramaddr_r <= rdAddr;
			ramWe <= '0';
		else 
			ramaddr_r <= rdAddr;
			ramWe <= '0';		
			end if;
	end process;

----	 map outputs
--	ramAddr <= ramaddr_r after 1 ns ;
--	ramCs_n <= not ramCs after 1 ns;
--	ramWe_n <= not ramWe after 1 ns;
--   ramOe_n <= not ramOe_pipe after 1 ns; 
--	ramBw_n <= "1111" after 1 ns when ramWe = '0' else "0000" after 1 ns;

--   ramData <= wrData_r after 1 ns when ramWe_r = '0' else (others => 'Z') after 1 ns;
--	  rdData <= ramData after 1 ns;	

-------pipe the interface port 1 cycle cleanly. for pack into IOB.
	dqout:	for i in 0 to histDataBits - 1 generate
		ramData(i) <= wrData_r(i) when ramdataWe_r(i) = '0' else 'Z';
	   rdData_int(i) <= ramData(i);
		ramdataWe_r_bit : process
		begin 
			wait until rising_edge(ramClk);
				ramdataWe_r(i) <= not ramWe_pipe;
		end process;
	end generate;
	
	bwout: for i in 0 to 3 generate
		ramBw_n(i) <= ramBw_r(i);
		ramBw_r_bit: process
		begin
			wait until rising_edge(ramClk);
				ramBw_r(i) <= not ramWe;
		end process;
	end generate;	

	ramWe_n <= ramWe_r;
	
	ramportreg: process
	begin
		wait until rising_edge(ramClk); 
			ramAddr <= ramaddr_r after 1 ns ;
			ramCs_n <= not ramCs after 1 ns;
			ramWe_r <= not ramWe after 1 ns;
			ramOe_n <= not ramOe_pipe after 1 ns;
--			ramWe_r <= not ramWe_pipe;
--			rdData <= ramData after 1 ns;

--			if ramWe = '0' then
--				ramBw_n <= "1111" after 1 ns;
--			else ramBw_n <= "0000" after 1 ns;
--			end if;
	
				wrData_r <= wrData after 1 ns;
	end process;	
	

	
	-- fixed signals
	ramCke_n <= '0';
	ramAdv <= '0';
	ramLbo <= '0';
	ramZz <= '0';
	rdData <= rdData_int;

ramOeDelay: pipeline_sig
		generic map (stages => ssramDelay)
		port map (
			clk => clk,
			din => ramOe,
			dout => ramOe_pipe
			);
			
ramWeDelay: pipeline_sig
		generic map (stages => ssramDelay)
		port map (
			clk => clk,
			din => ramWe,
			dout => ramWe_pipe
			);		
			
	-- zbt pipeline is 3 stages
	readDelay: pipeline_sig
		generic map (stages => 4)
		port map (
			clk => clk,
			din => rdEn,
			dout => rdAck
			);
	
--	sram_icon : icon
--	  port map (
--		 CONTROL0 => CONTROL0);
--
--	sram_ila : ila
--	  port map (
--		 CONTROL => CONTROL0,
--		 CLK => ramclk,
--		 TRIG0 => trig0,
--		 TRIG1 => trig1,
--		 TRIG2 => trig2,
--		 TRIG3 => trig3
--		 );
----
--      trig0(17 downto 0)   <= ramAddr_r;	
--      trig0(31 downto 18)  <= rdData_int(13 downto 0);
----    trig0(22) 				<= fifoFull;
----    trig0(23) 				<= fifoEmpty;
----    trig0(23 downto 20) <= totVal0_r;
----    trig0(24)           <= rfd0;
----    trig0(25)           <= histValid0;
----    trig0(26)           <= rfr0;
----    trig0(27)           <= roc0;
----    trig0(29 downto 28) <= rxdata_boc2rod(9 downto 8);
----    trig0(30)           <= rxdata_boc2rod(10);
----    trig0(31)           <= rxdata_boc2rod(11);
----                                                                                                                
----    trig1(15) 			   <= ;
----                                                
----    trig2(15)            <= bocHitEnable1;
----   
--      trig3(35 downto 0)  <= wrData_r;
--      trig3(36)   		  <= ramdataWe_r(0);
----    trig3(16 downto 10) <= col1_r;
----    trig3(19 downto 17) <= chip1_r;
----    trig3(23 downto 20) <= totVal1_r;
----    trig3(24)           <= rfd1;
----    trig3(25)           <= histValid1;
----    trig3(26)           <= rfr1;
----    trig3(27)           <= roc1;
----    trig3(28)           <= effectiveCtlReg(ctlHistMode0Bit);
----    trig3(29)           <= effectiveCtlReg(ctlHistMode1Bit);
----    trig3(30)           <= effectiveCtlReg(ctlHistMux0Bit);
----    trig3(31)           <= effectiveCtlReg(ctlHistMux1Bit);
----    trig3(39 downto 32) <= rxdata_boc2rod(7 downto 0);	
end Behavioral;

