--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   23:57:54 08/06/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/CERN/efb_bing/ibl_router/router_tb.vhd
-- Project Name:  ibl_router
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: router_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY router_tb IS
END router_tb;
 
ARCHITECTURE behavior OF router_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT router_top
    PORT(
         clk80 : IN  std_logic;
         clk100 : IN  std_logic;
         reset : IN  std_logic;
         chip_id : IN  std_logic_vector(2 downto 0);
         efb_data_in : IN  std_logic_vector(31 downto 0);
         efb_data_valid : IN  std_logic;
         slink_utest : OUT  std_logic;
         slink_ureset : OUT  std_logic;
         slink_uctrl : OUT  std_logic;
         slink_lff : IN  std_logic;
         slink_ldown : IN  std_logic;
         data_to_slink : OUT  std_logic_vector(15 downto 0);
         slink_data_wen : OUT  std_logic;
--         histo_unit_rfd : IN  std_logic;
         hitValid : OUT  std_logic;
         chip : OUT  std_logic_vector(2 downto 0);
         row : OUT  std_logic_vector(8 downto 0);
         col : OUT  std_logic_vector(6 downto 0);
         totVal : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal clk80 : std_logic := '0';
   signal clk100 : std_logic := '0';
   signal reset : std_logic := '0';
   signal chip_id : std_logic_vector(2 downto 0) := (others => '0');
   signal efb_data_in : std_logic_vector(31 downto 0) := (others => '0');
   signal efb_data_valid : std_logic := '0';
   signal slink_lff : std_logic := '0';
   signal slink_ldown : std_logic := '0';
--   signal histo_unit_rfd : std_logic := '0';

 	--Outputs
   signal slink_utest : std_logic;
   signal slink_ureset : std_logic;
   signal slink_uctrl : std_logic;
   signal data_to_slink : std_logic_vector(15 downto 0);
   signal slink_data_wen : std_logic;
   signal hitValid : std_logic;
   signal chip : std_logic_vector(2 downto 0);
   signal row : std_logic_vector(8 downto 0);
   signal col : std_logic_vector(6 downto 0);
   signal totVal : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant clk80_period : time := 10 ns;
   constant clk100_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: router_top PORT MAP (
          clk80 => clk80,
          clk100 => clk100,
          reset => reset,
          chip_id => chip_id,
          efb_data_in => efb_data_in,
          efb_data_valid => efb_data_valid,
          slink_utest => slink_utest,
          slink_ureset => slink_ureset,
          slink_uctrl => slink_uctrl,
          slink_lff => slink_lff,
          slink_ldown => slink_ldown,
          data_to_slink => data_to_slink,
          slink_data_wen => slink_data_wen,
--          histo_unit_rfd => histo_unit_rfd,
          hitValid => hitValid,
          chip => chip,
          row => row,
          col => col,
          totVal => totVal
        );

   -- Clock process definitions
   clk80_process :process
   begin
		clk80 <= '0';
		wait for clk80_period/2;
		clk80 <= '1';
		wait for clk80_period/2;
   end process;
 
   clk100_process :process
   begin
		clk100 <= '0';
		wait for clk100_period/2;
		clk100 <= '1';
		wait for clk100_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 25 ns;
      reset <= '1';	wait for 100 ns;
      reset <= '0';	-- histo_unit_rfd <= '0'; 
		wait for 100 ns;
      
		chip_id <= "000"; efb_data_valid <= '1'; -- chip 0
      efb_data_in <= X"2FFFFFFF"; wait for 10 ns; -- header should not do anything for histogrammer
		efb_data_in <= "10000011" & X"FF" & "0110111" & "000110111"; wait for 10 ns; -- valid hit row = 55, col = 55, tot= F
		efb_data_in <= "10000011" & X"44" & "1010000" & "001010000"; wait for 10 ns; -- invalid hit row = 80, col = 80, tot = 4
		efb_data_in <= "10000011" & X"99" & "1000110" & "100111011"; wait for 10 ns; -- valid hit row = 315, col = 70, tot= 9
		efb_data_in <= "10000011" & X"44" & "0000000" & "101101000"; wait for 10 ns;-- invalid hit row = 340, col = 0, tot = 4
		efb_data_valid <= '0'; wait for 20 ns;
		
		-- should not have any data at all...
		chip_id <= "010"; -- chip 2
      efb_data_in <= X"2FFFFFFF"; wait for 10 ns; -- header should not do anything for histogrammer
		efb_data_in <= "10000011" & X"44" & "0110111" & "000110111"; wait for 10 ns; -- valid hit row = 55, col = 55, tot= 4
		efb_data_in <= "10000011" & X"44" & "1010000" & "001010000"; wait for 10 ns; -- invalid hit row = 80, col = 80, tot = 4
		efb_data_in <= "10000011" & X"44" & "0110111" & "000110111"; wait for 10 ns; -- valid hit row = 315, col = 70, tot= 4
		efb_data_in <= "10000011" & X"44" & "0000000" & "101101000"; wait for 10 ns;-- invalid hit row = 340, col = 0, tot = 4

      -- histo_unit_rfd <= '1';

      efb_data_valid <= '1';
		chip_id <= "100"; -- chip 4
      efb_data_in <= X"2FFFFFFF"; wait for 10 ns; -- header should not do anything for histogrammer
		efb_data_in <= "10000011" & X"66" & "0110110" & "000110110"; wait for 10 ns; -- valid hit row = 54, col = 54, tot= 6
		efb_data_in <= "10000011" & X"44" & "1010000" & "001010000"; wait for 10 ns; -- invalid hit row = 80, col = 80, tot = 4
		efb_data_in <= "10000011" & X"88" & "1000110" & "100111011"; wait for 10 ns; -- valid hit row = 315, col = 70, tot= 8
		efb_data_in <= "10000011" & X"44" & "0000000" & "101101000"; wait for 10 ns;-- invalid hit row = 340, col = 0, tot = 4

      slink_lff <= '1';
      slink_ldown <= '1';
      wait;
   end process;

END;
