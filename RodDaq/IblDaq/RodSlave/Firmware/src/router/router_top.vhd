----------------------------------------------------------------------------------
-- Company: University of Washington Electrical Engineering/Physics
-- Engineer: Bing (Shaw-Pin) Chen
-- 
-- Create Date:    12:33:05 08/02/2013 
-- Design Name:    IBLROD Firmware
-- Module Name:    router_top - Behavioral 
-- Project Name:   ATLAS
-- Target Devices: Spartan 6 LX150 FPGA
-- Tool versions:  ISE 14.5
-- Description: 
--
-- Dependencies: 
--
--
--
-- To do list for next revision:
--   if the read and write clock of the slink is the same (in current test bench) then the read enable signal will always go
--   low (active low) after receives data....should be careful and check fifo_full signal...
--   check overflow of Slink Fifo
--   investigate the marking of end of histo calibration event, may have to deal with in EFB
--
-- Revision 0.04 taken out histo fifo
--
-- Revision 0.03 reactivate rfd signal from histo unit and added in a 1K fifo for storing hits. rfd is now read enable and 
--               fifo valid handshake is now hitValid. Suppresses invalid data (single hits)
--
-- Revision 0.02 added in efb to histogrammer register block as well as histo grammer data extractor block.
--               modified efb_top to provide chip ID info. histo_rfd not used.                
-- Revision 0.01 File Created
-- 
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity router_top is
  Port ( 
      clk40, clk80, clk100	: in  STD_LOGIC;
      reset                : in  STD_LOGIC;
		calib_mode				: in  STD_LOGIC;
		router_busy				: out STD_LOGIC;
--  Signals From a Single EFB Engine
		chip_id              : in  STD_LOGIC_VECTOR ( 2 DOWNTO 0);
		efb_data_in          : in  STD_LOGIC_VECTOR (31 DOWNTO 0);
		efb_data_valid       : in  STD_LOGIC;
		k_word					: in  STD_LOGIC;	-- from EFB, 1 when BOF or EOF, else 0
	 
--  Slink Interface Signals
--  slink1_uclk           : out STD_LOGIC; -- currently not used in sp6fmt
      slink_utest          : out STD_LOGIC; -- 
      slink_ureset			: out STD_LOGIC; -- user reset, ACTIVE LOW asynchronous
		slink_uctrl          : out STD_LOGIC; -- user control: high for command, low for data
    --  Data shall only be written to the S-LINK when this line is high. After it goes low, up to two
    --  more words may be written. Functional when S-LINK is in test mode.
      slink_lff            : in  STD_LOGIC; -- link full flag
    --  When low indicates that the S-LINK is not operational. Asynchronous. Can go low due to:
    --  S-LINK failure; then LDOWN# is latched low until cleared by a reset cycle.
    --  S-LINK undergoing reset cycle, then LDOWN# goes high when reset cycle is complete.
    --  S-LINK in test mode, then LDOWN# goes high when test mode is ended.
      slink_ldown          : in  STD_LOGIC; -- link down, the connected S-Link not operational
		data_to_slink        : out STD_LOGIC_VECTOR (31 downto 0);
		slink_data_wen       : out STD_LOGIC; -- write enable, ACTIVE LOW
	 
--  Histogrammer Interface Signals	 
	   histoFull				: out STD_LOGIC;
	   histoOverrun			: out STD_LOGIC;
		histo_rfd       		: in  STD_LOGIC; -- histogrammer is ready receive data
      hitValid             : out STD_LOGIC;
      chip                 : out STD_LOGIC_VECTOR (2 downto 0); -- one histogrammer deals with 8 chips
      row         			: out STD_LOGIC_VECTOR (8 downto 0);
      col				      : out STD_LOGIC_VECTOR (6 downto 0);
      totVal   			   : out STD_LOGIC_VECTOR (3 downto 0);
      --Error Registers
      link0_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link1_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link2_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link3_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link4_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link5_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link6_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      link7_errors_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      header_err_reg       : out STD_LOGIC_VECTOR(31 downto 0);     
      trailer_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
      rd_timeout_err_reg   : out STD_LOGIC_VECTOR(31 downto 0); 
      ht_limit_err_reg     : out STD_LOGIC_VECTOR(31 downto 0); 
      row_col_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
      l1_id_err_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
      bc_id_err_reg        : out STD_LOGIC_VECTOR(31 downto 0) 
  );
end router_top;

architecture Behavioral of router_top is

--------------------------------------------------------------
--                 Signals and COmponents
--------------------------------------------------------------

	COMPONENT slink_fifo
		PORT (
			rst 			: IN STD_LOGIC;
			wr_clk 		: IN STD_LOGIC;
			rd_clk 		: IN STD_LOGIC;
			din 			: IN STD_LOGIC_VECTOR(32 DOWNTO 0);
			wr_en			: IN STD_LOGIC;
			rd_en 		: IN STD_LOGIC;
			dout 			: OUT STD_LOGIC_VECTOR(32 DOWNTO 0);
			full 			: OUT STD_LOGIC;
			almost_full : OUT STD_LOGIC;
			empty 		: OUT STD_LOGIC;
			valid 		: OUT STD_LOGIC;
			prog_full   : OUT STD_LOGIC
		);
	END COMPONENT;
	
	COMPONENT histo_fifo_80to100
		PORT (
			rst 		   : IN  STD_LOGIC;
			wr_clk 	   : IN  STD_LOGIC;
			rd_clk	   : IN  STD_LOGIC;
			din 		   : IN  STD_LOGIC_VECTOR(23 DOWNTO 0);
			wr_en 	   : IN  STD_LOGIC;
			rd_en 	   : IN  STD_LOGIC;
			dout 		   : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
			full 		   : OUT STD_LOGIC;
			almost_full : OUT STD_LOGIC;
			empty 	   : OUT STD_LOGIC;
			valid 	   : OUT STD_LOGIC;
			prog_full 	: OUT STD_LOGIC
		);
	END COMPONENT;
	
	signal slink_fifo_write   : std_logic;
	signal slink_fifo_empty   : std_logic;
	signal slink_fifo_full	  : std_logic;
	signal slink_fifo_afull	  : std_logic;
	signal slink_fifo_ren_i   : std_logic;
	signal slink_fifo_ren	  : std_logic;
	signal slink_fifo_valid	  : std_logic;
	signal slink_fifo_indata  : std_logic_vector(32 downto 0);
	signal slink_fifo_dataout : std_logic_vector(32 downto 0);

   signal chip_id_i          : std_logic_vector( 2 downto 0);
	signal histo_data         : std_logic_vector(31 downto 0);
   signal histo_valid        : std_logic;	
	
	signal priority_a							: std_logic;
	signal write_a, 		write_b			: std_logic;
	signal fifo_a_din, 	fifo_b_din		: std_logic_vector(23 downto 0);
	signal pop_a, 			pop_b				: std_logic;
	signal fifo_a_dout, 	fifo_b_dout		: std_logic_vector(23 downto 0);
	signal fifo_a_valid, fifo_b_valid	: std_logic;
	signal fifo_a_empty, fifo_b_empty	: std_logic;
	signal fifo_a_afull, fifo_b_afull	: std_logic;
	signal fifo_a_full, 	fifo_b_full	   : std_logic;

   --Link Error Counters
   signal link0_errors   : UNSIGNED (31 downto 0);
   signal link1_errors   : UNSIGNED (31 downto 0);
   signal link2_errors   : UNSIGNED (31 downto 0);
   signal link3_errors   : UNSIGNED (31 downto 0);
   signal link4_errors   : UNSIGNED (31 downto 0);
   signal link5_errors   : UNSIGNED (31 downto 0);
   signal link6_errors   : UNSIGNED (31 downto 0);
   signal link7_errors   : UNSIGNED (31 downto 0);  
   --Individual Error Counters
   signal header_err     : UNSIGNED (31 downto 0);
   signal trailer_err    : UNSIGNED (31 downto 0);
   signal rd_timeout_err : UNSIGNED (31 downto 0);
   signal ht_limit_err   : UNSIGNED (31 downto 0);
   signal row_col_err    : UNSIGNED (31 downto 0);
   signal l1_id_err      : UNSIGNED (31 downto 0);
   signal bc_id_err      : UNSIGNED (31 downto 0);

   signal err_detect     : STD_LOGIC;
   signal link_and_errs  : STD_LOGIC_VECTOR(9 downto 0);
	
	signal fifo_a_prog_full: std_logic;
	signal fifo_b_prog_full: std_logic;

begin

-- DAV (20Oct2014)
router_busy	<= slink_fifo_afull when calib_mode = '0' else (fifo_a_prog_full or fifo_b_prog_full); 
-- router_busy <= slink_fifo_afull;

-- histogrammer FIFO must not drive BUSY !!!  OR fifo_a_afull OR fifo_b_afull;
histoFull <= fifo_a_full or fifo_B_full;
-- overrung when writing to full fifo
histoOverrun <= '1' when (fifo_a_full = '1' and write_a = '1') or (fifo_b_full = '1' and write_b = '1') else '0';

------------------------------------------------------
--        S-Link Interface
------------------------------------------------------

	slink_fifo_proc : process(reset, clk80)
	begin
		if(reset = '1')then
			slink_fifo_indata	<= (others => '0');
			slink_fifo_write 	<= '0';
		elsif(rising_edge(clk80))then
			if(calib_mode = '0')then -- go ahead and forward data to S-Link
				slink_fifo_indata	<= k_word & efb_data_in;
            slink_fifo_write 	<= efb_data_valid;
			else
				slink_fifo_indata	<= (others => '0');
				slink_fifo_write 	<= '0';
			end if;
		end if;
	end process slink_fifo_proc;

	slink_fifo0 : slink_fifo
	  PORT MAP (
			rst 		   => reset,
			wr_clk 	   => clk80,
			rd_clk 	   => clk40,
			din 		   => slink_fifo_indata,
			wr_en 	   => slink_fifo_write,
			rd_en 	   => slink_fifo_ren,
			dout 		   => slink_fifo_dataout,
			almost_full => open,
			full 		   => slink_fifo_full,
			empty 	   => slink_fifo_empty,
			valid       => slink_fifo_valid,
			prog_full   => slink_fifo_afull
	  );

	slink_ureset	 	<= NOT(reset); -- all control signals ACTIVE LOW
	slink_utest			<= '1';
	slink_uctrl 		<= NOT slink_fifo_dataout(32) when slink_fifo_empty = '0' else '1';	
	slink_fifo_ren 	<= NOT slink_fifo_empty AND slink_lff AND slink_ldown;
	slink_data_wen 	<= NOT slink_fifo_ren;
	data_to_slink     <= slink_fifo_dataout(31 downto 0);	
	
------------------------------------------------------
--        Histogrammer Interface
------------------------------------------------------
	
	histo_fifo_a : histo_fifo_80to100
		PORT MAP (
			rst 		   => reset, 
			wr_clk 	   => clk80,
			rd_clk 	   => clk100,
			din 		   => fifo_a_din,
			wr_en 	   => write_a,
			rd_en 	   => pop_a,
			dout 		   => fifo_a_dout,
			full 		   => fifo_a_full,
			almost_full => fifo_a_afull,
			empty 	   => fifo_a_empty,
			valid 	   => fifo_a_valid,
			prog_full	=> fifo_a_prog_full
		);
		
	histo_fifo_b : histo_fifo_80to100
		PORT MAP (
			rst 		   => reset, 
			wr_clk 	   => clk80,
			rd_clk 	   => clk100,
			din 		   => fifo_b_din,
			wr_en 	   => write_b,
			rd_en 	   => pop_b,
			dout 		   => fifo_b_dout,
			full 		   => fifo_b_full,
			almost_full => fifo_b_afull,
			empty 	   => fifo_b_empty,
			valid 	   => fifo_b_valid,
			prog_full	=> fifo_b_prog_full
		);
		
	histo_data_register : process(clk80,reset)
	begin
	  if(reset = '1') then
			histo_data    <= (others => '0');
			chip_id_i     <= (others => '0');
			histo_valid   <= '0';
	  elsif(rising_edge(clk80)) then
			histo_data 	  <= efb_data_in;
			chip_id_i     <= chip_id;
			histo_valid	  <= efb_data_valid;
	  end if;
	end process histo_data_register ;	
		
	-- extracts data and write into histo dual fifo (fifo_a for normal hits and fifo_b for double hits) 	
	histo_data_extractor : process (
		 clk80, 
		 reset
	  )
	begin	
		if(reset = '1') then
			fifo_a_din	<= (others => '0');
			write_a			<= '0';
			fifo_b_din	<= (others => '0');
			write_b			<= '0'; 
		elsif(rising_edge(clk80)) then
			if(histo_valid = '1' AND  histo_data(31 downto 29) = "100" -- valid hit :)
										AND (histo_data( 8 downto  0) < "101010001") -- row in range :)
										AND (histo_data(15 downto  9) < "1010001"))then -- col in range :)
				fifo_a_din <= '1' & chip_id_i & histo_data(8 downto 0) & histo_data(15 downto  9) & histo_data(23 downto 20);
				write_a <= '1';
				if(histo_data(19 downto 16) /= "1111")then -- double hit!
					fifo_b_din <= '1' & chip_id_i & histo_data(8 downto 0) & histo_data(15 downto  9) & histo_data(19 downto 16);
					write_b <= '1';
				else
					write_b <= '0';
				end if;	
			else
				fifo_a_din	<= (others => '0');
				write_a			<= '0';
				fifo_b_din	<= (others => '0');
				write_b			<= '0';
			end if;
		end if;
	end process histo_data_extractor;

	pop_fifo_process: process(clk100, reset)
	begin
		if(reset = '1') then
			pop_a	<= '0'; pop_b	<= '0';
			priority_a	<= '1';	    
		elsif(rising_edge(clk100)) then
			if(histo_rfd = '1') then
				if(fifo_a_empty = '0' AND priority_a = '1') then
					pop_a	<= '1'; pop_b	<= '0';
					priority_a		<= '0';
				elsif(fifo_b_empty = '0') then
					pop_a	<= '0'; pop_b	<= '1';
					priority_a		<= '1';		
				elsif(fifo_a_empty = '0') then
					pop_a	<= '1'; pop_b	<= '0';
					priority_a		<= '0';	
				else
					pop_a	<= '0'; pop_b	<= '0';
					priority_a		<= '1';
				end if;
			else
				pop_a	<= '0'; pop_b	<= '0';
				priority_a		<= '1';
			end if;
		end if;
	end process pop_fifo_process;

	readout_histo_data : process(reset, clk100)
	begin
		if(reset = '1')then
			hitValid  <= '0';         
			chip      <= (others => '0');
			row       <= (others => '0');  			
			col		 <= (others => '0');        
			totVal    <= (others => '0');		  
		elsif(rising_edge(clk100))then
			-- AKU: removed check for "other" fifo as this will lead to deadlock if both fifos valid
			-- FIFO-A is the main one. check it first
			-- if(fifo_a_valid = '1' AND fifo_b_valid = '0')then
			if(fifo_a_valid = '1')then
				hitValid  <= fifo_a_valid AND fifo_a_dout(23);
				chip      <= fifo_a_dout(22 downto 20);
				row       <= fifo_a_dout(19 downto 11);  			 
				col		 <= fifo_a_dout(10 downto  4);        
				totVal    <= fifo_a_dout( 3 downto  0);	
			-- elsif(fifo_a_valid = '0' AND fifo_b_valid = '1')then
			elsif(fifo_b_valid = '1')then
				hitValid  <= fifo_b_valid AND fifo_b_dout(23);
				chip      <= fifo_b_dout(22 downto 20);
				row       <= fifo_b_dout(19 downto 11) + 1;  			 
				col		 <= fifo_b_dout(10 downto  4);        
				totVal    <= fifo_b_dout( 3 downto  0);					
			else
				hitValid  <= '0';         
				chip      <= (others => '0');
				row       <= (others => '0');  			
				col		 <= (others => '0');        
				totVal    <= (others => '0');
			end if;
		end if;
	end process readout_histo_data;

   error_counter : process(clk80, reset)
   begin
      if(reset = '1') then
         link0_errors  <= (others => '0'); 
         link1_errors  <= (others => '0'); 
         link2_errors  <= (others => '0'); 
         link3_errors  <= (others => '0'); 
         link4_errors  <= (others => '0'); 
         link5_errors  <= (others => '0'); 
         link6_errors  <= (others => '0'); 
         link7_errors  <= (others => '0'); 
         header_err    <= (others => '0'); 
         trailer_err   <= (others => '0'); 
         rd_timeout_err<= (others => '0'); 
         ht_limit_err  <= (others => '0'); 
         row_col_err   <= (others => '0'); 
         l1_id_err     <= (others => '0'); 
         bc_id_err     <= (others => '0');
         link_and_errs <= (others => '0'); 
         err_detect    <= '0';
      elsif(rising_edge(clk80)) then
         --NOTE: Error Flags and positions in trailer are subject to change.
         if(efb_data_valid = '1' AND efb_data_in(31 downto 29) = "010") then
           link_and_errs <= efb_data_in(26 downto 23) & efb_data_in(20 downto 15);
           err_detect    <= efb_data_in(23) OR efb_data_in(20) OR efb_data_in(19) OR 
                            efb_data_in(18) OR efb_data_in(17) OR efb_data_in(16) OR
                            efb_data_in(15);
         else
            err_detect    <= '0';
         end if;
         if(err_detect = '1') then
            case link_and_errs is
               when "0001000000" =>link0_errors<=link0_errors+1;rd_timeout_err<=rd_timeout_err+1;
               when "0000100000" =>link0_errors<=link0_errors+1;row_col_err<=row_col_err+1;
               when "0000010000" =>link0_errors<=link0_errors+1;header_err<=header_err+1;  
               when "0000001000" =>link0_errors<=link0_errors+1;l1_id_err<=l1_id_err+1;
               when "0000000100" =>link0_errors<=link0_errors+1;bc_id_err<=bc_id_err+1;  
               when "0000000010" =>link0_errors<=link0_errors+1;trailer_err<=trailer_err+1; 
               when "0000000001" =>link0_errors<=link0_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "0011000000" =>link1_errors<=link1_errors+1;rd_timeout_err<=rd_timeout_err+1;
               when "0010100000" =>link1_errors<=link1_errors+1;row_col_err<=row_col_err+1; 
               when "0010010000" =>link1_errors<=link1_errors+1;header_err<=header_err+1; 
               when "0010001000" =>link1_errors<=link1_errors+1;l1_id_err<=l1_id_err+1; 
               when "0010000100" =>link1_errors<=link1_errors+1;bc_id_err<=bc_id_err+1;  
               when "0010000010" =>link1_errors<=link1_errors+1;trailer_err<=trailer_err+1; 
               when "0010000001" =>link1_errors<=link1_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "0101000000" =>link2_errors<=link2_errors+1;rd_timeout_err<=rd_timeout_err+1;
               when "0100100000" =>link2_errors<=link2_errors+1;row_col_err<=row_col_err+1;
               when "0100010000" =>link2_errors<=link2_errors+1;header_err<=header_err+1;
               when "0100001000" =>link2_errors<=link2_errors+1;l1_id_err<=l1_id_err+1;
               when "0100000100" =>link2_errors<=link2_errors+1;bc_id_err<=bc_id_err+1; 
               when "0100000010" =>link2_errors<=link2_errors+1;trailer_err<=trailer_err+1;
               when "0100000001" =>link2_errors<=link2_errors+1;ht_limit_err<=ht_limit_err+1;
               when "0111000000" =>link3_errors<=link3_errors+1;rd_timeout_err<=rd_timeout_err+1; 
               when "0110100000" =>link3_errors<=link3_errors+1;row_col_err<=row_col_err+1; 
               when "0110010000" =>link3_errors<=link3_errors+1;header_err<=header_err+1; 
               when "0110001000" =>link3_errors<=link3_errors+1;l1_id_err<=l1_id_err+1; 
               when "0110000100" =>link3_errors<=link3_errors+1;bc_id_err<=bc_id_err+1;  
               when "0110000010" =>link3_errors<=link3_errors+1;trailer_err<=trailer_err+1; 
               when "0110000001" =>link3_errors<=link3_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "1001000000" =>link4_errors<=link4_errors+1;rd_timeout_err<=rd_timeout_err+1; 
               when "1000100000" =>link4_errors<=link4_errors+1;row_col_err<=row_col_err+1; 
               when "1000010000" =>link4_errors<=link4_errors+1;header_err<=header_err+1; 
               when "1000001000" =>link4_errors<=link4_errors+1;l1_id_err<=l1_id_err+1; 
               when "1000000100" =>link4_errors<=link4_errors+1;bc_id_err<=bc_id_err+1;  
               when "1000000010" =>link4_errors<=link4_errors+1;trailer_err<=trailer_err+1; 
               when "1000000001" =>link4_errors<=link4_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "1011000000" =>link5_errors<=link5_errors+1;rd_timeout_err<=rd_timeout_err+1; 
               when "1010100000" =>link5_errors<=link5_errors+1;row_col_err<=row_col_err+1; 
               when "1010010000" =>link5_errors<=link5_errors+1;header_err<=header_err+1; 
               when "1010001000" =>link5_errors<=link5_errors+1;l1_id_err<=l1_id_err+1; 
               when "1010000100" =>link5_errors<=link5_errors+1;bc_id_err<=bc_id_err+1;  
               when "1010000010" =>link5_errors<=link5_errors+1;trailer_err<=trailer_err+1; 
               when "1010000001" =>link5_errors<=link5_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "1101000000" =>link6_errors<=link6_errors+1;rd_timeout_err<=rd_timeout_err+1; 
               when "1100100000" =>link6_errors<=link6_errors+1;row_col_err<=row_col_err+1; 
               when "1100010000" =>link6_errors<=link6_errors+1;header_err<=header_err+1; 
               when "1100001000" =>link6_errors<=link6_errors+1;l1_id_err<=l1_id_err+1; 
               when "1100000100" =>link6_errors<=link6_errors+1;bc_id_err<=bc_id_err+1;  
               when "1100000010" =>link6_errors<=link6_errors+1;trailer_err<=trailer_err+1; 
               when "1100000001" =>link6_errors<=link6_errors+1;ht_limit_err<=ht_limit_err+1; 
               when "1111000000" =>link7_errors<=link7_errors+1;rd_timeout_err<=rd_timeout_err+1; 
               when "1110100000" =>link7_errors<=link7_errors+1;row_col_err<=row_col_err+1; 
               when "1110010000" =>link7_errors<=link7_errors+1;header_err<=header_err+1; 
               when "1110001000" =>link7_errors<=link7_errors+1;l1_id_err<=l1_id_err+1; 
               when "1110000100" =>link7_errors<=link7_errors+1;bc_id_err<=bc_id_err+1;  
               when "1110000010" =>link7_errors<=link7_errors+1;trailer_err<=trailer_err+1; 
               when "1110000001" =>link7_errors<=link7_errors+1;ht_limit_err<=ht_limit_err+1;  
               when others       =>
                  link_and_errs <= (others => '0');
                  err_detect    <= '0';
            end case;
            err_detect <= '0';
         end if;
      end if;
   end process;

   link0_errors_reg   <= std_logic_vector(link0_errors); 
   link1_errors_reg   <= std_logic_vector(link1_errors);
   link2_errors_reg   <= std_logic_vector(link2_errors);
   link3_errors_reg   <= std_logic_vector(link3_errors);
   link4_errors_reg   <= std_logic_vector(link4_errors);
   link5_errors_reg   <= std_logic_vector(link5_errors);
   link6_errors_reg   <= std_logic_vector(link6_errors);
   link7_errors_reg   <= std_logic_vector(link7_errors);
   header_err_reg     <= std_logic_vector(header_err);
   trailer_err_reg    <= std_logic_vector(trailer_err);
   rd_timeout_err_reg <= std_logic_vector(rd_timeout_err);
   ht_limit_err_reg   <= std_logic_vector(ht_limit_err);
   row_col_err_reg    <= std_logic_vector(row_col_err);
   l1_id_err_reg      <= std_logic_vector(l1_id_err);
   bc_id_err_reg      <= std_logic_vector(bc_id_err);

end Behavioral;
