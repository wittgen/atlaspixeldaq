----------------------------------------------------------------------------------
-- Company: University of Washington, Seattle
-- Engineer: Shaw-Pin (Bing) Chen
-- 
-- Create Date:    11:26:17 10/5/2013 
-- Design Name: tt
-- Module Name:    rodSlave_top - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--   IBL ROD Slave firmware structure followed the implementation by John M Joseph (LBL)    
-- 
--   The IBL ROD slave firmware consists of one IBL ROD register block and two "half 
--   slaves". Each half slave consists of two formatters, one event fragment builder,
--   one router, and one (histogrammer). Each halfslave receives data from eight FEs.
--       along with embedded MicroBlaze softcore processor. Each formatter is capable of 
--   receiving, decoding, and formatting four front-end chip (FEI4-B) signals from 
--   each incoming signal bus from the Back-of-Crate (BOC) card. Under the quad-formatter
--   setup, integrated slave firmwareware handles data coming from 16 FE modules.
-- 
-- Dependencies: 
-- 
-- Revision: 

-- Revision 
-- Revision 03.20.2014 - "V6" passed bit-gen with speed grade -2, all timing constraints met, inserted chipscope cores.  
-- Revision 03.12.2014 - V2 mostly merged to V3
-- Revision 02.18.2014 - Merged histogrammer blocks, reserved spots for MicroBlaze
-- Revision 02.15.2014 - fixed Slink we and took out RAM Block items
-- Revision 0.01 - File Created

-- 
-- ROD BUSY LOGIC -- implement edge detector and counter for local busy signal histogramming (or should this be done in master?)
-- IDDR at top level for synchronizing BOC to ROD signals sampling at 80 - MHz
-- 

--     need to change how mode bits ef and ff works
--     to add fe occupancy counters
--     mode bits probably not needed for IBLROD
--     need to change how l1id trap works
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_arith.all;
use IEEE.STD_LOGIC_unsigned.all;
library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.all;

use work.rodHistoPack.all;
use work.datePack.all; -- via Windows Script by AK
use work.gitIdPack.all; -- by MK
use work.rodSlaveRegPack.all; -- by SPC

use std.textio.all;
use work.txt_util.all;

entity rodSlave_top is
	generic (use_bram		: boolean := true;
	         simulation 	: boolean := false; 
				useExtRam	: boolean := true); 
	port(
		clk40in_p, 	clk40in_n			: in 	STD_LOGIC;
		clk100in_p, clk100in_n			: in 	STD_LOGIC;
		clk200out							: out STD_LOGIC_VECTOR( 1 downto 0);
		clk200in								: in  STD_LOGIC_VECTOR( 1 downto 0);
		reset                    		: in  STD_LOGIC;
		reset_processor_core_SEL		: in  STD_LOGIC;
		-- V5 cmd to FE through SP6 since V5 does not have SSTL3_I IO capability
		xc_in                       	: in  STD_LOGIC_VECTOR( 7 downto 0);
		xc										: out STD_LOGIC_VECTOR( 7 downto 0);
		rxdata_boc2rod						: in  STD_LOGIC_VECTOR(47 downto 0);
		rod_busy_n                  	: out STD_LOGIC;
		-- Clock monitoring
		clk40mon 							: out STD_LOGIC;
		clk100mon 							: out STD_LOGIC;
		cpuclkmon 							: out STD_LOGIC;
		-- Master-Slave Communication
		rod_bus_ce		              	: in  STD_LOGIC;-- read-enable wrtie_strobe. Same as "rod_bus_strb_n", active high
		rod_bus_rnw              		: in  STD_LOGIC;-- read not write
		rod_bus_hwsb            		: in  STD_LOGIC;-- half-word select bit
		rod_bus_ds              		: in  STD_LOGIC;-- data word strobe for write
		rod_bus_addr             		: in  STD_LOGIC_VECTOR(15 downto 0);
		rod_bus_data             		: inout STD_LOGIC_VECTOR(15 downto 0);
		rod_bus_ack_out          		: out STD_LOGIC;
		-- Mode-Bits from Master (unused for IBLROD)
		mode_bits_in 						: in  STD_LOGIC_VECTOR(11 downto 0);
		modebits_fifo_wen_n_in  		: in  STD_LOGIC;
		modebits_fifo_rst_n_in  		: in  STD_LOGIC;
		modebits_fifo_ef_n_out  		: out STD_LOGIC;
		modebits_fifo_ff_n_out  		: out STD_LOGIC;
		-- Master to Slave Event ID Data
		ev_data_from_ppc					: in  STD_LOGIC_VECTOR(15 downto 0);
		ev_data_wen_n						: in  STD_LOGIC;
		ev_data_rst_n						: in  STD_LOGIC;   
		ev_data_almost_full_n			: out STD_LOGIC_VECTOR( 1 downto 0);
		ev_data_ready						: out STD_LOGIC_VECTOR( 1 downto 0); 
		ev_id_fifo_empty_error			: out STD_LOGIC_VECTOR( 1 downto 0);
      --master to slave front-end command pulse 
      fe_cmdpulse                   : in  STD_LOGIC;
		-- ROD to BOC S-Link
		slink0_utest, 	slink1_utest 		: out STD_LOGIC;		
		slink0_ureset, slink1_ureset		: out STD_LOGIC;
		slink0_uctrl, 	slink1_uctrl		: out STD_LOGIC;
		slink0_lff, 	slink1_lff			: in  STD_LOGIC;
		slink0_ldown, 	slink1_ldown		: in  STD_LOGIC;
		slink0_data_rod2boc 					: out STD_LOGIC_VECTOR(15 downto 0);
		slink1_data_rod2boc					: out STD_LOGIC_VECTOR(15 downto 0);
		slink0_we, 		slink1_we			: out STD_LOGIC;
		slink0_uclk, 	slink1_uclk			: out STD_LOGIC; --not used (ref: IBL BOC Prototype design pag 16)
		-- External SSRAM Signals
		sram1_io,			sram2_io				:	inout STD_LOGIC_VECTOR(35 downto 0);	-- Data I/O
		sram1_a,				sram2_a				:	out	STD_LOGIC_VECTOR(17 downto 0); 	-- Address
		sram1_lbo,			sram2_lbo			:	out	STD_LOGIC;  -- Burst Mode
		sram1_AdvLd_n,		sram2_AdvLd_n		:	out	STD_LOGIC;  -- Adv/Ld#
		sram1_bw1_n,		sram2_bw1_n			:	out	STD_LOGIC;  -- Bwa#
		sram1_bw2_n,		sram2_bw2_n			:	out	STD_LOGIC;  -- BWb#
		sram1_bw3_n,		sram2_bw3_n			:	out	STD_LOGIC;  -- Bwc#
		sram1_bw4_n,		sram2_bw4_n			:	out	STD_LOGIC;  -- BWd#
		sram1_we_n,			sram2_we_n			:	out	STD_LOGIC;  -- RW#
		sram1_oe_n,			sram2_oe_n			:	out	STD_LOGIC;  -- OE#
		sram1_cke_n,		sram2_cke_n			:	out	STD_LOGIC;  -- CKE#
		sram1_zz,			sram2_zz				:	out	STD_LOGIC;  -- ZZ#
		sram1_cs1,			sram2_cs1			:	out	STD_LOGIC;  -- CS# 
		-- MicroBlaze Signals
		rzq : inout STD_LOGIC;
		mcbx_dram_cs_n : out STD_LOGIC;
		mcbx_dram_we_n : out STD_LOGIC;
		mcbx_dram_udqs_n : inout STD_LOGIC;
		mcbx_dram_udqs : inout STD_LOGIC;
		mcbx_dram_udm : out STD_LOGIC;
		mcbx_dram_ras_n : out STD_LOGIC;
		mcbx_dram_odt : out STD_LOGIC;
		mcbx_dram_ldm : out STD_LOGIC;
		mcbx_dram_dqs_n : inout STD_LOGIC;
		mcbx_dram_dqs : inout STD_LOGIC;
		mcbx_dram_dq : inout STD_LOGIC_VECTOR(15 downto 0);
		mcbx_dram_clk_n : out STD_LOGIC;
		mcbx_dram_clk : out STD_LOGIC;
		mcbx_dram_cke : out STD_LOGIC;
		mcbx_dram_cas_n : out STD_LOGIC;
		mcbx_dram_ba : out STD_LOGIC_VECTOR(2 downto 0);
		mcbx_dram_addr : out STD_LOGIC_VECTOR(13 downto 0);
		RS232_Uart_1_sout : out STD_LOGIC;
		RS232_Uart_1_sin : in STD_LOGIC;
		RESET_in : in STD_LOGIC;
		ETHERNET_TX_ER : out STD_LOGIC;
		ETHERNET_TX_EN : out STD_LOGIC;
		ETHERNET_TX_CLK : out STD_LOGIC;
		ETHERNET_TXD : out STD_LOGIC_VECTOR(7 downto 0);
		ETHERNET_RX_ER : in STD_LOGIC;
		ETHERNET_RX_DV : in STD_LOGIC;
		ETHERNET_RX_CLK : in STD_LOGIC;
		ETHERNET_RXD : in STD_LOGIC_VECTOR(7 downto 0);
		ETHERNET_PHY_RST_N : out STD_LOGIC;
		ETHERNET_MII_TX_CLK : in STD_LOGIC;
		ETHERNET_MDIO : inout STD_LOGIC;
		ETHERNET_MDC : out STD_LOGIC;
		sd_vtt_s6 : out std_logic
	);                       
  	attribute IOB : string; 	
end rodSlave_top;                   
                                  
architecture Behavioral of rodSlave_top is
------------------------------------
--             SIGNALS
------------------------------------
	-- Top Level
	SIGNAL clk40, clk40_n				: STD_LOGIC; -- buffered internal clock		
	SIGNAL clk80late, clk80        : STD_LOGIC; -- buffered internal clock	
	signal clk100							: STD_LOGIC; -- buffered internal clock
	SIGNAL clk100in, clk100in_bar		: STD_LOGIC;			
	signal cpuclk, cpuclk_n, cpuclk2x: STD_LOGIC; -- Microblaze
	SIGNAL cpuclk100, cpuclk100_n		: STD_LOGIC;
	
	
   signal clk200out_p, clk200out_n :  std_logic;
   signal clk200 :  std_logic_vector(1 downto 0);	

	SIGNAL boc_dcm_locked_i         : STD_LOGIC;
	SIGNAL rst_i                    : STD_LOGIC;
	SIGNAL efb_id1_i, efb_id2_i			: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL modebits_fifo_ef_n_out_i	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL modebits_fifo_ff_n_out_i	: STD_LOGIC_VECTOR(1 downto 0);
	
	SIGNAL router0_busy_i		: STD_LOGIC;
	SIGNAL router1_busy_i		: STD_LOGIC;
	
	SIGNAL histoFull0_i		: STD_LOGIC := '0';
	SIGNAL histoFull1_i		: STD_LOGIC := '0';
	SIGNAL histoOverrun0_i		: STD_LOGIC := '0';
	SIGNAL histoOverrun1_i		: STD_LOGIC := '0';
			
	-- BOC-to-ROD
	CONSTANT b2r_serdes: BOOLEAN := false; -- boc2rod serdes config
	SIGNAL rxdata_boc2rod_r000	: STD_LOGIC_VECTOR(47 downto 0);
	SIGNAL rxdata_boc2rod_r00	: STD_LOGIC_VECTOR(47 downto 0);
	SIGNAL rxdata_boc2rod_r0	: STD_LOGIC_VECTOR(47 downto 0);
	SIGNAL rxdata_boc2rod_r1	: STD_LOGIC_VECTOR(47 downto 0);
	
	SIGNAL rx_rod_data_a_in, 	rx_rod_data_b_in	: STD_LOGIC_VECTOR(7 downto 0);
	SIGNAL rx_rod_data_c_in, 	rx_rod_data_d_in	: STD_LOGIC_VECTOR(7 downto 0);	
	SIGNAL rx_rod_addr_a_in, 	rx_rod_addr_b_in	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL rx_rod_addr_c_in, 	rx_rod_addr_d_in	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL rx_rod_we_a_in,		rx_rod_we_b_in		: STD_LOGIC;
	SIGNAL rx_rod_we_c_in,		rx_rod_we_d_in		: STD_LOGIC;
	SIGNAL rx_rod_ctrl_a_in, 	rx_rod_ctrl_b_in	: STD_LOGIC;
	SIGNAL rx_rod_ctrl_c_in, 	rx_rod_ctrl_d_in	: STD_LOGIC;
	
	SIGNAL rx_rod_data_a_i, rx_rod_data_b_i	: STD_LOGIC_VECTOR(7 downto 0);
	SIGNAL rx_rod_data_c_i, rx_rod_data_d_i	: STD_LOGIC_VECTOR(7 downto 0);	
	SIGNAL rx_rod_addr_a_i, rx_rod_addr_b_i	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL rx_rod_addr_c_i, rx_rod_addr_d_i	: STD_LOGIC_VECTOR(1 downto 0);
	SIGNAL rx_rod_we_a_i, 	rx_rod_we_b_i		: STD_LOGIC;
	SIGNAL rx_rod_we_c_i, 	rx_rod_we_d_i		: STD_LOGIC;
	SIGNAL rx_rod_ctrl_a_i, rx_rod_ctrl_b_i	: STD_LOGIC;
	SIGNAL rx_rod_ctrl_c_i, rx_rod_ctrl_d_i	: STD_LOGIC;

   -- ROD Bus & Block RAM Signals
	SIGNAL rod_bus_strb_in_i   : std_logic;
	SIGNAL rod_bus_strb_reg_i  : std_logic;
	SIGNAL rod_bus_strb_ram_i  : std_logic;	
	SIGNAL rod_bus_oe_n_i      : std_logic;-- tristate driver enable signal "tubf_i", active low
	SIGNAL rod_bus_data_out_i	: std_logic_vector(15 downto 0);
	SIGNAL rod_bus_data_out_reg_i	: std_logic_vector(15 downto 0);
	SIGNAL rod_bus_data_out_ram_i	: std_logic_vector(15 downto 0);
	SIGNAL rod_bus_data_in_i	: std_logic_vector(15 downto 0);
	SIGNAL rod_bus_ack_out_reg_i : std_logic;
	SIGNAL rod_bus_ack_out_ram_i : std_logic;
	SIGNAL ram_selected_i		: std_logic; -- determined from rod bus address

   -- INMEM FIFO Signals
	SIGNAL inmem_rst_i, 	inmem_rb_rst_i		: STD_LOGIC;
	SIGNAL inmemWe_i, 	inmemFifo_wen_i	: STD_LOGIC;
	SIGNAL inmemFifo_ren_i						: STD_LOGIC;
	SIGNAL inmemSel_i								: STD_LOGIC_VECTOR( 1 downto 0);
	SIGNAL inmemDin_i, 	inmemFifo_din_i	: STD_LOGIC_VECTOR(10 downto 0);
	SIGNAL inmem_regData_i						: STD_LOGIC_VECTOR(11 downto 0);
	SIGNAL inmemFifo_ef_i						: STD_LOGIC;
	SIGNAL inmemFifo_dout_i						: STD_LOGIC_VECTOR(10 downto 0);

	-- Slave Register Signals
	SIGNAL slave_id_i								: STD_LOGIC;
	SIGNAL fmt0_stts_reg_i						: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL fmt1_stts_reg_i						: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL fmt2_stts_reg_i						: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL fmt3_stts_reg_i						: STD_LOGIC_VECTOR(31 downto 0);	
   SIGNAL fmt0Occupancy_i                 : STD_LOGIC_VECTOR(39 downto 0); 
   SIGNAL fmt1Occupancy_i                 : STD_LOGIC_VECTOR(39 downto 0);
   SIGNAL fmt2Occupancy_i                 : STD_LOGIC_VECTOR(39 downto 0);
   SIGNAL fmt3Occupancy_i                 : STD_LOGIC_VECTOR(39 downto 0);
	SIGNAL fmt_link_enabled_i					: STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL fmt_trig_cnt_i                  : STD_LOGIC_VECTOR( 3 downto 0);
	SIGNAL fmt_timeout_limit_i					: STD_LOGIC_VECTOR(31 downto 0);
   SIGNAL fmt_trailer_limit_i					: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL fmt_data_overflow_limit_i			: STD_LOGIC_VECTOR(15 downto 0);
	SIGNAL fmt_header_trailer_limit_i		: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL fmt_rod_busy_limit_i	   		: STD_LOGIC_VECTOR( 9 downto 0);
   SIGNAL fmt_busy_i                      : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL fmt_time_out_error_i            : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL fmt_header_trailer_error_i      : STD_LOGIC_VECTOR(15 downto 0);

   SIGNAL hitdisccnfg_i                   : STD_LOGIC_VECTOR(31 downto 0);

   SIGNAL formatter0_frames_i             : STD_LOGIC_VECTOR(15 downto 0); 
   SIGNAL formatter1_frames_i             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL formatter2_frames_i             : STD_LOGIC_VECTOR(15 downto 0);
   SIGNAL formatter3_frames_i             : STD_LOGIC_VECTOR(15 downto 0);
   
	SIGNAL efb_source_id_i				: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL efb_format_version_i		: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL efb_run_number_i				: STD_LOGIC_VECTOR(31 downto 0);

	SIGNAL err_mask_wen1_i, 	err_mask_wen2_i   : STD_LOGIC;
	SIGNAL err_mask_wen3_i, 	err_mask_wen4_i   : STD_LOGIC;	
	SIGNAL err_mask_din1_i, 	err_mask_din2_i   : STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL err_mask_din3_i, 	err_mask_din4_i   : STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL err_mask_dout1_i, 	err_mask_dout2_i 	: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL err_mask_dout3_i, 	err_mask_dout4_i 	: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL err_mask_sel1_i, 	err_mask_sel2_i	: STD_LOGIC_VECTOR( 2 downto 0);
	SIGNAL err_mask_sel3_i, 	err_mask_sel4_i   : STD_LOGIC_VECTOR( 2 downto 0);

	SIGNAL efb1_out_fifo_rst_i, 		 	efb2_out_fifo_rst_i			 : STD_LOGIC;
	SIGNAL efb1_out_fifo1_empty_n_i, 	efb2_out_fifo1_empty_n_i    : STD_LOGIC;
	SIGNAL efb1_out_fifo1_aempty_n_i, 	efb2_out_fifo1_aempty_n_i   : STD_LOGIC;
	SIGNAL efb1_out_fifo1_full_n_i, 		efb2_out_fifo1_full_n_i     : STD_LOGIC;
	SIGNAL efb1_out_fifo1_afull_n_i, 	efb2_out_fifo1_afull_n_i    : STD_LOGIC;
	SIGNAL efb1_out_fifo1_play_done_i, 	efb2_out_fifo1_play_done_i	 : STD_LOGIC;
	SIGNAL efb1_out_fifo2_empty_n_i, 	efb2_out_fifo2_empty_n_i    : STD_LOGIC;
	SIGNAL efb1_out_fifo2_aempty_n_i, 	efb2_out_fifo2_aempty_n_i   : STD_LOGIC;
	SIGNAL efb1_out_fifo2_full_n_i, 		efb2_out_fifo2_full_n_i     : STD_LOGIC;
	SIGNAL efb1_out_fifo2_afull_n_i, 	efb2_out_fifo2_afull_n_i    : STD_LOGIC;
   SIGNAL efb1_out_fifo2_play_done_i,  efb2_out_fifo2_play_done_i  : STD_LOGIC;
	
	SIGNAL efb_bcid_offset_i         : STD_LOGIC_VECTOR( 9 downto 0);
	SIGNAL efb_grp_evt_enable_i      : STD_LOGIC_VECTOR( 1 downto 0); 
	SIGNAL efb_enable_l1_trap_i      : STD_LOGIC_VECTOR( 1 downto 0);   
	SIGNAL efb_l1_trap_full_in_i     : STD_LOGIC_VECTOR( 1 downto 0);
   
	SIGNAL efb1_event_count_i,		efb2_event_count_i		: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL efb1_l1id_fifo_data_i, efb2_l1id_fifo_data_i	: STD_LOGIC_VECTOR(39 downto 0);
	SIGNAL efb1_l1id_fifo_addr_i, efb2_l1id_fifo_addr_i	: STD_LOGIC_VECTOR( 3 downto 0);
	SIGNAL efb1_latched_l1id0_i, 	efb1_latched_l1id1_i	 : STD_LOGIC_VECTOR(12 downto 0);   
	SIGNAL efb1_latched_bcid0_i, 	efb1_latched_bcid1_i  : STD_LOGIC_VECTOR( 9 downto 0);   
	SIGNAL efb2_latched_l1id0_i, 	efb2_latched_l1id1_i  : STD_LOGIC_VECTOR(12 downto 0);   
	SIGNAL efb2_latched_bcid0_i, 	efb2_latched_bcid1_i  : STD_LOGIC_VECTOR( 9 downto 0); 

	SIGNAL efb1_misc_stts_reg_i, 	efb2_misc_stts_reg_i  : STD_LOGIC_VECTOR(30 downto 0);
	SIGNAL efb1_ev_header_data_i, efb2_ev_header_data_i	: STD_LOGIC_VECTOR(15 downto 0);
	SIGNAL efb1_ev_fifo_data1_i, 	efb2_ev_fifo_data1_i  : STD_LOGIC_VECTOR(34 downto 0);
	SIGNAL efb1_ev_fifo_data2_i, 	efb2_ev_fifo_data2_i  : STD_LOGIC_VECTOR(34 downto 0);
	                                 
	SIGNAL efb_mask_bcid_error_i			: STD_LOGIC;
	SIGNAL efb_mask_l1id_error_i   		: STD_LOGIC;
	SIGNAL efb_mask_boc_clock_err_i 	: STD_LOGIC;
	SIGNAL efb_mask_tim_clock_err_i 	: STD_LOGIC;
	SIGNAL efb_tim_bcid_in_rol_i    	: STD_LOGIC;
	SIGNAL efb_bcid_rollover_sel_i		: STD_LOGIC;
	
	SIGNAL chip0_i, 		chip1_i			: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL hitValid0_i, hitValid1_i : STD_LOGIC;
	SIGNAL row0_i, 			row1_i      : STD_LOGIC_VECTOR(8 downto 0);
	SIGNAL col0_i, 			col1_i			: STD_LOGIC_VECTOR(6 downto 0);
	SIGNAL totVal0_i, 	totVal1_i   : STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL hitValid0_i_r0, hitValid0_i_r1: STD_LOGIC;
	SIGNAL row0_i_r0, row0_i_r1:				STD_LOGIC_VECTOR(8 downto 0);
	SIGNAL col0_i_r0, col0_i_r1:				STD_LOGIC_VECTOR(6 downto 0);
	SIGNAL chip0_i_r0, chip0_i_r1:			STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL totVal0_i_r0, totVal0_i_r1:		STD_LOGIC_VECTOR(3 downto 0);		

	SIGNAL hitValid1_i_r0, hitValid1_i_r1: STD_LOGIC;
	SIGNAL row1_i_r0, row1_i_r1:				STD_LOGIC_VECTOR(8 downto 0);
	SIGNAL col1_i_r0, col1_i_r1:				STD_LOGIC_VECTOR(6 downto 0);
	SIGNAL chip1_i_r0, chip1_i_r1:			STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL totVal1_i_r0, totVal1_i_r1:		STD_LOGIC_VECTOR(3 downto 0);		
	
	signal calib_mode_i			: STD_LOGIC := '0';
	
	-- SSRAM Histogrammer Signals
	
	SIGNAL hitEnable0_r, hitEnable0_a, hitEnable0_b, hitEnable0: STD_LOGIC;
	SIGNAL col0_r, col0_a, col0_b,			col0: STD_LOGIC_VECTOR(6 downto 0);
	SIGNAL row0_r, row0_a, row0_b,			row0: STD_LOGIC_VECTOR(8 downto 0);
	SIGNAL chip0_r,	chip0_a, chip0_b,	chip0: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL totVal0_r, totVal0_a, totVal0_b,		totVal0: STD_LOGIC_VECTOR(3 downto 0);
	
	SIGNAL hitEnable1_r, hitEnable1_a, hitEnable1_b, hitEnable1: STD_LOGIC;
	SIGNAL col1_r, col1_a, col1_b,	col1: STD_LOGIC_VECTOR(6 downto 0);
	SIGNAL row1_r, row1_a, row1_b,			row1: STD_LOGIC_VECTOR(8 downto 0);
	SIGNAL chip1_r,	chip1_a, chip1_b,	chip1: STD_LOGIC_VECTOR(2 downto 0);
	SIGNAL totVal1_r, totVal1_a, totVal1_b,		totVal1: STD_LOGIC_VECTOR(3 downto 0);
	
	
	signal sram1_inclk  		: STD_LOGIC;
	signal sram2_inclk  		: STD_LOGIC;
   signal rfd0, 				rfd1 				: STD_LOGIC := '0';
   signal rfr0, 				rfr1				: STD_LOGIC := '0';
	signal roc0, 				roc1 				: STD_LOGIC := '0';
   signal mode0, 			mode1 			: STD_LOGIC_VECTOR(readoutModeBits - 1 downto 0) := (others => '0');
	signal histo0,  	 	histo1		 	: STD_LOGIC_VECTOR(31 downto 0);
	signal histValid0, 	histValid1	: STD_LOGIC;

	-- MicroBlaze System Signals
	signal cpurst 	: STD_LOGIC;
	signal localRst :  std_logic;
	signal rstPulse: std_logic;
	signal locked_ext :  std_logic;

	-- rod bus and local registers ---------
	signal axi_epc_0_PRH_CS_n_pin : std_logic;
	signal axi_epc_0_PRH_Addr_pin : std_logic_vector(21 downto 0);
	signal axi_epc_0_PRH_RNW_pin : std_logic;
	signal axi_epc_0_PRH_Data_O_pin : std_logic_vector(31 downto 0):= (others => '0');
	signal axi_epc_0_PRH_Rdy_pin : std_logic;
	signal axi_epc_0_PRH_Data_I_pin : std_logic_vector(31 downto 0) := (others => '0');
	
	-- axi spi signals
	signal axi_spi_0_SCK_pin :  std_logic;
	signal axi_spi_0_MISO_pin :  std_logic := '0';
	signal axi_spi_0_MOSI_pin :  std_logic;
	signal axi_spi_0_SS_pin :  std_logic_vector(3 downto 0);     
	
	--------------------
	-- addresses are byte address!
	constant RANGE_SIZE: integer:= 16; -- we use lower 16 bit for regs, upper 16 bits for ram
	constant DESIGN_ID_ADDR: std_logic_vector(15 downto 0) 	:= X"0000";
	constant CTL_REG_ADDR: std_logic_vector(15 downto 0) 		:= X"0004";
	constant STAT_REG_ADDR: std_logic_vector(15 downto 0) 	:= X"0008";
	constant BOC_TEST_ADDR: std_logic_vector(15 downto 0) 	:= X"000c";
	constant BUS_TX_ADDR:  std_logic_vector(15 downto 0) 		:= X"0010";
	constant HIST_0_TEST_ADDR: std_logic_vector(15 downto 0) := X"0018";
	constant HIST_1_TEST_ADDR: std_logic_vector(15 downto 0) := X"001c";
	constant HIST_0_CTL_ADDR:  std_logic_vector(15 downto 0) := X"0020";
	constant HIST_1_CTL_ADDR:  std_logic_vector(15 downto 0) := X"0024";	
	
	constant ctlBocEnableBit: integer := 0;	-- select real boc input instead of locally attached FE-I4
	constant ctlBocTestBit: integer := ctlBocEnableBit + 1;		-- boc test input mux. overwrites real boc

	constant ctlHistMode0Base: integer := ctlBocTestBit + 1;  -- histogrammer mode	
	constant ctlHistMode1Base: integer := ctlHistMode0Base + readoutModeBits;  -- histogrammer mode	

	constant ctlHistMux0Bit: integer := ctlHistMode1Base + readoutModeBits;  -- histogrammer input mux
	constant ctlHistMux1Bit: integer := ctlHistMux0Bit + 1;

	-- top of slave read-only bit section.
	constant ctlBusRstBit: integer :=  31;		-- main reset from master. read-only for slave
	constant ctlMasterModeBit: integer := ctlBusRstBit - 1;	-- Taken from master control register bit 31. 
															-- Read-only for slave processor. If set (by master) overrides local bits
	constant ctlMasterBase: integer := ctlMasterModeBit;	-- start of slave read-only bit section.


	constant statSimBit: integer := 31;
	constant statExtRamBit: integer := 30;

	constant statDma0RdyBit: 	integer :=  29;
	constant statDma1RdyBit: 	integer :=  28;
	constant statHistoRfr0Bit: integer :=  27;
	constant statHistoRfr1Bit: integer :=  26;
	constant statHistoRfd0Bit: integer :=  25;
	constant statHistoRfd1Bit: integer :=  24;
	constant statHistoRoc0Bit: integer :=  23;
	constant statHistoRoc1Bit: integer :=  22;

	constant statLastBit: integer :=  statHistoRoc1Bit; 

	-- lower 20 bits of status are used for DMOV test data

	signal localCtlReg:  std_logic_vector(31 downto 0); 
	signal effectiveCtlReg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal localStatReg:  std_logic_vector(31 downto 0); 
	signal localHistCtlReg0:  std_logic_vector(31 downto 0) := (others => '0');
	signal localHistCtlReg1:  std_logic_vector(31 downto 0) := (others => '0');
	signal localBusTxReg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal bocDelayPulse:  std_logic_vector(31 downto 0) := (others => '0');
--
--
--	--------------------
--	-- rod bus
--	signal busdin : std_logic_vector(31 downto 0) := (others => '0');
--	signal busaout : std_logic_vector(31 downto 0);
--	signal busdout : std_logic_vector(31 downto 0);
	signal busRamData : std_logic_vector(31 downto 0);
--	signal buswr, busRamWr : std_logic;
	signal busRamWr : std_logic;
--	signal busrd : std_logic;
--
--
	signal busRamAddr: std_logic_vector(31 downto 0);
	constant gndBits:  std_logic_vector(31 downto 0) := (others => '0');

	signal busCtlReg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busStatReg:  std_logic_vector(31 downto 0);

	signal busy_reg_out:  std_logic_vector(31 downto 0) := (others => '0'); 
   signal busy_status_reg:  std_logic_vector(28 downto 0) := (others => '0');
   signal busy_mask_reg:  std_logic_vector(28 downto 0) := (others => '0'); 
   signal force_input_reg:  std_logic_vector(28 downto 0) := (others => '0'); 
	signal busy_Slink0LffHist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Slink0LdownHist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Slink1LffHist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Slink1LdownHist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Efb1Stts0Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Efb1Stts1Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Efb2Stts0Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Efb2Stts1Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Router0Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
	signal busy_Router1Hist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
   signal busy_HistFull0_Hist_reg:  std_logic_vector(31 downto 0) := (others => '0');
   signal busy_HistFull1_Hist_reg:  std_logic_vector(31 downto 0) := (others => '0');
   signal busy_HistOverrun0_Hist_reg:  std_logic_vector(31 downto 0) := (others => '0');
   signal busy_HistOverrun1_Hist_reg:  std_logic_vector(31 downto 0) := (others => '0');
	signal busy_OutHist_reg:  std_logic_vector(31 downto 0) := (others => '0'); 
   signal busy_Fmt0FF0_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt0FF1_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt0FF2_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt0FF3_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt1FF0_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt1FF1_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt1FF2_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt1FF3_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt2FF0_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt2FF1_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt2FF2_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt2FF3_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt3FF0_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt3FF1_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt3FF2_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
   signal busy_Fmt3FF3_Hist_reg: std_logic_vector(31 downto 0) := (others => '0');
	
   --Error Register Signals 
   signal link0_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link1_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0'); 
   signal link2_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link3_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link4_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link5_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0'); 
   signal link6_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link7_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link8_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link9_err_reg_i      : std_logic_vector(31 downto 0) := (others => '0');
   signal link10_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal link11_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal link12_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal link13_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal link14_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal link15_err_reg_i     : std_logic_vector(31 downto 0) := (others => '0');
   signal header_err_regA      : std_logic_vector(31 downto 0) := (others => '0');
   signal header_err_regB      : std_logic_vector(31 downto 0) := (others => '0');
   signal header_err_regS      : unsigned        (31 downto 0) := (others => '0');
   signal header_err_reg       : std_logic_vector(31 downto 0) := (others => '0');
   signal trailer_err_regA     : std_logic_vector(31 downto 0) := (others => '0');
   signal trailer_err_regB     : std_logic_vector(31 downto 0) := (others => '0');
   signal trailer_err_regS     : unsigned        (31 downto 0) := (others => '0');
   signal trailer_err_reg      : std_logic_vector(31 downto 0) := (others => '0');
   signal rd_timeout_err_regA  : std_logic_vector(31 downto 0) := (others => '0');
   signal rd_timeout_err_regB  : std_logic_vector(31 downto 0) := (others => '0');
   signal rd_timeout_err_regS  : unsigned        (31 downto 0) := (others => '0');
   signal rd_timeout_err_reg   : std_logic_vector(31 downto 0) := (others => '0');
   signal ht_limit_err_regA    : std_logic_vector(31 downto 0) := (others => '0');
   signal ht_limit_err_regB    : std_logic_vector(31 downto 0) := (others => '0');
   signal ht_limit_err_regS    : unsigned        (31 downto 0) := (others => '0');
   signal ht_limit_err_reg     : std_logic_vector(31 downto 0) := (others => '0');
   signal row_col_err_regA     : std_logic_vector(31 downto 0) := (others => '0');
   signal row_col_err_regB     : std_logic_vector(31 downto 0) := (others => '0');
   signal row_col_err_regS     : unsigned        (31 downto 0) := (others => '0');
   signal row_col_err_reg      : std_logic_vector(31 downto 0) := (others => '0');
   signal l1_id_err_regA       : std_logic_vector(31 downto 0) := (others => '0');
   signal l1_id_err_regB       : std_logic_vector(31 downto 0) := (others => '0');
   signal l1_id_err_regS       : unsigned        (31 downto 0) := (others => '0');
   signal l1_id_err_reg        : std_logic_vector(31 downto 0) := (others => '0');
   signal bc_id_err_regA       : std_logic_vector(31 downto 0) := (others => '0');
   signal bc_id_err_regB       : std_logic_vector(31 downto 0) := (others => '0');
   signal bc_id_err_regS       : unsigned        (31 downto 0) := (others => '0');
   signal bc_id_err_reg        : std_logic_vector(31 downto 0) := (others => '0');
	
	signal toggle: std_logic := '0';	
	
		-- dma test
	signal dmovData:  std_logic_vector(31 downto 0); -- dummy: counter
	signal testBocData: std_logic_vector(10 downto 0);
	signal testBocEnable: std_logic:= '0';
	signal bocTestWr, bocTestEmpty: std_logic;

	-- boc io synchronisation
	component bocIoSync
	Generic (master : boolean);
	Port (
		dataIn : in  STD_LOGIC;
		dataOut : out  STD_LOGIC;
		clk : in  STD_LOGIC;	
		clk4x : in  STD_LOGIC;	
		slotOut: out std_logic_vector(1 downto 0);
		slotIn:  in std_logic_vector(1 downto 0);
		reset : in std_logic		--! reset
		);
	end component;
	
	signal bocDelaySlotA: std_logic_vector(1 downto 0);
	signal bocDelaySlotB: std_logic_vector(1 downto 0);

	-- the GIT hash key
	COMPONENT gitId
	PORT(
		reset : IN std_logic;
		read : IN std_logic;          
		id : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;
	
	signal gitIdReset : std_logic;
	signal gitIdRead: std_logic;
	signal gitIdByte: std_logic_vector(7 downto 0);

		-- Histo DMA signals
	-- Histo DMA signals
   signal axi_histo0_dma_s_axis_s2mm_tready_pin : std_logic := '0';
   signal axi_histo0_dma_s_axis_s2mm_tlast_pin : std_logic := '0';
   signal axi_histo0_dma_s_axis_s2mm_tkeep_pin : std_logic_vector(3 downto 0) := (others => '1');
   signal axi_histo0_dma_s_axis_s2mm_tdata_pin : std_logic_vector(31 downto 0) := (others => '0');
   signal axi_histo0_dma_s_axis_s2mm_tvalid_pin : std_logic := '0';

   signal axi_histo1_dma_s_axis_s2mm_tready_pin : std_logic := '0';
   signal axi_histo1_dma_s_axis_s2mm_tlast_pin : std_logic := '0';
   signal axi_histo1_dma_s_axis_s2mm_tkeep_pin : std_logic_vector(3 downto 0) := (others => '1');
   signal axi_histo1_dma_s_axis_s2mm_tdata_pin : std_logic_vector(31 downto 0) := (others => '0');
   signal axi_histo1_dma_s_axis_s2mm_tvalid_pin : std_logic := '0';
	
	signal hist0TestEnable, hist1TestEnable : std_logic;
	
	-- S-Link Signals
	signal slink0_data_rod2boc_i		: STD_LOGIC_VECTOR(31 downto 0); 
	signal slink1_data_rod2boc_i		: STD_LOGIC_VECTOR(31 downto 0); 
	
	signal slink0_utest_iob		: STD_LOGIC;
	signal slink0_utest_i		: STD_LOGIC;
	signal slink0_ureset_iob	: STD_LOGIC;
	signal slink0_ureset_i		: STD_LOGIC;
	signal slink0_uctrl_iob		: STD_LOGIC;
	signal slink0_uctrl_i		: STD_LOGIC;
	signal slink0_we_iob			: STD_LOGIC;
	signal slink0_we_i			: STD_LOGIC;
	signal slink0_lff_iob		: STD_LOGIC;
	signal slink0_lff_i			: STD_LOGIC;
	signal slink0_ldown_iob		: STD_LOGIC;
	signal slink0_ldown_i		: STD_LOGIC;
	
	attribute IOB of slink0_utest_iob	: signal is "TRUE";
	attribute IOB of slink0_ureset_iob  : signal is "TRUE";
	attribute IOB of slink0_uctrl_iob	: signal is "TRUE";
	attribute IOB of slink0_we_iob		: signal is "TRUE";
	attribute IOB of slink0_lff_iob		: signal is "TRUE";
	attribute IOB of slink0_ldown_iob	: signal is "TRUE";	
	
	signal slink1_utest_iob		: STD_LOGIC;
	signal slink1_utest_i		: STD_LOGIC;
	signal slink1_ureset_iob	:	STD_LOGIC;
	signal slink1_ureset_i		:	STD_LOGIC;
	signal slink1_uctrl_iob		:	STD_LOGIC;
	signal slink1_uctrl_i		:	STD_LOGIC;
	signal slink1_we_iob			:	STD_LOGIC;
	signal slink1_we_i			:	STD_LOGIC;
	signal slink1_lff_iob		:	STD_LOGIC;
	signal slink1_lff_i			:	STD_LOGIC;
	signal slink1_ldown_iob		:	STD_LOGIC;
	signal slink1_ldown_i		:	STD_LOGIC;
	
	attribute IOB of slink1_utest_iob	: signal is "TRUE";
	attribute IOB of slink1_ureset_iob: signal is "TRUE";
	attribute IOB of slink1_uctrl_iob	: signal is "TRUE";
	attribute IOB of slink1_we_iob		: signal is "TRUE";
	attribute IOB of slink1_lff_iob		: signal is "TRUE";
	attribute IOB of slink1_ldown_iob	: signal is "TRUE";	

------------------------------------
--            COMPONENTS
------------------------------------	
	file mappingFile	: text;		signal dumpIt	: integer := 1;
	file mappingFile2	: text;		signal dumpIt2: integer := 1;
	constant designVersion: std_logic_vector(15 downto 0) := X"0004";
	constant designRevision: std_logic_vector(15 downto 0) := X"0001";
	constant designId: std_logic_vector(31 downto 0) := designVersion & designRevision;	
	
--	-- !!!!!!!!!!!!!! run "make date" prior to compile the VHDL sources when using ISE !!!!!!!!!!!!

COMPONENT system IS
    PORT(
      rzq : inout STD_LOGIC;
      mcbx_dram_we_n : out STD_LOGIC;
      mcbx_dram_udqs_n : inout STD_LOGIC;
      mcbx_dram_udqs : inout STD_LOGIC;
      mcbx_dram_udm : out STD_LOGIC;
      mcbx_dram_ras_n : out STD_LOGIC;
      mcbx_dram_odt : out STD_LOGIC;
      mcbx_dram_ldm : out STD_LOGIC;
      mcbx_dram_dqs_n : inout STD_LOGIC;
      mcbx_dram_dqs : inout STD_LOGIC;
		mcbx_dram_dq : inout STD_LOGIC_VECTOR(15 downto 0);
      mcbx_dram_clk_n : out STD_LOGIC;
      mcbx_dram_clk : out STD_LOGIC;
      mcbx_dram_cke : out STD_LOGIC;
      mcbx_dram_cas_n : out STD_LOGIC;
      mcbx_dram_ba : out STD_LOGIC_VECTOR(2 downto 0);
      mcbx_dram_addr : out STD_LOGIC_VECTOR(13 downto 0);
      RS232_Uart_1_sout : out STD_LOGIC;
      RS232_Uart_1_sin : in STD_LOGIC;
      RESET_in : in STD_LOGIC;
      ETHERNET_TX_ER : out STD_LOGIC;
      ETHERNET_TX_EN : out STD_LOGIC;
      ETHERNET_TX_CLK : out STD_LOGIC;
      ETHERNET_TXD : out STD_LOGIC_VECTOR(7 downto 0);
      ETHERNET_RX_ER : in STD_LOGIC;
      ETHERNET_RX_DV : in STD_LOGIC;
      ETHERNET_RX_CLK : in STD_LOGIC;
      ETHERNET_RXD : in STD_LOGIC_VECTOR(7 downto 0);
      ETHERNET_PHY_RST_N : out STD_LOGIC;
      ETHERNET_MII_TX_CLK : in STD_LOGIC;
      ETHERNET_MDIO : inout STD_LOGIC;
      ETHERNET_MDC : out STD_LOGIC;
      cpuclk : out STD_LOGIC;
      cpuclk2x : out STD_LOGIC;
      cpuclk_in : in STD_LOGIC;
      cpurst : out STD_LOGIC;
      axi_epc_0_PRH_CS_n_pin : out STD_LOGIC;
      axi_epc_0_PRH_Addr_pin : out STD_LOGIC_VECTOR(21 downto 0);
      axi_epc_0_PRH_RNW_pin : out STD_LOGIC;
      axi_epc_0_PRH_Rdy_pin : in STD_LOGIC;
      axi_epc_0_PRH_Data_I_pin : in STD_LOGIC_VECTOR(31 downto 0);
      axi_epc_0_PRH_Data_O_pin : out STD_LOGIC_VECTOR(31 downto 0);

		axi_spi_0_SCK_O_pin : INOUT std_logic;
		axi_spi_0_MISO_I_pin : INOUT std_logic;
		axi_spi_0_MOSI_O_pin : INOUT std_logic;
		axi_spi_0_SS_O_pin : INOUT std_logic_vector(3 downto 0);      

		axi_histo0_dma_s_axis_s2mm_tready_pin : out std_logic;
		axi_histo0_dma_s_axis_s2mm_tlast_pin : in std_logic;
		axi_histo0_dma_s_axis_s2mm_tkeep_pin : in std_logic_vector(3 downto 0);
		axi_histo0_dma_s_axis_s2mm_tdata_pin : in std_logic_vector(31 downto 0);
		axi_histo0_dma_s_axis_s2mm_tvalid_pin : in std_logic;
		
		axi_histo1_dma_s_axis_s2mm_tready_pin : out std_logic;
		axi_histo1_dma_s_axis_s2mm_tlast_pin : IN std_logic;
		axi_histo1_dma_s_axis_s2mm_tkeep_pin : IN std_logic_vector(3 downto 0);
		axi_histo1_dma_s_axis_s2mm_tdata_pin : IN std_logic_vector(31 downto 0);
		axi_histo1_dma_s_axis_s2mm_tvalid_pin : IN std_logic    
   );
  END COMPONENT;

  attribute BOX_TYPE : STRING;
  attribute BOX_TYPE of system : component is "user_black_box";


	COMPONENT bocTestFifo
	  PORT(
		 rst 	: IN   STD_LOGIC;
		 wr_clk : IN   STD_LOGIC;
		 rd_clk : IN   STD_LOGIC;
		 din 	: IN STD_LOGIC_VECTOR(10 downto 0);
		 wr_en : IN   STD_LOGIC;
		 rd_en : IN   STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(10 downto 0);
		 full : OUT  STD_LOGIC;
		 empty : OUT  STD_LOGIC
	  );
	END COMPONENT;
	
--	COMPONENT dcm_40MHz
--		PORT(
--			CLK_IN40_P	: IN   STD_LOGIC;
--			CLK_IN40_N	: IN   STD_LOGIC;
--			CLK_OUT40		: OUT  STD_LOGIC;
--			CLK_OUT80		: OUT  STD_LOGIC;
--			CLK_OUT160	: OUT  STD_LOGIC;
--			RESET				: IN   STD_LOGIC;
--			LOCKED			: OUT  STD_LOGIC
--		);
--	END COMPONENT;

	COMPONENT dcm_40MHz
		PORT(
			CLK_IN40_P         : in     std_logic;
			CLK_IN40_N         : in     std_logic;
			CLK_OUT40          : out    std_logic;
			CLK_OUT80_PS240    : out    std_logic;
			CLK_OUT80          : out    std_logic;
			RESET              : in     std_logic;
			LOCKED             : out    std_logic
		 );
	END COMPONENT;
	
	--COMPONENT bocClk40from100

	COMPONENT inmem_fifo1024
		PORT (
			rst 		: IN  STD_LOGIC;
			wr_clk 	: IN  STD_LOGIC;
			rd_clk 	: IN  STD_LOGIC;
			din 		: IN  STD_LOGIC_VECTOR(10 downto 0);
			wr_en 	: IN  STD_LOGIC;
			rd_en 	: IN  STD_LOGIC;
			dout 		: OUT STD_LOGIC_VECTOR(10 downto 0);
			full 		: OUT STD_LOGIC;
			empty 	: OUT STD_LOGIC
		);
	END COMPONENT;
	
	COMPONENT ibl_slv_register
		PORT(
			clk                     	: in  STD_LOGIC;
			rst                     	: in  STD_LOGIC;
			-- ROD Bus Control and Data Signals
			rod_bus_strb_in         	: in  STD_LOGIC;
			rod_bus_addr            	: in  STD_LOGIC_VECTOR(15 downto 0);
			rod_bus_rnw         	      : in  STD_LOGIC;
			rod_bus_hwsb_in      	  	: in  STD_LOGIC;
			rod_bus_ds_in        	  	: in  STD_LOGIC;
			rod_bus_data_in         	: in  STD_LOGIC_VECTOR(15 downto 0);
			rod_bus_data_out        	: out STD_LOGIC_VECTOR(15 downto 0);
			rod_bus_ack_out         	: out STD_LOGIC;
			slave_id                	: out STD_LOGIC;
			-- MicroBlaze signals
			busCtlReg			      	: out STD_LOGIC_VECTOR(31 downto 0);
			busStatReg			      	: in  STD_LOGIC_VECTOR(31 downto 0);
			-- busy signals
			busy_reg_out				   : out STD_LOGIC_VECTOR(31 downto 0);	
         busy_mask_reg				   : out STD_LOGIC_VECTOR(28 downto 0);
         force_input_reg			   : out STD_LOGIC_VECTOR(28 downto 0);
			busy_status_reg			   : in  STD_LOGIC_VECTOR(28 downto 0);			
			busy_Slink0LffHist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Slink0LdownHist_reg	: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Slink1LffHist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Slink1LdownHist_reg	: in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt0FF0_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt0FF1_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt0FF2_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt0FF3_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt1FF0_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt1FF1_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt1FF2_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt1FF3_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt2FF0_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt2FF1_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt2FF2_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt2FF3_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt3FF0_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt3FF1_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt3FF2_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
         busy_Fmt3FF3_Hist_reg      : in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Efb1Stts0Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Efb1Stts1Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Efb2Stts0Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Efb2Stts1Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Router0Hist_reg			: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_Router1Hist_reg			: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_HistFull0Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_HistFull1Hist_reg		: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_HistOverrun0Hist_reg	: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_HistOverrun1Hist_reg	: in  STD_LOGIC_VECTOR(31 downto 0);
			busy_OutHist_reg			   : in  STD_LOGIC_VECTOR(31 downto 0);				
         hitdisccnfg                : out STD_LOGIC_VECTOR(31 downto 0);
			-- Formatter Signals
			fmt_link_enabled		    	: out STD_LOGIC_VECTOR(15 downto 0);
         fmt_trig_cnt               : out STD_LOGIC_VECTOR( 3 downto 0);
			fmt_timeout_limit			 	: out STD_LOGIC_VECTOR(31 downto 0);
			fmt_data_overflow_limit  	: out STD_LOGIC_VECTOR(15 downto 0);
			fmt_header_trailer_limit 	: out STD_LOGIC_VECTOR(31 downto 0);
			fmt_rod_busy_limit	 	 	: out STD_LOGIC_VECTOR( 9 downto 0);
         fmt_trailer_limit			 	: out STD_LOGIC_VECTOR(31 downto 0);
			fmt_header_trailer_error 	: in  STD_LOGIC_VECTOR(15 downto 0);
			fmt_time_out_error       	: in  STD_LOGIC_VECTOR(15 downto 0);
			fmt_data_overflow_error  	: in  STD_LOGIC_VECTOR(15 downto 0);
			fmt_ctrl_reg   		      : out STD_LOGIC_VECTOR(31 downto 0); 
			fmt0_stts_reg 				   : in  STD_LOGIC_VECTOR(31 downto 0);
			fmt1_stts_reg 				   : in  STD_LOGIC_VECTOR(31 downto 0);
			fmt2_stts_reg 				   : in  STD_LOGIC_VECTOR(31 downto 0);
			fmt3_stts_reg 				   : in  STD_LOGIC_VECTOR(31 downto 0);
         fmt0Occupancy              : in  STD_LOGIC_VECTOR(39 downto 0);
         fmt1Occupancy              : in  STD_LOGIC_VECTOR(39 downto 0);
         fmt2Occupancy              : in  STD_LOGIC_VECTOR(39 downto 0);
         fmt3Occupancy              : in  STD_LOGIC_VECTOR(39 downto 0);
         formatter0_frames          : in  STD_LOGIC_VECTOR(15 downto 0);
         formatter1_frames          : in  STD_LOGIC_VECTOR(15 downto 0);
         formatter2_frames          : in  STD_LOGIC_VECTOR(15 downto 0);
         formatter3_frames          : in  STD_LOGIC_VECTOR(15 downto 0);
			-- Inmem Fifo Signals
			inmem_reset            	 	: out STD_LOGIC;
			inmem_select             	: out STD_LOGIC_VECTOR( 1 downto 0);
			inmem_rden					  	: out STD_LOGIC;
			inmem_data	    			 	: in  STD_LOGIC_VECTOR(11 downto 0);
			-- Interfacing with the efb err_detect_new blocks
			err_mask_wen1            	: out STD_LOGIC;
			err_mask_din1            	: out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout1           	: in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel1            	: out STD_LOGIC_VECTOR( 2 downto 0);     
			err_mask_wen2            	: out STD_LOGIC;
			err_mask_din2            	: out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout2           	: in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel2            	: out STD_LOGIC_VECTOR( 2 downto 0);
			err_mask_wen3            	: out STD_LOGIC;
			err_mask_din3            	: out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout3           	: in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel3            	: out STD_LOGIC_VECTOR( 2 downto 0);     
			err_mask_wen4            	: out STD_LOGIC;
			err_mask_din4            	: out STD_LOGIC_VECTOR(31 downto 0);
			err_mask_dout4           	: in  STD_LOGIC_VECTOR(31 downto 0);
			err_mask_sel4            	: out STD_LOGIC_VECTOR( 2 downto 0);
			-- Information going into efb gen_fragment block
			source_id                	: out STD_LOGIC_VECTOR(31 downto 0);
			format_version           	: out STD_LOGIC_VECTOR(31 downto 0);
			run_number               	: out STD_LOGIC_VECTOR(31 downto 0);
			-- output evtmem fifos signals
			efb1_out_fifo_rst        	: out STD_LOGIC;	 
			efb1_out_fifo1_empty_n   	: in  STD_LOGIC;
			efb1_out_fifo1_aempty_n  	: in  STD_LOGIC;
			efb1_out_fifo1_full_n    	: in  STD_LOGIC;
			efb1_out_fifo1_afull_n   	: in  STD_LOGIC;
			efb1_out_fifo1_play_done 	: in  STD_LOGIC;
			efb1_out_fifo2_empty_n   	: in  STD_LOGIC;
			efb1_out_fifo2_aempty_n  	: in  STD_LOGIC;
			efb1_out_fifo2_full_n    	: in  STD_LOGIC;
			efb1_out_fifo2_afull_n   	: in  STD_LOGIC;
         efb1_out_fifo2_play_done   : in  STD_LOGIC;
			efb2_out_fifo_rst        	: out STD_LOGIC;	 
			efb2_out_fifo1_empty_n   	: in  STD_LOGIC;
			efb2_out_fifo1_aempty_n  	: in  STD_LOGIC;
			efb2_out_fifo1_full_n    	: in  STD_LOGIC;
			efb2_out_fifo1_afull_n   	: in  STD_LOGIC;
			efb2_out_fifo1_play_done 	: in  STD_LOGIC;
			efb2_out_fifo2_empty_n   	: in  STD_LOGIC;
			efb2_out_fifo2_aempty_n  	: in  STD_LOGIC;
			efb2_out_fifo2_full_n    	: in  STD_LOGIC;
			efb2_out_fifo2_afull_n   	: in  STD_LOGIC;
         efb2_out_fifo2_play_done   : in  STD_LOGIC;
			efb1_misc_stts_reg       	: in  STD_LOGIC_VECTOR(30 downto 0);			
			efb2_misc_stts_reg       	: in  STD_LOGIC_VECTOR(30 downto 0);
			efb1_ev_header_data      	: in  STD_LOGIC_VECTOR(15 downto 0);
			efb2_ev_header_data      	: in  STD_LOGIC_VECTOR(15 downto 0);
			efb1_ev_fifo_data1       	: in  STD_LOGIC_VECTOR(34 downto 0);
			efb1_ev_fifo_data2       	: in  STD_LOGIC_VECTOR(34 downto 0);
			efb2_ev_fifo_data1       	: in  STD_LOGIC_VECTOR(34 downto 0);
			efb2_ev_fifo_data2       	: in  STD_LOGIC_VECTOR(34 downto 0);
			efb_mask_bcid_error      	: out STD_LOGIC;
			efb_mask_l1id_error      	: out STD_LOGIC;
			efb_bcid_offset          	: out STD_LOGIC_VECTOR( 9 downto 0);
			efb_grp_evt_enable       	: out STD_LOGIC_VECTOR( 1 downto 0);
			efb1_event_count         	: in  STD_LOGIC_VECTOR(31 downto 0);
			efb2_event_count         	: in  STD_LOGIC_VECTOR(31 downto 0);
			mask_boc_clock_err       	: out STD_LOGIC;
			mask_tim_clock_err       	: out STD_LOGIC;
			efb1_l1id_fifo_data		 	: in  STD_LOGIC_VECTOR(39 downto 0);
			efb1_l1id_fifo_addr		 	: out STD_LOGIC_VECTOR( 3 downto 0);
			efb2_l1id_fifo_data	    	: in  STD_LOGIC_VECTOR(39 downto 0);
			efb2_l1id_fifo_addr		 	: out STD_LOGIC_VECTOR( 3 downto 0);
			efb_enable_l1_trap       	: out STD_LOGIC_VECTOR( 1 downto 0);
			efb_l1_trap_full_in      	: in  STD_LOGIC_VECTOR( 1 downto 0);
			efb1_latched_l1id0       	: in  STD_LOGIC_VECTOR(12 downto 0);
			efb1_latched_l1id1       	: in  STD_LOGIC_VECTOR(12 downto 0);
			efb1_latched_bcid0       	: in  STD_LOGIC_VECTOR( 9 downto 0);
			efb1_latched_bcid1       	: in  STD_LOGIC_VECTOR( 9 downto 0);
			efb2_latched_l1id0       	: in  STD_LOGIC_VECTOR(12 downto 0);
			efb2_latched_l1id1       	: in  STD_LOGIC_VECTOR(12 downto 0);
			efb2_latched_bcid0       	: in  STD_LOGIC_VECTOR( 9 downto 0);
			efb2_latched_bcid1       	: in  STD_LOGIC_VECTOR( 9 downto 0);						
			tim_bcid_in_rol          	: out STD_LOGIC;
			bcid_rollover_select     	: out STD_LOGIC;
			calib_mode						: out	STD_LOGIC;
         --Error Registers
         link0_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link1_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link2_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link3_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link4_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link5_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link6_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link7_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link8_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link9_errors_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         link10_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0);
         link11_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
         link12_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
         link13_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
         link14_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
         link15_errors_reg          : in  STD_LOGIC_VECTOR(31 downto 0); 
         header_err_reg             : in  STD_LOGIC_VECTOR(31 downto 0);     
         trailer_err_reg            : in  STD_LOGIC_VECTOR(31 downto 0); 
         rd_timeout_err_reg         : in  STD_LOGIC_VECTOR(31 downto 0); 
         ht_limit_err_reg           : in  STD_LOGIC_VECTOR(31 downto 0); 
         row_col_err_reg            : in  STD_LOGIC_VECTOR(31 downto 0); 
         l1_id_err_reg              : in  STD_LOGIC_VECTOR(31 downto 0); 
         bc_id_err_reg              : in  STD_LOGIC_VECTOR(31 downto 0)
		);
	END COMPONENT;

	COMPONENT ibl_halfSlave
		PORT(
			clk40   				    	: in  STD_LOGIC;
			clk80   				    	: in  STD_LOGIC;
			clk100                  : in  STD_LOGIC;
			reset                  	: in  STD_LOGIC;                                
			efb_id						: in  STD_LOGIC_VECTOR(1 downto 0);
			-- Signals From BOC
			data2rod1 					: in  STD_LOGIC_VECTOR(7 downto 0); 
			addr2rod1 					: in  STD_LOGIC_VECTOR(1 downto 0); 
			valid2rod1 					: in  STD_LOGIC;
			ctl2rod1 					: in  STD_LOGIC;
			data2rod2 					: in  STD_LOGIC_VECTOR(7 downto 0); 
			addr2rod2 					: in  STD_LOGIC_VECTOR(1 downto 0); 
			valid2rod2 					: in  STD_LOGIC;
			ctl2rod2 					: in  STD_LOGIC;
			fmt_fifo_busy           : out STD_LOGIC_VECTOR(7 downto 0); 
         fe_cmdpulse             : in  STD_LOGIC;
			-- Mode Bits from ROD Master
			mode_bits_in            : in  STD_LOGIC_VECTOR(11 downto 0);
			modebits_fifo_wen_n_in  : in  STD_LOGIC;
			modebits_fifo_rst_n_in  : in  STD_LOGIC;
			modebits_fifo_ef_n_out  : out STD_LOGIC; 
			modebits_fifo_ff_n_out  : out STD_LOGIC;
			-- Event Data from ROD Master
			ev_data_from_ppc       	: in  STD_LOGIC_VECTOR(15 downto 0);
			ev_data_wen_n          	: in  STD_LOGIC;
			ev_data_rst_n          	: in  STD_LOGIC;     
			ev_data_almost_full_n  	: out STD_LOGIC;
			ev_data_ready          	: out STD_LOGIC; -- ev_data_empty, ready for more
			-- HW DEBUG
		   fifocount_out           : out STD_LOGIC_VECTOR( 9 DOWNTO 0);
			ev_id_fifo_empty_error 	: out STD_LOGIC;
			-- Register Interface Signals
			fmt0_stts_reg				: out STD_LOGIC_VECTOR(31 downto 0);
			fmt1_stts_reg				: out STD_LOGIC_VECTOR(31 downto 0);
         fmt0Occupancy           : out STD_LOGIC_VECTOR(39 downto 0);
         fmt1Occupancy           : out STD_LOGIC_VECTOR(39 downto 0);
         fmt0_1_ht_error         : out STD_LOGIC_VECTOR( 7 downto 0);
         fmt0_1_timeout_error    : out STD_LOGIC_VECTOR( 7 downto 0);
			fmt_link_enabled			: in  STD_LOGIC_VECTOR( 7 downto 0);
         fmt_trig_cnt            : in  STD_LOGIC_VECTOR( 3 downto 0);
         hitdisccnfg             : in  STD_LOGIC_VECTOR(15 downto 0);
--		fmt_exp_mode_en				: in  STD_LOGIC_VECTOR(7 downto 0);
			fmt_timeout_limit			: in  STD_LOGIC_VECTOR(31 downto 0);
			fmt_data_overflow_limit	: in  STD_LOGIC_VECTOR(15 downto 0);
			fmt_header_trailer_limit: in  STD_LOGIC_VECTOR(31 downto 0);
			fmt_rod_busy_limit		: in  STD_LOGIC_VECTOR( 9 downto 0);
         fmt_trailer_limit			: in  STD_LOGIC_VECTOR(31 downto 0);
         formatter0_frames       : out STD_LOGIC_VECTOR(15 downto 0);
         formatter1_frames       : out STD_LOGIC_VECTOR(15 downto 0);
--		fmt_pxl_link_l1a_cnt		: in STD_LOGIC_VECTOR(15 downto 0);
			efb_err_mask_wen1       : in  STD_LOGIC;
			efb_err_mask_din1		   : in  STD_LOGIC_VECTOR(31 downto 0);
			efb_err_mask_dout1	   : out STD_LOGIC_VECTOR(31 downto 0);       
			efb_err_mask_sel1		   : in  STD_LOGIC_VECTOR( 2 downto 0);             
			efb_err_mask_wen2		   : in  STD_LOGIC;       
			efb_err_mask_din2		   : in  STD_LOGIC_VECTOR(31 downto 0);       
			efb_err_mask_dout2	   : out STD_LOGIC_VECTOR(31 downto 0);       
			efb_err_mask_sel2		   : in  STD_LOGIC_VECTOR( 2 downto 0);
			efb_source_id           : in  STD_LOGIC_VECTOR(31 downto 0);
			efb_format_version		: in  STD_LOGIC_VECTOR(31 downto 0);
			efb_run_number          : in  STD_LOGIC_VECTOR(31 downto 0);
			efb_out_fifo_rst        : in  STD_LOGIC;	 
			efb_out_fifo1_empty_n   : out STD_LOGIC;
			efb_out_fifo1_aempty_n  : out STD_LOGIC;
			efb_out_fifo1_full_n    : out STD_LOGIC;
			efb_out_fifo1_afull_n   : out STD_LOGIC;
			efb_out_fifo1_play_done : out STD_LOGIC;
			efb_out_fifo2_empty_n   : out STD_LOGIC;
			efb_out_fifo2_aempty_n  : out STD_LOGIC;
			efb_out_fifo2_full_n    : out STD_LOGIC;
			efb_out_fifo2_afull_n   : out STD_LOGIC;
			efb_out_fifo2_play_done : out STD_LOGIC;
			efb_misc_stts_reg       : out STD_LOGIC_VECTOR(30 downto 0);
			efb_ev_header_data      : out STD_LOGIC_VECTOR(15 downto 0);
			efb_ev_fifo_data1       : out STD_LOGIC_VECTOR(34 downto 0);
			efb_ev_fifo_data2       : out STD_LOGIC_VECTOR(34 downto 0);
			efb_mask_bcid_error     : in  STD_LOGIC;
			efb_mask_l1id_error     : in  STD_LOGIC;
			efb_bcid_offset         : in  STD_LOGIC_VECTOR( 9 downto 0);
			efb_grp_evt_enable      : in  STD_LOGIC;
			efb_event_count         : out STD_LOGIC_VECTOR(31 downto 0);
			efb_mask_boc_clock_err  : in  STD_LOGIC;
			efb_mask_tim_clock_err  : in  STD_LOGIC;
			efb_l1id_fifo_data      : out STD_LOGIC_VECTOR(39 downto 0);
			efb_l1id_fifo_addr      : in  STD_LOGIC_VECTOR( 3 downto 0);
			efb_enable_l1_trap      : in  STD_LOGIC;
			efb_l1_trap_full_in     : out STD_LOGIC;
			efb_latched_l1id0       : out STD_LOGIC_VECTOR(12 downto 0);
			efb_latched_l1id1       : out STD_LOGIC_VECTOR(12 downto 0);
			efb_latched_bcid0       : out STD_LOGIC_VECTOR( 9 downto 0);
			efb_latched_bcid1       : out STD_LOGIC_VECTOR( 9 downto 0);
			efb_tim_bcid_in_rol     : in  STD_LOGIC;
			efb_bcid_rollover_sel   : in  STD_LOGIC;
			calib_mode					: in  STD_LOGIC;
			router_busy					: out STD_LOGIC;
			-- S-Link Interface
			slink_utest					: out STD_LOGIC;
			slink_ureset         	: out STD_LOGIC; 
			slink_uctrl           	: out STD_LOGIC; 
			slink_lff             	: in  STD_LOGIC; 
			slink_ldown           	: in  STD_LOGIC; 
			slink_data_out         	: out STD_LOGIC_VECTOR(31 downto 0);
			slink_data_wen        	: out STD_LOGIC;		
			-- Data to Histogrammer
			histo_rfd					: in  STD_LOGIC;
			chip                    : out STD_LOGIC_VECTOR (2 downto 0);
			hitValid                : out STD_LOGIC;
			row         				: out STD_LOGIC_VECTOR (8 downto 0);
			col				         : out STD_LOGIC_VECTOR (6 downto 0);
			totVal   			      : out STD_LOGIC_VECTOR (3 downto 0);
         histoFull               : out STD_LOGIC;
         histoOverrun            : out STD_LOGIC;
			-- HW DEBUGGING FOR CHIPSCOPE
			fmt0_valid_to_efb   				 : out STD_LOGIC;  
			fmt0_data_to_efb					 : out STD_LOGIC_VECTOR(34 downto 0);      
			fmt0_boe_to_efb					 : out STD_LOGIC;                 
			fmt0_eoe_to_efb			: out STD_LOGIC;
         link0_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link1_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link2_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link3_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link4_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link5_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link6_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         link7_errors_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         header_err_reg          : out STD_LOGIC_VECTOR(31 downto 0);     
         trailer_err_reg         : out STD_LOGIC_VECTOR(31 downto 0); 
         rd_timeout_err_reg      : out STD_LOGIC_VECTOR(31 downto 0); 
         ht_limit_err_reg        : out STD_LOGIC_VECTOR(31 downto 0); 
         row_col_err_reg         : out STD_LOGIC_VECTOR(31 downto 0); 
         l1_id_err_reg           : out STD_LOGIC_VECTOR(31 downto 0); 
         bc_id_err_reg           : out STD_LOGIC_VECTOR(31 downto 0)
		);
	END COMPONENT;
	
	COMPONENT rod_slave_busy
		PORT(
			clk40in                     : in   STD_LOGIC;
         slink0_lff_n                : in   STD_LOGIC;
         slink0_ldown_n              : in   STD_LOGIC;
         slink1_lff_n                : in   STD_LOGIC;
         slink1_ldown_n              : in   STD_LOGIC; 
         efb1_misc_stts_reg_i        : in   STD_LOGIC_VECTOR (1 downto 0);
			efb2_misc_stts_reg_i        : in   STD_LOGIC_VECTOR (1 downto 0);
         fmt0_ff                     : in   STD_LOGIC_VECTOR (3 downto 0);
         fmt1_ff                     : in   STD_LOGIC_VECTOR (3 downto 0);
         fmt2_ff                     : in   STD_LOGIC_VECTOR (3 downto 0);
         fmt3_ff                     : in   STD_LOGIC_VECTOR (3 downto 0);
			router0_busy_i              : in   STD_LOGIC; 
			router1_busy_i              : in   STD_LOGIC; 
			histoFull0                  : in   STD_LOGIC;
			histoFull1                  : in   STD_LOGIC;
			reset                       : in   STD_LOGIC;
			reset_hists                 : in   STD_LOGIC;
			histoOverrun0               : in   STD_LOGIC;
			histoOverrun1               : in   STD_LOGIC;
			busy_mask                   : in   STD_LOGIC_VECTOR (28 downto 0);
			force_input                 : in   STD_LOGIC_VECTOR (28 downto 0);
         rod_busy_n_out_p            : out  STD_LOGIC;
         current_busy_status         : out  STD_LOGIC_VECTOR (28 downto 0);
         slink0_lff_hist             : out  STD_LOGIC_VECTOR (15 downto 0);
         slink0_ldown_hist           : out  STD_LOGIC_VECTOR (15 downto 0);
         slink1_lff_hist             : out  STD_LOGIC_VECTOR (15 downto 0);
         slink1_ldown_hist           : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt0_ff_0_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt0_ff_1_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt0_ff_2_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt0_ff_3_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt1_ff_0_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt1_ff_1_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt1_ff_2_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt1_ff_3_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt2_ff_0_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt2_ff_1_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt2_ff_2_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt2_ff_3_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt3_ff_0_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt3_ff_1_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt3_ff_2_hist              : out  STD_LOGIC_VECTOR (15 downto 0);
         fmt3_ff_3_hist              : out  STD_LOGIC_VECTOR (15 downto 0);     
         efb1_misc_stts_reg_i_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
			efb1_misc_stts_reg_i_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
         efb2_misc_stts_reg_i_0_hist : out  STD_LOGIC_VECTOR (15 downto 0);
			efb2_misc_stts_reg_i_1_hist : out  STD_LOGIC_VECTOR (15 downto 0);
         router0_busy_i_hist         : out  STD_LOGIC_VECTOR (15 downto 0);
			router1_busy_i_hist         : out  STD_LOGIC_VECTOR (15 downto 0);
			histoFull0_hist             : out  STD_LOGIC_VECTOR (15 downto 0);
			histoFull1_hist             : out  STD_LOGIC_VECTOR (15 downto 0);
			rod_busy_n_out_p_hist       : out  STD_LOGIC_VECTOR (15 downto 0);
			histoOverrun0_hist          : out  STD_LOGIC_VECTOR (15 downto 0);
			histoOverrun1_hist          : out  STD_LOGIC_VECTOR (15 downto 0)
			);
	END COMPONENT;	

	COMPONENT ssram_clk_gen
		PORT(
			CLK_IN 				: IN  STD_LOGIC;
			CLK_SRAM_IN1 	: IN  STD_LOGIC;
			CLK_SRAM_IN2 	: IN  STD_LOGIC;          
			CLK_SRAM_OUT1 : OUT STD_LOGIC;
			CLK_SRAM_OUT2 : OUT STD_LOGIC;
			clk1_100M 		: OUT STD_LOGIC;
			clk2_100M 		: OUT STD_LOGIC		
		);
	END COMPONENT;

  COMPONENT histoUnit
		GENERIC(simulation: boolean := false ;useExtRam: boolean := false);
    PORT(
 		  control 	: IN  STD_LOGIC_VECTOR(31 downto 0);
      hitEnable : IN  STD_LOGIC;
			row 			: IN  STD_LOGIC_VECTOR( rowBits - 1 downto 0);
			col 			: IN  STD_LOGIC_VECTOR( colBits - 1 downto 0);
			chip 			: IN  STD_LOGIC_VECTOR(chipBits - 1 downto 0);
			totVal 		: IN  STD_LOGIC_VECTOR(totWidth - 1 downto 0);
      rfd 			: OUT STD_LOGIC;
      mode 			: IN  STD_LOGIC_VECTOR(readoutModeBits - 1 downto 0);
      rfr 			: IN  STD_LOGIC;
      roc 			: OUT STD_LOGIC;
      histValid : OUT STD_LOGIC;
      histo 		: OUT STD_LOGIC_VECTOR(31 downto 0);
      rst 			: IN  STD_LOGIC;
      clk 			: IN  STD_LOGIC;
      ramClk 		: IN  STD_LOGIC;
      ramAddr 	: OUT STD_LOGIC_VECTOR(addrBits - 1 downto 0);
      ramData 	: INOUT STD_LOGIC_VECTOR(histDataBits - 1 downto 0);
      ramCke_n 	: OUT STD_LOGIC;
      ramCs_n 	: OUT STD_LOGIC;
      ramBw_n 	: OUT STD_LOGIC_VECTOR(3 downto 0);
      ramWe_n 	: OUT STD_LOGIC;
      ramOe_n 	: OUT STD_LOGIC;
      ramAdv 		: OUT STD_LOGIC;
      ramLbo 		: OUT STD_LOGIC;
      ramZz 		: OUT STD_LOGIC
    );
	END COMPONENT;

	COMPONENT ram_block 
		port (
	clk40                  : in  std_logic; -- clk40 input
	rst                    : in  std_logic; -- async global reset
	rod_bus_strb_in        : in  std_logic; --
	rod_bus_rnw_in         : in  std_logic; --
	rod_bus_hwsb_in        : in  std_logic; --
	rod_bus_ds_in          : in  std_logic; --
	rod_bus_addr_in        : in  std_logic_vector(9 downto 0); --
	rod_bus_data_in        : in  std_logic_vector(15 downto 0); --
	rod_bus_data_out       : out std_logic_vector(15 downto 0); --
	rod_bus_ack_out        : out std_logic;	
	cpuClk : in std_logic;
	cpuWr : in std_logic;
	cpuAddr : IN  std_logic_vector(9 downto 0);
	cpuDout : IN  std_logic_vector(31 downto 0);
	cpuDin : out  std_logic_vector(31 downto 0);
	ctlReg : out  std_logic_vector(31 downto 0)
    );   
	END COMPONENT;
	
-- ######### Chip Scope Cores #########
component icon
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL2 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));
end component;
	
	component ila
	PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR( 47 DOWNTO 0);
    TRIG1 : IN STD_LOGIC_VECTOR( 63 DOWNTO 0);
    TRIG2 : IN STD_LOGIC_VECTOR( 63 DOWNTO 0);
    TRIG3 : IN STD_LOGIC_VECTOR(127 DOWNTO 0));
	end component;
	
	component ila_histo
   PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0)); 
	end component;

	SIGNAL CONTROL0 : STD_LOGIC_VECTOR( 35 downto 0); 
	SIGNAL CONTROL1 : STD_LOGIC_VECTOR( 35 downto 0);
	SIGNAL CONTROL2 : STD_LOGIC_VECTOR( 35 downto 0);
	SIGNAL ila_trig0 	: STD_LOGIC_VECTOR( 47 DOWNTO 0); 
	SIGNAL ila_trig1 	: STD_LOGIC_VECTOR( 63 DOWNTO 0);	
	SIGNAL ila_trig2 	: STD_LOGIC_VECTOR( 63 DOWNTO 0);	
	SIGNAL ila_trig3 	: STD_LOGIC_VECTOR(127 DOWNTO 0); 
	
	SIGNAL ila_histo0: STD_LOGIC_VECTOR(31 downto 0);
	SIGNAL ila_histo1: STD_LOGIC_VECTOR(31 downto 0);
	
	-- HW DEBUGGING FOR CHIPSCOPE
	SIGNAL fmt0_valid_to_efb_i		: STD_LOGIC;  
	SIGNAL fmt0_data_to_efb_i		: STD_LOGIC_VECTOR(34 downto 0);      
	SIGNAL fmt0_boe_to_efb_i		: STD_LOGIC;                 
	SIGNAL fmt0_eoe_to_efb_i		: STD_LOGIC;
	
--	SIGNAL in_header_count			: UNSIGNED(15 downto 0):= (others => '0');
	-- SIGNAL rxdata_boc2rod_d1      : std_logic_vector(7 downto 0);
	
--	SIGNAL count_header_state		: STD_LOGIC_VECTOR(1 downto 0) := "00";
	
	SIGNAL ev_fifo_wr_count_i : STD_LOGIC_VECTOR(9 downto 0) := "0000000000";
	SIGNAL ev_id_fifo_empty_error0_i : STD_LOGIC;
	
	-- Trigger from Tim delay to FE
	constant delay_cycle : integer := 140; -- 150
	--signal x1_delay	: std_logic_vector(delay_cycle downto 0);
	
	
begin
----------------------------------------------------------------------------
--COMPONENT INSTANTIATION
----------------------------------------------------------------------------
	
	-- Chip Scope  
	icon_inst : icon
	port map (
		CONTROL0 => CONTROL0,
		CONTROL1 => CONTROL1,
		CONTROl2 => CONTROl2
	);
	
	ila_inst : ila
	port map (
		CONTROL => CONTROL0,
		CLK => clk80,
		TRIG0 => ila_trig0,
		TRIG1 => ila_trig1,
		TRIG2 => ila_trig2,
		TRIG3 => ila_trig3
	);
	
	ila_histo_instance0 : ila_histo
   port map (
		CONTROL => CONTROL1,
		CLK => cpuclk,
		TRIG0 => ila_histo0);
		
	ila_histo_instance1 : ila_histo
   port map (
		CONTROL => CONTROL2,
		CLK => cpuclk,
		TRIG0 => ila_histo1);

process
begin
	wait until rising_edge(clk80);
	
	ila_trig0 <=   rx_rod_ctrl_d_i	-- 1 bit  
				& rx_rod_we_d_i	-- 1 bit
				& rx_rod_addr_d_i	--	2 bits
				& rx_rod_data_d_i	-- 8 bits
				& rx_rod_ctrl_c_i 
				& rx_rod_we_c_i 
				& rx_rod_addr_c_i 
				& rx_rod_data_c_i
				& rx_rod_ctrl_b_i 
				& rx_rod_we_b_i 
				& rx_rod_addr_b_i 
				& rx_rod_data_b_i
				& rx_rod_ctrl_a_i 
				& rx_rod_we_a_i 
				& rx_rod_addr_a_i 
				& rx_rod_data_a_i;
	
	ila_trig1 <=   xc_in 							--  8 bits, currently only xc_in(0) is delayed
				& "0000" 
				& rod_bus_ce 					--  1 bit
				& rod_bus_rnw 					--  1 bit
				& rod_bus_hwsb 				--  1 bit
				& rod_bus_ds 					--  1 bit
				& rod_bus_addr 				-- 16 bits
				& rod_bus_data_out_reg_i	-- 16 bits, output from register block not ram block 
				& rod_bus_data_in_i;			-- 16 bits 
	
	ila_trig2 <=   fmt0_eoe_to_efb_i 		--  1 bit, end of event (last trailer of triggered event)
				& fmt0_boe_to_efb_i 		--  1 bit, begin of event (first header of triggered event)
				& fmt0_valid_to_efb_i 	--	 1 bit
				& fmt0_data_to_efb_i 	-- 35 bits
				& ev_id_fifo_empty_error0_i -- 1 bit, if goes to 1, sync error(event arrives to 
				-- EFB BEFORE EFB receives event ID from Master)
				& ev_fifo_wr_count_i(7 downto 0) -- 8 bits, 
				-- event ID written into FIFO to be processed by S-Link data generation
				-- if everything is in sync, this count will toggle between 0 and 1
				& NOT ev_data_wen_n 	--  1 bit, 8 pulses per event ID packet
				& ev_data_from_ppc;	-- 16 bits
				
   ila_trig3(66 downto 0) <= slink1_we_i & slink1_data_rod2boc_i & '0' & slink0_we_i & slink0_data_rod2boc_i;
end process;

process
begin
	wait until rising_edge(cpuclk);
		ila_histo1(24 downto 0) <= hitValid1_i & chip1_i & row1_i & col1_i & totVal1_i & rfd1;
		ila_histo0(24 downto 0) <= hitValid0_i & chip0_i & row0_i & col0_i & totVal0_i & rfd0;
end process;

	-- AKU: counter proc must not use rxdata directly!
	-- AKU: counting only one of the 4 channels so far
	-- AKU: result in_header_count not in use yet
--	counter_header_proc : process(rst_i, clk80)
--	begin
--		if(rst_i = '1') then
--			in_header_count <= (others => '0');
--			count_header_state <= "00";
--		elsif(rising_edge(clk80)) then
--			if(rx_rod_data_a_in = X"FC" AND rx_rod_addr_a_in = "00" AND rx_rod_we_a_in = '1' AND rx_rod_ctrl_a_in = '1' AND count_header_state = "00")then
--				count_header_state <= "01";
--			elsif(rx_rod_data_a_in = X"E9" AND rx_rod_addr_a_in = "00" AND rx_rod_we_a_in = '1' AND count_header_state = "01")then
--				in_header_count <= in_header_count + 1;
--				count_header_state <= "00";
--			end if;
--		end if;
--	end process; 

-- Malte: now implemented in dedicated module
--	rod_busy_n <= '0' when slink0_lff = '1' OR slink0_ldown  = '1' OR 
--					slink1_lff  = '1' OR slink1_ldown  = '1' OR 
--					efb1_misc_stts_reg_i(7)  = '1' OR efb1_misc_stts_reg_i(8)  = '1' OR
--					efb2_misc_stts_reg_i(7)  = '1' OR efb2_misc_stts_reg_i(8)  = '1' OR
--					router0_busy_i  = '1' OR router1_busy_i  = '1' else '1';
--					--fmt0_stts_reg_i
--					--fmt1_stts_reg_i
--					--fmt2_stts_reg_i
--					--fmt3_stts_reg_i				

-- Original SPC code, put back if no longer need xc delay					
--	-- Master control signals routed through Slave to FE-I4
--	rod_master_slave_route : for i in xc'range generate
--		xc(i) <= xc_in(i);
--	end generate;	

	ev_id_fifo_empty_error(0) <= ev_id_fifo_empty_error0_i;
	
--################### xc delay ########################
	xc(7) <= xc_in(7);
	xc(6) <= xc_in(6);
	xc(5) <= xc_in(5);
	xc(4) <= xc_in(4);
	xc(3) <= xc_in(3);	
	xc(2) <= xc_in(2);
	xc(1) <= xc_in(1);
   xc(0) <= xc_in(0);
	
	-- xc delay to the FE so Slave has enough time to receive event data from Master 
--	delay_xc_reg : process(clk80, reset)
--	begin
--		if(reset = '1')then
--			x1_delay <= (others => '0');
--			xc(0) <= '0';
--		elsif(rising_edge(clk80))then
--			x1_delay <= x1_delay(delay_cycle - 1 downto 0) & xc_in(0);
--			xc(0) <= x1_delay(delay_cycle);
--		end if;
--	end process;
--######################################################

--********************* Clocks ********************** 
	-- 40 MHz and 80 MHz clock
--	boc_clk_manager : dcm_40MHz
--	port map(
--		CLK_IN40_P	=> clk40in_p,
--		CLK_IN40_N	=> clk40in_n,
--		CLK_OUT40	=> clk40,
--		CLK_OUT80	=> clk80, -- Phase set to 240 degrees
--		CLK_OUT160	=> clk160,
--		RESET			=> reset,
--		LOCKED		=> boc_dcm_locked_i
--	);
	
	boc_clk_manager : dcm_40MHz
		port map(
			CLK_IN40_P => clk40in_p,
			CLK_IN40_N => clk40in_n,
			CLK_OUT40 => clk40,
			CLK_OUT80_PS240 => clk80late,
			CLK_OUT80 => clk80,
			RESET  => reset,
			LOCKED => boc_dcm_locked_i
		);	
		
	-- 100 mhz input clock
	clk100ibuf: ibufgds port map (I => clk100in_p, IB => clk100in_n, O => clk100in);
   clk100bufg: bufg    port map (i => clk100in, o => clk100);

   -- monitor 100 mhz clock
   clk100in_bar <= NOT clk100in;
   clk100Monbuf: oddr2 port map (c0 => clk100in, c1 => clk100in_bar, d0 => '0', d1 => '1', q => clk100mon, ce => '1', r => '0', s => '0');

   -- monitor 40 mhz clock
   clk40_n <= NOT clk40;
   clk40monbuf: oddr2 port map (c0 => clk40, c1 => clk40_n, d0 => '0', d1 => '1', q => clk40mon, ce => '1', r => '0', s => '0');

   -- monitor cpu clock, should be 100Mhz
	cpuclk_n <= not cpuclk;
	clkbuf: oddr2 port map (c0 => cpuclk, c1 => cpuclk_n, d0 => '0', d1 => '1', q => cpuclkmon, ce => '1', r => '0', s => '0');  

   --Error Sums
   header_err_regS     <= unsigned(header_err_regA)     + unsigned(header_err_regB);
   header_err_reg      <= std_logic_vector(header_err_regS); --Because if you try it with 1 signal, you get operator error
   trailer_err_regS    <= unsigned(trailer_err_regA)    + unsigned(trailer_err_regB);
   trailer_err_reg     <= std_logic_vector(trailer_err_regS);
   rd_timeout_err_regS <= unsigned(rd_timeout_err_regA) + unsigned(rd_timeout_err_regB);
   rd_timeout_err_reg  <= std_logic_vector(rd_timeout_err_regS);
   ht_limit_err_regS   <= unsigned(ht_limit_err_regA)   + unsigned(ht_limit_err_regB);
   ht_limit_err_reg    <= std_logic_vector(ht_limit_err_regS);
   row_col_err_regS    <= unsigned(row_col_err_regA)    + unsigned(row_col_err_regB); 
   row_col_err_reg     <= std_logic_vector(row_col_err_regS);
   l1_id_err_regS      <= unsigned(l1_id_err_regA)      + unsigned(l1_id_err_regB);
   l1_id_err_reg       <= std_logic_vector(l1_id_err_regS);
   bc_id_err_regS      <= unsigned(bc_id_err_regA)      + unsigned(bc_id_err_regB);
   bc_id_err_reg       <= std_logic_vector(bc_id_err_regS);

   --************** Component Instantiations ************
	system_i : system
	port map (
		rzq => rzq,
		mcbx_dram_we_n => mcbx_dram_we_n,
		mcbx_dram_udqs_n => mcbx_dram_udqs_n,
		mcbx_dram_udqs => mcbx_dram_udqs,
		mcbx_dram_udm => mcbx_dram_udm,
		mcbx_dram_ras_n => mcbx_dram_ras_n,
		mcbx_dram_odt => mcbx_dram_odt,
		mcbx_dram_ldm => mcbx_dram_ldm,
		mcbx_dram_dqs_n => mcbx_dram_dqs_n,
		mcbx_dram_dqs => mcbx_dram_dqs,
		mcbx_dram_dq => mcbx_dram_dq,
		mcbx_dram_clk_n => mcbx_dram_clk_n,
		mcbx_dram_clk => mcbx_dram_clk,
		mcbx_dram_cke => mcbx_dram_cke,
		mcbx_dram_cas_n => mcbx_dram_cas_n,
		mcbx_dram_ba => mcbx_dram_ba,
		mcbx_dram_addr => mcbx_dram_addr,
		RS232_Uart_1_sout => RS232_Uart_1_sout,
		RS232_Uart_1_sin => RS232_Uart_1_sin,
		RESET_in => localRst,
		ETHERNET_TX_ER => ETHERNET_TX_ER,
		ETHERNET_TX_EN => ETHERNET_TX_EN,
		ETHERNET_TX_CLK => ETHERNET_TX_CLK,
		ETHERNET_TXD => ETHERNET_TXD,
		ETHERNET_RX_ER => ETHERNET_RX_ER,
		ETHERNET_RX_DV => ETHERNET_RX_DV,
		ETHERNET_RX_CLK => ETHERNET_RX_CLK,
		ETHERNET_RXD => ETHERNET_RXD,
		ETHERNET_PHY_RST_N => ETHERNET_PHY_RST_N,
		ETHERNET_MII_TX_CLK => ETHERNET_MII_TX_CLK,
		ETHERNET_MDIO => ETHERNET_MDIO,
		ETHERNET_MDC => ETHERNET_MDC,
		axi_epc_0_PRH_CS_n_pin => axi_epc_0_PRH_CS_n_pin,
		axi_epc_0_PRH_Addr_pin => axi_epc_0_PRH_Addr_pin,
		axi_epc_0_PRH_RNW_pin => axi_epc_0_PRH_RNW_pin,
		axi_epc_0_PRH_Rdy_pin => axi_epc_0_PRH_Rdy_pin,
		axi_epc_0_PRH_Data_I_pin => axi_epc_0_PRH_Data_I_pin,
		axi_epc_0_PRH_Data_O_pin => axi_epc_0_PRH_Data_O_pin,
		
      axi_histo0_dma_s_axis_s2mm_tready_pin => axi_histo0_dma_s_axis_s2mm_tready_pin,
      axi_histo0_dma_s_axis_s2mm_tlast_pin => axi_histo0_dma_s_axis_s2mm_tlast_pin,
      axi_histo0_dma_s_axis_s2mm_tkeep_pin => axi_histo0_dma_s_axis_s2mm_tkeep_pin,
      axi_histo0_dma_s_axis_s2mm_tdata_pin => axi_histo0_dma_s_axis_s2mm_tdata_pin,
      axi_histo0_dma_s_axis_s2mm_tvalid_pin => axi_histo0_dma_s_axis_s2mm_tvalid_pin,
		--
      axi_histo1_dma_s_axis_s2mm_tready_pin => axi_histo1_dma_s_axis_s2mm_tready_pin,
      axi_histo1_dma_s_axis_s2mm_tlast_pin => axi_histo1_dma_s_axis_s2mm_tlast_pin,
      axi_histo1_dma_s_axis_s2mm_tkeep_pin => axi_histo1_dma_s_axis_s2mm_tkeep_pin,
      axi_histo1_dma_s_axis_s2mm_tdata_pin => axi_histo1_dma_s_axis_s2mm_tdata_pin,
      axi_histo1_dma_s_axis_s2mm_tvalid_pin => axi_histo1_dma_s_axis_s2mm_tvalid_pin,
		--
		axi_spi_0_SCK_O_pin => axi_spi_0_SCK_pin,
		axi_spi_0_MISO_I_pin => axi_spi_0_MISO_pin,
		axi_spi_0_MOSI_O_pin => axi_spi_0_MOSI_pin,
		axi_spi_0_SS_O_pin => axi_spi_0_SS_pin,
		
		cpuclk => cpuclk,
		cpuclk2x => cpuclk2x,
		cpuclk_in => clk100,
		cpurst => cpurst
	);

	inmem_fifo : inmem_fifo1024
	PORT MAP (
		rst 		=> inmem_rst_i,
		wr_clk 	=> clk80,
		rd_clk 	=> clk40,
		din 		=> inmemFifo_din_i,
		wr_en 	=> inmemFifo_wen_i,
		rd_en 	=> inmemFifo_ren_i,
		dout 		=> inmemFifo_dout_i,
		full 		=> open,
		empty 	=> inmemFifo_ef_i
	);
	
	rod_bus_data_io : for i in 0 to 15 generate
		rod_bus_data_iobufs : IOBUF
			port map(
				I  => rod_bus_data_out_i(i),
				O  => rod_bus_data_in_i(i),
				IO => rod_bus_data(i), 
				T  => rod_bus_oe_n_i
			);
	end generate;			
	
	register_block : ibl_slv_register
	PORT MAP (
		clk						   	=> clk40,
		rst                        => rst_i,            
		rod_bus_strb_in		   	=> rod_bus_strb_reg_i,        
		rod_bus_addr               => rod_bus_addr(15 downto 0), 
		rod_bus_rnw                => rod_bus_rnw,               
		rod_bus_hwsb_in            => rod_bus_hwsb,              
		rod_bus_ds_in              => rod_bus_ds,                
		rod_bus_data_in            => rod_bus_data_in_i,         
		rod_bus_data_out           => rod_bus_data_out_reg_i,
		rod_bus_ack_out            => rod_bus_ack_out_reg_i,
		slave_id                   => slave_id_i,
		busCtlReg		         	=> busCtlReg,	
		busStatReg		         	=> busStatReg,
		busy_reg_out	 	      	=> busy_reg_out,	
      busy_mask_reg              => busy_mask_reg,
      force_input_reg            => force_input_reg,
		busy_status_reg	 	   	=> busy_status_reg,
		busy_Slink0LffHist_reg		=> busy_Slink0LffHist_reg,
		busy_Slink0LdownHist_reg	=> busy_Slink0LdownHist_reg,
		busy_Slink1LffHist_reg		=> busy_Slink1LffHist_reg,
		busy_Slink1LdownHist_reg	=> busy_Slink1LdownHist_reg,
      busy_Fmt0FF0_Hist_reg      => busy_Fmt0FF0_Hist_reg,
      busy_Fmt0FF1_Hist_reg      => busy_Fmt0FF1_Hist_reg,
      busy_Fmt0FF2_Hist_reg      => busy_Fmt0FF2_Hist_reg,
      busy_Fmt0FF3_Hist_reg      => busy_Fmt0FF3_Hist_reg,
      busy_Fmt1FF0_Hist_reg      => busy_Fmt1FF0_Hist_reg,
      busy_Fmt1FF1_Hist_reg      => busy_Fmt1FF1_Hist_reg,
      busy_Fmt1FF2_Hist_reg      => busy_Fmt1FF2_Hist_reg,
      busy_Fmt1FF3_Hist_reg      => busy_Fmt1FF3_Hist_reg,
      busy_Fmt2FF0_Hist_reg      => busy_Fmt2FF0_Hist_reg,
      busy_Fmt2FF1_Hist_reg      => busy_Fmt2FF1_Hist_reg,
      busy_Fmt2FF2_Hist_reg      => busy_Fmt2FF2_Hist_reg,
      busy_Fmt2FF3_Hist_reg      => busy_Fmt2FF3_Hist_reg,
      busy_Fmt3FF0_Hist_reg      => busy_Fmt3FF0_Hist_reg,
      busy_Fmt3FF1_Hist_reg      => busy_Fmt3FF1_Hist_reg,
      busy_Fmt3FF2_Hist_reg      => busy_Fmt3FF2_Hist_reg,
      busy_Fmt3FF3_Hist_reg      => busy_Fmt3FF3_Hist_reg,
		busy_Efb1Stts0Hist_reg		=> busy_Efb1Stts0Hist_reg,
		busy_Efb1Stts1Hist_reg		=> busy_Efb1Stts1Hist_reg,
		busy_Efb2Stts0Hist_reg		=> busy_Efb2Stts0Hist_reg,
		busy_Efb2Stts1Hist_reg		=> busy_Efb2Stts1Hist_reg,
		busy_Router0Hist_reg		   => busy_Router0Hist_reg,
		busy_Router1Hist_reg		   => busy_Router1Hist_reg,
      busy_HistFull0Hist_reg     => busy_HistFull0_Hist_reg,    
      busy_HistFull1Hist_reg     => busy_HistFull1_Hist_reg,    
      busy_HistOverrun0Hist_reg  => busy_HistOverrun0_Hist_reg, 
      busy_HistOverrun1Hist_reg  => busy_HistOverrun1_Hist_reg, 
		busy_OutHist_reg		      => busy_OutHist_reg,		
		inmem_reset                => inmem_rb_rst_i,
		inmem_select               => inmemSel_i,
		inmem_rden                 => inmemFifo_ren_i,
		inmem_data                 => inmem_regData_i,	
      hitdisccnfg                => hitdisccnfg_i,
		fmt_link_enabled		  	   => fmt_link_enabled_i,		   
      fmt_trig_cnt               => fmt_trig_cnt_i,
		fmt_timeout_limit			   => fmt_timeout_limit_i,		
		fmt_data_overflow_limit    => fmt_data_overflow_limit_i, 
		fmt_header_trailer_limit   => fmt_header_trailer_limit_i,
		fmt_rod_busy_limit	 	   => fmt_rod_busy_limit_i,
      fmt_trailer_limit			   => fmt_trailer_limit_i,
		fmt_header_trailer_error   => fmt_header_trailer_error_i,
		fmt_time_out_error         => fmt_time_out_error_i,
		fmt_data_overflow_error    => X"0000",
		fmt_ctrl_reg   		      => open, 
		fmt0_stts_reg 				   => fmt0_stts_reg_i,
		fmt1_stts_reg 				   => fmt1_stts_reg_i, 
		fmt2_stts_reg 				   => fmt2_stts_reg_i, 
		fmt3_stts_reg 				   => fmt3_stts_reg_i, 
      fmt0Occupancy              => fmt0Occupancy_i,
      fmt1Occupancy              => fmt1Occupancy_i,
      fmt2Occupancy              => fmt2Occupancy_i,
      fmt3Occupancy              => fmt3Occupancy_i,
      formatter0_frames          => formatter0_frames_i,
      formatter1_frames          => formatter1_frames_i,
      formatter2_frames          => formatter2_frames_i,
      formatter3_frames          => formatter3_frames_i,
		err_mask_wen1              => err_mask_wen1_i,           
		err_mask_din1              => err_mask_din1_i,           
		err_mask_dout1             => err_mask_dout1_i,          
		err_mask_sel1              => err_mask_sel1_i,              
		err_mask_wen2              => err_mask_wen2_i,           
		err_mask_din2              => err_mask_din2_i,           
		err_mask_dout2             => err_mask_dout2_i,          
		err_mask_sel2              => err_mask_sel2_i,           
		err_mask_wen3              => err_mask_wen3_i,           
		err_mask_din3              => err_mask_din3_i,           
		err_mask_dout3             => err_mask_dout3_i,          
		err_mask_sel3              => err_mask_sel3_i,              
		err_mask_wen4              => err_mask_wen4_i,           
		err_mask_din4              => err_mask_din4_i,           
		err_mask_dout4             => err_mask_dout4_i,          
		err_mask_sel4              => err_mask_sel4_i,           
		source_id                  => efb_source_id_i,               
		format_version             => efb_format_version_i,          
		run_number                 => efb_run_number_i,              
		efb1_out_fifo_rst          => efb1_out_fifo_rst_i,       
		efb1_out_fifo1_empty_n     => efb1_out_fifo1_empty_n_i,  
		efb1_out_fifo1_aempty_n    => efb1_out_fifo1_aempty_n_i, 
		efb1_out_fifo1_full_n      => efb1_out_fifo1_full_n_i,   
		efb1_out_fifo1_afull_n     => efb1_out_fifo1_afull_n_i,  
		efb1_out_fifo1_play_done   => efb1_out_fifo1_play_done_i,
		efb1_out_fifo2_empty_n     => efb1_out_fifo2_empty_n_i,  
		efb1_out_fifo2_aempty_n    => efb1_out_fifo2_aempty_n_i, 
		efb1_out_fifo2_full_n      => efb1_out_fifo2_full_n_i,   
		efb1_out_fifo2_afull_n     => efb1_out_fifo2_afull_n_i,  
      efb1_out_fifo2_play_done   => efb1_out_fifo2_play_done_i,
		efb2_out_fifo_rst          => efb2_out_fifo_rst_i,       
		efb2_out_fifo1_empty_n     => efb2_out_fifo1_empty_n_i,  
		efb2_out_fifo1_aempty_n    => efb2_out_fifo1_aempty_n_i, 
		efb2_out_fifo1_full_n      => efb2_out_fifo1_full_n_i,   
		efb2_out_fifo1_afull_n     => efb2_out_fifo1_afull_n_i,  
		efb2_out_fifo1_play_done   => efb2_out_fifo1_play_done_i,
		efb2_out_fifo2_empty_n     => efb2_out_fifo2_empty_n_i,  
		efb2_out_fifo2_aempty_n    => efb2_out_fifo2_aempty_n_i, 
		efb2_out_fifo2_full_n      => efb2_out_fifo2_full_n_i,   
		efb2_out_fifo2_afull_n     => efb2_out_fifo2_afull_n_i,  
      efb2_out_fifo2_play_done   => efb2_out_fifo2_play_done_i,
		efb1_misc_stts_reg         => efb1_misc_stts_reg_i,      	
		efb2_misc_stts_reg         => efb2_misc_stts_reg_i,      
		efb1_ev_header_data        => efb1_ev_header_data_i,     
		efb2_ev_header_data        => efb2_ev_header_data_i,     
		efb1_ev_fifo_data1         => efb1_ev_fifo_data1_i,      
		efb1_ev_fifo_data2         => efb1_ev_fifo_data2_i,      
		efb2_ev_fifo_data1         => efb2_ev_fifo_data1_i,      
		efb2_ev_fifo_data2         => efb2_ev_fifo_data2_i,      
		efb_mask_bcid_error        => efb_mask_bcid_error_i,         
		efb_mask_l1id_error        => efb_mask_l1id_error_i,         
		efb_bcid_offset            => efb_bcid_offset_i,             
		efb_grp_evt_enable         => efb_grp_evt_enable_i,          
		efb1_event_count           => efb1_event_count_i,        
		efb2_event_count           => efb2_event_count_i,        
		mask_boc_clock_err         => efb_mask_boc_clock_err_i,      
		mask_tim_clock_err         => efb_mask_tim_clock_err_i,      
		efb1_l1id_fifo_data		   => efb1_l1id_fifo_data_i,	
		efb1_l1id_fifo_addr		   => efb1_l1id_fifo_addr_i,	
		efb2_l1id_fifo_data	      => efb2_l1id_fifo_data_i,   
		efb2_l1id_fifo_addr		   => efb2_l1id_fifo_addr_i,	
		efb_enable_l1_trap         => efb_enable_l1_trap_i,          
		efb_l1_trap_full_in        => efb_l1_trap_full_in_i,         
		efb1_latched_l1id0         => efb1_latched_l1id0_i,      
		efb1_latched_l1id1         => efb1_latched_l1id1_i,      
		efb1_latched_bcid0         => efb1_latched_bcid0_i,      
		efb1_latched_bcid1         => efb1_latched_bcid1_i,      
		efb2_latched_l1id0         => efb2_latched_l1id0_i,      
		efb2_latched_l1id1         => efb2_latched_l1id1_i,      
		efb2_latched_bcid0         => efb2_latched_bcid0_i,      
		efb2_latched_bcid1         => efb2_latched_bcid1_i,      				
		tim_bcid_in_rol            => efb_tim_bcid_in_rol_i,         
		bcid_rollover_select       => efb_bcid_rollover_sel_i,
		calib_mode					   => calib_mode_i,
      link0_errors_reg           => link0_err_reg_i, 
      link1_errors_reg           => link1_err_reg_i, 
      link2_errors_reg           => link2_err_reg_i, 
      link3_errors_reg           => link3_err_reg_i, 
      link4_errors_reg           => link4_err_reg_i, 
      link5_errors_reg           => link5_err_reg_i, 
      link6_errors_reg           => link6_err_reg_i, 
      link7_errors_reg           => link7_err_reg_i, 
      link8_errors_reg           => link8_err_reg_i, 
      link9_errors_reg           => link9_err_reg_i, 
      link10_errors_reg          => link10_err_reg_i,
      link11_errors_reg          => link11_err_reg_i,
      link12_errors_reg          => link12_err_reg_i,
      link13_errors_reg          => link13_err_reg_i,
      link14_errors_reg          => link14_err_reg_i,
      link15_errors_reg          => link15_err_reg_i,
      header_err_reg             => header_err_reg,     
      trailer_err_reg            => trailer_err_reg,    
      rd_timeout_err_reg         => rd_timeout_err_reg, 
      ht_limit_err_reg           => ht_limit_err_reg,   
      row_col_err_reg            => row_col_err_reg,   
      l1_id_err_reg              => l1_id_err_reg,      
      bc_id_err_reg              => bc_id_err_reg      
	);
	
	half_slave1 : ibl_halfSlave
	PORT MAP (
      clk40=>clk40, clk80=>clk80, 
		clk100=>cpuclk, -- clk100=>clk100, 
		reset                  	   => rst_i, 
		efb_id                     => efb_id1_i,
		data2rod1 					   => rx_rod_data_a_i,
		addr2rod1 					  	=> rx_rod_addr_a_i,
		valid2rod1 					  	=> rx_rod_we_a_i, 	
		ctl2rod1 					 	=> rx_rod_ctrl_a_i,
		data2rod2 						=> rx_rod_data_b_i,
		addr2rod2 						=> rx_rod_addr_b_i,
		valid2rod2 						=> rx_rod_we_b_i, 	
		ctl2rod2 						=>	rx_rod_ctrl_b_i,
		fmt_fifo_busy              =>	fmt_busy_i(7 downto 0),
      fe_cmdpulse                => fe_cmdpulse,
		mode_bits_in               => mode_bits_in, 			
		modebits_fifo_wen_n_in     => modebits_fifo_wen_n_in,
		modebits_fifo_rst_n_in     => modebits_fifo_rst_n_in,
		modebits_fifo_ef_n_out     => modebits_fifo_ef_n_out_i(0),
		modebits_fifo_ff_n_out     => modebits_fifo_ff_n_out_i(0),
		ev_data_from_ppc       	   => ev_data_from_ppc, 		
		ev_data_wen_n          	   => ev_data_wen_n, 			
		ev_data_rst_n          	   => ev_data_rst_n, 			
		ev_data_almost_full_n  	   => ev_data_almost_full_n(0), 	
		ev_data_ready          	   => ev_data_ready(0),
		-- HW DEBUG
		fifocount_out              => ev_fifo_wr_count_i,		
		-- HW DEBUG 
		--ev_id_fifo_empty_error 		 => ev_id_fifo_empty_error(0),
		ev_id_fifo_empty_error 		=> ev_id_fifo_empty_error0_i,
		fmt0_stts_reg					=> fmt0_stts_reg_i,
		fmt1_stts_reg				   => fmt1_stts_reg_i,
      fmt0Occupancy              => fmt0Occupancy_i,
      fmt1Occupancy              => fmt1Occupancy_i,
      fmt0_1_ht_error            => fmt_header_trailer_error_i(7 downto 0),
      fmt0_1_timeout_error       => fmt_time_out_error_i(7 downto 0),
		fmt_link_enabled			   => fmt_link_enabled_i(7 downto 0),
      fmt_trig_cnt               => fmt_trig_cnt_i,
      hitdisccnfg                => hitdisccnfg_i(15 downto 0),
      --	fmt_exp_mode_en			      => "00000000",
		fmt_timeout_limit			   => fmt_timeout_limit_i,	   
		fmt_data_overflow_limit	   => fmt_data_overflow_limit_i,	   
		fmt_header_trailer_limit   => fmt_header_trailer_limit_i,   
		fmt_rod_busy_limit			=> fmt_rod_busy_limit_i,			
      fmt_trailer_limit			   => fmt_trailer_limit_i,
      formatter0_frames          => formatter0_frames_i,
      formatter1_frames          => formatter1_frames_i,
		efb_err_mask_wen1          => err_mask_wen1_i,  
		efb_err_mask_din1			  	=> err_mask_din1_i,  
		efb_err_mask_dout1		  	=> err_mask_dout1_i, 
		efb_err_mask_sel1			  	=> err_mask_sel1_i,  
		efb_err_mask_wen2			   => err_mask_wen2_i,  
		efb_err_mask_din2			   =>	err_mask_din2_i,  
		efb_err_mask_dout2		   =>	err_mask_dout2_i, 
		efb_err_mask_sel2			   => err_mask_sel2_i,  
		efb_source_id              => efb_source_id_i,     
		efb_format_version		   => efb_format_version_i,
		efb_run_number             => efb_run_number_i,    
		efb_out_fifo_rst           => efb1_out_fifo_rst_i,       
		efb_out_fifo1_empty_n      => efb1_out_fifo1_empty_n_i,  
		efb_out_fifo1_aempty_n     => efb1_out_fifo1_aempty_n_i, 
		efb_out_fifo1_full_n       => efb1_out_fifo1_full_n_i,   
		efb_out_fifo1_afull_n      => efb1_out_fifo1_afull_n_i,  
		efb_out_fifo1_play_done    => efb1_out_fifo1_play_done_i,
		efb_out_fifo2_empty_n      => efb1_out_fifo2_empty_n_i,  
		efb_out_fifo2_aempty_n     => efb1_out_fifo2_aempty_n_i, 
		efb_out_fifo2_full_n       => efb1_out_fifo2_full_n_i,   
		efb_out_fifo2_afull_n      => efb1_out_fifo2_afull_n_i,  
      efb_out_fifo2_play_done    => efb1_out_fifo2_play_done_i,
		efb_misc_stts_reg          => efb1_misc_stts_reg_i,      
		efb_ev_header_data         => efb1_ev_header_data_i,     
		efb_ev_fifo_data1          => efb1_ev_fifo_data1_i,      
		efb_ev_fifo_data2          => efb1_ev_fifo_data2_i,     
		efb_mask_bcid_error        => efb_mask_bcid_error_i,
		efb_mask_l1id_error        => efb_mask_l1id_error_i,
		efb_bcid_offset            => efb_bcid_offset_i,
		efb_grp_evt_enable         =>	efb_grp_evt_enable_i(0),
		efb_event_count            =>	efb1_event_count_i,
		efb_mask_boc_clock_err     => efb_mask_boc_clock_err_i,
		efb_mask_tim_clock_err     => efb_mask_tim_clock_err_i,
		efb_l1id_fifo_data         => efb1_l1id_fifo_data_i,
		efb_l1id_fifo_addr         => efb1_l1id_fifo_addr_i,
		efb_enable_l1_trap         => efb_enable_l1_trap_i(0), 
		efb_l1_trap_full_in        => efb_l1_trap_full_in_i(0),
		efb_latched_l1id0          => efb1_latched_l1id0_i,
		efb_latched_l1id1          => efb1_latched_l1id1_i,
		efb_latched_bcid0          => efb1_latched_bcid0_i,
		efb_latched_bcid1          => efb1_latched_bcid1_i,
		efb_tim_bcid_in_rol     	=> efb_tim_bcid_in_rol_i,  
		efb_bcid_rollover_sel      => efb_bcid_rollover_sel_i,
		router_busy					   => router0_busy_i,
		calib_mode						=> calib_mode_i,
		slink_utest					   => slink0_utest_i,			
		slink_ureset         	   => slink0_ureset_i,  
		slink_uctrl           	   => slink0_uctrl_i,   
		slink_lff             	   => slink0_lff_i,     
		slink_ldown           	   => slink0_ldown_i,   
		slink_data_out         	   => slink0_data_rod2boc_i,
		slink_data_wen        	   => slink0_we_i,
		histo_rfd						=> rfd0, 
		chip								=> chip0_i,		
		hitValid                   => hitValid0_i, 
		row                        => row0_i,      
		col			               => col0_i,		
		totVal   	               => totVal0_i,
      histoFull                  => histoFull0_i,
      histoOverrun               => histoOverrun0_i,
		-- HW DEBUGGING FOR CHIPSCOPE
		fmt0_valid_to_efb   			=> fmt0_valid_to_efb_i,	 
		fmt0_data_to_efb				=> fmt0_data_to_efb_i,	 
		fmt0_boe_to_efb				=> fmt0_boe_to_efb_i,	 
		fmt0_eoe_to_efb				=> fmt0_eoe_to_efb_i,
      link0_errors_reg           => link0_err_reg_i,
      link1_errors_reg           => link1_err_reg_i,
      link2_errors_reg           => link2_err_reg_i,
      link3_errors_reg           => link3_err_reg_i,
      link4_errors_reg           => link4_err_reg_i,
      link5_errors_reg           => link5_err_reg_i,
      link6_errors_reg           => link6_err_reg_i,
      link7_errors_reg           => link7_err_reg_i,
      header_err_reg             => header_err_regA,      
      trailer_err_reg            => trailer_err_regA,     
      rd_timeout_err_reg         => rd_timeout_err_regA,  
      ht_limit_err_reg           => ht_limit_err_regA,    
      row_col_err_reg            => row_col_err_regA,     
      l1_id_err_reg              => l1_id_err_regA,       
      bc_id_err_reg              => bc_id_err_regA       
	);                            

	half_slave2 : ibl_halfSlave
	PORT MAP (
    clk40=>clk40, clk80=>clk80, 
		clk100=>cpuclk, -- clk100=>clk100,  
		reset                  	   => rst_i, 
		efb_id                     => efb_id2_i,
		data2rod1 					   => rx_rod_data_c_i,
		addr2rod1 					  	=> rx_rod_addr_c_i,
		valid2rod1 					  	=> rx_rod_we_c_i, 	
		ctl2rod1 					   => rx_rod_ctrl_c_i,
		data2rod2 					   => rx_rod_data_d_i,
		addr2rod2 					   => rx_rod_addr_d_i,
		valid2rod2 					   => rx_rod_we_d_i, 	
		ctl2rod2 					   =>	rx_rod_ctrl_d_i,
		fmt_fifo_busy              =>	fmt_busy_i(15 downto 8),
      fe_cmdpulse                => fe_cmdpulse,
		mode_bits_in               => mode_bits_in, 			
		modebits_fifo_wen_n_in     => modebits_fifo_wen_n_in,
		modebits_fifo_rst_n_in     => modebits_fifo_rst_n_in,
		modebits_fifo_ef_n_out     => modebits_fifo_ef_n_out_i(1),
		modebits_fifo_ff_n_out     => modebits_fifo_ff_n_out_i(1),
		ev_data_from_ppc       	   => ev_data_from_ppc, 			
		ev_data_wen_n          	   => ev_data_wen_n, 				
		ev_data_rst_n          	   => ev_data_rst_n, 				
		ev_data_almost_full_n  	   => ev_data_almost_full_n(1), 		
		ev_data_ready          	   => ev_data_ready(1), 
		-- HW DEBUG
		fifocount_out              => open,		
		ev_id_fifo_empty_error 		=> ev_id_fifo_empty_error(1), 	
		fmt0_stts_reg					=> fmt2_stts_reg_i,
		fmt1_stts_reg					=> fmt3_stts_reg_i,
      fmt0Occupancy              => fmt2Occupancy_i,
      fmt1Occupancy              => fmt3Occupancy_i,
      fmt0_1_ht_error            => fmt_header_trailer_error_i(15 downto 8),
      fmt0_1_timeout_error       => fmt_time_out_error_i(15 downto 8),
		fmt_link_enabled			   => fmt_link_enabled_i(15 downto 8),
      fmt_trig_cnt               => fmt_trig_cnt_i,
      hitdisccnfg                => hitdisccnfg_i(31 downto 16),
--	fmt_exp_mode_en			       => "00000000",
		fmt_timeout_limit			   => fmt_timeout_limit_i,	   
		fmt_data_overflow_limit	   => fmt_data_overflow_limit_i,	   
		fmt_header_trailer_limit   => fmt_header_trailer_limit_i,   
		fmt_rod_busy_limit			=> fmt_rod_busy_limit_i,	
      fmt_trailer_limit			   => fmt_trailer_limit_i,
      formatter0_frames          => formatter2_frames_i,
      formatter1_frames          => formatter3_frames_i,
		efb_err_mask_wen1          => err_mask_wen3_i, 
		efb_err_mask_din1			 	=> err_mask_din3_i, 
		efb_err_mask_dout1		  	=> err_mask_dout3_i,
		efb_err_mask_sel1			 	=> err_mask_sel3_i, 
		efb_err_mask_wen2			  	=> err_mask_wen4_i, 
		efb_err_mask_din2			   =>	err_mask_din4_i, 
		efb_err_mask_dout2		  	=>	err_mask_dout4_i,
		efb_err_mask_sel2			  	=> err_mask_sel4_i, 
		efb_source_id              => efb_source_id_i,              
		efb_format_version		   => efb_format_version_i,		   
		efb_run_number             => efb_run_number_i,             
		efb_out_fifo_rst           => efb2_out_fifo_rst_i,       
		efb_out_fifo1_empty_n      => efb2_out_fifo1_empty_n_i,  
		efb_out_fifo1_aempty_n     => efb2_out_fifo1_aempty_n_i, 
		efb_out_fifo1_full_n       => efb2_out_fifo1_full_n_i,   
		efb_out_fifo1_afull_n      => efb2_out_fifo1_afull_n_i,  
		efb_out_fifo1_play_done    => efb2_out_fifo1_play_done_i,
		efb_out_fifo2_empty_n      => efb2_out_fifo2_empty_n_i,  
		efb_out_fifo2_aempty_n     => efb2_out_fifo2_aempty_n_i, 
		efb_out_fifo2_full_n       => efb2_out_fifo2_full_n_i,   
		efb_out_fifo2_afull_n      => efb2_out_fifo2_afull_n_i,  
      efb_out_fifo2_play_done    => efb2_out_fifo2_play_done_i,
		efb_misc_stts_reg          => efb2_misc_stts_reg_i,      
		efb_ev_header_data         => efb2_ev_header_data_i,     
		efb_ev_fifo_data1          => efb2_ev_fifo_data1_i,      
		efb_ev_fifo_data2          => efb2_ev_fifo_data2_i,     
		efb_mask_bcid_error        => efb_mask_bcid_error_i,
		efb_mask_l1id_error        => efb_mask_l1id_error_i,
		efb_bcid_offset            => efb_bcid_offset_i,
		efb_grp_evt_enable         =>	efb_grp_evt_enable_i(1),
		efb_event_count            =>	efb2_event_count_i,
		efb_mask_boc_clock_err     => efb_mask_boc_clock_err_i,
		efb_mask_tim_clock_err     => efb_mask_tim_clock_err_i,
		efb_l1id_fifo_data         => efb2_l1id_fifo_data_i,
		efb_l1id_fifo_addr         => efb2_l1id_fifo_addr_i,
		efb_enable_l1_trap         => efb_enable_l1_trap_i(1), 
		efb_l1_trap_full_in        => efb_l1_trap_full_in_i(1),
		efb_latched_l1id0          => efb2_latched_l1id0_i,
		efb_latched_l1id1          => efb2_latched_l1id1_i,
		efb_latched_bcid0          => efb2_latched_bcid0_i,
		efb_latched_bcid1          => efb2_latched_bcid1_i,
		efb_tim_bcid_in_rol     	=> efb_tim_bcid_in_rol_i,  
		efb_bcid_rollover_sel      => efb_bcid_rollover_sel_i,
		router_busy					   => router1_busy_i,
		calib_mode						=> calib_mode_i,
		slink_utest					   => slink1_utest_i,			
		slink_ureset         	   => slink1_ureset_i,  
		slink_uctrl           	   => slink1_uctrl_i,   
		slink_lff             	   => slink1_lff_i,     
		slink_ldown           	   => slink1_ldown_i,   
		slink_data_out         	   => slink1_data_rod2boc_i,
		slink_data_wen        	   => slink1_we_i,
		histo_rfd						=> rfd1,
		chip								=> chip1_i,		
		hitValid                   => hitValid1_i, 
		row                        => row1_i,      
		col			               => col1_i,		
		totVal   	               => totVal1_i,
      histoFull                  => histoFull1_i,
      histoOverrun               => histoOverrun1_i,
		fmt0_valid_to_efb   			=> open,
		fmt0_data_to_efb				=> open,
		fmt0_boe_to_efb				=> open,
		fmt0_eoe_to_efb				=> open,
      link0_errors_reg           => link8_err_reg_i, 
      link1_errors_reg           => link9_err_reg_i, 
      link2_errors_reg           => link10_err_reg_i,
      link3_errors_reg           => link11_err_reg_i,
      link4_errors_reg           => link12_err_reg_i,
      link5_errors_reg           => link13_err_reg_i,
      link6_errors_reg           => link14_err_reg_i,
      link7_errors_reg           => link15_err_reg_i,
      header_err_reg             => header_err_regB,     
      trailer_err_reg            => trailer_err_regB,    
      rd_timeout_err_reg         => rd_timeout_err_regB, 
      ht_limit_err_reg           => ht_limit_err_regB,   
      row_col_err_reg            => row_col_err_regB,    
      l1_id_err_reg              => l1_id_err_regB,      
      bc_id_err_reg              => bc_id_err_regB       
	);

	
	rod_slave_busy_instance : rod_slave_busy
    PORT MAP (
	   clk40in                     => clk40,
      slink0_lff_n                => slink0_lff,
      slink0_ldown_n              => slink0_ldown,
      slink1_lff_n                => slink1_lff,
      slink1_ldown_n              => slink1_ldown, 
      efb1_misc_stts_reg_i        => efb1_misc_stts_reg_i (8 downto 7),
	   efb2_misc_stts_reg_i        => efb2_misc_stts_reg_i (8 downto 7),
      fmt0_ff                     => fmt_busy_i( 3 downto  0),
      fmt1_ff                     => fmt_busy_i( 7 downto  4),
      fmt2_ff                     => fmt_busy_i(11 downto  8),
      fmt3_ff                     => fmt_busy_i(15 downto 12),
	   router0_busy_i              => router0_busy_i, 
	   router1_busy_i              => router1_busy_i,
	   histoFull0                  => histoFull0_i,
	   histoFull1                  => histoFull1_i,
	   reset                       => rst_i,
	   reset_hists                 => busy_reg_out(0),
      histoOverrun0               => histoOverrun0_i,
      histoOverrun1               => histoOverrun1_i,
	   busy_mask                   => busy_mask_reg,
      force_input                 => force_input_reg,
      rod_busy_n_out_p            => rod_busy_n,
      current_busy_status         => busy_status_reg,
      slink0_lff_hist             => busy_Slink0LffHist_reg(15 downto 0),
      slink0_ldown_hist           => busy_Slink0LdownHist_reg(15 downto 0),
      slink1_lff_hist             => busy_Slink1LffHist_reg(15 downto 0),
      slink1_ldown_hist           => busy_Slink1LdownHist_reg(15 downto 0),
      fmt0_ff_0_hist              => busy_Fmt0FF0_Hist_reg(15 downto 0),
      fmt0_ff_1_hist              => busy_Fmt0FF1_Hist_reg(15 downto 0),
      fmt0_ff_2_hist              => busy_Fmt0FF2_Hist_reg(15 downto 0),
      fmt0_ff_3_hist              => busy_Fmt0FF3_Hist_reg(15 downto 0),
      fmt1_ff_0_hist              => busy_Fmt1FF0_Hist_reg(15 downto 0),
      fmt1_ff_1_hist              => busy_Fmt1FF1_Hist_reg(15 downto 0),
      fmt1_ff_2_hist              => busy_Fmt1FF2_Hist_reg(15 downto 0),
      fmt1_ff_3_hist              => busy_Fmt1FF3_Hist_reg(15 downto 0),
      fmt2_ff_0_hist              => busy_Fmt2FF0_Hist_reg(15 downto 0),
      fmt2_ff_1_hist              => busy_Fmt2FF1_Hist_reg(15 downto 0),
      fmt2_ff_2_hist              => busy_Fmt2FF2_Hist_reg(15 downto 0),
      fmt2_ff_3_hist              => busy_Fmt2FF3_Hist_reg(15 downto 0),
      fmt3_ff_0_hist              => busy_Fmt3FF0_Hist_reg(15 downto 0),
      fmt3_ff_1_hist              => busy_Fmt3FF1_Hist_reg(15 downto 0),
      fmt3_ff_2_hist              => busy_Fmt3FF2_Hist_reg(15 downto 0),
      fmt3_ff_3_hist              => busy_Fmt3FF3_Hist_reg(15 downto 0),
      efb1_misc_stts_reg_i_0_hist => busy_Efb1Stts0Hist_reg(15 downto 0),
	   efb1_misc_stts_reg_i_1_hist => busy_Efb1Stts1Hist_reg(15 downto 0),
      efb2_misc_stts_reg_i_0_hist => busy_Efb2Stts0Hist_reg(15 downto 0),
	   efb2_misc_stts_reg_i_1_hist => busy_Efb2Stts1Hist_reg(15 downto 0),
      router0_busy_i_hist         => busy_Router0Hist_reg(15 downto 0),
	   router1_busy_i_hist         => busy_Router1Hist_reg(15 downto 0),
      histoFull0_hist             => busy_HistFull0_Hist_reg(15 downto 0),
      histoFull1_hist             => busy_HistFull1_Hist_reg(15 downto 0),
	   rod_busy_n_out_p_hist       => busy_OutHist_reg(15 downto 0),
      histoOverrun0_hist          => busy_HistOverrun0_Hist_reg(15 downto 0),
      histoOverrun1_hist          => busy_HistOverrun1_Hist_reg(15 downto 0)
 );	
-----------------------------------------------------------------
--						        
-----------------------------------------------------------------	
	
	rst_i <= (NOT boc_dcm_locked_i) OR reset;
		
	efb_id1_i <= slave_id_i & '0';
	efb_id2_i <= slave_id_i & '1';
	
--############### BOC Data RX Block	#################
	-- boc2rod input sync with delayed clock
	b2r_no_serdes: if not b2r_serdes generate
	begin
		-- delayed clock
		process
		begin
			wait until rising_edge(clk80late);
				rxdata_boc2rod_r000	<= rxdata_boc2rod; -- first stage in IOB
				rxdata_boc2rod_r00	<= rxdata_boc2rod_r000; -- second stage in fabric
				rxdata_boc2rod_r0		<= rxdata_boc2rod_r00; 	-- third stage in fabric
		end process;
		-- normal clock
		process
		begin
			wait until rising_edge(clk80);
				rxdata_boc2rod_r1 <= rxdata_boc2rod_r0; -- fourth stage in fabric
		end process;		
	end generate;
	
	b2r_with_serdes: if b2r_serdes generate
	begin
		b2r_io_loop: for i in 0 to 47 generate
		begin
		b2r_io_inst : ISERDES2
			generic map (
				BITSLIP_ENABLE => FALSE, -- Enable Bitslip Functionality (TRUE/FALSE)
				DATA_RATE => "SDR", -- Data-rate ("SDR" or "DDR")
				DATA_WIDTH => 1, -- Parallel data width selection (2-8)
				INTERFACE_TYPE => "RETIMED", -- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED"
				SERDES_MODE => "NONE") -- "NONE", "MASTER" or "SLAVE"
			port map (
				CFB0 => open, -- 1-bit output: Clock feed-through route output
				CFB1 => open, -- 1-bit output: Clock feed-through route output
				DFB => open, -- 1-bit output: Feed-through clock output
				FABRICOUT => open, -- 1-bit output: Unsynchrnonized data output
				INCDEC => open, -- 1-bit output: Phase detector output
				-- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
				Q1 => open,
				Q2 => open,
				Q3 => open,
				Q4 => rxdata_boc2rod_r1(i), -- AKU: according to ug381 Q4 should be the earliest output
				SHIFTOUT => open, -- 1-bit output: Cascade output signal for master/slave I/O
				VALID => open, -- 1-bit output: Output status of the phase detector
				BITSLIP => '0', -- 1-bit input: Bitslip enable input
				CE0 => '1', -- 1-bit input: Clock enable input
				CLK0 => clk80late, -- 1-bit input: I/O clock network input
				CLK1 => '0', -- 1-bit input: Secondary I/O clock network input
				CLKDIV => clk80, -- 1-bit input: FPGA logic domain clock input
				D => rxdata_boc2rod(i), -- 1-bit input: Input data
				IOCE => '1', -- 1-bit input: Data strobe input
				RST => '0', -- 1-bit input: Asynchronous reset input
				SHIFTIN => '0' -- 1-bit input: Cascade input signal for master/slave I/O
			);
		end generate;
	end generate;
	
	boc2rod_in_data_reg: process(clk80, rst_i)
	begin
		if(rst_i = '1') then
			rx_rod_data_a_in 	<= (others => '0'); 
			rx_rod_addr_a_in	<= (others => '0');
			rx_rod_we_a_in		<= '0'; 	rx_rod_ctrl_a_in <= '0';
			rx_rod_data_b_in 	<= (others => '0');
			rx_rod_addr_b_in	<= (others => '0');
			rx_rod_we_b_in 	<= '0'; 	rx_rod_ctrl_b_in <= '0';
			rx_rod_data_c_in 	<= (others => '0');
			rx_rod_addr_c_in	<= (others => '0');
			rx_rod_we_c_in 	<= '0'; 	rx_rod_ctrl_c_in <= '0';
			rx_rod_data_d_in 	<= (others => '0'); 
			rx_rod_addr_d_in	<= (others => '0');
			rx_rod_we_d_in 	<= '0'; 	rx_rod_ctrl_d_in <= '0';
		elsif(rising_edge(clk80))then
			rx_rod_data_a_in 	<= rxdata_boc2rod_r1(7 downto 0);
			rx_rod_addr_a_in	<= rxdata_boc2rod_r1(9 downto 8);
			rx_rod_we_a_in		<= rxdata_boc2rod_r1(10);	
			rx_rod_ctrl_a_in	<= rxdata_boc2rod_r1(11);
			rx_rod_data_b_in 	<= rxdata_boc2rod_r1(19 downto 12);
			rx_rod_addr_b_in	<= rxdata_boc2rod_r1(21 downto 20);
			rx_rod_we_b_in		<= rxdata_boc2rod_r1(22);	
			rx_rod_ctrl_b_in	<= rxdata_boc2rod_r1(23);
			rx_rod_data_c_in 	<= rxdata_boc2rod_r1(31 downto 24);
			rx_rod_addr_c_in	<= rxdata_boc2rod_r1(33 downto 32);
			rx_rod_we_c_in		<= rxdata_boc2rod_r1(34);	
			rx_rod_ctrl_c_in	<= rxdata_boc2rod_r1(35);
			rx_rod_data_d_in 	<= rxdata_boc2rod_r1(43 downto 36);
			rx_rod_addr_d_in	<= rxdata_boc2rod_r1(45 downto 44);
			rx_rod_we_d_in		<= rxdata_boc2rod_r1(46);	
			rx_rod_ctrl_d_in	<= rxdata_boc2rod_r1(47);
		end if;
	end process;
	
	boc2rod_testboc_data_mux: process(clk80, rst_i)
	begin
		if(rst_i = '1') then
			rx_rod_data_a_i <= (others => '0'); rx_rod_addr_a_i <= (others => '0');
			rx_rod_we_a_i 	<= '0'; rx_rod_ctrl_a_i <= '0';
			rx_rod_data_b_i <= (others => '0'); rx_rod_addr_b_i <= (others => '0');
			rx_rod_we_b_i 	<= '0'; rx_rod_ctrl_b_i <= '0';
			rx_rod_data_c_i <= (others => '0'); rx_rod_addr_c_i <= (others => '0');
			rx_rod_we_c_i 	<= '0'; rx_rod_ctrl_c_i <= '0';
			rx_rod_data_d_i <= (others => '0'); rx_rod_addr_d_i <= (others => '0');
			rx_rod_we_d_i 	<= '0'; rx_rod_ctrl_d_i <= '0';
		elsif(rising_edge(clk80)) then
			-- testBocdata is injected into the testBocFifo through the MicroBlaze
			-- the dual clock FIFO simply forwards input data when itself is not empty
			-- ROD Slave input data MUXing is selected through the ctlBocTestBit	
			if(effectiveCtlReg(ctlBocTestBit) = '0')then -- testBocMux bit not selected
				rx_rod_data_a_i 	<= rx_rod_data_a_in;		rx_rod_addr_a_i	<= rx_rod_addr_a_in;
				rx_rod_we_a_i		<= rx_rod_we_a_in;		rx_rod_ctrl_a_i	<= rx_rod_ctrl_a_in;
				rx_rod_data_b_i 	<= rx_rod_data_b_in;		rx_rod_addr_b_i	<= rx_rod_addr_b_in;
				rx_rod_we_b_i		<= rx_rod_we_b_in;		rx_rod_ctrl_b_i	<= rx_rod_ctrl_b_in;
				rx_rod_data_c_i 	<= rx_rod_data_c_in;		rx_rod_addr_c_i	<= rx_rod_addr_c_in;
				rx_rod_we_c_i		<= rx_rod_we_c_in;		rx_rod_ctrl_c_i	<= rx_rod_ctrl_c_in;
				rx_rod_data_d_i 	<= rx_rod_data_d_in;		rx_rod_addr_d_i	<= rx_rod_addr_d_in;
				rx_rod_we_d_i		<= rx_rod_we_d_in;		rx_rod_ctrl_d_i	<= rx_rod_ctrl_d_in;
			else
				rx_rod_data_a_i 	<= testBocData(7 downto 0);
				rx_rod_addr_a_i	<=	testBocData(9 downto 8);
				rx_rod_we_a_i	<=	testBocEnable;		rx_rod_ctrl_a_i	<=	testBocData(10);
				rx_rod_data_b_i 	<= testBocData(7 downto 0);
				rx_rod_addr_b_i	<= testBocData(9 downto 8);
				rx_rod_we_b_i	<= testBocEnable;		rx_rod_ctrl_b_i	<= testBocData(10);
				rx_rod_data_c_i 	<= testBocData(7 downto 0);
				rx_rod_addr_c_i	<= testBocData(9 downto 8);
				rx_rod_we_c_i	<= testBocEnable;		rx_rod_ctrl_c_i	<= testBocData(10);
				rx_rod_data_d_i 	<= testBocData(7 downto 0);
				rx_rod_addr_d_i	<= testBocData(9 downto 8);
				rx_rod_we_d_i	<= testBocEnable;		rx_rod_ctrl_d_i	<= testBocData(10);
			end if;
		end if;
	end process;
	
	-- Inmem Fifo Channel Selection MUXes
	with inmemSel_i select
		inmemDin_i 	<= 	rx_rod_ctrl_a_i & rx_rod_addr_a_i & rx_rod_data_a_i when "00", 
										rx_rod_ctrl_b_i & rx_rod_addr_b_i & rx_rod_data_b_i when "01", 
										rx_rod_ctrl_c_i & rx_rod_addr_c_i & rx_rod_data_c_i when "10", 
										rx_rod_ctrl_d_i & rx_rod_addr_d_i & rx_rod_data_d_i when others; --"11"
	with inmemSel_i select
		inmemWe_i 	<= 	rx_rod_we_a_i when "00", 
										rx_rod_we_b_i when "01", 
										rx_rod_we_c_i when "10", 
										rx_rod_we_d_i when others; --"11";
	
	inmem_regData_i	<= inmemFifo_ef_i & inmemFifo_dout_i;
	inmem_rst_i  		<= rst_i or inmem_rb_rst_i;

	inmemFifo_data_reg: process(clk80)
	begin
		if(rising_edge(clk80)) then
			inmemFifo_din_i	<= inmemDin_i; -- ctrl & addr & data;
			if(inmemDin_i(7 downto 0) = x"3C" and inmemDin_i(10) = '1') then -- 00111100 DC-balanced idle signal and ctrl high
				inmemFifo_wen_i 		<= '0';
			else 
				inmemFifo_wen_i 		<= inmemWe_i;
			end if;
		end if;
	end process;						
	
--	rod_bus_oe_process: process (rst_i, clk40, rod_bus_ce, rod_bus_rnw)
--	begin
--		if(rst_i = '1' ) then
--			rod_bus_oe_n_i			<= '1';		
--		elsif(rising_edge(clk40)) then
--			if (rod_bus_ce = '1' AND rod_bus_rnw = '1') then
--				rod_bus_oe_n_i <= '0'; -- read
--			else
--				rod_bus_oe_n_i <= '1';
--			end if;
--		end if;	
--	end process;

	modebits_fifo_ef_n_out <= modebits_fifo_ef_n_out_i(1) OR modebits_fifo_ef_n_out_i(0);
	modebits_fifo_ff_n_out <= modebits_fifo_ff_n_out_i(1) OR modebits_fifo_ff_n_out_i(0);

-- ##############################################################
-- Histogrammers
	SSRAM_clk_gen_inst: ssram_clk_gen 
		PORT MAP(
			CLK_IN 			=> clk100in,
			CLK_SRAM_IN1 	=> CLK200in(0),
			CLK_SRAM_IN2 	=> CLK200in(1),
			CLK_SRAM_OUT1  => clk200out(0),
			CLK_SRAM_OUT2  => clk200out(1),
			clk1_100M 		=> sram1_inclk,
			clk2_100M 		=> sram2_inclk
		);
	
  histo_0: histoUnit
		generic map (simulation => simulation, useExtRam => useExtRam)
		PORT MAP (
			control 	=> localHistCtlReg0,               
			hitEnable => hitEnable0_r,			
			row 			=> row0_r,				
			col			=> col0_r,					
			chip 			=> chip0_r,					
			totVal 		=> totVal0_r,	
			rfd 			=> rfd0,              
			mode 			=> mode0,
			rfr 			=> rfr0,
			roc 			=> roc0,
			histValid => histValid0,
			histo 		=> histo0,
			rst 			=> cpurst, --cpurst,
			clk 			=> cpuclk, -- cpuclk    -- need to change to MB clock100 later
			ramClk 		=> sram1_inclk,   
			ramAddr 	=> sram1_a,       
			ramData 	=> sram1_io,      
			ramCke_n 	=> sram1_cke_n,   
			ramCs_n 	=> sram1_cs1,     
			ramBw_n(0)=> sram1_bw1_n,   
			ramBw_n(1)=> sram1_bw2_n,   
			ramBw_n(2)=> sram1_bw3_n,   
			ramBw_n(3)=> sram1_bw4_n,   
			ramWe_n 	=> sram1_we_n,    
			ramOe_n 	=> sram1_oe_n,    
			ramAdv 		=> sram1_AdvLd_n, 
			ramLbo 		=> sram1_lbo,   	
			ramZz 		=> sram1_zz     	
		);

  histo_1: histoUnit 
		generic map (simulation => simulation, useExtRam => useExtRam)
		PORT MAP (
			control 	=> localHistCtlReg1,	
			hitEnable => hitEnable1_r,			
			row 			=> row1_r,					
			col 			=> col1_r,					
			chip 			=> chip1_r,					
			totVal 		=> totVal1_r,				
			rfd 			=> rfd1,
			mode 			=> mode1,
			rfr 			=> rfr1,
			roc 			=> roc1,
			histValid => histValid1,
			histo 		=> histo1,
			rst 			=> cpurst, --cpurst,
			clk 			=> cpuclk, -- cpuclk     -- need to change to MB clock100 later
			ramClk 		=> sram2_inclk,   
			ramAddr 	=> sram2_a,       
			ramData 	=> sram2_io,      
			ramCke_n 	=> sram2_cke_n,   
			ramCs_n 	=> sram2_cs1,     
			ramBw_n(0)=> sram2_bw1_n,   
			ramBw_n(1)=> sram2_bw2_n,   
			ramBw_n(2)=> sram2_bw3_n,   
			ramBw_n(3)=> sram2_bw4_n,   
			ramWe_n 	=> sram2_we_n,    
			ramOe_n 	=> sram2_oe_n,    
			ramAdv 		=> sram2_AdvLd_n, 
			ramLbo		=> sram2_lbo,			
			ramZz		 	=> sram2_zz				
		);


-- ##############################################################
-- SLINK ddr buffers

	process begin
		wait until rising_edge(clk40);
		-- SLINK0
		slink0_utest_iob 		<= slink0_utest_i;
		slink0_ureset_iob		<= slink0_ureset_i;
		slink0_uctrl_iob 		<= slink0_uctrl_i;
		slink0_we_iob 			<= slink0_we_i;
		slink0_lff_i			<= slink0_lff_iob;
		slink0_ldown_i			<= slink0_ldown_iob;
		-- SLINK1
		slink1_utest_iob 		<= slink1_utest_i;
		slink1_ureset_iob		<= slink1_ureset_i;
		slink1_uctrl_iob 		<= slink1_uctrl_i;
		slink1_we_iob 			<= slink1_we_i;
		slink1_lff_i			<= slink1_lff_iob;
		slink1_ldown_i			<= slink1_ldown_iob;
	end process;

	slink0_utest			<= slink0_utest_iob;
	slink0_ureset			<= slink0_ureset_iob;
	slink0_uctrl			<= slink0_uctrl_iob;
	slink0_we				<= slink0_we_iob;
	slink0_lff_iob			<= slink0_lff;
	slink0_ldown_iob		<= slink0_ldown;
	
	slink1_utest			<= slink1_utest_iob;
	slink1_ureset			<= slink1_ureset_iob;
	slink1_uctrl			<= slink1_uctrl_iob;
	slink1_we				<= slink1_we_iob;
	slink1_lff_iob			<= slink1_lff;
	slink1_ldown_iob		<= slink1_ldown;

	clk40_n	<=	NOT(clk40);

	slink_ddr: for I in 0 to 15 generate
		slink0_udata_ddr_I: ODDR2
			generic map(
				DDR_ALIGNMENT => "C0",   -- Sets output alignment to "NONE", "C0", "C1" 
				INIT => '0',             -- Sets initial state of the Q output to '0' or '1'
				SRTYPE => "ASYNC")       -- Specifies "SYNC" or "ASYNC" set/reset
			port map (
				Q 	=> slink0_data_rod2boc(I),  -- 1-bit output data
				C0 => clk40,           -- 1-bit clock input
				C1 => clk40_n,     -- 1-bit clock input
				CE => '1',             -- 1-bit clock enable input
				D0 => slink0_data_rod2boc_i(2*I+1),   -- 1-bit data input (associated with C0)
				D1 => slink0_data_rod2boc_i(2*I), -- 1-bit data input (associated with C1)
				R 	=> reset,	           -- 1-bit reset input
				S 	=> '0'               -- 1-bit set input
			);

		slink1_udata_ddr_I: ODDR2
			generic map(
				DDR_ALIGNMENT => "C0",   -- Sets output alignment to "NONE", "C0", "C1" 
				INIT => '0',             -- Sets initial state of the Q output to '0' or '1'
				SRTYPE => "ASYNC")       -- Specifies "SYNC" or "ASYNC" set/reset
			port map (
				Q 	=> slink1_data_rod2boc(I),  -- 1-bit output data
				C0 => clk40,         -- 1-bit clock input
				C1 => clk40_n,     -- 1-bit clock input
				CE => '1',             -- 1-bit clock enable input
				D0 => slink1_data_rod2boc_i(2*I+1),   -- 1-bit data input (associated with C0)
				D1 => slink1_data_rod2boc_i(2*I), -- 1-bit data input (associated with C1)
				R 	=> reset,			     -- 1-bit reset input
				S 	=> '0'               -- 1-bit set input
			);
			
	end generate;

	slink0_clkout: ODDR2
		GENERIC MAP (
			DDR_ALIGNMENT => "NONE",
			INIT => '0',
			SRTYPE => "SYNC")
		PORT MAP (
			Q => slink0_uclk,
			C0 => clk40,
			C1 => clk40_n,
			CE => '1',
			D0 => '0',
			D1 => '1',
			R => '0',
			S => '0'
		);

	slink1_clkout: ODDR2
		GENERIC MAP (
			DDR_ALIGNMENT => "NONE",
			INIT => '0',
			SRTYPE => "SYNC")
		PORT MAP (
			Q => slink1_uclk,
			C0 => clk40,
			C1 => clk40_n,
			CE => '1',
			D0 => '0',
			D1 => '1',
			R => '0',
			S => '0'
		);
	
---------------------------------------------------------------------------------------
--               RAM BLOCK		
------------------------------------------------------------------------------------		
		
	ram_block_inst : ram_block
		PORT MAP (
		 clk40            => clk40,
		 rst              => rst_i,
		 cpuClk 				=> cpuclk,
		 cpuWr 				=> busRamWr,
		 cpuAddr 			=> axi_epc_0_PRH_Addr_pin(11 downto 2), -- need to check correctness
		 cpuDout 			=> axi_epc_0_PRH_Data_O_pin,
		 cpuDin 				=> busRamData,
		 rod_bus_strb_in  => rod_bus_strb_ram_i,
		 rod_bus_rnw_in   => rod_bus_rnw,
		 rod_bus_hwsb_in  => rod_bus_hwsb,
		 rod_bus_ds_in    => rod_bus_ds,
		 rod_bus_addr_in  => rod_bus_addr(9 downto 0), -- check length
		 rod_bus_data_in  => rod_bus_data_in_i,
		 rod_bus_data_out => rod_bus_data_out_ram_i,
		 rod_bus_ack_out	=> rod_bus_ack_out_ram_i
		);

		
	-- Strobe Signal Select Between Slave Register and Blcok RAM
	rod_bus_strb_reg_i 	<= NOT ram_selected_i and rod_bus_strb_in_i;
	rod_bus_strb_ram_i	<= ram_selected_i and rod_bus_strb_in_i;

	-- ROD Bus Data Out Select Between Slave Register and Blcok RAM
	rod_bus_data_out_i	<= rod_bus_data_out_reg_i when ram_selected_i = '0'
		else rod_bus_data_out_ram_i;
		
	-- ROD Bus Acknowledge Signal Select Between Slave Register and Blcok RAM		
	rod_bus_ack_out	<= rod_bus_ack_out_reg_i when ram_selected_i = '0'
		else rod_bus_ack_out_ram_i;
	
	rod_bus_process: process (rst_i, clk40, rod_bus_ce, rod_bus_rnw)
	begin
		if(rst_i = '1' ) then
			rod_bus_oe_n_i			<= '1';
         rod_bus_strb_in_i 	<= '0';
         ram_selected_i 		<= '0';			
		elsif(rising_edge(clk40)) then
			rod_bus_strb_in_i 	<= rod_bus_ce;
			if ((rod_bus_strb_reg_i = '1' OR rod_bus_strb_ram_i = '1')AND rod_bus_rnw = '1') then
				rod_bus_oe_n_i <= '0'; -- read
			else
				rod_bus_oe_n_i <= '1';
			end if;
			-- determine if Block RAM is selected
			if(rod_bus_addr(addr_width - 1 downto addr_width - range_bits) = ram_range)then
				ram_selected_i <= '1';
			else
				ram_selected_i <= '0';
			end if;
		end if;	
	end process;
		
      -- fix ddr termination regulator control pin to '1'
      sd_vtt_s6 <= '1'; -- '1' successfully tested; 'Z' untested
		
	-- MicroBlaze Process	
	-- begin header generator ----------
	d1: if not simulation generate
	begin
	dump_vars:process(dumpIt)
  variable fline: line; -- needs a variable
	variable gid: std_logic_vector(7 downto 0);
	begin
		  if dumpit = 1 then
        file_open(mappingFile,"../inc/rodSlave.hxx",WRITE_MODE);
				write(fline,string'(""));
        write(fline,string'("// IBL ROD spartan-6 formatter slave address and bit definitions"));
        writeline(mappingFile,fline);
        write(fline,string'("#ifndef ROD_SLAVE_H"));
        writeline(mappingFile,fline);
        write(fline,string'("#define ROD_SLAVE_H"));
        writeline(mappingFile,fline);
        write(fline,string'("// This is a generated file, do not edit!"));
        writeline(mappingFile,fline);
        write(fline,string'(""));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_VHDL_COMPILE_DATE "));
        write(fline,"""" & compileDate & """");
        writeline(mappingFile,fline);

        write(fline,string'("// make size of ram available"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_SLV_RAM_WORDS "));
		  -- ramSelBit is defined in slave register pack
        write(fline,str(2**ram_word_size));
        writeline(mappingFile,fline);
		  -- we also provide the formatter/slave registe numbers from register_block
		  ----------------------------------------
        write(fline,string'("// Design parameters"));
        writeline(mappingFile,fline);
        write(fline,string'("// The following register addresses sit on top of the EPC area, defined in xparameters.h"));
        writeline(mappingFile,fline);
		  -- include xilinx parameters, if not defined
        write(fline,string'("#ifndef XPAR_AXI_EPC_0_PRH0_BASEADDR"));
        writeline(mappingFile,fline);
        write(fline,string'("#include ""xparameters.h"""));
        writeline(mappingFile,fline);
        write(fline,string'("#endif"));
        writeline(mappingFile,fline);
        write(fline,string'("// Number of EPC address bits is an alternative way to identify the address range"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_EPC_ADDRESS_BITS "));
        write(fline,axi_epc_0_PRH_Addr_pin'length);
        writeline(mappingFile,fline);
        write(fline,string'("// EPC address range for registers"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_EPC_REG_BASE "));
        write(fline,string'("(XPAR_AXI_EPC_0_PRH0_BASEADDR)"));
        writeline(mappingFile,fline);
        write(fline,string'("// EPC address range for registers"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_EPC_RAM_BASE "));
        write(fline,"(XPAR_AXI_EPC_0_PRH0_BASEADDR + 0x" & str(2**RANGE_SIZE,16)&")");
        writeline(mappingFile,fline);

        write(fline,string'("// Design identification"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_DESIGN_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(DESIGN_ID_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Design identification"));
        writeline(mappingFile,fline);
		  -- we don't write the hash because it will be out of sync
		  -- just say we're using the hash
        write(fline,string'("#define MB_HDL_FPGA_HAS_GIT_HASH 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HDL_FPGA_GIT_HASH """));
		  for i in 0 to gitIdLength - 1 loop
				gid := gitHash(i);
				write(fline,hstr(gid));
			end loop;	
        write(fline,string'(""""));
		  
        writeline(mappingFile,fline);
        write(fline,string'("// Control register"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(CTL_REG_ADDR),16)&")");
        writeline(mappingFile,fline);
		  -----------------
        write(fline,string'("// Status register"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(STAT_REG_ADDR),16)&")");
        writeline(mappingFile,fline);
		  
        write(fline,string'("// Status bits"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_SIMULATION_BIT "));
        write(fline,statSimBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_EXT_HISTRAM_BIT "));
        write(fline,statExtRamBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_DMA0_RDY_BIT "));
        write(fline,statDma0RdyBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_DMA1_RDY_BIT "));
        write(fline,statDma1RdyBit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_RFR0_BIT "));
        write(fline,statHistoRfr0Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_RFR1_BIT "));
        write(fline,statHistoRfr1Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_RFD0_BIT "));
        write(fline,statHistoRfd0Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_RFD1_BIT "));
        write(fline,statHistoRfd1Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_ROC0_BIT "));
        write(fline,statHistoRoc0Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_HIST_ROC1_BIT "));
        write(fline,statHistoRoc1Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_STAT_LAST_BIT "));
        write(fline,statLastBit);
        writeline(mappingFile,fline);
		  -----------------
        write(fline,string'("// BOC test input address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_BOC_TEST_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(BOC_TEST_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// BOC serial transmit to master address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_TX_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(BUS_TX_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Histogrammer 0 test input address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HIST0_TEST_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(HIST_0_TEST_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Histogrammer 1 test input address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HIST1_TEST_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(HIST_1_TEST_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Histogrammer 0 control address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HIST0_CTL_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(HIST_0_CTL_ADDR),16)&")");
        writeline(mappingFile,fline);
        write(fline,string'("// Histogrammer 1 control address"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HIST1_CTL_REG "));
        write(fline,"(MB_EPC_REG_BASE + 0x" & str(conv_integer(HIST_1_CTL_ADDR),16)&")");
        writeline(mappingFile,fline);
		----------------------------------------------------------------------
        write(fline,string'(""));
        writeline(mappingFile,fline);

  
        write(fline,string'("// Control register bits"));
        writeline(mappingFile,fline);
        write(fline,string'("// Input control bits"));
        writeline(mappingFile,fline);
        write(fline,string'("// CTL_BOC_ENABLE_BIT: 0: input from local FE-I4. 1: BOC input"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_BOC_ENABLE_BIT "));
        write(fline,ctlBocEnableBit);
        writeline(mappingFile,fline);
        write(fline,string'("// CTL_BOC_TEST_BIT: Select test input for boc. Requires BOC_ENABLE to be 1. Writes to BOC_TEST_ADDR supply data"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_BOC_TEST_BIT "));
        write(fline,ctlBocTestBit);
        writeline(mappingFile,fline);
        write(fline,string'("// Histogrammer bits"));
        writeline(mappingFile,fline);
        
        write(fline,string'("// CTL_HIST_MODE uses multiple bits with sperate offsets for histo 0 and 1"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_HISTO_MODE_BITS "));
        write(fline,readoutModeBits);
        write(fline,string'(" // number of bits to set mode: "));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE0_BASE "));
        write(fline,ctlHistMode0Base);
        write(fline,string'(" // bit shift for histo 0 mode"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE1_BASE "));
        write(fline,ctlHistMode1Base);
        write(fline,string'(" // bit shift for histo 1 mode"));
        writeline(mappingFile,fline);
        
        write(fline,string'("// CTL_HIST_MODE definitions"));
        writeline(mappingFile,fline);
        write(fline,string'("// Insert this value at the base bit position of the histogrammer"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE_IDLE "));
        write(fline,conv_integer(modeIdle));
        write(fline,string'(" // Idle is wait state prior to sampling"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE_SAMPLE "));
        write(fline,conv_integer(modeSample));
        write(fline,string'(" // Sampling"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE_WAIT "));
        write(fline,conv_integer(modeReadoutWait));
        write(fline,string'(" // Wait is wait state prior to readout"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MODE_READOUT "));
        write(fline,conv_integer(modeReadout));
        write(fline,string'(" // Readout transfer"));
        writeline(mappingFile,fline);

        write(fline,string'("// CTL_HIST_MUX: 0: test input via write to MB_HISTx_TEST_REG. 1: EFB input"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MUX0_BIT "));
        write(fline,ctlHistMux0Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_HIST_MUX1_BIT "));
        write(fline,ctlHistMux1Bit);
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_MASTER_MODE_BIT "));
        write(fline,ctlMasterModeBit);
	write(fline,string'(" // read-only"));
        writeline(mappingFile,fline);
        write(fline,string'("#define MB_CTL_MASTER_RESET_BIT "));
        write(fline,ctlBusRstBit);
	write(fline,string'(" // read-only"));
        writeline(mappingFile,fline);
	write(fline,string'("#endif //ROD_SLAVE_H"));
        writeline(mappingFile,fline);
        file_close(mappingFile);
		  dumpIt <= 0;
		  end if;
	end process;
	end generate;

  -- force ddr2 chip select
  	mcbx_dram_cs_n <= '0';
		
	theGitId: gitId PORT MAP(
			reset => gitIdReset,
			read => gitIdRead,
			id => gitIdByte
		);
		process
		begin
			wait until rising_edge(cpuclk);
			-- write signal => reset
			if axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0' and 
						axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = DESIGN_ID_ADDR then 
					gitIdReset <= '1';
			else 
					gitIdReset <= '0';
			end if;
			-- read signal
			if axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '1' and 
						axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = DESIGN_ID_ADDR then 
					gitIdRead <= '1';
			else 
					gitIdRead <= '0';
			end if;
		end process;

		-- local registers and rod bus ---------------------
	
		-- epc without wait states
		axi_epc_0_PRH_Rdy_pin <= '1'; -- no wait states
		 
		localCtlRegProc: process(cpuclk, cpurst)
		begin
			if cpurst = '1' then
				localCtlReg <= (others => '0');
			elsif rising_edge(cpuclk) then
				if axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = CTL_REG_ADDR then 
					localCtlReg <= axi_epc_0_PRH_Data_O_pin;
				end if;
			end if;
		end process;

		-- the effective control register depends on the master control register
		-- upper 2 bits are read only for slave
		ctlRegBlock: block
		begin
			effectiveCtlReg(31 downto ctlMasterBase) <= busCtlReg(31 downto ctlMasterBase);
			effectiveCtlReg(ctlMasterBase - 1 downto  0) <= busCtlReg(ctlMasterBase - 1 downto  0) 
														when busCtlReg(ctlMasterModeBit) = '1' else 
															localCtlReg(ctlMasterBase - 1 downto 0);
		end block;


		-- map status register
		localStatReg(statSimBit) <= '1' when simulation else '0';
		localStatReg(statExtRamBit) <= '1' when useExtRam else '0';

		localStatReg(statDma0RdyBit) <= axi_histo0_dma_s_axis_s2mm_tready_pin;
		localStatReg(statDma1RdyBit) <= axi_histo1_dma_s_axis_s2mm_tready_pin;
		
		localStatReg(statHistoRfr0Bit) <= rfr0;
		localStatReg(statHistoRfr1Bit) <= rfr1;
		localStatReg(statHistoRfd0Bit) <= rfd0;
		localStatReg(statHistoRfd1Bit) <= rfd1;
		localStatReg(statHistoRoc0Bit) <= roc0;
		localStatReg(statHistoRoc1Bit) <= roc1;



		-- remaining bits used for dmov data (if used at all)
		localStatReg(statLastBit - 1 downto 0) <= dmovData(statLastBit - 1 downto 0);

		-- copy status to master
		busStatReg <= localStatReg;
		
		-- readback registers
			-- control register
		axi_epc_0_PRH_Data_I_pin <= effectiveCtlReg when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = CTL_REG_ADDR else 
			-- status register
			localStatReg when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0)= STAT_REG_ADDR else 
			-- histo0 control register
			localHistCtlReg0 when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0)= HIST_0_CTL_ADDR else 
			-- histo1 control register
			localHistCtlReg1 when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0)= HIST_1_CTL_ADDR else 
			-- git  id
			X"000000" & gitIdByte when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and 
					axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0)= DESIGN_ID_ADDR else 
			-- bus ram
			busRamData when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '1' else 
			(others => '1');
--	
	-----------------------
	-- rod bus interface
	busRamWr <= '1' when axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '1' else '0';

	-- drive local reset from input or bus reset
	-- however make sure bus reset is deasserted automatically 
	-- and not by cpu output clock (will stop during reset)
	-- use cpuclk_in for process
	rstDelProc:process
		variable rstDelCnt: integer range 0 to 100;
		variable rstDone: std_logic ;
	begin
		wait until rising_edge(clk100);
		if busCtlReg(ctlBusRstBit) = '1' then
			if rstDone = '0' then
				rstPulse <= '1';
			end if;
		else
			rstDone := '0';
			rstPulse <= '0';
			rstDelCnt := 0;
		end if;
		if rstPulse = '1' and rstDone = '0' then
			rstDelCnt := rstDelCnt + 1;
		end if;
		if rstDelCnt = 90 then
			rstDone := '1';
			rstPulse <= '0';
		end if;
	end process;

	localRst <= '1' when  ( reset ='1' and reset_processor_core_SEL  = '1') or rstPulse = '1' else '0';	
		
		
	------------
	-- boc input test. boc input runs at 80 mhz, we emust use fifo to cross clock domains
	theBocTestFifo : bocTestFifo
	  PORT MAP (
		 rst => cpurst,
		 wr_clk => cpuclk,
		 rd_clk => clk80,
		 din => axi_epc_0_PRH_Data_O_pin(testBocData'length - 1 downto 0),
		 wr_en => bocTestWr,
		 rd_en => testBocEnable,
		 dout => testBocData,
		 full => open,
		 empty => bocTestEmpty
	  );

	mode0 <= effectiveCtlReg(ctlHistMode0Base + readoutModeBits - 1 downto ctlHistMode0Base);
	mode1 <= effectiveCtlReg(ctlHistMode1Base + readoutModeBits - 1 downto ctlHistMode1Base);


	bocTestWr <= '1' when axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' 
							and axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0' 
							and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = BOC_TEST_ADDR else '0';
							
	testBocEnable <= not bocTestEmpty;

	------------


    -- histo mux. Test write with histMux bit = 0. else boc data
    -- add pipeline in front of mux for real data
    process
    begin
        wait until rising_edge(cpuclk);
			  hitValid0_i_r0 	<= hitValid0_i;
			  row0_i_r0 		<= row0_i;
			  col0_i_r0 		<= col0_i;
			  chip0_i_r0 		<= chip0_i;
			  totVal0_i_r0 	<= totVal0_i;

			  hitValid1_i_r0 	<= hitValid1_i;
			  row1_i_r0 		<= row1_i;
			  col1_i_r0 		<= col1_i;
			  chip1_i_r0 		<= chip1_i;
			  totVal1_i_r0 	<= totVal1_i;

			  hitValid0_i_r1 	<= hitValid0_i_r0;
			  row0_i_r1 		<= row0_i_r0;
			  col0_i_r1 		<= col0_i_r0;
			  chip0_i_r1 		<= chip0_i_r0;
			  totVal0_i_r1 	<= totVal0_i_r0;

			  hitValid1_i_r1 	<= hitValid1_i_r0;
			  row1_i_r1 		<= row1_i_r0;
			  col1_i_r1 		<= col1_i_r0;
			  chip1_i_r1 		<= chip1_i_r0;
			  totVal1_i_r1 	<= totVal1_i_r0;
    end process;
   
    -- use pipeline data in mux

    hist0TestEnable <= '1' when effectiveCtlReg(ctlHistMux0Bit) = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0'
                            and axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0'
                            and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = HIST_0_TEST_ADDR else '0';
    hitEnable0     	<= hitValid0_i_r1 when effectiveCtlReg(ctlHistMux0Bit) = '1'
                        else '1' when hist0TestEnable = '1' else '0';
    row0             <= row0_i_r1 when effectiveCtlReg(ctlHistMux0Bit) = '1' 
								else axi_epc_0_PRH_Data_O_pin(rowBits - 1 downto 0) when hist0TestEnable = '1' else (others => '0');
    col0             <= col0_i_r1 when effectiveCtlReg(ctlHistMux0Bit) = '1' 
								else axi_epc_0_PRH_Data_O_pin(colBits + rowBits - 1 downto rowBits) when hist0TestEnable = '1' else (others => '0');
    chip0            <= chip0_i_r1 when effectiveCtlReg(ctlHistMux0Bit) = '1'
                        else axi_epc_0_PRH_Data_O_pin(chipBits + colBits + rowBits - 1 downto rowBits + colBits) when hist0TestEnable = '1' 
								else (others => '0');
    totVal0         	<= totVal0_i_r1 when effectiveCtlReg(ctlHistMux0Bit) = '1'
                        else axi_epc_0_PRH_Data_O_pin(totWidth + chipBits + colBits + rowBits - 1 downto rowBits + colBits + chipBits)
								when hist0TestEnable = '1' else (others => '0');

    hist1TestEnable 	<= '1' when effectiveCtlReg(ctlHistMux1Bit) = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0'
                            and axi_epc_0_PRH_CS_n_pin = '0' and axi_epc_0_PRH_RNW_pin = '0' 
									 and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = HIST_1_TEST_ADDR else '0';
    hitEnable1     	<= hitValid1_i_r1 when effectiveCtlReg(ctlHistMux1Bit) = '1'
                        else '1' when hist1TestEnable = '1' else '0';
    row1             <= row1_i_r1 when effectiveCtlReg(ctlHistMux1Bit) = '1' 
								else axi_epc_0_PRH_Data_O_pin(rowBits - 1 downto 0) when hist1TestEnable = '1' else (others => '0');
    col1             <= col1_i_r1 when effectiveCtlReg(ctlHistMux1Bit) = '1'
                        else axi_epc_0_PRH_Data_O_pin(colBits + rowBits - 1 downto rowBits) when hist1TestEnable = '1' else (others => '0');
    chip1            <= chip1_i_r1 when effectiveCtlReg(ctlHistMux1Bit) = '1'
                        else axi_epc_0_PRH_Data_O_pin(chipBits + colBits + rowBits - 1 downto rowBits + colBits) when hist1TestEnable = '1' 
								else (others => '0');
    totVal1         	<= totVal1_i_r1 when effectiveCtlReg(ctlHistMux1Bit) = '1'
                        else axi_epc_0_PRH_Data_O_pin(totWidth + chipBits + colBits + rowBits - 1 downto rowBits + colBits + chipBits)
								when hist1TestEnable = '1' else (others => '0');

					
   -- histo input pipeline
	histInPipe1: process
	begin
		wait until rising_edge(cpuclk);
		hitEnable0_a <= hitEnable0;
		col0_a <= col0;
		row0_a <= row0;
		chip0_a <= chip0;
		totVal0_a <= totVal0;
		
		hitEnable1_a <= hitEnable1;
		col1_a <= col1;
		row1_a <= row1;
		chip1_a <= chip1;
		totVal1_a <= totVal1;
	end process;

   histInPipe2: process
	begin
		wait until rising_edge(cpuclk);
		hitEnable0_b <= hitEnable0_a;
		col0_b <= col0_a;
		row0_b <= row0_a;
		chip0_b <= chip0_A;
		totVal0_b <= totVal0_a;
		
		hitEnable1_b <= hitEnable1_a;
		col1_b <= col1_a;
		row1_b <= row1_a;
		chip1_b <= chip1_a;
		totVal1_b <= totVal1_a;
	end process;

   histInPipe3: process
	begin
		wait until rising_edge(cpuclk);
		hitEnable0_r <= hitEnable0_b;
		col0_r <= col0_b;
		row0_r <= row0_b;
		chip0_r <= chip0_b;
		totVal0_r <= totVal0_b;
		
		hitEnable1_r <= hitEnable1_b;
		col1_r <= col1_b;
		row1_r <= row1_b;
		chip1_r <= chip1_b;
		totVal1_r <= totVal1_b;
	end process;

	-- readout ready when histogrammer in readout mode
	rfr0 			<= axi_histo0_dma_s_axis_s2mm_tready_pin when mode0 = modeReadout else '0';
	rfr1 			<= axi_histo1_dma_s_axis_s2mm_tready_pin when mode1 = modeReadout else '0';

	--
	histCtlRegs: process
	begin
		wait until rising_edge(cpuclk);
		if axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and axi_epc_0_PRH_CS_n_pin = '0' 
				and axi_epc_0_PRH_RNW_pin = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = HIST_0_CTL_ADDR then
			localHistCtlReg0 <= axi_epc_0_PRH_Data_O_pin;
		end if;
		if axi_epc_0_PRH_Addr_pin(RANGE_SIZE) = '0' and axi_epc_0_PRH_CS_n_pin = '0' 
				and axi_epc_0_PRH_RNW_pin = '0' and axi_epc_0_PRH_Addr_pin(RANGE_SIZE - 1 downto 0) = HIST_1_CTL_ADDR then
			localHistCtlReg1 <= axi_epc_0_PRH_Data_O_pin;
		end if;
	end process;
	

	------------------------------------------
	-- dma signal assignements
		axi_histo0_dma_s_axis_s2mm_tkeep_pin <= (others => '1'); -- can be fixed to 1111
		axi_histo1_dma_s_axis_s2mm_tkeep_pin <= (others => '1'); -- can be fixed to 1111

	-- the "last" signal must be asserted together with the last data beat. 
		axi_histo0_dma_s_axis_s2mm_tlast_pin <= roc0 and histValid0 when mode0 = modeReadout else '0'; 
		axi_histo1_dma_s_axis_s2mm_tlast_pin <= roc1 and histValid1 when mode1 = modeReadout else '0'; 

-- mux to select from the correct sources for valid and data
	dmaMux: process (mode0, mode1, histo0, histValid0, histo1, histValid1)
	begin
		-- valid: muxed
		-- hist0 0
		if mode0 = modeReadout then
			axi_histo0_dma_s_axis_s2mm_tvalid_pin <= histValid0;
		else
			axi_histo0_dma_s_axis_s2mm_tvalid_pin <= '0';
		end if;
		-- histo 1
		if mode1 = modeReadout then
			axi_histo1_dma_s_axis_s2mm_tvalid_pin <= histValid1;
		else
			axi_histo1_dma_s_axis_s2mm_tvalid_pin <= '0';
		end if;

	end process;

	-- data: direct mapped 
	axi_histo0_dma_s_axis_s2mm_tdata_pin <= histo0;
	axi_histo1_dma_s_axis_s2mm_tdata_pin <= histo1;

--

	d2: if not simulation generate
	begin
	--- Slave Register Header Generator ---
	dump_vars2 : process(dumpIt2)
   variable fline: line;
	begin
		if dumpit2 = 1 then
			file_open(mappingFile2,"../inc/rodSlaveReg.hxx",WRITE_MODE);
			write(fline,string'(""));
			write(fline,string'("// IBL ROD Spartan-6 slave firmware registers and bit definitions"));
				writeline(mappingFile2,fline);
			write(fline,string'("// This is a generated file, do not edit!"));
				writeline(mappingFile2,fline);	  
			write(fline,string'(""));
				writeline(mappingFile2,fline);
			write(fline,string'("#ifndef ROD_SLAVE_REG_H"));
				writeline(mappingFile2,fline);
			write(fline,string'("#define ROD_SLAVE_REG_H"));
				writeline(mappingFile2,fline);
			write(fline,string'("#define SLV_VHDL_COMPILE_DATE "));
			write(fline,"""" & compileDate & """");
				writeline(mappingFile2,fline);
			write(fline,string'(""));
				writeline(mappingFile2,fline);
			  
			-- Slave dual port memory area
			write(fline,string'("// Slave Communication RAM "));
			writeline(mappingFile2,fline);
			write(fline,string'("#define SLV_COMMUNICATION_DPR_BASE "));        
			write(fline,"(0x" & str((conv_integer(ram_range) * 2**(addr_width - range_bits)),16) &")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define SLV_COMMUNICATION_DPR_WORDS "));        
			write(fline,"(" & str(2**ram_word_size) & ")");
			writeline(mappingFile2,fline);
			write(fline,string'(""));
			writeline(mappingFile2,fline);

			-- Slave control status
		  write(fline,string'("// Slave control status registers "));
		  writeline(mappingFile2,fline);
        write(fline,string'("#define SLV_CTL_REG "));
		  write(fline,"(0x" & str(conv_integer(slvCtrlReg),16)&")");
        writeline(mappingFile2,fline);
        write(fline,string'("#define SLV_CTL_RESET_BIT "));
        write(fline,ctlBusRstBit);
        writeline(mappingFile2,fline);
        write(fline,string'("#define SLV_CTL_MASTER_MODE_BIT "));
        write(fline,ctlMasterModeBit);
        writeline(mappingFile2,fline);
        write(fline,string'("#define SLV_STAT_REG "));
		  write(fline,"(0x" & str(conv_integer(slvStatReg),16)&")");
        writeline(mappingFile2,fline);
		  write(fline,string'(""));
		  writeline(mappingFile2,fline);

			-- Slave Global Variable Registers
			write(fline,string'("// Slave Global Variable Register Definitions "));
				writeline(mappingFile2,fline);
			write(fline,string'("#define FMT_LINK_ENABLE "));        
			write(fline,"(0x" & str(conv_integer(fmtLinkEnable),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define SLAVE_ID "));        
			write(fline,"(0x" & str(conv_integer(slaveID),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define CALIBRATION_MODE "));        
			write(fline,"(0x" & str(conv_integer(calibMode),16)&")");
				writeline(mappingFile2,fline);			
         write(fline,string'("#define TRIGGER_COUNT_NUMBER "));
         write(fline,"(0x" & str(conv_integer(fmtTrigCntReg),16)&")");
            writeline(mappingFile2,fline);
       write(fline,string'("#define HIT_DISC_CONFIG "));
       write(fline,"(0x" & str(conv_integer(HitDiscCnfgReg),16)&")");
          writeline(mappingFile2,fline);
			write(fline,string'("#define FORMAT_VERSION "));        
			write(fline,"(0x" & str(conv_integer(formatVersion),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define SOURCE_ID "));        
			write(fline,"(0x" & str(conv_integer(sourceID),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define RUN_NUMBER "));        
			write(fline,"(0x" & str(conv_integer(runNumber),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define CODE_VERSION "));        
			write(fline,"(0x" & str(conv_integer(codeVersion),16)&")");
				writeline(mappingFile2,fline);					  
			write(fline,string'(""));
				writeline(mappingFile2,fline);
			  
			-- Slave BUSY Registers  
			write(fline,string'("// Slave BUSY Registers Definitions "));
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_REG "));        
			write(fline,"(0x" & str(conv_integer(slvBsyReg),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(slvBsyStatusReg),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_MASK_REG "));        
			write(fline,"(0x" & str(conv_integer(slvBsyMaskReg),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FORCE_REG "));        
			write(fline,"(0x" & str(conv_integer(slvBsyForceReg),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_SLINK0LFF_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsySlink0LffHist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_SLINK0DOWN_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsySlink0LdownHist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_SLINK1LFF_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsySlink1LffHist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_SLINK1DOWN_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsySlink1LdownHist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_EFB1STTS0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyEfb1Stts0Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_EFB1STTS1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyEfb1Stts1Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_EFB2STTS0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyEfb2Stts0Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_EFB2STTS1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyEfb2Stts1Hist),16)&")");			
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_ROUTER0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyRouter0Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_ROUTER1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyRouter1Hist),16)&")");			
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_HISTFULL0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyHistFull0Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_HISTFULL1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyHistFull1Hist),16)&")");			
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_HISTOVERRUN0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyHistOverrun0Hist),16)&")");
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_HISTOVERRUN1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyHistOverrun1Hist),16)&")");			
			writeline(mappingFile2,fline);
			write(fline,string'("#define BUSY_OUT_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyOutHist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT0FF0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt0FF0Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT0FF1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt0FF1Hist),16)&")");
			writeline(mappingFile2,fline); 
         write(fline,string'("#define BUSY_FMT0FF2_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt0FF2Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT0FF3_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt0FF3Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT1FF0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt1FF0Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT1FF1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt1FF1Hist),16)&")");
			writeline(mappingFile2,fline); 
         write(fline,string'("#define BUSY_FMT1FF2_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt1FF2Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT1FF3_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt1FF3Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT2FF0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt2FF0Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT2FF1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt2FF1Hist),16)&")");
			writeline(mappingFile2,fline); 
         write(fline,string'("#define BUSY_FMT2FF2_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt2FF2Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT2FF3_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt2FF3Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT3FF0_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt3FF0Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT3FF1_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt3FF1Hist),16)&")");
			writeline(mappingFile2,fline); 
         write(fline,string'("#define BUSY_FMT3FF2_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt3FF2Hist),16)&")");
			writeline(mappingFile2,fline);
         write(fline,string'("#define BUSY_FMT3FF3_HIST "));        
			write(fline,"(0x" & str(conv_integer(slvBsyFmt3FF3Hist),16)&")");
			writeline(mappingFile2,fline);

			-- Slave INMEM FIFO Registers
			write(fline,string'("// Slave INMEM FIFO Register Definitions "));
				writeline(mappingFile2,fline);
			write(fline,string'("#define INMEM_FIFO_CH_SEL "));        
			write(fline,"(0x" & str(conv_integer(inmemFifoChSel),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define INMEM_FIFO_RST "));        
			write(fline,"(0x" & str(conv_integer(inmemFifoRst),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define INMEM_FIFO_DATA "));        
			write(fline,"(0x" & str(conv_integer(inmemFifoData),16)&")");
				writeline(mappingFile2,fline);					  
			write(fline,string'(""));
				writeline(mappingFile2,fline);

			-- ROD Slave Formatter Registers
			write(fline,string'("// Slave Formatter Register Definitions "));
				writeline(mappingFile2,fline);
			write(fline,string'("#define FMT_READOUT_TIMEOUT_LIMIT "));        
			write(fline,"(0x" & str(conv_integer(fmtReadoutTimeOutLimit),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define FMT_DATA_OVERFLOW_LIMIT "));        
			write(fline,"(0x" & str(conv_integer(fmtDataOverflowLimit),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define FMT_HEADER_TRAILER_LIMIT "));        
			write(fline,"(0x" & str(conv_integer(fmtHeaderTrailerLimit),16)&")");
				writeline(mappingFile2,fline);					  
			write(fline,string'("#define FMT_ROD_BUSY_LIMIT "));        
			write(fline,"(0x" & str(conv_integer(fmtRodBusyLimit),16)&")");
				writeline(mappingFile2,fline);
         write(fline,string'("#define FMT_TRAILER_TIMEOUT_LIMIT "));        
			write(fline,"(0x" & str(conv_integer(fmtTrailerTimeOutLimit),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'(""));
				writeline(mappingFile2,fline);				
			write(fline,string'("#define FMT_CTRL_REG "));        
			write(fline,"(0x" & str(conv_integer(fmtCtrlReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_0_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt0SttsReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_1_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt1SttsReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_2_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt2SttsReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_3_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt3SttsReg),16)&")");
				writeline(mappingFile2,fline);
         write(fline,string'("#define FMT_0_OCCUPANCY_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt0OccupancyReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_1_OCCUPANCY_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt1OccupancyReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_2_OCCUPANCY_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt2OccupancyReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_3_OCCUPANCY_REG "));        
			write(fline,"(0x" & str(conv_integer(fmt3OccupancyReg),16)&")");
				writeline(mappingFile2,fline);
         write(fline,string'("#define FORMATTTER0_FRAMES_REG "));        
         write(fline,"(0x" & str(conv_integer(Formatter0FramesReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FORMATTTER1_FRAMES_REG "));        
			write(fline,"(0x" & str(conv_integer(Formatter1FramesReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FORMATTTER2_FRAMES_REG "));        
			write(fline,"(0x" & str(conv_integer(Formatter2FramesReg),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FORMATTTER3_FRAMES_REG "));        
			write(fline,"(0x" & str(conv_integer(Formatter3FramesReg),16)&")");
				writeline(mappingFile2,fline);

			write(fline,string'(""));
				writeline(mappingFile2,fline);				
			write(fline,string'("#define FMT_READOUT_TIMEOUT_ERR "));        
			write(fline,"(0x" & str(conv_integer(fmtReadoutTimeoutErr),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_DATA_OVERFLOW_ERR "));        
			write(fline,"(0x" & str(conv_integer(fmtDataOverflowErr),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_HEADER_TRAILER_ERR "));        
			write(fline,"(0x" & str(conv_integer(fmtHeaderTrailerErr),16)&")");
				writeline(mappingFile2,fline);		
			write(fline,string'("#define FMT_ROD_BUSY_ERR "));        
			write(fline,"(0x" & str(conv_integer(fmtRodBusyErr),16)&")");
				writeline(mappingFile2,fline);		 	
			write(fline,string'(""));
				writeline(mappingFile2,fline);

         --Error Counter Registers
         write(fline,string'("#define LINK0_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link0ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK1_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link1ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK2_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link2ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK3_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link3ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK4_ERRORS_REG ")); 
			write(fline,"(0x" & str(conv_integer(Link4ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK5_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link5ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK6_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link6ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK7_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link7ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK8_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link8ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK9_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link9ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK10_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link10ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK11_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link11ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK12_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link12ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK13_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link13ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK14_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link14ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define LINK15_ERRORS_REG "));        
			write(fline,"(0x" & str(conv_integer(Link15ErrorsReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define HEADER_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(HeaderErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define TRAILER_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(TrailerErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define RD_TIMEOUT_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(RDTimeoutErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define HT_LIMIT_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(HTLimitErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define ROW_COL_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(RowColErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define L1_ID_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(L1IDErrorReg),16)&")");
			   writeline(mappingFile2,fline);
         write(fline,string'("#define BC_ID_ERROR_REG "));        
			write(fline,"(0x" & str(conv_integer(BCIDErrorReg),16)&")");
			   writeline(mappingFile2,fline);

			-- ROD Slave 0 EFB Error Mask Registers
			write(fline,string'("// Slave 0 EFB Error Mask Register Definitions "));
				writeline(mappingFile2,fline);
			write(fline,string'("// The following definition is probably not correc and should define only 16 links (per slave). to be checked ... "));
				writeline(mappingFile2,fline);
			for i1 in 0 to 2**linkErrGroupBits - 1 loop
				for i2 in 0 to 2**linkSelBits - 1 loop
					write(fline,string'("#define LINK_") & str(i1* 2**linkSelBits + i2) & string'("_ERR_MASK "));
					--write(fline, str(conv_integer(erm_range) * 2**(addr_width - range_bits) i1 * 2**linkErrGroupShift + i2 * 2**linkSelShift));
					write(fline, str(conv_integer(erm_range) * 2**(addr_width - range_bits) + i1 * 2**linkErrGroupShift + i2 * 2**linkSelShift));
					writeline(mappingFile2,fline);
				end loop;
			end loop;
			write(fline,string'(""));
				writeline(mappingFile2,fline);
				
--
			-- Slave EFB General Registers
			write(fline,string'("// Slave EFB General Register Definitions "));
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_1_CTRL_REG "));        
			write(fline,"(0x" & str(conv_integer(efb1CtrlReg),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_CTRL_REG "));        
			write(fline,"(0x" & str(conv_integer(efb2CtrlReg),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_1_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb1SttsReg),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb2SttsReg),16)&")");
				writeline(mappingFile2,fline);				
			write(fline,string'(""));
				writeline(mappingFile2,fline);
				
			write(fline,string'("#define EFB_1_EVENT_HEADER "));        
			write(fline,"(0x" & str(conv_integer(efb1EvtHeader),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_EVENT_HEADER "));        
			write(fline,"(0x" & str(conv_integer(efb2EvtHeader),16)&")");
				writeline(mappingFile2,fline);					
			write(fline,string'("#define EFB_1_GROUP_EVENT_COUNT "));        
			write(fline,"(0x" & str(conv_integer(efb1EvtCount),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_GROUP_EVENT_COUNT "));        
			write(fline,"(0x" & str(conv_integer(efb2EvtCount),16)&")");
				writeline(mappingFile2,fline);					
			write(fline,string'(""));
				writeline(mappingFile2,fline);
				
			write(fline,string'("#define EFB_1_OUT_FIFO_RST "));        
			write(fline,"(0x" & str(conv_integer(efb1OutFifoRst),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_OUT_FIFO_RST "));        
			write(fline,"(0x" & str(conv_integer(efb2OutFifoRst),16)&")");
				writeline(mappingFile2,fline);					
			write(fline,string'("#define EFB_1_OUT_FIFO_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb1OutFifoSttsReg),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_OUT_FIFO_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb2OutFifoSttsReg),16)&")");
				writeline(mappingFile2,fline);				
			write(fline,string'(""));
				writeline(mappingFile2,fline);
				
			write(fline,string'("#define EFB_1_L1BCID_TRAPPED_VAL "));        
			write(fline,"(0x" & str(conv_integer(efb1L1BcTrappedVal),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_L1BCID_TRAPPED_VAL "));        
			write(fline,"(0x" & str(conv_integer(efb2L1BcTrappedVal),16)&")");
				writeline(mappingFile2,fline);					
			write(fline,string'("#define EFB_1_MISC_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb1MiscSttsReg),16)&")");
				writeline(mappingFile2,fline);
			write(fline,string'("#define EFB_2_MISC_STATUS_REG "));        
			write(fline,"(0x" & str(conv_integer(efb2MiscSttsReg),16)&")");
				writeline(mappingFile2,fline);					
			write(fline,string'(""));
				writeline(mappingFile2,fline);				
				
			-- Slave Router Registers
	
			-- Slave Histogrammer Registers	

			-- Slave Error Status Registers  
			
			write(fline,string'("#endif //ROD_SLAVE_REG_H"));
			writeline(mappingFile2,fline);
			file_close(mappingFile2);
			dumpIt2 <= 0;
		end if;
	end process;		  
	end generate;


end Behavioral;
