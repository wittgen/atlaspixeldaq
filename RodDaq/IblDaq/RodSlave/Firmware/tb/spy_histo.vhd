----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:29:37 01/17/2014 
-- Design Name: 
-- Module Name:    spy_histo - arch 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library ieee;
library modelsim_lib;
use ieee.std_logic_1164.all;
use modelsim_lib.util.all;
use work.rodHistoPack.all;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity spy_histo is
generic (histo_path : string := ""); 
end spy_histo;

architecture arch of spy_histo is

constant dma_out_path : string := histo_path & "/histo_r";
signal histo_r: std_logic_vector(dmaWidth - 1 downto 0);

begin

spy_process: process
begin
	init_signal_spy(dma_out_path,"/spy_histo/histo_r",1);
end process spy_process;


end arch;

