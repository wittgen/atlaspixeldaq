--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:34:41 05/22/2014
-- Design Name:   
-- Module Name:   /home/kugel/daten/work/projekte/atlas/iblBoc/sw/ibldaq/RodDaq/IblDaq/RodSlave/Firmware/ISE/addrConv_tb.vhd
-- Project Name:  ibl_rodSlave
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: addrConverter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


use work.rodHistoPack.all;

 
ENTITY addrConv_tb IS
END addrConv_tb;
 
ARCHITECTURE behavior OF addrConv_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT addrConverter
    PORT(
         control : IN  std_logic_vector(31 downto 0);
         valid : IN  std_logic;
         hit : OUT  std_logic;
         addr : OUT  std_logic_vector(17 downto 0);
         row : IN  std_logic_vector(8 downto 0);
         col : IN  std_logic_vector(6 downto 0);
         chip : IN  std_logic_vector(2 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal control : std_logic_vector(31 downto 0) := (others => '0');
   signal valid : std_logic := '0';
   signal row : std_logic_vector(8 downto 0) := (others => '0');
   signal col : std_logic_vector(6 downto 0) := (others => '0');
   signal chip : std_logic_vector(2 downto 0) := (others => '0');
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal hit : std_logic;
   signal addr : std_logic_vector(17 downto 0);

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	
	-- columns
	constant cmax: integer := 4; -- 80;
	constant rmax: integer := 16; -- 336;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: addrConverter PORT MAP (
          control => control,
          valid => valid,
          hit => hit,
          addr => addr,
          row => row,
          col => col,
          chip => chip,
          rst => rst,
          clk => clk
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- reset process
   rs_proc: process
   begin		
      -- hold reset state for 100 ns.
		rst <= '1';
      wait for 100 ns;	
		rst <= '0';

      wait;
   end process;

   -- Stimulus process
   stim_proc: process -- (clk,rst)
	variable maxStep: integer;
   begin		
		
		wait until rst = '0'; -- then

			-- mask step mode
			for im in 4 to 4 loop
				if im = 1 then 
					control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMaskNone;
					maxStep := 1;
				elsif im = 2 then 
					control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMask2;
					maxStep := 2;
				elsif im = 3 then
					control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMask4;
					maxStep := 4;
				else 
					control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMask8;
					maxStep := 8;
				end if;
				
				-- mask step index
				for ii in 0 to maxStep loop -- 9 loop
					if ii = 1 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep8;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep1;
					elsif ii = 2 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep7;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep2;
					elsif ii = 3 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep6;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep3;
					elsif ii = 4 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep5;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep4;
					elsif ii = 5 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep4;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep5;
					elsif ii = 6 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep3;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep6;
					elsif ii = 7 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep2;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep7;
					elsif ii = 8 then 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlStep1;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlStep8;
					else 
						control(mstepIndexCtlShiftOdd + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftOdd) <= mstepIndexCtlIgnore;
						control(mstepIndexCtlShiftEven + mstepIndexCtlBits - 1 downto mstepIndexCtlShiftEven) <= mstepIndexCtlIgnore;
					end if;
					
					-- columns
					for ic in 1 to cmax + 1 loop
						-- rows
						for ir in 1 to rmax + 1 loop
					
							col <= std_logic_vector(conv_unsigned(ic,colBits));
							row <= std_logic_vector(conv_unsigned(ir,rowBits));
							if (ic <= cmax) and (ir <= rmax) then 
								valid <= '1';
							else
								valid <= '0';
							end if;
						wait until rising_edge(clk);	
						end loop;
					end loop;
				end loop;
			end loop;

		-- end if;

   end process;

END;
