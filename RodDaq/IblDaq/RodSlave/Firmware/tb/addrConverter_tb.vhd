--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   19:12:27 01/19/2013
-- Design Name:   
-- Module Name:   /home/mkretz/daq/RodDaq/IblDsp/Firmware/sp6fmt/addrConverter_tb.vhd
-- Project Name:  sp6fmt
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: addrConverter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
use work.rodHistoPack.all;
USE ieee.numeric_std.ALL;
 
ENTITY addrConverter_tb IS
END addrConverter_tb;
 
ARCHITECTURE behavior OF addrConverter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT addrConverter
    PORT(
         control : IN  std_logic_vector(31 downto 0);
         valid : IN  std_logic;
         hit : OUT  std_logic;
         addr : OUT  std_logic_vector(17 downto 0);
         row : IN  std_logic_vector(8 downto 0);
         col : IN  std_logic_vector(6 downto 0);
         chip : IN  std_logic_vector(2 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal control : std_logic_vector(31 downto 0) := (others => '0');
   signal valid : std_logic := '0';
   signal row : std_logic_vector(8 downto 0) := "000000001";
   signal col : std_logic_vector(6 downto 0) := "0000001";
   signal chip : std_logic_vector(2 downto 0) := (others => '0');
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';

 	--Outputs
   signal hit : std_logic;
   signal addr : std_logic_vector(17 downto 0);


	signal rowint: integer range 0 to 336;
	signal colint: integer range 0 to 80;
	signal chipint : integer range 0 to 7;
	
   -- Clock period definitions
   constant clk_period : time := 10 ns;
 
 	procedure tic (count : integer := 1) is
	begin
		for i in 1 to count loop
		 wait until rising_edge(clk);
		end loop;
	end procedure;
	
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: addrConverter PORT MAP (
          control => control,
          valid => valid,
          hit => hit,
          addr => addr,
          row => row,
          col => col,
          chip => chip,
          rst => rst,
          clk => clk
        );

   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
		rst <= '1';
      tic(10);
		rst <= '0';
		tic(1);
		
      -- insert stimulus here 
		valid <= '1';
		control <= (others => '0');
		control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift)  <= rowMask4;
		control(chipCtlShift + chipCtlBits - 1 downto chipCtlShift)  <= chipCtlOne;

-- 0
		rowint <= 1;
		colint <= 1;
		chipint <= 0;
		tic(1);
-- 1
		rowint <= 1;
		colint <= 2;
		chipint <= 0;
		tic(1);
-- 7
		rowint <= 1;
		colint <= 8;
		chipint <= 0;
		tic(1);
-- 8
		rowint <= 1;
		colint <= 9;
		chipint <= 0;
		tic(1);
--79
		rowint <= 1;
		colint <= 80;
		chipint <= 0;
		tic(1);
--0		
		rowint <= 2;
		colint <= 1;
		chipint <= 0;
		tic(1);
--1
		rowint <= 2;
		colint <= 2;
		chipint <= 0;
		tic(1);
--79
		rowint <= 2;
		colint <= 80;
		chipint <= 0;
		tic(1);
--0
		rowint <= 8;
		colint <= 1;
		chipint <= 0;
		tic(1);
--1		
		rowint <= 8;
		colint <= 2;
		chipint <= 0;
		tic(1);
--79
		rowint <= 8;
		colint <= 80;
		chipint <= 0;
		tic(1);		
--80		
		rowint <= 9;
		colint <= 1;
		chipint <= 0;
		tic(1);
--81		
		rowint <= 9;
		colint <= 2;
		chipint <= 0;
		tic(1);
--159
		rowint <= 9;
		colint <= 80;
		chipint <= 0;
		tic(1);		
--3280		
		rowint <= 336;
		colint <= 1;
		chipint <= 0;
		tic(1);
--3281		
		rowint <= 336;
		colint <= 2;
		chipint <= 0;
		tic(1);
--3359
		rowint <= 336;
		colint <= 80;
		chipint <= 0;
		tic(1);		
		
		valid <= '0';
		tic(1);
		
      wait;
   end process;

row <= std_logic_vector(to_unsigned(rowint, row'length));
col <= std_logic_vector(to_unsigned(colint, col'length));
chip <= std_logic_vector(to_unsigned(chipint, chip'length));

END;
