--------------------------------------------------------------------------------
-- Testbench for occSel module
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;

use work.rodHistoPack.all;
USE ieee.numeric_std.ALL;
 
ENTITY occSel_tb IS
END occSel_tb;
 
ARCHITECTURE behavior OF occSel_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT occSel
    PORT(
         rst : IN  std_logic;
         clk : IN  std_logic;
         valid : IN  std_logic;
         datain : IN  std_logic_vector(occBits - 1 downto 0);
         dataout : OUT  std_logic_vector(31 downto 0);
         complete : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal valid : std_logic := '0';
   signal datain : std_logic_vector(occBits - 1 downto 0) := (others => '0');

 	--Outputs
   signal dataout : std_logic_vector(31 downto 0);
   signal complete : std_logic;

   -- Clock period definitions
   constant clk_period : time := 10 ns;
	constant reset_delay : time := 45 ns;
	
	procedure tic (count : integer := 1) is
	begin
		for i in 1 to count loop
		 wait until rising_edge(clk);
		end loop;
	end procedure;
BEGIN
 
	-- instantiate the Unit Under Test (UUT)
   uut: occSel PORT MAP (
          rst => rst,
          clk => clk,
          valid => valid,
          datain => datain,
          dataout => dataout,
          complete => complete
        );

   -- clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
	

   -- stimulus process
   stim_proc: process
   begin		
      -- hold reset state
		rst <= '1';
		valid <= '0';
      wait for reset_delay;	
		rst <= '0';
		tic;
		
		datain <= (others => '0');
		datain <= std_logic_vector(to_unsigned(10, 7));

		tic;
		valid <= '1';
		datain <= std_logic_vector(to_unsigned(3, 7));
		
		tic;
		datain <= std_logic_vector(to_unsigned(1, 7));
		
		tic(1);
		datain <= std_logic_vector(to_unsigned(30, 7));
		
		tic;
		datain <= std_logic_vector(to_unsigned(50, 7));
		
		valid <= '0';
		datain <= std_logic_vector(to_unsigned(127, 7));
		tic;
		valid <= '1';
		
		tic;
		datain <= std_logic_vector(to_unsigned(1, 7));
		
		tic;
		datain <= std_logic_vector(to_unsigned(2, 7));

		datain <= std_logic_vector(to_unsigned(5, 7));
		
		tic;
		datain <= std_logic_vector(to_unsigned(3, 7));

		tic;
		datain <= std_logic_vector(to_unsigned(4, 7));

      wait;
   end process;

END;
