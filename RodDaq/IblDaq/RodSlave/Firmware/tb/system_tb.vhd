-------------------------------------------------------------------------------
-- system_tb.vhd
-------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

-- START USER CODE (Do not remove this line)

-- User: Put your libraries here. Code in this
--       section will not be overwritten.

-- END USER CODE (Do not remove this line)

entity system_tb is
end system_tb;

architecture STRUCTURE of system_tb is

  constant cpuclk_in_PERIOD : time := 10000.000000 ps;
  constant RESET_in_LENGTH : time := 160000 ps;

  component system is
    port (
      rzq : inout std_logic;
      mcbx_dram_we_n : out std_logic;
      mcbx_dram_udqs_n : inout std_logic;
      mcbx_dram_udqs : inout std_logic;
      mcbx_dram_udm : out std_logic;
      mcbx_dram_ras_n : out std_logic;
      mcbx_dram_odt : out std_logic;
      mcbx_dram_ldm : out std_logic;
      mcbx_dram_dqs_n : inout std_logic;
      mcbx_dram_dqs : inout std_logic;
      mcbx_dram_dq : inout std_logic_vector(15 downto 0);
      mcbx_dram_clk_n : out std_logic;
      mcbx_dram_clk : out std_logic;
      mcbx_dram_cke : out std_logic;
      mcbx_dram_cas_n : out std_logic;
      mcbx_dram_ba : out std_logic_vector(2 downto 0);
      mcbx_dram_addr : out std_logic_vector(13 downto 0);
      RS232_Uart_1_sout : out std_logic;
      RS232_Uart_1_sin : in std_logic;
      RESET_in : in std_logic;
      ETHERNET_TX_ER : out std_logic;
      ETHERNET_TX_EN : out std_logic;
      ETHERNET_TX_CLK : out std_logic;
      ETHERNET_TXD : out std_logic_vector(7 downto 0);
      ETHERNET_RX_ER : in std_logic;
      ETHERNET_RX_DV : in std_logic;
      ETHERNET_RX_CLK : in std_logic;
      ETHERNET_RXD : in std_logic_vector(7 downto 0);
      ETHERNET_PHY_RST_N : out std_logic;
      ETHERNET_MII_TX_CLK : in std_logic;
      ETHERNET_MDIO : inout std_logic;
      ETHERNET_MDC : out std_logic;
      cpuclk : out std_logic;
      cpuclk2x : out std_logic;
      cpuclk_in : in std_logic;
      cpurst : out std_logic;
      axi_epc_0_PRH_CS_n_pin : out std_logic;
      axi_epc_0_PRH_Addr_pin : out std_logic_vector(21 downto 0);
      axi_epc_0_PRH_RNW_pin : out std_logic;
      axi_epc_0_PRH_Rdy_pin : in std_logic;
      axi_epc_0_PRH_Data_I_pin : in std_logic_vector(31 downto 0);
      axi_epc_0_PRH_Data_O_pin : out std_logic_vector(31 downto 0);
      axi_histo_dma_s_axis_s2mm_tready_pin : out std_logic;
      axi_histo_dma_s_axis_s2mm_tlast_pin : in std_logic;
      axi_histo_dma_s_axis_s2mm_tkeep_pin : in std_logic_vector(3 downto 0);
      axi_histo_dma_s_axis_s2mm_tdata_pin : in std_logic_vector(31 downto 0);
      axi_histo_dma_s_axis_s2mm_tvalid_pin : in std_logic
    );
  end component;

  -- Internal signals

  signal ETHERNET_MDC : std_logic;
  signal ETHERNET_MDIO : std_logic;
  signal ETHERNET_MII_TX_CLK : std_logic;
  signal ETHERNET_PHY_RST_N : std_logic;
  signal ETHERNET_RXD : std_logic_vector(7 downto 0);
  signal ETHERNET_RX_CLK : std_logic;
  signal ETHERNET_RX_DV : std_logic;
  signal ETHERNET_RX_ER : std_logic;
  signal ETHERNET_TXD : std_logic_vector(7 downto 0);
  signal ETHERNET_TX_CLK : std_logic;
  signal ETHERNET_TX_EN : std_logic;
  signal ETHERNET_TX_ER : std_logic;
  signal RESET_in : std_logic;
  signal RS232_Uart_1_sin : std_logic;
  signal RS232_Uart_1_sout : std_logic;
  signal axi_epc_0_PRH_Addr_pin : std_logic_vector(21 downto 0);
  signal axi_epc_0_PRH_CS_n_pin : std_logic;
  signal axi_epc_0_PRH_Data_I_pin : std_logic_vector(31 downto 0);
  signal axi_epc_0_PRH_Data_O_pin : std_logic_vector(31 downto 0);
  signal axi_epc_0_PRH_RNW_pin : std_logic;
  signal axi_epc_0_PRH_Rdy_pin : std_logic;
  signal axi_histo_dma_s_axis_s2mm_tdata_pin : std_logic_vector(31 downto 0);
  signal axi_histo_dma_s_axis_s2mm_tkeep_pin : std_logic_vector(3 downto 0);
  signal axi_histo_dma_s_axis_s2mm_tlast_pin : std_logic;
  signal axi_histo_dma_s_axis_s2mm_tready_pin : std_logic;
  signal axi_histo_dma_s_axis_s2mm_tvalid_pin : std_logic;
  signal cpuclk : std_logic;
  signal cpuclk2x : std_logic;
  signal cpuclk_in : std_logic;
  signal cpurst : std_logic;
  signal mcbx_dram_addr : std_logic_vector(13 downto 0);
  signal mcbx_dram_ba : std_logic_vector(2 downto 0);
  signal mcbx_dram_cas_n : std_logic;
  signal mcbx_dram_cke : std_logic;
  signal mcbx_dram_clk : std_logic;
  signal mcbx_dram_clk_n : std_logic;
  signal mcbx_dram_dq : std_logic_vector(15 downto 0);
  signal mcbx_dram_dqs : std_logic;
  signal mcbx_dram_dqs_n : std_logic;
  signal mcbx_dram_ldm : std_logic;
  signal mcbx_dram_odt : std_logic;
  signal mcbx_dram_ras_n : std_logic;
  signal mcbx_dram_udm : std_logic;
  signal mcbx_dram_udqs : std_logic;
  signal mcbx_dram_udqs_n : std_logic;
  signal mcbx_dram_we_n : std_logic;
  signal rzq : std_logic;

  -- START USER CODE (Do not remove this line)

  -- User: Put your signals here. Code in this
  --       section will not be overwritten.

  -- END USER CODE (Do not remove this line)

begin

  dut : system
    port map (
      rzq => rzq,
      mcbx_dram_we_n => mcbx_dram_we_n,
      mcbx_dram_udqs_n => mcbx_dram_udqs_n,
      mcbx_dram_udqs => mcbx_dram_udqs,
      mcbx_dram_udm => mcbx_dram_udm,
      mcbx_dram_ras_n => mcbx_dram_ras_n,
      mcbx_dram_odt => mcbx_dram_odt,
      mcbx_dram_ldm => mcbx_dram_ldm,
      mcbx_dram_dqs_n => mcbx_dram_dqs_n,
      mcbx_dram_dqs => mcbx_dram_dqs,
      mcbx_dram_dq => mcbx_dram_dq,
      mcbx_dram_clk_n => mcbx_dram_clk_n,
      mcbx_dram_clk => mcbx_dram_clk,
      mcbx_dram_cke => mcbx_dram_cke,
      mcbx_dram_cas_n => mcbx_dram_cas_n,
      mcbx_dram_ba => mcbx_dram_ba,
      mcbx_dram_addr => mcbx_dram_addr,
      RS232_Uart_1_sout => RS232_Uart_1_sout,
      RS232_Uart_1_sin => RS232_Uart_1_sin,
      RESET_in => RESET_in,
      ETHERNET_TX_ER => ETHERNET_TX_ER,
      ETHERNET_TX_EN => ETHERNET_TX_EN,
      ETHERNET_TX_CLK => ETHERNET_TX_CLK,
      ETHERNET_TXD => ETHERNET_TXD,
      ETHERNET_RX_ER => ETHERNET_RX_ER,
      ETHERNET_RX_DV => ETHERNET_RX_DV,
      ETHERNET_RX_CLK => ETHERNET_RX_CLK,
      ETHERNET_RXD => ETHERNET_RXD,
      ETHERNET_PHY_RST_N => ETHERNET_PHY_RST_N,
      ETHERNET_MII_TX_CLK => ETHERNET_MII_TX_CLK,
      ETHERNET_MDIO => ETHERNET_MDIO,
      ETHERNET_MDC => ETHERNET_MDC,
      cpuclk => cpuclk,
      cpuclk2x => cpuclk2x,
      cpuclk_in => cpuclk_in,
      cpurst => cpurst,
      axi_epc_0_PRH_CS_n_pin => axi_epc_0_PRH_CS_n_pin,
      axi_epc_0_PRH_Addr_pin => axi_epc_0_PRH_Addr_pin,
      axi_epc_0_PRH_RNW_pin => axi_epc_0_PRH_RNW_pin,
      axi_epc_0_PRH_Rdy_pin => axi_epc_0_PRH_Rdy_pin,
      axi_epc_0_PRH_Data_I_pin => axi_epc_0_PRH_Data_I_pin,
      axi_epc_0_PRH_Data_O_pin => axi_epc_0_PRH_Data_O_pin,
      axi_histo_dma_s_axis_s2mm_tready_pin => axi_histo_dma_s_axis_s2mm_tready_pin,
      axi_histo_dma_s_axis_s2mm_tlast_pin => axi_histo_dma_s_axis_s2mm_tlast_pin,
      axi_histo_dma_s_axis_s2mm_tkeep_pin => axi_histo_dma_s_axis_s2mm_tkeep_pin,
      axi_histo_dma_s_axis_s2mm_tdata_pin => axi_histo_dma_s_axis_s2mm_tdata_pin,
      axi_histo_dma_s_axis_s2mm_tvalid_pin => axi_histo_dma_s_axis_s2mm_tvalid_pin
    );

  -- Clock generator for cpuclk_in

  process
  begin
    cpuclk_in <= '0';
    loop
      wait for (cpuclk_in_PERIOD/2);
      cpuclk_in <= not cpuclk_in;
    end loop;
  end process;

  -- Reset Generator for RESET_in

  process
  begin
    RESET_in <= '1';
    wait for (RESET_in_LENGTH);
    RESET_in <= not RESET_in;
    wait;
  end process;

  -- START USER CODE (Do not remove this line)

  -- User: Put your stimulus here. Code in this
  --       section will not be overwritten.

  -- END USER CODE (Do not remove this line)

end architecture STRUCTURE;

