--------------------------------------------------------------------------------
-- testbench for histoUnit module.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
use work.rodHistoPack.all;
USE ieee.numeric_std.ALL;
 
ENTITY histoUnitb IS
END histoUnitb;
 
ARCHITECTURE behavior OF histoUnitb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT rodHisto
	 generic (simulation : boolean := false; useExtRam: boolean := false);
    PORT(
			control : in  STD_LOGIC_VECTOR (31 downto 0);
         hitValid : IN  std_logic;
			row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
			col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
			chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
			totVal : IN  std_logic_vector(totWidth - 1 downto 0);
         rfd : OUT  std_logic;
         mode : IN  std_logic;
         rfr : IN  std_logic;
         roc : OUT  std_logic;
         histValid : OUT  std_logic;
         histo : OUT  std_logic_vector(31 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic;
         ramClk : IN  std_logic;
         ramAddr : OUT  std_logic_vector(addrBits - 1 downto 0);
         ramData : INOUT  std_logic_vector(histDataBits - 1 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
        );
end COMPONENT;
    

	    COMPONENT cy7c1370
		    GENERIC (

        -- Constant parameters
	addr_bits : INTEGER := 19;
	data_bits : INTEGER := 36
	);
        PORT (Dq	: INOUT STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
              Addr	: IN    STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
              Clk	: IN    STD_LOGIC;
              CEN_n	: IN    STD_LOGIC;
              AdvLd_n	: IN    STD_LOGIC;
              Mode	: IN    STD_LOGIC;
              Bwa_n	: IN    STD_LOGIC;
              Bwb_n	: IN    STD_LOGIC;
              Bwc_n	: IN    STD_LOGIC;
              Bwd_n	: IN    STD_LOGIC;
              Rw_n	: IN    STD_LOGIC;
              Oe_n	: IN    STD_LOGIC;
              Ce1_n	: IN    STD_LOGIC;
              Ce3_n	: IN    STD_LOGIC;
              Ce2	: IN    STD_LOGIC;
              Zz	: IN    STD_LOGIC
        );
    END COMPONENT;   

   --Inputs
   signal control : std_logic_vector(31 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal hitenable : std_logic := '0';
   signal col : std_logic_vector(colBits-1 downto 0) := (others => '0');
	signal hitValid : std_logic := '0';
	
   signal totVal : std_logic_vector(3 downto 0) := (others => '0');
   signal mode : std_logic := '0';
   signal rfr : std_logic := '0';
   signal ramClk : std_logic := '0';

	--BiDirs
   signal ramData : std_logic_vector(histDataBits -1 downto 0);

 	--Outputs
   signal histo : std_logic_vector(31 downto 0);
   signal rfd : std_logic;
   signal roc : std_logic;
   signal valid : std_logic;
   signal ramAddr : std_logic_vector(17 downto 0);
   signal ramCke_n : std_logic;
   signal ramCs_n : std_logic;
   signal ramBw_n : std_logic_vector(3 downto 0);
   signal ramWe_n : std_logic;
   signal ramOe_n : std_logic;
   signal ramAdv : std_logic;
   signal ramLbo : std_logic;
   signal ramZz : std_logic;

	signal histoInput : std_logic := '0';

	 
	 CONSTANT addr_bits	: INTEGER := 18;
    CONSTANT data_bits	: INTEGER := 36;
 	 
	 CONSTANT simulation	: boolean := false;
    CONSTANT useExtRam	: boolean := true;
	
   -- Clock period definitions
   constant clk_period : time := 10 ns;
   constant ramClk_period : time := 10 ns;
	constant reset_delay : time := 45 ns;
	
	procedure tic (count : integer := 1) is
	begin
		for i in 1 to count loop
		 wait until rising_edge(clk);
		end loop;
	end procedure;
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   histo0: rodHisto
	generic map(simulation => simulation, useExtRam => useExtRam)
	PORT MAP (
			 control => control,
          clk => clk,
          rst => rst,
          hitValid => hitValid,
			row => "000000001",
			col => col,
			chip => "001",
          totVal => totVal,
          mode => mode,
			 
          histo => histo,
			 rfr => rfr,	
          rfd => rfd,
          roc => roc,
          histValid => valid,
          ramClk => ramClk,
          ramAddr => ramAddr,
          ramData => ramData,
          ramCke_n => ramCke_n,
          ramCs_n => ramCs_n,
          ramBw_n => ramBw_n,
          ramWe_n => ramWe_n,
          ramOe_n => ramOe_n,
          ramAdv => ramAdv,
          ramLbo => ramLbo,
          ramZz => ramZz
        );

	
			   cy7c1370_instance : cy7c1370
				GENERIC MAP(addr_bits => addr_bits, data_bits => data_bits)
				PORT MAP (Dq => ramData, Addr => ramAddr, Clk => ramClk, CEN_n => ramCke_n,
                  Mode => ramLbo, AdvLd_n => ramAdv, 
						Bwa_n => ramBw_n(0), Bwb_n => ramBw_n(1), 
						Bwc_n => ramBw_n(2), Bwd_n => ramBw_n(3),
                  Rw_n => ramWe_n, Oe_n => ramOe_n, 
						Ce1_n => ramCs_n, Ce3_n => '0', Ce2 => '1', Zz => ramZz);
						
	
   -- Clock process definitions
   clk_process :process
   begin
		clk <= '0';
		wait for clk_period/2;
		clk <= '1';
		wait for clk_period/2;
   end process;
 
   ramClk_process :process
   begin
		ramClk <= '0';
		wait for ramClk_period/2;
		ramClk <= '1';
		wait for ramClk_period/2;
   end process;

	inputData :process
	variable tot : integer := 1;
	begin
		tic;
		if histoInput = '1' then
--		if rfd = '1' then
			hitValid <= '1';
			totVal <= std_logic_vector(to_unsigned(4, totVal'length));
			col <= std_logic_vector(to_unsigned(tot, col'length));
			tot := tot + 1;
			if tot > 30 then 
				tot := 1;
--				totVal <= std_logic_vector(to_unsigned(1, totVal'length));
--				pixAddr <= std_logic_vector(to_unsigned(26879, pixAddr'length));
			end if;
			else
				hitValid <= '0';
		end if;
		tic;--hujun
		hitValid <= '0';--hujun
		tic;
		tic;
		tic;
		tic;
		tic;
		tic;--hujun
	end process;

   -- Stimulus process
   stim_proc: process
   begin		
		rst <= '1';
      wait for reset_delay;	
		rst <= '0';
		
      control <= B"0000_0000_0000_0000_0000_0000_0000_0000";
		control(chipCtlShift) <= chipCtlNull(0); -- read out data of 1 or 8 FE
		control(opModeCtlShift + opModeCtlBits - 1 downto opModeCtlShift) <= opOnlineOccupancy;
																									--opOfflineOccupancy;
																									--opLongTot; 
																									--opShortTot; --read out all values
		control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMaskNone;
		control(expectedOccCtlShift + expectedOccCtlBits - 1 downto expectedOccCtlShift) <= std_logic_vector(to_unsigned(3, expectedOccCtlBits));
		--control(scanTypeCtlShift) <= scanTypeOcc(0); -- read out only OCC values
		wait for clk_period/2*11;

      -- insert stimulus here 
		tic(215041);
		
		-- write test data (tot[addr] = addr)
		histoInput <= '1';
		tic(100);
		histoInput <= '0';
		tic(4);
		-- TODO MK: mode needs to be asserted for 3 cycles after enable goes low to make sure all data is collected?
		-- TODO MK: what about READOUT -> CLEAN -> IDLE/RESET?
		
		--- readout block ---
		mode <= '1';
		tic(4);
		rfr <= '1';
		

--		--- step ---
--		wait for (20 * clk_period);
--		rfr <= '0';
--		wait for (14 * clk_period);
--		rfr <= '1';		
--
--		--- step ---
--		wait for (30 * clk_period);
--		rfr <= '0';
--		wait for (14 * clk_period);
--		rfr <= '1';		

		--- step ---
		tic(215040);
		
		tic(1000);
      wait;
   end process;
END;
