----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:36:16 03/26/2013 
-- Design Name: 
-- Module Name:    ZeroOhmArray - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ZeroOhmArray is 
  generic(a_to_b_loss : time := 1 ns; 
          b_to_a_loss : time := 2 ns;
			 NBIT   : natural := 1); 
  port 
    (A : inout Std_Logic_vector(NBIT-1 downto 0); 
     B : inout Std_Logic_vector(NBIT-1 downto 0) 
     ); 
end ZeroOhmArray;
 architecture ZeroOhmArray_a of ZeroOhmArray is 
 
 

component WireDelay2 
generic (Delay_A_to_B, Delay_B_to_A: time);
port
(A : inout Std_Logic;
B : inout Std_Logic
);
 end component;
 
begin

elements:
   for i in 0 to NBIT-1 generate
      begin
		
--		zeroohm_c : ZeroOhm
--		generic map (a_to_b_loss=> a_to_b_loss, b_to_a_loss=>b_to_a_loss)
--		port map (A => A(i),B => B(i));

wiredelay_c: WireDelay2
generic map (Delay_A_to_B => a_to_b_loss,Delay_B_to_A => b_to_a_loss)
port map (A => A(i),B => B(i));
         
   end generate;

end ZeroOhmArray_a;

