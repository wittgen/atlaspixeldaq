----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:43:29 03/27/2013 
-- Design Name: 
-- Module Name:    WireDelay2 - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity WireDelay2 is
generic (Delay_A_to_B, Delay_B_to_A: time);
port
(A : inout Std_Logic;
B : inout Std_Logic
);
end WireDelay2;


architecture WireDelay2_a of WireDelay2 is
begin
ABC0_Lbl: process
variable ThenTime_v : time;
begin
wait on A'transaction, B'transaction
until ThenTime_v /= now;
-- Break
if A'active then
wait for Delay_A_to_B; -- wire delay
else
wait for Delay_B_to_A;
end if;
ThenTime_v := now;
A <= 'Z';
B <= 'Z';
wait for 0 ns;

-- Make
A <= B;
B <= A;
end process ABC0_Lbl;
end WireDelay2_a;
