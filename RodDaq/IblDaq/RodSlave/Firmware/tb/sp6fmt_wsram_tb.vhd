--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   12:09:42 07/12/2013
-- Design Name:   
-- Module Name:   C:/AtlasRepo/IBLDAQ-0-0-0/RodDaq/IblDsp/Firmware/sp6fmt/tb/sp6fmt_wsram_tb.vhd
-- Project Name:  sp6fmt
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: sp6fmt
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY sp6fmt_wsram_tb IS
END sp6fmt_wsram_tb;
 
ARCHITECTURE behavior OF sp6fmt_wsram_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT sp6fmt
	 	  generic( 
		simulation : boolean := false; 
		useExtRam: boolean := false);
     PORT(
         rzq : INOUT  std_logic;
         mcbx_dram_cs_n : OUT  std_logic;
         mcbx_dram_we_n : OUT  std_logic;
         mcbx_dram_udqs_n : INOUT  std_logic;
         mcbx_dram_udqs : INOUT  std_logic;
         mcbx_dram_udm : OUT  std_logic;
         mcbx_dram_ras_n : OUT  std_logic;
         mcbx_dram_odt : OUT  std_logic;
         mcbx_dram_ldm : OUT  std_logic;
         mcbx_dram_dqs_n : INOUT  std_logic;
         mcbx_dram_dqs : INOUT  std_logic;
         mcbx_dram_dq : INOUT  std_logic_vector(15 downto 0);
         mcbx_dram_clk_n : OUT  std_logic;
         mcbx_dram_clk : OUT  std_logic;
         mcbx_dram_cke : OUT  std_logic;
         mcbx_dram_cas_n : OUT  std_logic;
         mcbx_dram_ba : OUT  std_logic_vector(2 downto 0);
         mcbx_dram_addr : OUT  std_logic_vector(13 downto 0);
         RS232_Uart_1_sout : OUT  std_logic;
         RS232_Uart_1_sin : IN  std_logic;
         RESET_in : IN  std_logic;
         ETHERNET_TX_ER : OUT  std_logic;
         ETHERNET_TX_EN : OUT  std_logic;
         ETHERNET_TX_CLK : OUT  std_logic;
         ETHERNET_TXD : OUT  std_logic_vector(7 downto 0);
         ETHERNET_RX_ER : IN  std_logic;
         ETHERNET_RX_DV : IN  std_logic;
         ETHERNET_RX_CLK : IN  std_logic;
         ETHERNET_RXD : IN  std_logic_vector(7 downto 0);
         ETHERNET_PHY_RST_N : OUT  std_logic;
         ETHERNET_MII_TX_CLK : IN  std_logic;
         ETHERNET_MDIO : INOUT  std_logic;
         ETHERNET_MDC : OUT  std_logic;
         CLK100in_P : IN  std_logic;
         CLK100in_N : IN  std_logic;
         CLK40in_P : IN  std_logic;
         CLK40in_N : IN  std_logic;
			clk200out: out std_logic_vector(1 downto 0);
			clk200in: in std_logic_vector(1 downto 0);
         clk_from_v5 : IN  std_logic;
         clk_to_fei4_p : OUT  std_logic;
         clk_to_fei4_n : OUT  std_logic;
         data_to_fei4_p : OUT  std_logic;
         data_to_fei4_n : OUT  std_logic;
         data_from_fei4_p : IN  std_logic;
         data_from_fei4_n : IN  std_logic;
         xc_in : IN  std_logic_vector(7 downto 0);
         xc : OUT  std_logic_vector(7 downto 0);
         rxdata_boc2rod : IN  std_logic_vector(47 downto 0);
         slink0_data_rod2boc : OUT  std_logic_vector(15 downto 0);
         slink0_we : OUT  std_logic;
         slink0_lff : IN  std_logic;
         slink0_ldown : IN  std_logic;
         slink0_uctrl : OUT  std_logic;
         slink0_uclk : OUT  std_logic;
         slink0_utest : OUT  std_logic;
         slink0_ureset : OUT  std_logic;
         slink1_data_rod2boc : OUT  std_logic_vector(15 downto 0);
         slink1_we : OUT  std_logic;
         slink1_lff : IN  std_logic;
         slink1_ldown : IN  std_logic;
         slink1_uctrl : OUT  std_logic;
         slink1_uclk : OUT  std_logic;
         slink1_utest : OUT  std_logic;
         slink1_ureset : OUT  std_logic;
         formatter_bus_strb_n_in : IN  std_logic;
         formatter_bus_rnw_in : IN  std_logic;
         formatter_bus_hwsb_in : IN  std_logic;
         formatter_bus_ds_in : IN  std_logic;
         formatter_bus_adr_in : IN  std_logic_vector(15 downto 0);
         formatter_bus_data_inout : INOUT  std_logic_vector(15 downto 0);
         formatter_bus_ack_n_out : OUT  std_logic;
         --sram1_clkfb_in : IN  std_logic;
         sram1_io : INOUT  std_logic_vector(35 downto 0);
         sram1_a : OUT  std_logic_vector(17 downto 0);
         sram1_lbo : OUT  std_logic;
         sram1_AdvLd_n : OUT  std_logic;
         sram1_bw1_n : OUT  std_logic;
         sram1_bw2_n : OUT  std_logic;
         sram1_bw3_n : OUT  std_logic;
         sram1_bw4_n : OUT  std_logic;
         sram1_we_n : OUT  std_logic;
         sram1_oe_n : OUT  std_logic;
         sram1_cke_n : OUT  std_logic;
         sram1_zz : OUT  std_logic;
         sram1_cs1 : OUT  std_logic;
         --sram1_clk : OUT  std_logic;
         --sram2_clkfb_in : IN  std_logic;
         sram2_io : INOUT  std_logic_vector(35 downto 0);
         sram2_a : OUT  std_logic_vector(17 downto 0);
         sram2_lbo : OUT  std_logic;
         sram2_AdvLd_n : OUT  std_logic;
         sram2_bw1_n : OUT  std_logic;
         sram2_bw2_n : OUT  std_logic;
         sram2_bw3_n : OUT  std_logic;
         sram2_bw4_n : OUT  std_logic;
         sram2_we_n : OUT  std_logic;
         sram2_oe_n : OUT  std_logic;
         sram2_cke_n : OUT  std_logic;
         sram2_zz : OUT  std_logic;
         sram2_cs1 : OUT  std_logic;
         --sram2_clk : OUT  std_logic;
         clk40mon : OUT  std_logic;
         clk100mon : OUT  std_logic;
         cpuclk_out : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal RS232_Uart_1_sin : std_logic := '0';
   signal RESET_in : std_logic := '0';
   signal ETHERNET_RX_ER : std_logic := '0';
   signal ETHERNET_RX_DV : std_logic := '0';
   signal ETHERNET_RX_CLK : std_logic := '0';
   signal ETHERNET_RXD : std_logic_vector(7 downto 0) := (others => '0');
   signal ETHERNET_MII_TX_CLK : std_logic := '0';
   signal CLK100in_P : std_logic := '0';
   signal CLK100in_N : std_logic := '0';
   signal CLK40in_P : std_logic := '0';
   signal CLK40in_N : std_logic := '0';
   signal clk_from_v5 : std_logic := '0';
   signal data_from_fei4_p : std_logic := '0';
   signal data_from_fei4_n : std_logic := '0';
   signal xc_in : std_logic_vector(7 downto 0) := (others => '0');
   signal rxdata_boc2rod : std_logic_vector(47 downto 0) := (others => '0');
   signal slink0_lff : std_logic := '0';
   signal slink0_ldown : std_logic := '0';
   signal slink1_lff : std_logic := '0';
   signal slink1_ldown : std_logic := '0';
   signal formatter_bus_strb_n_in : std_logic := '0';
   signal formatter_bus_rnw_in : std_logic := '0';
   signal formatter_bus_hwsb_in : std_logic := '0';
   signal formatter_bus_ds_in : std_logic := '0';
   signal formatter_bus_adr_in : std_logic_vector(15 downto 0) := (others => '0');
   signal sram1_clkfb_in : std_logic := '0';
   signal sram2_clkfb_in : std_logic := '0';

	--BiDirs
   signal rzq : std_logic;
   signal mcbx_dram_udqs_n : std_logic;
   signal mcbx_dram_udqs : std_logic;
   signal mcbx_dram_dqs_n : std_logic;
   signal mcbx_dram_dqs : std_logic;
   signal mcbx_dram_dq : std_logic_vector(15 downto 0);
   signal ETHERNET_MDIO : std_logic;
   signal formatter_bus_data_inout : std_logic_vector(15 downto 0);
   signal sram1_io : std_logic_vector(35 downto 0);
   signal sram2_io : std_logic_vector(35	downto 0);

 	--Outputs
   signal mcbx_dram_cs_n : std_logic;
   signal mcbx_dram_we_n : std_logic;
   signal mcbx_dram_udm : std_logic;
   signal mcbx_dram_ras_n : std_logic;
   signal mcbx_dram_odt : std_logic;
   signal mcbx_dram_ldm : std_logic;
   signal mcbx_dram_clk_n : std_logic;
   signal mcbx_dram_clk : std_logic;
   signal mcbx_dram_cke : std_logic;
   signal mcbx_dram_cas_n : std_logic;
   signal mcbx_dram_ba : std_logic_vector(2 downto 0);
   signal mcbx_dram_addr : std_logic_vector(13 downto 0);
   signal RS232_Uart_1_sout : std_logic;
   signal ETHERNET_TX_ER : std_logic;
   signal ETHERNET_TX_EN : std_logic;
   signal ETHERNET_TX_CLK : std_logic;
   signal ETHERNET_TXD : std_logic_vector(7 downto 0);
   signal ETHERNET_PHY_RST_N : std_logic;
   signal ETHERNET_MDC : std_logic;
   signal clk_to_fei4_p : std_logic;
   signal clk_to_fei4_n : std_logic;
   signal data_to_fei4_p : std_logic;
   signal data_to_fei4_n : std_logic;
   signal xc : std_logic_vector(7 downto 0);
   signal slink0_data_rod2boc : std_logic_vector(15 downto 0);
   signal slink0_we : std_logic;
   signal slink0_uctrl : std_logic;
   signal slink0_uclk : std_logic;
   signal slink0_utest : std_logic;
   signal slink0_ureset : std_logic;
   signal slink1_data_rod2boc : std_logic_vector(15 downto 0);
   signal slink1_we : std_logic;
   signal slink1_uctrl : std_logic;
   signal slink1_uclk : std_logic;
   signal slink1_utest : std_logic;
   signal slink1_ureset : std_logic;
   signal formatter_bus_ack_n_out : std_logic;
   signal sram1_a : std_logic_vector(17 downto 0);
   signal sram1_lbo : std_logic;
   signal sram1_AdvLd_n : std_logic;
   signal sram1_bw1_n : std_logic;
   signal sram1_bw2_n : std_logic;
   signal sram1_bw3_n : std_logic;
   signal sram1_bw4_n : std_logic;
   signal sram1_we_n : std_logic;
   signal sram1_oe_n : std_logic;
   signal sram1_cke_n : std_logic;
   signal sram1_zz : std_logic;
   signal sram1_cs1 : std_logic;
   signal sram1_clk : std_logic;
   signal sram2_a : std_logic_vector(17 downto 0);
   signal sram2_lbo : std_logic;
   signal sram2_AdvLd_n : std_logic;
   signal sram2_bw1_n : std_logic;
   signal sram2_bw2_n : std_logic;
   signal sram2_bw3_n : std_logic;
   signal sram2_bw4_n : std_logic;
   signal sram2_we_n : std_logic;
   signal sram2_oe_n : std_logic;
   signal sram2_cke_n : std_logic;
   signal sram2_zz : std_logic;
   signal sram2_cs1 : std_logic;
   signal sram2_clk : std_logic;
   signal clk40mon : std_logic;
   signal clk100mon : std_logic;
   signal cpuclk_out : std_logic;

   -- Clock period definitions
 
   constant CLK100in_P_period : time := 10 ns;
   constant CLK100in_N_period : time := 10 ns;
   constant CLK40in_P_period : time := 25 ns;
   constant CLK40in_N_period : time := 25 ns;
   constant clk_from_v5_period : time := 10 ns;
 
 	 
	 CONSTANT addr_bits	: INTEGER := 18;
    CONSTANT data_bits	: INTEGER := 36;
	
	    COMPONENT cy7c1370
		    GENERIC (

        -- Constant parameters
	addr_bits : INTEGER := 19;
	data_bits : INTEGER := 36
	);
        PORT (Dq	: INOUT STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
              Addr	: IN    STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
              Clk	: IN    STD_LOGIC;
              CEN_n	: IN    STD_LOGIC;
              AdvLd_n	: IN    STD_LOGIC;
              Mode	: IN    STD_LOGIC;
              Bwa_n	: IN    STD_LOGIC;
              Bwb_n	: IN    STD_LOGIC;
              Bwc_n	: IN    STD_LOGIC;
              Bwd_n	: IN    STD_LOGIC;
              Rw_n	: IN    STD_LOGIC;
              Oe_n	: IN    STD_LOGIC;
              Ce1_n	: IN    STD_LOGIC;
              Ce3_n	: IN    STD_LOGIC;
              Ce2	: IN    STD_LOGIC;
              Zz	: IN    STD_LOGIC
        );
    END COMPONENT;
	 
	constant propagation_pcb_clock_1 : time := 415 ps;
	constant propagation_pcb_clock_2 : time := 180 ps;
	constant propagation_pcb_data : time := 350 ps;

	 
	component ZeroOhmArray 
  generic(a_to_b_loss : time := 1 ns; 
          b_to_a_loss : time := 2 ns;
			 NBIT   : natural := 1); 
  port 
    (A : inout Std_Logic_vector(NBIT-1 downto 0); 
     B : inout Std_Logic_vector(NBIT-1 downto 0) 
     ); 
end component;

   SIGNAL sram1_io_b		: STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
   SIGNAL sram1_a_dly		: STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
   SIGNAL sram1_clk_ssram,sram1_clk_fb		: STD_LOGIC;
   SIGNAL sram1_we_n_dly		: STD_LOGIC;
 

   SIGNAL sram2_io_b		: STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
   SIGNAL sram2_a_dly		: STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
   SIGNAL sram2_clk_ssram,sram2_clk_fb		: STD_LOGIC;
   SIGNAL sram2_we_n_dly		: STD_LOGIC;
 

	COMPONENT ddr2
	PORT(
		ck : IN std_logic;
		ck_n : IN std_logic;
		cke : IN std_logic;
		cs_n : IN std_logic;
		ras_n : IN std_logic;
		cas_n : IN std_logic;
		we_n : IN std_logic;
		ba : IN std_logic_vector(2 downto 0);
		addr : IN std_logic_vector(13 downto 0);
		odt : IN std_logic;    
		dm_rdqs : INOUT std_logic_vector(1 downto 0);
		dq : INOUT std_logic_vector(15 downto 0);
		dqs : INOUT std_logic_vector(1 downto 0);
		dqs_n : INOUT std_logic_vector(1 downto 0);      
		rdqs_n : OUT std_logic_vector(1 downto 0)
		);
	END COMPONENT;
	

	signal sram1_bw1_n_dly,sram1_bw2_n_dly,sram1_bw3_n_dly,sram1_bw4_n_dly : std_logic;
	signal sram2_bw1_n_dly,sram2_bw2_n_dly,sram2_bw3_n_dly,sram2_bw4_n_dly : std_logic;



 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: sp6fmt 
		 	  generic map( 
			 simulation => true,
			 useExtRam => true)
   PORT MAP (
          rzq => rzq,
          mcbx_dram_cs_n => mcbx_dram_cs_n,
          mcbx_dram_we_n => mcbx_dram_we_n,
          mcbx_dram_udqs_n => mcbx_dram_udqs_n,
          mcbx_dram_udqs => mcbx_dram_udqs,
          mcbx_dram_udm => mcbx_dram_udm,
          mcbx_dram_ras_n => mcbx_dram_ras_n,
          mcbx_dram_odt => mcbx_dram_odt,
          mcbx_dram_ldm => mcbx_dram_ldm,
          mcbx_dram_dqs_n => mcbx_dram_dqs_n,
          mcbx_dram_dqs => mcbx_dram_dqs,
          mcbx_dram_dq => mcbx_dram_dq,
          mcbx_dram_clk_n => mcbx_dram_clk_n,
          mcbx_dram_clk => mcbx_dram_clk,
          mcbx_dram_cke => mcbx_dram_cke,
          mcbx_dram_cas_n => mcbx_dram_cas_n,
          mcbx_dram_ba => mcbx_dram_ba,
          mcbx_dram_addr => mcbx_dram_addr,
          RS232_Uart_1_sout => RS232_Uart_1_sout,
          RS232_Uart_1_sin => RS232_Uart_1_sin,
          RESET_in => RESET_in,
          ETHERNET_TX_ER => ETHERNET_TX_ER,
          ETHERNET_TX_EN => ETHERNET_TX_EN,
          ETHERNET_TX_CLK => ETHERNET_TX_CLK,
          ETHERNET_TXD => ETHERNET_TXD,
          ETHERNET_RX_ER => ETHERNET_RX_ER,
          ETHERNET_RX_DV => ETHERNET_RX_DV,
          ETHERNET_RX_CLK => ETHERNET_RX_CLK,
          ETHERNET_RXD => ETHERNET_RXD,
          ETHERNET_PHY_RST_N => ETHERNET_PHY_RST_N,
          ETHERNET_MII_TX_CLK => ETHERNET_MII_TX_CLK,
          ETHERNET_MDIO => ETHERNET_MDIO,
          ETHERNET_MDC => ETHERNET_MDC,
          CLK100in_P => CLK100in_P,
          CLK100in_N => CLK100in_N,
          CLK40in_P => CLK40in_P,
          CLK40in_N => CLK40in_N,
          clk_from_v5 => clk_from_v5,
          clk_to_fei4_p => clk_to_fei4_p,
          clk_to_fei4_n => clk_to_fei4_n,
          data_to_fei4_p => data_to_fei4_p,
          data_to_fei4_n => data_to_fei4_n,
          data_from_fei4_p => data_from_fei4_p,
          data_from_fei4_n => data_from_fei4_n,
          xc_in => xc_in,
          xc => xc,
          rxdata_boc2rod => rxdata_boc2rod,
          slink0_data_rod2boc => slink0_data_rod2boc,
          slink0_we => slink0_we,
          slink0_lff => slink0_lff,
          slink0_ldown => slink0_ldown,
          slink0_uctrl => slink0_uctrl,
          slink0_uclk => slink0_uclk,
          slink0_utest => slink0_utest,
          slink0_ureset => slink0_ureset,
          slink1_data_rod2boc => slink1_data_rod2boc,
          slink1_we => slink1_we,
          slink1_lff => slink1_lff,
          slink1_ldown => slink1_ldown,
          slink1_uctrl => slink1_uctrl,
          slink1_uclk => slink1_uclk,
          slink1_utest => slink1_utest,
          slink1_ureset => slink1_ureset,
          formatter_bus_strb_n_in => formatter_bus_strb_n_in,
          formatter_bus_rnw_in => formatter_bus_rnw_in,
          formatter_bus_hwsb_in => formatter_bus_hwsb_in,
          formatter_bus_ds_in => formatter_bus_ds_in,
          formatter_bus_adr_in => formatter_bus_adr_in,
          formatter_bus_data_inout => formatter_bus_data_inout,
          formatter_bus_ack_n_out => formatter_bus_ack_n_out,
          clk200in(0) => sram1_clk_fb,
          clk200in(1) => sram2_clk_fb,
           sram1_io => sram1_io,
          sram1_a => sram1_a,
          sram1_lbo => sram1_lbo,
          sram1_AdvLd_n => sram1_AdvLd_n,
          sram1_bw1_n => sram1_bw1_n,
          sram1_bw2_n => sram1_bw2_n,
          sram1_bw3_n => sram1_bw3_n,
          sram1_bw4_n => sram1_bw4_n,
          sram1_we_n => sram1_we_n,
          sram1_oe_n => sram1_oe_n,
          sram1_cke_n => sram1_cke_n,
          sram1_zz => sram1_zz,
          sram1_cs1 => sram1_cs1,
          sram2_io => sram2_io,
          sram2_a => sram2_a,
          sram2_lbo => sram2_lbo,
          sram2_AdvLd_n => sram2_AdvLd_n,
          sram2_bw1_n => sram2_bw1_n,
          sram2_bw2_n => sram2_bw2_n,
          sram2_bw3_n => sram2_bw3_n,
          sram2_bw4_n => sram2_bw4_n,
          sram2_we_n => sram2_we_n,
          sram2_oe_n => sram2_oe_n,
          sram2_cke_n => sram2_cke_n,
          sram2_zz => sram2_zz,
          sram2_cs1 => sram2_cs1,
          clk200out(0) => sram1_clk,
          clk200out(1) => sram2_clk,
          clk40mon => clk40mon,
          clk100mon => clk100mon,
          cpuclk_out => cpuclk_out
        );

   -- Clock process definitions
--   mcbx_dram_clk_process :process
--   begin
--		mcbx_dram_clk <= '0';
--		wait for mcbx_dram_clk_period/2;
--		mcbx_dram_clk <= '1';
--		wait for mcbx_dram_clk_period/2;
--   end process;
-- 
--   ETHERNET_TX_CLK_process :process
--   begin
--		ETHERNET_TX_CLK <= '0';
--		wait for ETHERNET_TX_CLK_period/2;
--		ETHERNET_TX_CLK <= '1';
--		wait for ETHERNET_TX_CLK_period/2;
--   end process;
-- 
--   ETHERNET_RX_CLK_process :process
--   begin
--		ETHERNET_RX_CLK <= '0';
--		wait for ETHERNET_RX_CLK_period/2;
--		ETHERNET_RX_CLK <= '1';
--		wait for ETHERNET_RX_CLK_period/2;
--   end process;
-- 
--   ETHERNET_MII_TX_CLK_process :process
--   begin
--		ETHERNET_MII_TX_CLK <= '0';
--		wait for ETHERNET_MII_TX_CLK_period/2;
--		ETHERNET_MII_TX_CLK <= '1';
--		wait for ETHERNET_MII_TX_CLK_period/2;
--   end process;
 
   CLK100in_P_process :process
   begin
		CLK100in_P <= '0';
		wait for CLK100in_P_period/2;
		CLK100in_P <= '1';
		wait for CLK100in_P_period/2;
   end process;
	CLK100in_N <= not(CLK100in_P);
 
 
 
   CLK40in_P_process :process
   begin
		CLK40in_P <= '0';
		wait for CLK40in_P_period/2;
		CLK40in_P <= '1';
		wait for CLK40in_P_period/2;
   end process;
	
	CLK40in_N <=CLK40in_P ;
 
 
   clk_from_v5_process :process
   begin
		clk_from_v5 <= '0';
		wait for clk_from_v5_period/2;
		clk_from_v5 <= '1';
		wait for clk_from_v5_period/2;
   end process;
 
  
   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      wait for 100 ns;	

      wait for CLK100in_P_period*10;

      -- insert stimulus here 

      wait;
   end process;
	
			   ssram1 : cy7c1370
				GENERIC MAP(addr_bits => addr_bits, data_bits => data_bits)
	       PORT MAP (Dq => sram1_io_b, Addr => sram1_a_dly, Clk => sram1_clk_ssram, CEN_n => sram1_cke_n,
                  Mode => sram1_lbo, AdvLd_n => sram1_AdvLd_n, 
						Bwa_n => sram1_bw1_n_dly, Bwb_n => sram1_bw2_n_dly, 
						Bwc_n => sram1_bw3_n_dly, Bwd_n => sram1_bw4_n_dly,
                  Rw_n => sram1_we_n_dly, Oe_n => sram1_oe_n, 
						Ce1_n => sram1_cs1, Ce3_n => '0', Ce2 => '1', Zz => sram1_zz);
						
	sram1_clk_fb <= transport sram1_clk_ssram after propagation_pcb_clock_1;
	sram1_clk_ssram <= transport sram1_clk after propagation_pcb_clock_1;
	sram1_a_dly <= transport sram1_a after propagation_pcb_data;
	sram1_we_n_dly <= transport sram1_we_n after propagation_pcb_data;
	sram1_bw1_n_dly <= transport sram1_bw1_n after propagation_pcb_data;
	sram1_bw2_n_dly <= transport sram1_bw2_n after propagation_pcb_data;
	sram1_bw3_n_dly <= transport sram1_bw3_n after propagation_pcb_data;
	sram1_bw4_n_dly <= transport sram1_bw4_n after propagation_pcb_data;

	
			   ssram2 : cy7c1370
				GENERIC MAP(addr_bits => addr_bits, data_bits => data_bits)
	       PORT MAP (Dq => sram2_io_b, Addr => sram2_a_dly, Clk => sram2_clk_ssram, CEN_n => sram2_cke_n,
                  Mode => sram2_lbo, AdvLd_n => sram2_AdvLd_n, 
						Bwa_n => sram2_bw1_n_dly, Bwb_n => sram2_bw2_n_dly, 
						Bwc_n => sram2_bw3_n_dly, Bwd_n => sram2_bw4_n_dly,
                  Rw_n => sram2_we_n_dly, Oe_n => sram2_oe_n, 
						Ce1_n => sram2_cs1, Ce3_n => '0', Ce2 => '1', Zz => sram2_zz);
						
	sram2_clk_fb <= transport sram2_clk_ssram after propagation_pcb_clock_2;
	sram2_clk_ssram <= transport sram2_clk after propagation_pcb_clock_2;
	sram2_a_dly <= transport sram2_a after propagation_pcb_data;
	sram2_we_n_dly <= transport sram2_we_n after propagation_pcb_data;
	sram2_bw1_n_dly <= transport sram2_bw1_n after propagation_pcb_data;
	sram2_bw2_n_dly <= transport sram2_bw2_n after propagation_pcb_data;
	sram2_bw3_n_dly <= transport sram2_bw3_n after propagation_pcb_data;
	sram2_bw4_n_dly <= transport sram2_bw4_n after propagation_pcb_data;
	
	Inst_bidirbus_1: ZeroOhmArray
  generic map (a_to_b_loss => propagation_pcb_data,
          b_to_a_loss => propagation_pcb_data,
			 NBIT => 36) 
  port map
    (A => sram1_io,
     B => sram1_io_b
     ); 
	  Inst_bidirbus_2: ZeroOhmArray
  generic map (a_to_b_loss => propagation_pcb_data,
          b_to_a_loss => propagation_pcb_data,
			 NBIT => 36) 
  port map
    (A => sram2_io,
     B => sram2_io_b
     ); 
	
	
		Inst_ddr2: ddr2 PORT MAP(
		ck => mcbx_dram_clk,
		ck_n => mcbx_dram_clk_n,
		cke => mcbx_dram_cke,
		cs_n => mcbx_dram_cs_n,
		ras_n => mcbx_dram_ras_n,
		cas_n => mcbx_dram_cas_n,
		we_n => mcbx_dram_we_n,
		dm_rdqs(0) => mcbx_dram_udqs,
		dm_rdqs(1) => mcbx_dram_udqs,
		ba => mcbx_dram_ba,
		addr => mcbx_dram_addr,
		dq => mcbx_dram_dq,
		dqs(0) => mcbx_dram_dqs,
		dqs(1) => mcbx_dram_dqs,
		dqs_n(0) => mcbx_dram_dqs_n,
		dqs_n(1) => mcbx_dram_dqs_n,
		rdqs_n(0) => mcbx_dram_udqs_n,
		rdqs_n(1) => mcbx_dram_udqs_n,
		odt => mcbx_dram_odt
	);

	
	

END;
