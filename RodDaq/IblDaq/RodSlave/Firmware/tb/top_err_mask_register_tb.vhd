--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   16:56:18 11/26/2013
-- Design Name:   
-- Module Name:   C:/Users/user/Desktop/ibl_rod_fw/ibl_rodSlave/top_err_mask_register_tb.vhd
-- Project Name:  ibl_rodSlave
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rodSlave_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY top_err_mask_register_tb IS
END top_err_mask_register_tb;
 
ARCHITECTURE behavior OF top_err_mask_register_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rodSlave_top
    PORT(
--         clk40 : IN  std_logic;
--         clk80 : IN  std_logic;
--         clk100 : IN  std_logic;
		clk40in_p							: in std_logic;
		clk40in_n							: in std_logic;
		clk100in_p							: in std_logic;
		clk100in_n							: in std_logic;
		xc_in                       : in  std_logic_vector(7 downto 0);
		xc													: out std_logic_vector(7 downto 0);
         reset : IN  std_logic;
         rxdata_boc2rod : IN  std_logic_vector(47 downto 0);
         rod_busy_n : OUT  std_logic;
         rod_bus_rnw : IN  std_logic;
         rod_bus_ce : IN  std_logic;
         rod_bus_hwsb : IN  std_logic;
         rod_bus_ds : IN  std_logic;
         rod_bus_addr : IN  std_logic_vector(15 downto 0);
         rod_bus_data : INOUT  std_logic_vector(15 downto 0);
         rod_bus_ack_out : OUT  std_logic;
         mode_bits_in : IN  std_logic_vector(11 downto 0);
         modebits_fifo_wen_n_in : IN  std_logic;
         modebits_fifo_rst_n_in : IN  std_logic;
         modebits_fifo_ef_n_out : OUT  std_logic;
         modebits_fifo_ff_n_out : OUT  std_logic;
         ev_data_from_ppc : IN  std_logic_vector(15 downto 0);
         ev_data_wen_n : IN  std_logic;
         ev_data_rst_n : IN  std_logic;
         ev_data_almost_full_n : OUT  std_logic_vector(1 downto 0);
         ev_data_ready : OUT  std_logic_vector(1 downto 0);
         ev_id_fifo_empty_error : OUT  std_logic_vector(1 downto 0);
         slink0_utest : OUT  std_logic;
         slink1_utest : OUT  std_logic;
         slink0_ureset : OUT  std_logic;
         slink1_ureset : OUT  std_logic;
         slink0_uctrl : OUT  std_logic;
         slink1_uctrl : OUT  std_logic;
         slink0_lff : IN  std_logic;
         slink1_lff : IN  std_logic;
         slink0_ldown : IN  std_logic;
         slink1_ldown : IN  std_logic;
         slink0_data_rod2boc : OUT  std_logic_vector(15 downto 0);
         slink1_data_rod2boc : OUT  std_logic_vector(15 downto 0);
         slink0_we : OUT  std_logic;
         slink1_we : OUT  std_logic;
         slink0_uclk : OUT  std_logic;
         slink1_uclk : OUT  std_logic
--         ssr1_ad : OUT  std_logic_vector(19 downto 0);
--         ssr2_ad : OUT  std_logic_vector(19 downto 0);
--         ssr1_dq : INOUT  std_logic_vector(35 downto 0);
--         ssr2_dq : INOUT  std_logic_vector(35 downto 0);
--         ssr1_adv : OUT  std_logic;
--         ssr2_adv : OUT  std_logic;
--         ssr1_bwa : OUT  std_logic;
--         ssr2_bwa : OUT  std_logic;
--         ssr1_bwb : OUT  std_logic;
--         ssr2_bwb : OUT  std_logic;
--         ssr1_bwc : OUT  std_logic;
--         ssr2_bwc : OUT  std_logic;
--         ssr1_bwd : OUT  std_logic;
--         ssr2_bwd : OUT  std_logic;
--         ssr1_cke : OUT  std_logic;
--         ssr2_cke : OUT  std_logic;
--         ssr1_clk : OUT  std_logic;
--         ssr2_clk : OUT  std_logic;
--         ssr1_csb : OUT  std_logic;
--         ssr2_csb : OUT  std_logic;
--         ssr1_lbo : OUT  std_logic;
--         ssr2_lbo : OUT  std_logic;
--         ssr1_oe : OUT  std_logic;
--         ssr2_oe : OUT  std_logic;
--         ssr1_we : OUT  std_logic;
--         ssr2_we : OUT  std_logic;
--         ssr1_zz : OUT  std_logic;
--         ssr2_zz : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
--   signal clk40 : std_logic := '0';
--   signal clk80 : std_logic := '0';
--   signal clk100 : std_logic := '0';
	signal clk40in_p	: std_logic := '0';
	signal clk40in_n	: std_logic := '0';
	signal clk100in_p	: std_logic := '0';
	signal clk100in_n	: std_logic := '0';
	signal xc_in 			: std_logic_vector(7 downto 0) := (others => '0');


   signal reset : std_logic := '0';
   signal rxdata_boc2rod : std_logic_vector(47 downto 0) := (others => '0');
   signal rod_bus_rnw : std_logic := '0';
   signal rod_bus_ce : std_logic := '0';
   signal rod_bus_hwsb : std_logic := '0';
   signal rod_bus_ds : std_logic := '0';
   signal rod_bus_addr : std_logic_vector(15 downto 0) := (others => '0');
   signal mode_bits_in : std_logic_vector(11 downto 0) := (others => '0');
   signal modebits_fifo_wen_n_in : std_logic := '0';
   signal modebits_fifo_rst_n_in : std_logic := '0';
   signal ev_data_from_ppc : std_logic_vector(15 downto 0) := (others => '0');
   signal ev_data_wen_n : std_logic := '0';
   signal ev_data_rst_n : std_logic := '0';
   signal slink0_lff : std_logic := '0';
   signal slink1_lff : std_logic := '0';
   signal slink0_ldown : std_logic := '0';
   signal slink1_ldown : std_logic := '0';

	--BiDirs
   signal rod_bus_data : std_logic_vector(15 downto 0);
--   signal ssr1_dq : std_logic_vector(35 downto 0);
--   signal ssr2_dq : std_logic_vector(35 downto 0);

 	--Outputs
	signal xc : std_logic_vector(7 downto 0);
   signal rod_busy_n : std_logic;
   signal rod_bus_ack_out : std_logic;
   signal modebits_fifo_ef_n_out : std_logic;
   signal modebits_fifo_ff_n_out : std_logic;
   signal ev_data_almost_full_n : std_logic_vector(1 downto 0);
   signal ev_data_ready : std_logic_vector(1 downto 0);
   signal ev_id_fifo_empty_error : std_logic_vector(1 downto 0);
   signal slink0_utest : std_logic;
   signal slink1_utest : std_logic;
   signal slink0_ureset : std_logic;
   signal slink1_ureset : std_logic;
   signal slink0_uctrl : std_logic;
   signal slink1_uctrl : std_logic;
   signal slink0_data_rod2boc : std_logic_vector(15 downto 0);
   signal slink1_data_rod2boc : std_logic_vector(15 downto 0);
   signal slink0_we : std_logic;
   signal slink1_we : std_logic;
   signal slink0_uclk : std_logic;
   signal slink1_uclk : std_logic;
--   signal ssr1_ad : std_logic_vector(19 downto 0);
--   signal ssr2_ad : std_logic_vector(19 downto 0);
--   signal ssr1_adv : std_logic;
--   signal ssr2_adv : std_logic;
--   signal ssr1_bwa : std_logic;
--   signal ssr2_bwa : std_logic;
--   signal ssr1_bwb : std_logic;
--   signal ssr2_bwb : std_logic;
--   signal ssr1_bwc : std_logic;
--   signal ssr2_bwc : std_logic;
--   signal ssr1_bwd : std_logic;
--   signal ssr2_bwd : std_logic;
--   signal ssr1_cke : std_logic;
--   signal ssr2_cke : std_logic;
--   signal ssr1_clk : std_logic;
--   signal ssr2_clk : std_logic;
--   signal ssr1_csb : std_logic;
--   signal ssr2_csb : std_logic;
--   signal ssr1_lbo : std_logic;
--   signal ssr2_lbo : std_logic;
--   signal ssr1_oe : std_logic;
--   signal ssr2_oe : std_logic;
--   signal ssr1_we : std_logic;
--   signal ssr2_we : std_logic;
--   signal ssr1_zz : std_logic;
--   signal ssr2_zz : std_logic;

   -- Clock period definitions
   constant clk40_period : time := 25 ns;
   constant clk80_period : time := 12.5 ns;
   constant clk100_period : time := 10 ns;
	
   constant slink0_uclk_period : time := 10 ns;
   constant slink1_uclk_period : time := 10 ns;
--   constant ssr1_clk_period : time := 10 ns;
--   constant ssr2_clk_period : time := 10 ns;
	

	
	
	procedure read_bus(
		address : in std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '1'; rod_bus_hwsb <= '0';
		rod_bus_ds <= '0';
	   wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	   rod_bus_hwsb <= '1'; wait for clk40_period*2;
      rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';		
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
      wait for clk40_period; rod_bus_ce <= '0';	wait for clk40_period*8;	
	end read_bus;

   procedure write_bus(
		address : in std_logic_vector(15 downto 0);
		data_in : in std_logic_vector(31 downto 0);
		signal rod_bus_data : out std_logic_vector(15 downto 0);
		signal rod_bus_ce : out std_logic;
		signal rod_bus_addr : out  std_logic_vector(15 downto 0);
		signal rod_bus_rnw  : out  std_logic;
		signal rod_bus_hwsb : out  std_logic;
		signal rod_bus_ds: out std_logic)is 
	begin
		rod_bus_addr <= address; rod_bus_ce <= '1';
		rod_bus_rnw <= '0'; rod_bus_hwsb <= '0';
		rod_bus_data <= data_in(15 downto 0);
	   wait for clk40_period*3; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
	   rod_bus_hwsb <= '1'; rod_bus_data <= data_in(31 downto 16);
		wait for clk40_period*2; rod_bus_ds <= '1'; wait for clk40_period; rod_bus_ds <= '0';
		wait for clk40_period*3;
		rod_bus_hwsb <= '0';
		rod_bus_addr <= (others => '0');
      wait for clk40_period; 
		rod_bus_ce <= '0'; rod_bus_rnw <= '1'; rod_bus_data <= (others => 'Z');	
		wait for clk40_period*8;			
	end write_bus;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: rodSlave_top PORT MAP (
--          clk40 => clk40,
--          clk80 => clk80,
--          clk100 => clk100,

		clk40in_p		=> clk40in_p,
		clk40in_n		=> clk40in_n,
		clk100in_p		=> clk100in_p,
		clk100in_n		=> clk100in_n,
		xc_in => xc_in,
		xc => xc,
          reset => reset,
          rxdata_boc2rod => rxdata_boc2rod,
          rod_busy_n => rod_busy_n,
          rod_bus_rnw => rod_bus_rnw,
          rod_bus_ce => rod_bus_ce,
          rod_bus_hwsb => rod_bus_hwsb,
          rod_bus_ds => rod_bus_ds,
          rod_bus_addr => rod_bus_addr,
          rod_bus_data => rod_bus_data,
          rod_bus_ack_out => rod_bus_ack_out,
          mode_bits_in => mode_bits_in,
          modebits_fifo_wen_n_in => modebits_fifo_wen_n_in,
          modebits_fifo_rst_n_in => modebits_fifo_rst_n_in,
          modebits_fifo_ef_n_out => modebits_fifo_ef_n_out,
          modebits_fifo_ff_n_out => modebits_fifo_ff_n_out,
          ev_data_from_ppc => ev_data_from_ppc,
          ev_data_wen_n => ev_data_wen_n,
          ev_data_rst_n => ev_data_rst_n,
          ev_data_almost_full_n => ev_data_almost_full_n,
          ev_data_ready => ev_data_ready,
          ev_id_fifo_empty_error => ev_id_fifo_empty_error,
          slink0_utest => slink0_utest,
          slink1_utest => slink1_utest,
          slink0_ureset => slink0_ureset,
          slink1_ureset => slink1_ureset,
          slink0_uctrl => slink0_uctrl,
          slink1_uctrl => slink1_uctrl,
          slink0_lff => slink0_lff,
          slink1_lff => slink1_lff,
          slink0_ldown => slink0_ldown,
          slink1_ldown => slink1_ldown,
          slink0_data_rod2boc => slink0_data_rod2boc,
          slink1_data_rod2boc => slink1_data_rod2boc,
          slink0_we => slink0_we,
          slink1_we => slink1_we,
          slink0_uclk => slink0_uclk,
          slink1_uclk => slink1_uclk
--          ssr1_ad => ssr1_ad,
--          ssr2_ad => ssr2_ad,
--          ssr1_dq => ssr1_dq,
--          ssr2_dq => ssr2_dq,
--          ssr1_adv => ssr1_adv,
--          ssr2_adv => ssr2_adv,
--          ssr1_bwa => ssr1_bwa,
--          ssr2_bwa => ssr2_bwa,
--          ssr1_bwb => ssr1_bwb,
--          ssr2_bwb => ssr2_bwb,
--          ssr1_bwc => ssr1_bwc,
--          ssr2_bwc => ssr2_bwc,
--          ssr1_bwd => ssr1_bwd,
--          ssr2_bwd => ssr2_bwd,
--          ssr1_cke => ssr1_cke,
--          ssr2_cke => ssr2_cke,
--          ssr1_clk => ssr1_clk,
--          ssr2_clk => ssr2_clk,
--          ssr1_csb => ssr1_csb,
--          ssr2_csb => ssr2_csb,
--          ssr1_lbo => ssr1_lbo,
--          ssr2_lbo => ssr2_lbo,
--          ssr1_oe => ssr1_oe,
--          ssr2_oe => ssr2_oe,
--          ssr1_we => ssr1_we,
--          ssr2_we => ssr2_we,
--          ssr1_zz => ssr1_zz,
--          ssr2_zz => ssr2_zz
        );

   -- Clock process definitions
   clk40_process :process
   begin
--		clk40 <= '1';
		clk40in_p <= '1';	
		clk40in_n <= '0';	
		wait for clk40_period/2;
--		clk40 <= '0';
		clk40in_p <= '0';	
		clk40in_n <= '1';
		wait for clk40_period/2;
   end process;
	
   clk100_process :process
   begin
--		clk100 <= '0';
		clk100in_p <= '1';	
		clk100in_n <= '0';	
		wait for clk100_period/2;
--		clk100 <= '0';
		clk100in_p <= '0';	
		clk100in_n <= '1';	
		wait for clk100_period/2;
   end process; 
	
--   clk80_process :process
--   begin
--		clk80 <= '1';
--		wait for clk80_period/2;
--		clk80 <= '0';
--		wait for clk80_period/2;
--   end process;
 

 
   slink0_uclk_process :process
   begin
		slink0_uclk <= '0';
		wait for slink0_uclk_period/2;
		slink0_uclk <= '0';
		wait for slink0_uclk_period/2;
   end process;
 
   slink1_uclk_process :process
   begin
		slink1_uclk <= '0';
		wait for slink1_uclk_period/2;
		slink1_uclk <= '0';
		wait for slink1_uclk_period/2;
   end process;
 
--   ssr1_clk_process :process
--   begin
--		ssr1_clk <= '0';
--		wait for ssr1_clk_period/2;
--		ssr1_clk <= '0';
--		wait for ssr1_clk_period/2;
--   end process;
-- 
--   ssr2_clk_process :process
--   begin
--		ssr2_clk <= '0';
--		wait for ssr2_clk_period/2;
--		ssr2_clk <= '0';
--		wait for ssr2_clk_period/2;
--   end process;
-- 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rod_bus_data <=(others => 'Z');
		reset <= '1';
      wait for 100 ns;	
		reset <= '0';
      wait for 450 ns;

      read_fmt_stts_reg : read_bus(X"0022", rod_bus_ce, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
      assert (rod_bus_data /= X"1234abcd")
         report "This is incorrect!"		
         severity NOTE;

      read_fmt_ctrl_reg0 : read_bus(X"0020", rod_bus_ce, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 
		
      write_fmt_ctrl_reg : write_bus(X"0020", X"20131122", rod_bus_data, 
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 

      wait for 50 ns;


      read_fmt_ctrl_reg1 : read_bus(X"0020", rod_bus_ce, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 

      write_error_mask_14 : write_bus(X"2038", X"EE01FF34", rod_bus_data, 
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 		

      read_error_mask_14 : read_bus(X"2038", rod_bus_ce, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);

      write_slave_id_1 : write_bus(X"0005", X"00000001", rod_bus_data, -- change slave id
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 	

      write_error_mask_15 : write_bus(X"213C", X"EFBCABCD", rod_bus_data, -- write slave 1
			rod_bus_ce, rod_bus_addr, rod_bus_rnw, rod_bus_hwsb, rod_bus_ds); 

      read_error_mask_14_2 : read_bus(X"2038", rod_bus_ce, rod_bus_addr, --should be all 0s
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);

      read_error_mask_15 : read_bus(X"213C", rod_bus_ce, rod_bus_addr,
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);

      wait for 400 ns;
		reset <= '1';
      wait for 50 ns;	
		reset <= '0';
      wait for 450 ns;
      
      read_error_mask_15_2 : read_bus(X"213C", rod_bus_ce, rod_bus_addr, --should be all 0s
         rod_bus_rnw, rod_bus_hwsb, rod_bus_ds);


      wait;
   end process;

END;
