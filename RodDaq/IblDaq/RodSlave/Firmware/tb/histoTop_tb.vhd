--------------------------------------------------------------------------------
-- testbench for histoTop module.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
use work.rodHistoPack.all;
USE ieee.numeric_std.ALL;

USE ieee.std_logic_unsigned.all;

use ieee.STD_LOGIC_TEXTIO.ALL;

use Std.TextIO;
use Std.TextIO.ALL;
 
ENTITY histoTop_tb IS
END histoTop_tb;
 
ARCHITECTURE behavior OF histoTop_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
    COMPONENT histoTop
	 generic (simulation : boolean := false; useExtRam: boolean := false);
    PORT(
         control : IN  std_logic_vector(31 downto 0);
         clk : IN  std_logic;
         rst : IN  std_logic;
         enable : IN  std_logic;
         pixAddr : IN  std_logic_vector(17 downto 0);
         totVal : IN  std_logic_vector(3 downto 0);
         mode : IN  std_logic_vector(1 downto 0);
         addrOut : OUT  std_logic_vector(17 downto 0);
         histo : OUT  std_logic_vector(31 downto 0);
         rfr : IN  std_logic;
         rfd : OUT  std_logic;
         roc : OUT  std_logic;
         valid : OUT  std_logic;
         ramClk : IN  std_logic;
         ramAddr : OUT  std_logic_vector(17 downto 0);
         ramData : INOUT  std_logic_vector(35 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal control : std_logic_vector(31 downto 0) := (others => '0');
   signal clk : std_logic := '0';
   signal rst : std_logic := '0';
   signal enable : std_logic := '0';
   signal pixAddr : std_logic_vector(17 downto 0) := (others => '0');
   signal totVal : std_logic_vector(3 downto 0) := (others => '0');
   signal mode : std_logic_vector(1 downto 0) := "00";
   signal rfr : std_logic := '0';
   signal ramClk : std_logic := '0';

	--BiDirs
   signal ramData : std_logic_vector(35 downto 0);

 	--Outputs
   signal addrOut : std_logic_vector(17 downto 0);
   signal histo : std_logic_vector(31 downto 0);
   signal rfd : std_logic;
   signal roc : std_logic;
   signal valid : std_logic;
   signal ramAddr : std_logic_vector(17 downto 0);
   signal ramCke_n : std_logic;
   signal ramCs_n : std_logic;
   signal ramBw_n : std_logic_vector(3 downto 0);
   signal ramWe_n : std_logic;
   signal ramOe_n : std_logic;
   signal ramAdv : std_logic;
   signal ramLbo : std_logic;
   signal ramZz : std_logic;

	signal histoInput : std_logic := '0';
	
   -- Clock period definitions
   constant efb_clk_period : time := 10 ns;
   constant clk_period : time := 10 ns;
   constant ramClk_period : time := 10 ns;
	constant reset_delay : time := 45 ns;
	
	procedure tic (count : integer := 1) is
	begin
		for i in 1 to count loop
		 wait until rising_edge(clk);
		end loop;
	end procedure;
	
	
	constant file_histo_output : string := "histo_sim_out.txt";
	
	constant useExtRam : boolean := true;
	
	 

	    COMPONENT cy7c1370
		    GENERIC (

        -- Constant parameters
	addr_bits : INTEGER := 19;
	data_bits : INTEGER := 36
	);
        PORT (Dq	: INOUT STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
              Addr	: IN    STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
              Clk	: IN    STD_LOGIC;
              CEN_n	: IN    STD_LOGIC;
              AdvLd_n	: IN    STD_LOGIC;
              Mode	: IN    STD_LOGIC;
              Bwa_n	: IN    STD_LOGIC;
              Bwb_n	: IN    STD_LOGIC;
              Bwc_n	: IN    STD_LOGIC;
              Bwd_n	: IN    STD_LOGIC;
              Rw_n	: IN    STD_LOGIC;
              Oe_n	: IN    STD_LOGIC;
              Ce1_n	: IN    STD_LOGIC;
              Ce3_n	: IN    STD_LOGIC;
              Ce2	: IN    STD_LOGIC;
              Zz	: IN    STD_LOGIC
        );
    END COMPONENT;
	 
	 	 component ZeroOhmArray 
  generic(a_to_b_loss : time := 1 ns; 
          b_to_a_loss : time := 2 ns;
			 NBIT   : natural := 1); 
  port 
    (A : inout Std_Logic_vector(NBIT-1 downto 0); 
     B : inout Std_Logic_vector(NBIT-1 downto 0) 
     ); 
end component;

	 constant propagation_pcb_clock : time := 180 ps;
	 constant propagation_pcb_data : time := 350 ps;
	
	 
	 	   CONSTANT addr_bits	: INTEGER := 18;
    CONSTANT data_bits	: INTEGER := 36;
	
	  SIGNAL sram0_io_b		: STD_LOGIC_VECTOR (data_bits - 1 DOWNTO 0) := (OTHERS => 'Z');
    SIGNAL sram0_a_dly		: STD_LOGIC_VECTOR (addr_bits - 1 DOWNTO 0) := (OTHERS => '0');
 
   SIGNAL sram0_clk,sram0_clk_ssram,sram0_clk_fb		: STD_LOGIC;

   SIGNAL sram0_we_n,sram0_we_n_dly		: STD_LOGIC;
	
	signal print_histo : boolean := false;
	
	constant opMode: std_logic_vector(opModeCtlBits - 1 downto 0) := opLongTot; 
	--opOnlineOccupancy; --opShortTot;
	
			signal cycle : std_logic := '0';
		
	constant nchipsim : integer := getNumOfChips(true);
	constant nround : integer := 30;
	constant maxpix : integer := 336*80*nchipsim;
	constant nhits : integer  := 30; -- maxpix * 5;
	
	SIGNAL sram0_ramBw_n : std_logic_vector(3 downto 0);

	
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: histoTop 
		 generic map(simulation => true, useExtRam=> useExtRam)
		 PORT MAP (
          control => control,
          clk => clk,
          rst => rst,
          enable => enable,
          pixAddr => pixAddr,
          totVal => totVal,
          mode => mode,
          addrOut => addrOut,
          histo => histo,
          rfr => rfr,
          rfd => rfd,
          roc => roc,
          valid => valid,
          ramClk => ramClk,
          ramAddr => ramAddr,
          ramData => ramData,
          ramCke_n => ramCke_n,
          ramCs_n => ramCs_n,
          ramBw_n => ramBw_n,
          ramWe_n => ramWe_n,
          ramOe_n => ramOe_n,
          ramAdv => ramAdv,
          ramLbo => ramLbo,
          ramZz => ramZz
        );

 
   ramClk_process :process
   begin
		ramClk <= '0';
		wait for ramClk_period/2;
		ramClk <= '1';
		wait for ramClk_period/2;
   end process;

	-- in slow clock mode clk is identical to ramclk. 
	clk <= ramClk;

	-- in fast clock mode clk is ramclk divided by 2
 
	inputData :process
	variable tot : integer := 0;
	begin
		tic;
		if histoInput = '1' then

			totVal <= std_logic_vector(to_unsigned((tot mod nround), totVal'length)); -- after 1 ns;
			pixAddr <= std_logic_vector(to_unsigned(tot, pixAddr'length)); -- after 1 ns;
			enable <= '1'; -- after 1 ns;
			tot := tot + 1;
			if tot >= maxpix then 
				tot := 0;
			end if;
		else
				enable <= '0'; -- after 1 ns;
		end if;
		tic;
		enable <= '0';
--		tic(50);
	end process;

   -- Stimulus process
   stim_proc: process
   begin		
		rst <= '0';
		mode <= modeIdle;
		wait for reset_delay;	
		rst <= '1';
		print_histo <= false;
      wait for reset_delay;	
		rst <= '0';


		
      --control <= B"0000_0000_0000_0000_0000_0000_0000_0000";
		control(chipCtlShift + chipCtlBits - 1 downto chipCtlShift) <= chipCtlOne;-- chipCtlNull(0); -- read out data of 1 or 8 FE
--		control(scanTypeCtlShift) <= scanTypeToT(0); -- read out all values
		control(rowMaskCtlShift + rowMaskCtlBits - 1 downto rowMaskCtlShift) <= rowMaskNone;
		
		control(opModeCtlShift + opModeCtlBits - 1 downto opModeCtlShift) <= opMode; --opLongTot;
		control(expectedOccCtlShift + expectedOccCtlBits - 1 downto expectedOccCtlShift) <= (others => '1');
	
		wait for clk_period/2*11;
	
-- temp	
--		wait on  rfd'transaction until rfd = '1'; -- is in SAMPLE
--		mode <= '1'; --- move till IDLE
--		tic(4);
--		rfr <= '1';
--		wait on  roc'transaction until roc = '1';		
--		tic(4);
--		rfr <= '0';
--		mode <= '0';  -- move to SAMPLE
-- end temp

      -- insert stimulus here 
		-- 
		-- ENTER SAMPLE MODE
		tic(100);
		mode <= modeSample;
		
	--	tic(215040);
		wait on  rfd'transaction until rfd = '1'; -- is in SAMPLE
		tic(2000);
		
		-- write test data (tot[addr] = addr)
		histoInput <= '1';
		tic(nhits * 2);
		
		histoInput <= '0';
		tic(20);
		-- TODO MK: mode needs to be asserted for 3 cycles after enable goes low to make sure all data is collected?
		-- TODO MK: what about READOUT -> CLEAN -> IDLE/RESET?
		
		-- end sample
		print_histo <= true;
		mode <= modeReadoutWait;
		tic(4);
		rfr <= '1';

		--- readout block ---
		mode <= modeReadout;
		

--		--- step ---
--		wait for (20 * clk_period);
--		rfr <= '0';
--		wait for (14 * clk_period);
--		rfr <= '1';		
--
--		--- step ---
--		wait for (30 * clk_period);
--		rfr <= '0';
--		wait for (14 * clk_period);
--		rfr <= '1';		

	 -- wait for roc
	wait on  roc'transaction until roc = '1'; -- is in SAMPLE

		--- step ---

		--tic(215040);
		-- we're still cleaning ...
		tic(100);
		mode <= modeIdle;

		tic(100);
		mode <= modeSample;

		wait on  rfd'transaction until rfd = '1'; -- is in SAMPLE

		tic(100);
		
		-- end sample
		mode <= modeReadoutWait;
		tic(4);

		mode <= modeReadout;
		tic(4);

		wait on  roc'transaction until roc = '1'; -- is in SAMPLE
		tic(4);
		
		
      wait;
   end process;
	
	Histo2File: process
		file my_output : TEXT open WRITE_MODE is file_histo_output;
       variable my_out_line,my_display_line : LINE;
--			variable pixaddr : character;
			variable tot : integer;
			variable totsqr : integer;
			variable occ : integer;
			variable totflag : integer;
	

	begin

			wait until rising_edge(clk);
			if( valid = '1' and print_histo = true) then
--			write(my_display_line, string'("PixAddr ="));
		if(opMode = opShortTot) then
			occ := to_integer(unsigned(histo(3 downto 0 )));
			tot := to_integer(unsigned(histo(15 downto 4 )));
			totsqr := to_integer(unsigned(histo(31 downto 16 )));
--			totflag := to_integer(unsigned(histo(7 downto 7)));
	
			write(my_display_line, occ);
			write(my_display_line, string'(" "));
--			write(my_display_line, totflag);
--			write(my_display_line, string'(" "));
			write(my_display_line, tot);
			write(my_display_line, string'(" "));
			write(my_display_line, totsqr);
			

			write(my_out_line, occ);
			write(my_out_line, string'(" "));
--			write(my_out_line, totflag);
--			write(my_out_line, string'(" "));
			write(my_out_line, tot);
			write(my_out_line, string'(" "));
			write(my_out_line, totsqr);
	
		elsif (opMode = opOnlineOccupancy) then
			occ := to_integer(unsigned(histo(23 downto 0 )));
			write(my_display_line, occ);
			write(my_display_line, string'(" "));
			write(my_out_line, occ);
			write(my_out_line, string'(" "));
	
			
		elsif (opMode = opLongTot) then
			if(cycle = '1') then
				tot := to_integer(unsigned(histo(11 downto 0 )));
				totsqr := to_integer(unsigned(histo(31 downto 16 )));
--				write(my_display_line, totflag);
--				write(my_display_line, string'(" "));
				write(my_display_line, tot);
				write(my_display_line, string'(" "));
				write(my_display_line, totsqr);
				write(my_display_line, string'(" "));
	
--				write(my_out_line, totflag);
--				write(my_out_line, string'(" "));
				write(my_out_line, tot);
				write(my_out_line, string'(" "));
				write(my_out_line, totsqr);
				write(my_out_line, string'(" "));
	
				
			else
				occ := to_integer(unsigned(histo(7 downto 0 )));
				write(my_display_line, occ);
				write(my_display_line, string'(" "));
				write(my_out_line, occ);
				write(my_out_line, string'(" "));
		
			end if;
			
			if(cycle = '1') then
				writeline(output, my_display_line);
				TextIO.WriteLine(my_output,my_out_line);
			end if;
			
			
			cycle <= not(cycle);
			
	

		end if;
	
	
			end if;
			

	end process Histo2File;
	
	
--	sram0_clk_fb <= transport sram0_clk_ssram after propagation_pcb_clock;
	sram0_clk_ssram <= ramClk; --transport sram0_clk after propagation_pcb_clock;
	sram0_a_dly <= transport ramAddr after propagation_pcb_data;
	sram0_we_n_dly <= transport ramWe_n after propagation_pcb_data;
	sram0_ramBw_n <= transport ramBw_n after propagation_pcb_data;
	
			   cy7c1370_instance : cy7c1370
				GENERIC MAP(addr_bits => addr_bits, data_bits => data_bits)
				PORT MAP (Dq => ramData, -- sram0_io_b, 
						Addr => sram0_a_dly, Clk => sram0_clk_ssram, CEN_n => ramCke_n,
                  Mode => ramLbo, AdvLd_n => ramAdv, 
						Bwa_n => sram0_ramBw_n(0), Bwb_n => sram0_ramBw_n(1), 
						Bwc_n => sram0_ramBw_n(2), Bwd_n => sram0_ramBw_n(3),
                  Rw_n => sram0_we_n_dly, Oe_n => ramOe_n, 
						Ce1_n => ramCs_n, Ce3_n => '0', Ce2 => '1', Zz => ramZz);

--						
--	Inst_bidirbus: ZeroOhmArray
--  generic map (a_to_b_loss => propagation_pcb_data,
--          b_to_a_loss => propagation_pcb_data,
--			 NBIT => 36) 
--  port map
--    (A => ramData,
--     B => sram0_io_b
--     ); 

END;
