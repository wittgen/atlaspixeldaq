-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

  -- Component Declaration
          COMPONENT histoUnit
          PORT(
			control : in  STD_LOGIC_VECTOR (31 downto 0);
         hitEnable : IN  std_logic;
			row : in  STD_LOGIC_VECTOR (rowBits - 1 downto 0);
			col : in  STD_LOGIC_VECTOR (colBits - 1 downto 0);
			chip : in  STD_LOGIC_VECTOR (chipBits - 1 downto 0);
			totVal : IN  std_logic_vector(totWidth - 1 downto 0);
         rfd : OUT  std_logic;
         mode : IN  std_logic;
         rfr : IN  std_logic;
         roc : OUT  std_logic;
         histValid : OUT  std_logic;
         histo : OUT  std_logic_vector(31 downto 0);
         rst : IN  std_logic;
         clk : IN  std_logic;
         ramClk : IN  std_logic;
         ramAddr : OUT  std_logic_vector(addrBits - 1 downto 0);
         ramData : INOUT  std_logic_vector(histDataBits - 1 downto 0);
         ramCke_n : OUT  std_logic;
         ramCs_n : OUT  std_logic;
         ramBw_n : OUT  std_logic_vector(3 downto 0);
         ramWe_n : OUT  std_logic;
         ramOe_n : OUT  std_logic;
         ramAdv : OUT  std_logic;
         ramLbo : OUT  std_logic;
         ramZz : OUT  std_logic
                  );
          END COMPONENT;


  BEGIN

  -- Component Instantiation
          uut: rodHisto PORT MAP(
--			 control => localHistCtlReg0,
control => x"23000010",
--          hitEnable => hitEnable0_r,
hitEnable => rfd0,
--          row => row0_r,
row =>(others => '0'),
--          col => col0_r,
col =>(others => '0'),
--          chip => chip0_r,
chip =>(others => '0'),
--          totVal => totVal0_r,
totVal =>"0001",
          rfd => rfd0,
--          mode => effectiveCtlReg(ctlHistMode0Bit),
mode => '0',
--          rfr => rfr0,
rfr =>  '0',
          roc => roc0,
          histValid => histValid0,
          histo => histo0,
          rst => cpurst,
          clk => cpuclk,
--          ramClk => cpuclk,--cpuclk2x
--          ramAddr => ramAddr0,
--          ramData => ramData0,
--          ramCke_n => ramCke0_n,
--          ramCs_n => ramCs0_n,
--          ramBw_n => ramBw0_n,
--          ramWe_n => ramWe0_n,
--          ramOe_n => ramOe0_n,
--          ramAdv => ramAdv0,
--          ramLbo => ramLbo0,
--          ramZz => ramZz0
          ramClk => cpuclk,
          ramAddr => sram1_a,
          ramData => sram1_io,
          ramCke_n => sram1_cke_n,
          ramCs_n => sram1_cs1,
          ramBw_n(0) => sram1_bw1_n,
          ramBw_n(1) => sram1_bw2_n,
          ramBw_n(2) => sram1_bw3_n,
          ramBw_n(3) => sram1_bw4_n,
          ramWe_n => sram1_we_n,
          ramOe_n => sram1_oe_n,
          ramAdv => sram1_AdvLd_n,
          ramLbo => sram1_lbo,
          ramZz => sram1_zz
          );


  --  Test Bench Statements
     tb : PROCESS
     BEGIN

        wait for 100 ns; -- wait until global set/reset completes

        -- Add user defined stimulus here

        wait; -- will wait forever
     END PROCESS tb;
  --  End Test Bench 

  END;
