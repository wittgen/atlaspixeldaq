####################################################################################
## CLOCK ##
####################################################################################
NET CLK100in_P LOC = V26;
NET CLK100in_N LOC = V27;
NET CLK100in_P TNM_NET = sys_clk_pin;
TIMESPEC TS_sys_clk_pin = PERIOD sys_clk_pin 100000 kHz HIGH 50%;
#NET CLK100in_N TNM_NET = CLK100in_N;
#TIMESPEC TS_CLK100in_N = PERIOD "CLK100in_N" TS_sys_clk_pin PHASE 5 ns HIGH 50%;

NET CLK40in_P LOC = B15;
NET CLK40in_N LOC = A15; 
NET CLK40in_P TNM_NET = boc_clk_pin;
TIMESPEC TS_boc_clk_pin = PERIOD boc_clk_pin 40000 kHz HIGH 50%;
#NET CLK40in_N TNM_NET = CLK40in_N;
#TIMESPEC TS_CLK40in_N = PERIOD "CLK40in_N" TS_boc_clk_pin PHASE 12.5 ns HIGH 50%;

NET reset LOC = P24 | IOSTANDARD = LVCMOS25 | TIG; # timing analysis ignored   ------ SFV0
NET reset_processor_core_SEL LOC = T24 | IOSTANDARD = LVCMOS25 | TIG; # SFV8 

NET rod_busy_n LOC = AG28 | IOSTANDARD = LVCMOS25;

# clock test outputs
NET cpuclkmon: LOC = C15 |IOSTANDARD = LVCMOS25 | slew = slow | DRIVE = 8; # fe emu clk -> j6/7.48
NET clk40mon: LOC = G13 |IOSTANDARD = LVCMOS25 | slew = slow | DRIVE = 8;  # fe emu in 0 -> j6/7.4
NET clk100mon: LOC = C7 |IOSTANDARD = LVCMOS25 | slew = slow | DRIVE = 8;  # fe emu in 2 -> j6/7.6

# sram1 clock
NET clk200out<0> LOC = W4 |IOSTANDARD = LVCMOS25 | drive = 8 |slew = fast;
# sram1clock: pin are far apart
NET clk200in<0> LOC = AB30 |IOSTANDARD = LVCMOS25 | nodelay;
# sram2 clock
NET clk200out<1> LOC = A17 |IOSTANDARD = LVCMOS25 | drive = 8 |slew = fast;
# pin are next to each other
NET clk200in<1> LOC = A18 |IOSTANDARD = LVCMOS25 | nodelay;

NET clk200in<*> TNM_NET = clk200;

#TIMESPEC TS_clk200 = PERIOD clk200 100000 kHz;##200MHz --
NET "*cpuclk*" TNM_NET = clk100_form;
NET "*sram1_inclk" TNM_NET = sram1_contr_0;
NET "*sram2_inclk" TNM_NET = sram2_contr_0;

TIMESPEC TS_false_path1 = FROM clk100_form  TO  sram1_contr_0    TIG;
TIMESPEC TS_false_path2 = FROM clk100_form  TO  sram2_contr_0    TIG;
TIMESPEC TS_false_path3 = FROM sram1_contr_0  TO  clk100_form    TIG;
TIMESPEC TS_false_path4 = FROM sram2_contr_0  TO  clk100_form    TIG;

//ssram clock resource locked
INST "*ssram_clk_gen_inst/clkfb1_bufio2fb" LOC = BUFIO2FB_X3Y12;
INST "*ssram_clk_gen_inst/pll1_base_inst" LOC = PLL_ADV_X0Y1;
INST "*ssram_clk_gen_inst/clk1_ext_bufio2*" LOC = BUFIO2_X3Y12;

INST "*ssram_clk_gen_inst/clkfb2_bufio2fb" LOC = BUFIO2FB_X4Y26;
INST "*ssram_clk_gen_inst/pll2_base_inst" LOC = PLL_ADV_X0Y3;
INST "*ssram_clk_gen_inst/clk2_ext_bufio2*" LOC = BUFIO2_X4Y20;
##locate mblaze clock generator
inst *system_i/clock_generator_0/clock_generator_0/PLL0_INST/Using_PLL_ADV.PLL_ADV_inst loc = PLL_ADV_X0Y0;
## PLL1_INST is the important one, connects to mcb bufpll and must(should?) be placed at PLL_ADV_X0Y2
inst *system_i/clock_generator_0/clock_generator_0/PLL1_INST/Using_PLL_ADV.PLL_ADV_inst loc = PLL_ADV_X0Y2;

inst *system_i/ETHERNET/ETHERNET/SOFT_SYS.I_TEMAC/GEN_GMII.I_GMII/clock_inst/bufgmux_speed_clk loc = BUFGMUX_X2Y3;

#NET "controller_instance/I_pipelined_stages_p/rw_p3<0>" MAXDELAY = 3 ns;
#
# pin constraints
#
NET ETHERNET_MDC LOC = "A10"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_MDIO LOC = "A11"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_MII_TX_CLK LOC = "c16"  |IOSTANDARD = "LVCMOS25"; #AK pin location changed
NET ETHERNET_PHY_RST_N LOC = "A12"  |IOSTANDARD = "LVCMOS25"  |TIG;
NET ETHERNET_RXD<0> LOC = "B9"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<1> LOC = "C9"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<2> LOC = "D9"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<3> LOC = "C10"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<4> LOC = "D10"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<5> LOC = "E10"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<6> LOC = "C11"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RXD<7> LOC = "D11"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RX_CLK LOC = "A16"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RX_DV LOC = "A13"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_RX_ER LOC = "A14"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<0> LOC = "C12"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<1> LOC = "D12"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<2> LOC = "E12"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<3> LOC = "C13"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<4> LOC = "D13"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<5> LOC = "C14"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<6> LOC = "D14"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TXD<7> LOC = "E14"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TX_CLK LOC = "A8"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TX_EN LOC = "B11"  |IOSTANDARD = "LVCMOS25";
NET ETHERNET_TX_ER LOC = "B13"  |IOSTANDARD = "LVCMOS25";
#NET RESET_in LOC = "P24"  |IOSTANDARD = "LVCMOS25"  |TIG; #SPC
# uart pins on unused pins
#NET RS232_Uart_1_sin LOC = "AE26"  |IOSTANDARD = "LVCMOS25";
#NET RS232_Uart_1_sout LOC = "AE25"  |IOSTANDARD = "LVCMOS25";
# uart pins on spare pins to master: spare_to/from_v5 [1] => spare[1], spare[10] on V5
NET RS232_Uart_1_sin LOC = "P25"  |IOSTANDARD = "LVCMOS25";
NET RS232_Uart_1_sout LOC = "W25"  |IOSTANDARD = "LVCMOS25";
NET rzq IOSTANDARD = "SSTL18_II";

# ddr2 chip select
net mcbx_dram_cs_n loc = a5 |IOSTANDARD = "SSTL18_II";


############################################################
# Changed to add Drive strength and INST Name
#
INST "ETHERNET_TXD_?_OBUF"  SLEW = FAST;
INST "ETHERNET_TX_EN_OBUF"  SLEW = FAST;
INST "ETHERNET_TX_ER_OBUF"  SLEW = FAST;
INST "ETHERNET_TX_CLK_OBUF" SLEW = FAST;

############################################################
# GMII: IODELAY Constraints				   #
############################################################
# Please modify the value of the IDELAY_VALUE
# according to your design.
# For more information on IDELAYCTRL and IODELAY, please
# refer to the Spartan-6 User Guide.
#
INST "*delay_gmii_rx_dv"	   IDELAY_VALUE = 35;
INST "*delay_gmii_rx_er"	   IDELAY_VALUE = 35;
INST "*data_bus<0>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<1>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<2>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<3>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<4>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<5>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<6>.delay_gmii_rxd" IDELAY_VALUE = 35;
INST "*data_bus<7>.delay_gmii_rxd" IDELAY_VALUE = 35;

# Group IODELAY and IDELAYCTRL components to aid placement
# INST "*delay_gmii_rx_clk" IODELAY_GROUP = "grp1";
INST "*delay_gmii_rx_dv"    IODELAY_GROUP = "grp1";
INST "*delay_gmii_rx_er"    IODELAY_GROUP = "grp1";
INST "*delay_gmii_rxd"      IODELAY_GROUP = "grp1";
# INST "*dlyctrl"           IODELAY_GROUP = "grp1";
# Changed to let the tools pick the LOC
# INST *trimac_block*clock_inst*BUFGMUX_SPEED_CLK LOC = BUFGMUX_X3Y13;

# this is ISE 14.5
# NET "*/rx_mac_aclk" TNM_NET = "phy_clk_rx";
# using ETHERNET_RX_CLK instead works, but might not do what we want.
# NET "ETHERNET_RX_CLK" TNM_NET = "phy_clk_rx";
# from system/implementation/system_ethernet_wrapper.ncf we know the net name: rx_mac_aclk_int
# but this is not the real one. Planahead says: ETHERNET/from_temac_rx_mac_aclk in ISE 14.7
# this is ISE 14.7
# NET "*/from_temac_rx_mac_aclk" TNM_NET = "phy_clk_rx";
# TIMESPEC "TS_ETHERNET_rx_clk" = PERIOD "phy_clk_rx" 8000 ps HIGH 50 %;
# let's try this to be compatible with current errorneous signal naming:
NET "*/*rx_mac_aclk*" TNM_NET = "phy_clk_rx";
TIMESPEC "TS_ETHERNET_rx_clk" = PERIOD "phy_clk_rx" 8000 ps HIGH 50 %;


# axi_ethernet: AXIStream and AXI Lite clocks are the same so no need for clock domain crossing constraints
TIMESPEC "TS_ETHERNET_AXISTREAMCLKS_2_RX_MAC_ACLK_ucf" = FROM "axistream_clk"  TO "phy_clk_rx"    8000  ps DATAPATHONLY;
TIMESPEC "TS_ETHERNET_RX_MAC_ACLK_2_AXISTREAMCLKS_ucf" = FROM "phy_clk_rx"         TO "axistream_clk" 10000 PS DATAPATHONLY;
TIMESPEC "TS_ETHERNET_AXI4LITECLKS_2_RX_MAC_ACLK_ucf"  = FROM "axi4lite_clk"   TO "phy_clk_rx"    8000  ps DATAPATHONLY;
TIMESPEC "TS_ETHERNET_RX_MAC_ACLK_2_AXI4LITECLKS_ucf"  = FROM "phy_clk_rx"         TO "axi4lite_clk"  10000 PS DATAPATHONLY;
TIMESPEC "TS_ETHERNET_RX_MAC_ACLK_2_TX_MAC_ACLK_ucf"   = FROM "phy_clk_rx"         TO "clk_tx_mac"    8000  ps DATAPATHONLY;
TIMESPEC "TS_ETHERNET_TX_MAC_ACLK_2_RX_MAC_ACLK_ucf"   = FROM "clk_tx_mac"     TO "phy_clk_rx"    8000  ps DATAPATHONLY;
TIMESPEC "TS_ETHERNET_RX_MAC_ACLK_2_GTX_CLK_ucf"       = FROM "phy_clk_rx"         TO "clk_gtx"       8000  ps DATAPATHONLY;
TIMESPEC "TS_ETHERNET_GTX_CLK_2_RX_MAC_ACLK_ucf"       = FROM "clk_gtx"        TO "phy_clk_rx"    8000  ps DATAPATHONLY;
#
OFFSET = IN  2.4 ns VALID 2.8 ns BEFORE "ETHERNET_RX_CLK";
#
##########################################################################
# ROD Master to FE (route through Slave for SSTL3_I)
NET "xc(*)" IOSTANDARD = SSTL3_I;
NET xc(0) LOC = AC18;
NET xc(1) LOC = AE17;
NET xc(2) LOC = AC16;
NET xc(3) LOC = AH16;
NET xc(4) LOC = AB15;
NET xc(5) LOC = AC14;
NET xc(6) LOC = AE14;
NET xc(7) LOC = AH14;

NET "xc_in(*)" IOSTANDARD = LVCMOS25;
NET xc_in(0) LOC = AC27;
NET xc_in(1) LOC = AC28;
NET xc_in(2) LOC = AC29;
NET xc_in(3) LOC = AC30;
NET xc_in(4) LOC = AE29;
NET xc_in(5) LOC = AE30;
NET xc_in(6) LOC = AE27;
NET xc_in(7) LOC = AE28;

#########################################################################
# BOC <--> ROD signals
# bottom right
NET rxdata_boc2rod(0)		LOC =	AG17	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(1)		LOC =	AG19	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(2)		LOC =	AE18	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(3)		LOC =	AE19	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(4)		LOC =	AG21	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(5)		LOC =	AH22	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(6)		LOC =	AK22	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(7)		LOC =	AK24	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(8)		LOC =	AG23	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(9)		LOC =	AE22	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(10)	LOC =	AH24	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(11)	LOC =	AE24	|IOSTANDARD= SSTL3_I;
# bottom right
NET rxdata_boc2rod(12)	LOC =	AD16	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(13)	LOC =	AF17	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(14)	LOC =	AG18	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(15)	LOC =	AF19	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(16)	LOC =	AD18	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(17)	LOC =	AD19	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(18)	LOC =	AF21	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(19)	LOC =	AG22	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(20)	LOC =	AJ24	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(21)	LOC =	AF23	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(22)	LOC =	AD22	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(23)	LOC =	AG24	|IOSTANDARD= SSTL3_I;
# bottom left
NET rxdata_boc2rod(24)	LOC =	AG6		|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(25)	LOC =	AD8		|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(26)	LOC =	AF7		|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(27)	LOC =	AF9		|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(28)	LOC =	AG10	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(29)	LOC =	AJ10	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(30)	LOC =	AF11	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(31)	LOC =	AJ12	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(32)	LOC =	AF25	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(33)	LOC =	AD24	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(34)	LOC =	AG15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(35)	LOC =	AE16	|IOSTANDARD= SSTL3_I;
# bottom left
NET rxdata_boc2rod(36)	LOC =	AD12	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(37)	LOC =	AG12	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(38)	LOC =	AF13	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(39)	LOC =	AH13	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(40)	LOC =	AJ14	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(41)	LOC =	AH15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(42)	LOC =	AD15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(43)	LOC =	AF15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(44)	LOC =	AE15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(45)	LOC =	AK15	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(46)	LOC =	AG13	|IOSTANDARD= SSTL3_I;
NET rxdata_boc2rod(47)	LOC =	AK14	|IOSTANDARD= SSTL3_I;

#########################################################################
# ROD Bus signals
NET "ROD_bus_ce" LOC = C30;
NET "ROD_bus_ce" IOSTANDARD = LVCMOS25;
INST "ROD_bus_ce" TNM = INPUT_RODBUS;

NET "ROD_bus_rnw" LOC = C29;
NET "ROD_bus_rnw" IOSTANDARD = LVCMOS25;
INST "ROD_bus_rnw" TNM = INPUT_RODBUS;

NET "ROD_bus_hwsb" LOC = B30;
NET "ROD_bus_hwsb" IOSTANDARD = LVCMOS25;
INST "ROD_bus_hwsb" TNM = INPUT_RODBUS;

NET "ROD_bus_ds" LOC = A29;
NET "ROD_bus_ds" IOSTANDARD = LVCMOS25;
INST "ROD_bus_ds" TNM = INPUT_RODBUS;

NET "ROD_bus_addr[0]"  LOC = F30;
NET "ROD_bus_addr[1]"  LOC = F28;
NET "ROD_bus_addr[2]"  LOC = G28;
NET "ROD_bus_addr[3]"  LOC = G27;
NET "ROD_bus_addr[4]"  LOC = J28;
NET "ROD_bus_addr[5]"  LOC = J27;
NET "ROD_bus_addr[6]"  LOC = K27;
NET "ROD_bus_addr[7]"  LOC = K26;
NET "ROD_bus_addr[8]"  LOC = H27;
NET "ROD_bus_addr[9]"  LOC = H26;
NET "ROD_bus_addr[10]" LOC = E30;
NET "ROD_bus_addr[11]" LOC = E29;
NET "ROD_bus_addr[12]" LOC = E28;
NET "ROD_bus_addr[13]" LOC = E27;
NET "ROD_bus_addr[14]" LOC = D30;
NET "ROD_bus_addr[15]" LOC = D28;
#NET "ROD_bus_addr[16]" LOC = C27;
#NET "ROD_bus_addr[17]" LOC = D27;
NET "ROD_bus_addr[*]" IOSTANDARD = LVCMOS25;

NET "ROD_bus_data[0]"  LOC = M30;
NET "ROD_bus_data[1]"  LOC = M28;
NET "ROD_bus_data[2]"  LOC = M27;
NET "ROD_bus_data[3]"  LOC = M26;
NET "ROD_bus_data[4]"  LOC = K30;
NET "ROD_bus_data[5]"  LOC = K28;
NET "ROD_bus_data[6]"  LOC = L30;
NET "ROD_bus_data[7]"  LOC = L29;
NET "ROD_bus_data[8]"  LOC = L28;
NET "ROD_bus_data[9]"  LOC = L27;
NET "ROD_bus_data[10]" LOC = H30;
NET "ROD_bus_data[11]" LOC = H28;
NET "ROD_bus_data[12]" LOC = G30;
NET "ROD_bus_data[13]" LOC = G29;
NET "ROD_bus_data[14]" LOC = J30;
NET "ROD_bus_data[15]" LOC = J29;
NET "ROD_bus_data[*]" IOSTANDARD = LVCMOS25 | slew = slow;
#NET "ROD_bus_data_inout[*]" OUT_TERM = UNTUNED_50;

NET "ROD_bus_ack_out" LOC = L25;
NET "ROD_bus_ack_out" IOSTANDARD = LVCMOS25 | slew = fast;
INST "ROD_bus_ack_out" TNM = OUTPUT_RODBUS;

TIMEGRP "INPUT_RODBUS" OFFSET = IN 17 ns VALID 18 ns BEFORE "CLK40in_P";

TIMEGRP "OUTPUT_RODBUS" OFFSET = OUT 20 ns AFTER "CLK40in_P";

#########################################################################

NET "mode_bits_in[0]"  LOC = P26;
NET "mode_bits_in[1]"  LOC = P30;
NET "mode_bits_in[2]"  LOC = P28;
NET "mode_bits_in[3]"  LOC = N28;
NET "mode_bits_in[4]"  LOC = N27;
NET "mode_bits_in[5]"  LOC = N30;
NET "mode_bits_in[6]"  LOC = N29;
NET "mode_bits_in[7]"  LOC = M24;
NET "mode_bits_in[8]"  LOC = M23;
NET "mode_bits_in[9]"  LOC = W28;
NET "mode_bits_in[10]" LOC = AB28;
NET "mode_bits_in[11]" LOC = AG30;
NET "mode_bits_in[*]" IOSTANDARD = LVCMOS25;

NET "modebits_fifo_wen_n_in" LOC = W22 | IOSTANDARD = LVCMOS25 | slew = fast;
NET "modebits_fifo_rst_n_in" LOC = U25 | IOSTANDARD = LVCMOS25 | slew = fast;

NET "modebits_fifo_ef_n_out" LOC = AK28 | IOSTANDARD = LVCMOS25 | slew = fast;
NET "modebits_fifo_ff_n_out" LOC = U24  | IOSTANDARD = LVCMOS25 | slew = fast;


###############################################################################

NET "ev_data_from_ppc[0]"  LOC = W27;
NET "ev_data_from_ppc[1]"  LOC = V30;
NET "ev_data_from_ppc[2]"  LOC = V28;
NET "ev_data_from_ppc[3]"  LOC = U28;
NET "ev_data_from_ppc[4]"  LOC = U27;
NET "ev_data_from_ppc[5]"  LOC = U30;
NET "ev_data_from_ppc[6]"  LOC = U29;
NET "ev_data_from_ppc[7]"  LOC = T30;
NET "ev_data_from_ppc[8]"  LOC = T28;
NET "ev_data_from_ppc[9]"  LOC = T27;
NET "ev_data_from_ppc[10]" LOC = T26;
NET "ev_data_from_ppc[11]" LOC = R28;
NET "ev_data_from_ppc[12]" LOC = R27;
NET "ev_data_from_ppc[13]" LOC = R30;
NET "ev_data_from_ppc[14]" LOC = R29;
NET "ev_data_from_ppc[15]" LOC = P27;
NET "ev_data_from_ppc[*]"  IOSTANDARD = LVCMOS25;

NET "ev_data_wen_n" LOC = W21 | IOSTANDARD = LVCMOS25 | slew = fast;
NET "ev_data_rst_n" LOC = AD27 | IOSTANDARD = LVCMOS25 | slew = fast;

NET "ev_data_almost_full_n[0]" LOC = R24 | IOSTANDARD = LVCMOS25 | slew = fast;		#spare_from_v5[4]
NET "ev_data_almost_full_n[1]" LOC = R25 | IOSTANDARD = LVCMOS25 | slew = fast;		#spare_from_v5[5]

NET "ev_data_ready[0]" LOC = AJ29 | IOSTANDARD = LVCMOS25 | slew = fast; 				#spare_from_v5[6]
NET "ev_data_ready[1]" LOC = AK29 | IOSTANDARD = LVCMOS25 | slew = fast; 				#spare_from_v5[7]

NET "ev_id_fifo_empty_error[0]" LOC = AH30 | IOSTANDARD = LVCMOS25 | slew = fast;	#spare_from_v5[2]
NET "ev_id_fifo_empty_error[1]" LOC = AJ30 | IOSTANDARD = LVCMOS25 | slew = fast;	#spare_from_v5[3]

NET "fe_cmdpulse" LOC = AJ28 | IOSTANDARD = LVCMOS25;

#########################################################################

NET slink0_utest					LOC = AE23 |IOSTANDARD= SSTL3_I;
NET slink1_utest					LOC = AD21 |IOSTANDARD= SSTL3_I;
NET slink0_ureset					LOC = AJ8  |IOSTANDARD= SSTL3_I;
NET slink1_ureset					LOC = AH7  |IOSTANDARD= SSTL3_I;
NET slink0_uctrl					LOC = AE20 |IOSTANDARD= SSTL3_I;	
NET slink1_uctrl					LOC = AK23 |IOSTANDARD= SSTL3_I;	
NET slink0_lff						LOC = AG8  |IOSTANDARD= SSTL3_I;
NET slink1_lff						LOC = AD9  |IOSTANDARD= SSTL3_I;
NET slink0_ldown					LOC = AH20 |IOSTANDARD= SSTL3_I;	
NET slink1_ldown					LOC = AB19 |IOSTANDARD= SSTL3_I;	
NET slink0_we					   	LOC = AK21 |IOSTANDARD= SSTL3_I;
NET slink1_we					   	LOC = AC20 |IOSTANDARD= SSTL3_I;
NET slink0_uclk						LOC = AK17 |IOSTANDARD= SSTL3_I;			
NET slink1_uclk						LOC = AH17 |IOSTANDARD= SSTL3_I;			

NET slink0_data_rod2boc(0)  LOC = AH12;
NET slink0_data_rod2boc(1) 	LOC = AE12;
NET slink0_data_rod2boc(2) 	LOC = AK12;
NET slink0_data_rod2boc(3)  LOC = AG11;
NET slink0_data_rod2boc(4)  LOC = AK10;
NET slink0_data_rod2boc(5)  LOC = AH10;
NET slink0_data_rod2boc(6)  LOC = AG9 ;
NET slink0_data_rod2boc(7)  LOC = AG7 ;
NET slink0_data_rod2boc(8)  LOC = AH6 ;
NET slink0_data_rod2boc(9)  LOC = AD23;
NET slink0_data_rod2boc(10) LOC = AC21;
NET slink0_data_rod2boc(11) LOC = AD20;
NET slink0_data_rod2boc(12) LOC = AH23;
NET slink0_data_rod2boc(13) LOC = AH21;
NET slink0_data_rod2boc(14) LOC = AJ20;
NET slink0_data_rod2boc(15) LOC = AB20;
NET slink0_data_rod2boc(*) IOSTANDARD= SSTL3_I;

NET slink1_data_rod2boc(0) 	LOC = AG20;
NET slink1_data_rod2boc(1) 	LOC = AA19;
NET slink1_data_rod2boc(2) 	LOC = AB18;
NET slink1_data_rod2boc(3) 	LOC = AD17;
NET slink1_data_rod2boc(4)  LOC = AB16;
NET slink1_data_rod2boc(5)  LOC = AG16;
NET slink1_data_rod2boc(6)  LOC = AA15;
NET slink1_data_rod2boc(7)  LOC = AB14;
NET slink1_data_rod2boc(8)  LOC = AD14;
NET slink1_data_rod2boc(9)  LOC = AG14;
NET slink1_data_rod2boc(10) LOC = AD13;
NET slink1_data_rod2boc(11) LOC = AH11;
NET slink1_data_rod2boc(12) LOC = AB12;
NET slink1_data_rod2boc(13) LOC = AB11;
NET slink1_data_rod2boc(14) LOC = AH9 ;
NET slink1_data_rod2boc(15)	LOC = AD10;
NET slink1_data_rod2boc(*)	IOSTANDARD= SSTL3_I;


####################################################################################
# SSRAM  I/O costraints
#
# CY7C1370D-250
#OUTPUT
#  ck-to-out 2.6 ns (max)
#  hold-after-clock 1.0 ns (min)
#INPUT
#  setup-time 1.2 ns (min)
#  hold-time 0.3 ns (min)
#
# propagation max 0.2 ns
#
# OFFSET IN -> period - (ck-to-out + propagation) + phase-> period - 2.8 ns + phase; 
# VALID -> period - ck-to-out +   hold-after-clock -> period - 1.6 ns;
# OFFSET OUT -> period - (setup-time + propagation) - phase -> period - 1.4 ns - phase;
# N.B.: phase with sign! Phase is referred to the internal clock shift!
#
# FREQ (MHz)   OFF-IN(ns)      VALID(ns)        OFF-OUT(ns)
#  100          7.2              8.4             8.6
#  150          3.9              5.1             5.3
#  160          3.45             4.65            4.85
#  170          3.08             4.28            4.48
#  200          2.2              3.4             3.6             
##100 MHz
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 7.2 ns VALID 8.4 ns BEFORE "clk_p" RISING;
#TIMEGRP "OUT_TO_SSRAM" OFFSET = OUT 8.6 ns AFTER "clk_p" ;
##150 MHz
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 3.9 ns VALID 5.1 ns BEFORE "clk_p" RISING;
#TIMEGRP "OUT_TO_SSRAM" OFFSET = OUT 5.3 ns AFTER "clk_p" ;
##160 MHz ; -27 degrees phase internal clock
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 3.14 ns VALID 4.65 ns BEFORE "clk_p" RISING;
#TIMEGRP "OUT_TO_SSRAM" OFFSET = OUT 5.16 ns AFTER "clk_p" ;
##170 MHz ; -27 degrees phase internal clock
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 2.639 ns VALID 4.28 ns BEFORE "clk_p" RISING;
#TIMEGRP "OUT_TO_SSRAM" OFFSET = OUT 4.921 ns AFTER "clk_p" ;
##180 MHz ; -36 degrees phase internal clock
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 2.195 ns VALID 3.95 ns BEFORE "clk_p" RISING;
#TIMEGRP "OUT_TO_SSRAM" 
##200 MHz - phase 0
#TIMEGRP "OUT_TO_SSRAM" OFFSET = OUT 3.45 ns AFTER "clk_p" ;
#TIMEGRP "INPUT_FROM_SSRAM" OFFSET = IN 2.05 ns VALID 3.4 ns BEFORE "clk_p" RISING;
#NET "ck_int" TNM_NET = CLOCK_INTERNAL;
#TIMESPEC TS02 = FROM INPUT_FROM_SSRAM TO CLOCK_INTERNAL 2.2 ns;

####################################################################################
## SSRAM1 ##
####################################################################################
TIMEGRP "OUT_TO_SSRAM1" OFFSET = OUT 8.6 ns AFTER "CLK100in_P" ;
TIMEGRP "INPUT_FROM_SSRAM1" OFFSET = IN 7.2 ns VALID 8.4 ns BEFORE "CLK100in_P" ;
####### Feedback constraint is just for timing analysis
NET "CLK200in(0)" FEEDBACK = 830 ps NET "clk200out(0)" ;

net "sram1_*" slew = fast;

NET "sram1_AdvLd_n" LOC = Y7 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_bw1_n" LOC = Y8 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_bw2_n" LOC = V2 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_bw3_n" LOC = Y9 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_bw4_n" LOC = W1 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_cke_n" LOC = W3 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_lbo" 	LOC = W6 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_oe_n" 	LOC = W7 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_we_n" 	LOC = W9 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_zz" 	LOC = V1 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram1_cs1" 	LOC = W5 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
INST "sram1_we_n"  TNM = OUT_TO_SSRAM1;
INST "sram1_bw1_n" TNM = OUT_TO_SSRAM1;
INST "sram1_bw2_n" TNM = OUT_TO_SSRAM1;
INST "sram1_bw3_n" TNM = OUT_TO_SSRAM1;
INST "sram1_bw4_n" TNM = OUT_TO_SSRAM1;
                                                 
NET "sram1_a<0>"  LOC = AC5;  NET "sram1_a<1>"  LOC = AC6;  NET "sram1_a<2>"  LOC = AB1;
NET "sram1_a<3>"  LOC = AB2;  NET "sram1_a<4>"  LOC = AB3;  NET "sram1_a<5>"  LOC = AB4;
NET "sram1_a<6>"  LOC = AB6;  NET "sram1_a<7>"  LOC = AB7;  NET "sram1_a<8>"  LOC = AA1;
NET "sram1_a<9>"  LOC = AA3;  NET "sram1_a<10>" LOC = AA4;  NET "sram1_a<11>" LOC = AA5;
NET "sram1_a<12>" LOC = AA6;  NET "sram1_a<13>" LOC = AA7;  NET "sram1_a<14>" LOC = AA9;
NET "sram1_a<15>" LOC = Y1 ;  NET "sram1_a<16>" LOC = Y2 ;  NET "sram1_a<17>" LOC = Y3 ;
#NET "sram1_a<18>" LOC = Y4;  NET "sram1_a<19>" LOC = Y6;
NET "sram1_a<*>" IOSTANDARD = LVCMOS25 | DRIVE = 8;

NET "sram1_io<0>"  LOC = AK2; NET "sram1_io<1>"  LOC = AK3; NET "sram1_io<2>"  LOC = AK4;
NET "sram1_io<3>"  LOC = AK5; NET "sram1_io<4>"  LOC = AJ1; NET "sram1_io<5>"  LOC = AJ2;
NET "sram1_io<6>"  LOC = AJ4; NET "sram1_io<7>"  LOC = AH1; NET "sram1_io<8>"  LOC = AH2;
NET "sram1_io<9>"  LOC = AH3; NET "sram1_io<10>" LOC = AH4; NET "sram1_io<11>" LOC = AH5;
NET "sram1_io<12>" LOC = AG1; NET "sram1_io<13>" LOC = AG3; NET "sram1_io<14>" LOC = AG4;
NET "sram1_io<15>" LOC = AG5; NET "sram1_io<16>" LOC = AF1; NET "sram1_io<17>" LOC = AF2;
NET "sram1_io<18>" LOC = AF3; NET "sram1_io<19>" LOC = AF4; NET "sram1_io<20>" LOC = AF6;
NET "sram1_io<21>" LOC = AE1; NET "sram1_io<22>" LOC = AE3; NET "sram1_io<23>" LOC = AE4;
NET "sram1_io<24>" LOC = AE5; NET "sram1_io<25>" LOC = AE6; NET "sram1_io<26>" LOC = AE7;
NET "sram1_io<27>" LOC = AD1; NET "sram1_io<28>" LOC = AD2; NET "sram1_io<29>" LOC = AD3;
NET "sram1_io<30>" LOC = AD4; NET "sram1_io<31>" LOC = AD6; NET "sram1_io<32>" LOC = AD7;
NET "sram1_io<33>" LOC = AC1; NET "sram1_io<34>" LOC = AC3; NET "sram1_io<35>" LOC = AC4;
NET "sram1_io<*>" IOSTANDARD = LVCMOS25 | DRIVE = 8;

INST "sram1_io<*>" TNM = INPUT_FROM_SSRAM1;
INST "sram1_io<*>" TNM = OUT_TO_SSRAM1;

####################################################################################
## SSRAM2 ##
####################################################################################
TIMEGRP "OUT_TO_SSRAM2" OFFSET = OUT 8.6 ns AFTER "CLK100in_P" ;
TIMEGRP "INPUT_FROM_SSRAM2" OFFSET = IN 7.2 ns VALID 8.4 ns BEFORE "CLK100in_P" ;
#NET "ck_int" TNM_NET = CLOCK_INTERNAL;
#TIMESPEC TS02 = FROM INPUT_FROM_SSRAM2 TO CLOCK_INTERNAL 2.05 ns;
NET "CLK200in(1)" FEEDBACK = 360 ps NET "clk200out(1)" ;

net "sram2_*" slew = fast;

NET "sram2_AdvLd_n" LOC = D18 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_bw1_n" LOC = E18 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_bw2_n" LOC = F18 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_bw3_n" LOC = H17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_bw4_n" LOC = J17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_cke_n" LOC = G18 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_lbo"   LOC = C17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_oe_n"  LOC = D17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_we_n"  LOC = F17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_zz"    LOC = G17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
NET "sram2_cs1"   LOC = B17 | IOSTANDARD = LVCMOS25 | DRIVE = 8;
INST "sram2_we_n"  TNM = OUT_TO_SSRAM2;
INST "sram2_bw1_n" TNM = OUT_TO_SSRAM2;
INST "sram2_bw2_n" TNM = OUT_TO_SSRAM2;
INST "sram2_bw3_n" TNM = OUT_TO_SSRAM2;
INST "sram2_bw4_n" TNM = OUT_TO_SSRAM2;

NET "sram2_a<0>"  LOC = F21;  NET "sram2_a<1>"  LOC = G21;  NET "sram2_a<2>"  LOC = H21;
NET "sram2_a<3>"  LOC = J21;  NET "sram2_a<4>"  LOC = A20;  NET "sram2_a<5>"  LOC = C20;
NET "sram2_a<6>"  LOC = D20;  NET "sram2_a<7>"  LOC = E20;  NET "sram2_a<8>"  LOC = G20;
NET "sram2_a<9>"  LOC = J20;  NET "sram2_a<10>" LOC = A19;  NET "sram2_a<11>" LOC = B19;
NET "sram2_a<12>" LOC = C19;  NET "sram2_a<13>" LOC = D19;  NET "sram2_a<14>" LOC = F19;
NET "sram2_a<15>" LOC = G19;  NET "sram2_a<16>" LOC = H19;  NET "sram2_a<17>" LOC = J19;
#NET "sram2_a<18>" LOC = E16;  NET "sram2_a<19>" LOC = C18;
NET "sram2_a<*>" IOSTANDARD = LVCMOS25 | DRIVE = 8;

NET "sram2_io<0>"  LOC = B29; NET "sram2_io<1>"  LOC = A28; NET "sram2_io<2>"  LOC = A27;
NET "sram2_io<3>"  LOC = B27;	NET "sram2_io<4>"  LOC = F27; NET "sram2_io<5>"  LOC = A26;
NET "sram2_io<6>"  LOC = C26;	NET "sram2_io<7>"  LOC = D26; NET "sram2_io<8>"  LOC = E26;
NET "sram2_io<9>"  LOC = F26;	NET "sram2_io<10>" LOC = A25; NET "sram2_io<11>" LOC = B25;
NET "sram2_io<12>" LOC = C25;	NET "sram2_io<13>" LOC = D25; NET "sram2_io<14>" LOC = F25;
NET "sram2_io<15>" LOC = G25;	NET "sram2_io<16>" LOC = A24; NET "sram2_io<17>" LOC = C24;
NET "sram2_io<18>" LOC = D24;	NET "sram2_io<19>" LOC = E24; NET "sram2_io<20>" LOC = A23;
NET "sram2_io<21>" LOC = B23;	NET "sram2_io<22>" LOC = C23; NET "sram2_io<23>" LOC = D23;
NET "sram2_io<24>" LOC = F23;	NET "sram2_io<25>" LOC = G23; NET "sram2_io<26>" LOC = A22;
NET "sram2_io<27>" LOC = C22;	NET "sram2_io<28>" LOC = D22; NET "sram2_io<29>" LOC = E22;
NET "sram2_io<30>" LOC = F22;	NET "sram2_io<31>" LOC = G22; NET "sram2_io<32>" LOC = A21;
NET "sram2_io<33>" LOC = B21;	NET "sram2_io<34>" LOC = C21; NET "sram2_io<35>" LOC = D21;
NET "sram2_io<*>" IOSTANDARD = LVCMOS25 | DRIVE = 8;

INST "sram2_io<*>" TNM = INPUT_FROM_SSRAM2;
INST "sram2_io<*>" TNM = OUT_TO_SSRAM2;

#NET "sram1_*" OUT_TERM = UNTUNED_50;
#NET "sram2_*" OUT_TERM = UNTUNED_50;

NET "sd_vtt_s6" LOC = D8;

