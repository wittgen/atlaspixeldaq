IBLDAQ_DIR := $(shell echo $(CURDIR) | sed 's@IblDaq.*@IblDaq@')
include $(IBLDAQ_DIR)/xilconf_mb.mk

# use bash
SHELL := /bin/bash -O extglob

# save time for certain targets
TIME = /usr/bin/time --output=$@.time
#TIME = 

# multithreading settings for XPS synthesis, MAP, and PAR
# MAP currently (14.x) supports only off or 2; PAR off or 2, 3, 4; XPS yes/no
MAP_MT = 2
PAR_MT = 4
XPS_MT = yes

#SYNTHESIS_OPT = 
TRANSLATE_OPT =	-dd _ngo -nt timestamp
#MAP_OPT = -w -logic_opt on -ol high -xe n -t 2 -xt 0 -register_duplication off -r 4 -global_opt off -mt $(MAP_MT) -ir off -pr off -lc off -power off
#PAR_OPT = -ol high -xe n -mt $(PAR_MT)

# AK: options changed after discussion on setup and hold times
# synthesis options are in build/rodSlave_top.xst. we have changed resource sharing and register duplication to YES
# ISE run with good timing with these settings
SYNTHESIS_OPT = 
MAP_OPT = -w -logic_opt off -ol high -xe n -t 1 -xt 0 -register_duplication off -r 4 -global_opt off -mt $(MAP_MT) -ir off -pr off -lc off -power off
#PAR_OPT = -ol high -xe n -mt $(PAR_MT) 
PAR_OPT = -ol high -xe c -mt $(PAR_MT) 



TIMING_OPT = -v 3 -s $(SLAVE_FPGA_SPEED) -n 3 -fastpaths

# directory structure as seen from toplevel Makefile
CORES_DIR = cores
SYSTEM_DIR = system
SOURCE_DIR = src
BUILD_DIR = build
LOGS_DIR = logs
SOFTWARE_DIR = sw
SRC_DIR = src

# logfiles that are copied to $(LOGS_DIR)
LOGFILES = $(CORES_DIR)/coregen_all.log \
		$(BUILD_DIR)/rodPrm_top.syr \
		$(BUILD_DIR)/rodPrm_top.bld \
		$(BUILD_DIR)/rodPrm_top_map.mrp \
		$(BUILD_DIR)/rodPrm_top.par \
		$(BUILD_DIR)/rodPrm_top.pad \
		$(BUILD_DIR)/rodPrm_top.twr \
		$(BUILD_DIR)/rodPrm_top.bgn
	
# FPGA settings
SLAVE_FPGA_FAMILY = spartan6
SLAVE_FPGA_DEVICE = xc6slx45

# set non-default speedgrade for FPGA
ifdef SG
    SLAVE_FPGA_SPEED = $(SG)
else
    SLAVE_FPGA_SPEED = 2
endif

SLAVE_FPGA_PACKAGE = fgg484

# Toolchain settings
INTSTYLE = xflow


#########################
# DO NOT EDIT BELOW!!!  #
#########################

SLAVE_FPGA_PART = $(SLAVE_FPGA_DEVICE)-$(SLAVE_FPGA_SPEED)-$(SLAVE_FPGA_PACKAGE)

# processor architecture
ARCH = $(shell uname -p)

# check for 32/64-bit architecture
# and source settings.sh
ifeq ($(ARCH), x86_64)
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin64
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin64
else
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin
endif

# command line tools
XST = $(ISE_BIN_PATH)/xst
COREGEN = $(ISE_BIN_PATH)/coregen
NGDBUILD = $(ISE_BIN_PATH)/ngdbuild
MAP = $(ISE_BIN_PATH)/map
PAR = $(ISE_BIN_PATH)/par
BITGEN = $(ISE_BIN_PATH)/bitgen
IMPACT = $(ISE_BIN_PATH)/impact
PROMGEN = $(ISE_BIN_PATH)/promgen
PLATGEN = $(EDK_BIN_PATH)/platgen
XPS = $(EDK_BIN_PATH)/xps
DATA2MEM = $(ISE_BIN_PATH)/data2mem
TRCE = $(ISE_BIN_PATH)/trce
