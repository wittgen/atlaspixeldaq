#!/bin/bash
# purpose: generate gitIdPkg file with current git hash

#set destination file name. might be propagated from Makefile later on
export IDFILE=src/prmGitIdPkg.vhd

echo "library ieee;" > $IDFILE
echo "use IEEE.STD_LOGIC_1164.ALL;" >> $IDFILE
echo "package gitIdPack is" >> $IDFILE
echo "constant gitIdLength: integer := 20 * 8;" >> $IDFILE
echo "constant gitHash: std_logic_vector(gitIdLength - 1 downto 0) := " >> $IDFILE
echo "X\"`git rev-parse HEAD`\";" >> $IDFILE
echo "end gitIdPack;" >> $IDFILE

#if desired copy to console for check
#cat $IDFILE
unset IDFILE

