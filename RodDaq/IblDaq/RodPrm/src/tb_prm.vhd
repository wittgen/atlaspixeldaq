--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   17:27:22 12/07/2010
-- Design Name:   
-- Module Name:   C:/ibl/ise_projects/prm_obama/tb_prm.vhd
-- Project Name:  prm_obama
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rodPrm_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.numeric_std.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tb_prm IS
END tb_prm;
 
ARCHITECTURE behavior OF tb_prm IS 
 
 
COMPONENT rodPrm_top
	 port (
-- clock and reset ports
  ck40_p      : in  std_logic;	
  ck40_n      : in  std_logic;	  
  rst         : in  std_logic;
        
-- dsp I/O and Reset command ports
  mdsp_hd            : inout std_logic_vector(15 downto 0);
  mdsp_hrdy_n        : in    std_logic;  -- HRDY from the Master DSP
  mdsp_hhwil         : out   std_logic;  -- HHWIL to the Master DSP
  mdsp_hcntl0        : out   std_logic;  -- HCNTL0 to the Master DSP
  mdsp_hcntl1        : out   std_logic;  -- HCNTL1 to the Master DSP
  mdsp_hds1_n        : out   std_logic;  -- HDS1_N to the Master DSP
  mdsp_hcs_n         : out   std_logic;  -- HCS_N to the Master DSP
  mdsp_hrnw          : out   std_logic;  -- HRNW to the Master DSP
  rcf_hrdy_n         : in    std_logic;  -- HRDY from the RCF
  rcf_hcs_n          : out   std_logic;  -- HCS_N to the RCF
  
  mdsp_boot0         : out   std_logic;
  mdsp_boot1         : out   std_logic;
  mdsp_boot2         : out   std_logic;
  mdsp_boot3         : out   std_logic;
  mdsp_boot4         : out   std_logic;

  mdsp_rst_n         : out   std_logic;

-- RCF Reset command ports
  reset_cmds_in      : in    std_logic_vector(1 downto 0);  -- reset cmd from V5
  reset_cmds_out     : out   std_logic_vector(1 downto 0);  -- acknowledge and clear reset cmd to V5

-- vme interface ports
  vme_data           : inout std_logic_vector(31 downto 0);                
  vme_oen            : out   std_logic;
  vme_dir            : out   std_logic;
  vme_address        : in    std_logic_vector(31 downto 0);
  vme_am             : in    std_logic_vector( 5 downto 0);
  vme_as             : in    std_logic;
  vme_dsa            : in    std_logic;
  vme_dsb            : in    std_logic;
  vme_write          : in    std_logic;
  vme_iack           : in    std_logic;
  vme_iackin         : in    std_logic;
  vme_iackout        : out   std_logic;
  vme_sysfail        : in    std_logic;
  vme_dtack          : out   std_logic;
  vme_berr           : out   std_logic;
  vme_irq_n          : in    std_logic;
  vme_irq            : in    std_logic_vector(2 downto 0);
  board_address      : in    std_logic_vector(7 downto 0);
  vme_gap            : in    std_logic;  -- parity bit for Geographical Addr

-- ROD Status
  rod_busy_from_v5   : in  std_logic;
  rod_busy           : out std_logic;

-- fpga programming and reset control/handshaking ports
  reset_v5       		: out   std_logic
  );
END COMPONENT;
	 
constant period : Time := 25 ns; 
constant half_period : Time := 12.5 ns; 
constant initial_setup : integer := 2; 

procedure sleep (
 constant  waittime: in integer range 0 to 31 ) is
  begin 
  for i in 0 to waittime loop
   wait for 50 ns;
  end loop;
end sleep;

procedure issue_RESET  (
  signal rstb : out std_logic ) is 
  begin 
	RSTb <= transport '0' after (1*period), 
	        '1' after (5*period); 
   wait for 10*period; 
end issue_RESET;

procedure VME_D32_write (
  constant addr: in std_logic_vector (31 downto 0);
  constant data: in std_logic_vector (31 downto 0); 
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(31 downto 0);
  signal internal_data: out std_logic_vector (31 downto 0); 
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic ) is
	 begin 
	  AM <= "001001";
	  A  <= addr;
	  internal_data <= data;
	  write <= '0';
	  lword <= addr(0);
	  IACK <= '1';
	  sleep(2);
     wait for 2*period;
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep(5);
     wait for 5*period; 
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  internal_data <= (others => '0');
	  write <= '1';
	  sleep(2);
     wait for 2*period;
 end VME_D32_write;

procedure VME_D32_read (
  constant addr:  in std_logic_vector (31 downto 0);
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(31 downto 0);
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001001";
	  A <=addr;
	  write <= '1';
--	  lword <= '0';
     lword <= addr(0);
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep (5);
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  sleep (2);
 end VME_D32_read;

procedure VME_D16_write (
  constant addr: in std_logic_vector (7 downto 1);
  constant data: in std_logic_vector (15 downto 0);
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(7 downto 1);
  signal internal_data: out std_logic_vector (15 downto 0); 
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001101";
	  A <=addr;
	  internal_data <= data;
	  write <= '0';
	  lword <= '1';
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep (5);
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  internal_data <= "0000000000000000";
	  write <= '1';
	  sleep (2);
 end VME_D16_write;

procedure VME_D16_read (
  constant addr:  in std_logic_vector (7 downto 1);
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(7 downto 1);
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001101";
	  A <=addr;
	  write <= '1';
	  lword <= '1';
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep (5);
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  sleep (2);
 end VME_D16_read;

 procedure VME_D08_write (
  constant addr: in std_logic_vector (7 downto 1);
  constant data: in std_logic_vector (15 downto 0);
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(7 downto 1);
  signal internal_data: out std_logic_vector (15 downto 0); 
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001101";
	  A <=addr;
	  internal_data <= data;
	  write <= '0';
	  lword <= '1';
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep (5);
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  internal_data <= "0000000000000000";
	  write <= '1';
	  sleep (2);
 end VME_D08_write;

procedure VME_D08_read (
  constant addr: in std_logic_vector (7 downto 1);
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(7 downto 1);
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001101";
	  A <=addr;
	  write <= '1';
	  lword <= '1';
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0';
     sleep (5);
  	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
	  sleep (2);
 end VME_D08_read;

procedure VME_block_write (
  constant addr: in std_logic_vector (31 downto 0);
  constant data: in std_logic_vector (31 downto 0);
  constant n: in integer range 0 to 31; 
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(31 downto 0);
  signal internal_data: out std_logic_vector (31 downto 0); 
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
  variable count: std_logic_vector (7 downto 0);
	 begin 
	  AM <="001011"; --xB
	  A <=addr;
	  write <= '0';
	  lword <= addr(0);
	  IACK <= '1';
	  internal_data <= data;
	  count := data(7 downto 0);
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0'; 
	  for i in 0 to n loop
	    sleep (10); 
	    DSA <= '1';
	    dsb <= '1';
		 count := count + "00000001";
		 internal_data <= "000000000000000000000000"&count;
		 sleep (10);
		 DSA <= '0';
	    dsb <= '0';
	  end loop;
	  sleep (10);
	  internal_data <= (others => '0');
	  write <= '1';
	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
 end VME_block_write;

procedure VME_block_read (
  constant addr: in std_logic_vector (31 downto 0);
  constant n: in integer range 0 to 31; 
  signal am: out std_logic_vector (5 downto 0);
  signal A : out std_logic_vector(31 downto 0);
  signal write: out std_logic;
  signal lword: out std_logic;
  signal iack: out std_logic;
  signal as: out std_logic;
  signal dsa: out std_logic;
  signal dsb: out std_logic) is
	 begin 
	  AM <="001011"; --xB
	  A <=addr;
	  write <= '1';
	  lword <= addr(0);
	  IACK <= '1';
	  sleep (2);
	  AS <= '0';
	  DSA <= '0';
	  dsb <= '0'; 
	  for i in 0 to n loop
	    sleep (10); 
	    DSA <= '1';
	    dsb <= '1';
		 sleep (10);
		 DSA <= '0';
	    dsb <= '0';
	  end loop;
	  sleep (10);
	  AS <= '1';
	  DSA <= '1';
	  dsb <= '1';
 end VME_block_read;   

   --Inputs
   signal ck40_p: 				std_logic := '0';
	signal ck40_n: 				std_logic := '1';
   signal rst: 					std_logic := '0';
	signal mdsp_hd:				std_logic_vector(15 downto 0);
	signal mdsp_hrdy_n:			std_logic := '0';
   signal mdsp_hhwil:			std_logic;
	signal mdsp_hcntl0:			std_logic;
	signal mdsp_hcntl1:			std_logic;
	signal mdsp_hds1_n:			std_logic;
	signal mdsp_hcs_n:			std_logic;
	signal mdsp_hrnw:				std_logic;
	signal rcf_hrdy_n:			std_logic := '0';
	signal rcf_hcs_n:				std_logic;
	signal mdsp_boot0:			std_logic;
   signal mdsp_boot1:			std_logic;
	signal mdsp_boot2:			std_logic;
	signal mdsp_boot3:			std_logic;
	signal mdsp_boot4:			std_logic;
	signal mdsp_rst_n:			std_logic;
	signal reset_cmds_in:		std_logic_vector(1 downto 0) := (others => '0');
	signal reset_cmds_out:		std_logic_vector(1 downto 0);
	signal vme_data:				std_logic_vector(31 downto 0);
	signal vme_oen:				std_logic;
	signal vme_dir:				std_logic;
	signal vme_address:			std_logic_vector(31 downto 0) := (others => '0');
	signal vme_am:					std_logic_vector(5 downto 0) := (others => '0');
	signal vme_as: 				std_logic := '1';
	signal vme_dsa:				std_logic := '1';
   signal vme_dsb:				std_logic := '1';
   signal vme_write:				std_logic := '0';
   signal vme_iack:				std_logic := '0';
   signal vme_iackin:			std_logic := '1';
	signal vme_iackout:			std_logic;
	signal vme_sysfail:			std_logic := '1';
   signal vme_dtack:				std_logic;
	signal vme_berr:				std_logic;
	signal vme_irq_n:				std_logic := '1';
	signal vme_irq:				std_logic_vector(2 downto 0) := (others => '0');
   signal board_address: 		std_logic_vector(7 downto 0) := "11001010"; --0xCA
   signal vme_gap: 				std_logic := '1';
	signal rod_busy_from_v5:	std_logic := '0';
	signal rod_busy:				std_logic;
	signal reset_v5:				std_logic;
	
	signal internal_data:		std_logic_vector(31 downto 0);
	signal lvme_as:				std_logic;	
	signal lvme_ds0:				std_logic;
	signal lvme_ds1:				std_logic;
	signal lvme_write:			std_logic;
	signal lvme_iack:				std_logic;
 
BEGIN
 
   uut: rodPrm_top 
	PORT MAP 
			(
		  ck40_p     				=>		  ck40_p,
		  ck40_n     	         =>      ck40_n,     	  
		  rst                   =>      rst,        
		  mdsp_hd               =>      mdsp_hd,            
		  mdsp_hrdy_n           =>      mdsp_hrdy_n,        
		  mdsp_hhwil            =>      mdsp_hhwil,         
		  mdsp_hcntl0           =>      mdsp_hcntl0,        
		  mdsp_hcntl1           =>      mdsp_hcntl1,        
		  mdsp_hds1_n           =>      mdsp_hds1_n,        
		  mdsp_hcs_n            =>      mdsp_hcs_n,         
		  mdsp_hrnw             =>      mdsp_hrnw,          
		  rcf_hrdy_n            =>      rcf_hrdy_n,         
		  rcf_hcs_n             =>      rcf_hcs_n,          
		  mdsp_boot0            =>      mdsp_boot0,         
		  mdsp_boot1            =>      mdsp_boot1,         
		  mdsp_boot2            =>      mdsp_boot2,         
		  mdsp_boot3            =>      mdsp_boot3,         
		  mdsp_boot4            =>      mdsp_boot4,         
		  mdsp_rst_n            =>      mdsp_rst_n,        
		  reset_cmds_in         =>      reset_cmds_in,      
		  reset_cmds_out        =>      reset_cmds_out,     
		  vme_data              =>      vme_data,           
		  vme_oen               =>      vme_oen,            
		  vme_dir               =>      vme_dir,            
		  vme_address           =>      vme_address,        
		  vme_am                =>      vme_am,             
		  vme_as                =>      lvme_as,             
		  vme_dsa               =>      lvme_ds0,            
		  vme_dsb               =>      lvme_ds1,            
		  vme_write             =>      lvme_write,          
		  vme_iack              =>      lvme_iack,           
		  vme_iackin            =>      vme_iackin,         
		  vme_iackout           =>      vme_iackout,        
		  vme_sysfail           =>      vme_sysfail,        
		  vme_dtack             =>      vme_dtack,          
		  vme_berr              =>      vme_berr,           
		  vme_irq_n             =>      vme_irq_n,          
		  vme_irq               =>      vme_irq,            
		  board_address         =>      board_address,      
		  vme_gap               =>      vme_gap,           
		  rod_busy_from_v5      =>      rod_busy_from_v5,   
		  rod_busy              =>      rod_busy,           
		  reset_v5       		   =>      reset_v5       		
  );
		  
time_ck: process 
   begin 
	 wait for 6.25 ns;	-- 80 MHz
	 ck40_p <= not(ck40_p);
	 ck40_n <= not(ck40_n);
end process time_ck;

process(vme_write, internal_data) 
  begin 
   if(vme_write = '0') then 
		   vme_data <= internal_data; 
	else 
			vme_data <= (others => 'Z');
	end if;
end process; 

lvme_as		<= NOT(vme_as);
lvme_ds0		<= NOT(vme_dsa);
lvme_ds1		<= NOT(vme_dsb);
lvme_write	<= NOT(vme_write);
lvme_iack	<= NOT(vme_iack);

vme_process: process  
   begin 
		issue_RESET(rst);
		sleep(10);
--		VME write/read of PRM vme_timeout_i register
--		VME_D32_write(b"00010101110000000000000000011000", x"12345555", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"12345556", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"12345557", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"12345558", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"12345559", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"1234555A", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"1234555B", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_write(b"00010101110000000000000000011000", x"1234555C", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);
--		VME_D32_read(b"00010101110000000000000000011000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
--		Sleep(10);

--		VME_block_write(b"00010101110000000000000000011000", x"1234ADBB", 31, vme_am,vme_address, internal_data, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);	
--
--		Sleep(20);
--		VME_block_read(b"00010101110000000000000000011000", 31, vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);	

--		VME write/read of MDSP HPI port (HPIC port)
--		00010101110000000000000000011000
		VME_D32_write(b"00010101110000000000000001001000", x"12345555", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		VME_D32_read(b"00010101110000000000000001001000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		VME_D32_write(b"00010101110000000000000001001000", x"12345555", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		VME_D32_read(b"00010101110000000000000001001000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		VME_D32_write(b"00010101110000000000000001001000", x"12345555", vme_am, vme_address, internal_data, vme_write, vme_address(0),vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		VME_D32_read(b"00010101110000000000000001001000", vme_am, vme_address, vme_write, vme_address(0), vme_iack, vme_as, vme_dsa, vme_dsb);
		Sleep(10);
		
		Sleep(30);
		Sleep(30);
		Sleep(30);
		Sleep(30);
		Sleep(30);
end process;

END;
