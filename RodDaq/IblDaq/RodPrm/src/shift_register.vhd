-- Lawrence Berkeley National Laboratory
--------------------------------------------------------------------------------
-- Filename: shift_register.vhd
-- Description:	This is an 8-bit parallel-in, serial-out shift register. 
--		Parallel data enters into the shift register and is shifted out 
--		each clock cycle. It controls the loading, shifting, outputs and
--		reset.	 
--
--------------------------------------------------------------------------------
-- Author: Linda Stromberg
--         John Joseph
--
-- Revision History : Aug 21, 2000 - jmj changed code to shift out LSB first
--                                       dropped en_output_in signal
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;          --needed for logical operations
use IEEE.std_logic_arith.all;         --needed for +/- operations

--------------------------------------------------------------------------------
--PORT DECLARATION
--------------------------------------------------------------------------------

entity shift_register is 
port(
    clk_in       : in  std_logic;  -- cclk input
    rst_n_in     : in  std_logic;
    en_load_in   : in  std_logic;  -- enable loading
    en_shift_in  : in  std_logic;  -- enable shifting
    data_in      : in  std_logic_vector(7 downto 0); -- 8-bit word in
    data_out     : out std_logic   -- single bit out	
    );
end shift_register;

architecture rtl of shift_register is

--------------------------------------------------------------------------------
--SIGNAL DECLARATION
--------------------------------------------------------------------------------

signal Q0 : std_logic;	--output from the first D-latch
signal Q1 : std_logic; 	--output from the second D-latch
signal Q2 : std_logic;	--...
signal Q3 : std_logic;
signal Q4 : std_logic;
signal Q5 : std_logic;
signal Q6 : std_logic;
signal Q7 : std_logic;

begin
--------------------------------------------------------------------------------
--PROCESS DECLARATION
--------------------------------------------------------------------------------

shift_register : process (rst_n_in,
                          clk_in,
                          data_in,
                          en_load_in,	
                          en_shift_in,
                          Q0,Q1,Q3,Q4,Q5,Q6,Q7
                          )			       
begin 
    if (rst_n_in = '0') then --if reset true	
        Q0 <= '0';	     --set all internal signals to zero 
        Q1 <= '0';
        Q2 <= '0';
        Q3 <= '0';
        Q4 <= '0';
        Q5 <= '0';
        Q6 <= '0';
        Q7 <= '0';
        data_out <= '0';     --set output to zero
    elsif (clk_in'event and clk_in = '1') then
        if (en_load_in = '1' AND en_shift_in = '0') then  -- load shift register
            Q0 <= data_in(0);  --  load data in LSB out first order
            Q1 <= data_in(1);
            Q2 <= data_in(2);
            Q3 <= data_in(3);
            Q4 <= data_in(4);
            Q5 <= data_in(5);
            Q6 <= data_in(6);
            Q7 <= data_in(7);
        elsif (en_load_in = '0' AND en_shift_in = '1') then  -- enable shift register
            Q0 <= Q1;  --  shift data through the register
            Q1 <= Q2;
            Q2 <= Q3;
            Q3 <= Q4;
            Q4 <= Q5;
            Q5 <= Q6;
            Q6 <= Q7;
        else
            null;
        end if;
        data_out <= Q0;  -- data out bit
    end if;
end process shift_register;
end rtl;

