----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:24:22 05/07/2012 
-- Design Name: 
-- Module Name:    ppc_hpi_controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.std_logic_arith.all; -- needed for +/- operations


-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ppc_hpi_controller is
 port (
    clk_in            : in  std_logic; -- vme_clk66 input
    rst_n_in          : in  std_logic; -- asynchronous global reset 
    ppc_cen_in       : in  std_logic; -- PPC CE_N signal in
	 mdsp_hcntl0_in	: in		std_logic;
    mdsp_hcntl1_in	: in			std_logic;
    ppc_ds_in        : in  std_logic; -- PPC Data Strobe (VME BE)
	 ppc_hpi_busy_in  : in  std_logic;
	 ppc_hpi_done_in  : in  std_logic;
--    mdsp_hrdy_n_in    : in  std_logic; -- Master DSP HRDY_N signal
    ppc_hpi_data_in  : in  std_logic_vector(31 downto 0); --
    ppc_hpi_data_out : out std_logic_vector(31 downto 0); --
--    mdsp_cen_n_out    : out std_logic; -- MDSP CE_N signal out
--    mdsp_rnw_out      : out std_logic; -- MDSP RNW signal out
--    mdsp_hhwil_out    : out std_logic; -- MDSP HHWIL signal
--    mdsp_hds1_n_out   : out std_logic; -- MDSP HHDS1 signal
    vme_bus_rnw_in    : in  std_logic; -- VMEBus read/not_write signal
    ppc_vme_data_in  : in  std_logic_vector(31 downto 0); --
    ppc_vme_data_out : out std_logic_vector(31 downto 0); --
    ppc_dtack_n_out   : out std_logic; -- VMEBus LACK_N 
    ppc_berr_out      : out std_logic; -- VMEBus BERR
--    rcf_cen_in        : in  std_logic;
--    rcf_hrdy_n_in     : in  std_logic;
--    rcf_cen_n_out     : out std_logic;
    vme_timeout_in    : in  std_logic_vector(31 downto 0);
--	 hpi_state_debug   : out std_logic_vector(4 downto 0)
		hpi_ppc_command_out : out std_logic_vector(3 downto 0);
		hpi_strobe_out      : out std_logic
    );
end ppc_hpi_controller;

architecture Behavioral of ppc_hpi_controller is


--------------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------------

type hpi_access is(
  idle, --ok
  setup_hpi_wr_cycle, --ok
  start_wr_data, --ok
  wr_hpi_data, --ok
--  setup_hpi_shw_wr_cycle,
--  setup_shw_data,
--  start_wr_hpi_data_shw,
--  wr_hpi_data_shw,
  setup_hpi_rd_cycle, --ok
  start_rd_data, --ok
--  rd_hpi_data_fhw,
 -- wait_for_fhw_hrdy,
  latch_hpi, --ok
 -- setup_hpi_shw_rd_cycle,
 -- rd_hpi_data_shw,
 -- wait_for_shw_hrdy,
 -- latch_hpi_shw,
  wait_for_hpi_ready,--ok
  send_hpi_dtack, --ok
  done --ok
  );

signal hpi_access_state  : hpi_access;
signal hpi_shw_wr_data_i : std_logic_vector(15 downto 0);
signal hrdy_n_i          : std_logic;
signal rcf_hrdy_i        : std_logic;
signal ppc_ds_i         : std_logic;
signal ppc_ds_ii        : std_logic;
signal hpi_strobe_i     : std_logic;
signal	 ppc_hpi_busy_i  :   std_logic;
signal	 ppc_hpi_done_i  :  std_logic;


signal hpi_state			 : std_logic_vector(4 downto 0);

signal encoded_cmd_i : std_logic_vector(3 downto 0);
signal decoded_cmd_i : std_logic_vector(2 downto 0);

--------------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------------
begin

--hpi_state_debug	<= hpi_state;
hpi_strobe_out <= hpi_strobe_i;

--------------------------------------------------------------------------------
-- Process to register all clk40 signals using the VME Clock
--------------------------------------------------------------------------------
signal_register_process: process(clk_in, rst_n_in)
begin
  if(rst_n_in = '0') then
    ppc_hpi_busy_i   <= '0';
    ppc_hpi_done_i <= '0';
  elsif(rising_edge(clk_in)) then
    ppc_hpi_busy_i   <= ppc_hpi_busy_in;
    ppc_hpi_done_i <= ppc_hpi_done_in;
  end if;
end process signal_register_process;

--------------------------------------------------------------------------------
-- Process to generate the VME read/write strobe to the ROD, and the DTACK 
-- signal to the VME Controller.
--------------------------------------------------------------------------------
vme_to_hpi_data_process: process(clk_in, rst_n_in)
--variable wait_count    : unsigned( 1 downto 0);
variable timeout_count : unsigned(31 downto 0);
begin
  if(rst_n_in = '0') then
    ppc_ds_i         <= '0';
    ppc_ds_ii        <= '0';
    hpi_strobe_i    <= '0';
--    mdsp_hds1_n_out   <= '1';
--    mdsp_hhwil_out    <= '0';
--    mdsp_rnw_out      <= '0';
    ppc_dtack_n_out   <= '1';
    ppc_berr_out      <= '0';
	 hpi_ppc_command_out <= (others => '0');
	 ppc_hpi_data_out <= (others => '0');
--    rcf_cen_n_out     <= '1';
--    mdsp_hpi_data_out <= (others => '0');
    ppc_vme_data_out <= (others => '0');
--    wait_count        := (others => '0');
    timeout_count     := (others => '0');
--    hpi_access_state  <= idle;
--	 hpi_state			 <= "00000";
  elsif(rising_edge(clk_in)) then
    ppc_ds_i  <= ppc_ds_in;
    ppc_ds_ii <= ppc_ds_i;
    if(ppc_cen_in = '1') then
      case hpi_access_state is
        when idle =>
          hpi_strobe_i  <= '0';
--          mdsp_hds1_n_out <= '1';
--          mdsp_hhwil_out  <= '0';
          ppc_dtack_n_out <= '1';
--          wait_count      := (others => '0');
			 hpi_ppc_command_out <= (others => '0');
			 ppc_hpi_data_out <= (others => '0');

--          mdsp_rnw_out    <= vme_bus_rnw_in;
          if(ppc_ds_i = '1') then
            if(vme_bus_rnw_in = '1') then
              hpi_access_state <= setup_hpi_rd_cycle;
--              mdsp_rnw_out     <= '1';
              hpi_strobe_i   <= '0';
            elsif (vme_bus_rnw_in = '0') then
              hpi_access_state <= setup_hpi_wr_cycle;
              hpi_strobe_i     <= '0';
            end if;
          end if;
--			 hpi_state			 <= "00000";
--          
----               ********** Write Algorithm States **********
        when setup_hpi_wr_cycle =>
          hpi_strobe_i    <= '0';
          ppc_hpi_data_out <= ppc_vme_data_in;
			 hpi_ppc_command_out <= encoded_cmd_i;
--          hpi_shw_wr_data_i <= mdsp_vme_data_in(31 downto 16);
          hpi_access_state  <= start_wr_data;
--			 hpi_state			 <= "00001";
--
        when start_wr_data => 
          if ppc_hpi_busy_i = '0' then
				hpi_strobe_i    <= '1';
--          if(wait_count = 1) then
					hpi_access_state <= wr_hpi_data;
			end if;
--            wait_count       := (others => '0');
--          else
--            wait_count := wait_count + 1;
--          end if;
--			 hpi_state			 <= "00010";
--        
        when wr_hpi_data =>
          hpi_strobe_i    <= '1';
--          if(ppc_hpi_done_i = '1') then
             hpi_access_state <= wait_for_hpi_ready;
--          end if;
  
--
--        when setup_hpi_shw_wr_cycle =>
--          mdsp_hhwil_out    <= '1';
--          mdsp_hpi_data_out <= hpi_shw_wr_data_i;
--          hpi_access_state  <= start_wr_hpi_data_shw;
--			 hpi_state			 <= "00100";
--
--        when start_wr_hpi_data_shw =>
--          mdsp_hds1_n_out   <= '0';
--          if(wait_count = 1) then
--            hpi_access_state <= setup_shw_data;
--            wait_count       := (others => '0');
--          else
--            wait_count := wait_count + 1;
--          end if;
--			 hpi_state			 <= "00101";
--
--        when setup_shw_data => 
--          mdsp_hds1_n_out  <= '1';
--          hpi_access_state <= wr_hpi_data_shw;
--			 hpi_state			 <= "00110";
--        
--        when wr_hpi_data_shw =>
--          if(wait_count = 1) then
--            hpi_access_state <= wait_for_hpi_ready;
--            wait_count       := (others => '0');
--          else
--            wait_count := wait_count + 1;
--          end if;
--			 hpi_state			 <= "00111";
--          
----               ********** Read Algorithm States **********
        when setup_hpi_rd_cycle =>
         hpi_strobe_i    <= '0';
			 hpi_ppc_command_out <= encoded_cmd_i;          
          hpi_access_state <= start_rd_data;
--			 hpi_state			 <= "01000";
--
        when start_rd_data =>
         if ppc_hpi_busy_i = '0' then
				hpi_strobe_i    <= '1';
--          if(wait_count = 1) then
					hpi_access_state <= latch_hpi;
			end if;--          hpi_access_state <= wait_for_fhw_hrdy;
--			 hpi_state			 <= "01001";
--          
--        when wait_for_fhw_hrdy =>
--          hpi_access_state <= latch_hpi_fhw;
--			 hpi_state			 <= "01010";
--
        when latch_hpi =>
		  	 hpi_strobe_i    <= '1';
 ---         if (ppc_hpi_busy_i = '0' and ppc_hpi_done_i = '1' ) then
             if (ppc_hpi_done_i = '1' ) then
            ppc_vme_data_out <= ppc_hpi_data_in;
  
            hpi_access_state <= wait_for_hpi_ready;
          end if;
--			 hpi_state			 <= "01011";

--        when setup_hpi_shw_rd_cycle =>
--          mdsp_hhwil_out   <= '1';
--          hpi_access_state <= rd_hpi_data_shw;
--			 hpi_state			 <= "01100";
--
--        when rd_hpi_data_shw =>
--          mdsp_hds1_n_out  <= '0';
--          hpi_access_state <= wait_for_shw_hrdy;
--			 hpi_state			 <= "01101";
--          
--        when wait_for_shw_hrdy =>
--          hpi_access_state <= latch_hpi_shw;
--			 hpi_state			 <= "01110";
--
--        when latch_hpi_shw =>
--          mdsp_hds1_n_out  <= '1';
--          mdsp_vme_data_out(31 downto 16) <= mdsp_hpi_data_in;
--          hpi_access_state <= wait_for_hpi_ready;
--			 hpi_state			 <= "01111";
--
----               ********** Common States **********
        when wait_for_hpi_ready =>
          if (ppc_hpi_busy_i = '0') then
			 				hpi_strobe_i   <= '0';
 
            hpi_access_state <= send_hpi_dtack;
          end if;
--			 hpi_state			 <= "10000";
--
        when send_hpi_dtack =>
		  				hpi_strobe_i   <= '0';
 
          if (ppc_hpi_busy_i = '0') then  -- test for busy low again
            ppc_dtack_n_out  <= '0';
           hpi_access_state <= done;
          end if;
--			 hpi_state			 <= "10001";
--
        when done =>
          hpi_strobe_i   <= '0';
          if (ppc_ds_i = '0' AND ppc_ds_ii = '0') then
            ppc_dtack_n_out  <= '1';
            hpi_access_state <= idle;
          end if;   
--			 hpi_state			 <= "10010";			 
--
        when others =>
--          mdsp_cen_n_out  <= '1';
--          mdsp_hds1_n_out <= '1';
--          mdsp_hhwil_out  <= '0';
				hpi_strobe_i   <= '0';
          ppc_dtack_n_out <= '1';
          hpi_access_state <= idle;
--			 hpi_state			 <= "11111";
      end case;
--		
--    elsif(rcf_cen_in = '1') then
--      if(rcf_cen_in = '1' AND mdsp_ds_in = '1') then
--        rcf_cen_n_out     <= '0';
--        hpi_dtack_n_out <= NOT mdsp_ds_in;
--      else
--        hpi_dtack_n_out <= rcf_hrdy_i;
--      end if;
    else  -- All other regions
--      mdsp_cen_n_out  <= '1';
--      mdsp_hds1_n_out <= '1';
--      mdsp_hhwil_out  <= '0';
      ppc_dtack_n_out <= '1';
    end if;
--
    if(ppc_ds_in = '1') then
      if(timeout_count = unsigned(vme_timeout_in)) then
        ppc_berr_out  <= '1';
        hpi_access_state <= done;
      else
        timeout_count := timeout_count + 1 ;
      end if;
    else
      ppc_berr_out  <= '0';
      timeout_count := (others => '0');
    end if;
  end if;
end process vme_to_hpi_data_process;

--RT
decoded_cmd_i(2) <= mdsp_hcntl1_in;
decoded_cmd_i(1) <= mdsp_hcntl0_in;
decoded_cmd_i(0) <= vme_bus_rnw_in;

encode_command_out: process(decoded_cmd_i)
begin
	case decoded_cmd_i is
		when "000" => encoded_cmd_i <= "0001"; --write HPIC
		when "001" => encoded_cmd_i <= "0010"; --read HPIC
		when "010" => encoded_cmd_i <= "0011"; --write HPIA
		when "011" => encoded_cmd_i <= "0100"; --read HPIA
		when "100" => encoded_cmd_i <= "0111"; --write HPID autoinc
		when "101" => encoded_cmd_i <= "1000"; --read HPID autoinc
		when "110" => encoded_cmd_i <= "0101"; --write HPID fixed
		when "111" => encoded_cmd_i <= "0110"; --read HPID fixed
		when others => encoded_cmd_i <= "0000";
	end case;
end process encode_command_out;

end Behavioral;

