--------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- ReadOutDriver Electronics
--  
--------------------------------------------------------------------------
-- Filename: 2048x2dpramblock.vhd
-- Description:
--  FifoBuffer for serial link data stream decoder. 
--  Current implementation is XILINX specific and uses built in 
--  block rams for fifo memories. 
--
--  This block is completely Xilinx specific code,
--  and comes directly from the "xapp130.pdf" file available from 
--  the Xilinx appnote web page.
--
--  As it turns out, the instructions/code in that xapp is not correct!
--  The below code is in fact (after much consulation from the help line)
--  different from the xapp and compiles/synthesizes without error/warning. 
--
--  Although there is copious documentation there, it is not all very 
--  clear. Specifically, there are no timing diagrams or examples of
--  how the memories need to be accessed.
--  
--------------------------------------------------------------------------
-- Structure:
--------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Latch inputs on positive edge of clk40
--      2.  Perform all logic operations on positive edge of clk40
--      3.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------
-- Author:         Mark L. Nagel
-- Board Engineer: 
-- History:
--    08/09/99 MLN First version
--    Monday 13 March 2000 MLN updated comments and adapted 
--                             to 2bit wide memory block.
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
--------------------------------------------------------------------------
--library UNISIM;
--use unisim.all;
--------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------

entity dpram_1024x4 is
  port(
    clk          : in  std_logic; -- clk40 input
    rst          : in  std_logic; -- asynchronous global reset
    dprmA_write  : in  std_logic; --
    dprmB_write  : in  std_logic; --
    dprmA_read   : in  std_logic; --
    dprmB_read   : in  std_logic; --
    dprmA_addr   : in  std_logic_vector(9 downto  0); -- 
    dprmB_addr   : in  std_logic_vector(9 downto  0); --
    dprmA_wr_bus : in  std_logic_vector(3 downto  0);
    dprmB_wr_bus : in  std_logic_vector(3 downto  0);
    dprmA_rd_bus : out std_logic_vector(3 downto  0);
    dprmB_rd_bus : out std_logic_vector(3 downto  0)
    );
end dpram_1024x4;

--------------------------------------------------------------------------
-- XILINX PARTS SPECIFIC COMPONENTS/SIGNALS
--------------------------------------------------------------------------

--------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------
architecture behave of dpram_1024x4 is
--------------------------------------------------------------------------
-- COMPONENT DECLARATION
--------------------------------------------------------------------------

component RAMB4_S4_S4
  port(
    DOA   : out STD_LOGIC_VECTOR (3 downto 0);
    DOB   : out STD_LOGIC_VECTOR (3 downto 0);
    ADDRA : in  STD_LOGIC_VECTOR (9 downto 0);
    DIA   : in  STD_LOGIC_VECTOR (3 downto 0);
    ENA   : in  STD_ULOGIC;
    CLKA  : in  STD_ULOGIC;
    WEA   : in  STD_ULOGIC;                
    RSTA  : in  STD_ULOGIC;
    ADDRB : in  STD_LOGIC_VECTOR (9 downto 0);
    DIB   : in  STD_LOGIC_VECTOR (3 downto 0);
    ENB   : in  STD_ULOGIC;
    CLKB  : in  STD_ULOGIC;
    WEB   : in  STD_ULOGIC;
    RSTB  : in  STD_ULOGIC
    ); 
end component;


attribute INIT_00: string;
attribute INIT_01: string;
attribute INIT_02: string;
attribute INIT_03: string;
attribute INIT_04: string;
attribute INIT_05: string;
attribute INIT_06: string;
attribute INIT_07: string;
attribute INIT_08: string;
attribute INIT_09: string;
attribute INIT_0A: string;
attribute INIT_0B: string;
attribute INIT_0C: string;
attribute INIT_0D: string;
attribute INIT_0E: string;
attribute INIT_0F: string;


attribute INIT_00 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_01 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_02 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_03 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_04 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_05 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_06 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_07 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_08 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_09 of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0A of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0B of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0C of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0D of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0E of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";
attribute INIT_0F of MB: label is
"0000000000000000000000000000000000000000000000000000000000000000";


--------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------
begin

MB : RAMB4_S4_S4
  port map(
    DOA   => dprmA_rd_bus,
    DOB   => dprmB_rd_bus,
    ADDRA => dprmA_addr,
    DIA   => dprmA_wr_bus,
    ENA   => dprmA_read,
    CLKA  => clk,
    WEA   => dprmA_write,
    RSTA  => rst,
    ADDRB => dprmB_addr,
    DIB   => dprmB_wr_bus,
    ENB   => dprmB_read,
    CLKB  => clk,
    WEB   => dprmB_write,
    RSTB  => rst
    ); 

end behave;