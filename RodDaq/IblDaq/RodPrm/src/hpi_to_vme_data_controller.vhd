--------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 1999
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------------
-- Filename: hpi_to_vme_data_controller.vhd
-- Description:
--  This block handles the VME to DSP HPI access and generates the LACK_N signal
--  required by the CY7C960 VMEBus interface chipset, and the Read/Write Strobe
--  required by the Master DSP (TMS320C6201).
--
--------------------------------------------------------------------------------
-- Documentation:
--
--                          ROD Memory Map to Region Map
--
-- REGION(2:0) = "000"
-- rod_master_dsp_host_port - CS_N(1) 
--  ba0xxxxx  hpi control register
--  ba2xxxxx  hpi address register
--
-- REGION(2:0) = "001"
-- rod_master_dsp_host_port - CS_N(1) 
--  ba4xxxxx  hpi data register with auto_incr
--  ba6xxxxx  hpi data register without auto_incr
--
-- REGION(2:0) = "010"
-- fpga_program_reset_manager - CS_N(2)
--  bacxxxxx  prm  registers

-- REGION(2:0) = "011" - NOP
-- REGION(2:0) = "100" - NOP
-- REGION(2:0) = "101" - NOP
-- REGION(2:0) = "110" - NOP
-- REGION(2:0) = "111" - NOP
--
--------------------------------------------------------------------------------
--  hostPortToVmeD32 Algorithm
--
-- The communication from the Host VME interface is specified as D32.  The 
-- ROD Master DSP host port is a D16 component.  To access the DSP as per the 
-- ROD requirements, an interface circuit shall be implemented in the FPGA PRM
-- to allow D32 block transfers to and from the host.
--
-------------------------------------------------------------------------------
--  Simplified Block Diagram
--
--                    +--------+                +---------------------+
--                    |        |                |      FPGA PRM       |
--                    |A(31:16)|----            |                     |     
--                    |        |            ----|A(31:2)       DTACK_N|--------
--                    |DIR(Vcc)|            ----|D(31:0)              |
--                    |EN(GND) |                |            MDSP_HPID|--------
--                    +--------+            ----|DBE_N(3:0)           |
--                                          ----|CS_N(2)        HCTRL1|--------
--                    +--------+            ----|RNW            HCTRL0|--------
--                    |        |                |                HHWIL|--------
--                    |D(31:16)|----        +---|HRDY_N       STROBE_N|-----+
--                    |        |            |   |           TRANS_EN_N|---- |
--     DENIN1 --------|DIR     |            |   +---------------------+     |
--                +---|EN      |            +----------------------------+  |
--                |   +--------+                +---------------------+  |  |
--                |                             |    DSP HOST PORT    |  |  |
--                |   +--------+                |                     |  |  |
--                |   |        |            ----|D(15:0)              |  |  |
--                |   |A(15:2) |----            |                     |  |  |
--                |   |        |            ----|HCTRL1 (A22)         |  |  |
--                |   |DIR(Vcc)|            ----|HCTRL0 (A21)         |  |  |
--                +   |EN(Add) |            ----|HHWIL                |  |  |
--                |   +--------+                |                     |  |  |
--                |                         ----|CS_N(1)              |  |  |
--                |   +--------+            ----|RNW                  |  |  |
--                |   |        |            +---|STROBE_N             |  |  |
--                |   |D(15:0) |----        |   |               HRDY_N|--+  |
--                |   |        |            |   |                     |     |
--     DENIN0 ----|---|DIR     |            |   +---------------------+     |
-- TRANS_EN_N ----+---|EN      |            |                               |
--                    +--------+            +-------------------------------+
--                                                                          
--
-------------------------------------------------------------------------------
--
-- Write Algorithm
-- If RNW=0, and A(24:20)=DSP access, then DTACK_N will be set to a value of 1
-- to hold off completion of the VME cycle, D(31:0) will be latched into 
-- registers in the PRM when DBE_N(3:0) transitions to 0.  A1 will be asserted
-- low and D(15:0) will be placed on the HPI data bus, STROBE_N will transition
-- to active, and stay low until HRDY_N transitions from high to low.  D(31:16)
-- will be placed on the HPI data bus (D(15:0)), A1 will be asserted high, and
-- STROBE_N will transition to active to write the second half-word into the
-- DSP.  The state machine will clear STROBE_N, and will wait for HRDY_N to 
-- return low before clearing DTACK_N.
-- 
-- Read Algorithm
-- If RNW=1, and A(24:20)=DSP access, then DTACK_N will be set to a value of 1
-- to hold off completion of the VME cycle.  A1 will be asserted low, then
-- STROBE_N will transition from high to low.  When HRDY_N transitions from
-- high to low, the data on the HPI data bus will be latched into the lower
-- half word register in the PRM on the rising edge of STROBE_N.  STROBE_N will
-- be  asserted low again, and the second half-word will be latched into the 
-- PRM data register that maps to D(31:16) on the rising edge of STROBE_N.
-- After HRDY_N transitions from high to low, D(31:0) will be placed on the 
-- LVME data bus and DTACK_N will be asserted low to complete the VME cycle.
--
-------------------------------------------------------------------------------
--
-- HPI Read Timing
--                           ________                ________
--  HCTRL(1:0) XXXXXXXXXXXXXX________XXXXXXXXXXXXXXXX________XXXXXXXXXXXXXXXXXX
--                           ________                ________
--  RNW        XXXXXXXXXXXXXX        XXXXXXXXXXXXXXXX        XXXXXXXXXXXXXXXXXX
--                                                   ________
--  HHWIL      XXXXXXXXXXXXXX________XXXXXXXXXXXXXXXX        XXXXXXXXXXXXXXXXXX
--             ___________                                             ________
--  HCS_N                 \___________________________________________/
--             __________________          _____________          _____________
--  STROBE_N                     \________/             \________/
--                                      _____                 _____
--  HD(15:0)   ----------------------XXX_____XX------------XXX_____XXX---------
--                           ____________                         ___
--  HRDY_N     _____________/            \_______________________/   \_________
--
--
-- Local VME Read Cycle
--                       _________________________________________________
--  A(31:1)    XXXXXXXXXX_________________________________________________XXXXX
--             ________________________________________________________________
--  RNW        
--             _____________________                                    _______
--  DBE_N(3:0)                      \__________________________________/
--             ___________________________________________________          ___
--  DTACK_N                                                       \________/
--                                                         ___________________ 
--  LD(31:0)   -------------------------------------------<___________________>-
--
--
-- HPI Write Timing
--                           ________                    ________
--  HCTRL(1:0) XXXXXXXXXXXXXX________XXXXXXXXXXXXXXXXXXXX________XXXXXXXXXXXXXX
--                           ________                    ________
--  RNW        XXXXXXXXXXXXXX        XXXXXXXXXXXXXXXXXXXX        XXXXXXXXXXXXXX
--                                                       ________
--  HHWIL      XXXXXXXXXXXXXX________XXXXXXXXXXXXXXXXXXXX        XXXXXXXXXXXXXX
--             ___________                                                _____
--  HCS_N                 \______________________________________________/
--             __________________              _____________          _________
--  STROBE_N                     \____________/             \________/
--                                        ________                _______
--  HD(15:0)   --------------------------<________>--------------<_______>-----
--                           ____________                             ___
--  HRDY_N     _____________/            \___________________________/   \_____
--
--
-- Local VME Write Cycle
--               _________________________________________________________
--  A(31:1)    XX_________________________________________________________XXXXX
--             
--  RNW        ________________________________________________________________
--             ____________                                                ____
--  DBE_N(3:0)             \______________________________________________/
--             ________________________________________________________       _
--  DTACK_N                                                            \_____/
--                      ________
--  LD(31:0)   --------<________>----------------------------------------------
--
--
--  Data Byte Order
--             D31..24  D23..16  D15..8  D7..0
--  From VME   [31:24]  [23:16]  [15:8]  [7:0]
--  To DSP     [31:24]  [23:16]  [15:8]  [7:0]
-------------------------------------------------------------------------------
--
--------------------------------------------------------------------------------
-- Structure: 
--------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 60MHz => vmeclk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--------------------------------------------------------------------------------
-- Author:         John Joseph
-- Board Engineer: 
-- History:
--    12/11/00 JMJ First version
--------------------------------------------------------------------------------
-- LIBRARY INCLUDES
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations

--------------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------------

entity hpi_data_controller is
  port (
    clk_in            : in  std_logic; -- vme_clk66 input
    rst_n_in          : in  std_logic; -- asynchronous global reset 
    mdsp_cen_in       : in  std_logic; -- MDSP CE_N signal in
    mdsp_ds_in        : in  std_logic; -- MDSP Data Strobe (VME BE)
    mdsp_hrdy_n_in    : in  std_logic; -- Master DSP HRDY_N signal
    mdsp_hpi_data_in  : in  std_logic_vector(15 downto 0); --
    mdsp_hpi_data_out : out std_logic_vector(15 downto 0); --
    mdsp_cen_n_out    : out std_logic; -- MDSP CE_N signal out
    mdsp_rnw_out      : out std_logic; -- MDSP RNW signal out
    mdsp_hhwil_out    : out std_logic; -- MDSP HHWIL signal
    mdsp_hds1_n_out   : out std_logic; -- MDSP HHDS1 signal
    vme_bus_rnw_in    : in  std_logic; -- VMEBus read/not_write signal
    mdsp_vme_data_in  : in  std_logic_vector(31 downto 0); --
    mdsp_vme_data_out : out std_logic_vector(31 downto 0); --
    hpi_dtack_n_out   : out std_logic; -- VMEBus LACK_N 
    hpi_berr_out      : out std_logic; -- VMEBus BERR
    rcf_cen_in        : in  std_logic;
--    rcf_hrdy_n_in     : in  std_logic;
    rcf_cen_n_out     : out std_logic;
    vme_timeout_in    : in  std_logic_vector(31 downto 0);
	 hpi_state_debug   : out std_logic_vector(4 downto 0)
    );
end hpi_data_controller; 

architecture rtl of hpi_data_controller is
--------------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------------

type hpi_access is(
  idle,
  setup_hpi_fhw_wr_cycle,
  setup_fhw_data,
  wr_hpi_data_fhw,
  setup_hpi_shw_wr_cycle,
  setup_shw_data,
  start_wr_hpi_data_shw,
  wr_hpi_data_shw,
  setup_hpi_fhw_rd_cycle,
  rd_hpi_data_fhw,
  wait_for_fhw_hrdy,
  latch_hpi_fhw,
  setup_hpi_shw_rd_cycle,
  rd_hpi_data_shw,
  wait_for_shw_hrdy,
  latch_hpi_shw,
  wait_for_hpi_ready,
  send_hpi_dtack,
  done
  );

signal hpi_access_state  : hpi_access;
signal hpi_shw_wr_data_i : std_logic_vector(15 downto 0);
signal hrdy_n_i          : std_logic;
--signal rcf_hrdy_i        : std_logic;
signal mdsp_ds_i         : std_logic;
signal mdsp_ds_ii        : std_logic;

signal hpi_state			 : std_logic_vector(4 downto 0);

--------------------------------------------------------------------------------
-- COMPONENT INSTANTIATION
--------------------------------------------------------------------------------
begin

hpi_state_debug	<= hpi_state;

--------------------------------------------------------------------------------
-- Process to register all clk40 signals using the VME Clock
--------------------------------------------------------------------------------
signal_register_process: process(clk_in, rst_n_in)
begin
  if(rst_n_in = '0') then
    hrdy_n_i   <= '0';
  elsif(rising_edge(clk_in)) then
    hrdy_n_i   <= mdsp_hrdy_n_in;
    --rcf_hrdy_i <= rcf_hrdy_n_in;
  end if;
end process signal_register_process;

--------------------------------------------------------------------------------
-- Process to generate the VME read/write strobe to the ROD, and the DTACK 
-- signal to the VME Controller.
--------------------------------------------------------------------------------
vme_to_hpi_data_process: process(clk_in, rst_n_in)
variable wait_count    : unsigned( 1 downto 0);
variable timeout_count : unsigned(31 downto 0);
begin
  if(rst_n_in = '0') then
    mdsp_ds_i         <= '0';
    mdsp_ds_ii        <= '0';
    mdsp_cen_n_out    <= '1';
    mdsp_hds1_n_out   <= '1';
    mdsp_hhwil_out    <= '0';
    mdsp_rnw_out      <= '0';
    hpi_dtack_n_out   <= '1';
    hpi_berr_out      <= '0';
    rcf_cen_n_out     <= '1';
    mdsp_hpi_data_out <= (others => '0');
    mdsp_vme_data_out <= (others => '0');
    wait_count        := (others => '0');
    timeout_count     := (others => '0');
    hpi_access_state  <= idle;
	 hpi_state			 <= "00000";
  elsif(rising_edge(clk_in)) then
    mdsp_ds_i  <= mdsp_ds_in;
    mdsp_ds_ii <= mdsp_ds_i;
    if(mdsp_cen_in = '1') then
      case hpi_access_state is
        when idle =>
          mdsp_cen_n_out  <= '1';
          mdsp_hds1_n_out <= '1';
          mdsp_hhwil_out  <= '0';
          hpi_dtack_n_out <= '1';
          wait_count      := (others => '0');
          mdsp_rnw_out    <= vme_bus_rnw_in;
          if(mdsp_ds_i = '1') then
            if(vme_bus_rnw_in = '1') then
              hpi_access_state <= setup_hpi_fhw_rd_cycle;
              mdsp_rnw_out     <= '1';
              mdsp_cen_n_out   <= '0';
            elsif (vme_bus_rnw_in = '0') then
              hpi_access_state <= setup_hpi_fhw_wr_cycle;
              mdsp_rnw_out     <= '0';
            end if;
          end if;
			 hpi_state			 <= "00000";
          
--               ********** Write Algorithm States **********
        when setup_hpi_fhw_wr_cycle =>
          mdsp_cen_n_out    <= '0';
          mdsp_hpi_data_out <= mdsp_vme_data_in(15 downto 0);
          hpi_shw_wr_data_i <= mdsp_vme_data_in(31 downto 16);
          hpi_access_state  <= setup_fhw_data;
			 hpi_state			 <= "00001";

        when setup_fhw_data => 
          mdsp_hds1_n_out   <= '0';
          if(wait_count = 1) then
            hpi_access_state <= wr_hpi_data_fhw;
            wait_count       := (others => '0');
          else
            wait_count := wait_count + 1;
          end if;
			 hpi_state			 <= "00010";
        
        when wr_hpi_data_fhw =>
          mdsp_hds1_n_out <= '1';
          if(wait_count = 1) then
            if(hrdy_n_i <= '0') then
              hpi_access_state <= setup_hpi_shw_wr_cycle;
              wait_count       := (others => '0');
            end if;
          else
            wait_count := wait_count + 1;
          end if;
			 hpi_state			 <= "00011";

        when setup_hpi_shw_wr_cycle =>
          mdsp_hhwil_out    <= '1';
          mdsp_hpi_data_out <= hpi_shw_wr_data_i;
          hpi_access_state  <= start_wr_hpi_data_shw;
			 hpi_state			 <= "00100";

        when start_wr_hpi_data_shw =>
          mdsp_hds1_n_out   <= '0';
          if(wait_count = 1) then
            hpi_access_state <= setup_shw_data;
            wait_count       := (others => '0');
          else
            wait_count := wait_count + 1;
          end if;
			 hpi_state			 <= "00101";

        when setup_shw_data => 
          mdsp_hds1_n_out  <= '1';
          hpi_access_state <= wr_hpi_data_shw;
			 hpi_state			 <= "00110";
        
        when wr_hpi_data_shw =>
          if(wait_count = 1) then
            hpi_access_state <= wait_for_hpi_ready;
            wait_count       := (others => '0');
          else
            wait_count := wait_count + 1;
          end if;
			 hpi_state			 <= "00111";
          
--               ********** Read Algorithm States **********
        when setup_hpi_fhw_rd_cycle =>
          mdsp_hds1_n_out  <= '0';
          hpi_access_state <= rd_hpi_data_fhw;
			 hpi_state			 <= "01000";

        when rd_hpi_data_fhw =>
          hpi_access_state <= wait_for_fhw_hrdy;
			 hpi_state			 <= "01001";
          
        when wait_for_fhw_hrdy =>
          hpi_access_state <= latch_hpi_fhw;
			 hpi_state			 <= "01010";

        when latch_hpi_fhw =>
          if (hrdy_n_i = '0') then
            mdsp_vme_data_out(15 downto 0) <= mdsp_hpi_data_in;
            mdsp_hds1_n_out <= '1';
            hpi_access_state <= setup_hpi_shw_rd_cycle;
          end if;
			 hpi_state			 <= "01011";

        when setup_hpi_shw_rd_cycle =>
          mdsp_hhwil_out   <= '1';
          hpi_access_state <= rd_hpi_data_shw;
			 hpi_state			 <= "01100";

        when rd_hpi_data_shw =>
          mdsp_hds1_n_out  <= '0';
          hpi_access_state <= wait_for_shw_hrdy;
			 hpi_state			 <= "01101";
          
        when wait_for_shw_hrdy =>
          hpi_access_state <= latch_hpi_shw;
			 hpi_state			 <= "01110";

        when latch_hpi_shw =>
          mdsp_hds1_n_out  <= '1';
          mdsp_vme_data_out(31 downto 16) <= mdsp_hpi_data_in;
          hpi_access_state <= wait_for_hpi_ready;
			 hpi_state			 <= "01111";

--               ********** Common States **********
        when wait_for_hpi_ready =>
          if (hrdy_n_i = '0') then
            hpi_access_state <= send_hpi_dtack;
          end if;
			 hpi_state			 <= "10000";

        when send_hpi_dtack =>
          if (hrdy_n_i = '0') then  -- test for hrdy_n low again
            hpi_dtack_n_out  <= '0';
            hpi_access_state <= done;
          end if;
			 hpi_state			 <= "10001";

        when done =>
          mdsp_cen_n_out   <= '1';
          if (mdsp_ds_i = '0' AND mdsp_ds_ii = '0') then
            hpi_dtack_n_out  <= '1';
            hpi_access_state <= idle;
          end if;   
			 hpi_state			 <= "10010";			 

        when others =>
          mdsp_cen_n_out  <= '1';
          mdsp_hds1_n_out <= '1';
          mdsp_hhwil_out  <= '0';
          hpi_dtack_n_out <= '1';
          hpi_access_state <= idle;
			 hpi_state			 <= "11111";
      end case;
		
    elsif(rcf_cen_in = '1') then
      if(rcf_cen_in = '1' AND mdsp_ds_in = '1') then
        rcf_cen_n_out     <= '0';
        hpi_dtack_n_out <= NOT mdsp_ds_in;
      else
--        hpi_dtack_n_out <= rcf_hrdy_i;
      end if;
    else  -- All other regions
      mdsp_cen_n_out  <= '1';
      mdsp_hds1_n_out <= '1';
      mdsp_hhwil_out  <= '0';
      hpi_dtack_n_out <= '1';
    end if;

    if(mdsp_ds_in = '1') then
      if(timeout_count = unsigned(vme_timeout_in)) then
        hpi_berr_out  <= '1';
        hpi_access_state <= done;
      else
        timeout_count := timeout_count + 1 ;
      end if;
    else
      hpi_berr_out  <= '0';
      timeout_count := (others => '0');
    end if;
  end if;
end process vme_to_hpi_data_process;

end rtl;
