--  Lawrence Berkeley National Laboratory
--------------------------------------------------------------------------------
--  Filename: flash_mem_controller.vhd
--  Title: FPGA Program Reset Manager
--  Description:    This FPGA controls configuration of all FPGA parts, and
--                  controls the reset signals to all FPGAs and DSPs on the
--                  Read Out Driver Board used in Atlas.
--
--                  The SST 39V040 series Flash Data sheet was required to
--                  understand how to code the write algorithm in the VHDL.
--
--------------------------------------------------------------------------------
-- Author: John Joseph
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Revision History
--------------------------------------------------------------------------------
--
--  June 8, 2000  JMJ defined top ports to match schematic requirements
--  Aug 21, 2000  JMJ began to revise code for first prototype
--  Dec 06, 2001  JMJ Added more comments
--------------------------------------------------------------------------------
--  Library Declaration
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;          --needed for logical operations
use IEEE.std_logic_arith.all;         --needed for +/- operations

--------------------------------------------------------------------------------
--  PORT DECLARATION
--------------------------------------------------------------------------------

entity flash_decoder_block is
  port (
    clk_in            : in  std_logic;
    cclk_in           : in  std_logic;
    rst_n_in          : in  std_logic;
    fdb_addr_in       : in  std_logic_vector(23 downto 0);
    fdb_data_in       : in  std_logic_vector( 7 downto 0);
    fdb_data_out      : out std_logic_vector( 7 downto 0);
    fdb_cr_in         : in  std_logic_vector( 2 downto 0);
    fdb_sr_out        : out std_logic_vector( 2 downto 0);
    cnfg_enable_in    : in  std_logic;
    cnfg_addr_in      : in  std_logic_vector(23 downto 0);
    cnfg_data_out     : out std_logic_vector( 7 downto 0);
    memory_addr_out   : out std_logic_vector(20 downto 0);
    memory_data_in    : in  std_logic_vector( 7 downto 0);
    memory_data_out   : out std_logic_vector( 7 downto 0);
    write_en_n_out    : out std_logic;
    output_en_n_out   : out std_logic;
    CE0_n_out         : out std_logic;
    shift_reg_load_in : in  std_logic_vector(1 downto 0)
    );
end flash_decoder_block;

architecture rtl of flash_decoder_block is

--------------------------------------------------------------------------------
--SIGNAL DECLARATION
--------------------------------------------------------------------------------

signal flash_sr_i    : std_logic_vector(2 downto 0);
signal oen_count_i   : natural range 0 to 10;
signal wen_count_i   : natural range 0 to 10;
signal cycle_count_i : natural range 1 to 4;

--------------------------------------------------------------------------------
--COMPONENT DECLARATION
--------------------------------------------------------------------------------

begin
--------------------------------------------------------------------------------
--COMPONENT INSTANTIATION
--------------------------------------------------------------------------------
 
--------------------------------------------------------------------------------
--PROCESS DECLARATION
--------------------------------------------------------------------------------

flash_read_write : process (
  clk_in, 
  cclk_in, 
  rst_n_in, 
  fdb_addr_in,
  cnfg_addr_in, 
  cnfg_enable_in, 
  memory_data_in, 
  shift_reg_load_in
  )
begin
  if (rst_n_in = '0') then
    write_en_n_out  <= '1';
    output_en_n_out <= '1';
    CE0_n_out       <= '1';
    fdb_data_out    <= (others => '0');
    fdb_sr_out      <= (others => '0');
    flash_sr_i      <= (others => '0');
    oen_count_i     <= 0;
    wen_count_i     <= 0;
    cycle_count_i   <= 1;
    cnfg_data_out   <= (others => '0');
  elsif (clk_in'event AND clk_in = '1') then
    CE0_n_out <= '1';
-- Read FPGA configuration data
    if (cnfg_enable_in = '1') then
      memory_addr_out <= cnfg_addr_in(20 downto 0);
      cnfg_data_out   <= memory_data_in;
	    CE0_n_out       <= '0';
      write_en_n_out  <= '1';
      output_en_n_out <= NOT (shift_reg_load_in(0) OR shift_reg_load_in(1));
--  VME cycle
    elsif (cnfg_enable_in = '0' AND fdb_addr_in(23 downto 21) = "111") then
      cnfg_data_out <= (others => '0');
      CE0_n_out <= '1';
      if (fdb_cr_in = "001" AND flash_sr_i(0) = '0') then  -- read initiated by vme
        CE0_n_out       <= '0';
        memory_addr_out <=  fdb_addr_in(20 downto 0);
        write_en_n_out  <= '1';
        if (oen_count_i = 9) then 
--        oen_count_i <= 0;          -- reset oen counter
          output_en_n_out <= '1';
          fdb_sr_out(0) <= '1';      -- clear flash cmd reg
          flash_sr_i(0) <= '1';
        elsif (oen_count_i < 3) then   -- wait for 75ns (FLASH access time)
          output_en_n_out <= '0'; 
          fdb_sr_out(0) <= '0';
          flash_sr_i(0) <= '0';
          oen_count_i <= oen_count_i + 1;  -- inc oen counter
        else
          output_en_n_out <= '0';
          fdb_sr_out(0) <= '0';
          flash_sr_i(0) <= '0';
          fdb_data_out <= memory_data_in;
          oen_count_i <= oen_count_i + 1;  -- inc oen counter
        end if;
      elsif (fdb_cr_in = "010" AND flash_sr_i(1) = '0') then  -- write initiated by vme
        memory_addr_out <=  fdb_addr_in(20 downto 0);
        output_en_n_out <= '1';
        CE0_n_out       <= '0';
        if (wen_count_i = 6) then
--        wen_count_i    <=  0 ; -- reset wen counter
          write_en_n_out <= '1'; 
          fdb_sr_out(1)  <= '1'; -- clear flash cmd reg
          flash_sr_i(1)  <= '1';
        elsif (wen_count_i < 5) then      -- wait for 75ns for setup
          memory_data_out <= fdb_data_in; -- reg data to FLASH data
          write_en_n_out  <= '0';
          fdb_sr_out(1)   <= '0';
          flash_sr_i(1)   <= '0';
          wen_count_i     <= wen_count_i + 1;  -- inc wen counter
        else
          memory_data_out <= fdb_data_in; -- at 100ns write data to FLASH
          write_en_n_out  <= '1';
          fdb_sr_out(1)   <= '0';
          flash_sr_i(1)   <= '0';
          wen_count_i     <= wen_count_i + 1;  -- inc wen counter
        end if;
      elsif (fdb_cr_in = "100" AND flash_sr_i(2) = '0') then  -- prm assisted write initiated by vme
        output_en_n_out <= '1';
--  1st Unlock cycle
        if (cycle_count_i = 1) then
          CE0_n_out       <= '0';
          memory_addr_out <=  "000000101010101010101"; -- 0x05555h
          if (wen_count_i = 5) then
            wen_count_i    <=  0 ; -- reset wen counter
            write_en_n_out <= '1';
            cycle_count_i <= cycle_count_i + 1;
          elsif (wen_count_i < 4) then      -- wait for setup
            memory_data_out <= "10101010";  -- data 0xAAh to FLASH
            write_en_n_out  <= '0';
            wen_count_i     <= wen_count_i + 1;  -- inc wen counter
          else
            write_en_n_out <= '1';
            wen_count_i <= wen_count_i + 1;  -- inc wen counter
          end if;
          fdb_sr_out(2) <= '0';
          flash_sr_i(2) <= '0';
--  2nd Unlock cycle
        elsif (cycle_count_i = 2) then
          CE0_n_out       <= '0';
          memory_addr_out <= "000000010101010101010"; -- 0x02AAAh
          if (wen_count_i = 5) then
            wen_count_i    <=  0 ;  -- reset wen counter
            write_en_n_out <= '1';
            cycle_count_i  <= cycle_count_i + 1;
          elsif (wen_count_i < 4) then        -- wait for setup
            memory_data_out <= "01010101";  -- data 0x55h to FLASH
            write_en_n_out  <= '0';
            wen_count_i     <= wen_count_i + 1;  -- inc wen counter
          else
            write_en_n_out <= '1';
            wen_count_i    <= wen_count_i + 1;  -- inc wen counter
          end if;
          fdb_sr_out(2) <= '0';
          flash_sr_i(2) <= '0';
--  3rd Unlock cycle
        elsif (cycle_count_i = 3) then
          CE0_n_out       <= '0';
          memory_addr_out <= "000000101010101010101"; -- 0x05555h
          if (wen_count_i = 5) then
            wen_count_i <= 0;               -- reset wen counter
            write_en_n_out <= '1';
            cycle_count_i <= cycle_count_i + 1;
          elsif (wen_count_i < 4) then        -- wait for setup
            memory_data_out <= "10100000";  -- data 0xA0h to FLASH
            write_en_n_out <= '0';
            wen_count_i <= wen_count_i + 1;  -- inc wen counter
          else
            write_en_n_out <= '1';
            wen_count_i <= wen_count_i + 1;  -- inc wen counter
          end if;
          fdb_sr_out(2) <= '0';
          flash_sr_i(2) <= '0';
--  Write Data
        elsif (cycle_count_i = 4) then
          CE0_n_out       <= '0';
          memory_addr_out <=  fdb_addr_in(20 downto 0);
          if (wen_count_i = 5) then
--          cycle_count_i  <=  1 ;  -- reset cycle counter
--          wen_count_i    <=  0 ;  -- reset wen counter
            write_en_n_out <= '1';
            fdb_sr_out(2)  <= '1';  -- clear flash cmd reg
            flash_sr_i(2)  <= '1';
          elsif (wen_count_i < 4) then    -- wait for setup
            memory_data_out <= fdb_data_in; -- reg data to FLASH data
            write_en_n_out <= '0';
            fdb_sr_out(2)  <= '0';
            flash_sr_i(2)  <= '0';
            wen_count_i    <= wen_count_i + 1;  -- inc wen counter
          else
            write_en_n_out <= '1';
            fdb_sr_out(2)  <= '0';
            flash_sr_i(2)  <= '0';
            wen_count_i    <= wen_count_i + 1;  -- inc wen counter
          end if;
        end if;
      elsif (fdb_cr_in = "000") then    -- idle if no read or write cmd
        fdb_sr_out      <= "000";
        flash_sr_i      <= "000";
        wen_count_i     <=  0 ;
        oen_count_i     <=  0 ;
        cycle_count_i   <=  1 ;  -- reset cycle counter
        CE0_n_out       <= '1';
        write_en_n_out  <= '1';
        output_en_n_out <= '1';
      else 
        CE0_n_out       <= '1';
        write_en_n_out  <= '1';
        output_en_n_out <= '1';
      end if;

    else
      cnfg_data_out   <= (others => '0');
      CE0_n_out       <= '1';
      output_en_n_out <= '1';
      write_en_n_out  <= '1';
      fdb_sr_out      <= "000";
    end if;
  end if;
end process flash_read_write;

end rtl;

