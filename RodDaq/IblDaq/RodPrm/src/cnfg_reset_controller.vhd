--  Lawrence Berkeley National Laboratory
--------------------------------------------------------------------------------
--  Filename: cnfg_reset_controller.vhd
--  Title: FPGA Program Reset Manager
--  Description:    This FPGA controls configuration of all FPGA parts, and
--                  controls the reset signals to all FPGAs and DSPs on the
--                  Read Out Driver Board used in Atlas.
--
--------------------------------------------------------------------------------
-- Author: John Joseph
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
-- Revision History
--------------------------------------------------------------------------------
--
--  June 8, 2000  JMJ defined top ports to match schematic requirements
--  Aug 21, 2000  JMJ began to revise code for first prototype

--------------------------------------------------------------------------------
--  Library Declaration
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;          --needed for logical operations
use IEEE.std_logic_arith.all;         --needed for +/- operations

--------------------------------------------------------------------------------
--  PORT DECLARATION
--------------------------------------------------------------------------------

entity fpga_cnfg_reset_controller_block is
port (
  clk_in   : in  std_logic;
  rst_n_in : in  std_logic;

  cb_fpga_rst_reg_in  : in  std_logic_vector( 5 downto 0);
  cb_dsp_rst_reg_in   : in  std_logic_vector( 5 downto 0);
  cb_clr_rst_reg_out  : out std_logic;

  cb_fpga_rst_status_reg_out : out std_logic_vector(4 downto 0);
  cb_dsp_rst_status_reg_out  : out std_logic_vector(4 downto 0);

  cb_rrif_rst_n_out    : out std_logic;
  cb_formA_rst_n_out   : out std_logic;
  cb_formB_rst_n_out   : out std_logic;
  cb_efb_rst_n_out     : out std_logic;
  cb_router_rst_n_out  : out std_logic;

  cb_mdsp_rst_n_out  : out std_logic;
  cb_sdsp0_rst_n_out : out std_logic;
  cb_sdsp1_rst_n_out : out std_logic;
  cb_sdsp2_rst_n_out : out std_logic;
  cb_sdsp3_rst_n_out : out std_logic
  );
end fpga_cnfg_reset_controller_block;

architecture RTL of fpga_cnfg_reset_controller_block is

--------------------------------------------------------------------------------
--SIGNAL DECLARATION
--------------------------------------------------------------------------------

-- internal reset signals
signal rc_rrif_rst_n_i   : std_logic;
signal rc_formA_rst_n_i  : std_logic;
signal rc_formB_rst_n_i  : std_logic;
signal rc_efb_rst_n_i    : std_logic;
signal rc_router_rst_n_i : std_logic;
signal rc_mdsp_rst_n_i   : std_logic;
signal rc_sdsp0_rst_n_i  : std_logic;
signal rc_sdsp1_rst_n_i  : std_logic;
signal rc_sdsp2_rst_n_i  : std_logic;
signal rc_sdsp3_rst_n_i  : std_logic;

--  internal signals for process counters
signal reset_count_i     : unsigned(18 downto 0);

begin
--------------------------------------------------------------------------------
-- Signal Connections
--------------------------------------------------------------------------------

cb_rrif_rst_n_out   <= rc_rrif_rst_n_i;
cb_formA_rst_n_out  <= rc_formA_rst_n_i;
cb_formB_rst_n_out  <= rc_formB_rst_n_i;
cb_efb_rst_n_out    <= rc_efb_rst_n_i;
cb_router_rst_n_out <= rc_router_rst_n_i;
cb_mdsp_rst_n_out   <= rc_mdsp_rst_n_i;
cb_sdsp0_rst_n_out  <= rc_sdsp0_rst_n_i;
cb_sdsp1_rst_n_out  <= rc_sdsp1_rst_n_i;
cb_sdsp2_rst_n_out  <= rc_sdsp2_rst_n_i;
cb_sdsp3_rst_n_out  <= rc_sdsp3_rst_n_i;
 
--------------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------------
--
--  The process reset_device_controller_process resets the devices on the ROD board by
--  reading the reset register located in the register block.  If a reset
--  command is sent, the reset controller will clear the appropriate register
--  when the reset is complete.
--
device_reset_controller_process: process(clk_in, rst_n_in)
subtype select_device_to_reset is std_logic_vector(11 downto 0);
begin
  if(rst_n_in = '0') then
    reset_count_i      <= (others => '0');
    cb_clr_rst_reg_out <= '0';
    rc_rrif_rst_n_i    <= '1';
    rc_formA_rst_n_i   <= '1';
    rc_formB_rst_n_i   <= '1';
    rc_efb_rst_n_i     <= '1';
    rc_router_rst_n_i  <= '1';
    rc_mdsp_rst_n_i    <= '1';
    rc_sdsp0_rst_n_i   <= '1';
    rc_sdsp1_rst_n_i   <= '1';
    rc_sdsp2_rst_n_i   <= '1';
    rc_sdsp3_rst_n_i   <= '1';
  elsif(rising_edge(clk_in)) then
      case select_device_to_reset(cb_dsp_rst_reg_in & cb_fpga_rst_reg_in) is
-- Only 1 CCLK required to successfully reset an FPGA
        when "000000000001" =>
          cb_clr_rst_reg_out <= '1';
          rc_rrif_rst_n_i    <= '0';

        when "000000000010" =>
          cb_clr_rst_reg_out <= '1';
          rc_formA_rst_n_i   <= '0';

        when "000000000100" =>
          cb_clr_rst_reg_out <= '1';
          rc_formB_rst_n_i   <= '0';

        when "000000001000" => 
          cb_clr_rst_reg_out <= '1';
          rc_efb_rst_n_i     <= '0';

        when "000000010000" => 
          cb_clr_rst_reg_out <= '1';
          rc_router_rst_n_i <= '0';
  
        when "000000100000" =>
          cb_clr_rst_reg_out <= '1';
          rc_rrif_rst_n_i   <= '0';
          rc_formA_rst_n_i  <= '0';
          rc_formB_rst_n_i  <= '0';
          rc_efb_rst_n_i    <= '0';
          rc_router_rst_n_i <= '0';
-- The   DSPs require a long reset pulse
        when "000001000000" =>
          if 
			 (reset_count_i > 524000) then   -- 140ms
            rc_mdsp_rst_n_i    <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_mdsp_rst_n_i <= '0';
          end if;

        when "000010000000" =>
          if(reset_count_i > 524000) then   -- 140ms
            rc_sdsp0_rst_n_i   <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_sdsp0_rst_n_i <= '0';
          end if;

        when "000100000000" =>
          if(reset_count_i > 524000) then   -- 140ms
            rc_sdsp1_rst_n_i  <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_sdsp1_rst_n_i <= '0';
          end if;

        when "001000000000" => 
          if(reset_count_i > 524000) then   -- 140ms
            rc_sdsp2_rst_n_i   <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_sdsp2_rst_n_i <= '0';
          end if;

        when "010000000000" => 
          if(reset_count_i > 524000) then   -- 140ms
            rc_sdsp3_rst_n_i   <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_sdsp3_rst_n_i <= '0';
          end if;
                
        when "100000000000" =>
          if(reset_count_i > 524000) then   -- 140ms
            rc_rrif_rst_n_i    <= '1';
            rc_formA_rst_n_i   <= '1';
            rc_formB_rst_n_i   <= '1';
            rc_efb_rst_n_i     <= '1';
            rc_router_rst_n_i  <= '1';
            rc_mdsp_rst_n_i    <= '1';
            rc_sdsp0_rst_n_i   <= '1';
            rc_sdsp1_rst_n_i   <= '1';
            rc_sdsp2_rst_n_i   <= '1';
            rc_sdsp3_rst_n_i   <= '1';
            cb_clr_rst_reg_out <= '1';
          else
            reset_count_i <= reset_count_i + 1;
            rc_rrif_rst_n_i   <= '0';
            rc_formA_rst_n_i  <= '0';
            rc_formB_rst_n_i  <= '0';
            rc_efb_rst_n_i    <= '0';
            rc_router_rst_n_i <= '0';
            rc_mdsp_rst_n_i   <= '0';
            rc_sdsp0_rst_n_i  <= '0';
            rc_sdsp1_rst_n_i  <= '0';
            rc_sdsp2_rst_n_i  <= '0';
            rc_sdsp3_rst_n_i  <= '0';
          end if;
                  
        when others => 
          rc_rrif_rst_n_i    <= '1';
          rc_formA_rst_n_i   <= '1';
          rc_formB_rst_n_i   <= '1';
          rc_efb_rst_n_i     <= '1';
          rc_router_rst_n_i  <= '1';
          rc_mdsp_rst_n_i    <= '1';
          rc_sdsp0_rst_n_i   <= '1';
          rc_sdsp1_rst_n_i   <= '1';
          rc_sdsp2_rst_n_i   <= '1';
          rc_sdsp3_rst_n_i   <= '1';
          cb_clr_rst_reg_out <= '0';
          reset_count_i      <= (others => '0');
      end case;
  end if;
end process device_reset_controller_process;

register_status_out_process: process(clk_in, rst_n_in)
begin
  if(rst_n_in = '0') then
    cb_fpga_rst_status_reg_out <= (others => '0');
    cb_dsp_rst_status_reg_out  <= (others => '0');
  elsif(rising_edge(clk_in)) then
    cb_fpga_rst_status_reg_out(0) <= rc_rrif_rst_n_i;   
    cb_fpga_rst_status_reg_out(1) <= rc_formA_rst_n_i;  
    cb_fpga_rst_status_reg_out(2) <= rc_formB_rst_n_i;  
    cb_fpga_rst_status_reg_out(3) <= rc_efb_rst_n_i;    
    cb_fpga_rst_status_reg_out(4) <= rc_router_rst_n_i; 

    cb_dsp_rst_status_reg_out(0)  <= rc_mdsp_rst_n_i;
    cb_dsp_rst_status_reg_out(1)  <= rc_sdsp0_rst_n_i; 
    cb_dsp_rst_status_reg_out(2)  <= rc_sdsp1_rst_n_i; 
    cb_dsp_rst_status_reg_out(3)  <= rc_sdsp2_rst_n_i; 
    cb_dsp_rst_status_reg_out(4)  <= rc_sdsp3_rst_n_i;
  end if;
end process register_status_out_process;

end RTL;


