-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support 
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: register_block.vhd
-- Description:
--      Register file for FPGA Program Reset Manager register bus. 
--
-- fpga_program_reset_manager
--  Control Registers : 8 bit
--   C00000  FPGA Cnfg Control Reg    RW
--                    Bit 0 : Cnfg RRIF 
--                    Bit 1 : Cnfg Formatter A&B 
--                    Bit 2 : Not Used
--                    Bit 3 : Cnfg EFB 
--                    Bit 4 : Cnfg Router 
--                    Bit 5 : Cnfg All
--                    Bit 6 : Cnfg Override
--   C00004  FPGA Reset Control Reg   RW
--                    Bit 0 : Reset RRIF 
--                    Bit 1 : Reset Formatter A 
--                    Bit 1 : Reset Formatter B 
--                    Bit 3 : Reset EFB 
--                    Bit 4 : Reset Router 
--                    Bit 5 : Reset All
--                    Bit 6 : Hold All in Reset
--   C00008  DSP Reset Control Reg      RW
--                    Bit 0 : MDSP Boot Mode Select
--                    Bit 1 : Reset Master DSP
--                    Bit 2 : Reset Slave DSP0
--                    Bit 3 : Reset Slave DSP1
--                    Bit 4 : Reset Slave DSP2
--                    Bit 5 : Reset Slave DSP3
--                    Bit 6 : Reset All DSPs
--   C0000C  FLASH Control Reg        RW
--                    Bit 0 : Read Flash Direct VME
--                    Bit 1 : Write Flash Direct VME
--                    Bit 2 : Write Flash PRM Control
--                            (This is the mode used for writing the FLASH
--                             in the Test Stand.  It minimizes VME cycles
--                             by allowing the PRM to control the unlock
--                             algorithm of the FLASH.)
--
--  (This register can be used to transfer the Address with one VME access)
--   C00010  FLASH Address and Write Data (31:0) reg      RW
--                    Bits 23..0  : Address Location
--                    Bits 31..24 : Data to Write
--
--   C00014  Misc ROD Control/Status                      RW
--                    Bit  0 : ROD Clock Select     (0 = BOC  1 = Internal ROD)
--                    Bit  1 : ROD Clock DLL Locked (1 = DLL Locked)
--                    Bit  2 : VME Clock DLL Locked (1 = DLL Locked)
--                    Bit  3 : ROD Busy In          (1 = ROD Busy)
--                    Bit  4 : ROD Type In          (0 = SCT, 1 = Pixel)
--                    Bit  5 : Formatter Type In(0) (Pixel Only)
--                    Bit  6 : Formatter Type In(1) (Pixel Only)
--                    Bit  7 : Slink LFF            (1 = Slink FIFO Full)
--                    Bit  8 : Slink LDOWN          (1 = Slink Down)
--                    Bit  9 : DSP0 Detect In       (1 = DSP Present)
--                    Bit 10 : DSP1 Detect In       (1 = DSP Present)
--                    Bit 11 : DSP2 Detect In       (1 = DSP Present)
--                    Bit 12 : DSP3 Detect In       (1 = DSP Present)
--                    Bit 13 : MDSP HRDY            (1 = HP Busy)
--                    Bit 14 : LVME LACK Status
--                    Bit 15 : MDSP ARDY Status   
--
--  Status Registers : 8 bit
--   C00020  FPGA Cnfg Status Reg     R
--                    Bit  0 : RRIF Done
--                    Bit  1 : Formatter A Done
--                    Bit  2 : Formatter B Done
--                    Bit  3 : EFB Done
--                    Bit  4 : Router Done
--                    Bit  5 : Cnfg Enable Status
--   C00024  FPGA Reset Status Reg    R
--                    Bit 0 : RRIF
--                    Bit 1 : Formatter A
--                    Bit 2 : Formatter B
--                    Bit 3 : EFB
--                    Bit 4 : Router
--   C00028  DSP Reset Status Reg       R
--                    Bit 0 : MDSP HPI Boot Enable
--                    Bit 1 : Master DSP
--                    Bit 2 : Slave DSP0
--                    Bit 3 : Slave DSP1
--                    Bit 4 : Slave DSP2
--                    Bit 5 : Slave DSP3
--   C0002C  FPGA INIT Status Reg     R
--                    Bit 0 : RRIF INIT_N
--                    Bit 1 : Formatter A INIT_N
--                    Bit 2 : Formatter B INIT_N
--                    Bit 3 : EFB INIT_N
--                    Bit 4 : Router INIT_N
--   C00030  Flash Status Reg         R
--   C00034  FPGA Halt Status Reg     R
--                    Bit 0 : RRIF Cnfg Failed
--                    Bit 1 : Formatter A Cnfg Failed
--                    Bit 2 : Formatter B Cnfg Failed
--                    Bit 3 : EFB Cnfg Failed
--                    Bit 4 : Router Cnfg Failed
--   C00038  ROD Serial Number          R
--                    Bits 0  ->  9 Board S/N
--                    Bits 16 -> 23 Board Revision
--                    Bits 24 -> 31 FPGA Code Version
--   C0003C  Flash Read Data Reg (7:0)  R
--
--  Aux Registers : 32 bit
--   c00040  VME Address (31:0)   R
--   c00044  PRM Spare Pins(31:0) R
--   c00048  Aux R/W (31:0)       R/W
--
-------------------------------------------------------------------------------
-- Structure:
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk
--      2.  Perform all memory operations on positive edge of clk
-------------------------------------------------------------------------------
-- Author: Johm M. Joseph
-- Board Engineer: John M. Joseph
-- History:
--          21 Aug 2000 JMJ Adapted control registers from 
--          ROD resources: register_block.vhd 
--          
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
use IEEE.std_logic_unsigned.all;

-------------------------------------------------------------------------------
-- PORT DECLARATION
-------------------------------------------------------------------------------

entity register_block is
  port (
    clk_in                     : in  std_logic; -- clk input
    rst_n_in                   : in  std_logic; -- async global reset
	 
    prb_bus_strb_in            : in  std_logic;
    prb_bus_ce_in              : in  std_logic;
    prb_bus_rnw_in             : in  std_logic;
    prb_bus_addr_in            : in  std_logic_vector(31 downto 0);
    prb_bus_data_in            : in  std_logic_vector(31 downto 0);
    prb_bus_data_out           : out std_logic_vector(31 downto 0);
    prb_bus_ack_out            : out std_logic;
	  
    prb_fpga_rst_status_reg_in : in  std_logic_vector( 4 downto 0);
    prb_dsp_rst_status_reg_in  : in  std_logic_vector( 4 downto 0);
    fpga_rst_reg_out           : out std_logic_vector( 7 downto 0);
    dsp_rst_reg_out            : out std_logic_vector( 6 downto 0);
	 fpga_config_write_out		 : out std_logic_vector(31 downto 0);
	 fpga_config_read_in			 : in  std_logic_vector(31 downto 0);
	 
    clr_rst_reg_in             : in  std_logic;
	 serial_id						 : in  std_logic_vector(7 downto 0);
	 
    rod_busy_in                : in  std_logic;
    rod_clk_status_in          : in  std_logic;
    mdsp_hrdy_n_in             : in  std_logic;
    vme_timeout_out            : out std_logic_vector(31 downto 0);
    vme_am_code_in             : in  std_logic_vector( 5 downto 0);  -- vme AM code
    ga_n_in                    : in  std_logic_vector( 4 downto 0);
    gap_n_in                   : in  std_logic;
    trigger_in                 : in  std_logic;
	 ace_control           : out	std_logic_vector(31 downto 0);
	 ace_status_read       : in	std_logic_vector(31 downto 0)
    );   
end register_block;

architecture rtl of register_block is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

signal fpga_cnfg_control_reg_i : std_logic_vector( 6 downto 0);
signal fpga_rst_control_reg_i  : std_logic_vector( 7 downto 0);
signal dsp_rst_control_reg_i   : std_logic_vector( 6 downto 0);
signal fpga_rst_status_reg_i   : std_logic_vector( 4 downto 0);
signal dsp_rst_status_reg_i    : std_logic_vector( 5 downto 0);
signal aux_rw_reg_i            : std_logic_vector(31 downto 0);
signal vme_timeout_i           : std_logic_vector(31 downto 0);
signal clr_error_bits_i        : std_logic;
signal dprmA_data_i            : std_logic_vector(31 downto 0);
signal dprmB_data_i            : std_logic_vector(31 downto 0);
signal dprmA_data_o            : std_logic_vector(31 downto 0);
signal dprmB_data_o            : std_logic_vector(31 downto 0);
signal dprmA_addr_i            : std_logic_vector( 9 downto 0);
signal dprmB_addr_i            : std_logic_vector(15 downto 0);
signal dprmA_wr_i              : std_logic;
signal dprmB_wr_i              : std_logic;
signal dprmA_rd_i              : std_logic;
signal dprmB_rd_i              : std_logic;
signal dprm_rst_i              : std_logic;
signal prb_bus_strb_i          : std_logic;
signal prb_bus_strb_ii         : std_logic;
signal rb_count_i              : unsigned(18 downto 0);
signal fpga_config_i_write     : std_logic_vector(31 downto 0);
signal fpga_config_i_read      : std_logic_vector(31 downto 0);
signal ace_control_i           : std_logic_vector(31 downto 0):=X"00000003";
signal ace_status_read_i       : std_logic_vector(31 downto 0):=x"89abcdef";
--signal ace_status_write_i       : std_logic_vector(31 downto 0):=(others=>'0');


type vme_access_states is (
  idle,
  write,
  wr_hold,
  read,
  rd_hold,
  done
  );
signal vme_access : vme_access_states;

type rbh_states is (
  init_mem,
  flush_mem,
  idle,
  wait_for_trigger,
  rb_detect,
  write_data,
  inc_dprm_addr,
  done
  );
signal rbh_state    : rbh_states;
signal rbh_enable_i : std_logic;
signal rbh_trigger_mode_i : std_logic;

-------------------------------------------------------------------------------
-- COMPONENT DECLARATION
-------------------------------------------------------------------------------

component dpram_1024x32
	port (
	clka: IN std_logic;
	rsta: IN std_logic;
	wea: IN std_logic_VECTOR(0 downto 0);
	addra: IN std_logic_VECTOR(9 downto 0);
	dina: IN std_logic_VECTOR(31 downto 0);
	douta: OUT std_logic_VECTOR(31 downto 0);
	clkb: IN std_logic;
	rstb: IN std_logic;
	web: IN std_logic_VECTOR(0 downto 0);
	addrb: IN std_logic_VECTOR(9 downto 0);
	dinb: IN std_logic_VECTOR(31 downto 0);
	doutb: OUT std_logic_VECTOR(31 downto 0));
end component;

begin

fpga_config_write_out	<= fpga_config_i_write;
fpga_config_i_read		<= fpga_config_read_in;
ace_control<=ace_control_i;
ace_status_read_i<=ace_status_read;
-------------------------------------------------------------------------------
-- COMPONENT INSTATIATION
-------------------------------------------------------------------------------

dpram_instance: dpram_1024x32
		port map (
			clka 		=> clk_in,
			rsta 		=> dprm_rst_i,
			wea(0)	=> dprmA_wr_i,
			addra 	=> dprmA_addr_i,
			dina 		=> dprmA_data_i,
			douta 	=> dprmA_data_o,
			clkb 		=> clk_in,
			rstb		=> dprm_rst_i,
			web(0)	=> dprmB_wr_i,
			addrb 	=> dprmB_addr_i(9 downto 0),
			dinb 		=> dprmB_data_i,
			doutb 	=> dprmB_data_o);

dprm_rst_i <= NOT rst_n_in;

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

rw_registers_process : process(clk_in, rst_n_in)

constant code_version : std_logic_vector( 7 downto 0) := "00011000";  --ver 1.8
constant rod_revision : std_logic_vector( 7 downto 0) := "00001101";  --RevD
constant rod_id       : std_logic_vector( 7 downto 0) := "10101101";  --0xAD
constant irq_id       : std_logic_vector( 3 downto 0) := "0100";      --0x4
constant atlas_src_id : std_logic_vector(31 downto 0) := (others => '0');
constant atlas_mfg_id : std_logic_vector(23 downto 0) := "000010000000000000110000"; -- 080030 CERN
constant atlas_brd_id : std_logic_vector(31 downto 0) := "00000000000000000000001100010000"; -- 310
constant atlas_rev_id : std_logic_vector( 3 downto 0) := "1111"; -- 0xFn

begin
  if(rst_n_in = '0') then
    fpga_cnfg_control_reg_i <= (others => '0'); 
    fpga_rst_control_reg_i  <= (others => '0'); 
    dsp_rst_control_reg_i   <= (others => '0'); 
    fpga_rst_status_reg_i   <= (others => '0');
    dsp_rst_status_reg_i    <= (others => '0');
    fpga_rst_reg_out        <= (others => '0');
    dsp_rst_reg_out         <= (others => '0');
    prb_bus_data_out        <= (others => '0');
	 fpga_config_i_write		 <= (others => '0');
    vme_timeout_i           <= "00000000000000100111000100000000"; -- 2.0ms (0x27100*12.5ns)
    aux_rw_reg_i            <= (others => '0');
    dprmA_wr_i              <= '0';
    dprmA_rd_i              <= '0';
    prb_bus_ack_out         <= '0';
    prb_bus_strb_i          <= '0';
    vme_access              <= idle;
    ace_control_i           <= X"00000003";  --RESET STATE (NOT ENABLED)
  elsif(rising_edge(clk_in)) then
    prb_bus_strb_i  		<= prb_bus_strb_in;
    prb_bus_strb_ii 		<= prb_bus_strb_i;
    dprmA_rd_i      		<= '1';
    case vme_access is
      when idle =>
        prb_bus_data_out <= (others => '0'); 
        prb_bus_ack_out  <= '0';
        dprmA_addr_i     <= prb_bus_addr_in(11 downto 2);
        if (prb_bus_ce_in = '1' AND prb_bus_strb_i = '1') then
          if (prb_bus_rnw_in = '1') then
            vme_access <= read;
          else
            vme_access <= write;
          end if;
        end if;

      when read   =>   -- read registers
        if (prb_bus_addr_in(23 downto 7) = "11000000000000000") then 
          case prb_bus_addr_in(6 downto 2) is
            when "00000" => prb_bus_data_out( 6 downto  0) <= fpga_cnfg_control_reg_i; 
            when "00001" => prb_bus_data_out( 7 downto  0) <= fpga_rst_control_reg_i;
            when "00010" => prb_bus_data_out( 6 downto  0) <= dsp_rst_control_reg_i; 
            when "00011" => prb_bus_data_out( 2 downto  0) <= (others => '0');
            when "00100" => prb_bus_data_out(23 downto  0) <= (others => '0');
                            prb_bus_data_out(31 downto 24) <= (others => '0');
            when "00101" => prb_bus_data_out( 0)           <= '0'; 					-- C00014
                            prb_bus_data_out( 1)           <= rod_clk_status_in; 
                            prb_bus_data_out( 2)           <= '1'; 
                            prb_bus_data_out( 3)           <= rod_busy_in; 
                            prb_bus_data_out( 4)           <= '1'; 
                            prb_bus_data_out( 5)           <= '0';
                            prb_bus_data_out( 6)           <= '0'; 
                            prb_bus_data_out( 7)           <= '0'; 
                            prb_bus_data_out( 8)           <= '0'; 
                            prb_bus_data_out(12 downto 9)  <= "0000";
                            prb_bus_data_out(13)           <= mdsp_hrdy_n_in;
                            prb_bus_data_out(14)           <= '0';
                            prb_bus_data_out(15)           <= '0';
                            prb_bus_data_out(19 downto 16) <= irq_id;
                            prb_bus_data_out(25 downto 20) <= vme_am_code_in;
                            clr_error_bits_i               <= '1';
            when "00110" => prb_bus_data_out               <= vme_timeout_i; 		-- C00018
            when "00111" => prb_bus_data_out( 0)           <= rbh_enable_i;		-- C0001C
                            prb_bus_data_out( 1)           <= rbh_trigger_mode_i;
            when "01000" => prb_bus_data_out( 9 downto  0) <= (others => '1'); 	-- C00020
            when "01001" => prb_bus_data_out( 4 downto  0) <= fpga_rst_status_reg_i; -- C00024
            when "01010" => prb_bus_data_out( 5 downto  0) <= dsp_rst_status_reg_i; -- C00028
            when "01011" => prb_bus_data_out( 5 downto  0) <= (others => '0'); -- C0002C
            when "01100" => prb_bus_data_out( 3 downto  0) <= (others => '0'); -- C00030
            when "01101" => prb_bus_data_out( 4 downto  0) <= (others => '0'); -- C00034
            when "01110" => prb_bus_data_out( 7 downto  0) <= serial_id;		 -- C00038
									 prb_bus_data_out(11 downto  8) <= "0000";
                            prb_bus_data_out(15 downto 12) <= code_version(7 downto 4);
                            prb_bus_data_out(23 downto 16) <= rod_revision;
                            prb_bus_data_out(31 downto 24) <= rod_id;
            when "01111" => prb_bus_data_out( 7 downto  0) <= (others => '0');  -- C0003C
            when "10000" => prb_bus_data_out               <= atlas_src_id;	  -- C00040
            when "10001" => prb_bus_data_out(23 downto  0) <= atlas_mfg_id;	  -- C00044
            when "10010" => prb_bus_data_out               <= atlas_brd_id;	  -- C00048
            when "10011" => prb_bus_data_out(31 downto 24) <= code_version;	  -- C0004C
                            prb_bus_data_out(15 downto  8) <= serial_id;
									 prb_bus_data_out(17 downto 16) <= "00";
                            prb_bus_data_out( 7 downto  4) <= atlas_rev_id;
                            prb_bus_data_out( 3 downto  0) <= (others => '0');
            when "10100" => prb_bus_data_out               <= prb_bus_addr_in;
            when "10101" => prb_bus_data_out(13)           <= NOT gap_n_in;
                            prb_bus_data_out(12 downto 8)  <= NOT ga_n_in;
                            prb_bus_data_out( 7 downto 0)  <= (others => '0');
            when "10110" => prb_bus_data_out               <= aux_rw_reg_i;
            when "10111" => prb_bus_data_out(15 downto 0)  <= std_logic_vector(dprmB_addr_i); --0xC0005C
				when "11001" => prb_bus_data_out					  <= fpga_config_i_read; -- added by Davide (20 April 2012)
				when "11010" => prb_bus_data_out               <= ace_control_i;
				when "11011" => prb_bus_data_out               <= ace_status_read_i;
            when others  => prb_bus_data_out               <= (others => '0'); 
          end case;
        elsif (prb_bus_addr_in(23 downto 12) = "110000000001") then 
          prb_bus_data_out <= dprmA_data_o;
        end if;
        vme_access <= rd_hold;

      when rd_hold => 
        if (prb_bus_addr_in(23 downto 12) = "110000000001") then 
          prb_bus_data_out <= dprmA_data_o;
        end if;
        vme_access <= done;

      when write =>  -- write registers
        if (prb_bus_addr_in(23 downto 7) = "11000000000000000") then 
          case prb_bus_addr_in(6 downto 2) is
            when "00000" => fpga_cnfg_control_reg_i <= prb_bus_data_in( 6 downto  0);
            when "00001" => fpga_rst_control_reg_i  <= prb_bus_data_in( 7 downto  0);	-- C00004
            when "00010" => dsp_rst_control_reg_i   <= prb_bus_data_in( 6 downto  0);
            when "00011" => null;
            when "00100" => null;
            when "00110" => vme_timeout_i           <= prb_bus_data_in;
            when "00111" => rbh_enable_i            <= prb_bus_data_in(0);
                            rbh_trigger_mode_i      <= prb_bus_data_in(1);
            when "10110" => aux_rw_reg_i            <= prb_bus_data_in;
				when "11000" => fpga_config_i_write		 <= prb_bus_data_in; -- added by Davide (20 April 2012)
				when "11010" => ace_control_i				 <= prb_bus_data_in;
--				when "11011" => ace_status_write_i		 <= prb_bus_data_in;
	         when others  => null;
          end case;
        elsif (prb_bus_addr_in(23 downto 12) = "110000000001") then 
          dprmA_data_i <= prb_bus_data_in;
          dprmA_wr_i   <= '1';
        end if;
        vme_access <= wr_hold;

      when wr_hold => 
        vme_access <= done;

      when done =>
        dprmA_wr_i 		<= '0';
        prb_bus_ack_out <= '1';
        if(prb_bus_strb_i = '0' AND prb_bus_strb_ii = '0') then
          vme_access <= idle;
        end if;
               
      when others => 
        prb_bus_data_out <= "11011110101011011111000000001101"; 
        vme_access <= idle;
    end case;

    vme_timeout_out <= vme_timeout_i;
-- load status registers with FPGA prb status

    fpga_rst_status_reg_i <= prb_fpga_rst_status_reg_in;
    dsp_rst_status_reg_i  <= prb_dsp_rst_status_reg_in & dsp_rst_control_reg_i(0);

-- source control registers to controller block
    fpga_rst_reg_out    <= fpga_rst_control_reg_i; 
    dsp_rst_reg_out     <= dsp_rst_control_reg_i(6 downto 0);
      
-- clear cmd registers
-- "fpga_cnfg_control_reg_i(6) can only be cleared with a reset, or a VME write
-- "dsp_rst_control_reg_i(0) can only be cleared with a reset, or a VME write

    if(clr_rst_reg_in = '1') then 
      fpga_rst_control_reg_i            <= (others => '0');
      dsp_rst_control_reg_i(6 downto 1) <= (others => '0');
    end if;

    if (clr_error_bits_i = '1') then
      clr_error_bits_i <= '0';
    else
    end if;
  end if;
end process rw_registers_process;

rod_busy_hsm : process(clk_in, rst_n_in)
--  dprmB_data_o
variable bin_count : unsigned(19 downto 0);
begin
  if(rst_n_in = '0') then
    dprmB_rd_i 	<= '0';
    dprmB_wr_i 	<= '0';
    dprmB_addr_i 	<= (others => '0');
    dprmB_data_i 	<= (others => '0');
    rbh_state 		<= init_mem;
    rb_count_i 	<= (others => '0');
    bin_count 		:= (others => '0');
   elsif(rising_edge(clk_in)) then
    dprmB_rd_i <= '1';
    case rbh_state is
      when init_mem =>
        dprmB_wr_i <= '1';
        dprmB_data_i <= "10111010110100001101101010110000";
        dprmB_addr_i <= (others => '0');
        rbh_state <= flush_mem;
      when flush_mem =>
        dprmB_wr_i <= '1';
        dprmB_addr_i <= dprmB_addr_i + 1;
        dprmB_data_i <= "10111010110100001101101010110000";
        if (dprmB_addr_i = "1111111111") then
          rbh_state <= idle;
        end if;  
      when idle =>
        dprmB_wr_i <= '0';
        dprmB_addr_i <= (others => '0');
        dprmB_data_i <= (others => '0');
        rb_count_i <= (others => '0');
        bin_count := (others => '0');
        if (rbh_enable_i = '1') then
          if (rbh_trigger_mode_i = '1') then
            rbh_state <= wait_for_trigger;
          else
            rbh_state <= rb_detect;
          end if;
        end if;
      when wait_for_trigger =>
        if (trigger_in = '1') then
          rbh_state <= rb_detect;
        end if;
      when rb_detect =>
        if(rod_busy_in = '1' AND rb_count_i /= 524287) then
          rb_count_i <= rb_count_i + 1;
        end if;
        if(std_logic_vector(bin_count) = "01111111111111111111") then
          dprmB_data_i(15 downto  0) <= std_logic_vector(rb_count_i(18 downto 3));
          rb_count_i  <= (others => '0');
        end if;          
        if(std_logic_vector(bin_count) = "11111111111111111111") then
          dprmB_data_i(31 downto 16) <= std_logic_vector(rb_count_i(18 downto 3));
          rb_count_i  <= (others => '0');
          rbh_state <= write_data;
        end if;          
        bin_count := bin_count + 1;
      when write_data =>
        dprmB_wr_i <= '1';
        if(rod_busy_in = '1') then
          rb_count_i <= rb_count_i + 1;
        end if;
        bin_count := (others => '0');
        rbh_state <= inc_dprm_addr;
      when inc_dprm_addr =>
        dprmB_wr_i <= '0';
        if(rod_busy_in = '1') then
          rb_count_i <= rb_count_i + 1;
        end if;
        bin_count := bin_count + 1;
        dprmB_addr_i <= dprmB_addr_i + '1';
        if(rbh_enable_i = '0') then
          rbh_state <= done;
        else
          rbh_state <= rb_detect;
        end if;
      when done =>
        dprmB_wr_i <= '0';
        dprmB_data_i <= (others => '0');
        rb_count_i <= (others => '0');
        rbh_state <= idle;
    end case;
  end if;
end process rod_busy_hsm;

end rtl; -- code of register_block
