--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   15:32:16 04/30/2013
-- Design Name:   
-- Module Name:   C:/ibl/ise_projects/ROD_revB_validation/PRM/prm/tp_rodPrm_top.vhd
-- Project Name:  prm
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rodPrm_top
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY tp_rodPrm_top IS
END tp_rodPrm_top;
 
ARCHITECTURE behavior OF tp_rodPrm_top IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rodPrm_top
    PORT(
         ck40_p : IN  std_logic;
         ck40_n : IN  std_logic;
         rst : IN  std_logic;
         main_tdi : IN  std_logic;
         main_tck : IN  std_logic;
         main_tms : IN  std_logic;
         main_tdo : OUT  std_logic;
         fpga_a_tdi : OUT  std_logic;
         fpga_a_tck : OUT  std_logic;
         fpga_a_tms : OUT  std_logic;
         fpga_a_tdo : IN  std_logic;
         fpga_b_tdi : OUT  std_logic;
         fpga_b_tck : OUT  std_logic;
         fpga_b_tms : OUT  std_logic;
         fpga_b_tdo : IN  std_logic;
         fpga_c_tdi : OUT  std_logic;
         fpga_c_tck : OUT  std_logic;
         fpga_c_tms : OUT  std_logic;
         fpga_c_tdo : IN  std_logic;
         mdsp_hd : INOUT  std_logic_vector(15 downto 0);
         mdsp_hrdy_n : IN  std_logic;
         mdsp_hhwil : OUT  std_logic;
         mdsp_hcntl0 : OUT  std_logic;
         mdsp_hcntl1 : OUT  std_logic;
         mdsp_hds1_n : OUT  std_logic;
         mdsp_hcs_n : OUT  std_logic;
         mdsp_hrnw : OUT  std_logic;
         rcf_hrdy_n : IN  std_logic;
         rcf_hcs_n : OUT  std_logic;
         mdsp_boot0 : OUT  std_logic;
         mdsp_boot1 : OUT  std_logic;
         mdsp_boot2 : OUT  std_logic;
         mdsp_boot3 : OUT  std_logic;
         mdsp_boot4 : OUT  std_logic;
         mdsp_rst_n : OUT  std_logic;
         reset_cmds_in : IN  std_logic_vector(1 downto 0);
         reset_cmds_out : OUT  std_logic_vector(1 downto 0);
         vme_data : INOUT  std_logic_vector(31 downto 0);
         vme_oen : OUT  std_logic;
         vme_dir : OUT  std_logic;
         vme_address : IN  std_logic_vector(31 downto 0);
         vme_am : IN  std_logic_vector(5 downto 0);
         vme_as : IN  std_logic;
         vme_dsa : IN  std_logic;
         vme_dsb : IN  std_logic;
         vme_write : IN  std_logic;
         vme_iack : IN  std_logic;
         vme_iackin : IN  std_logic;
         vme_iackout : OUT  std_logic;
         vme_sysfail : IN  std_logic;
         vme_dtack : OUT  std_logic;
         vme_berr : OUT  std_logic;
         vme_irq_n : IN  std_logic;
         vme_irq : IN  std_logic_vector(2 downto 0);
         serial_id : IN  std_logic_vector(7 downto 0);
         vme_ga_n : IN  std_logic_vector(4 downto 0);
         vme_gap : IN  std_logic;
         rod_busy_from_v5 : IN  std_logic;
         rod_busy : OUT  std_logic;
         reset_v5 : OUT  std_logic;
         prm_data : INOUT  std_logic_vector(31 downto 0);
         prm_ctrl_in : IN  std_logic_vector(2 downto 0);
         prm_ctrl_out : OUT  std_logic_vector(3 downto 0);
         prm_strobe0 : OUT  std_logic
        );
    END COMPONENT;
    

   --Inputs
   signal ck40_p : std_logic := '0';
   signal ck40_n : std_logic := '1';
   signal rst : std_logic := '1';
   signal main_tdi : std_logic := '0';
   signal main_tck : std_logic := '0';
   signal main_tms : std_logic := '0';
   signal fpga_a_tdo : std_logic := '0';
   signal fpga_b_tdo : std_logic := '0';
   signal fpga_c_tdo : std_logic := '0';
   signal mdsp_hrdy_n : std_logic := '0';
   signal rcf_hrdy_n : std_logic := '0';
   signal reset_cmds_in : std_logic_vector(1 downto 0) := (others => '0');
   signal vme_address : std_logic_vector(31 downto 0) := (others => '0');
   signal vme_am : std_logic_vector(5 downto 0) := (others => '0');
   signal vme_as : std_logic := '0';
   signal vme_dsa : std_logic := '0';
   signal vme_dsb : std_logic := '0';
   signal vme_write : std_logic := '0';
   signal vme_iack : std_logic := '0';
   signal vme_iackin : std_logic := '0';
   signal vme_sysfail : std_logic := '0';
   signal vme_irq_n : std_logic := '0';
   signal vme_irq : std_logic_vector(2 downto 0) := (others => '0');
   signal serial_id : std_logic_vector(7 downto 0) := (others => '0');
   signal vme_ga_n : std_logic_vector(4 downto 0) := (others => '0');
   signal vme_gap : std_logic := '0';
   signal rod_busy_from_v5 : std_logic := '0';
   signal prm_ctrl_in : std_logic_vector(2 downto 0) := (others => '0');

	--BiDirs
   signal mdsp_hd : std_logic_vector(15 downto 0);
   signal vme_data : std_logic_vector(31 downto 0);
   signal prm_data : std_logic_vector(31 downto 0);

 	--Outputs
   signal main_tdo : std_logic;
   signal fpga_a_tdi : std_logic;
   signal fpga_a_tck : std_logic;
   signal fpga_a_tms : std_logic;
   signal fpga_b_tdi : std_logic;
   signal fpga_b_tck : std_logic;
   signal fpga_b_tms : std_logic;
   signal fpga_c_tdi : std_logic;
   signal fpga_c_tck : std_logic;
   signal fpga_c_tms : std_logic;
   signal mdsp_hhwil : std_logic;
   signal mdsp_hcntl0 : std_logic;
   signal mdsp_hcntl1 : std_logic;
   signal mdsp_hds1_n : std_logic;
   signal mdsp_hcs_n : std_logic;
   signal mdsp_hrnw : std_logic;
   signal rcf_hcs_n : std_logic;
   signal mdsp_boot0 : std_logic;
   signal mdsp_boot1 : std_logic;
   signal mdsp_boot2 : std_logic;
   signal mdsp_boot3 : std_logic;
   signal mdsp_boot4 : std_logic;
   signal mdsp_rst_n : std_logic;
   signal reset_cmds_out : std_logic_vector(1 downto 0);
   signal vme_oen : std_logic;
   signal vme_dir : std_logic;
   signal vme_iackout : std_logic;
   signal vme_dtack : std_logic;
   signal vme_berr : std_logic;
   signal rod_busy : std_logic;
   signal reset_v5 : std_logic;
   signal prm_ctrl_out : std_logic_vector(3 downto 0);
   signal prm_strobe0 : std_logic;
   -- No clocks detected in port list. Replace <clock> below with 
   -- appropriate port name 
 
   constant PERIOD : time := 10 ns;
	signal control:std_logic;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: rodPrm_top PORT MAP (
          ck40_p => ck40_p,
          ck40_n => ck40_n,
          rst => rst,
          main_tdi => main_tdi,
          main_tck => main_tck,
          main_tms => main_tms,
          main_tdo => main_tdo,
          fpga_a_tdi => fpga_a_tdi,
          fpga_a_tck => fpga_a_tck,
          fpga_a_tms => fpga_a_tms,
          fpga_a_tdo => fpga_a_tdo,
          fpga_b_tdi => fpga_b_tdi,
          fpga_b_tck => fpga_b_tck,
          fpga_b_tms => fpga_b_tms,
          fpga_b_tdo => fpga_b_tdo,
          fpga_c_tdi => fpga_c_tdi,
          fpga_c_tck => fpga_c_tck,
          fpga_c_tms => fpga_c_tms,
          fpga_c_tdo => fpga_c_tdo,
          mdsp_hd => mdsp_hd,
          mdsp_hrdy_n => mdsp_hrdy_n,
          mdsp_hhwil => mdsp_hhwil,
          mdsp_hcntl0 => mdsp_hcntl0,
          mdsp_hcntl1 => mdsp_hcntl1,
          mdsp_hds1_n => mdsp_hds1_n,
          mdsp_hcs_n => mdsp_hcs_n,
          mdsp_hrnw => mdsp_hrnw,
          rcf_hrdy_n => rcf_hrdy_n,
          rcf_hcs_n => rcf_hcs_n,
          mdsp_boot0 => mdsp_boot0,
          mdsp_boot1 => mdsp_boot1,
          mdsp_boot2 => mdsp_boot2,
          mdsp_boot3 => mdsp_boot3,
          mdsp_boot4 => mdsp_boot4,
          mdsp_rst_n => mdsp_rst_n,
          reset_cmds_in => reset_cmds_in,
          reset_cmds_out => reset_cmds_out,
          vme_data => vme_data,
          vme_oen => vme_oen,
          vme_dir => vme_dir,
          vme_address => vme_address,
          vme_am => vme_am,
          vme_as => vme_as,
          vme_dsa => vme_dsa,
          vme_dsb => vme_dsb,
          vme_write => vme_write,
          vme_iack => vme_iack,
          vme_iackin => vme_iackin,
          vme_iackout => vme_iackout,
          vme_sysfail => vme_sysfail,
          vme_dtack => vme_dtack,
          vme_berr => vme_berr,
          vme_irq_n => vme_irq_n,
          vme_irq => vme_irq,
          serial_id => serial_id,
          vme_ga_n => vme_ga_n,
          vme_gap => vme_gap,
          rod_busy_from_v5 => rod_busy_from_v5,
          rod_busy => rod_busy,
          reset_v5 => reset_v5,
          prm_data => prm_data,
          prm_ctrl_in => prm_ctrl_in,
          prm_ctrl_out => prm_ctrl_out,
          prm_strobe0 => prm_strobe0
        );

   -- Clock process definitions
--   <clock>_process :process
--   begin
--		<clock> <= '0';
--		wait for <clock>_period/2;
--		<clock> <= '1';
--		wait for <clock>_period/2;
--   end process;
 
--ck40_p <= not (ck40_p and control) after PERIOD/2;
--ck40_n <= not (ck40_n) after PERIOD/2;


   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		--control<='0';
      wait for 1000 ns;
		while true loop
			ck40_p <= not (ck40_p);
			ck40_n <= not (ck40_n);
			wait for PERIOD;
		end loop;
		--control <='1';
      -- insert stimulus here 

      wait;
   end process;

END;
