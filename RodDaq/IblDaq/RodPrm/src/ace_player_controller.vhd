----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:25:54 06/06/2014 
-- Design Name: 
-- Module Name:    ace_player_controller - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ace_player_controller is
port (
    clk_in            : in  std_logic; -- vme_clk66 input
    rst_in          : in  std_logic; -- asynchronous global reset ACTIVE HIGH
    ace_cen_in       : in  std_logic; -- PPC CE_N signal in

    vme_ds_in        : in  std_logic; -- PPC Data Strobe (VME BE)

    vme_bus_rnw_in    : in  std_logic; -- VMEBus read/not_write signal
    vme_data_in   : in  std_logic_vector(31 downto 0); --
    par_data_out   : out std_logic_vector(31 downto 0); --
    data_wren   : out std_logic; --
    vme_ack_n_out   : out std_logic; -- VMEBus LACK_N 
    vme_berr_out      : out std_logic; -- VMEBus BERR

    vme_timeout_in    : in  std_logic_vector(31 downto 0)
    );
end ace_player_controller;

architecture Behavioral of ace_player_controller is

signal ace_bus_strb_i          : std_logic;
signal ace_bus_strb_ii         : std_logic;
signal ace2player_wren_i       : std_logic;
signal ace_bus_ack_i           : std_logic;


type ace_access_states is (
  idle,
  write,
  wr_hold,
  read,
  rd_hold,
  done
  );
signal ace_access : ace_access_states;




begin


par_data_out <= vme_data_in;
data_wren <= ace2player_wren_i;
vme_ack_n_out <= NOT ace_bus_ack_i;

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

w_ace_process : process(clk_in, rst_in)

variable timeout_count : unsigned(31 downto 0);

begin
  if(rst_in = '1') then
    ace_bus_ack_i         <= '0';
    ace_bus_strb_i          <= '0';
    ace_access              <= idle;
	 timeout_count     := (others => '0');
	 ace2player_wren_i <= '0';
	 ace_bus_strb_ii <= '0';
  elsif(rising_edge(clk_in)) then
    ace_bus_strb_i  		<= vme_ds_in;
    ace_bus_strb_ii 		<= ace_bus_strb_i;
 
    case ace_access is
      when idle =>
        ace_bus_ack_i  <= '0';
		  ace2player_wren_i <= '0';
 
        if (ace_cen_in = '1' AND ace_bus_strb_i = '1') then
          if (vme_bus_rnw_in = '1') then
            ace_access <= read;
          else
            ace_access <= write;
          end if;
        end if;

      when read   =>   -- read registers
 
        ace_access <= rd_hold;

      when rd_hold => 
 
        ace_access <= done;

      when write =>  -- write registers
      
        ace_access <= wr_hold;
		  ace2player_wren_i <= '1';

      when wr_hold => 
        ace_access <= done;
		  ace2player_wren_i <= '0';

      when done =>
 
        ace_bus_ack_i <= '1';
        if(ace_bus_strb_i = '0' AND ace_bus_strb_ii = '0') then
          ace_access <= idle;
        end if;
               
      when others => 

        ace_access <= idle;
    end case;
	 	 
	 if(vme_ds_in = '1') then
      if(timeout_count = unsigned(vme_timeout_in)) then
        vme_berr_out  <= '1';
        ace_access <= done;
      else
        timeout_count := timeout_count + 1 ;
      end if;
    else
      vme_berr_out  <= '0';
      timeout_count := (others => '0');
    end if;
	 
  end if;
end process w_ace_process;



end Behavioral;

