-------------------------------------------------------------------------------
-- Particle Physics Detector Electronics Support
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
-------------------------------------------------------------------------------
-- Filename: Reset Command Controller
-- Description:
--
--
--
-------------------------------------------------------------------------------
-- Structure:
-------------------------------------------------------------------------------
-- Timing:
--    *The Engine FPGA runs at 40MHz => clk40
--    *The timing structure of all engine code will be:
--      1.  Perform all logic operations on positive edge of clk40
--      2.  Perform all memory operations on positive edge of clk40
--   
-------------------------------------------------------------------------------
-- Author:         John Joseph
-- Board Engineer:
-- History:
--    12/09/02 JMJ First version
--    
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- LIBRARY INCLUDES
-------------------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations 
-------------------------------------------------------------------------------
-- SIMULATION LIBRARY INCLUDES
-------------------------------------------------------------------------------

entity reset_command_controller is
  port (
    clk40_in           : in  std_logic;
    rst_n_in           : in  std_logic;
    clr_reset_cmd_in   : in  std_logic;
    rod_busy_in        : in  std_logic;
    reset_commands_in  : in  std_logic_vector(1 downto 0);
    reset_commands_out : out std_logic_vector(1 downto 0);
    reset_sdsp_out     : out std_logic_vector(3 downto 0);
    trigger_out        : out std_logic
    );
end reset_command_controller;

architecture RTL of reset_command_controller is
-------------------------------------------------------------------------------
-- SIGNAL DECLARATION
-------------------------------------------------------------------------------

type counter_states is (
  clear,
  count
  );
signal counter_state : counter_states;

type ser_decode_states is (
  idle,
  start_counter,
  wait_for_pulse,
  capture_pulse,
  cmd_rcvd,
  send_reset,
  post_reset_delay,
  done
  );
signal ser_decode_state : ser_decode_states;

signal reset_cmds_i     : std_logic_vector(1 downto 0);
signal reset_cmd1_sh    : std_logic_vector(2 downto 0);
signal reset_cmd_busy_i : std_logic;
signal decode_count_i   : unsigned(8 downto 0);
signal pulse_count_i    : unsigned(2 downto 0);
signal clr_reset_cmd_i  : std_logic;

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------
begin

-------------------------------------------------------------------------------
-- Signal Register
-------------------------------------------------------------------------------
signal_register : process(rst_n_in, clk40_in)
begin
  if(rst_n_in = '0') then
    reset_cmds_i       <= (others => '0');
    reset_cmd1_sh      <= (others => '0');
    reset_commands_out <= (others => '0');
    clr_reset_cmd_i    <= '0';
  elsif(rising_edge(clk40_in)) then
    reset_cmds_i       <= reset_commands_in;
    reset_commands_out <= rod_busy_in & reset_cmd_busy_i;
    clr_reset_cmd_i    <= clr_reset_cmd_in;
    reset_cmd1_sh      <= reset_cmd1_sh(1 downto 0) & reset_commands_in(1);
  end if;
end process;

-------------------------------------------------------------------------------
-- PROCESS to decode the serial input stream
-------------------------------------------------------------------------------
trigger_decoder_process: process(rst_n_in, clk40_in)
begin
  if(rst_n_in = '0') then
    trigger_out <= '0';
  elsif(rising_edge(clk40_in)) then
    if(reset_cmd1_sh = "010") then
      trigger_out <= '1';
    else
      trigger_out <= '0';
    end if;
  end if;
end process trigger_decoder_process;

rst_cmd_serial_decoder_process: process(rst_n_in, clk40_in)
begin
  if(rst_n_in = '0') then
    reset_cmd_busy_i <= '0';
    pulse_count_i    <= (others => '0');
    reset_sdsp_out   <= (others => '0');
    ser_decode_state <= idle;
    counter_state    <= clear;
  elsif(rising_edge(clk40_in)) then
    case ser_decode_state is
      when idle =>
        reset_cmd_busy_i <= '0';
        pulse_count_i    <= (others => '0');
        reset_sdsp_out   <= (others => '0');
        if(reset_cmds_i(0) = '1') then
          reset_cmd_busy_i <= '1';
          ser_decode_state <= start_counter;
        end if;

      when start_counter =>
        counter_state    <= count;
        ser_decode_state <= wait_for_pulse;

      when wait_for_pulse =>
        if(reset_cmds_i(0) = '0') then
          ser_decode_state <= capture_pulse;
        end if;
          
      when capture_pulse =>
        if(decode_count_i > 250) then  -- 250 40MHz clocks => 6.25 us (it was 50 us before!)
          counter_state    <= clear;
          ser_decode_state <= cmd_rcvd;
        elsif(reset_cmds_i(0) = '1') then
          pulse_count_i    <= pulse_count_i + 1 ;
          ser_decode_state <= wait_for_pulse;
        end if;

      when cmd_rcvd =>
        case pulse_count_i is
          when "001"  => reset_sdsp_out <= "0001";
          when "010"  => reset_sdsp_out <= "0010";
          when "011"  => reset_sdsp_out <= "0100";
          when "100"  => reset_sdsp_out <= "1000";
          when others => reset_sdsp_out <= (others => '0');
                         pulse_count_i  <= (others => '0');
        end case;
        ser_decode_state <= send_reset;

      when send_reset =>
        if (clr_reset_cmd_i = '1' OR pulse_count_i = 0) then
          pulse_count_i    <= (others => '0');
          reset_sdsp_out   <= (others => '0');
          ser_decode_state <= post_reset_delay;
        end if;

      when post_reset_delay =>
        counter_state <= count;
        if (decode_count_i > 500) then  -- 500 40MHz clocks => 12.5us (it was 100 us before!)
          ser_decode_state <= done;
        end if;

      when done   => 
        reset_cmd_busy_i <= '0';
        counter_state    <= clear;
        ser_decode_state <= idle;
    end case;
  end if;
end process rst_cmd_serial_decoder_process;

-------------------------------------------------------------------------------
-- Process Counter
-------------------------------------------------------------------------------
decode_counter : process(rst_n_in, clk40_in)
begin
  if(rst_n_in = '0') then
    decode_count_i <= (others => '0');
  elsif(rising_edge(clk40_in)) then
    case counter_state is
      when count  => decode_count_i <= decode_count_i + 1;
      when others => decode_count_i <= (others => '0');
    end case;
  end if;
end process; 

end RTL;
