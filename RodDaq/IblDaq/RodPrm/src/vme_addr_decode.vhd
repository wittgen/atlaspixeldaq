--------------------------------------------------------------------------------
--
-- Particle Physics Detector Electronics Support
-- University of Wisconsin
-- Lawrence Berkeley National Laboratory (c) 2000
-- ReadOutDriver Electronics
--
--------------------------------------------------------------------------------
--
-- Filename: vme_addr_decode.vhd 
-- Description:
--  The VMEbus Address Decoder Block is implemented in the PRM FPGA and decodes
--  the ROD memory map into Regions as required by the VMEBus Interface.
--  The ROD uses the CY7C960A as a slave controller in I/O mode.
--
--  Memory Map and Regions
--
--  The ROD uses 32 bits of address LA(31:0).
--    LA(31:24) are compared to the Geographical Address GA(4:0) to select the
--    active ROD board.  The Rods occupy slots 5 - 12 and 14 - 21, and because
--    the Geographical Address bits are inverted on the backplane the values at
--    the input to the VME Address Decoder Block are as follows:
--        Slot 5  -> GA(4:0) = "11010"  -> NOT GA(4:0) = "00101"
--        Slot 6  -> GA(4:0) = "11001"  -> NOT GA(4:0) = "00110"
--        Slot 7  -> GA(4:0) = "11000"  -> NOT GA(4:0) = "00111"
--        Slot 8  -> GA(4:0) = "10111"  -> NOT GA(4:0) = "01000"
--        Slot 9  -> GA(4:0) = "10110"  -> NOT GA(4:0) = "01001"
--        Slot 10 -> GA(4:0) = "10101"  -> NOT GA(4:0) = "01010"
--        Slot 11 -> GA(4:0) = "10100"  -> NOT GA(4:0) = "01011"
--        Slot 12 -> GA(4:0) = "10011"  -> NOT GA(4:0) = "01100"
--        Slot 13 -> TIM
--        Slot 14 -> GA(4:0) = "10001"  -> NOT GA(4:0) = "01110"
--        Slot 15 -> GA(4:0) = "10000"  -> NOT GA(4:0) = "01111"
--        Slot 16 -> GA(4:0) = "01111"  -> NOT GA(4:0) = "10000"
--        Slot 17 -> GA(4:0) = "01110"  -> NOT GA(4:0) = "10001"
--        Slot 18 -> GA(4:0) = "01101"  -> NOT GA(4:0) = "10010"
--        Slot 19 -> GA(4:0) = "01100"  -> NOT GA(4:0) = "10011"
--        Slot 20 -> GA(4:0) = "01011"  -> NOT GA(4:0) = "10100"
--        Slot 21 -> GA(4:0) = "01010"  -> NOT GA(4:0) = "10101"
--
--    The equation for selecting the active ROD and decoding a valid Region Map:
--    LA(31:24) = "000" & NOT GA(4:0)
--
--    If the matching equation is true, then LA(23:16) is used to decode the 
--    function specific ROD Region Map, and the valid map is returned to the 
--    VMEBus Interface IC (CY7C960A).
--
--                          ROD Memory Map to Region Map
-- REGION(2:0) = "000"
-- rod_master_dsp_host_port - CS_N(1) 
--  ba0xxxxx  hpi control register
--  ba2xxxxx  hpi address register
--
-- REGION(2:0) = "001"
-- rod_master_dsp_host_port - CS_N(1) 
--  ba4xxxxx  hpi data register with auto_incr
--  ba6xxxxx  hpi data register without auto_incr
--
-- REGION(2:0) = "010"
-- fpga_program_reset_manager - CS_N(2)
--  bacxxxxx  prm  registers

-- REGION(2:0) = "011" - RCF HPI Port

-- REGION(2:0) = "100" - NOP
-- REGION(2:0) = "101" - NOP
-- REGION(2:0) = "110" - NOP
-- REGION(2:0) = "111" - NOP
--
---------------------------------------------------------------------------------
-- LIBRARY INCLUDES
---------------------------------------------------------------------------------
--
library IEEE;
use IEEE.std_logic_1164.all;  -- needed for logic operations
use IEEE.std_logic_arith.all; -- needed for +/- operations
--
--------------------------------------------------------------------------------
-- PORT DECLARATION
--------------------------------------------------------------------------------
--
entity vme_address_decoder is
  port (
    clk_in          : in  std_logic;  -- VMEBus clk (60MHz)
    rst_n_in        : in  std_logic;  -- asynchronous reset (active low)
    rod_addr_n_in   : in  std_logic_vector( 4 downto 0); -- GA bits must be inv
    gaddr_par_n_in  : in  std_logic;  --vme_gap_n_p
    vme_addr_in     : in  std_logic_vector(31 downto 0); -- VMEBus Addr (31:20)
    vme_am_in       : in  std_logic_vector( 5 downto 0);
    vme_as_in       : in  std_logic;
    vme_ds0_in      : in  std_logic;
    vme_ds1_in      : in  std_logic;
    prm_cen_out     : out std_logic;  -- PRM Register Block CS
    mdsp_cen_out    : out std_logic;  -- MDSP HPI State Machine Enable
    rcf_cen_out     : out std_logic;  -- RCF HPI State Machine Enable
	 ppc_cen_out     : out std_logic;  -- PPC HPI State Machine Enable --RT
	 ace_cen_out     : out std_logic;  -- ACE player Data Input Enable --RT
    ill_match_out   : out std_logic;  -- Address Match is true, but the decode region is false
    data_strb_out   : out std_logic;   -- VME DS signal
    vme_am_out      : out std_logic_vector( 5 downto 0)
    );
end vme_address_decoder;

architecture rtl of vme_address_decoder is
--------------------------------------------------------------------------------
-- SIGNAL DECLARATION
--------------------------------------------------------------------------------

signal vme_addr_i     : std_logic_vector(31 downto 0);
signal geo_addr_i     : std_logic_vector( 7 downto 0);
signal vme_am_i       : std_logic_vector( 5 downto 0);
signal vme_as_i       : std_logic;
signal mdsp_cen_i     : std_logic;
signal prm_cen_i      : std_logic;
signal rcf_cen_i      : std_logic;
signal ppc_cen_i      : std_logic; --RT
signal ace_cen_i      : std_logic; --RT
signal ill_match_i    : std_logic;
signal geo_addr_par_i : std_logic;
signal ga_par_i       : std_logic;


begin
--------------------------------------------------------------------------------
-- SIGNAL ASSIGNMENT
--------------------------------------------------------------------------------
geo_addr_i     <= "000" & NOT rod_addr_n_in;
geo_addr_par_i <= NOT gaddr_par_n_in;
mdsp_cen_out   <= mdsp_cen_i;
prm_cen_out    <= prm_cen_i;
rcf_cen_out    <= rcf_cen_i;
ill_match_out  <= ill_match_i;
ppc_cen_out    <= ppc_cen_i; --RT
ace_cen_out    <= ace_cen_i; --RT

--------------------------------------------------------------------------------
-- PROCESS DECLARATION
--------------------------------------------------------------------------------
vme_am_latch : process (
  rst_n_in, 
  vme_as_in, 
  vme_addr_in
  )
begin
  if (rst_n_in = '0') then
    vme_am_i <= (others => '0');
    vme_addr_i <= (others => '0');
  elsif (vme_as_in'event AND vme_as_in = '1') then
    vme_am_i <= vme_am_in;
    vme_addr_i <= vme_addr_in;
  end if;
end process vme_am_latch;

vme_region_map_decoder : process (
  rst_n_in, 
  clk_in
  )
begin
  if (rst_n_in = '0') then
    mdsp_cen_i  <= '0';
    prm_cen_i   <= '0';
    rcf_cen_i   <= '0';
	 ppc_cen_i   <= '0'; --RT
 	 ace_cen_i   <= '0'; --RT
    ill_match_i <= '0';
    vme_as_i    <= '0';
    vme_am_out  <= (others => '0');
  elsif (clk_in'event AND clk_in = '1') then
    vme_as_i <= vme_as_in;
    if (vme_as_i = '1') then
      vme_am_out <= vme_am_i;
      if (geo_addr_i = vme_addr_i(31 downto 24)) then -- VMEBus Addr (31:24)
--      if ((vme_am_i = "001001" OR vme_am_i = "001011") AND vme_addr_i(0) = '0') then  -- VME AM Codse 0x9 & 0xB and LongWord = 0
		  if  (vme_am_i = "001001" OR vme_am_i = "001011") then  -- VME AM Codes 0x9 & 0xB 
          case vme_addr_i(23 downto 20) is  -- VMEBus Addr (23:20)
            when "0000" => mdsp_cen_i  <= '1'; -- MDSP HPIC Port
            when "0010" => mdsp_cen_i  <= '1'; -- MDSP HPIA Port
            when "0100" => mdsp_cen_i  <= '1'; -- MDSP HPID Port
            when "0110" => mdsp_cen_i  <= '1'; -- MDSP HPID Port
				--RT
				when "0001" => ppc_cen_i  <= '1'; -- PPC  HPIC Port
            when "0011" => ppc_cen_i  <= '1'; -- PPC HPIA Port
            when "0101" => ppc_cen_i  <= '1'; -- PPC HPID Port
            when "0111" => ppc_cen_i  <= '1'; -- PPC HPID Port
				--end RT
            when "1000" => rcf_cen_i   <= '1'; -- RCF  HPI Port
            when "1100" => prm_cen_i   <= '1'; -- FPGA PRM Registers
				when "1101" => ace_cen_i   <= '1'; -- ACE player Data Port
            when others => mdsp_cen_i  <= '0';
                           prm_cen_i   <= '0';
                           rcf_cen_i   <= '0';
									ppc_cen_i   <= '0'; --RT
  								   ace_cen_i   <= '0'; --RT
                           ill_match_i <= '1';
          end case;
        else
          mdsp_cen_i  <= '0';
          prm_cen_i   <= '0';
          rcf_cen_i   <= '0';
			 ppc_cen_i   <= '0'; --RT
  		    ace_cen_i   <= '0'; --RT
          ill_match_i <= '1';
        end if;
      else
        mdsp_cen_i  <= '0';
        prm_cen_i   <= '0';
        rcf_cen_i   <= '0';
		  ppc_cen_i   <= '0'; --RT
 		  ace_cen_i   <= '0'; --RT
        ill_match_i <= '0';
      end if;
    else
      mdsp_cen_i  <= '0';
      prm_cen_i   <= '0';
      rcf_cen_i   <= '0';
		ppc_cen_i   <= '0'; --RT
 		ace_cen_i   <= '0'; --RT
      ill_match_i <= '0';
      vme_am_out  <= (others => '0');
    end if;
  end if;
end process vme_region_map_decoder;

vme_ds_decoder : process (
  rst_n_in, 
  clk_in
  )
begin
  if (rst_n_in = '0') then
    data_strb_out <= '0';
  elsif (clk_in'event AND clk_in = '1') then
--RT    if (mdsp_cen_i = '1' OR rcf_cen_i = '1' OR prm_cen_i = '1') then
--RT    if (mdsp_cen_i = '1' OR rcf_cen_i = '1' OR prm_cen_i = '1' OR ppc_cen_i = '1') then
	if (mdsp_cen_i = '1' OR rcf_cen_i = '1' OR prm_cen_i = '1' OR ppc_cen_i = '1' OR ace_cen_i = '1') then
       data_strb_out <= vme_ds0_in AND vme_ds1_in;
    else
      data_strb_out <= '0';
    end if;
  end if;
end process;

end rtl;
