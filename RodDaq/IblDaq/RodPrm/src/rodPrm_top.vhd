------------------------------------------------------------------------------
--  Lawrence Berkeley National Laboratory
-------------------------------------------------------------------------------
--  Filename: top.vhd
--  Title: FPGA Program Reset Manager
--  Description:    This FPGA controls configuration of all FPGA parts, and
--                  controls the reset signals to all FPGAs and DSPs on the 
--                  Read Out Driver Board used in Atlas. 
--		
--  Constraints required to that define the location of the FPGA PRM global
--  clocks and DLLs, and are used to properly synthesize the prototype ver:

-------------------------------------------------------------------------------
-- Revision History
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------
--  Library Declaration
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;         
use IEEE.std_logic_arith.all;     
use ieee.numeric_std.all;
use IEEE.STD_LOGIC_unsigned.ALL;

library UNISIM;
use UNISIM.vcomponents.all;

-- git id
use work.gitIdPack.all;

-------------------------------------------------------------------------------
--  PORT DECLARATION
-------------------------------------------------------------------------------

entity rodPrm_top is 
port (
-- clock and reset ports
  ck40_p      			: in  std_logic;	
  ck40_n      			: in  std_logic;	  
  rst         			: in  std_logic;	-- active LOW
  vme_sysreset			: in  std_logic;	-- active LOW

-- JTAG signals for accessing the other FPGAs via J9
  main_tdi 				: in  std_logic;
  main_tck 				: in  std_logic;
  main_tms 				: in  std_logic;
  main_tdo 				: out std_logic;
			  
  fpga_a_tdi 			: out  std_logic;
  fpga_a_tck 			: out  std_logic;
  fpga_a_tms 			: out  std_logic;
  fpga_a_tdo 			: in   std_logic;
			  
  fpga_b_tdi 			: out  std_logic;
  fpga_b_tck 			: out  std_logic;
  fpga_b_tms 			: out  std_logic;
  fpga_b_tdo 			: in   std_logic;
			  
  fpga_c_tdi 			: out  std_logic;
  fpga_c_tck 			: out  std_logic;
  fpga_c_tms 			: out  std_logic;
  fpga_c_tdo 			: in   std_logic;
        
-- MDSP I/O and Reset command ports
  mdsp_hd            : inout std_logic_vector(15 downto 0);
  mdsp_hrdy_n        : in    std_logic;  -- HRDY from the Master DSP
  mdsp_hhwil         : out   std_logic;  -- HHWIL to the Master DSP
  mdsp_hcntl0        : out   std_logic;  -- HCNTL0 to the Master DSP
  mdsp_hcntl1        : out   std_logic;  -- HCNTL1 to the Master DSP
  mdsp_hds1_n        : out   std_logic;  -- HDS1_N to the Master DSP
  mdsp_hcs_n         : out   std_logic;  -- HCS_N to the Master DSP
  mdsp_hrnw          : out   std_logic;  -- HRNW to the Master DSP
--  rcf_hrdy_n         : in    std_logic;  -- HRDY from the RCF
--  rcf_hcs_n          : out   std_logic;  -- HCS_N to the RCF
  
  mdsp_boot0         : out   std_logic;
  mdsp_boot1         : out   std_logic;
  mdsp_boot2         : out   std_logic;
  mdsp_boot3         : out   std_logic;
  mdsp_boot4         : out   std_logic;

  mdsp_rst_n         : out   std_logic;

-- RCF Reset command ports
  reset_cmds_in      : in    std_logic_vector(1 downto 0);  -- reset cmd from V5
  reset_cmds_out     : out   std_logic_vector(1 downto 0);  -- acknowledge and clear reset cmd to V5

-- vme interface ports
  vme_data           : inout std_logic_vector(31 downto 0);                
  vme_oen            : out   std_logic;
  vme_dir            : out   std_logic;
  vme_address        : in    std_logic_vector(31 downto 0);
  vme_am             : in    std_logic_vector( 5 downto 0);
  vme_as             : in    std_logic;
  vme_dsa            : in    std_logic;
  vme_dsb            : in    std_logic;
  vme_write          : in    std_logic;
  vme_iack           : in    std_logic;
  vme_iackin         : in    std_logic;  
  vme_iackout        : out   std_logic;  
  vme_sysfail        : in    std_logic;
  vme_dtack          : out   std_logic;
  vme_berr           : out   std_logic;
  vme_irq_n          : in    std_logic;  
  vme_irq            : in    std_logic_vector(2 downto 0);  
  serial_id          : in    std_logic_vector(7 downto 0);
  vme_ga_n 				: in    std_logic_vector(4 downto 0);
  vme_gap            : in    std_logic;  -- parity bit for Geographical Addr

-- ROD Status
  rod_busy_from_v5   : in    std_logic;
  rod_busy           : out   std_logic;
  reset_v5       		: out   std_logic;
  --RT V5 hpi port
	 -- NB: in and out direction below is MASTER's view!
	 -- prm_ctrl_out 2..0 and ctrl_from_prm 3..0 used by hpi port
	 -- ctrl_in: [0] data_valid -- [1]done -- [2]busy
	 -- AKU
    -- prm spi for slot number uses ctrl_in 5..3 and ctrl_from_prm 4
	 -- prm_ctrl_in(6) is watchdog reset from master
  -- AKU: with XVC Jtag
	 -- prm_ctl_in(7..9) = tck, tms, tdi
	 -- prm_ctl_out(5) = tdo
  prm_data                              : inout std_logic_vector(31 downto 0);
  prm_ctrl_in                           : in    std_logic_vector(9 downto 0); 
  prm_ctrl_out                          : out   std_logic_vector(5 downto 0);
  prm_ga_out			        : out		std_logic_vector(4 downto 0);
  reset_S6A_from_prm                    : out   std_logic;
  reset_S6B_from_prm                    : out   std_logic;
  reset_processor_core_select           : out   std_logic;
  prm_strobe0		                : out   std_logic 
  --end RT
  );
end rodPrm_top;

architecture RTL of rodPrm_top is

-------------------------------------------------------------------------------
--SIGNAL DECLARATION
-------------------------------------------------------------------------------

-- unused rcf signals
signal rcf_hrdy_n         :     std_logic;  -- HRDY from the RCF
signal rcf_hcs_n          :    std_logic;  -- HCS_N to the RCF


-- internal clock signals
signal ck: 							std_logic;
signal locked:						std_logic;
signal rst_n:						std_logic;

--  internal reset cmd interface signals
signal mdsp_hpi_data_in_i: 	std_logic_vector(15 downto 0);
signal mdsp_hpi_data_out_i: 	std_logic_vector(15 downto 0);
signal mdsp_hpi_data_dir_i: 	std_logic;
signal mdsp_cen_n_i: 			std_logic;
signal mdsp_cen_i: 				std_logic;
signal mdsp_rnw_i: 				std_logic;
signal mdsp_hrdy_n_i: 			std_logic;
signal mdsp_hhwil_i: 			std_logic;
signal mdsp_hcntl0_i: 			std_logic;
signal mdsp_hcntl1_i: 			std_logic;
signal mdsp_hds1_n_i: 			std_logic;
--signal rcf_hrdy_n_i: 			std_logic;
signal rcf_cen_i: 				std_logic;
signal rcf_cen_n_i: 				std_logic;

--RT begin
signal ppc_cen_i: 				std_logic;
signal vme_ppc_do_i: 			std_logic_vector(31 downto 0);
signal ppc_hpi_data_dir_i:    std_logic; 
signal ppc_hpi_data_in_i: 		std_logic_vector(31 downto 0);
signal ppc_hpi_data_out_i: 	std_logic_vector(31 downto 0);
signal ppc_hpi_busy_i: 			std_logic;
signal ppc_hpi_done_i: 			std_logic;
signal ppc_dtack_n_i:			std_logic;
signal ppc_berr_i:				std_logic;
signal hpi_ppc_command_i:     std_logic_vector(3 downto 0);
signal hpi_strobe_i:				std_logic;

signal ace_cen_i: 				std_logic;
signal ace_dtack_n_i:			std_logic;
signal ace_berr_i:				std_logic;

-- end RT
    
signal reset_cmds_in_i: 		std_logic_vector(1 downto 0);
signal reset_cmds_out_i: 		std_logic_vector(1 downto 0);

signal data_strb_i: 				std_logic;
   
--  internal vmebus signals
signal vme_data_out_p: 			std_logic_vector(31 downto 0);
signal vme_data_in_p: 			std_logic_vector(31 downto 0);
signal vme_prm_out_i: 			std_logic_vector(31 downto 0);
signal vme_hpi_do_i: 			std_logic_vector(31 downto 0);
signal vme_addr_i: 				std_logic_vector(31 downto 0);
signal vme_data_dir_i: 			std_logic;
signal ga_n_i: 					std_logic_vector( 4 downto 0);
signal vme_timeout_i: 			std_logic_vector(31 downto 0);
signal vme_rnw_i: 				std_logic;
signal vme_am_code_i: 			std_logic_vector( 5 downto 0);
signal vme_iack_i: 				std_logic;
signal lvme_iack_i: 				std_logic;

signal vme_de_n_p: 				std_logic;
signal vme_ddir_p: 				std_logic;
signal vme_am_p: 					std_logic_vector( 5 downto 0);
signal vme_as_p: 					std_logic;
signal vme_as_i: 					std_logic;
signal vme_ds_i: 					std_logic;
signal vme_ds0_p: 				std_logic;
signal vme_ds1_p: 				std_logic;
signal vme_wr_p: 					std_logic;
signal vme_iack_p: 				std_logic;
signal vme_iackin_n_p: 			std_logic;
signal vme_iackout_n_p: 		std_logic;
signal vme_sysfail_i: 			std_logic;
signal vme_dtack_n_p: 			std_logic;
signal vme_berr_n_p: 			std_logic;
signal lirq_i: 					std_logic;
signal lirq_val_p: 				std_logic_vector( 2 downto 0);
signal vme_gap_n_i: 				std_logic := '0';  -- parity bit for Geographical Addr
                       
--  program reset manager bus signals
signal prm_cen_i: 				std_logic;
                                                
--  internal dsp
signal dsp_rst_n_out_i: 		std_logic_vector(4 downto 0);

--  internal configuration and status signals
signal fpga_rst_status_reg_i: std_logic_vector(4 downto 0);
signal dsp_rst_status_reg_i: 	std_logic_vector(4 downto 0);

--  internal control signals (the names should define)
signal fpga_rst_reg_i: 			std_logic_vector( 7 downto 0);
signal dsp_rst_reg_i: 			std_logic_vector( 6 downto 0);
signal reset_dsp_i: 				std_logic_vector( 5 downto 0);
signal dsp_reset_cmd_i: 		std_logic_vector( 3 downto 0);
signal clr_rst_reg_i: 			std_logic;

signal rod_busy_n_p: 			std_logic;
signal rod_busy_i: 				std_logic;
signal rod_busy_n_i: 			std_logic;

signal ill_match_i: 				std_logic;  -- illegal decode region
signal hpi_dtack_n_i: 			std_logic;
signal hpi_berr_i: 				std_logic;
signal prb_bus_ack_i: 			std_logic;
signal rodresio_rst_n_out_i: 	std_logic;
signal formA_rst_n_out_i: 		std_logic;	
signal formB_rst_n_out_i:		std_logic;
signal efb_rst_n_out_i:			std_logic;
signal router_rst_n_out_i:		std_logic;

signal trigger_i: 				std_logic;

signal control_chipscope:		std_logic_vector(35 downto 0);
signal trig0:						std_logic_vector(31 downto 0);
signal trig1:						std_logic_vector(63 downto 0);
signal trig2:						std_logic_vector(15 downto 0);
signal trig3:						std_logic_vector(31 downto 0);
signal trig4:						std_logic_vector(41 downto 0); --RT
signal trig5:						std_logic_vector(63 downto 0); --RT
signal rst_dcm:					std_logic;

signal mdsp_boot4_i:			std_logic;
signal mdsp_boot3_i:			std_logic;
signal mdsp_boot2_i:			std_logic;
signal mdsp_boot1_i:			std_logic;
signal mdsp_boot0_i:			std_logic;

signal tdi_int: 				std_logic;
signal tms_int: 				std_logic;
signal tck_int: 				std_logic;
signal tdo_int_a: 			std_logic;
signal tdo_int_b: 			std_logic;
signal tdo_int_c: 			std_logic;

signal vme_dtack_delay0:	std_logic;
signal hpi_state_debug:		std_logic_vector(4 downto 0);

signal fpga_config_write_i: std_logic_vector(31 downto 0);
signal fpga_config_read_i:  std_logic_vector(31 downto 0);

-- synchronous spi from master
signal spi_miso, spi_sck_r0, spi_sck_r1, spi_sck_r: std_logic;
signal spi_ctl_mode : std_logic_vector(3 downto 0) := (others => '0');
signal spi_ctl_mode_r : std_logic_vector(3 downto 0);
signal spi_sck_buf: std_logic;
signal spi_ss_buf, spi_ss_buf0: std_logic;

constant useSpiClk: boolean := true; -- whether we use the spi clock directly. needs proper pin mapping

signal prmInfoWord: std_logic_vector(gitIdLength + 32 - 1 downto 0);
signal prmInfoIndex: integer range prmInfoWord'length downto 0;

signal master_if_clk: std_logic;


-- ----------- prm control out ----------
-- hpi cmd out is 4 bits
constant ctl_from_prm_hpi_cmd_lo_bit: integer := 0;
constant ctl_from_prm_hpi_cmd_hi_bit: integer := 3;
-- spi bits
constant ctl_from_prm_spi_miso_bit: integer := 4; -- needs to be swapped with in 4 if spi-sck used as clock
--xvc bits
constant ctl_from_prm_xvc_tdo_bit: integer := 5;

-- ----------- prm control in ----------
-- hpi cmd in is 3 bits, but bit 0 is unused
constant ctl_to_prm_hpi_valid_bit: integer := 0; -- unused
constant ctl_to_prm_hpi_done_bit: integer := 1;
constant ctl_to_prm_hpi_busy_bit: integer := 2;
-- spi bits
constant ctl_to_prm_spi_ss_bit: integer := 3;
constant ctl_to_prm_spi_sck_bit: integer := 4;   -- needs to be swapped with out 4 if used as clock
constant ctl_to_prm_spi_mosi_bit: integer := 5; 
--watchdog bit
constant ctl_to_prm_wdog_bit: integer := 6;
--xvc bits
constant ctl_to_prm_xvc_tck_bit: integer := 7;
constant ctl_to_prm_xvc_tms_bit: integer := 8;
constant ctl_to_prm_xvc_tdi_bit: integer := 9;

-- master reset monitor
signal mst_reset_r0, mst_reset_r1, mst_reset_r: std_logic;
signal mst_reset_cnt: std_logic_vector(7 downto 0);
signal mst_reprog: std_logic;

-- XVC JTAG
  -- AKU: with XVC Jtag
	 -- prm_ctl_in(9..7) = tck, tms, tdi
	 -- prm_ctl_out(5) = tdo
signal xvc_tck, xvc_tms, xvc_tdi, xvc_tdo, xvc_enable : std_logic;


-------------------------------------------------------------------------------
--COMPONENT DECLARATION
-------------------------------------------------------------------------------

--  The register block preforms all register function required by the FPGA PRM
component register_block
  port (
    clk_in                     : in  std_logic; -- clk input
    rst_n_in                   : in  std_logic; -- async global reset
	 
    prb_bus_strb_in            : in  std_logic;
    prb_bus_ce_in              : in  std_logic;
    prb_bus_rnw_in             : in  std_logic;
    prb_bus_addr_in            : in  std_logic_vector(31 downto 0);
    prb_bus_data_in            : in  std_logic_vector(31 downto 0);
    prb_bus_data_out           : out std_logic_vector(31 downto 0);
    prb_bus_ack_out            : out std_logic;
	  
    prb_fpga_rst_status_reg_in : in  std_logic_vector( 4 downto 0);
    prb_dsp_rst_status_reg_in  : in  std_logic_vector( 4 downto 0);
    fpga_rst_reg_out           : out std_logic_vector( 7 downto 0);
    dsp_rst_reg_out            : out std_logic_vector( 6 downto 0);
	 fpga_config_write_out		 : out std_logic_vector(31 downto 0);
	 fpga_config_read_in			 : in  std_logic_vector(31 downto 0);
	 
    clr_rst_reg_in             : in  std_logic;
	 serial_id						 : in  std_logic_vector(7 downto 0);
	 
    rod_busy_in                : in  std_logic;
    rod_clk_status_in          : in  std_logic;
    mdsp_hrdy_n_in             : in  std_logic;
    vme_timeout_out            : out std_logic_vector(31 downto 0);
    vme_am_code_in             : in  std_logic_vector( 5 downto 0);  -- vme AM code
    ga_n_in                    : in  std_logic_vector( 4 downto 0);
    gap_n_in                   : in  std_logic;
    trigger_in                 : in  std_logic;
	 ace_control					 :	out	std_logic_vector(31 downto 0);
	 ace_status_read				 : in	std_logic_vector(31 downto 0)
    );   
end component;

component fpga_cnfg_reset_controller_block
port (
  clk_in   : in  std_logic;
  rst_n_in : in  std_logic;

  cb_fpga_rst_reg_in  : in  std_logic_vector( 5 downto 0);
  cb_dsp_rst_reg_in   : in  std_logic_vector( 5 downto 0);
  cb_clr_rst_reg_out  : out std_logic;

  cb_fpga_rst_status_reg_out : out std_logic_vector(4 downto 0);
  cb_dsp_rst_status_reg_out  : out std_logic_vector(4 downto 0);

  cb_rrif_rst_n_out    : out std_logic;
  cb_formA_rst_n_out   : out std_logic;
  cb_formB_rst_n_out   : out std_logic;
  cb_efb_rst_n_out     : out std_logic;
  cb_router_rst_n_out  : out std_logic;

  cb_mdsp_rst_n_out  : out std_logic;
  cb_sdsp0_rst_n_out : out std_logic;
  cb_sdsp1_rst_n_out : out std_logic;
  cb_sdsp2_rst_n_out : out std_logic;
  cb_sdsp3_rst_n_out : out std_logic
  );
end component;

component hpi_data_controller
  port (
    clk_in            : in  std_logic; -- vme_clk66 input
    rst_n_in          : in  std_logic; -- asynchronous global reset 
    mdsp_cen_in       : in  std_logic; -- MDSP CE_N signal in
    mdsp_ds_in        : in  std_logic; -- MDSP Data Strobe (VME BE)
    mdsp_hrdy_n_in    : in  std_logic; -- Master DSP HRDY_N signal
    mdsp_hpi_data_in  : in  std_logic_vector(15 downto 0); --
    mdsp_hpi_data_out : out std_logic_vector(15 downto 0); --
    mdsp_cen_n_out    : out std_logic; -- MDSP CE_N signal out
    mdsp_rnw_out      : out std_logic; -- MDSP RNW signal out
    mdsp_hhwil_out    : out std_logic; -- MDSP HHWIL signal
    mdsp_hds1_n_out   : out std_logic; -- MDSP HHDS1 signal
    vme_bus_rnw_in    : in  std_logic; -- VMEBus read/not_write signal
    mdsp_vme_data_in  : in  std_logic_vector(31 downto 0); --
    mdsp_vme_data_out : out std_logic_vector(31 downto 0); --
    hpi_dtack_n_out   : out std_logic; -- VMEBus LACK_N 
    hpi_berr_out      : out std_logic; -- VMEBus BERR
    rcf_cen_in        : in  std_logic;
    --rcf_hrdy_n_in     : in  std_logic;
    rcf_cen_n_out     : out std_logic;
    vme_timeout_in    : in  std_logic_vector(31 downto 0);
	 hpi_state_debug   : out std_logic_vector(4 downto 0)
    );
end component; 

component vme_address_decoder
  port (
    clk_in          : in  std_logic;  -- VMEBus clk (60MHz)
    rst_n_in        : in  std_logic;  -- asynchronous reset (active low)
    rod_addr_n_in   : in  std_logic_vector( 4 downto 0); -- GA bits must be inv
    gaddr_par_n_in  : in  std_logic; 
    vme_addr_in     : in  std_logic_vector(31 downto 0); -- VMEBus Addr (31:20)
    vme_am_in       : in  std_logic_vector( 5 downto 0); -- LVME Byte Enables
    vme_as_in       : in  std_logic;
    vme_ds0_in      : in  std_logic;
    vme_ds1_in      : in  std_logic;
    prm_cen_out     : out std_logic;  -- PRM Register Block CS
    mdsp_cen_out    : out std_logic;  -- MDSP HPI State Machine Enable
    rcf_cen_out     : out std_logic;  -- RCF HPI State Machine Enable
	 ppc_cen_out     : out std_logic;  -- PPC HPI State Machine Enable --RT
 	 ace_cen_out     : out std_logic;  -- ACE player Data Input Enable --RT
    ill_match_out   : out std_logic;  -- Address Match is true, but the decode region is false
    data_strb_out   : out std_logic;   -- VME DS signal
    vme_am_out      : out std_logic_vector( 5 downto 0)
    );
end component;

component reset_command_controller
  port (
    clk40_in           : in  std_logic;
    rst_n_in           : in  std_logic;
    clr_reset_cmd_in   : in  std_logic;
    rod_busy_in        : in  std_logic;
    reset_commands_in  : in  std_logic_vector(1 downto 0);
    reset_commands_out : out std_logic_vector(1 downto 0);
    reset_sdsp_out     : out std_logic_vector(3 downto 0);
    trigger_out        : out std_logic
    );
end component;

component clock_gen
port
 (-- Clock in ports
 -- CLK_IN1_P         : in     std_logic;
 -- CLK_IN1_N         : in     std_logic;
   CLK_IN1         : in     std_logic;
  --- Clock out ports
  CLK_OUT1          : out    std_logic;
  -- Status and control signals
  RESET             : in     std_logic;
  LOCKED            : out    std_logic
 );
end component;

--RT
COMPONENT ppc_hpi_controller
	PORT(
		clk_in : IN std_logic;
		rst_n_in : IN std_logic;
		ppc_cen_in : IN std_logic;
		mdsp_hcntl0_in : IN std_logic;
		mdsp_hcntl1_in : IN std_logic;
		ppc_ds_in : IN std_logic;
		ppc_hpi_busy_in : IN std_logic;
		ppc_hpi_done_in : IN std_logic;
		ppc_hpi_data_in : IN std_logic_vector(31 downto 0);
		vme_bus_rnw_in : IN std_logic;
		ppc_vme_data_in : IN std_logic_vector(31 downto 0);
		vme_timeout_in : IN std_logic_vector(31 downto 0);          
		ppc_hpi_data_out : OUT std_logic_vector(31 downto 0);
		ppc_vme_data_out : OUT std_logic_vector(31 downto 0);
		ppc_dtack_n_out : OUT std_logic;
		ppc_berr_out : OUT std_logic;
		hpi_ppc_command_out : OUT std_logic_vector(3 downto 0);
		hpi_strobe_out : OUT std_logic
		);
	END COMPONENT;
	--end RT

component icon
  PORT 
    (
    CONTROL0: INOUT STD_LOGIC_VECTOR(35 DOWNTO 0)
	 );
end component;

component ila
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TRIG1 : IN STD_LOGIC_VECTOR(63 DOWNTO 0);
    TRIG2 : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	 TRIG3 : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    TRIG4 : IN STD_LOGIC_VECTOR(41 DOWNTO 0);
	 TRIG5 : IN STD_LOGIC_VECTOR(63 DOWNTO 0));
end component;

signal CK100_BUFF: std_logic;
--signal starter_count: integer range 0 to 1340000000 := 0;
signal reset_v5_int:	std_logic;


signal desktopMode: std_logic;

component ace_player_controller
port (
    clk_in            : in  std_logic;
    rst_in          : in  std_logic; 
    ace_cen_in       : in  std_logic; 
    vme_ds_in        : in  std_logic; 
    vme_bus_rnw_in    : in  std_logic; 
    vme_data_in   : in  std_logic_vector(31 downto 0); 
    par_data_out   : out std_logic_vector(31 downto 0);
    data_wren   : out std_logic; --
    vme_ack_n_out   : out std_logic; 
    vme_berr_out      : out std_logic; 
    vme_timeout_in    : in  std_logic_vector(31 downto 0)
    );
end component;

signal ace_cnrtl_data : std_logic_vector(31 downto 0); 
signal ace_cnrtl_wren : std_logic;
signal ace_control : std_logic_vector(31 downto 0); 
signal ace_status_read : std_logic_vector(31 downto 0); 
signal ace_tdi					:	std_logic;
signal ace_tck					:	std_logic;
signal ace_tms					:	std_logic;
signal ace_tdo					:	std_logic;
signal rst_i          		: std_logic;

attribute keep: string;
attribute keep of spi_ss_buf : signal is "true";
component ace_programmer is
port(
	clock					:	in		std_logic;
	rst   				:	in		std_logic;
	fifo_data			:	in		std_logic_vector(31 downto 0);
	wr_en					:	in		std_logic;
	ace_control			:	in		std_logic_vector(31 downto 0);
	ace_status_read	:	out	std_logic_vector(31 downto 0);
	tdi					:	out	std_logic;
	tck					:	out	std_logic;
	tms					:	out	std_logic;
	tdo					:	in		std_logic
	);
end component;

-------------------------------------------------------------------------------
-- SIGNAL ASSIGNMENTS
-------------------------------------------------------------------------------

begin

  IBUFDS_inst : IBUFDS
   generic map (
      DIFF_TERM => FALSE, -- Differential Termination 
      IBUF_LOW_PWR => TRUE, -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
      IOSTANDARD => "DEFAULT")
   port map (
      O => CK100_BUFF,  -- Buffer output
      I => ck40_p,  -- Diff_p buffer input (connect directly to top-level port)
      IB => ck40_n -- Diff_n buffer input (connect directly to top-level port)
   );
	
-- vme_sysreset is not stable in desktop operation. don't include it in dcm reset	
-- rst_dcm					<= '1' when (rst = '0' OR vme_sysreset = '0') else '0';
-- there are no pullups on sysfail and vmereset. Work around: check if all GA pins and GP are high. 
-- those have pullups. this should indicate in desktop system

desktopMode <= '1' when vme_ga_n = "11111" and vme_gap = '1' else '0';
rst_dcm					<= '1' when (rst = '0') or (desktopMode = '0' and vme_sysreset = '0') else '0';
rst_n 					<= '0' when (rst = '0') or (locked = '0') or (desktopMode = '0' and vme_sysreset = '0') else '1';
rst_i                <= not rst_n;

mdsp_rst_n				<= dsp_rst_n_out_i(4);

ga_n_i					<= vme_ga_n(4 downto 0);
prm_ga_out				<= ga_n_i;
vme_gap_n_i				<= vme_gap;
vme_dir					<= vme_ddir_p;
vme_oen					<= vme_de_n_p;
vme_addr_i				<= vme_address;
vme_am_p					<= vme_am;
vme_as_p					<= vme_as;
vme_wr_p					<= vme_write;
vme_ds0_p				<= vme_dsa;
vme_ds1_p				<= vme_dsb;
vme_iackin_n_p 		<= vme_iackin;
vme_iack_p				<= vme_iack;
vme_dtack				<= vme_dtack_n_p;
vme_berr					<= vme_berr_n_p;
vme_iackout				<= vme_iackout_n_p;
lirq_val_p				<= vme_irq;

mdsp_hcntl0				<= mdsp_hcntl0_i;
mdsp_hcntl1				<= mdsp_hcntl1_i;
mdsp_hrdy_n_i			<= mdsp_hrdy_n;
mdsp_hcs_n				<= mdsp_cen_n_i;
mdsp_hrnw				<= mdsp_rnw_i;
mdsp_hhwil				<= mdsp_hhwil_i;
mdsp_hds1_n				<= mdsp_hds1_n_i;
--rcf_hcs_n				<= rcf_cen_n_i;
--rcf_hrdy_n_i			<= rcf_hrdy_n;
mdsp_boot4				<= mdsp_boot4_i;
mdsp_boot3				<= mdsp_boot3_i;
mdsp_boot2				<= mdsp_boot2_i;
mdsp_boot1				<= mdsp_boot1_i;
mdsp_boot0				<= mdsp_boot0_i;

reset_dsp_i 			<= dsp_rst_reg_i(6 downto 1) OR ('0' & dsp_reset_cmd_i & '0');
reset_cmds_in_i		<= reset_cmds_in;
reset_cmds_out			<= reset_cmds_out_i;

reset_v5_int			<= NOT(rst) OR (NOT(rodresio_rst_n_out_i) AND  fpga_rst_reg_i(6)); -- active HIGH reset for V5
reset_v5					<= reset_v5_int;
rod_busy					<= rod_busy_n_i;
rod_busy_n_p			<= rod_busy_from_v5; 

--RT
--AKU
ppc_hpi_busy_i 		<= prm_ctrl_in(ctl_to_prm_hpi_busy_bit);
ppc_hpi_done_i 		<= prm_ctrl_in(ctl_to_prm_hpi_done_bit);
prm_ctrl_out(ctl_from_prm_hpi_cmd_hi_bit downto ctl_from_prm_hpi_cmd_lo_bit)         <= hpi_ppc_command_i;
prm_strobe0		      <= hpi_strobe_i;
--end RT 

trig0						<= vme_address;

trig1(5 downto 0)		<= vme_am;
trig1(6)					<= vme_as;
trig1(7)					<= vme_dsa;
trig1(8)					<= vme_dsb;
trig1(9)					<= vme_write;
trig1(10)				<= vme_gap;
trig1(18 downto 11) 	<= "000" & vme_ga_n;
trig1(19)				<= vme_dtack_n_p;
trig1(20)				<= vme_data_dir_i;
trig1(21)				<= vme_ddir_p;
trig1(22)				<= prm_cen_i;
trig1(23)				<= mdsp_cen_i;
trig1(24)				<= rcf_cen_i;
trig1(25)				<= data_strb_i;
trig1(26)				<= ill_match_i;
trig1(31 downto 27)	<= hpi_state_debug;
-- ...
trig1(63 downto 32)  <= vme_data_in_p;

-- mdsp no longer in use, so free up signals for other purpose
--trig2(0)					<= mdsp_hcntl0_i;
--trig2(1)					<= mdsp_hcntl1_i;
--trig2(2)					<= mdsp_hrdy_n;
--trig2(3)					<= mdsp_cen_n_i;					
--trig2(4)					<= mdsp_rnw_i;
--trig2(5)					<= mdsp_hhwil_i;
--trig2(6)					<= mdsp_hds1_n_i;
--trig2(7)					<= mdsp_boot4_i;
--trig2(8)					<= mdsp_boot3_i;
--trig2(9)					<= mdsp_boot2_i;
--trig2(10)				<= mdsp_boot1_i;
--trig2(11)				<= mdsp_boot0_i;

trig2(0) <= prm_ctrl_in(ctl_to_prm_spi_ss_bit);
trig2(1) <= prm_ctrl_in(ctl_to_prm_spi_mosi_bit);
trig2(2) <= spi_ss_buf;
trig2(3) <= spi_ss_buf0;
trig2(4) <= spi_miso;
trig2(5) <= spi_sck_buf; -- '0'; -- prm_ctrl_in(4);
trig2(6) <= prm_ctrl_in(ctl_to_prm_wdog_bit);
trig2(7) <= desktopMode;
trig2(11 downto 8) <= spi_ctl_mode;

-- keep from trig2(12)
--trig2(12)				<= dsp_rst_n_out_i(4);	-- MDSP reset
trig2(12)				<= rst_dcm;
trig2(13)				<= rodresio_rst_n_out_i;
trig2(14)				<= fpga_rst_reg_i(6);
trig2(15)				<= reset_v5_int;

trig3(15 downto 0)	<= mdsp_hpi_data_in_i;
trig3(31 downto 16)	<= mdsp_hpi_data_out_i;trig4(3 downto 0)		<= ace_status_read(3 downto 0);

trig4(4) 				<= tck_int;--hpi_strobe_i;
trig4(5) 				<= tms_int;--ppc_hpi_busy_i;
trig4(6) 				<= tdo_int_c;--ppc_hpi_done_i;
trig4(7) 				<= tdi_int;--ppc_cen_i;
trig4(8) 				<= '0';--ppc_dtack_n_i;
trig4(9) 				<= ace_cnrtl_wren;--ppc_berr_i;
trig4(16 downto 10 ) 	<= ace_status_read(10 downto 4); --vme_ppc_do_i;
trig4(19 downto 17 ) 	<= ace_status_read(13 downto 11);
trig4(27 downto 20)		<= ace_status_read(21 downto 14);--RT
--trig4(3 downto 0) 	<= hpi_ppc_command_i;
--trig4(4) 				<= hpi_strobe_i;
--trig4(5) 				<= ppc_hpi_busy_i;
--trig4(6) 				<= ppc_hpi_done_i;
--trig4(7) 				<= ppc_cen_i;
--trig4(8) 				<= ppc_dtack_n_i;
--trig4(9) 				<= ppc_berr_i;
--trig4(41 downto 10) 	<= vme_ppc_do_i;
--
trig5(31 downto 0)	<= ppc_hpi_data_in_i;
trig5(63 downto 32)	<= ppc_hpi_data_out_i;
--end RT

--------------------------------------------------------------------------------
--COMPONENT INSTANTIATION
--------------------------------------------------------------------------------

clock_gen_instance : clock_gen
  port map
   (
    CLK_IN1 => CK100_BUFF,
    CLK_OUT1 => ck,
    RESET  => rst_dcm,
    LOCKED => locked);
	 
icon_instance: icon
  port map 
   (
    CONTROL0 => control_chipscope
	 );
	 
ila_instance: ila
  port map 
   (
    CONTROL => control_chipscope,
    CLK => CK100_BUFF,	
    TRIG0 => trig0,
    TRIG1 => trig1,
	 TRIG2 => trig2,
	 TRIG3 => trig3,
	 TRIG4 => trig4,
	 TRIG5 => trig5
	 );
	 
fpga_cnfg_reset_controller_block_instance : fpga_cnfg_reset_controller_block
  port map (
    clk_in                     => ck,
    rst_n_in                   => rst_n,
    cb_fpga_rst_reg_in         => fpga_rst_reg_i(5 downto 0),
    cb_dsp_rst_reg_in          => reset_dsp_i,    --dsp_rst_reg_i,
    cb_clr_rst_reg_out         => clr_rst_reg_i,
    cb_fpga_rst_status_reg_out => fpga_rst_status_reg_i,
    cb_dsp_rst_status_reg_out  => dsp_rst_status_reg_i,	 
    cb_rrif_rst_n_out          => rodresio_rst_n_out_i,
    cb_formA_rst_n_out         => formA_rst_n_out_i,
    cb_formB_rst_n_out         => formB_rst_n_out_i,
    cb_efb_rst_n_out           => efb_rst_n_out_i,
    cb_router_rst_n_out        => router_rst_n_out_i,
    cb_mdsp_rst_n_out          => dsp_rst_n_out_i(4),
    cb_sdsp0_rst_n_out         => dsp_rst_n_out_i(0),
    cb_sdsp1_rst_n_out         => dsp_rst_n_out_i(1),
    cb_sdsp2_rst_n_out         => dsp_rst_n_out_i(2),
    cb_sdsp3_rst_n_out         => dsp_rst_n_out_i(3)
    );


reset_processor_core_select <= fpga_rst_reg_i(7);
reset_S6A_from_prm <= (not (rst_n)) or ( not (formA_rst_n_out_i));
reset_S6B_from_prm <= (not (rst_n)) or ( not (formB_rst_n_out_i));

register_block_instance : register_block
  port map (
    clk_in                     => ck,
    rst_n_in                   => rst_n,
    prb_bus_strb_in            => data_strb_i,
    prb_bus_ce_in              => prm_cen_i,
    prb_bus_rnw_in             => vme_rnw_i,
    prb_bus_addr_in            => vme_addr_i,
    prb_bus_data_in            => vme_data_in_p,
    prb_bus_data_out           => vme_prm_out_i,
    prb_bus_ack_out            => prb_bus_ack_i,
    prb_fpga_rst_status_reg_in => fpga_rst_status_reg_i,
    prb_dsp_rst_status_reg_in  => dsp_rst_status_reg_i,
    fpga_rst_reg_out           => fpga_rst_reg_i,
    dsp_rst_reg_out            => dsp_rst_reg_i,
	 fpga_config_write_out		 => fpga_config_write_i,
	 fpga_config_read_in			 => fpga_config_read_i,
    clr_rst_reg_in             => clr_rst_reg_i,
	 serial_id						 => serial_id,
    rod_busy_in                => rod_busy_i,
    rod_clk_status_in          => locked,
    mdsp_hrdy_n_in             => mdsp_hrdy_n_i,
    vme_timeout_out            => vme_timeout_i,
    vme_am_code_in             => vme_am_code_i,
    ga_n_in                    => ga_n_i,  		
    gap_n_in                   => vme_gap_n_i,
    trigger_in                 => trigger_i,
	 ace_control					 => ace_control,
	 ace_status_read				 => ace_status_read
    );

hpi_data_controller_instance : hpi_data_controller
  port map (
    clk_in            => ck,
    rst_n_in          => rst_n,
    mdsp_cen_in       => mdsp_cen_i,
    mdsp_ds_in        => data_strb_i,
    mdsp_hrdy_n_in    => mdsp_hrdy_n_i,
    mdsp_hpi_data_in  => mdsp_hpi_data_in_i,
    mdsp_hpi_data_out => mdsp_hpi_data_out_i,
    mdsp_cen_n_out    => mdsp_cen_n_i,
    mdsp_rnw_out      => mdsp_rnw_i,
    mdsp_hhwil_out    => mdsp_hhwil_i,
    mdsp_hds1_n_out   => mdsp_hds1_n_i,
    vme_bus_rnw_in    => vme_rnw_i,
    mdsp_vme_data_in  => vme_data_in_p,
    mdsp_vme_data_out => vme_hpi_do_i,
    hpi_dtack_n_out   => hpi_dtack_n_i,
    hpi_berr_out      => hpi_berr_i,
    rcf_cen_in        => rcf_cen_i,
--    rcf_hrdy_n_in     => rcf_hrdy_n_i,
    rcf_cen_n_out     => rcf_cen_n_i,
    vme_timeout_in    => vme_timeout_i,
	 hpi_state_debug	 => hpi_state_debug
    );

vme_address_decoder_instance: vme_address_decoder
  port map (
    clk_in          => ck,
    rst_n_in        => rst_n,
    rod_addr_n_in   => ga_n_i,
    gaddr_par_n_in  => vme_gap_n_i,
    vme_addr_in     => vme_addr_i,
    vme_am_in       => vme_am_p,
    vme_as_in       => vme_as_p,
    vme_ds0_in      => vme_ds0_p,
    vme_ds1_in      => vme_ds1_p,
    prm_cen_out     => prm_cen_i,
    mdsp_cen_out    => mdsp_cen_i,
    rcf_cen_out     => rcf_cen_i,
	 ppc_cen_out     => ppc_cen_i, --RT
	 ace_cen_out     => ace_cen_i, --RT
    ill_match_out   => ill_match_i,
    data_strb_out   => data_strb_i,
    vme_am_out      => vme_am_code_i
    );

reset_command_controller_instance : reset_command_controller
port map (
    clk40_in           => ck,
    rst_n_in           => rst_n,
    clr_reset_cmd_in   => clr_rst_reg_i,
    rod_busy_in        => rod_busy_i,
    reset_commands_in  => reset_cmds_in_i,
    reset_commands_out => reset_cmds_out_i,
    reset_sdsp_out     => dsp_reset_cmd_i,
    trigger_out        => trigger_i
    );

--RT	
	 	ppc_hpi_controller_instance: ppc_hpi_controller PORT MAP(
		clk_in => ck,
		rst_n_in => rst_n,
		ppc_cen_in => ppc_cen_i,
		mdsp_hcntl0_in => mdsp_hcntl0_i,
		mdsp_hcntl1_in => mdsp_hcntl1_i,
		ppc_ds_in => data_strb_i,
		ppc_hpi_busy_in => ppc_hpi_busy_i,
		ppc_hpi_done_in => ppc_hpi_done_i,
		ppc_hpi_data_in => ppc_hpi_data_in_i,
		ppc_hpi_data_out => ppc_hpi_data_out_i,
		vme_bus_rnw_in => vme_rnw_i,
		ppc_vme_data_in => vme_data_in_p,
		ppc_vme_data_out => vme_ppc_do_i,
		ppc_dtack_n_out => ppc_dtack_n_i,
		ppc_berr_out => ppc_berr_i,
		vme_timeout_in => vme_timeout_i,
		hpi_ppc_command_out => hpi_ppc_command_i,
		hpi_strobe_out => hpi_strobe_i
	);
	
	
	
	ace_controller_inst: ace_player_controller PORT MAP(
    clk_in    => ck,
    rst_in    => rst_i,
    ace_cen_in  => ace_cen_i,
    vme_ds_in  => data_strb_i,
    vme_bus_rnw_in  => vme_rnw_i,
    vme_data_in   => vme_data_in_p,
    par_data_out  => ace_cnrtl_data,
    data_wren  => ace_cnrtl_wren,
    vme_ack_n_out  => ace_dtack_n_i,
    vme_berr_out   => ace_berr_i,
    vme_timeout_in  => vme_timeout_i
    );

	--end RT
	


ace_programmer_instance: ace_programmer 
port map(
	clock					=>ck,
	rst   				=>rst_n,
	fifo_data			=>ace_cnrtl_data,
	wr_en					=>ace_cnrtl_wren,
	ace_control			=>ace_control,
	ace_status_read	=>ace_status_read,
	tdo					=>ace_tdo,
	tdi					=>ace_tdi,
	tms					=>ace_tms,
	tck					=>ace_tck
	);

--ace_tdo<=ace_tdi;




-- VMEBus I/O 
vme_data_inout_block: 
  for i in 0 to 31 generate
    vme_data_inout_iobufs : IOBUF 
      port map (
        I =>  vme_data_out_p(i),
        O =>  vme_data_in_p(i),
        IO => vme_data(i),
        T =>  vme_data_dir_i
        );
  end generate;

mdsp_hpi_data_inout_block : 
  for i in 0 to 15 generate
    mdsp_hpi_data_iobufs : IOBUF 
      port map (
        I  => mdsp_hpi_data_out_i(i),
        O  => mdsp_hpi_data_in_i(i),
        IO => mdsp_hd(i),
        T  => mdsp_hpi_data_dir_i
        );
end generate;

--RT
ppc_hpi_data_inout_block : 
  for i in 0 to 31 generate
    mdsp_hpi_data_iobufs : IOBUF 
      port map (
        I  => ppc_hpi_data_out_i(i),
        O  => ppc_hpi_data_in_i(i),
        IO => prm_data(i),
        T  => ppc_hpi_data_dir_i
        );
end generate;
--end RT

-------------------------------------------------------------------------------
-- PROCESS DECLARATION
-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
rod_busy_process : process(ck, rst_n)
begin
  if(rst_n = '0') then
    rod_busy_i   <= '1';
    rod_busy_n_i <= '0';
  elsif (rising_edge(ck)) then
    rod_busy_i   <= NOT rod_busy_n_p;
    rod_busy_n_i <= NOT rod_busy_i;
  end if;
end process rod_busy_process;

-------------------------------------------------------------------------------
vme_data_out_mux_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    vme_data_out_p <= (others => '1');
  elsif(rising_edge(ck)) then
    if(prm_cen_i = '1') then
      vme_data_out_p <= vme_prm_out_i;
    elsif(mdsp_cen_i = '1') then
      vme_data_out_p <= vme_hpi_do_i;
	 elsif(ppc_cen_i = '1') then --RT
      vme_data_out_p <= vme_ppc_do_i; --RT
    elsif(lvme_iack_i = '1') then
      vme_data_out_p <= "111111111111111111111111000" & NOT ga_n_i;  -- interrupt vector
    else
      vme_data_out_p <= (others => '1');
    end if;
  end if; 
end process vme_data_out_mux_process;

vme_data_output_en_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    vme_data_dir_i <= '1';  -- set FPGA data port to receive
    vme_ddir_p     <= '0';  -- lvt162245 set to receive
    vme_de_n_p     <= '1';  -- tristate lvt162245
  elsif(rising_edge(ck)) then
 --RT   vme_data_dir_i <= NOT (vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i) AND vme_rnw_i);
 --RT   vme_ddir_p     <=      vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i) AND vme_rnw_i;
		vme_data_dir_i <= NOT (vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i OR ppc_cen_i OR ace_cen_i) AND vme_rnw_i); --RT
		vme_ddir_p     <=      vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i OR ppc_cen_i OR ace_cen_i) AND vme_rnw_i;  --RT
   if(vme_sysfail_i = '1') then
      vme_de_n_p <= '1';  -- tristate lvt162245
    else
--RT     vme_de_n_p <= NOT (vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i) AND vme_ds_i);
		vme_de_n_p <= NOT (vme_as_i AND (prm_cen_i OR mdsp_cen_i OR lvme_iack_i OR ppc_cen_i OR ace_cen_i) AND vme_ds_i); --RT
     end if;
  end if;
end process vme_data_output_en_process;

vme_dtack_output_process : process(ck, rst_n)
begin
  if(rst_n = '0') then
    vme_dtack_n_p 	<= '1';
	 vme_dtack_delay0	<= '1';
  elsif(rising_edge(ck)) then
    if(vme_sysfail_i = '1') then
      vme_dtack_delay0 <= '1';
    elsif(prm_cen_i = '1' AND vme_as_i = '1') then
      vme_dtack_delay0 <= NOT prb_bus_ack_i;
    elsif(mdsp_cen_i = '1' AND vme_as_i = '1') then
      vme_dtack_delay0 <= hpi_dtack_n_i;
    elsif(ppc_cen_i = '1' AND vme_as_i = '1') then --RT
      vme_dtack_delay0 <= ppc_dtack_n_i;  --RT
    elsif(ace_cen_i = '1' AND vme_as_i = '1') then --RT
      vme_dtack_delay0 <= ace_dtack_n_i;  --RT
    elsif(lvme_iack_i = '1' AND vme_as_i = '1') then
      vme_dtack_delay0 <= NOT vme_ds_i;
    else
      vme_dtack_delay0 <= '1';
    end if;
	 vme_dtack_n_p	<= vme_dtack_delay0;
  end if;
end process vme_dtack_output_process;

vme_berr_output_process : process(ck, rst_n)
begin
  if(rst_n = '0') then
    vme_berr_n_p <= '1';
  elsif(rising_edge(ck)) then
    if (vme_sysfail_i = '1') then
      vme_berr_n_p <= '1';
    else
 --RT     vme_berr_n_p <= NOT (ill_match_i OR hpi_berr_i);
      vme_berr_n_p <= NOT (ill_match_i OR hpi_berr_i OR ppc_berr_i OR ace_berr_i); --RT
   end if;
  end if;
end process vme_berr_output_process;

vme_int_decode_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    lvme_iack_i <= '0';
  elsif(rising_edge(ck)) then
    if(vme_iack_i = '1' AND vme_iackout_n_p = '1' AND vme_as_i = '1') then
      if(vme_addr_i(3 downto 1) = lirq_val_p AND lirq_i = '1') then
        lvme_iack_i <= '1';
      else
        lvme_iack_i <= '0';
      end if;      
    else
      lvme_iack_i <= '0';
    end if;
  end if;
end process vme_int_decode_process;

hpi_data_output_en_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    mdsp_hpi_data_dir_i <= '1';
  elsif(rising_edge(ck)) then
    if(mdsp_cen_i = '1' AND vme_rnw_i = '0') then
      mdsp_hpi_data_dir_i <= '0';  -- output to MDSP
    else
      mdsp_hpi_data_dir_i <= '1';  -- input from MDSP
    end if;
  end if;
end process hpi_data_output_en_process;

--RT
ppc_hpi_data_output_en_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    ppc_hpi_data_dir_i <= '1';
  elsif(rising_edge(ck)) then
    if(ppc_cen_i = '1' AND vme_rnw_i = '0') then
      ppc_hpi_data_dir_i <= '0';  -- output to MDSP
    else
      ppc_hpi_data_dir_i <= '1';  -- input from MDSP
    end if;
  end if;
end process ppc_hpi_data_output_en_process;

--end RT
vme_to_mdsp_register_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    mdsp_hcntl1_i <= '0';
    mdsp_hcntl0_i <= '0';
  elsif(rising_edge(ck)) then
    mdsp_hcntl1_i <= NOT vme_addr_i(23) AND vme_addr_i(22);
    mdsp_hcntl0_i <= NOT vme_addr_i(23) AND vme_addr_i(21);
  end if;
end process vme_to_mdsp_register_process;

vme_signal_register_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    vme_rnw_i       <= '0';
    vme_as_i        <= '0';
    vme_ds_i        <= '0';
	 vme_iackout_n_p <= '1';
    vme_iack_i      <= '0';
    lirq_i          <= '0';
    vme_sysfail_i   <= '0';
  elsif(rising_edge(ck)) then
    vme_rnw_i       <= NOT vme_wr_p;
    vme_as_i        <= vme_as_p;
    vme_ds_i        <= vme_ds0_p AND vme_ds1_p;
	 vme_iackout_n_p <= vme_iackin_n_p;
    vme_iack_i      <= vme_iack_p;
    lirq_i          <= NOT vme_irq_n;
    vme_sysfail_i   <= NOT vme_sysfail;
  end if;
end process vme_signal_register_process;

mdsp_boot_mode_select_process: process(ck, rst_n)
begin
  if(rst_n = '0') then
    mdsp_boot4_i <= '0';   -- "01101" ROM, Map 1, Internal Memory at Loc 0x0h 
    mdsp_boot3_i <= '1';
    mdsp_boot2_i <= '1';
    mdsp_boot1_i <= '0';
    mdsp_boot0_i <= '1';
  elsif(rising_edge(ck)) then
    if(dsp_rst_reg_i(0) = '0') then
      mdsp_boot4_i <= '0'; -- "01101" ROM, Map 1, Internal Memory at Loc 0x0h 
      mdsp_boot3_i <= '1';
      mdsp_boot2_i <= '1';
      mdsp_boot1_i <= '0';
      mdsp_boot0_i <= '1';
    elsif(dsp_rst_reg_i(0) = '1') then
      mdsp_boot4_i <= '0'; -- "00111" HPI, Map 1, Internal Memory at Loc 0x0h 
      mdsp_boot3_i <= '0';
      mdsp_boot2_i <= '1';
      mdsp_boot1_i <= '1';
      mdsp_boot0_i <= '1';
    end if;
  end if;
end process mdsp_boot_mode_select_process;

--mdsp_boot_mode_select_process: process(ck, rst_n)
--begin
--  if(rst_n = '0') then
--    mdsp_boot4_i <= '0';   -- "01101" ROM, Map 1, Internal Memory at Loc 0x0h 
--    mdsp_boot3_i <= '0';
--    mdsp_boot2_i <= '1';
--    mdsp_boot1_i <= '1';
--    mdsp_boot0_i <= '1';
--  elsif(rising_edge(ck)) then
--    if(dsp_rst_reg_i(0) = '0') then
--      mdsp_boot4_i <= '0'; -- "01101" ROM, Map 1, Internal Memory at Loc 0x0h 
--      mdsp_boot3_i <= '0';
--      mdsp_boot2_i <= '1';
--      mdsp_boot1_i <= '1';
--      mdsp_boot0_i <= '1';
--    elsif(dsp_rst_reg_i(0) = '1') then
--      mdsp_boot4_i <= '0'; -- "00111" HPI, Map 1, Internal Memory at Loc 0x0h 
--      mdsp_boot3_i <= '0';
--      mdsp_boot2_i <= '1';
--      mdsp_boot1_i <= '1';
--      mdsp_boot0_i <= '1';
--    end if;
--  end if;
--end process mdsp_boot_mode_select_process;

process(fpga_config_write_i, tdo_int_c, main_tdi,main_tms,main_tck, ace_tdi,ace_tms,ace_tck,ace_control,xvc_enable)
begin
	if(fpga_config_write_i(31) = '1' and ace_control(0) = '1') then		-- via VME
		tdi_int		<=	fpga_config_write_i(2);
		tms_int		<=	fpga_config_write_i(1);
		tck_int		<=	fpga_config_write_i(0);
		main_tdo		<= '0';
	elsif (ace_control(0) = '0') then
		tdi_int		<=	ace_tdi;
		tms_int		<=	ace_tms;
		tck_int		<=	ace_tck;
		ace_tdo		<=	tdo_int_c;
	elsif xvc_enable = '0' then -- via JTAG (J9)
		tdi_int		<=	main_tdi;
		tms_int		<=	main_tms;
		tck_int		<=	main_tck;
		main_tdo		<=	tdo_int_c;
	else														-- via xvc
		tdi_int		<=	xvc_tdi;
		tms_int		<=	xvc_tms;
		tck_int		<=	xvc_tck;
		xvc_tdo		<=	tdo_int_c;
	end if;
end process;

fpga_config_read_i(0)		<=	tdo_int_c;

-- fpga_s6a interface
fpga_a_tdi	<=	tdi_int;
fpga_a_tms	<=	tms_int;
fpga_a_tck	<=	tck_int;
tdo_int_a	<=	fpga_a_tdo;

-- fpga_s6b interface
fpga_b_tdi	<=	tdo_int_a;
fpga_b_tms	<=	tms_int;
fpga_b_tck	<=	tck_int;
tdo_int_b	<=	fpga_b_tdo;

-- fpga_v5 interface
fpga_c_tdi	<=	tdo_int_b;
fpga_c_tms	<=	tms_int;
fpga_c_tck	<=	tck_int;
tdo_int_c	<=	fpga_c_tdo;


-- spi for slot number etc

	master_if_clk <= CK100_BUFF; -- alternative: ck_40

	spiSyncedClk: if not useSpiClk generate
	begin

		process(master_if_clk, reset_v5_int)
			  variable prmInfoWord: std_logic_vector(gitIdLength + 32 - 1 downto 0);
			  variable i: integer range prmInfoWord'length downto 0;
		 begin
			  if reset_v5_int = '1' then
					spi_sck_r    <= '0';
					spi_sck_r0    <= '0';
					spi_sck_r1    <= '0';
					spi_miso        <= '0';
					spi_ctl_mode  <= (others => '0');
					prmInfoWord    := (others => '0');
					i                := 0;
			  elsif rising_edge(master_if_clk) then

					spi_sck_r <= prm_ctrl_in(ctl_to_prm_spi_sck_bit);        -- initial stage sync
					spi_sck_r0 <= spi_sck_r;
					spi_sck_r1 <= spi_sck_r0;
					-- the large mux 
					spi_miso <= prmInfoWord(i);

					-- load address while ss inactive (high)
					if prm_ctrl_in(ctl_to_prm_spi_ss_bit) = '1' then
						 -- magic 0x5, command word (4 bit), serial id (8 bit), two 0 bits, ga parity, ga bits
						 prmInfoWord := X"5" & spi_ctl_mode & mst_reset_cnt & serial_id & "00" & vme_gap & vme_ga_n & gitHash;
						 i := prmInfoWord'length - 1;
						 -- update mode from input command register
						 spi_ctl_mode <= spi_ctl_mode_r;
					elsif (spi_sck_r0 = '0' and spi_sck_r1 = '1') then -- shift out on falling edge
						 i := i-1;
					elsif (spi_sck_r0 = '1' and spi_sck_r1 = '0') then -- shift in on rising edge
						 -- update input command register
						spi_ctl_mode_r <= prm_ctrl_in(ctl_to_prm_spi_mosi_bit) & spi_ctl_mode_r(spi_ctl_mode_r'length - 1 downto 1);
					end if;
			  end if;
		 end process;
	 
	 end generate;

	spiDirectClk: if useSpiClk generate
	begin


	   spibuf: ibufg port map (i => prm_ctrl_in(ctl_to_prm_spi_sck_bit), o => spi_sck_buf);

		-- for some reason, an BUFG is synthisesied for ss when used directly. This conflicts with
		-- the sclk clock buffer. To prevent the ss bufg, we use special logic below 
		-- registered ss
		ssbuf : FDPE
			generic map (INIT => '1') 
			port map (
				Q => spi_ss_buf0, 
				C => master_if_clk,
				CE => '1', 
				PRE => prm_ctrl_in(ctl_to_prm_spi_ss_bit), 
				D => prm_ctrl_in(ctl_to_prm_spi_ss_bit) 
			);
		-- with subsequent AND to make SPI work even without sync clock (just in case)
		spi_ss_buf <= spi_ss_buf0 AND prm_ctrl_in(ctl_to_prm_spi_ss_bit);

			

		-- spi data word
		prmInfoWord <= X"5" & spi_ctl_mode & mst_reset_cnt & serial_id & locked & "0" & vme_gap & vme_ga_n & gitHash;
		-- output mux
		spi_miso <= prmInfoWord(prmInfoIndex);
	
		spiIndex: process(spi_ss_buf, spi_sck_buf)
		begin
			if spi_ss_buf = '1' then
				prmInfoIndex <= prmInfoWord'length - 1; 
			elsif falling_edge(spi_sck_buf) then
				prmInfoIndex <= prmInfoIndex - 1; 
			end if;
		end process;
	
		spiInput: process(spi_ss_buf, spi_sck_buf, reset_v5_int)
		begin
			if reset_v5_int = '1' then
					spi_ctl_mode_r  <= (others => '0');
			elsif rising_edge(spi_sck_buf) then
				if prmInfoIndex < spi_ctl_mode_r'length then
					spi_ctl_mode_r(prmInfoIndex) <= prm_ctrl_in(ctl_to_prm_spi_mosi_bit); 
				end if;
			end if;
		end process;
	
	
		spiUpdate: process(master_if_clk, reset_v5_int)
		 begin
			  if reset_v5_int = '1' then
					spi_ctl_mode  <= (others => '0');
			  elsif rising_edge(master_if_clk) then
					-- update input command register when ss inactive (high)
					if spi_ss_buf = '1' then
						spi_ctl_mode <= spi_ctl_mode_r;
					end if;
			  end if;
		 end process;
	 
	 end generate;
	 

	-- map output
	prm_ctrl_out(ctl_from_prm_spi_miso_bit) <= spi_miso;
	
	-- xvc jtag map
  -- AKU: with XVC Jtag
	xvc_enable <= spi_ctl_mode(3);  --
	xvc_tck <= prm_ctrl_in(ctl_to_prm_xvc_tck_bit);
	xvc_tms <= prm_ctrl_in(ctl_to_prm_xvc_tms_bit);
	xvc_tdi <= prm_ctrl_in(ctl_to_prm_xvc_tdi_bit);
	prm_ctrl_out(ctl_from_prm_xvc_tdo_bit) <= xvc_tdo; 
	
	-- aku: master wdog reset  counter
	process(master_if_clk, reset_v5_int)
	begin
		if reset_v5_int = '1' then 
			mst_reset_cnt <= (others => '0');
			mst_reset_r <= '0';
			mst_reset_r0 <= '0';
			mst_reset_r1 <= '0';
			mst_reprog <= '0'; -- unused yet
		elsif rising_edge(master_if_clk) then
			-- synchrnonize input
			mst_reset_r <= prm_ctrl_in(ctl_to_prm_wdog_bit); 
			mst_reset_r0 <= mst_reset_r;
			mst_reset_r1 <= mst_reset_r0;
			-- increment on falling edge of watchdog reset from master 
			if mst_reset_r1 = '1' and mst_reset_r0 = '0' then
				mst_reset_cnt <= mst_reset_cnt + 1;
			end if;
		end if;
	end process;
	-- AKU
	-- The "PROG" pins of the other FPGAs are not available to the PRM
	-- The only way to reprogram the system is to control the internal prog via JTAG
	-- we need to add a small controller to do this


end RTL;
