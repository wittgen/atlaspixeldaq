----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:42:33 06/06/2014 
-- Design Name: 
-- Module Name:    ace_programmer - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity ace_programmer is
port(
	clock					:	in	std_logic;
	rst                                     :	in	std_logic;
	fifo_data                               :	in	std_logic_vector(31 downto 0);
	wr_en					:	in	std_logic;
	ace_control                             :	in	std_logic_vector(31 downto 0);
	ace_status_read                         :	out	std_logic_vector(31 downto 0);
	tdi					:	out	std_logic;
	tck					:	out	std_logic;
	tms					:	out	std_logic;
	tdo					:	in	std_logic
	);
end ace_programmer;

architecture Behavioral of ace_programmer is

COMPONENT ACE_FIFO
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
	 valid : OUT STD_LOGIC
  );
END COMPONENT;

    COMPONENT player_nty
    PORT(
			shift_count_o :out std_logic_vector(2 downto 0);
         tms : OUT  std_logic;
         tck : OUT  std_logic;
         tdi : OUT  std_logic;
         rdy : OUT  std_logic;
         error : OUT  std_logic;
         eof_done : OUT  std_logic;
         rst : IN  std_logic;
         clk : IN  std_logic;
         load : IN  std_logic;
         tdo : IN  std_logic;
         prog_cntr : OUT  std_logic_vector(6 downto 0);
         data : IN  std_logic_vector(7 downto 0)
        );
    END COMPONENT;
    
	type fifo_player_state is (idle_s, wait_s, load_s, empty_s, initial_s);
	signal state:fifo_player_state:=initial_s;
   --Inputs
--   signal rst : std_logic := '0';
   signal clk : std_logic := '0';
   signal load : std_logic := '0';
--   signal tdo : std_logic;
--   signal data : std_logic_vector(7 downto 0) := (others => '0');

 	--Outputs
--   signal tms : std_logic;
--   signal tck : std_logic;
--   signal tdi : std_logic;
	signal shift_count_i : std_logic_vector (2 downto 0);
	signal shift_count_o : std_logic_vector (2 downto 0);
   signal rdy : std_logic;
   signal error : std_logic;
   signal eof_done : std_logic;
   signal prog_cntr : std_logic_vector(6 downto 0);

	
   signal din					:	std_logic_vector(31 downto 0);
   signal wr_en_i				:	std_logic;
   signal rd_en				:	std_logic;
   signal data_to_player	:	std_logic_vector(7 downto 0);
   signal fifo_full_i		:	std_logic;
   signal fifo_empty_i		:	std_logic;
	signal fifo_valid			:	std_logic;
	signal reset_player		:	std_logic;
	signal reset_fifo			:	std_logic;
	signal reset_fsm			:	std_logic;
	
	

begin

clk<=clock;
wr_en_i<=wr_en;
--fifo_full<=fifo_full_i;
--fifo_empty<=fifo_empty_i;
din<=fifo_data;

ace_status_read(3 downto 0)<= fifo_full_i & fifo_empty_i & eof_done & error;
ace_status_read(10 downto 4)<= prog_cntr;
ace_status_read(13 downto 11)<= shift_count_i;
ace_status_read(21 downto 14)<=data_to_player;
ace_status_read(31 downto 22)<= (others=>'0');

reset_player<=not(ace_control(0)) and rst; --active low
reset_fifo<=ace_control(1) or not(rst);  --active high

reset_fsm <= reset_fifo or not(reset_player);

--pause_exec<=ace_control(2); --not implemented in the state machine
--speed_set<=ace_control(4 downto 3); --Maybe we could change the clock of the player?
--



player_instance: player_nty PORT MAP (
			 shift_count_o => shift_count_i,
          tms => tms,
          tck => tck,
          tdi => tdi,
          rdy => rdy,
          error => error,
          eof_done => eof_done,
          rst => reset_player,
          clk => clk,
          load => load,
          tdo => tdo,
          prog_cntr => prog_cntr,
          data => data_to_player
        );

ace_fifo_instance : ACE_FIFO
  PORT MAP (
    rst => reset_fifo,
    wr_clk => clk,
    rd_clk => clk,
    din => din,
    wr_en => wr_en_i,
    rd_en => rd_en,
    dout => data_to_player,
    full => fifo_full_i,
    empty => fifo_empty_i,
	 valid => fifo_valid
  );
  

data_controller:process(clk,rst)
	begin
	if (reset_fsm ='1') then
		state<=initial_s;
	elsif( rising_edge(clk)) then
		case state is
			when initial_s =>
				rd_en<='0';
				load<='0';
				if fifo_empty_i='0' then
					state<=idle_s;
				end if;
			when idle_s =>
				rd_en<='0';
				load<='0';
				if (fifo_empty_i = '0' and rdy = '1') then
					state <= load_s;
					rd_en<='1';
				elsif (fifo_empty_i ='1') then
					state<=empty_s;
				end if;
			when load_s =>
				rd_en<='0';
				if (fifo_valid='1' and fifo_empty_i='0') then
					load<='1';
					state<= wait_s;
				elsif (fifo_valid='1' and fifo_empty_i ='1') then
					load<='1';
					state<=initial_s;--empty_s;
				end if;
			when wait_s =>
				load<='0';
				if (rdy ='0') then
					state<=idle_s;
				end if;
			when empty_s =>
				load<='0';
				rd_en<='0';
				if (fifo_empty_i='0') then
					rd_en<='1';
					state<=idle_s;
				end if;
		end case;
	end if;		
end process;
end Behavioral;

