#ifndef ENV_H__
#define ENV_H__

typedef struct {
  const char *name;
  unsigned int size;
  unsigned char *default_value;
} env_entry_t;

#define ENV_OK			0		// environment variable was loaded successfully
#define ENV_NOTFOUND		-1		// environment variable was not found
#define ENV_DEFAULT		-2		// env. variable was found, but will be set to default

int env_get(const char *name, unsigned char *value);
int env_set(const char *name, unsigned char *value);
int env_load(void);
int env_save(void);

#endif /* ENV_H__ */