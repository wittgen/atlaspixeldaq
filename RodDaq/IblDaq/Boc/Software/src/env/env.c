#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<xil_types.h>
#include<xstatus.h>
#include<xparameters.h>

#include "env.h"
#include "flash.h"

unsigned char mac_default[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };
unsigned char ip_default[] = {192, 168, 0, 10};
unsigned char gw_default[] = {192, 168, 0, 1};
unsigned char subnet_default[] = {255, 255, 255, 0};
unsigned char ntpserver_default[] = {255, 255, 255, 255};      // pool.ntp.org
unsigned char syslog_default[] = {255, 255, 255, 255};
unsigned char coarse_default[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
unsigned char enet_mode_default = 0xFF;

#define ENV_PAGES     2
#define ENV_SECTOR    (0x800000-(ENV_PAGES)*0x010000)

unsigned char environment_memory[ENV_PAGES*FLASH_PAGE_SIZE];

env_entry_t env_entries[] = {
  { .name = "mac", .size = 6, .default_value = mac_default },		// MAC address
  { .name = "ip", .size = 4, .default_value = ip_default },		// IP address
  { .name = "gw", .size = 4, .default_value = gw_default },		// Gateway
  { .name = "subnet", .size = 4, .default_value = subnet_default },	// Subnet-Mask
  { .name = "ntpserver", .size = 4, .default_value = ntpserver_default }, // NTP server for time synchronization
  { .name = "syslog", .size = 4, .default_value = syslog_default }, // SYSLOG-NG server
  { .name = "coarse", .size = 32, .default_value = coarse_default }, // coarse delay settings
  { .name = "enet_mode", .size = 1, .default_value = &enet_mode_default }, // ethernet mode
};

int env_get(const char *name, unsigned char *value)
{
  int entry = 0;
  int entry_found = 0;
  unsigned short offset = 0;
  int i;
  unsigned char mask = 0xFF;
  
  /* loop through all environment variables to find matching */
  for(entry = 0; entry < sizeof(env_entries)/sizeof(env_entries[0]); entry++)
  {
    if(strcasecmp(name, env_entries[entry].name) == 0)
    {
      entry_found = 1;
      break;
    }
    
    offset += env_entries[entry].size;
  }
  
  /* check if an entry was found */
  if(!entry_found)
  {
    return ENV_NOTFOUND;
  }

  /* load entry from eeprom and check for all values 0xFF (empty eeprom) */
  for(i = 0; i < env_entries[entry].size; i++)
  {
    value[i] = environment_memory[offset+i];
    mask = mask & value[i];
  }
  
  /* need to load default environment ? */
  //mask = 0xFF;
  if(mask == 0xFF)
  {
    for(i = 0; i < env_entries[entry].size; i++)
    {
      value[i] = env_entries[entry].default_value[i];
    }
    
    return ENV_DEFAULT;
  }
  else
  {
    return ENV_OK;
  }
}

int env_set(const char *name, unsigned char *value)
{
  int entry = 0;
  int entry_found = 0;
  unsigned short offset = 0;
  int i;
  
  /* loop through all environment variables to find matching */
  for(entry = 0; entry < sizeof(env_entries)/sizeof(env_entries[0]); entry++)
  {
    if(strcasecmp(name, env_entries[entry].name) == 0)
    {
      entry_found = 1;
      break;
    }
    
    offset += env_entries[entry].size;
  }
  
  /* check if an entry was found */
  if(!entry_found)
  {
    return ENV_NOTFOUND;
  }

  /* load entry from eeprom and check for all values 0xFF (empty eeprom) */
  for(i = 0; i < env_entries[entry].size; i++)
  {
    environment_memory[offset+i] = value[i];
  }
  
  return ENV_OK;
}

int env_load(void)
{
  int i;

  xil_printf("Loading environment from flash.\r\n");

  for(i = 0; i < ENV_PAGES; i++)
  {
    if(FlashReadPage(ENV_SECTOR+(i*FLASH_PAGE_SIZE), &environment_memory[0+(i*FLASH_PAGE_SIZE)]) != XST_SUCCESS)
    {
      xil_printf("Error reading environment page from flash.\r\n");
      return XST_FAILURE;
    }
  }

  return XST_SUCCESS;
}

int env_save(void)
{
  int i;

  xil_printf("Saving environment to flash.\r\n");

  /* erase env sector */
  if(FlashEraseSector(ENV_SECTOR) != XST_SUCCESS)
  {
    xil_printf("Error erasing environment in flash.\r\n");
    return XST_FAILURE;
  }

  /* write pages */
  for(i = 0; i < ENV_PAGES; i++)
  {
    if(FlashWritePage(ENV_SECTOR+(i*FLASH_PAGE_SIZE), &environment_memory[0+(i*FLASH_PAGE_SIZE)]) != XST_SUCCESS)
    {
      xil_printf("Error writing environment page to flash.\r\n");
      return XST_FAILURE;
    }
  }

  return XST_SUCCESS;
}
