/*
 * boc.h
 *
 *  Created on: Jun 6, 2012
 *      Author: wensing
 */

#ifndef BOC_H_
#define BOC_H_

#include <stdint.h>

#include "boc_registers.h"

extern uint8_t boc_read_register(uint16_t address);
extern void boc_write_register(uint16_t address, uint8_t value);
extern void boc_reset_bmf(void);
extern void boc_reset_bcf(void);
extern void boc_coarse_load(unsigned int mask);
extern void boc_coarse_save(unsigned int mask);


#endif /* BOC_H_ */
