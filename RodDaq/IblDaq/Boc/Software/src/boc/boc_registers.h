/*
 * boc_registers.h
 *
 *  Created on: Jun 6, 2012
 *      Author: wensing
 */

#ifndef BOC_REGISTERS_H_
#define BOC_REGISTERS_H_

// base addresses
#define BCF_BASE		0x0000
#define BMF_S_BASE		0x4000
#define BMF_N_BASE		0x8000
#define BCF_GPMEM_BASE	0xC000

// BMF base addresses
#define BMF_GP_BASE		0x0000
#define BMF_TX_BASE		0x0C00
#define BMF_RX_BASE		0x0800

#endif /* BOC_REGISTERS_H_ */
