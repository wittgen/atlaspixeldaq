/*
 * boc.c
 *
 *  Created on: Jun 6, 2012
 *      Author: wensing
 */

#include<stdlib.h>
#include<stdint.h>
#include<stdio.h>

#include "xparameters.h"
#include "boc.h"
#include "env.h"

uint8_t boc_read_register(uint16_t address)
{
	volatile uint32_t *ptr = (volatile uint32_t *) XPAR_AXI_EPC_0_PRH0_BASEADDR;
	uint8_t value = (uint8_t) (ptr[address] & 0xFF);

	//xil_printf("Reading %04X: %02X\r\n", address, value);

	return value;
}

void boc_write_register(uint16_t address, uint8_t value)
{
	volatile uint32_t *ptr = (volatile uint32_t *) XPAR_AXI_EPC_0_PRH0_BASEADDR;

	/* catch reset BCF writes */
	if(address == 39)
	{
		if (value == 0xAB)
		{
			xil_printf("Rebooting BCF...\r\n");
		}
		else if (value == 0x54)
		{
			xil_printf("Rebooting BCF into recovery mode...\r\n");
		}
	}

	//xil_printf("Writing %04X: %02X\r\n", address, value);

	ptr[address] = value;
}

void boc_reset_bmf(void)
{
	// wait until programming unit is ready
	while(((boc_read_register(65) & 0x04) != 0) || ((boc_read_register(73) & 0x04) != 0));

	// reset the address registers
	// north
	boc_write_register(68, 0x00);
	boc_write_register(69, 0x00);
	boc_write_register(70, 0x00);
	// south
	boc_write_register(76, 0x00);
	boc_write_register(77, 0x00);
	boc_write_register(78, 0x00);

	// security register
	boc_write_register(42, 0x42);

	// start JTAG programming
	boc_write_register(64, 0x04);
	boc_write_register(72, 0x04);

	// wait until programming is finished
	while(((boc_read_register(65) & 0x04) != 0) || ((boc_read_register(73) & 0x04) != 0));
}

void boc_reset_bcf(void)
{
	// security register
	boc_write_register(42, 0x42);
	
	// start reset mechanism
	boc_write_register(39, 0xAB);

	// wait until reset
	while(1);
}

void boc_coarse_load(unsigned int mask)
{
    unsigned char coarse_values[32];

    // get the coarse values from environment
    env_get("coarse", coarse_values);

    // print values
    for(int i = 0; i < 32; i++)
        xil_printf("TX %d: coarse=%d\r\n", i, coarse_values[i]);
    
    // load values
    for(int i = 0; i < 32; i++)
    {
    	if(mask & (1 << i))
    	{
	        if(i >= 16)
	            boc_write_register(BMF_S_BASE | BMF_TX_BASE | ((i % 16) << 5) | 0x5, coarse_values[i]);
	        else
	            boc_write_register(BMF_N_BASE | BMF_TX_BASE | ((i % 16) << 5) | 0x5, coarse_values[i]);
	    }
    }
}

void boc_coarse_save(unsigned int mask)
{
    unsigned char coarse_values[32];

    // get values
    for(int i = 0; i < 32; i++)
    {
    	if(mask & (1 << i))
    	{
	        if(i >= 16)
	            coarse_values[i] = boc_read_register(BMF_S_BASE | BMF_TX_BASE | ((i % 16) << 5) | 0x5);
	        else
	            coarse_values[i] = boc_read_register(BMF_N_BASE | BMF_TX_BASE | ((i % 16) << 5) | 0x5);
	    }
    }

    // print values
    for(int i = 0; i < 32; i++)
        xil_printf("TX %d: coarse=%d\r\n", i, coarse_values[i]);

    // get the coarse values from environment
    env_set("coarse", coarse_values);
}