/*
 * ipbus.h
 *
 *  Created on: Jun 6, 2012
 *      Author: wensing
 */

#ifndef IPBUS_H_
#define IPBUS_H_

#define IPBUS_PORT		50000U

extern int start_ipbus_tcp(void);
extern int start_ipbus_udp(void);

#endif /* IPBUS_H_ */
