/*
 * ipbus.c
 *
 *  Created on: May 24, 2012
 *      Author: wensing
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <lwip/err.h>
#include <lwip/tcp.h>
#include "xparameters.h"

#include "ipbus.h"
#include "boc.h"

typedef union
{
	uint32_t header_32;

	uint8_t header_8[4];

	struct
	{
		unsigned int vers     :  4;
		unsigned int trans_id : 11;
		unsigned int words    :  9;
		unsigned int type     :  5;
		unsigned int d        :  1;
		unsigned int res      :  2;
	};
} ipbus_header_t;

static void header2bytes(ipbus_header_t *header, unsigned char *buf)
{
	uint32_t tmp;

	tmp = (header->vers << 28) |
	      (header->trans_id << 17) |
	      (header->words << 8) |
	      (header->type << 3) |
	      (header->d << 2) |
	      (header->res << 0);

	buf[3] = (tmp & 0xFF000000) >> 24;
	buf[2] = (tmp & 0x00FF0000) >> 16;
	buf[1] = (tmp & 0x0000FF00) >> 8;
	buf[0] = (tmp & 0x000000FF);
}

static void bytes2header(ipbus_header_t *header, unsigned char *buf)
{
	uint32_t tmp = 0;

	tmp = (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];

	header->vers = ((tmp & 0xF0000000) >> 28);
	header->trans_id = ((tmp & 0x0FFE0000) >> 17);
	header->words = ((tmp & 0x0001FF00) >> 8);
	header->type = ((tmp & 0x000000F8) >> 3);
	header->d = ((tmp & 0x00000004) >> 2);
	header->res = (tmp & 0x00000003);
}

err_t ipbus_recv_callback_tcp(void *arg, struct tcp_pcb *pcb, struct pbuf *p,err_t err)
{

	/* do not read the packet if we are not in ESTABLISHED state */
	if (!p) {
		tcp_close(pcb);
		tcp_recv(pcb, NULL);
		return ERR_OK;
	}

	char addr_string[20];
	ipbus_header_t request_header;
	ipbus_header_t response_header;
	uint8_t response_buffer[256];
	uint16_t response_length;
	struct pbuf *p_resp;

	/* indicate that the packet has been received */
	tcp_recved(pcb, p->len);

	if(p->len < 4)
	{
		return -2;
	}

	// convert header
	bytes2header(&request_header, p->payload);

	if(request_header.type == 0x1F) // Ping-Pong
	{
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = 0;
		response_header.type = 0x1F;
		response_header.d = 1;
		response_header.res = 0;

		header2bytes(&response_header, response_buffer);
		response_length = 4;
	}
	else if(request_header.type == 0x03) // Read
	{
		// prepare response header
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x03;
		response_header.d = 1;
		response_header.res = 0;
        
        header2bytes(&response_header, response_buffer);
        
		// extract base address
		uint16_t base_address = ((uint8_t*)p->payload)[4] | ((uint16_t)((uint8_t*)p->payload)[5] << 8);
        
		// extract data
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
            response_buffer[i+4] = boc_read_register(base_address);
            base_address++;
        }
		response_length = 4+request_header.words;
	}
	else if(request_header.type == 0x04) // Write
	{
		// extract address
		uint16_t base_address = ((uint8_t*)p->payload)[4] | ((uint16_t)((uint8_t*)p->payload)[5] << 8);
        
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
        	uint8_t value = ((uint8_t*)p->payload)[i+8];
        	boc_write_register(base_address, value);
        	base_address++;
        }
        
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x04;
		response_header.d = 1;
		response_header.res = 0;
        
		header2bytes(&response_header, response_buffer);
		response_length = 4;
	}
    else if(request_header.type == 0x08) // Non incrementing Read
	{
        // prepare response header
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x08;
		response_header.d = 1;
		response_header.res = 0;
        
        header2bytes(&response_header, response_buffer);
        
		// extract base address
		uint16_t base_address = ((uint8_t*)p->payload)[4] | ((uint16_t)((uint8_t*)p->payload)[5] << 8);
        
		// extract data
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
            response_buffer[i+4] = boc_read_register(base_address);
        }
		response_length = 4+request_header.words;
	}
    else if(request_header.type == 0x09) // Non incrementing Write
	{
		// extract address
		uint16_t base_address = ((uint8_t*)p->payload)[4] | ((uint16_t)((uint8_t*)p->payload)[5] << 8);
        
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
        	uint8_t value = ((uint8_t*)p->payload)[i+8];
        	boc_write_register(base_address, value);
        }
        
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x09;
		response_header.d = 1;
		response_header.res = 0;
        
		header2bytes(&response_header, response_buffer);
		response_length = 4;
	}
	else
	{
		// report error

		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = 0;
		response_header.type = request_header.type;
		response_header.d = 1;
		response_header.res = 2;

		header2bytes(&response_header, response_buffer);
		response_length = 4;
	}

	// send response
	p_resp = pbuf_alloc(PBUF_TRANSPORT,response_length, PBUF_POOL);
	memcpy(p_resp->payload, response_buffer, response_length);
	err = tcp_write(pcb, response_buffer, response_length, 1);

	pbuf_free(p_resp);
	pbuf_free(p);	// free the pbuf
	return ERR_OK;
}

err_t ipbus_accept_callback_tcp(void* arg, struct tcp_pcb* newpcb, err_t err)
{
	static int connection = 1;
		/* set the receive callback for this connection */
	tcp_recv(newpcb, ipbus_recv_callback_tcp);

	/* just use an integer number indicating the connection id as the
	   callback argument */
	tcp_arg(newpcb, (void*)connection);

	/* increment for subsequent accepted connections */
	connection++;

	return ERR_OK;
}

int start_ipbus_tcp(void)
{
	struct tcp_pcb *pcb;
	err_t err;
	const unsigned int port = IPBUS_PORT;

	/* create new udp pcb */
	pcb = tcp_new();
	if(pcb == NULL)
	{
		print("Couldn't create PCB! Out of memory!\r\n");
		return -1;
	}

	/* bind to port */
	err = tcp_bind(pcb, IP_ADDR_ANY, port);
	if (err != ERR_OK) {
		xil_printf("Unable to bind to port %d: err = %d\n\r", port, err);
		return -2;
	}

	/* we don't need arguments for accept callback */
	tcp_arg(pcb, NULL);

	/* listen for connections */
	pcb = tcp_listen(pcb);
	if (!pcb) {
		xil_printf("Out of memory while tcp_listen\n\r");
		return -3;
	}

	/* specify callback to use for incoming connections */
	tcp_accept(pcb, ipbus_accept_callback_tcp);	

	print("IPBus-Client started.\r\n");

	return 0;

}
