#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <xstatus.h>
#include <xparameters.h>
#include "errors.h"
#include "boc.h"

static int memtest_datalines()
{
	volatile unsigned int *ptr = XPAR_MCB_DDR2_S0_AXI_BASEADDR;

	// hello
	print("MEMTEST: Testing data lines\r\n");

	// let's do a moving-1 test
	for(unsigned int pattern = 0x1; pattern != 0; pattern = pattern << 1)
	{
		unsigned int readback;

		// write pattern
		*ptr = pattern;

		// read pattern
		readback = *ptr;

		// check
		if(readback != pattern)
		{
			print("MEMTEST: Readback error. Got 0x");
			putnum(readback);
			print(" when expecting 0x");
			putnum(pattern);
			print("\r\n");

			return 1;
		}
	}

	// finish
	return 0;
}

static int memtest_addrlines()
{
	volatile unsigned int *ptr = XPAR_MCB_DDR2_S0_AXI_BASEADDR;
	unsigned int addrmask = (XPAR_MCB_DDR2_S0_AXI_HIGHADDR - XPAR_MCB_DDR2_S0_AXI_BASEADDR) / 4;
	unsigned int pattern = 0xAAAAAAAA;
	unsigned int antipattern = ~pattern;

	// hello
	print("MEMTEST: Testing address lines\r\n");
	
	// fill power of two locations with pattern
	for(unsigned int offset = 1; (offset & addrmask) != 0; offset = offset << 1)
	{
		ptr[offset] = pattern;
	}

	// check for address bits stuck high
	ptr[0] = antipattern;
	for(unsigned int offset = 1; (offset & addrmask) != 0; offset = offset << 1)
	{
		unsigned int readback = ptr[offset];

		if(readback != pattern)
		{
			print("MEMTEST: Readback error @0x");
			putnum((unsigned int) &ptr[offset]);
			print(". Got 0x");
			putnum(readback);
			print(" when expecting 0x");
			putnum(pattern);
			print("\r\n");

			return 1;			
		}
	}
	ptr[0] = pattern;

	// check for address bits stuck low or shorted
	for(unsigned int offset = 1; (offset & addrmask) != 0; offset = offset << 1)
	{
		unsigned int readback;

		// write antipattern
		ptr[offset] = antipattern;

		// check addr 0
		readback = ptr[0];
		if(readback != pattern)
		{
			print("MEMTEST: Readback error @0x");
			putnum((unsigned int) &ptr[0]);
			print(". Got 0x");
			putnum(readback);
			print(" when expecting 0x");
			putnum(pattern);
			print("\r\n");

			return 2;
		}

		// check all other addresses
		for(unsigned int offset2 = 1; (offset2 & addrmask) != 0; offset2 = offset2 << 1)
		{
			// skip same address
			if(offset == offset2) continue;

			readback = ptr[offset2];
			if(readback != pattern)
			{
				print("MEMTEST: Readback error @0x");
				putnum((unsigned int) &ptr[offset2]);
				print(". Got 0x");
				putnum(readback);
				print(" when expecting 0x");
				putnum(pattern);
				print("\r\n");

				return 2;
			}
		}

		// write pattern back
		ptr[offset] = pattern;
	}

	// finish
	return 0;
}

static int memtest_pattern()
{
	volatile unsigned int *ptr = (volatile unsigned int *) XPAR_MCB_DDR2_S0_AXI_BASEADDR;
	const unsigned int pattern[] = {0x00000000, 0xFFFFFFFF, 0x55555555, 0xAAAAAAAA};

	// hello
	print("MEMTEST: Testing full memory with pattern\r\n");

	while((unsigned int)ptr < XPAR_MCB_DDR2_S0_AXI_HIGHADDR)
	{
		int i;

		for(i = 0; i < sizeof(pattern)/sizeof(pattern[0]); i++)
		{
			unsigned int readback;

			// write pattern
			*ptr = pattern[i];

			// readback
			readback = *ptr;

			if(readback != pattern[i])
			{
				print("MEMTEST: Readback error @0x");
				putnum((unsigned int) ptr);
				print(". Got 0x");
				putnum(readback);
				print(" when expecting 0x");
				putnum(pattern[i]);
				print("\r\n");
				return 3;
			}

			ptr++;
		}
	}

	return 0;
}

int memtest()
{
	int ret;

	// hello
	print("MEMTEST: Testing DDR2 memory @0x");
	putnum(XPAR_MCB_DDR2_S0_AXI_BASEADDR);
	print("\r\n");

	// first test the data lines
	ret = memtest_datalines();
	if(ret)	return ret;

	// then test address lines
	ret = memtest_addrlines();
	if(ret) return ret;

	// do a pattern test afterwards
	ret = memtest_pattern();
	if(ret) return ret;

	// finish
	print("MEMTEST: Passed\r\n");

	return 0;
}
