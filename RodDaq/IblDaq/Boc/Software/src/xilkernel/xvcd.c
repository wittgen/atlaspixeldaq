// XVCD implementation taken from
// http://debugmo.de/2012/02/xvcd-the-xilinx-virtual-cable-daemon/

#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include <stdio.h>
#include <string.h>
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwipopts.h"
#include "xvcd.h"
#include "xgpio.h"
#include "boc.h"

static XGpio xvcd_gpio;
static int jtag_state;
static int verbose;

enum
{
	test_logic_reset, run_test_idle,

	select_dr_scan, capture_dr, shift_dr,
	exit1_dr, pause_dr, exit2_dr, update_dr,

	select_ir_scan, capture_ir, shift_ir,
	exit1_ir, pause_ir, exit2_ir, update_ir,

	num_states
};

static void gpio_set(int i, int val)
{
	int tmp = XGpio_DiscreteRead(&xvcd_gpio, 1);

	if(val)
	{
		tmp |= (1 << i);
	}
	else
	{
		tmp &= ~(1 << i);
	}

	XGpio_DiscreteWrite(&xvcd_gpio, 1, tmp);
}

static inline int gpio_get(int i)
{
	if(XGpio_DiscreteRead(&xvcd_gpio, 1) & (1 << i))
		return 1;
	else
		return 0;
}

static int jtag_step(int state, int tms)
{
	static const int next_state[num_states][2] =
	{
		[test_logic_reset] = {run_test_idle, test_logic_reset},
		[run_test_idle] = {run_test_idle, select_dr_scan},

		[select_dr_scan] = {capture_dr, select_ir_scan},
		[capture_dr] = {shift_dr, exit1_dr},
		[shift_dr] = {shift_dr, exit1_dr},
		[exit1_dr] = {pause_dr, update_dr},
		[pause_dr] = {pause_dr, exit2_dr},
		[exit2_dr] = {shift_dr, update_dr},
		[update_dr] = {run_test_idle, select_dr_scan},

		[select_ir_scan] = {capture_ir, test_logic_reset},
		[capture_ir] = {shift_ir, exit1_ir},
		[shift_ir] = {shift_ir, exit1_ir},
		[exit1_ir] = {pause_ir, update_ir},
		[pause_ir] = {pause_ir, exit2_ir},
		[exit2_ir] = {shift_ir, update_ir},
		[update_ir] = {run_test_idle, select_dr_scan}
	};

	return next_state[state][tms];
}

static int sread(int sd, void *target, int len)
{
	unsigned char *t = target;
	while (len)
	{
		int r = read(sd, t, len);
		if (r <= 0)
			return r;
		t += r;
		len -= r;
	}
	return 1;
}

int xvcd_server_thread(void)
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	int size;

	verbose=0;

	/* initialize GPIO */
	XGpio_Initialize(&xvcd_gpio, XVCD_GPIO);
	XGpio_SetDataDirection(&xvcd_gpio, 1, (1 << XVCD_TDO));		// make only TDO an input
	XGpio_DiscreteWrite(&xvcd_gpio, 1, 0x00);					// set all to low

	/* create a TCP socket */
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return -1;
	}

	/* set TCP_NODELAY */
	int on = 1;
	setsockopt( sock, IPPROTO_TCP, TCP_NODELAY, (char *) &on, sizeof( on )); 

	/* bind to port 80 at any interface */
	address.sin_family = AF_INET;
	address.sin_port = htons(XVCD_PORT);
	address.sin_addr.s_addr = INADDR_ANY;
	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0) {
		return -1;
	}

	/* listen for incoming connections */
	lwip_listen(sock, 0);
	xil_printf("XVCD: Listening to incoming connections on TCP port %d.\r\n", XVCD_PORT);

	/* accept all incoming connections */
	size = sizeof(remote);
	while (1) {
		new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
		xil_printf("XVCD: Accepted connection.\r\n");

		/* switch over to virtual cable */
	    xil_printf("XVCD: Giving JTAG control to the virtual cable.\n\r");
	    boc_write_register(0x0008, 0x54);

		/* spawn a separate handler for each request */
		sys_thread_new("xvcd", (void(*)(void*))xvcd_process_thread, (void*)new_sd,
                        THREAD_STACKSIZE,
                        1);
	}

	return 0;
}

int xvcd_process_thread(int sd)
{
	int i;
	int seen_tlr = 0;
	int ret = 0;

	do
	{
		char cmd[16];
		unsigned char buffer[2048], result[1024];
		
		if (sread(sd, cmd, 6) != 1)
		{
			ret = 1;
			break;
		}
		
		if (memcmp(cmd, "shift:", 6))
		{
			cmd[6] = 0;
			printf("XVCD: invalid cmd '%s'\r\n", cmd);
			ret = 1;
			break;
		}
		
		int len;
		if (sread(sd, &len, 4) != 1)
		{
			printf("XVCD: reading length failed\r\n");
			ret = 1;
			break;
		}
		
		int nr_bytes = (len + 7) / 8;
		if (nr_bytes * 2 > sizeof(buffer))
		{
			printf("XVCD: buffer size exceeded\r\n");
			ret = 1;
			break;
		}
		
		if (sread(sd, buffer, nr_bytes * 2) != 1)
		{
			printf("XVCD: reading data failed\r\n");
			ret = 1;
			break;
		}
		
		memset(result, 0, nr_bytes);

		if (verbose)
		{
			printf("XVCD: #");
			for (i = 0; i < nr_bytes * 2; ++i)
				printf("%02x ", buffer[i]);
			printf("\n");
		}

		//
		// Only allow exiting if the state is rti and the IR
		// has the default value (IDCODE) by going through test_logic_reset.
		// As soon as going through capture_dr or capture_ir no exit is
		// allowed as this will change DR/IR.
		//
		seen_tlr = (seen_tlr || jtag_state == test_logic_reset) && (jtag_state != capture_dr) && (jtag_state != capture_ir);
		
		
		//
		// Due to a weird bug(??) xilinx impacts goes through another "capture_ir"/"capture_dr" cycle after
		// reading IR/DR which unfortunately sets IR to the read-out IR value.
		// Just ignore these transactions.
		//
		
		if ((jtag_state == exit1_ir && len == 5 && buffer[0] == 0x17) || (jtag_state == exit1_dr && len == 4 && buffer[0] == 0x0b))
		{
			if (verbose)
				printf("XVCD: ignoring bogus jtag state movement in jtag_state %d\r\n", jtag_state);
		} else {
			for (i = 0; i < len; ++i)
			{
				//
				// Do the actual cycle.
				//
				
				int tms = !!(buffer[i/8] & (1<<(i&7)));
				int tdi = !!(buffer[nr_bytes + i/8] & (1<<(i&7)));
				result[i / 8] |= gpio_get(XVCD_TDO) << (i&7);
				gpio_set(XVCD_TMS, tms);
				gpio_set(XVCD_TDI, tdi);
				gpio_set(XVCD_TCK, 1);
				gpio_set(XVCD_TCK, 0);
				
				//
				// Track the state.
				//
				jtag_state = jtag_step(jtag_state, tms);
			}
		}

		if (verbose)
		{
			xil_printf("XVCD: sending response %d bytes.\r\n", nr_bytes);
			printf("XVCD: #");
			for (i = 0; i < nr_bytes; ++i)
				printf("%02x ", result[i]);
			printf("\n");
		}

		if (write(sd, result, nr_bytes) != nr_bytes)
		{
			printf("XVCD: error write");
			ret = 1;
			break;
		}

		if (verbose)
		{
			printf("XVCD: jtag state %d\r\n", jtag_state);
		}
	} while(1); //while (!(seen_tlr && jtag_state == run_test_idle));
	
	printf("XVCD: exiting...\r\n");
	close(sd);

	return ret;
}