#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include <stdio.h>
#include <string.h>
#include "lwip/inet.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwipopts.h"
#include "boc.h"
#include "ipbus.h"

typedef union
{
	uint32_t header_32;

	uint8_t header_8[4];

	struct
	{
		unsigned int vers     :  4;
		unsigned int trans_id : 11;
		unsigned int words    :  9;
		unsigned int type     :  5;
		unsigned int d        :  1;
		unsigned int res      :  2;
	};
} ipbus_header_t;

static void header2bytes(ipbus_header_t *header, unsigned char *buf)
{
	uint32_t tmp;

	tmp = (header->vers << 28) |
	      (header->trans_id << 17) |
	      (header->words << 8) |
	      (header->type << 3) |
	      (header->d << 2) |
	      (header->res << 0);

	buf[3] = (tmp & 0xFF000000) >> 24;
	buf[2] = (tmp & 0x00FF0000) >> 16;
	buf[1] = (tmp & 0x0000FF00) >> 8;
	buf[0] = (tmp & 0x000000FF);
}

static void bytes2header(ipbus_header_t *header, unsigned char *buf)
{
	uint32_t tmp = 0;

	tmp = (buf[3] << 24) | (buf[2] << 16) | (buf[1] << 8) | buf[0];

	header->vers = ((tmp & 0xF0000000) >> 28);
	header->trans_id = ((tmp & 0x0FFE0000) >> 17);
	header->words = ((tmp & 0x0001FF00) >> 8);
	header->type = ((tmp & 0x000000F8) >> 3);
	header->d = ((tmp & 0x00000004) >> 2);
	header->res = (tmp & 0x00000003);
}

/* http server */
int ipbus_server_thread()
{
	int sock, new_sd;
	struct sockaddr_in address, remote;
	int size;

	/* create a TCP socket */
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return -1;
	}

	/* bind to port 80 at any interface */
	address.sin_family = AF_INET;
	address.sin_port = htons(IPBUS_PORT);
	address.sin_addr.s_addr = INADDR_ANY;
	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof (address)) < 0) {
		return -1;
	}

	/* listen for incoming connections */
	lwip_listen(sock, 0);
	xil_printf("IPBUSD: Listening to incoming connections on TCP port %d.\r\n", IPBUS_PORT);

	size = sizeof(remote);
	while (1) {
		new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
		xil_printf("IPBUSD: Accepted connection.\r\n");
		/* spawn a separate handler for each request */
		sys_thread_new("ipbusd", (void(*)(void*))ipbus_process_thread, (void*)new_sd,
                        THREAD_STACKSIZE,
                        1);
	}
	return 0;
}

/* thread spawned for each connection */
void ipbus_process_thread(int sd)
{
	int read_len;
	int RECV_BUF_SIZE = 512;    	/* http request size can be a max of RECV_BUF_SIZE */
	unsigned char recv_buf[512];	/* since these buffers are allocated on the stack .. */
									/* .. care should be taken to ensure there are no overflows */

	/* read in data */
	while(1)
	{
		read_len = read(sd, recv_buf, RECV_BUF_SIZE);

		if(read_len <= 0)
		{
			// exit loop (connection in error or closed)
			break;
		}
		else if(read_len > 0)
		{
			// generate the response
			ipbus_generate_response(sd, recv_buf, read_len);
		}
	}

	/* close connection */
	xil_printf("IPBUSD: Closing connection.\r\n");
	close(sd);
}

/* processing request */
void ipbus_generate_response(int sd, unsigned char *req, int req_len)
{
	unsigned char transmit_buf[512];
	int transmit_len;
	ipbus_header_t request_header;
	ipbus_header_t response_header;

	if(req_len < 4)
		return;

	// convert header
	bytes2header(&request_header, req);

	if(request_header.type == 0x1F) // Ping-Pong
	{
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = 0;
		response_header.type = 0x1F;
		response_header.d = 1;
		response_header.res = 0;

		header2bytes(&response_header, transmit_buf);
		transmit_len = 4;
	}
	else if(request_header.type == 0x03) // Read
	{
		// prepare response header
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x03;
		response_header.d = 1;
		response_header.res = 0;
        
        header2bytes(&response_header, transmit_buf);
        
		// extract base address
		uint16_t base_address = req[4] | ((uint16_t)req[5] << 8);
        
		// extract data
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
            transmit_buf[i+4] = boc_read_register(base_address);
            base_address++;
        }
		transmit_len = 4+request_header.words;
	}
	else if(request_header.type == 0x04) // Write
	{
		// extract address
		uint16_t base_address = (req[4] | ((uint16_t)req[5] << 8));
        
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
        	uint8_t value = req[i+8];
        	boc_write_register(base_address, value);
        	base_address++;
        }
        
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x04;
		response_header.d = 1;
		response_header.res = 0;
        
		header2bytes(&response_header, transmit_buf);
		transmit_len = 4;
	}
    else if(request_header.type == 0x08) // Non incrementing Read
	{
        // prepare response header
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x08;
		response_header.d = 1;
		response_header.res = 0;
        
        header2bytes(&response_header, transmit_buf);
        
		// extract base address
		uint16_t base_address = (req[4] | ((uint16_t)req[5] << 8));
        
		// extract data
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
            transmit_buf[i+4] = boc_read_register(base_address);
        }
		transmit_len = 4+request_header.words;
	}
    else if(request_header.type == 0x09) // Non incrementing Write
	{
		// extract address
		uint16_t base_address = (req[4] | ((uint16_t)req[5] << 8));
        
        uint32_t i = 0;
        for (i=0; i<request_header.words; i++) {
        	uint8_t value = req[i+8];
        	boc_write_register(base_address, value);
        }
        
		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = request_header.words;
		response_header.type = 0x09;
		response_header.d = 1;
		response_header.res = 0;
        
		header2bytes(&response_header, transmit_buf);
		transmit_len = 4;
	}
	else
	{
		// report error

		response_header.vers = 1;
		response_header.trans_id = request_header.trans_id;
		response_header.words = 0;
		response_header.type = request_header.type;
		response_header.d = 1;
		response_header.res = 2;

		header2bytes(&response_header, transmit_buf);
		transmit_len = 4;
	}

	// transmit response
	if(lwip_write(sd, transmit_buf, transmit_len) != transmit_len)
	{
		xil_printf("Error writing to socket.\r\n");
	}
}