#ifndef TIMER_H__
#define TIMER_H__

extern void timer_init(void);
extern void timer_thread(void);
extern uint32_t timer_gettime(void);
extern uint32_t timer_getuptime(void);

struct struct_ntppacket {
	uint8_t Flags;
	uint8_t Stratum;
	uint8_t Poll;
	uint8_t Precision;
	uint32_t RootDelay;
	uint32_t RootDispersion;
	uint8_t RefIdentifier[4];
	uint32_t RefTimestamp[2];
	uint32_t OrgTimestamp[2];
	uint32_t RecvTimestamp[2];
	uint32_t SendTimestamp[2];
} __attribute__ ((packed));

typedef struct struct_ntppacket ntp_packet_t;

#endif /* TIMER_H__ */