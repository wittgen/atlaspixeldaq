#ifndef XVCD_H__
#define XVCD_H__

#include "xparameters.h"

#define XVCD_PORT			2542
#define THREAD_STACKSIZE 	16384
#define XVCD_GPIO			XPAR_AXI_GPIO_XVC_DEVICE_ID
#define XVCD_TCK			0
#define XVCD_TMS			1
#define XVCD_TDI			2
#define XVCD_TDO			3

extern int xvcd_server_thread(void);
extern int xvcd_process_thread(int sd);

#endif