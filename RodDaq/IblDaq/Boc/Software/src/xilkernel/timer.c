#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#endif
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "sys/intr.h"
#include "sys/timer.h"
#include "lwip/sockets.h"
#include "lwip/ip_addr.h"
#include "lwip/inet.h"
#include "platform.h"
#include "xparameters.h"
#include "xtmrctr.h"
#include "env.h"
#include "timer.h"

#define TIMER_DEVICE_ID			XPAR_AXI_TIMER_1_DEVICE_ID
#define TIMER_INTERRUPT_MASK	XPAR_AXI_TIMER_1_INTERRUPT_MASK
#define TIMER_INTERRUPT_INTR 	XPAR_MICROBLAZE_0_INTC_AXI_TIMER_1_INTERRUPT_INTR
#define TIMER_FREQ				XPAR_AXI_TIMER_1_CLOCK_FREQ_HZ

static XTmrCtr tmrctr;
static volatile uint32_t timer_time = 0;
static volatile uint32_t timer_uptime = 0;

static void timer_handler(void *callback)
{
	timer_time++;
	timer_uptime++;
	XTmrCtr_Reset(&tmrctr, 0);
	acknowledge_interrupt(TIMER_INTERRUPT_INTR);
}

void timer_init(void)
{
	int ret;

	// clear time
	timer_time = 0;
	timer_uptime = 0;

	// initialize timer
	ret = XTmrCtr_Initialize(&tmrctr, TIMER_DEVICE_ID);
	if(ret != XST_SUCCESS)
	{
		printf("TIMER: Failed to initialize hardware timer.\r\n");
		return;
	}

	// register interrupt handler
	ret = register_int_handler(TIMER_INTERRUPT_INTR, timer_handler, NULL);
	if(ret != XST_SUCCESS)
	{
		printf("TIMER: failed to register interrupt handler.\r\n");
		return;
	}

	// enable interrupt
	enable_interrupt(TIMER_INTERRUPT_INTR);
	printf("TIMER: enabled interrupt.\r\n");

	// set some timer options
	XTmrCtr_SetOptions(&tmrctr, 0, XTC_INT_MODE_OPTION | XTC_AUTO_RELOAD_OPTION | XTC_DOWN_COUNT_OPTION);
	XTmrCtr_SetResetValue(&tmrctr, 0, TIMER_FREQ-1);
	printf("TIMER: Options set.\r\n");

	// start the timer
	XTmrCtr_Start(&tmrctr, 0);
}

void timer_thread(void)
{
	int socketfd;
	struct sockaddr_in serveraddr, clientaddr;
	struct ip_addr serverip;
	uint8_t serverip_env[4];
	uint8_t buf[64];
	ntp_packet_t packet;
	int ret;

	/* create new UDP socket */
	socketfd = socket(AF_INET, SOCK_DGRAM, 0);
	if(socketfd < 0)
	{
		printf("TIMER: could not create UDP socket for SNTP communication\r\n");
		return;
	}

	/* get server IP from environment */
	env_get("ntpserver", serverip_env);

	/* check if enabled */
	if((serverip_env[0] == 0xFF) && (serverip_env[1] == 0xFF) && (serverip_env[2] == 0xFF) && (serverip_env[3] == 0xFF))
	{
		printf("TIMER: NTP service disabled.\n");
		return;
	}
	
	/* set server IP */
	IP4_ADDR(&serverip,  serverip_env[0], serverip_env[1], serverip_env[2], serverip_env[3]);
	printf("TIMER: using NTP server %d.%d.%d.%d\r\n", ip4_addr1(&serverip), ip4_addr2(&serverip),
            ip4_addr3(&serverip), ip4_addr4(&serverip));

	/* setup server address */
	memset(&serveraddr, 0, sizeof(struct sockaddr_in));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_port = htons(123);		// NTP port
	inet_addr_from_ipaddr(&serveraddr.sin_addr, &serverip);

	/* setup client addr */
	memset(&clientaddr, 0, sizeof(struct sockaddr_in));
	clientaddr.sin_family = AF_INET;
	clientaddr.sin_addr.s_addr = INADDR_ANY;
	clientaddr.sin_port = htons(34994);		// random high port

	while(1)
	{
		/* prepare NTP request */
		memset(&packet, 0, sizeof(ntp_packet_t));
		packet.Flags = 0x23;		// request v4
		packet.SendTimestamp[0] = htonl(timer_time);

		/* copy packet to buffer */
		memcpy(buf, &packet, sizeof(ntp_packet_t));

		/* bind to clientaddr */
		ret = bind(socketfd, (struct sockaddr *) &clientaddr, sizeof(struct sockaddr_in));
		if(ret < 0)
		{
			printf("TIMER: failed to bind to port %d.\r\n", ntohs(clientaddr.sin_port));
			close(socketfd);
			return;
		}

		/* send out NTP packet */
		ret = sendto(socketfd, buf, 48, 0, (struct sockaddr *) &serveraddr, sizeof(serveraddr));
		if(ret < 0)
		{
			close(socketfd);
			printf("TIMER: Error sending request.\n");
			return;
		}

		/* wait for response */
		ret = recv(socketfd, buf, 48, 0);
		if(ret > 0)
		{
			printf("TIMER: Got %d bytes as response.\r\n", ret);
		}

		/* check if response is valid */
		if(buf[0] == 0x24)
		{
			// copy packet
			memcpy(&packet, buf, sizeof(ntp_packet_t));

			// extract time
			uint32_t new_time = ntohl(packet.SendTimestamp[0]);

			// set new system time
			printf("TIMER: Setting new system time from NTP: %lu\r\n", new_time);
			timer_time = new_time;
		}

		// sleep for 60 minutes
		sleep(3600000UL);
	}

	/* close socket */
	close(socketfd);
}

uint32_t timer_gettime(void)
{
	return timer_time;
}

uint32_t timer_getuptime(void)
{
	return timer_uptime;
}
