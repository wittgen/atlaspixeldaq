#ifndef IPBUS_H_
#define IPBUS_H_

#define IPBUS_PORT		50000U
#define THREAD_STACKSIZE 16384

extern int ipbus_server_thread();
extern void ipbus_process_thread(int sd);
extern void ipbus_generate_response(int sd, unsigned char *req, int req_len);

#endif /* IPBUS_H_ */
