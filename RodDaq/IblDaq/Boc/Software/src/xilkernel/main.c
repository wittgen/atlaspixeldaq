#if __MICROBLAZE__ || __PPC__
#include "xmk.h"
#include "sys/timer.h"
#include "xenv_standalone.h"
#endif
#include "xparameters.h"
#include "lwipopts.h"

#include "platform_config.h"
#include "platform.h"

#include "lwip/sockets.h"
#include "lwip/sys.h"
#include "lwip/init.h"
#include "netif/xadapter.h"

#include "xstatus.h"
#include "xgpio.h"
#include "boc.h"
#include "flash.h"
#include "env.h"
#include "ipbus.h"
#include "xvcd.h"
#include "primproc.h"
#include "timer.h"

#include <stdio.h>

#define THREAD_STACKSIZE    16384

extern void force_phy_100(struct netif *netif);

struct netif server_netif;

static void print_ip(char *msg, struct ip_addr *ip)
{
    print(msg);
    xil_printf("%d.%d.%d.%d\r\n", ip4_addr1(ip), ip4_addr2(ip),
            ip4_addr3(ip), ip4_addr4(ip));
}

static void print_ip_settings(struct ip_addr *ip, struct ip_addr *mask, struct ip_addr *gw)
{
    print_ip("Board IP: ", ip);
    print_ip("Netmask : ", mask);
    print_ip("Gateway : ", gw);
}

static void print_serial_number(void)
{
    /* read BOC serial number */
    xil_printf("Board serial number: %x-%x-%x-%x-%x-%x\r\n",
                    boc_read_register(0x000E),
                    boc_read_register(0x000D),
                    boc_read_register(0x000C),
                    boc_read_register(0x000B),
                    boc_read_register(0x000A),
                    boc_read_register(0x0009));
}

static void primprocd_thread(void)
{
    xil_printf("PRIMPROC: starting.\r\n");
    while(1)
    {
        primproc();
        sleep(10);
    }
}

static void blinki_thread(void)
{
    XGpio leds;

    XGpio_Initialize(&leds, XPAR_AXI_GPIO_0_DEVICE_ID);
    XGpio_SetDataDirection(&leds, 1, 0x00);
    XGpio_DiscreteSet(&leds, 1, 0x2);

    while(1)
    {
        XGpio_DiscreteWrite(&leds, 1, XGpio_DiscreteRead(&leds, 1) ^ 0x4);
        sleep(500);
    }
}

static void slink_reset_thread(void)
{
    /* deassert MGT reset */
    xil_printf("Releasing SLINK MGT reset...\r\n");
    boc_write_register(BMF_S_BASE + 16, 0x20);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x20);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x20);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x20);      // BMF north SLINK1
    sleep(2000);

    /* deassert HOLA reset */
    xil_printf("Releasing SLINK HOLA reset...\r\n");
    boc_write_register(BMF_S_BASE + 16, 0x00);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x00);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x00);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x00);      // BMF north SLINK1
    sleep(2000);

    /* Toggle SLINK reset */
    xil_printf("Releasing SLINK URESET...\r\n");
    boc_write_register(BMF_S_BASE + 16, 0x10);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x10);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x10);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x10);      // BMF north SLINK1
    sleep(1000);
    boc_write_register(BMF_S_BASE + 16, 0x00);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x00);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x00);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x00);      // BMF north SLINK1
    sleep(1000);
    boc_write_register(BMF_S_BASE + 16, 0x10);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x10);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x10);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x10);      // BMF north SLINK1
    sleep(1000);

    /* enable HOLA0 */
    xil_printf("Enabling HOLA0 for SLINK...\r\n");
    boc_write_register(BMF_S_BASE + 16, 0x14);      // BMF south SLINK0
    boc_write_register(BMF_S_BASE + 18, 0x14);      // BMF south SLINK1
    boc_write_register(BMF_N_BASE + 16, 0x14);      // BMF north SLINK0
    boc_write_register(BMF_N_BASE + 18, 0x14);      // BMF north SLINK1
}

#ifndef NO_PLL
static void pll_init(void)
{
    xil_printf("Resetting the PLL.\r\n");
    boc_write_register(0x0030, 0x40);
    while(boc_read_register(0x0031) & 0x01);

#ifdef PLL_LVDS
    xil_printf("Switching the PLL to LVDS for revC boards.\r\n");

    // set value for PLL control register (LVDS output)
    boc_write_register(0x0032, 0x0A);
    boc_write_register(0x0033, 0x00);
    boc_write_register(0x0034, 0xF7);
    boc_write_register(0x0035, 0x07);
    boc_write_register(0x0030, 0x20);

    // wait for writing to be finished
    while(boc_read_register(0x0031) & 0x01);

    // set value for PLL control register 2
    boc_write_register(0x0032, 0x1E);
    boc_write_register(0x0033, 0xF2);
    boc_write_register(0x0034, 0x39);
    boc_write_register(0x0035, 0x08);
    boc_write_register(0x0030, 0x21);

    // wait for writing to be finished
    while(boc_read_register(0x0031) & 0x01);
#else
    // set value for PLL control register (LVDS output)
    boc_write_register(0x0032, 0x0A);
    boc_write_register(0x0033, 0x00);
    boc_write_register(0x0034, 0xF7);
    boc_write_register(0x0035, 0x0F);
    boc_write_register(0x0030, 0x20);

    // wait for writing to be finished
    while(boc_read_register(0x0031) & 0x01);

    // set value for PLL control register 2
    boc_write_register(0x0032, 0x1E);
    boc_write_register(0x0033, 0xF2);
    boc_write_register(0x0034, 0x39);
    boc_write_register(0x0035, 0x08);
    boc_write_register(0x0030, 0x21);

    // wait for writing to be finished
    while(boc_read_register(0x0031) & 0x01);
#endif

    xil_printf("Resetting the PLL again.\r\n");
    boc_write_register(0x0030, 0x40);
    while(boc_read_register(0x0031) & 0x01);
}
#endif

int network_thread()
{
    struct netif *netif;
    struct ip_addr ipaddr, netmask, gw;
    int ret;

    netif = &server_netif;

    /* the mac address of the board. this should be unique per board */
    unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x0A, 0x0B, 0x0C };

    print("\r\n\r\n");
    print("-----BOC software (Xilkernel+Sockets) ------\r\n");
#ifdef RECOVERY
    print ("!!!! RECOVERY !!!!\r\n");
#endif
    xil_printf("Build on: " __DATE__ " " __TIME__ "\r\n");

#ifndef NO_PLL
    /* setup PLL for revB/C */
    pll_init();
#else
    /* switch to external clock for revD boards */

    /* output detected VME clock frequency */
    unsigned int vme_clkfreq = 0;
    vme_clkfreq = (unsigned int) boc_read_register(0x0021);
    vme_clkfreq |= (unsigned int) boc_read_register(0x0022) << 8;
    vme_clkfreq |= (unsigned int) boc_read_register(0x0023) << 16;
    vme_clkfreq |= (unsigned int) (boc_read_register(0x0024) & 0x0F) << 24;
    xil_printf("VME clock frequency is %d Hz.\r\n", vme_clkfreq);

    /* first check if clock is good */
    if((boc_read_register(0x0020) & 0x04) != 0)
    {
        xil_printf("VME clock is good. Switch over to VME clock.\r\n");
        xil_printf("Selecting PLL buffered clock.\r\n");
        boc_write_register(0x002A, 0x42);
        boc_write_register(0x0020, 0x01);

        /* check if switchover was successful */
        if(boc_read_register(0x0020) & 0x03 != 0x01)
        {
            xil_printf("Switchover was not successful.\r\n");
        }
    }
    else
    {
        xil_printf("Will not switch to VME clock as no valid VME clock was detected.\r\n");
    }
#endif

    /* initialize timer */
    timer_init();

    /* resetting BMFs */
    xil_printf("Resetting BMFs...\r\n");
    boc_reset_bmf();
    sleep(500);

    // switch jtag to jtag header
    xil_printf("Giving JTAG control to the virtual cable.\n\r");
    boc_write_register(0x0008, 0x54);

    /* init flash memory */
    ret = FlashInit();
    if(ret != XST_SUCCESS)
    {
        xil_printf("Error initializing flash memory!\r\n");
    }

    /* load environment */
    env_load();

    /* 
     * initialize MAC address from serial number if serial number is valid 
     * otherwise take environment one
     */
    if(boc_read_register(0x000F) == 0x03)
    {
        mac_ethernet_address[3] = boc_read_register(0x000B);
        mac_ethernet_address[4] = boc_read_register(0x000A);
        mac_ethernet_address[5] = boc_read_register(0x0009);
    }
    else
    {
        env_get("mac", mac_ethernet_address);
    }

    /* initliaze IP addresses to be used */
    uint8_t slot = boc_read_register(0x0019);
    uint8_t crate = boc_read_register(0x001A);
    
    /* load really default IP configuration if ENV fails */
    IP4_ADDR(&ipaddr, 192, 168, 0, 10);
    IP4_ADDR(&netmask, 255, 255, 255, 0);
    IP4_ADDR(&gw, 192, 168, 0, 1);
    
#ifndef RECOVERY
    /* check if slot and crate are set */
    if((slot == 0xFF) || (crate == 0xFF))
    {
        unsigned char env_ip[4], env_subnet[4], env_gw[4];
        int ret;
        
        ret = env_get("ip", env_ip);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading IP address from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default IP address...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load IP address from environment...\n\r"); break;
        }
        
        ret = env_get("gw", env_gw);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading gateway address from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default gateway address...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load gateway address from environment...\n\r"); break;
        }
        
        ret = env_get("subnet", env_subnet);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading netmask from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default netmask...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load netmask from environment...\n\r"); break;
        }
        
        IP4_ADDR(&ipaddr,  env_ip[0], env_ip[1], env_ip[2], env_ip[3]);
        IP4_ADDR(&netmask, env_subnet[0], env_subnet[1], env_subnet[2], env_subnet[3]);
        IP4_ADDR(&gw,      env_gw[0], env_gw[1], env_gw[2], env_gw[3]);
    }
    else
    {
        xil_printf("Loading slot based IP configuration...\n\r");
        IP4_ADDR(&ipaddr, 192, 168, 2, 10*slot+3);
        IP4_ADDR(&netmask, 255, 255, 255, 0);
        IP4_ADDR(&gw, 192, 168, 0, 254);
    }
#endif

    /* print out serial number & IP settings of the board */
    print_serial_number();
    print_ip_settings(&ipaddr, &netmask, &gw);

    /* Add network interface to the netif_list, and set it as default */
    if (!xemac_add(netif, &ipaddr, &netmask, &gw, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
        xil_printf("Error adding N/W interface\r\n");
        return 0;
    }
    netif_set_default(netif);

    /* specify that the network if is up */
    netif_set_up(netif);

    /* load coarse settings */
    boc_coarse_load(0xFFFFFFFFU);

    /* start packet receive thread - required for lwIP operation */
    sys_thread_new("xemacif_input_thread", (void(*)(void*))xemacif_input_thread, netif,
            THREAD_STACKSIZE,
            1);

    /* start timer thread */
    sys_thread_new("timer", (void(*)(void*)) timer_thread, 0, THREAD_STACKSIZE, 1);

    /* start IPBUS thread */
    sys_thread_new("ipbusd", (void(*)(void*))ipbus_server_thread, 0, THREAD_STACKSIZE, 1);

    /* start XVCD thread */
    sys_thread_new("xvcd", (void(*)(void*))xvcd_server_thread, 0, THREAD_STACKSIZE, 1);

    /* starting primitive processor */
    sys_thread_new("primprocd", (void(*)(void*))primprocd_thread, 0, THREAD_STACKSIZE, 1);

    /* start heartbeat thread */
    sys_thread_new("heartbeat", (void(*)(void*))blinki_thread, 0, THREAD_STACKSIZE, 1);

    /* start SLINK resetter */
    sys_thread_new("slink_reset", (void(*)(void*))slink_reset_thread, 0, THREAD_STACKSIZE, 1);

    // indicate good state
#ifndef RECOVERY
    boc_write_register(40, 0x02);       // green BCF LED
#else
    boc_write_register(40, 0x03);       // green+red BCF LED
#endif

	return 0;
}

int main_thread()
{
    /* init lwip */
    lwip_init();

    /* any thread using lwip should be created by using sys_thread_new */
    sys_thread_new("network_thread", network_thread, NULL, THREAD_STACKSIZE, 1);

    return 0;
}

int main(int argc, char **argv)
{
	// initialize platform
    if (init_platform() < 0) {
        xil_printf("ERROR initializing platform.\r\n");
        return -1;
    }

    // start the kernel
    xilkernel_main();

    while(1);	// we should never reach this

	return 0;
}
