#ifndef EEPROM_H__
#define EEPROM_H__

extern int eeprom_init(void);
extern unsigned char eeprom_scan(void);
extern unsigned char eeprom_read(unsigned short address);
extern void eeprom_write(unsigned short address, unsigned char value);

#endif
