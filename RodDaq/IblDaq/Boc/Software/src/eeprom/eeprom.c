#include<xil_io.h>
#include<xparameters.h>
#include "boc.h"
#include "eeprom.h"

static uint8_t eeprom_address = 0xFF;

int eeprom_init(void)
{
	/* scan for i2c device */
	uint8_t addr;
	
	for(addr = 0x0; addr < 127; addr++)
	{
		uint8_t ret;
		
		//xil_printf("Scanning EEPROM @0x%02X: ", addr);
		
		/* write address */
		boc_write_register(0x001C, addr);
		boc_write_register(0x001B, 0x10);
		
		/* try reading eeprom */
		boc_write_register(0x001D, 0);
		boc_write_register(0x001E, 0);
		boc_write_register(0x001B, 0x01);
		
		/* wait for finish */
		do {
			ret = boc_read_register(0x001B);
			
			//xil_printf("Status: 0x%02X\r\n", ret);
		}
		while(ret & 0x04);
	
		if(!(ret & 0x08))
		{
			xil_printf("Found EEPROM @0x%02X.\r\n", addr);
			eeprom_address = addr;
			//return addr;
		}
		else
		{
			//xil_printf("not found.\r\n");
		}
	}
	
	eeprom_address = 0x54;
	return 0x54;
	
	return 0xFF;
}

unsigned char eeprom_read(unsigned short address)
{
	uint8_t ret;
	
	//xil_printf("Reading from i2c-EERPOM@0x%02X\r\n", eeprom_address);
	
	if(eeprom_address == 0xFF)
		return 0xFF;
	
	/* write i2c address */
	boc_write_register(0x001C, eeprom_address);
	boc_write_register(0x001B, 0x10);
	
	/* put addr */
	boc_write_register(0x001D, address & 0xFF);
	boc_write_register(0x001E, address >> 8);
	
	/* start read */
	boc_write_register(0x001B, 0x01);
	
	/* wait for finish */
	do {
		ret = boc_read_register(0x001B);
		
		//xil_printf("Status: 0x%02X\r\n", ret);
	}
	while(ret & 0x04);
	
	if(ret & 0x08)
	{
		xil_printf("Error reading from EEPROM: 0x%02X!\r\n", ret);
		return 0xFF;
	}
	
	return boc_read_register(0x001C);
}

void eeprom_write(unsigned short address, unsigned char value)
{
	uint8_t ret;
	
	if(eeprom_address == 0xFF)
		return;
	
	/* write i2c address */
	boc_write_register(0x001C, eeprom_address);
	boc_write_register(0x001B, 0x10);
	
	/* put addr */
	boc_write_register(0x001C, value);
	boc_write_register(0x001D, address & 0xFF);
	boc_write_register(0x001E, address >> 8);
	
	/* start write */
	boc_write_register(0x001B, 0x02);
	
	/* wait for finish */
	do {
		ret = boc_read_register(0x001B);
		
		//xil_printf("Status: 0x%02X\r\n", ret);
	}
	while(ret & 0x04);
	
	if(ret & 0x08)
		xil_printf("Error writing to EEPROM: 0x%02X!\r\n", ret);
}
