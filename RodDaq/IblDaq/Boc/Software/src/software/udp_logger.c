/*
 * udp_logger.c
 *
 *  Created on: Jun 4, 2012
 *      Author: wensing
 */

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<lwip/err.h>
#include<lwip/udp.h>

int udp_logger(const char *str)
{
#ifdef DEBUG
	struct udp_pcb *pcb;
	struct pbuf *pbuf;
	int len = strlen(str) + 1;

	// create new pcb
	pcb = udp_new();
	if(pcb == NULL)
	{
		return -1;
	}

	// allocate a packet buffer
	pbuf = pbuf_alloc(PBUF_TRANSPORT, len, PBUF_POOL);
	if(pbuf == NULL)
	{
		udp_remove(pcb);
		return -2;
	}

	// fill pcb with log message
	memcpy(pbuf->payload, str, len);

	// send packet
	ip_addr_t dst_ip;
	dst_ip.addr = IPADDR_BROADCAST;
	udp_sendto(pcb, pbuf, &dst_ip, 50001);

	// clear packet buffer
	pbuf_free(pbuf);

	// clear pcb
	udp_remove(pcb);
#endif

	return 0;
}
