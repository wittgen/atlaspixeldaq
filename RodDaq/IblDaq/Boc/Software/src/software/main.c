/*
 * Copyright (c) 2009-2010 Xilinx, Inc.  All rights reserved.
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION.
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 *
 */

#include <stdio.h>

#include "xparameters.h"

#include "netif/xadapter.h"

#include "xaxiethernet.h"
#include "xaxidma.h"
#include "netif/xpqueue.h"

#include "platform.h"
#include "platform_config.h"

#include "ipbus.h"
#include "boc.h"
#include "eeprom.h"
#include "env.h"
#include "primproc.h"
#include "flash.h"

/* defined by each RAW mode application */
void print_app_header();
int start_application();
int transfer_data();

/* missing declaration in lwIP */
void lwip_init();

void blinki(void);

struct netif server_netif;

void
print_ip(char *msg, struct ip_addr *ip) 
{
	print(msg);
	xil_printf("%d.%d.%d.%d\n\r", ip4_addr1(ip), ip4_addr2(ip), 
			ip4_addr3(ip), ip4_addr4(ip));
}

void
print_ip_settings(struct ip_addr *ip, struct ip_addr *mask, struct ip_addr *gw)
{

	print_ip("Board IP: ", ip);
	print_ip("Netmask : ", mask);
	print_ip("Gateway : ", gw);
}

/* structure within each netif, encapsulating all information required for
 * using a particular temac instance
 */
typedef struct {
#ifdef XLWIP_CONFIG_INCLUDE_AXI_ETHERNET_FIFO
	XLlFifo      axififo;
#else
	XAxiDma      axidma;
#endif
	XAxiEthernet axi_ethernet;

	/* queue to store overflow packets */
	pq_queue_t *recv_q;
	pq_queue_t *send_q;

	/* pointers to memory holding buffer descriptors (used only with SDMA) */
	void *rx_bdspace;
	void *tx_bdspace;
} xaxiemacif_s;

void
force_phy_100(struct netif *netif)
{
	struct xemac_s *xemac = (struct xemac_s *)(netif->state);
	xaxiemacif_s *xaxiemacif = (xaxiemacif_s *)xemac->state;
	u16 tmp;

	// disable 1GbE due to hardware problems
	XAxiEthernet_PhyWrite(&xaxiemacif->axi_ethernet, 1, 0x09, 0x0000);

	// restart autonegotiation
	XAxiEthernet_PhyRead(&xaxiemacif->axi_ethernet, 1, 0x00, &tmp);
	tmp |= (1 << 9);
	XAxiEthernet_PhyWrite(&xaxiemacif->axi_ethernet, 1, 0x00, tmp);

	// wait until autonegotiation is finished
	do
	{
		XAxiEthernet_PhyRead(&xaxiemacif->axi_ethernet, 1, 0x00, &tmp);
	}
	while((tmp & (1 << 9)) != 0);
}

static eeprom_test(void)
{
	unsigned short addr;
	
	xil_printf("EEPROM-Test:\r\n\r\n");
	
	eeprom_write(0x0012, 0xEF);
	
	for(addr = 0; addr < 0x100; addr++)
	{
		if(addr % 16 == 0)
		{
			if(addr == 0)
			{
				xil_printf("\t+0x0 +0x1 +0x2 +0x3 +0x4 +0x5 +0x6 +0x7 +0x8 +0x9 +0xA +0xB +0xC +0xD +0xE +0xF");
			}
			xil_printf("\r\n0x%04X\t", addr);
		}
		
		xil_printf(" %02X  ", eeprom_read(addr));
	}
}

int main()
{
	struct netif *netif;
	struct ip_addr ipaddr, netmask, gw;
	uint8_t eeprom_address;
	int ret;
	volatile int delay;
	u16 timeout;

	/* the mac address of the board. this should be unique per board */
	unsigned char mac_ethernet_address[] = { 0x00, 0x0a, 0x35, 0x00, 0x01, 0x02 };

	xil_printf("BOC-Software\r\n");
#ifdef RECOVERY
    print ("!!!! RECOVERY !!!!\r\n");
#endif
	xil_printf("Build on: " __DATE__ " " __TIME__ "\r\n");

#if 0
	/* init eeprom */
	eeprom_address = eeprom_init();
	if(eeprom_address == 0xFF)
	{
		xil_printf("No EEPROM was found.\r\n");
	}
	else
	{
		xil_printf("EEPROM at address 0x%02X found.\r\n", eeprom_address);
	}
#endif


#ifndef NO_PLL
	xil_printf("Resetting the PLL.\r\n");
	boc_write_register(0x0030, 0x40);
	while(boc_read_register(0x0031) & 0x01);

#ifdef PLL_LVDS
	xil_printf("Switching the PLL to LVDS for revC boards.\r\n");

	// set value for PLL control register (LVDS output)
	boc_write_register(0x0032, 0x0A);
	boc_write_register(0x0033, 0x00);
	boc_write_register(0x0034, 0xF7);
	boc_write_register(0x0035, 0x07);
	boc_write_register(0x0030, 0x20);

	// wait for writing to be finished
	while(boc_read_register(0x0031) & 0x01);

	// set value for PLL control register 2
	boc_write_register(0x0032, 0x1E);
	boc_write_register(0x0033, 0xF2);
	boc_write_register(0x0034, 0x39);
	boc_write_register(0x0035, 0x08);
	boc_write_register(0x0030, 0x21);

	// wait for writing to be finished
	while(boc_read_register(0x0031) & 0x01);
#else
	// set value for PLL control register (LVDS output)
	boc_write_register(0x0032, 0x0A);
	boc_write_register(0x0033, 0x00);
	boc_write_register(0x0034, 0xF7);
	boc_write_register(0x0035, 0x0F);
	boc_write_register(0x0030, 0x20);

	// wait for writing to be finished
	while(boc_read_register(0x0031) & 0x01);

	// set value for PLL control register 2
	boc_write_register(0x0032, 0x1E);
	boc_write_register(0x0033, 0xF2);
	boc_write_register(0x0034, 0x39);
	boc_write_register(0x0035, 0x08);
	boc_write_register(0x0030, 0x21);

	// wait for writing to be finished
	while(boc_read_register(0x0031) & 0x01);
#endif

	xil_printf("Resetting the PLL again.\r\n");
	boc_write_register(0x0030, 0x40);
	while(boc_read_register(0x0031) & 0x01);
#endif

	/* reset serial number chip */
	//xil_printf("Resetting serial number chip.\r\n");
	//boc_write_register(0x000F, 0x04);
	//for(delay = 0; delay < 100; delay++);	// wait some time
	//boc_write_register(0x000F, 0x00);
	//for(delay = 0; delay < 100; delay++); 	// wait some time
	//xil_printf("Wait for serial number to be ready...\r\n");
	//while(boc_read_register(0x000F) & 0x01);
	//for(delay = 0; delay < 100000; delay++); 	// wait some time

	/* read BOC serial number */
	xil_printf("Board serial number: %x-%x-%x-%x-%x-%x\r\n",
					boc_read_register(0x000E),
					boc_read_register(0x000D),
					boc_read_register(0x000C),
					boc_read_register(0x000B),
					boc_read_register(0x000A),
					boc_read_register(0x0009));

	/* init flash memory */
	ret = FlashInit();
	if(ret != XST_SUCCESS)
	{
		xil_printf("Error initializing flash memory!\r\n");
	}

	/* load environment */
	env_load();

	/* read value from eeprom */
	/*xil_printf("EEPROM read from 0x0: 0x%02x\r\n", eeprom_read(0));
	xil_printf("EEPROM read from 0x1: 0x%02x\r\n", eeprom_read(1));
	eeprom_write(0, 0x12);
	xil_printf("EEPROM read from 0x0: 0x%02x\r\n", eeprom_read(0));
	xil_printf("EEPROM read from 0x1: 0x%02x\r\n", eeprom_read(1));
	//eeprom_write(0, 0xAB);
	xil_printf("EEPROM read from 0x0: 0x%02x\r\n", eeprom_read(0));
	xil_printf("EEPROM read from 0x1: 0x%02x\r\n", eeprom_read(1));
	//eeprom_write(0, 0xFF);*/
	//eeprom_test();

	netif = &server_netif;

	init_platform();

	/* 
	 * initialize MAC address from serial number if serial number is valid 
	 * otherwise take environment one
	 */
	if(boc_read_register(0x000F) == 0x03)
	{
		mac_ethernet_address[3] = boc_read_register(0x000B);
		mac_ethernet_address[4] = boc_read_register(0x000A);
		mac_ethernet_address[5] = boc_read_register(0x0009);
	}
	else
	{
		env_get("mac", mac_ethernet_address);
	}

	/* initliaze IP addresses to be used */
	uint8_t slot = boc_read_register(0x0019);
	uint8_t crate = boc_read_register(0x001A);
	
	/* load really default IP configuration if ENV fails */
	IP4_ADDR(&ipaddr, 192, 168, 0, 10);
	IP4_ADDR(&netmask, 255, 255, 255, 0);
	IP4_ADDR(&gw, 192, 168, 0, 1);
	
#ifndef RECOVERY
	/* check if slot and crate are set */
	if((slot == 0xFF) || (crate == 0xFF))
	{
		unsigned char env_ip[4], env_subnet[4], env_gw[4];
		int ret;
		
		ret = env_get("ip", env_ip);
		switch(ret)
		{
			case ENV_OK: xil_printf("Loading IP address from EEPROM...\n\r"); break;
			case ENV_DEFAULT: xil_printf("Loading default IP address...\n\r"); break;
			case ENV_NOTFOUND: xil_printf("Failed to load IP address from environment...\n\r"); break;
		}
		
		ret = env_get("gw", env_gw);
		switch(ret)
		{
			case ENV_OK: xil_printf("Loading gateway address from EEPROM...\n\r"); break;
			case ENV_DEFAULT: xil_printf("Loading default gateway address...\n\r"); break;
			case ENV_NOTFOUND: xil_printf("Failed to load gateway address from environment...\n\r"); break;
		}
		
		ret = env_get("subnet", env_subnet);
		switch(ret)
		{
			case ENV_OK: xil_printf("Loading netmask from EEPROM...\n\r"); break;
			case ENV_DEFAULT: xil_printf("Loading default netmask...\n\r"); break;
			case ENV_NOTFOUND: xil_printf("Failed to load netmask from environment...\n\r"); break;
		}
		
		IP4_ADDR(&ipaddr,  env_ip[0], env_ip[1], env_ip[2], env_ip[3]);
		IP4_ADDR(&netmask, env_subnet[0], env_subnet[1], env_subnet[2], env_subnet[3]);
		IP4_ADDR(&gw,      env_gw[0], env_gw[1], env_gw[2], env_gw[3]);
	}
	else
	{
		xil_printf("Loading slot based IP configuration...\n\r");
		IP4_ADDR(&ipaddr, 192, 168, 0, 10*slot);
		IP4_ADDR(&netmask, 255, 255, 255, 0);
		IP4_ADDR(&gw, 192, 168, 0, 254);
	}
#endif

	print_app_header();
	print_ip_settings(&ipaddr, &netmask, &gw);

	lwip_init();

  	/* Add network interface to the netif_list, and set it as default */
	if (!xemac_add(netif, &ipaddr, &netmask, &gw, mac_ethernet_address, PLATFORM_EMAC_BASEADDR)) {
		xil_printf("Error adding N/W interface\n\r");
		return -1;
	}
	netif_set_default(netif);
	
	#ifdef SLOW_ETH
		/*
	 	 * force netif to 100 MBit/s
	 	 */
	 	xil_printf("Setting PHY to 100 MBit/s for revision B boards.\n\r");
		force_phy_100(netif);
	#endif

	/* Create a new DHCP client for this interface.
	 * Note: you must call dhcp_fine_tmr() and dhcp_coarse_tmr() at
	 * the predefined regular intervals after starting the client.
	 */
	/* dhcp_start(netif); */

	/* now enable interrupts */
	platform_enable_interrupts();

	/* specify that the network if is up */
	netif_set_up(netif);

	// reset bmf
	boc_reset_bmf();

	// switch jtag to jtag header
	xil_printf("Giving JTAG control to JTAG headers.\n\r");
	boc_write_register(0x0008, 0xAB);

	// start ipbus service
	start_ipbus_udp();
	start_ipbus_tcp();

	// indicate good state
#ifndef RECOVERY
    boc_write_register(40, 0x02);       // green BCF LED
#else
    boc_write_register(40, 0x03);       // green+red BCF LED
#endif

	/* receive and process packets */
	while (1) {
		primproc();
		blinki();
		xemacif_input(netif);
		transfer_data();
	}
  
	/* never reached */
	cleanup_platform();

	return 0;
}
