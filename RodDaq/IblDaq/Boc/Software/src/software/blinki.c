/*
 * blinki.c
 *
 *  Created on: Jun 4, 2012
 *      Author: wensing
 */


#include<stdio.h>
#include<stdlib.h>
#include "xil_types.h"
#include "xtmrctr.h"
#include "xintc.h"
#include "xgpio.h"
#include "xparameters.h"

XGpio leds;

void blinki(void)
{
	static unsigned int init = 0;
	static unsigned int cnt = 0;

	if(init == 0)
	{
		XGpio_Initialize(&leds, XPAR_AXI_GPIO_0_DEVICE_ID);
		XGpio_SetDataDirection(&leds, 1, 0x00);
		XGpio_DiscreteSet(&leds, 1, 0x2);
		init = 1;
	}

	if(cnt == 200000)
	{
		cnt = 0;
		XGpio_DiscreteWrite(&leds, 1, XGpio_DiscreteRead(&leds, 1) ^ 0x4);
	}
	else
	{
		cnt++;
	}
}
