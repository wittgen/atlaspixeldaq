#include<stdio.h>
#include<stdlib.h>
#include<xspi.h>
#include<xil_types.h>
#include<xparameters.h>

#include "flash.h"

XSpi bcf_flash_spi;

/* SPI buffers */
#define SPI_BUFFER_SIZE		(FLASH_PAGE_SIZE+16)
unsigned char spi_rx[SPI_BUFFER_SIZE];
unsigned char spi_tx[SPI_BUFFER_SIZE];

static int FlashBusyWait(void)
{
	do {
		/* Read Status command */
		spi_tx[0] = 0x05;

		/* do SPI transfer */
		if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 2) != XST_SUCCESS)
		{
			return XST_FAILURE;
		}
	} while(spi_rx[1] & 0x01);		// poll WIP bit

	return XST_SUCCESS;
}

int FlashInit(void)
{
	/* initialize SPI module */
	if(XSpi_Initialize(&bcf_flash_spi, XPAR_SPI_0_DEVICE_ID) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* set SPI as master */
	if(XSpi_SetOptions(&bcf_flash_spi, XSP_MASTER_OPTION | XSP_MANUAL_SSELECT_OPTION) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* select first slave */
	if(XSpi_SetSlaveSelect(&bcf_flash_spi, 0x01) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* lets start the SPI driver and disable interrupt mode */
	XSpi_Start(&bcf_flash_spi);
	XSpi_IntrGlobalDisable(&bcf_flash_spi);
	XSpi_IntrDisable(&bcf_flash_spi, XSP_INTR_DFT_MASK);

	/* do a ID check */
	if(FlashIDCheck() != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}

void FlashDeInit(void)
{
	XSpi_Stop(&bcf_flash_spi);
	XSpi_Reset(&bcf_flash_spi);
}

int FlashIDCheck(void)
{
	/* prepare RDID instruction */
	spi_tx[0] = 0x9F;	/* RDID instruction */
	spi_tx[1] = 0xFF;	/* 3 dummy bytes */
	spi_tx[2] = 0xFF;
	spi_tx[3] = 0xFF;
	spi_rx[0] = 0x12;	/* 4 dummy bytes in receiver */
	spi_rx[1] = 0x34;
	spi_rx[2] = 0x56;
	spi_rx[3] = 0x78;

	/* start transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 4) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* compare ID */
	if((spi_rx[1] != 0x20) || (spi_rx[2] != 0x20) || (spi_rx[3] != 0x17))
	{
		return XST_FAILURE;
	}

	/* ID matches flash devide */
	return XST_SUCCESS;
}

int FlashReadPage(unsigned int addr, unsigned char *buffer)
{
	unsigned int i;

	/* prepare read command */
	spi_tx[0] = 0x03;
	spi_tx[1] = addr >> 16;
	spi_tx[2] = addr >> 8;
	spi_tx[3] = addr;

	/* dummy bytes */
	for(i = 4; i < FLASH_PAGE_SIZE+4; i++)
		spi_tx[i] = 0xFF;

	/* do SPI transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, FLASH_PAGE_SIZE+4) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}
	else
	{
		/* copy page to buffer */
		for(i = 0; i < FLASH_PAGE_SIZE; i++)
		{
			buffer[i] = spi_rx[i+4];
		}
	}

	return XST_SUCCESS;
}

int FlashWritePage(unsigned int addr, unsigned char *buffer)
{
	unsigned int i;

	/* check memory address */
	if(addr < 0x400000)
	{
		return XST_FAILURE;
	}

	/* enable write */
	spi_tx[0] = 0x06;
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 1) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* clear lower bytes */
	addr = addr & 0xFFFF00;

	/* prepare page program command */
	spi_tx[0] = 0x02;
	spi_tx[1] = addr >> 16;
	spi_tx[2] = addr >> 8;
	spi_tx[3] = addr;

	/* data bytes */
	for(i = 4; i < FLASH_PAGE_SIZE+4; i++)
		spi_tx[i] = buffer[i-4];

	/* do SPI transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, FLASH_PAGE_SIZE+4) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* wait for finish */
	if(FlashBusyWait() != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}

int FlashEraseSector(unsigned int addr)
{
	/* check memory address */
	if(addr < 0x400000)
	{
		return XST_FAILURE;
	}
	
	/* enable write */
	spi_tx[0] = 0x06;
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 1) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* clear lower bytes of addr */
	addr = addr & 0xFF0000;

	/* prepare sector erase */
	spi_tx[0] = 0xD8;
	spi_tx[1] = addr >> 16;
	spi_tx[2] = addr >> 8;
	spi_tx[3] = addr;

	/* do SPI transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 4) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* wait for finish */
	if(FlashBusyWait() != XST_SUCCESS)
		return XST_FAILURE;

	return XST_SUCCESS;
}

unsigned char FlashGetc(unsigned int flash_addr)
{
	/* prepare read command */
	spi_tx[0] = 0x03;
	spi_tx[1] = (flash_addr & 0x00FF0000) >> 16;
	spi_tx[2] = (flash_addr & 0x0000FF00) >> 8;
	spi_tx[3] = (flash_addr & 0x000000FF);
	spi_tx[4] = 0x00;

	/* start transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, 5) != XST_SUCCESS)
	{
		return 0xFF;
	}

	/* return data byte */
	return spi_rx[4];
}

int FlashCopy(unsigned int flash_addr, unsigned char *dest, unsigned int length)
{
	int i;

	/* check for zero pointer */
	if(dest == NULL)
		return XST_FAILURE;

	/* check if length is too long for SPI buffer */
	if(length > SPI_BUFFER_SIZE-4)
		return XST_FAILURE;

	/* prepare read command */
	spi_tx[0] = 0x03;
	spi_tx[1] = (flash_addr & 0x00FF0000) >> 16;
	spi_tx[2] = (flash_addr & 0x0000FF00) >> 8;
	spi_tx[3] = (flash_addr & 0x000000FF);

	/* fill dummy words */
	for(i = 4; i < (length+4); i++)
	{
		spi_tx[i] = 0x00;
	}
	for(i = 0; i < (length+4); i++)
	{
		spi_rx[i] = 0xAB;
	}

	/* start transfer */
	if(XSpi_Transfer(&bcf_flash_spi, spi_tx, spi_rx, length+4) != XST_SUCCESS)
	{
		return XST_FAILURE;
	}

	/* copy buffer to destination */
	memcpy(dest, &spi_rx[4], length);

	return XST_SUCCESS;
}