#ifndef FLASH_H__
#define FLASH_H__

#define FLASH_PAGE_SIZE		256

extern int FlashInit(void);
extern void FlashDeInit(void);
extern int FlashIDCheck(void);
extern int FlashReadPage(unsigned int addr, unsigned char *buffer);
extern int FlashWritePage(unsigned int addr, unsigned char *buffer);
extern int FlashEraseSector(unsigned int addr);
extern unsigned char FlashGetc(unsigned int flash_addr);
extern int FlashCopy(unsigned int flash_addr, unsigned char *dest, unsigned int length);

#endif /* FLASH_H__ */