#ifndef PRIMPROC_H__
#define PRIMPROC_H__

#define PRIMPROC_CMD_REG		0x0010
#define PRIMPROC_ARG0_REG		0x0011
#define PRIMPROC_ARG1_REG		0x0012
#define PRIMPROC_ARG2_REG		0x0013
#define PRIMPROC_ARG3_REG		0x0014
#define PRIMPROC_RET0_REG		0x0015
#define PRIMPROC_RET1_REG		0x0016
#define PRIMPROC_RET2_REG		0x0017
#define PRIMPROC_RET3_REG		0x0018


#define PRIM_NOP			0x00

/* EEPROM primitives */
#define PRIM_EEPROM_READ		0x01
#define PRIM_EEPROM_WRITE		0x02

/* dump gpmemory to uart */
#define PRIM_DUMP_GPMEM			0x03

/* environment primitives */
#define PRIM_ENV_SAVE			0x08
#define PRIM_ENV_LOAD			0x09

/* IP primitives */
#define PRIM_SET_IP				0x10
#define PRIM_GET_IP				0x11
#define PRIM_SET_GW				0x12
#define PRIM_GET_GW				0x13
#define PRIM_SET_SUBNET			0x14
#define PRIM_GET_SUBNET			0x15
#define PRIM_UPDATE_NETIF		0x16
#define PRIM_GET_ENET_MODE		0x17
#define PRIM_SET_ENET_MODE		0x18

/* recovery primitive */
#define PRIM_RECOVERY_IDENTIFY	0x20

/* flash reading / writing */
#define PRIM_FLASH_IDENTIFY		0x30
#define PRIM_FLASH_ERASESECTOR	0x31
#define PRIM_FLASH_PAGEREAD		0x32
#define PRIM_FLASH_PAGEWRITE	0x33

/* coarse set and get */
#define PRIM_COARSE_LOAD		0x40
#define PRIM_COARSE_SAVE		0x41

extern void primproc(void);

#endif /* PRIMPROC_H__ */

