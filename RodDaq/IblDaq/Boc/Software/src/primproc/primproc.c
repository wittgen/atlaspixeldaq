#include <stdint.h>
#include <stdio.h>
#include <xstatus.h>
#include "boc.h"
#include "primproc.h"
#include "flash.h"
//#include "eeprom.h"
#include "env.h"
#include "netif/xadapter.h"

// netif for IP update
extern struct netif server_netif;

static void netif_update(void)
{
    struct ip_addr ipaddr, netmask, gw;

    /* initliaze IP addresses to be used */
    uint8_t slot = boc_read_register(0x0019);
    uint8_t crate = boc_read_register(0x001A);
    
    /* load really default IP configuration if ENV fails */
    IP4_ADDR(&ipaddr, 192, 168, 0, 10);
    IP4_ADDR(&netmask, 255, 255, 255, 0);
    IP4_ADDR(&gw, 192, 168, 0, 1);
    
    /* check if slot and crate are set */
    if((slot == 0xFF) || (crate == 0xFF))
    {
        unsigned char env_ip[4], env_subnet[4], env_gw[4];
        int ret;
        
        ret = env_get("ip", env_ip);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading IP address from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default IP address...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load IP address from environment...\n\r"); break;
        }
        
        ret = env_get("gw", env_gw);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading gateway address from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default gateway address...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load gateway address from environment...\n\r"); break;
        }
        
        ret = env_get("subnet", env_subnet);
        switch(ret)
        {
            case ENV_OK: xil_printf("Loading netmask from EEPROM...\n\r"); break;
            case ENV_DEFAULT: xil_printf("Loading default netmask...\n\r"); break;
            case ENV_NOTFOUND: xil_printf("Failed to load netmask from environment...\n\r"); break;
        }
        
        IP4_ADDR(&ipaddr,  env_ip[0], env_ip[1], env_ip[2], env_ip[3]);
        IP4_ADDR(&netmask, env_subnet[0], env_subnet[1], env_subnet[2], env_subnet[3]);
        IP4_ADDR(&gw,      env_gw[0], env_gw[1], env_gw[2], env_gw[3]);
    }
    else
    {
        xil_printf("Loading slot based IP configuration...\n\r");
        IP4_ADDR(&ipaddr, 192, 168, 2, 10*slot+3);
        IP4_ADDR(&netmask, 255, 255, 255, 0);
        IP4_ADDR(&gw, 192, 168, 0, 254);
    }

    /* update network interface */
    xil_printf("Updating network interface IP settings...\n\r");
    netif_set_addr(&server_netif, &ipaddr, &netmask, &gw);
}

void primproc(void)
{
	uint8_t opcode = boc_read_register(PRIMPROC_CMD_REG);
	uint8_t arg[4];
	uint8_t ret[4];
	
	/* check for NOP */
	if(opcode == PRIM_NOP)
		return;

	/* load arguments */
	arg[0] = boc_read_register(PRIMPROC_ARG0_REG);
	arg[1] = boc_read_register(PRIMPROC_ARG1_REG);
	arg[2] = boc_read_register(PRIMPROC_ARG2_REG);
	arg[3] = boc_read_register(PRIMPROC_ARG3_REG);

	/* clear return value */
	ret[0] = 0;
	ret[1] = 0;
	ret[2] = 0;
	ret[3] = 0;

	/* execute opcode */
	if(opcode == PRIM_EEPROM_READ)
	{
		// uint16_t addr;
		// //xil_printf("Executing primitive: EEPROM_READ\n\r");
		// addr = arg[0];
		// addr |= arg[1] << 8;
		// ret[0] = eeprom_read(addr);
		// //xil_printf("Reading from address 0x%04X value 0x%02X\n\r", addr, value);
	}
	else if(opcode == PRIM_EEPROM_WRITE)
	{
		// uint16_t addr;
		// uint8_t value;
		// //xil_printf("Executing primitive: EEPROM_WRITE\n\r");
		// addr = arg[0];
		// addr |= arg[1] << 8;
		// value = arg[2];
		// //xil_printf("Writing to address 0x%04X value 0x%02X\n\r", addr, value);
		// eeprom_write(addr, value);
	}
	else if(opcode == PRIM_ENV_SAVE)
	{
		env_save();
	}
	else if(opcode == PRIM_ENV_LOAD)
	{
		env_load();
	}
	else if(opcode == PRIM_SET_IP)
	{
		env_set("ip", arg);
	}
	else if(opcode == PRIM_GET_IP)
	{
		env_get("ip", ret);
	}
	else if(opcode == PRIM_SET_GW)
	{
		env_set("gw", arg);
	}
	else if(opcode == PRIM_GET_GW)
	{
		env_get("gw", ret);
	}
	else if(opcode == PRIM_SET_SUBNET)
	{
		env_set("subnet", arg);
	}
	else if(opcode == PRIM_GET_SUBNET)
	{
		env_get("subnet", ret);
	}
	else if(opcode == PRIM_UPDATE_NETIF)
	{
		netif_update();
	}
	else if(opcode == PRIM_GET_ENET_MODE)
	{
		env_get("enet_mode", &ret[0]);
	}
	else if(opcode == PRIM_SET_ENET_MODE)
	{
		env_set("enet_mode", &arg[0]);
	}
	else if(opcode == PRIM_RECOVERY_IDENTIFY)
	{
#ifdef RECOVERY
		ret[0] = 0xDE;
		ret[1] = 0xAD;
		ret[2] = 0xBE;
		ret[3] = 0xEF;
#endif
	}
	else if(opcode == PRIM_DUMP_GPMEM)
	{
		unsigned int i;

		xil_printf("Dumping 256 byte of GP-Memory:");
		for(i = 0; i < 256; i++)
		{
			if(i % 16 == 0)
				xil_printf("\r\n");

			xil_printf("0x%02x ", boc_read_register(BCF_GPMEM_BASE+i));
		}

		xil_printf("\r\n");
	}
	else if(opcode == PRIM_FLASH_IDENTIFY)
	{
		if(FlashIDCheck() != XST_SUCCESS)
		{
			ret[0] = 0xFF;
		}
	}
	else if(opcode == PRIM_FLASH_ERASESECTOR)
	{
		unsigned int baseaddr;
		baseaddr = (unsigned int) arg[0] << 0;
		baseaddr |= (unsigned int) arg[1] << 8;
		baseaddr |= (unsigned int) arg[2] << 16;

		if(baseaddr < 0x400000)
		{
			xil_printf("Wrong flash base address.\r\n");
			ret[0] = 0xFF;
		}

		xil_printf("Erasing flash sector 0x%x\r\n", baseaddr);
		if(FlashEraseSector(baseaddr) != XST_SUCCESS)
		{
			xil_printf("Error erasing sector.\r\n");
			ret[0] = 0xFF;
		}
	}
	else if(opcode == PRIM_FLASH_PAGEWRITE)
	{
		unsigned char buf[FLASH_PAGE_SIZE];
		unsigned int baseaddr;
		unsigned int i;
		baseaddr = (unsigned int) arg[0] << 0;
		baseaddr |= (unsigned int) arg[1] << 8;
		baseaddr |= (unsigned int) arg[2] << 16;

		if(baseaddr < 0x400000)
		{
			xil_printf("Wrong flash base address.\r\n");
			ret[0] = 0xFF;
		}

		// copy page from GPMemory
		for(i = 0; i < FLASH_PAGE_SIZE; i++)
		{
			buf[i] = boc_read_register(BCF_GPMEM_BASE+i);
		}

		xil_printf("Programming flash page 0x%x\r\n", baseaddr);
		if(FlashWritePage(baseaddr, buf) != XST_SUCCESS)
		{
			xil_printf("Error programming page.\r\n");
			ret[0] = 0xFF;
		}
	}
	else if(opcode == PRIM_FLASH_PAGEREAD)
	{
		unsigned char buf[FLASH_PAGE_SIZE];
		unsigned int baseaddr;
		unsigned int i;
		baseaddr = (unsigned int) arg[0] << 0;
		baseaddr |= (unsigned int) arg[1] << 8;
		baseaddr |= (unsigned int) arg[2] << 16;

		xil_printf("Reading flash page 0x%x\r\n", baseaddr);
		if(FlashReadPage(baseaddr, buf) != XST_SUCCESS)
		{
			xil_printf("Error reading page.\r\n");
			ret[0] = 0xFF;
		}

		// copy page to GPMemory
		for(i = 0; i < FLASH_PAGE_SIZE; i++)
		{
			boc_write_register(BCF_GPMEM_BASE+i, buf[i]);
		}
	}
	else if(opcode == PRIM_COARSE_LOAD)
	{
		unsigned int mask;

		mask = (unsigned int) arg[0] << 0;
		mask |= (unsigned int) arg[1] << 8;
		mask |= (unsigned int) arg[2] << 16;
		mask |= (unsigned int) arg[3] << 24;
		
		boc_coarse_load(mask);

		ret[0] = 0;
		ret[1] = 0;
		ret[2] = 0;
		ret[3] = 0;
	}
	else if(opcode == PRIM_COARSE_SAVE)
	{
		unsigned int mask;

		mask = (unsigned int) arg[0] << 0;
		mask |= (unsigned int) arg[1] << 8;
		mask |= (unsigned int) arg[2] << 16;
		mask |= (unsigned int) arg[3] << 24;

		boc_coarse_save(mask);

		ret[0] = 0;
		ret[1] = 0;
		ret[2] = 0;
		ret[3] = 0;
	}

	/* write back result */
	boc_write_register(PRIMPROC_RET0_REG, ret[0]);
	boc_write_register(PRIMPROC_RET1_REG, ret[1]);
	boc_write_register(PRIMPROC_RET2_REG, ret[2]);
	boc_write_register(PRIMPROC_RET3_REG, ret[3]);

	/* enable writing to primproc cmd register */
	boc_write_register(42, 0x42);
	
	/* reset opcode register to acknowledge primitive */
	boc_write_register(PRIMPROC_CMD_REG, PRIM_NOP);
}
