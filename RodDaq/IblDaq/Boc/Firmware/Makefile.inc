# path to xilinx toolchain
ifdef XILINX
XILINX_PATH := $(dir $(XILINX))
endif
XILINX_PATH ?= /opt/Xilinx/14.7/ISE_DS

# Todo: move this to xilconf.mk or somewhere more global
ifeq ($(wildcard $(XILINX_PATH)), )
$(error Cannot find $(XILINX_PATH). Please set XILINX_PATH to a valid value.)
endif

# Next line needed for compatibility. Todo: fix this.
XIL_EDK_DIR=$(XILINX_PATH)/EDK
PIXELDAQ_ROOT=$(shell echo $(CURDIR) | sed 's/RodDaq.*//g')
include $(PIXELDAQ_ROOT)/RodDaq/IblDaq/xilconf_mb.mk

# use bash
SHELL := /bin/bash -O extglob

MKDIR ?= mkdir

# Toolchain settings
INTSTYLE = xflow

#########################
# DO NOT EDIT BELOW !!! #
#########################

# directory for temporary files
TMPDIR = build

# directory for output files
OUTDIR = binaries

# processor architecture
ARCH = $(shell uname -p)

# check for 32/64-bit architecture
# and source settings.sh
ifeq ($(ARCH), x86_64)
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin64
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin64
else
	ISE_BIN_PATH = $(XILINX_PATH)/ISE/bin/lin
	EDK_BIN_PATH = $(XILINX_PATH)/EDK/bin/lin
endif

# BuildDate variables
BUILD_YEAR = $(shell git show -s --format="%cd" --date="format:%y")
BUILD_MONTH = $(shell git show -s --format="%cd" --date="format:%m")
BUILD_DAY = $(shell git show -s --format="%cd" --date="format:%d")
BUILD_HOUR = $(shell git show -s --format="%cd" --date="format:%H")
BUILD_MINUTE = $(shell git show -s --format="%cd" --date="format:%M")
BUILD_SECOND = $(shell git show -s --format="%cd" --date="format:%S")
GIT_HASH = $(shell git rev-parse HEAD)

# command line tools
XST = $(ISE_BIN_PATH)/xst
COREGEN = $(ISE_BIN_PATH)/coregen
INSERTER = $(ISE_BIN_PATH)/inserter
NGDBUILD = $(ISE_BIN_PATH)/ngdbuild
MAP = $(ISE_BIN_PATH)/map
PAR = $(ISE_BIN_PATH)/par
BITGEN = $(ISE_BIN_PATH)/bitgen
IMPACT = $(ISE_BIN_PATH)/impact
PROMGEN = $(ISE_BIN_PATH)/promgen
PLATGEN = $(EDK_BIN_PATH)/platgen
XPS = $(EDK_BIN_PATH)/xps
DATA2MEM = $(ISE_BIN_PATH)/data2mem
TRCE = $(ISE_BIN_PATH)/trce
SMARTXPLORER = $(ISE_BIN_PATH)/smartxplorer
XMD = $(EDK_BIN_PATH)/xmd
