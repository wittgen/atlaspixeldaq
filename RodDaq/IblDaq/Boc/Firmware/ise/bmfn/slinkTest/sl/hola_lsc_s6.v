// author: philipp schaefer

module hola_lsc_s6 #(
	 parameter slink_ref_clk = 100,
	parameter SIMULATION = 1'b0)(
	input wire mgtrefclk,
	input wire sys_rst,
	input wire mgt_rst,
	// new for sharing tile user clocks
	// one gtpclkout is used to generate all user clocks, also for neightbouring tiles
	 output wire gtpclkout,
	 output wire gtp_plldet_out,
	 input wire usr_clk_in,
	 input wire usr_clk20_in,
	// s-link interface
	input wire [31:0] ud,
	input wire ureset_n,
	input wire utest_n,
	input wire uctrl_n,
	input wire uwen_n,
	input wire uclk,
	output wire lff_n,
	output wire [3:0] lrl0,
	output wire [3:0] lrl1,
	output wire ldown_n,
	// slink enables
	input wire hola_enable_0,
	input wire hola_enable_1,
	// sfp serial interface
	input wire tlk_sin0_p,
	input wire tlk_sin0_n,
	output wire tlk_sout0_p,
	output wire tlk_sout0_n,
	input wire tlk_sin1_p,
	input wire tlk_sin1_n,
	output wire tlk_sout1_p,
	output wire tlk_sout1_n,
	// leds
	output wire testled_n,
	output wire lderrled_n,
	output wire lupled_n,
	output wire flowctlled_n,
	output wire activityled_n,
	
	output wire usr_clk_out,
	output wire gtx_reset_done
);

wire lff_n0;
wire ldown_n0;
wire testled_n0;
wire lderrled_n0;
wire lupled_n0;
wire flowctlled_n0;
wire activityled_n0;

wire lsc_rst_n;
wire tlk_gtx_clk0;
wire tlk_enable0;
wire [15:0] tlk_txd0;
wire tlk_tx_en0;
wire tlk_tx_er0;
wire [15:0] tlk_rxd0;
wire tlk_rx_clk0;
wire tlk_rx_er0;
wire tlk_rx_dv0;

// should be the same as tlk_gtx_clk1
assign usr_clk_out = tlk_gtx_clk0;

wire lff_n1;
wire ldown_n1;
wire testled_n1;
wire lderrled_n1;
wire lupled_n1;
wire flowctlled_n1;
wire activityled_n1;

wire tlk_gtx_clk1;
wire tlk_enable1;
wire [15:0] tlk_txd1;
wire tlk_tx_en1;
wire tlk_tx_er1;
wire [15:0] tlk_rxd1;
wire tlk_rx_clk1;
wire tlk_rx_er1;
wire tlk_rx_dv1;

holalsc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.USE_PLL(0),
	.USE_ICLK2(0),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(64),
	.LOG2DEPTH(6),
	.FULLMARGIN(16)) holalsc_core0_I (
	.power_up_rst_n(lsc_rst_n),
	.ud(ud),
	.ureset_n(ureset_n),
	.utest_n(utest_n),
	.uctrl_n(uctrl_n),
	.uwen_n(uwen_n),
	.uclk(uclk),
	.lff_n(lff_n0),
	.lrl(lrl0),
	.ldown_n(ldown_n0),
	.testled_n(testled_n0),
	.lderrled_n(lderrled_n0),
	.lupled_n(lupled_n0),
	.flowctlled_n(flowctlled_n0),
	.activityled_n(activityled_n0),
	.xclk(tlk_gtx_clk0),
	.enable(tlk_enable0),
	.txd(tlk_txd0),
	.tx_en(tlk_tx_en0),
	.tx_er(tlk_tx_er0),
	.rxd(tlk_rxd0),
	.rx_clk(tlk_rx_clk0),
	.rx_er(tlk_rx_er0),
	.rx_dv(tlk_rx_dv0)
);

holalsc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.USE_PLL(0),
	.USE_ICLK2(0),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(64),
	.LOG2DEPTH(6),
	.FULLMARGIN(16)) holalsc_core1_I (
	.power_up_rst_n(lsc_rst_n),
	.ud(ud),
	.ureset_n(ureset_n),
	.utest_n(utest_n),
	.uctrl_n(uctrl_n),
	.uwen_n(uwen_n),
	.uclk(uclk),
	.lff_n(lff_n1),
	.lrl(lrl1),
	.ldown_n(ldown_n1),
	.testled_n(testled_n1),
	.lderrled_n(lderrled_n1),
	.lupled_n(lupled_n1),
	.flowctlled_n(flowctlled_n1),
	.activityled_n(activityled_n1),
	.xclk(tlk_gtx_clk1),
	.enable(tlk_enable1),
	.txd(tlk_txd1),
	.tx_en(tlk_tx_en1),
	.tx_er(tlk_tx_er1),
	.rxd(tlk_rxd1),
	.rx_clk(tlk_rx_clk1),
	.rx_er(tlk_rx_er1),
	.rx_dv(tlk_rx_dv1)
);


tlk_wrapper_s6 tlk_wrapper_s6_I (
	.mgtrefclk(mgtrefclk),
	.gtpclkout(gtpclkout),
	.gtp_plldet_out(gtp_plldet_out),
	.usr_clk_in(usr_clk_in),
	.usr_clk20_in(usr_clk20_in),
	.gtx_reset(mgt_rst),
	.gtx_resetdone(gtx_reset_done),
	.gtx_rxn0(tlk_sin0_n),
	.gtx_rxp0(tlk_sin0_p),
	.gtx_txn0(tlk_sout0_n),
	.gtx_txp0(tlk_sout0_p),
	.gtx_rxn1(tlk_sin1_n),
	.gtx_rxp1(tlk_sin1_p),
	.gtx_txn1(tlk_sout1_n),
	.gtx_txp1(tlk_sout1_p),
	.tlk_txclk0(tlk_gtx_clk0),
	.tlk_txd0(tlk_txd0),
	.tlk_txen0(tlk_tx_en0),
	.tlk_txer0(tlk_tx_er0),
	.tlk_rxclk0(tlk_rx_clk0),
	.tlk_rxd0(tlk_rxd0),
	.tlk_rxdv0(tlk_rx_dv0),
	.tlk_rxer0(tlk_rx_er0),
	.tlk_txclk1(tlk_gtx_clk1),
	.tlk_txd1(tlk_txd1),
	.tlk_txen1(tlk_tx_en1),
	.tlk_txer1(tlk_tx_er1),
	.tlk_rxclk1(tlk_rx_clk1),
	.tlk_rxd1(tlk_rxd1),
	.tlk_rxdv1(tlk_rx_dv1),
	.tlk_rxer1(tlk_rx_er1)
);




// use enable signals to qualify holas. All output are low active
assign testled_n = !((!testled_n0 & hola_enable_0) | (!testled_n1 & !hola_enable_1));
assign lderrled_n = !((!lderrled_n0 & !hola_enable_0) | (!lderrled_n1 & hola_enable_1));
assign lupled_n = !((!lupled_n0  & !hola_enable_0)| (!lupled_n1 & hola_enable_1));
assign flowctlled_n = !((!flowctlled_n0  & hola_enable_0) | (!flowctlled_n1 & hola_enable_1));
assign activityled_n = !((!activityled_n0 & hola_enable_0) | (!activityled_n1 & hola_enable_1));
assign lff_n = !((!lff_n0 & hola_enable_0) | (!lff_n1 & hola_enable_1));
assign ldown_n = !((!ldown_n0 & hola_enable_0) | (!ldown_n1 & hola_enable_1));
assign lsc_rst_n = !sys_rst & gtx_reset_done;

endmodule // hola_lsc_s6
