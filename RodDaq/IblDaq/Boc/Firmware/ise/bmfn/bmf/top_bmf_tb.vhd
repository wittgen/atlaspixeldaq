-- TestBench Template 

  LIBRARY ieee;
  USE ieee.std_logic_1164.ALL;
  USE ieee.numeric_std.ALL;

  ENTITY testbench IS
  END testbench;

  ARCHITECTURE behavior OF testbench IS 

	component top_bmf
	generic (slink_ref_clk: integer := 100; simulation: integer := 0); -- 100 or 80
	port (
		-- GPIO test outputs and pushbutton
		DATA_TEST_SE : out std_logic_vector(8 downto 0) := (others => '0');
		DATA_TEST_D_P : inout std_logic_vector(7 downto 0) := (others => '0');
		DATA_TEST_D_N : inout std_logic_vector(7 downto 0) := (others => '1');
		GPIO_pushbutton : in std_logic := '0';

		-- S-Link MGT Pins
		-- currently uses 1 dual-LSCs and 2 LDCs. To be replaced by 4 LSCs later on
		lsc_sin0_p : in std_logic;
		lsc_sin0_n : in std_logic;
		lsc_sout0_p : out std_logic;
		lsc_sout0_n : out std_logic;
		lsc_sin1_p : in std_logic;
		lsc_sin1_n : in std_logic;
		lsc_sout1_p : out std_logic;
		lsc_sout1_n : out std_logic;

		ldc_sin0_p : in std_logic;
		ldc_sin0_n : in std_logic;
		ldc_sout0_p : out std_logic;
		ldc_sout0_n : out std_logic;
		ldc_sin1_p : in std_logic;
		ldc_sin1_n : in std_logic;
		ldc_sout1_p : out std_logic;
		ldc_sout1_n : out std_logic;

		
		-- FMC LPC
		FMC_LA_P : inout std_logic_vector(33 downto 0) := (others => '0');
		FMC_LA_N : inout std_logic_vector(33 downto 0) := (others => '1');
		
		-- clock inputs
		CLK100_P : in std_logic := '0';
		CLK100_N : in std_logic := '1';
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		
		-- rx data from detector (snap 12)
		RX_DATA_S12_0_P : in std_logic_vector(11 downto 0) := (others => '0');
		RX_DATA_S12_0_N : in std_logic_vector(11 downto 0) := (others => '1');
		RX_DATA_S12_1_P : in std_logic_vector(11 downto 0) := (others => '0');
		RX_DATA_S12_1_N : in std_logic_vector(11 downto 0) := (others => '1');
		
		-- Data to the ROD
		RXDATA_BOC2ROD : out std_logic_vector(47 downto 0) := (others => '0');
		
		-- bcf wishbone interface
		BOC_ADD_BCF2BMF : in std_logic_vector(11 downto 0) := (others => '0');
		BOC_DAT_BCF2BMF : inout std_logic_vector(7 downto 0) := (others => '0');
		BOC_ACK_BCF2BMF : out std_logic := '0';
		BOC_WE_BCF2BMF : in std_logic := '0';
		BOC_STB_BCF2BMF : in std_logic := '0';
		BOC_CYC_BCF2BMF : in std_logic := '0';
		
		-- connection to the TX SNAP12 - B
		TX_DATA_S12_P : out std_logic_vector(11 downto 0) := (others => '0');
		TX_DATA_S12_N : out std_logic_vector(11 downto 0) := (others => '1');
		TXEN_S12TX : out std_logic := '0';
		Reset_S12_TX : out std_logic := '0';
		
		
		-- XC connections
		XC : in std_logic_vector(7 downto 0) := (others => '0')
	);
end component;


   --Inputs
   signal clk40p : std_logic := '0';
   signal clk40n : std_logic := '0';
   signal clk100p : std_logic := '0';
   signal clk100n : std_logic := '0';
   signal rst : std_logic := '0';
	signal lsc_sin0_p :   std_logic;
	signal lsc_sin0_n :   std_logic;
	signal lsc_sin1_p :   std_logic;
	signal lsc_sin1_n :   std_logic;


   signal lsc_sout0_p : std_logic;
   signal lsc_sout0_n : std_logic;
   signal lsc_sout1_p : std_logic;
   signal lsc_sout1_n : std_logic;


   -- Clock period definitions
   constant clk40p_period : time := 25 ns;
   constant clk100p_period : time := 10 ns;

component SIM_RESET_MGT_MODEL is
port 
(
    GSR_IN     : in std_logic
);
end component;

 
BEGIN
 
 rsModel: SIM_RESET_MGT_MODEL port map (gsr_in => '0');
 
	-- Instantiate the Unit Under Test (UUT)
   uut: top_bmf 
		generic map (slink_ref_clk => 100, simulation => 1)

		PORT MAP (
          CLK40_P => clk40p,
          CLK40_n => clk40n,
			 CLK100_P => clk100p,
			 CLK100_N => clk100n,
          lsc_sin0_p => lsc_sin0_p,
          lsc_sin0_n => lsc_sin0_n,
          lsc_sout0_p => lsc_sout0_p,
          lsc_sout0_n => lsc_sout0_n,
          lsc_sin1_p => lsc_sin1_p,
          lsc_sin1_n => lsc_sin1_n,
          lsc_sout1_p => lsc_sout1_p,
          lsc_sout1_n => lsc_sout1_n,
          ldc_sin0_p => lsc_sout0_p,
          ldc_sin0_n => lsc_sout0_n,
          ldc_sout0_p => lsc_sin0_p,
          ldc_sout0_n => lsc_sin0_n,
          ldc_sin1_p => lsc_sout1_p,
          ldc_sin1_n => lsc_sout1_n,
          ldc_sout1_p => lsc_sin1_p,
          ldc_sout1_n => lsc_sin1_n
        );

   -- Clock process definitions
   clk40p_process :process
   begin
		clk40p <= '0';
		wait for clk40p_period/2;
		clk40p <= '1';
		wait for clk40p_period/2;
   end process;
 
 	clk40n <= not clk40p;
 
   clk100p_process :process
   begin
		clk100p <= '0';
		wait for clk100p_period/2;
		clk100p <= '1';
		wait for clk100p_period/2;
   end process;
 
 	clk100n <= not clk100p;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
		rst <= '1';
      wait for 100 ns;	
		rst <= '0';

      wait for clk40p_period*10;

      -- insert stimulus here 

      wait;
   end process;

end;
