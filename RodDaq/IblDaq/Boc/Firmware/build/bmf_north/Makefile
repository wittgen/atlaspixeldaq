include ../../Makefile.inc
include ../../Makefile.rev
include ../../Makefile.buildvers
include ../../Makefile.bmf

#.PHONY: $(BMF_TOP).prj $(BMF_TOP).xst $(BMF_TOP).ut all clean
.PHONY: clean

all: $(BMF_TOP).ace $(BMF_TOP).twr

clean:
	@rm -rf !(Makefile)

BuildDate.vhd: $(addprefix ../../,$(BMF_VHDL)) $(addprefix ../../,$(BMF_VERILOG)) $(addprefix ../../,$(BMF_XCO))
	@cp ../../rtl/common/BuildDate.vhd.template BuildDate.vhd
	@sed -i 's:BUILD_YEAR:$(BUILD_YEAR):g' BuildDate.vhd
	@sed -i 's:BUILD_MONTH:$(BUILD_MONTH):g' BuildDate.vhd
	@sed -i 's:BUILD_DAY:$(BUILD_DAY):g' BuildDate.vhd
	@sed -i 's:BUILD_HOUR:$(BUILD_HOUR):g' BuildDate.vhd
	@sed -i 's:BUILD_MINUTE:$(BUILD_MINUTE):g' BuildDate.vhd
	@sed -i 's:BUILD_SECOND:$(BUILD_SECOND):g' BuildDate.vhd
	@sed -i 's:GIT_HASH:$(GIT_HASH):g' BuildDate.vhd

BuildVersion.vhd: ../../Makefile.buildvers ../../rtl/common/BuildVersion.vhd.template
	@cp ../../rtl/common/BuildVersion.vhd.template BuildVersion.vhd
	@if [ "$(BUILDVERS_RX)" == "pixel" ]; then sed -i 's:BUILD_RXPIX:true:g' BuildVersion.vhd; fi
	@if [ "$(BUILDVERS_RX)" == "ibl" ]; then sed -i 's:BUILD_RXPIX:false:g' BuildVersion.vhd; fi
	@sed -i 's:BUILD_DCM_SLINK0_PHASE:$(BMFN_SLINK0_PHASE):g' BuildVersion.vhd
	@sed -i 's:BUILD_DCM_SLINK1_PHASE:$(BMFN_SLINK1_PHASE):g' BuildVersion.vhd

$(BMF_TOP).ngc: $(addprefix ../../,$(BMF_VHDL)) $(addprefix ../../,$(BMF_VERILOG)) $(BMF_TOP).prj $(BMF_TOP).xst BuildDate.vhd BuildVersion.vhd
	@rm -rf xst
	@mkdir xst
	@mkdir xst/projnav.tmp
	$(XST) -intstyle $(INTSTYLE) -ifn $(BMF_TOP).xst -ofn $(BMF_TOP).syr

$(BMF_TOP).prj: ../../Makefile.bmf
	@rm -rf $(BMF_TOP).prj
	@touch $(BMF_TOP).prj
	@for i in $(BMF_VHDL); do \
		echo "vhdl work \"../../$$i\"" >> $(BMF_TOP).prj ; \
	done;
	@for i in $(BMF_CORE_VHDL); do \
		echo "vhdl work \"$$i\"" >> $(BMF_TOP).prj ; \
	done;
	@for i in $(BMF_VERILOG); do \
		echo "verilog work \"../../$$i\"" >> $(BMF_TOP).prj ; \
	done;
	@echo "vhdl work \"BuildDate.vhd\"" >> $(BMF_TOP).prj
	@echo "vhdl work \"BuildVersion.vhd\"" >> $(BMF_TOP).prj
	@sed -i 's:work \"../../rtl/bmf/rx/decode_8b10b:decode_8b10b \"../../rtl/bmf/rx/decode_8b10b:g' $(BMF_TOP).prj
	@sed -i 's:work \"../../rtl/bmf/tx/encode_8b10b:encode_8b10b \"../../rtl/bmf/tx/encode_8b10b:g' $(BMF_TOP).prj

$(BMF_TOP).xst: ../../Makefile.bmf
	@echo "set -tmpdir \"xst/projnav.tmp\"" > $(BMF_TOP).xst
	@echo "set -xsthdpdir \"xst\"" >> $(BMF_TOP).xst
	@echo "run" >> $(BMF_TOP).xst
	@echo "-ifn $(BMF_TOP).prj" >> $(BMF_TOP).xst
	@echo "-ofn $(BMF_TOP)" >> $(BMF_TOP).xst
	@echo "-ofmt NGC" >> $(BMF_TOP).xst
	@echo "-p $(BMF_FPGA_DEVICE)$(BMF_FPGA_SPEED)-$(BMF_FPGA_PACKAGE)" >> $(BMF_TOP).xst
	@echo "-top $(BMF_TOP)" >> $(BMF_TOP).xst
	@echo "-opt_mode Speed" >> $(BMF_TOP).xst
	@echo "-opt_level 1" >> $(BMF_TOP).xst
	@echo "-power NO" >> $(BMF_TOP).xst
	@echo "-iuc NO" >> $(BMF_TOP).xst
	@echo "-keep_hierarchy No" >> $(BMF_TOP).xst
	@echo "-netlist_hierarchy As_Optimized" >> $(BMF_TOP).xst
	@echo "-rtlview Yes" >> $(BMF_TOP).xst
	@echo "-glob_opt AllClockNets" >> $(BMF_TOP).xst
	@echo "-read_cores YES" >> $(BMF_TOP).xst
	@echo "-sd {\"../bmf_ip\" }" >> $(BMF_TOP).xst
	@echo "-write_timing_constraints NO" >> $(BMF_TOP).xst
	@echo "-cross_clock_analysis NO" >> $(BMF_TOP).xst
	@echo "-hierarchy_separator /" >> $(BMF_TOP).xst
	@echo "-bus_delimiter <>" >> $(BMF_TOP).xst
	@echo "-case Maintain" >> $(BMF_TOP).xst
	@echo "-slice_utilization_ratio 100" >> $(BMF_TOP).xst
	@echo "-bram_utilization_ratio 100" >> $(BMF_TOP).xst
	@echo "-dsp_utilization_ratio 100" >> $(BMF_TOP).xst
	@echo "-lc Auto" >> $(BMF_TOP).xst
	@echo "-reduce_control_sets Auto" >> $(BMF_TOP).xst
	@echo "-fsm_extract YES -fsm_encoding Auto" >> $(BMF_TOP).xst
	@echo "-safe_implementation No" >> $(BMF_TOP).xst
	@echo "-fsm_style LUT" >> $(BMF_TOP).xst
	@echo "-ram_extract Yes" >> $(BMF_TOP).xst
	@echo "-ram_style Auto" >> $(BMF_TOP).xst
	@echo "-rom_extract Yes" >> $(BMF_TOP).xst
	@echo "-shreg_extract YES" >> $(BMF_TOP).xst
	@echo "-rom_style Auto" >> $(BMF_TOP).xst
	@echo "-auto_bram_packing NO" >> $(BMF_TOP).xst
	@echo "-resource_sharing YES" >> $(BMF_TOP).xst
	@echo "-async_to_sync NO" >> $(BMF_TOP).xst
	@echo "-shreg_min_size 2" >> $(BMF_TOP).xst
	@echo "-use_dsp48 Auto" >> $(BMF_TOP).xst
	@echo "-iobuf YES" >> $(BMF_TOP).xst
	@echo "-max_fanout 100000" >> $(BMF_TOP).xst
	@echo "-bufg 16" >> $(BMF_TOP).xst
	@echo "-register_duplication YES" >> $(BMF_TOP).xst
	@echo "-register_balancing No" >> $(BMF_TOP).xst
	@echo "-optimize_primitives NO" >> $(BMF_TOP).xst
	@echo "-use_clock_enable Auto" >> $(BMF_TOP).xst
	@echo "-use_sync_set Auto" >> $(BMF_TOP).xst
	@echo "-use_sync_reset Auto" >> $(BMF_TOP).xst
	@echo "-iob Auto" >> $(BMF_TOP).xst
	@echo "-equivalent_register_removal YES" >> $(BMF_TOP).xst
	@echo "-slice_utilization_ratio_maxmargin 5" >> $(BMF_TOP).xst

$(BMF_TOP).ut: ../../Makefile.bmf
	@echo "-w" > $(BMF_TOP).ut
	@echo "-g DebugBitstream:No" >> $(BMF_TOP).ut
	@echo "-g Binary:no" >> $(BMF_TOP).ut
	@echo "-g CRC:Enable" >> $(BMF_TOP).ut
	@echo "-g Reset_on_err:No" >> $(BMF_TOP).ut
	@echo "-g ConfigRate:2" >> $(BMF_TOP).ut
	@echo "-g ProgPin:PullUp" >> $(BMF_TOP).ut
	@echo "-g TckPin:PullUp" >> $(BMF_TOP).ut
	@echo "-g TdiPin:PullUp" >> $(BMF_TOP).ut
	@echo "-g TdoPin:PullUp" >> $(BMF_TOP).ut
	@echo "-g TmsPin:PullUp" >> $(BMF_TOP).ut
	@echo "-g UnusedPin:PullNone" >> $(BMF_TOP).ut
	@echo "-g UserID:0xFFFFFFFF" >> $(BMF_TOP).ut
	@echo "-g ExtMasterCclk_en:No" >> $(BMF_TOP).ut
	@echo "-g SPI_buswidth:1" >> $(BMF_TOP).ut
	@echo "-g TIMER_CFG:0xFFFF" >> $(BMF_TOP).ut
	@echo "-g multipin_wakeup:No" >> $(BMF_TOP).ut
	@echo "-g StartUpClk:CClk" >> $(BMF_TOP).ut
	@echo "-g DONE_cycle:4" >> $(BMF_TOP).ut
	@echo "-g GTS_cycle:5" >> $(BMF_TOP).ut
	@echo "-g GWE_cycle:6" >> $(BMF_TOP).ut
	@echo "-g LCK_cycle:NoWait" >> $(BMF_TOP).ut
	@echo "-g Security:None" >> $(BMF_TOP).ut
	@echo "-g DonePipe:No" >> $(BMF_TOP).ut
	@echo "-g DriveDone:Yes" >> $(BMF_TOP).ut
	@echo "-g Encrypt:No" >> $(BMF_TOP).ut
	@echo "-g en_sw_gsr:No" >> $(BMF_TOP).ut
	@echo "-g drive_awake:No" >> $(BMF_TOP).ut
	@echo "-g sw_clk:Startupclk" >> $(BMF_TOP).ut
	@echo "-g sw_gwe_cycle:5" >> $(BMF_TOP).ut
	@echo "-g sw_gts_cycle:4" >> $(BMF_TOP).ut

$(BMF_TOP).ngd: $(BMF_TOP).ngc ../../$(BMFN_UCF)
	@rm -rf _ngo
	@mkdir _ngo
	$(NGDBUILD) -intstyle $(INTSTYLE) -dd _ngo -sd ../bmf_ip -nt timestamp -uc ../../$(BMFN_UCF) -p $(BMF_FPGA_DEVICE)$(BMF_FPGA_SPEED)-$(BMF_FPGA_PACKAGE) $(BMF_TOP).ngc $(BMF_TOP).ngd

$(BMF_TOP)_map.ncd: $(BMF_TOP).ngd
#	map -intstyle $(INTSTYLE) -p $(BMF_FPGA_DEVICE)$(BMF_FPGA_SPEED)-$(BMF_FPGA_PACKAGE) -w -logic_opt on -ol high -t 2 -xt 0 -register_duplication on -r 4 -global_opt off -mt on -ir off -pr off -lc off -power off -convert_bram8 -o $(BMF_TOP)_map.ncd $(BMF_TOP).ngd $(BMF_TOP).pcf
	$(MAP) -ol high -xe n -logic_opt on -register_duplication on -t 2 -w -p $(BMF_FPGA_DEVICE)$(BMF_FPGA_SPEED)-$(BMF_FPGA_PACKAGE) -o $(BMF_TOP)_map.ncd $(BMF_TOP).ngd $(BMF_TOP).pcf
	
$(BMF_TOP)_par.ncd: $(BMF_TOP)_map.ncd
	$(PAR) -w -intstyle $(INTSTYLE) -ol high -mt on $(BMF_TOP)_map.ncd $(BMF_TOP)_par.ncd $(BMF_TOP).pcf

$(BMF_TOP)_par.bit: $(BMF_TOP)_par.ncd $(BMF_TOP).ut
	$(BITGEN) -intstyle $(INTSTYLE) -f $(BMF_TOP).ut $(BMF_TOP)_par.ncd

$(BMF_TOP).ace: $(BMF_TOP)_par.bit
	rm -f $(BMF_TOP).ace
	@echo "setMode -bs" > impact_svf.cmd
	@echo "setCable -p svf -file $(BMF_TOP).svf" >> impact_svf.cmd
	@echo "addDevice -p 1 -file $(BMF_TOP)_par.bit" >> impact_svf.cmd
	@echo "program -p 1" >> impact_svf.cmd
	@echo "closeCable" >> impact_svf.cmd
	@echo "setMode -bs" >> impact_svf.cmd
	@echo "svf2ace -wtck -tck 25e6 -i $(BMF_TOP).svf -o $(BMF_TOP).ace" >> impact_svf.cmd
	@echo "quit" >> impact_svf.cmd
	$(IMPACT) -batch impact_svf.cmd

$(BMF_TOP).twr: $(BMF_TOP)_par.ncd
	$(TRCE) -e 5 -ucf $(BMFN_UCF) -o $(BMF_TOP).twr $(BMF_TOP)_par.ncd $(BMF_TOP).pcf

smartxplorer: $(BMF_TOP).ngc ../../$(BMFN_UCF)
	$(SMARTXPLORER) -p $(BMF_FPGA_DEVICE)$(BMF_FPGA_SPEED)-$(BMF_FPGA_PACKAGE) -uc ../../$(BMFN_UCF) -wd smartxplorer_results -sd='.;../bmf_ip' $(BMF_TOP).ngc
