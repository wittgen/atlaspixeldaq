#ChipScope Core Generator Project File Version 3.0
#Mi Mär 16 21:24:39 CET 2016
SignalExport.bus<0000>.channelList=0 1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24
SignalExport.bus<0000>.name=TRIG0
SignalExport.bus<0000>.offset=0.0
SignalExport.bus<0000>.precision=0
SignalExport.bus<0000>.radix=Bin
SignalExport.bus<0000>.scaleFactor=1.0
SignalExport.clockChannel=CLK
SignalExport.dataEqualsTrigger=true
SignalExport.triggerChannel<0000><0000>=emu_outlinks[0]
SignalExport.triggerChannel<0000><0001>=emu_outlinks[1]
SignalExport.triggerChannel<0000><0002>=emu_outlinks[2]
SignalExport.triggerChannel<0000><0003>=emu_outlinks[3]
SignalExport.triggerChannel<0000><0004>=speed[0]
SignalExport.triggerChannel<0000><0005>=speed[1]
SignalExport.triggerChannel<0000><0006>=mon_slow
SignalExport.triggerChannel<0000><0007>=mon_state
SignalExport.triggerChannel<0000><0008>=mon_fifo_din[0]
SignalExport.triggerChannel<0000><0009>=mon_fifo_din[1]
SignalExport.triggerChannel<0000><0010>=mon_fifo_din[2]
SignalExport.triggerChannel<0000><0011>=mon_fifo_din[3]
SignalExport.triggerChannel<0000><0012>=mon_fifo_din[4]
SignalExport.triggerChannel<0000><0013>=mon_fifo_din[5]
SignalExport.triggerChannel<0000><0014>=mon_fifo_din[6]
SignalExport.triggerChannel<0000><0015>=mon_fifo_din[7]
SignalExport.triggerChannel<0000><0016>=mon_fifo_wren
SignalExport.triggerChannel<0000><0017>=mon_found_header
SignalExport.triggerChannel<0000><0018>=mon_found_trailer
SignalExport.triggerChannel<0000><0019>=mon_srcnt[0]
SignalExport.triggerChannel<0000><0020>=mon_srcnt[1]
SignalExport.triggerChannel<0000><0021>=mon_srcnt[2]
SignalExport.triggerChannel<0000><0022>=xc
SignalExport.triggerChannel<0000><0023>=unused[0]
SignalExport.triggerChannel<0000><0024>=unused[1]
SignalExport.triggerPort<0000>.name=TRIG0
SignalExport.triggerPortCount=1
SignalExport.triggerPortIsData<0000>=true
SignalExport.triggerPortWidth<0000>=25
SignalExport.type=ila

