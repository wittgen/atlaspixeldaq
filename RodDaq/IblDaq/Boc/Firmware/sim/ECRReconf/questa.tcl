# all design files come here including their library
set library_file_list {
	work {
		../../rtl/bmf/tx/ECRReconf/ECRDetect.vhd
		../../rtl/bmf/tx/ECRReconf/ECRReconfBit.vhd
		../txt_util.vhd
		sim_ECRReconfBit.vhd
	}
}

# compile all files
foreach {library file_list} $library_file_list {
	vlib $library
	foreach file $file_list {
		if [regexp {.vhdl?$} $file] {
			vcom -work $library $file
		} else {
			vlog -work $library $file
		}
	}
}

# disable unbound to initial warning
# suppress 8684

# start simulation
eval vsim sim_ECRReconfBit

# add some signals to waveform
noview wave
add wave sim:/sim_ECRReconfBit/*
add wave sim:/sim_ECRReconfBit/dut/*

# run for 10 us
run 10us

# exit
#quit
