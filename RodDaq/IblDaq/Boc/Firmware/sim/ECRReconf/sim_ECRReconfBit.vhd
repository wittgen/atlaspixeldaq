library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sim_ECRReconfBit is
end sim_ECRReconfBit;

architecture tb of sim_ECRReconfBit is
	component ECRReconfBit
		generic (
			BITSTREAM_BYTES : integer := 4096		-- 4 kB for a bitstream
		);
		port (
			-- clock and reset
			clk : in std_logic;
			reset : in std_logic;

			-- monitoring for collision
			xc : in std_logic;

			-- data output (serial stream)
			reconf_dout : out std_logic;
			reconf_active : out std_logic;

			-- registers
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			status_rd : out std_logic_vector(7 downto 0);
			status_wr : in std_logic_vector(7 downto 0);
			status_rden : in std_logic;
			status_wren : in std_logic;
			addr_lsb_rd : out std_logic_vector(7 downto 0);
			addr_lsb_wr : in std_logic_vector(7 downto 0);
			addr_lsb_rden : in std_logic;
			addr_lsb_wren : in std_logic;
			addr_msb_rd : out std_logic_vector(7 downto 0);
			addr_msb_wr : in std_logic_vector(7 downto 0);
			addr_msb_rden : in std_logic;
			addr_msb_wren : in std_logic;
			data_rd : out std_logic_vector(7 downto 0);
			data_wr : in std_logic_vector(7 downto 0);
			data_rden : in std_logic;
			data_wren : in std_logic
		);
	end component;

	signal clk : std_logic := '0';
	signal reset : std_logic := '1';
	signal xc : std_logic := '0';
	signal reconf_dout : std_logic := '0';
	signal reconf_active : std_logic := '0';
	signal control_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal control_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal control_rden : std_logic := '0';
	signal control_wren : std_logic := '0';
	signal status_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal status_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal status_rden : std_logic := '0';
	signal status_wren : std_logic := '0';
	signal addr_lsb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal addr_lsb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal addr_lsb_rden : std_logic := '0';
	signal addr_lsb_wren : std_logic := '0';
	signal addr_msb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal addr_msb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal addr_msb_rden : std_logic := '0';
	signal addr_msb_wren : std_logic := '0';
	signal data_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal data_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal data_rden : std_logic := '0';
	signal data_wren : std_logic := '0';

	type bitstream_t is array (natural range <>) of std_logic_vector(7 downto 0);
	constant example_bitstream : bitstream_t := (
		x"5a",
		x"0a",
		x"00",
		x"00",
		x"00",
		x"5a",
		x"0a",
		x"01",
		x"00",
		x"01"
	);
begin

dut: ECRReconfBit
	generic map (
		BITSTREAM_BYTES => 4096
	)
	port map (
		clk => clk,
		reset => reset,
		xc => xc,
		reconf_dout => reconf_dout,
		reconf_active => reconf_active,
		control_rd => control_rd,
		control_wr => control_wr,
		control_rden => control_rden,
		control_wren => control_wren,
		status_rd => status_rd,
		status_wr => status_wr,
		status_rden => status_rden,
		status_wren => status_wren,
		addr_lsb_rd => addr_lsb_rd,
		addr_lsb_wr => addr_lsb_wr,
		addr_lsb_rden => addr_lsb_rden,
		addr_lsb_wren => addr_lsb_wren,
		addr_msb_rd => addr_msb_rd,
		addr_msb_wr => addr_msb_wr,
		addr_msb_rden => addr_msb_rden,
		addr_msb_wren => addr_msb_wren,
		data_rd => data_rd,
		data_wr => data_wr,
		data_rden => data_rden,
		data_wren => data_wren
	);

-- generate clock and reset
clk <= not clk after 12.5 ns;
reset <= '1', '0' after 100 ns;

-- stimulus process
process
	procedure send_ecr is
		constant cmdECR : std_logic_vector(8 downto 0) := "101100010";
	begin
		for I in cmdECR'range loop
			wait until rising_edge(clk);
			xc <= cmdECR(I);
		end loop;

		wait until rising_edge(clk);
		xc <= '0';
	end procedure;

	procedure load_bitstream(bitstream : in bitstream_t) is
	begin
		-- reset address register
		wait until rising_edge(clk);
		addr_msb_wr <= (others => '0');
		addr_msb_wren <= '1';
		wait until rising_edge(clk);
		addr_msb_wren <= '0';
		wait until rising_edge(clk);
		addr_lsb_wr <= (others => '0');
		addr_lsb_wren <= '1';
		wait until rising_edge(clk);
		addr_lsb_wren <= '0';

		-- now write all bitstream bytes into the data register
		for I in bitstream'range loop
			wait until rising_edge(clk);
			data_wr <= bitstream(I);
			data_wren <= '1';
			wait until rising_edge(clk);
			data_wren <= '0';
		end loop;

		-- update last-address
		wait until rising_edge(clk);
		control_wr <= "00000100";
		control_wren <= '1';
		wait until rising_edge(clk);
		control_wren <= '0';
	end procedure;
begin
	-- no data on XC
	xc <= '0';

	-- wait reset
	wait until reset = '0';
	wait for 100 ns;

	-- load the bitstream
	load_bitstream(example_bitstream);

	-- enable manual triggering
	wait until rising_edge(clk);
	control_wr <= "00100010";
	control_wren <= '1';
	wait until rising_edge(clk);
	control_wren <= '0';

	-- wait a bit
	wait for 200 ns;

	-- do a manual trigger
	wait until rising_edge(clk);
	control_wr <= "10100010";
	control_wren <= '1';
	wait until rising_edge(clk);
	control_wren <= '0';

	-- wait for the reconfiguration to start
	wait until reconf_active = '1';

	-- wait for the reconfiguration to finish
	wait until reconf_active = '0';

	-- enable ECR trigger
	wait until rising_edge(clk);
	control_wr <= "00100001";
	control_wren <= '1';
	wait until rising_edge(clk);
	control_wren <= '0';

	-- wait a bit
	wait for 200 ns;

	-- send ECR
	send_ecr;	

	wait;
end process;


end architecture;