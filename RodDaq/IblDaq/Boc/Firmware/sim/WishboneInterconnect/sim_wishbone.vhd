library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sim_wishbone is
end sim_wishbone;

architecture tb of sim_wishbone is
	-- DUT
	component wb_interconnect
		port (
			clk : in std_logic;
			reset : in std_logic;

			-- master 0
			wbm0_address : in std_logic_vector(15 downto 0);
			wbm0_dat_i : out std_logic_vector(7 downto 0);
			wbm0_dat_o : in std_logic_vector(7 downto 0);
			wbm0_cyc : in std_logic;
			wbm0_stb : in std_logic;
			wbm0_we : in std_logic;
			wbm0_ack : out std_logic;

			-- master 1
			wbm1_address : in std_logic_vector(15 downto 0);
			wbm1_dat_i : out std_logic_vector(7 downto 0);
			wbm1_dat_o : in std_logic_vector(7 downto 0);
			wbm1_cyc : in std_logic;
			wbm1_stb : in std_logic;
			wbm1_we : in std_logic;
			wbm1_ack : out std_logic;

			-- slave 0
			wbs0_address : out std_logic_vector(15 downto 0);
			wbs0_dat_i : out std_logic_vector(7 downto 0);
			wbs0_dat_o : in std_logic_vector(7 downto 0);
			wbs0_cyc : out std_logic;
			wbs0_stb : out std_logic;
			wbs0_we : out std_logic;
			wbs0_ack : in std_logic;

			-- slave 1
			wbs1_address : out std_logic_vector(15 downto 0);
			wbs1_dat_i : out std_logic_vector(7 downto 0);
			wbs1_dat_o : in std_logic_vector(7 downto 0);
			wbs1_cyc : out std_logic;
			wbs1_stb : out std_logic;
			wbs1_we : out std_logic;
			wbs1_ack : in std_logic;

			-- slave 2
			wbs2_address : out std_logic_vector(15 downto 0);
			wbs2_dat_i : out std_logic_vector(7 downto 0);
			wbs2_dat_o : in std_logic_vector(7 downto 0);
			wbs2_cyc : out std_logic;
			wbs2_stb : out std_logic;
			wbs2_we : out std_logic;
			wbs2_ack : in std_logic;

			-- slave 3
			wbs3_address : out std_logic_vector(15 downto 0);
			wbs3_dat_i : out std_logic_vector(7 downto 0);
			wbs3_dat_o : in std_logic_vector(7 downto 0);
			wbs3_cyc : out std_logic;
			wbs3_stb : out std_logic;
			wbs3_we : out std_logic;
			wbs3_ack : in std_logic;

			-- debugging
			debug : out std_logic_vector(95 downto 0)
		);
	end component;

	-- helper models
	component wb_master_model
		generic (
			NAME : string := "MASTER";
			PRESEED1 : positive := 1234;
			PRESEED2 : positive := 4321
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			wb_address : out std_logic_vector(15 downto 0);
			wb_dat_o : out std_logic_vector(7 downto 0);
			wb_dat_i : in std_logic_vector(7 downto 0);
			wb_cyc : out std_logic;
			wb_stb : out std_logic;
			wb_we : out std_logic;
			wb_ack : in std_logic
		);
	end component;

	component wb_slave_model
		port (
			clk : in std_logic;
			reset : in std_logic;

			wb_address : in std_logic_vector(15 downto 0);
			wb_dat_o : out std_logic_vector(7 downto 0);
			wb_dat_i : in std_logic_vector(7 downto 0);
			wb_cyc : in std_logic;
			wb_stb : in std_logic;
			wb_we : in std_logic;
			wb_ack : out std_logic
		);
	end component;

	-- signal declaration
	signal clk : std_logic := '0';
	signal reset : std_logic := '0';
	signal wbm0_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbm0_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm0_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm0_cyc : std_logic := '0';
	signal wbm0_stb : std_logic := '0';
	signal wbm0_we : std_logic := '0';
	signal wbm0_ack : std_logic := '0';
	signal wbm1_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbm1_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm1_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm1_cyc : std_logic := '0';
	signal wbm1_stb : std_logic := '0';
	signal wbm1_we : std_logic := '0';
	signal wbm1_ack : std_logic := '0';
	signal wbs0_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs0_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs0_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs0_cyc : std_logic := '0';
	signal wbs0_stb : std_logic := '0';
	signal wbs0_we : std_logic := '0';
	signal wbs0_ack : std_logic := '0';
	signal wbs1_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs1_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs1_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs1_cyc : std_logic := '0';
	signal wbs1_stb : std_logic := '0';
	signal wbs1_we : std_logic := '0';
	signal wbs1_ack : std_logic := '0';
	signal wbs2_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs2_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs2_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs2_cyc : std_logic := '0';
	signal wbs2_stb : std_logic := '0';
	signal wbs2_we : std_logic := '0';
	signal wbs2_ack : std_logic := '0';
	signal wbs3_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs3_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs3_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs3_cyc : std_logic := '0';
	signal wbs3_stb : std_logic := '0';
	signal wbs3_we : std_logic := '0';
	signal wbs3_ack : std_logic := '0';
begin

dut: wb_interconnect
	port map (
		clk => clk,
		reset => reset,
		wbm0_address => wbm0_address,
		wbm0_dat_o => wbm0_dat_o,
		wbm0_dat_i => wbm0_dat_i,
		wbm0_cyc => wbm0_cyc,
		wbm0_stb => wbm0_stb,
		wbm0_we => wbm0_we,
		wbm0_ack => wbm0_ack,
		wbm1_address => wbm1_address,
		wbm1_dat_o => wbm1_dat_o,
		wbm1_dat_i => wbm1_dat_i,
		wbm1_cyc => wbm1_cyc,
		wbm1_stb => wbm1_stb,
		wbm1_we => wbm1_we,
		wbm1_ack => wbm1_ack,
		wbs0_address => wbs0_address,
		wbs0_dat_o => wbs0_dat_o,
		wbs0_dat_i => wbs0_dat_i,
		wbs0_cyc => wbs0_cyc,
		wbs0_stb => wbs0_stb,
		wbs0_we => wbs0_we,
		wbs0_ack => wbs0_ack,
		wbs1_address => wbs1_address,
		wbs1_dat_o => wbs1_dat_o,
		wbs1_dat_i => wbs1_dat_i,
		wbs1_cyc => wbs1_cyc,
		wbs1_stb => wbs1_stb,
		wbs1_we => wbs1_we,
		wbs1_ack => wbs1_ack,
		wbs2_address => wbs2_address,
		wbs2_dat_o => wbs2_dat_o,
		wbs2_dat_i => wbs2_dat_i,
		wbs2_cyc => wbs2_cyc,
		wbs2_stb => wbs2_stb,
		wbs2_we => wbs2_we,
		wbs2_ack => wbs2_ack,
		wbs3_address => wbs3_address,
		wbs3_dat_o => wbs3_dat_o,
		wbs3_dat_i => wbs3_dat_i,
		wbs3_cyc => wbs3_cyc,
		wbs3_stb => wbs3_stb,
		wbs3_we => wbs3_we,
		wbs3_ack => wbs3_ack
	);

-- helper models
wbm0: wb_master_model
	generic map (
		NAME => "MASTER0",
		PRESEED1 => 1234,
		PRESEED2 => 4321
	)
	port map (
		clk => clk,
		reset => reset,
		wb_address => wbm0_address,
		wb_dat_o => wbm0_dat_o,
		wb_dat_i => wbm0_dat_i,
		wb_cyc => wbm0_cyc,
		wb_stb => wbm0_stb,
		wb_we => wbm0_we,
		wb_ack => wbm0_ack
	);

wbm1: wb_master_model
	generic map (
		NAME => "MASTER1",
		PRESEED1 => 56764,
		PRESEED2 => 2314
	)
	port map (
		clk => clk,
		reset => reset,
		wb_address => wbm1_address,
		wb_dat_o => wbm1_dat_o,
		wb_dat_i => wbm1_dat_i,
		wb_cyc => wbm1_cyc,
		wb_stb => wbm1_stb,
		wb_we => wbm1_we,
		wb_ack => wbm1_ack
	);

wbs0: wb_slave_model port map (
	clk => clk,
	reset => reset,
	wb_address => wbs0_address,
	wb_dat_o => wbs0_dat_o,
	wb_dat_i => wbs0_dat_i,
	wb_cyc => wbs0_cyc,
	wb_stb => wbs0_stb,
	wb_we => wbs0_we,
	wb_ack => wbs0_ack
);

wbs1: wb_slave_model port map (
	clk => clk,
	reset => reset,
	wb_address => wbs1_address,
	wb_dat_o => wbs1_dat_o,
	wb_dat_i => wbs1_dat_i,
	wb_cyc => wbs1_cyc,
	wb_stb => wbs1_stb,
	wb_we => wbs1_we,
	wb_ack => wbs1_ack
);

wbs2: wb_slave_model port map (
	clk => clk,
	reset => reset,
	wb_address => wbs2_address,
	wb_dat_o => wbs2_dat_o,
	wb_dat_i => wbs2_dat_i,
	wb_cyc => wbs2_cyc,
	wb_stb => wbs2_stb,
	wb_we => wbs2_we,
	wb_ack => wbs2_ack
);

wbs3: wb_slave_model port map (
	clk => clk,
	reset => reset,
	wb_address => wbs3_address,
	wb_dat_o => wbs3_dat_o,
	wb_dat_i => wbs3_dat_i,
	wb_cyc => wbs3_cyc,
	wb_stb => wbs3_stb,
	wb_we => wbs3_we,
	wb_ack => wbs3_ack
);

-- generate clock and reset
clk <= not clk after 5 ns;
reset <= '1', '0' after 1 us;

end architecture;