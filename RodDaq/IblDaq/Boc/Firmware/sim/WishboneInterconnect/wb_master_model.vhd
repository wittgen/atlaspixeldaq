library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;
use work.txt_util.ALL;

entity wb_master_model is
	generic (
		NAME : string := "MASTER";
		PRESEED1 : positive := 1234;
		PRESEED2 : positive := 4321
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		wb_address : out std_logic_vector(15 downto 0);
		wb_dat_o : out std_logic_vector(7 downto 0);
		wb_dat_i : in std_logic_vector(7 downto 0);
		wb_cyc : out std_logic;
		wb_stb : out std_logic;
		wb_we : out std_logic;
		wb_ack : in std_logic
	);
end wb_master_model;

architecture rtl of wb_master_model is

begin

-- master process
process
	variable seed1 : positive := preseed1;
	variable seed2 : positive := preseed2;
	variable rand : real;					-- random value (0...1)
	variable address : std_logic_vector(15 downto 0);
	variable completed : integer := 0;
	variable expected : std_logic_vector(7 downto 0);
begin
	-- cleanup
	wb_address <= (others => '0');
	wb_dat_o <= (others => '0');
	wb_cyc <= '0';
	wb_stb <= '0';
	wb_we <= '0';

	-- let's wait some time (50 ...1000ns)
	UNIFORM(seed1, seed2, rand);
	wait for (rand * 950 ns + 50 ns);

	-- wait for a rising edge
	wait until rising_edge(clk);

	-- generate a random address to be read
	UNIFORM(seed1, seed2, rand);
	address := std_logic_vector(to_unsigned(integer(trunc(rand*65536.0)), 16));

	-- start wishbone cycle
	wb_cyc <= '1';
	wb_stb <= '1';
	wb_we <= '0';
	wb_address <= address;

	-- wait for acknowledge
	wait until wb_ack = '1';
	wait until rising_edge(clk);

	-- expected data
	case address(15 downto 14) is
		when "00" => expected := address(7 downto 0);
		when "01" => expected := address(15 downto 8);
		when "10" => expected := address(7 downto 0) xor x"AB";
		when "11" => expected := address(15 downto 8) xor x"AB";
		when others => wb_dat_o <= x"00";
	end case;

	-- check readback
	if wb_dat_i /= expected then
		report NAME & ": Readback failed at address: 0x" & hstr(address) &
			   ", expected 0x" & hstr(expected) & " but got 0x" & hstr(wb_dat_i) severity error;
	end if;

	-- release bus
	wb_cyc <= '0';
	wb_stb <= '0';
	wb_we <= '0';
	wb_address <= (others => '0');

	-- count completed cycles
	completed := completed + 1;
	if completed mod 1000 = 0 then
		report NAME & ": " & integer'image(completed) & " completed cycles" severity note;
	end if;
end process;

end architecture;
