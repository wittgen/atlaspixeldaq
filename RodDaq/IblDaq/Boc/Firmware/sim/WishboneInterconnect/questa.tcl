# all design files come here including their library
set library_file_list {
	work {
		../txt_util.vhd
		sim_wishbone.vhd
		wb_master_model.vhd
		wb_slave_model.vhd
		../../rtl/bcf/wb_interconnect.vhd
	}
}

# compile all files
foreach {library file_list} $library_file_list {
	vlib $library
	foreach file $file_list {
		if [regexp {.vhdl?$} $file] {
			vcom -work $library $file
		} else {
			vlog -work $library $file
		}
	}
}

# disable unbound to initial warning
# suppress 8684

# start simulation
eval vsim sim_wishbone

# add some signals to waveform
add wave sim:/sim_wishbone/wbm0_cyc
add wave sim:/sim_wishbone/wbm1_cyc
add wave sim:/sim_wishbone/wbm0_stb
add wave sim:/sim_wishbone/wbm1_stb
add wave sim:/sim_wishbone/wbm0_ack
add wave sim:/sim_wishbone/wbm1_ack
add wave sim:/sim_wishbone/wbs0_ack
add wave sim:/sim_wishbone/wbs1_ack
add wave sim:/sim_wishbone/wbs2_ack
add wave sim:/sim_wishbone/wbs3_ack
add wave sim:/sim_wishbone/dut/arbiter_grant_m0
add wave sim:/sim_wishbone/dut/arbiter_grant_m1

# run for 100 us
run 100ms

# exit
# quit

