library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.MATH_REAL.ALL;

entity wb_slave_model is
	port (
		clk : in std_logic;
		reset : in std_logic;

		wb_address : in std_logic_vector(15 downto 0);
		wb_dat_o : out std_logic_vector(7 downto 0);
		wb_dat_i : in std_logic_vector(7 downto 0);
		wb_cyc : in std_logic;
		wb_stb : in std_logic;
		wb_we : in std_logic;
		wb_ack : out std_logic
	);
end wb_slave_model;

architecture beh of wb_slave_model is
begin

process begin
	wait until rising_edge(clk);

	if (wb_cyc = '1') and (wb_stb = '1') then
		if (wb_we = '0') then
			wb_ack <= '1';
			case wb_address(15 downto 14) is
				when "00" => wb_dat_o <= wb_address(7 downto 0);
				when "01" => wb_dat_o <= wb_address(15 downto 8);
				when "10" => wb_dat_o <= wb_address(7 downto 0) xor x"AB";
				when "11" => wb_dat_o <= wb_address(15 downto 8) xor x"AB";
				when others => wb_dat_o <= x"00";
			end case;
		else
			wb_ack <= '1';
		end if;
	else
		wb_dat_o <= (others => '0');
		wb_ack <= '0';
	end if;
end process;

end architecture;
