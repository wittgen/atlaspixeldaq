# all design files come here including their library
set library_file_list {
	work {
		../../rtl/bmf/slink/lscpack.vhd
		../../rtl/common/bocpack.vhd
		../../rtl/bmf/bmf_clocking.vhd
		../../rtl/bmf/rx/pix_phasealign.vhd
		../../rtl/bmf/rx/bmf_pix_rx.vhd
		../../rtl/bmf/rx/pixrx.vhd
		../../rtl/bmf/mcc-simulator/4-bit_counter.vhd
		../../rtl/bmf/mcc-simulator/8-bit_counter.vhd
		../../rtl/bmf/mcc-simulator/bit-flipper.vhd
		../../rtl/bmf/mcc-simulator/fe_cmd_decoder_pxl.vhd
		../../rtl/bmf/mcc-simulator/level1_stretcher.vhd
		../../rtl/bmf/mcc-simulator/lfsr.vhd
		../../rtl/bmf/mcc-simulator/multiplexer.vhd
		../../rtl/bmf/mcc-simulator/pixel_simulator.vhd
		../../rtl/bmf/mcc-simulator/treadmill.vhd
		../../build/bmf_ip/pix_monitor_fifo.vhd
		../../build/bmf_ip/pixsimFifo.vhd
		../txt_util.vhd
		sim_pixrx.vhd
	}
}

# compile all files
foreach {library file_list} $library_file_list {
	vlib $library
	foreach file $file_list {
		if [regexp {.vhdl?$} $file] {
			vcom -work $library $file
		} else {
			vlog -work $library $file
		}
	}
}

# disable unbound to initial warning
# suppress 8684

# start simulation
eval vsim sim_pixrx

# add some signals to waveform
# noview wave
# add wave sim:/sim_iblrx/*
add wave -position insertpoint  \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/DEFAULT_MASTER \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/DEFAULT_SLAVE \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/clk_regbank \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/clk_serial \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/reset \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/din \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/dout \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/speed \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/allrx_ctrl \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/allrx_ctrl_wren \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/reg_wr \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/reg_rd \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/reg_wren \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/reg_rden \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/debug_fast \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/debug_slow \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/rxreset \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/select_master \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/select_slave \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/din_master \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/din_slave \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_din \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_dout \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_wren \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_rden \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_full \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_fifo_empty \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_sr \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_srcnt \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_found_trailer \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_found_header \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_Z \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/rx_enable \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_enable \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/mon_raw \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/rod_enable \
sim:/sim_pixrx/pixrx_i/rxchA_generate(0)/rxch_i/interleaved

# run for 10 us
run 10us

# exit
#quit
