library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

use work.bocpack.all;


entity sim_pixrx is
end sim_pixrx;

architecture tb of sim_pixrx is
	component pixrx
		port (
			-- clock inputs
			clk_serial : in std_logic;
			clk_serial8x : in std_logic;
			clk_serial8x_serdesstrobe : in std_logic;
			clk_backplane : in std_logic;
			clk_regbank : in std_logic;

			-- reset input
			reset : in std_logic;

			-- data inputs
			din_optical : in std_logic_vector(23 downto 0);

			-- data outputs to the backplane
			dout : out std_logic_vector(47 downto 0);
			
			-- XC serial data for emulator
			xc_vec : in std_logic_vector(15 downto 0);

			-- register interface
			rxregs_wr : in RegBank_wr_t(0 to 1023);
			rxregs_rd : out RegBank_rd_t(0 to 1023);
			rxregs_wren : in RegBank_wren_t(0 to 1023);
			rxregs_rden : in RegBank_rden_t(0 to 1023);
			allrx_ctrl : in std_logic_vector(7 downto 0);
			allrx_ctrl_wren : in std_logic;

			-- phase registers
			phase_wr : IN RegBank_wr_t(0 to 47);
			phase_rd : OUT RegBank_rd_t(0 to 47);
			phase_wren : IN RegBank_wren_t(0 to 47);
			phase_rden : IN RegBank_rden_t(0 to 47);

			-- speed register
			speed_wr : in std_logic_vector(7 downto 0);
			speed_rd : out std_logic_vector(7 downto 0);
			speed_wren : in std_logic;
			speed_rden : in std_logic;

			-- rx debugging
			rxdebug_select : in std_logic_vector(3 downto 0);
			rxdebug_slow : out std_logic_vector(24 downto 0);
			rxdebug_fast : out std_logic_vector(21 downto 0)
		);
	end component;

	component bmf_clocking
		port
		(
			-- clock input
			clk40_p : in std_logic;
			clk40_n : in std_logic;
			clk100_p : in std_logic;
			clk100_n : in std_logic;
			mgtrefclk_p : in std_logic;
			mgtrefclk_n : in std_logic;

			-- reset input
			reset : in std_logic;

			-- clock outputs
			clk_regbank : out std_logic;
			clk_tx : out std_logic;
			clk_tx_fast : out std_logic;
			clk_tx_fast_strobe : out std_logic;
			clk_rx : out std_logic;
			clk_rx_fast : out std_logic;
			clk_rx_fast_strobe : out std_logic;
			clk_backplane : out std_logic;
			clk_slink : out std_logic;

			-- lock loss counter
			lockloss_cnt : out std_logic_vector(7 downto 0);

			-- status outputs
			locked : out std_logic
		);
	end component;

	-- clock  signals
	signal clk40_p : std_logic := '0';
	signal clk40_n : std_logic := '1';
	signal clk100_p : std_logic := '0';
	signal clk100_n : std_logic := '1';
	signal mgtrefclk_p : std_logic := '0';
	signal mgtrefclk_n : std_logic := '1';
	signal pll_reset : std_logic := '0';
	signal pll_locked : std_logic := '0';

	-- clock outputs
	signal clk_regbank : std_logic := '0';
	signal clk_rx : std_logic := '0';
	signal clk_rx_fast : std_logic := '0';
	signal clk_rx_fast_strobe : std_logic := '0';
	signal clk_backplane : std_logic := '0';

	-- reset
	signal reset : std_logic := '0';

	-- optical signals
	signal rx_opto : std_logic_vector(23 downto 0) := (others => '0');

	-- backplane signals
	signal rx_backplane : std_logic_vector(47 downto 0) := (others => '0');
	
	-- serial command
	signal xc : std_logic := '0';
	signal xc_vec : std_logic_vector(15 downto 0) := (others => '0');

	-- register bank
	signal rxregs_wr : RegBank_wr_t(0 to 1023) := (others => (others => '0'));
	signal rxregs_rd : RegBank_rd_t(0 to 1023) := (others => (others => '0'));
	signal rxregs_wren : RegBank_wren_t(0 to 1023) := (others => '0');
	signal rxregs_rden : RegBank_rden_t(0 to 1023) := (others => '0');
	signal allrx_ctrl : std_logic_vector(7 downto 0) := (others => '0');
	signal allrx_ctrl_wren : std_logic := '0';
	signal phase_wr : RegBank_wr_t(0 to 47) := (others => (others => '0'));
	signal phase_rd : RegBank_rd_t(0 to 47) := (others => (others => '0'));
	signal phase_wren : RegBank_wren_t(0 to 47) := (others => '0');
	signal phase_rden : RegBank_rden_t(0 to 47) := (others => '0');
	signal speed_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal speed_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal speed_wren : std_logic := '0';
	signal speed_rden : std_logic := '0';

	-- MCC DDR
	signal mcc_ddr_master_data : std_logic_vector(1 downto 0) := "00";
	signal mcc_ddr_slave_data : std_logic_vector(1 downto 0) := "00";
	signal mcc_master : std_logic := '0';
	signal mcc_slave : std_logic := '0';

	constant speed : std_logic_vector(1 downto 0) := "00";
	constant trig : std_logic_vector(4 downto 0) := "11101";
	constant emu_enable : std_logic := '0';
	constant emu_config0 : std_logic_vector(31 downto 0) := x"000000" & "00000" & speed & emu_enable;
	constant emu_config1 : std_logic_vector(31 downto 0) := x"00000000";
begin

-- generate clocks
clkgen: bmf_clocking
	port map (
		clk40_p => clk40_p,
		clk40_n => clk40_n,
		clk100_p => clk100_p,
		clk100_n => clk100_n,
		mgtrefclk_p => mgtrefclk_p,
		mgtrefclk_n => mgtrefclk_n,
		reset => pll_reset,
		clk_regbank => clk_regbank,
		clk_rx => clk_rx,
		clk_rx_fast => clk_rx_fast,
		clk_rx_fast_strobe => clk_rx_fast_strobe,
		clk_tx => open,
		clk_tx_fast => open,
		clk_tx_fast_strobe => open,
		clk_backplane => clk_backplane,
		clk_slink => open,
		lockloss_cnt => open,
		locked => pll_locked
	);

-- generate reset
reset <= not pll_locked;
pll_reset <= '1', '0' after 100 ns;

-- generate clocks
clk40_p <= not clk40_p after 12.5 ns;
clk40_n <= not clk40_p;
clk100_p <= not clk100_p after 5 ns;
clk100_n <= not clk100_p;

-- DUT
pixrx_i: pixrx
	port map (
		clk_serial => clk_rx,
		clk_serial8x => clk_rx_fast,
		clk_serial8x_serdesstrobe => clk_rx_fast_strobe,
		clk_backplane => clk_backplane,
		clk_regbank => clk_regbank,
		reset => reset,
		din_optical => rx_opto,
		dout => rx_backplane,
		xc_vec => xc_vec,
		rxregs_wr => rxregs_wr,
		rxregs_rd => rxregs_rd,
		rxregs_wren => rxregs_wren,
		rxregs_rden => rxregs_rden,
		allrx_ctrl => allrx_ctrl,
		allrx_ctrl_wren => allrx_ctrl_wren,
		phase_wr => phase_wr,
		phase_rd => phase_rd,
		phase_wren => phase_wren,
		phase_rden => phase_rden,
		speed_wr => speed_wr,
		speed_rd => speed_rd,
		speed_wren => speed_wren,
		speed_rden => speed_rden,
		rxdebug_select => "0000",
		rxdebug_fast => open,
		rxdebug_slow => open
	);

-- MCC generator
process
	procedure send_data(data : in std_logic_vector) is
		variable sr : std_logic_vector(data'range);
	begin
		sr := data;
	
		if (speed = "00") or (speed = "11") then
			for I in 0 to data'length loop
				wait until rising_edge(clk40_p);
				mcc_ddr_master_data <= sr(0) & sr(0);
				mcc_ddr_slave_data <= sr(0) & sr(0);
				sr := sr(1 to sr'length-1) & '0';
			end loop;
		elsif speed = "01" then
			for I in 0 to data'length/2 loop
				wait until rising_edge(clk40_p);
				mcc_ddr_master_data(0) <= sr(0);
				mcc_ddr_master_data(1) <= sr(1);
				mcc_ddr_slave_data(0) <= sr(0);
				mcc_ddr_slave_data(1) <= sr(1);
				sr := sr(2 to sr'length-1) & "00";
			end loop;
		elsif speed = "10" then
			for I in 0 to data'length/4 loop
				wait until rising_edge(clk40_p);
				mcc_ddr_master_data(0) <= sr(0);
				mcc_ddr_master_data(1) <= sr(2);
				mcc_ddr_slave_data(0) <= sr(1);
				mcc_ddr_slave_data(1) <= sr(3);
				sr := sr(4 to sr'length-1) & "0000";
			end loop;
		end if;
	end procedure;
begin
	wait until reset = '0';
	wait for 100 ns;

	loop
		wait for 100 ns;
		send_data("1110100000000000111111110101010110101010000000000000000000000000");
	end loop;
end process;

-- DDR (master)
mcc_ddr_master: ODDR2
	generic map(
		DDR_ALIGNMENT => "C0",	-- Sets output alignment to "NONE", "C0", "C1"
		INIT => '0',			-- Sets initial state of the Q output to 0 or 1
		SRTYPE => "SYNC")		-- Specifies "SYNC" or "ASYNC" set/reset
	port map (
		Q => mcc_master,		-- 1-bit output data
		C0 => clk40_p,			-- 1-bit clock input
		C1 => clk40_n,			-- 1-bit clock input
		CE => '1',				-- 1-bit clock enable input
		D0 => mcc_ddr_master_data(0),   -- 1-bit data input (associated with C0)
		D1 => mcc_ddr_master_data(1),   -- 1-bit data input (associated with C1)
		R => '0',				-- 1-bit reset input
		S => '0'				-- 1-bit set input
	);

-- DDR (slave)
mcc_ddr_slave: ODDR2
	generic map(
		DDR_ALIGNMENT => "C0", 	-- Sets output alignment to "NONE", "C0", "C1"
		INIT => '0', 			-- Sets initial state of the Q output to 0 or 1
		SRTYPE => "SYNC") 		-- Specifies "SYNC" or "ASYNC" set/reset
	port map (
		Q => mcc_slave,			-- 1-bit output data
		C0 => clk40_p,			-- 1-bit clock input
		C1 => clk40_n,			-- 1-bit clock input
		CE => '1',				-- 1-bit clock enable input
		D0 => mcc_ddr_slave_data(0),   -- 1-bit data input (associated with C0)
		D1 => mcc_ddr_slave_data(1),   -- 1-bit data input (associated with C1)
		R => '0',				-- 1-bit reset input
		S => '0'				-- 1-bit set input
	);

-- map MCC data to optical fibres (including invert of RX plugin)
process (mcc_slave, mcc_master) begin
	if speed /= "10" then
		for I in 0 to 23 loop
			rx_opto(I) <= not mcc_master;
		end loop;
	else
		for I in 0 to 23 loop
			if I >= 12 then
				rx_opto(I) <= not mcc_slave;
			else
				rx_opto(I) <= not mcc_master;
			end if;
		end loop;
	end if;
end process;

-- map XC
xc_vec <= (others => xc);

-- generate triggers
process begin
	wait until reset = '0';
	wait for 10 us;

	loop
		for I in trig'range loop
			wait until rising_edge(clk40_p);
			xc <= trig(I);
		end loop;
		wait until rising_edge(clk40_p);
		xc <= '0';

		wait for 100 us;
	end loop;
end process;

-- stimulus process
process begin
	wait until reset = '0';
	wait for 1 us;

	-- set speed
	wait until rising_edge(clk_regbank);
	speed_wr <= "000000" & speed;
	speed_wren <= '1';
	wait until rising_edge(clk_regbank);
	speed_wren <= '0';

	-- enable all RX
	wait until rising_edge(clk_regbank);
	allrx_ctrl <= x"1F";
	allrx_ctrl_wren <= '1';
	wait until rising_edge(clk_regbank);
	allrx_ctrl_wren <= '0';
	
	-- enable & configure all emulators
	for I in 0 to 15 loop
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+16) <= emu_config0(7 downto 0);
		rxregs_wren(32*I+16) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+16) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+17) <= emu_config0(15 downto 8);
		rxregs_wren(32*I+17) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+17) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+18) <= emu_config0(23 downto 16);
		rxregs_wren(32*I+18) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+18) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+19) <= emu_config0(31 downto 24);
		rxregs_wren(32*I+19) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+19) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+20) <= emu_config1(7 downto 0);
		rxregs_wren(32*I+20) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+20) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+21) <= emu_config1(15 downto 8);
		rxregs_wren(32*I+21) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+21) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+22) <= emu_config1(23 downto 16);
		rxregs_wren(32*I+22) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+22) <= '0';
		
		wait until rising_edge(clk_regbank);
		rxregs_wr(32*I+23) <= emu_config1(31 downto 24);
		rxregs_wren(32*I+23) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wren(32*I+23) <= '0';
	end loop;

	wait;
end process;

end architecture;