# all design files come here including their library
set library_file_list {
	decode_8b10b {
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_pkg.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_disp.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_lut_base.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_lut.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_bram.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_rtl.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_top.vhd
		../../rtl/bmf/rx/decode_8b10b/decode_8b10b_wrapper.vhd
	}
	
	work {
		../../rtl/common/bocpack.vhd
		../../rtl/bmf/slink/lscpack.vhd
		../../rtl/bmf/rx/iblrx.vhd
		../../rtl/bmf/rx/cdr_rce.vhd
		../../rtl/bmf/rx/bmf_rx.vhd
		../../rtl/bmf/rx/data_alignment.vhd
		../../rtl/bmf/rx/data_monitor_v2.vhd
		../../rtl/bmf/rx/dcs_monitor.vhd
		../../rtl/bmf/rx/prng_rx.vhd
		../../rtl/bmf/rx/ErrHisto.vhd
		../../rtl/bmf/rx/LinkOccMonitor.vhd
		../../rtl/bmf/rx/rx_status.vhd
		../../rtl/bmf/RxMux.vhd
		../../rtl/bmf/Mux_BOC2ROD.vhd
		../../rtl/common/SimpleReg.vhd
		../../rtl/common/prng.vhd
		../../build/bmf_ip/dcs_fifo.vhd
		../../build/bmf_ip/monitor_fifo.vhd
		../../build/bmf_ip/rx_clkcross.vhd
		../../build/bmf_ip/rx_fifo.vhd
		../../build/bmf_ip/rx_status_fifo.vhd
		../txt_util.vhd
		enc8b10b_sender.vhd
		sim_iblrx.vhd
	}
}

# compile all files
foreach {library file_list} $library_file_list {
	vlib $library
	foreach file $file_list {
		if [regexp {.vhdl?$} $file] {
			vcom -work $library $file
		} else {
			vlog -work $library $file
		}
	}
}

# disable unbound to initial warning
# suppress 8684

# start simulation
eval vsim sim_iblrx

# add some signals to waveform
# noview wave
# add wave sim:/sim_iblrx/*

# run for 100 us
run 100us

# exit
quit
