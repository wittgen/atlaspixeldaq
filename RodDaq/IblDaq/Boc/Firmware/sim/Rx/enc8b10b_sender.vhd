library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity enc8b10b_sender is
	generic (
		t_bit : time := 6.25 ns 	-- 160 MHz
	);
	port (
		dout : out std_logic
	);
end enc8b10b_sender;

architecture tb of enc8b10b_sender is
	type enc8b10b_data_t is array (natural range <>) of std_logic_vector(9 downto 0);
	constant enc8b10b_data : enc8b10b_data_t(0 to 16) := (
		"0110000011",	-- 0x183 (IDLE)
		"1001111100",	-- 0x27C (IDLE)
		"0110000011",	-- 0x183 (IDLE)
		"1001111100",	-- 0x27C (IDLE)
		"0110000011",	-- 0x183 (IDLE)
		"1001111100",	-- 0x27C (IDLE)
		"1110000011",	-- 0x383 (SOF)
		"1000101010",	-- 0x22A (0xEA)
		"0010111001",	-- 0x0B9 (0x00)
		"1000110101",	-- 0x235 (0xFF)
		"0111101100",	-- 0x1EC (0xEC)
		"1010010101",	-- 0x295 (0x55)
		"0101101010",	-- 0x16A (0xAA)
		"1010000011",	-- 0x283 (EOF)
		"1001111100",	-- 0x27C (IDLE)
		"0110000011",	-- 0x183 (IDLE)
		"1001111100"	-- 0x27C (IDLE)
	);
begin

-- sender
process begin
	for I in enc8b10b_data'range loop
		for J in 0 to 9 loop
			dout <= enc8b10b_data(I)(J);
			wait for t_bit;
		end loop;
	end loop;
end process;

end architecture;
