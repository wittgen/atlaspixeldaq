library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.txt_util.ALL;
use work.bocpack.ALL;

entity sim_iblrx is
end sim_iblrx;

architecture tb of sim_iblrx is
	-- sample 8b10b sender
	component enc8b10b_sender
		generic (
			t_bit : time := 6.25 ns 	-- 160 MHz
		);
		port (
			dout : out std_logic
		);
	end component;

	-- dut
	component iblrx
		port (
			-- clock inputs
			clk_serial : in std_logic;
			clk_serial4x : in std_logic;
			clk_serial4x_serdesstrobe : in std_logic;
			clk_backplane : in std_logic;
			clk_regbank : in std_logic;

			-- reset input
			reset : in std_logic;

			-- data inputs
			din_optical : in std_logic_vector(23 downto 0);
			din_loopback : in std_logic_vector(15 downto 0);
			din_feemu : in std_logic_vector(15 downto 0);

			-- data outputs to the backplane
			dout : out std_logic_vector(47 downto 0);

			-- register interface
			rxregs_wr : in RegBank_wr_t(0 to 1023);
			rxregs_rd : out RegBank_rd_t(0 to 1023);
			rxregs_wren : in RegBank_wren_t(0 to 1023);
			rxregs_rden : in RegBank_rden_t(0 to 1023);
			allrx_ctrl : in std_logic_vector(7 downto 0);
			allrx_ctrl_wren : in std_logic;

			-- rx debugging
			rxdebug_select : in std_logic_vector(3 downto 0);
			rxdebug_slow : out std_logic_vector(24 downto 0);
			rxdebug_fast : out std_logic_vector(21 downto 0);

			-- rx multiplexer selection
			rxmux_wr : in RegBank_wr_t(0 to 7);
			rxmux_rd : out RegBank_rd_t(0 to 7);
			rxmux_wren : in RegBank_wren_t(0 to 7);
			rxmux_rden : in RegBank_rden_t(0 to 7);

			-- BERT bypass
			bert_bypass : out std_logic_vector(15 downto 0);
			bert_bypass_valid : out std_logic_vector(15 downto 0)
		);
	end component;

	-- signals
	signal clk_serial : std_logic := '0';
	signal clk_serial4x : std_logic := '0';
	signal clk_serial4x_serdesstrobe : std_logic := '0';
	signal clk_backplane : std_logic := '0';
	signal clk_regbank : std_logic := '0';
	signal reset : std_logic := '0';
	signal din_optical : std_logic_vector(23 downto 0) := (others => '0');
	signal din_loopback : std_logic_vector(15 downto 0) := (others => '0');
	signal din_feemu : std_logic_vector(15 downto 0) := (others => '0');
	signal dout : std_logic_vector(47 downto 0) := (others => '0');
	signal rxregs_wr : RegBank_wr_t(0 to 1023) := (others => (others => '0'));
	signal rxregs_rd : RegBank_rd_t(0 to 1023) := (others => (others => '0'));
	signal rxregs_wren : RegBank_wren_t(0 to 1023) := (others => '0');
	signal rxregs_rden : RegBank_rden_t(0 to 1023) := (others => '0');
	signal allrx_ctrl : std_logic_vector(7 downto 0) := (others => '0');
	signal allrx_ctrl_wren : std_logic := '0';

	signal serdesstrobe_sr : std_logic_vector(3 downto 0) := "0001";
begin

-- generate 23 example senders
senders: for I in 0 to 23 generate
	sender_I: enc8b10b_sender
		generic map (t_bit => 6248 ps)
		port map (dout => din_optical(I));
end generate;

dut: iblrx port map (
	clk_serial => clk_serial,
	clk_serial4x => clk_serial4x,
	clk_serial4x_serdesstrobe => clk_serial4x_serdesstrobe,
	clk_backplane => clk_backplane,
	clk_regbank => clk_regbank,
	reset => reset,
	din_optical => din_optical,
	din_loopback => (others => '0'),	-- unused
	din_feemu => (others => '0'),		-- unused
	dout => dout,
	rxregs_wr => rxregs_wr,
	rxregs_rd => rxregs_rd,
	rxregs_wren => rxregs_wren,
	rxregs_rden => rxregs_rden,
	allrx_ctrl => allrx_ctrl,
	allrx_ctrl_wren => allrx_ctrl_wren,
	rxdebug_slow => open,				-- unused in this testbench
	rxdebug_fast => open,				-- unused in this testbench
	rxdebug_select => "0000",			-- unused in this testbench
	rxmux_wr => (others => (others => '0')),	-- unused
	rxmux_rd => open,					-- unused
	rxmux_wren => (others => '0'),		-- unused
	rxmux_rden => (others => '0'),		-- unused
	bert_bypass => open,				-- unused
	bert_bypass_valid => open			-- unused
);

-- clocking
clk_serial <= not clk_serial after 3124 ps;
clk_serial4x <= not clk_serial4x after 781 ps;
clk_backplane <= not clk_backplane after 6248 ps;
clk_regbank <= not clk_regbank after 12496 ps; 

-- generate serdesstrobe
process begin
	wait until rising_edge(clk_serial4x);
	serdesstrobe_sr <= serdesstrobe_sr(2 downto 0) & serdesstrobe_sr(3);
end process;
clk_serial4x_serdesstrobe <= serdesstrobe_sr(0);

-- generate reset
reset <= '1', '0' after 1 us;

-- stimulus process
process
	procedure register_write(channel : in integer range 0 to 31;
				 regnum : in integer range 0 to 31;
				 value : in std_logic_vector(7 downto 0)) is
	begin
		wait until rising_edge(clk_regbank);
		rxregs_wr(channel * 32 + regnum) <= value;
		rxregs_wren(channel * 32 + regnum) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_wr(channel * 32 + regnum) <= x"00";
		rxregs_wren(channel * 32 + regnum) <= '0';
	end procedure;

	procedure register_read(channel : in integer range 0 to 31;
				regnum : in integer range 0 to 31;
				value : out std_logic_vector(7 downto 0)) is
		variable tmp : std_logic_vector(7 downto 0);
	begin
		wait until rising_edge(clk_regbank);
		tmp := rxregs_rd(channel * 32 + regnum);
		value := tmp;
		rxregs_rden(channel * 32 + regnum) <= '1';
		wait until rising_edge(clk_regbank);
		rxregs_rden(channel * 32 + regnum) <= '0';

		report "Readback Channel " & str(channel) & ", Register " & str(regnum) & ": 0x" & hstr(tmp) severity note;
	end procedure;

	variable readback : std_logic_vector(7 downto 0);
begin
	wait for 2 us;

	-- enable all RX channels
	wait until rising_edge(clk_regbank);
	allrx_ctrl <= x"01";
	allrx_ctrl_wren <= '1';
	wait until rising_edge(clk_regbank);
	allrx_ctrl <= x"00";
	allrx_ctrl_wren <= '0';

	-- wait some time for the RX to sync
	wait for 50 us;

	-- reset and readback all status registers
	for I in 0 to 15 loop
		register_write(I, 1, x"FF");
		wait for 200 ns;
		register_read(I, 1, readback);

		if readback /= x"61" then
			report "Channel " & str(I) & " is not in the right state. Check FAILED." severity error;
		end if;
	end loop;	

	-- wait
	wait;
end process;
	

end architecture;
