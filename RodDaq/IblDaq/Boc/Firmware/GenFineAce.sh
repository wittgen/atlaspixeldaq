#!/bin/bash

# check command line
if [ $# -lt 3 ]; then
	echo "Usage: $0 <NCDFile> <ValueFile> <OutputFile>"
	exit 1
fi

# get files
FILE_NCD=$1
FILE_VALUE=$2
FILE_OUTPUT=$3
FILE_BIT=`basename $FILE_NCD .ncd`.bit
FILE_ACE=`basename $FILE_NCD .ncd`.ace

# check if all files exist
if [ ! -f $FILE_NCD ]; then
	echo "NCD '$FILE_NCD' was not found."
	exit 1
fi
if [ ! -f $FILE_VALUE ]; then
	echo "Values '$FILE_VALUE' was not found."
	exit 1
fi

# check for ISE tools
if [ -z ${ISE_BIN_PATH} ] ; then
        echo ERROR: ISE_BIN_PATH not set
        exit
fi

BITGEN=${ISE_BIN_PATH}/bitgen
IMPACT=${ISE_BIN_PATH}/impact
FPGA_EDLINE=${ISE_BIN_PATH}/fpga_edline

# save working directory
WORK=`pwd`

# create temporary directory
TMPDIR=`mktemp -d -p $WORK`
if [ -z ${TMPDIR} ]; then
	echo "ERROR: TMPDIR not set"
	exit 1
fi

# copy ncd to temporary directory
cp $FILE_NCD $TMPDIR

# create script
echo "open design `basename $FILE_NCD`" > $TMPDIR/fpga_editor.scr

# read value file line by line and generate script
IFS=,
while read CH VALUE
do
	if ! [[ -z "$CH" ]] && ! [[ -z "$VALUE" ]] && ! [[ "$CH" == \#* ]]
	then
		echo "unselect -all" >> $TMPDIR/fpga_editor.scr
		echo "select comp 'bmf_main_i/transmitters[$CH].bmf_tx_i/IODELAY_i'" >> $TMPDIR/fpga_editor.scr
		echo "setattr comp bmf_main_i/transmitters[$CH].bmf_tx_i/IODELAY_i ODELAY_VALUE \"$VALUE\"" >> $TMPDIR/fpga_editor.scr	
	fi
done < $FILE_VALUE

# finish script
echo "save design" >> $TMPDIR/fpga_editor.scr
echo "exit" >> $TMPDIR/fpga_editor.scr

# switch to tmp dir and execute
cd $TMPDIR

# bitgen options
echo "-w" > bitgen.ut
echo "-g DebugBitstream:No" >> bitgen.ut
echo "-g Binary:no" >> bitgen.ut
echo "-g CRC:Enable" >> bitgen.ut
echo "-g Reset_on_err:No" >> bitgen.ut
echo "-g ConfigRate:2" >> bitgen.ut
echo "-g ProgPin:PullUp" >> bitgen.ut
echo "-g TckPin:PullUp" >> bitgen.ut
echo "-g TdiPin:PullUp" >> bitgen.ut
echo "-g TdoPin:PullUp" >> bitgen.ut
echo "-g TmsPin:PullUp" >> bitgen.ut
echo "-g UnusedPin:PullNone" >> bitgen.ut
echo "-g UserID:0xFFFFFFFF" >> bitgen.ut
echo "-g ExtMasterCclk_en:No" >> bitgen.ut
echo "-g SPI_buswidth:1" >> bitgen.ut
echo "-g TIMER_CFG:0xFFFF" >> bitgen.ut
echo "-g multipin_wakeup:No" >> bitgen.ut
echo "-g StartUpClk:CClk" >> bitgen.ut
echo "-g DONE_cycle:4" >> bitgen.ut
echo "-g GTS_cycle:5" >> bitgen.ut
echo "-g GWE_cycle:6" >> bitgen.ut
echo "-g LCK_cycle:NoWait" >> bitgen.ut
echo "-g Security:None" >> bitgen.ut
echo "-g DonePipe:No" >> bitgen.ut
echo "-g DriveDone:Yes" >> bitgen.ut
echo "-g Encrypt:No" >> bitgen.ut
echo "-g en_sw_gsr:No" >> bitgen.ut
echo "-g drive_awake:No" >> bitgen.ut
echo "-g sw_clk:Startupclk" >> bitgen.ut
echo "-g sw_gwe_cycle:5" >> bitgen.ut
echo "-g sw_gts_cycle:4" >> bitgen.ut

# generate impact script
echo "setMode -bs" > impact_svf.cmd
echo "setCable -p svf -file `basename $FILE_ACE .ace`.svf" >> impact_svf.cmd
echo "addDevice -p 1 -file `basename $FILE_NCD .ncd`.bit" >> impact_svf.cmd
echo "program -p 1" >> impact_svf.cmd
echo "closeCable" >> impact_svf.cmd
echo "setMode -bs" >> impact_svf.cmd
echo "svf2ace -wtck -tck 25e6 -i `basename $FILE_ACE .ace`.svf -o `basename $FILE_ACE`" >> impact_svf.cmd
echo "quit" >> impact_svf.cmd

# execute script
${FPGA_EDLINE} -p fpga_editor.scr
${BITGEN} -f bitgen.ut `basename $FILE_NCD`
${IMPACT} -batch impact_svf.cmd

# copy to output file
cd $OLDPWD
cp $TMPDIR/`basename $FILE_ACE` $FILE_OUTPUT

# remove temp dir
rm -rf $TMPDIR
