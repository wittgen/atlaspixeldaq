setMode -bs
setCable -port auto
addDevice -p 1 -file binaries/bcf_revC.bit
attachFlash -p 1 -spi M25P64
assignfiletoattachedflash -p 1 -file binaries/bcf_revC.mcs
program -p 1 -e -v -spionly
closeCable
quit
