----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Marius Wensing
-- E-Mail:				wensing@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				SimpleReg
-- Description:			A simple configurable register
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 30.11.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
-- Comments ...
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SimpleReg is
	generic (
		rst_value : std_logic_vector(7 downto 0) := x"00";
		readonly : string := "false"
	);
	port (
		-- clock and reset
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		wr : in std_logic_vector(7 downto 0) := (others => '0');
		rd : out std_logic_vector(7 downto 0) := (others => '0');
		wren : in std_logic := '0';
		rden : in std_logic := '0';
		
		regval : out std_logic_vector(7 downto 0) := (others => '0')
	);
end SimpleReg;

architecture Behavioral of SimpleReg is
	signal reg_value : std_logic_vector(7 downto 0) := rst_value;
begin

rd <= reg_value;
regval <= reg_value;
	
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		reg_value <= rst_value;
	else
		if readonly = "false" then
			if wren = '1' then
				reg_value <= wr;
			end if;
		end if;
	end if;
end process;
	
end architecture;