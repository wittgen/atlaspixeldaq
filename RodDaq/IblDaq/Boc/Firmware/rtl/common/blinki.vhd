----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Marius Wensing
-- E-Mail:				wensing@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				Blinki
-- Description:			A heartbeat generator with configurable frequency
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 30.11.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
-- Comments ...
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity blinki is
	generic
	(
		f_clk : natural := 40000000;	-- input clock frequency
		f_blinki : natural := 1 		-- output clock frequency
	);
	port
	(
		clk : IN std_logic;
		reset : IN std_logic;          
		blinki : OUT std_logic
	);
end entity;

architecture Behavioral of blinki is
	signal blinki_cnt : integer range 0 to ((f_clk / (2*f_blinki))-1) := 0;
	signal output : std_logic := '0';
begin

process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		blinki_cnt <= 0;
		output <= '0';
	else
		if blinki_cnt = ((f_clk / (2*f_blinki))-1) then
			blinki_cnt <= 0;
			output <= not output;
		else
			blinki_cnt <= blinki_cnt + 1;
		end if;
	end if;
end process;
blinki <= output;


end architecture;
