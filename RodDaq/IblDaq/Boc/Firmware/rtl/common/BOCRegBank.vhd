-- register bank
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.ALL;

entity BOCRegBank is
	generic
	(
		-- number of registers
		nReg : natural := 32
	);
	port
	(
		-- clock and reset
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		-- wishbone interface
		wb_dat_i : in std_logic_vector(7 downto 0) := (others => '0');
		wb_dat_o : out std_logic_vector(7 downto 0) := (others => '0');
		wb_adr_i : in std_logic_vector(15 downto 0) := (others => '0');
		wb_cyc_i : in std_logic := '0';
		wb_stb_i : in std_logic := '0';
		wb_we_i : in std_logic := '0';
		wb_ack_o : out std_logic := '0';
		
		-- register interface
		reg_wr : out RegBank_wr_t;
		reg_wren : out RegBank_wren_t;
		reg_rd : in RegBank_rd_t;
		reg_rden : out RegBank_rden_t
	);
end BOCRegBank;

architecture Behavioral of BOCRegBank is
	signal we_reg : std_logic := '0';
	signal rd_reg : std_logic := '0';
	signal rd : std_logic := '0';
	signal we : std_logic := '0';
begin

--rd <= (not wb_we_i) and wb_stb_i and wb_cyc_i;
--we <= wb_we_i and wb_stb_i and wb_cyc_i;

process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		wb_ack_o <= '0';
		wb_dat_o <= (others => '0');
		we_reg <= '0';
		rd_reg <= '0';
		for I in 0 to nReg-1 loop
			reg_wr(I) <= (others => '0');
			reg_wren(I) <= '0';
			reg_rden(I) <= '0';
		end loop;
	else
		rd <= (not wb_we_i) and wb_stb_i and wb_cyc_i;
		we <= wb_we_i and wb_stb_i and wb_cyc_i;
		rd_reg <= rd;
		we_reg <= we;
		wb_ack_o <= '0';
		
		for I in 0 to nReg-1 loop
			reg_wr(I) <= wb_dat_i;
			reg_wren(I) <= '0';
			reg_rden(I) <= '0';
		end loop;
		--wb_dat_o <= (others => '0');
		
		if (wb_cyc_i = '1') and (wb_stb_i = '1') then
			if (we = '1') or (rd = '1') then
				wb_ack_o <= '1';
			end if;

			if to_integer(unsigned(wb_adr_i)) < nReg then
				if (we = '1') and (we_reg = '0') then		-- start of write cycle
					reg_wren(to_integer(unsigned(wb_adr_i))) <= '1';
				elsif (rd = '1') and (rd_reg = '0') then
					reg_rden(to_integer(unsigned(wb_adr_i))) <= '1';
					wb_dat_o <= reg_rd(to_integer(unsigned(wb_adr_i)));
				end if;
			end if;
		end if;
	end if;
end process;

end Behavioral;

