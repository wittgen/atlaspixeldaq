-- simple priorising interrupt controller
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity intctrl is
	generic (
		channels : integer := 64
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		
		-- interrupt intputs
		interrupt_in : in std_logic_vector(channels-1 downto 0);
	
		-- global interrupt output
		interrupt_out : out std_logic;

		-- control and status register
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rden : in std_logic;
		csr_wren : in std_logic;
		int_rd : out std_logic_vector(7 downto 0);
		int_wr : in std_logic_vector(7 downto 0);
		int_rden : in std_logic;
		int_wren : in std_logic
	);
end intctrl;

architecture RTL of intctrl is
	-- interrupt enable and flag
	signal interrupt_enable : std_logic := '0';
	signal interrupt_flag : std_logic := '0';
	
	-- interrupt register
	signal interrupt_reg : std_logic_vector(7 downto 0);
begin

-- interrupt priority decoder
process begin
	wait until rising_edge(clk);

	-- default assignment
	interrupt_reg <= (others => '0');
	interrupt_flag <= '0';

	-- loop through all interrupt sources
	for I in 0 to channels-1 loop
		if interrupt_in(I) = '1' then
			interrupt_reg <= std_logic_vector(to_unsigned(I, 8));
			interrupt_flag <= '1';
		end if;
	end loop;
end process;

-- write interrupt enable
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		interrupt_enable <= '0';
	else
		if csr_wren = '1' then
			interrupt_enable <= csr_wr(0);
		end if;
	end if;
end process;

-- read interrupt csr
csr_rd <= "000000" & interrupt_flag & interrupt_enable;

-- output interrupt register and flag
interrupt_out <= interrupt_flag and interrupt_enable;

end architecture;
