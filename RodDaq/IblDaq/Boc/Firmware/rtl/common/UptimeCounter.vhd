library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity UptimeCounter is
	generic (
		f_clk : integer := 40000000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		uptime_rd : out std_logic_vector(7 downto 0);
		uptime_wr : in std_logic_vector(7 downto 0);
		uptime_rden : in std_logic;
		uptime_wren : in std_logic
	);
end UptimeCounter;

architecture rtl of UptimeCounter is
	signal prescaler : integer range 0 to f_clk - 1 := 0;
	signal uptime : unsigned(31 downto 0) := (others => '0');
	signal tmp : std_logic_vector(31 downto 0) := (others => '0');
begin

-- count uptime
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		prescaler <= 0;
		uptime <= (others => '0');
	else
		if prescaler = f_clk - 1 then
			prescaler <= 0;
			uptime <= uptime + 1;
		else
			prescaler <= prescaler + 1;
		end if;
	end if;
end process;

-- readout uptime
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		tmp <= (others => '0');
	else
		-- trigger copy on (dummy) write
		if uptime_wren = '1' then
			tmp <= std_logic_vector(uptime);
		end if;

		-- shift out uptime
		if uptime_rden = '1' then
			tmp <= x"00" & tmp(31 downto 8);
		end if;
	end if;
end process;
uptime_rd <= tmp(7 downto 0);


end architecture;