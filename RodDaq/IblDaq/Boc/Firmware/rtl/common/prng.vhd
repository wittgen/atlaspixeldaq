-- Pseudo-random number generator
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity prng is
	generic (
		width : integer := 32
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		load : in std_logic;
		seed : in std_logic_vector(width-1 downto 0);
		step : in std_logic;
		dout : out std_logic_vector(width-1 downto 0)
	);
end entity;

architecture rtl of prng is
	signal lfsr : std_logic_vector(width-1 downto 0) := (others => '0');
begin

-- test arguments
assert ((width >= 3) and (width <= 32)) report "width must be between 3 and 32!";

process begin
	wait until rising_edge(clk);

	if reset = '1' then
		lfsr <= seed;
		--dout <= seed;
	else
		if load = '1' then
			lfsr <= seed;
		elsif step = '1' then
			lfsr(width-1 downto 1) <= lfsr(width-2 downto 0);
			
			case width is
				when 3 => lfsr(0) <= not (lfsr(2) xor lfsr(1));
				when 4 => lfsr(0) <= not (lfsr(3) xor lfsr(2));
				when 5 => lfsr(0) <= not (lfsr(4) xor lfsr(2));
				when 6 => lfsr(0) <= not (lfsr(5) xor lfsr(4));
				when 7 => lfsr(0) <= not (lfsr(6) xor lfsr(5));
				when 8 => lfsr(0) <= not (lfsr(7) xor lfsr(5) xor lfsr(4) xor lfsr(3));
				when 9 => lfsr(0) <= not (lfsr(8) xor lfsr(4));
				when 10 => lfsr(0) <= not (lfsr(9) xor lfsr(6));
				when 11 => lfsr(0) <= not (lfsr(10) xor lfsr(8));
				when 12 => lfsr(0) <= not (lfsr(11) xor lfsr(5) xor lfsr(3) xor lfsr(0));
				when 13 => lfsr(0) <= not (lfsr(12) xor lfsr(3) xor lfsr(2) xor lfsr(0));
				when 14 => lfsr(0) <= not (lfsr(13) xor lfsr(4) xor lfsr(2) xor lfsr(0));
				when 15 => lfsr(0) <= not (lfsr(14) xor lfsr(13));
				when 16 => lfsr(0) <= not (lfsr(15) xor lfsr(14) xor lfsr(12) xor lfsr(3));
				when 17 => lfsr(0) <= not (lfsr(16) xor lfsr(13));
				when 18 => lfsr(0) <= not (lfsr(17) xor lfsr(10));
				when 19 => lfsr(0) <= not (lfsr(18) xor lfsr(5) xor lfsr(1) xor lfsr(0));
				when 20 => lfsr(0) <= not (lfsr(19) xor lfsr(16));
				when 21 => lfsr(0) <= not (lfsr(20) xor lfsr(18));
				when 22 => lfsr(0) <= not (lfsr(21) xor lfsr(20));
				when 23 => lfsr(0) <= not (lfsr(22) xor lfsr(17));
				when 24 => lfsr(0) <= not (lfsr(23) xor lfsr(22) xor lfsr(21) xor lfsr(16));
				when 25 => lfsr(0) <= not (lfsr(24) xor lfsr(21));
				when 26 => lfsr(0) <= not (lfsr(25) xor lfsr(5) xor lfsr(1) xor lfsr(0));
				when 27 => lfsr(0) <= not (lfsr(26) xor lfsr(4) xor lfsr(1) xor lfsr(0));
				when 28 => lfsr(0) <= not (lfsr(27) xor lfsr(24));
				when 29 => lfsr(0) <= not (lfsr(28) xor lfsr(26));
				when 30 => lfsr(0) <= not (lfsr(29) xor lfsr(5) xor lfsr(3) xor lfsr(0));
				when 31 => lfsr(0) <= not (lfsr(30) xor lfsr(27));
				when 32 => lfsr(0) <= not (lfsr(31) xor lfsr(21) xor lfsr(1) xor lfsr(0));
				when others => lfsr(0) <= lfsr(width-1);
			end case;
		end if;

		-- copy lfsr to output
		--dout <= lfsr;
	end if;
end process;

dout <= lfsr;

end architecture;
