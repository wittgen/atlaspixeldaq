library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity I2CCtrl is
	generic (
		f_clk : integer := 40000000;
		f_scl : integer := 100000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		ctrl_rd : out std_logic_vector(7 downto 0);
		ctrl_wr : in std_logic_vector(7 downto 0);
		ctrl_rden : in std_logic;
		ctrl_wren : in std_logic;
		status_rd : out std_logic_vector(7 downto 0);
		status_wr : in std_logic_vector(7 downto 0);
		status_rden : in std_logic;
		status_wren : in std_logic;
		data_rd : out std_logic_vector(7 downto 0);
		data_wr : in std_logic_vector(7 downto 0);
		data_rden : in std_logic;
		data_wren : in std_logic;

		scl : inout std_logic;
		sda : inout std_logic
	);
end I2CCtrl;

architecture RTL of I2CCtrl is
	-- i2c master from opencores
	component i2c_master_byte_ctrl
		port (
			clk    : in std_logic;
			rst    : in std_logic; -- synchronous active high reset (WISHBONE compatible)
			nReset : in std_logic;	-- asynchornous active low reset (FPGA compatible)
			ena    : in std_logic; -- core enable signal

			clk_cnt : in unsigned(15 downto 0);	-- 4x SCL

			-- input signals
			start,
			stop,
			read,
			write,
			ack_in : std_logic;
			din    : in std_logic_vector(7 downto 0);

			-- output signals
			cmd_ack  : out std_logic; -- command done
			ack_out  : out std_logic;
			i2c_busy : out std_logic; -- arbitration lost
			i2c_al   : out std_logic; -- i2c bus busy
			dout     : out std_logic_vector(7 downto 0);

			-- i2c lines
			scl_i   : in std_logic;  -- i2c clock line input
			scl_o   : out std_logic; -- i2c clock line output
			scl_oen : out std_logic; -- i2c clock line output enable, active low
			sda_i   : in std_logic;  -- i2c data line input
			sda_o   : out std_logic; -- i2c data line output
			sda_oen : out std_logic  -- i2c data line output enable, active low
		);
	end component;

	constant clk_cnt : unsigned(15 downto 0) := to_unsigned(f_clk / (4 * f_scl), 16);

	-- i2c core
	signal i2c_start : std_logic := '0';
	signal i2c_stop : std_logic := '0';
	signal i2c_read : std_logic := '0';
	signal i2c_write : std_logic := '0';
	signal i2c_ack_in : std_logic := '0';
	signal i2c_din : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_cmd_ack : std_logic := '0';
	signal i2c_ack_out : std_logic := '0';
	signal i2c_busy : std_logic := '0';
	signal i2c_arblost : std_logic := '0';
	signal i2c_dout : std_logic_vector(7 downto 0) := (others => '0');

	-- i2c physical layer
	signal scl_i : std_logic := '0';
	signal scl_o : std_logic := '0';
	signal scl_oen : std_logic := '1';
	signal sda_i : std_logic := '0';
	signal sda_o : std_logic := '0';
	signal sda_oen : std_logic := '1';

	-- rx / tx data
	signal rxdata : std_logic_vector(7 downto 0) := (others => '0');
	signal txdata : std_logic_vector(7 downto 0) := (others => '0');

	-- done signal
	signal done : std_logic := '1';
	
	-- fsm
	type fsm_states is (st_idle, st_exec);
	signal Z : fsm_states := st_idle;
begin

-- i2c controller
i2c_ctrl: i2c_master_byte_ctrl
	PORT MAP (
		clk => clk,
		rst => reset,
		nReset => '1',
		ena => '1',
		clk_cnt => clk_cnt,
		start => i2c_start,
		stop => i2c_stop,
		read => i2c_read,
		write => i2c_write,
		ack_in => i2c_ack_in,
		din => i2c_din,
		cmd_ack => i2c_cmd_ack,
		ack_out => i2c_ack_out,
		i2c_busy => i2c_busy,
		i2c_al => i2c_arblost,
		dout => i2c_dout,
		scl_i => scl_i,
		scl_o => scl_o,
		scl_oen => scl_oen,
		sda_i => sda_i,
		sda_o => sda_o,
		sda_oen => sda_oen
	);

-- map physical layer
scl <= scl_o when scl_oen = '0' else 'Z';
sda <= sda_o when sda_oen = '0' else 'Z';
scl_i <= scl;
sda_i <= sda;

-- readback control register
ctrl_rd(0) <= i2c_read;
ctrl_rd(1) <= i2c_write;
ctrl_rd(2) <= i2c_start;
ctrl_rd(3) <= i2c_stop;
ctrl_rd(4) <= i2c_ack_in;

-- readback status register
status_rd(0) <= done;
status_rd(1) <= i2c_ack_out;
status_rd(2) <= i2c_busy;
status_rd(3) <= i2c_arblost;

-- readback data register
data_rd <= rxdata;

-- write data register
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		txdata <= (others => '0');
	else
		if data_wren = '1' then
			txdata <= data_wr;
		end if;
	end if;
end process;

-- fsm
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		i2c_read <= '0';
		i2c_write <= '0';
		i2c_start <= '0';
		i2c_stop <= '0';
		i2c_ack_in <= '0';
		done <= '1';
		rxdata <= (others => '0');
		Z <= st_idle;
	else
		case Z is
			when st_idle =>
				if ctrl_wren = '1' then
					if ctrl_wr(3 downto 0) /= "0000" then
						i2c_read <= ctrl_wr(0);
						i2c_write <= ctrl_wr(1);
						i2c_start <= ctrl_wr(2);
						i2c_stop <= ctrl_wr(3);
						i2c_ack_in <= ctrl_wr(4);
						i2c_din <= txdata;
						done <= '0';
						Z <= st_exec;
					end if;
				end if;

			when st_exec =>
				if i2c_cmd_ack = '1' then
					i2c_read <= '0';
					i2c_write <= '0';
					i2c_start <= '0';
					i2c_stop <= '0';
					done <= '1';
					rxdata <= i2c_dout;
					Z <= st_idle;
				end if;
		end case;
	end if;
end process;

end architecture;

