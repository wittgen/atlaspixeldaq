library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BERTSender is
	generic (
		prng32_seed : std_logic_vector := x"3fa91c20";
		prng8_seed : std_logic_vector := x"91"
	);
	port (
		-- clocking and reset
		clk_serial : in std_logic;
		reset : in std_logic;

		-- mode settings
		pattern_select : in std_logic_vector(1 downto 0);
		mcc_pattern_break : in std_logic_vector(5 downto 0);

		-- data output
		dout : out std_logic
	);
end BERTSender;

architecture rtl of BERTSender is
	component prng
		generic (
			width : integer := 32
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			load : in std_logic;
			seed : in std_logic_vector(width-1 downto 0);
			step : in std_logic;
			dout : out std_logic_vector(width-1 downto 0)
		);
	end component;

	component BERTMCCGenerator
		port (
			clk_serial : in std_logic;
			reset : in std_logic;

			mcc_pattern_break : in std_logic_vector(5 downto 0);

			mcc_pattern_dout : out std_logic
		);
	end component;

	signal prng32_dout : std_logic_vector(31 downto 0);
	signal prng8_dout : std_logic_vector(7 downto 0);
	signal enc8b10b_pattern : std_logic_vector(19 downto 0) := "10011111000110000011";
	signal mcc_pattern_dout : std_logic := '0';
begin

prng8_i: prng
	generic map (
		width => 8
	)
	port map (
		clk => clk_serial,
		reset => reset,
		seed => prng8_seed,
		load => '0',
		step => '1',
		dout => prng8_dout
	);

prng32_i: prng
	generic map (
		width => 32
	)
	port map (
		clk => clk_serial,
		reset => reset,
		seed => prng32_seed,
		load => '0',
		step => '1',
		dout => prng32_dout
	);

mcc_gen_i: BERTMCCGenerator
	port map (
		clk_serial => clk_serial,
		reset => reset,
		mcc_pattern_break => mcc_pattern_break,
		mcc_pattern_dout => mcc_pattern_dout
	);

-- generate 8b10b output
process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		enc8b10b_pattern <= "10011111000110000011";
	else
		enc8b10b_pattern <= enc8b10b_pattern(0) & enc8b10b_pattern(19 downto 1);
	end if;
end process;

-- select output
process begin
	wait until rising_edge(clk_serial);

	case pattern_select is
		when "00" =>   dout <= prng32_dout(prng32_dout'left);
		when "01" =>   dout <= prng8_dout(prng8_dout'left);
		when "10" =>   dout <= enc8b10b_pattern(0);
		when others => dout <= mcc_pattern_dout;
	end case;
end process;

end architecture;