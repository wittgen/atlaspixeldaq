library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BERTReceiver is
	port (
		-- clocking and reset
		clk_serial : in std_logic;
		reset : in std_logic;

		-- serial data input
		din : in std_logic;
		din_valid : in std_logic;

		-- expected from sender
		expected_data : in std_logic;

		-- sync status
		sync : out std_logic;

		-- bit and error counter
		bitcnt : out std_logic_vector(47 downto 0);
		errcnt : out std_logic_vector(47 downto 0);

		-- debugging
		debug : out std_logic_vector(16 downto 0)
	);
end BERTReceiver;

architecture rtl of BERTReceiver is
	signal lat_sr : std_logic_vector(63 downto 0);
	signal lat_idx : integer range 0 to lat_sr'left := 0;
	signal lat_dout : std_logic := '0';
	signal lat_inccnt : integer range 0 to 63 := 0;

	signal match_cnt : integer range 0 to 127 := 0;

	signal sync_i : std_logic := '0';
	signal bitcnt_i : unsigned(bitcnt'range) := (others => '0');
	signal errcnt_i : unsigned(errcnt'range) := (others => '0');
begin

-- shift register
process begin
	wait until rising_edge(clk_serial);

	lat_sr <= lat_sr(lat_sr'left-1 downto 0) & expected_data;
	lat_dout <= lat_sr(lat_idx);
end process;

-- sync output
sync <= sync_i;

-- counter output
bitcnt <= std_logic_vector(bitcnt_i);
errcnt <= std_logic_vector(errcnt_i);

-- sync state machine
process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		lat_idx <= 0;
		match_cnt <= 0;
		sync_i <= '0';
	else
		if din_valid = '1' then
			-- compare for match generation
			if din = lat_dout then
				if match_cnt /= 127 then
					match_cnt <= match_cnt + 1;
				end if;
			else
				if match_cnt /= 0 then
					match_cnt <= match_cnt - 1;
				end if;			
			end if;

			-- sync generation with hysteresis
			if match_cnt = 127 then
				sync_i <= '1';
			elsif match_cnt = 95 then
				sync_i <= '0';
			end if;

			-- increment lat_idx only once every 32 clock cycles
			if lat_inccnt = 63 then
				lat_inccnt <= 0;
				if (match_cnt < 60) then
					if lat_idx = lat_sr'left then
						lat_idx <= 0;
					else
						lat_idx <= lat_idx + 1;
					end if;
				end if;
			else
				lat_inccnt <= lat_inccnt + 1;
			end if;
		end if;
	end if;
end process;

-- bit error counter
process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		bitcnt_i <= (others => '0');
		errcnt_i <= (others => '0');
	elsif (sync_i = '1') and (din_valid = '1') then
		bitcnt_i <= bitcnt_i + 1;
		if din /= lat_dout then
			errcnt_i <= errcnt_i + 1;
		end if;
	end if;
end process;

-- debugging
process begin
	wait until rising_edge(clk_serial);
	debug(5 downto 0) <= std_logic_vector(to_unsigned(lat_idx, 6));
	debug(6) <= expected_data;
	debug(7) <= din;
	debug(8) <= lat_dout;
	debug(9) <= sync_i;
	debug(16 downto 10) <= std_logic_vector(to_unsigned(match_cnt, 7));
end process;

end architecture;