-- DCS monitoring for FE-I4
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity dcs_monitor is
	port (
		clk : in std_logic;
		reset : in std_logic;
		
		-- front-end data
		din : in std_logic_vector(7 downto 0);
		kchar : in std_logic;
		valid : in std_logic;
		
		-- register interface
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rden : in std_logic;
		csr_wren : in std_logic;
		fifoh_rd : out std_logic_vector(7 downto 0);
		fifoh_wr : in std_logic_vector(7 downto 0);
		fifoh_rden : in std_logic;
		fifoh_wren : in std_logic;
		fifol_rd : out std_logic_vector(7 downto 0);
		fifol_wr : in std_logic_vector(7 downto 0);
		fifol_rden : in std_logic;
		fifol_wren : in std_logic;
		
		-- interrupt
		interrupt : out std_logic
	);
end dcs_monitor;

architecture RTL of dcs_monitor is
	-- data shift register
	type datasr_type is array (0 to 5) of std_logic_vector(7 downto 0);
	signal datasr : datasr_type := (others => (others => '0'));
	
	-- count received bytes
	signal bytecnt : integer range 0 to 6 := 0;
	
	-- data processing strobe
	signal data_process : std_logic := '0';
	
	-- FIFO interface
	signal fifo_din : std_logic_vector(15 downto 0) := (others => '0');
	signal fifo_dout : std_logic_vector(15 downto 0) := (others => '0');
	signal fifo_wren : std_logic := '0';
	signal fifo_rden : std_logic := '0';
	signal fifo_empty : std_logic := '0';
	signal fifo_full : std_logic := '0';
	
	-- control and status bits
	signal enable : std_logic := '1';
	signal collect_all : std_logic := '0';
	signal interrupt_enable : std_logic := '0';
	signal interrupt_flag : std_logic := '0';

	COMPONENT dcs_fifo
	  PORT (
	    clk : IN STD_LOGIC;
	    srst : IN STD_LOGIC;
	    din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
	    wr_en : IN STD_LOGIC;
	    rd_en : IN STD_LOGIC;
	    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
	    full : OUT STD_LOGIC;
	    empty : OUT STD_LOGIC
	  );
	END COMPONENT;
begin

-- FIFO instance
fifo_i: dcs_fifo
	PORT MAP (
		clk => clk,
		srst => reset,
		din => fifo_din,
		wr_en => fifo_wren,
		rd_en => fifo_rden,
		dout => fifo_dout,
		full => fifo_full,
		empty => fifo_empty
	);

-- collect data frames
process begin
	wait until rising_edge(clk);
	
	-- default assignment
	data_process <= '0';
	
	if reset = '1' then
		-- clear data sr
		datasr <= (others => (others => '0'));
		bytecnt <= 0;
	else
		if enable = '1' then
			-- clear data sr on sof detection
			if (valid = '1') and (din = x"FC") and (kchar = '1') then
				datasr <= (others => (others => '0'));
				bytecnt <= 0;
			else
				if (valid = '1') and (kchar = '0') then
					-- shift in data
					for I in 0 to 4 loop
						datasr(I+1) <= datasr(I);
					end loop;
					datasr(0) <= din;
					
					-- count bytes
					if bytecnt = 5 then
						bytecnt <= 0;
						
						-- process strobe
						data_process <= '1';
					else
						bytecnt <= bytecnt + 1;
					end if;
				end if;
			end if;
		end if;
	end if;
end process;

-- process incoming data
process begin
	wait until rising_edge(clk);
	
	-- default assignments
	fifo_wren <= '0';
	
	if data_process = '1' then
		-- compare frame
		if 	(datasr(5) = x"EA") and
			(datasr(4) = x"00") and
			(datasr(3) = x"28") and
			(datasr(2) = x"EC") then
			
			-- should collect all GADC values or only valid ones?
			-- see FE-I4B guide section 4.14 and 7.2.1
			if (collect_all = '1') or (datasr(0)(3) = '1') then
				fifo_din <= datasr(1) & datasr(0);
				fifo_wren <= '1';
			end if;
		end if;
	end if;
end process;

-- process control/status register write
process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		enable <= '0';
		collect_all <= '0';
		interrupt_enable <= '0';
	else
		if csr_wren = '1' then
			enable <= csr_wr(0);
			collect_all <= csr_wr(1);
			interrupt_enable <= csr_wr(2);
		end if;
	end if;
end process;

csr_rd(0) <= enable;
csr_rd(1) <= collect_all;
csr_rd(2) <= interrupt_enable;
csr_rd(3) <= interrupt_flag;
csr_rd(4) <= fifo_full;
csr_rd(5) <= fifo_empty;

-- FIFO readback (low byte first, then high byte)
fifol_rd <= fifo_dout(7 downto 0);
fifoh_rd <= fifo_dout(15 downto 8);
fifo_rden <= fifoh_rden;

-- interrupt generation
interrupt_flag <= not fifo_empty;
interrupt <= interrupt_flag and interrupt_enable;

end architecture;
