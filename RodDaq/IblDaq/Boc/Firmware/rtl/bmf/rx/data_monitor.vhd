-- data monitoring
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity data_monitor is
	port
	(
		clk : in std_logic;
		reset : in std_logic;

		din : in std_logic_vector(7 downto 0);
		din_kchar : in std_logic;
		din_valid : in std_logic;

		frame_error : out std_logic;
		frame_cnt : out std_logic_vector(31 downto 0);
		frame_err_cnt : out std_logic_vector(31 downto 0)
	);
end data_monitor;

architecture rtl of data_monitor is
	type monitor_states is (sof_wait_reset, sof_wait, data0, data1, data2, eof_wait);
	signal Z : monitor_states := sof_wait_reset;

	signal data_frame : std_logic := '0';
	signal empty_frame : std_logic := '1';

	signal frame_err_cnt_int : unsigned(31 downto 0) := (others => '0');
	signal frame_cnt_int : unsigned(31 downto 0) := (others => '0');
begin

frame_cnt <= std_logic_vector(frame_cnt_int);
frame_err_cnt <= std_logic_vector(frame_err_cnt_int);

process begin
	wait until rising_edge(clk);

	if reset = '1' then
		Z <= sof_wait_reset;
		frame_cnt_int <= (others => '0');
		frame_err_cnt_int <= (others => '0');
	else
		frame_error <= '0';

		case Z is
			when sof_wait_reset =>
				empty_frame <= '1';
				data_frame <= '0';
				
				if (din_valid = '1') and (din_kchar = '1') and (din = x"fc") then
					Z <= data0;
				end if;

			when sof_wait =>
				empty_frame <= '1';
				data_frame <= '0';
				
				if (din_valid = '1') then
					if (din_kchar = '0') then
						frame_err_cnt_int <= frame_err_cnt_int + 1;
						frame_error <= '1';
						Z <= sof_wait_reset;
					else
						if (din = x"bc") then
							frame_err_cnt_int <= frame_err_cnt_int + 1;
							frame_error <= '1';
							Z <= sof_wait_reset;
						elsif (din = x"fc") then
							Z <= data0;
						end if;
					end if;
				end if;

			when data0 =>
				if din_valid = '1' then
					if din_kchar = '1' then
						if (din = x"bc") and (empty_frame = '0') then
							frame_cnt_int <= frame_cnt_int + 1;
							Z <= sof_wait;
						else
							frame_err_cnt_int <= frame_err_cnt_int + 1;
							frame_error <= '1';
							Z <= sof_wait_reset;
						end if;
					else
						empty_frame <= '0';
						if data_frame = '0' then
							if (din = x"E9") then
								data_frame <= '1';
								Z <= data1;
							elsif (din = x"EA") or (din = x"EC") or (din = x"EF") then
								Z <= data1;
							else
								frame_err_cnt_int <= frame_err_cnt_int + 1;
								frame_error <= '1';
								Z <= sof_wait_reset;
							end if;
						else
							Z <= data1;
						end if;
					end if;
				end if;

			when data1 =>
				if din_valid = '1' then
					if din_kchar = '1' then
						frame_err_cnt_int <= frame_err_cnt_int + 1;
						frame_error <= '1';
						Z <= sof_wait_reset;
					else
						Z <= data2;
					end if;
				end if;

			when data2 =>
				if din_valid = '1' then
					if din_kchar = '1' then
						frame_err_cnt_int <= frame_err_cnt_int + 1;
						frame_error <= '1';
						Z <= sof_wait_reset;
					else
						Z <= data0;
					end if;
				end if;

			when eof_wait =>
				if din_valid = '1' then
					if din_kchar = '0' then
						Z <= data1;
					else
						if din /= x"bc" then
							frame_err_cnt_int <= frame_err_cnt_int + 1;
							frame_error <= '1';
							Z <= sof_wait_reset;
						else
							frame_cnt_int <= frame_cnt_int + 1;
							Z <= sof_wait;
						end if;
					end if;
				end if;
		end case;
	end if;
end process;

end architecture;
