-- CDR
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.bocpack.all;

entity cdr is
	port
	(
		-- clocks
		clk : in std_logic;
		clk4x : in std_logic;
		clk4x_serdesstrobe : in std_logic;

		-- reset
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- data output
		data : out CDR_record;

		-- train phase alignment
		train_phase : in std_logic
	);
end cdr;

architecture rtl of cdr is
	component cdr_serdes
		port
		(
			-- clocks
			clk : in std_logic;
			clk4x : in std_logic;
			clk4x_serdesstrobe : in std_logic;

			-- reset
			reset : in std_logic;

			-- data input
			din : in std_logic;

			-- data output
			data : out CDR_record
		);
	end component;

	component cdr_slowrx
		port
		(
			-- clocks
			clk : in std_logic;

			-- reset
			reset : in std_logic;

			-- data input
			din : in std_logic;

			-- data output
			data : out CDR_record
		);
	end component;

	component cdr_rce
	    port (
	        clk         : in std_logic ;            -- clock input
	        clk4x       : in std_logic ;            -- fast oversampling clock
	        clk4x_serdesstrobe : in std_logic;
	        rdatain     : in std_logic;             -- data input
	        rst         : in std_logic ;            -- reset input
	        useaout     : out std_logic ;           -- useA output for cascade
	        usebout     : out std_logic ;           -- useB output for cascade
	        usecout     : out std_logic ;           -- useC output for cascade
	        usedout     : out std_logic ;           -- useD output for cascade
	    --  useain      : in std_logic ;            -- useA output for cascade
	    --  usebin      : in std_logic ;            -- useB output for cascade
	    --  usecin      : in std_logic ;            -- useC output for cascade
	    --  usedin      : in std_logic ;            -- useD output for cascade
	        dataout     : out CDR_record ;          -- data output
	        phaseConfig : in std_logic              -- Flag for calibration
	    );
	end component;
begin

serdes_generate: if not SlowRx generate
	serdes_generate: if not UseRceCDR generate
		cdr_serdes_i: cdr_serdes PORT MAP(
			clk => clk,
			clk4x => clk4x,
			clk4x_serdesstrobe => clk4x_serdesstrobe,
			reset => reset,
			din => din,
			data => data
		);
	end generate;
	rce_generate: if UseRceCDR generate
		cdr_rce_i: cdr_rce PORT MAP (
			clk => clk,
			clk4x => clk4x,
			clk4x_serdesstrobe => clk4x_serdesstrobe,
			rdatain => din,
			rst => reset,
			useaout => open,
			usebout => open,
			usecout => open,
			usedout => open,
			dataout => data,
			phaseConfig => train_phase
		);
	end generate;
end generate;

slowrx_generate: if SlowRx generate
	cdr_slowrx_i: cdr_slowrx PORT MAP (
		clk => clk,
		reset => reset,
		din => din,
		data => data
	);
end generate;

end architecture;
