library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity pix_compare is
	port (
		-- clock and reset
		clk_serial : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic_vector(7 downto 0);
		valid : in std_logic;
		trailer : in std_logic;

		-- register interface
		control_rd : out std_logic_vector(7 downto 0);
		control_wr : in std_logic_vector(7 downto 0);
		control_rden : in std_logic;
		control_wren : in std_logic;
		pattern_rd : out std_logic_vector(7 downto 0);
		pattern_wr : in std_logic_vector(7 downto 0);
		pattern_rden : in std_logic;
		pattern_wren : in std_logic;
		mask_rd : out std_logic_vector(7 downto 0);
		mask_wr : in std_logic_vector(7 downto 0);
		mask_rden : in std_logic;
		mask_wren : in std_logic;
		counter_rd : out std_logic_vector(7 downto 0);
		counter_wr : in std_logic_vector(7 downto 0);
		counter_rden : in std_logic;
		counter_wren : in std_logic;

		-- debug
		bitcnt : out std_logic_vector(31 downto 0);
		errcnt : out std_logic_vector(31 downto 0)
	);
end pix_compare;

architecture rtl of pix_compare is
	-- bit and error counters
	signal bitcnt_i : unsigned(31 downto 0) := (others => '0');
	signal errcnt_i : unsigned(31 downto 0) := (others => '0');

	-- read/write pointers
	constant mem_depth : integer := 512;
	type mem_t is array (0 to mem_depth-1) of std_logic_vector(15 downto 0);
	signal mem : mem_t := (others => (others => '0'));
	signal mem_rdptr : integer range 0 to mem_depth-1 := 0;
	signal mem_wrptr : integer range 0 to mem_depth-1 := 0;

	-- store mask
	signal write_mask : std_logic_vector(7 downto 0);

	-- enable compare
	signal enable : std_logic := '0';
	signal reset_wrptr : std_logic := '0';
	signal clear_counters : std_logic := '0';

	-- compare unit
	signal compare_exec : std_logic := '0';
	signal compare_exec2 : std_logic := '0';
	signal compare_diff : std_logic_vector(7 downto 0) := (others => '0'); 
	signal compare_got : std_logic_vector(7 downto 0) := (others => '0');
	signal compare_expected : std_logic_vector(7 downto 0) := (others => '0');
	signal compare_mask : std_logic_vector(7 downto 0) := (others => '0');
	signal compare_mask2 : std_logic_vector(7 downto 0) := (others => '0');

	-- counter readout shift register
	signal counter_sr : std_logic_vector(63 downto 0) := (others => '0');
begin

-- write into memory
process begin
	wait until rising_edge(clk_serial);

	if (reset = '1') or (reset_wrptr = '1') then
		mem_wrptr <= 0;
	else
		if mask_wren = '1' then
			write_mask <= mask_wr;
		end if;

		if (pattern_wren = '1') and (enable = '0') then
			mem(mem_wrptr) <= write_mask & pattern_wr;
			if mem_wrptr = mem_depth-1 then
				mem_wrptr <= 0;
			else
				mem_wrptr <= mem_wrptr + 1;
			end if;
		end if;
	end if;
end process;

-- receive data and compare it
process
	variable diff_bits : integer;
	variable comp_bits : integer;
begin
	wait until rising_edge(clk_serial);

	-- default
	compare_exec <= '0';
	compare_exec2 <= '0';

	-- input data
	if (reset = '1') then
		mem_rdptr <= 0;
		bitcnt_i <= (others => '0');
		errcnt_i <= (others => '0');
	else
		if (valid = '1') and (enable = '1') then
			compare_expected <= mem(mem_rdptr)(7 downto 0);
			compare_mask <= mem(mem_rdptr)(15 downto 8);
			compare_got <= din;
			compare_exec <= '1';
			if mem_rdptr = mem_depth-1 then
				mem_rdptr <= 0;
			else
				mem_rdptr <= mem_rdptr + 1;
			end if;
		end if;

		-- compare (step 1): generate diff
		if compare_exec = '1' then
			compare_diff <= (compare_got and compare_mask) xor (compare_expected and compare_mask);
			compare_mask2 <= compare_mask;
			compare_exec2 <= '1';
		end if;

		-- compare (step 2): count
		if compare_exec2 = '1' then
			diff_bits := 0;
			comp_bits := 0;
			for I in 0 to 7 loop
				if compare_diff(I) = '1' then
					diff_bits := diff_bits + 1;
				end if;
				if compare_mask2(I) = '1' then
					comp_bits := comp_bits + 1;
				end if;
			end loop;
			bitcnt_i <= bitcnt_i + comp_bits;
			errcnt_i <= errcnt_i + diff_bits;
		end if;

		-- reset read pointer if we got a trailer
		if trailer = '1' then
			mem_rdptr <= 0;
		end if;

		-- reset counters
		if clear_counters = '1' then
			bitcnt_i <= (others => '0');
			errcnt_i <= (others => '0');
		end if;
	end if;
end process;

-- control register
process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		enable <= '0';
		clear_counters <= '0';
		reset_wrptr <= '0';
	else
		if control_wren = '1' then
			enable <= control_wr(0);
			clear_counters <= control_wr(1);
			reset_wrptr <= control_wr(2);
		end if;
	end if;
end process;

-- counter readout
process begin
	wait until rising_edge(clk_serial);

	if counter_wren = '1' then
		counter_sr <= std_logic_vector(errcnt_i) & std_logic_vector(bitcnt_i);
	end if;

	if counter_rden = '1' then
		counter_sr <= x"00" & counter_sr(63 downto 8);
	end if;
end process;
counter_rd <= counter_sr(7 downto 0);

-- output counter for debugging reasons
bitcnt <= std_logic_vector(bitcnt_i);
errcnt <= std_logic_vector(errcnt_i);

end architecture;