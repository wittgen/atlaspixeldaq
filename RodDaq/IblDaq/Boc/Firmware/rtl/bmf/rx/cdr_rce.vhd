------------------------------------------------------------------------------
--
--  Xilinx, Inc. 2002                 www.xilinx.com
--
--  XAPP 225
--
------------------------------------------------------------------------------
--
--  File name :       sync_master_v2.vhd
--
--  Description :     Master phase aligner module for Virtex2 (2 bits)
--
--  Date - revision : January 6th 2009 - v 1.2
--
--  Author :          NJS
--
--  Disclaimer: LIMITED WARRANTY AND DISCLAMER. These designs are
--              provided to you "as is". Xilinx and its licensors make and you
--              receive no warranties or conditions, express, implied,
--              statutory or otherwise, and Xilinx specifically disclaims any
--              implied warranties of merchantability, non-infringement,or
--              fitness for a particular purpose. Xilinx does not warrant that
--              the functions contained in these designs will meet your
--              requirements, or that the operation of these designs will be
--              uninterrupted or error free, or that defects in the Designs
--              will be corrected. Furthermore, Xilinx does not warrantor
--              make any representations regarding use or the results of the
--              use of the designs in terms of correctness, accuracy,
--              reliability, or otherwise.
--
--              LIMITATION OF LIABILITY. In no event will Xilinx or its
--              licensors be liable for any loss of data, lost profits,cost
--              or procurement of substitute goods or services, or for any
--              special, incidental, consequential, or indirect d[DX] Dependencies for CalibGui.cc
--              arising from the use or operation of the designs or
--              accompanying documentation, however caused and on any theory
--              of liability. This limitation will apply even if Xilinx
--              has been advised of the possibility of such damage. This
--              limitation shall apply not-withstanding the failure of the
--              essential purpose of any limited remedies herein.
--
--  Copyright © 2002 Xilinx, Inc.
--  All rights reserved
--
------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library unisim ;
use unisim.vcomponents.all ;

library work;
use work.bocpack.all;

entity cdr_rce is
    port (
        clk         : in std_logic ;            -- clock input
        clk4x       : in std_logic ;            -- fast oversampling clock
        clk4x_serdesstrobe : in std_logic;
        rdatain     : in std_logic;             -- data input
        rst         : in std_logic ;            -- reset input
        phaseOut    : out std_logic_vector(1 downto 0) ;
        phaseIn     : in std_logic_vector(1 downto 0) ;
        dataout     : out CDR_record ;          -- data output
        phaseTrain : in std_logic ;            -- Flag for calibration
        phaseOverride : in std_logic;             -- Flag for manual phase override
        countera    : out std_logic_vector(7 downto 0);
        counterb    : out std_logic_vector(7 downto 0);
        counterc    : out std_logic_vector(7 downto 0);
        counterd    : out std_logic_vector(7 downto 0)
    );
end cdr_rce;

architecture arch_syncdata of cdr_rce is

signal aa0          : std_logic;
signal bb0          : std_logic;
signal cc0              : std_logic;
signal dd0              : std_logic;
signal usea         : std_logic;
signal useb         : std_logic;
signal usec         : std_logic;
signal used         : std_logic;
signal useaint      : std_logic;
signal usebint      : std_logic;
signal usecint      : std_logic;
signal usedint      : std_logic;
signal ctrlint      : std_logic_vector(1 downto 0);
signal sdataa       : std_logic;
signal sdatab       : std_logic;
signal sdatac       : std_logic;
signal sdatad       : std_logic;
signal az           : std_logic_vector(2 downto 0) ;
signal bz           : std_logic_vector(2 downto 0) ;
signal cz           : std_logic_vector(2 downto 0) ;
signal dz           : std_logic_vector(2 downto 0) ;
signal aap, bbp     : std_logic;
signal aan, bbn     : std_logic;
signal ccp, ddp     : std_logic;
signal ccn, ddn     : std_logic;
signal pipe_ce0     : std_logic;

-- signal control          : std_logic_vector(35 downto 0) ;


-- FS: Counters to determine most likely phase
signal countera_i         : std_logic_vector(7 downto 0);
signal counterb_i         : std_logic_vector(7 downto 0);
signal counterc_i         : std_logic_vector(7 downto 0);
signal counterd_i         : std_logic_vector(7 downto 0);

signal useatemp         : std_logic;
signal usebtemp         : std_logic;
signal usectemp         : std_logic;
signal usedtemp         : std_logic;

-- signal data             : std_logic_vector(15 downto 0);
-- signal trigger          : std_logic_vector(7 downto 0);
signal count : std_logic;
signal changed : std_logic;

begin

-- output phase
process begin
    wait until rising_edge(clk);

    if useaint = '1' then
        phaseOut <= "00";
    elsif usebint = '1' then
        phaseOut <= "01";
    elsif usecint = '1' then
        phaseOut <= "10";
    else
        phaseOut <= "11";
    end if;
end process;

sdataa <= (aa0 and useaint) ;
sdatab <= (bb0 and usebint) ;
sdatac <= (cc0 and usecint) ;
sdatad <= (dd0 and usedint) ;

saa0 : srl16 port map(d => az(2), clk => clk, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => aa0);
sbb0 : srl16 port map(d => bz(2), clk => clk, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => bb0);
scc0 : srl16 port map(d => cz(2), clk => clk, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => cc0);
sdd0 : srl16 port map(d => dz(2), clk => clk, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => dd0);

process (clk, rst)
begin
if rst = '1' then
    ctrlint <= "10" ;
    useaint <= '0' ; usebint <= '0' ; usecint <= '0' ; usedint <= '0' ;
    usea <= '0' ; useb <= '0' ; usec <= '0' ; used <= '0' ;
        useatemp <= '0';usebtemp <= '0';usectemp <= '0';usedtemp <= '0';
    pipe_ce0 <= '0' ; dataout.value <= "00" ;
    aap <= '0' ; bbp <= '0' ; ccp <= '0' ; ddp <= '0' ;
    aan <= '0' ; bbn <= '0' ; ccn <= '0' ; ddn <= '0' ;
    az(2) <= '0' ; bz(2) <= '0' ; cz(2) <= '0' ; dz(2) <= '0' ;
        countera_i <= "00000000"; counterb_i <= "00000000"; counterc_i <= "00000000"; counterd_i <= "00000000";
        count <= '0';
        changed <= '0';
elsif clk'event and clk = '1' then
    az(2) <= az(1) ; bz(2) <= bz(1) ; cz(2) <= cz(1) ; dz(2) <= dz(1) ;
    aap <= (az(2) xor az(1)) and not az(1) ;    -- find positive edges
    bbp <= (bz(2) xor bz(1)) and not bz(1) ;
    ccp <= (cz(2) xor cz(1)) and not cz(1) ;
    ddp <= (dz(2) xor dz(1)) and not dz(1) ;
    aan <= (az(2) xor az(1)) and az(1) ;        -- find negative edges
    bbn <= (bz(2) xor bz(1)) and bz(1) ;
    ccn <= (cz(2) xor cz(1)) and cz(1) ;
    ddn <= (dz(2) xor dz(1)) and dz(1) ;
    useatemp <= (aap and bbp and not ccp and not ddp) or
                (aan and bbn and not ccn and not ddn);
    usebtemp <= (aap and bbp and ccp and not ddp) or
                (aan and bbn and ccn and not ddn);
    usectemp <= (aap and bbp and ccp and ddp) or
                (aan and bbn and ccn and ddn);
    usedtemp <= (aap and not bbp and not ccp and not ddp) or
                (aan and not bbn and not ccn and not ddn);

      if phaseOverride = '0' then
          if (phaseTrain = '1') and (count = '0') then
              countera_i <= "00000000";        -- FS: Reset all counters if one of them has reached the end
              counterb_i <= "00000000";
              counterc_i <= "00000000";
              counterd_i <= "00000000";
              count <= '1';
      else
        if count = '1' then
          -- histogram valid phases
          if (useatemp='1') and (countera_i/=x"FF") then                -- FS: Only one of the usex equal 1
            countera_i <= countera_i+1;
          elsif (usebtemp='1') and (counterb_i/=x"FF") then
            counterb_i <= counterb_i+1;
          elsif (usectemp='1') and (counterc_i/=x"FF") then
            counterc_i <= counterc_i+1;
          elsif (usedtemp='1') and (counterd_i/=x"FF") then
            counterd_i <= counterd_i+1;
          end if;

          -- decide which phase to take
          if countera_i = "11111111" then    -- FS: . . . and set the one reaching the end of the counter
                usea <= '1';
                useb <= '0';
                usec <= '0';
                used <= '0';
                changed <= '1';
                count <= '0';
                
          elsif counterb_i = "11111111" then
                usea <= '0';
                useb <= '1';
                usec <= '0';
                used <= '0';
                changed <= '1';
                count <= '0';
                
          elsif counterc_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '1';
                used <= '0';
                changed <= '1';
                count <= '0';

          elsif counterd_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '0';
                used <= '1';
                changed <= '1';
                count <= '0';
          end if;
        else
          if changed  = '1' then
               changed <= '0';
               pipe_ce0 <= '1' ;
               useaint <= usea ;
               usebint <= useb ;
               usecint <= usec ;
               usedint <= used ; 
          end if ;
        end if;
      end if;
    else
      case phaseIn is
          when "00" =>
              usea <= '1';
              useb <= '0';
              usec <= '0';
              used <= '0';
          when "01" =>
              usea <= '0';
              useb <= '1';
              usec <= '0';
              used <= '0';
          when "10" =>
              usea <= '0';
              useb <= '0';
              usec <= '1';
              used <= '0';
          when "11" =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '1';
          when others =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '1';
      end case;

      pipe_ce0 <= '1' ;
      useaint <= usea ;
      usebint <= useb ;
      usecint <= usec ;
      usedint <= used ;                
    end if ;

    if pipe_ce0 = '1' then
        dataout.value(0) <= sdataa or sdatab or sdatac or sdatad;
        dataout.value(1) <= '0';
    end if ;
    if usedint = '1' and usea = '1' then        -- 'd' going to 'a'
        ctrlint <= ctrlint - 1 ;
    elsif useaint = '1' and used = '1' then     -- 'a' going to 'd'
        ctrlint <= ctrlint + 1 ;
    end if ;
end if ;
end process ;

dataout.valid <= "01" when pipe_ce0 = '1' else "00";
dataout.lock <= pipe_ce0;

-- get all the samples into the same time domain (using SERDES)
serdes: ISERDES2
        generic map (
            BITSLIP_ENABLE => FALSE,        -- Enable Bitslip Functionality (TRUE/FALSE)
            DATA_RATE => "SDR",             -- Data-rate ("SDR" or "DDR")
            DATA_WIDTH => 4,                -- Parallel data width selection (2-8)
            INTERFACE_TYPE => "RETIMED",    -- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED" 
            SERDES_MODE => "NONE"           -- "NONE", "MASTER" or "SLAVE" 
        )
        port map (
            CFB0 => open,           -- 1-bit output: Clock feed-through route output
            CFB1 => open,           -- 1-bit output: Clock feed-through route output
            DFB => open,            -- 1-bit output: Feed-through clock output
            FABRICOUT => open,  -- 1-bit output: Unsynchrnonized data output
            INCDEC => open,         -- 1-bit output: Phase detector output
            -- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
            Q1 => AZ(0),
            Q2 => BZ(0),
            Q3 => CZ(0),
            Q4 => DZ(0),
            SHIFTOUT => open,       -- 1-bit output: Cascade output signal for master/slave I/O
            VALID => open,          -- 1-bit output: Output status of the phase detector
            BITSLIP => '0',         -- 1-bit input: Bitslip enable input
            CE0 => '1',             -- 1-bit input: Clock enable input
            CLK0 => clk4x ,         -- 1-bit input: I/O clock network input
            CLK1 => '0',            -- 1-bit input: Secondary I/O clock network input
            CLKDIV => clk,       -- 1-bit input: FPGA logic domain clock input
            D => rdatain,           -- 1-bit input: Input data
            IOCE => clk4x_serdesstrobe,            -- 1-bit input: Data strobe input
            RST => rst,             -- 1-bit input: Asynchronous reset input
            SHIFTIN => '0'          -- 1-bit input: Cascade input signal for master/slave I/O
        );

AZ(1) <= AZ(0) when rising_edge(clk);
BZ(1) <= BZ(0) when rising_edge(clk);
CZ(1) <= CZ(0) when rising_edge(clk);
DZ(1) <= DZ(0) when rising_edge(clk);

countera <= countera_i;
counterb <= counterb_i;
counterc <= counterc_i;
counterd <= counterd_i;

end arch_syncdata;
