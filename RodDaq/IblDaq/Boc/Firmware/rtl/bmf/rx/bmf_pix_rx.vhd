-- RX path unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.all;

entity bmf_pix_rx is
	generic (
		DEFAULT_MASTER : integer := 0;
		DEFAULT_SLAVE : integer := 0;
		DEFAULT_XC : integer := 0
	);
	port (
		-- clocks
		clk_regbank : IN std_logic;
		clk_serial : IN std_logic;

		-- reset input
		reset : IN std_logic;
		
		-- data inputs
		din : in pix_align_data_t;

		-- data output
		dout : out std_logic_vector(3 downto 0);

		-- speed selection (00/11: 40, 01: 80, 10: 160)
		speed : in std_logic_vector(1 downto 0);

		-- XC lines (to the emulator)
		xc_vec : in std_logic_vector(15 downto 0);

		-- Registers
		reg_wr : IN RegBank_wr_t(0 to 31);
		reg_rd : OUT RegBank_rd_t(0 to 31);
		reg_wren : IN RegBank_wren_t(0 to 31);
		reg_rden : IN RegBank_rden_t(0 to 31);

		-- broadcast
		bcast_wr : in RegBank_wr_t(0 to 31);
		bcast_wren : in RegBank_wren_t(0 to 31);
		bcast_enable : in std_logic;

		-- debugging
		debug_fast : out std_logic_vector(50 downto 0);
		debug_slow : out std_logic_vector(24 downto 0)
	);
end bmf_pix_rx;

architecture rtl of bmf_pix_rx is
	component pix_monitor_fifo
	  PORT (
	    rst : IN STD_LOGIC;
	    wr_clk : IN STD_LOGIC;
	    rd_clk : IN STD_LOGIC;
	    din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
	    wr_en : IN STD_LOGIC;
	    rd_en : IN STD_LOGIC;
	    dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
	    full : OUT STD_LOGIC;
	    empty : OUT STD_LOGIC
	  );
	end component;

	component pixel_simulator
	  port(
	    clk_in         : in  std_logic; -- 40 MHz clock
	    rst_n_in       : in  std_logic; -- Powerup global reset
	    config_in_1    : in  std_logic_vector( 31 DOWNTO 0); -- configuration word for the simulator
	    config_in_2    : in  std_logic_vector( 31 DOWNTO 0); -- configuration word for the simulator
	    ser_data_in    : in  std_logic;
	    inlinks        : in  std_logic_vector(3 DOWNTO 0); -- Real Input links
	    outlinks       : out std_logic_vector(3 DOWNTO 0); -- output for real or encoded data
	    debug          : out std_logic_vector(50 DOWNTO 0) -- some debugging information
	    );
	end component;

	component pix_compare
		port (
			-- clock and reset
			clk_serial : in std_logic;
			reset : in std_logic;

			-- data input
			din : in std_logic_vector(7 downto 0);
			valid : in std_logic;
			trailer : in std_logic;

			-- register interface
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			pattern_rd : out std_logic_vector(7 downto 0);
			pattern_wr : in std_logic_vector(7 downto 0);
			pattern_rden : in std_logic;
			pattern_wren : in std_logic;
			mask_rd : out std_logic_vector(7 downto 0);
			mask_wr : in std_logic_vector(7 downto 0);
			mask_rden : in std_logic;
			mask_wren : in std_logic;
			counter_rd : out std_logic_vector(7 downto 0);
			counter_wr : in std_logic_vector(7 downto 0);
			counter_rden : in std_logic;
			counter_wren : in std_logic;

			-- debug
			bitcnt : out std_logic_vector(31 downto 0);
			errcnt : out std_logic_vector(31 downto 0)
		);
	end component;

	-- reset distribution
	signal reset_n : std_logic := '0';
	signal rxreset : std_logic := '1';
	signal rxreset_fast : std_logic := '1';

	-- select master and slave fibres
	signal select_master : integer range 0 to 23 := DEFAULT_MASTER;
	signal select_slave : integer range 0 to 23 := DEFAULT_SLAVE;
	signal din_master : std_logic_vector(1 downto 0) := "00";
	signal din_slave : std_logic_vector(1 downto 0) := "00";

	-- monitoring FIFO
	signal mon_fifo_din : std_logic_vector(7 downto 0) := (others => '0');
	signal mon_fifo_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal mon_fifo_wren : std_logic := '0';
	signal mon_fifo_rden : std_logic := '0';
	signal mon_fifo_full : std_logic := '0';
	signal mon_fifo_empty : std_logic := '0';

	-- monitoring
	signal mon_sr : std_logic_vector(7 downto 0) := (others => '0');
	signal mon_srcnt : integer range 0 to 7 := 0;
	signal mon_found_header : std_logic := '0';
	type mon_fsm is (st_idle, st_data);
	signal mon_Z : mon_fsm := st_idle;

	-- RX control
	signal rx_enable : std_logic := '0';
	signal mon_enable : std_logic := '0';
	signal mon_raw : std_logic := '0';
	signal mon_slow : std_logic := '0';
	signal rod_enable : std_logic := '0';
	signal rx_invert : std_logic := '1';

	-- trailer detection
	signal mon_found_trailer : std_logic := '0';
	signal mon_trailer_cnt : integer range 0 to 65535 := 0;
	signal mon_trailer_length : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(22, 16));

	-- occupancy monitoring
	signal occmon_cnt : integer range 0 to 65535 := 0;
	signal occmon_occcnt : integer range 0 to 65535 := 0;
	constant occmon_tau_max : integer := 256;
	constant occmon_tau : integer range 0 to occmon_tau_max := 4;
	signal occmon_meanpipe_din : integer range 0 to 65535 := 0;
	signal occmon_meanpipe_enable : std_logic := '0';
	signal occmon_meanpipe_enable2 : std_logic := '0';
	signal occmon_meanpipe_tmp1 : integer range 0 to occmon_tau_max*65535 := 0;
	signal occmon_meanpipe_tmp2 : integer range 0 to occmon_tau_max*65535 := 0;
	signal occmon_liveoccupancy : std_logic_vector(15 downto 0) := (others => '0');
	signal occmon_meanoccupancy : std_logic_vector(15 downto 0) := (others => '0');

	-- activity monitoring
	constant activity_period : integer := 2000000;				-- activity monitoring per second
	signal activity_cnt : integer range 0 to activity_period-1 := 0;
	signal activity : std_logic := '0';
	signal activity_oldoutlink : std_logic;

	-- check for stuck lines
	signal stuck : std_logic := '1';
	signal stuck_reset : std_logic := '0';

	-- emulator
	signal emu_config1 : std_logic_vector(31 downto 0) := (others => '0');
	signal emu_config2 : std_logic_vector(31 downto 0) := (others => '0');
	signal emu_inlinks : std_logic_vector(3 downto 0) := "0000";
	signal emu_outlinks : std_logic_vector(3 downto 0) := "0000";

	-- compare register interface
	signal comp_control_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_control_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_control_rden : std_logic := '0';
	signal comp_control_wren : std_logic := '0';
	signal comp_pattern_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_pattern_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_pattern_rden : std_logic := '0';
	signal comp_pattern_wren : std_logic := '0';
	signal comp_mask_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_mask_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_mask_rden : std_logic := '0';
	signal comp_mask_wren : std_logic := '0';
	signal comp_counter_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_counter_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal comp_counter_rden : std_logic := '0';
	signal comp_counter_wren : std_logic := '0';

	-- compare unit
	signal compare_din : std_logic_vector(7 downto 0) := (others => '0');
	signal compare_valid : std_logic := '0';

	-- frame counter
	signal framecnt : unsigned(31 downto 0) := (others => '0');
	signal framecnt_sr : std_logic_vector(31 downto 0) := (others => '0');

	-- select serial data lines for emulator
	signal xc_sel : integer range 0 to 15 := DEFAULT_XC;
	signal xc : std_logic := '0';
begin

-- monitoring FIFO
pix_monitor_fifo_i: pix_monitor_fifo
	port map (
		rst => reset,
		wr_clk => clk_serial,
		rd_clk => clk_regbank,
		din => mon_fifo_din,
		dout => mon_fifo_dout,
		wr_en => mon_fifo_wren,
		rd_en => mon_fifo_rden,
		full => mon_fifo_full,
		empty => mon_fifo_empty
	);

-- generate path reset if rx channel is not enabled
rxreset <= (reset or (not rx_enable)) when rising_edge(clk_regbank);
rxreset_fast <= rxreset when rising_edge(clk_serial);
reset_n <= not reset;

-- data input selection and inversion
process begin
	wait until rising_edge(clk_serial);

	if rx_invert = '1' then
		din_master <= not din(select_master);
		din_slave <= not din(select_slave);
	else
		din_master <= din(select_master);
		din_slave <= din(select_slave);
	end if;
end process;

-- select input for serial data
xc <= xc_vec(xc_sel);

-- MCC/FE-I3 simulator
emu: pixel_simulator
	port map (
		clk_in => clk_serial,
		rst_n_in => reset_n,
		config_in_1 => emu_config1,
		config_in_2 => emu_config2,
		ser_data_in => xc,
		inlinks => emu_inlinks,
		outlinks => emu_outlinks,
		debug => debug_fast
	);

-- comparator for fast opto scans
compare_i: pix_compare
	port map (
		clk_serial => clk_serial,
		reset => rxreset,
		din => compare_din,
		valid => compare_valid,
		trailer => mon_found_trailer,
		control_rd => comp_control_rd,
		control_wr => comp_control_wr,
		control_rden => comp_control_rden,
		control_wren => comp_control_wren,
		pattern_rd => comp_pattern_rd,
		pattern_wr => comp_pattern_wr,
		pattern_rden => comp_pattern_rden,
		pattern_wren => comp_pattern_wren,
		mask_rd => comp_mask_rd,
		mask_wr => comp_mask_wr,
		mask_rden => comp_mask_rden,
		mask_wren => comp_mask_wren,
		counter_rd => comp_counter_rd,
		counter_wr => comp_counter_wr,
		counter_rden => comp_counter_rden,
		counter_wren => comp_counter_wren,
		bitcnt => open,
		errcnt => open
	);

reg_rd(8) <= comp_control_rd;
comp_control_rden <= reg_rden(8);
comp_control_wr <= bcast_wr(8) when (bcast_enable = '1' and bcast_wren(8) = '1') else reg_wr(8);
comp_control_wren <= reg_wren(8) or (bcast_enable and bcast_wren(8));
reg_rd(9) <= comp_pattern_rd;
comp_pattern_rden <= reg_rden(9);
comp_pattern_wr <= bcast_wr(9) when (bcast_enable = '1' and bcast_wren(9) = '1') else reg_wr(9);
comp_pattern_wren <= reg_wren(9) or (bcast_enable and bcast_wren(9));
reg_rd(10) <= comp_mask_rd;
comp_mask_rden <= reg_rden(10);
comp_mask_wr <= bcast_wr(10) when (bcast_enable = '1' and bcast_wren(10) = '1') else reg_wr(10);
comp_mask_wren <= reg_wren(10) or (bcast_enable and bcast_wren(10));
reg_rd(11) <= comp_counter_rd;
comp_counter_rden <= reg_rden(11);
comp_counter_wr <= bcast_wr(11) when (bcast_enable = '1' and bcast_wren(11) = '1') else reg_wr(11);
comp_counter_wren <= reg_wren(11) or (bcast_enable and bcast_wren(11));



-- map inlinks (with interleaving if needed)
process (din_master, din_slave, speed)
begin
	case speed is
		when "00" | "11" =>
			emu_inlinks <= "000" & din_master(0);

		when "01" =>
			emu_inlinks <= "00" & din_master(1) & din_master(0);

		when "10" =>
			emu_inlinks <= din_master(1) & din_slave(1) & din_master(0) & din_slave(0);

		when others =>
			emu_inlinks <= "0000";
	end case;
end process;

-- simple control register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		rx_enable <= '0';
		mon_enable <= '0';
		mon_raw <= '0';
		mon_slow <= '0';
		rod_enable <= '0';
	else
		if reg_wren(0) = '1' then
			rx_enable <= reg_wr(0)(0);
			rod_enable <= reg_wr(0)(1);
			mon_enable <= reg_wr(0)(2);
			mon_raw <= reg_wr(0)(3);
			mon_slow <= reg_wr(0)(4);
		end if;
		if (bcast_enable = '1') and (bcast_wren(0) = '1') then
			rx_enable <= bcast_wr(0)(0);
			rod_enable <= bcast_wr(0)(1);
			mon_enable <= bcast_wr(0)(2);
			mon_raw <= bcast_wr(0)(3);
			mon_slow <= bcast_wr(0)(4);
		end if;
	end if;
end process;
reg_rd(0) <= "000" & mon_slow & mon_raw & mon_enable & rod_enable & rx_enable;

-- second control register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		rx_invert <= '1';
	else
		if reg_wren(15) = '1' then
			rx_invert <= reg_wr(15)(6);
		end if;
		if (bcast_enable = '1') and (bcast_wren(15) = '1') then
			rx_invert <= bcast_wr(15)(6);
		end if;
	end if;
end process;
reg_rd(15) <= "0" & rx_invert & "000000";

-- status register
reg_rd(1) <= "00" & mon_fifo_empty & mon_fifo_full & "00" & stuck & activity;
stuck_reset <= reg_wren(1) and reg_wr(1)(1);

-- readback FIFO
reg_rd(2) <= (others => '0');
reg_rd(3) <= mon_fifo_dout;
mon_fifo_rden <= reg_rden(3);

-- select master/slave registers
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		select_master <= DEFAULT_MASTER;
		select_slave <= DEFAULT_SLAVE;
	else
		if reg_wren(4) = '1' then
			select_master <= to_integer(unsigned(reg_wr(4)(4 downto 0)));
		end if;
		if (bcast_enable = '1') and (bcast_wren(4) = '1') then
			select_master <= to_integer(unsigned(bcast_wr(4)(4 downto 0)));
		end if;

		if reg_wren(5) = '1' then
			select_slave <= to_integer(unsigned(reg_wr(5)(4 downto 0)));
		end if;
		if (bcast_enable = '1') and (bcast_wren(5) = '1') then
			select_slave <= to_integer(unsigned(bcast_wr(5)(4 downto 0)));
		end if;
	end if;
end process;
reg_rd(4) <= "000" & std_logic_vector(to_unsigned(select_master, 5));
reg_rd(5) <= "000" & std_logic_vector(to_unsigned(select_slave, 5));

-- set trailer length
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		mon_trailer_length <= std_logic_vector(to_unsigned(22, 16));
	else
		if reg_wren(6) = '1' then
			mon_trailer_length(7 downto 0) <= reg_wr(6);
		end if;
		if reg_wren(7) = '1' then
			mon_trailer_length(15 downto 8) <= reg_wr(7);
		end if;
		if (bcast_enable = '1') and (bcast_wren(6) = '1') then
			mon_trailer_length(7 downto 0) <= bcast_wr(6);			
		end if;
		if (bcast_enable = '1') and (bcast_wren(7) = '1') then
			mon_trailer_length(15 downto 8) <= bcast_wr(7);			
		end if;
	end if;
end process;
reg_rd(6) <= mon_trailer_length(7 downto 0);
reg_rd(7) <= mon_trailer_length(15 downto 8);

-- emulator configuration registers
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		emu_config1 <= (others => '0');
		emu_config2 <= (others => '0');
	else
		for I in 0 to 3 loop
			if reg_wren(16+I) = '1' then
				emu_config1(8*I+7 downto 8*I) <= reg_wr(16+I);
			end if;
			if (bcast_enable = '1') and (bcast_wren(16+I) = '1') then
				emu_config1(8*I+7 downto 8*I) <= bcast_wr(16+I);
			end if;
			reg_rd(16+I) <= emu_config1(8*I+7 downto 8*I);

			if reg_wren(20+I) = '1' then
				emu_config2(8*I+7 downto 8*I) <= reg_wr(20+I);
			end if;
			if (bcast_enable = '1') and (bcast_wren(20+I) = '1') then
				emu_config2(8*I+7 downto 8*I) <= bcast_wr(20+I);
			end if;
			reg_rd(20+I) <= emu_config2(8*I+7 downto 8*I);
		end loop;
	end if;

	-- overwrite speed setting with global BMF speed setting
	emu_config1(2 downto 1) <= speed;
end process;

-- XC selection register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		xc_sel <= DEFAULT_XC;
	else
		if reg_wren(14) = '1' then
			xc_sel <= to_integer(unsigned(reg_wr(14)(3 downto 0)));
		end if;
		if (bcast_enable = '1') and (bcast_wren(14) = '1') then
			xc_sel <= to_integer(unsigned(bcast_wr(14)(3 downto 0)));
		end if;
	end if;
end process;
reg_rd(14) <= "0000" & std_logic_vector(to_unsigned(xc_sel, 4));

-- forward incoming data to the ROD
process begin
	wait until rising_edge(clk_serial);

	if (rod_enable = '0') or (rx_enable = '0') then
		dout <= "0000";
	else
		case speed is
			when "00" | "11" => dout <= "000" & emu_outlinks(0);
			when "01" => dout <= "00" & emu_outlinks(1 downto 0);
			when others => dout <= emu_outlinks;
		end case;
	end if;
end process;

-- monitoring
process begin
	wait until rising_edge(clk_serial);

	if rxreset = '1' then
		mon_fifo_wren <= '0';
		mon_fifo_din <= (others => '0');
		mon_srcnt <= 0;
	else
		-- input data at different rates into the shift register
		if (speed = "00") or (speed = "11") or (mon_slow = '1') then
			mon_sr <= mon_sr(mon_sr'high-1 downto 0) & emu_outlinks(0);
			if mon_srcnt = 7 then
				mon_srcnt <= 0;
			else
				mon_srcnt <= mon_srcnt + 1;
			end if;
		elsif speed = "01" then
			mon_sr <= mon_sr(mon_sr'high-2 downto 0) & emu_outlinks(0) & emu_outlinks(1);
			if mon_srcnt = 6 then
				mon_srcnt <= 0;
			else
				mon_srcnt <= mon_srcnt + 2;
			end if;
		elsif speed = "10" then
			mon_sr <= mon_sr(mon_sr'high-4 downto 0) & emu_outlinks(0) & emu_outlinks(1) & emu_outlinks(2) & emu_outlinks(3);
			if mon_srcnt = 4 then
				mon_srcnt <= 0;
			else
				mon_srcnt <= mon_srcnt + 4;
			end if;
		end if;

		-- output to FIFO
		if (mon_srcnt = 0) and (mon_Z = st_data) then
			mon_fifo_din <= mon_sr(7 downto 0);
			mon_fifo_wren <= (not mon_fifo_full) and mon_enable;
			compare_din <= mon_sr(7 downto 0);
			compare_valid <= '1';
		else
			mon_fifo_wren <= '0';
			compare_valid <= '0';
		end if;

		-- output to FIFO in case of a header word
		if (mon_found_header = '1') and (mon_Z = st_idle) then
			mon_fifo_din <= mon_sr(7 downto 0);
			mon_fifo_wren <= not mon_fifo_full and mon_enable;
			compare_din <= mon_sr(7 downto 0);
			compare_valid <= '1';
			if (speed = "00") or (speed = "11") then
				mon_srcnt <= 1;
			elsif speed = "01" then
				mon_srcnt <= 2;
			elsif speed = "10" then
				mon_srcnt <= 4;
			end if;
		end if;
	end if;
end process;

-- detect header
process (mon_sr, mon_raw, speed) begin
	-- default
	mon_found_header <= '0';

	-- check for header
	if mon_raw = '0'  then
		if mon_sr(7 downto 3) = "11101" then
			mon_found_header <= '1';
		end if;
	else
		case speed is
			when "00" | "11" =>
				if mon_sr(7) /= '0' then
					mon_found_header <= '1';
				end if;

			when "01" =>
				if mon_sr(7 downto 6) /= "00" then
					mon_found_header <= '1';
				end if;

			when "10" =>
				if mon_sr(7 downto 4) /= "0000" then
					mon_found_header <= '1';
				end if;

			when others =>
				null;
		end case;
	end if;
end process;

-- detect trailer
process begin
	wait until rising_edge(clk_serial);

	if rxreset = '1' then
		mon_trailer_cnt <= 0;
	else
		if (speed = "00") or (speed = "11") or (mon_slow = '1') then
			if emu_outlinks(0) = '1' then
				mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length));
			else
				if mon_trailer_cnt > 0 then
                                        mon_trailer_cnt <= mon_trailer_cnt - 1;
                                end if;
			end if;
		elsif (speed = "01") then
			if emu_outlinks(1) = '1' then
				mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length));
			elsif emu_outlinks(0) = '1' then
				mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length)) - 1;
			else
				if mon_trailer_cnt > 1 then
					mon_trailer_cnt <= mon_trailer_cnt - 2;
				else
					mon_trailer_cnt <= 0;
				end if;
			end if;
		elsif (speed = "10") then
			if emu_outlinks(3) = '1' then
                                mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length));
			elsif emu_outlinks(2) = '1' then
                                mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length)) - 1;
			elsif emu_outlinks(1) = '1' then
                                mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length)) - 2;
                        elsif emu_outlinks(0) = '1' then
                                mon_trailer_cnt <= to_integer(unsigned(mon_trailer_length)) - 3;
                        else
                                if mon_trailer_cnt > 3 then
                                        mon_trailer_cnt <= mon_trailer_cnt - 4;
                                else
                                        mon_trailer_cnt <= 0;
                                end if;
                        end if;
		end if;
	end if;
end process;
mon_found_trailer <= '1' when mon_trailer_cnt = 0 else '0';

-- state machine for monitoring
-- and frame counting
process begin
	wait until rising_edge(clk_serial);

	if rxreset = '1' then
		framecnt <= (others => '0');
		mon_Z <= st_idle;
	else
		case mon_Z is
			when st_idle =>
				if mon_found_header = '1' then
					framecnt <= framecnt + 1;
					mon_Z <= st_data;
				end if;

			when st_data =>
				if mon_found_trailer = '1' then
					mon_Z <= st_idle;
				end if;
		end case;
	end if;
end process;

-- frame counter readout
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		framecnt_sr <= (others => '0');
	else
		if (reg_wren(12) = '1') or ((bcast_enable = '1') and (bcast_wren(12) = '1')) then
			framecnt_sr <= std_logic_vector(framecnt);
		end if;

		if reg_rden(12) = '1' then
			framecnt_sr <= x"00" & framecnt_sr(31 downto 8);
		end if;
	end if;
end process;
reg_rd(12) <= framecnt_sr(7 downto 0);

-- occupancy monitoring
process begin
	wait until rising_edge(clk_serial);

	occmon_meanpipe_enable <= '0';
	occmon_meanpipe_enable2 <= '0';

	if reset = '1' then
		occmon_cnt <= 0;
		occmon_occcnt <= 0;
		occmon_liveoccupancy <= (others => '0');
	else
		-- live monitoring
		if occmon_cnt = 65535 then
			-- copy result
			if mon_Z = st_data then
				occmon_liveoccupancy <= std_logic_vector(to_unsigned(occmon_occcnt+1, 16));
				occmon_meanpipe_din <= occmon_occcnt+1;
			else
				occmon_liveoccupancy <= std_logic_vector(to_unsigned(occmon_occcnt, 16));
				occmon_meanpipe_din <= occmon_occcnt;
			end if;

			-- enable mean calc
			occmon_meanpipe_enable <= '1';

			-- reset internal counters
			occmon_cnt <= 0;
			occmon_occcnt <= 0;
		else
			occmon_cnt <= occmon_cnt + 1;

			if mon_Z = st_data then
				occmon_occcnt <= occmon_occcnt + 1;
			end if;
		end if;

		-- mean calculation (step 1)
		if occmon_meanpipe_enable = '1' then
			occmon_meanpipe_tmp1 <= (occmon_tau_max-occmon_tau) * to_integer(unsigned(occmon_meanoccupancy));
			occmon_meanpipe_tmp2 <= occmon_tau * occmon_meanpipe_din;
			occmon_meanpipe_enable2 <= '1';
		end if;

		-- mean calculation (step 2)
		if occmon_meanpipe_enable2 = '1' then
			occmon_meanoccupancy <= std_logic_vector(to_unsigned((occmon_meanpipe_tmp1 + occmon_meanpipe_tmp2)/occmon_tau_max, 16));
		end if;
	end if;
end process;

-- readout of occupancy
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		reg_rd(25) <= (others => '0');
		reg_rd(27) <= (others => '0');
	else
		if reg_rden(24) = '1' then
			reg_rd(25) <= occmon_meanoccupancy(15 downto 8);
		end if;

		if reg_rden(26) = '1' then
			reg_rd(27) <= occmon_liveoccupancy(15 downto 8);
		end if;
	end if;
end process;
reg_rd(24) <= occmon_meanoccupancy(7 downto 0);
reg_rd(26) <= occmon_liveoccupancy(7 downto 0);

-- check if data line is stuck
process begin
	wait until rising_edge(clk_serial);

	if (rxreset = '1') or (stuck_reset = '1') then
		stuck <= '1';
	else
		case speed is
			when "00" | "11" =>
				if emu_outlinks(0) /= '1' then
					stuck <= '0';
				end if;

			when "01" =>
				if emu_outlinks(1 downto 0) /= "11" then
					stuck <= '0';
				end if;

			when "10" =>
				if emu_outlinks /= "1111" then
					stuck <= '0';
				end if;

			when others =>
				null;
		end case;
	end if;
end process;

-- activity monitoring
process begin
	wait until rising_edge(clk_serial);

	if rxreset = '1' then
		activity_cnt <= 0;
		activity <= '0';
		activity_oldoutlink <= '0';
	else
		-- count down
		if activity_cnt > 0 then
			activity_cnt <= activity_cnt - 1;
			activity <= '1';
		else
			activity <= '0';
		end if;

		case speed is
			when "00" | "11" =>
				activity_oldoutlink <= emu_outlinks(0);
				if activity_oldoutlink /= emu_outlinks(0) then
					activity_cnt <= activity_period-1;
				end if;

			when "01" =>
				activity_oldoutlink <= emu_outlinks(1);
				if ((emu_outlinks(1) /= emu_outlinks(0)) or
					(emu_outlinks(0) /= activity_oldoutlink)) then
					activity_cnt <= activity_period-1;
				end if;

			when "10" =>
				activity_oldoutlink <= emu_outlinks(3);
				if ((emu_outlinks(3) /= emu_outlinks(2)) or
					(emu_outlinks(2) /= emu_outlinks(1)) or
					(emu_outlinks(1) /= emu_outlinks(0)) or
					(emu_outlinks(0) /= activity_oldoutlink)) then
					activity_cnt <= activity_period-1;
				end if;

			when others =>
				null;
		end case;
	end if;
end process;

-- debugging signals
debug_slow(3 downto 0) <= emu_outlinks;
debug_slow(5 downto 4) <= speed;
debug_slow(6) <= mon_slow;
debug_slow(7) <= '1' when mon_Z = st_data else '0';
debug_slow(15 downto 8) <= mon_fifo_din;
debug_slow(16) <= mon_fifo_wren;
debug_slow(17) <= mon_found_header;
debug_slow(18) <= mon_found_trailer;
debug_slow(21 downto 19) <= std_logic_vector(to_unsigned(mon_srcnt, 3));
debug_slow(22) <= xc;
debug_slow(24 downto 23) <= "00";

end architecture;
