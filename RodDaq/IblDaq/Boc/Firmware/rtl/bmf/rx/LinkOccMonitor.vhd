library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity LinkOccMonitor is
	port (
		-- clocking and reset
		clk : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic_vector(7 downto 0);
		kchar : in std_logic;
		valid : in std_logic;

		-- occupancy output (16 bits)
		live_occ : out std_logic_vector(15 downto 0);
		mean_occ : out std_logic_vector(15 downto 0)
	);
end LinkOccMonitor;

architecture rtl of LinkOccMonitor is

signal cnt : integer range 0 to 65535 := 0;
signal occ_i : integer range 0 to 65535 := 0;
signal mean_occ_i : integer range 0 to 65535 := 0;

-- time constant for mean calculation
constant tau_max : integer := 256;
constant tau : integer range 0 to tau_max := 4;
signal meanpipe_din : integer range 0 to 65535 := 0;
signal meanpipe_enable : std_logic := '0';
signal meanpipe_enable2 : std_logic := '0';
signal meanpipe_tmp1 : integer range 0 to tau_max*65535 := 0;
signal meanpipe_tmp2 : integer range 0 to tau_max*65535 := 0;

begin

-- counting
process begin
	wait until rising_edge(clk);

	-- default assignments
	meanpipe_enable <= '0';

	if reset = '1' then
		cnt <= 0;
		occ_i <= 0;
		live_occ <= (others => '0');
	else
		if (valid = '1') then
			-- count up
			if cnt = 65535 then
				cnt <= 0;

				-- copy result
				if ((kchar = '1') and (din = x"3C")) or (occ_i = 65535) then
					live_occ <= std_logic_vector(to_unsigned(occ_i, 16));
					meanpipe_din <= occ_i;
				else
					live_occ <= std_logic_vector(to_unsigned(occ_i + 1, 16));
					meanpipe_din <= occ_i + 1;
				end if;

				-- enable mean pipe
				meanpipe_enable <= '1';

				-- reset internal occupancy counter
				occ_i <= 0;
			else
				cnt <= cnt + 1;

				if not(kchar = '1' and din = x"3C") then
					occ_i <= occ_i + 1;
				end if;
			end if;
		end if;
	end if;
end process;

-- mean pipeline
process begin
	wait until rising_edge(clk);

	-- default assignment
	meanpipe_enable2 <= '0';

	if (reset = '1') then
		mean_occ_i <= 0;
	else
		if meanpipe_enable = '1' then
			meanpipe_tmp1 <= (tau_max-tau) * mean_occ_i;
			meanpipe_tmp2 <= tau * meanpipe_din;
			meanpipe_enable2 <= '1';
		end if;

		if meanpipe_enable2 = '1' then
			mean_occ_i <= (meanpipe_tmp1 + meanpipe_tmp2)/tau_max;
		end if;
	end if;
end process;

mean_occ <= std_logic_vector(to_unsigned(mean_occ_i, 16));

end architecture ; -- rtl