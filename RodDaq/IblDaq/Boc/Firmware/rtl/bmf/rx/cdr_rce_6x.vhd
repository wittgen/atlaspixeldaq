------------------------------------------------------------------------------
--
--  Xilinx, Inc. 2002                 www.xilinx.com
--
--  XAPP 225
--
------------------------------------------------------------------------------
--
--  File name :       sync_master_v2.vhd
--
--  Description :     Master phase aligner module for Virtex2 (2 bits)
--
--  Date - revision : January 6th 2009 - v 1.2
--
--  Author :          NJS
--
--  Disclaimer: LIMITED WARRANTY AND DISCLAMER. These designs are
--              provided to you "as is". Xilinx and its licensors make and you
--              receive no warranties or conditions, express, implied,
--              statutory or otherwise, and Xilinx specifically disclaims any
--              implied warranties of merchantability, non-infringement,or
--              fitness for a particular purpose. Xilinx does not warrant that
--              the functions contained in these designs will meet your
--              requirements, or that the operation of these designs will be
--              uninterrupted or error free, or that defects in the Designs
--              will be corrected. Furthermore, Xilinx does not warrantor
--              make any representations regarding use or the results of the
--              use of the designs in terms of correctness, accuracy,
--              reliability, or otherwise.
--
--              LIMITATION OF LIABILITY. In no event will Xilinx or its
--              licensors be liable for any loss of data, lost profits,cost
--              or procurement of substitute goods or services, or for any
--              special, incidental, consequential, or indirect d[DX] Dependencies for CalibGui.cc
--              arising from the use or operation of the designs or
--              accompanying documentation, however caused and on any theory
--              of liability. This limitation will apply even if Xilinx
--              has been advised of the possibility of such damage. This
--              limitation shall apply not-withstanding the failure of the
--              essential purpose of any limited remedies herein.
--
--  Copyright  2002 Xilinx, Inc.
--  All rights reserved
--
------------------------------------------------------------------------------
--
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library unisim ;
use unisim.vcomponents.all ;

library work;
use work.bocpack.all;

entity cdr_rce_6x is
    port (
        clk160      : in std_logic ;            -- clock input
        clk960      : in std_logic ;            -- fast oversampling clock
        clk960_serdesstrobe : in std_logic ;    -- SERDES strobe
        rdatain     : in std_logic;             -- data input
        rst         : in std_logic ;            -- reset input
        phaseOut    : out std_logic_vector(2 downto 0) ;
        phaseIn     : in std_logic_vector(2 downto 0) ;
        dataout     : out CDR_record ;          -- data output
        phaseTrain : in std_logic ;            -- Flag for calibration
        phaseOverride : in std_logic;             -- Flag for manual phase override
        countera    : out std_logic_vector(7 downto 0);
        counterb    : out std_logic_vector(7 downto 0);
        counterc    : out std_logic_vector(7 downto 0);
        counterd    : out std_logic_vector(7 downto 0);
		    countere    : out std_logic_vector(7 downto 0);
		    counterf    : out std_logic_vector(7 downto 0);
		    debug       : out std_logic_vector(23 downto 0)
    );
end cdr_rce_6x;

architecture arch_syncdata of cdr_rce_6x is

signal aa0          : std_logic;
signal bb0          : std_logic;
signal cc0              : std_logic;
signal dd0              : std_logic;
signal ee0              : std_logic;
signal ff0              : std_logic;
signal usea         : std_logic;
signal useb         : std_logic;
signal usec         : std_logic;
signal used         : std_logic;
signal usee         : std_logic;
signal usef         : std_logic;
signal useaint      : std_logic;
signal usebint      : std_logic;
signal usecint      : std_logic;
signal usedint      : std_logic;
signal useeint      : std_logic;
signal usefint      : std_logic;
signal ctrlint      : std_logic_vector(1 downto 0);
signal sdataa       : std_logic;
signal sdatab       : std_logic;
signal sdatac       : std_logic;
signal sdatad       : std_logic;
signal sdatae       : std_logic;
signal sdataf       : std_logic;
signal az           : std_logic_vector(2 downto 0) ;
signal bz           : std_logic_vector(2 downto 0) ;
signal cz           : std_logic_vector(2 downto 0) ;
signal dz           : std_logic_vector(2 downto 0) ;
signal ez           : std_logic_vector(2 downto 0) ;
signal fz           : std_logic_vector(2 downto 0) ;
signal aap, bbp     : std_logic;
signal aan, bbn     : std_logic;
signal ccp, ddp     : std_logic;
signal ccn, ddn     : std_logic;
signal eep, ffp     : std_logic;
signal een, ffn     : std_logic;

signal pipe_ce0     : std_logic;

signal serdes_cascade : std_logic;

-- signal control          : std_logic_vector(35 downto 0) ;


-- FS: Counters to determine most likely phase
signal countera_i         : std_logic_vector(7 downto 0);
signal counterb_i         : std_logic_vector(7 downto 0);
signal counterc_i         : std_logic_vector(7 downto 0);
signal counterd_i         : std_logic_vector(7 downto 0);
signal countere_i         : std_logic_vector(7 downto 0);
signal counterf_i         : std_logic_vector(7 downto 0);

signal useatemp         : std_logic;
signal usebtemp         : std_logic;
signal usectemp         : std_logic;
signal usedtemp         : std_logic;
signal useetemp         : std_logic;
signal useftemp         : std_logic;

-- signal data             : std_logic_vector(15 downto 0);
-- signal trigger          : std_logic_vector(7 downto 0);
signal count : std_logic;
signal changed : std_logic;

begin

-- output phase
process begin
    wait until rising_edge(clk160);

    if useaint = '1' then
        phaseOut <= "000";
    elsif usebint = '1' then
        phaseOut <= "001";
    elsif usecint = '1' then
        phaseOut <= "010";
    elsif usedint = '1' then
        phaseOut <= "011";
    elsif useeint = '1' then
        phaseOut <= "100";
    elsif usefint = '1' then
        phaseOut <= "101";
    else
        phaseOut <= "111";
    end if;
end process;

sdataa <= (aa0 and useaint) ;
sdatab <= (bb0 and usebint) ;
sdatac <= (cc0 and usecint) ;
sdatad <= (dd0 and usedint) ;
sdatae <= (ee0 and useeint) ;
sdataf <= (ff0 and usefint) ;


saa0 : srl16 port map(d => az(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => aa0);
sbb0 : srl16 port map(d => bz(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => bb0);
scc0 : srl16 port map(d => cz(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => cc0);
sdd0 : srl16 port map(d => dz(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => dd0);
see0 : srl16 port map(d => ez(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => ee0);
sff0 : srl16 port map(d => fz(2), clk => clk160, a0 => ctrlint(0), a1 => ctrlint(1), a2 => '0', a3 => '0', q => ff0);

process (clk160, rst)
begin
if rst = '1' then
    ctrlint <= "10" ;
    useaint <= '0' ; usebint <= '0' ; usecint <= '0' ; usedint <= '0' ; useeint <= '0' ; usefint <= '0' ;
    usea <= '0' ; useb <= '0' ; usec <= '0' ; used <= '0' ; usee <= '0' ; usef <= '0' ;
        useatemp <= '0';usebtemp <= '0';usectemp <= '0';usedtemp <= '0';useetemp <= '0';useftemp <= '0';
    pipe_ce0 <= '0' ; dataout.value <= "00" ;
    aap <= '0' ; bbp <= '0' ; ccp <= '0' ; ddp <= '0' ; eep <= '0' ; ffp <= '0' ;
    aan <= '0' ; bbn <= '0' ; ccn <= '0' ; ddn <= '0' ; een <= '0' ; ffn <= '0' ;
    az(2) <= '0' ; bz(2) <= '0' ; cz(2) <= '0' ; dz(2) <= '0' ; ez(2) <= '0' ; fz(2) <= '0' ;
        countera_i <= "00000000"; counterb_i <= "00000000"; counterc_i <= "00000000"; counterd_i <= "00000000";
        countere_i <= "00000000"; counterf_i <= "00000000";
        count <= '0';
        changed <= '0';
elsif clk160'event and clk160 = '1' then
    az(2) <= az(1) ; bz(2) <= bz(1) ; cz(2) <= cz(1) ; dz(2) <= dz(1) ; ez(2) <= ez(1) ; fz(2) <= fz(1) ;
    aap <= (az(2) xor az(1)) and not az(1) ;    -- find positive edges
    bbp <= (bz(2) xor bz(1)) and not bz(1) ;
    ccp <= (cz(2) xor cz(1)) and not cz(1) ;
    ddp <= (dz(2) xor dz(1)) and not dz(1) ;
    eep <= (ez(2) xor ez(1)) and not ez(1) ;
    ffp <= (ez(2) xor fz(1)) and not fz(1) ;
    aan <= (az(2) xor az(1)) and az(1) ;        -- find negative edges
    bbn <= (bz(2) xor bz(1)) and bz(1) ;
    ccn <= (cz(2) xor cz(1)) and cz(1) ;
    ddn <= (dz(2) xor dz(1)) and dz(1) ;
    een <= (ez(2) xor ez(1)) and ez(1) ;
    ffn <= (fz(2) xor fz(1)) and fz(1) ;
    useatemp <= (aap and bbp and ccp and not ddp and not eep and not ffp) or
                (aan and bbn and ccn and not ddn and not een and not ffn);
    usebtemp <= (aap and bbp and ccp and ddp and not eep and not ffp) or
                (aan and bbn and ccn and ddn and not een and not ffn);
    usectemp <= (aap and bbp and ccp and ddp and eep and not ffp) or
                (aan and bbn and ccn and ddn and een and not ffn);
    usedtemp <= (aap and bbp and ccp and ddp and eep and ffp) or
                (aan and bbn and ccn and ddn and een and ffn);
    useetemp <= (aap and not bbp and not ccp and not ddp and not eep and not ffp) or
                (aan and not bbn and not ccn and not ddn and not een and not ffn);
    useftemp <= (aap and bbp and not ccp and not ddp and not eep and not ffp) or
                (aan and bbn and not ccn and not ddn and not een and not ffn);

	  if phaseOverride = '0' then
		  if (phaseTrain = '1') and (count = '0') then
			  countera_i <= "00000000";        -- FS: Reset all counters if one of them has reached the end
			  counterb_i <= "00000000";
			  counterc_i <= "00000000";
			  counterd_i <= "00000000";
			  countere_i <= "00000000";
			  counterf_i <= "00000000";
			  count <= '1';
      else
        if count = '1' then
          -- histogram valid phases
          if (useatemp='1') and (countera_i/=x"FF") then                -- FS: Only one of the usex equal 1
            countera_i <= countera_i+1;
          elsif (usebtemp='1') and (counterb_i/=x"FF") then
            counterb_i <= counterb_i+1;
          elsif (usectemp='1') and (counterc_i/=x"FF") then
            counterc_i <= counterc_i+1;
          elsif (usedtemp='1') and (counterd_i/=x"FF") then
            counterd_i <= counterd_i+1;
          elsif (useetemp='1') and (countere_i/=x"FF") then
            countere_i <= countere_i+1;
          elsif (useftemp='1') and (counterf_i/=x"FF") then
            counterf_i <= counterf_i+1;
          end if;

          -- decide which phase to take
          if countera_i = "11111111" then    -- FS: . . . and set the one reaching the end of the counter
                usea <= '1';
                useb <= '0';
                usec <= '0';
                used <= '0';
                usee <= '0';
                usef <= '0';
                changed <= '1';
                count <= '0';
                
          elsif counterb_i = "11111111" then
                usea <= '0';
                useb <= '1';
                usec <= '0';
                used <= '0';
                usee <= '0';
                usef <= '0';
                changed <= '1';
                count <= '0';
                
          elsif counterc_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '1';
                used <= '0';
                usee <= '0';
                usef <= '0';
                changed <= '1';
                count <= '0';

          elsif counterd_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '0';
                used <= '1';
                usee <= '0';
                usef <= '0';
                changed <= '1';
                count <= '0';

          elsif countere_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '0';
                used <= '0';
                usee <= '1';
                usef <= '0';
                changed <= '1';
                count <= '0';

          elsif counterf_i = "11111111" then
                usea <= '0';
                useb <= '0';
                usec <= '0';
                used <= '0';
                usee <= '0';
                usef <= '1';
                changed <= '1';
                count <= '0';
          end if;
        else
          if changed  = '1' then
               changed <= '0';
               pipe_ce0 <= '1' ;
               useaint <= usea ;
               usebint <= useb ;
               usecint <= usec ;
               usedint <= used ;    
               useeint <= usee ;
               usefint <= usef ;            
          end if ;
        end if;
      end if;
    else
      case phaseIn is
          when "000" =>
              usea <= '1';
              useb <= '0';
              usec <= '0';
              used <= '0';
              usee <= '0';
              usef <= '0';
          when "001" =>
              usea <= '0';
              useb <= '1';
              usec <= '0';
              used <= '0';
              usee <= '0';
              usef <= '0';
          when "010" =>
              usea <= '0';
              useb <= '0';
              usec <= '1';
              used <= '0';
              usee <= '0';
              usef <= '0';
          when "011" =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '1';
              usee <= '0';
              usef <= '0';
          when "100" =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '0';
              usee <= '1';
              usef <= '0';
          when "101" =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '0';
              usee <= '0';
              usef <= '1';
          when others =>
              usea <= '0';
              useb <= '0';
              usec <= '0';
              used <= '0';
              usee <= '0';
              usef <= '0';
      end case;

      pipe_ce0 <= '1' ;
      useaint <= usea ;
      usebint <= useb ;
      usecint <= usec ;
      usedint <= used ;                
      useeint <= usee ;
      usefint <= usef ;
    end if ;

    if pipe_ce0 = '1' then
        dataout.value(0) <= sdataa or sdatab or sdatac or sdatad or sdatae or sdataf;
        dataout.value(1) <= '0';
    end if ;
    if usefint = '1' and usea = '1' then        -- 'f' going to 'a'
        ctrlint <= ctrlint - 1 ;
    elsif useaint = '1' and usef = '1' then     -- 'a' going to 'f'
        ctrlint <= ctrlint + 1 ;
    end if ;
end if ;
end process ;

dataout.valid <= "01" when pipe_ce0 = '1' else "00";
dataout.lock <= pipe_ce0;

-- get all the samples into the same time domain (using SERDES)
serdes_master: ISERDES2
        generic map (
            BITSLIP_ENABLE => FALSE,        -- Enable Bitslip Functionality (TRUE/FALSE)
            DATA_RATE => "SDR",             -- Data-rate ("SDR" or "DDR")
            DATA_WIDTH => 6,                -- Parallel data width selection (2-8)
            INTERFACE_TYPE => "RETIMED",    -- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED" 
            SERDES_MODE => "MASTER"         -- "NONE", "MASTER" or "SLAVE" 
        )
        port map (
            CFB0 => open,           -- 1-bit output: Clock feed-through route output
            CFB1 => open,           -- 1-bit output: Clock feed-through route output
            DFB => open,            -- 1-bit output: Feed-through clock output
            FABRICOUT => open,  -- 1-bit output: Unsynchrnonized data output
            INCDEC => open,         -- 1-bit output: Phase detector output
            -- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
            Q1 => CZ(0),
            Q2 => DZ(0),
            Q3 => EZ(0),
            Q4 => FZ(0),
            SHIFTOUT => serdes_cascade,-- 1-bit output: Cascade output signal for master/slave I/O
            VALID => open,          -- 1-bit output: Output status of the phase detector
            BITSLIP => '0',         -- 1-bit input: Bitslip enable input
            CE0 => '1',             -- 1-bit input: Clock enable input
            CLK0 => clk960,         -- 1-bit input: I/O clock network input
            CLK1 => '0',            -- 1-bit input: Secondary I/O clock network input
            CLKDIV => clk160,       -- 1-bit input: FPGA logic domain clock input
            D => rdatain,           -- 1-bit input: Input data
            IOCE => clk960_serdesstrobe,            -- 1-bit input: Data strobe input
            RST => rst,             -- 1-bit input: Asynchronous reset input
            SHIFTIN => '0'          -- 1-bit input: Cascade input signal for master/slave I/O
        );

serdes_slaves: ISERDES2
        generic map (
            BITSLIP_ENABLE => FALSE,        -- Enable Bitslip Functionality (TRUE/FALSE)
            DATA_RATE => "SDR",             -- Data-rate ("SDR" or "DDR")
            DATA_WIDTH => 6,                -- Parallel data width selection (2-8)
            INTERFACE_TYPE => "RETIMED",    -- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED" 
            SERDES_MODE => "SLAVE"         -- "NONE", "MASTER" or "SLAVE" 
        )
        port map (
            CFB0 => open,           -- 1-bit output: Clock feed-through route output
            CFB1 => open,           -- 1-bit output: Clock feed-through route output
            DFB => open,            -- 1-bit output: Feed-through clock output
            FABRICOUT => open,  -- 1-bit output: Unsynchrnonized data output
            INCDEC => open,         -- 1-bit output: Phase detector output
            -- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
            Q1 => open,
            Q2 => open,
            Q3 => AZ(0),
            Q4 => BZ(0),
            SHIFTOUT => open,       -- 1-bit output: Cascade output signal for master/slave I/O
            VALID => open,          -- 1-bit output: Output status of the phase detector
            BITSLIP => '0',         -- 1-bit input: Bitslip enable input
            CE0 => '1',             -- 1-bit input: Clock enable input
            CLK0 => clk960,         -- 1-bit input: I/O clock network input
            CLK1 => '0',            -- 1-bit input: Secondary I/O clock network input
            CLKDIV => clk160,       -- 1-bit input: FPGA logic domain clock input
            D => rdatain,           -- 1-bit input: Input data
            IOCE => clk960_serdesstrobe,            -- 1-bit input: Data strobe input
            RST => rst,             -- 1-bit input: Asynchronous reset input
            SHIFTIN => serdes_cascade  -- 1-bit input: Cascade input signal for master/slave I/O
        );

AZ(1) <= AZ(0) when rising_edge(clk160);
BZ(1) <= BZ(0) when rising_edge(clk160);
CZ(1) <= CZ(0) when rising_edge(clk160);
DZ(1) <= DZ(0) when rising_edge(clk160);
EZ(1) <= EZ(0) when rising_edge(clk160);
FZ(1) <= FZ(0) when rising_edge(clk160);

countera <= countera_i;
counterb <= counterb_i;
counterc <= counterc_i;
counterd <= counterd_i;
countere <= countere_i;
counterf <= counterf_i;

debug(5 downto 0) <= ffp & eep & ddp & ccp & bbp & aap;
debug(11 downto 6) <= ffn & een & ddn & ccn & bbn & aan;
debug(17 downto 12) <= useftemp & useetemp & usedtemp & usectemp & usebtemp & useatemp;
debug(23 downto 18) <= fz(2) & ez(2) & dz(2) & cz(2) & bz(2) & az(2);

end arch_syncdata;
