-- RX path unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library decode_8b10b;

use work.bocpack.all;

entity bmf_ibl_rx is
	Port (
		-- clocks
		clk_regbank : IN std_logic;
		clk_serial : IN std_logic;

		-- reset input
		reset : IN std_logic;
		
		-- data inputs
		optical_in : IN CDR_record;
		emulator_in : IN CDR_record;
		datatest_in : IN CDR_record;
		loopback_in : IN CDR_record;
		opt_spareA_in : IN CDR_record;
		opt_spareB_in : IN CDR_record;
		opt_spareC_in : IN CDR_record;
		opt_spareD_in : IN CDR_record;

		-- data output
		dout : OUT std_logic_vector(7 downto 0);
		valid : OUT std_logic;
		kchar : OUT std_logic;

		-- phase train
		cdr_train_phase : out std_logic;

		-- allrx control register
		allrx_ctrl : IN std_logic_vector(7 downto 0);
		allrx_ctrl_wren : IN std_logic;

		-- BERT bypass
		bert_bypass : out std_logic;
		bert_bypass_valid : out std_logic;

		-- debugging
		debug_fast : out std_logic_vector(21 downto 0);
		debug_slow : out std_logic_vector(24 downto 0);
		
		-- Registers
		reg_wr : IN RegBank_wr_t(0 to 31);
		reg_rd : OUT RegBank_rd_t(0 to 31);
		reg_wren : IN RegBank_wren_t(0 to 31);
		reg_rden : IN RegBank_rden_t(0 to 31)	
	);
end bmf_ibl_rx;

architecture rtl of bmf_ibl_rx is

	---------------------------------
	-- Component Declaration Begin --
	---------------------------------

	component data_alignment
		port
		(
			clk : in std_logic;
			reset : in std_logic;
		
			din : in std_logic_vector(1 downto 0);
			din_valid : in std_logic_vector(1 downto 0);
		
			dout : out std_logic_vector(9 downto 0);
			dout_valid : out std_logic;
			dout_sync : out std_logic;
			dout_kword : out std_logic
		);
	end component;

	component monitor_fifo
		port
		(
			  clk : in std_logic;
			  srst : in std_logic;
			  din : in std_logic_vector(15 downto 0);
			  wr_en : in std_logic;
			  rd_en : in std_logic;
			  dout : out std_logic_vector(15 downto 0);
			  full : out std_logic;
			  empty : out std_logic
		);
	end component;

	COMPONENT decode_8b10b_wrapper
	PORT(
		CLK : IN std_logic;
		DIN : IN std_logic_vector(9 downto 0);
		CE : IN std_logic;
		SINIT : IN std_logic;          
		DOUT : OUT std_logic_vector(7 downto 0);
		KOUT : OUT std_logic;
		CODE_ERR : OUT std_logic;
		DISP_ERR : OUT std_logic;
		ND : OUT std_logic
		);
	END COMPONENT;
	
	COMPONENT rx_clkcross
	  PORT (
		 wr_clk : IN STD_LOGIC;
		 rd_clk : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(9 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(9 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC
	  );
	END COMPONENT;

	component data_monitor
		port
		(
			clk : in std_logic;
			reset : in std_logic;

			din : in std_logic_vector(7 downto 0);
			din_kchar : in std_logic;
			din_valid : in std_logic;

			cntmode : in std_logic;

			frame_error : out std_logic;
			frame_cnt : out std_logic_vector(31 downto 0);
			frame_err_cnt : out std_logic_vector(31 downto 0)
		);
	end component;

	component dcs_monitor
		port (
			clk : in std_logic;
			reset : in std_logic;
		
			-- front-end data
			din : in std_logic_vector(7 downto 0);
			kchar : in std_logic;
			valid : in std_logic;
		
			-- register interface
			csr_rd : out std_logic_vector(7 downto 0);
			csr_wr : in std_logic_vector(7 downto 0);
			csr_rden : in std_logic;
			csr_wren : in std_logic;
			fifoh_rd : out std_logic_vector(7 downto 0);
			fifoh_wr : in std_logic_vector(7 downto 0);
			fifoh_rden : in std_logic;
			fifoh_wren : in std_logic;
			fifol_rd : out std_logic_vector(7 downto 0);
			fifol_wr : in std_logic_vector(7 downto 0);
			fifol_rden : in std_logic;
			fifol_wren : in std_logic;
		
			-- interrupt
			interrupt : out std_logic
		);
	end component;

	component SimpleReg
		generic
		(
			rst_value : std_logic_vector(7 downto 0) := x"00";
			readonly : string := "false"
		);
		port
		(
			clk_i : in std_logic;
			rst_i : in std_logic;
			wr : in std_logic_vector(7 downto 0);
			wren : in std_logic;          
			rd : out std_logic_vector(7 downto 0);
			rden : in std_logic;
			regval : out std_logic_vector(7 downto 0)
		);
	end component;

	component rx_status
		port (
			-- clocking
			clk : in std_logic;

			-- data input
			din : in std_logic_vector(7 downto 0);
			din_kchar : in std_logic;
			din_valid : in std_logic;

			-- data output
			dout : out std_logic_vector(7 downto 0);
			dout_kchar : out std_logic;
			dout_valid : out std_logic;

			-- enable status reporting
			enable : in std_logic;

			-- status inputs
			lock : in std_logic;
			sync : in std_logic;
			dec_error : in std_logic;
			disp_error : in std_logic;
			frame_error : in std_logic
		);
	end component;

	component LinkOccMonitor
		port (
			-- clocking and reset
			clk : in std_logic;
			reset : in std_logic;

			-- data input
			din : in std_logic_vector(7 downto 0);
			kchar : in std_logic;
			valid : in std_logic;

			-- occupancy output (16 bits)
			live_occ : out std_logic_vector(15 downto 0);
			mean_occ : out std_logic_vector(15 downto 0)
		);
	end component;

	component ErrHisto
		port (
			-- clocking and reset
			clk : in std_logic;
			reset : in std_logic;

			-- flag inputs
			locked : in std_logic;
			synced : in std_logic;
			dec_err : in std_logic;
			disp_err : in std_logic;
			decdisp_valid : in std_logic;
			frame_err : in std_logic;

			-- register interface
			control_wr : in std_logic_vector(7 downto 0);
			control_rd : out std_logic_vector(7 downto 0);
			control_wren : in std_logic;
			control_rden : in std_logic;
			count_wr : in std_logic_vector(7 downto 0);
			count_rd : out std_logic_vector(7 downto 0);
			count_wren : in std_logic;
			count_rden : in std_logic;
			mask_wr : in std_logic_vector(7 downto 0);
			mask_rd : out std_logic_vector(7 downto 0);
			mask_wren : in std_logic;
			mask_rden : in std_logic
		);
	end component;
	
	-------------------------------
	-- Component Declaration End --
	-------------------------------
	
	----------------------------
	-- Internal Signals Begin --
	----------------------------

	signal rxreset : std_logic := '1';
	signal rxreset_fast : std_logic := '1';

	signal align_lock : std_logic := '0';
	signal align_din : std_logic_vector(1 downto 0) := "00";
	signal align_din_tmp : std_logic_vector(1 downto 0) := "00";
	signal align_din_valid : std_logic_vector(1 downto 0) := "00";	
	signal align_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal align_dout_valid : std_logic := '0';
	signal align_sync : std_logic := '0';
	signal align_kword : std_logic := '0';
	
	signal clkcross_din : std_logic_vector(9 downto 0) := (others => '0');
	signal clkcross_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal clkcross_rden : std_logic := '0';
	signal clkcross_wren : std_logic := '0';
	signal clkcross_full : std_logic := '0';
	signal clkcross_empty : std_logic := '0';

	signal dec_din : std_logic_vector(9 downto 0) := (others => '0');
	signal dec_din_valid : std_logic := '0';
	signal dec_din_valid_async : std_logic := '0';
	signal dec_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal dec_kchar : std_logic := '0';
	signal dec_dout_valid : std_logic := '0';
	signal dec_decErr : std_logic := '0';
	signal dec_dispErr : std_logic := '0';
	signal dec_din_valid_delay : std_logic := '0';

	signal monitor_fifo_din : std_logic_vector(15 downto 0) := (others => '0');
	signal monitor_fifo_dout : std_logic_vector(15 downto 0) := (others => '0');
	signal monitor_fifo_wren : std_logic := '0';
	signal monitor_fifo_rden : std_logic := '0';
	signal monitor_fifo_empty : std_logic := '0';
	signal monitor_fifo_full : std_logic := '0';

	signal frame_error : std_logic := '0';
	
	signal control_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal monitor_raw : std_logic := '0';
	signal monitor_enable : std_logic := '0';
	signal mux_enable : std_logic := '0';
	signal mux_enable_reset : std_logic := '0';
	signal rx_enable : std_logic := '0';
	signal rx_enable_n : std_logic := '1';
	signal allow_idles : std_logic := '0';
--	signal loop_enable : std_logic := '0';
	signal input_select : std_logic_vector(2 downto 0) := "000";

	signal frame_cnt : std_logic_vector(31 downto 0) := (others => '0');
	signal frame_cnt_tmp : std_logic_vector(23 downto 0) := (others => '0');
	signal frame_err_cnt : std_logic_vector(31 downto 0) := (others => '0');
	signal frame_err_cnt_tmp : std_logic_vector(23 downto 0) := (others => '0');
	signal cntmode : std_logic := '0';

	signal rxstatus_din : std_logic_vector(7 downto 0) := (others => '0');
	signal rxstatus_din_valid : std_logic := '0';
	signal rxstatus_din_kchar : std_logic := '0';
	signal rxstatus_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal rxstatus_dout_valid : std_logic := '0';
	signal rxstatus_dout_kchar : std_logic := '0';
	signal rxstatus_enable : std_logic := '0';

	signal occmon_liveocc : std_logic_vector(15 downto 0) := (others => '0');
	signal occmon_meanocc : std_logic_vector(15 downto 0) := (others => '0');
	signal occmon_temp1 : std_logic_vector(7 downto 0) := (others => '0');		-- temp register for counter readout
	signal occmon_temp2 : std_logic_vector(7 downto 0) := (others => '0');

	signal cdr_train_phase_i : std_logic := '0';
	signal dec_decErr_fast : std_logic := '0';
	signal dec_dispErr_fast : std_logic := '0';
	signal frame_error_fast : std_logic := '0';
	signal rx_enable_fast : std_logic := '0';
	signal rx_invert : std_logic := '0';

	signal rodsave_cnt : integer range 0 to 255 := 0;
	signal rodsave_errcnt : integer range 0 to 255 := 0;
	signal rodsave_thresh : integer range 0 to 255 := 255;
	signal rodsave_enable : std_logic := '0';

	--------------------------
	-- Internal Signals End --
	--------------------------
	
begin

-- generate path reset if rx channel is not enabled
rxreset <= (reset or (not rx_enable)) when rising_edge(clk_regbank);
rxreset_fast <= rxreset when rising_edge(clk_serial);

-- multiplex data input to the alignment block
process begin
	wait until rising_edge(clk_serial);

	case input_select is
		when "000" =>
			align_din_tmp <= optical_in.value;
			align_din_valid <= optical_in.valid;
			align_lock <= optical_in.lock;
		when "001" =>
			align_din_tmp <= emulator_in.value;
			align_din_valid <= emulator_in.valid;
			align_lock <= emulator_in.lock;
		when "010" =>
			align_din_tmp <= datatest_in.value;
			align_din_valid <= datatest_in.valid;
			align_lock <= datatest_in.lock;
		when "011" =>
			align_din_tmp <= loopback_in.value;
			align_din_valid <= loopback_in.valid;
			align_lock <= loopback_in.lock;
		when "100" =>
			align_din_tmp <= opt_spareA_in.value;
			align_din_valid <= opt_spareA_in.valid;
			align_lock <= opt_spareA_in.lock;
		when "101" =>
			align_din_tmp <= opt_spareB_in.value;
			align_din_valid <= opt_spareB_in.valid;
			align_lock <= opt_spareB_in.lock;
		when "110" =>
			align_din_tmp <= opt_spareC_in.value;
			align_din_valid <= opt_spareC_in.valid;
			align_lock <= opt_spareC_in.lock;
		when "111" =>
			align_din_tmp <= opt_spareD_in.value;
			align_din_valid <= opt_spareD_in.valid;
			align_lock <= opt_spareD_in.lock;
		when others =>
			align_din_tmp <= optical_in.value;
			align_din_valid <= optical_in.valid;
	end case;
end process;

-- invert before alignment unit
align_din <= not align_din_tmp when rx_invert = '1' else align_din_tmp;

-- output bypass signal
bert_bypass <= align_din(0) when rising_edge(clk_serial);
bert_bypass_valid <= align_din_valid(0) when rising_edge(clk_serial);

-- data alignment
align: data_alignment PORT MAP(
	clk => clk_serial,
	reset => rxreset_fast,
	din => align_din,
	din_valid => align_din_valid,
	dout => align_dout,
	dout_valid => align_dout_valid,
	dout_sync => align_sync,
	dout_kword => align_kword
);

-- fill clock crossing FIFO
clkcross_din <= align_dout;
clkcross_wren <= align_dout_valid and not clkcross_full and align_sync;

-- clock crossing FIFO
clkcross_i: rx_clkcross
  PORT MAP (
    wr_clk => clk_serial,
    rd_clk => clk_regbank,
    din => clkcross_din,
    wr_en => clkcross_wren,
    rd_en => clkcross_rden,
    dout => clkcross_dout,
    full => clkcross_full,
    empty => clkcross_empty
  );

-- register input data to the decoder
process begin
	wait until rising_edge(clk_regbank);

	-- we need to reverse the bit order of 8b10b data
	for I in 0 to 9 loop
		dec_din(I) <= clkcross_dout(9-I);
	end loop;

	clkcross_rden <= not clkcross_empty and not dec_din_valid;
	dec_din_valid <= not clkcross_empty and not dec_din_valid;
	--dec_din_valid_delay <= dec_din_valid;
	--dec_dout_valid <= dec_din_valid_delay;
	dec_dout_valid <= dec_din_valid;
end process;

-- 8b10b decoder
dec: decode_8b10b_wrapper PORT MAP(
	CLK => clk_regbank,
	DIN => dec_din,
	CE => dec_din_valid,
	SINIT => '0',
	DOUT => dec_dout,
	KOUT => dec_kchar,
	CODE_ERR => dec_decErr,
	DISP_ERR => dec_dispErr,
	ND => open
);

-- process data output
process begin
	wait until rising_edge(clk_regbank);

	rxstatus_din <= dec_dout;
	rxstatus_din_valid <= dec_dout_valid and align_sync and rx_enable and mux_enable;
	-- force only SOF, EOF and IDLE to be kchar
	if (dec_dout = x"FC") or (dec_dout = x"BC") or (dec_dout = x"3C") then
		rxstatus_din_kchar <= dec_kchar;
	else
		rxstatus_din_kchar <= '0';
	end if;
end process;

dout <= rxstatus_dout;
kchar <= rxstatus_dout_kchar;
valid <= rxstatus_dout_valid;

rx_enable_n <= not rx_enable;

-- FE-I4 data monitoring
mon: data_monitor PORT MAP(
	clk => clk_regbank,
	reset => rxreset,
	din => dec_dout,
	din_valid => dec_dout_valid,
	din_kchar => dec_kchar,
	cntmode => cntmode,
	frame_error => frame_error,
	frame_cnt => frame_cnt,
	frame_err_cnt => frame_err_cnt
);

-- monitor fifo instance
mon_fifo: monitor_fifo PORT MAP(
	clk => clk_regbank,
	srst => rxreset,
	din => monitor_fifo_din,
	dout => monitor_fifo_dout,
	wr_en => monitor_fifo_wren,
	rd_en => monitor_fifo_rden,
	full => monitor_fifo_full,
	empty => monitor_fifo_empty
);

-- DCS monitoring
dcs_monitor_i: dcs_monitor PORT MAP (
	clk => clk_regbank,
	reset => rxreset,
	din => dec_dout,
	kchar => dec_kchar,
	valid => dec_dout_valid,
	csr_rd => reg_rd(12),
	csr_wr => reg_wr(12),
	csr_wren => reg_wren(12),
	csr_rden => reg_rden(12),
	fifol_rd => reg_rd(13),
	fifol_wr => reg_wr(13),
	fifol_wren => reg_wren(13),
	fifol_rden => reg_rden(13),
	fifoh_rd => reg_rd(14),
	fifoh_wr => reg_wr(14),
	fifoh_wren => reg_wren(14),
	fifoh_rden => reg_rden(14),
	interrupt => open
);

-- RX status reporting
rx_status_i: rx_status
	port map (
		clk => clk_regbank,
		din => rxstatus_din,
		din_valid => rxstatus_din_valid,
		din_kchar => rxstatus_din_kchar,
		dout => rxstatus_dout,
		dout_valid => rxstatus_dout_valid,
		dout_kchar => rxstatus_dout_kchar,
		enable => rxstatus_enable,
		lock => align_lock,
		sync => align_sync,
		dec_error => dec_decErr,
		disp_error => dec_dispErr,
		frame_error => frame_error
	);

-- link occupancy monitoring
occmon: LinkOccMonitor
	port map (
		clk => clk_regbank,
		reset => rxreset,
		din => dec_dout,
		kchar => dec_kchar,
		valid => dec_dout_valid,
		live_occ => occmon_liveocc,
		mean_occ => occmon_meanocc
	);

-- error histogramming
ErrHisto_i: ErrHisto
	port map (
		clk => clk_regbank,
		reset => reset,
		locked => align_lock,
		synced => align_sync,
		dec_err => dec_decErr,
		disp_err => dec_dispErr,
		decdisp_valid => dec_dout_valid,
		frame_err => frame_error,
		control_wr => reg_wr(28),
		control_rd => reg_rd(28),
		control_wren => reg_wren(28),
		control_rden => reg_rden(28),
		count_wr => reg_wr(29),
		count_rd => reg_rd(29),
		count_wren => reg_wren(29),
		count_rden => reg_rden(29),
		mask_wr => reg_wr(31),
		mask_rd => reg_rd(31),
		mask_wren => reg_wren(31),
		mask_rden => reg_rden(31)
	);

-- write to monitoring fifo
process begin
	wait until rising_edge(clk_regbank);

	if monitor_enable = '1' then
		if monitor_raw = '1' then
			-- put the raw data into the monitor fifo
			-- but do not allow idle words
			if ((clkcross_dout = idle_word) or (clkcross_dout = not idle_word)) and (allow_idles = '0') then
				monitor_fifo_wren <= '0';
			else
				monitor_fifo_din <= align_sync & dec_dispErr & dec_decErr & "000" & dec_din;
				monitor_fifo_wren <= dec_din_valid;
			end if;
		else
			-- put the decoded data into the monitor fifo (do not allow idles)
			if (dec_dout = x"3c" and dec_kchar = '1' and allow_idles = '0') then
				monitor_fifo_wren <= '0';
			else
				monitor_fifo_din <= align_sync & dec_dispErr & dec_decErr & "0000" & dec_kchar & dec_dout;
				monitor_fifo_wren <= dec_dout_valid and align_sync;
			end if;
		end if;
	else
		monitor_fifo_wren <= '0';
	end if;
end process;

-- control register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		control_reg <= "00000000";
	else
		if reg_wren(0) = '1' then
			control_reg <= reg_wr(0);
		elsif allrx_ctrl_wren = '1' then
			control_reg <= allrx_ctrl;
		end if;

		-- reset ROD mux output (in case of too many decoding errors)
		if mux_enable_reset = '1' then
			control_reg(1) <= '0';
		end if;
	end if;
end process;
reg_rd(0) <= control_reg;

rx_enable <= control_reg(0);
mux_enable <= control_reg(1);
monitor_enable <= control_reg(2);
monitor_raw <= control_reg(3);
allow_idles <= control_reg(4);
input_select <= control_reg(7 downto 5);

-- accumulated status register
process begin
	wait until rising_edge(clk_regbank);

	if (rxreset = '1') then
		reg_rd(1)(0) <= '1';
		reg_rd(1)(1) <= '0';
		reg_rd(1)(2) <= '0';
		reg_rd(1)(3) <= '0';
		reg_rd(1)(6) <= '1';
		reg_rd(1)(7) <= '0';
	elsif (reg_wren(1) = '1') then
		if reg_wr(1)(0) = '1' then
			reg_rd(1)(0) <= '1';
		end if;

		if reg_wr(1)(1) = '1' then
			reg_rd(1)(1) <= '0';
		end if;

		if reg_wr(1)(2) = '1' then
			reg_rd(1)(2) <= '0';
		end if;

		if reg_wr(1)(3) = '1' then
			reg_rd(1)(3) <= '0';
		end if;

		if reg_wr(1)(6) = '1' then
			reg_rd(1)(6) <= '1';
		end if;

		if reg_wr(1)(7) = '1' then
			reg_rd(1)(7) <= '0';
		end if;
	else
		if align_sync = '0' then
			reg_rd(1)(0) <= '0';
		end if;

		if dec_decErr = '1' then
			reg_rd(1)(1) <= '1';
		end if;

		if dec_dispErr = '1' then
			reg_rd(1)(2) <= '1';
		end if;
		
		if frame_error = '1' then
			reg_rd(1)(3) <= '1';
		end if;

		if align_lock = '0' then
			reg_rd(1)(6) <= '0';
		end if;

		if mux_enable_reset = '1' then
			reg_rd(1)(7) <= '1';
		end if;
	end if;

	reg_rd(1)(4) <= monitor_fifo_full;
	reg_rd(1)(5) <= monitor_fifo_empty;
end process;

-- rx data register from monitor fifo
reg_rd(2) <= monitor_fifo_dout(15 downto 8);
reg_rd(3) <= monitor_fifo_dout(7 downto 0);
monitor_fifo_rden <= reg_rden(3);

-- frame and frame error counter readout
-- save lower 24 bits into temporary register to allow "atomic" readout
process begin
	wait until rising_edge(clk_regbank);

	if reg_rden(4) = '1' then
		frame_cnt_tmp <= frame_cnt(23 downto 0);
	end if;

	if reg_rden(8) = '1' then
		frame_err_cnt_tmp <= frame_err_cnt(23 downto 0);
	end if;
end process;

reg_rd(4) <= frame_cnt(31 downto 24);
reg_rd(5) <= frame_cnt_tmp(23 downto 16);
reg_rd(6) <= frame_cnt_tmp(15 downto 8);
reg_rd(7) <= frame_cnt_tmp(7 downto 0);
reg_rd(8) <= frame_err_cnt(31 downto 24);
reg_rd(9) <= frame_err_cnt_tmp(23 downto 16);
reg_rd(10) <= frame_err_cnt_tmp(15 downto 8);
reg_rd(11) <= frame_err_cnt_tmp(7 downto 0);

-- second control register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		rodsave_enable <= '0';
		rxstatus_enable <= '0';
		cdr_train_phase_i <= '0';
		rx_invert <= '0';
		cntmode <= '0';
	else
		if reg_wren(15) = '1' then
			rodsave_enable <= reg_wr(15)(0);
			rxstatus_enable <= reg_wr(15)(4);
			cdr_train_phase_i <= reg_wr(15)(5);
			rx_invert <= reg_wr(15)(6);
			cntmode <= reg_wr(15)(7);
		end if;
	end if;
end process;
reg_rd(15) <= cntmode & rx_invert & cdr_train_phase_i & rxstatus_enable & "000" & rodsave_enable;

-- output phase training
process begin
	wait until rising_edge(clk_regbank);

	if cdr_train_phase_i = '1' then
		-- train phase when bit in control reg 2 is set
		cdr_train_phase <= '1';
	elsif (reg_wren(0) = '1') and (reg_wr(0)(0) = '1') then
		-- or train phase when you are enabling the RX channel
		cdr_train_phase <= '1';
	elsif (allrx_ctrl_wren = '1') and (allrx_ctrl(0) = '1') then
		-- or global control register write is enabling the channel
		cdr_train_phase <= '1';
	else
		-- else do not train
		cdr_train_phase <= '0';
	end if;
end process;

-- readout link occupancy
reg_rd(24) <= occmon_meanocc(7 downto 0);
reg_rd(25) <= occmon_temp1;
reg_rd(26) <= occmon_liveocc(7 downto 0);
reg_rd(27) <= occmon_temp2;

process begin
	wait until rising_edge(clk_regbank);

	if reg_rden(24) = '1' then
		occmon_temp1 <= occmon_meanocc(15 downto 8);
	end if;

	if reg_rden(26) = '1' then
		occmon_temp2 <= occmon_liveocc(15 downto 8);
	end if;
end process;

-- disable ROD output if a high error rate is detected
process begin
	wait until rising_edge(clk_regbank);

	if (rxreset = '1') or (rodsave_enable = '0') then
		rodsave_cnt <= 0;
		rodsave_errcnt <= 0;
	else
		-- count incoming words
		if dec_dout_valid = '1' then
			if rodsave_cnt = 255 then
				rodsave_cnt <= 0;
				rodsave_errcnt <= 0;
			else
				rodsave_cnt <= rodsave_cnt + 1;
				-- count decoding errors in all incoming words
				if dec_decErr = '1' then
					rodsave_errcnt <= rodsave_errcnt + 1;
				end if;
			end if;
		end if;
	end if;
end process;

-- rodsave threshold register write
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		rodsave_thresh <= 255;
	else
		-- set threshold
		if reg_wren(30) = '1' then
			rodsave_thresh <= to_integer(unsigned(reg_wr(30)));
		end if;
	end if;
end process;

-- rodsave threshold register readback
reg_rd(30) <= std_logic_vector(to_unsigned(rodsave_thresh, 8));

-- reset ROD mux if error counter is above the threshold
mux_enable_reset <= '1' when ((rodsave_enable = '1') and (rodsave_errcnt > rodsave_thresh)) else '0';

-- do some debug output
process begin
	wait until rising_edge(clk_serial);
	
	debug_fast(1 downto 0) <= align_din;
	debug_fast(3 downto 2) <= align_din_valid;
	debug_fast(4) <= align_lock;

	for I in 0 to 9 loop
		debug_fast(I+5) <= align_dout(9-I);
	end loop;
	debug_fast(15) <= align_dout_valid;
	debug_fast(16) <= align_sync;
	debug_fast(17) <= align_kword;
	debug_fast(18) <= dec_decErr_fast;
	debug_fast(19) <= dec_dispErr_fast;
	debug_fast(20) <= frame_error_fast;
	debug_fast(21) <= rx_enable_fast;
end process;

-- register dec/disp error flags for transfer to 160 MHz domain
dec_decErr_fast <= dec_decErr when rising_edge(clk_serial);
dec_dispErr_fast <= dec_dispErr when rising_edge(clk_serial);
frame_error_fast <= frame_error when rising_edge(clk_serial);
rx_enable_fast <= rx_enable when rising_edge(clk_serial);

process begin
	wait until rising_edge(clk_regbank);

	debug_slow(9 downto 0) <= dec_din;
	debug_slow(10) <= dec_din_valid;
	debug_slow(18 downto 11) <= dec_dout;
	debug_slow(19) <= dec_kchar;
	debug_slow(20) <= dec_dout_valid;
	debug_slow(21) <= dec_decErr;
	debug_slow(22) <= dec_dispErr;
	debug_slow(23) <= frame_error;
	debug_slow(24) <= rx_enable;
end process;

end rtl;
