-- data monitoring
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity data_monitor is
	port
	(
		clk : in std_logic;
		reset : in std_logic;

		din : in std_logic_vector(7 downto 0);
		din_kchar : in std_logic;
		din_valid : in std_logic;

		cntmode : in std_logic;

		frame_error : out std_logic;
		frame_cnt : out std_logic_vector(31 downto 0);
		frame_err_cnt : out std_logic_vector(31 downto 0)
	);
end data_monitor;

architecture rtl of data_monitor is
	signal frame_err_cnt_int : unsigned(31 downto 0) := (others => '0');
	signal frame_cnt_int : unsigned(31 downto 0) := (others => '0');
	signal event_cnt_int : unsigned(31 downto 0) := (others => '0');

	signal fei4_record : std_logic_vector(23 downto 0) := (others => '0');
	signal check_record : std_logic := '0';
	signal hit_frame : std_logic := '0';

	signal frame_error_int : std_logic := '0';

	type fsm_states is (st_reset, st_sof, st_data0, st_data1, st_data2);
	signal Z : fsm_states := st_reset;

	-- store old data
	signal old_data : std_logic_vector(8 downto 0) := (others => '0');

	-- hit record
	signal fei4_col : unsigned(6 downto 0) := (others => '0');
	signal fei4_row : unsigned(8 downto 0) := (others => '0');
	signal fei4_tot1 : std_logic_vector(3 downto 0) := x"0";
	signal fei4_tot2 : std_logic_vector(3 downto 0) := x"0";
begin

-- output internal counters and frame error flag
frame_cnt <= std_logic_vector(frame_cnt_int) when cntmode = '0' else std_logic_vector(event_cnt_int);
frame_err_cnt <= std_logic_vector(frame_err_cnt_int);
frame_error <= frame_error_int;

-- assign hit record
fei4_col <= unsigned(fei4_record(23 downto 17));
fei4_row <= unsigned(fei4_record(16 downto 8));
fei4_tot1 <= fei4_record(7 downto 4);
fei4_tot2 <= fei4_record(3 downto 0);


process begin
	wait until rising_edge(clk);

	-- default assignment
	frame_error_int <= '0';
	check_record <= '0';

	if reset = '1' then
		Z <= st_reset;
		frame_cnt_int <= (others => '0');
		frame_err_cnt_int <= (others => '0');
		event_cnt_int <= (others => '0');
	else
		-- FSM
		if (din_valid = '1') then
			case Z is
				when st_reset =>
					null;		-- do nothing in this state

				when st_sof =>
					if (din_kchar = '0') then
						frame_error_int <= '1';
					else
						if (din /= x"FC") and (din /= x"3C") then
							frame_error_int <= '1';
						end if;
					end if;

				when st_data0 =>
					if (din_kchar = '1') then
						if (din = x"BC") then
							Z <= st_sof;
						else
							frame_error_int <= '1';
						end if;
					else
						fei4_record(23 downto 16) <= din;
						Z <= st_data1;
					end if;

				when st_data1 =>
					if (din_kchar = '1') then
						frame_error_int <= '1';
					else
						fei4_record(15 downto 8) <= din;
						Z <= st_data2;
					end if;

				when st_data2 =>
					if (din_kchar = '1') then
						frame_error_int <= '1';
					else
						fei4_record(7 downto 0) <= din;
						check_record <= '1';
						Z <= st_data0;
					end if;
			end case;
		end if;

		-- count up frame counter always on SOF
		-- also switch to data0 state
		if (din_valid = '1') and (din_kchar = '1') and (din = x"FC") then
			frame_cnt_int <= frame_cnt_int + 1;
			Z <= st_data0;
		end if;

		-- count up events
		if (din_valid = '1') then
			if (din_kchar = '0') and (din = x"E9") and (old_data = '1' & x"FC") then
				event_cnt_int <= event_cnt_int + 1;
			end if;
			old_data <= din_kchar & din;
		end if;

		-- in IDLE state you will always end up in st_sof
		if (din_valid = '1') and (din_kchar = '1') and (din = x"3C") then
			Z <= st_sof;
		end if;

		-- count up frame error counter if an error is detected
		if (frame_error_int = '1') then
			frame_err_cnt_int <= frame_err_cnt_int + 1;
		end if;

		-- check incoming records
		if check_record = '1' then
			if ((fei4_record(23 downto 16) = x"EF") or 		-- service record
			    (fei4_record(23 downto 16) = x"EC") or 		-- value record
			    (fei4_record(23 downto 16) = x"EA") or 		-- address record
			    (fei4_record(23 downto 16) = x"E9")) then	-- data header

				frame_error_int <= '0';						-- all are valid
			else
				-- this seems to be hit data, lets check row col tot
				if ((fei4_row = 0) or (fei4_row > 336) or
					(fei4_col = 0) or (fei4_col > 80) or
					(fei4_tot1 = x"F")) then
					frame_error_int <= '1';
				else
					frame_error_int <= '0';
				end if;
			end if;
		end if;
	end if;
end process;

end architecture;