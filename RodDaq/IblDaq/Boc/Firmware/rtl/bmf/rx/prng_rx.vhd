library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity prng_rx is
  generic (
  	words : integer := 32;
  	seed : std_logic_vector(7 downto 0) := x"42"
  );
  port (
	clk : in std_logic;
	reset : in std_logic;

	enable : in std_logic;
	din : in std_logic_vector(7 downto 0);
	kchar : in std_logic;
	valid : in std_logic;

	clear : in std_logic;
	bitcnt : out std_logic_vector(63 downto 0);
	errcnt : out std_logic_vector(63 downto 0)
  );
end entity ; -- prng_rx

architecture RTL of prng_rx is

component prng
	generic (
		width : integer := 32
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		load : in std_logic;
		seed : in std_logic_vector(width-1 downto 0);
		step : in std_logic;
		dout : out std_logic_vector(width-1 downto 0)
	);
end component;

signal prng_load : std_logic := '0';
signal prng_step : std_logic := '0';
signal prng_dout : std_logic_vector(7 downto 0) := (others => '0');

type fsm_states is (st_idle, st_sync, st_sof, st_data, st_eof);
signal Z : fsm_states := st_idle;

signal bitcnt_int : unsigned(63 downto 0) := (others => '0');
signal errcnt_int : unsigned(63 downto 0) := (others => '0');

signal compare : std_logic := '0';
signal opA : std_logic_vector(7 downto 0) := (others => '0');
signal opB : std_logic_vector(7 downto 0) := (others => '0');
signal diff : std_logic_vector(7 downto 0) := (others => '0');

signal wordcnt : integer range 0 to words-1 := 0;

begin

-- prng instance
prng_i: prng
	GENERIC MAP (
		width => 8
	)
	PORT MAP (
		clk => clk,
		reset => reset,
		load => prng_load,
		seed => seed,
		step => prng_step,
		dout => prng_dout
	);


-- compare received values
process
	variable biterr : integer range 0 to 8;
begin
	wait until rising_edge(clk);

	if (reset = '1') or (clear = '1') then
		bitcnt_int <= (others => '0');
		errcnt_int <= (others => '0');
	else
		if (compare = '1') and (enable = '1') then
			diff <= opA xor opB;

			biterr := 0;
			for I in 0 to 7 loop
				if diff(I) = '1' then
					biterr := biterr + 1;
				end if;
			end loop;

			bitcnt_int <= bitcnt_int + 8;
			errcnt_int <= errcnt_int + biterr;
		end if;
	end if;
end process;

prng_load <= '1' when Z = st_idle else
			 '1' when Z = st_eof else
			 '0';
prng_step <= valid when Z = st_data else '0';

-- receive and sync state machine
process begin
	wait until rising_edge(clk);

	-- default values
	compare <= '0';

	if reset = '1' then
		Z <= st_idle;
		wordcnt <= 0;
	else
		case Z is
			when st_idle =>
				if enable = '1' then
					Z <= st_sync;
				end if;

			when st_sync =>
				null;

			when st_sof =>
				if (valid = '1') then
					opA <= din;
					opB <= x"fc";
					compare <= '1';

					Z <= st_data;
				end if;

			when st_data =>
				if valid = '1' then
					opA <= din;
					opB <= prng_dout;
					compare <= '1';

					if wordcnt = words-1 then
						Z <= st_eof;
						wordcnt <= 0;
					else
						wordcnt <= wordcnt + 1;
					end if;
				end if;

			when st_eof =>
				if (valid = '1') then
					opA <= din;
					opB <= x"bc";
					compare <= '1';
					
					if enable = '1' then
						Z <= st_sof;
					else
						Z <= st_idle;
					end if;
				end if;
		end case;

		-- make sure that next step after sof detection is st_data
		if (valid = '1') and (din = x"fc") and (kchar = '1') and (enable = '1') then
			Z <= st_data;
		end if;
	end if;
end process;

-- output counters
bitcnt <= std_logic_vector(bitcnt_int);
errcnt <= std_logic_vector(errcnt_int);

end architecture ; -- RTL