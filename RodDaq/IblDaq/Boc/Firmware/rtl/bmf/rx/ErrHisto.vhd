library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ErrHisto is
	port (
		-- clocking and reset
		clk : in std_logic;
		reset : in std_logic;

		-- flag inputs
		locked : in std_logic;
		synced : in std_logic;
		dec_err : in std_logic;
		disp_err : in std_logic;
		decdisp_valid : in std_logic;
		frame_err : in std_logic;

		-- register interface
		control_wr : in std_logic_vector(7 downto 0);
		control_rd : out std_logic_vector(7 downto 0);
		control_wren : in std_logic;
		control_rden : in std_logic;
		count_wr : in std_logic_vector(7 downto 0);
		count_rd : out std_logic_vector(7 downto 0);
		count_wren : in std_logic;
		count_rden : in std_logic;
		mask_wr : in std_logic_vector(7 downto 0);
		mask_rd : out std_logic_vector(7 downto 0);
		mask_wren : in std_logic;
		mask_rden : in std_logic
	);
end ErrHisto;

architecture rtl of ErrHisto is
	-- SimpleReg componennt
	component SimpleReg
		generic (
			rst_value : std_logic_vector(7 downto 0) := x"00";
			readonly : string := "false"
		);
		port (
			-- clock and reset
			clk_i : in std_logic := '0';
			rst_i : in std_logic := '0';
			
			wr : in std_logic_vector(7 downto 0) := (others => '0');
			rd : out std_logic_vector(7 downto 0) := (others => '0');
			wren : in std_logic := '0';
			rden : in std_logic := '0';
			
			regval : out std_logic_vector(7 downto 0) := (others => '0')
		);
	end component;

	-- counters
	signal locked_cnt : unsigned(31 downto 0) := (others => '0');
	signal synced_cnt : unsigned(31 downto 0) := (others => '0');
	signal dec_err_cnt : unsigned(31 downto 0) := (others => '0');
	signal disp_err_cnt : unsigned(31 downto 0) := (others => '0');
	signal frame_err_cnt : unsigned(31 downto 0) := (others => '0');

	-- save old values for edge detection
	signal locked_reg : std_logic := '1';
	signal synced_reg : std_logic := '1';

	-- temporary register for counter readout
	signal tmpreg : std_logic_vector(31 downto 0) := (others => '0');

	-- control register bits
	signal control : std_logic_vector(7 downto 0) := (others => '0');
	signal counter_select : std_logic_vector(2 downto 0) := "000";
	signal counter_reset : std_logic := '0';
	signal counter_all_reset : std_logic := '0';

	-- counter mask
	signal mask : std_logic_vector(7 downto 0) := "11111111";
begin

-- save old values
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		locked_reg <= '1';
		synced_reg <= '1';
	else
		locked_reg <= locked;
		synced_reg <= synced;
	end if;
end process;

-- counter readout
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		tmpreg <= (others => '0');
	else
		if count_wren = '1' then
			case count_wr(2 downto 0) is
				when "000" => tmpreg <= std_logic_vector(locked_cnt);
				when "001" => tmpreg <= std_logic_vector(synced_cnt);
				when "010" => tmpreg <= std_logic_vector(dec_err_cnt);
				when "011" => tmpreg <= std_logic_vector(disp_err_cnt);
				when "100" => tmpreg <= std_logic_vector(frame_err_cnt);
				when others => tmpreg <= (others => '0');
			end case;
		end if;

		if count_rden = '1' then
			tmpreg <= tmpreg(23 downto 0) & x"00";
		end if;
	end if;
end process;
count_rd <= tmpreg(31 downto 24);

-- counters
process begin
	wait until rising_edge(clk);

	if (reset = '1') or (counter_all_reset = '1') or ((counter_reset = '1') and (counter_select = "000")) then
		locked_cnt <= (others => '0');
	else
		if (locked = '0') and (locked_reg = '1') and mask(0) = '1' then
			locked_cnt <= locked_cnt + 1;
		end if;		
	end if;
end process;

process begin
	wait until rising_edge(clk);

	if (reset = '1') or (counter_all_reset = '1') or ((counter_reset = '1') and (counter_select = "001")) then
		synced_cnt <= (others => '0');
	else
		if (synced = '0') and (synced_reg = '1') and mask(1) = '1' then
			synced_cnt <= synced_cnt + 1;
		end if;		
	end if;
end process;

process begin
	wait until rising_edge(clk);

	if (reset = '1') or (counter_all_reset = '1') or ((counter_reset = '1') and (counter_select = "010")) then
		dec_err_cnt <= (others => '0');
	else
		if (dec_err = '1') and (decdisp_valid = '1') and (mask(2) = '1') then
			dec_err_cnt <= dec_err_cnt + 1;
		end if;		
	end if;
end process;

process begin
	wait until rising_edge(clk);

	if (reset = '1') or (counter_all_reset = '1') or ((counter_reset = '1') and (counter_select = "011")) then
		disp_err_cnt <= (others => '0');
	else
		if (disp_err = '1') and (decdisp_valid = '1') and (mask(3) = '1') then
			disp_err_cnt <= disp_err_cnt + 1;
		end if;		
	end if;
end process;

process begin
	wait until rising_edge(clk);

	if (reset = '1') or (counter_all_reset = '1') or ((counter_reset = '1') and (counter_select = "100")) then
		frame_err_cnt <= (others => '0');
	else
		if (frame_err = '1') and mask(4) = '1' then
			frame_err_cnt <= frame_err_cnt + 1;
		end if;		
	end if;
end process;

-- mask register
mask_reg: SimpleReg
	generic map (
		rst_value => x"FF"
	)
	port map (
		clk_i => clk,
		rst_i => reset,
		wr => mask_wr,
		rd => mask_rd,
		wren => mask_wren,
		rden => mask_rden,
		regval => mask
	);

-- control register
ctrl_reg: SimpleReg
	generic map (
		rst_value => x"00"
	)
	port map (
		clk_i => clk,
		rst_i => reset,
		wr => control_wr,
		rd => control_rd,
		wren => control_wren,
		rden => control_rden,
		regval => control
	);

-- bit mapping in the control register
counter_select <= control(2 downto 0);
counter_reset <= control(3);
counter_all_reset <= control(4);

end architecture ; -- rtl