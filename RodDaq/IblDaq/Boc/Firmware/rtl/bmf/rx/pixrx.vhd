library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.ALL;

entity pixrx is
	port (
		-- clock inputs
		clk_serial : in std_logic;
		clk_serial8x : in std_logic;
		clk_serial8x_serdesstrobe : in std_logic;
		clk_backplane : in std_logic;
		clk_regbank : in std_logic;

		-- reset input
		reset : in std_logic;

		-- data inputs
		din_optical : in std_logic_vector(23 downto 0);

		-- data outputs to the backplane
		dout : out std_logic_vector(47 downto 0);

		-- XC serial data for emulator
		xc_vec : in std_logic_vector(15 downto 0);

		-- register interface
		rxregs_wr : in RegBank_wr_t(0 to 1023);
		rxregs_rd : out RegBank_rd_t(0 to 1023);
		rxregs_wren : in RegBank_wren_t(0 to 1023);
		rxregs_rden : in RegBank_rden_t(0 to 1023);

		-- phase registers
		phase_wr : IN RegBank_wr_t(0 to 47);
		phase_rd : OUT RegBank_rd_t(0 to 47);
		phase_wren : IN RegBank_wren_t(0 to 47);
		phase_rden : IN RegBank_rden_t(0 to 47);

		-- speed register
		speed_wr : in std_logic_vector(7 downto 0);
		speed_rd : out std_logic_vector(7 downto 0);
		speed_wren : in std_logic;
		speed_rden : in std_logic;

		-- broadcast mask
		bcast_enable : in std_logic_vector(15 downto 0);

		-- rx debugging
		rxdebug_select : in std_logic_vector(3 downto 0);
		rxdebug_slow : out std_logic_vector(24 downto 0);
		rxdebug_fast : out std_logic_vector(50 downto 0)
	);
end pixrx;

architecture rtl of pixrx is

-- phase alignment
component pix_phasealign
	port (
		-- clocking
		clk_serial : in std_logic;
		clk_serial8x : in std_logic;
		clk_serial8x_serdesstrobe : in std_logic;

		-- reset
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- data output (2 bits for DDR, if speed = 80 Mbit/s)
		dout : out std_logic_vector(1 downto 0);

		-- phase selection
		phase : in std_logic_vector(2 downto 0);
		delay : in std_logic_vector(7 downto 0);
		delay_ready : out std_logic;
		delay_busy : out std_logic;

		-- swap sampling
		swap_sampling : in std_logic;

		-- IODELAY2 debug
		iodelay_cal_ext : in std_logic;
		iodelay_rst_ext : in std_logic;
		debug : out std_logic_vector(21 downto 0)
	);
end component;

-- RX channel
component bmf_pix_rx
	generic (
		DEFAULT_MASTER : integer := 0;
		DEFAULT_SLAVE : integer := 0;
		DEFAULT_XC : integer := 0
	);
	port (
		-- clocks
		clk_regbank : IN std_logic;
		clk_serial : IN std_logic;

		-- reset input
		reset : IN std_logic;
		
		-- data inputs
		din : in pix_align_data_t;

		-- data output
		dout : out std_logic_vector(3 downto 0);

		-- speed selection (00/11: 40, 01: 80, 10: 160)
		speed : in std_logic_vector(1 downto 0);

		-- XC lines (to the emulator)
		xc_vec : in std_logic_vector(15 downto 0);

		-- Registers
		reg_wr : IN RegBank_wr_t(0 to 31);
		reg_rd : OUT RegBank_rd_t(0 to 31);
		reg_wren : IN RegBank_wren_t(0 to 31);
		reg_rden : IN RegBank_rden_t(0 to 31);

		-- broadcast
		bcast_wr : in RegBank_wr_t(0 to 31);
		bcast_wren : in RegBank_wren_t(0 to 31);
		bcast_enable : in std_logic;

		-- debugging
		debug_fast : out std_logic_vector(50 downto 0);
		debug_slow : out std_logic_vector(24 downto 0)
	);
end component;

-- data from phase alignment
signal align_data : pix_align_data_t := (others => (others => '0'));
signal align_phase : pix_align_phase_t := (others => (others => '0'));
signal align_delay : pix_align_delay_t := (others => (others => '0'));

-- data output from the 16 RX channels
type rx_dout_t is array (0 to 15) of std_logic_vector(3 downto 0);
signal rx_dout : rx_dout_t := (others => (others => '0'));

-- chipscope debugging signals
type rxdebug_fast_t is array (0 to 15) of std_logic_vector(50 downto 0);
type rxdebug_slow_t is array (0 to 15) of std_logic_vector(24 downto 0);
signal rxdebug_slow_all : rxdebug_slow_t := (others => (others => '0'));
signal rxdebug_fast_all : rxdebug_fast_t := (others => (others => '0'));

-- IODELAY debug
type debug_iodelay_t is array (0 to 23) of std_logic_vector(21 downto 0);
signal debug_iodelay : debug_iodelay_t := (others => (others => '0'));
signal iodelay_cal_ext : std_logic_vector(23 downto 0) := (others => '0');
signal iodelay_rst_ext : std_logic_vector(23 downto 0) := (others => '0');
signal swap_sampling : std_logic_vector(23 downto 0) := (others => '0');
signal delay_ready : std_logic_vector(23 downto 0) := (others => '0');
signal delay_busy : std_logic_vector(23 downto 0) := (others => '0');

-- speed selection
signal speed : std_logic_vector(1 downto 0) := "00";

-- mapping selection
signal map160 : std_logic := '0';

begin

-- each fibre will get its own phase aligner
align_generate: for I in 0 to 23 generate
	process begin
		wait until rising_edge(clk_regbank);

		iodelay_cal_ext(I) <= '0';
		iodelay_rst_ext(I) <= '0';

		if reset = '1' then
			align_phase(I) <= (others => '0');
			align_delay(I) <= (others => '0');
		elsif phase_wren(2*I) = '1' then
			align_phase(I) <= phase_wr(2*I)(2 downto 0);
			swap_sampling(I) <= phase_wr(2*I)(3);
			iodelay_cal_ext(I) <= phase_wr(2*I)(7);
			iodelay_rst_ext(I) <= phase_wr(2*I)(6);
		elsif phase_wren(2*I+1) = '1' then
			align_delay(I) <= phase_wr(2*I+1);
		end if;
	end process;
	phase_rd(2*I)(2 downto 0) <= align_phase(I);
	phase_rd(2*I)(3) <= swap_sampling(I);
	phase_rd(2*I)(5) <= delay_ready(I);
	phase_rd(2*I)(4) <= delay_busy(I);
	phase_rd(2*I)(7 downto 6) <= (others => '0');
	phase_rd(2*I+1) <= align_delay(I);

	align_i: pix_phasealign
		port map (
			clk_serial => clk_serial,
			clk_serial8x => clk_serial8x,
			clk_serial8x_serdesstrobe => clk_serial8x_serdesstrobe,
			reset => reset,
			din => din_optical(I),
			dout => align_data(I),
			phase => align_phase(I),
			delay => align_delay(I),
			delay_ready => delay_ready(I),
			delay_busy => delay_busy(I),
			swap_sampling => swap_sampling(I),
			iodelay_cal_ext => iodelay_cal_ext(I),
			iodelay_rst_ext => iodelay_rst_ext(I),
			debug => debug_iodelay(I)
		);
end generate;

-- generate RX channels
rxchA_generate: for I in 0 to 7 generate
	rxch_i: bmf_pix_rx
		generic map (
			DEFAULT_MASTER => 2+I,
			DEFAULT_SLAVE => 14+I,
			DEFAULT_XC => I
		)
		port map (
			clk_regbank => clk_regbank,
			clk_serial => clk_serial,
			reset => reset,
			din => align_data,
			dout => rx_dout(I),
			speed => speed,
			xc_vec => xc_vec,
			reg_wr => rxregs_wr(32*I to 32*I+31),
			reg_rd => rxregs_rd(32*I to 32*I+31),
			reg_wren => rxregs_wren(32*I to 32*I+31),
			reg_rden => rxregs_rden(32*I to 32*I+31),
			bcast_enable => bcast_enable(I),
			bcast_wr => rxregs_wr(32*16 to 32*16+31),			-- "magic" channel 16 as broadcast
			bcast_wren => rxregs_wren(32*16 to 32*16+31),
			debug_fast => rxdebug_fast_all(I),
			debug_slow => rxdebug_slow_all(I)
		);
end generate;

rxchB_generate: for I in 8 to 15 generate
	rxch_i: bmf_pix_rx
		generic map (
			DEFAULT_MASTER => 6+I,
			DEFAULT_SLAVE => 0,
			DEFAULT_XC => I
		)
		port map (
			clk_regbank => clk_regbank,
			clk_serial => clk_serial,
			reset => reset,
			din => align_data,
			dout => rx_dout(I),
			speed => speed,
			xc_vec => xc_vec,
			reg_wr => rxregs_wr(32*I to 32*I+31),
			reg_rd => rxregs_rd(32*I to 32*I+31),
			reg_wren => rxregs_wren(32*I to 32*I+31),
			reg_rden => rxregs_rden(32*I to 32*I+31),
			bcast_enable => bcast_enable(I),
			bcast_wr => rxregs_wr(32*16 to 32*16+31),			-- "magic" channel 16 as broadcast
			bcast_wren => rxregs_wren(32*16 to 32*16+31),
			debug_fast => rxdebug_fast_all(I),
			debug_slow => rxdebug_slow_all(I)
		);
end generate;

-- map backplane signals
process begin
	wait until rising_edge(clk_serial);

	-- default
	dout <= (others => '0');

	-- in 40/80 mbit/s mode simply map all channels to the backplane
	if speed /= "10" then
		-- map RX 0...15 to the backplane
		for I in 0 to 15 loop
                	dout(2*I+1 downto 2*I) <= rx_dout(I)(1 downto 0);
	        end loop;
	else
		if map160 = '1' then
			-- this will map RX 8...15 to the backplane
                        for I in 0 to 7 loop
                                dout(2*I+1 downto 2*I) <= rx_dout(I+8)(1 downto 0);
                        end loop;
                        for I in 8 to 15 loop
                                dout(2*I+1 downto 2*I) <= rx_dout(I)(3 downto 2);
                        end loop;
		else
			-- this will map RX 0...7 to the backplane
			for I in 0 to 7 loop
		                dout(2*I+1 downto 2*I) <= rx_dout(I)(1 downto 0);
		        end loop;
			for I in 8 to 15 loop
        	                dout(2*I+1 downto 2*I) <= rx_dout(I-8)(3 downto 2);
	                end loop;
		end if;
	end if;
end process;

-------------------------------
-- speed (global)
-------------------------------
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		speed <= "00";
		map160 <= '0';
	elsif speed_wren = '1' then
		speed <= speed_wr(1 downto 0);
		map160 <= speed_wr(7);
	end if;
end process;
speed_rd <= map160 & "00000" & speed;

-------------------------------
-- chipscope signals selector
-------------------------------
process begin
	wait until rising_edge(clk_serial);
	rxdebug_fast <= rxdebug_fast_all(to_integer(unsigned(rxdebug_select)));
end process;
process begin
	wait until rising_edge(clk_regbank);
	rxdebug_slow <= rxdebug_slow_all(to_integer(unsigned(rxdebug_select)));
end process;

end architecture;

