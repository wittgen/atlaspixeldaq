-- CDR with SERDES
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.bocpack.all;

entity cdr_slowrx is
	port
	(
		-- clocks
		clk : in std_logic;

		-- reset
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- data output
		data : out CDR_record
	);
end cdr_slowrx;

architecture rtl of cdr_slowrx is
	signal subsample_cnt : integer range 0 to 3 := 0;
	signal locked_cnt : integer range 0 to 127 := 0;
	signal din_reg : std_logic := '0';
	signal din_reg2 : std_logic := '0';

	attribute IOB : string;
	attribute IOB of din_reg : signal is "true";
begin

-- synchronize data
din_reg <= din when rising_edge(clk);
din_reg2 <= din_reg when rising_edge(clk);

-- edge detection and sampling
process begin
	wait until rising_edge(clk);

	data.valid <= "00";

	if reset = '1' then
		data.lock <= '0';
		data.value <= "00";
	else
		if (din_reg xor din_reg2) = '1' then
			-- we got an edge :)
			locked_cnt <= 127;
			subsample_cnt <= 0;
			data.lock <= '1';
		elsif locked_cnt > 0 then
			if subsample_cnt = 1 then
				data.valid <= "01";
				data.value <= '0' & din_reg2;
			end if;

			-- count up
			if subsample_cnt = 3 then
				subsample_cnt <= 0;
			else
				subsample_cnt <= subsample_cnt + 1;
			end if;

			-- count down
			locked_cnt <= locked_cnt - 1;
		elsif locked_cnt = 0 then
			data.lock <= '0';
		end if;
	end if;
end process;

end architecture;
