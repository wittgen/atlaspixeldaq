library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity pix_phasealign is
	port (
		-- clocking
		clk_serial : in std_logic;
		clk_serial8x : in std_logic;
		clk_serial8x_serdesstrobe : in std_logic;

		-- reset
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- data output (2 bits for DDR, if speed = 80 Mbit/s)
		dout : out std_logic_vector(1 downto 0);

		-- phase selection
		phase : in std_logic_vector(2 downto 0);
		delay : in std_logic_vector(7 downto 0);
		delay_ready : out std_logic;
		delay_busy : out std_logic;

		-- swap sampling
		swap_sampling : in std_logic;

		-- IODELAY2 debug
		iodelay_cal_ext : in std_logic;
		iodelay_rst_ext : in std_logic;
		debug : out std_logic_vector(21 downto 0)
	);
end pix_phasealign;

architecture rtl of pix_phasealign is
	signal serdes_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal serdes_cascade : std_logic := '0';
	signal shiftreg : std_logic_vector(11 downto 0) := (others => '0');

	signal iodelay_busy : std_logic := '0';
	signal iodelay_dout : std_logic := '0';
	signal iodelay_cal : std_logic := '0';
	signal iodelay_ce : std_logic := '0';
	signal iodelay_inc : std_logic := '0';
	signal iodelay_rst : std_logic := '0';

	type iodelay_init_fsm is (st_busywait0, st_cal, st_busywait1, st_rst, st_busywait2, st_idle, st_incdec);
	signal Z_iodelay : iodelay_init_fsm := st_busywait0;

	signal delay_i : std_logic_vector(7 downto 0) := (others => '0');
	signal delay_ready_i : std_logic := '0';
	signal delay_timeout : integer range 0 to 31 := 0;
begin

---------------------------------
-- IODELAY2 for fine adjustment
---------------------------------
iodelay_master: IODELAY2
	generic map (
		COUNTER_WRAPAROUND => "STAY_AT_LIMIT", 	-- "STAY_AT_LIMIT" or "WRAPAROUND"
		DATA_RATE => "SDR",						-- "SDR" or "DDR"
		DELAY_SRC => "IDATAIN",					-- "IO", "ODATAIN" or "IDATAIN"
		IDELAY2_VALUE => 0,						-- Delay value when IDELAY_MODE="PCI" (0-255)
		IDELAY_MODE => "NORMAL",				-- "NORMAL" or "PCI"
		IDELAY_TYPE => "VARIABLE_FROM_ZERO",	-- "FIXED", "DEFAULT", "VARIABLE_FROM_ZERO", "VARIABLE_FROM_HALF_MAX"
												-- or "DIFF_PHASE_DETECTOR"
		IDELAY_VALUE => 0,						-- Amount of taps for fixed input delay (0-255)
		ODELAY_VALUE => 0,						-- Amount of taps fixed output delay (0-255)
		SERDES_MODE => "NONE",				-- "NONE", "MASTER" or "SLAVE"
		SIM_TAPDELAY_VALUE => 35				-- Per tap delay used for simulation in ps
	)
	port map (
		BUSY => iodelay_busy,			-- 1-bit output: Busy output after CAL
		DATAOUT => iodelay_dout,			-- 1-bit output: Delayed data output to ISERDES/input register
		DATAOUT2 => open,						-- 1-bit output: Delayed data output to general FPGA fabric
		DOUT => open,							-- 1-bit output: Delayed data output
		TOUT => open,							-- 1-bit output: Delayed 3-state output
		CAL => iodelay_cal,						-- 1-bit input: Initiate calibration input
		CE => iodelay_ce,						-- 1-bit input: Enable INC input
		CLK => clk_serial,						-- 1-bit input: Clock input
		IDATAIN => din,							-- 1-bit input: Data input (connect to top-level port or I/O buffer)
		INC => iodelay_inc,						-- 1-bit input: Increment / decrement input
		IOCLK0 => clk_serial8x,					-- 1-bit input: Input from the I/O clock network
		IOCLK1 => '0',							-- 1-bit input: Input from the I/O clock network
		ODATAIN => '0',							-- 1-bit input: Output data input from output register or OSERDES2.
		RST => iodelay_rst,						-- 1-bit input: Reset to zero or 1/2 of total delay period
		T => '0'								-- 1-bit input: 3-state input signal
	);

----------------------------------
-- IODELAY control state machine
----------------------------------
process begin
	wait until rising_edge(clk_serial);

	-- default
	iodelay_cal <= '0';
	iodelay_rst <= '0';
	iodelay_inc <= '0';
	iodelay_ce <= '0';

	if reset = '1' then
		iodelay_rst <= '1';
		delay_i <= (others => '0');
		Z_iodelay <= st_busywait0;
		delay_timeout <= 0;
	else
		case Z_iodelay is
			when st_busywait0 =>
				if (iodelay_busy = '0') then
					Z_iodelay <= st_cal;
				end if;

			when st_cal =>
				iodelay_cal <= '1';
				if (iodelay_busy = '1') then
					Z_iodelay <= st_busywait1;
				end if;

			when st_busywait1 =>
				if (iodelay_busy = '0') then
					Z_iodelay <= st_rst;
				end if;

			when st_rst =>
				iodelay_rst <= '1';
				delay_i <= (others => '0');
				if (iodelay_busy = '1') then
					Z_iodelay <= st_busywait2;
				end if;

			when st_busywait2 =>
				if (iodelay_busy = '0') then
					Z_iodelay <= st_idle;
				end if;

			when st_idle =>
				-- set new delay
				if (unsigned(delay) > unsigned(delay_i)) then
					-- increment delay
					iodelay_inc <= '1';
					iodelay_ce <= '1';
					delay_i <= std_logic_vector(unsigned(delay_i)+1);
					Z_iodelay <= st_incdec;
				elsif (unsigned(delay) < unsigned(delay_i)) then
					-- decrement delay
					iodelay_inc <= '0';
					iodelay_ce <= '1';
					delay_i <= std_logic_vector(unsigned(delay_i)-1);
					Z_iodelay <= st_incdec;
				end if;

				-- check for external calibration request
				if (iodelay_cal_ext = '1') then
					Z_iodelay <= st_cal;
				end if;

				-- check for external reset request
				if (iodelay_rst_ext = '1') then
					Z_iodelay <= st_rst;
				end if;

			when st_incdec =>
				-- check for busy or timeout
				if iodelay_busy = '1' then
					Z_iodelay <= st_busywait2;
					delay_timeout <= 0;
				else
					if delay_timeout = 31 then
						delay_timeout <= 0;
						Z_iodelay <= st_rst;
					else
						delay_timeout <= delay_timeout + 1;
					end if;
				end if;

				-- check for external calibration request
				if (iodelay_cal_ext = '1') then
					Z_iodelay <= st_cal;
				end if;

				-- check for external reset request
				if (iodelay_rst_ext = '1') then
					Z_iodelay <= st_rst;
				end if;
		end case;
	end if;
end process;

-- generate ready bit
delay_ready_i <= '1' when (Z_iodelay = st_idle) and (delay_i = delay) else '0';
delay_ready <= delay_ready_i;
delay_busy <= iodelay_busy;

-----------------------------
-- SERDES for oversampling
-----------------------------
serdes_master: ISERDES2
	generic map (
		BITSLIP_ENABLE => FALSE,        -- Enable Bitslip Functionality (TRUE/FALSE)
		DATA_RATE => "SDR",				-- Data-rate ("SDR" or "DDR")
		DATA_WIDTH => 8,				-- Parallel data width selection (2-8)
		INTERFACE_TYPE => "RETIMED",	-- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED"
		SERDES_MODE => "MASTER"			-- "NONE", "MASTER" or "SLAVE"
	)
	port map (
		CFB0 => open,				-- 1-bit output: Clock feed-through route output
		CFB1 => open,				-- 1-bit output: Clock feed-through route output
		DFB => open,				-- 1-bit output: Feed-through clock output
		FABRICOUT => open,			-- 1-bit output: Unsynchrnonized data output
		INCDEC => open,				-- 1-bit output: Phase detector output
		
		-- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
		Q1 => serdes_dout(4),
		Q2 => serdes_dout(5),
		Q3 => serdes_dout(6),
		Q4 => serdes_dout(7),

		SHIFTOUT => serdes_cascade,	-- 1-bit output: Cascade output signal for master/slave I/O
		VALID => open,				-- 1-bit output: Output status of the phase detector
		BITSLIP => '0',				-- 1-bit input: Bitslip enable input
		CE0 => '1',					-- 1-bit input: Clock enable input
		CLK0 => clk_serial8x,		-- 1-bit input: I/O clock network input
		CLK1 => '0',				-- 1-bit input: Secondary I/O clock network input
		CLKDIV => clk_serial,      	-- 1-bit input: FPGA logic domain clock input
		D => iodelay_dout,	-- 1-bit input: Input data
		IOCE => clk_serial8x_serdesstrobe,	-- 1-bit input: Data strobe input
		RST => reset,				-- 1-bit input: Asynchronous reset input
		SHIFTIN => '0'				-- 1-bit input: Cascade input signal for master/slave I/O
	);

serdes_slave: ISERDES2
	generic map (
		BITSLIP_ENABLE => FALSE,        -- Enable Bitslip Functionality (TRUE/FALSE)
		DATA_RATE => "SDR",				-- Data-rate ("SDR" or "DDR")
		DATA_WIDTH => 8,				-- Parallel data width selection (2-8)
		INTERFACE_TYPE => "RETIMED",	-- "NETWORKING", "NETWORKING_PIPELINED" or "RETIMED"
		SERDES_MODE => "SLAVE"			-- "NONE", "MASTER" or "SLAVE"
	)
	port map (
		CFB0 => open,				-- 1-bit output: Clock feed-through route output
		CFB1 => open,				-- 1-bit output: Clock feed-through route output
		DFB => open,				-- 1-bit output: Feed-through clock output
		FABRICOUT => open,			-- 1-bit output: Unsynchrnonized data output
		INCDEC => open,				-- 1-bit output: Phase detector output
		
		-- Q1 - Q4: 1-bit (each) output: Registered outputs to FPGA logic
		Q1 => serdes_dout(0),
		Q2 => serdes_dout(1),
		Q3 => serdes_dout(2),
		Q4 => serdes_dout(3),
		
		SHIFTOUT => open,			-- 1-bit output: Cascade output signal for master/slave I/O
		VALID => open,				-- 1-bit output: Output status of the phase detector
		BITSLIP => '0',				-- 1-bit input: Bitslip enable input
		CE0 => '1',					-- 1-bit input: Clock enable input
		CLK0 => clk_serial8x,		-- 1-bit input: I/O clock network input
		CLK1 => '0',				-- 1-bit input: Secondary I/O clock network input
		CLKDIV => clk_serial,      	-- 1-bit input: FPGA logic domain clock input
		D => '0',					-- 1-bit input: Input data
		IOCE => clk_serial8x_serdesstrobe,	-- 1-bit input: Data strobe input
		RST => reset,				-- 1-bit input: Asynchronous reset input
		SHIFTIN => serdes_cascade	-- 1-bit input: Cascade input signal for master/slave I/O
	);

-----------------------------------------------
-- shift register for delay / phase selection
-----------------------------------------------
process begin
	wait until rising_edge(clk_serial);
	shiftreg <= serdes_dout & shiftreg(11 downto 8);
end process;

-----------------------------------
-- output data
-----------------------------------
process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		dout <= (others => '0');
	else
		if swap_sampling = '1' then
			dout(0) <= shiftreg(11-to_integer(unsigned(phase)));
			dout(1) <= shiftreg(7-to_integer(unsigned(phase)));
		else
			dout(0) <= shiftreg(7-to_integer(unsigned(phase)));
			dout(1) <= shiftreg(11-to_integer(unsigned(phase)));
		end if;
	end if;
end process;

---------------------------------
-- debugging
---------------------------------
debug(7 downto 0) <= serdes_dout;
debug(15 downto 8) <= delay_i;
debug(16) <= iodelay_ce;
debug(17) <= iodelay_inc;
debug(18) <= iodelay_cal;
debug(19) <= iodelay_busy;
debug(20) <= iodelay_rst;
debug(21) <= delay_ready_i;


end architecture;
