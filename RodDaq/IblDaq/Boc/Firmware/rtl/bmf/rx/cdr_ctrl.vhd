library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cdr_ctrl is
	port (
		clk : in std_logic;
		reset : in std_logic;

		-- CDR
		cdr_locked : in std_logic;
		cdr_phase_current : in std_logic_vector(1 downto 0);
		cdr_phase_select : out std_logic_vector(1 downto 0);
		cdr_phase_train : out std_logic;
		cdr_phase_override : out std_logic;
		cdr_reset : out std_logic;
		cdr_countera : in std_logic_vector(7 downto 0);
		cdr_counterb : in std_logic_vector(7 downto 0);
		cdr_counterc : in std_logic_vector(7 downto 0);
		cdr_counterd : in std_logic_vector(7 downto 0);

		-- external phase train from RX
		rx_phasetrain : in std_logic;

		-- register
		reg0_rd : out std_logic_vector(7 downto 0);
		reg0_wr : in std_logic_vector(7 downto 0);
		reg0_rden : in std_logic;
		reg0_wren : in std_logic;
		reg1_rd : out std_logic_vector(7 downto 0);
		reg1_wr : in std_logic_vector(7 downto 0);
		reg1_rden : in std_logic;
		reg1_wren : in std_logic
	);
end cdr_ctrl;

architecture rtl of cdr_ctrl is
	signal reg0_train : std_logic := '0';
	signal reg0_override : std_logic := '0';
	signal reg0_softreset : std_logic := '0';
	signal reg0_phase_current : std_logic_vector(1 downto 0) := "00";
	signal reg0_phase_select : std_logic_vector(1 downto 0) := "00";
	signal reg0_locked : std_logic := '0';

	signal counter_sr : std_logic_vector(31 downto 0) := (others => '0');
begin

-- register writing
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		reg0_train <= '0';
		reg0_softreset <= '0';
		reg0_override <= '0';
		reg0_phase_select <= "00";
		counter_sr <= (others => '0');
	else
		if reg0_wren = '1' then
			reg0_softreset <= reg0_wr(0);
			reg0_train <= reg0_wr(1);
			reg0_phase_select <= reg0_wr(3 downto 2);
			reg0_override <= reg0_wr(4);
		end if;

		if reg1_wren = '1' then
			counter_sr <= cdr_countera & cdr_counterb & cdr_counterc & cdr_counterd;
		end if;

		if reg1_rden = '1' then
			counter_sr <= counter_sr(23 downto 0) & x"00";
		end if;
	end if;
end process;

-- register reading
reg0_rd <= "00" & reg0_locked & reg0_override & reg0_phase_current & reg0_train & reg0_softreset;
reg1_rd <= counter_sr(31 downto 24);

-- reset mapping
cdr_reset <= reg0_softreset or reset;

-- phase training
process begin
	wait until rising_edge(clk);

	if reg0_override = '1' then
		-- manual phase select does not need training
		cdr_phase_train <= '0';
	else
		-- automatic phase select will train on ECR if activated or if train signal is sent
		cdr_phase_train <= reg0_train or rx_phasetrain;
	end if;
end process;

-- phase select and monitor
cdr_phase_select <= reg0_phase_select;
reg0_phase_current <= cdr_phase_current;

-- lock detection
reg0_locked <= cdr_locked;

-- override
cdr_phase_override <= reg0_override;

end architecture;