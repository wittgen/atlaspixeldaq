-- CDR with clock and 90deg clock
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.bocpack.all;

entity cdr_deg90 is
	port
	(
		-- clocks
		clk : in std_logic;
		clk_90 : in std_logic;

		-- reset
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- data output
		data : out CDR_record
	);
end cdr_deg90;

architecture rtl of cdr_deg90 is
	signal AZ : std_logic_vector(4 downto 0) := (others => '0');
	signal BZ : std_logic_vector(4 downto 0) := (others => '0');
	signal CZ : std_logic_vector(4 downto 0) := (others => '0');
	signal DZ : std_logic_vector(4 downto 0) := (others => '0');

	signal AAP, AAN : std_logic := '0';
	signal BBP, BBN : std_logic := '0';
	signal CCP, CCN : std_logic := '0';
	signal DDP, DDN : std_logic := '0';

	signal use_A : std_logic := '0';
	signal use_B : std_logic := '0';
	signal use_C : std_logic := '0';
	signal use_D : std_logic := '0';

	signal use_A1, use_A2 : std_logic := '0';
	signal use_B1, use_B2 : std_logic := '0';
	signal use_C1, use_C2 : std_logic := '0';
	signal use_D1, use_D2 : std_logic := '0';

	signal use_A_reg : std_logic := '0';
	signal use_B_reg : std_logic := '0';
	signal use_C_reg : std_logic := '0';
	signal use_D_reg : std_logic := '0';

	signal use_A_reg2 : std_logic := '0';
	signal use_B_reg2 : std_logic := '0';
	signal use_C_reg2 : std_logic := '0';
	signal use_D_reg2 : std_logic := '0';

	signal sdata_A : std_logic_vector(1 downto 0) := "00";
	signal sdata_B : std_logic_vector(1 downto 0) := "00";
	signal sdata_C : std_logic_vector(1 downto 0) := "00";
	signal sdata_D : std_logic_vector(1 downto 0) := "00";

	signal pipe_ce0 : std_logic := '0';
	signal pipe_ce1 : std_logic := '0';

	signal valid_int : std_logic_vector(1 downto 0) := "00";
	
	signal not_clk : std_logic := '0';
	signal not_clk_90 : std_logic := '0';

	signal lockcnt : integer range 0 to 31 := 0;

	attribute RLOC 		: string ;
	attribute IOB 		: string ;

	attribute RLOC of ff_a0 : label is "X0Y0";
	attribute IOB of ff_a0  : label is "FALSE";
	attribute RLOC of ff_a1 : label is "X0Y0";
	attribute RLOC of ff_a2 : label is "X3Y1";

	attribute RLOC of ff_b0 : label is "X1Y0";
	attribute IOB of ff_b0  : label is "FALSE";
	attribute RLOC of ff_b1 : label is "X3Y0";
	attribute RLOC of ff_b2 : label is "X3Y0";

	attribute RLOC of ff_c0 : label is "X1Y1";
	attribute IOB of ff_c0  : label is "FALSE";
	attribute RLOC of ff_c1 : label is "X2Y1";
	attribute RLOC of ff_c2 : label is "X2Y1";

	attribute RLOC of ff_d0 : label is "X0Y1";
	attribute IOB of ff_d0  : label is "FALSE";
	attribute RLOC of ff_d1 : label is "X1Y1";
	attribute RLOC of ff_d2 : label is "X2Y0";

begin

not_clk <= not clk;
not_clk_90 <= not clk_90;

ff_a0 : FDC port map(d => din,  c => clk, clr => reset, q => AZ(0));
ff_a1 : FDC port map(d => AZ(0), c => clk, clr => reset, q => AZ(1));
ff_a2 : FDC port map(d => AZ(1), c => clk, clr => reset, q => AZ(2));
ff_a3 : FDC port map(d => AZ(2), c => clk, clr => reset, q => AZ(3));

ff_b0 : FDC port map(d => din,  c => clk_90, clr => reset, q => BZ(0));
ff_b1 : FDC port map(d => bz(0), c => clk,   clr => reset, q => bz(1));
ff_b2 : FDC port map(d => bz(1), c => clk,   clr => reset, q => bz(2));
ff_b3 : FDC port map(d => bz(2), c => clk,   clr => reset, q => bz(3));	

ff_c0 : FDC port map(d => din,  c => not_clk, clr => reset, q => CZ(0));
ff_c1 : FDC port map(d => CZ(0), c => clk_90,  clr => reset, q => CZ(1));
ff_c2 : FDC port map(d => CZ(1), c => clk,    clr => reset, q => CZ(2));
ff_c3 : FDC port map(d => CZ(2), c => clk,    clr => reset, q => CZ(3));	

ff_d0 : FDC port map(d => din,  c => not_clk_90, clr => reset, q => DZ(0));
ff_d1 : FDC port map(d => DZ(0), c => not_clk,   clr => reset, q => DZ(1));
ff_d2 : FDC port map(d => DZ(1), c => clk_90,    clr => reset, q => DZ(2));
ff_d3 : FDC port map(d => DZ(2), c => clk,      clr => reset, q => DZ(3));	

process begin
	wait until rising_edge(clk);

	if reset = '1' then
		AZ(4) <= '0';
		BZ(4) <= '0';
		CZ(4) <= '0';
		DZ(4) <= '0';

		AAP <= '0'; AAN <= '0';
		BBP <= '0'; BBN <= '0';
		CCP <= '0'; CCN <= '0';
		DDP <= '0'; DDN <= '0';

		use_A1 <= '0'; use_A2 <= '0'; use_A <= '0';
		use_B1 <= '0'; use_B2 <= '0'; use_B <= '0';
		use_C1 <= '0'; use_C2 <= '0'; use_C <= '0';
		use_D1 <= '0'; use_D2 <= '0'; use_D <= '0';

		use_A_reg <= '0'; use_A_reg2 <= '0';
		use_B_reg <= '0'; use_B_reg2 <= '0';
		use_C_reg <= '0'; use_C_reg2 <= '0';
		use_D_reg <= '0'; use_D_reg2 <= '0';

		sdata_A <= "00";
		sdata_B <= "00";
		sdata_C <= "00";
		sdata_D <= "00";
		valid_int <= "00";
		
		data.value <= "00";
		data.valid <= "00";
		data.lock <= '0';

		pipe_ce0 <= '0';
		pipe_ce1 <= '0';
	else
		-- clock in the data
		AZ(4) <= AZ(3);
		BZ(4) <= BZ(3);
		CZ(4) <= CZ(3);
		DZ(4) <= DZ(3);

		-- find positive edges
		AAP <= (AZ(2) xor AZ(3)) and not AZ(2);
		BBP <= (BZ(2) xor BZ(3)) and not BZ(2);
		CCP <= (CZ(2) xor CZ(3)) and not CZ(2);
		DDP <= (DZ(2) xor DZ(3)) and not DZ(2);

		-- find negative edges
		AAN <= (AZ(2) xor AZ(3)) and AZ(2);
		BBN <= (BZ(2) xor BZ(3)) and BZ(2);
		CCN <= (CZ(2) xor CZ(3)) and CZ(2);
		DDN <= (DZ(2) xor DZ(3)) and DZ(2);

		-- decision of sampling point
		use_A1 <= (BBP and not CCP and not DDP and AAP);
		use_A2 <= (BBN and not CCN and not DDN and AAN);
		use_B1 <= (CCP and not DDP and AAP and BBP);
		use_B2 <= (CCN and not DDN and AAN and BBN);
		use_C1 <= (DDP and AAP and BBP and CCP);
		use_C2 <= (DDN and AAN and BBN and CCN);
		use_D1 <= (AAP and not BBP and not CCP and not DDP);
		use_D2 <= (AAN and not BBN and not CCN and not DDN);
		use_A <= use_A1 or use_A2;
		use_B <= use_B1 or use_B2;
		use_C <= use_C1 or use_C2;
		use_D <= use_D1 or use_D2;
		
		-- if we found an edge
		if (use_A or use_B or use_C or use_D) = '1' then
			lockcnt <= 31;
			pipe_ce0 <= '1';	-- sync marker
			pipe_ce1 <= '1';
		else
			if lockcnt = 0 then
				pipe_ce0 <= '0';
			else
				lockcnt <= lockcnt - 1;
			end if;
			pipe_ce1 <= '0';
		end if;

		-- register
		use_A_reg <= use_A;
		use_B_reg <= use_B;
		use_C_reg <= use_C;
		use_D_reg <= use_D;

		if pipe_ce1 = '1' then
			use_A_reg2 <= use_A_reg;
			use_B_reg2 <= use_B_reg;
			use_C_reg2 <= use_C_reg;
			use_D_reg2 <= use_D_reg;
		end if;

		-- collect output data
		sdata_A(0) <= AZ(4) and use_A_reg2; sdata_A(1) <= AZ(4) and use_D_reg2;
		sdata_B(0) <= BZ(4) and use_B_reg2; sdata_B(1) <= '0';
		sdata_C(0) <= CZ(4) and use_C_reg2; sdata_C(1) <= '0';
		sdata_D(0) <= DZ(4) and use_D_reg2; sdata_D(1) <= DZ(4) and use_A_reg2;

		-- ouput data if we have seen an edge
		if pipe_ce0 = '1' then
			data.value <= sdata_A or sdata_B or sdata_C or sdata_D;
		end if;

		-- data valid output
		if use_D_reg2 = '1' and use_A_reg = '1' then
			valid_int <= "00";		-- move from A to D: no valid data
		elsif use_A_reg2 = '1' and use_D_reg = '1' then
			valid_int <= "11";		-- move from D to A: 2 bits valid
		else
			valid_int <= "01";		-- only one bit is valid
		end if;
		
		if pipe_ce0 = '1' then
			data.valid <= valid_int;
		else
			data.valid <= "00";
		end if;
		
		data.lock <= pipe_ce0;
	end if;	
end process;
		
end architecture;
