library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.ALL;

entity iblrx is
	port (
		-- clock inputs
		clk_serial : in std_logic;
		clk_serial4x : in std_logic;
		clk_serial4x_serdesstrobe : in std_logic;
		clk_backplane : in std_logic;
		clk_regbank : in std_logic;

		-- reset input
		reset : in std_logic;

		-- data inputs
		din_optical : in std_logic_vector(23 downto 0);
		din_loopback : in std_logic_vector(15 downto 0);
		din_feemu : in std_logic_vector(15 downto 0);

		-- data outputs to the backplane
		dout : out std_logic_vector(47 downto 0);

		-- register interface
		rxregs_wr : in RegBank_wr_t(0 to 1023);
		rxregs_rd : out RegBank_rd_t(0 to 1023);
		rxregs_wren : in RegBank_wren_t(0 to 1023);
		rxregs_rden : in RegBank_rden_t(0 to 1023);
		allrx_ctrl : in std_logic_vector(7 downto 0);
		allrx_ctrl_wren : in std_logic;

		-- rx debugging
		rxdebug_select : in std_logic_vector(3 downto 0);
		rxdebug_slow : out std_logic_vector(24 downto 0);
		rxdebug_fast : out std_logic_vector(50 downto 0);

		-- rx multiplexer selection
		rxmux_wr : in RegBank_wr_t(0 to 7);
		rxmux_rd : out RegBank_rd_t(0 to 7);
		rxmux_wren : in RegBank_wren_t(0 to 7);
		rxmux_rden : in RegBank_rden_t(0 to 7);

		-- CDR controller registers
		cdr_wr : in RegBank_wr_t(0 to 47);
		cdr_rd : out RegBank_rd_t(0 to 47);
		cdr_wren : in RegBank_wren_t(0 to 47);
		cdr_rden : in RegBank_rden_t(0 to 47);

		-- BERT bypass
		bert_bypass : out std_logic_vector(15 downto 0);
		bert_bypass_valid : out std_logic_vector(15 downto 0)
	);
end iblrx;

architecture rtl of iblrx is

component cdr_ctrl
	port (
		clk : in std_logic;
		reset : in std_logic;

		-- CDR
		cdr_locked : in std_logic;
		cdr_phase_current : in std_logic_vector(1 downto 0);
		cdr_phase_select : out std_logic_vector(1 downto 0);
		cdr_phase_train : out std_logic;
		cdr_phase_override : out std_logic;
		cdr_reset : out std_logic;
		cdr_countera : in std_logic_vector(7 downto 0);
		cdr_counterb : in std_logic_vector(7 downto 0);
		cdr_counterc : in std_logic_vector(7 downto 0);
		cdr_counterd : in std_logic_vector(7 downto 0);

		-- external phase train from RX
		rx_phasetrain : in std_logic;

		-- register
		reg0_rd : out std_logic_vector(7 downto 0);
		reg0_wr : in std_logic_vector(7 downto 0);
		reg0_rden : in std_logic;
		reg0_wren : in std_logic;
		reg1_rd : out std_logic_vector(7 downto 0);
		reg1_wr : in std_logic_vector(7 downto 0);
		reg1_rden : in std_logic;
		reg1_wren : in std_logic
	);
end component;


component cdr_rce
    port (
        clk         : in std_logic ;            -- clock input
        clk4x       : in std_logic ;            -- fast oversampling clock
        clk4x_serdesstrobe : in std_logic;
        rdatain     : in std_logic;             -- data input
        rst         : in std_logic ;            -- reset input
        phaseOut    : out std_logic_vector(1 downto 0) ;
        phaseIn     : in std_logic_vector(1 downto 0) ;
        dataout     : out CDR_record ;          -- data output
        phaseTrain : in std_logic ;             -- Flag for calibration
        phaseOverride : in std_logic;           -- Flag for manual phase override
        countera    : out std_logic_vector(7 downto 0);
        counterb    : out std_logic_vector(7 downto 0);
        counterc    : out std_logic_vector(7 downto 0);
        counterd    : out std_logic_vector(7 downto 0)
    );
end component;

component RxMux
	port (
		clk_serial : in std_logic;
		clk_regbank : in std_logic;
		reset : in std_logic;

		-- normal channels
		optical_in : in CDR_record_array(0 to 15);

		-- to RX (as optical)
		selected_out : out CDR_record_array(0 to 15);

		-- select registers
		sel_rd : out RegBank_rd_t(0 to 7);
		sel_wr : in RegBank_wr_t(0 to 7);
		sel_rden : in RegBank_rden_t(0 to 7);
		sel_wren : in RegBank_wren_t(0 to 7)
	);
end component;

component Mux_BOC2ROD
	port (
		clk_regbank : in std_logic;
		clk_backplane : in std_logic;
		rst : in std_logic;

		-- From 8b10b decoder
		fei4_data_0 : in std_logic_vector(7 downto 0);
		fei4_data_1 : in std_logic_vector(7 downto 0);
		fei4_data_2 : in std_logic_vector(7 downto 0);
		fei4_data_3 : in std_logic_vector(7 downto 0);
		fei4_data_valid_0 : in std_logic;
		fei4_data_valid_1 : in std_logic;
		fei4_data_valid_2 : in std_logic;
		fei4_data_valid_3 : in std_logic;
		fei4_data_k_0 : in std_logic;
		fei4_data_k_1 : in std_logic;
		fei4_data_k_2 : in std_logic;
		fei4_data_k_3 : in std_logic;

		-- ROD RX data bus
		rx_rod_data : out std_logic_vector(7 downto 0);
		rx_rod_addr : out std_logic_vector(1 downto 0);
		rx_rod_valid : out std_logic;
		rx_rod_ctrl : out std_logic
	);
end component;

component bmf_ibl_rx
	Port (
		-- clocks
		clk_regbank : IN std_logic;
		clk_serial : IN std_logic;

		-- reset input
		reset : IN std_logic;
		
		-- data inputs
		optical_in : IN CDR_record;
		emulator_in : IN CDR_record;
		datatest_in : IN CDR_record;
		loopback_in : IN CDR_record;
		opt_spareA_in : IN CDR_record;
		opt_spareB_in : IN CDR_record;
		opt_spareC_in : IN CDR_record;
		opt_spareD_in : IN CDR_record;

		-- data output
		dout : OUT std_logic_vector(7 downto 0);
		valid : OUT std_logic;
		kchar : OUT std_logic;

		-- phase train
		cdr_train_phase : out std_logic;

		-- allrx control register
		allrx_ctrl : IN std_logic_vector(7 downto 0);
		allrx_ctrl_wren : IN std_logic;

		-- BERT bypass
		bert_bypass : out std_logic;
		bert_bypass_valid : out std_logic;

		-- debugging
		debug_fast : out std_logic_vector(21 downto 0);
		debug_slow : out std_logic_vector(24 downto 0);
		
		-- Registers
		reg_wr : IN RegBank_wr_t(0 to 31);
		reg_rd : OUT RegBank_rd_t(0 to 31);
		reg_wren : IN RegBank_wren_t(0 to 31);
		reg_rden : IN RegBank_rden_t(0 to 31)
	);
end component;

-- data outputs from CDR
signal cdr_optical : CDR_record_array(0 to 23);
signal cdr_optical_mux : CDR_record_array(0 to 15);
signal cdr_datatest : CDR_record;
signal cdr_loopback : CDR_record_array(0 to 15);
signal cdr_feemu : CDR_record_array(0 to 15);
signal cdr_phase_train : std_logic_vector(23 downto 0) := (others => '0');
signal cdr_phase_override : std_logic_vector(23 downto 0) := (others => '0');
signal cdr_phase_select : std_logic_vector(47 downto 0) := (others => '0');
signal cdr_phase_current : std_logic_vector(47 downto 0) := (others => '0');
signal cdr_reset : std_logic_vector(23 downto 0) := (others => '0');
signal cdr_countera : std_logic_vector(24*8-1 downto 0) := (others => '0');
signal cdr_counterb : std_logic_vector(24*8-1 downto 0) := (others => '0');
signal cdr_counterc : std_logic_vector(24*8-1 downto 0) := (others => '0');
signal cdr_counterd : std_logic_vector(24*8-1 downto 0) := (others => '0');

-- signals to ROD mux
type rodmux_rxdata_t is array (0 to 15) of std_logic_vector(7 downto 0);
signal rodmux_rxdata : rodmux_rxdata_t := (others => (others => '0'));
signal rodmux_rxkchar : std_logic_vector(15 downto 0);
signal rodmux_rxvalid : std_logic_vector(15 downto 0);

-- signals to the backplane
signal rodmux_data : Mux_BOC2ROD_data(0 to 3);
signal rodmux_addr : Mux_BOC2ROD_addr(0 to 3);
signal rodmux_ctrl : Mux_BOC2ROD_ctrl(0 to 3);
signal rodmux_valid : Mux_BOC2ROD_valid(0 to 3);

-- phase training from RX
signal rx_phasetrain : std_logic_vector(23 downto 0) := (others => '0');

-- chipscope debugging signals
type rxdebug_fast_t is array (0 to 15) of std_logic_vector(21 downto 0);
type rxdebug_slow_t is array (0 to 15) of std_logic_vector(24 downto 0);
signal rxdebug_slow_all : rxdebug_slow_t := (others => (others => '0'));
signal rxdebug_fast_all : rxdebug_fast_t := (others => (others => '0'));

begin

----------------------------------
-- CDR controllers
----------------------------------
cdr_controllers: for I in 0 to 23 generate
	cdr_ctrl_i: cdr_ctrl
		port map (
			clk => clk_regbank,
			reset => reset,
			cdr_locked => cdr_optical(I).lock,
			cdr_phase_current => cdr_phase_current(2*I+1 downto 2*I),
			cdr_phase_select => cdr_phase_select(2*I+1 downto 2*I),
			cdr_phase_train => cdr_phase_train(I),
			cdr_phase_override => cdr_phase_override(I),
			cdr_reset => cdr_reset(I),
			cdr_countera => cdr_countera(8*I+7 downto 8*I),
			cdr_counterb => cdr_counterb(8*I+7 downto 8*I),
			cdr_counterc => cdr_counterc(8*I+7 downto 8*I),
			cdr_counterd => cdr_counterd(8*I+7 downto 8*I),
			rx_phasetrain => rx_phasetrain(I),
			reg0_rd => cdr_rd(2*I),
			reg0_wr => cdr_wr(2*I),
			reg0_rden => cdr_rden(2*I),
			reg0_wren => cdr_wren(2*I),
			reg1_rd => cdr_rd(2*I+1),
			reg1_wr => cdr_wr(2*I+1),
			reg1_rden => cdr_rden(2*I+1),
			reg1_wren => cdr_wren(2*I+1)
		);
end generate;

----------------------------------
-- CDR instances & mappings
----------------------------------

cdr_optical_generate: for I in 0 to 23 generate
	cdr_optical_I: cdr_rce port map (
		clk => clk_serial,
		clk4x => clk_serial4x,
		clk4x_serdesstrobe => clk_serial4x_serdesstrobe,
		rst => reset,
		rdatain => din_optical(I),
		dataout => cdr_optical(I),
		phaseTrain => cdr_phase_train(I),
		phaseOverride => cdr_phase_override(I),
		phaseIn => cdr_phase_select(2*I+1 downto 2*I),
		phaseOut => cdr_phase_current(2*I+1 downto 2*I),
		countera => cdr_countera(8*I+7 downto 8*I),
		counterb => cdr_counterb(8*I+7 downto 8*I),
		counterc => cdr_counterc(8*I+7 downto 8*I),
		counterd => cdr_counterd(8*I+7 downto 8*I)
	);
end generate;

cdr_datatest.value <= "00";
cdr_datatest.valid <= "00";
cdr_datatest.lock <= '0';

cdr_loopback_generate: for I in 0 to 15 generate
	cdr_loopback(I).value <= '0' & din_loopback(I);
	cdr_loopback(I).valid <= "01";
	cdr_loopback(I).lock <= '1';
end generate;

cdr_feemu_generate: for I in 0 to 15 generate
	cdr_feemu(I).value <= '0' & din_feemu(I);
	cdr_feemu(I).valid <= "01";
	cdr_feemu(I).lock <= '1';
end generate;

----------------------------------
-- RxMux instance
----------------------------------
RxMux_generate: if UseRxMux = true generate
	RxMux_i: RxMux
		port map (
			clk_serial => clk_serial,
			clk_regbank => clk_regbank,
			reset => reset,
			optical_in(0 to 7) => cdr_optical(2 to 9),		-- first RX plugin (regular channels)
			optical_in(8 to 15) => cdr_optical(14 to 21),	-- second RX plugin (regular channels)
			selected_out => cdr_optical_mux,
			sel_wr => rxmux_wr,
			sel_rd => rxmux_rd,
			sel_wren => rxmux_wren,
			sel_rden => rxmux_rden
		);
end generate;

RxMuxNot_generate: if UseRxMux = false generate
	cdr_optical_mux(0 to 7) <= cdr_optical(2 to 9);
	cdr_optical_mux(8 to 15) <= cdr_optical(14 to 21);
end generate;

----------------------------------
-- RX decoding
----------------------------------
receivers0: for I in 0 to 7 generate
	bmf_rx_i: bmf_ibl_rx PORT MAP (
		clk_serial => clk_serial,
		clk_regbank => clk_regbank,
		reset => reset,
		optical_in => cdr_optical_mux(I),
		emulator_in => cdr_feemu(I),
		datatest_in => cdr_datatest,
		loopback_in => cdr_loopback(I),
		opt_spareA_in => cdr_optical(0),
		opt_spareB_in => cdr_optical(1),
		opt_spareC_in => cdr_optical(10),
		opt_spareD_in => cdr_optical(11),
		dout => rodmux_rxdata(I),
		valid => rodmux_rxvalid(I),
		kchar => rodmux_rxkchar(I),
		cdr_train_phase => rx_phasetrain(2+I),
		allrx_ctrl => allrx_ctrl,
		allrx_ctrl_wren => allrx_ctrl_wren,
		debug_fast => rxdebug_fast_all(I),
		debug_slow => rxdebug_slow_all(I),
		reg_wr => rxregs_wr(32*I to 32*I+31),
		reg_rd => rxregs_rd(32*I to 32*I+31),
		reg_wren => rxregs_wren(32*I to 32*I+31),
		reg_rden => rxregs_rden(32*I to 32*I+31),
		bert_bypass => bert_bypass(I),
		bert_bypass_valid => bert_bypass_valid(I)
	);
end generate;
receivers1: for I in 8 to 15 generate
	bmf_rx_i: bmf_ibl_rx PORT MAP (
		clk_serial => clk_serial,
		clk_regbank => clk_regbank,
		reset => reset,
		optical_in => cdr_optical_mux(I),
		emulator_in => cdr_feemu(I),
		datatest_in => cdr_datatest,
		loopback_in => cdr_loopback(I),
		opt_spareA_in => cdr_optical(12),
		opt_spareB_in => cdr_optical(13),
		opt_spareC_in => cdr_optical(22),
		opt_spareD_in => cdr_optical(23),
		dout => rodmux_rxdata(I),
		valid => rodmux_rxvalid(I),
		kchar => rodmux_rxkchar(I),
		cdr_train_phase => rx_phasetrain(6+I),
		allrx_ctrl => allrx_ctrl,
		allrx_ctrl_wren => allrx_ctrl_wren,
		debug_fast => rxdebug_fast_all(I),
		debug_slow => rxdebug_slow_all(I),
		reg_wr => rxregs_wr(32*I to 32*I+31),
		reg_rd => rxregs_rd(32*I to 32*I+31),
		reg_wren => rxregs_wren(32*I to 32*I+31),
		reg_rden => rxregs_rden(32*I to 32*I+31),
		bert_bypass => bert_bypass(I),
		bert_bypass_valid => bert_bypass_valid(I)
	);
end generate;

-- spare channels
rx_phasetrain(0) <= rx_phasetrain(2);
rx_phasetrain(1) <= rx_phasetrain(2);
rx_phasetrain(10) <= rx_phasetrain(2);
rx_phasetrain(11) <= rx_phasetrain(2);
rx_phasetrain(12) <= rx_phasetrain(14);
rx_phasetrain(13) <= rx_phasetrain(14);
rx_phasetrain(22) <= rx_phasetrain(14);
rx_phasetrain(23) <= rx_phasetrain(14);

----------------------------------
-- Multiplexer for ROD data
----------------------------------
rodmux_generate: for I in 0 to 3 generate
	rodmux_I: Mux_BOC2ROD PORT MAP (
		clk_regbank => clk_regbank,
		clk_backplane => clk_backplane,
		rst => reset,
		fei4_data_0 => rodmux_rxdata(4*I),
		fei4_data_1 => rodmux_rxdata(4*I+1),
		fei4_data_2 => rodmux_rxdata(4*I+2),
		fei4_data_3 => rodmux_rxdata(4*I+3),
		fei4_data_valid_0 => rodmux_rxvalid(4*I),
		fei4_data_valid_1 => rodmux_rxvalid(4*I+1),
		fei4_data_valid_2 => rodmux_rxvalid(4*I+2),
		fei4_data_valid_3 => rodmux_rxvalid(4*I+3),
		fei4_data_k_0 => rodmux_rxkchar(4*I),
		fei4_data_k_1 => rodmux_rxkchar(4*I+1),
		fei4_data_k_2 => rodmux_rxkchar(4*I+2),
		fei4_data_k_3 => rodmux_rxkchar(4*I+3),
		rx_rod_data => rodmux_data(I),
		rx_rod_addr => rodmux_addr(I),
		rx_rod_valid => rodmux_valid(I),
		rx_rod_ctrl => rodmux_ctrl(I)
	);
end generate;

-------------------------------
-- mapping backplane signals
-------------------------------
process begin
	wait until rising_edge(clk_backplane);

	dout(7 downto 0) <= rodmux_data(0);
	dout(35 downto 28) <= rodmux_data(1);
	dout(19 downto 12) <= rodmux_data(2);
	dout(27 downto 20) <= rodmux_data(3);

	dout(9 downto 8) <= rodmux_addr(0);
	dout(37 downto 36) <= rodmux_addr(1);
	dout(41 downto 40) <= rodmux_addr(2);
	dout(45 downto 44) <= rodmux_addr(3);

	dout(10) <= rodmux_valid(0);
	dout(38) <= rodmux_valid(1);
	dout(43) <= rodmux_valid(2);
	dout(47) <= rodmux_valid(3);

	dout(11) <= rodmux_ctrl(0);
	dout(39) <= rodmux_ctrl(1);
	dout(42) <= rodmux_ctrl(2);
	dout(46) <= rodmux_ctrl(3);
end process;

-------------------------------
-- chipscope signals selector
-------------------------------
process begin
	wait until rising_edge(clk_serial);
	rxdebug_fast(21 downto 0) <= rxdebug_fast_all(to_integer(unsigned(rxdebug_select)));
end process;
process begin
	wait until rising_edge(clk_regbank);
	rxdebug_slow <= rxdebug_slow_all(to_integer(unsigned(rxdebug_select)));
end process;
rxdebug_fast(50 downto 22) <= (others => '0');



end architecture;

