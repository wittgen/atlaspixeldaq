library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity rx_status is
	port (
		-- clocking
		clk : in std_logic;

		-- data input
		din : in std_logic_vector(7 downto 0);
		din_kchar : in std_logic;
		din_valid : in std_logic;

		-- data output
		dout : out std_logic_vector(7 downto 0);
		dout_kchar : out std_logic;
		dout_valid : out std_logic;

		-- enable status reporting
		enable : in std_logic;

		-- status inputs
		lock : in std_logic;
		sync : in std_logic;
		dec_error : in std_logic;
		disp_error : in std_logic;
		frame_error : in std_logic
	);
end entity;

architecture RTL of rx_status is
	component rx_status_fifo
	  PORT (
	    clk : IN STD_LOGIC;
	    rst : IN STD_LOGIC;
	    din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
	    wr_en : IN STD_LOGIC;
	    rd_en : IN STD_LOGIC;
	    dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
	    full : OUT STD_LOGIC;
	    empty : OUT STD_LOGIC
	  );
	end component;

	signal reset : std_logic := '0';

	signal fifo_din : std_logic_vector(8 downto 0) := (others => '0');
	signal fifo_dout : std_logic_vector(8 downto 0) := (others => '0');
	signal fifo_rden : std_logic := '0';
	signal fifo_wren : std_logic := '0';
	signal fifo_full : std_logic := '0';
	signal fifo_empty : std_logic := '0';

	signal status : std_logic_vector(5 downto 0) := (others => '0');
	signal status_reg : std_logic_vector(5 downto 0) := (others => '0');
begin

-- reset when not enabled
reset <= not enable;

-- FIFO
rx_status_fifo_i: rx_status_fifo
	port map (
		clk => clk,
		rst => reset,
		din => fifo_din,
		dout => fifo_dout,
		wr_en => fifo_wren,
		rd_en => fifo_rden,
		full => fifo_full,
		empty => fifo_empty
	);

-- put data into FIFO
fifo_din <= din_kchar & din when rising_edge(clk);
fifo_wren <= (din_valid and enable) when rising_edge(clk);

-- status vector
status <= fifo_full & lock & sync & dec_error & disp_error & frame_error when rising_edge(clk);

-- look for status changes output data
process begin
	wait until rising_edge(clk);

	-- default values
	fifo_rden <= '0';
	dout_valid <= '0';

	if enable = '0' then
		-- bypass everything if disabled
		dout <= din;
		dout_kchar <= din_kchar;
		dout_valid <= din_valid;
	else
		-- check for status changes
		if status /= status_reg then
			dout <= status(5 downto 2) & "00" & status(1 downto 0);
			dout_kchar <= '1';
			dout_valid <= '1';
		else
			dout <= fifo_dout(7 downto 0);
			dout_kchar <= fifo_dout(8);
			dout_valid <= not fifo_empty;
			fifo_rden <= '1';
		end if;

		-- store status for comparision
		status_reg <= status;
	end if;	
end process;

end architecture ; -- RTL