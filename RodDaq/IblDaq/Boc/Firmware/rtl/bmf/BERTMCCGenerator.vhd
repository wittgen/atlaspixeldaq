library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BERTMCCGenerator is
	port (
		clk_serial : in std_logic;
		reset : in std_logic;

		mcc_pattern_break : in std_logic_vector(5 downto 0);

		mcc_pattern_dout : out std_logic
	);
end entity;

architecture rtl of BERTMCCGenerator is
	constant mcc_pattern : std_logic_vector := "11101000001011100110011110000110101101011001000111001";
	signal sr : std_logic_vector(mcc_pattern'length-1 downto 0) := mcc_pattern;

	signal sr_cnt : integer range 0 to mcc_pattern'length-1 := 0;
	signal break_cnt : unsigned(11 downto 0);
	signal break : std_logic := '0';
begin

process begin
	wait until rising_edge(clk_serial);

	if reset = '1' then
		break <= '0';
		if mcc_pattern_break = "000000" then
			break_cnt <= to_unsigned(2048, break_cnt'length);
		else
			break_cnt <= unsigned('0' & mcc_pattern_break & "00000");
		end if;
		sr <= mcc_pattern;
		sr_cnt <= 0;
	else
		if break = '1' then
			if break_cnt = 0 then
				break <= '0';
				if mcc_pattern_break = "000000" then
					break_cnt <= to_unsigned(2048, break_cnt'length);
				else
					break_cnt <= unsigned('0' & mcc_pattern_break & "00000");
				end if;
			else
				break_cnt <= break_cnt - 1;
			end if;

		else
			if sr_cnt = mcc_pattern'length-1 then
				sr_cnt <= 0;
				sr <= mcc_pattern;
				break <= '1';
			else
				sr_cnt <= sr_cnt + 1;
				sr <= sr(sr'left-1 downto 0) & '0';
			end if;
		end if;
	end if;
end process;

-- output pattern
mcc_pattern_dout <= sr(sr'left) when break = '0' else '0';

end architecture;