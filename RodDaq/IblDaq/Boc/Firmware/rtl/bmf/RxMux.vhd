library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.ALL;

entity RxMux is
  port (
	clk_serial : in std_logic;
	clk_regbank : in std_logic;
	reset : in std_logic;

	-- normal channels
	optical_in : in CDR_record_array(0 to 15);

	-- to RX (as optical)
	selected_out : out CDR_record_array(0 to 15);

	-- select registers
	sel_rd : out RegBank_rd_t(0 to 7);
	sel_wr : in RegBank_wr_t(0 to 7);
	sel_rden : in RegBank_rden_t(0 to 7);
	sel_wren : in RegBank_wren_t(0 to 7)
  ) ;
end entity ; -- RxMux

architecture RTL of RxMux is

-- select register
signal select_reg : std_logic_vector(63 downto 0) := x"FEDCBA9876543210";

begin


-- cross-bar
process begin
	wait until rising_edge(clk_serial);

	for I in 0 to 15 loop
		selected_out(I) <= optical_in(to_integer(unsigned(select_reg(4*I+3 downto 4*I))));
	end loop;
end process;

-- register readback
regrd_generate: for I in 0 to 7 generate
	sel_rd(I) <= select_reg(8*I+7 downto 8*I);
end generate;

-- register write
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		select_reg <= x"FEDCBA9876543210";
	else
		for I in 0 to 7 loop
			if sel_wren(I) = '1' then
				select_reg(8*I+7 downto 8*I) <= sel_wr(I);
			end if;
		end loop;
	end if;
end process;			

end architecture ; -- RTL