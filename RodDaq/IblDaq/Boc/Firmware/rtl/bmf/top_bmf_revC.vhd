----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Marius Wensing
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				BCF top module
-- Description:			Port routing of BCF
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 05.12.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.bocpack.ALL;
use work.lscpack.all;

entity top_bmf is
	port (
		-- GPIO test outputs and pushbutton
		DATA_TEST_SE : out std_logic_vector(8 downto 0) := (others => '0');
		DATA_TEST_D_P : inout std_logic_vector(7 downto 0) := (others => '0');
		DATA_TEST_D_N : inout std_logic_vector(7 downto 0) := (others => '1');
		GPIO_pushbutton : in std_logic := '0';
		
		-- clock inputs
		MGTREFCLK0_P : in std_logic := '0';
		MGTREFCLK0_N : in std_logic := '0';
		CLK100_P : in std_logic := '0';
		CLK100_N : in std_logic := '1';
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		
		-- rx data from detector (snap 12)
		RX_DATA_S12_0_P : in std_logic_vector(11 downto 0) := (others => '0');
		RX_DATA_S12_0_N : in std_logic_vector(11 downto 0) := (others => '1');
		RX_DATA_S12_1_P : in std_logic_vector(11 downto 0) := (others => '0');
		RX_DATA_S12_1_N : in std_logic_vector(11 downto 0) := (others => '1');

		-- RX plugin control (I2C)
		SDA_S12RX0 : inout std_logic := 'Z';
		SCL_S12RX0 : inout std_logic := 'Z';
		SDA_S12RX1 : inout std_logic := 'Z';
		SCL_S12RX1 : inout std_logic := 'Z';
		
		-- Data to the ROD
		RXDATA_BOC2ROD : out std_logic_vector(47 downto 0) := (others => '0');
		
		-- bcf wishbone interface
		BOC_ADD_BCF2BMF : in std_logic_vector(11 downto 0) := (others => '0');
		BOC_DAT_BCF2BMF : inout std_logic_vector(7 downto 0) := (others => '0');
		BOC_ACK_BCF2BMF : out std_logic := '0';
		BOC_WE_BCF2BMF : in std_logic := '0';
		BOC_STB_BCF2BMF : in std_logic := '0';
		BOC_CYC_BCF2BMF : in std_logic := '0';
		
		-- connection to the TX SNAP12 - B
		TX_DATA_S12_0_P : out std_logic_vector(11 downto 0) := (others => '0');
		TX_DATA_S12_0_N : out std_logic_vector(11 downto 0) := (others => '1');
		TXEN_S12TX0 : out std_logic := '0';
		Reset_S12_TX0 : out std_logic := '0';
		TX_DATA_S12_1_P : out std_logic_vector(11 downto 0) := (others => '0');
		TX_DATA_S12_1_N : out std_logic_vector(11 downto 0) := (others => '1');
		TXEN_S12TX1 : out std_logic := '0';
		Reset_S12_TX1 : out std_logic := '0';

		-- SLINK
		SLINK0_UCLK : in std_logic;
		SLINK0_UDATA : in std_logic_vector(15 downto 0);
		SLINK0_UCTRL : in std_logic;
		SLINK0_UWE : in std_logic;
		SLINK0_URESET : in std_logic;
		SLINK0_UTEST : in std_logic;
		SLINK0_LFF : out std_logic;
		SLINK0_LDOWN : out std_logic;
		SLINK1_UCLK : in std_logic;
		SLINK1_UDATA : in std_logic_vector(15 downto 0);
		SLINK1_UCTRL : in std_logic;
		SLINK1_UWE : in std_logic;
		SLINK1_URESET : in std_logic;
		SLINK1_UTEST : in std_logic;
		SLINK1_LFF : out std_logic;
		SLINK1_LDOWN : out std_logic;

		-- QSFP connections
		QSFP_RX_P : in std_logic_vector(3 downto 0);
		QSFP_RX_N : in std_logic_vector(3 downto 0);
		QSFP_TX_P : out std_logic_vector(3 downto 0);
		QSFP_TX_N : out std_logic_vector(3 downto 0);
		QSFP_RESET_N : out std_logic;
		QSFP_LOWPWR_N : out std_logic;
		QSFP_MODSEL_N : out std_logic;
		QSFP_INT_N : in std_logic;
		QSFP_PRESENT_N : in std_logic;
		QSFP_SCL : inout std_logic;
		QSFP_SDA : inout std_logic;
		
		-- XC connections
		XC : in std_logic_vector(7 downto 0) := (others => '0')
	);
end top_bmf;

architecture Behavioral of top_bmf is

	---------------------------------
	-- Component Declaration Begin --
	---------------------------------

	component bmf_main
		port
		(
			clk_regbank : in std_logic;
			clk_tx : in std_logic;
			clk_tx_fast : in std_logic;
			clk_tx_fast_strobe : in std_logic;
			clk_rx : in std_logic;
			clk_rx_fast : in std_logic;
			clk_rx_fast_strobe : in std_logic;
			clk_backplane : in std_logic;
			clk_slink : in std_logic;
			clk_backplane_slink0 : in std_logic;
			clk_backplane_slink1 : in std_logic;
			reset : in std_logic;

			-- wishbone interface
			wb_dat_i : in std_logic_vector(7 downto 0);
			wb_dat_o : out std_logic_vector(7 downto 0);
			wb_adr : in std_logic_vector(15 downto 0);
			wb_cyc : in std_logic;
			wb_stb : in std_logic;
			wb_we : in std_logic;
			wb_ack : out std_logic;

			-- serial data from ROD
			xc : in std_logic_vector(15 downto 0);

			-- receiver
			optical_in : in std_logic_vector(23 downto 0);

			-- transmitter
			txout : out std_logic_vector(15 downto 0);

			-- SLINK
			slink0_uclk : in std_logic;
			slink0_udata : in std_logic_vector(31 downto 0);
			slink0_uctrl : in std_logic;
			slink0_uwe : in std_logic;
			slink0_ureset : in std_logic;
			slink0_utest : in std_logic;
			slink0_lff : out std_logic;
			slink0_ldown : out std_logic;
			slink1_uclk : in std_logic;
			slink1_udata : in std_logic_vector(31 downto 0);
			slink1_uctrl : in std_logic;
			slink1_uwe : in std_logic;
			slink1_ureset : in std_logic;
			slink1_utest : in std_logic;
			slink1_lff : out std_logic;
			slink1_ldown : out std_logic;

			-- QSFP
			qsfp_rx_p : in std_logic_vector(3 downto 0);
			qsfp_rx_n : in std_logic_vector(3 downto 0);
			qsfp_tx_p : out std_logic_vector(3 downto 0);
			qsfp_tx_n : out std_logic_vector(3 downto 0);
			qsfp_scl : inout std_logic;
			qsfp_sda : inout std_logic;
			qsfp_modsel_n : out std_logic;
			qsfp_lowpwr_n : out std_logic;
			qsfp_reset_n : out std_logic;
			qsfp_int_n : in std_logic;
			qsfp_present_n : in std_logic;

			-- SLINK LEDs
			slink_led : out std_logic_vector(4 downto 0);

			-- lock loss counter
			lockloss_cnt : in std_logic_vector(7 downto 0);

			-- I2C connections to RX plugins
			rx_scl : inout std_logic_vector(1 downto 0);
			rx_sda : inout std_logic_vector(1 downto 0);

			-- multiplexer outputs
			rxdata_boc2rod : out std_logic_vector(47 downto 0)
		);
	end component;

	component bmf_clocking is
		port
		(
			-- clock input
			clk40_p : in std_logic;
			clk40_n : in std_logic;
			clk100_p : in std_logic;
			clk100_n : in std_logic;
			mgtrefclk_p : in std_logic;
			mgtrefclk_n : in std_logic;

			-- reset input
			reset : in std_logic;

			-- clock outputs
			clk_regbank : out std_logic;
			clk_tx : out std_logic;
			clk_tx_fast : out std_logic;
			clk_tx_fast_strobe : out std_logic;
			clk_rx : out std_logic;
			clk_rx_fast : out std_logic;
			clk_rx_fast_strobe : out std_logic;
			clk_backplane : out std_logic;
			clk_backplane_slink0 : out std_logic;
			clk_backplane_slink1 : out std_logic;
			clk_slink : out std_logic;

			-- lock loss counter
			lockloss_cnt : out std_logic_vector(7 downto 0);

			-- status outputs
			locked : out std_logic
		);
	end component;

	component blinki
		generic
		(
			f_clk : natural := 40000000;	-- input clock frequency
			f_blinki : natural := 1 		-- output clock frequency
		);
		port
		(
			clk : IN std_logic;
			reset : IN std_logic;          
			blinki : OUT std_logic
		);
	end component;

	-------------------------------
	-- Component Declaration End --
	-------------------------------	
	
	----------------------------
	-- Internal Signals Begin --
	----------------------------
	signal clk_regbank : std_logic;
	signal not_clk_regbank : std_logic;
	signal clk_tx : std_logic;
	signal clk_tx_fast : std_logic;
	signal clk_tx_fast_strobe : std_logic;
	signal clk_rx : std_logic;
	signal clk_rx_fast : std_logic;
	signal clk_rx_fast_strobe : std_logic;
	signal clk_backplane : std_logic;
	signal clk_backplane_slink0 : std_logic;
	signal clk_backplane_slink1 : std_logic;
	signal not_clk_backplane_slink0 : std_logic;
	signal not_clk_backplane_slink1 : std_logic;
	signal clk_slink : std_logic;
	signal locked : std_logic := '0';
	signal lockloss_cnt : std_logic_vector(7 downto 0) := (others => '0');

	signal wb_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal wb_we : std_logic := '0';
	signal wb_cyc : std_logic := '0';
	signal wb_stb : std_logic := '0';
	signal wb_ack : std_logic := '0';
	
	-- rx
	signal rx_optical : std_logic_vector(23 downto 0);

	-- tx
	signal txout : std_logic_vector(15 downto 0) := (others => '0');
	
	-- reset
	signal reset : std_logic := '0';
	signal reset_sr : std_logic_vector(3 downto 0) := "1111";

	-- clock 40 output
	signal clk40_obuf: std_logic := '0';

	-- rxdata iob reg
	signal rxdata : std_logic_vector(47 downto 0) := (others => '0');
	signal rxdata_iob : std_logic_vector(47 downto 0) := (others => '0');
	attribute IOB : string;
	attribute IOB of rxdata_iob : signal is "true";

	-- XC iob
	signal xc_reg : std_logic_vector(7 downto 0) := (others => '0');
	attribute IOB of xc_reg : signal is "true";

	-- wishbone iob registers
	signal wb_dat_o_pad : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_dat_o_iob : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_dat_i_iob : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_adr_iob : std_logic_vector(11 downto 0) := (others => '0');
	signal wb_we_iob : std_logic := '0';
	signal wb_cyc_iob : std_logic := '0';
	signal wb_stb_iob : std_logic := '0';
	signal wb_ack_iob : std_logic := '0';

	signal tx_copy_reg : std_logic_vector(8 downto 0);
	signal tx_copy_reg2 : std_logic_vector(8 downto 0);
	signal tx_copy_iob : std_logic_vector(8 downto 0);
	attribute IOB of tx_copy_iob : signal is "true";
	attribute KEEP : string;
	attribute KEEP of tx_copy_reg : signal is "true";
	attribute KEEP of tx_copy_reg2 : signal is "true";
	attribute KEEP of tx_copy_iob : signal is "true";

	attribute IOB of wb_dat_o_iob : signal is "true";
	attribute IOB of wb_dat_i_iob : signal is "true";
	attribute IOB of wb_adr_iob : signal is "true";
	attribute IOB of wb_we_iob : signal is "true";
	attribute IOB of wb_cyc_iob : signal is "true";
	attribute IOB of wb_stb_iob : signal is "true";
	attribute IOB of wb_ack_iob : signal is "true";

	-- register statges for slink input signal
	-- NB uclk is not used
	signal slink0_data : std_logic_vector(31 downto 0) := (others => '0');
	signal slink1_data : std_logic_vector(31 downto 0) := (others => '0');
	
	signal SLINK0_UCTRL_r :  std_logic;
	signal SLINK0_UWE_r :  std_logic;
	signal SLINK0_URESET_r :  std_logic;
	signal SLINK0_UTEST_r :  std_logic;
	signal SLINK0_LFF_i :  std_logic;
	signal SLINK0_LDOWN_i :  std_logic;
	signal SLINK0_UCLK_i : std_logic;
	signal SLINK1_UCTRL_r :  std_logic;
	signal SLINK1_UWE_r :  std_logic;
	signal SLINK1_URESET_r :  std_logic;
	signal SLINK1_UTEST_r : std_logic;
	signal SLINK1_LFF_i :  std_logic;
	signal SLINK1_LDOWN_i :  std_logic;
	signal SLINK1_UCLK_i : std_logic;

	attribute buffer_type: string;
	attribute buffer_type of SLINK0_UCLK_i : signal is "none";
	attribute buffer_type of SLINK1_UCLK_i : signal is "none";
	
begin

-- map wishbone signals
process begin
	wait until rising_edge(clk_regbank);
	wb_dat_o_iob <= wb_dat_o;
	wb_dat_i <= wb_dat_i_iob;
	wb_adr <= "0000" & wb_adr_iob;
	wb_we <= wb_we_iob;
	wb_cyc <= wb_cyc_iob;
	wb_stb <= wb_stb_iob;
	wb_ack_iob <= wb_ack;

	wb_dat_o_pad <= wb_dat_o_iob;
	wb_dat_i_iob <= BOC_DAT_BCF2BMF;
	wb_adr_iob <= BOC_ADD_BCF2BMF;
	wb_we_iob <= BOC_WE_BCF2BMF;
	wb_stb_iob <= BOC_STB_BCF2BMF;
	wb_cyc_iob <= BOC_CYC_BCF2BMF;
	BOC_ACK_BCF2BMF <= wb_ack_iob;
end process;

BOC_DAT_BCF2BMF <= wb_dat_o_pad when BOC_WE_BCF2BMF = '0' else (others => 'Z');

-- clock generation
bmf_clocking_i: bmf_clocking
	PORT MAP(
		clk40_p => CLK40_P,
		clk40_n => CLK40_N,
		clk100_p => CLK100_P,
		clk100_n => CLK100_N,
		mgtrefclk_p => MGTREFCLK0_P,
		mgtrefclk_n => MGTREFCLK0_N,
		reset => GPIO_pushbutton,
		clk_regbank => clk_regbank,
		clk_tx => clk_tx,
		clk_tx_fast => clk_tx_fast,
		clk_tx_fast_strobe => clk_tx_fast_strobe,
		clk_rx => clk_rx,
		clk_rx_fast => clk_rx_fast,
		clk_rx_fast_strobe => clk_rx_fast_strobe,
		clk_backplane => clk_backplane,
		clk_backplane_slink0 => clk_backplane_slink0,
		clk_backplane_slink1 => clk_backplane_slink1,
		clk_slink => clk_slink,
		lockloss_cnt => lockloss_cnt,
		locked => locked
	);

-- main component
bmf_main_i: bmf_main
	PORT MAP(
		clk_regbank => clk_regbank,
		clk_tx => clk_tx,
		clk_tx_fast => clk_tx_fast,
		clk_tx_fast_strobe => clk_tx_fast_strobe,
		clk_rx => clk_rx,
		clk_rx_fast => clk_rx_fast,
		clk_rx_fast_strobe => clk_rx_fast_strobe,
		clk_backplane => clk_backplane,
		clk_slink => clk_slink,
		clk_backplane_slink0 => clk_backplane_slink0,
		clk_backplane_slink1 => clk_backplane_slink1,
		reset => reset,
		wb_dat_i => wb_dat_i,
		wb_dat_o => wb_dat_o,
		wb_adr => wb_adr,
		wb_cyc => wb_cyc,
		wb_stb => wb_stb,
		wb_we => wb_we,
		wb_ack => wb_ack,
		xc(7 downto 0) => xc_reg,
		xc(15 downto 8) => xc_reg,
		optical_in => rx_optical,
		txout => txout,
		slink0_udata => slink0_data,
		slink0_uclk => SLINK0_UCLK_i,
		slink0_uctrl => SLINK0_UCTRL_r,
		slink0_ureset => SLINK0_URESET_r,
		slink0_utest => SLINK0_UTEST_r,
		slink0_uwe => SLINK0_UWE_r,
		slink0_lff => SLINK0_LFF_i,
		slink0_ldown => SLINK0_LDOWN_i,
		slink1_udata => slink1_data,
		slink1_uclk => SLINK1_UCLK_i,
		slink1_uctrl => SLINK1_UCTRL_r,
		slink1_ureset => SLINK1_URESET_r,
		slink1_utest => SLINK1_UTEST_r,
		slink1_uwe => SLINK1_UWE_r,
		slink1_lff => SLINK1_LFF_i,
		slink1_ldown => SLINK1_LDOWN_i,
		qsfp_tx_n => QSFP_TX_N,
		qsfp_tx_p => QSFP_TX_P,
		qsfp_rx_n => QSFP_RX_N,
		qsfp_rx_p => QSFP_RX_P,
		qsfp_reset_n => QSFP_RESET_N,
		qsfp_modsel_n => QSFP_MODSEL_N,
		qsfp_lowpwr_n => QSFP_LOWPWR_N,
		qsfp_int_n => QSFP_INT_N,
		qsfp_present_n => QSFP_PRESENT_N,
		qsfp_scl => QSFP_SCL,
		qsfp_sda => QSFP_SDA,
		slink_led => DATA_TEST_SE(4 downto 0),
		lockloss_cnt => lockloss_cnt,
		rx_scl(0) => SCL_S12RX0,
		rx_scl(1) => SCL_S12RX1,
		rx_sda(0) => SDA_S12RX0,
		rx_sda(1) => SDA_S12RX1,
		rxdata_boc2rod => rxdata
	);
			
-- reset generation (from gpio-pushbutton and pll locked)
process (clk_regbank, locked, GPIO_pushbutton) begin
	if (locked = '0') or (GPIO_pushbutton = '1') then
		reset_sr <= "1111";
	else
		if rising_edge(clk_regbank) then
			reset_sr <= reset_sr(2 downto 0) & '0';
		end if;
	end if;
end process;
reset <= reset_sr(2);

-- register xc input
process begin
	wait until rising_edge(clk_tx);

	xc_reg <= XC;
end process;

-- tx iob
tx0_buffer_0: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_0_P(0),
		 OB => TX_DATA_S12_0_N(0));
tx0_buffer_1: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_0_P(1),
		 OB => TX_DATA_S12_0_N(1));
tx0_buffers: for I in 0 to 7 generate
	tx0_buffer_I: OBUFDS
		PORT MAP(I => txout(I),
			 O => TX_DATA_S12_0_P(I+2),
			 OB => TX_DATA_S12_0_N(I+2));
end generate;
tx0_buffer_10: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_0_P(10),
		 OB => TX_DATA_S12_0_N(10));
tx0_buffer_11: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_0_P(11),
		 OB => TX_DATA_S12_0_N(11));

tx1_buffer_0: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_1_P(0),
		 OB => TX_DATA_S12_1_N(0));
tx1_buffer_1: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_1_P(1),
		 OB => TX_DATA_S12_1_N(1));
tx1_buffers: for I in 0 to 7 generate
	tx1_buffer_I: OBUFDS
		PORT MAP(I => txout(I+8),
			 O => TX_DATA_S12_1_P(I+2),
			 OB => TX_DATA_S12_1_N(I+2));
end generate;
tx1_buffer_10: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_1_P(10),
		 OB => TX_DATA_S12_1_N(10));
tx1_buffer_11: OBUFDS
	PORT MAP(I => '0',
		 O => TX_DATA_S12_1_P(11),
		 OB => TX_DATA_S12_1_N(11));

-- data test headers
tx_buffer_datatest: OBUFDS
        PORT MAP(I => '0',
                O => DATA_TEST_D_P(0),
                OB => DATA_TEST_D_N(0));
txclk_buffer_datatest: OBUFDS
        PORT MAP(I => '0',
                O => DATA_TEST_D_P(1),
                OB => DATA_TEST_D_N(1));

opto_linkA: for I in 0 to 11 generate
	opto_linkA_buffers: IBUFDS
		GENERIC MAP(DIFF_TERM => TRUE,
					IBUF_LOW_PWR => FALSE)
		PORT MAP(O => rx_optical(I),
				 I => RX_DATA_S12_0_P(I),
				 IB => RX_DATA_S12_0_N(I));
end generate;

opto_linkB: for I in 12 to 23 generate
	opto_linkB_buffers: IBUFDS
		GENERIC MAP(DIFF_TERM => TRUE,
					IBUF_LOW_PWR => FALSE)
		PORT MAP(O => rx_optical(I),
				 I => RX_DATA_S12_1_P(I-12),
				 IB => RX_DATA_S12_1_N(I-12));
end generate;

-- buffers RXDATA
process begin
	wait until rising_edge(clk_backplane);

	rxdata_iob <= rxdata;
end process;
RXDATA_BOC2ROD <= rxdata_iob;

-- enable lasers
TXEN_S12TX0 <= '1';
TXEN_S12TX1 <= '1';

-- system reset as laser-reset
Reset_S12_TX0 <= not reset;
Reset_S12_TX1 <= not reset;

-- heartbeat LED
heartbeat: blinki
	GENERIC MAP (
		f_clk => 40000000,
		f_blinki => 1
	)
	PORT MAP (
		clk => clk_regbank,
		reset => reset,
		blinki => DATA_TEST_SE(8)
	);

-- PLL locked status on LED7
DATA_TEST_SE(7) <= locked;

-- clock sync on LED6
DATA_TEST_SE(6) <= '0';

-- output tx channel 0 as reference to datatest headers
txcopy_testheaders_buf: OBUFDS
	PORT MAP (
		I => '0',
		O => DATA_TEST_D_P(3),
		OB => DATA_TEST_D_N(3)
	);

not_clk_regbank <= not clk_regbank;

-- output 40 MHz clock on data test headers
clk40_ddr: ODDR2
	GENERIC MAP (
		DDR_ALIGNMENT => "NONE",
		INIT => '0',
		SRTYPE => "SYNC"
	)
	PORT MAP (
		Q => clk40_obuf,
		C0 => clk_regbank,
		C1 => not_clk_regbank,
		CE => '1',
		D0 => '0',
		D1 => '1',
		R => '0',
		S => '0'
	);

clk40_obufds: OBUFDS
	PORT MAP (
		I => clk40_obuf,
		O => DATA_TEST_D_P(7),
		OB => DATA_TEST_D_N(7)
	);

-- invert SLINK io clock
not_clk_backplane_slink0 <= not clk_backplane_slink0;
not_clk_backplane_slink1 <= not clk_backplane_slink1;

-- DDR buffer for SLINK data lines
slink_data_ddrbuf: for I in 0 to 15 generate
	slink_data0_buf_I: IDDR2
		GENERIC MAP (
			DDR_ALIGNMENT => "C0",
			INIT_Q0 => '0',
			INIT_Q1 => '0',
			SRTYPE => "ASYNC"
		)
		PORT MAP (
			Q0 => slink0_data(2*I),
			Q1 => slink0_data(2*I+1),
			C0 => clk_backplane_slink0,
			C1 => not_clk_backplane_slink0,
			CE => '1',
			D => SLINK0_UDATA(I),
			R => reset,
			S => '0'
		);

	slink_data1_buf_I: IDDR2
		GENERIC MAP (
			DDR_ALIGNMENT => "C0",
			INIT_Q0 => '0',
			INIT_Q1 => '0',
			SRTYPE => "ASYNC"
		)
		PORT MAP (
			Q0 => slink1_data(2*I),
			Q1 => slink1_data(2*I+1),
			C0 => clk_backplane_slink1,
			C1 => not_clk_backplane_slink1,
			CE => '1',
			D => SLINK1_UDATA(I),
			R => reset,
			S => '0'
		);
end generate;

-- we also need to register the SDR S-Link input and output signals
	sl0RegisterProc: process
	begin
		wait until rising_edge(clk_backplane_slink0);
			SLINK0_UCTRL_r <= SLINK0_UCTRL;
			SLINK0_UWE_r <= SLINK0_UWE;
			SLINK0_URESET_r <= SLINK0_URESET;
			SLINK0_UTEST_r <= SLINK0_UTEST;
			
			SLINK0_LFF <= SLINK0_LFF_i;
			SLINK0_LDOWN <= SLINK0_LDOWN_i;
	end process;

	sl1RegisterProc: process
	begin
		wait until rising_edge(clk_backplane_slink1);
			
			SLINK1_UCTRL_r <= SLINK1_UCTRL;
			SLINK1_UWE_r <= SLINK1_UWE;
			SLINK1_URESET_r <= SLINK1_URESET;
			SLINK1_UTEST_r <= SLINK1_UTEST;
			
			SLINK1_LFF <= SLINK1_LFF_i;
			SLINK1_LDOWN <= SLINK1_LDOWN_i;

	end process;

-- slink0_uclk_buf: IBUFG
-- 	PORT MAP (
-- 		I => SLINK0_UCLK,
-- 		O => SLINK0_UCLK_i
-- 	);

-- slink1_uclk_buf: IBUFG
-- 	PORT MAP (
-- 		I => SLINK1_UCLK,
-- 		O => SLINK1_UCLK_i
-- 	);

SLINK0_UCLK_i <= SLINK0_UCLK;
SLINK1_UCLK_i <= SLINK1_UCLK;

end Behavioral;

