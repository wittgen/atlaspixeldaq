// author: philipp schaefer

module tlk_wrapper_s6(
	input wire mgtrefclk,
	input wire gtx_reset,
	input wire lsc_rst0_n,
	input wire lsc_rst1_n,
	output wire gtx_resetdone0,
	output wire gtx_resetdone1,
	// new for sharing tile user clocks
	// one gtpclkout is used to generate all user clocks, also for neightbouring tiles
	 output wire gtpclkout,
	 output wire gtp_plldet_out,
	 input wire usr_clk_in,
	 input wire usr_clk20_in,
	// GTX serial I/O
	input wire gtx_rxn0,
	input wire gtx_rxp0,
	output wire gtx_txn0,
	output wire gtx_txp0,
	input wire gtx_rxn1,
	input wire gtx_rxp1,
	output wire gtx_txn1,
	output wire gtx_txp1,
	// clock correction
   output wire [2:0]    GTX_RXCLKCORCNT0,
   output wire [2:0]    GTX_RXCLKCORCNT1,
	// tlk2501 transmit ports
	output wire tlk_txclk0,
	input wire [15:0] tlk_txd0,
	input wire tlk_txen0,
	input wire tlk_txer0,
	// tlk2501 receive ports
	output wire tlk_rxclk0,
	output wire [15:0] tlk_rxd0,
	output wire tlk_rxdv0,
	output wire tlk_rxer0,
	output wire [57:0] dbg_tlkgtx0,
	// tlk2501 transmit ports
	output wire tlk_txclk1,
	input wire [15:0] tlk_txd1,
	input wire tlk_txen1,
	input wire tlk_txer1,
	// tlk2501 receive ports
	output wire tlk_rxclk1,
	output wire [15:0] tlk_rxd1,
	output wire tlk_rxdv1,
	output wire tlk_rxer1,
	output wire [57:0] dbg_tlkgtx1,
	// AKU additional debugging
	output wire tile0_rxrecclk0,
	output wire [1:0] tile0_rxlossofsync0,
	input wire tile0_rxreset0,
	input wire tile0_rxcdrreset0,
	input wire tile0_rxbufreset0,
	output wire tile0_rxrecclk1,
	output wire [1:0] tile0_rxlossofsync1,
	input wire tile0_rxreset1,
	input wire tile0_rxcdrreset1,
	input wire tile0_rxbufreset1
);

wire gtx_tlk_reset0;
wire gtx_tlk_reset1;
assign gtx_tlk_reset0 = !gtx_resetdone0 | gtx_reset;
assign gtx_tlk_reset1 = !gtx_resetdone1 | gtx_reset;

wire gtx_usr_clk0;
wire [15:0] gtx_rx_data0;
wire [1:0] gtx_rxcharisk0;
wire gtx_rxencommaalign0;
wire gtx_byteisaligned0;
wire [1:0] gtx_txcharisk0;
wire [15:0] gtx_tx_data0;

tlk_gtx_interface tlk_gtx_interface0_I (
	.SYS_RST(gtx_tlk_reset0),
	.GTX_RXUSRCLK2(gtx_usr_clk0),
	.GTX_RXDATA(gtx_rx_data0),
	.GTX_RXCHARISK(gtx_rxcharisk0),
	.GTX_RXENCOMMAALIGN(gtx_rxencommaalign0),
	.GTX_RXBYTEISALIGNED(gtx_byteisaligned0),
	.GTX_TXUSRCLK2(gtx_usr_clk0),
	.GTX_TXCHARISK(gtx_txcharisk0),
	.GTX_TXDATA(gtx_tx_data0),
	.TLK_TXD(tlk_txd0),
	.TLK_TXEN(tlk_txen0),
	.TLK_TXER(tlk_txer0),
	.TLK_RXD(tlk_rxd0),
	.TLK_RXDV(tlk_rxdv0),
	.TLK_RXER(tlk_rxer0),
	.LSC_RST_N(lsc_rst0_n),
	.CS_TRIG(dbg_tlkgtx0)
);

wire gtx_usr_clk1;
wire [15:0] gtx_rx_data1;
wire [1:0] gtx_rxcharisk1;
wire gtx_byteisaligned1;
wire gtx_rxencommaalign1;
wire [1:0] gtx_txcharisk1;
wire [15:0] gtx_tx_data1;

tlk_gtx_interface tlk_gtx_interface1_I (
	.SYS_RST(gtx_tlk_reset1),
	.GTX_RXUSRCLK2(gtx_usr_clk1),
	.GTX_RXDATA(gtx_rx_data1),
	.GTX_RXCHARISK(gtx_rxcharisk1),
	.GTX_RXENCOMMAALIGN(gtx_rxencommaalign1),
	.GTX_RXBYTEISALIGNED(gtx_byteisaligned1),
	.GTX_TXUSRCLK2(gtx_usr_clk1),
	.GTX_TXCHARISK(gtx_txcharisk1),
	.GTX_TXDATA(gtx_tx_data1),
	.TLK_TXD(tlk_txd1),
	.TLK_TXEN(tlk_txen1),
	.TLK_TXER(tlk_txer1),
	.TLK_RXD(tlk_rxd1),
	.TLK_RXDV(tlk_rxdv1),
	.TLK_RXER(tlk_rxer1),
	.LSC_RST_N(lsc_rst0_n),
	.CS_TRIG(dbg_tlkgtx1)
);

assign tlk_txclk0 = gtx_usr_clk0;
assign tlk_rxclk0 = gtx_usr_clk0;
assign tlk_txclk1 = gtx_usr_clk1;
assign tlk_rxclk1 = gtx_usr_clk1;



//mgt_wrapper_ak mgt_wrapper_I (
mgt_wrapper mgt_wrapper_I (
	.tile0_gtp0_refclk_i(mgtrefclk),
	.TILE0_GTP0_PLLLKDET_OUT(gtp_plldet_out),
	.gtpclkout(gtpclkout),
	.usr_clk_in(usr_clk_in),
	.usr_clk20_in(usr_clk20_in),
	.TILE0_RESET0_DONE(gtx_resetdone0),
	.TILE0_RESET1_DONE(gtx_resetdone1),
	.GTP0_RESET_IN(gtx_reset),
	.RXDATA0(gtx_rx_data0),
	.RXCHARISK0(gtx_rxcharisk0),
	.TXDATA0(gtx_tx_data0),
	.TXCHARISK0(gtx_txcharisk0),
	.GTX_RXENCOMMAALIGN0(gtx_rxencommaalign0),
	.GTX_RXBYTEISALIGNED0(gtx_byteisaligned0),
	.GTX_RXCLKCORCNT0(GTX_RXCLKCORCNT0),
	.GTX_RXCLKCORCNT1(GTX_RXCLKCORCNT1),
	.USR_CLK0(gtx_usr_clk0),
	.RXN_IN0(gtx_rxn0),
	.RXP_IN0(gtx_rxp0),
	.TXN_OUT0(gtx_txn0),
	.TXP_OUT0(gtx_txp0),
	.GTP1_RESET_IN(gtx_reset),
	.RXDATA1(gtx_rx_data1),
	.RXCHARISK1(gtx_rxcharisk1),
	.TXDATA1(gtx_tx_data1),
	.TXCHARISK1(gtx_txcharisk1),
	.GTX_RXENCOMMAALIGN1(gtx_rxencommaalign1),
	.GTX_RXBYTEISALIGNED1(gtx_byteisaligned1),
	.USR_CLK1(gtx_usr_clk1),
	.RXN_IN1(gtx_rxn1),
	.RXP_IN1(gtx_rxp1),
	.TXN_OUT1(gtx_txn1),
	.TXP_OUT1(gtx_txp1),
	// AKU debugging
	.tile0_rxrecclk0(tile0_rxrecclk0),
	.tile0_rxlossofsync0(tile0_rxlossofsync0),
	.tile0_rxreset0(tile0_rxreset0),
	.tile0_rxcdrreset0(tile0_rxcdrreset0),
	.tile0_rxbufreset0(tile0_rxbufreset0),
	.tile0_rxrecclk1(tile0_rxrecclk1),
	.tile0_rxlossofsync1(tile0_rxlossofsync1),
	.tile0_rxreset1(tile0_rxreset1),
	.tile0_rxcdrreset1(tile0_rxcdrreset1),
	.tile0_rxbufreset1(tile0_rxbufreset1)

);

endmodule // tlk_wrapper
