-------------------------------------------------------------------------------
--   ____  ____
--  /   /\/   /
-- /___/  \  /    Vendor: Xilinx
-- \   \   \/     Version : 1.11
--  \   \         Application : Spartan-6 FPGA GTP Transceiver Wizard
--  /   /         Filename : mgt_usrclk_source.vhd
-- /___/   /\      
-- \   \  /  \ 
--  \___\/\___\
--
--
-- Module MGT_USRCLK_SOURCE (for use with GTP Transceivers)
-- Generated by Xilinx Spartan-6 FPGA GTP Transceiver Wizard
-- 
-- 
-- (c) Copyright 2009 - 2011 Xilinx, Inc. All rights reserved.
-- 
-- This file contains confidential and proprietary information
-- of Xilinx, Inc. and is protected under U.S. and
-- international copyright and other intellectual property
-- laws.
-- 
-- DISCLAIMER
-- This disclaimer is not a license and does not grant any
-- rights to the materials distributed herewith. Except as
-- otherwise provided in a valid license issued to you by
-- Xilinx, and to the maximum extent permitted by applicable
-- law: (1) THESE MATERIALS ARE MADE AVAILABLE "AS IS" AND
-- WITH ALL FAULTS, AND XILINX HEREBY DISCLAIMS ALL WARRANTIES
-- AND CONDITIONS, EXPRESS, IMPLIED, OR STATUTORY, INCLUDING
-- BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, NON-
-- INFRINGEMENT, OR FITNESS FOR ANY PARTICULAR PURPOSE; and
-- (2) Xilinx shall not be liable (whether in contract or tort,
-- including negligence, or under any other theory of
-- liability) for any loss or damage of any kind or nature
-- related to, arising under or in connection with these
-- materials, including for any direct, or any indirect,
-- special, incidental, or consequential loss or damage
-- (including loss of data, profits, goodwill, or any type of
-- loss or damage suffered as a result of any action brought
-- by a third party) even if such damage or loss was
-- reasonably foreseeable or Xilinx had been advised of the
-- possibility of the same.
-- 
-- CRITICAL APPLICATIONS
-- Xilinx products are not designed or intended to be fail-
-- safe, or for use in any application requiring fail-safe
-- performance, such as life-support or safety devices or
-- systems, Class III medical devices, nuclear facilities,
-- applications related to the deployment of airbags, or any
-- other applications that could lead to death, personal
-- injury, or severe property or environmental damage
-- (individually and collectively, "Critical
-- Applications"). Customer assumes the sole risk and
-- liability of any use of Xilinx products in Critical
-- Applications, subject only to applicable laws and
-- regulations governing limitations on product liability.
-- 
-- THIS COPYRIGHT NOTICE AND DISCLAIMER MUST BE RETAINED AS
-- PART OF THIS FILE AT ALL TIMES. 


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;


--***********************************Entity Declaration************************
entity MGT_USRCLK_SOURCE_v2 is
port
(
    DIV1_OUT                : out std_logic;
    DIV2_OUT                : out std_logic;
    DIV4_OUT                : out std_logic;
    CLK2X_OUT               : out std_logic;
    DCM_LOCKED_OUT          : out std_logic;
    CLK_IN                  : in  std_logic;
    DCM_RESET_IN            : in  std_logic
);


end MGT_USRCLK_SOURCE_v2;

architecture RTL of MGT_USRCLK_SOURCE_v2 is
--*********************************Wire Declarations***************************
    signal clkdv_i                  : std_logic;
    signal clkdv2_i                  : std_logic;
    signal clk0_i                   : std_logic;
    signal clk2x_i                  : std_logic;
    signal clk_fbout                : std_logic;
    signal clk_fbin                 : std_logic;
begin

--*********************************** Main Body of Code ***********************


    ---------------------------------------
    -- PLL instance
    -- VCO frequency: 640 MHz
    -- Input clock: 40 MHz
    -- Output 0: 640 MHz
    -- Output 1: 320 MHz
    -- Output 2: 160 MHz
    -- Output 3: 40 MHz
    -- Output 4: 80 MHz
    -- Output 5: 40 MHz
    ---------------------------------------
    pll: PLL_BASE
        generic map (
            BANDWIDTH            => "OPTIMIZED",
            CLK_FEEDBACK         => "CLKFBOUT",
            COMPENSATION         => "SYSTEM_SYNCHRONOUS",
            DIVCLK_DIVIDE        => 1,
            CLKFBOUT_MULT        => 4,
            CLKFBOUT_PHASE       => 0.000,
            CLKOUT0_DIVIDE       => 2,
            CLKOUT0_PHASE        => 0.000,
            CLKOUT0_DUTY_CYCLE   => 0.500,
            CLKOUT1_DIVIDE       => 4,
            CLKOUT1_PHASE        => 0.000,
            CLKOUT1_DUTY_CYCLE   => 0.500,
            CLKOUT2_DIVIDE       => 8,
            CLKOUT2_PHASE        => 0.000,
            CLKOUT2_DUTY_CYCLE   => 0.500,
            CLKOUT3_DIVIDE       => 16,
            CLKOUT3_PHASE        => 0.000,
            CLKOUT3_DUTY_CYCLE   => 0.500,
            CLKOUT4_DIVIDE       => 16,
            CLKOUT4_PHASE        => 0.000,
            CLKOUT4_DUTY_CYCLE   => 0.500,
            CLKOUT5_DIVIDE       => 16,
            CLKOUT5_PHASE        => 0.000,
            CLKOUT5_DUTY_CYCLE   => 0.500,
            CLKIN_PERIOD         => 5.000,
            REF_JITTER           => 0.010
        )
        port map (
            CLKFBOUT            => clk_fbout,
            CLKOUT0             => clk2x_i,
            CLKOUT1             => clk0_i,
            CLKOUT2             => clkdv_i,
            CLKOUT3             => clkdv2_i,
            CLKOUT4             => open,
            CLKOUT5             => open,
            -- Status and control signals
            LOCKED              => DCM_LOCKED_OUT,
            RST                 => DCM_RESET_IN,
            -- Input clock control
            CLKFBIN             => clk_fbin,
            CLKIN               => CLK_IN
        );

    fb_bufg_i : BUFG
    port map
    (
        I => clk_fbout,
        O => clk_fbin
    );

    dcm_1x_bufg_i : BUFG
    port map
    (
        I                   =>          clk0_i,
        O                   =>          DIV1_OUT
    );
    
    dcm_2x_bufg_i : BUFG
    port map
    (
        I                   =>          clk2x_i,
        O                   =>          CLK2X_OUT
    );

    dcm_div2_bufg_i : BUFG 
    port map
    (
        I                   =>          clkdv_i,
        O                   =>          DIV2_OUT
    );

    dcm_div4_bufg_i : BUFG 
    port map
    (
        I                   =>          clkdv2_i,
        O                   =>          DIV4_OUT
    );



end RTL;

