
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity s6cdr_reset_fsm_tb is 
end entity;

architecture a of s6cdr_reset_fsm_tb is

component s6cdr_reset_fsm 
generic ( 
watchPeriod: integer := 10000; -- cycles to monitor
maxOscillations: integer := 10 -- max number of isaligned osciallation in watchPeriod
);
port (
clk: in std_logic; -- the user clock
rst: in std_logic; -- gtp reset
byteIsaligned: in std_logic; -- alignment signal from GTP
rxCdrRst: out std_logic -- the reset signal
);
end component;


  constant watchPeriod: integer := 10000; -- cycles to monitor
  constant maxOscillations: integer := 10; -- max number of isaligned osciallation in watchPeriod

  signal clk: std_logic; -- the user clock
  signal rst: std_logic; -- gtp reset
  signal byteIsaligned: std_logic; -- alignment signal from GTP
  signal rxCdrRst: std_logic; -- the reset signal

  constant cp: time := 10 ns;
  
begin

  uut: s6cdr_reset_fsm
  generic map (watchPeriod => watchPeriod, maxOscillations => maxOscillations)
  port map (
	clk => clk,
	rst => rst,
	byteIsaligned => byteIsaligned,
	rxCdrRst => rxCdrRst
  );

  process
  begin
	clk <= '1';
	wait for cp/2;
	clk <= '0';
	wait for cp/2;
  end process;

  process
  begin
	wait for 2 * cp;
	rst <= '1';
	wait for 100 ns;
	rst <= '0';
	wait;
  end process;

  process
  begin
	wait until falling_edge(rst);
	wait for 10 * cp;
	
	byteIsaligned <= '1';
	
	wait for  (watchPeriod + 10) * cp;
	

	for i in 1 to 5 loop
	byteIsaligned <= '0';
	wait for  15 * cp;
	byteIsaligned <= '1';
	wait for  watchPeriod / 5 * cp;
	
	end loop;

	wait for  (watchPeriod + 10) * cp;

	for i in 1 to maxOscillations + 1 loop
	byteIsaligned <= '0';
	wait for  15 * cp;
	byteIsaligned <= '1';
	wait for  watchPeriod / (maxOscillations + 5) * cp;
	
	end loop;

	wait for  (watchPeriod + 10) * cp;
	
	wait;
  
  end process;

end a;

