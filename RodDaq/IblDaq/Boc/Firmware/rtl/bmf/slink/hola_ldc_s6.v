// author: philipp schaefer

module hola_ldc_s6 #
	(parameter SIMULATION = 1'b0)
	(input wire mgtrefclk,
	input wire sys_rst,
	input wire mgt_rst,
	// new for sharing tile user clocks
	// one gtpclkout is used to generate all user clocks, also for neightbouring tiles
	 output wire gtpclkout,
	 output wire gtp_plldet_out,
	 output wire gtx_reset_done,
	 input wire usr_clk_in,
	 input wire usr_clk20_in,
	// s-link interface
	output wire [31:0] ld0,
	output wire [31:0] ld1,
	input wire uxoff_n,
	input wire utdo_n,
	input wire ureset_n,
	output wire lctrl_n0,
	output wire lwen_n0,
	output wire lctrl_n1,
	output wire lwen_n1,
	output wire lclk,
	output wire lderr_n,
	input wire [3:0] url,
	output wire ldown_n,
	input wire lclk_in,
	input wire dll_reset,
	// sfp serial interface
	input wire tlk_sin0_p,
	input wire tlk_sin0_n,
	output wire tlk_sout0_p,
	output wire tlk_sout0_n,
	input wire tlk_sin1_p,
	input wire tlk_sin1_n,
	output wire tlk_sout1_p,
	output wire tlk_sout1_n,
	// debug
	output wire dbg_txclk0,
	output wire [15:0] dbg_txd0,
	output wire dbg_tx_er0,
	output wire dbg_tx_en0,
	output wire [2:0] dbg_cc0,
	output wire dbg_rxclk0,
	output wire [15:0] dbg_rxd0,
	output wire dbg_rx_er0,
	output wire dbg_rx_dv0,
	output wire dbg_txclk1,
	output wire [15:0] dbg_txd1,
	output wire dbg_tx_er1,
	output wire dbg_tx_en1,
	output wire [2:0] dbg_cc1,
	output wire dbg_rxclk1,
	output wire [15:0] dbg_rxd1,
	output wire dbg_rx_er1,
	output wire dbg_rx_dv1,
	// leds
	output wire testled_n,
	output wire lderrled_n,
	output wire lupled_n,
	output wire flowctlled_n,
	output wire activityled_n
);

wire ldown_n0;
wire testled_n0;
wire lderrled_n0;
wire lupled_n0;
wire flowctlled_n0;
wire activityled_n0;
wire lclk0;
wire lderr_n0;

// lclk0 & lclk1 should be the same
assign lclk = lclk0;

wire ldc_rst_n;
//wire gtx_reset_done;
wire tlk_gtx_clk0;
wire tlk_enable0;
wire [15:0] tlk_txd0;
wire tlk_tx_en0;
wire tlk_tx_er0;
wire [15:0] tlk_rxd0;
wire tlk_rx_clk0;
wire tlk_rx_er0;
wire tlk_rx_dv0;

wire testled_n1;
wire lderrled_n1;
wire lupled_n1;
wire flowctlled_n1;
wire activityled_n1;
wire ldown_n1;
wire lclk1;
wire lderr_n1;

wire tlk_gtx_clk1;
wire tlk_enable1;
wire [15:0] tlk_txd1;
wire tlk_tx_en1;
wire tlk_tx_er1;
wire [15:0] tlk_rxd1;
wire tlk_rx_clk1;
wire tlk_rx_er1;
wire tlk_rx_dv1;

holaldc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(512),
	.LOG2DEPTH(9),
	.FULLMARGIN(256)) 
	
	 holaldc_core0_I (
	.power_up_rst_n(ldc_rst_n),
	.ld(ld0),
	.uxoff_n(uxoff_n),
	.ureset_n(ureset_n),
	.utdo_n(utdo_n),
	.lctrl_n(lctrl_n0),
	.lwen_n(lwen_n0),
	.lclk(lclk0),
	.lderr_n(lderr_n0),
	.url(url),
	.ldown_n(ldown_n0),
	.testled_n(testled_n0),
	.lderrled_n(lderrled_n0),
	.lupled_n(lupled_n0),
	.flowctlled_n(flowctlled_n0),
	.activityled_n(activityled_n0),
	.xclk(tlk_gtx_clk0),
	.enable(tlk_enable0),
	.txd(tlk_txd0),
	.tx_en(tlk_tx_en0),
	.tx_er(tlk_tx_er0),
	.rxd(tlk_rxd0),
	.rx_clk(tlk_rx_clk0),
	.rx_er(tlk_rx_er0),
	.rx_dv(tlk_rx_dv0),
	.lclk_in(lclk_in),
	.dll_reset(dll_reset)
);

holaldc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(512),
	.LOG2DEPTH(9),
	.FULLMARGIN(256)) 
	
	holaldc_core1_I (
	.power_up_rst_n(ldc_rst_n),
	.ld(ld1),
	.uxoff_n(uxoff_n),
	.ureset_n(ureset_n),
	.utdo_n(utdo_n),
	.lctrl_n(lctrl_n1),
	.lwen_n(lwen_n1),
	.lclk(lclk1),
	.lderr_n(lderr_n1),
	.url(url),
	.ldown_n(ldown_n1),
	.testled_n(testled_n1),
	.lderrled_n(lderrled_n1),
	.lupled_n(lupled_n1),
	.flowctlled_n(flowctlled_n1),
	.activityled_n(activityled_n1),
	.xclk(tlk_gtx_clk1),
	.enable(tlk_enable1),
	.txd(tlk_txd1),
	.tx_en(tlk_tx_en1),
	.tx_er(tlk_tx_er1),
	.rxd(tlk_rxd1),
	.rx_clk(tlk_rx_clk1),
	.rx_er(tlk_rx_er1),
	.rx_dv(tlk_rx_dv1),
	.lclk_in(lclk_in),
	.dll_reset(dll_reset)
);

tlk_wrapper_s6 tlk_wrapper_s6_I (
	.mgtrefclk(mgtrefclk),
	.gtpclkout(gtpclkout),
	.gtp_plldet_out(gtp_plldet_out),
	.usr_clk_in(usr_clk_in),
	.usr_clk20_in(usr_clk20_in),
	.gtx_reset(mgt_rst),
	.gtx_resetdone(gtx_reset_done),
	.gtx_rxn0(tlk_sin0_n),
	.gtx_rxp0(tlk_sin0_p),
	.gtx_txn0(tlk_sout0_n),
	.gtx_txp0(tlk_sout0_p),
	.gtx_rxn1(tlk_sin1_n),
	.gtx_rxp1(tlk_sin1_p),
	.gtx_txn1(tlk_sout1_n),
	.gtx_txp1(tlk_sout1_p),
	.GTX_RXCLKCORCNT0(dbg_cc0),
	.GTX_RXCLKCORCNT1(dbg_cc1),
	.tlk_txclk0(tlk_gtx_clk0),
	.tlk_txd0(tlk_txd0),
	.tlk_txen0(tlk_tx_en0),
	.tlk_txer0(tlk_tx_er0),
	.tlk_rxclk0(tlk_rx_clk0),
	.tlk_rxd0(tlk_rxd0),
	.tlk_rxdv0(tlk_rx_dv0),
	.tlk_rxer0(tlk_rx_er0),
	.tlk_txclk1(tlk_gtx_clk1),
	.tlk_txd1(tlk_txd1),
	.tlk_txen1(tlk_tx_en1),
	.tlk_txer1(tlk_tx_er1),
	.tlk_rxclk1(tlk_rx_clk1),
	.tlk_rxd1(tlk_rxd1),
	.tlk_rxdv1(tlk_rx_dv1),
	.tlk_rxer1(tlk_rx_er1)
);

assign testled_n = testled_n0 & testled_n1;
assign lderrled_n = lderrled_n0 & lderrled_n1;
assign lupled_n = lupled_n0 & lupled_n1;
assign flowctlled_n = flowctlled_n0 & flowctlled_n1;
assign activityled_n = activityled_n0 & activityled_n1;
assign ldown_n = ldown_n0 & ldown_n1;
assign lderr_n = lderr_n0 & lderr_n1;
assign ldc_rst_n = !sys_rst & gtx_reset_done;


assign dbg_txclk0 = tlk_gtx_clk0;
assign dbg_txd0 = tlk_txd0;
assign dbg_tx_er0 = tlk_tx_er0;
assign dbg_tx_en0 = tlk_tx_en0;
assign dbg_rxclk0 = tlk_rx_clk0;
assign dbg_rxd0 = tlk_rxd0;
assign dbg_rx_er0 = tlk_rx_er0;
assign dbg_rx_dv0 = tlk_rx_dv0;
assign dbg_txclk1 = tlk_gtx_clk1;
assign dbg_txd1 = tlk_txd1;
assign dbg_tx_er1 = tlk_tx_er1;
assign dbg_tx_en1 = tlk_tx_en1;
assign dbg_rxclk1 = tlk_rx_clk1;
assign dbg_rxd1 = tlk_rxd1;
assign dbg_rx_er1 = tlk_rx_er1;
assign dbg_rx_dv1 = tlk_rx_dv1;



endmodule // hola_ldc_s6
