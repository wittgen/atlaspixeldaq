library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SlinkChecker is
	port (
		clk : in std_logic;
		reset : in std_logic;

		udata : in std_logic_vector(31 downto 0);
		uctrl : in std_logic;
		uwe : in std_logic;

		error : out std_logic;
		error_source : out std_logic_vector(2 downto 0)
	);
end SlinkChecker;

architecture rtl of SlinkChecker is
	type check_fsm is (st_reset,
					   st_check_header,		-- expect ee1234ee
					   st_check_length,		-- expect 9
					   st_check_l1id,		-- check L1ID
					   st_check_version,	-- check version
					   st_check_data,		-- check first data word to be a header
					   st_skip);			-- skip words
	signal Z : check_fsm := st_reset;
	signal Z_return : check_fsm := st_reset;
	signal skip_words : integer range 0 to 15 := 0;
	signal l1id : unsigned(31 downto 0) := (others => '0');
	signal wrong_l1id : integer range 0 to 3 := 0;
begin

process begin
	wait until rising_edge(clk);

	-- default assignment
	error <= '0';
	error_source <= (others => '0');

	if reset = '1' then
		Z <= st_reset;
		l1id <= (others => '0');
	else
		if uwe = '0' then
			case Z is
				when st_reset =>
					-- do nothing
					null;

				when st_check_header =>
					Z <= st_check_length;
					if not ((uctrl = '1') and (udata = x"EE1234EE")) then
						error_source <= "000";
						error <= '1';
					end if;

				when st_check_length =>
					Z <= st_check_version;
					if not ((uctrl = '1') and (udata = x"00000009")) then
						error_source <= "001";
						error <= '1';
					end if;

				when st_check_version =>
					-- skip source-id and run number
					skip_words <= 2;
					Z <= st_skip;
					Z_return <= st_check_l1id;
					if not ((uctrl = '1') and (udata = x"03010000")) then
						error_source <= "010";
						error <= '1';
					end if;

				when st_check_l1id =>
					-- skip next three and then do data checking
					skip_words <= 3;
					Z <= st_skip;
					Z_return <= st_check_data;

					if (uctrl = '1') then
						if l1id = x"00000000" then
							-- first set l1id to expect
							l1id <= unsigned(udata) + 1;
						else
							-- check l1id
							if unsigned(udata) = l1id then
								-- next will be +1
								l1id <= l1id + 1;
							elsif (udata = x"00000000") then
								-- L1ID 0 is after ECR, so let's assume next L1ID will be 1...
								-- and don't throw an error because of unconsecutive L1IDs
								l1id <= x"00000001";
							else
								-- we got wrong L1ID...
								if wrong_l1id = 3 then
									-- resync after 3 failed l1ids
									l1id <= unsigned(udata) + 1;
									wrong_l1id <= 0;
								else
									l1id <= l1id + 1;
									wrong_l1id <= wrong_l1id + 1;
								end if;
								error <= '1';
								error_source <= "011";
							end if;
						end if;
					else
						error_source <= "011";
						error <= '1';
					end if;

				when st_check_data =>
					Z <= st_reset;
					if (uctrl = '1') then
						if (udata(31 downto 29) /= "001") then
							error_source <= "100";
							error <= '1';
						end if;
					else
						error_source <= "100";
						error <= '1';
					end if;

				when st_skip =>
					if (uwe = '0') then
						if (skip_words <= 1) then
							Z <= Z_return;
						else
							skip_words <= skip_words - 1;
						end if;
					end if;
			end case;

			-- always go to check header if BOF is detected
			if (uctrl = '0') and (udata = x"B0F00000") then
				-- assert error if BOF is unexpected
				if Z /= st_reset then
					error <= '1';
				end if;
				Z <= st_check_header;
			end if;
		end if;
	end if;
end process;

end architecture;

