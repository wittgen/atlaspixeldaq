 
this directory contains a couple of test utilities for s-link

in the testbench subdir there is an ise project with both quad lsc and quad ldc
which can be used to simulate S-Link behaviour

in this directory there are two synthesisable ise projects, using the 
south fpga as quad lsc and the north fpga as quad ldc.
both are equipped with chipscope cores to monitor the entire behaviour.
The lsc (south) can also be used to send test data to a robin.
