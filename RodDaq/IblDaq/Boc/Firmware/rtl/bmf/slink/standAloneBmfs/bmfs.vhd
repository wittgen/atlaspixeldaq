-- BMF main unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity bmfs is
	port
	(
		-- clocking and reset
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		clk40mon : out std_logic;

		-- system clock, 100 MHz
		CLK100_P : in std_logic;
		CLK100_N : in std_logic;
		clk100mon : out std_logic;
		
		-- mgt ref clock. don't use system clock here!
		MGTCLK100_P : in std_logic;
		MGTCLK100_N : in std_logic;
		clkmon : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);

		QSFP_RESET_N : out std_logic;
		QSFP_LPMODE : out std_logic;
		QSFP_MODSEL : out std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0)

	);
end entity bmfs;

architecture rtl of bmfs is

	COMPONENT bmf_lsc
		PORT(
		-- clocking and reset
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		clk40mon : out std_logic;

		-- system clock, 100 MHz
		CLK100_P : in std_logic;
		CLK100_N : in std_logic;
		clk100mon : out std_logic;
		
		-- mgt ref clock. don't use system clock here!
		MGTCLK100_P : in std_logic;
		MGTCLK100_N : in std_logic;
		clkmon : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);

		QSFP_RESET_N : out std_logic;
		QSFP_LPMODE : out std_logic;
		QSFP_MODSEL : out std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0)
		);
	end component;

begin


the_bmf: bmf_lsc
	port map (
	CLK40_P,CLK40_n,clk40mon,
	CLK100_P, CLK100_n, CLK100mon,
	MGTCLK100_P,MGTCLK100_N,clkmon,
	qsfp_rx_p,qsfp_rx_n,qsfp_tx_p,qsfp_tx_n,
	QSFP_RESET_N,QSFP_LPMODE,QSFP_MODSEL,
	slink_led
	);

end architecture;
