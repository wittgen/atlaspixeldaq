-- BMF main unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.lscpack.all;

use std.textio.all;
 
entity bmf_ldc is
	generic (simulation: integer := 0);
	port
	(
		-- clocking and reset
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		clk40mon : out std_logic;

		-- system clock, 100 MHz
		CLK100_P : in std_logic;
		CLK100_N : in std_logic;
		clk100mon : out std_logic;
		
		-- mgt ref clock. don't use system clock here!
		MGTCLK100_P : in std_logic;
		MGTCLK100_N : in std_logic;
		clkmon : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);

		QSFP_RESET_N : out std_logic;
		QSFP_LPMODE : out std_logic;
		QSFP_MODSEL : out std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0)

	);
end entity bmf_ldc;

architecture rtl of bmf_ldc is


	COMPONENT hola_ldc_s6
	generic (SIMULATION: integer := 0);
	PORT(
			--
		 dbg_txclk0 : OUT std_logic;
		 dbg_txd0 : OUT std_logic_vector(15 downto 0);
		 dbg_tx_er0 : OUT std_logic;
		 dbg_tx_en0 : OUT std_logic;
		 dbg_cc0 : OUT std_logic_vector(2 downto 0);
		 dbg_rxclk0 : OUT std_logic;
		 dbg_rxd0 : OUT std_logic_vector(15 downto 0);
		 dbg_rx_er0 : OUT std_logic;
		 dbg_rx_dv0 : OUT std_logic;
		 dbg_txclk1 : OUT std_logic;
		 dbg_txd1 : OUT std_logic_vector(15 downto 0);
		 dbg_tx_er1 : OUT std_logic;
		 dbg_tx_en1 : OUT std_logic;
		 dbg_cc1 : OUT std_logic_vector(2 downto 0);
		 dbg_rxclk1 : OUT std_logic;
		 dbg_rxd1  : OUT std_logic_vector(15 downto 0);
		 dbg_rx_er1 : OUT std_logic;
		 dbg_rx_dv1 : OUT std_logic;
			--
		usr_clk_in : IN std_logic;
		usr_clk20_in : IN std_logic;
		gtpclkout : OUT std_logic;
		gtp_plldet_out : OUT std_logic;
		gtx_reset_done : out std_logic;
			--
		mgtrefclk : IN std_logic;
		sys_rst : IN std_logic;
		mgt_rst : IN std_logic;
		uxoff_n : IN std_logic;
		utdo_n : IN std_logic;
		ureset_n : IN std_logic;
		url : IN std_logic_vector(3 downto 0);
		lclk_in : IN std_logic;
		dll_reset : IN std_logic;
		tlk_sin0_p : IN std_logic;
		tlk_sin0_n : IN std_logic;
		tlk_sin1_p : IN std_logic;
		tlk_sin1_n : IN std_logic;          
		ld0 : OUT std_logic_vector(31 downto 0);
		ld1 : OUT std_logic_vector(31 downto 0);
		lctrl_n0 : OUT std_logic;
		lwen_n0 : OUT std_logic;
		lctrl_n1 : OUT std_logic;
		lwen_n1 : OUT std_logic;
		lclk : OUT std_logic;
		lderr_n : OUT std_logic;
		ldown_n : OUT std_logic;
		tlk_sout0_p : OUT std_logic;
		tlk_sout0_n : OUT std_logic;
		tlk_sout1_p : OUT std_logic;
		tlk_sout1_n : OUT std_logic;
		testled_n : OUT std_logic;
		lderrled_n : OUT std_logic;
		lupled_n : OUT std_logic;
		flowctlled_n : OUT std_logic;
		activityled_n : OUT std_logic
		);
	END COMPONENT;

    -- 100 mhz ref clock
    component slink_refclk100_primary
    port
      (-- Clock in ports
      CLK_IN1_P         : in     std_logic;
      CLK_IN1_N         : in     std_logic;
      -- Clock out ports
      CLK_SLINK          : out    std_logic;
      CLK_40          : out    std_logic;
      -- Status and control signals
      RESET             : in     std_logic;
      LOCKED            : out    std_logic
      );
    end component;

    component slink_refclk150_primary
    port
      (-- Clock in ports
      CLK_IN1_P         : in     std_logic;
      CLK_IN1_N         : in     std_logic;
      -- Clock out ports
      CLK_SLINK          : out    std_logic;
      CLK_40          : out    std_logic;
      -- Status and control signals
      RESET             : in     std_logic;
      LOCKED            : out    std_logic
      );
    end component;
	 
	  -- slink_direct_pll
	component slink_100from100_pll
	port
	 (-- Clock in ports
	  CLK100_IN_P         : in     std_logic;
	  CLK100_IN_N         : in     std_logic;
	  -- Clock out ports
	  CLK_SLINK          : out    std_logic;
	  -- Status and control signals
	  RESET             : in     std_logic;
	  LOCKED            : out    std_logic
	 );
	end component;

	component slink_150from100_pll
	port
	 (-- Clock in ports
	  CLK100_IN_P         : in     std_logic;
	  CLK100_IN_N         : in     std_logic;
	  -- Clock out ports
	  CLK_SLINK          : out    std_logic;
	  -- Status and control signals
	  RESET             : in     std_logic;
	  LOCKED            : out    std_logic
	 );
	end component;

	component MGT_USRCLK_SOURCE 
	generic
	(
		 FREQUENCY_MODE   : string   := "LOW";
		 DIVIDE_2         : boolean  := false;    
		 FEEDBACK         : string   := "1X";
		 DIVIDE           : real     := 2.0    
	);
	port
	(
		 DIV1_OUT                : out std_logic;
		 DIV2_OUT                : out std_logic;
		 CLK2X_OUT               : out std_logic;
		 DCM_LOCKED_OUT          : out std_logic;
		 CLK_IN                  : in  std_logic;
		 FB_IN                   : in  std_logic;
		 DCM_RESET_IN            : in  std_logic

	);
	end component;


    signal reset, reset_n : std_logic;
	 signal sysrst: std_logic;
    signal clk_slink: std_logic;
    signal clk_slink_i: std_logic;
	 signal clk_slink_direct_i, clk_slink_direct, clk_slink_pll, clk_slink_ref: std_logic;
    signal clk40: std_logic;
    signal slink_direct_pll_locked, slink_up_pll_locked, slink_pll_locked: std_logic;
	 signal gtp_rst: std_logic := '1';
	 signal gtp_rst_done0: std_logic;
	 signal gtp_rst_done1: std_logic;


	-- generate MGT user clock
	signal gtpclk_to_cmt: std_logic := '0';
	signal gtpclk_fb: std_logic := '0';
	signal gtpclk_rst: std_logic := '0';
	signal gtp_locked: std_logic := '0';
	signal gtpclk: std_logic := '0';
	signal gtp_plldet_out: std_logic := '0';
	signal usr_clk: std_logic := '0';
	signal usr_clk20: std_logic := '0';

	-- SLINK LEDs
	signal slink0_testled_n : std_logic := '0';
	signal slink0_lderrled_n : std_logic := '0';
	signal slink0_lupled_n : std_logic := '0';
	signal slink0_flowctlled_n : std_logic := '0';
	signal slink0_activityled_n : std_logic := '0';

component ldc_icon
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL2 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL3 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL4 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL5 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL6 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL7 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL8 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL9 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL10: INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL11: INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL12: INOUT STD_LOGIC_VECTOR(35 DOWNTO 0)
	 );

end component;

signal  CONTROL0 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL1 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL2 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL3 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL4 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL5 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL6 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL7 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL8 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL9 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL10 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL11 : STD_LOGIC_VECTOR(35 DOWNTO 0);
signal  CONTROL12 : STD_LOGIC_VECTOR(35 DOWNTO 0);
	
component lsc_vio
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    ASYNC_IN : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    ASYNC_OUT : OUT STD_LOGIC_VECTOR(15 DOWNTO 0));
end component;

signal  vioIn : STD_LOGIC_VECTOR(15 DOWNTO 0);
signal  vioOut : STD_LOGIC_VECTOR(15 DOWNTO 0);
	
	
component lsc_ila
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(37 DOWNTO 0));
end component;

component tlk_ila
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(20 DOWNTO 0));

end component;

signal TRIG0 : STD_LOGIC_VECTOR(37 DOWNTO 0);
signal TRIG1 : STD_LOGIC_VECTOR(37 DOWNTO 0);
signal TRIG2 : STD_LOGIC_VECTOR(37 DOWNTO 0);
signal TRIG3 : STD_LOGIC_VECTOR(37 DOWNTO 0);


signal TXTRIG0 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal TXTRIG1 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal TXTRIG2 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal TXTRIG3 : STD_LOGIC_VECTOR(20 DOWNTO 0);

signal RXTRIG0 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal RXTRIG1 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal RXTRIG2 : STD_LOGIC_VECTOR(20 DOWNTO 0);
signal RXTRIG3 : STD_LOGIC_VECTOR(20 DOWNTO 0);

signal dbg_cc0 : std_logic_vector(2 downto 0);
signal dbg_cc1 : std_logic_vector(2 downto 0);
signal dbg_cc2 : std_logic_vector(2 downto 0);
signal dbg_cc3 : std_logic_vector(2 downto 0);

signal dbg_txclk0: std_logic;
signal dbg_txd0: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_tx_er0: std_logic;
signal dbg_tx_en0: std_logic;
signal dbg_rxclk0: std_logic;
signal dbg_rxd0: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_rx_er0: std_logic;
signal dbg_rx_dv0: std_logic;
signal dbg_txclk1: std_logic;
signal dbg_txd1: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_tx_er1: std_logic;
signal dbg_tx_en1: std_logic;
signal dbg_rxclk1: std_logic;
signal dbg_rxd1: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_rx_er1: std_logic;
signal dbg_rx_dv1: std_logic;

signal dbg_txclk2: std_logic;
signal dbg_txd2: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_tx_er2: std_logic;
signal dbg_tx_en2: std_logic;
signal dbg_rxclk2: std_logic;
signal dbg_rxd2: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_rx_er2: std_logic;
signal dbg_rx_dv2: std_logic;
signal dbg_txclk3: std_logic;
signal dbg_txd3: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_tx_er3: std_logic;
signal dbg_tx_en3: std_logic;
signal dbg_rxclk3: std_logic;
signal dbg_rxd3: STD_LOGIC_VECTOR(15 DOWNTO 0);
signal dbg_rx_er3: std_logic;
signal dbg_rx_dv3: std_logic;

--LDC
signal ldc0_XCLK_int : std_logic := '0';
signal ldc0_DLL_RESET : std_logic := '0';
signal ldc0_LCLK : std_logic;

--
signal ldc0_UXOFF_N0 : std_logic := '1';
signal ldc0_URESET_N0 : std_logic := '1';
signal ldc0_UTDO_N0 : std_logic := '0';
signal ldc0_URL0 : std_logic_vector(3 downto 0) := (others => '0');

signal ldc0_LD0 : std_logic_vector(31 downto 0);
signal ldc0_LD1 : std_logic_vector(31 downto 0);
signal ldc0_LCTRL_N0 : std_logic;
signal ldc0_LWEN_N0 : std_logic;
signal ldc0_LCTRL_N1 : std_logic;
signal ldc0_LWEN_N1 : std_logic;
signal ldc0_LDERR_N0 : std_logic;
signal ldc0_LDOWN_N0 : std_logic;
signal ldc0_TESTLED_N0 : std_logic;
signal ldc0_LDERRLED_N0 : std_logic;
signal ldc0_LUPLED_N0 : std_logic;
signal ldc0_FLOWCTLLED_N0 : std_logic;
signal ldc0_ACTIVITYLED_N0 : std_logic;
signal ldc0_ENABLE0 : std_logic;


-------------------
signal ldc1_XCLK_int : std_logic := '0';
signal ldc1_DLL_RESET : std_logic := '0';
signal ldc1_LCLK : std_logic;

--
signal ldc1_UXOFF_N0 : std_logic := '1';
signal ldc1_URESET_N0 : std_logic := '1';
signal ldc1_UTDO_N0 : std_logic := '0';
signal ldc1_URL0 : std_logic_vector(3 downto 0) := (others => '0');

signal ldc1_LD0 : std_logic_vector(31 downto 0);
signal ldc1_LD1 : std_logic_vector(31 downto 0);
signal ldc1_LCTRL_N0 : std_logic;
signal ldc1_LWEN_N0 : std_logic;
signal ldc1_LCTRL_N1 : std_logic;
signal ldc1_LWEN_N1 : std_logic;
signal ldc1_LDERR_N0 : std_logic;
signal ldc1_LDOWN_N0 : std_logic;
signal ldc1_TESTLED_N0 : std_logic;
signal ldc1_LDERRLED_N0 : std_logic;
signal ldc1_LUPLED_N0 : std_logic;
signal ldc1_FLOWCTLLED_N0 : std_logic;
signal ldc1_ACTIVITYLED_N0 : std_logic;
signal ldc1_ENABLE0 : std_logic;



signal usr_clk_n: std_logic;
signal clk40inv: std_logic;
signal clkSlinv: std_logic;


begin


	normalSpeed: if not GTP_HI_SPEED generate
	begin
	-- primary pll, driven by 40 MHz system clock. generates 40 MHz and 100 MHz
	 primary_Pll:  slink_refclk100_primary port map
		(-- Clock in ports
		 CLK_IN1_P => clk40_p,
		 CLK_IN1_N => clk40_n,
		 -- Clock out ports
		 CLK_SLINK => CLK_SLINK_pll,
		 CLK_40 => clk40,
		 -- Status and control signals
		 RESET  => '0', -- RESET,
		 LOCKED => slink_up_pll_locked);

	-- secondary pll, dirven by 100 MHz system clock. generates 100 MHz
		direct_Pll : slink_100from100_pll
		  port map
			(-- Clock in ports
			 CLK100_IN_P => clk100_p,
			 CLK100_IN_N => clk100_n,
			 -- Clock out ports
			 CLK_SLINK => clk_slink_direct,
			 -- Status and control signals
			 RESET  => '0',
			 LOCKED => slink_direct_pll_locked);

		 gtpclkout_dcm : MGT_USRCLK_SOURCE
		 generic map
		 (
			  FREQUENCY_MODE                  =>      "LOW",
			  DIVIDE_2                        =>      false,
			  FEEDBACK                        =>      "1X",
			  DIVIDE                          =>      2.0
		 )
		 port map
		 (
			  DIV1_OUT                        =>      usr_clk,
			  DIV2_OUT                        =>      usr_clk20,
			  CLK2X_OUT                       =>      open,
			  DCM_LOCKED_OUT                  =>      gtp_locked,
			  CLK_IN                          =>      gtpclk_to_cmt,
			  FB_IN                           =>      usr_clk,
			  DCM_RESET_IN                    =>      gtpclk_rst
		 );


	end generate;

	highSpeed: if GTP_HI_SPEED generate
	begin
	-- primary pll, driven by 40 MHz system clock. generates 40 MHz and 150 MHz
	-- remember, this needs a 3Gbps GTP version
	 primary_Pll:  slink_refclk150_primary port map
		(-- Clock in ports
		 CLK_IN1_P => clk40_p,
		 CLK_IN1_N => clk40_n,
		 -- Clock out ports
		 CLK_SLINK => CLK_SLINK_pll,
		 CLK_40 => clk40,
		 -- Status and control signals
		 RESET  => '0', -- RESET,
		 LOCKED => slink_up_pll_locked);

	-- secondary pll, dirven by 150 MHz system clock. generates 100 MHz
		direct_Pll : slink_150from100_pll
		  port map
			(-- Clock in ports
			 CLK100_IN_P => clk100_p,
			 CLK100_IN_N => clk100_n,
			 -- Clock out ports
			 CLK_SLINK => clk_slink_direct,
			 -- Status and control signals
			 RESET  => '0',
			 LOCKED => slink_direct_pll_locked);

		-- 3Gbps has different user clock settings
		 gtpclkout_dcm : MGT_USRCLK_SOURCE
		 generic map
		 (
			  FREQUENCY_MODE                  =>      "LOW",
			  DIVIDE_2                        =>      false,
			  FEEDBACK                        =>      "1X",
			  DIVIDE                          =>      2.0
		 )
		 port map
		 (
			  DIV1_OUT                        =>      usr_clk20,
			  DIV2_OUT                        =>      open,
			  CLK2X_OUT                       =>      usr_clk,
			  DCM_LOCKED_OUT                  =>      gtp_locked,
			  CLK_IN                          =>      gtpclk_to_cmt,
			  FB_IN                           =>      usr_clk,
			  DCM_RESET_IN                    =>      gtpclk_rst
		 );

	end generate;

	slink_pll_locked <= slink_up_pll_locked and slink_direct_pll_locked;


	-- ibuf for gtp ref clock
		 gtpclkIbuf: ibufds 
		 port map (i => mgtclk100_p, ib => mgtclk100_n, o => clk_slink_ref);
	
	
	
	 pllUpClk: if (SL_USE_PLL = true) and (SL_PLL_DIRECT = false) generate
	 begin
		clk_slink <= clk_slink_pll;
	 end generate;

	 pllDirectClk: if (SL_USE_PLL = true) and (SL_PLL_DIRECT = true) generate
	 begin
		clk_slink <= clk_slink_direct;
	 end generate;

	 noPllClk: if SL_USE_PLL /= true generate
	 begin
		clk_slink <= clk_slink_ref;
	 end generate;
	 
 
    reset <= not slink_pll_locked;
    reset_n <= not reset;
	 
	 sysrst <= reset or (not gtp_rst_done0) or (not gtp_rst_done1);

	usr_clk_n <= not usr_clk;
	clkmon_out : ODDR2
	generic map(
		DDR_ALIGNMENT => "NONE", -- Sets output alignment to "NONE", "C0", "C1"
		INIT => '0', -- Sets initial state of the Q output to '0' or '1'
		SRTYPE => "SYNC") -- Specifies "SYNC" or "ASYNC" set/reset
	port map (
		Q => clkmon, -- 1-bit output data
		C0 => usr_clk, -- 1-bit clock input
		C1 => usr_clk_n, -- 1-bit clock input
		CE => '1', -- 1-bit clock enable input
		D0 => '1',
		-- 1-bit data input (associated with C0)
		D1 => '0',
		-- 1-bit data input (associated with C1)
		R => '0',
		-- 1-bit reset input
		S => '0'
		-- 1-bit set input
	);

	clk40inv <= not clk40;

	clk40mon_out : ODDR2
	generic map(
		DDR_ALIGNMENT => "NONE", -- Sets output alignment to "NONE", "C0", "C1"
		INIT => '0', -- Sets initial state of the Q output to '0' or '1'
		SRTYPE => "SYNC") -- Specifies "SYNC" or "ASYNC" set/reset
	port map (
		Q => clk40mon, -- 1-bit output data
		C0 => clk40, -- 1-bit clock input
		C1 => clk40inv, -- 1-bit clock input
		CE => '1', -- 1-bit clock enable input
		D0 => '1',
		-- 1-bit data input (associated with C0)
		D1 => '0',
		-- 1-bit data input (associated with C1)
		R => '0',
		-- 1-bit reset input
		S => '0'
		-- 1-bit set input
	);

	clkSlinv <= not clk_slink_direct;

	clk100mon_out : ODDR2
	generic map(
		DDR_ALIGNMENT => "NONE", -- Sets output alignment to "NONE", "C0", "C1"
		INIT => '0', -- Sets initial state of the Q output to '0' or '1'
		SRTYPE => "SYNC") -- Specifies "SYNC" or "ASYNC" set/reset
	port map (
		Q => clk100mon, -- 1-bit output data
		C0 => clk_slink_direct, -- 1-bit clock input
		C1 => clkSlinv, -- 1-bit clock input
		CE => '1', -- 1-bit clock enable input
		D0 => '1',
		-- 1-bit data input (associated with C0)
		D1 => '0',
		-- 1-bit data input (associated with C1)
		R => '0',
		-- 1-bit reset input
		S => '0'
		-- 1-bit set input
	);


-- reset to QSFP
QSFP_RESET_N <= '1'; --not reset;
QSFP_MODSEL <= '1'; --'0';
QSFP_LPMODE <= '1'; --'0';

-- SLINK clocking
gtpclk_rst     <=  not gtp_plldet_out;

-- MGT clocking
gtpclkout0_0_pll0_bufio2_i : BUFIO2
    generic map
    (
        DIVIDE                          =>      1,
        DIVIDE_BYPASS                   =>      TRUE
    )
    port map
    (
        I                               =>      gtpclk, -- tile0_gtpclkout0_i(0),
        DIVCLK                          =>      gtpclk_to_cmt, -- tile0_gtpclkout0_0_to_cmt_i,
        IOCLK                           =>      open,
        SERDESSTROBE                    =>      open
    );

	 
vioControl : lsc_vio
  port map (
    CONTROL => CONTROL12,
    ASYNC_IN => vioIn,
    ASYNC_OUT => vioOut);


	vioIn( 0) <= slink_pll_locked;
	vioIn( 1) <= gtp_plldet_out;
	vioIn( 2) <= gtpclk_rst;
	vioIn( 3) <= gtp_locked;
	vioIn( 4) <= '0';
	vioIn( 5) <= '0';
	vioIn( 6) <= '0';
	vioIn( 7) <= '0';
	vioIn( 8) <= '0';
	vioIn( 9) <= '0';
	vioIn(10) <= '0';
	vioIn(11) <= '0';
	vioIn(12) <= '0';
	vioIn(13) <= '0';
	vioIn(14) <= '0';
	vioIn(15) <= '0';

  vioReal: if simulation /= 1 generate
  begin
   gtp_rst <= reset OR vioOut(0);
	end generate;

  vioSim: if simulation = 1 generate
  begin
   gtp_rst <= reset;
	end generate;

	
	-- LDC --
	ldc0: hola_ldc_s6 
	generic map(simulation => simulation)
	PORT MAP(
		--
		 dbg_txclk0 => dbg_txclk0,
		 dbg_txd0 => dbg_txd0,
		 dbg_tx_er0 => dbg_tx_er0, 
		 dbg_tx_en0 => dbg_tx_en0,
		 dbg_cc0 => dbg_cc0,
		 dbg_rxclk0 => dbg_rxclk0,
		 dbg_rxd0 => dbg_rxd0,
		 dbg_rx_er0 => dbg_rx_er0,
		 dbg_rx_dv0 => dbg_rx_dv0,
		 dbg_txclk1 => dbg_txclk1,
		 dbg_txd1 => dbg_txd1,
		 dbg_tx_er1 => dbg_tx_er1,
		 dbg_tx_en1 => dbg_tx_en1,
		 dbg_cc1 => dbg_cc1,
		 dbg_rxclk1 => dbg_rxclk1,
		 dbg_rxd1 => dbg_rxd1,
		 dbg_rx_er1 => dbg_rx_er1,
		 dbg_rx_dv1 => dbg_rx_dv1,
		--
		gtpclkout => gtpclk,
		gtp_plldet_out => gtp_plldet_out,
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		--
		mgtrefclk => clk_slink,
		sys_rst => sysrst,
		mgt_rst => gtp_rst,
		gtx_reset_done => gtp_rst_done0,
		ld0 => ldc0_LD0,
		ld1 => ldc0_LD1,
		uxoff_n => ldc0_UXOFF_N0,
		utdo_n => ldc0_UTDO_N0,
		ureset_n => ldc0_URESET_N0,
		lctrl_n0 => ldc0_LCTRL_N0,
		lwen_n0 => ldc0_LWEN_N0,
		lctrl_n1 => ldc0_LCTRL_N1,
		lwen_n1 => ldc0_LWEN_N1,
		lclk => open,
		lderr_n => ldc0_LDERR_N0,
		url => ldc0_URL0,
		ldown_n => ldc0_LDOWN_N0,
		lclk_in => clk40,
		dll_reset => '0',
		tlk_sin0_p => qsfp_rx_p(0),
		tlk_sin0_n => qsfp_rx_n(0),
		tlk_sout0_p => qsfp_tx_p(0),
		tlk_sout0_n => qsfp_tx_n(0),
		tlk_sin1_p => qsfp_rx_p(1),
		tlk_sin1_n => qsfp_rx_n(1),
		tlk_sout1_p => qsfp_tx_p(1),
		tlk_sout1_n => qsfp_tx_n(1),
		testled_n => ldc0_TESTLED_N0,
		lderrled_n => ldc0_LDERRLED_N0,
		lupled_n => ldc0_LUPLED_N0,
		flowctlled_n => ldc0_FLOWCTLLED_N0,
		activityled_n => ldc0_ACTIVITYLED_N0
	);
	

	txtrig0(15 downto 0) <= dbg_txd0;
	txtrig0(16) <= dbg_tx_er0;
	txtrig0(17) <= dbg_tx_en0;
	txtrig1(15 downto 0) <= dbg_txd1;
	txtrig1(16) <= dbg_tx_er1;
	txtrig1(17) <= dbg_tx_en1;
	rxtrig0(20 downto 18) <= dbg_cc0;
	rxtrig0(15 downto 0) <= dbg_rxd0;
	rxtrig0(16) <= dbg_rx_er0;
	rxtrig0(17) <= dbg_rx_dv0;
	rxtrig1(20 downto 18) <= dbg_cc1;
	rxtrig1(15 downto 0) <= dbg_rxd1;
	rxtrig1(16) <= dbg_rx_er1;
	rxtrig1(17) <= dbg_rx_dv1;

	txtrig0(20 downto 18) <= "000";
	txtrig1(20 downto 18) <= "000";
	

	TRIG0(31 downto 0) <= ldc1_LD0;
	trig0(32) <= ldc1_UXOFF_N0;
	trig0(33) <= ldc1_LDOWN_N0;
	trig0(34) <= ldc1_LWEN_N0;
	trig0(35) <= ldc1_LCTRL_N0;
	trig0(36) <= ldc1_UTDO_N0;
	trig0(37) <= ldc1_URESET_N0;

	TRIG1(31 downto 0) <= ldc1_LD1;
	trig1(32) <= ldc1_UXOFF_N0;
	trig1(33) <= ldc1_LDOWN_N0;
	trig1(34) <= ldc1_LWEN_N1;
	trig1(35) <= ldc1_LCTRL_N1;
	trig1(36) <= ldc1_UTDO_N0;
	trig1(37) <= ldc1_URESET_N0;

	-----------------------------------------
	-- LDC --
	ldc1: hola_ldc_s6 
	generic map(simulation => simulation)
	PORT MAP(
		--
		 dbg_txclk0 => dbg_txclk2,
		 dbg_txd0 => dbg_txd2,
		 dbg_tx_er0 => dbg_tx_er2, 
		 dbg_tx_en0 => dbg_tx_en2,
		 dbg_cc0 => dbg_cc2,
		 dbg_rxclk0 => dbg_rxclk2,
		 dbg_rxd0 => dbg_rxd2,
		 dbg_rx_er0 => dbg_rx_er2,
		 dbg_rx_dv0 => dbg_rx_dv2,
		 dbg_txclk1 => dbg_txclk3,
		 dbg_txd1 => dbg_txd3,
		 dbg_tx_er1 => dbg_tx_er3,
		 dbg_tx_en1 => dbg_tx_en3,
		 dbg_cc1 => dbg_cc3,
		 dbg_rxclk1 => dbg_rxclk3,
		 dbg_rxd1 => dbg_rxd3,
		 dbg_rx_er1 => dbg_rx_er3,
		 dbg_rx_dv1 => dbg_rx_dv3,
		--
		gtpclkout => open,
		gtp_plldet_out => open,
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		gtx_reset_done => gtp_rst_done1,
		--
		mgtrefclk => clk_slink,
		sys_rst => sysrst,
		mgt_rst => gtp_rst,
		ld0 => ldc1_LD0,
		ld1 => ldc1_LD1,
		uxoff_n => ldc1_UXOFF_N0,
		utdo_n => ldc1_UTDO_N0,
		ureset_n => ldc1_URESET_N0,
		lctrl_n0 => ldc1_LCTRL_N0,
		lwen_n0 => ldc1_LWEN_N0,
		lctrl_n1 => ldc1_LCTRL_N1,
		lwen_n1 => ldc1_LWEN_N1,
		lclk => open,
		lderr_n => ldc1_LDERR_N0,
		url => ldc1_URL0,
		ldown_n => ldc1_LDOWN_N0,
		lclk_in => clk40,
		dll_reset => '0',
		tlk_sin0_p => qsfp_rx_p(2),
		tlk_sin0_n => qsfp_rx_n(2),
		tlk_sout0_p => qsfp_tx_p(2),
		tlk_sout0_n => qsfp_tx_n(2),
		tlk_sin1_p => qsfp_rx_p(3),
		tlk_sin1_n => qsfp_rx_n(3),
		tlk_sout1_p => qsfp_tx_p(3),
		tlk_sout1_n => qsfp_tx_n(3),
		testled_n => ldc1_TESTLED_N0,
		lderrled_n => ldc1_LDERRLED_N0,
		lupled_n => ldc1_LUPLED_N0,
		flowctlled_n => ldc1_FLOWCTLLED_N0,
		activityled_n => ldc1_ACTIVITYLED_N0
	);
	
	
	txtrig2(15 downto 0) <= dbg_txd2;
	txtrig2(16) <= dbg_tx_er2;
	txtrig2(17) <= dbg_tx_en2;
	txtrig3(15 downto 0) <= dbg_txd3;
	txtrig3(16) <= dbg_tx_er3;
	txtrig3(17) <= dbg_tx_en3;
	rxtrig2(20 downto 18) <= dbg_cc2;
	rxtrig2(15 downto 0) <= dbg_rxd2;
	rxtrig2(16) <= dbg_rx_er2;
	rxtrig2(17) <= dbg_rx_dv2;
	rxtrig3(20 downto 18) <= dbg_cc3;
	rxtrig3(15 downto 0) <= dbg_rxd3;
	rxtrig3(16) <= dbg_rx_er3;
	rxtrig3(17) <= dbg_rx_dv3;

	txtrig2(20 downto 18) <= "000";
	txtrig3(20 downto 18) <= "000";


	TRIG2(31 downto 0) <= ldc1_LD0;
	trig2(32) <= ldc1_UXOFF_N0;
	trig2(33) <= ldc1_LDOWN_N0;
	trig2(34) <= ldc1_LWEN_N0;
	trig2(35) <= ldc1_LCTRL_N0;
	trig2(36) <= ldc1_UTDO_N0;
	trig2(37) <= ldc1_URESET_N0;

	TRIG3(31 downto 0) <= ldc1_LD1;
	trig3(32) <= ldc1_UXOFF_N0;
	trig3(33) <= ldc1_LDOWN_N0;
	trig3(34) <= ldc1_LWEN_N1;
	trig3(35) <= ldc1_LCTRL_N1;
	trig3(36) <= ldc1_UTDO_N0;
	trig3(37) <= ldc1_URESET_N0;
 
-- SLINK LEDs
slink_led(0) <= not (ldc0_TESTLED_N0 and ldc1_TESTLED_N0);
slink_led(1) <= not (ldc0_LDERRLED_N0 and ldc1_LDERRLED_N0);
slink_led(2) <= not (ldc0_LUPLED_N0 and ldc1_LUPLED_N0);
slink_led(3) <= not (ldc0_FLOWCTLLED_N0 and ldc1_FLOWCTLLED_N0);
slink_led(4) <= not (ldc0_ACTIVITYLED_N0 and ldc1_ACTIVITYLED_N0);


icon : ldc_icon
  port map (
    CONTROL0 => CONTROL0,
    CONTROL1 => CONTROL1,
    CONTROL2 => CONTROL2,
    CONTROL3 => CONTROL3,
    CONTROL4 => CONTROL4,
    CONTROL5 => CONTROL5,
    CONTROL6 => CONTROL6,
    CONTROL7 => CONTROL7,
    CONTROL8 => CONTROL8,
    CONTROL9 => CONTROL9,
    CONTROL10 => CONTROL10,
    CONTROL11 => CONTROL11,
    CONTROL12 => CONTROL12
	 );

ila0 : lsc_ila
  port map (
    CONTROL => CONTROL0,
    CLK => clk40,
    TRIG0 => TRIG0);

ila1 : lsc_ila
  port map (
    CONTROL => CONTROL1,
    CLK => clk40,
    TRIG0 => TRIG1);

ila2 : lsc_ila
  port map (
    CONTROL => CONTROL2,
    CLK => clk40,
    TRIG0 => TRIG2);

ila3 : lsc_ila
  port map (
    CONTROL => CONTROL3,
    CLK => clk40,
    TRIG0 => TRIG3);


txila0: tlk_ila
  port map (
    CONTROL => CONTROL4,
    CLK => dbg_txclk0,
    TRIG0 => TXTRIG0);


txila1: tlk_ila
  port map (
    CONTROL => CONTROL6,
    CLK => dbg_txclk1,
    TRIG0 => TXTRIG1);

txila2: tlk_ila
  port map (
    CONTROL => CONTROL8,
    CLK => dbg_txclk2,
    TRIG0 => TXTRIG2);

txila3: tlk_ila
  port map (
    CONTROL => CONTROL10,
    CLK => dbg_txclk3,
    TRIG0 => TXTRIG3);


rxila0: tlk_ila
  port map (
    CONTROL => CONTROL5,
    CLK => dbg_rxclk0,
    TRIG0 => RXTRIG0);
rxila1: tlk_ila
  port map (
    CONTROL => CONTROL7,
    CLK => dbg_rxclk1,
    TRIG0 => RXTRIG1);
rxila2: tlk_ila
  port map (
    CONTROL => CONTROL9,
    CLK => dbg_rxclk2,
    TRIG0 => RXTRIG2);
rxila3: tlk_ila
  port map (
    CONTROL => CONTROL11,
    CLK => dbg_rxclk3,
    TRIG0 => RXTRIG3);


end architecture;
