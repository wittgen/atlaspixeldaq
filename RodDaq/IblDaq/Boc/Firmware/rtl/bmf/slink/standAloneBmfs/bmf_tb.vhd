--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   10:22:57 07/12/2013
-- Design Name:   
-- Module Name:   /home/kugel/daten/work/projekte/atlas/iblBoc/vhdl/boc/bmfLscTest/bmfn/bmf_tb.vhd
-- Project Name:  bmfn
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: bmfn
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY bmf_tb IS
END bmf_tb;
 
ARCHITECTURE behavior OF bmf_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 	COMPONENT bmf_lsc
		generic (simulation: integer := 0);
		PORT(
		-- clocking and reset
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		clk40mon : out std_logic;

		-- system clock, 100 MHz
		CLK100_P : in std_logic;
		CLK100_N : in std_logic;
		clk100mon : out std_logic;
		
		-- mgt ref clock. don't use system clock here!
		MGTCLK100_P : in std_logic;
		MGTCLK100_N : in std_logic;
		clkmon : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);

		QSFP_RESET_N : out std_logic;
		QSFP_LPMODE : out std_logic;
		QSFP_MODSEL : out std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0)
		);
	end component;

 	COMPONENT bmf_ldc
		generic (simulation: integer := 0);
		PORT(
		-- clocking and reset
		CLK40_P : in std_logic := '0';
		CLK40_N : in std_logic := '1';
		clk40mon : out std_logic;

		-- system clock, 100 MHz
		CLK100_P : in std_logic;
		CLK100_N : in std_logic;
		clk100mon : out std_logic;
		
		-- mgt ref clock. don't use system clock here!
		MGTCLK100_P : in std_logic;
		MGTCLK100_N : in std_logic;
		clkmon : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);

		QSFP_RESET_N : out std_logic;
		QSFP_LPMODE : out std_logic;
		QSFP_MODSEL : out std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0)
		);
	end component;
    

   --Inputs
   signal CLK40_P : std_logic := '0';
   signal CLK40_N : std_logic := '0';
   signal lsc_CLK40mon : std_logic := '0';
   signal ldc_CLK40mon : std_logic := '0';
   signal lsc_CLK100_P : std_logic := '0';
   signal lsc_CLK100_N : std_logic := '0';
   signal ldc_CLK100_P : std_logic := '0';
   signal ldc_CLK100_N : std_logic := '0';
   signal lsc_CLK100mon : std_logic := '0';
   signal ldc_CLK100mon : std_logic := '0';
   signal lsc_clkmon : std_logic := '0';
   signal ldc_clkmon : std_logic := '0';

   signal qsfp_rx_p : std_logic_vector(3 downto 0) := (others => '0');
   signal qsfp_rx_n : std_logic_vector(3 downto 0) := (others => '0');
   signal qsfp_tx_p : std_logic_vector(3 downto 0);
   signal qsfp_tx_n : std_logic_vector(3 downto 0);

   signal lsc_QSFP_RESET_N : std_logic;
   signal lsc_QSFP_LPMODE : std_logic;
   signal lsc_QSFP_MODSEL : std_logic;
   signal lsc_slink_led : std_logic_vector(4 downto 0);

   signal ldc_QSFP_RESET_N : std_logic;
   signal ldc_QSFP_LPMODE : std_logic;
   signal ldc_QSFP_MODSEL : std_logic;
   signal ldc_slink_led : std_logic_vector(4 downto 0);

   -- Clock period definitions
   constant CLK40_P_period : time := 25 ns;
   constant LSC_CLK100_P_period : time := 10.00 ns;
   constant LDC_CLK100_P_period : time := 10.01 ns;
 
BEGIN
 
the_lsc: bmf_lsc
	generic map (simulation => 1)
	port map (
	CLK40_P => CLK40_P,
	CLK40_n => CLK40_N,
	clk40mon => lsc_clk40mon,
	CLK100_P => lsc_CLK100_P, 
	CLK100_n => lsc_CLK100_N, 
	CLK100mon => lsc_CLK100mon,
	MGTCLK100_P => lsc_CLK100_P,
	MGTCLK100_N => lsc_CLK100_N,
	clkmon => lsc_clkmon,
	qsfp_rx_p => qsfp_rx_p,
	qsfp_rx_n => qsfp_rx_n,
	qsfp_tx_p => qsfp_tx_p,
	qsfp_tx_n => qsfp_tx_n,
	QSFP_RESET_N => lsc_QSFP_RESET_N,
	QSFP_LPMODE => lsc_QSFP_LPMODE,
	QSFP_MODSEL => lsc_QSFP_MODSEL,
	slink_led => lsc_slink_led
	);

the_ldc: bmf_ldc
	generic map (simulation => 1)
	port map (
	CLK40_P => CLK40_P,
	CLK40_n => CLK40_N,
	clk40mon => ldc_clk40mon,
	CLK100_P => ldc_CLK100_P, 
	CLK100_n => ldc_CLK100_N, 
	CLK100mon => ldc_CLK100mon,
	MGTCLK100_P => ldc_CLK100_P,
	MGTCLK100_N => ldc_CLK100_N,
	clkmon => ldc_clkmon,
	qsfp_rx_p => qsfp_tx_p,
	qsfp_rx_n => qsfp_tx_n,
	qsfp_tx_p => qsfp_rx_p,
	qsfp_tx_n => qsfp_rx_n,
	QSFP_RESET_N => ldc_QSFP_RESET_N,
	QSFP_LPMODE => ldc_QSFP_LPMODE,
	QSFP_MODSEL => ldc_QSFP_MODSEL,
	slink_led => ldc_slink_led
	);

   -- Clock process definitions
   CLK40_P_process :process
   begin
		CLK40_P <= '0';
		wait for CLK40_P_period/2;
		CLK40_P <= '1';
		wait for CLK40_P_period/2;
   end process;
 
	CLK40_N <= not clk40_p;

   LSC_CLK100_P_process :process
   begin
		LSC_CLK100_P <= '0';
		wait for LSC_CLK100_P_period/2;
		LSC_CLK100_P <= '1';
		wait for LSC_CLK100_P_period/2;
   end process;
 
	LSC_CLK100_N <= not LSC_clk100_p;

   LDC_CLK100_P_process :process
   begin
		LDC_CLK100_P <= '0';
		wait for LDC_CLK100_P_period/2;
		LDC_CLK100_P <= '1';
		wait for LDC_CLK100_P_period/2;
   end process;
 
	LDC_CLK100_N <= not LDC_clk100_p;
	

   -- Stimulus process
   stim_proc: process
   begin		

      wait;
   end process;

END;
