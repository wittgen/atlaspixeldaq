library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_ARITH.all;
use IEEE.STD_LOGIC_UNSIGNED.all;

-- Uncomment the following lines to use the declarations that are
-- provided for instantiating Xilinx primitive components.
--library UNISIM;
--use UNISIM.VComponents.all;

-- AK, Mar 01 2007
--		Format updated to 03010000
--		URL lines 3..2 set data format: 00 => plain event increment from 0
--												  01 => event increment with channel code 0x11xxxxxx
--												  10 => event increment with channel code 0x22xxxxxx
--												  11 => event increment with channel code 0x33xxxxxx
--		URL lines 1..0 unused

-- AK, June 14th 2005
-- 	writing to the control register sets the event length via bits 15 .. 0
-- 	and the run number via bits 31 .. 16
-- 	current event format is version 3.0
-- 	Status value = 0, data value counting up from 0, status AFTER data
--

-- GK, August 31th 2005
-- Implemented optional byte swapping of output data
-- to test the endianness check and byte swapping
-- capability of the ROL Handler
-- When the channelCode is set to "0000" alternate fragments
-- generated will have normal byte order and byte order swapped 


entity lscDataGen is
  port (
    lscData                 : out std_logic_vector(31 downto 0);
    lscFull_n               : in  std_logic;
    lscEnable		          : in  std_logic;
    lscWen_n                : out std_logic;
    lscCtrl_n               : out std_logic;
    lscTest_n               : out std_logic;
    lscReset_n              : out std_logic;
    clk                     : in  std_logic;
    reset                   : in  std_logic
    );
end lscDataGen;

architecture Behavioral of lscDataGen is

  constant begining_of_fragment : std_logic_vector(31 downto 0) := x"B0F00000";
  constant end_of_fragment      : std_logic_vector(31 downto 0) := x"E0F00000";

  type header_array is array (0 to 8) of std_logic_vector(31 downto 0);
  type trailer_array is array (0 to 2) of std_logic_vector(31 downto 0);

  constant rod_header : header_array := (x"ee1234ee",  -- hdr marker
                                         x"00000009",  -- hdr size
                                         x"03010000",  -- format code
                                         x"b0c00b0c",  -- source id
                                         x"00000000",  -- run number
                                         x"00000000",  -- event id
                                         x"11111111",  -- bx id
                                         x"b0b0b0b0",  -- trigger type
                                         x"0d0d0d0d"   -- event type 
													  );

  constant status_element : std_logic_vector(31 downto 0) := x"00000000"; -- x"00000001";

  constant rod_trailer : trailer_array := (x"00000001",  -- number of status
                                           x"00000001",  -- data size
                                           x"00000001");

--  signal control_register_cd : std_logic_vector(15 downto 0);
--  signal sync_bit_cd1        : std_logic;
--  signal sync_bit_cd2        : std_logic;
--  signal sync_bit_sd         : std_logic;

  signal event_size             : std_logic_vector(15 downto 0) := X"0100";
  signal run_number             : std_logic_vector(15 downto 0) := X"1234";
                                        -- excluding trailer
  signal data_counter           : std_logic_vector(15 downto 0);
  signal data_counter_decrement : std_logic;
  signal data_counter_preload   : std_logic;

  signal data_value           : std_logic_vector(15 downto 0);

  signal data_length      : std_logic_vector(15 downto 0);
  signal data_length_load : std_logic;

  signal event_ID_Counter           : std_logic_vector(31 downto 0);
  signal event_ID_Counter_increment : std_logic;


  signal control_word_flag : std_logic;
  signal data_output       : std_logic_vector(31 downto 0);
  signal output_data_valid : std_logic;
  signal XOFF              : std_logic;

  type select_data_type is (bof_select,
                            header_0_select,
                            header_1_select,
                            header_2_select,
                            header_3_select,
                            header_4_select,
                            header_5_select,
                            header_6_select,
                            header_7_select,
                            header_8_select,
                            trailer_0_select,
                            trailer_1_select,
                            trailer_2_select,
                            data_element_select,
                            status_element_select,
                            eof_select);

  signal selector : select_data_type;

  type machine_states is (idle_state,
                          bof_state,
                          header_0_state,
                          header_1_state,
                          header_2_state,
                          header_3_state,
                          header_4_state,
                          header_5_state,
                          header_6_state,
                          header_7_state,
                          header_8_state,
                          status_element_state,
                          data_element_state,
                          xoff_state,
                          trailer_0_state,
                          trailer_1_state,
                          trailer_2_state,
                          eof_state );

  signal current_state, next_state : machine_states;

  signal dg_reset            : std_logic             := '1';
--  signal rst_n             : std_logic             := '1';
--  signal urstDelay1_n      : std_logic             := '1';
--  signal urstDelay2_n      : std_logic             := '1';
--  signal resetCnt          : integer range 0 to 15;
--  signal resetDelay        : integer range 0 to 15 := 10;
  signal lscEnable_delay_1  : std_logic             := '0';
  signal lscEnable_delay_2  : std_logic             := '0';
    
  signal channelCode: std_logic_vector(15 downto 0);
--  signal runNumber: std_logic_vector(15 downto 0);

  signal toggle:              std_logic;
  signal enable_toggle_clock: std_logic;
--  signal toggle: alternate select normal and reversed endianness header marker word

begin

-- set channel code
	channelCode <= x"11" & event_ID_counter(7 downto 0); -- or  x"0000";
	
-- static output
	lscTest_n <= '1';
	lrstProc:process
		variable cnt: integer range 0 to 256;
	begin
		wait until rising_edge(clk);
		if reset = '1' then 
			cnt := 0;
			lscReset_n <= '0';
		elsif cnt < 200 then
			cnt := cnt + 1;
		else
			lscReset_n <= '1';
		end if;
--		lscReset_n <= not reset;
	end process;

-------------------------------------------------------------------------------

-------------------------------------------------------------------------------
-- Data Counter clocking                ---------------------------------------
-------------------------------------------------------------------------------
  data_counter_clock_process : process(clk, dg_reset)
  begin
    if dg_reset = '1' then                 -- asynchronous dg_reset
      data_counter     <= (others => '0');
      data_value       <= (others => '0');
    else
      if rising_edge(clk) then
        if data_counter_preload = '1' then
		    data_value <= (others => '0');
          data_counter <= event_size;
        elsif data_counter_decrement = '1' then
          data_counter <= data_counter - 1;
		    data_value <= data_value + 1;
        end if;
      end if;
    end if;

  end process data_counter_clock_process;
-------------------------------------------------------------------------------
-- Event Counter clocking               ---------------------------------------
-------------------------------------------------------------------------------
  event_ID_counter_clock_process : process(clk, dg_reset)
  begin
    if dg_reset = '1' then                 -- asynchronous dg_reset
      event_ID_counter     <= (others => '0');
    else
      if rising_edge(clk) then
        if event_ID_counter_increment = '1' then
          event_ID_counter <= event_ID_counter + 1;
        end if;
      end if;
    end if;

  end process event_ID_counter_clock_process;

-------------------------------------------------------------------------------
-- This is the data selector. It selects which data is presented on LD  -------
-------------------------------------------------------------------------------
  odms : process (event_ID_counter, data_counter, selector, run_number, data_length,
                  channelCode, data_value)
  begin  -- process lcd

    control_word_flag <= '0';           -- set the default value

    case selector is

      when bof_select =>
        data_output       <= begining_of_fragment;
        control_word_flag <= '1';

      when header_0_select =>
        data_output <= rod_header(0);

      when header_1_select =>
        data_output <= rod_header(1);

      when header_2_select =>
        data_output <= rod_header(2);

      when header_3_select =>
        data_output <= rod_header(3);

      when header_4_select =>
        data_output <= x"0000" & run_number; -- rod_header(4);

      when header_5_select =>
        data_output <= event_ID_counter;

      when header_6_select =>
        data_output <= rod_header(6);

      when header_7_select =>
        data_output <= rod_header(7);

      when header_8_select =>
        data_output <= rod_header(8);

      when trailer_0_select =>
        data_output <= rod_trailer(0);

      when trailer_1_select                  =>
        data_output(15 downto 0)  <= data_length;
        data_output(31 downto 16) <= (others => '0');

      when trailer_2_select =>
        data_output <= rod_trailer(2);

      when eof_select =>
        data_output       <= end_of_fragment;
        control_word_flag <= '1';

      when data_element_select =>
        data_output(31 downto 0) <= channelCode & data_value; -- data_counter;

      when status_element_select =>
        data_output(31 downto 0) <= status_element;

      when others =>
        data_output <= x"0badfeed";

    end case;

  end process odms;

-------------------------------------------------------------------------------
-- Data length register                 ---------------------------------------
-------------------------------------------------------------------------------
  dlr : process(clk, dg_reset)
  begin
    if dg_reset = '1' then                 -- asynchronous reset
      data_length     <= (others => '0');
    else
      if rising_edge(clk) then
        if data_length_load = '1' then
          data_length <= event_size;    -- take into account one status word
        end if;
      end if;
    end if;

  end process dlr;


-------------------------------------------------------------------------------
-- Map data signals                     ---------------------------------------
-------------------------------------------------------------------------------
  map_data : process(toggle, control_word_flag, channelCode, data_output)
  begin
    if toggle = '1' and control_word_flag /= '1' and channelCode = "0000" then 
       lscData(31 downto 24)   <= data_output( 7 downto  0);
       lscData(23 downto 16)   <= data_output(15 downto  8);
       lscData(15 downto  8)   <= data_output(23 downto 16);
       lscData( 7 downto  0)   <= data_output(31 downto 24);
    else
       lscData(31 downto  0)   <= data_output(31 downto  0);
    end if;

  end process map_data;


-------------------------------------------------------------------------------
-- Map other signals                    ---------------------------------------
-------------------------------------------------------------------------------
  lscWen_n         <= not output_data_valid;
  lscCtrl_n        <= not control_word_flag;
  XOFF <= not lscFull_n;

-------------------------------------------------------------------------------
-- reset generator                      ---------------------------------------
-------------------------------------------------------------------------------                  

  dg_reset <=  not (lscEnable and lscEnable_delay_2) or reset;

  resetGen : process
  begin
    wait until rising_edge(clk);
    lscEnable_delay_1 <= lscEnable;
    lscEnable_delay_2 <= lscEnable_delay_1;

--    wait until rising_edge(clk);
--    urstDelay1_n <= ureset_n;
--    urstDelay2_n <= urstDelay1_n;
--    if urstDelay1_n = '0' and urstDelay2_n = '1' then
--      resetCnt   <= resetDelay;
--    elsif resetCnt /= 0 then
--      resetCnt   <= resetCnt - 1;
--    end if;
--    if resetCnt = 0 then
--      -- rst_n      <= '1';
--		dg_reset		  <= reset; -- '0';	-- drive main dg_reset also from reset
--    else
--      -- rst_n      <= '0';
--		dg_reset 	<= '1';
--    end if;

  end process;

-------------------------------------------------------------------------------
-- Toggle process               ---------------------------------------
-------------------------------------------------------------------------------
  toggle_process : process(clk, dg_reset)
  begin
    if dg_reset = '1' then
       toggle     <= '0';
    elsif rising_edge(clk) then
        if enable_toggle_clock = '1' then
           toggle  <= not toggle;
        end if;
    end if;

  end process toggle_process;

-------------------------------------------------------------------------------
-- State Machine clocking               ---------------------------------------
-------------------------------------------------------------------------------
  state_machine_clock_process : process(clk, dg_reset)
  begin
    if dg_reset = '1' then
      current_state   <= idle_state;
    else
      if rising_edge(clk) then
        current_state <= next_state;
      end if;
    end if;

  end process state_machine_clock_process;

-------------------------------------------------------------------------------
-- This is the state machine which steps through the event  -------------------
-------------------------------------------------------------------------------
-- Note XOFF only acted on during data (not header)  --------------------------
-------------------------------------------------------------------------------

  the_state_machine : process (event_size, XOFF, data_counter, current_state)
  begin

    event_ID_Counter_increment <= '0';
    data_counter_decrement     <= '0';
    output_data_valid          <= '0';
    next_state                 <= idle_state;
    data_counter_preload       <= '0';
    selector                   <= data_element_select;
    data_length_load           <= '0';
    enable_toggle_clock        <= '0';
    
    case current_state is

      when idle_state =>
        if event_size = x"00" or XOFF = '1' then
          next_state           <= idle_state;
        else
          data_counter_preload <= '1';
          data_length_load     <= '1';
          next_state           <= bof_state;
        end if;

      when bof_state =>
        output_data_valid    <= '1';
        selector             <= bof_select;
        next_state           <= header_0_state;
        
      when header_0_state =>
        output_data_valid <= '1';
        selector          <= header_0_select;
        next_state        <= header_1_state;

      when header_1_state =>
        output_data_valid <= '1';
        selector          <= header_1_select;
        next_state        <= header_2_state;

      when header_2_state =>
        output_data_valid <= '1';
        selector          <= header_2_select;
        next_state        <= header_3_state;

      when header_3_state =>
        output_data_valid <= '1';
        selector          <= header_3_select;
        next_state        <= header_4_state;

      when header_4_state =>
        output_data_valid <= '1';
        selector          <= header_4_select;
        next_state        <= header_5_state;

      when header_5_state =>
        output_data_valid          <= '1';
        selector                   <= header_5_select;
        next_state                 <= header_6_state;

      when header_6_state =>
        output_data_valid <= '1';
        selector          <= header_6_select;
        next_state        <= header_7_state;

      when header_7_state =>
        output_data_valid <= '1';
        selector          <= header_7_select;
        next_state        <= header_8_state;

      when header_8_state =>
        output_data_valid <= '1';
        selector          <= header_8_select;
        -- next_state        <= status_element_state;
        next_state        <= data_element_state;

      when status_element_state =>
        output_data_valid <= '1';
        selector          <= status_element_select;
        -- next_state        <= data_element_state;
        next_state        <= trailer_0_state;


      when data_element_state =>
        output_data_valid      <= '1';
        data_counter_decrement <= '1';
        selector               <= data_element_select;
        if data_counter = x"0001" then
          -- next_state           <= trailer_0_state;
          next_state           <= status_element_state;
        elsif XOFF = '1' then
          next_state           <= xoff_state;
        else
          next_state           <= data_element_state;
        end if;

      when trailer_0_state =>
        output_data_valid <= '1';
        selector          <= trailer_0_select;
        next_state        <= trailer_1_state;

      when trailer_1_state =>
        output_data_valid <= '1';
        selector          <= trailer_1_select;
        next_state        <= trailer_2_state;

      when trailer_2_state =>
        output_data_valid <= '1';
        selector          <= trailer_2_select;
        next_state        <= eof_state;

      when eof_state =>
        event_ID_counter_increment <= '1';
        enable_toggle_clock        <= '1';
        output_data_valid          <= '1';
        selector                   <= eof_select;
        if event_size = x"00" then
          next_state               <= idle_state;
        else
          data_counter_preload     <= '1';
          data_length_load         <= '1';
          next_state               <= bof_state;
        end if;

      when xoff_state =>
        output_data_valid      <= '0';
        data_counter_decrement <= '0';
        selector               <= data_element_select;
        if XOFF = '1' then
          next_state           <= xoff_state;
        else
          next_state           <= data_element_state;
        end if;

      when others => null;

    end case;

  end process the_state_machine;

-------------------------------------------------------------------------------


end Behavioral;
