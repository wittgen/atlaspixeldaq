library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity hola_monitoring_counters is
	port (
		-- system clocking
		clk : in std_logic;

		-- status inputs from both HOLAs
		hola0_lff_n : in std_logic;
		hola0_ldown_n : in std_logic;
		hola1_lff_n : in std_logic;
		hola1_ldown_n : in std_logic;

		-- counters
		count_hola0_lff : out std_logic_vector(15 downto 0);
		count_hola0_ldown : out std_logic_vector(15 downto 0);
		count_hola1_lff : out std_logic_vector(15 downto 0);
		count_hola1_ldown : out std_logic_vector(15 downto 0);

		-- counter reset
		count_reset : in std_logic
	);
end hola_monitoring_counters;

architecture rtl of hola_monitoring_counters is
	-- synchronizing shift registers
	signal hola0_lff_n_sr : std_logic_vector(3 downto 0) := (others => '1');
	signal hola0_ldown_n_sr : std_logic_vector(3 downto 0) := (others => '1');
	signal hola1_lff_n_sr : std_logic_vector(3 downto 0) := (others => '1');
	signal hola1_ldown_n_sr : std_logic_vector(3 downto 0) := (others => '1');

	-- internal counters
	signal count_hola0_lff_i : unsigned(15 downto 0) := (others => '0');
	signal count_hola0_ldown_i : unsigned(15 downto 0) := (others => '0');
	signal count_hola1_lff_i : unsigned(15 downto 0) := (others => '0');
	signal count_hola1_ldown_i : unsigned(15 downto 0) := (others => '0');
begin

-- synchronize lff/ldown to counter clock
process begin
	wait until rising_edge(clk);

	hola0_lff_n_sr <= hola0_lff_n_sr(2 downto 0) & hola0_lff_n;
	hola0_ldown_n_sr <= hola0_ldown_n_sr(2 downto 0) & hola0_ldown_n;
	hola1_lff_n_sr <= hola1_lff_n_sr(2 downto 0) & hola1_lff_n;
	hola1_ldown_n_sr <= hola1_ldown_n_sr(2 downto 0) & hola1_ldown_n;
end process;

-- counters
process begin
	wait until rising_edge(clk);

	if count_reset = '1' then
		count_hola0_ldown_i <= (others => '0');
		count_hola0_lff_i <= (others => '0');
		count_hola1_ldown_i <= (others => '0');
		count_hola1_lff_i <= (others => '0');
	else
		if (hola0_lff_n_sr(3 downto 2) = "10") and (count_hola0_lff_i /= (count_hola0_lff_i'range => '1')) then
			count_hola0_lff_i <= count_hola0_lff_i + 1;
		end if;

		if (hola0_ldown_n_sr(3 downto 2) = "10") and (count_hola0_ldown_i /= (count_hola0_ldown_i'range => '1')) then
			count_hola0_ldown_i <= count_hola0_ldown_i + 1;
		end if;

		if (hola1_lff_n_sr(3 downto 2) = "10") and (count_hola1_lff_i /= (count_hola1_lff_i'range => '1')) then
			count_hola1_lff_i <= count_hola1_lff_i + 1;
		end if;

		if (hola1_ldown_n_sr(3 downto 2) = "10") and (count_hola1_ldown_i /= (count_hola1_ldown_i'range => '1')) then
			count_hola1_ldown_i <= count_hola1_ldown_i + 1;
		end if;
	end if;
end process;

-- output counters
count_hola0_lff <= std_logic_vector(count_hola0_lff_i);
count_hola0_ldown <= std_logic_vector(count_hola0_ldown_i);
count_hola1_lff <= std_logic_vector(count_hola1_lff_i);
count_hola1_ldown <= std_logic_vector(count_hola1_ldown_i);

end architecture;
