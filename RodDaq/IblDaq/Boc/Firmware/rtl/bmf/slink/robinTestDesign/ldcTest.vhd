--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   13:38:53 07/11/2013
-- Design Name:   
-- Module Name:   /home/kugel/daten/work/projekte/atlas/afterTp/robArea/DAQ/DataFlow/robin_fpga/branches/pcie/rolTest/ldcTest.vhd
-- Project Name:  rolTest
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: holaldc_core
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
USE ieee.std_logic_unsigned.all;
USE ieee.numeric_std.ALL;
 
ENTITY ldcTest IS
    PORT(
         TXD0 : OUT  std_logic_vector(15 downto 0);
         TX_EN0 : OUT  std_logic;
         TX_ER0 : OUT  std_logic;
         RXD0 : IN  std_logic_vector(15 downto 0);
         RX_CLK0 : IN  std_logic;  -- ah15
         RX_ER0 : IN  std_logic;
         RX_DV0 : IN  std_logic;

         TXD1 : OUT  std_logic_vector(15 downto 0);
         TX_EN1 : OUT  std_logic;
         TX_ER1 : OUT  std_logic;
         RXD1 : IN  std_logic_vector(15 downto 0);
         RX_CLK1 : IN  std_logic;	-- h16
         RX_ER1 : IN  std_logic;
         RX_DV1 : IN  std_logic;

         TXD2 : OUT  std_logic_vector(15 downto 0);
         TX_EN2 : OUT  std_logic;
         TX_ER2 : OUT  std_logic;
         RXD2 : IN  std_logic_vector(15 downto 0);
         RX_CLK2 : IN  std_logic;	-- ad16
         RX_ER2 : IN  std_logic;
         RX_DV2 : IN  std_logic;
			--
			rol0_en: out std_logic; -- 1
			rol0_loop : out std_logic; -- 0
			rol0_prbs : out std_logic; -- 0                                               
			rol0_los : inout std_logic; -- input
			txdis0 : out std_logic; -- 0
			
			rol1_en: out std_logic;
			rol1_loop : out std_logic;
			rol1_prbs : out std_logic;                                                    
			rol1_los : inout std_logic;
			txdis1 : out std_logic;

			rol2_en: out std_logic;
			rol2_loop : out std_logic;
			rol2_prbs : out std_logic;                                                    
			rol2_los : inout std_logic;
			txdis2 : out std_logic;

			--
         XCLK : IN  std_logic  -- pin ae15: rclk0
        );
END ldcTest;
 
ARCHITECTURE behavior OF ldcTest IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT holaldc_core
	generic (
		ALTERA_XILINX    : integer :=    0;                  -- 1=Altera 0=Xilinx
		SIMULATION       : integer :=    0;                  -- simulation mode
		LCLK_SELECT      : integer :=    1;                  -- LCLK generator
																			  -- selection
																			  -- 0 -> no clk gen.
																			  -- 1 -> 40MHz clk gen.
																			  -- 2 -> 62.5MHz clk gen.
		ACTIVITY_LENGTH  : integer :=   24;                  -- ACTLED duration
		FIFODEPTH        : integer :=  512;                  -- only powers of 2
		LOG2DEPTH        : integer :=    9;                  -- 2log of depth
		FULLMARGIN       : integer :=  256                   -- words left when
																			  -- LFF_N set
  );
    PORT(
         POWER_UP_RST_N : IN  std_logic;
         LD : OUT  std_logic_vector(31 downto 0);
         UXOFF_N : IN  std_logic;
         URESET_N : IN  std_logic;
         UTDO_N : IN  std_logic;
         LCTRL_N : OUT  std_logic;
         LWEN_N : OUT  std_logic;
         LCLK : OUT  std_logic;
         LDERR_N : OUT  std_logic;
         URL : IN  std_logic_vector(3 downto 0);
         LDOWN_N : OUT  std_logic;
         TESTLED_N : OUT  std_logic;
         LDERRLED_N : OUT  std_logic;
         LUPLED_N : OUT  std_logic;
         FLOWCTLLED_N : OUT  std_logic;
         ACTIVITYLED_N : OUT  std_logic;
         XCLK : IN  std_logic;
         ENABLE : OUT  std_logic;
         TXD : OUT  std_logic_vector(15 downto 0);
         TX_EN : OUT  std_logic;
         TX_ER : OUT  std_logic;
         RXD : IN  std_logic_vector(15 downto 0);
         RX_CLK : IN  std_logic;
         RX_ER : IN  std_logic;
         RX_DV : IN  std_logic;
         LCLK_IN : IN  std_logic;
         DLL_RESET : IN  std_logic
        );
    END COMPONENT;

  component XLCLK625
    port (
      RST_IN    : in  std_logic;        --
      CLKIN_IN  : in  std_logic;        -- 125 MHz input
      CLKFX_OUT : out std_logic;        -- 40 MHz output
      CLK0_OUT  : out std_logic
      );
  end component;
    
component rolIla
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(40 DOWNTO 0));

end component;

component rolIcon
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL2 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

end component;

component tlkIla
  PORT (
    CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CLK : IN STD_LOGIC;
    TRIG0 : IN STD_LOGIC_VECTOR(17 DOWNTO 0));

end component;

component rolIconLarge
  PORT (
    CONTROL0 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL1 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL2 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL3 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL4 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL5 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL6 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL7 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
    CONTROL8 : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0));

end component;


COMPONENT clkGen
PORT(
	CLKIN_IN : IN std_logic;
	RST_IN : IN std_logic;          
	CLKDV_OUT : OUT std_logic;
	CLKIN_IBUFG_OUT : OUT std_logic;
	CLK0_OUT : OUT std_logic
	);
END COMPONENT;



signal CONTROL0 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL1 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL2 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL3 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL4 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL5 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL6 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL7 :  STD_LOGIC_VECTOR(35 DOWNTO 0);
signal CONTROL8 :  STD_LOGIC_VECTOR(35 DOWNTO 0);

signal TRIG0 :  STD_LOGIC_VECTOR(40 DOWNTO 0);
signal TRIG1 :  STD_LOGIC_VECTOR(40 DOWNTO 0);
signal TRIG2 :  STD_LOGIC_VECTOR(40 DOWNTO 0);

signal TXTRIG0 :  STD_LOGIC_VECTOR(17 DOWNTO 0);
signal TXTRIG1 :  STD_LOGIC_VECTOR(17 DOWNTO 0);
signal TXTRIG2 :  STD_LOGIC_VECTOR(17 DOWNTO 0);

signal RXTRIG0 :  STD_LOGIC_VECTOR(17 DOWNTO 0);
signal RXTRIG1 :  STD_LOGIC_VECTOR(17 DOWNTO 0);
signal RXTRIG2 :  STD_LOGIC_VECTOR(17 DOWNTO 0);

   --Inputs
   signal POWER_UP_RST_N : std_logic := '0';

   signal XCLK_int : std_logic := '0';
   signal DLL_RESET : std_logic := '0';
   signal LCLK : std_logic;
	
	--
   signal UXOFF_N0 : std_logic := '1';
   signal URESET_N0 : std_logic := '1';
   signal UTDO_N0 : std_logic := '0';
   signal URL0 : std_logic_vector(3 downto 0) := (others => '0');

   signal LD0 : std_logic_vector(31 downto 0);
   signal LCTRL_N0 : std_logic;
   signal LWEN_N0 : std_logic;
   signal LDERR_N0 : std_logic;
   signal LDOWN_N0 : std_logic;
   signal TESTLED_N0 : std_logic;
   signal LDERRLED_N0 : std_logic;
   signal LUPLED_N0 : std_logic;
   signal FLOWCTLLED_N0 : std_logic;
   signal ACTIVITYLED_N0 : std_logic;
   signal ENABLE0 : std_logic;

	--
   signal UXOFF_N1 : std_logic := '1';
   signal URESET_N1 : std_logic := '1';
   signal UTDO_N1 : std_logic := '0';
   signal URL1 : std_logic_vector(3 downto 0) := (others => '0');

   signal LD1 : std_logic_vector(31 downto 0);
   signal LCTRL_N1 : std_logic;
   signal LWEN_N1 : std_logic;
   signal LDERR_N1 : std_logic;
   signal LDOWN_N1 : std_logic;
   signal TESTLED_N1 : std_logic;
   signal LDERRLED_N1 : std_logic;
   signal LUPLED_N1 : std_logic;
   signal FLOWCTLLED_N1 : std_logic;
   signal ACTIVITYLED_N1 : std_logic;
   signal ENABLE1 : std_logic;
	--
	
   signal UXOFF_N2 : std_logic := '1';
   signal URESET_N2 : std_logic := '1';
   signal UTDO_N2 : std_logic := '0';
   signal URL2 : std_logic_vector(3 downto 0) := (others => '0');

   signal LD2 : std_logic_vector(31 downto 0);
   signal LCTRL_N2 : std_logic;
   signal LWEN_N2 : std_logic;
   signal LDERR_N2 : std_logic;
   signal LDOWN_N2 : std_logic;
   signal TESTLED_N2 : std_logic;
   signal LDERRLED_N2 : std_logic;
   signal LUPLED_N2 : std_logic;
   signal FLOWCTLLED_N2 : std_logic;
   signal ACTIVITYLED_N2 : std_logic;
   signal ENABLE2 : std_logic;

	signal txd0_i, txd1_i, txd2_i: std_logic_vector(15 downto 0);
	signal tx_er0_i, tx_er1_i, tx_er2_i: std_logic;
	signal tx_en0_i, tx_en1_i, tx_en2_i: std_logic;

	signal rxd0_i, rxd1_i, rxd2_i: std_logic_vector(15 downto 0);
	signal rx_er0_i, rx_er1_i, rx_er2_i: std_logic;
	signal rx_dv0_i, rx_dv1_i, rx_dv2_i: std_logic;

-- ROL fifo setting
  -- constant HOLA_FIFO_DEPTH  : integer  := 32; -- other option : 512
  constant HOLA_FIFO_DEPTH  : integer  := 512; -- other option : 512
  -- constant HOLA_LOG2_DEPTH  : integer  := 5; -- or 9 for 512
  -- constant HOLA_LOG2_DEPTH  : integer  := 4; -- With CoreGen 6.2 the max fifo count is 1 bit less than before
  constant HOLA_LOG2_DEPTH  : integer  := 9; -- With CoreGen 6.2 the max fifo count is 1 bit less than before

	signal rstCnt: std_logic_vector(9 downto 0) := (others => '0');
	constant rstCntMax: std_logic_vector(9 downto 0) := (others => '1');

BEGIN

	rsProc:process
	begin
		wait until rising_edge(xclk_int);
		if rstCnt /= rstCntMax then
			POWER_UP_RST_N <= '0';
			rstCnt <= rstCnt + 1;
		else
			POWER_UP_RST_N <= '1';
		end if;
	end process;
	
--	XLCLK625_01 : XLCLK625
--	  port map(
--		 RST_IN    => '0',
--		 CLKIN_IN  => XCLK,            -- 125 MHz input
--		 CLKFX_OUT => LCLK,            -- 62.5 MHz output
--		 CLK0_OUT  => XCLK_int         -- XCLK output
--		 );

	Inst_clkGen: clkGen PORT MAP(
		CLKIN_IN => XCLK,
		RST_IN => '0',
		CLKDV_OUT => LCLK,
		CLKIN_IBUFG_OUT => open,
		CLK0_OUT => XCLK_int
	);




	rol0_en <= '1';
	rol0_loop <= '0';
	rol0_prbs <= '0';
	rol0_los <= 'Z';
	txdis0 <= '0';

	rol1_en <= '1';
	rol1_loop <= '0';
	rol1_prbs <= '0';
	rol1_los <= 'Z';
	txdis1 <= '0';

	rol2_en <= '1';
	rol2_loop <= '0';
	rol2_prbs <= '0';
	rol2_los <= 'Z';
	txdis2 <= '0';

icon: rolIconLarge
  PORT map (
    CONTROL0 => CONTROL0,
    CONTROL1 => CONTROL1,
    CONTROL2 => CONTROL2,
    CONTROL3 => CONTROL3,
    CONTROL4 => CONTROL4,
    CONTROL5 => CONTROL5,
    CONTROL6 => CONTROL6,
    CONTROL7 => CONTROL7,
    CONTROL8 => CONTROL8
	 );

ila0: rolIla
	port map (
    CONTROL => control0,
    CLK => xclk_int,
    TRIG0 => trig0);

txila0: tlkIla
	port map (
    CONTROL => control1,
    CLK => xclk_int,
    TRIG0 => txtrig0);

rxila0: tlkIla
	port map (
    CONTROL => control2,
    CLK => RX_CLK0,
    TRIG0 => rxtrig0);

ila1: rolIla
	port map (
    CONTROL => control3,
    CLK => xclk_int,
    TRIG0 => trig1);
	 
txila1: tlkIla
	port map (
    CONTROL => control4,
    CLK => xclk_int,
    TRIG0 => txtrig1);

rxila1: tlkIla
	port map (
    CONTROL => control5,
    CLK => RX_CLK1,
    TRIG0 => rxtrig1);
	 
ila2: rolIla
	port map (
    CONTROL => control6,
    CLK => xclk_int,
    TRIG0 => trig2);
txila2: tlkIla
	port map (
    CONTROL => control7,
    CLK => xclk_int,
    TRIG0 => txtrig2);

rxila2: tlkIla
	port map (
    CONTROL => control8,
    CLK => RX_CLK2,
    TRIG0 => rxtrig2);
 
   ldc0: holaldc_core 
	   generic map ( 
      SIMULATION => 0,
      LCLK_SELECT => 0,
      ACTIVITY_LENGTH => 24,
      FIFODEPTH => HOLA_FIFO_DEPTH,
      LOG2DEPTH => HOLA_LOG2_DEPTH,
      FULLMARGIN => HOLA_FIFO_DEPTH / 2,
      ALTERA_XILINX => 0)
		PORT MAP (
          POWER_UP_RST_N => POWER_UP_RST_N,
          LD => LD0,
          UXOFF_N => UXOFF_N0,
          URESET_N => URESET_N0,
          UTDO_N => UTDO_N0,
          LCTRL_N => LCTRL_N0,
          LWEN_N => LWEN_N0,
          LCLK => LCLK,
          LDERR_N => LDERR_N0,
          URL => URL0,
          LDOWN_N => LDOWN_N0,
          TESTLED_N => TESTLED_N0,
          LDERRLED_N => LDERRLED_N0,
          LUPLED_N => LUPLED_N0,
          FLOWCTLLED_N => FLOWCTLLED_N0,
          ACTIVITYLED_N => ACTIVITYLED_N0,
          XCLK => XCLK_int,
          ENABLE => ENABLE0,
          TXD => TXD0_i,
          TX_EN => TX_EN0_i,
          TX_ER => TX_ER0_i,
          RXD => RXD0_i,
          RX_CLK => RX_CLK0,
          RX_ER => RX_ER0_i,
          RX_DV => RX_DV0_i,
          LCLK_IN => LCLK,
          DLL_RESET => DLL_RESET
        );
		  
	rx0proc: process
	begin
		wait until rising_edge(rx_clk0);
		rxd0_i <= rxd0;
		rx_er0_i <= rx_er0;
		rx_dv0_i <= rx_dv0;
	end process;

	trig0(31 downto 0) <= ld0;
	trig0(32) <= LCTRL_N0;
	trig0(33) <= LWEN_N0;
	trig0(34) <= LDERR_N0;
	trig0(35) <= LDOWN_N0;
	trig0(36) <= TESTLED_N0;
	trig0(37) <= LDERRLED_N0;
	trig0(38) <= LUPLED_N0;
	trig0(39) <= FLOWCTLLED_N0;
	trig0(40) <= ACTIVITYLED_N0;
	txtrig0(15 downto 0) <= txd0_i;
	txtrig0(16) <= tx_er0_i;
	txtrig0(17) <= tx_en0_i;
	tx0proc: process
	begin
		wait until rising_edge(XCLK_int);
		txd0 <= txd0_i;
		tx_er0 <= tx_er0_i;
		tx_en0 <= tx_en0_i;
	end process;
	rxtrig0(15 downto 0) <= rxd0;
	rxtrig0(16) <= rx_er0;
	rxtrig0(17) <= rx_dv0;
	
	
   ldc1: holaldc_core 
	   generic map ( 
      SIMULATION => 0,
      LCLK_SELECT => 0,
      ACTIVITY_LENGTH => 24,
      FIFODEPTH => HOLA_FIFO_DEPTH,
      LOG2DEPTH => HOLA_LOG2_DEPTH,
      FULLMARGIN => HOLA_FIFO_DEPTH / 2,
      ALTERA_XILINX => 0)
	PORT MAP (
          POWER_UP_RST_N => POWER_UP_RST_N,
          LD => LD1,
          UXOFF_N => UXOFF_N1,
          URESET_N => URESET_N1,
          UTDO_N => UTDO_N1,
          LCTRL_N => LCTRL_N1,
          LWEN_N => LWEN_N1,
          LCLK => LCLK,
          LDERR_N => LDERR_N1,
          URL => URL1,
          LDOWN_N => LDOWN_N1,
          TESTLED_N => TESTLED_N1,
          LDERRLED_N => LDERRLED_N1,
          LUPLED_N => LUPLED_N1,
          FLOWCTLLED_N => FLOWCTLLED_N1,
          ACTIVITYLED_N => ACTIVITYLED_N1,
          XCLK => XCLK_int,
          ENABLE => ENABLE1,
          TXD => TXD1_i,
          TX_EN => TX_EN1_i,
          TX_ER => TX_ER1_i,
          RXD => RXD1_i,
          RX_CLK => RX_CLK1,
          RX_ER => RX_ER1_i,
          RX_DV => RX_DV1_i,
          LCLK_IN => LCLK,
          DLL_RESET => DLL_RESET
        );
	rx1proc: process
	begin
		wait until rising_edge(rx_clk1);
		rxd1_i <= rxd1;
		rx_er1_i <= rx_er1;
		rx_dv1_i <= rx_dv1;
	end process;
	trig1(31 downto 0) <= ld1;
	trig1(32) <= LCTRL_N1;
	trig1(33) <= LWEN_N1;
	trig1(34) <= LDERR_N1;
	trig1(35) <= LDOWN_N1;
	trig1(36) <= TESTLED_N1;
	trig1(37) <= LDERRLED_N1;
	trig1(38) <= LUPLED_N1;
	trig1(39) <= FLOWCTLLED_N1;
	trig1(40) <= ACTIVITYLED_N1;
	txtrig1(15 downto 0) <= txd1_i;
	txtrig1(16) <= tx_er1_i;
	txtrig1(17) <= tx_en1_i;
	tx1proc: process
	begin
		wait until rising_edge(XCLK_int);
		txd1 <= txd1_i;
		tx_er1 <= tx_er1_i;
		tx_en1 <= tx_en1_i;
	end process;
	rxtrig1(15 downto 0) <= rxd1;
	rxtrig1(16) <= rx_er1;
	rxtrig1(17) <= rx_dv1;
	
   ldc2: holaldc_core 
	   generic map ( 
      SIMULATION => 0,
      LCLK_SELECT => 0,
      ACTIVITY_LENGTH => 24,
      FIFODEPTH => HOLA_FIFO_DEPTH,
      LOG2DEPTH => HOLA_LOG2_DEPTH,
      FULLMARGIN => HOLA_FIFO_DEPTH / 2,
      ALTERA_XILINX => 0)
	PORT MAP (
          POWER_UP_RST_N => POWER_UP_RST_N,
          LD => LD2,
          UXOFF_N => UXOFF_N2,
          URESET_N => URESET_N2,
          UTDO_N => UTDO_N2,
          LCTRL_N => LCTRL_N2,
          LWEN_N => LWEN_N2,
          LCLK => LCLK,
          LDERR_N => LDERR_N2,
          URL => URL2,
          LDOWN_N => LDOWN_N2,
          TESTLED_N => TESTLED_N2,
          LDERRLED_N => LDERRLED_N2,
          LUPLED_N => LUPLED_N2,
          FLOWCTLLED_N => FLOWCTLLED_N2,
          ACTIVITYLED_N => ACTIVITYLED_N2,
          XCLK => XCLK_int,
          ENABLE => ENABLE2,
          TXD => TXD2_i,
          TX_EN => TX_EN2_i,
          TX_ER => TX_ER2_i,
          RXD => RXD2_i,
          RX_CLK => RX_CLK2,
          RX_ER => RX_ER2_i,
          RX_DV => RX_DV2_i,
          LCLK_IN => LCLK,
          DLL_RESET => DLL_RESET
        );
	rx2proc: process
	begin
		wait until rising_edge(rx_clk2);
		rxd2_i <= rxd2;
		rx_er2_i <= rx_er2;
		rx_dv2_i <= rx_dv2;
	end process;
	trig2(31 downto 0) <= ld2;
	trig2(32) <= LCTRL_N2;
	trig2(33) <= LWEN_N2;
	trig2(34) <= LDERR_N2;
	trig2(35) <= LDOWN_N2;
	trig2(36) <= TESTLED_N2;
	trig2(37) <= LDERRLED_N2;
	trig2(38) <= LUPLED_N2;
	trig2(39) <= FLOWCTLLED_N2;
	trig2(40) <= ACTIVITYLED_N2;
	
	txtrig2(15 downto 0) <= txd2_i;
	txtrig2(16) <= tx_er2_i;
	txtrig2(17) <= tx_en2_i;
	tx2proc: process
	begin
		wait until rising_edge(XCLK_int);
		txd2 <= txd2_i;
		tx_er2 <= tx_er2_i;
		tx_en2 <= tx_en2_i;
	end process;
	rxtrig2(15 downto 0) <= rxd2;
	rxtrig2(16) <= rx_er2;
	rxtrig2(17) <= rx_dv2;
 

END;
