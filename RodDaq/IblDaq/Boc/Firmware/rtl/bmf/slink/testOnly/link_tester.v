// author: philipp schaefer
//
// generate data and test correct data transmission of hola link

module link_tester (
	input wire uclk,
	input wire lclk,
	input wire res_n,
	input wire lsc_down_n,
	input wire ldc_down_n,
	output reg [31:0] data2lsc,
	input wire [31:0] data_from_ldc,
	input wire lff_n,
	output reg u_res_n,
	output reg uwen_n,
	output reg uctrl_n,
	input wire lwen_n
);

reg [31:0] ld_I;

always @ (posedge uclk or negedge res_n)
if (!res_n)
	ld_I <= 32'b0;
else
	ld_I <= data_from_ldc;

// reset ldc and lsc while links are down
reg res_pulled;
reg [1:0] res_cnt;

always @ (posedge uclk or negedge res_n)
	if (!res_n) begin
		u_res_n <= 1'b0;
		res_pulled <= 1'b0;
		res_cnt <= 2'b0;
	end
	else begin
		if (lsc_down_n & ldc_down_n & !res_pulled) begin
			u_res_n <= 1'b0;
			res_pulled <= 1'b1;
			res_cnt <= 2'b0;
		end
		else if (res_pulled & (res_cnt != 2'b11)) begin
			res_pulled <= 1'b1;
			u_res_n <= 1'b0;
			res_cnt <= res_cnt + 1'b1;
		end
		else begin
			u_res_n <= 1'b1;
			res_cnt <= res_cnt;
			res_pulled <= res_pulled;
		end
	end

wire [2:0] lsc_state;
assign lsc_state = {!u_res_n, lff_n, lsc_down_n};
reg [7:0] cnt_data;
reg [31:0] data_lsc;

always @ (posedge uclk or negedge res_n)
	if(!res_n) begin
		data_lsc <= 32'b0;
		data2lsc <= 32'b0;
		uwen_n <= 1'b1;
		uctrl_n <= 1'b1;
		cnt_data <= 8'b0;
	end
	else begin
		casex(lsc_state)
			3'b1xx: begin
				data_lsc <= 32'b0;
				data2lsc <= 32'b0;
				uwen_n <= 1'b1;
				uctrl_n <= 1'b1;
				cnt_data <= 8'b0;
			end
			3'b00x: begin
				data_lsc <= data2lsc;
				data2lsc <= data2lsc;
				uwen_n <= 1'b1;
				uctrl_n <= 1'b1;
				cnt_data <= cnt_data;
			end
			3'b011: begin
				if (cnt_data == 8'hff)
					data2lsc <= 32'hb0f00000;
				else if (cnt_data == 8'h00)
					data2lsc <= 32'he0f00000;
				else
					data2lsc <= data_lsc;
				data_lsc <= (cnt_data != 8'hff && cnt_data != 8'h00) ? data_lsc + 1'b1 : data_lsc;
				uwen_n <= 1'b0;
				uctrl_n <= ((cnt_data != 8'hff) && (cnt_data  != 8'h00)) ? 1'b1 : 1'b0;
				cnt_data <= cnt_data + 1'b1;
			end
			default: begin
				data_lsc <= data2lsc;
				data2lsc <= data2lsc;
				uwen_n <= 1'b1;
				uctrl_n <= 1'b1;
				cnt_data <= cnt_data;
			end
		endcase
	end

wire [2:0] ldc_state;
assign ldc_state = {!u_res_n, lwen_n, ldc_down_n};

reg [31:0] data_cnt;
reg data_error;

always @ (posedge lclk or negedge res_n)
	if (!res_n) begin
		data_error <= 1'b0;
		data_cnt <= 32'b0;
	end
	else begin
		casex(ldc_state)
			3'b1xx: begin
				data_error <= 1'b0;
				data_cnt <= 32'b0;
			end
			3'b001: begin
				data_cnt <= data_cnt + 1'b1;
				data_error <= (data_cnt == ld_I);
			end
			3'b01x: begin
				data_error <= data_error;
				data_cnt <= data_cnt;
			end
			default: begin
				data_error <= data_error;
				data_cnt <= data_cnt;
			end
		endcase
	end

endmodule // link_tester
