----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    13:40:16 06/21/2013 
-- Design Name: 
-- Module Name:    slTest - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity slTest is
	 generic (slink_ref_clk: integer := 100; simulation: integer := 0); -- 100 or 80
    Port ( 
		clk40p : in  STD_LOGIC;
		clk40n : in  STD_LOGIC;
		rst : in  STD_LOGIC;
		
		lsc_sin0_p : in std_logic;
		lsc_sin0_n : in std_logic;
		lsc_sout0_p : out std_logic;
		lsc_sout0_n : out std_logic;
		lsc_sin1_p : in std_logic;
		lsc_sin1_n : in std_logic;
		lsc_sout1_p : out std_logic;
		lsc_sout1_n : out std_logic;

		ldc_sin0_p : in std_logic;
		ldc_sin0_n : in std_logic;
		ldc_sout0_p : out std_logic;
		ldc_sout0_n : out std_logic;
		ldc_sin1_p : in std_logic;
		ldc_sin1_n : in std_logic;
		ldc_sout1_p : out std_logic;
		ldc_sout1_n : out std_logic;

		led0 : out  STD_LOGIC;
		led1 : out  STD_LOGIC;
		led2 : out  STD_LOGIC
			  );
end slTest;

architecture Behavioral of slTest is


component MGT_USRCLK_SOURCE_PLL 
generic
(
    MULT                 : integer          := 2;
    DIVIDE               : integer          := 2;
    FEEDBACK             : string           := "CLKFBOUT";    
    CLK_PERIOD           : real             := 12.5;
    OUT0_DIVIDE          : integer          := 2;
    OUT1_DIVIDE          : integer          := 2;
    OUT2_DIVIDE          : integer          := 2;
    OUT3_DIVIDE          : integer          := 2   
);
port
( 
    CLK0_OUT                : out std_logic;
    CLK1_OUT                : out std_logic;
    CLK2_OUT                : out std_logic;
    CLK3_OUT                : out std_logic;
    CLK_IN                  : in  std_logic;
    CLKFB_IN                : in  std_logic;    
    CLKFB_OUT               : out std_logic;      
    PLL_LOCKED_OUT          : out std_logic;
    PLL_RESET_IN            : in  std_logic
);
end component;


	COMPONENT hola_lsc_s6
	 generic (slink_ref_clk: integer := 100; simulation: integer);
	PORT(
		mgtrefclk : IN std_logic;
		sys_rst : IN std_logic;
		mgt_rst : IN std_logic;
		usr_clk_in : IN std_logic;
		usr_clk20_in : IN std_logic;
		ud : IN std_logic_vector(31 downto 0);
		ureset_n : IN std_logic;
		utest_n : IN std_logic;
		uctrl_n : IN std_logic;
		uwen_n : IN std_logic;
		uclk : IN std_logic;
		hola_enable_0 : IN std_logic;
		hola_enable_1 : IN std_logic;
		tlk_sin0_p : IN std_logic;
		tlk_sin0_n : IN std_logic;
		tlk_sin1_p : IN std_logic;
		tlk_sin1_n : IN std_logic;          
		gtpclkout : OUT std_logic;
		gtp_plldet_out : OUT std_logic;
		lff_n : OUT std_logic;
		lrl0 : OUT std_logic_vector(3 downto 0);
		lrl1 : OUT std_logic_vector(3 downto 0);
		ldown_n : OUT std_logic;
		tlk_sout0_p : OUT std_logic;
		tlk_sout0_n : OUT std_logic;
		tlk_sout1_p : OUT std_logic;
		tlk_sout1_n : OUT std_logic;
		testled_n : OUT std_logic;
		lderrled_n : OUT std_logic;
		lupled_n : OUT std_logic;
		flowctlled_n : OUT std_logic;
		activityled_n : OUT std_logic;
		usr_clk_out : OUT std_logic;
		gtx_reset_done : OUT std_logic
		);
	END COMPONENT;

	COMPONENT hola_ldc_s6
	 generic (slink_ref_clk: integer := 100; simulation: integer);
	PORT(
		mgtrefclk : IN std_logic;
		sys_rst : IN std_logic;
		mgt_rst : IN std_logic;
		usr_clk_in : IN std_logic;
		usr_clk20_in : IN std_logic;
		uxoff_n : IN std_logic;
		utdo_n : IN std_logic;
		ureset_n : IN std_logic;
		url : IN std_logic_vector(3 downto 0);
		lclk_in : IN std_logic;
		dll_reset : IN std_logic;
		tlk_sin0_p : IN std_logic;
		tlk_sin0_n : IN std_logic;
		tlk_sin1_p : IN std_logic;
		tlk_sin1_n : IN std_logic;          
		gtpclkout : OUT std_logic;
		gtp_plldet_out : OUT std_logic;
		ld0 : OUT std_logic_vector(31 downto 0);
		ld1 : OUT std_logic_vector(31 downto 0);
		lctrl_n : OUT std_logic;
		lwen_n : OUT std_logic;
		lclk : OUT std_logic;
		lderr_n : OUT std_logic;
		ldown_n : OUT std_logic;
		tlk_sout0_p : OUT std_logic;
		tlk_sout0_n : OUT std_logic;
		tlk_sout1_p : OUT std_logic;
		tlk_sout1_n : OUT std_logic;
		testled_n : OUT std_logic;
		lderrled_n : OUT std_logic;
		lupled_n : OUT std_logic;
		flowctlled_n : OUT std_logic;
		activityled_n : OUT std_logic
		);
	END COMPONENT;

	COMPONENT link_tester
	PORT(
		uclk : IN std_logic;
		lclk : IN std_logic;
		res_n : IN std_logic;
		lsc_down_n : IN std_logic;
		ldc_down_n : IN std_logic;
		data_from_ldc : IN std_logic_vector(31 downto 0);
		lff_n : IN std_logic;
		lwen_n : IN std_logic;          
		data2lsc : OUT std_logic_vector(31 downto 0);
		u_res_n : OUT std_logic;
		uwen_n : OUT std_logic;
		uctrl_n : OUT std_logic
		);
	END COMPONENT;

	component bmf_clocking is
	 generic (slink_ref_clk: integer := 100);
		port
		(
			-- clock input
			clk40_p : in std_logic;
			clk40_n : in std_logic;

			-- reset input
			reset : in std_logic;

			-- clock outputs
			clk10 : out std_logic;
			clk40 : out std_logic;
			clk160 : out std_logic;
			clk160_90 : out std_logic;
			clk640a : out std_logic;
			clk640b : out std_logic;
			-- with new slink reference clock
			CLK_SLINK : out std_logic;

			-- clock enable outputs
			ce40 : out std_logic;
			ce80a : out std_logic;
			ce80b : out std_logic;

			-- status outputs
			locked : out std_logic;
			synced : out std_logic
		);
	end component;

	------------------
	signal clk_slink : std_logic := '0';

	signal clk10 : std_logic := '0';
	signal clk40 : std_logic := '0';
	signal clk160 : std_logic := '0';
	signal clk160_90 : std_logic := '0';
	signal clk640a : std_logic := '0';
	signal clk640b : std_logic := '0';
	signal ce40 : std_logic := '0';
	signal ce80a : std_logic := '0';
	signal ce80b : std_logic := '0';
	signal locked : std_logic := '0';
	signal synced : std_logic := '0';

	-- --------------------------------------
	-- generate MGT user clock
	signal gtpclk_to_cmt: std_logic := '0';
	signal gtpclk_fb: std_logic := '0';
	signal gtpclk_rst: std_logic := '0';
	signal gtp_locked: std_logic := '0';


	signal gtpclk: std_logic := '0';
	signal gtp_plldet_out: std_logic := '0';
	signal usr_clk: std_logic := '0';
	signal usr_clk20: std_logic := '0';

	signal sys_rst: std_logic := '0';
	signal sys_rst_n: std_logic;
	signal gtx_reset_done: std_logic := '0';

	-- slink signals
-- source 
signal ud: std_logic_vector(31 downto 0);
signal utest_n: std_logic := '1';
signal ureset_n: std_logic := '0';
signal uctrl_n: std_logic := '0';
signal uwen_n: std_logic := '0';
signal lff_n: std_logic := '0';
signal lrl0: std_logic_vector(3 downto 0) := (others => '0'); 
signal lrl1: std_logic_vector(3 downto 0) := (others => '0'); 
signal lsc_ldown_n: std_logic := '0';
	
-- destination(s)	 
signal ld0, ld1 : std_logic_vector(31 downto 0);
signal uxoff_n: std_logic := '1';
signal utdo_n: std_logic := '1';
signal lctrl_n: std_logic := '0';
signal lwen_n: std_logic := '0';
signal lclk: std_logic := '0';
signal lderr_n: std_logic := '0';
signal url : std_logic_vector(3 downto 0) := "1000";
signal ldc_ldown_n: std_logic := '0';



begin

	sys_rst <= not locked;
	sys_rst_n <= not sys_rst;

	-- clock generation
	bmf_clocking_i: bmf_clocking 
	generic map (slink_ref_clk => slink_ref_clk)
	PORT MAP(
		clk40_p => CLK40P,
		clk40_n => CLK40N,
		reset => rst,
		clk_slink => clk_slink,
		clk10 => clk10,
		clk40 => clk40,
		clk160 => clk160,
		clk160_90 => clk160_90,
		clk640a => clk640a,
		clk640b => clk640b,
		ce40 => ce40,
		ce80a => ce80a,
		ce80b => ce80b,
		locked => locked,
		synced => synced
	);

	 gtpclk_rst     <=  not gtp_plldet_out;


	-- mgt clocking

    gtpclkout0_0_pll0_bufio2_i : BUFIO2
    generic map
    (
        DIVIDE                          =>      1,
        DIVIDE_BYPASS                   =>      TRUE
    )
    port map
    (
        I                               =>      gtpclk, -- tile0_gtpclkout0_i(0),
        DIVCLK                          =>      gtpclk_to_cmt, -- tile0_gtpclkout0_0_to_cmt_i,
        IOCLK                           =>      open,
        SERDESSTROBE                    =>      open
    );

	
refclk100: if slink_ref_clk = 100 generate
begin

    gtpclkout0_0_pll0_i : MGT_USRCLK_SOURCE_PLL
    generic map
    (
--	100 mhz ref clock settings
        MULT                            =>      8,
        DIVIDE                          =>      1,
        FEEDBACK                        =>      "CLKFBOUT",
        CLK_PERIOD                      =>      10.0,
        OUT0_DIVIDE                     =>      8,
        OUT1_DIVIDE                     =>      4,
        OUT2_DIVIDE                     =>      1,
        OUT3_DIVIDE                     =>      1
    )
    port map
    (
        CLK0_OUT                        =>      usr_clk20,
        CLK1_OUT                        =>      usr_clk,
        CLK2_OUT                        =>      open,
        CLK3_OUT                        =>      open,
        CLK_IN                          =>      gtpclk_to_cmt,
        CLKFB_IN                        =>      gtpclk_fb,
        CLKFB_OUT                       =>      gtpclk_fb,
        PLL_LOCKED_OUT                  =>      gtp_locked,
        PLL_RESET_IN                    =>      gtpclk_rst
    );
end generate;	 

refclk80: if slink_ref_clk /= 100 generate
begin

    gtpclkout0_0_pll0_i : MGT_USRCLK_SOURCE_PLL
    generic map
    (
-- 80 mhz ref clock settings
        MULT                            =>      10,
        DIVIDE                          =>      1,
        FEEDBACK                        =>      "CLKFBOUT",
        CLK_PERIOD                      =>      12.5,
        OUT0_DIVIDE                     =>      8,
        OUT1_DIVIDE                     =>      4,
        OUT2_DIVIDE                     =>      1,
        OUT3_DIVIDE                     =>      1
    )
    port map
    (
        CLK0_OUT                        =>      usr_clk20,
        CLK1_OUT                        =>      usr_clk,
        CLK2_OUT                        =>      open,
        CLK3_OUT                        =>      open,
        CLK_IN                          =>      gtpclk, --_to_cmt,
        CLKFB_IN                        =>      gtpclk_fb,
        CLKFB_OUT                       =>      gtpclk_fb,
        PLL_LOCKED_OUT                  =>      gtp_locked,
        PLL_RESET_IN                    =>      gtpclk_rst
    );
end generate;	 


	-- one dual hola instance
	Inst_hola_lsc_s6: hola_lsc_s6 
	generic map (simulation => simulation, slink_ref_clk => slink_ref_clk)
	PORT MAP(
		mgtrefclk => clk_slink ,
		sys_rst => sys_rst,
		mgt_rst => '0',
		gtpclkout => gtpclk,
		gtp_plldet_out => gtp_plldet_out,
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		ud => ud,
		ureset_n => ureset_n,
		utest_n => utest_n,
		uctrl_n => uctrl_n,
		uwen_n => uwen_n,
		uclk => clk40,
		lff_n => lff_n,
		lrl0 => lrl0,
		lrl1 => lrl1,
		ldown_n => lsc_ldown_n,
		hola_enable_0 => '1',
		hola_enable_1 => '1',
		tlk_sin0_p => lsc_sin0_p,
		tlk_sin0_n => lsc_sin0_n,
		tlk_sout0_p => lsc_sout0_p,
		tlk_sout0_n => lsc_sout0_n,
		tlk_sin1_p => lsc_sin1_p,
		tlk_sin1_n => lsc_sin1_n,
		tlk_sout1_p => lsc_sout1_p,
		tlk_sout1_n => lsc_sout1_n,
		testled_n => open,
		lderrled_n => open,
		lupled_n => open,
		flowctlled_n => open,
		activityled_n => open,
		usr_clk_out => open, -- usr_clk_out,
		gtx_reset_done => gtx_reset_done
	);

	-- LDC instatiation for test only !!!
	Inst_hola_ldc_s6: hola_ldc_s6 
	generic map (simulation => simulation, slink_ref_clk => slink_ref_clk)
	PORT MAP(
		mgtrefclk => clk_slink,
		sys_rst => sys_rst,
		mgt_rst => '0',
		gtpclkout => open,	-- used only on first MGT
		gtp_plldet_out => open,	-- used only on first MGT
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		ld0 => ld0,
		ld1 => ld1,
		uxoff_n => uxoff_n,
		utdo_n => utdo_n,
		ureset_n => ureset_n,
		lctrl_n => lctrl_n,
		lwen_n => lwen_n,
		lclk => lclk,
		lderr_n => lderr_n,
		url => url,
		ldown_n => ldc_ldown_n,
		lclk_in => clk40,
		dll_reset => '1',
		tlk_sin0_p => ldc_sin0_p,
		tlk_sin0_n => ldc_sin0_n,
		tlk_sout0_p => ldc_sout0_p,
		tlk_sout0_n => ldc_sout0_n,
		tlk_sin1_p => ldc_sin1_p,
		tlk_sin1_n => ldc_sin1_n,
		tlk_sout1_p => ldc_sout1_p,
		tlk_sout1_n => ldc_sout1_n,
		testled_n => open,
		lderrled_n => open,
		lupled_n => open,
		flowctlled_n => open,
		activityled_n => open
	);


	Inst_link_tester: link_tester PORT MAP(
		uclk => clk40,
		lclk => lclk,
		res_n => sys_rst_n,
		lsc_down_n => lsc_ldown_n,
		ldc_down_n => ldc_ldown_n,
		data2lsc => ud,
		data_from_ldc => ld1,
		lff_n => lff_n,
		u_res_n => ureset_n,
		uwen_n => uwen_n,
		uctrl_n => uctrl_n,
		lwen_n => lwen_n
	);



end Behavioral;

