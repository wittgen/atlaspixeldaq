-------------------------------------------------------------------------------
-- Title      : Interface between TLK2501 and Virtex-6 GTX
-- Project    : HOLA S-LINK
-------------------------------------------------------------------------------
-- File       : tlk_gtx_interface.vhd
-- Author     : Stefan Haas
-- Company    : CERN PH-ESE
-- Created    : 23-11-11
-- Last update: 2011-12-13
-- Platform   : Windows XP
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: Based on code from B.Green written for VILAR
-------------------------------------------------------------------------------
-- Copyright (c) 2011 CERN PH-ATE
-------------------------------------------------------------------------------
-- Revisions  :
-- Date        Version  Author  Description
-- 23-11-11  1.0      haass	Created
-- 30-08-13  1.1      akugel	byte-is-aligned input added to provide for 
--										additional checks in synchronization FSM
--										(FSM not yet updated)
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity tlk_gtx_interface is

  port (SYS_RST            : in  std_logic;
        -- GTX receive ports
        GTX_RXUSRCLK2      : in  std_logic;
        GTX_RXDATA         : in  std_logic_vector(15 downto 0);
        GTX_RXCHARISK      : in  std_logic_vector(1 downto 0);
        GTX_RXENCOMMAALIGN : out std_logic;
		  GTX_RXBYTEISALIGNED : in std_logic;
        -- GTX transmit ports
        GTX_TXUSRCLK2      : in  std_logic;
        GTX_TXCHARISK      : out std_logic_vector(1 downto 0);
        GTX_TXDATA         : out std_logic_vector(15 downto 0);
        -- TLK2501 ports
        TLK_TXD            : in  std_logic_vector(15 downto 0);
        TLK_TXEN           : in  std_logic;
        TLK_TXER           : in  std_logic;
        TLK_RXD            : out std_logic_vector(15 downto 0);
        TLK_RXDV           : out std_logic;
        TLK_RXER           : out std_logic;
        -- chipscope for monitoring signals (including HOLA reset)
        LSC_RST_N          : in std_logic;
        CS_TRIG            : out std_logic_vector(57 downto 0));

end tlk_gtx_interface;

-------------------------------------------------------------------------------

architecture behaviour of tlk_gtx_interface is

  -----------------------------------------------------------------------------
  -- 8B10B characters
  -----------------------------------------------------------------------------
  constant K28_5 : std_logic_vector(7 downto 0) := "10111100";  -- BC
  constant D5_6  : std_logic_vector(7 downto 0) := "11000101";  -- C5
  constant D16_2 : std_logic_vector(7 downto 0) := "01010000";  -- 50
  constant K23_7 : std_logic_vector(7 downto 0) := "11110111";  -- F7
  constant K30_7 : std_logic_vector(7 downto 0) := "11111110";  -- FE

  constant IDLE_Data1 : std_logic_vector(15 downto 0) := D5_6 & K28_5;
  constant IDLE_Data2 : std_logic_vector(15 downto 0) := D16_2 & K28_5;
  constant IDLE_isK   : std_logic_vector(1 downto 0)  := "01";

  constant CarrierExtend_Data : std_logic_vector(15 downto 0) := K23_7 & K23_7;
  constant CarrierExtend_isK  : std_logic_vector(1 downto 0)  := "11";
  constant NormalData_isK     : std_logic_vector(1 downto 0)  := "00";
  constant Error_Data         : std_logic_vector(15 downto 0) := K30_7 & K30_7;
  constant Error_isK          : std_logic_vector(1 downto 0)  := "11";

  -----------------------------------------------------------------------------
  -- FSM states
  -----------------------------------------------------------------------------
  type InitAndSync_t is (ACQ,
                         ACQ1,
                         ACQ2,
                         Sync,
                         Check,
                         CheckInvalid1,
                         CheckInvalid2,
                         CheckValid1,
                         CheckValid2,
                         CheckValid3);

  constant ACQ_c           : std_logic_vector(9 downto 0) := b"0000000001";
  constant ACQ1_c          : std_logic_vector(9 downto 0) := b"0000000010";
  constant ACQ2_c          : std_logic_vector(9 downto 0) := b"0000000100";
  constant Sync_c          : std_logic_vector(9 downto 0) := b"0000001000";
  constant Check_c         : std_logic_vector(9 downto 0) := b"0000010000";
  constant CheckInvalid1_c : std_logic_vector(9 downto 0) := b"0000100000";
  constant CheckInvalid2_c : std_logic_vector(9 downto 0) := b"0001000000";
  constant CheckValid1_c   : std_logic_vector(9 downto 0) := b"0010000000";
  constant CheckValid2_c   : std_logic_vector(9 downto 0) := b"0100000000";
  constant CheckValid3_c   : std_logic_vector(9 downto 0) := b"1000000000";

  -----------------------------------------------------------------------------
  -- Internal signals
  -----------------------------------------------------------------------------
  type RXStatus_t is (IDLE, CarrierExtend, NormalData, ReceiveError, OtherData);
  constant RXStatus_IDLE_c          : std_logic_vector(4 downto 0) := "00001";
  constant RXStatus_CarrierExtend_c : std_logic_vector(4 downto 0) := "00010";
  constant RXStatus_NormalData_c    : std_logic_vector(4 downto 0) := "00100";
  constant RXStatus_ReceiveError_c  : std_logic_vector(4 downto 0) := "01000";
  constant RXStatus_OtherData_c     : std_logic_vector(4 downto 0) := "10000";

  type TXControl_t is (IDLE, CarrierExtend, NormalData, TransmitError);
  constant TXControl_IDLE_c          : std_logic_vector(3 downto 0) := "0001";
  constant TXControl_CarrierExtend_c : std_logic_vector(3 downto 0) := "0010";
  constant TXControl_NormalData_c    : std_logic_vector(3 downto 0) := "0100";
  constant TXControl_TransmitError_c : std_logic_vector(3 downto 0) := "1000";

  signal TxControl               : TXControl_t;
  signal TxControl_slv           : std_logic_vector(3 downto 0);
  signal RxStatus                : RXStatus_t;
  signal RxStatus_slv            : std_logic_vector(4 downto 0);
  signal RxValid                 : std_logic;  -- Valid character received
  signal RxComma                 : std_logic;  -- Comma received
  signal InitAndSyncMachine      : InitAndSync_t;
  signal next_InitAndSyncMachine : InitAndSync_t;
  signal InitAndSyncMachine_slv  : std_logic_vector(9 downto 0);
  signal GTX_RXDATA_REG          : std_logic_vector(GTX_RXDATA'range);
  signal TLK_TXD_REG             : std_logic_vector(TLK_TXD'range);

  signal GTX_RXENCOMMAALIGN_i : std_logic;
  signal GTX_TXCHARISK_i : std_logic_vector(1 downto 0);
  signal GTX_TXDATA_i : std_logic_vector(15 downto 0);

  signal GTX_RXDATA_i : std_logic_vector(15 downto 0);
  signal GTX_RXCHARISK_i : std_logic_vector(1 downto 0);
  signal GTX_RXDATA_delayline : std_logic_vector(23 downto 0);
  signal GTX_RXCHARISK_delayline : std_logic_vector(2 downto 0);
  signal GTX_RXDELAY : std_logic := '0';
  signal GTX_RXDELAY_reg : std_logic := '0';
begin

  -----------------------------------------------------------------------------
  -- output state machines in readable format
  -----------------------------------------------------------------------------
  InitAndSyncMachine_slv <= ACQ_c when InitAndSyncMachine = ACQ else
                            ACQ1_c when InitAndSyncMachine = ACQ1 else
                            ACQ2_c when InitAndSyncMachine = ACQ2 else
                            Sync_c when InitAndSyncMachine = Sync else
                            Check_c when InitAndSyncMachine = Check else
                            CheckInvalid1_c when InitAndSyncMachine = CheckInvalid1 else
                            CheckInvalid2_c when InitAndSyncMachine = CheckInvalid2 else
                            CheckValid1_c when InitAndSyncMachine = CheckValid1 else
                            CheckValid2_c when InitAndSyncMachine = CheckValid2 else
                            CheckValid3_c when InitAndSyncMachine = CheckValid3 else
                            (others => '0');

  TxControl_slv <= TXControl_IDLE_c when TxControl = IDLE else
                   TXControl_CarrierExtend_c when TxControl = CarrierExtend else
                   TXControl_NormalData_c when TxControl = NormalData else
                   TXControl_TransmitError_c when TxControl = TransmitError else
                   (others => '0');

  RxStatus_slv <= RXStatus_IDLE_c when RxStatus = IDLE else
                  RXStatus_CarrierExtend_c when RxStatus = CarrierExtend else
                  RXStatus_NormalData_c when RxStatus = NormalData else
                  RXStatus_ReceiveError_c when RxStatus = ReceiveError else
                  RXStatus_OtherData_c when RxStatus = OtherData else
                  (others => '0');

  -----------------------------------------------------------------------------
  -- chipscope monitoring for important signals
  -----------------------------------------------------------------------------
  CS_TRIG(0) <= LSC_RST_N;
  CS_TRIG(10 downto 1) <= InitAndSyncMachine_slv;
  CS_TRIG(15 downto 11) <= RxStatus_slv;
  CS_TRIG(31 downto 16) <= GTX_RXDATA_i;
  CS_TRIG(33 downto 32) <= GTX_RXCHARISK_i;
  CS_TRIG(34) <= GTX_RXENCOMMAALIGN_i;
  CS_TRIG(35) <= GTX_RXBYTEISALIGNED;
  CS_TRIG(39 downto 36) <= TxControl_slv;
  CS_TRIG(55 downto 40) <= GTX_TXDATA_i;
  CS_TRIG(57 downto 56) <= GTX_TXCHARISK_i;

  -----------------------------------------------------------------------------
  -- delay in case of RX misalignment
  -----------------------------------------------------------------------------
  process begin
    wait until rising_edge(GTX_RXUSRCLK2);

    -- delay line for RXDATA and RXCHARISK
    GTX_RXDATA_delayline <= GTX_RXDATA & GTX_RXDATA_delayline(23 downto 16);
    GTX_RXCHARISK_delayline <= GTX_RXCHARISK & GTX_RXCHARISK_delayline(2);
    GTX_RXDELAY_reg <= GTX_RXDELAY;

    -- select if delay or not
    if GTX_RXDELAY = '0' then
      GTX_RXDATA_i <= GTX_RXDATA_delayline(23 downto 8);
      GTX_RXCHARISK_i <= GTX_RXCHARISK_delayline(2 downto 1);
    else
      GTX_RXDATA_i <= GTX_RXDATA_delayline(15 downto 0);
      GTX_RXCHARISK_i <= GTX_RXCHARISK_delayline(1 downto 0);
    end if;

    if (GTX_RXBYTEISALIGNED = '1') then
      -- toggle delay if we receive "wrong idles" (0xBCC5) instead of true IDLEs (0xC5BC)
      if ((GTX_RXDATA_i = K28_5 & D5_6) and (GTX_RXCHARISK_i = "10") and (GTX_RXDELAY_reg = GTX_RXDELAY)) then
        -- toggle delay
        GTX_RXDELAY <= not GTX_RXDELAY;
      end if;
    else
      GTX_RXDELAY <= '0';
    end if;
  end process;

  -----------------------------------------------------------------------------
  -- output internal signals
  -----------------------------------------------------------------------------
  GTX_RXENCOMMAALIGN <= GTX_RXENCOMMAALIGN_i;
  GTX_TXDATA <= GTX_TXDATA_i;
  GTX_TXCHARISK <= GTX_TXCHARISK_i;

  -----------------------------------------------------------------------------
  -- TLK receiver synchronization state machine
  -----------------------------------------------------------------------------
  RXInitAndSyncClocking : process (GTX_RXUSRCLK2, SYS_RST) is
  begin
    if SYS_RST = '1' then
      InitAndSyncMachine <= ACQ;
    elsif rising_edge(GTX_RXUSRCLK2) then
      InitAndSyncMachine <= next_InitAndSyncMachine;
    end if;
  end process RXInitAndSyncClocking;

  -- check byte-is-aligned active in decoding of valid and comma
  RxValid <= '1' when ((RXStatus = NormalData) or
                       (RXStatus = ReceiveError) or
                       (RXStatus = IDLE) or
                       (RXStatus = CarrierExtend)) else '0';

  RxComma <= '1' when ((RXStatus = IDLE) or
                       (RXStatus = CarrierExtend)) else '0';

--  RxComma <= '1' when (((GTX_RXBYTEISALIGNED = '1') and (RXStatus = IDLE)) or
--                       ((GTX_RXBYTEISALIGNED = '1') and (RXStatus = CarrierExtend))) else '0';

  RXInitAndSync : process (InitAndSyncMachine, RxComma, RxValid, GTX_RXBYTEISALIGNED) is
  begin
    case InitAndSyncMachine is

      when ACQ =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxComma = '1') then
          next_InitAndSyncMachine <= ACQ1;
--        elsif (RxValid = '1') then 
--          next_InitAndSyncMachine <= Sync;
        else
          next_InitAndSyncMachine <= ACQ;
        end if;

      when ACQ1 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxComma = '1') then
          next_InitAndSyncMachine <= ACQ2;
--        elsif (RxValid = '1') then
--          next_InitAndSyncMachine <= Sync;
        else
          next_InitAndSyncMachine <= ACQ;
        end if;

      when ACQ2 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxComma = '1') then
          next_InitAndSyncMachine <= Sync;
--        elsif (RxValid = '1') then
--          next_InitAndSyncMachine <= Sync;
        else
          next_InitAndSyncMachine <= ACQ;
        end if;

      when Sync =>
        GTX_RXENCOMMAALIGN_i <= '0';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= Sync;
        else
          next_InitAndSyncMachine <= check;
        end if;

      when check =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= CheckValid1;
        else
          next_InitAndSyncMachine <= CheckInvalid1;
        end if;

      when CheckValid1 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= CheckValid2;
        else
          next_InitAndSyncMachine <= CheckInvalid1;
        end if;

      when CheckValid2 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= CheckValid3;
        else
          next_InitAndSyncMachine <= CheckInvalid1;
        end if;

      when CheckValid3 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= Sync;
        else
          next_InitAndSyncMachine <= CheckInvalid1;
        end if;

      when CheckInvalid1 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= CheckValid1;
        else
          next_InitAndSyncMachine <= CheckInvalid2;
        end if;

      when CheckInvalid2 =>
        GTX_RXENCOMMAALIGN_i <= '1';
        if (RxValid = '1') then
          next_InitAndSyncMachine <= CheckValid1;
        else
          next_InitAndSyncMachine <= ACQ;
        end if;

      when others =>
        GTX_RXENCOMMAALIGN_i      <= '1';
        next_InitAndSyncMachine <= ACQ;

    end case;

  end process RXInitAndSync;

-----------------------------------------------------------------------------
-- GTX receive decoder
-----------------------------------------------------------------------------
-- These sections are combinatorial, hopefully most of the logic is simplified
-- This decodes the MGT RX signals
-- Here we assume K characters are marked as commas but we need to ensure characters
-- not marked as Ks are not commas
  RXDecoder : process (GTX_RXUSRCLK2) is
  begin
    if (rising_edge(GTX_RXUSRCLK2)) then
      if ((GTX_RXCHARISK_i = CarrierExtend_isK) and (GTX_RXDATA_i = CarrierExtend_Data)) then
        RXStatus <= CarrierExtend;
      elsif ((GTX_RXCHARISK_i = IDLE_isK) and ((GTX_RXDATA_i = IDLE_Data1) or (GTX_RXDATA_i = IDLE_Data2))) then
        RXStatus <= IDLE;
      elsif ((GTX_RXCHARISK_i = Error_isK) and (GTX_RXDATA_i = Error_Data))then
        RXStatus <= ReceiveError;
      elsif (GTX_RXCHARISK_i = NormalData_isK) then
        RXStatus <= NormalData;
      else
        RXStatus <= OtherData;
      end if;

      -- if we lose RX alignment we should go into OtherData state
      if (GTX_RXBYTEISALIGNED = '0') then   -- GTX error behavior. Must go back to search
        RXStatus <= OtherData;              -- this is not valid data and drives the FSM through the "invalid" sequence 
      end if;
    end if;
  end process RXDecoder;

  -----------------------------------------------------------------------------
  -- TLK RXDV & RXER encoder
  -----------------------------------------------------------------------------
  RXEncoder : process (GTX_RXUSRCLK2) is
  begin
    if (rising_edge(GTX_RXUSRCLK2)) then
      TLK_RXDV <= '1';                  -- set default
      TLK_RXER <= '1';                  -- set default
      if InitAndSyncMachine = sync then
        case RXStatus is
          when IDLE =>
            TLK_RXDV <= '0';
            TLK_RXER <= '0';
          when CarrierExtend =>
            TLK_RXDV <= '0';
            TLK_RXER <= '1';
          when ReceiveError =>
            TLK_RXDV <= '1';
            TLK_RXER <= '1';
          when NormalData =>
            TLK_RXDV <= '1';
            TLK_RXER <= '0';
          when others => null;
        end case;
      end if;
      -- Data is simply passed on unmodified
      GTX_RXDATA_REG <= GTX_RXDATA_i;
      TLK_RXD        <= GTX_RXDATA_REG;
    end if;
  end process RXEncoder;

  -----------------------------------------------------------------------------
  -- TLK transmit interface decoder
  -----------------------------------------------------------------------------
  TXDecoder : process (GTX_TXUSRCLK2) is
  begin
    if (rising_edge(GTX_TXUSRCLK2)) then
      if TLK_TXEN = '0' then
        if TLK_TXER = '0' then
          TXControl <= IDLE;
        else
          TXControl <= CarrierExtend;
        end if;
      else
        if TLK_TXER = '0' then
          TXControl <= NormalData;
        else
          TXControl <= TransmitError;
        end if;
      end if;
    end if;
  end process TXDecoder;

  -----------------------------------------------------------------------------
  -- GTX transmit interface encoder
  -----------------------------------------------------------------------------
  TXMux : process (GTX_TXUSRCLK2) is
  begin
    if (rising_edge(GTX_TXUSRCLK2)) then
      TLK_TXD_REG <= TLK_TXD;
      case TXControl is
        when IDLE =>
          GTX_TXCHARISK_i <= IDLE_isK;
          GTX_TXDATA_i    <= IDLE_Data1; --IDLE_Data2;
        when CarrierExtend =>
          GTX_TXCHARISK_i <= CarrierExtend_isK;
          GTX_TXDATA_i    <= CarrierExtend_Data;
        when NormalData =>
          GTX_TXCHARISK_i <= NormalData_isK;
          GTX_TXDATA_i    <= TLK_TXD_REG;
        when TransmitError =>
          GTX_TXCHARISK_i <= Error_isK;
          GTX_TXDATA_i    <= Error_Data;
      end case;
    end if;
  end process TXMux;

end behaviour;

