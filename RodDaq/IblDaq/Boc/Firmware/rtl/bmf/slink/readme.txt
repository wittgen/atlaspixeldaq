default S-Link LSC configuration is
2Gbit/s from 100MGT GTP ref clock.

Default GTP core: mgt2g100ref_txout_ppm_even_vtt3q_cc

Options exists sto use a generated clock (e.g. 150MHz fpr a 3Gbit/s link)
derived from 100MHz system clock via a PLL.

Faster GTP core: mgt3g150ref_txout_ppm_even_vtt3q_cc

Optional clock PLLs:
slink_100from100_pll (alternative to refclk for 2Gbit/s link)
slink_150from100_pll (mandatory for 3Gbit/s link, in absence of faster ref clock)


