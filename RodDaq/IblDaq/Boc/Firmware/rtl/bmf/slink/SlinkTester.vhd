library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity SlinkTester is
	port (
		-- clock and reset
		clk : in std_logic;
		reset : in std_logic;

		-- control input
		enable : in std_logic;

		-- slink inputs
		slink_udata : in std_logic_vector(31 downto 0);
		slink_uctrl : in std_logic;
		slink_uwe : in std_logic;

		-- error flag
		slink_error : out std_logic
	);
end entity;

architecture rtl of SlinkTester is

-- expected fragment data
type frag_data_t is array (0 to 67) of std_logic_vector(31 downto 0);
constant frag_data : frag_data_t := (
	x"00000000", x"FFFFFFFF", x"55555555", x"AAAAAAAA",
	x"00000001", x"00000002", x"00000004", x"00000008",
	x"00000010", x"00000020", x"00000040", x"00000080",
	x"00000100", x"00000200", x"00000400", x"00000800",
	x"00001000", x"00002000", x"00004000", x"00008000",
	x"00010000", x"00020000", x"00040000", x"00080000",
	x"00100000", x"00200000", x"00400000", x"00800000",
	x"01000000", x"02000000", x"04000000", x"08000000",
	x"10000000", x"20000000", x"40000000", x"80000000",
	x"FFFFFFFE", x"FFFFFFFD", x"FFFFFFFB", x"FFFFFFF7",
	x"FFFFFFEF", x"FFFFFFDF", x"FFFFFFBF", x"FFFFFF7F",
	x"FFFFFEFF", x"FFFFFDFF", x"FFFFFBFF", x"FFFFF7FF",
	x"FFFFEFFF", x"FFFFDFFF", x"FFFFBFFF", x"FFFF7FFF",
	x"FFFEFFFF", x"FFFDFFFF", x"FFFBFFFF", x"FFF7FFFF",
	x"FFEFFFFF", x"FFDFFFFF", x"FFBFFFFF", x"FF7FFFFF",
	x"FEFFFFFF", x"FDFFFFFF", x"FBFFFFFF", x"F7FFFFFF",
	x"EFFFFFFF", x"DFFFFFFF", x"BFFFFFFF", x"7FFFFFFF"
);

-- sync state machine
type fsm is (st_waitbof, st_data);
signal Z : fsm := st_waitbof;
signal wordcnt : integer range 0 to 67 := 0;

begin

-- FSM
process begin
	wait until rising_edge(clk);

	if (reset = '1') or (enable = '0') then
		Z <= st_waitbof;
		wordcnt <= 0;
		slink_error <= '0';
	else
		case Z is
			when st_waitbof =>
				if (slink_uwe = '0') and (slink_uctrl = '0') and (slink_udata = x"B0F00000") then
					Z <= st_data;
				end if;
				slink_error <= '0';

			when st_data =>
				if (slink_uwe = '0') then
					if slink_udata /= frag_data(wordcnt) then
						slink_error <= '1';
					else
						slink_error <= '0';
					end if;

					if wordcnt = 67 then
						wordcnt <= 0;
						Z <= st_waitbof;
					else
						wordcnt <= wordcnt + 1;
					end if;
				else
					slink_error <= '0';
				end if;
		end case;
	end if;
end process;

end architecture;