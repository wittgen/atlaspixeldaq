// author: philipp schaefer

module hola_lsc_s6 #(
	parameter SIMULATION = 1'b0)(
	input wire mgtrefclk,
	input wire sys_rst,
	input wire mgt_rst,
	// new for sharing tile user clocks
	// one gtpclkout is used to generate all user clocks, also for neightbouring tiles
	 output wire gtpclkout,
	 output wire gtp_plldet_out,
	 input wire usr_clk_in,
	 input wire usr_clk20_in,
	 input wire usr_clk40_in,
	// s-link interface
	input wire [31:0] ud,
	input wire ureset_n,
	input wire utest_n,
	input wire uctrl_n,
	input wire uwen_n,
	input wire uclk,
	output wire lff_n,
	output wire [3:0] lrl0,
	output wire [3:0] lrl1,
	output wire ldown_n,
	// slink enables
	input wire hola_enable_lff_0,
	input wire hola_enable_ldown_0,
	input wire hola_enable_lff_1,
	input wire hola_enable_ldown_1,
	// monitoring counters
	input wire count_clk,
	output wire [15:0] count_hola0_lff,
	output wire [15:0] count_hola0_ldown,
	output wire [15:0] count_hola1_lff,
	output wire [15:0] count_hola1_ldown,
	input wire count_reset,
	// sfp serial interface
	input wire tlk_sin0_p,
	input wire tlk_sin0_n,
	output wire tlk_sout0_p,
	output wire tlk_sout0_n,
	input wire tlk_sin1_p,
	input wire tlk_sin1_n,
	output wire tlk_sout1_p,
	output wire tlk_sout1_n,
	// leds
	output wire testled_n,
	output wire lderrled_n,
	output wire lupled_n,
	output wire flowctlled_n,
	output wire activityled_n,
	// debug
	output wire dbg_txclk0,
	output wire [15:0] dbg_txd0,
	output wire dbg_tx_er0,
	output wire dbg_tx_en0,
	output wire dbg_rxclk0,
	output wire [2:0] dbg_cc0,
	output wire [15:0] dbg_rxd0,
	output wire dbg_rx_er0,
	output wire dbg_rx_dv0,
	output wire [57:0] dbg_tlkgtx0,
	output wire [33:0] dbg_fifo_q0,
	output wire dbg_fifo_empty0,
	output wire dbg_fifo_rden0,
	output wire dbg_fifo_clk0,
	output wire dbg_txclk1,
	output wire [15:0] dbg_txd1,
	output wire dbg_tx_er1,
	output wire dbg_tx_en1,
	output wire dbg_rxclk1,
	output wire [2:0] dbg_cc1,
	output wire [15:0] dbg_rxd1,
	output wire dbg_rx_er1,
	output wire dbg_rx_dv1,
	output wire [57:0] dbg_tlkgtx1,
	output wire [33:0] dbg_fifo_q1,
	output wire dbg_fifo_empty1,
	output wire dbg_fifo_rden1,
	output wire dbg_fifo_clk1,
	
	output wire usr_clk_out,
	output wire gtx_reset_done0,
	output wire gtx_reset_done1,
	// AKU additional debugging
	output wire dbg_rxrecclk0,
	output wire [1:0] dbg_rxlossofsync0,	
	input wire dbg_rxreset0,
	input wire dbg_rxcdrreset0,
	input wire dbg_rxbufreset0,
	output wire dbg_rxrecclk1,
	output wire [1:0] dbg_rxlossofsync1,	
	input wire dbg_rxreset1,
	input wire dbg_rxcdrreset1,
	input wire dbg_rxbufreset1
);

wire lff_n0;
wire ldown_n0;
wire testled_n0;
wire lderrled_n0;
wire lupled_n0;
wire flowctlled_n0;
wire activityled_n0;

wire lsc_rst0_n;
wire lsc_rst1_n;
wire tlk_gtx_clk0;
wire tlk_enable0;
wire [15:0] tlk_txd0;
wire tlk_tx_en0;
wire tlk_tx_er0;
wire [15:0] tlk_rxd0;
wire tlk_rx_clk0;
wire tlk_rx_er0;
wire tlk_rx_dv0;

// should be the same as tlk_gtx_clk1
assign usr_clk_out = tlk_gtx_clk0;

wire lff_n1;
wire ldown_n1;
wire testled_n1;
wire lderrled_n1;
wire lupled_n1;
wire flowctlled_n1;
wire activityled_n1;

wire tlk_gtx_clk1;
wire tlk_enable1;
wire [15:0] tlk_txd1;
wire tlk_tx_en1;
wire tlk_tx_er1;
wire [15:0] tlk_rxd1;
wire tlk_rx_clk1;
wire tlk_rx_er1;
wire tlk_rx_dv1;

holalsc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.USE_PLL(0),
	.USE_ICLK2(1),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(64),
	.LOG2DEPTH(6),
	.FULLMARGIN(16)) holalsc_core0_I (
	.power_up_rst_n(lsc_rst0_n),
	.ud(ud),
	.ureset_n(ureset_n),
	.utest_n(utest_n),
	.uctrl_n(uctrl_n),
	.uwen_n(uwen_n),
	.uclk(uclk),
	.lff_n(lff_n0),
	.lrl(lrl0),
	.ldown_n(ldown_n0),
	.testled_n(testled_n0),
	.lderrled_n(lderrled_n0),
	.lupled_n(lupled_n0),
	.flowctlled_n(flowctlled_n0),
	.activityled_n(activityled_n0),
	.xclk(tlk_gtx_clk0),
	.iclk2_in(usr_clk40_in),
	.enable(tlk_enable0),
	.txd(tlk_txd0),
	.tx_en(tlk_tx_en0),
	.tx_er(tlk_tx_er0),
	.rxd(tlk_rxd0),
	.rx_clk(tlk_rx_clk0),
	.rx_er(tlk_rx_er0),
	.rx_dv(tlk_rx_dv0),
	.debug_fifo_q(dbg_fifo_q0),
	.debug_fifo_empty(dbg_fifo_empty0),
	.debug_fifo_rden(dbg_fifo_rden0),
	.debug_fifo_clk(dbg_fifo_clk0)
);

holalsc_core #(
	.SIMULATION(SIMULATION),
	.ALTERA_XILINX(0),
	.XCLK_FREQ(100),
	.USE_PLL(0),
	.USE_ICLK2(1),
	.ACTIVITY_LENGTH(15),
	.FIFODEPTH(64),
	.LOG2DEPTH(6),
	.FULLMARGIN(16)) holalsc_core1_I (
	.power_up_rst_n(lsc_rst1_n),
	.ud(ud),
	.ureset_n(ureset_n),
	.utest_n(utest_n),
	.uctrl_n(uctrl_n),
	.uwen_n(uwen_n),
	.uclk(uclk),
	.lff_n(lff_n1),
	.lrl(lrl1),
	.ldown_n(ldown_n1),
	.testled_n(testled_n1),
	.lderrled_n(lderrled_n1),
	.lupled_n(lupled_n1),
	.flowctlled_n(flowctlled_n1),
	.activityled_n(activityled_n1),
	.xclk(tlk_gtx_clk1),
	.iclk2_in(usr_clk40_in),
	.enable(tlk_enable1),
	.txd(tlk_txd1),
	.tx_en(tlk_tx_en1),
	.tx_er(tlk_tx_er1),
	.rxd(tlk_rxd1),
	.rx_clk(tlk_rx_clk1),
	.rx_er(tlk_rx_er1),
	.rx_dv(tlk_rx_dv1),
	.debug_fifo_q(dbg_fifo_q1),
	.debug_fifo_empty(dbg_fifo_empty1),
	.debug_fifo_rden(dbg_fifo_rden1),
	.debug_fifo_clk(dbg_fifo_clk1)
);


tlk_wrapper_s6 tlk_wrapper_s6_I (
	.mgtrefclk(mgtrefclk),
	.gtpclkout(gtpclkout),
	.gtp_plldet_out(gtp_plldet_out),
	.usr_clk_in(usr_clk_in),
	.usr_clk20_in(usr_clk20_in),
	.lsc_rst0_n(lsc_rst0_n),
	.lsc_rst1_n(lsc_rst1_n),
	.gtx_reset(mgt_rst),
	.gtx_resetdone0(gtx_reset_done0),
	.gtx_resetdone1(gtx_reset_done1),
	.gtx_rxn0(tlk_sin0_n),
	.gtx_rxp0(tlk_sin0_p),
	.gtx_txn0(tlk_sout0_n),
	.gtx_txp0(tlk_sout0_p),
	.gtx_rxn1(tlk_sin1_n),
	.gtx_rxp1(tlk_sin1_p),
	.gtx_txn1(tlk_sout1_n),
	.gtx_txp1(tlk_sout1_p),
	.GTX_RXCLKCORCNT0(dbg_cc0),
	.GTX_RXCLKCORCNT1(dbg_cc1),
	.tlk_txclk0(tlk_gtx_clk0),
	.tlk_txd0(tlk_txd0),
	.tlk_txen0(tlk_tx_en0),
	.tlk_txer0(tlk_tx_er0),
	.tlk_rxclk0(tlk_rx_clk0),
	.tlk_rxd0(tlk_rxd0),
	.tlk_rxdv0(tlk_rx_dv0),
	.tlk_rxer0(tlk_rx_er0),
	.dbg_tlkgtx0(dbg_tlkgtx0),
	.tlk_txclk1(tlk_gtx_clk1),
	.tlk_txd1(tlk_txd1),
	.tlk_txen1(tlk_tx_en1),
	.tlk_txer1(tlk_tx_er1),
	.tlk_rxclk1(tlk_rx_clk1),
	.tlk_rxd1(tlk_rxd1),
	.tlk_rxdv1(tlk_rx_dv1),
	.tlk_rxer1(tlk_rx_er1),
	.dbg_tlkgtx1(dbg_tlkgtx1),
	// AKU debugging
	.tile0_rxrecclk0(dbg_rxrecclk0),
	.tile0_rxlossofsync0(dbg_rxlossofsync0),
	.tile0_rxreset0(dbg_rxreset0),
	.tile0_rxcdrreset0(dbg_rxcdrreset0),
	.tile0_rxbufreset0(dbg_rxbufreset0),
	.tile0_rxrecclk1(dbg_rxrecclk1),
	.tile0_rxlossofsync1(dbg_rxlossofsync1),
	.tile0_rxreset1(dbg_rxreset1),
	.tile0_rxcdrreset1(dbg_rxcdrreset1),
	.tile0_rxbufreset1(dbg_rxbufreset1)
);

hola_monitoring_counters hola_monitoring_counters_I (
	.clk(count_clk),
	.hola0_lff_n(lff_n0),
	.hola0_ldown_n(ldown_n0),
	.hola1_lff_n(lff_n1),
	.hola1_ldown_n(ldown_n1),
	.count_hola0_lff(count_hola0_lff),
	.count_hola0_ldown(count_hola0_ldown),
	.count_hola1_lff(count_hola1_lff),
	.count_hola1_ldown(count_hola1_ldown),
	.count_reset(count_reset)
);

// use enable signals to qualify holas. All output are low active
assign testled_n = testled_n0 & testled_n1;
assign lderrled_n = !((!lderrled_n0 & hola_enable_ldown_0) | (!lderrled_n1 & hola_enable_ldown_1));
assign lupled_n = !((!lupled_n0  & hola_enable_ldown_0)| (!lupled_n1 & hola_enable_ldown_1));
assign flowctlled_n = !((!flowctlled_n0  & hola_enable_lff_0 & ldown_n0) | (!flowctlled_n1 & hola_enable_lff_1 & ldown_n1));
assign activityled_n = activityled_n0 & activityled_n1;
assign lff_n = !((!lff_n0 & hola_enable_lff_0 & ldown_n0) | (!lff_n1 & hola_enable_lff_1 & ldown_n1));
assign ldown_n = !((!ldown_n0 & hola_enable_ldown_0) | (!ldown_n1 & hola_enable_ldown_1));
assign lsc_rst0_n = !sys_rst & gtx_reset_done0;
assign lsc_rst1_n = !sys_rst & gtx_reset_done1;


assign dbg_txclk0 = tlk_gtx_clk0;
assign dbg_txd0 = tlk_txd0;
assign dbg_tx_er0 = tlk_tx_er0;
assign dbg_tx_en0 = tlk_tx_en0;
assign dbg_rxclk0 = tlk_rx_clk0;
assign dbg_rxd0 = tlk_rxd0;
assign dbg_rx_er0 = tlk_rx_er0;
assign dbg_rx_dv0 = tlk_rx_dv0;
assign dbg_txclk1 = tlk_gtx_clk1;
assign dbg_txd1 = tlk_txd1;
assign dbg_tx_er1 = tlk_tx_er1;
assign dbg_tx_en1 = tlk_tx_en1;
assign dbg_rxclk1 = tlk_rx_clk1;
assign dbg_rxd1 = tlk_rxd1;
assign dbg_rx_er1 = tlk_rx_er1;
assign dbg_rx_dv1 = tlk_rx_dv1;



endmodule // hola_lsc_s6
