-- FSM to control the Spartan-6 GTP RX CDR reset
-- A. Kugel, 2015-01-29
-- s6cdr_reset_fsm.vhd 

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity s6cdr_reset_fsm is 
generic ( 
  watchPeriod: integer := 10000; -- cycles to monitor
  maxOscillations: integer := 10 -- max number of allowed "isAligned" osciallations in watchPeriod
);
port (
  clk: in std_logic; -- the user clock
  rst: in std_logic; -- gtp reset
  byteIsaligned: in std_logic; -- alignment signal from GTP
  rxCdrRst: out std_logic -- the reset signal
);
end entity;

architecture arch of s6cdr_reset_fsm is 

  signal aligned_r0, aligned_r1: std_logic;  -- edge detection
  signal aligendEdge: std_logic;
  signal pCnt: natural range 0 to watchPeriod; -- period
  signal oCnt: natural range 0 to maxOscillations; -- oscillations
  constant rsTime: natural:= 100; -- cycles reset is active
  signal rCnt: natural range 0 to rsTime; -- reset time counter
  signal restart: std_logic;

begin

  -- period counter
  process(rst, clk)
  begin
	if rst = '1' then
		pCnt <= 0;
		restart <= '1';
	elsif rising_edge(clk) then
		if pCnt = watchPeriod - 1 then
			pCnt <= 0;
			restart <= '1';
		else
			pCnt <= pCnt + 1;
			restart <= '0';
		end if;
	end if;
  end process;

  -- shift stage
  process(rst, clk)
  begin
	if rst = '1' then
		aligned_r0 <= '0';
		aligned_r1 <= '0';
	elsif rising_edge(clk) then
		aligned_r0 <= byteIsaligned;
		aligned_r1 <= aligned_r0;
	end if;
  end process;
  
  -- event counter and reset logic. 
  -- count on falling edges of isAligend
  process(restart, clk)
  begin
	if restart = '1' then
		oCnt <= 0;
		rCnt <= 0;
		rxCdrRst <= '0';
	elsif rising_edge(clk) then
		if oCnt < maxOscillations - 1 then
			if (aligned_r0 = '0') and (aligned_r1 = '1') then
				oCnt <= oCnt + 1;
			end if;
		elsif rCnt < rsTime - 1 then
			rCnt <= rCnt + 1;
			rxCdrRst <= '1';
		else
			rxCdrRst <= '0';
		end if;
	end if;
  end process;
  

end arch;
