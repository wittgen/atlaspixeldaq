--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;

package lscpack is

-- some constants

-- select GTP speed: 2 (normal) or 3 (fast) Gb/s
constant GTP_HI_SPEED: boolean := false;
-- constant GTP_HI_SPEED: boolean := true;

-- select gtp reference clock source
-- there are 3 optins:
--		gtp ref clock
--		100MHz global clock from 100 MHz input via direct (direct) PLL
--		100MHz global clock from 40 MHz input via primary (up) PLL

constant SL_USE_PLL: boolean := true;
-- constant SL_USE_PLL: boolean := false;

constant SL_PLL_DIRECT: boolean := true;
-- constant SL_PLL_DIRECT: boolean := false;

end lscpack;

