library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity ICAPCtrl is
	port (
		clk : in std_logic;
		reset : in std_logic;

		-- ICAP control and status register
		icap_csr_rd : out std_logic_vector(7 downto 0);
		icap_csr_wr : in std_logic_vector(7 downto 0);
		icap_csr_rden : in std_logic;
		icap_csr_wren : in std_logic;

		-- data registers
		icap_dh_rd : out std_logic_vector(7 downto 0);
		icap_dh_wr : in std_logic_vector(7 downto 0);
		icap_dh_rden : in std_logic;
		icap_dh_wren : in std_logic;
		icap_dl_rd : out std_logic_vector(7 downto 0);
		icap_dl_wr : in std_logic_vector(7 downto 0);
		icap_dl_rden : in std_logic;
		icap_dl_wren : in std_logic;

		-- register protection
		secure_reg : in std_logic
	);
end ICAPCtrl;

architecture RTL of ICAPCtrl is
	COMPONENT BitFIFO
	  PORT (
		 rd_clk : IN STD_LOGIC;
		 wr_clk : IN STD_LOGIC;
		 rst : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC
	  );
	END COMPONENT;

	-- slow clock signal (internally generated)
	signal icapclk : std_logic := '0';
	signal icapclk_reg : std_logic := '0';
	signal icapclk_cnt : integer range 0 to 7 := 0;

	-- icap signals
	signal icap_start : std_logic := '0';
	signal icap_start_slow : std_logic := '0';
	signal icap_abort : std_logic := '0';
	signal icap_abort_slow : std_logic := '0';
	signal icap_ack : std_logic := '0';
	signal icap_ack_fast : std_logic := '0';
	signal icap_din : std_logic_vector(15 downto 0) := (others => '0');
	signal icap_dout : std_logic_vector(15 downto 0) := (others => '0');
	signal icap_write_n : std_logic := '0';
	signal icap_ce_n : std_logic := '1';
	signal icap_busy : std_logic := '0';
	signal icap_done : std_logic := '0';
	signal icap_done_fast : std_logic := '0';

	-- FIFO signals
	signal fifo_din : std_logic_vector(15 downto 0) := (others => '0');
	signal fifo_dout : std_logic_vector(15 downto 0) := (others => '0');
	signal fifo_rden : std_logic := '0';
	signal fifo_wren : std_logic := '0';
	signal fifo_empty : std_logic := '0';
	signal fifo_empty_fast : std_logic := '0';
	signal fifo_full : std_logic := '0';

	-- abort counter
	signal abort_cnt : integer range 0 to 15 := 0;

	type fsm_icap_states is (st_abort0, st_abort1, st_idle, st_icap);
	signal Z : fsm_icap_states := st_idle;
begin

-- generate internal slow clock (clk / 16)
process begin
	wait until rising_edge(clk);

	if (icapclk_cnt = 7) then
		icapclk_cnt <= 0;
		icapclk_reg <= not icapclk_reg;
	else
		icapclk_cnt <= icapclk_cnt + 1;
	end if;
end process;

icapclk_bufg: BUFG
	port map (
		I => icapclk_reg,
		O => icapclk
	);

-- icap interface
icap_i: ICAP_SPARTAN6
	generic map (
		DEVICE_ID => X"0403D093",     -- Specifies the pre-programmed Device ID value
		SIM_CFG_FILE_NAME => "NONE"   -- Specifies the Raw Bitstream (RBT) file to be parsed by the simulation
									  -- model
	)
	port map (
		BUSY => icap_busy,     -- 1-bit output: Busy/Ready output
		O => icap_dout,        -- 16-bit output: Configuartion data output bus
		CE => icap_ce_n,       -- 1-bit input: Active-Low ICAP Enable input
		CLK => icapclk,       -- 1-bit input: Clock input
		I => icap_din,         -- 16-bit input: Configuration data input bus
		WRITE => icap_write_n  -- 1-bit input: Read/Write control input
	);

-- bitfile fifo
bitfifo_i: BitFIFO
	port map (
		rd_clk => icapclk,
		wr_clk => clk,
		rst => reset,
		din => fifo_din,
		wr_en => fifo_wren,
		dout => fifo_dout,
		rd_en => fifo_rden,
		full => fifo_full,
		empty => fifo_empty
	);

-- register icap start command for slow clock
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		icap_start <= '0';
		icap_abort <= '1';
	else
		if (icap_csr_wren = '1') and (icap_csr_wr(0) = '1') and (secure_reg = '1') then
			icap_start <= '1';
		elsif (icap_csr_wren = '1') and (icap_csr_wr(1) = '1') and (secure_reg = '1') then
			icap_abort <= '1';
		elsif icap_ack_fast = '1' then
			icap_start <= '0';
			icap_abort <= '0';
		end if;
	end if;
end process;

-- write to fifo when writing the lower byte
process begin
	wait until rising_edge(clk);

	fifo_wren <= '0';		-- default assignment

	if icap_dh_wren = '1' then
		fifo_din(15 downto 8) <= icap_dh_wr;
	end if;

	if icap_dl_wren = '1' then
		fifo_din(7 downto 0) <= icap_dl_wr;
		fifo_wren <= '1';
	end if;
end process;

-- icap state machine
process begin
	wait until rising_edge(icapclk);

	-- default values
	fifo_rden <= '0';
	icap_ack <= '0';

	-- sync start and abort command
	icap_start_slow <= icap_start;
	icap_abort_slow <= icap_abort;

	case Z is
		when st_abort0 =>
			icap_ce_n <= '0';
			icap_write_n <= '0';
			Z <= st_abort1;
			abort_cnt <= 0;
	
		when st_abort1 =>
			icap_ce_n <= '0';
			icap_write_n <= '1';
			if abort_cnt = 15 then
				icap_done <= '1';
				Z <= st_idle;
			else
				abort_cnt <= abort_cnt + 1;
			end if;
		
		when st_idle =>
			-- start configuration by writing a 1 into CSR(0)
			if icap_start_slow = '1' then
				icap_ack <= '1';
				icap_done <= '0';
				Z <= st_icap;
			elsif icap_abort_slow = '1' then
				icap_ack <= '1';
				icap_done <= '0';
				Z <= st_abort0;
			end if;
		
		when st_icap =>
			-- while the FIFO is not empty readout
			if fifo_empty = '0' then
				fifo_rden <= '1';
				icap_din(0) <= fifo_dout(7);
				icap_din(1) <= fifo_dout(6);
				icap_din(2) <= fifo_dout(5);
				icap_din(3) <= fifo_dout(4);
				icap_din(4) <= fifo_dout(3);
				icap_din(5) <= fifo_dout(2);
				icap_din(6) <= fifo_dout(1);
				icap_din(7) <= fifo_dout(0);
				icap_din(8) <= fifo_dout(15);
				icap_din(9) <= fifo_dout(14);
				icap_din(10) <= fifo_dout(13);
				icap_din(11) <= fifo_dout(12);
				icap_din(12) <= fifo_dout(11);
				icap_din(13) <= fifo_dout(10);
				icap_din(14) <= fifo_dout(9);
				icap_din(15) <= fifo_dout(8);
				icap_ce_n <= '0';
				icap_write_n <= '0';
			else
				icap_done <= '1';
				Z <= st_idle;
			end if;
	end case;
end process;

-- synchronize ack and done
process begin
	wait until rising_edge(clk);

	icap_done_fast <= icap_done;
	icap_ack_fast <= icap_ack;
	fifo_empty_fast <= fifo_empty;
end process;

-- register readback
icap_csr_rd <= "000" & fifo_full & fifo_empty_fast & icap_done_fast & icap_abort & icap_start;
icap_dh_rd <= (others => '0');
icap_dl_rd <= (others => '0');

end architecture;

