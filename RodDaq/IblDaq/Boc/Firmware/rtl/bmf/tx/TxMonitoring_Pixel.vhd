library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TxMonitoring_Pixel is
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- enable (will also reset counters and FIFO)
		enable : in std_logic;
		fifo_enable : in std_logic;

		-- FIFO info
		fifo_full : out std_logic;
		fifo_empty : out std_logic;

		-- trigger on commands
		cmd_trigger_sel : in std_logic_vector(2 downto 0);
		cmd_trigger : out std_logic;

		-- simple counter readout
		cnt_rd : out std_logic_vector(7 downto 0);
		cnt_wr : in std_logic_vector(7 downto 0);
		cnt_rden : in std_logic;
		cnt_wren : in std_logic
	);
end TxMonitoring_Pixel;

architecture rtl of TxMonitoring_Pixel is

-- counters for trigger, BCR and ECR
signal trig_cnt : unsigned(31 downto 0) := (others => '0');
signal ecr_cnt : unsigned(31 downto 0) := (others => '0');
signal bcr_cnt : unsigned(31 downto 0) := (others => '0');
signal cal_cnt : unsigned(31 downto 0) := (others => '0');
signal sync_cnt : unsigned(31 downto 0) := (others => '0');
signal slow_cnt : unsigned(31 downto 0) := (others => '0');
signal corrupt_cnt : unsigned(31 downto 0) := (others => '0');

-- MCC CNT register
signal MCC_CNT_reg : std_logic_vector(15 downto 0) := (others => '0');
signal MCC_WrFe_ignore : integer range 0 to 32767 := 0;
signal MCC_WrRecv_ignore : integer range 0 to 32767 := 0;

-- shift register for counter monitoring
signal cntmon_sr : std_logic_vector(32 downto 0) := (others => '0');
signal ignore_cnt : integer range 0 to 32767 := 0;

-- readout shift register for counters
signal readcnt_sr : std_logic_vector(31 downto 0) := (others => '0');

begin

-- shift in new data for monitoring in counter mode
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		cntmon_sr <= (others => '0');
	else
		cntmon_sr <= cntmon_sr(31 downto 0) & din;
	end if;
end process;

-- calculate ignore bits
process begin
	wait until rising_edge(clk40);
	MCC_WrFe_ignore <= 5 + 4 + 4 + 4 + 8*to_integer(unsigned(MCC_CNT_reg(2 downto 0))) + 8*to_integer(unsigned(MCC_CNT_reg(7 downto 3)));
	MCC_WrRecv_ignore <= 5 + 4 + 4 + 4 + 8*to_integer(unsigned(MCC_CNT_reg(2 downto 0)));
end process;

-- compare the shift register for commands
process begin
	wait until rising_edge(clk40);

	if (reset = '1') or (enable = '0') then
		trig_cnt <= (others => '0');
		ecr_cnt <= (others => '0');
		bcr_cnt <= (others => '0');
		cal_cnt <= (others => '0');
		sync_cnt <= (others => '0');
		slow_cnt <= (others => '0');
		corrupt_cnt <= (others => '0');
	else
		if ignore_cnt = 0 then
			-- compare shift register contents
			if cntmon_sr(32 downto 28) = "11101" then			-- Trigger
				trig_cnt <= trig_cnt + 1;
				ignore_cnt <= 5;
			elsif cntmon_sr(32 downto 24) = "101100001" then	-- BCR
				bcr_cnt <= bcr_cnt + 1;
				ignore_cnt <= 9;
			elsif cntmon_sr(32 downto 24) = "101100010" then	-- ECR
				ecr_cnt <= ecr_cnt + 1;
				ignore_cnt <= 9;
			elsif cntmon_sr(32 downto 24) = "101100100" then	-- CAL
				cal_cnt <= cal_cnt + 1;
				ignore_cnt <= 9;
			elsif cntmon_sr(32 downto 24) = "101101000" then	-- SYNC command
				sync_cnt <= sync_cnt + 1;
				ignore_cnt <= 9;
			elsif cntmon_sr(32 downto 24) = "101101011" then	-- Slow command
				slow_cnt <= slow_cnt + 1;
				case cntmon_sr(23 downto 20) is
					when "0000" => 
						ignore_cnt <= 33;						-- write register
						-- catch writes to the CNT register
						if (cntmon_sr(19 downto 16) = "0101") then
							MCC_CNT_reg <= cntmon_sr(15 downto 0);
						end if;
					when "0001" =>
						ignore_cnt <= 33;						-- read register
					when "0010" | "0011" =>
						ignore_cnt <= 38;						-- write/read FIFO
					when "0100" | "0101" =>
						ignore_cnt <= MCC_WrFe_ignore;			-- write (read) front end
					when "0110" =>
						ignore_cnt <= MCC_WrRecv_ignore;		-- write receiver
					when "1000" | "1001" =>
						ignore_cnt <= 17;						-- enable data taking / reset MCC
					when "1010" =>
						ignore_cnt <= 21;						-- reset FE
					when others =>
						ignore_cnt <= 33;
						corrupt_cnt <= corrupt_cnt + 1;
				end case;
			elsif cntmon_sr(cntmon_sr'left) = '1' then	-- anything else
				corrupt_cnt <= corrupt_cnt + 1;
				ignore_cnt <= 33;
			end if;
		else
			ignore_cnt <= ignore_cnt - 1;
		end if;		
	end if;
end process;

-- counter readout
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		readcnt_sr <= (others => '0');
	else
		if cnt_wren = '1' then
			case cnt_wr(2 downto 0) is
				when "000" =>	readcnt_sr <= std_logic_vector(trig_cnt);
				when "001" =>	readcnt_sr <= std_logic_vector(ecr_cnt);
				when "010" =>	readcnt_sr <= std_logic_vector(bcr_cnt);
				when "011" =>	readcnt_sr <= std_logic_vector(cal_cnt);
				when "100" =>	readcnt_sr <= std_logic_vector(slow_cnt);
				when "101" =>	readcnt_sr <= std_logic_vector(corrupt_cnt);
				when "110" =>   readcnt_sr <= std_logic_vector(sync_cnt);
				when others =>  readcnt_sr <= (others => '0');
			end case;
		end if;

		if cnt_rden = '1' then
			readcnt_sr <= readcnt_sr(23 downto 0) & x"00";
		end if;
	end if;
end process;

-- read back from MSB to LSB
cnt_rd <= readcnt_sr(31 downto 24);

-- TODO: implement cmd_trigger also for pixel
cmd_trigger <= '0';

end architecture;
