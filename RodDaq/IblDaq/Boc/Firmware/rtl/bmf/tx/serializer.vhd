-- serializer
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity serializer is
	port
	(
		clk40 : in std_logic := '0';
		clk160 : in std_logic := '0';
		ce : in std_logic := '0';
		mode : in std_logic := '0';
		load : out std_logic := '0';
		
		din : in std_logic_vector(9 downto 0) := (others => '0');
		dout : out std_logic := '0'
	);
end serializer;

architecture Behavioral of serializer is
	signal shift_reg : std_logic_vector(9 downto 0) := (others => '0');
	signal shift_cnt : integer range 0 to 9 := 0;
	signal load_160 : std_logic := '0';
	signal load_40 : std_logic := '0';
	signal load_stretch : std_logic := '0';
	signal load_stretch_reg : std_logic := '0';
	signal load_stretch_reg2 : std_logic := '0';
	signal load_ack : std_logic := '0';
begin

-- we need to stretch the load pulse of the synchronizer for the clock domain crossing
--process (load_160, load_40) begin
--	if load_40 = '1' then
--		load_stretch <= '0';
--	else
--		if rising_edge(load_160) then
--			load_stretch <= '1';
--		end if;
--	end if;
--end process;

-- synchronize stretched pulse
--process begin
--	wait until rising_edge(clk40);
--
--	load_stretch_reg <= load_stretch;
--	load_stretch_reg2 <= load_stretch_reg;
--end process;
--load_40 <= load_stretch_reg and not load_stretch_reg2;
load <= load_160;

-- synchronize load pulse to slow clock
--process (load_160, clk40) begin
--	if load_160 = '1' then
--		load_40 <= '1';
--	else
--		if rising_edge(clk40) then
--			-- acknowledge
--			if load_40 = '1' then
--				load_40 <= '0';
--			end if;
--			
--			-- synchronous output
--			load <= load_40;
--		end if;
--	end if;
--end process;

process begin
	wait until rising_edge(clk160);
	
	load_160 <= '0';
	
	if ce = '1' then
		if mode = '0' then
			if shift_cnt = 7 then
				shift_reg <= din;
				shift_cnt <= 0;
				load_160 <= '1';
			else
				shift_reg <= shift_reg(8 downto 0) & '0';
				shift_cnt <= shift_cnt + 1;
			end if;
		else
			if shift_cnt = 9 then
				shift_reg <= din;
				shift_cnt <= 0;
				load_160 <= '1';
			else
				shift_reg <= shift_reg(8 downto 0) & '0';
				shift_cnt <= shift_cnt + 1;
			end if;			
		end if;
		
		dout <= shift_reg(9);
	end if;
end process;

end Behavioral;

