----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Marius Wensing
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				BMF coarse delay
-- Description:			Coarse delay for TX path
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 13.12.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity cdelay is
	port (
		-- clock and reset
		clk160 : in std_logic := '0';
		reset : in std_logic := '1';
		
		-- data input
		din : in std_logic := '0';
		
		-- data output
		dout : out std_logic := '0';
		
		-- control
		cdelay : in std_logic_vector(7 downto 0) := (others => '0')
	);
end cdelay;

architecture Behavioral of cdelay is
	signal dout_prereg : std_logic := '0';
begin

shift_reg: SRLC32E
	PORT MAP (
		D => din,
		Q => dout_prereg,
		CE => '1',
		CLK => clk160,
		A => cdelay(4 downto 0),
		Q31 => open
	);

dout <= dout_prereg when rising_edge(clk160);

end Behavioral;

