-- TX path unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.bocpack.all;
use work.BuildVersion.all;

library encode_8b10b;

entity bmf_tx is
	generic
	(
		DEFAULT_XC_LINK : integer range 0 to 15;
		INCLUDE_ECR_RECONF : boolean := false
	);
	port
	(
		-- clocking
		clk_tx : in std_logic := '0';
		clk_tx_fast : in std_logic := '0';
		clk_tx_fast_strobe : in std_logic := '0';
		
		-- resets
		reset : in std_logic := '0';
		soft_reset : in std_logic := '0';
		
		-- input from ROD
		xc_vec : in std_logic_vector(15 downto 0);

		-- output
		txout : out std_logic := '0';

		-- emulator data
		txemudata : out std_logic := '0';

		-- trigger on command flag
		cmd_trigger : out std_logic;

		-- registers
		reg_rd : out RegBank_rd_t(0 to 31);
		reg_wr : in RegBank_wr_t(0 to 31);
		reg_wren : in RegBank_wren_t(0 to 31);
		reg_rden : in RegBank_rden_t(0 to 31);

		-- broadcast
		bcast_wr : in RegBank_wr_t(0 to 31);
		bcast_wren : in RegBank_wren_t(0 to 31);
		bcast_enable : in std_logic;

		-- register protection
		secure_reg : in std_logic
	);
end bmf_tx;

architecture Behavioral of bmf_tx is
	-- 8b10b encoder
	component encode_8b10b_wrapper
		port (
			CLK : in std_logic;
			DIN : in std_logic_vector(7 downto 0);
			KIN : in std_logic;
			DOUT : out std_logic_vector(9 downto 0);
			CE : in std_logic
		);
	end component;

	-- TX monitoring
	component TxMonitoring
		port (
			clk40 : in std_logic;
			reset : in std_logic;

			-- data input
			din : in std_logic;

			-- enable (will also reset counters and FIFO)
			enable : in std_logic;
			fifo_enable : in std_logic;

			-- FIFO info
			fifo_full : out std_logic;
			fifo_empty : out std_logic;

			-- trigger on command
			cmd_trigger_sel : in std_logic_vector(2 downto 0);
			cmd_trigger : out std_logic;

			-- simple counter readout
			cnt_rd : out std_logic_vector(7 downto 0);
			cnt_wr : in std_logic_vector(7 downto 0);
			cnt_rden : in std_logic;
			cnt_wren : in std_logic;

			-- FIFO for storing error patterns
			errfifo0_rd : out std_logic_vector(7 downto 0);
			errfifo0_wr : in std_logic_vector(7 downto 0);
			errfifo0_wren : in std_logic;
			errfifo0_rden : in std_logic;
			errfifo1_rd : out std_logic_vector(7 downto 0);
			errfifo1_wr : in std_logic_vector(7 downto 0);
			errfifo1_wren : in std_logic;
			errfifo1_rden : in std_logic
		);
	end component;

	component TxMonitoring_Pixel
		port (
			clk40 : in std_logic;
			reset : in std_logic;

			-- data input
			din : in std_logic;

			-- enable (will also reset counters and FIFO)
			enable : in std_logic;
			fifo_enable : in std_logic;

			-- FIFO info
			fifo_full : out std_logic;
			fifo_empty : out std_logic;

			-- trigger on command
			cmd_trigger_sel : in std_logic_vector(2 downto 0);
			cmd_trigger : out std_logic;

			-- simple counter readout
			cnt_rd : out std_logic_vector(7 downto 0);
			cnt_wr : in std_logic_vector(7 downto 0);
			cnt_rden : in std_logic;
			cnt_wren : in std_logic
		);
	end component;

	component ECRReconfGReg
		port (
			-- clock and reset
			clk : in std_logic;
			reset : in std_logic;

			-- monitoring for collision
			xc : in std_logic;

			-- data output (serial stream)
			reconf_dout : out std_logic;
			reconf_active : out std_logic;

			-- registers
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			status_rd : out std_logic_vector(7 downto 0);
			status_wr : in std_logic_vector(7 downto 0);
			status_rden : in std_logic;
			status_wren : in std_logic;
			addr_rd : out std_logic_vector(7 downto 0);
			addr_wr : in std_logic_vector(7 downto 0);
			addr_rden : in std_logic;
			addr_wren : in std_logic;
			chipid_rd : out std_logic_vector(7 downto 0);
			chipid_wr : in std_logic_vector(7 downto 0);
			chipid_rden : in std_logic;
			chipid_wren : in std_logic;
			data_lsb_rd : out std_logic_vector(7 downto 0);
			data_lsb_wr : in std_logic_vector(7 downto 0);
			data_lsb_rden : in std_logic;
			data_lsb_wren : in std_logic;
			data_msb_rd : out std_logic_vector(7 downto 0);
			data_msb_wr : in std_logic_vector(7 downto 0);
			data_msb_rden : in std_logic;
			data_msb_wren : in std_logic
		);
	end component;

	component ECRReconfBit
		generic (
			BITSTREAM_BYTES : integer := 4096		-- 4 kB for a bitstream
		);
		port (
			-- clock and reset
			clk : in std_logic;
			reset : in std_logic;

			-- monitoring for collision
			xc : in std_logic;
			boc_serialport : in std_logic;

			-- data output (serial stream)
			reconf_dout : out std_logic;
			reconf_active : out std_logic;

			-- registers
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			status_rd : out std_logic_vector(7 downto 0);
			status_wr : in std_logic_vector(7 downto 0);
			status_rden : in std_logic;
			status_wren : in std_logic;
			addr_lsb_rd : out std_logic_vector(7 downto 0);
			addr_lsb_wr : in std_logic_vector(7 downto 0);
			addr_lsb_rden : in std_logic;
			addr_lsb_wren : in std_logic;
			addr_msb_rd : out std_logic_vector(7 downto 0);
			addr_msb_wr : in std_logic_vector(7 downto 0);
			addr_msb_rden : in std_logic;
			addr_msb_wren : in std_logic;
			data_rd : out std_logic_vector(7 downto 0);
			data_wr : in std_logic_vector(7 downto 0);
			data_rden : in std_logic;
			data_wren : in std_logic
		);
	end component;

	COMPONENT tx_fifo
	  PORT (
		 clk : IN STD_LOGIC;
		 srst : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC
	  );
	END COMPONENT;

	-- ORed reset
	signal or_reset : std_logic;

	-- serial line selection
	signal xc : std_logic := '0';
	signal xc_sel : std_logic_vector(3 downto 0) := std_logic_vector(to_unsigned(DEFAULT_XC_LINK, 4));

	-- serial port FIFO
	signal txfifo_din : std_logic_vector(8 downto 0) := (others => '0');
	signal txfifo_dout : std_logic_vector(8 downto 0) := (others => '0');
	signal txfifo_wren : std_logic := '0';
	signal txfifo_rden : std_logic := '0';
	signal txfifo_full : std_logic := '0';
	signal txfifo_empty : std_logic := '0';
	signal txfifo_transmit : std_logic := '0';

	-- serial port
	signal serport_din : std_logic_vector(8 downto 0) := (others => '0');
	signal serport_dout : std_logic := '0';
	signal serport_cnt : integer range 0 to 7 := 0;
	signal serport_subcnt : integer range 0 to 7 := 0;
	signal serport_sr : std_logic_vector(7 downto 0) := (others => '0');
	signal serport_load : std_logic := '0';
	signal serport_en5M : std_logic := '0';
	signal serport_is5M : std_logic := '0';

	-- serial port for 8b10b mode
	signal serport8b10b_din : std_logic_vector(9 downto 0) := (others => '0');
	signal serport8b10b_dout : std_logic_vector(3 downto 0) := (others => '0');
	signal serport8b10b_load : std_logic := '0';
	signal serport8b10b_sr : std_logic_vector(9 downto 0) := (others => '0');
	signal serport8b10b_cnt : integer range 0 to 4 := 0;

	-- encoder
	signal enc_din : std_logic_vector(7 downto 0) := (others => '0');
	signal enc_kchar : std_logic := '0';
	signal enc_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal enc_ce : std_logic := '0';

	-- first stage of coarse delay (25 ns steps)
	signal coarse_s1_din : std_logic := '0';
	signal coarse_s1_dout : std_logic := '0';
	signal coarse_s1_delay : std_logic_vector(4 downto 0) := "00000";

	-- second stage of coarse delay (3.125 ns steps)
	signal coarse_s2_sr : std_logic_vector(14 downto 0) := (others => '0');
	signal coarse_s2_din : std_logic_vector(7 downto 0) := (others => '0');
	signal coarse_s2_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal coarse_s2_delay : integer range 0 to 7 := 0;

	-- BPM encoder
	signal bpm_din : std_logic := '0';
	signal bpm_din_reg : std_logic := '0';
	signal bpm_dout : std_logic_vector(7 downto 0) := (others => '0');

	-- SERDES
	signal serdes_cascade_tci : std_logic;
	signal serdes_cascade_dci : std_logic;
	signal serdes_cascade_tco : std_logic;
	signal serdes_cascade_dco : std_logic;
	signal serdes_din : std_logic_vector(7 downto 0) := (others => '0');
	signal serdes_dout : std_logic;

	-- IODELAY
	signal iodelay_din : std_logic;
	signal iodelay_dout : std_logic;

	-- register interface
	signal control_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal control2_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal status_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal coarse_reg : std_logic_vector(7 downto 0) := (others => '0');

	-- control and status bits
	signal bpm_disable : std_logic := '0';
	signal input_select : std_logic := '0';
	signal mute_optical : std_logic := '0';
	signal tx_enable : std_logic := '0';
	signal tx_mode : std_logic := '0';
	signal txmon_enable : std_logic := '0';
	signal txmon_fifo_enable : std_logic := '0';
	signal txmon_fifo_full : std_logic := '0';
	signal txmon_fifo_empty : std_logic := '0';
	signal cmd_trigger_sel : std_logic_vector(2 downto 0) := "000";

	-- txmon counter registers
	signal txmon_cnt_rd : std_logic_vector(7 downto 0) := x"00";
	signal txmon_cnt_wr : std_logic_vector(7 downto 0) := x"00";
	signal txmon_cnt_wren : std_logic := '0';
	signal txmon_cnt_rden : std_logic := '0';

	-- data to the emulator
	signal txemudata_i : std_logic := '0';

	-- serial data after mux
	signal ser_data : std_logic := '0';

	-- reconfiguration
	signal reconf_dout : std_logic := '0';
	signal reconf_active : std_logic := '0';
begin

-- OR reset and soft-reset
or_reset <= reset or soft_reset when rising_edge(clk_tx);

-- select xc data line
xc <= xc_vec(to_integer(unsigned(xc_sel))); -- when rising_edge(clk_tx);

-- encoder instance
enc_i: encode_8b10b_wrapper
	port map (
		CLK => clk_tx,
		CE => enc_ce,
		DIN => enc_din,
		KIN => enc_kchar,
		DOUT => enc_dout
	);

-- serial port FIFO
txfifo_i: tx_fifo
	port map (
		clk => clk_tx,
		srst => or_reset,
		din => txfifo_din,
		dout => txfifo_dout,
		wr_en => txfifo_wren,
		rd_en => txfifo_rden,
		full => txfifo_full,
		empty => txfifo_empty
	);

-- serial port
process begin
	wait until rising_edge(clk_tx);

	if or_reset = '1' then
		serport_cnt <= 0;
		serport_subcnt <= 0;
		serport_load <= '0';
		serport_dout <= '0';
		serport_sr <= (others => '0');
		serport_is5M <= '0';
	else
		if serport_cnt = 7 then
			if (serport_en5M = '1') and (serport_is5M = '1') then
				if serport_subcnt = 7 then
					serport_subcnt <= 0;
					serport_cnt <= 0;
					serport_sr <= serport_din(7 downto 0);
					serport_is5M <= serport_din(8);
					serport_load <= '1';
				else
					serport_subcnt <= serport_subcnt + 1;
					serport_load <= '0';
				end if;
			else
				serport_cnt <= 0;
				serport_subcnt <= 0;
				serport_sr <= serport_din(7 downto 0);
				serport_is5M <= serport_din(8);
				serport_load <= '1';
			end if;
		else
			if (serport_en5M = '1') and (serport_is5M = '1') then
				if serport_subcnt = 7 then
					serport_subcnt <= 0;
					serport_cnt <= serport_cnt + 1;
					serport_sr <= serport_sr(6 downto 0) & '0';
					serport_load <= '0';
				else
					serport_subcnt <= serport_subcnt + 1;
					serport_load <= '0';
				end if;
			else
				serport_cnt <= serport_cnt + 1;
				serport_subcnt <= 0;
				serport_sr <= serport_sr(6 downto 0) & '0';
				serport_load <= '0';
			end if;
		end if;
		serport_dout <= serport_sr(7);
	end if;
end process;

-- serial port for 8b10b data
process begin
	wait until rising_edge(clk_tx);

	if or_reset = '1' then
		serport8b10b_load <= '0';
		serport8b10b_cnt <= 0;
		serport8b10b_sr <= (others => '0');
	else
		if serport8b10b_cnt = 2 then
			serport8b10b_dout <= serport8b10b_din(1 downto 0) & serport8b10b_sr(1 downto 0);
			serport8b10b_sr <= "00" & serport8b10b_din(9 downto 2);
			serport8b10b_load <= '1';
		elsif serport8b10b_cnt = 4 then
			serport8b10b_dout <= serport8b10b_sr(3 downto 0);
			serport8b10b_sr <= serport8b10b_din;
			serport8b10b_load <= '1';
		else
			serport8b10b_dout <= serport8b10b_sr(3 downto 0);
			serport8b10b_sr <= "0000" & serport8b10b_sr(9 downto 4);
			serport8b10b_load <= '0';
		end if;

		if serport8b10b_cnt = 4 then
			serport8b10b_cnt <= 0;
		else
			serport8b10b_cnt <= serport8b10b_cnt + 1;
		end if;
	end if;
end process;

-- first stage coarse delay
coarse_s1: SRLC32E
	port map (
		Q => coarse_s1_dout,
		Q31 => open,
		D => coarse_s1_din,
		CLK => clk_tx,
		CE => '1',
		A => coarse_s1_delay
	);

-- second stage coarse delay
process begin
	wait until rising_edge(clk_tx);

	coarse_s2_sr <= coarse_s2_sr(6 downto 0) & coarse_s2_din;
end process;
coarse_s2_dout <= coarse_s2_sr(7+coarse_s2_delay downto coarse_s2_delay);

-- coarse delay settings
coarse_s1_delay <= std_logic_vector(unsigned(coarse_reg(7 downto 3))-1) when rising_edge(clk_tx);
coarse_s2_delay <= to_integer(unsigned(coarse_reg(2 downto 0))) when rising_edge(clk_tx);

-- BPM encoding
process begin
	wait until rising_edge(clk_tx);

	if reset = '1' then
		bpm_dout <= (others => '0');
	else
		if bpm_disable = '1' then
			bpm_dout <= (others => bpm_din);
		else
			bpm_dout(7 downto 4) <= not bpm_dout(3 downto 0);
			if bpm_din = '0' then
				bpm_dout(3 downto 0) <= not bpm_dout(3 downto 0);
			end if;
		end if;
	end if;
end process;

-- SERDES
OSERDES2_master : OSERDES2
	generic map (
		BYPASS_GCLK_FF => FALSE,	-- Bypass CLKDIV syncronization registers (TRUE/FALSE)
		DATA_RATE_OQ => "SDR",		-- Output Data Rate ("SDR" or "DDR")
		DATA_RATE_OT => "SDR",		-- 3-state Data Rate ("SDR" or "DDR")
		DATA_WIDTH => 8,			-- Parallel data width (2-8)
		OUTPUT_MODE => "SINGLE_ENDED", -- "SINGLE_ENDED" or "DIFFERENTIAL"
		SERDES_MODE => "MASTER"		-- "NONE", "MASTER" or "SLAVE"
	)
	port map (
		OQ => serdes_dout,	-- 1-bit output Data output to pad or IODELAY2
		SHIFTOUT1 => serdes_cascade_dci, -- 1-bit output Cascade data output
		SHIFTOUT2 => serdes_cascade_tci, -- 1-bit output Cascade 3-state output
		SHIFTOUT3 => open, 	-- 1-bit output Cascade differential data output
		SHIFTOUT4 => open, 	-- 1-bit output Cascade differential 3-state output
		TQ => open,				-- 1-bit output 3-state output to pad or IODELAY2
		CLK0 => clk_tx_fast,			-- 1-bit input I/O clock input
		CLK1 => '0',			-- 1-bit input Secondary I/O clock input
		CLKDIV => clk_tx,		-- 1-bit input Logic domain clock input
								-- D1 - D4: 1-bit (each) input Parallel data inputs
		D1 => serdes_din(3),
		D2 => serdes_din(2),
		D3 => serdes_din(1),
		D4 => serdes_din(0),
		IOCE => clk_tx_fast_strobe,	-- 1-bit input Data strobe input
		OCE => '1',					-- 1-bit input Clock enable input
		RST => reset,			-- 1-bit input Asynchrnous reset input
		SHIFTIN1 => '1',			-- 1-bit input Cascade data input
		SHIFTIN2 => '1',			-- 1-bit input Cascade 3-state input
		SHIFTIN3 => serdes_cascade_dco,	-- 1-bit input Cascade differential data input
		SHIFTIN4 => serdes_cascade_tco,	-- 1-bit input Cascade differential 3-state input
								-- T1 - T4: 1-bit (each) input 3-state control inputs
		T1 => '0',
		T2 => '0',
		T3 => '0',
		T4 => '0',
		TCE => '1',				-- 1-bit input 3-state clock enable input
		TRAIN => '0'			-- 1-bit input Training pattern enable input
);

OSERDES2_slave : OSERDES2
	generic map (
		BYPASS_GCLK_FF => FALSE,	-- Bypass CLKDIV syncronization registers (TRUE/FALSE)
		DATA_RATE_OQ => "SDR",		-- Output Data Rate ("SDR" or "DDR")
		DATA_RATE_OT => "SDR",		-- 3-state Data Rate ("SDR" or "DDR")
		DATA_WIDTH => 8,			-- Parallel data width (2-8)
		OUTPUT_MODE => "SINGLE_ENDED", -- "SINGLE_ENDED" or "DIFFERENTIAL"
		SERDES_MODE => "SLAVE"		-- "NONE", "MASTER" or "SLAVE"
	)
	port map (
		OQ => open,				-- 1-bit output Data output to pad or IODELAY2
		SHIFTOUT1 => open,	-- 1-bit output Cascade data output
		SHIFTOUT2 => open, 	-- 1-bit output Cascade 3-state output
		SHIFTOUT3 => serdes_cascade_dco, -- 1-bit output Cascade differential data output
		SHIFTOUT4 => serdes_cascade_tco, -- 1-bit output Cascade differential 3-state output	
		TQ => open,				-- 1-bit output 3-state output to pad or IODELAY2
		CLK0 => clk_tx_fast,			-- 1-bit input I/O clock input
		CLK1 => '0',			-- 1-bit input Secondary I/O clock input
		CLKDIV => clk_tx,		-- 1-bit input Logic domain clock input
								-- D1 - D4: 1-bit (each) input Parallel data inputs
		D1 => serdes_din(7),
		D2 => serdes_din(6),
		D3 => serdes_din(5),
		D4 => serdes_din(4),
		IOCE => clk_tx_fast_strobe,	-- 1-bit input Data strobe input
		OCE => '1',				-- 1-bit input Clock enable input
		RST => reset,			-- 1-bit input Asynchrnous reset input
		SHIFTIN1 => serdes_cascade_dci,	-- 1-bit input Cascade data input
		SHIFTIN2 => serdes_cascade_tci,	-- 1-bit input Cascade 3-state input
		SHIFTIN3 => '1',	-- 1-bit input Cascade differential data input
		SHIFTIN4 => '1',	-- 1-bit input Cascade differential 3-state input
								-- T1 - T4: 1-bit (each) input 3-state control inputs
		T1 => '0',
		T2 => '0',
		T3 => '0',
		T4 => '0',
		TCE => '1',				-- 1-bit input 3-state clock enable input
		TRAIN => '0'			-- 1-bit input Training pattern enable input
);

-- fine delay block
IODELAY_i: IODELAY2
	GENERIC MAP (
		DATA_RATE => "SDR",
		DELAY_SRC => "ODATAIN",
		ODELAY_VALUE => 0
	)
	PORT MAP (
		BUSY => open,
		CAL => '0',
		CE => '0',
		CLK => '0',
		DATAOUT => open,
		DATAOUT2 => open,
		DOUT => iodelay_dout,
		IDATAIN => '0',
		INC => '0',
		IOCLK0 => '0',
		IOCLK1 => '0',
		ODATAIN => iodelay_din,
		RST => '0',
		T => '0',
		TOUT => open
	);

-- data monitoring
txmon_ibl: if PixelRx = false generate
	txmon_i: TxMonitoring port map (
		clk40 => clk_tx,
		reset => or_reset,
		din => txemudata_i,
		enable => txmon_enable,
		fifo_enable => txmon_fifo_enable,
		fifo_empty => txmon_fifo_empty,
		fifo_full => txmon_fifo_full,
		cmd_trigger_sel => cmd_trigger_sel,
		cmd_trigger => cmd_trigger,
		cnt_rd => txmon_cnt_rd,
		cnt_wr => txmon_cnt_wr,
		cnt_rden => txmon_cnt_rden,
		cnt_wren => txmon_cnt_wren,
		errfifo0_rd => reg_rd(7),
		errfifo0_wr => reg_wr(7),
		errfifo0_wren => reg_wren(7),
		errfifo0_rden => reg_rden(7),
		errfifo1_rd => reg_rd(8),
		errfifo1_wr => reg_wr(8),
		errfifo1_wren => reg_wren(8),
		errfifo1_rden => reg_rden(8)
	);
end generate;

txmon_pix: if PixelRx = true generate
	txmon_i: TxMonitoring_Pixel port map (
		clk40 => clk_tx,
		reset => or_reset,
		din => txemudata_i,
		enable => txmon_enable,
		fifo_enable => txmon_fifo_enable,
		fifo_empty => txmon_fifo_empty,
		fifo_full => txmon_fifo_full,
		cmd_trigger_sel => cmd_trigger_sel,
		cmd_trigger => cmd_trigger,
		cnt_rd => txmon_cnt_rd,
		cnt_wr => txmon_cnt_wr,
		cnt_rden => txmon_cnt_rden,
		cnt_wren => txmon_cnt_wren
	);
end generate;

ecr_generate: if INCLUDE_ECR_RECONF = true generate
	ecr_greg: if ECRReconfBitEnable = false generate
		ecr_i: ECRReconfGReg port map (
			clk => clk_tx,
			reset => or_reset,
			xc => ser_data,
			reconf_dout => reconf_dout,
			reconf_active => reconf_active,
			control_rd => reg_rd(10),
			control_wr => reg_wr(10),
			control_rden => reg_rden(10),
			control_wren => reg_wren(10),
			status_rd => reg_rd(11),
			status_wr => reg_wr(11),
			status_rden => reg_rden(11),
			status_wren => reg_wren(11),
			addr_rd => reg_rd(12),
			addr_wr => reg_wr(12),
			addr_rden => reg_rden(12),
			addr_wren => reg_wren(12),
			chipid_rd => reg_rd(13),
			chipid_wr => reg_wr(13),
			chipid_rden => reg_rden(13),
			chipid_wren => reg_wren(13),
			data_lsb_rd => reg_rd(14),
			data_lsb_wr => reg_wr(14),
			data_lsb_rden => reg_rden(14),
			data_lsb_wren => reg_wren(14),
			data_msb_rd => reg_rd(15),
			data_msb_wr => reg_wr(15),
			data_msb_rden => reg_rden(15),
			data_msb_wren => reg_wren(15)
		);
	end generate;

	ecr_bit: if ECRReconfBitEnable = true generate
		ecr_i: ECRReconfBit
			generic map (
				BITSTREAM_BYTES => 4096
			)
			port map (
				clk => clk_tx,
				reset => reset,
				xc => xc,
				boc_serialport => serport_dout,
				reconf_dout => reconf_dout,
				reconf_active => reconf_active,
				control_rd => reg_rd(10),
				control_wr => reg_wr(10),
				control_rden => reg_rden(10),
				control_wren => reg_wren(10),
				status_rd => reg_rd(11),
				status_wr => reg_wr(11),
				status_rden => reg_rden(11),
				status_wren => reg_wren(11),
				addr_lsb_rd => reg_rd(12),
				addr_lsb_wr => reg_wr(12),
				addr_lsb_rden => reg_rden(12),
				addr_lsb_wren => reg_wren(12),
				addr_msb_rd => reg_rd(13),
				addr_msb_wr => reg_wr(13),
				addr_msb_rden => reg_rden(13),
				addr_msb_wren => reg_wren(13),
				data_rd => reg_rd(14),
				data_wr => reg_wr(14),
				data_rden => reg_rden(14),
				data_wren => reg_wren(14)
			);
	end generate;
end generate;

ecr_not_generate: if INCLUDE_ECR_RECONF = false generate
	reconf_active <= '0';
	reconf_dout <= '0';
end generate;

-- mapping TX monitoring counter signals to allow broadcasting
txmon_cnt_wr <= bcast_wr(6) when (bcast_wren(6) = '1' and bcast_enable = '1') else reg_wr(6);
txmon_cnt_wren <= reg_wren(6) or (bcast_wren(6) and bcast_enable);
txmon_cnt_rden <= reg_rden(6);
reg_rd(6) <= txmon_cnt_rd;

-- connect stages
ser_data <= xc when (input_select = '0') else serport_dout;
coarse_s1_din <= ser_data when reconf_active = '0' else reconf_dout;
coarse_s2_din <= bpm_dout;
iodelay_din <= serdes_dout;
txout <= iodelay_dout;
serport_din <= txfifo_dout when (tx_enable = '1') and (txfifo_transmit = '1') and (txfifo_empty = '0') else (others => '0');
enc_din <= txfifo_dout(7 downto 0) when (tx_enable = '1') and (txfifo_transmit = '1') and (txfifo_empty = '0') else x"3C";
enc_kchar <= txfifo_dout(8) when (tx_enable = '1') and (txfifo_transmit = '1') and (txfifo_empty = '0') else '1';
enc_ce <= serport8b10b_load;
serport8b10b_din <= enc_dout;
process (coarse_reg, coarse_s1_din, coarse_s1_dout, mute_optical) begin
	if mute_optical = '1' then
		bpm_din <= '0';
	else
		if coarse_reg(7 downto 3) = "00000" then
			bpm_din <= coarse_s1_din;
		else
			bpm_din <= coarse_s1_dout;
		end if;
	end if;
end process;

-- TX FIFO read
process (tx_mode, serport_load, serport8b10b_load, txfifo_empty, txfifo_transmit) begin
	if (txfifo_empty = '0') and (txfifo_transmit = '1') then
		if tx_mode = '0' then
			txfifo_rden <= serport_load;
		else
			txfifo_rden <= serport8b10b_load;
		end if;
	else
		txfifo_rden <= '0';
	end if;
end process;

-- data output
process (tx_mode, coarse_s2_dout, serport8b10b_dout) begin
	if tx_mode = '1' then
		-- 160 MBit/s 8b10b mode
		serdes_din(7) <= serport8b10b_dout(0);
		serdes_din(6) <= serport8b10b_dout(0);
		serdes_din(5) <= serport8b10b_dout(1);
		serdes_din(4) <= serport8b10b_dout(1);
		serdes_din(3) <= serport8b10b_dout(2);
		serdes_din(2) <= serport8b10b_dout(2);
		serdes_din(1) <= serport8b10b_dout(3);
		serdes_din(0) <= serport8b10b_dout(3);
	else
		-- standard BPM 40 MBit/s mode
		serdes_din <= coarse_s2_dout;
	end if;
end process;

-- emulator data
txemudata_i <= coarse_s1_din when rising_edge(clk_tx);
txemudata <= txemudata_i;

-- register interface
process begin
	wait until rising_edge(clk_tx);

	-- default
	txfifo_wren <= '0';

	if reset = '1' then
		coarse_reg <= (others => '0');
		control_reg <= x"01";
		control2_reg <= x"00";
		xc_sel <= std_logic_vector(to_unsigned(DEFAULT_XC_LINK, 4));
	elsif soft_reset = '1' then
		control_reg <= x"01";
		control2_reg <= x"00";
		xc_sel <= std_logic_vector(to_unsigned(DEFAULT_XC_LINK, 4));
	else
		if reg_wren(0) = '1' then
			if secure_reg = '1' then
				control_reg <= reg_wr(0);
			else
				-- mask out bits 4 (BPM disable) and 1 (160 MBit/s mode)
				control_reg <= reg_wr(0) and "11101101";
			end if;
		end if;
		if (bcast_enable = '1') and (bcast_wren(0) = '1') then
			if secure_reg = '1' then
				control_reg <= bcast_wr(0);
			else
				-- mask out bits 4 (BPM disable) and 1 (160 MBit/s mode)
				control_reg <= bcast_wr(0) and "11101101";
			end if;
		end if;

		if reg_wren(2) = '1' then
			txfifo_wren <= not txfifo_full;
			txfifo_din <= '1' & reg_wr(2);
		end if;
		if (bcast_enable = '1') and (bcast_wren(2) = '1') then
			txfifo_wren <= not txfifo_full;
			txfifo_din <= '1' & bcast_wr(2);
		end if;

		if reg_wren(3) = '1' then
			txfifo_wren <= not txfifo_full;
			txfifo_din <= '0' & reg_wr(3);
		end if;
		if (bcast_enable = '1') and (bcast_wren(3) = '1') then
			txfifo_wren <= not txfifo_full;
			txfifo_din <= '0' & bcast_wr(3);
		end if;

		if reg_wren(4) = '1' then
			control2_reg <= reg_wr(4);
		end if;
		if (bcast_enable = '1') and (bcast_wren(4) = '1') then
			control2_reg <= bcast_wr(4);
		end if;

		if (reg_wren(5) = '1') and (secure_reg = '1') then
			coarse_reg <= reg_wr(5);
		end if;
		if (bcast_enable = '1') and (bcast_wren(5) = '1') and (secure_reg = '1') then
			coarse_reg <= bcast_wr(5);
		end if;

		if reg_wren(9) = '1' then
			xc_sel <= reg_wr(9)(3 downto 0);
		end if;
		if (bcast_enable = '1') and (bcast_wren(9) = '1') then
			xc_sel <= bcast_wr(9)(3 downto 0);
		end if;
	end if;
end process;

reg_rd(0) <= control_reg;
reg_rd(1) <= status_reg;
reg_rd(2) <= x"00";
reg_rd(3) <= x"00";
reg_rd(4) <= control2_reg;
reg_rd(5) <= coarse_reg;
reg_rd(9) <= "0000" & xc_sel;

-- mapping of control and status bits
tx_enable <= control_reg(0);
tx_mode <= control_reg(1);
txfifo_transmit <= control_reg(2);
bpm_disable <= control_reg(4);
input_select <= control_reg(6);
mute_optical <= control2_reg(0);
txmon_enable <= control2_reg(2);
txmon_fifo_enable <= control2_reg(3);
serport_en5M <= control2_reg(4);
cmd_trigger_sel <= control2_reg(7 downto 5);

-- status bits
status_reg(0) <= txfifo_empty;
status_reg(1) <= txfifo_full;
status_reg(3) <= mute_optical;
status_reg(4) <= txmon_fifo_empty;
status_reg(5) <= txmon_fifo_full;
	
end Behavioral;
