library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity prng_tx is
  generic (
  	words : integer := 64;
  	seed : std_logic_vector(7 downto 0) := x"42"
  );
  port (
	clk : in std_logic;
	reset : in std_logic;

	enable : in std_logic;
	data : out std_logic_vector(7 downto 0);
	kchar : out std_logic;
	ack : in std_logic
  );
end entity ; -- prng_tx

architecture RTL of prng_tx is

component prng
	generic (
		width : integer := 32
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		load : in std_logic;
		seed : in std_logic_vector(width-1 downto 0);
		step : in std_logic;
		dout : out std_logic_vector(width-1 downto 0)
	);
end component;

signal prng_load : std_logic := '0';
signal prng_step : std_logic := '0';
signal prng_dout : std_logic_vector(7 downto 0) := (others => '0');

type fsm_states is (st_idle, st_sof, st_data, st_eof);
signal Z : fsm_states := st_sof;
signal wordcnt : integer range 0 to words-1 := 0;

begin

-- prng instance
prng_i: prng
	GENERIC MAP (
		width => 8
	)
	PORT MAP (
		clk => clk,
		reset => reset,
		load => prng_load,
		seed => seed,
		step => prng_step,
		dout => prng_dout
	);

-- fsm
process begin
	wait until rising_edge(clk);

	-- default values
	prng_step <= '0';
	prng_load <= '0';

	if reset = '1' then
		Z <= st_idle;
		wordcnt <= 0;
	else
		case Z is
			when st_idle =>
				data <= x"3C";
				kchar <= '1';

				if enable = '1' then
					Z <= st_sof;
				end if;

			when st_sof =>
				data <= x"FC";
				kchar <= '1';
				prng_load <= '1';
				if ack = '1' then
					Z <= st_data;
				end if;

			when st_data =>
				data <= prng_dout;
				kchar <= '0';
				if ack = '1' then
					prng_step <= '1';
					if wordcnt = words-1 then
						wordcnt <= 0;
						Z <= st_eof;
					else
						wordcnt <= wordcnt + 1;
					end if;
				end if;

			when st_eof =>
				data <= x"BC";
				kchar <= '1';
				if ack = '1' then
					if enable = '1' then
						Z <= st_sof;
					else
						Z <= st_idle;
					end if;
				end if;
		end case;
	end if;

end process;

end architecture ; -- RTL