library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity TxMonitoring is
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic;

		-- enable (will also reset counters and FIFO)
		enable : in std_logic;
		fifo_enable : in std_logic;

		-- FIFO info
		fifo_full : out std_logic;
		fifo_empty : out std_logic;

		-- trigger on commands
		cmd_trigger_sel : in std_logic_vector(2 downto 0);
		cmd_trigger : out std_logic;

		-- simple counter readout
		cnt_rd : out std_logic_vector(7 downto 0);
		cnt_wr : in std_logic_vector(7 downto 0);
		cnt_rden : in std_logic;
		cnt_wren : in std_logic;

		-- FIFO for storing error patterns
		errfifo0_rd : out std_logic_vector(7 downto 0);
		errfifo0_wr : in std_logic_vector(7 downto 0);
		errfifo0_wren : in std_logic;
		errfifo0_rden : in std_logic;
		errfifo1_rd : out std_logic_vector(7 downto 0);
		errfifo1_wr : in std_logic_vector(7 downto 0);
		errfifo1_wren : in std_logic;
		errfifo1_rden : in std_logic
	);
end TxMonitoring;

architecture rtl of TxMonitoring is

COMPONENT TxMonFifo
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

-- counters for trigger, BCR and ECR
signal trig_cnt : unsigned(31 downto 0) := (others => '0');
signal ecr_cnt : unsigned(31 downto 0) := (others => '0');
signal bcr_cnt : unsigned(31 downto 0) := (others => '0');
signal cal_cnt : unsigned(31 downto 0) := (others => '0');
signal slow_cnt : unsigned(31 downto 0) := (others => '0');
signal corrupt_cnt : unsigned(31 downto 0) := (others => '0');

-- shift register for counter monitoring
signal cntmon_sr : std_logic_vector(12 downto 0) := (others => '0');
signal ignore_cnt : integer range 0 to 695 := 0;

-- readout shift register for counters
signal readcnt_sr : std_logic_vector(31 downto 0) := (others => '0');

-- command triggers
signal cmd_trig : std_logic := '0';
signal cmd_ecr : std_logic := '0';
signal cmd_bcr : std_logic := '0';
signal cmd_cal : std_logic := '0';
signal cmd_slow : std_logic := '0';
signal cmd_corrupt : std_logic := '0';

-- FIFO signals
signal fifo_din : std_logic_vector(15 downto 0) := (others => '0');
signal fifo_dout : std_logic_vector(15 downto 0) := (others => '0');
signal fifo_wren : std_logic := '0';
signal fifo_rden : std_logic := '0';
signal fifo_full_i : std_logic := '0';
signal fifo_empty_i : std_logic := '0';

begin

-- FIFO instance
fifo_i: TxMonFifo port map (
	clk => clk40,
	rst => reset,
	din => fifo_din,
	dout => fifo_dout,
	wr_en => fifo_wren,
	rd_en => fifo_rden,
	full => fifo_full_i,
	empty => fifo_empty_i
);

-- shift in new data for monitoring in counter mode
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		cntmon_sr <= (others => '0');
	else
		cntmon_sr <= cntmon_sr(11 downto 0) & din;
	end if;
end process;

-- compare the shift register for commands
process begin
	wait until rising_edge(clk40);

	-- default
	fifo_wren <= '0';

	-- default external triggers
	cmd_trig <= '0';
	cmd_ecr <= '0';
	cmd_bcr <= '0';
	cmd_cal <= '0';
	cmd_slow <= '0';
	cmd_corrupt <= '0';

	if (reset = '1') or (enable = '0') then
		trig_cnt <= (others => '0');
		ecr_cnt <= (others => '0');
		bcr_cnt <= (others => '0');
		cal_cnt <= (others => '0');
		slow_cnt <= (others => '0');
		corrupt_cnt <= (others => '0');
	else
		if ignore_cnt = 0 then
			-- compare shift register contents
			if cntmon_sr(12 downto 8) = "11101" then			-- Trigger
				trig_cnt <= trig_cnt + 1;
				ignore_cnt <= 5;
				cmd_trig <= '1';
			elsif cntmon_sr(12 downto 4) = "101100001" then		-- BCR
				bcr_cnt <= bcr_cnt + 1;
				ignore_cnt <= 9;
				cmd_bcr <= '1';
			elsif cntmon_sr(12 downto 4) = "101100010" then		-- ECR
				ecr_cnt <= ecr_cnt + 1;
				ignore_cnt <= 9;
				cmd_ecr <= '1';
			elsif cntmon_sr(12 downto 4) = "101100100" then		-- CAL
				cal_cnt <= cal_cnt + 1;
				ignore_cnt <= 9;
				cmd_cal <= '1';
			elsif cntmon_sr(12 downto 4) = "101101000" then		-- Slow command
				slow_cnt <= slow_cnt + 1;
				case cntmon_sr(3 downto 0) is
					when "0001" => ignore_cnt <= 23;			-- read register
					when "0010" => ignore_cnt <= 39;			-- write register
					when "0100" => ignore_cnt <= 695;			-- write front end
					when "1000" => ignore_cnt <= 17;			-- global reset
					when "1001" => ignore_cnt <= 23;			-- global pulse
					when "1010" => ignore_cnt <= 23;			-- runmode
					when others => ignore_cnt <= 23;
				end case;
				cmd_slow <= '1';
			elsif cntmon_sr(cntmon_sr'left) = '1' then	-- anything else
				corrupt_cnt <= corrupt_cnt + 1;
				cmd_corrupt <= '1';
				if fifo_enable = '1' then
					fifo_din <= "000" & cntmon_sr;
					fifo_wren <= not fifo_full_i;
				end if;
				ignore_cnt <= 23;
			end if;
		else
			ignore_cnt <= ignore_cnt - 1;
		end if;		
	end if;
end process;

-- counter readout
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		readcnt_sr <= (others => '0');
	else
		if cnt_wren = '1' then
			case cnt_wr(2 downto 0) is
				when "000" =>	readcnt_sr <= std_logic_vector(trig_cnt);
				when "001" =>	readcnt_sr <= std_logic_vector(ecr_cnt);
				when "010" =>	readcnt_sr <= std_logic_vector(bcr_cnt);
				when "011" =>	readcnt_sr <= std_logic_vector(cal_cnt);
				when "100" =>	readcnt_sr <= std_logic_vector(slow_cnt);
				when "101" =>	readcnt_sr <= std_logic_vector(corrupt_cnt);
				when others =>  readcnt_sr <= (others => '0');
			end case;
		end if;

		if cnt_rden = '1' then
			readcnt_sr <= readcnt_sr(23 downto 0) & x"00";
		end if;
	end if;
end process;

-- read back from MSB to LSB
cnt_rd <= readcnt_sr(31 downto 24);

-- output FIFO status
fifo_full <= fifo_full_i;
fifo_empty <= fifo_empty_i;

-- fifo readback
errfifo0_rd <= fifo_dout(7 downto 0);
errfifo1_rd <= fifo_dout(15 downto 8);
fifo_rden <= errfifo0_rden and not fifo_empty_i;

-- select trigger
cmd_trigger <= cmd_trig when cmd_trigger_sel = "000" else
               cmd_ecr when cmd_trigger_sel = "001" else
               cmd_bcr when cmd_trigger_sel = "010" else
               cmd_cal when cmd_trigger_sel = "011" else
               cmd_slow when cmd_trigger_sel = "100" else
               cmd_corrupt when cmd_trigger_sel = "101" else
               '0';

end architecture;
