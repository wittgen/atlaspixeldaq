library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ECRDetect is
	port (
		-- clock and reset
		clk : in std_logic;
		reset : in std_logic;

		-- serial data input
		xc : in std_logic;

		-- ecr detection flag
		ecr_detect : out std_logic
	);
end ECRDetect;

architecture rtl of ECRDetect is
	signal ecr_detect_sr : std_logic_vector(31 downto 0);
begin

process begin
	wait until rising_edge(clk);

	if reset = '1' then
		ecr_detect_sr <= (others => '0');
	else
		ecr_detect_sr <= ecr_detect_sr(ecr_detect_sr'left-1 downto 0) & xc;
	end if;

	if ecr_detect_sr = "00000000000000001011000100000000" then
		ecr_detect <= '1';
	else
		ecr_detect <= '0';
	end if;
end process;

end architecture;