library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ECRReconfGReg is
	port (
		-- clock and reset
		clk : in std_logic;
		reset : in std_logic;

		-- monitoring for collision
		xc : in std_logic;

		-- data output (serial stream)
		reconf_dout : out std_logic;
		reconf_active : out std_logic;

		-- registers
		control_rd : out std_logic_vector(7 downto 0);
		control_wr : in std_logic_vector(7 downto 0);
		control_rden : in std_logic;
		control_wren : in std_logic;
		status_rd : out std_logic_vector(7 downto 0);
		status_wr : in std_logic_vector(7 downto 0);
		status_rden : in std_logic;
		status_wren : in std_logic;
		addr_rd : out std_logic_vector(7 downto 0);
		addr_wr : in std_logic_vector(7 downto 0);
		addr_rden : in std_logic;
		addr_wren : in std_logic;
		chipid_rd : out std_logic_vector(7 downto 0);
		chipid_wr : in std_logic_vector(7 downto 0);
		chipid_rden : in std_logic;
		chipid_wren : in std_logic;
		data_lsb_rd : out std_logic_vector(7 downto 0);
		data_lsb_wr : in std_logic_vector(7 downto 0);
		data_lsb_rden : in std_logic;
		data_lsb_wren : in std_logic;
		data_msb_rd : out std_logic_vector(7 downto 0);
		data_msb_wr : in std_logic_vector(7 downto 0);
		data_msb_rden : in std_logic;
		data_msb_wren : in std_logic
	);
end ECRReconfGReg;

architecture rtl of ECRReconfGReg is
	-- constants
	constant CMD_RUNMODE : std_logic_vector(38 downto 0)  := "000001011010001010100011100000000000000";
	constant CMD_CONFMODE : std_logic_vector(38 downto 0) := "000001011010001010100000011100000000000";
	constant CMD_WRREG : std_logic_vector(12 downto 0) := "1011010000010";

	-- configuration memory
	type config_mem_t is array (0 to 4*64-1) of std_logic_vector(15 downto 0);
	signal config_mem : config_mem_t := (others => (others => '0'));
	signal config_mem_din : std_logic_vector(15 downto 0) := (others => '0');
	signal config_mem_dout1 : std_logic_vector(15 downto 0) := (others => '0');
	signal config_mem_dout2 : std_logic_vector(15 downto 0) := (others => '0');
	signal config_mem_wrptr : integer range 0 to 4*64-1 := 0;
	signal config_mem_rdptr1 : integer range 0 to 4*64-1 := 0;
	signal config_mem_rdptr2 : integer range 0 to 4*64-1 := 0;
	signal config_mem_wren : std_logic := '0';

	-- shift register signals
	signal sr : std_logic_vector(38 downto 0);
	signal srval : std_logic_vector(38 downto 0);
	signal srcnt : integer range 0 to 39 := 0;
	signal srdone : std_logic;
	signal srload : std_logic;

	-- store chip IDs
	signal chip0id : std_logic_vector(3 downto 0) := "0111";
	signal chip1id : std_logic_vector(3 downto 0) := "0110";
	signal chip2id : std_logic_vector(3 downto 0) := "0000";
	signal chip3id : std_logic_vector(3 downto 0) := "0000";

	-- reconf enable
	signal chip0reconf : std_logic := '0';
	signal chip1reconf : std_logic := '0';
	signal chip2reconf : std_logic := '0';
	signal chip3reconf : std_logic := '0';
	signal globalreconf : std_logic := '0';

	-- manual trigger
	signal trigger : std_logic := '0';
	signal manual_trigger : std_logic := '0';
	signal manual_trigger_enable : std_logic := '0';
	signal ecr_trigger_enable : std_logic := '0';

	-- reconfiguration fsm
	type fsm_t is (st_wait_for_trigger, st_confmode, st_thresholdhigh, st_chipreconf, st_thresholdreal, st_runmode, st_busywait);
	signal Z : fsm_t := st_wait_for_trigger;
	signal Z_return : fsm_t := st_wait_for_trigger;
	signal curr_reg : std_logic_vector(5 downto 0) := "000000";
	signal curr_data : std_logic_vector(15 downto 0) := (others => '0');	
	signal curr_chip : std_logic_vector(1 downto 0) := "00";
	signal curr_chipid : std_logic_vector(3 downto 0) := "0000";
	signal curr_thresh : std_logic_vector(15 downto 0) := x"FFFF";
	signal gapcnt : integer range 0 to 15 := 15;

	-- address register
	signal addr_reg : std_logic_vector(7 downto 0);

	-- status bits
	signal collision : std_logic := '0';
	signal reconf_active_i : std_logic := '0';

	-- ECR detector
	signal ecr_detect : std_logic := '0';

	-- threshold
	signal chip0thresh : std_logic_vector(15 downto 0) := (others => '1');
	signal chip1thresh : std_logic_vector(15 downto 0) := (others => '1');
	signal chip2thresh : std_logic_vector(15 downto 0) := (others => '1');
	signal chip3thresh : std_logic_vector(15 downto 0) := (others => '1');
begin

-- decode current chip id
curr_chipid <= chip0id when curr_chip = "00" else
	           chip1id when curr_chip = "01" else
	           chip2id when curr_chip = "10" else
	           chip3id when curr_chip = "11" else
	           "0000";

-- decode current threshold value
curr_thresh <= chip0thresh when curr_chip = "00" else
			   chip1thresh when curr_chip = "01" else
			   chip2thresh when curr_chip = "10" else
			   chip3thresh when curr_chip = "11" else
			   x"FFFF";

-- detect ECR
ECRDetect_i: entity work.ECRDetect
	port map (
		clk => clk,
		reset => reset,
		xc => xc,
		ecr_detect => ecr_detect
	);

-- generate a global reconf enable
globalreconf <= chip0reconf or chip1reconf or chip2reconf or chip3reconf;

-- generate a manual trigger
manual_trigger <= control_wr(7) and control_wren;
trigger <= ((ecr_detect and ecr_trigger_enable) or (manual_trigger and manual_trigger_enable)) and globalreconf;

-- configuration memory
process begin
	wait until rising_edge(clk);

	-- write port
	if config_mem_wren = '1' then
		config_mem(config_mem_wrptr) <= config_mem_din;
	end if;

	-- catch threshold writes
	if config_mem_wren = '1' then
		if config_mem_wrptr = 20 then
			chip0thresh <= config_mem_din;
		elsif config_mem_wrptr = 84 then
			chip1thresh <= config_mem_din;
		elsif config_mem_wrptr = 148 then
			chip2thresh <= config_mem_din;
		elsif config_mem_wrptr = 212 then
			chip3thresh <= config_mem_din;
		end if;
	end if;

	-- read 1 port
	config_mem_dout1 <= config_mem(config_mem_rdptr1);
	config_mem_dout2 <= config_mem(config_mem_rdptr2);
end process;

-- generate memory pointers
config_mem_wrptr <= to_integer(unsigned(addr_reg));
config_mem_rdptr1 <= to_integer(unsigned(addr_reg));
config_mem_rdptr2 <= to_integer(unsigned(curr_chip) & unsigned(curr_reg));
curr_data <= config_mem_dout2;

-- shift register
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		srcnt <= 0;
		sr <= (others => '0');
	else
		if srcnt > 0 then
			sr <= sr(sr'left-1 downto 0) & '0';
			srcnt <= srcnt - 1;
		else
			if srload = '1' then
				sr <= srval;
				srcnt <= 39;
			end if;
		end if;
	end if;
end process;

-- generate shift done signal
srdone <= '1' when srcnt = 0 else '0';

-- output shift register bits to outer world
reconf_dout <= sr(sr'left);

-- reconfiguration state machine
process begin
	wait until rising_edge(clk);

	-- defaults
	srload <= '0';

	if reset = '1' then
		Z <= st_wait_for_trigger;
		Z_return <= st_wait_for_trigger;
		srval <= (others => '0');
		curr_reg <= "000000";
		curr_chip <= "00";
	else
		case Z is
			when st_wait_for_trigger =>
				if trigger = '1' then
					Z <= st_confmode;
				end if;

			when st_confmode =>
				srval <= CMD_CONFMODE;		-- broadcast confmode command to chipid 8
				srload <= '1';
				if srdone = '0' then
					Z <= st_busywait;

					curr_reg <= (others => '0');
					if chip0reconf = '1' then
						curr_chip <= "00";
						Z_return <= st_thresholdhigh;
					elsif chip1reconf = '1' then
						curr_chip <= "01";
						Z_return <= st_thresholdhigh;
					elsif chip2reconf = '1' then
						curr_chip <= "10";
						Z_return <= st_thresholdhigh;
					elsif chip3reconf = '1' then
						curr_chip <= "11";
						Z_return <= st_thresholdhigh;
					else
						Z_return <= st_runmode;
					end if;
				end if;

			when st_thresholdhigh =>
				srval <= CMD_WRREG & curr_chipid & "010100" & x"FFFF";			-- set the threshold (reg 20) to high value
				srload <= '1';
				if srdone = '0' then
					Z <= st_busywait;
					Z_return <= st_chipreconf;
				end if;

			when st_chipreconf =>
				srval <= CMD_WRREG & curr_chipid & curr_reg & curr_data;
				srload <= '1';
				if srdone = '0' then
					Z <= st_busywait;
					if curr_reg = "100011" then		-- are we finished with this chip?
						curr_reg <= "000000";
						Z_return <= st_thresholdreal;
					else
						-- skip register 20
						if curr_reg = "010011" then
							curr_reg <= std_logic_vector(unsigned(curr_reg) + 2);
						else
							curr_reg <= std_logic_vector(unsigned(curr_reg) + 1);
						end if;
						Z_return <= st_chipreconf;
					end if;
				end if;

			when st_thresholdreal =>
				srval <= CMD_WRREG & curr_chipid & "010100" & curr_thresh;			-- set threshold to real value
				srload <= '1';
				if srdone = '0' then
					Z <= st_busywait;

					-- select next chip depending on current
					if curr_chip = "00" then
						if chip1reconf = '1' then
							curr_chip <= "01";
							Z_return <= st_thresholdhigh;
						elsif chip2reconf = '1' then
							curr_chip <= "10";
							Z_return <= st_thresholdhigh;
						elsif chip3reconf = '1' then
							curr_chip <= "11";
							Z_return <= st_thresholdhigh;
						else
							Z_return <= st_runmode;
						end if;
					elsif curr_chip = "01" then
						if chip2reconf = '1' then
							curr_chip <= "10";
							Z_return <= st_thresholdhigh;
						elsif chip3reconf = '1' then
							curr_chip <= "11";
							Z_return <= st_thresholdhigh;
						else
							Z_return <= st_runmode;
						end if;
					elsif curr_chip = "10" then
						if chip3reconf = '1' then
							curr_chip <= "11";
							Z_return <= st_thresholdhigh;
						else
							Z_return <= st_runmode;
						end if;
					elsif curr_chip = "11" then
						Z_return <= st_runmode;
					end if;
				end if;

			when st_runmode =>
				srval <= CMD_RUNMODE;		-- broadcast runmode command to chipid 8
				srload <= '1';
				if srdone = '0' then
					Z <= st_busywait;
					Z_return <= st_wait_for_trigger;
				end if;

			when st_busywait =>
				if srdone = '1' then
					if gapcnt = 0 then
						gapcnt <= 15;
						Z <= Z_return;
					else
						gapcnt <= gapcnt - 1;
					end if;
				else
					gapcnt <= 15;
				end if;
		end case;
	end if;
end process;

-- generate a reconfiguration active
reconf_active_i <= '0' when Z = st_wait_for_trigger else '1';

-- register writing
process begin
	wait until rising_edge(clk);

	-- default
	config_mem_wren <= '0';

	if reset = '1' then
		addr_reg <= (others => '0');
		chip0reconf <= '0';
		chip1reconf <= '0';
		chip2reconf <= '0';
		chip3reconf <= '0';
		ecr_trigger_enable <= '0';
		manual_trigger_enable <= '0';
		chip0id <= "0111";		-- 7
		chip1id <= "0110";		-- 6
		chip2id <= "0000";		-- 0
		chip3id <= "0000";		-- 0
	else
		-- control register
		if control_wren = '1' then
			chip0reconf <= control_wr(0);
			chip1reconf <= control_wr(1);
			chip2reconf <= control_wr(2);
			chip3reconf <= control_wr(3);
			ecr_trigger_enable <= control_wr(4);
			manual_trigger_enable <= control_wr(5);
		end if;

		-- chipid
		if chipid_wren = '1' then
			case addr_reg(7 downto 6) is
				when "00" => chip0id <= chipid_wr(3 downto 0);
				when "01" => chip1id <= chipid_wr(3 downto 0);
				when "10" => chip2id <= chipid_wr(3 downto 0);
				when "11" => chip3id <= chipid_wr(3 downto 0);
				when others => null;
			end case;
		end if;

		-- address register
		if addr_wren = '1' then
			addr_reg <= addr_wr;
		end if;

		-- data
		if data_lsb_wren = '1' then
			config_mem_din(7 downto 0) <= data_lsb_wr;
		end if;
		if data_msb_wren = '1' then
			config_mem_din(15 downto 8) <= data_msb_wr;
			config_mem_wren <= '1';
		end if;

		-- increment address pointer on read/write
		if (config_mem_wren = '1') or (data_msb_rden = '1') then
			addr_reg <= std_logic_vector(unsigned(addr_reg) + 1);		-- auto increment write address
		end if;
	end if;
end process;

-- collision monitoring
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		collision <= '0';
	else
		if (reconf_active_i = '1') and (xc = '1') then
			collision <= '1';
		else
			if (status_wren = '1') and (status_wr(1) = '1') then
				collision <= '0';
			end if;
		end if;
	end if;
end process;
reconf_active <= reconf_active_i;

-- register reading
data_msb_rd <= config_mem_dout1(15 downto 8);
data_lsb_rd <= config_mem_dout1(7 downto 0);
addr_rd <= addr_reg;
control_rd <= "00" & manual_trigger_enable & ecr_trigger_enable & chip3reconf & chip2reconf & chip1reconf & chip0reconf;
chipid_rd <= "0000" & chip0id when addr_reg(7 downto 6) = "00" else
			 "0000" & chip1id when addr_reg(7 downto 6) = "01" else
			 "0000" & chip2id when addr_reg(7 downto 6) = "10" else
			 "0000" & chip3id;
status_rd <= "000000" & collision & reconf_active_i;

end architecture;
