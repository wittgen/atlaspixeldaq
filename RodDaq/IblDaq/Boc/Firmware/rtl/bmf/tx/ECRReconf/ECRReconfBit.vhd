library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ECRReconfBit is
	generic (
		BITSTREAM_BYTES : integer := 4096		-- 4 kB for a bitstream
	);
	port (
		-- clock and reset
		clk : in std_logic;
		reset : in std_logic;

		-- monitoring for ECR and collision
		xc : in std_logic;
		boc_serialport : in std_logic;

		-- data output (serial stream)
		reconf_dout : out std_logic;
		reconf_active : out std_logic;

		-- registers
		control_rd : out std_logic_vector(7 downto 0);
		control_wr : in std_logic_vector(7 downto 0);
		control_rden : in std_logic;
		control_wren : in std_logic;
		status_rd : out std_logic_vector(7 downto 0);
		status_wr : in std_logic_vector(7 downto 0);
		status_rden : in std_logic;
		status_wren : in std_logic;
		addr_lsb_rd : out std_logic_vector(7 downto 0);
		addr_lsb_wr : in std_logic_vector(7 downto 0);
		addr_lsb_rden : in std_logic;
		addr_lsb_wren : in std_logic;
		addr_msb_rd : out std_logic_vector(7 downto 0);
		addr_msb_wr : in std_logic_vector(7 downto 0);
		addr_msb_rden : in std_logic;
		addr_msb_wren : in std_logic;
		data_rd : out std_logic_vector(7 downto 0);
		data_wr : in std_logic_vector(7 downto 0);
		data_rden : in std_logic;
		data_wren : in std_logic
	);
end ECRReconfBit;

architecture rtl of ECRReconfBit is
	-- configuration memory
	type bitstream_mem_t is array (0 to BITSTREAM_BYTES-1) of std_logic_vector(7 downto 0);
	signal bitstream_mem : bitstream_mem_t := (others => (others => '0'));
	signal bitstream_mem_din : std_logic_vector(7 downto 0) := (others => '0');
	signal bitstream_mem_dout1 : std_logic_vector(7 downto 0) := (others => '0');
	signal bitstream_mem_dout2 : std_logic_vector(7 downto 0) := (others => '0');
	signal bitstream_mem_wrptr : integer range 0 to BITSTREAM_BYTES-1 := 0;
	signal bitstream_mem_rdptr1 : integer range 0 to BITSTREAM_BYTES-1 := 0;
	signal bitstream_mem_rdptr2 : integer range 0 to BITSTREAM_BYTES-1 := 0;
	signal bitstream_mem_wren : std_logic := '0';

	-- shift register signals
	signal sr : std_logic_vector(7 downto 0);
	signal srval : std_logic_vector(7 downto 0);
	signal srcnt : integer range 0 to 7 := 0;
	signal srdone : std_logic;
	signal srdone_reg : std_logic;
	signal srload : std_logic;

	-- manual trigger
	signal trigger : std_logic := '0';
	signal manual_trigger : std_logic := '0';
	signal manual_trigger_enable : std_logic := '0';
	signal ecr_trigger_enable : std_logic := '0';
	signal ecr_trigger_once_enable : std_logic := '0';
	signal qs_allow : std_logic := '0';

	-- input select for ECR detect
	signal serdata : std_logic := '0';
	signal ecr_input : std_logic := '0';

	-- reconfiguration fsm
	type fsm_t is (st_wait_for_trigger, st_play, st_finish);
	signal Z : fsm_t := st_wait_for_trigger;

	-- address register
	signal addr_reg : std_logic_vector(15 downto 0);
	signal curr_addr : std_logic_vector(15 downto 0);
	signal last_addr : std_logic_vector(15 downto 0);

	-- status bits
	signal collision : std_logic := '0';
	signal reconf_active_i : std_logic := '0';
	signal reconf_done : std_logic := '0';

	-- ECR detector
	signal ecr_detect : std_logic := '0';
begin

-- select input for ECR detection
serdata <= xc when ecr_input = '0' else boc_serialport;

-- detect ECR
ECRDetect_i: entity work.ECRDetect
	port map (
		clk => clk,
		reset => reset,
		xc => serdata,
		ecr_detect => ecr_detect
	);

-- generate a manual trigger
manual_trigger <= control_wr(7) and control_wren;
trigger <= qs_allow and ((ecr_detect and (ecr_trigger_enable or ecr_trigger_once_enable)) or (manual_trigger and manual_trigger_enable));

-- configuration memory
process begin
	wait until rising_edge(clk);

	-- write port
	if bitstream_mem_wren = '1' then
		bitstream_mem(bitstream_mem_wrptr) <= bitstream_mem_din;
	end if;

	-- read 1 port
	bitstream_mem_dout1 <= bitstream_mem(bitstream_mem_rdptr1);
	bitstream_mem_dout2 <= bitstream_mem(bitstream_mem_rdptr2);
end process;

-- generate memory pointers
bitstream_mem_wrptr <= to_integer(unsigned(addr_reg));
bitstream_mem_rdptr1 <= to_integer(unsigned(addr_reg));
bitstream_mem_rdptr2 <= to_integer(unsigned(curr_addr));

-- shift register
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		srcnt <= 0;
		sr <= (others => '0');
	else
		if srcnt > 0 then
			sr <= sr(sr'left-1 downto 0) & '0';
			srcnt <= srcnt - 1;
		else
			if srload = '1' then
				sr <= srval;
				srcnt <= 7;
			else
				sr <= (others => '0');
			end if;
		end if;
	end if;
end process;

-- generate shift done signal
srdone <= '1' when srcnt = 0 else '0';
srdone_reg <= srdone when rising_edge(clk);

-- output shift register bits to outer world
reconf_dout <= sr(sr'left);

-- take new value directly from memory
srval <= bitstream_mem_dout2;

-- reconfiguration state machine
process begin
	wait until rising_edge(clk);

	-- defaults
	srload <= '0';

	if reset = '1' then
		Z <= st_wait_for_trigger;
		curr_addr <= (others => '0');
	else
		case Z is
			when st_wait_for_trigger =>
				curr_addr <= (others => '0');
				srload <= '0';
				if trigger = '1' then
					Z <= st_play;
				end if;

			when st_play =>
				srload <= '1';
				if (srdone = '0') and (srdone_reg = '1') then
					if curr_addr = last_addr then
						Z <= st_finish;
						srload <= '0';
					else
						curr_addr <= std_logic_vector(unsigned(curr_addr) + 1);
					end if;
				end if;

			when st_finish =>
				curr_addr <= (others => '0');
				if srdone_reg = '1' then
					Z <= st_wait_for_trigger;
				end if;
		end case;
	end if;
end process;

-- generate a reconfiguration active
reconf_active_i <= '0' when Z = st_wait_for_trigger else '1';

-- register writing
process begin
	wait until rising_edge(clk);

	-- default
	bitstream_mem_wren <= '0';

	if reset = '1' then
		last_addr <= (others => '1');
		addr_reg <= (others => '0');
		ecr_trigger_enable <= '0';
		ecr_trigger_once_enable <= '0';
		manual_trigger_enable <= '0';
		qs_allow <= '0';
		ecr_input <= '0';
	else
		-- control register
		if control_wren = '1' then
			ecr_trigger_enable <= control_wr(0);
			manual_trigger_enable <= control_wr(1);
			ecr_trigger_once_enable <= control_wr(4);
			qs_allow <= control_wr(5);
			ecr_input <= control_wr(6);
			if (control_wr(2) = '1') then
				last_addr <= std_logic_vector(unsigned(addr_reg)-1);
			end if;
			if (control_wr(3) = '1') then
				addr_reg <= std_logic_vector(unsigned(last_addr)+1);
			end if;
		end if;

		-- address register
		if addr_lsb_wren = '1' then
			addr_reg(7 downto 0) <= addr_lsb_wr;
		end if;
		if addr_msb_wren = '1' then
			addr_reg(15 downto 8) <= addr_msb_wr;
		end if;

		-- data (can be written only when reconfiguration is inactive!)
		if (data_wren = '1') and (reconf_active_i = '0') then
			ecr_trigger_enable <= '0';
			ecr_trigger_once_enable <= '0';
			manual_trigger_enable <= '0';
			bitstream_mem_din <= data_wr;
			bitstream_mem_wren <= '1';
		end if;

		-- increment address pointer on read/write
		if (bitstream_mem_wren = '1') or (data_rden = '1') then
			addr_reg <= std_logic_vector(unsigned(addr_reg) + 1);		-- auto increment write address
		end if;

		-- reset trigger once on a ECR trigger
		if (ecr_detect = '1') and (qs_allow = '1') then
			ecr_trigger_once_enable <= '0';
		end if;
	end if;
end process;

-- collision monitoring
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		collision <= '0';
	else
		if (reconf_active_i = '1') and (serdata = '1') then
			collision <= '1';
		else
			if (status_wren = '1') and (status_wr(1) = '1') then
				collision <= '0';
			end if;
		end if;
	end if;
end process;
reconf_active <= reconf_active_i;

-- check reconfiguration done at least once
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		reconf_done <= '0';
	else
		if Z = st_finish then
			reconf_done <= '1';
		elsif (ecr_trigger_enable = '0') and (manual_trigger_enable = '0') and (ecr_trigger_once_enable = '0') then
			reconf_done <= '0';
		end if;
	end if;
end process;

-- register reading
data_rd <= bitstream_mem_dout1;
addr_lsb_rd <= addr_reg(7 downto 0);
addr_msb_rd <= addr_reg(15 downto 8);
control_rd <= "0" & ecr_input & qs_allow & ecr_trigger_once_enable & "00" & manual_trigger_enable & ecr_trigger_enable;
status_rd <= "00000" & reconf_done & collision & reconf_active_i;

end architecture;
