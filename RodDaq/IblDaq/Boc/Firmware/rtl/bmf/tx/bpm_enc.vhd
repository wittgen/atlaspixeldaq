-- simple BPM encoder
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bpm_enc is
	port (
			clk : in std_logic := '0';
			ce : in std_logic := '0';
			ce2 : in std_logic := '0';
			
			din : in std_logic := '0';
			dout : out std_logic := '0'
		);
end bpm_enc;

architecture Behavioral of bpm_enc is
	signal bpm_in : std_logic := '0';
	signal bpm_in_delayed : std_logic := '0';
	signal bpm_out : std_logic := '0';
begin

process begin
	wait until rising_edge(clk);
	
	if ce2 = '1' then
		if ce = '1' then
			bpm_out <= not bpm_out;		-- toggle at beginning of bit
			bpm_in_delayed <= bpm_in;	-- save input
		else
			if bpm_in_delayed = '1' then
				bpm_out <= not bpm_out;	-- if din was 1 toggle in the middle
			end if;
		end if;
	end if;
end process;

-- input and output
bpm_in <= din;
dout <= bpm_out;

end Behavioral;

