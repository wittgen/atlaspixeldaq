-- BMF main unit
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.all;
use work.BuildVersion.all;

library UNISIM;
use UNISIM.VComponents.all;

entity bmf_main is
	port
	(
		-- clocking and reset
		clk_regbank : in std_logic;
		clk_tx : in std_logic;
		clk_tx_fast : in std_logic;
		clk_tx_fast_strobe : in std_logic;
		clk_rx : in std_logic;
		clk_rx_fast : in std_logic;
		clk_rx_fast_strobe : in std_logic;
		clk_backplane : in std_logic;
		clk_slink : in std_logic;
		clk_backplane_slink0 : in std_logic;
		clk_backplane_slink1 : in std_logic;
		reset : in std_logic;

		-- wishbone interface
		wb_dat_i : in std_logic_vector(7 downto 0);
		wb_dat_o : out std_logic_vector(7 downto 0);
		wb_adr : in std_logic_vector(15 downto 0);
		wb_cyc : in std_logic;
		wb_stb : in std_logic;
		wb_we : in std_logic;
		wb_ack : out std_logic;

		-- serial data from ROD
		xc : in std_logic_vector(15 downto 0);

		-- receiver
		optical_in : in std_logic_vector(23 downto 0);

		-- transmitter
		txout : out std_logic_vector(15 downto 0);

		-- SLINK
		slink0_uclk : in std_logic;
		slink0_udata : in std_logic_vector(31 downto 0);
		slink0_uctrl : in std_logic;
		slink0_uwe : in std_logic;
		slink0_ureset : in std_logic;
		slink0_utest : in std_logic;
		slink0_lff : out std_logic;
		slink0_ldown : out std_logic;
		slink1_uclk : in std_logic;
		slink1_udata : in std_logic_vector(31 downto 0);
		slink1_uctrl : in std_logic;
		slink1_uwe : in std_logic;
		slink1_ureset : in std_logic;
		slink1_utest : in std_logic;
		slink1_lff : out std_logic;
		slink1_ldown : out std_logic;

		-- QSFP
		qsfp_rx_p : in std_logic_vector(3 downto 0);
		qsfp_rx_n : in std_logic_vector(3 downto 0);
		qsfp_tx_p : out std_logic_vector(3 downto 0);
		qsfp_tx_n : out std_logic_vector(3 downto 0);
		qsfp_scl : inout std_logic;
		qsfp_sda : inout std_logic;
		qsfp_modsel_n : out std_logic;
		qsfp_lowpwr_n : out std_logic;
		qsfp_reset_n : out std_logic;
		qsfp_int_n : in std_logic;
		qsfp_present_n : in std_logic;

		-- SLINK LEDs
		slink_led : out std_logic_vector(4 downto 0);

		-- lock loss counter
		lockloss_cnt : in std_logic_vector(7 downto 0);

		-- I2C connections to RX plugins
		rx_scl : inout std_logic_vector(1 downto 0);
		rx_sda : inout std_logic_vector(1 downto 0);

		-- backplane data
		rxdata_boc2rod : out std_logic_vector(47 downto 0)
	);
end entity bmf_main;

architecture rtl of bmf_main is
	component bmf_slave_mux
		port
		(
			clk : in std_logic;
			reset : in std_logic;

			wb_dat_i : in std_logic_vector(7 downto 0);
			wb_dat_o : out std_logic_vector(7 downto 0);
			wb_adr : in std_logic_vector(15 downto 0);
			wb_cyc : in std_logic;
			wb_stb : in std_logic;
			wb_we : in std_logic;
			wb_ack : out std_logic;

			rx_regs_wr : out RegBank_wr_t(0 to 1023);
			rx_regs_rd : in RegBank_rd_t(0 to 1023);
			rx_regs_wren : out RegBank_wren_t(0 to 1023);
			rx_regs_rden : out RegBank_rden_t(0 to 1023);

			tx_regs_wr : out RegBank_wr_t(0 to 1023);
			tx_regs_rd : in RegBank_rd_t(0 to 1023);
			tx_regs_wren : out RegBank_wren_t(0 to 1023);
			tx_regs_rden : out RegBank_rden_t(0 to 1023);

			com_regs_wr : out RegBank_wr_t(0 to 255);
			com_regs_rd : in RegBank_rd_t(0 to 255);
			com_regs_wren : out RegBank_wren_t(0 to 255);
			com_regs_rden : out RegBank_rden_t(0 to 255)
		);
	end component;

	component bmf_tx
		generic
		(
			DEFAULT_XC_LINK : integer range 0 to 15;
			INCLUDE_ECR_RECONF : boolean := false
		);
		port
		(
			-- clocking
			clk_tx : in std_logic := '0';
			clk_tx_fast : in std_logic := '0';
			clk_tx_fast_strobe : in std_logic := '0';
	
			-- reset
			reset : in std_logic := '0';
			soft_reset : in std_logic := '0';
		
			-- input from ROD
			xc_vec : in std_logic_vector(15 downto 0);

			-- output
			txout : out std_logic := '0';

			-- emulator
			txemudata : out std_logic := '0';

			-- trigger on command
			cmd_trigger : out std_logic := '0';

			-- registers
			reg_rd : out RegBank_rd_t(0 to 31);
			reg_wr : in RegBank_wr_t(0 to 31);
			reg_wren : in RegBank_wren_t(0 to 31);
			reg_rden : in RegBank_rden_t(0 to 31);

			-- broadcast
			bcast_wr : in RegBank_wr_t(0 to 31);
			bcast_wren : in RegBank_wren_t(0 to 31);
			bcast_enable : in std_logic;

			-- register protection
			secure_reg : in std_logic
		);
	end component;

	component iblrx
		port (
			-- clock inputs
			clk_serial : in std_logic;
			clk_serial4x : in std_logic;
			clk_serial4x_serdesstrobe : in std_logic;
			clk_backplane : in std_logic;
			clk_regbank : in std_logic;

			-- reset input
			reset : in std_logic;

			-- data inputs
			din_optical : in std_logic_vector(23 downto 0);
			din_loopback : in std_logic_vector(15 downto 0);
			din_feemu : in std_logic_vector(15 downto 0);

			-- data outputs to the backplane
			dout : out std_logic_vector(47 downto 0);

			-- register interface
			rxregs_wr : in RegBank_wr_t(0 to 1023);
			rxregs_rd : out RegBank_rd_t(0 to 1023);
			rxregs_wren : in RegBank_wren_t(0 to 1023);
			rxregs_rden : in RegBank_rden_t(0 to 1023);
			allrx_ctrl : in std_logic_vector(7 downto 0);
			allrx_ctrl_wren : in std_logic;

			-- rx debugging
			rxdebug_select : in std_logic_vector(3 downto 0);
			rxdebug_slow : out std_logic_vector(24 downto 0);
			rxdebug_fast : out std_logic_vector(50 downto 0);

			-- rx multiplexer selection
			rxmux_wr : in RegBank_wr_t(0 to 7);
			rxmux_rd : in RegBank_rd_t(0 to 7);
			rxmux_wren : in RegBank_wren_t(0 to 7);
			rxmux_rden : in RegBank_rden_t(0 to 7);

			-- CDR controller registers
			cdr_wr : in RegBank_wr_t(0 to 47);
			cdr_rd : out RegBank_rd_t(0 to 47);
			cdr_wren : in RegBank_wren_t(0 to 47);
			cdr_rden : in RegBank_rden_t(0 to 47);

			-- BERT bypass
			bert_bypass : out std_logic_vector(15 downto 0);
			bert_bypass_valid : out std_logic_vector(15 downto 0)
		);
	end component;

	component pixrx
		port (
			-- clock inputs
			clk_serial : in std_logic;
			clk_serial8x : in std_logic;
			clk_serial8x_serdesstrobe : in std_logic;
			clk_backplane : in std_logic;
			clk_regbank : in std_logic;

			-- reset input
			reset : in std_logic;

			-- data inputs
			din_optical : in std_logic_vector(23 downto 0);

			-- data outputs to the backplane
			dout : out std_logic_vector(47 downto 0);

			-- XC serial data for emulator
			xc_vec : in std_logic_vector(15 downto 0);

			-- register interface
			rxregs_wr : in RegBank_wr_t(0 to 1023);
			rxregs_rd : out RegBank_rd_t(0 to 1023);
			rxregs_wren : in RegBank_wren_t(0 to 1023);
			rxregs_rden : in RegBank_rden_t(0 to 1023);

			-- phase registers
			phase_wr : IN RegBank_wr_t(0 to 47);
			phase_rd : OUT RegBank_rd_t(0 to 47);
			phase_wren : IN RegBank_wren_t(0 to 47);
			phase_rden : IN RegBank_rden_t(0 to 47);

			-- speed register
			speed_wr : in std_logic_vector(7 downto 0);
			speed_rd : out std_logic_vector(7 downto 0);
			speed_wren : in std_logic;
			speed_rden : in std_logic;

			-- broadcast enable
			bcast_enable : in std_logic_vector(15 downto 0);

			-- rx debugging
			rxdebug_select : in std_logic_vector(3 downto 0);
			rxdebug_slow : out std_logic_vector(24 downto 0);
			rxdebug_fast : out std_logic_vector(50 downto 0)
		);
	end component;

	component BuildDate
		port (
			year : out std_logic_vector(7 downto 0) := x"00";
			month : out std_logic_vector(7 downto 0) := x"00";
			day : out std_logic_vector(7 downto 0) := x"00";
			hour : out std_logic_vector(7 downto 0) := x"00";
			minute : out std_logic_vector(7 downto 0) := x"00";
			second : out std_logic_vector(7 downto 0) := x"00";
			git_hash : out std_logic_vector(159 downto 0) := (others => '0')
		);
	end component;

	component fei4_spy
		port (
			clk : in std_logic;
			reset : in std_logic;

			din : in std_logic_vector(15 downto 0);

			csr_wr : in std_logic_vector(7 downto 0);
			csr_rd : out std_logic_vector(7 downto 0);
			csr_wren : in std_logic;
			csr_rden : in std_logic;
			fifo0_wr : in std_logic_vector(7 downto 0);
			fifo0_rd : out std_logic_vector(7 downto 0);
			fifo0_wren : in std_logic;
			fifo0_rden : in std_logic;
			fifo1_wr : in std_logic_vector(7 downto 0);
			fifo1_rd : out std_logic_vector(7 downto 0);
			fifo1_wren : in std_logic;
			fifo1_rden : in std_logic;
			fifo2_wr : in std_logic_vector(7 downto 0);
			fifo2_rd : out std_logic_vector(7 downto 0);
			fifo2_wren : in std_logic;
			fifo2_rden : in std_logic;
			fifo3_wr : in std_logic_vector(7 downto 0);
			fifo3_rd : out std_logic_vector(7 downto 0);
			fifo3_wren : in std_logic;
			fifo3_rden : in std_logic;
			trigcnt0_wr : in std_logic_vector(7 downto 0);
			trigcnt0_rd : out std_logic_vector(7 downto 0);
			trigcnt0_rden : in std_logic;
			trigcnt0_wren : in std_logic;
			trigcnt1_wr : in std_logic_vector(7 downto 0);
			trigcnt1_rd : out std_logic_vector(7 downto 0);
			trigcnt1_rden : in std_logic;
			trigcnt1_wren : in std_logic;
			trigcnt2_wr : in std_logic_vector(7 downto 0);
			trigcnt2_rd : out std_logic_vector(7 downto 0);
			trigcnt2_rden : in std_logic;
			trigcnt2_wren : in std_logic;
			trigcnt3_wr : in std_logic_vector(7 downto 0);
			trigcnt3_rd : out std_logic_vector(7 downto 0);
			trigcnt3_rden : in std_logic;
			trigcnt3_wren : in std_logic;
			calcnt0_wr : in std_logic_vector(7 downto 0);
			calcnt0_rd : out std_logic_vector(7 downto 0);
			calcnt0_rden : in std_logic;
			calcnt0_wren : in std_logic;
			calcnt1_wr : in std_logic_vector(7 downto 0);
			calcnt1_rd : out std_logic_vector(7 downto 0);
			calcnt1_rden : in std_logic;
			calcnt1_wren : in std_logic;
			calcnt2_wr : in std_logic_vector(7 downto 0);
			calcnt2_rd : out std_logic_vector(7 downto 0);
			calcnt2_rden : in std_logic;
			calcnt2_wren : in std_logic;
			calcnt3_wr : in std_logic_vector(7 downto 0);
			calcnt3_rd : out std_logic_vector(7 downto 0);
			calcnt3_rden : in std_logic;
			calcnt3_wren : in std_logic
		);
	end component;

	component qsfp_ctrl
		generic (
			f_clk : integer := 40000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			-- lines to/from the qsfp
			qsfp_scl : inout std_logic;
			qsfp_sda : inout std_logic;
			qsfp_modsel_n : out std_logic;
			qsfp_lowpwr_n : out std_logic;
			qsfp_reset_n : out std_logic;
			qsfp_int_n : in std_logic;
			qsfp_present_n : in std_logic;

			-- access to register bank
			csr_wr : in std_logic_vector(7 downto 0);
			csr_rd : out std_logic_vector(7 downto 0);
			csr_wren : in std_logic;
			csr_rden : in std_logic;
			addr_wr : in std_logic_vector(7 downto 0);
			addr_rd : out std_logic_vector(7 downto 0);
			addr_wren : in std_logic;
			addr_rden : in std_logic;
			data_wr : in std_logic_vector(7 downto 0);
			data_rd : out std_logic_vector(7 downto 0);
			data_wren : in std_logic;
			data_rden : in std_logic
		);
	end component;

	component UptimeCounter
		generic (
			f_clk : integer := 40000000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			uptime_rd : out std_logic_vector(7 downto 0);
			uptime_wr : in std_logic_vector(7 downto 0);
			uptime_rden : in std_logic;
			uptime_wren : in std_logic
		);
	end component;

	component RxPluginCtrl
		generic (
			f_clk : integer := 40000000;
			f_scl : integer := 100000;
			dac0_addr : std_logic_vector(6 downto 0) := "0110010";		-- 0x32
			dac1_addr : std_logic_vector(6 downto 0) := "1000000";		-- 0x40
			dac2_addr : std_logic_vector(6 downto 0) := "1000001";		-- 0x41
			dac3_addr : std_logic_vector(6 downto 0) := "0010000"		-- 0x10	(RGS DAC)
		);
		port (
			-- clocking and reset
			clk : in std_logic;
			reset : in std_logic;

			-- I2C interface
			i2c_scl : inout std_logic;
			i2c_sda : inout std_logic;

			-- register interface
			csr_rd : out std_logic_vector(7 downto 0);
			csr_wr : in std_logic_vector(7 downto 0);
			csr_rden : in std_logic;
			csr_wren : in std_logic;
			data_lsb_rd : out std_logic_vector(7 downto 0);
			data_lsb_wr : in std_logic_vector(7 downto 0);
			data_lsb_rden : in std_logic;
			data_lsb_wren : in std_logic;
			data_msb_rd : out std_logic_vector(7 downto 0);
			data_msb_wr : in std_logic_vector(7 downto 0);
			data_msb_rden : in std_logic;
			data_msb_wren : in std_logic		
		);
	end component;	

	component MGT_USRCLK_SOURCE 
	generic
	(
		 FREQUENCY_MODE   : string   := "LOW";
		 DIVIDE_2         : boolean  := false;    
		 FEEDBACK         : string   := "1X";
		 DIVIDE           : real     := 2.0    
	);
	port
	(
		 DIV1_OUT                : out std_logic;
		 DIV2_OUT                : out std_logic;
		 CLK2X_OUT               : out std_logic;
		 DCM_LOCKED_OUT          : out std_logic;
		 CLK_IN                  : in  std_logic;
		 FB_IN                   : in  std_logic;
		 DCM_RESET_IN            : in  std_logic

	);
	end component;

	component MGT_USRCLK_SOURCE_v2
	port
	(
	    DIV1_OUT                : out std_logic;
	    DIV2_OUT                : out std_logic;
	    DIV4_OUT                : out std_logic;
	    CLK2X_OUT               : out std_logic;
	    DCM_LOCKED_OUT          : out std_logic;
	    CLK_IN                  : in  std_logic;
	    DCM_RESET_IN            : in  std_logic
	);
	end component;

	COMPONENT hola_lsc_s6
		GENERIC (simulation: integer);
		PORT(
			mgtrefclk : IN std_logic;
			sys_rst : IN std_logic;
			mgt_rst : IN std_logic;
			usr_clk_in : IN std_logic;
			usr_clk20_in : IN std_logic;
			usr_clk40_in : IN std_logic;
			ud : IN std_logic_vector(31 downto 0);
			ureset_n : IN std_logic;
			utest_n : IN std_logic;
			uctrl_n : IN std_logic;
			uwen_n : IN std_logic;
			uclk : IN std_logic;
			hola_enable_ldown_0 : IN std_logic;
			hola_enable_lff_0 : IN std_logic;
			hola_enable_ldown_1 : IN std_logic;
			hola_enable_lff_1 : IN std_logic;
			count_clk : IN std_logic;
			count_hola0_lff : OUT std_logic_vector(15 downto 0);
			count_hola0_ldown : OUT std_logic_vector(15 downto 0);
			count_hola1_lff : OUT std_logic_vector(15 downto 0);
			count_hola1_ldown : OUT std_logic_vector(15 downto 0);
			count_reset : IN std_logic;
			tlk_sin0_p : IN std_logic;
			tlk_sin0_n : IN std_logic;
			tlk_sin1_p : IN std_logic;
			tlk_sin1_n : IN std_logic;          
			gtpclkout : OUT std_logic;
			gtp_plldet_out : OUT std_logic;
			lff_n : OUT std_logic;
			lrl0 : OUT std_logic_vector(3 downto 0);
			lrl1 : OUT std_logic_vector(3 downto 0);
			ldown_n : OUT std_logic;
			tlk_sout0_p : OUT std_logic;
			tlk_sout0_n : OUT std_logic;
			tlk_sout1_p : OUT std_logic;
			tlk_sout1_n : OUT std_logic;
			testled_n : OUT std_logic;
			lderrled_n : OUT std_logic;
			lupled_n : OUT std_logic;
			flowctlled_n : OUT std_logic;
			activityled_n : OUT std_logic;
			usr_clk_out : OUT std_logic;
			gtx_reset_done0 : OUT std_logic;
			gtx_reset_done1 : OUT std_logic;
			dbg_txclk0 : OUT std_logic;
			dbg_txd0 : OUT std_logic_vector(15 downto 0);
			dbg_tx_er0 : OUT std_logic;
			dbg_tx_en0 : OUT std_logic;
			dbg_rxclk0 : OUT std_logic;
			dbg_cc0 : OUT std_logic_vector(2 downto 0);
			dbg_rxd0 : OUT std_logic_vector(15 downto 0);
			dbg_rx_er0 : OUT std_logic;
			dbg_rx_dv0 : OUT std_logic;
			dbg_tlkgtx0 : OUT std_logic_vector(57 downto 0);
			dbg_txclk1 : OUT std_logic;
			dbg_txd1 : OUT std_logic_vector(15 downto 0);
			dbg_tx_er1 : OUT std_logic;
			dbg_tx_en1 : OUT std_logic;
			dbg_rxclk1 : OUT std_logic;
			dbg_cc1 : OUT std_logic_vector(2 downto 0);
			dbg_rxd1 : OUT std_logic_vector(15 downto 0);
			dbg_rx_er1 : OUT std_logic;
			dbg_rx_dv1 : OUT std_logic;
			dbg_tlkgtx1 : OUT std_logic_vector(57 downto 0);
			-- AKU additional debugging
			dbg_rxrecclk0 : OUT std_logic;
			dbg_rxlossofsync0 : OUT std_logic_vector(1 downto 0);
			dbg_rxreset0 : IN std_logic;
			dbg_rxcdrreset0 : IN std_logic;
			dbg_rxbufreset0 : IN std_logic;
			dbg_rxrecclk1 : OUT std_logic;
			dbg_rxlossofsync1 : OUT std_logic_vector(1 downto 0);
			dbg_rxreset1 : IN std_logic;
			dbg_rxcdrreset1 : IN std_logic;
			dbg_rxbufreset1 : IN std_logic;

			-- FIFO
			dbg_fifo_q0 : OUT STD_LOGIC_VECTOR(33 downto 0);
			dbg_fifo_rden0 : OUT STD_LOGIC;
			dbg_fifo_empty0 : OUT STD_LOGIC;
			dbg_fifo_clk0 : OUT STD_LOGIC;
			dbg_fifo_q1 : OUT STD_LOGIC_VECTOR(33 downto 0);
			dbg_fifo_rden1 : OUT STD_LOGIC;
			dbg_fifo_empty1 : OUT STD_LOGIC;
			dbg_fifo_clk1 : OUT STD_LOGIC
		);
	end component;

	component SlinkChecker
		port (
			clk : in std_logic;
			reset : in std_logic;

			udata : in std_logic_vector(31 downto 0);
			uctrl : in std_logic;
			uwe : in std_logic;

			error : out std_logic;
			error_source : out std_logic_vector(2 downto 0)
		);
	end component;

	COMPONENT lscDataGen
	PORT(
	    lscData : OUT  std_logic_vector(31 downto 0);
	    lscFull_n : IN  std_logic;
	    lscEnable : IN  std_logic;
	    lscWen_n : OUT  std_logic;
	    lscCtrl_n : OUT  std_logic;
	    lscTest_n : out std_logic;
	    lscReset_n : out std_logic;
	    clk : IN  std_logic;
	    reset : IN  std_logic
	    );
	end component;

	component SlinkTester
		port (
			-- clock and reset
			clk : in std_logic;
			reset : in std_logic;

			-- control input
			enable : in std_logic;

			-- slink inputs
			slink_udata : in std_logic_vector(31 downto 0);
			slink_uctrl : in std_logic;
			slink_uwe : in std_logic;

			-- error flag
			slink_error : out std_logic
		);
	end component;

	component fei4_emulator
		generic (
			DEFAULT_CHIPID : integer := 0;
			DEFAULT_DINSEL : integer := 0
		);
		port (
			-- clocking and reset
			clk40 : in std_logic;
			clk160 : in std_logic;
			reset : in std_logic;

			-- serial data input (synchronous with clk40)
			din : in std_logic_vector(15 downto 0);

			-- serial data output (synchronous with clk160)
			dout : out std_logic;

			-- register interface to BMF register bank
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			status_rd : out std_logic_vector(7 downto 0);
			status_wr : in std_logic_vector(7 downto 0);
			status_rden : in std_logic;
			status_wren : in std_logic;
			extra_frames_rd : out std_logic_vector(7 downto 0);
			extra_frames_wr : in std_logic_vector(7 downto 0);
			extra_frames_rden : in std_logic;
			extra_frames_wren : in std_logic;
			control2_rd : out std_logic_vector(7 downto 0);
			control2_wr : in std_logic_vector(7 downto 0);
			control2_rden : in std_logic;
			control2_wren : in std_logic;
			hit_wr : in std_logic_vector(7 downto 0);
			hit_rd : out std_logic_vector(7 downto 0);
			hit_wren : in std_logic;
			hit_rden : in std_logic;
			hitcount_wr : in std_logic_vector(7 downto 0);
			hitcount_rd : out std_logic_vector(7 downto 0);
			hitcount_wren : in std_logic;
			hitcount_rden : in std_logic
	  	);
	end component;

	-- chipscope controller
	component chipscope_icon
		port(
			CONTROL0 : inout std_logic_vector(35 downto 0);
			CONTROL1 : inout std_logic_vector(35 downto 0);
			CONTROL2 : inout std_logic_vector(35 downto 0);
			CONTROL3 : inout std_logic_vector(35 downto 0);
			CONTROL4 : inout std_logic_vector(35 downto 0);
			CONTROL5 : inout std_logic_vector(35 downto 0);
			CONTROL6 : inout std_logic_vector(35 downto 0)
		);
	end component;

	component chipscope_icon_with_vio
		port(
			CONTROL0 : inout std_logic_vector(35 downto 0);
			CONTROL1 : inout std_logic_vector(35 downto 0);
			CONTROL2 : inout std_logic_vector(35 downto 0);
			CONTROL3 : inout std_logic_vector(35 downto 0);
			CONTROL4 : inout std_logic_vector(35 downto 0);
			CONTROL5 : inout std_logic_vector(35 downto 0);
			CONTROL6 : inout std_logic_vector(35 downto 0);
			CONTROL7 : inout std_logic_vector(35 downto 0);
			CONTROL8 : inout std_logic_vector(35 downto 0);
			CONTROL9 : inout std_logic_vector(35 downto 0)
		);
	end component;

	-- chipscope for XC
	component chipscope_ila_xc
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(31 downto 0)
		);
	end component;

	-- chipscope for ROD2BOC
	component chipscope_ila_rod2boc
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(47 downto 0)
		);
	end component;

	-- chipscope for SLINK
	component chipscope_ila_slink
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(42 downto 0)
		);
	end component;
	
	component chipscope_ila_slink_ll
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(209 downto 0)
		);
	end component;

	component chipscope_ila_slink_fifo
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(71 downto 0)
		);
	end component;


	-- chipscope for Wishbone
	component chipscope_ila_wishbone
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(35 downto 0)
		);
	end component;

	-- chipscope for rxdebug
	component rxdebug_fast
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(50 downto 0)
		);
	end component;
	component rxdebug_slow
		port (
			CONTROL : inout std_logic_vector(35 downto 0);
			CLK : in std_logic;
			TRIG0 : in std_logic_vector(24 downto 0)
		);
	end component;
	-- chip slink reset controller
	component chipscope_vio_slink_ll
	PORT (
	CONTROL : INOUT STD_LOGIC_VECTOR(35 DOWNTO 0);
	ASYNC_OUT : OUT STD_LOGIC_VECTOR(11 DOWNTO 0));
	end component;

	-- chipscope control signals
	signal cs_control0 : std_logic_vector(35 downto 0);
	signal cs_control1 : std_logic_vector(35 downto 0);
	signal cs_control2 : std_logic_vector(35 downto 0);
	signal cs_control3 : std_logic_vector(35 downto 0);
	signal cs_control4 : std_logic_vector(35 downto 0);
	signal cs_control5 : std_logic_vector(35 downto 0);
	signal cs_control6 : std_logic_vector(35 downto 0);
	signal cs_control7 : std_logic_vector(35 downto 0);
	signal cs_control8 : std_logic_vector(35 downto 0);
	signal cs_control9 : std_logic_vector(35 downto 0);

	-- debug signals from RX
	signal rxdebug_select : std_logic_vector(3 downto 0) := "0000";
	signal rxdebug_fast_select : std_logic_vector(50 downto 0);
	signal rxdebug_slow_select : std_logic_vector(24 downto 0);

	-- slow level slink debugging
	signal slink0_lowlevel_trig : std_logic_vector(209 downto 0);
	signal slink0_dbg_txclk0 : std_logic;
	signal slink0_dbg_txd0 : std_logic_vector(15 downto 0);
	signal slink0_dbg_tx_er0 : std_logic;
	signal slink0_dbg_tx_en0 : std_logic;
	signal slink0_dbg_rxclk0 : std_logic;
	signal slink0_dbg_cc0 : std_logic_vector(2 downto 0);
	signal slink0_dbg_rxd0 : std_logic_vector(15 downto 0);
	signal slink0_dbg_rx_er0 : std_logic;
	signal slink0_dbg_rx_dv0 : std_logic;
	signal slink0_dbg_tlkgtx0 : std_logic_vector(57 downto 0);
	signal slink0_dbg_txclk1 : std_logic;
	signal slink0_dbg_txd1 : std_logic_vector(15 downto 0);
	signal slink0_dbg_tx_er1 : std_logic;
	signal slink0_dbg_tx_en1 : std_logic;
	signal slink0_dbg_rxclk1 : std_logic;
	signal slink0_dbg_cc1 : std_logic_vector(2 downto 0);
	signal slink0_dbg_rxd1 : std_logic_vector(15 downto 0);
	signal slink0_dbg_rx_er1 : std_logic;
	signal slink0_dbg_rx_dv1 : std_logic;
	signal slink0_dbg_tlkgtx1 : std_logic_vector(57 downto 0);
	signal slink0_dbg_rxlos0 : std_logic_vector(1 downto 0);
	signal slink0_dbg_rxlos1 : std_logic_vector(1 downto 0);

	signal slink0_dbg_fifo_q0 : std_logic_vector(33 downto 0);
	signal slink0_dbg_fifo_empty0 : std_logic;
	signal slink0_dbg_fifo_rden0 : std_logic;
	signal slink0_dbg_fifo_clk0 : std_logic;
	signal slink0_dbg_fifo_q1 : std_logic_vector(33 downto 0);
	signal slink0_dbg_fifo_empty1 : std_logic;
	signal slink0_dbg_fifo_rden1 : std_logic;
	signal slink0_dbg_fifo_clk1 : std_logic;


	-- vio controllable signals. don't forget to make 0 default
	-- the vio port is shared for both slinks
	signal slink_common_vio_out : STD_LOGIC_VECTOR(11 DOWNTO 0);
	signal slink0_dbg_rxcdrreset0 : std_logic := '0';
	signal slink0_dbg_rxcdrreset1 : std_logic := '0';
	signal slink0_dbg_rxbufreset0 : std_logic := '0';
	signal slink0_dbg_rxbufreset1 : std_logic := '0';
	signal slink0_dbg_rxreset0 : std_logic := '0';
	signal slink0_dbg_rxreset1 : std_logic := '0';

	signal slink1_lowlevel_trig : std_logic_vector(209 downto 0);
	signal slink1_dbg_txclk0 : std_logic;
	signal slink1_dbg_txd0 : std_logic_vector(15 downto 0);
	signal slink1_dbg_tx_er0 : std_logic;
	signal slink1_dbg_tx_en0 : std_logic;
	signal slink1_dbg_rxclk0 : std_logic;
	signal slink1_dbg_cc0 : std_logic_vector(2 downto 0);
	signal slink1_dbg_rxd0 : std_logic_vector(15 downto 0);
	signal slink1_dbg_rx_er0 : std_logic;
	signal slink1_dbg_rx_dv0 : std_logic;
	signal slink1_dbg_tlkgtx0 : std_logic_vector(57 downto 0);
	signal slink1_dbg_txclk1 : std_logic;
	signal slink1_dbg_txd1 : std_logic_vector(15 downto 0);
	signal slink1_dbg_tx_er1 : std_logic;
	signal slink1_dbg_tx_en1 : std_logic;
	signal slink1_dbg_rxclk1 : std_logic;
	signal slink1_dbg_cc1 : std_logic_vector(2 downto 0);
	signal slink1_dbg_rxd1 : std_logic_vector(15 downto 0);
	signal slink1_dbg_rx_er1 : std_logic;
	signal slink1_dbg_rx_dv1 : std_logic;
	signal slink1_dbg_tlkgtx1 : std_logic_vector(57 downto 0);
	signal slink1_dbg_rxlos0 : std_logic_vector(1 downto 0);
	signal slink1_dbg_rxlos1 : std_logic_vector(1 downto 0);

	signal slink1_dbg_fifo_q0 : std_logic_vector(33 downto 0);
	signal slink1_dbg_fifo_empty0 : std_logic;
	signal slink1_dbg_fifo_rden0 : std_logic;
	signal slink1_dbg_fifo_clk0 : std_logic;
	signal slink1_dbg_fifo_q1 : std_logic_vector(33 downto 0);
	signal slink1_dbg_fifo_empty1 : std_logic;
	signal slink1_dbg_fifo_rden1 : std_logic;
	signal slink1_dbg_fifo_clk1 : std_logic;

	-- vio controllable signals. don't forget to make 0 default
	signal slink1_dbg_rxcdrreset0 : std_logic := '0';
	signal slink1_dbg_rxcdrreset1 : std_logic := '0';
	signal slink1_dbg_rxbufreset0 : std_logic := '0';
	signal slink1_dbg_rxbufreset1 : std_logic := '0';
	signal slink1_dbg_rxreset0 : std_logic := '0';
	signal slink1_dbg_rxreset1 : std_logic := '0';

	-- ORed reset
	signal or_reset : std_logic;

	
	-- register banks
	signal rx_regs_wr : RegBank_wr_t(0 to 1023) := (others => (others => '0'));
	signal rx_regs_rd : RegBank_rd_t(0 to 1023) := (others => (others => '0'));
	signal rx_regs_wren : RegBank_wren_t(0 to 1023) := (others => '0');
	signal rx_regs_rden : RegBank_rden_t(0 to 1023) := (others => '0');

	signal tx_regs_wr : RegBank_wr_t(0 to 1023) := (others => (others => '0'));
	signal tx_regs_rd : RegBank_rd_t(0 to 1023) := (others => (others => '0'));
	signal tx_regs_wren : RegBank_wren_t(0 to 1023) := (others => '0');
	signal tx_regs_rden : RegBank_rden_t(0 to 1023) := (others => '0');

	signal com_regs_wr : RegBank_wr_t(0 to 255) := (others => (others => '0'));
	signal com_regs_rd : RegBank_rd_t(0 to 255) := (others => (others => '0'));
	signal com_regs_wren : RegBank_wren_t(0 to 255) := (others => '0');
	signal com_regs_rden : RegBank_rden_t(0 to 255) := (others => '0');

	-- qsfp control register
	signal qsfp_control : std_logic_vector(7 downto 0) := "00000111";

	-- chipscope monitoring of serial lines
	signal txcs_select : std_logic := '0';
	signal txcs_data : std_logic_vector(15 downto 0) := (others => '0');

	-- internal signals for transmitter
	signal txout_int : std_logic_vector(15 downto 0) := (others => '0');
	signal txemu_data : std_logic_vector(15 downto 0) := (others => '0');
	signal tx_cmdtrigger : std_logic_vector(15 downto 0) := (others => '0');

	-- fe emulator signals
	signal fei4_dout : std_logic_vector(15 downto 0) := (others => '0');

	-- all tx/rx control
	signal alltx_ctrl : std_logic_vector(7 downto 0) := (others => '0');
	signal alltx_ctrl_wren : std_logic := '0';
	signal allrx_ctrl : std_logic_vector(7 downto 0) := (others => '0');
	signal allrx_ctrl_wren : std_logic := '0';

	-- secure reg
	signal soft_reset : std_logic := '0';
	signal secure_reg : std_logic := '0';
	constant secure_cnt_max : integer := (4000000)-1;
	signal secure_cnt : integer range 0 to secure_cnt_max := 0;

	-- internal wishbone signals
	signal wb_dat_o_int : std_logic_vector(7 downto 0);
	signal wb_dat_i_int : std_logic_vector(7 downto 0);
	signal wb_adr_int : std_logic_vector(15 downto 0);
	signal wb_cyc_int : std_logic;
	signal wb_stb_int : std_logic;
	signal wb_we_int : std_logic;
	signal wb_ack_int : std_logic;

	-- generate MGT user clock
	signal gtpclk_to_cmt: std_logic := '0';
	signal gtpclk_fb: std_logic := '0';
	signal gtpclk_rst: std_logic := '0';
	signal gtp_locked: std_logic := '0';
	signal gtpclk: std_logic := '0';
	signal gtp0_plldet_out: std_logic := '0';
	signal gtp1_plldet_out: std_logic := '0';
	signal usr_clk: std_logic := '0';
	signal usr_clk20: std_logic := '0';
	signal usr_clk40: std_logic := '0';
	signal gtp_rst_done0_0: std_logic;
	signal gtp_rst_done0_1: std_logic;
	signal gtp_rst_done1_0: std_logic;
	signal gtp_rst_done1_1: std_logic;

	-- backplane data
	signal rxdata_boc2rod_i : std_logic_vector(47 downto 0);

	-- SLINK control and status registers
	signal slink0_control_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal slink0_control_reg_sync : std_logic_vector(7 downto 0) := (others => '0');
	signal slink1_control_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal slink1_control_reg_sync : std_logic_vector(7 downto 0) := (others => '0');
	signal slink0_status_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal slink1_status_reg : std_logic_vector(7 downto 0) := (others => '0');
	
	signal hola0reset : std_logic;
	signal hola1reset : std_logic;

	-- SLINK LEDs
	signal slink0_testled_n : std_logic := '0';
	signal slink0_lderrled_n : std_logic := '0';
	signal slink0_lupled_n : std_logic := '0';
	signal slink0_flowctlled_n : std_logic := '0';
	signal slink0_activityled_n : std_logic := '0';
	signal slink1_testled_n : std_logic := '0';
	signal slink1_lderrled_n : std_logic := '0';
	signal slink1_lupled_n : std_logic := '0';
	signal slink1_flowctlled_n : std_logic := '0';
	signal slink1_activityled_n : std_logic := '0';

	-- internal SLINK test signals + selection
	signal slink0_test_udata : std_logic_vector(31 downto 0) := (others => '0');
	signal slink0_test_uctrl : std_logic := '1';
	signal slink0_test_uwe : std_logic := '1';
	signal slink0_test_ureset : std_logic := '1';
	signal slink0_test_utest : std_logic := '1';
	signal slink0_test_lff : std_logic := '1';
	signal slink0_test_ldown : std_logic := '1';
	signal slink1_test_udata : std_logic_vector(31 downto 0) := (others => '0');
	signal slink1_test_uctrl : std_logic := '1';
	signal slink1_test_uwe : std_logic := '1';
	signal slink1_test_ureset : std_logic := '1';
	signal slink1_test_utest : std_logic := '1';
	signal slink1_test_lff : std_logic := '1';
	signal slink1_test_ldown : std_logic := '1';
	signal slink0_internal_udata : std_logic_vector(31 downto 0) := (others => '0');
	signal slink0_internal_uctrl : std_logic := '1';
	signal slink0_internal_uwe : std_logic := '1';
	signal slink0_internal_ureset : std_logic := '1';
	signal slink0_internal_utest : std_logic := '1';
	signal slink0_internal_lff : std_logic := '1';
	signal slink0_internal_ldown : std_logic := '1';
	signal slink1_internal_udata : std_logic_vector(31 downto 0) := (others => '0');
	signal slink1_internal_uctrl : std_logic := '1';
	signal slink1_internal_uwe : std_logic := '1';
	signal slink1_internal_ureset : std_logic := '1';
	signal slink1_internal_utest : std_logic := '1';
	signal slink1_internal_lff : std_logic := '1';
	signal slink1_internal_ldown : std_logic := '1';
	signal slink1_gen_enable : std_logic := '0';
	signal slink0_gen_enable : std_logic := '0';
	signal mgt0_reset : std_logic := '0';
	signal mgt1_reset : std_logic := '0';
	signal clk_slink0 : std_logic := '0';
	signal clk_slink1 : std_logic := '0';
	signal slink0_clksel : std_logic := '0';
	signal slink1_clksel : std_logic := '0';
	signal slink0_clkgood : std_logic := '0';
	signal slink1_clkgood : std_logic := '0';
	signal slink0_patcheck_err : std_logic := '0';
	signal slink0_patcheck_bitcnt : std_logic_vector(63 downto 0) := (others => '0');
	signal slink0_patcheck_biterr : std_logic_vector(63 downto 0) := (others => '0');
	signal slink0_patcheck_enable : std_logic := '0';
	signal slink1_patcheck_err : std_logic := '0';
	signal slink1_patcheck_bitcnt : std_logic_vector(63 downto 0) := (others => '0');
	signal slink1_patcheck_biterr : std_logic_vector(63 downto 0) := (others => '0');
	signal slink1_patcheck_enable : std_logic := '0';
	signal slink0_chk_error : std_logic := '0';
	signal slink0_chk_error_src : std_logic_vector(2 downto 0) := "000";
	signal slink1_chk_error : std_logic := '0';
	signal slink1_chk_error_src : std_logic_vector(2 downto 0) := "000";

	-- S-LINK counters
	signal slink_count_readsr : std_logic_vector(127 downto 0) := (others => '0');
	signal slink_count_reset : std_logic := '0';
	signal slink0_count_hola0_lff : std_logic_vector(15 downto 0) := (others => '0');
	signal slink0_count_hola0_ldown : std_logic_vector(15 downto 0) := (others => '0');
	signal slink0_count_hola1_lff : std_logic_vector(15 downto 0) := (others => '0');
	signal slink0_count_hola1_ldown : std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_count_hola0_lff : std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_count_hola0_ldown : std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_count_hola1_lff : std_logic_vector(15 downto 0) := (others => '0');
	signal slink1_count_hola1_ldown : std_logic_vector(15 downto 0) := (others => '0');

	-- broadcast enable
	signal bcast_enable_tx : std_logic_vector(15 downto 0) := (others => '0');
	signal bcast_enable_rx : std_logic_vector(15 downto 0) := (others => '0');

	-- git hash
	signal git_hash : std_logic_vector(159 downto 0) := (others => '0');

	-- feature register
	signal feature_reg : std_logic_vector(15 downto 0) := (others => '0');
begin

-- OR reset and soft-reset
or_reset <= reset or soft_reset when rising_edge(clk_regbank);

-- register banks + multiplexer
bmf_slave_mux_i: bmf_slave_mux PORT MAP (
	clk => clk_regbank,
	reset => reset,
	wb_dat_i => wb_dat_i_int,
	wb_dat_o => wb_dat_o_int,
	wb_adr => wb_adr_int,
	wb_cyc => wb_cyc_int,
	wb_stb => wb_stb_int,
	wb_we => wb_we_int,
	wb_ack => wb_ack_int,
	rx_regs_wr => rx_regs_wr,
	rx_regs_rd => rx_regs_rd,
	rx_regs_wren => rx_regs_wren,
	rx_regs_rden => rx_regs_rden,
	tx_regs_wr => tx_regs_wr,
	tx_regs_rd => tx_regs_rd,
	tx_regs_wren => tx_regs_wren,
	tx_regs_rden => tx_regs_rden,
	com_regs_wr => com_regs_wr,
	com_regs_rd => com_regs_rd,
	com_regs_wren => com_regs_wren,
	com_regs_rden => com_regs_rden
);

-- map wishbone internals to i/o ports
wb_dat_i_int <= wb_dat_i;
wb_dat_o <= wb_dat_o_int;
wb_adr_int <= wb_adr;
wb_cyc_int <= wb_cyc;
wb_stb_int <= wb_stb;
wb_we_int <= wb_we;
wb_ack <= wb_ack_int;

-- transmitter instances
transmitters: for I in 0 to 15 generate
	bmf_tx_i: bmf_tx 
		GENERIC MAP (
			DEFAULT_XC_LINK => I,
			INCLUDE_ECR_RECONF => not PixelRx and (I < 8)		-- include the ECR reconfiguration only for IBL and only in the lower 8 channels
		)
		PORT MAP (
			clk_tx => clk_tx,
			clk_tx_fast => clk_tx_fast,
			clk_tx_fast_strobe => clk_tx_fast_strobe,
			reset => reset,
			soft_reset => soft_reset,
			xc_vec => xc,
			txout => txout_int(I),
			txemudata => txemu_data(I),
			cmd_trigger => tx_cmdtrigger(I),
			reg_rd => tx_regs_rd(32*I to 32*I+31),
			reg_wr => tx_regs_wr(32*I to 32*I+31),
			reg_wren => tx_regs_wren(32*I to 32*I+31),
			reg_rden => tx_regs_rden(32*I to 32*I+31),
			bcast_enable => bcast_enable_tx(I),
			bcast_wren => tx_regs_wren(32*16 to 32*16+31),		-- broadcast channel will be "magic" channel 16
			bcast_wr => tx_regs_wr(32*16 to 32*16+31),
			secure_reg => secure_reg
		);	
end generate;

-- IBL rx path
iblrx_generate: if PixelRx = false generate
	iblrx_i: iblrx port map (
		clk_serial => clk_rx,
		clk_serial4x => clk_rx_fast,
		clk_serial4x_serdesstrobe => clk_rx_fast_strobe,
		clk_backplane => clk_backplane,
		clk_regbank => clk_regbank,
		reset => or_reset,
		din_optical => optical_in,
		din_loopback => (others => '0'),
		din_feemu => fei4_dout,
		dout => rxdata_boc2rod_i,
		rxregs_wr => rx_regs_wr,
		rxregs_rd => rx_regs_rd,
		rxregs_wren => rx_regs_wren,
		rxregs_rden => rx_regs_rden,
		allrx_ctrl => (others => '0'),
		allrx_ctrl_wren => '0',
		rxdebug_select => rxdebug_select,
		rxdebug_slow => rxdebug_slow_select,
		rxdebug_fast => rxdebug_fast_select,
		rxmux_wr => com_regs_wr(56 to 63),
		rxmux_rd => com_regs_rd(56 to 63),
		rxmux_wren => com_regs_wren(56 to 63),
		rxmux_rden => com_regs_rden(56 to 63),
		cdr_wr => com_regs_wr(80 to 127),
		cdr_rd => com_regs_rd(80 to 127),
		cdr_wren => com_regs_wren(80 to 127),
		cdr_rden => com_regs_rden(80 to 127),
		bert_bypass => open,
		bert_bypass_valid => open
	);
end generate;

-- Pixel RX
pixrx_generate: if PixelRx = true generate
	pixrx_i: pixrx port map (
		clk_serial => clk_rx,
		clk_serial8x => clk_rx_fast,
		clk_serial8x_serdesstrobe => clk_rx_fast_strobe,
		clk_backplane => clk_backplane,
		clk_regbank => clk_regbank,
		reset => or_reset,
		din_optical => optical_in,
		dout => rxdata_boc2rod_i,
		xc_vec => txemu_data,
		rxregs_wr => rx_regs_wr,
		rxregs_rd => rx_regs_rd,
		rxregs_wren => rx_regs_wren,
		rxregs_rden => rx_regs_rden,
		phase_wr => com_regs_wr(80 to 127),
		phase_rd => com_regs_rd(80 to 127),
		phase_wren => com_regs_wren(80 to 127),
		phase_rden => com_regs_rden(80 to 127),
		speed_wr => com_regs_wr(31),
		speed_rd => com_regs_rd(31),
		speed_wren => com_regs_wren(31),
		speed_rden => com_regs_rden(31),
		bcast_enable => bcast_enable_rx,
		rxdebug_select => rxdebug_select,
		rxdebug_slow => rxdebug_slow_select,
		rxdebug_fast => rxdebug_fast_select
	);
end generate;

-- output backplane data
rxdata_boc2rod <= rxdata_boc2rod_i;

-- readonly firmware register
com_regs_rd(0) <= x"60";

-- don't generate stuff for IBL in the Pixel firmware
fei4_ibl_generate: if PixelRx = false generate
	-- FE-I4 command spy
	fei4_spy_i: fei4_spy
		port map (
			clk => clk_tx,
			reset => or_reset,
			din => txemu_data,
			csr_wr => com_regs_wr(64),
			csr_rd => com_regs_rd(64),
			csr_wren => com_regs_wren(64),
			csr_rden => com_regs_rden(64),
			fifo0_wr => com_regs_wr(65),
			fifo0_rd => com_regs_rd(65),
			fifo0_wren => com_regs_wren(65),
			fifo0_rden => com_regs_rden(65),
			fifo1_wr => com_regs_wr(66),
			fifo1_rd => com_regs_rd(66),
			fifo1_wren => com_regs_wren(66),
			fifo1_rden => com_regs_rden(66),
			fifo2_wr => com_regs_wr(67),
			fifo2_rd => com_regs_rd(67),
			fifo2_wren => com_regs_wren(67),
			fifo2_rden => com_regs_rden(67),
			fifo3_wr => com_regs_wr(68),
			fifo3_rd => com_regs_rd(68),
			fifo3_wren => com_regs_wren(68),
			fifo3_rden => com_regs_rden(68),
			trigcnt0_wr => com_regs_wr(69),
			trigcnt0_rd => com_regs_rd(69),
			trigcnt0_wren => com_regs_wren(69),
			trigcnt0_rden => com_regs_rden(69),
			trigcnt1_wr => com_regs_wr(70),
			trigcnt1_rd => com_regs_rd(70),
			trigcnt1_wren => com_regs_wren(70),
			trigcnt1_rden => com_regs_rden(70),
			trigcnt2_wr => com_regs_wr(71),
			trigcnt2_rd => com_regs_rd(71),
			trigcnt2_wren => com_regs_wren(71),
			trigcnt2_rden => com_regs_rden(71),
			trigcnt3_wr => com_regs_wr(72),
			trigcnt3_rd => com_regs_rd(72),
			trigcnt3_wren => com_regs_wren(72),
			trigcnt3_rden => com_regs_rden(72),
			calcnt0_wr => com_regs_wr(73),
			calcnt0_rd => com_regs_rd(73),
			calcnt0_wren => com_regs_wren(73),
			calcnt0_rden => com_regs_rden(73),
			calcnt1_wr => com_regs_wr(74),
			calcnt1_rd => com_regs_rd(74),
			calcnt1_wren => com_regs_wren(74),
			calcnt1_rden => com_regs_rden(74),
			calcnt2_wr => com_regs_wr(75),
			calcnt2_rd => com_regs_rd(75),
			calcnt2_wren => com_regs_wren(75),
			calcnt2_rden => com_regs_rden(75),
			calcnt3_wr => com_regs_wr(76),
			calcnt3_rd => com_regs_rd(76),
			calcnt3_wren => com_regs_wren(76),
			calcnt3_rden => com_regs_rden(76)
		);

	-- FE-I4 emulator
	fei4_emulators: for I in 0 to 15 generate
		fei4_emulator_i: fei4_emulator
			generic map (
				DEFAULT_CHIPID => (7-(I mod 2)),
				DEFAULT_DINSEL => I/2
			)
			port map (
				clk40 => clk_tx,
				clk160 => clk_rx,
				reset => or_reset,
				din => txemu_data,
				dout => fei4_dout(I),
				control_rd => com_regs_rd(128 + 8*I + 0),
				control_wr => com_regs_wr(128 + 8*I + 0),
				control_rden => com_regs_rden(128 + 8*I + 0),
				control_wren => com_regs_wren(128 + 8*I + 0),
				status_rd => com_regs_rd(128 + 8*I + 1),
				status_wr => com_regs_wr(128 + 8*I + 1),
				status_rden => com_regs_rden(128 + 8*I + 1),
				status_wren => com_regs_wren(128 + 8*I + 1),
				extra_frames_rd => com_regs_rd(128 + 8*I + 2),
				extra_frames_wr => com_regs_wr(128 + 8*I + 2),
				extra_frames_rden => com_regs_rden(128 + 8*I + 2),
				extra_frames_wren => com_regs_wren(128 + 8*I + 2),
				control2_rd => com_regs_rd(128 + 8*I + 3),
				control2_wr => com_regs_wr(128 + 8*I + 3),
				control2_rden => com_regs_rden(128 + 8*I + 3),
				control2_wren => com_regs_wren(128 + 8*I + 3),
				hit_rd => com_regs_rd(128 + 8*I + 6),
				hit_wr => com_regs_wr(128 + 8*I + 6),
				hit_rden => com_regs_rden(128 + 8*I + 6),
				hit_wren => com_regs_wren(128 + 8*I + 6),
				hitcount_rd => com_regs_rd(128 + 8*I + 7),
				hitcount_wr => com_regs_wr(128 + 8*I + 7),
				hitcount_rden => com_regs_rden(128 + 8*I + 7),
				hitcount_wren => com_regs_wren(128 + 8*I + 7)
			);
	end generate;
end generate;

-- BuildDate registers
BuildDate_i: BuildDate PORT MAP(
	year => com_regs_rd(1),
	month => com_regs_rd(2),
	day => com_regs_rd(3),
	hour => com_regs_rd(4),
	minute => com_regs_rd(5),
	second => com_regs_rd(6),
	git_hash => git_hash
);

-- read out lockloss cnt
com_regs_rd(7) <= lockloss_cnt;

-- git hash generate
git_hash_regs: for I in 0 to 19 generate
	com_regs_rd(32+I) <= git_hash(8*I+7 downto 8*I);
end generate;

-- broadcast masks
process begin
	wait until rising_edge(clk_regbank);

	if or_reset = '1' then
		bcast_enable_tx <= (others => '0');
		bcast_enable_rx <= (others => '0');
	else
		if com_regs_wren(52) = '1' then
			bcast_enable_tx(7 downto 0) <= com_regs_wr(52);
		end if;
		if com_regs_wren(53) = '1' then
			bcast_enable_tx(15 downto 8) <= com_regs_wr(53);
		end if;
		if com_regs_wren(54) = '1' then
			bcast_enable_rx(7 downto 0) <= com_regs_wr(54);
		end if;
		if com_regs_wren(55) = '1' then
			bcast_enable_rx(15 downto 8) <= com_regs_wr(55);
		end if;
	end if;
end process;
com_regs_rd(52) <= bcast_enable_tx(7 downto 0);
com_regs_rd(53) <= bcast_enable_tx(15 downto 8);
com_regs_rd(54) <= bcast_enable_rx(7 downto 0);
com_regs_rd(55) <= bcast_enable_rx(15 downto 8);

-- RX plugin controller
RX0_Ctrl: RxPluginCtrl
	generic map (
		f_clk => 40000000,
		f_scl => 100000
	)
	port map (
		clk => clk_regbank,
		reset => or_reset,
		i2c_scl => rx_scl(0),
		i2c_sda => rx_sda(0),
		csr_rd => com_regs_rd(22),
		csr_wr => com_regs_wr(22),
		csr_rden => com_regs_rden(22),
		csr_wren => com_regs_wren(22),
		data_lsb_rd => com_regs_rd(23),
		data_lsb_wr => com_regs_wr(23),
		data_lsb_rden => com_regs_rden(23),
		data_lsb_wren => com_regs_wren(23),
		data_msb_rd => com_regs_rd(24),
		data_msb_wr => com_regs_wr(24),
		data_msb_rden => com_regs_rden(24),
		data_msb_wren => com_regs_wren(24)
	);

-- RX plugin controller
RX1_Ctrl: RxPluginCtrl
	generic map (
		f_clk => 40000000,
		f_scl => 100000
	)
	port map (
		clk => clk_regbank,
		reset => or_reset,
		i2c_scl => rx_scl(1),
		i2c_sda => rx_sda(1),
		csr_rd => com_regs_rd(25),
		csr_wr => com_regs_wr(25),
		csr_rden => com_regs_rden(25),
		csr_wren => com_regs_wren(25),
		data_lsb_rd => com_regs_rd(26),
		data_lsb_wr => com_regs_wr(26),
		data_lsb_rden => com_regs_rden(26),
		data_lsb_wren => com_regs_wren(26),
		data_msb_rd => com_regs_rd(27),
		data_msb_wr => com_regs_wr(27),
		data_msb_rden => com_regs_rden(27),
		data_msb_wren => com_regs_wren(27)
	);

-- output tx
txout <= txout_int;

-- SLINK clocking
gtpclk_rst     <=  not gtp0_plldet_out;


-- MGT clocking
gtpclkout0_0_pll0_bufio2_i : BUFIO2
    generic map
    (
        DIVIDE                          =>      1,
        DIVIDE_BYPASS                   =>      TRUE
    )
    port map
    (
        I                               =>      gtpclk, -- tile0_gtpclkout0_i(0),
        DIVCLK                          =>      gtpclk_to_cmt, -- tile0_gtpclkout0_0_to_cmt_i,
        IOCLK                           =>      open,
        SERDESSTROBE                    =>      open
    );

 gtpclkout_dcm : MGT_USRCLK_SOURCE_v2
 port map
 (
	  DIV1_OUT                        =>      usr_clk,
	  DIV2_OUT                        =>      usr_clk20,
	  DIV4_OUT                        =>      usr_clk40,
	  CLK2X_OUT                       =>      open,
	  DCM_LOCKED_OUT                  =>      gtp_locked,
	  CLK_IN                          =>      gtpclk_to_cmt,
	  DCM_RESET_IN                    =>      gtpclk_rst
 );

	 hola0reset <= reset or slink0_control_reg_sync(5);
	 hola1reset <= reset or slink1_control_reg_sync(5);
	 mgt0_reset <= reset or slink0_control_reg_sync(6);
	 mgt1_reset <= reset or slink1_control_reg_sync(6);
	
-- SLINK LSC
lsc0: hola_lsc_s6 
	GENERIC MAP (simulation => 0)
	PORT MAP(
		mgtrefclk => clk_slink,
		sys_rst => hola0reset , -- reset,
		mgt_rst => mgt0_reset, -- '0',
		gtpclkout => gtpclk,
		gtp_plldet_out => gtp0_plldet_out,
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		usr_clk40_in => usr_clk40,
		ud => slink0_internal_udata,
		ureset_n => slink0_internal_ureset,
		utest_n => slink0_internal_utest,
		uctrl_n => slink0_internal_uctrl,
		uwen_n => slink0_internal_uwe,
		uclk => clk_slink0,
		lff_n => slink0_internal_lff,
		lrl0 => open,
		lrl1 => open,
		ldown_n => slink0_internal_ldown,
		hola_enable_ldown_0 => '1',
		hola_enable_lff_0 => '1',
		hola_enable_ldown_1 => slink0_control_reg_sync(3),
		hola_enable_lff_1 => slink0_control_reg_sync(2),
		count_clk => clk_regbank,
		count_hola0_lff => slink0_count_hola0_lff,
		count_hola0_ldown => slink0_count_hola0_ldown,
		count_hola1_lff => slink0_count_hola1_lff,
		count_hola1_ldown => slink0_count_hola1_ldown,
		count_reset => slink_count_reset,
		tlk_sin0_p => qsfp_rx_p(0),
		tlk_sin0_n => qsfp_rx_n(0),
		tlk_sout0_p => qsfp_tx_p(0),
		tlk_sout0_n => qsfp_tx_n(0),
		tlk_sin1_p => qsfp_rx_p(1),
		tlk_sin1_n => qsfp_rx_n(1),
		tlk_sout1_p => qsfp_tx_p(1),
		tlk_sout1_n => qsfp_tx_n(1),
		testled_n => slink0_testled_n,
		lderrled_n => slink0_lderrled_n,
		lupled_n => slink0_lupled_n,
		flowctlled_n => slink0_flowctlled_n,
		activityled_n => slink0_activityled_n,
		usr_clk_out => open, -- usr_clk_out,
		gtx_reset_done0 => gtp_rst_done0_0,
		gtx_reset_done1 => gtp_rst_done0_1,
		dbg_txclk0 => slink0_dbg_txclk0,
		dbg_txd0 => slink0_dbg_txd0,
		dbg_tx_er0 => slink0_dbg_tx_er0,
		dbg_tx_en0 => slink0_dbg_tx_en0,
		dbg_rxclk0 => slink0_dbg_rxclk0,
		dbg_cc0 => slink0_dbg_cc0,
		dbg_rxd0 => slink0_dbg_rxd0,
		dbg_rx_er0 => slink0_dbg_rx_er0,
		dbg_rx_dv0 => slink0_dbg_rx_dv0,
		dbg_tlkgtx0 => slink0_dbg_tlkgtx0,
		dbg_txclk1 => slink0_dbg_txclk1,
		dbg_txd1 => slink0_dbg_txd1,
		dbg_tx_er1 => slink0_dbg_tx_er1,
		dbg_tx_en1 => slink0_dbg_tx_en1,
		dbg_rxclk1 => slink0_dbg_rxclk1,
		dbg_cc1 => slink0_dbg_cc1,
		dbg_rxd1 => slink0_dbg_rxd1,
		dbg_rx_er1 => slink0_dbg_rx_er1,
		dbg_rx_dv1 => slink0_dbg_rx_dv1,
		dbg_tlkgtx1 => slink0_dbg_tlkgtx1,
		-- 
		dbg_rxrecclk0 => open,
		dbg_rxlossofsync0 => slink0_dbg_rxlos0,
		dbg_rxreset0 => slink0_dbg_rxreset0,
		dbg_rxcdrreset0 => slink0_dbg_rxcdrreset0,
		dbg_rxbufreset0 => slink0_dbg_rxbufreset0,
		dbg_rxrecclk1 => open,
		dbg_rxlossofsync1 => slink0_dbg_rxlos1,
		dbg_rxreset1  => slink0_dbg_rxreset1,
		dbg_rxcdrreset1  => slink0_dbg_rxcdrreset1,
		dbg_rxbufreset1  => slink0_dbg_rxbufreset1,

		dbg_fifo_q0 => slink0_dbg_fifo_q0,
		dbg_fifo_rden0 => slink0_dbg_fifo_rden0,
		dbg_fifo_empty0 => slink0_dbg_fifo_empty0,
		dbg_fifo_clk0 => slink0_dbg_fifo_clk0,
		dbg_fifo_q1 => slink0_dbg_fifo_q1,
		dbg_fifo_rden1 => slink0_dbg_fifo_rden1,
		dbg_fifo_empty1 => slink0_dbg_fifo_empty1,
		dbg_fifo_clk1 => slink0_dbg_fifo_clk1
	);

lsc1: hola_lsc_s6
	GENERIC MAP (simulation => 0)
	PORT MAP(
		mgtrefclk => clk_slink,
		sys_rst => hola1reset , -- reset,
		mgt_rst => mgt1_reset, -- '0'
		gtpclkout => open,
		gtp_plldet_out => gtp1_plldet_out,
		usr_clk_in => usr_clk,
		usr_clk20_in => usr_clk20,
		usr_clk40_in => usr_clk40,
		ud => slink1_internal_udata,
		ureset_n => slink1_internal_ureset,
		utest_n => slink1_internal_utest,
		uctrl_n => slink1_internal_uctrl,
		uwen_n => slink1_internal_uwe,
		uclk => clk_slink1,
		lff_n => slink1_internal_lff,
		lrl0 => open,
		lrl1 => open,
		ldown_n => slink1_internal_ldown,
		hola_enable_ldown_0 => '1',
		hola_enable_lff_0 => '1',
		hola_enable_ldown_1 => slink1_control_reg_sync(3),
		hola_enable_lff_1 => slink1_control_reg_sync(2),
		count_clk => clk_regbank,
		count_hola0_lff => slink1_count_hola0_lff,
		count_hola0_ldown => slink1_count_hola0_ldown,
		count_hola1_lff => slink1_count_hola1_lff,
		count_hola1_ldown => slink1_count_hola1_ldown,
		count_reset => slink_count_reset,
		tlk_sin0_p => qsfp_rx_p(2),
		tlk_sin0_n => qsfp_rx_n(2),
		tlk_sout0_p => qsfp_tx_p(2),
		tlk_sout0_n => qsfp_tx_n(2),
		tlk_sin1_p => qsfp_rx_p(3),
		tlk_sin1_n => qsfp_rx_n(3),
		tlk_sout1_p => qsfp_tx_p(3),
		tlk_sout1_n => qsfp_tx_n(3),
		testled_n => slink1_testled_n,
		lderrled_n => slink1_lderrled_n,
		lupled_n => slink1_lupled_n,
		flowctlled_n => slink1_flowctlled_n,
		activityled_n => slink1_activityled_n,
		usr_clk_out => open, -- usr_clk_out,
		gtx_reset_done0 => gtp_rst_done1_0,
		gtx_reset_done1 => gtp_rst_done1_1,
		dbg_txclk0 => slink1_dbg_txclk0,
		dbg_txd0 => slink1_dbg_txd0,
		dbg_tx_er0 => slink1_dbg_tx_er0,
		dbg_tx_en0 => slink1_dbg_tx_en0,
		dbg_rxclk0 => slink1_dbg_rxclk0,
		dbg_cc0 => slink1_dbg_cc0,
		dbg_rxd0 => slink1_dbg_rxd0,
		dbg_rx_er0 => slink1_dbg_rx_er0,
		dbg_rx_dv0 => slink1_dbg_rx_dv0,
		dbg_tlkgtx0 => slink1_dbg_tlkgtx0,
		dbg_txclk1 => slink1_dbg_txclk1,
		dbg_txd1 => slink1_dbg_txd1,
		dbg_tx_er1 => slink1_dbg_tx_er1,
		dbg_tx_en1 => slink1_dbg_tx_en1,
		dbg_rxclk1 => slink1_dbg_rxclk1,
		dbg_cc1 => slink1_dbg_cc1,
		dbg_rxd1 => slink1_dbg_rxd1,
		dbg_rx_er1 => slink1_dbg_rx_er1,
		dbg_rx_dv1 => slink1_dbg_rx_dv1,
		dbg_tlkgtx1 => slink1_dbg_tlkgtx1,
		--
		dbg_rxrecclk0 => open,
		dbg_rxlossofsync0 => slink1_dbg_rxlos0,
		dbg_rxreset0 => slink1_dbg_rxreset0,
		dbg_rxcdrreset0 => slink1_dbg_rxcdrreset0,
		dbg_rxbufreset0 => slink1_dbg_rxbufreset0,
		dbg_rxrecclk1 => open,
		dbg_rxlossofsync1 => slink1_dbg_rxlos1,
		dbg_rxreset1  => slink1_dbg_rxreset1,
		dbg_rxcdrreset1  => slink1_dbg_rxcdrreset1,
		dbg_rxbufreset1  => slink1_dbg_rxbufreset1,

		dbg_fifo_q0 => slink1_dbg_fifo_q0,
		dbg_fifo_rden0 => slink1_dbg_fifo_rden0,
		dbg_fifo_empty0 => slink1_dbg_fifo_empty0,
		dbg_fifo_clk0 => slink1_dbg_fifo_clk0,
		dbg_fifo_q1 => slink1_dbg_fifo_q1,
		dbg_fifo_rden1 => slink1_dbg_fifo_rden1,
		dbg_fifo_empty1 => slink1_dbg_fifo_empty1,
		dbg_fifo_clk1 => slink1_dbg_fifo_clk1
	);

-- SLINK LEDs
slink_led(0) <= not (slink0_testled_n and slink1_testled_n);
slink_led(1) <= not (slink0_lderrled_n and slink1_lderrled_n);
slink_led(2) <= not (slink0_lupled_n and slink1_lupled_n);
slink_led(3) <= not (slink0_flowctlled_n and slink1_flowctlled_n);
slink_led(4) <= not (slink0_activityled_n and slink1_activityled_n);

-- SLINK assignment depending on control register
-- common clock!
-- at the moment, we don't use the urest from rod. it is monitired and 
-- ureset is driven from the slink control register (bit4)
-- utest from rod is not used, drive to 1 (inactive)
process begin
	wait until rising_edge(clk_slink0);

	if slink0_control_reg_sync(0) = '1' then
		slink0_internal_udata <= slink0_test_udata;
		slink0_internal_ureset <= (slink0_control_reg_sync(4) and slink0_test_ureset);
		if (((slink0_control_reg_sync(4) and slink0_test_ureset) = '0') or (slink0_internal_ldown = '0')) then
			slink0_internal_uctrl <= '1';
			slink0_internal_utest <= '1';
			slink0_internal_uwe <= '1';
		else
			slink0_internal_uctrl <= slink0_test_uctrl;
			slink0_internal_utest <= slink0_test_utest;
			slink0_internal_uwe <= slink0_test_uwe;
		end if;
	else
		slink0_internal_udata <= slink0_udata;
		slink0_internal_ureset <= (slink0_control_reg_sync(4) and slink0_ureset); -- slink0_ureset;
		if (((slink0_control_reg_sync(4) and slink0_ureset) = '0') or (slink0_internal_ldown = '0')) then
			slink0_internal_uctrl <= '1';
			slink0_internal_uwe <= '1';
		else
			slink0_internal_uctrl <= slink0_uctrl;
			slink0_internal_uwe <= slink0_uwe;
		end if;
		slink0_internal_utest <= '1'; -- slink0_utest;
	end if;
	
	slink0_test_lff <= slink0_internal_lff;
	slink0_test_ldown <= slink0_internal_ldown;
	slink0_lff <= slink0_internal_lff;
	slink0_ldown <= slink0_internal_ldown;
end process;

process begin
	wait until rising_edge(clk_slink1);

	if slink1_control_reg_sync(0) = '1' then
		slink1_internal_udata <= slink1_test_udata;
		slink1_internal_ureset <= (slink1_control_reg_sync(4) and slink1_test_ureset);
		if (((slink1_control_reg_sync(4) and slink1_test_ureset) = '0') or (slink1_internal_ldown = '0')) then
			slink1_internal_uctrl <= '1';
			slink1_internal_utest <= '1';
			slink1_internal_uwe <= '1';
		else
			slink1_internal_uctrl <= slink1_test_uctrl;
			slink1_internal_utest <= slink1_test_utest;
			slink1_internal_uwe <= slink1_test_uwe;
		end if;
	else
		slink1_internal_udata <= slink1_udata;
		slink1_internal_ureset <= (slink1_control_reg_sync(4) and slink1_ureset); -- slink1_ureset;
		if (((slink1_control_reg_sync(4) and slink1_ureset) = '0') or (slink1_internal_ldown = '0')) then
			slink1_internal_uctrl <= '1';
			slink1_internal_uwe <= '1';
		else
			slink1_internal_uctrl <= slink1_uctrl;
			slink1_internal_uwe <= slink1_uwe;
		end if;
		slink1_internal_utest <= '1'; -- slink1_utest;
	end if;

	slink1_test_lff <= slink1_internal_lff;
	slink1_test_ldown <= slink1_internal_ldown;
	slink1_lff <= slink1_internal_lff;
	slink1_ldown <= slink1_internal_ldown;
end process;


   slink0_gen_enable <= slink0_control_reg_sync(1) and slink0_internal_ldown;

   slink0_tester_i: lscDataGen PORT MAP (
          lscData => slink0_test_udata,
          lscFull_n => slink0_test_lff,
          lscEnable => slink0_gen_enable,
          lscWen_n => slink0_test_uwe,
          lscCtrl_n => slink0_test_uctrl,
          lscTest_n => slink0_test_utest,
          lscReset_n => slink0_test_ureset,
          clk => clk_slink0,
          reset => hola0reset 
        );

	slink1_gen_enable <= slink1_control_reg_sync(1) and slink1_internal_ldown;        

   slink1_tester_i: lscDataGen PORT MAP (
          lscData => slink1_test_udata,
          lscFull_n => slink1_test_lff,
          lscEnable => slink1_gen_enable,
          lscWen_n => slink1_test_uwe,
          lscCtrl_n => slink1_test_uctrl,
          lscTest_n => slink1_test_utest,
          lscReset_n => slink1_test_ureset,
          clk => clk_slink1,
          reset => hola1reset 
        );

-- SLINK pattern checker
slink0_pattern_check: SlinkTester
	port map (
		clk => clk_slink0,
		reset => hola0reset,
		enable => '1',
		slink_udata => slink0_internal_udata,
		slink_uwe => slink0_internal_uwe,
		slink_uctrl => slink0_internal_uctrl,
		slink_error => slink0_patcheck_err
	);

slink1_pattern_check: SlinkTester
	port map (
		clk => clk_slink1,
		reset => hola1reset,
		enable => '1',
		slink_udata => slink1_internal_udata,
		slink_uwe => slink1_internal_uwe,
		slink_uctrl => slink1_internal_uctrl,
		slink_error => slink1_patcheck_err
	);

-- check S-LINK frame format
slink0_chk: SlinkChecker
	port map (
		clk => clk_slink0,
		reset => hola0reset,
		udata => slink0_internal_udata,
		uctrl => slink0_internal_uctrl,
		uwe => slink0_internal_uwe,
		error => slink0_chk_error,
		error_source => slink0_chk_error_src
	);

slink1_chk: SlinkChecker
	port map (
		clk => clk_slink1,
		reset => hola1reset,
		udata => slink1_internal_udata,
		uctrl => slink1_internal_uctrl,
		uwe => slink1_internal_uwe,
		error => slink1_chk_error,
		error_source => slink1_chk_error_src
	);

-- copy control reg into slink clock domain (1 sync ff)
slink0_control_reg_sync <= slink0_control_reg when rising_edge(clk_slink0);
slink1_control_reg_sync <= slink1_control_reg when rising_edge(clk_slink1);

-- control and status register
process begin
	wait until rising_edge(clk_regbank);

	if or_reset = '1' then
		slink0_control_reg <= "01100000";
		slink1_control_reg <= "01100000";
	else
		if com_regs_wren(16) = '1' then
			slink0_control_reg <= com_regs_wr(16);
		end if;
		if com_regs_wren(18) = '1' then
			slink1_control_reg <= com_regs_wr(18);
		end if;
	end if;
end process;
com_regs_rd(16) <= slink0_control_reg;
com_regs_rd(17) <= slink0_status_reg;
com_regs_rd(18) <= slink1_control_reg;
com_regs_rd(19) <= slink1_status_reg;

-- map SLINK status register (with sync-ff to clk40)
process begin
	wait until rising_edge(clk_regbank);

	slink0_status_reg(0) <= not slink0_testled_n;
	slink0_status_reg(1) <= not slink0_lderrled_n;
	slink0_status_reg(2) <= not slink0_lupled_n;
	slink0_status_reg(3) <= not slink0_flowctlled_n;
	slink0_status_reg(4) <= not slink0_activityled_n;
	slink0_status_reg(5) <= slink0_internal_lff;
	slink0_status_reg(6) <= slink0_internal_ldown;
	-- monitor ureset from rod
	slink0_status_reg(7) <= slink0_ureset;

	slink1_status_reg(0) <= not slink1_testled_n;
	slink1_status_reg(1) <= not slink1_lderrled_n;
	slink1_status_reg(2) <= not slink1_lupled_n;
	slink1_status_reg(3) <= not slink1_flowctlled_n;
	slink1_status_reg(4) <= not slink1_activityled_n;
	slink1_status_reg(5) <= slink1_internal_lff;
	slink1_status_reg(6) <= slink1_internal_ldown;
	-- monitor ureset from rod
	slink1_status_reg(7) <= slink1_ureset;
end process;

-- select SLINK clock input
clk_slink0 <= clk_backplane_slink0;
clk_slink1 <= clk_backplane_slink1;

-- select channel for RX debug
process begin
	wait until rising_edge(clk_regbank);

	if or_reset = '1' then
		rxdebug_select <= "0000";
		txcs_select <= '0';
	else
		if com_regs_wren(10) = '1' then
			rxdebug_select <= com_regs_wr(10)(3 downto 0);
			txcs_select <= com_regs_wr(10)(4);
		end if;
	end if;
end process;
com_regs_rd(10) <= "000" & txcs_select & rxdebug_select;

-- uptime counter
UptimeCounter_i: UptimeCounter
	generic map (
		f_clk => 40000000
	)
	port map (
		clk => clk_regbank,
		reset => reset,
		uptime_rd => com_regs_rd(12),
		uptime_wr => com_regs_wr(12),
		uptime_rden => com_regs_rden(12),
		uptime_wren => com_regs_wren(12)
	);

-- secure reg
HasSecureReg_true: if HasSecureReg = true generate
	process begin
		wait until rising_edge(clk_regbank);

		if com_regs_wren(77) = '1' then
			secure_cnt <= secure_cnt_max;
			if com_regs_wr(77) = x"42" then
				secure_reg <= '1';
			elsif com_regs_wr(77) = x"AB" then
				soft_reset <= '1';
			end if;
		else
			if secure_cnt = 0 then
				secure_reg <= '0';
				soft_reset <= '0';
			else
				secure_cnt <= secure_cnt-1;
			end if;
		end if;
	end process;

	-- indicate secure reg is available and active
	com_regs_rd(77) <= x"42";
end generate;

HasSecureReg_false: if HasSecureReg = false generate
	secure_reg <= '1';
	soft_reset <= '0';
	com_regs_rd(77) <= x"00";
end generate;


-- feature register
feature_reg(0) <= '1';
feature_reg(1) <= '1' when UseRxMux = true else '0';
feature_reg(2) <= '1' when SlowRx = true else '0';
feature_reg(3) <= '1' when UseRceCDR = true else '0';
feature_reg(4) <= '1' when PixelRx = true else '0';
feature_reg(5) <= '1';									-- TX broadcasting available
feature_reg(6) <= '1' when PixelRx = true else '0';		-- RX broadcasting only for L1/L2 currently
feature_reg(7) <= '1' when ECRReconfBitEnable = true else '0';

-- readback of feature register
com_regs_rd(20) <= feature_reg(7 downto 0);
com_regs_rd(21) <= feature_reg(15 downto 8);

-- qsfp control
qsfp_ctrl_i: qsfp_ctrl
	generic map (
		f_clk => 40000000,
		f_scl => 100000
	)
	port map (
		clk => clk_regbank,
		reset => or_reset,
		qsfp_scl => qsfp_scl,
		qsfp_sda => qsfp_sda,
		qsfp_modsel_n => qsfp_modsel_n,
		qsfp_lowpwr_n => qsfp_lowpwr_n,
		qsfp_reset_n => qsfp_reset_n,
		qsfp_int_n => qsfp_int_n,
		qsfp_present_n => qsfp_present_n,
		csr_wr => com_regs_wr(13),
		csr_rd => com_regs_rd(13),
		csr_wren => com_regs_wren(13),
		csr_rden => com_regs_rden(13),
		addr_wr => com_regs_wr(14),
		addr_rd => com_regs_rd(14),
		addr_wren => com_regs_wren(14),
		addr_rden => com_regs_rden(14),
		data_wr => com_regs_wr(15),
		data_rd => com_regs_rd(15),
		data_wren => com_regs_wren(15),
		data_rden => com_regs_rden(15)
	);

-- monitor debug signals from slink
slink0_lowlevel_trig(15 downto 0) <= slink0_dbg_txd0;
slink0_lowlevel_trig(16) <= slink0_dbg_tx_er0;
slink0_lowlevel_trig(17) <= slink0_dbg_tx_en0;
slink0_lowlevel_trig(20 downto 18) <= slink0_dbg_cc0;
slink0_lowlevel_trig(36 downto 21) <= slink0_dbg_rxd0;
slink0_lowlevel_trig(37) <= slink0_dbg_rx_er0;
slink0_lowlevel_trig(38) <= slink0_dbg_rx_dv0;
slink0_lowlevel_trig(54 downto 39) <= slink0_dbg_txd1;
slink0_lowlevel_trig(55) <= slink0_dbg_tx_er1;
slink0_lowlevel_trig(56) <= slink0_dbg_tx_en1;
slink0_lowlevel_trig(59 downto 57) <= slink0_dbg_cc1;
slink0_lowlevel_trig(75 downto 60) <= slink0_dbg_rxd1;
slink0_lowlevel_trig(76) <= slink0_dbg_rx_er1;
slink0_lowlevel_trig(77) <= slink0_dbg_rx_dv1;
slink0_lowlevel_trig(78) <= mgt0_reset;
slink0_lowlevel_trig(79) <= hola0reset;
slink0_lowlevel_trig(80) <= gtp0_plldet_out;
slink0_lowlevel_trig(81) <= slink0_internal_utest;
slink0_lowlevel_trig(82) <= slink0_internal_ureset;
slink0_lowlevel_trig(83) <= slink0_internal_uctrl;
slink0_lowlevel_trig(84) <= slink0_internal_uwe;
slink0_lowlevel_trig(85) <= slink0_internal_lff;
slink0_lowlevel_trig(86) <= slink0_internal_ldown;
slink0_lowlevel_trig(87) <= slink0_control_reg_sync(2);
slink0_lowlevel_trig(88) <= slink0_control_reg_sync(3);
slink0_lowlevel_trig(89) <= gtp_rst_done0_0 and gtp_rst_done0_1;
slink0_lowlevel_trig(147 downto 90) <= slink0_dbg_tlkgtx0;
slink0_lowlevel_trig(205 downto 148) <= slink0_dbg_tlkgtx1;
slink0_lowlevel_trig(207 downto 206) <= slink0_dbg_rxlos0;
slink0_lowlevel_trig(209 downto 208) <= slink0_dbg_rxlos1;


slink1_lowlevel_trig(15 downto 0) <= slink1_dbg_txd0;
slink1_lowlevel_trig(16) <= slink1_dbg_tx_er0;
slink1_lowlevel_trig(17) <= slink1_dbg_tx_en0;
slink1_lowlevel_trig(20 downto 18) <= slink1_dbg_cc0;
slink1_lowlevel_trig(36 downto 21) <= slink1_dbg_rxd0;
slink1_lowlevel_trig(37) <= slink1_dbg_rx_er0;
slink1_lowlevel_trig(38) <= slink1_dbg_rx_dv0;
slink1_lowlevel_trig(54 downto 39) <= slink1_dbg_txd1;
slink1_lowlevel_trig(55) <= slink1_dbg_tx_er1;
slink1_lowlevel_trig(56) <= slink1_dbg_tx_en1;
slink1_lowlevel_trig(59 downto 57) <= slink1_dbg_cc1;
slink1_lowlevel_trig(75 downto 60) <= slink1_dbg_rxd1;
slink1_lowlevel_trig(76) <= slink1_dbg_rx_er1;
slink1_lowlevel_trig(77) <= slink1_dbg_rx_dv1;
slink1_lowlevel_trig(78) <= mgt1_reset;
slink1_lowlevel_trig(79) <= hola1reset;
slink1_lowlevel_trig(80) <= gtp1_plldet_out;
slink1_lowlevel_trig(81) <= slink1_internal_utest;
slink1_lowlevel_trig(82) <= slink1_internal_ureset;
slink1_lowlevel_trig(83) <= slink1_internal_uctrl;
slink1_lowlevel_trig(84) <= slink1_internal_uwe;
slink1_lowlevel_trig(85) <= slink1_internal_lff;
slink1_lowlevel_trig(86) <= slink1_internal_ldown;
slink1_lowlevel_trig(87) <= slink1_control_reg_sync(2);
slink1_lowlevel_trig(88) <= slink1_control_reg_sync(3);
slink1_lowlevel_trig(89) <= gtp_rst_done1_0 and gtp_rst_done1_1;
slink1_lowlevel_trig(147 downto 90) <= slink1_dbg_tlkgtx0;
slink1_lowlevel_trig(205 downto 148) <= slink1_dbg_tlkgtx1;
slink1_lowlevel_trig(207 downto 206) <= slink1_dbg_rxlos0;
slink1_lowlevel_trig(209 downto 208) <= slink1_dbg_rxlos1;

-- S-LINK counter readout
process begin
	wait until rising_edge(clk_regbank);

	-- default
	slink_count_reset <= '0';

	-- reset
	if or_reset = '1' then
		slink_count_reset <= '1';
	end if;

	if com_regs_wren(11) = '1' then
		-- reset counters after reading with magic value 0xAB
		if com_regs_wr(11) = x"AB" then
			slink_count_reset <= '1';
		end if;

		-- put counter values into shift register
		slink_count_readsr <= slink1_count_hola1_ldown & slink1_count_hola1_lff &
							  slink1_count_hola0_ldown & slink1_count_hola0_lff &
							  slink0_count_hola1_ldown & slink0_count_hola1_lff &
							  slink0_count_hola0_ldown & slink0_count_hola0_lff;
	end if;

	-- serialize shift register
	if com_regs_rden(11) = '1' then
		slink_count_readsr <= x"00" & slink_count_readsr(slink_count_readsr'left downto 8);
	end if;
end process;
com_regs_rd(11) <= slink_count_readsr(7 downto 0);

-- chipscope controller
--cs_control_i: chipscope_icon
--	PORT MAP (
--		CONTROL0 => cs_control0,
--		CONTROL1 => cs_control1,
--		CONTROL2 => cs_control2,
--		CONTROL3 => cs_control3,
--		CONTROL4 => cs_control4,
--		CONTROL5 => cs_control5,
--		CONTROL6 => cs_control6
--	);

cs_control_i: chipscope_icon_with_vio
	PORT MAP (
		CONTROL0 => cs_control0,
		CONTROL1 => cs_control1,
		CONTROL2 => cs_control2,
		CONTROL3 => cs_control3,
		CONTROL4 => cs_control4,
		CONTROL5 => cs_control5,
		CONTROL6 => cs_control6,
		CONTROL7 => cs_control7,
		CONTROL8 => cs_control8,
		CONTROL9 => cs_control9
	);

-- chipscope for XC
txcs_data <= txemu_data when txcs_select = '0' else xc;

cs_ila_xc_i: chipscope_ila_xc
	PORT MAP (
		CONTROL => cs_control0,
		CLK => clk_tx,
		TRIG0(15 downto 0) => txcs_data,
		TRIG0(31 downto 16) => tx_cmdtrigger
	);

-- chipscope for SLINK
cs_ila_slink0_i: chipscope_ila_slink
	PORT MAP (
		CONTROL => cs_control1,
		CLK => clk_slink0,
		TRIG0(31 downto 0) => slink0_internal_udata,
		TRIG0(32) => slink0_internal_uctrl,
		TRIG0(33) => slink0_internal_uwe,
		TRIG0(34) => slink0_internal_utest,
		TRIG0(35) => slink0_internal_ureset,
		TRIG0(36) => slink0_internal_ldown,
		TRIG0(37) => slink0_internal_lff,
		TRIG0(38) => slink0_patcheck_err,
		TRIG0(39) => slink0_chk_error,
		TRIG0(42 downto 40) => slink0_chk_error_src
	);

--cs_ila_slink0_ll_i: chipscope_ila_slink_ll
--	PORT MAP (
--		CONTROL => cs_control7,
--		CLK => slink0_dbg_rxclk0,
--		TRIG0 => slink0_lowlevel_trig
--	);

cs_ila_slink0_fifo_i: chipscope_ila_slink_fifo
	port map (
		CONTROL => cs_control7,
		CLK => slink0_dbg_fifo_clk0,
		TRIG0(33 downto 0) => slink0_dbg_fifo_q0,
		TRIG0(34) => slink0_dbg_fifo_empty0,
		TRIG0(35) => slink0_dbg_fifo_rden0,
		TRIG0(69 downto 36) => slink0_dbg_fifo_q1,
		TRIG0(70) => slink0_dbg_fifo_empty1,
		TRIG0(71) => slink0_dbg_fifo_rden1
	);

cs_ila_slink1_i: chipscope_ila_slink
	PORT MAP (
		CONTROL => cs_control2,
		CLK => clk_slink1,
		TRIG0(31 downto 0) => slink1_internal_udata,
		TRIG0(32) => slink1_internal_uctrl,
		TRIG0(33) => slink1_internal_uwe,
		TRIG0(34) => slink1_internal_utest,
		TRIG0(35) => slink1_internal_ureset,
		TRIG0(36) => slink1_internal_ldown,
		TRIG0(37) => slink1_internal_lff,
		TRIG0(38) => slink1_patcheck_err,
		TRIG0(39) => slink1_chk_error,
		TRIG0(42 downto 40) => slink0_chk_error_src
	);

--cs_ila_slink1_ll_i: chipscope_ila_slink_ll
--	PORT MAP (
--		CONTROL => cs_control8,
--		CLK => slink1_dbg_rxclk0,
--		TRIG0 => slink1_lowlevel_trig
--	);

cs_ila_slink1_fifo_i: chipscope_ila_slink_fifo
	port map (
		CONTROL => cs_control8,
		CLK => slink1_dbg_fifo_clk0,
		TRIG0(33 downto 0) => slink1_dbg_fifo_q0,
		TRIG0(34) => slink1_dbg_fifo_empty0,
		TRIG0(35) => slink1_dbg_fifo_rden0,
		TRIG0(69 downto 36) => slink1_dbg_fifo_q1,
		TRIG0(70) => slink1_dbg_fifo_empty1,
		TRIG0(71) => slink1_dbg_fifo_rden1
	);

cs_vio_slink_ll_i: chipscope_vio_slink_ll
	PORT map (
	CONTROL => cs_control9,
	ASYNC_OUT => slink_common_vio_out
	);

	-- drive the resets
	slink0_dbg_rxcdrreset0 <= slink_common_vio_out(0);
	slink0_dbg_rxcdrreset1 <= slink_common_vio_out(1); 
	slink0_dbg_rxreset0 <= slink_common_vio_out(2);
	slink0_dbg_rxreset1 <= slink_common_vio_out(3);
	slink0_dbg_rxbufreset0 <= slink_common_vio_out(4);
	slink0_dbg_rxbufreset1 <= slink_common_vio_out(5);

	slink1_dbg_rxcdrreset0 <= slink_common_vio_out(6);
	slink1_dbg_rxcdrreset1 <= slink_common_vio_out(7); 
	slink1_dbg_rxreset0 <= slink_common_vio_out(8);
	slink1_dbg_rxreset1 <= slink_common_vio_out(9);
	slink1_dbg_rxbufreset0 <= slink_common_vio_out(10);
	slink1_dbg_rxbufreset1 <= slink_common_vio_out(11);

	
	
-- chipscope for ROD2BOC
cs_ila_rod2boc_i: chipscope_ila_rod2boc
	PORT MAP (
		CONTROL => cs_control3,
		CLK => clk_backplane,
		TRIG0 => rxdata_boc2rod_i
	);

-- chipscope for wishbone
cs_ila_wishbone_i: chipscope_ila_wishbone
	PORT MAP (
		CONTROL => cs_control4,
		CLK => clk_regbank,
		TRIG0(7 downto 0) => wb_dat_o_int,
		TRIG0(15 downto 8) => wb_dat_i_int,
		TRIG0(31 downto 16) => wb_adr_int,
		TRIG0(32) => wb_cyc_int,
		TRIG0(33) => wb_stb_int,
		TRIG0(34) => wb_we_int,
		TRIG0(35) => wb_ack_int
	);

cs_ila_rxdebug_fast: rxdebug_fast
	PORT MAP (
		CONTROL => cs_control5,
		CLK => clk_rx,
		TRIG0 => rxdebug_fast_select
	);

cs_ila_rxdebug_slow: rxdebug_slow
	PORT MAP (
		CONTROL => cs_control6,
		CLK => clk_regbank,
		TRIG0 => rxdebug_slow_select
	);

end architecture;
