library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity treadmill_v2 is
  port ( 
    clk_in      : in  STD_LOGIC;
    rst_n_in    : in  STD_LOGIC;
    mode        : in  STD_LOGIC_VECTOR ( 1 downto 0);
    dataword    : in  STD_LOGIC_VECTOR (23 downto 0);
    datalength  : in  STD_LOGIC_VECTOR ( 4 downto 0);
    output      : out STD_LOGIC_VECTOR ( 3 downto 0);
    acknowledge : out STD_LOGIC
    );
end treadmill_v2;

architecture Behavioral of treadmill_v2 is
	-- conversion of datalength input to integer (easier to read and work with)
	signal datalength_int : integer range 0 to 31;
	
	-- shift register and counter
	signal shiftreg : std_logic_vector(31 downto 0) := (others => '0');
	signal fill_counter : integer range 0 to 31 := 0;	
	
	-- helper signals
	signal dataword_isavailable : std_logic := '0';
	signal dataword_istrailer : std_logic := '0';
begin

-- convert datalength input to integer
datalength_int <= to_integer(unsigned(datalength));

-- shift register output
output <= shiftreg(3 downto 0);

dataword_isavailable <= '0' when datalength_int = 0 else '1';
dataword_istrailer <= '1' when ((dataword = "000000000000000000000001") and (datalength_int = 23)) else '0';
acknowledge <= '1' when ((fill_counter <= 4) and (dataword_isavailable = '1') and (mode = "10")) else -- ack for 160
	           '1' when ((fill_counter <= 2) and (dataword_isavailable = '1') and (mode = "01")) else -- ack for 80
	           '1' when ((fill_counter <= 1) and (dataword_isavailable = '1') and ((mode = "00") or (mode = "11"))) else 
	           '0';

-- shift register (+ filling)
process begin
	wait until rising_edge(clk_in);
	
	if rst_n_in = '0' then
		shiftreg <= (others => '0');
		fill_counter <= 0;
	else
		case mode is
			when "01" =>					-- 80 Mbit/s
				if fill_counter > 2 then
					-- we still have enough bits in the shift register, so we don't
					-- need to load new data
					shiftreg <= "00" & shiftreg(31 downto 2);
					fill_counter <= fill_counter - 2;
				elsif fill_counter = 2 then
					-- load new data if it is available
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							-- insert a stuff-bit at the end of the trailer
							-- to achieve frame length to be a multiple of 2
							fill_counter <= datalength_int + 1;
						else
							-- add new data
							fill_counter <= datalength_int;
						end if;
						shiftreg <= "000000" & dataword & shiftreg(3 downto 2);
					else
						-- no new data is available, so just shift
						shiftreg <= "00" & shiftreg(31 downto 2);
						fill_counter <= 0;
					end if;
				elsif fill_counter = 1 then
					if dataword_isavailable = '1' then
						shiftreg <= "0000000" & dataword & shiftreg(2);
						fill_counter <= datalength_int - 1;
					else
						shiftreg <= "00" & shiftreg(31 downto 2);
						fill_counter <= 0;
					end if;
				else
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							-- stuff bit after trailer
							fill_counter <= datalength_int - 1;
						else
							fill_counter <= datalength_int - 2;
						end if;
						shiftreg <= "00000000" & dataword;
					end if;
				end if;
							
			when "10" =>					-- 2x80 Mbit/s
				if fill_counter > 4 then
					shiftreg <= "0000" & shiftreg(31 downto 4);
					fill_counter <= fill_counter - 4;
				elsif fill_counter = 4 then
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							fill_counter <= fill_counter - 4 + datalength_int + 1;
						else
							fill_counter <= fill_counter - 4 + datalength_int;
						end if;
						shiftreg <= "0000" & dataword & shiftreg(7 downto 4);
					else
						shiftreg <= "0000" & shiftreg(31 downto 4);
						fill_counter <= fill_counter - 4;
					end if;
				elsif fill_counter = 3 then
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							fill_counter <= fill_counter - 4 + datalength_int + 2;
						else
							fill_counter <= fill_counter - 4 + datalength_int;
						end if;
						shiftreg <= "00000" & dataword & shiftreg(6 downto 4);
					else
						shiftreg <= "0000" & shiftreg(31 downto 4);
						fill_counter <= fill_counter - 3;
					end if;
				elsif fill_counter = 2 then
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							fill_counter <= fill_counter - 4 + datalength_int + 3;
						else
							fill_counter <= fill_counter - 4 + datalength_int;
						end if;
						shiftreg <= "000000" & dataword & shiftreg(5 downto 4);
					else
						shiftreg <= "0000" & shiftreg(31 downto 4);
						fill_counter <= fill_counter - 2;
					end if;
				elsif fill_counter = 1 then
					if dataword_isavailable = '1' then
						shiftreg <= "0000000" & dataword & shiftreg(4);
						fill_counter <= fill_counter - 4 + datalength_int;
					else
						shiftreg <= "0000" & shiftreg(31 downto 4);
						fill_counter <= fill_counter - 1;
					end if;
				else
					if dataword_isavailable = '1' then
						if dataword_istrailer = '1' then
							fill_counter <= datalength_int - 3;
						else
							fill_counter <= datalength_int - 4;
						end if;
						shiftreg <= "00000000" & dataword;
					end if;
				end if;

			when others =>					-- 40 Mbit/s
				if fill_counter > 1 then
					shiftreg <= '0' & shiftreg(31 downto 1);
					fill_counter <= fill_counter - 1;
				elsif fill_counter = 1 then
					if dataword_isavailable = '1' then
						shiftreg <= "0000000" & dataword & shiftreg(1);
						fill_counter <= datalength_int;
					else
						shiftreg <= '0' & shiftreg(31 downto 1);
						fill_counter <= fill_counter - 1;
					end if;
				else
					if dataword_isavailable = '1' then
						shiftreg <= "00000000" & dataword;
						fill_counter <= datalength_int - 1;
					end if;
				end if;
		end case;
	end if;
end process;

end Behavioral;
