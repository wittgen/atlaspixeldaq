library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity bookkeeper is
	port (
		-- clock and reset inputs
		clk : in std_logic;
		reset : in std_logic;

		-- event write port
		evwrite_bcid : in std_logic_vector(7 downto 0);
		evwrite_l1id : in std_logic_vector(3 downto 0);
		evwrite_hitocc : in std_logic_vector(7 downto 0);
		evwrite : in std_logic;

		-- event read port
		evread_bcid : out std_logic_vector(7 downto 0);
		evread_l1id : out std_logic_vector(3 downto 0);
		evread_skipped : out std_logic_vector(3 downto 0);
		evread_hitocc : out std_logic_vector(7 downto 0);
		evread_ready : out std_logic;
		evread : in std_logic;

		-- full/empty flag
		full : out std_logic;
		empty : out std_logic
	);
end bookkeeper;

architecture rtl of bookkeeper is
	-- read and write pointer
	signal rdptr : integer range 0 to 15 := 0;
	signal wrptr : integer range 0 to 15 := 0;
	signal wrptr_old : integer range 0 to 15 := 0;
	signal evcnt : integer range 0 to 15 := 0;

	-- event meta data storage
	type event_t is record
		bcid : std_logic_vector(7 downto 0);
		l1id : std_logic_vector(3 downto 0);
		skipped : std_logic_vector(3 downto 0);
		hitocc : std_logic_vector(7 downto 0);
		waitcnt : integer range 0 to 511;
	end record event_t;
	type event_array_t is array (0 to 15) of event_t;
	signal events : event_array_t;
	

	-- internal signals
	signal evread_ready_i : std_logic := '0';

	-- skipped trigger counter
	signal skipped_triggers : integer range 0 to 15 := 0;
begin

-- full and empty flags
full <= '1' when evcnt = 15 else '0';
empty <= '1' when evcnt = 0 else '0';

-- generate output signals
process begin
	wait until rising_edge(clk);

	if evcnt = 0 then
		evread_bcid <= (others => '0');
		evread_l1id <= (others => '0');
		evread_hitocc <= (others => '0');
		evread_skipped <= (others => '0');
		evread_ready_i <= '0';
	else
		evread_bcid <= events(rdptr).bcid;
		evread_l1id <= events(rdptr).l1id;
		evread_hitocc <= events(rdptr).hitocc;
		evread_skipped <= events(rdptr).skipped;

		if (events(rdptr).waitcnt = 0) then
			evread_ready_i <= '1';
		else
			evread_ready_i <= '0';
		end if;
	end if;
end process;
evread_ready <= evread_ready_i;

-- read/write event data
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		evcnt <= 0;
		wrptr <= 0;
		wrptr_old <= 0;
		rdptr <= 0;
		skipped_triggers <= 0;
	else
		-- decrement all wait counters
		for I in 0 to 15 loop
			if events(I).waitcnt > 0 then
				events(I).waitcnt <= events(I).waitcnt - 1;
			end if;
		end loop;

		-- process incoming L1A
		if evwrite = '1' then
			if (evcnt = 15) and (evread = '0') then
				-- ignore trigger and count as skipped, but saturate at 15
				if skipped_triggers < 15 then
					skipped_triggers <= skipped_triggers + 1;
				end if;
			else
				-- skipped triggers will be reported in the "old" event
				events(wrptr_old).skipped <= std_logic_vector(to_unsigned(skipped_triggers, 4));

				-- write to event buffer
				events(wrptr).bcid <= evwrite_bcid;
				events(wrptr).l1id <= evwrite_l1id;
				events(wrptr).hitocc <= evwrite_hitocc;
				events(wrptr).skipped <= (others => '0');
				events(wrptr).waitcnt <= 21 * to_integer(unsigned(evwrite_hitocc)) / 16;
				if wrptr = 15 then
					wrptr <= 0;
				else
					wrptr <= wrptr + 1;
				end if;

				-- now we can reset the skipped triggers
				skipped_triggers <= 0;

				-- update old wrptr
				wrptr_old <= wrptr;
			end if;
		end if;

		-- process event reading
		if (evread = '1') and (evread_ready_i = '1') then
			if rdptr = 15 then
				rdptr <= 0;
			else
				rdptr <= rdptr + 1;
			end if;
		end if;

		-- count events in queue
		if (evwrite = '1') and ((evread = '0') or (evread_ready_i = '0')) and (evcnt < 15) then
			evcnt <= evcnt + 1;
		elsif (evwrite = '0') and ((evread = '1') and (evread_ready_i = '1')) and (evcnt > 0) then
			evcnt <= evcnt - 1;
		end if;
	end if;
end process;

end architecture;