library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity fe_cmd_decoder_pxl_v2 is
	port (
		clk_in : in std_logic;
		rst_n_in : in std_logic;
    	enable_in   : in std_logic;
    	ser_data_in : in std_logic; 
    	l1a_out     : out std_logic;
    	bcr_out     : out std_logic;
    	ecr_out     : out std_logic
	);
end fe_cmd_decoder_pxl_v2;

architecture rtl of fe_cmd_decoder_pxl_v2 is
	signal sr : std_logic_vector(12 downto 0) := (others => '0');
	signal in_datataking : std_logic := '0';
	signal l1a : std_logic := '0';
	signal bcr : std_logic := '0';
	signal bcr_delayed : std_logic := '0';
	signal ecr : std_logic := '0';
begin

process begin
	wait until rising_edge(clk_in);

	-- defaults
	l1a <= '0';
	bcr <= '0';
	ecr <= '0';

	if rst_n_in = '0' then
		in_datataking <= '0';
		sr <= (others => '0');
	else
		if (sr(4 downto 0) = "11101") then			-- trigger
			l1a <= '1';
			sr <= "000000000000" & ser_data_in;
		elsif (sr(8 downto 0) = "101100001") then	-- BCR
			bcr <= '1';
			sr <= "000000000000" & ser_data_in;
		elsif (sr(8 downto 0) = "101100010") then	-- ECR
			ecr <= '1';
			sr <= "000000000000" & ser_data_in;
		elsif (sr(12 downto 4) = "101101011") then	-- slow
			if sr(3 downto 0) = "1000" then			-- enDataTaking
				in_datataking <= '1';
			else
				in_datataking <= '0';
			end if;
			sr <= "000000000000" & ser_data_in;
		else
			-- shift in new data
			sr <= sr(11 downto 0) & ser_data_in;
		end if;
	end if;
end process;

-- output if enabled and in data taking
l1a_out <= (l1a and in_datataking and enable_in) when rising_edge(clk_in);
ecr_out <= (ecr and in_datataking and enable_in) when rising_edge(clk_in);
bcr_delayed <= (bcr and in_datataking and enable_in) when rising_edge(clk_in);		
bcr_out <= bcr_delayed when rising_edge(clk_in);

end architecture;
