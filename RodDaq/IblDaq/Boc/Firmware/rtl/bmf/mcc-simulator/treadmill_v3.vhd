library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity treadmill_v3 is
  port ( 
    clk_in      : in  STD_LOGIC;
    rst_n_in    : in  STD_LOGIC;
    mode        : in  STD_LOGIC_VECTOR ( 1 downto 0);
    dataword    : in  STD_LOGIC_VECTOR (23 downto 0);
    datalength  : in  STD_LOGIC_VECTOR ( 4 downto 0);
    output      : out STD_LOGIC_VECTOR ( 3 downto 0);
    acknowledge : out STD_LOGIC
    );
end treadmill_v3;

architecture Behavioral of treadmill_v3 is
	-- conversion of datalength input to integer (easier to read and work with)
	signal datalength_int : integer range 0 to 31;
	
	-- shift register and counter
	signal sr : std_logic_vector(31 downto 0) := (others => '0');
	signal srcnt : integer range 0 to 31 := 0;	
	
	-- helper signals
	signal dataword_isavailable : std_logic := '0';
	signal dataword_istrailer : std_logic := '0';
begin

-- convert datalength input to integer
datalength_int <= to_integer(unsigned(datalength));

-- shift register output
output <= sr(3 downto 0);

dataword_isavailable <= '0' when datalength_int = 0 else '1';
dataword_istrailer <= '1' when ((dataword = "000000000000000000000001") and (datalength_int = 23)) else '0';

-- generate acknowledge
acknowledge <= '1' when (dataword_isavailable = '1') and (srcnt <= 3) else '0';

-- shift register (+ filling)
process begin
	wait until rising_edge(clk_in);
	
	if rst_n_in = '0' then
		sr <= (others => '0');
		srcnt <= 0;
	else
		-- shift data
		if (mode = "01") then
			sr <= "00" & sr(sr'left downto 2);
		elsif (mode = "10") then
			sr <= "0000" & sr(sr'left downto 4);
		else
			sr <= '0' & sr(sr'left downto 1);
		end if;

		-- load new data if it is available
		if (dataword_isavailable = '1') and (srcnt <= 3) then
			sr(srcnt+23 downto srcnt) <= dataword;
			if (mode = "01") then
				if dataword_istrailer = '1' then
					if (srcnt = 3) or (srcnt = 2) then
						srcnt <= 24;
					else
						srcnt <= 22;
					end if;
				else
					srcnt <= srcnt + datalength_int - 2;
				end if;
			elsif (mode = "10") then
				if dataword_istrailer = '1' then
					if (srcnt = 3) or (srcnt = 2) then
						srcnt <= 24;
					else
						srcnt <= 20;
					end if;
				else
					srcnt <= srcnt + datalength_int - 4;
				end if;
			else
				srcnt <= srcnt + datalength_int - 1;
			end if;
		else
			if (mode = "01") then
				if srcnt > 2 then
					srcnt <= srcnt - 2;
				else
					srcnt <= 0;
				end if;
			elsif (mode = "10") then
				if srcnt > 4 then
					srcnt <= srcnt - 4;
				else
					srcnt <= 0;
				end if;
			else
				if srcnt > 1 then
					srcnt <= srcnt - 1;
				else
					srcnt <= 0;
				end if;
			end if;
		end if;
	end if;
end process;

end Behavioral;
