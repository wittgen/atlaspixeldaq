-- clock generation from external 40 MHz for the BMF
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

use work.bocpack.ALL;
use work.BuildVersion.ALL;
use work.lscpack.ALL;

entity bmf_clocking is
	port
	(
		-- clock input
		clk40_p : in std_logic;
		clk40_n : in std_logic;
		clk100_p : in std_logic;
		clk100_n : in std_logic;
		mgtrefclk_p : in std_logic;
		mgtrefclk_n : in std_logic;

		-- reset input
		reset : in std_logic;

		-- clock outputs
		clk_regbank : out std_logic;
		clk_tx : out std_logic;
		clk_tx_fast : out std_logic;
		clk_tx_fast_strobe : out std_logic;
		clk_rx : out std_logic;
		clk_rx_fast : out std_logic;
		clk_rx_fast_strobe : out std_logic;
		clk_backplane : out std_logic;
		clk_backplane_slink0 : out std_logic;
		clk_backplane_slink1 : out std_logic;
		clk_slink : out std_logic;

		-- lock loss counter
		lockloss_cnt : out std_logic_vector(7 downto 0);

		-- status outputs
		locked : out std_logic
	);
end bmf_clocking;

architecture rtl of bmf_clocking is
	-- input clocks
	signal clk40 : std_logic := '0';
	signal clk100 : std_logic := '0';
	signal mgtrefclk : std_logic := '0';

	-- PLL feedback
	signal pll_fbin : std_logic := '0';
	signal pll_fbout : std_logic := '0';

	-- PLL output clocks
	signal pll_clk_regbank : std_logic;
	signal pll_clk_backplane : std_logic;
	signal pll_clk_backplane_slink0 : std_logic;
	signal pll_clk_backplane_slink1 : std_logic;
	signal pll_clk_tx : std_logic;
	signal pll_clk_tx_fast : std_logic;
	signal pll_clk_tx_fast_strobe : std_logic;
	signal pll_clk_rx : std_logic;
	signal pll_clk_rx_fast : std_logic;
	signal pll_clk_rx_fast_strobe : std_logic;

	-- BUFPLL lock
	signal clk_tx_fast_lock : std_logic;
	signal clk_rx_fast_lock : std_logic;

	-- PLL locked
	signal pll_locked : std_logic;
	signal dcm_slink0_locked : std_logic;
	signal dcm_slink1_locked : std_logic;

	-- DCM reset for both slinks
	signal dcm_reset : std_logic;

	-- clocks after BUFG
	signal clk_regbank_i : std_logic;
	signal clk_backplane_i : std_logic;
	signal clk_backplane_slink0_i : std_logic;
	signal clk_backplane_slink1_i : std_logic;
	signal clk_rx_i : std_logic;
	signal clk_tx_i : std_logic;

	-- count lock losses
	signal lockloss_cnt_i : unsigned(7 downto 0) := (others => '0');
	signal lockloss_sr : std_logic_vector(2 downto 0) := "000";
begin

-------------------------------------
-- input buffers
-------------------------------------
clk40_buf: IBUFGDS
	generic map (
		DIFF_TERM => true
	)
	port map (
		I => clk40_p,
		IB => clk40_n,
		O => clk40
	);

clk100_buf: IBUFGDS
	generic map (
		DIFF_TERM => true
	)
	port map (
		I => clk100_p,
		IB => clk100_n,
		O => clk100
	);

mgtrefclk_buf: IBUFDS
	generic map (
		DIFF_TERM => true
	)
	port map (
		I => mgtrefclk_p,
		IB => mgtrefclk_n,
		O => mgtrefclk
	);

---------------------------------------
-- PLL feedback buffer
---------------------------------------
pll_fb_buf: BUFG port map (I => pll_fbout, O => pll_fbin);

ibl_pll: if PixelRx = false generate
	---------------------------------------
	-- PLL instance
	-- VCO frequency: 640 MHz
	-- Input clock: 40 MHz
	-- Output 0: 640 MHz
	-- Output 1: 320 MHz
	-- Output 2: 160 MHz
	-- Output 3: 40 MHz
	-- Output 4: 80 MHz
	-- Output 5: 40 MHz
	---------------------------------------
	pll: PLL_BASE
		generic map (
			BANDWIDTH            => "OPTIMIZED",
		    CLK_FEEDBACK         => "CLKFBOUT",
		    COMPENSATION         => "SYSTEM_SYNCHRONOUS",
		    DIVCLK_DIVIDE        => 1,
		    CLKFBOUT_MULT        => 16,
		    CLKFBOUT_PHASE       => 0.000,
		    CLKOUT0_DIVIDE       => 1,
		    CLKOUT0_PHASE        => 0.000,
		    CLKOUT0_DUTY_CYCLE   => 0.500,
		    CLKOUT1_DIVIDE       => 2,
		    CLKOUT1_PHASE        => 0.000,
		    CLKOUT1_DUTY_CYCLE   => 0.500,
		    CLKOUT2_DIVIDE       => 4,
		    CLKOUT2_PHASE        => 0.000,
		    CLKOUT2_DUTY_CYCLE   => 0.500,
		    CLKOUT3_DIVIDE       => 16,
		    CLKOUT3_PHASE        => 0.000,
		    CLKOUT3_DUTY_CYCLE   => 0.500,
		    CLKOUT4_DIVIDE       => 8,
		    CLKOUT4_PHASE        => 0.000,
		    CLKOUT4_DUTY_CYCLE   => 0.500,
		    CLKOUT5_DIVIDE       => 16,
		    CLKOUT5_PHASE        => 0.000,
		    CLKOUT5_DUTY_CYCLE   => 0.500,
		    CLKIN_PERIOD         => 25.000,
		    REF_JITTER           => 0.010
		)
		port map (
			CLKFBOUT            => pll_fbout,
	    	CLKOUT0             => pll_clk_rx_fast,
	    	CLKOUT1             => pll_clk_tx_fast,
	    	CLKOUT2             => pll_clk_rx,
	    	CLKOUT3             => pll_clk_tx,
	    	CLKOUT4             => pll_clk_backplane,
	    	CLKOUT5             => pll_clk_regbank,
	    	-- Status and control signals
	    	LOCKED              => pll_locked,
	    	RST                 => reset,
	    	-- Input clock control
	    	CLKFBIN             => pll_fbin,
	    	CLKIN               => clk40
		);
end generate;

pix_pll: if PixelRx = true generate
	---------------------------------------
	-- PLL instance
	-- VCO frequency: 640 MHz
	-- Input clock: 40 MHz
	-- Output 0: 320 MHz
	-- Output 1: 320 MHz
	-- Output 2: 40 MHz
	-- Output 3: 40 MHz
	-- Output 4: 40 MHz
	-- Output 5: 40 MHz
	---------------------------------------
	pll: PLL_BASE
		generic map (
			BANDWIDTH            => "OPTIMIZED",
		    CLK_FEEDBACK         => "CLKFBOUT",
		    COMPENSATION         => "SYSTEM_SYNCHRONOUS",
		    DIVCLK_DIVIDE        => 1,
		    CLKFBOUT_MULT        => 16,
		    CLKFBOUT_PHASE       => 0.000,
		    CLKOUT0_DIVIDE       => 2,
		    CLKOUT0_PHASE        => 0.000,
		    CLKOUT0_DUTY_CYCLE   => 0.500,
		    CLKOUT1_DIVIDE       => 2,
		    CLKOUT1_PHASE        => 0.000,
		    CLKOUT1_DUTY_CYCLE   => 0.500,
		    CLKOUT2_DIVIDE       => 16,
		    CLKOUT2_PHASE        => 0.000,
		    CLKOUT2_DUTY_CYCLE   => 0.500,
		    CLKOUT3_DIVIDE       => 16,
		    CLKOUT3_PHASE        => 0.000,
		    CLKOUT3_DUTY_CYCLE   => 0.500,
		    CLKOUT4_DIVIDE       => 16,
		    CLKOUT4_PHASE        => 0.000,
		    CLKOUT4_DUTY_CYCLE   => 0.500,
		    CLKOUT5_DIVIDE       => 16,
		    CLKOUT5_PHASE        => 0.000,
		    CLKOUT5_DUTY_CYCLE   => 0.500,
		    CLKIN_PERIOD         => 25.000,
		    REF_JITTER           => 0.010
		)
		port map (
			CLKFBOUT            => pll_fbout,
	    	CLKOUT0             => pll_clk_rx_fast,
	    	CLKOUT1             => pll_clk_tx_fast,
	    	CLKOUT2             => pll_clk_rx,
	    	CLKOUT3             => pll_clk_tx,
	    	CLKOUT4             => pll_clk_backplane,
	    	CLKOUT5             => pll_clk_regbank,
	    	-- Status and control signals
	    	LOCKED              => pll_locked,
	    	RST                 => reset,
	    	-- Input clock control
	    	CLKFBIN             => pll_fbin,
	    	CLKIN               => clk40
		);
end generate;

------------------------
-- DCM for SLINK0
------------------------
slink0_dcm: DCM_SP
	generic map (
		CLKDV_DIVIDE => 2.0,
		CLKFX_DIVIDE => 1,
		CLKFX_MULTIPLY => 4,
		CLKIN_DIVIDE_BY_2 => false,
		CLKIN_PERIOD => 25.0,
		CLKOUT_PHASE_SHIFT => "FIXED",
		CLK_FEEDBACK => "1X",
		DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
		STARTUP_WAIT => false,
		PHASE_SHIFT => DCM_SLINK0_PHASE
	)
	port map (
		-- input clocks
		CLKIN => clk_regbank_i,				-- this is always 40 MHz
		CLKFB => clk_backplane_slink0_i,

		-- output clocks
		CLK0 => pll_clk_backplane_slink0,
		CLK90 => open,
		CLK180 => open,
		CLK270 => open,
		CLK2X => open,
		CLK2X180 => open,
		CLKDV => open,
		CLKFX => open,
		CLKFX180 => open,

		-- status and reset
		LOCKED => dcm_slink0_locked,
		STATUS => open,
		DSSEN => '0',
		RST => dcm_reset,


		-- dynamic phase shift interface
		PSINCDEC => '0',
		PSEN => '0',
		PSCLK => '0',
		PSDONE => open
	);

------------------------
-- DCM for SLINK1
------------------------
slink1_dcm: DCM_SP
	generic map (
		CLKDV_DIVIDE => 2.0,
		CLKFX_DIVIDE => 1,
		CLKFX_MULTIPLY => 4,
		CLKIN_DIVIDE_BY_2 => false,
		CLKIN_PERIOD => 25.0,
		CLKOUT_PHASE_SHIFT => "FIXED",
		CLK_FEEDBACK => "1X",
		DESKEW_ADJUST => "SYSTEM_SYNCHRONOUS",
		STARTUP_WAIT => false,
		PHASE_SHIFT => DCM_SLINK1_PHASE
	)
	port map (
		-- input clocks
		CLKIN => clk_regbank_i,				-- this is always 40 MHz
		CLKFB => clk_backplane_slink1_i,

		-- output clocks
		CLK0 => pll_clk_backplane_slink1,
		CLK90 => open,
		CLK180 => open,
		CLK270 => open,
		CLK2X => open,
		CLK2X180 => open,
		CLKDV => open,
		CLKFX => open,
		CLKFX180 => open,

		-- status and reset
		LOCKED => dcm_slink1_locked,
		STATUS => open,
		DSSEN => '0',
		RST => dcm_reset,


		-- dynamic phase shift interface
		PSINCDEC => '0',
		PSEN => '0',
		PSCLK => '0',
		PSDONE => open
	);


------------------------
-- BUFG
------------------------
clk_rx_buf: BUFG port map (I => pll_clk_rx, O => clk_rx_i);
clk_tx_buf: BUFG port map (I => pll_clk_tx, O => clk_tx_i);
clk_backplane_buf: BUFG port map (I => pll_clk_backplane, O => clk_backplane_i);
clk_regbank_buf: BUFG port map (I => pll_clk_regbank, O => clk_regbank_i);
clk_backplane_slink0_buf: BUFG port map (I => pll_clk_backplane_slink0, O => clk_backplane_slink0_i);
clk_backplane_slink1_buf: BUFG port map (I => pll_clk_backplane_slink1, O => clk_backplane_slink1_i);

------------------------
-- BUFPLL
------------------------
ibl_rxbuf: if PixelRx = false generate
	clk_rx_fast_buf: BUFPLL
		generic map (
			DIVIDE => 4,
			ENABLE_SYNC => TRUE
		)
		port map (
			IOCLK => clk_rx_fast,
			LOCK => clk_rx_fast_lock,
			SERDESSTROBE => clk_rx_fast_strobe,
			GCLK => clk_rx_i,
			LOCKED => pll_locked,
			PLLIN => pll_clk_rx_fast
		);
end generate;

pix_rxbuf: if PixelRx = true generate
	clk_rx_fast_buf: BUFPLL
		generic map (
			DIVIDE => 8,
			ENABLE_SYNC => TRUE
		)
		port map (
			IOCLK => clk_rx_fast,
			LOCK => clk_rx_fast_lock,
			SERDESSTROBE => clk_rx_fast_strobe,
			GCLK => clk_rx_i,
			LOCKED => pll_locked,
			PLLIN => pll_clk_rx_fast
		);
end generate;

clk_tx_fast_buf: BUFPLL
	generic map (
		DIVIDE => 8,
		ENABLE_SYNC => TRUE
	)
	port map (
		IOCLK => clk_tx_fast,
		LOCK => clk_tx_fast_lock,
		SERDESSTROBE => clk_tx_fast_strobe,
		GCLK => clk_tx_i,
		LOCKED => pll_locked,
		PLLIN => pll_clk_tx_fast
	);

-------------------------------
-- output internal clocks
-------------------------------
clk_regbank <= clk_regbank_i;
clk_backplane <= clk_backplane_i;
clk_backplane_slink0 <= clk_backplane_slink0_i;
clk_backplane_slink1 <= clk_backplane_slink1_i;
clk_rx <= clk_rx_i;
clk_tx <= clk_tx_i;
clk_slink <= mgtrefclk;

----------------------------------
-- lock loss counter
----------------------------------

-- count lock losses of the PLLs (with independent board clock)
lockloss_sr <= (lockloss_sr(1 downto 0) & pll_locked) when rising_edge(clk100);
process begin
	wait until rising_edge(clk100);

	if lockloss_sr(2 downto 1) = "10" then
		lockloss_cnt_i <= lockloss_cnt_i + 1;
	end if;
end process;

-- output lock loss counter synchronous to 40 MHz
lockloss_cnt <= std_logic_vector(lockloss_cnt_i) when rising_edge(clk_regbank_i);
locked <= pll_locked and dcm_slink0_locked and dcm_slink1_locked and clk_tx_fast_lock and clk_rx_fast_lock;
dcm_reset <= not pll_locked;

end architecture;
