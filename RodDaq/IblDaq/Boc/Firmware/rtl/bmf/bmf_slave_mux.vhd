-- BMF slave multiplexer + register bank
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

use work.bocpack.all;

entity bmf_slave_mux is
	port
	(
		clk : in std_logic;
		reset : in std_logic;

		wb_dat_i : in std_logic_vector(7 downto 0);
		wb_dat_o : out std_logic_vector(7 downto 0);
		wb_adr : in std_logic_vector(15 downto 0);
		wb_cyc : in std_logic;
		wb_stb : in std_logic;
		wb_we : in std_logic;
		wb_ack : out std_logic;

		rx_regs_wr : out RegBank_wr_t(0 to 1023);
		rx_regs_rd : in RegBank_rd_t(0 to 1023);
		rx_regs_wren : out RegBank_wren_t(0 to 1023);
		rx_regs_rden : out RegBank_rden_t(0 to 1023);

		tx_regs_wr : out RegBank_wr_t(0 to 1023);
		tx_regs_rd : in RegBank_rd_t(0 to 1023);
		tx_regs_wren : out RegBank_wren_t(0 to 1023);
		tx_regs_rden : out RegBank_rden_t(0 to 1023);

		com_regs_wr : out RegBank_wr_t(0 to 255);
		com_regs_rd : in RegBank_rd_t(0 to 255);
		com_regs_wren : out RegBank_wren_t(0 to 255);
		com_regs_rden : out RegBank_rden_t(0 to 255)
	);
end entity;

architecture rtl of bmf_slave_mux is
	component BOCRegBank
		generic
		(
			-- number of registers
			nReg : natural := 32
		);
		port
		(
			-- clock and reset
			clk_i : in std_logic := '0';
			rst_i : in std_logic := '0';
		
			-- wishbone interface
			wb_dat_i : in std_logic_vector(7 downto 0) := (others => '0');
			wb_dat_o : out std_logic_vector(7 downto 0) := (others => '0');
			wb_adr_i : in std_logic_vector(15 downto 0) := (others => '0');
			wb_cyc_i : in std_logic := '0';
			wb_stb_i : in std_logic := '0';
			wb_we_i : in std_logic := '0';
			wb_ack_o : out std_logic := '0';
		
			-- register interface
			reg_wr : out RegBank_wr_t;
			reg_wren : out RegBank_wren_t;
			reg_rd : in RegBank_rd_t;
			reg_rden : out RegBank_rden_t
		);
	end component;

	signal rx_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal rx_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal rx_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal rx_cyc : std_logic := '0';
	signal rx_stb : std_logic := '0';
	signal rx_we : std_logic := '0';
	signal rx_ack : std_logic := '0';

	signal tx_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal tx_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal tx_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal tx_cyc : std_logic := '0';
	signal tx_stb : std_logic := '0';
	signal tx_we : std_logic := '0';
	signal tx_ack : std_logic := '0';

	signal com_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal com_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal com_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal com_cyc : std_logic := '0';
	signal com_stb : std_logic := '0';
	signal com_we : std_logic := '0';
	signal com_ack : std_logic := '0';

	signal dummy_ack : std_logic := '0';
	signal dummy_stb : std_logic := '0';
	signal dummy_cyc : std_logic := '0';
begin

com_adr <= "000000" & wb_adr(9 downto 0);
rx_adr <= "000000" & wb_adr(9 downto 0);
tx_adr <= "000000" & wb_adr(9 downto 0);
com_we <= wb_we;
rx_we <= wb_we;
tx_we <= wb_we;
com_dat_i <= wb_dat_i;
rx_dat_i <= wb_dat_i;
tx_dat_i <= wb_dat_i;

-- multiplexer
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		rx_cyc <= '0';
		rx_stb <= '0';
		tx_cyc <= '0';
		tx_stb <= '0';
		com_cyc <= '0';
		com_stb <= '0';
		wb_dat_o <= (others => '0');
		wb_ack <= '0';
	else
		case wb_adr(11 downto 10) is
			when "00" =>
				rx_cyc <= '0';
				rx_stb <= '0';
				tx_cyc <= '0';
				tx_stb <= '0';
				com_cyc <= wb_cyc;
				com_stb <= wb_stb;
				dummy_cyc <= '0';
				dummy_stb <= '0';
				wb_dat_o <= com_dat_o;
				wb_ack <= com_ack;

			when "10" =>
				rx_cyc <= wb_cyc;
				rx_stb <= wb_stb;
				tx_cyc <= '0';
				tx_stb <= '0';
				com_cyc <= '0';
				com_stb <= '0';
				dummy_cyc <= '0';
				dummy_stb <= '0';
				wb_dat_o <= rx_dat_o;
				wb_ack <= rx_ack;

			when "11" =>
				rx_cyc <= '0';
				rx_stb <= '0';
				tx_cyc <= wb_cyc;
				tx_stb <= wb_stb;
				com_cyc <= '0';
				com_stb <= '0';
				dummy_cyc <= '0';
				dummy_stb <= '0';
				wb_dat_o <= tx_dat_o;
				wb_ack <= tx_ack;

			when others =>
				rx_cyc <= '0';
				rx_stb <= '0';
				tx_cyc <= '0';
				tx_stb <= '0';
				com_cyc <= '0';
				com_stb <= '0';
				dummy_cyc <= wb_cyc;
				dummy_stb <= wb_stb;
				wb_dat_o <= (others => '1');
				wb_ack <= dummy_ack;
		end case;
	end if;
end process;

rx_regs: BOCRegBank
	GENERIC MAP(
		nReg => 1024
	)
	PORT MAP(
		clk_i => clk,
		rst_i => reset,
		wb_dat_i => rx_dat_i,
		wb_dat_o => rx_dat_o,
		wb_adr_i => rx_adr,
		wb_cyc_i => rx_cyc,
		wb_stb_i => rx_stb,
		wb_we_i => rx_we,
		wb_ack_o => rx_ack,
		reg_wr => rx_regs_wr,
		reg_rd => rx_regs_rd,
		reg_wren => rx_regs_wren,
		reg_rden => rx_regs_rden
	);

tx_regs: BOCRegBank
	GENERIC MAP(
		nReg => 1024
	)
	PORT MAP(
		clk_i => clk,
		rst_i => reset,
		wb_dat_i => tx_dat_i,
		wb_dat_o => tx_dat_o,
		wb_adr_i => tx_adr,
		wb_cyc_i => tx_cyc,
		wb_stb_i => tx_stb,
		wb_we_i => tx_we,
		wb_ack_o => tx_ack,
		reg_wr => tx_regs_wr,
		reg_rd => tx_regs_rd,
		reg_wren => tx_regs_wren,
		reg_rden => tx_regs_rden
	);

com_regs: BOCRegBank
	GENERIC MAP(
		nReg => 256
	)
	PORT MAP(
		clk_i => clk,
		rst_i => reset,
		wb_dat_i => com_dat_i,
		wb_dat_o => com_dat_o,
		wb_adr_i => com_adr,
		wb_cyc_i => com_cyc,
		wb_stb_i => com_stb,
		wb_we_i => com_we,
		wb_ack_o => com_ack,
		reg_wr => com_regs_wr,
		reg_rd => com_regs_rd,
		reg_wren => com_regs_wren,
		reg_rden => com_regs_rden
	);


-- dummy slave
process begin
	wait until rising_edge(clk);

	dummy_ack <= dummy_stb and dummy_cyc;
end process;


end architecture;
