library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library decode_8b10b;

entity sim_testrx is
	port (
		clk160 : in std_logic;
		reset : in std_logic;

		din : in std_logic;

		dout : out std_logic_vector(7 downto 0);
		kchar : out std_logic;
		valid : out std_logic;

		sync : out std_logic;
		decerr : out std_logic;
		disperr : out std_logic
	);
end sim_testrx;

architecture rtl of sim_testrx is
	component data_alignment
		port
		(
			clk : in std_logic;
			reset : in std_logic;
		
			din : in std_logic_vector(1 downto 0);
			din_valid : in std_logic_vector(1 downto 0);
		
			dout : out std_logic_vector(9 downto 0);
			dout_valid : out std_logic;
			dout_sync : out std_logic
		);
	end component;

	COMPONENT decode_8b10b_wrapper
	PORT(
		CLK : IN std_logic;
		DIN : IN std_logic_vector(9 downto 0);
		CE : IN std_logic;
		SINIT : IN std_logic;          
		DOUT : OUT std_logic_vector(7 downto 0);
		KOUT : OUT std_logic;
		CODE_ERR : OUT std_logic;
		DISP_ERR : OUT std_logic;
		ND : OUT std_logic
		);
	END COMPONENT;

	signal sync_i : std_logic := '0';
	signal align_dout : std_logic_vector(9 downto 0) := (others => '0');
	signal align_dout_valid : std_logic := '0';
	signal dec_din : std_logic_vector(9 downto 0) := (others => '0');
	signal dec_din_valid : std_logic := '0';
begin

align_i: data_alignment
	port map (
		clk => clk160,
		reset => reset,
		din(1) => '0',
		din(0) => din,
		din_valid => "01",
		dout => align_dout,
		dout_valid => align_dout_valid,
		dout_sync => sync_i
	);

rev: for I in 0 to 9 generate
	dec_din(I) <= align_dout(9-I);
end generate;

sync <= sync_i;
dec_din_valid <= align_dout_valid and sync_i;

dec_i: decode_8b10b_wrapper
	port map (
		clk => clk160,
		din => dec_din,
		ce => dec_din_valid,
		sinit => '0',
		dout => dout,
		kout => kchar,
		code_err => decerr,
		disp_err => disperr,
		nd => open
	);

valid <= dec_din_valid when rising_edge(clk160);

end architecture ; -- rtl
