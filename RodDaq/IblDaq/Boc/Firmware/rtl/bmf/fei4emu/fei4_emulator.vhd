library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity fei4_emulator is
	generic (
		DEFAULT_CHIPID : integer := 0;
		DEFAULT_DINSEL : integer := 0
	);
	port (
		-- clocking and reset
		clk40 : in std_logic;
		clk160 : in std_logic;
		reset : in std_logic;

		-- serial data input (synchronous with clk40)
		din : in std_logic_vector(15 downto 0);

		-- serial data output (synchronous with clk160)
		dout : out std_logic;

		-- register interface to BMF register bank
		control_rd : out std_logic_vector(7 downto 0);
		control_wr : in std_logic_vector(7 downto 0);
		control_rden : in std_logic;
		control_wren : in std_logic;
		status_rd : out std_logic_vector(7 downto 0);
		status_wr : in std_logic_vector(7 downto 0);
		status_rden : in std_logic;
		status_wren : in std_logic;
		extra_frames_rd : out std_logic_vector(7 downto 0);
		extra_frames_wr : in std_logic_vector(7 downto 0);
		extra_frames_rden : in std_logic;
		extra_frames_wren : in std_logic;
		control2_rd : out std_logic_vector(7 downto 0);
		control2_wr : in std_logic_vector(7 downto 0);
		control2_rden : in std_logic;
		control2_wren : in std_logic;
		hit_wr : in std_logic_vector(7 downto 0);
		hit_rd : out std_logic_vector(7 downto 0);
		hit_wren : in std_logic;
		hit_rden : in std_logic;
		hitcount_wr : in std_logic_vector(7 downto 0);
		hitcount_rd : out std_logic_vector(7 downto 0);
		hitcount_wren : in std_logic;
		hitcount_rden : in std_logic
  	);
end entity ; -- fei4_emulator

architecture rtl of fei4_emulator is

-- components
component fei4_regbank
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- register bank access
		reg_clear : in std_logic;
		reg_addr : in std_logic_vector(5 downto 0);
		reg_din : in std_logic_vector(15 downto 0);
		reg_dout : out std_logic_vector(15 downto 0);
		reg_wren : in std_logic;

		-- trigger multiplication value
		trig_count : out std_logic_vector(3 downto 0);

		-- serial number for readback
		serial_number : in std_logic_vector(15 downto 0);

		-- delay values for trigger delay
		trig_delay : out std_logic_vector(4 downto 0);

		-- enable SR14
		sr14_enable : out std_logic
	);
end component;

component fei4_cmd_decoder
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- serial data in (synchronous to 40 MHz clock)
		din : in std_logic;

		-- command outputs
		cmd_lv1 : out std_logic;
		cmd_bcr : out std_logic;
		cmd_ecr : out std_logic;
		cmd_cal : out std_logic;
		cmd_rdreg : out std_logic;
		cmd_wrreg : out std_logic;
		cmd_wrfrontend : out std_logic;
		cmd_greset : out std_logic;
		cmd_gpulse : out std_logic;
		cmd_runmode : out std_logic;

		-- parameter outputs
		par_address : out std_logic_vector(5 downto 0);
		par_runmode : out std_logic;
		par_data : out std_logic_vector(15 downto 0);
		par_width : out std_logic_vector(5 downto 0);

		-- chipid / runmode input
		runmode : in std_logic;
		chipid : in std_logic_vector(3 downto 0)
	);
end component;

component fei4_dataout
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		-- command/data inputs
		cmd_lv1 : in std_logic;
		cmd_rdreg : in std_logic;
		reg_addr : in std_logic_vector(5 downto 0);
		reg_data : in std_logic_vector(15 downto 0);

		-- counters
		cnt_bcr : in std_logic_vector(12 downto 0);
		cnt_ecr : in std_logic_vector(11 downto 0);

		-- hit fifo
		hitfifo_data : in std_logic_vector(7 downto 0);
		hitfifo_rden : out std_logic;
		hitfifo_empty : in std_logic;
		hitfifo_armed : in std_logic;

		-- configuration
		hit_mode : in std_logic;
		hitcount : in std_logic_vector(7 downto 0);
		trig_count : in std_logic_vector(3 downto 0);
		sr14_enable : in std_logic;
		extra_frames : in std_logic_vector(7 downto 0);

		-- hit pool
		hitpool_data : in std_logic_vector(23 downto 0);
		hitpool_rden : out std_logic;
		hitpool_empty : in std_logic;

		-- data output (not encoded, parallel)
		dout : out std_logic_vector(7 downto 0);
		kchar : out std_logic;
		valid : out std_logic;
		xoff : in std_logic
	);
end component;

component fei4_counters
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		cmd_bcr : in std_logic;
		cmd_ecr : in std_logic;
		cmd_lv1 : in std_logic;

		cnt_bcr : out std_logic_vector(12 downto 0);
		cnt_ecr : out std_logic_vector(11 downto 0)
	);
end component;

component fei4_encoding
	port (
		-- clocking and reset
		clk40 : in std_logic;
		clk160 : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic_vector(7 downto 0);
		din_valid : in std_logic;
		din_kchar : in std_logic;
		din_xoff : out std_logic;

		-- data output
		dout : out std_logic
	);
end component;

component fei4_hitgen
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- hitfifo interface
		fifo_data : out std_logic_vector(23 downto 0);
		fifo_wren : out std_logic;
		fifo_full : in std_logic;

		-- tot value
		tot_value : in std_logic_vector(7 downto 0)
	);
end component;

COMPONENT fei4emu_hitfifo
  PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

COMPONENT fei4emu_hitpool
  PORT (
    clk : IN STD_LOGIC;
    srst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(23 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(23 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

-- input selection
signal din_selected : std_logic := '0';
signal din_sel : std_logic_vector(3 downto 0) := std_logic_vector(to_unsigned(DEFAULT_DINSEL, 4));

-- internal enable and reset
signal enable : std_logic := '0';
signal int_reset : std_logic := '0';

-- command decoder
signal cmd_lv1 : std_logic := '0';
signal cmd_lv1_delayed : std_logic := '0';
signal cmd_bcr : std_logic := '0';
signal cmd_ecr : std_logic := '0';
signal cmd_cal : std_logic := '0';
signal cmd_rdreg : std_logic := '0';
signal cmd_rdreg_delayed : std_logic := '0';
signal cmd_wrreg : std_logic := '0';
signal cmd_wrfrontend : std_logic := '0';
signal cmd_greset : std_logic := '0';
signal cmd_gpulse : std_logic := '0';
signal cmd_runmode : std_logic := '0';
signal par_address : std_logic_vector(5 downto 0) := (others => '0');
signal par_runmode : std_logic := '0';
signal par_data : std_logic_vector(15 downto 0) := (others => '0');
signal par_width : std_logic_vector(5 downto 0) := (others => '0');

-- register bank
signal reg_clear : std_logic := '0';
signal reg_addr : std_logic_vector(5 downto 0) := (others => '0');
signal reg_din : std_logic_vector(15 downto 0) := (others => '0');
signal reg_dout : std_logic_vector(15 downto 0) := (others => '0');
signal reg_wren : std_logic := '0';

-- counters
signal cnt_ecr : std_logic_vector(11 downto 0) := (others => '0');
signal cnt_bcr : std_logic_vector(12 downto 0) := (others => '0');

-- signals of dataout
signal dataout_dout : std_logic_vector(7 downto 0) := (others => '0');
signal dataout_kchar : std_logic := '0';
signal dataout_valid : std_logic := '0';
signal dataout_xoff : std_logic := '0';

-- configuration registers
signal serial_number : std_logic_vector(15 downto 0) := x"0000";
signal chipid : std_logic_vector(3 downto 0) := std_logic_vector(to_unsigned(DEFAULT_CHIPID, 4));
signal runmode : std_logic := '0';
signal hit_mode : std_logic := '0';		-- hit generator (0 = manual injection, 1 = automatic injection)
signal hitcount : std_logic_vector(7 downto 0) := "00000001";
signal trig_count : std_logic_vector(3 downto 0) := "0000";
signal trig_delay : std_logic_vector(4 downto 0) := "00000";
signal sr14_enable : std_logic := '0';

-- temporary registers for reading the counters
signal tempreg1 : std_logic_vector(7 downto 0) := "00000000";
signal tempreg2 : std_logic_vector(7 downto 0) := "00000000";

-- hit-fifo signals
signal hitfifo_dout : std_logic_vector(7 downto 0) := (others => '0');
signal hitfifo_din : std_logic_vector(7 downto 0) := (others => '0');
signal hitfifo_rden : std_logic := '0';
signal hitfifo_wren : std_logic := '0';
signal hitfifo_full : std_logic := '0';
signal hitfifo_empty : std_logic := '0';
signal hitfifo_armed : std_logic := '0';

-- hit-pool
signal hitpool_dout : std_logic_vector(23 downto 0) := (others => '0');
signal hitpool_din : std_logic_vector(23 downto 0) := (others => '0');
signal hitpool_rden : std_logic := '0';
signal hitpool_wren : std_logic := '0';
signal hitpool_full : std_logic := '0';
signal hitpool_empty : std_logic := '0';

-- extra frames setting
signal extra_frames : std_logic_vector(7 downto 0) := (others => '0');

-- tot value
signal tot_value : std_logic_vector(7 downto 0) := (others => '0');

begin

-- select input
din_selected <= din(to_integer(unsigned(din_sel))) when rising_edge(clk40);

regbank_i: fei4_regbank
	port map (
		clk40 => clk40,
		reset => int_reset,
		reg_clear => reg_clear,
		reg_addr => reg_addr,
		reg_din => reg_din,
		reg_dout => reg_dout,
		reg_wren => reg_wren,
		trig_count => trig_count,
		serial_number => serial_number,
		sr14_enable => sr14_enable
	);

cmd_decoder_i: fei4_cmd_decoder
	port map (
		clk40 => clk40,
		reset => int_reset,
		din => din_selected,
		cmd_lv1 => cmd_lv1,
		cmd_bcr => cmd_bcr,
		cmd_ecr => cmd_ecr,
		cmd_cal => cmd_cal,
		cmd_rdreg => cmd_rdreg,
		cmd_wrreg => cmd_wrreg,
		cmd_wrfrontend => cmd_wrfrontend,
		cmd_greset => cmd_greset,
		cmd_gpulse => cmd_gpulse,
		cmd_runmode => cmd_runmode,
		par_address => par_address,
		par_runmode => par_runmode,
		par_data => par_data,
		par_width => par_width,
		runmode => runmode,
		chipid => chipid
	);

counters_i: fei4_counters
	port map (
		clk40 => clk40,
		reset => int_reset,
		cmd_bcr => cmd_bcr,
		cmd_ecr => cmd_ecr,
		cmd_lv1 => cmd_lv1_delayed,
		cnt_bcr => cnt_bcr,
		cnt_ecr => cnt_ecr
	);

dataout_i: fei4_dataout
	port map (
		clk40 => clk40,
		reset => int_reset,
		cmd_lv1 => cmd_lv1_delayed,
		cmd_rdreg => cmd_rdreg_delayed,
		reg_addr => reg_addr,
		reg_data => reg_dout,
		cnt_bcr => cnt_bcr,
		cnt_ecr => cnt_ecr,
		hitfifo_data => hitfifo_dout,
		hitfifo_rden => hitfifo_rden,
		hitfifo_empty => hitfifo_empty,
		hitfifo_armed => hitfifo_armed,
		hit_mode => hit_mode,
		hitcount => hitcount,
		trig_count => trig_count,
		sr14_enable => sr14_enable,
		extra_frames => extra_frames,
		hitpool_data => hitpool_dout,
		hitpool_rden => hitpool_rden,
		hitpool_empty => hitpool_empty,
		dout => dataout_dout,
		kchar => dataout_kchar,
		valid => dataout_valid,
		xoff => dataout_xoff
	);

encoding_i: fei4_encoding
	port map (
		clk40 => clk40,
		clk160 => clk160,
		reset => int_reset,
		din => dataout_dout,
		din_kchar => dataout_kchar,
		din_valid => dataout_valid,
		din_xoff => dataout_xoff,
		dout => dout
	);

hitgen_i: fei4_hitgen
	port map (
		clk40 => clk40,
		reset => reset,
		fifo_data => hitpool_din,
		fifo_wren => hitpool_wren,
		fifo_full => hitpool_full,
		tot_value => tot_value
	);

hitfifo_i: fei4emu_hitfifo
	port map (
		clk => clk40,
		srst => reset,
		din => hitfifo_din,
		dout => hitfifo_dout,
		wr_en => hitfifo_wren,
		rd_en => hitfifo_rden,
		full => hitfifo_full,
		empty => hitfifo_empty
	);

hitpool_i: fei4emu_hitpool
	port map (
		clk => clk40,
		srst => reset,
		din => hitpool_din,
		dout => hitpool_dout,
		wr_en => hitpool_wren,
		rd_en => hitpool_rden,
		full => hitpool_full,
		empty => hitpool_empty
	);

-- map wrreg command to register bank
reg_clear <= cmd_greset;
reg_addr <= par_address;
reg_din <= par_data;
reg_wren <= cmd_wrreg;

-- delay rdreg by 1 cycle
cmd_rdreg_delayed <= cmd_rdreg when rising_edge(clk40);

-- SRL
trig_delay_srl: SRLC32E
	generic map (
		INIT => x"00000000"
	)
	port map (
		Q => cmd_lv1_delayed,
		Q31 => open,
		A => trig_delay,
		CE => '1',
		CLK => clk40,
		D => cmd_lv1
	);

-- map internal reset
int_reset <= (reset or not enable) when rising_edge(clk40);

-- register readout
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		enable <= '0';
		chipid <= std_logic_vector(to_unsigned(DEFAULT_CHIPID, 4));
		hit_mode <= '0';
		hitfifo_armed <= '0';
		hitcount <= "00000001";
		tot_value <= "01011111";
		extra_frames <= x"00";
		din_sel <= std_logic_vector(to_unsigned(DEFAULT_DINSEL, 4));
	else
		if control_wren = '1' then
			chipid <= control_wr(7 downto 4);
			hit_mode <= control_wr(2);
			hitfifo_armed <= control_wr(1);
			enable <= control_wr(0);
		end if;

		if control2_wren = '1' then
			tot_value <= control2_wr;
		end if;

		if status_wren = '1' then
			din_sel <= status_wr(7 downto 4);
		end if;

		if extra_frames_wren = '1' then
			extra_frames <= extra_frames_wr;
		end if;

		if hit_wren = '1' then
			-- automatically disarm hitfifo on write
			hitfifo_armed <= '0';
		end if;

		if hitcount_wren = '1' then
			hitcount <= hitcount_wr;
		end if;
	end if;
end process;

control_rd <= chipid & "0" & hit_mode & hitfifo_armed & enable;
control2_rd <= tot_value;
status_rd <= din_sel & "0" & hitfifo_empty & hitfifo_full & runmode;
extra_frames_rd <= extra_frames;
hitcount_rd <= hitcount;

-- switch runmode
process begin
	wait until rising_edge(clk40);

	if (int_reset = '1') or (cmd_greset = '1') then
		runmode <= '0';
	else
		if cmd_runmode = '1' then
			runmode <= par_runmode;
		end if;
	end if;
end process;

-- write to hitfifo
hitfifo_din <= hit_wr;
hitfifo_wren <= hit_wren and not hitfifo_full;

end architecture ; -- rtl