library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_regbank is
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- register bank access
		reg_clear : in std_logic;
		reg_addr : in std_logic_vector(5 downto 0);
		reg_din : in std_logic_vector(15 downto 0);
		reg_dout : out std_logic_vector(15 downto 0);
		reg_wren : in std_logic;

		-- trigger multiplication value
		trig_count : out std_logic_vector(3 downto 0);

		-- serial number for readback
		serial_number : in std_logic_vector(15 downto 0);

		-- delay values for trigger delay and frame delay
		trig_delay : out std_logic_vector(4 downto 0);

		-- enable SR14
		sr14_enable : out std_logic
	);
end fei4_regbank;

architecture rtl of fei4_regbank is
	-- register bank memory
	type regbank_t is array (0 to 34) of std_logic_vector(15 downto 0);
	signal regbank : regbank_t := (others => (others => '0'));
begin

process
	variable reg_addr_int : integer range 0 to 63;
begin
	wait until rising_edge(clk40);

	reg_addr_int := to_integer(unsigned(reg_addr));

	if (reset = '1') or (reg_clear = '1') then
		regbank <= (others => (others => '0'));
	else
		if (reg_addr_int < 34) then
			reg_dout <= regbank(reg_addr_int);
		elsif reg_addr_int = 35 then
			reg_dout <= serial_number;
		else
			reg_dout <= x"0000";
		end if;

		if (reg_wren = '1') and (reg_addr_int < 34) then
			regbank(reg_addr_int) <= reg_din;
		end if;
	end if;
end process;

-- output trigger count
trig_count <= regbank(2)(15 downto 12);

-- delays
trig_delay <= regbank(32)(4 downto 0);

-- enable SR14
sr14_enable <= not regbank(3)(14);

end architecture;