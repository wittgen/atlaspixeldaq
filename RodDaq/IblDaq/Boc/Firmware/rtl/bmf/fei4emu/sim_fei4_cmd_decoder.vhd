library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sim_fei4_cmd_decoder is
end entity ; -- sim_fei4_cmd_decoder

architecture simulation of sim_fei4_cmd_decoder is

signal clk40 : std_logic := '0';
signal reset : std_logic := '0';

signal din : std_logic := '0';

signal cmd_lv1 : std_logic := '0';
signal cmd_bcr : std_logic := '0';
signal cmd_ecr : std_logic := '0';
signal cmd_cal : std_logic := '0';
signal cmd_rdreg : std_logic := '0';
signal cmd_wrreg : std_logic := '0';
signal cmd_wrfrontend : std_logic := '0';
signal cmd_greset : std_logic := '0';
signal cmd_gpulse : std_logic := '0';
signal cmd_runmode : std_logic := '0';

signal par_chipid : std_logic_vector(3 downto 0) := "0000";
signal par_address : std_logic_vector(5 downto 0) := "000000";
signal par_runmode : std_logic := '0';
signal par_data : std_logic_vector(15 downto 0) := x"0000";
signal par_srdata : std_logic_vector(671 downto 0) := (others => '0');
signal par_width : std_logic_vector(5 downto 0) := "000000";

component fei4_cmd_decoder
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- serial data in (synchronous to 40 MHz clock)
		din : in std_logic;

		-- command outputs
		cmd_lv1 : out std_logic;
		cmd_bcr : out std_logic;
		cmd_ecr : out std_logic;
		cmd_cal : out std_logic;
		cmd_rdreg : out std_logic;
		cmd_wrreg : out std_logic;
		cmd_wrfrontend : out std_logic;
		cmd_greset : out std_logic;
		cmd_gpulse : out std_logic;
		cmd_runmode : out std_logic;

		-- parameter outputs
		par_chipid : out std_logic_vector(3 downto 0);
		par_address : out std_logic_vector(5 downto 0);
		par_runmode : out std_logic;
		par_data : out std_logic_vector(15 downto 0);
		par_srdata : out std_logic_vector(671 downto 0);
		par_width : out std_logic_vector(5 downto 0)
	);
end component;

constant cFE_trigger : std_logic_vector(4 downto 0) := "11101";
constant cFE_bcr : std_logic_vector(8 downto 0) := "101100001";
constant cFE_ecr : std_logic_vector(8 downto 0) := "101100010";
constant cFE_cal : std_logic_vector(8 downto 0) := "101100100";
constant cFE_slow : std_logic_vector(8 downto 0) := "101101000";
constant cFE_slow_rdreg : std_logic_vector(3 downto 0) := "0001";
constant cFE_slow_wrreg : std_logic_vector(3 downto 0) := "0010";
constant cFE_slow_wrfrontend : std_logic_vector(3 downto 0) := "0100";
constant cFE_slow_greset : std_logic_vector(3 downto 0) := "1000";
constant cFE_slow_gpulse : std_logic_vector(3 downto 0) := "1001";
constant cFE_slow_runmode : std_logic_vector(3 downto 0) := "1010";
constant cFE_chipid : std_logic_vector(3 downto 0) := "1010";
constant cFE_runmode : std_logic_vector(5 downto 0) := "111000";
constant cFE_confmode : std_logic_vector(5 downto 0) := "000111";

begin

dut: fei4_cmd_decoder
	port map (
		clk40 => clk40,
		reset => reset,
		din => din,
		cmd_lv1 => cmd_lv1,
		cmd_bcr => cmd_bcr,
		cmd_ecr => cmd_ecr,
		cmd_cal => cmd_cal,
		cmd_rdreg => cmd_rdreg,
		cmd_wrreg => cmd_wrreg,
		cmd_wrfrontend => cmd_wrfrontend,
		cmd_greset => cmd_greset,
		cmd_gpulse => cmd_gpulse,
		cmd_runmode => cmd_runmode,
		par_chipid => par_chipid,
		par_address => par_address,
		par_runmode => par_runmode,
		par_data => par_data,
		par_srdata => par_srdata,
		par_width => par_width
	);

clk40 <= not clk40 after 12.5 ns;

process
	procedure trigger is
	begin
		for I in 4 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_trigger(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure bcr is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_bcr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure ecr is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_ecr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure cal is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_cal(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure wrreg(addr : integer range 0 to 63; value : in std_logic_vector(15 downto 0)) is
		variable addr_slv : std_logic_vector(5 downto 0);
	begin
		addr_slv := std_logic_vector(to_unsigned(addr, 6));
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_wrreg(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= addr_slv(I);
		end loop;
		for I in 15 downto 0 loop
			wait until rising_edge(clk40);
			din <= value(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure rdreg(addr : integer range 0 to 63) is
		variable addr_slv : std_logic_vector(5 downto 0);
	begin
		addr_slv := std_logic_vector(to_unsigned(addr, 6));
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_rdreg(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= addr_slv(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure wrfrontend(sr : std_logic_vector(671 downto 0)) is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_wrfrontend(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= '0';
		end loop;
		for I in 671 downto 0 loop
			wait until rising_edge(clk40);
			din <= sr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure mode(runmode : boolean) is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_runmode(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			if runmode = true then
				din <= cFE_runmode(I);
			else
				din <= cFE_confmode(I);
			end if;
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;



	variable sr : std_logic_vector(671 downto 0) := (others => '0');
begin
	reset <= '1';
	wait for 100 ns;
	reset <= '0';
	wait for 100 ns;

	-- test some L1 triggers
	for I in 0 to 3 loop
		trigger;
		wait for 1 us;
	end loop;

	-- do register read and write
	for I in 0 to 63 loop
		rdreg(I);
		wait for 1 us;
	end loop;
	for I in 0 to 63 loop
		wrreg(I, std_logic_vector(to_unsigned(256*I + I, 16)));
		wait for 1 us;
	end loop;

	-- switch between modes
	mode(true);		-- runmode
	wait for 1 us;
	mode(false);	-- confmode
	wait for 1 us;

	-- write shift register
	sr(33) := '1';
	wrfrontend(sr);

	wait for 10 us;
	report  "Simulation finished" severity ERROR;
	wait;
end process;

end architecture ; -- simulation