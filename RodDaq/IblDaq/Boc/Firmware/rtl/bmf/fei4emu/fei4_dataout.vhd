library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_dataout is
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		-- command/data inputs
		cmd_lv1 : in std_logic;
		cmd_rdreg : in std_logic;
		reg_addr : in std_logic_vector(5 downto 0);
		reg_data : in std_logic_vector(15 downto 0);

		-- counters
		cnt_bcr : in std_logic_vector(12 downto 0);
		cnt_ecr : in std_logic_vector(11 downto 0);

		-- hit fifo
		hitfifo_data : in std_logic_vector(7 downto 0);
		hitfifo_rden : out std_logic;
		hitfifo_empty : in std_logic;
		hitfifo_armed : in std_logic;

		-- configuration
		hit_mode : in std_logic;
		hitcount : in std_logic_vector(7 downto 0);
		trig_count : in std_logic_vector(3 downto 0);
		sr14_enable : in std_logic;
		extra_frames : in std_logic_vector(7 downto 0);

		-- hit pool
		hitpool_data : in std_logic_vector(23 downto 0);
		hitpool_rden : out std_logic;
		hitpool_empty : in std_logic;

		-- data output (not encoded, parallel)
		dout : out std_logic_vector(7 downto 0);
		kchar : out std_logic;
		valid : out std_logic;
		xoff : in std_logic
	);
end entity ; -- fei4_dataout

architecture rtl of fei4_dataout is

-- pending trigger FIFO
COMPONENT fei4emu_pendtrig
  PORT (
    clk : IN STD_LOGIC;
    rst : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(24 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(24 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC
  );
END COMPONENT;

-- pending trigger FIFO signals
signal pendfifo_din : std_logic_vector(24 downto 0) := (others => '0');
signal pendfifo_dout : std_logic_vector(24 downto 0) := (others => '0');
signal pendfifo_wren : std_logic := '0';
signal pendfifo_rden : std_logic := '0';
signal pendfifo_full : std_logic := '0';
signal pendfifo_empty : std_logic := '0';

-- output fsm
type output_fsm is (st_idle, st_starthit, st_sof_regrd, st_sof_hit, st_eof,
					st_regrd0, st_regrd1, st_regrd2, st_regrd3, st_regrd4, st_regrd5,
					st_hit0, st_hit1, st_hit2, st_hit3, st_hit4, st_hit5, st_hit6, st_hit7, st_hit8, st_hit9,
					st_hitextra0, st_hitextra1);
signal Z : output_fsm := st_idle;

-- storage for some special values
signal reg_addr_i : std_logic_vector(5 downto 0) := (others => '0');
signal reg_data_i : std_logic_vector(15 downto 0) := (others => '0');
signal cnt_bcr_i : std_logic_vector(12 downto 0) := (others => '0');
signal cnt_ecr_i : std_logic_vector(11 downto 0) := (others => '0');

-- count number of hits
signal maxhits : integer range 0 to 255 := 8;
signal hitcnt : integer range 0 to 255 := 0;

-- internal trigger counter
signal trig_count_i : integer range 0 to 16 := 0;

-- extra frame counter
signal extra_frames_cnt : unsigned(7 downto 0) := (others => '0');

begin

-- instance of pending trigger FIFO
fei4emu_pendtrig_i: fei4emu_pendtrig
	port map (
		clk => clk40,
		rst => reset,
		din => pendfifo_din,
		dout => pendfifo_dout,
		rd_en => pendfifo_rden,
		wr_en => pendfifo_wren,
		full => pendfifo_full,
		empty => pendfifo_empty
	);

-- write on lv1 signals
pendfifo_din <= cnt_bcr & cnt_ecr;
pendfifo_wren <= cmd_lv1 and not pendfifo_full;

process begin
	wait until rising_edge(clk40);

	-- default assignments
	valid <= '0';
	kchar <= '0';
	pendfifo_rden <= '0';

	if reset = '1' then
		Z <= st_idle;

		-- send out IDLEs as default
		dout <= x"3C";
		kchar <= '1';

		-- reset extra frame count
		extra_frames_cnt <= (others => '0');
	else
		case Z is
			when st_idle =>
				dout <= x"3C";
				kchar <= '1';
				valid <= not xoff;

				if (pendfifo_empty = '0') then
					Z <= st_starthit;
					pendfifo_rden <= '1';
					maxhits <= to_integer(unsigned(hitcount));
					if (trig_count = "0000") then
						trig_count_i <= 16;
					else
						trig_count_i <= to_integer(unsigned(trig_count));
					end if;
				elsif (cmd_rdreg = '1') then
					Z <= st_sof_regrd;
					reg_addr_i <= reg_addr;
					reg_data_i <= reg_data;
				end if;

			when st_starthit =>
				cnt_bcr_i <= pendfifo_dout(24 downto 12);
				cnt_ecr_i <= pendfifo_dout(11 downto 0);
				Z <= st_sof_hit;

			when st_sof_regrd =>
				dout <= x"FC";
				kchar <= '1';
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd0;
				end if;

			when st_sof_hit =>
				dout <= x"FC";
				kchar <= '1';
				if xoff = '0' then
					valid <= '1';
					Z <= st_hit0;
				end if;
				
			when st_eof =>
				dout <= x"BC";
				kchar <= '1';
				if xoff = '0' then
					valid <= '1';
					Z <= st_idle;
				end if;

			when st_regrd0 =>
				dout <= x"EA";
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd1;
				end if;

			when st_regrd1 =>
				dout <= x"00";
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd2;
				end if;

			when st_regrd2 =>
				dout <= "00" & reg_addr_i;
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd3;
				end if;

			when st_regrd3 =>
				dout <= x"EC";
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd4;
				end if;

			when st_regrd4 =>
				dout <= reg_data_i(15 downto 8);
				if xoff = '0' then
					valid <= '1';
					Z <= st_regrd5;
				end if;

			when st_regrd5 =>
				dout <= reg_data_i(7 downto 0);
				if xoff = '0' then
					valid <= '1';
					Z <= st_eof;
				end if;

			when st_hit0 =>
				dout <= x"E9";
				if xoff = '0' then
					valid <= '1';
					Z <= st_hit1;
				end if;

			when st_hit1 =>
				dout <= '1' & cnt_ecr_i(4 downto 0) & cnt_bcr_i(9 downto 8);
				if xoff = '0' then
					valid <= '1';
					Z <= st_hit2;
				end if;

			when st_hit2 =>
				dout <= cnt_bcr_i(7 downto 0);
				if xoff = '0' then
					valid <= '1';
					if (sr14_enable = '1') then
						Z <= st_hit3;
					else
						cnt_bcr_i <= std_logic_vector(unsigned(cnt_bcr_i)+1);		-- increment bcr
						trig_count_i <= trig_count_i - 1;							-- decrement trig_count
						extra_frames_cnt <= extra_frames_cnt + 1;
						if extra_frames_cnt < unsigned(extra_frames) then
							Z <= st_hitextra0;
						else
							if hit_mode = '1' then
								Z <= st_hit7;
							else
								Z <= st_hit6;
							end if;
						end if;
					end if;
				end if;

			when st_hit3 =>
				dout <= x"EF";
				if xoff = '0' then
					valid <= '1';
					Z <= st_hit4;
				end if;

			when st_hit4 =>
				dout <= "001110" & cnt_ecr_i(11 downto 10);
				if xoff = '0' then
					valid <= '1';
					Z <= st_hit5;
				end if;

			when st_hit5 =>
				dout <= cnt_ecr_i(9 downto 5) & cnt_bcr_i(12 downto 10);
				if xoff = '0' then
					cnt_bcr_i <= std_logic_vector(unsigned(cnt_bcr_i)+1);		-- increment bcr
					trig_count_i <= trig_count_i - 1;							-- decrement trig_count
					valid <= '1';
					extra_frames_cnt <= extra_frames_cnt + 1;
					if extra_frames_cnt < unsigned(extra_frames) then
						Z <= st_hitextra0;
					else
						if hit_mode = '1' then
							Z <= st_hit7;
						else
							Z <= st_hit6;
						end if;
					end if;
				end if;

			when st_hit6 =>
				if (hitfifo_empty = '1') or (hitfifo_armed = '0') then
					dout <= x"BC";
					kchar <= '1';
					if xoff = '0' then
						valid <= '1';
						Z <= st_idle;
					end if;
				else
					dout <= hitfifo_data;
					if xoff = '0' then
						valid <= '1';
					end if;
				end if;

			when st_hit7 =>
				if (hitpool_empty = '1') or (hitcnt = maxhits) then
					dout <= x"BC";
					kchar <= '1';
					if xoff = '0' then
						hitcnt <= 0;
						valid <= '1';
						if trig_count_i = 0 then
							Z <= st_idle;
						else
							Z <= st_sof_hit;
						end if;
					end if;
				else
					dout <= hitpool_data(23 downto 16);
					if xoff = '0' then
						Z <= st_hit8;
						hitcnt <= hitcnt + 1;
						valid <= '1';
					end if;
				end if;

			when st_hit8 =>
				dout <= hitpool_data(15 downto 8);

				if xoff = '0' then
					valid <= '1';
					Z <= st_hit9;
				end if;

			when st_hit9 =>
				dout <= hitpool_data(7 downto 0);

				if xoff = '0' then
					valid <= '1';
					Z <= st_hit7;
				end if;

			when st_hitextra0 =>
				dout <= x"BC";
				kchar <= '1';

				if xoff = '0' then
					valid <= '1';
					Z <= st_hitextra1;
				end if;

			when st_hitextra1 =>
				dout <= x"FC";
				kchar <= '1';

				if xoff = '0' then
					valid <= '1';
					if hit_mode = '1' then
						Z <= st_hit7;
					else
						Z <= st_hit6;
					end if;
				end if;
		end case;
	end if;
end process;

hitfifo_rden <= '1' when (Z = st_hit6) and (hitfifo_armed = '1') and (hitfifo_empty = '0') and (xoff = '0') else
				'0';

hitpool_rden <= '1' when (Z = st_hit9) and (hitpool_empty = '0') and (xoff = '0') else
				'0';

end architecture ; -- rtl