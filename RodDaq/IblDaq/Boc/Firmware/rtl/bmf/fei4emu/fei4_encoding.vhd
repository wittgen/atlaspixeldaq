library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library encode_8b10b;

entity fei4_encoding is
	port (
		-- clocking and reset
		clk40 : in std_logic;
		clk160 : in std_logic;
		reset : in std_logic;

		-- data input
		din : in std_logic_vector(7 downto 0);
		din_valid : in std_logic;
		din_kchar : in std_logic;
		din_xoff : out std_logic;

		-- data output
		dout : out std_logic
	);
end fei4_encoding;

architecture rtl of fei4_encoding is
	COMPONENT serializer
	PORT(
	 clk40 : IN  std_logic;
	 clk160 : IN  std_logic;
	 ce : IN std_logic;
	 mode : IN  std_logic;
	 load : OUT  std_logic;
	 din : IN  std_logic_vector(9 downto 0);
	 dout : OUT  std_logic
	);
	END COMPONENT;

	COMPONENT encode_8b10b_wrapper
	  PORT (
		 CLK          : IN  STD_LOGIC                    := '0';
		 DIN          : IN  STD_LOGIC_VECTOR(7 DOWNTO 0) := (OTHERS => '0');
		 KIN          : IN  STD_LOGIC                    := '0';                   
		 DOUT         : OUT STD_LOGIC_VECTOR(9 DOWNTO 0)       ;
		 CE           : IN  STD_LOGIC                    := '0'
		);
	END COMPONENT;

	COMPONENT fei4emu_datafifo
	  PORT (
	    rst : IN STD_LOGIC;
	    wr_clk : IN STD_LOGIC;
	    rd_clk : IN STD_LOGIC;
	    din : IN STD_LOGIC_VECTOR(8 DOWNTO 0);
	    wr_en : IN STD_LOGIC;
	    rd_en : IN STD_LOGIC;
	    dout : OUT STD_LOGIC_VECTOR(8 DOWNTO 0);
	    full : OUT STD_LOGIC;
	    almost_full : OUT STD_LOGIC;
	    empty : OUT STD_LOGIC;
	    wr_data_count : OUT STD_LOGIC_VECTOR(3 DOWNTO 0)
	  );
	END COMPONENT;

	signal fifo_din : std_logic_vector(8 downto 0) := (others => '0');
	signal fifo_dout : std_logic_vector(8 downto 0) := (others => '0');
	signal fifo_wren : std_logic := '0';
	signal fifo_rden : std_logic := '0';
	signal fifo_empty : std_logic := '0';
	signal fifo_full : std_logic := '0';
	signal fifo_almost_full : std_logic := '0';
	signal fifo_wrdcnt : std_logic_vector(3 downto 0) := "0000";

	signal ser_din : std_logic_vector(9 downto 0) := (others => '0');
	signal ser_load : std_logic := '0';
	signal ser_dout : std_logic := '0';

	signal enc_din : std_logic_vector(7 downto 0) := "00000000";
	signal enc_kchar : std_logic := '0';
	signal enc_dout : std_logic_vector(9 downto 0) := "0000000000";
	signal enc_enable : std_logic := '0';
begin

-- FIFO
fifo_i: fei4emu_datafifo
	port map (
		rst => reset,
		wr_clk => clk40,
		rd_clk => clk160,
		din => fifo_din,
		dout => fifo_dout,
		wr_en => fifo_wren,
		rd_en => fifo_rden,
		full => fifo_full,
		empty => fifo_empty,
		almost_full => fifo_almost_full,
		wr_data_count => fifo_wrdcnt
	);

-- encoder
enc_i: encode_8b10b_wrapper
	port map (
		clk => clk160,
		din => enc_din,
		kin => enc_kchar,
		dout => enc_dout,
		ce => enc_enable
	);

-- serializer
ser_i: serializer
	port map (
		clk40 => '0',
		clk160 => clk160,
		ce => '1',
		mode => '1',
		load => ser_load,
		din => ser_din,
		dout => ser_dout
	);

-- map internal signals
-- FIFO input
fifo_wren <= (din_valid and not fifo_full) when rising_edge(clk40);
fifo_din <= (din_kchar & din) when rising_edge(clk40);

-- encoder input and FIFO output
fifo_rden <= ser_load;
enc_enable <= ser_load;
enc_din <= fifo_dout(7 downto 0);
enc_kchar <= fifo_dout(8);

-- serializer input
reverse_order: for I in 0 to 9 generate
	ser_din(I) <= enc_dout(9-I) when rising_edge(clk160);
end generate;

-- data output
dout <= ser_dout when rising_edge(clk160);

din_xoff <= fifo_wrdcnt(fifo_wrdcnt'left);


end architecture;
