library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_cmd_decoder is
	generic (
		RUNMODE_CHIPID_CHECK : boolean := true
	);
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- serial data in (synchronous to 40 MHz clock)
		din : in std_logic;

		-- command outputs
		cmd_lv1 : out std_logic;
		cmd_bcr : out std_logic;
		cmd_ecr : out std_logic;
		cmd_cal : out std_logic;
		cmd_rdreg : out std_logic;
		cmd_wrreg : out std_logic;
		cmd_wrfrontend : out std_logic;
		cmd_greset : out std_logic;
		cmd_gpulse : out std_logic;
		cmd_runmode : out std_logic;

		-- parameter outputs
		par_address : out std_logic_vector(5 downto 0);
		par_runmode : out std_logic;
		par_data : out std_logic_vector(15 downto 0);
		par_width : out std_logic_vector(5 downto 0);
		par_chipid : out std_logic_vector(3 downto 0);

		-- chipid / runmode input
		runmode : in std_logic;
		chipid : in std_logic_vector(3 downto 0)
	);
end fei4_cmd_decoder;

architecture rtl of fei4_cmd_decoder is
	type decoder_fsm is (st_idle, st_decoding);
	signal Z : decoder_fsm := st_idle;

	constant cmd_sr_length : integer := 40;
	signal cmd_sr : std_logic_vector(cmd_sr_length-1 downto 0) := (others => '0');
	signal cmd_sr_cnt : integer range 0 to cmd_sr_length := 0;

	signal ignore_cnt : integer range 0 to 680 := 0;

	signal decoded_chipid : std_logic_vector(3 downto 0) := (others => '0');
	signal cmd_rdreg_i : std_logic := '0';
	signal cmd_wrreg_i : std_logic := '0';
	signal cmd_greset_i : std_logic := '0';
	signal cmd_gpulse_i : std_logic := '0';
	signal cmd_runmode_i : std_logic := '0';
begin

-- decoder fsm
process begin
	wait until rising_edge(clk40);

	-- default output
	cmd_lv1 <= '0';
	cmd_bcr <= '0';
	cmd_ecr <= '0';
	cmd_cal <= '0';
	cmd_rdreg_i <= '0';
	cmd_wrreg_i <= '0';
	cmd_greset_i <= '0';
	cmd_gpulse_i <= '0';
	cmd_runmode_i <= '0';
	cmd_wrfrontend <= '0';

	if reset = '1' then
		Z <= st_idle;
		cmd_sr <= (others => '0');
		cmd_sr_cnt <= 0;
		ignore_cnt <= 0;
	else
		-- shift in data
		if ignore_cnt = 0 then
			cmd_sr <= cmd_sr(cmd_sr'left-1 downto 0) & din;
		else
			ignore_cnt <= ignore_cnt - 1;
		end if;

		-- distinguish state
		case Z is
			when st_idle =>
				if (din = '1') and (ignore_cnt = 0) then
					Z <= st_decoding;
					cmd_sr_cnt <= 1;
				else
					cmd_sr_cnt <= 0;
				end if;

			when st_decoding =>
				if cmd_sr_cnt = cmd_sr_length then
					Z <= st_idle;
					cmd_sr_cnt <= 0;
				else
					cmd_sr_cnt <= cmd_sr_cnt + 1;
				end if;

				-- try decoding some patterns
				if (cmd_sr_cnt = 5) and (cmd_sr(4 downto 0) = "11101") then					-- LV1
					if RUNMODE_CHIPID_CHECK = true then
						cmd_lv1 <= runmode;
					else
						cmd_lv1 <= '1';
					end if;
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 9) and (cmd_sr(8 downto 0) = "101100001") then			-- BCR
					cmd_bcr <= '1';
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 9) and (cmd_sr(8 downto 0) = "101100010") then			-- ECR
					cmd_ecr <= '1';
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 9) and (cmd_sr(8 downto 0) = "101100100") then			-- CAL
					cmd_cal <= '1';
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 17) and (cmd_sr(16 downto 4) = "1011010001000") then	-- global reset
					decoded_chipid <= cmd_sr(3 downto 0);
					cmd_greset_i <= '1';
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 23) and (cmd_sr(22 downto 10) = "1011010001001") then	-- global pulse
					decoded_chipid <= cmd_sr(9 downto 6);
					cmd_gpulse_i <= '1';
					par_width <= cmd_sr(5 downto 0);
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 23) and (cmd_sr(22 downto 10) = "1011010001010") then	-- switch run/conf mode
					decoded_chipid <= cmd_sr(9 downto 6);
					
					if cmd_sr(5 downto 0) = "000111" then
						cmd_runmode_i <= '1';
						par_runmode <= '0';
					elsif cmd_sr(5 downto 0) = "111000" then
						cmd_runmode_i <= '1';
						par_runmode <= '1';
					end if;
					
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 23) and (cmd_sr(22 downto 10) = "1011010000001") then	-- read register
					decoded_chipid <= cmd_sr(9 downto 6);
					cmd_rdreg_i <= '1';
					par_address <= cmd_sr(5 downto 0);				
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				elsif (cmd_sr_cnt = 23) and (cmd_sr(22 downto 10) = "1011010000100") then
					ignore_cnt <= 672;		-- ignore next 672 bits
					Z <= st_idle;
				elsif (cmd_sr_cnt = 39) and (cmd_sr(38 downto 26) = "1011010000010") then	-- write register
					decoded_chipid <= cmd_sr(25 downto 22);
					cmd_wrreg_i <= '1';
					par_address <= cmd_sr(21 downto 16);
					par_data <= cmd_sr(15 downto 0);
					cmd_sr_cnt <= 0;
					Z <= st_idle;
				end if;
		end case;
	end if;
end process;

-- mask out slow commands with wrong chipid
process begin
	wait until rising_edge(clk40);

	if RUNMODE_CHIPID_CHECK = true then
		if ((decoded_chipid = chipid) or (decoded_chipid = "1000")) then
			cmd_rdreg <= cmd_rdreg_i and not runmode;
			cmd_wrreg <= cmd_wrreg_i and not runmode;
			cmd_greset <= cmd_greset_i and not runmode;
			cmd_gpulse <= cmd_gpulse_i and not runmode;
			cmd_runmode <= cmd_runmode_i;
		else
			cmd_rdreg <= '0';
			cmd_wrreg <= '0';
			cmd_greset <= '0';
			cmd_gpulse <= '0';
			cmd_runmode <= '0';
		end if;
	else
		cmd_rdreg <= cmd_rdreg_i;
		cmd_wrreg <= cmd_wrreg_i;
		cmd_greset <= cmd_greset_i;
		cmd_gpulse <= cmd_gpulse_i;
		cmd_runmode <= cmd_runmode_i;
	end if;

	par_chipid <= decoded_chipid;
end process;

end architecture ; -- rtl