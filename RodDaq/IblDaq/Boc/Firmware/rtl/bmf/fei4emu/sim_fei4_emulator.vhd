library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sim_fei4_emulator is
end sim_fei4_emulator;

architecture sim of sim_fei4_emulator is
	signal clk40 : std_logic := '0';
	signal clk160 : std_logic := '0';
	signal reset : std_logic := '0';
	signal din : std_logic := '0';
	signal dout : std_logic := '0';
	signal control_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal control_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal control_rden : std_logic := '0';
	signal control_wren : std_logic := '0';
	signal status_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal status_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal status_rden : std_logic := '0';
	signal status_wren : std_logic := '0';
	signal l1id_lsb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal l1id_lsb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal l1id_lsb_rden : std_logic := '0';
	signal l1id_lsb_wren : std_logic := '0';
	signal l1id_msb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal l1id_msb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal l1id_msb_rden : std_logic := '0';
	signal l1id_msb_wren : std_logic := '0';
	signal bcid_lsb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal bcid_lsb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal bcid_lsb_rden : std_logic := '0';
	signal bcid_lsb_wren : std_logic := '0';
	signal bcid_msb_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal bcid_msb_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal bcid_msb_rden : std_logic := '0';
	signal bcid_msb_wren : std_logic := '0';
	signal hit_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal hit_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal hit_rden : std_logic := '0';
	signal hit_wren : std_logic := '0';
	signal hitcount_wr : std_logic_vector(7 downto 0) := (others => '0');
	signal hitcount_rd : std_logic_vector(7 downto 0) := (others => '0');
	signal hitcount_rden : std_logic := '0';
	signal hitcount_wren : std_logic := '0';

	signal rx_dout : std_logic_vector(7 downto 0);
	signal rx_kchar : std_logic;
	signal rx_valid : std_logic;
	signal rx_sync : std_logic;
	signal rx_decerr : std_logic;
	signal rx_disperr : std_logic;

	component fei4_emulator
		port (
			-- clocking and reset
			clk40 : in std_logic;
			clk160 : in std_logic;
			reset : in std_logic;

			-- serial data input (synchronous with clk40)
			din : in std_logic;

			-- serial data output (synchronous with clk160)
			dout : out std_logic;

			-- register interface to BMF register bank
			control_rd : out std_logic_vector(7 downto 0);
			control_wr : in std_logic_vector(7 downto 0);
			control_rden : in std_logic;
			control_wren : in std_logic;
			status_rd : out std_logic_vector(7 downto 0);
			status_wr : in std_logic_vector(7 downto 0);
			status_rden : in std_logic;
			status_wren : in std_logic;
			l1id_lsb_rd : out std_logic_vector(7 downto 0);
			l1id_lsb_wr : in std_logic_vector(7 downto 0);
			l1id_lsb_rden : in std_logic;
			l1id_lsb_wren : in std_logic;
			l1id_msb_rd : out std_logic_vector(7 downto 0);
			l1id_msb_wr : in std_logic_vector(7 downto 0);
			l1id_msb_rden : in std_logic;
			l1id_msb_wren : in std_logic;
			bcid_lsb_rd : out std_logic_vector(7 downto 0);
			bcid_lsb_wr : in std_logic_vector(7 downto 0);
			bcid_lsb_rden : in std_logic;
			bcid_lsb_wren : in std_logic;
			bcid_msb_rd : out std_logic_vector(7 downto 0);
			bcid_msb_wr : in std_logic_vector(7 downto 0);
			bcid_msb_rden : in std_logic;
			bcid_msb_wren : in std_logic;
			hit_wr : in std_logic_vector(7 downto 0);
			hit_rd : out std_logic_vector(7 downto 0);
			hit_wren : in std_logic;
			hit_rden : in std_logic;
			hitcount_wr : in std_logic_vector(7 downto 0);
			hitcount_rd : out std_logic_vector(7 downto 0);
			hitcount_wren : in std_logic;
			hitcount_rden : in std_logic
	  	);
	end component;

	component sim_testrx
		port (
			clk160 : in std_logic;
			reset : in std_logic;

			din : in std_logic;

			dout : out std_logic_vector(7 downto 0);
			kchar : out std_logic;
			valid : out std_logic;

			sync : out std_logic;
			decerr : out std_logic;
			disperr : out std_logic
		);
	end component;

	constant cFE_trigger : std_logic_vector(4 downto 0) := "11101";
	constant cFE_bcr : std_logic_vector(8 downto 0) := "101100001";
	constant cFE_ecr : std_logic_vector(8 downto 0) := "101100010";
	constant cFE_cal : std_logic_vector(8 downto 0) := "101100100";
	constant cFE_slow : std_logic_vector(8 downto 0) := "101101000";
	constant cFE_slow_rdreg : std_logic_vector(3 downto 0) := "0001";
	constant cFE_slow_wrreg : std_logic_vector(3 downto 0) := "0010";
	constant cFE_slow_wrfrontend : std_logic_vector(3 downto 0) := "0100";
	constant cFE_slow_greset : std_logic_vector(3 downto 0) := "1000";
	constant cFE_slow_gpulse : std_logic_vector(3 downto 0) := "1001";
	constant cFE_slow_runmode : std_logic_vector(3 downto 0) := "1010";
	constant cFE_chipid : std_logic_vector(3 downto 0) := "1000";
	constant cFE_runmode : std_logic_vector(5 downto 0) := "111000";
	constant cFE_confmode : std_logic_vector(5 downto 0) := "000111";
begin

dut: fei4_emulator
	port map (
		clk40 => clk40,
		clk160 => clk160,
		reset => reset,
		din => din,
		dout => dout,
		control_rd => control_rd,
		control_wr => control_wr,
		control_rden => control_rden,
		control_wren => control_wren,
		status_rd => status_rd,
		status_wr => status_wr,
		status_rden => status_rden,
		status_wren => status_wren,
		l1id_lsb_rd => l1id_lsb_rd,
		l1id_lsb_wr => l1id_lsb_wr,
		l1id_lsb_rden => l1id_lsb_rden,
		l1id_lsb_wren => l1id_lsb_wren,
		l1id_msb_rd => l1id_msb_rd,
		l1id_msb_wr => l1id_msb_wr,
		l1id_msb_rden => l1id_msb_rden,
		l1id_msb_wren => l1id_msb_wren,
		bcid_lsb_rd => bcid_lsb_rd,
		bcid_lsb_wr => bcid_lsb_wr,
		bcid_lsb_rden => bcid_lsb_rden,
		bcid_lsb_wren => bcid_lsb_wren,
		bcid_msb_rd => bcid_msb_rd,
		bcid_msb_wr => bcid_msb_wr,
		bcid_msb_rden => bcid_msb_rden,
		bcid_msb_wren => bcid_msb_wren,
		hit_rd => hit_rd,
		hit_wr => hit_wr,
		hit_rden => hit_rden,
		hit_wren => hit_wren,
		hitcount_rd => hitcount_rd,
		hitcount_wr => hitcount_wr,
		hitcount_rden => hitcount_rden,
		hitcount_wren => hitcount_wren
	);

rx: sim_testrx
	port map (
		clk160 => clk160,
		reset => reset,
		din => dout,
		dout => rx_dout,
		kchar => rx_kchar,
		valid => rx_valid,
		sync => rx_sync,
		decerr => rx_decerr,
		disperr => rx_disperr
	);

clk40 <= not clk40 after 12.5 ns;
clk160 <= not clk160 after 3.125 ns;

process
	procedure trigger is
	begin
		for I in 4 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_trigger(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure bcr is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_bcr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure ecr is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_ecr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure cal is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_cal(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure wrreg(addr : integer range 0 to 63; value : in std_logic_vector(15 downto 0)) is
		variable addr_slv : std_logic_vector(5 downto 0);
	begin
		addr_slv := std_logic_vector(to_unsigned(addr, 6));
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_wrreg(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= addr_slv(I);
		end loop;
		for I in 15 downto 0 loop
			wait until rising_edge(clk40);
			din <= value(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure rdreg(addr : integer range 0 to 63) is
		variable addr_slv : std_logic_vector(5 downto 0);
	begin
		addr_slv := std_logic_vector(to_unsigned(addr, 6));
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_rdreg(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= addr_slv(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure wrfrontend(sr : std_logic_vector(671 downto 0)) is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_wrfrontend(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			din <= '0';
		end loop;
		for I in 671 downto 0 loop
			wait until rising_edge(clk40);
			din <= sr(I);
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;

	procedure mode(runmode : boolean) is
	begin
		for I in 8 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_slow_runmode(I);
		end loop;
		for I in 3 downto 0 loop
			wait until rising_edge(clk40);
			din <= cFE_chipid(I);
		end loop;
		for I in 5 downto 0 loop
			wait until rising_edge(clk40);
			if runmode = true then
				din <= cFE_runmode(I);
			else
				din <= cFE_confmode(I);
			end if;
		end loop;
		wait until rising_edge(clk40);
		din <= '0';
	end procedure;
begin
	reset <= '1';
	wait for 100 ns;
	reset <= '0';
	wait for 100 ns;

	-- enable front-end emulator
	wait until rising_edge(clk40);
	control_wr <= "00000101";
	control_wren <= '1';
	wait until rising_edge(clk40);
	control_wren <= '0';
	wait until rising_edge(clk40);
	hitcount_wr <= std_logic_vector(to_unsigned(100, 8));
	hitcount_wren <= '1';
	wait until rising_edge(clk40);
	hitcount_wren <= '0';

	mode(true);
	wait for 1 us;

	loop
		trigger;
		wait for 100 us;
	end loop;

	wait for 10 us;
	report  "Simulation finished" severity ERROR;

	wait;
end process;



end architecture ;