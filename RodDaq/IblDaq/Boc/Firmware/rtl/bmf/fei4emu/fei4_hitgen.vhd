library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_hitgen is
	port (
		-- clocking and reset
		clk40 : in std_logic;
		reset : in std_logic;

		-- hitfifo interface
		fifo_data : out std_logic_vector(23 downto 0);
		fifo_wren : out std_logic;
		fifo_full : in std_logic;

		-- tot value
		tot_value : in std_logic_vector(7 downto 0)
	);
end entity ; -- fei4_hitgen

architecture rtl of fei4_hitgen is
	component prng
		generic (
			width : integer := 32
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			load : in std_logic;
			seed : in std_logic_vector(width-1 downto 0);
			step : in std_logic;
			dout : out std_logic_vector(width-1 downto 0)
		);
	end component;

	-- row and column
	signal hit_row : std_logic_vector(8 downto 0) := (others => '0');
	signal hit_col : std_logic_vector(6 downto 0) := (others => '0');

	-- prng signals
	constant prng_row_seed : std_logic_vector(8 downto 0) := "101100010";
	constant prng_col_seed : std_logic_vector(6 downto 0) := "0100001";
	signal prng_row : std_logic_vector(8 downto 0);
	signal prng_col : std_logic_vector(6 downto 0);

	-- output data record
	signal hitrecord : std_logic_vector(23 downto 0) := (others => '0');

	-- state machine
	type hitgen_fsm is (st_getrow, st_getcol, st_genrecord, st_output);
	signal Z : hitgen_fsm := st_getrow;
begin

-- row prng
prng_row_i: prng
	generic map (
		width => prng_row'length)
	port map (
		clk => clk40,
		reset => reset,
		load => '0',
		seed => prng_row_seed,
		step => '1',
		dout => prng_row
	);

-- col prng
prng_col_i: prng
	generic map (
		width => prng_col'length)
	port map (
		clk => clk40,
		reset => reset,
		load => '0',
		seed => prng_col_seed,
		step => '1',
		dout => prng_col
	);

-- fsm
process begin
	wait until rising_edge(clk40);

	-- default assignment
	fifo_wren <= '0';

	if (reset = '1') then
		Z <= st_getrow;
	else
		case Z is
			when st_getrow =>
				if (to_integer(unsigned(prng_row)) > 0) and (to_integer(unsigned(prng_row)) <= 336) then
					hit_row <= prng_row;
					Z <= st_getcol;
				end if;

			when st_getcol =>
				if (to_integer(unsigned(prng_col)) > 0) and (to_integer(unsigned(prng_col)) <= 80) then
					hit_col <= prng_col;
					Z <= st_genrecord;
				end if;

			when st_genrecord =>
				hitrecord(7 downto 0) <= tot_value;
				hitrecord(16 downto 8) <= hit_row;
				hitrecord(23 downto 17) <= hit_col;
				Z <= st_output;

			when st_output =>
				fifo_data <= hitrecord;
				if fifo_full = '0' then
					fifo_wren <= '1';
					Z <= st_getrow;
				end if;
		end case;
	end if;
end process;



end architecture ; -- rtl