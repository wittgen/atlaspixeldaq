library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_counters is
	port (
		clk40 : in std_logic;
		reset : in std_logic;

		cmd_bcr : in std_logic;
		cmd_ecr : in std_logic;
		cmd_lv1 : in std_logic;

		cnt_bcr : out std_logic_vector(12 downto 0);
		cnt_ecr : out std_logic_vector(11 downto 0)
  ) ;
end entity ; -- fei4_counters

architecture rtl of fei4_counters is

-- internal counters
signal cnt_bcr_i : unsigned(12 downto 0) := (others => '0');
signal cnt_ecr_i : unsigned(11 downto 0) := (others => '0');

begin

-- counter
process begin
	wait until rising_edge(clk40);

	if reset = '1' then
		cnt_bcr_i <= (others => '0');
		cnt_ecr_i <= (others => '0');
	else
		if cmd_bcr = '1' then
			cnt_bcr_i <= (others => '0');
		elsif cmd_ecr = '1' then
			cnt_ecr_i <= (others => '0');
		else
			cnt_bcr_i <= cnt_bcr_i + 1;
			if cmd_lv1 = '1' then
				cnt_ecr_i <= cnt_ecr_i + 1;
			end if;
		end if;
	end if;
end process;

-- output counters
cnt_bcr <= std_logic_vector(cnt_bcr_i);
cnt_ecr <= std_logic_vector(cnt_ecr_i);

end architecture ; -- rtl