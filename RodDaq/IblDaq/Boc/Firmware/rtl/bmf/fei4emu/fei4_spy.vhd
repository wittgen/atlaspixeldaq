library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity fei4_spy is
	port (
		clk : in std_logic;
		reset : in std_logic;

		din : in std_logic_vector(15 downto 0);

		csr_wr : in std_logic_vector(7 downto 0);
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wren : in std_logic;
		csr_rden : in std_logic;
		fifo0_wr : in std_logic_vector(7 downto 0);
		fifo0_rd : out std_logic_vector(7 downto 0);
		fifo0_wren : in std_logic;
		fifo0_rden : in std_logic;
		fifo1_wr : in std_logic_vector(7 downto 0);
		fifo1_rd : out std_logic_vector(7 downto 0);
		fifo1_wren : in std_logic;
		fifo1_rden : in std_logic;
		fifo2_wr : in std_logic_vector(7 downto 0);
		fifo2_rd : out std_logic_vector(7 downto 0);
		fifo2_wren : in std_logic;
		fifo2_rden : in std_logic;
		fifo3_wr : in std_logic_vector(7 downto 0);
		fifo3_rd : out std_logic_vector(7 downto 0);
		fifo3_wren : in std_logic;
		fifo3_rden : in std_logic;
		trigcnt0_wr : in std_logic_vector(7 downto 0);
		trigcnt0_rd : out std_logic_vector(7 downto 0);
		trigcnt0_rden : in std_logic;
		trigcnt0_wren : in std_logic;
		trigcnt1_wr : in std_logic_vector(7 downto 0);
		trigcnt1_rd : out std_logic_vector(7 downto 0);
		trigcnt1_rden : in std_logic;
		trigcnt1_wren : in std_logic;
		trigcnt2_wr : in std_logic_vector(7 downto 0);
		trigcnt2_rd : out std_logic_vector(7 downto 0);
		trigcnt2_rden : in std_logic;
		trigcnt2_wren : in std_logic;
		trigcnt3_wr : in std_logic_vector(7 downto 0);
		trigcnt3_rd : out std_logic_vector(7 downto 0);
		trigcnt3_rden : in std_logic;
		trigcnt3_wren : in std_logic;
		calcnt0_wr : in std_logic_vector(7 downto 0);
		calcnt0_rd : out std_logic_vector(7 downto 0);
		calcnt0_rden : in std_logic;
		calcnt0_wren : in std_logic;
		calcnt1_wr : in std_logic_vector(7 downto 0);
		calcnt1_rd : out std_logic_vector(7 downto 0);
		calcnt1_rden : in std_logic;
		calcnt1_wren : in std_logic;
		calcnt2_wr : in std_logic_vector(7 downto 0);
		calcnt2_rd : out std_logic_vector(7 downto 0);
		calcnt2_rden : in std_logic;
		calcnt2_wren : in std_logic;
		calcnt3_wr : in std_logic_vector(7 downto 0);
		calcnt3_rd : out std_logic_vector(7 downto 0);
		calcnt3_rden : in std_logic;
		calcnt3_wren : in std_logic
	);
end fei4_spy;

architecture rtl of fei4_spy is
	COMPONENT fei4_spy_fifo
	  PORT (
	    clk : IN STD_LOGIC;
	    rst : IN STD_LOGIC;
	    din : IN STD_LOGIC_VECTOR(31 DOWNTO 0);
	    wr_en : IN STD_LOGIC;
	    rd_en : IN STD_LOGIC;
	    dout : OUT STD_LOGIC_VECTOR(31 DOWNTO 0);
	    full : OUT STD_LOGIC;
	    empty : OUT STD_LOGIC
	  );
	END COMPONENT;

	component fei4_cmd_decoder
		generic (
			RUNMODE_CHIPID_CHECK : boolean := true
		);
		port (
			-- clocking and reset
			clk40 : in std_logic;
			reset : in std_logic;

			-- serial data in (synchronous to 40 MHz clock)
			din : in std_logic;

			-- command outputs
			cmd_lv1 : out std_logic;
			cmd_bcr : out std_logic;
			cmd_ecr : out std_logic;
			cmd_cal : out std_logic;
			cmd_rdreg : out std_logic;
			cmd_wrreg : out std_logic;
			cmd_wrfrontend : out std_logic;
			cmd_greset : out std_logic;
			cmd_gpulse : out std_logic;
			cmd_runmode : out std_logic;

			-- parameter outputs
			par_address : out std_logic_vector(5 downto 0);
			par_runmode : out std_logic;
			par_data : out std_logic_vector(15 downto 0);
			par_width : out std_logic_vector(5 downto 0);
			par_chipid : out std_logic_vector(3 downto 0);

			-- chipid / runmode input
			runmode : in std_logic;
			chipid : in std_logic_vector(3 downto 0)
		);	
	end component;

	signal enable : std_logic := '0';
	signal sel : std_logic_vector(3 downto 0) := (others => '0');
	signal selected_din : std_logic := '0';
	signal cmd_lv1, cmd_bcr, cmd_ecr, cmd_cal, cmd_rdreg, cmd_wrreg, cmd_wrfrontend, cmd_greset, cmd_gpulse, cmd_runmode : std_logic := '0';
	signal par_address : std_logic_vector(5 downto 0);
	signal par_runmode : std_logic;
	signal par_data : std_logic_vector(15 downto 0);
	signal par_width : std_logic_vector(5 downto 0);
	signal par_chipid : std_logic_vector(3 downto 0);

	signal fifo_din : std_logic_vector(31 downto 0) := (others => '0');
	signal fifo_dout : std_logic_vector(31 downto 0) := (others => '0');
	signal fifo_rden : std_logic := '0';
	signal fifo_wren : std_logic := '0';
	signal fifo_full : std_logic := '0';
	signal fifo_empty : std_logic := '0';

	signal trigcnt_enable : std_logic := '0';
	signal trigcnt : unsigned(31 downto 0) := (others => '0');
	signal trigcnt_tmp : std_logic_vector(23 downto 0) := (others => '0');
	signal calcnt : unsigned(31 downto 0) := (others => '0');
	signal calcnt_tmp : std_logic_vector(23 downto 0) := (others => '0');
begin

spy_fifo: fei4_spy_fifo
	port map (
		clk => clk,
		rst => reset,
		din => fifo_din,
		dout => fifo_dout,
		wr_en => fifo_wren,
		rd_en => fifo_rden,
		full => fifo_full,
		empty => fifo_empty
	);

spy_cmd_decoder: fei4_cmd_decoder
	generic map (
		RUNMODE_CHIPID_CHECK => false
	)
	port map (
		clk40 => clk,
		reset => reset,
		din => selected_din,
		cmd_lv1 => cmd_lv1,
		cmd_bcr => cmd_bcr,
		cmd_ecr => cmd_ecr,
		cmd_cal => cmd_cal,
		cmd_rdreg => cmd_rdreg,
		cmd_wrreg => cmd_wrreg,
		cmd_wrfrontend => cmd_wrfrontend,
		cmd_greset => cmd_greset,
		cmd_gpulse => cmd_gpulse,
		cmd_runmode => cmd_runmode,
		par_address => par_address,
		par_runmode => par_runmode,
		par_data => par_data,
		par_width => par_width,
		par_chipid => par_chipid,
		runmode => '0',
		chipid => "0000"
	);

-- select specific monitor channel
selected_din <= din(to_integer(unsigned(sel))) when rising_edge(clk);

-- FIFO writing
process begin
	wait until rising_edge(clk);

	-- defaults
	fifo_wren <= '0';

	if enable = '1' and fifo_full = '0' then
		if cmd_lv1 = '1' then
			fifo_din <= x"10000000";
			fifo_wren <= '1';
		elsif cmd_bcr = '1' then
			fifo_din <= x"20000000";
			fifo_wren <= '1';
		elsif cmd_ecr = '1' then
			fifo_din <= x"30000000";
			fifo_wren <= '1';
		elsif cmd_cal = '1' then
			fifo_din <= x"40000000";
			fifo_wren <= '1';
		elsif cmd_rdreg = '1' then
			fifo_din(31 downto 28) <= x"5";
			fifo_din(27 downto 24) <= par_chipid;
			fifo_din(23 downto 16) <= "00" & par_address;
			fifo_din(15 downto 0) <= x"0000";
			fifo_wren <= '1';
		elsif cmd_wrreg = '1' then
			fifo_din(31 downto 28) <= x"6";
			fifo_din(27 downto 24) <= par_chipid;
			fifo_din(23 downto 16) <= "00" & par_address;
			fifo_din(15 downto 0) <= par_data;
			fifo_wren <= '1';
		elsif cmd_greset = '1' then
			fifo_din(31 downto 28) <= x"7";
			fifo_din(27 downto 24) <= par_chipid;
			fifo_wren <= '1';
		elsif cmd_gpulse = '1' then
			fifo_din(31 downto 28) <= x"8";
			fifo_din(27 downto 24) <= par_chipid;
			fifo_din(23 downto 0) <= x"0000" & "00" & par_width;
			fifo_wren <= '1';
		elsif cmd_runmode = '1' then
			fifo_din(31 downto 28) <= x"9";
			fifo_din(27 downto 24) <= par_chipid;
			fifo_din(23 downto 1) <= (others => '0');
			fifo_din(0) <= par_runmode;
			fifo_wren <= '1';
		end if;
	end if;
end process;

-- count triggers and cal pulses
process begin
	wait until rising_edge(clk);

	if (reset = '1') or (trigcnt_enable = '0') then
		trigcnt <= (others => '0');
		calcnt <= (others => '0');
	else
		if cmd_lv1 = '1' then
			trigcnt <= trigcnt + 1;
		end if;
		if cmd_cal = '1' then
			calcnt <= calcnt + 1;
		end if;
	end if;
end process;

-- FIFO readout
fifo0_rd <= fifo_dout(7 downto 0) when fifo_empty = '0' else x"00";
fifo1_rd <= fifo_dout(15 downto 8) when fifo_empty = '0' else x"00";
fifo2_rd <= fifo_dout(23 downto 16) when fifo_empty = '0' else x"00";
fifo3_rd <= fifo_dout(31 downto 24) when fifo_empty = '0' else x"00";
fifo_rden <= fifo0_rden and not fifo_empty;

-- trigger counter readout
trigcnt3_rd <= std_logic_vector(trigcnt(31 downto 24));
trigcnt2_rd <= trigcnt_tmp(23 downto 16);
trigcnt1_rd <= trigcnt_tmp(15 downto 8);
trigcnt0_rd <= trigcnt_tmp(7 downto 0);

-- calpulse counter readout
calcnt3_rd <= std_logic_vector(calcnt(31 downto 24));
calcnt2_rd <= calcnt_tmp(23 downto 16);
calcnt1_rd <= calcnt_tmp(15 downto 8);
calcnt0_rd <= calcnt_tmp(7 downto 0);

-- copy trigger count to shadow register when reading the upper byte
process begin
	wait until rising_edge(clk);

	if trigcnt3_rden = '1' then
		trigcnt_tmp <= std_logic_vector(trigcnt(23 downto 0));
	end if;
	if calcnt3_rden = '1' then
		calcnt_tmp <= std_logic_vector(calcnt(23 downto 0));
	end if;
end process;

-- control register
csr_rd <= enable & trigcnt_enable & fifo_full & fifo_empty & sel;
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		sel <= x"0";
		enable <= '0';
		trigcnt_enable <= '0';
	else
		if csr_wren = '1' then
			sel <= csr_wr(3 downto 0);
			trigcnt_enable <= csr_wr(6);
			enable <= csr_wr(7);
		end if;
	end if;
end process;		

end architecture;