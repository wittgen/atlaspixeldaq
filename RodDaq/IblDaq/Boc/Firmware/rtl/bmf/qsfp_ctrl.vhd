library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity qsfp_ctrl is
	generic (
		f_clk : integer := 40000000;
		f_scl : integer := 100000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		-- lines to/from the qsfp
		qsfp_scl : inout std_logic;
		qsfp_sda : inout std_logic;
		qsfp_modsel_n : out std_logic;
		qsfp_lowpwr_n : out std_logic;
		qsfp_reset_n : out std_logic;
		qsfp_int_n : in std_logic;
		qsfp_present_n : in std_logic;

		-- access to register bank
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wren : in std_logic;
		csr_rden : in std_logic;
		addr_wr : in std_logic_vector(7 downto 0);
		addr_rd : out std_logic_vector(7 downto 0);
		addr_wren : in std_logic;
		addr_rden : in std_logic;
		data_wr : in std_logic_vector(7 downto 0);
		data_rd : out std_logic_vector(7 downto 0);
		data_wren : in std_logic;
		data_rden : in std_logic
	);
end qsfp_ctrl;


architecture rtl of qsfp_ctrl is
	component i2c_ctrl
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			i2c_rxdata : out std_logic_vector(7 downto 0);
			i2c_txdata : in std_logic_vector(7 downto 0);
			
			i2c_read : in std_logic;
			i2c_write : in std_logic;
			i2c_start : in std_logic;
			i2c_stop : in std_logic;
			
			i2c_ack_in : in std_logic;
			i2c_ack_out : out std_logic;
			
			busy : out std_logic;
			
			i2c_scl : inout std_logic;
			i2c_sda : inout std_logic
		);
	end component;

	-- signals to/from i2c controller
	signal i2c_txdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_rxdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_start : std_logic := '0';
	signal i2c_stop : std_logic := '0';
	signal i2c_read : std_logic := '0';
	signal i2c_write : std_logic := '0';
	signal i2c_ack_in : std_logic := '0';
	signal i2c_ack_out : std_logic := '0';
	signal i2c_busy : std_logic := '0';

	-- steering
	signal reg_read : std_logic := '0';
	signal reg_write : std_logic := '0';
	signal reg_addr : std_logic_vector(7 downto 0) := (others => '0');
	signal reg_rdata : std_logic_vector(7 downto 0) := (others => '0');
	signal reg_wdata : std_logic_vector(7 downto 0) := (others => '0');
	signal reg_busy : std_logic := '0';

	-- latch signals
	signal qsfp_present_reg : std_logic;
	signal qsfp_int_reg : std_logic;
	signal qsfp_reset_reg : std_logic;
	signal qsfp_lowpwr_reg : std_logic;

	-- i2c state machine
	type fsm_states is (st_idle, st_read0, st_read1, st_read2, st_read3, st_read4, st_write0, st_write1, st_write2, st_busywait);
	signal Z : fsm_states := st_idle;
	signal Z_return : fsm_states := st_idle;
begin

i2c_ctrl_i: i2c_ctrl
	generic map (
		f_clk => f_clk,
		f_scl => f_scl
	)
	port map (
		clk => clk,
		reset => reset,
		i2c_rxdata => i2c_rxdata,
		i2c_txdata => i2c_txdata,
		i2c_read => i2c_read,
		i2c_write => i2c_write,
		i2c_start => i2c_start,
		i2c_stop => i2c_stop,
		i2c_ack_in => i2c_ack_in,
		i2c_ack_out => i2c_ack_out,
		busy => i2c_busy,
		i2c_scl => qsfp_scl,
		i2c_sda => qsfp_sda
	);

-- state machine
process begin
	wait until rising_edge(clk);

	-- default
	i2c_start <= '0';
	i2c_stop <= '0';
	i2c_read <= '0';
	i2c_write <= '0';
	i2c_ack_in <= '0';

	if reset = '1' then
		Z <= st_idle;
		reg_rdata <= (others => '0');
	else
		case Z is
			when st_idle =>
				if reg_write = '1' then
					Z <= st_write0;
				elsif reg_read = '1' then
					Z <= st_read0;
				end if;

			when st_read0 =>
				i2c_txdata <= x"A0";
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_read1;
					Z <= st_busywait;
				end if;

			when st_read1 =>
				i2c_txdata <= reg_addr;
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_read2;
					Z <= st_busywait;
				end if;

			when st_read2 =>
				i2c_txdata <= x"A1";
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_read3;
					Z <= st_busywait;
				end if;

			when st_read3 =>
				i2c_stop <= '1';
				i2c_read <= '1';
				i2c_ack_in <= '1';
				if i2c_busy = '1' then
					Z_return <= st_read4;
					Z <= st_busywait;
				end if;

			when st_read4 =>
				reg_rdata <= i2c_rxdata;
				Z <= st_idle;	

			when st_write0 =>
				i2c_txdata <= x"A0";
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_write1;
					Z <= st_busywait;
				end if;

			when st_write1 =>
				i2c_txdata <= reg_addr;
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_write2;
					Z <= st_busywait;
				end if;

			when st_write2 =>
				i2c_txdata <= reg_wdata;
				i2c_stop <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z_return <= st_idle;
					Z <= st_busywait;
				end if;

			when st_busywait =>
				if i2c_busy = '0' then
					Z <= Z_return;
				end if;
		end case;
	end if;
end process;

-- busy from register reading/writing
reg_busy <= '0' when Z = st_idle else '1';

-- we allow only non-shared QSFP operation at the moment
qsfp_modsel_n <= '0';

-- register interface
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		qsfp_reset_reg <= '0';
		qsfp_lowpwr_reg <= '0';
		reg_wdata <= (others => '0');
		reg_addr <= (others => '0');
		reg_write <= '0';
		reg_read <= '0';
	else
		-- control register
		if (csr_wren = '1') then
			reg_read <= csr_wr(0);
			reg_write <= csr_wr(1);
			qsfp_reset_reg <= csr_wr(3);
			qsfp_lowpwr_reg <= csr_wr(4);
		end if;

		-- reset read/write flag when command is acknowledged
		if reg_busy = '1' then
			reg_read <= '0';
			reg_write <= '0';
		end if;

		-- write to data register when not busy
		if (data_wren = '1') and (reg_busy = '0') then
			reg_wdata <= data_wr;
		end if;

		-- same for addr
		if (addr_wren = '1') and (reg_busy = '0') then
			reg_addr <= addr_wr;
		end if;
	end if;
end process;

-- readback
csr_rd <= '0' & qsfp_int_reg & qsfp_present_reg & qsfp_lowpwr_reg & qsfp_reset_reg & reg_busy & reg_write & reg_read;
data_rd <= reg_rdata;
addr_rd <= reg_addr;

-- latch incoming signals
qsfp_present_reg <= (not qsfp_present_n) when rising_edge(clk);
qsfp_int_reg <= (not qsfp_int_n) when rising_edge(clk);

-- output internal signals
qsfp_reset_n <= (not qsfp_reset_reg) and (not reset);
qsfp_lowpwr_n <= not qsfp_lowpwr_reg;

end architecture;