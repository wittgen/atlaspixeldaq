library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity BERT is
	port (
		-- clocking and reset
		clk_regbank : in std_logic;
		clk_serial : in std_logic;
		reset : in std_logic;

		-- serial data output
		dout : out std_logic;

		-- serial data input
		din : in std_logic_vector(15 downto 0);
		din_valid : in std_logic_vector(15 downto 0);

		-- bit and error counter
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rden : in std_logic;
		csr_wren : in std_logic;
		cnt_rd : out std_logic_vector(7 downto 0);
		cnt_wr : in std_logic_vector(7 downto 0);
		cnt_rden : in std_logic;
		cnt_wren : in std_logic;
		pat_rd : out std_logic_vector(7 downto 0);
		pat_wr : in std_logic_vector(7 downto 0);
		pat_rden : in std_logic;
		pat_wren : in std_logic;

		-- debug output
		debug : out std_logic_vector(16 downto 0)
	);
end BERT;

architecture rtl of BERT is
	component BERTSender
		generic (
			prng32_seed : std_logic_vector := x"3fa91c20";
			prng8_seed : std_logic_vector := x"91"
		);
		port (
			-- clocking and reset
			clk_serial : in std_logic;
			reset : in std_logic;

			-- mode settings
			pattern_select : in std_logic_vector(1 downto 0);
			mcc_pattern_break : in std_logic_vector(5 downto 0);

			-- data output
			dout : out std_logic
		);
	end component;

	component BERTReceiver
		port (
			-- clocking and reset
			clk_serial : in std_logic;
			reset : in std_logic;

			-- serial data input
			din : in std_logic;
			din_valid : in std_logic;

			-- expected from sender
			expected_data : in std_logic;

			-- sync status
			sync : out std_logic;

			-- bit and error counter
			bitcnt : out std_logic_vector(47 downto 0);
			errcnt : out std_logic_vector(47 downto 0);

			-- latency output (for debugging)
			debug : out std_logic_vector(16 downto 0)
		);
	end component;

	-- counter signals
	signal bitcnt_clkserial : std_logic_vector(47 downto 0);
	signal errcnt_clkserial : std_logic_vector(47 downto 0);
	signal bitcnt_gray : std_logic_vector(47 downto 0);
	signal errcnt_gray : std_logic_vector(47 downto 0);
	signal bitcnt_clkregbank : std_logic_vector(47 downto 0);
	signal errcnt_clkregbank : std_logic_vector(47 downto 0);
	signal bitcnt : std_logic_vector(47 downto 0);
	signal errcnt : std_logic_vector(47 downto 0);

	-- data from sender
	signal sender_data : std_logic := '0';

	-- data to receiver
	signal receiver_data : std_logic := '0';
	signal receiver_data_valid : std_logic := '0';

	-- control and status bits
	signal enable : std_logic := '0';
	signal channel : std_logic_vector(3 downto 0);
	signal sync : std_logic := '0';

	-- pattern config
	signal pattern_select : std_logic_vector(1 downto 0) := "00";
	signal pattern_break : std_logic_vector(5 downto 0) := "000000";
	signal pattern_reg : std_logic_vector(7 downto 0) := x"00";

	-- soft reset
	signal softreset : std_logic := '0';

	-- shift register for counter readback
	signal cnt_firstread : std_logic := '0';
	signal cnt_sr : std_logic_vector(bitcnt'length+errcnt'length-9 downto 0) := (others => '0');
begin

sender_i: BERTSender
	port map (
		clk_serial => clk_serial,
		reset => softreset,
		pattern_select => pattern_select,
		mcc_pattern_break => pattern_break,
		dout => sender_data
	);

receiver_i: BERTReceiver
	port map (
		clk_serial => clk_serial,
		reset => softreset,
		din => receiver_data,
		din_valid => receiver_data_valid,
		expected_data => sender_data,
		sync => sync,
		bitcnt => bitcnt_clkserial,
		errcnt => errcnt_clkserial,
		debug => debug
	);

-- generate soft reset
softreset <= reset or not enable when rising_edge(clk_serial);

-- map incoming and outgoing data
dout <= sender_data when rising_edge(clk_serial);
receiver_data <= din(to_integer(unsigned(channel))) when rising_edge(clk_serial);
receiver_data_valid <= din_valid(to_integer(unsigned(channel))) when rising_edge(clk_serial);

-- pattern configuration
pattern_select <= pattern_reg(1 downto 0) when sync = '1' else "00";
pattern_break <= pattern_reg(7 downto 2);

-- freeze counters for readout
process begin
	wait until rising_edge(clk_serial);

	if cnt_wren = '1' then
		bitcnt_clkregbank <= bitcnt_clkserial;
		errcnt_clkregbank <= errcnt_clkserial;
	end if;
end process;

-- register in regbank clock domain
bitcnt <= bitcnt_clkregbank when rising_edge(clk_regbank);
errcnt <= errcnt_clkregbank when rising_edge(clk_regbank);

-- counter readback via shift register
process begin
	wait until rising_edge(clk_regbank);

	if cnt_wren = '1' then
		cnt_firstread <= '1';
	elsif cnt_rden = '1' then
		if cnt_firstread = '1' then
			cnt_sr <= bitcnt(bitcnt'left-8 downto 0) & errcnt;
			cnt_firstread <= '0';
		else
			cnt_sr <= cnt_sr(cnt_sr'left-8 downto 0) & x"00";
		end if;
	end if;
end process;
cnt_rd <= bitcnt(bitcnt'left downto bitcnt'left-7) when cnt_firstread = '1' else cnt_sr(cnt_sr'left downto cnt_sr'left-7);

-- control register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		enable <= '0';
		channel <= x"0";
	else
		if csr_wren = '1' then
			enable <= csr_wr(0);
			channel <= csr_wr(7 downto 4);
		end if;
	end if;
end process;
csr_rd <= channel & "00" & sync & enable;

-- pattern register
process begin
	wait until rising_edge(clk_regbank);

	if reset = '1' then
		pattern_reg <= x"00";
	else
		if pat_wren = '1' then
			pattern_reg <= pat_wr;
		end if;
	end if;
end process;
pat_rd <= pattern_reg;

end architecture;
