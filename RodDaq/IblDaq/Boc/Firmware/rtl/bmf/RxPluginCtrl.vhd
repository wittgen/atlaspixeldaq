library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity RxPluginCtrl is
	generic (
		f_clk : integer := 40000000;
		f_scl : integer := 100000;
		dac0_addr : std_logic_vector(6 downto 0) := "0110010";		-- 0x32
		dac1_addr : std_logic_vector(6 downto 0) := "1000000";		-- 0x40
		dac2_addr : std_logic_vector(6 downto 0) := "1000001";		-- 0x41
		dac3_addr : std_logic_vector(6 downto 0) := "0010000"		-- 0x10	(RGS DAC)
	);
	port (
		-- clocking and reset
		clk : in std_logic;
		reset : in std_logic;

		-- I2C interface
		i2c_scl : inout std_logic;
		i2c_sda : inout std_logic;

		-- register interface
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rden : in std_logic;
		csr_wren : in std_logic;
		data_lsb_rd : out std_logic_vector(7 downto 0);
		data_lsb_wr : in std_logic_vector(7 downto 0);
		data_lsb_rden : in std_logic;
		data_lsb_wren : in std_logic;
		data_msb_rd : out std_logic_vector(7 downto 0);
		data_msb_wr : in std_logic_vector(7 downto 0);
		data_msb_rden : in std_logic;
		data_msb_wren : in std_logic		
	);
end RxPluginCtrl;

architecture rtl of RxPluginCtrl is
	-- I2C low level interface
	component i2c_ctrl
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			i2c_rxdata : out std_logic_vector(7 downto 0);
			i2c_txdata : in std_logic_vector(7 downto 0);
			
			i2c_read : in std_logic;
			i2c_write : in std_logic;
			i2c_start : in std_logic;
			i2c_stop : in std_logic;
			
			i2c_ack_in : in std_logic;
			i2c_ack_out : out std_logic;
			
			busy : out std_logic;
			
			i2c_scl : inout std_logic;
			i2c_sda : inout std_logic
		);
	end component;

	-- signals to I2C low-level controller
	signal i2c_rxdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_txdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_read : std_logic := '0';
	signal i2c_write : std_logic := '0';
	signal i2c_start : std_logic := '0';
	signal i2c_stop : std_logic := '0';
	signal i2c_ack_in : std_logic := '0';
	signal i2c_ack_out : std_logic := '0';
	signal i2c_busy : std_logic := '0';
	signal i2c_error : std_logic := '0';

	-- FSM for writing the new register value
	type fsm_states is (st_idle, st_busywait, st_transfer0, st_transfer1, st_transfer2, st_transfer3, st_writeshadow);
	signal Z : fsm_states := st_idle;
	signal Z_return : fsm_states := st_idle;

	-- shadow registers for reading back threshold/gain values
	type shadow_regs_t is array (0 to 12) of std_logic_vector(15 downto 0);
	signal shadow_regs : shadow_regs_t := (others => (others => '0'));

	-- registers
	signal dac_data : std_logic_vector(15 downto 0) := (others => '0');
	signal dac_sel : std_logic_vector(1 downto 0) := "00";
	signal ch_sel : std_logic_vector(1 downto 0) := "00";
begin

-- I2C controller
i2c_ctrl_i: i2c_ctrl
	generic map (
		f_clk => f_clk,
		f_scl => f_scl
	)
	port map (
		clk => clk,
		reset => reset,
		i2c_rxdata => i2c_rxdata,
		i2c_txdata => i2c_txdata,
		i2c_read => i2c_read,
		i2c_write => i2c_write,
		i2c_start => i2c_start,
		i2c_stop => i2c_stop,
		i2c_ack_in => i2c_ack_in,
		i2c_ack_out => i2c_ack_out,
		busy => i2c_busy,
		i2c_scl => i2c_scl,
		i2c_sda => i2c_sda
	);

-- data register readback
process begin
	wait until rising_edge(clk);

	if dac_sel = "11" then
		data_msb_rd <= shadow_regs(12)(15 downto 8);
		data_lsb_rd <= shadow_regs(12)(7 downto 0);
	else
		data_msb_rd <= shadow_regs(4*to_integer(unsigned(dac_sel)) + to_integer(unsigned(ch_sel)))(15 downto 8);
		data_lsb_rd <= shadow_regs(4*to_integer(unsigned(dac_sel)) + to_integer(unsigned(ch_sel)))(7 downto 0);
	end if;
end process;

-- data register writing (only in idle state)
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		dac_data <= (others => '0');
	else
		if Z = st_idle then
			if data_msb_wren = '1' then
				dac_data(15 downto 8) <= data_msb_wr;
			end if;

			if data_lsb_wren = '1' then
				dac_data(7 downto 0) <= data_lsb_wr;
			end if;
		end if;
	end if;
end process;

-- readback control register
csr_rd(7 downto 6) <= dac_sel;
csr_rd(5 downto 4) <= ch_sel;
csr_rd(3) <= i2c_error;
csr_rd(2 downto 1) <= "00";
csr_rd(0) <= '0' when Z = st_idle else '1';

-- writing of control register
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		dac_sel <= "00";
		ch_sel <= "00";
	else
		if Z = st_idle then
			if csr_wren = '1' then
				dac_sel <= csr_wr(7 downto 6);
				ch_sel <= csr_wr(5 downto 4);
			end if;
		end if;
	end if;
end process;

-- FSM
process begin
	wait until rising_edge(clk);

	-- default assignments
	i2c_start <= '0';
	i2c_stop <= '0';
	i2c_read <= '0';
	i2c_write <= '0';
	i2c_ack_in <= '0';

	if reset = '1' then
		shadow_regs <= (others => (others => '0'));
		Z <= st_idle;
	else
		case Z is
			when st_idle =>
				if (csr_wr(0) = '1') and (csr_wren = '1') then
					Z <= st_transfer0;
				end if;

			when st_transfer0 =>
				-- select slave address
				if dac_sel = "00" then
					i2c_txdata <= dac0_addr & '0';
				elsif dac_sel = "01" then
					i2c_txdata <= dac1_addr & '0';
				elsif dac_sel = "10" then
					i2c_txdata <= dac2_addr & '0';
				else
					i2c_txdata <= dac3_addr & '0';
				end if;

				-- start transmission
				i2c_start <= '1';
				i2c_write <= '1';

				-- wait for command ack
				if i2c_busy = '1' then
					Z_return <= st_transfer1;
					Z <= st_busywait;
				end if;

			when st_transfer1 =>
				if dac_sel = "11" then
					i2c_txdata <= "00110000";		-- we need a different command for RGS
				else
					i2c_txdata <= "001000" & ch_sel;
				end if;

				-- start the transmission
				i2c_write <= '1';

				-- wait for command ack
				if i2c_busy = '1' then
					Z_return <= st_transfer2;
					Z <= st_busywait;
				end if;

			when st_transfer2 =>
				-- start transmission of data (msb)
				i2c_txdata <= dac_data(15 downto 8);
				i2c_write <= '1';
				
				-- wait for command ack
				if i2c_busy = '1' then
					Z_return <= st_transfer3;
					Z <= st_busywait;
				end if;				

			when st_transfer3 =>
				-- start transmission of data (lsb) and stop I2C communication
				i2c_txdata <= dac_data(7 downto 0);
				i2c_write <= '1';
				i2c_stop <= '1';
				
				-- wait for command ack
				if i2c_busy = '1' then
					Z_return <= st_writeshadow;
					Z <= st_busywait;
				end if;

			when st_writeshadow =>
				if dac_sel = "11" then
					shadow_regs(12) <= dac_data;
				else
					shadow_regs(4*to_integer(unsigned(dac_sel)) + to_integer(unsigned(ch_sel))) <= dac_data;
				end if;
				Z <= st_idle;

			when st_busywait =>
				if i2c_busy = '0' then
					i2c_error <= i2c_ack_out;
					Z <= Z_return;
				end if;
		end case;
	end if;
end process;


end architecture;
