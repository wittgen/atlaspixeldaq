library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity clocking_revBC is
  port (
	clk : in std_logic;
	reset : in std_logic;

	-- registers
	freq0_rd : out std_logic_vector(7 downto 0);
	freq0_wr : in std_logic_vector(7 downto 0);
	freq0_rden : in std_logic;
	freq0_wren : in std_logic;
	freq1_rd : out std_logic_vector(7 downto 0);
	freq1_wr : in std_logic_vector(7 downto 0);
	freq1_rden : in std_logic;
	freq1_wren : in std_logic;
	freq2_rd : out std_logic_vector(7 downto 0);
	freq2_wr : in std_logic_vector(7 downto 0);
	freq2_rden : in std_logic;
	freq2_wren : in std_logic;
	freq3_rd : out std_logic_vector(7 downto 0);
	freq3_wr : in std_logic_vector(7 downto 0);
	freq3_rden : in std_logic;
	freq3_wren : in std_logic;

	-- clock control
	vme_clk : in std_logic
  ) ;
end entity ; -- clocking_revBC

architecture RTL of clocking_revBC is

component ClkDetect
  generic (
  	f_clk : integer := 100000000;
  	f_min : integer := 39500000;
  	f_max : integer := 40500000;
  	speedup : integer := 1
  );
  port (
  	-- reference clock and reset
	clk : in std_logic;
	reset : in std_logic;

	-- measure
	meas_clk : in std_logic;

	-- status
	clk_good : out std_logic;
	clk_freq : out std_logic_vector(27 downto 0)
  ) ;
end component;

signal vme_clk_good : std_logic := '0';
signal vme_clk_freq : std_logic_vector(27 downto 0) := (others => '0');
signal freq_temp : std_logic_vector(19 downto 0) := (others => '0');

begin

ClkDetect_i: ClkDetect
	generic map (
		f_clk => 100000000,
		f_min => 39500000,
		f_max => 40500000,
		speedup => 1
	)
	port map (
		clk => clk,
		reset => reset,
		meas_clk => vme_clk,
		clk_good => vme_clk_good,
		clk_freq => vme_clk_freq
	);

-- frequency readback
process begin
	wait until rising_edge(clk);

	if freq0_rden = '1' then
		freq_temp <= vme_clk_freq(27 downto 8);
	end if;
end process;
freq0_rd <= vme_clk_freq(7 downto 0);
freq1_rd <= freq_temp(7 downto 0);
freq2_rd <= freq_temp(15 downto 8);
freq3_rd <= vme_clk_good & "000" & freq_temp(19 downto 16);

end architecture ; -- RTL