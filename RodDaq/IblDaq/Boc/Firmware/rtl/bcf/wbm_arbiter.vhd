----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				Wishbone Interconnect 4 way arbiter
-- Description:		Round-robin CLK synchronus arbiter, granting the Wishbone BUS to the Masters.
--							There is a pause of 1 CLK cycle per grant change.
----------------------------------------------------------------------------------
-- Changelog:
-- 18.02.2011 - Initial Version
-- 20.02.2011 - Changed IDLE state to go directly to mX state not mX_setup
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.bocpack.all;

----------------------------------------------------------------------
-- Entity declaration.
----------------------------------------------------------------------

entity wbm_arbiter is
    port(
            CLK:        in  std_logic;
            COMCYC:     out std_logic;
            CYC3:       in  std_logic;
            CYC2:       in  std_logic;
            CYC1:       in  std_logic;
            CYC0:       in  std_logic;
            GNT:        out std_logic_vector( 1 downto 0 );
            GNT3:       out std_logic;
            GNT2:       out std_logic;
            GNT1:       out std_logic;
            GNT0:       out std_logic;
            RST:        in  std_logic
         );

end entity wbm_arbiter;


----------------------------------------------------------------------
-- Architecture definition.
----------------------------------------------------------------------

architecture behavioral of wbm_arbiter is
	type arbiter_state_type is (idle, m0, m1, m2, m3, m0_setup, m1_setup, m2_setup, m3_setup);
	signal arbiter_state: arbiter_state_type := idle;
	
begin
	arbiter_fsm: process (clk, rst, CYC0, CYC1, CYC2, CYC3)
	begin
		if (rst = '1') then
			arbiter_state <= idle;			
		elsif (clk'event and clk ='1') then
			case arbiter_state is
			
				-- IDLE state, wait for cycle from masters
				when idle =>
					if (CYC0 = '1') then
						arbiter_state <= m0;
					elsif (CYC0 = '0' and CYC1 = '1') then
						arbiter_state <= m1;
					elsif (CYC0 = '0' and CYC1 = '0' and CYC2 = '1') then
						arbiter_state <= m2;
					elsif (CYC0 = '0' and CYC1 = '0' and CYC2 = '0' and CYC3 = '1') then
						arbiter_state <= m3;
					else
						arbiter_state <= idle;
					end if;
				
				-- Master0 owns the BUS
				when m0 =>
					if (CYC0 = '1') then
						arbiter_state <= m0;
					elsif (CYC0 = '0' and CYC1 = '1') then
						arbiter_state <= m1_setup;
					elsif (CYC0 = '0' and CYC1 = '0' and CYC2 = '1') then
						arbiter_state <= m2_setup;
					elsif (CYC0 = '0' and CYC1 = '0' and CYC2 = '0' and CYC3 = '1') then
						arbiter_state <= m3_setup;
					else
						arbiter_state <= idle;
					end if;
					
				-- Master1 owns the BUS	
				when m1 =>
					if (CYC1 = '1') then
						arbiter_state <= m1;
					elsif (CYC1 = '0' and CYC2 = '1') then
						arbiter_state <= m2_setup;
					elsif (CYC1 = '0' and CYC2 = '0' and CYC3 = '1') then
						arbiter_state <= m3_setup;
					elsif (CYC1 = '0' and CYC2 = '0' and CYC3 = '0' and CYC0 = '1') then
						arbiter_state <= m0_setup;
					else
						arbiter_state <= idle;
					end if;
				
				-- Master2 owns the BUS
				when m2 =>
					if (CYC2 = '1') then
						arbiter_state <= m2;
					elsif (CYC2 = '0' and CYC3 = '1') then
						arbiter_state <= m3_setup;
					elsif (CYC2 = '0' and CYC3 = '0' and CYC0 = '1') then
						arbiter_state <= m0_setup;
					elsif (CYC2 = '0' and CYC3 = '0' and CYC0 = '0' and CYC1 = '1') then
						arbiter_state <= m1_setup;
					else
						arbiter_state <= idle;
					end if;
				
				-- Master3 owns the BUS
				when m3 =>
					if (CYC3 = '1') then
						arbiter_state <= m3;
					elsif (CYC3 = '0' and CYC0 = '1') then
						arbiter_state <= m0_setup;
					elsif (CYC3 = '0' and CYC0 = '0' and CYC1 = '1') then
						arbiter_state <= m1_setup;
					elsif (CYC3 = '0' and CYC0 = '0' and CYC1 = '0' and CYC2 = '1') then
						arbiter_state <= m2_setup;
					else
						arbiter_state <= idle;
					end if;
					
				-- Master0 will own the BUS next CLK cycle
				when m0_setup => arbiter_state <= m0;
				
				-- Master1 will own the BUS next CLK cycle
				when m1_setup => arbiter_state <= m1;
				
				-- Master2 will own the BUS next CLK cycle
				when m2_setup => arbiter_state <= m2;
			
				-- Master3 will own the BUS next CLK cycle	
				when m3_setup => arbiter_state <= m3;
				
				when others => arbiter_state <= idle;
			end case;
		end if;
	end process arbiter_fsm;
	
	arbiter_output: process (arbiter_state)
	begin
		case arbiter_state is
			when idle =>
				COMCYC <= '0';
				GNT <= "00";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '0';
				
			when m0 =>
				COMCYC <= '1';
				GNT <= "00";
				GNT0 <= '1';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '0';
				
			when m1 =>
				COMCYC <= '1';
				GNT <= "01";
				GNT0 <= '0';
				GNT1 <= '1';
				GNT2 <= '0';
				GNT3 <= '0';
				
			when m2 =>
				COMCYC <= '1';
				GNT <= "10";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '1';
				GNT3 <= '0';
				
			when m3 =>
				COMCYC <= '1';
				GNT <= "11";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '1';
				
			when m0_setup =>
				COMCYC <= '0';
				GNT <= "00";
				GNT0 <= '1';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '0';
				
			when m1_setup =>
				COMCYC <= '0';
				GNT <= "01";
				GNT0 <= '0';
				GNT1 <= '1';
				GNT2 <= '0';
				GNT3 <= '0';
				
			when m2_setup =>
				COMCYC <= '0';
				GNT <= "10";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '1';
				GNT3 <= '0';
				
			when m3_setup =>
				COMCYC <= '0';
				GNT <= "11";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '1';
				
			when others =>
				COMCYC <= '0';
				GNT <= "00";
				GNT0 <= '0';
				GNT1 <= '0';
				GNT2 <= '0';
				GNT3 <= '0';
		end case;
	end process arbiter_output;
end architecture behavioral;

