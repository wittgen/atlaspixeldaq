-- serial number readout
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity DS2401_sernum is
	generic (
		f_clk : integer := 100000000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		dq : inout std_logic;

		sernum : out std_logic_vector(47 downto 0);
		sernum_ok : out std_logic;
		sernum_done : out std_logic;
		sernum_reset : in std_logic
	);
end DS2401_sernum;

architecture RTL of DS2401_sernum is
	-- device timing
	constant t_reset : integer := 750;		-- 750 us reset pulse
	constant t_presence : integer := 70;		-- 70 us wait for presence after reset
	constant t_presrecovery : integer := 500;	-- 500 us recovery time after presence
	constant t_recovery : integer := 10;		-- 10 us recovery time after data
	constant t_low : integer := 10;			-- 10 us low pulse
	constant t_write : integer := 70;		-- 70 us write time
	constant t_sample : integer := 20;		-- 20 us wait time for sampling
	constant t_read : integer := 120;		-- 120 us wait after sampling
	
	-- timer
	constant timer1us_max : integer := (f_clk / 1000000)-1;
	signal timer1us_cnt : integer range 0 to timer1us_max;
	signal timer1us_ce : std_logic := '0';
	
	-- wait timer
	signal waitcnt : integer range 0 to 1000 := 0;
	signal waittime : integer range 0 to 1000 := 0;
	signal waitstart : std_logic := '0';
	signal waitdone : std_logic := '1';
	
	-- state machine
	type fsm_states is (st_reset, st_presence0, st_presence1, st_tx0, st_tx1, st_tx2, st_rx0, st_rx1, st_rx2, st_rx3, st_check, st_done, st_wait);
	signal Z : fsm_states := st_reset;
	signal Z_waitreturn : fsm_states := st_reset;
	
	-- shift registers for TX / RX
	signal txsr : std_logic_vector(7 downto 0) := (others => '0');
	signal rxsr : std_logic_vector(63 downto 0) := (others => '0');
	signal bitcnt : integer range 0 to 64 := 0;
	
	-- IOB
	signal dq_in : std_logic := '0';
	signal dq_out : std_logic := '0';
	signal dq_oe : std_logic := '0';
begin

-- dq IOB
dq <= dq_out when dq_oe = '1' else 'Z';
dq_in <= dq;

-- generate 1 us timer
process begin
	wait until rising_edge(clk);
	
	if timer1us_cnt = timer1us_max then
		timer1us_ce <= '1';
		timer1us_cnt <= 0;
	else
		timer1us_ce <= '0';
		timer1us_cnt <= timer1us_cnt + 1;
	end if;
end process;

-- wait timer (with 1 us step)
process begin
	wait until rising_edge(clk);
	
	-- start timer?
	if waitstart = '1' then
		waitcnt <= waittime;
		waitdone <= '0';
	else
		if timer1us_ce = '1' then
			if waitcnt = 0 then
				waitdone <= '1';
			else
				waitcnt <= waitcnt - 1;
			end if;
		end if;
	end if;
end process;

-- controlling fsm
process begin
	wait until rising_edge(clk);
	
	-- default assignments
	waitstart <= '0';
	
	if (reset = '1') or (sernum_reset = '1') then
		Z <= st_reset;
	else
		case Z is
			when st_reset =>
				-- reset outputs
				sernum <= (others => '0');
				sernum_ok <= '0';
			
				-- generate reset pulse
				dq_out <= '0';
				dq_oe <= '1';
				waittime <= t_reset;
				waitstart <= '1';
				Z_waitreturn <= st_presence0;
				Z <= st_wait;
			
			when st_presence0 =>
				-- release dq and wait for presence pulse
				dq_out <= '1';
				dq_oe <= '0';
				waittime <= t_presence;
				waitstart <= '1';
				Z_waitreturn <= st_presence1;
				Z <= st_wait;
				
			when st_presence1 =>
				-- check for presence
				dq_out <= '1';
				dq_oe <= '0';
				
				if dq_in = '0' then
					-- found a DS2401 -> wait and send Read ROM command 0x0F
					txsr <= x"0F";
					waittime <= t_presrecovery;
					waitstart <= '1';
					Z_waitreturn <= st_tx0;
					Z <= st_wait;
				else
					-- no DS2401 found, resetting
					Z <= st_reset;
				end if;
				
			when st_tx0 =>
				-- generate low pulse
				dq_out <= '0';
				dq_oe <= '1';
				waittime <= t_low;
				waitstart <= '1';
				Z_waitreturn <= st_tx1;
				Z <= st_wait;
				
			when st_tx1 =>
				-- send 0/1
				if txsr(0) = '0' then
					-- send a zero
					dq_out <= '0';
					dq_oe <= '1';
				else
					-- send a one
					dq_out <= '1';
					dq_oe <= '0';
				end if;
				
				-- shift txsr
				txsr <= '0' & txsr(7 downto 1);
				
				waittime <= t_write;
				waitstart <= '1';
				Z_waitreturn <= st_tx2;
				Z <= st_wait;
				
			when st_tx2 =>
				-- recovery time
				dq_out <= '1';
				dq_oe <= '0';
				waittime <= t_recovery;
				waitstart <= '1';
				Z <= st_wait;
				
				if bitcnt = 7 then
					bitcnt <= 0;
					Z_waitreturn <= st_rx0;
				else
					bitcnt <= bitcnt + 1;
					Z_waitreturn <= st_tx0;
				end if;
				
			when st_rx0 =>
				-- generate low pulse
				dq_out <= '0';
				dq_oe <= '1';
				waittime <= t_low;
				waitstart <= '1';
				Z_waitreturn <= st_rx1;
				Z <= st_wait;
				
			when st_rx1 =>
				-- release data line
				dq_out <= '1';
				dq_oe <= '0';
				waittime <= t_sample;
				waitstart <= '1';
				Z_waitreturn <= st_rx2;
				Z <= st_wait;
				
			when st_rx2 =>
				-- sample data line
				rxsr <= dq_in & rxsr(63 downto 1);
				
				waittime <= t_read;
				waitstart <= '1';
				Z_waitreturn <= st_rx3;
				Z <= st_wait;
				
			when st_rx3 =>
				-- recovery time
				dq_out <= '1';
				dq_oe <= '0';
				waittime <= t_recovery;
				waitstart <= '1';
				Z <= st_wait;
				
				if bitcnt = 63 then
					bitcnt <= 0;
					Z_waitreturn <= st_check;
				else
					bitcnt <= bitcnt + 1;
					Z_waitreturn <= st_rx0;
				end if;
				
			when st_check =>
				-- check family code
				if rxsr(7 downto 0) = x"01" then
					sernum_ok <= '1';
				else
					sernum_ok <= '0';
				end if;
				Z <= st_done;
				
			when st_done =>
				sernum <= rxsr(55 downto 8);
				
			when st_wait =>
				-- if wait timer finished step into next state
				if (waitdone = '1') and (waitstart = '0') then
					Z <= Z_waitreturn;
				end if;
		end case;
	end if;
end process;

-- indicate done
sernum_done <= '1' when Z = st_done else '0';

end architecture;