----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim, Marius Wensing
-- E-Mail:				heim@physik.uni-wuppertal.de
--							wensing@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				Wishbone Interconnect Address Decoder
-- Description:		Coarse decode of the address and activation of the
--							the responsible Slave. Two types of activation 
--							signals, one direkt 1bit signal and an address like
--							2bit vector.
----------------------------------------------------------------------------------
-- Changelog:
-- 15.02.2011 - Initial Version
-- 20.02.2010 - Currently only 1 working Slave implented
-- 01.12.2011 - New mapping and rework of the code
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library work;
use work.bocpack.all;

entity wbs_addr_dec is
	port (
		-- Wishbone Address IN
		adr_i : IN std_logic_vector(15 downto 0);
		-- Wishbone activate out
		ACT : OUT std_logic_vector(1 downto 0);
		act0_o : OUT std_logic;
		act1_o : OUT std_logic;
		act2_o : OUT std_logic;
		act3_o : OUT std_logic
	);
end wbs_addr_dec;

architecture Behavioral of wbs_addr_dec is
begin

	------------------------------------------------
	-- Address Map:		       		      --
	-- Slave #		| Address MSB	| ACT --
	------------------------------------------------
	-- 0 (BCF-RegBank)	|     0x00      | 00  --
	-- 1 (BMF north)	|     0x01      | 01  --
	-- 2 (BMF south)	|     0x10      | 10  --
	-- 3 unused     	|     0x11      | 11  --
	------------------------------------------------
	
	---------------------
	-- Address Mapping --
	---------------------
	addr_decoder: process (adr_i)
	begin
		-- active slave
		act <= adr_i(adr_i'left downto adr_i'left-1);
		
		-- default is none active
		act0_o <= '0';
		act1_o <= '0';
		act2_o <= '0';
		act3_o <= '0';
		
		case adr_i(adr_i'left downto adr_i'left-1) is
			when "00" =>
				act0_o <= '1';
			when "01" =>
				act1_o <= '1';
			when "10" =>
				act2_o <= '1';
			when "11" =>
				act3_o <= '1';
			when others =>
				null;
		end case;
	end process;
	
end Behavioral;

