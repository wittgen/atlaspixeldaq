library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity i2c_eeprom is
	generic (
		f_clk : integer := 100000000;
		f_scl : integer := 100000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		
		scl : inout std_logic;
		sda : inout std_logic;
		
		csr_rd : out std_logic_vector(7 downto 0);
		csr_wr : in std_logic_vector(7 downto 0);
		csr_rden : in std_logic;
		csr_wren : in std_logic;
		
		data_rd : out std_logic_vector(7 downto 0);
		data_wr : in std_logic_vector(7 downto 0);
		data_rden : in std_logic;
		data_wren : in std_logic;
		
		addrl_rd : out std_logic_vector(7 downto 0);
		addrl_wr : in std_logic_vector(7 downto 0);
		addrl_rden : in std_logic;
		addrl_wren : in std_logic;
		addrh_rd : out std_logic_vector(7 downto 0);
		addrh_wr : in std_logic_vector(7 downto 0);
		addrh_rden : in std_logic;
		addrh_wren : in std_logic
	);
end i2c_eeprom;

architecture RTL of i2c_eeprom is
	component i2c_ctrl
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			i2c_rxdata : out std_logic_vector(7 downto 0);
			i2c_txdata : in std_logic_vector(7 downto 0);
			
			i2c_read : in std_logic;
			i2c_write : in std_logic;
			i2c_start : in std_logic;
			i2c_stop : in std_logic;
			
			i2c_ack_in : in std_logic;
			i2c_ack_out : out std_logic;
			
			busy : out std_logic;
			
			i2c_scl : inout std_logic;
			i2c_sda : inout std_logic
		);
	end component;
	
	signal i2c_start : std_logic := '0';
	signal i2c_stop : std_logic := '0';
	signal i2c_read : std_logic := '0';
	signal i2c_write : std_logic := '0';
	signal i2c_ack_in : std_logic := '0';
	signal i2c_ack_out : std_logic := '0';
	signal i2c_busy : std_logic := '0';
	signal i2c_rxdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_txdata : std_logic_vector(7 downto 0) := (others => '0');
	
	signal eeprom_addr : std_logic_vector(15 downto 0) := (others => '0');
	signal eeprom_read : std_logic := '0';
	signal eeprom_write : std_logic := '0';
	signal eeprom_busy : std_logic := '0';
	signal eeprom_readdata : std_logic_vector(7 downto 0) := (others => '0');
	signal eeprom_writedata : std_logic_vector(7 downto 0) := (others => '0');
	signal eeprom_error : std_logic := '0';
	
	signal i2c_addr : std_logic_vector(6 downto 0) := "1010100";
	
	type fsm_states is (st_idle, st_addr0, st_addr1, st_addr2, st_addr3, st_addr4, st_addr5,
			    st_read0, st_read1, st_read2, st_read3, st_write0, st_write1, st_busy0, st_busy1,
			    st_error0, st_error1, st_error2);
	signal Z : fsm_states := st_idle;
begin

i2c_ctrl_i: i2c_ctrl
	GENERIC MAP (
		f_clk => f_clk,
		f_scl => f_scl
	)
	PORT MAP (
		clk => clk,
		reset => reset,
		i2c_rxdata => i2c_rxdata,
		i2c_txdata => i2c_txdata,
		i2c_read => i2c_read,
		i2c_write => i2c_write,
		i2c_start => i2c_start,
		i2c_stop => i2c_stop,
		i2c_ack_in => i2c_ack_in,
		i2c_ack_out => i2c_ack_out,
		busy => i2c_busy,
		i2c_scl => scl,
		i2c_sda => sda
	);
	
-- register writes (only valid in idle state)
process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		eeprom_writedata <= (others => '0');
		eeprom_addr <= (others => '0');
	else
		if Z = st_idle then
			if addrl_wren = '1' then
				eeprom_addr(7 downto 0) <= addrl_wr;
			end if;
			
			if addrh_wren = '1' then
				eeprom_addr(15 downto 8) <= addrh_wr;
			end if;
			
			if data_wren = '1' then
				eeprom_writedata <= data_wr;
			end if;
		end if;
	end if;
end process;

-- read back registers
data_rd <= eeprom_readdata;
addrh_rd <= eeprom_addr(15 downto 8);
addrl_rd <= eeprom_addr(7 downto 0);
csr_rd <= "0000" & eeprom_error & eeprom_busy & eeprom_write & eeprom_read;
	
process begin
	wait until rising_edge(clk);
	
	-- default assignments
	i2c_read <= '0';
	i2c_write <= '0';
	i2c_start <= '0';
	i2c_stop <= '0';
	i2c_ack_in <= '0';
	
	if reset = '1' then
		Z <= st_idle;
	else
		case Z is
			when st_idle =>
				eeprom_read <= '0';
				eeprom_write <= '0';
				
				if csr_wren = '1' then
					if csr_wr(0) = '1' then
						eeprom_read <= '1';
						eeprom_error <= '0';
						Z <= st_addr0;
					elsif csr_wr(1) = '1' then
						eeprom_write <= '1';
						eeprom_error <= '0';
						Z <= st_addr0;
					elsif csr_wr(4) = '1' then
						i2c_addr <= eeprom_writedata(6 downto 0);
					elsif csr_wr(5) = '1' then
						eeprom_readdata <= '0' & i2c_addr;
					end if;
				end if;
				
			when st_addr0 =>
				-- send start condition and address for write
				i2c_txdata <= i2c_addr & '0';
				i2c_start <= '1';
				i2c_write <= '1';
				
				if i2c_busy = '1' then
					Z <= st_addr1;
				end if;
				
			when st_addr1 =>
				if i2c_busy = '0' then
					if i2c_ack_out = '1' then
						Z <= st_error0;
					else
						Z <= st_addr2;
					end if;
				end if;
				
			when st_addr2 =>
				-- send msb of address
				i2c_txdata <= eeprom_addr(15 downto 8);
				i2c_write <= '1';
				
				if i2c_busy = '1' then
					Z <= st_addr3;
				end if;
				
			when st_addr3 =>
				if i2c_busy = '0' then
					if i2c_ack_out = '1' then
						Z <= st_error0;
					else
						Z <= st_addr4;
					end if;
				end if;
				
			when st_addr4 =>
				-- send lsb of address
				i2c_txdata <= eeprom_addr(7 downto 0);
				i2c_write <= '1';
				
				if i2c_busy = '1' then
					Z <= st_addr5;
				end if;
				
			when st_addr5 =>
				if i2c_busy = '0' then
					if i2c_ack_out = '1' then
						Z <= st_error0;
					else
						if eeprom_read = '1' then
							Z <= st_read0;
						elsif eeprom_write = '1' then
							Z <= st_write0;
						end if;
					end if;
				end if;
				
			when st_read0 =>
				-- send addr + rep start for read
				i2c_txdata <= i2c_addr & '1';
				i2c_write <= '1';
				i2c_start <= '1';
				
				if i2c_busy = '1' then
					Z <= st_read1;
				end if;
				
			when st_read1 =>
				if i2c_busy = '0' then
					if i2c_ack_out = '1' then
						Z <= st_error0;
					else
						Z <= st_read2;
					end if;
					
				end if;
				
			when st_read2 =>
				-- read data byte with NAK and stop
				i2c_read <= '1';
				i2c_ack_in <= '1';
				i2c_stop <= '1';
				
				if i2c_busy = '1' then
					Z <= st_read3;
				end if;
				
			when st_read3 =>
				if i2c_busy = '0' then
					eeprom_readdata <= i2c_rxdata;
					Z <= st_idle;
				end if;
				
			when st_write0 =>
				-- send data + stop
				i2c_txdata <= eeprom_writedata;
				i2c_write <= '1';
				i2c_stop <= '1';
				
				if i2c_busy = '1' then
					Z <= st_write1;
				end if;
				
			when st_write1 =>
				if i2c_busy = '0' then
					Z <= st_busy0;
				end if;
				
			when st_busy0 =>
				-- poll acknowledge
				i2c_start <= '1';
				i2c_txdata <= i2c_addr & '0';
				i2c_write <= '1';
				i2c_stop <= '1';
				
				if i2c_busy = '1' then
					Z <= st_busy1;
				end if;
				
			when st_busy1 =>
				if i2c_busy = '0' then
					if i2c_ack_out = '0' then
						Z <= st_idle;
					else
						Z <= st_busy0;
					end if;
				end if;
				
			when st_error0 =>
				i2c_stop <= '1';
				
				if i2c_busy = '1' then
					Z <= st_error1;
				end if;
			
			when st_error1 =>
				if i2c_busy = '0' then
					Z <= st_error2;
				end if;
				
			when st_error2 =>
				eeprom_error <= '1';
				Z <= st_idle;
		end case;
	end if;
end process;

eeprom_busy <= '0' when Z = st_idle else '1';

end architecture;