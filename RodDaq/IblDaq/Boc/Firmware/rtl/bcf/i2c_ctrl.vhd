-- I2C controller
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity i2c_ctrl is
	generic (
		f_clk : integer := 100000000;
		f_scl : integer := 100000
	);
	port (
		clk : in std_logic;
		reset : in std_logic;

		i2c_rxdata : out std_logic_vector(7 downto 0);
		i2c_txdata : in std_logic_vector(7 downto 0);
		
		i2c_read : in std_logic;
		i2c_write : in std_logic;
		i2c_start : in std_logic;
		i2c_stop : in std_logic;
		
		i2c_ack_in : in std_logic;
		i2c_ack_out : out std_logic;
		
		busy : out std_logic;
		
		i2c_scl : inout std_logic;
		i2c_sda : inout std_logic
	);
end i2c_ctrl;

architecture RTL of i2c_ctrl is
	-- baudrate generator
	constant i2c_baudcnt_max : integer := ((f_clk / (3*f_scl))-1);
	signal i2c_baudcnt : integer range 0 to i2c_baudcnt_max := 0;
	signal i2c_baudcnt_ce : std_logic := '0';
	
	-- shift registers and bit counter
	signal rxsr : std_logic_vector(7 downto 0) := (others => '0');
	signal txsr : std_logic_vector(7 downto 0) := (others => '0');
	signal bitcnt : integer range 0 to 7 := 0;
	
	-- latch i2c_stop and ack_in
	signal i2c_stop_reg : std_logic := '0';
	signal i2c_ack_in_reg : std_logic := '0';
	
	-- i2c phy controller
	signal i2c_sda_out : std_logic := '0';
	signal i2c_sda_in : std_logic := '0';
	signal i2c_scl_out : std_logic := '0';
	signal i2c_scl_in : std_logic := '0';
	
	-- fsm
	type fsm_states is (st_idle, st_start0, st_start1, st_start2, st_start3, st_read0, st_read1, st_read2,
			    st_write0, st_write1, st_write2, st_readack0, st_readack1, st_readack2,
			    st_writeack0, st_writeack1, st_writeack2, st_stop0, st_stop1, st_stop2);
	signal Z : fsm_states := st_idle;
begin

-- I2C physical layer
scl_buf: IOBUF
	port map (
		I => i2c_scl_out,
		O => i2c_scl_in,
		T => '0',
		IO => i2c_scl
	);

sda_buf: IOBUF
	port map (
		I => '0',
		O => i2c_sda_in,
		T => i2c_sda_out,
		IO => i2c_sda
	);

-- baudrate generation
process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		i2c_baudcnt <= 0;
		i2c_baudcnt_ce <= '0';
	else
		if i2c_baudcnt = i2c_baudcnt_max then
			i2c_baudcnt_ce <= '1';
			i2c_baudcnt <= 0;
		else
			i2c_baudcnt_ce <= '0';
			i2c_baudcnt <= i2c_baudcnt + 1;
		end if;
	end if;
end process;

-- fsm
process begin
	wait until rising_edge(clk);
	
	if reset = '1' then
		i2c_sda_out <= '1';
		i2c_scl_out <= '1';
		i2c_rxdata <= (others => '0');
		i2c_ack_out <= '0';
		bitcnt <= 0;
		Z <= st_idle;
	else
		if i2c_baudcnt_ce = '1' then
			case Z is
				when st_idle =>
					-- latch i2c_stop and ack_in for later use
					i2c_stop_reg <= i2c_stop;
					i2c_ack_in_reg <= i2c_ack_in;
					
					if i2c_write = '1' then
						txsr <= i2c_txdata;
						
						if i2c_start = '1' then
							Z <= st_start0;
						else
							Z <= st_write0;
						end if;
					elsif i2c_read = '1' then
						Z <= st_read0;
					elsif i2c_stop = '1' then
						Z <= st_stop0;
					end if;
				
				when st_start0 =>
					i2c_sda_out <= '1';
					Z <= st_start1;
				
				when st_start1 =>
					i2c_scl_out <= '1';
					Z <= st_start2;
					
				when st_start2 =>
					i2c_sda_out <= '0';
					Z <= st_start3;
				
				when st_start3 =>
					i2c_scl_out <= '0';
					Z <= st_write0;
					
				when st_write0 =>
					-- put first bit on the data line
					i2c_sda_out <= txsr(7);
					
					-- shift data
					txsr <= txsr(6 downto 0) & '0';
					
					-- rest
					i2c_scl_out <= '0';
					Z <= st_write1;
					
				when st_write1 =>
					i2c_scl_out <= '1';
					Z <= st_write2;
					
				when st_write2 =>
					i2c_scl_out <= '0';
					
					if bitcnt = 7 then
						bitcnt <= 0;
						Z <= st_readack0;
					else
						bitcnt <= bitcnt + 1;
						Z <= st_write0;
					end if;
					
				when st_read0 =>
					i2c_sda_out <= '1';
					i2c_scl_out <= '0';
					Z <= st_read1;
					
				when st_read1 =>
					i2c_scl_out <= '1';
					Z <= st_read2;
					
				when st_read2 =>
					rxsr <= rxsr(6 downto 0) & i2c_sda_in;
					i2c_scl_out <= '0';
					
					if bitcnt = 7 then
						bitcnt <= 0;
						Z <= st_writeack0;
					else
						bitcnt <= bitcnt + 1;
						Z <= st_read0;
					end if;
					
				when st_readack0 =>
					i2c_scl_out <= '0';
					i2c_sda_out <= '1';
					Z <= st_readack1;
					
				when st_readack1 =>
					i2c_scl_out <= '1';
					i2c_sda_out <= '1';
					Z <= st_readack2;
					
				when st_readack2 =>
					i2c_ack_out <= i2c_sda_in;
					i2c_sda_out <= '1';
					i2c_scl_out <= '0';
					
					if i2c_stop_reg = '1' then
						Z <= st_stop0;
					else
						Z <= st_idle;
					end if;
					
				when st_writeack0 =>
					-- write back read data
					i2c_rxdata <= rxsr;
				
					i2c_sda_out <= i2c_ack_in_reg;
					i2c_scl_out <= '0';
					Z <= st_writeack1;
					
				when st_writeack1 =>
					i2c_sda_out <= i2c_ack_in_reg;
					i2c_scl_out <= '1';
					Z <= st_writeack2;
					
				when st_writeack2 =>
					i2c_sda_out <= i2c_ack_in_reg;
					i2c_scl_out <= '0';
					
					if i2c_stop_reg = '1' then
						Z <= st_stop0;
					else
						Z <= st_idle;
					end if;
					
				when st_stop0 =>
					i2c_sda_out <= '0';
					Z <= st_stop1;
					
				when st_stop1 =>
					i2c_scl_out <= '1';
					Z <= st_stop2;
					
				when st_stop2 =>
					i2c_sda_out <= '1';
					Z <= st_idle;
			end case;
		end if;
	end if;
end process;


-- generate busy
busy <= '0' when Z = st_idle else '1';

end architecture;
