-- power on reset
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity por is
	generic (
		f_clk : integer := 100000000;		-- 100 MHz CPU speed
		t_por_ms : integer := 1000			-- 1000 ms power on reset time
	);
	port (
		clk : in std_logic;
		reset : out std_logic
	);
end por;

architecture RTL of por is
	constant cnt_max : integer := ((f_clk/1000) * t_por_ms)-1;
	signal cnt : integer range 0 to cnt_max := 0;
	signal reset_int : std_logic := '1';
begin

process begin
	wait until rising_edge(clk);

	if cnt = cnt_max then
		reset_int <= '0';
	else
		reset_int <= '1';
		cnt <= cnt + 1;
	end if;
end process;

reset <= reset_int;

end architecture;
