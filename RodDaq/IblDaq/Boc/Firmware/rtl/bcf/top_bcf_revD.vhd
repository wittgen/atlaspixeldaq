----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				BCF top module
-- Description:			Port routing of BCF
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 29.11.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity top_bcf is
	port (
		-- BCF clocking
		CLK100_BCF_P : IN std_logic;
		CLK100_BCF_N : IN std_logic;
		--CLK40_BCF_P : IN std_logic;
		--CLK40_BCF_N : IN std_logic;

		-- BOC clock control (PLL, input delay, etc.)
		VME_PLL_MR : OUT std_logic;
		VME_PLL_CKSEL : OUT std_logic;
		VME_CLK_SEL : OUT std_logic;
		VME_CLK_P : IN std_logic;
		VME_CLK_N : IN std_logic;
		CLK40_Input_Delay : OUT std_logic_vector(10 downto 0);

		-- Ethernet Phy
		GMII_ETH_RXD : IN std_logic_vector(7 downto 0);
		GMII_ETH_TXD : OUT std_logic_vector(7 downto 0);
		GMII_ETH_MDIO : INOUT std_logic;
		GMII_ETH_MDC : OUT std_logic;
		GMII_ETH_TX_CLK : IN std_logic;
		GMII_ETH_RX_CLK : IN std_logic;
		GMII_ETH_GTX_CLK : OUT std_logic;
--		GMII_ETH_GBE_MCLK : IN std_logic;
		GMII_ETH_TX_EN : OUT std_logic;
		GMII_ETH_TX_ER : OUT std_logic;
		GMII_ETH_RX_ER : IN std_logic;
		GMII_ETH_RX_DV : IN std_logic;
		GMII_ETH_COL : IN std_logic;
		GMII_ETH_CRS : IN std_logic;
		GMII_ETH_RST : OUT std_logic;

		-- BCF flash
		SPI_CCLK : INOUT std_logic;
		SPI_MOSI : INOUT std_logic;
		SPI_MISO : INOUT std_logic;
		SPI_CSO : INOUT std_logic;

		-- DDR 2
		DDR2_BCF_A : OUT std_logic_vector(12 downto 0);
		DDR2_BCF_DQ : INOUT std_logic_vector(15 downto 0);
		DDR2_BCF_BA : OUT std_logic_vector(1 downto 0);
		DDR2_BCF_CKE : OUT std_logic;
		DDR2_BCF_WE : OUT std_logic;
		DDR2_BCF_ODT : OUT std_logic;
		DDR2_BCF_RAS : OUT std_logic;
		DDR2_BCF_CAS : OUT std_logic;
		DDR2_BCF_UDM0 : OUT std_logic;
		DDR2_BCF_LDM0 : OUT std_logic;
		DDR2_BCF_CLK0_P : OUT std_logic;
		DDR2_BCF_CLK0_N : OUT std_logic;
		DDR2_BCF_LDQS0_P : INOUT std_logic;
		DDR2_BCF_LDQS0_N : INOUT std_logic;   
		DDR2_BCF_UDQS0_P : INOUT std_logic;
		DDR2_BCF_UDQS0_N : INOUT std_logic;
		DDR2_BCF_RZQ : INOUT std_logic;
		
		-- Interface to southern BMF (Southbone)
		SCI_BCF2S6S_DONE : IN std_logic;
		SCI_BCF2S6S_INIT : OUT std_logic;
		TCK_BCF2S6S	: OUT std_logic;
		TDO_BCF2S6S : OUT std_logic;
		TMS_BCF2S6S : OUT std_logic;
		TDI_S6S2BCF : IN std_logic;
		BOC_ADD_BCF2S6S : OUT std_logic_vector(11 downto 0);
		BOC_ACK_BCF2S6S : IN std_logic;
		BOC_WE_BCF2S6S : OUT std_logic;
		BOC_STB_BCF2S6S : OUT std_logic;
		BOC_CYC_BCF2S6S : OUT std_logic;
		BOC_DAT_BCF2S6S : INOUT std_logic_vector(7 downto 0);
		
		-- Interface to northern BMF (Northbone)
		SCI_BCF2S6N_DONE : IN std_logic;
		SCI_BCF2S6N_INIT : OUT std_logic;
		TCK_BCF2S6N	: OUT std_logic;
		TDO_BCF2S6N : OUT std_logic;
		TMS_BCF2S6N : OUT std_logic;
		TDI_S6N2BCF : IN std_logic;
		BOC_ADD_BCF2S6N : OUT std_logic_vector(11 downto 0);
		BOC_ACK_BCF2S6N : IN std_logic;
		BOC_WE_BCF2S6N : OUT std_logic;
		BOC_STB_BCF2S6N : OUT std_logic;
		BOC_CYC_BCF2S6N : OUT std_logic;
		BOC_DAT_BCF2S6N : INOUT std_logic_vector(7 downto 0);		
	
		-- Setup Bus
		BOC_ADD_ROD2BCF : IN std_logic_vector(15 downto 0);
		BOC_DAT_ROD2BCF : INOUT std_logic_vector(7 downto 0);
		SWRN : IN std_logic;
		SSTBN : IN std_logic;
		SBUSY : INOUT std_logic;

		-- busy signal to TIM
		BUSY : OUT std_logic;

		-- interlock signals
		DET_LAS_EN_i : IN std_logic;
		BOC_LAS_EN_i : IN std_logic;

		-- USB to serial converter
		USB_RX : IN std_logic;
		USB_TX : OUT std_logic;
		
		-- Flash A
		CFG_FLASH_A_SI : OUT std_logic;
		CFG_FLASH_A_SO : IN std_logic;
		CFG_FLASH_A_SCLK : OUT std_logic;
		CFG_FLASH_A_WP : OUT std_logic; -- low active
		CFG_FLASH_A_HOLD : OUT std_logic; --low active
		CFG_FLASH_A_CS : OUT std_logic; -- low active
		
		-- Flash B
		CFG_FLASH_B_SI : OUT std_logic;
		CFG_FLASH_B_SO : IN std_logic;
		CFG_FLASH_B_SCLK : OUT std_logic;
		CFG_FLASH_B_WP : OUT std_logic; -- low active
		CFG_FLASH_B_HOLD : OUT std_logic; --low active
		CFG_FLASH_B_CS : OUT std_logic; -- low active
		
		-- Buttons
		GPIO_BCF_SW1 : IN std_logic;
		GPIO_BCF_SW2 : IN std_logic;

		-- LEDs
		GPIO_LED_BCF : INOUT std_logic_vector(2 downto 0);

		-- I2C port expander
		BCF_GPOEX_SDA : INOUT std_logic;
		BCF_GPOEX_SCL : INOUT std_logic;
		
		-- JTAG connector for BMF
		TCK_CON2BCF : IN std_logic;
		TMS_CON2BCF : IN std_logic;
		TDI_CON2BCF : IN std_logic;
		TDO_BCF2CON : OUT std_logic;
		
		-- Other
		SDA_EEPROM : INOUT std_logic;
		SCL_EEPROM : INOUT std_logic;
		BCF_LASER_EN	: OUT std_logic;
		SERNUM : INOUT std_logic;
		MGT_REF_CLK_FS : OUT  std_logic_vector(2 downto 0);
		BOC_OK : OUT std_logic
	);
end top_bcf;

architecture structure of top_bcf is

	---------------------------------
	-- Component Declaration Begin --
	---------------------------------
	
	component bcf_main
	port (
		-- clocking
		clk_p : in std_logic;
		clk_n : in std_logic;

		-- reset
		reset_in : in std_logic;
		reset_out : out std_logic;

		-- Setup Bus
		sb_data : inout std_logic_vector(7 downto 0);
		sb_addr : in std_logic_vector(15 downto 0);
		sb_stb_n : in std_logic;
		sb_wrt_n : in std_logic;
		sb_busy : out std_logic;
		
		-- Northbone
		wb_bmf_n_ack : in std_logic;
		wb_bmf_n_dat_i : in std_logic_vector(7 downto 0);
		wb_bmf_n_dat_o : out std_logic_vector(7 downto 0);
		wb_bmf_n_adr : out std_logic_vector(15 downto 0);
		wb_bmf_n_stb : out std_logic;
		wb_bmf_n_cyc : out std_logic;
		wb_bmf_n_we : out std_logic;
		
		-- JTAG north
		jtag_n_done : in std_logic;
		jtag_n_tck : out std_logic;
		jtag_n_tms : out std_logic;
		jtag_n_tdi : out std_logic;
		jtag_n_tdo : in std_logic;
		
		-- SPI flash north
		spi_n_sck : out std_logic;
		spi_n_so : in std_logic;
		spi_n_si : out std_logic;
		spi_n_cs_n : out std_logic;
		spi_n_wp_n : out std_logic;
		spi_n_hold_n : out std_logic;
		
		-- Southbone
		wb_bmf_s_ack : in std_logic;
		wb_bmf_s_dat_i : in std_logic_vector(7 downto 0);
		wb_bmf_s_dat_o : out std_logic_vector(7 downto 0);
		wb_bmf_s_adr : out std_logic_vector(15 downto 0);
		wb_bmf_s_stb : out std_logic;
		wb_bmf_s_cyc : out std_logic;
		wb_bmf_s_we : out std_logic;
		
		-- JTAG south
		jtag_s_done : in std_logic;
		jtag_s_tck : out std_logic;
		jtag_s_tms : out std_logic;
		jtag_s_tdi : out std_logic;
		jtag_s_tdo : in std_logic;
		
		-- SPI flash south
		spi_s_sck : out std_logic;
		spi_s_so : in std_logic;
		spi_s_si : out std_logic;
		spi_s_cs_n : out std_logic;
		spi_s_wp_n : out std_logic;
		spi_s_hold_n : out std_logic;
		
		-- UART
		uart_tx : out std_logic;
		uart_rx : in std_logic;
		
		-- I2C
		i2c_scl : inout std_logic;
		i2c_sda : inout std_logic;

		-- serial number
		sernum : inout std_logic;
		
		-- ethernet (GMII interface)
		gmii_gtx_clk : out std_logic;
		gmii_tx_clk : in std_logic;
		gmii_tx_en : out std_logic;
		gmii_tx_er : out std_logic;
		gmii_txd : out std_logic_vector(7 downto 0);
		gmii_rx_clk : in std_logic;
		gmii_rx_dv : in std_logic;
		gmii_rx_er: in std_logic;
		gmii_rxd : in std_logic_vector(7 downto 0);
		gmii_crs : in std_logic;
		gmii_col : in std_logic;
		gmii_phy_rstb : out std_logic;
		gmii_mdc : out std_logic;
		gmii_mdio : inout std_logic;
		
		-- clocking
		vme_clk : in std_logic;
		vme_clk_sel : out std_logic;
		vme_pll_mr : out std_logic;
		vme_pll_cksel : out std_logic;
		vme_idelay : out std_logic_vector(10 downto 0);
		
		-- ddr2 memory
		ddr2_dq : inout std_logic_vector(15 downto 0) := (others => 'Z');
		ddr2_a : out std_logic_vector(12 downto 0) := (others => '0');
		ddr2_ba : out std_logic_vector(1 downto 0) := "00";
		ddr2_ras_n : out std_logic := '1';
		ddr2_cas_n : out std_logic := '1';
		ddr2_we_n : out std_logic := '1';
		ddr2_cke : out std_logic := '0';
		ddr2_odt : out std_logic := '0';
		ddr2_ldm : out std_logic := '0';
		ddr2_udm : out std_logic := '0';
		ddr2_ldqs_p : inout std_logic := '0';
		ddr2_ldqs_n : inout std_logic := '0';
		ddr2_udqs_p : inout std_logic := '0';
		ddr2_udqs_n : inout std_logic := '0';
		ddr2_clk_p : out std_logic := '0';
		ddr2_clk_n : out std_logic := '0';
		ddr2_rzq : inout std_logic := '0';

		-- laser enable
		laser_enable : out std_logic := '0';
		boc_las_en : in std_logic;
		det_las_en : in std_logic;

		-- gpio led
		gpio_led : inout std_logic_vector(2 downto 0);

		-- MGT frequency selection
		mgt_fs : out std_logic_vector(2 downto 0);

		-- I2C port expander
		i2c_gpio_scl : inout std_logic;
		i2c_gpio_sda : inout std_logic;

		-- BCF SPI flash
		bcf_flash_sck : inout std_logic;
		bcf_flash_miso : inout std_logic;
		bcf_flash_mosi : inout std_logic;
		bcf_flash_cs_n : inout std_logic;

		-- BOC OK status
		boc_ok : out std_logic;

		-- JTAG
		jtag_switch : out std_logic_vector(1 downto 0);
		jtag_xvc_tck : out std_logic;
		jtag_xvc_tms : out std_logic;
		jtag_xvc_tdi : out std_logic;
		jtag_xvc_tdo : in std_logic
	);
	end component;
		
	-------------------------------
	-- Component Declaration End --
	-------------------------------
	
	----------------------------
	-- Internal Signals Begin --
	----------------------------

	signal vme_clk : std_logic := '0';
	
	-- northbone
	signal wb_bmf_n_ack : std_logic := '0';
	signal wb_bmf_n_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_bmf_n_dat_o  : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_bmf_n_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal wb_bmf_n_stb : std_logic := '0';
	signal wb_bmf_n_cyc : std_logic := '0';
	signal wb_bmf_n_we : std_logic := '0';
	
	-- southbone
	signal wb_bmf_s_ack : std_logic := '0';
	signal wb_bmf_s_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_bmf_s_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wb_bmf_s_adr : std_logic_vector(15 downto 0) := (others => '0');
	signal wb_bmf_s_stb : std_logic := '0';
	signal wb_bmf_s_cyc : std_logic := '0';
	signal wb_bmf_s_we : std_logic := '0';
	
	-- JTAG north
	signal jtag_n_done : std_logic := '0';
	signal jtag_n_tck : std_logic := '0';
	signal jtag_n_tms : std_logic := '0';
	signal jtag_n_tdo : std_logic := '0';
	signal jtag_n_tdi : std_logic := '0';
	
	-- JTAG south
	signal jtag_s_done : std_logic := '0';
	signal jtag_s_tck : std_logic := '0';
	signal jtag_s_tms : std_logic := '0';
	signal jtag_s_tdo : std_logic := '0';
	signal jtag_s_tdi : std_logic := '0';
	
	signal uart_rx : std_logic := '1';
	signal uart_tx : std_logic := '1';

	signal jtag_switch : std_logic_vector(1 downto 0) := "00";

	signal jtag_xvc_tck : std_logic := '0';
	signal jtag_xvc_tms : std_logic := '0';
	signal jtag_xvc_tdi : std_logic := '0';
	signal jtag_xvc_tdo : std_logic := '0';
	
	--------------------------
	-- Internal Signals End --
	--------------------------
	
begin

	---------------
	-- Southbone --
	---------------

	BOC_ADD_BCF2S6S <= wb_bmf_s_adr(11 downto 0);					-- only lower 12 bits of address are used
	BOC_DAT_BCF2S6S <= wb_bmf_s_dat_o when wb_bmf_s_we = '1' else (others => 'Z');	-- tristate output buffers for southbone data lines
	wb_bmf_s_dat_i <= BOC_DAT_BCF2S6S;						-- read input buffer of southbone data lines
	wb_bmf_s_ack <= BOC_ACK_BCF2S6S;						-- acknowledge from south BMF
	BOC_WE_BCF2S6S <= wb_bmf_s_we;							-- write enable to south BMF
	BOC_STB_BCF2S6S <= wb_bmf_s_stb;						-- strobe to south BMF
	BOC_CYC_BCF2S6S <= wb_bmf_s_cyc;						-- cycle to south BMF
	
	---------------
	-- Northbone --
	---------------

	BOC_ADD_BCF2S6N <= wb_bmf_n_adr(11 downto 0);					-- only lower 12 bits of address are used
	BOC_DAT_BCF2S6N <= wb_bmf_n_dat_o when wb_bmf_n_we = '1' else (others => 'Z');	-- tristate output buffers for southbone data lines
	wb_bmf_n_dat_i <= BOC_DAT_BCF2S6N;						-- read input buffer of southbone data lines
	wb_bmf_n_ack <= BOC_ACK_BCF2S6N;						-- acknowledge from south BMF
	BOC_WE_BCF2S6N <= wb_bmf_n_we;							-- write enable to south BMF
	BOC_STB_BCF2S6N <= wb_bmf_n_stb;						-- strobe to south BMF
	BOC_CYC_BCF2S6N <= wb_bmf_n_cyc;						-- cycle to south BMF

	----------------------
	-- jtag connections --
	----------------------
    TCK_BCF2S6S <= TCK_CON2BCF when jtag_switch = "01" else
    			   jtag_xvc_tck when jtag_switch = "10" else
    			   jtag_s_tck;
    TMS_BCF2S6S <= TMS_CON2BCF when jtag_switch = "01" else
    			   jtag_xvc_tms when jtag_switch = "10" else
    			   jtag_s_tms;
    TCK_BCF2S6N <= TCK_CON2BCF when jtag_switch = "01" else
    			   jtag_xvc_tck when jtag_switch = "10" else
    			   jtag_n_tck;
    TMS_BCF2S6N <= TMS_CON2BCF when jtag_switch = "01" else
    			   jtag_xvc_tms when jtag_switch = "10" else
    			   jtag_n_tms;
    TDO_BCF2S6S <= TDI_CON2BCF when jtag_switch = "01" else
    			   jtag_xvc_tdi when jtag_switch = "10" else
    			   jtag_s_tdi;
    TDO_BCF2S6N <= jtag_n_tdi when jtag_switch = "00" else TDI_S6S2BCF;
    TDO_BCF2CON <= TDI_S6N2BCF when jtag_switch = "01" else '0';
    jtag_xvc_tdo <= TDI_S6N2BCF when jtag_switch = "10" else '0';
    jtag_s_tdo <= TDI_S6S2BCF when jtag_switch = "00" else '0';
    jtag_n_tdo <= TDI_S6N2BCF when jtag_switch = "00" else '0';
	uart_rx <= '1';
	
	-- init signals
	SCI_BCF2S6S_INIT <= '1';
	SCI_BCF2S6N_INIT <= '1';

	-----------------------
	-- other assignments --
	-----------------------
	BUSY <= 'Z';					-- keep BUSY high-impedance with pull-up
	
	-------------------------
	-- Instantiation Begin --
	-------------------------
	
	bcf_main_i: bcf_main PORT MAP(
		clk_p => CLK100_BCF_P,
		clk_n => CLK100_BCF_N,
		reset_in => GPIO_BCF_SW1,
		reset_out => open,
		sb_data => BOC_DAT_ROD2BCF,
		sb_addr => BOC_ADD_ROD2BCF,
		sb_stb_n => SSTBN,
		sb_wrt_n => SWRN,
		sb_busy => SBUSY,
		wb_bmf_n_ack => wb_bmf_n_ack,
		wb_bmf_n_dat_i => wb_bmf_n_dat_i,
		wb_bmf_n_dat_o => wb_bmf_n_dat_o,
		wb_bmf_n_adr => wb_bmf_n_adr,
		wb_bmf_n_stb => wb_bmf_n_stb,
		wb_bmf_n_cyc => wb_bmf_n_cyc,
		wb_bmf_n_we => wb_bmf_n_we,
		wb_bmf_s_ack => wb_bmf_s_ack,
		wb_bmf_s_dat_i => wb_bmf_s_dat_i,
		wb_bmf_s_dat_o => wb_bmf_s_dat_o,
		wb_bmf_s_adr => wb_bmf_s_adr,
		wb_bmf_s_stb => wb_bmf_s_stb,
		wb_bmf_s_cyc => wb_bmf_s_cyc,
		wb_bmf_s_we => wb_bmf_s_we,
		jtag_n_done => SCI_BCF2S6N_DONE,
		jtag_n_tck => jtag_n_tck,
		jtag_n_tms => jtag_n_tms,
		jtag_n_tdo => jtag_n_tdo,
		jtag_n_tdi => jtag_n_tdi,
		jtag_s_done => SCI_BCF2S6S_DONE,
		jtag_s_tck => jtag_s_tck,
		jtag_s_tms => jtag_s_tms,
		jtag_s_tdo => jtag_s_tdo,
		jtag_s_tdi => jtag_s_tdi,		
		spi_n_sck => CFG_FLASH_A_SCLK,
		spi_n_so => CFG_FLASH_A_SO,
		spi_n_si => CFG_FLASH_A_SI,
		spi_n_cs_n => CFG_FLASH_A_CS,
		spi_n_wp_n => CFG_FLASH_A_WP,
		spi_n_hold_n => CFG_FLASH_A_HOLD,
		spi_s_sck => CFG_FLASH_B_SCLK,
		spi_s_so => CFG_FLASH_B_SO,
		spi_s_si => CFG_FLASH_B_SI,
		spi_s_cs_n => CFG_FLASH_B_CS,
		spi_s_wp_n => CFG_FLASH_B_WP,
		spi_s_hold_n => CFG_FLASH_B_HOLD,		
		uart_tx => USB_TX,
		uart_rx => USB_RX,
		gmii_gtx_clk => GMII_ETH_GTX_CLK,
		gmii_tx_clk => GMII_ETH_TX_CLK,
		gmii_tx_en => GMII_ETH_TX_EN,
		gmii_tx_er => GMII_ETH_TX_ER,
		gmii_txd => GMII_ETH_TXD,
		gmii_rx_clk => GMII_ETH_RX_CLK,
		gmii_rx_dv => GMII_ETH_RX_DV,
		gmii_rx_er => GMII_ETH_RX_ER,
		gmii_rxd => GMII_ETH_RXD,
		gmii_col => GMII_ETH_COL,
		gmii_crs => GMII_ETH_CRS,
		gmii_phy_rstb => GMII_ETH_RST,
		gmii_mdc => GMII_ETH_MDC,
		gmii_mdio => GMII_ETH_MDIO,
		vme_clk => vme_clk,
		vme_clk_sel => VME_CLK_SEL,
		vme_pll_mr => VME_PLL_MR,
		vme_pll_cksel => VME_PLL_CKSEL,
		vme_idelay => CLK40_Input_Delay,
		laser_enable => BCF_LASER_EN,
		det_las_en => DET_LAS_EN_i,
		boc_las_en => BOC_LAS_EN_i,
		ddr2_dq => DDR2_BCF_DQ,
		ddr2_a => DDR2_BCF_A,
		ddr2_ba => DDR2_BCF_BA,
		ddr2_ras_n => DDR2_BCF_RAS,
		ddr2_cas_n => DDR2_BCF_CAS,
		ddr2_we_n => DDR2_BCF_WE,
		ddr2_cke => DDR2_BCF_CKE,
		ddr2_odt => DDR2_BCF_ODT,
		ddr2_ldm => DDR2_BCF_LDM0,
		ddr2_udm => DDR2_BCF_UDM0,
		ddr2_ldqs_p => DDR2_BCF_LDQS0_P,
		ddr2_ldqs_n => DDR2_BCF_LDQS0_N,
		ddr2_udqs_p => DDR2_BCF_UDQS0_P,
		ddr2_udqs_n => DDR2_BCF_UDQS0_N,
		ddr2_clk_p => DDR2_BCF_CLK0_P,
		ddr2_clk_n => DDR2_BCF_CLK0_N,
		ddr2_rzq => DDR2_BCF_RZQ,
		i2c_sda => SDA_EEPROM,
		i2c_scl => SCL_EEPROM,
		gpio_led => GPIO_LED_BCF,
		mgt_fs => MGT_REF_CLK_FS,
		i2c_gpio_scl => BCF_GPOEX_SCL,
		i2c_gpio_sda => BCF_GPOEX_SDA,
		bcf_flash_sck => SPI_CCLK,
		bcf_flash_mosi => SPI_MOSI,
		bcf_flash_miso => SPI_MISO,
		bcf_flash_cs_n => SPI_CSO,
		boc_ok => BOC_OK,
		jtag_switch => jtag_switch,
		jtag_xvc_tdo => jtag_xvc_tdo,
		jtag_xvc_tdi => jtag_xvc_tdi,
		jtag_xvc_tms => jtag_xvc_tms,
		jtag_xvc_tck => jtag_xvc_tck,
		sernum => SERNUM
	);

	-- vme clock buffer
	vme_clk_buf: IBUFDS
		PORT MAP (
			I => VME_CLK_P,
			IB => VME_CLK_N,
			O => vme_clk
		);

	-----------------------
	-- Instantiation End --
	-----------------------
	
end structure;
