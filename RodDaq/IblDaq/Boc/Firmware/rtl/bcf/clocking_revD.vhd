library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity clocking_revD is
  port (
	clk : in std_logic;
	reset : in std_logic;

	-- registers
	csr_rd : out std_logic_vector(7 downto 0);
	csr_wr : in std_logic_vector(7 downto 0);
	csr_rden : in std_logic;
	csr_wren : in std_logic;
	freq0_rd : out std_logic_vector(7 downto 0);
	freq0_wr : in std_logic_vector(7 downto 0);
	freq0_rden : in std_logic;
	freq0_wren : in std_logic;
	freq1_rd : out std_logic_vector(7 downto 0);
	freq1_wr : in std_logic_vector(7 downto 0);
	freq1_rden : in std_logic;
	freq1_wren : in std_logic;
	freq2_rd : out std_logic_vector(7 downto 0);
	freq2_wr : in std_logic_vector(7 downto 0);
	freq2_rden : in std_logic;
	freq2_wren : in std_logic;
	freq3_rd : out std_logic_vector(7 downto 0);
	freq3_wr : in std_logic_vector(7 downto 0);
	freq3_rden : in std_logic;
	freq3_wren : in std_logic;
	idelay0_rd : out std_logic_vector(7 downto 0);
	idelay0_wr : in std_logic_vector(7 downto 0);
	idelay0_rden : in std_logic;
	idelay0_wren : in std_logic;
	idelay1_rd : out std_logic_vector(7 downto 0);
	idelay1_wr : in std_logic_vector(7 downto 0);
	idelay1_rden : in std_logic;
	idelay1_wren : in std_logic;

	-- clock control
	vme_clk : in std_logic;
	vme_clk_sel : out std_logic;
	vme_pll_mr : out std_logic;
	vme_pll_cksel : out std_logic;
	vme_idelay : out std_logic_vector(10 downto 0);
	vme_clk_good : out std_logic;
	on_vme_clock : out std_logic;
	secure_reg : in std_logic
  ) ;
end entity ; -- clocking_revD

architecture RTL of clocking_revD is

component ClkDetect
  generic (
  	f_clk : integer := 100000000;
  	f_min : integer := 39500000;
  	f_max : integer := 40500000;
  	speedup : integer := 1;
   	glitchdet_enable : boolean := false;
  	glitchdet_cntmax : integer := 127
  );
  port (
  	-- reference clock and reset
	clk : in std_logic;
	reset : in std_logic;

	-- measure
	meas_clk : in std_logic;

	-- status
	clk_good : out std_logic;
	clk_freq : out std_logic_vector(27 downto 0)
  ) ;
end component;

signal vme_clk_good_int : std_logic := '0';
signal vme_clk_freq : std_logic_vector(27 downto 0) := (others => '0');
signal vme_clk_sel_int : std_logic := '1';
signal vme_pll_cksel_int : std_logic := '1';
signal vme_idelay_int : std_logic_vector(10 downto 0);
signal freq_temp : std_logic_vector(19 downto 0) := (others => '0');

begin

ClkDetect_i: ClkDetect
	generic map (
		f_clk => 100000000,
		f_min => 39500000,
		f_max => 40500000,
		speedup => 1,
		glitchdet_cntmax => 31,
		glitchdet_enable => true
	)
	port map (
		clk => clk,
		reset => reset,
		meas_clk => vme_clk,
		clk_good => vme_clk_good_int,
		clk_freq => vme_clk_freq
	);

-- vme master reset
vme_pll_mr <= reset;

-- switch-over to VME clock only allowed if clk_good indicator is active
process begin
	wait until rising_edge(clk);

	if (reset = '1') or (vme_clk_good_int = '0') then
		vme_clk_sel_int <= '1';
		vme_pll_cksel_int <= '1';
	else
		if (csr_wren = '1') and (secure_reg = '1') then
			if csr_wr(1 downto 0) = "11" then
				vme_clk_sel_int <= '1';
				vme_pll_cksel_int <= '1';
			else
				if vme_clk_good_int = '1' then
					vme_clk_sel_int <= csr_wr(0);
					vme_pll_cksel_int <= csr_wr(1);
				end if;
			end if;
		end if;
	end if;
end process;

-- status register readback
csr_rd <= "00000" & vme_clk_good_int & vme_pll_cksel_int & vme_clk_sel_int;

-- frequency readback
process begin
	wait until rising_edge(clk);

	if freq0_rden = '1' then
		freq_temp <= vme_clk_freq(27 downto 8);
	end if;
end process;
freq0_rd <= vme_clk_freq(7 downto 0);
freq1_rd <= freq_temp(7 downto 0);
freq2_rd <= freq_temp(15 downto 8);
freq3_rd <= "0000" & freq_temp(19 downto 16);

-- idelay readback
idelay0_rd <= vme_idelay_int(7 downto 0);
idelay1_rd <= "00000" & vme_idelay_int(10 downto 8);

-- idelay write
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		vme_idelay_int <= (others => '0');
	else
		if idelay0_wren = '1' then
			vme_idelay_int(7 downto 0) <= idelay0_wr;
		end if;
		if idelay1_wren = '1' then
			vme_idelay_int(10 downto 8) <= idelay1_wr(2 downto 0);
		end if;
	end if;
end process;

-- export internal signals
vme_pll_cksel <= vme_pll_cksel_int;
vme_clk_sel <= vme_clk_sel_int;
vme_idelay <= vme_idelay_int;
vme_clk_good <= vme_clk_good_int;
on_vme_clock <= '0' when ((vme_clk_sel_int = '1') and (vme_pll_cksel_int = '1')) else '1';

end architecture ; -- RTL