----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:18:50 01/30/2012 
-- Design Name: 
-- Module Name:    ePLLUnit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ePLLUnit is
	generic (
		-- input clock frequency
		f_clk : natural := 40000000
	);
	port (
		-- clock and reset
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		-- control register
		ctrl_rd : out std_logic_vector(7 downto 0) := (others => '0');
		ctrl_wr : in std_logic_vector(7 downto 0) := (others => '0');
		ctrl_wren : in std_logic := '0';
		
		-- status register
		status_rd : out std_logic_vector(7 downto 0) := (others => '0');
		
		-- data registers
		data0_rd : out std_logic_vector(7 downto 0) := (others => '0');
		data0_wr : in std_logic_vector(7 downto 0) := (others => '0');
		data0_wren : in std_logic := '0';
		data1_rd : out std_logic_vector(7 downto 0) := (others => '0');
		data1_wr : in std_logic_vector(7 downto 0) := (others => '0');
		data1_wren : in std_logic := '0';
		data2_rd : out std_logic_vector(7 downto 0) := (others => '0');
		data2_wr : in std_logic_vector(7 downto 0) := (others => '0');
		data2_wren : in std_logic := '0';
		data3_rd : out std_logic_vector(7 downto 0) := (others => '0');
		data3_wr : in std_logic_vector(7 downto 0) := (others => '0');
		data3_wren : in std_logic := '0';
		
		-- input delay registers
		idelay0_rd : out std_logic_vector(7 downto 0) := (others => '0');
		idelay0_wr : in std_logic_vector(7 downto 0) := (others => '0');
		idelay0_wren : in std_logic := '0';
		idelay1_rd : out std_logic_vector(7 downto 0) := (others => '0');
		idelay1_wr : in std_logic_vector(7 downto 0) := (others => '0');
		idelay1_wren : in std_logic := '0';
		
		-- interface to PLL
		pll_sck : out std_logic := '0';
		pll_mosi : out std_logic := '0';
		pll_miso : in std_logic := '0';
		pll_cs_n : out std_logic := '1';
		pll_locked : in std_logic := '0';
		pll_idelay : out std_logic_vector(10 downto 0) := (others => '0')
	);
end ePLLUnit;

architecture Behavioral of ePLLUnit is
	COMPONENT PLLSPI
		generic (
			f_clk : natural := 80000000;
			f_sck : natural := 10000000
		);
		port (
			clk_i : in std_logic := '0';
			rst_i : in std_logic := '0';
			
			sck : out std_logic := '0';
			miso : in std_logic := '0';
			mosi : out std_logic := '0';
			cs_n : out std_logic := '1';
			
			txdat : in std_logic_vector(31 downto 0) := (others => '0');
			rxdat : out std_logic_vector(31 downto 0) := (others => '0');
			
			transceive : in std_logic := '0';
			busy : out std_logic := '0'
		);
	END COMPONENT;
	
	-- spi signals
	signal spi_txdat : std_logic_vector(31 downto 0) := (others => '0');
	signal spi_rxdat : std_logic_vector(31 downto 0) := (others => '0');
	signal spi_transceive : std_logic := '0';
	signal spi_busy : std_logic := '0';
	
	-- default configuration
	-- create 40 MHz out of external 40 MHz (just buffer)
	constant pll_default_reg0 : std_logic_vector(27 downto 0) := x"FF7000E";
	constant pll_default_reg1 : std_logic_vector(27 downto 0) := x"839F21E";
	
	-- state machine
	type cStates is (init0, init1, init2, init3, idle, write0, write1, read0, read1, read2, read3,
			 reset0, reset1, reset2, reset3, reset4, reset5, reset6, reset7, reset8, reset9);
	signal Z : cStates := init0;
	signal busy : std_logic := '0';
	
	-- pll_locked shift registers
	signal pll_locked_sr : std_logic_vector(3 downto 0) := "0000";
	
	signal pll_addr : std_logic_vector(3 downto 0) := (others => '0');
	signal pll_data : std_logic_vector(27 downto 0) := (others => '0');
	signal data0_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal data1_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal data2_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal data3_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal idelay_reg : std_logic_vector(10 downto 0) := (others => '0');

	-- temporary register for read-modify-write
	signal temp : std_logic_vector(27 downto 0) := (others => '0');
begin

-- instantiate SPI controller running @ 1 MHz clock frequency
PLLSPI_i: PLLSPI
	GENERIC MAP(
		f_clk => f_clk,
		f_sck => 1000000
	)
	PORT MAP(
		clk_i => clk_i,
		rst_i => rst_i,
		sck => pll_sck,
		miso => pll_miso,
		mosi => pll_mosi,
		cs_n => pll_cs_n,
		txdat => spi_txdat,
		rxdat => spi_rxdat,
		transceive => spi_transceive,
		busy => spi_busy
	);
	
-- state machine
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		Z <= init0;
	else
		case Z is
			when init0 =>
				-- write default value to register 0 of the PLL
				spi_txdat <= pll_default_reg0 & "0000";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= init1;
				end if;
				
			when init1 =>
				-- wait for end of transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= init2;
				end if;
				
			when init2 =>
				-- write default value to register 1 of the PLL
				spi_txdat <= pll_default_reg1 & "0001";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= init3;
				end if;
				
			when init3 =>
				-- wait for end of transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= reset0;	-- reset the PLL
				end if;
				
			when idle =>
				-- idle state (wait for 'command')
				if ctrl_wren = '1' then
					pll_addr <= ctrl_wr(3 downto 0);					-- take register address from control reg
					pll_data <= data3_reg(3 downto 0) & data2_reg & data1_reg & data0_reg;	-- tage register data from data registers
					
					if ctrl_wr(4) = '1' then
						Z <= read0;		-- do a read cycle
					elsif ctrl_wr(5) = '1' then
						Z <= write0;		-- do a write cycle
					elsif ctrl_wr(6) = '1' then
						Z <= reset0;		-- reset the PLL
					end if;
				end if;
				
			when write0 =>
				-- write register data to PLL
				spi_txdat <= pll_data & pll_addr;
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= write1;
				end if;
				
			when write1 =>
				-- wait for the transmission to finish
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= idle;
				end if;
				
			when read0 =>
				-- prepare the read cycle
				spi_txdat <= x"000000" & pll_addr & "1110";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= read1;
				end if;
				
			when read1 =>
				-- wait for the transmission to finish
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= read2;
				end if;
				
			when read2 =>
				-- transmit a dummy byte to read the data from the PLL
				spi_txdat <= x"00000000";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= read3;
				end if;
				
			when read3 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					-- writeback the data
					data0_rd <= spi_rxdat(11 downto 4);
					data1_rd <= spi_rxdat(19 downto 12);
					data2_rd <= spi_rxdat(27 downto 20);
					data3_rd <= "0000" & spi_rxdat(31 downto 28);
					
					Z <= idle;
				end if;

			when reset0 =>
				-- read command for register 2 for read-modify-write
				spi_txdat <= x"0000002E";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= reset1;
				end if;

			when reset1 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= reset2;
				end if;

			when reset2 =>
				-- read register 2
				spi_txdat <= x"00000000";
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= reset3;
				end if;

			when reset3 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					temp <= spi_rxdat(31 downto 4) or x"0102000";		-- set PLLRESET and CALSELECT
					Z <= reset4;
				end if;

			when reset4 =>
				-- write register 2 with PLLRESET and CALSELECT set
				spi_txdat <= (temp & x"2");
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= reset5;
				end if;

			when reset5 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					temp <= temp and not x"0100000";	-- reset PLLRESET
					Z <= reset6;
				end if;

			when reset6 =>
				-- write register 2 with only CALSELECT set
				spi_txdat <= (temp & x"2");
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= reset7;
				end if;

			when reset7 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					temp <= temp or x"0100000";		-- set PLLRESET
					Z <= reset8;
				end if;

			when reset8 =>
				-- write the old register content
				spi_txdat <= (temp & x"2");
				spi_transceive <= '1';
				if spi_busy = '1' then
					Z <= reset9;
				end if;

			when reset9 =>
				-- wait for the end of the transmission
				spi_transceive <= '0';
				if spi_busy = '0' then
					Z <= idle;
				end if;
		end case;
	end if;
end process;

-- we are busy if we are not in idle state
busy <= '0' when Z = idle else '1';

-- data and idelay registers
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		data0_reg <= (others => '0');
		data1_reg <= (others => '0');
		data2_reg <= (others => '0');
		data3_reg <= (others => '0');
		idelay_reg <= (others => '0');
	else
		if data0_wren = '1' then
			data0_reg <= data0_wr;
		end if;
		
		if data1_wren = '1' then
			data1_reg <= data1_wr;
		end if;
		
		if data2_wren = '1' then
			data2_reg <= data2_wr;
		end if;
		
		if data3_wren = '1' then
			data3_reg <= data3_wr;
		end if;
		
		if idelay0_wren = '1' then
			idelay_reg(7 downto 0) <= idelay0_wr;
		end if;
		
		if idelay1_wren = '1' then
			idelay_reg(10 downto 8) <= idelay1_wr(2 downto 0);
		end if;
	end if;
end process;

-- synchronize locked signal
process begin
	wait until rising_edge(clk_i);
	
	pll_locked_sr <= pll_locked_sr(2 downto 0) & pll_locked;
end process;

-- status register output
status_rd(1) <= pll_locked_sr(3);
status_rd(0) <= busy;
		

end Behavioral;

