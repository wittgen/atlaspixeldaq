library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bcf_gpmem is
	port (
		clk : in std_logic;

		wb_addr : in std_logic_vector(15 downto 0);
		wb_dat_i : in std_logic_vector(7 downto 0);
		wb_dat_o : out std_logic_vector(7 downto 0);
		wb_cyc : in std_logic;
		wb_stb : in std_logic;
		wb_we : in std_logic;
		wb_ack : out std_logic
	);
end bcf_gpmem;

architecture rtl of bcf_gpmem is
	component gp_bram
		PORT (
			clka : IN STD_LOGIC;
			wea : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
			addra : IN STD_LOGIC_VECTOR(13 DOWNTO 0);
			dina : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
			douta : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
		);
	end component;

	signal wea : std_logic := '0';
	signal tmp : std_logic := '0';	-- temp register to delay acknowledge
begin

bram_i: gp_bram
	port map (
		clka => clk,
		wea(0) => wea,
		addra => wb_addr(13 downto 0),
		dina => wb_dat_i,
		douta => wb_dat_o
	);

-- write enable
wea <= wb_cyc and wb_stb and wb_we;

-- generate acknowledge
process begin
	wait until rising_edge(clk);

	wb_ack <= tmp;

	if (wb_cyc = '1') and (wb_stb = '1') then
		tmp <= '1';
	else
		tmp <= '0';
	end if;
end process;

end architecture;