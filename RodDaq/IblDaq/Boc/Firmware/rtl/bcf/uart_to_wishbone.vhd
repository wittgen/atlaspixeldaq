----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				UART 2 WB Master
-- Description:		Bridges a UART connection onto a wishbone master interface.
--							A Transaction package consists of a SOF, Address 
--							and data payload.
----------------------------------------------------------------------------------
-- Changelog:
-- 12.05.2011 - Initial Version
-- 16.05.2011 - Tested and works for single read/write
-- 15.06.2011 - Adding an EOF to fix continous readout
-- 05.12.2011 - Modified Frame structure to 16bit address
----------------------------------------------------------------------------------
-- TODO:
-- - Block Transfer
---------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library work;
use work.bocpack.all;

entity uart_to_wishbone is
	port (
		-- Sys con
		sys_clk : in std_logic;
		sys_rst : in std_logic;
		-- UART
		uart_tx	: out std_logic;
		uart_rx	: in std_logic;
		-- Wishbone
		wb_uart_cyc_o	: out std_logic;
		wb_uart_stb_o	: out std_logic;
		wb_uart_we_o	: out std_logic;
		wb_uart_data_o	: out std_logic_vector(wbbus_Dbits-1 downto 0);
		wb_uart_adr_o	: out std_logic_vector(wbbus_Abits-1 downto 0);
		wb_uart_data_i	: in std_logic_vector(wbbus_Dbits-1 downto 0);
		wb_uart_ack_i	: in std_logic
		);
end uart_to_wishbone;

architecture Behavirioal of uart_to_wishbone is

	-- Internal UART signals
	signal rx_data_out : std_logic_vector(7 downto 0);
	signal tx_data_in : std_logic_vector(7 downto 0);
	
	signal rx_data_rdy : std_logic;
	signal tx_data_en	: std_logic;
	signal tx_busy : std_logic;
	
	signal rx_data_err : std_logic;
	
	-- Internal Frame signals
	signal sof_frame : std_logic_vector(7 downto 0);
	signal adr_frame_low : std_logic_vector(7 downto 0);
	signal adr_frame_high : std_logic_vector(7 downto 0);
	signal data_frame : std_logic_vector(7 downto 0);
	signal eof_frame : std_logic_vector(7 downto 0);
	
	signal fmt : std_logic_vector(1 downto 0);
	signal payload : std_logic_vector(3 downto 0);
	
	constant flag : std_logic_vector(2 downto 0) := "101";
	constant eof : std_logic_vector(7 downto 0) := x"3c";
	
	-- Wishbone handshake
	signal wb_en : std_logic;
	

	COMPONENT uart_core
	PORT(
		reset : IN std_logic;
		clk : IN std_logic;
		rx_data_serial : IN std_logic;
		tx_data_in : IN std_logic_vector(7 downto 0);
		tx_data_en : IN std_logic;          
		rx_data_out : OUT std_logic_vector(7 downto 0);
		rx_data_rdy : OUT std_logic;
		rx_data_err : OUT std_logic;
		tx_data_serial : OUT std_logic;
		tx_busy : OUT std_logic
		);
	END COMPONENT;
	
	type rx_state_type is (rx_idle, rx_dw1, rx_dw2, rx_dw3, rx_dw4, rx_setup, rx_wait);
	signal rx_state : rx_state_type := rx_idle;
	
	type wb_state_type is (wb_idle, wb_read, wb_read_stop, wb_setup_write, wb_write);
	signal wb_state : wb_state_type := wb_idle;
	
begin

	-- Wishbone FSM reacting on a full Transaction package
	wb_fsm: process (sys_clk, sys_rst, wb_uart_ack_i, tx_busy, fmt)
	begin
		if (sys_rst = '1' or rx_data_err = '1') then
			wb_state <= wb_idle;
			tx_data_in <= (others => '0');
			
		elsif (sys_clk'event and sys_clk = '1') then
			case wb_state is
				
				when wb_idle =>
					tx_data_en <= '0';
					if (wb_en = '1') then
						if (fmt(0) = '1') then
							wb_state <= wb_read;
						elsif (fmt(1) = '1') then
							wb_state <= wb_write;
						end if;
					else
						wb_state <= wb_idle;
					end if;
					
				when wb_read =>
					if (wb_uart_ack_i = '1') then
						tx_data_in <= wb_uart_data_i;
						wb_state <= wb_read_stop;
						tx_data_en <= '1';
					else
						wb_state <= wb_read;
						tx_data_en <= '0';
					end if;
					
				when wb_read_stop =>
					tx_data_en <= '0';
					if (tx_busy = '1') then
						wb_state <= wb_idle;
					else
						wb_state <= wb_read_stop;
					end if;
					
				when wb_write =>
					tx_data_en <= '0';
					if (wb_uart_ack_i = '1') then
						wb_state <= wb_idle;
					else
						wb_state <= wb_write;
					end if;
					
				when others =>
					tx_data_en <= '0';
					wb_state <= wb_idle;
			end case;
		end if;
	end process wb_fsm;
	
	-- Wishbone Outputs
	wb_uart_adr_o(15 downto 8) <= adr_frame_high;
	wb_uart_adr_o(7 downto 0) <= adr_frame_low;
	wb_uart_data_o <= data_frame;
	
	wb_dec: process (wb_state)
	begin
		case wb_state is
			when wb_idle =>
				wb_uart_cyc_o <= '0';
				wb_uart_stb_o <= '0';
				wb_uart_we_o <= '0';
				
			when wb_read =>
				wb_uart_cyc_o <= '1';
				wb_uart_stb_o <= '1';
				wb_uart_we_o <= '0';
				
			when wb_read_stop =>
				wb_uart_cyc_o <= '0';
				wb_uart_stb_o <= '0';
				wb_uart_we_o <= '0';
				
			when wb_write =>
				wb_uart_cyc_o <= '1';
				wb_uart_stb_o <= '1';
				wb_uart_we_o <= '1';
				
			when others =>
				wb_uart_cyc_o <= '0';
				wb_uart_stb_o <= '0';
				wb_uart_we_o <= '0';
				
		end case;
	end process wb_dec;
	
	-- Package decoding
	fmt <= sof_frame(1 downto 0);
	payload <= sof_frame(5 downto 2);
	
	rx_dec: process (sys_clk, sys_rst, rx_data_out, rx_data_rdy, rx_data_err, wb_uart_ack_i)
	begin
		if(sys_rst = '1' or rx_data_err = '1') then
			rx_state <= rx_idle;
			sof_frame <= (others => '0');
			data_frame <= (others => '0');
			adr_frame_high <= (others => '0');
			adr_frame_low <= (others => '0');
			eof_frame <= (others => '0');
			
		elsif (sys_clk'event and sys_clk = '1') then
			case rx_state is
				when rx_idle =>
					wb_en <= '0';
					if (rx_data_rdy = '1') then
						sof_frame <= rx_data_out;
						rx_state <= rx_dw1;
					else
						rx_state <= rx_idle;
					end if;
					
				when rx_dw1 =>
					wb_en <= '0';
					if (rx_data_err = '0') then
						if ( rx_data_rdy = '1') then
							adr_frame_high <= rx_data_out;
							rx_state <= rx_dw2;
						else
							rx_state <= rx_dw1;
						end if;
					else
						rx_state <= rx_idle;
					end if;
					
				when rx_dw2 =>
					wb_en <= '0';
					if (rx_data_err = '0') then
						if ( rx_data_rdy = '1') then
							adr_frame_low <= rx_data_out;
							if (fmt = "10") then
								rx_state <= rx_dw3;
							elsif (fmt = "01") then
								rx_state <= rx_dw4;
							else
								rx_state <= rx_idle;
							end if;
						else
							rx_state <= rx_dw2;
						end if;
					else
						rx_state <= rx_idle;
					end if;
					
				when rx_dw3 =>
					wb_en <= '0';
					if (rx_data_err = '0') then
						if (rx_data_rdy = '1') then
							data_frame <= rx_data_out;
							rx_state <= rx_dw4;
						else 
							rx_state <= rx_dw3;
						end if;
					elsif (fmt = "01" and rx_data_err = '0') then
						rx_state <= rx_dw4;
					else 
						rx_state <= rx_idle;
					end if;
					
				when rx_dw4 =>
					wb_en <= '0';
					if (rx_data_err = '0') then
						if (rx_data_rdy = '1') then
							eof_frame <= rx_data_out;
							rx_state <= rx_setup;
						else 
							rx_state <= rx_dw4;
						end if;
					else 
						rx_state <= rx_idle;
					end if;
				
				when rx_setup =>
					if (eof_frame = eof) then
						wb_en <= '1';
						rx_state <= rx_wait;
					else
						wb_en <= '0';
						rx_state <= rx_idle;
					end if;
						
				when rx_wait =>
					wb_en <= '0';
					if (wb_uart_ack_i = '1') then
						rx_state <= rx_idle;
					else
						rx_state <= rx_wait;
					end if;
					
				when others =>
					wb_en <= '0';
					rx_state <= rx_idle;
					sof_frame <= (others => '0');
					data_frame <= (others => '0');
					adr_frame_high <= (others => '0');
					adr_frame_low <= (others => '0');
			end case;
		end if;
			
	end process rx_dec;

	uart_core_i: uart_core PORT MAP(
		reset => sys_rst,
		clk => sys_clk,
		rx_data_serial => uart_rx,
		rx_data_out => rx_data_out,
		rx_data_rdy => rx_data_rdy,
		rx_data_err => rx_data_err,
		tx_data_serial => uart_tx,
		tx_data_in => tx_data_in,
		tx_data_en => tx_data_en,
		tx_busy => tx_busy
	);

end Behavirioal;
