library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity i2c_tca6424a is
	generic (
		f_clk : integer := 100000000;
		f_scl : integer := 100000;
		tca_addr : std_logic := '0'
	);
	port (
		clk : in std_logic;
		reset : in std_logic;
		
		-- I2C interface
		scl : inout std_logic;
		sda : inout std_logic;
		
		-- ports
		port_input : out std_logic_vector(23 downto 0);
		port_output : in std_logic_vector(23 downto 0);
		port_dir : in std_logic_vector(23 downto 0)
	);
end i2c_tca6424a;

architecture RTL of i2c_tca6424a is
	component i2c_ctrl
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			i2c_rxdata : out std_logic_vector(7 downto 0);
			i2c_txdata : in std_logic_vector(7 downto 0);
			
			i2c_read : in std_logic;
			i2c_write : in std_logic;
			i2c_start : in std_logic;
			i2c_stop : in std_logic;
			
			i2c_ack_in : in std_logic;
			i2c_ack_out : out std_logic;
			
			busy : out std_logic;
			
			i2c_scl : inout std_logic;
			i2c_sda : inout std_logic
		);
	end component;
	
	-- TCA I2C addresses
	constant TCAaddr_read : std_logic_vector(7 downto 0) := "010001" & TCA_addr & '1';
	constant TCAaddr_write : std_logic_vector(7 downto 0) := "010001" & TCA_addr & '0';
	
	-- signals for i2c controller
	signal i2c_rxdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_txdata : std_logic_vector(7 downto 0) := (others => '0');
	signal i2c_read : std_logic := '0';
	signal i2c_write : std_logic := '0';
	signal i2c_start : std_logic := '0';
	signal i2c_stop : std_logic := '0';
	signal i2c_ack_in : std_logic := '0';
	signal i2c_ack_out : std_logic := '0';
	signal i2c_busy : std_logic := '0';
	signal i2c_error : std_logic := '0';
	
	-- state machine
	type fsm_states is (st_write_dir0, st_write_dir1, st_write_dir2, st_write_dir3, st_write_dir4,
							  st_write_output0, st_write_output1, st_write_output2, st_write_output3, st_write_output4,
							  st_read_input0, st_read_input1, st_read_input2, st_read_input3, st_read_input4, st_read_input5, st_read_input6,
							  st_busywait);
	signal Z : fsm_states := st_write_dir0;
	signal Z_return : fsm_states := st_write_dir1;
begin

i2c_ctrl_i: i2c_ctrl
	GENERIC MAP (
		f_clk => f_clk,
		f_scl => f_scl
	)
	PORT MAP (
		clk => clk,
		reset => reset,
		i2c_rxdata => i2c_rxdata,
		i2c_txdata => i2c_txdata,
		i2c_read => i2c_read,
		i2c_write => i2c_write,
		i2c_start => i2c_start,
		i2c_stop => i2c_stop,
		i2c_ack_in => i2c_ack_in, 
		i2c_ack_out => i2c_ack_out,
		busy => i2c_busy,
		i2c_scl => scl,
		i2c_sda => sda
	);
	
process begin
	wait until rising_edge(clk);
	
	-- default values
	i2c_start <= '0';
	i2c_stop <= '0';
	i2c_read <= '0';
	i2c_write <= '0';
	i2c_ack_in <= '0';
	
	if reset = '1' then
		Z <= st_write_dir0;
	else
		case Z is			
			when st_write_dir0 =>
				i2c_txdata <= TCAaddr_write;
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_dir1;
				end if;
				
			when st_write_dir1 =>
				i2c_txdata <= x"8C";
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_dir2;
				end if;
				
			when st_write_dir2 =>
				i2c_txdata <= port_dir(7 downto 0);
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_dir3;
				end if;
				
			when st_write_dir3 =>
				i2c_txdata <= port_dir(15 downto 8);
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_dir4;
				end if;

			when st_write_dir4	=>
				i2c_txdata <= port_dir(23 downto 16);
				i2c_write <= '1';
				i2c_stop <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_output0;
				end if;
				
			when st_write_output0 =>
				i2c_txdata <= TCAaddr_write;
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_output1;
				end if;
				
			when st_write_output1 =>
				i2c_txdata <= x"84";
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_output2;
				end if;
				
			when st_write_output2 =>
				i2c_txdata <= port_output(7 downto 0);
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_output3;
				end if;
				
			when st_write_output3 =>
				i2c_txdata <= port_output(15 downto 8);
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_write_output4;
				end if;

			when st_write_output4 =>
				i2c_txdata <= port_output(23 downto 16);
				i2c_write <= '1';
				i2c_stop <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input0;
				end if;
				
			when st_read_input0 =>
				i2c_txdata <= TCAaddr_write;
				i2c_start <= '1';
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input1;
				end if;
				
			when st_read_input1 =>
				i2c_txdata <= x"80";
				i2c_write <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input2;
				end if;
				
			when st_read_input2 =>
				i2c_txdata <= TCAaddr_read;
				i2c_write <= '1';
				i2c_start <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input3;
				end if;
				
			when st_read_input3 =>
				i2c_read <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input4;
				end if;
				
			when st_read_input4 =>
				port_input(7 downto 0) <= i2c_rxdata;
				i2c_read <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input5;
				end if;
				
			when st_read_input5 =>
				port_input(15 downto 8) <= i2c_rxdata;
				i2c_read <= '1';
				i2c_stop <= '1';
				if i2c_busy = '1' then
					Z <= st_busywait;
					Z_return <= st_read_input6;
				end if;
				
			when st_read_input6 =>
				port_input(23 downto 16) <= i2c_rxdata;
				Z <= st_write_dir0;
				
			when st_busywait =>
				if i2c_busy = '0' then
					i2c_error <= i2c_ack_out;
					Z <= Z_return;
				end if;
		end case;
	end if;
end process;

end architecture;
