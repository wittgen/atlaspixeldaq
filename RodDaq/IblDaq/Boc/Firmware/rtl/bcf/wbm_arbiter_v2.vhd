----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				Wishbone Interconnect 4 way arbiter
-- Description:		Round-robin CLK synchronus arbiter, granting the Wishbone BUS to the Masters.
--							There is a pause of 1 CLK cycle per grant change.
----------------------------------------------------------------------------------
-- Changelog:
-- 18.02.2011 - Initial Version
-- 20.02.2011 - Changed IDLE state to go directly to mX state not mX_setup
----------------------------------------------------------------------------------
-- TODO:
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

----------------------------------------------------------------------
-- Entity declaration.
----------------------------------------------------------------------

entity wbm_arbiter_v2 is
    port(
            CLK:        in  std_logic;
            COMCYC:     out std_logic;
            CYC1:       in  std_logic;
            CYC0:       in  std_logic;
            GNT:		out std_logic;
            GNT1:       out std_logic;
            GNT0:       out std_logic;
            ACK:		in  std_logic;
            RST:        in  std_logic
         );
end entity wbm_arbiter_v2;


----------------------------------------------------------------------
-- Architecture definition.
----------------------------------------------------------------------

architecture behavioral of wbm_arbiter_v2 is
	type arbiter_states_t is (st_idle, st_master0, st_master1, st_release);
	signal Z : arbiter_states_t := st_idle;
	constant release_cnt_max : integer := 7;	-- wait at least 8 cycles to release the bus
	signal release_cnt : integer range 0 to release_cnt_max := 0;
begin

-- fsm
process begin
	wait until rising_edge(CLK);

	if RST = '1' then
		Z <= st_idle;

		COMCYC <= '0';
		GNT <= '0';
		GNT0 <= '0';
		GNT1 <= '0';

		release_cnt <= 0;
	else
		case Z is
			when st_idle =>
				COMCYC <= '0';
				GNT <= '0';
				GNT0 <= '0';
				GNT1 <= '0';

				if CYC0 = '1' then
					Z <= st_master0;
				elsif CYC1 = '1' then
					Z <= st_master1;
				end if;

			when st_master0 =>
				COMCYC <= CYC0;
				GNT <= '0';
				GNT0 <= '1';
				GNT1 <= '0';

				if (CYC0 = '0') and (ACK = '0') then
					Z <= st_release;
				end if;

			when st_master1 =>
				COMCYC <= CYC1;
				GNT <= '1';
				GNT0 <= '0';
				GNT1 <= '1';

				if (CYC1 = '0') and (ACK = '0') then
					Z <= st_release;
				end if;

			when st_release =>
				COMCYC <= '0';
				GNT <= '0';
				GNT0 <= '0';
				GNT1 <= '0';

				if release_cnt = release_cnt_max then
					release_cnt <= 0;

					if CYC0 = '1' then
						Z <= st_master0;
					elsif CYC1 = '1' then
						Z <= st_master1;
					else
						Z <= st_idle;
					end if;
				else
					release_cnt <= release_cnt + 1;
				end if;
		end case;
	end if;
end process;

end architecture behavioral;

