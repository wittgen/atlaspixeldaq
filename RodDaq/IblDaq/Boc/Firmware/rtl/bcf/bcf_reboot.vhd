library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

entity bcf_reboot is
  generic (
  	f_clk : integer := 100000000
  );
  port (
	clk : in std_logic;
	issue_reboot : in std_logic;
	recovery : in std_logic
  );
end entity ; -- bcf_reboot

architecture RTL of bcf_reboot is

constant timer_max : integer := f_clk-1;
signal timer_cnt : integer range 0 to timer_max := 0;

type icap_command_t is array (0 to 15) of std_logic_vector(15 downto 0);
constant icap_command_normal : icap_command_t := (
	x"FFFF",					-- dummy
	x"AA99", x"5566",			-- sync
	x"3261", x"0000",			-- multiboot start address [15:0]
	x"3281", x"0B40",			-- multiboot op + start addr [23:16]
	x"32A1", x"0044",			-- fallback addr [15:0]
	x"32C1", x"0B00",			-- fallback op + addr [23:16]
	x"30A1", x"000E",			-- IPROG command
	x"2000", x"2000", x"2000"	-- NOOP
);
constant icap_command_recovery : icap_command_t := (
	x"FFFF",					-- dummy
	x"AA99", x"5566",			-- sync
	x"3261", x"0044",			-- multiboot start address [15:0]
	x"3281", x"0B00",			-- multiboot op + start addr [23:16]
	x"32A1", x"0044",			-- fallback addr [15:0]
	x"32C1", x"0B00",			-- fallback op + addr [23:16]
	x"30A1", x"000E",			-- IPROG command
	x"2000", x"2000", x"2000"	-- NOOP
);
signal icap_command_cnt : integer range 0 to 15 := 0;

signal icap_busy : std_logic := '0';
signal icap_din : std_logic_vector(15 downto 0) := (others => '0');
signal icap_dout : std_logic_vector(15 downto 0) := (others => '0');
signal icap_ce_n : std_logic := '1';
signal icap_write_n : std_logic := '0';
signal icap_clk : std_logic := '0';
constant icap_clk_cnt_max : integer := (f_clk / (2*10000000))-1;
signal icap_clk_cnt : integer range 0 to icap_clk_cnt_max;

signal do_reboot : std_logic := '0';

begin

-- start timer for reboot
process begin
	wait until rising_edge(clk);

	if (issue_reboot = '0') then
		timer_cnt <= 0;
		do_reboot <= '0';
	else
		if timer_cnt = timer_max then
			do_reboot <= '1';
		else
			timer_cnt <= timer_cnt + 1;
		end if;
	end if;
end process;

-- generate a slow clock for ICAP (max. 10 MHz)
process begin
	wait until rising_edge(clk);

	if icap_clk_cnt = icap_clk_cnt_max then
		icap_clk <= not icap_clk;
		icap_clk_cnt <= 0;
	else
		icap_clk_cnt <= icap_clk_cnt + 1;
	end if;
end process;
		
icap_i: ICAP_SPARTAN6
	generic map (
		DEVICE_ID => X"0402E093",     -- Specifies the pre-programmed Device ID value
		SIM_CFG_FILE_NAME => "NONE"   -- Specifies the Raw Bitstream (RBT) file to be parsed by the simulation
									  -- model
	)
	port map (
		BUSY => icap_busy,     -- 1-bit output: Busy/Ready output
		O => icap_dout,        -- 16-bit output: Configuartion data output bus
		CE => icap_ce_n,       -- 1-bit input: Active-Low ICAP Enable input
		CLK => icap_clk,       -- 1-bit input: Clock input
		I => icap_din,         -- 16-bit input: Configuration data input bus
		WRITE => icap_write_n  -- 1-bit input: Read/Write control input
	);

icap_write_n <= '0';

-- issue IPROG command if do_reboot
process begin
	wait until rising_edge(icap_clk);

	if do_reboot = '0' then
		icap_ce_n <= '1';
		icap_command_cnt <= 0;
	else
		if icap_command_cnt < 15 then
			if recovery = '0' then
				icap_din(0) <= icap_command_normal(icap_command_cnt)(7);
				icap_din(1) <= icap_command_normal(icap_command_cnt)(6);
				icap_din(2) <= icap_command_normal(icap_command_cnt)(5);
				icap_din(3) <= icap_command_normal(icap_command_cnt)(4);
				icap_din(4) <= icap_command_normal(icap_command_cnt)(3);
				icap_din(5) <= icap_command_normal(icap_command_cnt)(2);
				icap_din(6) <= icap_command_normal(icap_command_cnt)(1);
				icap_din(7) <= icap_command_normal(icap_command_cnt)(0);
				icap_din(8) <= icap_command_normal(icap_command_cnt)(15);
				icap_din(9) <= icap_command_normal(icap_command_cnt)(14);
				icap_din(10) <= icap_command_normal(icap_command_cnt)(13);
				icap_din(11) <= icap_command_normal(icap_command_cnt)(12);
				icap_din(12) <= icap_command_normal(icap_command_cnt)(11);
				icap_din(13) <= icap_command_normal(icap_command_cnt)(10);
				icap_din(14) <= icap_command_normal(icap_command_cnt)(9);
				icap_din(15) <= icap_command_normal(icap_command_cnt)(8);
			else
				icap_din(0) <= icap_command_recovery(icap_command_cnt)(7);
				icap_din(1) <= icap_command_recovery(icap_command_cnt)(6);
				icap_din(2) <= icap_command_recovery(icap_command_cnt)(5);
				icap_din(3) <= icap_command_recovery(icap_command_cnt)(4);
				icap_din(4) <= icap_command_recovery(icap_command_cnt)(3);
				icap_din(5) <= icap_command_recovery(icap_command_cnt)(2);
				icap_din(6) <= icap_command_recovery(icap_command_cnt)(1);
				icap_din(7) <= icap_command_recovery(icap_command_cnt)(0);
				icap_din(8) <= icap_command_recovery(icap_command_cnt)(15);
				icap_din(9) <= icap_command_recovery(icap_command_cnt)(14);
				icap_din(10) <= icap_command_recovery(icap_command_cnt)(13);
				icap_din(11) <= icap_command_recovery(icap_command_cnt)(12);
				icap_din(12) <= icap_command_recovery(icap_command_cnt)(11);
				icap_din(13) <= icap_command_recovery(icap_command_cnt)(10);
				icap_din(14) <= icap_command_recovery(icap_command_cnt)(9);
				icap_din(15) <= icap_command_recovery(icap_command_cnt)(8);
			end if;
			icap_command_cnt <= icap_command_cnt + 1;
			icap_ce_n <= '0';
		else
			icap_ce_n <= '1';
		end if;
	end if;
end process;

end architecture ; -- RTL