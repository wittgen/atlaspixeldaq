-- SetupBus to Wishbone connector
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity sb2wb is
	port
	(
		-- clocking and reset
		clk : in std_logic := '0';
		reset : in std_logic := '0';
		
		-- setup bus slave
		sb_strobe_n : in std_logic := '1';
		sb_write_n : in std_logic := '1';
		sb_busy : out std_logic := '0';
		sb_addr : in std_logic_vector(15 downto 0);
		sb_data_in : in std_logic_vector(7 downto 0);
		sb_data_out : out std_logic_vector(7 downto 0);
		sb_data_oe : out std_logic;
		
		-- wishbone master
		wb_cyc : out std_logic := '0';
		wb_stb : out std_logic := '0';
		wb_we : out std_logic := '0';
		wb_addr : out std_logic_vector(15 downto 0) := (others => '0');
		wb_dat_mosi : out std_logic_vector(7 downto 0) := (others => '0');
		wb_dat_miso : in std_logic_vector(7 downto 0) := (others => '0');
		wb_ack : in std_logic := '0'
	);
end sb2wb;

architecture Behavioral of sb2wb is
	-- sample rising edge of wb_ack
	signal wb_ack_reg : std_logic := '0';

	-- sample falling edge of strobe
	signal sb_strobe_sr : std_logic_vector(1 downto 0) := "11";

	-- signal setup bus hold
	signal sb_hold : std_logic := '0';

	-- delay busy
	signal sb_busy_sr : std_logic_vector(3 downto 0) := "0000";
begin


process begin
	wait until rising_edge(clk);

	-- sample strobe
	sb_strobe_sr <= sb_strobe_sr(0) & sb_strobe_n;

	-- find falling edge on strobe
	if sb_strobe_sr = "10" then
		sb_hold <= '1';
	elsif sb_strobe_sr = "01" then
		sb_hold <= '0';
	end if;

	-- sample data and address if sb_hold is not enabled
	if sb_hold = '0' then
		wb_addr <= sb_addr;
		wb_dat_mosi <= sb_data_in;
		wb_we <= not sb_write_n;
	end if;

	-- output enable
	sb_data_oe <= sb_write_n;

	-- generate cycle and strobe for wishbone
	wb_cyc <= sb_hold;
	wb_stb <= sb_hold;

	-- busy from ack (delayed)
	sb_busy_sr <= sb_busy_sr(2 downto 0) & wb_ack;
	sb_busy <= sb_busy_sr(3);

	-- put data on Setup Bus
	if (wb_ack = '1') and (wb_ack_reg = '0') then
		sb_data_out <= wb_dat_miso;
	end if;
end process;

end Behavioral;

