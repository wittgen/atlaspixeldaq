----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			
-- E-Mail:				
--
-- Project:				IBL BOC firmware
-- Module:				PLL SPI Master
-- Description:		SPI Master for PLL configuration
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 16.12.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
-- Comments ...
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity PLLSPI is
	generic (
		f_clk : natural := 80000000;		-- input clock frequency
		f_sck : natural := 40000000		-- spi clock frequency
	);
	port (
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		sck : out std_logic := '0';
		miso : in std_logic := '0';
		mosi : out std_logic := '0';
		cs_n : out std_logic := '1';
		
		txdat : in std_logic_vector(31 downto 0) := (others => '0');
		rxdat : out std_logic_vector(31 downto 0) := (others => '0');
		
		transceive : in std_logic := '0';
		busy : out std_logic := '0'
	);
end PLLSPI;

architecture Behavioral of PLLSPI is
	-- baud generator signals
	signal sck_en : std_logic := '0';
	signal sck_cnt : integer range 0 to ((f_clk / (2 * f_sck))-1) := 0;
	
	-- fsm
	type spi_states is (idle, load, tx1, tx2, writeback);
	signal Z : spi_states := idle;
	
	-- shift-registers
	signal rx_sr : std_logic_vector(31 downto 0) := (others => '0');
	signal tx_sr : std_logic_vector(31 downto 0) := (others => '0');
	
	-- bit counter
	signal bitcnt : integer range 0 to 31 := 0;
begin

-- sck baudrate generator (ce = '1' twice a spi clock cycle)
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		sck_en <= '0';
	else
		if sck_cnt = ((f_clk / (2 * f_sck))-1) then
			sck_en <= '1';
			sck_cnt <= 0;
		else
			sck_en <= '0';
			sck_cnt <= sck_cnt + 1;
		end if;
	end if;
end process;

-- spi fsm
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		Z <= idle;
		cs_n <= '1';
	else
		if sck_en = '1' then
			case Z is
				when idle =>
					if transceive = '1' then
						Z <= load;
					end if;
					sck <= '0';
				
				when load =>
					cs_n <= '0';
					bitcnt <= 31;
					sck <= '0';
					tx_sr <= txdat;
					Z <= tx1;
					
				when tx1 =>
					rx_sr <= miso & rx_sr(31 downto 1);		-- sample miso on rising edge of sck
					sck <= '1';
					Z <= tx2;
					
				when tx2 =>
					tx_sr <= '0' & tx_sr(31 downto 1);		-- shift on falling edge of sck
					sck <= '0';
					if bitcnt = 0 then
						Z <= writeback;
					else
						Z <= tx1;
						bitcnt <= bitcnt - 1;
					end if;
					
				when writeback =>
					rxdat <= rx_sr;
					cs_n <= '1';
					Z <= idle;					
			end case;
		end if;
	end if;
end process;
mosi <= tx_sr(0);
busy <= '0' when (Z = idle) else '1';

end Behavioral;
