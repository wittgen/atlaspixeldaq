-- CRC32 calculation for ProgUnit verification
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity CRC32Calc is
	port (
		-- clock and reset
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		-- data
		data_i : in std_logic_vector(7 downto 0) := (others => '0');
		data_valid_i : in std_logic := '0';
		
		-- crc output
		crc32_o : out std_logic_vector(31 downto 0) := (others => '0')
	);		
end CRC32Calc;

architecture Behavioral of CRC32Calc is

	-- lookup table of precalculated values
	type crc32_table_t is array (0 to 255) of std_logic_vector(31 downto 0);
	constant crc32_table : crc32_table_t := (
		x"00000000", x"77073096", x"ee0e612c", x"990951ba", x"076dc419",
		x"706af48f", x"e963a535", x"9e6495a3", x"0edb8832", x"79dcb8a4",
      		x"e0d5e91e", x"97d2d988", x"09b64c2b", x"7eb17cbd", x"e7b82d07",
      		x"90bf1d91", x"1db71064", x"6ab020f2", x"f3b97148", x"84be41de",
	      	x"1adad47d", x"6ddde4eb", x"f4d4b551", x"83d385c7", x"136c9856",
	      	x"646ba8c0", x"fd62f97a", x"8a65c9ec", x"14015c4f", x"63066cd9",
	      	x"fa0f3d63", x"8d080df5", x"3b6e20c8", x"4c69105e", x"d56041e4",
	      	x"a2677172", x"3c03e4d1", x"4b04d447", x"d20d85fd", x"a50ab56b",
	      	x"35b5a8fa", x"42b2986c", x"dbbbc9d6", x"acbcf940", x"32d86ce3",
	      	x"45df5c75", x"dcd60dcf", x"abd13d59", x"26d930ac", x"51de003a",
	      	x"c8d75180", x"bfd06116", x"21b4f4b5", x"56b3c423", x"cfba9599",
	      	x"b8bda50f", x"2802b89e", x"5f058808", x"c60cd9b2", x"b10be924",
	      	x"2f6f7c87", x"58684c11", x"c1611dab", x"b6662d3d", x"76dc4190",
	      	x"01db7106", x"98d220bc", x"efd5102a", x"71b18589", x"06b6b51f",
	      	x"9fbfe4a5", x"e8b8d433", x"7807c9a2", x"0f00f934", x"9609a88e",
	      	x"e10e9818", x"7f6a0dbb", x"086d3d2d", x"91646c97", x"e6635c01",
	      	x"6b6b51f4", x"1c6c6162", x"856530d8", x"f262004e", x"6c0695ed",
	      	x"1b01a57b", x"8208f4c1", x"f50fc457", x"65b0d9c6", x"12b7e950",
      		x"8bbeb8ea", x"fcb9887c", x"62dd1ddf", x"15da2d49", x"8cd37cf3",
      		x"fbd44c65", x"4db26158", x"3ab551ce", x"a3bc0074", x"d4bb30e2",
		x"4adfa541", x"3dd895d7", x"a4d1c46d", x"d3d6f4fb", x"4369e96a",
      		x"346ed9fc", x"ad678846", x"da60b8d0", x"44042d73", x"33031de5",
      		x"aa0a4c5f", x"dd0d7cc9", x"5005713c", x"270241aa", x"be0b1010",
      		x"c90c2086", x"5768b525", x"206f85b3", x"b966d409", x"ce61e49f",
		x"5edef90e", x"29d9c998", x"b0d09822", x"c7d7a8b4", x"59b33d17",
      		x"2eb40d81", x"b7bd5c3b", x"c0ba6cad", x"edb88320", x"9abfb3b6",
      		x"03b6e20c", x"74b1d29a", x"ead54739", x"9dd277af", x"04db2615",
		x"73dc1683", x"e3630b12", x"94643b84", x"0d6d6a3e", x"7a6a5aa8",
      		x"e40ecf0b", x"9309ff9d", x"0a00ae27", x"7d079eb1", x"f00f9344",
      		x"8708a3d2", x"1e01f268", x"6906c2fe", x"f762575d", x"806567cb",
      		x"196c3671", x"6e6b06e7", x"fed41b76", x"89d32be0", x"10da7a5a",
      		x"67dd4acc", x"f9b9df6f", x"8ebeeff9", x"17b7be43", x"60b08ed5",
      		x"d6d6a3e8", x"a1d1937e", x"38d8c2c4", x"4fdff252", x"d1bb67f1",
      		x"a6bc5767", x"3fb506dd", x"48b2364b", x"d80d2bda", x"af0a1b4c",
      		x"36034af6", x"41047a60", x"df60efc3", x"a867df55", x"316e8eef",
	      	x"4669be79", x"cb61b38c", x"bc66831a", x"256fd2a0", x"5268e236",
      		x"cc0c7795", x"bb0b4703", x"220216b9", x"5505262f", x"c5ba3bbe",
      		x"b2bd0b28", x"2bb45a92", x"5cb36a04", x"c2d7ffa7", x"b5d0cf31",
      		x"2cd99e8b", x"5bdeae1d", x"9b64c2b0", x"ec63f226", x"756aa39c",
      		x"026d930a", x"9c0906a9", x"eb0e363f", x"72076785", x"05005713",
      		x"95bf4a82", x"e2b87a14", x"7bb12bae", x"0cb61b38", x"92d28e9b",
      		x"e5d5be0d", x"7cdcefb7", x"0bdbdf21", x"86d3d2d4", x"f1d4e242",
      		x"68ddb3f8", x"1fda836e", x"81be16cd", x"f6b9265b", x"6fb077e1",
		x"18b74777", x"88085ae6", x"ff0f6a70", x"66063bca", x"11010b5c",
      		x"8f659eff", x"f862ae69", x"616bffd3", x"166ccf45", x"a00ae278",
      		x"d70dd2ee", x"4e048354", x"3903b3c2", x"a7672661", x"d06016f7",
      		x"4969474d", x"3e6e77db", x"aed16a4a", x"d9d65adc", x"40df0b66",
      		x"37d83bf0", x"a9bcae53", x"debb9ec5", x"47b2cf7f", x"30b5ffe9",
      		x"bdbdf21c", x"cabac28a", x"53b39330", x"24b4a3a6", x"bad03605",
      		x"cdd70693", x"54de5729", x"23d967bf", x"b3667a2e", x"c4614ab8",
      		x"5d681b02", x"2a6f2b94", x"b40bbe37", x"c30c8ea1", x"5a05df1b",
      		x"2d02ef8d"
	);
	
	-- table index
	signal crc32_table_idx : integer range 0 to 255 := 0;

	-- crc register
	signal crc32_value : std_logic_vector(31 downto 0) := (others => '1');
begin

-- calculate table index
crc32_table_idx <= to_integer(unsigned(crc32_value(7 downto 0) xor data_i));

-- calculate new crc32 value
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		crc32_value <= (others => '1');
	else
		if data_valid_i = '1' then
			crc32_value <= crc32_table(crc32_table_idx) xor ("00000000" & crc32_value(31 downto 8));
		end if;
	end if;
end process;

-- output crc32 value
crc32_o <= not crc32_value;

end Behavioral;
