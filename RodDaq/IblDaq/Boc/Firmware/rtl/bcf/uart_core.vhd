----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim
-- E-Mail:				heim@physik.uni-wuppertal.de
--
-- Project:				Fake ROD
-- Module:				UART core
-- Description:		UART communication over serial port.
--							Important: Change CLK_DIVISOR to match your system clock
--							and BAUD rate.
----------------------------------------------------------------------------------
-- Changelog:
-- 18.12.2010 - Initial Version
-- 16.05.2011 - Tested with a uart2wb, works fine!
-- 15.06.2011 - Isnt working that fine, dunno right now if it is the uart_core.
--					 Changing from variables to signals.
-- 04.06.2011 - Changed something, now works perfectly without bugs
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library work;
use work.bocpack.all;

entity uart_core is
	port(
		 -- Global signal
		 reset          : in  std_logic;                      -- reset control signal
		 clk            : in  std_logic;                      -- 40 MHz Clock
		 -- Reception channel
		 rx_data_serial : in  std_logic;                      -- Received Serial data from RS232
		 rx_data_out    : out std_logic_vector(7 downto 0);   -- Received Data
		 rx_data_rdy    : out std_logic;                      -- Received data enable control signal
		 rx_data_err    : out std_logic;                      -- Received data error detected
		 -- Transmition channel
		 tx_data_serial : out std_logic;                      -- Transmitted Serial data to RS232
		 tx_data_in     : in  std_logic_vector(7 downto 0);   -- Transmitted data 
		 tx_data_en     : in  std_logic;                      -- Transmitted data latch enable
		 tx_busy			 : out std_logic								-- Transmitter busy
		 );
end uart_core;

architecture Behavioral of uart_core is
	-- Internal signal
	signal rx_data_serial_i	: std_logic;
	signal rx_data_out_i		: std_logic_vector(7 downto 0);
	signal rx_data_rdy_i		: std_logic;
	signal rx_data_err_i		: std_logic;
	
	signal tx_data_serial_i	: std_logic;
	signal tx_data_in_i		: std_logic_vector(7 downto 0);
	signal tx_data_en_i		: std_logic;
	signal tx_busy_i			: std_logic;
	-- Constants
	constant CLK_DIVISOR		: integer := 109;	-- clock divisor to optain 921600 baud from 100 MHz
	constant CLK_DIVISOR_CLR : integer := 89;		-- Sync offset
	-- TX/RX States
	type tx_state_m is (IDLE,LOAD_TX_DATA,TX_DATA,TX_STOP);
	type rx_state_m is (IDLE,START_RX,EDGE_RX,SHIFT_RX,STOP_RX,RX_ERR);
	signal tx_state      : tx_state_m;
	signal rx_state      : rx_state_m;
	-- Signals
	signal tx_baud_clk	: std_logic;				-- TX clock
	signal rx_baud_clk	: std_logic;				-- RX clock
	signal tx_data_reg	: std_logic_vector(9 downto 0);	-- TX temp register
	signal rx_data_reg	: std_logic_vector(7 downto 0);	-- RX temp register
	signal clr_rx_baud	: std_logic;				-- Restart/Sync RX CLK
	
	signal tx_clk_divisor_cnt : integer range 0 to CLK_DIVISOR;  -- Clock divisier counter
	signal rx_clk_divisor_cnt : integer range 0 to CLK_DIVISOR;  -- Clock divisier counter
	signal rx_bit_cnt : integer range 0 to 10;
	signal tx_bit_cnt : integer range 0 to 10;
		
begin
	
	-- External port mapping
	rx_data_serial_i <= rx_data_serial;
	rx_data_out <= rx_data_out_i;
	rx_data_rdy <= rx_data_rdy_i;
	rx_data_err <= rx_data_err_i;
	
	--tx_data_serial <= tx_data_serial_i;
	tx_data_in_i <= tx_data_in;
	tx_data_en_i <= tx_data_en;
	tx_busy <= tx_busy_i;
	
	-- TX BAUD generator
	tx_baud_gen : process (reset, clk, tx_clk_divisor_cnt)
	begin
		if reset = '1' then
			tx_clk_divisor_cnt <= 0;
			tx_baud_clk <= '0';
		elsif (clk'event and clk = '1') then
			tx_baud_clk <= '0';
			tx_clk_divisor_cnt <= tx_clk_divisor_cnt + 1;
			if tx_clk_divisor_cnt = CLK_DIVISOR then
				tx_baud_clk <= '1';
				tx_clk_divisor_cnt <= 0;
			end if;
		end if;
	end process;
	
	-- RX BAUD generator
	rx_baud_gen : process (reset, clk, rx_clk_divisor_cnt)
	begin
		if reset = '1' then
			rx_clk_divisor_cnt <= 0;
			rx_baud_clk <= '0';
		elsif (clk'event and clk = '1') then
			rx_baud_clk    <= '0';
			rx_clk_divisor_cnt <= rx_clk_divisor_cnt + 1;
			if rx_clk_divisor_cnt = CLK_DIVISOR then
				rx_baud_clk    <= '1';
				rx_clk_divisor_cnt <= 0;
			elsif ( clr_rx_baud = '1') then -- Sync rx_baud_clk
				rx_clk_divisor_cnt <= CLK_DIVISOR_CLR;
				rx_baud_clk <= '0';
			end if;
		end if;
	end process;
	
	-- Transmission process	
	tx_proc : process (reset, clk, tx_data_in, tx_data_en_i, tx_baud_clk)
	begin
		if (reset = '1') then
			tx_busy_i <= '0';
			tx_data_reg <= (others => '1');
			tx_bit_cnt  <= 9;
		elsif (clk'event and clk = '1') then
			case tx_state is
				-- TX idle
				when IDLE =>
					if (tx_data_en_i) = '1' then -- Send Data on tx data enable
						tx_state <= LOAD_TX_DATA;
						tx_busy_i <= '0';
					else
						tx_busy_i <= '0';
					end if;
					tx_bit_cnt  <= 9;
				-- TX load data
				when LOAD_TX_DATA =>
					if ( tx_baud_clk = '1') then
						tx_busy_i <= '0';
						-- prepare data
						tx_data_reg(9 downto 0) <= '1' & tx_data_in(7 downto 0) & '0';
						tx_bit_cnt  <= 9;
						tx_state <= TX_DATA;
					end if;
				--TX send data
				when TX_DATA =>
					if (tx_baud_clk = '1') then
						-- shift out the data LSB to MSB
						tx_busy_i <= '0';
						tx_data_reg(9 downto 0) <= '1' & tx_data_reg(9 downto 1);
						tx_bit_cnt  <= tx_bit_cnt-1;
						if (tx_bit_cnt = 1) then
							tx_state <= TX_STOP;
						end if;
					end if;
				-- TX stop	
				when TX_STOP =>
					tx_bit_cnt  <= 9;
					if ( tx_baud_clk = '1') then
						tx_state <= IDLE;
						tx_busy_i <= '1';
					end if;
				when others =>
					tx_state <= IDLE;
			end case;
		end if;
	end process;
	
	tx_data_serial <= tx_data_reg(0);
		
	-- Reception process
	rx_proc : process(clk, reset, rx_data_serial, rx_data_serial_i, rx_bit_cnt)
	begin
		if (reset = '1') then
			rx_data_out_i <= ( others => '0');
			rx_data_reg <= ( others => '0');
			rx_data_rdy_i <= '0';
			rx_data_err_i <= '0';
			clr_rx_baud <= '0';
			rx_bit_cnt <= 0;
		elsif (clk'event and clk = '1') then
			case rx_state is
				when IDLE =>
				rx_data_rdy_i <= '0';
				rx_bit_cnt <= 0;
				rx_data_err_i <= '0';
				if ( rx_data_serial_i = '0' ) then
					clr_rx_baud <= '1';
					rx_state <= START_RX;
				end if;
					
				when START_RX =>
					rx_bit_cnt <= 0;
					clr_rx_baud <= '0';
					rx_data_rdy_i <= '0';
					if ( rx_baud_clk = '1' ) then
						-- Start bit
						if ( rx_data_serial_i = '1' ) then
							rx_state <= RX_ERR;
						else
							rx_state <= EDGE_RX;
						end if;
					end if;
				
				when EDGE_RX =>
					clr_rx_baud <= '0';
					if ( rx_baud_clk = '1' ) then
						if ( rx_bit_cnt = 8 ) then
							rx_state <= STOP_RX;
						else
							rx_state <= SHIFT_RX;
						end if;
					end if;
				
				when SHIFT_RX =>
					rx_bit_cnt <= rx_bit_cnt + 1;
					clr_rx_baud <= '0';
					if ( rx_bit_cnt = 8 ) then
						-- Stop bit
						if (rx_data_serial_i = '0') then
							rx_state <= RX_ERR;
						else
							rx_state <= EDGE_RX;
						end if;
					else
						rx_data_reg(7 downto 0) <= rx_data_serial_i & rx_data_reg(7 downto 1);
						rx_state <= EDGE_RX;
					end if;
					
				when STOP_RX =>
					rx_data_out_i <= rx_data_reg;
					clr_rx_baud <= '1';
					rx_data_err_i <= '0';
					rx_data_rdy_i <= '1';
					rx_state <= IDLE;
				
				when RX_ERR =>
					rx_data_err_i <= '1';
					if ( rx_data_serial = '1' ) then
						rx_state <= IDLE;
					end if;
					
				when others =>
					null;
					
			end case;
		end if;
	end process;
							
end Behavioral;
