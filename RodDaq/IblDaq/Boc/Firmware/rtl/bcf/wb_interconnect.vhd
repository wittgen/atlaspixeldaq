library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity wb_interconnect is
	port (
		clk : in std_logic;
		reset : in std_logic;

		-- master 0
		wbm0_address : in std_logic_vector(15 downto 0);
		wbm0_dat_i : out std_logic_vector(7 downto 0);
		wbm0_dat_o : in std_logic_vector(7 downto 0);
		wbm0_cyc : in std_logic;
		wbm0_stb : in std_logic;
		wbm0_we : in std_logic;
		wbm0_ack : out std_logic;

		-- master 1
		wbm1_address : in std_logic_vector(15 downto 0);
		wbm1_dat_i : out std_logic_vector(7 downto 0);
		wbm1_dat_o : in std_logic_vector(7 downto 0);
		wbm1_cyc : in std_logic;
		wbm1_stb : in std_logic;
		wbm1_we : in std_logic;
		wbm1_ack : out std_logic;

		-- slave 0
		wbs0_address : out std_logic_vector(15 downto 0);
		wbs0_dat_i : out std_logic_vector(7 downto 0);
		wbs0_dat_o : in std_logic_vector(7 downto 0);
		wbs0_cyc : out std_logic;
		wbs0_stb : out std_logic;
		wbs0_we : out std_logic;
		wbs0_ack : in std_logic;

		-- slave 1
		wbs1_address : out std_logic_vector(15 downto 0);
		wbs1_dat_i : out std_logic_vector(7 downto 0);
		wbs1_dat_o : in std_logic_vector(7 downto 0);
		wbs1_cyc : out std_logic;
		wbs1_stb : out std_logic;
		wbs1_we : out std_logic;
		wbs1_ack : in std_logic;

		-- slave 2
		wbs2_address : out std_logic_vector(15 downto 0);
		wbs2_dat_i : out std_logic_vector(7 downto 0);
		wbs2_dat_o : in std_logic_vector(7 downto 0);
		wbs2_cyc : out std_logic;
		wbs2_stb : out std_logic;
		wbs2_we : out std_logic;
		wbs2_ack : in std_logic;

		-- slave 3
		wbs3_address : out std_logic_vector(15 downto 0);
		wbs3_dat_i : out std_logic_vector(7 downto 0);
		wbs3_dat_o : in std_logic_vector(7 downto 0);
		wbs3_cyc : out std_logic;
		wbs3_stb : out std_logic;
		wbs3_we : out std_logic;
		wbs3_ack : in std_logic
	);
end wb_interconnect;

architecture rtl of wb_interconnect is
	-- arbiter for master
	type arbiter_states is (st_idle, st_master0, st_master1, st_release);
	signal Z_arbiter : arbiter_states := st_idle;
	signal arbiter_grant_m0 : std_logic := '0';			-- master 0 owns the bus
	signal arbiter_grant_m1 : std_logic := '0';			-- master 1 owns the bus

	-- owning master signals
	signal owner_address : std_logic_vector(15 downto 0) := (others => '0');
	signal owner_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal owner_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal owner_stb : std_logic := '0';
	signal owner_cyc : std_logic := '0';
	signal owner_we : std_logic := '0';
	signal owner_ack : std_logic := '0';

	-- slave timout
	constant SLAVE_TIMOUT_MAX : integer := 127;		-- slave timeout will be 128 clock cycles
	signal slave_timeout : integer range 0 to SLAVE_TIMOUT_MAX := SLAVE_TIMOUT_MAX;

	-- master timeout
	constant MASTER_TIMEOUT_MAX : integer := 127;
	signal master_timeout : integer range 0 to MASTER_TIMEOUT_MAX := MASTER_TIMEOUT_MAX;
begin

-- bus arbiter
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		Z_arbiter <= st_idle;
		arbiter_grant_m0 <= '0';
		arbiter_grant_m1 <= '0';
		slave_timeout <= SLAVE_TIMOUT_MAX;
		master_timeout <= MASTER_TIMEOUT_MAX;
	else
		case Z_arbiter is
			when st_idle =>
				if (wbm0_cyc = '1') then
					-- master0 will own the bus
					arbiter_grant_m0 <= '1';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_master0;
				elsif (wbm1_cyc = '1') then
					-- master1 will own the bus
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '1';
					Z_arbiter <= st_master1;
				else
					-- nobody wants the bus
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_idle;					
				end if;

			when st_master0 =>
				-- wait until master 0 releases the bus
				-- or timeout occurs
				if (wbm0_cyc = '0') or (master_timeout = 0) then
					master_timeout <= MASTER_TIMEOUT_MAX;
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_release;
				else
					master_timeout <= master_timeout - 1;
					arbiter_grant_m0 <= '1';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_master0;
				end if;

			when st_master1 =>
				-- wait until master 0 releases the bus
				-- or timeout occurs
				if (wbm1_cyc = '0') or (master_timeout = 0) then
					master_timeout <= MASTER_TIMEOUT_MAX;
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_release;
				else
					master_timeout <= master_timeout - 1;
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '1';
					Z_arbiter <= st_master1;
				end if;

			when st_release =>
				-- wait until all slaves have pulled down their acknowledge
				-- adding a timout if a wishbone slave is not responding properly
				if ((wbs0_ack = '0') and (wbs1_ack = '0') and (wbs2_ack = '0') and (wbs3_ack = '0')) or (slave_timeout = 0) then
					slave_timeout <= SLAVE_TIMOUT_MAX;
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_idle;
				else
					-- decrement slave timeout
					slave_timeout <= slave_timeout - 1;
					arbiter_grant_m0 <= '0';
					arbiter_grant_m1 <= '0';
					Z_arbiter <= st_release;
				end if;
		end case;
	end if;
end process;

-- map signals of bus owner
owner_address <= wbm0_address when arbiter_grant_m0 = '1' else
				 wbm1_address when arbiter_grant_m1 = '1' else
				 (others => '0');
owner_dat_o <= wbm0_dat_o when arbiter_grant_m0 = '1' else
			   wbm1_dat_o when arbiter_grant_m1 = '1' else
			   (others => '0');
wbm0_dat_i <= owner_dat_i when arbiter_grant_m0 = '1' else
			  (others => '0');
wbm1_dat_i <= owner_dat_i when arbiter_grant_m1 = '1' else
			  (others => '0');
owner_cyc <= wbm0_cyc when arbiter_grant_m0 = '1' else
			 wbm1_cyc when arbiter_grant_m1 = '1' else
			 '0';
owner_stb <= wbm0_stb when arbiter_grant_m0 = '1' else
			 wbm1_stb when arbiter_grant_m1 = '1' else
			 '0';
owner_we <= wbm0_we when arbiter_grant_m0 = '1' else
			wbm1_we when arbiter_grant_m1 = '1' else
			'0';
wbm0_ack <= owner_ack when arbiter_grant_m0 = '1' else
			'0';
wbm1_ack <= owner_ack when arbiter_grant_m1 = '1' else
			'0';

-- address decoder
process begin
	wait until rising_edge(clk);

	-- default assignments (all slaves inactive)
	wbs0_address <= (others => '0');
	wbs1_address <= (others => '0');
	wbs2_address <= (others => '0');
	wbs3_address <= (others => '0');
	wbs0_dat_i <= (others => '0');
	wbs1_dat_i <= (others => '0');
	wbs2_dat_i <= (others => '0');
	wbs3_dat_i <= (others => '0');
	wbs0_cyc <= '0';
	wbs1_cyc <= '0';
	wbs2_cyc <= '0';
	wbs3_cyc <= '0';
	wbs0_stb <= '0';
	wbs1_stb <= '0';
	wbs2_stb <= '0';
	wbs3_stb <= '0';
	wbs0_we <= '0';
	wbs1_we <= '0';
	wbs2_we <= '0';
	wbs3_we <= '0';
	owner_dat_i <= (others => '0');
	owner_ack <= '0';

	-- assign values for active slave
	case owner_address(15 downto 14) is
		when "00" =>
			wbs0_address <= owner_address;
			wbs0_dat_i <= owner_dat_o;
			wbs0_cyc <= owner_cyc;
			wbs0_stb <= owner_stb;
			wbs0_we <= owner_we;
			owner_dat_i <= wbs0_dat_o;
			owner_ack <= wbs0_ack;
		when "01" =>
			wbs1_address <= owner_address;
			wbs1_dat_i <= owner_dat_o;
			wbs1_cyc <= owner_cyc;
			wbs1_stb <= owner_stb;
			wbs1_we <= owner_we;
			owner_dat_i <= wbs1_dat_o;
			owner_ack <= wbs1_ack;
		when "10" =>
			wbs2_address <= owner_address;
			wbs2_dat_i <= owner_dat_o;
			wbs2_cyc <= owner_cyc;
			wbs2_stb <= owner_stb;
			wbs2_we <= owner_we;
			owner_dat_i <= wbs2_dat_o;
			owner_ack <= wbs2_ack;
		when "11" =>
			wbs3_address <= owner_address;
			wbs3_dat_i <= owner_dat_o;
			wbs3_cyc <= owner_cyc;
			wbs3_stb <= owner_stb;
			wbs3_we <= owner_we;
			owner_dat_i <= wbs3_dat_o;
			owner_ack <= wbs3_ack;			
		when others =>
			null;
	end case;
end process;

end architecture;