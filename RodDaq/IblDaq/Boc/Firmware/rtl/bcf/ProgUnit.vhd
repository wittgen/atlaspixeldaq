----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Marius Wensing
-- E-Mail:				wensing@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				ProgUnit
-- Description:		A programming unit for BMF programming from SPI flash via JTAG
----------------------------------------------------------------------------------
-- Changelog:
-- Version 0x1:
-- 30.11.2011 - Initial Version
----------------------------------------------------------------------------------
-- TODO:
-- Comments ...
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ProgUnit is
	port (
		-- clock and reset
		clk_i : in std_logic := '0';
		rst_i : in std_logic := '0';
		
		-- flash interface
		fl_sck : out std_logic := '0';
		fl_si : out std_logic := '0';
		fl_so : in std_logic := '0';
		fl_cs_n : out std_logic := '1';
		fl_wp_n : out std_logic := '1';
		fl_hold_n : out std_logic := '1';
		
		-- jtag master
		jtag_done : in std_logic := '0';
		jtag_tck : out std_logic := '0';
		jtag_tms : out std_logic := '0';
		jtag_tdo : in std_logic := '0';
		jtag_tdi : out std_logic := '0';
		
		-- control/status interfaces
		control_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		control_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		control_reg_wren : in std_logic := '0';
		status_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		status_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		status_reg_wren : in std_logic := '0';
		pfifo_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		pfifo_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		pfifo_reg_wren : in std_logic := '0';
		fl_addr0_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		fl_addr0_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		fl_addr0_reg_wren : in std_logic := '0';
		fl_addr1_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		fl_addr1_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		fl_addr1_reg_wren : in std_logic := '0';
		fl_addr2_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		fl_addr2_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		fl_addr2_reg_wren : in std_logic := '0';
		dcnt_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		dcnt_reg_wr : in std_logic_vector(7 downto 0) := (others => '0');
		dcnt_reg_wren : in std_logic := '0';
		crc0_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc1_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc2_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc3_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');

		-- secure reg
		secure_reg : in std_logic
	);
end ProgUnit;

architecture Behavioral of ProgUnit is

	-- fifo to hold a single flash-page
	COMPONENT page_fifo
	  PORT (
		 clk : IN STD_LOGIC;
		 srst : IN STD_LOGIC;
		 din : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
		 wr_en : IN STD_LOGIC;
		 rd_en : IN STD_LOGIC;
		 dout : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
		 full : OUT STD_LOGIC;
		 empty : OUT STD_LOGIC;
		 data_count : OUT STD_LOGIC_VECTOR(7 DOWNTO 0)
	  );
	END COMPONENT;
	
	-- jtag-player
	component jtag_player is
		port ( 
			tms, tck, tdi : out std_logic;
			rdy, error, eof_done : out std_logic;
			rst, clk, load, tdo : in std_logic;
			prog_cntr : out std_logic_vector (6 downto 0); 
			data : in std_logic_vector (7 downto 0)
		);
	end component;

	-- spi master
	component SPIMaster is
		generic (
			f_clk : natural := 40000000;
			f_sck : natural := 20000000
		);
		port (
			clk_i : in std_logic := '0';
			rst_i : in std_logic := '0';
			
			sck : out std_logic := '0';
			miso : in std_logic := '0';
			mosi : out std_logic := '0';
			
			txdat : in std_logic_vector(7 downto 0) := (others => '0');
			rxdat : out std_logic_vector(7 downto 0) := (others => '0');
			
			transceive : in std_logic := '0';
			busy : out std_logic := '0'
		);
	end component;
	
	-- CRC verification
	COMPONENT CRC32Calc
	PORT(
		clk_i : IN std_logic;
		rst_i : IN std_logic;
		data_i : IN std_logic_vector(7 downto 0);
		data_valid_i : IN std_logic;          
		crc32_o : OUT std_logic_vector(31 downto 0)
		);
	END COMPONENT;
	
	signal player_tdo : std_logic := '0';
	signal player_tdi : std_logic := '0';
	signal player_tck : std_logic := '0';
	signal player_tms : std_logic := '0';
	signal player_rst : std_logic := '0';
	signal player_rdy : std_logic := '0';
	signal player_err : std_logic := '0';
	signal player_eof : std_logic := '0';
	signal player_load : std_logic := '0';
	signal player_progcnt : std_logic_vector(6 downto 0) := (others => '0');
	signal player_data : std_logic_vector(7 downto 0) := (others => '0');
	
	signal spi_txdat : std_logic_vector(7 downto 0) := (others => '0');
	signal spi_rxdat : std_logic_vector(7 downto 0) := (others => '0');
	signal spi_transceive : std_logic := '0';
	signal spi_busy : std_logic := '0';
	
	signal verify_rst : std_logic := '0';
	signal verify_data : std_logic_vector(7 downto 0) := (others => '0');
	signal verify_data_valid : std_logic := '0';
	signal verify_checksum : std_logic_vector(31 downto 0) := (others => '0');
	signal verify_cnt : unsigned(23 downto 0) := (others => '0');
	
	signal pfifo_dout : std_logic_vector(7 downto 0) := (others => '0');
	signal pfifo_dcnt : std_logic_vector(7 downto 0) := (others => '0');
	signal pfifo_rd_en : std_logic := '0';
	signal pfifo_full : std_logic := '0';
	signal pfifo_empty : std_logic := '0';
	
	type prog_states is (idle, prog_page1, prog_page2, prog_page3, prog_page4, prog_page5, prog_page6, prog_page7,
								prog_page8, prog_page9, prog_page10, prog_page11, prog_page12, prog_page13, prog_page14, prog_page14a,
								prog_page15, prog_page16, prog_page17, prog_page18, chiperase1, chiperase2, chiperase3,
								chiperase4, chiperase5, chiperase6, chiperase7, chiperase8, chiperase9, chiperase10, 
								busywait1, busywait2, busywait3, busywait4, busywait5, busywait6, jtagprog1, jtagprog2,
								jtagprog3, jtagprog4, jtagprog5, jtagprog6, jtagprog7, jtagprog8, jtagprog9, jtagprog10,
								jtagprog11, jtagprog12, jtagprog13, jtagprog14, jtagprog15, crc1, crc2, crc3, crc4, crc5, crc6,
								crc7, crc8, crc9, crc10, crc11, crc12, read1, read2, read3, read4, read5, read6, read7, read8,
								read9, read10, read11, read12);
	signal progZ : prog_states := idle;
	
	signal fl_addr : std_logic_vector(23 downto 0) := (others => '0');
	signal fl_addr_reg : std_logic_vector(23 downto 0) := (others => '0');
	signal fl_addr_write : std_logic := '0';

	signal read_reg : std_logic := '0';
	signal crc_reg : std_logic := '0';
	signal jtag_reg : std_logic := '0';
	signal pageprog_reg : std_logic := '0';
	signal chiperase_reg : std_logic := '0';
	signal soft_rst : std_logic := '0';
	signal spi_rst : std_logic := '1';
	signal pfifo_rst : std_logic := '1';
	
	signal busyflag : std_logic := '0';
	signal prog_busy : std_logic := '0';
	signal busywait : integer range 0 to 65535 := 0;
	
	signal done_sr : std_logic_vector(3 downto 0) := (others => '0');
	signal done : std_logic := '0';
begin

-- JTAG-Player
jtag_player_inst: jtag_player
	port map(clk => clk_i,
				rst => player_rst,
				rdy => player_rdy,
				error => player_err,
				eof_done => player_eof,
				load => player_load,
				prog_cntr => player_progcnt,
				data => player_data,
				tms => jtag_tms,
				tdo => jtag_tdo,
				tdi => jtag_tdi,
				tck => jtag_tck);

		
-- SPI Master
spi_master_inst: SPIMaster
	generic map(
		f_clk => 100000000,
		f_sck => 4000000
	)
	port map(
		clk_i => clk_i,
		rst_i => spi_rst,
		sck => fl_sck,
		mosi => fl_si,
		miso => fl_so,
		rxdat => spi_rxdat,
		txdat => spi_txdat,
		transceive => spi_transceive,
		busy => spi_busy);
				
spi_rst <= rst_i or soft_rst;				

-- Page-FIFO
page_fifo_inst: page_fifo port map(
	clk => clk_i,
	srst => pfifo_rst,
	din => pfifo_reg_wr,
	dout => pfifo_dout,
	data_count => pfifo_dcnt,
	empty => pfifo_empty,
	full => pfifo_full,
	wr_en => pfifo_reg_wren,
	rd_en => pfifo_rd_en
);
				
pfifo_rst <= rst_i or soft_rst;

-- CRC32 calculation
CRC32Calc_i: CRC32Calc PORT MAP(
	clk_i => clk_i,
	rst_i => verify_rst,
	data_i => verify_data,
	data_valid_i => verify_data_valid,
	crc32_o => verify_checksum
);

-- register processing				
process begin
	wait until rising_edge(clk_i);
	
	if rst_i = '1' then
		crc_reg <= '0';
		jtag_reg <= '0';
		chiperase_reg <= '0';
		pageprog_reg <= '0';
		soft_rst <= '0';
		fl_addr_write <= '0';
		fl_addr <= (others => '0');
	else
		fl_addr_write <= '0';
	
		-- write control register
		if control_reg_wren = '1' then
			if (control_reg_wr(0) = '1') and (secure_reg = '1') then
				chiperase_reg <= '1';
			end if;
			if (control_reg_wr(1) = '1') and (secure_reg = '1') then
				pageprog_reg <= '1';
			end if;
			if (control_reg_wr(2) = '1') and (secure_reg = '1') then
				jtag_reg <= '1';
			end if;
			if control_reg_wr(4) = '1' then
				crc_reg <= '1';
			end if;

			-- disabled the READ functionality
			-- if control_reg_wr(5) = '1' then
			--	read_reg <= '1';
			--end if;
			soft_rst <= control_reg_wr(3);
		end if;
		
		-- flashaddr register
		if fl_addr0_reg_wren = '1' then
			fl_addr_write <= '1';
			fl_addr(7 downto 0) <= fl_addr0_reg_wr;
		end if;
		if fl_addr1_reg_wren = '1' then
			fl_addr_write <= '1';
			fl_addr(15 downto 8) <= fl_addr1_reg_wr;
		end if;
		if fl_addr2_reg_wren = '1' then
			fl_addr_write <= '1';
			fl_addr(23 downto 16) <= fl_addr2_reg_wr;
		end if;
		
		-- reset control signals if necessary
		if progZ = prog_page1 then
			pageprog_reg <= '0';
		end if;
		if progZ = chiperase1 then
			chiperase_reg <= '0';
		end if;
		if progZ = jtagprog1 then
			jtag_reg <= '0';
		end if;
		if progZ = crc1 then
			crc_reg <= '0';
		end if;
		if progZ = read1 then
			read_reg <= '0';
		end if;
	end if;
end process;

-- register-outputs
control_reg_rd <= "0000" & soft_rst & jtag_reg & pageprog_reg & chiperase_reg;
status_reg_rd <= "00" & done & player_eof & player_err & busyflag & pfifo_full & pfifo_empty;
fl_addr0_reg_rd <= fl_addr_reg(7 downto 0);
fl_addr1_reg_rd <= fl_addr_reg(15 downto 8);
fl_addr2_reg_rd <= fl_addr_reg(23 downto 16);
dcnt_reg_rd <= pfifo_dcnt;
crc3_reg_rd <= verify_checksum(31 downto 24);
crc2_reg_rd <= verify_checksum(23 downto 16);
crc1_reg_rd <= verify_checksum(15 downto 8);
crc0_reg_rd <= verify_checksum(7 downto 0);


-- programming fsm
process begin
	wait until rising_edge(clk_i);
	
	if (rst_i = '1') or (soft_rst = '1') then
		fl_addr_reg <= (others => '0');
		player_rst <= '0';
		verify_data_valid <= '0';
		progZ <= idle;
	else
		player_rst <= '1';
		verify_data_valid <= '0';
		case progZ is
			when idle =>
				if chiperase_reg = '1' then
					progZ <= chiperase1;
				elsif pageprog_reg = '1' then
					progZ <= prog_page1;
				elsif jtag_reg = '1' then
					progZ <= jtagprog1;
				elsif crc_reg = '1' then
					progZ <= crc1;
				elsif read_reg = '1' then
					progZ <= read1;
				end if;
			
			when prog_page1 =>
				-- select flash
				fl_cs_n <= '0';
				progZ <= prog_page2;
			
			when prog_page2 =>
				-- write enable command
				spi_txdat <= x"06";
				spi_transceive <= '1';
				progZ <= prog_page3;
				
			when prog_page3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= prog_page4;
				end if;
			
			when prog_page4 =>
				-- unselect flash
				fl_cs_n <= '1';
				progZ <= prog_page5;
				
			when prog_page5 =>
				progZ <= prog_page6;
				
			when prog_page6 =>
				-- select flash
				fl_cs_n <= '0';
				progZ <= prog_page7;
				
			when prog_page7 =>
				-- page program command
				spi_txdat <= x"02";
				spi_transceive <= '1';
				progZ <= prog_page8;
				
			when prog_page8 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= prog_page9;
				end if;
				
			when prog_page9 =>
				-- fl_addr(23:16)
				spi_txdat <= fl_addr_reg(23 downto 16);
				spi_transceive <= '1';
				progZ <= prog_page10;
				
			when prog_page10 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= prog_page11;
				end if;
				
			when prog_page11 =>
				-- fl_addr(15:8)
				spi_txdat <= fl_addr_reg(15 downto 8);
				spi_transceive <= '1';
				progZ <= prog_page12;
				
			when prog_page12 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= prog_page13;
				end if;
				
			when prog_page13 =>
				-- fl_addr(7:0)
				spi_txdat <= fl_addr_reg(7 downto 0);
				spi_transceive <= '1';
				progZ <= prog_page14;
				
			when prog_page14 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= prog_page14a;
					pfifo_rd_en <= '1';
				end if;
			
			when prog_page14a =>
				pfifo_rd_en <= '0';
				progZ <= prog_page15;
				
			when prog_page15 =>
				-- data from pfifo
				spi_txdat <= pfifo_dout;
				spi_transceive <= '1';
				pfifo_rd_en <= '0';
				progZ <= prog_page16;
				
			when prog_page16 =>
				spi_transceive <= '0';
				-- pfifo_rd_en <= '0';
				if spi_busy = '0' then
					if pfifo_empty = '1' then
						progZ <= prog_page17;
					else
						progZ <= prog_page14a;
						pfifo_rd_en <= '1';
					end if;
				end if;
				
			when prog_page17 =>
				fl_cs_n <= '1';
				progZ <= prog_page18;
				
			when prog_page18 =>
				fl_addr_reg <= std_logic_vector(unsigned(fl_addr_reg) + 256);
				busywait <= 0;
				progZ <= busywait1;
				
			when busywait1 =>
				--progZ <= busywait2;
				if busywait = 65535 then
					busywait <= 0;
					fl_cs_n <= '0';
					progZ <= busywait2;
				else
					busywait <= busywait + 1;
				end if;
				
			when busywait2 =>
				-- read status register command
				spi_txdat <= x"05";
				spi_transceive <= '1';
				progZ <= busywait3;
				
			when busywait3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= busywait4;
				end if;		
				
			when busywait4 =>
				-- dummy byte
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= busywait5;
				
			when busywait5 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					if spi_rxdat(0) = '1' then
						progZ <= busywait4;
					else
						progZ <= busywait6;
					end if;
				end if;
			
			when busywait6 =>
				fl_cs_n <= '1';
				progZ <= idle;
				
			when chiperase1 =>
				-- select flash
				fl_cs_n <= '0';
				progZ <= chiperase2;
			
			when chiperase2 =>
				-- write enable command
				spi_txdat <= x"06";
				spi_transceive <= '1';
				progZ <= chiperase3;
				
			when chiperase3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= chiperase4;
				end if;
			
			when chiperase4 =>
				-- unselect flash
				fl_cs_n <= '1';
				progZ <= chiperase5;
				
			when chiperase5 =>
				progZ <= chiperase6;
				
			when chiperase6 =>
				-- select flash
				fl_cs_n <= '0';
				progZ <= chiperase7;
				
			when chiperase7 =>
				-- bulk erase command
				spi_txdat <= x"C7";
				spi_transceive <= '1';
				progZ <= chiperase8;
				
			when chiperase8 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= chiperase9;
				end if;
			
			when chiperase9 =>
				fl_cs_n <= '1';
				progZ <= chiperase10;
				
			when chiperase10 =>
				busywait <= 0;
				progZ <= busywait1;
				
			when jtagprog1 =>
				player_rst <= '0';
			
				-- select flash memory
				fl_cs_n <= '0';
				progZ <= jtagprog2;
				
			when jtagprog2 =>
				player_rst <= '1';
			
				-- read command
				spi_txdat <= x"03";
				spi_transceive <= '1';
				progZ <= jtagprog3;
				
			when jtagprog3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= jtagprog4;
				end if;
				
			when jtagprog4 =>
				-- addr(23:16)
				spi_txdat <= fl_addr_reg(23 downto 16);
				spi_transceive <= '1';
				progZ <= jtagprog5;
				
			when jtagprog5 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= jtagprog6;
				end if;
			
			when jtagprog6 =>
				-- addr(15:8)
				spi_txdat <= fl_addr_reg(15 downto 8);
				spi_transceive <= '1';
				progZ <= jtagprog7;
				
			when jtagprog7 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= jtagprog8;
				end if;
				
			when jtagprog8 =>
				-- addr(7:0)
				spi_txdat <= fl_addr_reg(7 downto 0);
				spi_transceive <= '1';
				progZ <= jtagprog9;
				
			when jtagprog9 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= jtagprog10;
				end if;
				
			when jtagprog10 =>
				-- read command
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= jtagprog11;
				
			when jtagprog11 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= jtagprog12;
				end if;
				
			when jtagprog12 =>
				if player_rdy = '1' then
					-- fl_addr <= std_logic_vector(unsigned(fl_addr) + 1);
					player_data <= spi_rxdat;
					player_load <= '1';
					progZ <= jtagprog13;
				elsif (player_eof = '1') or (player_err = '1') then
					progZ <= jtagprog14;
				end if;
				
			when jtagprog13 =>
				player_load <= '0';
				if player_rdy = '0' then
					progZ <= jtagprog10;
				end if;
				
			when jtagprog14 =>
				-- unselect chip
				fl_cs_n <= '1';
				progZ <= jtagprog15;
				
			when jtagprog15 =>
				-- waitstate
				progZ <= idle;
				
			when crc1 =>
				-- select flash
				fl_cs_n <= '0';
				verify_cnt <= (others => '0');
				verify_rst <= '1';
				progZ <= crc2;
				
			when crc2 =>
				-- read command
				verify_rst <= '0';
				spi_txdat <= x"03";
				spi_transceive <= '1';
				progZ <= crc3;
				
			when crc3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= crc4;
				end if;
				
			when crc4 =>
				-- addr(23:16)
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= crc5;
				
			when crc5 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= crc6;
				end if;
			
			when crc6 =>
				-- addr(15:8)
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= crc7;
				
			when crc7 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= crc8;
				end if;
				
			when crc8 =>
				-- addr(7:0)
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= crc9;
				
			when crc9 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= crc10;
				end if;
				
			when crc10 =>
				-- read command
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= crc11;
				
			when crc11 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= crc12;
				end if;

			when crc12 =>
				verify_data <= spi_rxdat;
				verify_data_valid <= '1';
				
				if (verify_cnt = unsigned(fl_addr_reg)) or (verify_cnt = (verify_cnt'range => '1')) then
					fl_cs_n <= '1';
					progZ <= idle;
				else
					verify_cnt <= verify_cnt + 1;
					progZ <= crc10;
				end if;

			when read1 =>
				-- select flash memory
				fl_cs_n <= '0';
				progZ <= read2;
				
			when read2 =>
				-- read command
				spi_txdat <= x"03";
				spi_transceive <= '1';
				progZ <= read3;
				
			when read3 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= read4;
				end if;
				
			when read4 =>
				-- addr(23:16)
				spi_txdat <= fl_addr_reg(23 downto 16);
				spi_transceive <= '1';
				progZ <= read5;
				
			when read5 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= read6;
				end if;
			
			when read6 =>
				-- addr(15:8)
				spi_txdat <= fl_addr_reg(15 downto 8);
				spi_transceive <= '1';
				progZ <= read7;
				
			when read7 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= read8;
				end if;
				
			when read8 =>
				-- addr(7:0)
				spi_txdat <= fl_addr_reg(7 downto 0);
				spi_transceive <= '1';
				progZ <= read9;
				
			when read9 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= read10;
				end if;
				
			when read10 =>
				-- read command
				spi_txdat <= x"00";
				spi_transceive <= '1';
				progZ <= read11;
				
			when read11 =>
				-- wait until spi transmission complete
				spi_transceive <= '0';
				if spi_busy = '0' then
					progZ <= read12;
				end if;
				
			when read12 =>
				pfifo_reg_rd <= spi_rxdat;
				fl_cs_n <= '1';
				progZ <= idle;
		end case;
		
		if fl_addr_write = '1' then
			fl_addr_reg <= fl_addr;
		end if;
	end if;
end process;
prog_busy <= '0' when progZ = idle else '1';

-- status outputs
busyflag <= prog_busy or pageprog_reg or chiperase_reg or jtag_reg or crc_reg or read_reg;

-- synchronize done pin
process begin
	wait until rising_edge(clk_i);
	
	done_sr <= done_sr(2 downto 0) & jtag_done;
end process;
done <= done_sr(3);

end Behavioral;

