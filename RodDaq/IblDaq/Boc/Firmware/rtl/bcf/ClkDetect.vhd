library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity ClkDetect is
  generic (
  	f_clk : integer := 100000000;
  	f_min : integer := 39500000;
  	f_max : integer := 40500000;
  	speedup : integer := 1;
   	glitchdet_enable : boolean := false;
  	glitchdet_cntmax : integer := 127
  );
  port (
  	-- reference clock and reset
	clk : in std_logic;
	reset : in std_logic;

	-- measure
	meas_clk : in std_logic;

	-- status
	clk_good : out std_logic;
	clk_freq : out std_logic_vector(27 downto 0)
  ) ;
end entity ; -- ClkDetect

architecture RTL of ClkDetect is

-- gate counter
signal gate_cntmax : integer := ((f_clk/speedup)-1);
signal gate_cnt : integer range 0 to gate_cntmax := 0;
signal gate_en : std_logic := '0';
signal gate_start : std_logic := '0';
signal gate_done : std_logic := '0';

-- measure counter
signal meas_cnt : integer range 0 to f_clk := 0;
signal meas_reset : std_logic := '0';

-- fsm
type fsm_states is (st_reset0, st_reset1, st_gatestart, st_measwait, st_checkmeas, st_wait);
signal Z : fsm_states := st_reset0;
signal Z_returnwait : fsm_states := st_reset0;
signal wait_cnt : integer range 0 to 255 := 255;

signal clk_good_int : std_logic := '0';
signal clk_freq_int : std_logic_vector(clk_freq'range) := (others => '0');

-- glitch detector
signal glitchdet_sr : std_logic_vector(2 downto 0) := "000";
signal glitchdet_cnt : integer range 0 to glitchdet_cntmax := glitchdet_cntmax;
signal glitchdet_good : std_logic := '0';

begin

-- glitch detector
glitchdet_enabled: if glitchdet_enable = true generate
	glitchdet_sr <= glitchdet_sr(1 downto 0) & meas_clk when rising_edge(clk);
	process begin
		wait until rising_edge(clk);

		if glitchdet_sr(2) = glitchdet_sr(1) then
			if glitchdet_cnt > 0 then
				glitchdet_cnt <= glitchdet_cnt - 1;
			end if;
		else
			glitchdet_cnt <= glitchdet_cntmax;
		end if;
	end process;
	glitchdet_good <= '0' when glitchdet_cnt = 0 else '1';
end generate;

glitchdet_disabled: if glitchdet_enable = false generate
	glitchdet_good <= '1';
end generate;

-- gate counter
process begin
	wait until rising_edge(clk);

	if reset = '1' then
		gate_en <= '0';
		gate_done <= '0';
		gate_cnt <= 0;
	else
		if gate_start = '1' then
			gate_en <= '1';
			gate_done <= '0';
			gate_cnt <= 0;
		else
			if gate_cnt = gate_cntmax then
				gate_done <= '1';
				gate_en <= '0';
			else
				gate_cnt <= gate_cnt + 1;
			end if;
		end if;
	end if;
end process;

-- measure
process (meas_clk, meas_reset, gate_en) begin
	if meas_reset = '1' then
		meas_cnt <= 0;
	else
		if rising_edge(meas_clk) then
			if gate_en = '1' then
				meas_cnt <= meas_cnt + 1;
			end if;
		end if;
	end if;
end process;

-- FSM
process begin
	wait until rising_edge(clk);

	-- default values
	gate_start <= '0';

	if reset = '1' then
		Z <= st_reset0;
		clk_good_int <= '0';
		clk_freq_int <= (others => '0');
		meas_reset <= '1';
	else
		case Z is
			when st_reset0 =>
				meas_reset <= '1';
				wait_cnt <= 255;
				Z_returnwait <= st_reset1;
				Z <= st_wait;

			when st_reset1 =>
				meas_reset <= '0';
				wait_cnt <= 255;
				Z_returnwait <= st_gatestart;
				Z <= st_wait;

			when st_gatestart =>
				gate_start <= '1';
				wait_cnt <= 32;
				Z_returnwait <= st_measwait;
				Z <= st_wait;

			when st_measwait =>
				if gate_done = '1' then
					wait_cnt <= 255;
					Z_returnwait <= st_checkmeas;
					Z <= st_wait;
				end if;

			when st_checkmeas =>
				if (meas_cnt < f_min/speedup) or (meas_cnt > f_max/speedup) then
					clk_good_int <= '0';
				else
					clk_good_int <= '1';
				end if;
				clk_freq_int <= std_logic_vector(to_unsigned(meas_cnt * speedup, clk_freq_int'length));

				wait_cnt <= 255;
				Z_returnwait <= st_reset0;
				Z <= st_wait;

			when st_wait =>
				if wait_cnt = 0 then
					Z <= Z_returnwait;
				else
					wait_cnt <= wait_cnt - 1;
				end if;
		end case;
	end if;
end process;

clk_good <= clk_good_int and glitchdet_good;
clk_freq <= clk_freq_int;

end architecture ; -- arch