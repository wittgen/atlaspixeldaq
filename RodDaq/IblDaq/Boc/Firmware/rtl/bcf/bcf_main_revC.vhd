----------------------------------------------------------------------------------
-- Company: 			University of Wuppertal
-- Engineer: 			Timon Heim, Marius Wensing
-- E-Mail:				heim@physik.uni-wuppertal.de
--							wensing@physik.uni-wuppertal.de
--
-- Project:				IBL BOC firmware
-- Module:				IBL BOC Board Controller FPGA
-- Description:		Top Module of the BCF. All Component Instantiation will be done
--							here. Logical Parts are due to the Wishbone BUS Interconnect,
--							currently able to hold 4 Wishbone Masters and 4 Wishbone Slaves.
--							Important: If tested without Setup Bus connection, keep sb_stb_n
--							high or it will block the wb bus!
----------------------------------------------------------------------------------
-- Changelog:
-- 15.02.2011 - Initial Version
-- 20.02.2011 - First working version (atleast in simulation)
-- 20.02.2011 - Fixed Acknowledge MUX
-- 13.04.2011 - Added Master Connection for UART2WB
-- 16.05.2011 - System Bus seems to work fine, tested with UART2WB
-- Version 0x1:
-- 29.11.2011 - Preparing for use on BOC prototype
-- 30.11.2011 - Added North/Southbone
-- 30.11.2011 - Ported UART core into bcf_main module
-- 01.12.2011 - Added ProgUnit and some general purpose registers
----------------------------------------------------------------------------------
-- TODO:
-- 20.02.2011 - JTAG Chain
-- 30.11.2011 - Ethernet Mac and Controller
-- 30.11.2011 - Redo Address Map
-- 30.11.2011 - New Reg Bank
----------------------------------------------------------------------------------
-- Address Map: selected by the 2 most significant bits in wb_addr
-- 0x00:		BCF
-- 0x01:		BMF (north)
-- 0x02:		BMF (south)
-- 0x03:		unused
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VCOMPONENTS.ALL;

library work;
use work.bocpack.all;

entity bcf_main is
	port (
		-- clocking
		clk_p : in std_logic;
		clk_n : in std_logic;
		vme_clk : in std_logic;

		-- reset
		reset_in : in std_logic;
		reset_out : out std_logic;

		-- Setup Bus
		sb_data : inout std_logic_vector(7 downto 0);
		sb_addr : in std_logic_vector(15 downto 0);
		sb_stb_n : in std_logic;
		sb_wrt_n : in std_logic;
		sb_busy : out std_logic;
		
		-- Northbone
		wb_bmf_n_ack : in std_logic;
		wb_bmf_n_dat_i : in std_logic_vector(7 downto 0);
		wb_bmf_n_dat_o : out std_logic_vector(7 downto 0);
		wb_bmf_n_adr : out std_logic_vector(15 downto 0);
		wb_bmf_n_stb : out std_logic;
		wb_bmf_n_cyc : out std_logic;
		wb_bmf_n_we : out std_logic;
		
		-- JTAG north
		jtag_n_done : in std_logic;
		jtag_n_tck : out std_logic;
		jtag_n_tms : out std_logic;
		jtag_n_tdi : out std_logic;
		jtag_n_tdo : in std_logic;
		
		-- SPI flash north
		spi_n_sck : out std_logic;
		spi_n_so : in std_logic;
		spi_n_si : out std_logic;
		spi_n_cs_n : out std_logic;
		spi_n_wp_n : out std_logic;
		spi_n_hold_n : out std_logic;
		
		-- Southbone
		wb_bmf_s_ack : in std_logic;
		wb_bmf_s_dat_i : in std_logic_vector(7 downto 0);
		wb_bmf_s_dat_o : out std_logic_vector(7 downto 0);
		wb_bmf_s_adr : out std_logic_vector(15 downto 0);
		wb_bmf_s_stb : out std_logic;
		wb_bmf_s_cyc : out std_logic;
		wb_bmf_s_we : out std_logic;
		
		-- JTAG south
		jtag_s_done : in std_logic;
		jtag_s_tck : out std_logic;
		jtag_s_tms : out std_logic;
		jtag_s_tdi : out std_logic;
		jtag_s_tdo : in std_logic;
		
		-- SPI flash south
		spi_s_sck : out std_logic;
		spi_s_so : in std_logic;
		spi_s_si : out std_logic;
		spi_s_cs_n : out std_logic;
		spi_s_wp_n : out std_logic;
		spi_s_hold_n : out std_logic;
		
		-- UART
		uart_tx : out std_logic;
		uart_rx : in std_logic;
		
		-- I2C
		i2c_scl : inout std_logic;
		i2c_sda : inout std_logic;

		-- serial number
		sernum : inout std_logic;
		
		-- ethernet (GMII interface)
		gmii_gtx_clk : out std_logic;
		gmii_tx_clk : in std_logic;
		gmii_tx_en : out std_logic;
		gmii_tx_er : out std_logic;
		gmii_txd : out std_logic_vector(7 downto 0);
		gmii_rx_clk : in std_logic;
		gmii_rx_dv : in std_logic;
		gmii_rx_er: in std_logic;
		gmii_rxd : in std_logic_vector(7 downto 0);
		gmii_crs : in std_logic;
		gmii_col : in std_logic;
		gmii_phy_rstb : out std_logic;
		gmii_mdc : out std_logic;
		gmii_mdio : inout std_logic;
		
		-- pll
		pll_sck : out std_logic := '0';
		pll_mosi : out std_logic := '0';
		pll_miso : in std_logic := '0';
		pll_cs_n : out std_logic := '1';
		pll_locked : in std_logic := '0';
		pll_idelay : out std_logic_vector(10 downto 0) := (others => '0');
		
		-- ddr2 memory
		ddr2_dq : inout std_logic_vector(15 downto 0) := (others => 'Z');
		ddr2_a : out std_logic_vector(12 downto 0) := (others => '0');
		ddr2_ba : out std_logic_vector(1 downto 0) := "00";
		ddr2_ras_n : out std_logic := '1';
		ddr2_cas_n : out std_logic := '1';
		ddr2_we_n : out std_logic := '1';
		ddr2_cke : out std_logic := '0';
		ddr2_odt : out std_logic := '0';
		ddr2_ldm : out std_logic := '0';
		ddr2_udm : out std_logic := '0';
		ddr2_ldqs_p : inout std_logic := '0';
		ddr2_ldqs_n : inout std_logic := '0';
		ddr2_udqs_p : inout std_logic := '0';
		ddr2_udqs_n : inout std_logic := '0';
		ddr2_clk_p : out std_logic := '0';
		ddr2_clk_n : out std_logic := '0';
		ddr2_rzq : inout std_logic := '0';

		-- laser enable
		laser_enable : out std_logic := '0';
		boc_las_en : in std_logic;
		det_las_en : in std_logic;

		-- gpio led
		gpio_led : inout std_logic_vector(2 downto 0);

		-- MGT frequency selection
		mgt_fs : out std_logic_vector(2 downto 0);

		-- I2C port expander
		i2c_gpio_scl : inout std_logic;
		i2c_gpio_sda : inout std_logic;

		-- BCF SPI flash
		bcf_flash_sck : inout std_logic;
		bcf_flash_miso : inout std_logic;
		bcf_flash_mosi : inout std_logic;
		bcf_flash_cs_n : inout std_logic;

		-- BOC OK status
		boc_ok : out std_logic;

		-- JTAG switch
		jtag_switch : out std_logic_vector(1 downto 0);
		jtag_xvc_tck : out std_logic;
		jtag_xvc_tms : out std_logic;
		jtag_xvc_tdi : out std_logic;
		jtag_xvc_tdo : in std_logic
	);
end bcf_main;

architecture Behavioral of bcf_main is

	---------------------------------
	-- Component Declaration Begin --
	---------------------------------
	component wb_interconnect
		port (
			clk : in std_logic;
			reset : in std_logic;

			-- master 0
			wbm0_address : in std_logic_vector(15 downto 0);
			wbm0_dat_i : out std_logic_vector(7 downto 0);
			wbm0_dat_o : in std_logic_vector(7 downto 0);
			wbm0_cyc : in std_logic;
			wbm0_stb : in std_logic;
			wbm0_we : in std_logic;
			wbm0_ack : out std_logic;

			-- master 1
			wbm1_address : in std_logic_vector(15 downto 0);
			wbm1_dat_i : out std_logic_vector(7 downto 0);
			wbm1_dat_o : in std_logic_vector(7 downto 0);
			wbm1_cyc : in std_logic;
			wbm1_stb : in std_logic;
			wbm1_we : in std_logic;
			wbm1_ack : out std_logic;

			-- slave 0
			wbs0_address : out std_logic_vector(15 downto 0);
			wbs0_dat_i : out std_logic_vector(7 downto 0);
			wbs0_dat_o : in std_logic_vector(7 downto 0);
			wbs0_cyc : out std_logic;
			wbs0_stb : out std_logic;
			wbs0_we : out std_logic;
			wbs0_ack : in std_logic;

			-- slave 1
			wbs1_address : out std_logic_vector(15 downto 0);
			wbs1_dat_i : out std_logic_vector(7 downto 0);
			wbs1_dat_o : in std_logic_vector(7 downto 0);
			wbs1_cyc : out std_logic;
			wbs1_stb : out std_logic;
			wbs1_we : out std_logic;
			wbs1_ack : in std_logic;

			-- slave 2
			wbs2_address : out std_logic_vector(15 downto 0);
			wbs2_dat_i : out std_logic_vector(7 downto 0);
			wbs2_dat_o : in std_logic_vector(7 downto 0);
			wbs2_cyc : out std_logic;
			wbs2_stb : out std_logic;
			wbs2_we : out std_logic;
			wbs2_ack : in std_logic;

			-- slave 3
			wbs3_address : out std_logic_vector(15 downto 0);
			wbs3_dat_i : out std_logic_vector(7 downto 0);
			wbs3_dat_o : in std_logic_vector(7 downto 0);
			wbs3_cyc : out std_logic;
			wbs3_stb : out std_logic;
			wbs3_we : out std_logic;
			wbs3_ack : in std_logic
		);
	end component;
	
	COMPONENT sb2wb
	port
	(
		-- clocking and reset
		clk : in std_logic := '0';
		reset : in std_logic := '0';
		
		-- setup bus slave
		sb_strobe_n : in std_logic := '1';
		sb_write_n : in std_logic := '1';
		sb_busy : out std_logic := '0';
		sb_addr : in std_logic_vector(15 downto 0);
		sb_data_in : in std_logic_vector(7 downto 0);
		sb_data_out : out std_logic_vector(7 downto 0);
		sb_data_oe : out std_logic;
		
		-- wishbone master
		wb_cyc : out std_logic := '0';
		wb_stb : out std_logic := '0';
		wb_we : out std_logic := '0';
		wb_addr : out std_logic_vector(15 downto 0) := (others => '0');
		wb_dat_mosi : out std_logic_vector(7 downto 0) := (others => '0');
		wb_dat_miso : in std_logic_vector(7 downto 0) := (others => '0');
		wb_ack : in std_logic := '0'
	);
	END COMPONENT;
	
	COMPONENT BOCRegBank
	GENERIC(
		nReg : natural
		);
	PORT(
		clk_i : IN std_logic;
		rst_i : IN std_logic;
		wb_dat_i : IN std_logic_vector(7 downto 0);
		wb_adr_i : IN std_logic_vector(15 downto 0);
		wb_cyc_i : IN std_logic;
		wb_stb_i : IN std_logic;
		wb_we_i : IN std_logic;
		reg_rd : IN RegBank_rd_t;          
		wb_dat_o : OUT std_logic_vector(7 downto 0);
		wb_ack_o : OUT std_logic;
		reg_wr : OUT RegBank_wr_t;
		reg_wren : OUT RegBank_wren_t;
		reg_rden : OUT RegBank_rden_t
		);
	END COMPONENT;
	
	COMPONENT ProgUnit
	PORT(
		clk_i : IN std_logic;
		rst_i : IN std_logic;
		fl_so : IN std_logic;
		jtag_done : IN std_logic;
		jtag_tdo : IN std_logic;
		control_reg_wr : IN std_logic_vector(7 downto 0);
		control_reg_wren : IN std_logic;
		status_reg_wr : IN std_logic_vector(7 downto 0);
		status_reg_wren : IN std_logic;
		pfifo_reg_wr : IN std_logic_vector(7 downto 0);
		pfifo_reg_wren : IN std_logic;
		fl_addr0_reg_wr : IN std_logic_vector(7 downto 0);
		fl_addr0_reg_wren : IN std_logic;
		fl_addr1_reg_wr : IN std_logic_vector(7 downto 0);
		fl_addr1_reg_wren : IN std_logic;
		fl_addr2_reg_wr : IN std_logic_vector(7 downto 0);
		fl_addr2_reg_wren : IN std_logic;
		dcnt_reg_wr : IN std_logic_vector(7 downto 0);
		dcnt_reg_wren : IN std_logic;          
		fl_sck : OUT std_logic;
		fl_si : OUT std_logic;
		fl_cs_n : OUT std_logic;
		fl_wp_n : OUT std_logic;
		fl_hold_n : OUT std_logic;
		jtag_tck : OUT std_logic;
		jtag_tms : OUT std_logic;
		jtag_tdi : OUT std_logic;
		control_reg_rd : OUT std_logic_vector(7 downto 0);
		status_reg_rd : OUT std_logic_vector(7 downto 0);
		pfifo_reg_rd : OUT std_logic_vector(7 downto 0);
		fl_addr0_reg_rd : OUT std_logic_vector(7 downto 0);
		fl_addr1_reg_rd : OUT std_logic_vector(7 downto 0);
		fl_addr2_reg_rd : OUT std_logic_vector(7 downto 0);
		dcnt_reg_rd : OUT std_logic_vector(7 downto 0);
		crc0_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc1_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc2_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		crc3_reg_rd : out std_logic_vector(7 downto 0) := (others => '0');
		secure_reg : in std_logic
		);
	END COMPONENT;

	-- serial number readout
	COMPONENT ds2401_sernum
		generic (
			f_clk : integer := 100000000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;

			dq : inout std_logic;

			sernum : out std_logic_vector(47 downto 0);
			sernum_ok : out std_logic;
			sernum_done : out std_logic;
			sernum_reset : in std_logic
		);
	END COMPONENT;
	
	-- extended pll configuration
	COMPONENT ePLLUnit
	PORT(
		clk_i : IN std_logic;
		rst_i : IN std_logic;
		ctrl_wr : IN std_logic_vector(7 downto 0);
		ctrl_wren : IN std_logic;
		data0_wr : IN std_logic_vector(7 downto 0);
		data0_wren : IN std_logic;
		data1_wr : IN std_logic_vector(7 downto 0);
		data1_wren : IN std_logic;
		data2_wr : IN std_logic_vector(7 downto 0);
		data2_wren : IN std_logic;
		data3_wr : IN std_logic_vector(7 downto 0);
		data3_wren : IN std_logic;
		idelay0_wr : IN std_logic_vector(7 downto 0);
		idelay0_wren : IN std_logic;
		idelay1_wr : IN std_logic_vector(7 downto 0);
		idelay1_wren : IN std_logic;
		pll_miso : IN std_logic;
		pll_locked : IN std_logic;          
		ctrl_rd : OUT std_logic_vector(7 downto 0);
		status_rd : OUT std_logic_vector(7 downto 0);
		data0_rd : OUT std_logic_vector(7 downto 0);
		data1_rd : OUT std_logic_vector(7 downto 0);
		data2_rd : OUT std_logic_vector(7 downto 0);
		data3_rd : OUT std_logic_vector(7 downto 0);
		idelay0_rd : OUT std_logic_vector(7 downto 0);
		idelay1_rd : OUT std_logic_vector(7 downto 0);
		pll_sck : OUT std_logic;
		pll_mosi : OUT std_logic;
		pll_cs_n : OUT std_logic;
		pll_idelay : OUT std_logic_vector(10 downto 0)
		);
	END COMPONENT;

	component clocking_revBC
	  port (
		clk : in std_logic;
		reset : in std_logic;

		-- registers
		freq0_rd : out std_logic_vector(7 downto 0);
		freq0_wr : in std_logic_vector(7 downto 0);
		freq0_rden : in std_logic;
		freq0_wren : in std_logic;
		freq1_rd : out std_logic_vector(7 downto 0);
		freq1_wr : in std_logic_vector(7 downto 0);
		freq1_rden : in std_logic;
		freq1_wren : in std_logic;
		freq2_rd : out std_logic_vector(7 downto 0);
		freq2_wr : in std_logic_vector(7 downto 0);
		freq2_rden : in std_logic;
		freq2_wren : in std_logic;
		freq3_rd : out std_logic_vector(7 downto 0);
		freq3_wr : in std_logic_vector(7 downto 0);
		freq3_rden : in std_logic;
		freq3_wren : in std_logic;

		-- clock control
		vme_clk : in std_logic
	  ) ;
	end component;
	
	COMPONENT SimpleReg
	GENERIC(
		rst_value : std_logic_vector(7 downto 0) := x"00";
		readonly : string := "false"
		);
	PORT(
		clk_i : IN std_logic;
		rst_i : IN std_logic;
		wr : IN std_logic_vector(7 downto 0);
		wren : IN std_logic;          
		rd : OUT std_logic_vector(7 downto 0);
		rden : IN std_logic;
		regval : OUT std_logic_vector(7 downto 0)
		);
	END COMPONENT;

	COMPONENT BuildDate
		port (
			year : out std_logic_vector(7 downto 0) := x"00";
			month : out std_logic_vector(7 downto 0) := x"00";
			day : out std_logic_vector(7 downto 0) := x"00";
			hour : out std_logic_vector(7 downto 0) := x"00";
			minute : out std_logic_vector(7 downto 0) := x"00";
			second : out std_logic_vector(7 downto 0) := x"00";
			git_hash : out std_logic_vector(159 downto 0) := (others => '0')
		);
	END COMPONENT;

	component bcf_gpmem
		port (
			clk : in std_logic;

			wb_addr : in std_logic_vector(15 downto 0);
			wb_dat_i : in std_logic_vector(7 downto 0);
			wb_dat_o : out std_logic_vector(7 downto 0);
			wb_cyc : in std_logic;
			wb_stb : in std_logic;
			wb_we : in std_logic;
			wb_ack : out std_logic
		);
	end component;

	COMPONENT bcf_mb
	PORT(
		RS232_rxd : IN std_logic;
		RESET : IN std_logic;
--		CLK_P : IN std_logic;
--		CLK_N : IN std_logic;
		CLK : IN std_logic;
		axi_epc_0_PRH_Rdy_pin : IN std_logic;
		axi_epc_0_PRH_Data_I_pin : IN std_logic_vector(0 to 31);
		MCB_DDR2_rzq : INOUT std_logic;
		MCB_DDR2_dram_udqs_n : INOUT std_logic;
		MCB_DDR2_dram_udqs : INOUT std_logic;
		MCB_DDR2_dram_dqs_n : INOUT std_logic;
		MCB_DDR2_dram_dqs : INOUT std_logic;
		MCB_DDR2_dram_dq : INOUT std_logic_vector(15 downto 0);
		--Generic_IIC_Bus_Sda : INOUT std_logic;
		--Generic_IIC_Bus_Scl : INOUT std_logic;
		axi_gpio_0_GPIO_IO_pin : INOUT std_logic_vector(2 downto 0);      
		RS232_txd : OUT std_logic;
		MCB_DDR2_dram_we_n : OUT std_logic;
		MCB_DDR2_dram_udm : OUT std_logic;
		MCB_DDR2_dram_ras_n : OUT std_logic;
		MCB_DDR2_dram_odt : OUT std_logic;
		MCB_DDR2_dram_ldm : OUT std_logic;
		MCB_DDR2_dram_clk_n : OUT std_logic;
		MCB_DDR2_dram_clk : OUT std_logic;
		MCB_DDR2_dram_cke : OUT std_logic;
		MCB_DDR2_dram_cas_n : OUT std_logic;
		MCB_DDR2_dram_ba : OUT std_logic_vector(1 downto 0);
		MCB_DDR2_dram_addr : OUT std_logic_vector(12 downto 0);
		axi_epc_0_PRH_CS_n_pin : OUT std_logic;
		axi_epc_0_PRH_Addr_pin : OUT std_logic_vector(0 to 17);
		axi_epc_0_PRH_RNW_pin : OUT std_logic;
		clock_generator_0_CLKOUT2_pin : OUT std_logic;
		axi_epc_0_PRH_Data_O_pin : OUT std_logic_vector(0 to 31);
		RESET_OUT : OUT std_logic;
		axi_spi_0_SPISEL_pin : IN std_logic;
		axi_spi_0_SCK_pin : INOUT std_logic;
		axi_spi_0_MISO_pin : INOUT std_logic;
		axi_spi_0_MOSI_pin : INOUT std_logic;
		axi_spi_0_SS_pin : INOUT std_logic;
		bmf_north_int : IN std_logic;
		bmf_south_int : IN std_logic;
	    Soft_TEMAC_MII_TX_CLK_pin : in std_logic;
	    Soft_TEMAC_GMII_TX_EN_pin : out std_logic;
	    Soft_TEMAC_PHY_RST_N_pin : out std_logic;
	    Soft_TEMAC_GMII_TXD_pin : out std_logic_vector(7 downto 0);
	    Soft_TEMAC_MDIO_pin : inout std_logic;
	    Soft_TEMAC_MDC_pin : out std_logic;
	    Soft_TEMAC_GMII_RXD_pin : in std_logic_vector(7 downto 0);
	    Soft_TEMAC_GMII_TX_ER_pin : out std_logic;
	    Soft_TEMAC_GMII_COL_pin : in std_logic;
	    Soft_TEMAC_GMII_RX_ER_pin : in std_logic;
	    Soft_TEMAC_GMII_RX_CLK_pin : in std_logic;
	    Soft_TEMAC_GMII_RX_DV_pin : in std_logic;
	    Soft_TEMAC_GMII_CRS_pin : in std_logic;
	    Soft_TEMAC_GMII_TX_CLK_pin : out std_logic;
	    axi_gpio_xvc_GPIO_IO_I_pin : in std_logic_vector(3 downto 0);
	    axi_gpio_xvc_GPIO_IO_O_pin : out std_logic_vector(3 downto 0);
	    axi_gpio_xvc_GPIO_IO_T_pin : out std_logic_vector(3 downto 0)
	);
	END COMPONENT;

	component por
		generic (
			f_clk : integer := 100000000;		-- 100 MHz CPU speed
			t_por_ms : integer := 1000			-- 1000 ms power on reset time
		);
		port (
			clk : in std_logic;
			reset : out std_logic
		);
	end component;
	
	component i2c_eeprom
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000
		);
		port (
			clk : in std_logic;
			reset : in std_logic;
			
			scl : inout std_logic;
			sda : inout std_logic;
			
			csr_rd : out std_logic_vector(7 downto 0);
			csr_wr : in std_logic_vector(7 downto 0);
			csr_rden : in std_logic;
			csr_wren : in std_logic;
			
			data_rd : out std_logic_vector(7 downto 0);
			data_wr : in std_logic_vector(7 downto 0);
			data_rden : in std_logic;
			data_wren : in std_logic;
			
			addrl_rd : out std_logic_vector(7 downto 0);
			addrl_wr : in std_logic_vector(7 downto 0);
			addrl_rden : in std_logic;
			addrl_wren : in std_logic;
			addrh_rd : out std_logic_vector(7 downto 0);
			addrh_wr : in std_logic_vector(7 downto 0);
			addrh_rden : in std_logic;
			addrh_wren : in std_logic
		);
	end component;

	component i2c_tca6424a
		generic (
			f_clk : integer := 100000000;
			f_scl : integer := 100000;
			tca_addr : std_logic := '0'
		);
		port (
			clk : in std_logic;
			reset : in std_logic;
			
			-- I2C interface
			scl : inout std_logic;
			sda : inout std_logic;
			
			-- ports
			port_input : out std_logic_vector(23 downto 0);
			port_output : in std_logic_vector(23 downto 0);
			port_dir : in std_logic_vector(23 downto 0)
		);
	end component;

	component bcf_reboot
	  generic (
	  	f_clk : integer := 100000000
	  );
	  port (
		clk : in std_logic;
		issue_reboot : in std_logic;
		recovery : in std_logic
	  );
  	end component;

	attribute box_type : string;
	attribute box_type of bcf_mb : component is "user_black_box";
	
	----------------------
	-- Internal Signals --
	----------------------
	
	-- system clock and reset
	signal clk : std_logic := '0';
	signal sys_clk : std_logic := '0';
	signal sys_rst : std_logic := '0';
	signal mb_reset : std_logic := '0';
	signal por_reset : std_logic := '0';
	signal ext_reset : std_logic := '0';

	-- Reg Bank
	signal reg_wr : RegBank_wr_t(0 to 127);
	signal reg_wren : RegBank_wren_t(0 to 127);
	signal reg_rd : RegBank_rd_t(0 to 127); 
	signal reg_rden : RegBank_rden_t(0 to 127);

	-- BCF secure reg
	signal secure_reg : std_logic := '0';
	constant secure_cnt_max : integer := (10*100000000)-1;
	signal secure_cnt : integer range 0 to secure_cnt_max := 0;
	
	-- Wishbone
	-- Wishbone
	signal wbm0_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbm0_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm0_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm0_cyc : std_logic := '0';
	signal wbm0_stb : std_logic := '0';
	signal wbm0_we : std_logic := '0';
	signal wbm0_ack : std_logic := '0';
	signal wbm1_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbm1_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm1_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbm1_cyc : std_logic := '0';
	signal wbm1_stb : std_logic := '0';
	signal wbm1_we : std_logic := '0';
	signal wbm1_ack : std_logic := '0';
	signal wbs0_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs0_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs0_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs0_cyc : std_logic := '0';
	signal wbs0_stb : std_logic := '0';
	signal wbs0_we : std_logic := '0';
	signal wbs0_ack : std_logic := '0';
	signal wbs1_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs1_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs1_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs1_cyc : std_logic := '0';
	signal wbs1_stb : std_logic := '0';
	signal wbs1_we : std_logic := '0';
	signal wbs1_ack : std_logic := '0';
	signal wbs2_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs2_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs2_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs2_cyc : std_logic := '0';
	signal wbs2_stb : std_logic := '0';
	signal wbs2_we : std_logic := '0';
	signal wbs2_ack : std_logic := '0';
	signal wbs3_address : std_logic_vector(15 downto 0) := (others => '0');
	signal wbs3_dat_i : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs3_dat_o : std_logic_vector(7 downto 0) := (others => '0');
	signal wbs3_cyc : std_logic := '0';
	signal wbs3_stb : std_logic := '0';
	signal wbs3_we : std_logic := '0';
	signal wbs3_ack : std_logic := '0';
	
	-- laser enable
	signal laser_enable_i : std_logic := '0';
	signal laser_enable_mon_sr : std_logic_vector(2 downto 0) := "000";
	signal laser_enable_mon_cnt : unsigned(7 downto 0) := x"00";
	
	-- jtag switch
	signal jtag_switch_reg : std_logic_vector(7 downto 0) := (others => '0');

	-- xvc gpio
	signal xvc_gpio_o : std_logic_vector(3 downto 0) := (others => '0');
	signal xvc_gpio_i : std_logic_vector(3 downto 0) := (others => '0');
	signal xvc_gpio_t : std_logic_vector(3 downto 0) := (others => '0');

	-- wishbone synchronizer stages for external bus interface
	signal wb_ack_n_sync : std_logic_vector(1 downto 0) := "00";
	signal wb_ack_s_sync : std_logic_vector(1 downto 0) := "00";
	
	-- epc signals
	signal epc_addr_rev : std_logic_vector(0 to 17);
	signal epc_addr : std_logic_vector(17 downto 0);
	signal epc_data_miso_rev : std_logic_vector(0 to 31);
	signal epc_data_miso : std_logic_vector(31 downto 0);
	signal epc_data_mosi_rev : std_logic_vector(0 to 31);
	signal epc_data_mosi : std_logic_vector(31 downto 0);
	signal epc_cs_n : std_logic;
	signal epc_rnw : std_logic;
	signal epc_rdy : std_logic;

	-- serial number status bits
	signal sernum_ok : std_logic := '0';
	signal sernum_done : std_logic := '0';
	signal sernum_reset : std_logic := '0';

	-- i2c port expander
	signal i2c_gpio_input : std_logic_vector(23 downto 0) := (others => '0');
	signal i2c_gpio_output : std_logic_vector(23 downto 0) := (others => '0');
	signal i2c_gpio_dir : std_logic_vector(23 downto 0) := (others => '0');

	-- setup bus data
	signal sb_data_in : std_logic_vector(7 downto 0);
	signal sb_data_out : std_logic_vector(7 downto 0);
	signal sb_data_oe : std_logic;
	signal sb_busy_int : std_logic;

	-- git hash
	signal git_hash : std_logic_vector(159 downto 0) := (others => '0');

	-- reboot signal
	signal reboot_reg : std_logic_vector(7 downto 0) := (others => '0');
	signal issue_reboot : std_logic := '0';
	signal recovery : std_logic := '0';

	-- MGT frequency select
	signal mgt_fs_reg : std_logic_vector(7 downto 0) := (others => '0');
begin

	---------------------------
	-- Wishbone interconnect --
	---------------------------
	wb_interconnect_i: wb_interconnect
		port map (
			clk => sys_clk,
			reset => sys_rst,
			wbm0_address => wbm0_address,
			wbm0_dat_o => wbm0_dat_o,
			wbm0_dat_i => wbm0_dat_i,
			wbm0_cyc => wbm0_cyc,
			wbm0_stb => wbm0_stb,
			wbm0_we => wbm0_we,
			wbm0_ack => wbm0_ack,
			wbm1_address => wbm1_address,
			wbm1_dat_o => wbm1_dat_o,
			wbm1_dat_i => wbm1_dat_i,
			wbm1_cyc => wbm1_cyc,
			wbm1_stb => wbm1_stb,
			wbm1_we => wbm1_we,
			wbm1_ack => wbm1_ack,
			wbs0_address => wbs0_address,
			wbs0_dat_o => wbs0_dat_o,
			wbs0_dat_i => wbs0_dat_i,
			wbs0_cyc => wbs0_cyc,
			wbs0_stb => wbs0_stb,
			wbs0_we => wbs0_we,
			wbs0_ack => wbs0_ack,
			wbs1_address => wbs1_address,
			wbs1_dat_o => wbs1_dat_o,
			wbs1_dat_i => wbs1_dat_i,
			wbs1_cyc => wbs1_cyc,
			wbs1_stb => wbs1_stb,
			wbs1_we => wbs1_we,
			wbs1_ack => wbs1_ack,
			wbs2_address => wbs2_address,
			wbs2_dat_o => wbs2_dat_o,
			wbs2_dat_i => wbs2_dat_i,
			wbs2_cyc => wbs2_cyc,
			wbs2_stb => wbs2_stb,
			wbs2_we => wbs2_we,
			wbs2_ack => wbs2_ack,
			wbs3_address => wbs3_address,
			wbs3_dat_o => wbs3_dat_o,
			wbs3_dat_i => wbs3_dat_i,
			wbs3_cyc => wbs3_cyc,
			wbs3_stb => wbs3_stb,
			wbs3_we => wbs3_we,
			wbs3_ack => wbs3_ack
		);

	---------------------------
	-- Setup Bus to Wishbone --
	-- (acts as master 0)	 --
	---------------------------
	sb2wb_i: sb2wb PORT MAP(
		clk => sys_clk,
		reset => sys_rst,
		sb_write_n => sb_wrt_n,
		sb_strobe_n => sb_stb_n,
		sb_addr => sb_addr,
		sb_data_in => sb_data_in,
		sb_data_out => sb_data_out,
		sb_data_oe => sb_data_oe,
		sb_busy => sb_busy_int,
		wb_we => wbm1_we,
		wb_stb => wbm1_stb,
		wb_cyc => wbm1_cyc,
		wb_dat_mosi => wbm1_dat_o,
		wb_dat_miso => wbm1_dat_i,
		wb_ack => wbm1_ack,
		wb_addr => wbm1_address
	);

	sb_data <= sb_data_out when sb_data_oe = '1' else (others => 'Z');
	sb_data_in <= sb_data;
	sb_busy <= sb_busy_int;
	
	-----------------------
	-- Register bank     --
	-- (acts as slave 0) --
	-----------------------
	BOCRegBank_i: BOCRegBank 
	GENERIC MAP(
		nReg => 128		-- 128 registers in this register bank
	)
	PORT MAP(
		clk_i => sys_clk,
		rst_i => sys_rst,
		wb_dat_i => wbs0_dat_i,
		wb_dat_o => wbs0_dat_o,
		wb_adr_i => wbs0_address,
		wb_cyc_i => wbs0_cyc,
		wb_stb_i => wbs0_stb,
		wb_we_i => wbs0_we,
		wb_ack_o => wbs0_ack,
		reg_wr => reg_wr,
		reg_wren => reg_wren,
		reg_rd => reg_rd,
		reg_rden => reg_rden
	);

	----------------------
	-- simple registers --
	----------------------

	-- the firmware register represents a read only register for version identification
	Firmware: SimpleReg
		GENERIC MAP (
			readonly => "true",
			rst_value => x"50"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(0),
			wren => reg_wren(0),
			rd => reg_rd(0),
			rden => reg_rden(0),
			regval => open
		);

	-- readonly registers representing the build date and time of the firmware
	-- (will be updated from the Makefile)
	BuildDate_i: BuildDate
		PORT MAP (
			year => reg_rd(1),
			month => reg_rd(2),
			day => reg_rd(3),
			hour => reg_rd(4),
			minute => reg_rd(5),
			second => reg_rd(6),
			git_hash => git_hash
		);

	git_hash_regs: for I in 0 to 19 generate
		reg_rd(80+I) <= git_hash(8*I+7 downto 8*I);
	end generate;

	-- Register for laser enable (needs to be written to 0xAB to enable all lasers)
	process
		variable tmp : std_logic_vector(2 downto 0);
	begin
		wait until rising_edge(sys_clk);

		if sys_rst = '1' then
			laser_enable_i <= '0';
			reg_rd(7) <= x"00";
		else
			if (reg_wren(7) = '1') and (secure_reg = '1') then
				if reg_wr(7) = x"AB" then
					laser_enable_i <= '1';
				else
					laser_enable_i <= '0';
				end if;
			end if;

			-- register readback
			tmp := det_las_en & boc_las_en & laser_enable_i;
			case tmp is
				when "000" => reg_rd(7) <= x"00";
				when "001" => reg_rd(7) <= x"01";
				when "010" => reg_rd(7) <= x"02";
				when "011" => reg_rd(7) <= x"03";
				when "100" => reg_rd(7) <= x"04";
				when "101" => reg_rd(7) <= x"05";
				when "110" => reg_rd(7) <= x"06";
				when "111" => reg_rd(7) <= x"AB";
				when others => reg_rd(7) <= x"00";
			end case;
		end if;
	end process;
	laser_enable <= laser_enable_i;
	
	-- monitor laser interlock
	laser_enable_mon_sr <= laser_enable_mon_sr(1 downto 0) & (laser_enable_i and det_las_en and boc_las_en) when rising_edge(sys_clk);
	process begin
		wait until rising_edge(sys_clk);

		if sys_rst = '1' then
			laser_enable_mon_cnt <= x"00";
		else
			if laser_enable_mon_sr(2 downto 1) = "10" then
				laser_enable_mon_cnt <= laser_enable_mon_cnt + 1;
			end if;
		end if;
	end process;
	reg_rd(43) <= std_logic_vector(laser_enable_mon_cnt);

	-- Register for JTAG switch
	JTAGSwitchReg: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"00"
		)
		PORT MAP(
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(8),
			wren => reg_wren(8),
			rd => reg_rd(8),
			rden => reg_rden(8),
			regval => jtag_switch_reg
		);

	jtag_switch <= "01" when jtag_switch_reg = x"AB" else
				   "10" when jtag_switch_reg = x"54" else
				   "00";
				   
	-- Microblaze primitive processing
	MB_PrimProc_Op: SimpleReg
			GENERIC MAP (
				readonly => "false",
				rst_value => x"00"
			)
			PORT MAP (
				clk_i => sys_clk,
				rst_i => sys_rst,
				wr => reg_wr(16),
				wren => reg_wren(16),
				rd => reg_rd(16),
				rden => reg_rden(16),
				regval => open
			);
			
	MB_PrimProc_Args: for I in 0 to 3 generate
		MB_PrimProc_Arg_I: SimpleReg
			GENERIC MAP (
				readonly => "false",
				rst_value => x"00"
			)
			PORT MAP (
				clk_i => sys_clk,
				rst_i => sys_rst,
				wr => reg_wr(17+I),
				wren => reg_wren(17+I),
				rd => reg_rd(17+I),
				rden => reg_rden(17+I),
				regval => open
			);
	end generate;
	
	MB_PrimProc_Rets: for I in 0 to 3 generate
		MB_PrimProc_Ret_I: SimpleReg
			GENERIC MAP (
				readonly => "false",
				rst_value => x"00"
			)
			PORT MAP (
				clk_i => sys_clk,
				rst_i => sys_rst,
				wr => reg_wr(21+I),
				wren => reg_wren(21+I),
				rd => reg_rd(21+I),
				rden => reg_rden(21+I),
				regval => open
			);
	end generate;
	
	Slot_Reg: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"FF"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(25),
			wren => reg_wren(25),
			rd => reg_rd(25),
			rden => reg_rden(25),
			regval => open
		);
		
	Crate_Reg: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"FF"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(26),
			wren => reg_wren(26),
			rd => reg_rd(26),
			rden => reg_rden(26),
			regval => open
		);

	reg_rd(37) <= x"03";

	DCSHandshake_Reg: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"00"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(38),
			wren => reg_wren(38),
			rd => reg_rd(38),
			rden => reg_rden(38),
			regval => open
		);

	Reboot_Reg_i: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"00"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(39),
			wren => reg_wren(39),
			rd => reg_rd(39),
			rden => reg_rden(39),
			regval => reboot_reg
		);

	BCFState_Reg_i: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"01"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(40),
			wren => reg_wren(40),
			rd => reg_rd(40),
			rden => reg_rden(40),
			regval => open
		);

	MGTFS_Reg_i: SimpleReg
		GENERIC MAP (
			readonly => "false",
			rst_value => x"00"
		)
		PORT MAP (
			clk_i => sys_clk,
			rst_i => sys_rst,
			wr => reg_wr(41),
			wren => reg_wren(41),
			rd => reg_rd(41),
			rden => reg_rden(41),
			regval => mgt_fs_reg
		);

	mgt_fs <= mgt_fs_reg(2 downto 0);
	
	-- some general purpose registers
	-- can be used to communicate with the microblaze via setup bus
	SomeRegs: for I in 44 to 47 generate
		reg_I: SimpleReg
			GENERIC MAP (
				readonly => "false",
				rst_value => x"00"
			)
			PORT MAP (
				clk_i => sys_clk,
				rst_i => sys_rst,
				wr => reg_wr(I),
				wren => reg_wren(I),
				rd => reg_rd(I),
				rden => reg_rden(I),
				regval => open
			);
	end generate;

	------------------------
	-- serial number chip --
	------------------------
	ds2401_sernum_i: ds2401_sernum
		GENERIC MAP (
			f_clk => 100000000
		)
		PORT MAP (
			clk => sys_clk,
			reset => sys_rst,
			dq => sernum,
			sernum(7 downto 0) => reg_rd(9),
			sernum(15 downto 8) => reg_rd(10),
			sernum(23 downto 16) => reg_rd(11),
			sernum(31 downto 24) => reg_rd(12),
			sernum(39 downto 32) => reg_rd(13),
			sernum(47 downto 40) => reg_rd(14),
			sernum_ok => sernum_ok,
			sernum_done => sernum_done,
			sernum_reset => sernum_reset
		);

	reg_rd(15) <= "00000" & sernum_reset & sernum_ok & sernum_done;
	process begin
		wait until rising_edge(sys_clk);

		if reg_wren(15) = '1' then
			sernum_reset <= reg_wr(15)(2);
		end if;
	end process;
			
	-----------------------
	-- I2C EEPROM access --
	-----------------------
	i2c_eeprom_i: i2c_eeprom
		GENERIC MAP (
			f_clk => 100000000,
			f_scl => 100000
		)
		PORT MAP (
			clk => sys_clk,
			reset => sys_rst,
			scl => i2c_scl,
			sda => i2c_sda,
			csr_wr => reg_wr(27),
			csr_rd => reg_rd(27),
			csr_wren => reg_wren(27),
			csr_rden => reg_rden(27),
			data_wr => reg_wr(28),
			data_rd => reg_rd(28),
			data_wren => reg_wren(28),
			data_rden => reg_rden(28),
			addrl_wr => reg_wr(29),
			addrl_rd => reg_rd(29),
			addrl_wren => reg_wren(29),
			addrl_rden => reg_rden(29),
			addrh_wr => reg_wr(30),
			addrh_rd => reg_rd(30),
			addrh_wren => reg_wren(30),
			addrh_rden => reg_rden(30)
		);

	-----------------------
	-- PLL configuration --
	-----------------------
	ePLLUnit_i: ePLLUnit PORT MAP (
		clk_i => sys_clk,
		rst_i => sys_rst,
		ctrl_rd => reg_rd(48),
		ctrl_wr => reg_wr(48),
		ctrl_wren => reg_wren(48),
		status_rd => reg_rd(49),
		data0_rd => reg_rd(50),
		data0_wr => reg_wr(50),
		data0_wren => reg_wren(50),
		data1_rd => reg_rd(51),
		data1_wr => reg_wr(51),
		data1_wren => reg_wren(51),
		data2_rd => reg_rd(52),
		data2_wr => reg_wr(52),
		data2_wren => reg_wren(52),
		data3_rd => reg_rd(53),
		data3_wr => reg_wr(53),
		data3_wren => reg_wren(53),
		idelay0_rd => reg_rd(54),
		idelay0_wr => reg_wr(54),
		idelay0_wren => reg_wren(54),
		idelay1_rd => reg_rd(55),
		idelay1_wr => reg_wr(55),
		idelay1_wren => reg_wren(55),
		pll_sck => pll_sck,
		pll_mosi => pll_mosi,
		pll_miso => pll_miso,
		pll_cs_n => pll_cs_n,
		pll_locked => pll_locked,
		pll_idelay => pll_idelay
	);

	----------------------
	-- clock monitoring --
	----------------------
	clocking_revD_i: clocking_revBC
		PORT MAP (
			clk => sys_clk,
			reset => sys_rst,
			freq0_rd => reg_rd(33),
			freq0_wr => reg_wr(33),
			freq0_rden => reg_rden(33),
			freq0_wren => reg_wren(33),
			freq1_rd => reg_rd(34),
			freq1_wr => reg_wr(34),
			freq1_rden => reg_rden(34),
			freq1_wren => reg_wren(34),
			freq2_rd => reg_rd(35),
			freq2_wr => reg_wr(35),
			freq2_rden => reg_rden(35),
			freq2_wren => reg_wren(35),
			freq3_rd => reg_rd(36),
			freq3_wr => reg_wr(36),
			freq3_rden => reg_rden(36),
			freq3_wren => reg_wren(36),
			vme_clk => vme_clk
	);

	---------------------------------------
	-- programming unit for northern BMF --
	---------------------------------------
	ProgUnit_N_i: ProgUnit PORT MAP(
		clk_i => sys_clk,
		rst_i => sys_rst,
		fl_sck => spi_n_sck,
		fl_si => spi_n_si,
		fl_so => spi_n_so,
		fl_cs_n => spi_n_cs_n,
		fl_wp_n => spi_n_wp_n,
		fl_hold_n => spi_n_hold_n,
		jtag_done => jtag_n_done,
		jtag_tck => jtag_n_tck,
		jtag_tms => jtag_n_tms,
		jtag_tdo => jtag_n_tdo,
		jtag_tdi => jtag_n_tdi,
		control_reg_rd => reg_rd(64),
		control_reg_wr => reg_wr(64),
		control_reg_wren => reg_wren(64),
		status_reg_rd => reg_rd(65),
		status_reg_wr => reg_wr(65),
		status_reg_wren => reg_wren(65),
		dcnt_reg_rd => reg_rd(66),
		dcnt_reg_wr => reg_wr(66),
		dcnt_reg_wren => reg_wren(66),
		pfifo_reg_rd => reg_rd(67),
		pfifo_reg_wr => reg_wr(67),
		pfifo_reg_wren => reg_wren(67),
		fl_addr0_reg_rd => reg_rd(68),
		fl_addr0_reg_wr => reg_wr(68),
		fl_addr0_reg_wren => reg_wren(68),
		fl_addr1_reg_rd => reg_rd(69),
		fl_addr1_reg_wr => reg_wr(69),
		fl_addr1_reg_wren => reg_wren(69),
		fl_addr2_reg_rd => reg_rd(70),
		fl_addr2_reg_wr => reg_wr(70),
		fl_addr2_reg_wren => reg_wren(70),
		crc0_reg_rd => reg_rd(56),
		crc1_reg_rd => reg_rd(57),
		crc2_reg_rd => reg_rd(58),
		crc3_reg_rd => reg_rd(59),
		secure_reg => secure_reg
	);
	
	ProgUnit_S_i: ProgUnit 
		PORT MAP(
		clk_i => sys_clk,
		rst_i => sys_rst,
		fl_sck => spi_s_sck,
		fl_si => spi_s_si,
		fl_so => spi_s_so,
		fl_cs_n => spi_s_cs_n,
		fl_wp_n => spi_s_wp_n,
		fl_hold_n => spi_s_hold_n,
		jtag_done => jtag_s_done,
		jtag_tck => jtag_s_tck,
		jtag_tms => jtag_s_tms,
		jtag_tdo => jtag_s_tdo,
		jtag_tdi => jtag_s_tdi,
		control_reg_rd => reg_rd(72),
		control_reg_wr => reg_wr(72),
		control_reg_wren => reg_wren(72),
		status_reg_rd => reg_rd(73),
		status_reg_wr => reg_wr(73),
		status_reg_wren => reg_wren(73),
		dcnt_reg_rd => reg_rd(74),
		dcnt_reg_wr => reg_wr(74),
		dcnt_reg_wren => reg_wren(74),
		pfifo_reg_rd => reg_rd(75),
		pfifo_reg_wr => reg_wr(75),
		pfifo_reg_wren => reg_wren(75),
		fl_addr0_reg_rd => reg_rd(76),
		fl_addr0_reg_wr => reg_wr(76),
		fl_addr0_reg_wren => reg_wren(76),
		fl_addr1_reg_rd => reg_rd(77),
		fl_addr1_reg_wr => reg_wr(77),
		fl_addr1_reg_wren => reg_wren(77),
		fl_addr2_reg_rd => reg_rd(78),
		fl_addr2_reg_wr => reg_wr(78),
		fl_addr2_reg_wren => reg_wren(78),
		crc0_reg_rd => reg_rd(60),
		crc1_reg_rd => reg_rd(61),
		crc2_reg_rd => reg_rd(62),
		crc3_reg_rd => reg_rd(63),
		secure_reg => secure_reg
	);


	-----------------------------------------------------
	-- microblaze to handle ethernet communication and --
	-- self-test of the BOC                            --
	-----------------------------------------------------
	bcf_mb_i: bcf_mb PORT MAP(
		RS232_txd => uart_tx,
		RS232_rxd => uart_rx,
		RESET => mb_reset,
--		RESET => reset_in,
		MCB_DDR2_rzq => ddr2_rzq,
		MCB_DDR2_dram_we_n => ddr2_we_n,
		MCB_DDR2_dram_udqs_n => ddr2_udqs_n,
		MCB_DDR2_dram_udqs => ddr2_udqs_p,
		MCB_DDR2_dram_udm => ddr2_udm,
		MCB_DDR2_dram_ras_n => ddr2_ras_n,
		MCB_DDR2_dram_odt => ddr2_odt,
		MCB_DDR2_dram_ldm => ddr2_ldm,
		MCB_DDR2_dram_dqs_n => ddr2_ldqs_n,
		MCB_DDR2_dram_dqs => ddr2_ldqs_p,
		MCB_DDR2_dram_dq => ddr2_dq,
		MCB_DDR2_dram_clk_n => ddr2_clk_n,
		MCB_DDR2_dram_clk => ddr2_clk_p,
		MCB_DDR2_dram_cke => ddr2_cke,
		MCB_DDR2_dram_cas_n => ddr2_cas_n,
		MCB_DDR2_dram_ba => ddr2_ba,
		MCB_DDR2_dram_addr => ddr2_a,
--		CLK_P => clk_p,
--		CLK_N => clk_n,
		CLK => clk,
		axi_epc_0_PRH_CS_n_pin => epc_cs_n,
		axi_epc_0_PRH_Addr_pin => epc_addr_rev,
		axi_epc_0_PRH_RNW_pin => epc_rnw,
		axi_epc_0_PRH_Rdy_pin => epc_rdy,
		axi_epc_0_PRH_Data_O_pin => epc_data_mosi_rev,
		axi_epc_0_PRH_Data_I_pin => epc_data_miso_rev,
		clock_generator_0_CLKOUT2_pin => sys_clk,
		Soft_TEMAC_MII_TX_CLK_pin => gmii_tx_clk,
		Soft_TEMAC_GMII_TX_EN_pin => gmii_tx_en,
		Soft_TEMAC_PHY_RST_N_pin => gmii_phy_rstb,
		Soft_TEMAC_GMII_TXD_pin => gmii_txd,
		Soft_TEMAC_MDIO_pin => gmii_mdio,
		Soft_TEMAC_MDC_pin => gmii_mdc,
		Soft_TEMAC_GMII_RXD_pin => gmii_rxd,
		Soft_TEMAC_GMII_TX_ER_pin => gmii_tx_er,
		Soft_TEMAC_GMII_COL_pin => gmii_col,
		Soft_TEMAC_GMII_RX_ER_pin => gmii_rx_er,
		Soft_TEMAC_GMII_RX_CLK_pin => gmii_rx_clk,
		Soft_TEMAC_GMII_RX_DV_pin => gmii_rx_dv,
		Soft_TEMAC_GMII_CRS_pin => gmii_crs,
		Soft_TEMAC_GMII_TX_CLK_pin => gmii_gtx_clk,
		--Generic_IIC_Bus_Sda => i2c_sda,
		--Generic_IIC_Bus_Scl => i2c_scl,
		RESET_OUT => sys_rst,
		axi_gpio_0_GPIO_IO_pin => gpio_led,
		axi_spi_0_SPISEL_pin => '0',
		axi_spi_0_SCK_pin => bcf_flash_sck,
		axi_spi_0_MISO_pin => bcf_flash_miso,
		axi_spi_0_MOSI_pin => bcf_flash_mosi,
		axi_spi_0_SS_pin => bcf_flash_cs_n,
		bmf_north_int => '0',
		bmf_south_int => '0',
		axi_gpio_xvc_GPIO_IO_I_pin => xvc_gpio_i,
		axi_gpio_xvc_GPIO_IO_O_pin => xvc_gpio_o,
		axi_gpio_xvc_GPIO_IO_T_pin => xvc_gpio_t
	);

	-- reverse bit order of the EPC
	epc_data_convert: for I in 0 to 31 generate
		epc_data_mosi(I) <= epc_data_mosi_rev(31-I);
		epc_data_miso_rev(I) <= epc_data_miso(31-I);
	end generate;
	
	epc_addr_convert: for I in 0 to 17 generate
		epc_addr(I) <= epc_addr_rev(17-I);
	end generate;

	-- map internal wishbone to external peripheral controller (EPC will be master 1)
	-- as epc uses 32-bit data width we only use the lower 8 bits and expand the EPC address by 2 bits
	wbm0_we <= not epc_rnw;
	wbm0_stb <= not epc_cs_n;
	wbm0_cyc <= not epc_cs_n;
	epc_rdy <= wbm0_ack;
	wbm0_dat_o <= epc_data_mosi(7 downto 0) when epc_cs_n = '0' else (others => '0');
	epc_data_miso(7 downto 0) <= wbm0_dat_i;
	epc_data_miso(31 downto 8) <= (others => '0');
	wbm0_address <= epc_addr(17 downto 2) when epc_cs_n = '0' else (others => '0');		-- expanded epc address
	
	-- Slave 1 - Southbone
	wb_bmf_s_adr <= wbs1_address;
	wbs1_dat_o <= wb_bmf_s_dat_i;
	wb_bmf_s_dat_o <= wbs1_dat_i;
	wb_bmf_s_stb <= wbs1_stb;
	wb_bmf_s_cyc <= wbs1_cyc;
	wb_bmf_s_we <= wbs1_we;
	wbs1_ack <= wb_ack_s_sync(1);
	
	-- Slave 2 - Northbone
	wb_bmf_n_adr <= wbs2_address;
	wbs2_dat_o <= wb_bmf_n_dat_i;
	wb_bmf_n_dat_o <= wbs2_dat_i;
	wb_bmf_n_stb <= wbs2_stb;
	wb_bmf_n_cyc <= wbs2_cyc;
	wb_bmf_n_we <= wbs2_we;
	wbs2_ack <= wb_ack_n_sync(1);

	-- Slave 3 - General purpose memory (16kB)
	bcf_gpmem_i: bcf_gpmem
		port map (
			clk => sys_clk,
			wb_addr => wbs3_address,
			wb_dat_i => wbs3_dat_i,
			wb_dat_o => wbs3_dat_o,
			wb_cyc => wbs3_cyc,
			wb_stb => wbs3_stb,
			wb_we => wbs3_we,
			wb_ack => wbs3_ack
		);
	
	-- wishbone ack synchronizer
	-- including masking, if the BMF is not loaded
	process begin
		wait until rising_edge(sys_clk);
		
		if sys_rst = '1' then
			wb_ack_n_sync <= "00";
			wb_ack_s_sync <= "00";
		else
			if jtag_n_done = '1' then
				wb_ack_n_sync <= wb_ack_n_sync(0) & wb_bmf_n_ack;
			else
				wb_ack_n_sync <= wb_ack_n_sync(0) & '0';
			end if;

			if jtag_s_done = '1' then
				wb_ack_s_sync <= wb_ack_s_sync(0) & wb_bmf_s_ack;
			else
				wb_ack_s_sync <= wb_ack_s_sync(0) & '0';
			end if;
		end if;
	end process;

	-- output reset
	reset_out <= sys_rst;

	-- power on reset
	por_i: por
		GENERIC MAP (
			f_clk => 100000000,
			t_por_ms => 5000
		)
		PORT MAP (
			clk => clk,
			reset => por_reset
		);

	-- generate microblaze reset
	process begin
		wait until rising_edge(clk);

		ext_reset <= reset_in;
		mb_reset <= ext_reset or por_reset;
	end process;

	-- clock input buffer
	clkinbuf: IBUFGDS PORT MAP (
		I => clk_p,
		IB => clk_n,
		O => clk
	);

	-- I2C port expander
	i2c_expander_i: i2c_tca6424a
		GENERIC MAP (
			f_clk => 100000000,
			f_scl => 100000,
			TCA_addr => '0'
		)
		PORT MAP (
			clk => sys_clk,
			reset => sys_rst,
			scl => i2c_gpio_scl,
			sda => i2c_gpio_sda,
			port_input => i2c_gpio_input,
			port_output => i2c_gpio_output,
			port_dir => i2c_gpio_dir
		);

	-- LEDs as Output and pin header as input
	i2c_gpio_dir <= x"FF0000";

	-- route back pin header status to LED
	i2c_gpio_output <= x"00AA" & i2c_gpio_input(23 downto 16);

	-- BCF reboot mechanism
		bcf_reboot_i: bcf_reboot
		generic map (
			f_clk => 100000000
		)
		port map (
			clk => sys_clk,
			issue_reboot => issue_reboot,
			recovery => recovery
		);

	issue_reboot <= '1' when reboot_reg = x"AB" else
					'1' when reboot_reg = x"54" else
					'1' when sys_rst = '1' else
					'0';

	recovery <= '1' when reboot_reg = x"54" else '0';

	-- BOC ok output
	boc_ok <= jtag_n_done and jtag_s_done;

	-- JTAG XVC interface
	jtag_xvc_tck <= xvc_gpio_o(0);
	jtag_xvc_tms <= xvc_gpio_o(1);
	jtag_xvc_tdi <= xvc_gpio_o(2);
	xvc_gpio_i(0) <= xvc_gpio_o(0);
	xvc_gpio_i(1) <= xvc_gpio_o(1);
	xvc_gpio_i(2) <= xvc_gpio_o(2);
	xvc_gpio_i(3) <= jtag_xvc_tdo;

	-- secure reg
	HasSecureReg_true: if HasSecureReg = true generate
		process begin
			wait until rising_edge(sys_clk);

			if (reg_wr(42) = x"AB") and (reg_wren(42) = '1') then
				secure_cnt <= secure_cnt_max;
				secure_reg <= '1';
			else
				if secure_cnt = 0 then
					secure_reg <= '0';
				else
					secure_cnt <= secure_cnt-1;
					secure_reg <= '1';
				end if;
			end if;
		end process;

		-- indicate secure reg is available and active
		reg_rd(42) <= x"AB";
	end generate;

	HasSecureReg_false: if HasSecureReg = false generate
		secure_reg <= '1';
		reg_rd(42) <= x"00";
	end generate;


end Behavioral;

