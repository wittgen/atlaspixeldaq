/*
 * networking.h
 *
 *  Created on: May 23, 2012
 *      Author: kugel
 */

#ifndef NETCMDS_H_
#define NETCMDS_H_

#include <cstdint>


typedef struct {
	uint32_t magic;
	uint32_t command;
	uint32_t address;
	uint32_t parm;
	// on reads, parm is void
	// on writes, parm is the data work
	// on block reads and block writes, parm is the word count
	// on block writes, data follows immediately
	// on histogram config,
	//      address is last (max) address used in this configuration
	//      parm is the number of words following, describing the address converter settings
	// on histogram data,
	//      address is first pixel address of this block
	//      parm is the number of histogram data words following
	// on histogram termination address and parm are void
} RodTcpCmd;

#define MAGIC 0x41424344
#define WRITE_CMD 0x10
#define WRITE_BURST_CMD 0x11
#define READ_CMD 0x20
#define READ_BURST_CMD 0x21
#define HIST_DATA_CMD 0x40
#define HIST_CFG_CMD 0x41
#define HIST_TERM_CMD 0x42
#define CMD_CODE_MASK 0xFFFF    // lower 16 bit command
#define CMD_TID_RSHIFT 16       // upper 16 bit transaction id


#endif /* NETCMDS_H_ */
