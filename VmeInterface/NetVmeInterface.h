/////////////////////////////////////////////////////////////////////
//! NetVmeInterface.h 
//! version 1.2
//!
//! Dummy VmeInterface implemetation 
//!                                 
/////////////////////////////////////////////////////////////////////
// 24/04/2002  V 1.1 PM - First implementation
// 19/02/2004  V 1.2 PM - Safe single word R/W methods added            
//             

#ifndef SCTPIXELROD_DUMMY_VMEINTERFACE_H
#define SCTPIXELROD_DUMMY_VMEINTERFACE_H

#include <vector>

#include "VmeInterface.h"
#include "VmePort.h"

//AKU
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


namespace SctPixelRod {

class NetVmeInterface : public VmeInterface {
public:
  //! Constructor
  NetVmeInterface(const char *ip_addr);
  //! Destructor  
  ~NetVmeInterface();                                                                  
  //! Factory
  static NetVmeInterface* makeNetVmeInterface();                                                                  
  //! Factory
  static NetVmeInterface* makeNetVmeInterface(const char *ip_addr);                                                                  

  //! Install an interrupt handler
  virtual void declareInterruptHandler(VmeInterruptHandler &handler);
  //! Remove an interrupt handler
  virtual void removeInterruptHandler(VmeInterruptHandler &handler);
  //! Set the interrupt subvector mask
  virtual void cleanInterruptHandlers();
  //! Remove an interrupt handler
  virtual inline void activateSoftVector(unsigned char vect, unsigned char subvect) {
    if (subvect>0 && subvect<=32) {
      m_softVectMask |= 1 << (subvect-1);
    }
  };
  //! Re-enable VME RORA levels
  virtual void reEnableInterrupt() { }; 
  //! Suspend interrupt notification
  virtual void blockInterruptNotification(); 
  //! Resume interrupt notification
  virtual void resumeInterruptNotification(); 
  //! Set the interrupt data word
  virtual void setInterruptData(int32_t data) {
    m_interruptData = data;
  };
  //! Read the interrupt data word
  virtual int32_t getInterruptData() {
    return m_interruptData;
  };
  
  //! Return the number of bus errors
  virtual int32_t getBusErrors();
  //! Throw exceptions in case of bus errors
  virtual void busErrorReport();
  virtual void busErrorReport(const uint32_t handle);
  virtual void busErrorReport(VmePort &port);
  //! Return the last error code
  virtual int32_t getLastErrcode() { return 0; }
  //! Print the message corresponding to an error code
  virtual std::string getErrorMessage(const int32_t errcode);

  //! Byte read (automatic mapping)
  virtual unsigned char read8  (const uint32_t handle, const uint32_t offset);  
  virtual unsigned char readS8  (const uint32_t handle, const uint32_t offset);  
  // Word read (automatic mapping)
  virtual uint16_t read16 (const uint32_t handle, const uint32_t offset);  
  virtual uint16_t readS16 (const uint32_t handle, const uint32_t offset);  
  //! Long Word read (automatic mapping)
  virtual uint32_t read32 (const uint32_t handle, const uint32_t offset);  
  virtual uint32_t readS32 (const uint32_t handle, const uint32_t offset);  
  //! Byte write (automatic mapping)
  virtual void write8 (const uint32_t handle, const uint32_t offset, const unsigned char value);   
  virtual void writeS8 (const uint32_t handle, const uint32_t offset, const unsigned char value);   
  //! Word write (automatic mapping)
  virtual void write16(const uint32_t handle, const uint32_t offset, const uint16_t value);    
  virtual void writeS16(const uint32_t handle, const uint32_t offset, const uint16_t value);    
  //! Long word write (automatic mapping)
  virtual void write32(const uint32_t handle, const uint32_t offset, const uint32_t value);   
  virtual void writeS32(const uint32_t handle, const uint32_t offset, const uint32_t value);   

  //! 32 bits block read (automatic mapping)
  virtual void blockRead32 (VmePort &port, const uint32_t offset, uint32_t *buf, const int32_t len);  
  //! 64 bits block read (automatic mapping)
  virtual void blockRead64 (VmePort &port, const uint32_t offset, uint32_t *buf, const int32_t len);   
  //! 32 bits block write (automatic mapping)
  virtual void blockWrite32(VmePort &port, const uint32_t offset, const uint32_t *buf, const int32_t len);  
  //! 64 bits block write (automatic mapping)
  virtual void blockWrite64(VmePort &port, const uint32_t offset, const uint32_t *buf, const int32_t len);   

  //! Register a new VmePort object
  virtual uint32_t registerPort(VmePort &port);         
  //! Delete a VmePort
  virtual void deletePort(VmePort &port);
  //! Return a pointer to the port map
  //virtual void *getPortMap(const uint32_t handle);

private:
  std::vector<VmePort *> m_ports;
  sigset_t m_sigMask;                 // Mask for blocking signals

  static unsigned char m_softVectMask;                  // Interrupt subvector array
  static int32_t m_interruptData;                          // Interrupt data word
  static NetVmeInterface *m_if;                       // Static pointer to THE interface
  static std::vector<VmeInterruptHandler *> m_hardIH;        // Vector of basic IH (always executed)
  static std::vector<VmeInterruptHandler *> m_softIH[32];    // Vector of slaves IH (executed when the subvector mask set
                                                        // by the basic IH equals the one defined in the slave IH

  //! Interrupt handler
  static void m_interruptHandler(int signum);

  // AKU network stuff
  int m_rodSock;
  struct sockaddr_in m_rod_addr; // will hold the destination addr
  int m_netConnected;

};                                             // End of RCCVmeInterface declaration

} // End namespace SctPixelRod

#endif // SCTPIXELROD_RCCVMEINTERFACE_H
