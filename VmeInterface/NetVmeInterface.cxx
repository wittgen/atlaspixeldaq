/////////////////////////////////////////////////////////////////////
//! NetVmeInterface.cxx
//! version 1.2
//!
//! Dummy VmeInterface implemetation
//!
/////////////////////////////////////////////////////////////////////
// 20/02/2002  V 1.0 PM - First implementation
// 19/02/2004  V 1.2 PM - Safe single word R/W methods added
//

#include <signal.h>
#include <unistd.h>
#include <sys/types.h>


// AKU
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "netCmds.h"


#define MAGIC_MASTER_LOCATION 0x80000000
#define I_AM_ALIVE 0xC0FFEE
#define STILLBORN_BOOT 0xdeaddead

#define PORTNUM 5001
//static char ipStrings[][16] = {"127.0.0.1","147.142.42.20","192.168.1.10","192.168.1.11"};
//#define DEST_IP "147.142.42.20"
//#define DEST_IP "127.0.0.1"
//#define DEST_IP "192.168.1.10"
//#define DEST_IP "192.168.1.11"
//#define DEST_IP "192.168.0.134"
//extern int errno;

/////////////////////////////////////////////

#include "NetVmeInterface.h"

using namespace SctPixelRod;

unsigned char  NetVmeInterface::m_softVectMask;
int32_t NetVmeInterface::m_interruptData = 0;
NetVmeInterface *NetVmeInterface::m_if = NULL;
std::vector<VmeInterruptHandler *>  NetVmeInterface::m_hardIH;
std::vector<VmeInterruptHandler *>  NetVmeInterface::m_softIH[32];

void NetVmeInterface::m_interruptHandler(int signum) {
  static int is, smask;
  static unsigned int i;

  // Execute hard handlers
  m_softVectMask = 0;
  m_if->m_interruptData = 0;
  for (i=0; i<m_hardIH.size(); i++) {
    if (m_hardIH[i]->getActive()) {
      m_hardIH[i]->interruptHandler(0, 0, 0);
    }
  }

  // Execute soft handlers
  smask = m_softVectMask;
  for (is=0; is<32; is++) {
    if ((smask & (1 << is)) != 0) {
      for (i=0; i<m_softIH[is].size(); i++) {
        if (m_softIH[is][i]->getActive()) {
          m_softIH[is][i]->interruptHandler(0, is, 0);
	}
      }
    }
  }
}

NetVmeInterface::NetVmeInterface(const char *ip_addr) {
  // Install the global interrupt handler
  struct sigaction sa;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = m_interruptHandler;
  sigaction(SIGRTMAX-1, &sa, NULL);
  // Pepare the mask for blocking interrupt handlers
  sigemptyset(&m_sigMask);
  sigaddset(&m_sigMask, SIGRTMAX-1);
  // Set the pointer to this object
  m_if = this;
  // AKU network
  // creating socket
  //  char *destIp = ipStrings[slot - 1]; // slot starts at 1
  m_netConnected = 0;
  std::cout << "Creating socket" << std::endl;
  m_rodSock = socket(AF_INET, SOCK_STREAM, 0); // or PF_INET
  if (-1 == m_rodSock){ // we have an error
	  std::cout << "Error creating socket" << std::endl;
      //cout << strerror(errno) << endl;
      perror(__FUNCTION__);
      return;
  }
  // setup connection and connect
  m_rod_addr.sin_family = AF_INET; // host byte order
  m_rod_addr.sin_port = htons(PORTNUM); // uint16_t, network byte order
  m_rod_addr.sin_addr.s_addr = inet_addr(ip_addr);//destIp);
  memset(m_rod_addr.sin_zero, '\0', sizeof m_rod_addr.sin_zero);

  std::cout << "Connecting to " << ip_addr << ", port "<< PORTNUM << std::endl;
  int rc = connect(m_rodSock, (struct sockaddr *)&m_rod_addr, sizeof m_rod_addr);
  if (0 != rc){ // we have an error
	  std::cout << "Error connecting to socket" << std::endl;
      //cout << strerror(errno) << endl;
      perror(__FUNCTION__);
      return;
  }
  m_netConnected = 1;
  sleep(1);
}

NetVmeInterface::~NetVmeInterface() {
	if (m_netConnected) close(m_rodSock);
}

NetVmeInterface* NetVmeInterface::makeNetVmeInterface(const char *ip_addr){
  if (m_if == NULL) {
    return new NetVmeInterface(ip_addr);
  } else {
    return m_if;
  }
}
NetVmeInterface* NetVmeInterface::makeNetVmeInterface() {
  char ip_addr[10] = "127.0.0.1";
  return makeNetVmeInterface(ip_addr);
}

void NetVmeInterface::declareInterruptHandler(VmeInterruptHandler &handler) {
  unsigned char softvec = handler.getInterruptSoftVect();
  if (softvec == 0) {
    // Hard IH definition
    unsigned int i;
    for (i=0; i<m_hardIH.size(); i++) {
      if (&handler == m_hardIH[i]) return;
    }
    m_hardIH.push_back(&handler);
  } else if (softvec > 0 && softvec <= 32) {
    // Soft IH definition
    unsigned int i;
    for (i=0; i<m_softIH[softvec-1].size(); i++) {
      if (&handler == m_softIH[softvec-1][i]) return;
    }
    m_softIH[softvec-1].push_back(&handler);
  }
}

void NetVmeInterface::removeInterruptHandler(VmeInterruptHandler &handler) {
  char softvec = handler.getInterruptSoftVect();
  std::vector<VmeInterruptHandler *>::iterator i;
  if (softvec == 0) {
    // Basic IH cancellation
    for (i=m_hardIH.begin(); i != m_hardIH.end(); i++) {
      if (&handler == *i) {
        m_hardIH.erase(i);
        break;
      }
    }
  } else if (softvec > 0 && softvec <= 32) {
    // Slave IH cancellation
    for (i=m_softIH[softvec-1].begin(); i != m_softIH[softvec-1].end(); i++) {
      if (&handler == *i) {
        m_softIH[softvec-1].erase(i);
        return;
      }
    }
  }
}

void NetVmeInterface::cleanInterruptHandlers() {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::blockInterruptNotification() {
  sigprocmask(SIG_BLOCK, &m_sigMask, NULL);
}

void NetVmeInterface::resumeInterruptNotification() {
  sigprocmask(SIG_UNBLOCK, &m_sigMask, NULL);
}

unsigned char NetVmeInterface::read8  (const uint32_t handle, const uint32_t offset) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
  return 0;
}

unsigned char NetVmeInterface::readS8  (const uint32_t handle, const uint32_t offset) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
  return 0;
}

uint16_t NetVmeInterface::read16 (const uint32_t handle, const uint32_t offset) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
  return 0;
}

uint16_t NetVmeInterface::readS16 (const uint32_t handle, const uint32_t offset) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
  return 0;
}

uint32_t NetVmeInterface::readS32 (const uint32_t handle, const uint32_t offset) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
  return 0;
}

uint32_t NetVmeInterface::read32 (const uint32_t handle, const uint32_t offset) {
	// std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
    int rc = 0;
    unsigned int rxBytes = 0;
	RodTcpCmd cmd;
	uint32_t buf;
	if (!m_netConnected){
		std::cout << __FILE__ << ", " << __FUNCTION__ << " not connected" << std::endl;
		return 0;
	}
	//std::cout << __FILE__ << ", " << __FUNCTION__ << " sending READ command for offset " << std::hex << offset << std::endl;
    cmd.magic = htonl(MAGIC);
    // read
    cmd.command = htonl(READ_CMD);
    cmd.address = htonl(offset);
    cmd.parm = htonl(0);
    //std::cout << "Sending read" << std::endl;
    rc = send(m_rodSock, (void *)&cmd, sizeof(RodTcpCmd), 0);
    if (-1 == rc){
    	std::cout << "Error sending packet for read" << std::endl;
        perror(__FUNCTION__);
    	return 0;
    }
    //std::cout << "Waiting for data" << std::endl;
    while ((-1 != rc) && (rxBytes < sizeof(uint32_t))){
    	rc = recv(m_rodSock,(char*)&buf + rxBytes,sizeof(uint32_t) - rxBytes,0);
    	rxBytes += rc;
    }
    if (-1 == rc){
    	std::cout << "Error receiving packet" << std::endl;
        perror(__FUNCTION__);
    }
    //std::cout << "Read returned " << std::hex << buf << std::endl;
    return ntohl(buf);
}

void NetVmeInterface::write8 (const uint32_t handle, const uint32_t offset, const unsigned char value) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::writeS8 (const uint32_t handle, const uint32_t offset, const unsigned char value) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::write16(const uint32_t handle, const uint32_t offset, const uint16_t value) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::writeS16(const uint32_t handle, const uint32_t offset, const uint16_t value) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::write32(const uint32_t handle, const uint32_t offset, const uint32_t value) {
	// std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
    int rc;
	RodTcpCmd cmd;
	if (!m_netConnected){
		std::cout << __FILE__ << ", " << __FUNCTION__ << " not connected" << std::endl;
		return;
	}
	//std::cout << __FILE__ << ", " << __FUNCTION__ << " sending WRITE command for offset " << std::hex << offset << ", data " << value << std::endl;
    cmd.magic = htonl(MAGIC);
    // read
    cmd.command = htonl(WRITE_CMD);
    cmd.address = htonl(offset);
    cmd.parm = htonl(value);
    //std::cout << "Sending write" << std::endl;
    rc = send(m_rodSock, (void *)&cmd, sizeof(RodTcpCmd), 0);
    if (-1 == rc){
    	std::cout << "Error sending packet for write" << std::endl;
        perror(__FUNCTION__);
    }
}

void NetVmeInterface::writeS32(const uint32_t handle, const uint32_t offset, const uint32_t value) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::blockRead32 (VmePort &port, const uint32_t offset, uint32_t *buf, const int32_t len) {
	int i;
	// std::cout << __FILE__ << ", " << __FUNCTION__ << " need more work" << std::endl;
	for (i=0;i<len;i++){
		buf[i] = NetVmeInterface::read32(port.getHandle(), offset + 4*i);
	}
}

void NetVmeInterface::blockRead64 (VmePort &port, const uint32_t offset, uint32_t *buf, const int32_t len) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::blockWrite32(VmePort &port, const uint32_t offset, const uint32_t *buf, const int32_t len) {
	int i;
	// std::cout << __FILE__ << ", " << __FUNCTION__ << " needs more work" << std::endl;
	for (i=0;i<len;i++){
		NetVmeInterface::write32(port.getHandle(), offset + 4*i, buf[i]);
	}
}

void NetVmeInterface::blockWrite64(VmePort &port, const uint32_t offset, const uint32_t *buf, const int32_t len) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

uint32_t NetVmeInterface::registerPort(VmePort &port) {
  unsigned int ip;

  for (ip=0; ip<m_ports.size(); ip++) {
    if (m_ports[ip] == NULL) {
      m_ports[ip] = &port;
      return ip;
    }
  }
  m_ports.push_back(&port);
  return m_ports.size()-1;
}

void NetVmeInterface::deletePort(VmePort &port) {
  unsigned int ip;

  for (ip=0; ip<m_ports.size(); ip++) {
    if (m_ports[ip] == &port) {
      m_ports[ip] = NULL;
    }
  }
}

//void *NetVmeInterface::getPortMap(const uint32_t handle) {
//  return NULL;
//}

void NetVmeInterface::busErrorReport() {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::busErrorReport(const uint32_t handle) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

void NetVmeInterface::busErrorReport(VmePort &port) {
	std::cout << __FILE__ << ", " << __FUNCTION__ << " not implemented" << std::endl;
}

std::string NetVmeInterface::getErrorMessage(const int32_t errcod) {
  return "";
}

int32_t NetVmeInterface::getBusErrors() {
  return 0;
}
