tdaq_package(VmeInterface)

tdaq_add_library(VmeInterface
*.cxx
INCLUDE_DIRECTORIES ../RodDaq/CommonWithDsp .
LINK_LIBRARIES  tdaq::vme_cmem tdaq::io_rcc tdaq::rcc_error tdaq::vme_rcc pthread
)

tdaq_add_header_directory(VmeInterface)
