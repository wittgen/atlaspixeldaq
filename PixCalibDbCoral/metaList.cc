#include "CoralDbClient.hh"
#include "PixMetaException.hh"

#include <cstring>

#include <iomanip>

time_t parseTime(const std::string &time_str)
{
  if (time_str.empty() || time_str=="now" || time_str=="NOW") {
    return time(0);
  }

  if (time_str[0]=='-') {
    char *end_ptr=0;
    long value = strtol(time_str.c_str(),&end_ptr,10);
    time_t now=time(0);
    switch( *end_ptr) {
    case 'w': {
      return now + value*3600*24*7;
    }
    case 'd': {
      return now + value*3600*24;
    }
     case 'h': {
      return now + value*3600;
    }
    case 's':
    default: {
      return now + value;
    }
    }
  }
  else {
    const char *start_time = time_str.c_str();
    bool time_is_given_in_utc = false;
    if (time_str.compare(0,3, "utc")==0 || time_str.compare(0,3, "UTC")==0) {
      time_is_given_in_utc=true;
      start_time+=3;
      while (isspace(*start_time)) start_time++;
    }

    struct ::tm time_struct;
    // supported formats
    // Fri Mar 14 20:47:52 2008
    // Mar 14 20:47:52 2008
    // 2008-04-07 06:47:16
    std::vector<std::string> time_formats;
    //    time_formats.push_back("%Ea %Eb %d %H:%M:$S %Y");
    //    time_formats.push_back("%Eb %d %H:%M:$S %Y");
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    time_formats.push_back("%a %b %d %H:%M:%S %Y");
    //    time_formats.push_back("%Eb %d %H:%M:%S %Y");
    time_formats.push_back("%b %d %H:%M:%S %Y");
    //    time_formats.push_back("%Y-%m-%d %H:%M:%S");
    time_formats.push_back("%Y-%m-%d %H:%M:%S");
    time_formats.push_back("%Y-%m-%d %H:%M");
    time_formats.push_back("%Y-%m-%d");
    for (std::vector<std::string>::const_iterator format_iter = time_formats.begin();
	 format_iter != time_formats.end();
	 format_iter++) {
      char *end_ptr = strptime(start_time, format_iter->c_str(),&time_struct);
      std::cout << *format_iter << static_cast<void *>(end_ptr) << " " << static_cast<unsigned long>(end_ptr - start_time) << std::endl;
      if (end_ptr) {
        std::cout << " last_char = \"" << *end_ptr << "\" " << static_cast<unsigned int>(*end_ptr) << std::endl;
	while (isspace(*end_ptr)) end_ptr++;
	if (*end_ptr=='\0' || ((strncmp(end_ptr,"UTC",3)==0 || strncmp(end_ptr,"utc",3)==0) && *(end_ptr+3)=='\0')) {
	  if (*end_ptr) {
	    time_is_given_in_utc=true;
	  }
	  time_t utc_time = mktime(&time_struct);
	  std::cout << " input = " << time_str << " -> "<< utc_time << std::endl;
	  if (time_is_given_in_utc) {
	    struct ::tm gm_time_struct;
	    gmtime_r( &utc_time , &gm_time_struct);
	    gm_time_struct.tm_isdst=0;
	    utc_time = mktime(&gm_time_struct);
	    std::cout << " utc = " << asctime(&gm_time_struct) << " -> "<< utc_time << std::endl;
	  }
	  return utc_time;
	}
      }
    }
  }
  std::stringstream message;
  message << "Unsupported time format : " << time_str << ".";
  throw std::runtime_error(message.str());

}

std::map<std::string, CAN::ScanLocation> s_locationMap;
std::map<std::string, CAN::EQuality> s_qualityMap;
std::map<std::string, CAN::GlobalStatus> s_statusMap;


CAN::ScanLocation parseLocation(const std::string &location_string)
{
  if (s_locationMap.empty()) {
    s_locationMap.insert(std::make_pair(std::string("SR1"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("SR1/ToothPix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("SR1/TOOTHPIX"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("sr1/toothpix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("sr1"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("ToothPix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("toothpix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("TOOTHPIX"),CAN::kSR1));

    s_locationMap.insert(std::make_pair(std::string("PIT"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("pit"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("atlas"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("ATLAS"),CAN::kPIT));

    s_locationMap.insert(std::make_pair(std::string("PixelLab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("pixellab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("pixel_lab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("PIXEL_LAB"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("lab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("LAB"),CAN::kPixelLab));
  }
  std::map<std::string,CAN::ScanLocation>::const_iterator location_iter = s_locationMap.find(location_string);
  if (location_iter == s_locationMap.end()) {
    return CAN::kNowhere;
  }
  else {
    return location_iter->second;
  }
}

CAN::EQuality parseQuality(const std::string &quality_string)
{
  if (s_qualityMap.empty()) {
    s_qualityMap.insert(std::make_pair(std::string("normal"),CAN::kNormal));
    s_qualityMap.insert(std::make_pair(std::string("Normal"),CAN::kNormal));
    s_qualityMap.insert(std::make_pair(std::string("NORMAL"),CAN::kNormal));
    s_qualityMap.insert(std::make_pair(std::string("n"),CAN::kNormal));
    s_qualityMap.insert(std::make_pair(std::string("N"),CAN::kNormal));
    s_qualityMap.insert(std::make_pair(std::string("0"),CAN::kNormal));

    s_qualityMap.insert(std::make_pair(std::string("trash"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("Trash"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("TRASH"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("t"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("T"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("-"),CAN::kTrash));

    s_qualityMap.insert(std::make_pair(std::string("debug"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("DEBUG"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("dbg"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("d"),CAN::kTrash));
    s_qualityMap.insert(std::make_pair(std::string("D"),CAN::kTrash));
  }
  std::map<std::string,CAN::EQuality>::const_iterator quality_iter = s_qualityMap.find(quality_string);
  if (quality_iter == s_qualityMap.end()) {
    return CAN::kUndefined;
  }
  else {
    return quality_iter->second;
  }
}

CAN::GlobalStatus parseStatus(const std::string &status_string)
{
  if (s_statusMap.empty()) {
    s_statusMap.insert(std::make_pair(std::string("SUCCESS"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("Success"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("success"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("true"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("T"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("t"),CAN::kSuccess));
    s_statusMap.insert(std::make_pair(std::string("1"),CAN::kSuccess));

    s_statusMap.insert(std::make_pair(std::string("FAILURE"),CAN::kFailure));
    s_statusMap.insert(std::make_pair(std::string("Failure"),CAN::kFailure));
    s_statusMap.insert(std::make_pair(std::string("failure"),CAN::kFailure));
    s_statusMap.insert(std::make_pair(std::string("F"),CAN::kFailure));
    s_statusMap.insert(std::make_pair(std::string("f"),CAN::kFailure));
    s_statusMap.insert(std::make_pair(std::string("0"),CAN::kFailure));

    s_statusMap.insert(std::make_pair(std::string("ABORTED"),CAN::kAborted));
    s_statusMap.insert(std::make_pair(std::string("Aborted"),CAN::kAborted));
    s_statusMap.insert(std::make_pair(std::string("aborted"),CAN::kAborted));
    s_statusMap.insert(std::make_pair(std::string("A"),CAN::kAborted));
    s_statusMap.insert(std::make_pair(std::string("a"),CAN::kAborted));
    s_statusMap.insert(std::make_pair(std::string("-1"),CAN::kAborted));

    s_statusMap.insert(std::make_pair(std::string("Unknown"),CAN::kUnknown));
    s_statusMap.insert(std::make_pair(std::string("unknown"),CAN::kUnknown));
    s_statusMap.insert(std::make_pair(std::string("?"),CAN::kUnknown));

  }
  std::map<std::string,CAN::GlobalStatus>::const_iterator status_iter = s_statusMap.find(status_string);
  if (status_iter == s_statusMap.end()) {
    return CAN::kNotSet;
  }
  else {
    return status_iter->second;
  }
}

std::pair< unsigned int , std::pair<unsigned int, unsigned int> >
 analysePixDisableString(const std::string &pix_disable_string) {
  //std::pair< unsigned int , std::pair<unsigned int, unsigned int> > ret;
  unsigned int n_rods=0;
  unsigned int n_modules=0;
  unsigned int max_modules_per_rod=0;
  unsigned int modules_per_rod=0;
  std::string::size_type start_pos=0;
  for(;start_pos+1<pix_disable_string.size() ;) {
    std::string::size_type pos =  pix_disable_string.find(":",start_pos+1);
    if (pos != std::string::npos) {
      if (pos+1<pix_disable_string.size() && pix_disable_string[pos+1]==':') {
	n_rods++;
	if (modules_per_rod>max_modules_per_rod) {
	  max_modules_per_rod=modules_per_rod;
	}
	modules_per_rod=0;
	pos++;
      }
      else {
	modules_per_rod++;
	n_modules++;
      }
      start_pos=pos;
    }
  }

  if (modules_per_rod>max_modules_per_rod) {
    max_modules_per_rod=modules_per_rod;
  }
  //  std::cout << "INFO [analysePixDisableString] rods = " << n_rods << " modules = " << n_modules << "  max. modules / ROD " << max_modules_per_rod << std::endl;
  return std::make_pair(n_rods, std::make_pair(n_modules, max_modules_per_rod));
}

std::string timeString(const ::time_t &time)
{
    ::time_t tmp_time=time;
    std::string time_string(ctime(&tmp_time));
    std::string::size_type pos = time_string.find("\n");
    if (pos==std::string::npos) {
        pos = time_string.find("\r");
    }
    if (pos != std::string::npos) {
       time_string.erase(pos, time_string.size()-pos);
    }
    return time_string;
}

std::vector<std::string> s_rodStatus;

std::string rodStatus( CAN::GlobalStatus status ) {
  if (s_rodStatus.empty()) {
    s_rodStatus.resize(CAN::kUnknown+1);
    s_rodStatus[CAN::kNotSet]="(Not-set)";
    s_rodStatus[CAN::kSuccess]="Success";
    s_rodStatus[CAN::kFailure]="Failure";
    s_rodStatus[CAN::kAborted]="Aborted";
    s_rodStatus[CAN::kUnknown]="Unknwon";
  }
  if (status>CAN::kUnknown) {
    return "(unknown)";
  }
  else {
    assert( status < s_rodStatus.size()); //paranoia
    return s_rodStatus[status];
  }
}


void usage() {
  std::cout <<    "usage:" << std::endl
	    << std::endl
	    << "-l or --location [SR1,PIT,PixelLab] : limit to list to scans which were taken in " << std::endl
	    << "                                      SR1, the PIT or the PixelLab" << std::endl
	    << std::endl
	    << "-b or --begin [time] :                limit the search to the given time interval." << std::endl
	    << "-e or --end [time]                    time could be : -1w, -1d, -1h for one week" << std::endl
	    << "                                      one day or one hour ago; \"2008-04-15 08:30:00\""<< std::endl
            << "                                      (you have to put the qutes)" << std::endl
	    << std::endl
	    << "-n [name]                             limit the search to scans of the given name." << std::endl
	    << "                                      The scan name is either the name of the CT" << std::endl
	    << "                                      config file or the selected preset name" << std::endl
	    << std::endl
	    << "-t [cfg-tag]                          limit the search to scans which used the scan" << std::endl
	    << "                                      configurations of the given config tag " << std::endl
	    << "                                      (has nothing to do with the general (module,ROD,..." << std::endl
	    << "                                      config tag which is specified together with the" << std::endl
	    << "                                      connectivity tag." << std::endl
	    << "                                      " << std::endl
	    << "-c or --comment [comment]             limit the search to scans which contain the given" << std::endl
	    << "                                      expression in the comment field." << std::endl
	    << std::endl
	    << "-q or --quality [normal, debug,thrash] limit the search to scans which are tagged with the" << std::endl
	    << "                                       given quality specifier." << std::endl
	    << std::endl
	    << "-v or --verbose                       also show the comment and the file names of the scans." << std::endl
	    << std::endl;
}

int main(int argc, char **argv)
{
  bool analysis=false;
  std::vector<CAN::SerialNumber_t> analysis_serial_numbers;
  time_t begin_time=-1;
  time_t end_time=-1;
  CAN::ScanLocation location = CAN::kNowhere;
  std::string scan_name;
  std::string &analysis_name = scan_name; // same storage different variable names
  std::string cfg_tag;

  bool is_analysis_id = false;
  CAN::SerialNumber_t scan_id=0;
  std::string class_name;
  std::string class_cfg_tag;
  std::string post_name;
  std::string post_cfg_tag;
  std::string comment;
  std::string module_name;
  CAN::EQuality quality=CAN::kUndefined;
  CAN::GlobalStatus status=CAN::kNotSet;

  bool elog_report=false;
  bool elog_table=false;
  bool show_best_analysis=false;
  CAN::ETripleFlag has_analysis=CAN::kIndifferent;

  unsigned int verbose=0;
  bool check_file_names=false;

  std::map<CAN::ScanLocation, std::string> location_name_list;
  location_name_list.insert(std::make_pair(CAN::kNowhere,std::string("-")));
  location_name_list.insert(std::make_pair(CAN::kSR1,std::string("SR1/ToothPix")));
  location_name_list.insert(std::make_pair(CAN::kPIT,std::string("PIT")));
  location_name_list.insert(std::make_pair(CAN::kPixelLab,std::string("PixelLab")));

  std::map<CAN::EQuality,std::string>      quality_name_list;
  quality_name_list.insert(std::make_pair(CAN::kUndefined,std::string("")));
  quality_name_list.insert(std::make_pair(CAN::kNormal,std::string("normal")));
  quality_name_list.insert(std::make_pair(CAN::kDebug,std::string("debug")));
  quality_name_list.insert(std::make_pair(CAN::kTrash,std::string("trash")));

  std::map<CAN::GlobalStatus,std::string>      status_name_list;
  status_name_list.insert(std::make_pair(CAN::kNotSet,std::string("Not-set")));
  status_name_list.insert(std::make_pair(CAN::kSuccess,std::string("Success")));
  status_name_list.insert(std::make_pair(CAN::kFailure,std::string("Failure")));
  status_name_list.insert(std::make_pair(CAN::kAborted,std::string("Aborted")));
  status_name_list.insert(std::make_pair(CAN::kUnknown,std::string("Unknown")));

  bool error=false;
  try {

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if ((strcmp(argv[arg_i],"-a")==0 || strcmp(argv[arg_i],"--analysis")==0)) {
	analysis=true;
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  while ( arg_i+1<argc && isdigit( argv[arg_i+1][0] ) ) {
	    analysis_serial_numbers.push_back(atoi(argv[++arg_i]));
	  }
	}
      }
      else if ((strcmp(argv[arg_i],"-i")==0 || strcmp(argv[arg_i],"--scan-id")==0)  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	const char *ptr=argv[++arg_i];
	if (*ptr=='A' || *ptr=='a') {
	  is_analysis_id=true;
	  scan_id = atoi(++ptr);
	  analysis_serial_numbers.push_back(scan_id);
	}
	else if (*ptr=='S' || isdigit(*ptr) || *ptr=='s') {
	  is_analysis_id=false;
	  if (*ptr=='S' || *ptr=='s') ++ptr;
	  scan_id = atoi(ptr);
	}
      }
      else if ((strcmp(argv[arg_i],"-l")==0 || strcmp(argv[arg_i],"--location")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	location = parseLocation(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"-b")==0 || strcmp(argv[arg_i],"--begin")==0) && arg_i+1<argc ) {
	begin_time = parseTime(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"-e")==0 || strcmp(argv[arg_i],"--end")==0) && arg_i+1<argc ) {
	end_time = parseTime(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"-n")==0 || strcmp(argv[arg_i],"--name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	scan_name=argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-t")==0 || strcmp(argv[arg_i],"--tag")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	cfg_tag=argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"--class-name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	class_name=argv[++arg_i];
	analysis =true;
      }
      else if ((strcmp(argv[arg_i],"--class-tag")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	class_cfg_tag=argv[++arg_i];
	analysis =true;
      }
      else if ((strcmp(argv[arg_i],"--post-name")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	post_name=argv[++arg_i];
	analysis =true;
      }
      else if ((strcmp(argv[arg_i],"--post-tag")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	post_cfg_tag=argv[++arg_i];
	analysis =true;
      }
      else if ((strcmp(argv[arg_i],"-c")==0 || strcmp(argv[arg_i],"--comment")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	comment=argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quality")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	quality=parseQuality(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"--status")==0 || strcmp(argv[arg_i],"-s")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	status=parseStatus(argv[++arg_i]);
      }
      else if (strcmp(argv[arg_i],"--check-file-names")==0) {
	check_file_names=true;
      }
      else if (strcmp(argv[arg_i],"--has-analysis")==0) { 
	has_analysis=CAN::kYes;
      }
      else if (strcmp(argv[arg_i],"--has-no-analysis")==0) { 
	has_analysis=CAN::kNo;
	show_best_analysis=false;
      }
      else if (strcmp(argv[arg_i],"--show-best-analysis")==0 || strcmp(argv[arg_i],"--show-analysis")==0) {
	show_best_analysis=true;
      }
      else if ((strcmp(argv[arg_i],"--module-name")==0 || strcmp(argv[arg_i],"-m")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	module_name=argv[++arg_i];
      }
      else if (strcmp(argv[arg_i],"--report")==0) {
	elog_report=true;
	quality = CAN::kNormal;
	::time_t now=time(0);
	struct tm now_local;
	//::time_t start_time;
	localtime_r(&now, &now_local);

	if (::abs(now_local.tm_hour-7)<4) {
	  if (verbose>0) {
	    std::cout << "INFO [" << argv[0] << ":main] Set time range for night shift." << std::endl;
	  }
	  //night shift
	  now_local.tm_hour = 7;
	}
	else if (::abs(now_local.tm_hour-15)<4) {
	  if (verbose>0) {
	    std::cout << "INFO [" << argv[0] << ":main] Set time range for morning shift." << std::endl;
	  }
	  //morning shift
	  now_local.tm_hour = 15;
	}
	else {
	  if (verbose>0) {
	    std::cout << "INFO [" << argv[0] << ":main] Set time range for evening shift." << std::endl;
	  }
	  //evening shift
	  now_local.tm_hour = 23;
	}
	now_local.tm_min = 0;
	now_local.tm_sec = 0;

	end_time = mktime(&now_local);
	// does not take summer time switch into account
	begin_time = end_time - 8*3600;
	location = CAN::kPIT;
	elog_table = true;
	show_best_analysis=true;
      }
      else if ((strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
	if (arg_i+1<argc && argv[arg_i+1][0]!='-') {
	  verbose=atoi(argv[++arg_i]);
	}
	else {
	  verbose++;
	}
      }
      else {
	error=true;
	std::cout << "ERROR [" <<argv[0] << ":main] unhandled argument : " <<argv[arg_i] << std::endl;
      }
    }
    if (error) {
      std::cout << "USAGE  -- scans : "<< std::endl
		<< " metaList [-l location: PIT, SR1, PixelLab] [-b/--begin time: e.g. 2008-04-07 06:47:16, -1d, -1w, -1h, now]]" << std::endl
		<< "[-e/--end time] [-n/--name scan name: DIGITAL_TEST, CT_BocFastTuning_mod1212121] [-t/--tag conn-tag] " << std::endl
		<< "[-c/-comment part of the comment field] [-q/--quality normal, trash, debug, ] [-s/--status success, failure, aborted ]"  << std::endl
		<< "[-v/--verbose [level]] [--check-file-names]" 
		<< "[--has-analysis/--has-no-analysis] [--show-best-analysis] [-m/--module-name module connectivity name.]" << std::endl
		<< std::endl
		<< "USAGE  -- analyses : "<< std::endl
		<< " metaList -a [-b/--begin time: e.g. 2008-04-07 06:47:16, -1d, -1w, -1h, now]]" << std::endl
		<< "[-e/--end time] [-n/--name analysis name: THRESHOLDAnalysis, ...] [-t/--tag analysis config tag] " << std::endl
		<< "[--class-name classification name: TestClassification, ...] [--class-tag classification config tag] " << std::endl
		<< "[--post-name post-processing name: TestPostProcessing, ...] [--post-tag post-processing config tag] " << std::endl
		<< "[-c/-comment part of the comment field] [-q/--quality normal, trash, debug, ] [-s/--status success, failure, aborted ]" << std::endl
		<< "[-l scan-location: PIT, SR1, PixelLab] [-v/--verbose [level]]" << std::endl;

      return -1;
    }

    // limit the default period to one week
    if (begin_time==0) {
      begin_time=time(0)-3600*24*7;
    }
 
    if (begin_time>0 && begin_time != -1 && (end_time==0 || end_time==-1)) {
      end_time=time(0);
    }

    if (verbose>0) {
      if (verbose>4) {
	std::cout << begin_time << " " << end_time << std::endl;
      }
      std::cout << (analysis  ? "Analyses" : "Scans") << " performed from " << timeString(begin_time) << " to " << timeString(end_time) << std::endl;
    }
   
    CAN::PixCoralDbClient coral_client( (!elog_report && verbose>4), /*update : */ false );

    // Get metadata for scan anal_id
    try {
      if (analysis) {
	std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> analysis_list;

	if (!analysis_serial_numbers.empty()) {

	  for(std::vector<CAN::SerialNumber_t>::const_iterator analysis_iter = analysis_serial_numbers.begin();
	      analysis_iter != analysis_serial_numbers.end();
	      ++analysis_iter) {
	    analysis_list.insert(std::make_pair(*analysis_iter, coral_client.getMetadata<CAN::AnalysisInfo_t>(*analysis_iter)));
	  }

	}
	else {
	  analysis_list = coral_client.searchAnalysisMetadata(scan_id,
							      begin_time,
							      end_time,
							      analysis_name,
							      cfg_tag,
							      class_name,
							      class_cfg_tag,
							      post_name,
							      post_cfg_tag,
							      quality,
							      status,
							      comment,
							      location);
	}

	for (std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>::const_iterator analysis_iter = analysis_list.begin();
	     analysis_iter != analysis_list.end();
	     analysis_iter++) {

	  std::cout << "A" << std::setw(9) << std::setfill('0') << analysis_iter->first << std::setfill(' ')
		    << " of " << 'S' << std::setw(9) << std::setfill('0') << analysis_iter->second.scanSerialNumber()  << std::setfill(' ')
		    << std::setw(30) << analysis_iter->second.name(CAN::kAlg) << " "
		    << std::setw(20) << analysis_iter->second.tag(CAN::kAlg) << " "
		    << std::setw(8) << status_name_list[analysis_iter->second.status()] << " "
		    << std::setw(10)  << quality_name_list[analysis_iter->second.quality()] << " "
		    << std::setw(20)  << analysis_iter->second.analysisStartString() << " "
		    << std::setw(6)  << analysis_iter->second.analysisEnd() - analysis_iter->second.analysisStart() << "s" << std::endl;

	  if (verbose>0) {
            if (verbose>1) {
	    std::cout << std::setw(30) << analysis_iter->second.name(CAN::kAlg) << " "
		      << std::setw(20) << analysis_iter->second.tag(CAN::kAlg) << " " 
		      << " (" << timeString(analysis_iter->second.revision(CAN::kAlg)) << ")" <<  std::endl
		      << std::setw(30) << analysis_iter->second.name(CAN::kClassification) << " "
		      << std::setw(20) << analysis_iter->second.tag(CAN::kClassification) << " " 
		      << " (" << timeString(analysis_iter->second.revision(CAN::kClassification)) << ")" <<  std::endl 
                      << std::setw(30) << analysis_iter->second.name(CAN::kPostProcessing) << " "
		      << std::setw(20) << analysis_iter->second.tag(CAN::kPostProcessing) 
                      << " (" << timeString(analysis_iter->second.revision(CAN::kPostProcessing)) << ")"
                      <<  std::endl;
            }
	    std::string sources = analysis_iter->second.createSourceString();
	    if (!sources.empty()) {
	      std::cout << "\t" << sources << std::endl;
	    }

	    std::cout << "\t" << analysis_iter->second.comment() << std::endl
		      << "\t" << analysis_iter->second.file() << std::endl;
	    if (!analysis_iter->second.alternateFile().empty()) {
	      std::cout << "\t" << analysis_iter->second.alternateFile() << std::endl;
	    }
	    std::cout << std::endl;
	  }
	}

      }
      else {

	std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t> scan_list;
	if (scan_id>0) {
	  if (is_analysis_id) {
	    CAN::AnalysisInfo_t analysis_info = coral_client.getMetadata<CAN::AnalysisInfo_t>(scan_id, verbose>2);

	    PixLib::ScanInfo_t scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(analysis_info.scanSerialNumber(), verbose>2);
	    scan_list[analysis_info.scanSerialNumber()]=scan_info;
	  }
	  else {
	    PixLib::ScanInfo_t scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(scan_id, verbose>2);
	    scan_list[scan_id]=scan_info;
	  }
	}
	else {
	  scan_list = coral_client.searchScanMetadata(location,
						      begin_time,
						      end_time,
						      scan_name,
						      cfg_tag,
						      quality,
						      status,
						      comment,
						      has_analysis,
						      module_name,
						      verbose>4);
	}

	std::string table_sep=" ";
	std::string table_next_line;
	if (verbose>0) {
	  std::cout << " Normal scans performed from  " << timeString(begin_time) << " to " << timeString(end_time) << std::endl;
	}
        if (elog_table) {
	  table_sep=" | ";
	  table_next_line=" |-";
	  std::cout << "[b]Scans[/b]" << std::endl
		    << "[TABLE border=\"1\"]" << std::endl;
	  std::cout << std::setw(11) << "ID" << table_sep
		    << std::setw(30) << "Type" << table_sep
		    << std::setw(20) << "Tag" << table_sep
		    << std::setw(30) << "Config" << table_sep
		    << "Comment" << table_next_line << std::endl;
	}

	for (std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t>::const_iterator scan_iter = scan_list.begin();
	     scan_iter != scan_list.end();
	     scan_iter++) {
	  if (elog_report) {
	    std::cout << "S" << std::setw(9) << std::setfill('0') << scan_iter->first << std::setfill(' ') << " "<< table_sep
		      << std::setw(30) << scan_iter->second.scanType() << table_sep
		      << std::setw(20) << scan_iter->second.scanTypeTag() << table_sep
		      << std::setw(15) << scan_iter->second.cfgTag();
	    if (scan_iter->second.cfgTag() !=  scan_iter->second.modCfgTag()) {
	      std::cout << "," << std::setw(14) << scan_iter->second.modCfgTag();
	    }
	    else {
	      std::cout<< std::setw(30) << " ";
	    }
	    std::cout << table_sep << scan_iter->second.comment() << table_next_line << std::endl;
	  }
	  else {
	  std::cout << "S" << std::setw(9) << std::setfill('0') << scan_iter->first << std::setfill(' ')
		    << std::setw(30) << scan_iter->second.scanType() << " "
		    << std::setw(20) << scan_iter->second.scanTypeTag() << " "
		    << std::setw(20) << scan_iter->second.connTag() << " / " << std::setw(20) << scan_iter->second.cfgTag();
	  if (scan_iter->second.cfgTag() !=  scan_iter->second.modCfgTag()) {
	    std::cout << "," << scan_iter->second.modCfgTag();
	  }
	  std::cout << std::setw(8) << status_name_list[scan_iter->second.status()] << " "
		    << std::setw(13) << location_name_list[scan_iter->second.location()] << " "
		    << std::setw(10)  << quality_name_list[scan_iter->second.quality()] << " "
		    << std::setw(20)  << scan_iter->second.scanStartString() << " "
		    << std::setw(6)  << scan_iter->second.scanEnd() - scan_iter->second.scanStart() << "s" << std::endl;
	  bool file_exists[2] = { true, true };
	  if (check_file_names) {
	    for (unsigned int file_i=0; file_i<2; file_i++) {
	      std::string a_file_name=(file_i==0 ? scan_iter->second.file() : scan_iter->second.alternateFile());
	      std::string::size_type pos = a_file_name.find("::");
	      if (pos != std::string::npos) {
		a_file_name.erase(pos,a_file_name.size()-pos);
	      }
	      if (!a_file_name.empty()) {
		std::ifstream in(a_file_name.c_str());
		if (!in) {
		  file_exists[file_i]=false;
		}
	      }
	    }
	  }

	  if (verbose>0) {
            if (verbose>1) {
	    std::cout  << "\t" 
		       << scan_iter->second.idTag() << ":" <<  scan_iter->second.connTag() << ";"
		       << scan_iter->second.cfgTag() << " / " << scan_iter->second.cfgRev() 
                       << " (" <<timeString(scan_iter->second.cfgRev()) << ")" 
		       << ", module :  " << scan_iter->second.modCfgTag() << " / " << scan_iter->second.modCfgRev() 
                       << " (" <<timeString(scan_iter->second.modCfgRev()) << ")" 
		       << std::endl;
            }

            if (verbose>1) {
	    std::cout  << "\t" << scan_iter->second.scanType() << " / " << scan_iter->second.scanTypeTag() << " / " << scan_iter->second.scanTypeRev() 
                       << " (" <<timeString(scan_iter->second.scanTypeRev()) << ")" << std::endl;
            }

            if (!scan_iter->second.comment().empty() || verbose>4) {
              std::cout  << "\t" << scan_iter->second.comment() << std::endl;
            }
            if ( !scan_iter->second.file().empty() || verbose>4) {
	      std::cout  << "\t" << scan_iter->second.file() << (!file_exists[0] && !scan_iter->second.file().empty()  ? " ERROR: missing" : "") << std::endl;
            }

	    if (!scan_iter->second.alternateFile().empty()) {
	      std::cout << "\t" << scan_iter->second.alternateFile() << (!file_exists[0] && !scan_iter->second.file().empty()  ? "ERROR: missing" : "")  << std::endl;
	    }
	    if (verbose>2) {
	      std::cout << "\t start=" << scan_iter->second.scanStart() << " end=" << scan_iter->second.scanEnd() << std::endl;
	    }
 	    if (verbose>3) {

	      for(std::map<std::string, PixLib::RODstatus_t>::const_iterator rod_iter = scan_iter->second.statusMap().begin();
	  	  rod_iter != scan_iter->second.statusMap().end();
		  ++rod_iter) {

	         std::cout << "\t" << rod_iter->first << " : " << std::setw(8) << rodStatus( rod_iter->second.status() )
		  	   << " " << std::setw(18) << rod_iter->second.timeString(rod_iter->second.scanStart() ) 
			   << " - " << std::setw(18) << rod_iter->second.timeString(rod_iter->second.scanEnd() )
			   << " cfg  " << std::setw(6) << rod_iter->second.configEnd() - rod_iter->second.scanStart() << " s,"
			   << " scan " << std::setw(6) << rod_iter->second.scanEnd() - rod_iter->second.scanStart() << " s,"
			   << " writing  " << std::setw(6) << rod_iter->second.timeString(rod_iter->second.histogramWriteStart())
			   << " : " << std::setw(6) << rod_iter->second.histogramWriteEnd() - rod_iter->second.histogramWriteStart() << " s,"
			   << "  download " << std::setw(9) << rod_iter->second.histogramDownloadTime() << " mus,"
			   << "  loop0 " << std::setw(9) << rod_iter->second.loop0Time() << " mus"
			   << std::endl;
	      }

	    }
	    if (verbose>2 && !scan_iter->second.pixDisableString().empty()) {
	      if (verbose>4) {
		std::cout << "\t" << scan_iter->second.pixDisableString() << std::endl;
	      }
	      std::pair< unsigned int , std::pair<unsigned int, unsigned int> > 
		module_stat = analysePixDisableString(scan_iter->second.pixDisableString());
	      std::cout << "\t " << module_stat.first << " RODs, " << module_stat.second.first << " modules, "
			<< module_stat.second.second << " modules / ROD max."
			<< std::endl;
	    }
            std::cout << std::endl;
	  }
          }
	  if (show_best_analysis) {
	    std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> 

	      analysis_list = coral_client.searchAnalysisMetadata(scan_iter->first,
								  -1,
								  -1,
								  "",
								  "",
								  "",
								  "",
								  "",
								  "",
								  CAN::kUndefined,
								  CAN::kNotSet,
								  "",
								  CAN::kNowhere);
	    std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>::const_iterator best_analysis_iter = analysis_list.begin();

	    for (std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>::const_iterator analysis_iter = analysis_list.begin();
		 analysis_iter != analysis_list.end();
		 analysis_iter++) {
	      if (analysis_iter->second.quality()==CAN::kNormal || best_analysis_iter->second.quality()!=CAN::kNormal) {
		if ((   analysis_iter->second.status()<best_analysis_iter->second.status()
		     && analysis_iter->second.status()>=CAN::kSuccess) || best_analysis_iter->second.status()==CAN::kNotSet) {
		  best_analysis_iter = analysis_iter;
		}
	      }
	    }

	    if (best_analysis_iter != analysis_list.end()) {

	      std::cout << "+A" << std::setw(9) << std::setfill('0') << best_analysis_iter->first << std::setfill(' ') << table_sep
			<< std::setw(30) << best_analysis_iter->second.name(CAN::kAlg) << table_sep
			<< std::setw(20) << best_analysis_iter->second.tag(CAN::kAlg) << table_sep;
	      if (!elog_report) {
		std::cout << std::setw(8) << status_name_list[best_analysis_iter->second.status()] << table_sep;
	      }
	      else {
		std::cout << std::setw(30) << "" << table_sep;
	      }
	      std::cout << std::setw(20)  << best_analysis_iter->second.comment() << " " << table_next_line << std::endl;
	    }
	  }
	}

        if (elog_table) {
	  table_sep=" | ";
	  table_next_line=" |-";
	  std::cout << "[/TABLE]" << std::endl;
	}
      }

    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what() << std::endl;
    }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}



