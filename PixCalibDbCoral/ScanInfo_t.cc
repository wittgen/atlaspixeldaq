#include "ScanInfo_t.hh"
#include <iostream>
#include "PixMetaException.hh"

namespace PixLib {

  void ScanInfo_t::Print()
  {
    std::cout<<"== Scan metadata:";
    std::cout<<"\n ScanTypeName:     "<<m_scanTypeName
            <<"\n ScanTypeTag:      "<<m_scanTypeTag
            <<"\n ScanTypeRevision: "<<m_scanTypeRevision
            <<"\n Quality:          ";
    if (m_quality == CAN::kNormal) std::cout<<"Normal";
    else if (m_quality == CAN::kTrash) std::cout<<"Trash";
    else if (m_quality == CAN::kDebug) std::cout<<"Debug";
    else if (m_quality == CAN::kUndefined) std::cout<<"N/A";
    std::cout<<"\n Status:           ";
    if (m_status == CAN::kSuccess) std::cout<<"Success";
    else if (m_status == CAN::kFailure) std::cout<<"Failure";
    else if (m_status == CAN::kAborted) std::cout<<"Aborted";
    else if (m_status == CAN::kNotSet) std::cout<<"N/A";
    std::cout<<"\n Location:         ";
    if (m_location == CAN::kNowhere) std::cout<<"N/A";
    else if (m_location == CAN::kSR1) std::cout<<"SR1";
    else if (m_location == CAN::kPIT) std::cout<<"PIT";
    else if (m_location == CAN::kPixelLab) std::cout<<"PixelLab";
    std::cout<<"\n Comment:          "<<m_comment
            <<std::endl;
    std::cout<<" Tags:";
    std::cout<<"\n   Id:     "<<m_tagsId
	     <<"\n   Conn:   "<<m_tagsConn
	     <<"\n   Data:   "<<m_tagsData
	     <<"\n   Alias:  "<<m_tagsAlias
	     <<"\n   Cfg:    "<<m_tagsCfg
	     <<"\n   CfgRev: "<<m_tagsCfgRev;
    std::cout<<"\n Module tags:";
    std::cout<<"\n   Cfg:    "<<modCfgTag()
	     <<"\n   CfgRev: "<<modCfgRev();
    std::cout<<"\n ScanStart:        "<<m_timeScanStart<<" ("<<scanStartString()<<")"
	     <<"\n ScanEnd:          "<<m_timeScanEnd<<" ("<<scanEndString()<<")";
    std::cout<<"\n File location:    "<<m_file
	     <<"\n   alt. location:  "<<m_altFile
	     <<"\n PixDisable:       "<<m_pixDisable
	     <<std::endl;
    for (std::map<std::string, RODstatus_t>::const_iterator iter=m_rodStatus.begin(); iter != m_rodStatus.end(); ++iter) {
      std::cout<<" ROD "<<iter->first;
      (iter->second).Print();
    }
  }



  /** Get a specific ROD status.
   */
  const RODstatus_t ScanInfo_t::RODstatus(const std::string &rodname) const {
    std::map<std::string, RODstatus_t>::const_iterator iter = m_rodStatus.find(rodname);
    if (iter == m_rodStatus.end()) {
      throw CAN::PixMetaException("No ROD with name : "+ rodname);
    }
    return iter->second;
  }


  /** Convert a time_t to a string
   */
  const std::string RODstatus_t::timeString(time_t t) const {
    std::string time = "";  //initialize
    std::time_t std_time(t);
    char *ptr = ctime(&std_time);
    if (!ptr) return time;
    time = ptr;
    if (time.size()==0)  return time;
    return time.substr(0,time.size()-1);
  }


  /** Convert a PixScan formatted string to a time_t
   */
  const time_t RODstatus_t::stringToTime(std::string s) const {
    struct tm tm;
    if (strptime(s.c_str(), "%Y-%m-%d %H:%M:%S", &tm) == NULL) {
      // Error in format, return 0
      std::cout<<"ERROR RODstatus_t::stringToTime: invalid string format "<<s<<std::endl;
      return 0;
    }
    tm.tm_isdst = -1; //not set by strptime(). Tells mktime to ignore if daylight saving time is in effect 
    return mktime(&tm);
  }



  void RODstatus_t::Print() const {
    std::cout<<"      status: ";
    if (rodstatus == CAN::kSuccess) std::cout<<"Success"<<std::endl;
    else if (rodstatus == CAN::kFailure) std::cout<<"Failure"<<std::endl;
    else if (rodstatus == CAN::kAborted) std::cout<<"Aborted"<<std::endl;
    else if (rodstatus == CAN::kNotSet) std::cout<<"N/A"<<std::endl;
    std::cout<<"  scan start/end:        "<<timeString(scan_start)<<"/"<<timeString(scan_end)<<std::endl;
    std::cout<<"  histo write start/end: "<<timeString(histo_write_start)<<"/"<<timeString(histo_write_end)<<std::endl;
    std::cout<<"  config end:            "<<timeString(config_end)<<std::endl;
    std::cout<<"  histo download time:   "<<histo_download<<" mus"<<std::endl;
    std::cout<<"  loop 0 time:           "<<loop0<<" mus"<<std::endl;
  }

}
