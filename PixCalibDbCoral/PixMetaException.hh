#ifndef _CAN_PixMetaException_hh_
#define _CAN_PixMetaException_hh_

#include <exception>

namespace CAN {

class  PixMetaException : public std::exception {
public:
  PixMetaException(const std::string& msg) : m_msg(msg) {}
  ~PixMetaException() throw() {}
  const char* what() const throw() { return m_msg.c_str(); }
  
private:
  std::string m_msg;
};
}

#endif
