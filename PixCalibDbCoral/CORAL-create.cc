#include "CoralDbClient.hh"

int main(int argc, char **argv){

  try {

    // Start CORAL client
    printf("CORAL-create starting \n");
//     PixCoralClient coralClient(coralConnStr,app.context());
//     CAN::PixCoralDbClient coralClient(app.context(), true);
    CAN::PixCoralDbClient coralClient(true);

    try {
      //coralClient.dropTables();
      //coralClient.createAuxTables();
      coralClient.createTables();
    } catch (coral::TableAlreadyExistingException & ex) {
      std::cout<<"TableAlreadyExistingException: "<<ex.what()<<std::endl;
    }

    std::cout << "DONE" << std::endl;
  }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL


  catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}
                                                      



