//////////////////////////////////////////////////
///
/** @file CoralDbclient.cpp

C++ class for creating the scan/analysis metadata DB schema,
fill the DB, read it back

*/
///////////////////////////////////////////////////


#include "CoralDbClient.hh"
#include "PixMetaException.hh"
#include <sstream>

// same definitions are done by seal and omni


#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#ifdef CORAL_ONE
#  include "SealKernel/Context.h"
#  include "SealKernel/ComponentLoader.h"
#  include <SealServices/Application.h>
#else
#  include "CoralKernel/Context.h"
#endif
//#include "CoralBase/Date.h"

// CORAL API
#include "CoralBase/Attribute.h"
#include "CoralBase/AttributeList.h"
#include "RelationalAccess/IConnectionService.h"
#include "RelationalAccess/IConnectionServiceConfiguration.h"
#include "RelationalAccess/ISessionProxy.h"
#include "RelationalAccess/ISchema.h"
#include "RelationalAccess/ITransaction.h"

#include "RelationalAccess/ITable.h"
#include "RelationalAccess/ITableDescription.h"
#include "RelationalAccess/IColumn.h"
#include "RelationalAccess/ICursor.h"
#include "RelationalAccess/IQuery.h"
#include "RelationalAccess/TableDescription.h"
#include "RelationalAccess/ITableDataEditor.h"
#include "RelationalAccess/IBulkOperation.h"


namespace CAN {

#ifdef CORAL_ONE
 coral::ISessionProxy *createSession(const std::string& connection_string, bool verbose, bool update)
 { 
   seal::Application app;
   seal::Context *context = app.context();
  if (verbose) std::cout << "Creating Connection Service"<<std::endl;
  // get all IConnectionService's already loaded
  std::vector< seal::IHandle< coral::IConnectionService > > loadedServices;
  context->query( loadedServices );

  if ( loadedServices.empty() ) { // if no connection service available
    // Get the component loader
    seal::Handle<seal::ComponentLoader> loader = 
      context->component<seal::ComponentLoader>();
    // Load the COOLCORAL connection service
    loader->load( "CORAL/Services/ConnectionService" );
    context->query( loadedServices );
  }
    
  seal::IHandle<coral::IConnectionService> connectionService = loadedServices.front();
    
  /// connection to CORAL
  return connectionService->connect( connection_string, (update ? coral::Update : coral::ReadOnly) );
 }
#else

  coral::ISessionProxy *createSession(const std::string& connection_string, bool verbose, bool update) 
  { 

  // Create the RCDConnectionService
   if(verbose) std::cout << "Creating Connection Service"<<std::endl;
  // get all IConnectionService's already loaded
    
   // Instantiate connection service
   coral::Context* context = &coral::Context::instance();
    
   // Load CORAL connection service
   coral::IHandle<coral::IConnectionService> lookSvcH = context->query<coral::IConnectionService>();
   if (!lookSvcH.isValid()) {
       context->loadComponent( "CORAL/Services/ConnectionService" );
       lookSvcH = context->query<coral::IConnectionService>();
   }
   if (!lookSvcH.isValid()) {
       throw std::runtime_error( "Could not locate the connection service" );
   }
  /// connection to CORAL
   return lookSvcH->connect( connection_string, (update ? coral::Update : coral::ReadOnly) );
 }
#endif


//---------------------------------------------------------------------------
/** Constructor.
    Open the default database and seal context.
*/
PixCoralDbClient::PixCoralDbClient(bool verbose, bool update)
  : m_session(0), m_pixeltable("CALIB_META")
{ 
  m_verbose = verbose;
  char * ctmp = getenv("PIX_METADATA_DB");
  ctmp == NULL ? m_connString = "" : m_connString = ctmp;

  m_session = createSession(m_connString, verbose, update);
}


//---------------------------------------------------------------------------
/** Destructor.
    Close the connection
*/
PixCoralDbClient::~PixCoralDbClient() {
  if (m_session){
    if (m_verbose) std::cout << "\nCOOLCORAL Client: Disconnecting from database '"
			     << m_connString << "'" << std::endl; 
    delete m_session;
  }
}



  
//---------------------------------------------------------------------------
/** Get the list of tables on the database and print it on stdout.
 */
void PixCoralDbClient::printTables()
{
  if (m_verbose) std::cout << "\nCOOLCORAL Client: list of tables" << std::endl;

  transactionStartReadOnly();
  std::set<std::string> tables = m_session->nominalSchema().listTables();
  transactionCommit();
  for ( std::set<std::string>::iterator tName = tables.begin();
	tName != tables.end(); ++tName ){
    std::cout << "\t" << *tName << std::endl;
  }
}




//--------------------------------------------------------------------------
/** Get the description of a table and print it on stdout.
 */
void PixCoralDbClient::printTableDesc(std::string tableName)
{
  if (m_verbose) std::cout << "\nCOOLCORAL Client: " << tableName <<" Table description" << std::endl;

  transactionStartReadOnly();
  const coral::ITableDescription& tableDesc =
    m_session->nominalSchema().tableHandle(tableName).description();
  std::string Name = tableDesc.name();
  int NumCol = tableDesc.numberOfColumns();
  std::vector<std::string> colName; colName.resize(NumCol);
  std::vector<std::string> colType; colType.resize(NumCol);
  for(int i=0; i<NumCol; ++i){
    const coral::IColumn& colDesc = tableDesc.columnDescription(i);     
    colName[i] = colDesc.name();
    colType[i] = colDesc.type();
  }
 
  transactionCommit();
    
  std::cout << "\nName         : "<< Name << std::endl;
  printf(" Num of cols : %d",NumCol);
  std::cout << "\n Columns     :"<< std::endl;
  for(int i=0; i<NumCol; ++i) 
    std::cout <<"	"<< colName[i] <<"	("<< colType[i]<<")"<< std::endl;
}




//---------------------------------------------------------------------------
/** Get the content of a table and print its number of lines.
 */
void PixCoralDbClient::printTableContent(std::string tableName){
  if (m_verbose) std::cout << "\nCOOLCORAL Client: " << tableName 
			   <<" Table content"<< std::endl;
   
  transactionStartReadOnly();
    
  coral::IQuery* query = m_session->nominalSchema().tableHandle(tableName).newQuery();
  coral::ICursor& cursor = query->execute();
  int nRows = 0;
  while ( cursor.next() ) {
    cursor.currentRow().toOutputStream( std::cout ) << std::endl;
    ++nRows;
  }
  delete query;

  printf("Total  %d    records\n", nRows);
  transactionCommit();
}





//---------------------------------------------------------------------------
/** Create CORAL tables 
 */
void PixCoralDbClient::createTables(){
  if (m_verbose) std::cout << "\nCOOLCORAL Client: Create tables" << std::endl;
    
  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  createAuxTables();

  transactionStartUpdate();

  // Scan metadata table
  std::string tablename = m_pixeltable+"_SCAN";
  //     std::cout << " Deleting the old table: " << tablename << std::endl;
  //     m_session->nominalSchema().dropIfExistsTable(tablename);
  if (!m_session->nominalSchema().existsTable(tablename)) {
    if (m_verbose) std::cout << " Creating table: " << tablename << std::endl;
    coral::TableDescription pixel_columns;
    pixel_columns.setName( tablename );
    pixel_columns.insertColumn( "SCAN_ID",
            coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    pixel_columns.insertColumn( "NAME",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "TAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "REVISION",
	    coral::AttributeSpecification::typeNameForType<unsigned int>());
    pixel_columns.insertColumn( "QUALITY",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns.insertColumn( "STATUS",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns.insertColumn( "LOCATION",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns.insertColumn( "COMMENT",
	    coral::AttributeSpecification::typeNameForType<std::string>());
    pixel_columns.insertColumn( "IDTAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "CONNTAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "CONNDATATAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "CONNALIASTAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "CONNCFGTAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "CONNCFGREV",
	    coral::AttributeSpecification::typeNameForType<unsigned int>());
    pixel_columns.insertColumn( "MODCFGTAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "MODCFGREV",
	    coral::AttributeSpecification::typeNameForType<unsigned int>());
    pixel_columns.insertColumn( "START",
            coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "END",
	    coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "FILE",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 1000, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "ALTFILE",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 1000, /*fixed_size=*/ false);
    // A size > 4000 triggers the use of a CLOB object
    pixel_columns.insertColumn( "PIXDISABLE",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 4001, /*fixed_size=*/ false);

    pixel_columns.setPrimaryKey("SCAN_ID");
    pixel_columns.setNotNullConstraint ("SCAN_ID");
    pixel_columns.createIndex("NAME_IDX", "NAME");
    pixel_columns.createIndex("TAG_IDX", "TAG");
    pixel_columns.createIndex("QUALITY_IDX", "QUALITY");
    pixel_columns.createIndex("LOCATION_IDX", "LOCATION");
    pixel_columns.createIndex("START_IDX", "START");

    // Create the actual table
    m_session->nominalSchema().createTable( pixel_columns );
  } else
    if (m_verbose) std::cout << " "<< tablename <<" already exists " << std::endl;


  // ROD status table
  tablename = m_pixeltable+"_ROD_SCAN";
  //     std::cout << " Deleting the old table: " << tablename << std::endl;
  //     m_session->nominalSchema().dropIfExistsTable(tablename);
  if (!m_session->nominalSchema().existsTable(tablename)) {
    if (m_verbose) std::cout << " Creating table: " << tablename << std::endl;
    coral::TableDescription pixel_columns;
    pixel_columns.setName( tablename );
    pixel_columns.insertColumn( "SCAN_ID",
            coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    pixel_columns.insertColumn( "RODNAME",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 100, /*fixed_size=*/ false);
    pixel_columns.insertColumn( "STATUS",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns.insertColumn( "SCANSTART",
            coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "SCANEND",
	    coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "HISTOWSTART",
            coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "HISTOWEND",
	    coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "CONFIGEND",
	    coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns.insertColumn( "HISTODOWNLOAD",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns.insertColumn( "LOOP0",
	    coral::AttributeSpecification::typeNameForType<int>());

    //set the foreign key
    pixel_columns.createForeignKey("ROD_SCAN_FK","SCAN_ID",m_pixeltable+"_SCAN","SCAN_ID");
    pixel_columns.createIndex("RODNAME_IDX", "RODNAME");

    // Create the actual table
    m_session->nominalSchema().createTable( pixel_columns );
  } else
    if (m_verbose) std::cout << " "<< tablename <<" already exists " << std::endl;



  // Analysis metadata table
  tablename = m_pixeltable+"_ANAL";
  //     std::cout << " Deleting the old table: " << tablename << std::endl;
  //     m_session->nominalSchema().dropIfExistsTable(tablename);
  if (!m_session->nominalSchema().existsTable(tablename)) {
    if (m_verbose) std::cout << " Creating table: " << tablename << std::endl;
    coral::TableDescription pixel_columns2;
    pixel_columns2.setName( tablename );
    pixel_columns2.insertColumn( "ANAL_ID",
            coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    pixel_columns2.insertColumn( "SCAN_ID",
            coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    pixel_columns2.insertColumn( "SRC_ANAL_ID",
            coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    pixel_columns2.insertColumn( "ANAL_NAME",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "ANAL_TAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "ANAL_REVISION",
	    coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    pixel_columns2.insertColumn( "CLASS_NAME",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "CLASS_TAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "CLASS_REVISION",
	    coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    pixel_columns2.insertColumn( "POST_NAME",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "POST_TAG",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 200, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "POST_REVISION",
	    coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    pixel_columns2.insertColumn( "QUALITY",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns2.insertColumn( "STATUS",
	    coral::AttributeSpecification::typeNameForType<int>());
    pixel_columns2.insertColumn( "COMMENT",
	    coral::AttributeSpecification::typeNameForType<std::string>());
    pixel_columns2.insertColumn( "START",
            coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns2.insertColumn( "END",
	    coral::AttributeSpecification::typeNameForType<time_t>());
    pixel_columns2.insertColumn( "FILE",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 1000, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "ALTFILE",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 1000, /*fixed_size=*/ false);
    pixel_columns2.insertColumn( "SOURCES",
	    coral::AttributeSpecification::typeNameForType<std::string>(), /*size in BYTES = */ 1000, /*fixed_size=*/ false);

    pixel_columns2.setPrimaryKey("ANAL_ID");
    pixel_columns2.setNotNullConstraint ("ANAL_ID");
    //set the foreign key
    pixel_columns2.createForeignKey("SCAN_ID_FK","SCAN_ID",m_pixeltable+"_SCAN","SCAN_ID");

    // Create the actual table
    m_session->nominalSchema().createTable( pixel_columns2 );
  } else
    if (m_verbose) std::cout << " "<< tablename <<" already exists " << std::endl;



  // measuring elapsed time
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  transactionCommit();
}





//---------------------------------------------------------------------------  
/** Drop CORAL tables 
 */
void PixCoralDbClient::dropTables(){
  if (m_verbose) std::cout << "\nCOOLCORAL Client: Drop tables" << std::endl;

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);


  transactionStartUpdate();

  std::set<std::string> tables = m_session->nominalSchema().listTables();

  if (tables.size() > 0)
    if (m_verbose) std::cout << "Deleting the old tables: " << std::endl;
  for ( std::set<std::string>::iterator tName = tables.begin();
	tName != tables.end(); ++tName ){
    if ((*tName).find(m_pixeltable) != std::string::npos) {
      if (m_verbose) std::cout << "\t" << *tName << std::endl;
      m_session->nominalSchema().dropTable(*tName);
    }
  }

  // measuring elapsed time
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << "Total time was " << total_usecs << " usec" << std::endl;

  transactionCommit();
}





//---------------------------------------------------------------------------
/** Get metadata for a particular scan ID
    Throws a PixMetaException if scan ID is not found in the DB
*/
template <> 
PixLib::ScanInfo_t PixCoralDbClient::getMetadata<PixLib::ScanInfo_t>(CAN::SerialNumber_t scan_id, bool getRODstatus){

  std::string tablename = m_pixeltable+"_SCAN";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Get metadata for scan " << scan_id << std::endl;

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartReadOnly();

  PixLib::ScanInfo_t info;

  // avoid double quotes for sqlite, only use this for coral
  bool isSqlite = m_connString.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0 ||
    m_connString.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")==0;
  // Get all entries for scan_id
  coral::IQuery* query = m_session->nominalSchema().tableHandle(tablename).newQuery();
  // Get the list of columns
  const coral::ITableDescription& tableDesc =
    m_session->nominalSchema().tableHandle(tablename).description();
  int NumCol = tableDesc.numberOfColumns();
  for(int i=0; i<NumCol; ++i){
    const coral::IColumn& colDesc = tableDesc.columnDescription(i);
    // put column names that are oracle keywords into double quotes
    // avoid using oracle keywords as column names in the future
    std::string columnStr;
    if (!isSqlite && (colDesc.name() == "START" || colDesc.name() == "END" ||
		     colDesc.name() == "FILE" || colDesc.name() == "COMMENT")) columnStr = "\""+colDesc.name()+"\"";
    else columnStr = colDesc.name();
    query->addToOutputList(columnStr);
    //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
  }
  query->defineOutputType("REVISION",coral::AttributeSpecification::typeNameForType<unsigned int>());
  query->defineOutputType("CONNCFGREV",coral::AttributeSpecification::typeNameForType<unsigned int>());
  query->defineOutputType("MODCFGREV",coral::AttributeSpecification::typeNameForType<unsigned int>());
  query->defineOutputType(isSqlite?"START":"\"START\"",coral::AttributeSpecification::typeNameForType<time_t>());
  query->defineOutputType(isSqlite?"END":"\"END\"",coral::AttributeSpecification::typeNameForType<time_t>());

  std::string pixel_cond = tablename+".SCAN_ID = :scanid";
  coral::AttributeList pixel_condData;
  pixel_condData.extend<CAN::SerialNumber_t>( "scanid" );
  query->setCondition( pixel_cond, pixel_condData);
  pixel_condData["scanid"].data<CAN::SerialNumber_t>() = scan_id;
  coral::ICursor& cursor = query->execute();

  if ( cursor.next() ) {
    const coral::AttributeList &row0 = cursor.currentRow();
    info.setScanType(row0["NAME"].data<std::string>());
    info.setScanTypeTag(row0["TAG"].data<std::string>());
    info.setScanTypeRev(row0["REVISION"].data<unsigned int>());
    info.setQuality((CAN::EQuality)row0["QUALITY"].data<int>());
    info.setStatus((CAN::GlobalStatus)row0["STATUS"].data<int>());
    info.setLocation((CAN::ScanLocation)row0["LOCATION"].data<int>());
    info.setComment(row0[isSqlite?"COMMENT":"\"COMMENT\""].data<std::string>());
    info.setIdTag(row0["IDTAG"].data<std::string>());
    info.setConnTag(row0["CONNTAG"].data<std::string>());
    info.setDataTag(row0["CONNDATATAG"].data<std::string>());
    info.setAliasTag(row0["CONNALIASTAG"].data<std::string>());
    info.setCfgTag(row0["CONNCFGTAG"].data<std::string>());
    info.setCfgRev(row0["CONNCFGREV"].data<unsigned int>());
    info.setModCfgTag(row0["MODCFGTAG"].data<std::string>());
    info.setModCfgRev(row0["MODCFGREV"].data<unsigned int>());
    info.setScanStart(row0[isSqlite?"START":"\"START\""].data<time_t>());
    info.setScanEnd(row0[isSqlite?"END":"\"END\""].data<time_t>());
    info.setFile(row0[isSqlite?"FILE":"\"FILE\""].data<std::string>());
    info.setAlternateFile(row0["ALTFILE"].data<std::string>());
    info.setPixDisableString(row0["PIXDISABLE"].data<std::string>());

    // Get ROD status
    if (getRODstatus) {
      tablename = m_pixeltable+"_ROD_SCAN";
      coral::IQuery* query_2 = m_session->nominalSchema().tableHandle(tablename).newQuery();
      // Get the list of columns
      const coral::ITableDescription& tableDesc_2 = m_session->nominalSchema().tableHandle(tablename).description();
      NumCol = tableDesc_2.numberOfColumns();
      for(int i=0; i<NumCol; ++i){
	const coral::IColumn& colDesc = tableDesc_2.columnDescription(i);
        // put column names that are oracle keywords into double quotes
	// avoid using oracle keywords as column names in the future
	std::string columnStr;
	if (!isSqlite && (colDesc.name() == "START" || colDesc.name() == "END" ||
			  colDesc.name() == "FILE" || colDesc.name() == "COMMENT")) columnStr = "\""+colDesc.name()+"\"";
	else columnStr = colDesc.name();
	query_2->addToOutputList(columnStr);
      }
      query_2->defineOutputType("SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
      query_2->defineOutputType("SCANSTART",coral::AttributeSpecification::typeNameForType<time_t>());
      query_2->defineOutputType("SCANEND",coral::AttributeSpecification::typeNameForType<time_t>());
      query_2->defineOutputType("HISTOWSTART",coral::AttributeSpecification::typeNameForType<time_t>());
      query_2->defineOutputType("HISTOWEND",coral::AttributeSpecification::typeNameForType<time_t>());
      query_2->defineOutputType("CONFIGEND",coral::AttributeSpecification::typeNameForType<time_t>());
      std::string pixel_cond_2 = tablename+".SCAN_ID = :scanid";
      coral::AttributeList pixel_condData_2;
      pixel_condData_2.extend<CAN::SerialNumber_t>( "scanid" );
      query_2->setCondition( pixel_cond_2, pixel_condData_2);
      pixel_condData_2["scanid"].data<CAN::SerialNumber_t>() = scan_id;
      coral::ICursor& cursor_2 = query_2->execute();
      while ( cursor_2.next() ) {
	const coral::AttributeList &row_2 = cursor_2.currentRow();
	info.addRODStatus(row_2["RODNAME"].data<std::string>(),
			  (CAN::GlobalStatus)row_2["STATUS"].data<int>(),
			  row_2["SCANSTART"].data<time_t>(),
			  row_2["SCANEND"].data<time_t>(),
			  row_2["HISTOWSTART"].data<time_t>(),
			  row_2["HISTOWEND"].data<time_t>(),
			  row_2["CONFIGEND"].data<time_t>(),
			  row_2["HISTODOWNLOAD"].data<int>(),
			  row_2["LOOP0"].data<int>());
      }
      delete query_2;
    } // endif getRODstatus
  } else {
    //std::cout << "LINE: " << __LINE__ << std::endl;
    delete query;
    transactionCommit();
    std::stringstream message;
    message<<"CAN::PixCoralDbClient::getMetadata<PixLib::ScanInfo_t>: No metadata found for scan ID "<<scan_id;
    throw CAN::PixMetaException(message.str());
  }
  //std::cout << "LINE: " << __LINE__ << std::endl;
  delete query;
  //std::cout << "LINE: " << __LINE__ << std::endl;
  transactionCommit();
  //std::cout << "LINE: " << __LINE__ << std::endl;
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;
  //std::cout << "LINE: " << __LINE__ << std::endl;
  return info;
}





//---------------------------------------------------------------------------
/** Get scan ID for a particular output file
    Throws a PixMetaException if file is found several times in the DB
    Returns 0 if the file was not found
*/
CAN::SerialNumber_t PixCoralDbClient::scanSerialNumberForFile(std::string file){

  try {
    std::string tablename = m_pixeltable+"_SCAN";

    if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Get scan ID for output file " << file << std::endl;

    struct timeval start_time, end_time;
    int total_usecs;
    gettimeofday(&start_time, NULL);

    transactionStartReadOnly();

    CAN::SerialNumber_t id = 0;

    // Get all entries for file
    coral::IQuery* query = m_session->nominalSchema().tableHandle(tablename).newQuery();

    // avoid double quotes for sqlite, only use this for coral
    bool isSqlite = m_connString.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0 ||
      m_connString.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")==0;

    std::string pixel_cond = tablename+(isSqlite?".FILE = :File":".\"FILE\" = :File");
    coral::AttributeList pixel_condData;
    pixel_condData.extend<std::string>( "File" );
    query->setCondition( pixel_cond, pixel_condData);
    pixel_condData["File"].data<std::string>() = file;
    query->addToOutputList("SCAN_ID");
    query->defineOutputType("SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
    coral::ICursor& cursor = query->execute();

    if ( cursor.next() ) {
      id = cursor.currentRow()["SCAN_ID"].data<CAN::SerialNumber_t>();
    } else {
      if (m_verbose) std::cerr<<"ERROR [CAN::PixCoralDbClient::scanSerialNumberForFile]: No scan found with output file "<<file<<std::endl;
      //     throw CAN::PixMetaException("CAN::PixCoralDbClient::scanSerialNumberForFile: No scan found with output file "+file);
    }

    if ( cursor.next() ) {
      delete query;
      transactionCommit();
      throw CAN::PixMetaException("CAN::PixCoralDbClient::scanSerialNumberForFile: Found several scans with output file "+file);
    }

    delete query;

    transactionCommit();

    gettimeofday(&end_time, NULL);
    total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
      (end_time.tv_usec-start_time.tv_usec);
    if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;
    return id;
  }
  catch (const coral::Exception &err) {
    std::cout << "INFO [PixCoralDbClient::scanSerialNumberForFile] While searching for serial number for file " << file
	      << ", caught exception : " <<err.what() << "." << std::endl;
    return 0;
  }
}



const char *s_statusName[CAN::kAborted+1] = {"(unknwon)","success","failure","aborted"};
const char *s_qualityName[CAN::kDebug+1] =  {"(unknwon)","Normal", "Trash","Debug"};

template <class T1, class T2> void setScanInfo(PixLib::ScanInfo_t &scan_info,  void (PixLib::ScanInfo_t::* a_func)(T1), const coral::Attribute &a)
{
  if (!a.isNull()) {
    (scan_info.*a_func)( a.data<T2>() );
  }
}

//---------------------------------------------------------------------------
/** Get scan ID's and metadata matching query criteria
    @param location Where the scan was taken
    @param mintime  Scan taken after this date
    @param maxtime  Scan taken before this date
    @param name     Scan name
    @param tag      Scan tag
    @param quality  Scan quality
    @param comment  Comment field contains this string
    @param add_disable add the pix disable string to the scan info.

*/
std::map<CAN::SerialNumber_t,PixLib::ScanInfo_t> PixCoralDbClient::searchScanMetadata(CAN::ScanLocation location,
										      time_t mintime,
										      time_t maxtime,
										      std::string name,
										      std::string tag,
										      CAN::EQuality quality,
										      CAN::GlobalStatus status,
										      std::string comment,
										      ETripleFlag has_analysis,
										      std::string module_name,
										      bool add_disable){

  if (status>CAN::kAborted) {
    status =CAN::kNotSet;
  }
  if (quality>CAN::kDebug) {
    quality = CAN::kUndefined;
  }
  std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t> id_info;
  try {
    std::string tablename = m_pixeltable+"_SCAN";

    if (m_verbose) {
      std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Get scan ID's for the following criteria:"<< std::endl;
      if (location != CAN::kNowhere) {
	std::cout<<"  taken in: ";
	if (location == CAN::kSR1) std::cout<<"SR1";
	else if (location == CAN::kPIT) std::cout<<"PIT";
	else if (location == CAN::kPixelLab) std::cout<<"PixelLab";
	std::cout<<std::endl;
      }
      if (mintime>-1) std::cout<<"     after: "<<timeString(mintime)<<std::endl;
      if (maxtime>-1) std::cout<<"    before: "<<timeString(maxtime)<<std::endl;
      if (name != "") std::cout<<"      name: "<<name<<std::endl;
      if (tag != "") std::cout<<"       tag: "<<tag<<std::endl;
      if (status != CAN::kNotSet) {
	std::cout<<"       status: "<< s_statusName[status]<<std::endl;
      }
      if (quality != CAN::kUndefined) {
	std::cout<<"   quality: " << s_qualityName[quality] <<std::endl;
      }
      if (comment != "") std::cout<<"   comment contains: "<<comment<<std::endl;
      if (module_name != "") std::cout<<" pixdisable contains: :"<<module_name<< ":"<<std::endl;
    }
    struct timeval start_time, end_time;
    int total_usecs;
    gettimeofday(&start_time, NULL);

    transactionStartReadOnly();

    std::unique_ptr<coral::IQuery> master_query( m_session->nominalSchema().newQuery() );
    coral::IQueryDefinition *query = master_query.get();


    if (has_analysis == kNo ) {
      query = &(master_query->defineSubQuery("Q"));
      query->addToTableList(m_pixeltable+"_SCAN","S");
      query->addToOutputList( "S.SCAN_ID","SID" );

      master_query->addToTableList("Q");
      master_query->addToTableList(tablename, "M");
    }
    else {
      master_query->addToTableList(tablename, "S");
    }


    // avoid double quotes for sqlite, only use this for coral
    bool isSqlite = m_connString.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0 ||
      m_connString.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")==0;
    // Get the list of columns
    const coral::ITableDescription& tableDesc =
      m_session->nominalSchema().tableHandle(tablename).description();
    int NumCol = tableDesc.numberOfColumns();
    for(int i=0; i<NumCol; ++i){
      const coral::IColumn& colDesc = tableDesc.columnDescription(i);     
      if (add_disable || colDesc.name()!="PIXDISABLE") {
	// put column names that are oracle keywords into double quotes
	// avoid using oracle keywords as column names in the future
	std::string columnStr;
	if (!isSqlite && (colDesc.name() == "START" || colDesc.name() == "END" ||
			  colDesc.name() == "FILE" || colDesc.name() == "COMMENT")) columnStr = "\""+colDesc.name()+"\"";
	else columnStr = colDesc.name();
	master_query->addToOutputList(columnStr);
      }
    }
    master_query->defineOutputType("SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
    master_query->defineOutputType("REVISION",coral::AttributeSpecification::typeNameForType<unsigned int>());
    master_query->defineOutputType("CONNCFGREV",coral::AttributeSpecification::typeNameForType<unsigned int>());
    master_query->defineOutputType("MODCFGREV",coral::AttributeSpecification::typeNameForType<unsigned int>());
    master_query->defineOutputType(isSqlite?"START":"\"START\"",coral::AttributeSpecification::typeNameForType<time_t>());
    master_query->defineOutputType(isSqlite?"END":"\"END\"",coral::AttributeSpecification::typeNameForType<time_t>());

    std::string link = "";
    std::string pixel_cond;
    coral::AttributeList pixel_condData;

    if (has_analysis != kIndifferent) {
      if (has_analysis == kYes ) {
      // select entries which have an analysis
      coral::IQueryDefinition& scan_id_list_query = query->defineSubQuery( "Q" );
      scan_id_list_query.addToOutputList( "A.SCAN_ID" ,"SID");
      scan_id_list_query.addToTableList(m_pixeltable+"_ANAL","A");


      coral::AttributeList scan_id_condition_inputs;
      std::string scan_id_condition;
      std::string quality_name("QUALITY_MAX");
      std::string status_ok_name("OK");
      std::string status_failed_name("FAILED");
      scan_id_condition += "A.QUALITY<:";
      scan_id_condition += quality_name;
      scan_id_condition += " AND A.STATUS in (:";
      scan_id_condition += status_ok_name;
      scan_id_condition += ",:";
      scan_id_condition += status_failed_name;
      scan_id_condition += ")";

      scan_id_condition_inputs.extend<int>(quality_name);
      scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kTrash); // only consider analyses with the status unknown or normal but not trash and debug
      scan_id_condition_inputs.extend<int>(status_ok_name);
      scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kSuccess); // only consider analyses which were successful
      scan_id_condition_inputs.extend<int>(status_failed_name);
      scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kFailure); // ...  or which failed

      scan_id_list_query.setCondition(scan_id_condition, scan_id_condition_inputs);
      query->addToTableList( "Q" );

      pixel_cond += link;
      pixel_cond += "S.SCAN_ID";
      pixel_cond += "=";
      pixel_cond += "Q.SID";
      link = " AND ";
      }
    }

    if (location != CAN::kNowhere) {
      pixel_cond += link + "S.LOCATION = :location";
      link = " AND ";
      pixel_condData.extend<int>("location");
      pixel_condData["location"].data<int>() = location;
    }
    if (mintime>-1) {
      pixel_cond += link + (isSqlite?"S.START >= :mintime":"S.\"START\" >= :mintime");
      link = " AND ";
      pixel_condData.extend<time_t>("mintime");
      pixel_condData["mintime"].data<time_t>() = mintime;
    }
    if (maxtime>-1) {
      pixel_cond += link + (isSqlite?"S.START < :maxtime":"S.\"START\" < :maxtime");
      link = " AND ";
      pixel_condData.extend<time_t>("maxtime");
      pixel_condData["maxtime"].data<time_t>() = maxtime;
    }
    if (name != "") {
      pixel_cond += link + "S.NAME = :Name";
      link = " AND ";
      pixel_condData.extend<std::string>("Name");
      pixel_condData["Name"].data<std::string>() = name;
    }
    if (tag != "") {
      pixel_cond += link + "S.TAG = :Tag";
      link = " AND ";
      pixel_condData.extend<std::string>("Tag");
      pixel_condData["Tag"].data<std::string>() = tag;
    }
    if (quality != CAN::kUndefined) {
      pixel_cond += link + "S.QUALITY = :Quality";
      link = " AND ";
      pixel_condData.extend<int>("Quality");
      pixel_condData["Quality"].data<int>() = quality;
    }
    if (status != CAN::kNotSet) {
      pixel_cond += link + "S.STATUS = :Status";
      link = " AND ";
      pixel_condData.extend<int>("Status");
      pixel_condData["Status"].data<int>() = status;
    }
    if (comment != "") {
      pixel_cond += link + (isSqlite?"S.COMMENT LIKE :Comment":"S.\"COMMENT\" LIKE :Comment");
      link = " AND ";
      pixel_condData.extend<std::string>("Comment");
      pixel_condData["Comment"].data<std::string>() = "%"+comment+"%";
    }

    if (module_name != "") {
      pixel_cond += link + " S.PIXDISABLE LIKE :Pixdisable";
      link = " AND ";
      pixel_condData.extend<std::string>("Pixdisable");
      std::string search_string = "%:"+module_name;
      if (module_name.find("_M")!= std::string::npos) {
	search_string += ":";
      }
      search_string+="%";
      pixel_condData["Pixdisable"].data<std::string>()=search_string;
    }

    if (has_analysis != kIndifferent) {
      if (has_analysis == kNo ) {
	coral::IQueryDefinition& scan_id_list_query = query->applySetOperation( coral::IQueryDefinition::Minus );

	scan_id_list_query.addToTableList(m_pixeltable+"_ANAL","A");
	scan_id_list_query.addToOutputList( "A.SCAN_ID","SID");
	//	scan_id_list_query.defineOutputType("A.SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
	
	coral::AttributeList scan_id_condition_inputs;
	std::string scan_id_condition;
	std::string quality_name("QUALITY_MAX");
	std::string status_ok_name("OK");
	std::string status_failed_name("FAILED");
	scan_id_condition += "A.QUALITY<:";
	scan_id_condition += quality_name;
	scan_id_condition += " AND A.STATUS in (:";
	scan_id_condition += status_ok_name;
	scan_id_condition += ",:";
	scan_id_condition += status_failed_name;
	scan_id_condition += ")";

	scan_id_condition_inputs.extend<int>(quality_name);
	scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kTrash); // only consider analyses with the status unknown or normal but not trash and debug
	scan_id_condition_inputs.extend<int>(status_ok_name);
	scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kSuccess); // only consider analyses which were successful
	scan_id_condition_inputs.extend<int>(status_failed_name);
	scan_id_condition_inputs[scan_id_condition_inputs.size()-1].setValue<int>(kFailure); // ...  or which failed

	scan_id_list_query.setCondition(scan_id_condition, scan_id_condition_inputs);
	
	std::string join_cond;
	coral::AttributeList join_cond_inputs;
	join_cond = "M.SCAN_ID = Q.SID";

	master_query->setCondition(join_cond, join_cond_inputs);

      }
    }

    query->setCondition( pixel_cond, pixel_condData);
    coral::ICursor& cursor = master_query->execute();

    while ( cursor.next() ) {
      const coral::AttributeList &row0 = cursor.currentRow();
      PixLib::ScanInfo_t info;
      info.setScanType(row0["NAME"].data<std::string>());
      info.setScanTypeTag(row0["TAG"].data<std::string>());
      info.setScanTypeRev(row0["REVISION"].data<unsigned int>());
      info.setQuality((CAN::EQuality)row0["QUALITY"].data<int>());
      info.setStatus((CAN::GlobalStatus)row0["STATUS"].data<int>());
      info.setLocation((CAN::ScanLocation)row0["LOCATION"].data<int>());
      // info.setComment(row0["COMMENT"].data<std::string>());
      setScanInfo<const std::string &,std::string>(info, &PixLib::ScanInfo_t::setComment, row0[isSqlite?"COMMENT":"\"COMMENT\""]);
      info.setIdTag(row0["IDTAG"].data<std::string>());
      info.setConnTag(row0["CONNTAG"].data<std::string>());
      info.setDataTag(row0["CONNDATATAG"].data<std::string>());
      info.setAliasTag(row0["CONNALIASTAG"].data<std::string>());
      info.setCfgTag(row0["CONNCFGTAG"].data<std::string>());
      info.setCfgRev(row0["CONNCFGREV"].data<unsigned int>());
      info.setModCfgTag(row0["MODCFGTAG"].data<std::string>());
      info.setModCfgRev(row0["MODCFGREV"].data<unsigned int>());
      info.setScanStart(row0[isSqlite?"START":"\"START\""].data<time_t>());
      info.setScanEnd(row0[isSqlite?"END":"\"END\""].data<time_t>());
      info.setFile(row0[isSqlite?"FILE":"\"FILE\""].data<std::string>());
      info.setAlternateFile(row0["ALTFILE"].data<std::string>());
      // setScanInfo<const std::string &,std::string>(info, &PixLib::ScanInfo_t::setAlternateFile, row0["ALTFILE"]);
      if(add_disable) {
        setScanInfo<const std::string &,std::string>(info,&PixLib::ScanInfo_t::setPixDisableString,row0["PIXDISABLE"]);
        //	info.setPixDisableString(row0["PIXDISABLE"].data<std::string>());
      }
      id_info.insert(std::make_pair(cursor.currentRow()["SCAN_ID"].data<CAN::SerialNumber_t>(), info));
    }

    transactionCommit();

    gettimeofday(&end_time, NULL);
    total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
      (end_time.tv_usec-start_time.tv_usec);
    if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;
    return id_info;
  }
  catch (const coral::Exception &err) {
    std::cout << "INFO [PixCoralDbClient::searchScanMetadata] While searching for scan metadata with: ";
    if (location != CAN::kNowhere) std::cout<<" taken in "<<location;
    if (mintime>-1) std::cout<<" after "<<timeString(mintime);
    if (maxtime>-1) std::cout<<" before "<<timeString(maxtime);
    if (name != "") std::cout<<" name "<<name;
    if (tag != "") std::cout<<" tag "<<tag;
    if (quality != CAN::kUndefined) std::cout<<" quality "<<quality;
    if (status != CAN::kNotSet) {
      std::cout<<"       status: "<< s_statusName[status]<<std::endl;
    }
    if (comment != "") std::cout<<" comment: "<<comment;
    std::cout << ", caught exception : " <<err.what() << "." << std::endl;
    return id_info;
  }
}

/** Get scan ID's and metadata matching query criteria
    @param location Where the scan was taken
    @param mintime  Scan taken after this date
    @param maxtime  Scan taken before this date
    @param name     Scan name
    @param tag      Scan tag
    @param quality  Scan quality
    @param comment  Comment field contains this string
*/
  std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t>
  PixCoralDbClient::searchAnalysisMetadata(CAN::SerialNumber_t scan_serial_number,
					   time_t mintime,
					   time_t maxtime,
					   std::string analysis_name,
					   std::string analysis_tag,
					   std::string classification_name,
					   std::string classification_tag,
					   std::string post_processing_name,
					   std::string post_processing_tag,
					   CAN::EQuality quality,
					   CAN::GlobalStatus status,
					   std::string comment,
					   CAN::ScanLocation location)
{

  if (status>CAN::kAborted) {
    status =CAN::kNotSet;
  }
  if (quality>CAN::kDebug) {
    quality = CAN::kUndefined;
  }

  std::vector<std::string>   name;
  std::vector<std::string>   att_name;
  std::vector<std::string>   value;

  std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> id_info;
  try {
    std::string tablename = m_pixeltable+"_ANAL";
    std::string scan_tablename = m_pixeltable+"_SCAN";
    std::string analysis_table_header;
    std::string scan_table_header;


    name.push_back("ANAL_NAME");
    att_name.push_back("anal_name");
    value.push_back(analysis_name);
    name.push_back("ANAL_TAG");
    att_name.push_back("anal_tag");
    value.push_back(analysis_tag);

    name.push_back("CLASS_NAME");
    att_name.push_back("class_name");
    value.push_back(classification_name);
    name.push_back("CLASS_TAG");
    att_name.push_back("class_tag");
    value.push_back(classification_tag);

    name.push_back("POST_NAME");
    att_name.push_back("post_name");
    value.push_back(post_processing_name);
    name.push_back("POST_TAG");
    att_name.push_back("post_tag");
    value.push_back(post_processing_tag);

    assert(att_name.size() == name.size());
    assert(value.size() == name.size());

    if (m_verbose) {
      std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Get analysis ID's for the following criteria: ";
      if (scan_serial_number>0) {
	std::cout<< "scan serial number: "<<scan_serial_number;
      }
      if (mintime>-1) std::cout<<"             after: "<<timeString(mintime);
      if (maxtime>-1) std::cout<<"            before: "<<timeString(maxtime);
      for (unsigned int name_i=0; name_i<name.size(); name_i++) {
	if (!value[name_i].empty()) std::cout<<"              " << name[name_i]<< ": "<<value[name_i];
      }
      if (status != CAN::kNotSet) {
	std::cout<<"            status: "<< s_statusName[status];
      }
      if (quality != CAN::kUndefined) {
	std::cout<<"   quality: " << s_qualityName[quality];
      }
      if (comment != "") std::cout<<"   comment contains: "<<comment;
      std::cout << '\n';
    }
    struct timeval start_time, end_time;
    int total_usecs;
    gettimeofday(&start_time, NULL);

    transactionStartReadOnly();

    coral::IQuery* query;
    if (location != CAN::kNowhere) {
      query = m_session->nominalSchema().newQuery();
      query->addToTableList(tablename);
      query->addToTableList(scan_tablename);
      analysis_table_header = tablename;
      analysis_table_header+='.';
      scan_table_header = scan_tablename;
      scan_table_header+='.';
    }
    else {
      query = m_session->nominalSchema().tableHandle(tablename).newQuery();
    }
    // avoid double quotes for sqlite, only use this for coral
    bool isSqlite = m_connString.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0 ||
      m_connString.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")==0;
    // Get the list of columns
    const coral::ITableDescription& tableDesc =
      m_session->nominalSchema().tableHandle(tablename).description();
    int NumCol = tableDesc.numberOfColumns();
    for(int i=0; i<NumCol; ++i){
      const coral::IColumn& colDesc = tableDesc.columnDescription(i);
      // put column names that are oracle keywords into double quotes
      // avoid using oracle keywords as column names in the future
      std::string columnStr;
      if (!isSqlite && (colDesc.name() == "START" || colDesc.name() == "END" ||
			colDesc.name() == "FILE" || colDesc.name() == "COMMENT")) columnStr = "\""+colDesc.name()+"\"";
      else columnStr = colDesc.name();
      query->addToOutputList(analysis_table_header+""+columnStr);
      //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
    }
    query->defineOutputType(analysis_table_header+"ANAL_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
    query->defineOutputType(analysis_table_header+"ANAL_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    query->defineOutputType(analysis_table_header+"CLASS_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    query->defineOutputType(analysis_table_header+"POST_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
    query->defineOutputType(analysis_table_header+"SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    query->defineOutputType(analysis_table_header+"SRC_ANAL_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    query->defineOutputType(analysis_table_header+(isSqlite?"START":"\"START\""),coral::AttributeSpecification::typeNameForType<time_t>());
    query->defineOutputType(analysis_table_header+(isSqlite?"END":"\"END\""),coral::AttributeSpecification::typeNameForType<time_t>());

    std::string link = "";
    std::string pixel_cond;
    coral::AttributeList pixel_condData;
    if (scan_serial_number>0) {
      pixel_cond += link + analysis_table_header+"SCAN_ID = :scan_id";
      link = " AND ";
      pixel_condData.extend<CAN::SerialNumber_t>("scan_id");
      pixel_condData["scan_id"].data<CAN::SerialNumber_t>() = scan_serial_number;
    }
    if (mintime>-1) {
      pixel_cond += link + analysis_table_header+(isSqlite?"START >= :mintime":"\"START\" >= :mintime");
      link = " AND ";
      pixel_condData.extend<time_t>("mintime");
      pixel_condData["mintime"].data<time_t>() = mintime;
    }
    if (maxtime>-1) {
      pixel_cond += link + analysis_table_header+(isSqlite?"START < :maxtime":"\"START\" < :maxtime");
      link = " AND ";
      pixel_condData.extend<time_t>("maxtime");
      pixel_condData["maxtime"].data<time_t>() = maxtime;
    }

    assert(att_name.size() == name.size());
    assert(value.size() == name.size());
    for (unsigned int name_i=0; name_i<name.size(); name_i++) {
      if (!value[name_i].empty()) {
	pixel_cond += link + analysis_table_header+""+name[name_i]+" = :" + att_name[name_i];
	link = " AND ";
	pixel_condData.extend<std::string>(att_name[name_i]);
	pixel_condData[ att_name[name_i] ].data<std::string>() = value[name_i];
      }
    }

    if (quality != CAN::kUndefined) {
      pixel_cond += link + analysis_table_header+"QUALITY = :Quality";
      link = " AND ";
      pixel_condData.extend<int>("Quality");
      pixel_condData["Quality"].data<int>() = quality;
    }
    if (status != CAN::kNotSet) {
      pixel_cond += link + analysis_table_header+"STATUS = :Status";
      link = " AND ";
      pixel_condData.extend<int>("Status");
      pixel_condData["Status"].data<int>() = status;
    }
    if (location != CAN::kNowhere) {
      pixel_cond += link;
      pixel_cond += analysis_table_header+"SCAN_ID = " +  scan_tablename + ".SCAN_ID AND ";
      pixel_cond += scan_tablename+".LOCATION = :location";
      link = " AND ";
      pixel_condData.extend<int>("location");
      pixel_condData["location"].data<int>() = location;
      //      query->addToTableList(scan_tablename);
    }
    if (comment != "") {
      pixel_cond += link + analysis_table_header+(isSqlite?"COMMENT LIKE :Comment":"\"COMMENT\" LIKE :Comment");
      link = " AND ";
      pixel_condData.extend<std::string>("Comment");
      pixel_condData["Comment"].data<std::string>() = "%"+comment+"%";
    }
    query->setCondition( pixel_cond, pixel_condData);
    coral::ICursor& cursor = query->execute();

    while ( cursor.next() ) {
      const coral::AttributeList &row0 = cursor.currentRow();
      CAN::AnalysisInfo_t info;
      CAN::ObjectInfo_t alg(row0[analysis_table_header+"ANAL_NAME"].data<std::string>(),
			    row0[analysis_table_header+"ANAL_TAG"].data<std::string>(),
			    row0[analysis_table_header+"ANAL_REVISION"].data<CAN::Revision_t>());
      CAN::ObjectInfo_t clas(row0[analysis_table_header+"CLASS_NAME"].data<std::string>(),
			     row0[analysis_table_header+"CLASS_TAG"].data<std::string>(),
			     row0[analysis_table_header+"CLASS_REVISION"].data<CAN::Revision_t>());
      CAN::ObjectInfo_t post(row0[analysis_table_header+"POST_NAME"].data<std::string>(),
			     row0[analysis_table_header+"POST_TAG"].data<std::string>(),
			     row0[analysis_table_header+"POST_REVISION"].data<CAN::Revision_t>());
      info = CAN::AnalysisInfo_t(alg,clas,post,row0[analysis_table_header+"SCAN_ID"].data<CAN::SerialNumber_t>(),
				 row0[analysis_table_header+"SRC_ANAL_ID"].data<CAN::SerialNumber_t>());
      info.setQuality((CAN::EQuality)row0[analysis_table_header+"QUALITY"].data<int>());
      info.setStatus((CAN::GlobalStatus)row0[analysis_table_header+"STATUS"].data<int>());
      info.setComment(row0[analysis_table_header+(isSqlite?"COMMENT":"\"COMMENT\"")].data<std::string>());
      info.setAnalysisStart(row0[analysis_table_header+(isSqlite?"START":"\"START\"")].data<time_t>());
      info.setAnalysisEnd(row0[analysis_table_header+(isSqlite?"END":"\"END\"")].data<time_t>());
      info.setFile(row0[analysis_table_header+(isSqlite?"FILE":"\"FILE\"")].data<std::string>());
      info.setAlternateFile(row0[analysis_table_header+"ALTFILE"].data<std::string>());
      info.setSourcesFromString(row0[analysis_table_header+"SOURCES"].data<std::string>().c_str());

      id_info.insert(std::make_pair(cursor.currentRow()[analysis_table_header+"ANAL_ID"].data<CAN::SerialNumber_t>(), info));
    }

    delete query;

    transactionCommit();

    gettimeofday(&end_time, NULL);
    total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
      (end_time.tv_usec-start_time.tv_usec);
    if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;
    return id_info;
  }
  catch (const coral::Exception &err) {
    std::cout << "INFO [PixCoralDbClient::searchScanMetadata] While searching for scan metadata with: ";
    if (scan_serial_number>0) {
      std::cout<< "scan serial number: "<<scan_serial_number<<std::endl;
    }
    if (mintime>-1) std::cout<<" after "<<timeString(mintime);
    if (maxtime>-1) std::cout<<" before "<<timeString(maxtime);
    for (unsigned int name_i=0; name_i<name.size(); name_i++) {
      if (!value[name_i].empty()) std::cout<<"              " << name[name_i]<< ": "<<value[name_i]<<std::endl;
    }
    if (quality != CAN::kUndefined) std::cout<<" quality "<<quality;
    if (status != CAN::kNotSet) {
      std::cout<<"       status: "<< s_statusName[status]<<std::endl;
    }
    if (comment != "") std::cout<<" comment: "<<comment;
    std::cout << ", caught exception : " <<err.what() << "." << std::endl;
    return id_info;
  }
}





//---------------------------------------------------------------------------
/** Get metadata for a particular analysis ID
    Throws an PixMetaException if analysis ID is not found in the DB
*/
template <> 
CAN::AnalysisInfo_t PixCoralDbClient::getMetadata<CAN::AnalysisInfo_t>(CAN::SerialNumber_t anal_id, bool /* getRODstatus [not used] */){

  std::string tablename = m_pixeltable+"_ANAL";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Get metadata for analysis " << anal_id << std::endl;

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartReadOnly();

  CAN::AnalysisInfo_t info;

  // avoid double quotes for sqlite, only use this for coral
  bool isSqlite = m_connString.compare(0,strlen("sqlite_file:"),"sqlite_file:")==0 ||
    m_connString.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")==0;
  // Get all entries for anal_id
  coral::IQuery* query = m_session->nominalSchema().tableHandle(tablename).newQuery();
  // Get the list of columns
  const coral::ITableDescription& tableDesc =
    m_session->nominalSchema().tableHandle(tablename).description();
  int NumCol = tableDesc.numberOfColumns();
  for(int i=0; i<NumCol; ++i){
    const coral::IColumn& colDesc = tableDesc.columnDescription(i);
    // put column names that are oracle keywords into double quotes
    // avoid using oracle keywords as column names in the future
    std::string columnStr;
    if (!isSqlite && (colDesc.name() == "START" || colDesc.name() == "END" ||
		      colDesc.name() == "FILE" || colDesc.name() == "COMMENT")) columnStr = "\""+colDesc.name()+"\"";
    else columnStr = colDesc.name();
    query->addToOutputList(columnStr);
    //if (m_verbose) std::cout <<"	"<<i<<": "<< colDesc.name() << std::endl;
  }
  query->defineOutputType("ANAL_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
  query->defineOutputType("CLASS_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
  query->defineOutputType("POST_REVISION",coral::AttributeSpecification::typeNameForType<CAN::Revision_t>());
  query->defineOutputType("SCAN_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
  query->defineOutputType("SRC_ANAL_ID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
  query->defineOutputType(isSqlite?"START":"\"START\"",coral::AttributeSpecification::typeNameForType<time_t>());
  query->defineOutputType(isSqlite?"END":"\"END\"",coral::AttributeSpecification::typeNameForType<time_t>());

  std::string pixel_cond = tablename+".ANAL_ID = :analid";
  coral::AttributeList pixel_condData;
  pixel_condData.extend<CAN::SerialNumber_t>( "analid" );
  query->setCondition( pixel_cond, pixel_condData);
  pixel_condData["analid"].data<CAN::SerialNumber_t>() = anal_id;
  coral::ICursor& cursor = query->execute();

  if ( cursor.next() ) {
    const coral::AttributeList &row0 = cursor.currentRow();
    CAN::ObjectInfo_t alg(row0["ANAL_NAME"].data<std::string>(),row0["ANAL_TAG"].data<std::string>(),row0["ANAL_REVISION"].data<CAN::Revision_t>());
    CAN::ObjectInfo_t clas(row0["CLASS_NAME"].data<std::string>(),row0["CLASS_TAG"].data<std::string>(),row0["CLASS_REVISION"].data<CAN::Revision_t>());
    CAN::ObjectInfo_t post(row0["POST_NAME"].data<std::string>(),row0["POST_TAG"].data<std::string>(),row0["POST_REVISION"].data<CAN::Revision_t>());
    info = CAN::AnalysisInfo_t(alg,clas,post,row0["SCAN_ID"].data<CAN::SerialNumber_t>(),row0["SRC_ANAL_ID"].data<CAN::SerialNumber_t>());
    info.setQuality((CAN::EQuality)row0["QUALITY"].data<int>());
    info.setStatus((CAN::GlobalStatus)row0["STATUS"].data<int>());
    info.setComment(row0[isSqlite?"COMMENT":"\"COMMENT\""].data<std::string>());
    info.setAnalysisStart(row0[isSqlite?"START":"\"START\""].data<time_t>());
    info.setAnalysisEnd(row0[isSqlite?"END":"\"END\""].data<time_t>());
    info.setFile(row0[isSqlite?"FILE":"\"FILE\""].data<std::string>());
    info.setAlternateFile(row0["ALTFILE"].data<std::string>());
    info.setSourcesFromString(row0["SOURCES"].data<std::string>().c_str());
  } else {
    delete query;
    transactionCommit();
    std::stringstream message;
    message<<"CAN::PixCoralDbClient::getMetadata<CAN::AnalysisInfo_t>: No metadata found for analysis ID "<<anal_id;
    throw CAN::PixMetaException(message.str());
  }
  delete query;
  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return info;
}







//---------------------------------------------------------------------------
/** Modify metadata for a particular scan ID
    Throws a coral::DataEditorException if it failed to update scan
*/
template <> 
bool PixCoralDbClient::updateMetadata<PixLib::ScanInfo_t>(CAN::SerialNumber_t id, PixLib::ScanInfo_t * info){

  std::string tablename = m_pixeltable+"_SCAN";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for scan " << id << std::endl;

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for scan id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
  //Should never modify these
//   pixel_row.extend<std::string>("NAME");
//   pixel_row.extend<std::string>("TAG");
//   pixel_row.extend<unsigned int>("REVISION");
  pixel_row.extend<int>("QUALITY");
  pixel_row.extend<int>("STATUS");
  pixel_row.extend<std::string>("COMMENT");
  //Should never modify these
//   pixel_row.extend<int>("LOCATION");
//   pixel_row.extend<std::string>("IDTAG");
//   pixel_row.extend<std::string>("CONNTAG");
//   pixel_row.extend<std::string>("CONNDATATAG");
//   pixel_row.extend<std::string>("CONNALIASTAG");
//   pixel_row.extend<std::string>("CONNCFGTAG");
//   pixel_row.extend<unsigned int>("CONNCFGREV");
//   pixel_row.extend<std::string>("MODCFGTAG");
//   pixel_row.extend<unsigned int>("MODCFGREV");
  pixel_row.extend<time_t>("START");
  pixel_row.extend<time_t>("END");
  pixel_row.extend<std::string>("FILE");
  pixel_row.extend<std::string>("ALTFILE");
//   pixel_row.extend<std::string>("PIXDISABLE");

  pixel_row["SCAN_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row["QUALITY"].data<int>() = info->quality();
  pixel_row["STATUS"].data<int>() = info->status();
  pixel_row["COMMENT"].data<std::string>() = info->comment();
  pixel_row["START"].data<time_t>() = info->scanStart();
  pixel_row["END"].data<time_t>() = info->scanEnd();
  pixel_row["FILE"].data<std::string>() = info->file();
  pixel_row["ALTFILE"].data<std::string>() = info->alternateFile();

  long rowsUpdated = editor.updateRows("QUALITY = :QUALITY, STATUS = :STATUS, COMMENT = :COMMENT, START = :START, END = :END, FILE = :FILE, ALTFILE = :ALTFILE", "SCAN_ID = :SCAN_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}







//---------------------------------------------------------------------------
/** Modify metadata for a particular analysis ID
    Throws a coral::DataEditorException if it failed to update metadata
*/
template <> 
bool PixCoralDbClient::updateMetadata<CAN::AnalysisInfo_t>(CAN::SerialNumber_t id, CAN::AnalysisInfo_t * info){

  std::string tablename = m_pixeltable+"_ANAL";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for analysis " << id << std::endl;

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for anal id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("ANAL_ID");
  //Should never modify these
//   pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
//   pixel_row.extend<CAN::SerialNumber_t>("SRC_ANAL_ID");
//   pixel_row.extend<std::string>("ANAL_NAME");
//   pixel_row.extend<std::string>("ANAL_TAG");
//   pixel_row.extend<CAN::Revision_t>("ANAL_REVISION");
//   pixel_row.extend<std::string>("CLASS_NAME");
//   pixel_row.extend<std::string>("CLASS_TAG");
//   pixel_row.extend<CAN::Revision_t>("CLASS_REVISION");
//   pixel_row.extend<std::string>("POST_NAME");
//   pixel_row.extend<std::string>("POST_TAG");
//   pixel_row.extend<CAN::Revision_t>("POST_REVISION");
  pixel_row.extend<int>("QUALITY");
  pixel_row.extend<int>("STATUS");
  pixel_row.extend<std::string>("COMMENT");
  pixel_row.extend<time_t>("START");
  pixel_row.extend<time_t>("END");
  pixel_row.extend<std::string>("FILE");
  pixel_row.extend<std::string>("ALTFILE");
//   pixel_row.extend<std::string>("SOURCES");

  pixel_row["ANAL_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row["QUALITY"].data<int>() = (int)info->quality();
  pixel_row["STATUS"].data<int>() = (int)info->status();
  pixel_row["COMMENT"].data<std::string>() = info->comment();
  pixel_row["START"].data<time_t>() = info->analysisStart();
  pixel_row["END"].data<time_t>() = info->analysisEnd();
  pixel_row["FILE"].data<std::string>() = info->file();
  pixel_row["ALTFILE"].data<std::string>() = info->alternateFile();

  long rowsUpdated = editor.updateRows("QUALITY = :QUALITY, STATUS = :STATUS, COMMENT = :COMMENT, START = :START, END = :END, FILE = :FILE, ALTFILE = :ALTFILE", "ANAL_ID = :ANAL_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}







//--------------------------------------------------------------------------
/** Create and fill the CALIB_KEYGEN_META serial number generation table 
 */
void PixCoralDbClient::createAuxTables(){
  std::string FK_TABLE = "CALIB_KEYGEN_META";
  transactionStartUpdate();

  //   std::cout << "Deleting the old table: " <<  FK_TABLE << std::endl;
  //   m_session->nominalSchema().dropIfExistsTable(FK_TABLE);

  if (!m_session->nominalSchema().existsTable(FK_TABLE)) {
    if (m_verbose) std::cout << "Creating table: " <<FK_TABLE << std::endl;
    coral::TableDescription key_columns;
    key_columns.setName( FK_TABLE );
    key_columns.insertColumn( "SCAN",
			      coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    key_columns.insertColumn( "ANALYSIS",
			      coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    key_columns.insertColumn( "SESSIONID",
			      coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>());
    // Create the actual table
    try {
      m_session->nominalSchema().createTable( key_columns );
      // Fill the first key
      coral::ITableDataEditor& keyeditor = m_session->nominalSchema().tableHandle( FK_TABLE ).dataEditor();
      coral::AttributeList rowBuffer;
      rowBuffer.extend<CAN::SerialNumber_t>( "SCAN" );
      rowBuffer.extend<CAN::SerialNumber_t>( "ANALYSIS" );
      rowBuffer.extend<CAN::SerialNumber_t>( "SESSIONID" );
      rowBuffer[ "SCAN" ].data<CAN::SerialNumber_t>() = 1000;
      rowBuffer[ "ANALYSIS" ].data<CAN::SerialNumber_t>() = 1000;
      rowBuffer[ "SESSIONID" ].data<CAN::SerialNumber_t>() = 1000;
      keyeditor.insertRow( rowBuffer );
    }  catch (const coral::TableAlreadyExistingException & ex) {
      //Must have been created in parallel
    }
  }
  transactionCommit();
}




//---------------------------------------------------------------------------
/** Get a new serial number for scan or analysis
 */
CAN::SerialNumber_t PixCoralDbClient::getNewSerialNumber(CAN::ESerialNumberType type){
  if (m_verbose) std::cout << "\nCOOLCORAL Client: new serial number of type ";
  if (type == 0)
    std::cout<< "kScanNumber"<<std::endl;
  else if (type == 1)
    std::cout<< "kAnalysisNumber"<<std::endl;
  else if (type == 2)
    std::cout<< "kSessionID"<<std::endl;

  std::string FK_TABLE = "CALIB_KEYGEN_META";
  transactionStartUpdate();
  if (!m_session->nominalSchema().existsTable(FK_TABLE)) {
    createAuxTables();
    transactionStartUpdate();
  }

  coral::ITableDataEditor& keyeditor = m_session->nominalSchema().tableHandle( FK_TABLE ).dataEditor();
  std::string updateAction;
  if (type == CAN::kScanNumber) {
    updateAction = "SCAN = SCAN + :offset";
  } else if (type == CAN::kAnalysisNumber) {
    updateAction = "ANALYSIS = ANALYSIS + :offset";
  } else if (type == CAN::kSessionID) {
    updateAction = "SESSIONID = SESSIONID + :offset";
  }
  coral::AttributeList updateData;
  updateData.extend<CAN::SerialNumber_t>("offset");
  updateData["offset"].data<CAN::SerialNumber_t>() = 1;
  //long rowsUpdated = 
  keyeditor.updateRows( updateAction, "", updateData );
  //std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  coral::IQuery* query = m_session->nominalSchema().tableHandle(FK_TABLE).newQuery();
  if (type == CAN::kScanNumber) {
    query->addToOutputList("SCAN"); 
    query->defineOutputType("SCAN",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
  } else if (type == CAN::kAnalysisNumber) {
    query->addToOutputList("ANALYSIS"); 
    query->defineOutputType("ANALYSIS",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
  } else if (type == CAN::kSessionID) {
    query->addToOutputList("SESSIONID"); 
    query->defineOutputType("SESSIONID",coral::AttributeSpecification::typeNameForType<CAN::SerialNumber_t>()); 
  }
  coral::ICursor& cursor = query->execute();
  CAN::SerialNumber_t key=0;
  while ( cursor.next() ) {
    const coral::AttributeList &row0 = cursor.currentRow();
    key = row0[0].data<CAN::SerialNumber_t>();
    if (m_verbose) std::cout << " key = " << key << std::endl; 
  }
  transactionCommit();
  return key;
}





//---------------------------------------------------------------------------
/** Store scan metadata in DB
 */
template<> void PixCoralDbClient::storeMetadata<PixLib::ScanInfo_t>(CAN::SerialNumber_t id, PixLib::ScanInfo_t *info){

  if (m_verbose) std::cout << "\nCOOLCORAL Client: storeMetadata for scan "<<id << std::endl;
  
  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  std::string tablename = m_pixeltable+"_SCAN";

  // Create if not yet there
  if (!m_session->nominalSchema().existsTable(tablename)) {
    createTables();
    transactionStartUpdate();
  }

  coral::ITableDataEditor& pixel_editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
  pixel_row.extend<std::string>("NAME");
  pixel_row.extend<std::string>("TAG");
  pixel_row.extend<unsigned int>("REVISION");
  pixel_row.extend<int>("QUALITY");
  pixel_row.extend<int>("STATUS");
  pixel_row.extend<int>("LOCATION");
  pixel_row.extend<std::string>("COMMENT");
  pixel_row.extend<std::string>("IDTAG");
  pixel_row.extend<std::string>("CONNTAG");
  pixel_row.extend<std::string>("CONNDATATAG");
  pixel_row.extend<std::string>("CONNALIASTAG");
  pixel_row.extend<std::string>("CONNCFGTAG");
  pixel_row.extend<unsigned int>("CONNCFGREV");
  pixel_row.extend<std::string>("MODCFGTAG");
  pixel_row.extend<unsigned int>("MODCFGREV");
  pixel_row.extend<time_t>("START");
  pixel_row.extend<time_t>("END");
  pixel_row.extend<std::string>("FILE");
  pixel_row.extend<std::string>("ALTFILE");
  pixel_row.extend<std::string>("PIXDISABLE");

  pixel_row["SCAN_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row["NAME"].data<std::string>() = info->scanType();
  pixel_row["TAG"].data<std::string>() = info->scanTypeTag();
  pixel_row["REVISION"].data<unsigned int>() = info->scanTypeRev();
  pixel_row["QUALITY"].data<int>() = (int)info->quality();
  pixel_row["STATUS"].data<int>() = (int)info->status();
  pixel_row["LOCATION"].data<int>() = (int)info->location();
  pixel_row["COMMENT"].data<std::string>() = info->comment();
  pixel_row["IDTAG"].data<std::string>() = info->idTag();
  pixel_row["CONNTAG"].data<std::string>() = info->connTag();
  pixel_row["CONNDATATAG"].data<std::string>() = info->dataTag();
  pixel_row["CONNALIASTAG"].data<std::string>() = info->aliasTag();
  pixel_row["CONNCFGTAG"].data<std::string>() = info->cfgTag();
  pixel_row["CONNCFGREV"].data<unsigned int>() = info->cfgRev();
  pixel_row["MODCFGTAG"].data<std::string>() = info->modCfgTag();
  pixel_row["MODCFGREV"].data<unsigned int>() = info->modCfgRev();
  pixel_row["START"].data<time_t>() = info->scanStart();
  pixel_row["END"].data<time_t>() = info->scanEnd();
  pixel_row["FILE"].data<std::string>() = info->file();
  pixel_row["ALTFILE"].data<std::string>() = info->alternateFile();
  pixel_row["PIXDISABLE"].data<std::string>() = info->pixDisableString();

  pixel_editor.insertRow(pixel_row);


  //Fill per ROD status, if any
  if (info->statusMap().begin() != info->statusMap().end()) {
    coral::ITableDataEditor& pixel_editor_2 = m_session->nominalSchema().tableHandle(m_pixeltable+"_ROD_SCAN").dataEditor();
    coral::AttributeList pixel_row_2;
    pixel_row_2.extend<CAN::SerialNumber_t>("SCAN_ID");
    pixel_row_2.extend<std::string>("RODNAME");
    pixel_row_2.extend<int>("STATUS");
    pixel_row_2.extend<time_t>("SCANSTART");
    pixel_row_2.extend<time_t>("SCANEND");
    pixel_row_2.extend<time_t>("HISTOWSTART");
    pixel_row_2.extend<time_t>("HISTOWEND");
    pixel_row_2.extend<time_t>("CONFIGEND");
    pixel_row_2.extend<int>("HISTODOWNLOAD");
    pixel_row_2.extend<int>("LOOP0");
    coral::IBulkOperation* pixel_bulk_2= pixel_editor_2.bulkInsert(pixel_row_2,info->statusMap().size());
    for (std::map<std::string, PixLib::RODstatus_t >::const_iterator val_iter=info->statusMap().begin();
	 val_iter != info->statusMap().end();
	 ++val_iter) {
      pixel_row_2["SCAN_ID"].data<CAN::SerialNumber_t>() = id;
      pixel_row_2["RODNAME"].data<std::string>() = val_iter->first;
      pixel_row_2["STATUS"].data<int>() = (int)val_iter->second.status();
      pixel_row_2["SCANSTART"].data<time_t>() = val_iter->second.scanStart();
      pixel_row_2["SCANEND"].data<time_t>() = val_iter->second.scanEnd();
      pixel_row_2["HISTOWSTART"].data<time_t>() = val_iter->second.histogramWriteStart();
      pixel_row_2["HISTOWEND"].data<time_t>() = val_iter->second.histogramWriteEnd();
      pixel_row_2["CONFIGEND"].data<time_t>() = val_iter->second.configEnd();
      pixel_row_2["HISTODOWNLOAD"].data<int>() = val_iter->second.histogramDownloadTime();
      pixel_row_2["LOOP0"].data<int>() = val_iter->second.loop0Time();
      pixel_bulk_2->processNextIteration();
    }
    pixel_bulk_2->flush();
    delete pixel_bulk_2;
  }

  transactionCommit();

  // measuring elapsed time
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

}





//---------------------------------------------------------------------------
/** Store ROD status in scan metadata
 */
void PixCoralDbClient::storeRODstatus(CAN::SerialNumber_t id, std::map<std::string, PixLib::RODstatus_t> *info){

  if (m_verbose) std::cout << "\nCOOLCORAL Client: storeRODstatus for scan "<<id << std::endl;
  
  // Check if the scan ID exists
  try {
    getMetadata<PixLib::ScanInfo_t>(id);
  } catch (const CAN::PixMetaException &) {
    //scan ID doesn't exist
    std::stringstream message;
    message<<"CAN::PixCoralDbClient::storeRODstatus: No metadata found for scan ID "<<id<<". Cannot store ROD status for non-existing scan.";
    throw CAN::PixMetaException(message.str());
  }


  std::string tablename = m_pixeltable+"_ROD_SCAN";

  transactionStartUpdate();

  // Create if not yet there
  if (!m_session->nominalSchema().existsTable(tablename)) {
    createTables();
    transactionStartUpdate();
  }

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  //Fill per ROD status, if any
  if (info->begin() != info->end()) {
    coral::ITableDataEditor& pixel_editor_2 = m_session->nominalSchema().tableHandle(tablename).dataEditor();
    coral::AttributeList pixel_row_2;
    pixel_row_2.extend<CAN::SerialNumber_t>("SCAN_ID");
    pixel_row_2.extend<std::string>("RODNAME");
    pixel_row_2.extend<int>("STATUS");
    pixel_row_2.extend<time_t>("SCANSTART");
    pixel_row_2.extend<time_t>("SCANEND");
    pixel_row_2.extend<time_t>("HISTOWSTART");
    pixel_row_2.extend<time_t>("HISTOWEND");
    pixel_row_2.extend<time_t>("CONFIGEND");
    pixel_row_2.extend<int>("HISTODOWNLOAD");
    pixel_row_2.extend<int>("LOOP0");
    coral::IBulkOperation* pixel_bulk_2= pixel_editor_2.bulkInsert(pixel_row_2,info->size());
    for (std::map<std::string, PixLib::RODstatus_t >::const_iterator val_iter=info->begin();
	 val_iter != info->end();
	 ++val_iter) {
      pixel_row_2["SCAN_ID"].data<CAN::SerialNumber_t>() = id;
      pixel_row_2["RODNAME"].data<std::string>() = val_iter->first;
      pixel_row_2["STATUS"].data<int>() = (int)val_iter->second.status();
      pixel_row_2["SCANSTART"].data<time_t>() = val_iter->second.scanStart();
      pixel_row_2["SCANEND"].data<time_t>() = val_iter->second.scanEnd();
      pixel_row_2["HISTOWSTART"].data<time_t>() = val_iter->second.histogramWriteStart();
      pixel_row_2["HISTOWEND"].data<time_t>() = val_iter->second.histogramWriteEnd();
      pixel_row_2["CONFIGEND"].data<time_t>() = val_iter->second.configEnd();
      pixel_row_2["HISTODOWNLOAD"].data<int>() = val_iter->second.histogramDownloadTime();
      pixel_row_2["LOOP0"].data<int>() = val_iter->second.loop0Time();
      pixel_bulk_2->processNextIteration();
    }
    pixel_bulk_2->flush();
    delete pixel_bulk_2;
  }

  transactionCommit();

  // measuring elapsed time
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

}





//---------------------------------------------------------------------------
/** Store analysis metadata in DB
    Throws a PixMetaException if the scan ID doesn't exist
*/
template<> void PixCoralDbClient::storeMetadata<CAN::AnalysisInfo_t>(CAN::SerialNumber_t id, CAN::AnalysisInfo_t *info){

  if (m_verbose) std::cout << "\nCOOLCORAL Client: storeMetadata for analysis "<<id << std::endl;

  // Check if the scan ID exists
  try {
    getMetadata<PixLib::ScanInfo_t>(info->scanSerialNumber());
  } catch (const CAN::PixMetaException &) {
    //scan ID doesn't exist
    std::stringstream message;
    message<<"CAN::PixCoralDbClient::storeMetadata<PixLib::AnalysisInfo_t>: No metadata found for scan ID "<<info->scanSerialNumber()<<". Cannot create analysis metadata for non-existing scan.";
    throw CAN::PixMetaException(message.str());
  }
    
  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  std::string tablename = m_pixeltable+"_ANAL";

  // Create if not yet there
  if (!m_session->nominalSchema().existsTable(tablename)) {
    createTables();
    transactionStartUpdate();
  }

  coral::ITableDataEditor& pixel_editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("ANAL_ID");
  pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
  pixel_row.extend<CAN::SerialNumber_t>("SRC_ANAL_ID");
  pixel_row.extend<std::string>("ANAL_NAME");
  pixel_row.extend<std::string>("ANAL_TAG");
  pixel_row.extend<CAN::Revision_t>("ANAL_REVISION");
  pixel_row.extend<std::string>("CLASS_NAME");
  pixel_row.extend<std::string>("CLASS_TAG");
  pixel_row.extend<CAN::Revision_t>("CLASS_REVISION");
  pixel_row.extend<std::string>("POST_NAME");
  pixel_row.extend<std::string>("POST_TAG");
  pixel_row.extend<CAN::Revision_t>("POST_REVISION");
  pixel_row.extend<int>("QUALITY");
  pixel_row.extend<int>("STATUS");
  pixel_row.extend<std::string>("COMMENT");
  pixel_row.extend<time_t>("START");
  pixel_row.extend<time_t>("END");
  pixel_row.extend<std::string>("FILE");
  pixel_row.extend<std::string>("ALTFILE");
  pixel_row.extend<std::string>("SOURCES");

  pixel_row["ANAL_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row["SCAN_ID"].data<CAN::SerialNumber_t>() = info->scanSerialNumber();
  pixel_row["SRC_ANAL_ID"].data<CAN::SerialNumber_t>() = info->srcAnalysisSerialNumber();
  pixel_row["ANAL_NAME"].data<std::string>() = info->name(CAN::kAlg);
  pixel_row["ANAL_TAG"].data<std::string>() = info->tag(CAN::kAlg);
  pixel_row["ANAL_REVISION"].data<CAN::Revision_t>() = info->revision(CAN::kAlg);
  pixel_row["CLASS_NAME"].data<std::string>() = info->name(CAN::kClassification);
  pixel_row["CLASS_TAG"].data<std::string>() = info->tag(CAN::kClassification);
  pixel_row["CLASS_REVISION"].data<CAN::Revision_t>() = info->revision(CAN::kClassification);
  pixel_row["POST_NAME"].data<std::string>() = info->name(CAN::kPostProcessing);
  pixel_row["POST_TAG"].data<std::string>() = info->tag(CAN::kPostProcessing);
  pixel_row["POST_REVISION"].data<CAN::Revision_t>() = info->revision(CAN::kPostProcessing);
  pixel_row["QUALITY"].data<int>() = (int)info->quality();
  pixel_row["STATUS"].data<int>() = (int)info->status();
  pixel_row["COMMENT"].data<std::string>() = info->comment();
  pixel_row["START"].data<time_t>() = info->analysisStart();
  pixel_row["END"].data<time_t>() = info->analysisEnd();
  pixel_row["FILE"].data<std::string>() = info->file();
  pixel_row["ALTFILE"].data<std::string>() = info->alternateFile();
  pixel_row["SOURCES"].data<std::string>() = info->createSourceString();

  pixel_editor.insertRow(pixel_row);

  // measuring elapsed time
  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  transactionCommit();

}


const char *PixCoralDbClient::s_statusFieldName="STATUS";
const char *PixCoralDbClient::s_endTimeFieldName="END";

const char *PixCoralDbClient::s_modifiableFieldsNormal[]={
  "QUALITY",
  "STATUS",
  "COMMENT",
  "END",
  "FILE",
  "ALTFILE",
  NULL
};

const char *PixCoralDbClient::s_modifiableFieldsForFixing[]={
  "QUALITY",
  "STATUS",
  "LOCATION",
  "COMMENT",
  "START",
  "END",
  "FILE",
  "ALTFILE",
  "CONNCFGREV",
  "CFGTAG",
  "FGREV",
  "MODCFGTAG",
  "MODCFGREV",
  "PIXDISABLE",
  NULL
};

const char **PixCoralDbClient::s_modifiableFields=s_modifiableFieldsNormal;

void PixCoralDbClient::fixMode() {
  s_modifiableFields=s_modifiableFieldsForFixing;
}

/** return true if the given field can be modified.
 */
bool PixCoralDbClient::fieldCanBeModified(const std::string &field_name) {
  const char **field_name_ptr = s_modifiableFields;
  while (*field_name_ptr) {
    if (field_name == *field_name_ptr) return true;
    ++field_name_ptr;
  }
  return false;
}



//---------------------------------------------------------------------------
/** Modify specific metadata field for a particular scan ID
    Throws a coral::DataEditorException if it failed to update scan
    Throws a CAN::PixMetaException if trying to update a readonly field
*/
template <class T> 
bool PixCoralDbClient::updateScanMetadataField(CAN::SerialNumber_t id, std::string field, T value) {

  std::string tablename = m_pixeltable+"_SCAN";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for scan " << id << " and field " << field << std::endl;

  if (!scanFieldCanBeModified(field)) {
    throw CAN::PixMetaException("CAN::PixCoralDbClient::updateScanMetadataField<T>: Cannot update scan metadata field "+field);
  }

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for scan id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
  pixel_row.extend<T>(field);

  pixel_row["SCAN_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row[field].data<T>() = value;
  std::string updatestring = field + " = :" + field;
  long rowsUpdated = editor.updateRows(updatestring, "SCAN_ID = :SCAN_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}

template <class T1, class T2>
bool PixCoralDbClient::updateScanMetadataField(CAN::SerialNumber_t id, const std::string &field1, T1 value1, const std::string &field2, T2 value2) {

  std::string tablename = m_pixeltable+"_SCAN";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for scan " << id << " and field " << field1 << ", " << field2 << std::endl;

  if (!scanFieldCanBeModified(field1) || !scanFieldCanBeModified(field2)) {
    throw CAN::PixMetaException("CAN::PixCoralDbClient::updateScanMetadataField<T1,T2>: Cannot update scan metadata field "+field1+" or "+field2 );
  }

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for scan id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("SCAN_ID");
  pixel_row.extend<T1>(field1);
  pixel_row.extend<T2>(field2);

  pixel_row["SCAN_ID"].data<CAN::SerialNumber_t>() = id;

  pixel_row[field1].data<T1>() = value1;
  std::string updatestring = field1 + " = :" + field1;

  pixel_row[field2].data<T2>() = value2;
  updatestring += ", ";
  updatestring += field2 + " = :" + field2;
  long rowsUpdated = editor.updateRows(updatestring, "SCAN_ID = :SCAN_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}





bool PixCoralDbClient::updateScanMetadataFile(CAN::SerialNumber_t id, std::string value) {
  return updateScanMetadataField<std::string>(id, "FILE", value);
}

bool PixCoralDbClient::updateScanMetadataAlternateFile(CAN::SerialNumber_t id, std::string value) {
  return updateScanMetadataField<std::string>(id, "ALTFILE", value);
}

bool PixCoralDbClient::updateScanMetadataComment(CAN::SerialNumber_t id, std::string value) {
  return updateScanMetadataField<std::string>(id, "COMMENT", value);
}

bool PixCoralDbClient::updateScanMetadataDisable(CAN::SerialNumber_t id, std::string value) {
  return updateScanMetadataField<std::string>(id, "PIXDISABLE", value);
}

bool PixCoralDbClient::updateScanMetadataStart(CAN::SerialNumber_t id, time_t value) {
  return updateScanMetadataField<time_t>(id, "START", value);
}

bool PixCoralDbClient::updateScanMetadataEnd(CAN::SerialNumber_t id, time_t value) {
  return updateScanMetadataField<time_t>(id, "END", value);
}

bool PixCoralDbClient::updateScanMetadataStatus(CAN::SerialNumber_t id, CAN::GlobalStatus value) {
  return updateScanMetadataField<int>(id, "STATUS", value);
}

bool PixCoralDbClient::updateScanMetadataLocation(CAN::SerialNumber_t id, CAN::ScanLocation value) {
  return updateScanMetadataField<int>(id, "LOCATION", value);
}

bool PixCoralDbClient::updateScanMetadataQuality(CAN::SerialNumber_t id, CAN::EQuality value) {
  return updateScanMetadataField<int>(id, "QUALITY", value);
}

template
bool PixCoralDbClient::updateScanMetadataField<unsigned int>(SerialNumber_t id, std::string name, unsigned int value);


//---------------------------------------------------------------------------
/** Modify specific metadata field for a particular analysis ID
    Throws a coral::DataEditorException if it failed to update metadata
    Throws a CAN::PixMetaException if trying to update a readonly field
*/
template <class T> 
bool PixCoralDbClient::updateAnalysisMetadataField(CAN::SerialNumber_t id, std::string field, T value) {

  std::string tablename = m_pixeltable+"_ANAL";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for analysis " << id << " and field " << field << std::endl;

  if (!analysisFieldCanBeModified(field)) {
    throw CAN::PixMetaException("CAN::PixCoralDbClient::updateAnalysisMetadataField<T>: Cannot update analysis metadata field "+field);
  }

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for anal id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("ANAL_ID");
  pixel_row.extend<T>(field);

  pixel_row["ANAL_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row[field].data<T>() = value;
  std::string updatestring = field + " = :" + field;
  long rowsUpdated = editor.updateRows(updatestring, "ANAL_ID = :ANAL_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}

template <class T1, class T2> 
bool PixCoralDbClient::updateAnalysisMetadataField(CAN::SerialNumber_t id, const std::string &field1, T1 value1, const std::string &field2, T2 value2) {

  std::string tablename = m_pixeltable+"_ANAL";

  if (m_verbose) std::cout << "DEBUG [COOLCORAL Client] " << tablename <<" Update metadata for analysis " << id << " and field " << field1 << " ," << field2 << std::endl;

  if (!analysisFieldCanBeModified(field2) || !analysisFieldCanBeModified(field1)) {
    throw CAN::PixMetaException("CAN::PixCoralDbClient::updateAnalysisMetadataField<T1,T2>: Cannot update analysis metadata field "+field1+", or "+field2);
  }

  struct timeval start_time, end_time;
  int total_usecs;
  gettimeofday(&start_time, NULL);

  transactionStartUpdate();

  // Get all entries for anal id
  coral::ITableDataEditor& editor = m_session->nominalSchema().tableHandle(tablename).dataEditor();
  coral::AttributeList pixel_row;
  pixel_row.extend<CAN::SerialNumber_t>("ANAL_ID");
  pixel_row.extend<T1>(field1);
  pixel_row.extend<T2>(field2);

  pixel_row["ANAL_ID"].data<CAN::SerialNumber_t>() = id;
  pixel_row[field1].data<T1>() = value1;
  pixel_row[field2].data<T2>() = value2;
  std::string updatestring = field1 + " = :" + field1;
  updatestring += ", ";
  updatestring += field2 + " = :" + field2;
  long rowsUpdated = editor.updateRows(updatestring, "ANAL_ID = :ANAL_ID", pixel_row);

  if (m_verbose) std::cout << " Updated " << rowsUpdated << " rows" << std::endl;

  transactionCommit();

  gettimeofday(&end_time, NULL);
  total_usecs = (end_time.tv_sec-start_time.tv_sec) * 1000000 +
    (end_time.tv_usec-start_time.tv_usec);
  if (m_verbose) std::cout << " Total time was " << total_usecs << " usec" << std::endl;

  return rowsUpdated>0;
}


bool PixCoralDbClient::updateAnalysisMetadataFile(CAN::SerialNumber_t id, std::string value) {
  return updateAnalysisMetadataField<std::string>(id, "FILE", value);
}

bool PixCoralDbClient::updateAnalysisMetadataAlternateFile(CAN::SerialNumber_t id, std::string value) {
  return updateAnalysisMetadataField<std::string>(id, "ALTFILE", value);
}

bool PixCoralDbClient::updateAnalysisMetadataComment(CAN::SerialNumber_t id, std::string value) {
  return updateAnalysisMetadataField<std::string>(id, "COMMENT", value);
}

bool PixCoralDbClient::updateAnalysisMetadataStart(CAN::SerialNumber_t id, time_t value) {
  return updateAnalysisMetadataField<time_t>(id, "START", value);
}

bool PixCoralDbClient::updateAnalysisMetadataEnd(CAN::SerialNumber_t id, time_t value) {
  return updateAnalysisMetadataField<time_t>(id, "END", value);
}

bool PixCoralDbClient::updateAnalysisMetadataStatus(CAN::SerialNumber_t id, CAN::GlobalStatus value) {
  return updateAnalysisMetadataField<int>(id, "STATUS", value);
}

bool PixCoralDbClient::updateAnalysisMetadataQuality(CAN::SerialNumber_t id, CAN::EQuality value) {
  return updateAnalysisMetadataField<int>(id, "QUALITY", value);
}


//instantiate templates
template bool PixCoralDbClient::updateScanMetadataField<int,time_t>(CAN::SerialNumber_t,const std::string &, int, const std::string &, time_t);
template bool PixCoralDbClient::updateAnalysisMetadataField<int,time_t>(CAN::SerialNumber_t,const std::string &, int, const std::string &, time_t);


/** CORAL helper to start a transaction in readonly mode
 */
void PixCoralDbClient::transactionStartReadOnly(){
  if (m_session->transaction().isActive()) m_session->transaction().rollback();
  m_session->transaction().start(true /*ReadOnly*/);
}



/** CORAL helper to start a transaction in update mode
 */
void PixCoralDbClient::transactionStartUpdate(){
  if (m_session->transaction().isActive()) m_session->transaction().rollback();
  m_session->transaction().start(false /*update*/);
}



/** CORAL helper to commit changes at the end of a transaction
 */
void PixCoralDbClient::transactionCommit(){
  if (m_session->transaction().isActive()) m_session->transaction().commit();
}


} //namespace CAN

