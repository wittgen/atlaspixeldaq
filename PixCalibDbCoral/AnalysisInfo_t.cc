#include "AnalysisInfo_t.hh"
#include <iostream>
#include <iomanip>
#include "PixMetaException.hh"

namespace CAN {

  const char *AnalysisInfo_t::s_objectName[kNObjectTypes]={
    "Algorithm",
    "Classification",
    "PostProcessing"
  };

  void AnalysisInfo_t::init() {

    m_objectInfo.resize( kNObjectTypes );

  }

  void AnalysisInfo_t::addSource( const std::string name, ESerialNumberType type, SerialNumber_t serial_number) {
    assert( !name.empty() && type<=kAnalysisNumber && serial_number>0);
    m_multiSources.insert(std::make_pair(name,std::make_pair(type,serial_number)));
    std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::iterator anonymous_iter = m_multiSources.find("");
    if (anonymous_iter != m_multiSources.end()) {
      m_multiSources.erase(anonymous_iter);
    }
  }

  void AnalysisInfo_t::Print() {
    std::cout<<" == Analysis metadata:"<<std::endl;
    if (isAnalysisOfAnAnalysis()) {
      std::cout<<"  Analysis of analysis "<<m_srcAnalysisSerialNumber<<std::endl;
    }
    //m_info.dump(std::cout);
    std::cout<<"  Scan ID:             "<<scanSerialNumber()
	     <<"\n  Algorithm name:      "<<name(CAN::kAlg)
	     <<"\n             tag:      "<<tag(CAN::kAlg)
	     <<"\n        revision:      "<<revision(CAN::kAlg)
	     <<"\n  Classification name: "<<name(CAN::kClassification)
	     <<"\n                  tag: "<<tag(CAN::kClassification)
	     <<"\n             revision: "<<revision(CAN::kClassification)
	     <<"\n  PostProcessing name: "<<name(CAN::kPostProcessing)
	     <<"\n                  tag: "<<tag(CAN::kPostProcessing)
	     <<"\n             revision: "<<revision(CAN::kPostProcessing)
	     <<"\n  Quality:             ";
    if (m_quality == kNormal) std::cout<<"Normal";
    else if (m_quality == kTrash) std::cout<<"Trash";
    else if (m_quality == kDebug) std::cout<<"Debug";
    else if (m_quality == kUndefined) std::cout<<"N/A";
    std::cout<<"\n  Status:              ";
    if (m_status == CAN::kSuccess) std::cout<<"Success";
    else if (m_status == CAN::kFailure) std::cout<<"Failure";
    else if (m_status == CAN::kAborted) std::cout<<"Aborted";
    else if (m_status == CAN::kNotSet) std::cout<<"N/A";

    std::cout<<"\n  Comment:             "<<comment()
	     <<"\n  Analysis start:      "<<analysisStart()<<" ("<<analysisStartString()<<")"
	     <<"\n             end:      "<<analysisEnd()<<" ("<<analysisEndString()<<")";
    std::cout<<"\n  File location:       "<<file()
	     <<"\n  AltFile location:    "<<alternateFile()
	     << std::endl;
    std::cout << "Scan / analysis names : " << std::endl;
    for (std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = beginSource();
	 source_iter != endSource();
	 source_iter++) {
      std::cout << "\t";
      if (!source_iter->first.empty()) {
	std::cout << source_iter->first << " : ";
      }
      else {
	std::cout << "anonymous  : ";
      }
      if (source_iter->second.first == kScanNumber) {
	std::cout << 'S';
      }
      if (source_iter->second.first == kAnalysisNumber) {
	std::cout << 'A';
      }
      std::cout << std::setw(9) << std::setfill('0') << source_iter->second.second << std::setfill(' ') << std::endl;
    }
  }


  inline void copyObjectInfo(const ObjectInfoData_t &src, ObjectInfo_t &dest) {
    dest.m_name = src.m_name;
    dest.m_tag =  src.m_tag;
    dest.m_revision = src.m_revision;
  }

  void copyAnalysisInfo(const CAN::AnalysisInfoData_t &src, CAN::AnalysisInfo_t &dest) {

    if (src.m_objectInfo.length()<kNObjectTypes) {
      throw PixMetaException("CAN::copyAnalysisInfo: Missing object information. Need information about the algorithm, the classification and the post-processing.");
    }

    for (unsigned int obj_i=0; obj_i<kNObjectTypes; obj_i++) {
      copyObjectInfo(src.m_objectInfo[obj_i],dest.m_objectInfo[obj_i]);
    }
    dest.m_scanSerialNumber=src.m_scanSerialNumber;
    dest.m_srcAnalysisSerialNumber = src.m_srcAnalysisSerialNumber;
    dest.m_quality = src.m_quality;
    dest.m_status = src.m_status;
    dest.m_comment = src.m_comment;
    dest.m_timeAnalysisStart = src.m_timeAnalysisStart;
    dest.m_timeAnalysisEnd = src.m_timeAnalysisEnd;
    dest.m_file = src.m_file;
    dest.m_altFile = src.m_altFile;
    dest.setSourcesFromString(src.m_sources);
  }

  void AnalysisInfo_t::setSourcesFromString(const char *source_string)
  {
    // string format:
    //   hvoff:S000001009,hvon:S000001010 or
    //   new:S00001009,old:A00001008


    m_multiSources.clear();
    if (source_string) {
      const char *ptr=source_string;

      bool error=false;
      while (*ptr) {
	const char *name_start_ptr = ptr;
	// source names are separated by a ':'. Everything else is allowed
	while (*ptr && *ptr != ':') ptr++;

	const char *name_end_ptr = ptr;
	if (!*ptr) {
	  error = true;
	  break;
	}
	ptr++;

	ESerialNumberType type;
	if (*ptr=='S') {
	  type=kScanNumber;
	}
	else if (*ptr=='A') {
	  type=kAnalysisNumber;
	}
	else {
	  error=true;
	  break;
	}

	if (!*ptr) {
	  error = true;
	  break;
	}
	ptr++;
	const char *number_start = ptr;
	while (isdigit(*ptr)) ptr++;
	if (*ptr && *ptr!=',') {
	  error=true;
	  break;
	}
	SerialNumber_t serial_number = atoi( number_start);

	m_multiSources.insert(std::make_pair( std::string(name_start_ptr, name_end_ptr-name_start_ptr),
					      std::make_pair(type,serial_number)));
	if (*ptr==',') {
	  ptr++;
	  if (!*ptr) {
	    error=true;
	    break;
	  }
	}
      }

      if (error) {
	std::stringstream message;
	message << "Format error in source string : " << source_string;
	throw PixMetaException(message.str());
      }
    }
    addAnonymousSource();
  }
  void AnalysisInfo_t::addAnonymousSource()
  {
    if (m_multiSources.empty()) {
      if (m_scanSerialNumber>0) {
	m_multiSources.insert(std::make_pair( "",
					      std::make_pair(kScanNumber,m_scanSerialNumber)));
      }
      else if (m_srcAnalysisSerialNumber) {
	m_multiSources.insert(std::make_pair( "",
					      std::make_pair(kAnalysisNumber,m_srcAnalysisSerialNumber)));
      }
    }
  }

  std::string AnalysisInfo_t::createSourceString() const
  {
    // string format:
    //   hvoff:S000001009,hvon:S000001010 or
    //   new:S00001009,old:A00001008
    std::stringstream source_string;
    /*source_string << std::setfill('0')*/
    bool first=true;
    for (std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = beginSource();
	 source_iter != endSource();
	 source_iter++) {
      if (!source_iter->first.empty()) {
      if ( !first ) {
	source_string << ',';
      }
      else {
	first=false;
      }

      source_string << source_iter->first << ":";
      if (source_iter->second.first == kScanNumber) {
	source_string << 'S';
      }
      if (source_iter->second.first == kAnalysisNumber) {
	source_string << 'A';
      }
      source_string << /*std::setw(9) << */ source_iter->second.second;
      }
    }
    return source_string.str();
  }

  bool AnalysisInfo_t::check() const {
    // check serial numbers
    if (m_scanSerialNumber==0 && m_srcAnalysisSerialNumber==0) return false;

    // check object names and tags
    for (unsigned int obj_i=0; obj_i<kNObjectTypes; obj_i++) {
      if (m_objectInfo[obj_i].m_name.empty()) return false;
      if (m_objectInfo[obj_i].m_tag.empty()) return false;
    }

    // check the multi serial numbers field
    bool contains_master_serial_number =false;
    for (std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = beginSource();
	 source_iter != endSource();
	 source_iter++) {
      if (source_iter->second.first==kScanNumber) {
	if (m_scanSerialNumber>0 && source_iter->second.second == m_scanSerialNumber) {
	  contains_master_serial_number =true;
	  break;
	}
      }
      else if (source_iter->second.first==kAnalysisNumber) {
	if (m_srcAnalysisSerialNumber>0 && source_iter->second.second == m_srcAnalysisSerialNumber) {
	  contains_master_serial_number =true;
	  break;
	}
      }
    }
    return contains_master_serial_number;
  }

  const std::pair<ESerialNumberType, SerialNumber_t> &AnalysisInfo_t::source(const std::string &name) const {
    std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator source_iter = m_multiSources.find(name);
    if (source_iter == m_multiSources.end()) {
	std::stringstream message;
	message << "No source with name : " << name;
	throw PixMetaException(message.str());
    }
    return source_iter->second;
  }


  void copyAnalysisInfo(const CAN::AnalysisInfo_t &src, CAN::AnalysisInfoData_t &dest) {

    dest.m_objectInfo.length(kNObjectTypes);
    for (unsigned int obj_i=0; obj_i<kNObjectTypes; obj_i++) {
      const ObjectInfo_t &obj=src.objectInfo(static_cast<EObjectType>(obj_i));
      dest.m_objectInfo[obj_i].m_name = CORBA::string_dup(obj.name().c_str());
      dest.m_objectInfo[obj_i].m_tag = CORBA::string_dup(obj.tag().c_str());
      dest.m_objectInfo[obj_i].m_revision = obj.revision();
    }
    dest.m_scanSerialNumber = src.scanSerialNumber();
    dest.m_srcAnalysisSerialNumber = src.srcAnalysisSerialNumber();
    dest.m_quality = src.quality();
    dest.m_status = src.status();
    dest.m_comment = CORBA::string_dup(src.comment().c_str());
    dest.m_timeAnalysisStart = src.analysisStart();
    dest.m_timeAnalysisEnd = src.analysisEnd();
    dest.m_file = CORBA::string_dup(src.file().c_str());
    dest.m_altFile = CORBA::string_dup(src.alternateFile().c_str());
    dest.m_sources = CORBA::string_dup(src.createSourceString().c_str());

  }

}

