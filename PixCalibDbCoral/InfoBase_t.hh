#ifndef _InfoBase_t_hh_
#define _InfoBase_t_hh_

#include <string>

namespace CAN {

  class InfoBase_t
  {
  public:
    InfoBase_t(const InfoBase_t &src) : m_name(src.m_name) {}

    InfoBase_t(const std::string &name) : m_name(name) {}
    virtual ~InfoBase_t() {}

    virtual void Print() = 0;

  protected:
    std::string m_name;
  };

}
#endif
