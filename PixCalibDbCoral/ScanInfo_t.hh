#ifndef _ScanInfo_t_hh_
#define _ScanInfo_t_hh_

#include "InfoBase_t.hh"
#include "Common.hh"
#include <map>
#include <ctime>

namespace PixLib {

  /** ROD status.
   * Information about scan start, end times, status.
   */
  class RODstatus_t
  {
  public:
    // ctor from time_t timestamps
    RODstatus_t(CAN::GlobalStatus status,
		std::time_t scan_start_arg, std::time_t scan_end_arg,
		std::time_t histo_write_start_arg, std::time_t histo_write_end_arg,
		std::time_t config_end_arg, int histo_download_arg, int loop0_arg)
      : rodstatus(status), scan_start(scan_start_arg), scan_end(scan_end_arg),
	histo_write_start(histo_write_start_arg), histo_write_end(histo_write_end_arg),
	config_end(config_end_arg), histo_download(histo_download_arg), loop0(loop0_arg)
    {}
    // ctor from string time stamps as used in PixScan.cxx
    RODstatus_t(CAN::GlobalStatus status,
		std::string scan_start_arg, std::string scan_end_arg,
		std::string histo_write_start_arg, std::string histo_write_end_arg,
		std::string config_end_arg, int histo_download_arg, int loop0_arg)
      : rodstatus(status), scan_start(stringToTime(scan_start_arg)),
	scan_end(stringToTime(scan_end_arg)),
	histo_write_start(stringToTime(histo_write_start_arg)),
	histo_write_end(stringToTime(histo_write_end_arg)),
	config_end(stringToTime(config_end_arg)),
	histo_download(histo_download_arg),loop0(loop0_arg)
    {}
    const CAN::GlobalStatus status() const {return rodstatus;}
    const std::time_t scanStart() const {return scan_start;}
    const std::time_t scanEnd() const {return scan_end;}
    const std::time_t histogramWriteStart() const {return histo_write_start;}
    const std::time_t histogramWriteEnd() const {return histo_write_end;}
    const std::time_t configEnd() const {return config_end;}
    const int histogramDownloadTime() const {return histo_download;}
    const int loop0Time() const {return loop0;}
    const std::string timeString(time_t t) const;
    const time_t stringToTime(std::string t) const;
    void Print() const;

  private:
    CAN::GlobalStatus rodstatus;
    std::time_t scan_start;
    std::time_t scan_end;
    std::time_t histo_write_start;
    std::time_t histo_write_end;
    std::time_t config_end;
    int histo_download;
    int loop0;
  };



  /** Scan meta data.
   * Information about scan type, quality, start, end times, connectivity tags.
   */
  class ScanInfo_t : public CAN::InfoBase_t
  {
  public:
    ScanInfo_t() : InfoBase_t("ScanInfo"), m_timeScanStart(0), m_timeScanEnd(0), m_comment(""), m_scanTypeName(""), m_scanTypeTag(""), m_scanTypeRevision(0), m_tagsId(""), m_tagsConn(""), m_tagsData(""), m_tagsAlias(""), m_tagsCfg(""), m_tagsCfgRev(0), m_modCfg(""), m_modCfgRev(0), m_quality(CAN::kUndefined), m_file(""), m_altFile(""), m_status(CAN::kNotSet), m_location(CAN::kNowhere), m_pixDisable("") {}

  public:

    //@{
    /** Scan start / end times. */
    const time_t &scanStart() const {return m_timeScanStart;}
    const std::string scanStartString() const {
      std::string time;
      char *ptr = ctime(&m_timeScanStart);
      if (!ptr) return time;
      time = ctime(&m_timeScanStart);
      if (time.size()==0)  return time;
      return time.substr(0,time.size()-1);
    }
    void setScanStart(const time_t &start_time) {m_timeScanStart = start_time;}

    const time_t &scanEnd() const {return m_timeScanEnd;}
    const std::string scanEndString() const {
      std::string time;
      char *ptr = ctime(&m_timeScanEnd);
      if (!ptr) return time;
      time = ctime(&m_timeScanEnd);
      if (time.size()==0)  return time;
      return time.substr(0,time.size()-1);
    }
    void setScanEnd(const time_t &end_time) {m_timeScanEnd = end_time;}
    //@}


    //@{
    /** comment, quality, global status tag, location */
    const std::string &comment() const {return m_comment;}
    void setComment(const std::string &comment) {m_comment = comment;}

    CAN::EQuality quality() const {return m_quality;}
    void setQuality(CAN::EQuality quality) {m_quality=quality;}

    CAN::GlobalStatus status() const {return m_status;}
    void setStatus(CAN::GlobalStatus status) {m_status=status;}

    CAN::ScanLocation location() const {return m_location;}
    void setLocation(CAN::ScanLocation location) {m_location=location;}
    //@}


    //@{
    /** scan preset name with tag and revision. */
    const std::string &scanType() const {return m_scanTypeName;}
    void setScanType(const std::string &scan_type_name) {m_scanTypeName = scan_type_name;}

    const std::string &scanTypeTag() const {return m_scanTypeTag;}
    void setScanTypeTag(const std::string &scan_type_tag) {m_scanTypeTag = scan_type_tag;}

    unsigned int scanTypeRev() const {return m_scanTypeRevision;}
    void setScanTypeRev(unsigned int scan_type_revision) {m_scanTypeRevision = scan_type_revision;}
    //@}


    //@{
    /** Connectivity tags. */
    const std::string &idTag() const {return m_tagsId;}
    void setIdTag(const std::string &id_tag) {m_tagsId = id_tag;}

    const std::string &connTag() const {return m_tagsConn;}
    void setConnTag(const std::string &conn_tag) {m_tagsConn = conn_tag;}

    const std::string &dataTag() const {return m_tagsData;}
    void setDataTag(const std::string &data_tag) {m_tagsData = data_tag;}

    const std::string &aliasTag() const {return m_tagsAlias;}
    void setAliasTag(const std::string &alias_tag) {m_tagsAlias = alias_tag;}

    const std::string &cfgTag() const {return m_tagsCfg;}
    void setCfgTag(const std::string &cfg_tag) {m_tagsCfg = cfg_tag;}

    unsigned int cfgRev() const {return m_tagsCfgRev;}
    void setCfgRev(unsigned int config_revision) {m_tagsCfgRev = config_revision;}
    const std::string &modCfgTag() const {
      if (m_modCfg == "") 
	return m_tagsCfg;
      else
	return m_modCfg;}
    void setModCfgTag(const std::string &cfg_tag) {m_modCfg = cfg_tag;}

    unsigned int modCfgRev() const {
      if (m_modCfgRev == 0)
	return m_tagsCfgRev;
      else
	return m_modCfgRev;}
    void setModCfgRev(unsigned int config_revision) {m_modCfgRev = config_revision;}
    //@}


    //@{
    /** Scan output file locations. */
    const std::string &file() const {return m_file;}
    void setFile(const std::string &file) {m_file = file;}

    const std::string &alternateFile() const {return m_altFile;}
    void setAlternateFile(const std::string &file) {m_altFile = file;}
    //@}

    /** ROD status map. */
    const std::map<std::string, RODstatus_t> & statusMap() const {return  m_rodStatus;}
    /** Get a specific ROD status. */
    const RODstatus_t RODstatus(const std::string &rodname) const;
    /** Add a ROD status to the map. */
    void addRODStatus(std::string rodname, const RODstatus_t &rod) {
      m_rodStatus.insert(std::make_pair(rodname,rod));
    }
    /** Add a ROD status from time_t timestamps. */
    void addRODStatus(std::string rodname, CAN::GlobalStatus status,
		      std::time_t scan_start, std::time_t scan_end,
		      std::time_t histo_write_start, std::time_t histo_write_end,
		      std::time_t config_end, int histo_download, int loop0) {
      const PixLib::RODstatus_t rod(status,scan_start,scan_end,
				    histo_write_start,histo_write_end,
				    config_end,histo_download,loop0);
      m_rodStatus.insert(std::make_pair(rodname,rod));
    }
    /** Add a ROD status from string timestamps. */
    void addRODStatus(std::string rodname, CAN::GlobalStatus status,
		      std::string scan_start, std::string scan_end,
		      std::string histo_write_start, std::string histo_write_end,
		      std::string config_end, int histo_download, int loop0) {
      const PixLib::RODstatus_t rod(status,scan_start,scan_end,
				    histo_write_start,histo_write_end,
				    config_end,histo_download,loop0);
      m_rodStatus.insert(std::make_pair(rodname,rod));
    }

    /** PixDisable map */
    const std::string &pixDisableString() const {return m_pixDisable;}
    void setPixDisableString(const std::string &pixdisable) {m_pixDisable = pixdisable;}

    void Print();

  private:
    time_t m_timeScanStart;
    time_t m_timeScanEnd;
    std::string m_comment;
    std::string m_scanTypeName;
    std::string m_scanTypeTag;
    unsigned int m_scanTypeRevision;
    std::string m_tagsId;
    std::string m_tagsConn;
    std::string m_tagsData;
    std::string m_tagsAlias;
    std::string m_tagsCfg;
    unsigned int m_tagsCfgRev;
    std::string m_modCfg;
    unsigned int m_modCfgRev;
    CAN::EQuality m_quality;
    std::string m_file;
    std::string m_altFile;
    CAN::GlobalStatus m_status;
    CAN::ScanLocation m_location;
    std::map<std::string, RODstatus_t> m_rodStatus;
    std::string m_pixDisable;
  };

}

#endif
