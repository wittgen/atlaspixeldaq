#include "CoralDbClient.hh"
#include "PixMetaException.hh"

#include <cstdlib>
#include <cstring>

#include <string>
#include <iostream>
#include <iomanip>


std::map<std::string, CAN::ScanLocation> s_locationMap;

CAN::ScanLocation parseLocation(const std::string &location_string)
{
  if (s_locationMap.empty()) {
    s_locationMap.insert(std::make_pair(std::string("SR1"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("SR1/ToothPix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("SR1/TOOTHPIX"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("sr1/toothpix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("sr1"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("ToothPix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("toothpix"),CAN::kSR1));
    s_locationMap.insert(std::make_pair(std::string("TOOTHPIX"),CAN::kSR1));

    s_locationMap.insert(std::make_pair(std::string("PIT"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("pit"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("atlas"),CAN::kPIT));
    s_locationMap.insert(std::make_pair(std::string("ATLAS"),CAN::kPIT));

    s_locationMap.insert(std::make_pair(std::string("PixelLab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("pixellab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("pixel_lab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("PIXEL_LAB"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("lab"),CAN::kPixelLab));
    s_locationMap.insert(std::make_pair(std::string("LAB"),CAN::kPixelLab));
  }
  std::map<std::string,CAN::ScanLocation>::const_iterator location_iter = s_locationMap.find(location_string);
  if (location_iter == s_locationMap.end()) {
    return CAN::kNowhere;
  }
  else {
    return location_iter->second;
  }
}

std::map<CAN::ScanLocation, std::string> s_locationNameList;
const std::string &locationName(CAN::ScanLocation a_location) {
  if (s_locationNameList.empty()) {
    s_locationNameList.insert(std::make_pair(CAN::kNowhere,std::string("-")));
    s_locationNameList.insert(std::make_pair(CAN::kSR1,std::string("SR1/ToothPix")));
    s_locationNameList.insert(std::make_pair(CAN::kPIT,std::string("PIT")));
    s_locationNameList.insert(std::make_pair(CAN::kPixelLab,std::string("PixelLab")));
  }
  std::map<CAN::ScanLocation, std::string>::const_iterator location_iter = s_locationNameList.find(a_location);
  if (location_iter != s_locationNameList.end()) {
    return location_iter->second;
  }
  else {
    return s_locationNameList[CAN::kNowhere];
  }
}

void changeLocation(CAN::PixCoralDbClient &coral_client,
		    CAN::SerialNumber_t scan_serial_number,
		    const PixLib::ScanInfo_t &current_scan_info,
		    CAN::ScanLocation location, 
		    bool verbose,
		    bool simulate=true)
{
  if (location!=CAN::kNowhere) {
    if (verbose) {
      std::cout << "INFO [changeLocation] change location from " << locationName(current_scan_info.location()) << " to " << locationName(location) << std::endl;
    }
    if (!simulate) {
      bool ret = coral_client.updateScanMetadataLocation(scan_serial_number, location);
      if (!ret) {
	std::cout << "ERROR [changeLocation] failed to change status for " << std::setw(9) << std::setfill('0') << scan_serial_number << std::setfill(' ')
		  << " to " << locationName(location) << "." << std::endl;
      }
    }
  }
}

void changeModCfgTag(CAN::PixCoralDbClient &coral_client,
		     CAN::SerialNumber_t scan_serial_number,
		     const PixLib::ScanInfo_t &current_scan_info,
		     const std::string &new_tag,
		     bool verbose,
		     bool simulate=true)
{
  if (!new_tag.empty()) {
    if (verbose) {
      std::cout << "INFO [changeModCfgTag] change module config tag from " << current_scan_info.modCfgTag() << " to " << new_tag << std::endl;
    }
    std::string comment;
    if (!simulate) {
      bool ret = coral_client.updateScanMetadataField<std::string>(scan_serial_number, "MODCFGTAG", new_tag);
      if (!ret) {
	std::cout << "ERROR [changeModCfgTag] failed to change module cfg tag for " << std::setw(9) << std::setfill('0') << scan_serial_number << std::setfill(' ')
		  << " to " << new_tag << "." << std::endl;
      }
    }
  }
}

void addComment(CAN::PixCoralDbClient &coral_client,
		CAN::SerialNumber_t scan_serial_number,
		const PixLib::ScanInfo_t &current_scan_info,
		const std::string &add_comment,
		bool verbose,
		bool simulate=true)
{
  if (!add_comment.empty()) {
    std::string comment (current_scan_info.comment());
    if (!comment.empty()) {
      comment += "\n";
    }
    comment += add_comment;
    if (verbose) {
      std::cout << "INFO [addComment] add \"" << add_comment << "\" to \"" << current_scan_info.comment() << "\" : " << std::endl
		<< comment << std::endl;
    }
    if (!simulate) {
      bool ret = coral_client.updateScanMetadataComment(scan_serial_number, comment);

      if (!ret) {
	std::cout << "ERROR [addComment] failed to add cmment for " << std::setw(9) << std::setfill('0') << scan_serial_number << std::setfill(' ')
		  << " to " << comment << "." << std::endl;
      }
    }
  }
}


enum EMakeCommentType {kReplaceTempModCfgTag, kNCommentTypes};
const char *messages[kNCommentTypes]={"Replaced temporary MODCFGTAG ="};

std::string makeComment(EMakeCommentType type, const PixLib::ScanInfo_t &scan_info) {
  std::string comment;
  if (type<kNCommentTypes) {
    comment = messages[type];
    switch(type) {
    case kReplaceTempModCfgTag: {
      comment += " ";
      comment += scan_info.modCfgTag();
      break;
    }
    default:
      break;
    }
  }
  return comment;
}

int main(int argc, char **argv)
{


  std::vector<CAN::SerialNumber_t> serial_number;
  std::string new_file_name;
  std::string new_alt_file_name;
  std::string mod_cfg_tag;
  std::string add_comment;
  std::vector<EMakeCommentType> comment;
  
  bool simulate=true;
  int verbose=1;
  CAN::ScanLocation location = CAN::kNowhere;
  try {

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if ((strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--serial-number")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number.push_back(atoi(argv[++arg_i]));
      }
      else if ((strcmp(argv[arg_i],"-l")==0 || strcmp(argv[arg_i],"--location")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	location = parseLocation(argv[++arg_i]);
      }
      //      else if ((strcmp(argv[arg_i],"--mod-cfg-tag")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
      //	mod_cfg_tag = argv[++arg_i];
      //      }
      else if ((strcmp(argv[arg_i],"--replace-temp-mod-cfg-tag")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	mod_cfg_tag = argv[++arg_i];
	comment.push_back(kReplaceTempModCfgTag);
      }
      else if ((strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"--execute")==0)) {
	simulate=false;
      }
      else if ((strcmp(argv[arg_i],"--add-comment")==0 || strcmp(argv[arg_i],"-m")==0)  && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	add_comment=argv[++arg_i];
      }
      else if ((strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
	verbose++;
      }
      else if ((strcmp(argv[arg_i],"-q")==0 || strcmp(argv[arg_i],"--quiet")==0)) {
	verbose=0;
      }
      else {
	std::cerr << "ERROR ["<<argv[0]<<":main] unhandled argument " << argv[arg_i] << std::endl;
	std::cout << std::endl
		  << "USAGE: -s serial number [-s ... ] [-l/--location SR1/PIT/LAB] [--replace-temp-mod-cfg-tag new-tag] [--add-comment comment]" 
		  << std::endl;
	return -1;
      }
    }
    if (serial_number.empty() ) return -1;

    CAN::PixCoralDbClient::fixMode();
    CAN::PixCoralDbClient coral_client(true);
    
    // Get metadata for scan anal_id
    for (std::vector<CAN::SerialNumber_t>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter) {
    try {
      PixLib::ScanInfo_t scan_info;
      if (verbose>0 || !add_comment.empty() || !comment.empty()) {
	scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(*serial_number_iter);
      }

      if (!add_comment.empty()) addComment(coral_client, *serial_number_iter, scan_info, add_comment, verbose>0, simulate);
      for (std::vector<EMakeCommentType>::const_iterator comment_type_iter = comment.begin();
	   comment_type_iter != comment.end();
	   ++comment_type_iter) {
	addComment(coral_client, *serial_number_iter, scan_info, makeComment(*comment_type_iter, scan_info), verbose>0, simulate);
      }

      if (location != CAN::kNowhere) changeLocation(coral_client, *serial_number_iter, scan_info, location, verbose>0, simulate);
      if (!mod_cfg_tag.empty()) changeModCfgTag(coral_client, *serial_number_iter, scan_info, mod_cfg_tag, verbose>0, simulate);

      if (verbose>1) {
	scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(*serial_number_iter);
	scan_info.Print();
      }
    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    }
    }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}
                                                      



