#include <stdlib.h> 
#include "CoralDbClient.hh"
#include "PixMetaException.hh"
int main(int argc, char **argv){
  //printf("CORAL-create starting \n");

    CAN::SerialNumber_t anal_id = 123456789;
    if (argc > 1)  anal_id = atoi(argv[1]);


  try {

    // Start CORAL client
    printf("CORAL-read starting \n");
//     PixCoralClient coralClient(coralConnStr,app.context());
//     CAN::PixCoralDbClient coralClient(app.context(), true);
    CAN::PixCoralDbClient coralClient(true);

    // Show the structure of the master table
    try {
      coralClient.printTableDesc("CALIB_META_ANAL");
      //coralClient.printTableContent("CALIB_META_ANAL");
    } catch (coral::TableNotExistingException& err) {
      std::cout<<"TableNotExistingException: "<<err.what()<<std::endl;
    }

    // Get metadata for analysis anal_id
    try {
      coralClient.getMetadata<CAN::AnalysisInfo_t>(anal_id).Print();
//       CAN::ObjectInfo_t alg("updated algo","updated tag",3);
//       CAN::ObjectInfo_t clas("updated clas","updated tag",3);
//       CAN::ObjectInfo_t post("updated post","updated tag2",3);
//       CAN::AnalysisInfo_t info(alg,clas,post,1);
//       time_t t = time (NULL);
//       info.setAnalysisStart(t);
//       t += 10;
//       info.setAnalysisEnd(t);
//       //Update metadata
//       bool status = coralClient.updateMetadata<CAN::AnalysisInfo_t>(anal_id, &info);
//       coralClient.getMetadata<CAN::AnalysisInfo_t>(anal_id).Print();
    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    }

    // Get metadata for scan anal_id
    try {
      coralClient.getMetadata<PixLib::ScanInfo_t>(anal_id, /*getRODstatus = */ true).Print();
//       PixLib::ScanInfo_t scan_info;
//       scan_info.setScanStart(time(0));
//       scan_info.setScanType("Newtype");
//       scan_info.setScanTypeTag("Newtag");
//       scan_info.setScanTypeRev(2);
//       scan_info.setIdTag("notCT");
//       scan_info.setConnTag("YCc");
//       scan_info.setDataTag("YCd");
//       scan_info.setAliasTag("YCa");
//       scan_info.setCfgTag("YCc");
//       scan_info.setCfgRev(2);
//       scan_info.setQuality(CAN::kDebug);
//       scan_info.setComment("YCcomment");
//       scan_info.setStatus(CAN::kFailure);
//       scan_info.setFile("YCfile");
//       scan_info.setAlternateFile("YCafile");
//       scan_info.setScanEnd(time(0));
//       coralClient.updateMetadata<PixLib::ScanInfo_t>(anal_id, &scan_info);
//       coralClient.getMetadata<PixLib::ScanInfo_t>(anal_id).Print();
    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}
                                                      



