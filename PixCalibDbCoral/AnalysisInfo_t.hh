#ifndef _AnalysisInfo_t_hh_
#define _AnalysisInfo_t_hh_

#include "InfoBase_t.hh"
#include "Common.hh"
#include <cassert>
#include <vector>
#include <map>
#include <time.h>

namespace CAN {

  typedef std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> > SerialNumbers_t; //CLA

  class MetaDataServicce;
  class AnalysisInfo_t;
  //  class Scheduler_impl;
  class ObjectInfo_t;

  void copyAnalysisInfo(const AnalysisInfo_t &src, AnalysisInfoData_t &dest);
  void copyAnalysisInfo(const AnalysisInfoData_t &src, AnalysisInfo_t &dest);
  void copyObjectInfo(const ObjectInfoData_t &src, ObjectInfo_t &dest);

  /** Data which describes the object of an analysis and its parameters.
   */
  class ObjectInfo_t {
  public:
    friend void copyObjectInfo(const ObjectInfoData_t &src, ObjectInfo_t &dest);
    friend class AnalysisInfo_t;

    ObjectInfo_t() {};

    ObjectInfo_t(const std::string &name, const std::string &tag, Revision_t revision) 
      : m_name(name), m_tag(tag), m_revision(revision) {};

    /** Returns the class name of the object.
     */
    const std::string &name() const {return m_name;}
    
    /** Returns the database tag for the parameters.
     */
    const std::string &tag()  const {return m_tag;}

    /** Returns the parameter set revision.
     */
    const Revision_t &revision()     const {return m_revision;}

  protected:
    /** Set the parameters of the object should only be used when translating the equivalent CORBA object.
     */
    void set(const std::string &name, const std::string &tag, Revision_t revision) 
    {m_name = name; m_tag = tag; m_revision = revision;}

  private:
    std::string m_name;
    std::string m_tag;
    Revision_t  m_revision;
  };

  /** Data to describe an analysis.
   * An analysis is described by the analysis name,
   * and its parameters by the name the tag and the revision.
   * The src data is defined by the source serial number and 
   * type.
   */
    class AnalysisInfo_t : public InfoBase_t
    {
      //friend class MetaDataService;
      friend void copyAnalysisInfo(const CAN::AnalysisInfoData_t &src, CAN::AnalysisInfo_t &dest);
      friend void copyAnalysisInfo(const CAN::AnalysisInfo_t &src, CAN::AnalysisInfoData_t &dest);
      //      friend class Scheduler_impl;

      //    protected:
    public:

      /** Creates a half initialised class.
       * This constructor should only be used just before reading the config from the database.
       */
      AnalysisInfo_t() : InfoBase_t("AnalysisInfo") { init(); }

      AnalysisInfo_t(const ObjectInfo_t &analysis_algorithm,
		     const ObjectInfo_t &classification,
		     const ObjectInfo_t &post_processing,
		     SerialNumber_t scan_serial_number,
		     SerialNumber_t src_analysis_serial_number=0,
                     EQuality quality = CAN::kUndefined,
                     std::string comment = "",
                     time_t timeAnalysisStart = 0,
                     time_t timeAnalysisEnd = 0,
                     std::string file = "",
                     std::string altFile = "",
		     CAN::GlobalStatus status = CAN::kNotSet)
	: InfoBase_t("AnalysisInfo"),
	  m_scanSerialNumber(scan_serial_number),
	  m_srcAnalysisSerialNumber(src_analysis_serial_number),
          m_quality(quality),
          m_comment(comment),
          m_timeAnalysisStart(timeAnalysisStart),
          m_timeAnalysisEnd(timeAnalysisEnd),
          m_file(file),
          m_altFile(altFile),
	  m_status(status)
      {
	init();
	assert( kNObjectTypes == 3);

	m_objectInfo[kAlg]=analysis_algorithm;
	m_objectInfo[kClassification]=classification;
	m_objectInfo[kPostProcessing]=post_processing;

	addAnonymousSource();
      }


      /** Get the serial number of the source.
       */
      SerialNumber_t    scanSerialNumber() const {return m_scanSerialNumber;}

      SerialNumbers_t scanSerialNumbers() const { return m_multiSources; } //CLA

      /** Ge the serial number of the source analysis if any or zeror.
       */
      SerialNumber_t    srcAnalysisSerialNumber() const {return m_srcAnalysisSerialNumber;}

      /** Return true if analysis results are analysed instead of a scan.
       */
      bool isAnalysisOfAnAnalysis() const {return m_srcAnalysisSerialNumber != 0; }

      /** Get the information about name, tag an revision for the given object type.
       */
      const ObjectInfo_t &objectInfo(EObjectType type)    const {return m_objectInfo[type];}

      /** Get the name of an analysis.
       * The name is a unique identifier of the analysis class.
       */
      const std::string &name(EObjectType type)           const {return m_objectInfo[type].name();}

      /** Get the tag for the analysis parameters and the analysis class version.
       */
      const std::string &tag(EObjectType type)            const {return m_objectInfo[type].tag();}

      /** Get the revision of the analysis parameters and the analysis class.
       */
      Revision_t  revision(EObjectType type)              const {return m_objectInfo[type].revision(); }

      //@{
      /** comment, quality, global status tag */
      const std::string &comment() const {return m_comment;}
      void setComment(const std::string &comment) {m_comment = comment;}

      EQuality quality() const {return m_quality;}
      void setQuality(EQuality quality) {m_quality=quality;}

      CAN::GlobalStatus status() const {return m_status;}
      void setStatus(CAN::GlobalStatus status) {m_status=status;}
      //@}

      //@{
      /** Analysis start / end times. */
      const time_t &analysisStart() const {return m_timeAnalysisStart;}
      const std::string analysisStartString() const {
	::time_t long_start_time=m_timeAnalysisStart;
	std::string time = ctime(&long_start_time);
	return time.substr(0,time.size()-1);
      }
      void setAnalysisStart(const time_t &start_time) {m_timeAnalysisStart = start_time;}

      const time_t &analysisEnd() const {return m_timeAnalysisEnd;}
      const std::string analysisEndString() const {
	::time_t long_end_time=m_timeAnalysisEnd;
	std::string time = ctime(&long_end_time);
	return time.substr(0,time.size()-1);
      }
      void setAnalysisEnd(const time_t &end_time) {m_timeAnalysisEnd = end_time;}
      //@}


      //@{
      /** Analysis output file locations. */
      const std::string &file() const {return m_file;}
      void setFile(const std::string &file) {m_file = file;}

      const std::string &alternateFile() const {return m_altFile;}
      void setAlternateFile(const std::string &file) {m_altFile = file;}
      //@}

      /** Get the serial number and type assigned to the given name.
       * Will throw an exception (std::runtime) in case the source does not exist.
       */
      const std::pair<ESerialNumberType, SerialNumber_t> &source(const std::string &name) const;

      /** Get the begin iterator for the sources.
       */
      std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator beginSource() const  { return m_multiSources.begin(); }

      /** Get the end iterator for the sources.
       */
      std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> >::const_iterator endSource() const  { return m_multiSources.end(); }

      /** Add the given serial number as source with the given name.
       * When ever one named source is added also the original scan or analysis which 
       * given to the constructor must be named.
       */
      void addSource( const std::string name, ESerialNumberType type, SerialNumber_t serial_number);

      /** Check the analysis info for errors. 
       */
      bool check() const;

      /** Create a string which contains the name and numbers of all the analysis sources.
       * string format:  hvoff:S000001009,hvon:S000001010 or new:S00001009,old:A00001008.
       */
      std::string createSourceString() const;

      /** Set the list of sources from the given string.
       * string format:  hvoff:S000001009,hvon:S000001010 or new:S00001009,old:A00001008.
       */
      void setSourcesFromString(const char *source_string);

      /** Print the content of the analysis metadata
       */
      void Print();

    protected:
      void init();

      ObjectInfo_t &object(EObjectType type)
        { assert(static_cast<unsigned int>(type) < m_objectInfo.size() ); return m_objectInfo[type]; }

      void setScanSerialNumber(SerialNumber_t scan_serial_number)
        { m_scanSerialNumber = scan_serial_number; }

      void setSrcAnalysisSerialNumber(SerialNumber_t src_analysis_serial_number)
        { m_srcAnalysisSerialNumber = src_analysis_serial_number; }

    private:
      void addAnonymousSource();

      std::vector<ObjectInfo_t> m_objectInfo; // should this be a vector ?
      SerialNumber_t      m_scanSerialNumber;
      SerialNumber_t      m_srcAnalysisSerialNumber;
      EQuality m_quality;
      std::string m_comment;
      time_t m_timeAnalysisStart;
      time_t m_timeAnalysisEnd;
      std::string m_file;
      std::string m_altFile;
      CAN::GlobalStatus m_status;
      std::map<std::string, std::pair<ESerialNumberType, SerialNumber_t> > m_multiSources;

      static const char  *s_objectName[kNObjectTypes];
    };

}

#endif
