#ifndef _PixCoralDbMetaDataService_h_
#define _PixCoralDbMetaDataService_h_

#include "Common.hh"
#include "InfoBase_t.hh"
#include "ScanInfo_t.hh"
#include "AnalysisInfo_t.hh"
#include "IMetaDataService.hh"
#include <map>
#include "CoralDbClient.hh"

namespace CAN {

  class PixCoralDbMetaDataService;

  template <class T>
  class PixCoralDbInfoPtr_t : public InfoPtr_t<T>
  {
    friend class PixCoralDbMetaDataService;
  protected:
    PixCoralDbInfoPtr_t(const std::shared_ptr<InfoBase_t> &a) : InfoPtr_t<T>(a) {}
    PixCoralDbInfoPtr_t(InfoBase_t *a) : InfoPtr_t<T>(a) {}
  
  };

  class PixCoralDbMetaDataService : public IMetaDataService
  {
  public:
    PixCoralDbMetaDataService();

    const CAN::SerialNumber_t newSerialNumber(ESerialNumberType type);

    const InfoPtr_t<PixLib::ScanInfo_t> getScanInfo(SerialNumber_t scan_serial_number) {return info<PixLib::ScanInfo_t>(scan_serial_number);}

    const InfoPtr_t<CAN::AnalysisInfo_t> getAnalysisInfo(SerialNumber_t analysis_serial_number) {return info<CAN::AnalysisInfo_t>(analysis_serial_number);}

    //    const InfoPtr_t<PixLib::ScanInfo_t> getScanInfo(ESerialNumberType src_serial_number_type, SerialNumber_t src_serial_number);

    void addInfo(CAN::SerialNumber_t serial_number, const CAN::InfoPtr_t<PixLib::ScanInfo_t> &info) {
     _addInfo<PixLib::ScanInfo_t>(serial_number,info);
    }

    void addInfo(CAN::SerialNumber_t serial_number, const CAN::InfoPtr_t<CAN::AnalysisInfo_t>&info) {
     _addInfo<CAN::AnalysisInfo_t>(serial_number,info);
    }

    bool setFinalScanStatus(SerialNumber_t scan_serial_number, CAN::GlobalStatus final_status, const time_t *end_time=NULL);

    bool setFinalAnalysisStatus(SerialNumber_t analysis_serial_number, CAN::GlobalStatus final_status, const time_t *end_time=NULL);

    /** Cleanup the info cache.
     * Will only delete objects which have not been used recentlu,Should be called in regular intervalles. 
     */
    void cleanup();

    CAN::SerialNumber_t scanSerialNumberForFile(const std::string &file);

  private:
    template <class T> static  ESerialNumberType infoType();
    typedef std::map<SerialNumber_t, std::pair<unsigned int, std::shared_ptr<InfoBase_t> > > InfoCacheMap_t;


    template <class T>
    void _addInfo(SerialNumber_t serial_number, const InfoPtr_t<T> &a) {
      writeInfoToDb(infoType<T>(), serial_number, a);
    }


    template <class T>
    InfoPtr_t<T> info(const SerialNumber_t serial_number) {
      PixCoralDbInfoPtr_t<T> ptr(findInfo<T>(serial_number));
      if (!ptr) {
	ptr = readInfo<T>(serial_number);
      }
      return ptr;
    }

    template <class T> 
    InfoPtr_t<T> findInfo(const SerialNumber_t serial_number) {
      InfoCacheMap_t &info_map=m_infoCache[infoType<T>()];
      InfoCacheMap_t::iterator info_iter = info_map.find(serial_number);
      if (info_iter != info_map.end()) {
	info_iter->second.first++;
	return PixCoralDbInfoPtr_t<T>(info_iter->second.second);
      }
      return PixCoralDbInfoPtr_t<T>(static_cast<T *>(NULL));
    }

    template <class T> InfoPtr_t<T> insertInfo(const SerialNumber_t serial_number, const T *new_info_obj) {
      InfoCacheMap_t &info_map=m_infoCache[infoType<T>()];
      return PixCoralDbInfoPtr_t<T>(insertInfo(info_map, serial_number, std::shared_ptr<InfoBase_t>(new_info_obj)));
    }


    std::shared_ptr<InfoBase_t> insertInfo( InfoCacheMap_t &info_map, const SerialNumber_t serial_number, const std::shared_ptr<InfoBase_t> &new_info_obj);

    template <class T>
    InfoPtr_t<T> readInfo(const SerialNumber_t serial_number) {
      return PixCoralDbInfoPtr_t<T>(readInfoFromDb(infoType<T>(),serial_number));
    }

    void getDbHandle(bool update=true);

    std::shared_ptr<InfoBase_t> readInfoFromDb(ESerialNumberType type, const SerialNumber_t serial_number);

    std::shared_ptr<InfoBase_t> writeInfoToDb(ESerialNumberType type, const SerialNumber_t serial_number, const std::shared_ptr<InfoBase_t> &info_ptr);

    static const unsigned short s_nSerialNumberTypes = 2;

    static const char *s_dataBaseNames[s_nSerialNumberTypes];
    static const char *s_serialNumberHeader[s_nSerialNumberTypes];
    static const char *s_infoClassName[s_nSerialNumberTypes];
    std::string m_pathName;

    InfoCacheMap_t m_infoCache[s_nSerialNumberTypes];
    std::shared_ptr<CAN::PixCoralDbClient> m_db;
    bool           m_isUpdateConnection;

    static const char *s_metaDbEnvName;
  };

  template <> inline ESerialNumberType PixCoralDbMetaDataService::infoType<PixLib::ScanInfo_t>() {return kScanNumber; }
  template <> inline ESerialNumberType PixCoralDbMetaDataService::infoType<CAN::AnalysisInfo_t>() {return kAnalysisNumber; }

}
#endif
