#include "AnalysisInfo_t.hh"
#include "PixMetaException.hh"
#include <iostream>

int main()
{
  for (unsigned int pass_i=0; pass_i<4; pass_i++) {
    std::cout << "INFO [test_multipleSources] pass = " << pass_i << std::endl;
    CAN::AnalysisInfo_t info(CAN::ObjectInfo_t("test","test",0),
			     CAN::ObjectInfo_t("test","test",0),
			     CAN::ObjectInfo_t("test","test",0),
			     (pass_i%2==0 ? 3 : 0),
			     (pass_i%2==1 ? 1 : 0)
			     );
    if (pass_i>1) {
      info.addSource("hvon",CAN::kScanNumber,2);
      info.addSource("hvoff",CAN::kScanNumber,3);
      info.addSource("old",CAN::kAnalysisNumber,1);
    }

    info.Print();
    assert(info.check());

    CAN::AnalysisInfoData_t info_data;
    CAN::copyAnalysisInfo(info, info_data);

    CAN::AnalysisInfo_t info_dest;
    CAN::copyAnalysisInfo(info_data, info_dest);

    info_dest.Print();

    assert(info_dest.check());
    std::cout << info.createSourceString() << " == " << info_dest.createSourceString() << std::endl;
    assert( info.createSourceString() == info_dest.createSourceString());
    if (pass_i>1) {
      try {
	{
	  const std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> &hv_on = info_dest.source("hvon");
	  assert( hv_on.first == CAN::kScanNumber && hv_on.second == 2);
	}

	{
	  const std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> &hv_off = info_dest.source("hvoff");
	  assert( hv_off.first == CAN::kScanNumber && hv_off.second == 3);
	  if (pass_i==2) {
	    assert( hv_off.second == info_dest.scanSerialNumber() );
	  }
	}

	{
	  const std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> &old = info_dest.source("old");
	  assert( old.first == CAN::kAnalysisNumber && old.second == 1);
	  if (pass_i==3) {
	    assert( old.second == info_dest.srcAnalysisSerialNumber() );
	  }
	}
      }
      catch (CAN::PixMetaException &err) {
	std::cerr << "ERROR [test_multipleSources] Caught exception : " << err.what() << std::endl;
	assert(false);
      }
      catch (...) {
	assert(false);
      }
    }
    try {
      if (pass_i==0) {
	const std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> &scan = info_dest.source("");
	assert( scan.first == CAN::kScanNumber && scan.second == 3);
	assert( scan.second == info_dest.scanSerialNumber() );
      }
      if (pass_i==1) {
	const std::pair<CAN::ESerialNumberType, CAN::SerialNumber_t> &analysis= info_dest.source("");
	assert( analysis.first == CAN::kAnalysisNumber && analysis.second == 1);
	assert( analysis.second == info_dest.srcAnalysisSerialNumber() );
      }
    }
    catch (CAN::PixMetaException &err) {
      std::cerr << "ERROR [test_multipleSources] Caught exception : " << err.what() << std::endl;
      assert(false);
    }
    catch (...) {
      assert(false);
    }
  }
}
