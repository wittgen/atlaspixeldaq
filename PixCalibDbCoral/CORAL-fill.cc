#include <stdlib.h>
#include "CoralDbClient.hh"

int main(int argc, char **argv){
  //printf("CORAL-fill starting \n");

    CAN::SerialNumber_t anal_id;
    CAN::SerialNumber_t scanid = 1001;
    if (argc > 1)  scanid = atoi(argv[1]);



  try {

    // Start CORAL client
    printf("CORAL-fill starting \n");

    CAN::PixCoralDbClient coralClient(true);

    anal_id = coralClient.getNewSerialNumber(CAN::kScanNumber);
    PixLib::ScanInfo_t info;
    info.setScanType("YCscan");
    info.setScanTypeTag("test");
    info.setScanTypeRev(1);
    info.setQuality(CAN::kNormal);
    info.setComment("blah blah");
    info.setIdTag("CT");
    info.setConnTag("BiStave_5");
    info.setDataTag("BiStave_5");
    info.setAliasTag("BiStave_5");
    info.setCfgTag("DataTaking-4000e-ST");
    info.setCfgRev(0);
    time_t t = time (NULL);
    info.setScanStart(t);
    t += 10;
    info.setScanEnd(t);
    info.setFile("local");
    info.setAlternateFile("permanent");
    info.addRODStatus("from fill strings",CAN::kFailure,"2001-02-03 04:05:06","2002-02-03 04:05:06","2001-04-03 04:05:06","2001-02-06 04:05:06","2001-02-03 08:05:06",7266,276);
    info.Print();
    coralClient.storeMetadata<PixLib::ScanInfo_t>(anal_id, &info);

    CAN::ObjectInfo_t alg("algo","tag",1);
    CAN::ObjectInfo_t clas("clas","tag",1);
    CAN::ObjectInfo_t post("post","tag2",2);

    anal_id = coralClient.getNewSerialNumber(CAN::kAnalysisNumber);
    CAN::AnalysisInfo_t info2(alg,clas,post,scanid);
    info2.setQuality(CAN::kNormal);
    info2.setComment("blah blah");
    t = time (NULL);
    info2.setAnalysisStart(t);
    t += 10;
    info2.setAnalysisEnd(t);
    info2.addSource("hvon",CAN::kScanNumber,2);
    coralClient.storeMetadata<CAN::AnalysisInfo_t>(anal_id, &info2);
  }
  catch ( coral::DataEditorException &e) {
    std::cout<<"DataEditorException: "<<e.what()<<std::endl;
  }
  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  std::cout<<"Done"<<std::endl;
  return 0;

}
                                                      



