#include "MetaDataService.hh"
#include "PixCoralDbMetaDataService.hh"
//#include "PixDbMetaDataService.hh"

namespace CAN {

  IMetaDataService *MetaDataService::s_instance=NULL;


   IMetaDataService *MetaDataService::instance() 
   {
      if (!s_instance) {
	s_instance = new PixCoralDbMetaDataService;
	//        s_instance = new PixDbMetaDataService;
      }
      return s_instance;
   }

  void MetaDataService::deepCleanup() {
    if (s_instance) {
      s_instance->cleanup();
      delete s_instance ;
      s_instance = NULL;
    }

  }


}

