#include "PixCoralDbMetaDataService.hh"

//#include <mrs/message.h>

#include <sstream>
#include <iomanip>

//#include "exception.hh"
#include "PixMetaException.hh"

namespace CAN {

const char *PixCoralDbMetaDataService::s_dataBaseNames[s_nSerialNumberTypes]={
  "ScanCatalog",
  "AnalysisCatalog"
};

const char *PixCoralDbMetaDataService::s_serialNumberHeader[s_nSerialNumberTypes]={
  "S",
  "A"
};

const char *PixCoralDbMetaDataService::s_infoClassName[s_nSerialNumberTypes]={
  "ScanInfo_t",
  "AnalysisInfo_t"
};

const char *PixCoralDbMetaDataService::s_metaDbEnvName="PIX_METADATA_DB";

PixCoralDbMetaDataService::PixCoralDbMetaDataService() 
  : m_isUpdateConnection(false)
{

  const char *ptr=getenv(s_metaDbEnvName);
  if (!ptr) {
    std::stringstream message;
    message << "CAN::PixCoralDbMetaDataService::PixCoralDbMetaDataService:  Path name for meta data is unknown. Please set the environment variable " << s_metaDbEnvName << " to the correct path name.";
//     throw MRSException("NOMETADATAPATH", MRS_FATAL, "CAN::PixCoralDbMetaDataService::instance",message.str());
    throw PixMetaException(message.str());
  }
  m_pathName=ptr;

  //    if (!m_pathName.empty()) {
  //      if (m_pathName[m_pathName.size()-1]!='/') {
  //	m_pathName+='/';
  //      }
  //    }
}


void PixCoralDbMetaDataService::cleanup() {
  unsigned int counter_sum=0;
  unsigned int n_counter=0;
  // delete all cached info objects which have not been used since last cleanup.
  for (unsigned short serial_type_i = 0; serial_type_i < s_nSerialNumberTypes; serial_type_i++) {
    for (InfoCacheMap_t::iterator cache_iter = m_infoCache[serial_type_i].begin();
	 cache_iter != m_infoCache[serial_type_i].end();
	 cache_iter++) {
      if (cache_iter->second.first == 0) {
	m_infoCache[serial_type_i].erase(cache_iter);
      }
      else {
	n_counter++;
	counter_sum += cache_iter->second.first;
      }
    }
  }
  if (n_counter>0) {
    // Mark all objects below the average use counts for cleanup in next round.
    // @todo Is this a useful idea ? Or should all objects be marked for cleanup ?
    counter_sum = ( counter_sum + n_counter-1 ) / n_counter;
    for (unsigned short serial_type_i = 0; serial_type_i < s_nSerialNumberTypes; serial_type_i++) {
      for (InfoCacheMap_t::iterator cache_iter = m_infoCache[serial_type_i].begin();
	   cache_iter != m_infoCache[serial_type_i].end();
	   cache_iter++) {
	if (cache_iter->second.first <= counter_sum) {
	  cache_iter->second.first=0;
	}
      }
    }
  }
}

void PixCoralDbMetaDataService::getDbHandle(bool update){
  if (!m_db || (update && !m_isUpdateConnection)) {
    if (m_pathName.compare(0,strlen("oracle:"),"oracle:")!=0 &&
	m_pathName.compare(0,strlen("ORACLE:"),"ORACLE:")!=0 &&
	m_pathName.compare(0,strlen("sqlite_file:"),"sqlite_file:")!=0 &&
	m_pathName.compare(0,strlen("SQLITE_FILE:"),"SQLITE_FILE:")!=0) {
      std::string message("CAN::PixCoralDbMetaDataService::getDbHandle: Tried to access ");
      message += m_pathName;
      message += ". Cannot deal with DB formats other than oracle or sqlite. Use CAN::PixDbMetaDataService instead or change PIX_METADATA_DB";
//       throw MRSException("METASVC_db_open_failed", MRS_FATAL, "CAN::PixCoralDbMetaDataService::getDbHandle",message);
      throw PixMetaException(message);
    }

    try {
      m_db = std::make_shared<CAN::PixCoralDbClient>(/*verbose = */ true, /*update*/ update);
      if (!m_db) {
	std::string message("CAN::PixCoralDbMetaDataService::getDbHandle: Failed to acquire database handle for ");
	message += m_pathName;
// 	throw MRSException("METASVC_db_open_failed", MRS_FATAL, "CAN::PixCoralDbMetaDataService::getDbHandle",message);
      throw PixMetaException(message);
      }
      m_isUpdateConnection = update;
    }
    catch (std::exception &err) {
      std::stringstream message;
      message << "CAN::PixCoralDbMetaDataService::getDbHandle: Failed to acquire database handle for "
	      << m_pathName
	      << ". Reason exception: " << err.what() ;
//       throw MRSException("METASVC_db_open_failed", MRS_FATAL, "CAN::PixCoralDbMetaDataService::getDbHandle",message.str());
      throw PixMetaException(message.str());
    }
  }
}






std::shared_ptr<InfoBase_t> PixCoralDbMetaDataService::readInfoFromDb(ESerialNumberType type, const SerialNumber_t serial_number) 
{
  getDbHandle(false);
  std::shared_ptr<InfoBase_t> info_ptr=nullptr;
  switch (type) {
  case kScanNumber:
    info_ptr=std::shared_ptr<InfoBase_t>(new PixLib::ScanInfo_t(m_db->getMetadata<PixLib::ScanInfo_t>(serial_number)));
    break;
  case kAnalysisNumber:
    info_ptr=std::shared_ptr<InfoBase_t>(new AnalysisInfo_t(m_db->getMetadata<CAN::AnalysisInfo_t>(serial_number)));
    break;
  default:
    break; //FIXME
  }
  return insertInfo(m_infoCache[type],serial_number,info_ptr);
}





std::shared_ptr<InfoBase_t> PixCoralDbMetaDataService::writeInfoToDb(ESerialNumberType type, const SerialNumber_t serial_number, const std::shared_ptr<InfoBase_t> &info_ptr)
{
  assert(type < s_nSerialNumberTypes);
  // search for serial number 
  getDbHandle(true);

  switch (type) {
  case kScanNumber:
    m_db->storeMetadata<PixLib::ScanInfo_t>(serial_number,static_cast<PixLib::ScanInfo_t *>(info_ptr.get()));
    break;
  case kAnalysisNumber:
    m_db->storeMetadata<CAN::AnalysisInfo_t>(serial_number,static_cast<CAN::AnalysisInfo_t *>(info_ptr.get()));
    break;
  default:
    break; //FIXME should not happen
  }
  return insertInfo(m_infoCache[type],serial_number,info_ptr);
}





std::shared_ptr<InfoBase_t> PixCoralDbMetaDataService::insertInfo( InfoCacheMap_t &info_map, const SerialNumber_t serial_number, const std::shared_ptr<InfoBase_t> &new_info_obj) {
  std::pair<InfoCacheMap_t::const_iterator,bool> 
    ret = info_map.insert(std::make_pair(serial_number,
					 std::make_pair<unsigned int, std::shared_ptr<InfoBase_t> >(1, (std::shared_ptr<InfoBase_t>)new_info_obj)));
  if (!ret.second) {
    std::stringstream message;
    message << "CAN::PixCoralDbMetaDataService::insertInfo: Failed to insert info object for serial number " << serial_number << ".";
//     throw MRSException("MAPINSFAILED", MRS_FATAL, "CAN::PixCoralDbMetaDataService::insertInfo",message.str());
      throw PixMetaException(message.str());
  }
  return ret.first->second.second;
}



CAN::SerialNumber_t PixCoralDbMetaDataService::scanSerialNumberForFile(const std::string &file)
{
  getDbHandle(false);
  return m_db->scanSerialNumberForFile(file);
}


const CAN::SerialNumber_t PixCoralDbMetaDataService::newSerialNumber(ESerialNumberType type) {
  getDbHandle(true);
  return m_db->getNewSerialNumber(type);
}

bool PixCoralDbMetaDataService::setFinalScanStatus(SerialNumber_t scan_serial_number, CAN::GlobalStatus final_status, const time_t *end_time) {

  time_t current_time;
  if (!end_time) {
    current_time = time(NULL);
    end_time = &current_time;
  }
  getDbHandle(true);
  return m_db->updateScanMetadataStatusAndEndTime(scan_serial_number, final_status, *end_time);

}

bool PixCoralDbMetaDataService::setFinalAnalysisStatus(SerialNumber_t analysis_serial_number, CAN::GlobalStatus final_status, const time_t *end_time) {

  time_t current_time;
  if (!end_time) {
    current_time = time(NULL);
    end_time = &current_time;
  }
  getDbHandle(true);
  return m_db->updateAnalysisMetadataStatusAndEndTime(analysis_serial_number, final_status, *end_time);

}

} // namespace CAN
