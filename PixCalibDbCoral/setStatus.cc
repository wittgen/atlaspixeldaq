#include "CoralDbClient.hh"
#include "PixMetaException.hh"

#include <cstring>

#include <iomanip>


std::map<std::string, CAN::GlobalStatus> s_status;

CAN::GlobalStatus parseStatus(const std::string &status_name) {
  if (s_status.empty()) {
  s_status["SUCCESS"]=CAN::kSuccess;
  s_status["Success"]=CAN::kSuccess;
  s_status["success"]=CAN::kSuccess;
  s_status["s"]=CAN::kSuccess;
  s_status["S"]=CAN::kSuccess;
  s_status["t"]=CAN::kSuccess;
  s_status["1"]=CAN::kSuccess;

  s_status["FAILURE"]=CAN::kFailure;
  s_status["Failure"]=CAN::kFailure;
  s_status["failure"]=CAN::kFailure;
  s_status["F"]=CAN::kFailure;
  s_status["f"]=CAN::kFailure;
  s_status["0"]=CAN::kFailure;

  s_status["ABORTED"]=CAN::kAborted;
  s_status["Aborted"]=CAN::kAborted;
  s_status["aborted"]=CAN::kAborted;
  s_status["A"]=CAN::kAborted;
  s_status["a"]=CAN::kAborted;

  s_status["Unknown"]=CAN::kUnknown;
  s_status["unknown"]=CAN::kUnknown;
  s_status["?"]=CAN::kUnknown;
  }
  std::map<std::string, CAN::GlobalStatus>::const_iterator status_iter = s_status.find(status_name);
  if (status_iter != s_status.end()) {
    return status_iter->second;
  }
  return CAN::kNotSet;
}


std::map<CAN::GlobalStatus,std::string>      s_statusNameList;

std::string statusName(CAN::GlobalStatus status)
{
  if (s_statusNameList.empty()) {
    s_statusNameList.insert(std::make_pair(CAN::kNotSet,std::string("Not-Set")));
    s_statusNameList.insert(std::make_pair(CAN::kSuccess,std::string("Success")));
    s_statusNameList.insert(std::make_pair(CAN::kFailure,std::string("Failure")));
    s_statusNameList.insert(std::make_pair(CAN::kAborted,std::string("Aborted")));
  }

  std::map<CAN::GlobalStatus,std::string>::const_iterator status_iter = s_statusNameList.find(status);
  if (status_iter != s_statusNameList.end()) {
    return status_iter->second;
  }
  else {
    return "Unknown";
  }
}

int main(int argc, char **argv)
{


  std::vector<CAN::SerialNumber_t> serial_number;
  std::string new_file_name;
  std::string new_alt_file_name;
  bool simulate=true;
  bool verbose=false;
  bool analysis_status=false;
  bool administrate=false;
  CAN::GlobalStatus status = CAN::kNotSet;
  try {

    for (int arg_i=1; arg_i<argc; arg_i++) {
      if ((strcmp(argv[arg_i],"-s")==0 || strcmp(argv[arg_i],"--serial-number")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	serial_number.push_back(atoi(argv[++arg_i]));
      }
      if ((strcmp(argv[arg_i],"-t")==0 || strcmp(argv[arg_i],"--status")==0) && arg_i+1<argc && argv[arg_i+1][0]!='-') {
	status = parseStatus(argv[++arg_i]);
      }
      else if ((strcmp(argv[arg_i],"-x")==0 || strcmp(argv[arg_i],"--execute")==0)) {
	simulate=false;
      }
      else if ((strcmp(argv[arg_i],"-a")==0 || strcmp(argv[arg_i],"--analysis")==0)) {
	analysis_status=true;
      }
      else if ((strcmp(argv[arg_i],"-v")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
	verbose=true;
      }
       else if ((strcmp(argv[arg_i],"--administrate")==0 || strcmp(argv[arg_i],"--verbose")==0)) {
	administrate=true;
      }
   }
   if (serial_number.empty() || (status==CAN::kNotSet)) {
       std::cout << "USAGE " << argv[0] << " [-v] -t {Success, Failure, Aborted, Unknown} [-x] [-a] -s serial-nr [-s ...] " << std::endl
	         << std::endl
                 << "-v\t verbose" << std::endl
                 << "-a\t analysis instead of a scan." << std::endl
                 << "-x\t execute the operation." << std::endl
                 << "-s serial-nr\tserial number of a scan (analysis) for which the status should be changed, can be used multiple times." << std::endl
                 << "-t status-name\tthe new status : Success, Failure, Aborted, Unknown." << std::endl
                 << std::endl
                 << "NOTE: The status will only be changed if -x is specified."
                 << std::endl;
        return -1;
   }

    CAN::PixCoralDbClient coral_client(true);

    // Get metadata for scan anal_id
    for (std::vector<CAN::SerialNumber_t>::const_iterator serial_number_iter = serial_number.begin();
	 serial_number_iter != serial_number.end();
	 ++serial_number_iter) {
    try {

      PixLib::ScanInfo_t scan_info = coral_client.getMetadata<PixLib::ScanInfo_t>(*serial_number_iter);
      if (verbose) {
	if (scan_info.status()==CAN::kNotSet || administrate) {
	  std::cout << "INFO change status of " << std::setw(9) << std::setfill('0') << *serial_number_iter << std::setfill(' ') 
                    << " from " << statusName(scan_info.status()) << " to " << statusName(status) <<  (simulate ? " [Simulation]" : "")<< std::endl;
        }
        else {
	  std::cout << "INFO will not change status of " << std::setw(9) << std::setfill('0') << *serial_number_iter << std::setfill(' ') 
	            << " from " << statusName(scan_info.status()) << "." <<  (simulate ? " [Simulation]" : "") << std::endl;
        }
      }
      if (!simulate) {
	if (status!=CAN::kNotSet && (scan_info.status()==CAN::kNotSet || administrate)) {
	  bool ret;
	  if (!analysis_status) {
	    ret = coral_client.updateScanMetadataStatus(*serial_number_iter, status);
	  }
	  else {
	    ret = coral_client.updateAnalysisMetadataStatus(*serial_number_iter, status);
	  }
	  if (!ret) {
	    std::cout << "ERROR failed to change status for " << std::setw(9) << std::setfill('0') << *serial_number_iter << std::setfill(' ')
		      << " to " << statusName(status) << "." << std::endl;
	  }
	}
      }

    } catch (CAN::PixMetaException & err) {
      std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    }
    }

  // COOL, CORAL POOL exceptions inherit from std exceptions: catching
  // std::exception will catch all errors from COOL, CORAL and POOL
  } catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }

  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;

}
                                                      



