#ifndef CORALDBCLIENT
#define CORALDBCLIENT 

// GENERIC C++/C headers
#include <time.h>
#include <sys/time.h>
#include <iostream>
#include <fstream>
#include <string>
#include <set>
#include <cstdio>

#include "Common.hh"
#include "ScanInfo_t.hh"
#include "AnalysisInfo_t.hh"

// COOL API: database service bootstrap
//#include "CoolApplication/Application.h"
//#include "CoolApplication/DatabaseSvcFactory.h"

#ifdef HAVE_NAMESPACES
#  undef HAVE_NAMESPACES
#endif
#ifdef HAVE_BOOL
#  undef HAVE_BOOL
#endif
#ifdef HAVE_DYNAMIC_CAST
#  undef HAVE_DYNAMIC_CAST
#endif

#include "CoralBase/AttributeListException.h"
#include "RelationalAccess/SchemaException.h"

namespace seal {
  class Context;
}
namespace coral {
  class ISessionProxy;
}
namespace CAN {

  //using namespace cool;
enum ETripleFlag {kYes,kNo, kIndifferent};

class PixCoralDbClient {

private:
  std::string m_connString;
  coral::ISessionProxy *m_session;

  std::string m_pixeltable;
  bool m_verbose;

  void transactionStartReadOnly();
  void transactionStartUpdate();
  void transactionCommit();

public:
  PixCoralDbClient(bool verbose=false, bool update=true);
  ~PixCoralDbClient();
  
  void printTables();
  void printTableDesc(std::string tableName);
  void printTableContent(std::string tableName);
  void createTables();
  void createAuxTables();
  void dropTables();
  //for PixLib::ScanInfo_t and CAN::AnalysisInfo_t
  template <class T> void storeMetadata(CAN::SerialNumber_t id, T *info);
  void storeRODstatus(CAN::SerialNumber_t id, std::map<std::string, PixLib::RODstatus_t> *info);
  template <class T> T getMetadata(CAN::SerialNumber_t id, bool getRODstatus=false);
  template <class T> bool updateMetadata(CAN::SerialNumber_t id, T *info);

  template <class T> bool updateScanMetadataField(CAN::SerialNumber_t id, std::string field, T value);
  template <class T1, class T2> bool updateScanMetadataField(CAN::SerialNumber_t id, const std::string &field1, T1 value1, const std::string &field2, T2 value2);

  bool updateScanMetadataFile(CAN::SerialNumber_t id, std::string value);
  bool updateScanMetadataAlternateFile(CAN::SerialNumber_t id, std::string value);
  bool updateScanMetadataComment(CAN::SerialNumber_t id, std::string value);
  bool updateScanMetadataDisable(CAN::SerialNumber_t id, std::string value);

  /**  Update the scan start time.
   * @param id the scan serial_number.
   * @param vlaue time in UTC.
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateScanMetadataStart(CAN::SerialNumber_t id, time_t value);

  /**  Update the scan end time.
   * @param id the scan serial_number.
   * @param vlaue time in UTC.
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateScanMetadataEnd(CAN::SerialNumber_t id, time_t value);
  bool updateScanMetadataStatus(CAN::SerialNumber_t id, CAN::GlobalStatus value);
  bool updateScanMetadataLocation(CAN::SerialNumber_t id, CAN::ScanLocation location);
  bool updateScanMetadataQuality(CAN::SerialNumber_t id, CAN::EQuality value);

  template <class T> bool updateAnalysisMetadataField(CAN::SerialNumber_t id, std::string field, T value);
  template <class T1, class T2> bool updateAnalysisMetadataField(CAN::SerialNumber_t id, const std::string &field1, T1 value1, const std::string &field2, T2 value2);
  bool updateAnalysisMetadataFile(CAN::SerialNumber_t id, std::string value);
  bool updateAnalysisMetadataAlternateFile(CAN::SerialNumber_t id, std::string value);
  bool updateAnalysisMetadataComment(CAN::SerialNumber_t id, std::string value);

  /**  Update the analysis status and the end time in one go.
   * @param id the analysis serial_number.
   * @param status the final status of an analysis.
   * @param end_time time in UTC.
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateScanMetadataStatusAndEndTime(CAN::SerialNumber_t id, CAN::GlobalStatus status, const time_t &end_time) {
    return updateScanMetadataField<int,time_t>(id,s_statusFieldName, static_cast<int>(status), s_endTimeFieldName, end_time);
  }


  /**  Update the analysis start time.
   * @param id the analysis serial_number
   * @param vlaue time in UTC
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateAnalysisMetadataStart(CAN::SerialNumber_t id, time_t value);

  /**  Update the analysis end time.
   * @param id the analysis serial_number.
   * @param vlaue time in UTC.
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateAnalysisMetadataEnd(CAN::SerialNumber_t id, time_t value);
  bool updateAnalysisMetadataStatus(CAN::SerialNumber_t id, CAN::GlobalStatus value);

  /**  Update the analysis status and the end time in one go.
   * @param id the analysis serial_number.
   * @param status the final status of an analysis.
   * @param end_time time in UTC.
   * The UTC time is returned e.g. by ::time(NULL)
   */
  bool updateAnalysisMetadataStatusAndEndTime(CAN::SerialNumber_t id, CAN::GlobalStatus status, const time_t &end_time) {
    return updateAnalysisMetadataField<int,time_t>(id,s_statusFieldName, static_cast<int>(status), s_endTimeFieldName, end_time);
  }

  bool updateAnalysisMetadataQuality(CAN::SerialNumber_t id, CAN::EQuality value);

  /** Get scan ID's and metadata matching query criteria
   *  @param location Where the scan was taken
   *  @param mintime  Scan taken after this date (in UTC).
   *  @param maxtime  Scan taken before this date (in UTC).
   *  @param name     Scan name
   *  @param tag      Scan tag
   *  @param quality  Scan quality
   *  @param comment  Comment field contains this string
   *  @param has_analysis set CAN::kYes or CAN::kNo if only scans should be returned for which analyeses exist or not.
   *  @param add_disable add the pix disable string to the scan info.
   */
  std::map<CAN::SerialNumber_t, PixLib::ScanInfo_t> searchScanMetadata(CAN::ScanLocation location=CAN::kNowhere,
								       time_t mintime=-1,
								       time_t maxtime=-1,
								       std::string name="",
								       std::string tag="",
								       CAN::EQuality quality=CAN::kUndefined,
								       CAN::GlobalStatus = CAN::kNotSet,
								       std::string comment="",
								       ETripleFlag has_analysis=kIndifferent,
								       std::string module_name="",
								       bool add_disable=false);

  /** Get analysis ID's and metadata matching query criteria
   * @param scan_serial_number the serial number of the analysed scan
   * @param mintime analyses taken after this date (in UTC).
   * @param maxtime analyses taken before this date (in UTC).
   * @param analysis_name analyses with this name.
   * @param analysis_tag analyses with this configuration tag.
   * @param classification_name analyses which used a classification with this name.
   * @param classification_tag analyses which used a classification with this configuration tag.
   * @param post_processing_name analyses which used a post processor with this name.
   * @param post_processing_tag analyses which used a post-processor with this configuration tag.
   * @param quality analyses of this quality
   * @param status analyses of this status
   * @param comment Comment field contains this string
   */
  std::map<CAN::SerialNumber_t, CAN::AnalysisInfo_t> searchAnalysisMetadata(CAN::SerialNumber_t scan_serial_number=0,
									    time_t mintime=-1,
									    time_t maxtime=-1,
									    std::string analysis_name="",
									    std::string analysis_tag="",
									    std::string classification_name="",
									    std::string classification_tag="",
									    std::string post_processing_name="",
									    std::string post_processing_tag="",
									    CAN::EQuality quality=CAN::kUndefined,
									    CAN::GlobalStatus status = CAN::kNotSet,
									    std::string comment="",
									    CAN::ScanLocation location=CAN::kNowhere);

  CAN::SerialNumber_t getNewSerialNumber(CAN::ESerialNumberType);
  CAN::SerialNumber_t scanSerialNumberForFile(std::string file);
  void VerboseOutput(bool verbose) {m_verbose = verbose;};

  // Convert local time t to GMT (UTC)
  static std::time_t LocalToGMTTime(std::time_t t) {
    std::time_t rawtime = t ? t : time(0);
    tm * ptm;
    struct tm temp;
    ptm = gmtime_r(&rawtime, &temp);
    return mktime(ptm);
  }
  // Get current time in GMT (UTC)
  static std::time_t GMTCurrentTime() { return LocalToGMTTime(0); }
  // Convert GMT (UTC) time to local time

  static std::time_t GMTtoLocalTime(std::time_t time_in_utc) {
    tm t_m;
    t_m.tm_gmtoff=0; // to make sure that the offset is at least reasonable.
    gmtime_r(&time_in_utc, &t_m);
    /*time_t utc_time_minus_some_offset = */ mktime(&t_m); // otherwise t_m.tm_gmtoff will not be set
    return time_in_utc + t_m.tm_gmtoff;

    // unfortunately mktime (gmtime_r( )) will take the daylight saving flag into account
    // such the offset calculated below is wrong during summer time :
    //
    //   struct tm temp;
    //   return 2*gmttime - mktime (gmtime_r(&gmttime, &temp));

  }

  // Convert a time_t to a string
  static const std::string timeString(time_t t) {
    std::string time;
    std::time_t std_time(t);

    char *ptr = ctime(&std_time);
    if (!ptr) return time;
    time = ptr;
    if (time.size()==0)  return time;
    return time.substr(0,time.size()-1);
  }

  static const char *s_scanStatusFieldName;
  static const char *s_scanEndTimeFieldName;

  static bool scanFieldCanBeModified(const std::string &field_name) {
    return fieldCanBeModified(field_name);
  }


  static bool analysisFieldCanBeModified(const std::string &field_name) {
    return fieldCanBeModified(field_name);
  }

  static const char *s_statusFieldName;
  static const char *s_endTimeFieldName;

  static bool fieldCanBeModified(const std::string &field_name);

  static void fixMode();

  static const char *s_modifiableFieldsNormal[];
  static const char *s_modifiableFieldsForFixing[];
  static const char **s_modifiableFields;
};

} //namespace CAN

#endif
