#include "MetaDataService.hh"
#include "PixMetaException.hh"
#include <iostream>

int main(int argc, char **argv)
{
  try {
    // Get serial number from CORAL MetaDataService
    CAN::SerialNumber_t scan_serial_number = CAN::MetaDataService::instance()->newSerialNumber(CAN::kScanNumber);
    CAN::SerialNumber_t analysis_serial_number2 = CAN::MetaDataService::instance()->newSerialNumber(CAN::kAnalysisNumber);
    CAN::SerialNumber_t sessionID = CAN::MetaDataService::instance()->newSerialNumber(CAN::kSessionID);
  }
  catch (CAN::PixMetaException & err) {
    std::cout<<"PixMetaException: "<<err.what()<<std::endl;
    return -1;
  }
  catch ( std::exception& e ) {
    std::cout << "std::exception caught: " << e.what() << std::endl;
    return -1;
  }
  catch (...) {
    std::cout << "Unknown exception caught!" << std::endl;
    return -1;
  }

  return 0;
}
