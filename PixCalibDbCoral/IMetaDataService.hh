#ifndef _IMetaDataService_h_
#define _IMetaDataService_h_

#include "ScanInfo_t.hh"
#include "AnalysisInfo_t.hh"
#include <memory>

namespace CAN {

  template <class T>
  class InfoPtr_t : public std::shared_ptr<InfoBase_t>
  {
  protected:
    InfoPtr_t(const std::shared_ptr<InfoBase_t> &a) : std::shared_ptr<InfoBase_t>(a) {}
    InfoPtr_t(InfoBase_t *a) : std::shared_ptr<InfoBase_t>(a) {}
 
  public:
    InfoPtr_t()  {}
    InfoPtr_t(T *a) : std::shared_ptr<InfoBase_t>(a) {}
    InfoPtr_t(const InfoPtr_t &a) : std::shared_ptr<InfoBase_t>(a) {}
 
    InfoPtr_t<T> &operator=(const InfoPtr_t<T> &a) {std::shared_ptr<InfoBase_t>::operator=(a); return *this;}
 
    const T &operator*() const  {return static_cast< T &>(std::shared_ptr<InfoBase_t>::operator*());}
    const T *operator->() const {return static_cast< T *>(std::shared_ptr<InfoBase_t>::operator->());}
 
    T &operator*()              {return static_cast< T &>(std::shared_ptr<InfoBase_t>::operator*());}
    T *operator->()             {return static_cast< T *>(std::shared_ptr<InfoBase_t>::operator->());}
 
    operator bool() const {return std::shared_ptr<InfoBase_t>::get()!=NULL;}
  };
 
  class IMetaDataService
  {
  public:
    virtual ~IMetaDataService() {};

    virtual const CAN::SerialNumber_t newSerialNumber(ESerialNumberType type) = 0;

    virtual const InfoPtr_t<PixLib::ScanInfo_t> getScanInfo(SerialNumber_t scan_serial_number) = 0;

    virtual const InfoPtr_t<CAN::AnalysisInfo_t> getAnalysisInfo(SerialNumber_t analysis_serial_number) = 0;

    //    const InfoPtr_t<PixLib::ScanInfo_t> getScanInfo(ESerialNumberType src_serial_number_type, SerialNumber_t src_serial_number);

    virtual void addInfo(SerialNumber_t serial_number, const InfoPtr_t<PixLib::ScanInfo_t> &a) = 0;

    virtual void addInfo(SerialNumber_t serial_number, const InfoPtr_t<CAN::AnalysisInfo_t> &a) = 0;

    /** Set the final scan status and set the end time of the scan.
     */
    virtual bool setFinalScanStatus(SerialNumber_t scan_serial_number, CAN::GlobalStatus final_status, const time_t *end_time=NULL) = 0;

    /** Set the final analysis status and set the end time of the analysis.
     */
    virtual bool setFinalAnalysisStatus(SerialNumber_t analysis_serial_number, CAN::GlobalStatus final_status, const time_t *end_time=NULL) = 0;

    /** Cleanup the info cache.
     * Will only delete objects which have not been used recentlu,Should be called in regular intervalles. 
     */
    virtual void cleanup() = 0;
  };
}
#endif
