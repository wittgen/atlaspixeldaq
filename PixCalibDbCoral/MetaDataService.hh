#ifndef _MetaDataService_h_
#define _MetaDataService_h_

#include "IMetaDataService.hh"

namespace CAN {

  class MetaDataService
  {
   public:
    /** Get the PixDbMetaDataService instance.
     */
    static IMetaDataService *instance();

    static void deepCleanup();

  private:
    static IMetaDataService *s_instance;
  };
}
#endif
