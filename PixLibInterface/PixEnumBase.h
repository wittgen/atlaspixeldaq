#ifndef PIXENUMBASE_H_
#define PIXENUMBASE_H_
#include "scanEnums.h"
#include <map>
#include <string>

namespace PixLib {

#define _reverse_lookup(m) \
   std::string lookup(int key) { \
     for (std::map<std::string,int>::iterator i=(m).begin();i!=(m).end();i++) { \
       if(key==i->second) return i->first;   }				\
    return std::string(""); }
			
  /** Different fitting methods for SCURVE */
  class EnumScanFitMethod {
    public:
      enum FitMethod : int { FIT_LMMIN,
                                 FIT_LINEAR_INT,
                                 FIT_DERIVATIVE};
      EnumScanFitMethod() {
        m_ScanFitMethodMap["FIT_LMMIN"] = FIT_LMMIN;
        m_ScanFitMethodMap["FIT_LINEAR_INT"] = FIT_LINEAR_INT;
        m_ScanFitMethodMap["FIT_DERIVATIVE"] = FIT_DERIVATIVE;
      }
    std::map<std::string, int> EnumScanFitMethodMap() {return m_ScanFitMethodMap;}
    _reverse_lookup(m_ScanFitMethodMap);
    private:
    std::map<std::string, int> m_ScanFitMethodMap;
  };
  
  class EnumScanAct {
  public:
    enum ScanAct: int {NO_ACTION=SCAN_NO_ACTION, 
		  FIT=SCAN_FIT, 
		  TUNE_THRESH=SCAN_TUNE_THRESH, 
		  CALC_THRESH=SCAN_CALC_THRESH};
   EnumScanAct() {
      m_ScanActMap["NO_ACTION"]=NO_ACTION;
      m_ScanActMap["FIT"]=FIT; 
      m_ScanActMap["TUNE_THRESH"]=TUNE_THRESH;
      m_ScanActMap["CALC_THRESH"]=CALC_THRESH;
    }
    std::map<std::string, int> EnumScanActMap() {return m_ScanActMap;}
    _reverse_lookup(m_ScanActMap);
  private: 
    std::map<std::string, int> m_ScanActMap;    
  };


  class EnumTriggMode {
  public:
    enum TriggMode: int {DSP=SCAN_DSP,
		    INTERNAL_SELF=SCAN_INTERNAL_SELF, 
		    INTERNAL_INTERVAL=SCAN_INTERNAL_INTERVAL, 
		    INTERNAL_SYNCHRONOUS=SCAN_INTERNAL_SYNCHRONOUS, 
		    EXTERNAL, TRIGGERMODES,MIXED};
   EnumTriggMode() {
      m_TriggModeMap["DSP"]=DSP;
      m_TriggModeMap["INTERNAL_SELF"]=INTERNAL_SELF; 
      m_TriggModeMap["INTERNAL_INTERVAL"]=INTERNAL_INTERVAL;
      m_TriggModeMap["INTERNAL_SYNCHRONOUS"]=INTERNAL_SYNCHRONOUS;
      m_TriggModeMap["EXTERNAL"]=EXTERNAL;
      m_TriggModeMap["TRIGGERMODES"]=TRIGGERMODES;
      m_TriggModeMap["MIXED"]=MIXED;
    }
    std::map<std::string, int> EnumTriggModeMap() {return m_TriggModeMap;}
    _reverse_lookup(m_TriggModeMap);
  private: 
    std::map<std::string, int> m_TriggModeMap;    
  };
  
  
  class EnumTriggOptions {
  public:
    enum TriggOptions: int {USE_CLOW=SCAN_USE_CLOW, 
		       USE_CHIGH=SCAN_USE_CHIGH, 
		       DIGITAL_INJECT=SCAN_DIGITAL_INJECT, 
		       HITBUS_ON=SCAN_HITBUS_ON, 
		       SPECIFY_CHARGE_NOT_VCAL=SCAN_SPECIFY_CHARGE_NOT_VCAL};
     EnumTriggOptions() {
      m_TriggOptionsMap["USE_CLOW"]=USE_CLOW;
      m_TriggOptionsMap["USE_CHIGH"]=USE_CHIGH; 
      m_TriggOptionsMap["DIGITAL_INJECT"]=DIGITAL_INJECT;
      m_TriggOptionsMap["HITBUS_ON"]=HITBUS_ON;
      m_TriggOptionsMap["SPECIFY_CHARGE_NOT_VCAL"]=SPECIFY_CHARGE_NOT_VCAL;
    }
    std::map<std::string, int> EnumTriggOptionsMap() {return m_TriggOptionsMap;}
    _reverse_lookup(m_TriggOptionsMap);
  private: 
    std::map<std::string, int> m_TriggOptionsMap;    
};

  
  class EnumFitFunc {
  public:
   enum FitFunc: int {SCURVE=SCAN_SCURVE, 
		 TOTCAL=SCAN_TOTCAL, 
		 GAUSS=SCAN_GAUSS,
		 T0=SCAN_T0,
		 SCURVE_NOCONV,
                 SCURVE_XTALK};
   EnumFitFunc() {
      m_FitFuncMap["SCURVE"]=SCURVE;
      m_FitFuncMap["TOTCAL"]=TOTCAL; 
      m_FitFuncMap["GAUSS"]=GAUSS;
      m_FitFuncMap["T0"]=T0;
      m_FitFuncMap["SCURVE_NOCONV"]=SCURVE_NOCONV;
      m_FitFuncMap["SCURVE_XTALK"]=SCURVE_XTALK;
    }
    std::map<std::string, int> EnumFitFuncMap() {return m_FitFuncMap;}
    _reverse_lookup(m_FitFuncMap);
  private: 
    std::map<std::string, int> m_FitFuncMap;    
  };

  class EnumAssemblyType {
  public:
    enum AssemblyType: int { NONE, STAVE, SECTOR };
    EnumAssemblyType() {
      m_AssemblyTypeMap["NONE"]=NONE;
      m_AssemblyTypeMap["STAVE"]=STAVE; 
      m_AssemblyTypeMap["SECTOR"]=SECTOR;
    }
    std::map<std::string, int> EnumAssemblyTypeMap() {return m_AssemblyTypeMap;}
    _reverse_lookup(m_AssemblyTypeMap);
  private: 
    std::map<std::string, int> m_AssemblyTypeMap;    
  };

  class EnumPP0Type {
  public:
    enum PP0Type :int { OPTICAL, OPTICAL_TEST, ELECTRICAL };
    EnumPP0Type() {
      m_PP0TypeMap["OPTICAL"]=OPTICAL;
      m_PP0TypeMap["OPTICAL_TEST"]=OPTICAL_TEST;
      m_PP0TypeMap["ELECTRICAL"]=ELECTRICAL;
    }
    std::map<std::string, int> EnumPP0TypeMap()  {return m_PP0TypeMap;}
    _reverse_lookup(m_PP0TypeMap);
  private:
    std::map<std::string, int> m_PP0TypeMap;
  };


  class EnumMCCflavour {
  public:
    enum MCCflavour: int {PM_NO_MCC=0, PM_MCC_I1=1, PM_MCC_I2=2};
    EnumMCCflavour() {
      m_MCCflavourMap["PM_NO_MCC"]=PM_NO_MCC;
      m_MCCflavourMap["PM_MCC_I1"]=PM_MCC_I1;
      m_MCCflavourMap["PM_MCC_I2"]=PM_MCC_I2;
    }
    std::map<std::string, int> EnumMCCflavourMap() {return m_MCCflavourMap; }
    _reverse_lookup(m_MCCflavourMap);
  private:
    std::map<std::string, int> m_MCCflavourMap;
  };

  class EnumFEflavour {
  public:
    enum FEflavour: int  {PM_NO_FE=0, PM_FE_I1=1, PM_FE_I2=2, 
		    PM_MODULE=0x100,PM_CHIP=0x200, /* for PixGeometry class */
		    PM_FE_I4_MODULE=0x180,
		    PM_FE_I4_CHIP=0x280,
		    PM_FE_I2_MODULE=0x101,
		    PM_FE_I2_CHIP=0x201,
		    /* FEI4 */
		    PM_FE_I4=0x80,PM_FE_I4A=0x81, PM_FE_I4B=0x82,
		    /* aliases for PixModuleGroup */
                    FE_I_1=PM_FE_I1,FE_I_2=PM_FE_I2,FE_I_4= PM_FE_I4,FE_I_3=PM_FE_I2 };
    /* FE_I_3 is only used in PixModuleGroup */ 

    EnumFEflavour() {
      m_FEflavourMap["PM_NO_FE"]=PM_NO_FE;
      m_FEflavourMap["PM_FE_I1"]=PM_FE_I1;
      m_FEflavourMap["PM_FE_I2"]=PM_FE_I2;
      m_FEflavourMap["PM_FE_I4B"]=PM_FE_I4A;
      m_FEflavourMap["PM_FE_I4B"]=PM_FE_I4B;
    }
    bool isFEI4(FEflavour fe) {return (((int) fe & (int) PM_FE_I4) == ((int) PM_FE_I4));}
    /* ugly hack - should only use enum types everywhere */
    bool isFEI4(int fe) {return ((fe & (int) PM_FE_I4) == ((int) PM_FE_I4));}
    bool isModule(FEflavour fe) {return (((int) fe & (int) PM_MODULE) == ((int) PM_MODULE));}
    bool isChip(FEflavour fe) {return (((int) fe & (int) PM_CHIP) == ((int) PM_CHIP));}
    std::map<std::string, int> EnumFEflavourMap() {return m_FEflavourMap;}
    _reverse_lookup(m_FEflavourMap);
  private:
    std::map<std::string, int>  m_FEflavourMap;
  };

  class EnumRunType {
  public:
    enum RunType: int { NORMAL_SCAN, RAW_EVENT, FMT_COUNT,
      FMT_COUNT_LINKSCAN, IN_LINK_SCAN, MONLEAKRUN,MSR_MEASURE,
      REG_TEST, REG_TEST_SEU
    };
    EnumRunType() {
      m_RunTypeMap["NORMAL_SCAN"]=NORMAL_SCAN; 
      m_RunTypeMap["RAW_EVENT"]=RAW_EVENT; 
      m_RunTypeMap["FMT_COUNT"]=FMT_COUNT;
      m_RunTypeMap["FMT_COUNT_LINKSCAN"]=FMT_COUNT_LINKSCAN; 
      m_RunTypeMap["IN_LINK_SCAN"]=IN_LINK_SCAN;
      m_RunTypeMap["MONLEAKRUN"]=MONLEAKRUN;
      m_RunTypeMap["MSR_MEASURE"]=MSR_MEASURE;
      m_RunTypeMap["REG_TEST"]=REG_TEST;
      m_RunTypeMap["REG_TEST_SEU"]=REG_TEST_SEU;
    }
    std::map<std::string, int>  EnumRunTypeMap() { return m_RunTypeMap; }
    _reverse_lookup(m_RunTypeMap);
  private:
    std::map<std::string, int>  m_RunTypeMap;

  };
  class EnumScanParam {
  public:
    enum ScanParam: int {
      NO_PAR = SCAN_NONE,
      IVDD2,
      ID,
      IP2,
      IP,
      TRIMT,
      IF,
      TRIMF,
      ITH1,
      ITH2,
      IL,
      IL2,
      LATENCY = SCAN_L1ALATENCY,
      TDACS = SCAN_TDAC,
      FDACS = SCAN_FDAC,
      GDAC = SCAN_GDAC,
      TRIGGER_DELAY = SCAN_TRIGGER_DELAY,
      MULTITRIG_INTERVAL,
      STROBE_DURATION,
      VCAL = SCAN_VCAL,
      STROBE_DELAY = SCAN_STROBE_DELAY,
      STROBE_DEL_RANGE,
      CHARGE,
      TDACS_VARIATION,
      GDAC_VARIATION,
      BOC_BPH,
      BOC_VPH0,
      BOC_VPH1,
      BOC_VFINE,
      BOC_VPH,
      BOC_BPMPH,
      BOC_TX_CURR,
      BOC_TX_MS,
      BOC_RX_THR,
      BOC_RX_DELAY,
      BOC_RX_THR_DIFF,
      BOC_RX_DELAY_DIFF,
      BOC_TX_BPM,
      BOC_TX_BPMF,
      BOC_BVPH,
      BPM_INVERT,
      OB_VISET,
      OPTO_VISET,
      BOC_THR_TUNE,
      DISCBIAS,
      TEMP,
      GDAC_COARSE
    };
    EnumScanParam() {
      m_ScanParamMap["NO_PAR"]= NO_PAR ;
      m_ScanParamMap["IVDD2"]= IVDD2 ;
      m_ScanParamMap["ID"]=  ID;
      m_ScanParamMap["IP2"]= IP2 ;
      m_ScanParamMap["IP"]= IP ;
      m_ScanParamMap["TRIMT"]= TRIMT ;
      m_ScanParamMap["IF"]= IF ;
      m_ScanParamMap["TRIMF"]=TRIMF  ;
      m_ScanParamMap["ITH1"]=ITH1  ;
      m_ScanParamMap["ITH2"]=ITH2  ;
      m_ScanParamMap["IL"]= IL ;
      m_ScanParamMap["IL2"]=IL2  ;
      m_ScanParamMap["LATENCY"]=LATENCY  ;
      m_ScanParamMap["TDACS"]=TDACS  ;
      m_ScanParamMap["FDACS"]=FDACS  ;
      m_ScanParamMap["GDAC"]=GDAC  ;
      m_ScanParamMap["TRIGGER_DELAY"]=TRIGGER_DELAY  ;
      m_ScanParamMap["MULTITRIG_INTERVAL"]=MULTITRIG_INTERVAL  ;
      m_ScanParamMap["STROBE_DURATION"]=STROBE_DURATION  ;
      m_ScanParamMap["VCAL"]= VCAL ;
      m_ScanParamMap["STROBE_DELAY"]=STROBE_DELAY  ;
      m_ScanParamMap["STROBE_DEL_RANGE"]=STROBE_DEL_RANGE  ;
      m_ScanParamMap["CHARGE"]=CHARGE  ;
      m_ScanParamMap["TDACS_VARIATION"]=TDACS_VARIATION  ;
      m_ScanParamMap["GDAC_VARIATION"]=GDAC_VARIATION  ;
      m_ScanParamMap["BOC_BPH"]= BOC_BPH ;
      m_ScanParamMap["BOC_VPH0"]=BOC_VPH0  ;
      m_ScanParamMap["BOC_VPH1"]=BOC_VPH1  ;
      m_ScanParamMap["BOC_VFINE"]=BOC_VFINE  ;
      m_ScanParamMap["BOC_VPH"]= BOC_VPH ;
      m_ScanParamMap["BOC_BPMPH"]=BOC_BPMPH  ;
      m_ScanParamMap["BOC_TX_CURR"]=BOC_TX_CURR  ;
      m_ScanParamMap["BOC_TX_MS"]=BOC_TX_MS  ;
      m_ScanParamMap["BOC_RX_THR"]=BOC_RX_THR  ;
      m_ScanParamMap["BOC_RX_DELAY"]=BOC_RX_DELAY  ;
      m_ScanParamMap["BOC_RX_THR_DIFF"]=BOC_RX_THR_DIFF  ;
      m_ScanParamMap["BOC_RX_DELAY_DIFF"]=BOC_RX_DELAY_DIFF  ;
      m_ScanParamMap["BOC_TX_BPM"]=BOC_TX_BPM  ;
      m_ScanParamMap["BOC_TX_BPMF"]=BOC_TX_BPMF  ;
      m_ScanParamMap["BOC_BVPH"]=BOC_BVPH  ;
      m_ScanParamMap["BPM_INVERT"]=BPM_INVERT  ;
      m_ScanParamMap["OB_VISET"]= OB_VISET ;
      m_ScanParamMap["OPTO_VISET"]=OPTO_VISET  ;
      m_ScanParamMap["BOC_THR_TUNE"]= BOC_THR_TUNE ;
      m_ScanParamMap["DISCBIAS"]= DISCBIAS ;
      m_ScanParamMap["TEMP"]= TEMP ;
      m_ScanParamMap["GDAC_COARSE"]= GDAC_COARSE ;
    }
     std::map<std::string, int>  EnumScanParamMap() { return m_ScanParamMap ;}
     _reverse_lookup(m_ScanParamMap);
  private:
    std::map<std::string, int>  m_ScanParamMap;

  };
  class EnumScanType {
  public:
    enum ScanType: int {
      DIGITAL_TEST,
      ANALOG_TEST,
      THRESHOLD_SCAN,
      TOT_CALIB,
      TDAC_TUNE,
      TDAC_TUNE_ITERATED,
      TDAC_FAST_TUNE,
      THR_FAST_SCAN,
      GDAC_TUNE,
      FDAC_TUNE,
      FDAC_FAST_TUNE,
      IF_TUNE,
      TIMEWALK_MEASURE,
      INCREMENTAL_TDAC_SCAN,
      BOC_RX_DELAY_SCAN,
      BOC_THR_RX_DELAY_SCAN,
      BOC_V0_RX_DELAY_SCAN,
      BOC_RX_THR_SCAN,
      BOC_THR_DEL_LONG,
      BOC_THR_DEL_FAST,
      BOC_THR_DEL_DSP,
      STO_DSP,
      LEAK_SCAN_DSP,
      BOC_LINKSCAN,
      INTIME_THRESH_SCAN,
      T0_SCAN,
      CROSSTALK_SCAN,
      TWOTRIGGER_THRESHOLD,
      TWOTRIGGER_NOISE,
      DIFFUSION,
      SHAPESCAN,
      BOC_TUNE,
      OPTO_TUNE,
      BOC_INLINKSCAN,
      IF_FAST_TUNE,
      MSR_MEASUREMENT,
      REGISTER_TEST,
      REGISTER_TEST_SEU,
      TOT_VERIF,
      //FEI4 RCE specific
      DELAY_SCAN, 
      LV1LATENCY_SCAN, 
      COSMIC_DATA,
      MEASUREMENT_SCAN, 
      VTHIN_SCAN, 
      SELFTRIGGER, 
      MULTISHOT, 
      STUCKPIXELS, 
      NOISESCAN, 
      TOT_TEST,
      MODULE_CROSSTALK,
      OFFSET_SCAN,
      SCINTDELAY_SCAN,
      EXTTRIGGER,
      COSMIC_RCE,
      GDAC_RETUNE,
      GDAC_SCAN,
      TEMPERATURE_SCAN,
      MONLEAK_SCAN,
      SERIAL_NUMBER_SCAN,
      TDAC_FAST_RETUNE,
      GDAC_FAST_TUNE,
      GDAC_TUNE_ITERATED,
      GDAC_FAST_RETUNE,
      EXT_REGISTER_VERIFICATION,
      NOISESCAN_SELFTRIGGER,
      DIGITALTEST_SELFTRIGGER,
      GDAC_FINE_FAST_TUNE,
      GDAC_COARSE_FAST_TUNE,
      DISCBIAS_TUNE
      
    };
    EnumScanType() {
  // Initialize scanTypes
      m_scanTypeMap["DIGITAL_TEST"] = DIGITAL_TEST;
      m_scanTypeMap["ANALOG_TEST"] = ANALOG_TEST;
      m_scanTypeMap["THRESHOLD_SCAN"] = THRESHOLD_SCAN;
      m_scanTypeMap["TOT_CALIB"] = TOT_CALIB;
      m_scanTypeMap["TOT_VERIF"] = TOT_VERIF;
      m_scanTypeMap["TDAC_TUNE"] = TDAC_TUNE;
      m_scanTypeMap["TDAC_FAST_TUNE"] = TDAC_FAST_TUNE;
      m_scanTypeMap["THR_FAST_SCAN"] = THR_FAST_SCAN;
      m_scanTypeMap["TDAC_TUNE_ITERATED"] = TDAC_TUNE_ITERATED;
      m_scanTypeMap["GDAC_TUNE"] = GDAC_TUNE;
      m_scanTypeMap["FDAC_TUNE"] = FDAC_TUNE;
      m_scanTypeMap["FDAC_FAST_TUNE"] = FDAC_FAST_TUNE;
      m_scanTypeMap["IF_TUNE"] = IF_TUNE;
      m_scanTypeMap["TIMEWALK_MEASURE"] = TIMEWALK_MEASURE;
      m_scanTypeMap["INCREMENTAL_TDAC_SCAN"] = INCREMENTAL_TDAC_SCAN;
      m_scanTypeMap["BOC_RX_DELAY_SCAN"] = BOC_RX_DELAY_SCAN;
      m_scanTypeMap["BOC_THR_RX_DELAY_SCAN"] = BOC_THR_RX_DELAY_SCAN;
      m_scanTypeMap["BOC_THR_DEL_LONG"] = BOC_THR_DEL_LONG;
      m_scanTypeMap["BOC_RX_THR_SCAN"] = BOC_RX_THR_SCAN;
      m_scanTypeMap["BOC_THR_DEL_FAST"] = BOC_THR_DEL_FAST;
      m_scanTypeMap["STO_DSP"] = STO_DSP;
      m_scanTypeMap["BOC_THR_DEL_DSP"] = BOC_THR_DEL_DSP;
      m_scanTypeMap["LEAK_SCAN_DSP"] = LEAK_SCAN_DSP;
      m_scanTypeMap["SHAPESCAN"] = SHAPESCAN;
      m_scanTypeMap["BOC_TUNE"] = BOC_TUNE;
      m_scanTypeMap["OPTO_TUNE"] = OPTO_TUNE;
      m_scanTypeMap["BOC_V0_RX_DELAY_SCAN"] = BOC_V0_RX_DELAY_SCAN;
      m_scanTypeMap["BOC_LINKSCAN"] = BOC_LINKSCAN;
      m_scanTypeMap["BOC_INLINKSCAN"] = BOC_INLINKSCAN;
      m_scanTypeMap["INTIME_THRESH_SCAN"] = INTIME_THRESH_SCAN;
      m_scanTypeMap["T0_SCAN"] = T0_SCAN;
      m_scanTypeMap["CROSSTALK_SCAN"] = CROSSTALK_SCAN;
      m_scanTypeMap["IF_FAST_TUNE"] = IF_FAST_TUNE;
      m_scanTypeMap["MSR_MEASUREMENT"] = MSR_MEASUREMENT;
      m_scanTypeMap["REGISTER_TEST"] = REGISTER_TEST;
      m_scanTypeMap["REGISTER_TEST_SEU"] = REGISTER_TEST_SEU;
      /* add for FEI4/RCE */  
      m_scanTypeMap["DELAY_SCAN"]=DELAY_SCAN ;
      m_scanTypeMap["LV1LATENCY_SCAN"]=LV1LATENCY_SCAN; 
      m_scanTypeMap["COSMIC_DATA"]=COSMIC_DATA;
      m_scanTypeMap["MEASUREMENT_SCAN"]=MEASUREMENT_SCAN ;
      m_scanTypeMap["VTHIN_SCAN"]=VTHIN_SCAN ;
      m_scanTypeMap["SELFTRIGGER"]=SELFTRIGGER;
      m_scanTypeMap["MULTISHOT"]=MULTISHOT ;
      m_scanTypeMap["STUCKPIXELS"]=STUCKPIXELS; 
      m_scanTypeMap["NOISESCAN"]=NOISESCAN; 
      m_scanTypeMap["TOT_TEST"]=TOT_TEST;
      m_scanTypeMap["MODULE_CROSSTALK"]=MODULE_CROSSTALK;
      m_scanTypeMap["OFFSET_SCAN"]=OFFSET_SCAN;
      m_scanTypeMap["SCINTDELAY_SCAN"]=SCINTDELAY_SCAN;
      m_scanTypeMap["TWOTRIGGER_THRESHOLD"]=TWOTRIGGER_THRESHOLD;
      m_scanTypeMap["TWOTRIGGER_NOISE"]=TWOTRIGGER_NOISE;
      m_scanTypeMap["DIFFUSION"]=DIFFUSION;
      m_scanTypeMap["EXTTRIGGER"]=EXTTRIGGER;
      m_scanTypeMap["COSMIC_RCE"]=COSMIC_RCE;
      m_scanTypeMap["GDAC_RETUNE"]=GDAC_RETUNE;
      m_scanTypeMap["GDAC_SCAN"]=GDAC_SCAN;
      m_scanTypeMap["TEMPERATURE_SCAN"]=TEMPERATURE_SCAN;
      m_scanTypeMap["MONLEAK_SCAN"]=MONLEAK_SCAN;
      m_scanTypeMap["SERIAL_NUMBER_SCAN"]=SERIAL_NUMBER_SCAN;
      m_scanTypeMap["TDAC_FAST_RETUNE"] = TDAC_FAST_RETUNE;
      m_scanTypeMap["GDAC_FAST_TUNE"] = GDAC_FAST_TUNE;
      m_scanTypeMap["GDAC_FAST_RETUNE"] = GDAC_FAST_RETUNE;
      m_scanTypeMap["GDAC_TUNE_ITERATED"] = GDAC_TUNE_ITERATED;
      m_scanTypeMap["EXT_REGISTER_VERIFICATION"] = EXT_REGISTER_VERIFICATION;
      //m_scanTypeMap["GDAC_COARSE_FAST_TUNE"] = GDAC_COARSE_FAST_TUNE;
      m_scanTypeMap["NOISESCAN_SELFTRIGGER"] = NOISESCAN_SELFTRIGGER;
      m_scanTypeMap["DIGITALTEST_SELFTRIGGER"] = DIGITALTEST_SELFTRIGGER;
      m_scanTypeMap["GDAC_FINE_FAST_TUNE"] = GDAC_FINE_FAST_TUNE;
      m_scanTypeMap["GDAC_COARSE_FAST_TUNE"] = GDAC_COARSE_FAST_TUNE;
      m_scanTypeMap["DISCBIAS_TUNE"] = DISCBIAS_TUNE;
    }
    std::map<std::string, int> EnumScanTypeMap() {return m_scanTypeMap;}
    _reverse_lookup(m_scanTypeMap);
  private:
    std::map<std::string, int> m_scanTypeMap;
  };
  class EnumEndLoopAction {
  public:
    enum EndLoopAction: int {
      NO_ACTION,
      SCURVE_FIT,
      TDAC_TUNING,
      TDAC_TUNING_ITERATED,
      TDAC_FAST_TUNING,
      THR_FAST_SCANNING,
      GDAC_TUNING,
      T0_SET,
      FDAC_TUNING,
      IF_TUNING,
      MIN_THRESHOLD,
      MCCDEL_FIT,
      BOC_TUNING,
      OPTO_TUNING,
      CHARGE_SET,
      IF_FAST_TUNING,
      TOTCAL_FIT,
      //FEI4 specific
      //TODO: review if necessary
      NORMALIZE,
      OFFSET,
      GDAC_FINE_FAST_TUNING,
      GDAC_FAST_TUNING,
      GDAC_TUNING_ITERATED,
      GDAC_COARSE_FAST_TUNING,
      DISCBIAS_TUNING,
      SETMASK,
      FDAC_FAST_TUNING
    };
    EnumEndLoopAction() {
      m_EndLoopMap["NO_ACTION"]=NO_ACTION;
      m_EndLoopMap["SCURVE_FIT"]=SCURVE_FIT;
      m_EndLoopMap["TDAC_TUNING"]=TDAC_TUNING;
      m_EndLoopMap["TDAC_TUNING_ITERATED"]=TDAC_TUNING_ITERATED;
      m_EndLoopMap["TDAC_FAST_TUNING"]=TDAC_FAST_TUNING;
      m_EndLoopMap["FDAC_FAST_TUNING"]=FDAC_FAST_TUNING;
      m_EndLoopMap["THR_FAST_SCANNING"]=THR_FAST_SCANNING;
      m_EndLoopMap["GDAC_TUNING"]=GDAC_TUNING;
      m_EndLoopMap["GDAC_FAST_TUNING"]=GDAC_FAST_TUNING;
      m_EndLoopMap["GDAC_TUNING_ITERATED"]=GDAC_TUNING_ITERATED;
      m_EndLoopMap["T0_SET"]=T0_SET;
      m_EndLoopMap["FDAC_TUNING"]=FDAC_TUNING;
      m_EndLoopMap["IF_TUNING"]=IF_TUNING;
      m_EndLoopMap["MIN_THRESHOLD"]=MIN_THRESHOLD;
      m_EndLoopMap["MCCDEL_FIT"]=MCCDEL_FIT;
      m_EndLoopMap["BOC_TUNING"]=BOC_TUNING;
      m_EndLoopMap["OPTO_TUNING"]=OPTO_TUNING;
      m_EndLoopMap["CHARGE_SET"]=CHARGE_SET;
      m_EndLoopMap["IF_FAST_TUNING"]=IF_FAST_TUNING;
      m_EndLoopMap["TOTCAL_FIT"]=TOTCAL_FIT;
      //FEI4 specific
      //TODO: review if necessary
      m_EndLoopMap["NORMALIZE"]=NORMALIZE;
      m_EndLoopMap["OFFSET"]=OFFSET;
      m_EndLoopMap["GDAC_FINE_FAST_TUNING"]=GDAC_FINE_FAST_TUNING;
      m_EndLoopMap["GDAC_COARSE_FAST_TUNING"]=GDAC_COARSE_FAST_TUNING;
      m_EndLoopMap["DISCBIAS_TUNING"]=DISCBIAS_TUNING;
      m_EndLoopMap["SETMASK"]=SETMASK;
    }
    _reverse_lookup(m_EndLoopMap);
    std::map<std::string, int> EnumEndLoopActionMap() { return m_EndLoopMap;}
  private:
    
    std::map<std::string, int> m_EndLoopMap;  
  };
  class EnumHistogramType {
  public:
    enum HistogramType: int {
      OCCUPANCY,
      TOT_MEAN,
      TOT_SIGMA,
      SCURVE_MEAN,
      SCURVE_SIGMA,
      SCURVE_CHI2,
      MONLEAK_HISTO,
      TDAC_T,
      TDAC_THR,
      OCCUPANCY_THR,
      GDAC_T,
      GDAC_THR,
      FDAC_T,
      FDAC_TOT,
      IF_T,
      IF_TOT,
      TIMEWALK,
      RAW_DATA_REF,
      RAW_DATA_0,
      RAW_DATA_1,
      RAW_DATA_2,
      RAW_DATA_DIFF_1,
      RAW_DATA_DIFF_2,
      FMT_COUNTERS,
      FMTC_LINKMAP,
      INLINKMAP,
      CUSTOM,
      SUMMARY,
      DISCBIAS_T,
      DISCBIAS_TIMEWALK,
      TUNING_DIR,
      TUNING_FAIL,    
      MAX_HISTO_TYPES
    };
    EnumHistogramType() {
      m_DspHistogramTypeMap["OCCUPANCY"] = OCCUPANCY;
      m_DspHistogramTypeMap["TOT_MEAN"] = TOT_MEAN;
      m_DspHistogramTypeMap["TOT_SIGMA"] = TOT_SIGMA;
      m_DspHistogramTypeMap["MONLEAK_HISTO"] = MONLEAK_HISTO;
      m_HistogramTypeMap["OCCUPANCY"] = OCCUPANCY;
      m_HistogramTypeMap["OCCUPANCY_THR"] = OCCUPANCY_THR;
      m_HistogramTypeMap["TOT_MEAN"] = TOT_MEAN;
      m_HistogramTypeMap["TOT_SIGMA"] = TOT_SIGMA;
      m_HistogramTypeMap["SCURVE_MEAN"] = SCURVE_MEAN;
      m_HistogramTypeMap["SCURVE_SIGMA"] = SCURVE_SIGMA;
      m_HistogramTypeMap["SCURVE_CHI2"] = SCURVE_CHI2;
      m_HistogramTypeMap["TDAC_T"] = TDAC_T;
      m_HistogramTypeMap["TDAC_THR"] = TDAC_THR;  
      m_HistogramTypeMap["GDAC_T"] = GDAC_T;
      m_HistogramTypeMap["GDAC_THR"] = GDAC_THR;  
      m_HistogramTypeMap["FDAC_T"] = FDAC_T;
      m_HistogramTypeMap["FDAC_TOT"] = FDAC_TOT;
      m_HistogramTypeMap["IF_T"] = IF_T;
      m_HistogramTypeMap["IF_TOT"] = IF_TOT;
      m_HistogramTypeMap["TIMEWALK"] = TIMEWALK;
      m_HistogramTypeMap["RAW_DATA_REF"] = RAW_DATA_REF;
      m_HistogramTypeMap["RAW_DATA_0"] = RAW_DATA_0;
      m_HistogramTypeMap["RAW_DATA_1"] = RAW_DATA_1;
      m_HistogramTypeMap["RAW_DATA_2"] = RAW_DATA_2;
      m_HistogramTypeMap["RAW_DATA_DIFF_1"] = RAW_DATA_DIFF_1;
      m_HistogramTypeMap["RAW_DATA_DIFF_2"] = RAW_DATA_DIFF_2;
      m_HistogramTypeMap["FMT_COUNTERS"] = FMT_COUNTERS;
      m_HistogramTypeMap["FMTC_LINKMAP"] = FMTC_LINKMAP;
      m_HistogramTypeMap["INLINKMAP"] = INLINKMAP;
      m_HistogramTypeMap["MONLEAK_HISTO"] = MONLEAK_HISTO;
      m_HistogramTypeMap["CUSTOM"] = CUSTOM;
      m_HistogramTypeMap["SUMMARY"] = SUMMARY;
      //m_HistogramTypeMap["VCAL_THR"] = VCAL_THR;
      m_HistogramTypeMap["DISCBIAS_TIMEWALK"] = DISCBIAS_TIMEWALK;
      m_HistogramTypeMap["DISCBIAS_T"] = DISCBIAS_T;
      //TBi: added for calib debug
      m_HistogramTypeMap["TUNING_DIR"] = TUNING_DIR;
      m_HistogramTypeMap["TUNING_FAIL"] = TUNING_FAIL;
      
    }
     std::map<std::string, int>  EnumDspHistogramTypeMap() {return m_DspHistogramTypeMap;}
     std::map<std::string, int> EnumHistogramTypeMap() {return m_HistogramTypeMap;}
     _reverse_lookup(m_HistogramTypeMap);
  private:
    std::map<std::string, int>  m_DspHistogramTypeMap;
    std::map<std::string, int> m_HistogramTypeMap;
  };
  class EnumMaskStageMode {
  public:
    enum MaskStageMode: int {
      SEL_ENA = SCAN_SEL_ENA,
      SEL = SCAN_SEL,
      ENA = SCAN_ENA,
      XTALK = SCAN_XTALK,
      SENS_XTALK = SCAN_SENS_XTALK,
      SEL_ENA_PRE = SCAN_SEL_ENA_PRE,
      MONLEAK = SCAN_MONLEAK,
      SEL_PRE = SCAN_SEL_PRE,
      STATIC = SCAN_STATIC,
      SCALER_N,
      SCALER_D,
      SCALER_A,
      // FEI4 specific
      FEI4_ENA_NOCAP = SCAN_FEI4_ENA_NOCAP,
      FEI4_ENA_SCAP = SCAN_FEI4_ENA_SCAP,
      FEI4_ENA_LCAP = SCAN_FEI4_ENA_LCAP, 
      FEI4_ENA_BCAP = SCAN_FEI4_ENA_BCAP,
      FEI4_ENA_HITBUS = SCAN_FEI4_ENA_HITBUS,
      FEI4_MONLEAK = SCAN_FEI4_MONLEAK,
      FEI4_XTALK = SCAN_FEI4_XTALK,
      FEI4_NOISE = SCAN_FEI4_NOISE,
      FEI4_ENA_HITBUS_DIG
    };
    EnumMaskStageMode() {
      m_maskStageModeMap["SEL_ENA"]=SEL_ENA;
      m_maskStageModeMap["SEL"]=SEL;
      m_maskStageModeMap["ENA"]=ENA;
      m_maskStageModeMap["XTALK"]=XTALK;
      m_maskStageModeMap["SENS_XTALK"]=SENS_XTALK;
      m_maskStageModeMap["SEL_ENA_PRE"]=SEL_ENA_PRE;
      m_maskStageModeMap["MONLEAK"]=MONLEAK;
      m_maskStageModeMap["SEL_PRE"]=SEL_PRE;
      m_maskStageModeMap["STATIC"]=STATIC;
      m_maskStageModeMap["SCALER_N"]=SCALER_N;
      m_maskStageModeMap["SCALER_D"]=SCALER_D;
      m_maskStageModeMap["SCALER_A"]=SCALER_A;
      // FEI4 specific
      //TODO: review if necessary
      m_maskStageModeMap["FEI4_ENA_NOCAP"]=FEI4_ENA_NOCAP;
      m_maskStageModeMap["FEI4_ENA_SCAP"]=FEI4_ENA_SCAP;
      m_maskStageModeMap["FEI4_ENA_LCAP"]= FEI4_ENA_LCAP;
      m_maskStageModeMap["FEI4_ENA_BCAP"]=FEI4_ENA_BCAP;
      m_maskStageModeMap["FEI4_ENA_HITBUS"]=FEI4_ENA_HITBUS;
      m_maskStageModeMap["FEI4_MONLEAK"]=FEI4_MONLEAK;
      m_maskStageModeMap["FEI4_XTALK"]=FEI4_XTALK;
      m_maskStageModeMap["FEI4_NOISE"]=FEI4_NOISE;
      m_maskStageModeMap["FEI4_ENA_HITBUS_DIG"]=FEI4_ENA_HITBUS_DIG;

    }
    _reverse_lookup(m_maskStageModeMap);
    std::map<std::string, int> EnumMaskStageModeMap() {return  m_maskStageModeMap;}
  private:
    
    std::map<std::string, int>  m_maskStageModeMap ;
  };

  class EnumMaskSteps {
  public:
    enum MaskStageSteps: int {
      STEPS_32,
      STEPS_64,
      STEPS_1,
      //FEI4 specific
      STEPS_1_DC,
      STEPS_2_DC,
      STEPS_4_DC,
      STEPS_8_DC,
      STEPS_8_4DC,
      STEPS_8_8DC
    };

    EnumMaskSteps() {
      m_MaskStageStepsMap["STEPS_32"]=STEPS_32 ;
      m_MaskStageStepsMap["STEPS_64"]=STEPS_64 ;
      m_MaskStageStepsMap["STEPS_1"]=STEPS_1;
      //FEI4 specific
      m_MaskStageStepsMap["STEPS_1_DC"]=STEPS_1_DC;
      m_MaskStageStepsMap["STEPS_2_DC"]=STEPS_2_DC;
      m_MaskStageStepsMap["STEPS_4_DC"]=STEPS_4_DC;
      m_MaskStageStepsMap["STEPS_8_DC"]=STEPS_8_DC;
      m_MaskStageStepsMap["STEPS_8_4DC"]=STEPS_8_4DC;
      m_MaskStageStepsMap["STEPS_8_8DC"]=STEPS_8_8DC;
    }

    _reverse_lookup(m_MaskStageStepsMap);
    std::map<std::string, int> &EnumMaskStageStepsMap () { return m_MaskStageStepsMap;}

    static int getNumberOfSteps(MaskStageSteps mode){
      auto rv = 0;
      switch(mode){
        case STEPS_1:
        case STEPS_1_DC:
          rv = 1;
          break;
        case STEPS_2_DC:
          rv = 2;
          break;
        case STEPS_4_DC:
          rv = 4;
          break;
        case STEPS_8_DC:
        case STEPS_8_4DC:
        case STEPS_8_8DC:
          rv = 8;
          break;
	case STEPS_32:
	  rv = 32;
          break;
	case STEPS_64:
	  rv = 64;
          break;
	default:
	  rv = 1;
      }
      return rv;
    }

  private:
     std::map<std::string, int> m_MaskStageStepsMap;
  };
  class EnumMccBandwidth {
  public:
    enum MccBandwidth: int {
      SINGLE_40=MCC_SINGLE_40, DOUBLE_40=MCC_DOUBLE_40, SINGLE_80=MCC_SINGLE_80, DOUBLE_80=MCC_DOUBLE_80,  MCC_SPEED_UNK = 999
    };
    EnumMccBandwidth() {
      m_MccBandwidthMap["SINGLE_40"]=SINGLE_40;
      m_MccBandwidthMap["DOUBLE_40"]=DOUBLE_40;
      m_MccBandwidthMap["SINGLE_80"]=SINGLE_80;
      m_MccBandwidthMap["DOUBLE_80"]=DOUBLE_80;
    }
    _reverse_lookup(m_MccBandwidthMap);
    std::map<std::string, int> EnumMccBandwidthMap(){return  m_MccBandwidthMap;}
  private:
    std::map<std::string, int> m_MccBandwidthMap;
  };
}
#endif  
