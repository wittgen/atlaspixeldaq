//  bool PixScanBase::preset(ScanType presetName, FEflavour feFlavour) {
  m_FECounter = 0;
  m_tuningStartsFromConfigValues = false;
  m_dynamicThresholdTargetValue = 0;  
  m_runType=NORMAL_SCAN;
  // generally change latency for FE-I4 - works just bit-iverted to FE-I3
  if (isFEI4(feFlavour)) {
    m_LVL1Latency = 15;
    m_strobeLVL1Delay = 240;
  }
  if (presetName == DIGITAL_TEST && feFlavour == PM_FE_I2) { 
    m_maskStageSteps = 32;
    m_strobeDuration = 50;
    m_digitalInjection = true;    
  } 
  else if (presetName == DIGITAL_TEST && (isFEI4(feFlavour))) {
    m_maskStageTotalSteps= STEPS_8_DC;
    m_maskStageMode = FEI4_ENA_NOCAP;
    m_maskStageSteps = 8;
    m_strobeDuration = 50;
    m_digitalInjection = true;    
  }
  else if (presetName == ANALOG_TEST && feFlavour == PM_FE_I2) {
    m_maskStageSteps = 32;
    m_strobeDuration = 500;
    m_feVCal = 400;
    m_digitalInjection = false;    
  } 
  else if (presetName == ANALOG_TEST && (isFEI4(feFlavour))) {
    m_maskStageTotalSteps= STEPS_8_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_repetitions = 200;
    m_maskStageSteps = 8;
    m_strobeDuration = 500;
    m_feVCal = 400;
    m_digitalInjection = false;    
  } else if (presetName == THRESHOLD_SCAN && feFlavour == PM_FE_I2) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageSteps = 32;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
  } else if (presetName == THRESHOLD_SCAN && (isFEI4(feFlavour))) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_repetitions = 200;
    m_maskStageSteps = 2;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
  } else if ( (presetName == TDAC_TUNE || presetName == GDAC_TUNE || presetName == TDAC_TUNE_ITERATED || presetName == GDAC_FINE_FAST_TUNE || presetName == GDAC_TUNE_ITERATED ) 
	     && feFlavour == PM_FE_I2 ) {
    m_repetitions = 25;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    if (presetName == TDAC_TUNE) {
      m_maskStageSteps = 32;
      m_loopParam[1] = TDACS;
      std::vector<float> st;
      st.push_back(70);
      st.push_back(16);
      st.push_back(8);
      st.push_back(4);
      st.push_back(2);
      st.push_back(1);
      setLoopVarValues(1, st);
      m_loopAction[1] = TDAC_TUNING;
      m_histogramFilled[TDAC_T] = true;
      m_histogramKept[TDAC_T] = true;
      m_histogramFilled[TDAC_THR] = true;
      m_histogramKept[TDAC_THR] = true;
      m_thresholdTargetValue = 4000;
    } else if (presetName == TDAC_TUNE_ITERATED) { // GDAC in loop2, TDAC in loop1
      // Loop 1 definition: TDACt steps and number of total iterations
      m_loopParam[1] = TDACS;
      std::vector<float> st;
      st.push_back(0); //Initial points are the original ones in the module config.
      st.push_back(4);
      st.push_back(2);
      st.push_back(1);
      setLoopVarValues(1, st);
      m_loopAction[1] = TDAC_TUNING; // Action is a TDAC tuning
      m_histogramFilled[TDAC_T] = true;
      m_histogramKept[TDAC_T] = true;
      m_histogramFilled[TDAC_THR] = true;
      m_histogramKept[TDAC_THR] = true;

      // Loop 2 definition: GDAC decrement setup
      m_loopActive[2] = true;
      m_maskStageSteps = 32; 
      m_loopParam[2] = GDAC_VARIATION; // Decrement GDAC values here
      std::vector<float> st2;
      st2.push_back(-4);
      st2.push_back(-3);
      st2.push_back(-2);
      setLoopVarValues(2, st2);
      m_loopAction[2] = NO_ACTION; //No action here, only GDAC updates
      m_tuningStartsFromConfigValues = true;
      m_thresholdTargetValue = 0; // Means the target is dinamically calculated
    } else {
      m_maskStageSteps = 8;
      m_loopParam[1] = GDAC;
      setLoopVarValues(1, 10, 30, 3);
      m_loopAction[1] = GDAC_TUNING;
      m_histogramFilled[GDAC_T] = true;
      m_histogramKept[GDAC_T] = true;
      m_histogramFilled[GDAC_THR] = true;
      m_histogramKept[GDAC_THR] = true;
      m_thresholdTargetValue = 4000;
    }
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
    m_restoreModuleConfig = false;


  } else if ( (presetName == TDAC_TUNE || presetName == GDAC_TUNE || presetName == GDAC_COARSE_FAST_TUNE || presetName == GDAC_FINE_FAST_TUNE || 
	       presetName == GDAC_FAST_TUNE || presetName == GDAC_TUNE_ITERATED || presetName == TDAC_TUNE_ITERATED)  && (isFEI4(feFlavour)) ) {
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_repetitions = 25;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    if (presetName == TDAC_TUNE) {
      m_loopParam[1] = TDACS;
      std::vector<float> st;
      st.push_back(16);
      st.push_back(8);
      st.push_back(4);
      st.push_back(2);
      st.push_back(1);
      st.push_back(1);
      setLoopVarValues(1, st);
      m_loopAction[1] = TDAC_TUNING;
      m_histogramFilled[TDAC_T] = true;
      m_histogramKept[TDAC_T] = true;
      m_histogramFilled[TDAC_THR] = true;
      m_histogramKept[TDAC_THR] = true;
      m_thresholdTargetValue = 3000;
    } else if (presetName == TDAC_TUNE_ITERATED) { // GDAC in loop2, TDAC in loop1
      // check if it makes sense to do this for FE-I4
      // Loop 1 definition: TDACt steps and number of total iterations
      m_loopParam[1] = TDACS;
      std::vector<float> st;
      st.push_back(0); //Initial points are the original ones in the module config.
      st.push_back(4);
      st.push_back(2);
      st.push_back(1);
      setLoopVarValues(1, st);
      m_loopAction[1] = TDAC_TUNING; // Action is a TDAC tuning
      m_histogramFilled[TDAC_T] = true;
      m_histogramKept[TDAC_T] = true;
      m_histogramFilled[TDAC_THR] = true;
      m_histogramKept[TDAC_THR] = true;

      // Loop 2 definition: GDAC decrement setup
      m_loopActive[2] = true;
      //m_maskStageSteps = 32; 
      m_loopParam[2] = GDAC_VARIATION; // Decrement GDAC values here
      std::vector<float> st2;
      st2.push_back(-4);
      st2.push_back(-3);
      st2.push_back(-2);
      setLoopVarValues(2, st2);
      m_loopAction[2] = NO_ACTION; //No action here, only GDAC updates
      m_tuningStartsFromConfigValues = true;
      m_thresholdTargetValue = 0; // Means the target is dinamically calculated
    } else {
      m_maskStageSteps = 1;
      m_loopParam[1] = GDAC;
      std::vector<float> st;
      if(presetName == GDAC_FAST_TUNE || presetName == GDAC_FINE_FAST_TUNE){
        st.push_back(160);
        st.push_back(48);
        st.push_back(24);
        st.push_back(12);
        st.push_back(6);
	st.push_back(3);
	st.push_back(1);
	st.push_back(1);
	st.push_back(1);
      }else if(presetName == GDAC_COARSE_FAST_TUNE){
        st.push_back(0);
        st.push_back(1);
      }else{
        st.push_back(60);
        st.push_back(80);
        st.push_back(105);
        st.push_back(140);
        st.push_back(180);
      }
      setLoopVarValues(1, st);
      if(presetName == GDAC_COARSE_FAST_TUNE){
	m_loopAction[1] = GDAC_COARSE_FAST_TUNING;
      }else if(presetName == GDAC_FINE_FAST_TUNE){
	m_loopAction[1] = GDAC_FINE_FAST_TUNING;
      }else{
	m_loopAction[1] = GDAC_TUNING;
      }
      //m_loopAction[1] = GDAC_TUNING;
      m_histogramFilled[GDAC_T] = true;
      m_histogramKept[GDAC_T] = true;
      m_histogramFilled[GDAC_THR] = true;
      m_histogramKept[GDAC_THR] = true;
      m_thresholdTargetValue = 3000;
    }
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = 
    m_restoreModuleConfig = false;

  }  else if (presetName == TDAC_FAST_TUNE && feFlavour == PM_FE_I2) {
    m_repetitions = 100;
    m_maskStageMode = SEL_ENA;
    m_maskStageSteps = 32;
    m_strobeDuration = 500;
    m_feVCal = 0x1fff;              
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = TDACS;

    std::vector<float> tdacSteps;  
    tdacSteps.push_back(64);
    tdacSteps.push_back(32);
    tdacSteps.push_back(16);
    tdacSteps.push_back(8);
    tdacSteps.push_back(4);
    tdacSteps.push_back(2);
    tdacSteps.push_back(1);
    tdacSteps.push_back(1);
    setLoopVarValues(0, tdacSteps);
    m_loopAction[0] = TDAC_FAST_TUNING;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;

    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = true;
    m_histogramFilled[TDAC_T] = true;
    m_histogramKept[TDAC_T] = true;

    m_thresholdTargetValue = 4000;
    m_restoreModuleConfig = false;

  }  else if (presetName == TDAC_FAST_TUNE && (isFEI4(feFlavour))) {
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_feVCal = 0x1fff;              
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = TDACS;

    std::vector<float> tdacSteps;  
    tdacSteps.push_back(16);
    tdacSteps.push_back(8);
    tdacSteps.push_back(4);
    tdacSteps.push_back(2);
    tdacSteps.push_back(1);
    tdacSteps.push_back(1);
    setLoopVarValues(0, tdacSteps);
    m_loopAction[0] = TDAC_FAST_TUNING;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;

    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = true;
    m_histogramFilled[TDAC_T] = true;
    m_histogramKept[TDAC_T] = true;

    m_thresholdTargetValue = 2000;
    m_restoreModuleConfig = false;

  }  else if (presetName == THR_FAST_SCAN && feFlavour == PM_FE_I2) {
    m_maskStageSteps = 32;
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_feVCal = 0x1fff;              
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;

    std::vector<float> vcalSteps;
    vcalSteps.push_back(512);
    vcalSteps.push_back(256);
    vcalSteps.push_back(128);
    vcalSteps.push_back(64);
    vcalSteps.push_back(32);
    vcalSteps.push_back(16);
    vcalSteps.push_back(8);
    vcalSteps.push_back(4);
    vcalSteps.push_back(2);
    vcalSteps.push_back(1);
    vcalSteps.push_back(1);
    setLoopVarValues(0,vcalSteps);
    m_loopAction[0] = THR_FAST_SCANNING;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;

    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = true;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;

    m_restoreModuleConfig = false;

  }  else if (presetName == THR_FAST_SCAN && (isFEI4(feFlavour))) {
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_feVCal = 0x1fff;              
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;

    std::vector<float> vcalSteps;
    vcalSteps.push_back(512);
    vcalSteps.push_back(256);
    vcalSteps.push_back(128);
    vcalSteps.push_back(64);
    vcalSteps.push_back(32);
    vcalSteps.push_back(16);
    vcalSteps.push_back(8);
    vcalSteps.push_back(4);
    vcalSteps.push_back(2);
    vcalSteps.push_back(1);
    vcalSteps.push_back(1);
    setLoopVarValues(0,vcalSteps);
    m_loopAction[0] = THR_FAST_SCANNING;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;

    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = true;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;

    m_restoreModuleConfig = false;

  }  else if ((presetName == FDAC_TUNE || presetName == FDAC_FAST_TUNE || presetName == IF_TUNE) && feFlavour == PM_FE_I2) {
    m_repetitions = 25;
    m_maskStageSteps = 32;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    if (presetName == FDAC_TUNE || presetName == FDAC_FAST_TUNE) {
      m_loopParam[0] = FDACS;
      setLoopVarValues(0, 0, 7, 8);
      m_loopAction[0] = FDAC_TUNING;
      m_histogramFilled[FDAC_T] = true;
      m_histogramKept[FDAC_T] = true;
      m_histogramFilled[FDAC_TOT] = true;
      m_histogramKept[FDAC_TOT] = true;
    } else {
      m_loopParam[0] = IF;
      setLoopVarValues(0, 25, 50, 26);
      m_loopAction[0] = IF_TUNING;
      m_histogramFilled[IF_T] = true;
      m_histogramKept[IF_T] = true;
      m_histogramFilled[IF_TOT] = true;
      m_histogramKept[IF_TOT] = true;
    }
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = false;
    m_totTargetValue = 30;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = false;
    m_feVCal = 0x1fff;
  } else if ((presetName == FDAC_TUNE || presetName == FDAC_FAST_TUNE || presetName == IF_TUNE) && (isFEI4(feFlavour)) ) {
    m_repetitions = 25;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    if (presetName == FDAC_TUNE || presetName == FDAC_FAST_TUNE) {
      m_loopParam[0] = FDACS;
      setLoopVarValues(0, 0, 15, 16);
      m_loopAction[0] = FDAC_TUNING;
      m_histogramFilled[FDAC_T] = true;
      m_histogramKept[FDAC_T] = true;
      m_histogramFilled[FDAC_TOT] = true;
      m_histogramKept[FDAC_TOT] = true;
    } else {
      m_loopParam[0] = IF;
      setLoopVarValues(0, 0, 250, 26);
      m_loopAction[0] = IF_TUNING;
      m_histogramFilled[IF_T] = true;
      m_histogramKept[IF_T] = true;
      m_histogramFilled[IF_TOT] = true;
      m_histogramKept[IF_TOT] = true;
    }
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = false;
    m_totTargetValue = 6;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = false;
    m_feVCal = 0x1fff;
  } else if (presetName == TOT_VERIF && feFlavour == PM_FE_I2) {
    m_maskStageSteps = 32;
    m_repetitions = 200;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    setLoopVarValues(0, 0, 0, 1);
    m_loopParam[0] = NO_PAR;
    m_loopAction[0] = FDAC_TUNING;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = true;
    m_totTargetValue = 6;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = true;
    m_feVCal = 0x1fff;
  } else if (presetName == TOT_VERIF && (isFEI4(feFlavour)) ) {
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_repetitions = 200;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    setLoopVarValues(0, 0, 0, 1);
    m_loopParam[0] = NO_PAR;
    m_loopAction[0] = FDAC_TUNING;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = true;
    m_totTargetValue = 6;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = true;
    m_feVCal = 0x1fff;
  } else if (presetName == TOT_CALIB && feFlavour == PM_FE_I2) {
    m_repetitions = 50;
    m_maskStageSteps = 32;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 70, 950, 23);
    m_loopAction[0] = TOTCAL_FIT; 
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = true;
  } else if (presetName == TOT_CALIB && (isFEI4(feFlavour))) {
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 100, 700, 21);
    m_loopAction[0] = NO_ACTION; 
    m_dspLoopAction[0] = false;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = true;
  } else if (presetName == T0_SCAN && feFlavour == PM_FE_I2) {
    m_repetitions = 25;
    m_maskStageSteps = 32;
    m_feVCal = 0x1fff;
    m_totTargetCharge = 50000;
    m_chargeInjCapHigh = true;
    m_consecutiveLvl1Trig = 1;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = STROBE_DELAY;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 63, 64);
    m_loopAction[0] = SCURVE_FIT; // nothing
    m_dspLoopAction[0] = true;   //false 
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    m_loopParam[1] = TRIGGER_DELAY;
    setLoopVarValues(1, 238, 240, 2); 
    m_loopAction[1] = T0_SET;
/*     m_loopParam[2] = STROBE_DEL_RANGE; */
/*     m_loopActive[2] = true; */
/*     m_dspProcessing[2] = false; */
/*     m_dspLoopAction[2] = false; */
/*     setLoopVarValues(2, 3, 8, 6); //248,251,4 */
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
    m_histogramFilled[TIMEWALK] = false;
    m_histogramKept[TIMEWALK] = false;
    m_restoreModuleConfig = false;
  } else if (presetName == T0_SCAN && (isFEI4(feFlavour))) {
    m_repetitions = 25;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_feVCal = 0x1fff;
    m_totTargetCharge = 50000;
    m_strobeMCCDelayRange = 100;// used now to set FE-I4 PlsrIdacRamp
    m_consecutiveLvl1Trig = 1;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = STROBE_DELAY;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 62, 63);// 63 does not work on FE-I4
    m_loopAction[0] = SCURVE_FIT; // nothing
    m_dspLoopAction[0] = true;   //false 
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    m_loopParam[1] = TRIGGER_DELAY;
    setLoopVarValues(1, 252, 254, 3);
    m_loopAction[1] = T0_SET;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
    m_histogramFilled[TIMEWALK] = false;
    m_histogramKept[TIMEWALK] = false;
    m_restoreModuleConfig = false;
  }else if (presetName == DISCBIAS_TUNE) {
		m_repetitions = 25;
		m_feVCal = 1000;
		m_totTargetCharge = 100000;
		m_chargeInjCapHigh = true;
		//m_strobeFrequency = 4000;
		//m_singleDCloop = true;
		//m_avoidSpecialsCols = false;
		m_maskStageTotalSteps = STEPS_32;
		m_maskStageSteps = 16;
		m_consecutiveLvl1Trig = 1;
		m_strobeDuration = 500;
		m_strobeLVL1DelayOverride = false;
		m_digitalInjection = false;
		m_loopActive[0] = true;
		m_loopParam[0] = STROBE_DELAY;
		m_dspProcessing[0] = false;
		setLoopVarValues(0, 0, 63, 64);
		m_dspLoopAction[0] = false;
		m_loopAction[0] = MCCDEL_FIT;
		m_loopActive[1] = true;
		m_dspProcessing[1] = false;
		m_dspLoopAction[1] = false;
		m_loopParam[1] = DISCBIAS;
		std::vector<float> st;
		st.push_back(-1);
		st.push_back(8);
		st.push_back(4);
		st.push_back(2);
		st.push_back(1);
		st.push_back(1);
		setLoopVarValues(1, st);
		m_loopAction[1] = DISCBIAS_TUNING;
		m_histogramFilled[OCCUPANCY] = true;
		m_histogramKept[OCCUPANCY] = true;
		m_histogramFilled[TIMEWALK] = true;
		m_histogramKept[TIMEWALK] = true;
		m_histogramFilled[DISCBIAS_T] = true;
		m_histogramKept[DISCBIAS_T] = true;
		m_histogramFilled[DISCBIAS_TIMEWALK] = true;
		m_histogramKept[DISCBIAS_TIMEWALK] = true;
		m_restoreModuleConfig = false;
	} else if (presetName == TIMEWALK_MEASURE && feFlavour == PM_FE_I2) {
    m_repetitions = 25;
    m_maskStageSteps = 32;
    m_chargeInjCapHigh = false;
    m_consecutiveLvl1Trig = 1;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_strobeLVL1DelayOverride = false;
    m_loopActive[0] = true;
    m_loopParam[0] = STROBE_DELAY;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 63, 64);
    m_loopAction[0] = MCCDEL_FIT;
    m_dspLoopAction[0] = false;//true;
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    m_loopParam[1] = VCAL;
    std::vector<float> vcalVals;
    vcalVals.push_back(70);
    vcalVals.push_back(75);
    vcalVals.push_back(80);
    vcalVals.push_back(85);
    vcalVals.push_back(90);
    vcalVals.push_back(95);
    vcalVals.push_back(100);
    vcalVals.push_back(110);
    vcalVals.push_back(120);
    vcalVals.push_back(130);
    vcalVals.push_back(140);
    vcalVals.push_back(160);
    vcalVals.push_back(180);
    vcalVals.push_back(200);
    vcalVals.push_back(220);
    vcalVals.push_back(240);
    vcalVals.push_back(280);
    vcalVals.push_back(320);
    vcalVals.push_back(360);
    vcalVals.push_back(400);
    vcalVals.push_back(500);
    vcalVals.push_back(600);
    vcalVals.push_back(700);
    vcalVals.push_back(800);
    vcalVals.push_back(1000);
    setLoopVarValues(1, vcalVals);
    m_loopAction[1] = NO_ACTION;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TIMEWALK] = true;
    m_histogramKept[TIMEWALK] = true;
    m_restoreModuleConfig = true;
  } else if (presetName == TIMEWALK_MEASURE && (isFEI4(feFlavour))) {
    // preliminary, to be checked
    m_repetitions = 25;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_consecutiveLvl1Trig = 1;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_strobeLVL1DelayOverride = false;
    m_loopActive[0] = true;
    m_loopParam[0] = STROBE_DELAY;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 63, 64);
    m_loopAction[0] = MCCDEL_FIT;
    m_dspLoopAction[0] = false;//true;
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    m_loopParam[1] = VCAL;
    std::vector<float> vcalVals;
    vcalVals.push_back(70);
    vcalVals.push_back(75);
    vcalVals.push_back(80);
    vcalVals.push_back(85);
    vcalVals.push_back(90);
    vcalVals.push_back(95);
    vcalVals.push_back(100);
    vcalVals.push_back(110);
    vcalVals.push_back(120);
    vcalVals.push_back(130);
    vcalVals.push_back(140);
    vcalVals.push_back(160);
    vcalVals.push_back(180);
    vcalVals.push_back(200);
    vcalVals.push_back(220);
    vcalVals.push_back(240);
    vcalVals.push_back(280);
    vcalVals.push_back(320);
    vcalVals.push_back(360);
    vcalVals.push_back(400);
    vcalVals.push_back(500);
    vcalVals.push_back(600);
    vcalVals.push_back(700);
    vcalVals.push_back(800);
    vcalVals.push_back(1000);
    setLoopVarValues(1, vcalVals);
    m_loopAction[1] = NO_ACTION;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TIMEWALK] = true;
    m_histogramKept[TIMEWALK] = true;
    m_restoreModuleConfig = true;
  } else if (presetName == INTIME_THRESH_SCAN && feFlavour == PM_FE_I2) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageSteps = 32;
    m_consecutiveLvl1Trig = 1;
    m_strobeLVL1DelayOverride = false;
    m_strobeMCCDelayRange = 31;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
  } else if (presetName == INTIME_THRESH_SCAN && (isFEI4(feFlavour))) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_consecutiveLvl1Trig = 1;
    m_strobeLVL1DelayOverride = false;
    m_strobeMCCDelayRange = 31;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;


  } else if (presetName == CROSSTALK_SCAN && feFlavour == PM_FE_I2) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageSteps = 32;
    m_maskStageMode = XTALK;
    m_digitalInjection = false;
    m_chargeInjCapHigh = true;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 1000, 26);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
  } else if (presetName == CROSSTALK_SCAN && (isFEI4(feFlavour))) {
    m_repetitions = 100;
    m_strobeDuration = 500;
    m_maskStageTotalSteps= STEPS_8_DC;
    m_maskStageSteps = 8;
    m_maskStageMode = XTALK;
    m_digitalInjection = false;
    m_chargeInjCapHigh = true;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 1000, 26);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;

  } else if (presetName == TWOTRIGGER_THRESHOLD) {

    m_maskStageTotalSteps= STEPS_8_8DC;
    m_maskStageMode = FEI4_ENA_SCAP;
    m_LVL1Latency = 21; //FE-I4
    
    m_consecutiveLvl1Trig = 8;  //Can't be more than 8, since only 16 triggers possible
    
    m_repetitions = 50;
    m_strobeDuration = 12;
    m_strobeMCCDelay= 0;
    m_maskStageSteps = 8; //  120;//normally 40
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 000, 300, 101);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;

  } else if (presetName == INCREMENTAL_TDAC_SCAN && feFlavour == PM_FE_I2) {
    m_repetitions = 25;
    m_maskStageSteps = 32;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_loopParam[0] = VCAL;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 200, 201);
    m_loopAction[0] = SCURVE_FIT;
    m_dspLoopAction[0] = true;
    m_loopActive[1] = true;
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;
    m_loopParam[1] = TDACS_VARIATION;
    std::vector<float> st;
    st.push_back(-5);
    setLoopVarValues(1, st);
    setLoopVarValuesFree(1);
    m_loopAction[1] = MIN_THRESHOLD;
    m_histogramFilled[TDAC_T] = true;
    m_histogramKept[TDAC_T] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[SCURVE_MEAN] = true;
    m_histogramKept[SCURVE_MEAN] = true;
    m_histogramFilled[SCURVE_SIGMA] = true;
    m_histogramKept[SCURVE_SIGMA] = true;
    m_histogramFilled[SCURVE_CHI2] = true;
    m_histogramKept[SCURVE_CHI2] = true;
  } else if (presetName == INCREMENTAL_TDAC_SCAN && (isFEI4(feFlavour))) {
    // JGK: not sure if this is needed, to do nothing for now
    m_repetitions = 0;
    m_maskStageSteps = 0;
    m_maskStageTotalSteps= STEPS_1;
    m_maskStageMode = ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = false;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
  } else if (presetName == BOC_RX_DELAY_SCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 24, 25);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_runType = RAW_EVENT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_V0_RX_DELAY_SCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 24, 13);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_VPH0;
    m_dspProcessing[1] = false;
    setLoopVarValues(1, 0, 24, 13);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = RAW_EVENT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_2] = true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
    m_mccBandwidth = SINGLE_80;
  } else if (presetName == BOC_THR_RX_DELAY_SCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 24, 25);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_RX_THR;
    m_dspProcessing[1] = false;
    setLoopVarValues(1, 50, 250, 6);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = RAW_EVENT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_2] = true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_THR_DEL_DSP) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 0, 24, 25);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_RX_THR;
    m_dspProcessing[1] = true;
    setLoopVarValues(1, 50, 250, 20);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = RAW_EVENT;
    //    m_restoreModuleConfig = false;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] =true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
  } else if (presetName == STO_DSP) {
    m_repetitions = 1000;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_THR;
    m_dspProcessing[0] = true;
    setLoopVarValues(0, 50, 250, 5);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = false;
    m_runType = RAW_EVENT;
    //    m_restoreModuleConfig = false;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == LEAK_SCAN_DSP) {
    m_runType = MONLEAKRUN;
    m_loopActive[0] = true; 
    m_loopAction[0] = NO_ACTION; 
    m_loopParam[0] = IF; 
    m_dspProcessing[0] = false; 
    setLoopVarValues(0, 1, 1, 1); 
    m_loopActive[1] = true; 
    m_loopAction[1] = NO_ACTION; 
    m_loopParam[1] = TRIMF; 
    m_dspProcessing[1] = false; 
    setLoopVarValues(1, 1, 1, 1); 
    m_loopActive[2] = true; 
    m_loopAction[2] = NO_ACTION; 
    m_loopParam[2] = FDACS; 
    m_dspProcessing[2] = false; 
    setLoopVarValues(2, 1, 1, 1); 
    m_repetitions = 1; 
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[MONLEAK_HISTO] = true;
    m_histogramKept[MONLEAK_HISTO] = true;
    m_maskStageSteps = 32;//because  1 mask stage = 1/2 x FE
  } else if (presetName == BOC_THR_DEL_LONG) {
    m_repetitions = 3;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 24, 25);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_RX_THR;
    m_dspProcessing[1] = false;
    setLoopVarValues(1, 50, 250, 6);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = RAW_EVENT;
    m_patternSeeds.clear();
    m_patternSeeds.push_back(0xffff);
    for (int ip=0; ip<10; ip++) {
      m_patternSeeds.push_back(0x3e5aa);
      m_patternSeeds.push_back(0x23f55);
      m_patternSeeds.push_back(0x13f55);
    }
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = false;
    m_histogramKept[RAW_DATA_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_RX_THR_SCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_THR;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 250, 13);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_runType = RAW_EVENT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = true;
    m_histogramFilled[RAW_DATA_DIFF_2] = true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_THR_DEL_FAST) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_RX_DELAY;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 24, 25);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_RX_THR;
    m_dspProcessing[1] = false;
    setLoopVarValues(1, 0, 250, 26);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = FMT_COUNT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_1] = true;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = true;
    m_histogramKept[RAW_DATA_DIFF_2] = true;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == MSR_MEASUREMENT) {
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_TX_MS;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 31, 32);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_runType = MSR_MEASURE;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_1] = false;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = false;
    m_histogramKept[RAW_DATA_DIFF_2] = false;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == SHAPESCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_VPH;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 49, 50);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_RX_THR;
    m_dspProcessing[1] = false;
    setLoopVarValues(1, 0, 255, 27);
    m_loopAction[1] = NO_ACTION;
    m_dspLoopAction[1] = false;
    m_runType = FMT_COUNT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = false;
    m_histogramFilled[RAW_DATA_2] = true;
    m_histogramKept[RAW_DATA_2] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = false;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = false;
    m_histogramKept[RAW_DATA_DIFF_2] = false;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_TUNE) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_loopActive[0] = true;
    m_loopParam[0] = BOC_VPH;
    m_dspProcessing[0] = false;
    setLoopVarValues(0, 0, 49, 50);
    m_loopAction[0] = NO_ACTION;
    m_dspLoopAction[0] = false;
    m_loopActive[1] = true;
    m_loopParam[1] = BOC_THR_TUNE;
    m_dspProcessing[1] = false;
    std::vector<float> thrTuneVals;
    thrTuneVals.push_back(128);
    thrTuneVals.push_back(64);
    thrTuneVals.push_back(32);
    thrTuneVals.push_back(16);
    thrTuneVals.push_back(8);
    thrTuneVals.push_back(4);
    thrTuneVals.push_back(2);
    thrTuneVals.push_back(1);
    setLoopVarValues(1, thrTuneVals);
    m_loopAction[1] = BOC_TUNING;
    m_dspLoopAction[1] = false;
    m_runType = FMT_COUNT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_2] = true;
    m_histogramKept[RAW_DATA_2] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = false;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = false;
    m_histogramKept[RAW_DATA_DIFF_2] = false;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == OPTO_TUNE) {

    // the OPTO_TUNE is looping over the visets using a nested
    // interval search. During that loop it performs a loop over the
    // treshold and delay also using nested intervals. for the latter,
    // see BOC_TUNE.
    // THIS DOES NOT YET WORK ON B-LAYER MODULES. 

    // define loop 0 attributes: delays 
    m_loopActive[0] = true;
    m_loopAction[0] = NO_ACTION;
    m_loopParam[0]  = BOC_VPH;
    setLoopVarValues(0, 0, 49, 50);
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;

    // define loop 1 attributes: thresholds 
    m_loopActive[1] = true;
    m_loopAction[1] = BOC_TUNING;
    m_loopParam[1]  = BOC_THR_TUNE;
    std::vector<float> thrTuneVals;
    thrTuneVals.push_back(128);
    thrTuneVals.push_back(64);
    thrTuneVals.push_back(32);
    thrTuneVals.push_back(16);
    thrTuneVals.push_back(8);
    thrTuneVals.push_back(4);
    thrTuneVals.push_back(2);
    thrTuneVals.push_back(1);
    setLoopVarValues(1, thrTuneVals);
    m_dspProcessing[1] = false;
    m_dspLoopAction[1] = false;

    // define loop 2 attributes: viset 
    m_loopActive[2] = true;
    m_loopAction[2] = OPTO_TUNING;
    m_loopParam[2] = OPTO_VISET; 
    std::vector<float> OPTOTuneVals; 
    OPTOTuneVals.push_back(800.0); 
    OPTOTuneVals.push_back(100.0); 
    OPTOTuneVals.push_back( 50.0); 
    OPTOTuneVals.push_back( 25.0); 
    OPTOTuneVals.push_back( 12.0); 
    OPTOTuneVals.push_back(  6.0); 
    setLoopVarValues(2, OPTOTuneVals); 
    // define scan attributes 
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_runType = FMT_COUNT;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[RAW_DATA_0] = true;
    m_histogramKept[RAW_DATA_0] = false;
    m_histogramFilled[RAW_DATA_1] = true;
    m_histogramKept[RAW_DATA_1] = true;
    m_histogramFilled[RAW_DATA_2] = true;
    m_histogramKept[RAW_DATA_2] = true;
    m_histogramFilled[RAW_DATA_DIFF_1] = false;
    m_histogramKept[RAW_DATA_DIFF_1] = false;
    m_histogramFilled[RAW_DATA_DIFF_2] = false;
    m_histogramKept[RAW_DATA_DIFF_2] = false;
    m_histogramFilled[RAW_DATA_REF] = true;
    m_histogramKept[RAW_DATA_REF] = true;
  } else if (presetName == BOC_LINKSCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_runType = FMT_COUNT_LINKSCAN;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[FMTC_LINKMAP] = true;
    m_histogramKept[FMTC_LINKMAP] = true;
  } else if (presetName == BOC_INLINKSCAN) {
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_runType = IN_LINK_SCAN;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[INLINKMAP] = true;
    m_histogramKept[INLINKMAP] = true;
  } else if (presetName == IF_FAST_TUNE && feFlavour == PM_FE_I2){
    m_repetitions = 25;
    m_maskStageSteps = 32;
    m_maskStageMode = SEL_ENA;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    std::vector<float> ifSteps;  
    ifSteps.push_back(32);
    ifSteps.push_back(16);
    ifSteps.push_back(8);
    ifSteps.push_back(4);
    ifSteps.push_back(2);
    ifSteps.push_back(1);
    setLoopVarValues(0, ifSteps);
    m_loopAction[0] = IF_FAST_TUNING;
    m_loopParam[0] = IF;
    m_histogramFilled[IF_T] = true;
    m_histogramKept[IF_T] = true;
    m_histogramFilled[IF_TOT] = true;
    m_histogramKept[IF_TOT] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = false;
    m_totTargetValue = 30;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = false;
    m_feVCal = 0x1fff;
  } else if (presetName == IF_FAST_TUNE && (isFEI4(feFlavour))){
    m_repetitions = 25;
    m_maskStageTotalSteps= STEPS_2_DC;
    m_maskStageMode = FEI4_ENA_BCAP; 
    m_maskStageSteps = 2;
    m_strobeDuration = 500;
    m_digitalInjection = false;
    m_loopActive[0] = true;
    m_dspProcessing[0] = false;
    m_dspLoopAction[0] = false;
    std::vector<float> ifSteps;  
    ifSteps.push_back(125);
    ifSteps.push_back(64);
    ifSteps.push_back(32);
    ifSteps.push_back(16);
    ifSteps.push_back(8);
    ifSteps.push_back(4);
    ifSteps.push_back(2);
    ifSteps.push_back(1);
    setLoopVarValues(0, ifSteps);
    m_loopAction[0] = IF_FAST_TUNING;
    m_loopParam[0] = IF;
    m_histogramFilled[IF_T] = true;
    m_histogramKept[IF_T] = true;
    m_histogramFilled[IF_TOT] = true;
    m_histogramKept[IF_TOT] = true;
    m_histogramFilled[OCCUPANCY] = true;
    m_histogramKept[OCCUPANCY] = false;
    m_histogramFilled[TOT_MEAN] = true;
    m_histogramKept[TOT_MEAN] = true;
    m_histogramFilled[TOT_SIGMA] = true;
    m_histogramKept[TOT_SIGMA] = false;
    m_totTargetValue = 6;
    m_totTargetCharge = 20000;
    m_restoreModuleConfig = false;
    m_feVCal = 0x1fff;
  } else if (presetName == REGISTER_TEST) {
    // probably OK for FE-I3 and FE-I4, so no need for separate case
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_runType = REG_TEST;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
  } else if (presetName == REGISTER_TEST_SEU){
    // probably OK for FE-I3 and FE-I4, so no need for separate case
    m_repetitions = 10;
    m_maskStageSteps = 0;
    m_maskStageMode = STATIC;
    m_runType = REG_TEST_SEU;
    m_histogramFilled[OCCUPANCY] = false;
    m_histogramKept[OCCUPANCY] = false;
  } else {
    std::cout<<"Scan not implemented"<<std::endl;
    //throw PixScanExc(PixControllerExc::ERROR, "Undefined scan preset");
    return false;
  }
  m_scanTypeEnum=presetName;
  

  return true;
//}

