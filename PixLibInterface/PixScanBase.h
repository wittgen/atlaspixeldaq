/*
 * PixScanBase.h
 *
 *  Created on: Jul 29, 2011
 * 
 * This PixLib class contains the scan configuration.
 * Keep this file in sync with PixLib 
 */


#ifndef PIXSCANBASE_H_
#define PIXSCANBASE_H_
#include <string>
#include <vector>
#include <assert.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include "PixEnumBase.h"
#define MAX_GROUPS 4
#define MAX_LOOPS 3

namespace PixLib {
  class PixScanBase : public EnumRunType,public  EnumScanParam ,
    public EnumEndLoopAction, public EnumHistogramType,public  EnumMaskStageMode, public EnumMaskSteps,
    public EnumMccBandwidth, public EnumScanType,public EnumFEflavour, public EnumScanFitMethod
   {
public:
	PixScanBase(): m_actualFE(-1) {
	}

       ~PixScanBase() = default;

 protected:
	//This seems to be used in a single, maybe not relevant, place, check
	ScanType m_scanTypeEnum;
	MaskStageMode m_maskStageMode;
	int m_maskStageSteps;
	bool m_tuningStartsFromConfigValues;
	bool m_useTuningPointsFile;
	std::string m_tuningPointsFile;
	int m_repetitions;
	//Used, but apparently only as a hack to see if it's a noise scan
	bool m_selfTrigger;
	bool m_strobeLVL1DelayOverride;
	int m_strobeLVL1Delay;
	int m_LVL1Latency;
	int m_strobeDuration;
	int m_strobeMCCDelay;
	int m_strobeMCCDelayRange;
	bool m_overrideStrobeMCCDelay;
	bool m_overrideStrobeMCCDelayRange;
	unsigned int m_moduleMask[MAX_GROUPS];
	bool m_configEnabled[MAX_GROUPS];
	bool m_triggerEnabled[MAX_GROUPS];
	bool m_strobeEnabled[MAX_GROUPS];
	bool m_readoutEnabled[MAX_GROUPS];
	//Used as a hack to set the sleep delay in noise scan
	int m_superGroupTrigDelay;
	int m_consecutiveLvl1Trig;
	bool m_histogramFilled[MAX_HISTO_TYPES];
	bool m_histogramKept[MAX_HISTO_TYPES];
	MccBandwidth m_mccBandwidth;
	bool m_digitalInjection;
	bool m_chargeInjCapHigh;
	//Used, seems to override Fei3::GlobReg::CEU_Clock in scan
	int m_columnROFreq;
	int m_feVCal;
	//Used, seems to override Fei3::GlobReg::ReadMode in scan
	int m_totThrMode;
	//Used, seems to override Fei3::GlobReg::THRMIN in scan
	int m_totMin;
	//Used, seems to override Fei3::GlobReg::THRDUB in scan
	int m_totDHThr;
	int m_totTimeStampMode;
	bool m_cloneCfgTag;
	bool m_cloneModCfgTag;
	std::string m_tagSuffix;

	bool m_restoreModuleConfig;
	bool m_useEmulator;
	int m_nHitsEmu;
	RunType m_runType;
	bool m_loopActive[MAX_LOOPS];
	ScanParam m_loopParam[MAX_LOOPS];
	bool m_dspProcessing[MAX_LOOPS];
	bool m_dspMaskStaging;
	//Used in a single place
	bool m_innerLoopSwap;
	std::vector<float> m_loopVarValues[MAX_LOOPS];
	std::vector<std::string> m_tuningPointsMod;
	std::vector<std::string> m_tuningPointsThr;
	std::vector<std::string> m_tuningPointsToT;
	int m_loopVarNSteps[MAX_LOOPS];
	float m_loopVarMin[MAX_LOOPS];
	float m_loopVarMax[MAX_LOOPS];
	//Is set in some places, but never read, probably can go; also seems to be the inverse of next variables
	bool m_loopVarUniform[MAX_LOOPS];
	bool m_loopVarValuesFree[MAX_LOOPS];
	EndLoopAction m_loopAction[MAX_LOOPS];
	//This is stil used, but may actually not be supported, more detailed follow up needed
	bool m_dspLoopAction[MAX_LOOPS];
	MaskStageSteps m_maskStageTotalSteps;
	// Scan execution
	int m_loopIndex[MAX_LOOPS];
	//Used, but probably can be refactored, I think we only support loop 0 masks
	int m_maskStageIndex;
	//Maybe used, seems to be used when mask staging is done on host
	bool m_newMaskStage; 
	//Maybe used, seems to be used when mask staging is done on host
	bool m_newScanStep;
	//Used in FE-by-FE mode, is this still supported? Follow-up
	bool m_newFE;
	bool m_loopTerminating[MAX_LOOPS];
	bool m_loopEnded[MAX_LOOPS];
	//Next four are for FE-by-FE mode
	bool m_FEbyFE;
	int m_FECounter;
	int m_actualFE;
	unsigned int m_FEbyFEMask;
	// Scan specific parameters
	int m_thresholdTargetValue;
	//Is used in two maybe not used places, follow up
	int m_dynamicThresholdTargetValue;
	int m_totTargetValue;
	int m_totTargetCharge;
	unsigned int m_useAltModLinks;
	//Used in one place, but weird using a string
	std::string m_swapInLinks;
	//Used in one place, but weird using a string
	std::string m_swapOutLinks;
	FitMethod m_fitMethod;
      
 public:
	void setScanTypeEnum(ScanType scanType){
	  m_scanTypeEnum=scanType;
	}
	ScanType getScanType(){
	  return m_scanTypeEnum;
	}
	MaskStageMode getMaskStageMode() {
	  return m_maskStageMode;
	};
	void setMaskStageMode(MaskStageMode stageMode) {
	  m_maskStageMode = stageMode;
	};
	MaskStageSteps getMaskStageTotalSteps() {
	  return m_maskStageTotalSteps;
	};
	void setMaskStageTotalSteps(MaskStageSteps nSteps) {
	  m_maskStageTotalSteps = nSteps;
	};
	int getMaskStageSteps() {
	  return m_maskStageSteps;
	};
	void setMaskStageSteps(int nSteps) {
	  m_maskStageSteps = nSteps;
	};
	bool tuningStartsFromCfgValues() {
	  return m_tuningStartsFromConfigValues;
	};
	bool useTuningPointsFile() {
	  return m_useTuningPointsFile;
	};
	std::string tuningPointsFile() {
	  return m_tuningPointsFile;
	};
	void tuningStartsFromCfgValues(bool isFromCfg) {
	  m_tuningStartsFromConfigValues = isFromCfg;
	};
	void useTuningPointsFile(bool usetuningpointsfile) {
	  m_useTuningPointsFile = usetuningpointsfile;
	};
	void tuningPointsFile(std::string tuningpointsfile) {
	  m_tuningPointsFile = tuningpointsfile;
	};
	int getRepetitions() {
	  return m_repetitions;
	};
	void setRepetitions(int nRep) {
	  m_repetitions = nRep;
	};
	bool getSelfTrigger() {
	  return m_selfTrigger;
	};
	void setSelfTrigger(bool self) {
	  m_selfTrigger = self;
	};
	bool getStrobeLVL1DelayOverride() {
	  return m_strobeLVL1DelayOverride;
	};
	void setStrobeLVL1DelayOveride(bool lvl1StrDelayOverride) {
	  m_strobeLVL1DelayOverride = lvl1StrDelayOverride;
	};
	int getStrobeLVL1Delay() {
	  return m_strobeLVL1Delay;
	};
	void setStrobeLVL1Delay(int lvl1StrDelay) {
	  m_strobeLVL1Delay = lvl1StrDelay;
	};
	int getLVL1Latency() {
	  return m_LVL1Latency;
	};
	void setLVL1Latency(int lvl1Latency) {
	  m_LVL1Latency = lvl1Latency;
	};
	int getStrobeMCCDelay() {
	  return m_strobeMCCDelay;
	};
	void setStrobeMCCDelay(int delay) {
	  m_strobeMCCDelay = delay;
	};
	bool getOverrideStrobeMCCDelay() {
	  return m_overrideStrobeMCCDelay;
	};
	void setOverrideStrobeMCCDelay(bool change) {
	  m_overrideStrobeMCCDelay = change;
	};
	int getStrobeMCCDelayRange() {
	  return m_strobeMCCDelayRange;
	};
	void setStrobeMCCDelayRange(int delayRange) {
	  m_strobeMCCDelayRange = delayRange;
	};
	bool getOverrideStrobeMCCDelayRange() {
	  return m_overrideStrobeMCCDelayRange;
	};
	void setOverrideStrobeMCCDelayRange(bool change) {
	  m_overrideStrobeMCCDelayRange = change;
	};
	int getStrobeDuration() {
	  return m_strobeDuration;
    };
    void setStrobeDuration(int strobeDuration) {
      m_strobeDuration = strobeDuration;
    };
    unsigned int getModuleMask(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_moduleMask[group];
      }
      return 0;
    };
    void setModuleMask(int group, unsigned int mask) {
      if (group >=0 && group < MAX_GROUPS) {
	m_moduleMask[group] = mask;
      }
    }; 
    bool getConfigEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_configEnabled[group];
      }
      return false;
    };
    void setConfigEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_configEnabled[group] = ena;
      }
    };
    bool getTriggerEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_triggerEnabled[group];
      }
      return false;
    };
    void setTriggerEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_triggerEnabled[group] = ena;
      }
    };
    bool getStrobeEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_strobeEnabled[group];
      }
      return false;
    };
    void setStrobeEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_strobeEnabled[group] = ena;
      }
    };
    bool getReadoutEnabled(int group) {
      if (group >=0 && group < MAX_GROUPS) {
	return m_readoutEnabled[group];
      }
      return false;
    };
    void setReadoutEnabled(int group, bool ena) {
      if (group >=0 && group < MAX_GROUPS) {
	m_readoutEnabled[group] = ena;
      }
    };
    int getSuperGroupTrigDelay() {
      return m_superGroupTrigDelay;
    };
    void setSuperGroupTrigDelay(int delay) {
      m_superGroupTrigDelay = delay;
    };
    int getConsecutiveLvl1Trig() {
	return m_consecutiveLvl1Trig;
    };
    void setConsecutiveLvl1Trig(int nLvl1) {
	m_consecutiveLvl1Trig = nLvl1;
    };
    MccBandwidth getMccBandwidth() {
      return m_mccBandwidth;
    };
    void setMccBandwidth(MccBandwidth bw) {
      m_mccBandwidth = bw;
    };
    bool getHistogramFilled(HistogramType type) {
      return m_histogramFilled[type];
    };
    void setHistogramFilled(HistogramType type, bool fill) {
      m_histogramFilled[type] = fill;
    };
    bool getHistogramKept(HistogramType type) {
      return m_histogramKept[type];
    };
    void setHistogramKept(HistogramType type, bool keep) {
      m_histogramKept[type] = keep;
    };
    bool getDigitalInjection() {
      return m_digitalInjection;
    };
    void setDigitalInjection(bool digiInj) {
      m_digitalInjection = digiInj;
    };
    bool getChargeInjCapHigh() {
      return m_chargeInjCapHigh;
    };
    void setChargeInjCapHigh(bool capHigh) {
      m_chargeInjCapHigh = capHigh;
    };
    int getColumnROFreq() {
      return m_columnROFreq;
    };
    void setColumnROFreq(int colROFreq) {
      m_columnROFreq = colROFreq;
    };
    int getFeVCal() {
      return m_feVCal;
    };
    void setFeVCal(int vcal) {
      m_feVCal = vcal;
    };
    int getTotThrMode() {
      return m_totThrMode;
    };
    void setTotThrMode(int totMode) {
      m_totThrMode = totMode;
    };
    int getTotMin() {
      return m_totMin;
    };
    void setTotMin(int totMin) {
      m_totMin = totMin;
    };
    int getTotDHThr() {
      return m_totDHThr;
    };
    void setTotDHThr(int totDHThr) {
      m_totDHThr = totDHThr;
    };
    int getTotTimeStampMode() {
      return m_totTimeStampMode;
    };
    void setTotTimeStampMode(int totMode) {
      m_totTimeStampMode = totMode;
    };
   bool getRestoreModuleConfig() {
      return m_restoreModuleConfig;
    }
    void setRestoreModuleConfig(bool restore) {
      m_restoreModuleConfig = restore;
    }
    bool getUseEmulator() {
      return m_useEmulator;
    }
    void setUseEmulator(bool useEmu) {
      m_useEmulator = useEmu;
    }
    uint8_t getNHitsEmu() {
      return m_nHitsEmu;
    }
    void setNHitsEmu(uint8_t nHits) {
      m_nHitsEmu = nHits;
    }
    
    RunType getRunType() {
    return m_runType;
    }
    void setRunType(RunType run) {
      m_runType = run;
    }
    std::vector<unsigned int>& getPatternSeeds() {
      return m_patternSeeds;
    };
    void setPatternSeeds(std::vector<unsigned int> values) {
      m_patternSeeds = values;
    };
    bool getCloneCfgTag() {
      return m_cloneCfgTag;
    }
    bool getCloneModCfgTag() {
      return m_cloneModCfgTag;
    }
    std::string getTagSuffix() {
      return m_tagSuffix;
    }
    void setTagSuffix(std::string suff){
      m_tagSuffix = suff;
    }
    //! Loop attributes
    bool getLoopActive(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopActive[index];
      }
      return false;
    };
    void setLoopActive(int index, bool active) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopActive[index] = active;
      }
    };
    ScanParam getLoopParam(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopParam[index];
      }
      return NO_PAR;
    };
    void setLoopParam(int index, ScanParam par) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopParam[index] = par;
      }
    };
    bool getDspProcessing(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_dspProcessing[index];
      }
      return false;
    };
    void setDspProcessing(int index, bool dsp) {
      if (index >=0 && index < MAX_LOOPS) {
	m_dspProcessing[index] = dsp;
      }
    };
    bool getDspMaskStaging() {
      return m_dspMaskStaging;
    };
    void setDspMaskStaging(bool dsp) { 
      m_dspMaskStaging = dsp; 
    };
    bool getInnerLoopSwap() {
      return m_innerLoopSwap;
    };
    void setInnerLoopSwap(bool swp) {
      m_innerLoopSwap = swp;
    };
    std::vector<float>& getLoopVarValues(int index) {
      static std::vector<float> tmp;
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarValues[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return tmp;
    }; 

    bool preset(ScanType presetName, FEflavour feFlavour) {
#include "PixScanBase_preset.h"
      return true;
    }
    void setLoopVarValues(int index, double startVal, double endVal, int nSteps) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarNSteps[index] = nSteps;
	m_loopVarMin[index] = startVal;
	m_loopVarMax[index] = endVal;
	m_loopVarValues[index].clear();
	double v = startVal;
	double incr = (endVal - startVal)/(nSteps -1);
	for (int iv=0; iv<nSteps; iv++) {
	  m_loopVarValues[index].push_back(v);
	  v += incr;
	}
	m_loopVarUniform[index] = true;
	m_loopVarValuesFree[index] = false;
      } else {
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");        
	assert(0);
      }
    };
    void setLoopVarValues(int index, std::vector<float> values) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarValues[index] = values;
	m_loopVarNSteps[index] = values.size();
	m_loopVarMin[index] = m_loopVarMax[index] = values[0];
	for (unsigned int i=0; i<values.size(); i++) {
	  if (values[i] < m_loopVarMin[index]) m_loopVarMin[index] = values[i];
	  if (values[i] > m_loopVarMax[index]) m_loopVarMax[index] = values[i];
	}
	m_loopVarUniform[index] = false;
	m_loopVarValuesFree[index] = false;
      } else {
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");       
	assert(0);
      }
    };
    int getLoopVarNSteps(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarNSteps[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return 0;
    }
    float getLoopVarMin(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarMin[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return 0;
    }
    float getLoopVarMax(int index) {
    if (index >=0 && index < MAX_LOOPS) {
      return m_loopVarMax[index];
    }
    //    throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
    assert(0);
    return 0;
    }
    bool getLoopVarValuesFree(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarValuesFree[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    void setLoopVarValuesFree(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopVarValuesFree[index] = true;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
    bool getLoopVarUniform(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopVarUniform[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    EndLoopAction getLoopAction(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_loopAction[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return NO_ACTION;
    };
    void setLoopAction(int index, EndLoopAction action) {
      if (index >=0 && index < MAX_LOOPS) {
	m_loopAction[index] = action;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
    bool getDspLoopAction(int index) {
      if (index >=0 && index < MAX_LOOPS) {
	return m_dspLoopAction[index];
      }
      //      throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
      assert(0);
      return false;
    };
    void setDspLoopAction(int index, bool dsp) {
      if (index >=0 && index < MAX_LOOPS) {
	m_dspLoopAction[index] = dsp;
      } else
	//	throw PixScanExc(PixControllerExc::ERROR, "Invalid loop index");
	assert(0);
    };
       // Scan specific parameters
    void setThresholdTargetValue(int val) {
      m_thresholdTargetValue = val;
    };
    int getThresholdTargetValue(std::string moduleName="") {
      if(moduleName.empty() || !m_useTuningPointsFile)return m_thresholdTargetValue;
      return getTuningPointFromInputFile(moduleName, true);
    };
    void setDynamicThresholdTargetValue(int val) {
      m_dynamicThresholdTargetValue = val;
    };
    int getDynamicThresholdTargetValue() {
      return m_dynamicThresholdTargetValue;
    };
    void setTotTargetValue(int val) {
      m_totTargetValue = val;
    };
    int getTotTargetValue(std::string moduleName="") {
      if(moduleName.empty() || !m_useTuningPointsFile)return m_totTargetValue;
      return getTuningPointFromInputFile(moduleName, false);
    };
    void setTotTargetCharge(int val) {
      m_totTargetCharge = val;
    };
    int getTotTargetCharge() {
      return m_totTargetCharge;
    };
    unsigned int getUseAltModLinks() { 
      return m_useAltModLinks; 
    };
    void setUseAltModLinks(unsigned int alt) { 
      m_useAltModLinks = alt; 
    };
    std::string getSwapInLinks() { 
      return m_swapInLinks; 
    };
    void setSwapInLinks(std::string swin) { 
      m_swapInLinks = swin; 
    };
    std::string getSwapOutLinks() { 
      return m_swapOutLinks; 
    };
    void setSwapOutLinks(std::string swout) { 
      m_swapOutLinks = swout; 
    };
    void setFE(int val) {
      m_actualFE = val;
    };
    int getFE() { 
      return m_actualFE; 
    };
  
    std::vector<unsigned int> m_patternSeeds;
    bool newMaskStep() {
      return m_newMaskStage;
    }
    bool newScanStep() {
      return m_newScanStep;
    }
    bool newFE() {
      return m_newFE;
    }
    void setFEbyFE(bool val) {
      m_FEbyFE = val;
    };
    bool getFEbyFE()  {
      return m_FEbyFE;
    };
    void setFEbyFEMask(unsigned int feMask) {   
      m_FEbyFEMask = feMask;
    };  
    unsigned int getFEbyFEMask() {
      return m_FEbyFEMask;
    };
    void setFECounter(int val) {
      m_FECounter = val;
    };
    int getFECounter() {
      return m_FECounter;
    };
    
    void getFitMethod (FitMethod fitMethod){
     m_fitMethod = fitMethod;
    }
    
    FitMethod getFitMethod(){
      return m_fitMethod;
    };
    
    //Parse tuning point for each module from a given input file. If the module is not found the defaultValue is returned
    int getTuningPointFromInputFile(const std::string& moduleName,bool useThreshold) {

      int tuningPoint = 0;
      //Default
      if (useThreshold)tuningPoint = m_thresholdTargetValue;
      else tuningPoint = m_totTargetValue;

      std::string line;
      std::ifstream infile(m_tuningPointsFile);
        if (!infile.is_open() || !infile.good()) {
          std::cout<<"ERROR in opening tuning point file "<<m_tuningPointsFile<<std::endl;
          return tuningPoint;
        }

        while (std::getline(infile, line)) {
          std::istringstream iss(line);
          std::string ObjectName;
          float pointThr;
          float pointTot;
            if (!(iss>>ObjectName>>pointThr>>pointTot)) {
              std::cout<<"Error reading moduleName and tuning point from file. Returning default value:"<<tuningPoint<<std::endl;
              infile.close();
              return tuningPoint;
            }
            if (ObjectName==moduleName) {
              if (useThreshold)
	        tuningPoint = pointThr;
              else
	        tuningPoint = pointTot;

              std::cout<<"Found object name="<<ObjectName<<" corresponding to "<<moduleName<<" - returning "<<tuningPoint<<std::endl;
              infile.close();
              return tuningPoint;
           }
        }
      return tuningPoint;
    };

    // include presets
    /* return false if scan does not exist for a certain FE flavour */

   };
}

#endif /* PIXSCANBASE_H_ */

