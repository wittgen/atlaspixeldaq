#  Getting the repository

```
git clone --recurse-submodules https://:@gitlab.cern.ch:8443/atlas-pixel/daq/atlaspixeldaq.git
```



# Setting up the TDAQ environment 

 - SR1 development: 
```
source  /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-09-02-01
```
 - tbed runtime (after runnig the deployment script DeployTools/deploy_oks.py):
 ```
source  /your/deploy/path/setup_everything.sh
```
 - P1: see below

#  Compile on CentOS7

```
cmake_config $CMTCONFIG -- -DTDAQ_SYSTEM_INCLUDE=ON [-DBUILD_PPC=ON]

cd $CMTCONFIG

make -j8
```

# Compilation using clang


```
scripts/cmake_fix.sh

cd $CMTCONFIG

make CXX=clang++ CC=clang
```

# Other info
* Install i686 packages for Xilinx ISE 14.7 MicroBlaze cross compiler

```
sudo yum install -y libgcc.i686  glib2.i686 libstdc++.i686 zlib.i686
```

# Setting up and compiling at P1

Log into public machine, e.g. pc-atlas-pub-08 (c+ devel tools are installed)

```
source  /det/tdaq/scripts/setup_devel_09-02-01.sh

cmake_config $CMTCONFIG -- -DTDAQ_SYSTEM_INCLUDE=ON [-DBUILD_PPC=ON]

. DeployTool/deply_p1.sh   (will prompt for atpixdaq password)

```
