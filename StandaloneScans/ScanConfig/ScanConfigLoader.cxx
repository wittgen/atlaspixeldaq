#include <fstream>
#include <iostream>

#include "ScanConfigLoader.h"


using namespace StandaloneScans;

//Public Members
std::string ScanConfig::getConfigPreset(const std::string& config_name) {
  if (config_presets.count(config_name) > 0) {
    return config_presets[config_name];
  } else {
    // Replace with range-error exception?
    std::cout << "WARNING: Could not find config preset with key '" << config_name << "'\n";
    return "";
  }
}


void ScanConfig::storeConfigPreset(const std::string& key,
                                   const std::string& value) {
  config_presets[key] = value;
}


std::string ScanConfig::trimWhitespace(const std::string& str) {
  const auto str_start = str.find_first_not_of(" \t");
  const auto str_stop = str.find_last_not_of(" \t");
  
  // Excluding empty/whitespace-only lines for long str_start
  if (str_start == std::string::npos) {
    return "";
  }
  
  // Added 1 to avoid fencepost error
  const auto str_len = str_stop - str_start + 1;
  return str.substr(str_start, str_len);
}


bool ScanConfig::isComment(const std::string& str) {
  return str.find_first_of('#') == 0;
}


int ScanConfig::loadConfig() {
  std::ifstream config_file_stream;
  config_file_stream.open(config_path);
  
  if (!config_file_stream.is_open()) {
    std::cout << "ERROR: Could not open config file '"
	      << config_path << "'.\n";
    return 1;
  }
  
  std::string tmp_read_line;
  
  // Loop through Config, skipping comments starting with '#'
  std::cout << "Reading config...\n";
  while (std::getline(config_file_stream, tmp_read_line)) {
    std::string trimmed_line = trimWhitespace(tmp_read_line);

    if (isComment(trimmed_line)) {
      continue;
    }

    // Skipping empty lines
    if (trimmed_line == "") {
      continue;
    }

    const auto key_len = trimmed_line.find_first_of('=');
    if (key_len == std::string::npos) {
      std::cout << "WARNING: Could not identify config line '"
		<< tmp_read_line << "', skipping.\n";
      continue;
    }
    
    // Line is split in config key (before '=') and value, with '=' removed
    const auto value_start = key_len + 1;
    std::string key_with_ws = trimmed_line.substr(0, key_len);
    std::string value_with_ws = trimmed_line.substr(value_start);
    std::string key = trimWhitespace(key_with_ws);
    std::string value = trimWhitespace(value_with_ws);

    std::cout << "  Storing key '" << key << "' with value '" << value << "'\n";
    
    storeConfigPreset(key, value);
  }
  
  // Test, whether stream has reached the end of file
  if (!config_file_stream.eof()) {
    std::cout << "WARNING: Something went wrong before config EOF! "
	            << "Closing file.\n";
  }

  config_file_stream.close();
  return 0;
}
