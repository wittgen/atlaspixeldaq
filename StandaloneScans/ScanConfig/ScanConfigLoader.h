#ifndef _STANDALONESCANS_SCANCONFIGLOAD
#define _STANDALONESCANS_SCANCONFIGLOAD

#include <map>
#include <string>
#include <iostream>

namespace StandaloneScans {
  class ScanConfig {
    private:
    std::string config_path;
    std::map<std::string, std::string> config_presets;

    std::string trimWhitespace(const std::string&);
    bool isComment(const std::string&);
    void storeConfigPreset(const std::string&, const std::string&);

    public:
    // Initialization either interactive via console or with preset path
    ScanConfig() {std::cout << "Config_Path:\n"; std::cin >> config_path;};
    ScanConfig(const std::string& path) : config_path(path) {};

    int loadConfig();
    std::string getConfigPreset(const std::string&);
  };
}


#endif // _STANDALONESCANS_SCANCONFIGLOAD
