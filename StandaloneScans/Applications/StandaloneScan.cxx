#include <memory>
#include <iostream>
#include <stdexcept>

// TDAQ
#include "ipc/partition.h"

// PixLib
#include "PixLib/PixBroker/PixBrokerMultiCrate.h"
#include "PixLib/PixDbServer/PixDbServerInterface.h"
#include "PixLib/PixConnectivity/PixConnectivity.h"
#include "PixLib/PixModuleGroup/PixModuleGroup.h"
#include "PixLib/PixController/PixScan.h"
#include "PixLib/PixHistoServer/PixHistoServerInterface.h"

// StandaloneScans
#include "StandaloneScans/ScanConfig/ScanConfigLoader.h"


int main(int argc, char **argv) {

  // Very simple help message, to be replaced later by some
  // boost::program_options solution
  if (argc == 2) {
    std::string first_arg = argv[1];
    if (first_arg == "--help" || first_arg == "-h" || first_arg == "man") {
      std::cout << "StandaloneScans\n\n";
      std::cout << "Controls the automatic execution of scans without the need ";
      std::cout << "for ConsoleApp.  The input of scan parameters is handled ";
      std::cout << "via config files.  Templates for these can currently be ";
      std::cout << "found in the Pixel DAQ's 'StandaloneScans/Configs/' folder.\n";
      std::cout << "Config file paths are prompted later by StandaloneScans as ";
      std::cout << "absolute paths.\n";
      std::cout << "Currently, all command line options are passed on to IPCCore, ";
      std::cout << "except for help queries like this one.\n\n";
      std::cout << "Current config options:\n";
      std::cout << " - tdaq_partition :  TDAQ_Partition of current user.\n";
      std::cout << " - rod_crate      :  Number of the Crate housing the scanning ROD.\n";
      std::cout << " - crate_module   :  Number of the scanning ROD.\n";
      std::cout << " - fe_flav        :  Type of the scanned modules (I2 or I4B).\n";
      std::cout << " - servername     :  IS-instance to publish parameters to.\n";
      std::cout << " - occupancy      :  Number of hits to scale emulated results to.\n";
      std::cout << " - scan_id_no     :  Scan_ID for IS and (later) histogram identification.\n";
      return 0;
    }
  }

  auto scan_config = StandaloneScans::ScanConfig();

  if (scan_config.loadConfig() != 0) {
    std::cout << "Aborting!\n";
    return 1;
  }

  std::cout << "Config Loaded\n";

  std::string partition_name_string = scan_config.getConfigPreset("tdaq_partition");
  std::string rod_crate_string    = scan_config.getConfigPreset("rod_crate");
  std::string crate_module_string = scan_config.getConfigPreset("crate_module");
  std::string srvname             = scan_config.getConfigPreset("servername");
  std::string fe_flav_string      = scan_config.getConfigPreset("fe_flav");
  std::string occupancy_string    = scan_config.getConfigPreset("occupancy");
  std::string scan_id_string      = scan_config.getConfigPreset("scan_id_no");
  int scan_id_int;
  int occupancy;

  try {
    scan_id_int = std::stoi(scan_id_string);
  } catch (const std::invalid_argument& e) {
    std::cerr << "ERROR: Invalid argument in parameter 'scan_id_no'!\n"
              << "Parameter reads: \n "
              << scan_id_string << "\nAborting...\n";
    return 1;
  } catch (const std::out_of_range& e) {
    std::cerr << "ERROR: Parameter 'scan_id_no' out of range!\n"
              << "Parameter reads: \n "
              << scan_id_string << "\nAborting...\n";
    return 1;
  }

  try {
    occupancy = std::stoi(occupancy_string);
  } catch (const std::invalid_argument& e) {
    std::cerr << "ERROR: Invalid argument in parameter 'occupancy'!\n"
              << "Message reads: \n "
              << occupancy_string << "\nAborting...\n";
    return 1;
  } catch (const std::out_of_range& e) {
    std::cerr << "ERROR: Parameter 'occupancy' out of range!\n"
              << "Parameter reads: \n "
              << occupancy_string << "\nAborting...\n";
    return 1;
  }

  std::cout << "Be sure to set $TDAQ_PARTITION to your partition before running this script.\n";
  IPCCore::init(argc, argv);
  auto partition = IPCPartition(partition_name_string);
  std::cout << "Working TDAQ-Partition\n" << partition_name_string << "\n";
  
  //PixLib::PixDbServerInterface* DbServerIf = nullptr;
  bool ready = false;
  auto DbServerIf = PixLib::PixDbServerInterface(&partition, "PixelDbServer", PixLib::PixDbServerInterface::CLIENT, ready);
  
  std::string rod_longname_string = "ROD_CRATE_" + rod_crate_string;
  std::string rod_module_shortname_string = ( "ROD_C" + rod_crate_string
                                            + "_S" + crate_module_string);
  std::string broker_action_string = ( "SINGLE_ROD:"
                                     + rod_longname_string
                                     + "/" + rod_module_shortname_string);

  auto broker = PixLib::PixBrokerMultiCrate("StandaloneScanBroker", &partition);
  auto actions = broker.allocateActions({broker_action_string});
  auto conn = PixLib::PixConnectivity(&DbServerIf, "SR1-CONN", "SR1-CONN", "SR1-CONN", "SR1_BOC", "SR1-20", "SR1_MOD");
  conn.loadConn();
/*
  for(auto [sbcName, sbcConn]: conn.sbcs) {
    std::cout << "Got SBC " << sbcName << '\n';
  }

  for(auto [rodName, rodConn]: conn.rods) {
    std::cout << "Got ROD " << rodName << '\n';
  }

  auto modules = conn.listCfgObj("MODULE");
  for(auto [key, value] : modules) {
   std::cout << "Key: " << key << " Value: " << value << '\n';
  }

  */

  std::string pixel_provider_string = "pixel_provider_" + rod_longname_string;

  auto histo = PixLib::PixHistoServerInterface(&partition, "pixel_histo_server_sr1", "nameServer", pixel_provider_string);
  auto modgrp = PixLib::PixModuleGroup(&conn, &DbServerIf, nullptr, rod_module_shortname_string, partition_name_string);
  
  PixLib::EnumFEflavour::FEflavour fe_flav_enum;
  if (fe_flav_string == "I2" || fe_flav_string == "I3") {
    fe_flav_enum = PixLib::EnumFEflavour::PM_FE_I2;
  } else if (fe_flav_string == "I4A") {
    fe_flav_enum = PixLib::EnumFEflavour::PM_FE_I4A;
  } else if (fe_flav_string == "I4B") {
    fe_flav_enum = PixLib::EnumFEflavour::PM_FE_I4B;
  } else {
    std::cerr << "ERROR: FE Flavour Parameter '" << fe_flav_string << "' not recognised! Aborting...\n";
    broker.deallocateActions(actions);
    return 1;
  }
  auto dig_scn = PixLib::PixScan(PixLib::EnumScanType::DIGITAL_TEST, fe_flav_enum);

  ISInfoDictionary dict(partition);
  dict.checkin(srvname + "." + rod_module_shortname_string + "_ScanId", ISInfoInt(scan_id_int) );

  for(auto i = 0; i < 32; i++) modgrp.setModuleActive(i, true);
  modgrp.writeConfig();

  dig_scn.setHistogramFilled(PixLib::EnumHistogramType::OCCUPANCY, true);
  dig_scn.setHistogramKept(PixLib::EnumHistogramType::OCCUPANCY, true);
  dig_scn.setUseEmulator(true);
  dig_scn.setNHitsEmu(occupancy);

  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Repetitions", ISInfoInt(dig_scn.getRepetitions()) );
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_MaskTotalSteps", ISInfoInt(PixLib::EnumMaskSteps::getNumberOfSteps(
    			      static_cast<PixLib::EnumMaskSteps::MaskStageSteps>(dig_scn.getMaskStageTotalSteps())) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_MaskSteps", ISInfoInt(dig_scn.getMaskStageSteps()) );
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_ScurveMean_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::SCURVE_MEAN) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_ScurveSigma_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::SCURVE_SIGMA ) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_ScurveChi2_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::SCURVE_CHI2 ) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_ToTMean_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::TOT_MEAN ) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_ToTSigma_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::TOT_SIGMA ) ));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Hist_Occupancy_Fill", ISInfoBool(dig_scn.getHistogramFilled(PixLib::EnumHistogramType::OCCUPANCY )));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Loop0_Steps", ISInfoInt(dig_scn.getLoopVarNSteps(0)));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Loop0_Min", ISInfoInt(dig_scn.getLoopVarMin(0)));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Loop0_Max", ISInfoInt(dig_scn.getLoopVarMax(0)));
  dict.checkin(  srvname+"."+rod_module_shortname_string+"_Scan_Loop0_DSPProcessing", ISInfoBool(dig_scn.getDspProcessing(0)));

  dig_scn.resetScan();
  modgrp.initScan(&dig_scn);

  //modgrp.scanExecute(&dig_scn);

  modgrp.scanLoopStart(2, &dig_scn);
  while (dig_scn.loop(2)) {
    modgrp.prepareStep(2, &dig_scn, &histo);
    modgrp.scanLoopStart(1, &dig_scn);
    while (dig_scn.loop(1)) {
      modgrp.prepareStep(1, &dig_scn, &histo);
      modgrp.scanLoopStart(0, &dig_scn);
      while (dig_scn.loop(0)) {
        modgrp.prepareStep(0, &dig_scn, &histo);
        modgrp.scanExecute(&dig_scn);
        std::cout << "Completed loop: " << dig_scn.scanIndex(2) << "/";
        std::cout << dig_scn.scanIndex(1) << "/" << dig_scn.scanIndex(0) << std::endl;
        modgrp.scanTerminate(&dig_scn);
        dig_scn.next(0);
      }
      modgrp.scanLoopEnd(0, &dig_scn, &histo);
      dig_scn.next(1);
    }
    modgrp.scanLoopEnd(1, &dig_scn, &histo);
    dig_scn.next(2);
  }
  modgrp.scanLoopEnd(2, &dig_scn, &histo);
  modgrp.terminateScan(&dig_scn);
  std::cout << "Getting histos...\n";
  
  dig_scn.writeHisto(&histo, &modgrp, "/DMY"+std::to_string(scan_id_int)+"/");
  histo.writeRootHistoServer("/DMY"+std::to_string(scan_id_int)+"/", "test.root", false);
  
  /*
scn->resetScan();
grp->initScan(scn);

grp->scanLoopStart(2, scn);
while (scn->loop(2)) {
  grp->prepareStep(2,scn);
  grp->scanLoopStart(1, scn);
  while (scn->loop(1)) {
    grp->prepareStep(1,scn);
    grp->scanLoopStart(0, scn);
    while (scn->loop(0)) {
      grp->prepareStep(0,scn);
      grp->scanExecute(scn);
      std::cout << "Completed loop: " << scn->scanIndex(2) << "/";
      std::cout << scn->scanIndex(1) << "/" << scn->scanIndex(0) << std::endl;
      grp->scanTerminate(scn);
      scn->next(0);
    }
    grp->scanLoopEnd(0, scn);
    scn->next(1);
  }
  grp->scanLoopEnd(1, scn);
  scn->next(2);
}
grp->scanLoopEnd(2, scn);

grp->terminateScan(scn);
*/


  broker.deallocateActions(actions);
  //broker._destroy();
  return 0;
}
