# StandaloneScan Configurations for FEI4B (IBL, so C7S11, C3S18 ONLY)

tdaq_partition = PixelInfr_chscheul
rod_crate      = 3
crate_module   = 11
fe_flav        = I4B
servername     = Histogramming
occupancy      = 5
scan_id_no     = 9999999

# TODO: Amend for more parameters (Scan to be carried out, other scan settings...)
